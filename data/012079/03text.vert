<p>
<s>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
samostatné	samostatný	k2eAgNnSc1d1	samostatné
dílo	dílo	k1gNnSc1	dílo
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
hluboce	hluboko	k6eAd1	hluboko
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
drobné	drobný	k2eAgFnPc4d1	drobná
prózy	próza	k1gFnPc4	próza
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
autor	autor	k1gMnSc1	autor
soustředil	soustředit	k5eAaPmAgInS	soustředit
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c6	na
věci	věc	k1gFnSc6	věc
zdánlivě	zdánlivě	k6eAd1	zdánlivě
se	se	k3xPyFc4	se
vymykající	vymykající	k2eAgInSc1d1	vymykající
lidskému	lidský	k2eAgInSc3d1	lidský
rozumu	rozum	k1gInSc3	rozum
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nejednom	nejeden	k4xCyIgInSc6	nejeden
případě	případ	k1gInSc6	případ
válečnými	válečný	k2eAgFnPc7d1	válečná
událostmi	událost	k1gFnPc7	událost
přímo	přímo	k6eAd1	přímo
inspirovány	inspirovat	k5eAaBmNgFnP	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
princip	princip	k1gInSc1	princip
třinácti	třináct	k4xCc2	třináct
novelových	novelový	k2eAgFnPc2d1	novelový
balad	balada	k1gFnPc2	balada
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
všechno	všechen	k3xTgNnSc4	všechen
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
neviditelné	viditelný	k2eNgFnSc3d1	neviditelná
<g/>
,	,	kIx,	,
neznámé	známý	k2eNgFnSc3d1	neznámá
nebo	nebo	k8xC	nebo
nerozluštěné	rozluštěný	k2eNgFnSc3d1	nerozluštěná
události	událost	k1gFnSc3	událost
<g/>
:	:	kIx,	:
k	k	k7c3	k
původu	původ	k1gInSc3	původ
záhadné	záhadný	k2eAgFnSc2d1	záhadná
stopy	stopa	k1gFnSc2	stopa
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
<g/>
,	,	kIx,	,
k	k	k7c3	k
osudu	osud	k1gInSc3	osud
dívky	dívka	k1gFnSc2	dívka
bez	bez	k7c2	bez
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
k	k	k7c3	k
povaze	povaha	k1gFnSc3	povaha
prchajícího	prchající	k2eAgMnSc2d1	prchající
zločince	zločinec	k1gMnSc2	zločinec
či	či	k8xC	či
k	k	k7c3	k
cizí	cizí	k2eAgFnSc3d1	cizí
minulosti	minulost	k1gFnSc3	minulost
<g/>
,	,	kIx,	,
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
autorově	autorův	k2eAgInSc6d1	autorův
hlubokém	hluboký	k2eAgInSc6d1	hluboký
soucitu	soucit	k1gInSc6	soucit
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc7	jejich
omyly	omyl	k1gInPc7	omyl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povídky	povídka	k1gFnPc1	povídka
==	==	k?	==
</s>
</p>
<p>
<s>
Šlépěj	šlépěj	k1gFnSc1	šlépěj
je	být	k5eAaImIp3nS	být
filozofování	filozofování	k1gNnSc4	filozofování
dvou	dva	k4xCgMnPc2	dva
mužů	muž	k1gMnPc2	muž
nad	nad	k7c7	nad
stopou	stopa	k1gFnSc7	stopa
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
nevedou	vést	k5eNaImIp3nP	vést
žádné	žádný	k3yNgFnPc4	žádný
jiné	jiný	k2eAgFnPc4d1	jiná
stopy	stopa	k1gFnPc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Jakoby	jakoby	k8xS	jakoby
někdo	někdo	k3yInSc1	někdo
spadl	spadnout	k5eAaPmAgMnS	spadnout
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Lída	Lída	k1gFnSc1	Lída
řeší	řešit	k5eAaImIp3nS	řešit
pan	pan	k1gMnSc1	pan
Holub	Holub	k1gMnSc1	Holub
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Martincem	Martinec	k1gMnSc7	Martinec
záhadu	záhada	k1gFnSc4	záhada
ztracení	ztracení	k1gNnSc4	ztracení
dívky	dívka	k1gFnSc2	dívka
Lídy	Lída	k1gFnSc2	Lída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
novely	novela	k1gFnSc2	novela
Hora	hora	k1gFnSc1	hora
leží	ležet	k5eAaImIp3nS	ležet
mrtvola	mrtvola	k1gFnSc1	mrtvola
pod	pod	k7c7	pod
útesem	útes	k1gInSc7	útes
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
honba	honba	k1gFnSc1	honba
za	za	k7c7	za
pomyslným	pomyslný	k2eAgMnSc7d1	pomyslný
vrahem	vrah	k1gMnSc7	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
je	být	k5eAaImIp3nS	být
pronásledovaný	pronásledovaný	k2eAgInSc1d1	pronásledovaný
velkým	velký	k2eAgNnSc7d1	velké
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
houslista	houslista	k1gMnSc1	houslista
Jevíšek	Jevíšek	k1gMnSc1	Jevíšek
vše	všechen	k3xTgNnSc4	všechen
vidí	vidět	k5eAaImIp3nS	vidět
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Milostná	milostný	k2eAgFnSc1d1	milostná
píseň	píseň	k1gFnSc1	píseň
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
povídku	povídka	k1gFnSc4	povídka
"	"	kIx"	"
<g/>
Lída	Lída	k1gFnSc1	Lída
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Holub	Holub	k1gMnSc1	Holub
sleduje	sledovat	k5eAaImIp3nS	sledovat
stále	stále	k6eAd1	stále
veškeré	veškerý	k3xTgInPc4	veškerý
pohyby	pohyb	k1gInPc4	pohyb
Lídy	Lída	k1gFnSc2	Lída
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elegie	elegie	k1gFnSc1	elegie
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
povídky	povídka	k1gFnSc2	povídka
"	"	kIx"	"
<g/>
Šlépěj	šlépěj	k1gFnSc1	šlépěj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
filozofujícími	filozofující	k2eAgMnPc7d1	filozofující
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
nás	my	k3xPp1nPc4	my
zavedou	zavést	k5eAaPmIp3nP	zavést
do	do	k7c2	do
kavarny	kavarna	k1gFnSc2	kavarna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkají	potkat	k5eAaPmIp3nP	potkat
i	i	k9	i
bratra	bratr	k1gMnSc4	bratr
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
beze	beze	k7c2	beze
stopy	stopa	k1gFnSc2	stopa
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Utkvění	utkvění	k1gNnSc4	utkvění
času	čas	k1gInSc2	čas
filozofická	filozofický	k2eAgFnSc1d1	filozofická
úvaha	úvaha	k1gFnSc1	úvaha
nad	nad	k7c7	nad
významem	význam	k1gInSc7	význam
zvuků	zvuk	k1gInPc2	zvuk
ticha	ticho	k1gNnSc2	ticho
a	a	k8xC	a
hluku	hluk	k1gInSc2	hluk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
setkání	setkání	k1gNnSc6	setkání
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Ježek	Ježek	k1gMnSc1	Ježek
se	se	k3xPyFc4	se
ptá	ptat	k5eAaImIp3nS	ptat
vandráka	vandrák	k1gMnSc4	vandrák
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
mu	on	k3xPp3gMnSc3	on
vandrák	vandrák	k1gMnSc1	vandrák
nic	nic	k3yNnSc1	nic
neřekne	říct	k5eNaPmIp3nS	říct
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
představuje	představovat	k5eAaImIp3nS	představovat
jeho	jeho	k3xOp3gFnSc4	jeho
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
cesta	cesta	k1gFnSc1	cesta
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
rozhovoru	rozhovor	k1gInSc6	rozhovor
dvou	dva	k4xCgMnPc2	dva
mužů	muž	k1gMnPc2	muž
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
.	.	kIx.	.
</s>
<s>
Slyší	slyšet	k5eAaImIp3nS	slyšet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevidí	vidět	k5eNaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Nápis	nápis	k1gInSc1	nápis
hlavního	hlavní	k2eAgMnSc4d1	hlavní
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
nemocného	nemocný	k1gMnSc4	nemocný
Matyse	Matys	k1gMnSc4	Matys
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
navštívit	navštívit	k5eAaPmF	navštívit
jeho	jeho	k3xOp3gMnSc1	jeho
kolega	kolega	k1gMnSc1	kolega
Kvíčala	kvíčala	k1gFnSc1	kvíčala
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc2	který
trápí	trápit	k5eAaImIp3nS	trápit
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Matys	Matys	k1gMnSc1	Matys
je	být	k5eAaImIp3nS	být
nemocný	mocný	k2eNgMnSc1d1	nemocný
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
zdravý	zdravý	k1gMnSc1	zdravý
<g/>
.	.	kIx.	.
</s>
<s>
Matys	Matys	k1gMnSc1	Matys
poukáže	poukázat	k5eAaPmIp3nS	poukázat
na	na	k7c4	na
nápis	nápis	k1gInSc4	nápis
vyrytý	vyrytý	k2eAgInSc4d1	vyrytý
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
si	se	k3xPyFc3	se
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakého	jaký	k3yQgInSc2	jaký
důvodu	důvod	k1gInSc2	důvod
ho	on	k3xPp3gInSc4	on
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
smutné	smutný	k2eAgNnSc1d1	smutné
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
minulost	minulost	k1gFnSc4	minulost
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokušení	pokušení	k1gNnSc4	pokušení
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
hrdinovi	hrdina	k1gMnSc3	hrdina
dilema	dilema	k1gNnSc1	dilema
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zůstat	zůstat	k5eAaPmF	zůstat
doma	doma	k6eAd1	doma
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vydat	vydat	k5eAaPmF	vydat
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
?	?	kIx.	?
</s>
<s>
Oční	oční	k2eAgInSc4d1	oční
kontakt	kontakt	k1gInSc4	kontakt
které	který	k3yRgFnSc2	který
ženy	žena	k1gFnSc2	žena
ho	on	k3xPp3gMnSc4	on
láká	lákat	k5eAaImIp3nS	lákat
víc	hodně	k6eAd2	hodně
<g/>
?	?	kIx.	?
</s>
<s>
Mladého	mladý	k1gMnSc2	mladý
naivního	naivní	k2eAgNnSc2d1	naivní
děvčete	děvče	k1gNnSc2	děvče
nebo	nebo	k8xC	nebo
zkušené	zkušený	k2eAgFnSc2d1	zkušená
nestoudné	stoudný	k2eNgFnSc2d1	nestoudná
ženy	žena	k1gFnSc2	žena
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Odrazy	odraz	k1gInPc1	odraz
všech	všecek	k3xTgFnPc2	všecek
událostí	událost	k1gFnPc2	událost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vidí	vidět	k5eAaImIp3nS	vidět
nemocný	mocný	k2eNgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
každý	každý	k3xTgInSc4	každý
letní	letní	k2eAgInSc4d1	letní
den	den	k1gInSc4	den
vysedává	vysedávat	k5eAaImIp3nS	vysedávat
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čekárna	čekárna	k1gFnSc1	čekárna
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Záruba	Záruba	k1gMnSc1	Záruba
<g/>
,	,	kIx,	,
přečkává	přečkávat	k5eAaImIp3nS	přečkávat
noc	noc	k1gFnSc4	noc
v	v	k7c6	v
čekárně	čekárna	k1gFnSc6	čekárna
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
nocležníky	nocležník	k1gMnPc7	nocležník
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
hnusí	hnusit	k5eAaImIp3nS	hnusit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
připomíná	připomínat	k5eAaImIp3nS	připomínat
sebe	sebe	k3xPyFc4	sebe
samého	samý	k3xTgMnSc4	samý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomoc	pomoc	k1gFnSc1	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yRgFnPc1	jaký
myšlenky	myšlenka	k1gFnPc1	myšlenka
vás	vy	k3xPp2nPc4	vy
budou	být	k5eAaImBp3nP	být
provázet	provázet	k5eAaImF	provázet
<g/>
,	,	kIx,	,
<g/>
když	když	k8xS	když
nevyslyšíte	vyslyšet	k5eNaPmIp2nP	vyslyšet
volání	volání	k1gNnSc4	volání
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
==	==	k?	==
Překlady	překlad	k1gInPc4	překlad
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Překlady	překlad	k1gInPc4	překlad
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
:	:	kIx,	:
Gottesmarter	Gottesmarter	k1gMnSc1	Gottesmarter
(	(	kIx(	(
<g/>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
:	:	kIx,	:
S.	S.	kA	S.
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvních	první	k4xOgNnPc6	první
pět	pět	k4xCc4	pět
novel	novela	k1gFnPc2	novela
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Boží	božit	k5eAaImIp3nS	božit
muka	muka	k1gFnSc1	muka
(	(	kIx(	(
<g/>
Šlépěj	šlépěj	k1gFnSc1	šlépěj
<g/>
,	,	kIx,	,
Lída	Lída	k1gFnSc1	Lída
<g/>
,	,	kIx,	,
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Milostná	milostný	k2eAgFnSc1d1	milostná
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
Elegie	elegie	k1gFnSc1	elegie
<g/>
)	)	kIx)	)
u	u	k7c2	u
čtyřech	čtyři	k4xCgInPc2	čtyři
novel	novela	k1gFnPc2	novela
uveden	uveden	k2eAgMnSc1d1	uveden
překladatel	překladatel	k1gMnSc1	překladatel
Otto	Otto	k1gMnSc1	Otto
Pick	Pick	k1gMnSc1	Pick
<g/>
,	,	kIx,	,
u	u	k7c2	u
novely	novela	k1gFnSc2	novela
Lída	Lída	k1gFnSc1	Lída
(	(	kIx(	(
<g/>
Lelia	Lelia	k1gFnSc1	Lelia
<g/>
)	)	kIx)	)
překladatel	překladatel	k1gMnSc1	překladatel
není	být	k5eNaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
:	:	kIx,	:
Kreuzwege	Kreuzwege	k1gFnSc1	Kreuzwege
(	(	kIx(	(
<g/>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
:	:	kIx,	:
Kurt	kurt	k1gInSc1	kurt
Wolff	Wolff	k1gInSc1	Wolff
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
edice	edice	k1gFnPc1	edice
"	"	kIx"	"
<g/>
Der	drát	k5eAaImRp2nS	drát
Jüngste	Jüngst	k1gMnSc5	Jüngst
Tag	tag	k1gInSc1	tag
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
64	[number]	k4	64
<g/>
.	.	kIx.	.
-	-	kIx~	-
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
novel	novela	k1gFnPc2	novela
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Boží	božit	k5eAaImIp3nS	božit
muka	muka	k1gFnSc1	muka
(	(	kIx(	(
<g/>
Stocken	Stocken	k1gInSc1	Stocken
der	drát	k5eAaImRp2nS	drát
Zeit	Zeit	k1gInSc1	Zeit
-	-	kIx~	-
Utkvění	utkvění	k1gNnSc1	utkvění
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
ohne	ohnout	k5eAaPmIp3nS	ohnout
Worte	Wort	k1gInSc5	Wort
-	-	kIx~	-
Historie	historie	k1gFnSc1	historie
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
Verlorener	Verlorener	k1gMnSc1	Verlorener
Weg	Weg	k1gMnSc1	Weg
-	-	kIx~	-
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
Die	Die	k1gFnSc1	Die
Aufschrift	Aufschrift	k1gMnSc1	Aufschrift
-	-	kIx~	-
Nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
Die	Die	k1gMnSc1	Die
Versuchung	Versuchung	k1gMnSc1	Versuchung
-	-	kIx~	-
Pokušení	pokušení	k1gNnSc1	pokušení
<g/>
,	,	kIx,	,
Spiegelung	Spiegelung	k1gInSc1	Spiegelung
-	-	kIx~	-
Odrazy	odraz	k1gInPc1	odraz
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Wartesaal	Wartesaal	k1gInSc1	Wartesaal
-	-	kIx~	-
Čekárna	čekárna	k1gFnSc1	čekárna
<g/>
,	,	kIx,	,
Hilfe	Hilf	k1gMnSc5	Hilf
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
Pomoc	pomoc	k1gFnSc1	pomoc
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Otto	Otto	k1gMnSc1	Otto
Pick	Pick	k1gMnSc1	Pick
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Boží	božit	k5eAaImIp3nS	božit
muka	muka	k1gFnSc1	muka
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boží	božit	k5eAaImIp3nS	božit
muka	muka	k1gFnSc1	muka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
(	(	kIx(	(
<g/>
soubor	soubor	k1gInSc1	soubor
novel	novela	k1gFnPc2	novela
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
<p>
<s>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
cesky-jazyk	ceskyazyko	k1gNnPc2	cesky-jazyko
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
