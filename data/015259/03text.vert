<s>
Kawasaki	Kawasaki	k1gNnSc1
OH-1	OH-1	k1gFnSc2
</s>
<s>
Kawasaki	Kawasaki	k1gNnSc1
OH-1	OH-1	k1gFnSc2
Kawasaki	Kawasaki	k1gNnSc2
OH-	OH-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
Určení	určení	k1gNnSc2
</s>
<s>
ozbrojený	ozbrojený	k2eAgInSc4d1
pozorovací	pozorovací	k2eAgInSc4d1
vrtulník	vrtulník	k1gInSc4
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Kawasaki	Kawasaki	k6eAd1
Aerospace	Aerospace	k1gFnSc1
Company	Compana	k1gFnSc2
První	první	k4xOgFnPc4
let	léto	k1gNnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1996	#num#	k4
Uživatel	uživatel	k1gMnSc1
</s>
<s>
Japonské	japonský	k2eAgFnSc2d1
pozemní	pozemní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
sebeobrany	sebeobrana	k1gFnSc2
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
38	#num#	k4
(	(	kIx(
<g/>
březen	březen	k1gInSc1
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kawasaki	Kawasaki	k1gNnSc1
OH-1	OH-1	k1gFnSc2
je	být	k5eAaImIp3nS
ozbrojený	ozbrojený	k2eAgInSc1d1
lehký	lehký	k2eAgInSc1d1
pozorovací	pozorovací	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
vyvinutý	vyvinutý	k2eAgInSc1d1
japonskou	japonský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Kawasaki	Kawasak	k1gFnSc2
Aerospace	Aerospace	k1gFnSc2
Company	Compana	k1gFnSc2
(	(	kIx(
<g/>
součást	součást	k1gFnSc1
koncernu	koncern	k1gInSc2
Kawasaki	Kawasaki	k1gNnSc2
Heavy	Heava	k1gFnSc2
Industries	Industriesa	k1gFnPc2
<g/>
)	)	kIx)
pro	pro	k7c4
Japonské	japonský	k2eAgFnPc4d1
pozemní	pozemní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
sebeobrany	sebeobrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
vrtulník	vrtulník	k1gInSc1
vyvinutý	vyvinutý	k2eAgInSc1d1
japonským	japonský	k2eAgInSc7d1
průmyslem	průmysl	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
koncipován	koncipovat	k5eAaBmNgInS
pro	pro	k7c4
operace	operace	k1gFnPc4
v	v	k7c6
hornatém	hornatý	k2eAgNnSc6d1
přírodním	přírodní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Často	často	k6eAd1
je	být	k5eAaImIp3nS
přezdíván	přezdíván	k2eAgMnSc1d1
Ninja	Ninja	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vrtulník	vrtulník	k1gInSc1
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
jako	jako	k8xS,k8xC
náhrada	náhrada	k1gFnSc1
amerického	americký	k2eAgInSc2d1
typu	typ	k1gInSc2
OH-	OH-	k1gFnSc2
<g/>
6	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
financí	finance	k1gFnPc2
však	však	k8xC
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
příliš	příliš	k6eAd1
malém	malý	k2eAgInSc6d1
počtu	počet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Maketa	maketa	k1gFnSc1
XOH-1	XOH-1	k1gFnSc2
</s>
<s>
OH-1	OH-1	k4
za	za	k7c2
letu	let	k1gInSc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
<g/>
,	,	kIx,
provizorně	provizorně	k6eAd1
označeného	označený	k2eAgMnSc4d1
OH-X	OH-X	k1gMnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
ve	v	k7c6
finančním	finanční	k2eAgInSc6d1
roce	rok	k1gInSc6
1992	#num#	k4
zadán	zadán	k2eAgInSc1d1
konstrukčnímu	konstrukční	k2eAgInSc3d1
týmu	tým	k1gInSc3
OHET	OHET	kA
(	(	kIx(
<g/>
Observation	Observation	k1gInSc1
Helicopter	Helicopter	k1gMnSc1
Team	team	k1gInSc1
<g/>
)	)	kIx)
tvořenému	tvořený	k2eAgInSc3d1
společnostmi	společnost	k1gFnPc7
Kawasaki	Kawasak	k1gFnSc2
(	(	kIx(
<g/>
60	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Fuji	Fuji	kA
(	(	kIx(
<g/>
20	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Mitsubishi	mitsubishi	k1gNnSc4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
získat	získat	k5eAaPmF
náhradu	náhrada	k1gFnSc4
licenčně	licenčně	k6eAd1
vyráběného	vyráběný	k2eAgInSc2d1
typu	typ	k1gInSc2
OH-	OH-	k1gFnSc2
<g/>
6	#num#	k4
<g/>
D.	D.	kA
V	v	k7c6
září	září	k1gNnSc6
1994	#num#	k4
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
technologická	technologický	k2eAgFnSc1d1
maketa	maketa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
veřejnosti	veřejnost	k1gFnSc2
představena	představit	k5eAaPmNgFnS
pod	pod	k7c7
označením	označení	k1gNnSc7
Kogata	Kogat	k1gMnSc2
Kansoku	Kansok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1996	#num#	k4
program	program	k1gInSc1
dostal	dostat	k5eAaPmAgInS
definitivní	definitivní	k2eAgNnSc4d1
označení	označení	k1gNnSc4
OH-	OH-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	léto	k1gNnPc6
1996-1997	1996-1997	k4
byly	být	k5eAaImAgFnP
postaveny	postavit	k5eAaPmNgFnP
celkem	celkem	k6eAd1
čtyři	čtyři	k4xCgInPc4
letové	letový	k2eAgInPc4d1
prototypy	prototyp	k1gInPc4
a	a	k8xC
dva	dva	k4xCgInPc4
další	další	k2eAgInPc4d1
pro	pro	k7c4
pozemní	pozemní	k2eAgInPc4d1
testy	test	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
květnu	květen	k1gInSc6
1996	#num#	k4
a	a	k8xC
poprvé	poprvé	k6eAd1
vzlétl	vzlétnout	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgMnSc1
sériový	sériový	k2eAgMnSc1d1
OH-1	OH-1	k1gMnSc1
byl	být	k5eAaImAgMnS
japonské	japonský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
předán	předat	k5eAaPmNgInS
v	v	k7c6
lednu	leden	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
původně	původně	k6eAd1
chtělo	chtít	k5eAaImAgNnS
zakoupit	zakoupit	k5eAaPmF
180	#num#	k4
<g/>
–	–	k?
<g/>
200	#num#	k4
vrtulníků	vrtulník	k1gInPc2
OH-1	OH-1	k1gFnPc2
(	(	kIx(
<g/>
amerických	americký	k2eAgFnPc2d1
OH-6D	OH-6D	k1gFnPc2
měla	mít	k5eAaImAgFnS
armáda	armáda	k1gFnSc1
téměř	téměř	k6eAd1
300	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
rozpočtovým	rozpočtový	k2eAgInPc3d1
škrtům	škrt	k1gInPc3
však	však	k9
bylo	být	k5eAaImAgNnS
do	do	k7c2
března	březen	k1gInSc2
2012	#num#	k4
dodáno	dodat	k5eAaPmNgNnS
pouhých	pouhý	k2eAgInPc2d1
34	#num#	k4
sériových	sériový	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zvažovaný	zvažovaný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
zvětšené	zvětšený	k2eAgFnSc2d1
bitevní	bitevní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
OH-	OH-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
Kai	Kai	k1gMnSc1
(	(	kIx(
<g/>
AH-X	AH-X	k1gMnSc1
<g/>
)	)	kIx)
nepřekročil	překročit	k5eNaPmAgInS
fázi	fáze	k1gFnSc4
studií	studio	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgInPc4d1
bitevní	bitevní	k2eAgInPc4d1
vrtulníky	vrtulník	k1gInPc4
AH-1S	AH-1S	k1gFnSc2
Cobra	Cobro	k1gNnSc2
tak	tak	k6eAd1
nahradily	nahradit	k5eAaPmAgInP
vrtulníky	vrtulník	k1gInPc1
AH-64D	AH-64D	k1gMnSc2
Apache	Apach	k1gMnSc2
Longbow	Longbow	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
dvoumotorový	dvoumotorový	k2eAgInSc4d1
dvoumístný	dvoumístný	k2eAgInSc4d1
vrtulník	vrtulník	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
svou	svůj	k3xOyFgFnSc7
koncepcí	koncepce	k1gFnSc7
připomíná	připomínat	k5eAaImIp3nS
bitevní	bitevní	k2eAgInPc4d1
typy	typ	k1gInPc4
(	(	kIx(
<g/>
napr	napr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurocopter	Eurocopter	k1gMnSc1
Tiger	Tiger	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoumístná	dvoumístný	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
sedí	sedit	k5eAaImIp3nS
v	v	k7c6
tandemové	tandemový	k2eAgFnSc6d1
kabině	kabina	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
chráněna	chránit	k5eAaImNgFnS
pancéřováním	pancéřování	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
skleněný	skleněný	k2eAgInSc4d1
kokpit	kokpit	k1gInSc4
s	s	k7c7
průhledovým	průhledový	k2eAgInSc7d1
displejem	displej	k1gInSc7
<g/>
,	,	kIx,
více	hodně	k6eAd2
multifunkčními	multifunkční	k2eAgInPc7d1
displeji	displej	k1gInPc7
a	a	k8xC
ovládáním	ovládání	k1gNnSc7
HOTAS	HOTAS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
konstrukci	konstrukce	k1gFnSc6
vrtulníku	vrtulník	k1gInSc2
jsou	být	k5eAaImIp3nP
široce	široko	k6eAd1
využity	využít	k5eAaPmNgInP
kompozitní	kompozitní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
dva	dva	k4xCgInPc4
turbohřídelové	turbohřídelový	k2eAgInPc4d1
motory	motor	k1gInPc4
Mitsubishi	mitsubishi	k1gNnSc2
TS1-1QT	TS1-1QT	k1gFnSc2
(	(	kIx(
<g/>
662	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
s	s	k7c7
digitálním	digitální	k2eAgNnSc7d1
ovládáním	ovládání	k1gNnSc7
FADEC	FADEC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ty	ty	k3xPp2nSc1
pohání	pohánět	k5eAaImIp3nS
Kompozitní	kompozitní	k2eAgInSc1d1
kompozitní	kompozitní	k2eAgInSc1d1
čtyřlistý	čtyřlistý	k2eAgInSc1d1
hlavní	hlavní	k2eAgInSc1d1
rotor	rotor	k1gInSc1
a	a	k8xC
vyrovnávací	vyrovnávací	k2eAgInSc4d1
fenestron	fenestron	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojice	dvojice	k1gFnSc1
pomocných	pomocný	k2eAgNnPc2d1
křídel	křídlo	k1gNnPc2
o	o	k7c6
třímetrovém	třímetrový	k2eAgNnSc6d1
rozpětí	rozpětí	k1gNnSc6
nese	nést	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
závěsníky	závěsník	k1gInPc4
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
druhy	druh	k1gInPc4
výzbroje	výzbroj	k1gFnSc2
(	(	kIx(
<g/>
protitankové	protitankový	k2eAgFnSc2d1
střely	střela	k1gFnSc2
Toshiba	Toshiba	kA
Type	typ	k1gInSc5
91	#num#	k4
<g/>
,	,	kIx,
protiletadlové	protiletadlový	k2eAgInPc4d1
řízené	řízený	k2eAgInPc4d1
střely	střel	k1gInPc4
<g/>
,	,	kIx,
bloky	blok	k1gInPc4
neřízených	řízený	k2eNgFnPc2d1
střel	střela	k1gFnPc2
<g/>
,	,	kIx,
kanónové	kanónový	k2eAgInPc1d1
kontejnery	kontejner	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
vnitřních	vnitřní	k2eAgInPc6d1
závěsnících	závěsník	k1gInPc6
též	tenž	k3xDgFnSc2
přídavné	přídavný	k2eAgFnSc2d1
palivové	palivový	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
nosnost	nosnost	k1gFnSc4
je	být	k5eAaImIp3nS
132	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
jejímu	její	k3xOp3gNnSc3
zaměřování	zaměřování	k1gNnSc3
slouží	sloužit	k5eAaImIp3nP
nad	nad	k7c7
kabinou	kabina	k1gFnSc7
umístěný	umístěný	k2eAgInSc1d1
senzorový	senzorový	k2eAgInSc1d1
blok	blok	k1gInSc1
<g/>
,	,	kIx,
nesoucí	nesoucí	k2eAgInSc1d1
systém	systém	k1gInSc1
FLIR	FLIR	kA
(	(	kIx(
<g/>
od	od	k7c2
Fujitsu	Fujits	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
TV	TV	kA
kameru	kamera	k1gFnSc4
a	a	k8xC
laserový	laserový	k2eAgInSc4d1
značkovač	značkovač	k1gInSc4
s	s	k7c7
dálkoměrem	dálkoměr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulník	vrtulník	k1gInSc1
je	být	k5eAaImIp3nS
vybaven	vybavit	k5eAaPmNgInS
pevným	pevný	k2eAgInSc7d1
podvozkem	podvozek	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Specifikace	specifikace	k1gFnSc1
(	(	kIx(
<g/>
OH-	OH-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čelní	čelní	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
OH-1	OH-1	k1gFnSc4
</s>
<s>
Údaje	údaj	k1gInPc1
podle	podle	k7c2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
2	#num#	k4
</s>
<s>
Průměr	průměr	k1gInSc1
nosného	nosný	k2eAgInSc2d1
rotoru	rotor	k1gInSc2
<g/>
:	:	kIx,
11,6	11,6	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
13,4	13,4	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
3,8	3,8	k4
m	m	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
prázdného	prázdný	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
:	:	kIx,
2450	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
3550	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
4000	#num#	k4
kg	kg	kA
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
2	#num#	k4
×	×	k?
<g/>
turbohřídelový	turbohřídelový	k2eAgInSc1d1
motor	motor	k1gInSc1
Mitsubishi	mitsubishi	k1gNnSc2
TS1-10QT	TS1-10QT	k1gFnSc2
</s>
<s>
Výkon	výkon	k1gInSc1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
662	#num#	k4
kW	kW	kA
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
220	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
270	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
u	u	k7c2
země	zem	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
200	#num#	k4
km	km	kA
(	(	kIx(
<g/>
bojový	bojový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
550	#num#	k4
km	km	kA
(	(	kIx(
<g/>
přeletový	přeletový	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
16	#num#	k4
010	#num#	k4
stop	stopa	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
OH-1	OH-1	k1gFnPc2
Ninja	Ninj	k1gInSc2
Light	Light	k2eAgInSc1d1
Observation	Observation	k1gInSc1
Helicopter	Helicopter	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Army-technology	Army-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
XOH-1	XOH-1	k1gFnPc2
(	(	kIx(
<g/>
OH-X	OH-X	k1gFnSc1
<g/>
)	)	kIx)
New	New	k1gFnSc1
Observation	Observation	k1gInSc1
Helicopter	Helicopter	k1gMnSc1
/	/	kIx~
AH-2	AH-2	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Globalsecurity	Globalsecurita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Japanese	Japanese	k1gFnSc1
to	ten	k3xDgNnSc4
launch	launch	k1gMnSc1
Kawasaki	Kawasaki	k1gNnSc2
OH-X	OH-X	k1gMnSc1
scout	scout	k1gMnSc1
helicopter	helicopter	k1gMnSc1
in	in	k?
1997	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FlightGlobal	FlightGlobal	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Kawasaki	Kawasaki	k1gNnPc2
OH-1	OH-1	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aviastar	Aviastar	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Kawasaki	Kawasaki	k1gNnPc2
XOH-	XOH-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
LXXII	LXXII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
17	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
OH-1	OH-1	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airforceworld	Airforceworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kawasaki	Kawasak	k1gFnSc2
OH-1	OH-1	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kawasaki	Kawasaki	k1gNnSc1
OH-1	OH-1	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
