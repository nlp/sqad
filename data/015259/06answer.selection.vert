<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
vrtulník	vrtulník	k1gInSc1
vyvinutý	vyvinutý	k2eAgInSc1d1
japonským	japonský	k2eAgInSc7d1
průmyslem	průmysl	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
koncipován	koncipovat	k5eAaBmNgInS
pro	pro	k7c4
operace	operace	k1gFnPc4
v	v	k7c6
hornatém	hornatý	k2eAgNnSc6d1
přírodním	přírodní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Často	často	k6eAd1
je	být	k5eAaImIp3nS
přezdíván	přezdíván	k2eAgMnSc1d1
Ninja	Ninja	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vrtulník	vrtulník	k1gInSc1
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
jako	jako	k8xS,k8xC
náhrada	náhrada	k1gFnSc1
amerického	americký	k2eAgInSc2d1
typu	typ	k1gInSc2
OH-	OH-	k1gFnSc2
<g/>
6	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
financí	finance	k1gFnPc2
však	však	k8xC
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
příliš	příliš	k6eAd1
malém	malý	k2eAgInSc6d1
počtu	počet	k1gInSc6
<g/>
.	.	kIx.
</s>