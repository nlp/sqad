<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
adeliae	adelia	k1gInSc2	adelia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
zástupce	zástupce	k1gMnSc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
tučňákovitých	tučňákovitý	k2eAgMnPc2d1	tučňákovitý
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
zpravidla	zpravidla	k6eAd1	zpravidla
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
–	–	k?	–
pokud	pokud	k8xS	pokud
nehladoví	hladovět	k5eNaImIp3nS	hladovět
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
váhy	váha	k1gFnPc4	váha
až	až	k9	až
6	[number]	k4	6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
totožné	totožný	k2eAgNnSc4d1	totožné
zbarvení	zbarvení	k1gNnSc4	zbarvení
jako	jako	k8xS	jako
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bývá	bývat	k5eAaImIp3nS	bývat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
uniformně	uniformně	k6eAd1	uniformně
černo-bílé	černoíl	k1gMnPc1	černo-bíl
<g/>
,	,	kIx,	,
specifickým	specifický	k2eAgInSc7d1	specifický
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
lemování	lemování	k1gNnSc1	lemování
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kroužku	kroužek	k1gInSc2	kroužek
kolem	kolem	k7c2	kolem
obou	dva	k4xCgNnPc2	dva
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
krilem	kril	k1gInSc7	kril
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
hlavonožce	hlavonožec	k1gMnSc4	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
uzpůsobeno	uzpůsobit	k5eAaPmNgNnS	uzpůsobit
k	k	k7c3	k
potápění	potápění	k1gNnSc3	potápění
a	a	k8xC	a
plavání	plavání	k1gNnSc4	plavání
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc1d1	připomínající
torpédo	torpédo	k1gNnSc1	torpédo
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
vlnu	vlna	k1gFnSc4	vlna
–	–	k?	–
začíná	začínat	k5eAaImIp3nS	začínat
užším	úzký	k2eAgInSc7d2	užší
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
silnějším	silný	k2eAgNnSc7d2	silnější
zaobleným	zaoblený	k2eAgNnSc7d1	zaoblené
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
zúženým	zúžený	k2eAgInSc7d1	zúžený
koncem	konec	k1gInSc7	konec
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
úzkým	úzký	k2eAgInSc7d1	úzký
jednoduše	jednoduše	k6eAd1	jednoduše
řešeným	řešený	k2eAgInSc7d1	řešený
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
až	až	k9	až
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
plavat	plavat	k5eAaImF	plavat
rychlostí	rychlost	k1gFnSc7	rychlost
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
je	být	k5eAaImIp3nS	být
i	i	k9	i
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
známý	známý	k2eAgInSc1d1	známý
až	až	k9	až
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podniká	podnikat	k5eAaImIp3nS	podnikat
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nalezl	naleznout	k5eAaPmAgInS	naleznout
stabilní	stabilní	k2eAgInSc1d1	stabilní
masivní	masivní	k2eAgInSc1d1	masivní
ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
letní	letní	k2eAgFnSc1d1	letní
teplotou	teplota	k1gFnSc7	teplota
neroztaje	roztát	k5eNaPmIp3nS	roztát
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gInSc4	on
nezničí	zničit	k5eNaPmIp3nS	zničit
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
kolonie	kolonie	k1gFnPc1	kolonie
naopak	naopak	k6eAd1	naopak
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
moře	moře	k1gNnSc4	moře
podstatně	podstatně	k6eAd1	podstatně
blíže	blízce	k6eAd2	blízce
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
během	během	k7c2	během
antarktického	antarktický	k2eAgNnSc2d1	antarktické
léta	léto	k1gNnSc2	léto
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
až	až	k8xS	až
únor	únor	k1gInSc1	únor
<g/>
)	)	kIx)	)
v	v	k7c6	v
několikatisícových	několikatisícový	k2eAgFnPc6d1	několikatisícová
až	až	k8xS	až
statisícových	statisícový	k2eAgFnPc6d1	statisícová
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaPmIp3nP	stavit
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
hnízda	hnízdo	k1gNnPc1	hnízdo
z	z	k7c2	z
oblázků	oblázek	k1gInPc2	oblázek
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snese	snést	k5eAaPmIp3nS	snést
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc1	dva
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
při	při	k7c6	při
sezení	sezení	k1gNnSc6	sezení
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
se	s	k7c7	s
samcem	samec	k1gInSc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Narozená	narozený	k2eAgNnPc1d1	narozené
mláďata	mládě	k1gNnPc1	mládě
vychovávají	vychovávat	k5eAaImIp3nP	vychovávat
společně	společně	k6eAd1	společně
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k1gInSc1	pár
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
věrný	věrný	k2eAgInSc1d1	věrný
po	po	k7c4	po
několik	několik	k4yIc4	několik
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgMnPc4d3	Nejběžnější
zástupce	zástupce	k1gMnPc4	zástupce
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
není	být	k5eNaImIp3nS	být
prozatím	prozatím	k6eAd1	prozatím
významně	významně	k6eAd1	významně
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
jakožto	jakožto	k8xS	jakožto
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
jedinců	jedinec	k1gMnPc2	jedinec
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
12	[number]	k4	12
až	až	k9	až
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňáka	tučňák	k1gMnSc4	tučňák
kroužkového	kroužkový	k2eAgInSc2d1	kroužkový
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Jules	Jules	k1gMnSc1	Jules
Sébastien	Sébastina	k1gFnPc2	Sébastina
César	César	k1gMnSc1	César
Dumont	Dumont	k1gMnSc1	Dumont
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Urville	Urville	k1gNnSc1	Urville
(	(	kIx(	(
<g/>
známější	známý	k2eAgMnSc1d2	známější
spíše	spíše	k9	spíše
jako	jako	k9	jako
Jules	Jules	k1gMnSc1	Jules
Dumont	Dumont	k1gMnSc1	Dumont
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Urville	Urville	k1gFnSc1	Urville
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
objevil	objevit	k5eAaPmAgInS	objevit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
lodi	loď	k1gFnSc2	loď
Adélinu	Adélin	k2eAgFnSc4d1	Adélina
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
podle	podle	k7c2	podle
křestního	křestní	k2eAgNnSc2d1	křestní
jména	jméno	k1gNnSc2	jméno
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
vědecky	vědecky	k6eAd1	vědecky
popsán	popsat	k5eAaPmNgMnS	popsat
dvojicí	dvojice	k1gFnSc7	dvojice
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
a	a	k8xC	a
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
<g/>
,	,	kIx,	,
Jacquesem	Jacques	k1gMnSc7	Jacques
Bernardem	Bernard	k1gMnSc7	Bernard
Hombronem	Hombron	k1gMnSc7	Hombron
a	a	k8xC	a
Honoré	Honorý	k2eAgFnPc4d1	Honorý
Jacquinotem	Jacquinot	k1gInSc7	Jacquinot
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
spatřen	spatřen	k2eAgMnSc1d1	spatřen
–	–	k?	–
tučňák	tučňák	k1gMnSc1	tučňák
Adéliin	Adéliin	k2eAgMnSc1d1	Adéliin
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
jako	jako	k9	jako
Catarrhactes	Catarrhactes	k1gMnSc1	Catarrhactes
adeliae	adeliaat	k5eAaPmIp3nS	adeliaat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
adeliae	adeliae	k1gFnSc1	adeliae
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Pygoscelis	Pygoscelis	k1gFnSc2	Pygoscelis
spadají	spadat	k5eAaImIp3nP	spadat
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
tučňáků	tučňák	k1gMnPc2	tučňák
–	–	k?	–
tučňák	tučňák	k1gMnSc1	tučňák
uzdičkový	uzdičkový	k2eAgMnSc1d1	uzdičkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
antarctica	antarctic	k1gInSc2	antarctic
<g/>
)	)	kIx)	)
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
papua	papu	k1gInSc2	papu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
a	a	k8xC	a
jaderná	jaderný	k2eAgFnSc1d1	jaderná
DNA	dna	k1gFnSc1	dna
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předci	předek	k1gMnPc1	předek
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
tučňáků	tučňák	k1gMnPc2	tučňák
asi	asi	k9	asi
před	před	k7c7	před
38	[number]	k4	38
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
po	po	k7c6	po
předcích	předek	k1gInPc6	předek
rodu	rod	k1gInSc2	rod
Aptenodytes	Aptenodytesa	k1gFnPc2	Aptenodytesa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
bazální	bazální	k2eAgFnSc7d1	bazální
větví	větev	k1gFnSc7	větev
celého	celý	k2eAgInSc2d1	celý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
druh	druh	k1gInSc1	druh
oddělil	oddělit	k5eAaPmAgInS	oddělit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
19	[number]	k4	19
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Jedinci	jedinec	k1gMnPc1	jedinec
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
známý	známý	k1gMnSc1	známý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
přenášejí	přenášet	k5eAaImIp3nP	přenášet
oblázky	oblázek	k1gInPc4	oblázek
(	(	kIx(	(
<g/>
menší	malý	k2eAgInPc4d2	menší
kameny	kámen	k1gInPc4	kámen
<g/>
)	)	kIx)	)
z	z	k7c2	z
okolních	okolní	k2eAgNnPc2d1	okolní
sousedních	sousední	k2eAgNnPc2d1	sousední
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokládají	pokládat	k5eAaImIp3nP	pokládat
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rodový	rodový	k2eAgInSc1d1	rodový
název	název	k1gInSc1	název
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
scelis	scelis	k1gFnSc1	scelis
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
scelus	scelus	k1gInSc1	scelus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
které	který	k3yIgFnPc1	který
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
zločin	zločin	k1gInSc1	zločin
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
krádež	krádež	k1gFnSc4	krádež
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
internetové	internetový	k2eAgFnSc2d1	internetová
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
ARKive	ARKiev	k1gFnSc2	ARKiev
dostali	dostat	k5eAaPmAgMnP	dostat
vědecké	vědecký	k2eAgNnSc4d1	vědecké
jméno	jméno	k1gNnSc4	jméno
Pygoscelis	Pygoscelis	k1gFnSc2	Pygoscelis
–	–	k?	–
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
opatřený	opatřený	k2eAgMnSc1d1	opatřený
smetákem	smeták	k1gInSc7	smeták
<g/>
"	"	kIx"	"
–	–	k?	–
na	na	k7c6	na
základě	základ	k1gInSc6	základ
komického	komický	k2eAgInSc2d1	komický
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc1d1	připomínající
zametání	zametání	k1gNnSc1	zametání
<g/>
;	;	kIx,	;
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolébavou	kolébavý	k2eAgFnSc7d1	kolébavá
chůzí	chůze	k1gFnSc7	chůze
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
roztřepeným	roztřepený	k2eAgInSc7d1	roztřepený
ocasem	ocas	k1gInSc7	ocas
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
zavadí	zavadit	k5eAaPmIp3nS	zavadit
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
nese	nést	k5eAaImIp3nS	nést
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
název	název	k1gInSc1	název
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
podle	podle	k7c2	podle
bílé	bílý	k2eAgFnSc2d1	bílá
zbarvených	zbarvený	k2eAgInPc2d1	zbarvený
kroužků	kroužek	k1gInPc2	kroužek
kolem	kolem	k7c2	kolem
obou	dva	k4xCgNnPc2	dva
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
žádné	žádný	k3yNgInPc1	žádný
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
podél	podél	k7c2	podél
celého	celý	k2eAgNnSc2d1	celé
pobřeží	pobřeží	k1gNnSc2	pobřeží
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
u	u	k7c2	u
Antarktického	antarktický	k2eAgInSc2d1	antarktický
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
na	na	k7c6	na
okolních	okolní	k2eAgInPc6d1	okolní
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
Scottův	Scottův	k2eAgInSc1d1	Scottův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
Petra	Petra	k1gFnSc1	Petra
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Shetlandy	Shetlanda	k1gFnSc2	Shetlanda
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnPc1d1	jižní
Orkneje	Orkneje	k1gFnPc1	Orkneje
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgInPc1d1	jižní
Sandwichovy	Sandwichův	k2eAgInPc1d1	Sandwichův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Georgie	Georgie	k1gFnSc1	Georgie
a	a	k8xC	a
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
Haswellově	Haswellův	k2eAgInSc6d1	Haswellův
ostrově	ostrov	k1gInSc6	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k7c4	mimo
hnízdní	hnízdní	k2eAgNnSc4d1	hnízdní
období	období	k1gNnSc4	období
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zatoulá	zatoulat	k5eAaPmIp3nS	zatoulat
až	až	k9	až
k	k	k7c3	k
Argentině	Argentina	k1gFnSc3	Argentina
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Novému	nový	k2eAgInSc3d1	nový
Zélandu	Zéland	k1gInSc3	Zéland
<g/>
,	,	kIx,	,
Falklandám	Falklanda	k1gFnPc3	Falklanda
(	(	kIx(	(
<g/>
Malvíny	Malvína	k1gFnSc2	Malvína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Heardovu	Heardův	k2eAgInSc3d1	Heardův
ostrovu	ostrov	k1gInSc3	ostrov
a	a	k8xC	a
k	k	k7c3	k
McDonaldovým	McDonaldův	k2eAgInPc3d1	McDonaldův
ostrovům	ostrov	k1gInPc3	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ocitá	ocitat	k5eAaImIp3nS	ocitat
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
4000	[number]	k4	4000
km	km	kA	km
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
migračního	migrační	k2eAgNnSc2d1	migrační
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
zdolat	zdolat	k5eAaPmF	zdolat
13	[number]	k4	13
000	[number]	k4	000
až	až	k9	až
17	[number]	k4	17
000	[number]	k4	000
km	km	kA	km
na	na	k7c6	na
širém	širý	k2eAgInSc6d1	širý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dostaví	dostavit	k5eAaPmIp3nP	dostavit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
vykrmit	vykrmit	k5eAaPmF	vykrmit
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
si	se	k3xPyFc3	se
tukové	tukový	k2eAgFnPc4d1	tuková
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
se	se	k3xPyFc4	se
na	na	k7c4	na
nadcházející	nadcházející	k2eAgNnSc4d1	nadcházející
období	období	k1gNnSc4	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
teplotách	teplota	k1gFnPc6	teplota
vzduchu	vzduch	k1gInSc2	vzduch
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
-30	-30	k4	-30
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Antarktidy	Antarktida	k1gFnSc2	Antarktida
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
neustále	neustále	k6eAd1	neustále
kolem	kolem	k7c2	kolem
bodu	bod	k1gInSc2	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
klesnout	klesnout	k5eAaPmF	klesnout
i	i	k9	i
těsně	těsně	k6eAd1	těsně
pod	pod	k7c4	pod
nulu	nula	k1gFnSc4	nula
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
živé	živý	k2eAgInPc4d1	živý
organizmy	organizmus	k1gInPc4	organizmus
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
pramenitě	pramenitě	k6eAd1	pramenitě
čisté	čistý	k2eAgInPc4d1	čistý
a	a	k8xC	a
prostoupené	prostoupený	k2eAgInPc4d1	prostoupený
ionty	ion	k1gInPc4	ion
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
i	i	k9	i
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
ledem	led	k1gInSc7	led
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
pod	pod	k7c7	pod
nulou	nula	k1gFnSc7	nula
tekutá	tekutý	k2eAgFnSc1d1	tekutá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
cm	cm	kA	cm
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
hranici	hranice	k1gFnSc4	hranice
70	[number]	k4	70
cm	cm	kA	cm
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
)	)	kIx)	)
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
3,9	[number]	k4	3,9
až	až	k9	až
6,5	[number]	k4	6,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
výkyvy	výkyv	k1gInPc4	výkyv
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
línání	línání	k1gNnSc2	línání
a	a	k8xC	a
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
ohled	ohled	k1gInSc4	ohled
i	i	k9	i
na	na	k7c4	na
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
(	(	kIx(	(
<g/>
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
poměrně	poměrně	k6eAd1	poměrně
uniformní	uniformní	k2eAgNnPc1d1	uniformní
zbarvení	zbarvení	k1gNnPc1	zbarvení
–	–	k?	–
černou	černý	k2eAgFnSc4d1	černá
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
záda	záda	k1gNnPc4	záda
a	a	k8xC	a
vrch	vrch	k1gInSc4	vrch
křídel	křídlo	k1gNnPc2	křídlo
s	s	k7c7	s
úzkými	úzký	k2eAgInPc7d1	úzký
bílými	bílý	k2eAgInPc7d1	bílý
pruhy	pruh	k1gInPc7	pruh
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc4d1	spodní
stranu	strana	k1gFnSc4	strana
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
břicho	břicho	k1gNnSc1	břicho
má	mít	k5eAaImIp3nS	mít
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Specifickým	specifický	k2eAgInSc7d1	specifický
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
kroužky	kroužek	k1gInPc1	kroužek
kolem	kolem	k6eAd1	kolem
obou	dva	k4xCgNnPc2	dva
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
lemující	lemující	k2eAgFnPc1d1	lemující
duhovky	duhovka	k1gFnPc1	duhovka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
červenohnědé	červenohnědý	k2eAgFnSc2d1	červenohnědá
až	až	k6eAd1	až
zelenohnědou	zelenohnědý	k2eAgFnSc7d1	zelenohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
nepřehlédnutelný	přehlédnutelný	k2eNgMnSc1d1	nepřehlédnutelný
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
výjimečně	výjimečně	k6eAd1	výjimečně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
kartáčový	kartáčový	k2eAgInSc1d1	kartáčový
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarvit	k5eAaPmNgFnP	zbarvit
do	do	k7c2	do
růžové	růžový	k2eAgFnSc2d1	růžová
až	až	k8xS	až
světle	světle	k6eAd1	světle
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podrážky	podrážka	k1gFnPc1	podrážka
mají	mít	k5eAaImIp3nP	mít
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
pozdravech	pozdrav	k1gInPc6	pozdrav
nebo	nebo	k8xC	nebo
při	při	k7c6	při
vzrušení	vzrušení	k1gNnSc6	vzrušení
se	se	k3xPyFc4	se
peří	peřit	k5eAaImIp3nS	peřit
na	na	k7c6	na
temeni	temeno	k1gNnSc6	temeno
hlavy	hlava	k1gFnSc2	hlava
naježí	naježit	k5eAaPmIp3nS	naježit
do	do	k7c2	do
pomyslného	pomyslný	k2eAgInSc2d1	pomyslný
hřebene	hřeben	k1gInSc2	hřeben
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzhledem	vzhled	k1gInSc7	vzhled
připomíná	připomínat	k5eAaImIp3nS	připomínat
"	"	kIx"	"
<g/>
punkový	punkový	k2eAgInSc1d1	punkový
střih	střih	k1gInSc1	střih
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
a	a	k8xC	a
dospívající	dospívající	k2eAgMnPc1d1	dospívající
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
zbarvení	zbarvení	k1gNnSc4	zbarvení
podobné	podobný	k2eAgNnSc4d1	podobné
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
specifických	specifický	k2eAgInPc2d1	specifický
očních	oční	k2eAgInPc2d1	oční
lemů	lem	k1gInPc2	lem
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
vybarví	vybarvit	k5eAaPmIp3nS	vybarvit
až	až	k9	až
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
temně	temně	k6eAd1	temně
černého	černý	k2eAgInSc2d1	černý
šatu	šat	k1gInSc2	šat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
se	se	k3xPyFc4	se
v	v	k7c6	v
divoké	divoký	k2eAgFnSc6d1	divoká
přírodě	příroda	k1gFnSc6	příroda
dožívá	dožívat	k5eAaImIp3nS	dožívat
až	až	k9	až
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedinec	jedinec	k1gMnSc1	jedinec
páří	pářit	k5eAaImIp3nS	pářit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dožije	dožít	k5eAaPmIp3nS	dožít
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nižšího	nízký	k2eAgInSc2d2	nižší
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obranné	obranný	k2eAgInPc4d1	obranný
mechanizmy	mechanizmus	k1gInPc4	mechanizmus
proti	proti	k7c3	proti
mrazu	mráz	k1gInSc3	mráz
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
krátké	krátké	k1gNnSc4	krátké
ale	ale	k8xC	ale
husté	hustý	k2eAgNnSc4d1	husté
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
překrývající	překrývající	k2eAgFnSc4d1	překrývající
se	se	k3xPyFc4	se
jako	jako	k9	jako
střešní	střešní	k2eAgFnPc1d1	střešní
tašky	taška	k1gFnPc1	taška
odpuzující	odpuzující	k2eAgFnSc4d1	odpuzující
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
1	[number]	k4	1
cm	cm	kA	cm
čtvereční	čtvereční	k2eAgFnSc2d1	čtvereční
připadne	připadnout	k5eAaPmIp3nS	připadnout
asi	asi	k9	asi
12	[number]	k4	12
peříček	peříčko	k1gNnPc2	peříčko
dlouhých	dlouhý	k2eAgMnPc2d1	dlouhý
až	až	k6eAd1	až
3,6	[number]	k4	3,6
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Perní	perný	k2eAgMnPc1d1	perný
šat	šat	k1gInSc1	šat
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vrstva	vrstva	k1gFnSc1	vrstva
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
nad	nad	k7c7	nad
kůží	kůže	k1gFnSc7	kůže
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
prachové	prachový	k2eAgNnSc1d1	prachové
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
plní	plnit	k5eAaImIp3nP	plnit
funkci	funkce	k1gFnSc4	funkce
izolační	izolační	k2eAgMnSc1d1	izolační
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
leží	ležet	k5eAaImIp3nS	ležet
druhá	druhý	k4xOgFnSc1	druhý
vrstva	vrstva	k1gFnSc1	vrstva
mastných	mastný	k2eAgNnPc2d1	mastné
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
odolávají	odolávat	k5eAaImIp3nP	odolávat
vnějším	vnější	k2eAgInPc3d1	vnější
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nS	peřit
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
izolaci	izolace	k1gFnSc6	izolace
před	před	k7c7	před
nízkými	nízký	k2eAgFnPc7d1	nízká
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
čištěno	čistit	k5eAaImNgNnS	čistit
žluklým	žluklý	k2eAgInSc7d1	žluklý
sekretem	sekret	k1gInSc7	sekret
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
tučňák	tučňák	k1gMnSc1	tučňák
nanese	nanést	k5eAaPmIp3nS	nanést
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
g	g	kA	g
tohoto	tento	k3xDgInSc2	tento
sekretu	sekreta	k1gFnSc4	sekreta
<g/>
.	.	kIx.	.
</s>
<s>
Promaštěné	promaštěný	k2eAgNnSc1d1	promaštěné
peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgNnSc1d1	funkční
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ochráněné	ochráněný	k2eAgFnSc6d1	ochráněná
před	před	k7c7	před
plísněmi	plíseň	k1gFnPc7	plíseň
a	a	k8xC	a
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Podkožní	podkožní	k2eAgFnSc1d1	podkožní
vrstva	vrstva	k1gFnSc1	vrstva
tuku	tuk	k1gInSc2	tuk
u	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
kroužkového	kroužkový	k2eAgInSc2d1	kroužkový
není	být	k5eNaImIp3nS	být
primárně	primárně	k6eAd1	primárně
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
chladem	chlad	k1gInSc7	chlad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
výživová	výživový	k2eAgFnSc1d1	výživová
rezerva	rezerva	k1gFnSc1	rezerva
v	v	k7c6	v
období	období	k1gNnSc6	období
hladovění	hladovění	k1gNnSc2	hladovění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
pelichání	pelichání	k1gNnSc2	pelichání
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgNnSc1d1	původní
peří	peří	k1gNnSc1	peří
postupně	postupně	k6eAd1	postupně
vytlačeno	vytlačit	k5eAaPmNgNnS	vytlačit
novým	nový	k2eAgInSc7d1	nový
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
mezery	mezera	k1gFnPc1	mezera
okamžitě	okamžitě	k6eAd1	okamžitě
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
se	se	k3xPyFc4	se
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vody	voda	k1gFnSc2	voda
však	však	k9	však
tučňáci	tučňák	k1gMnPc1	tučňák
během	během	k7c2	během
pelichání	pelichání	k1gNnSc2	pelichání
nemohou	moct	k5eNaImIp3nP	moct
a	a	k8xC	a
hladoví	hladovět	k5eAaImIp3nP	hladovět
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
trvá	trvat	k5eAaImIp3nS	trvat
přepeřování	přepeřování	k1gNnSc4	přepeřování
20	[number]	k4	20
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
jsou	být	k5eAaImIp3nP	být
tučňáci	tučňák	k1gMnPc1	tučňák
obdařeni	obdařen	k2eAgMnPc1d1	obdařen
kožním	kožní	k2eAgInSc7d1	kožní
vakem	vak	k1gInSc7	vak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obrácená	obrácený	k2eAgFnSc1d1	obrácená
kapsa	kapsa	k1gFnSc1	kapsa
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
břišní	břišní	k2eAgFnSc6d1	břišní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
chrání	chránit	k5eAaImIp3nS	chránit
vejce	vejce	k1gNnSc1	vejce
a	a	k8xC	a
narozená	narozený	k2eAgNnPc1d1	narozené
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sezení	sezení	k1gNnSc6	sezení
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
bezprostřednímu	bezprostřední	k2eAgInSc3d1	bezprostřední
kontaktu	kontakt	k1gInSc3	kontakt
se	s	k7c7	s
silně	silně	k6eAd1	silně
prokrvenou	prokrvený	k2eAgFnSc7d1	prokrvená
kůží	kůže	k1gFnSc7	kůže
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hnízdní	hnízdní	k2eAgFnSc7d1	hnízdní
nažinou	nažina	k1gFnSc7	nažina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vejce	vejce	k1gNnPc4	vejce
i	i	k8xC	i
mláďata	mládě	k1gNnPc4	mládě
do	do	k7c2	do
určitého	určitý	k2eAgNnSc2d1	určité
stáří	stáří	k1gNnSc2	stáří
jsou	být	k5eAaImIp3nP	být
skryta	skryt	k2eAgFnSc1d1	skryta
před	před	k7c7	před
nepříznivými	příznivý	k2eNgInPc7d1	nepříznivý
vlivy	vliv	k1gInPc7	vliv
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
zahřívána	zahříván	k2eAgFnSc1d1	zahřívána
při	při	k7c6	při
stabilní	stabilní	k2eAgFnSc6d1	stabilní
teplotě	teplota	k1gFnSc6	teplota
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
===	===	k?	===
Orientace	orientace	k1gFnSc1	orientace
===	===	k?	===
</s>
</p>
<p>
<s>
Pokusy	pokus	k1gInPc1	pokus
ornitologů	ornitolog	k1gMnPc2	ornitolog
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
má	mít	k5eAaImIp3nS	mít
výborně	výborně	k6eAd1	výborně
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
navigaci	navigace	k1gFnSc4	navigace
a	a	k8xC	a
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
toulá	toulat	k5eAaImIp3nS	toulat
mimo	mimo	k7c4	mimo
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
tisíce	tisíc	k4xCgInSc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
širém	širý	k2eAgNnSc6d1	širé
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
ve	v	k7c4	v
správný	správný	k2eAgInSc4d1	správný
čas	čas	k1gInSc4	čas
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
tzv.	tzv.	kA	tzv.
vrozené	vrozený	k2eAgFnSc2d1	vrozená
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
odvezený	odvezený	k2eAgInSc1d1	odvezený
letadlem	letadlo	k1gNnSc7	letadlo
4000	[number]	k4	4000
km	km	kA	km
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
se	se	k3xPyFc4	se
za	za	k7c4	za
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
vrátil	vrátit	k5eAaPmAgMnS	vrátit
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
odchycen	odchycen	k2eAgInSc1d1	odchycen
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
orientační	orientační	k2eAgInSc4d1	orientační
smysl	smysl	k1gInSc4	smysl
zpřesňuje	zpřesňovat	k5eAaImIp3nS	zpřesňovat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
polohy	poloha	k1gFnSc2	poloha
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
srovnává	srovnávat	k5eAaImIp3nS	srovnávat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
biologický	biologický	k2eAgInSc4d1	biologický
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pokusy	pokus	k1gInPc1	pokus
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zatažené	zatažený	k2eAgFnSc6d1	zatažená
obloze	obloha	k1gFnSc6	obloha
viditelně	viditelně	k6eAd1	viditelně
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
přehled	přehled	k1gInSc4	přehled
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
svým	svůj	k3xOyFgInSc7	svůj
vrozeným	vrozený	k2eAgInSc7d1	vrozený
instinktem	instinkt	k1gInSc7	instinkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komunikace	komunikace	k1gFnPc4	komunikace
a	a	k8xC	a
sluch	sluch	k1gInSc4	sluch
===	===	k?	===
</s>
</p>
<p>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
kontaktní	kontaktní	k2eAgInSc1d1	kontaktní
zvuk	zvuk	k1gInSc1	zvuk
zní	znět	k5eAaImIp3nS	znět
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgNnSc1d1	krátké
"	"	kIx"	"
<g/>
aark	aark	k6eAd1	aark
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
výzvu	výzva	k1gFnSc4	výzva
používá	používat	k5eAaImIp3nS	používat
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
taktéž	taktéž	k?	taktéž
během	běh	k1gInSc7	běh
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Volání	volání	k1gNnSc1	volání
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
zhruba	zhruba	k6eAd1	zhruba
znamenat	znamenat	k5eAaImF	znamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
Vám	vy	k3xPp2nPc3	vy
stále	stále	k6eAd1	stále
na	na	k7c6	na
blízku	blízko	k1gNnSc6	blízko
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
takový	takový	k3xDgInSc4	takový
projev	projev	k1gInSc4	projev
doprovázen	doprovázen	k2eAgInSc4d1	doprovázen
tichým	tichý	k2eAgInSc7d1	tichý
a	a	k8xC	a
krátkým	krátký	k2eAgNnPc3d1	krátké
zavrčením	zavrčení	k1gNnPc3	zavrčení
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
očním	oční	k2eAgInSc7d1	oční
kontaktem	kontakt	k1gInSc7	kontakt
na	na	k7c4	na
obcházející	obcházející	k2eAgMnPc4d1	obcházející
tučňáky	tučňák	k1gMnPc4	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
naopak	naopak	k6eAd1	naopak
zpravidla	zpravidla	k6eAd1	zpravidla
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
zvuku	zvuk	k1gInSc2	zvuk
drží	držet	k5eAaImIp3nS	držet
"	"	kIx"	"
<g/>
podřízený	podřízený	k2eAgInSc1d1	podřízený
postoj	postoj	k1gInSc1	postoj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mají	mít	k5eAaImIp3nP	mít
napřímené	napřímený	k2eAgNnSc4d1	napřímené
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
celkově	celkově	k6eAd1	celkově
přitaženým	přitažený	k2eAgNnSc7d1	přitažené
peřím	peří	k1gNnSc7	peří
a	a	k8xC	a
roztažená	roztažený	k2eAgNnPc4d1	roztažené
křídla	křídlo	k1gNnPc4	křídlo
směrující	směrující	k2eAgNnPc4d1	směrující
dozadu	dozadu	k6eAd1	dozadu
–	–	k?	–
čímž	což	k3yRnSc7	což
zhruba	zhruba	k6eAd1	zhruba
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jen	jen	k9	jen
procházím	procházet	k5eAaImIp1nS	procházet
a	a	k8xC	a
nemám	mít	k5eNaImIp1nS	mít
zlý	zlý	k2eAgInSc4d1	zlý
úmysl	úmysl	k1gInSc4	úmysl
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
projevuje	projevovat	k5eAaImIp3nS	projevovat
agresivním	agresivní	k2eAgNnSc7d1	agresivní
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
"	"	kIx"	"
<g/>
grr	grr	k?	grr
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
gwarr	gwarr	k1gInSc1	gwarr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
zastrašování	zastrašování	k1gNnSc2	zastrašování
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
vysunutým	vysunutý	k2eAgInSc7d1	vysunutý
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
přímému	přímý	k2eAgInSc3d1	přímý
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
období	období	k1gNnSc4	období
námluv	námluva	k1gFnPc2	námluva
samec	samec	k1gMnSc1	samec
láká	lákat	k5eAaImIp3nS	lákat
samici	samice	k1gFnSc3	samice
hlasitými	hlasitý	k2eAgInPc7d1	hlasitý
hrdelními	hrdelní	k2eAgInPc7d1	hrdelní
tóny	tón	k1gInPc7	tón
<g/>
,	,	kIx,	,
uzavřené	uzavřený	k2eAgInPc1d1	uzavřený
páry	pár	k1gInPc1	pár
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
svými	svůj	k3xOyFgInPc7	svůj
hlasy	hlas	k1gInPc7	hlas
neustále	neustále	k6eAd1	neustále
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
ujišťují	ujišťovat	k5eAaImIp3nP	ujišťovat
se	se	k3xPyFc4	se
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsi	být	k5eAaImIp2nS	být
to	ten	k3xDgNnSc1	ten
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
můj	můj	k3xOp1gMnSc1	můj
manžel	manžel	k1gMnSc1	manžel
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
tvůj	tvůj	k3xOp2gInSc4	tvůj
manžel	manžel	k1gMnSc1	manžel
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
žádají	žádat	k5eAaImIp3nP	žádat
potravu	potrava	k1gFnSc4	potrava
pískáním	pískání	k1gNnSc7	pískání
<g/>
.	.	kIx.	.
<g/>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
slyší	slyšet	k5eAaImIp3nS	slyšet
frekvence	frekvence	k1gFnSc1	frekvence
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
27	[number]	k4	27
až	až	k9	až
12	[number]	k4	12
520	[number]	k4	520
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgNnPc6d2	nižší
kmitočtových	kmitočtový	k2eAgNnPc6d1	kmitočtové
pásmech	pásmo	k1gNnPc6	pásmo
dokáže	dokázat	k5eAaPmIp3nS	dokázat
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
8	[number]	k4	8
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Sluch	sluch	k1gInSc1	sluch
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
párem	pár	k1gInSc7	pár
(	(	kIx(	(
<g/>
námluvy	námluva	k1gFnSc2	námluva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodičem	rodič	k1gMnSc7	rodič
a	a	k8xC	a
mládětem	mládě	k1gNnSc7	mládě
<g/>
,	,	kIx,	,
cizími	cizí	k2eAgFnPc7d1	cizí
tučňáky	tučňák	k1gMnPc4	tučňák
nebo	nebo	k8xC	nebo
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koloniích	kolonie	k1gFnPc6	kolonie
nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
že	že	k8xS	že
tučňáci	tučňák	k1gMnPc1	tučňák
musí	muset	k5eAaImIp3nP	muset
dobře	dobře	k6eAd1	dobře
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
též	též	k9	též
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
hlasy	hlas	k1gInPc4	hlas
a	a	k8xC	a
zvuky	zvuk	k1gInPc4	zvuk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hlas	hlas	k1gInSc4	hlas
družky	družka	k1gFnSc2	družka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
tučňák	tučňák	k1gMnSc1	tučňák
má	mít	k5eAaImIp3nS	mít
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
hlas	hlas	k1gInSc1	hlas
(	(	kIx(	(
<g/>
zvuk	zvuk	k1gInSc1	zvuk
hlasu	hlas	k1gInSc2	hlas
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
také	také	k9	také
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fyziologické	fyziologický	k2eAgNnSc4d1	fyziologické
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
k	k	k7c3	k
nadměrnému	nadměrný	k2eAgInSc3d1	nadměrný
příjmu	příjem	k1gInSc3	příjem
soli	sůl	k1gFnSc2	sůl
===	===	k?	===
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
slaná	slaný	k2eAgNnPc4d1	slané
moře	moře	k1gNnPc4	moře
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
tekutin	tekutina	k1gFnPc2	tekutina
(	(	kIx(	(
<g/>
případným	případný	k2eAgInSc7d1	případný
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
i	i	k9	i
krev	krev	k1gFnSc1	krev
kořisti	kořist	k1gFnSc2	kořist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jeho	jeho	k3xOp3gFnSc1	jeho
potrava	potrava	k1gFnSc1	potrava
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
bez	bez	k7c2	bez
nějakého	nějaký	k3yIgNnSc2	nějaký
opatření	opatření	k1gNnSc2	opatření
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
koncentraci	koncentrace	k1gFnSc3	koncentrace
sodíku	sodík	k1gInSc2	sodík
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Přebytečnou	přebytečný	k2eAgFnSc4d1	přebytečná
sůl	sůl	k1gFnSc4	sůl
tak	tak	k6eAd1	tak
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
pomocí	pomocí	k7c2	pomocí
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
supraorbitálních	supraorbitální	k2eAgFnPc2d1	supraorbitální
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
umístěných	umístěný	k2eAgInPc2d1	umístěný
nad	nad	k7c7	nad
očnicí	očnice	k1gFnSc7	očnice
a	a	k8xC	a
ústících	ústící	k2eAgFnPc2d1	ústící
do	do	k7c2	do
nosních	nosní	k2eAgFnPc2d1	nosní
dutin	dutina	k1gFnPc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Nadbytečné	nadbytečný	k2eAgNnSc1d1	nadbytečné
množství	množství	k1gNnSc1	množství
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
stéká	stékat	k5eAaImIp3nS	stékat
po	po	k7c6	po
zobáku	zobák	k1gInSc6	zobák
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
častokrát	častokrát	k6eAd1	častokrát
otřepe	otřepat	k5eAaPmIp3nS	otřepat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
==	==	k?	==
</s>
</p>
<p>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byl	být	k5eAaImAgMnS	být
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
výhradně	výhradně	k6eAd1	výhradně
rybožravý	rybožravý	k2eAgMnSc1d1	rybožravý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
vybíjení	vybíjení	k1gNnSc2	vybíjení
tuleňů	tuleň	k1gMnPc2	tuleň
a	a	k8xC	a
velryb	velryba	k1gFnPc2	velryba
od	od	k7c2	od
asi	asi	k9	asi
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
přebývat	přebývat	k5eAaImF	přebývat
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
vodách	voda	k1gFnPc6	voda
až	až	k9	až
150	[number]	k4	150
tun	tuna	k1gFnPc2	tuna
krilu	krilat	k5eAaPmIp1nS	krilat
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
využil	využít	k5eAaPmAgMnS	využít
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	s	k7c7	s
krilem	kril	k1gInSc7	kril
živí	živit	k5eAaImIp3nP	živit
z	z	k7c2	z
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
korýšem	korýš	k1gMnSc7	korýš
krunýřovkou	krunýřovka	k1gFnSc7	krunýřovka
krillovou	krillová	k1gFnSc7	krillová
(	(	kIx(	(
<g/>
Euphausia	Euphausia	k1gFnSc1	Euphausia
superba	superba	k1gMnSc1	superba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roje	roj	k1gInPc1	roj
těchto	tento	k3xDgFnPc2	tento
krunýřovek	krunýřovka	k1gFnPc2	krunýřovka
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
200	[number]	k4	200
m	m	kA	m
<g/>
,	,	kIx,	,
povětšinou	povětšinou	k6eAd1	povětšinou
za	za	k7c2	za
jasného	jasný	k2eAgNnSc2d1	jasné
světla	světlo	k1gNnSc2	světlo
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
přiživí	přiživit	k5eAaPmIp3nP	přiživit
na	na	k7c6	na
menších	malý	k2eAgFnPc6d2	menší
rybách	ryba	k1gFnPc6	ryba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
druhu	druh	k1gInSc2	druh
Pagothenia	Pagothenium	k1gNnSc2	Pagothenium
borchgrevinki	borchgrevink	k1gFnSc2	borchgrevink
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ledovkovití	ledovkovitý	k2eAgMnPc1d1	ledovkovitý
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
rovněž	rovněž	k9	rovněž
nemrznoucí	mrznoucí	k2eNgInPc4d1	nemrznoucí
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
loví	lovit	k5eAaImIp3nP	lovit
především	především	k9	především
pod	pod	k7c7	pod
mořským	mořský	k2eAgInSc7d1	mořský
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sledování	sledování	k1gNnSc1	sledování
miniaturními	miniaturní	k2eAgFnPc7d1	miniaturní
kamerami	kamera	k1gFnPc7	kamera
dokáže	dokázat	k5eAaPmIp3nS	dokázat
za	za	k7c2	za
1,5	[number]	k4	1,5
hodiny	hodina	k1gFnSc2	hodina
ulovit	ulovit	k5eAaPmF	ulovit
244	[number]	k4	244
korýšů	korýš	k1gMnPc2	korýš
nebo	nebo	k8xC	nebo
33	[number]	k4	33
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
období	období	k1gNnSc2	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
km	km	kA	km
od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
dostupnosti	dostupnost	k1gFnSc6	dostupnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
zvyšujícím	zvyšující	k2eAgFnPc3d1	zvyšující
letním	letní	k2eAgFnPc3d1	letní
teplotám	teplota	k1gFnPc3	teplota
ledové	ledový	k2eAgFnSc2d1	ledová
pole	pole	k1gFnSc2	pole
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
a	a	k8xC	a
krmení	krmení	k1gNnSc1	krmení
již	již	k6eAd1	již
vylíhlých	vylíhlý	k2eAgNnPc2d1	vylíhlé
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
tučňáků	tučňák	k1gMnPc2	tučňák
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
bez	bez	k7c2	bez
ledové	ledový	k2eAgFnSc2d1	ledová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
mládě	mládě	k1gNnSc1	mládě
nakrmit	nakrmit	k5eAaPmF	nakrmit
<g/>
,	,	kIx,	,
pobývá	pobývat	k5eAaImIp3nS	pobývat
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
tah	tah	k1gInSc4	tah
uloví	ulovit	k5eAaPmIp3nS	ulovit
obvykle	obvykle	k6eAd1	obvykle
320	[number]	k4	320
<g/>
–	–	k?	–
<g/>
490	[number]	k4	490
g	g	kA	g
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
<g/>
Průměrně	průměrně	k6eAd1	průměrně
plave	plavat	k5eAaImIp3nS	plavat
tempem	tempo	k1gNnSc7	tempo
2,2	[number]	k4	2,2
<g/>
–	–	k?	–
<g/>
8,2	[number]	k4	8,2
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
blížící	blížící	k2eAgFnSc4d1	blížící
se	s	k7c7	s
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
nebo	nebo	k8xC	nebo
při	při	k7c6	při
úprku	úprk	k1gInSc6	úprk
před	před	k7c7	před
predátory	predátor	k1gMnPc7	predátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
patra	patro	k1gNnSc2	patro
zobáku	zobák	k1gInSc2	zobák
má	mít	k5eAaImIp3nS	mít
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
opatřený	opatřený	k2eAgInSc1d1	opatřený
zpětně	zpětně	k6eAd1	zpětně
orientovanými	orientovaný	k2eAgInPc7d1	orientovaný
háčky	háček	k1gInPc7	háček
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
lapené	lapený	k2eAgFnSc3d1	lapená
kořisti	kořist	k1gFnSc3	kořist
únik	únik	k1gInSc4	únik
<g/>
.	.	kIx.	.
</s>
<s>
Ulovená	ulovený	k2eAgFnSc1d1	ulovená
potrava	potrava	k1gFnSc1	potrava
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
ihned	ihned	k6eAd1	ihned
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ryby	ryba	k1gFnPc1	ryba
jsou	být	k5eAaImIp3nP	být
pojídány	pojídán	k2eAgFnPc1d1	pojídána
hlavou	hlava	k1gFnSc7	hlava
napřed	napřed	k6eAd1	napřed
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
jejich	jejich	k3xOp3gFnSc1	jejich
ploutve	ploutev	k1gFnPc1	ploutev
neuvíznou	uvíznout	k5eNaPmIp3nP	uvíznout
v	v	k7c6	v
jícnu	jícen	k1gInSc6	jícen
(	(	kIx(	(
<g/>
uzavírají	uzavírat	k5eAaPmIp3nP	uzavírat
se	se	k3xPyFc4	se
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
kořisti	kořist	k1gFnSc2	kořist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
kořist	kořist	k1gFnSc1	kořist
usmrcena	usmrcen	k2eAgFnSc1d1	usmrcena
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
živá	živá	k1gFnSc1	živá
následně	následně	k6eAd1	následně
udusí	udusit	k5eAaPmIp3nS	udusit
a	a	k8xC	a
rozloží	rozložit	k5eAaPmIp3nS	rozložit
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přirození	přirozený	k2eAgMnPc1d1	přirozený
predátoři	predátor	k1gMnPc1	predátor
==	==	k?	==
</s>
</p>
<p>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pobývá	pobývat	k5eAaImIp3nS	pobývat
nejvíce	hodně	k6eAd3	hodně
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgInSc1d1	kroužkový
zdrojem	zdroj	k1gInSc7	zdroj
potravy	potrava	k1gFnSc2	potrava
pro	pro	k7c4	pro
tuleně	tuleň	k1gMnPc4	tuleň
leopardí	leopardí	k2eAgNnSc1d1	leopardí
(	(	kIx(	(
<g/>
Hydrurga	Hydrurga	k1gFnSc1	Hydrurga
leptonyx	leptonyx	k1gInSc1	leptonyx
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kosatky	kosatka	k1gFnPc1	kosatka
dravé	dravý	k2eAgFnPc1d1	dravá
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuleni	tuleň	k1gMnPc1	tuleň
často	často	k6eAd1	často
na	na	k7c4	na
tučňáky	tučňák	k1gMnPc4	tučňák
číhají	číhat	k5eAaImIp3nP	číhat
u	u	k7c2	u
okraje	okraj	k1gInSc2	okraj
ledové	ledový	k2eAgFnSc2d1	ledová
plochy	plocha	k1gFnSc2	plocha
poblíž	poblíž	k7c2	poblíž
jejich	jejich	k3xOp3gFnPc2	jejich
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
překvapili	překvapit	k5eAaPmAgMnP	překvapit
ptáky	pták	k1gMnPc4	pták
mířící	mířící	k2eAgMnPc4d1	mířící
na	na	k7c4	na
lov	lov	k1gInSc4	lov
či	či	k8xC	či
vracející	vracející	k2eAgNnSc4d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
lovu	lov	k1gInSc2	lov
<g/>
;	;	kIx,	;
břehy	břeh	k1gInPc1	břeh
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
plní	plnit	k5eAaImIp3nS	plnit
skupinkami	skupinka	k1gFnPc7	skupinka
rozrušených	rozrušený	k2eAgMnPc2d1	rozrušený
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zdráhají	zdráhat	k5eAaImIp3nP	zdráhat
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
nepomine	pominout	k5eNaPmIp3nS	pominout
<g/>
.	.	kIx.	.
</s>
<s>
Tuleni	tuleň	k1gMnPc1	tuleň
ale	ale	k9	ale
umí	umět	k5eAaImIp3nP	umět
tenký	tenký	k2eAgInSc4d1	tenký
led	led	k1gInSc4	led
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
úderem	úder	k1gInSc7	úder
rozlomit	rozlomit	k5eAaPmF	rozlomit
a	a	k8xC	a
shodit	shodit	k5eAaPmF	shodit
tak	tak	k9	tak
tučňáky	tučňák	k1gMnPc4	tučňák
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
Vejce	vejce	k1gNnPc1	vejce
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
ohrožována	ohrožován	k2eAgFnSc1d1	ohrožována
velkými	velký	k2eAgMnPc7d1	velký
buřňáky	buřňák	k1gMnPc7	buřňák
<g/>
,	,	kIx,	,
chaluhami	chaluha	k1gFnPc7	chaluha
<g/>
,	,	kIx,	,
racky	racek	k1gMnPc4	racek
a	a	k8xC	a
štítonosy	štítonosa	k1gFnPc4	štítonosa
světlezobými	světlezobý	k2eAgNnPc7d1	světlezobý
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
chaluhy	chaluha	k1gFnSc2	chaluha
antarktické	antarktický	k2eAgFnSc2d1	antarktická
bývají	bývat	k5eAaImIp3nP	bývat
kolikrát	kolikrát	k6eAd1	kolikrát
vynalézavé	vynalézavý	k2eAgFnPc1d1	vynalézavá
a	a	k8xC	a
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
páru	pár	k1gInSc6	pár
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
odláká	odlákat	k5eAaPmIp3nS	odlákat
rodiče	rodič	k1gMnPc1	rodič
bránícího	bránící	k2eAgInSc2d1	bránící
hnízdo	hnízdo	k1gNnSc1	hnízdo
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
mezitím	mezitím	k6eAd1	mezitím
vejce	vejce	k1gNnSc1	vejce
či	či	k8xC	či
mládě	mládě	k1gNnSc1	mládě
sebere	sebrat	k5eAaPmIp3nS	sebrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
kolonii	kolonie	k1gFnSc6	kolonie
spolu	spolu	k6eAd1	spolu
v	v	k7c6	v
těsném	těsný	k2eAgInSc6d1	těsný
kontaktu	kontakt	k1gInSc6	kontakt
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
stovky	stovka	k1gFnPc1	stovka
a	a	k8xC	a
tisíce	tisíc	k4xCgInSc2	tisíc
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vůči	vůči	k7c3	vůči
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
tučňáků	tučňák	k1gMnPc2	tučňák
poměrně	poměrně	k6eAd1	poměrně
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
tříměsíční	tříměsíční	k2eAgNnPc4d1	tříměsíční
rozmnožování	rozmnožování	k1gNnPc4	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
života	život	k1gInSc2	život
tak	tak	k6eAd1	tak
tráví	trávit	k5eAaImIp3nP	trávit
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
až	až	k9	až
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
monogamní	monogamní	k2eAgNnSc1d1	monogamní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
dny	den	k1gInPc4	den
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
dorazí	dorazit	k5eAaPmIp3nS	dorazit
samec	samec	k1gInSc1	samec
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
svou	svůj	k3xOyFgFnSc7	svůj
nepřítomností	nepřítomnost	k1gFnSc7	nepřítomnost
ochráněna	ochráněn	k2eAgFnSc1d1	ochráněna
proti	proti	k7c3	proti
bojovému	bojový	k2eAgNnSc3d1	bojové
naladění	naladění	k1gNnSc3	naladění
svého	svůj	k1gMnSc2	svůj
druha	druh	k1gMnSc2	druh
a	a	k8xC	a
ostatních	ostatní	k2eAgMnPc2d1	ostatní
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
zarytého	zarytý	k2eAgNnSc2d1	zaryté
obhajování	obhajování	k1gNnSc2	obhajování
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ledě	led	k1gInSc6	led
<g/>
,	,	kIx,	,
pěší	pěší	k2eAgFnSc6d1	pěší
chůzí	chůze	k1gFnSc7	chůze
a	a	k8xC	a
klouzáním	klouzání	k1gNnSc7	klouzání
po	po	k7c6	po
břiše	břicho	k1gNnSc6	břicho
ujde	ujít	k5eAaPmIp3nS	ujít
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jedince	jedinec	k1gMnSc4	jedinec
hnízdícího	hnízdící	k2eAgMnSc4d1	hnízdící
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
blízko	blízko	k7c2	blízko
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
migrace	migrace	k1gFnSc1	migrace
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
starostí	starost	k1gFnSc7	starost
samce	samec	k1gInSc2	samec
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
upravit	upravit	k5eAaPmF	upravit
loňské	loňský	k2eAgNnSc4d1	loňské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
přenáší	přenášet	k5eAaImIp3nS	přenášet
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kamínky	kamínek	k1gInPc4	kamínek
a	a	k8xC	a
pokládá	pokládat	k5eAaImIp3nS	pokládat
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
vyhrabané	vyhrabaný	k2eAgFnSc2d1	vyhrabaná
jamky	jamka	k1gFnSc2	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
neobejde	obejde	k6eNd1	obejde
bez	bez	k7c2	bez
agresivního	agresivní	k2eAgNnSc2d1	agresivní
počínání	počínání	k1gNnSc2	počínání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tučňáci	tučňák	k1gMnPc1	tučňák
vzájemně	vzájemně	k6eAd1	vzájemně
o	o	k7c4	o
kamínky	kamínek	k1gInPc4	kamínek
okrádají	okrádat	k5eAaImIp3nP	okrádat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
kolonie	kolonie	k1gFnSc2	kolonie
jsou	být	k5eAaImIp3nP	být
hnízda	hnízdo	k1gNnPc4	hnízdo
viditelně	viditelně	k6eAd1	viditelně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
hnízda	hnízdo	k1gNnSc2	hnízdo
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
samotnému	samotný	k2eAgInSc3d1	samotný
středu	střed	k1gInSc3	střed
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zkušenějšího	zkušený	k2eAgMnSc4d2	zkušenější
samce	samec	k1gMnSc4	samec
<g/>
.	.	kIx.	.
</s>
<s>
Ostřílený	ostřílený	k2eAgInSc1d1	ostřílený
samec	samec	k1gInSc1	samec
o	o	k7c4	o
kamínky	kamínek	k1gInPc4	kamínek
lehce	lehko	k6eAd1	lehko
okrade	okrást	k5eAaPmIp3nS	okrást
mladého	mladý	k2eAgMnSc4d1	mladý
a	a	k8xC	a
nepozorného	pozorný	k2eNgMnSc4d1	nepozorný
soka	sok	k1gMnSc4	sok
z	z	k7c2	z
okrajové	okrajový	k2eAgFnSc2d1	okrajová
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Námluvy	námluva	k1gFnSc2	námluva
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
dostaví	dostavit	k5eAaPmIp3nS	dostavit
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
naplní	naplnit	k5eAaPmIp3nS	naplnit
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
kolonie	kolonie	k1gFnSc1	kolonie
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
hlukem	hluk	k1gInSc7	hluk
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
přeruší	přerušit	k5eAaPmIp3nP	přerušit
veškerou	veškerý	k3xTgFnSc4	veškerý
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
plně	plně	k6eAd1	plně
jim	on	k3xPp3gMnPc3	on
naslouchají	naslouchat	k5eAaImIp3nP	naslouchat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
upoutání	upoutání	k1gNnSc3	upoutání
samiček	samička	k1gFnPc2	samička
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
svá	svůj	k3xOyFgNnPc4	svůj
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
vychloubají	vychloubat	k5eAaImIp3nP	vychloubat
<g/>
.	.	kIx.	.
</s>
<s>
Postaví	postavit	k5eAaPmIp3nS	postavit
se	se	k3xPyFc4	se
u	u	k7c2	u
jejich	jejich	k3xOp3gInPc2	jejich
okrajů	okraj	k1gInPc2	okraj
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
vydávat	vydávat	k5eAaPmF	vydávat
hluboké	hluboký	k2eAgInPc4d1	hluboký
hrdelní	hrdelní	k2eAgInPc4d1	hrdelní
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Hlavu	hlava	k1gFnSc4	hlava
mají	mít	k5eAaImIp3nP	mít
vztyčenou	vztyčený	k2eAgFnSc4d1	vztyčená
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
třepetají	třepetat	k5eAaImIp3nP	třepetat
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
rychlé	rychlý	k2eAgNnSc1d1	rychlé
mávání	mávání	k1gNnSc1	mávání
křídly	křídlo	k1gNnPc7	křídlo
předznamená	předznamenat	k5eAaPmIp3nS	předznamenat
vyvrcholený	vyvrcholený	k2eAgInSc1d1	vyvrcholený
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
pleskají	pleskat	k5eAaImIp3nP	pleskat
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
obvykle	obvykle	k6eAd1	obvykle
dělají	dělat	k5eAaImIp3nP	dělat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
dorazí	dorazit	k5eAaPmIp3nS	dorazit
jejich	jejich	k3xOp3gFnSc1	jejich
loňská	loňský	k2eAgFnSc1d1	loňská
družka	družka	k1gFnSc1	družka
nebo	nebo	k8xC	nebo
dokud	dokud	k6eAd1	dokud
neupoutají	upoutat	k5eNaPmIp3nP	upoutat
volnou	volný	k2eAgFnSc4d1	volná
samici	samice	k1gFnSc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohledání	dohledání	k1gNnSc6	dohledání
se	se	k3xPyFc4	se
u	u	k7c2	u
společného	společný	k2eAgNnSc2d1	společné
hnízda	hnízdo	k1gNnSc2	hnízdo
oddávají	oddávat	k5eAaImIp3nP	oddávat
pozdravům	pozdrav	k1gInPc3	pozdrav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
společným	společný	k2eAgNnSc7d1	společné
ukláněním	uklánění	k1gNnSc7	uklánění
a	a	k8xC	a
troubením	troubení	k1gNnSc7	troubení
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
chování	chování	k1gNnSc1	chování
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
napadení	napadení	k1gNnPc4	napadení
obou	dva	k4xCgMnPc2	dva
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
opakují	opakovat	k5eAaImIp3nP	opakovat
při	při	k7c6	při
každém	každý	k3xTgNnSc6	každý
přiblížení	přiblížení	k1gNnSc6	přiblížení
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
načepýří	načepýřit	k5eAaPmIp3nS	načepýřit
jejich	jejich	k3xOp3gFnSc1	jejich
hlava	hlava	k1gFnSc1	hlava
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
hřebene	hřeben	k1gInSc2	hřeben
a	a	k8xC	a
oči	oko	k1gNnPc4	oko
rozjasní	rozjasnit	k5eAaPmIp3nS	rozjasnit
až	až	k9	až
vyboulí	vyboulit	k5eAaPmIp3nS	vyboulit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Páření	páření	k1gNnSc2	páření
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
námluvách	námluva	k1gFnPc6	námluva
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Kopulace	kopulace	k1gFnSc1	kopulace
je	být	k5eAaImIp3nS	být
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
probíhá	probíhat	k5eAaImIp3nS	probíhat
opakovaně	opakovaně	k6eAd1	opakovaně
–	–	k?	–
u	u	k7c2	u
mladších	mladý	k2eAgInPc2d2	mladší
méně	málo	k6eAd2	málo
zkušených	zkušený	k2eAgInPc2d1	zkušený
párů	pár	k1gInPc2	pár
zpravidla	zpravidla	k6eAd1	zpravidla
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
naskočí	naskočit	k5eAaPmIp3nS	naskočit
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
samičky	samička	k1gFnSc2	samička
<g/>
,	,	kIx,	,
vyzdvihne	vyzdvihnout	k5eAaPmIp3nS	vyzdvihnout
ocas	ocas	k1gInSc1	ocas
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
rovnováhu	rovnováha	k1gFnSc4	rovnováha
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
svou	svůj	k3xOyFgFnSc4	svůj
kloaku	kloaka	k1gFnSc4	kloaka
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
stačí	stačit	k5eAaBmIp3nS	stačit
pouhé	pouhý	k2eAgNnSc4d1	pouhé
přitisknutí	přitisknutí	k1gNnSc4	přitisknutí
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
pár	pár	k4xCyI	pár
varlat	varle	k1gNnPc2	varle
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
se	se	k3xPyFc4	se
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
tření	tření	k1gNnSc6	tření
se	se	k3xPyFc4	se
do	do	k7c2	do
samice	samice	k1gFnSc2	samice
dostane	dostat	k5eAaPmIp3nS	dostat
sperma	sperma	k1gNnSc4	sperma
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
cestuje	cestovat	k5eAaImIp3nS	cestovat
do	do	k7c2	do
skladovacích	skladovací	k2eAgInPc2d1	skladovací
tubulů	tubulus	k1gInPc2	tubulus
<g/>
.	.	kIx.	.
</s>
<s>
Spermie	spermie	k1gFnSc1	spermie
oplodní	oplodnit	k5eAaPmIp3nS	oplodnit
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
posléze	posléze	k6eAd1	posléze
ve	v	k7c6	v
vejcovodu	vejcovod	k1gInSc6	vejcovod
tvrdnou	tvrdnout	k5eAaImIp3nP	tvrdnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Snesení	snesení	k1gNnSc2	snesení
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
sezení	sezení	k1gNnPc2	sezení
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
položí	položit	k5eAaPmIp3nS	položit
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
dnů	den	k1gInPc2	den
zpravidla	zpravidla	k6eAd1	zpravidla
dvě	dva	k4xCgNnPc4	dva
nazelenalá	nazelenalý	k2eAgNnPc4d1	nazelenalé
vejce	vejce	k1gNnPc4	vejce
do	do	k7c2	do
hnízdní	hnízdní	k2eAgFnSc2d1	hnízdní
kotlinky	kotlinka	k1gFnSc2	kotlinka
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
dní	den	k1gInPc2	den
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
bývá	bývat	k5eAaImIp3nS	bývat
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
těžší	těžký	k2eAgNnSc1d2	těžší
než	než	k8xS	než
druhé	druhý	k4xOgNnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
asi	asi	k9	asi
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Sedět	sedět	k5eAaImF	sedět
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
začíná	začínat	k5eAaImIp3nS	začínat
samec	samec	k1gInSc1	samec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vydržet	vydržet	k5eAaPmF	vydržet
bez	bez	k7c2	bez
potravy	potrava	k1gFnSc2	potrava
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
dní	den	k1gInPc2	den
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
živí	živit	k5eAaImIp3nP	živit
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
samec	samec	k1gMnSc1	samec
ztratí	ztratit	k5eAaPmIp3nS	ztratit
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
samice	samice	k1gFnSc1	samice
hladoví	hladovět	k5eAaImIp3nS	hladovět
kratší	krátký	k2eAgFnSc4d2	kratší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
zhubne	zhubnout	k5eAaPmIp3nS	zhubnout
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
(	(	kIx(	(
<g/>
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
spoustu	spousta	k1gFnSc4	spousta
energie	energie	k1gFnSc2	energie
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
vajec	vejce	k1gNnPc2	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
rodiči	rodič	k1gMnPc7	rodič
zahřívána	zahříván	k2eAgMnSc4d1	zahříván
v	v	k7c6	v
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
hnízdní	hnízdní	k2eAgFnSc6d1	hnízdní
nažině	nažina	k1gFnSc6	nažina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
bezprostřednímu	bezprostřední	k2eAgInSc3d1	bezprostřední
kontaktu	kontakt	k1gInSc3	kontakt
se	s	k7c7	s
silně	silně	k6eAd1	silně
prokrvenou	prokrvený	k2eAgFnSc7d1	prokrvená
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
jsou	být	k5eAaImIp3nP	být
vejce	vejce	k1gNnPc1	vejce
zahřívána	zahříván	k2eAgNnPc1d1	zahříváno
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dnech	den	k1gInPc6	den
lze	lze	k6eAd1	lze
na	na	k7c6	na
rodičích	rodič	k1gMnPc6	rodič
dobře	dobře	k6eAd1	dobře
pozorovat	pozorovat	k5eAaImF	pozorovat
tuto	tento	k3xDgFnSc4	tento
prokrvenou	prokrvený	k2eAgFnSc4d1	prokrvená
spodní	spodní	k2eAgFnSc4d1	spodní
plochu	plocha	k1gFnSc4	plocha
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
otlačena	otlačit	k5eAaPmNgFnS	otlačit
od	od	k7c2	od
tvrdých	tvrdý	k2eAgNnPc2d1	tvrdé
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pár	pár	k4xCyI	pár
o	o	k7c4	o
snůšku	snůška	k1gFnSc4	snůška
okraden	okrást	k5eAaPmNgMnS	okrást
dravými	dravý	k2eAgMnPc7d1	dravý
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zárodky	zárodek	k1gInPc1	zárodek
nepřežijí	přežít	k5eNaPmIp3nP	přežít
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
náhradní	náhradní	k2eAgFnSc4d1	náhradní
snůšku	snůška	k1gFnSc4	snůška
neklade	klást	k5eNaImIp3nS	klást
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
nakrmí	nakrmit	k5eAaPmIp3nS	nakrmit
a	a	k8xC	a
potom	potom	k6eAd1	potom
znovu	znovu	k6eAd1	znovu
obsadí	obsadit	k5eAaPmIp3nP	obsadit
hnízdo	hnízdo	k1gNnSc4	hnízdo
a	a	k8xC	a
hladovějí	hladovět	k5eAaImIp3nP	hladovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Líhnutí	líhnutí	k1gNnSc2	líhnutí
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
chov	chov	k1gInSc4	chov
===	===	k?	===
</s>
</p>
<p>
<s>
Obě	dva	k4xCgNnPc1	dva
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
současně	současně	k6eAd1	současně
a	a	k8xC	a
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jsou	být	k5eAaImIp3nP	být
slepá	slepý	k2eAgNnPc1d1	slepé
a	a	k8xC	a
porostlá	porostlý	k2eAgNnPc1d1	porostlé
stříbřitým	stříbřitý	k2eAgNnPc3d1	stříbřité
nebo	nebo	k8xC	nebo
kouřově	kouřově	k6eAd1	kouřově
šedým	šedý	k2eAgNnSc7d1	šedé
prachovým	prachový	k2eAgNnSc7d1	prachové
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc4	rodič
je	být	k5eAaImIp3nS	být
krmí	krmě	k1gFnSc7	krmě
natrávenou	natrávený	k2eAgFnSc7d1	natrávená
kašovitou	kašovitý	k2eAgFnSc7d1	kašovitá
stravou	strava	k1gFnSc7	strava
z	z	k7c2	z
volete	vole	k1gNnSc2	vole
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
není	být	k5eNaImIp3nS	být
rodiči	rodič	k1gMnPc7	rodič
trávena	trávit	k5eAaImNgFnS	trávit
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
i	i	k9	i
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
zachovány	zachovat	k5eAaPmNgFnP	zachovat
výživové	výživový	k2eAgFnPc1d1	výživová
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
asi	asi	k9	asi
22	[number]	k4	22
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
střežena	stříct	k5eAaPmNgNnP	stříct
střídavě	střídavě	k6eAd1	střídavě
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
vždy	vždy	k6eAd1	vždy
hlídá	hlídat	k5eAaImIp3nS	hlídat
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
loví	lovit	k5eAaImIp3nS	lovit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
od	od	k7c2	od
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
<g/>
,	,	kIx,	,
asi	asi	k9	asi
počátkem	počátkem	k7c2	počátkem
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
přepeří	přepeřit	k5eAaPmIp3nP	přepeřit
do	do	k7c2	do
vyspělejšího	vyspělý	k2eAgInSc2d2	vyspělejší
šatu	šat	k1gInSc2	šat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
peří	peří	k1gNnSc1	peří
ještě	ještě	k6eAd1	ještě
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
přizpůsobeno	přizpůsobit	k5eAaPmNgNnS	přizpůsobit
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
odolné	odolný	k2eAgNnSc1d1	odolné
vůči	vůči	k7c3	vůči
vnějším	vnější	k2eAgFnPc3d1	vnější
teplotám	teplota	k1gFnPc3	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
si	se	k3xPyFc3	se
již	již	k6eAd1	již
regulovat	regulovat	k5eAaImF	regulovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
teplotu	teplota	k1gFnSc4	teplota
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
tak	tak	k9	tak
scházet	scházet	k5eAaImF	scházet
do	do	k7c2	do
školek	školka	k1gFnPc2	školka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
houf	houf	k1gInSc4	houf
stejně	stejně	k6eAd1	stejně
starých	starý	k2eAgMnPc2d1	starý
vrstevníků	vrstevník	k1gMnPc2	vrstevník
o	o	k7c4	o
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
choulí	choulet	k5eAaImIp3nS	choulet
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
se	se	k3xPyFc4	se
tak	tak	k9	tak
proti	proti	k7c3	proti
nízkým	nízký	k2eAgFnPc3d1	nízká
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
predátorům	predátor	k1gMnPc3	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nejsou	být	k5eNaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
nikým	nikdo	k3yNnSc7	nikdo
hlídáni	hlídat	k5eAaImNgMnP	hlídat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
nejkritičtější	kritický	k2eAgNnPc4d3	nejkritičtější
období	období	k1gNnPc4	období
(	(	kIx(	(
<g/>
teoreticky	teoreticky	k6eAd1	teoreticky
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mladiství	mladistvý	k2eAgMnPc1d1	mladistvý
jedinci	jedinec	k1gMnPc1	jedinec
staří	starý	k2eAgMnPc1d1	starý
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
dopřávají	dopřávat	k5eAaImIp3nP	dopřávat
pouze	pouze	k6eAd1	pouze
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnPc3	který
mláďata	mládě	k1gNnPc1	mládě
zahynula	zahynout	k5eAaPmAgFnS	zahynout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Školky	školka	k1gFnPc1	školka
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rodičům	rodič	k1gMnPc3	rodič
lovit	lovit	k5eAaImF	lovit
společně	společně	k6eAd1	společně
a	a	k8xC	a
síly	síla	k1gFnPc4	síla
tak	tak	k6eAd1	tak
podělit	podělit	k5eAaPmF	podělit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
mládě	mládě	k1gNnSc4	mládě
vyšší	vysoký	k2eAgInPc4d2	vyšší
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
přísun	přísun	k1gInSc4	přísun
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc4	mládě
tučňáka	tučňák	k1gMnSc2	tučňák
kroužkového	kroužkový	k2eAgMnSc2d1	kroužkový
mohou	moct	k5eAaImIp3nP	moct
naráz	naráz	k6eAd1	naráz
spořádat	spořádat	k5eAaPmF	spořádat
potravu	potrava	k1gFnSc4	potrava
o	o	k7c6	o
ekvivalentu	ekvivalent	k1gInSc6	ekvivalent
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Krmení	krmení	k1gNnSc1	krmení
mláďat	mládě	k1gNnPc2	mládě
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
zásadu	zásada	k1gFnSc4	zásada
–	–	k?	–
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodiče	rodič	k1gMnPc1	rodič
své	svůj	k3xOyFgFnPc4	svůj
různě	různě	k6eAd1	různě
se	se	k3xPyFc4	se
potulující	potulující	k2eAgMnPc4d1	potulující
potomky	potomek	k1gMnPc4	potomek
nutí	nutit	k5eAaImIp3nS	nutit
nejprve	nejprve	k6eAd1	nejprve
dostavit	dostavit	k5eAaPmF	dostavit
se	se	k3xPyFc4	se
k	k	k7c3	k
hnízdu	hnízdo	k1gNnSc3	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaPmF	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodiče	rodič	k1gMnPc1	rodič
odmítají	odmítat	k5eAaImIp3nP	odmítat
mládě	mládě	k1gNnSc4	mládě
krmit	krmit	k5eAaImF	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rodiče	rodič	k1gMnPc4	rodič
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
snadnější	snadný	k2eAgNnPc4d2	snazší
mláďata	mládě	k1gNnPc4	mládě
krmit	krmit	k5eAaImF	krmit
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
cizí	cizí	k2eAgNnPc1d1	cizí
mláďata	mládě	k1gNnPc1	mládě
neodváží	odvázat	k5eNaPmIp3nP	odvázat
zatoulat	zatoulat	k5eAaPmF	zatoulat
a	a	k8xC	a
krmení	krmení	k1gNnSc1	krmení
tak	tak	k6eAd1	tak
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
toto	tento	k3xDgNnSc4	tento
neobvyklé	obvyklý	k2eNgNnSc4d1	neobvyklé
chování	chování	k1gNnSc4	chování
mnohem	mnohem	k6eAd1	mnohem
hlubší	hluboký	k2eAgInSc1d2	hlubší
význam	význam	k1gInSc1	význam
<g/>
;	;	kIx,	;
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
údajně	údajně	k6eAd1	údajně
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
z	z	k7c2	z
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
zdatnější	zdatný	k2eAgMnPc1d2	zdatnější
a	a	k8xC	a
vytrvalejší	vytrvalý	k2eAgMnPc1d2	vytrvalejší
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
více	hodně	k6eAd2	hodně
žadoní	žadonit	k5eAaImIp3nS	žadonit
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
jistou	jistý	k2eAgFnSc4d1	jistá
vitalitu	vitalita	k1gFnSc4	vitalita
(	(	kIx(	(
<g/>
živost	živost	k1gFnSc4	živost
<g/>
)	)	kIx)	)
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
tak	tak	k6eAd1	tak
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
viditelně	viditelně	k6eAd1	viditelně
více	hodně	k6eAd2	hodně
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
zdařilý	zdařilý	k2eAgInSc4d1	zdařilý
odchov	odchov	k1gInSc4	odchov
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
takovému	takový	k3xDgNnSc3	takový
chování	chování	k1gNnSc3	chování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nedostatek	nedostatek	k1gInSc4	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
<g/>
Koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
mláďata	mládě	k1gNnPc4	mládě
již	již	k6eAd1	již
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgNnSc1d1	funkční
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nP	lišit
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
pigmentací	pigmentace	k1gFnSc7	pigmentace
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
terčem	terč	k1gInSc7	terč
agresivního	agresivní	k2eAgNnSc2d1	agresivní
jednání	jednání	k1gNnSc2	jednání
cizích	cizí	k2eAgMnPc2d1	cizí
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
letní	letní	k2eAgNnSc4d1	letní
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
ledový	ledový	k2eAgInSc4d1	ledový
pokryv	pokryv	k1gInSc4	pokryv
taje	taj	k1gInSc2	taj
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
mláďatům	mládě	k1gNnPc3	mládě
výrazně	výrazně	k6eAd1	výrazně
blíže	blízce	k6eAd2	blízce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
vrhnou	vrhnout	k5eAaImIp3nP	vrhnout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
zcela	zcela	k6eAd1	zcela
přepeří	přepeřit	k5eAaPmIp3nS	přepeřit
do	do	k7c2	do
dospělého	dospělý	k2eAgInSc2d1	dospělý
šatu	šat	k1gInSc2	šat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
ještě	ještě	k6eAd1	ještě
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
neopadanými	opadaný	k2eNgInPc7d1	opadaný
chomáčky	chomáček	k1gInPc7	chomáček
starého	starý	k2eAgNnSc2d1	staré
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
takové	takový	k3xDgNnSc4	takový
chování	chování	k1gNnSc4	chování
u	u	k7c2	u
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
chov	chov	k1gInSc1	chov
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
blízko	blízko	k7c2	blízko
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
dospělí	dospělí	k1gMnPc1	dospělí
rovněž	rovněž	k6eAd1	rovněž
vracejí	vracet	k5eAaImIp3nP	vracet
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Krmí	krmit	k5eAaImIp3nS	krmit
se	se	k3xPyFc4	se
asi	asi	k9	asi
10	[number]	k4	10
dní	den	k1gInPc2	den
a	a	k8xC	a
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
přepeřit	přepeřit	k5eAaPmF	přepeřit
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
línání	línání	k1gNnSc2	línání
trvá	trvat	k5eAaImIp3nS	trvat
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
<g/>
Začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
je	být	k5eAaImIp3nS	být
již	již	k9	již
hnízdiště	hnízdiště	k1gNnSc1	hnízdiště
zcela	zcela	k6eAd1	zcela
opuštěné	opuštěný	k2eAgNnSc1d1	opuštěné
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
nové	nový	k2eAgNnSc4d1	nové
období	období	k1gNnSc4	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
<g/>
Mladí	mladý	k2eAgMnPc1d1	mladý
tučňáci	tučňák	k1gMnPc1	tučňák
jsou	být	k5eAaImIp3nP	být
loajální	loajální	k2eAgInPc4d1	loajální
k	k	k7c3	k
lokalitě	lokalita	k1gFnSc3	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
sami	sám	k3xTgMnPc1	sám
odchováni	odchován	k2eAgMnPc1d1	odchován
<g/>
.	.	kIx.	.
</s>
<s>
Vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
totéž	týž	k3xTgNnSc4	týž
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
jsou	být	k5eAaImIp3nP	být
pohlavně	pohlavně	k6eAd1	pohlavně
vyzrálí	vyzrálý	k2eAgMnPc1d1	vyzrálý
od	od	k7c2	od
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
a	a	k8xC	a
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
pokusí	pokusit	k5eAaPmIp3nP	pokusit
o	o	k7c4	o
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
odchov	odchov	k1gInSc4	odchov
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studie	studie	k1gFnSc1	studie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Expedice	expedice	k1gFnSc1	expedice
"	"	kIx"	"
<g/>
Terra	Terra	k1gFnSc1	Terra
Nova	nova	k1gFnSc1	nova
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
britská	britský	k2eAgFnSc1d1	britská
výprava	výprava	k1gFnSc1	výprava
za	za	k7c7	za
dobytím	dobytí	k1gNnSc7	dobytí
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
kapitána	kapitán	k1gMnSc2	kapitán
Roberta	Robert	k1gMnSc2	Robert
Falcona	Falcon	k1gMnSc2	Falcon
Scotta	Scott	k1gMnSc2	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Výpravy	výprava	k1gFnPc1	výprava
se	se	k3xPyFc4	se
také	také	k9	také
účastnil	účastnit	k5eAaImAgInS	účastnit
George	George	k1gInSc1	George
Murray	Murraa	k1gFnSc2	Murraa
Levick	Levicka	k1gFnPc2	Levicka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
chirurg	chirurg	k1gMnSc1	chirurg
a	a	k8xC	a
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
tuto	tento	k3xDgFnSc4	tento
výpravu	výprava	k1gFnSc4	výprava
dokumentovat	dokumentovat	k5eAaBmF	dokumentovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
strávil	strávit	k5eAaPmAgInS	strávit
s	s	k7c7	s
tučňáky	tučňák	k1gMnPc7	tučňák
kroužkovými	kroužkový	k2eAgMnPc7d1	kroužkový
na	na	k7c6	na
mysu	mys	k1gInSc2	mys
Adare	Adar	k1gInSc5	Adar
celý	celý	k2eAgInSc4d1	celý
jejich	jejich	k3xOp3gInSc4	jejich
rozmnožovací	rozmnožovací	k2eAgInSc4d1	rozmnožovací
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
pozorování	pozorování	k1gNnPc4	pozorování
při	při	k7c6	při
námluvách	námluva	k1gFnPc6	námluva
<g/>
,	,	kIx,	,
páření	páření	k1gNnSc3	páření
a	a	k8xC	a
chovu	chov	k1gInSc3	chov
mláďat	mládě	k1gNnPc2	mládě
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
zvané	zvaný	k2eAgFnSc2d1	zvaná
Antarctic	Antarctice	k1gFnPc2	Antarctice
penguin	penguin	k2eAgMnSc1d1	penguin
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
stati	stať	k1gFnPc4	stať
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
tučňáků	tučňák	k1gMnPc2	tučňák
vyhodnotil	vyhodnotit	k5eAaPmAgMnS	vyhodnotit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
pro	pro	k7c4	pro
publikaci	publikace	k1gFnSc4	publikace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Levick	Levick	k6eAd1	Levick
ostře	ostro	k6eAd1	ostro
popisuje	popisovat	k5eAaImIp3nS	popisovat
dle	dle	k7c2	dle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
zvrhlé	zvrhlý	k2eAgNnSc1d1	zvrhlé
chování	chování	k1gNnSc1	chování
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
:	:	kIx,	:
známky	známka	k1gFnPc1	známka
nekrofilie	nekrofilie	k1gFnSc2	nekrofilie
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
páření	páření	k1gNnSc2	páření
s	s	k7c7	s
mrtvými	mrtvý	k2eAgFnPc7d1	mrtvá
samicemi	samice	k1gFnPc7	samice
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnPc1d1	sexuální
či	či	k8xC	či
fyzické	fyzický	k2eAgNnSc1d1	fyzické
zneužívání	zneužívání	k1gNnSc1	zneužívání
mláďat	mládě	k1gNnPc2	mládě
ba	ba	k9	ba
dokonce	dokonce	k9	dokonce
i	i	k9	i
sex	sex	k1gInSc4	sex
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
známky	známka	k1gFnPc4	známka
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
zápiskům	zápisek	k1gInPc3	zápisek
(	(	kIx(	(
<g/>
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
raději	rád	k6eAd2	rád
psal	psát	k5eAaImAgMnS	psát
řecky	řecky	k6eAd1	řecky
<g/>
)	)	kIx)	)
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
dal	dát	k5eAaPmAgMnS	dát
Levick	Levick	k1gMnSc1	Levick
alespoň	alespoň	k9	alespoň
formu	forma	k1gFnSc4	forma
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
a	a	k8xC	a
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
zmizela	zmizet	k5eAaPmAgFnS	zmizet
v	v	k7c6	v
archivech	archiv	k1gInPc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
studie	studie	k1gFnSc1	studie
poprvé	poprvé	k6eAd1	poprvé
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c4	v
časopis	časopis	k1gInSc4	časopis
<g/>
]	]	kIx)	]
<g/>
e	e	k0	e
Polar	Polara	k1gFnPc2	Polara
Record	Record	k1gInSc4	Record
a	a	k8xC	a
i	i	k9	i
po	po	k7c6	po
takové	takový	k3xDgFnSc6	takový
době	doba	k1gFnSc6	doba
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
značný	značný	k2eAgInSc4d1	značný
rozruch	rozruch	k1gInSc4	rozruch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studie	studie	k1gFnPc1	studie
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Ross	Rossa	k1gFnPc2	Rossa
<g/>
;	;	kIx,	;
Fion	Fion	k1gMnSc1	Fion
Hunter	Hunter	k1gMnSc1	Hunter
a	a	k8xC	a
Lloyd	Lloyd	k1gMnSc1	Lloyd
Spencer	Spencer	k1gMnSc1	Spencer
Davis	Davis	k1gFnSc1	Davis
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Pětiletá	pětiletý	k2eAgFnSc1d1	pětiletá
studie	studie	k1gFnSc1	studie
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Ross	Rossa	k1gFnPc2	Rossa
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Fiona	Fion	k1gMnSc2	Fion
Huntera	Hunter	k1gMnSc2	Hunter
a	a	k8xC	a
Lloyda	Lloyd	k1gMnSc2	Lloyd
Spencera	Spencera	k1gFnSc1	Spencera
Davise	Davise	k1gFnSc1	Davise
<g/>
,	,	kIx,	,
odkryla	odkrýt	k5eAaPmAgFnS	odkrýt
zajímavý	zajímavý	k2eAgInSc4d1	zajímavý
úkaz	úkaz	k1gInSc4	úkaz
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
tučňáků	tučňák	k1gMnPc2	tučňák
proklouzávaly	proklouzávat	k5eAaImAgFnP	proklouzávat
mezi	mezi	k7c7	mezi
nezadanými	zadaný	k2eNgMnPc7d1	nezadaný
samci	samec	k1gMnPc7	samec
a	a	k8xC	a
po	po	k7c6	po
páření	páření	k1gNnSc6	páření
si	se	k3xPyFc3	se
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
hnízda	hnízdo	k1gNnSc2	hnízdo
odnášely	odnášet	k5eAaImAgFnP	odnášet
kamínek	kamínek	k1gInSc4	kamínek
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
vylepšily	vylepšit	k5eAaPmAgFnP	vylepšit
své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
rituální	rituální	k2eAgInSc1d1	rituální
trik	trik	k1gInSc1	trik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kamínek	kamínek	k1gInSc1	kamínek
si	se	k3xPyFc3	se
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
odnesly	odnést	k5eAaPmAgFnP	odnést
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	on	k3xPp3gMnPc4	on
odborníky	odborník	k1gMnPc4	odborník
vysvětlováno	vysvětlován	k2eAgNnSc1d1	vysvětlováno
jako	jako	k8xC	jako
mylné	mylný	k2eAgNnSc1d1	mylné
gesto	gesto	k1gNnSc1	gesto
samce	samec	k1gInSc2	samec
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
samice	samice	k1gFnSc1	samice
vykonala	vykonat	k5eAaPmAgFnS	vykonat
pouze	pouze	k6eAd1	pouze
čistou	čistý	k2eAgFnSc4d1	čistá
krádež	krádež	k1gFnSc4	krádež
kamínku	kamínek	k1gInSc2	kamínek
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
tučňáků	tučňák	k1gMnPc2	tučňák
kroužkových	kroužkový	k2eAgMnPc2d1	kroužkový
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
hlediska	hledisko	k1gNnSc2	hledisko
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
zjistit	zjistit	k5eAaPmF	zjistit
odůvodnění	odůvodnění	k1gNnSc4	odůvodnění
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
pouze	pouze	k6eAd1	pouze
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
v	v	k7c6	v
získávání	získávání	k1gNnSc6	získávání
kamínků	kamínek	k1gInPc2	kamínek
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možným	možný	k2eAgMnSc7d1	možný
strůjcem	strůjce	k1gMnSc7	strůjce
takového	takový	k3xDgInSc2	takový
sklonu	sklon	k1gInSc2	sklon
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
vůbec	vůbec	k9	vůbec
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tak	tak	k9	tak
samice	samice	k1gFnPc1	samice
konají	konat	k5eAaImIp3nP	konat
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získaly	získat	k5eAaPmAgFnP	získat
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
agrese	agrese	k1gFnSc2	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
teorie	teorie	k1gFnSc1	teorie
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
samice	samice	k1gFnPc1	samice
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
potenciální	potenciální	k2eAgMnPc4d1	potenciální
partnery	partner	k1gMnPc4	partner
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
jejich	jejich	k3xOp3gMnPc2	jejich
stávající	stávající	k2eAgMnPc1d1	stávající
uhynuli	uhynout	k5eAaPmAgMnP	uhynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Světová	světový	k2eAgFnSc1d1	světová
populace	populace	k1gFnSc1	populace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
vědci	vědec	k1gMnPc1	vědec
celkový	celkový	k2eAgInSc4d1	celkový
počet	počet	k1gInSc4	počet
jedinců	jedinec	k1gMnPc2	jedinec
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
statistiky	statistika	k1gFnPc4	statistika
zaměřující	zaměřující	k2eAgFnPc4d1	zaměřující
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
na	na	k7c4	na
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
bioregiony	bioregion	k1gInPc4	bioregion
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jen	jen	k9	jen
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
nepřesný	přesný	k2eNgInSc4d1	nepřesný
odhad	odhad	k1gInSc4	odhad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
nových	nový	k2eAgInPc2d1	nový
pozemních	pozemní	k2eAgInPc2d1	pozemní
a	a	k8xC	a
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
průzkumů	průzkum	k1gInPc2	průzkum
<g/>
,	,	kIx,	,
či	či	k8xC	či
získaných	získaný	k2eAgNnPc2d1	získané
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
automatických	automatický	k2eAgFnPc2d1	automatická
kamer	kamera	k1gFnPc2	kamera
odbornicí	odbornice	k1gFnSc7	odbornice
usuzují	usuzovat	k5eAaImIp3nP	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
na	na	k7c4	na
12	[number]	k4	12
až	až	k9	až
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
jedinců	jedinec	k1gMnPc2	jedinec
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
kolonie	kolonie	k1gFnSc1	kolonie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Rossova	Rossův	k2eAgNnSc2d1	Rossovo
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Crozier	Crozira	k1gFnPc2	Crozira
a	a	k8xC	a
mysu	mys	k1gInSc2	mys
Adare	Adar	k1gMnSc5	Adar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Faktory	faktor	k1gInPc4	faktor
a	a	k8xC	a
možné	možný	k2eAgFnPc4d1	možná
hrozby	hrozba	k1gFnPc4	hrozba
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
počítačových	počítačový	k2eAgInPc2d1	počítačový
modelů	model	k1gInPc2	model
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
může	moct	k5eAaImIp3nS	moct
populace	populace	k1gFnSc1	populace
tučňáka	tučňák	k1gMnSc2	tučňák
kroužkového	kroužkový	k2eAgMnSc2d1	kroužkový
významně	významně	k6eAd1	významně
klesnout	klesnout	k5eAaPmF	klesnout
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
troposféry	troposféra	k1gFnSc2	troposféra
o	o	k7c4	o
+2	+2	k4	+2
°	°	k?	°
<g/>
C.	C.	kA	C.
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
údajně	údajně	k6eAd1	údajně
nastat	nastat	k5eAaPmF	nastat
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
2025	[number]	k4	2025
až	až	k9	až
2052	[number]	k4	2052
–	–	k?	–
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
soubory	soubor	k1gInPc1	soubor
modelů	model	k1gInPc2	model
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
dosavadním	dosavadní	k2eAgFnPc3d1	dosavadní
změnám	změna	k1gFnPc3	změna
klimatu	klima	k1gNnSc2	klima
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
modelovány	modelován	k2eAgInPc1d1	modelován
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
rozsah	rozsah	k1gInSc4	rozsah
a	a	k8xC	a
ubývání	ubývání	k1gNnSc4	ubývání
ledové	ledový	k2eAgFnSc2d1	ledová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
,	,	kIx,	,
koncentrace	koncentrace	k1gFnSc1	koncentrace
a	a	k8xC	a
tloušťka	tloušťka	k1gFnSc1	tloušťka
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
či	či	k8xC	či
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
přežil	přežít	k5eAaPmAgMnS	přežít
už	už	k6eAd1	už
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
skrze	skrze	k?	skrze
různé	různý	k2eAgFnSc2d1	různá
proměny	proměna	k1gFnSc2	proměna
biotopu	biotop	k1gInSc2	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
svého	svůj	k3xOyFgNnSc2	svůj
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
dokáže	dokázat	k5eAaPmIp3nS	dokázat
včasně	včasně	k6eAd1	včasně
adaptovat	adaptovat	k5eAaBmF	adaptovat
a	a	k8xC	a
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dokázal	dokázat	k5eAaPmAgMnS	dokázat
případným	případný	k2eAgNnPc3d1	případné
změnám	změna	k1gFnPc3	změna
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prozatím	prozatím	k6eAd1	prozatím
vědci	vědec	k1gMnPc1	vědec
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
populační	populační	k2eAgInSc4d1	populační
růst	růst	k1gInSc4	růst
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
známých	známý	k2eAgFnPc2d1	známá
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
kolonie	kolonie	k1gFnPc1	kolonie
úplně	úplně	k6eAd1	úplně
nové	nový	k2eAgFnPc1d1	nová
<g/>
,	,	kIx,	,
obnovují	obnovovat	k5eAaImIp3nP	obnovovat
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
zaniklé	zaniklý	k2eAgFnPc1d1	zaniklá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Antarktidě	Antarktida	k1gFnSc6	Antarktida
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
populace	populace	k1gFnSc1	populace
tučňáka	tučňák	k1gMnSc2	tučňák
kroužkového	kroužkový	k2eAgMnSc2d1	kroužkový
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
30	[number]	k4	30
let	léto	k1gNnPc2	léto
o	o	k7c4	o
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
27	[number]	k4	27
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgFnPc4d1	ohrožená
kolonie	kolonie	k1gFnPc4	kolonie
se	se	k3xPyFc4	se
však	však	k9	však
řadí	řadit	k5eAaImIp3nP	řadit
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Antarktického	antarktický	k2eAgInSc2d1	antarktický
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
souvislé	souvislý	k2eAgFnSc6d1	souvislá
vrstvě	vrstva	k1gFnSc6	vrstva
pevného	pevný	k2eAgInSc2d1	pevný
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
však	však	k9	však
k	k	k7c3	k
patrnému	patrný	k2eAgNnSc3d1	patrné
tání	tání	k1gNnSc3	tání
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
stěhování	stěhování	k1gNnSc3	stěhování
potravních	potravní	k2eAgInPc2d1	potravní
zdrojů	zdroj	k1gInPc2	zdroj
vlivem	vlivem	k7c2	vlivem
oteplování	oteplování	k1gNnSc2	oteplování
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
<g/>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zprávy	zpráva	k1gFnPc1	zpráva
spíše	spíše	k9	spíše
uspokojivé	uspokojivý	k2eAgFnPc1d1	uspokojivá
<g/>
,	,	kIx,	,
nerozptylují	rozptylovat	k5eNaImIp3nP	rozptylovat
obavy	obava	k1gFnPc1	obava
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
jeho	on	k3xPp3gNnSc2	on
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
rovněž	rovněž	k9	rovněž
hrozby	hrozba	k1gFnPc4	hrozba
mající	mající	k2eAgInSc4d1	mající
lidský	lidský	k2eAgInSc4d1	lidský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
pro	pro	k7c4	pro
tučňáka	tučňák	k1gMnSc4	tučňák
kroužkového	kroužkový	k2eAgInSc2d1	kroužkový
je	být	k5eAaImIp3nS	být
neregulovaný	regulovaný	k2eNgInSc1d1	neregulovaný
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
tak	tak	k9	tak
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
také	také	k9	také
uvízne	uvíznout	k5eAaPmIp3nS	uvíznout
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
rybářské	rybářský	k2eAgFnSc6d1	rybářská
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
velkou	velký	k2eAgFnSc4d1	velká
vodní	vodní	k2eAgFnSc4d1	vodní
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
<g/>
Znepokojující	znepokojující	k2eAgInSc1d1	znepokojující
je	být	k5eAaImIp3nS	být
také	také	k9	také
narůstající	narůstající	k2eAgInSc1d1	narůstající
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgNnSc1d1	spojené
narušování	narušování	k1gNnSc1	narušování
hnízdních	hnízdní	k2eAgFnPc2d1	hnízdní
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
dbáno	dbán	k2eAgNnSc1d1	dbáno
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
zdejší	zdejší	k2eAgFnSc2d1	zdejší
netknuté	tknutý	k2eNgFnSc2d1	netknutá
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
narušován	narušován	k2eAgInSc1d1	narušován
její	její	k3xOp3gInSc1	její
chod	chod	k1gInSc1	chod
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
zásahů	zásah	k1gInPc2	zásah
vědců	vědec	k1gMnPc2	vědec
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stupňující	stupňující	k2eAgInSc4d1	stupňující
se	se	k3xPyFc4	se
turismus	turismus	k1gInSc4	turismus
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změny	změna	k1gFnPc4	změna
v	v	k7c4	v
chování	chování	k1gNnSc4	chování
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
přítomnost	přítomnost	k1gFnSc1	přítomnost
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
lidí	člověk	k1gMnPc2	člověk
narušuje	narušovat	k5eAaImIp3nS	narušovat
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
klid	klid	k1gInSc4	klid
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
při	při	k7c6	při
hnízdění	hnízdění	k1gNnSc6	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
přímé	přímý	k2eAgNnSc4d1	přímé
ohrožení	ohrožení	k1gNnSc4	ohrožení
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnSc2	některý
kolonie	kolonie	k1gFnSc2	kolonie
tučňáka	tučňák	k1gMnSc2	tučňák
kroužkového	kroužkový	k2eAgMnSc2d1	kroužkový
si	se	k3xPyFc3	se
třeba	třeba	k6eAd1	třeba
natolik	natolik	k6eAd1	natolik
osvojily	osvojit	k5eAaPmAgFnP	osvojit
výzkumné	výzkumný	k2eAgFnPc1d1	výzkumná
stanice	stanice	k1gFnPc1	stanice
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
<g/>
.	.	kIx.	.
<g/>
Samotný	samotný	k2eAgInSc1d1	samotný
antarktický	antarktický	k2eAgInSc1d1	antarktický
kontinent	kontinent	k1gInSc1	kontinent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tučňáci	tučňák	k1gMnPc1	tučňák
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
<g/>
,	,	kIx,	,
skrývá	skrývat	k5eAaImIp3nS	skrývat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
nerostné	nerostný	k2eAgNnSc4d1	nerostné
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
aktivita	aktivita	k1gFnSc1	aktivita
by	by	kYmCp3nS	by
ale	ale	k9	ale
mohla	moct	k5eAaImAgFnS	moct
zdejší	zdejší	k2eAgFnSc4d1	zdejší
biodiverzitu	biodiverzita	k1gFnSc4	biodiverzita
nezvratně	zvratně	k6eNd1	zvratně
narušit	narušit	k5eAaPmF	narušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vědecky	vědecky	k6eAd1	vědecky
zkoumaným	zkoumaný	k2eAgInSc7d1	zkoumaný
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
několika	několik	k4yIc6	několik
přírodovědných	přírodovědný	k2eAgInPc6d1	přírodovědný
dokumentech	dokument	k1gInPc6	dokument
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
města	město	k1gNnSc2	město
tučňáků	tučňák	k1gMnPc2	tučňák
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Za	za	k7c4	za
tučňáky	tučňák	k1gMnPc4	tučňák
na	na	k7c4	na
Antarktidu	Antarktida	k1gFnSc4	Antarktida
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neklid	neklid	k1gInSc1	neklid
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
Hrají	hrát	k5eAaImIp3nP	hrát
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
animovaných	animovaný	k2eAgInPc6d1	animovaný
filmech	film	k1gInPc6	film
Happy	Happa	k1gFnSc2	Happa
Feet	Feet	k1gInSc4	Feet
a	a	k8xC	a
Happy	Happ	k1gInPc4	Happ
Feet	Feet	k1gInSc1	Feet
2	[number]	k4	2
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
banda	banda	k1gFnSc1	banda
"	"	kIx"	"
<g/>
kompaňéros	kompaňérosa	k1gFnPc2	kompaňérosa
<g/>
"	"	kIx"	"
–	–	k?	–
Ramon	Ramona	k1gFnPc2	Ramona
<g/>
,	,	kIx,	,
Nestor	Nestor	k1gMnSc1	Nestor
<g/>
,	,	kIx,	,
Raul	Raul	k1gMnSc1	Raul
a	a	k8xC	a
Lombardo	Lombardo	k1gMnSc1	Lombardo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňáků	tučňák	k1gMnPc2	tučňák
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
dva	dva	k4xCgInPc4	dva
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
Den	den	k1gInSc1	den
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6	o
tučňácích	tučňák	k1gMnPc6	tučňák
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
jedním	jeden	k4xCgInSc7	jeden
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
i	i	k9	i
ochrana	ochrana	k1gFnSc1	ochrana
jejich	jejich	k3xOp3gFnSc4	jejich
habitatu	habitata	k1gFnSc4	habitata
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
slaví	slavit	k5eAaImIp3nP	slavit
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Slaví	slavit	k5eAaImIp3nS	slavit
se	se	k3xPyFc4	se
cíleně	cíleně	k6eAd1	cíleně
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhá	probíhat	k5eAaImIp3nS	probíhat
migrace	migrace	k1gFnSc1	migrace
tučňáka	tučňák	k1gMnSc2	tučňák
kroužkového	kroužkový	k2eAgMnSc2d1	kroužkový
z	z	k7c2	z
Antarktidy	Antarktida	k1gFnSc2	Antarktida
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VESELOVSKÝ	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zvířata	zvíře	k1gNnPc1	zvíře
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WILLIAMS	WILLIAMS	kA	WILLIAMS
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
D.	D.	kA	D.
<g/>
;	;	kIx,	;
WILSON	WILSON	kA	WILSON
<g/>
,	,	kIx,	,
Rory	Rory	k1gInPc7	Rory
P.	P.	kA	P.
The	The	k1gMnPc2	The
penguins	penguinsa	k1gFnPc2	penguinsa
:	:	kIx,	:
Spheniscidae	Spheniscidae	k1gFnPc2	Spheniscidae
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bird	Bird	k1gMnSc1	Bird
Families	Families	k1gMnSc1	Families
of	of	k?	of
the	the	k?	the
World	World	k1gMnSc1	World
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
19854667	[number]	k4	19854667
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
LEVICK	LEVICK	kA	LEVICK
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
Murray	Murraa	k1gMnSc2	Murraa
<g/>
.	.	kIx.	.
</s>
<s>
Antarctic	Antarctice	k1gFnPc2	Antarctice
Penguins	Penguinsa	k1gFnPc2	Penguinsa
<g/>
:	:	kIx,	:
A	a	k8xC	a
Study	stud	k1gInPc1	stud
of	of	k?	of
Their	Their	k1gInSc1	Their
Social	Social	k1gMnSc1	Social
Habits	Habits	k1gInSc1	Habits
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Projekt	projekt	k1gInSc1	projekt
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
William	William	k1gInSc1	William
Heinemann	Heinemanna	k1gFnPc2	Heinemanna
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Penguinscience	Penguinscience	k1gFnSc1	Penguinscience
-	-	kIx~	-
understanding	understanding	k1gInSc1	understanding
penguin	penguina	k1gFnPc2	penguina
response	response	k1gFnSc2	response
to	ten	k3xDgNnSc1	ten
climate	climat	k1gInSc5	climat
and	and	k?	and
ecosystem	ecosyst	k1gInSc7	ecosyst
change	change	k1gFnSc2	change
<g/>
.	.	kIx.	.
www.penguinscience.com	www.penguinscience.com	k1gInSc1	www.penguinscience.com
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
adeliae	adeliae	k1gFnSc1	adeliae
(	(	kIx(	(
<g/>
Adelie	Adelie	k1gFnSc1	Adelie
penguin	penguin	k1gMnSc1	penguin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc4	Diversit
Web	web	k1gInSc1	web
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Pygoscelis	Pygoscelis	k1gFnSc2	Pygoscelis
adeliae	adeliaat	k5eAaPmIp3nS	adeliaat
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
