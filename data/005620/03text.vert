<s>
Oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgNnPc4d1	velké
masa	maso	k1gNnPc4	maso
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
či	či	k8xC	či
jiné	jiný	k2eAgFnPc1d1	jiná
kapaliny	kapalina	k1gFnPc1	kapalina
<g/>
)	)	kIx)	)
nalézající	nalézající	k2eAgNnSc1d1	nalézající
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
či	či	k8xC	či
pod	pod	k7c4	pod
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ledem	led	k1gInSc7	led
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
tvoří	tvořit	k5eAaImIp3nP	tvořit
oceány	oceán	k1gInPc7	oceán
jedno	jeden	k4xCgNnSc4	jeden
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
a	a	k8xC	a
které	který	k3yIgInPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
většinu	většina	k1gFnSc4	většina
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Oceány	oceán	k1gInPc1	oceán
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgMnPc4d1	známý
nejlépe	dobře	k6eAd3	dobře
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
5	[number]	k4	5
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgInSc1d1	jižní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Oceány	oceán	k1gInPc1	oceán
nejsou	být	k5eNaImIp3nP	být
vázány	vázat	k5eAaImNgInP	vázat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
vesmírných	vesmírný	k2eAgNnPc6d1	vesmírné
tělesech	těleso	k1gNnPc6	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
specifických	specifický	k2eAgFnPc6d1	specifická
podmínkách	podmínka	k1gFnPc6	podmínka
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
a	a	k8xC	a
tvořeny	tvořit	k5eAaImNgInP	tvořit
různými	různý	k2eAgFnPc7d1	různá
kapalinami	kapalina	k1gFnPc7	kapalina
<g/>
,	,	kIx,	,
či	či	k8xC	či
tekutými	tekutý	k2eAgInPc7d1	tekutý
kovy	kov	k1gInPc7	kov
atd.	atd.	kA	atd.
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
#	#	kIx~	#
<g/>
Oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
otázek	otázka	k1gFnPc2	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
skutečně	skutečně	k6eAd1	skutečně
existoval	existovat	k5eAaImAgInS	existovat
komplexní	komplexní	k2eAgInSc1d1	komplexní
oceán	oceán	k1gInSc1	oceán
anebo	anebo	k8xC	anebo
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
lokálních	lokální	k2eAgFnPc2d1	lokální
zaplavených	zaplavený	k2eAgFnPc2d1	zaplavená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
oceán	oceán	k1gInSc1	oceán
nejspíše	nejspíše	k9	nejspíše
existoval	existovat	k5eAaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
doklady	doklad	k1gInPc4	doklad
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
počítají	počítat	k5eAaImIp3nP	počítat
geologické	geologický	k2eAgInPc1d1	geologický
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
připomínají	připomínat	k5eAaImIp3nP	připomínat
mořské	mořský	k2eAgNnSc4d1	mořské
pobřeží	pobřeží	k1gNnSc4	pobřeží
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
severní	severní	k2eAgFnSc1d1	severní
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
zcela	zcela	k6eAd1	zcela
hladká	hladkat	k5eAaImIp3nS	hladkat
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vyhlazená	vyhlazený	k2eAgFnSc1d1	vyhlazená
erozivní	erozivní	k2eAgFnSc7d1	erozivní
silou	síla	k1gFnSc7	síla
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
tvořila	tvořit	k5eAaImAgFnS	tvořit
oceánské	oceánský	k2eAgNnSc4d1	oceánské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
oceán	oceán	k1gInSc1	oceán
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
začala	začít	k5eAaPmAgFnS	začít
část	část	k1gFnSc1	část
vědců	vědec	k1gMnPc2	vědec
podrobněji	podrobně	k6eAd2	podrobně
zaobírat	zaobírat	k5eAaImF	zaobírat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
mohl	moct	k5eAaImAgInS	moct
existovat	existovat	k5eAaImF	existovat
oceán	oceán	k1gInSc1	oceán
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
oblastech	oblast	k1gFnPc6	oblast
<g/>
:	:	kIx,	:
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
(	(	kIx(	(
<g/>
Oceanus	Oceanus	k1gInSc1	Oceanus
Borealis	Borealis	k1gFnSc2	Borealis
<g/>
)	)	kIx)	)
–	–	k?	–
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
severních	severní	k2eAgFnPc2d1	severní
planin	planina	k1gFnPc2	planina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
model	model	k1gInSc1	model
oceánu	oceán	k1gInSc2	oceán
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgInSc2	tento
oceánu	oceán	k1gInSc2	oceán
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
ohromné	ohromný	k2eAgFnSc2d1	ohromná
záplavy	záplava	k1gFnSc2	záplava
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
108	[number]	k4	108
až	až	k9	až
109	[number]	k4	109
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
objemu	objem	k1gInSc6	objem
105	[number]	k4	105
až	až	k9	až
107	[number]	k4	107
km	km	kA	km
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
následek	následek	k1gInSc4	následek
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
v	v	k7c6	v
celoplanetárním	celoplanetární	k2eAgNnSc6d1	celoplanetární
měřítku	měřítko	k1gNnSc6	měřítko
(	(	kIx(	(
<g/>
žádný	žádný	k3yNgInSc4	žádný
impakt	impakt	k1gInSc4	impakt
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
by	by	kYmCp3nS	by
nejspíše	nejspíše	k9	nejspíše
nemohl	moct	k5eNaImAgInS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
takto	takto	k6eAd1	takto
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
oteplení	oteplení	k1gNnSc4	oteplení
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
roztáním	roztání	k1gNnSc7	roztání
permafrostu	permafrost	k1gInSc2	permafrost
a	a	k8xC	a
následné	následný	k2eAgFnPc1d1	následná
záplavy	záplava	k1gFnPc1	záplava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vypařování	vypařování	k1gNnSc1	vypařování
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
z	z	k7c2	z
plochy	plocha	k1gFnSc2	plocha
oceánu	oceán	k1gInSc2	oceán
obohatilo	obohatit	k5eAaPmAgNnS	obohatit
skleníkové	skleníkový	k2eAgInPc4d1	skleníkový
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vznik	vznik	k1gInSc4	vznik
teplejší	teplý	k2eAgInSc1d2	teplejší
a	a	k8xC	a
hustší	hustý	k2eAgFnPc1d2	hustší
atmosféry	atmosféra	k1gFnPc1	atmosféra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Oceán	oceán	k1gInSc1	oceán
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
nížinách	nížina	k1gFnPc6	nížina
(	(	kIx(	(
<g/>
Utopia	Utopia	k1gFnSc1	Utopia
Planitia	Planitia	k1gFnSc1	Planitia
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
menší	malý	k2eAgNnSc1d2	menší
vodní	vodní	k2eAgNnSc1d1	vodní
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyplňovalo	vyplňovat	k5eAaImAgNnS	vyplňovat
oblast	oblast	k1gFnSc4	oblast
Utopia	Utopium	k1gNnSc2	Utopium
Planitia	Planitium	k1gNnSc2	Planitium
a	a	k8xC	a
které	který	k3yQgNnSc1	který
teoreticky	teoreticky	k6eAd1	teoreticky
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
zlomkovou	zlomkový	k2eAgFnSc7d1	zlomková
částí	část	k1gFnSc7	část
většího	veliký	k2eAgInSc2d2	veliký
oceánu	oceán	k1gInSc2	oceán
Oceanus	Oceanus	k1gMnSc1	Oceanus
Borealis	Borealis	k1gFnSc1	Borealis
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
oblasti	oblast	k1gFnSc2	oblast
Utopia	Utopia	k1gFnSc1	Utopia
Planitia	Planitia	k1gFnSc1	Planitia
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
známky	známka	k1gFnPc4	známka
po	po	k7c6	po
přítomnosti	přítomnost	k1gFnSc6	přítomnost
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vrstvy	vrstva	k1gFnPc1	vrstva
sedimentů	sediment	k1gInPc2	sediment
či	či	k8xC	či
teras	terasa	k1gFnPc2	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumná	průzkumný	k2eAgNnPc1d1	průzkumné
vozítka	vozítko	k1gNnPc1	vozítko
Spirit	Spirita	k1gFnPc2	Spirita
a	a	k8xC	a
Opportunity	Opportunita	k1gFnSc2	Opportunita
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
sírany	síran	k1gInPc4	síran
vznikající	vznikající	k2eAgFnSc4d1	vznikající
během	během	k7c2	během
vypařování	vypařování	k1gNnSc2	vypařování
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
předchozí	předchozí	k2eAgInSc1d1	předchozí
výskyt	výskyt	k1gInSc1	výskyt
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
vědce	vědec	k1gMnPc4	vědec
neznámý	známý	k2eNgInSc4d1	neznámý
a	a	k8xC	a
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
oceánu	oceán	k1gInSc6	oceán
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
Mars	Mars	k1gInSc4	Mars
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
složení	složení	k1gNnSc1	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
nejspíše	nejspíše	k9	nejspíše
zapříčinil	zapříčinit	k5eAaPmAgMnS	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
kyselá	kyselý	k2eAgFnSc1d1	kyselá
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
pozemská	pozemský	k2eAgFnSc1d1	pozemská
<g/>
.	.	kIx.	.
</s>
<s>
Kyselé	kyselé	k1gNnSc1	kyselé
prostředí	prostředí	k1gNnSc2	prostředí
bránilo	bránit	k5eAaImAgNnS	bránit
srážení	srážení	k1gNnSc4	srážení
karbonátů	karbonát	k1gInPc2	karbonát
z	z	k7c2	z
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
aktivita	aktivita	k1gFnSc1	aktivita
v	v	k7c6	v
noachianu	noachian	k1gInSc6	noachian
vypouštěla	vypouštět	k5eAaImAgFnS	vypouštět
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
stále	stále	k6eAd1	stále
další	další	k2eAgNnSc4d1	další
množství	množství	k1gNnSc4	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
jeho	jeho	k3xOp3gFnSc4	jeho
koncentraci	koncentrace	k1gFnSc4	koncentrace
až	až	k9	až
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
stav	stav	k1gInSc4	stav
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
tvoří	tvořit	k5eAaImIp3nS	tvořit
95,32	[number]	k4	95,32
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
modelu	model	k1gInSc2	model
kyselého	kyselý	k2eAgInSc2d1	kyselý
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
chybějící	chybějící	k2eAgInPc4d1	chybějící
karbonáty	karbonát	k1gInPc4	karbonát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
by	by	kYmCp3nS	by
s	s	k7c7	s
oceánem	oceán	k1gInSc7	oceán
nejspíše	nejspíše	k9	nejspíše
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Jupiterovu	Jupiterův	k2eAgFnSc4d1	Jupiterova
měsíci	měsíc	k1gInSc6	měsíc
Europa	Europ	k1gMnSc2	Europ
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
pod	pod	k7c7	pod
ledovou	ledový	k2eAgFnSc7d1	ledová
vrstvou	vrstva	k1gFnSc7	vrstva
nachází	nacházet	k5eAaImIp3nS	nacházet
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zahříván	zahřívat	k5eAaImNgInS	zahřívat
vlivem	vlivem	k7c2	vlivem
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Tvořen	tvořen	k2eAgInSc4d1	tvořen
nejspíše	nejspíše	k9	nejspíše
kapalným	kapalný	k2eAgInSc7d1	kapalný
metanem	metan	k1gInSc7	metan
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oceán	oceán	k1gInSc1	oceán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
oceán	oceán	k1gInSc1	oceán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
