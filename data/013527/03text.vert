<s>
Ignác	Ignác	k1gMnSc1
Spiro	Spiro	k1gNnSc1
</s>
<s>
Ignác	Ignác	k1gMnSc1
Spiro	Spiro	k1gNnSc1
Ignác	Ignác	k1gMnSc1
Spiro	Spiro	k1gNnSc5
Narození	narozený	k2eAgMnPc5d1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1817	#num#	k4
<g/>
KaleniceRakouské	KaleniceRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1894	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Český	český	k2eAgInSc1d1
KrumlovRakousko-Uhersko	KrumlovRakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Povolání	povolání	k1gNnSc2
</s>
<s>
obchodník	obchodník	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Jacob	Jacobit	k5eAaPmRp2nS
Spiro	Spira	k1gMnSc5
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ignác	Ignác	k1gMnSc1
Spiro	Spiro	k1gMnSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1817	#num#	k4
Kalenice	kalenice	k1gFnSc2
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1894	#num#	k4
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
českokrumlovský	českokrumlovský	k2eAgMnSc1d1
občan	občan	k1gMnSc1
<g/>
,	,	kIx,
průmyslník	průmyslník	k1gMnSc1
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
Větřínských	Větřínský	k2eAgFnPc2d1
papíren	papírna	k1gFnPc2
u	u	k7c2
Českého	český	k2eAgInSc2d1
Krumlova	Krumlov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Přestože	přestože	k8xS
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
do	do	k7c2
velmi	velmi	k6eAd1
chudé	chudý	k2eAgFnSc2d1
židovské	židovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
dokázal	dokázat	k5eAaPmAgMnS
se	se	k3xPyFc4
ještě	ještě	k9
jako	jako	k9
náctiletý	náctiletý	k2eAgMnSc1d1
z	z	k7c2
bídy	bída	k1gFnSc2
vypracovat	vypracovat	k5eAaPmF
a	a	k8xC
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
Jákobem	Jákob	k1gMnSc7
založili	založit	k5eAaPmAgMnP
sběrnu	sběrna	k1gFnSc4
papíru	papír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
studií	studie	k1gFnPc2
bratři	bratr	k1gMnPc1
zakoupili	zakoupit	k5eAaPmAgMnP
papírnu	papírna	k1gFnSc4
v	v	k7c6
Červené	Červené	k2eAgFnSc6d1
Řečici	Řečice	k1gFnSc6
a	a	k8xC
konečně	konečně	k6eAd1
i	i	k9
papírnu	papírna	k1gFnSc4
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
si	se	k3xPyFc3
majetek	majetek	k1gInSc4
mezi	mezi	k7c4
sebe	sebe	k3xPyFc4
rozdělili	rozdělit	k5eAaPmAgMnP
a	a	k8xC
co	co	k9
krumlovská	krumlovský	k2eAgFnSc1d1
papírna	papírna	k1gFnSc1
připadla	připadnout	k5eAaPmAgFnS
Ignácovi	Ignác	k1gMnSc3
<g/>
,	,	kIx,
zakoupil	zakoupit	k5eAaPmAgMnS
Ignác	Ignác	k1gMnSc1
i	i	k8xC
tzv.	tzv.	kA
Pečkovský	Pečkovský	k2eAgInSc4d1
mlýn	mlýn	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
kompletně	kompletně	k6eAd1
přebudoval	přebudovat	k5eAaPmAgMnS
a	a	k8xC
rozšířil	rozšířit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
založil	založit	k5eAaPmAgMnS
papírnu	papírna	k1gFnSc4
ve	v	k7c4
Větřní	Větřní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
během	během	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
se	se	k3xPyFc4
Větřínské	Větřínský	k2eAgFnPc1d1
papírny	papírna	k1gFnPc1
staly	stát	k5eAaPmAgFnP
nejvýznamnějšími	významný	k2eAgInPc7d3
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
za	za	k7c4
vedení	vedení	k1gNnSc4
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
syna	syn	k1gMnSc2
i	i	k8xC
nejvýznamnějšími	významný	k2eAgInPc7d3
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ignác	Ignác	k1gMnSc1
Spiro	Spiro	k1gNnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
zasloužil	zasloužit	k5eAaPmAgMnS
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
průmyslu	průmysl	k1gInSc2
a	a	k8xC
především	především	k9
zaměstnanosti	zaměstnanost	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
a	a	k8xC
celém	celý	k2eAgInSc6d1
regionu	region	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
i	i	k9
na	na	k7c6
založení	založení	k1gNnSc6
židovské	židovský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
rodinné	rodinný	k2eAgFnSc6d1
hrobce	hrobka	k1gFnSc6
na	na	k7c6
židovském	židovský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://budejovice.idnes.cz/papirny-vetrni-v-insolvenci-dgi-/budejovice-zpravy.aspx?c=A130506_1925315_budejovice-zpravy_jkr	http://budejovice.idnes.cz/papirny-vetrni-v-insolvenci-dgi-/budejovice-zpravy.aspx?c=A130506_1925315_budejovice-zpravy_jkr	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1
Spiro	Spiro	k1gNnSc1
<g/>
,	,	kIx,
Vyšší	vysoký	k2eAgInSc1d2
Brod	Brod	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ignác	Ignác	k1gMnSc1
Spiro	Spiro	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Historie	historie	k1gFnSc1
papírny	papírna	k1gFnSc2
Větřní	Větřní	k2eAgFnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
|	|	kIx~
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Hebraistika	hebraistika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
