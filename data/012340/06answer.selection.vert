<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
řídce	řídce	k6eAd1	řídce
hnízdícím	hnízdící	k2eAgInSc7d1	hnízdící
druhem	druh	k1gInSc7	druh
<g/>
;	;	kIx,	;
celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
a	a	k8xC	a
silně	silně	k6eAd1	silně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
