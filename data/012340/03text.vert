<p>
<s>
Bekasina	Bekasina	k1gFnSc1	Bekasina
otavní	otavní	k2eAgFnSc1d1	otavní
(	(	kIx(	(
<g/>
Gallinago	Gallinago	k1gMnSc1	Gallinago
gallinago	gallinago	k1gMnSc1	gallinago
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velkým	velký	k2eAgInSc7d1	velký
druhem	druh	k1gInSc7	druh
bahňáka	bahňák	k1gMnSc2	bahňák
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slukovitých	slukovitý	k2eAgFnPc2d1	slukovitý
(	(	kIx(	(
<g/>
Scolopacidae	Scolopacidae	k1gFnPc2	Scolopacidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
kosa	kosa	k1gFnSc1	kosa
<g/>
,	,	kIx,	,
podsaditá	podsaditý	k2eAgFnSc1d1	podsaditá
<g/>
,	,	kIx,	,
s	s	k7c7	s
nápadně	nápadně	k6eAd1	nápadně
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
rovným	rovný	k2eAgInSc7d1	rovný
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
krátkýma	krátký	k2eAgFnPc7d1	krátká
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
28	[number]	k4	28
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
44	[number]	k4	44
<g/>
–	–	k?	–
<g/>
47	[number]	k4	47
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
140	[number]	k4	140
g.	g.	k?	g.
Svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
,	,	kIx,	,
se	s	k7c7	s
světlými	světlý	k2eAgInPc7d1	světlý
podélnými	podélný	k2eAgInPc7d1	podélný
pruhy	pruh	k1gInPc7	pruh
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
hřbetě	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
kaštanově	kaštanově	k6eAd1	kaštanově
hnědým	hnědý	k2eAgInSc7d1	hnědý
<g/>
,	,	kIx,	,
černě	černě	k6eAd1	černě
pruhovaným	pruhovaný	k2eAgInSc7d1	pruhovaný
ocasem	ocas	k1gInSc7	ocas
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
okrajem	okraj	k1gInSc7	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
a	a	k8xC	a
zadní	zadní	k2eAgInSc1d1	zadní
okraj	okraj	k1gInSc1	okraj
křídla	křídlo	k1gNnSc2	křídlo
má	mít	k5eAaImIp3nS	mít
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobná	podobný	k2eAgFnSc1d1	podobná
bekasině	bekasina	k1gFnSc3	bekasina
větší	veliký	k2eAgFnSc2d2	veliký
(	(	kIx(	(
<g/>
G.	G.	kA	G.
media	medium	k1gNnSc2	medium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
větší	veliký	k2eAgFnSc7d2	veliký
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
pruhovaným	pruhovaný	k2eAgNnSc7d1	pruhované
břichem	břicho	k1gNnSc7	břicho
a	a	k8xC	a
více	hodně	k6eAd2	hodně
bíle	bíle	k6eAd1	bíle
pruhovanými	pruhovaný	k2eAgNnPc7d1	pruhované
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
bílého	bílý	k2eAgInSc2d1	bílý
zadního	zadní	k2eAgInSc2d1	zadní
okraje	okraj	k1gInSc2	okraj
<g/>
.	.	kIx.	.
<g/>
Žije	žít	k5eAaImIp3nS	žít
skrytě	skrytě	k6eAd1	skrytě
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyplašení	vyplašení	k1gNnSc6	vyplašení
vyletuje	vyletovat	k5eAaImIp3nS	vyletovat
nízkým	nízký	k2eAgInSc7d1	nízký
klikatým	klikatý	k2eAgInSc7d1	klikatý
letem	let	k1gInSc7	let
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
ozývá	ozývat	k5eAaImIp3nS	ozývat
nosovým	nosový	k2eAgInSc7d1	nosový
"	"	kIx"	"
<g/>
keč	keč	k?	keč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
zvukovým	zvukový	k2eAgInSc7d1	zvukový
projevem	projev	k1gInSc7	projev
jsou	být	k5eAaImIp3nP	být
mekavé	mekavý	k2eAgInPc1d1	mekavý
zvuky	zvuk	k1gInPc1	zvuk
vydávané	vydávaný	k2eAgInPc1d1	vydávaný
samci	samec	k1gMnSc3	samec
chvěním	chvění	k1gNnSc7	chvění
ocasních	ocasní	k2eAgNnPc2d1	ocasní
per	pero	k1gNnPc2	pero
zejména	zejména	k9	zejména
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
a	a	k8xC	a
podvečerních	podvečerní	k2eAgFnPc6d1	podvečerní
hodinách	hodina	k1gFnPc6	hodina
při	při	k7c6	při
střemhlavém	střemhlavý	k2eAgInSc6d1	střemhlavý
letu	let	k1gInSc6	let
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předvádí	předvádět	k5eAaImIp3nS	předvádět
během	během	k7c2	během
obletování	obletování	k1gNnSc2	obletování
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
také	také	k9	také
rytmickým	rytmický	k2eAgInSc7d1	rytmický
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
dlouze	dlouho	k6eAd1	dlouho
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
"	"	kIx"	"
<g/>
tik	tik	k1gInSc1	tik
ke	k	k7c3	k
tik	tik	k1gInSc4	tik
ke	k	k7c3	k
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Eurasie	Eurasie	k1gFnSc2	Eurasie
a	a	k8xC	a
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
uznávaných	uznávaný	k2eAgInPc2d1	uznávaný
poddruhů	poddruh	k1gInPc2	poddruh
i	i	k9	i
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
evropští	evropský	k2eAgMnPc1d1	evropský
ptáci	pták	k1gMnPc1	pták
zimují	zimovat	k5eAaImIp3nP	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
po	po	k7c4	po
rovník	rovník	k1gInSc4	rovník
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kapitolu	kapitola	k1gFnSc4	kapitola
Systematika	systematika	k1gFnSc1	systematika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podzimní	podzimní	k2eAgInSc1d1	podzimní
tah	tah	k1gInSc1	tah
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
přilétá	přilétat	k5eAaImIp3nS	přilétat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
930	[number]	k4	930
tisíc	tisíc	k4xCgInSc4	tisíc
až	až	k8xS	až
1,9	[number]	k4	1,9
milionu	milion	k4xCgInSc2	milion
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
asi	asi	k9	asi
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
populačních	populační	k2eAgFnPc2d1	populační
hustot	hustota	k1gFnPc2	hustota
přitom	přitom	k6eAd1	přitom
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
stavy	stav	k1gInPc1	stav
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
oblastech	oblast	k1gFnPc6	oblast
výrazně	výrazně	k6eAd1	výrazně
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
;	;	kIx,	;
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biotopu	biotop	k1gInSc2	biotop
spojená	spojený	k2eAgFnSc1d1	spojená
zejména	zejména	k9	zejména
s	s	k7c7	s
odvodňováním	odvodňování	k1gNnSc7	odvodňování
mokřadů	mokřad	k1gInPc2	mokřad
a	a	k8xC	a
intenzifikací	intenzifikace	k1gFnSc7	intenzifikace
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
řídce	řídce	k6eAd1	řídce
hnízdícím	hnízdící	k2eAgInSc7d1	hnízdící
druhem	druh	k1gInSc7	druh
<g/>
;	;	kIx,	;
celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
a	a	k8xC	a
silně	silně	k6eAd1	silně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
také	také	k9	také
početně	početně	k6eAd1	početně
protahuje	protahovat	k5eAaImIp3nS	protahovat
a	a	k8xC	a
nepravidelně	pravidelně	k6eNd1	pravidelně
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
30-70	[number]	k4	30-70
jedinců	jedinec	k1gMnPc2	jedinec
i	i	k9	i
zimuje	zimovat	k5eAaImIp3nS	zimovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
chráněná	chráněný	k2eAgFnSc1d1	chráněná
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc2	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
rašeliništích	rašeliniště	k1gNnPc6	rašeliniště
<g/>
,	,	kIx,	,
slatiništích	slatiniště	k1gNnPc6	slatiniště
<g/>
,	,	kIx,	,
vlhkých	vlhký	k2eAgFnPc6d1	vlhká
a	a	k8xC	a
podmáčených	podmáčený	k2eAgFnPc6d1	podmáčená
loukách	louka	k1gFnPc6	louka
a	a	k8xC	a
okrajích	okraj	k1gInPc6	okraj
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
červy	červ	k1gMnPc4	červ
a	a	k8xC	a
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
,	,	kIx,	,
korýši	korýš	k1gMnPc1	korýš
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc4	pavouk
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k8xC	i
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potravě	potrava	k1gFnSc6	potrava
pátrá	pátrat	k5eAaImIp3nS	pátrat
na	na	k7c6	na
bahnech	bahno	k1gNnPc6	bahno
a	a	k8xC	a
v	v	k7c6	v
mělké	mělký	k2eAgFnSc6d1	mělká
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
píchá	píchat	k5eAaImIp3nS	píchat
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
svým	svůj	k3xOyFgInSc7	svůj
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	jíst	k5eAaImIp1nS	jíst
přitom	přitom	k6eAd1	přitom
schopna	schopen	k2eAgFnSc1d1	schopna
malou	malý	k2eAgFnSc4d1	malá
kořist	kořist	k1gFnSc4	kořist
chytit	chytit	k5eAaPmF	chytit
a	a	k8xC	a
polknout	polknout	k5eAaPmF	polknout
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
musela	muset	k5eAaImAgFnS	muset
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
často	často	k6eAd1	často
ponořená	ponořený	k2eAgFnSc1d1	ponořená
až	až	k9	až
po	po	k7c4	po
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
do	do	k7c2	do
měkké	měkký	k2eAgFnSc2d1	měkká
půdy	půda	k1gFnSc2	půda
pak	pak	k6eAd1	pak
zobák	zobák	k1gInSc4	zobák
mnohdy	mnohdy	k6eAd1	mnohdy
zapichuje	zapichovat	k5eAaImIp3nS	zapichovat
až	až	k9	až
po	po	k7c4	po
kořen	kořen	k1gInSc4	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdiště	hnízdiště	k1gNnSc1	hnízdiště
vybírá	vybírat	k5eAaImIp3nS	vybírat
samec	samec	k1gInSc4	samec
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
zahnízdění	zahnízdění	k1gNnSc3	zahnízdění
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
travinách	travina	k1gFnPc6	travina
<g/>
,	,	kIx,	,
vystlané	vystlaný	k2eAgNnSc1d1	vystlané
suchým	suchý	k2eAgInSc7d1	suchý
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
snáší	snášet	k5eAaImIp3nP	snášet
většinou	většina	k1gFnSc7	většina
4	[number]	k4	4
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
hruškovitá	hruškovitý	k2eAgNnPc1d1	hruškovité
<g/>
,	,	kIx,	,
olivově	olivově	k6eAd1	olivově
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
,	,	kIx,	,
39,2	[number]	k4	39,2
×	×	k?	×
28,3	[number]	k4	28,3
mm	mm	kA	mm
velká	velká	k1gFnSc1	velká
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
sama	sám	k3xTgMnSc4	sám
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
střeží	střežit	k5eAaImIp3nS	střežit
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc1	hnízdo
opouští	opouštět	k5eAaImIp3nS	opouštět
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
a	a	k8xC	a
rodiči	rodič	k1gMnPc7	rodič
jsou	být	k5eAaImIp3nP	být
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
dalších	další	k2eAgInPc2d1	další
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začínají	začínat	k5eAaImIp3nP	začínat
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Plné	plný	k2eAgFnPc1d1	plná
vzletnosti	vzletnost	k1gFnPc1	vzletnost
pak	pak	k6eAd1	pak
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Balty	Balt	k1gMnPc4	Balt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Litevce	Litevec	k1gMnSc4	Litevec
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pohanském	pohanský	k2eAgNnSc6d1	pohanské
období	období	k1gNnSc6	období
bekasina	bekasina	k1gFnSc1	bekasina
posvátným	posvátný	k2eAgMnSc7d1	posvátný
ptákem	pták	k1gMnSc7	pták
<g/>
,	,	kIx,	,
spojeným	spojený	k2eAgMnSc7d1	spojený
s	s	k7c7	s
kultem	kult	k1gInSc7	kult
boha	bůh	k1gMnSc2	bůh
Perkunase	Perkunasa	k1gFnSc3	Perkunasa
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
slovanského	slovanský	k2eAgMnSc2d1	slovanský
Peruna	Perun	k1gMnSc2	Perun
<g/>
)	)	kIx)	)
a	a	k8xC	a
nazývali	nazývat	k5eAaImAgMnP	nazývat
ji	on	k3xPp3gFnSc4	on
Perkū	Perkū	k1gFnSc4	Perkū
oželis	oželis	k1gFnSc2	oželis
"	"	kIx"	"
<g/>
Perkunova	Perkunův	k2eAgFnSc1d1	Perkunův
kozička	kozička	k1gFnSc1	kozička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systematika	systematika	k1gFnSc1	systematika
==	==	k?	==
</s>
</p>
<p>
<s>
Polytypický	Polytypický	k2eAgInSc1d1	Polytypický
druh	druh	k1gInSc1	druh
s	s	k7c7	s
2-3	[number]	k4	2-3
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
G.	G.	kA	G.
g.	g.	k?	g.
gallinago	gallinago	k1gMnSc1	gallinago
<g/>
:	:	kIx,	:
většina	většina	k1gFnSc1	většina
eurasijského	eurasijský	k2eAgInSc2d1	eurasijský
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
od	od	k7c2	od
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
východně	východně	k6eAd1	východně
až	až	k9	až
po	po	k7c4	po
Kamčatku	Kamčatka	k1gFnSc4	Kamčatka
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
2	[number]	k4	2
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
západně	západně	k6eAd1	západně
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
a	a	k8xC	a
zimuje	zimovat	k5eAaImIp3nS	zimovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
populace	populace	k1gFnSc1	populace
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
východně	východně	k6eAd1	východně
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
a	a	k8xC	a
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
zimuje	zimovat	k5eAaImIp3nS	zimovat
především	především	k9	především
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
G.	G.	kA	G.
g.	g.	k?	g.
faeroensis	faeroensis	k1gInSc1	faeroensis
<g/>
:	:	kIx,	:
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
,	,	kIx,	,
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
Orkenejích	Orkenej	k1gInPc6	Orkenej
a	a	k8xC	a
Shetlandech	Shetland	k1gInPc6	Shetland
<g/>
;	;	kIx,	;
zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
poddruhů	poddruh	k1gInPc2	poddruh
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
popsána	popsán	k2eAgFnSc1d1	popsána
i	i	k8xC	i
bekasina	bekasin	k2eAgFnSc1d1	bekasina
otavní	otavní	k2eAgFnSc1d1	otavní
americká	americký	k2eAgFnSc1d1	americká
(	(	kIx(	(
<g/>
G.	G.	kA	G.
g.	g.	k?	g.
delicata	delicata	k1gFnSc1	delicata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgFnPc1d1	lišící
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
ocasních	ocasní	k2eAgNnPc2d1	ocasní
per	pero	k1gNnPc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zpochybnění	zpochybnění	k1gNnSc6	zpochybnění
této	tento	k3xDgFnSc2	tento
klasifikace	klasifikace	k1gFnSc2	klasifikace
Banksem	Banks	k1gMnSc7	Banks
et	et	k?	et
<g/>
.	.	kIx.	.
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
jej	on	k3xPp3gInSc4	on
American	American	k1gInSc4	American
Ornithologists	Ornithologists	k1gInSc1	Ornithologists
<g/>
'	'	kIx"	'
Union	union	k1gInSc1	union
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
a	a	k8xC	a
South	South	k1gInSc1	South
American	Americana	k1gFnPc2	Americana
Classification	Classification	k1gInSc1	Classification
Committee	Committe	k1gInSc2	Committe
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
uznali	uznat	k5eAaPmAgMnP	uznat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
–	–	k?	–
bekasinu	bekasin	k2eAgFnSc4d1	bekasina
americkou	americký	k2eAgFnSc4d1	americká
(	(	kIx(	(
<g/>
G.	G.	kA	G.
delicata	delicata	k1gFnSc1	delicata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
BirdLife	BirdLif	k1gMnSc5	BirdLif
Taxonomic	Taxonomic	k1gMnSc1	Taxonomic
Working	Working	k1gInSc4	Working
Group	Group	k1gMnSc1	Group
však	však	k9	však
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
učinit	učinit	k5eAaPmF	učinit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
morfologické	morfologický	k2eAgInPc4d1	morfologický
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
taxony	taxon	k1gInPc7	taxon
spočívají	spočívat	k5eAaImIp3nP	spočívat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
šířce	šířka	k1gFnSc6	šířka
a	a	k8xC	a
počtu	počet	k1gInSc6	počet
ocasních	ocasní	k2eAgNnPc2d1	ocasní
per	pero	k1gNnPc2	pero
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
i	i	k9	i
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
tohoto	tento	k3xDgInSc2	tento
taxonu	taxon	k1gInSc2	taxon
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
zatím	zatím	k6eAd1	zatím
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bekasina	bekasina	k1gFnSc1	bekasina
otavní	otavní	k2eAgFnSc1d1	otavní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
bekasina	bekasin	k2eAgFnSc1d1	bekasina
otavní	otavní	k2eAgFnPc4d1	otavní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Gallinago	Gallinago	k6eAd1	Gallinago
gallinago	gallinago	k6eAd1	gallinago
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Hlasová	hlasový	k2eAgFnSc1d1	hlasová
ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hlas	hlas	k1gInSc1	hlas
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
