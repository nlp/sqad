<s>
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Gdańsk	Gdańsk	k1gInSc1	Gdańsk
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
[	[	kIx(	[
<g/>
gdaɲ	gdaɲ	k?	gdaɲ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
kašubsky	kašubsky	k6eAd1	kašubsky
Gduńsk	Gduńsk	k1gInSc1	Gduńsk
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Danzig	Danzig	k1gInSc1	Danzig
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Gedania	Gedanium	k1gNnSc2	Gedanium
<g/>
,	,	kIx,	,
Gedanum	Gedanum	k1gInSc1	Gedanum
abo	abo	k?	abo
Dantiscum	Dantiscum	k1gInSc1	Dantiscum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
a	a	k8xC	a
největších	veliký	k2eAgNnPc2d3	veliký
polských	polský	k2eAgNnPc2d1	polské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
metropolí	metropol	k1gFnPc2	metropol
Pomořského	pomořský	k2eAgNnSc2d1	Pomořské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
Pomoří	Pomoří	k1gNnSc6	Pomoří
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Visle	Visla	k1gFnSc6	Visla
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
u	u	k7c2	u
Gdaňského	gdaňský	k2eAgInSc2d1	gdaňský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
roli	role	k1gFnSc4	role
kašubského	kašubský	k2eAgNnSc2d1	kašubské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
kulturního	kulturní	k2eAgNnSc2d1	kulturní
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
sídla	sídlo	k1gNnSc2	sídlo
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
kašubských	kašubský	k2eAgFnPc2d1	kašubská
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
262,5	[number]	k4	262,5
km2	km2	k4	km2
a	a	k8xC	a
455	[number]	k4	455
717	[number]	k4	717
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
velký	velký	k2eAgInSc1d1	velký
námořní	námořní	k2eAgInSc1d1	námořní
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
městy	město	k1gNnPc7	město
Sopoty	sopot	k1gInPc1	sopot
a	a	k8xC	a
Gdyně	Gdyně	k1gFnPc1	Gdyně
tvoří	tvořit	k5eAaImIp3nP	tvořit
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
název	název	k1gInSc1	název
Trojměstí	trojměstí	k1gNnSc2	trojměstí
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
'	'	kIx"	'
<g/>
Trójmiasto	Trójmiasta	k1gMnSc5	Trójmiasta
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Motlavy	Motlava	k1gFnSc2	Motlava
do	do	k7c2	do
Visly	Visla	k1gFnSc2	Visla
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
Visly	Visla	k1gFnSc2	Visla
tvoří	tvořit	k5eAaImIp3nS	tvořit
východní	východní	k2eAgFnSc4d1	východní
administrativní	administrativní	k2eAgFnSc4d1	administrativní
hranici	hranice	k1gFnSc4	hranice
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
město	město	k1gNnSc4	město
vede	vést	k5eAaImIp3nS	vést
hlavní	hlavní	k2eAgFnSc1d1	hlavní
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Varšava	Varšava	k1gFnSc1	Varšava
-	-	kIx~	-
Gdyně	Gdyně	k1gFnSc1	Gdyně
a	a	k8xC	a
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
ní	on	k3xPp3gFnSc6	on
Szybka	Szybka	k1gFnSc1	Szybka
kolej	kolej	k1gFnSc4	kolej
miejska	miejsk	k1gInSc2	miejsk
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
dopravu	doprava	k1gFnSc4	doprava
po	po	k7c6	po
gdaňské	gdaňský	k2eAgFnSc6d1	Gdaňská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
je	být	k5eAaImIp3nS	být
Gdańsk	Gdańsk	k1gInSc1	Gdańsk
Główny	Główna	k1gFnSc2	Główna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Gdaňsku	Gdaňsk	k1gInSc2	Gdaňsk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malý	malý	k2eAgInSc1d1	malý
poloostrov	poloostrov	k1gInSc1	poloostrov
Westerplatte	Westerplatt	k1gMnSc5	Westerplatt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
loděnicích	loděnice	k1gFnPc6	loděnice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Solidarita	solidarita	k1gFnSc1	solidarita
(	(	kIx(	(
<g/>
Solidarność	Solidarność	k1gFnSc1	Solidarność
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborová	odborový	k2eAgFnSc1d1	odborová
organizace	organizace	k1gFnSc1	organizace
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
politická	politický	k2eAgFnSc1d1	politická
opozice	opozice	k1gFnSc1	opozice
komunistického	komunistický	k2eAgNnSc2d1	komunistické
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
Lech	Lech	k1gMnSc1	Lech
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
polským	polský	k2eAgMnSc7d1	polský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
<g/>
,	,	kIx,	,
dokládající	dokládající	k2eAgFnSc1d1	dokládající
místní	místní	k2eAgNnSc4d1	místní
osídlení	osídlení	k1gNnSc4	osídlení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
rybářské	rybářský	k2eAgFnSc2d1	rybářská
osady	osada	k1gFnSc2	osada
<g/>
,	,	kIx,	,
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
první	první	k4xOgFnSc4	první
písemnou	písemný	k2eAgFnSc4d1	písemná
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
městě	město	k1gNnSc6	město
nalézáme	nalézat	k5eAaImIp1nP	nalézat
v	v	k7c6	v
legendě	legenda	k1gFnSc6	legenda
Život	život	k1gInSc1	život
svatého	svatý	k2eAgMnSc2d1	svatý
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
k	k	k7c3	k
roku	rok	k1gInSc3	rok
997	[number]	k4	997
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
sepsána	sepsat	k5eAaPmNgFnS	sepsat
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
světcově	světcův	k2eAgFnSc6d1	světcova
smrti	smrt	k1gFnSc6	smrt
-	-	kIx~	-
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
997	[number]	k4	997
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
tradičně	tradičně	k6eAd1	tradičně
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k9	jako
počátek	počátek	k1gInSc4	počátek
historie	historie	k1gFnSc2	historie
Gdaňsku	Gdaňsk	k1gInSc2	Gdaňsk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zabral	zabrat	k5eAaPmAgMnS	zabrat
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Křivoústý	křivoústý	k2eAgInSc1d1	křivoústý
Západní	západní	k2eAgInSc1d1	západní
Pomořansko	Pomořansko	k1gNnSc4	Pomořansko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1227	[number]	k4	1227
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
území	území	k1gNnSc3	území
zmocňuje	zmocňovat	k5eAaImIp3nS	zmocňovat
pomořanský	pomořanský	k2eAgMnSc1d1	pomořanský
kníže	kníže	k1gMnSc1	kníže
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Pomořanská	pomořanský	k2eAgNnPc1d1	Pomořanské
knížata	kníže	k1gNnPc1	kníže
si	se	k3xPyFc3	se
panství	panství	k1gNnSc1	panství
nad	nad	k7c7	nad
Gdaňskem	Gdaňsk	k1gInSc7	Gdaňsk
udržela	udržet	k5eAaPmAgFnS	udržet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1294	[number]	k4	1294
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
důležitá	důležitý	k2eAgFnSc1d1	důležitá
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1263	[number]	k4	1263
byla	být	k5eAaImAgFnS	být
Gdaňsku	Gdaňsk	k1gInSc2	Gdaňsk
udělena	udělen	k2eAgNnPc1d1	uděleno
městská	městský	k2eAgNnPc1d1	Městské
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
997	[number]	k4	997
<g/>
-	-	kIx~	-
<g/>
1308	[number]	k4	1308
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Polského	polský	k2eAgNnSc2d1	polské
království	království	k1gNnSc2	království
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
1308	[number]	k4	1308
<g/>
-	-	kIx~	-
<g/>
1454	[number]	k4	1454
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
řádového	řádový	k2eAgInSc2d1	řádový
státu	stát	k1gInSc2	stát
něm.	něm.	k?	něm.
rytířů	rytíř	k1gMnPc2	rytíř
1454	[number]	k4	1454
<g/>
-	-	kIx~	-
<g/>
1466	[number]	k4	1466
<g/>
:	:	kIx,	:
město	město	k1gNnSc1	město
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
Třináctileté	třináctiletý	k2eAgFnSc2d1	třináctiletá
války	válka	k1gFnSc2	válka
1466	[number]	k4	1466
<g/>
-	-	kIx~	-
<g/>
1569	[number]	k4	1569
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Polského	polský	k2eAgNnSc2d1	polské
království	království	k1gNnSc2	království
1569	[number]	k4	1569
<g/>
-	-	kIx~	-
<g/>
1793	[number]	k4	1793
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Polsko-litevské	polskoitevský	k2eAgFnSc2d1	polsko-litevská
unie	unie	k1gFnSc2	unie
1793	[number]	k4	1793
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1807	[number]	k4	1807
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Pruska	Prusko	k1gNnSc2	Prusko
1807	[number]	k4	1807
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
:	:	kIx,	:
svobodné	svobodný	k2eAgNnSc1d1	svobodné
město	město	k1gNnSc1	město
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Pruska	Prusko	k1gNnSc2	Prusko
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
svobodné	svobodný	k2eAgNnSc1d1	svobodné
město	město	k1gNnSc1	město
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
nacistické	nacistický	k2eAgFnSc2d1	nacistická
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
1945	[number]	k4	1945
<g/>
-dosud	osuda	k1gFnPc2	-dosuda
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
Polské	polský	k2eAgFnSc2d1	polská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
převážně	převážně	k6eAd1	převážně
německým	německý	k2eAgNnSc7d1	německé
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvořili	tvořit	k5eAaImAgMnP	tvořit
90	[number]	k4	90
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
převážně	převážně	k6eAd1	převážně
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Kašubové	Kašub	k1gMnPc1	Kašub
<g/>
;	;	kIx,	;
většinové	většinový	k2eAgNnSc1d1	většinové
vyznání	vyznání	k1gNnSc1	vyznání
bylo	být	k5eAaImAgNnS	být
evangelické	evangelický	k2eAgNnSc1d1	evangelické
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
rostl	růst	k5eAaImAgInS	růst
podíl	podíl	k1gInSc1	podíl
katolictví	katolictví	k1gNnSc2	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
sem	sem	k6eAd1	sem
přišlo	přijít	k5eAaPmAgNnS	přijít
nové	nový	k2eAgNnSc1d1	nové
polské	polský	k2eAgNnSc1d1	polské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
osobnosti	osobnost	k1gFnPc4	osobnost
pocházející	pocházející	k2eAgFnPc4d1	pocházející
z	z	k7c2	z
Gdaňska	Gdaňsk	k1gInSc2	Gdaňsk
<g/>
,	,	kIx,	,
či	či	k8xC	či
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spojili	spojit	k5eAaPmAgMnP	spojit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Daniel	Daniel	k1gMnSc1	Daniel
Gabriel	Gabriel	k1gMnSc1	Gabriel
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
Günter	Güntra	k1gFnPc2	Güntra
Grass	Grass	k1gInSc1	Grass
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
Johannes	Johannes	k1gMnSc1	Johannes
Hevelius	Hevelius	k1gMnSc1	Hevelius
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
Daniel	Daniel	k1gMnSc1	Daniel
Chodowiecki	Chodowieck	k1gMnPc1	Chodowieck
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
Arthur	Arthur	k1gMnSc1	Arthur
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
Donald	Donald	k1gMnSc1	Donald
Tusk	Tusk	k1gMnSc1	Tusk
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
politik	politik	k1gMnSc1	politik
Lech	Lech	k1gMnSc1	Lech
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
politik	politik	k1gMnSc1	politik
</s>
