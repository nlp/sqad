<s>
KOH-I-NOOR	KOH-I-NOOR	k?	KOH-I-NOOR
HARDTMUTH	HARDTMUTH	kA	HARDTMUTH
a.s.	a.s.	k?	a.s.
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
jihočeských	jihočeský	k2eAgInPc6d1	jihočeský
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
psacích	psací	k2eAgFnPc2d1	psací
a	a	k8xC	a
výtvarných	výtvarný	k2eAgFnPc2d1	výtvarná
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
Josefem	Josef	k1gMnSc7	Josef
Hardtmuthem	Hardtmuth	k1gInSc7	Hardtmuth
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slavného	slavný	k2eAgInSc2d1	slavný
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
si	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
firma	firma	k1gFnSc1	firma
patentovat	patentovat	k5eAaBmF	patentovat
recept	recept	k1gInSc4	recept
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tuhy	tuha	k1gFnSc2	tuha
do	do	k7c2	do
tužek	tužka	k1gFnPc2	tužka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
sestávala	sestávat	k5eAaImAgFnS	sestávat
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
jemného	jemný	k2eAgInSc2d1	jemný
jílu	jíl	k1gInSc2	jíl
a	a	k8xC	a
grafitu	grafit	k1gInSc2	grafit
smíchaných	smíchaný	k2eAgFnPc2d1	smíchaná
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
poměru	poměr	k1gInSc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
znárodněna	znárodnit	k5eAaPmNgFnS	znárodnit
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
soukromých	soukromý	k2eAgFnPc2d1	soukromá
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
součástí	součást	k1gFnPc2	součást
skupiny	skupina	k1gFnSc2	skupina
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
holding	holding	k1gInSc1	holding
a.s.	a.s.	k?	a.s.
Společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
obchodní	obchodní	k2eAgFnPc4d1	obchodní
kapacity	kapacita	k1gFnPc4	kapacita
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
podnikatel	podnikatel	k1gMnSc1	podnikatel
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Bříza	Bříza	k1gMnSc1	Bříza
<g/>
.	.	kIx.	.
</s>
<s>
Pastelky	pastelka	k1gFnPc4	pastelka
Pastely	pastel	k1gInPc4	pastel
vodovky	vodovka	k1gFnSc2	vodovka
Obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
tužky	tužka	k1gFnSc2	tužka
Mikrotužky	Mikrotužka	k1gFnSc2	Mikrotužka
<g/>
,	,	kIx,	,
verzatilky	verzatilka	k1gFnSc2	verzatilka
Potřeby	potřeba	k1gFnSc2	potřeba
a	a	k8xC	a
nástroje	nástroj	k1gInPc4	nástroj
k	k	k7c3	k
rýsování	rýsování	k1gNnSc3	rýsování
a	a	k8xC	a
kreslení	kreslení	k1gNnSc3	kreslení
Kancelářské	kancelářský	k2eAgFnSc2d1	kancelářská
potřeby	potřeba	k1gFnSc2	potřeba
Společnost	společnost	k1gFnSc1	společnost
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
HARDTMUTH	HARDTMUTH	kA	HARDTMUTH
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
producentů	producent	k1gMnPc2	producent
a	a	k8xC	a
distributorů	distributor	k1gMnPc2	distributor
uměleckých	umělecký	k2eAgInPc2d1	umělecký
<g/>
,	,	kIx,	,
školních	školní	k2eAgInPc2d1	školní
a	a	k8xC	a
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
skupiny	skupina	k1gFnSc2	skupina
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
holding	holding	k1gInSc1	holding
a.s.	a.s.	k?	a.s.
Součástí	součást	k1gFnSc7	součást
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
závod	závod	k1gInSc4	závod
7	[number]	k4	7
Logarex	Logarex	k1gInSc4	Logarex
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
plastových	plastový	k2eAgNnPc2d1	plastové
pravítek	pravítko	k1gNnPc2	pravítko
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
logaritmických	logaritmický	k2eAgFnPc2d1	logaritmická
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
plastových	plastový	k2eAgFnPc2d1	plastová
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
založil	založit	k5eAaPmAgMnS	založit
Josef	Josef	k1gMnSc1	Josef
Hardtmuth	Hardtmuth	k1gMnSc1	Hardtmuth
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
grafitových	grafitový	k2eAgNnPc2d1	grafitové
jader	jádro	k1gNnPc2	jádro
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
Carlem	Carl	k1gMnSc7	Carl
Hardtmuthem	Hardtmuth	k1gInSc7	Hardtmuth
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Tužky	tužka	k1gFnPc1	tužka
KOH-I-NOOR	KOH-I-NOOR	k1gMnPc2	KOH-I-NOOR
slavily	slavit	k5eAaImAgFnP	slavit
postupně	postupně	k6eAd1	postupně
úspěchy	úspěch	k1gInPc4	úspěch
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
světových	světový	k2eAgFnPc6d1	světová
výstavách	výstava	k1gFnPc6	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
si	se	k3xPyFc3	se
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
přivezly	přivézt	k5eAaPmAgInP	přivézt
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
světových	světový	k2eAgFnPc2d1	světová
výstav	výstava	k1gFnPc2	výstava
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
a	a	k8xC	a
1925	[number]	k4	1925
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
nebo	nebo	k8xC	nebo
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
z	z	k7c2	z
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
KOH-I-NOOR	KOH-I-NOOR	k1gFnPc2	KOH-I-NOOR
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zaregistrována	zaregistrovat	k5eAaPmNgFnS	zaregistrovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
v	v	k7c6	v
73	[number]	k4	73
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
nabízí	nabízet	k5eAaImIp3nS	nabízet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
500	[number]	k4	500
různých	různý	k2eAgInPc2d1	různý
produktů	produkt	k1gInPc2	produkt
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
školní	školní	k2eAgNnSc4d1	školní
<g/>
,	,	kIx,	,
kancelářské	kancelářský	k2eAgNnSc4d1	kancelářské
<g/>
,	,	kIx,	,
technické	technický	k2eAgNnSc4d1	technické
a	a	k8xC	a
umělecké	umělecký	k2eAgNnSc4d1	umělecké
využití	využití	k1gNnSc4	využití
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jsou	být	k5eAaImIp3nP	být
prodávány	prodávat	k5eAaImNgFnP	prodávat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
65	[number]	k4	65
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
grafitových	grafitový	k2eAgFnPc2d1	grafitová
tuh	touha	k1gFnPc2	touha
přidal	přidat	k5eAaPmAgInS	přidat
kompletní	kompletní	k2eAgInSc1d1	kompletní
sortiment	sortiment	k1gInSc1	sortiment
zboží	zboží	k1gNnSc2	zboží
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
základních	základní	k2eAgFnPc6d1	základní
řadách	řada	k1gFnPc6	řada
<g/>
:	:	kIx,	:
umělecký	umělecký	k2eAgInSc4d1	umělecký
sortiment	sortiment	k1gInSc4	sortiment
(	(	kIx(	(
<g/>
ART	ART	kA	ART
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
školní	školní	k2eAgInSc1d1	školní
sortiment	sortiment	k1gInSc1	sortiment
(	(	kIx(	(
<g/>
SCHOOL	SCHOOL	kA	SCHOOL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
sortiment	sortiment	k1gInSc1	sortiment
(	(	kIx(	(
<g/>
OFFICE	Office	kA	Office
<g/>
)	)	kIx)	)
a	a	k8xC	a
sortiment	sortiment	k1gInSc1	sortiment
pro	pro	k7c4	pro
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
HOBBY	hobby	k1gNnSc2	hobby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
šíři	šíře	k1gFnSc4	šíře
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kategorií	kategorie	k1gFnPc2	kategorie
určují	určovat	k5eAaImIp3nP	určovat
potřeby	potřeba	k1gFnPc1	potřeba
vznikající	vznikající	k2eAgFnSc1d1	vznikající
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
HARDTMUTH	HARDTMUTH	kA	HARDTMUTH
a.s.	a.s.	k?	a.s.
neustále	neustále	k6eAd1	neustále
sleduje	sledovat	k5eAaImIp3nS	sledovat
vývojové	vývojový	k2eAgInPc4d1	vývojový
a	a	k8xC	a
módní	módní	k2eAgInPc4d1	módní
trendy	trend	k1gInPc4	trend
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
produktové	produktový	k2eAgFnSc2d1	produktová
řady	řada	k1gFnSc2	řada
neustále	neustále	k6eAd1	neustále
aktualizuje	aktualizovat	k5eAaBmIp3nS	aktualizovat
a	a	k8xC	a
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
podle	podle	k7c2	podle
nově	nově	k6eAd1	nově
vznikajících	vznikající	k2eAgInPc2d1	vznikající
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4500	[number]	k4	4500
druhů	druh	k1gInPc2	druh
zboží	zboží	k1gNnSc2	zboží
–	–	k?	–
nejen	nejen	k6eAd1	nejen
tužky	tužka	k1gFnSc2	tužka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uhly	uhel	k1gInPc1	uhel
<g/>
,	,	kIx,	,
rudky	rudka	k1gFnPc1	rudka
<g/>
,	,	kIx,	,
pastely	pastel	k1gInPc1	pastel
<g/>
,	,	kIx,	,
pastelky	pastelek	k1gInPc1	pastelek
<g/>
,	,	kIx,	,
křídy	křída	k1gFnPc1	křída
<g/>
,	,	kIx,	,
olejové	olejový	k2eAgFnPc1d1	olejová
<g/>
,	,	kIx,	,
temperové	temperový	k2eAgFnPc1d1	temperová
a	a	k8xC	a
vodové	vodový	k2eAgFnPc1d1	vodová
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
tuše	tuš	k1gInPc1	tuš
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
výběr	výběr	k1gInSc1	výběr
psacích	psací	k2eAgFnPc2d1	psací
a	a	k8xC	a
rýsovacích	rýsovací	k2eAgFnPc2d1	rýsovací
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
pryže	pryž	k1gFnSc2	pryž
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
holding	holding	k1gInSc1	holding
a.s.	a.s.	k?	a.s.
domácí	domácí	k2eAgFnSc2d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
akvizice	akvizice	k1gFnSc2	akvizice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
holdingu	holding	k1gInSc2	holding
byly	být	k5eAaImAgInP	být
začleněny	začleněn	k2eAgInPc1d1	začleněn
výrobní	výrobní	k2eAgInPc1d1	výrobní
závody	závod	k1gInPc1	závod
PONAS	PONAS	kA	PONAS
v	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
a	a	k8xC	a
bulharský	bulharský	k2eAgInSc1d1	bulharský
HEMUS	HEMUS	kA	HEMUS
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Burgasu	Burgas	k1gInSc6	Burgas
<g/>
.	.	kIx.	.
</s>
<s>
Nástrojárna	nástrojárna	k1gFnSc1	nástrojárna
Ponas	Ponasa	k1gFnPc2	Ponasa
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
PONAS	PONAS	kA	PONAS
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
výrobě	výroba	k1gFnSc3	výroba
vstřikovacích	vstřikovací	k2eAgFnPc2d1	vstřikovací
a	a	k8xC	a
vyfukovacích	vyfukovací	k2eAgFnPc2d1	vyfukovací
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
jsou	být	k5eAaImIp3nP	být
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
všechny	všechen	k3xTgFnPc1	všechen
dostupné	dostupný	k2eAgFnPc1d1	dostupná
moderní	moderní	k2eAgFnPc1d1	moderní
technologie	technologie	k1gFnPc1	technologie
–	–	k?	–
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
systémy	systém	k1gInPc4	systém
CAD-CAM	CAD-CAM	k1gMnPc2	CAD-CAM
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
moderní	moderní	k2eAgInPc1d1	moderní
CNC	CNC	kA	CNC
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
technologické	technologický	k2eAgInPc4d1	technologický
výlisky	výlisek	k1gInPc4	výlisek
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
elektrotechnického	elektrotechnický	k2eAgInSc2d1	elektrotechnický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
obalové	obalový	k2eAgFnSc2d1	obalová
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1	bulharský
závod	závod	k1gInSc1	závod
HEMUS	HEMUS	kA	HEMUS
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
HemusMark	HemusMark	k1gInSc1	HemusMark
AD	ad	k7c4	ad
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osmdesátiletou	osmdesátiletý	k2eAgFnSc4d1	osmdesátiletá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
školních	školní	k2eAgInPc2d1	školní
a	a	k8xC	a
kancelářských	kancelářský	k2eAgInPc2d1	kancelářský
popisovačů	popisovač	k1gInPc2	popisovač
<g/>
,	,	kIx,	,
značkovačů	značkovač	k1gInPc2	značkovač
a	a	k8xC	a
zvýrazňovačů	zvýrazňovač	k1gInPc2	zvýrazňovač
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
producentů	producent	k1gMnPc2	producent
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
zboží	zboží	k1gNnSc2	zboží
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
složeno	složen	k2eAgNnSc1d1	složeno
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Ing.	ing.	kA	ing.
Vlastislava	Vlastislav	k1gMnSc2	Vlastislav
Břízy	Bříza	k1gMnSc2	Bříza
<g/>
,	,	kIx,	,
místopředsedy	místopředseda	k1gMnSc2	místopředseda
představenstva	představenstvo	k1gNnSc2	představenstvo
PhDr.	PhDr.	kA	PhDr.
Ing.	ing.	kA	ing.
Vlastislava	Vlastislav	k1gMnSc2	Vlastislav
Břízy	Bříza	k1gMnSc2	Bříza
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc2	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
Ing.	ing.	kA	ing.
Lenky	Lenka	k1gFnSc2	Lenka
Drahošové	Drahošová	k1gFnSc2	Drahošová
<g/>
,	,	kIx,	,
předsedy	předseda	k1gMnSc2	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Ing.	ing.	kA	ing.
Davida	David	k1gMnSc2	David
Břízy	bříza	k1gFnSc2	bříza
<g/>
,	,	kIx,	,
členů	člen	k1gInPc2	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Dany	Dana	k1gFnSc2	Dana
Marešové	Marešová	k1gFnSc2	Marešová
a	a	k8xC	a
Ing.	ing.	kA	ing.
Marie	Maria	k1gFnSc2	Maria
Buštové	Buštová	k1gFnSc2	Buštová
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
HARDTMUTH	HARDTMUTH	kA	HARDTMUTH
a.s.	a.s.	k?	a.s.
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
skupiny	skupina	k1gFnSc2	skupina
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
holding	holding	k1gInSc1	holding
a.s.	a.s.	k?	a.s.
Narozen	narozen	k2eAgInSc1d1	narozen
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
majitelem	majitel	k1gMnSc7	majitel
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
tužkárny	tužkárna	k1gFnSc2	tužkárna
Koh-i-noor	Kohoor	k1gMnSc1	Koh-i-noor
Hadrtmuth	Hadrtmuth	k1gMnSc1	Hadrtmuth
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
i	i	k8xC	i
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
strojírenské	strojírenský	k2eAgNnSc1d1	strojírenské
inženýrství	inženýrství	k1gNnSc1	inženýrství
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
konstruktér	konstruktér	k1gMnSc1	konstruktér
<g/>
,	,	kIx,	,
šéfkonstruktér	šéfkonstruktér	k1gMnSc1	šéfkonstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
GAMA	gama	k1gNnSc2	gama
a.s.	a.s.	k?	a.s.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
také	také	k9	také
jako	jako	k9	jako
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Koh-i-noor	Kohoor	k1gMnSc1	Koh-i-noor
Hardtmuth	Hardtmuth	k1gMnSc1	Hardtmuth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
řídí	řídit	k5eAaImIp3nS	řídit
skupinu	skupina	k1gFnSc4	skupina
KOH-I-NOOR	KOH-I-NOOR	k1gFnPc2	KOH-I-NOOR
HOLDING	holding	k1gInSc4	holding
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
KOH-I-NOOR	KOH-I-NOOR	k?	KOH-I-NOOR
HOLDING	holding	k1gInSc1	holding
je	být	k5eAaImIp3nS	být
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
organizací	organizace	k1gFnSc7	organizace
ryze	ryze	k6eAd1	ryze
české	český	k2eAgFnSc2d1	Česká
holdingové	holdingový	k2eAgFnSc2d1	holdingová
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
celosvětovou	celosvětový	k2eAgFnSc7d1	celosvětová
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
výrobní	výrobní	k2eAgFnPc4d1	výrobní
kapacity	kapacita	k1gFnPc4	kapacita
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
nebo	nebo	k8xC	nebo
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Klastr	klastr	k1gInSc1	klastr
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2500	[number]	k4	2500
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
a	a	k8xC	a
ročním	roční	k2eAgInSc7d1	roční
obratem	obrat	k1gInSc7	obrat
přes	přes	k7c4	přes
4	[number]	k4	4
mld.	mld.	k?	mld.
se	se	k3xPyFc4	se
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
stal	stát	k5eAaPmAgInS	stát
významným	významný	k2eAgInSc7d1	významný
hnacím	hnací	k2eAgInSc7d1	hnací
motorem	motor	k1gInSc7	motor
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
řady	řada	k1gFnSc2	řada
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivně	intenzivně	k6eAd1	intenzivně
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
věnuje	věnovat	k5eAaPmIp3nS	věnovat
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
neustále	neustále	k6eAd1	neustále
inovuje	inovovat	k5eAaBmIp3nS	inovovat
technologický	technologický	k2eAgInSc1d1	technologický
park	park	k1gInSc1	park
a	a	k8xC	a
realizuje	realizovat	k5eAaBmIp3nS	realizovat
výrobní	výrobní	k2eAgInPc4d1	výrobní
procesy	proces	k1gInPc4	proces
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
minimalizování	minimalizování	k1gNnSc4	minimalizování
jejich	jejich	k3xOp3gInPc2	jejich
dopadů	dopad	k1gInPc2	dopad
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
filantropických	filantropický	k2eAgFnPc2d1	filantropická
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
přispívá	přispívat	k5eAaImIp3nS	přispívat
na	na	k7c6	na
renovaci	renovace	k1gFnSc6	renovace
historických	historický	k2eAgInPc2d1	historický
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc2d1	kulturní
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
podporuje	podporovat	k5eAaImIp3nS	podporovat
činnost	činnost	k1gFnSc4	činnost
nadací	nadace	k1gFnPc2	nadace
a	a	k8xC	a
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
značných	značný	k2eAgFnPc2d1	značná
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
strojového	strojový	k2eAgInSc2d1	strojový
parku	park	k1gInSc2	park
dbá	dbát	k5eAaImIp3nS	dbát
rovněž	rovněž	k9	rovněž
na	na	k7c4	na
vlastní	vlastní	k2eAgFnPc4d1	vlastní
inovace	inovace	k1gFnPc4	inovace
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
segmentu	segment	k1gInSc6	segment
vedle	vedle	k7c2	vedle
zřízení	zřízení	k1gNnSc2	zřízení
vlastního	vlastní	k2eAgNnSc2d1	vlastní
střediska	středisko	k1gNnSc2	středisko
znalostní	znalostní	k2eAgFnSc2d1	znalostní
inteligence	inteligence	k1gFnSc2	inteligence
navázala	navázat	k5eAaPmAgFnS	navázat
společnost	společnost	k1gFnSc1	společnost
intensivní	intensivní	k2eAgFnSc1d1	intensivní
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
několika	několik	k4yIc7	několik
českými	český	k2eAgFnPc7d1	Česká
univerzitami	univerzita	k1gFnPc7	univerzita
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
s	s	k7c7	s
TU	tu	k6eAd1	tu
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
participuje	participovat	k5eAaImIp3nS	participovat
na	na	k7c6	na
projektech	projekt	k1gInPc6	projekt
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgNnSc4d1	budoucí
využití	využití	k1gNnSc4	využití
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
v	v	k7c6	v
sériové	sériový	k2eAgFnSc6d1	sériová
produkci	produkce	k1gFnSc6	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
koncernu	koncern	k1gInSc2	koncern
je	být	k5eAaImIp3nS	být
strategicky	strategicky	k6eAd1	strategicky
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
hlavních	hlavní	k2eAgInPc6d1	hlavní
pilířích	pilíř	k1gInPc6	pilíř
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
jsou	být	k5eAaImIp3nP	být
spotřební	spotřební	k2eAgInSc1d1	spotřební
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc1	strojírenství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
a	a	k8xC	a
energetika	energetika	k1gFnSc1	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgFnSc7d1	dominantní
společností	společnost	k1gFnSc7	společnost
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
spotřebního	spotřební	k2eAgInSc2d1	spotřební
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
firem	firma	k1gFnPc2	firma
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
KOH-I-NOOR	KOH-I-NOOR	k1gMnPc2	KOH-I-NOOR
HARDTMUTH	HARDTMUTH	kA	HARDTMUTH
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
dvousetletou	dvousetletý	k2eAgFnSc7d1	dvousetletá
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
výrobní	výrobní	k2eAgFnPc4d1	výrobní
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc4d1	obchodní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Sortiment	sortiment	k1gInSc1	sortiment
společnosti	společnost	k1gFnSc2	společnost
čítá	čítat	k5eAaImIp3nS	čítat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgInPc4	čtyři
tisíce	tisíc	k4xCgInPc4	tisíc
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
se	s	k7c7	s
základním	základní	k2eAgNnSc7d1	základní
členěním	členění	k1gNnSc7	členění
na	na	k7c4	na
umělecký	umělecký	k2eAgInSc4d1	umělecký
<g/>
,	,	kIx,	,
školní	školní	k2eAgInSc4d1	školní
<g/>
,	,	kIx,	,
kancelářský	kancelářský	k2eAgInSc4d1	kancelářský
a	a	k8xC	a
hobby	hobby	k1gNnSc4	hobby
sortiment	sortiment	k1gInSc1	sortiment
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
divizi	divize	k1gFnSc4	divize
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
další	další	k2eAgMnSc1d1	další
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
koncernu	koncern	k1gInSc2	koncern
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
GAMA	gama	k1gNnSc2	gama
GROUP	GROUP	kA	GROUP
podnikající	podnikající	k2eAgFnSc1d1	podnikající
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
tváření	tváření	k1gNnSc2	tváření
plastů	plast	k1gInPc2	plast
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
<g/>
,	,	kIx,	,
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
a	a	k8xC	a
veterinárních	veterinární	k2eAgFnPc2d1	veterinární
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
segment	segment	k1gInSc1	segment
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
společnostmi	společnost	k1gFnPc7	společnost
KOH-I-NOOR	KOH-I-NOOR	k1gMnSc2	KOH-I-NOOR
PONAS	PONAS	kA	PONAS
<g/>
,	,	kIx,	,
KOH-I-NOOR	KOH-I-NOOR	k1gFnPc1	KOH-I-NOOR
RONAS	RONAS	kA	RONAS
a	a	k8xC	a
KOH-I-NOOR	KOH-I-NOOR	k1gFnSc2	KOH-I-NOOR
FORMEX	FORMEX	kA	FORMEX
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc7	jejich
hlavními	hlavní	k2eAgFnPc7d1	hlavní
činnostmi	činnost	k1gFnPc7	činnost
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k6eAd1	zejména
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
vstřikovaných	vstřikovaný	k2eAgInPc2d1	vstřikovaný
dílců	dílec	k1gInPc2	dílec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
segmentu	segment	k1gInSc6	segment
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
co	co	k9	co
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
know-how	knowow	k?	know-how
mezi	mezi	k7c4	mezi
středoevropskou	středoevropský	k2eAgFnSc4d1	středoevropská
špičku	špička	k1gFnSc4	špička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
energetiky	energetika	k1gFnSc2	energetika
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zástupcem	zástupce	k1gMnSc7	zástupce
koncernu	koncern	k1gInSc2	koncern
společnost	společnost	k1gFnSc1	společnost
KRALUPOL	KRALUPOL	kA	KRALUPOL
specializující	specializující	k2eAgInPc4d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
distribuci	distribuce	k1gFnSc4	distribuce
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
ekologickými	ekologický	k2eAgNnPc7d1	ekologické
palivy	palivo	k1gNnPc7	palivo
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
zkapalněnými	zkapalněný	k2eAgInPc7d1	zkapalněný
plyny	plyn	k1gInPc7	plyn
LPG	LPG	kA	LPG
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
odběratelem	odběratel	k1gMnSc7	odběratel
LPG	LPG	kA	LPG
plynů	plyn	k1gInPc2	plyn
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
v	v	k7c6	v
tuzemských	tuzemský	k2eAgFnPc6d1	tuzemská
rafinériích	rafinérie	k1gFnPc6	rafinérie
v	v	k7c6	v
Litvínově	Litvínov	k1gInSc6	Litvínov
a	a	k8xC	a
v	v	k7c6	v
Kralupech	Kralupy	k1gInPc6	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Koh-i-noor	Kohoora	k1gFnPc2	Koh-i-noora
Hardtmuth	Hardtmutha	k1gFnPc2	Hardtmutha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Koh-i-noor	Kohoor	k1gMnSc1	Koh-i-noor
Hardtmuth	Hardtmuth	k1gMnSc1	Hardtmuth
Výrobky	výrobek	k1gInPc1	výrobek
Logarex	Logarex	k1gInSc1	Logarex
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
http://sliderulemuseum.com/	[url]	k?	http://sliderulemuseum.com/
</s>
