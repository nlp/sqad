<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
peří	peří	k1gNnSc2	peří
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
metabolismus	metabolismus	k1gInSc1	metabolismus
ptáků	pták	k1gMnPc2	pták
až	až	k9	až
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jak	jak	k6eAd1	jak
zvýšenými	zvýšený	k2eAgInPc7d1	zvýšený
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
výživu	výživa	k1gFnSc4	výživa
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
vnímavostí	vnímavost	k1gFnSc7	vnímavost
ke	k	k7c3	k
stresorům	stresor	k1gMnPc3	stresor
<g/>
.	.	kIx.	.
</s>
