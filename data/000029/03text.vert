<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
či	či	k8xC	či
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
každoročně	každoročně	k6eAd1	každoročně
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
květen	květen	k1gInSc1	květen
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
jako	jako	k9	jako
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyškrtnutí	vyškrtnutí	k1gNnSc3	vyškrtnutí
homosexuality	homosexualita	k1gFnSc2	homosexualita
z	z	k7c2	z
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
klasifikace	klasifikace	k1gFnSc2	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
IDAHO	IDAHO	kA	IDAHO
je	být	k5eAaImIp3nS	být
akcentace	akcentace	k1gFnSc1	akcentace
respektování	respektování	k1gNnSc2	respektování
práv	právo	k1gNnPc2	právo
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
než	než	k8xS	než
heterosexuální	heterosexuální	k2eAgFnSc7d1	heterosexuální
orientací	orientace	k1gFnSc7	orientace
a	a	k8xC	a
ukončení	ukončení	k1gNnSc1	ukončení
diskriminace	diskriminace	k1gFnSc2	diskriminace
a	a	k8xC	a
násilí	násilí	k1gNnSc4	násilí
pramenící	pramenící	k2eAgNnSc4d1	pramenící
z	z	k7c2	z
homofobie	homofobie	k1gFnSc2	homofobie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svátek	svátek	k1gInSc4	svátek
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
název	název	k1gInSc1	název
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc4	den
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
a	a	k8xC	a
transfobii	transfobie	k1gFnSc3	transfobie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
pojmenování	pojmenování	k1gNnSc2	pojmenování
International	International	k1gFnSc1	International
Day	Day	k1gFnSc1	Day
Against	Against	k1gFnSc1	Against
Homophobia	Homophobia	k1gFnSc1	Homophobia
(	(	kIx(	(
<g/>
and	and	k?	and
Transphobia	Transphobia	k1gFnSc1	Transphobia
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
zkratka	zkratka	k1gFnSc1	zkratka
IDAHO	IDAHO	kA	IDAHO
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
úrovni	úroveň	k1gFnSc6	úroveň
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
Den	den	k1gInSc4	den
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Iniciátorem	iniciátor	k1gMnSc7	iniciátor
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
francouzský	francouzský	k2eAgMnSc1d1	francouzský
akademik	akademik	k1gMnSc1	akademik
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
Louis-Georges	Louis-Georgesa	k1gFnPc2	Louis-Georgesa
Tin	Tina	k1gFnPc2	Tina
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
koordinačního	koordinační	k2eAgInSc2d1	koordinační
výboru	výbor	k1gInSc2	výbor
IDAHO	IDAHO	kA	IDAHO
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
akce	akce	k1gFnPc1	akce
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
dni	den	k1gInSc3	den
ve	v	k7c6	v
40	[number]	k4	40
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
institucionální	institucionální	k2eAgFnSc6d1	institucionální
úrovni	úroveň	k1gFnSc6	úroveň
jej	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
hlasováním	hlasování	k1gNnSc7	hlasování
parlamentu	parlament	k1gInSc2	parlament
uznala	uznat	k5eAaPmAgFnS	uznat
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgFnP	následovat
další	další	k2eAgFnPc4d1	další
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
přijal	přijmout	k5eAaPmAgInS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c6	o
homofobii	homofobie	k1gFnSc6	homofobie
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zmínil	zmínit	k5eAaPmAgMnS	zmínit
i	i	k9	i
IDAHO	IDAHO	kA	IDAHO
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
usnesení	usnesení	k1gNnSc6	usnesení
schválil	schválit	k5eAaPmAgInS	schválit
každoroční	každoroční	k2eAgNnSc4d1	každoroční
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
za	za	k7c4	za
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgMnS	vydat
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
Terry	Terra	k1gFnSc2	Terra
Davis	Davis	k1gInSc1	Davis
rezoluci	rezoluce	k1gFnSc4	rezoluce
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
IDAHO	IDAHO	kA	IDAHO
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
podpořili	podpořit	k5eAaPmAgMnP	podpořit
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
prohlášeními	prohlášení	k1gNnPc7	prohlášení
mj.	mj.	kA	mj.
vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
představitelé	představitel	k1gMnPc1	představitel
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
Herman	Herman	k1gMnSc1	Herman
Van	vana	k1gFnPc2	vana
Rompuy	Rompua	k1gFnSc2	Rompua
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
Jerzy	Jerza	k1gFnSc2	Jerza
Buzek	Buzek	k1gInSc1	Buzek
a	a	k8xC	a
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
Viviane	Vivian	k1gMnSc5	Vivian
Redingová	Redingový	k2eAgFnSc1d1	Redingová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
uspořádán	uspořádán	k2eAgInSc4d1	uspořádán
happening	happening	k1gInSc4	happening
Odhalení	odhalení	k1gNnSc2	odhalení
duhy	duha	k1gFnSc2	duha
(	(	kIx(	(
<g/>
Rainbow	Rainbow	k1gMnSc1	Rainbow
Flash	Flash	k1gMnSc1	Flash
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
Běh	běh	k1gInSc1	běh
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
a	a	k8xC	a
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
projekce	projekce	k1gFnSc1	projekce
dokumentu	dokument	k1gInSc2	dokument
S	s	k7c7	s
důvěrou	důvěra	k1gFnSc7	důvěra
a	a	k8xC	a
láskou	láska	k1gFnSc7	láska
s	s	k7c7	s
udělením	udělení	k1gNnSc7	udělení
Ceny	cena	k1gFnSc2	cena
Colour	Coloura	k1gFnPc2	Coloura
Planet	planeta	k1gFnPc2	planeta
za	za	k7c4	za
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
přínos	přínos	k1gInSc4	přínos
LGBT	LGBT	kA	LGBT
minoritě	minorita	k1gFnSc6	minorita
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgMnS	získat
ji	on	k3xPp3gFnSc4	on
Jiří	Jiří	k1gMnSc1	Jiří
Hromada	Hromada	k1gMnSc1	Hromada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
LGBT	LGBT	kA	LGBT
portál	portál	k1gInSc4	portál
Colour	Coloura	k1gFnPc2	Coloura
Planet	planeta	k1gFnPc2	planeta
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
opět	opět	k6eAd1	opět
večer	večer	k6eAd1	večer
s	s	k7c7	s
udělením	udělení	k1gNnSc7	udělení
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
tentokrát	tentokrát	k6eAd1	tentokrát
odnesla	odnést	k5eAaPmAgFnS	odnést
Marcela	Marcela	k1gFnSc1	Marcela
Šulcová	Šulcová	k1gFnSc1	Šulcová
<g/>
,	,	kIx,	,
průkopnice	průkopnice	k1gFnSc1	průkopnice
novodobého	novodobý	k2eAgNnSc2d1	novodobé
českého	český	k2eAgNnSc2d1	české
lesbického	lesbický	k2eAgNnSc2d1	lesbické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgFnPc1d3	veliký
akce	akce	k1gFnPc1	akce
<g/>
:	:	kIx,	:
kromě	kromě	k7c2	kromě
tradičního	tradiční	k2eAgInSc2d1	tradiční
Běhu	běh	k1gInSc2	běh
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
udělení	udělení	k1gNnSc1	udělení
Cen	cena	k1gFnPc2	cena
Colour	Coloura	k1gFnPc2	Coloura
Planet	planeta	k1gFnPc2	planeta
při	při	k7c6	při
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
večeru	večer	k1gInSc6	večer
festivalu	festival	k1gInSc6	festival
eLnadruhou	eLnadruha	k1gFnSc7	eLnadruha
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
nově	nově	k6eAd1	nově
také	také	k9	také
veřejný	veřejný	k2eAgInSc4d1	veřejný
happening	happening	k1gInSc4	happening
Pohřeb	pohřeb	k1gInSc1	pohřeb
homofobie	homofobie	k1gFnSc2	homofobie
a	a	k8xC	a
vypuštění	vypuštění	k1gNnSc4	vypuštění
duhových	duhový	k2eAgInPc2d1	duhový
balónků	balónek	k1gInPc2	balónek
se	se	k3xPyFc4	se
vzkazy	vzkaz	k1gInPc1	vzkaz
Rainbow	Rainbow	k1gFnPc2	Rainbow
Flash	Flasha	k1gFnPc2	Flasha
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc1	všechen
17	[number]	k4	17
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
konference	konference	k1gFnPc1	konference
Hate	Hat	k1gInSc2	Hat
crimes	crimes	k1gInSc1	crimes
<g/>
/	/	kIx~	/
<g/>
Hate	Hate	k1gFnSc1	Hate
speech	speech	k1gInSc4	speech
proti	proti	k7c3	proti
LGBTQ	LGBTQ	kA	LGBTQ
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
