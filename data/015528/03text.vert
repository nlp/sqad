<s>
Volkswagen	volkswagen	k1gInSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
VW	VW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnPc1
Voskovec	Voskovec	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Werich	Werich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Logo	logo	k1gNnSc4
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1937	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
státní	státní	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Wolfsburg	Wolfsburg	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Wolfsburg	Wolfsburg	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc4
Klíčoví	klíčový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
</s>
<s>
Herbert	Herbert	k1gInSc1
Diess	Diess	k1gInSc1
(	(	kIx(
<g/>
CEO	CEO	kA
<g/>
)	)	kIx)
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Automobilový	automobilový	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
Produkty	produkt	k1gInPc1
</s>
<s>
Automobily	automobil	k1gInPc1
Obrat	obrat	k5eAaPmF
</s>
<s>
202,5	202,5	k4
mld.	mld.	k?
Euro	euro	k1gNnSc4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Ig	Ig	k?
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
www.volkswagen.cz	www.volkswagen.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
VW	VW	kA
Brouk	brouk	k1gMnSc1
</s>
<s>
VW	VW	kA
Transporter	Transporter	k1gMnSc1
</s>
<s>
VW	VW	kA
K70	K70	k1gFnSc1
</s>
<s>
Vůz	vůz	k1gInSc1
VW	VW	kA
Caddy	Cadd	k1gInPc1
(	(	kIx(
<g/>
v	v	k7c6
popředí	popředí	k1gNnSc6
<g/>
)	)	kIx)
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
VW	VW	kA
LT	LT	kA
35	#num#	k4
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
AG	AG	kA
nebo	nebo	k8xC
jen	jen	k9
VW	VW	kA
je	být	k5eAaImIp3nS
německá	německý	k2eAgFnSc1d1
automobilka	automobilka	k1gFnSc1
sídlící	sídlící	k2eAgFnSc1d1
ve	v	k7c6
Wolfsburgu	Wolfsburg	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
skupiny	skupina	k1gFnSc2
Volkswagen	volkswagen	k1gInSc1
Group	Group	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
název	název	k1gInSc4
v	v	k7c6
překladu	překlad	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
lidový	lidový	k2eAgInSc4d1
vůz	vůz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
jako	jako	k8xC,k8xS
Gesellschaft	Gesellschaft	k2eAgInSc1d1
zur	zur	k?
Vorbereitung	Vorbereitung	k1gInSc1
des	des	k1gNnSc1
Deutschen	Deutschna	k1gFnPc2
Volkswagens	Volkswagensa	k1gFnPc2
mbH	mbH	k?
a	a	k8xC
později	pozdě	k6eAd2
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Volkswagenwerk	Volkswagenwerk	k1gInSc4
GmbH	GmbH	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založení	založení	k1gNnSc1
automobilky	automobilka	k1gFnPc1
v	v	k7c6
nacistickém	nacistický	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
prosazoval	prosazovat	k5eAaImAgMnS
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Volkswagen	volkswagen	k1gInSc1
AG	AG	kA
(	(	kIx(
<g/>
Volkswagen	volkswagen	k1gInSc1
=	=	kIx~
německy	německy	k6eAd1
lidový	lidový	k2eAgInSc4d1
vůz	vůz	k1gInSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideu	idea	k1gFnSc4
„	„	k?
<g/>
lidového	lidový	k2eAgInSc2d1
vozu	vůz	k1gInSc2
<g/>
“	“	k?
prosazoval	prosazovat	k5eAaImAgMnS
především	především	k9
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1934	#num#	k4
při	při	k7c6
zahájení	zahájení	k1gNnSc6
24	#num#	k4
<g/>
.	.	kIx.
mezinárodní	mezinárodní	k2eAgFnSc2d1
automobilové	automobilový	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
(	(	kIx(
<g/>
Internationale	Internationale	k1gMnSc1
Automobil-Ausstellung	Automobil-Ausstellung	k1gMnSc1
<g/>
,	,	kIx,
IAA	IAA	kA
<g/>
)	)	kIx)
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vyrábět	vyrábět	k5eAaImF
vozy	vůz	k1gInPc4
pro	pro	k7c4
široké	široký	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mělo	mít	k5eAaImAgNnS
jít	jít	k5eAaImF
o	o	k7c4
automobil	automobil	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
při	při	k7c6
jízdě	jízda	k1gFnSc6
na	na	k7c6
německých	německý	k2eAgFnPc6d1
dálnicích	dálnice	k1gFnPc6
dosahoval	dosahovat	k5eAaImAgInS
cestovní	cestovní	k2eAgFnPc4d1
rychlosti	rychlost	k1gFnPc4
100	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
čtyři	čtyři	k4xCgNnPc1
sedadla	sedadlo	k1gNnPc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
by	by	kYmCp3nS
úsporný	úsporný	k2eAgInSc1d1
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
spotřeby	spotřeba	k1gFnPc1
paliva	palivo	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
cena	cena	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
1000	#num#	k4
RM	RM	kA
<g/>
.	.	kIx.
</s>
<s>
Konstrukcí	konstrukce	k1gFnSc7
takového	takový	k3xDgInSc2
automobilu	automobil	k1gInSc2
pověřil	pověřit	k5eAaPmAgMnS
Hitler	Hitler	k1gMnSc1
rodáka	rodák	k1gMnSc2
z	z	k7c2
Vratislavic	Vratislavice	k1gFnPc2
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Liberce	Liberec	k1gInSc2
<g/>
)	)	kIx)
Ferdinanda	Ferdinand	k1gMnSc2
Porscheho	Porsche	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
také	také	k9
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ten	ten	k3xDgMnSc1
už	už	k6eAd1
se	se	k3xPyFc4
takovou	takový	k3xDgFnSc7
koncepcí	koncepce	k1gFnSc7
několik	několik	k4yIc4
let	léto	k1gNnPc2
zabýval	zabývat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověření	pověření	k1gNnPc1
k	k	k7c3
výstavbě	výstavba	k1gFnSc3
automobilky	automobilka	k1gFnSc2
obdržel	obdržet	k5eAaPmAgMnS
Ferdinand	Ferdinand	k1gMnSc1
Porsche	Porsche	k1gNnSc2
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
nejasný	jasný	k2eNgInSc1d1
způsob	způsob	k1gInSc1
financování	financování	k1gNnSc2
celý	celý	k2eAgInSc1d1
projekt	projekt	k1gInSc1
pozdržel	pozdržet	k5eAaPmAgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
Gesellschaft	Gesellschaft	k1gInSc1
zur	zur	k?
Vorbereitung	Vorbereitung	k1gInSc1
des	des	k1gNnSc2
Deutschen	Deutschen	k2eAgInSc1d1
Volkswagens	Volkswagens	k1gInSc1
mbH	mbH	k?
(	(	kIx(
<g/>
Společnost	společnost	k1gFnSc1
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
německého	německý	k2eAgInSc2d1
lidového	lidový	k2eAgInSc2d1
vozu	vůz	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Volkswagenwerk	Volkswagenwerk	k1gInSc4
GmbH	GmbH	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
britského	britský	k2eAgMnSc2d1
historika	historik	k1gMnSc2
Richarda	Richard	k1gMnSc2
J.	J.	kA
Evanse	Evanse	k1gFnSc2
<g/>
,	,	kIx,
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
auta	auto	k1gNnPc4
(	(	kIx(
<g/>
avšak	avšak	k8xC
nikdy	nikdy	k6eAd1
žádné	žádný	k3yNgFnPc4
neřídil	řídit	k5eNaImAgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osobně	osobně	k6eAd1
navrhl	navrhnout	k5eAaPmAgMnS
charakteristický	charakteristický	k2eAgInSc4d1
zaoblený	zaoblený	k2eAgInSc4d1
tvar	tvar	k1gInSc4
budoucího	budoucí	k2eAgInSc2d1
vozu	vůz	k1gInSc2
<g/>
,	,	kIx,
inspirovaný	inspirovaný	k2eAgInSc1d1
designem	design	k1gInSc7
českých	český	k2eAgInPc2d1
vozů	vůz	k1gInPc2
Tatra	Tatra	k1gFnSc1
konstruktéra	konstruktér	k1gMnSc2
Hanse	Hans	k1gMnSc2
Ledwinky	Ledwinka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
výraznou	výrazný	k2eAgFnSc4d1
inspiraci	inspirace	k1gFnSc4
vozem	vůz	k1gInSc7
Tatra	Tatra	k1gFnSc1
ovšem	ovšem	k9
Volkswagen	volkswagen	k1gInSc1
uznal	uznat	k5eAaPmAgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
jednorázovým	jednorázový	k2eAgNnSc7d1
vyplacením	vyplacení	k1gNnSc7
odškodného	odškodné	k1gNnSc2
československému	československý	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
město	město	k1gNnSc1
s	s	k7c7
názvem	název	k1gInSc7
Stadt	Stadt	k1gInSc1
des	des	k1gNnSc1
KdF-Wagens	KdF-Wagens	k1gInSc1
bei	bei	k?
Fallersleben	Fallersleben	k2eAgInSc1d1
(	(	kIx(
<g/>
Město	město	k1gNnSc1
vozů	vůz	k1gInPc2
KdF	KdF	k1gFnSc2
u	u	k7c2
Fallerslebenu	Fallersleben	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
plánována	plánovat	k5eAaImNgFnS
výroba	výroba	k1gFnSc1
lidového	lidový	k2eAgInSc2d1
vozu	vůz	k1gInSc2
pod	pod	k7c7
značkou	značka	k1gFnSc7
KdF	KdF	k1gFnSc2
(	(	kIx(
<g/>
Kraft	Kraft	k2eAgInSc1d1
durch	durch	k1gInSc1
Freude	Freud	k1gInSc5
<g/>
,	,	kIx,
tj.	tj.	kA
Síla	síla	k1gFnSc1
skrze	skrze	k?
radost	radost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
začátku	začátek	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
společnost	společnost	k1gFnSc1
musela	muset	k5eAaImAgFnS
přeorientovat	přeorientovat	k5eAaPmF
na	na	k7c4
zbrojní	zbrojní	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závod	závod	k1gInSc1
přebral	přebrat	k5eAaPmAgInS
opravárenské	opravárenský	k2eAgFnPc4d1
práce	práce	k1gFnPc4
na	na	k7c6
letadlech	letadlo	k1gNnPc6
Junkers	Junkersa	k1gFnPc2
Ju	ju	k0
88	#num#	k4
a	a	k8xC
začal	začít	k5eAaPmAgInS
vyrábět	vyrábět	k5eAaImF
vojenské	vojenský	k2eAgInPc4d1
terénní	terénní	k2eAgInPc4d1
a	a	k8xC
obojživelné	obojživelný	k2eAgInPc4d1
vozy	vůz	k1gInPc4
KdF	KdF	k1gFnSc2
82	#num#	k4
a	a	k8xC
KdF	KdF	k1gMnSc1
166	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
byl	být	k5eAaImAgInS
s	s	k7c7
přehledem	přehled	k1gInSc7
největším	veliký	k2eAgInSc7d3
výrobcem	výrobce	k1gMnSc7
osobních	osobní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
tvořili	tvořit	k5eAaImAgMnP
dvě	dva	k4xCgFnPc4
třetiny	třetina	k1gFnPc4
pracujících	pracující	k1gMnPc2
v	v	k7c6
závodě	závod	k1gInSc6
lidé	člověk	k1gMnPc1
na	na	k7c6
nucených	nucený	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
Wolfsburg	Wolfsburg	k1gInSc4
a	a	k8xC
vyráběné	vyráběný	k2eAgInPc1d1
automobily	automobil	k1gInPc1
i	i	k8xC
celá	celý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
dostaly	dostat	k5eAaPmAgFnP
nové	nový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Volkswagen	volkswagen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
osobních	osobní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
podle	podle	k7c2
původních	původní	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
pod	pod	k7c7
britským	britský	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1946-1948	1946-1948	k4
dosáhl	dosáhnout	k5eAaPmAgInS
objem	objem	k1gInSc1
produkce	produkce	k1gFnSc2
1	#num#	k4
000	#num#	k4
automobilů	automobil	k1gInPc2
měsíčně	měsíčně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1950	#num#	k4
zahájil	zahájit	k5eAaPmAgInS
Volkswagen	volkswagen	k1gInSc1
výrobu	výrob	k1gInSc2
užitkového	užitkový	k2eAgInSc2d1
modelu	model	k1gInSc2
Transporter	Transportra	k1gFnPc2
(	(	kIx(
<g/>
interní	interní	k2eAgFnSc1d1
označení	označení	k1gNnSc4
typ	typ	k1gInSc1
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
nejslavnějších	slavný	k2eAgInPc2d3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
dalších	další	k2eAgFnPc6d1
generacích	generace	k1gFnPc6
stále	stále	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zahájil	zahájit	k5eAaPmAgInS
Volkswagen	volkswagen	k1gInSc1
expanzi	expanze	k1gFnSc4
do	do	k7c2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
a	a	k8xC
zahájen	zahájit	k5eAaPmNgInS
prodej	prodej	k1gInSc1
automobilů	automobil	k1gInPc2
VW	VW	kA
na	na	k7c6
všech	všecek	k3xTgInPc6
kontinentech	kontinent	k1gInPc6
včetně	včetně	k7c2
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
pokračoval	pokračovat	k5eAaImAgInS
Volkswagen	volkswagen	k1gInSc1
v	v	k7c6
expanzi	expanze	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
zažil	zažít	k5eAaPmAgMnS
i	i	k9
několik	několik	k4yIc4
odbytových	odbytový	k2eAgFnPc2d1
krizí	krize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
rozšířil	rozšířit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
skrovnou	skrovný	k2eAgFnSc4d1
typovou	typový	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
<g/>
,	,	kIx,
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
výrobu	výroba	k1gFnSc4
modelu	model	k1gInSc2
1500	#num#	k4
(	(	kIx(
<g/>
typ	typ	k1gInSc1
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
chtěl	chtít	k5eAaImAgMnS
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
byl	být	k5eAaImAgInS
technicky	technicky	k6eAd1
obdobný	obdobný	k2eAgInSc1d1
řadě	řada	k1gFnSc3
Brouk	brouk	k1gMnSc1
(	(	kIx(
<g/>
typ	typ	k1gInSc1
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
pontonová	pontonový	k2eAgFnSc1d1
karoserie	karoserie	k1gFnSc1
poskytovala	poskytovat	k5eAaImAgFnS
trochu	trochu	k6eAd1
více	hodně	k6eAd2
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgInSc4d1
zavazadlový	zavazadlový	k2eAgInSc4d1
prostor	prostor	k1gInSc4
vhodnějšího	vhodný	k2eAgInSc2d2
tvaru	tvar	k1gInSc2
byl	být	k5eAaImAgInS
větší	veliký	k2eAgInSc1d2
a	a	k8xC
do	do	k7c2
zadního	zadní	k2eAgMnSc2d1
byl	být	k5eAaImAgMnS
přístup	přístup	k1gInSc4
zvenku	zvenku	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízel	nabízet	k5eAaImAgMnS
se	se	k3xPyFc4
s	s	k7c7
karoserií	karoserie	k1gFnSc7
tudor	tudor	k1gInSc1
<g/>
,	,	kIx,
dvoudveřový	dvoudveřový	k2eAgInSc1d1
fastback	fastback	k1gInSc1
nebo	nebo	k8xC
třídveřové	třídveřový	k2eAgNnSc1d1
kombi	kombi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
se	se	k3xPyFc4
Volkswagen	volkswagen	k1gInSc1
spojil	spojit	k5eAaPmAgInS
s	s	k7c7
Auto	auto	k1gNnSc1
Union	union	k1gInSc4
GmbH	GmbH	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
postupně	postupně	k6eAd1
získal	získat	k5eAaPmAgInS
téměř	téměř	k6eAd1
100	#num#	k4
%	%	kIx~
podíl	podíl	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
posílil	posílit	k5eAaPmAgInS
výrobní	výrobní	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
výrobu	výroba	k1gFnSc4
většího	veliký	k2eAgInSc2d2
modelu	model	k1gInSc2
411	#num#	k4
(	(	kIx(
<g/>
typ	typ	k1gInSc1
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
předchozích	předchozí	k2eAgInPc2d1
se	se	k3xPyFc4
lišil	lišit	k5eAaImAgInS
samonosnou	samonosný	k2eAgFnSc7d1
karoserií	karoserie	k1gFnSc7
<g/>
,	,	kIx,
silnější	silný	k2eAgFnSc7d2
pohonnou	pohonný	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
<g/>
,	,	kIx,
nabídkou	nabídka	k1gFnSc7
automatické	automatický	k2eAgFnSc2d1
převodovky	převodovka	k1gFnSc2
a	a	k8xC
elektronického	elektronický	k2eAgNnSc2d1
vstřikování	vstřikování	k1gNnSc2
benzínu	benzín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
ale	ale	k9
také	také	k9
vzduchem	vzduch	k1gInSc7
chlazený	chlazený	k2eAgInSc4d1
motor	motor	k1gInSc4
vzadu	vzadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízel	nabízet	k5eAaImAgMnS
se	se	k3xPyFc4
s	s	k7c7
karoserií	karoserie	k1gFnSc7
dvoudveřový	dvoudveřový	k2eAgMnSc1d1
nebo	nebo	k8xC
čtyřdveřový	čtyřdveřový	k2eAgInSc1d1
fastback	fastback	k1gInSc1
a	a	k8xC
třídveřové	třídveřový	k2eAgNnSc1d1
kombi	kombi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1962	#num#	k4
Volkswagen	volkswagen	k1gInSc1
pod	pod	k7c7
interním	interní	k2eAgNnSc7d1
označením	označení	k1gNnSc7
EA	EA	kA
128	#num#	k4
připravoval	připravovat	k5eAaImAgInS
ještě	ještě	k9
větší	veliký	k2eAgInSc1d2
model	model	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
sedan	sedan	k1gInSc1
pro	pro	k7c4
americký	americký	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
měl	mít	k5eAaImAgInS
pohánět	pohánět	k5eAaImF
vzduchem	vzduch	k1gInSc7
chlazený	chlazený	k2eAgInSc4d1
šestiválcový	šestiválcový	k2eAgInSc4d1
motor	motor	k1gInSc4
Porsche	Porsche	k1gNnSc4
911	#num#	k4
objemu	objem	k1gInSc2
2,0	2,0	k4
litru	litr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
hnací	hnací	k2eAgNnSc4d1
ústrojí	ústrojí	k1gNnSc4
a	a	k8xC
nepopularita	nepopularita	k1gFnSc1
vozů	vůz	k1gInPc2
s	s	k7c7
motorem	motor	k1gInSc7
vzadu	vzadu	k6eAd1
po	po	k7c6
vydání	vydání	k1gNnSc6
knihy	kniha	k1gFnSc2
Ralpha	Ralph	k1gMnSc2
Nadera	nadrat	k5eAaBmSgInS
Unsafe	Unsaf	k1gInSc5
at	at	k?
Any	Any	k1gMnSc1
Speed	Speed	k1gMnSc1
(	(	kIx(
<g/>
Nebezpečný	bezpečný	k2eNgMnSc1d1
při	při	k7c6
jakékoli	jakýkoli	k3yIgFnSc6
rychlosti	rychlost	k1gFnSc6
<g/>
)	)	kIx)
ale	ale	k8xC
zastavily	zastavit	k5eAaPmAgFnP
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
vývoj	vývoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgInPc4d1
luxusní	luxusní	k2eAgInPc4d1
sedany	sedan	k1gInPc4
s	s	k7c7
pohonem	pohon	k1gInSc7
předních	přední	k2eAgFnPc2d1
kol	kola	k1gFnPc2
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
koncernu	koncern	k1gInSc6
vyrábět	vyrábět	k5eAaImF
Audi	Audi	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
Volkswagen	volkswagen	k1gInSc1
převzal	převzít	k5eAaPmAgInS
NSU	NSU	kA
s	s	k7c7
tehdy	tehdy	k6eAd1
pespektivním	pespektivní	k2eAgInSc7d1
Wankelovým	Wankelův	k2eAgInSc7d1
motorem	motor	k1gInSc7
<g/>
,	,	kIx,
sloučil	sloučit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
s	s	k7c7
Auto	auto	k1gNnSc4
Union	union	k1gInSc1
a	a	k8xC
celek	celek	k1gInSc1
přejmenoval	přejmenovat	k5eAaPmAgInS
na	na	k7c4
Audi	Audi	k1gNnSc4
NSU	NSU	kA
Auto	auto	k1gNnSc4
Union	union	k1gInSc1
AG	AG	kA
<g/>
.	.	kIx.
</s>
<s>
70	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
přinesla	přinést	k5eAaPmAgFnS
zásadní	zásadní	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
modelové	modelový	k2eAgFnSc6d1
politice	politika	k1gFnSc6
VW	VW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typ	typ	k1gInSc1
3	#num#	k4
a	a	k8xC
typ	typ	k1gInSc1
4	#num#	k4
se	se	k3xPyFc4
prodávaly	prodávat	k5eAaImAgInP
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
než	než	k8xS
Brouk	brouk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc1
prodej	prodej	k1gInSc1
coby	coby	k?
koncepčně	koncepčně	k6eAd1
zastaralého	zastaralý	k2eAgInSc2d1
modelu	model	k1gInSc2
se	s	k7c7
vzduchem	vzduch	k1gInSc7
chlazeným	chlazený	k2eAgInSc7d1
motorem	motor	k1gInSc7
ale	ale	k8xC
také	také	k9
rapidně	rapidně	k6eAd1
klesl	klesnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volkswagen	volkswagen	k1gInSc1
inovoval	inovovat	k5eAaBmAgInS
Brouk	brouk	k1gMnSc1
a	a	k8xC
modely	model	k1gInPc1
1500	#num#	k4
a	a	k8xC
411	#num#	k4
na	na	k7c4
1600	#num#	k4
a	a	k8xC
412	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahájil	zahájit	k5eAaPmAgMnS
i	i	k9
výrobu	výroba	k1gFnSc4
modelu	model	k1gInSc2
střední	střední	k2eAgFnSc2d1
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
připraveného	připravený	k2eAgInSc2d1
NSU	NSU	kA
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
VW	VW	kA
K	k	k7c3
<g/>
70	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
prodeje	prodej	k1gInSc2
to	ten	k3xDgNnSc1
nezvýšilo	zvýšit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologické	technologický	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
pohonu	pohon	k1gInSc2
předních	přední	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nabyl	nabýt	k5eAaPmAgInS
převzetím	převzetí	k1gNnSc7
Auto	auto	k1gNnSc4
Union	union	k1gInSc1
a	a	k8xC
NSU	NSU	kA
<g/>
,	,	kIx,
však	však	k9
využil	využít	k5eAaPmAgMnS
při	při	k7c6
konstrukci	konstrukce	k1gFnSc6
nástupných	nástupný	k2eAgInPc2d1
modelů	model	k1gInPc2
s	s	k7c7
karoseriemi	karoserie	k1gFnPc7
navrženými	navržený	k2eAgInPc7d1
předními	přední	k2eAgMnPc7d1
italskými	italský	k2eAgMnPc7d1
designéry	designér	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
výrobu	výroba	k1gFnSc4
středního	střední	k2eAgInSc2d1
typu	typ	k1gInSc2
Passat	Passat	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
pouhou	pouhý	k2eAgFnSc7d1
liftbackovou	liftbackův	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
Audi	Audi	k1gNnSc1
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
výrobu	výroba	k1gFnSc4
kompaktního	kompaktní	k2eAgInSc2d1
typu	typ	k1gInSc2
Golf	golf	k1gInSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
malého	malý	k2eAgInSc2d1
typu	typ	k1gInSc2
Polo	polo	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
levnější	levný	k2eAgFnSc1d2
verze	verze	k1gFnSc1
Audi	Audi	k1gNnSc1
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
trojice	trojice	k1gFnPc4
navrátila	navrátit	k5eAaPmAgFnS
VW	VW	kA
pozici	pozice	k1gFnSc4
největšího	veliký	k2eAgMnSc4d3
evropského	evropský	k2eAgMnSc4d1
výrobce	výrobce	k1gMnSc4
aut	auto	k1gNnPc2
a	a	k8xC
v	v	k7c6
dalších	další	k1gNnPc6
v	v	k7c6
dalších	další	k2eAgFnPc6d1
generacích	generace	k1gFnPc6
se	se	k3xPyFc4
stále	stále	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
nejlevnějšího	levný	k2eAgInSc2d3
modelu	model	k1gInSc2
Brouk	brouk	k1gMnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Německu	Německo	k1gNnSc6
ukončena	ukončen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
byl	být	k5eAaImAgInS
vyráběn	vyrábět	k5eAaImNgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
pokračoval	pokračovat	k5eAaImAgInS
VW	VW	kA
v	v	k7c6
modernizaci	modernizace	k1gFnSc6
vyráběných	vyráběný	k2eAgInPc2d1
typů	typ	k1gInPc2
a	a	k8xC
zavádění	zavádění	k1gNnSc1
nových	nový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
(	(	kIx(
<g/>
katalyzátor	katalyzátor	k1gInSc1
<g/>
,	,	kIx,
elektronické	elektronický	k2eAgNnSc4d1
vstřikování	vstřikování	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgInS
nadpoloviční	nadpoloviční	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
akcií	akcie	k1gFnPc2
španělské	španělský	k2eAgFnSc2d1
značky	značka	k1gFnSc2
Seat	Seat	k1gMnSc1
(	(	kIx(
<g/>
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
třetí	třetí	k4xOgFnSc1
samostatná	samostatný	k2eAgFnSc1d1
značka	značka	k1gFnSc1
koncernu	koncern	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
spoluprací	spolupráce	k1gFnSc7
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
japonskými	japonský	k2eAgFnPc7d1
<g/>
)	)	kIx)
firmami	firma	k1gFnPc7
dosáhl	dosáhnout	k5eAaPmAgInS
20	#num#	k4
<g/>
%	%	kIx~
podílu	podíl	k1gInSc2
na	na	k7c6
automobilovém	automobilový	k2eAgInSc6d1
trhu	trh	k1gInSc6
Evropy	Evropa	k1gFnSc2
a	a	k8xC
10	#num#	k4
<g/>
%	%	kIx~
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
připojil	připojit	k5eAaPmAgInS
Volkswagen	volkswagen	k1gInSc1
do	do	k7c2
svého	svůj	k3xOyFgInSc2
koncernu	koncern	k1gInSc2
českou	český	k2eAgFnSc4d1
automobilku	automobilka	k1gFnSc4
Škoda	Škoda	k1gMnSc1
Auto	auto	k1gNnSc4
včetně	včetně	k7c2
jejích	její	k3xOp3gFnPc2
veškerých	veškerý	k3xTgFnPc2
výrobních	výrobní	k2eAgFnPc2d1
kapacit	kapacita	k1gFnPc2
a	a	k8xC
tradiční	tradiční	k2eAgFnSc2d1
značky	značka	k1gFnSc2
Škoda	škoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paradoxní	paradoxní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Volkswagen	volkswagen	k1gInSc1
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
mladší	mladý	k2eAgFnSc1d2
značkou	značka	k1gFnSc7
než	než	k8xS
Škoda	škoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
připojení	připojení	k1gNnSc2
do	do	k7c2
koncernu	koncern	k1gInSc2
Volkswagen	volkswagen	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
však	však	k8xC
„	„	k?
<g/>
Škodovka	škodovka	k1gFnSc1
<g/>
“	“	k?
prodělala	prodělat	k5eAaPmAgFnS
velké	velký	k2eAgFnPc4d1
změny	změna	k1gFnPc4
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
postupně	postupně	k6eAd1
moderním	moderní	k2eAgMnSc7d1
výrobcem	výrobce	k1gMnSc7
automobilů	automobil	k1gInPc2
s	s	k7c7
všestranně	všestranně	k6eAd1
vysokou	vysoký	k2eAgFnSc7d1
úrovní	úroveň	k1gFnSc7
<g/>
,	,	kIx,
plně	plně	k6eAd1
konkurenceschopným	konkurenceschopný	k2eAgInSc7d1
na	na	k7c6
světových	světový	k2eAgInPc6d1
trzích	trh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
prodělal	prodělat	k5eAaPmAgInS
koncern	koncern	k1gInSc1
Volkswagen	volkswagen	k1gInSc1
velkou	velký	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
však	však	k9
překonal	překonat	k5eAaPmAgMnS
hlavně	hlavně	k9
díky	díky	k7c3
úspěšným	úspěšný	k2eAgFnPc3d1
novým	nový	k2eAgFnPc3d1
generacím	generace	k1gFnPc3
modelů	model	k1gInPc2
Golf	golf	k1gInSc1
a	a	k8xC
Passat	Passat	k1gMnPc1
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
doplnil	doplnit	k5eAaPmAgInS
model	model	k1gInSc1
Polo	polo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Passat	Passat	k1gFnSc2
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Golf	golf	k1gInSc1
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Passat	Passat	k1gFnSc2
Variant	varianta	k1gFnPc2
2.0	2.0	k4
FSI	FSI	kA
4	#num#	k4
<g/>
Motion	Motion	k1gInSc4
</s>
<s>
Až	až	k9
donedávna	donedávna	k6eAd1
byla	být	k5eAaImAgFnS
značka	značka	k1gFnSc1
VW	VW	kA
jednou	jednou	k6eAd1
z	z	k7c2
mála	málo	k1gNnSc2
těch	ten	k3xDgFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
vyhýbala	vyhýbat	k5eAaImAgFnS
celosvětová	celosvětový	k2eAgFnSc1d1
automobilová	automobilový	k2eAgFnSc1d1
recese	recese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volkswagen	volkswagen	k1gInSc1
vstupoval	vstupovat	k5eAaImAgInS
do	do	k7c2
nových	nový	k2eAgInPc2d1
segmentů	segment	k1gInPc2
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
automobilového	automobilový	k2eAgInSc2d1
(	(	kIx(
<g/>
nákladní	nákladní	k2eAgInPc1d1
automobily	automobil	k1gInPc1
-	-	kIx~
značka	značka	k1gFnSc1
Scania	Scanium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
například	například	k6eAd1
trhu	trh	k1gInSc2
lodních	lodní	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modely	model	k1gInPc1
Volkswagen	volkswagen	k1gInSc1
Golf	golf	k1gInSc1
v	v	k7c6
souhrnu	souhrn	k1gInSc6
překonaly	překonat	k5eAaPmAgInP
počet	počet	k1gInSc4
vyrobených	vyrobený	k2eAgInPc2d1
vozů	vůz	k1gInPc2
Brouk	brouk	k1gMnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Käfer	Käfer	k1gInSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Beetle	Beetle	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
celkem	celkem	k6eAd1
21	#num#	k4
529	#num#	k4
464	#num#	k4
exemplářů	exemplář	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vedení	vedení	k1gNnSc1
koncernu	koncern	k1gInSc2
</s>
<s>
Vedení	vedení	k1gNnSc1
koncernu	koncern	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
dozorčí	dozorčí	k2eAgFnSc1d1
rada	rada	k1gFnSc1
a	a	k8xC
představenstvo	představenstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
byl	být	k5eAaImAgInS
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2002	#num#	k4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
vnuk	vnuk	k1gMnSc1
Ferdinanda	Ferdinand	k1gMnSc4
Porscheho	Porsche	k1gMnSc4
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgMnPc2d3
německých	německý	k2eAgMnPc2d1
podnikatelů	podnikatel	k1gMnPc2
a	a	k8xC
velkoakcionář	velkoakcionář	k1gMnSc1
Volkswagenu	volkswagen	k1gInSc2
Ferdinand	Ferdinand	k1gMnSc1
Piëch	Piëch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představenstvo	představenstvo	k1gNnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
sedm	sedm	k4xCc4
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
až	až	k9
do	do	k7c2
vypuknutí	vypuknutí	k1gNnSc2
tzv.	tzv.	kA
Dieselgate	Dieselgat	k1gInSc5
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Winterkorn	Winterkorn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současným	současný	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
představenstva	představenstvo	k1gNnSc2
(	(	kIx(
<g/>
CEO	CEO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Herbert	Herbert	k1gMnSc1
Diess	Diess	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tržní	tržní	k2eAgFnSc1d1
kapitalizace	kapitalizace	k1gFnSc1
</s>
<s>
Automobilka	automobilka	k1gFnSc1
Volkswagen	volkswagen	k1gInSc1
(	(	kIx(
<g/>
VW	VW	kA
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc4
akcií	akcie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k8xS
tzv.	tzv.	kA
zvýhodněné	zvýhodněný	k2eAgFnPc4d1
akcie	akcie	k1gFnPc4
(	(	kIx(
<g/>
Vorzugsaktien	Vorzugsaktien	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
Vorzüge	Vorzüge	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
70,0	70,0	k4
mld.	mld.	k?
Eur	euro	k1gNnPc2
jsou	být	k5eAaImIp3nP
vedeny	vést	k5eAaImNgInP
v	v	k7c6
indexu	index	k1gInSc6
DAX	DAX	kA
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
kurzy	kurz	k1gInPc1
tzv.	tzv.	kA
kmenových	kmenový	k2eAgFnPc2d1
akcií	akcie	k1gFnPc2
(	(	kIx(
<g/>
Stammaktien	Stammaktien	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
Stämme	Stämme	k1gMnPc1
<g/>
)	)	kIx)
lze	lze	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
pouze	pouze	k6eAd1
v	v	k7c6
seznamu	seznam	k1gInSc6
Prime	prim	k1gInSc5
Standard	standard	k1gInSc1
bez	bez	k7c2
tržní	tržní	k2eAgFnSc2d1
kapitalizace	kapitalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
všech	všecek	k3xTgFnPc2
akcií	akcie	k1gFnPc2
VW	VW	kA
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
či	či	k8xC
nepřímo	přímo	k6eNd1
(	(	kIx(
<g/>
přes	přes	k7c4
Porsche	Porsche	k1gNnSc4
Holding	holding	k1gInSc1
<g/>
)	)	kIx)
držena	držen	k2eAgFnSc1d1
dvěma	dva	k4xCgFnPc7
podnikatelskými	podnikatelský	k2eAgFnPc7d1
rodinami	rodina	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
rodinami	rodina	k1gFnPc7
Wolfganga	Wolfgang	k1gMnSc2
Porscheho	Porsche	k1gMnSc2
a	a	k8xC
Ferdinanda	Ferdinand	k1gMnSc2
Piëcha	Piëch	k1gMnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
jsou	být	k5eAaImIp3nP
vnuci	vnuk	k1gMnPc1
zakladatele	zakladatel	k1gMnSc2
firem	firma	k1gFnPc2
Volkswagen	volkswagen	k1gInSc4
a	a	k8xC
Porsche	Porsche	k1gNnSc4
<g/>
,	,	kIx,
Ferdinanda	Ferdinand	k1gMnSc2
Porscheho	Porsche	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volkswagen	volkswagen	k1gInSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
automobilek	automobilka	k1gFnPc2
světa	svět	k1gInSc2
a	a	k8xC
patří	patřit	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
mj.	mj.	kA
100	#num#	k4
%	%	kIx~
akcií	akcie	k1gFnPc2
Škody	škoda	k1gFnSc2
Auto	auto	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
měla	mít	k5eAaImAgFnS
celkově	celkově	k6eAd1
592	#num#	k4
600	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skandál	skandál	k1gInSc1
s	s	k7c7
falšováním	falšování	k1gNnSc7
emisí	emise	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dieselgate	Dieselgat	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2015	#num#	k4
vydala	vydat	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
Agentura	agentura	k1gFnSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
EPA	EPA	kA
<g/>
)	)	kIx)
prohlášení	prohlášení	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
zmiňuje	zmiňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
firma	firma	k1gFnSc1
Volkswagen	volkswagen	k1gInSc1
manipuluje	manipulovat	k5eAaImIp3nS
s	s	k7c7
motorovým	motorový	k2eAgInSc7d1
softwarem	software	k1gInSc7
na	na	k7c6
dieselových	dieselový	k2eAgFnPc6d1
TDI	TDI	kA
motorech	motor	k1gInPc6
a	a	k8xC
porušuje	porušovat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
emisní	emisní	k2eAgInPc4d1
limity	limit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
k	k	k7c3
tomuto	tento	k3xDgInSc3
skandálu	skandál	k1gInSc3
postavila	postavit	k5eAaPmAgFnS
čelem	čelo	k1gNnSc7
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nP
se	se	k3xPyFc4
ústy	ústa	k1gNnPc7
Michaela	Michael	k1gMnSc2
Horna	Horn	k1gMnSc2
<g/>
,	,	kIx,
prezidenta	prezident	k1gMnSc2
americké	americký	k2eAgFnSc2d1
pobočky	pobočka	k1gFnSc2
Volkswagenu	volkswagen	k1gInSc2
<g/>
,	,	kIx,
veřejnosti	veřejnost	k1gFnSc2
omluvila	omluvit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
uvedla	uvést	k5eAaPmAgFnS
že	že	k8xS
tento	tento	k3xDgInSc4
vadný	vadný	k2eAgInSc4d1
motor	motor	k1gInSc4
obsahuje	obsahovat	k5eAaImIp3nS
11	#num#	k4
miliónů	milión	k4xCgInPc2
aut	auto	k1gNnPc2
značky	značka	k1gFnPc4
Volkswagen	volkswagen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akcie	akcie	k1gFnSc1
společnosti	společnost	k1gFnSc2
se	se	k3xPyFc4
po	po	k7c6
tomto	tento	k3xDgInSc6
strmě	strmě	k6eAd1
propadly	propadlo	k1gNnPc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
po	po	k7c4
jednání	jednání	k1gNnSc4
předsednictva	předsednictvo	k1gNnSc2
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
rezignoval	rezignovat	k5eAaBmAgMnS
šéf	šéf	k1gMnSc1
koncernu	koncern	k1gInSc2
Martin	Martin	k1gMnSc1
Winterkorn	Winterkorn	k1gMnSc1
ačkoli	ačkoli	k8xS
ještě	ještě	k9
den	den	k1gInSc4
před	před	k7c7
tím	ten	k3xDgNnSc7
rezignaci	rezignace	k1gFnSc4
odmítal	odmítat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akcie	akcie	k1gFnSc1
firmy	firma	k1gFnSc2
za	za	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
klesly	klesnout	k5eAaPmAgInP
o	o	k7c4
celou	celý	k2eAgFnSc4d1
třetinu	třetina	k1gFnSc4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
německý	německý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
Alexander	Alexandra	k1gFnPc2
Dobrindt	Dobrindt	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
automobilka	automobilka	k1gFnSc1
manipulovala	manipulovat	k5eAaImAgFnS
s	s	k7c7
emisními	emisní	k2eAgNnPc7d1
měřeními	měření	k1gNnPc7
u	u	k7c2
naftových	naftový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
nejen	nejen	k6eAd1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
kauzou	kauza	k1gFnSc7
čelí	čelit	k5eAaImIp3nS
Volkswagen	volkswagen	k1gInSc4
řadě	řada	k1gFnSc3
hromadných	hromadný	k2eAgFnPc2d1
žalob	žaloba	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výrobní	výrobní	k2eAgInPc1d1
závody	závod	k1gInPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Altebauna	Altebauna	k1gFnSc1
<g/>
:	:	kIx,
závod	závod	k1gInSc1
zahájil	zahájit	k5eAaPmAgInS
výrobu	výroba	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
sloužil	sloužit	k5eAaImAgInS
pro	pro	k7c4
repasování	repasování	k1gNnSc4
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
z	z	k7c2
něj	on	k3xPp3gNnSc2
centrum	centrum	k1gNnSc1
výroby	výroba	k1gFnSc2
náhradních	náhradní	k2eAgInPc2d1
dílů	díl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Drážďany	Drážďany	k1gInPc1
<g/>
:	:	kIx,
základní	základní	k2eAgInSc1d1
kámen	kámen	k1gInSc1
toho	ten	k3xDgInSc2
závodu	závod	k1gInSc2
byl	být	k5eAaImAgInS
položen	položit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
unikátní	unikátní	k2eAgFnSc4d1
prosklenou	prosklený	k2eAgFnSc4d1
továrnu	továrna	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
sledovat	sledovat	k5eAaImF
průběh	průběh	k1gInSc4
výroby	výroba	k1gFnSc2
z	z	k7c2
venku	venek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
model	model	k1gInSc1
Phaeton	Phaeton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Emden	Emden	k1gInSc1
<g/>
:	:	kIx,
tento	tento	k3xDgInSc1
závod	závod	k1gInSc1
sloužil	sloužit	k5eAaImAgInS
původně	původně	k6eAd1
k	k	k7c3
výrobě	výroba	k1gFnSc3
vozů	vůz	k1gInPc2
Brouk	brouk	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyráběly	vyrábět	k5eAaImAgInP
i	i	k8xC
model	model	k1gInSc1
Passat	Passat	k1gFnSc2
ve	v	k7c6
všech	všecek	k3xTgFnPc6
jeho	jeho	k3xOp3gFnPc6
generacích	generace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Hannover	Hannover	k1gInSc1
<g/>
:	:	kIx,
závod	závod	k1gInSc1
zahájil	zahájit	k5eAaPmAgInS
výrobu	výroba	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
a	a	k8xC
znamenal	znamenat	k5eAaImAgInS
začátek	začátek	k1gInSc1
decentralizace	decentralizace	k1gFnSc2
výroby	výroba	k1gFnSc2
Volkswagenu	volkswagen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyráběl	vyrábět	k5eAaImAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
model	model	k1gInSc1
Transporter	Transportra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
vyrábějí	vyrábět	k5eAaImIp3nP
motory	motor	k1gInPc4
i	i	k9
pro	pro	k7c4
jiné	jiný	k2eAgInPc4d1
vozy	vůz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Salzgitter	Salzgitter	k1gInSc1
<g/>
:	:	kIx,
závod	závod	k1gInSc1
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
výrobou	výroba	k1gFnSc7
modelu	model	k1gInSc2
K	k	k7c3
70	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
znamenal	znamenat	k5eAaImAgInS
přechod	přechod	k1gInSc4
k	k	k7c3
nové	nový	k2eAgFnSc3d1
generaci	generace	k1gFnSc3
vozů	vůz	k1gInPc2
Volkswagen	volkswagen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
lodní	lodní	k2eAgInPc4d1
motory	motor	k1gInPc4
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Wolfsburg	Wolfsburg	k1gInSc1
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
začala	začít	k5eAaPmAgFnS
v	v	k7c6
nynějším	nynější	k2eAgInSc6d1
kmenovém	kmenový	k2eAgInSc6d1
závodě	závod	k1gInSc6
ve	v	k7c6
Wolfsburgu	Wolfsburg	k1gInSc6
výroba	výroba	k1gFnSc1
modelu	model	k1gInSc2
Transporter	Transportrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
přišla	přijít	k5eAaPmAgFnS
na	na	k7c4
řadu	řada	k1gFnSc4
výroba	výroba	k1gFnSc1
modelu	model	k1gInSc2
Brouk	brouk	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
vystřídal	vystřídat	k5eAaPmAgInS
Golf	golf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chvíli	chvíle	k1gFnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
vyráběly	vyrábět	k5eAaImAgInP
modely	model	k1gInPc1
Polo	polo	k6eAd1
a	a	k8xC
Passat	Passat	k1gFnSc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
výroba	výroba	k1gFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
přesunuta	přesunout	k5eAaPmNgFnS
jinam	jinam	k6eAd1
a	a	k8xC
později	pozdě	k6eAd2
opět	opět	k6eAd1
obnovena	obnoven	k2eAgFnSc1d1
v	v	k7c6
dalších	další	k2eAgFnPc6d1
generacích	generace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1994	#num#	k4
byla	být	k5eAaImAgFnS
zvětšena	zvětšen	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
tohoto	tento	k3xDgInSc2
závodu	závod	k1gInSc2
250	#num#	k4
000	#num#	k4
vozů	vůz	k1gInPc2
ročně	ročně	k6eAd1
a	a	k8xC
zaměstnáno	zaměstnat	k5eAaPmNgNnS
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
přes	přes	k7c4
3	#num#	k4
200	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
začala	začít	k5eAaPmAgFnS
výroba	výroba	k1gFnSc1
malého	malý	k2eAgInSc2d1
modelu	model	k1gInSc2
Lupo	lupa	k1gFnSc5
a	a	k8xC
o	o	k7c4
4	#num#	k4
roky	rok	k1gInPc7
později	pozdě	k6eAd2
také	také	k6eAd1
většího	veliký	k2eAgInSc2d2
rodinného	rodinný	k2eAgInSc2d1
Touranu	Touran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
modelů	model	k1gInPc2
Polo	polo	k6eAd1
<g/>
,	,	kIx,
Lupo	lupa	k1gFnSc5
<g/>
,	,	kIx,
Golf	golf	k1gInSc1
<g/>
,	,	kIx,
Touran	Touran	k1gInSc1
zde	zde	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
i	i	k9
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Argentina	Argentina	k1gFnSc1
<g/>
:	:	kIx,
na	na	k7c4
Argentinský	argentinský	k2eAgInSc4d1
trh	trh	k1gInSc4
vstoupila	vstoupit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Volkswagen	volkswagen	k1gInSc1
již	již	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
se	se	k3xPyFc4
výroba	výroba	k1gFnSc1
vozů	vůz	k1gInPc2
koncentrovala	koncentrovat	k5eAaBmAgFnS
do	do	k7c2
dvou	dva	k4xCgNnPc2
center	centrum	k1gNnPc2
-	-	kIx~
San	San	k1gFnSc1
Justo	Justo	k1gNnSc1
a	a	k8xC
Pacheco	Pacheco	k1gNnSc1
u	u	k7c2
Buenos	Buenosa	k1gFnPc2
Aires	Airesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Pacheco	Pacheco	k1gNnSc4
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1995	#num#	k4
otevřen	otevřen	k2eAgInSc1d1
další	další	k2eAgInSc1d1
<g/>
,	,	kIx,
nový	nový	k2eAgInSc1d1
závod	závod	k1gInSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
150	#num#	k4
000	#num#	k4
vozidel	vozidlo	k1gNnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
modelů	model	k1gInPc2
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
Jihoamerický	jihoamerický	k2eAgInSc4d1
trh	trh	k1gInSc4
se	se	k3xPyFc4
zde	zde	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nP
modely	model	k1gInPc1
Golf	golf	k1gInSc1
<g/>
,	,	kIx,
Polo	polo	k6eAd1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
také	také	k9
další	další	k2eAgInPc4d1
koncernové	koncernový	k2eAgInPc4d1
vozy	vůz	k1gInPc4
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Audi	Audi	k1gNnSc1
A3	A3	k1gFnSc2
ale	ale	k8xC
také	také	k9
Škoda	škoda	k1gFnSc1
Octavia	octavia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
:	:	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
spolupracoval	spolupracovat	k5eAaImAgInS
Volkswagen	volkswagen	k1gInSc1
se	s	k7c7
značkou	značka	k1gFnSc7
TAS	tasit	k5eAaPmRp2nS
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
montovaly	montovat	k5eAaImAgInP
vozy	vůz	k1gInPc1
Volkswagen	volkswagen	k1gInSc1
od	od	k7c2
července	červenec	k1gInSc2
1998	#num#	k4
zde	zde	k6eAd1
začal	začít	k5eAaPmAgInS
koncern	koncern	k1gInSc1
vyrábět	vyrábět	k5eAaImF
vozy	vůz	k1gInPc4
Škoda	škoda	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
<g/>
:	:	kIx,
závod	závod	k1gInSc1
v	v	k7c6
brazilském	brazilský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Sao	Sao	k1gMnSc2
Bernardo	Bernardo	k1gNnSc1
do	do	k7c2
Campo	Campa	k1gFnSc5
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
již	již	k6eAd1
roku	rok	k1gInSc2
1959	#num#	k4
a	a	k8xC
Volkswagen	volkswagen	k1gInSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
stal	stát	k5eAaPmAgInS
největším	veliký	k2eAgMnSc7d3
výrobcem	výrobce	k1gMnSc7
automobilů	automobil	k1gInPc2
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
rozšíření	rozšíření	k1gNnSc4
přišlo	přijít	k5eAaPmAgNnS
až	až	k9
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
nákladních	nákladní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
:	:	kIx,
v	v	k7c6
Číně	Čína	k1gFnSc6
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
výroba	výroba	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1985	#num#	k4
modelem	model	k1gInSc7
Santana	Santana	k1gFnSc1
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
nejmodernější	moderní	k2eAgFnSc7d3
továrnou	továrna	k1gFnSc7
na	na	k7c4
výrobu	výroba	k1gFnSc4
automobilů	automobil	k1gInPc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modely	model	k1gInPc1
pro	pro	k7c4
čínský	čínský	k2eAgInSc4d1
trh	trh	k1gInSc4
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
odvozené	odvozený	k2eAgInPc1d1
od	od	k7c2
starších	starý	k2eAgInPc2d2
evropských	evropský	k2eAgInPc2d1
modelů	model	k1gInPc2
a	a	k8xC
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
výrobě	výroba	k1gFnSc6
se	se	k3xPyFc4
stále	stále	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
rozšíření	rozšíření	k1gNnSc3
výrobních	výrobní	k2eAgFnPc2d1
kapacit	kapacita	k1gFnPc2
především	především	k9
na	na	k7c4
výrobu	výroba	k1gFnSc4
motorů	motor	k1gInPc2
pro	pro	k7c4
menší	malý	k2eAgInPc4d2
vozy	vůz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
<g/>
:	:	kIx,
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1982	#num#	k4
zahájena	zahájen	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
modelu	model	k1gInSc2
Santana	Santana	k1gFnSc1
v	v	k7c6
téměř	téměř	k6eAd1
stejné	stejný	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
jako	jako	k8xS,k8xC
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
<g/>
:	:	kIx,
výroba	výroba	k1gFnSc1
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1964	#num#	k4
kvůli	kvůli	k7c3
zhoršení	zhoršení	k1gNnSc3
dovozních	dovozní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
na	na	k7c4
vozy	vůz	k1gInPc4
z	z	k7c2
ciziny	cizina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
v	v	k7c6
Pueble	Pueble	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Mexika	Mexiko	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
dovážely	dovážet	k5eAaImAgInP
vozy	vůz	k1gInPc1
i	i	k8xC
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
probíhala	probíhat	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
modelu	model	k1gInSc2
Brouk	brouk	k1gMnSc1
už	už	k9
pouze	pouze	k6eAd1
zde	zde	k6eAd1
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
model	model	k1gInSc1
New	New	k1gFnSc2
Beetle	Beetle	k1gFnSc2
připomínající	připomínající	k2eAgFnSc1d1
původního	původní	k2eAgMnSc4d1
Brouka	brouk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračuje	pokračovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
ve	v	k7c6
výrobě	výroba	k1gFnSc6
zaběhnutých	zaběhnutý	k2eAgInPc2d1
modelů	model	k1gInPc2
Jetta	Jetto	k1gNnSc2
<g/>
,	,	kIx,
Passat	Passat	k1gFnSc2
a	a	k8xC
Golf	golf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
závod	závod	k1gInSc1
v	v	k7c6
Lagosu	Lagos	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
však	však	k9
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
objem	objem	k1gInSc1
výroby	výroba	k1gFnSc2
omezen	omezit	k5eAaPmNgInS
kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
deviz	deviza	k1gFnPc2
a	a	k8xC
plně	plně	k6eAd1
obnovena	obnoven	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Polsko	Polsko	k1gNnSc1
<g/>
:	:	kIx,
výroba	výroba	k1gFnSc1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
započala	započnout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1993	#num#	k4
v	v	k7c6
Poznani	Poznaň	k1gFnSc6
modelem	model	k1gInSc7
Transporter	Transportra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
se	se	k3xPyFc4
přidala	přidat	k5eAaPmAgFnS
výroba	výroba	k1gFnSc1
modelu	model	k1gInSc2
Caddy	Cadda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
výroba	výroba	k1gFnSc1
v	v	k7c6
nové	nový	k2eAgFnSc6d1
továrně	továrna	k1gFnSc6
u	u	k7c2
města	město	k1gNnSc2
Wrzésnia	Wrzésnium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
<g/>
:	:	kIx,
výroba	výroba	k1gFnSc1
v	v	k7c6
Portugalském	portugalský	k2eAgInSc6d1
závodě	závod	k1gInSc6
Palmela	Palmela	k1gFnSc1
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1995	#num#	k4
modelem	model	k1gInSc7
Sharan	Sharana	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
nástupce	nástupce	k1gMnSc1
se	se	k3xPyFc4
tu	tu	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Volskwagen	Volskwagen	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
rozšířit	rozšířit	k5eAaPmF
své	svůj	k3xOyFgFnSc3
pole	pola	k1gFnSc3
působnosti	působnost	k1gFnSc2
do	do	k7c2
tehdejší	tehdejší	k2eAgFnSc2d1
ČSFR	ČSFR	kA
byla	být	k5eAaImAgFnS
v	v	k7c6
převzaté	převzatý	k2eAgFnSc6d1
továrně	továrna	k1gFnSc6
od	od	k7c2
Bratislavských	bratislavský	k2eAgInPc2d1
Automobilových	automobilový	k2eAgInPc2d1
Závodů	závod	k1gInPc2
rozběhnuta	rozběhnut	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
modelu	model	k1gInSc2
Passat	Passat	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
také	také	k9
zahájena	zahájit	k5eAaPmNgFnS
výroba	výroba	k1gFnSc1
terénního	terénní	k2eAgInSc2d1
modelu	model	k1gInSc2
Touareg	Touarega	k1gFnPc2
a	a	k8xC
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
platformě	platforma	k1gFnSc6
Audi	Audi	k1gNnSc1
Q7	Q7	k1gFnSc2
a	a	k8xC
Porsche	Porsche	k1gNnPc1
Cayenne	Cayenn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
začala	začít	k5eAaPmAgFnS
Španělská	španělský	k2eAgFnSc1d1
automobilka	automobilka	k1gFnSc1
SEAT	SEAT	kA
vyrábět	vyrábět	k5eAaImF
v	v	k7c6
licenci	licence	k1gFnSc6
modely	model	k1gInPc1
Volkswagen	volkswagen	k1gInSc1
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
koncernu	koncern	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
přesun	přesun	k1gInSc4
výroby	výroba	k1gFnSc2
některých	některý	k3yIgInPc2
modelů	model	k1gInPc2
do	do	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
vyráběly	vyrábět	k5eAaImAgInP
modely	model	k1gInPc1
Passat	Passat	k1gMnPc2
a	a	k8xC
Polo	polo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
výroba	výroba	k1gFnSc1
stále	stále	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
USA	USA	kA
<g/>
:	:	kIx,
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
Volkswagen	volkswagen	k1gInSc1
vyrábět	vyrábět	k5eAaImF
své	svůj	k3xOyFgInPc4
vozy	vůz	k1gInPc4
díky	díky	k7c3
klesajícímu	klesající	k2eAgInSc3d1
kurzu	kurz	k1gInSc3
dolaru	dolar	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ztěžoval	ztěžovat	k5eAaImAgMnS
vývoz	vývoz	k1gInSc4
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1978	#num#	k4
postaven	postavit	k5eAaPmNgInS
závod	závod	k1gInSc1
ve	v	k7c4
Westmorelandu	Westmorelanda	k1gFnSc4
v	v	k7c6
Pensylvánii	Pensylvánie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
výroba	výroba	k1gFnSc1
modelu	model	k1gInSc2
Golf	golf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
závod	závod	k1gInSc1
v	v	k7c6
Detroitu	Detroit	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgMnS
z	z	k7c2
důvodu	důvod	k1gInSc2
nevytíženosti	nevytíženost	k1gFnSc2
roku	rok	k1gInSc2
1983	#num#	k4
prodán	prodat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc1d1
osud	osud	k1gInSc1
postihl	postihnout	k5eAaPmAgInS
i	i	k9
závod	závod	k1gInSc1
ve	v	k7c6
Westmorelandu	Westmoreland	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zásadní	zásadní	k2eAgInPc1d1
modely	model	k1gInPc1
</s>
<s>
KdF	KdF	k?
82	#num#	k4
<g/>
:	:	kIx,
KdF	KdF	k1gMnSc1
82	#num#	k4
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc6
sériově	sériově	k6eAd1
vyráběným	vyráběný	k2eAgInSc7d1
vozem	vůz	k1gInSc7
nově	nově	k6eAd1
založené	založený	k2eAgFnSc2d1
automobilky	automobilka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
lehký	lehký	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
terénní	terénní	k2eAgInSc1d1
automobil	automobil	k1gInSc1
<g/>
,	,	kIx,
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1940	#num#	k4
-	-	kIx~
1945	#num#	k4
vyrobeno	vyrobit	k5eAaPmNgNnS
asi	asi	k9
50	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Brouk	brouk	k1gMnSc1
<g/>
:	:	kIx,
tento	tento	k3xDgInSc1
model	model	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
již	již	k6eAd1
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
válka	válka	k1gFnSc1
nedovolila	dovolit	k5eNaPmAgFnS
výrobě	výroba	k1gFnSc3
rozběhnout	rozběhnout	k5eAaPmF
se	se	k3xPyFc4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
základním	základní	k2eAgInSc7d1
stavebním	stavební	k2eAgInSc7d1
kamenem	kámen	k1gInSc7
značky	značka	k1gFnSc2
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
a	a	k8xC
přispěl	přispět	k5eAaPmAgInS
k	k	k7c3
poválečné	poválečný	k2eAgFnSc3d1
motorizaci	motorizace	k1gFnSc3
západního	západní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
skončila	skončit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
výroba	výroba	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
pokračovala	pokračovat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůz	vůz	k1gInSc1
byl	být	k5eAaImAgInS
charakteristický	charakteristický	k2eAgInSc1d1
vzadu	vzadu	k6eAd1
uloženým	uložený	k2eAgInSc7d1
vzduchem	vzduch	k1gInSc7
chlazeným	chlazený	k2eAgInSc7d1
motorem	motor	k1gInSc7
a	a	k8xC
subtilní	subtilní	k2eAgFnSc7d1
karoserií	karoserie	k1gFnSc7
typického	typický	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Golf	golf	k1gInSc1
<g/>
:	:	kIx,
Golf	golf	k1gInSc1
vystřídal	vystřídat	k5eAaPmAgInS
ve	v	k7c6
výrobním	výrobní	k2eAgInSc6d1
programu	program	k1gInSc6
značky	značka	k1gFnSc2
Brouka	brouk	k1gMnSc2
a	a	k8xC
navázal	navázat	k5eAaPmAgInS
na	na	k7c4
jeho	jeho	k3xOp3gInPc4
úspěchy	úspěch	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
začala	začít	k5eAaPmAgFnS
výroba	výroba	k1gFnSc1
už	už	k6eAd1
jeho	jeho	k3xOp3gFnPc4
šesté	šestý	k4xOgFnPc4
generace	generace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
je	být	k5eAaImIp3nS
v	v	k7c6
několika	několik	k4yIc6
variantách	varianta	k1gFnPc6
s	s	k7c7
různými	různý	k2eAgInPc7d1
motory	motor	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
s	s	k7c7
postupem	postup	k1gInSc7
získávaly	získávat	k5eAaImAgFnP
na	na	k7c6
výkonu	výkon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
výkonem	výkon	k1gInSc7
rostla	růst	k5eAaImAgFnS
i	i	k9
hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
rozměry	rozměr	k1gInPc1
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Passat	Passat	k5eAaImF,k5eAaPmF
<g/>
:	:	kIx,
Passat	Passat	k1gFnSc1
je	být	k5eAaImIp3nS
klasický	klasický	k2eAgInSc4d1
vůz	vůz	k1gInSc4
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
v	v	k7c6
modelové	modelový	k2eAgFnSc6d1
hierarchii	hierarchie	k1gFnSc6
nad	nad	k7c7
Golfem	golf	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
ve	v	k7c6
verzi	verze	k1gFnSc6
sedan	sedan	k1gInSc4
a	a	k8xC
kombi	kombi	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
Golfu	golf	k1gInSc3
je	být	k5eAaImIp3nS
prostornější	prostorný	k2eAgInSc4d2
<g/>
,	,	kIx,
luxusnější	luxusní	k2eAgInSc4d2
a	a	k8xC
má	mít	k5eAaImIp3nS
výkonnější	výkonný	k2eAgInPc4d2
motory	motor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
si	se	k3xPyFc3
vysloužil	vysloužit	k5eAaPmAgMnS
přezdívku	přezdívka	k1gFnSc4
manažerské	manažerský	k2eAgNnSc1d1
auto	auto	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Transporter	Transporter	k1gInSc1
<g/>
:	:	kIx,
tento	tento	k3xDgInSc1
model	model	k1gInSc1
začal	začít	k5eAaPmAgInS
Volkswagen	volkswagen	k1gInSc1
vyrábět	vyrábět	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
a	a	k8xC
vyrábí	vyrábět	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
pod	pod	k7c7
tímto	tento	k3xDgInSc7
názvem	název	k1gInSc7
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
velmi	velmi	k6eAd1
oblíbeným	oblíbený	k2eAgInSc7d1
vozem	vůz	k1gInSc7
Hippies	Hippiesa	k1gFnPc2
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
dle	dle	k7c2
trendů	trend	k1gInPc2
v	v	k7c6
automobilovém	automobilový	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
narostl	narůst	k5eAaPmAgInS
do	do	k7c2
všech	všecek	k3xTgInPc2
směrů	směr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Prodeje	prodej	k1gInPc1
vozů	vůz	k1gInPc2
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Koncern	koncern	k1gInSc1
Volkswagen	volkswagen	k1gInSc1
vyrábí	vyrábět	k5eAaImIp3nS
podle	podle	k7c2
potřeb	potřeba	k1gFnPc2
a	a	k8xC
požadavků	požadavek	k1gInPc2
kupujících	kupující	k2eAgInPc2d1
výrobky	výrobek	k1gInPc7
vysoké	vysoký	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
odbyt	odbyt	k1gInSc1
a	a	k8xC
péče	péče	k1gFnSc1
o	o	k7c4
ně	on	k3xPp3gMnPc4
jsou	být	k5eAaImIp3nP
v	v	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
zajišťovány	zajišťovat	k5eAaImNgInP
sítí	síť	k1gFnSc7
autorizovaných	autorizovaný	k2eAgInPc2d1
dealerů	dealer	k1gMnPc2
<g/>
,	,	kIx,
za	za	k7c7
účelem	účel	k1gInSc7
dosažení	dosažení	k1gNnSc2
vysoké	vysoký	k2eAgFnSc2d1
spokojenosti	spokojenost	k1gFnSc2
zákazníků	zákazník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
autorizovaní	autorizovaný	k2eAgMnPc1d1
dealeři	dealer	k1gMnPc1
<g/>
,	,	kIx,
kterých	který	k3yIgMnPc2,k3yQgMnPc2,k3yRgMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
v	v	k7c6
ČR	ČR	kA
19	#num#	k4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
splňovat	splňovat	k5eAaImF
kvalitativní	kvalitativní	k2eAgInPc4d1
standardy	standard	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
jednotný	jednotný	k2eAgInSc4d1
design	design	k1gInSc4
autosalónů	autosalón	k1gInPc2
<g/>
,	,	kIx,
Corporate	Corporat	k1gInSc5
design	design	k1gInSc1
<g/>
,	,	kIx,
povinný	povinný	k2eAgInSc1d1
počet	počet	k1gInSc1
předváděcích	předváděcí	k2eAgInPc2d1
a	a	k8xC
skladových	skladový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
a	a	k8xC
apod.	apod.	kA
Tímto	tento	k3xDgInSc7
si	se	k3xPyFc3
koncern	koncern	k1gInSc1
Audi	Audi	k1gNnSc1
udržuje	udržovat	k5eAaImIp3nS
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
dohlíží	dohlížet	k5eAaImIp3nS
na	na	k7c4
kvalitu	kvalita	k1gFnSc4
služeb	služba	k1gFnPc2
poskytovanou	poskytovaný	k2eAgFnSc4d1
všem	všecek	k3xTgMnPc3
stávajícím	stávající	k2eAgMnPc3d1
i	i	k8xC
novým	nový	k2eAgMnPc3d1
zákazníkům	zákazník	k1gMnPc3
koncernu	koncern	k1gInSc2
Audi	Audi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
1998	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
Osobní	osobní	k2eAgInPc4d1
vozy	vůz	k1gInPc4
</s>
<s>
6	#num#	k4
746	#num#	k4
</s>
<s>
8	#num#	k4
151	#num#	k4
</s>
<s>
7	#num#	k4
836	#num#	k4
</s>
<s>
9	#num#	k4
517	#num#	k4
</s>
<s>
7	#num#	k4
803	#num#	k4
</s>
<s>
7	#num#	k4
640	#num#	k4
</s>
<s>
9	#num#	k4
098	#num#	k4
</s>
<s>
6	#num#	k4
926	#num#	k4
</s>
<s>
7	#num#	k4
116	#num#	k4
</s>
<s>
Užitkové	užitkový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
(	(	kIx(
<g/>
N	N	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
718	#num#	k4
</s>
<s>
1	#num#	k4
902	#num#	k4
</s>
<s>
2	#num#	k4
109	#num#	k4
</s>
<s>
2	#num#	k4
171	#num#	k4
</s>
<s>
2	#num#	k4
129	#num#	k4
</s>
<s>
2	#num#	k4
440	#num#	k4
</s>
<s>
4	#num#	k4
286	#num#	k4
</s>
<s>
4	#num#	k4
779	#num#	k4
</s>
<s>
6	#num#	k4
018	#num#	k4
</s>
<s>
Prodeje	prodej	k1gInPc4
vozů	vůz	k1gInPc2
1998-2006	1998-2006	k4
</s>
<s>
Automobily	automobil	k1gInPc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
</s>
<s>
Amarok	Amarok	k1gInSc1
</s>
<s>
Arteon	Arteon	k1gMnSc1
</s>
<s>
Caddy	Caddy	k6eAd1
</s>
<s>
Crafter	Crafter	k1gMnSc1
</s>
<s>
Golf	golf	k1gInSc1
</s>
<s>
Passat	Passat	k5eAaImF,k5eAaPmF
</s>
<s>
Polo	polo	k6eAd1
</s>
<s>
Sharan	Sharan	k1gInSc1
</s>
<s>
Tiguan	Tiguan	k1gMnSc1
</s>
<s>
Touareg	Touareg	k1gMnSc1
</s>
<s>
Touran	Touran	k1gInSc1
</s>
<s>
T-Roc	T-Roc	k6eAd1
</s>
<s>
Transporter	Transporter	k1gMnSc1
</s>
<s>
Up	Up	k?
<g/>
!	!	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
starších	starý	k2eAgInPc2d2
modelů	model	k1gInPc2
</s>
<s>
Bora	Bora	k6eAd1
</s>
<s>
Corrado	Corrada	k1gFnSc5
</s>
<s>
Karmann	Karmann	k1gMnSc1
Ghia	Ghia	k1gMnSc1
</s>
<s>
LT	LT	kA
</s>
<s>
Lupo	lupa	k1gFnSc5
</s>
<s>
Phaeton	Phaeton	k1gInSc1
</s>
<s>
New	New	k?
Beetle	Beetle	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jürgen	Jürgen	k1gInSc1
Lewandowski	Lewandowsk	k1gFnSc2
<g/>
:	:	kIx,
VW	VW	kA
Typen	Typen	k1gInSc1
und	und	k?
Geschichte	Geschicht	k1gMnSc5
<g/>
,	,	kIx,
Steiger-Verlag	Steiger-Verlag	k1gInSc1
<g/>
,	,	kIx,
Augsburg	Augsburg	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
89652	#num#	k4
<g/>
-	-	kIx~
<g/>
126	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
8	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOL	Kola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
vytvořit	vytvořit	k5eAaPmF
atraktivní	atraktivní	k2eAgInSc4d1
obchodní	obchodní	k2eAgInSc4d1
název	název	k1gInSc4
firmy	firma	k1gFnSc2
<g/>
,	,	kIx,
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
produktu	produkt	k1gInSc2
<g/>
,	,	kIx,
značky	značka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NZB	NZB	kA
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
340	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904272	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
324	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Aktien-Analyse	Aktien-Analysa	k1gFnSc6
<g/>
:	:	kIx,
Volkswagen	volkswagen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staženo	stáhnout	k5eAaPmNgNnS
elektronicky	elektronicky	k6eAd1
jako	jako	k9
PDF	PDF	kA
od	od	k7c2
analytické	analytický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
GeVestor	GeVestor	k1gMnSc1
(	(	kIx(
<g/>
zdarma	zdarma	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORÁČEK	Horáček	k1gMnSc1
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
<g/>
;	;	kIx,
ČERNÝ	Černý	k1gMnSc1
<g/>
,	,	kIx,
Aleč	Aleč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aféra	aféra	k1gFnSc1
s	s	k7c7
emisemi	emise	k1gFnPc7
se	se	k3xPyFc4
podle	podle	k7c2
Volkswagenu	volkswagen	k1gInSc2
celosvětově	celosvětově	k6eAd1
týká	týkat	k5eAaImIp3nS
11	#num#	k4
milionů	milion	k4xCgInPc2
aut	auto	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
POLÁK	Polák	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéf	šéf	k1gMnSc1
Volkswagenu	volkswagen	k1gInSc2
Winterkorn	Winterkorn	k1gMnSc1
odstoupil	odstoupit	k5eAaPmAgMnS
kvůli	kvůli	k7c3
skandálu	skandál	k1gInSc3
s	s	k7c7
emisemi	emise	k1gFnPc7
z	z	k7c2
funkce	funkce	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Berlín	Berlín	k1gInSc1
<g/>
:	:	kIx,
Volkswagen	volkswagen	k1gInSc1
manipuloval	manipulovat	k5eAaImAgInS
s	s	k7c7
emisemi	emise	k1gFnPc7
i	i	k8xC
u	u	k7c2
vozů	vůz	k1gInPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
První	první	k4xOgFnSc1
čtyři	čtyři	k4xCgMnPc1
Češi	Čech	k1gMnPc1
podali	podat	k5eAaPmAgMnP
žalobu	žaloba	k1gFnSc4
o	o	k7c4
odškodnění	odškodnění	k1gNnSc4
v	v	k7c6
kauze	kauza	k1gFnSc6
Dieselgate	Dieselgat	k1gInSc5
-	-	kIx~
Seznam	seznam	k1gInSc4
Zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
www.seznamzpravy.cz	www.seznamzpravy.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Volkswagen	volkswagen	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.volkswagen.cz	www.volkswagen.cz	k1gMnSc1
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
www.volkswagen.de	www.volkswagen.de	k6eAd1
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
německy	německy	k6eAd1
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Cult	Culta	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Volkswagen	volkswagen	k1gInSc1
AG	AG	kA
Group	Group	k1gInSc4
Osobní	osobní	k2eAgInPc4d1
automobily	automobil	k1gInPc4
</s>
<s>
Audi	Audi	k1gNnSc1
•	•	k?
Bentley	Bentlea	k1gMnSc2
•	•	k?
Bugatti	Bugatť	k1gFnSc2
•	•	k?
Lamborghini	Lamborghin	k1gMnPc1
•	•	k?
SEAT	SEAT	kA
•	•	k?
Porsche	Porsche	k1gNnSc1
•	•	k?
Škoda	Škoda	k1gMnSc1
•	•	k?
Volkswagen	volkswagen	k1gInSc1
Nákladní	nákladní	k2eAgInPc1d1
automobily	automobil	k1gInPc1
a	a	k8xC
další	další	k2eAgNnPc1d1
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Užitkové	užitkový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
•	•	k?
Scania	Scanium	k1gNnSc2
•	•	k?
MAN	Man	k1gMnSc1
(	(	kIx(
<g/>
podíl	podíl	k1gInSc1
75,03	75,03	k4
%	%	kIx~
<g/>
)	)	kIx)
•	•	k?
Ducati	ducat	k5eAaImF
(	(	kIx(
<g/>
motocykly	motocykl	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Německé	německý	k2eAgFnSc2d1
automobilové	automobilový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
1918	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
</s>
<s>
AAA	AAA	kA
•	•	k?
ABC	ABC	kA
•	•	k?
Adler	Adler	k1gMnSc1
•	•	k?
AGA	aga	k1gMnSc1
•	•	k?
Alan	Alan	k1gMnSc1
•	•	k?
Alfi	Alf	k1gFnSc2
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alfi	Alfi	k1gNnSc4
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AMBAG	AMBAG	kA
•	•	k?
Amor	Amor	k1gMnSc1
•	•	k?
Anker	Anker	k1gMnSc1
•	•	k?
Apollo	Apollo	k1gMnSc1
•	•	k?
Argeo	Argeo	k1gNnSc4
•	•	k?
Arimofa	Arimof	k1gMnSc2
•	•	k?
Atlantic	Atlantice	k1gFnPc2
•	•	k?
Audi	Audi	k1gNnSc4
•	•	k?
Auto-Ell	Auto-Ell	k1gInSc1
•	•	k?
Badenia	Badenium	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Baer	Baer	k1gMnSc1
•	•	k?
BAW	BAW	kA
•	•	k?
BEB	BEB	kA
•	•	k?
Beckmann	Beckmann	k1gMnSc1
•	•	k?
Benz	Benz	k1gMnSc1
•	•	k?
Bergmann	Bergmann	k1gMnSc1
•	•	k?
Bergo	Bergo	k1gMnSc1
•	•	k?
BF	BF	kA
•	•	k?
Biene	Bien	k1gInSc5
•	•	k?
Bleichert	Bleichert	k1gInSc1
•	•	k?
BMW	BMW	kA
•	•	k?
Bob	Bob	k1gMnSc1
•	•	k?
Borcharding	Borcharding	k1gInSc1
•	•	k?
Borgward	Borgward	k1gMnSc1
•	•	k?
Bravo	bravo	k1gMnSc1
•	•	k?
Brennabor	Brennabor	k1gMnSc1
•	•	k?
Bufag	Bufag	k1gMnSc1
•	•	k?
Bully	bulla	k1gFnSc2
•	•	k?
Butz	Butz	k1gMnSc1
•	•	k?
BZ	bz	k0
•	•	k?
C.	C.	kA
Benz	Benz	k1gInSc1
Söhne	Söhn	k1gInSc5
•	•	k?
Certus	Certus	k1gInSc1
•	•	k?
Club	club	k1gInSc1
•	•	k?
Cockerell	Cockerell	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Combi	Comb	k1gFnSc2
•	•	k?
Cyklon	cyklon	k1gInSc1
•	•	k?
Davidl	Davidl	k1gFnSc2
•	•	k?
Dehn	Dehn	k1gInSc1
•	•	k?
DEW	DEW	kA
•	•	k?
Diabolo	diabolo	k1gNnSc1
•	•	k?
Diana	Diana	k1gFnSc1
•	•	k?
Dinos	Dinos	k1gInSc1
•	•	k?
Dixi	Dix	k1gFnSc2
•	•	k?
DKW	DKW	kA
•	•	k?
Dorner	Dorner	k1gMnSc1
•	•	k?
Dürkopp	Dürkopp	k1gMnSc1
•	•	k?
Dux	Dux	k1gMnSc1
•	•	k?
D-Wagen	D-Wagen	k1gInSc1
•	•	k?
EBS	EBS	kA
•	•	k?
Ego	ego	k1gNnSc2
•	•	k?
Ehrhardt	Ehrhardt	k1gMnSc1
•	•	k?
Ehrhardt-Szawe	Ehrhardt-Szawe	k1gInSc1
•	•	k?
Eibach	Eibach	k1gInSc1
•	•	k?
Electra	Electr	k1gMnSc2
•	•	k?
Elektric	Elektric	k1gMnSc1
•	•	k?
Elite	Elit	k1gInSc5
•	•	k?
Elitewagen	Elitewagen	k1gInSc1
•	•	k?
Eos	Eos	k1gFnSc2
•	•	k?
Erco	Erco	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Espenlaub	Espenlaub	k1gInSc1
•	•	k?
Eubu	Eubus	k1gInSc2
•	•	k?
Exor	Exor	k1gMnSc1
•	•	k?
Fadag	Fadag	k1gMnSc1
•	•	k?
Fafag	Fafag	k1gMnSc1
•	•	k?
Fafnir	Fafnir	k1gMnSc1
•	•	k?
Falcon	Falcon	k1gMnSc1
•	•	k?
Fama	Fama	k?
•	•	k?
Faun	fauna	k1gFnPc2
•	•	k?
Ferbedo	Ferbedo	k1gNnSc4
•	•	k?
Ford	ford	k1gInSc1
•	•	k?
Fox	fox	k1gInSc1
•	•	k?
Framo	Frama	k1gFnSc5
•	•	k?
Freia	Freium	k1gNnSc2
•	•	k?
Fulmina	Fulmin	k2eAgFnSc1d1
•	•	k?
Garbaty	Garbat	k1gInPc4
•	•	k?
Gasi	Gas	k1gFnSc2
•	•	k?
Goliath	Goliath	k1gInSc1
•	•	k?
Görke	Görke	k1gInSc1
•	•	k?
Grade	grad	k1gInSc5
•	•	k?
Gridi	Grid	k1gMnPc1
•	•	k?
Gries	Gries	k1gMnSc1
•	•	k?
Habag	Habag	k1gMnSc1
•	•	k?
HAG	HAG	kA
•	•	k?
HAG-Gastell	HAG-Gastell	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Hagea-Moto	Hagea-Mota	k1gFnSc5
•	•	k?
Hanomag	Hanomag	k1gInSc1
•	•	k?
Hansa	hansa	k1gFnSc1
•	•	k?
Hansa-Lloyd	Hansa-Lloyd	k1gInSc1
•	•	k?
Hascho	Hascha	k1gFnSc5
•	•	k?
Hataz	Hataz	k1gInSc1
•	•	k?
Hawa	Haw	k1gInSc2
•	•	k?
Heim	Heim	k1gMnSc1
•	•	k?
Helios	Helios	k1gMnSc1
•	•	k?
Helo	Hela	k1gFnSc5
•	•	k?
Hercules	Hercules	k1gMnSc1
•	•	k?
Hero	Hero	k1gMnSc1
•	•	k?
Hildebrand	Hildebrand	k1gInSc1
•	•	k?
Hiller	Hiller	k1gInSc1
•	•	k?
Horch	Horch	k1gInSc1
•	•	k?
HT	HT	kA
•	•	k?
Imperia	Imperium	k1gNnSc2
•	•	k?
Induhag	Induhag	k1gMnSc1
•	•	k?
Ipe	Ipe	k1gMnSc1
•	•	k?
Joswin	Joswin	k1gMnSc1
•	•	k?
Juhö	Juhö	k1gMnSc1
•	•	k?
Kaha	Kaha	k1gMnSc1
•	•	k?
Kaiser	Kaiser	k1gMnSc1
•	•	k?
Keitel	Keitel	k1gMnSc1
•	•	k?
Kenter	Kenter	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Kico	Kico	k1gMnSc1
•	•	k?
Kieling	Kieling	k1gInSc1
•	•	k?
Knöllner	Knöllner	k1gMnSc1
•	•	k?
Kobold	kobold	k1gMnSc1
•	•	k?
Koco	Koco	k6eAd1
•	•	k?
Komet	kometa	k1gFnPc2
•	•	k?
Komnick	Komnick	k1gMnSc1
•	•	k?
Körting	Körting	k1gInSc1
•	•	k?
Kühn	Kühn	k1gInSc1
•	•	k?
Landgrebe	Landgreb	k1gInSc5
•	•	k?
Lauer	Lauer	k1gMnSc1
•	•	k?
Leichtauto	Leichtaut	k2eAgNnSc1d1
•	•	k?
Leifa	Leif	k1gMnSc2
•	•	k?
Lesshaft	Lesshaft	k1gMnSc1
•	•	k?
Ley	Lea	k1gFnSc2
•	•	k?
Libelle	Libelle	k1gFnSc2
•	•	k?
Lindcar	Lindcar	k1gMnSc1
•	•	k?
Lipsia	Lipsia	k1gFnSc1
•	•	k?
Loeb	Loeb	k1gInSc1
•	•	k?
Luther	Luthra	k1gFnPc2
&	&	k?
Heyer	Heyer	k1gMnSc1
•	•	k?
LuWe	LuW	k1gFnSc2
•	•	k?
Luwo	Luwo	k1gMnSc1
•	•	k?
Lux	Lux	k1gMnSc1
•	•	k?
Macu	Maca	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
MAF	MAF	kA
•	•	k?
Magnet	magnet	k1gInSc1
•	•	k?
Maier	Maier	k1gInSc1
•	•	k?
Maja	Maja	k1gFnSc1
•	•	k?
Mannesmann	Mannesmann	k1gInSc1
•	•	k?
Martinette	Martinett	k1gInSc5
•	•	k?
Maurer	Maurer	k1gMnSc1
•	•	k?
Mauser	Mauser	k1gMnSc1
•	•	k?
Maybach	Maybach	k1gMnSc1
•	•	k?
Mayrette	Mayrett	k1gInSc5
•	•	k?
Mercedes	mercedes	k1gInSc1
•	•	k?
Mercedes-Benz	Mercedes-Benz	k1gInSc1
•	•	k?
MFB	MFB	kA
•	•	k?
Mikromobil	Mikromobil	k1gMnSc1
•	•	k?
Minimus	Minimus	k1gMnSc1
•	•	k?
Möckwagen	Möckwagen	k1gInSc1
•	•	k?
Mölkamp	Mölkamp	k1gInSc1
•	•	k?
Moll	moll	k1gNnSc2
•	•	k?
Monos	Monos	k1gMnSc1
•	•	k?
Mops	mops	k1gMnSc1
•	•	k?
Morgan	morgan	k1gMnSc1
•	•	k?
Motobil	Motobil	k1gMnSc1
•	•	k?
Motrix	Motrix	k1gInSc1
•	•	k?
Muvo	Muvo	k6eAd1
•	•	k?
Nafa	Nafa	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
NAG	NAG	kA
•	•	k?
NAG-Presto	NAG-Presta	k1gMnSc5
•	•	k?
NAG-Protos	NAG-Protos	k1gMnSc1
•	•	k?
Nawa	Nawa	k1gMnSc1
•	•	k?
Neander	Neander	k1gMnSc1
•	•	k?
Neiman	Neiman	k1gMnSc1
•	•	k?
Nemalette	Nemalett	k1gInSc5
•	•	k?
Nenndorf	Nenndorf	k1gMnSc1
•	•	k?
Nowa	Nowa	k1gMnSc1
•	•	k?
NSU	NSU	kA
•	•	k?
NSU-Fiat	NSU-Fiat	k1gInSc1
•	•	k?
Nufmobil	Nufmobil	k1gFnSc2
•	•	k?
Nug	Nug	k1gMnSc1
•	•	k?
Omega	omega	k1gFnSc1
•	•	k?
Omikron	omikron	k1gInSc1
•	•	k?
Omnobil	Omnobil	k1gFnSc2
•	•	k?
Onnasch	Onnasch	k1gInSc1
•	•	k?
Opel	opel	k1gInSc1
•	•	k?
Otto	Otto	k1gMnSc1
•	•	k?
Pawi	Paw	k1gFnSc2
•	•	k?
Pe-Ka	Pe-Ka	k1gMnSc1
•	•	k?
Peer	peer	k1gMnSc1
Gynt	Gynt	k1gMnSc1
•	•	k?
Pelikan	Pelikan	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Moritz	moritz	k1gInSc1
•	•	k?
Pfeil	Pfeil	k1gInSc1
•	•	k?
Phänomen	Phänomen	k2eAgInSc1d1
•	•	k?
Pilot	pilot	k1gInSc1
•	•	k?
Pluto	Pluto	k1gMnSc1
•	•	k?
Presto	presto	k6eAd1
•	•	k?
Priamus	Priamus	k1gMnSc1
•	•	k?
Protos	Protos	k1gMnSc1
•	•	k?
Rabag	Rabag	k1gMnSc1
•	•	k?
Remag	Remag	k1gMnSc1
•	•	k?
Renfert	Renfert	k1gMnSc1
•	•	k?
Rex-Simplex	Rex-Simplex	k1gInSc1
•	•	k?
Rhemag	Rhemag	k1gMnSc1
•	•	k?
Rikas	Rikas	k1gMnSc1
•	•	k?
Rivo	Rivo	k1gMnSc1
•	•	k?
Roland	Roland	k1gInSc1
•	•	k?
Röhr	Röhr	k1gInSc1
•	•	k?
Rollfix	Rollfix	k1gInSc1
•	•	k?
Rumpler	Rumpler	k1gInSc1
•	•	k?
Rüttger	Rüttger	k1gInSc1
•	•	k?
RWN	RWN	kA
•	•	k?
Sablatnig-Beuchelt	Sablatnig-Beuchelt	k1gInSc1
•	•	k?
Sauer	Sauer	k1gInSc1
•	•	k?
SB	sb	kA
•	•	k?
Schebera	Schebera	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Schönnagel	Schönnagel	k1gMnSc1
•	•	k?
Schuricht	Schuricht	k1gMnSc1
•	•	k?
Schütte-Lanz	Schütte-Lanz	k1gMnSc1
•	•	k?
Seidel-Arop	Seidel-Arop	k1gInSc1
•	•	k?
Selve	Selev	k1gFnSc2
•	•	k?
SHW	SHW	kA
•	•	k?
Simson	Simson	k1gMnSc1
•	•	k?
Slaby-Beringer	Slaby-Beringer	k1gMnSc1
•	•	k?
Slevogt	Slevogt	k1gMnSc1
•	•	k?
Solomobil	Solomobil	k1gMnSc1
•	•	k?
Sperber	Sperber	k1gMnSc1
•	•	k?
Sphinx	Sphinx	k1gInSc1
•	•	k?
Spinell	Spinell	k1gInSc1
•	•	k?
Staiger	Staiger	k1gInSc1
•	•	k?
Standard	standard	k1gInSc1
•	•	k?
Steiger	Steiger	k1gInSc1
•	•	k?
Stoewer	Stoewer	k1gInSc1
•	•	k?
Stolle	Stolle	k1gFnSc2
•	•	k?
Sun	Sun	kA
•	•	k?
Szawe	Szawe	k1gInSc1
•	•	k?
Tamag	Tamag	k1gInSc1
•	•	k?
Tamm	Tamm	k1gInSc1
•	•	k?
Tatra	Tatra	k1gFnSc1
•	•	k?
Teco	Teco	k6eAd1
•	•	k?
Tempo	tempo	k1gNnSc4
•	•	k?
Theis	Theis	k1gInSc1
•	•	k?
Tornax	Tornax	k1gInSc1
•	•	k?
Tourist	Tourist	k1gInSc1
•	•	k?
Traeger	Traeger	k1gInSc1
•	•	k?
Trinks	Trinks	k1gInSc1
•	•	k?
Trippel	Trippel	k1gInSc1
•	•	k?
Triumph	Triumph	k1gInSc1
•	•	k?
Turbo	turba	k1gFnSc5
Utilitas	Utilitas	k1gInSc4
•	•	k?
VL	VL	kA
•	•	k?
Voran	Voran	k1gInSc1
•	•	k?
Volkswagen	volkswagen	k1gInSc1
•	•	k?
Walmobil	Walmobil	k1gFnSc2
•	•	k?
Wanderer	Wanderer	k1gMnSc1
•	•	k?
Wegmann	Wegmann	k1gMnSc1
•	•	k?
Weise	Weise	k1gFnSc2
•	•	k?
Wesnigk	Wesnigk	k1gInSc1
•	•	k?
Westfalia	Westfalius	k1gMnSc2
•	•	k?
Winkler	Winkler	k1gMnSc1
•	•	k?
Wittekind	Wittekind	k1gMnSc1
•	•	k?
York	York	k1gInSc1
•	•	k?
Zetgelette	Zetgelett	k1gInSc5
•	•	k?
Zündapp	Zündapp	k1gMnSc1
•	•	k?
Zwerg	Zwerg	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
