<p>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS	Spytihnět
I.	I.	kA	I.
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
875	[number]	k4	875
–	–	k?	–
915	[number]	k4	915
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Spytihněvovým	Spytihněvův	k2eAgMnSc7d1	Spytihněvův
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
následník	následník	k1gMnSc1	následník
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
knížecím	knížecí	k2eAgInSc6d1	knížecí
stolci	stolec	k1gInSc6	stolec
Vratislav	Vratislav	k1gMnSc1	Vratislav
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Mezi	mezi	k7c7	mezi
Velkou	velký	k2eAgFnSc7d1	velká
Moravou	Morava	k1gFnSc7	Morava
a	a	k8xC	a
Bavorskem	Bavorsko	k1gNnSc7	Bavorsko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
otcovy	otcův	k2eAgFnSc2d1	otcova
smrti	smrt	k1gFnSc2	smrt
nebyl	být	k5eNaImAgInS	být
ještě	ještě	k6eAd1	ještě
zletilý	zletilý	k2eAgInSc1d1	zletilý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Čechy	Čechy	k1gFnPc1	Čechy
spadaly	spadat	k5eAaPmAgFnP	spadat
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
pod	pod	k7c4	pod
přímou	přímý	k2eAgFnSc4d1	přímá
vládu	vláda	k1gFnSc4	vláda
velkomoravského	velkomoravský	k2eAgMnSc2d1	velkomoravský
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
mladého	mladý	k2eAgMnSc2d1	mladý
Přemyslovce	Přemyslovec	k1gMnSc2	Přemyslovec
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
se	se	k3xPyFc4	se
časově	časově	k6eAd1	časově
přibližně	přibližně	k6eAd1	přibližně
překrýval	překrývat	k5eAaImAgMnS	překrývat
se	s	k7c7	s
skonem	skon	k1gInSc7	skon
nejslavnějšího	slavný	k2eAgMnSc2d3	nejslavnější
panovníka	panovník	k1gMnSc2	panovník
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
894	[number]	k4	894
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rodící	rodící	k2eAgInSc1d1	rodící
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
odpoutal	odpoutat	k5eAaPmAgInS	odpoutat
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
východofranskou	východofranský	k2eAgFnSc4d1	Východofranská
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
895	[number]	k4	895
vymanily	vymanit	k5eAaPmAgFnP	vymanit
ze	z	k7c2	z
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
velkomoravské	velkomoravský	k2eAgFnSc6d1	Velkomoravská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Fuldských	Fuldský	k2eAgInPc2d1	Fuldský
letopisů	letopis	k1gInPc2	letopis
se	se	k3xPyFc4	se
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
Říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzdal	vzdát	k5eAaPmAgMnS	vzdát
poctu	pocta	k1gFnSc4	pocta
Východofranckému	východofrancký	k2eAgMnSc3d1	východofrancký
králi	král	k1gMnSc3	král
Arnulfu	Arnulf	k1gMnSc3	Arnulf
Korutanskému	korutanský	k2eAgMnSc3d1	korutanský
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
latinské	latinský	k2eAgFnSc2d1	Latinská
liturgie	liturgie	k1gFnSc2	liturgie
byl	být	k5eAaImAgInS	být
proto	proto	k6eAd1	proto
prvním	první	k4xOgMnSc7	první
křesťanským	křesťanský	k2eAgMnSc7d1	křesťanský
vládcem	vládce	k1gMnSc7	vládce
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
Metoděj	Metoděj	k1gMnSc1	Metoděj
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
už	už	k6eAd1	už
Bořivoje	Bořivoj	k1gMnSc4	Bořivoj
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Spytihněv	Spytihněv	k1gMnSc3	Spytihněv
I.	I.	kA	I.
jako	jako	k8xC	jako
kníže	kníže	k1gMnSc1	kníže
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Spytihněva	Spytihněva	k1gFnSc1	Spytihněva
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
plošina	plošina	k1gFnSc1	plošina
ostrohu	ostroh	k1gInSc2	ostroh
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
nechal	nechat	k5eAaPmAgMnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
kostelík	kostelík	k1gInSc4	kostelík
Panny	Panna	k1gFnSc2	Panna
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
obehnána	obehnán	k2eAgNnPc4d1	obehnáno
obranným	obranný	k2eAgInSc7d1	obranný
valem	val	k1gInSc7	val
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zde	zde	k6eAd1	zde
knížecí	knížecí	k2eAgInSc1d1	knížecí
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
kníže	kníže	k1gMnSc1	kníže
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
z	z	k7c2	z
Levého	levý	k2eAgInSc2d1	levý
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centr	k1gInSc7	centr
rodícího	rodící	k2eAgMnSc2d1	rodící
se	se	k3xPyFc4	se
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS	Spytihnět
je	on	k3xPp3gMnPc4	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
zakladatelem	zakladatel	k1gMnSc7	zakladatel
skupiny	skupina	k1gFnSc2	skupina
hradišť	hradiště	k1gNnPc2	hradiště
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
stála	stát	k5eAaImAgNnP	stát
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
původní	původní	k2eAgFnSc2d1	původní
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
domény	doména	k1gFnSc2	doména
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
knížecího	knížecí	k2eAgNnSc2d1	knížecí
rodového	rodový	k2eAgNnSc2d1	rodové
patrimonia	patrimonium	k1gNnSc2	patrimonium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hradiště	hradiště	k1gNnSc4	hradiště
Tetín	Tetín	k1gInSc1	Tetín
<g/>
,	,	kIx,	,
Libušín	Libušín	k1gInSc1	Libušín
<g/>
,	,	kIx,	,
Budeč	Budeč	k1gFnSc1	Budeč
<g/>
,	,	kIx,	,
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
a	a	k8xC	a
Lštění	Lštění	k1gMnSc1	Lštění
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
hradiště	hradiště	k1gNnPc4	hradiště
je	být	k5eAaImIp3nS	být
společná	společný	k2eAgFnSc1d1	společná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
-	-	kIx~	-
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
způsob	způsob	k1gInSc1	způsob
výběru	výběr	k1gInSc2	výběr
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
,	,	kIx,	,
i	i	k8xC	i
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
jsou	být	k5eAaImIp3nP	být
zmiňována	zmiňován	k2eAgFnSc1d1	zmiňována
v	v	k7c6	v
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
(	(	kIx(	(
<g/>
vždy	vždy	k6eAd1	vždy
druhořadé	druhořadý	k2eAgNnSc1d1	druhořadé
vůči	vůči	k7c3	vůči
Pražskému	pražský	k2eAgInSc3d1	pražský
hradu	hrad	k1gInSc3	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
hradišť	hradiště	k1gNnPc2	hradiště
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
začali	začít	k5eAaPmAgMnP	začít
přemyslovští	přemyslovský	k2eAgMnPc1d1	přemyslovský
vládci	vládce	k1gMnPc1	vládce
spravovat	spravovat	k5eAaImF	spravovat
okolní	okolní	k2eAgNnSc4d1	okolní
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
podřídili	podřídit	k5eAaPmAgMnP	podřídit
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
přiměli	přimět	k5eAaPmAgMnP	přimět
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
odvádění	odvádění	k1gNnSc3	odvádění
dávek	dávka	k1gFnPc2	dávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spytihněvovou	Spytihněvový	k2eAgFnSc7d1	Spytihněvový
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
žena	žena	k1gFnSc1	žena
pohřbená	pohřbený	k2eAgFnSc1d1	pohřbená
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
hrobky	hrobka	k1gFnSc2	hrobka
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
pod	pod	k7c7	pod
podlahou	podlaha	k1gFnSc7	podlaha
kostela	kostel	k1gInSc2	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
náušnice	náušnice	k1gFnPc1	náušnice
pocházejí	pocházet	k5eAaImIp3nP	pocházet
nejvýš	vysoce	k6eAd3	vysoce
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
desetiletí	desetiletí	k1gNnPc2	desetiletí
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejich	jejich	k3xOp3gFnPc6	jejich
případných	případný	k2eAgFnPc6d1	případná
potomcích	potomek	k1gMnPc6	potomek
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Knížecí	knížecí	k2eAgNnSc1d1	knížecí
rodové	rodový	k2eAgNnSc1d1	rodové
patrimonium	patrimonium	k1gNnSc1	patrimonium
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
I.	I.	kA	I.
Zrození	zrození	k1gNnSc1	zrození
státu	stát	k1gInSc2	stát
872	[number]	k4	872
<g/>
-	-	kIx~	-
<g/>
972	[number]	k4	972
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
po	po	k7c4	po
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
Čechů	Čech	k1gMnPc2	Čech
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
530	[number]	k4	530
<g/>
–	–	k?	–
<g/>
935	[number]	k4	935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
658	[number]	k4	658
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spytihněv	Spytihněv	k1gFnPc2	Spytihněv
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
