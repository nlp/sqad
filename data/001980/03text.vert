<s>
Lilongwe	Lilongwe	k6eAd1	Lilongwe
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
afrického	africký	k2eAgInSc2d1	africký
státu	stát	k1gInSc2	stát
Malawi	Malawi	k1gNnSc2	Malawi
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
902	[number]	k4	902
388	[number]	k4	388
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
050	[number]	k4	050
m.	m.	k?	m.
Nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
Malawim	Malawi	k1gNnSc7	Malawi
<g/>
,	,	kIx,	,
Mosambikem	Mosambik	k1gInSc7	Mosambik
a	a	k8xC	a
Zambií	Zambie	k1gFnSc7	Zambie
<g/>
.	.	kIx.	.
</s>
<s>
Lilongwe	Lilongwe	k6eAd1	Lilongwe
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Malawi	Malawi	k1gNnSc6	Malawi
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc1d2	veliký
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
Blantyre	Blantyr	k1gMnSc5	Blantyr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídli	sídlet	k5eAaImRp2nS	sídlet
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
Blantyre	Blantyr	k1gInSc5	Blantyr
hlavní	hlavní	k2eAgFnSc7d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
z	z	k7c2	z
Zomby	Zomba	k1gFnSc2	Zomba
do	do	k7c2	do
Lilongwe	Lilongw	k1gFnSc2	Lilongw
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
v	v	k7c6	v
celku	celek	k1gInSc6	celek
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
,	,	kIx,	,
suché	suchý	k2eAgInPc1d1	suchý
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
na	na	k7c4	na
teploty	teplota	k1gFnPc4	teplota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
přes	přes	k7c4	přes
den	den	k1gInSc4	den
a	a	k8xC	a
dny	den	k1gInPc4	den
se	s	k7c7	s
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
