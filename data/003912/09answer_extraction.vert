Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
společná	společný	k2eAgFnSc1d1	společná
hranice	hranice	k1gFnSc1	hranice
41,2	[number]	k4	41,2
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
hospodářsky	hospodářsky	k6eAd1	hospodářsky
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
spojeno	spojit	k5eAaPmNgNnS	spojit
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
měnovou	měnový	k2eAgFnSc4d1	měnová
a	a	k8xC	a
celní	celní	k2eAgFnSc2d1	celní
unií	unie	k1gFnSc7	unie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
