<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
<g/>
,	,	kIx,	,
úředně	úředně	k6eAd1	úředně
Lichtenštejnské	lichtenštejnský	k2eAgNnSc4d1	Lichtenštejnské
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Fürstentum	Fürstentum	k1gNnSc1	Fürstentum
Liechtenstein	Liechtenstein	k1gMnSc1	Liechtenstein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgInPc2d3	nejmenší
států	stát	k1gInPc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Alp	Alpy	k1gFnPc2	Alpy
(	(	kIx(	(
<g/>
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Rätikon	Rätikon	k1gMnSc1	Rätikon
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Rýna	Rýn	k1gInSc2	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
na	na	k7c4	na
160	[number]	k4	160
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
37	[number]	k4	37
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
11	[number]	k4	11
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Vaduz	Vaduz	k1gInSc4	Vaduz
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
je	být	k5eAaImIp3nS	být
Schaan	Schaan	k1gMnSc1	Schaan
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
společná	společný	k2eAgFnSc1d1	společná
hranice	hranice	k1gFnSc1	hranice
41,2	[number]	k4	41,2
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
hospodářsky	hospodářsky	k6eAd1	hospodářsky
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
spojeno	spojit	k5eAaPmNgNnS	spojit
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
měnovou	měnový	k2eAgFnSc4d1	měnová
a	a	k8xC	a
celní	celní	k2eAgFnSc2d1	celní
unií	unie	k1gFnSc7	unie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
(	(	kIx(	(
<g/>
společná	společný	k2eAgFnSc1d1	společná
hranice	hranice	k1gFnSc1	hranice
36,7	[number]	k4	36,7
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vklíněno	vklíněn	k2eAgNnSc1d1	vklíněno
mezi	mezi	k7c4	mezi
území	území	k1gNnSc4	území
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spolu	spolu	k6eAd1	spolu
na	na	k7c4	na
sever	sever	k1gInSc4	sever
i	i	k9	i
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
hraničí	hraničit	k5eAaImIp3nS	hraničit
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc1d1	konstituční
monarchie	monarchie	k1gFnSc1	monarchie
(	(	kIx(	(
<g/>
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavou	hlava	k1gFnSc7	hlava
je	být	k5eAaImIp3nS	být
lichtenštejnský	lichtenštejnský	k2eAgMnSc1d1	lichtenštejnský
kníže	kníže	k1gMnSc1	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jako	jako	k8xS	jako
jediné	jediný	k2eAgFnPc1d1	jediná
země	zem	k1gFnPc1	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
od	od	k7c2	od
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
písemně	písemně	k6eAd1	písemně
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
roku	rok	k1gInSc2	rok
1136	[number]	k4	1136
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
jej	on	k3xPp3gMnSc4	on
opustili	opustit	k5eAaPmAgMnP	opustit
kolem	kolem	k6eAd1	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnské	lichtenštejnský	k2eAgNnSc1d1	Lichtenštejnské
knížectví	knížectví	k1gNnSc1	knížectví
bývalo	bývat	k5eAaImAgNnS	bývat
součástí	součást	k1gFnSc7	součást
Chursko-rétského	Churskoétský	k2eAgNnSc2d1	Chursko-rétský
vévodství	vévodství	k1gNnSc2	vévodství
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
536	[number]	k4	536
od	od	k7c2	od
franckých	francký	k2eAgMnPc2d1	francký
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
<g/>
800	[number]	k4	800
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
768	[number]	k4	768
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
francký	francký	k2eAgInSc4d1	francký
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Rétské	Rétský	k2eAgNnSc1d1	Rétský
vévodství	vévodství	k1gNnSc1	vévodství
včleněno	včleněn	k2eAgNnSc1d1	včleněno
do	do	k7c2	do
"	"	kIx"	"
<g/>
Vévodství	vévodství	k1gNnSc2	vévodství
Švábsko	Švábsko	k1gNnSc4	Švábsko
a	a	k8xC	a
Raetie	Raetie	k1gFnPc4	Raetie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nástupci	nástupce	k1gMnPc1	nástupce
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgInSc2d1	veliký
silou	síla	k1gFnSc7	síla
ovládali	ovládat	k5eAaImAgMnP	ovládat
hrabství	hrabství	k1gNnSc4	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1180	[number]	k4	1180
připadlo	připadnout	k5eAaPmAgNnS	připadnout
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc1	dva
století	století	k1gNnPc1	století
(	(	kIx(	(
<g/>
1180	[number]	k4	1180
<g/>
-	-	kIx~	-
<g/>
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
Vaduzské	Vaduzský	k2eAgFnSc2d1	Vaduzský
a	a	k8xC	a
část	část	k1gFnSc4	část
"	"	kIx"	"
<g/>
Schellenberského	Schellenberský	k2eAgNnSc2d1	Schellenberský
hrabství	hrabství	k1gNnSc2	hrabství
<g/>
"	"	kIx"	"
rodu	rod	k1gInSc2	rod
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Monfortu	Monfort	k1gInSc2	Monfort
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
do	do	k7c2	do
1507	[number]	k4	1507
bylo	být	k5eAaImAgNnS	být
vévodství	vévodství	k1gNnSc1	vévodství
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
svobodných	svobodný	k2eAgMnPc2d1	svobodný
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Brandis	Brandis	k1gFnSc2	Brandis
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
panství	panství	k1gNnSc1	panství
Schellenbergu	Schellenberg	k1gInSc2	Schellenberg
(	(	kIx(	(
<g/>
Lichtenštejny	Lichtenštejna	k1gFnSc2	Lichtenštejna
koupeno	koupit	k5eAaPmNgNnS	koupit
1699	[number]	k4	1699
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
"	"	kIx"	"
<g/>
Dolní	dolní	k2eAgFnPc1d1	dolní
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Unterland	Unterland	k1gInSc1	Unterland
<g/>
)	)	kIx)	)
a	a	k8xC	a
hrabství	hrabství	k1gNnSc1	hrabství
Vaduzu	Vaduz	k1gInSc2	Vaduz
(	(	kIx(	(
<g/>
1712	[number]	k4	1712
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
"	"	kIx"	"
<g/>
Horní	horní	k2eAgFnSc1d1	horní
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Oberland	Oberland	k1gInSc1	Oberland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1719	[number]	k4	1719
bylo	být	k5eAaImAgNnS	být
římským	římský	k2eAgInSc7d1	římský
císařem	císař	k1gMnSc7	císař
uznáno	uznat	k5eAaPmNgNnS	uznat
jako	jako	k9	jako
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
středověké	středověký	k2eAgFnSc2d1	středověká
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgInSc2d1	německý
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
suverénní	suverénní	k2eAgInSc1d1	suverénní
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Rýnském	rýnské	k1gNnSc6	rýnské
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Německého	německý	k2eAgInSc2d1	německý
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
sjednocení	sjednocení	k1gNnSc2	sjednocení
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
nedotklo	dotknout	k5eNaPmAgNnS	dotknout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
územím	území	k1gNnSc7	území
nehraničí	hraničit	k5eNaImIp3nP	hraničit
<g/>
.	.	kIx.	.
</s>
<s>
Kulturně	kulturně	k6eAd1	kulturně
<g/>
,	,	kIx,	,
hospodářsky	hospodářsky	k6eAd1	hospodářsky
i	i	k8xC	i
politicky	politicky	k6eAd1	politicky
se	se	k3xPyFc4	se
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
na	na	k7c4	na
sousední	sousední	k2eAgNnSc4d1	sousední
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
zániku	zánik	k1gInSc2	zánik
koncem	koncem	k7c2	koncem
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
žili	žít	k5eAaImAgMnP	žít
především	především	k6eAd1	především
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
moravských	moravský	k2eAgNnPc6d1	Moravské
panstvích	panství	k1gNnPc6	panství
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odešli	odejít	k5eAaPmAgMnP	odejít
až	až	k9	až
před	před	k7c7	před
nacisty	nacista	k1gMnPc7	nacista
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
knížectví	knížectví	k1gNnSc1	knížectví
stalo	stát	k5eAaPmAgNnS	stát
jejich	jejich	k3xOp3gNnSc7	jejich
stálým	stálý	k2eAgNnSc7d1	stálé
sídlem	sídlo	k1gNnSc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc1d1	konstituční
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
je	být	k5eAaImIp3nS	být
knížectví	knížectví	k1gNnSc4	knížectví
–	–	k?	–
dědičná	dědičný	k2eAgFnSc1d1	dědičná
konstituční	konstituční	k2eAgFnSc1d1	konstituční
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavou	hlava	k1gFnSc7	hlava
je	být	k5eAaImIp3nS	být
úřadující	úřadující	k2eAgMnSc1d1	úřadující
kníže	kníže	k1gMnSc1	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
za	za	k7c4	za
většinu	většina	k1gFnSc4	většina
politických	politický	k2eAgFnPc2d1	politická
záležitostí	záležitost	k1gFnPc2	záležitost
zodpovídá	zodpovídat	k5eAaPmIp3nS	zodpovídat
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
má	mít	k5eAaImIp3nS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
vetovat	vetovat	k5eAaBmF	vetovat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
navrhovat	navrhovat	k5eAaImF	navrhovat
nové	nový	k2eAgInPc4d1	nový
právní	právní	k2eAgInPc4d1	právní
předpisy	předpis	k1gInPc4	předpis
a	a	k8xC	a
také	také	k9	také
rozpustit	rozpustit	k5eAaPmF	rozpustit
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc7d1	současná
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
Hans	Hans	k1gMnSc1	Hans
Adam	Adam	k1gMnSc1	Adam
II	II	kA	II
<g/>
.	.	kIx.	.
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
referendum	referendum	k1gNnSc4	referendum
spojené	spojený	k2eAgNnSc4d1	spojené
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
pravomocí	pravomoc	k1gFnPc2	pravomoc
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
současně	současně	k6eAd1	současně
práv	právo	k1gNnPc2	právo
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
přímo	přímo	k6eAd1	přímo
změnit	změnit	k5eAaPmF	změnit
státní	státní	k2eAgNnSc4d1	státní
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
silné	silný	k2eAgNnSc1d1	silné
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
demokratická	demokratický	k2eAgNnPc4d1	demokratické
práva	právo	k1gNnPc4	právo
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zániku	zánik	k1gInSc2	zánik
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
je	být	k5eAaImIp3nS	být
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
úzce	úzko	k6eAd1	úzko
spojeno	spojen	k2eAgNnSc1d1	spojeno
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ono	onen	k3xDgNnSc1	onen
udržuje	udržovat	k5eAaImIp3nS	udržovat
nízké	nízký	k2eAgFnPc4d1	nízká
daně	daň	k1gFnPc4	daň
<g/>
,	,	kIx,	,
neutralitu	neutralita	k1gFnSc4	neutralita
a	a	k8xC	a
bankovní	bankovní	k2eAgNnSc4d1	bankovní
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
také	také	k9	také
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
zájmy	zájem	k1gInPc4	zájem
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
Bern	Bern	k1gInSc4	Bern
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
Ženevu	Ženeva	k1gFnSc4	Ženeva
a	a	k8xC	a
OSN	OSN	kA	OSN
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
vlastní	vlastní	k2eAgNnSc4d1	vlastní
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
členem	člen	k1gMnSc7	člen
Evropského	evropský	k2eAgNnSc2d1	Evropské
sdružení	sdružení	k1gNnSc2	sdružení
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
ESVO	ESVO	kA	ESVO
-	-	kIx~	-
EFTA	EFTA	kA	EFTA
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tam	tam	k6eAd1	tam
jeho	jeho	k3xOp3gInPc4	jeho
zájmy	zájem	k1gInPc4	zájem
zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
součástí	součást	k1gFnPc2	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
;	;	kIx,	;
není	být	k5eNaImIp3nS	být
však	však	k9	však
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Konfiskace	konfiskace	k1gFnSc1	konfiskace
majetku	majetek	k1gInSc2	majetek
lichtenštejnských	lichtenštejnský	k2eAgMnPc2d1	lichtenštejnský
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgInPc1d1	specifický
jsou	být	k5eAaImIp3nP	být
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Československem	Československo	k1gNnSc7	Československo
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lichtenštejnskem	Lichtenštejnsko	k1gNnSc7	Lichtenštejnsko
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
negativně	negativně	k6eAd1	negativně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
konfiskací	konfiskace	k1gFnSc7	konfiskace
veškerého	veškerý	k3xTgInSc2	veškerý
majetku	majetek	k1gInSc2	majetek
rodu	rod	k1gInSc2	rod
Lichtenštejnů	Lichtenštejn	k1gMnPc2	Lichtenštejn
na	na	k7c6	na
území	území	k1gNnSc6	území
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
majetek	majetek	k1gInSc1	majetek
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
vyvlastnil	vyvlastnit	k5eAaPmAgInS	vyvlastnit
československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
byli	být	k5eAaImAgMnP	být
německé	německý	k2eAgFnPc4d1	německá
národnosti	národnost	k1gFnPc4	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
ohradili	ohradit	k5eAaPmAgMnP	ohradit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nebyli	být	k5eNaImAgMnP	být
německé	německý	k2eAgFnPc4d1	německá
národnosti	národnost	k1gFnPc4	národnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
měli	mít	k5eAaImAgMnP	mít
lichtenštejnské	lichtenštejnský	k2eAgNnSc4d1	Lichtenštejnské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
také	také	k6eAd1	také
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
kněžna	kněžna	k1gFnSc1	kněžna
Elsa	Elsa	k1gFnSc1	Elsa
byla	být	k5eAaImAgFnS	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Restituce	restituce	k1gFnPc1	restituce
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
majetek	majetek	k1gInSc4	majetek
Lichtenštejnů	Lichtenštejn	k1gInPc2	Lichtenštejn
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
nevztahovaly	vztahovat	k5eNaImAgFnP	vztahovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
zabaven	zabavit	k5eAaPmNgInS	zabavit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
neuznaným	uznaný	k2eNgInPc3d1	neuznaný
majetkovým	majetkový	k2eAgInPc3d1	majetkový
nárokům	nárok	k1gInPc3	nárok
Lichtenštejnů	Lichtenštejn	k1gInPc2	Lichtenštejn
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vzájemně	vzájemně	k6eAd1	vzájemně
neuznaly	uznat	k5eNaPmAgFnP	uznat
svoji	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
neměly	mít	k5eNaImAgInP	mít
spolu	spolu	k6eAd1	spolu
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
byly	být	k5eAaImAgFnP	být
přerušeny	přerušit	k5eAaPmNgFnP	přerušit
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
však	však	k9	však
premiér	premiér	k1gMnSc1	premiér
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Jan	Jan	k1gMnSc1	Jan
Kohout	Kohout	k1gMnSc1	Kohout
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
uznání	uznání	k1gNnSc3	uznání
a	a	k8xC	a
navázání	navázání	k1gNnSc3	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
v	v	k7c6	v
dohledné	dohledný	k2eAgFnSc6d1	dohledná
době	doba	k1gFnSc6	doba
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
stalo	stát	k5eAaPmAgNnS	stát
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
představitelé	představitel	k1gMnPc1	představitel
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
diplomatické	diplomatický	k2eAgFnPc4d1	diplomatická
nóty	nóta	k1gFnPc4	nóta
a	a	k8xC	a
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Horního	horní	k2eAgInSc2d1	horní
Rýna	Rýn	k1gInSc2	Rýn
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
západní	západní	k2eAgFnSc1d1	západní
hranice	hranice	k1gFnSc1	hranice
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
měří	měřit	k5eAaImIp3nS	měřit
tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
24	[number]	k4	24
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohu	rozloha	k1gFnSc4	rozloha
160	[number]	k4	160
km2	km2	k4	km2
tvoří	tvořit	k5eAaImIp3nP	tvořit
lesy	les	k1gInPc1	les
41	[number]	k4	41
%	%	kIx~	%
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
pole	pole	k1gNnPc1	pole
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
34	[number]	k4	34
%	%	kIx~	%
<g/>
,	,	kIx,	,
neproduktivní	produktivní	k2eNgFnPc1d1	neproduktivní
plochy	plocha	k1gFnPc1	plocha
15	[number]	k4	15
%	%	kIx~	%
<g/>
,	,	kIx,	,
zastavěné	zastavěný	k2eAgFnSc2d1	zastavěná
plochy	plocha	k1gFnSc2	plocha
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Grauspitz	Grauspitz	k1gInSc1	Grauspitz
<g/>
,	,	kIx,	,
2599	[number]	k4	2599
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgInSc4d3	nejnižší
bod	bod	k1gInSc4	bod
je	být	k5eAaImIp3nS	být
Ruggeler	Ruggeler	k1gMnSc1	Ruggeler
Ried	Ried	k1gMnSc1	Ried
<g/>
,	,	kIx,	,
430	[number]	k4	430
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
vnitrozemských	vnitrozemský	k2eAgInPc2d1	vnitrozemský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
zcela	zcela	k6eAd1	zcela
obklopený	obklopený	k2eAgInSc1d1	obklopený
jinými	jiný	k2eAgInPc7d1	jiný
vnitrozemskými	vnitrozemský	k2eAgInPc7d1	vnitrozemský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
rozloze	rozloha	k1gFnSc3	rozloha
je	být	k5eAaImIp3nS	být
šestým	šestý	k4xOgInSc7	šestý
nejmenším	malý	k2eAgInSc7d3	nejmenší
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnské	lichtenštejnský	k2eAgNnSc1d1	Lichtenštejnské
knížectví	knížectví	k1gNnSc1	knížectví
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
11	[number]	k4	11
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
tvořeny	tvořit	k5eAaImNgInP	tvořit
pouze	pouze	k6eAd1	pouze
samotným	samotný	k2eAgNnSc7d1	samotné
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vesnicí	vesnice	k1gFnSc7	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tzv.	tzv.	kA	tzv.
nižšího	nízký	k2eAgNnSc2d2	nižší
hrabství	hrabství	k1gNnSc2	hrabství
patří	patřit	k5eAaImIp3nS	patřit
Eschen	Eschen	k1gInSc1	Eschen
<g/>
,	,	kIx,	,
Gamprin	Gamprin	k1gInSc1	Gamprin
<g/>
,	,	kIx,	,
Mauren	Maurna	k1gFnPc2	Maurna
<g/>
,	,	kIx,	,
Ruggell	Ruggella	k1gFnPc2	Ruggella
a	a	k8xC	a
Schellenberg	Schellenberg	k1gInSc1	Schellenberg
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
hrabství	hrabství	k1gNnSc1	hrabství
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
Balzers	Balzers	k1gInSc1	Balzers
<g/>
,	,	kIx,	,
Planken	Planken	k1gInSc1	Planken
<g/>
,	,	kIx,	,
Schaan	Schaan	k1gInSc1	Schaan
<g/>
,	,	kIx,	,
Triesen	Triesen	k2eAgInSc1d1	Triesen
<g/>
,	,	kIx,	,
Triesenberg	Triesenberg	k1gInSc1	Triesenberg
a	a	k8xC	a
Vaduz	Vaduz	k1gInSc1	Vaduz
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
převažující	převažující	k2eAgInSc1d1	převažující
jižní	jižní	k2eAgInSc1d1	jižní
vítr	vítr	k1gInSc1	vítr
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
relativně	relativně	k6eAd1	relativně
mírné	mírný	k2eAgNnSc1d1	mírné
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jsou	být	k5eAaImIp3nP	být
horské	horský	k2eAgInPc1d1	horský
svahy	svah	k1gInPc1	svah
ideální	ideální	k2eAgInPc1d1	ideální
pro	pro	k7c4	pro
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
10,2	[number]	k4	10,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
nejteplejší	teplý	k2eAgInPc1d3	nejteplejší
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
červen	červen	k1gInSc4	červen
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
úhrn	úhrn	k1gInSc1	úhrn
ročních	roční	k2eAgFnPc2d1	roční
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
,	,	kIx,	,
San	San	k1gMnPc4	San
Marinu	Marina	k1gFnSc4	Marina
a	a	k8xC	a
Monackém	monacký	k2eAgNnSc6d1	Monacké
knížectví	knížectví	k1gNnSc6	knížectví
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
nejmenší	malý	k2eAgInSc1d3	nejmenší
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
hovoří	hovořit	k5eAaImIp3nS	hovořit
alamanským	alamanský	k2eAgInSc7d1	alamanský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
občané	občan	k1gMnPc1	občan
jiné	jiný	k2eAgFnSc2d1	jiná
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dvou	dva	k4xCgNnPc2	dva
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
905	[number]	k4	905
(	(	kIx(	(
<g/>
31.12	[number]	k4	31.12
<g/>
.2005	.2005	k4	.2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
Oberland	Oberland	k1gInSc1	Oberland
<g/>
/	/	kIx~	/
<g/>
Vaduz	Vaduz	k1gInSc1	Vaduz
22	[number]	k4	22
845	[number]	k4	845
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
Unterland	Unterland	k1gInSc1	Unterland
<g/>
/	/	kIx~	/
<g/>
Schellenberg	Schellenberg	k1gInSc1	Schellenberg
12	[number]	k4	12
060	[number]	k4	060
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
cizinců	cizinec	k1gMnPc2	cizinec
na	na	k7c6	na
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
%	%	kIx~	%
(	(	kIx(	(
<g/>
11	[number]	k4	11
917	[number]	k4	917
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
3	[number]	k4	3
617	[number]	k4	617
Švýcarů	Švýcar	k1gMnPc2	Švýcar
<g/>
,	,	kIx,	,
2	[number]	k4	2
045	[number]	k4	045
Rakušanů	Rakušan	k1gMnPc2	Rakušan
<g/>
,	,	kIx,	,
1	[number]	k4	1
208	[number]	k4	208
Italů	Ital	k1gMnPc2	Ital
a	a	k8xC	a
1	[number]	k4	1
178	[number]	k4	178
Němců	Němec	k1gMnPc2	Němec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
048	[number]	k4	048
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
76	[number]	k4	76
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
:	:	kIx,	:
27	[number]	k4	27
280	[number]	k4	280
z	z	k7c2	z
35	[number]	k4	35
890	[number]	k4	890
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
knížectví	knížectví	k1gNnSc4	knížectví
tvoří	tvořit	k5eAaImIp3nS	tvořit
arcidiecézi	arcidiecéze	k1gFnSc4	arcidiecéze
vaduzskou	vaduzský	k2eAgFnSc4d1	vaduzský
s	s	k7c7	s
katedrálou	katedrála	k1gFnSc7	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Florina	Florin	k1gMnSc2	Florin
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
8,5	[number]	k4	8,5
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
1,1	[number]	k4	1,1
<g/>
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
5,4	[number]	k4	5,4
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlásících	hlásící	k2eAgNnPc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
a	a	k8xC	a
r.	r.	kA	r.
2010	[number]	k4	2010
činil	činit	k5eAaImAgInS	činit
5,4	[number]	k4	5,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
Turci	Turek	k1gMnPc1	Turek
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
však	však	k9	však
mluví	mluvit	k5eAaImIp3nS	mluvit
alamanským	alamanský	k2eAgInSc7d1	alamanský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
standardní	standardní	k2eAgFnSc2d1	standardní
němčiny	němčina	k1gFnSc2	němčina
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
blízko	blízko	k6eAd1	blízko
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dialektům	dialekt	k1gInPc3	dialekt
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
národní	národní	k2eAgInSc1d1	národní
produkt	produkt	k1gInSc1	produkt
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
činí	činit	k5eAaImIp3nS	činit
4,1	[number]	k4	4,1
miliardy	miliarda	k4xCgFnSc2	miliarda
CHF	CHF	kA	CHF
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
158	[number]	k4	158
000	[number]	k4	000
CHF	CHF	kA	CHF
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zemi	zem	k1gFnSc4	zem
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
činí	činit	k5eAaImIp3nS	činit
2,4	[number]	k4	2,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
HNP	HNP	kA	HNP
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
zboží	zboží	k1gNnSc2	zboží
(	(	kIx(	(
<g/>
42	[number]	k4	42
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
služby	služba	k1gFnPc4	služba
(	(	kIx(	(
<g/>
27	[number]	k4	27
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
finančnictví	finančnictví	k1gNnSc1	finančnictví
(	(	kIx(	(
<g/>
24	[number]	k4	24
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
liechtenštejnské	liechtenštejnský	k2eAgNnSc4d1	liechtenštejnský
hospodářství	hospodářství	k1gNnSc4	hospodářství
30	[number]	k4	30
170	[number]	k4	170
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
20	[number]	k4	20
035	[number]	k4	035
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
do	do	k7c2	do
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
dojíždějí	dojíždět	k5eAaImIp3nP	dojíždět
především	především	k9	především
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
zboží	zboží	k1gNnSc2	zboží
44	[number]	k4	44
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
39	[number]	k4	39
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finančnictví	finančnictví	k1gNnSc6	finančnictví
1	[number]	k4	1
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
zaměstnaní	zaměstnaný	k1gMnPc1	zaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
,	,	kIx,	,
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
má	mít	k5eAaImIp3nS	mít
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
sektor	sektor	k1gInSc1	sektor
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podíl	podíl	k1gInSc1	podíl
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
i	i	k9	i
podíl	podíl	k1gInSc4	podíl
sektoru	sektor	k1gInSc2	sektor
na	na	k7c6	na
HNP	HNP	kA	HNP
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
oproti	oproti	k7c3	oproti
jeho	jeho	k3xOp3gMnPc3	jeho
sousedům	soused	k1gMnPc3	soused
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
je	být	k5eAaImIp3nS	být
činných	činný	k2eAgFnPc2d1	činná
597	[number]	k4	597
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
jsou	být	k5eAaImIp3nP	být
závody	závod	k1gInPc1	závod
s	s	k7c7	s
méně	málo	k6eAd2	málo
než	než	k8xS	než
50	[number]	k4	50
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
,	,	kIx,	,
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
diverzifikována	diverzifikován	k2eAgFnSc1d1	diverzifikován
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
strojírenská	strojírenský	k2eAgFnSc1d1	strojírenská
výroba	výroba	k1gFnSc1	výroba
–	–	k?	–
přesné	přesný	k2eAgInPc1d1	přesný
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
vybavení	vybavení	k1gNnSc1	vybavení
(	(	kIx(	(
<g/>
Hilti	Hilti	k1gNnSc1	Hilti
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilové	automobilový	k2eAgFnSc2d1	automobilová
komponenty	komponenta	k1gFnSc2	komponenta
(	(	kIx(	(
<g/>
ThyssenKrupp	ThyssenKrupp	k1gInSc1	ThyssenKrupp
Presta	presto	k1gNnSc2	presto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dentistické	dentistický	k2eAgNnSc4d1	dentistický
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
(	(	kIx(	(
<g/>
Ivoclar	Ivoclar	k1gMnSc1	Ivoclar
Vivadent	Vivadent	k1gMnSc1	Vivadent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
Hilcona	Hilcona	k1gFnSc1	Hilcona
Schaan	Schaan	k1gMnSc1	Schaan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
(	(	kIx(	(
<g/>
Unaxis	Unaxis	k1gFnSc1	Unaxis
Balzers	Balzersa	k1gFnPc2	Balzersa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnské	lichtenštejnský	k2eAgNnSc1d1	Lichtenštejnské
hospodářství	hospodářství	k1gNnSc1	hospodářství
je	být	k5eAaImIp3nS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
na	na	k7c4	na
export	export	k1gInSc4	export
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
EU	EU	kA	EU
(	(	kIx(	(
<g/>
44	[number]	k4	44
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
(	(	kIx(	(
<g/>
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
18	[number]	k4	18
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
(	(	kIx(	(
<g/>
24	[number]	k4	24
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
Rakouské	rakouský	k2eAgFnPc4d1	rakouská
spolkové	spolkový	k2eAgFnPc4d1	spolková
dráhy	dráha	k1gFnPc4	dráha
(	(	kIx(	(
<g/>
Österreichische	Österreichisch	k1gInPc4	Österreichisch
Bundesbahnen	Bundesbahnen	k1gInSc1	Bundesbahnen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Saldo	saldo	k1gNnSc1	saldo
bilance	bilance	k1gFnSc2	bilance
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
Liechtenštejnska	Liechtenštejnsko	k1gNnSc2	Liechtenštejnsko
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
investice	investice	k1gFnSc1	investice
do	do	k7c2	do
hospodářství	hospodářství	k1gNnSc2	hospodářství
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
CHF	CHF	kA	CHF
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zmiňované	zmiňovaný	k2eAgNnSc1d1	zmiňované
finančnictví	finančnictví	k1gNnSc1	finančnictví
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
HNP	HNP	kA	HNP
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
bankovními	bankovní	k2eAgInPc7d1	bankovní
domy	dům	k1gInPc7	dům
zaměstnávajícími	zaměstnávající	k2eAgInPc7d1	zaměstnávající
1700	[number]	k4	1700
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Banky	banka	k1gFnPc1	banka
spravují	spravovat	k5eAaImIp3nP	spravovat
majetek	majetek	k1gInSc4	majetek
klientů	klient	k1gMnPc2	klient
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
130	[number]	k4	130
miliard	miliarda	k4xCgFnPc2	miliarda
CHF	CHF	kA	CHF
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6	Lichtenštejnsko
pracuje	pracovat	k5eAaImIp3nS	pracovat
163	[number]	k4	163
investičních	investiční	k2eAgFnPc2d1	investiční
společností	společnost	k1gFnPc2	společnost
s	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
cca	cca	kA	cca
21	[number]	k4	21
miliard	miliarda	k4xCgFnPc2	miliarda
CHF	CHF	kA	CHF
a	a	k8xC	a
31	[number]	k4	31
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
spotřeba	spotřeba	k1gFnSc1	spotřeba
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
1360	[number]	k4	1360
GWh	GWh	k1gFnPc2	GWh
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
6	[number]	k4	6
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
94	[number]	k4	94
%	%	kIx~	%
energetických	energetický	k2eAgInPc2d1	energetický
zdrojů	zdroj	k1gInPc2	zdroj
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
dováží	dovázat	k5eAaPmIp3nP	dovázat
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
spotřeby	spotřeba	k1gFnSc2	spotřeba
energie	energie	k1gFnSc2	energie
dle	dle	k7c2	dle
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
:	:	kIx,	:
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
29	[number]	k4	29
%	%	kIx~	%
<g/>
,	,	kIx,	,
elektřina	elektřina	k1gFnSc1	elektřina
26	[number]	k4	26
%	%	kIx~	%
<g/>
,	,	kIx,	,
topné	topný	k2eAgInPc1d1	topný
oleje	olej	k1gInPc1	olej
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
benzín	benzín	k1gInSc1	benzín
16	[number]	k4	16
%	%	kIx~	%
<g/>
,	,	kIx,	,
nafta	nafta	k1gFnSc1	nafta
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6	Lichtenštejnsko
celkem	celkem	k6eAd1	celkem
31	[number]	k4	31
710	[number]	k4	710
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
osobních	osobní	k2eAgNnPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
24	[number]	k4	24
293	[number]	k4	293
tj.	tj.	kA	tj.
696	[number]	k4	696
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
rozpočet	rozpočet	k1gInSc1	rozpočet
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
s	s	k7c7	s
přebytkem	přebytek	k1gInSc7	přebytek
<g/>
.	.	kIx.	.
příjmy	příjem	k1gInPc1	příjem
<g/>
:	:	kIx,	:
daně	daň	k1gFnPc1	daň
a	a	k8xC	a
odvody	odvod	k1gInPc1	odvod
74	[number]	k4	74
%	%	kIx~	%
<g/>
,	,	kIx,	,
<g/>
výnos	výnos	k1gInSc1	výnos
z	z	k7c2	z
majetku	majetek	k1gInSc2	majetek
státu	stát	k1gInSc2	stát
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
poplatky	poplatek	k1gInPc1	poplatek
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
výdaje	výdaj	k1gInPc1	výdaj
<g/>
:	:	kIx,	:
správa	správa	k1gFnSc1	správa
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
daní	daň	k1gFnPc2	daň
35	[number]	k4	35
%	%	kIx~	%
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
25	[number]	k4	25
%	%	kIx~	%
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
18	[number]	k4	18
%	%	kIx~	%
<g/>
,	,	kIx,	,
veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
11	[number]	k4	11
%	%	kIx~	%
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
7	[number]	k4	7
%	%	kIx~	%
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgInPc1d2	nižší
správní	správní	k2eAgInPc1d1	správní
celky	celek	k1gInPc1	celek
(	(	kIx(	(
<g/>
obce	obec	k1gFnPc1	obec
<g/>
)	)	kIx)	)
hospodaří	hospodařit	k5eAaImIp3nP	hospodařit
s	s	k7c7	s
přebytkem	přebytek	k1gInSc7	přebytek
<g/>
.	.	kIx.	.
příjmy	příjem	k1gInPc1	příjem
<g/>
:	:	kIx,	:
daně	daň	k1gFnSc2	daň
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
fin.	fin.	k?	fin.
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
24	[number]	k4	24
%	%	kIx~	%
<g/>
,	,	kIx,	,
poplatky	poplatek	k1gInPc1	poplatek
7	[number]	k4	7
%	%	kIx~	%
<g/>
,	,	kIx,	,
výnos	výnos	k1gInSc1	výnos
z	z	k7c2	z
majetku	majetek	k1gInSc2	majetek
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
výdaje	výdaj	k1gInPc1	výdaj
<g/>
:	:	kIx,	:
personální	personální	k2eAgMnSc1d1	personální
34	[number]	k4	34
%	%	kIx~	%
<g/>
,	,	kIx,	,
věcné	věcný	k2eAgFnSc2d1	věcná
28	[number]	k4	28
%	%	kIx~	%
<g/>
,	,	kIx,	,
obecní	obecní	k2eAgFnSc1d1	obecní
28	[number]	k4	28
%	%	kIx~	%
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1	zajímavost
<g/>
:	:	kIx,	:
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
má	mít	k5eAaImIp3nS	mít
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
nulový	nulový	k2eAgInSc1d1	nulový
státní	státní	k2eAgInSc1d1	státní
dluh	dluh	k1gInSc1	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bohatství	bohatství	k1gNnSc2	bohatství
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
je	být	k5eAaImIp3nS	být
podporován	podporovat	k5eAaImNgInS	podporovat
pestrý	pestrý	k2eAgInSc1d1	pestrý
kulturní	kulturní	k2eAgInSc1d1	kulturní
a	a	k8xC	a
sportovní	sportovní	k2eAgInSc1d1	sportovní
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
umělcem	umělec	k1gMnSc7	umělec
narozeným	narozený	k2eAgMnSc7d1	narozený
v	v	k7c6	v
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6	Lichtenštejnsko
je	být	k5eAaImIp3nS	být
romantický	romantický	k2eAgMnSc1d1	romantický
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Josef	Josef	k1gMnSc1	Josef
Gabriel	Gabriel	k1gMnSc1	Gabriel
Rheinberger	Rheinberger	k1gMnSc1	Rheinberger
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
pocházejí	pocházet	k5eAaImIp3nP	pocházet
regionálně	regionálně	k6eAd1	regionálně
známé	známý	k2eAgFnPc1d1	známá
rockové	rockový	k2eAgFnPc1d1	rocková
a	a	k8xC	a
populární	populární	k2eAgFnPc1d1	populární
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
spolky	spolek	k1gInPc1	spolek
pěstující	pěstující	k2eAgFnSc4d1	pěstující
lokální	lokální	k2eAgFnSc4d1	lokální
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vaduzu	Vaduz	k1gInSc6	Vaduz
je	být	k5eAaImIp3nS	být
Zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Landesmuseum	Landesmuseum	k1gNnSc1	Landesmuseum
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
Kunstmuseum	Kunstmuseum	k1gNnSc1	Kunstmuseum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
lyžařství	lyžařství	k1gNnSc1	lyžařství
(	(	kIx(	(
<g/>
Skimuseum	Skimuseum	k1gNnSc1	Skimuseum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poštovní	poštovní	k2eAgNnSc1d1	poštovní
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Postmuseum	Postmuseum	k1gInSc1	Postmuseum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mnohá	mnohý	k2eAgNnPc1d1	mnohé
moderní	moderní	k2eAgNnPc1d1	moderní
místní	místní	k2eAgNnPc1d1	místní
muzea	muzeum	k1gNnPc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Schaanu	Schaan	k1gInSc6	Schaan
Divadlo	divadlo	k1gNnSc4	divadlo
na	na	k7c6	na
Kostelním	kostelní	k2eAgNnSc6d1	kostelní
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
Theater	Theater	k1gMnSc1	Theater
am	am	k?	am
Kirchplatz	Kirchplatz	k1gMnSc1	Kirchplatz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vaduzu	Vaduz	k1gInSc6	Vaduz
nové	nový	k2eAgNnSc4d1	nové
divadlo	divadlo	k1gNnSc4	divadlo
Zámecký	zámecký	k2eAgInSc1d1	zámecký
sklípek	sklípek	k1gInSc1	sklípek
(	(	kIx(	(
<g/>
Schlösslekeller	Schlösslekeller	k1gInSc1	Schlösslekeller
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Grafika	grafika	k1gFnSc1	grafika
<g/>
,	,	kIx,	,
užité	užitý	k2eAgNnSc1d1	užité
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
design	design	k1gInSc1	design
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
a	a	k8xC	a
národní	národní	k2eAgNnSc1d1	národní
mužstvo	mužstvo	k1gNnSc1	mužstvo
jsou	být	k5eAaImIp3nP	být
částí	část	k1gFnSc7	část
Švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
svazu	svaz	k1gInSc2	svaz
SFV	SFV	kA	SFV
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
každoroční	každoroční	k2eAgFnSc4d1	každoroční
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
UEFA	UEFA	kA	UEFA
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
kvalifikuje	kvalifikovat	k5eAaBmIp3nS	kvalifikovat
FC	FC	kA	FC
Vaduz	Vaduz	k1gInSc1	Vaduz
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
mužstvo	mužstvo	k1gNnSc1	mužstvo
se	se	k3xPyFc4	se
pilně	pilně	k6eAd1	pilně
účastní	účastnit	k5eAaImIp3nS	účastnit
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
světového	světový	k2eAgNnSc2d1	světové
a	a	k8xC	a
evropského	evropský	k2eAgNnSc2d1	Evropské
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
velmocí	velmoc	k1gFnPc2	velmoc
ve	v	k7c6	v
sjezdovém	sjezdový	k2eAgNnSc6d1	sjezdové
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
slavné	slavný	k2eAgFnPc4d1	slavná
sjezdařské	sjezdařský	k2eAgFnPc4d1	sjezdařská
osobnosti	osobnost	k1gFnPc4	osobnost
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
patří	patřit	k5eAaImIp3nP	patřit
sourozenci	sourozenec	k1gMnPc1	sourozenec
Andreas	Andreas	k1gMnSc1	Andreas
Wenzel	Wenzel	k1gMnSc1	Wenzel
a	a	k8xC	a
Hanni	Haneň	k1gFnSc6	Haneň
Wenzelová	Wenzelová	k1gFnSc1	Wenzelová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
ve	v	k7c6	v
sjezdovém	sjezdový	k2eAgNnSc6d1	sjezdové
lyžování	lyžování	k1gNnSc6	lyžování
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnSc1	medaile
z	z	k7c2	z
OH	OH	kA	OH
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
sjezdaři	sjezdař	k1gMnPc1	sjezdař
Willi	Wille	k1gFnSc4	Wille
Frommelt	Frommelt	k1gMnSc1	Frommelt
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Frommelt	Frommelt	k1gMnSc1	Frommelt
a	a	k8xC	a
Ursula	Ursula	k1gFnSc1	Ursula
Konzett	Konzetta	k1gFnPc2	Konzetta
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
získalo	získat	k5eAaPmAgNnS	získat
9	[number]	k4	9
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc1	všechen
ve	v	k7c6	v
sjezdovém	sjezdový	k2eAgNnSc6d1	sjezdové
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
závodů	závod	k1gInPc2	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nahlédl	nahlédnout	k5eAaPmAgInS	nahlédnout
Rikky	Rikka	k1gFnSc2	Rikka
von	von	k1gInSc1	von
Opel	opel	k1gInSc1	opel
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
se	se	k3xPyFc4	se
též	též	k9	též
pravidelně	pravidelně	k6eAd1	pravidelně
účastní	účastnit	k5eAaImIp3nS	účastnit
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1980	[number]	k4	1980
však	však	k9	však
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
bojkotovala	bojkotovat	k5eAaImAgFnS	bojkotovat
LOH	LOH	kA	LOH
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
sovětské	sovětský	k2eAgFnSc3d1	sovětská
invazi	invaze	k1gFnSc3	invaze
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1979	[number]	k4	1979
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
LOH	LOH	kA	LOH
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
byla	být	k5eAaImAgFnS	být
vlajkonoškou	vlajkonoška	k1gFnSc7	vlajkonoška
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
Stephanie	Stephanie	k1gFnSc1	Stephanie
Vogtová	Vogtová	k1gFnSc1	Vogtová
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
ve	v	k7c6	v
Vaduzu	Vaduz	k1gInSc6	Vaduz
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Balzersu	Balzers	k1gInSc6	Balzers
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
