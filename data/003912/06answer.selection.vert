<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
společná	společný	k2eAgFnSc1d1	společná
hranice	hranice	k1gFnSc1	hranice
41,2	[number]	k4	41,2
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
hospodářsky	hospodářsky	k6eAd1	hospodářsky
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
spojeno	spojit	k5eAaPmNgNnS	spojit
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
měnovou	měnový	k2eAgFnSc4d1	měnová
a	a	k8xC	a
celní	celní	k2eAgFnSc2d1	celní
unií	unie	k1gFnSc7	unie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
(	(	kIx(	(
<g/>
společná	společný	k2eAgFnSc1d1	společná
hranice	hranice	k1gFnSc1	hranice
36,7	[number]	k4	36,7
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vklíněno	vklíněn	k2eAgNnSc1d1	vklíněno
mezi	mezi	k7c4	mezi
území	území	k1gNnSc4	území
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spolu	spolu	k6eAd1	spolu
na	na	k7c4	na
sever	sever	k1gInSc4	sever
i	i	k9	i
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
hraničí	hraničit	k5eAaImIp3nS	hraničit
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
