<p>
<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
infekce	infekce	k1gFnSc1	infekce
či	či	k8xC	či
bakteriofisóza	bakteriofisóza	k1gFnSc1	bakteriofisóza
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
infekční	infekční	k2eAgNnSc4d1	infekční
onemocnění	onemocnění	k1gNnSc4	onemocnění
způsobené	způsobený	k2eAgInPc1d1	způsobený
patogenními	patogenní	k2eAgFnPc7d1	patogenní
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
napadají	napadat	k5eAaBmIp3nP	napadat
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc4	houba
nebo	nebo	k8xC	nebo
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
patogenního	patogenní	k2eAgInSc2d1	patogenní
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
domény	doména	k1gFnSc2	doména
spíše	spíše	k9	spíše
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
neškodná	škodný	k2eNgFnSc1d1	neškodná
(	(	kIx(	(
<g/>
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
názvosloví	názvosloví	k1gNnSc6	názvosloví
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
infekcí	infekce	k1gFnPc2	infekce
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
používají	používat	k5eAaImIp3nP	používat
tradiční	tradiční	k2eAgInPc1d1	tradiční
názvy	název	k1gInPc1	název
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
cholera	cholera	k1gFnSc1	cholera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
názvy	název	k1gInPc7	název
tvoří	tvořit	k5eAaImIp3nS	tvořit
přidáním	přidání	k1gNnSc7	přidání
řecké	řecký	k2eAgFnSc2d1	řecká
přípony	přípona	k1gFnSc2	přípona
-osis	sis	k1gFnSc2	-osis
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
-óza	-óza	k6eAd1	-óza
<g/>
/	/	kIx~	/
<g/>
-osa	sa	k6eAd1	-osa
<g/>
)	)	kIx)	)
k	k	k7c3	k
rodovému	rodový	k2eAgNnSc3d1	rodové
jménu	jméno	k1gNnSc3	jméno
bakterie	bakterie	k1gFnSc2	bakterie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
listerióza	listerióza	k1gFnSc1	listerióza
od	od	k7c2	od
Listeria	Listerium	k1gNnSc2	Listerium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
striktní	striktní	k2eAgInPc1d1	striktní
(	(	kIx(	(
<g/>
obligátní	obligátní	k2eAgInPc1d1	obligátní
<g/>
)	)	kIx)	)
patogeny	patogen	k1gInPc1	patogen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
působí	působit	k5eAaImIp3nP	působit
nemoc	nemoc	k1gFnSc4	nemoc
zpravidla	zpravidla	k6eAd1	zpravidla
vždy	vždy	k6eAd1	vždy
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
méně	málo	k6eAd2	málo
hojné	hojný	k2eAgNnSc1d1	hojné
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
<g/>
,	,	kIx,	,
Neisseria	Neisserium	k1gNnSc2	Neisserium
gonorrhoeae	gonorrhoea	k1gFnSc2	gonorrhoea
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
oportunistické	oportunistický	k2eAgFnPc1d1	oportunistická
patogenní	patogenní	k2eAgFnPc1d1	patogenní
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
přítomné	přítomný	k1gMnPc4	přítomný
jako	jako	k8xS	jako
součásti	součást	k1gFnPc4	součást
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
případě	případ	k1gInSc6	případ
začnou	začít	k5eAaPmIp3nP	začít
škodit	škodit	k5eAaImF	škodit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oportunní	oportunní	k2eAgInPc4d1	oportunní
patogeny	patogen	k1gInPc4	patogen
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	col	k1gFnSc2	col
nebo	nebo	k8xC	nebo
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
farmakoterapii	farmakoterapie	k1gFnSc3	farmakoterapie
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
infekcí	infekce	k1gFnPc2	infekce
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Kochovy	Kochův	k2eAgInPc1d1	Kochův
postuláty	postulát	k1gInPc1	postulát
<g/>
,	,	kIx,	,
navržené	navržený	k2eAgNnSc1d1	navržené
Robertem	Robert	k1gMnSc7	Robert
Kochem	Koch	k1gMnSc7	Koch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kritéria	kritérion	k1gNnPc1	kritérion
navržená	navržený	k2eAgNnPc1d1	navržené
k	k	k7c3	k
prokazování	prokazování	k1gNnSc3	prokazování
souvislosti	souvislost	k1gFnSc2	souvislost
mezi	mezi	k7c7	mezi
patogenní	patogenní	k2eAgFnSc7d1	patogenní
bakterií	bakterie	k1gFnSc7	bakterie
a	a	k8xC	a
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
trvá	trvat	k5eAaImIp3nS	trvat
objevení	objevení	k1gNnSc1	objevení
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
danou	daný	k2eAgFnSc4d1	daná
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bakterie	bakterie	k1gFnSc2	bakterie
Helicobacter	Helicobactra	k1gFnPc2	Helicobactra
pylori	pylor	k1gFnSc2	pylor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bakteriózy	bakterióza	k1gFnPc1	bakterióza
rostlin	rostlina	k1gFnPc2	rostlina
==	==	k?	==
</s>
</p>
<p>
<s>
Známe	znát	k5eAaImIp1nP	znát
mnoho	mnoho	k6eAd1	mnoho
tzv.	tzv.	kA	tzv.
fytopatogenních	fytopatogenní	k2eAgFnPc2d1	fytopatogenní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
infekční	infekční	k2eAgFnPc1d1	infekční
choroby	choroba	k1gFnPc1	choroba
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
údajů	údaj	k1gInPc2	údaj
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
bakterie	bakterie	k1gFnPc4	bakterie
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
hospodářsky	hospodářsky	k6eAd1	hospodářsky
důležitých	důležitý	k2eAgFnPc2d1	důležitá
infekčních	infekční	k2eAgFnPc2d1	infekční
chorob	choroba	k1gFnPc2	choroba
8	[number]	k4	8
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
jen	jen	k6eAd1	jen
viróz	viróza	k1gFnPc2	viróza
a	a	k8xC	a
mykóz	mykóza	k1gFnPc2	mykóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
bakteriálním	bakteriální	k2eAgFnPc3d1	bakteriální
infekcím	infekce	k1gFnPc3	infekce
plodin	plodina	k1gFnPc2	plodina
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
měkká	měkký	k2eAgFnSc1d1	měkká
hniloba	hniloba	k1gFnSc1	hniloba
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
skvrnitost	skvrnitost	k1gFnSc1	skvrnitost
a	a	k8xC	a
spála	spála	k1gFnSc1	spála
u	u	k7c2	u
bobovitých	bobovitý	k2eAgInPc2d1	bobovitý
a	a	k8xC	a
skvrnitost	skvrnitost	k1gFnSc4	skvrnitost
a	a	k8xC	a
vadnutí	vadnutí	k1gNnSc4	vadnutí
lilkovitých	lilkovitý	k2eAgFnPc2d1	lilkovitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
fytoplazmózy	fytoplazmóza	k1gFnPc4	fytoplazmóza
(	(	kIx(	(
<g/>
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
Fytoplasma	Fytoplasma	k1gFnSc1	Fytoplasma
<g/>
)	)	kIx)	)
a	a	k8xC	a
spiroplazmózy	spiroplazmóza	k1gFnSc2	spiroplazmóza
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
je	být	k5eAaImIp3nS	být
Spiroplasma	Spiroplasma	k1gFnSc1	Spiroplasma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
bakterióz	bakterióza	k1gFnPc2	bakterióza
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Primárním	primární	k2eAgInSc7d1	primární
cílem	cíl	k1gInSc7	cíl
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
vhodné	vhodný	k2eAgNnSc4d1	vhodné
prostředí	prostředí	k1gNnSc4	prostředí
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozmnoží	rozmnožit	k5eAaPmIp3nS	rozmnožit
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
nikou	nika	k1gFnSc7	nika
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabízí	nabízet	k5eAaImIp3nS	nabízet
stabilní	stabilní	k2eAgFnPc4d1	stabilní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
vlhkost	vlhkost	k1gFnSc4	vlhkost
a	a	k8xC	a
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
bakterióz	bakterióza	k1gFnPc2	bakterióza
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
===	===	k?	===
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
,	,	kIx,	,
jak	jak	k8xC	jak
proniknout	proniknout	k5eAaPmF	proniknout
z	z	k7c2	z
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
bariéry	bariéra	k1gFnPc4	bariéra
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
hlen	hlen	k1gInSc1	hlen
<g/>
,	,	kIx,	,
řasinkový	řasinkový	k2eAgInSc1d1	řasinkový
epitel	epitel	k1gInSc1	epitel
či	či	k8xC	či
různé	různý	k2eAgFnPc1d1	různá
sekrety	sekreta	k1gFnPc1	sekreta
obsahující	obsahující	k2eAgFnPc1d1	obsahující
enzymy	enzym	k1gInPc4	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
mohou	moct	k5eAaImIp3nP	moct
buď	buď	k8xC	buď
prolomit	prolomit	k5eAaPmF	prolomit
tyto	tento	k3xDgFnPc4	tento
bariéry	bariéra	k1gFnPc4	bariéra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
najít	najít	k5eAaPmF	najít
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
však	však	k9	však
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
na	na	k7c6	na
pokožce	pokožka	k1gFnSc6	pokožka
(	(	kIx(	(
<g/>
i	i	k9	i
tam	tam	k6eAd1	tam
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
patogenní	patogenní	k2eAgMnPc4d1	patogenní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnPc1	bakterie
chtějí	chtít	k5eAaImIp3nP	chtít
kožní	kožní	k2eAgFnSc1d1	kožní
bariéře	bariéra	k1gFnSc3	bariéra
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
bývají	bývat	k5eAaImIp3nP	bývat
místem	místo	k1gNnSc7	místo
průniku	průnik	k1gInSc2	průnik
následující	následující	k2eAgFnSc2d1	následující
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
nos	nos	k1gInSc1	nos
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgFnSc1d1	dýchací
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc1	ucho
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
<g/>
,	,	kIx,	,
urogenitální	urogenitální	k2eAgInSc1d1	urogenitální
trakt	trakt	k1gInSc1	trakt
a	a	k8xC	a
řitní	řitní	k2eAgInSc1d1	řitní
otvor	otvor	k1gInSc1	otvor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rány	rána	k1gFnPc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
(	(	kIx(	(
<g/>
alimentárně	alimentárně	k6eAd1	alimentárně
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dostává	dostávat	k5eAaImIp3nS	dostávat
například	například	k6eAd1	například
Salmonella	Salmonello	k1gNnSc2	Salmonello
<g/>
,	,	kIx,	,
Shigella	Shigello	k1gNnSc2	Shigello
<g/>
,	,	kIx,	,
Brucella	Brucello	k1gNnSc2	Brucello
nebo	nebo	k8xC	nebo
Listeria	Listerium	k1gNnSc2	Listerium
<g/>
,	,	kIx,	,
vdechem	vdech	k1gInSc7	vdech
zase	zase	k9	zase
například	například	k6eAd1	například
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
nebo	nebo	k8xC	nebo
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
psittaci	psittace	k1gFnSc4	psittace
<g/>
.	.	kIx.	.
</s>
<s>
Ranami	Rana	k1gFnPc7	Rana
se	se	k3xPyFc4	se
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dostane	dostat	k5eAaPmIp3nS	dostat
například	například	k6eAd1	například
původce	původce	k1gMnSc1	původce
tetanu	tetan	k1gInSc2	tetan
<g/>
,	,	kIx,	,
Clostridium	Clostridium	k1gNnSc4	Clostridium
tetani	tetaň	k1gFnSc3	tetaň
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
jsou	být	k5eAaImIp3nP	být
přenosy	přenos	k1gInPc1	přenos
přes	přes	k7c4	přes
členovce	členovec	k1gMnPc4	členovec
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
například	například	k6eAd1	například
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
Borrelia	Borrelium	k1gNnSc2	Borrelium
<g/>
,	,	kIx,	,
Rickettsia	Rickettsium	k1gNnSc2	Rickettsium
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
urogenitální	urogenitální	k2eAgInSc4d1	urogenitální
trakt	trakt	k1gInSc4	trakt
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnPc1	bakterie
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Neisseria	Neisserium	k1gNnSc2	Neisserium
gonorrhoae	gonorrhoae	k1gNnSc2	gonorrhoae
či	či	k8xC	či
Treponema	Treponem	k1gMnSc4	Treponem
pallidum	pallidum	k1gInSc4	pallidum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kolonizace	kolonizace	k1gFnSc2	kolonizace
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
bakterie	bakterie	k1gFnSc2	bakterie
musí	muset	k5eAaImIp3nS	muset
najít	najít	k5eAaPmF	najít
vhodné	vhodný	k2eAgNnSc4d1	vhodné
útočiště	útočiště	k1gNnSc4	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
uchytí	uchytit	k5eAaPmIp3nS	uchytit
(	(	kIx(	(
<g/>
adheze	adheze	k1gFnSc1	adheze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
díky	díky	k7c3	díky
speciálním	speciální	k2eAgFnPc3d1	speciální
látkám	látka	k1gFnPc3	látka
<g/>
,	,	kIx,	,
adhezinům	adhezin	k1gInPc3	adhezin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomné	přítomný	k1gMnPc4	přítomný
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
fimbrií	fimbrie	k1gFnPc2	fimbrie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
na	na	k7c6	na
určitém	určitý	k2eAgInSc6d1	určitý
povrchu	povrch	k1gInSc6	povrch
biofilm	biofilm	k1gInSc1	biofilm
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
převážně	převážně	k6eAd1	převážně
polysacharidy	polysacharid	k1gInPc1	polysacharid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Onemocnění	onemocnění	k1gNnSc1	onemocnění
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
bakterií	bakterie	k1gFnPc2	bakterie
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
přijímat	přijímat	k5eAaImF	přijímat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
patogenních	patogenní	k2eAgFnPc2d1	patogenní
bakterií	bakterie	k1gFnPc2	bakterie
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
hostitelského	hostitelský	k2eAgInSc2d1	hostitelský
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
určitou	určitý	k2eAgFnSc7d1	určitá
činností	činnost	k1gFnSc7	činnost
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
narušováním	narušování	k1gNnSc7	narušování
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
či	či	k8xC	či
zplodinami	zplodina	k1gFnPc7	zplodina
jejich	jejich	k3xOp3gInSc2	jejich
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
však	však	k9	však
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
i	i	k9	i
imunitní	imunitní	k2eAgFnSc1d1	imunitní
reakce	reakce	k1gFnSc1	reakce
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
sepse	sepse	k1gFnSc1	sepse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
===	===	k?	===
</s>
</p>
<p>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
bakteriemi	bakterie	k1gFnPc7	bakterie
pomocí	pomocí	k7c2	pomocí
některých	některý	k3yIgFnPc2	některý
složek	složka	k1gFnPc2	složka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bakterie	bakterie	k1gFnPc4	bakterie
žijící	žijící	k2eAgFnPc4d1	žijící
uvnitř	uvnitř	k6eAd1	uvnitř
nebo	nebo	k8xC	nebo
vně	vně	k6eAd1	vně
lidských	lidský	k2eAgFnPc2d1	lidská
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
extracelulárním	extracelulární	k2eAgFnPc3d1	extracelulární
bakteriím	bakterie	k1gFnPc3	bakterie
(	(	kIx(	(
<g/>
žijícím	žijící	k2eAgInSc7d1	žijící
mimo	mimo	k7c4	mimo
lidské	lidský	k2eAgFnPc4d1	lidská
buňky	buňka	k1gFnPc4	buňka
<g/>
;	;	kIx,	;
namátkou	namátkou	k6eAd1	namátkou
streptokoky	streptokok	k1gInPc4	streptokok
či	či	k8xC	či
neisserie	neisserie	k1gFnPc4	neisserie
<g/>
)	)	kIx)	)
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
bojují	bojovat	k5eAaImIp3nP	bojovat
hlavně	hlavně	k9	hlavně
buňky	buňka	k1gFnPc1	buňka
neutrofily	neutrofil	k1gMnPc7	neutrofil
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jim	on	k3xPp3gInPc3	on
však	však	k9	však
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
i	i	k9	i
tzv.	tzv.	kA	tzv.
komplement	komplement	k1gInSc1	komplement
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
protilátky	protilátka	k1gFnPc1	protilátka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
opsonizují	opsonizovat	k5eAaPmIp3nP	opsonizovat
povrch	povrch	k1gInSc4	povrch
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
usnadní	usnadnit	k5eAaPmIp3nS	usnadnit
její	její	k3xOp3gFnSc4	její
fagocytózu	fagocytóza	k1gFnSc4	fagocytóza
<g/>
.	.	kIx.	.
<g/>
Vnitrobuněčné	vnitrobuněčný	k2eAgFnPc1d1	vnitrobuněčná
bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
cílem	cíl	k1gInSc7	cíl
aktivovaných	aktivovaný	k2eAgInPc2d1	aktivovaný
makrofágů	makrofág	k1gInPc2	makrofág
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgInSc4d2	vyšší
stupeň	stupeň	k1gInSc4	stupeň
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
představují	představovat	k5eAaImIp3nP	představovat
TC-lymfocyty	TCymfocyt	k1gInPc1	TC-lymfocyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
bakteriálních	bakteriální	k2eAgNnPc2d1	bakteriální
onemocnění	onemocnění	k1gNnPc2	onemocnění
člověka	člověk	k1gMnSc2	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
chorobou	choroba	k1gFnSc7	choroba
vyvolanou	vyvolaný	k2eAgFnSc7d1	vyvolaná
bakteriemi	bakterie	k1gFnPc7	bakterie
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
WHO	WHO	kA	WHO
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
smrt	smrt	k1gFnSc4	smrt
asi	asi	k9	asi
1,4	[number]	k4	1,4
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
však	však	k9	však
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
závažné	závažný	k2eAgFnPc4d1	závažná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
bakteriální	bakteriální	k2eAgFnPc4d1	bakteriální
pneumonie	pneumonie	k1gFnPc4	pneumonie
(	(	kIx(	(
<g/>
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
<g/>
,	,	kIx,	,
Pseudomonas	Pseudomonas	k1gMnSc1	Pseudomonas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jídlem	jídlo	k1gNnSc7	jídlo
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
bakterie	bakterie	k1gFnSc1	bakterie
Shigella	Shigella	k1gFnSc1	Shigella
<g/>
,	,	kIx,	,
Campylobacter	Campylobacter	k1gMnSc1	Campylobacter
<g/>
,	,	kIx,	,
Salmonella	Salmonella	k1gMnSc1	Salmonella
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
však	však	k9	však
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
i	i	k9	i
infekce	infekce	k1gFnPc1	infekce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tetanus	tetanus	k1gInSc1	tetanus
<g/>
,	,	kIx,	,
tyfus	tyfus	k1gInSc1	tyfus
<g/>
,	,	kIx,	,
syfilis	syfilis	k1gFnSc1	syfilis
a	a	k8xC	a
lepra	lepra	k1gFnSc1	lepra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
</s>
</p>
<p>
<s>
Antibiotikum	antibiotikum	k1gNnSc1	antibiotikum
</s>
</p>
<p>
<s>
Virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
</s>
</p>
