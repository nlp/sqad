<s>
Athéna	Athéna	k1gFnSc1	Athéna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
dcera	dcera	k1gFnSc1	dcera
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
bohyni	bohyně	k1gFnSc4	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
a	a	k8xC	a
vítězné	vítězný	k2eAgFnSc2d1	vítězná
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ochránkyni	ochránkyně	k1gFnSc4	ochránkyně
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
,	,	kIx,	,
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
byla	být	k5eAaImAgFnS	být
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
římským	římský	k2eAgInSc7d1	římský
protějškem	protějšek	k1gInSc7	protějšek
Minervou	Minerva	k1gFnSc7	Minerva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
původu	původ	k1gInSc6	původ
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
mnoho	mnoho	k4c1	mnoho
mýtů	mýtus	k1gInPc2	mýtus
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
Pelasgů	Pelasg	k1gMnPc2	Pelasg
se	se	k3xPyFc4	se
bohyně	bohyně	k1gFnSc1	bohyně
Athéna	Athéna	k1gFnSc1	Athéna
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
jezera	jezero	k1gNnSc2	jezero
Tritónis	Tritónis	k1gFnSc2	Tritónis
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ji	on	k3xPp3gFnSc4	on
našly	najít	k5eAaPmAgFnP	najít
tři	tři	k4xCgFnPc4	tři
libyjské	libyjský	k2eAgFnPc4d1	Libyjská
nymfy	nymfa	k1gFnPc4	nymfa
a	a	k8xC	a
ujaly	ujmout	k5eAaPmAgFnP	ujmout
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
malé	malý	k2eAgNnSc1d1	malé
děvčátko	děvčátko	k1gNnSc1	děvčátko
prý	prý	k9	prý
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
při	při	k7c6	při
hravém	hravý	k2eAgInSc6d1	hravý
zápasu	zápas	k1gInSc6	zápas
s	s	k7c7	s
oštěpem	oštěp	k1gInSc7	oštěp
a	a	k8xC	a
štítem	štít	k1gInSc7	štít
zabila	zabít	k5eAaPmAgFnS	zabít
svou	svůj	k3xOyFgFnSc4	svůj
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Palladu	Pallad	k1gInSc2	Pallad
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
památku	památka	k1gFnSc4	památka
přiřadila	přiřadit	k5eAaPmAgFnS	přiřadit
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
před	před	k7c4	před
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
podle	podle	k7c2	podle
Hellénů	Hellén	k1gInPc2	Hellén
byl	být	k5eAaImAgInS	být
Athéniným	Athénin	k2eAgMnSc7d1	Athénin
otcem	otec	k1gMnSc7	otec
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
okřídlený	okřídlený	k2eAgMnSc1d1	okřídlený
kozlí	kozlí	k2eAgMnSc1d1	kozlí
obr	obr	k1gMnSc1	obr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
později	pozdě	k6eAd2	pozdě
pokusil	pokusit	k5eAaPmAgMnS	pokusit
znásilnit	znásilnit	k5eAaPmF	znásilnit
<g/>
.	.	kIx.	.
</s>
<s>
Stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
ho	on	k3xPp3gNnSc4	on
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
udělala	udělat	k5eAaPmAgFnS	udělat
si	se	k3xPyFc3	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
aigidu	aigida	k1gFnSc4	aigida
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc2	jeho
křídla	křídlo	k1gNnSc2	křídlo
si	se	k3xPyFc3	se
připjala	připnout	k5eAaPmAgFnS	připnout
k	k	k7c3	k
ramenům	rameno	k1gNnPc3	rameno
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
připojila	připojit	k5eAaPmAgFnS	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
verze	verze	k1gFnSc2	verze
však	však	k9	však
je	být	k5eAaImIp3nS	být
aigida	aigida	k1gFnSc1	aigida
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
Gorgony	Gorgo	k1gFnSc2	Gorgo
Medúsy	Medúsa	k1gFnSc2	Medúsa
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
kůži	kůže	k1gFnSc4	kůže
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Perseus	Perseus	k1gMnSc1	Perseus
uťal	utít	k5eAaPmAgMnS	utít
Medúsinu	Medúsin	k2eAgFnSc4d1	Medúsin
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
podle	podle	k7c2	podle
další	další	k2eAgFnSc2d1	další
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
Itónos	Itónosa	k1gFnPc2	Itónosa
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
města	město	k1gNnSc2	město
jiný	jiný	k2eAgInSc4d1	jiný
zdroj	zdroj	k1gInSc4	zdroj
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Athéna	Athéna	k1gFnSc1	Athéna
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Poseidóna	Poseidón	k1gMnSc2	Poseidón
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
adoptována	adoptovat	k5eAaPmNgFnS	adoptovat
Diem	Die	k1gNnSc7	Die
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
požádala	požádat	k5eAaPmAgFnS	požádat
asi	asi	k9	asi
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
když	když	k9	když
Zeus	Zeus	k1gInSc1	Zeus
zatoužil	zatoužit	k5eAaPmAgInS	zatoužit
po	po	k7c6	po
Titánce	titánka	k1gFnSc6	titánka
Métidě	Métida	k1gFnSc6	Métida
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
proměnách	proměna	k1gFnPc6	proměna
unikala	unikat	k5eAaImAgFnS	unikat
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
byla	být	k5eAaImAgFnS	být
polapena	polapen	k2eAgFnSc1d1	polapena
a	a	k8xC	a
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
však	však	k9	však
věštba	věštba	k1gFnSc1	věštba
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
moudrá	moudrý	k2eAgFnSc1d1	moudrá
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
mít	mít	k5eAaImF	mít
Métis	Métis	k1gFnSc7	Métis
další	další	k2eAgNnSc1d1	další
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
statečný	statečný	k2eAgMnSc1d1	statečný
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
svrhne	svrhnout	k5eAaPmIp3nS	svrhnout
Dia	Dia	k1gMnSc7	Dia
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
Krona	Kron	k1gMnSc4	Kron
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Zeus	Zeus	k1gInSc4	Zeus
raději	rád	k6eAd2	rád
Métidu	Métida	k1gFnSc4	Métida
spolkl	spolknout	k5eAaPmAgMnS	spolknout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ji	on	k3xPp3gFnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nespatřil	spatřit	k5eNaPmAgMnS	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
Dia	Dia	k1gFnSc4	Dia
krutá	krutý	k2eAgFnSc1d1	krutá
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Tritón	tritón	k1gInSc1	tritón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
přispěchal	přispěchat	k5eAaPmAgInS	přispěchat
bůh	bůh	k1gMnSc1	bůh
Hermés	Hermésa	k1gFnPc2	Hermésa
<g/>
,	,	kIx,	,
donutil	donutit	k5eAaPmAgMnS	donutit
Héfaista	Héfaista	k1gMnSc1	Héfaista
nebo	nebo	k8xC	nebo
Prométhea	Prométheus	k1gMnSc4	Prométheus
přinést	přinést	k5eAaPmF	přinést
klín	klín	k1gInSc4	klín
a	a	k8xC	a
palici	palice	k1gFnSc4	palice
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
udělali	udělat	k5eAaPmAgMnP	udělat
do	do	k7c2	do
Diovy	Diův	k2eAgFnSc2d1	Diova
lebky	lebka	k1gFnSc2	lebka
skulinu	skulina	k1gFnSc4	skulina
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyskočila	vyskočit	k5eAaPmAgFnS	vyskočit
Athéna	Athéna	k1gFnSc1	Athéna
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
znak	znak	k1gInSc4	znak
byla	být	k5eAaImAgFnS	být
sova	sova	k1gFnSc1	sova
Přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
věštby	věštba	k1gFnSc2	věštba
byla	být	k5eAaImAgFnS	být
Athéna	Athéna	k1gFnSc1	Athéna
zosobněná	zosobněný	k2eAgFnSc1d1	zosobněná
moudrost	moudrost	k1gFnSc1	moudrost
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
Diovou	Diův	k2eAgFnSc7d1	Diova
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
dcerou	dcera	k1gFnSc7	dcera
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rozmlouval	rozmlouvat	k5eAaImAgMnS	rozmlouvat
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
neskrýval	skrývat	k5eNaImAgMnS	skrývat
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jí	on	k3xPp3gFnSc3	on
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Athéna	Athéna	k1gFnSc1	Athéna
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gNnSc4	jeho
přízně	přízně	k6eAd1	přízně
považovala	považovat	k5eAaImAgFnS	považovat
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
také	také	k9	také
nezatoužila	zatoužit	k5eNaPmAgFnS	zatoužit
po	po	k7c6	po
žádném	žádný	k1gMnSc6	žádný
jiném	jiný	k1gMnSc6	jiný
bohu	bůh	k1gMnSc6	bůh
nebo	nebo	k8xC	nebo
muži	muž	k1gMnSc6	muž
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
krásu	krása	k1gFnSc4	krása
a	a	k8xC	a
vznešenost	vznešenost	k1gFnSc4	vznešenost
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neprovdala	provdat	k5eNaPmAgFnS	provdat
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Athénou	Athéna	k1gFnSc7	Athéna
Panenskou	panenský	k2eAgFnSc4d1	panenská
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Athéna	Athéna	k1gFnSc1	Athéna
Parthenos	Parthenosa	k1gFnPc2	Parthenosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
byla	být	k5eAaImAgFnS	být
nemilosrdná	milosrdný	k2eNgFnSc1d1	nemilosrdná
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velice	velice	k6eAd1	velice
nadaná	nadaný	k2eAgFnSc1d1	nadaná
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vytahovala	vytahovat	k5eAaImAgFnS	vytahovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
tkadlena	tkadlena	k1gFnSc1	tkadlena
než	než	k8xS	než
sama	sám	k3xTgFnSc1	sám
Athéna	Athéna	k1gFnSc1	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
dívka	dívka	k1gFnSc1	dívka
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Arachné	Arachné	k2eAgFnSc1d1	Arachné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
souboji	souboj	k1gInSc6	souboj
v	v	k7c6	v
tkaní	tkaní	k1gNnSc6	tkaní
ji	on	k3xPp3gFnSc4	on
Athéna	Athéna	k1gFnSc1	Athéna
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
pavouka	pavouk	k1gMnSc4	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
mezi	mezi	k7c4	mezi
sluneční	sluneční	k2eAgNnPc4d1	sluneční
božstva	božstvo	k1gNnPc4	božstvo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
vynálezem	vynález	k1gInSc7	vynález
tkaní	tkaní	k1gNnSc2	tkaní
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
Aton	Aton	k1gInSc1	Aton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
bohyň	bohyně	k1gFnPc2	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
boha	bůh	k1gMnSc2	bůh
zuřivé	zuřivý	k2eAgFnSc2d1	zuřivá
války	válka	k1gFnSc2	válka
Area	area	k1gFnSc1	area
byla	být	k5eAaImAgFnS	být
Athéna	Athéna	k1gFnSc1	Athéna
bohyní	bohyně	k1gFnPc2	bohyně
války	válka	k1gFnSc2	válka
moudře	moudřit	k5eAaImSgInS	moudřit
a	a	k8xC	a
rozvážně	rozvážně	k6eAd1	rozvážně
vedené	vedený	k2eAgNnSc1d1	vedené
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
končící	končící	k2eAgNnSc1d1	končící
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
,	,	kIx,	,
ovládala	ovládat	k5eAaImAgFnS	ovládat
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
strategii	strategie	k1gFnSc4	strategie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
její	její	k3xOp3gFnSc4	její
radu	rada	k1gFnSc4	rada
prosili	prosít	k5eAaPmAgMnP	prosít
moudří	moudrý	k2eAgMnPc1d1	moudrý
náčelníci	náčelník	k1gMnPc1	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
ji	on	k3xPp3gFnSc4	on
ctili	ctít	k5eAaImAgMnP	ctít
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Athénu	Athéna	k1gFnSc4	Athéna
ve	v	k7c6	v
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Athéna	Athéna	k1gFnSc1	Athéna
Enoplos	Enoplos	k1gMnSc1	Enoplos
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Athénu	Athéna	k1gFnSc4	Athéna
vyzývající	vyzývající	k2eAgFnSc4d1	vyzývající
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Athéna	Athéna	k1gFnSc1	Athéna
Promachos	Promachos	k1gMnSc1	Promachos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byla	být	k5eAaImAgNnP	být
zvaná	zvaný	k2eAgNnPc1d1	zvané
Athénou	Athéna	k1gFnSc7	Athéna
Vítěznou	vítězný	k2eAgFnSc7d1	vítězná
(	(	kIx(	(
<g/>
Athéna	Athéna	k1gFnSc1	Athéna
Níké	Níká	k1gFnSc2	Níká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
ochránkyní	ochránkyně	k1gFnSc7	ochránkyně
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
nejmilejší	milý	k2eAgFnSc4d3	nejmilejší
jí	on	k3xPp3gFnSc3	on
byli	být	k5eAaImAgMnP	být
však	však	k9	však
Athéňané	Athéňan	k1gMnPc1	Athéňan
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
chránila	chránit	k5eAaImAgNnP	chránit
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
měla	mít	k5eAaImAgNnP	mít
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
její	její	k3xOp3gFnSc2	její
sošky	soška	k1gFnSc2	soška
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
palladia	palladion	k1gNnSc2	palladion
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
bylo	být	k5eAaImAgNnS	být
palladium	palladium	k1gNnSc1	palladium
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
nedobytné	dobytný	k2eNgNnSc1d1	nedobytné
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
vzácné	vzácný	k2eAgNnSc1d1	vzácné
palladium	palladium	k1gNnSc1	palladium
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
v	v	k7c6	v
Tróji	Trója	k1gFnSc6	Trója
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Pergamu	Pergamos	k1gInSc2	Pergamos
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
Odysseus	Odysseus	k1gInSc4	Odysseus
a	a	k8xC	a
Diomédes	Diomédes	k1gInSc4	Diomédes
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
Trója	Trója	k1gFnSc1	Trója
dobyta	dobyt	k2eAgFnSc1d1	dobyta
<g/>
.	.	kIx.	.
</s>
<s>
Athéna	Athéna	k1gFnSc1	Athéna
však	však	k9	však
byla	být	k5eAaImAgFnS	být
i	i	k9	i
bohyní	bohyně	k1gFnSc7	bohyně
života	život	k1gInSc2	život
v	v	k7c6	v
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
ochránkyní	ochránkyně	k1gFnSc7	ochránkyně
práva	právo	k1gNnSc2	právo
i	i	k8xC	i
sněmů	sněm	k1gInPc2	sněm
<g/>
,	,	kIx,	,
dávala	dávat	k5eAaImAgFnS	dávat
městům	město	k1gNnPc3	město
i	i	k9	i
lidem	člověk	k1gMnPc3	člověk
blahobyt	blahobyt	k1gInSc4	blahobyt
<g/>
,	,	kIx,	,
ochraňovala	ochraňovat	k5eAaImAgFnS	ochraňovat
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
nemocné	nemocný	k1gMnPc4	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Athéňanům	Athéňan	k1gMnPc3	Athéňan
darovala	darovat	k5eAaPmAgFnS	darovat
olivu	oliva	k1gFnSc4	oliva
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zakladatelkou	zakladatelka	k1gFnSc7	zakladatelka
a	a	k8xC	a
ochránkyní	ochránkyně	k1gFnSc7	ochránkyně
řeckého	řecký	k2eAgNnSc2d1	řecké
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Ochraňovala	ochraňovat	k5eAaImAgNnP	ochraňovat
také	také	k9	také
umění	umění	k1gNnPc1	umění
a	a	k8xC	a
umělecké	umělecký	k2eAgFnPc4d1	umělecká
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
naučila	naučit	k5eAaPmAgFnS	naučit
ženy	žena	k1gFnPc4	žena
příst	příst	k5eAaImF	příst
a	a	k8xC	a
tkát	tkát	k5eAaImF	tkát
<g/>
,	,	kIx,	,
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
stavitelům	stavitel	k1gMnPc3	stavitel
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
muže	muž	k1gMnPc4	muž
naučila	naučit	k5eAaPmAgFnS	naučit
řemeslům	řemeslo	k1gNnPc3	řemeslo
jako	jako	k8xC	jako
zlatnictví	zlatnictví	k1gNnSc2	zlatnictví
<g/>
,	,	kIx,	,
kovářství	kovářství	k1gNnSc2	kovářství
<g/>
,	,	kIx,	,
barvířství	barvířství	k1gNnSc2	barvířství
<g/>
.	.	kIx.	.
</s>
<s>
Chránila	chránit	k5eAaImAgFnS	chránit
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jí	on	k3xPp3gFnSc3	on
přinášeli	přinášet	k5eAaImAgMnP	přinášet
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Athéna	Athéna	k1gFnSc1	Athéna
sama	sám	k3xTgFnSc1	sám
vynalezla	vynaleznout	k5eAaPmAgFnS	vynaleznout
pluh	pluh	k1gInSc4	pluh
<g/>
,	,	kIx,	,
hrábě	hrábě	k1gFnPc4	hrábě
<g/>
,	,	kIx,	,
uzdu	uzda	k1gFnSc4	uzda
<g/>
,	,	kIx,	,
jho	jho	k1gNnSc4	jho
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Poseidona	Poseidon	k1gMnSc2	Poseidon
i	i	k8xC	i
válečný	válečný	k2eAgInSc4d1	válečný
vůz	vůz	k1gInSc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hliněný	hliněný	k2eAgInSc4d1	hliněný
hrnec	hrnec	k1gInSc4	hrnec
<g/>
,	,	kIx,	,
trubku	trubka	k1gFnSc4	trubka
a	a	k8xC	a
flétnu	flétna	k1gFnSc4	flétna
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
milosrdná	milosrdný	k2eAgFnSc1d1	milosrdná
<g/>
,	,	kIx,	,
u	u	k7c2	u
hrdelního	hrdelní	k2eAgInSc2d1	hrdelní
soudu	soud	k1gInSc2	soud
na	na	k7c6	na
areopagu	areopag	k1gInSc6	areopag
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vyrovnaných	vyrovnaný	k2eAgInPc2d1	vyrovnaný
hlasů	hlas	k1gInPc2	hlas
soudců	soudce	k1gMnPc2	soudce
vždy	vždy	k6eAd1	vždy
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
obviněného	obviněný	k1gMnSc2	obviněný
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
vzezření	vzezření	k1gNnSc1	vzezření
vzbuzovalo	vzbuzovat	k5eAaImAgNnS	vzbuzovat
respekt	respekt	k1gInSc4	respekt
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
a	a	k8xC	a
urostlá	urostlý	k2eAgFnSc1d1	urostlá
<g/>
,	,	kIx,	,
zobrazovaná	zobrazovaný	k2eAgFnSc1d1	zobrazovaná
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
říze	říza	k1gFnSc6	říza
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
ve	v	k7c6	v
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
nosila	nosit	k5eAaImAgFnS	nosit
přilbici	přilbice	k1gFnSc4	přilbice
<g/>
,	,	kIx,	,
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
měla	mít	k5eAaImAgFnS	mít
kopí	kopí	k1gNnSc4	kopí
<g/>
,	,	kIx,	,
v	v	k7c4	v
levé	levá	k1gFnPc4	levá
štít	štít	k1gInSc4	štít
aigidu	aigid	k1gInSc2	aigid
(	(	kIx(	(
<g/>
též	též	k9	též
egidu	egida	k1gFnSc4	egida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
připevněnou	připevněný	k2eAgFnSc4d1	připevněná
hlavu	hlava	k1gFnSc4	hlava
hrůzné	hrůzný	k2eAgFnSc2d1	hrůzná
Medúsy	Medúsa	k1gFnSc2	Medúsa
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
provázela	provázet	k5eAaImAgFnS	provázet
sova	sova	k1gFnSc1	sova
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Zbroj	zbroj	k1gFnSc1	zbroj
si	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
půjčovala	půjčovat	k5eAaImAgFnS	půjčovat
od	od	k7c2	od
Dia	Dia	k1gFnSc2	Dia
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
době	doba	k1gFnSc6	doba
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
si	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
vytepat	vytepat	k5eAaPmF	vytepat
vlastní	vlastní	k2eAgFnSc1d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
samotného	samotný	k2eAgMnSc4d1	samotný
Héfaista	Héfaist	k1gMnSc4	Héfaist
<g/>
,	,	kIx,	,
božského	božský	k2eAgMnSc4d1	božský
kováře	kovář	k1gMnSc4	kovář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
nechtěl	chtít	k5eNaImAgMnS	chtít
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
peníze	peníz	k1gInSc2	peníz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc4	její
přízeň	přízeň	k1gFnSc4	přízeň
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
znásilnit	znásilnit	k5eAaPmF	znásilnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Athéna	Athéna	k1gFnSc1	Athéna
se	se	k3xPyFc4	se
vytrhla	vytrhnout	k5eAaPmAgFnS	vytrhnout
a	a	k8xC	a
sémě	sémě	k1gNnSc1	sémě
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
oplodnilo	oplodnit	k5eAaPmAgNnS	oplodnit
matku	matka	k1gFnSc4	matka
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
si	se	k3xPyFc3	se
očekávané	očekávaný	k2eAgNnSc4d1	očekávané
dítě	dítě	k1gNnSc4	dítě
nechtěl	chtít	k5eNaImAgMnS	chtít
vzít	vzít	k5eAaPmF	vzít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
a	a	k8xC	a
odpovídat	odpovídat	k5eAaImF	odpovídat
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
výchovu	výchova	k1gFnSc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Athéna	Athéna	k1gFnSc1	Athéna
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
ujala	ujmout	k5eAaPmAgFnS	ujmout
a	a	k8xC	a
chlapec	chlapec	k1gMnSc1	chlapec
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
Erichthonios	Erichthonios	k1gInSc4	Erichthonios
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	Dítě	k2eAgFnSc1d1	Dítě
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
do	do	k7c2	do
posvátného	posvátný	k2eAgInSc2d1	posvátný
košíku	košík	k1gInSc2	košík
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
svěřila	svěřit	k5eAaPmAgFnS	svěřit
Aglauře	Aglaura	k1gFnSc3	Aglaura
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc3d3	nejstarší
dceři	dcera	k1gFnSc3	dcera
athénského	athénský	k2eAgMnSc2d1	athénský
krále	král	k1gMnSc2	král
Kekropa	Kekrop	k1gMnSc2	Kekrop
<g/>
,	,	kIx,	,
a	a	k8xC	a
nařídila	nařídit	k5eAaPmAgFnS	nařídit
jí	jíst	k5eAaImIp3nS	jíst
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dítě	dítě	k1gNnSc1	dítě
přísně	přísně	k6eAd1	přísně
střežila	střežit	k5eAaImAgNnP	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Kekrops	Kekrops	k6eAd1	Kekrops
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
matky	matka	k1gFnSc2	matka
země	zem	k1gFnSc2	zem
a	a	k8xC	a
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
napůl	napůl	k6eAd1	napůl
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
had	had	k1gMnSc1	had
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
vypadal	vypadat	k5eAaPmAgInS	vypadat
Erichthonios	Erichthonios	k1gInSc1	Erichthonios
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kekrops	Kekrops	k1gInSc1	Kekrops
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Aglauros	Aglaurosa	k1gFnPc2	Aglaurosa
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
sestry	sestra	k1gFnPc1	sestra
Hersé	Hersý	k2eAgFnPc1d1	Hersý
a	a	k8xC	a
Pandrosos	Pandrosos	k1gInSc1	Pandrosos
už	už	k6eAd1	už
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
zkrotit	zkrotit	k5eAaPmF	zkrotit
svou	svůj	k3xOyFgFnSc4	svůj
zvědavost	zvědavost	k1gFnSc4	zvědavost
a	a	k8xC	a
košík	košík	k1gInSc4	košík
otevřely	otevřít	k5eAaPmAgFnP	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
uviděly	uvidět	k5eAaPmAgInP	uvidět
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
hadím	hadí	k2eAgInSc7d1	hadí
ocasem	ocas	k1gInSc7	ocas
místo	místo	k1gNnSc4	místo
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
ležel	ležet	k5eAaImAgMnS	ležet
obrovský	obrovský	k2eAgMnSc1d1	obrovský
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
v	v	k7c6	v
panické	panický	k2eAgFnSc6d1	panická
hrůze	hrůza	k1gFnSc6	hrůza
se	se	k3xPyFc4	se
vrhly	vrhnout	k5eAaImAgInP	vrhnout
z	z	k7c2	z
athénské	athénský	k2eAgFnSc2d1	Athénská
Akropole	Akropole	k1gFnSc2	Akropole
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
<g/>
.	.	kIx.	.
</s>
<s>
Erichthonios	Erichthonios	k1gMnSc1	Erichthonios
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stál	stát	k5eAaImAgInS	stát
zakladatelem	zakladatel	k1gMnSc7	zakladatel
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
kult	kult	k1gInSc1	kult
bohyně	bohyně	k1gFnSc2	bohyně
Athény	Athéna	k1gFnSc2	Athéna
a	a	k8xC	a
přikázal	přikázat	k5eAaPmAgInS	přikázat
světit	světit	k5eAaImF	světit
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
bohyně	bohyně	k1gFnSc2	bohyně
Athény	Athéna	k1gFnSc2	Athéna
slavnost	slavnost	k1gFnSc1	slavnost
panathénají	panathénat	k5eAaPmIp3nP	panathénat
<g/>
.	.	kIx.	.
s	s	k7c7	s
Diem	Die	k1gNnSc7	Die
bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
Gigantům	gigant	k1gInPc3	gigant
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
zabila	zabít	k5eAaPmAgFnS	zabít
Enkelada	Enkelada	k1gFnSc1	Enkelada
a	a	k8xC	a
Pallanta	Pallant	k1gMnSc2	Pallant
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
Argonautům	argonaut	k1gMnPc3	argonaut
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
rounem	rouno	k1gNnSc7	rouno
když	když	k8xS	když
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
dostala	dostat	k5eAaPmAgFnS	dostat
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
<g />
.	.	kIx.	.
</s>
<s>
moří	mořit	k5eAaImIp3nP	mořit
Poseidónem	Poseidón	k1gInSc7	Poseidón
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Attikou	Attika	k1gFnSc7	Attika
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
právě	právě	k9	právě
král	král	k1gMnSc1	král
Kekrops	Kekrops	k1gInSc4	Kekrops
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
čí	čí	k3xOyQgInPc4	čí
dar	dar	k1gInSc1	dar
městu	město	k1gNnSc3	město
je	být	k5eAaImIp3nS	být
cennější	cenný	k2eAgMnSc1d2	cennější
<g/>
:	:	kIx,	:
Athéna	Athéna	k1gFnSc1	Athéna
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
darovala	darovat	k5eAaPmAgFnS	darovat
městu	město	k1gNnSc3	město
olivovník	olivovník	k1gInSc1	olivovník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Poseidón	Poseidón	k1gMnSc1	Poseidón
nechal	nechat	k5eAaPmAgMnS	nechat
vytrysknout	vytrysknout	k5eAaPmF	vytrysknout
pramen	pramen	k1gInSc4	pramen
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
provázela	provázet	k5eAaImAgFnS	provázet
Persea	Perseus	k1gMnSc4	Perseus
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Gorgonám	Gorgo	k1gFnPc3	Gorgo
<g />
.	.	kIx.	.
</s>
<s>
rekovi	rek	k1gMnSc3	rek
Bellerofontovi	Bellerofont	k1gMnSc3	Bellerofont
dala	dát	k5eAaPmAgFnS	dát
uzdu	uzda	k1gFnSc4	uzda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
ovládnout	ovládnout	k5eAaPmF	ovládnout
okřídleného	okřídlený	k2eAgMnSc4d1	okřídlený
koně	kůň	k1gMnSc4	kůň
Pégasa	Pégas	k1gMnSc4	Pégas
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
urazil	urazit	k5eAaPmAgMnS	urazit
trojský	trojský	k2eAgMnSc1d1	trojský
princ	princ	k1gMnSc1	princ
Paris	Paris	k1gMnSc1	Paris
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nepřiznal	přiznat	k5eNaPmAgInS	přiznat
prvenství	prvenství	k1gNnSc4	prvenství
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
jablko	jablko	k1gNnSc4	jablko
"	"	kIx"	"
<g/>
té	ten	k3xDgFnSc2	ten
nejkrásnější	krásný	k2eAgFnSc2d3	nejkrásnější
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pomstila	pomstit	k5eAaImAgFnS	pomstit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Achajců	Achajce	k1gMnPc2	Achajce
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
<g />
.	.	kIx.	.
</s>
<s>
jim	on	k3xPp3gInPc3	on
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
Trójou	Trója	k1gFnSc7	Trója
Když	když	k8xS	když
se	se	k3xPyFc4	se
Diomédes	Diomédes	k1gMnSc1	Diomédes
-	-	kIx~	-
její	její	k3xOp3gMnSc1	její
ctitel	ctitel	k1gMnSc1	ctitel
-	-	kIx~	-
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
do	do	k7c2	do
úzkých	úzké	k1gInPc2	úzké
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
sedla	sednout	k5eAaPmAgFnS	sednout
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
válečného	válečný	k2eAgInSc2d1	válečný
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
zachránila	zachránit	k5eAaPmAgFnS	zachránit
ho	on	k3xPp3gMnSc4	on
radila	radil	k1gMnSc4	radil
Epeiovi	Epeius	k1gMnSc3	Epeius
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
trójského	trójský	k2eAgMnSc2d1	trójský
koně	kůň	k1gMnSc2	kůň
Pomohla	pomoct	k5eAaPmAgFnS	pomoct
ithackému	ithacký	k2eAgMnSc3d1	ithacký
králi	král	k1gMnSc3	král
Odysseovi	Odysseus	k1gMnSc3	Odysseus
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
porazit	porazit	k5eAaPmF	porazit
přesilu	přesila	k1gFnSc4	přesila
nepřátel	nepřítel	k1gMnPc2	nepřítel
když	když	k8xS	když
thébský	thébský	k2eAgMnSc1d1	thébský
věštec	věštec	k1gMnSc1	věštec
Teiresiás	Teiresiása	k1gFnPc2	Teiresiása
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
mýtu	mýtus	k1gInSc2	mýtus
spatřil	spatřit	k5eAaPmAgMnS	spatřit
nedopatřením	nedopatření	k1gNnSc7	nedopatření
Athénu	Athéna	k1gFnSc4	Athéna
nahou	nahý	k2eAgFnSc4d1	nahá
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
trest	trest	k1gInSc4	trest
stihla	stihnout	k5eAaPmAgFnS	stihnout
slepotou	slepota	k1gFnSc7	slepota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
mu	on	k3xPp3gMnSc3	on
dala	dát	k5eAaPmAgFnS	dát
schopnost	schopnost	k1gFnSc4	schopnost
rozumět	rozumět	k5eAaImF	rozumět
řeči	řeč	k1gFnPc4	řeč
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mu	on	k3xPp3gNnSc3	on
věnovala	věnovat	k5eAaImAgFnS	věnovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
slepeckou	slepecký	k2eAgFnSc4d1	slepecká
hůl	hůl	k1gFnSc4	hůl
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
bezpečně	bezpečně	k6eAd1	bezpečně
vedla	vést	k5eAaImAgFnS	vést
a	a	k8xC	a
darovala	darovat	k5eAaPmAgFnS	darovat
mu	on	k3xPp3gNnSc3	on
též	též	k9	též
hodně	hodně	k6eAd1	hodně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
život	život	k1gInSc1	život
jiný	jiný	k2eAgInSc1d1	jiný
byl	být	k5eAaImAgInS	být
případ	případ	k1gInSc1	případ
Arachné	Arachná	k1gFnSc2	Arachná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
soupeřila	soupeřit	k5eAaImAgFnS	soupeřit
s	s	k7c7	s
Athénou	Athéna	k1gFnSc7	Athéna
ve	v	k7c6	v
tkaní	tkaní	k1gNnSc6	tkaní
-	-	kIx~	-
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
pýchu	pýcha	k1gFnSc4	pýcha
ji	on	k3xPp3gFnSc4	on
bohyně	bohyně	k1gFnSc1	bohyně
tvrdě	tvrdě	k6eAd1	tvrdě
potrestala	potrestat	k5eAaPmAgFnS	potrestat
<g/>
,	,	kIx,	,
proměnila	proměnit	k5eAaPmAgFnS	proměnit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
pavouka	pavouk	k1gMnSc2	pavouk
Podobných	podobný	k2eAgInPc2d1	podobný
příběhů	příběh	k1gInPc2	příběh
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
a	a	k8xC	a
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
Athéna	Athéna	k1gFnSc1	Athéna
nikdy	nikdy	k6eAd1	nikdy
své	svůj	k3xOyFgFnPc4	svůj
věrné	věrný	k2eAgFnPc4d1	věrná
neopustila	opustit	k5eNaPmAgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
vždy	vždy	k6eAd1	vždy
Řekům	Řek	k1gMnPc3	Řek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Athéňanům	Athéňan	k1gMnPc3	Athéňan
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Římanům	Říman	k1gMnPc3	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ji	on	k3xPp3gFnSc4	on
rovněž	rovněž	k6eAd1	rovněž
ctili	ctít	k5eAaImAgMnP	ctít
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Panathénaje	Panathénaje	k1gFnSc2	Panathénaje
<g/>
.	.	kIx.	.
</s>
<s>
Panathénaje	Panathénat	k5eAaImSgMnS	Panathénat
byly	být	k5eAaImAgFnP	být
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
Řekové	Řek	k1gMnPc1	Řek
vzdávali	vzdávat	k5eAaImAgMnP	vzdávat
pocty	pocta	k1gFnPc4	pocta
Athéně	Athéna	k1gFnSc3	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
rok	rok	k1gInSc4	rok
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
velké	velký	k2eAgInPc4d1	velký
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
začínaly	začínat	k5eAaImAgInP	začínat
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Ahéniných	Ahénin	k2eAgFnPc2d1	Ahénin
narozenin	narozeniny	k1gFnPc2	narozeniny
(	(	kIx(	(
<g/>
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
pochodňovým	pochodňový	k2eAgInSc7d1	pochodňový
během	běh	k1gInSc7	běh
<g/>
,	,	kIx,	,
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
průvodem	průvod	k1gInSc7	průvod
na	na	k7c4	na
Akropolis	Akropolis	k1gFnSc4	Akropolis
a	a	k8xC	a
končily	končit	k5eAaImAgInP	končit
veslařskými	veslařský	k2eAgInPc7d1	veslařský
závody	závod	k1gInPc7	závod
v	v	k7c6	v
Peiraieu	Peiraieum	k1gNnSc6	Peiraieum
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
součástí	součást	k1gFnSc7	součást
byly	být	k5eAaImAgInP	být
také	také	k9	také
závody	závod	k1gInPc1	závod
hudební	hudební	k2eAgInPc1d1	hudební
<g/>
,	,	kIx,	,
básnické	básnický	k2eAgInPc1d1	básnický
<g/>
,	,	kIx,	,
řečnické	řečnický	k2eAgInPc1d1	řečnický
<g/>
,	,	kIx,	,
gymnastické	gymnastický	k2eAgInPc1d1	gymnastický
a	a	k8xC	a
jezdecké	jezdecký	k2eAgInPc1d1	jezdecký
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Malé	Malé	k2eAgFnPc1d1	Malé
<g/>
"	"	kIx"	"
panathénaje	panathénat	k5eAaImSgInS	panathénat
se	se	k3xPyFc4	se
slavily	slavit	k5eAaImAgInP	slavit
každoročně	každoročně	k6eAd1	každoročně
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
méně	málo	k6eAd2	málo
okázalé	okázalý	k2eAgFnPc1d1	okázalá
<g/>
.	.	kIx.	.
</s>
<s>
Epiteta	epiteton	k1gNnPc1	epiteton
<g/>
:	:	kIx,	:
Polias	Polias	k1gInSc1	Polias
-	-	kIx~	-
ochránkyně	ochránkyně	k1gFnPc1	ochránkyně
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
promachos	promachos	k1gInSc1	promachos
-	-	kIx~	-
patronka	patronka	k1gFnSc1	patronka
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
nikéforos	nikéforosa	k1gFnPc2	nikéforosa
-	-	kIx~	-
propůjčující	propůjčující	k2eAgNnSc1d1	propůjčující
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
,	,	kIx,	,
ergané	erganá	k1gFnPc1	erganá
-	-	kIx~	-
patronka	patronka	k1gFnSc1	patronka
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
hygieiá	hygieiat	k5eAaPmIp3nS	hygieiat
-	-	kIx~	-
léčící	léčící	k2eAgFnSc1d1	léčící
<g/>
,	,	kIx,	,
kúrotrofos	kúrotrofos	k1gInSc1	kúrotrofos
-	-	kIx~	-
opatrovnice	opatrovnice	k1gFnSc1	opatrovnice
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tritónis	tritónis	k1gFnSc1	tritónis
<g/>
,	,	kIx,	,
tritogenická	tritogenický	k2eAgFnSc1d1	tritogenický
-	-	kIx~	-
tritónská	tritónský	k2eAgFnSc1d1	tritónský
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
glaukópis	glaukópis	k1gFnSc1	glaukópis
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
sovooká	sovooký	k2eAgFnSc1d1	sovooký
<g/>
,	,	kIx,	,
parthenos	parthenos	k1gInSc1	parthenos
-	-	kIx~	-
panenská	panenský	k2eAgFnSc1d1	panenská
<g/>
,	,	kIx,	,
enhoplos	enhoplos	k1gInSc1	enhoplos
-	-	kIx~	-
připravená	připravený	k2eAgFnSc1d1	připravená
k	k	k7c3	k
boji	boj	k1gInSc3	boj
Ke	k	k7c3	k
jménu	jméno	k1gNnSc3	jméno
Athény	Athéna	k1gFnSc2	Athéna
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nP	pojit
nejslavnější	slavný	k2eAgInPc1d3	nejslavnější
výtvory	výtvor	k1gInPc1	výtvor
řeckého	řecký	k2eAgNnSc2d1	řecké
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
,	,	kIx,	,
namátkou	namátkou	k6eAd1	namátkou
chrámy	chrám	k1gInPc1	chrám
na	na	k7c6	na
athénské	athénský	k2eAgFnSc6d1	Athénská
Akropoli	Akropole	k1gFnSc6	Akropole
<g/>
:	:	kIx,	:
Parthenón	Parthenón	k1gInSc1	Parthenón
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
architektů	architekt	k1gMnPc2	architekt
Iktina	Iktino	k1gNnSc2	Iktino
a	a	k8xC	a
Kallikrata	Kallikrata	k1gFnSc1	Kallikrata
z	z	k7c2	z
let	léto	k1gNnPc2	léto
447	[number]	k4	447
-	-	kIx~	-
434	[number]	k4	434
<g />
.	.	kIx.	.
</s>
<s>
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
chrám	chrám	k1gInSc1	chrám
Níké	Níká	k1gFnSc2	Níká
<g/>
,	,	kIx,	,
zasvěcený	zasvěcený	k2eAgMnSc1d1	zasvěcený
Athéně	Athéna	k1gFnSc3	Athéna
Vítězné	vítězný	k2eAgInPc1d1	vítězný
Erechtheion	Erechtheion	k1gInSc1	Erechtheion
<g/>
,	,	kIx,	,
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
Athéně	Athéna	k1gFnSc3	Athéna
<g/>
,	,	kIx,	,
Poseidónovi	Poseidón	k1gMnSc3	Poseidón
a	a	k8xC	a
Erechtheovi	Erechtheus	k1gMnSc3	Erechtheus
Ze	z	k7c2	z
sochařských	sochařský	k2eAgFnPc2d1	sochařská
dílen	dílna	k1gFnPc2	dílna
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgInPc1d3	nejznámější
výtvory	výtvor	k1gInPc1	výtvor
<g/>
:	:	kIx,	:
Feidiovy	Feidiův	k2eAgFnPc1d1	Feidiova
sochy	socha	k1gFnPc1	socha
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
Athéna	Athéna	k1gFnSc1	Athéna
Parthenos	Parthenos	k1gMnSc1	Parthenos
<g/>
,	,	kIx,	,
Athéna	Athéna	k1gFnSc1	Athéna
Promachos	Promachos	k1gMnSc1	Promachos
a	a	k8xC	a
Athéna	Athéna	k1gFnSc1	Athéna
Lemnia	Lemnium	k1gNnSc2	Lemnium
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
bohužel	bohužel	k6eAd1	bohužel
jenom	jenom	k9	jenom
z	z	k7c2	z
pozdních	pozdní	k2eAgFnPc2d1	pozdní
kopií	kopie	k1gFnPc2	kopie
reliéfová	reliéfový	k2eAgFnSc1d1	reliéfová
výzdoba	výzdoba	k1gFnSc1	výzdoba
Parthenónu	Parthenón	k1gInSc2	Parthenón
<g/>
,	,	kIx,	,
zhotovená	zhotovený	k2eAgNnPc4d1	zhotovené
pod	pod	k7c7	pod
Feidiovým	Feidiový	k2eAgNnSc7d1	Feidiový
vedením	vedení	k1gNnSc7	vedení
Athéna	Athéna	k1gFnSc1	Athéna
Myrónova	Myrónův	k2eAgFnSc1d1	Myrónův
(	(	kIx(	(
<g/>
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Athéna	Athéna	k1gFnSc1	Athéna
Varvakejská	Varvakejský	k2eAgFnSc1d1	Varvakejský
<g/>
,	,	kIx,	,
napodobenina	napodobenina	k1gFnSc1	napodobenina
Feidiovy	Feidiův	k2eAgFnSc2d1	Feidiova
chrámové	chrámový	k2eAgFnSc2d1	chrámová
sochy	socha	k1gFnSc2	socha
z	z	k7c2	z
Parthenónu	Parthenón	k1gInSc2	Parthenón
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
Athena	Atheno	k1gNnPc1	Atheno
Dánská	dánský	k2eAgNnPc1d1	dánské
</s>
