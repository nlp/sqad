<s>
Vikariát	vikariát	k1gInSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
město	město	k1gNnSc1
</s>
<s>
vikariát	vikariát	k1gInSc1
České	český	k2eAgFnSc2d1
Budějovice-městoDiecéze	Budějovice-městoDiecéza	k1gFnSc3
</s>
<s>
českobudějovická	českobudějovický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Okrskový	okrskový	k2eAgInSc4d1
vikář	vikář	k1gMnSc1
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Zdeněk	Zdeněk	k1gMnSc1
Mareš	Mareš	k1gMnSc1
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
vikáře	vikář	k1gMnSc2
</s>
<s>
děkan	děkan	k1gMnSc1
<g/>
,	,	kIx,
České	český	k2eAgFnSc6d1
Budějovice-	Budějovice-	k1gFnSc6
<g/>
sv.	sv.	kA
Mikuláš	mikuláš	k1gInSc1
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
2018	#num#	k4
(	(	kIx(
<g/>
X.	X.	kA
<g/>
)	)	kIx)
</s>
<s>
Vikariát	vikariát	k1gInSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
město	město	k1gNnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
deseti	deset	k4xCc2
vikariátů	vikariát	k1gInPc2
českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Dr	dr	kA
<g/>
.	.	kIx.
Zdeněk	Zdeněk	k1gMnSc1
Mareš	Mareš	k1gMnSc1
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
<g/>
,	,	kIx,
děkan	děkan	k1gMnSc1
ve	v	k7c6
farnosti	farnost	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
-	-	kIx~
<g/>
sv.	sv.	kA
Mikuláš	Mikuláš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vikariátní	vikariátní	k2eAgInSc1d1
sekretář	sekretář	k1gInSc1
není	být	k5eNaImIp3nS
obsazen	obsadit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
šesti	šest	k4xCc2
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
v	v	k7c6
pěti	pět	k4xCc6
sídlí	sídlet	k5eAaImIp3nS
děkan	děkan	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
kostela	kostel	k1gInSc2
nebo	nebo	k8xC
administrátor	administrátor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
duchovní	duchovní	k2eAgFnSc6d1
správě	správa	k1gFnSc6
zde	zde	k6eAd1
působí	působit	k5eAaImIp3nS
15	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
katedrální	katedrální	k2eAgInSc1d1
chrám	chrám	k1gInSc1
Českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
a	a	k8xC
také	také	k9
kněžský	kněžský	k2eAgInSc4d1
domov	domov	k1gInSc4
pro	pro	k7c4
staré	starý	k2eAgMnPc4d1
a	a	k8xC
nemocné	mocný	k2eNgMnPc4d1,k2eAgMnPc4d1
kněze	kněz	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
</s>
<s>
FarnostDuchovní	FarnostDuchovní	k2eAgNnSc1d1
správceDalší	správceDalší	k1gNnSc1
duchovní	duchovní	k2eAgNnSc1d1
ve	v	k7c4
farnostiFarní	farnostiFarní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
-	-	kIx~
Panny	Panna	k1gFnPc1
Marie	Maria	k1gFnSc2
Růžencové	růžencový	k2eAgFnPc1d1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
J.	J.	kA
M.	M.	kA
Vianney	Viannea	k1gMnSc2
Peter	Peter	k1gMnSc1
Madár	Madár	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
kostela	kostel	k1gInSc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Maxmilián	Maxmilián	k1gMnSc1
Petr	Petr	k1gMnSc1
Koutský	Koutský	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
České	český	k2eAgFnPc1d1
Budějovice-Suché	Budějovice-Suchý	k2eAgFnPc1d1
Vrbné	Vrbná	k1gFnPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Witold	Witold	k1gMnSc1
Piotr	Piotr	k1gMnSc1
Kocon	Kocon	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Bc.	Bc.	k1gFnSc1
Ladislav	Ladislav	k1gMnSc1
Proks	Proks	k1gInSc1
<g/>
,	,	kIx,
farní	farní	k2eAgInSc1d1
vikářMgr	vikářMgr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Hes	hes	k1gNnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Cyrila	Cyril	k1gMnSc4
a	a	k8xC
Metoděje	Metoděj	k1gMnSc4
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
sv.	sv.	kA
Jan	Jan	k1gMnSc1
Nepomucký	Nepomucký	k1gMnSc1
</s>
<s>
P.	P.	kA
Bc.	Bc.	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Bicek	Bicek	k1gMnSc1
</s>
<s>
–	–	k?
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
sv.	sv.	kA
Mikuláš	Mikuláš	k1gMnSc1
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Zdeněk	Zdeněk	k1gMnSc1
Mareš	Mareš	k1gMnSc1
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
<g/>
,	,	kIx,
děkan	děkan	k1gMnSc1
</s>
<s>
P.	P.	kA
Dominik	Dominik	k1gMnSc1
Hroznata	Hroznat	k1gMnSc2
Holický	holický	k2eAgInSc1d1
JC	JC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Bohuslav	Bohuslav	k1gMnSc1
Richter	Richter	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Miroslav	Miroslav	k1gMnSc1
Šašek	Šašek	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
P.	P.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Němec	Němec	k1gMnSc1
DiS.	DiS.	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgInSc1d1
vikářMons	vikářMons	k1gInSc1
<g/>
.	.	kIx.
prof.	prof.	kA
Dr	dr	kA
<g/>
.	.	kIx.
Karel	Karel	k1gMnSc1
Skalický	Skalický	k1gMnSc1
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
Mons	Mons	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Kulhánek	Kulhánek	k1gMnSc1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Blažek	Blažek	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgInSc1d1
vikářdoc	vikářdoc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ThDr.	ThDr.	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
ThD	ThD	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
–	–	k?
katedrála	katedrál	k1gMnSc2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
sv.	sv.	kA
Vojtěch	Vojtěch	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Josef	Josef	k1gMnSc1
Mendel	Mendel	k1gMnSc1
SDB	SDB	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Vít	Vít	k1gMnSc1
Dlapka	Dlapek	k1gMnSc2
SDB	SDB	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Kuchař	Kuchař	k1gMnSc1
SDB	SDB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Th	Th	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc4
</s>
<s>
Dobrá	dobrý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Witold	Witold	k1gMnSc1
Piotr	Piotr	k1gMnSc1
Kocon	Kocon	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
Panny	Panna	k1gFnPc1
Marie	Maria	k1gFnSc2
Bolestné	bolestný	k2eAgFnPc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vikariáty	vikariát	k1gInPc1
českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
město	město	k1gNnSc4
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
venkov	venkov	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
•	•	k?
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
•	•	k?
Pelhřimov	Pelhřimov	k1gInSc1
•	•	k?
Písek	Písek	k1gInSc1
•	•	k?
Prachatice	Prachatice	k1gFnPc4
•	•	k?
Strakonice	Strakonice	k1gFnPc4
•	•	k?
Sušice-Nepomuk	Sušice-Nepomuk	k1gMnSc1
•	•	k?
Tábor	Tábor	k1gInSc1
</s>
