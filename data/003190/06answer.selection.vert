<s>
A	a	k9	a
od	od	k7c2	od
fanoušků	fanoušek	k1gMnPc2	fanoušek
se	se	k3xPyFc4	se
zase	zase	k9	zase
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
"	"	kIx"	"
<g/>
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
systému	systém	k1gInSc3	systém
a	a	k8xC	a
odluka	odluka	k1gFnSc1	odluka
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lidé	člověk	k1gMnPc1	člověk
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
metalem	metal	k1gInSc7	metal
i	i	k9	i
oddaní	oddaný	k2eAgMnPc1d1	oddaný
fanoušci	fanoušek	k1gMnPc1	fanoušek
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
považovat	považovat	k5eAaImF	považovat
některé	některý	k3yIgMnPc4	některý
interprety	interpret	k1gMnPc4	interpret
(	(	kIx(	(
<g/>
a	a	k8xC	a
též	též	k9	též
některé	některý	k3yIgMnPc4	některý
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
)	)	kIx)	)
za	za	k7c2	za
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pozéry	pozér	k1gMnPc4	pozér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
předstírají	předstírat	k5eAaImIp3nP	předstírat
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
subkultuře	subkultura	k1gFnSc3	subkultura
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nevědí	vědět	k5eNaImIp3nP	vědět
nic	nic	k6eAd1	nic
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
textech	text	k1gInPc6	text
ani	ani	k8xC	ani
poselstvích	poselství	k1gNnPc6	poselství
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
muziku	muzika	k1gFnSc4	muzika
poslouchají	poslouchat	k5eAaImIp3nP	poslouchat
<g/>
.	.	kIx.	.
</s>
