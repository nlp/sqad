<s>
Jak	jak	k8xS	jak
bývají	bývat	k5eAaImIp3nP	bývat
označováni	označován	k2eAgMnPc1d1	označován
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
subkultuře	subkultura	k1gFnSc3	subkultura
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
o	o	k7c6	o
muzice	muzika	k1gFnSc6	muzika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
poslouchají	poslouchat	k5eAaImIp3nP	poslouchat
nic	nic	k6eAd1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
?	?	kIx.	?
</s>
