<p>
<s>
Já	já	k3xPp1nSc1	já
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
dělám	dělat	k5eAaImIp1nS	dělat
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
Bratří	bratr	k1gMnPc2	bratr
Ebenů	eben	k1gInPc2	eben
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
12	[number]	k4	12
písniček	písnička	k1gFnPc2	písnička
Marka	Marek	k1gMnSc2	Marek
Ebena	Eben	k1gMnSc2	Eben
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
zhudebněné	zhudebněný	k2eAgInPc1d1	zhudebněný
sonety	sonet	k1gInPc1	sonet
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Jana	Jan	k1gMnSc2	Jan
Vladislava	Vladislav	k1gMnSc2	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Vidíš	vidět	k5eAaImIp2nS	vidět
<g/>
,	,	kIx,	,
vidíš	vidět	k5eAaImIp2nS	vidět
zpívá	zpívat	k5eAaImIp3nS	zpívat
Iva	Iva	k1gFnSc1	Iva
Bittová	Bittová	k1gFnSc1	Bittová
<g/>
,	,	kIx,	,
Sprostý	sprostý	k2eAgMnSc1d1	sprostý
chlap	chlap	k1gMnSc1	chlap
Jiří	Jiří	k1gMnSc1	Jiří
Schmitzer	Schmitzer	k1gMnSc1	Schmitzer
a	a	k8xC	a
v	v	k7c6	v
Senecte	Senecte	k1gFnSc6	Senecte
<g/>
,	,	kIx,	,
pomoz	pomoct	k5eAaPmRp2nSwC	pomoct
hostuje	hostovat	k5eAaImIp3nS	hostovat
Jiří	Jiří	k1gMnSc1	Jiří
Bartoška	Bartoška	k1gMnSc1	Bartoška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
takové	takový	k3xDgNnSc1	takový
<g/>
...	...	k?	...
si	se	k3xPyFc3	se
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc4	eben
dělá	dělat	k5eAaImIp3nS	dělat
legraci	legrace	k1gFnSc4	legrace
z	z	k7c2	z
mluvy	mluva	k1gFnSc2	mluva
obyvatel	obyvatel	k1gMnPc2	obyvatel
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
Trampská	trampský	k2eAgFnSc1d1	trampská
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgNnSc4d1	ebenové
hráli	hrát	k5eAaImAgMnP	hrát
již	již	k9	již
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
paroduje	parodovat	k5eAaImIp3nS	parodovat
Jana	Jan	k1gMnSc4	Jan
Nedvěda	Nedvěd	k1gMnSc4	Nedvěd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Písničky	písnička	k1gFnSc2	písnička
==	==	k?	==
</s>
</p>
<p>
<s>
Já	já	k3xPp1nSc1	já
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
dělám	dělat	k5eAaImIp1nS	dělat
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
</s>
</p>
<p>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
to	ten	k3xDgNnSc4	ten
nebere	brát	k5eNaImIp3nS	brát
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
</s>
</p>
<p>
<s>
Hotely	hotel	k1gInPc1	hotel
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
59	[number]	k4	59
</s>
</p>
<p>
<s>
Vidíš	vidět	k5eAaImIp2nS	vidět
<g/>
,	,	kIx,	,
vidíš	vidět	k5eAaImIp2nS	vidět
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
38	[number]	k4	38
</s>
</p>
<p>
<s>
Masky	maska	k1gFnPc4	maska
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
Na	na	k7c6	na
růžích	růž	k1gFnPc6	růž
ustláno	ustlat	k5eAaPmNgNnS	ustlat
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
</s>
</p>
<p>
<s>
Senecte	Senéct	k5eAaPmRp2nPwK	Senéct
<g/>
,	,	kIx,	,
pomoz	pomoct	k5eAaPmRp2nSwC	pomoct
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
</s>
</p>
<p>
<s>
O	o	k7c6	o
filmových	filmový	k2eAgMnPc6d1	filmový
milovnících	milovník	k1gMnPc6	milovník
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
</s>
</p>
<p>
<s>
Sprostý	sprostý	k2eAgMnSc1d1	sprostý
chlap	chlap	k1gMnSc1	chlap
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
</s>
</p>
<p>
<s>
Sonet	sonet	k1gInSc1	sonet
57	[number]	k4	57
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
</s>
</p>
<p>
<s>
Trampská	trampský	k2eAgFnSc1d1	trampská
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
53	[number]	k4	53
</s>
</p>
<p>
<s>
Něva	Něva	k1gFnSc1	Něva
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
takové	takový	k3xDgInPc4	takový
<g/>
...	...	k?	...
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
42	[number]	k4	42
</s>
</p>
<p>
<s>
Sonet	sonet	k1gInSc1	sonet
66	[number]	k4	66
–	–	k?	–
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
</s>
</p>
