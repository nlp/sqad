<p>
<s>
Ident	Ident	k1gInSc1	Ident
protokol	protokol	k1gInSc1	protokol
(	(	kIx(	(
<g/>
popsaný	popsaný	k2eAgInSc1d1	popsaný
v	v	k7c6	v
RFC	RFC	kA	RFC
1413	[number]	k4	1413
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
internetový	internetový	k2eAgInSc4d1	internetový
protokol	protokol	k1gInSc4	protokol
sloužící	sloužící	k2eAgInSc4d1	sloužící
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
identifikace	identifikace	k1gFnSc2	identifikace
uživatele	uživatel	k1gMnSc2	uživatel
pomocí	pomocí	k7c2	pomocí
samostatného	samostatný	k2eAgNnSc2d1	samostatné
TCP	TCP	kA	TCP
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
démonem	démon	k1gMnSc7	démon
poskytujícím	poskytující	k2eAgFnPc3d1	poskytující
tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
je	být	k5eAaImIp3nS	být
identd	identd	k1gInSc1	identd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jak	jak	k8xS	jak
Ident	Ident	k1gMnSc1	Ident
pracuje	pracovat	k5eAaImIp3nS	pracovat
==	==	k?	==
</s>
</p>
<p>
<s>
Protokol	protokol	k1gInSc1	protokol
Ident	Ident	k1gInSc1	Ident
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k8xC	jako
serverová	serverový	k2eAgFnSc1d1	serverová
aplikace	aplikace	k1gFnSc1	aplikace
běžící	běžící	k2eAgFnSc1d1	běžící
na	na	k7c6	na
uživatelově	uživatelův	k2eAgInSc6d1	uživatelův
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
na	na	k7c6	na
specifickém	specifický	k2eAgInSc6d1	specifický
portu	port	k1gInSc6	port
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
portu	port	k1gInSc2	port
113	[number]	k4	113
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
jako	jako	k9	jako
odezvu	odezvat	k5eAaPmIp1nS	odezvat
v	v	k7c6	v
odpovědi	odpověď	k1gFnSc6	odpověď
na	na	k7c4	na
požadavek	požadavek	k1gInSc4	požadavek
odešle	odešle	k6eAd1	odešle
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
jméno	jméno	k1gNnSc4	jméno
právě	právě	k6eAd1	právě
přihlášeného	přihlášený	k2eAgMnSc4d1	přihlášený
uživatele	uživatel	k1gMnSc4	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užitečnost	užitečnost	k1gFnSc4	užitečnost
protokolu	protokol	k1gInSc2	protokol
==	==	k?	==
</s>
</p>
<p>
<s>
Ident	Ident	k1gInSc1	Ident
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
užitečný	užitečný	k2eAgInSc4d1	užitečný
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zjistit	zjistit	k5eAaPmF	zjistit
jméno	jméno	k1gNnSc4	jméno
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
spojení	spojení	k1gNnSc4	spojení
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
server	server	k1gInSc4	server
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
zamezit	zamezit	k5eAaPmF	zamezit
zneužití	zneužití	k1gNnSc4	zneužití
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
statistik	statistika	k1gFnPc2	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
užitečný	užitečný	k2eAgInSc1d1	užitečný
také	také	k9	také
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přihlášeno	přihlásit	k5eAaPmNgNnS	přihlásit
více	hodně	k6eAd2	hodně
uživatelů	uživatel	k1gMnPc2	uživatel
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
protokol	protokol	k1gInSc1	protokol
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
neúčinný	účinný	k2eNgInSc1d1	neúčinný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
zneužití	zneužití	k1gNnSc2	zneužití
služby	služba	k1gFnSc2	služba
administrátor	administrátor	k1gMnSc1	administrátor
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
stupně	stupeň	k1gInPc4	stupeň
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
se	se	k3xPyFc4	se
také	také	k9	také
ident	ident	k1gMnSc1	ident
může	moct	k5eAaImIp3nS	moct
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
podle	podle	k7c2	podle
reverzního	reverzní	k2eAgInSc2d1	reverzní
DNS	DNS	kA	DNS
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Filtrování	filtrování	k1gNnPc4	filtrování
ident	ident	k1gInSc1	ident
portu	port	k1gInSc2	port
může	moct	k5eAaImIp3nS	moct
při	pře	k1gFnSc3	pře
připojování	připojování	k1gNnSc2	připojování
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
službám	služba	k1gFnPc3	služba
nebo	nebo	k8xC	nebo
serverům	server	k1gInPc3	server
způsobit	způsobit	k5eAaPmF	způsobit
časové	časový	k2eAgFnPc1d1	časová
prodlevy	prodleva	k1gFnPc1	prodleva
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ident	ident	k1gInSc1	ident
server	server	k1gInSc1	server
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
požadavek	požadavek	k1gInSc4	požadavek
a	a	k8xC	a
v	v	k7c6	v
odpovědi	odpověď	k1gFnSc6	odpověď
posílá	posílat	k5eAaImIp3nS	posílat
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
jméno	jméno	k1gNnSc4	jméno
osoby	osoba	k1gFnSc2	osoba
přihlášené	přihlášený	k2eAgFnSc2d1	přihlášená
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
nastavit	nastavit	k5eAaPmF	nastavit
filtrovací	filtrovací	k2eAgNnPc4d1	filtrovací
pravidla	pravidlo	k1gNnPc4	pravidlo
na	na	k7c4	na
firewallu	firewalla	k1gFnSc4	firewalla
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
z	z	k7c2	z
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
není	být	k5eNaImIp3nS	být
navázáno	navázán	k2eAgNnSc1d1	navázáno
žádné	žádný	k3yNgNnSc4	žádný
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ident	Ident	k1gInSc1	Ident
protokol	protokol	k1gInSc1	protokol
není	být	k5eNaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hackerům	hacker	k1gMnPc3	hacker
získat	získat	k5eAaPmF	získat
seznam	seznam	k1gInSc4	seznam
uživatelských	uživatelský	k2eAgNnPc2d1	Uživatelské
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
využit	využít	k5eAaPmNgInS	využít
pro	pro	k7c4	pro
počítačový	počítačový	k2eAgInSc4d1	počítačový
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
uznávané	uznávaný	k2eAgNnSc4d1	uznávané
řešení	řešení	k1gNnSc4	řešení
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vytvořit	vytvořit	k5eAaPmF	vytvořit
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
nebo	nebo	k8xC	nebo
automaticky	automaticky	k6eAd1	automaticky
generovaný	generovaný	k2eAgInSc1d1	generovaný
identifikátor	identifikátor	k1gInSc1	identifikátor
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgMnSc1d1	vracející
například	například	k6eAd1	například
Kerberos	Kerberos	k1gMnSc1	Kerberos
tickety	ticketa	k1gFnSc2	ticketa
namísto	namísto	k7c2	namísto
uživatelských	uživatelský	k2eAgNnPc2d1	Uživatelské
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
UN	UN	kA	UN
<g/>
*	*	kIx~	*
<g/>
Xových	Xových	k2eAgInPc6d1	Xových
systémech	systém	k1gInPc6	systém
je	být	k5eAaImIp3nS	být
identd	identd	k6eAd1	identd
služba	služba	k1gFnSc1	služba
obvykle	obvykle	k6eAd1	obvykle
spouštěna	spouštět	k5eAaImNgFnS	spouštět
pomocí	pomocí	k7c2	pomocí
inetd	inetda	k1gFnPc2	inetda
<g/>
/	/	kIx~	/
<g/>
tcp	tcp	k?	tcp
<g/>
,	,	kIx,	,
xinetd	xinetd	k6eAd1	xinetd
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
slinkován	slinkován	k2eAgMnSc1d1	slinkován
s	s	k7c7	s
knihovnou	knihovna	k1gFnSc7	knihovna
libwrap	libwrap	k1gInSc4	libwrap
umožňující	umožňující	k2eAgInSc4d1	umožňující
používat	používat	k5eAaImF	používat
TCP	TCP	kA	TCP
Wrapper	Wrapper	k1gInSc4	Wrapper
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc4	jeho
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
etc	etc	k?	etc
<g/>
/	/	kIx~	/
<g/>
hosts	hosts	k1gInSc1	hosts
<g/>
.	.	kIx.	.
<g/>
allow	allow	k?	allow
</s>
</p>
<p>
<s>
identd	identd	k1gMnSc1	identd
<g/>
,	,	kIx,	,
authd	authd	k1gMnSc1	authd
<g/>
:	:	kIx,	:
.	.	kIx.	.
<g/>
intranet	intranet	k1gInSc1	intranet
<g/>
.	.	kIx.	.
<g/>
lan	lano	k1gNnPc2	lano
<g/>
,	,	kIx,	,
mail	mail	k1gInSc1	mail
<g/>
.	.	kIx.	.
<g/>
isp	isp	k?	isp
<g/>
.	.	kIx.	.
<g/>
tld	tld	k?	tld
<g/>
,	,	kIx,	,
ssh	ssh	k?	ssh
<g/>
.	.	kIx.	.
<g/>
isp	isp	k?	isp
<g/>
.	.	kIx.	.
<g/>
tld	tld	k?	tld
<g/>
,	,	kIx,	,
irc	irc	k?	irc
<g/>
.	.	kIx.	.
<g/>
isp	isp	k?	isp
<g/>
.	.	kIx.	.
<g/>
tld	tld	k?	tld
<g/>
,	,	kIx,	,
ftp	ftp	k?	ftp
<g/>
.	.	kIx.	.
<g/>
isp	isp	k?	isp
<g/>
.	.	kIx.	.
<g/>
tld	tld	k?	tld
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Ident	Ident	k1gInSc1	Ident
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
fungování	fungování	k1gNnSc4	fungování
služby	služba	k1gFnSc2	služba
IRC	IRC	kA	IRC
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ident	identa	k1gFnPc2	identa
by	by	kYmCp3nP	by
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
blokovat	blokovat	k5eAaImF	blokovat
problémové	problémový	k2eAgMnPc4d1	problémový
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
jedině	jedině	k6eAd1	jedině
blokováním	blokování	k1gNnSc7	blokování
celé	celá	k1gFnSc2	celá
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nS	by
ale	ale	k9	ale
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
znemožněn	znemožnit	k5eAaPmNgInS	znemožnit
přístup	přístup	k1gInSc1	přístup
ostatním	ostatní	k2eAgMnPc3d1	ostatní
uživatelům	uživatel	k1gMnPc3	uživatel
používajícím	používající	k2eAgMnPc3d1	používající
stejný	stejný	k2eAgInSc4d1	stejný
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
IRC	IRC	kA	IRC
server	server	k1gInSc1	server
nedostane	dostat	k5eNaPmIp3nS	dostat
na	na	k7c4	na
ident	ident	k1gInSc4	ident
požadavek	požadavek	k1gInSc1	požadavek
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
zpravidla	zpravidla	k6eAd1	zpravidla
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
jméno	jméno	k1gNnSc4	jméno
poskytnuté	poskytnutý	k2eAgFnSc2d1	poskytnutá
IRC	IRC	kA	IRC
klientem	klient	k1gMnSc7	klient
<g/>
.	.	kIx.	.
</s>
<s>
IRC	IRC	kA	IRC
server	server	k1gInSc1	server
takového	takový	k3xDgMnSc4	takový
uživatele	uživatel	k1gMnSc4	uživatel
obvykle	obvykle	k6eAd1	obvykle
označí	označit	k5eAaPmIp3nS	označit
prefixem	prefix	k1gInSc7	prefix
"	"	kIx"	"
<g/>
~	~	kIx~	~
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tilda	tilda	k1gFnSc1	tilda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
indikuje	indikovat	k5eAaBmIp3nS	indikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
přezdívka	přezdívka	k1gFnSc1	přezdívka
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zfalšována	zfalšován	k2eAgFnSc1d1	zfalšována
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ostatní	ostatní	k2eAgInPc1d1	ostatní
servery	server	k1gInPc1	server
uživatele	uživatel	k1gMnSc2	uživatel
bez	bez	k7c2	bez
ident	identa	k1gFnPc2	identa
rovnou	rovnou	k6eAd1	rovnou
zablokují	zablokovat	k5eAaPmIp3nP	zablokovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Relay	Relaa	k1gFnSc2	Relaa
Chat	chata	k1gFnPc2	chata
(	(	kIx(	(
<g/>
IRC	IRC	kA	IRC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
File	File	k6eAd1	File
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
FTP	FTP	kA	FTP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Simple	Simple	k6eAd1	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
SMTP	SMTP	kA	SMTP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Network	network	k1gInSc1	network
News	News	k1gInSc1	News
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
NNTP	NNTP	kA	NNTP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Secure	Secur	k1gMnSc5	Secur
Shell	Shell	k1gMnSc1	Shell
(	(	kIx(	(
<g/>
SSH	SSH	kA	SSH
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
RFC	RFC	kA	RFC
912	[number]	k4	912
-	-	kIx~	-
Authentication	Authentication	k1gInSc1	Authentication
Service	Service	k1gFnSc2	Service
</s>
</p>
<p>
<s>
RFC	RFC	kA	RFC
931	[number]	k4	931
-	-	kIx~	-
Authentication	Authentication	k1gInSc1	Authentication
Server	server	k1gInSc1	server
</s>
</p>
<p>
<s>
RFC	RFC	kA	RFC
1413	[number]	k4	1413
-	-	kIx~	-
Identification	Identification	k1gInSc1	Identification
Protocol	Protocola	k1gFnPc2	Protocola
</s>
</p>
<p>
<s>
RFC	RFC	kA	RFC
1414	[number]	k4	1414
-	-	kIx~	-
Identification	Identification	k1gInSc1	Identification
MIB	MIB	kA	MIB
</s>
</p>
