<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
nebo	nebo	k8xC	nebo
Stará	starý	k2eAgFnSc1d1	stará
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
posvátné	posvátný	k2eAgFnPc4d1	posvátná
knihy	kniha	k1gFnPc4	kniha
(	(	kIx(	(
<g/>
tóra	tóra	k1gFnSc1	tóra
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
tanach	tanach	k1gInSc1	tanach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
křesťané	křesťan	k1gMnPc1	křesťan
převzali	převzít	k5eAaPmAgMnP	převzít
z	z	k7c2	z
judaismu	judaismus	k1gInSc2	judaismus
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
oběma	dva	k4xCgMnPc7	dva
náboženstvím	náboženství	k1gNnSc7	náboženství
společné	společný	k2eAgFnSc6d1	společná
<g/>
.	.	kIx.	.
</s>
