<p>
<s>
Kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
(	(	kIx(	(
<g/>
též	též	k9	též
pevninský	pevninský	k2eAgInSc1d1	pevninský
<g/>
)	)	kIx)	)
typ	typ	k1gInSc1	typ
podnebí	podnebí	k1gNnSc2	podnebí
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkými	velký	k2eAgInPc7d1	velký
denními	denní	k2eAgInPc7d1	denní
i	i	k8xC	i
ročními	roční	k2eAgInPc7d1	roční
rozdíly	rozdíl	k1gInPc7	rozdíl
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc7d1	malá
oblačností	oblačnost	k1gFnSc7	oblačnost
a	a	k8xC	a
nízkými	nízký	k2eAgInPc7d1	nízký
úhrny	úhrn	k1gInPc7	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
podnebí	podnebí	k1gNnSc2	podnebí
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
např.	např.	kA	např.
pro	pro	k7c4	pro
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
nebo	nebo	k8xC	nebo
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
státech	stát	k1gInPc6	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
také	také	k9	také
pro	pro	k7c4	pro
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
;	;	kIx,	;
nelze	lze	k6eNd1	lze
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
ovšem	ovšem	k9	ovšem
setkat	setkat	k5eAaPmF	setkat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nízkému	nízký	k2eAgInSc3d1	nízký
podílu	podíl	k1gInSc3	podíl
souvislé	souvislý	k2eAgFnSc2d1	souvislá
souše	souš	k1gFnSc2	souš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc1	teplota
a	a	k8xC	a
počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
kontinentálním	kontinentální	k2eAgNnSc7d1	kontinentální
podnebím	podnebí	k1gNnSc7	podnebí
mnohdy	mnohdy	k6eAd1	mnohdy
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
nezřídkakdy	nezřídkakda	k1gFnSc2	nezřídkakda
i	i	k8xC	i
vlny	vlna	k1gFnSc2	vlna
veder	vedro	k1gNnPc2	vedro
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
klesají	klesat	k5eAaImIp3nP	klesat
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
letní	letní	k2eAgFnSc1d1	letní
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
činí	činit	k5eAaImIp3nS	činit
během	během	k7c2	během
dne	den	k1gInSc2	den
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
klesá	klesat	k5eAaImIp3nS	klesat
k	k	k7c3	k
rozmezí	rozmezí	k1gNnSc3	rozmezí
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
hodnot	hodnota	k1gFnPc2	hodnota
-12	-12	k4	-12
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
-23	-23	k4	-23
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
<p>
<s>
Oceánické	oceánický	k2eAgNnSc1d1	oceánické
podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
