<s>
Který	který	k3yQgInSc1	který
typ	typ	k1gInSc1	typ
podnebí	podnebí	k1gNnSc2	podnebí
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkými	velký	k2eAgInPc7d1	velký
denními	denní	k2eAgInPc7d1	denní
i	i	k8xC	i
ročními	roční	k2eAgInPc7d1	roční
rozdíly	rozdíl	k1gInPc7	rozdíl
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc7d1	malá
oblačností	oblačnost	k1gFnSc7	oblačnost
a	a	k8xC	a
nízkými	nízký	k2eAgInPc7d1	nízký
úhrny	úhrn	k1gInPc7	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
?	?	kIx.	?
</s>
