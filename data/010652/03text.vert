<p>
<s>
Včela	včela	k1gFnSc1	včela
Apis	Apisa	k1gFnPc2	Apisa
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
blanokřídlého	blanokřídlý	k2eAgInSc2d1	blanokřídlý
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
projevem	projev	k1gInSc7	projev
rodu	rod	k1gInSc2	rod
včela	včela	k1gFnSc1	včela
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
díla	dílo	k1gNnSc2	dílo
z	z	k7c2	z
vosku	vosk	k1gInSc2	vosk
vyprodukovaného	vyprodukovaný	k2eAgInSc2d1	vyprodukovaný
vlastní	vlastní	k2eAgFnSc7d1	vlastní
voskovou	voskový	k2eAgFnSc7d1	vosková
žlázou	žláza	k1gFnSc7	žláza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zařazení	zařazení	k1gNnSc1	zařazení
rodu	rod	k1gInSc2	rod
včela	včela	k1gFnSc1	včela
==	==	k?	==
</s>
</p>
<p>
<s>
Nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
včely	včela	k1gFnSc2	včela
(	(	kIx(	(
<g/>
Apoidea	Apoidea	k1gFnSc1	Apoidea
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc4	sedm
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
šest	šest	k4xCc4	šest
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
samotářské	samotářský	k2eAgFnPc4d1	samotářská
včely	včela	k1gFnPc4	včela
druhově	druhově	k6eAd1	druhově
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
žije	žít	k5eAaImIp3nS	žít
609	[number]	k4	609
druhů	druh	k1gInPc2	druh
včel	včela	k1gFnPc2	včela
samotářek	samotářka	k1gFnPc2	samotářka
<g/>
.	.	kIx.	.
</s>
<s>
Sedmou	sedmý	k4xOgFnSc4	sedmý
čeleď	čeleď	k1gFnSc4	čeleď
představuje	představovat	k5eAaImIp3nS	představovat
čeleď	čeleď	k1gFnSc1	čeleď
včelovití	včelovitý	k2eAgMnPc1d1	včelovitý
(	(	kIx(	(
<g/>
Apidae	Apidae	k1gNnSc7	Apidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
patří	patřit	k5eAaImIp3nS	patřit
včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
<g/>
,	,	kIx,	,
čmeláci	čmelák	k1gMnPc1	čmelák
a	a	k8xC	a
tropické	tropický	k2eAgFnPc1d1	tropická
bezžihadlé	bezžihadlý	k2eAgFnPc1d1	bezžihadlý
včely	včela	k1gFnPc1	včela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
sociální	sociální	k2eAgInSc4d1	sociální
hmyz	hmyz	k1gInSc4	hmyz
tvořící	tvořící	k2eAgFnSc1d1	tvořící
trvalá	trvalá	k1gFnSc1	trvalá
společenstva	společenstvo	k1gNnSc2	společenstvo
–	–	k?	–
včelstva	včelstvo	k1gNnSc2	včelstvo
</s>
</p>
<p>
<s>
staví	stavit	k5eAaBmIp3nS	stavit
voskové	voskový	k2eAgNnSc1d1	voskové
dílo	dílo	k1gNnSc1	dílo
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
vosku	vosk	k1gInSc2	vosk
</s>
</p>
<p>
<s>
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
zásoby	zásoba	k1gFnPc4	zásoba
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
vosku	vosk	k1gInSc2	vosk
a	a	k8xC	a
pylu	pyl	k1gInSc2	pyl
</s>
</p>
<p>
<s>
rozhodující	rozhodující	k2eAgMnSc1d1	rozhodující
opylovač	opylovač	k1gMnSc1	opylovač
hmyzosnubných	hmyzosnubný	k2eAgFnPc2d1	hmyzosnubná
rostlin	rostlina	k1gFnPc2	rostlina
</s>
</p>
<p>
<s>
rozvinuta	rozvinut	k2eAgFnSc1d1	rozvinuta
schopnost	schopnost	k1gFnSc1	schopnost
předat	předat	k5eAaPmF	předat
informaci	informace	k1gFnSc4	informace
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
zdroje	zdroj	k1gInSc2	zdroj
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
včelí	včelí	k2eAgInSc4d1	včelí
tanec	tanec	k1gInSc4	tanec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Podrody	podrod	k1gInPc4	podrod
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Apis	Apis	k1gInSc1	Apis
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
podrodů	podrod	k1gInPc2	podrod
</s>
</p>
<p>
<s>
Apis	Apis	k6eAd1	Apis
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
sensu	sens	k1gInSc2	sens
stricto	stricto	k1gNnSc4	stricto
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
několika	několik	k4yIc2	několik
svislých	svislý	k2eAgMnPc2d1	svislý
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
uložených	uložený	k2eAgInPc2d1	uložený
plástů	plást	k1gInPc2	plást
v	v	k7c6	v
temné	temný	k2eAgFnSc6d1	temná
dutině	dutina	k1gFnSc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
včely	včela	k1gFnPc1	včela
schopné	schopný	k2eAgFnPc1d1	schopná
pomocí	pomocí	k7c2	pomocí
svých	svůj	k3xOyFgInPc2	svůj
tanců	tanec	k1gInPc2	tanec
na	na	k7c6	na
svislých	svislý	k2eAgInPc6d1	svislý
plástech	plást	k1gInPc6	plást
předat	předat	k5eAaPmF	předat
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
zdroji	zdroj	k1gInSc6	zdroj
snůšky	snůška	k1gFnSc2	snůška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Megapis	Megapis	k1gInSc1	Megapis
Ashmead	Ashmead	k1gInSc1	Ashmead
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
velkých	velký	k2eAgFnPc2d1	velká
včel	včela	k1gFnPc2	včela
(	(	kIx(	(
<g/>
dělnice	dělnice	k1gFnPc1	dělnice
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
staví	stavit	k5eAaImIp3nS	stavit
výhradně	výhradně	k6eAd1	výhradně
jediný	jediný	k2eAgInSc1d1	jediný
svislý	svislý	k2eAgInSc1d1	svislý
plást	plást	k1gInSc1	plást
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
<g/>
2	[number]	k4	2
volně	volně	k6eAd1	volně
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
tančící	tančící	k2eAgFnPc1d1	tančící
na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
či	či	k8xC	či
svislé	svislý	k2eAgFnSc6d1	svislá
ploše	plocha	k1gFnSc6	plocha
plástu	plást	k1gInSc2	plást
musejí	muset	k5eAaImIp3nP	muset
vidět	vidět	k5eAaImF	vidět
slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Micrapis	Micrapis	k1gInSc1	Micrapis
Ashmead	Ashmead	k1gInSc1	Ashmead
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
–	–	k?	–
drobné	drobný	k2eAgFnSc2d1	drobná
včely	včela	k1gFnSc2	včela
(	(	kIx(	(
<g/>
dělnice	dělnice	k1gFnSc1	dělnice
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
mm	mm	kA	mm
<g/>
)	)	kIx)	)
stavějí	stavět	k5eAaImIp3nP	stavět
jediný	jediný	k2eAgInSc4d1	jediný
plást	plást	k1gInSc4	plást
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
prostoru	prostor	k1gInSc6	prostor
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
do	do	k7c2	do
0,4	[number]	k4	0,4
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
dělnice	dělnice	k1gFnPc1	dělnice
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
předat	předat	k5eAaPmF	předat
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
snůšce	snůška	k1gFnSc6	snůška
při	při	k7c6	při
tanci	tanec	k1gInSc6	tanec
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
ploše	plocha	k1gFnSc6	plocha
plástu	plást	k1gInSc2	plást
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
a	a	k8xC	a
poddruhy	poddruh	k1gInPc1	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
systematizaci	systematizace	k1gFnSc6	systematizace
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
včela	včela	k1gFnSc1	včela
dochází	docházet	k5eAaImIp3nS	docházet
stále	stále	k6eAd1	stále
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
objevům	objev	k1gInPc3	objev
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
je	být	k5eAaImIp3nS	být
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
orientaci	orientace	k1gFnSc3	orientace
především	především	k9	především
v	v	k7c6	v
platném	platný	k2eAgNnSc6d1	platné
českém	český	k2eAgNnSc6d1	české
názvosloví	názvosloví	k1gNnSc6	názvosloví
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
včela	včela	k1gFnSc1	včela
indická	indický	k2eAgFnSc1d1	indická
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
užívaný	užívaný	k2eAgInSc1d1	užívaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesprávný	správný	k2eNgInSc1d1	nesprávný
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
pro	pro	k7c4	pro
druh	druh	k1gInSc4	druh
včela	včela	k1gFnSc1	včela
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
poddruh	poddruh	k1gInSc1	poddruh
<g/>
)	)	kIx)	)
indická	indický	k2eAgFnSc1d1	indická
(	(	kIx(	(
<g/>
Apis	Apis	k1gInSc1	Apis
cerana	ceran	k1gMnSc2	ceran
indica	indicus	k1gMnSc2	indicus
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Sloupec	sloupec	k1gInSc1	sloupec
"	"	kIx"	"
<g/>
synonyma	synonymum	k1gNnSc2	synonymum
<g/>
"	"	kIx"	"
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
označuje	označovat	k5eAaImIp3nS	označovat
latinské	latinský	k2eAgInPc4d1	latinský
ekvivalenty	ekvivalent	k1gInPc4	ekvivalent
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nevyhovují	vyhovovat	k5eNaImIp3nP	vyhovovat
</s>
</p>
<p>
<s>
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
pravidlům	pravidlo	k1gNnPc3	pravidlo
zoologické	zoologický	k2eAgFnSc2d1	zoologická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
se	se	k3xPyFc4	se
už	už	k6eAd1	už
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ING.	ing.	kA	ing.
PŘIDAL	přidat	k5eAaPmAgInS	přidat
<g/>
,	,	kIx,	,
PH	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
naše	náš	k3xOp1gFnSc1	náš
včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
...	...	k?	...
<g/>
.	.	kIx.	.
</s>
<s>
Včelařství	včelařství	k1gNnSc1	včelařství
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
ČSV	ČSV	kA	ČSV
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
57	[number]	k4	57
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
2924	[number]	k4	2924
<g/>
.	.	kIx.	.
</s>
<s>
ING.	ing.	kA	ing.
PŘIDAL	přidat	k5eAaPmAgInS	přidat
<g/>
,	,	kIx,	,
PH	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Dol	dol	k1gInSc1	dol
<g/>
:	:	kIx,	:
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
včelařský	včelařský	k2eAgInSc1d1	včelařský
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87196	[number]	k4	87196
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Podrody	podrod	k1gInPc1	podrod
rodu	rod	k1gInSc2	rod
Apis	Apis	k1gInSc1	Apis
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
bionomie	bionomie	k1gFnSc1	bionomie
<g/>
,	,	kIx,	,
s.	s.	k?	s.
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Eusocialita	Eusocialita	k1gFnSc1	Eusocialita
</s>
</p>
<p>
<s>
Včelařství	včelařství	k1gNnSc1	včelařství
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
včela	včela	k1gFnSc1	včela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Včela	včela	k1gFnSc1	včela
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
včela	včela	k1gFnSc1	včela
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Apis	Apisa	k1gFnPc2	Apisa
ve	v	k7c4	v
WikidruzíchVýzkumný	WikidruzíchVýzkumný	k2eAgInSc4d1	WikidruzíchVýzkumný
ústav	ústav	k1gInSc4	ústav
včelařský	včelařský	k2eAgInSc4d1	včelařský
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Dol	dol	k1gInSc1	dol
</s>
</p>
<p>
<s>
Pokusný	pokusný	k2eAgInSc1d1	pokusný
včelín	včelín	k1gInSc1	včelín
Zubří	zubří	k2eAgFnSc2d1	zubří
</s>
</p>
<p>
<s>
vcelar	vcelar	k1gInSc1	vcelar
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
