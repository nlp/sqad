<s>
Felix	Felix	k1gMnSc1	Felix
Bloch	Bloch	k1gMnSc1	Bloch
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Curych	Curych	k1gInSc1	Curych
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
nových	nový	k2eAgFnPc2d1	nová
metod	metoda	k1gFnPc2	metoda
pro	pro	k7c4	pro
přesná	přesný	k2eAgNnPc4d1	přesné
měření	měření	k1gNnSc4	měření
jaderného	jaderný	k2eAgInSc2d1	jaderný
magnetismu	magnetismus	k1gInSc2	magnetismus
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
objevy	objev	k1gInPc4	objev
společně	společně	k6eAd1	společně
s	s	k7c7	s
Edwardem	Edward	k1gMnSc7	Edward
Millsem	Mills	k1gMnSc7	Mills
Purcellem	Purcell	k1gMnSc7	Purcell
<g/>
.	.	kIx.	.
</s>
