<s>
Sylvín	Sylvín	k1gMnSc1	Sylvín
(	(	kIx(	(
<g/>
Beudant	Beudant	k1gMnSc1	Beudant
<g/>
,	,	kIx,	,
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
vzorec	vzorec	k1gInSc4	vzorec
KCl	KCl	k1gFnSc2	KCl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
krychlový	krychlový	k2eAgInSc4d1	krychlový
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Sylvín	sylvín	k1gInSc1	sylvín
vzniká	vznikat	k5eAaImIp3nS	vznikat
vysrážením	vysrážení	k1gNnSc7	vysrážení
v	v	k7c6	v
solných	solný	k2eAgInPc6d1	solný
jezerech	jezero	k1gNnPc6	jezero
nebo	nebo	k8xC	nebo
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
podrobněji	podrobně	k6eAd2	podrobně
viz	vidět	k5eAaImRp2nS	vidět
halit	halit	k1gInSc1	halit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
ze	z	k7c2	z
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
řídký	řídký	k2eAgInSc1d1	řídký
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
zrnité	zrnitý	k2eAgInPc1d1	zrnitý
nebo	nebo	k8xC	nebo
vláknité	vláknitý	k2eAgInPc1d1	vláknitý
agregáty	agregát	k1gInPc1	agregát
<g/>
,	,	kIx,	,
kůry	kůra	k1gFnPc1	kůra
<g/>
,	,	kIx,	,
krystaly	krystal	k1gInPc1	krystal
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
hexaedru	hexaedr	k1gInSc2	hexaedr
nebo	nebo	k8xC	nebo
oktoedru	oktoedr	k1gInSc2	oktoedr
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Lze	lze	k6eAd1	lze
rýpat	rýpat	k5eAaImF	rýpat
nehtem	nehet	k1gInSc7	nehet
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc1	tvrdost
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
2,0	[number]	k4	2,0
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
štěpný	štěpný	k2eAgMnSc1d1	štěpný
podle	podle	k7c2	podle
krychle	krychle	k1gFnSc2	krychle
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
nažloutlá	nažloutlý	k2eAgNnPc1d1	nažloutlé
<g/>
,	,	kIx,	,
načervenalá	načervenalý	k2eAgNnPc1d1	načervenalé
<g/>
,	,	kIx,	,
šedá	šedý	k2eAgNnPc1d1	šedé
<g/>
,	,	kIx,	,
namodralá	namodralý	k2eAgNnPc1d1	namodralé
<g/>
.	.	kIx.	.
</s>
<s>
Průhledný	průhledný	k2eAgInSc1d1	průhledný
až	až	k8xS	až
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
K	k	k7c3	k
52,44	[number]	k4	52,44
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
47,56	[number]	k4	47,56
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
příměsi	příměs	k1gFnPc4	příměs
I	I	kA	I
<g/>
,	,	kIx,	,
Na	na	k7c6	na
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hygroskopický	hygroskopický	k2eAgMnSc1d1	hygroskopický
(	(	kIx(	(
<g/>
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plamen	plamen	k1gInSc1	plamen
barví	barvit	k5eAaImIp3nS	barvit
fialově	fialově	k6eAd1	fialově
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Hořkoslaná	hořkoslaný	k2eAgFnSc1d1	hořkoslaná
chuť	chuť	k1gFnSc1	chuť
<g/>
.	.	kIx.	.
halit	halit	k1gInSc1	halit
<g/>
,	,	kIx,	,
carnallit	carnallit	k1gInSc1	carnallit
Doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
minerál	minerál	k1gInSc1	minerál
v	v	k7c6	v
ložiskách	ložisko	k1gNnPc6	ložisko
halitu	halit	k1gInSc2	halit
<g/>
.	.	kIx.	.
</s>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
draslíku	draslík	k1gInSc2	draslík
<g/>
.	.	kIx.	.
</s>
<s>
SRN	SRN	kA	SRN
–	–	k?	–
Hannover	Hannover	k1gInSc1	Hannover
<g/>
,	,	kIx,	,
Stassfurt	Stassfurt	k1gInSc1	Stassfurt
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
Kaluž	kaluž	k1gFnSc1	kaluž
USA	USA	kA	USA
Kanada	Kanada	k1gFnSc1	Kanada
Itálie	Itálie	k1gFnSc2	Itálie
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
mineralní	mineralný	k2eAgMnPc1d1	mineralný
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
lazních	lazeň	k1gFnPc6	lazeň
Skalka	Skalka	k1gMnSc1	Skalka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sylvín	sylvín	k1gInSc4	sylvín
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Sylvín	Sylvín	k1gMnSc1	Sylvín
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sylvín	Sylvín	k1gMnSc1	Sylvín
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaImF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
