<s>
Juliánský	juliánský	k2eAgInSc1d1	juliánský
kalendář	kalendář	k1gInSc1	kalendář
je	být	k5eAaImIp3nS	být
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
46	[number]	k4	46
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zavedl	zavést	k5eAaPmAgMnS	zavést
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kalendář	kalendář	k1gInSc1	kalendář
byl	být	k5eAaImAgInS	být
reformou	reforma	k1gFnSc7	reforma
starořímského	starořímský	k2eAgInSc2d1	starořímský
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xC	jako
sluneční	sluneční	k2eAgInSc1d1	sluneční
kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Drobná	drobný	k2eAgFnSc1d1	drobná
úprava	úprava	k1gFnSc1	úprava
kalendáře	kalendář	k1gInSc2	kalendář
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
počtu	počet	k1gInSc2	počet
dnů	den	k1gInPc2	den
některých	některý	k3yIgInPc2	některý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
naší	náš	k3xOp1gFnSc2	náš
<g/>
)	)	kIx)	)
éry	éra	k1gFnSc2	éra
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
rok	rok	k1gInSc4	rok
1	[number]	k4	1
stanoven	stanovit	k5eAaPmNgInS	stanovit
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
předpokládaného	předpokládaný	k2eAgNnSc2d1	předpokládané
narození	narození	k1gNnSc2	narození
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravený	upravený	k2eAgInSc4d1	upravený
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
(	(	kIx(	(
<g/>
s	s	k7c7	s
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
)	)	kIx)	)
platil	platit	k5eAaImAgInS	platit
až	až	k6eAd1	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gNnSc4	on
upravila	upravit	k5eAaPmAgFnS	upravit
reforma	reforma	k1gFnSc1	reforma
papeže	papež	k1gMnSc2	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Starořímský	starořímský	k2eAgInSc4d1	starořímský
kalendář	kalendář	k1gInSc4	kalendář
v	v	k7c6	v
době	doba	k1gFnSc6	doba
republiky	republika	k1gFnSc2	republika
měl	mít	k5eAaImAgMnS	mít
dvanáct	dvanáct	k4xCc1	dvanáct
měsíců	měsíc	k1gInPc2	měsíc
tvořících	tvořící	k2eAgInPc2d1	tvořící
dohromady	dohromady	k6eAd1	dohromady
355	[number]	k4	355
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
s	s	k7c7	s
tropickým	tropický	k2eAgInSc7d1	tropický
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
vkládal	vkládat	k5eAaImAgInS	vkládat
pomocný	pomocný	k2eAgInSc1d1	pomocný
měsíc	měsíc	k1gInSc1	měsíc
Mercedonius	Mercedonius	k1gInSc1	Mercedonius
o	o	k7c6	o
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
"	"	kIx"	"
měsíce	měsíc	k1gInPc1	měsíc
Februaria	Februarium	k1gNnSc2	Februarium
–	–	k?	–
5	[number]	k4	5
dnů	den	k1gInPc2	den
před	před	k7c4	před
jeho	on	k3xPp3gInSc4	on
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Neexistovalo	existovat	k5eNaImAgNnS	existovat
pravidlo	pravidlo	k1gNnSc1	pravidlo
pro	pro	k7c4	pro
vkládání	vkládání	k1gNnSc4	vkládání
Mercedonia	Mercedonium	k1gNnSc2	Mercedonium
ani	ani	k8xC	ani
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
stanovovali	stanovovat	k5eAaImAgMnP	stanovovat
je	on	k3xPp3gNnSc4	on
určení	určený	k2eAgMnPc1d1	určený
kněží	kněz	k1gMnPc1	kněz
(	(	kIx(	(
<g/>
pontifikové	pontifex	k1gMnPc1	pontifex
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
uvážení	uvážení	k1gNnSc2	uvážení
a	a	k8xC	a
libovůle	libovůle	k1gFnSc2	libovůle
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
republiky	republika	k1gFnSc2	republika
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vynechání	vynechání	k1gNnSc3	vynechání
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
tropickým	tropický	k2eAgMnSc7d1	tropický
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
využil	využít	k5eAaPmAgMnS	využít
znalostí	znalost	k1gFnSc7	znalost
alexandrijských	alexandrijský	k2eAgMnPc2d1	alexandrijský
astronomů	astronom	k1gMnPc2	astronom
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
určili	určit	k5eAaPmAgMnP	určit
délku	délka	k1gFnSc4	délka
roku	rok	k1gInSc2	rok
na	na	k7c4	na
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
a	a	k8xC	a
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
i	i	k9	i
schéma	schéma	k1gNnSc4	schéma
vyrovnávání	vyrovnávání	k1gNnSc2	vyrovnávání
kalendáře	kalendář	k1gInSc2	kalendář
–	–	k?	–
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
rok	rok	k1gInSc1	rok
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
365	[number]	k4	365
dnů	den	k1gInPc2	den
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
4	[number]	k4	4
<g/>
.	.	kIx.	.
rok	rok	k1gInSc1	rok
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
o	o	k7c6	o
1	[number]	k4	1
den	den	k1gInSc1	den
delší	dlouhý	k2eAgInSc1d2	delší
(	(	kIx(	(
<g/>
přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
respektoval	respektovat	k5eAaImAgMnS	respektovat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
římských	římský	k2eAgFnPc2d1	římská
zvyklostí	zvyklost	k1gFnPc2	zvyklost
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
dní	den	k1gInPc2	den
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
vlastně	vlastně	k9	vlastně
přešly	přejít	k5eAaPmAgInP	přejít
i	i	k9	i
do	do	k7c2	do
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
jako	jako	k8xS	jako
Kánobský	Kánobský	k2eAgInSc4d1	Kánobský
kalendář	kalendář	k1gInSc4	kalendář
nebo	nebo	k8xC	nebo
kalendář	kalendář	k1gInSc4	kalendář
Kánobského	Kánobský	k2eAgInSc2d1	Kánobský
dekretu	dekret	k1gInSc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrijský	alexandrijský	k2eAgMnSc1d1	alexandrijský
učenec	učenec	k1gMnSc1	učenec
Sósigenes	Sósigenes	k1gMnSc1	Sósigenes
z	z	k7c2	z
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
pak	pak	k6eAd1	pak
provedl	provést	k5eAaPmAgInS	provést
syntézu	syntéza	k1gFnSc4	syntéza
římského	římský	k2eAgMnSc2d1	římský
a	a	k8xC	a
alexandrijského	alexandrijský	k2eAgInSc2d1	alexandrijský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
měsíců	měsíc	k1gInPc2	měsíc
zůstala	zůstat	k5eAaPmAgNnP	zůstat
stejná	stejné	k1gNnPc1	stejné
<g/>
,	,	kIx,	,
délky	délka	k1gFnPc1	délka
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgFnP	změnit
<g/>
,	,	kIx,	,
Mercednius	Mercednius	k1gInSc4	Mercednius
byl	být	k5eAaImAgInS	být
vypuštěn	vypuštěn	k2eAgInSc1d1	vypuštěn
zcela	zcela	k6eAd1	zcela
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
Februariu	Februarium	k1gNnSc6	Februarium
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vkládán	vkládat	k5eAaImNgInS	vkládat
přestupný	přestupný	k2eAgInSc1d1	přestupný
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
římského	římský	k2eAgNnSc2d1	římské
dopředného	dopředný	k2eAgNnSc2d1	dopředné
počítání	počítání	k1gNnSc2	počítání
dnů	den	k1gInPc2	den
následoval	následovat	k5eAaImAgInS	následovat
po	po	k7c6	po
6	[number]	k4	6
<g/>
.	.	kIx.	.
dnu	dna	k1gFnSc4	dna
před	před	k7c7	před
březnovými	březnový	k2eAgFnPc7d1	březnová
Kalendami	kalendy	k1gFnPc7	kalendy
–	–	k?	–
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
nazvali	nazvat	k5eAaPmAgMnP	nazvat
dnem	den	k1gInSc7	den
dvojšestým	dvojšestý	k2eAgInSc7d1	dvojšestý
(	(	kIx(	(
<g/>
dies	dies	k1gInSc1	dies
bissextus	bissextus	k1gMnSc1	bissextus
Cal	Cal	k1gMnSc1	Cal
<g/>
.	.	kIx.	.
</s>
<s>
Martiae	Martiae	k1gInSc1	Martiae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
označení	označení	k1gNnSc1	označení
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
visokos	visokos	k1gMnSc1	visokos
a	a	k8xC	a
visokosnyj	visokosnyj	k1gMnSc1	visokosnyj
god	god	k?	god
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
bissextile	bissextile	k6eAd1	bissextile
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
tohoto	tento	k3xDgInSc2	tento
kalendáře	kalendář	k1gInSc2	kalendář
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tropický	tropický	k2eAgInSc1d1	tropický
rok	rok	k1gInSc1	rok
netrvá	trvat	k5eNaImIp3nS	trvat
přesně	přesně	k6eAd1	přesně
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
365,242	[number]	k4	365,242
<g/>
20	[number]	k4	20
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nepřesnost	nepřesnost	k1gFnSc4	nepřesnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
juliánský	juliánský	k2eAgInSc1d1	juliánský
kalendář	kalendář	k1gInSc1	kalendář
oproti	oproti	k7c3	oproti
realitě	realita	k1gFnSc3	realita
opozdil	opozdit	k5eAaPmAgInS	opozdit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
za	za	k7c4	za
133-134	[number]	k4	133-134
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
opoždění	opoždění	k1gNnSc1	opoždění
výrazně	výrazně	k6eAd1	výrazně
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
především	především	k9	především
na	na	k7c6	na
dnech	den	k1gInPc6	den
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1582	[number]	k4	1582
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
bulou	bula	k1gFnSc7	bula
Inter	Intra	k1gFnPc2	Intra
gravissimas	gravissimas	k1gInSc4	gravissimas
reformu	reforma	k1gFnSc4	reforma
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
reformovaný	reformovaný	k2eAgInSc4d1	reformovaný
kalendář	kalendář	k1gInSc4	kalendář
získal	získat	k5eAaPmAgInS	získat
název	název	k1gInSc1	název
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
využil	využít	k5eAaPmAgMnS	využít
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
funkcí	funkce	k1gFnPc2	funkce
<g/>
)	)	kIx)	)
i	i	k8xC	i
Pontifikem	pontifex	k1gMnSc7	pontifex
maximem	maxim	k1gInSc7	maxim
-	-	kIx~	-
neboli	neboli	k8xC	neboli
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
knězem	kněz	k1gMnSc7	kněz
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
určovat	určovat	k5eAaImF	určovat
délku	délka	k1gFnSc4	délka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
prodloužit	prodloužit	k5eAaPmF	prodloužit
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
starého	starý	k2eAgInSc2d1	starý
kalendáře	kalendář	k1gInSc2	kalendář
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
počátek	počátek	k1gInSc4	počátek
nového	nový	k2eAgInSc2d1	nový
kalendáře	kalendář	k1gInSc2	kalendář
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
předchozím	předchozí	k2eAgFnPc3d1	předchozí
zvyklostem	zvyklost	k1gFnPc3	zvyklost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začátek	začátek	k1gInSc1	začátek
roku	rok	k1gInSc2	rok
přesunul	přesunout	k5eAaPmAgInS	přesunout
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
708	[number]	k4	708
A.U.C.	A.U.C.	k1gFnPc2	A.U.C.
(	(	kIx(	(
<g/>
46	[number]	k4	46
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
prodloužen	prodloužit	k5eAaPmNgInS	prodloužit
o	o	k7c4	o
85	[number]	k4	85
dní	den	k1gInPc2	den
na	na	k7c4	na
445	[number]	k4	445
dní	den	k1gInPc2	den
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
nazýván	nazývat	k5eAaImNgInS	nazývat
rokem	rok	k1gInSc7	rok
zmatků	zmatek	k1gInPc2	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
zmatky	zmatek	k1gInPc1	zmatek
neskončily	skončit	k5eNaPmAgInP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
starat	starat	k5eAaImF	starat
o	o	k7c4	o
počítání	počítání	k1gNnSc4	počítání
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
nepochopili	pochopit	k5eNaPmAgMnP	pochopit
pravidla	pravidlo	k1gNnPc4	pravidlo
juliánské	juliánský	k2eAgFnPc4d1	juliánská
reformy	reforma	k1gFnPc4	reforma
a	a	k8xC	a
vkládali	vkládat	k5eAaImAgMnP	vkládat
přestupný	přestupný	k2eAgInSc4d1	přestupný
den	den	k1gInSc4	den
ne	ne	k9	ne
každý	každý	k3xTgInSc1	každý
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
každý	každý	k3xTgInSc4	každý
třetí	třetí	k4xOgInSc4	třetí
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
Macrobia	Macrobium	k1gNnSc2	Macrobium
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
inkluzivním	inkluzivní	k2eAgNnSc7d1	inkluzivní
počítáním	počítání	k1gNnSc7	počítání
přestupných	přestupný	k2eAgNnPc2d1	přestupné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
počítali	počítat	k5eAaImAgMnP	počítat
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
čtyřletého	čtyřletý	k2eAgInSc2d1	čtyřletý
cyklu	cyklus	k1gInSc2	cyklus
zároveň	zároveň	k6eAd1	zároveň
za	za	k7c4	za
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
dalšího	další	k2eAgInSc2d1	další
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	Tom	k1gMnSc3	Tom
učinil	učinit	k5eAaPmAgMnS	učinit
přítrž	přítrž	k1gFnSc4	přítrž
císař	císař	k1gMnSc1	císař
Augustus	Augustus	k1gMnSc1	Augustus
roku	rok	k1gInSc2	rok
8	[number]	k4	8
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nastřádaná	nastřádaný	k2eAgFnSc1d1	nastřádaná
nepřesnost	nepřesnost	k1gFnSc1	nepřesnost
odstraní	odstranit	k5eAaPmIp3nS	odstranit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
8	[number]	k4	8
n.	n.	k?	n.
l.	l.	k?	l.
nebudou	být	k5eNaImBp3nP	být
přestupné	přestupný	k2eAgInPc1d1	přestupný
roky	rok	k1gInPc1	rok
zařazovat	zařazovat	k5eAaImF	zařazovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
dále	daleko	k6eAd2	daleko
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
menší	malý	k2eAgFnSc1d2	menší
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
názvech	název	k1gInPc6	název
délkách	délka	k1gFnPc6	délka
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
porušen	porušen	k2eAgInSc4d1	porušen
původní	původní	k2eAgInSc4d1	původní
princip	princip	k1gInSc4	princip
střídání	střídání	k1gNnSc2	střídání
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
31	[number]	k4	31
a	a	k8xC	a
30	[number]	k4	30
dnech	den	k1gInPc6	den
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
v	v	k7c6	v
nepřestupných	přestupný	k2eNgInPc6d1	nepřestupný
letech	let	k1gInPc6	let
29	[number]	k4	29
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc1	měsíc
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
Caesarovi	Caesar	k1gMnSc3	Caesar
Julius	Julius	k1gMnSc1	Julius
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc4	měsíc
pak	pak	k6eAd1	pak
po	po	k7c6	po
jeho	jeho	k3xOp3gMnSc6	jeho
nástupci	nástupce	k1gMnSc6	nástupce
Augustovi	August	k1gMnSc6	August
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
nový	nový	k2eAgInSc1d1	nový
císařský	císařský	k2eAgInSc1d1	císařský
měsíc	měsíc	k1gInSc1	měsíc
nepatřil	patřit	k5eNaImAgInS	patřit
mezi	mezi	k7c7	mezi
kratší	krátký	k2eAgFnSc7d2	kratší
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prodloužen	prodloužit	k5eAaPmNgMnS	prodloužit
ze	z	k7c2	z
30	[number]	k4	30
na	na	k7c4	na
31	[number]	k4	31
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
4	[number]	k4	4
měsícům	měsíc	k1gInPc3	měsíc
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
počet	počet	k1gInSc1	počet
dnů	den	k1gInPc2	den
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
střídala	střídat	k5eAaImAgFnS	střídat
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
narostla	narůst	k5eAaPmAgFnS	narůst
délka	délka	k1gFnSc1	délka
druhého	druhý	k4xOgNnSc2	druhý
pololetí	pololetí	k1gNnSc2	pololetí
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Chybějící	chybějící	k2eAgInSc1d1	chybějící
den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
odebrán	odebrat	k5eAaPmNgInS	odebrat
únoru	únor	k1gInSc3	únor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
panování	panování	k1gNnSc2	panování
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
zůstal	zůstat	k5eAaPmAgInS	zůstat
kalendář	kalendář	k1gInSc1	kalendář
prakticky	prakticky	k6eAd1	prakticky
nezměněn	změnit	k5eNaPmNgInS	změnit
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
postupný	postupný	k2eAgInSc1d1	postupný
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1582	[number]	k4	1582
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
používat	používat	k5eAaImF	používat
kalendář	kalendář	k1gInSc4	kalendář
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
opuštění	opuštění	k1gNnSc3	opuštění
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
římskokatolických	římskokatolický	k2eAgFnPc6d1	Římskokatolická
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
nekatolické	katolický	k2eNgFnPc1d1	nekatolická
země	zem	k1gFnPc1	zem
sice	sice	k8xC	sice
přijímaly	přijímat	k5eAaImAgFnP	přijímat
nový	nový	k2eAgInSc4d1	nový
kalendář	kalendář	k1gInSc4	kalendář
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
ke	k	k7c3	k
katolickému	katolický	k2eAgNnSc3d1	katolické
papežství	papežství	k1gNnSc3	papežství
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
prosadil	prosadit	k5eAaPmAgMnS	prosadit
i	i	k9	i
zde	zde	k6eAd1	zde
(	(	kIx(	(
<g/>
Čechy	Čechy	k1gFnPc1	Čechy
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1584	[number]	k4	1584
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc1	Slezsko
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1584	[number]	k4	1584
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
odpor	odpor	k1gInSc4	odpor
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
zrušení	zrušení	k1gNnSc1	zrušení
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
u	u	k7c2	u
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1583	[number]	k4	1583
vydala	vydat	k5eAaPmAgFnS	vydat
Dokument	dokument	k1gInSc1	dokument
Sigillion	Sigillion	k1gInSc4	Sigillion
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslavné	pravoslavný	k2eAgFnPc1d1	pravoslavná
země	zem	k1gFnPc1	zem
tedy	tedy	k9	tedy
používaly	používat	k5eAaImAgFnP	používat
ještě	ještě	k9	ještě
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
od	od	k7c2	od
jeho	jeho	k3xOp3gNnPc2	jeho
využívání	využívání	k1gNnPc2	využívání
upustily	upustit	k5eAaPmAgFnP	upustit
až	až	k6eAd1	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rusko	Rusko	k1gNnSc1	Rusko
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
juliánský	juliánský	k2eAgInSc1d1	juliánský
kalendář	kalendář	k1gInSc1	kalendář
používal	používat	k5eAaImAgInS	používat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Řecko	Řecko	k1gNnSc1	Řecko
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
využíván	využíván	k2eAgInSc4d1	využíván
jako	jako	k8xC	jako
církevní	církevní	k2eAgInSc4d1	církevní
kalendář	kalendář	k1gInSc4	kalendář
pravoslavných	pravoslavný	k2eAgFnPc2d1	pravoslavná
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
má	mít	k5eAaImIp3nS	mít
přísné	přísný	k2eAgNnSc1d1	přísné
řazení	řazení	k1gNnSc1	řazení
obyčejných	obyčejný	k2eAgNnPc2d1	obyčejné
a	a	k8xC	a
přestupných	přestupný	k2eAgNnPc2d1	přestupné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jednak	jednak	k8xC	jednak
jeho	jeho	k3xOp3gFnSc4	jeho
nepřesnost	nepřesnost	k1gFnSc4	nepřesnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jistým	jistý	k2eAgFnPc3d1	jistá
zákonitostem	zákonitost	k1gFnPc3	zákonitost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pravidelnosti	pravidelnost	k1gFnSc3	pravidelnost
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
přestupných	přestupný	k2eAgNnPc2d1	přestupné
let	léto	k1gNnPc2	léto
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
všech	všecek	k3xTgNnPc2	všecek
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
osmi	osm	k4xCc2	osm
letech	léto	k1gNnPc6	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opakování	opakování	k1gNnSc3	opakování
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
a	a	k8xC	a
datem	datum	k1gNnSc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
po	po	k7c6	po
devatenácti	devatenáct	k4xCc2	devatenáct
letech	léto	k1gNnPc6	léto
opakuje	opakovat	k5eAaImIp3nS	opakovat
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
dny	den	k1gInPc7	den
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
lunárním	lunární	k2eAgInSc7d1	lunární
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
náboženského	náboženský	k2eAgNnSc2d1	náboženské
hlediska	hledisko	k1gNnSc2	hledisko
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
stávají	stávat	k5eAaImIp3nP	stávat
periodickými	periodický	k2eAgFnPc7d1	periodická
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
532	[number]	k4	532
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
ze	z	k7c2	z
stati	stať	k1gFnSc2	stať
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Rafaila	Rafaila	k1gFnSc1	Rafaila
Karelina	Karelina	k1gFnSc1	Karelina
a	a	k8xC	a
A.	A.	kA	A.
G.	G.	kA	G.
Čchartišvili	Čchartišvili	k1gMnSc2	Čchartišvili
<g/>
,	,	kIx,	,
doktora	doktor	k1gMnSc2	doktor
matematicko-fyzikálních	matematickoyzikální	k2eAgFnPc2d1	matematicko-fyzikální
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
učitele	učitel	k1gMnPc4	učitel
na	na	k7c6	na
Matematické	matematický	k2eAgFnSc6d1	matematická
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
;	;	kIx,	;
sborník	sborník	k1gInSc1	sborník
Kalendářní	kalendářní	k2eAgFnSc1d1	kalendářní
otázka	otázka	k1gFnSc1	otázka
<g/>
;	;	kIx,	;
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Sretenský	Sretenský	k2eAgInSc1d1	Sretenský
monastýr	monastýr	k1gInSc1	monastýr
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Juliánské	juliánský	k2eAgFnPc1d1	juliánská
datum	datum	k1gInSc1	datum
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc2	galerie
Juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
