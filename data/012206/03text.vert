<p>
<s>
Betty	Betty	k1gFnSc7	Betty
Mahmoody	Mahmooda	k1gFnSc2	Mahmooda
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
v	v	k7c6	v
Alma	alma	k1gFnSc1	alma
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
především	především	k9	především
prvotinou	prvotina	k1gFnSc7	prvotina
Bez	bez	k7c2	bez
dcerky	dcerka	k1gFnSc2	dcerka
neodejdu	odejít	k5eNaPmIp1nS	odejít
a	a	k8xC	a
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
práv	právo	k1gNnPc2	právo
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1984	[number]	k4	1984
vycestovala	vycestovat	k5eAaPmAgFnS	vycestovat
Betty	Betty	k1gFnSc1	Betty
Mahmoody	Mahmooda	k1gFnSc2	Mahmooda
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
čtyřletou	čtyřletý	k2eAgFnSc7d1	čtyřletá
dcerou	dcera	k1gFnSc7	dcera
Mahtob	Mahtoba	k1gFnPc2	Mahtoba
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
mužem	muž	k1gMnSc7	muž
lékařem	lékař	k1gMnSc7	lékař
Bozorgem	Bozorg	k1gMnSc7	Bozorg
Mahmoodym	Mahmoodym	k1gInSc4	Mahmoodym
<g/>
,	,	kIx,	,
za	za	k7c7	za
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Íránu	Írán	k1gInSc2	Írán
na	na	k7c4	na
dvoutýdenní	dvoutýdenní	k2eAgFnSc4d1	dvoutýdenní
návštěvu	návštěva	k1gFnSc4	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
změnila	změnit	k5eAaPmAgFnS	změnit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zůstat	zůstat	k5eAaPmF	zůstat
a	a	k8xC	a
neumožnil	umožnit	k5eNaPmAgMnS	umožnit
Betty	Betty	k1gFnSc4	Betty
odcestovat	odcestovat	k5eAaPmF	odcestovat
zpět	zpět	k6eAd1	zpět
domů	domů	k6eAd1	domů
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
dcerou	dcera	k1gFnSc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
zážitcích	zážitek	k1gInPc6	zážitek
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
útěku	útěk	k1gInSc6	útěk
odtamtud	odtamtud	k6eAd1	odtamtud
napsala	napsat	k5eAaBmAgFnS	napsat
později	pozdě	k6eAd2	pozdě
Betty	Betty	k1gFnSc1	Betty
knihu	kniha	k1gFnSc4	kniha
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Bez	bez	k7c2	bez
dcerky	dcerka	k1gFnSc2	dcerka
neodejdu	odejít	k5eNaPmIp1nS	odejít
<g/>
.	.	kIx.	.
</s>
</p>
