<s>
Starokladrubský	Starokladrubský	k2eAgMnSc1d1	Starokladrubský
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgNnSc4d3	nejstarší
české	český	k2eAgNnSc4d1	české
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
,	,	kIx,	,
český	český	k2eAgInSc4d1	český
chovatelský	chovatelský	k2eAgInSc4d1	chovatelský
unikát	unikát	k1gInSc4	unikát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
původ	původ	k1gInSc1	původ
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgNnSc1d1	jediné
plemeno	plemeno	k1gNnSc1	plemeno
koní	koní	k2eAgFnSc2d1	koní
vyšlechtěné	vyšlechtěný	k2eAgFnSc2d1	vyšlechtěná
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
ceremoniální	ceremoniální	k2eAgInPc4d1	ceremoniální
účely	účel	k1gInPc4	účel
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
udržel	udržet	k5eAaPmAgMnS	udržet
barokní	barokní	k2eAgInSc4d1	barokní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
výstavní	výstavní	k2eAgInSc1d1	výstavní
nebo	nebo	k8xC	nebo
rodinný	rodinný	k2eAgMnSc1d1	rodinný
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
vhodný	vhodný	k2eAgMnSc1d1	vhodný
i	i	k9	i
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
sobě	se	k3xPyFc3	se
vozil	vozit	k5eAaImAgMnS	vozit
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostal	dostat	k5eAaPmAgInS	dostat
podle	podle	k7c2	podle
hřebčína	hřebčín	k1gInSc2	hřebčín
v	v	k7c6	v
Kladrubech	Kladruby	k1gInPc6	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
starokladrubský	starokladrubský	k2eAgInSc4d1	starokladrubský
bělouš	bělouš	k1gInSc4	bělouš
a	a	k8xC	a
starokladrubský	starokladrubský	k2eAgMnSc1d1	starokladrubský
vraník	vraník	k1gMnSc1	vraník
<g/>
.	.	kIx.	.
</s>
<s>
Starokladrubský	Starokladrubský	k2eAgMnSc1d1	Starokladrubský
vraník	vraník	k1gMnSc1	vraník
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
společně	společně	k6eAd1	společně
s	s	k7c7	s
běloušem	bělouš	k1gInSc7	bělouš
<g/>
.	.	kIx.	.
</s>
<s>
Vraníci	vraník	k1gMnPc1	vraník
ale	ale	k9	ale
zdědili	zdědit	k5eAaPmAgMnP	zdědit
více	hodně	k6eAd2	hodně
vlastností	vlastnost	k1gFnPc2	vlastnost
po	po	k7c6	po
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
neapolských	neapolský	k2eAgInPc6d1	neapolský
předcích	předek	k1gInPc6	předek
<g/>
,	,	kIx,	,
nebyli	být	k5eNaImAgMnP	být
tak	tak	k6eAd1	tak
ustálení	ustálený	k2eAgMnPc1d1	ustálený
v	v	k7c6	v
exteriéru	exteriér	k1gInSc6	exteriér
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
bráni	brán	k2eAgMnPc1d1	brán
jako	jako	k9	jako
méně	málo	k6eAd2	málo
ušlechtilí	ušlechtilý	k2eAgMnPc1d1	ušlechtilý
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
u	u	k7c2	u
císařského	císařský	k2eAgInSc2d1	císařský
dvora	dvůr	k1gInSc2	dvůr
používáni	používat	k5eAaImNgMnP	používat
především	především	k9	především
pro	pro	k7c4	pro
smuteční	smuteční	k2eAgInPc4d1	smuteční
obřady	obřad	k1gInPc4	obřad
a	a	k8xC	a
pro	pro	k7c4	pro
církevní	církevní	k2eAgMnPc4d1	církevní
hodnostáře	hodnostář	k1gMnPc4	hodnostář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
těžké	těžký	k2eAgFnSc3d1	těžká
práci	práce	k1gFnSc3	práce
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
a	a	k8xC	a
tahavost	tahavost	k1gFnSc4	tahavost
byli	být	k5eAaImAgMnP	být
oblíbeni	oblíben	k2eAgMnPc1d1	oblíben
mezi	mezi	k7c7	mezi
chovateli	chovatel	k1gMnPc7	chovatel
a	a	k8xC	a
rolníky	rolník	k1gMnPc7	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
chov	chov	k1gInSc4	chov
založil	založit	k5eAaPmAgMnS	založit
císař	císař	k1gMnSc1	císař
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
nejstarší	starý	k2eAgInSc1d3	nejstarší
hřebčín	hřebčín	k1gInSc1	hřebčín
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
fungujících	fungující	k2eAgInPc2d1	fungující
hřebčínů	hřebčín	k1gInPc2	hřebčín
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
chovu	chov	k1gInSc2	chov
byly	být	k5eAaImAgFnP	být
domácí	domácí	k2eAgFnPc1d1	domácí
klisny	klisna	k1gFnPc1	klisna
zkřížené	zkřížený	k2eAgFnPc1d1	zkřížená
s	s	k7c7	s
dovezenými	dovezený	k2eAgFnPc7d1	dovezená
starošpanělskými	starošpanělský	k2eAgFnPc7d1	starošpanělský
a	a	k8xC	a
staroitalskými	staroitalský	k2eAgFnPc7d1	staroitalská
(	(	kIx(	(
<g/>
neapolskými	neapolský	k2eAgMnPc7d1	neapolský
<g/>
)	)	kIx)	)
hřebci	hřebec	k1gMnPc7	hřebec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
plemenná	plemenný	k2eAgFnSc1d1	plemenná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
přes	přes	k7c4	přes
1	[number]	k4	1
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
starokladrubských	starokladrubský	k2eAgMnPc2d1	starokladrubský
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
měli	mít	k5eAaImAgMnP	mít
údajně	údajně	k6eAd1	údajně
výšku	výška	k1gFnSc4	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
současní	současný	k2eAgMnPc1d1	současný
koně	kůň	k1gMnPc1	kůň
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc4d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
hřebčín	hřebčín	k1gInSc1	hřebčín
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
a	a	k8xC	a
přestože	přestože	k8xS	přestože
koně	kůň	k1gMnPc1	kůň
byli	být	k5eAaImAgMnP	být
včas	včas	k6eAd1	včas
evakuováni	evakuovat	k5eAaBmNgMnP	evakuovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
mnoho	mnoho	k4c1	mnoho
dokumentů	dokument	k1gInPc2	dokument
o	o	k7c6	o
šlechtění	šlechtění	k1gNnSc6	šlechtění
a	a	k8xC	a
chovu	chov	k1gInSc6	chov
starokladrubských	starokladrubský	k2eAgMnPc2d1	starokladrubský
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
pak	pak	k6eAd1	pak
hřebčín	hřebčín	k1gInSc4	hřebčín
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jej	on	k3xPp3gInSc4	on
ale	ale	k9	ale
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
znovu	znovu	k6eAd1	znovu
obnovil	obnovit	k5eAaPmAgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
chov	chov	k1gInSc1	chov
potýkal	potýkat	k5eAaImAgInS	potýkat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
koně	kůň	k1gMnPc1	kůň
byli	být	k5eAaImAgMnP	být
vnímáni	vnímat	k5eAaImNgMnP	vnímat
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
vranému	vraný	k2eAgMnSc3d1	vraný
hřebci	hřebec	k1gMnSc3	hřebec
Pepoli	Pepole	k1gFnSc4	Pepole
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
potomek	potomek	k1gMnSc1	potomek
<g/>
,	,	kIx,	,
hřebeček	hřebeček	k1gMnSc1	hřebeček
Imperatore	Imperator	k1gMnSc5	Imperator
byl	být	k5eAaImAgInS	být
otcem	otec	k1gMnSc7	otec
plemeníka	plemeník	k1gMnSc2	plemeník
jménem	jméno	k1gNnSc7	jméno
General	General	k1gFnSc2	General
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
měl	mít	k5eAaImAgInS	mít
4	[number]	k4	4
syny	syn	k1gMnPc7	syn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
vlastní	vlastní	k2eAgFnPc4d1	vlastní
linie	linie	k1gFnPc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
byla	být	k5eAaImAgFnS	být
linie	linie	k1gFnSc1	linie
Generale	General	k1gMnSc5	General
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
Generalissimus	generalissimus	k1gMnSc1	generalissimus
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
a	a	k8xC	a
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
byla	být	k5eAaImAgNnP	být
obnovena	obnovit	k5eAaPmNgNnP	obnovit
pouze	pouze	k6eAd1	pouze
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Bělouši	bělouš	k1gMnPc1	bělouš
byli	být	k5eAaImAgMnP	být
tedy	tedy	k8xC	tedy
chováni	chovat	k5eAaImNgMnP	chovat
ve	v	k7c6	v
2	[number]	k4	2
liniích	linie	k1gFnPc6	linie
a	a	k8xC	a
4	[number]	k4	4
rodinách	rodina	k1gFnPc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
rozštěpila	rozštěpit	k5eAaPmAgFnS	rozštěpit
na	na	k7c4	na
3	[number]	k4	3
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
rodina	rodina	k1gFnSc1	rodina
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc1d3	veliký
zastoupení	zastoupení	k1gNnSc2	zastoupení
a	a	k8xC	a
jména	jméno	k1gNnSc2	jméno
představitelek	představitelka	k1gFnPc2	představitelka
začínají	začínat	k5eAaImIp3nP	začínat
písmeny	písmeno	k1gNnPc7	písmeno
E	E	kA	E
<g/>
,	,	kIx,	,
A	A	kA	A
a	a	k8xC	a
P.	P.	kA	P.
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
rodiny	rodina	k1gFnPc4	rodina
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
,	,	kIx,	,
R	R	kA	R
nebo	nebo	k8xC	nebo
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
rodina	rodina	k1gFnSc1	rodina
S.	S.	kA	S.
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
křížení	křížení	k1gNnSc6	křížení
použita	použit	k2eAgFnSc1d1	použita
krev	krev	k1gFnSc1	krev
lipicánů	lipicán	k1gMnPc2	lipicán
<g/>
,	,	kIx,	,
orlovských	orlovský	k2eAgMnPc2d1	orlovský
klusáků	klusák	k1gMnPc2	klusák
a	a	k8xC	a
arabských	arabský	k2eAgMnPc2d1	arabský
polokrevníků	polokrevník	k1gMnPc2	polokrevník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
křížení	křížení	k1gNnPc1	křížení
s	s	k7c7	s
anglickými	anglický	k2eAgMnPc7d1	anglický
plnokrevníky	plnokrevník	k1gMnPc7	plnokrevník
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Vraník	vraník	k1gMnSc1	vraník
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
bělouš	bělouš	k1gInSc1	bělouš
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
křížením	křížení	k1gNnSc7	křížení
domácích	domácí	k2eAgFnPc2d1	domácí
klisen	klisna	k1gFnPc2	klisna
se	s	k7c7	s
starošpanělskými	starošpanělský	k2eAgMnPc7d1	starošpanělský
a	a	k8xC	a
staroitalskými	staroitalský	k2eAgMnPc7d1	staroitalský
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Zakladateli	zakladatel	k1gMnSc3	zakladatel
stáda	stádo	k1gNnPc1	stádo
vraníků	vraník	k1gMnPc2	vraník
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
hřebců	hřebec	k1gMnPc2	hřebec
<g/>
,	,	kIx,	,
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
a	a	k8xC	a
Napoleone	Napoleon	k1gMnSc5	Napoleon
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc6	jenž
poslední	poslední	k2eAgMnSc1d1	poslední
jedinec	jedinec	k1gMnSc1	jedinec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Napoleone	Napoleon	k1gMnSc5	Napoleon
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
jatka	jatka	k1gFnSc1	jatka
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
téměř	téměř	k6eAd1	téměř
také	také	k9	také
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ale	ale	k9	ale
udržel	udržet	k5eAaPmAgMnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
založen	založit	k5eAaPmNgMnS	založit
italsko-španělským	italsko-španělský	k2eAgMnSc7d1	italsko-španělský
hřebcem	hřebec	k1gMnSc7	hřebec
vraníkem	vraník	k1gMnSc7	vraník
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Kladrub	Kladruby	k1gInPc2	Kladruby
z	z	k7c2	z
arcibiskupského	arcibiskupský	k2eAgInSc2d1	arcibiskupský
solnohradského	solnohradský	k2eAgInSc2d1	solnohradský
hřebčína	hřebčín	k1gInSc2	hřebčín
Riess	Riessa	k1gFnPc2	Riessa
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
linie	linie	k1gFnPc1	linie
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
konce	konec	k1gInPc4	konec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
nadlouho	nadlouho	k6eAd1	nadlouho
<g/>
;	;	kIx,	;
brzy	brzy	k6eAd1	brzy
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
a	a	k8xC	a
udržela	udržet	k5eAaPmAgFnS	udržet
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
hřebcem	hřebec	k1gMnSc7	hřebec
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
narozeným	narozený	k2eAgMnPc3d1	narozený
1800	[number]	k4	1800
a	a	k8xC	a
pocházejícím	pocházející	k2eAgFnPc3d1	pocházející
z	z	k7c2	z
hřebčína	hřebčín	k1gInSc2	hřebčín
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
z	z	k7c2	z
nenávisti	nenávist	k1gFnSc2	nenávist
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
habsburskému	habsburský	k2eAgNnSc3d1	habsburské
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
záměr	záměr	k1gInSc1	záměr
zničit	zničit	k5eAaPmF	zničit
chov	chov	k1gInSc4	chov
starokladrubských	starokladrubský	k2eAgMnPc2d1	starokladrubský
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
bráni	brán	k2eAgMnPc1d1	brán
jako	jako	k8xC	jako
zdegenerovaní	zdegenerovaný	k2eAgMnPc1d1	zdegenerovaný
a	a	k8xC	a
jako	jako	k9	jako
přežitek	přežitek	k1gInSc4	přežitek
staré	starý	k2eAgFnSc2d1	stará
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ponechat	ponechat	k5eAaPmF	ponechat
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
pouze	pouze	k6eAd1	pouze
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
bílých	bílý	k2eAgMnPc2d1	bílý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vraníci	vraník	k1gMnPc1	vraník
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
méně	málo	k6eAd2	málo
ušlechtilé	ušlechtilý	k2eAgFnPc4d1	ušlechtilá
<g/>
,	,	kIx,	,
asi	asi	k9	asi
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
byli	být	k5eAaImAgMnP	být
používáni	používán	k2eAgMnPc1d1	používán
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
likvidací	likvidace	k1gFnSc7	likvidace
začalo	začít	k5eAaPmAgNnS	začít
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
rozprodáváním	rozprodávání	k1gNnPc3	rozprodávání
koní	kůň	k1gMnPc2	kůň
sedlákům	sedlák	k1gInPc3	sedlák
a	a	k8xC	a
na	na	k7c4	na
jatka	jatka	k1gFnSc1	jatka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
zrodil	zrodit	k5eAaPmAgInS	zrodit
plán	plán	k1gInSc1	plán
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
profesora	profesor	k1gMnSc2	profesor
Františka	František	k1gMnSc2	František
Bílka	Bílek	k1gMnSc2	Bílek
<g/>
,	,	kIx,	,
zachránit	zachránit	k5eAaPmF	zachránit
a	a	k8xC	a
zregenerovat	zregenerovat	k5eAaPmF	zregenerovat
toto	tento	k3xDgNnSc4	tento
mohutné	mohutný	k2eAgNnSc4d1	mohutné
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
3	[number]	k4	3
hřebce	hřebec	k1gMnSc2	hřebec
z	z	k7c2	z
linie	linie	k1gFnSc2	linie
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
(	(	kIx(	(
<g/>
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
XXVII	XXVII	kA	XXVII
-	-	kIx~	-
Aja	Aja	k1gFnSc1	Aja
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
XXVII	XXVII	kA	XXVII
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
XXXI	XXXI	kA	XXXI
-	-	kIx~	-
Solo	Solo	k6eAd1	Solo
1927	[number]	k4	1927
-	-	kIx~	-
ten	ten	k3xDgInSc1	ten
dal	dát	k5eAaPmAgInS	dát
pak	pak	k6eAd1	pak
vzniknout	vzniknout	k5eAaPmF	vzniknout
nové	nový	k2eAgFnSc3d1	nová
linii	linie	k1gFnSc3	linie
Solo	Solo	k1gMnSc1	Solo
<g/>
)	)	kIx)	)
a	a	k8xC	a
11	[number]	k4	11
klisen	klisna	k1gFnPc2	klisna
-	-	kIx~	-
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
čistokrevné	čistokrevný	k2eAgFnPc1d1	čistokrevná
vranky	vranka	k1gFnPc1	vranka
<g/>
.	.	kIx.	.
</s>
<s>
Regenerace	regenerace	k1gFnSc1	regenerace
probíhala	probíhat	k5eAaImAgFnS	probíhat
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Průhonicích	Průhonice	k1gFnPc6	Průhonice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
stanice	stanice	k1gFnSc2	stanice
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
koní	kůň	k1gMnPc2	kůň
ve	v	k7c6	v
Slatiňanech	Slatiňany	k1gInPc6	Slatiňany
u	u	k7c2	u
Chrudimi	Chrudim	k1gFnSc2	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Chovná	chovný	k2eAgFnSc1d1	chovná
základna	základna	k1gFnSc1	základna
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
krom	krom	k7c2	krom
běloušů	bělouš	k1gMnPc2	bělouš
i	i	k9	i
hřebce	hřebec	k1gMnPc4	hřebec
nejvíce	nejvíce	k6eAd1	nejvíce
podobných	podobný	k2eAgNnPc2d1	podobné
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
především	především	k9	především
lipického	lipický	k2eAgMnSc4d1	lipický
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
nejbližšího	blízký	k2eAgMnSc4d3	nejbližší
příbuzného	příbuzný	k1gMnSc4	příbuzný
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
něj	on	k3xPp3gNnSc2	on
ale	ale	k9	ale
byli	být	k5eAaImAgMnP	být
použití	použití	k1gNnSc4	použití
i	i	k8xC	i
fríští	fríský	k2eAgMnPc1d1	fríský
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
orlovští	orlovský	k2eAgMnPc1d1	orlovský
klusáci	klusák	k1gMnPc1	klusák
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
starokladrubského	starokladrubský	k2eAgMnSc4d1	starokladrubský
vraníka	vraník	k1gMnSc4	vraník
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
proces	proces	k1gInSc1	proces
regenerace	regenerace	k1gFnSc2	regenerace
prohlášen	prohlášen	k2eAgInSc4d1	prohlášen
za	za	k7c2	za
úspěšně	úspěšně	k6eAd1	úspěšně
splněný	splněný	k2eAgMnSc1d1	splněný
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
se	se	k3xPyFc4	se
v	v	k7c6	v
udržovacím	udržovací	k2eAgNnSc6d1	udržovací
šlechtění	šlechtění	k1gNnSc6	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
chov	chov	k1gInSc1	chov
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
čtyřech	čtyři	k4xCgFnPc6	čtyři
základních	základní	k2eAgFnPc6d1	základní
liniích	linie	k1gFnPc6	linie
-	-	kIx~	-
pojmenovaných	pojmenovaný	k2eAgFnPc2d1	pojmenovaná
po	po	k7c6	po
hřebcích	hřebec	k1gMnPc6	hřebec
zakladatelích	zakladatel	k1gMnPc6	zakladatel
<g/>
:	:	kIx,	:
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
<g/>
,	,	kIx,	,
Solo	Sola	k1gFnSc5	Sola
<g/>
,	,	kIx,	,
Siglavi	Siglaev	k1gFnSc3	Siglaev
Pakra	Pakr	k1gInSc2	Pakr
(	(	kIx(	(
<g/>
Lipický	Lipický	k2eAgMnSc1d1	Lipický
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
a	a	k8xC	a
Romke	Romke	k1gInSc1	Romke
(	(	kIx(	(
<g/>
Fríský	fríský	k2eAgMnSc1d1	fríský
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starokladrubský	Starokladrubský	k2eAgMnSc1d1	Starokladrubský
kůň	kůň	k1gMnSc1	kůň
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejtěžší	těžký	k2eAgMnPc4d3	nejtěžší
teplokrevníky	teplokrevník	k1gMnPc4	teplokrevník
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
koně	kůň	k1gMnPc1	kůň
velkého	velký	k2eAgInSc2d1	velký
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
rámce	rámec	k1gInSc2	rámec
s	s	k7c7	s
ušlechtilými	ušlechtilý	k2eAgFnPc7d1	ušlechtilá
proporcemi	proporce	k1gFnPc7	proporce
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
hlava	hlava	k1gFnSc1	hlava
má	mít	k5eAaImIp3nS	mít
klabonos	klabonos	k1gInSc4	klabonos
typický	typický	k2eAgInSc4d1	typický
pro	pro	k7c4	pro
starokladrubské	starokladrubský	k2eAgMnPc4d1	starokladrubský
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
výrazné	výrazný	k2eAgFnPc1d1	výrazná
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
velké	velký	k2eAgNnSc4d1	velké
oči	oko	k1gNnPc4	oko
s	s	k7c7	s
živým	živý	k2eAgInSc7d1	živý
a	a	k8xC	a
inteligentním	inteligentní	k2eAgInSc7d1	inteligentní
výrazem	výraz	k1gInSc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vysoko	vysoko	k6eAd1	vysoko
nasazený	nasazený	k2eAgInSc1d1	nasazený
<g/>
,	,	kIx,	,
mohutný	mohutný	k2eAgInSc1d1	mohutný
<g/>
,	,	kIx,	,
klenutý	klenutý	k2eAgInSc1d1	klenutý
a	a	k8xC	a
středně	středně	k6eAd1	středně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
i	i	k9	i
dobře	dobře	k6eAd1	dobře
osvalený	osvalený	k2eAgMnSc1d1	osvalený
<g/>
.	.	kIx.	.
</s>
<s>
Kohoutek	kohoutek	k1gInSc1	kohoutek
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
.	.	kIx.	.
</s>
<s>
Kohoutková	kohoutkový	k2eAgFnSc1d1	kohoutková
výška	výška	k1gFnSc1	výška
<g/>
[	[	kIx(	[
<g/>
KVH	KVH	kA	KVH
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
165	[number]	k4	165
-	-	kIx~	-
175	[number]	k4	175
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hrudník	hrudník	k1gInSc1	hrudník
je	být	k5eAaImIp3nS	být
hluboký	hluboký	k2eAgInSc1d1	hluboký
a	a	k8xC	a
široký	široký	k2eAgInSc1d1	široký
s	s	k7c7	s
přiměřeně	přiměřeně	k6eAd1	přiměřeně
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
lopatkou	lopatka	k1gFnSc7	lopatka
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
akci	akce	k1gFnSc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
hrudi	hruď	k1gFnSc2	hruď
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
205	[number]	k4	205
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Záď	záď	k1gFnSc1	záď
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
skloněná	skloněný	k2eAgFnSc1d1	skloněná
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
záď	záď	k1gFnSc1	záď
střechovitá	střechovitý	k2eAgFnSc1d1	střechovitá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přimícháním	přimíchání	k1gNnSc7	přimíchání
cizí	cizí	k2eAgFnSc2d1	cizí
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
zakulatila	zakulatit	k5eAaPmAgFnS	zakulatit
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
měkčí	měkký	k2eAgInSc4d2	měkčí
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
kostnatý	kostnatý	k2eAgInSc4d1	kostnatý
a	a	k8xC	a
suchý	suchý	k2eAgInSc4d1	suchý
fundament	fundament	k1gInSc4	fundament
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgFnPc1d1	krátká
se	s	k7c7	s
strmějšími	strmý	k2eAgFnPc7d2	strmější
spěnkami	spěnka	k1gFnPc7	spěnka
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
holeně	holeň	k1gFnSc2	holeň
činí	činit	k5eAaImIp3nS	činit
okolo	okolo	k7c2	okolo
20	[number]	k4	20
-	-	kIx~	-
24	[number]	k4	24
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Kopyta	kopyto	k1gNnPc1	kopyto
jsou	být	k5eAaImIp3nP	být
tvrdá	tvrdý	k2eAgNnPc1d1	tvrdé
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
tvarovaná	tvarovaný	k2eAgFnSc1d1	tvarovaná
a	a	k8xC	a
prostorná	prostorný	k2eAgFnSc1d1	prostorná
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
hustá	hustý	k2eAgFnSc1d1	hustá
hříva	hříva	k1gFnSc1	hříva
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
neměly	mít	k5eNaImAgFnP	mít
stříhat	stříhat	k5eAaImF	stříhat
ani	ani	k8xC	ani
jinak	jinak	k6eAd1	jinak
upravovat	upravovat	k5eAaImF	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
elastické	elastický	k2eAgInPc4d1	elastický
<g/>
,	,	kIx,	,
kadencové	kadencový	k2eAgInPc4d1	kadencový
a	a	k8xC	a
prostorné	prostorný	k2eAgInPc4d1	prostorný
chody	chod	k1gInPc4	chod
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
akcí	akce	k1gFnSc7	akce
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
typické	typický	k2eAgMnPc4d1	typický
a	a	k8xC	a
podtrhují	podtrhovat	k5eAaImIp3nP	podtrhovat
majestátní	majestátní	k2eAgInSc4d1	majestátní
vzhled	vzhled	k1gInSc4	vzhled
tohoto	tento	k3xDgMnSc2	tento
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
700	[number]	k4	700
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
bělouši	bělouš	k1gMnPc7	bělouš
a	a	k8xC	a
vraníky	vraník	k1gMnPc7	vraník
nejsou	být	k5eNaImIp3nP	být
valné	valný	k2eAgFnPc1d1	valná
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
až	až	k9	až
na	na	k7c4	na
barvu	barva	k1gFnSc4	barva
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Vraník	vraník	k1gMnSc1	vraník
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
<g/>
,	,	kIx,	,
mohutný	mohutný	k2eAgMnSc1d1	mohutný
teplokrevný	teplokrevný	k2eAgMnSc1d1	teplokrevný
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
robustní	robustní	k2eAgFnPc1d1	robustní
než	než	k8xS	než
bělouš	bělouš	k1gInSc1	bělouš
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
válcovitý	válcovitý	k2eAgMnSc1d1	válcovitý
<g/>
.	.	kIx.	.
</s>
<s>
Záď	záď	k1gFnSc1	záď
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
více	hodně	k6eAd2	hodně
skloněná	skloněný	k2eAgFnSc1d1	skloněná
než	než	k8xS	než
u	u	k7c2	u
běloušů	bělouš	k1gInPc2	bělouš
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
výraznější	výrazný	k2eAgInSc4d2	výraznější
klabonos	klabonos	k1gInSc4	klabonos
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
lesklá	lesklý	k2eAgFnSc1d1	lesklá
černá	černý	k2eAgFnSc1d1	černá
srst	srst	k1gFnSc1	srst
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
žádné	žádný	k3yNgInPc4	žádný
bílé	bílý	k2eAgInPc4d1	bílý
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Chody	Chod	k1gMnPc4	Chod
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
energické	energický	k2eAgFnPc1d1	energická
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
s	s	k7c7	s
rychlým	rychlý	k2eAgInSc7d1	rychlý
klusem	klus	k1gInSc7	klus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
nasazený	nasazený	k2eAgInSc1d1	nasazený
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
u	u	k7c2	u
běloušů	bělouš	k1gInPc2	bělouš
<g/>
.	.	kIx.	.
</s>
<s>
Starokladrubský	Starokladrubský	k2eAgMnSc1d1	Starokladrubský
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
s	s	k7c7	s
impozantním	impozantní	k2eAgInSc7d1	impozantní
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
pozdním	pozdní	k2eAgNnSc7d1	pozdní
dospíváním	dospívání	k1gNnSc7	dospívání
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
dlouhověkostí	dlouhověkost	k1gFnSc7	dlouhověkost
(	(	kIx(	(
<g/>
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
velkou	velký	k2eAgFnSc7d1	velká
vytrvalostí	vytrvalost	k1gFnSc7	vytrvalost
<g/>
.	.	kIx.	.
</s>
<s>
Klisny	klisna	k1gFnPc1	klisna
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
plodné	plodný	k2eAgFnPc1d1	plodná
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
plemeno	plemeno	k1gNnSc1	plemeno
má	mít	k5eAaImIp3nS	mít
živý	živý	k2eAgInSc4d1	živý
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
klidný	klidný	k2eAgInSc4d1	klidný
vyrovnaný	vyrovnaný	k2eAgInSc4d1	vyrovnaný
temperament	temperament	k1gInSc4	temperament
<g/>
.	.	kIx.	.
</s>
<s>
Povahou	povaha	k1gFnSc7	povaha
je	být	k5eAaImIp3nS	být
laskavý	laskavý	k2eAgMnSc1d1	laskavý
a	a	k8xC	a
ochotný	ochotný	k2eAgMnSc1d1	ochotný
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jim	on	k3xPp3gMnPc3	on
trvá	trvat	k5eAaImIp3nS	trvat
trochu	trochu	k6eAd1	trochu
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
jezdec	jezdec	k1gInSc1	jezdec
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
případní	případný	k2eAgMnPc1d1	případný
majitelé	majitel	k1gMnPc1	majitel
měli	mít	k5eAaImAgMnP	mít
obrnit	obrnit	k5eAaPmF	obrnit
trpělivostí	trpělivost	k1gFnSc7	trpělivost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
velké	velký	k2eAgNnSc4d1	velké
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
,	,	kIx,	,
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
citlivostí	citlivost	k1gFnSc7	citlivost
a	a	k8xC	a
jemností	jemnost	k1gFnSc7	jemnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
milí	milý	k2eAgMnPc1d1	milý
a	a	k8xC	a
přátelští	přátelský	k2eAgMnPc1d1	přátelský
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
vhodní	vhodný	k2eAgMnPc1d1	vhodný
i	i	k9	i
k	k	k7c3	k
vožení	vožení	k1gNnSc3	vožení
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vraník	vraník	k1gMnSc1	vraník
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
běloušem	bělouš	k1gInSc7	bělouš
ve	v	k7c6	v
vlastnostech	vlastnost	k1gFnPc6	vlastnost
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgMnSc1d1	podobný
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
přátelský	přátelský	k2eAgInSc4d1	přátelský
<g/>
,	,	kIx,	,
klidný	klidný	k2eAgInSc4d1	klidný
a	a	k8xC	a
pracovitý	pracovitý	k2eAgInSc4d1	pracovitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
je	být	k5eAaImIp3nS	být
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
<g/>
,	,	kIx,	,
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
vlastnostem	vlastnost	k1gFnPc3	vlastnost
a	a	k8xC	a
dobré	dobrý	k2eAgFnSc3d1	dobrá
ovladatelnosti	ovladatelnost	k1gFnSc3	ovladatelnost
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
stáda	stádo	k1gNnSc2	stádo
běloušů	bělouš	k1gMnPc2	bělouš
byl	být	k5eAaImAgMnS	být
italsko-španělský	italsko-španělský	k2eAgMnSc1d1	italsko-španělský
hřebec	hřebec	k1gMnSc1	hřebec
Peppoli	Peppole	k1gFnSc4	Peppole
<g/>
,	,	kIx,	,
od	od	k7c2	od
jehož	jehož	k3xOyRp3gMnPc2	jehož
potomků	potomek	k1gMnPc2	potomek
Imperatora	Imperator	k1gMnSc2	Imperator
<g/>
,	,	kIx,	,
Generala	General	k1gMnSc2	General
a	a	k8xC	a
Generalissima	generalissimus	k1gMnSc2	generalissimus
se	se	k3xPyFc4	se
chov	chov	k1gInSc4	chov
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
kmenech	kmen	k1gInPc6	kmen
kmenové	kmenový	k2eAgFnSc2d1	kmenová
číslo	číslo	k1gNnSc4	číslo
–	–	k?	–
kmen	kmen	k1gInSc1	kmen
-	-	kIx~	-
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
narození	narození	k1gNnSc2	narození
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
)	)	kIx)	)
L1	L1	k1gFnSc1	L1
–	–	k?	–
Generale	General	k1gMnSc5	General
(	(	kIx(	(
<g/>
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
L11	L11	k1gFnSc1	L11
–	–	k?	–
Generale-Generalissimus	Generale-Generalissimus	k1gInSc1	Generale-Generalissimus
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Gss	Gss	k?	Gss
XXIII-	XXIII-	k1gFnSc1	XXIII-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
L2	L2	k1gFnSc1	L2
–	–	k?	–
Favory	Favory	k?	Favory
(	(	kIx(	(
<g/>
1779	[number]	k4	1779
<g/>
)	)	kIx)	)
L21	L21	k1gFnSc1	L21
–	–	k?	–
Favory-Generalissimus	Favory-Generalissimus	k1gInSc1	Favory-Generalissimus
(	(	kIx(	(
<g/>
Gss	Gss	k1gFnSc1	Gss
XXIX-	XXIX-	k1gFnSc1	XXIX-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
L3	L3	k1gFnSc1	L3
–	–	k?	–
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
L31	L31	k1gMnSc1	L31
–	–	k?	–
Solo	Solo	k1gMnSc1	Solo
(	(	kIx(	(
<g/>
Sacramoso	Sacramosa	k1gFnSc5	Sacramosa
XXXI-	XXXI-	k1gMnSc7	XXXI-
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
L12	L12	k1gFnSc1	L12
–	–	k?	–
Napoleone	Napoleon	k1gMnSc5	Napoleon
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
L13	L13	k1gMnSc1	L13
–	–	k?	–
Generalissimus	generalissimus	k1gMnSc1	generalissimus
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
L4	L4	k1gFnSc3	L4
–	–	k?	–
Siglavi	Siglaev	k1gFnSc3	Siglaev
Pakra	Pakra	k1gMnSc1	Pakra
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
L5	L5	k1gFnSc1	L5
–	–	k?	–
Romke	Romke	k1gFnSc1	Romke
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
L6	L6	k1gFnSc1	L6
–	–	k?	–
Rudolfo	Rudolfa	k1gFnSc5	Rudolfa
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
Napoleone	Napoleon	k1gMnSc5	Napoleon
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
r.	r.	kA	r.
<g/>
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
kmen	kmen	k1gInSc1	kmen
Generalissimus	generalissimus	k1gMnSc1	generalissimus
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
úhynem	úhyn	k1gInSc7	úhyn
hřebce	hřebec	k1gMnPc4	hřebec
Generalissimus	generalissimus	k1gMnSc1	generalissimus
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
zařazením	zařazení	k1gNnSc7	zařazení
hřebce	hřebec	k1gMnPc4	hřebec
Generalissimus	generalissimus	k1gMnSc1	generalissimus
XXIII	XXIII	kA	XXIII
(	(	kIx(	(
<g/>
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
Generale	General	k1gMnSc5	General
XXIII	XXIII	kA	XXIII
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
407	[number]	k4	407
Gss	Gss	k1gMnSc2	Gss
XXII	XXII	kA	XXII
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
zařazením	zařazení	k1gNnSc7	zařazení
hřebce	hřebec	k1gMnPc4	hřebec
Generalissimus	generalissimus	k1gMnSc1	generalissimus
XXIX	XXIX	kA	XXIX
(	(	kIx(	(
<g/>
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
Favory	Favory	k?	Favory
IV	Iva	k1gFnPc2	Iva
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
817	[number]	k4	817
Gss	Gss	k1gMnSc2	Gss
XXIII	XXIII	kA	XXIII
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starokladrubští	Starokladrubský	k2eAgMnPc1d1	Starokladrubský
koně	kůň	k1gMnPc1	kůň
jsou	být	k5eAaImIp3nP	být
uplatňováni	uplatňovat	k5eAaImNgMnP	uplatňovat
jako	jako	k9	jako
koně	kůň	k1gMnPc1	kůň
tažní	tažní	k2eAgMnPc1d1	tažní
nebo	nebo	k8xC	nebo
kočároví	kočárový	k2eAgMnPc1d1	kočárový
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
čtyřspřeží	čtyřspřeží	k1gNnSc6	čtyřspřeží
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
stoupá	stoupat	k5eAaImIp3nS	stoupat
jejich	jejich	k3xOp3gFnSc1	jejich
obliba	obliba	k1gFnSc1	obliba
v	v	k7c6	v
jízdě	jízda	k1gFnSc6	jízda
pod	pod	k7c7	pod
sedlem	sedlo	k1gNnSc7	sedlo
-	-	kIx~	-
v	v	k7c6	v
drezuře	drezura	k1gFnSc6	drezura
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
koně	kůň	k1gMnPc1	kůň
byli	být	k5eAaImAgMnP	být
speciálně	speciálně	k6eAd1	speciálně
vyšlechtěni	vyšlechtit	k5eAaPmNgMnP	vyšlechtit
pro	pro	k7c4	pro
císařský	císařský	k2eAgInSc4d1	císařský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
vzhledu	vzhled	k1gInSc3	vzhled
a	a	k8xC	a
reprezentativnímu	reprezentativní	k2eAgInSc3d1	reprezentativní
chodu	chod	k1gInSc3	chod
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
přehlídkách	přehlídka	k1gFnPc6	přehlídka
a	a	k8xC	a
ceremoniálních	ceremoniální	k2eAgFnPc6d1	ceremoniální
akcích	akce	k1gFnPc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
také	také	k9	také
pořizují	pořizovat	k5eAaImIp3nP	pořizovat
jako	jako	k9	jako
dobré	dobrý	k2eAgMnPc4d1	dobrý
společníky	společník	k1gMnPc4	společník
pro	pro	k7c4	pro
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
se	se	k3xPyFc4	se
výborně	výborně	k6eAd1	výborně
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
jako	jako	k9	jako
kyrysnický	kyrysnický	k2eAgMnSc1d1	kyrysnický
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
klidný	klidný	k2eAgInSc4d1	klidný
a	a	k8xC	a
respekt	respekt	k1gInSc4	respekt
vzbuzující	vzbuzující	k2eAgInSc1d1	vzbuzující
pohled	pohled	k1gInSc1	pohled
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
je	on	k3xPp3gMnPc4	on
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
i	i	k9	i
jízdní	jízdní	k2eAgFnSc1d1	jízdní
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
výborné	výborný	k2eAgFnPc4d1	výborná
povahové	povahový	k2eAgFnPc4d1	povahová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
koně	kůň	k1gMnPc1	kůň
užívají	užívat	k5eAaImIp3nP	užívat
pro	pro	k7c4	pro
hipoterapii	hipoterapie	k1gFnSc4	hipoterapie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
starokladrubských	starokladrubský	k2eAgMnPc2d1	starokladrubský
koní	kůň	k1gMnPc2	kůň
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vozatajském	vozatajský	k2eAgInSc6d1	vozatajský
sportu	sport	k1gInSc6	sport
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vynikajících	vynikající	k2eAgInPc2d1	vynikající
výsledků	výsledek	k1gInPc2	výsledek
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Starokladrubský	Starokladrubský	k2eAgMnSc1d1	Starokladrubský
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
světovým	světový	k2eAgInSc7d1	světový
unikátem	unikát	k1gInSc7	unikát
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
původu	původ	k1gInSc3	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
snahou	snaha	k1gFnSc7	snaha
chovatelů	chovatel	k1gMnPc2	chovatel
o	o	k7c6	o
vzkříšení	vzkříšení	k1gNnSc6	vzkříšení
původního	původní	k2eAgNnSc2d1	původní
českého	český	k2eAgNnSc2d1	české
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
starokladrubský	starokladrubský	k2eAgMnSc1d1	starokladrubský
kůň	kůň	k1gMnSc1	kůň
přijat	přijmout	k5eAaPmNgMnS	přijmout
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
označen	označen	k2eAgMnSc1d1	označen
i	i	k8xC	i
hřebčín	hřebčín	k1gInSc1	hřebčín
v	v	k7c6	v
Kladrubech	Kladruby	k1gInPc6	Kladruby
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
starokladrubský	starokladrubský	k2eAgMnSc1d1	starokladrubský
kůň	kůň	k1gMnSc1	kůň
jediným	jediný	k2eAgMnSc7d1	jediný
živým	živý	k1gMnSc7	živý
tvorem	tvor	k1gMnSc7	tvor
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
památkářskou	památkářský	k2eAgFnSc7d1	památkářská
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
ocenění	ocenění	k1gNnSc4	ocenění
nemají	mít	k5eNaImIp3nP	mít
ani	ani	k8xC	ani
Lipicáni	lipicán	k1gMnPc1	lipicán
španělské	španělský	k2eAgFnSc2d1	španělská
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
plemeno	plemeno	k1gNnSc1	plemeno
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
přijato	přijmout	k5eAaPmNgNnS	přijmout
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
FAO	FAO	kA	FAO
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgFnSc2d1	odborná
organizace	organizace	k1gFnSc2	organizace
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
UNESCO	Unesco	k1gNnSc1	Unesco
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgNnSc1d1	kulturní
dědictví	dědictví	k1gNnSc1	dědictví
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
