<s>
Bisexualita	bisexualita	k1gFnSc1	bisexualita
je	být	k5eAaImIp3nS	být
erotická	erotický	k2eAgFnSc1d1	erotická
<g/>
,	,	kIx,	,
emocionální	emocionální	k2eAgFnSc1d1	emocionální
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
sexuální	sexuální	k2eAgFnSc4d1	sexuální
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
k	k	k7c3	k
mužům	muž	k1gMnPc3	muž
i	i	k8xC	i
ženám	žena	k1gFnPc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc1	dva
pojetí	pojetí	k1gNnPc1	pojetí
bisexuality	bisexualita	k1gFnSc2	bisexualita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
je	být	k5eAaImIp3nS	být
chápána	chápán	k2eAgFnSc1d1	chápána
jako	jako	k8xC	jako
svébytná	svébytný	k2eAgFnSc1d1	svébytná
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
heterosexualita	heterosexualita	k1gFnSc1	heterosexualita
a	a	k8xC	a
homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
je	být	k5eAaImIp3nS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
specifický	specifický	k2eAgInSc1d1	specifický
projev	projev	k1gInSc1	projev
sexuální	sexuální	k2eAgFnSc2d1	sexuální
aktivit	aktivita	k1gFnPc2	aktivita
heterosexuálně	heterosexuálně	k6eAd1	heterosexuálně
a	a	k8xC	a
homosexuálně	homosexuálně	k6eAd1	homosexuálně
orientovaných	orientovaný	k2eAgFnPc2d1	orientovaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
bisexualita	bisexualita	k1gFnSc1	bisexualita
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
užíván	užívat	k5eAaImNgInS	užívat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
biologického	biologický	k2eAgInSc2d1	biologický
hermafroditismu	hermafroditismus	k1gInSc2	hermafroditismus
(	(	kIx(	(
<g/>
u	u	k7c2	u
jedinců	jedinec	k1gMnPc2	jedinec
s	s	k7c7	s
mužskými	mužský	k2eAgInPc7d1	mužský
i	i	k8xC	i
ženskými	ženský	k2eAgInPc7d1	ženský
pohlavními	pohlavní	k2eAgInPc7d1	pohlavní
znaky	znak	k1gInPc7	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
byly	být	k5eAaImAgFnP	být
stejnopohlavní	stejnopohlavní	k2eAgInPc4d1	stejnopohlavní
vztahy	vztah	k1gInPc4	vztah
dobovou	dobový	k2eAgFnSc7d1	dobová
psychiatrií	psychiatrie	k1gFnSc7	psychiatrie
a	a	k8xC	a
psychoanalýzou	psychoanalýza	k1gFnSc7	psychoanalýza
vysvětlovány	vysvětlován	k2eAgFnPc1d1	vysvětlována
jako	jako	k8xC	jako
psychický	psychický	k2eAgInSc1d1	psychický
hermafroditismus	hermafroditismus	k1gInSc1	hermafroditismus
či	či	k8xC	či
projev	projev	k1gInSc1	projev
sexuální	sexuální	k2eAgFnSc2d1	sexuální
inverze	inverze	k1gFnSc2	inverze
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Karl	Karl	k1gMnSc1	Karl
Heinrich	Heinrich	k1gMnSc1	Heinrich
Ulrichs	Ulrichs	k1gInSc4	Ulrichs
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
označil	označit	k5eAaPmAgMnS	označit
sexuální	sexuální	k2eAgFnSc4d1	sexuální
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
k	k	k7c3	k
osobám	osoba	k1gFnPc3	osoba
stejného	stejné	k1gNnSc2	stejné
pohlaví	pohlaví	k1gNnSc4	pohlaví
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc4	důsledek
"	"	kIx"	"
<g/>
ženské	ženský	k2eAgFnPc4d1	ženská
duše	duše	k1gFnPc4	duše
v	v	k7c6	v
mužském	mužský	k2eAgNnSc6d1	mužské
těle	tělo	k1gNnSc6	tělo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Tři	tři	k4xCgInPc1	tři
pojednání	pojednání	k1gNnSc1	pojednání
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
sexuality	sexualita	k1gFnSc2	sexualita
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
podstata	podstata	k1gFnSc1	podstata
inverse	inverse	k1gFnSc2	inverse
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
terminologii	terminologie	k1gFnSc6	terminologie
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
ani	ani	k8xC	ani
domněnkou	domněnka	k1gFnSc7	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
domněnkou	domněnka	k1gFnSc7	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
získaná	získaný	k2eAgFnSc1d1	získaná
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
pozornost	pozornost	k1gFnSc1	pozornost
Freud	Freuda	k1gFnPc2	Freuda
věnuje	věnovat	k5eAaPmIp3nS	věnovat
tzv.	tzv.	kA	tzv.
obojaké	obojaký	k2eAgFnSc6d1	obojaká
inversi	inverse	k1gFnSc6	inverse
(	(	kIx(	(
<g/>
psychickému	psychický	k2eAgInSc3d1	psychický
hermafroditismu	hermafroditismus	k1gInSc3	hermafroditismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
zhruba	zhruba	k6eAd1	zhruba
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
pojmu	pojem	k1gInSc3	pojem
bisexuality	bisexualita	k1gFnSc2	bisexualita
<g/>
,	,	kIx,	,
a	a	k8xC	a
příležitostné	příležitostný	k2eAgFnSc6d1	příležitostná
inversi	inverse	k1gFnSc6	inverse
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
případy	případ	k1gInPc4	případ
dnes	dnes	k6eAd1	dnes
interpretované	interpretovaný	k2eAgNnSc4d1	interpretované
jako	jako	k8xS	jako
náhradní	náhradní	k2eAgNnSc4d1	náhradní
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
výskytu	výskyt	k1gInSc2	výskyt
inverse	inverse	k1gFnSc2	inverse
teoriemi	teorie	k1gFnPc7	teorie
Franka	Frank	k1gMnSc2	Frank
Lydstonea	Lydstoneus	k1gMnSc2	Lydstoneus
<g/>
,	,	kIx,	,
Kiernana	Kiernan	k1gMnSc2	Kiernan
a	a	k8xC	a
Chevaliera	chevalier	k1gMnSc2	chevalier
<g/>
,	,	kIx,	,
vycházejícími	vycházející	k2eAgMnPc7d1	vycházející
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
výskytu	výskyt	k1gInSc2	výskyt
bisexuálnosti	bisexuálnost	k1gFnSc2	bisexuálnost
neboli	neboli	k8xC	neboli
hermafroditismu	hermafroditismus	k1gInSc2	hermafroditismus
a	a	k8xC	a
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistý	jistý	k2eAgInSc4d1	jistý
stupeň	stupeň	k1gInSc4	stupeň
anatomického	anatomický	k2eAgInSc2d1	anatomický
hermafroditismu	hermafroditismus	k1gInSc2	hermafroditismus
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
normě	norma	k1gFnSc3	norma
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgNnSc4d1	původní
založení	založení	k1gNnSc4	založení
je	být	k5eAaImIp3nS	být
bisexuální	bisexuální	k2eAgMnSc1d1	bisexuální
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
hermafroditní	hermafroditní	k2eAgInSc1d1	hermafroditní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c4	v
monosexuální	monosexuální	k2eAgFnPc4d1	monosexuální
specializace	specializace	k1gFnPc4	specializace
se	s	k7c7	s
zakrnělými	zakrnělý	k2eAgInPc7d1	zakrnělý
zbytky	zbytek	k1gInPc7	zbytek
znaků	znak	k1gInPc2	znak
druhého	druhý	k4xOgNnSc2	druhý
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Přenesení	přenesení	k1gNnSc1	přenesení
na	na	k7c4	na
psychickou	psychický	k2eAgFnSc4d1	psychická
inversi	inverse	k1gFnSc4	inverse
(	(	kIx(	(
<g/>
psychický	psychický	k2eAgInSc1d1	psychický
hermafroditismus	hermafroditismus	k1gInSc1	hermafroditismus
<g/>
)	)	kIx)	)
však	však	k9	však
Freud	Freud	k1gMnSc1	Freud
shledával	shledávat	k5eAaImAgMnS	shledávat
problematickým	problematický	k2eAgMnSc7d1	problematický
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
inverze	inverze	k1gFnSc1	inverze
není	být	k5eNaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
psychickými	psychický	k2eAgInPc7d1	psychický
a	a	k8xC	a
somatickými	somatický	k2eAgInPc7d1	somatický
příznaky	příznak	k1gInPc7	příznak
hermafroditismu	hermafroditismus	k1gInSc2	hermafroditismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obojí	obojí	k4xRgFnSc7	obojí
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
téměř	téměř	k6eAd1	téměř
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
se	se	k3xPyFc4	se
odvolával	odvolávat	k5eAaImAgMnS	odvolávat
například	například	k6eAd1	například
na	na	k7c4	na
Halbana	Halban	k1gMnSc4	Halban
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
exaktnější	exaktní	k2eAgInSc4d2	exaktnější
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
považoval	považovat	k5eAaImAgMnS	považovat
Freud	Freud	k1gMnSc1	Freud
Krafft-Ebingovu	Krafft-Ebingův	k2eAgFnSc4d1	Krafft-Ebingův
tezi	teze	k1gFnSc4	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k6eAd1	jak
bisexuální	bisexuální	k2eAgNnSc4d1	bisexuální
založení	založení	k1gNnSc4	založení
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
odchylky	odchylka	k1gFnPc4	odchylka
obojího	obojí	k4xRgMnSc2	obojí
jsou	být	k5eAaImIp3nP	být
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
poruchou	porucha	k1gFnSc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
mužských	mužský	k2eAgInPc6d1	mužský
a	a	k8xC	a
ženských	ženský	k2eAgInPc6d1	ženský
centrech	centr	k1gInPc6	centr
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
však	však	k9	však
podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
nebylo	být	k5eNaImAgNnS	být
ještě	ještě	k9	ještě
známo	znám	k2eAgNnSc1d1	známo
nic	nic	k6eAd1	nic
než	než	k8xS	než
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
.	.	kIx.	.
</s>
<s>
Freudův	Freudův	k2eAgInSc1d1	Freudův
koncept	koncept	k1gInSc1	koncept
konstituční	konstituční	k2eAgFnSc2d1	konstituční
bisexuality	bisexualita	k1gFnSc2	bisexualita
dále	daleko	k6eAd2	daleko
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
a	a	k8xC	a
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Stekel	Stekel	k1gMnSc1	Stekel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
je	být	k5eAaImIp3nS	být
bisexualita	bisexualita	k1gFnSc1	bisexualita
původním	původní	k2eAgMnSc7d1	původní
a	a	k8xC	a
univerzálním	univerzální	k2eAgInSc7d1	univerzální
základem	základ	k1gInSc7	základ
lidské	lidský	k2eAgFnSc2d1	lidská
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
mentora	mentor	k1gMnSc2	mentor
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
navazovat	navazovat	k5eAaImF	navazovat
vztahy	vztah	k1gInPc4	vztah
jak	jak	k8xC	jak
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
monosexualita	monosexualita	k1gFnSc1	monosexualita
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
heterosexualita	heterosexualita	k1gFnSc1	heterosexualita
a	a	k8xC	a
homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
jedince	jedinec	k1gMnSc2	jedinec
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
části	část	k1gFnSc2	část
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Freuda	Freud	k1gMnSc2	Freud
<g/>
,	,	kIx,	,
Stekela	Stekel	k1gMnSc2	Stekel
a	a	k8xC	a
několika	několik	k4yIc2	několik
dalších	další	k2eAgMnPc2d1	další
psychoanalytiků	psychoanalytik	k1gMnPc2	psychoanalytik
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
bisexualitě	bisexualita	k1gFnSc3	bisexualita
věnována	věnován	k2eAgFnSc1d1	věnována
významnější	významný	k2eAgFnSc1d2	významnější
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ve	v	k7c6	v
výzkumech	výzkum	k1gInPc6	výzkum
objevil	objevit	k5eAaPmAgMnS	objevit
bisexuálně	bisexuálně	k6eAd1	bisexuálně
se	se	k3xPyFc4	se
projevující	projevující	k2eAgMnSc1d1	projevující
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
buď	buď	k8xC	buď
evidován	evidován	k2eAgMnSc1d1	evidován
jako	jako	k8xC	jako
homosexuál	homosexuál	k1gMnSc1	homosexuál
či	či	k8xC	či
vůbec	vůbec	k9	vůbec
nebyl	být	k5eNaImAgInS	být
brán	brát	k5eAaImNgInS	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
zlom	zlom	k1gInSc1	zlom
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
přístupů	přístup	k1gInPc2	přístup
k	k	k7c3	k
bisexualitě	bisexualita	k1gFnSc3	bisexualita
nastal	nastat	k5eAaPmAgMnS	nastat
díky	díky	k7c3	díky
práci	práce	k1gFnSc3	práce
amerického	americký	k2eAgMnSc2d1	americký
biologa	biolog	k1gMnSc2	biolog
a	a	k8xC	a
průkopníka	průkopník	k1gMnSc2	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sexuologie	sexuologie	k1gFnSc1	sexuologie
Alfreda	Alfred	k1gMnSc2	Alfred
Kinseyho	Kinsey	k1gMnSc2	Kinsey
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
až	až	k9	až
1949	[number]	k4	1949
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
výzkum	výzkum	k1gInSc1	výzkum
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
a	a	k8xC	a
1953	[number]	k4	1953
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Kinseyho	Kinsey	k1gMnSc4	Kinsey
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhodnocování	vyhodnocování	k1gNnSc6	vyhodnocování
jeho	jeho	k3xOp3gInPc2	jeho
výsledků	výsledek	k1gInPc2	výsledek
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgMnPc4	všechen
respondenty	respondent	k1gMnPc4	respondent
nelze	lze	k6eNd1	lze
podle	podle	k7c2	podle
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
označit	označit	k5eAaPmF	označit
buď	buď	k8xC	buď
za	za	k7c4	za
výlučně	výlučně	k6eAd1	výlučně
heterosexuální	heterosexuální	k2eAgInSc4d1	heterosexuální
nebo	nebo	k8xC	nebo
homosexuální	homosexuální	k2eAgInSc4d1	homosexuální
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
zohlednil	zohlednit	k5eAaPmAgMnS	zohlednit
<g/>
,	,	kIx,	,
sestavil	sestavit	k5eAaPmAgMnS	sestavit
sedmibodovou	sedmibodový	k2eAgFnSc4d1	sedmibodová
stupnici	stupnice	k1gFnSc4	stupnice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kinseyho	Kinsey	k1gMnSc2	Kinsey
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
)	)	kIx)	)
kontinuálního	kontinuální	k2eAgInSc2d1	kontinuální
přechodu	přechod	k1gInSc2	přechod
mezi	mezi	k7c7	mezi
heterosexualitou	heterosexualita	k1gFnSc7	heterosexualita
a	a	k8xC	a
homosexualitou	homosexualita	k1gFnSc7	homosexualita
se	se	k3xPyFc4	se
stupni	stupeň	k1gInSc6	stupeň
0	[number]	k4	0
až	až	k9	až
6	[number]	k4	6
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
0	[number]	k4	0
znamenala	znamenat	k5eAaImAgFnS	znamenat
výlučně	výlučně	k6eAd1	výlučně
heterosexuální	heterosexuální	k2eAgNnSc4d1	heterosexuální
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
6	[number]	k4	6
výlučně	výlučně	k6eAd1	výlučně
homosexuální	homosexuální	k2eAgNnSc4d1	homosexuální
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Mezilehlé	mezilehlý	k2eAgInPc1d1	mezilehlý
stupně	stupeň	k1gInPc1	stupeň
1	[number]	k4	1
až	až	k8xS	až
5	[number]	k4	5
značily	značit	k5eAaImAgFnP	značit
různou	různý	k2eAgFnSc4d1	různá
míru	míra	k1gFnSc4	míra
bisexuálního	bisexuální	k2eAgNnSc2d1	bisexuální
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
stupeň	stupeň	k1gInSc1	stupeň
3	[number]	k4	3
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
bisexuální	bisexuální	k2eAgMnSc1d1	bisexuální
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
heterosexuální	heterosexuální	k2eAgFnSc1d1	heterosexuální
a	a	k8xC	a
homosexuální	homosexuální	k2eAgFnSc1d1	homosexuální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
analogicky	analogicky	k6eAd1	analogicky
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
koncepce	koncepce	k1gFnSc1	koncepce
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
Kinseyho	Kinsey	k1gMnSc4	Kinsey
výzkum	výzkum	k1gInSc4	výzkum
z	z	k7c2	z
metodického	metodický	k2eAgNnSc2d1	metodické
hlediska	hledisko	k1gNnSc2	hledisko
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
etap	etapa	k1gFnPc2	etapa
vývoje	vývoj	k1gInSc2	vývoj
moderní	moderní	k2eAgFnSc2d1	moderní
sexuologie	sexuologie	k1gFnSc2	sexuologie
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
významným	významný	k2eAgMnSc7d1	významný
přínosů	přínos	k1gInPc2	přínos
je	být	k5eAaImIp3nS	být
posun	posun	k1gInSc1	posun
vymezení	vymezení	k1gNnSc2	vymezení
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
od	od	k7c2	od
dvou	dva	k4xCgFnPc2	dva
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
kategorií	kategorie	k1gFnPc2	kategorie
(	(	kIx(	(
<g/>
heterosexuality	heterosexualita	k1gFnPc4	heterosexualita
a	a	k8xC	a
homosexuality	homosexualita	k1gFnPc4	homosexualita
<g/>
)	)	kIx)	)
k	k	k7c3	k
již	již	k9	již
zmíněnému	zmíněný	k2eAgNnSc3d1	zmíněné
kontinuálnímu	kontinuální	k2eAgNnSc3d1	kontinuální
pojetí	pojetí	k1gNnSc3	pojetí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podrobné	podrobný	k2eAgInPc1d1	podrobný
výsledky	výsledek	k1gInPc1	výsledek
Kinseyho	Kinsey	k1gMnSc2	Kinsey
výzkumu	výzkum	k1gInSc2	výzkum
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
bisexualitě	bisexualita	k1gFnSc3	bisexualita
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
Zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Na	na	k7c4	na
Kinseyho	Kinsey	k1gMnSc4	Kinsey
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
sedmibodovou	sedmibodový	k2eAgFnSc4d1	sedmibodová
stupnici	stupnice	k1gFnSc4	stupnice
navázala	navázat	k5eAaPmAgFnS	navázat
řada	řada	k1gFnSc1	řada
teoretiků	teoretik	k1gMnPc2	teoretik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
kvalitativnější	kvalitativní	k2eAgInSc4d2	kvalitativní
multidimenzionální	multidimenzionální	k2eAgInSc4d1	multidimenzionální
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
psychiatr	psychiatr	k1gMnSc1	psychiatr
Fritz	Fritz	k1gMnSc1	Fritz
Klein	Klein	k1gMnSc1	Klein
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
tzv.	tzv.	kA	tzv.
Kleinovu	Kleinův	k2eAgFnSc4d1	Kleinova
stupnici	stupnice	k1gFnSc4	stupnice
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
(	(	kIx(	(
<g/>
Klein	Klein	k1gMnSc1	Klein
Sexual	Sexual	k1gMnSc1	Sexual
Orientation	Orientation	k1gInSc4	Orientation
Grid	Grid	k1gInSc1	Grid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zohlednil	zohlednit	k5eAaPmAgMnS	zohlednit
komplexnost	komplexnost	k1gFnSc4	komplexnost
lidské	lidský	k2eAgFnSc2d1	lidská
sexuality	sexualita	k1gFnSc2	sexualita
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
nedostatky	nedostatek	k1gInPc1	nedostatek
Kinseyho	Kinsey	k1gMnSc2	Kinsey
stupnice	stupnice	k1gFnSc2	stupnice
(	(	kIx(	(
<g/>
statičnost	statičnost	k1gFnSc1	statičnost
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
omezení	omezení	k1gNnSc6	omezení
se	se	k3xPyFc4	se
na	na	k7c4	na
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
hodnocení	hodnocení	k1gNnSc1	hodnocení
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kromě	kromě	k7c2	kromě
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
také	také	k9	také
sexuální	sexuální	k2eAgFnSc4d1	sexuální
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnPc4d1	sexuální
fantazie	fantazie	k1gFnPc4	fantazie
<g/>
,	,	kIx,	,
emocionální	emocionální	k2eAgFnPc4d1	emocionální
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
preference	preference	k1gFnPc4	preference
<g/>
,	,	kIx,	,
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
a	a	k8xC	a
sebeidentifikaci	sebeidentifikace	k1gFnSc4	sebeidentifikace
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
posuzováno	posuzovat	k5eAaImNgNnS	posuzovat
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
současnosti	současnost	k1gFnSc2	současnost
a	a	k8xC	a
ideálního	ideální	k2eAgInSc2d1	ideální
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
koncepcí	koncepce	k1gFnPc2	koncepce
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
bisexualita	bisexualita	k1gFnSc1	bisexualita
chápána	chápat	k5eAaImNgFnS	chápat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Dichotomická	dichotomický	k2eAgFnSc1d1	dichotomická
koncepce	koncepce	k1gFnSc1	koncepce
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
binární	binární	k2eAgFnSc1d1	binární
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
existenci	existence	k1gFnSc4	existence
dvou	dva	k4xCgFnPc2	dva
navzájem	navzájem	k6eAd1	navzájem
protikladných	protikladný	k2eAgFnPc2d1	protikladná
a	a	k8xC	a
ohraničených	ohraničený	k2eAgFnPc2d1	ohraničená
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Sexuologové	sexuolog	k1gMnPc1	sexuolog
Blumstein	Blumstein	k1gMnSc1	Blumstein
a	a	k8xC	a
Schwartzová	Schwartzová	k1gFnSc1	Schwartzová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
tři	tři	k4xCgInPc4	tři
dichotomie	dichotomie	k1gFnSc2	dichotomie
strukturující	strukturující	k2eAgNnPc4d1	strukturující
západní	západní	k2eAgNnPc4d1	západní
chápání	chápání	k1gNnPc4	chápání
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gMnPc7	on
pohlaví	pohlaví	k1gNnSc1	pohlaví
(	(	kIx(	(
<g/>
žena	žena	k1gFnSc1	žena
×	×	k?	×
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gender	gender	k1gInSc1	gender
(	(	kIx(	(
<g/>
maskulinní	maskulinní	k2eAgMnPc1d1	maskulinní
×	×	k?	×
feminní	feminný	k2eAgMnPc1d1	feminný
<g/>
)	)	kIx)	)
a	a	k8xC	a
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
či	či	k8xC	či
preference	preference	k1gFnSc1	preference
(	(	kIx(	(
<g/>
heterosexuální	heterosexuální	k2eAgInSc1d1	heterosexuální
×	×	k?	×
homosexuální	homosexuální	k2eAgInSc1d1	homosexuální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařazení	zařazení	k1gNnSc1	zařazení
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kategorií	kategorie	k1gFnPc2	kategorie
pak	pak	k6eAd1	pak
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
výlučnosti	výlučnost	k1gFnSc2	výlučnost
<g/>
;	;	kIx,	;
pakliže	pakliže	k8xS	pakliže
jedinec	jedinec	k1gMnSc1	jedinec
splňuje	splňovat	k5eAaImIp3nS	splňovat
daná	daný	k2eAgNnPc4d1	dané
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
<g/>
.	.	kIx.	.
</s>
<s>
Bisexualita	bisexualita	k1gFnSc1	bisexualita
však	však	k9	však
takovouto	takovýto	k3xDgFnSc4	takovýto
výlučnost	výlučnost	k1gFnSc4	výlučnost
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
<g/>
,	,	kIx,	,
coby	coby	k?	coby
samostatné	samostatný	k2eAgFnSc2d1	samostatná
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
a	a	k8xC	a
identity	identita	k1gFnSc2	identita
<g/>
,	,	kIx,	,
v	v	k7c6	v
dichotomické	dichotomický	k2eAgFnSc6d1	dichotomická
koncepci	koncepce	k1gFnSc6	koncepce
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
bisexuálním	bisexuální	k2eAgNnSc6d1	bisexuální
chování	chování	k1gNnSc6	chování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
specifický	specifický	k2eAgInSc1d1	specifický
projev	projev	k1gInSc1	projev
sexuální	sexuální	k2eAgFnSc2d1	sexuální
aktivit	aktivita	k1gFnPc2	aktivita
heterosexuálně	heterosexuálně	k6eAd1	heterosexuálně
a	a	k8xC	a
homosexuálně	homosexuálně	k6eAd1	homosexuálně
orientovaných	orientovaný	k2eAgFnPc2d1	orientovaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
vysvětlováno	vysvětlován	k2eAgNnSc1d1	vysvětlováno
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
experimentování	experimentování	k1gNnSc1	experimentování
<g/>
,	,	kIx,	,
přechodná	přechodný	k2eAgFnSc1d1	přechodná
fáze	fáze	k1gFnSc1	fáze
coming	coming	k1gInSc1	coming
outu	ouíst	k5eAaPmIp1nS	ouíst
u	u	k7c2	u
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
,	,	kIx,	,
ego-dystonní	egoystonní	k2eAgFnSc1d1	ego-dystonní
homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
,	,	kIx,	,
situační	situační	k2eAgFnSc1d1	situační
homosexualita	homosexualita	k1gFnSc1	homosexualita
apod.	apod.	kA	apod.
Tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
sexuologii	sexuologie	k1gFnSc6	sexuologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Zvěřiny	Zvěřina	k1gMnSc2	Zvěřina
sice	sice	k8xC	sice
existují	existovat	k5eAaImIp3nP	existovat
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nedovedou	dovést	k5eNaPmIp3nP	dovést
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc1	jaký
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
preferují	preferovat	k5eAaImIp3nP	preferovat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
prý	prý	k9	prý
o	o	k7c4	o
extrémně	extrémně	k6eAd1	extrémně
vzácné	vzácný	k2eAgInPc4d1	vzácný
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
o	o	k7c6	o
bisexualitě	bisexualita	k1gFnSc6	bisexualita
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sexuálním	sexuální	k2eAgNnSc7d1	sexuální
chováním	chování	k1gNnSc7	chování
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
též	též	k9	též
za	za	k7c4	za
bisexuály	bisexuál	k1gMnPc4	bisexuál
označují	označovat	k5eAaImIp3nP	označovat
homosexuálové	homosexuál	k1gMnPc1	homosexuál
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
aby	aby	k9	aby
existenci	existence	k1gFnSc4	existence
své	svůj	k3xOyFgFnSc2	svůj
orientace	orientace	k1gFnSc2	orientace
učinili	učinit	k5eAaPmAgMnP	učinit
sociálně	sociálně	k6eAd1	sociálně
přijatelnější	přijatelný	k2eAgMnPc1d2	přijatelnější
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
proti	proti	k7c3	proti
homosexuálním	homosexuální	k2eAgMnPc3d1	homosexuální
lidem	člověk	k1gMnPc3	člověk
staví	stavit	k5eAaImIp3nS	stavit
nepříliš	příliš	k6eNd1	příliš
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bisexualitu	bisexualita	k1gFnSc4	bisexualita
jako	jako	k8xC	jako
svébytnou	svébytný	k2eAgFnSc4d1	svébytná
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
nepovažují	považovat	k5eNaImIp3nP	považovat
ani	ani	k9	ani
Petr	Petr	k1gMnSc1	Petr
Weiss	Weiss	k1gMnSc1	Weiss
a	a	k8xC	a
Radim	Radim	k1gMnSc1	Radim
Uzel	uzel	k1gInSc1	uzel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Kontinuální	kontinuální	k2eAgFnSc3d1	kontinuální
koncepci	koncepce	k1gFnSc3	koncepce
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xC	jako
bipolární	bipolární	k2eAgFnSc4d1	bipolární
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Alfred	Alfred	k1gMnSc1	Alfred
Kinsey	Kinsea	k1gMnSc2	Kinsea
<g/>
.	.	kIx.	.
</s>
<s>
Heterosexualita	Heterosexualita	k1gFnSc1	Heterosexualita
a	a	k8xC	a
homosexualita	homosexualita	k1gFnSc1	homosexualita
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
stále	stále	k6eAd1	stále
chápány	chápat	k5eAaImNgInP	chápat
jako	jako	k8xS	jako
kategorie	kategorie	k1gFnPc1	kategorie
protikladné	protikladný	k2eAgFnPc1d1	protikladná
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
výlučné	výlučný	k2eAgInPc1d1	výlučný
<g/>
,	,	kIx,	,
ohraničené	ohraničený	k2eAgInPc1d1	ohraničený
a	a	k8xC	a
binární	binární	k2eAgInPc1d1	binární
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
zastávat	zastávat	k5eAaImF	zastávat
různé	různý	k2eAgFnPc4d1	různá
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
i	i	k9	i
bisexuální	bisexuální	k2eAgFnSc1d1	bisexuální
<g/>
.	.	kIx.	.
</s>
<s>
Bisexualita	bisexualita	k1gFnSc1	bisexualita
je	být	k5eAaImIp3nS	být
pojata	pojmout	k5eAaPmNgFnS	pojmout
jako	jako	k9	jako
kombinace	kombinace	k1gFnSc1	kombinace
heterosexuálního	heterosexuální	k2eAgNnSc2d1	heterosexuální
a	a	k8xC	a
homosexuálního	homosexuální	k2eAgNnSc2d1	homosexuální
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k9	jako
jejich	jejich	k3xOp3gFnSc1	jejich
prostřední	prostřední	k2eAgFnSc1d1	prostřední
varianta	varianta	k1gFnSc1	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koncepce	koncepce	k1gFnSc1	koncepce
je	být	k5eAaImIp3nS	být
však	však	k9	však
jednodimenzionální	jednodimenzionální	k2eAgNnSc1d1	jednodimenzionální
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
Kinseyho	Kinsey	k1gMnSc4	Kinsey
stupnici	stupnice	k1gFnSc4	stupnice
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
1	[number]	k4	1
až	až	k9	až
5	[number]	k4	5
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
málo	málo	k6eAd1	málo
sexuálně	sexuálně	k6eAd1	sexuálně
přitahován	přitahován	k2eAgInSc1d1	přitahován
jedním	jeden	k4xCgNnSc7	jeden
pohlavím	pohlaví	k1gNnSc7	pohlaví
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
silně	silně	k6eAd1	silně
přitahován	přitahován	k2eAgInSc1d1	přitahován
druhým	druhý	k4xOgNnSc7	druhý
pohlavím	pohlaví	k1gNnSc7	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
přitom	přitom	k6eAd1	přitom
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
málo	málo	k6eAd1	málo
sexuálně	sexuálně	k6eAd1	sexuálně
přitahován	přitahovat	k5eAaImNgInS	přitahovat
oběma	dva	k4xCgNnPc7	dva
pohlavími	pohlaví	k1gNnPc7	pohlaví
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
silně	silně	k6eAd1	silně
sexuálně	sexuálně	k6eAd1	sexuálně
přitahován	přitahovat	k5eAaImNgInS	přitahovat
oběma	dva	k4xCgNnPc7	dva
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
multidimenzionální	multidimenzionální	k2eAgFnSc2d1	multidimenzionální
koncepce	koncepce	k1gFnSc2	koncepce
nastal	nastat	k5eAaPmAgInS	nastat
významný	významný	k2eAgInSc1d1	významný
posun	posun	k1gInSc1	posun
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
bisexuality	bisexualita	k1gFnSc2	bisexualita
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
komplexnější	komplexní	k2eAgNnSc4d2	komplexnější
zachycení	zachycení	k1gNnSc4	zachycení
lidské	lidský	k2eAgFnSc2d1	lidská
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bisexualitu	bisexualita	k1gFnSc4	bisexualita
je	být	k5eAaImIp3nS	být
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
v	v	k7c6	v
širších	široký	k2eAgFnPc6d2	širší
souvislostech	souvislost	k1gFnPc6	souvislost
a	a	k8xC	a
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
perspektiv	perspektiva	k1gFnPc2	perspektiva
<g/>
/	/	kIx~	/
<g/>
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
například	například	k6eAd1	například
sexuální	sexuální	k2eAgFnSc2d1	sexuální
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
<g/>
,	,	kIx,	,
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
fantazií	fantazie	k1gFnPc2	fantazie
<g/>
,	,	kIx,	,
emocionálních	emocionální	k2eAgFnPc2d1	emocionální
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
preferencí	preference	k1gFnPc2	preference
<g/>
,	,	kIx,	,
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
sebeidentifikace	sebeidentifikace	k1gFnSc2	sebeidentifikace
(	(	kIx(	(
<g/>
těchto	tento	k3xDgFnPc2	tento
sedm	sedm	k4xCc4	sedm
dimenzí	dimenze	k1gFnPc2	dimenze
použil	použít	k5eAaPmAgInS	použít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
modelu	model	k1gInSc6	model
výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
Fritz	Fritz	k1gInSc1	Fritz
Klein	Klein	k1gMnSc1	Klein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
přitom	přitom	k6eAd1	přitom
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
deterministický	deterministický	k2eAgInSc1d1	deterministický
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
třeba	třeba	k6eAd1	třeba
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
bisexuála	bisexuál	k1gMnSc4	bisexuál
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
emocionálních	emocionální	k2eAgFnPc2d1	emocionální
preferencí	preference	k1gFnPc2	preference
mít	mít	k5eAaImF	mít
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
osobám	osoba	k1gFnPc3	osoba
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
žít	žít	k5eAaImF	žít
v	v	k7c6	v
heterosexuálním	heterosexuální	k2eAgInSc6d1	heterosexuální
svazku	svazek	k1gInSc6	svazek
a	a	k8xC	a
být	být	k5eAaImF	být
výrazněji	výrazně	k6eAd2	výrazně
sexuálně	sexuálně	k6eAd1	sexuálně
přitahován	přitahovat	k5eAaImNgMnS	přitahovat
osobami	osoba	k1gFnPc7	osoba
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ortogonální	ortogonální	k2eAgFnSc6d1	ortogonální
koncepci	koncepce	k1gFnSc6	koncepce
jsou	být	k5eAaImIp3nP	být
maskulinita	maskulinit	k2eAgNnPc1d1	maskulinit
a	a	k8xC	a
feminita	feminit	k2eAgNnPc1d1	feminit
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
genderu	gendrat	k5eAaPmIp1nS	gendrat
<g/>
)	)	kIx)	)
a	a	k8xC	a
heterosexualita	heterosexualita	k1gFnSc1	heterosexualita
a	a	k8xC	a
homosexualita	homosexualita	k1gFnSc1	homosexualita
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
)	)	kIx)	)
chápány	chápán	k2eAgFnPc1d1	chápána
jako	jako	k8xC	jako
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
navzájem	navzájem	k6eAd1	navzájem
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
představil	představit	k5eAaPmAgMnS	představit
americký	americký	k2eAgMnSc1d1	americký
psycholog	psycholog	k1gMnSc1	psycholog
Michael	Michael	k1gMnSc1	Michael
Storms	Storms	k1gInSc4	Storms
svoji	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
je	být	k5eAaImIp3nS	být
výslednicí	výslednice	k1gFnSc7	výslednice
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
,	,	kIx,	,
vynesených	vynesený	k2eAgFnPc2d1	vynesená
v	v	k7c6	v
kartézské	kartézský	k2eAgFnSc6d1	kartézská
soustavě	soustava	k1gFnSc6	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
homosexualita	homosexualita	k1gFnSc1	homosexualita
představuje	představovat	k5eAaImIp3nS	představovat
osu	osa	k1gFnSc4	osa
x	x	k?	x
a	a	k8xC	a
heterosexualita	heterosexualita	k1gFnSc1	heterosexualita
osu	osa	k1gFnSc4	osa
y.	y.	k?	y.
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ortogonálním	ortogonální	k2eAgNnSc7d1	ortogonální
pojetím	pojetí	k1gNnSc7	pojetí
označil	označit	k5eAaPmAgMnS	označit
John	John	k1gMnSc1	John
C.	C.	kA	C.
Gonsiorek	Gonsiorek	k1gInSc4	Gonsiorek
bisexualitu	bisexualita	k1gFnSc4	bisexualita
za	za	k7c4	za
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
na	na	k7c6	na
heterosexualitě	heterosexualita	k1gFnSc6	heterosexualita
a	a	k8xC	a
homosexualitě	homosexualita	k1gFnSc6	homosexualita
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontinuální	kontinuální	k2eAgFnSc6d1	kontinuální
koncepci	koncepce	k1gFnSc6	koncepce
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
kombinaci	kombinace	k1gFnSc4	kombinace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgFnSc1d1	sexuální
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
k	k	k7c3	k
jednomu	jeden	k4xCgNnSc3	jeden
pohlaví	pohlaví	k1gNnSc3	pohlaví
nemusí	muset	k5eNaImIp3nS	muset
vylučovat	vylučovat	k5eAaImF	vylučovat
sexuální	sexuální	k2eAgFnSc4d1	sexuální
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
k	k	k7c3	k
druhému	druhý	k4xOgNnSc3	druhý
pohlaví	pohlaví	k1gNnSc3	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
rámci	rámec	k1gInSc6	rámec
definovat	definovat	k5eAaBmF	definovat
jako	jako	k8xS	jako
kombinaci	kombinace	k1gFnSc4	kombinace
individuálního	individuální	k2eAgInSc2d1	individuální
poměru	poměr	k1gInSc2	poměr
heterosexuality	heterosexualita	k1gFnSc2	heterosexualita
a	a	k8xC	a
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
,	,	kIx,	,
představujícího	představující	k2eAgMnSc2d1	představující
dvě	dva	k4xCgNnPc4	dva
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
paralelní	paralelní	k2eAgFnSc2d1	paralelní
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
chápat	chápat	k5eAaImF	chápat
bisexualitu	bisexualita	k1gFnSc4	bisexualita
jako	jako	k8xC	jako
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
a	a	k8xC	a
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Konceptem	koncept	k1gInSc7	koncept
rozvoje	rozvoj	k1gInSc2	rozvoj
bisexuální	bisexuální	k2eAgFnSc2d1	bisexuální
identity	identita	k1gFnSc2	identita
se	se	k3xPyFc4	se
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Francisca	k1gFnSc4	Francisca
zabývali	zabývat	k5eAaImAgMnP	zabývat
Martin	Martin	k1gMnSc1	Martin
Weinberg	Weinberg	k1gMnSc1	Weinberg
<g/>
,	,	kIx,	,
Colin	Colin	k1gMnSc1	Colin
Williams	Williams	k1gInSc1	Williams
a	a	k8xC	a
Douglas	Douglas	k1gMnSc1	Douglas
Pryor	Pryor	k1gMnSc1	Pryor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
představili	představit	k5eAaPmAgMnP	představit
čtyřfázové	čtyřfázový	k2eAgNnSc4d1	čtyřfázové
schéma	schéma	k1gNnSc4	schéma
rozvoje	rozvoj	k1gInSc2	rozvoj
identity	identita	k1gFnSc2	identita
a	a	k8xC	a
coming	coming	k1gInSc1	coming
outu	ouíst	k5eAaPmIp1nS	ouíst
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
počáteční	počáteční	k2eAgNnSc4d1	počáteční
zmatení	zmatení	k1gNnSc4	zmatení
<g/>
,	,	kIx,	,
hledání	hledání	k1gNnSc1	hledání
se	se	k3xPyFc4	se
a	a	k8xC	a
aplikování	aplikování	k1gNnSc1	aplikování
"	"	kIx"	"
<g/>
nálepky	nálepka	k1gFnPc1	nálepka
<g/>
"	"	kIx"	"
bisexuála	bisexuál	k1gMnSc2	bisexuál
<g/>
,	,	kIx,	,
zvykání	zvykání	k1gNnSc1	zvykání
si	se	k3xPyFc3	se
na	na	k7c4	na
identitu	identita	k1gFnSc4	identita
a	a	k8xC	a
pokračující	pokračující	k2eAgFnSc4d1	pokračující
nejistotu	nejistota	k1gFnSc4	nejistota
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
teoretici	teoretik	k1gMnPc1	teoretik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Paula	Paul	k1gMnSc2	Paul
Rodríguez	Rodríguez	k1gMnSc1	Rodríguez
Rus	Rus	k1gMnSc1	Rus
<g/>
)	)	kIx)	)
však	však	k9	však
podobné	podobný	k2eAgInPc1d1	podobný
modely	model	k1gInPc1	model
odmítají	odmítat	k5eAaImIp3nP	odmítat
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
coming	coming	k1gInSc1	coming
out	out	k?	out
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňován	k2eAgInSc1d1	ovlivňován
celým	celý	k2eAgNnSc7d1	celé
množstvím	množství	k1gNnSc7	množství
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
zastoupením	zastoupení	k1gNnSc7	zastoupení
bisexuálních	bisexuální	k2eAgFnPc2d1	bisexuální
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
obtížný	obtížný	k2eAgInSc1d1	obtížný
jednak	jednak	k8xC	jednak
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
problematičnosti	problematičnost	k1gFnSc2	problematičnost
samotné	samotný	k2eAgFnSc2d1	samotná
definice	definice	k1gFnSc2	definice
bisexuality	bisexualita	k1gFnSc2	bisexualita
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
metodického	metodický	k2eAgNnSc2d1	metodické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
rozporuplná	rozporuplný	k2eAgFnSc1d1	rozporuplná
akceptace	akceptace	k1gFnSc1	akceptace
bisexuality	bisexualita	k1gFnSc2	bisexualita
jako	jako	k8xS	jako
samostatné	samostatný	k2eAgFnSc2d1	samostatná
orientace	orientace	k1gFnSc2	orientace
výzkumníky	výzkumník	k1gMnPc7	výzkumník
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvantitativní	kvantitativní	k2eAgInPc1d1	kvantitativní
výzkumy	výzkum	k1gInPc1	výzkum
často	často	k6eAd1	často
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
spíše	spíše	k9	spíše
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
a	a	k8xC	a
bisexuálně	bisexuálně	k6eAd1	bisexuálně
orientovaní	orientovaný	k2eAgMnPc1d1	orientovaný
lidé	člověk	k1gMnPc1	člověk
tak	tak	k6eAd1	tak
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
homo-	homo-	k?	homo-
či	či	k8xC	či
heterosexálních	heterosexální	k2eAgFnPc2d1	heterosexální
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
o	o	k7c6	o
lidech	lid	k1gInPc6	lid
vykazujících	vykazující	k2eAgFnPc2d1	vykazující
obojí	oboj	k1gFnPc2	oboj
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
nelze	lze	k6eNd1	lze
vždy	vždy	k6eAd1	vždy
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
bisexuálně	bisexuálně	k6eAd1	bisexuálně
orientováni	orientován	k2eAgMnPc1d1	orientován
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
jen	jen	k9	jen
o	o	k7c4	o
náhražkové	náhražkový	k2eAgNnSc4d1	náhražkové
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
experimentování	experimentování	k1gNnSc4	experimentování
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
případy	případ	k1gInPc4	případ
nesouvisející	související	k2eNgMnSc1d1	nesouvisející
s	s	k7c7	s
trvalou	trvalý	k2eAgFnSc7d1	trvalá
orientací	orientace	k1gFnSc7	orientace
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výzkumech	výzkum	k1gInPc6	výzkum
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
Zvěřiny	Zvěřina	k1gMnSc2	Zvěřina
a	a	k8xC	a
Weisse	Weiss	k1gMnSc2	Weiss
proto	proto	k8xC	proto
číselné	číselný	k2eAgInPc4d1	číselný
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
bisexuálech	bisexuál	k1gMnPc6	bisexuál
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
až	až	k9	až
1949	[number]	k4	1949
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
provedl	provést	k5eAaPmAgInS	provést
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Alfred	Alfred	k1gMnSc1	Alfred
Kinsey	Kinsea	k1gFnSc2	Kinsea
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
řadu	řada	k1gFnSc4	řada
překvapivých	překvapivý	k2eAgInPc2d1	překvapivý
výsledků	výsledek	k1gInPc2	výsledek
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
bisexualitě	bisexualita	k1gFnSc3	bisexualita
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
37	[number]	k4	37
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
19	[number]	k4	19
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
alespoň	alespoň	k9	alespoň
jednu	jeden	k4xCgFnSc4	jeden
homosexuální	homosexuální	k2eAgFnSc4d1	homosexuální
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Stupněm	stupeň	k1gInSc7	stupeň
3	[number]	k4	3
v	v	k7c6	v
Kinseyho	Kinsey	k1gMnSc2	Kinsey
stupnici	stupnice	k1gFnSc6	stupnice
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jako	jako	k8xC	jako
zcela	zcela	k6eAd1	zcela
bisexuální	bisexuální	k2eAgNnSc1d1	bisexuální
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
11,6	[number]	k4	11,6
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
7	[number]	k4	7
%	%	kIx~	%
svobodných	svobodný	k2eAgFnPc2d1	svobodná
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
4	[number]	k4	4
%	%	kIx~	%
vdaných	vdaný	k2eAgFnPc2d1	vdaná
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
průzkum	průzkum	k1gInSc4	průzkum
provedla	provést	k5eAaPmAgFnS	provést
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
americká	americký	k2eAgFnSc1d1	americká
sexuoložka	sexuoložka	k1gFnSc1	sexuoložka
Shere	Sher	k1gInSc5	Sher
Hite	hit	k1gInSc5	hit
(	(	kIx(	(
<g/>
publikován	publikován	k2eAgMnSc1d1	publikován
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976	[number]	k4	1976
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
bisexuálně	bisexuálně	k6eAd1	bisexuálně
orientované	orientovaný	k2eAgInPc4d1	orientovaný
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
označily	označit	k5eAaPmAgFnP	označit
4	[number]	k4	4
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
13	[number]	k4	13
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc4d1	jiný
průzkum	průzkum	k1gInSc4	průzkum
provedli	provést	k5eAaPmAgMnP	provést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1983	[number]	k4	1983
až	až	k9	až
1992	[number]	k4	1992
manželé	manžel	k1gMnPc1	manžel
Samuel	Samuela	k1gFnPc2	Samuela
a	a	k8xC	a
Cynthia	Cynthium	k1gNnSc2	Cynthium
Janusovi	Janus	k1gMnSc3	Janus
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
závěry	závěr	k1gInPc4	závěr
publikovali	publikovat	k5eAaBmAgMnP	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
bisexuální	bisexuální	k2eAgFnSc3d1	bisexuální
orientaci	orientace	k1gFnSc3	orientace
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
5	[number]	k4	5
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
3	[number]	k4	3
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
dvou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňován	k2eAgFnPc1d1	zmiňována
obdobné	obdobný	k2eAgFnPc1d1	obdobná
metodické	metodický	k2eAgFnPc1d1	metodická
výtky	výtka	k1gFnPc1	výtka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Kinseyho	Kinsey	k1gMnSc4	Kinsey
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
studie	studie	k1gFnSc1	studie
Edwarda	Edward	k1gMnSc2	Edward
Laumanna	Laumann	k1gMnSc2	Laumann
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Gagnona	Gagnon	k1gMnSc2	Gagnon
<g/>
,	,	kIx,	,
Roberta	Robert	k1gMnSc2	Robert
Michaela	Michael	k1gMnSc2	Michael
a	a	k8xC	a
Stuarta	Stuart	k1gMnSc2	Stuart
Michalse	Michals	k1gMnSc2	Michals
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvolila	zvolit	k5eAaPmAgFnS	zvolit
náhodný	náhodný	k2eAgInSc4d1	náhodný
vzorek	vzorek	k1gInSc4	vzorek
respondentů	respondent	k1gMnPc2	respondent
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sexuální	sexuální	k2eAgFnPc1d1	sexuální
identifikace	identifikace	k1gFnPc1	identifikace
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c7	za
heterosexuály	heterosexuál	k1gMnPc7	heterosexuál
(	(	kIx(	(
<g/>
97	[number]	k4	97
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
99	[number]	k4	99
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
a	a	k8xC	a
minimum	minimum	k1gNnSc1	minimum
za	za	k7c7	za
homosexuály	homosexuál	k1gMnPc7	homosexuál
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
sexuálně	sexuálně	k6eAd1	sexuálně
přitahována	přitahován	k2eAgFnSc1d1	přitahována
osobami	osoba	k1gFnPc7	osoba
stejného	stejný	k2eAgInSc2d1	stejný
pohlaví	pohlaví	k1gNnPc2	pohlaví
a	a	k8xC	a
plných	plný	k2eAgNnPc2d1	plné
10	[number]	k4	10
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
4	[number]	k4	4
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
homosexuální	homosexuální	k2eAgFnSc1d1	homosexuální
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
provedlo	provést	k5eAaPmAgNnS	provést
americké	americký	k2eAgNnSc1d1	americké
Národní	národní	k2eAgNnSc1d1	národní
centrum	centrum	k1gNnSc1	centrum
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
statistik	statistika	k1gFnPc2	statistika
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Health	Healtha	k1gFnPc2	Healtha
Statistics	Statisticsa	k1gFnPc2	Statisticsa
<g/>
)	)	kIx)	)
průzkum	průzkum	k1gInSc1	průzkum
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
bisexuálně	bisexuálně	k6eAd1	bisexuálně
orientované	orientovaný	k2eAgNnSc1d1	orientované
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
1,8	[number]	k4	1,8
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
2,8	[number]	k4	2,8
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
až	až	k9	až
44	[number]	k4	44
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
srovnal	srovnat	k5eAaPmAgInS	srovnat
Gary	Gara	k1gFnSc2	Gara
J.	J.	kA	J.
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
demograf	demograf	k1gMnSc1	demograf
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
LGBT	LGBT	kA	LGBT
populaci	populace	k1gFnSc4	populace
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc4	výsledek
devíti	devět	k4xCc2	devět
průzkumů	průzkum	k1gInPc2	průzkum
<g/>
,	,	kIx,	,
provedených	provedený	k2eAgFnPc2d1	provedená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
až	až	k9	až
2010	[number]	k4	2010
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
dospělých	dospělý	k2eAgFnPc2d1	dospělá
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
přihlásily	přihlásit	k5eAaPmAgFnP	přihlásit
k	k	k7c3	k
bisexuální	bisexuální	k2eAgFnSc3d1	bisexuální
orientaci	orientace	k1gFnSc3	orientace
byl	být	k5eAaImAgMnS	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
Hlavním	hlavní	k2eAgInSc7d1	hlavní
symbolem	symbol	k1gInSc7	symbol
bisexuální	bisexuální	k2eAgFnSc2d1	bisexuální
komunity	komunita	k1gFnSc2	komunita
je	být	k5eAaImIp3nS	být
vlajka	vlajka	k1gFnSc1	vlajka
hrdosti	hrdost	k1gFnSc2	hrdost
bisexuálů	bisexuál	k1gMnPc2	bisexuál
či	či	k8xC	či
též	též	k9	též
vlajka	vlajka	k1gFnSc1	vlajka
bisexuální	bisexuální	k2eAgFnSc2d1	bisexuální
hrdosti	hrdost	k1gFnSc2	hrdost
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgNnP	být
představena	představit	k5eAaPmNgNnP	představit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1998	[number]	k4	1998
a	a	k8xC	a
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
barevných	barevný	k2eAgInPc2d1	barevný
horizontálních	horizontální	k2eAgInPc2d1	horizontální
pruhů	pruh	k1gInPc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
pruh	pruh	k1gInSc1	pruh
v	v	k7c6	v
purpurové	purpurový	k2eAgFnSc6d1	purpurová
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc7d1	tvořící
asi	asi	k9	asi
dvě	dva	k4xCgFnPc1	dva
pětiny	pětina	k1gFnPc1	pětina
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
homosexualitě	homosexualita	k1gFnSc3	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgMnSc1d1	spodní
stejně	stejně	k6eAd1	stejně
široký	široký	k2eAgInSc4d1	široký
pruh	pruh	k1gInSc4	pruh
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
královské	královský	k2eAgFnSc2d1	královská
modři	modř	k1gFnSc2	modř
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
heterosexualitě	heterosexualita	k1gFnSc3	heterosexualita
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
prostřední	prostřednět	k5eAaImIp3nP	prostřednět
levandulový	levandulový	k2eAgInSc4d1	levandulový
kruh	kruh	k1gInSc4	kruh
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
zbývající	zbývající	k2eAgFnSc4d1	zbývající
pětinu	pětina	k1gFnSc4	pětina
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
bisexualitě	bisexualita	k1gFnSc3	bisexualita
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
symbolem	symbol	k1gInSc7	symbol
<g/>
,	,	kIx,	,
využívajícím	využívající	k2eAgInSc7d1	využívající
stejné	stejný	k2eAgNnSc4d1	stejné
barevné	barevný	k2eAgNnSc4d1	barevné
schéma	schéma	k1gNnSc4	schéma
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
překrývající	překrývající	k2eAgInPc1d1	překrývající
se	se	k3xPyFc4	se
trojúhelníky	trojúhelník	k1gInPc7	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
barevně	barevně	k6eAd1	barevně
opět	opět	k6eAd1	opět
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
k	k	k7c3	k
homosexualitě	homosexualita	k1gFnSc3	homosexualita
a	a	k8xC	a
heterosexualitě	heterosexualita	k1gFnSc3	heterosexualita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
malý	malý	k2eAgInSc1d1	malý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
překryvem	překryv	k1gInSc7	překryv
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
bisexualitě	bisexualita	k1gFnSc3	bisexualita
<g/>
.	.	kIx.	.
</s>
<s>
Řadě	řada	k1gFnSc3	řada
homosexuálů	homosexuál	k1gMnPc2	homosexuál
a	a	k8xC	a
bisexuálů	bisexuál	k1gMnPc2	bisexuál
však	však	k9	však
symbol	symbol	k1gInSc4	symbol
tohoto	tento	k3xDgInSc2	tento
tvaru	tvar	k1gInSc2	tvar
vadil	vadit	k5eAaImAgInS	vadit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
růžové	růžový	k2eAgFnPc1d1	růžová
trojúhelníkové	trojúhelníkový	k2eAgFnPc1d1	trojúhelníková
nášivky	nášivka	k1gFnPc1	nášivka
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
používány	používat	k5eAaImNgFnP	používat
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
homosexuálů	homosexuál	k1gMnPc2	homosexuál
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
alternativní	alternativní	k2eAgInSc1d1	alternativní
symbol	symbol	k1gInSc1	symbol
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dvou	dva	k4xCgInPc2	dva
bisexuálních	bisexuální	k2eAgInPc2d1	bisexuální
půlměsíců	půlměsíc	k1gInPc2	půlměsíc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
barevně	barevně	k6eAd1	barevně
přecházejí	přecházet	k5eAaImIp3nP	přecházet
z	z	k7c2	z
růžové	růžový	k2eAgFnSc2d1	růžová
v	v	k7c4	v
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
bisexualitu	bisexualita	k1gFnSc4	bisexualita
někdy	někdy	k6eAd1	někdy
používány	používat	k5eAaImNgFnP	používat
různé	různý	k2eAgFnPc1d1	různá
kombinace	kombinace	k1gFnPc1	kombinace
genderových	genderův	k2eAgInPc2d1	genderův
symbolů	symbol	k1gInPc2	symbol
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
symbolů	symbol	k1gInPc2	symbol
existuje	existovat	k5eAaImIp3nS	existovat
Bisexuální	bisexuální	k2eAgInSc4d1	bisexuální
den	den	k1gInSc4	den
hrdosti	hrdost	k1gFnSc2	hrdost
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Celebrate	Celebrat	k1gInSc5	Celebrat
Bisexuality	bisexualita	k1gFnPc1	bisexualita
Day	Day	k1gFnSc7	Day
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
slaven	slavit	k5eAaImNgInS	slavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
ustavení	ustavení	k1gNnSc1	ustavení
bylo	být	k5eAaImAgNnS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
častou	častý	k2eAgFnSc4d1	častá
marginalizaci	marginalizace	k1gFnSc4	marginalizace
bisexuální	bisexuální	k2eAgFnSc2d1	bisexuální
komunity	komunita	k1gFnSc2	komunita
a	a	k8xC	a
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
analýza	analýza	k1gFnSc1	analýza
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
fakticky	fakticky	k6eAd1	fakticky
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
bisexuální	bisexuální	k2eAgFnSc2d1	bisexuální
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
bisexuálních	bisexuální	k2eAgFnPc2d1	bisexuální
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
integruje	integrovat	k5eAaBmIp3nS	integrovat
do	do	k7c2	do
lesbické	lesbický	k2eAgFnSc2d1	lesbická
komunity	komunita	k1gFnSc2	komunita
bez	bez	k7c2	bez
větší	veliký	k2eAgFnSc2d2	veliký
potřeby	potřeba	k1gFnSc2	potřeba
zdůrazňovat	zdůrazňovat	k5eAaImF	zdůrazňovat
svoji	svůj	k3xOyFgFnSc4	svůj
odlišnost	odlišnost	k1gFnSc4	odlišnost
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
bisexuální	bisexuální	k2eAgMnSc1d1	bisexuální
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
organizace	organizace	k1gFnSc1	organizace
či	či	k8xC	či
sekce	sekce	k1gFnSc1	sekce
sdružující	sdružující	k2eAgFnSc1d1	sdružující
bisexuální	bisexuální	k2eAgFnPc4d1	bisexuální
ženy	žena	k1gFnPc4	žena
či	či	k8xC	či
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Bisexuálně	bisexuálně	k6eAd1	bisexuálně
žila	žít	k5eAaImAgFnS	žít
či	či	k8xC	či
se	se	k3xPyFc4	se
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
orientaci	orientace	k1gFnSc3	orientace
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
herců	herec	k1gMnPc2	herec
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
například	například	k6eAd1	například
o	o	k7c4	o
Drew	Drew	k1gMnSc5	Drew
Barrymore	Barrymor	k1gMnSc5	Barrymor
<g/>
,	,	kIx,	,
Marlona	Marlona	k1gFnSc1	Marlona
Brando	Brando	k6eAd1	Brando
<g/>
,	,	kIx,	,
Joan	Joan	k1gMnSc1	Joan
Crawford	Crawford	k1gMnSc1	Crawford
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc1	Jamese
Deana	Deana	k1gFnSc1	Deana
<g/>
,	,	kIx,	,
Marlene	Marlen	k1gInSc5	Marlen
Dietrich	Dietrich	k1gInSc4	Dietrich
<g/>
,	,	kIx,	,
Gretu	Greta	k1gFnSc4	Greta
Garbo	Garba	k1gFnSc5	Garba
<g/>
,	,	kIx,	,
Katharine	Katharin	k1gInSc5	Katharin
Hepburn	Hepburn	k1gNnSc1	Hepburn
<g/>
,	,	kIx,	,
Judy	judo	k1gNnPc7	judo
Garland	Garland	k1gInSc1	Garland
<g/>
,	,	kIx,	,
Angelinu	Angelin	k2eAgFnSc4d1	Angelina
Jolie	Jolie	k1gFnPc4	Jolie
<g/>
,	,	kIx,	,
Amandlu	Amandlo	k1gNnSc3	Amandlo
Stenberg	Stenberg	k1gInSc4	Stenberg
či	či	k8xC	či
Lindsay	Lindsay	k1gInPc4	Lindsay
Lohan	Lohany	k1gInPc2	Lohany
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hudebníků	hudebník	k1gMnPc2	hudebník
třeba	třeba	k6eAd1	třeba
o	o	k7c4	o
Davida	David	k1gMnSc4	David
Bowieho	Bowie	k1gMnSc4	Bowie
<g/>
,	,	kIx,	,
Fergie	Fergie	k1gFnSc1	Fergie
<g/>
,	,	kIx,	,
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
,	,	kIx,	,
Miley	Mile	k2eAgFnPc1d1	Mile
Cyrus	Cyrus	k1gInSc4	Cyrus
nebo	nebo	k8xC	nebo
Keshu	Kesha	k1gFnSc4	Kesha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
umělců	umělec	k1gMnPc2	umělec
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
kupříkladu	kupříkladu	k6eAd1	kupříkladu
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Andyho	Andy	k1gMnSc2	Andy
Warhola	Warhola	k1gFnSc1	Warhola
<g/>
,	,	kIx,	,
dirigenta	dirigent	k1gMnSc2	dirigent
a	a	k8xC	a
skladatele	skladatel	k1gMnSc2	skladatel
Leonarda	Leonardo	k1gMnSc2	Leonardo
Bernsteina	Bernstein	k1gMnSc2	Bernstein
a	a	k8xC	a
malířky	malířka	k1gFnSc2	malířka
Fridu	Frida	k1gFnSc4	Frida
Kahlo	Kahlo	k1gNnSc4	Kahlo
a	a	k8xC	a
Tamaru	Tamara	k1gFnSc4	Tamara
de	de	k?	de
Lempicku	Lempick	k1gInSc2	Lempick
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
spisovateli	spisovatel	k1gMnPc7	spisovatel
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
například	například	k6eAd1	například
o	o	k7c4	o
Williama	Williama	k1gNnSc4	Williama
S.	S.	kA	S.
Burroughse	Burroughs	k1gMnSc2	Burroughs
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
Gordona	Gordon	k1gMnSc2	Gordon
Byrona	Byron	k1gMnSc2	Byron
<g/>
,	,	kIx,	,
Williama	William	k1gMnSc2	William
Somerseta	Somerset	k1gMnSc2	Somerset
Maughama	Maugham	k1gMnSc2	Maugham
<g/>
,	,	kIx,	,
Vitu	vit	k2eAgFnSc4d1	Vita
Sackville-Westovou	Sackville-Westová	k1gFnSc4	Sackville-Westová
či	či	k8xC	či
Virginii	Virginie	k1gFnSc4	Virginie
Woolfovou	Woolfový	k2eAgFnSc4d1	Woolfová
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
o	o	k7c4	o
vědce	vědec	k1gMnSc4	vědec
Alfreda	Alfred	k1gMnSc2	Alfred
Kinseyho	Kinsey	k1gMnSc2	Kinsey
a	a	k8xC	a
módního	módní	k2eAgMnSc2d1	módní
návrháře	návrhář	k1gMnSc2	návrhář
Calvina	Calvin	k2eAgMnSc2d1	Calvin
Kleina	Klein	k1gMnSc2	Klein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
kandidatura	kandidatura	k1gFnSc1	kandidatura
podnikatele	podnikatel	k1gMnSc2	podnikatel
Václava	Václav	k1gMnSc2	Václav
Fischera	Fischer	k1gMnSc2	Fischer
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předvolební	předvolební	k2eAgFnSc6d1	předvolební
kampani	kampaň	k1gFnSc6	kampaň
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
billboardy	billboard	k1gInPc1	billboard
"	"	kIx"	"
<g/>
Homosexuál	homosexuál	k1gMnSc1	homosexuál
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
?	?	kIx.	?
</s>
<s>
Proč	proč	k6eAd1	proč
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Fischer	Fischer	k1gMnSc1	Fischer
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřprocentní	čtyřprocentní	k2eAgFnSc1d1	čtyřprocentní
menšina	menšina	k1gFnSc1	menšina
<g/>
"	"	kIx"	"
a	a	k8xC	a
Fischer	Fischer	k1gMnSc1	Fischer
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Mladou	mladý	k2eAgFnSc4d1	mladá
frontu	fronta	k1gFnSc4	fronta
DNES	dnes	k6eAd1	dnes
reagoval	reagovat	k5eAaBmAgMnS	reagovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Moje	můj	k3xOp1gFnSc1	můj
orientace	orientace	k1gFnSc1	orientace
je	být	k5eAaImIp3nS	být
bohatší	bohatý	k2eAgFnSc1d2	bohatší
<g/>
.	.	kIx.	.
</s>
<s>
Můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
doprovodilo	doprovodit	k5eAaPmAgNnS	doprovodit
mnoho	mnoho	k4c1	mnoho
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
také	také	k9	také
několik	několik	k4yIc1	několik
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
nikdy	nikdy	k6eAd1	nikdy
netajil	tajit	k5eNaImAgMnS	tajit
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Žít	žít	k5eAaImF	žít
v	v	k7c6	v
pravdě	pravda	k1gFnSc6	pravda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
já	já	k3xPp1nSc1	já
bych	by	kYmCp1nS	by
to	ten	k3xDgNnSc1	ten
přál	přát	k5eAaImAgInS	přát
hodně	hodně	k6eAd1	hodně
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
řadě	řada	k1gFnSc6	řada
politiků	politik	k1gMnPc2	politik
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
třeba	třeba	k6eAd1	třeba
s	s	k7c7	s
obdobným	obdobný	k2eAgInSc7d1	obdobný
<g/>
,	,	kIx,	,
řekněme	říct	k5eAaPmRp1nP	říct
neobvyklým	obvyklý	k2eNgNnSc7d1	neobvyklé
bisexuálním	bisexuální	k2eAgNnSc7d1	bisexuální
zaměřením	zaměření	k1gNnSc7	zaměření
procházejí	procházet	k5eAaImIp3nP	procházet
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bisexuální	bisexuální	k2eAgNnSc1d1	bisexuální
chování	chování	k1gNnSc1	chování
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
u	u	k7c2	u
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
o	o	k7c4	o
šimpanze	šimpanz	k1gMnPc4	šimpanz
bonobo	bonoba	k1gFnSc5	bonoba
(	(	kIx(	(
<g/>
Pan	Pan	k1gMnSc1	Pan
paniscus	paniscus	k1gMnSc1	paniscus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
makaky	makak	k1gMnPc4	makak
červenolící	červenolící	k2eAgNnSc1d1	červenolící
(	(	kIx(	(
<g/>
Macaca	Macac	k2eAgFnSc1d1	Macaca
fuscata	fuscata	k1gFnSc1	fuscata
<g/>
)	)	kIx)	)
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
delfínovití	delfínovitý	k2eAgMnPc1d1	delfínovitý
(	(	kIx(	(
<g/>
Delphinidae	Delphinidae	k1gNnSc7	Delphinidae
<g/>
)	)	kIx)	)
–	–	k?	–
například	například	k6eAd1	například
kosatky	kosatka	k1gFnPc1	kosatka
dravé	dravý	k2eAgFnPc1d1	dravá
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
delfíny	delfín	k1gMnPc7	delfín
skákavé	skákavá	k1gFnSc2	skákavá
(	(	kIx(	(
<g/>
Tursiops	Tursiops	k1gInSc1	Tursiops
truncatus	truncatus	k1gInSc1	truncatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
delfíny	delfín	k1gMnPc4	delfín
dlouholebé	dlouholebý	k2eAgNnSc1d1	dlouholebý
(	(	kIx(	(
<g/>
Stenella	Stenella	k1gFnSc1	Stenella
longirostris	longirostris	k1gFnSc2	longirostris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plejtvákovce	plejtvákovec	k1gInPc1	plejtvákovec
šedé	šedý	k2eAgInPc1d1	šedý
(	(	kIx(	(
<g/>
Eschrichtius	Eschrichtius	k1gInSc1	Eschrichtius
robustus	robustus	k1gInSc1	robustus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptactva	ptactvo	k1gNnSc2	ptactvo
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
u	u	k7c2	u
tučňáků	tučňák	k1gMnPc2	tučňák
Humboldtových	Humboldtův	k2eAgMnPc2d1	Humboldtův
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
humboldti	humboldť	k1gFnSc2	humboldť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
ploštěnců	ploštěnec	k1gMnPc2	ploštěnec
(	(	kIx(	(
<g/>
Platyhelminthes	Platyhelminthes	k1gMnSc1	Platyhelminthes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
řady	řada	k1gFnSc2	řada
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc2	vznik
sexuálních	sexuální	k2eAgMnPc2d1	sexuální
a	a	k8xC	a
nesexuálních	sexuální	k2eNgMnPc2d1	sexuální
svazků	svazek	k1gInPc2	svazek
mezi	mezi	k7c7	mezi
jedinci	jedinec	k1gMnPc7	jedinec
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
i	i	k9	i
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
pářit	pářit	k5eAaImF	pářit
se	se	k3xPyFc4	se
s	s	k7c7	s
jedincem	jedinec	k1gMnSc7	jedinec
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
stejnopohlavní	stejnopohlavní	k2eAgInSc4d1	stejnopohlavní
svazek	svazek	k1gInSc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
druhy	druh	k1gInPc4	druh
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
gazely	gazela	k1gFnPc1	gazela
<g/>
,	,	kIx,	,
antilopy	antilopa	k1gFnPc1	antilopa
<g/>
,	,	kIx,	,
bizoni	bizon	k1gMnPc1	bizon
a	a	k8xC	a
tetřívek	tetřívek	k1gMnSc1	tetřívek
pelyňkový	pelyňkový	k2eAgMnSc1d1	pelyňkový
(	(	kIx(	(
<g/>
Centrocercus	Centrocercus	k1gMnSc1	Centrocercus
urophasianus	urophasianus	k1gMnSc1	urophasianus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
literárního	literární	k2eAgNnSc2d1	literární
hlediska	hledisko	k1gNnSc2	hledisko
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
bisexualitou	bisexualita	k1gFnSc7	bisexualita
zabývají	zabývat	k5eAaImIp3nP	zabývat
z	z	k7c2	z
odborného	odborný	k2eAgNnSc2d1	odborné
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
faktu	fakt	k1gInSc2	fakt
a	a	k8xC	a
beletrii	beletrie	k1gFnSc4	beletrie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
čtvrtletně	čtvrtletně	k6eAd1	čtvrtletně
recenzovaný	recenzovaný	k2eAgInSc1d1	recenzovaný
časopis	časopis	k1gInSc1	časopis
Journal	Journal	k1gFnSc2	Journal
of	of	k?	of
Bisexuality	bisexualita	k1gFnSc2	bisexualita
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vydává	vydávat	k5eAaImIp3nS	vydávat
American	American	k1gInSc4	American
Institute	institut	k1gInSc5	institut
of	of	k?	of
Bisexuality	bisexualita	k1gFnSc2	bisexualita
<g/>
.	.	kIx.	.
</s>
<s>
Literaturu	literatura	k1gFnSc4	literatura
faktu	fakt	k1gInSc2	fakt
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
například	například	k6eAd1	například
knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
životě	život	k1gInSc6	život
britské	britský	k2eAgFnSc2d1	britská
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Vity	vit	k2eAgFnPc1d1	Vita
Sackville-Westové	Sackville-Westová	k1gFnPc1	Sackville-Westová
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
její	její	k3xOp3gFnSc1	její
autobiografie	autobiografie	k1gFnSc1	autobiografie
The	The	k1gFnSc1	The
Challenge	Challenge	k1gFnSc1	Challenge
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
biografie	biografie	k1gFnSc1	biografie
Portrait	Portraita	k1gFnPc2	Portraita
of	of	k?	of
a	a	k8xC	a
Marriage	Marriag	k1gFnSc2	Marriag
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
jejím	její	k3xOp3gMnSc7	její
synem	syn	k1gMnSc7	syn
Nigelem	Nigel	k1gMnSc7	Nigel
Nicolsonem	Nicolson	k1gMnSc7	Nicolson
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
již	již	k6eAd1	již
zmíněnou	zmíněný	k2eAgFnSc7d1	zmíněná
Sackville-Westovou	Sackville-Westová	k1gFnSc7	Sackville-Westová
měla	mít	k5eAaImAgFnS	mít
vztah	vztah	k1gInSc4	vztah
významná	významný	k2eAgFnSc1d1	významná
britská	britský	k2eAgFnSc1d1	britská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Virginia	Virginium	k1gNnSc2	Virginium
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
sepsala	sepsat	k5eAaPmAgFnS	sepsat
román	román	k1gInSc4	román
Orlando	Orlanda	k1gFnSc5	Orlanda
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnPc1	její
partnerkou	partnerka	k1gFnSc7	partnerka
inspirovaný	inspirovaný	k2eAgMnSc1d1	inspirovaný
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
dílem	díl	k1gInSc7	díl
Woolfové	Woolfový	k2eAgFnPc4d1	Woolfová
s	s	k7c7	s
bisexuální	bisexuální	k2eAgFnSc7d1	bisexuální
tematikou	tematika	k1gFnSc7	tematika
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
Paní	paní	k1gFnSc1	paní
Dallowayová	Dallowayová	k1gFnSc1	Dallowayová
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
autory	autor	k1gMnPc4	autor
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
například	například	k6eAd1	například
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
spisovatelku	spisovatelka	k1gFnSc4	spisovatelka
Colette	Colett	k1gInSc5	Colett
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
sérii	série	k1gFnSc4	série
novel	novela	k1gFnPc2	novela
s	s	k7c7	s
názvem	název	k1gInSc7	název
Claudine	Claudin	k1gInSc5	Claudin
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
D.	D.	kA	D.
H.	H.	kA	H.
Lawrence	Lawrence	k1gFnSc2	Lawrence
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
knihy	kniha	k1gFnSc2	kniha
Ženy	žena	k1gFnSc2	žena
milující	milující	k2eAgFnSc2d1	milující
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Fox	fox	k1gInSc1	fox
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bisexuální	bisexuální	k2eAgInSc1d1	bisexuální
motiv	motiv	k1gInSc1	motiv
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
románu	román	k1gInSc2	román
Goldfinger	Goldfingra	k1gFnPc2	Goldfingra
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Iana	Ianus	k1gMnSc4	Ianus
Fleminga	Fleming	k1gMnSc4	Fleming
ze	z	k7c2	z
série	série	k1gFnSc2	série
o	o	k7c4	o
Jamesi	Jamese	k1gFnSc4	Jamese
Bondovi	Bonda	k1gMnSc3	Bonda
nebo	nebo	k8xC	nebo
v	v	k7c6	v
detektivním	detektivní	k2eAgInSc6d1	detektivní
románu	román	k1gInSc6	román
Dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
hrála	hrát	k5eAaImAgFnS	hrát
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
švédského	švédský	k2eAgMnSc2d1	švédský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Stiega	Stieg	k1gMnSc2	Stieg
Larssona	Larsson	k1gMnSc2	Larsson
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tematika	tematika	k1gFnSc1	tematika
LGBT	LGBT	kA	LGBT
ve	v	k7c6	v
filmu	film	k1gInSc6	film
a	a	k8xC	a
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Motivy	motiv	k1gInPc1	motiv
bisexuality	bisexualita	k1gFnSc2	bisexualita
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ve	v	k7c6	v
snímcích	snímek	k1gInPc6	snímek
Kabaret	kabaret	k1gInSc1	kabaret
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psí	psí	k2eAgNnSc1d1	psí
odpoledne	odpoledne	k1gNnSc1	odpoledne
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rocky	rock	k1gInPc1	rock
Horror	horror	k1gInSc1	horror
Picture	Pictur	k1gMnSc5	Pictur
Show	show	k1gNnPc1	show
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Caravaggio	Caravaggio	k1gMnSc1	Caravaggio
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
a	a	k8xC	a
June	jun	k1gMnSc5	jun
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Základní	základní	k2eAgInSc1d1	základní
instinkt	instinkt	k1gInSc1	instinkt
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Showgirls	Showgirls	k1gInSc1	Showgirls
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hledám	hledat	k5eAaImIp1nS	hledat
Amy	Amy	k1gFnSc1	Amy
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sametová	sametový	k2eAgFnSc1d1	sametová
extáze	extáze	k1gFnSc1	extáze
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Líbat	líbat	k5eAaImF	líbat
Jessicu	Jessica	k1gFnSc4	Jessica
<g />
.	.	kIx.	.
</s>
<s>
Steinovou	Steinová	k1gFnSc4	Steinová
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frida	Frida	k1gMnSc1	Frida
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zkrocená	zkrocený	k2eAgFnSc1d1	Zkrocená
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shortbus	Shortbus	k1gMnSc1	Shortbus
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
či	či	k8xC	či
Černá	černý	k2eAgFnSc1d1	černá
labuť	labuť	k1gFnSc1	labuť
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
Bi	Bi	k1gMnSc1	Bi
the	the	k?	the
Way	Way	k1gMnSc1	Way
o	o	k7c6	o
bisexualitě	bisexualita	k1gFnSc6	bisexualita
mladých	mladý	k1gMnPc2	mladý
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
filmových	filmový	k2eAgInPc2d1	filmový
festivalů	festival	k1gInPc2	festival
zaměřených	zaměřený	k2eAgInPc2d1	zaměřený
na	na	k7c4	na
filmovou	filmový	k2eAgFnSc4d1	filmová
tvorbu	tvorba	k1gFnSc4	tvorba
s	s	k7c7	s
LGBT	LGBT	kA	LGBT
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
třeba	třeba	k6eAd1	třeba
festival	festival	k1gInSc1	festival
Side	Sid	k1gInSc2	Sid
by	by	kYmCp3nS	by
Side	Side	k1gFnSc1	Side
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
<g/>
,	,	kIx,	,
Vancouver	Vancouver	k1gInSc1	Vancouver
Queer	Queer	k1gInSc1	Queer
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
In	In	k1gMnSc1	In
<g/>
&	&	k?	&
<g/>
Out	Out	k1gMnSc1	Out
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
či	či	k8xC	či
San	San	k1gFnSc6	San
Francisco	Francisco	k6eAd1	Francisco
Pride	Prid	k1gInSc5	Prid
v	v	k7c6	v
San	San	k1gMnSc6	San
Franciscu	Francisc	k1gMnSc6	Francisc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
koná	konat	k5eAaImIp3nS	konat
každoročně	každoročně	k6eAd1	každoročně
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
Mezipatra	mezipatro	k1gNnSc2	mezipatro
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
udělovány	udělovat	k5eAaImNgFnP	udělovat
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
LGBT	LGBT	kA	LGBT
tvorbu	tvorba	k1gFnSc4	tvorba
na	na	k7c6	na
tradičních	tradiční	k2eAgInPc6d1	tradiční
filmových	filmový	k2eAgInPc6d1	filmový
festivalech	festival	k1gInPc6	festival
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ceny	cena	k1gFnSc2	cena
Teddy	Tedda	k1gFnSc2	Tedda
Award	Awarda	k1gFnPc2	Awarda
na	na	k7c6	na
Berlínském	berlínský	k2eAgInSc6d1	berlínský
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
<g/>
,	,	kIx,	,
Queerlion	Queerlion	k1gInSc1	Queerlion
na	na	k7c6	na
Benátském	benátský	k2eAgInSc6d1	benátský
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
nebo	nebo	k8xC	nebo
Queer	Queer	k1gMnSc1	Queer
Palm	Palm	k1gMnSc1	Palm
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
.	.	kIx.	.
</s>
<s>
Bisexuální	bisexuální	k2eAgFnSc1d1	bisexuální
orientace	orientace	k1gFnSc1	orientace
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
třeba	třeba	k6eAd1	třeba
Remy	remy	k1gNnSc4	remy
Hadley	Hadlea	k1gFnSc2	Hadlea
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Třináctka	třináctka	k1gFnSc1	třináctka
<g/>
,	,	kIx,	,
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Alex	Alex	k1gMnSc1	Alex
Kelly	Kella	k1gFnSc2	Kella
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
O.C.	O.C.	k1gMnSc2	O.C.
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
postavy	postava	k1gFnPc1	postava
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Olivia	Olivium	k1gNnPc1	Olivium
Wilde	Wild	k1gMnSc5	Wild
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Callie	Callie	k1gFnSc1	Callie
Torresová	Torresový	k2eAgFnSc1d1	Torresová
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Chirurgové	chirurg	k1gMnPc1	chirurg
či	či	k8xC	či
Angela	Angela	k1gFnSc1	Angela
Montenegro	Montenegro	k1gNnSc1	Montenegro
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Sběratelé	sběratel	k1gMnPc1	sběratel
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
seriálů	seriál	k1gInPc2	seriál
jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c6	o
Dívka	dívka	k1gFnSc1	dívka
odjinud	odjinud	k6eAd1	odjinud
<g/>
,	,	kIx,	,
Torchwood	Torchwooda	k1gFnPc2	Torchwooda
nebo	nebo	k8xC	nebo
Hollyoaks	Hollyoaksa	k1gFnPc2	Hollyoaksa
<g/>
.	.	kIx.	.
</s>
