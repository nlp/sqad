<s>
Romantismus	romantismus	k1gInSc1	romantismus
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
a	a	k8xC	a
filozofický	filozofický	k2eAgInSc4d1	filozofický
směr	směr	k1gInSc4	směr
a	a	k8xC	a
životní	životní	k2eAgInSc4d1	životní
postoj	postoj	k1gInSc4	postoj
euroamerické	euroamerický	k2eAgFnSc2d1	euroamerická
kultury	kultura	k1gFnSc2	kultura
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
kameny	kámen	k1gInPc7	kámen
romantismu	romantismus	k1gInSc2	romantismus
jsou	být	k5eAaImIp3nP	být
cit	cit	k1gInSc1	cit
<g/>
,	,	kIx,	,
individualita	individualita	k1gFnSc1	individualita
(	(	kIx(	(
<g/>
a	a	k8xC	a
individuální	individuální	k2eAgInSc4d1	individuální
prožitek	prožitek	k1gInSc4	prožitek
<g/>
)	)	kIx)	)
a	a	k8xC	a
duše	duše	k1gFnSc1	duše
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
trýzněná	trýzněný	k2eAgFnSc1d1	trýzněná
duše	duše	k1gFnSc1	duše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
monopol	monopol	k1gInSc4	monopol
rozumu	rozum	k1gInSc2	rozum
ve	v	k7c4	v
filozofii	filozofie	k1gFnSc4	filozofie
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
strohost	strohost	k1gFnSc1	strohost
antikou	antika	k1gFnSc7	antika
inspirovaného	inspirovaný	k2eAgInSc2d1	inspirovaný
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Výchozí	výchozí	k2eAgInPc4d1	výchozí
podněty	podnět	k1gInPc4	podnět
hledal	hledat	k5eAaImAgMnS	hledat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
exotických	exotický	k2eAgFnPc6d1	exotická
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
osvícenskému	osvícenský	k2eAgInSc3d1	osvícenský
rozumu	rozum	k1gInSc3	rozum
staví	stavit	k5eAaImIp3nS	stavit
romantismus	romantismus	k1gInSc4	romantismus
často	často	k6eAd1	často
iracionální	iracionální	k2eAgInSc4d1	iracionální
cit	cit	k1gInSc4	cit
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
touze	touha	k1gFnSc3	touha
znát	znát	k5eAaImF	znát
a	a	k8xC	a
poznat	poznat	k5eAaPmF	poznat
touhu	touha	k1gFnSc4	touha
prožít	prožít	k5eAaPmF	prožít
a	a	k8xC	a
zakusit	zakusit	k5eAaPmF	zakusit
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
známému	známý	k2eAgMnSc3d1	známý
a	a	k8xC	a
jasnému	jasný	k2eAgInSc3d1	jasný
mystérium	mystérium	k1gNnSc1	mystérium
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
racionalitě	racionalita	k1gFnSc3	racionalita
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Osvícenský	osvícenský	k2eAgInSc4d1	osvícenský
optimismus	optimismus	k1gInSc4	optimismus
pokroku	pokrok	k1gInSc2	pokrok
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
zoufalství	zoufalství	k1gNnSc1	zoufalství
bezmoci	bezmoc	k1gFnSc2	bezmoc
a	a	k8xC	a
odhodlání	odhodlání	k1gNnSc4	odhodlání
k	k	k7c3	k
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
marné	marný	k2eAgFnSc3d1	marná
<g/>
)	)	kIx)	)
oběti	oběť	k1gFnSc3	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
charakteristiky	charakteristika	k1gFnPc1	charakteristika
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc4d1	typický
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
romantické	romantický	k2eAgNnSc4d1	romantické
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
životní	životní	k2eAgInPc4d1	životní
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
romantismus	romantismus	k1gInSc1	romantismus
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
román	román	k1gInSc1	román
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
označení	označení	k1gNnSc2	označení
literárního	literární	k2eAgInSc2d1	literární
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
hojně	hojně	k6eAd1	hojně
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
psychologické	psychologický	k2eAgInPc1d1	psychologický
(	(	kIx(	(
<g/>
sentimentální	sentimentální	k2eAgInSc1d1	sentimentální
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
či	či	k8xC	či
mystické	mystický	k2eAgNnSc1d1	mystické
(	(	kIx(	(
<g/>
gotický	gotický	k2eAgInSc4d1	gotický
román	román	k1gInSc4	román
<g/>
)	)	kIx)	)
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kdo	kdo	k3yRnSc1	kdo
praví	pravit	k5eAaBmIp3nS	pravit
romantismus	romantismus	k1gInSc4	romantismus
<g/>
,	,	kIx,	,
praví	pravit	k5eAaBmIp3nS	pravit
umění	umění	k1gNnSc4	umění
moderní	moderní	k2eAgFnSc2d1	moderní
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
intimita	intimita	k1gFnSc1	intimita
<g/>
,	,	kIx,	,
duchovost	duchovost	k1gFnSc1	duchovost
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
,	,	kIx,	,
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
umění	umění	k1gNnPc1	umění
vládne	vládnout	k5eAaImIp3nS	vládnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
<g/>
)	)	kIx)	)
Rozvoji	rozvoj	k1gInSc3	rozvoj
romantismu	romantismus	k1gInSc2	romantismus
napomohl	napomoct	k5eAaPmAgInS	napomoct
mj.	mj.	kA	mj.
anglický	anglický	k2eAgInSc1d1	anglický
gotický	gotický	k2eAgInSc1d1	gotický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
vášeň	vášeň	k1gFnSc4	vášeň
pro	pro	k7c4	pro
tajemno	tajemno	k1gNnSc4	tajemno
a	a	k8xC	a
středověk	středověk	k1gInSc4	středověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
německé	německý	k2eAgNnSc1d1	německé
preromantické	preromantický	k2eAgNnSc1d1	preromantický
literární	literární	k2eAgNnSc1d1	literární
hnutí	hnutí	k1gNnSc1	hnutí
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
(	(	kIx(	(
<g/>
Bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
vzdor	vzdor	k1gInSc1	vzdor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
několik	několik	k4yIc1	několik
generací	generace	k1gFnPc2	generace
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
gotický	gotický	k2eAgInSc1d1	gotický
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
módní	módní	k2eAgInSc4d1	módní
dobový	dobový	k2eAgInSc4d1	dobový
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
literární	literární	k2eAgFnSc1d1	literární
kritika	kritika	k1gFnSc1	kritika
zpravidla	zpravidla	k6eAd1	zpravidla
upírá	upírat	k5eAaImIp3nS	upírat
umělecké	umělecký	k2eAgFnSc2d1	umělecká
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
díla	dílo	k1gNnSc2	dílo
hnutí	hnutí	k1gNnSc1	hnutí
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
čelnými	čelný	k2eAgMnPc7d1	čelný
představili	představit	k5eAaPmAgMnP	představit
byli	být	k5eAaImAgMnP	být
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
čtenáři	čtenář	k1gMnPc1	čtenář
i	i	k8xC	i
kritikou	kritika	k1gFnSc7	kritika
ceněna	cenit	k5eAaImNgFnS	cenit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1	gotický
román	román	k1gInSc1	román
působil	působit	k5eAaImAgInS	působit
na	na	k7c4	na
představivost	představivost	k1gFnSc4	představivost
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
vyvolat	vyvolat	k5eAaPmF	vyvolat
bázeň	bázeň	k1gFnSc4	bázeň
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
tajemné	tajemný	k2eAgNnSc1d1	tajemné
napětí	napětí	k1gNnSc1	napětí
(	(	kIx(	(
<g/>
typické	typický	k2eAgInPc1d1	typický
jsou	být	k5eAaImIp3nP	být
motivy	motiv	k1gInPc1	motiv
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
prokletí	prokletí	k1gNnPc2	prokletí
a	a	k8xC	a
polozpustlých	polozpustlý	k2eAgInPc2d1	polozpustlý
hradů	hrad	k1gInPc2	hrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
častá	častý	k2eAgFnSc1d1	častá
však	však	k9	však
byla	být	k5eAaImAgFnS	být
i	i	k9	i
milostná	milostný	k2eAgFnSc1d1	milostná
zápletka	zápletka	k1gFnSc1	zápletka
akcentující	akcentující	k2eAgFnSc2d1	akcentující
city	city	k1gFnSc2	city
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
hnutí	hnutí	k1gNnSc1	hnutí
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Dranga	k1gFnPc2	Dranga
zpracovávalo	zpracovávat	k5eAaImAgNnS	zpracovávat
city	cit	k1gInPc4	cit
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
například	například	k6eAd1	například
ztotožnila	ztotožnit	k5eAaPmAgFnS	ztotožnit
s	s	k7c7	s
titulním	titulní	k2eAgMnSc7d1	titulní
hrdinou	hrdina	k1gMnSc7	hrdina
Goethova	Goethův	k2eAgInSc2d1	Goethův
románu	román	k1gInSc2	román
Utrpení	utrpení	k1gNnSc1	utrpení
mladého	mladý	k2eAgNnSc2d1	mladé
Werthera	Werthero	k1gNnSc2	Werthero
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
příkladu	příklad	k1gInSc2	příklad
odívat	odívat	k5eAaImF	odívat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Werthertracht	Werthertracht	k1gInSc1	Werthertracht
<g/>
,	,	kIx,	,
wertherovská	wertherovský	k2eAgFnSc1d1	wertherovský
móda	móda	k1gFnSc1	móda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
chlapci	chlapec	k1gMnPc1	chlapec
a	a	k8xC	a
dívky	dívka	k1gFnPc1	dívka
dokonce	dokonce	k9	dokonce
spáchali	spáchat	k5eAaPmAgMnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Wertherův	Wertherův	k2eAgInSc1d1	Wertherův
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
vzoru	vzor	k1gInSc2	vzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podmanivost	podmanivost	k1gFnSc1	podmanivost
Werherovy	Werherův	k2eAgFnSc2d1	Werherův
postavy	postava	k1gFnSc2	postava
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Goethe	Goethe	k1gFnSc1	Goethe
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
emancipoval	emancipovat	k5eAaBmAgMnS	emancipovat
citové	citový	k2eAgFnPc4d1	citová
složky	složka	k1gFnPc4	složka
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Romantické	romantický	k2eAgFnPc1d1	romantická
ideje	idea	k1gFnPc1	idea
se	se	k3xPyFc4	se
masově	masově	k6eAd1	masově
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
v	v	k7c6	v
období	období	k1gNnSc6	období
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
obdobím	období	k1gNnSc7	období
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
sporů	spor	k1gInPc2	spor
podařilo	podařit	k5eAaPmAgNnS	podařit
urovnat	urovnat	k5eAaPmF	urovnat
diplomatickou	diplomatický	k2eAgFnSc7d1	diplomatická
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc4d1	poslední
celoevropský	celoevropský	k2eAgInSc4d1	celoevropský
konflikt	konflikt	k1gInSc4	konflikt
byla	být	k5eAaImAgFnS	být
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
<g/>
;	;	kIx,	;
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nicméně	nicméně	k8xC	nicméně
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
méně	málo	k6eAd2	málo
významné	významný	k2eAgInPc4d1	významný
válečné	válečný	k2eAgInPc4d1	válečný
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
či	či	k8xC	či
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
kontinentu	kontinent	k1gInSc2	kontinent
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
nejprve	nejprve	k6eAd1	nejprve
tzv.	tzv.	kA	tzv.
revolučními	revoluční	k2eAgFnPc7d1	revoluční
válkami	válka	k1gFnPc7	válka
(	(	kIx(	(
<g/>
války	válka	k1gFnPc1	válka
s	s	k7c7	s
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
válkami	válka	k1gFnPc7	válka
napoleonskými	napoleonský	k2eAgFnPc7d1	napoleonská
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnPc1d1	válečná
hrůzy	hrůza	k1gFnPc1	hrůza
<g/>
,	,	kIx,	,
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
i	i	k8xC	i
potřeba	potřeba	k1gFnSc1	potřeba
silných	silný	k2eAgFnPc2d1	silná
individualit	individualita	k1gFnPc2	individualita
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
Horatio	Horatio	k1gMnSc1	Horatio
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Kutuzov	Kutuzov	k1gInSc1	Kutuzov
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
rozněcovaly	rozněcovat	k5eAaImAgInP	rozněcovat
fantazii	fantazie	k1gFnSc4	fantazie
umělců	umělec	k1gMnPc2	umělec
(	(	kIx(	(
<g/>
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
malířů	malíř	k1gMnPc2	malíř
i	i	k8xC	i
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
např.	např.	kA	např.
Ludwiga	Ludwiga	k1gFnSc1	Ludwiga
van	vana	k1gFnPc2	vana
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
)	)	kIx)	)
i	i	k8xC	i
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonské	napoleonský	k2eAgFnPc1d1	napoleonská
války	válka	k1gFnPc1	válka
zároveň	zároveň	k6eAd1	zároveň
podněcovaly	podněcovat	k5eAaImAgFnP	podněcovat
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
romantismu	romantismus	k1gInSc2	romantismus
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
díky	díky	k7c3	díky
osvícenským	osvícenský	k2eAgFnPc3d1	osvícenská
reformám	reforma	k1gFnPc3	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
kupní	kupní	k2eAgFnSc1d1	kupní
síla	síla	k1gFnSc1	síla
středních	střední	k2eAgFnPc2d1	střední
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
počátkem	počátek	k1gInSc7	počátek
liberalizace	liberalizace	k1gFnSc2	liberalizace
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
měšťané	měšťan	k1gMnPc1	měšťan
začali	začít	k5eAaPmAgMnP	začít
intenzivněji	intenzivně	k6eAd2	intenzivně
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
kulturním	kulturní	k2eAgInSc6d1	kulturní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mohli	moct	k5eAaImAgMnP	moct
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
vyžití	vyžití	k1gNnSc2	vyžití
<g/>
.	.	kIx.	.
</s>
<s>
Aristokracie	aristokracie	k1gFnSc1	aristokracie
reagovala	reagovat	k5eAaBmAgFnS	reagovat
na	na	k7c4	na
emancipaci	emancipace	k1gFnSc4	emancipace
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
vůči	vůči	k7c3	vůči
novým	nový	k2eAgInPc3d1	nový
společenským	společenský	k2eAgInPc3d1	společenský
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
již	již	k6eAd1	již
nenalezneme	nalézt	k5eNaBmIp1nP	nalézt
téměř	téměř	k6eAd1	téměř
žádného	žádný	k3yNgMnSc4	žádný
šlechtického	šlechtický	k2eAgMnSc4d1	šlechtický
spisovatele	spisovatel	k1gMnSc4	spisovatel
či	či	k8xC	či
filosofa	filosof	k1gMnSc4	filosof
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
domény	doména	k1gFnPc4	doména
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
;	;	kIx,	;
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
buržoazie	buržoazie	k1gFnSc1	buržoazie
šlechtě	šlechta	k1gFnSc3	šlechta
konkurovat	konkurovat	k5eAaImF	konkurovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
polovina	polovina	k1gFnSc1	polovina
a	a	k8xC	a
část	část	k1gFnSc1	část
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byly	být	k5eAaImAgFnP	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
objevil	objevit	k5eAaPmAgMnS	objevit
císařský	císařský	k2eAgInSc4d1	císařský
styl	styl	k1gInSc4	styl
empír	empír	k1gInSc1	empír
a	a	k8xC	a
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
měšťanský	měšťanský	k2eAgInSc4d1	měšťanský
styl	styl	k1gInSc4	styl
biedermeier	biedermeier	k1gInSc1	biedermeier
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
skromnějším	skromný	k2eAgMnSc7d2	skromnější
<g/>
,	,	kIx,	,
měšťanským	měšťanský	k2eAgMnSc7d1	měšťanský
zástupcem	zástupce	k1gMnSc7	zástupce
empíru	empír	k1gInSc2	empír
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
knihách	kniha	k1gFnPc6	kniha
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
jejich	jejich	k3xOp3gFnSc3	jejich
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
produkci	produkce	k1gFnSc3	produkce
-	-	kIx~	-
co	co	k3yInSc1	co
do	do	k7c2	do
spisovatelů	spisovatel	k1gMnPc2	spisovatel
i	i	k8xC	i
knihtiskařů	knihtiskař	k1gMnPc2	knihtiskař
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
i	i	k9	i
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
spotřebním	spotřební	k2eAgNnSc6d1	spotřební
zboží	zboží	k1gNnSc6	zboží
<g/>
,	,	kIx,	,
nábytku	nábytek	k1gInSc2	nábytek
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nebývalý	bývalý	k2eNgInSc4d1	bývalý
rozmach	rozmach	k1gInSc4	rozmach
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
porcelánu	porcelán	k1gInSc2	porcelán
a	a	k8xC	a
keramiky	keramika	k1gFnSc2	keramika
<g/>
,	,	kIx,	,
nábytkářství	nábytkářství	k1gNnSc2	nábytkářství
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
poptávka	poptávka	k1gFnSc1	poptávka
zpětně	zpětně	k6eAd1	zpětně
stimulovala	stimulovat	k5eAaImAgFnS	stimulovat
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovlivňovalo	ovlivňovat	k5eAaImAgNnS	ovlivňovat
průběh	průběh	k1gInSc4	průběh
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
kultura	kultura	k1gFnSc1	kultura
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
samostatným	samostatný	k2eAgInSc7d1	samostatný
pojmem	pojem	k1gInSc7	pojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
především	především	k6eAd1	především
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
národním	národní	k2eAgNnSc7d1	národní
obrozením	obrození	k1gNnSc7	obrození
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
s	s	k7c7	s
biedermeierem	biedermeier	k1gInSc7	biedermeier
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
od	od	k7c2	od
romantismu	romantismus	k1gInSc2	romantismus
stala	stát	k5eAaPmAgFnS	stát
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
zálibu	zálib	k1gInSc2	zálib
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
už	už	k6eAd1	už
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
např.	např.	kA	např.
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
hudebního	hudební	k2eAgInSc2d1	hudební
nástroje	nástroj	k1gInSc2	nástroj
a	a	k8xC	a
dovednost	dovednost	k1gFnSc4	dovednost
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
staly	stát	k5eAaPmAgInP	stát
otázkou	otázka	k1gFnSc7	otázka
prestiže	prestiž	k1gFnSc2	prestiž
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
statusu	status	k1gInSc2	status
a	a	k8xC	a
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgFnPc4d1	bohatá
měšťanské	měšťanský	k2eAgFnPc4d1	měšťanská
dívky	dívka	k1gFnPc4	dívka
součástí	součást	k1gFnPc2	součást
povinné	povinný	k2eAgFnSc2d1	povinná
výchovy	výchova	k1gFnSc2	výchova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Romantici	romantik	k1gMnPc1	romantik
žijí	žít	k5eAaImIp3nP	žít
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
idejí	idea	k1gFnPc2	idea
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
společnosti	společnost	k1gFnPc1	společnost
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
uspořádáním	uspořádání	k1gNnSc7	uspořádání
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
toulají	toulat	k5eAaImIp3nP	toulat
<g/>
,	,	kIx,	,
chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
nezvykle	zvykle	k6eNd1	zvykle
apod.	apod.	kA	apod.
Romantismus	romantismus	k1gInSc1	romantismus
našel	najít	k5eAaPmAgInS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
snažili	snažit	k5eAaImAgMnP	snažit
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
duchovní	duchovní	k2eAgFnSc4d1	duchovní
náplň	náplň	k1gFnSc4	náplň
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
melancholii	melancholie	k1gFnSc4	melancholie
<g/>
,	,	kIx,	,
stesk	stesk	k1gInSc4	stesk
(	(	kIx(	(
<g/>
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
termín	termín	k1gInSc4	termín
spleen	spleen	k1gInSc1	spleen
-	-	kIx~	-
trudomyslnost	trudomyslnost	k1gFnSc1	trudomyslnost
<g/>
,	,	kIx,	,
smutek	smutek	k1gInSc1	smutek
<g/>
)	)	kIx)	)
a	a	k8xC	a
boj	boj	k1gInSc4	boj
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
proti	proti	k7c3	proti
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Malíři	malíř	k1gMnPc1	malíř
zobrazovali	zobrazovat	k5eAaImAgMnP	zobrazovat
ponuré	ponurý	k2eAgFnPc4d1	ponurá
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
ruiny	ruina	k1gFnPc4	ruina
<g/>
,	,	kIx,	,
bouří	bouřit	k5eAaImIp3nS	bouřit
stižené	stižený	k2eAgInPc4d1	stižený
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Osamocení	osamocený	k2eAgMnPc1d1	osamocený
hrdinové	hrdina	k1gMnPc1	hrdina
(	(	kIx(	(
<g/>
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
i	i	k8xC	i
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
)	)	kIx)	)
trávili	trávit	k5eAaImAgMnP	trávit
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
v	v	k7c6	v
panenské	panenský	k2eAgFnSc6d1	panenská
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
či	či	k8xC	či
byli	být	k5eAaImAgMnP	být
zmítáni	zmítat	k5eAaImNgMnP	zmítat
delirii	delirium	k1gNnPc7	delirium
a	a	k8xC	a
vášněmi	vášeň	k1gFnPc7	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
využívá	využívat	k5eAaPmIp3nS	využívat
lidových	lidový	k2eAgInPc2d1	lidový
motivů	motiv	k1gInPc2	motiv
i	i	k8xC	i
snových	snový	k2eAgInPc2d1	snový
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
motivy	motiv	k1gInPc1	motiv
byly	být	k5eAaImAgInP	být
osamocení	osamocení	k1gNnSc4	osamocení
<g/>
,	,	kIx,	,
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
stesk	stesk	k1gInSc4	stesk
či	či	k8xC	či
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berliozově	Berliozův	k2eAgFnSc6d1	Berliozova
Fantastické	fantastický	k2eAgFnSc6d1	fantastická
symfonii	symfonie	k1gFnSc6	symfonie
<g/>
,	,	kIx,	,
Máchově	Máchův	k2eAgInSc6d1	Máchův
Máji	máj	k1gInSc6	máj
či	či	k8xC	či
na	na	k7c6	na
obrazu	obraz	k1gInSc6	obraz
popravy	poprava	k1gFnSc2	poprava
Lady	Lada	k1gFnSc2	Lada
Jane	Jan	k1gMnSc5	Jan
Grey	Gre	k1gMnPc7	Gre
Paula	Paul	k1gMnSc2	Paul
Delaroche	Delaroch	k1gMnSc2	Delaroch
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
obraz	obraz	k1gInSc4	obraz
popravy	poprava	k1gFnSc2	poprava
zachycený	zachycený	k2eAgInSc4d1	zachycený
výtvarnými	výtvarný	k2eAgInPc7d1	výtvarný
<g/>
,	,	kIx,	,
hudebními	hudební	k2eAgInPc7d1	hudební
či	či	k8xC	či
literárními	literární	k2eAgInPc7d1	literární
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Romantismus	romantismus	k1gInSc1	romantismus
(	(	kIx(	(
<g/>
literatura	literatura	k1gFnSc1	literatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc4d1	široké
umělecké	umělecký	k2eAgNnSc4d1	umělecké
hnutí	hnutí	k1gNnSc4	hnutí
především	především	k9	především
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
umělecký	umělecký	k2eAgInSc4d1	umělecký
protiklad	protiklad	k1gInSc4	protiklad
klasicismu	klasicismus	k1gInSc2	klasicismus
a	a	k8xC	a
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
hluboký	hluboký	k2eAgInSc4d1	hluboký
rozpor	rozpor	k1gInSc4	rozpor
mezi	mezi	k7c7	mezi
realitou	realita	k1gFnSc7	realita
a	a	k8xC	a
humanistickými	humanistický	k2eAgInPc7d1	humanistický
ideály	ideál	k1gInPc7	ideál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
proklamovala	proklamovat	k5eAaBmAgFnS	proklamovat
Velká	velký	k2eAgFnSc1d1	velká
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
jméno	jméno	k1gNnSc1	jméno
dostalo	dostat	k5eAaPmAgNnS	dostat
od	od	k7c2	od
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
moderního	moderní	k2eAgInSc2d1	moderní
slovesného	slovesný	k2eAgInSc2d1	slovesný
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
obraznost	obraznost	k1gFnSc1	obraznost
a	a	k8xC	a
citovost	citovost	k1gFnSc1	citovost
převládala	převládat	k5eAaImAgFnS	převládat
nad	nad	k7c7	nad
rozumovostí	rozumovost	k1gFnSc7	rozumovost
<g/>
.	.	kIx.	.
</s>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jako	jako	k9	jako
výraz	výraz	k1gInSc4	výraz
nových	nový	k2eAgInPc2d1	nový
sociálně	sociálně	k6eAd1	sociálně
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
skutečností	skutečnost	k1gFnPc2	skutečnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
odrážel	odrážet	k5eAaImAgMnS	odrážet
prohlubování	prohlubování	k1gNnSc4	prohlubování
protikladů	protiklad	k1gInPc2	protiklad
mezi	mezi	k7c7	mezi
individuem	individuum	k1gNnSc7	individuum
a	a	k8xC	a
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
ideálem	ideál	k1gInSc7	ideál
a	a	k8xC	a
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
uměním	umění	k1gNnSc7	umění
a	a	k8xC	a
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
kolébku	kolébka	k1gFnSc4	kolébka
literárního	literární	k2eAgInSc2d1	literární
romantismu	romantismus	k1gInSc2	romantismus
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
Anglie	Anglie	k1gFnSc1	Anglie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
jezerní	jezerní	k2eAgMnPc1d1	jezerní
básníci	básník	k1gMnPc1	básník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc4	jeho
počátky	počátek	k1gInPc4	počátek
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
již	již	k9	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastupující	nastupující	k2eAgFnSc1d1	nastupující
sociální	sociální	k2eAgFnSc1d1	sociální
třída	třída	k1gFnSc1	třída
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
začala	začít	k5eAaPmAgFnS	začít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
svůj	svůj	k3xOyFgInSc4	svůj
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
chladnému	chladný	k2eAgNnSc3d1	chladné
rozumářství	rozumářství	k1gNnSc3	rozumářství
klasicismu	klasicismus	k1gInSc2	klasicismus
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
uměleckému	umělecký	k2eAgInSc3d1	umělecký
směru	směr	k1gInSc3	směr
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
)	)	kIx)	)
a	a	k8xC	a
proti	proti	k7c3	proti
stavovskému	stavovský	k2eAgNnSc3d1	Stavovské
feudálnímu	feudální	k2eAgNnSc3d1	feudální
zřízení	zřízení	k1gNnSc3	zřízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
jí	on	k3xPp3gFnSc3	on
neposkytovalo	poskytovat	k5eNaImAgNnS	poskytovat
možnost	možnost	k1gFnSc4	možnost
plného	plný	k2eAgNnSc2d1	plné
uplatnění	uplatnění	k1gNnSc2	uplatnění
a	a	k8xC	a
všestranného	všestranný	k2eAgInSc2d1	všestranný
svobodného	svobodný	k2eAgInSc2d1	svobodný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
preromantismus	preromantismus	k1gInSc1	preromantismus
nebo	nebo	k8xC	nebo
sentimentalismus	sentimentalismus	k1gInSc1	sentimentalismus
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
orientaci	orientace	k1gFnSc4	orientace
na	na	k7c4	na
posílení	posílení	k1gNnSc4	posílení
emocionální	emocionální	k2eAgFnSc2d1	emocionální
působivosti	působivost	k1gFnSc2	působivost
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Romantická	romantický	k2eAgNnPc1d1	romantické
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
vznikala	vznikat	k5eAaImAgNnP	vznikat
i	i	k9	i
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vůdčím	vůdčí	k2eAgInSc7d1	vůdčí
literárním	literární	k2eAgInSc7d1	literární
proudem	proud	k1gInSc7	proud
kritický	kritický	k2eAgInSc1d1	kritický
realismus	realismus	k1gInSc1	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
pozdním	pozdní	k2eAgInSc6d1	pozdní
romantismu	romantismus	k1gInSc6	romantismus
nebo	nebo	k8xC	nebo
o	o	k7c6	o
novoromantismu	novoromantismus	k1gInSc6	novoromantismus
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
termínu	termín	k1gInSc2	termín
novoromantismus	novoromantismus	k1gInSc1	novoromantismus
není	být	k5eNaImIp3nS	být
však	však	k9	však
ustálené	ustálený	k2eAgNnSc1d1	ustálené
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
literárního	literární	k2eAgInSc2d1	literární
proudu	proud	k1gInSc2	proud
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
představitelé	představitel	k1gMnPc1	představitel
většinou	většinou	k6eAd1	většinou
epigonsky	epigonsky	k6eAd1	epigonsky
a	a	k8xC	a
vyumělkovaně	vyumělkovaně	k6eAd1	vyumělkovaně
napodobovali	napodobovat	k5eAaImAgMnP	napodobovat
složitý	složitý	k2eAgInSc4d1	složitý
a	a	k8xC	a
archaistický	archaistický	k2eAgInSc4d1	archaistický
sloh	sloh	k1gInSc4	sloh
pozdního	pozdní	k2eAgInSc2d1	pozdní
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
obnovili	obnovit	k5eAaPmAgMnP	obnovit
tím	ten	k3xDgNnSc7	ten
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
romantickou	romantický	k2eAgFnSc4d1	romantická
tajemnost	tajemnost	k1gFnSc4	tajemnost
<g/>
,	,	kIx,	,
zešeřelost	zešeřelost	k1gFnSc4	zešeřelost
a	a	k8xC	a
světobol	světobol	k1gInSc4	světobol
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
kult	kult	k1gInSc4	kult
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
katolictví	katolictví	k1gNnSc2	katolictví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mystiku	mystika	k1gFnSc4	mystika
a	a	k8xC	a
morbidnost	morbidnost	k1gFnSc4	morbidnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Romantismus	romantismus	k1gInSc1	romantismus
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Romantická	romantický	k2eAgFnSc1d1	romantická
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
z	z	k7c2	z
hudebního	hudební	k2eAgInSc2d1	hudební
klasicismu	klasicismus	k1gInSc2	klasicismus
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
impulsů	impuls	k1gInPc2	impuls
uměleckého	umělecký	k2eAgInSc2d1	umělecký
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
dominoval	dominovat	k5eAaImAgInS	dominovat
až	až	k6eAd1	až
do	do	k7c2	do
prvních	první	k4xOgNnPc2	první
desetiletí	desetiletí	k1gNnPc2	desetiletí
století	století	k1gNnSc2	století
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
<g/>
.	.	kIx.	.
</s>
<s>
Romantická	romantický	k2eAgFnSc1d1	romantická
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
zdůrazněnou	zdůrazněný	k2eAgFnSc7d1	zdůrazněná
emocionalitou	emocionalita	k1gFnSc7	emocionalita
<g/>
,	,	kIx,	,
rozšířením	rozšíření	k1gNnSc7	rozšíření
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
i	i	k9	i
překročením	překročení	k1gNnSc7	překročení
tradiční	tradiční	k2eAgFnSc2d1	tradiční
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
,	,	kIx,	,
silnějším	silný	k2eAgNnSc7d2	silnější
přijímáním	přijímání	k1gNnSc7	přijímání
impulsů	impuls	k1gInPc2	impuls
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
častým	častý	k2eAgNnSc7d1	časté
spojováním	spojování	k1gNnSc7	spojování
hudby	hudba	k1gFnSc2	hudba
s	s	k7c7	s
mimohudebními	mimohudební	k2eAgInPc7d1	mimohudební
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
literárními	literární	k2eAgFnPc7d1	literární
myšlenkami	myšlenka	k1gFnPc7	myšlenka
(	(	kIx(	(
<g/>
programní	programní	k2eAgFnSc1d1	programní
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
neustále	neustále	k6eAd1	neustále
rozšiřován	rozšiřován	k2eAgMnSc1d1	rozšiřován
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnil	umožnit	k5eAaPmAgInS	umožnit
jak	jak	k6eAd1	jak
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
stále	stále	k6eAd1	stále
jemnější	jemný	k2eAgInPc4d2	jemnější
citové	citový	k2eAgInPc4d1	citový
odstíny	odstín	k1gInPc4	odstín
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
vyvolat	vyvolat	k5eAaPmF	vyvolat
mohutný	mohutný	k2eAgInSc4d1	mohutný
dojem	dojem	k1gInSc4	dojem
při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
nasazení	nasazení	k1gNnSc6	nasazení
všech	všecek	k3xTgInPc2	všecek
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
české	český	k2eAgMnPc4d1	český
skladatele	skladatel	k1gMnPc4	skladatel
hudebního	hudební	k2eAgInSc2d1	hudební
romantismu	romantismus	k1gInSc2	romantismus
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
mj.	mj.	kA	mj.
cyklu	cyklus	k1gInSc3	cyklus
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
složil	složit	k5eAaPmAgInS	složit
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
Polednice	polednice	k1gFnSc2	polednice
<g/>
,	,	kIx,	,
Vodník	vodník	k1gMnSc1	vodník
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kolovrat	kolovrat	k1gInSc1	kolovrat
a	a	k8xC	a
Holoubek	Holoubek	k1gMnSc1	Holoubek
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Erbenových	Erbenových	k2eAgFnPc2d1	Erbenových
balad	balada	k1gFnPc2	balada
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Romantická	romantický	k2eAgFnSc1d1	romantická
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zásadně	zásadně	k6eAd1	zásadně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
nostalgickým	nostalgický	k2eAgNnSc7d1	nostalgické
zaměřením	zaměření	k1gNnSc7	zaměření
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
na	na	k7c4	na
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Architekti	architekt	k1gMnPc1	architekt
se	se	k3xPyFc4	se
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
dílu	díl	k1gInSc6	díl
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
stavbami	stavba	k1gFnPc7	stavba
jiných	jiný	k2eAgFnPc2d1	jiná
historických	historický	k2eAgFnPc2d1	historická
epoch	epocha	k1gFnPc2	epocha
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Romantická	romantický	k2eAgFnSc1d1	romantická
architektura	architektura	k1gFnSc1	architektura
jako	jako	k8xC	jako
svébytný	svébytný	k2eAgInSc1d1	svébytný
styl	styl	k1gInSc1	styl
prakticky	prakticky	k6eAd1	prakticky
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dána	dát	k5eAaPmNgFnS	dát
slohem	sloh	k1gInSc7	sloh
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
stavba	stavba	k1gFnSc1	stavba
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
slohem	sloh	k1gInSc7	sloh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
býval	bývat	k5eAaImAgInS	bývat
nejdříve	dříve	k6eAd3	dříve
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
napodobován	napodobován	k2eAgInSc1d1	napodobován
byla	být	k5eAaImAgFnS	být
gotika	gotika	k1gFnSc1	gotika
<g/>
,	,	kIx,	,
takovéto	takovýto	k3xDgFnPc4	takovýto
stavby	stavba	k1gFnPc4	stavba
či	či	k8xC	či
stavební	stavební	k2eAgFnPc4d1	stavební
úpravy	úprava	k1gFnPc4	úprava
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
novogotické	novogotický	k2eAgFnPc1d1	novogotická
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
hlavně	hlavně	k9	hlavně
vznikem	vznik	k1gInSc7	vznik
veřejných	veřejný	k2eAgInPc2d1	veřejný
sadů	sad	k1gInPc2	sad
-	-	kIx~	-
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
anglické	anglický	k2eAgNnSc1d1	anglické
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
parky	park	k1gInPc1	park
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
budit	budit	k5eAaImF	budit
dojem	dojem	k1gInSc4	dojem
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
instalovány	instalovat	k5eAaBmNgFnP	instalovat
restaurace	restaurace	k1gFnPc1	restaurace
<g/>
,	,	kIx,	,
letohrádky	letohrádek	k1gInPc1	letohrádek
<g/>
,	,	kIx,	,
altány	altán	k1gInPc1	altán
a	a	k8xC	a
umělé	umělý	k2eAgFnPc1d1	umělá
zříceniny	zřícenina	k1gFnPc1	zřícenina
<g/>
.	.	kIx.	.
</s>
<s>
Umělecký	umělecký	k2eAgInSc1d1	umělecký
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
nosily	nosit	k5eAaImAgFnP	nosit
rozevláté	rozevlátý	k2eAgFnPc1d1	rozevlátá
sukně	sukně	k1gFnPc1	sukně
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kužele	kužel	k1gInSc2	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
sukněmi	sukně	k1gFnPc7	sukně
bývalo	bývat	k5eAaImAgNnS	bývat
mnoho	mnoho	k4c4	mnoho
spodniček	spodnička	k1gFnPc2	spodnička
<g/>
.	.	kIx.	.
</s>
<s>
Šaty	šat	k1gInPc1	šat
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgInP	vyznačovat
úzkým	úzký	k2eAgInSc7d1	úzký
pasem	pas	k1gInSc7	pas
se	s	k7c7	s
sepnutým	sepnutý	k2eAgInSc7d1	sepnutý
korzetem	korzet	k1gInSc7	korzet
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
pás	pás	k1gInSc1	pás
ještě	ještě	k6eAd1	ještě
hubenější	hubený	k2eAgMnSc1d2	hubenější
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
oděv	oděv	k1gInSc1	oděv
v	v	k7c6	v
ramenou	rameno	k1gNnPc6	rameno
více	hodně	k6eAd2	hodně
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
nosily	nosit	k5eAaImAgInP	nosit
velké	velký	k2eAgInPc1d1	velký
klobouky	klobouk	k1gInPc1	klobouk
zdobené	zdobený	k2eAgFnSc2d1	zdobená
umělým	umělý	k2eAgNnSc7d1	umělé
ovocem	ovoce	k1gNnSc7	ovoce
nebo	nebo	k8xC	nebo
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
</s>
<s>
Vlasy	vlas	k1gInPc1	vlas
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
složité	složitý	k2eAgInPc1d1	složitý
a	a	k8xC	a
sepnuté	sepnutý	k2eAgInPc1d1	sepnutý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
plesy	ples	k1gInPc4	ples
nosily	nosit	k5eAaImAgFnP	nosit
hedvábné	hedvábný	k2eAgFnPc4d1	hedvábná
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
rukavičky	rukavička	k1gFnPc4	rukavička
<g/>
,	,	kIx,	,
šály	šála	k1gFnPc4	šála
a	a	k8xC	a
vějíře	vějíř	k1gInPc4	vějíř
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
nosili	nosit	k5eAaImAgMnP	nosit
frakový	frakový	k2eAgInSc4d1	frakový
kabát	kabát	k1gInSc4	kabát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
pestré	pestrý	k2eAgFnPc4d1	pestrá
a	a	k8xC	a
syté	sytý	k2eAgFnPc4d1	sytá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kabátem	kabát	k1gInSc7	kabát
mívali	mívat	k5eAaImAgMnP	mívat
kostkované	kostkovaný	k2eAgFnPc4d1	kostkovaná
vesty	vesta	k1gFnPc4	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Kalhoty	kalhoty	k1gFnPc1	kalhoty
byly	být	k5eAaImAgFnP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
především	především	k6eAd1	především
jiné	jiný	k2eAgFnPc4d1	jiná
barvy	barva	k1gFnPc4	barva
než	než	k8xS	než
kabát	kabát	k1gInSc4	kabát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
si	se	k3xPyFc3	se
oblékali	oblékat	k5eAaImAgMnP	oblékat
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
pláště	plášť	k1gInPc4	plášť
s	s	k7c7	s
několika	několik	k4yIc7	několik
límci	límec	k1gInPc7	límec
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
kabátům	kabát	k1gInPc3	kabát
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
carriky	carrika	k1gFnSc2	carrika
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kabátu	kabát	k1gInSc3	kabát
nosili	nosit	k5eAaImAgMnP	nosit
špacírku	špacírka	k1gFnSc4	špacírka
(	(	kIx(	(
<g/>
hůlku	hůlka	k1gFnSc4	hůlka
<g/>
)	)	kIx)	)
s	s	k7c7	s
cylindrem	cylindr	k1gInSc7	cylindr
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
mužnost	mužnost	k1gFnSc4	mužnost
podtrhoval	podtrhovat	k5eAaImAgInS	podtrhovat
knír	knír	k1gInSc1	knír
a	a	k8xC	a
postranní	postranní	k2eAgInPc1d1	postranní
licousy	licousy	k1gInPc1	licousy
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
osvícenství	osvícenství	k1gNnSc1	osvícenství
bylo	být	k5eAaImAgNnS	být
univerzálním	univerzální	k2eAgNnPc3d1	univerzální
celoevropským	celoevropský	k2eAgNnPc3d1	celoevropské
hnutím	hnutí	k1gNnPc3	hnutí
<g/>
,	,	kIx,	,
romantismus	romantismus	k1gInSc4	romantismus
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
zemi	zem	k1gFnSc6	zem
své	svůj	k3xOyFgNnSc4	svůj
specifické	specifický	k2eAgNnSc4d1	specifické
naplnění	naplnění	k1gNnSc4	naplnění
a	a	k8xC	a
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
vycházející	vycházející	k2eAgInSc1d1	vycházející
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
prostředí	prostředí	k1gNnSc6	prostředí
využil	využít	k5eAaPmAgMnS	využít
dějiny	dějiny	k1gFnPc4	dějiny
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
prostředí	prostředí	k1gNnSc6	prostředí
apeloval	apelovat	k5eAaImAgInS	apelovat
na	na	k7c4	na
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
humanistickou	humanistický	k2eAgFnSc4d1	humanistická
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
na	na	k7c4	na
křížové	křížový	k2eAgFnPc4d1	křížová
výpravy	výprava	k1gFnPc4	výprava
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
na	na	k7c4	na
nedávné	dávný	k2eNgFnPc4d1	nedávná
napoleonské	napoleonský	k2eAgFnPc4d1	napoleonská
války	válka	k1gFnPc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Romantičtí	romantický	k2eAgMnPc1d1	romantický
tvůrci	tvůrce	k1gMnPc1	tvůrce
zobrazovali	zobrazovat	k5eAaImAgMnP	zobrazovat
bájné	bájný	k2eAgMnPc4d1	bájný
hrdiny	hrdina	k1gMnPc4	hrdina
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
umění	umění	k1gNnSc6	umění
např.	např.	kA	např.
Přemysl	Přemysl	k1gMnSc1	Přemysl
Oráč	oráč	k1gMnSc1	oráč
a	a	k8xC	a
kněžna	kněžna	k1gFnSc1	kněžna
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
,	,	kIx,	,
v	v	k7c6	v
německém	německý	k2eAgMnSc6d1	německý
Parsifal	Parsifal	k1gMnSc6	Parsifal
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
král	král	k1gMnSc1	král
Artuš	Artuš	k1gMnSc1	Artuš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
směrů	směr	k1gInPc2	směr
posilovala	posilovat	k5eAaImAgFnS	posilovat
jiný	jiný	k2eAgInSc4d1	jiný
ideologický	ideologický	k2eAgInSc4d1	ideologický
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
–	–	k?	–
národní	národní	k2eAgNnSc1d1	národní
cítění	cítění	k1gNnSc1	cítění
–	–	k?	–
podporovaný	podporovaný	k2eAgMnSc1d1	podporovaný
i	i	k9	i
na	na	k7c6	na
teoretické	teoretický	k2eAgFnSc6d1	teoretická
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
německého	německý	k2eAgMnSc2d1	německý
filozofa	filozof	k1gMnSc2	filozof
Johanna	Johann	k1gMnSc2	Johann
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
von	von	k1gInSc4	von
Herdera	Herdero	k1gNnSc2	Herdero
<g/>
,	,	kIx,	,
což	což	k9	což
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
měrou	míra	k1gFnSc7wR	míra
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
národnímu	národní	k2eAgNnSc3d1	národní
probuzení	probuzení	k1gNnSc3	probuzení
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
vzniku	vznik	k1gInSc2	vznik
romantismu	romantismus	k1gInSc2	romantismus
právě	právě	k6eAd1	právě
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
byla	být	k5eAaImAgFnS	být
drsnost	drsnost	k1gFnSc1	drsnost
<g/>
,	,	kIx,	,
nezkrotnost	nezkrotnost	k1gFnSc1	nezkrotnost
a	a	k8xC	a
tajemnost	tajemnost	k1gFnSc1	tajemnost
tamní	tamní	k2eAgFnSc2d1	tamní
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
legendy	legenda	k1gFnSc2	legenda
i	i	k8xC	i
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
orientem	orient	k1gInSc7	orient
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
byla	být	k5eAaImAgFnS	být
anglickým	anglický	k2eAgMnPc3d1	anglický
romantikům	romantik	k1gMnPc3	romantik
gotika	gotika	k1gFnSc1	gotika
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgNnPc4	svůj
tajemství	tajemství	k1gNnPc4	tajemství
<g/>
,	,	kIx,	,
rytířství	rytířství	k1gNnPc4	rytířství
a	a	k8xC	a
ideály	ideál	k1gInPc4	ideál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
lepší	dobrý	k2eAgInPc4d2	lepší
mezilidské	mezilidský	k2eAgInPc4d1	mezilidský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zpracovávali	zpracovávat	k5eAaImAgMnP	zpracovávat
v	v	k7c6	v
románech	román	k1gInPc6	román
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
nazváno	nazvat	k5eAaBmNgNnS	nazvat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
anglické	anglický	k2eAgFnSc2d1	anglická
romantické	romantický	k2eAgFnSc2d1	romantická
literatury	literatura	k1gFnSc2	literatura
byl	být	k5eAaImAgMnS	být
George	Georg	k1gMnSc2	Georg
Gordon	Gordon	k1gMnSc1	Gordon
Byron	Byron	k1gMnSc1	Byron
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schlegel	Schlegel	k1gMnSc1	Schlegel
Novalis	Novalis	k1gFnPc2	Novalis
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
nabylo	nabýt	k5eAaPmAgNnS	nabýt
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
významu	význam	k1gInSc2	význam
a	a	k8xC	a
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významný	významný	k2eAgMnSc1d1	významný
ruský	ruský	k2eAgMnSc1d1	ruský
romantik	romantik	k1gMnSc1	romantik
byl	být	k5eAaImAgMnS	být
Michail	Michail	k1gMnSc1	Michail
Jurjevič	Jurjevič	k1gMnSc1	Jurjevič
Lermontov	Lermontovo	k1gNnPc2	Lermontovo
<g/>
.	.	kIx.	.
</s>
<s>
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
Juliusz	Juliusz	k1gMnSc1	Juliusz
Słowacki	Słowack	k1gFnSc2	Słowack
České	český	k2eAgFnSc2d1	Česká
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Frič	Frič	k1gMnSc1	Frič
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
Anglické	anglický	k2eAgFnSc2d1	anglická
George	Georg	k1gFnSc2	Georg
Gordon	Gordon	k1gMnSc1	Gordon
Byron	Byron	k1gMnSc1	Byron
James	James	k1gMnSc1	James
Macpherson	Macpherson	k1gMnSc1	Macpherson
Sir	sir	k1gMnSc1	sir
Walter	Walter	k1gMnSc1	Walter
Scott	Scott	k1gMnSc1	Scott
Mary	Mary	k1gFnSc1	Mary
Shelleyová	Shelleyová	k1gFnSc1	Shelleyová
Percy	Perca	k1gFnSc2	Perca
Bysshe	Byssh	k1gFnSc2	Byssh
Shelley	Shellea	k1gFnSc2	Shellea
William	William	k1gInSc1	William
Wordsworth	Wordsworth	k1gMnSc1	Wordsworth
Samuel	Samuel	k1gMnSc1	Samuel
Taylor	Taylor	k1gMnSc1	Taylor
Coleridge	Coleridg	k1gFnSc2	Coleridg
William	William	k1gInSc1	William
Blake	Blak	k1gFnSc2	Blak
Robert	Robert	k1gMnSc1	Robert
Southey	Southea	k1gMnSc2	Southea
Jezerní	jezerní	k2eAgFnSc2d1	jezerní
básníci	básník	k1gMnPc1	básník
Jane	Jan	k1gMnSc5	Jan
Austenová	Austenový	k2eAgFnSc1d1	Austenová
Charlotte	Charlott	k1gInSc5	Charlott
Brontëová	Brontëový	k2eAgFnSc1d1	Brontëová
Emily	Emil	k1gMnPc7	Emil
Brontëová	Brontëová	k1gFnSc1	Brontëová
Anne	Anne	k1gFnSc1	Anne
Brontëová	Brontëová	k1gFnSc1	Brontëová
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Alexandre	Alexandr	k1gInSc5	Alexandr
Dumas	Dumas	k1gMnSc1	Dumas
<g />
.	.	kIx.	.
</s>
<s>
Alexandre	Alexandr	k1gInSc5	Alexandr
Dumas	Dumas	k1gInSc4	Dumas
mladší	mladý	k2eAgMnSc1d2	mladší
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
François	François	k1gFnPc2	François
René	René	k1gMnSc1	René
de	de	k?	de
Chateaubriand	Chateaubriand	k1gInSc1	Chateaubriand
Eugene	Eugen	k1gInSc5	Eugen
Delacroix	Delacroix	k1gInSc1	Delacroix
Théodore	Théodor	k1gInSc5	Théodor
Géricault	Géricault	k1gInSc4	Géricault
Prosper	Prospero	k1gNnPc2	Prospero
Mérimée	Mérimé	k1gMnSc2	Mérimé
George	Georg	k1gMnSc2	Georg
Sand	Sand	k1gMnSc1	Sand
Alfred	Alfred	k1gMnSc1	Alfred
de	de	k?	de
Musset	Musset	k1gMnSc1	Musset
Stendhal	Stendhal	k1gMnSc1	Stendhal
Německé	německý	k2eAgFnSc2d1	německá
E.T.A.	E.T.A.	k1gFnSc2	E.T.A.
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
Novalis	Novalis	k1gFnPc2	Novalis
Caspar	Caspar	k1gMnSc1	Caspar
David	David	k1gMnSc1	David
Friedrich	Friedrich	k1gMnSc1	Friedrich
Felix	Felix	k1gMnSc1	Felix
Mendelssohn	Mendelssohn	k1gMnSc1	Mendelssohn
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gNnSc4	Schumann
Bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmový	k2eAgFnSc2d1	Grimmová
Americké	americký	k2eAgFnSc2d1	americká
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
Walt	Walt	k1gMnSc1	Walt
Whitman	Whitman	k1gMnSc1	Whitman
Herman	Herman	k1gMnSc1	Herman
Melville	Melville	k1gFnSc2	Melville
James	James	k1gMnSc1	James
Fenimore	Fenimor	k1gInSc5	Fenimor
Cooper	Cooper	k1gMnSc1	Cooper
Nathaniel	Nathaniela	k1gFnPc2	Nathaniela
Hawthorne	Hawthorn	k1gInSc5	Hawthorn
Polské	polský	k2eAgFnPc1d1	polská
Fryderyk	Fryderyka	k1gFnPc2	Fryderyka
Chopin	Chopin	k1gMnSc1	Chopin
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
Ruské	ruský	k2eAgFnSc2d1	ruská
Alexandr	Alexandr	k1gMnSc1	Alexandr
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
Michail	Michail	k1gMnSc1	Michail
Jurjevič	Jurjevič	k1gMnSc1	Jurjevič
Lermontov	Lermontov	k1gInSc4	Lermontov
Český	český	k2eAgInSc4d1	český
romantismus	romantismus	k1gInSc4	romantismus
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
romantismus	romantismus	k1gInSc1	romantismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
romantismus	romantismus	k1gInSc1	romantismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
