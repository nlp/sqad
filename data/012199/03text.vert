<p>
<s>
Antropogenní	antropogenní	k2eAgNnSc1d1	antropogenní
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
také	také	k9	také
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
umělé	umělý	k2eAgNnSc1d1	umělé
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
působením	působení	k1gNnSc7	působení
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přehradní	přehradní	k2eAgFnPc4d1	přehradní
nádrže	nádrž	k1gFnPc4	nádrž
<g/>
,	,	kIx,	,
rybníky	rybník	k1gInPc4	rybník
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
antropogenní	antropogenní	k2eAgMnPc1d1	antropogenní
jezera	jezero	k1gNnSc2	jezero
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
způsobem	způsob	k1gInSc7	způsob
vypustit	vypustit	k5eAaPmF	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Využívány	využíván	k2eAgInPc1d1	využíván
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
k	k	k7c3	k
rybářství	rybářství	k1gNnSc3	rybářství
či	či	k8xC	či
rekreaci	rekreace	k1gFnSc3	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
písníky	písník	k1gInPc1	písník
–	–	k?	–
zatopené	zatopený	k2eAgFnSc2d1	zatopená
pískovny	pískovna	k1gFnSc2	pískovna
a	a	k8xC	a
štěrkovny	štěrkovna	k1gFnSc2	štěrkovna
</s>
</p>
<p>
<s>
zatopené	zatopený	k2eAgInPc1d1	zatopený
lomy	lom	k1gInPc1	lom
</s>
</p>
<p>
<s>
jezera	jezero	k1gNnPc4	jezero
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
propadem	propad	k1gInSc7	propad
poddolovaného	poddolovaný	k2eAgNnSc2d1	poddolované
nadloží	nadloží	k1gNnSc2	nadloží
(	(	kIx(	(
<g/>
Záplavy	záplava	k1gFnPc1	záplava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
některá	některý	k3yIgNnPc1	některý
fluviální	fluviální	k2eAgNnPc1d1	fluviální
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
vodohospodářskými	vodohospodářský	k2eAgFnPc7d1	vodohospodářská
úpravami	úprava	k1gFnPc7	úprava
toků	tok	k1gInPc2	tok
<g/>
)	)	kIx)	)
<g/>
Pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgNnP	být
jezera	jezero	k1gNnPc1	jezero
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rekultivace	rekultivace	k1gFnSc2	rekultivace
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovaná	označovaný	k2eAgNnPc1d1	označované
jako	jako	k8xS	jako
rekultivační	rekultivační	k2eAgNnPc1d1	rekultivační
jezera	jezero	k1gNnPc1	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
antropogenní	antropogenní	k2eAgFnSc2d1	antropogenní
jezero	jezero	k1gNnSc4	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
