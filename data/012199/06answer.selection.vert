<s>
Antropogenní	antropogenní	k2eAgNnSc1d1	antropogenní
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
také	také	k9	také
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
umělé	umělý	k2eAgNnSc1d1	umělé
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
působením	působení	k1gNnSc7	působení
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
