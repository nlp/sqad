<p>
<s>
Nába	Nába	k1gFnSc1	Nába
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Naab	Naab	k1gInSc1	Naab
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
levý	levý	k2eAgInSc4d1	levý
přítok	přítok	k1gInSc4	přítok
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
.	.	kIx.	.
</s>
<s>
Nába	Nába	k6eAd1	Nába
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
165	[number]	k4	165
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
spádové	spádový	k2eAgFnSc2d1	spádová
oblasti	oblast	k1gFnSc2	oblast
patří	patřit	k5eAaImIp3nS	patřit
5225	[number]	k4	5225
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
jména	jméno	k1gNnSc2	jméno
řeky	řeka	k1gFnSc2	řeka
Náby	Nába	k1gFnSc2	Nába
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
==	==	k?	==
</s>
</p>
<p>
<s>
Nába	Nába	k6eAd1	Nába
vzniká	vznikat	k5eAaImIp3nS	vznikat
asi	asi	k9	asi
devět	devět	k4xCc4	devět
kilometrů	kilometr	k1gInPc2	kilometr
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Luhe-Wildenau	Luhe-Wildenaus	k1gInSc2	Luhe-Wildenaus
soutokem	soutok	k1gInSc7	soutok
Waldnaab	Waldnaaba	k1gFnPc2	Waldnaaba
a	a	k8xC	a
Haidenaab	Haidenaaba	k1gFnPc2	Haidenaaba
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
teče	téct	k5eAaImIp3nS	téct
přes	přes	k7c4	přes
Schwandorf	Schwandorf	k1gInSc4	Schwandorf
a	a	k8xC	a
Burglengenfeld	Burglengenfeld	k1gInSc4	Burglengenfeld
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
přítok	přítok	k1gInSc1	přítok
je	být	k5eAaImIp3nS	být
Vils	Vils	k1gInSc4	Vils
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
zprava	zprava	k6eAd1	zprava
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
od	od	k7c2	od
města	město	k1gNnSc2	město
Amberg	Amberg	k1gInSc1	Amberg
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
města	město	k1gNnSc2	město
Schwarzenfeld	Schwarzenfelda	k1gFnPc2	Schwarzenfelda
do	do	k7c2	do
Náby	Nába	k1gFnSc2	Nába
ústí	ústit	k5eAaImIp3nS	ústit
zleva	zleva	k6eAd1	zleva
Nemanický	manický	k2eNgInSc1d1	Nemanický
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schwarzach	Schwarzach	k1gInSc1	Schwarzach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obce	obec	k1gFnPc1	obec
a	a	k8xC	a
města	město	k1gNnPc1	město
podél	podél	k7c2	podél
Náby	Nába	k1gFnSc2	Nába
===	===	k?	===
</s>
</p>
<p>
<s>
Tirschenreuth	Tirschenreuth	k1gMnSc1	Tirschenreuth
</s>
</p>
<p>
<s>
Neustadt	Neustadt	k1gInSc1	Neustadt
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Waldnaab	Waldnaab	k1gInSc1	Waldnaab
</s>
</p>
<p>
<s>
Weiden	Weidno	k1gNnPc2	Weidno
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Oberpfalz	Oberpfalz	k1gInSc1	Oberpfalz
</s>
</p>
<p>
<s>
Erbendorf	Erbendorf	k1gMnSc1	Erbendorf
</s>
</p>
<p>
<s>
Pressath	Pressath	k1gMnSc1	Pressath
</s>
</p>
<p>
<s>
Luhe-Wildenau	Luhe-Wildenau	k6eAd1	Luhe-Wildenau
</s>
</p>
<p>
<s>
Wernberg-Köblitz	Wernberg-Köblitz	k1gMnSc1	Wernberg-Köblitz
</s>
</p>
<p>
<s>
Pfreimd	Pfreimd	k6eAd1	Pfreimd
</s>
</p>
<p>
<s>
Nabburg	Nabburg	k1gMnSc1	Nabburg
</s>
</p>
<p>
<s>
Schwarzenfeld	Schwarzenfeld	k6eAd1	Schwarzenfeld
</s>
</p>
<p>
<s>
Schwandorf	Schwandorf	k1gMnSc1	Schwandorf
</s>
</p>
<p>
<s>
Teublitz	Teublitz	k1gMnSc1	Teublitz
</s>
</p>
<p>
<s>
Burglengenfeld	Burglengenfeld	k6eAd1	Burglengenfeld
</s>
</p>
<p>
<s>
Kallmünz	Kallmünz	k1gMnSc1	Kallmünz
</s>
</p>
<p>
<s>
Duggendorf	Duggendorf	k1gMnSc1	Duggendorf
</s>
</p>
<p>
<s>
Pielenhofen	Pielenhofen	k1gInSc1	Pielenhofen
</s>
</p>
<p>
<s>
Penk	Penk	k6eAd1	Penk
</s>
</p>
<p>
<s>
Etterzhausen	Etterzhausen	k1gInSc1	Etterzhausen
</s>
</p>
<p>
<s>
Pettendorf	Pettendorf	k1gMnSc1	Pettendorf
</s>
</p>
<p>
<s>
Windischeschenbach	Windischeschenbach	k1gMnSc1	Windischeschenbach
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nába	Náb	k1gInSc2	Náb
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vodácká	vodácký	k2eAgFnSc1d1	vodácká
kilometráž	kilometráž	k1gFnSc1	kilometráž
řeky	řeka	k1gFnSc2	řeka
Naab	Naaba	k1gFnPc2	Naaba
</s>
</p>
