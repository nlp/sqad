<s>
Konec	konec	k1gInSc1
dětství	dětství	k1gNnSc2
(	(	kIx(
<g/>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Konec	konec	k1gInSc1
dětství	dětství	k1gNnSc2
</s>
<s>
Díl	díl	k1gInSc1
seriálu	seriál	k1gInSc2
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
Pův	Pův	k1gFnSc1
<g/>
.	.	kIx.
název	název	k1gInSc1
</s>
<s>
Childhood	Childhood	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
End	End	k1gFnSc7
Číslo	číslo	k1gNnSc4
</s>
<s>
řada	řada	k1gFnSc1
1	#num#	k4
<g/>
díl	díl	k1gInSc1
6	#num#	k4
Premiéra	premiér	k1gMnSc2
</s>
<s>
20040813	#num#	k4
<g/>
a	a	k8xC
<g/>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2004	#num#	k4
20081119	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
Tvorba	tvorba	k1gFnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Gero	Gero	k1gMnSc1
Režie	režie	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Winning	Winning	k1gInSc4
Prod	Prod	k1gInSc4
<g/>
.	.	kIx.
kód	kód	k1gInSc4
</s>
<s>
106	#num#	k4
Hosté	host	k1gMnPc1
</s>
<s>
Courtenay	Courtenay	k1gInPc1
J.	J.	kA
Stevens	Stevens	k1gInSc4
jako	jako	k8xS,k8xC
Keras	Keras	k1gInSc4
</s>
<s>
Dominic	Dominice	k1gFnPc2
Zamprogna	Zamprogn	k1gInSc2
jako	jako	k8xC,k8xS
Aries	Ariesa	k1gFnPc2
</s>
<s>
Sam	Sam	k1gMnSc1
Charles	Charles	k1gMnSc1
jako	jako	k8xC,k8xS
Casta	Casta	k1gMnSc1
</s>
<s>
Jessica	Jessic	k2eAgFnSc1d1
Amlee	Amlee	k1gFnSc1
jako	jako	k8xC,k8xS
Cleo	Cleo	k1gNnSc1
</s>
<s>
Shane	Shanout	k5eAaPmIp3nS,k5eAaImIp3nS
Meier	Meier	k1gMnSc1
jako	jako	k8xS,k8xC
Neleus	Neleus	k1gMnSc1
</s>
<s>
Julie	Julie	k1gFnSc1
Patzwald	Patzwald	k1gMnSc1
jako	jako	k8xS,k8xC
Pelius	Pelius	k1gMnSc1
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1
dílů	díl	k1gInPc2
</s>
<s>
←	←	k?
Předchozí	předchozí	k2eAgFnSc1d1
PodezřeníNásledující	PodezřeníNásledující	k2eAgFnSc1d1
→	→	k?
Otrávená	otrávený	k2eAgFnSc1d1
studnice	studnice	k1gFnSc1
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
AtlantidaNěkterá	AtlantidaNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
dětství	dětství	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Childhood	Childhood	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
End	End	k1gFnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
6	#num#	k4
<g/>
.	.	kIx.
epizoda	epizoda	k1gFnSc1
I.	I.	kA
série	série	k1gFnSc1
sci-fi	sci-fi	k1gFnSc1
seriálu	seriál	k1gInSc2
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
epizody	epizoda	k1gFnSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Tým	tým	k1gInSc1
přilétá	přilétat	k5eAaImIp3nS
na	na	k7c4
planetu	planeta	k1gFnSc4
M	M	kA
<g/>
7	#num#	kA
<g/>
G-	G-	kA
<g/>
677	#num#	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jumperu	jumper	k1gInSc2
selžou	selhat	k5eAaPmIp3nP
motory	motor	k1gInPc1
a	a	k8xC
havaruje	havarovat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
se	se	k3xPyFc4
vydá	vydat	k5eAaPmIp3nS
hledat	hledat	k5eAaImF
bránu	brána	k1gFnSc4
a	a	k8xC
narazí	narazit	k5eAaPmIp3nS
na	na	k7c6
vesnici	vesnice	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
žijí	žít	k5eAaImIp3nP
samé	samý	k3xTgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
zneklidněné	zneklidněný	k2eAgFnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gMnPc4
navštívili	navštívit	k5eAaPmAgMnP
"	"	kIx"
<g/>
dorostlí	dorostlý	k2eAgMnPc1d1
<g/>
"	"	kIx"
a	a	k8xC
rozhodnou	rozhodnout	k5eAaPmIp3nP
se	se	k3xPyFc4
odvést	odvést	k5eAaPmF
je	být	k5eAaImIp3nS
ke	k	k7c3
svým	svůj	k3xOyFgNnPc3
"	"	kIx"
<g/>
starcům	stařec	k1gMnPc3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sheppardův	Sheppardův	k2eAgInSc4d1
tým	tým	k1gInSc4
je	být	k5eAaImIp3nS
překvapen	překvapit	k5eAaPmNgMnS
<g/>
,	,	kIx,
když	když	k8xS
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
starcům	stařec	k1gMnPc3
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
maximálně	maximálně	k6eAd1
24	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Keras	Keras	k1gMnSc1
–	–	k?
nejstarší	starý	k2eAgMnSc1d3
ze	z	k7c2
zdejších	zdejší	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
–	–	k?
týmu	tým	k1gInSc2
vysvětlí	vysvětlit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gMnPc1
předci	předek	k1gMnPc1
před	před	k7c7
mnoha	mnoho	k4c3
sty	sto	k4xCgNnPc7
lety	léto	k1gNnPc7
rozhodli	rozhodnout	k5eAaPmAgMnP
vyhnout	vyhnout	k5eAaPmF
wraithským	wraithský	k2eAgInPc3d1
útokům	útok	k1gInPc3
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nenechají	nechat	k5eNaPmIp3nP
své	svůj	k3xOyFgMnPc4
obyvatele	obyvatel	k1gMnPc4
dospět	dospět	k5eAaPmF
do	do	k7c2
tak	tak	k6eAd1
vysokého	vysoký	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
Wraithy	Wraith	k1gInPc4
zajímali	zajímat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
v	v	k7c4
předvečer	předvečer	k1gInSc4
svých	svůj	k3xOyFgMnPc2
25	#num#	k4
<g/>
.	.	kIx.
narozenin	narozeniny	k1gFnPc2
spáchá	spáchat	k5eAaPmIp3nS
sebevraždu	sebevražda	k1gFnSc4
a	a	k8xC
předejde	předejít	k5eAaPmIp3nS
tak	tak	k6eAd1
možnosti	možnost	k1gFnPc4
násilné	násilný	k2eAgFnSc3d1
smrti	smrt	k1gFnSc3
z	z	k7c2
rukou	ruka	k1gFnPc2
Wraithů	Wraith	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
zabránila	zabránit	k5eAaPmAgFnS
jejich	jejich	k3xOp3gFnSc4
duši	duše	k1gFnSc4
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
další	další	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rituál	rituál	k1gInSc1
se	se	k3xPyFc4
praktikuje	praktikovat	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgInPc6
12	#num#	k4
vesnicích	vesnice	k1gFnPc6
na	na	k7c6
planetě	planeta	k1gFnSc6
a	a	k8xC
večer	večer	k6eAd1
jej	on	k3xPp3gMnSc4
má	mít	k5eAaImIp3nS
podstoupit	podstoupit	k5eAaPmF
také	také	k9
Keras	Keras	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rodney	Rodne	k2eAgFnPc1d1
McKay	McKaa	k1gFnPc1
zatím	zatím	k6eAd1
zjistí	zjistit	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jumper	jumper	k1gInSc4
havaroval	havarovat	k5eAaPmAgMnS
díky	díky	k7c3
štítu	štít	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
likviduje	likvidovat	k5eAaBmIp3nS
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
záření	záření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wraithské	Wraithský	k2eAgFnPc4d1
šipky	šipka	k1gFnPc4
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
jejich	jejich	k3xOp3gFnPc1
zbraně	zbraň	k1gFnPc1
tak	tak	k6eAd1
byly	být	k5eAaImAgFnP
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
vesnic	vesnice	k1gFnPc2
neúčinné	účinný	k2eNgFnPc1d1
-	-	kIx~
proto	proto	k8xC
se	se	k3xPyFc4
této	tento	k3xDgFnSc3
planetě	planeta	k1gFnSc3
vyhýbají	vyhýbat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štít	štít	k1gInSc1
je	být	k5eAaImIp3nS
napájen	napájen	k2eAgInSc1d1
ZPM	ZPM	kA
<g/>
,	,	kIx,
McKay	McKaa	k1gFnSc2
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
si	se	k3xPyFc3
jej	on	k3xPp3gNnSc4
vypůjčit	vypůjčit	k5eAaPmF
a	a	k8xC
odvézt	odvézt	k5eAaPmF
na	na	k7c4
Atlantidu	Atlantida	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Netuší	tušit	k5eNaImIp3nS
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
wraithský	wraithský	k1gMnSc1
vysílač	vysílač	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ihned	ihned	k6eAd1
po	po	k7c6
vypnutí	vypnutí	k1gNnSc6
štítu	štít	k1gInSc2
začne	začít	k5eAaPmIp3nS
vysílat	vysílat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistí	zjistit	k5eAaPmIp3nS
však	však	k9
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
rituální	rituální	k2eAgFnPc1d1
sebevraždy	sebevražda	k1gFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
úkol	úkol	k1gInSc4
udržet	udržet	k5eAaPmF
populaci	populace	k1gFnSc4
v	v	k7c6
přijatelném	přijatelný	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
štít	štít	k1gInSc1
byl	být	k5eAaImAgInS
schopen	schopen	k2eAgMnSc1d1
pokrýt	pokrýt	k5eAaPmF
pouze	pouze	k6eAd1
malou	malý	k2eAgFnSc4d1
část	část	k1gFnSc4
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Sheppard	Sheppard	k1gMnSc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
pokouší	pokoušet	k5eAaImIp3nP
dětem	dítě	k1gFnPc3
sebevraždy	sebevražda	k1gFnSc2
rozmluvit	rozmluvit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
ze	z	k7c2
starších	starší	k1gMnPc2
mu	on	k3xPp3gMnSc3
přestávají	přestávat	k5eAaImIp3nP
důvěřovat	důvěřovat	k5eAaImF
a	a	k8xC
požadují	požadovat	k5eAaImIp3nP
okamžitý	okamžitý	k2eAgInSc4d1
odchod	odchod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
McKay	McKaa	k1gFnSc2
se	se	k3xPyFc4
nad	nad	k7c7
vesnicí	vesnice	k1gFnSc7
objeví	objevit	k5eAaPmIp3nS
wraithská	wraithský	k2eAgFnSc1d1
průzkumná	průzkumný	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
zvažují	zvažovat	k5eAaImIp3nP
obětovat	obětovat	k5eAaBmF
Sheppardův	Sheppardův	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
McKayovi	McKaya	k1gMnSc3
se	se	k3xPyFc4
po	po	k7c6
delším	dlouhý	k2eAgNnSc6d2
úsilí	úsilí	k1gNnSc6
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
ZPM	ZPM	kA
zapojit	zapojit	k5eAaPmF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
některé	některý	k3yIgFnPc4
z	z	k7c2
dětí	dítě	k1gFnPc2
vidí	vidět	k5eAaImIp3nP
a	a	k8xC
pochopí	pochopit	k5eAaPmIp3nP
souvislost	souvislost	k1gFnSc4
s	s	k7c7
Wraithy	Wraith	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysvětlí	vysvětlit	k5eAaPmIp3nS
vše	všechen	k3xTgNnSc1
ostatním	ostatní	k2eAgFnPc3d1
<g/>
,	,	kIx,
společně	společně	k6eAd1
se	se	k3xPyFc4
rozhodnou	rozhodnout	k5eAaPmIp3nP
ukončit	ukončit	k5eAaPmF
sebevraždy	sebevražda	k1gFnPc4
a	a	k8xC
vše	všechen	k3xTgNnSc4
vysvětlit	vysvětlit	k5eAaPmF
i	i	k9
obyvatelům	obyvatel	k1gMnPc3
ostatních	ostatní	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sheppardův	Sheppardův	k2eAgInSc1d1
tým	tým	k1gInSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
na	na	k7c4
Atlantidu	Atlantida	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
Díla	dílo	k1gNnSc2
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
Archa	archa	k1gFnSc1
pravdy	pravda	k1gFnSc2
•	•	k?
Návrat	návrat	k1gInSc1
Seriály	seriál	k1gInPc7
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
(	(	kIx(
<g/>
SG-	SG-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlantida	Atlantida	k1gFnSc1
•	•	k?
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
•	•	k?
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
•	•	k?
Infinity	Infinita	k1gFnSc2
(	(	kIx(
<g/>
animovaný	animovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
SG-1	SG-1	k4
</s>
<s>
epizody	epizoda	k1gFnPc4
•	•	k?
postavy	postava	k1gFnSc2
Samantha	Samantha	k1gFnSc1
Carterová	Carterová	k1gFnSc1
•	•	k?
George	George	k1gNnSc1
Hammond	Hammond	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Jackson	Jackson	k1gMnSc1
•	•	k?
Hank	Hank	k1gInSc1
Landry	Landr	k1gInPc1
•	•	k?
Vala	Vala	k1gMnSc1
Mal	málit	k5eAaImRp2nS
Doran	Doran	k1gInSc1
•	•	k?
Cameron	Cameron	k1gInSc1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
Jack	Jack	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Neill	Neill	k1gMnSc1
•	•	k?
Jonas	Jonas	k1gMnSc1
Quinn	Quinn	k1gMnSc1
•	•	k?
Teal	Teal	k1gMnSc1
<g/>
'	'	kIx"
<g/>
c	c	k0
Atlantida	Atlantida	k1gFnSc1
</s>
<s>
epizody	epizoda	k1gFnPc4
•	•	k?
postavy	postava	k1gFnSc2
Carson	Carson	k1gMnSc1
Beckett	Beckett	k1gMnSc1
•	•	k?
Samantha	Samantha	k1gFnSc1
Carterová	Carterová	k1gFnSc1
•	•	k?
Ronon	Ronon	k1gMnSc1
Dex	Dex	k1gMnSc1
•	•	k?
Teyla	Teyla	k1gMnSc1
Emmagan	Emmagan	k1gMnSc1
•	•	k?
Aiden	Aiden	k1gInSc1
Ford	ford	k1gInSc1
•	•	k?
Jennifer	Jennifer	k1gInSc1
Kellerová	Kellerová	k1gFnSc1
•	•	k?
Rodney	Rodne	k2eAgFnPc4d1
McKay	McKaa	k1gFnPc4
•	•	k?
John	John	k1gMnSc1
Sheppard	Sheppard	k1gMnSc1
•	•	k?
Elizabeth	Elizabeth	k1gFnSc1
Weirová	Weirová	k1gFnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Woolsey	Woolsea	k1gFnSc2
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
</s>
<s>
epizody	epizoda	k1gFnPc4
•	•	k?
postavy	postava	k1gFnSc2
Chloe	Chloe	k1gFnSc1
Armstrongová	Armstrongová	k1gFnSc1
•	•	k?
Ronald	Ronald	k1gMnSc1
Greer	Greer	k1gMnSc1
•	•	k?
Tamara	Tamara	k1gFnSc1
Johansenová	Johansenová	k1gFnSc1
•	•	k?
Nicholas	Nicholas	k1gMnSc1
Rush	Rush	k1gMnSc1
•	•	k?
Matthew	Matthew	k1gMnSc1
Scott	Scott	k1gMnSc1
•	•	k?
Eli	Eli	k1gMnSc1
Wallace	Wallace	k1gFnSc2
•	•	k?
Everett	Everett	k1gMnSc1
Young	Young	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Telford	Telford	k1gMnSc1
•	•	k?
Camile	Camila	k1gFnSc6
Wrayová	Wrayová	k1gFnSc1
Infinity	Infinita	k1gFnSc2
</s>
<s>
epizody	epizoda	k1gFnPc4
Rasy	rasa	k1gFnSc2
</s>
<s>
SG-1	SG-1	k4
</s>
<s>
Abydosané	Abydosaný	k2eAgFnPc1d1
•	•	k?
Antikové	Antikový	k2eAgFnPc1d1
(	(	kIx(
<g/>
postavy	postava	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Asgardi	Asgard	k1gMnPc1
•	•	k?
Furlingové	Furlingový	k2eAgNnSc1d1
•	•	k?
Goa	Goa	k1gFnSc1
<g/>
'	'	kIx"
<g/>
uldi	uldi	k1gNnSc1
(	(	kIx(
<g/>
vládci	vládce	k1gMnPc1
soustavy	soustava	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Jaffové	Jaffový	k2eAgFnSc2d1
•	•	k?
Kull	Kull	k1gMnSc1
bojovníci	bojovník	k1gMnPc1
•	•	k?
Langarané	Langaraný	k2eAgNnSc1d1
•	•	k?
Luciánská	Luciánský	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
•	•	k?
Noxové	Noxová	k1gFnSc2
•	•	k?
Oriové	Oriová	k1gFnSc2
•	•	k?
Replikátoři	Replikátor	k1gMnPc1
•	•	k?
Tau	tau	k1gNnSc2
<g/>
'	'	kIx"
<g/>
riové	riový	k2eAgNnSc1d1
•	•	k?
Tok	tok	k1gInSc4
<g/>
'	'	kIx"
<g/>
rové	rové	k1gNnSc4
•	•	k?
Tolláni	Tollán	k2eAgMnPc1d1
•	•	k?
Unasové	Unasové	k2eAgFnSc1d1
Atlantida	Atlantida	k1gFnSc1
</s>
<s>
Athosiané	Athosian	k1gMnPc1
•	•	k?
Asurané	Asuraný	k2eAgNnSc1d1
•	•	k?
Cestovatelé	cestovatel	k1gMnPc1
•	•	k?
Geniiové	Geniiový	k2eAgFnSc2d1
•	•	k?
Sateďané	Sateďaná	k1gFnSc2
•	•	k?
Wraithové	Wraithové	k2eAgInSc1d1
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
</s>
<s>
Nakaiové	Nakaius	k1gMnPc1
•	•	k?
Nované	Novaný	k2eAgInPc4d1
Seznamy	seznam	k1gInPc4
</s>
<s>
lidské	lidský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
lidské	lidský	k2eAgFnPc4d1
civilizace	civilizace	k1gFnPc4
v	v	k7c6
seriálu	seriál	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
fiktivní	fiktivní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
•	•	k?
planety	planeta	k1gFnSc2
•	•	k?
mytologie	mytologie	k1gFnSc2
(	(	kIx(
<g/>
povznesení	povznesení	k1gNnSc3
<g/>
)	)	kIx)
•	•	k?
technologie	technologie	k1gFnSc1
(	(	kIx(
<g/>
hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
pozemské	pozemský	k2eAgNnSc4d1
•	•	k?
antické	antický	k2eAgFnSc2d1
•	•	k?
asgardské	asgardský	k2eAgNnSc1d1
•	•	k?
asuranské	asuranský	k2eAgNnSc1d1
•	•	k?
goa	goa	k?
<g/>
'	'	kIx"
<g/>
uldské	uldské	k2eAgInSc4d1
•	•	k?
orijské	orijský	k2eAgNnSc1d1
•	•	k?
tokránské	tokránský	k2eAgNnSc1d1
•	•	k?
tollánské	tollánský	k2eAgNnSc1d1
•	•	k?
wraithské	wraithské	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Program	program	k1gInSc1
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
Velitelství	velitelství	k1gNnSc2
Hvězdné	hvězdný	k2eAgFnSc2d1
brány	brána	k1gFnSc2
•	•	k?
vesmírné	vesmírný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
(	(	kIx(
<g/>
Atlantida	Atlantida	k1gFnSc1
•	•	k?
Destiny	Destina	k1gFnSc2
•	•	k?
pozemské	pozemský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
|	|	kIx~
Televize	televize	k1gFnSc1
</s>
