<s>
Divadlo	divadlo	k1gNnSc1	divadlo
je	být	k5eAaImIp3nS	být
výkonné	výkonný	k2eAgNnSc1d1	výkonné
čili	čili	k8xC	čili
múzické	múzický	k2eAgNnSc1d1	múzické
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
herci	herc	k1gInSc6	herc
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
zpěváci	zpěvák	k1gMnPc1	zpěvák
a	a	k8xC	a
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
)	)	kIx)	)
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
předvádějí	předvádět	k5eAaImIp3nP	předvádět
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
obvykle	obvykle	k6eAd1	obvykle
sedí	sedit	k5eAaImIp3nS	sedit
v	v	k7c6	v
hledišti	hlediště	k1gNnSc6	hlediště
kryté	krytý	k2eAgFnSc2d1	krytá
nebo	nebo	k8xC	nebo
otevřené	otevřený	k2eAgFnSc2d1	otevřená
divadelní	divadelní	k2eAgFnSc2d1	divadelní
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Divadlem	divadlo	k1gNnSc7	divadlo
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
divadelní	divadelní	k2eAgFnSc1d1	divadelní
věda	věda	k1gFnSc1	věda
čili	čili	k8xC	čili
teatrologie	teatrologie	k1gFnSc1	teatrologie
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolektiv	kolektiv	k1gInSc1	kolektiv
tvůrců	tvůrce	k1gMnPc2	tvůrce
(	(	kIx(	(
<g/>
dramaturgové	dramaturg	k1gMnPc1	dramaturg
<g/>
,	,	kIx,	,
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
choreografové	choreograf	k1gMnPc1	choreograf
<g/>
,	,	kIx,	,
scenáristé	scenárista	k1gMnPc1	scenárista
<g/>
,	,	kIx,	,
inspicient	inspicient	k1gMnSc1	inspicient
<g/>
,	,	kIx,	,
korepetitor	korepetitor	k1gMnSc1	korepetitor
<g/>
,	,	kIx,	,
scénograf	scénograf	k1gMnSc1	scénograf
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
technických	technický	k2eAgMnPc2d1	technický
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
(	(	kIx(	(
<g/>
technici	technik	k1gMnPc1	technik
<g/>
,	,	kIx,	,
zvukaři	zvukař	k1gMnPc1	zvukař
<g/>
,	,	kIx,	,
osvětlovači	osvětlovač	k1gMnPc1	osvětlovač
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g />
.	.	kIx.	.
</s>
<s>
společně	společně	k6eAd1	společně
divadelní	divadelní	k2eAgFnSc3d1	divadelní
inscenaci	inscenace	k1gFnSc3	inscenace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
předvedena	předveden	k2eAgFnSc1d1	předvedena
buďto	buďto	k8xC	buďto
jednorázově	jednorázově	k6eAd1	jednorázově
nebo	nebo	k8xC	nebo
ve	v	k7c4	v
vícero	vícero	k1gNnSc4	vícero
reprízách	repríza	k1gFnPc6	repríza
divákům	divák	k1gMnPc3	divák
<g/>
.	.	kIx.	.
volba	volba	k1gFnSc1	volba
dramatického	dramatický	k2eAgInSc2d1	dramatický
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
dramatu	drama	k1gNnSc2	drama
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
)	)	kIx)	)
zkoušky	zkouška	k1gFnPc1	zkouška
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgNnPc6	který
je	být	k5eAaImIp3nS	být
dramatický	dramatický	k2eAgInSc1d1	dramatický
text	text	k1gInSc1	text
převáděn	převádět	k5eAaImNgInS	převádět
do	do	k7c2	do
jevištní	jevištní	k2eAgFnSc2d1	jevištní
podoby	podoba	k1gFnSc2	podoba
hry	hra	k1gFnSc2	hra
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
scénografií	scénografie	k1gFnSc7	scénografie
<g/>
,	,	kIx,	,
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
tancem	tanec	k1gInSc7	tanec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
režiséra	režisér	k1gMnSc2	režisér
(	(	kIx(	(
<g/>
u	u	k7c2	u
hudebně-dramatických	hudebněramatický	k2eAgInPc2d1	hudebně-dramatický
žánrů	žánr	k1gInPc2	žánr
také	také	k9	také
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dirigentem	dirigent	k1gMnSc7	dirigent
divadelního	divadelní	k2eAgInSc2d1	divadelní
orchestru	orchestr	k1gInSc2	orchestr
<g/>
)	)	kIx)	)
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
literatury	literatura	k1gFnSc2	literatura
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
společné	společný	k2eAgNnSc4d1	společné
dílo	dílo	k1gNnSc4	dílo
několika	několik	k4yIc2	několik
tvůrců	tvůrce	k1gMnPc2	tvůrce
(	(	kIx(	(
<g/>
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
scénograf	scénograf	k1gMnSc1	scénograf
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
také	také	k9	také
publikum	publikum	k1gNnSc1	publikum
v	v	k7c6	v
hledišti	hlediště	k1gNnSc6	hlediště
vnímá	vnímat	k5eAaImIp3nS	vnímat
kolektivně	kolektivně	k6eAd1	kolektivně
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
umění	umění	k1gNnSc1	umění
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
tak	tak	k6eAd1	tak
úzkém	úzký	k2eAgInSc6d1	úzký
vztahu	vztah	k1gInSc6	vztah
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
jako	jako	k8xC	jako
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
však	však	k9	však
též	též	k9	též
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
divák	divák	k1gMnSc1	divák
a	a	k8xC	a
čtenář	čtenář	k1gMnSc1	čtenář
vnímá	vnímat	k5eAaImIp3nS	vnímat
definitivně	definitivně	k6eAd1	definitivně
ukončené	ukončený	k2eAgNnSc1d1	ukončené
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
se	se	k3xPyFc4	se
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
jako	jako	k9	jako
aktivní	aktivní	k2eAgFnSc1d1	aktivní
složka	složka	k1gFnSc1	složka
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
dotváření	dotváření	k1gNnSc6	dotváření
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgNnPc2d1	jiné
interaktivně	interaktivně	k6eAd1	interaktivně
vytvářených	vytvářený	k2eAgNnPc2d1	vytvářené
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
divák	divák	k1gMnSc1	divák
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
výsledné	výsledný	k2eAgNnSc4d1	výsledné
dílo	dílo	k1gNnSc4	dílo
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
účastí	účast	k1gFnSc7	účast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zvukové	zvukový	k2eAgInPc1d1	zvukový
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnPc1d1	speciální
umělecké	umělecký	k2eAgFnPc1d1	umělecká
instalace	instalace	k1gFnPc1	instalace
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgNnPc1d1	digitální
interaktivní	interaktivní	k2eAgNnPc1d1	interaktivní
díla	dílo	k1gNnPc1	dílo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Tématem	téma	k1gNnSc7	téma
klasického	klasický	k2eAgNnSc2d1	klasické
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
nějaká	nějaký	k3yIgFnSc1	nějaký
lidská	lidský	k2eAgFnSc1d1	lidská
nebo	nebo	k8xC	nebo
společenská	společenský	k2eAgFnSc1d1	společenská
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
zabývat	zabývat	k5eAaImF	zabývat
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hlavním	hlavní	k2eAgMnSc7d1	hlavní
nositelem	nositel	k1gMnSc7	nositel
divadelního	divadelní	k2eAgInSc2d1	divadelní
výrazu	výraz	k1gInSc2	výraz
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
člověk-herec	člověkerec	k1gInSc4	člověk-herec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
platit	platit	k5eAaImF	platit
pro	pro	k7c4	pro
experimentální	experimentální	k2eAgNnSc4d1	experimentální
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc4	umění
opticko-akustické	optickokustický	k2eAgInPc4d1	opticko-akustický
a	a	k8xC	a
časově-prostorové	časověrostorový	k2eAgInPc4d1	časově-prostorový
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
umění	umění	k1gNnSc2	umění
vnímáme	vnímat	k5eAaImIp1nP	vnímat
buď	buď	k8xC	buď
zrakem	zrak	k1gInSc7	zrak
(	(	kIx(	(
<g/>
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
klasické	klasický	k2eAgNnSc1d1	klasické
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
sluchem	sluch	k1gInSc7	sluch
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
divadlo	divadlo	k1gNnSc1	divadlo
všemi	všecek	k3xTgInPc7	všecek
lidskými	lidský	k2eAgInPc7d1	lidský
smysly	smysl	k1gInPc7	smysl
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
proudech	proud	k1gInPc6	proud
současného	současný	k2eAgNnSc2d1	současné
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
procesuální	procesuální	k2eAgNnSc4d1	procesuální
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
akční	akční	k2eAgNnSc4d1	akční
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
performance	performance	k1gFnPc4	performance
<g/>
,	,	kIx,	,
kinetické	kinetický	k2eAgNnSc4d1	kinetické
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
multimediální	multimediální	k2eAgNnSc4d1	multimediální
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
interaktivní	interaktivní	k2eAgNnSc4d1	interaktivní
umění	umění	k1gNnSc4	umění
apod.	apod.	kA	apod.
Do	do	k7c2	do
divadelní	divadelní	k2eAgFnSc2d1	divadelní
struktury	struktura	k1gFnSc2	struktura
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
umění	umění	k1gNnSc2	umění
jako	jako	k8xC	jako
její	její	k3xOp3gFnSc2	její
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
mimo	mimo	k7c4	mimo
divadlo	divadlo	k1gNnSc4	divadlo
samostatnými	samostatný	k2eAgNnPc7d1	samostatné
uměními	umění	k1gNnPc7	umění
(	(	kIx(	(
<g/>
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
herectví	herectví	k1gNnSc1	herectví
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
scenáristika	scenáristika	k1gFnSc1	scenáristika
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
divadlu	divadlo	k1gNnSc3	divadlo
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
oborech	obor	k1gInPc6	obor
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
rozhlas	rozhlas	k1gInSc1	rozhlas
nebo	nebo	k8xC	nebo
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
futuristé	futurista	k1gMnPc1	futurista
a	a	k8xC	a
dadaisté	dadaista	k1gMnPc1	dadaista
<g/>
)	)	kIx)	)
a	a	k8xC	a
potom	potom	k6eAd1	potom
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Jackson	Jackson	k1gMnSc1	Jackson
Pollock	Pollock	k1gMnSc1	Pollock
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Rauschenberg	Rauschenberg	k1gMnSc1	Rauschenberg
<g/>
,	,	kIx,	,
akční	akční	k2eAgNnSc1d1	akční
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
performance	performance	k1gFnSc1	performance
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umělec	umělec	k1gMnSc1	umělec
též	též	k9	též
integrovanou	integrovaný	k2eAgFnSc7d1	integrovaná
součástí	součást	k1gFnSc7	součást
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základní	základní	k2eAgFnSc2d1	základní
estetické	estetický	k2eAgFnSc2d1	estetická
úlohy	úloha	k1gFnSc2	úloha
může	moct	k5eAaImIp3nS	moct
divadlo	divadlo	k1gNnSc4	divadlo
plnit	plnit	k5eAaImF	plnit
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
funkcí	funkce	k1gFnPc2	funkce
–	–	k?	–
etickou	etický	k2eAgFnSc4d1	etická
<g/>
,	,	kIx,	,
výchovnou	výchovný	k2eAgFnSc4d1	výchovná
<g/>
,	,	kIx,	,
politickou	politický	k2eAgFnSc4d1	politická
<g/>
,	,	kIx,	,
zábavnou	zábavný	k2eAgFnSc4d1	zábavná
<g/>
,	,	kIx,	,
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
nebo	nebo	k8xC	nebo
didaktickou	didaktický	k2eAgFnSc4d1	didaktická
–	–	k?	–
které	který	k3yQgFnPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
jeho	on	k3xPp3gInSc4	on
účinek	účinek	k1gInSc4	účinek
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
jeho	jeho	k3xOp3gFnSc4	jeho
celkovou	celkový	k2eAgFnSc4d1	celková
společenskou	společenský	k2eAgFnSc4d1	společenská
prestiž	prestiž	k1gFnSc4	prestiž
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rozličné	rozličný	k2eAgInPc4d1	rozličný
žánry	žánr	k1gInPc4	žánr
dramatického	dramatický	k2eAgNnSc2d1	dramatické
umění	umění	k1gNnSc2	umění
nebo	nebo	k8xC	nebo
hudebně-dramatického	hudebněramatický	k2eAgNnSc2d1	hudebně-dramatické
umění	umění	k1gNnSc2	umění
podle	podle	k7c2	podle
prostorového	prostorový	k2eAgNnSc2d1	prostorové
členění	členění	k1gNnSc2	členění
a	a	k8xC	a
hereckého	herecký	k2eAgInSc2d1	herecký
projevu	projev	k1gInSc2	projev
<g/>
:	:	kIx,	:
jevištní	jevištní	k2eAgFnSc2d1	jevištní
formy	forma	k1gFnSc2	forma
činohra	činohra	k1gFnSc1	činohra
hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
opera	opera	k1gFnSc1	opera
opereta	opereta	k1gFnSc1	opereta
muzikál	muzikál	k1gInSc1	muzikál
zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
pohybové	pohybový	k2eAgFnPc4d1	pohybová
balet	balet	k1gInSc1	balet
pantomima	pantomim	k1gMnSc2	pantomim
tanec	tanec	k1gInSc4	tanec
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
stínové	stínový	k2eAgNnSc1d1	stínové
divadlo	divadlo	k1gNnSc1	divadlo
nebo	nebo	k8xC	nebo
černé	černý	k2eAgNnSc1d1	černé
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
nejevištní	jevištní	k2eNgFnSc2d1	jevištní
audiovizuální	audiovizuální	k2eAgFnSc2d1	audiovizuální
formy	forma	k1gFnSc2	forma
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
hra	hra	k1gFnSc1	hra
film	film	k1gInSc1	film
a	a	k8xC	a
video	video	k1gNnSc1	video
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
nejevištní	jevištní	k2eNgFnPc1d1	jevištní
formy	forma	k1gFnPc1	forma
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
divadlo	divadlo	k1gNnSc4	divadlo
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozhlasem	rozhlas	k1gInSc7	rozhlas
<g/>
,	,	kIx,	,
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
filmem	film	k1gInSc7	film
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
dramatická	dramatický	k2eAgNnPc4d1	dramatické
(	(	kIx(	(
<g/>
scénická	scénický	k2eAgNnPc4d1	scénické
<g/>
)	)	kIx)	)
umění	umění	k1gNnPc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
lidové	lidový	k2eAgNnSc1d1	lidové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
celá	celý	k2eAgFnSc1d1	celá
škála	škála	k1gFnSc1	škála
dramatických	dramatický	k2eAgInPc2d1	dramatický
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
projevech	projev	k1gInPc6	projev
lidové	lidový	k2eAgFnSc2d1	lidová
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
lidové	lidový	k2eAgInPc4d1	lidový
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
obřady	obřad	k1gInPc4	obřad
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
pověsti	pověst	k1gFnPc4	pověst
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgFnPc4d1	dětská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgInPc4d1	lidový
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
<g/>
)	)	kIx)	)
a	a	k8xC	a
samostatné	samostatný	k2eAgInPc1d1	samostatný
lidové	lidový	k2eAgInPc1d1	lidový
dramatické	dramatický	k2eAgInPc1d1	dramatický
útvary	útvar	k1gInPc1	útvar
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnPc1d1	různá
vánoční	vánoční	k2eAgFnPc1d1	vánoční
kolední	kolední	k2eAgFnPc1d1	kolední
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
o	o	k7c6	o
Dorotě	Dorota	k1gFnSc6	Dorota
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařazujeme	zařazovat	k5eAaImIp1nP	zařazovat
sem	sem	k6eAd1	sem
i	i	k9	i
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
lidový	lidový	k2eAgMnSc1d1	lidový
vypravěč	vypravěč	k1gMnSc1	vypravěč
vlastně	vlastně	k9	vlastně
zčásti	zčásti	k6eAd1	zčásti
hraje	hrát	k5eAaImIp3nS	hrát
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
svým	svůj	k3xOyFgInSc7	svůj
projevem	projev	k1gInSc7	projev
dramatizuje	dramatizovat	k5eAaBmIp3nS	dramatizovat
děj	děj	k1gInSc1	děj
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
mimiku	mimika	k1gFnSc4	mimika
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc4d1	různý
divadelní	divadelní	k2eAgInPc4d1	divadelní
prvky	prvek	k1gInPc4	prvek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaPmF	nalézt
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
svatebním	svatební	k2eAgInSc6d1	svatební
obřadu	obřad	k1gInSc6	obřad
(	(	kIx(	(
<g/>
námluvy	námluva	k1gFnSc2	námluva
prováděné	prováděný	k2eAgFnSc2d1	prováděná
obrazným	obrazný	k2eAgNnSc7d1	obrazné
vyjadřováním	vyjadřování	k1gNnSc7	vyjadřování
<g/>
,	,	kIx,	,
výstupy	výstup	k1gInPc4	výstup
maskovaných	maskovaný	k2eAgFnPc2d1	maskovaná
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svatby	svatba	k1gFnSc2	svatba
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
obřadech	obřad	k1gInPc6	obřad
(	(	kIx(	(
<g/>
chození	chození	k1gNnSc4	chození
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
a	a	k8xC	a
Lucie	Lucie	k1gFnPc1	Lucie
<g/>
,	,	kIx,	,
průvody	průvod	k1gInPc1	průvod
masek	maska	k1gFnPc2	maska
na	na	k7c6	na
konci	konec	k1gInSc6	konec
masopustu	masopust	k1gInSc2	masopust
<g/>
,	,	kIx,	,
vynášení	vynášení	k1gNnSc4	vynášení
Morany	Morana	k1gFnSc2	Morana
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejvyspělejším	vyspělý	k2eAgInSc7d3	nejvyspělejší
lidovým	lidový	k2eAgInSc7d1	lidový
divadelním	divadelní	k2eAgInSc7d1	divadelní
útvarem	útvar	k1gInSc7	útvar
jsou	být	k5eAaImIp3nP	být
vánoční	vánoční	k2eAgFnPc1d1	vánoční
kolední	kolední	k2eAgFnPc1d1	kolední
a	a	k8xC	a
pašijové	pašijový	k2eAgFnPc1d1	pašijová
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
námětem	námět	k1gInSc7	námět
narození	narození	k1gNnSc2	narození
Krista	Krista	k1gFnSc1	Krista
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
chození	chození	k1gNnSc2	chození
s	s	k7c7	s
hadem	had	k1gMnSc7	had
prezentující	prezentující	k2eAgFnSc2d1	prezentující
biblický	biblický	k2eAgInSc4d1	biblický
příběh	příběh	k1gInSc4	příběh
vyhnání	vyhnání	k1gNnSc2	vyhnání
Adama	Adam	k1gMnSc2	Adam
a	a	k8xC	a
Evy	Eva	k1gFnSc2	Eva
z	z	k7c2	z
Ráje	rája	k1gFnSc2	rája
<g/>
,	,	kIx,	,
chození	chození	k1gNnSc1	chození
s	s	k7c7	s
kolíbkou	kolíbka	k1gFnSc7	kolíbka
va	va	k0wR	va
Tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
<g/>
,	,	kIx,	,
chození	chození	k1gNnSc4	chození
s	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c4	na
Tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
o	o	k7c6	o
Dorotě	Dorota	k1gFnSc6	Dorota
je	být	k5eAaImIp3nS	být
zdramatizovaná	zdramatizovaný	k2eAgFnSc1d1	zdramatizovaná
středověká	středověký	k2eAgFnSc1d1	středověká
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
vladaře	vladař	k1gMnSc2	vladař
a	a	k8xC	a
poddaného	poddaný	k2eAgMnSc2d1	poddaný
(	(	kIx(	(
<g/>
krále	král	k1gMnSc2	král
a	a	k8xC	a
Doroty	Dorota	k1gFnSc2	Dorota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
divadla	divadlo	k1gNnSc2	divadlo
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc1	žádný
přímé	přímý	k2eAgInPc1d1	přímý
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
domněnky	domněnka	k1gFnPc4	domněnka
a	a	k8xC	a
teorie	teorie	k1gFnPc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
divadlo	divadlo	k1gNnSc1	divadlo
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
pravěkých	pravěký	k2eAgFnPc2d1	pravěká
slavností	slavnost	k1gFnPc2	slavnost
a	a	k8xC	a
rituálů	rituál	k1gInPc2	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obřady	obřad	k1gInPc1	obřad
měly	mít	k5eAaImAgInP	mít
snad	snad	k9	snad
také	také	k9	také
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgInSc2	který
bylo	být	k5eAaImAgNnS	být
přímo	přímo	k6eAd1	přímo
závislé	závislý	k2eAgNnSc1d1	závislé
přežití	přežití	k1gNnSc1	přežití
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
rituálech	rituál	k1gInPc6	rituál
člověk	člověk	k1gMnSc1	člověk
používal	používat	k5eAaImAgInS	používat
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
zvířecí	zvířecí	k2eAgFnPc1d1	zvířecí
masky	maska	k1gFnPc1	maska
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
co	co	k3yQnSc1	co
nejvíce	nejvíce	k6eAd1	nejvíce
připodobnil	připodobnit	k5eAaPmAgMnS	připodobnit
lovenému	lovený	k2eAgNnSc3d1	lovené
zvířeti	zvíře	k1gNnSc3	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Maska	maska	k1gFnSc1	maska
se	se	k3xPyFc4	se
tak	tak	k9	tak
možná	možná	k9	možná
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
divadelních	divadelní	k2eAgFnPc2d1	divadelní
rekvizit	rekvizita	k1gFnPc2	rekvizita
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
starověké	starověký	k2eAgNnSc1d1	starověké
řecké	řecký	k2eAgNnSc1d1	řecké
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
chápalo	chápat	k5eAaImAgNnS	chápat
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
kultu	kult	k1gInSc2	kult
a	a	k8xC	a
uprostřed	uprostřed	k7c2	uprostřed
jeviště	jeviště	k1gNnSc2	jeviště
(	(	kIx(	(
<g/>
orchestra	orchestra	k1gFnSc1	orchestra
<g/>
)	)	kIx)	)
stával	stávat	k5eAaImAgInS	stávat
obětní	obětní	k2eAgInSc1d1	obětní
oltář	oltář	k1gInSc1	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
divadlo	divadlo	k1gNnSc4	divadlo
provozované	provozovaný	k2eAgNnSc4d1	provozované
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
rituálů	rituál	k1gInPc2	rituál
a	a	k8xC	a
náboženských	náboženský	k2eAgInPc2d1	náboženský
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
oslavy	oslava	k1gFnPc4	oslava
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tehdy	tehdy	k6eAd1	tehdy
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
.	.	kIx.	.
</s>
<s>
Předváděna	předváděn	k2eAgFnSc1d1	předváděna
byla	být	k5eAaImAgFnS	být
Mardukova	Mardukův	k2eAgFnSc1d1	Mardukova
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
Sarpanit	Sarpanit	k1gInSc1	Sarpanit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
symbolizováno	symbolizován	k2eAgNnSc1d1	symbolizováno
rozsekáním	rozsekání	k1gNnSc7	rozsekání
a	a	k8xC	a
spálením	spálení	k1gNnSc7	spálení
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
byly	být	k5eAaImAgFnP	být
poté	poté	k6eAd1	poté
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Rituál	rituál	k1gInSc1	rituál
souvisel	souviset	k5eAaImAgInS	souviset
se	s	k7c7	s
vzkříšením	vzkříšení	k1gNnSc7	vzkříšení
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
se	se	k3xPyFc4	se
kultivovaným	kultivovaný	k2eAgInSc7d1	kultivovaný
tanečním	taneční	k2eAgInSc7d1	taneční
projevem	projev	k1gInSc7	projev
a	a	k8xC	a
integrací	integrace	k1gFnSc7	integrace
hudby	hudba	k1gFnSc2	hudba
i	i	k8xC	i
akrobatických	akrobatický	k2eAgInPc2d1	akrobatický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
egyptské	egyptský	k2eAgFnSc2d1	egyptská
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešních	dnešní	k2eAgFnPc2d1	dnešní
dob	doba	k1gFnPc2	doba
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
svitků	svitek	k1gInPc2	svitek
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
egyptských	egyptský	k2eAgMnPc2d1	egyptský
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
Usírův	Usírův	k2eAgInSc1d1	Usírův
cyklus	cyklus	k1gInSc1	cyklus
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
řeckého	řecký	k2eAgNnSc2d1	řecké
divadla	divadlo	k1gNnSc2	divadlo
můžeme	moct	k5eAaImIp1nP	moct
sledovat	sledovat	k5eAaImF	sledovat
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastal	nastat	k5eAaPmAgInS	nastat
jeho	jeho	k3xOp3gInSc4	jeho
úpadek	úpadek	k1gInSc4	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgNnSc1d1	divadelní
umění	umění	k1gNnSc1	umění
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
z	z	k7c2	z
náboženských	náboženský	k2eAgFnPc2d1	náboženská
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dionýsií	dionýsie	k1gFnPc2	dionýsie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
sborový	sborový	k2eAgInSc1d1	sborový
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
živé	živý	k2eAgInPc1d1	živý
obrazy	obraz	k1gInPc1	obraz
a	a	k8xC	a
tanec	tanec	k1gInSc1	tanec
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
doplnil	doplnit	k5eAaPmAgMnS	doplnit
dramatický	dramatický	k2eAgInSc4d1	dramatický
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
předváděný	předváděný	k2eAgInSc4d1	předváděný
jedním	jeden	k4xCgInSc7	jeden
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
třemi	tři	k4xCgInPc7	tři
herci	herc	k1gInPc7	herc
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
se	se	k3xPyFc4	se
pořádala	pořádat	k5eAaImAgFnS	pořádat
o	o	k7c6	o
velkých	velký	k2eAgInPc6d1	velký
svátcích	svátek	k1gInPc6	svátek
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
podobu	podoba	k1gFnSc4	podoba
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
však	však	k9	však
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Thespis	Thespis	k1gMnSc1	Thespis
snad	snad	k9	snad
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
636-633	[number]	k4	636-633
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
první	první	k4xOgFnSc3	první
tragédii	tragédie	k1gFnSc3	tragédie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pokládala	pokládat	k5eAaImAgFnS	pokládat
za	za	k7c4	za
vyšší	vysoký	k2eAgFnSc4d2	vyšší
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
připojila	připojit	k5eAaPmAgFnS	připojit
i	i	k9	i
komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1	řecké
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
položilo	položit	k5eAaPmAgNnS	položit
základ	základ	k1gInSc4	základ
evropskému	evropský	k2eAgNnSc3d1	Evropské
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
divadlo	divadlo	k1gNnSc4	divadlo
navázal	navázat	k5eAaPmAgInS	navázat
starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tragédie	tragédie	k1gFnSc1	tragédie
i	i	k8xC	i
komedie	komedie	k1gFnPc1	komedie
se	se	k3xPyFc4	se
hrály	hrát	k5eAaImAgFnP	hrát
v	v	k7c6	v
přírodních	přírodní	k2eAgNnPc6d1	přírodní
divadlech	divadlo	k1gNnPc6	divadlo
(	(	kIx(	(
<g/>
theatron	theatron	k1gInSc1	theatron
od	od	k7c2	od
theaomai	theaoma	k1gFnSc2	theaoma
<g/>
,	,	kIx,	,
dívat	dívat	k5eAaImF	dívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stavěla	stavět	k5eAaImAgFnS	stavět
na	na	k7c6	na
návrších	návrš	k1gFnPc6	návrš
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgInSc1d1	divadelní
prostor	prostor	k1gInSc1	prostor
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Theatron	Theatron	k1gInSc4	Theatron
tj.	tj.	kA	tj.
stupňovitý	stupňovitý	k2eAgMnSc1d1	stupňovitý
<g/>
,	,	kIx,	,
terasovitý	terasovitý	k2eAgInSc1d1	terasovitý
půlkruhový	půlkruhový	k2eAgInSc4d1	půlkruhový
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
sedící	sedící	k2eAgMnPc4d1	sedící
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
divadla	divadlo	k1gNnPc1	divadlo
mohla	moct	k5eAaImAgNnP	moct
pojmout	pojmout	k5eAaPmF	pojmout
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
tisíc	tisíc	k4xCgInSc4	tisíc
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
Epidauros	Epidaurosa	k1gFnPc2	Epidaurosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orchestra	orchestra	k1gFnSc1	orchestra
tj.	tj.	kA	tj.
kruhový	kruhový	k2eAgMnSc1d1	kruhový
či	či	k8xC	či
půlkruhový	půlkruhový	k2eAgInSc1d1	půlkruhový
prostor	prostor	k1gInSc1	prostor
dole	dole	k6eAd1	dole
uprostřed	uprostřed	k7c2	uprostřed
hlediště	hlediště	k1gNnSc2	hlediště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
původně	původně	k6eAd1	původně
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
pouze	pouze	k6eAd1	pouze
chór	chór	k1gInSc1	chór
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
herci	herec	k1gMnPc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Skené	Skené	k2eAgFnSc1d1	Skené
čili	čili	k8xC	čili
divadelní	divadelní	k2eAgFnSc1d1	divadelní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
patrová	patrový	k2eAgFnSc1d1	patrová
<g/>
,	,	kIx,	,
se	s	k7c7	s
sloupovím	sloupoví	k1gNnSc7	sloupoví
<g/>
,	,	kIx,	,
balkony	balkon	k1gInPc7	balkon
a	a	k8xC	a
branami	brána	k1gFnPc7	brána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uzavírala	uzavírat	k5eAaPmAgFnS	uzavírat
orchestru	orchestra	k1gFnSc4	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
proscénium	proscénium	k1gNnSc1	proscénium
<g/>
,	,	kIx,	,
vyvýšené	vyvýšený	k2eAgNnSc1d1	vyvýšené
nad	nad	k7c7	nad
orchestrou	orchestra	k1gFnSc7	orchestra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
herci	herec	k1gMnPc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Římané	Říman	k1gMnPc1	Říman
zavedli	zavést	k5eAaPmAgMnP	zavést
oponu	opona	k1gFnSc4	opona
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
naše	náš	k3xOp1gFnSc1	náš
předscéna	předscéna	k1gFnSc1	předscéna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
divadelní	divadelní	k2eAgFnPc4d1	divadelní
hry	hra	k1gFnPc4	hra
měly	mít	k5eAaImAgInP	mít
přesná	přesný	k2eAgFnSc1d1	přesná
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
přísná	přísný	k2eAgNnPc1d1	přísné
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
:	:	kIx,	:
ještě	ještě	k9	ještě
v	v	k7c6	v
Aischylových	Aischylový	k2eAgFnPc6d1	Aischylový
hrách	hra	k1gFnPc6	hra
převládá	převládat	k5eAaImIp3nS	převládat
úloha	úloha	k1gFnSc1	úloha
sboru	sbor	k1gInSc2	sbor
hráli	hrát	k5eAaImAgMnP	hrát
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
i	i	k8xC	i
ženské	ženský	k2eAgFnPc4d1	ženská
role	role	k1gFnPc4	role
od	od	k7c2	od
Aischyla	Aischyla	k1gFnSc2	Aischyla
hráli	hrát	k5eAaImAgMnP	hrát
dva	dva	k4xCgMnPc1	dva
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
třetího	třetí	k4xOgNnSc2	třetí
zavedl	zavést	k5eAaPmAgMnS	zavést
Sofokles	Sofokles	k1gMnSc1	Sofokles
<g/>
;	;	kIx,	;
každý	každý	k3xTgMnSc1	každý
však	však	k9	však
mohl	moct	k5eAaImAgMnS	moct
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
více	hodně	k6eAd2	hodně
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
hlasem	hlasem	k6eAd1	hlasem
hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
ve	v	k7c6	v
slavnostních	slavnostní	k2eAgInPc6d1	slavnostní
kostýmech	kostým	k1gInPc6	kostým
s	s	k7c7	s
maskami	maska	k1gFnPc7	maska
<g/>
.	.	kIx.	.
</s>
<s>
Maska	maska	k1gFnSc1	maska
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
danou	daný	k2eAgFnSc4d1	daná
postavu	postava	k1gFnSc4	postava
a	a	k8xC	a
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
role	role	k1gFnSc2	role
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
maska	maska	k1gFnSc1	maska
<g/>
,	,	kIx,	,
ne	ne	k9	ne
oblek	oblek	k1gInSc1	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Dutá	dutý	k2eAgFnSc1d1	dutá
maska	maska	k1gFnSc1	maska
a	a	k8xC	a
paruka	paruka	k1gFnSc1	paruka
zvýrazňovala	zvýrazňovat	k5eAaImAgFnS	zvýrazňovat
nějakou	nějaký	k3yIgFnSc4	nějaký
typickou	typický	k2eAgFnSc4d1	typická
vlastnost	vlastnost	k1gFnSc4	vlastnost
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
podporovala	podporovat	k5eAaImAgFnS	podporovat
slyšitelnost	slyšitelnost	k1gFnSc1	slyšitelnost
mluveného	mluvený	k2eAgNnSc2d1	mluvené
slova	slovo	k1gNnSc2	slovo
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Thespis	Thespis	k1gMnSc1	Thespis
(	(	kIx(	(
<g/>
asi	asi	k9	asi
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Aischylos	Aischylos	k1gMnSc1	Aischylos
(	(	kIx(	(
<g/>
asi	asi	k9	asi
525	[number]	k4	525
<g/>
–	–	k?	–
<g/>
456	[number]	k4	456
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Sofoklés	Sofoklés	k1gInSc1	Sofoklés
(	(	kIx(	(
<g/>
asi	asi	k9	asi
496	[number]	k4	496
–	–	k?	–
405	[number]	k4	405
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Aristofanés	Aristofanés	k1gInSc1	Aristofanés
(	(	kIx(	(
<g/>
asi	asi	k9	asi
445	[number]	k4	445
–	–	k?	–
380	[number]	k4	380
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
Starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
starořeckého	starořecký	k2eAgNnSc2d1	starořecké
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
i	i	k9	i
prvky	prvek	k1gInPc7	prvek
etruského	etruský	k2eAgNnSc2d1	etruské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
spojili	spojit	k5eAaPmAgMnP	spojit
amfiteatrální	amfiteatrální	k2eAgNnSc4d1	amfiteatrální
hlediště	hlediště	k1gNnSc4	hlediště
s	s	k7c7	s
divadelní	divadelní	k2eAgFnSc7d1	divadelní
budovou	budova	k1gFnSc7	budova
(	(	kIx(	(
<g/>
scaena	scaena	k1gFnSc1	scaena
<g/>
)	)	kIx)	)
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
proscenium	proscenium	k1gNnSc1	proscenium
bylo	být	k5eAaImAgNnS	být
nižší	nízký	k2eAgMnSc1d2	nižší
a	a	k8xC	a
hlubší	hluboký	k2eAgMnSc1d2	hlubší
a	a	k8xC	a
orchestra	orchestra	k1gFnSc1	orchestra
polokruhová	polokruhový	k2eAgFnSc1d1	polokruhová
<g/>
.	.	kIx.	.
</s>
<s>
Zavedli	zavést	k5eAaPmAgMnP	zavést
také	také	k9	také
oponu	opona	k1gFnSc4	opona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vytahovala	vytahovat	k5eAaImAgFnS	vytahovat
zespodu	zespodu	k6eAd1	zespodu
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgNnPc1d1	římské
divadla	divadlo	k1gNnPc1	divadlo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stavěla	stavět	k5eAaImAgFnS	stavět
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
daleko	daleko	k6eAd1	daleko
náročnější	náročný	k2eAgNnSc1d2	náročnější
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
kamenné	kamenný	k2eAgNnSc1d1	kamenné
divadlo	divadlo	k1gNnSc1	divadlo
dal	dát	k5eAaPmAgInS	dát
postavit	postavit	k5eAaPmF	postavit
Pompeius	Pompeius	k1gInSc1	Pompeius
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
roku	rok	k1gInSc2	rok
55	[number]	k4	55
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Představení	představení	k1gNnSc4	představení
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
slavností	slavnost	k1gFnPc2	slavnost
ludi	ludi	k6eAd1	ludi
Romani	Roman	k1gMnPc1	Roman
(	(	kIx(	(
<g/>
Římské	římský	k2eAgFnSc2d1	římská
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
–	–	k?	–
představení	představení	k1gNnSc4	představení
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
prostranstvích	prostranství	k1gNnPc6	prostranství
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
a	a	k8xC	a
kamenných	kamenný	k2eAgNnPc2d1	kamenné
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
císařské	císařský	k2eAgFnSc2d1	císařská
doby	doba	k1gFnSc2	doba
účelem	účel	k1gInSc7	účel
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
bylo	být	k5eAaImAgNnS	být
především	především	k6eAd1	především
bavit	bavit	k5eAaImF	bavit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
státnického	státnický	k2eAgNnSc2d1	státnické
hesla	heslo	k1gNnSc2	heslo
panem	pan	k1gMnSc7	pan
et	et	k?	et
circenses	circenses	k1gInSc1	circenses
–	–	k?	–
chléb	chléb	k1gInSc1	chléb
a	a	k8xC	a
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
termín	termín	k1gInSc1	termín
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
histrio	histrio	k1gMnSc1	histrio
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pojmenování	pojmenování	k1gNnSc2	pojmenování
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
aktérů	aktér	k1gMnPc2	aktér
(	(	kIx(	(
<g/>
ister	ister	k1gMnSc1	ister
<g/>
)	)	kIx)	)
etruských	etruský	k2eAgNnPc2d1	etruské
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
pantomimu	pantomima	k1gFnSc4	pantomima
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc4	tanec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
improvizovaný	improvizovaný	k2eAgInSc4d1	improvizovaný
dialog	dialog	k1gInSc4	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
přestali	přestat	k5eAaPmAgMnP	přestat
používat	používat	k5eAaImF	používat
masky	maska	k1gFnPc4	maska
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Etrusků	Etrusk	k1gMnPc2	Etrusk
líčili	líčit	k5eAaImAgMnP	líčit
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
nedávali	dávat	k5eNaImAgMnP	dávat
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
správnou	správný	k2eAgFnSc4d1	správná
výslovnost	výslovnost	k1gFnSc4	výslovnost
a	a	k8xC	a
procítěné	procítěný	k2eAgNnSc4d1	procítěné
deklamování	deklamování	k1gNnSc4	deklamování
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
výřečnost	výřečnost	k1gFnSc4	výřečnost
a	a	k8xC	a
přesvědčivost	přesvědčivost	k1gFnSc4	přesvědčivost
<g/>
,	,	kIx,	,
na	na	k7c4	na
výraznou	výrazný	k2eAgFnSc4d1	výrazná
mimiku	mimika	k1gFnSc4	mimika
a	a	k8xC	a
gestikulaci	gestikulace	k1gFnSc4	gestikulace
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
pokud	pokud	k8xS	pokud
nepoužívali	používat	k5eNaImAgMnP	používat
masky	maska	k1gFnPc4	maska
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
neomezovali	omezovat	k5eNaImAgMnP	omezovat
počet	počet	k1gInSc4	počet
herců	herc	k1gInPc2	herc
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
,	,	kIx,	,
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
zde	zde	k6eAd1	zde
tolik	tolik	k4xDc1	tolik
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
hra	hra	k1gFnSc1	hra
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
<g/>
.	.	kIx.	.
</s>
<s>
Ženské	ženský	k2eAgFnPc1d1	ženská
role	role	k1gFnPc1	role
také	také	k9	také
začaly	začít	k5eAaPmAgFnP	začít
hrát	hrát	k5eAaImF	hrát
ženy-herečky	ženyerečka	k1gFnPc1	ženy-herečka
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
především	především	k6eAd1	především
inscenovali	inscenovat	k5eAaBmAgMnP	inscenovat
latinské	latinský	k2eAgNnSc4d1	latinské
překlady	překlad	k1gInPc1	překlad
řeckých	řecký	k2eAgNnPc2d1	řecké
dramat	drama	k1gNnPc2	drama
<g/>
;	;	kIx,	;
komedie	komedie	k1gFnPc1	komedie
přebásněné	přebásněný	k2eAgFnPc1d1	přebásněná
podle	podle	k7c2	podle
řeckých	řecký	k2eAgFnPc2d1	řecká
předloh	předloha	k1gFnPc2	předloha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
fabulae	fabulae	k1gFnSc1	fabulae
palliatae	palliatae	k1gFnSc1	palliatae
a	a	k8xC	a
komedie	komedie	k1gFnPc1	komedie
čerpající	čerpající	k2eAgFnPc1d1	čerpající
náměty	námět	k1gInPc4	námět
z	z	k7c2	z
římského	římský	k2eAgInSc2d1	římský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
fabulae	fabulae	k1gFnSc1	fabulae
togatae	togatae	k1gFnSc1	togatae
<g/>
.	.	kIx.	.
</s>
<s>
Titus	Titus	k1gMnSc1	Titus
Maccius	Maccius	k1gMnSc1	Maccius
Plautus	Plautus	k1gMnSc1	Plautus
Publius	Publius	k1gMnSc1	Publius
Terentius	Terentius	k1gMnSc1	Terentius
Afer	Afer	k1gMnSc1	Afer
Mezi	mezi	k7c7	mezi
publikem	publikum	k1gNnSc7	publikum
byly	být	k5eAaImAgFnP	být
nejoblíbenější	oblíbený	k2eAgFnPc1d3	nejoblíbenější
tzv.	tzv.	kA	tzv.
attelánské	attelánský	k2eAgFnSc2d1	attelánský
frašky	fraška	k1gFnSc2	fraška
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgNnSc1d1	římské
divadlo	divadlo	k1gNnSc1	divadlo
nikdy	nikdy	k6eAd1	nikdy
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
tak	tak	k9	tak
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úrovně	úroveň	k1gFnSc2	úroveň
jako	jako	k8xS	jako
divadlo	divadlo	k1gNnSc1	divadlo
řecké	řecký	k2eAgNnSc1d1	řecké
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
však	však	k9	však
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Římané	Říman	k1gMnPc1	Říman
zprostředkovali	zprostředkovat	k5eAaPmAgMnP	zprostředkovat
řecké	řecký	k2eAgNnSc4d1	řecké
divadelní	divadelní	k2eAgNnSc4d1	divadelní
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
pokrok	pokrok	k1gInSc4	pokrok
dalším	další	k2eAgInPc3d1	další
národům	národ	k1gInPc3	národ
žijícím	žijící	k2eAgInPc3d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
říše	říš	k1gFnSc2	říš
upadlo	upadnout	k5eAaPmAgNnS	upadnout
i	i	k9	i
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
považováno	považován	k2eAgNnSc1d1	považováno
nastupující	nastupující	k2eAgFnSc7d1	nastupující
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
církví	církev	k1gFnSc7	církev
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgNnSc4d1	nežádoucí
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
středověkého	středověký	k2eAgNnSc2d1	středověké
divadla	divadlo	k1gNnSc2	divadlo
trval	trvat	k5eAaImAgInS	trvat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
sice	sice	k8xC	sice
udržovala	udržovat	k5eAaImAgFnS	udržovat
antickou	antický	k2eAgFnSc4d1	antická
literární	literární	k2eAgFnSc4d1	literární
i	i	k8xC	i
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
populární	populární	k2eAgNnSc1d1	populární
pozdně	pozdně	k6eAd1	pozdně
římské	římský	k2eAgNnSc1d1	římské
divadlo	divadlo	k1gNnSc1	divadlo
však	však	k9	však
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
pohanský	pohanský	k2eAgInSc4d1	pohanský
ráz	ráz	k1gInSc4	ráz
odmítala	odmítat	k5eAaImAgFnS	odmítat
a	a	k8xC	a
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
věřícím	věřící	k1gMnPc3	věřící
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sa	sa	k?	sa
divadlo	divadlo	k1gNnSc1	divadlo
začalo	začít	k5eAaPmAgNnS	začít
rodit	rodit	k5eAaImF	rodit
znova	znova	k6eAd1	znova
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
kultu	kult	k1gInSc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
oslovit	oslovit	k5eAaPmF	oslovit
i	i	k9	i
negramotné	gramotný	k2eNgFnPc4d1	negramotná
lidové	lidový	k2eAgFnPc4d1	lidová
masy	masa	k1gFnPc4	masa
a	a	k8xC	a
přiblížit	přiblížit	k5eAaPmF	přiblížit
jim	on	k3xPp3gMnPc3	on
biblické	biblický	k2eAgMnPc4d1	biblický
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostelech	kostel	k1gInPc6	kostel
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
hrát	hrát	k5eAaImF	hrát
latinské	latinský	k2eAgInPc1d1	latinský
dramatické	dramatický	k2eAgInPc1d1	dramatický
výstupy	výstup	k1gInPc1	výstup
při	při	k7c6	při
velikonočních	velikonoční	k2eAgFnPc6d1	velikonoční
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
velikonoční	velikonoční	k2eAgInPc1d1	velikonoční
tropy	trop	k1gInPc1	trop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgInP	vznikat
i	i	k9	i
další	další	k2eAgInPc1d1	další
náboženské	náboženský	k2eAgInPc1d1	náboženský
žánry	žánr	k1gInPc1	žánr
jako	jako	k8xS	jako
mystérium	mystérium	k1gNnSc1	mystérium
<g/>
,	,	kIx,	,
mirákulum	mirákulum	k1gNnSc1	mirákulum
a	a	k8xC	a
moralita	moralita	k1gFnSc1	moralita
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
náboženských	náboženský	k2eAgInPc2d1	náboženský
žánrů	žánr	k1gInPc2	žánr
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
později	pozdě	k6eAd2	pozdě
i	i	k9	i
žánry	žánr	k1gInPc4	žánr
světské	světský	k2eAgInPc4d1	světský
–	–	k?	–
plebejské	plebejský	k2eAgNnSc1d1	plebejské
<g/>
:	:	kIx,	:
fraška	fraška	k1gFnSc1	fraška
a	a	k8xC	a
pastorála	pastorála	k1gFnSc1	pastorála
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
budovou	budova	k1gFnSc7	budova
pro	pro	k7c4	pro
středověké	středověký	k2eAgNnSc4d1	středověké
divadlo	divadlo	k1gNnSc4	divadlo
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dramatické	dramatický	k2eAgInPc1d1	dramatický
výstupy	výstup	k1gInPc1	výstup
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
nejprve	nejprve	k6eAd1	nejprve
při	při	k7c6	při
hlavním	hlavní	k2eAgMnSc6d1	hlavní
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
u	u	k7c2	u
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
oltářů	oltář	k1gInPc2	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
divadlo	divadlo	k1gNnSc1	divadlo
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
veřejná	veřejný	k2eAgNnPc4d1	veřejné
prostranství	prostranství	k1gNnSc4	prostranství
–	–	k?	–
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
simultánní	simultánní	k2eAgNnSc1d1	simultánní
jeviště	jeviště	k1gNnSc1	jeviště
<g/>
,	,	kIx,	,
simultánní	simultánní	k2eAgFnSc4d1	simultánní
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
společné	společný	k2eAgNnSc1d1	společné
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
výstupy	výstup	k1gInPc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
bývaly	bývat	k5eAaImAgInP	bývat
umístěny	umístit	k5eAaPmNgInP	umístit
malé	malý	k2eAgInPc1d1	malý
domečky	domeček	k1gInPc1	domeček
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mansiony	mansion	k1gInPc1	mansion
–	–	k?	–
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
byla	být	k5eAaImAgFnS	být
namalována	namalován	k2eAgFnSc1d1	namalována
dekorace	dekorace	k1gFnSc1	dekorace
a	a	k8xC	a
herci	herec	k1gMnPc1	herec
stáli	stát	k5eAaImAgMnP	stát
vždy	vždy	k6eAd1	vždy
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgMnS	odehrávat
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
humanistického	humanistický	k2eAgNnSc2d1	humanistické
a	a	k8xC	a
renesančního	renesanční	k2eAgNnSc2d1	renesanční
divadla	divadlo	k1gNnSc2	divadlo
sahá	sahat	k5eAaImIp3nS	sahat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bohatých	bohatý	k2eAgNnPc6d1	bohaté
městech	město	k1gNnPc6	město
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
objevuje	objevovat	k5eAaImIp3nS	objevovat
nové	nový	k2eAgNnSc1d1	nové
pojetí	pojetí	k1gNnSc1	pojetí
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
(	(	kIx(	(
<g/>
Giotto	Giotto	k1gNnSc1	Giotto
di	di	k?	di
Bondone	Bondon	k1gMnSc5	Bondon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
v	v	k7c6	v
básnictví	básnictví	k1gNnSc6	básnictví
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dante	Dante	k1gMnSc1	Dante
Alighieri	Alighier	k1gFnSc2	Alighier
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
náboženských	náboženský	k2eAgFnPc6d1	náboženská
básních	báseň	k1gFnPc6	báseň
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
objevem	objev	k1gInSc7	objev
a	a	k8xC	a
překlady	překlad	k1gInPc1	překlad
zejména	zejména	k9	zejména
řecké	řecký	k2eAgFnSc2d1	řecká
starověké	starověký	k2eAgFnSc2d1	starověká
literatury	literatura	k1gFnSc2	literatura
vzniká	vznikat	k5eAaImIp3nS	vznikat
renesanční	renesanční	k2eAgInSc4d1	renesanční
humanismus	humanismus	k1gInSc4	humanismus
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
asketickým	asketický	k2eAgInPc3d1	asketický
ideálům	ideál	k1gInPc3	ideál
středověku	středověk	k1gInSc2	středověk
staví	stavit	k5eAaBmIp3nS	stavit
ideál	ideál	k1gInSc1	ideál
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
radosti	radost	k1gFnSc2	radost
<g/>
,	,	kIx,	,
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
starověké	starověký	k2eAgNnSc4d1	starověké
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1531	[number]	k4	1531
ve	v	k7c6	v
Ferraře	Ferrara	k1gFnSc6	Ferrara
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1545	[number]	k4	1545
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
první	první	k4xOgNnPc4	první
krytá	krytý	k2eAgNnPc4d1	kryté
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
Teatro	Teatro	k1gNnSc1	Teatro
Olimpico	Olimpico	k6eAd1	Olimpico
ve	v	k7c6	v
Vicenze	Vicenz	k1gInSc6	Vicenz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1585	[number]	k4	1585
<g/>
.	.	kIx.	.
</s>
<s>
Andrea	Andrea	k1gFnSc1	Andrea
Palladio	Palladio	k6eAd1	Palladio
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
důkladného	důkladný	k2eAgNnSc2d1	důkladné
studia	studio	k1gNnSc2	studio
římských	římský	k2eAgNnPc2d1	římské
divadel	divadlo	k1gNnPc2	divadlo
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obnovit	obnovit	k5eAaPmF	obnovit
přesnou	přesný	k2eAgFnSc4d1	přesná
podobu	podoba	k1gFnSc4	podoba
starověkého	starověký	k2eAgNnSc2d1	starověké
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
<g/>
)	)	kIx)	)
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
Sofoklovou	Sofoklův	k2eAgFnSc7d1	Sofoklova
tragédií	tragédie	k1gFnSc7	tragédie
"	"	kIx"	"
<g/>
Oidipus	Oidipus	k1gMnSc1	Oidipus
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
také	také	k6eAd1	také
nové	nový	k2eAgInPc4d1	nový
divadelní	divadelní	k2eAgInPc4d1	divadelní
žánry	žánr	k1gInPc4	žánr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
Claudio	Claudio	k6eAd1	Claudio
Monteverdi	Monteverd	k1gMnPc1	Monteverd
<g/>
,	,	kIx,	,
Orfeo	Orfeo	k6eAd1	Orfeo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1607	[number]	k4	1607
<g/>
)	)	kIx)	)
a	a	k8xC	a
balet	balet	k1gInSc1	balet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
operní	operní	k2eAgNnSc1d1	operní
divadlo	divadlo	k1gNnSc1	divadlo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Změnily	změnit	k5eAaPmAgInP	změnit
se	se	k3xPyFc4	se
i	i	k9	i
inscenační	inscenační	k2eAgFnSc2d1	inscenační
techniky	technika	k1gFnSc2	technika
–	–	k?	–
simultánní	simultánní	k2eAgNnSc1d1	simultánní
jeviště	jeviště	k1gNnSc1	jeviště
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
jeviště	jeviště	k1gNnSc4	jeviště
s	s	k7c7	s
pohyblivými	pohyblivý	k2eAgFnPc7d1	pohyblivá
dekoracemi	dekorace	k1gFnPc7	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dekorací	dekorace	k1gFnPc2	dekorace
Italové	Ital	k1gMnPc1	Ital
využili	využít	k5eAaPmAgMnP	využít
objev	objev	k1gInSc4	objev
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnPc1	dekorace
byly	být	k5eAaImAgFnP	být
malované	malovaný	k2eAgInPc1d1	malovaný
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
napnuto	napnout	k5eAaPmNgNnS	napnout
na	na	k7c6	na
trojbokých	trojboký	k2eAgInPc6d1	trojboký
hranolech	hranol	k1gInPc6	hranol
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
telari	telar	k1gInPc7	telar
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
předchůdce	předchůdce	k1gMnSc2	předchůdce
dnešních	dnešní	k2eAgFnPc2d1	dnešní
kulis	kulisa	k1gFnPc2	kulisa
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
uspořádání	uspořádání	k1gNnSc1	uspořádání
jeviště	jeviště	k1gNnSc2	jeviště
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
našich	náš	k3xOp1gFnPc2	náš
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
:	:	kIx,	:
Commedia	Commedium	k1gNnPc1	Commedium
dell	della	k1gFnPc2	della
<g/>
'	'	kIx"	'
<g/>
arte	arte	k1gFnSc4	arte
<g/>
,	,	kIx,	,
Alžbětinské	alžbětinský	k2eAgNnSc4d1	alžbětinské
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barokní	barokní	k2eAgNnSc1d1	barokní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pronikl	proniknout	k5eAaPmAgInS	proniknout
barok	barok	k1gInSc1	barok
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
sfér	sféra	k1gFnPc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc4	divadlo
obohatil	obohatit	k5eAaPmAgMnS	obohatit
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
monumentálnost	monumentálnost	k1gFnSc4	monumentálnost
a	a	k8xC	a
technické	technický	k2eAgFnPc4d1	technická
vymoženosti	vymoženost	k1gFnPc4	vymoženost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městech	město	k1gNnPc6	město
vznikaly	vznikat	k5eAaImAgFnP	vznikat
reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
divadelní	divadelní	k2eAgFnPc1d1	divadelní
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Jeviště	jeviště	k1gNnSc1	jeviště
se	se	k3xPyFc4	se
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
na	na	k7c4	na
kulisové	kulisový	k2eAgInPc4d1	kulisový
nebo	nebo	k8xC	nebo
také	také	k9	také
kukátkové	kukátkový	k2eAgNnSc1d1	kukátkové
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
prostor	prostor	k1gInSc4	prostor
před	před	k7c7	před
oponou	opona	k1gFnSc7	opona
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
rampa	rampa	k1gFnSc1	rampa
<g/>
.	.	kIx.	.
</s>
<s>
Jevištní	jevištní	k2eAgInSc1d1	jevištní
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
párů	pár	k1gInPc2	pár
malovaných	malovaný	k2eAgFnPc2d1	malovaná
stěn	stěna	k1gFnPc2	stěna
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
(	(	kIx(	(
<g/>
kulisy	kulisa	k1gFnPc4	kulisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
rampou	rampa	k1gFnSc7	rampa
a	a	k8xC	a
oddělených	oddělený	k2eAgFnPc6d1	oddělená
uličkami	ulička	k1gFnPc7	ulička
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc4	prostor
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
zadní	zadní	k2eAgFnSc1d1	zadní
stěna	stěna	k1gFnSc1	stěna
–	–	k?	–
prospekt	prospekt	k1gInSc4	prospekt
a	a	k8xC	a
nahoře	nahoře	k6eAd1	nahoře
sufity	sufita	k1gFnSc2	sufita
–	–	k?	–
závěsné	závěsný	k2eAgInPc4d1	závěsný
pásy	pás	k1gInPc4	pás
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
jevištní	jevištní	k2eAgFnSc2d1	jevištní
dekorace	dekorace	k1gFnSc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
kulis	kulisa	k1gFnPc2	kulisa
umožnil	umožnit	k5eAaPmAgInS	umožnit
využít	využít	k5eAaPmF	využít
celou	celý	k2eAgFnSc4d1	celá
hloubku	hloubka	k1gFnSc4	hloubka
jeviště	jeviště	k1gNnSc2	jeviště
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
změnit	změnit	k5eAaPmF	změnit
scénu	scéna	k1gFnSc4	scéna
během	během	k7c2	během
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takové	takový	k3xDgFnSc2	takový
značně	značně	k6eAd1	značně
mechanizované	mechanizovaný	k2eAgFnSc2d1	mechanizovaná
barokní	barokní	k2eAgFnSc2d1	barokní
scény	scéna	k1gFnSc2	scéna
je	být	k5eAaImIp3nS	být
zámecké	zámecký	k2eAgNnSc4d1	zámecké
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jevištní	jevištní	k2eAgInSc1d1	jevištní
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
s	s	k7c7	s
jistými	jistý	k2eAgFnPc7d1	jistá
úpravami	úprava	k1gFnPc7	úprava
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
a	a	k8xC	a
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
klasicistické	klasicistický	k2eAgNnSc4d1	klasicistické
umění	umění	k1gNnSc4	umění
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
antika	antika	k1gFnSc1	antika
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
viděli	vidět	k5eAaImAgMnP	vidět
smysl	smysl	k1gInSc4	smysl
v	v	k7c4	v
napodobování	napodobování	k1gNnSc4	napodobování
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
sa	sa	k?	sa
utvářel	utvářet	k5eAaImAgInS	utvářet
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
racionalistické	racionalistický	k2eAgFnSc2d1	racionalistická
filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
nesmělo	smět	k5eNaImAgNnS	smět
protiřečit	protiřečit	k5eAaImF	protiřečit
rozumu	rozum	k1gInSc3	rozum
<g/>
,	,	kIx,	,
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
zákon	zákon	k1gInSc4	zákon
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
soulad	soulad	k1gInSc1	soulad
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
dobra	dobro	k1gNnSc2	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Corneille	Corneill	k1gMnSc5	Corneill
Jean	Jean	k1gMnSc1	Jean
Racine	Racin	k1gInSc5	Racin
Moliè	Moliè	k1gMnSc6	Moliè
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
německého	německý	k2eAgInSc2d1	německý
klasicismu	klasicismus	k1gInSc2	klasicismus
stál	stát	k5eAaImAgMnS	stát
profesor	profesor	k1gMnSc1	profesor
Johann	Johann	k1gMnSc1	Johann
Christoph	Christoph	k1gMnSc1	Christoph
Gottsched	Gottsched	k1gMnSc1	Gottsched
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
francouzské	francouzský	k2eAgNnSc4d1	francouzské
klasické	klasický	k2eAgNnSc4d1	klasické
drama	drama	k1gNnSc4	drama
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
divadelní	divadelní	k2eAgFnSc2d1	divadelní
ředitelky	ředitelka	k1gFnSc2	ředitelka
a	a	k8xC	a
herečky	herečka	k1gFnSc2	herečka
Friederiky	Friederika	k1gFnSc2	Friederika
Karoliny	Karolinum	k1gNnPc7	Karolinum
Neuberové	Neuberové	k2eAgFnSc4d1	Neuberové
reformoval	reformovat	k5eAaBmAgMnS	reformovat
německé	německý	k2eAgNnSc4d1	německé
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
očistit	očistit	k5eAaPmF	očistit
Německo	Německo	k1gNnSc1	Německo
od	od	k7c2	od
laciných	laciný	k2eAgFnPc2d1	laciná
a	a	k8xC	a
bezcenných	bezcenný	k2eAgFnPc2d1	bezcenná
her	hra	k1gFnPc2	hra
a	a	k8xC	a
nahradit	nahradit	k5eAaPmF	nahradit
je	on	k3xPp3gInPc4	on
překlady	překlad	k1gInPc4	překlad
francouzské	francouzský	k2eAgFnSc2d1	francouzská
klasiky	klasika	k1gFnSc2	klasika
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc7d1	vlastní
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Gotthold	Gotthold	k1gMnSc1	Gotthold
Ephraim	Ephraim	k1gMnSc1	Ephraim
Lessing	Lessing	k1gInSc4	Lessing
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
tvůrce	tvůrce	k1gMnSc4	tvůrce
německého	německý	k2eAgNnSc2d1	německé
měšťanského	měšťanský	k2eAgNnSc2d1	měšťanské
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Preromantismus	preromantismus	k1gInSc1	preromantismus
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
dramatu	drama	k1gNnSc6	drama
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
skupiny	skupina	k1gFnSc2	skupina
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Dranga	k1gFnPc2	Dranga
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
jak	jak	k8xS	jak
proti	proti	k7c3	proti
aristokratickému	aristokratický	k2eAgInSc3d1	aristokratický
klasicismu	klasicismus	k1gInSc3	klasicismus
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
také	také	k9	také
proti	proti	k7c3	proti
osvícenskému	osvícenský	k2eAgInSc3d1	osvícenský
racionalismu	racionalismus	k1gInSc3	racionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
zdůrazňovalo	zdůrazňovat	k5eAaImAgNnS	zdůrazňovat
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
svobodu	svoboda	k1gFnSc4	svoboda
osobnosti	osobnost	k1gFnSc2	osobnost
odmítající	odmítající	k2eAgFnSc2d1	odmítající
společenské	společenský	k2eAgFnPc4d1	společenská
konvence	konvence	k1gFnPc4	konvence
a	a	k8xC	a
předsudky	předsudek	k1gInPc4	předsudek
a	a	k8xC	a
řídící	řídící	k2eAgNnPc4d1	řídící
se	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňovalo	upřednostňovat	k5eAaImAgNnS	upřednostňovat
cit	cit	k1gInSc4	cit
a	a	k8xC	a
vášeň	vášeň	k1gFnSc4	vášeň
před	před	k7c7	před
racionalismem	racionalismus	k1gInSc7	racionalismus
a	a	k8xC	a
intelektualismem	intelektualismus	k1gInSc7	intelektualismus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
protikladu	protiklad	k1gInSc2	protiklad
k	k	k7c3	k
městu	město	k1gNnSc3	město
dávalo	dávat	k5eAaImAgNnS	dávat
volnost	volnost	k1gFnSc4	volnost
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Přiklánělo	přiklánět	k5eAaImAgNnS	přiklánět
se	se	k3xPyFc4	se
k	k	k7c3	k
lidovým	lidový	k2eAgFnPc3d1	lidová
tradicím	tradice	k1gFnPc3	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
představitelé	představitel	k1gMnPc1	představitel
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
jsou	být	k5eAaImIp3nP	být
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
–	–	k?	–
<g/>
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
proměny	proměna	k1gFnPc1	proměna
zastaralé	zastaralý	k2eAgFnPc1d1	zastaralá
commedia	commedium	k1gNnPc1	commedium
dell	della	k1gFnPc2	della
<g/>
'	'	kIx"	'
<g/>
Arte	Arte	k1gInSc1	Arte
ujal	ujmout	k5eAaPmAgInS	ujmout
Carlo	Carlo	k1gNnSc4	Carlo
Goldoni	Goldoň	k1gFnSc3	Goldoň
–	–	k?	–
nahradil	nahradit	k5eAaPmAgInS	nahradit
starou	starý	k2eAgFnSc4d1	stará
improvizovanou	improvizovaný	k2eAgFnSc4d1	improvizovaná
komedii	komedie	k1gFnSc4	komedie
typových	typový	k2eAgFnPc2d1	typová
figur	figura	k1gFnPc2	figura
charakterovou	charakterový	k2eAgFnSc7d1	charakterová
komedií	komedie	k1gFnSc7	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
nejznámější	známý	k2eAgFnPc1d3	nejznámější
hry	hra	k1gFnPc1	hra
<g/>
:	:	kIx,	:
Sluha	sluha	k1gMnSc1	sluha
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
Čertice	čertice	k1gFnSc2	čertice
Mirandolina	Mirandolina	k1gFnSc1	Mirandolina
Romantismus	romantismus	k1gInSc1	romantismus
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
příliš	příliš	k6eAd1	příliš
rozumové	rozumový	k2eAgNnSc4d1	rozumové
osvícenství	osvícenství	k1gNnSc4	osvícenství
a	a	k8xC	a
na	na	k7c4	na
vzdělance	vzdělanec	k1gMnPc4	vzdělanec
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
rozumu	rozum	k1gInSc2	rozum
hájili	hájit	k5eAaImAgMnP	hájit
romantici	romantik	k1gMnPc1	romantik
právo	právo	k1gNnSc4	právo
citu	cit	k1gInSc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadovali	vyžadovat	k5eAaImAgMnP	vyžadovat
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
originalitu	originalita	k1gFnSc4	originalita
<g/>
,	,	kIx,	,
vytváření	vytváření	k1gNnSc4	vytváření
neskutečných	skutečný	k2eNgInPc2d1	neskutečný
světů	svět	k1gInPc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc4	drama
obohatili	obohatit	k5eAaPmAgMnP	obohatit
o	o	k7c4	o
lyrické	lyrický	k2eAgInPc4d1	lyrický
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
opisy	opis	k1gInPc4	opis
duševních	duševní	k2eAgInPc2d1	duševní
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
základním	základní	k2eAgInSc7d1	základní
motivem	motiv	k1gInSc7	motiv
bylo	být	k5eAaImAgNnS	být
osobní	osobní	k2eAgNnSc1d1	osobní
rozčarování	rozčarování	k1gNnSc1	rozčarování
a	a	k8xC	a
zklamání	zklamání	k1gNnSc1	zklamání
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
pochmurné	pochmurný	k2eAgNnSc4d1	pochmurné
"	"	kIx"	"
<g/>
osudové	osudový	k2eAgNnSc4d1	osudové
<g/>
"	"	kIx"	"
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
neměl	mít	k5eNaImAgInS	mít
na	na	k7c4	na
divadlo	divadlo	k1gNnSc4	divadlo
podstatný	podstatný	k2eAgInSc1d1	podstatný
vliv	vliv	k1gInSc1	vliv
–	–	k?	–
působil	působit	k5eAaImAgInS	působit
však	však	k9	však
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
na	na	k7c4	na
herecké	herecký	k2eAgNnSc4d1	herecké
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vnesl	vnést	k5eAaPmAgMnS	vnést
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
pestrost	pestrost	k1gFnSc4	pestrost
<g/>
.	.	kIx.	.
</s>
<s>
Oslňoval	oslňovat	k5eAaImAgInS	oslňovat
rušným	rušný	k2eAgInSc7d1	rušný
dějem	děj	k1gInSc7	děj
<g/>
,	,	kIx,	,
fantazií	fantazie	k1gFnSc7	fantazie
<g/>
,	,	kIx,	,
tajuplností	tajuplnost	k1gFnSc7	tajuplnost
<g/>
,	,	kIx,	,
dobrodružstvím	dobrodružství	k1gNnSc7	dobrodružství
<g/>
,	,	kIx,	,
cizokrajností	cizokrajnost	k1gFnSc7	cizokrajnost
<g/>
,	,	kIx,	,
velkými	velký	k2eAgFnPc7d1	velká
tragédiemi	tragédie	k1gFnPc7	tragédie
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
vášně	vášeň	k1gFnSc2	vášeň
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc7d1	bohatá
výpravou	výprava	k1gFnSc7	výprava
a	a	k8xC	a
dekorací	dekorace	k1gFnSc7	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
George	Georg	k1gMnSc2	Georg
Gordon	Gordon	k1gMnSc1	Gordon
Noël	Noël	k1gMnSc1	Noël
Byron	Byron	k1gNnSc4	Byron
Percy	Perca	k1gMnSc2	Perca
Bysshe	Byssh	k1gMnSc2	Byssh
Shelley	Shellea	k1gMnSc2	Shellea
Alexandr	Alexandr	k1gMnSc1	Alexandr
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
Michail	Michail	k1gMnSc1	Michail
Jurjevič	Jurjevič	k1gMnSc1	Jurjevič
Lermontov	Lermontovo	k1gNnPc2	Lermontovo
Realismus	realismus	k1gInSc1	realismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k9	jako
protipól	protipól	k1gInSc4	protipól
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
podmíněný	podmíněný	k2eAgMnSc1d1	podmíněný
také	také	k9	také
politicky	politicky	k6eAd1	politicky
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
na	na	k7c4	na
svět	svět	k1gInSc4	svět
nazírat	nazírat	k5eAaImF	nazírat
bez	bez	k7c2	bez
iluzí	iluze	k1gFnPc2	iluze
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
(	(	kIx(	(
<g/>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
realis	realis	k1gFnSc2	realis
=	=	kIx~	=
skutečný	skutečný	k2eAgInSc1d1	skutečný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nástup	nástup	k1gInSc1	nástup
podpořil	podpořit	k5eAaPmAgInS	podpořit
i	i	k9	i
pokrok	pokrok	k1gInSc1	pokrok
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
→	→	k?	→
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
<g/>
.	.	kIx.	.
</s>
<s>
Preferováno	preferován	k2eAgNnSc4d1	preferováno
bylo	být	k5eAaImAgNnS	být
objektivní	objektivní	k2eAgNnSc1d1	objektivní
zkoumání	zkoumání	k1gNnSc1	zkoumání
<g/>
,	,	kIx,	,
metafyzika	metafyzika	k1gFnSc1	metafyzika
byla	být	k5eAaImAgFnS	být
zavržena	zavrhnout	k5eAaPmNgFnS	zavrhnout
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
si	se	k3xPyFc3	se
začalo	začít	k5eAaPmAgNnS	začít
osvojovat	osvojovat	k5eAaImF	osvojovat
metody	metoda	k1gFnPc4	metoda
objektivního	objektivní	k2eAgNnSc2d1	objektivní
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
realismus	realismus	k1gInSc4	realismus
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
metoda	metoda	k1gFnSc1	metoda
Meiningenčanů	Meiningenčan	k1gMnPc2	Meiningenčan
–	–	k?	–
divadelního	divadelní	k2eAgInSc2d1	divadelní
souboru	soubor	k1gInSc2	soubor
Jiřího	Jiří	k1gMnSc2	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
sasko-meiningenského	saskoeiningenský	k2eAgMnSc2d1	sasko-meiningenský
<g/>
.	.	kIx.	.
</s>
<s>
Usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
co	co	k3yQnSc4	co
nejvěrnější	věrný	k2eAgNnSc4d3	nejvěrnější
zobrazení	zobrazení	k1gNnSc4	zobrazení
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
nebo	nebo	k8xC	nebo
dekorace	dekorace	k1gFnPc1	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Naturalismus	naturalismus	k1gInSc1	naturalismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
natura	natura	k1gFnSc1	natura
=	=	kIx~	=
příroda	příroda	k1gFnSc1	příroda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
zobrazení	zobrazení	k1gNnSc2	zobrazení
skutečnosti	skutečnost	k1gFnSc2	skutečnost
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
filozofii	filozofie	k1gFnSc4	filozofie
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
a	a	k8xC	a
darwinismu	darwinismus	k1gInSc3	darwinismus
<g/>
.	.	kIx.	.
</s>
<s>
Opisoval	opisovat	k5eAaImAgMnS	opisovat
temné	temný	k2eAgFnPc4d1	temná
stránky	stránka	k1gFnPc4	stránka
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
se	se	k3xPyFc4	se
vulgárním	vulgární	k2eAgInSc7d1	vulgární
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
drastickými	drastický	k2eAgFnPc7d1	drastická
a	a	k8xC	a
krutými	krutý	k2eAgFnPc7d1	krutá
scénami	scéna	k1gFnPc7	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
však	však	k9	však
jen	jen	k6eAd1	jen
"	"	kIx"	"
<g/>
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nevysvětloval	vysvětlovat	k5eNaImAgMnS	vysvětlovat
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k9	jen
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnSc1	dekorace
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
odrazem	odraz	k1gInSc7	odraz
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzbuzovat	vzbuzovat	k5eAaImF	vzbuzovat
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
obraz	obraz	k1gInSc4	obraz
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Honoré	Honorý	k2eAgInPc1d1	Honorý
de	de	k?	de
Balzac	Balzac	k1gMnSc1	Balzac
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
Henrik	Henrik	k1gMnSc1	Henrik
Ibsen	Ibsen	k1gMnSc1	Ibsen
Alexander	Alexandra	k1gFnPc2	Alexandra
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Ostrovskij	Ostrovskij	k1gMnSc1	Ostrovskij
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
August	August	k1gMnSc1	August
Strindberg	Strindberg	k1gMnSc1	Strindberg
Ivan	Ivan	k1gMnSc1	Ivan
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Turgeněv	Turgeněv	k1gMnSc1	Turgeněv
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
Émile	Émil	k1gInSc5	Émil
Zola	Zola	k1gMnSc1	Zola
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
uměleckých	umělecký	k2eAgInPc2d1	umělecký
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
impresionismus	impresionismus	k1gInSc1	impresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
snahou	snaha	k1gFnSc7	snaha
bylo	být	k5eAaImAgNnS	být
vyjádření	vyjádření	k1gNnSc1	vyjádření
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
smyslového	smyslový	k2eAgInSc2d1	smyslový
dojmu	dojem	k1gInSc2	dojem
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
zobrazovaná	zobrazovaný	k2eAgFnSc1d1	zobrazovaná
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nálada	nálada	k1gFnSc1	nálada
(	(	kIx(	(
<g/>
imprese	imprese	k1gFnSc1	imprese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
se	se	k3xPyFc4	se
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
dostávaly	dostávat	k5eAaImAgInP	dostávat
lyrické	lyrický	k2eAgInPc1d1	lyrický
opisy	opis	k1gInPc1	opis
a	a	k8xC	a
epizodní	epizodní	k2eAgInPc1d1	epizodní
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
impresionismu	impresionismus	k1gInSc2	impresionismus
je	být	k5eAaImIp3nS	být
znát	znát	k5eAaImF	znát
i	i	k9	i
vliv	vliv	k1gInSc4	vliv
symbolismu	symbolismus	k1gInSc2	symbolismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přikládal	přikládat	k5eAaImAgInS	přikládat
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
hlavně	hlavně	k9	hlavně
obraznosti	obraznost	k1gFnSc2	obraznost
a	a	k8xC	a
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
,	,	kIx,	,
skutečnost	skutečnost	k1gFnSc4	skutečnost
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
jen	jen	k6eAd1	jen
náznakem	náznak	k1gInSc7	náznak
<g/>
,	,	kIx,	,
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dramatu	drama	k1gNnSc2	drama
přinesl	přinést	k5eAaPmAgInS	přinést
maximální	maximální	k2eAgFnSc4d1	maximální
stylizaci	stylizace	k1gFnSc4	stylizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
někdy	někdy	k6eAd1	někdy
přerostla	přerůst	k5eAaPmAgFnS	přerůst
až	až	k9	až
do	do	k7c2	do
nežádoucí	žádoucí	k2eNgFnSc2d1	nežádoucí
abstrakce	abstrakce	k1gFnSc2	abstrakce
–	–	k?	–
v	v	k7c6	v
herectví	herectví	k1gNnSc6	herectví
i	i	k8xC	i
ve	v	k7c6	v
výpravě	výprava	k1gFnSc6	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Německým	německý	k2eAgInSc7d1	německý
přínosem	přínos	k1gInSc7	přínos
do	do	k7c2	do
vlny	vlna	k1gFnSc2	vlna
avantgardního	avantgardní	k2eAgNnSc2d1	avantgardní
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgInS	být
expresionismus	expresionismus	k1gInSc1	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
ponor	ponor	k1gInSc4	ponor
do	do	k7c2	do
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
vystižení	vystižení	k1gNnSc4	vystižení
její	její	k3xOp3gFnSc2	její
podstaty	podstata	k1gFnSc2	podstata
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gNnSc2	její
nitra	nitro	k1gNnSc2	nitro
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
zobrazení	zobrazení	k1gNnSc6	zobrazení
její	její	k3xOp3gFnSc2	její
vnější	vnější	k2eAgFnSc2d1	vnější
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
životním	životní	k2eAgInSc7d1	životní
pesimismem	pesimismus	k1gInSc7	pesimismus
<g/>
,	,	kIx,	,
tísní	tíseň	k1gFnSc7	tíseň
<g/>
,	,	kIx,	,
napětím	napětí	k1gNnSc7	napětí
a	a	k8xC	a
emocemi	emoce	k1gFnPc7	emoce
<g/>
,	,	kIx,	,
způsobenými	způsobený	k2eAgInPc7d1	způsobený
hlavně	hlavně	k9	hlavně
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Expresionistické	expresionistický	k2eAgNnSc1d1	expresionistické
herectví	herectví	k1gNnSc1	herectví
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
realistického	realistický	k2eAgInSc2d1	realistický
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
postavu	postava	k1gFnSc4	postava
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
jako	jako	k9	jako
její	její	k3xOp3gInSc1	její
náčrt	náčrt	k1gInSc1	náčrt
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
jednání	jednání	k1gNnSc4	jednání
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
a	a	k8xC	a
účelností	účelnost	k1gFnSc7	účelnost
technických	technický	k2eAgNnPc2d1	technické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
konstrukcí	konstrukce	k1gFnPc2	konstrukce
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
ve	v	k7c6	v
scénografii	scénografie	k1gFnSc6	scénografie
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Stanislavskij	Stanislavskij	k1gMnSc1	Stanislavskij
a	a	k8xC	a
Vladimir	Vladimir	k1gMnSc1	Vladimir
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Němirovič-Dančenko	Němirovič-Dančenka	k1gFnSc5	Němirovič-Dančenka
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
zakladateli	zakladatel	k1gMnSc6	zakladatel
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
divadelních	divadelní	k2eAgFnPc2d1	divadelní
institucí	instituce	k1gFnPc2	instituce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
Moskevského	moskevský	k2eAgNnSc2d1	moskevské
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
akademického	akademický	k2eAgNnSc2d1	akademické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
známého	známý	k2eAgNnSc2d1	známé
také	také	k9	také
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
MCHAT	MCHAT	kA	MCHAT
<g/>
.	.	kIx.	.
</s>
<s>
Stanislavskij	Stanislavskít	k5eAaPmRp2nS	Stanislavskít
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hereckou	herecký	k2eAgFnSc4d1	herecká
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sám	sám	k3xTgMnSc1	sám
nazval	nazvat	k5eAaBmAgInS	nazvat
psychickým	psychický	k2eAgInSc7d1	psychický
realismem	realismus	k1gInSc7	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
plně	plně	k6eAd1	plně
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
rolí	role	k1gFnSc7	role
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
ji	on	k3xPp3gFnSc4	on
prožívat	prožívat	k5eAaImF	prožívat
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
umět	umět	k5eAaImF	umět
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hry	hra	k1gFnSc2	hra
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
nálady	nálada	k1gFnPc1	nálada
<g/>
,	,	kIx,	,
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
odstíny	odstín	k1gInPc1	odstín
citů	cit	k1gInPc2	cit
namísto	namísto	k7c2	namísto
okázalých	okázalý	k2eAgNnPc2d1	okázalé
gest	gesto	k1gNnPc2	gesto
a	a	k8xC	a
efektních	efektní	k2eAgInPc2d1	efektní
pohybů	pohyb	k1gInPc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Stanislavského	Stanislavský	k2eAgInSc2d1	Stanislavský
školy	škola	k1gFnSc2	škola
vyšly	vyjít	k5eAaPmAgFnP	vyjít
režisérské	režisérský	k2eAgFnPc1d1	režisérská
osobnosti	osobnost	k1gFnPc1	osobnost
sovětského	sovětský	k2eAgNnSc2d1	sovětské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
:	:	kIx,	:
Vsevolod	Vsevolod	k1gInSc1	Vsevolod
Emiljevič	Emiljevič	k1gMnSc1	Emiljevič
Mejerchold	Mejerchold	k1gMnSc1	Mejerchold
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Bagrationovič	Bagrationovič	k1gInSc4	Bagrationovič
Vachtangov	Vachtangov	k1gInSc1	Vachtangov
Alexander	Alexandra	k1gFnPc2	Alexandra
Jakovlevič	Jakovlevič	k1gInSc4	Jakovlevič
Tairov	Tairov	k1gInSc4	Tairov
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Epické	epický	k2eAgNnSc1d1	epické
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
Zcizovací	zcizovací	k2eAgInSc4d1	zcizovací
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Dramatický	dramatický	k2eAgInSc1d1	dramatický
a	a	k8xC	a
divadelní	divadelní	k2eAgInSc1d1	divadelní
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
rozbití	rozbití	k1gNnSc6	rozbití
divákovy	divákův	k2eAgFnSc2d1	divákova
iluze	iluze	k1gFnSc2	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
není	být	k5eNaImIp3nS	být
předváděn	předváděn	k2eAgInSc1d1	předváděn
skutečný	skutečný	k2eAgInSc1d1	skutečný
život	život	k1gInSc1	život
skutečných	skutečný	k2eAgFnPc2d1	skutečná
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
iluzi	iluze	k1gFnSc4	iluze
<g/>
,	,	kIx,	,
o	o	k7c4	o
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Divák	divák	k1gMnSc1	divák
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
takto	takto	k6eAd1	takto
veden	vést	k5eAaImNgMnS	vést
k	k	k7c3	k
aktivnímu	aktivní	k2eAgNnSc3d1	aktivní
porovnávání	porovnávání	k1gNnSc3	porovnávání
divadelního	divadelní	k2eAgInSc2d1	divadelní
příběhu	příběh	k1gInSc2	příběh
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
formální	formální	k2eAgFnSc6d1	formální
stránce	stránka	k1gFnSc6	stránka
dramatici	dramatik	k1gMnPc1	dramatik
nahradili	nahradit	k5eAaPmAgMnP	nahradit
přítomný	přítomný	k2eAgInSc4d1	přítomný
dramatický	dramatický	k2eAgInSc4d1	dramatický
čas	čas	k1gInSc4	čas
časem	čas	k1gInSc7	čas
minulým	minulý	k2eAgInSc7d1	minulý
a	a	k8xC	a
epickým	epický	k2eAgInSc7d1	epický
<g/>
,	,	kIx,	,
v	v	k7c6	v
hrách	hra	k1gFnPc6	hra
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
množství	množství	k1gNnSc1	množství
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
scéna	scéna	k1gFnSc1	scéna
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
odcizení	odcizení	k1gNnSc1	odcizení
<g/>
:	:	kIx,	:
herec	herec	k1gMnSc1	herec
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
odstup	odstup	k1gInSc4	odstup
od	od	k7c2	od
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
úlohou	úloha	k1gFnSc7	úloha
<g/>
,	,	kIx,	,
postavu	postava	k1gFnSc4	postava
má	mít	k5eAaImIp3nS	mít
představovat	představovat	k5eAaImF	představovat
<g/>
,	,	kIx,	,
racionálně	racionálně	k6eAd1	racionálně
objasnit	objasnit	k5eAaPmF	objasnit
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
diváka	divák	k1gMnSc4	divák
citově	citově	k6eAd1	citově
vázat	vázat	k5eAaImF	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zcizovací	zcizovací	k2eAgInPc4d1	zcizovací
efekty	efekt	k1gInPc4	efekt
patří	patřit	k5eAaImIp3nP	patřit
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInPc1	komentář
<g/>
,	,	kIx,	,
transparenty	transparent	k1gInPc1	transparent
<g/>
,	,	kIx,	,
přestavba	přestavba	k1gFnSc1	přestavba
scény	scéna	k1gFnSc2	scéna
bez	bez	k7c2	bez
spuštění	spuštění	k1gNnSc2	spuštění
opony	opona	k1gFnSc2	opona
apod.	apod.	kA	apod.
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
epického	epický	k2eAgNnSc2d1	epické
divadla	divadlo	k1gNnSc2	divadlo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
německý	německý	k2eAgMnSc1d1	německý
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
teoretik	teoretik	k1gMnSc1	teoretik
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Baal	Baal	k1gInSc1	Baal
<g/>
,	,	kIx,	,
Žebrácká	žebrácký	k2eAgFnSc1d1	Žebrácká
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Matka	matka	k1gFnSc1	matka
Kuráž	Kuráž	k?	Kuráž
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Život	život	k1gInSc4	život
Galileiho	Galilei	k1gMnSc2	Galilei
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
člověk	člověk	k1gMnSc1	člověk
ze	z	k7c2	z
Sečuanu	Sečuan	k1gInSc2	Sečuan
a	a	k8xC	a
Kavkazský	kavkazský	k2eAgInSc4d1	kavkazský
křídový	křídový	k2eAgInSc4d1	křídový
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Absurdní	absurdní	k2eAgNnSc1d1	absurdní
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
antidrama	antidrama	k1gNnSc4	antidrama
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
na	na	k7c6	na
nihilistické	nihilistický	k2eAgFnSc6d1	nihilistická
koncepci	koncepce	k1gFnSc6	koncepce
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Dílům	díl	k1gInPc3	díl
často	často	k6eAd1	často
chybí	chybit	k5eAaPmIp3nS	chybit
souvislý	souvislý	k2eAgInSc1d1	souvislý
děj	děj	k1gInSc1	děj
a	a	k8xC	a
zápletka	zápletka	k1gFnSc1	zápletka
<g/>
.	.	kIx.	.
</s>
<s>
Motivace	motivace	k1gFnSc1	motivace
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
postavami	postava	k1gFnPc7	postava
nabývá	nabývat	k5eAaImIp3nS	nabývat
absurdní	absurdní	k2eAgFnSc7d1	absurdní
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
chybí	chybit	k5eAaPmIp3nS	chybit
i	i	k9	i
rozuzlení	rozuzlení	k1gNnSc4	rozuzlení
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
obyčejně	obyčejně	k6eAd1	obyčejně
osamělý	osamělý	k2eAgMnSc1d1	osamělý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
neschopný	schopný	k2eNgMnSc1d1	neschopný
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Eugè	Eugè	k?	Eugè
Ionesco	Ionesco	k1gMnSc1	Ionesco
(	(	kIx(	(
<g/>
Židle	židle	k1gFnSc1	židle
<g/>
,	,	kIx,	,
Plešatá	plešatý	k2eAgFnSc1d1	plešatá
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
)	)	kIx)	)
Samuel	Samuel	k1gMnSc1	Samuel
Beckett	Beckett	k1gMnSc1	Beckett
(	(	kIx(	(
<g/>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godotta	Godott	k1gMnSc4	Godott
<g/>
,	,	kIx,	,
Šťastné	Šťastné	k2eAgInPc1d1	Šťastné
dny	den	k1gInPc1	den
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
(	(	kIx(	(
<g/>
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
,	,	kIx,	,
Audience	audience	k1gFnSc1	audience
<g/>
)	)	kIx)	)
Norman	Norman	k1gMnSc1	Norman
Frederick	Frederick	k1gMnSc1	Frederick
Simpson	Simpson	k1gMnSc1	Simpson
Experimentální	experimentální	k2eAgNnSc4d1	experimentální
divadlo	divadlo	k1gNnSc4	divadlo
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
vizuální	vizuální	k2eAgFnSc4d1	vizuální
i	i	k8xC	i
akusticky	akusticky	k6eAd1	akusticky
orientované	orientovaný	k2eAgNnSc4d1	orientované
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
narativního	narativní	k2eAgInSc2d1	narativní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
více	hodně	k6eAd2	hodně
z	z	k7c2	z
vizuality	vizualita	k1gFnSc2	vizualita
<g/>
,	,	kIx,	,
gesta	gesto	k1gNnSc2	gesto
<g/>
,	,	kIx,	,
tance	tanec	k1gInSc2	tanec
(	(	kIx(	(
<g/>
současného	současný	k2eAgNnSc2d1	současné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nonverbality	nonverbalita	k1gFnSc2	nonverbalita
<g/>
,	,	kIx,	,
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
absentuje	absentovat	k5eAaImIp3nS	absentovat
literární	literární	k2eAgInSc1d1	literární
příběh	příběh	k1gInSc1	příběh
a	a	k8xC	a
dramatizace	dramatizace	k1gFnSc1	dramatizace
<g/>
.	.	kIx.	.
</s>
<s>
Futuristické	futuristický	k2eAgNnSc1d1	futuristické
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Totální	totální	k2eAgNnSc1d1	totální
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Merz	Merz	k1gInSc1	Merz
–	–	k?	–
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Figurální	figurální	k2eAgInSc1d1	figurální
kabinet	kabinet	k1gInSc1	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Performance	performance	k1gFnSc1	performance
<g/>
.	.	kIx.	.
</s>
<s>
Butó	Butó	k?	Butó
<g/>
.	.	kIx.	.
</s>
<s>
Taneční	taneční	k2eAgNnSc1d1	taneční
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Pohybové	pohybový	k2eAgNnSc1d1	pohybové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgFnSc1d1	experimentální
akustická	akustický	k2eAgFnSc1d1	akustická
poezie	poezie	k1gFnSc1	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Multimediální	multimediální	k2eAgNnSc1d1	multimediální
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Interaktivní	interaktivní	k2eAgNnSc1d1	interaktivní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Gordon	Gordon	k1gMnSc1	Gordon
Craig	Craig	k1gMnSc1	Craig
<g/>
:	:	kIx,	:
Umění	umění	k1gNnSc1	umění
divadla	divadlo	k1gNnSc2	divadlo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
gesta	gesto	k1gNnSc2	gesto
a	a	k8xC	a
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
dramatici	dramatik	k1gMnPc1	dramatik
nejsou	být	k5eNaImIp3nP	být
dětmi	dítě	k1gFnPc7	dítě
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Tennessee	Tennessee	k1gFnSc1	Tennessee
Williams	Williamsa	k1gFnPc2	Williamsa
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
složitý	složitý	k2eAgInSc4d1	složitý
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
honosné	honosný	k2eAgFnPc4d1	honosná
budovy	budova	k1gFnPc4	budova
"	"	kIx"	"
<g/>
kamenných	kamenný	k2eAgNnPc2d1	kamenné
<g/>
"	"	kIx"	"
divadel	divadlo	k1gNnPc2	divadlo
vznikala	vznikat	k5eAaImAgFnS	vznikat
už	už	k6eAd1	už
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
i	i	k8xC	i
v	v	k7c6	v
USA	USA	kA	USA
malá	malý	k2eAgNnPc4d1	malé
divadélka	divadélko	k1gNnPc4	divadélko
s	s	k7c7	s
několika	několik	k4yIc7	několik
herci	herc	k1gInPc7	herc
<g/>
,	,	kIx,	,
komickými	komický	k2eAgInPc7d1	komický
<g/>
,	,	kIx,	,
satirickými	satirický	k2eAgInPc7d1	satirický
a	a	k8xC	a
často	často	k6eAd1	často
improvizovanými	improvizovaný	k2eAgNnPc7d1	improvizované
představeními	představení	k1gNnPc7	představení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
s	s	k7c7	s
Voskovcem	Voskovec	k1gMnSc7	Voskovec
<g/>
,	,	kIx,	,
Werichem	Werich	k1gMnSc7	Werich
a	a	k8xC	a
Ježkem	Ježek	k1gMnSc7	Ježek
<g/>
,	,	kIx,	,
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k6eAd1	mnoho
malých	malý	k2eAgNnPc2d1	malé
divadel	divadlo	k1gNnPc2	divadlo
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
,	,	kIx,	,
Semafor	Semafor	k1gInSc4	Semafor
<g/>
,	,	kIx,	,
Žižkovské	žižkovský	k2eAgNnSc1d1	Žižkovské
divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgNnPc2d1	jiné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
proslula	proslout	k5eAaPmAgFnS	proslout
polská	polský	k2eAgFnSc1d1	polská
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
experimentální	experimentální	k2eAgNnPc4d1	experimentální
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Laboratorium	laboratorium	k1gNnSc1	laboratorium
<g/>
"	"	kIx"	"
J.	J.	kA	J.
Grotkowského	Grotkowského	k2eAgFnSc1d1	Grotkowského
v	v	k7c6	v
Opole	Opol	k1gInSc6	Opol
se	s	k7c7	s
34	[number]	k4	34
sedadly	sedadlo	k1gNnPc7	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nó	nó	k0	nó
–	–	k?	–
japonské	japonský	k2eAgNnSc1d1	Japonské
tradiční	tradiční	k2eAgNnSc1d1	tradiční
divadlo	divadlo	k1gNnSc1	divadlo
Kabuki	Kabuk	k1gFnSc2	Kabuk
-	-	kIx~	-
japonské	japonský	k2eAgNnSc4d1	Japonské
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
divadlo	divadlo	k1gNnSc4	divadlo
Bunraku	Bunrak	k1gInSc2	Bunrak
-	-	kIx~	-
japonské	japonský	k2eAgNnSc4d1	Japonské
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
</s>
