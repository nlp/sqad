<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
menších	malý	k2eAgNnPc2d2	menší
divadel	divadlo	k1gNnPc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
přední	přední	k2eAgFnSc4d1	přední
scénu	scéna	k1gFnSc4	scéna
současných	současný	k2eAgFnPc2d1	současná
divadelních	divadelní	k2eAgFnPc2d1	divadelní
tendencí	tendence	k1gFnPc2	tendence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
Helenou	Helena	k1gFnSc7	Helena
Philipovou	Philipový	k2eAgFnSc7d1	Philipový
<g/>
,	,	kIx,	,
Ivanem	Ivan	k1gMnSc7	Ivan
Vyskočilem	Vyskočil	k1gMnSc7	Vyskočil
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Suchým	Suchý	k1gMnSc7	Suchý
a	a	k8xC	a
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Vodičkou	Vodička	k1gMnSc7	Vodička
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
mnoho	mnoho	k4c1	mnoho
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
známých	známý	k2eAgMnPc2d1	známý
režisérů	režisér	k1gMnPc2	režisér
a	a	k8xC	a
herců	herec	k1gMnPc2	herec
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Grossman	Grossman	k1gMnSc1	Grossman
<g/>
,	,	kIx,	,
Evald	Evald	k1gMnSc1	Evald
Schorm	Schorm	k1gInSc1	Schorm
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Pokorný	Pokorný	k1gMnSc1	Pokorný
nebo	nebo	k8xC	nebo
Petr	Petr	k1gMnSc1	Petr
Lébl	Lébl	k1gMnSc1	Lébl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
nové	nový	k2eAgNnSc4d1	nové
vedení	vedení	k1gNnSc4	vedení
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Petr	Petr	k1gMnSc1	Petr
Štědroň	Štědroň	k1gMnSc1	Štědroň
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dora	Dora	k1gFnSc1	Dora
Viceníková	Viceníková	k1gFnSc1	Viceníková
(	(	kIx(	(
<g/>
umělecký	umělecký	k2eAgMnSc1d1	umělecký
šéf	šéf	k1gMnSc1	šéf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
(	(	kIx(	(
<g/>
kmenový	kmenový	k2eAgMnSc1d1	kmenový
režisér	režisér	k1gMnSc1	režisér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
určuje	určovat	k5eAaImIp3nS	určovat
nové	nový	k2eAgNnSc4d1	nové
umělecké	umělecký	k2eAgNnSc4d1	umělecké
směřování	směřování	k1gNnSc4	směřování
DNz	DNz	k1gFnSc2	DNz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
získalo	získat	k5eAaPmAgNnS	získat
divadlo	divadlo	k1gNnSc1	divadlo
ceny	cena	k1gFnSc2	cena
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
kategoriích	kategorie	k1gFnPc6	kategorie
Cen	cena	k1gFnPc2	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prestižního	prestižní	k2eAgNnSc2d1	prestižní
ocenění	ocenění	k1gNnSc3	ocenění
Divadlo	divadlo	k1gNnSc1	divadlo
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
===	===	k?	===
</s>
</p>
<p>
<s>
Zakladateli	zakladatel	k1gMnSc3	zakladatel
divadla	divadlo	k1gNnSc2	divadlo
byli	být	k5eAaImAgMnP	být
Helena	Helena	k1gFnSc1	Helena
Philippová	Philippová	k1gFnSc1	Philippová
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
dali	dát	k5eAaPmAgMnP	dát
svému	svůj	k3xOyFgNnSc3	svůj
profesionálnímu	profesionální	k2eAgNnSc3d1	profesionální
divadlu	divadlo	k1gNnSc3	divadlo
název	název	k1gInSc4	název
podle	podle	k7c2	podle
uličky	ulička	k1gFnSc2	ulička
vedoucí	vedoucí	k1gMnPc1	vedoucí
z	z	k7c2	z
Anenského	anenský	k2eAgNnSc2d1	Anenské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
divadlo	divadlo	k1gNnSc1	divadlo
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgNnPc4	první
představení	představení	k1gNnPc2	představení
uvedli	uvést	k5eAaPmAgMnP	uvést
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1958	[number]	k4	1958
divadelní	divadelní	k2eAgNnSc1d1	divadelní
leporelo	leporelo	k1gNnSc1	leporelo
s	s	k7c7	s
písničkami	písnička	k1gFnPc7	písnička
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pantomima	pantomima	k1gFnSc1	pantomima
===	===	k?	===
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1959	[number]	k4	1959
se	se	k3xPyFc4	se
k	k	k7c3	k
činohernímu	činoherní	k2eAgInSc3d1	činoherní
souboru	soubor	k1gInSc3	soubor
divadla	divadlo	k1gNnSc2	divadlo
připojil	připojit	k5eAaPmAgInS	připojit
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fialka	fialka	k1gFnSc1	fialka
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
pantomimickou	pantomimický	k2eAgFnSc7d1	pantomimická
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
vrátil	vrátit	k5eAaPmAgMnS	vrátit
slávu	sláva	k1gFnSc4	sláva
opomíjenému	opomíjený	k2eAgInSc3d1	opomíjený
divadelnímu	divadelní	k2eAgInSc3d1	divadelní
žánru	žánr	k1gInSc3	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Činohra	činohra	k1gFnSc1	činohra
a	a	k8xC	a
pantomima	pantomima	k1gFnSc1	pantomima
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
existovaly	existovat	k5eAaImAgFnP	existovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fialka	fialka	k1gFnSc1	fialka
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Absurdní	absurdní	k2eAgNnSc1d1	absurdní
divadlo	divadlo	k1gNnSc1	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
režiséra	režisér	k1gMnSc2	režisér
Jana	Jan	k1gMnSc2	Jan
Grossmana	Grossman	k1gMnSc2	Grossman
<g/>
,	,	kIx,	,
scénografa	scénograf	k1gMnSc2	scénograf
Libora	Libor	k1gMnSc2	Libor
Fáry	fáry	k0	fáry
a	a	k8xC	a
kulisáka	kulisák	k1gMnSc4	kulisák
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dramaturga	dramaturg	k1gMnSc4	dramaturg
a	a	k8xC	a
dramatika	dramatik	k1gMnSc4	dramatik
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
začalo	začít	k5eAaPmAgNnS	začít
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
českou	český	k2eAgFnSc4d1	Česká
podobu	podoba	k1gFnSc4	podoba
absurdního	absurdní	k2eAgNnSc2d1	absurdní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
hrály	hrát	k5eAaImAgFnP	hrát
takové	takový	k3xDgFnPc1	takový
hry	hra	k1gFnPc1	hra
jako	jako	k9	jako
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
:	:	kIx,	:
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
,	,	kIx,	,
Vyrozumění	vyrozumění	k1gNnSc1	vyrozumění
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Jarry	Jarra	k1gFnSc2	Jarra
<g/>
:	:	kIx,	:
Král	Král	k1gMnSc1	Král
Ubu	Ubu	k1gMnSc1	Ubu
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
:	:	kIx,	:
ProcesDivadlu	ProcesDivadlo	k1gNnSc3	ProcesDivadlo
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
značného	značný	k2eAgNnSc2d1	značné
uznání	uznání	k1gNnSc2	uznání
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
museli	muset	k5eAaImAgMnP	muset
Jan	Jan	k1gMnSc1	Jan
Grossman	Grossman	k1gMnSc1	Grossman
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
stalo	stát	k5eAaPmAgNnS	stát
útočištěm	útočiště	k1gNnSc7	útočiště
některých	některý	k3yIgMnPc2	některý
filmových	filmový	k2eAgMnPc2d1	filmový
režisérů	režisér	k1gMnPc2	režisér
české	český	k2eAgFnSc2d1	Česká
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
šedesátých	šedesátý	k4xOgInPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
byla	být	k5eAaImAgFnS	být
normalizačními	normalizační	k2eAgFnPc7d1	normalizační
praktikami	praktika	k1gFnPc7	praktika
znemožněna	znemožnit	k5eAaPmNgFnS	znemožnit
filmová	filmový	k2eAgFnSc1d1	filmová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
Evald	Evald	k1gInSc1	Evald
Schorm	Schorm	k1gInSc1	Schorm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
řadu	řad	k1gInSc2	řad
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
až	až	k8xS	až
kultovních	kultovní	k2eAgFnPc2d1	kultovní
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
:	:	kIx,	:
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
Macbeth	Macbeth	k1gMnSc1	Macbeth
</s>
</p>
<p>
<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
:	:	kIx,	:
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
</s>
</p>
<p>
<s>
Claude	Claude	k6eAd1	Claude
Confortés	Confortés	k1gInSc1	Confortés
<g/>
:	:	kIx,	:
Maraton	maraton	k1gInSc1	maraton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Jan	Jan	k1gMnSc1	Jan
Grossman	Grossman	k1gMnSc1	Grossman
jako	jako	k8xS	jako
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
bylo	být	k5eAaImAgNnS	být
jmenováno	jmenován	k2eAgNnSc1d1	jmenováno
nové	nový	k2eAgNnSc1d1	nové
vedení	vedení	k1gNnSc1	vedení
–	–	k?	–
ředitelka	ředitelka	k1gFnSc1	ředitelka
Doubravka	Doubravka	k1gFnSc1	Doubravka
Svobodová	Svobodová	k1gFnSc1	Svobodová
a	a	k8xC	a
umělecký	umělecký	k2eAgMnSc1d1	umělecký
šéf	šéf	k1gMnSc1	šéf
Petr	Petr	k1gMnSc1	Petr
Lébl	Lébl	k1gMnSc1	Lébl
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Lébl	Lébl	k1gMnSc1	Lébl
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejtalentovanějších	talentovaný	k2eAgMnPc2d3	nejtalentovanější
režisérů	režisér	k1gMnPc2	režisér
svébytné	svébytný	k2eAgFnSc2d1	svébytná
imaginace	imaginace	k1gFnSc2	imaginace
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
provokoval	provokovat	k5eAaImAgInS	provokovat
svými	svůj	k3xOyFgInPc7	svůj
interpretačními	interpretační	k2eAgInPc7d1	interpretační
objevy	objev	k1gInPc7	objev
<g/>
.	.	kIx.	.
</s>
<s>
Jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
například	například	k6eAd1	například
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
Genet	Genet	k1gMnSc1	Genet
<g/>
:	:	kIx,	:
Služky	služka	k1gFnPc1	služka
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Stroupežnický	Stroupežnický	k2eAgMnSc1d1	Stroupežnický
<g/>
:	:	kIx,	:
Naši	náš	k3xOp1gMnPc1	náš
furianti	furiant	k1gMnPc1	furiant
</s>
</p>
<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
:	:	kIx,	:
Revizor	revizor	k1gMnSc1	revizor
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
:	:	kIx,	:
Racek	racek	k1gMnSc1	racek
<g/>
,	,	kIx,	,
Ivanov	Ivanov	k1gInSc1	Ivanov
<g/>
,	,	kIx,	,
Strýček	strýček	k1gMnSc1	strýček
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
tragické	tragický	k2eAgFnSc6d1	tragická
smrti	smrt	k1gFnSc6	smrt
Petra	Petr	k1gMnSc2	Petr
Lébla	Lébl	k1gMnSc2	Lébl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
podoby	podoba	k1gFnSc2	podoba
současného	současný	k2eAgNnSc2d1	současné
Divadla	divadlo	k1gNnSc2	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
umělecké	umělecký	k2eAgNnSc4d1	umělecké
vedení	vedení	k1gNnSc4	vedení
ve	v	k7c4	v
složení	složení	k1gNnSc4	složení
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Ornest	Ornest	k1gMnSc1	Ornest
a	a	k8xC	a
dramaturgyně	dramaturgyně	k1gFnSc1	dramaturgyně
Ivana	Ivana	k1gFnSc1	Ivana
Slámová	Slámová	k1gFnSc1	Slámová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
tento	tento	k3xDgInSc4	tento
"	"	kIx"	"
<g/>
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
"	"	kIx"	"
umělecké	umělecký	k2eAgNnSc1d1	umělecké
vedení	vedení	k1gNnSc1	vedení
Ivana	Ivan	k1gMnSc2	Ivan
Slámová	Slámová	k1gFnSc1	Slámová
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
texty	text	k1gInPc1	text
klasické	klasický	k2eAgFnSc2d1	klasická
dramatické	dramatický	k2eAgFnSc2d1	dramatická
literatury	literatura	k1gFnSc2	literatura
prokládat	prokládat	k5eAaImF	prokládat
českými	český	k2eAgFnPc7d1	Česká
i	i	k8xC	i
světovými	světový	k2eAgFnPc7d1	světová
premiérami	premiéra	k1gFnPc7	premiéra
současných	současný	k2eAgMnPc2d1	současný
dramatických	dramatický	k2eAgMnPc2d1	dramatický
autorů	autor	k1gMnPc2	autor
evropské	evropský	k2eAgFnSc2d1	Evropská
<g/>
,	,	kIx,	,
angloamerické	angloamerický	k2eAgFnSc2d1	angloamerická
i	i	k8xC	i
české	český	k2eAgFnSc2d1	Česká
provenience	provenience	k1gFnSc2	provenience
<g/>
.	.	kIx.	.
</s>
<s>
Uváděly	uvádět	k5eAaImAgFnP	uvádět
se	se	k3xPyFc4	se
například	například	k6eAd1	například
hry	hra	k1gFnPc1	hra
</s>
</p>
<p>
<s>
Jean-Claude	Jean-Claude	k6eAd1	Jean-Claude
Carriè	Carriè	k1gFnSc1	Carriè
<g/>
:	:	kIx,	:
Terasa	terasa	k1gFnSc1	terasa
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Tabori	Tabor	k1gFnSc2	Tabor
<g/>
:	:	kIx,	:
Balada	balada	k1gFnSc1	balada
o	o	k7c6	o
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
řízku	řízek	k1gInSc6	řízek
</s>
</p>
<p>
<s>
Gabriela	Gabriela	k1gFnSc1	Gabriela
Preissová	Preissová	k1gFnSc1	Preissová
<g/>
:	:	kIx,	:
Gazdina	gazdina	k1gFnSc1	gazdina
robaV	robaV	k?	robaV
letech	let	k1gInPc6	let
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
iniciovali	iniciovat	k5eAaBmAgMnP	iniciovat
a	a	k8xC	a
se	s	k7c7	s
statečnou	statečný	k2eAgFnSc7d1	statečná
podporou	podpora	k1gFnSc7	podpora
celého	celý	k2eAgNnSc2d1	celé
divadla	divadlo	k1gNnSc2	divadlo
realizovali	realizovat	k5eAaBmAgMnP	realizovat
náročný	náročný	k2eAgInSc4d1	náročný
a	a	k8xC	a
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
divadelní	divadelní	k2eAgInSc4d1	divadelní
projekt	projekt	k1gInSc4	projekt
Československé	československý	k2eAgNnSc1d1	Československé
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
uvádět	uvádět	k5eAaImF	uvádět
ve	v	k7c6	v
světových	světový	k2eAgFnPc6d1	světová
premiérách	premiéra	k1gFnPc6	premiéra
původní	původní	k2eAgFnSc2d1	původní
hry	hra	k1gFnSc2	hra
českých	český	k2eAgMnPc2d1	český
současných	současný	k2eAgMnPc2d1	současný
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Jiřího	Jiří	k1gMnSc2	Jiří
Pokorného	Pokorný	k1gMnSc2	Pokorný
z	z	k7c2	z
Divadla	divadlo	k1gNnSc2	divadlo
Na	na	k7c4	na
zábradlí	zábradlí	k1gNnSc4	zábradlí
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
jeho	jeho	k3xOp3gFnSc1	jeho
ředitelka	ředitelka	k1gFnSc1	ředitelka
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
poradní	poradní	k2eAgInSc4d1	poradní
orgán	orgán	k1gInSc4	orgán
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
radu	rada	k1gFnSc4	rada
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Igor	Igor	k1gMnSc1	Igor
Chmela	Chmel	k1gMnSc2	Chmel
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gMnSc1	Juraj
Nvota	Nvota	k1gMnSc1	Nvota
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Ornest	Ornest	k1gMnSc1	Ornest
a	a	k8xC	a
Ivana	Ivana	k1gFnSc1	Ivana
Slámová	Slámová	k1gFnSc1	Slámová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
nového	nový	k2eAgMnSc2d1	nový
uměleckého	umělecký	k2eAgMnSc2d1	umělecký
šéfa	šéf	k1gMnSc2	šéf
Davida	David	k1gMnSc2	David
Czesanyho	Czesany	k1gMnSc2	Czesany
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
začalo	začít	k5eAaPmAgNnS	začít
DNz	DNz	k1gMnPc4	DNz
s	s	k7c7	s
tematickými	tematický	k2eAgFnPc7d1	tematická
sezónami	sezóna	k1gFnPc7	sezóna
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sezona	sezona	k1gFnSc1	sezona
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
nesla	nést	k5eAaImAgFnS	nést
podtitul	podtitul	k1gInSc1	podtitul
"	"	kIx"	"
<g/>
Koho	kdo	k3yInSc4	kdo
nebe	nebe	k1gNnSc1	nebe
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
sezóna	sezóna	k1gFnSc1	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
se	se	k3xPyFc4	se
vztahovala	vztahovat	k5eAaImAgFnS	vztahovat
k	k	k7c3	k
veřejnému	veřejný	k2eAgInSc3d1	veřejný
prostoru	prostor	k1gInSc3	prostor
a	a	k8xC	a
městu	město	k1gNnSc3	město
jako	jako	k8xC	jako
místu	místo	k1gNnSc3	místo
pro	pro	k7c4	pro
život	život	k1gInSc4	život
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
podtitul	podtitul	k1gInSc4	podtitul
"	"	kIx"	"
<g/>
Čí	čí	k3xOyRgMnSc1	čí
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
sezóna	sezóna	k1gFnSc1	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
-	-	kIx~	-
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
hledání	hledání	k1gNnSc2	hledání
identity	identita	k1gFnSc2	identita
člověka	člověk	k1gMnSc2	člověk
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
Nepřizpůsobiví	přizpůsobivý	k2eNgMnPc5d1	nepřizpůsobivý
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgInPc2	tento
podtitulů	podtitul	k1gInPc2	podtitul
se	se	k3xPyFc4	se
uskutečňoval	uskutečňovat	k5eAaImAgInS	uskutečňovat
dramaturgický	dramaturgický	k2eAgInSc1d1	dramaturgický
výběr	výběr	k1gInSc1	výběr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
širší	široký	k2eAgNnSc4d2	širší
dění	dění	k1gNnSc4	dění
kolem	kolem	k7c2	kolem
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sezon	sezona	k1gFnPc2	sezona
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
například	například	k6eAd1	například
tematické	tematický	k2eAgInPc1d1	tematický
večery	večer	k1gInPc1	večer
s	s	k7c7	s
pamětníky	pamětník	k1gMnPc7	pamětník
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Prahy	Praha	k1gFnSc2	Praha
<g/>
;	;	kIx,	;
v	v	k7c6	v
Eliadově	Eliadův	k2eAgFnSc6d1	Eliadova
knihovně	knihovna	k1gFnSc6	knihovna
s	s	k7c7	s
tématy	téma	k1gNnPc7	téma
pracovali	pracovat	k5eAaImAgMnP	pracovat
studenti	student	k1gMnPc1	student
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Eliadova	Eliadův	k2eAgFnSc1d1	Eliadova
knihovna	knihovna	k1gFnSc1	knihovna
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
divadlo	divadlo	k1gNnSc1	divadlo
uvádělo	uvádět	k5eAaImAgNnS	uvádět
klauzurní	klauzurní	k2eAgFnPc4d1	klauzurní
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
autorské	autorský	k2eAgInPc4d1	autorský
projekty	projekt	k1gInPc4	projekt
či	či	k8xC	či
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Czesanyho	Czesany	k1gMnSc2	Czesany
proměnil	proměnit	k5eAaPmAgMnS	proměnit
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
ansambl	ansambnout	k5eAaPmAgMnS	ansambnout
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
nové	nový	k2eAgInPc4d1	nový
členy	člen	k1gInPc4	člen
–	–	k?	–
Ivana	Ivan	k1gMnSc4	Ivan
Luptáka	Lupták	k1gMnSc4	Lupták
<g/>
,	,	kIx,	,
Natálii	Natálie	k1gFnSc4	Natálie
Řehořovou	Řehořová	k1gFnSc4	Řehořová
a	a	k8xC	a
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Veselého	Veselý	k1gMnSc2	Veselý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c4	na
zábradlí	zábradlí	k1gNnSc4	zábradlí
nové	nový	k2eAgNnSc4d1	nové
umělecké	umělecký	k2eAgNnSc4d1	umělecké
vedení	vedení	k1gNnSc4	vedení
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Petr	Petr	k1gMnSc1	Petr
Štědroň	Štědroň	k1gMnSc1	Štědroň
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dora	Dora	k1gFnSc1	Dora
Viceníková	Viceníková	k1gFnSc1	Viceníková
(	(	kIx(	(
<g/>
umělecký	umělecký	k2eAgMnSc1d1	umělecký
šéf	šéf	k1gMnSc1	šéf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
(	(	kIx(	(
<g/>
kmenový	kmenový	k2eAgMnSc1d1	kmenový
režisér	režisér	k1gMnSc1	režisér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
nejen	nejen	k6eAd1	nejen
zcela	zcela	k6eAd1	zcela
změnit	změnit	k5eAaPmF	změnit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
repertoár	repertoár	k1gInSc4	repertoár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
rozšířit	rozšířit	k5eAaPmF	rozšířit
herecký	herecký	k2eAgInSc4d1	herecký
soubor	soubor	k1gInSc4	soubor
o	o	k7c4	o
několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
získalo	získat	k5eAaPmAgNnS	získat
divadlo	divadlo	k1gNnSc1	divadlo
pod	pod	k7c7	pod
novým	nový	k2eAgNnSc7d1	nové
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
vedením	vedení	k1gNnSc7	vedení
ocenění	ocenění	k1gNnPc2	ocenění
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
možných	možný	k2eAgFnPc6d1	možná
kategoriích	kategorie	k1gFnPc6	kategorie
Cen	cena	k1gFnPc2	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ocenění	ocenění	k1gNnSc2	ocenění
Divadlo	divadlo	k1gNnSc1	divadlo
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
pod	pod	k7c7	pod
novým	nový	k2eAgNnSc7d1	nové
vedením	vedení	k1gNnSc7	vedení
za	za	k7c7	za
sebou	se	k3xPyFc7	se
tři	tři	k4xCgFnPc4	tři
sezony	sezona	k1gFnPc4	sezona
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
-	-	kIx~	-
Tradice	tradice	k1gFnSc1	tradice
v	v	k7c6	v
novém	nový	k2eAgMnSc6d1	nový
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
-	-	kIx~	-
Za	za	k7c2	za
nás	my	k3xPp1nPc2	my
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
-	-	kIx~	-
Držte	držet	k5eAaImRp2nP	držet
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
divadlo	divadlo	k1gNnSc1	divadlo
prochází	procházet	k5eAaImIp3nS	procházet
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
hlediště	hlediště	k1gNnSc2	hlediště
a	a	k8xC	a
zázemí	zázemí	k1gNnSc2	zázemí
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
venkovního	venkovní	k2eAgInSc2d1	venkovní
dvorku	dvorek	k1gInSc2	dvorek
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
dvoupatrové	dvoupatrový	k2eAgNnSc1d1	dvoupatrové
zastřešené	zastřešený	k2eAgNnSc1d1	zastřešené
foyer	foyer	k1gNnSc1	foyer
<g/>
,	,	kIx,	,
úprav	úprava	k1gFnPc2	úprava
se	se	k3xPyFc4	se
dočká	dočkat	k5eAaPmIp3nS	dočkat
i	i	k9	i
divadelní	divadelní	k2eAgInSc1d1	divadelní
bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
divácká	divácký	k2eAgFnSc1d1	divácká
šatna	šatna	k1gFnSc1	šatna
a	a	k8xC	a
divadelní	divadelní	k2eAgInSc1d1	divadelní
sál	sál	k1gInSc1	sál
<g/>
.	.	kIx.	.
</s>
<s>
Znovuotevření	znovuotevření	k1gNnSc1	znovuotevření
divadla	divadlo	k1gNnSc2	divadlo
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
sezony	sezona	k1gFnPc4	sezona
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Inscenace	inscenace	k1gFnSc1	inscenace
roku	rok	k1gInSc2	rok
VELVET	VELVET	kA	VELVET
HAVEL	Havel	k1gMnSc1	Havel
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Divadlo	divadlo	k1gNnSc1	divadlo
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
MARIE	Marie	k1gFnSc1	Marie
SPURNÁ	Spurná	k1gFnSc1	Spurná
(	(	kIx(	(
<g/>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
v	v	k7c6	v
inscenaci	inscenace	k1gFnSc6	inscenace
Velvet	Velvet	k1gMnSc1	Velvet
Havel	Havel	k1gMnSc1	Havel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
MILOSLAV	Miloslava	k1gFnPc2	Miloslava
KÖNIG	KÖNIG	kA	KÖNIG
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
inscenaci	inscenace	k1gFnSc6	inscenace
Velvet	Velvet	k1gMnSc1	Velvet
Havel	Havel	k1gMnSc1	Havel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
poprvé	poprvé	k6eAd1	poprvé
uvedená	uvedený	k2eAgFnSc1d1	uvedená
česká	český	k2eAgFnSc1d1	Česká
hra	hra	k1gFnSc1	hra
VELVET	VELVET	kA	VELVET
HAVEL	Havel	k1gMnSc1	Havel
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
MILOŠ	Miloš	k1gMnSc1	Miloš
ORSON	ORSON	kA	ORSON
ŠTĚDROŇ	ŠTĚDROŇ	kA	ŠTĚDROŇ
Velvet	Velvet	k1gMnSc1	Velvet
Havel	Havel	k1gMnSc1	Havel
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Scénografie	scénografie	k1gFnSc1	scénografie
roku	rok	k1gInSc2	rok
MAREK	marka	k1gFnPc2	marka
CPIN	CPIN	kA	CPIN
Požitkáři	požitkář	k1gMnPc1	požitkář
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Ceny	cena	k1gFnPc4	cena
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
-	-	kIx~	-
Talent	talent	k1gInSc1	talent
roku	rok	k1gInSc2	rok
ANNA	Anna	k1gFnSc1	Anna
PETRŽELKOVÁ	PETRŽELKOVÁ	kA	PETRŽELKOVÁ
(	(	kIx(	(
<g/>
režisérka	režisérka	k1gFnSc1	režisérka
inscenace	inscenace	k1gFnSc2	inscenace
Báby	bába	k1gFnSc2	bába
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Ceny	cena	k1gFnPc1	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
-	-	kIx~	-
Inscenace	inscenace	k1gFnSc1	inscenace
roku	rok	k1gInSc2	rok
ZLATÁ	zlatý	k2eAgFnSc1d1	zlatá
ŠEDESÁTÁ	šedesátý	k4xOgNnPc1	šedesátý
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Ceny	cena	k1gFnPc1	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
hra	hra	k1gFnSc1	hra
ZÁZRAK	zázrak	k1gInSc1	zázrak
V	v	k7c6	v
ČERNÉM	černý	k2eAgInSc6d1	černý
DOMĚ	dům	k1gInSc6	dům
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Ceny	cena	k1gFnPc1	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
-	-	kIx~	-
Divadlo	divadlo	k1gNnSc1	divadlo
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Ceny	cena	k1gFnPc1	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
-	-	kIx~	-
Inscenace	inscenace	k1gFnSc1	inscenace
roku	rok	k1gInSc2	rok
Z	z	k7c2	z
CIZOTY	cizota	k1gFnSc2	cizota
</s>
</p>
<p>
<s>
==	==	k?	==
Herecký	herecký	k2eAgInSc1d1	herecký
soubor	soubor	k1gInSc1	soubor
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
tvoří	tvořit	k5eAaImIp3nS	tvořit
herecký	herecký	k2eAgInSc4d1	herecký
soubor	soubor	k1gInSc4	soubor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Čtvrtníček	Čtvrtníček	k1gMnSc1	Čtvrtníček
</s>
</p>
<p>
<s>
Natália	Natália	k1gFnSc1	Natália
Drabiščáková	Drabiščáková	k1gFnSc1	Drabiščáková
</s>
</p>
<p>
<s>
Honza	Honza	k1gMnSc1	Honza
Hájek	Hájek	k1gMnSc1	Hájek
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Jeništa	Jeništa	k1gMnSc1	Jeništa
</s>
</p>
<p>
<s>
Dita	Dita	k1gFnSc1	Dita
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
König	König	k1gMnSc1	König
</s>
</p>
<p>
<s>
Anežka	Anežka	k1gFnSc1	Anežka
Kubátová	Kubátová	k1gFnSc1	Kubátová
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Lupták	Lupták	k1gMnSc1	Lupták
</s>
</p>
<p>
<s>
Leoš	Leoš	k1gMnSc1	Leoš
Noha	noh	k1gMnSc2	noh
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Plodková	Plodkový	k2eAgFnSc1d1	Plodková
</s>
</p>
<p>
<s>
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Sidonová	Sidonová	k1gFnSc1	Sidonová
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Vašák	Vašák	k1gMnSc1	Vašák
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Vyorálek	Vyorálek	k1gMnSc1	Vyorálek
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Žáček	Žáček	k1gMnSc1	Žáček
</s>
</p>
<p>
<s>
==	==	k?	==
Inscenace	inscenace	k1gFnSc1	inscenace
==	==	k?	==
</s>
</p>
<p>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
má	mít	k5eAaImIp3nS	mít
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
na	na	k7c6	na
repertoáru	repertoár	k1gInSc6	repertoár
následující	následující	k2eAgInPc4d1	následující
tituly	titul	k1gInPc4	titul
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
,	,	kIx,	,
Dora	Dora	k1gFnSc1	Dora
Viceníková	Viceníková	k1gFnSc1	Viceníková
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
POSEDLOST	posedlost	k1gFnSc1	posedlost
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Orson	Orson	k1gMnSc1	Orson
Štědroň	Štědroň	k1gMnSc1	Štědroň
<g/>
:	:	kIx,	:
KRÁSNÉ	krásný	k2eAgInPc4d1	krásný
PSACÍ	psací	k2eAgInPc4d1	psací
STROJE	stroj	k1gInPc4	stroj
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Havelka	Havelka	k1gMnSc1	Havelka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
,	,	kIx,	,
Dora	Dora	k1gFnSc1	Dora
Viceníková	Viceníková	k1gFnSc1	Viceníková
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
HAMLETI	HAMLETI	kA	HAMLETI
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Bodó	Bodó	k?	Bodó
<g/>
,	,	kIx,	,
Júlia	Júlia	k1gFnSc1	Júlia
Róbert	Róbert	k1gMnSc1	Róbert
<g/>
:	:	kIx,	:
ANAMNÉZA	anamnéza	k1gFnSc1	anamnéza
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Rastislav	Rastislav	k1gMnSc1	Rastislav
Ballek	Ballek	k1gMnSc1	Ballek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniil	Daniil	k1gInSc1	Daniil
Charms	Charms	k1gInSc1	Charms
<g/>
:	:	kIx,	:
BÁBY	bába	k1gFnPc1	bába
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Anna	Anna	k1gFnSc1	Anna
Petrželková	Petrželková	k1gFnSc1	Petrželková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
,	,	kIx,	,
Dora	Dora	k1gFnSc1	Dora
Viceníková	Viceníková	k1gFnSc1	Viceníková
<g/>
:	:	kIx,	:
POŽITKÁŘI	požitkář	k1gMnPc1	požitkář
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
<g/>
:	:	kIx,	:
CIZINEC	cizinec	k1gMnSc1	cizinec
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Orson	Orson	k1gMnSc1	Orson
Štědroň	Štědroň	k1gMnSc1	Štědroň
<g/>
:	:	kIx,	:
VELVET	VELVET	kA	VELVET
HAVEL	Havel	k1gMnSc1	Havel
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Frič	Frič	k1gMnSc1	Frič
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Armin	Armin	k1gMnSc1	Armin
Petras	Petras	k1gMnSc1	Petras
<g/>
:	:	kIx,	:
ANNA	Anna	k1gFnSc1	Anna
KARENINA	KARENINA	kA	KARENINA
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Daniel	Daniel	k1gMnSc1	Daniel
Špinar	Špinar	k1gMnSc1	Špinar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
,	,	kIx,	,
Dora	Dora	k1gFnSc1	Dora
Viceníková	Viceníková	k1gFnSc1	Viceníková
<g/>
:	:	kIx,	:
BURŽOAZIE	buržoazie	k1gFnSc1	buržoazie
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Patrik	Patrik	k1gMnSc1	Patrik
Ouředník	Ouředník	k1gMnSc1	Ouředník
<g/>
:	:	kIx,	:
EUROPEANA	EUROPEANA	kA	EUROPEANA
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Juráček	Juráček	k1gMnSc1	Juráček
<g/>
:	:	kIx,	:
ZLATÁ	zlatý	k2eAgNnPc4d1	Zlaté
ŠEDESÁTÁ	šedesátý	k4xOgNnPc4	šedesátý
aneb	aneb	k?	aneb
DENÍK	deník	k1gInSc1	deník
PAVLA	Pavel	k1gMnSc2	Pavel
J.	J.	kA	J.
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
:	:	kIx,	:
KORESPONDENCE	korespondence	k1gFnSc1	korespondence
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
<g/>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
inscenacím	inscenace	k1gFnPc3	inscenace
po	po	k7c6	po
derniéře	derniéra	k1gFnSc6	derniéra
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
:	:	kIx,	:
Platonov	Platonov	k1gInSc1	Platonov
je	být	k5eAaImIp3nS	být
darebák	darebák	k1gMnSc1	darebák
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Per	prát	k5eAaImRp2nS	prát
Olov	olovit	k5eAaImRp2nS	olovit
Enquist	Enquist	k1gInSc1	Enquist
<g/>
:	:	kIx,	:
Blanche	Blanche	k1gFnSc1	Blanche
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
</s>
</p>
<p>
<s>
Kelly	Kell	k1gInPc1	Kell
McAllister	McAllistra	k1gFnPc2	McAllistra
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
hořícího	hořící	k2eAgMnSc2d1	hořící
muže	muž	k1gMnSc2	muž
</s>
</p>
<p>
<s>
Gemma	Gemma	k1gNnSc4	Gemma
Rodríguezová	Rodríguezový	k2eAgFnSc5d1	Rodríguezový
<g/>
:	:	kIx,	:
35,4	[number]	k4	35,4
Vypadáme	vypadat	k5eAaImIp1nP	vypadat
jako	jako	k9	jako
blbci	blbec	k1gMnPc1	blbec
</s>
</p>
<p>
<s>
Jean-Luc	Jean-Luc	k6eAd1	Jean-Luc
Lagarce	Lagarka	k1gFnSc3	Lagarka
<g/>
:	:	kIx,	:
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
hrdinové	hrdina	k1gMnPc1	hrdina
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Dürrenmatt	Dürrenmatt	k1gMnSc1	Dürrenmatt
<g/>
:	:	kIx,	:
Komplic	komplic	k1gMnSc1	komplic
</s>
</p>
<p>
<s>
Ingmar	Ingmar	k1gMnSc1	Ingmar
Bergman	Bergman	k1gMnSc1	Bergman
<g/>
:	:	kIx,	:
Sarabanda	sarabanda	k1gFnSc1	sarabanda
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
:	:	kIx,	:
Milada	Milada	k1gFnSc1	Milada
(	(	kIx(	(
<g/>
nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Uhde	Uhd	k1gFnSc2	Uhd
<g/>
:	:	kIx,	:
Zázrak	zázrak	k1gInSc1	zázrak
v	v	k7c6	v
černém	černý	k2eAgInSc6d1	černý
domě	dům	k1gInSc6	dům
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Walczak	Walczak	k1gMnSc1	Walczak
<g/>
:	:	kIx,	:
Pískoviště	pískoviště	k1gNnSc1	pískoviště
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Greig	Greig	k1gMnSc1	Greig
<g/>
:	:	kIx,	:
Kosmonautův	kosmonautův	k2eAgInSc1d1	kosmonautův
poslední	poslední	k2eAgInSc1d1	poslední
vzkaz	vzkaz	k1gInSc1	vzkaz
ženě	žena	k1gFnSc3	žena
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kdysi	kdysi	k6eAd1	kdysi
<g/>
,	,	kIx,	,
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Ornest	Ornest	k1gMnSc1	Ornest
<g/>
.	.	kIx.	.
</s>
<s>
Dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
:	:	kIx,	:
Radka	Radka	k1gFnSc1	Radka
Denemarková	Denemarková	k1gFnSc1	Denemarková
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Slámová	Slámová	k1gFnSc1	Slámová
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Matásek	Matásek	k1gMnSc1	Matásek
<g/>
.	.	kIx.	.
</s>
<s>
Kostýmy	kostým	k1gInPc1	kostým
<g/>
:	:	kIx,	:
Kateřina	Kateřina	k1gFnSc1	Kateřina
Štefková	Štefková	k1gFnSc1	Štefková
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Novinski	Novinsk	k1gFnSc2	Novinsk
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Juraj	Juraj	k1gMnSc1	Juraj
Nvota	Nvota	k1gMnSc1	Nvota
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Petra	Petra	k1gFnSc1	Petra
Špalková	špalkový	k2eAgFnSc1d1	Špalková
<g/>
,	,	kIx,	,
Táňa	Táňa	k1gFnSc1	Táňa
Pauhofová	Pauhofový	k2eAgFnSc1d1	Pauhofová
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
Chmela	Chmel	k1gMnSc2	Chmel
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Finger	Finger	k1gMnSc1	Finger
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Švehlík	Švehlík	k1gMnSc1	Švehlík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Čtvrtníček	Čtvrtníček	k1gMnSc1	Čtvrtníček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Ornest	Ornest	k1gMnSc1	Ornest
<g/>
,	,	kIx,	,
Kristina	Kristin	k2eAgFnSc1d1	Kristina
Maděričová	Maděričová	k1gFnSc1	Maděričová
<g/>
,	,	kIx,	,
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Sidonová	Sidonová	k1gFnSc1	Sidonová
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
Suchařípa	Suchaříp	k1gMnSc2	Suchaříp
/	/	kIx~	/
Pavel	Pavel	k1gMnSc1	Pavel
Liška	Liška	k1gMnSc1	Liška
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
;	;	kIx,	;
derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Greig	Greig	k1gMnSc1	Greig
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
<g/>
,	,	kIx,	,
Dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
:	:	kIx,	:
Ivana	Ivana	k1gFnSc1	Ivana
Slámová	Slámová	k1gFnSc1	Slámová
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Kostýmy	kostým	k1gInPc1	kostým
<g/>
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Čorba	Čorba	k1gMnSc1	Čorba
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Nejtek	Nejtek	k1gMnSc1	Nejtek
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Ornest	Ornest	k1gMnSc1	Ornest
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Igor	Igor	k1gMnSc1	Igor
Chmela	Chmel	k1gMnSc2	Chmel
<g/>
,	,	kIx,	,
Adéla	Adéla	k1gFnSc1	Adéla
Kačerová-Kubačáková	Kačerová-Kubačáková	k1gFnSc1	Kačerová-Kubačáková
<g/>
,	,	kIx,	,
Natália	Natália	k1gFnSc1	Natália
Drabiščáková	Drabiščáková	k1gFnSc1	Drabiščáková
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
<g/>
,	,	kIx,	,
Sylvie	Sylvie	k1gFnSc1	Sylvie
Krobová	Krobová	k1gFnSc1	Krobová
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c4	na
zábradlí	zábradlí	k1gNnSc4	zábradlí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.nazabradli.cz	[url]	k1gFnPc2	http://www.nazabradli.cz
–	–	k?	–
domácí	domácí	k2eAgFnSc2d1	domácí
stránky	stránka	k1gFnSc2	stránka
Divadla	divadlo	k1gNnSc2	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
</s>
</p>
<p>
<s>
EUTA	EUTA	kA	EUTA
-	-	kIx~	-
heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
EUTA	EUTA	kA	EUTA
</s>
</p>
