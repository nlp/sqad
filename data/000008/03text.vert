<s>
Mako	mako	k1gNnSc1	mako
<g/>
!	!	kIx.	!
<g/>
mako	mako	k1gNnSc1	mako
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
jako	jako	k8xS	jako
finalista	finalista	k1gMnSc1	finalista
televizní	televizní	k2eAgFnSc2d1	televizní
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
Slovensko	Slovensko	k1gNnSc4	Slovensko
má	mít	k5eAaImIp3nS	mít
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
svou	svůj	k3xOyFgFnSc7	svůj
melodickou	melodický	k2eAgFnSc7d1	melodická
svižnou	svižný	k2eAgFnSc7d1	svižná
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
rap	rap	k1gMnSc1	rap
a	a	k8xC	a
beatbox	beatbox	k1gInSc1	beatbox
<g/>
.	.	kIx.	.
</s>
<s>
Zpívají	zpívat	k5eAaImIp3nP	zpívat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
-	-	kIx~	-
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
manažerkou	manažerka	k1gFnSc7	manažerka
Vendula	Vendula	k1gFnSc1	Vendula
Šrýtrová	Šrýtrová	k1gFnSc1	Šrýtrová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Kristína	Kristína	k1gFnSc1	Kristína
Šimegová	Šimegový	k2eAgFnSc1d1	Šimegová
-	-	kIx~	-
rodačka	rodačka	k1gFnSc1	rodačka
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
JAMU	jam	k1gInSc3	jam
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
muzikálové	muzikálový	k2eAgNnSc1d1	muzikálové
herectví	herectví	k1gNnSc1	herectví
Ondřej	Ondřej	k1gMnSc1	Ondřej
Havlík	Havlík	k1gMnSc1	Havlík
-	-	kIx~	-
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
znám	znát	k5eAaImIp1nS	znát
také	také	k9	také
jako	jako	k9	jako
En	En	k1gFnSc1	En
<g/>
.	.	kIx.	.
<g/>
dru	dru	k?	dru
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
raper	raper	k1gMnSc1	raper
<g/>
,	,	kIx,	,
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
skupině	skupina	k1gFnSc3	skupina
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
s	s	k7c7	s
beatboxem	beatbox	k1gInSc7	beatbox
<g/>
,	,	kIx,	,
také	také	k9	také
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
JAMU	jam	k1gInSc2	jam
<g/>
,	,	kIx,	,
moderoval	moderovat	k5eAaBmAgMnS	moderovat
pořady	pořad	k1gInPc4	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Za	za	k7c4	za
školu	škola	k1gFnSc4	škola
nebo	nebo	k8xC	nebo
Vertikal	Vertikal	k1gFnSc4	Vertikal
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
skupiny	skupina	k1gFnSc2	skupina
pořadu	pořad	k1gInSc2	pořad
Na	na	k7c4	na
stojáka	stoják	k1gMnSc4	stoják
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
moderátor	moderátor	k1gMnSc1	moderátor
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
Evropa	Evropa	k1gFnSc1	Evropa
2	[number]	k4	2
Michal	Michal	k1gMnSc1	Michal
Procházka	Procházka	k1gMnSc1	Procházka
-	-	kIx~	-
rodák	rodák	k1gMnSc1	rodák
ze	z	k7c2	z
Zlína	Zlín	k1gInSc2	Zlín
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
JAMU	jam	k1gInSc2	jam
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
učitel	učitel	k1gMnSc1	učitel
Peter	Peter	k1gMnSc1	Peter
Strenáčik	Strenáčik	k1gMnSc1	Strenáčik
-	-	kIx~	-
rodák	rodák	k1gMnSc1	rodák
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
s	s	k7c7	s
beatboxem	beatbox	k1gInSc7	beatbox
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
JAMU	jam	k1gInSc2	jam
Přestože	přestože	k8xS	přestože
kapela	kapela	k1gFnSc1	kapela
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
i	i	k8xC	i
slovenské	slovenský	k2eAgFnSc6d1	slovenská
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
Slovensko	Slovensko	k1gNnSc4	Slovensko
má	mít	k5eAaImIp3nS	mít
talent	talent	k1gInSc1	talent
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
společně	společně	k6eAd1	společně
organizovaly	organizovat	k5eAaBmAgFnP	organizovat
televize	televize	k1gFnPc1	televize
Prima	primo	k1gNnSc2	primo
a	a	k8xC	a
JOJ	jojo	k1gNnPc2	jojo
<g/>
.	.	kIx.	.
</s>
<s>
Porotu	porota	k1gFnSc4	porota
i	i	k8xC	i
diváky	divák	k1gMnPc4	divák
nadchla	nadchnout	k5eAaPmAgFnS	nadchnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
prošla	projít	k5eAaPmAgFnS	projít
až	až	k9	až
do	do	k7c2	do
samotného	samotný	k2eAgNnSc2d1	samotné
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Euphory	Euphora	k1gFnSc2	Euphora
Dark	Darka	k1gFnPc2	Darka
Sky	Sky	k1gMnSc1	Sky
Cuba	Cuba	k1gMnSc1	Cuba
Bird	Bird	k1gMnSc1	Bird
Krčmička	krčmička	k1gFnSc1	krčmička
Taumata	Tauma	k1gNnPc1	Tauma
http://www.csmatalent.cz/mako-mako-cz/clanok/cz-makomako.html	[url]	k5eAaPmAgInS	http://www.csmatalent.cz/mako-mako-cz/clanok/cz-makomako.html
http://www.makomako.cz/aktuality/	[url]	k?	http://www.makomako.cz/aktuality/
http://bandzone.cz/makomako	[url]	k6eAd1	http://bandzone.cz/makomako
</s>
