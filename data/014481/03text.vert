<s>
Šimonovice	Šimonovice	k1gFnSc1
</s>
<s>
Šimonovice	Šimonovice	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0513	CZ0513	k4
564460	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Liberec	Liberec	k1gInSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
513	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Liberecký	liberecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
51	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
314	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
7,19	7,19	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
485	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
463	#num#	k4
12	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
3	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
3	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
3	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Minkovice	Minkovice	k1gFnSc1
70	#num#	k4
<g/>
Šimonovice	Šimonovice	k1gFnSc1
<g/>
46312	#num#	k4
Liberec	Liberec	k1gInSc1
25	#num#	k4
simonovice@volny.cz	simonovice@volny.cza	k1gFnPc2
Starostka	starostka	k1gFnSc1
</s>
<s>
Ing.	ing.	kA
Leona	Leona	k1gFnSc1
Vránová	Vránová	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.simonovice.cz	www.simonovice.cz	k1gInSc1
</s>
<s>
Šimonovice	Šimonovice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
564460	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šimonovice	Šimonovice	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Schimsdorf	Schimsdorf	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
obcí	obec	k1gFnSc7
v	v	k7c6
okrese	okres	k1gInSc6
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c6
svazích	svah	k1gInPc6
Ještědského	ještědský	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
pod	pod	k7c7
tzv.	tzv.	kA
Rašovským	Rašovský	k1gMnSc7
hřebenem	hřeben	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obce	obec	k1gFnSc2
vede	vést	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
do	do	k7c2
Rašovského	Rašovského	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
je	být	k5eAaImIp3nS
nádherný	nádherný	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
vrch	vrch	k1gInSc4
Javorník	Javorník	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
1	#num#	k4
300	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1545	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ves	ves	k1gFnSc1
náležela	náležet	k5eAaImAgFnS
k	k	k7c3
dubskému	dubský	k2eAgNnSc3d1
panství	panství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
pravděpodobně	pravděpodobně	k6eAd1
českou	český	k2eAgFnSc7d1
vnitřní	vnitřní	k2eAgFnSc7d1
kolonizací	kolonizace	k1gFnSc7
-	-	kIx~
název	název	k1gInSc1
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
jména	jméno	k1gNnSc2
prvního	první	k4xOgMnSc2
rychtáře	rychtář	k1gMnSc2
Šimona	Šimon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
zejména	zejména	k9
za	za	k7c2
protireformace	protireformace	k1gFnSc2
<g/>
,	,	kIx,
sem	sem	k6eAd1
však	však	k9
přicházeli	přicházet	k5eAaImAgMnP
noví	nový	k2eAgMnPc1d1
osadníci	osadník	k1gMnPc1
ze	z	k7c2
Saska	Sasko	k1gNnSc2
<g/>
,	,	kIx,
ves	ves	k1gFnSc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
poněmčila	poněmčit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Ves	ves	k1gFnSc1
byla	být	k5eAaImAgFnS
dlouho	dlouho	k6eAd1
samostatnou	samostatný	k2eAgFnSc4d1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1980	#num#	k4
však	však	k9
byla	být	k5eAaImAgFnS
sloučena	sloučit	k5eAaPmNgFnS
s	s	k7c7
Dlouhým	dlouhý	k2eAgInSc7d1
Mostem	most	k1gInSc7
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
pak	pak	k9
byla	být	k5eAaImAgFnS
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
připojena	připojen	k2eAgFnSc1d1
k	k	k7c3
Liberci	Liberec	k1gInSc3
jako	jako	k8xC,k8xS
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Liberec	Liberec	k1gInSc1
XXXIX-Šimonovice	XXXIX-Šimonovice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
opětovnému	opětovný	k2eAgNnSc3d1
osamostatnění	osamostatnění	k1gNnSc3
Šimonovic	Šimonovice	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1990	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
kdy	kdy	k6eAd1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
sídlem	sídlo	k1gNnSc7
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
spravuje	spravovat	k5eAaImIp3nS
území	území	k1gNnSc4
čtyř	čtyři	k4xCgFnPc2
kdysi	kdysi	k6eAd1
samostatných	samostatný	k2eAgFnPc2d1
vsí	ves	k1gFnPc2
či	či	k8xC
osad	osada	k1gFnPc2
<g/>
:	:	kIx,
Šimonovic	Šimonovice	k1gFnPc2
<g/>
,	,	kIx,
Minkovic	Minkovice	k1gFnPc2
<g/>
,	,	kIx,
Rašovky	Rašovka	k1gFnPc1
a	a	k8xC
Bystré	bystrý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nebývalému	bývalý	k2eNgInSc3d1,k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
stavební	stavební	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
na	na	k7c6
katastru	katastr	k1gInSc6
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
k	k	k7c3
výstavbě	výstavba	k1gFnSc3
nových	nový	k2eAgInPc2d1
rodinných	rodinný	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
její	její	k3xOp3gFnSc2
pěkné	pěkný	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
a	a	k8xC
blízkosti	blízkost	k1gFnSc2
města	město	k1gNnSc2
Liberce	Liberec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
bylo	být	k5eAaImAgNnS
schváleno	schválit	k5eAaPmNgNnS
usnesením	usnesení	k1gNnSc7
výboru	výbor	k1gInSc2
pro	pro	k7c4
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc4
a	a	k8xC
tělovýchovu	tělovýchova	k1gFnSc4
udělení	udělení	k1gNnSc2
znaku	znak	k1gInSc2
a	a	k8xC
vlajky	vlajka	k1gFnSc2
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
barokní	barokní	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1734	#num#	k4
-	-	kIx~
čtvercového	čtvercový	k2eAgInSc2d1
půdorysu	půdorys	k1gInSc2
<g/>
,	,	kIx,
krytá	krytý	k2eAgFnSc1d1
cibulovitou	cibulovitý	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
se	s	k7c7
zvoničkou	zvonička	k1gFnSc7
<g/>
;	;	kIx,
zbořena	zbořen	k2eAgFnSc1d1
za	za	k7c2
socialismu	socialismus	k1gInSc2
při	při	k7c6
výstavbě	výstavba	k1gFnSc6
zbytečně	zbytečně	k6eAd1
předimenzovaného	předimenzovaný	k2eAgInSc2d1
kravína	kravín	k1gInSc2
přímo	přímo	k6eAd1
ve	v	k7c6
středu	střed	k1gInSc6
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
sochy	socha	k1gFnPc1
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Nepomuckého	Nepomucký	k1gMnSc4
a	a	k8xC
sv.	sv.	kA
Pavla	Pavla	k1gFnSc1
</s>
<s>
několik	několik	k4yIc1
staveb	stavba	k1gFnPc2
lidové	lidový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
</s>
<s>
významnější	významný	k2eAgFnPc4d2
památky	památka	k1gFnPc4
či	či	k8xC
zajímavosti	zajímavost	k1gFnPc4
lze	lze	k6eAd1
také	také	k9
najít	najít	k5eAaPmF
v	v	k7c6
osadách	osada	k1gFnPc6
obce	obec	k1gFnSc2
Šimonovice	Šimonovice	k1gFnSc2
-	-	kIx~
zejména	zejména	k9
na	na	k7c6
Rašovce	Rašovka	k1gFnSc6
(	(	kIx(
<g/>
kostelík	kostelík	k1gInSc1
<g/>
,	,	kIx,
hostinec	hostinec	k1gInSc1
s	s	k7c7
rozhlednou	rozhledna	k1gFnSc7
<g/>
)	)	kIx)
či	či	k8xC
v	v	k7c4
Bystré	bystrý	k2eAgFnPc4d1
(	(	kIx(
<g/>
stavby	stavba	k1gFnPc4
lidové	lidový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Horecké	Horecký	k2eAgFnPc1d1
skály	skála	k1gFnPc1
</s>
<s>
vrch	vrch	k1gInSc1
Javorník	Javorník	k1gInSc1
(	(	kIx(
<g/>
684	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Rašovský	Rašovský	k1gMnSc1
hřeben	hřeben	k1gInSc4
</s>
<s>
Rašovské	Rašovské	k2eAgNnSc1d1
sedlo	sedlo	k1gNnSc1
</s>
<s>
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Šimonovice	Šimonovice	k1gFnSc1
–	–	k?
Osídlení	osídlení	k1gNnSc2
Šimonovic	Šimonovice	k1gFnPc2
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
roztroušené	roztroušený	k2eAgNnSc1d1
a	a	k8xC
vyvíjelo	vyvíjet	k5eAaImAgNnS
se	se	k3xPyFc4
ve	v	k7c6
třech	tři	k4xCgFnPc6
odlišných	odlišný	k2eAgFnPc6d1
částech	část	k1gFnPc6
<g/>
:	:	kIx,
spodní	spodní	k2eAgFnPc1d1
části	část	k1gFnPc1
vsi	ves	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
býval	bývat	k5eAaImAgInS
mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
říkalo	říkat	k5eAaImAgNnS
Stará	starý	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c6
dnešní	dnešní	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
000	#num#	k4
Ještědský	ještědský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
pomístní	pomístní	k2eAgInSc1d1
název	název	k1gInSc1
V	v	k7c6
Lípě	lípa	k1gFnSc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
dnešním	dnešní	k2eAgInSc6d1
katastru	katastr	k1gInSc6
Minkovic	Minkovice	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
tzv.	tzv.	kA
Hraničním	hraniční	k2eAgInSc6d1
potoce	potok	k1gInSc6
<g/>
;	;	kIx,
třetí	třetí	k4xOgFnSc7
částí	část	k1gFnSc7
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
Horní	horní	k2eAgFnSc2d1
Šimonovice	Šimonovice	k1gFnSc2
a	a	k8xC
tento	tento	k3xDgInSc1
název	název	k1gInSc1
přesně	přesně	k6eAd1
vystihuje	vystihovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
polohu	poloha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Oficiálními	oficiální	k2eAgFnPc7d1
součástmi	součást	k1gFnPc7
dnešní	dnešní	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Šimonovice	Šimonovice	k1gFnSc2
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgFnPc4
další	další	k2eAgFnPc4d1
někdejší	někdejší	k2eAgFnPc4d1
samostatná	samostatný	k2eAgNnPc4d1
sídla	sídlo	k1gNnPc4
<g/>
:	:	kIx,
</s>
<s>
místní	místní	k2eAgFnSc1d1
část	část	k1gFnSc1
Minkovice	Minkovice	k1gFnSc2
</s>
<s>
místní	místní	k2eAgFnSc1d1
část	část	k1gFnSc1
Rašovka	Rašovka	k1gFnSc1
s	s	k7c7
osadou	osada	k1gFnSc7
Bystrá	bystrý	k2eAgNnPc4d1
</s>
<s>
pohled	pohled	k1gInSc1
od	od	k7c2
Šimonovic	Šimonovice	k1gFnPc2
na	na	k7c4
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
</s>
<s>
lidová	lidový	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Šimonovicích	Šimonovice	k1gFnPc6
</s>
<s>
pohled	pohled	k1gInSc1
od	od	k7c2
Šimonovic	Šimonovice	k1gFnPc2
na	na	k7c4
Minkovice	Minkovice	k1gFnPc4
a	a	k8xC
Liberec	Liberec	k1gInSc4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Usnesení	usnesení	k1gNnSc4
č.	č.	k?
42	#num#	k4
<g/>
,	,	kIx,
Usnesení	usnesení	k1gNnSc1
výboru	výbor	k1gInSc2
pro	pro	k7c4
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc4
a	a	k8xC
tělovýchovu	tělovýchova	k1gFnSc4
z	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
k	k	k7c3
návrhu	návrh	k1gInSc3
na	na	k7c4
udělení	udělení	k1gNnSc4
znaků	znak	k1gInPc2
a	a	k8xC
vlajek	vlajka	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šimonovice	Šimonovice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Šimonovice	Šimonovice	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
Šimonovice	Šimonovice	k1gFnSc2
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
MinkoviceRašovkaŠimonovice	MinkoviceRašovkaŠimonovice	k1gFnSc1
Ostatní	ostatní	k2eAgFnSc2d1
</s>
<s>
Bystrá	bystrý	k2eAgFnSc1d1
</s>
<s>
Města	město	k1gNnPc1
<g/>
,	,	kIx,
městyse	městys	k1gInPc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Liberec	Liberec	k1gInSc1
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
•	•	k?
Bílý	bílý	k2eAgInSc1d1
Kostel	kostel	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
•	•	k?
Bílý	bílý	k2eAgInSc1d1
Potok	potok	k1gInSc1
•	•	k?
Bulovka	Bulovka	k1gFnSc1
•	•	k?
Cetenov	Cetenov	k1gInSc4
•	•	k?
Černousy	Černous	k1gInPc4
•	•	k?
Český	český	k2eAgMnSc1d1
Dub	Dub	k1gMnSc1
•	•	k?
Čtveřín	Čtveřín	k1gMnSc1
•	•	k?
Dětřichov	Dětřichov	k1gInSc1
•	•	k?
Dlouhý	dlouhý	k2eAgInSc1d1
Most	most	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Řasnice	Řasnice	k1gFnPc1
•	•	k?
Frýdlant	Frýdlant	k1gInSc1
•	•	k?
Habartice	Habartice	k1gFnSc2
•	•	k?
Hejnice	Hejnice	k1gFnSc2
•	•	k?
Heřmanice	Heřmanice	k1gFnSc2
•	•	k?
Hlavice	hlavice	k1gFnSc2
•	•	k?
Hodkovice	Hodkovice	k1gFnSc1
nad	nad	k7c7
Mohelkou	Mohelka	k1gFnSc7
•	•	k?
Horní	horní	k2eAgFnSc1d1
Řasnice	Řasnice	k1gFnSc1
•	•	k?
Hrádek	hrádek	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
•	•	k?
Chotyně	Chotyně	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Chrastava	Chrastava	k1gFnSc1
•	•	k?
Jablonné	Jablonná	k1gFnSc2
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
•	•	k?
Janovice	Janovice	k1gFnPc1
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
•	•	k?
Janův	Janův	k2eAgInSc1d1
Důl	důl	k1gInSc1
•	•	k?
Jeřmanice	Jeřmanice	k1gFnSc2
•	•	k?
Jindřichovice	Jindřichovice	k1gFnSc1
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
•	•	k?
Kobyly	kobyla	k1gFnSc2
•	•	k?
Krásný	krásný	k2eAgInSc4d1
Les	les	k1gInSc4
•	•	k?
Kryštofovo	Kryštofův	k2eAgNnSc4d1
Údolí	údolí	k1gNnSc4
•	•	k?
Křižany	Křižana	k1gFnPc4
•	•	k?
Kunratice	Kunratice	k1gFnPc4
•	•	k?
Lázně	lázeň	k1gFnSc2
Libverda	Libverda	k1gMnSc1
•	•	k?
Lažany	Lažana	k1gFnSc2
•	•	k?
Liberec	Liberec	k1gInSc1
•	•	k?
Mníšek	Mníšek	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
•	•	k?
Oldřichov	Oldřichov	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
Hájích	háj	k1gInPc6
•	•	k?
Osečná	Osečný	k2eAgFnSc1d1
•	•	k?
Paceřice	Paceřice	k1gFnSc1
•	•	k?
Pěnčín	Pěnčín	k1gInSc1
•	•	k?
Pertoltice	Pertoltice	k1gFnSc2
•	•	k?
Proseč	Proseč	k1gFnSc4
pod	pod	k7c7
Ještědem	Ještěd	k1gInSc7
•	•	k?
Příšovice	Příšovice	k1gFnSc2
•	•	k?
Radimovice	Radimovice	k1gFnSc2
•	•	k?
Raspenava	Raspenava	k1gFnSc1
•	•	k?
Rynoltice	Rynoltice	k1gFnSc2
•	•	k?
Soběslavice	Soběslavice	k1gFnSc2
•	•	k?
Stráž	stráž	k1gFnSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
•	•	k?
Světlá	světlat	k5eAaImIp3nS
pod	pod	k7c7
Ještědem	Ještěd	k1gInSc7
•	•	k?
Svijanský	Svijanský	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Svijany	Svijana	k1gFnSc2
•	•	k?
Sychrov	Sychrov	k1gInSc1
•	•	k?
Šimonovice	Šimonovice	k1gFnSc2
•	•	k?
Višňová	višňový	k2eAgFnSc1d1
•	•	k?
Vlastibořice	Vlastibořice	k1gFnSc1
•	•	k?
Všelibice	Všelibice	k1gFnSc1
•	•	k?
Zdislava	Zdislava	k1gFnSc1
•	•	k?
Žďárek	Žďárek	k1gInSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
</s>
