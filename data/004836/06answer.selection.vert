<s>
Jméno	jméno	k1gNnSc1	jméno
Ghibli	Ghibli	k1gFnSc2	Ghibli
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
přezdívky	přezdívka	k1gFnSc2	přezdívka
pro	pro	k7c4	pro
saharská	saharský	k2eAgNnPc4d1	saharské
průzkumná	průzkumný	k2eAgNnPc4d1	průzkumné
letadla	letadlo	k1gNnPc4	letadlo
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
výrazu	výraz	k1gInSc2	výraz
ghibli	ghibnout	k5eAaPmAgMnP	ghibnout
(	(	kIx(	(
<g/>
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
sirocco	sirocco	k1gMnSc1	sirocco
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgMnSc1d1	jiný
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
horký	horký	k2eAgInSc4d1	horký
vítr	vítr	k1gInSc4	vítr
vanoucí	vanoucí	k2eAgFnSc7d1	vanoucí
Saharou	Sahara	k1gFnSc7	Sahara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
