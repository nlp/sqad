<s>
Kremace	kremace	k1gFnSc1	kremace
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
cremare	cremar	k1gMnSc5	cremar
-	-	kIx~	-
spalovat	spalovat	k5eAaImF	spalovat
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
pohřeb	pohřeb	k1gInSc4	pohřeb
žehem	žeh	k1gInSc7	žeh
<g/>
,	,	kIx,	,
zpopelnění	zpopelnění	k1gNnSc4	zpopelnění
(	(	kIx(	(
<g/>
synonyma	synonymum	k1gNnSc2	synonymum
cinerace	cinerace	k1gFnSc2	cinerace
<g/>
,	,	kIx,	,
incinerace	incinerace	k1gFnSc2	incinerace
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
cinis	cinis	k1gFnSc2	cinis
<g/>
,	,	kIx,	,
gen.	gen.	kA	gen.
cineris	cineris	k1gInSc1	cineris
popel	popel	k1gInSc1	popel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nekrokaustie	nekrokaustie	k1gFnSc1	nekrokaustie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
nekros	nekrosa	k1gFnPc2	nekrosa
–	–	k?	–
mrtvola	mrtvola	k1gFnSc1	mrtvola
a	a	k8xC	a
kausis	kausis	k1gFnSc1	kausis
–	–	k?	–
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
pálení	pálení	k1gNnSc1	pálení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
pohřbu	pohřeb	k1gInSc2	pohřeb
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
tělo	tělo	k1gNnSc1	tělo
spáleno	spálen	k2eAgNnSc1d1	spáleno
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgInPc1d1	lidský
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
urnách	urna	k1gFnPc6	urna
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
na	na	k7c6	na
rozptylové	rozptylový	k2eAgFnSc6d1	rozptylová
loučce	loučka	k1gFnSc6	loučka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uchovat	uchovat	k5eAaPmF	uchovat
část	část	k1gFnSc4	část
popela	popel	k1gInSc2	popel
v	v	k7c6	v
miniurně	miniurna	k1gFnSc6	miniurna
anebo	anebo	k8xC	anebo
zatavit	zatavit	k5eAaPmF	zatavit
popel	popel	k1gInSc4	popel
do	do	k7c2	do
památečního	památeční	k2eAgNnSc2d1	památeční
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
moderní	moderní	k2eAgInSc4d1	moderní
způsob	způsob	k1gInSc4	způsob
zachování	zachování	k1gNnSc4	zachování
památky	památka	k1gFnSc2	památka
na	na	k7c4	na
zesnulého	zesnulý	k1gMnSc4	zesnulý
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
také	také	k9	také
vysypány	vysypat	k5eAaPmNgFnP	vysypat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
zemřelém	zemřelý	k2eAgInSc6d1	zemřelý
nezůstal	zůstat	k5eNaPmAgInS	zůstat
žádný	žádný	k3yNgInSc4	žádný
hmotný	hmotný	k2eAgInSc4d1	hmotný
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
předmětem	předmět	k1gInSc7	předmět
úcty	úcta	k1gFnSc2	úcta
(	(	kIx(	(
<g/>
např.	např.	kA	např.
případ	případ	k1gInSc1	případ
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kremace	kremace	k1gFnSc1	kremace
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
starý	starý	k2eAgInSc1d1	starý
způsob	způsob	k1gInSc1	způsob
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
kultury	kultura	k1gFnSc2	kultura
popelnicových	popelnicový	k2eAgNnPc2d1	popelnicové
polí	pole	k1gNnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
kremace	kremace	k1gFnPc1	kremace
prováděly	provádět	k5eAaImAgFnP	provádět
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
krematoriích	krematorium	k1gNnPc6	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
pohřeb	pohřeb	k1gInSc1	pohřeb
žehem	žeh	k1gInSc7	žeh
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
mizel	mizet	k5eAaImAgMnS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
totiž	totiž	k9	totiž
patří	patřit	k5eAaImIp3nS	patřit
víra	víra	k1gFnSc1	víra
ve	v	k7c6	v
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc6	zmrtvýchvstání
zemřelých	zemřelý	k1gMnPc2	zemřelý
a	a	k8xC	a
kremace	kremace	k1gFnSc1	kremace
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
popření	popření	k1gNnSc4	popření
této	tento	k3xDgFnSc2	tento
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Spalování	spalování	k1gNnSc1	spalování
mrtvých	mrtvý	k1gMnPc2	mrtvý
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
dekret	dekret	k1gInSc1	dekret
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
789	[number]	k4	789
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
fungovalo	fungovat	k5eAaImAgNnS	fungovat
upálení	upálení	k1gNnSc4	upálení
jako	jako	k8xS	jako
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzrůstem	vzrůst	k1gInSc7	vzrůst
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
ubývat	ubývat	k5eAaImF	ubývat
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
zpopelňováním	zpopelňování	k1gNnSc7	zpopelňování
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
světě	svět	k1gInSc6	svět
povolil	povolit	k5eAaPmAgInS	povolit
zpopelňování	zpopelňování	k1gNnSc4	zpopelňování
zesnulých	zesnulá	k1gFnPc2	zesnulá
a	a	k8xC	a
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
postaveno	postaven	k2eAgNnSc4d1	postaveno
první	první	k4xOgNnSc4	první
krematorium	krematorium	k1gNnSc4	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
ještě	ještě	k9	ještě
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kremaci	kremace	k1gFnSc3	kremace
přísně	přísně	k6eAd1	přísně
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
preferuje	preferovat	k5eAaImIp3nS	preferovat
klasický	klasický	k2eAgInSc4d1	klasický
pohřeb	pohřeb	k1gInSc4	pohřeb
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kremace	kremace	k1gFnSc1	kremace
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
tolerována	tolerován	k2eAgFnSc1d1	tolerována
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyla	být	k5eNaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
jako	jako	k9	jako
manifestace	manifestace	k1gFnSc1	manifestace
nebožtíkova	nebožtíkův	k2eAgFnSc1d1	nebožtíkova
pohrdání	pohrdání	k1gNnSc6	pohrdání
katolickými	katolický	k2eAgFnPc7d1	katolická
představami	představa	k1gFnPc7	představa
a	a	k8xC	a
předpisy	předpis	k1gInPc7	předpis
o	o	k7c6	o
pohřbu	pohřeb	k1gInSc6	pohřeb
a	a	k8xC	a
o	o	k7c6	o
posledních	poslední	k2eAgFnPc6d1	poslední
věcech	věc	k1gFnPc6	věc
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Uchovávání	uchovávání	k1gNnSc1	uchovávání
popelu	popel	k1gInSc2	popel
zemřelých	zemřelý	k1gMnPc2	zemřelý
doma	doma	k6eAd1	doma
či	či	k8xC	či
jeho	jeho	k3xOp3gNnSc4	jeho
rozptylování	rozptylování	k1gNnSc4	rozptylování
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
však	však	k9	však
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
-	-	kIx~	-
popel	popel	k1gInSc1	popel
zbylý	zbylý	k2eAgInSc1d1	zbylý
po	po	k7c6	po
kremaci	kremace	k1gFnSc6	kremace
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
uchováván	uchovávat	k5eAaImNgMnS	uchovávat
na	na	k7c6	na
posvěcených	posvěcený	k2eAgNnPc6d1	posvěcené
místech	místo	k1gNnPc6	místo
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
určených	určený	k2eAgInPc2d1	určený
<g/>
.	.	kIx.	.
</s>
<s>
Kremace	kremace	k1gFnSc1	kremace
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
nepřijatelné	přijatelný	k2eNgNnSc1d1	nepřijatelné
nakládání	nakládání	k1gNnSc1	nakládání
s	s	k7c7	s
lidskými	lidský	k2eAgInPc7d1	lidský
ostatky	ostatek	k1gInPc7	ostatek
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
je	být	k5eAaImIp3nS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
halachou	halacha	k1gFnSc7	halacha
<g/>
.	.	kIx.	.
na	na	k7c6	na
základě	základ	k1gInSc6	základ
biblických	biblický	k2eAgInPc2d1	biblický
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Islámu	islám	k1gInSc6	islám
platí	platit	k5eAaImIp3nS	platit
přísný	přísný	k2eAgInSc1d1	přísný
zákaz	zákaz	k1gInSc1	zákaz
kremací	kremace	k1gFnPc2	kremace
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vyžadovány	vyžadován	k2eAgInPc4d1	vyžadován
všechny	všechen	k3xTgInPc4	všechen
rituály	rituál	k1gInPc4	rituál
islámského	islámský	k2eAgInSc2d1	islámský
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
šířila	šířit	k5eAaImAgFnS	šířit
propagaci	propagace	k1gFnSc4	propagace
kremace	kremace	k1gFnSc1	kremace
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
spalování	spalování	k1gNnSc4	spalování
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
zániku	zánik	k1gInSc6	zánik
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
spolek	spolek	k1gInSc1	spolek
"	"	kIx"	"
<g/>
Krematorium	krematorium	k1gNnSc1	krematorium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Společnost	společnost	k1gFnSc1	společnost
přátel	přítel	k1gMnPc2	přítel
žehu	žeh	k1gInSc2	žeh
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
prvním	první	k4xOgMnSc6	první
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
monarchie	monarchie	k1gFnSc2	monarchie
platil	platit	k5eAaImAgInS	platit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
absolutní	absolutní	k2eAgInSc1d1	absolutní
zákaz	zákaz	k1gInSc1	zákaz
pohřbívání	pohřbívání	k1gNnSc1	pohřbívání
žehem	žeh	k1gInSc7	žeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
postavené	postavený	k2eAgNnSc1d1	postavené
krematorium	krematorium	k1gNnSc1	krematorium
německým	německý	k2eAgInSc7d1	německý
spolkem	spolek	k1gInSc7	spolek
Die	Die	k1gMnSc2	Die
Flamme	Flamme	k1gMnSc2	Flamme
tak	tak	k9	tak
mohlo	moct	k5eAaImAgNnS	moct
zahájit	zahájit	k5eAaPmF	zahájit
činnost	činnost	k1gFnSc4	činnost
až	až	k9	až
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
pohřbívání	pohřbívání	k1gNnPc4	pohřbívání
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
dle	dle	k7c2	dle
autora	autor	k1gMnSc2	autor
"	"	kIx"	"
<g/>
Lex	Lex	k1gMnSc1	Lex
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
účinný	účinný	k2eAgInSc4d1	účinný
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstručnějších	stručný	k2eAgInPc2d3	nejstručnější
zákonů	zákon	k1gInPc2	zákon
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
měl	mít	k5eAaImAgInS	mít
dva	dva	k4xCgInPc4	dva
paragrafy	paragraf	k1gInPc4	paragraf
<g/>
:	:	kIx,	:
§	§	k?	§
1	[number]	k4	1
–	–	k?	–
Pohřbívání	pohřbívání	k1gNnSc1	pohřbívání
ohněm	oheň	k1gInSc7	oheň
jest	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
<g/>
,	,	kIx,	,
§	§	k?	§
2	[number]	k4	2
–	–	k?	–
Provedením	provedení	k1gNnSc7	provedení
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
ministr	ministr	k1gMnSc1	ministr
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ve	v	k7c6	v
srozumění	srozumění	k1gNnSc6	srozumění
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pohřeb	pohřeb	k1gInSc1	pohřeb
žehem	žeh	k1gInSc7	žeh
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1921	[number]	k4	1921
v	v	k7c6	v
provizorním	provizorní	k2eAgNnSc6d1	provizorní
krematoriu	krematorium	k1gNnSc6	krematorium
na	na	k7c6	na
Olšanech	Olšany	k1gInPc6	Olšany
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
a	a	k8xC	a
nehumánnímu	humánní	k2eNgNnSc3d1	nehumánní
zpopelňování	zpopelňování	k1gNnSc3	zpopelňování
lidí	člověk	k1gMnPc2	člověk
docházelo	docházet	k5eAaImAgNnS	docházet
ve	v	k7c6	v
spalovacích	spalovací	k2eAgFnPc6d1	spalovací
pecích	pec	k1gFnPc6	pec
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
například	například	k6eAd1	například
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rakev	rakev	k1gFnSc1	rakev
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
se	se	k3xPyFc4	se
zpopelňuje	zpopelňovat	k5eAaImIp3nS	zpopelňovat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1150	[number]	k4	1150
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
1400	[number]	k4	1400
k	k	k7c3	k
2100	[number]	k4	2100
°	°	k?	°
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kremace	kremace	k1gFnSc2	kremace
shoří	shořet	k5eAaPmIp3nS	shořet
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
–	–	k?	–
obzvláště	obzvláště	k6eAd1	obzvláště
orgány	orgán	k1gInPc7	orgán
a	a	k8xC	a
jiná	jiný	k2eAgFnSc1d1	jiná
měkká	měkký	k2eAgFnSc1d1	měkká
tkáň	tkáň	k1gFnSc1	tkáň
–	–	k?	–
je	být	k5eAaImIp3nS	být
odpařován	odpařovat	k5eAaImNgInS	odpařovat
kvůli	kvůli	k7c3	kvůli
teplu	teplo	k1gNnSc3	teplo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vykonáván	vykonáván	k2eAgInSc1d1	vykonáván
přes	přes	k7c4	přes
odsávací	odsávací	k2eAgNnSc4d1	odsávací
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
žehu	žeh	k1gInSc2	žeh
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
pět	pět	k4xCc4	pět
procent	procento	k1gNnPc2	procento
původní	původní	k2eAgFnSc2d1	původní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
popel	popel	k1gInSc1	popel
rakve	rakev	k1gFnSc2	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
zpopelněny	zpopelněn	k2eAgFnPc1d1	zpopelněna
<g/>
.	.	kIx.	.
</s>
<s>
Spálení	spálení	k1gNnSc1	spálení
obvykle	obvykle	k6eAd1	obvykle
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
kremací	kremace	k1gFnSc7	kremace
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
vyjmut	vyjmout	k5eAaPmNgInS	vyjmout
kardiostimulátor	kardiostimulátor	k1gInSc1	kardiostimulátor
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
implantáty	implantát	k1gInPc1	implantát
a	a	k8xC	a
šperky	šperk	k1gInPc1	šperk
nejsou	být	k5eNaImIp3nP	být
překážkou	překážka	k1gFnSc7	překážka
zpopelnění	zpopelnění	k1gNnSc2	zpopelnění
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
Arimatheum	Arimatheum	k1gInSc1	Arimatheum
Kultura	kultura	k1gFnSc1	kultura
popelnicových	popelnicový	k2eAgNnPc2d1	popelnicové
polí	pole	k1gNnPc2	pole
Krematorium	krematorium	k1gNnSc1	krematorium
Pohřeb	pohřeb	k1gInSc1	pohřeb
ohněm	oheň	k1gInSc7	oheň
:	:	kIx,	:
několik	několik	k4yIc1	několik
kapitol	kapitola	k1gFnPc2	kapitola
o	o	k7c4	o
zpopelňování	zpopelňování	k1gNnSc4	zpopelňování
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Mencl	Mencl	k1gMnSc1	Mencl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Krematorium	krematorium	k1gNnSc1	krematorium
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
kremace	kremace	k1gFnSc2	kremace
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
otevření	otevření	k1gNnSc1	otevření
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
fungování	fungování	k1gNnSc1	fungování
pražského	pražský	k2eAgNnSc2d1	Pražské
krematoria	krematorium	k1gNnSc2	krematorium
v	v	k7c6	v
ústřední	ústřední	k2eAgFnSc6d1	ústřední
síni	síň	k1gFnSc6	síň
na	na	k7c6	na
Olšanském	olšanský	k2eAgInSc6d1	olšanský
hřbitově	hřbitov	k1gInSc6	hřbitov
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
texty	text	k1gInPc4	text
propagující	propagující	k2eAgInSc4d1	propagující
pohřeb	pohřeb	k1gInSc4	pohřeb
žehem	žeh	k1gInSc7	žeh
<g/>
..	..	k?	..
</s>
