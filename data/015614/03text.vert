<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
veslování	veslování	k1gNnSc6
1974	#num#	k4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
jména	jméno	k1gNnPc4
bez	bez	k7c2
správné	správný	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
transkripce	transkripce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspiraci	inspirace	k1gFnSc4
k	k	k7c3
vylepšení	vylepšení	k1gNnSc3
můžete	moct	k5eAaImIp2nP
hledat	hledat	k5eAaImF
v	v	k7c6
radách	rada	k1gFnPc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Transkripce	transkripce	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
veslování	veslování	k1gNnSc6
1974	#num#	k4
Místo	místo	k6eAd1
</s>
<s>
Rotsee	Rotsee	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Pořadatel	pořadatel	k1gMnSc1
</s>
<s>
World	World	k6eAd1
Rowing	Rowing	k1gInSc1
Federation	Federation	k1gInSc1
Datum	datum	k1gInSc1
</s>
<s>
1974	#num#	k4
Předchozí	předchozí	k2eAgFnSc1d1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
veslování	veslování	k1gNnSc6
1970	#num#	k4
Následující	následující	k2eAgFnSc4d1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
veslování	veslování	k1gNnSc6
1975	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
veslování	veslování	k1gNnSc6
1974	#num#	k4
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1974	#num#	k4
na	na	k7c6
jezeře	jezero	k1gNnSc6
Rotsee	Rotse	k1gFnSc2
ve	v	k7c6
švýcarském	švýcarský	k2eAgNnSc6d1
Luzernu	Luzerna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
již	již	k6eAd1
podruhé	podruhé	k6eAd1
(	(	kIx(
<g/>
konalo	konat	k5eAaImAgNnS
se	se	k3xPyFc4
zde	zde	k6eAd1
historicky	historicky	k6eAd1
první	první	k4xOgNnSc4
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
mistrovství	mistrovství	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
několika	několik	k4yIc6
ohledech	ohled	k1gInPc6
přelomové	přelomový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
závodily	závodit	k5eAaImAgFnP
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
byly	být	k5eAaImAgInP
v	v	k7c6
programu	program	k1gInSc6
závody	závod	k1gInPc1
veslařů	veslař	k1gMnPc2
lehkých	lehký	k2eAgFnPc2d1
vah	váha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počínaje	počínaje	k7c7
tímto	tento	k3xDgInSc7
rokem	rok	k1gInSc7
se	se	k3xPyFc4
mistrovství	mistrovství	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
konat	konat	k5eAaImF
každoročně	každoročně	k6eAd1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
jen	jen	k9
jednou	jeden	k4xCgFnSc7
za	za	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Veslařská	veslařský	k2eAgFnSc1d1
regata	regata	k1gFnSc1
je	být	k5eAaImIp3nS
organizována	organizovat	k5eAaBmNgFnS
Mezinárodní	mezinárodní	k2eAgFnSc7d1
veslařskou	veslařský	k2eAgFnSc7d1
federací	federace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neolympijských	olympijský	k2eNgNnPc6d1
letech	léto	k1gNnPc6
představuje	představovat	k5eAaImIp3nS
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
vyvrcholení	vyvrcholení	k1gNnSc4
mezinárodního	mezinárodní	k2eAgInSc2d1
veslařského	veslařský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
předchází	předcházet	k5eAaImIp3nS
olympijským	olympijský	k2eAgFnPc3d1
hrám	hra	k1gFnPc3
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
hlavní	hlavní	k2eAgFnSc4d1
kvalifikační	kvalifikační	k2eAgFnSc4d1
událost	událost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
olympijských	olympijský	k2eAgNnPc6d1
letech	léto	k1gNnPc6
pak	pak	k6eAd1
program	program	k1gInSc4
mistrovství	mistrovství	k1gNnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
neolympijské	olympijský	k2eNgFnPc4d1
disciplíny	disciplína	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Muži	muž	k1gMnPc1
závodili	závodit	k5eAaImAgMnP
na	na	k7c6
trati	trať	k1gFnSc6
o	o	k7c6
délce	délka	k1gFnSc6
2000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
ženy	žena	k1gFnPc1
na	na	k7c6
trati	trať	k1gFnSc6
o	o	k7c6
délce	délka	k1gFnSc6
1000	#num#	k4
m.	m.	k?
</s>
<s>
Medailové	medailový	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
USA	USA	kA
USA	USA	kA
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
17171751	#num#	k4
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mužské	mužský	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
SkifM	SkifM	k?
<g/>
1	#num#	k4
<g/>
x	x	k?
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Wolfgang	Wolfgang	k1gMnSc1
Hönig	Hönig	k1gMnSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
20,11	20,11	k4
</s>
<s>
USA	USA	kA
USA	USA	kA
James	James	k1gMnSc1
Dietz	Dietz	k1gMnSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
23,95	23,95	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Nikolaj	Nikolaj	k1gMnSc1
Dovgaň	Dovgaň	k1gMnSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
24,74	24,74	k4
</s>
<s>
DvojskifM	DvojskifM	k?
<g/>
2	#num#	k4
<g/>
x	x	k?
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Christof	Christof	k1gInSc1
Kreuziger	Kreuziger	k1gMnSc1
Hans-Ulrich	Hans-Ulrich	k1gMnSc1
Schmied	Schmied	k1gMnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
35,95	35,95	k4
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc4
Alf	alfa	k1gFnPc2
John	John	k1gMnSc1
Hansen	Hansen	k2eAgInSc4d1
Frank	frank	k1gInSc4
Hansen	Hansen	k1gInSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
38,34	38,34	k4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Christopher	Christophra	k1gFnPc2
Baillieu	Baillieus	k1gInSc2
Michael	Michael	k1gMnSc1
Hart	Hart	k1gMnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
43,32	43,32	k4
</s>
<s>
Párová	párový	k2eAgFnSc1d1
čtyřkaM	čtyřkam	k6eAd1
<g/>
4	#num#	k4
<g/>
x	x	k?
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Joachim	Joachima	k1gFnPc2
Dreifke	Dreifke	k1gNnSc2
Götz	Götz	k1gMnSc1
Draeger	Draeger	k1gMnSc1
Rüdiger	Rüdiger	k1gMnSc1
Reiche	Reiche	k1gFnPc2
Jürgen	Jürgen	k1gInSc4
Bertow	Bertow	k1gFnSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4,01	4,01	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Mustafajev	Mustafajev	k1gMnSc1
Kochel	Kochel	k1gMnSc1
Jurij	Jurij	k1gMnSc1
Jakimov	Jakimov	k1gInSc4
Gennadij	Gennadij	k1gFnSc2
Koršikov	Koršikov	k1gInSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5,79	5,79	k4
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Filip	Filip	k1gMnSc1
Koudela	Koudela	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Pecka	Pecka	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Laholík	Laholík	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Hellebrand	Hellebrand	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,12	8,12	k4
</s>
<s>
Dvojka	dvojka	k1gFnSc1
bez	bez	k7c2
kormidelníkaM	kormidelníkaM	k?
<g/>
2	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Bernd	Bernd	k1gInSc1
Landvoigt	Landvoigt	k1gMnSc1
Jörg	Jörg	k1gMnSc1
Landvoigt	Landvoigt	k1gMnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
59,09	59,09	k4
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Ilie	Ili	k1gFnSc2
Oanț	Oanț	k1gFnSc2
Dumitru	Dumitr	k1gInSc2
Grumezescu	Grumezescus	k1gInSc2
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3,99	3,99	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
Alfons	Alfons	k1gMnSc1
Ślusarski	Ślusarski	k1gNnSc4
Zbigniew	Zbigniew	k1gMnSc2
Ślusarski	Ślusarsk	k1gFnSc2
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4,64	4,64	k4
</s>
<s>
Dvojka	dvojka	k1gFnSc1
s	s	k7c7
kormidelníkemM	kormidelníkemM	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Vladimir	Vladimira	k1gFnPc2
Ješinov	Ješinov	k1gInSc1
Nikolaj	Nikolaj	k1gMnSc1
Ivanov	Ivanov	k1gInSc1
Alexandr	Alexandr	k1gMnSc1
Lukjanov	Lukjanov	k1gInSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
21,90	21,90	k4
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Wolfgang	Wolfgang	k1gMnSc1
Gunkel	Gunkel	k1gMnSc1
Jörg	Jörg	k1gMnSc1
Lucke	Lucke	k1gFnPc2
Klaus-Dieter	Klaus-Dieter	k1gMnSc1
Neubert	Neubert	k1gMnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
27,86	27,86	k4
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Oldřich	Oldřich	k1gMnSc1
Svojanovský	Svojanovský	k2eAgMnSc1d1
Pavel	Pavel	k1gMnSc1
Svojanovský	Svojanovský	k2eAgMnSc1d1
Vladimír	Vladimír	k1gMnSc1
Petříček	Petříček	k1gMnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
29,93	29,93	k4
</s>
<s>
Ctyřka	Ctyřka	k1gFnSc1
bez	bez	k7c2
kormidelníkaM	kormidelníkaM	k?
<g/>
4	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Siegfried	Siegfried	k1gMnSc1
Brietzke	Brietzk	k1gMnSc2
Andreas	Andreas	k1gMnSc1
Decker	Decker	k1gMnSc1
Stefan	Stefan	k1gMnSc1
Semmler	Semmler	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
Mager	Mager	k1gMnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
19,20	19,20	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Raul	Raula	k1gFnPc2
Arnemann	Arnemann	k1gInSc1
Nikolaj	Nikolaj	k1gMnSc1
Kuzněcov	Kuzněcov	k1gInSc1
Sergej	Sergej	k1gMnSc1
Posdějev	Posdějev	k1gMnSc1
Anušavan	Anušavan	k1gMnSc1
Gasan-Džalalov	Gasan-Džalalov	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
22,83	22,83	k4
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Peter	Petra	k1gFnPc2
van	vana	k1gFnPc2
Roye	Roy	k1gMnSc2
Klaus	Klaus	k1gMnSc1
Jäger	Jäger	k1gMnSc1
Bernd	Bernd	k1gMnSc1
Truschinski	Truschinsk	k1gFnSc2
Reinhard	Reinhard	k1gMnSc1
Wendemuth	Wendemuth	k1gMnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
27,01	27,01	k4
</s>
<s>
Čtyřka	čtyřka	k1gFnSc1
s	s	k7c7
kormidelníkemM	kormidelníkemM	k?
<g/>
4	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Andreas	Andreas	k1gInSc1
Schulz	Schulz	k1gMnSc1
Rüdiger	Rüdiger	k1gMnSc1
Kunze	Kunze	k1gFnSc2
Ullrich	Ullrich	k1gMnSc1
Dießner	Dießner	k1gMnSc1
Walter	Walter	k1gMnSc1
Dießner	Dießner	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
Groß	Groß	k1gMnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
26,38	26,38	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Gennadij	Gennadij	k1gMnSc1
Moskovskij	Moskovskij	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Pljuškin	Pljuškin	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Vasiljev	Vasiljev	k1gMnSc1
Anatolij	Anatolij	k1gFnPc2
Němtyrjov	Němtyrjov	k1gInSc1
Igor	Igor	k1gMnSc1
Rudakov	Rudakov	k1gInSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
27,34	27,34	k4
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Hans-Johann	Hans-Johann	k1gInSc1
Färber	Färber	k1gMnSc1
Ralph	Ralph	k1gMnSc1
Kubail	Kubail	k1gMnSc1
Peter-Michael	Peter-Michael	k1gMnSc1
Kolbe	Kolb	k1gInSc5
Peter	Peter	k1gMnSc1
Niehusen	Niehusna	k1gFnPc2
Uwe	Uwe	k1gMnSc1
Benter	Benter	k1gMnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
27,98	27,98	k4
</s>
<s>
OsmaM	OsmaM	k?
<g/>
8	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
USA	USA	kA
USA	USA	kA
Alan	Alan	k1gMnSc1
Shealy	Sheala	k1gFnSc2
Hugh	Hugh	k1gMnSc1
Stevenson	Stevenson	k1gMnSc1
Richard	Richard	k1gMnSc1
Cashin	Cashin	k1gMnSc1
Mark	Mark	k1gMnSc1
Norelius	Norelius	k1gMnSc1
John	John	k1gMnSc1
Everett	Everett	k1gMnSc1
Michael	Michael	k1gMnSc1
Vespoli	Vespole	k1gFnSc4
Timothy	Timotha	k1gFnSc2
Mickelson	Mickelson	k1gMnSc1
Kenneth	Kenneth	k1gMnSc1
Brown	Brown	k1gMnSc1
David	David	k1gMnSc1
Weinberg	Weinberg	k1gMnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
46,37	46,37	k4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Frederick	Fredericka	k1gFnPc2
Smallbone	Smallbon	k1gInSc5
John	John	k1gMnSc1
Yallop	Yallop	k1gInSc4
Timothy	Timotha	k1gFnSc2
Crooks	Crooksa	k1gFnPc2
Hugh	Hugh	k1gMnSc1
Matheson	Matheson	k1gMnSc1
David	David	k1gMnSc1
Maxwell	maxwell	k1gInSc4
Jim	on	k3xPp3gMnPc3
Clark	Clark	k1gInSc1
William	William	k1gInSc1
Mason	mason	k1gMnSc1
Leonard	Leonard	k1gMnSc1
Robertson	Robertson	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Sweeney	Sweenea	k1gFnSc2
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
47,49	47,49	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc4d1
Zéland	Zéland	k1gInSc4
Tony	Tony	k1gMnSc1
Hurt	Hurt	k1gMnSc1
Danny	Danna	k1gFnSc2
Keane	Kean	k1gInSc5
Lindsay	Lindsaa	k1gFnSc2
Wilson	Wilson	k1gMnSc1
Athol	Athola	k1gFnPc2
Earl	earl	k1gMnSc1
Trevor	Trevor	k1gMnSc1
Coker	Coker	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
McLean	McLean	k1gMnSc1
David	David	k1gMnSc1
Rodger	Rodger	k1gMnSc1
Ross	Rossa	k1gFnPc2
Blomfield	Blomfield	k1gMnSc1
David	David	k1gMnSc1
Simmons	Simmonsa	k1gFnPc2
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
47,84	47,84	k4
</s>
<s>
Skif	skif	k1gInSc1
LVLM	LVLM	kA
<g/>
1	#num#	k4
<g/>
x	x	k?
</s>
<s>
USA	USA	kA
USA	USA	kA
William	William	k1gInSc4
Belden	Beldna	k1gFnPc2
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
33,72	33,72	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
Harald	Haralda	k1gFnPc2
Punt	punto	k1gNnPc2
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
36,80	36,80	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Reto	Reto	k?
Wyss	Wyssa	k1gFnPc2
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
36,96	36,96	k4
</s>
<s>
Čtyřka	čtyřka	k1gFnSc1
bez	bez	k7c2
kormidelníka	kormidelník	k1gMnSc2
LVLM4-	LVLM4-	k1gMnSc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
Campbell	Campbell	k1gMnSc1
Johnstone	Johnston	k1gInSc5
Andrew	Andrew	k1gMnSc1
Michelmore	Michelmor	k1gInSc5
Geoffrey	Geoffrea	k1gMnSc2
Rees	Reesa	k1gFnPc2
Colin	Colin	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
38,12	38,12	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
Los	los	k1gInSc1
Hans	Hans	k1gMnSc1
Pieterman	Pieterman	k1gMnSc1
Jannes	Jannes	k1gMnSc1
Bruyn	Bruyn	k1gMnSc1
Hans	Hans	k1gMnSc1
Lycklama	Lycklama	k1gFnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
43,26	43,26	k4
</s>
<s>
USA	USA	kA
USA	USA	kA
Andrew	Andrew	k1gMnSc1
Washburn	Washburn	k1gMnSc1
Barry	Barra	k1gFnSc2
Selick	Selick	k1gMnSc1
James	James	k1gMnSc1
Ehrmann	Ehrmann	k1gMnSc1
Peter	Peter	k1gMnSc1
Huntsman	Huntsman	k1gMnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
43,48	43,48	k4
</s>
<s>
Osma	osma	k1gFnSc1
LVLM	LVLM	kA
<g/>
8	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
USA	USA	kA
USA	USA	kA
Matthew	Matthew	k1gMnSc1
Baldino	Baldin	k2eAgNnSc1d1
Joseph	Joseph	k1gMnSc1
Gayner	Gaynra	k1gFnPc2
Ralph	Ralph	k1gMnSc1
Nauman	Nauman	k1gMnSc1
David	David	k1gMnSc1
Harman	Harman	k1gMnSc1
Eric	Eric	k1gInSc4
Aserlind	Aserlind	k1gMnSc1
Richard	Richard	k1gMnSc1
Ewing	Ewing	k1gMnSc1
Mick	Mick	k1gMnSc1
Feld	Feld	k1gMnSc1
Richard	Richard	k1gMnSc1
Grogan	Grogan	k1gMnSc1
John	John	k1gMnSc1
Hartigan	Hartigan	k1gMnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
15,25	15,25	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
Cornelis	Cornelis	k1gInSc1
Bos	bos	k1gMnSc1
L.	L.	kA
Burghgraef	Burghgraef	k1gMnSc1
E.	E.	kA
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Snoek	Snoek	k6eAd1
W.	W.	kA
Mulder	Muldra	k1gFnPc2
B.	B.	kA
van	van	k1gInSc1
Aken	Aken	k1gInSc1
G.	G.	kA
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Werff	Werff	k1gMnSc1
Ch	Ch	kA
<g/>
.	.	kIx.
Hoynck	Hoynck	k1gMnSc1
van	van	k1gInSc4
Papendrecht	Papendrecht	k2eAgInSc4d1
H.	H.	kA
Hommen	Hommen	k2eAgInSc4d1
M.	M.	kA
van	van	k1gInSc1
de	de	k?
Broek	Broek	k1gInSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
17,54	17,54	k4
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Uwe	Uwe	k1gMnSc1
Barwig	Barwig	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
Fritsch	Fritsch	k1gMnSc1
Paul	Paul	k1gMnSc1
Lutz	Lutz	k1gMnSc1
Julius	Julius	k1gMnSc1
Nick	Nick	k1gMnSc1
Lutz	Lutz	k1gMnSc1
Kalmbacher	Kalmbachra	k1gFnPc2
Michael	Michael	k1gMnSc1
Speth	Speth	k1gMnSc1
Ekkehard	Ekkehard	k1gMnSc1
Braun	Braun	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
Gabler	Gabler	k1gMnSc1
Willi	Wille	k1gFnSc4
Seyer	Seyra	k1gFnPc2
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
17,54	17,54	k4
</s>
<s>
Ženské	ženský	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
SkifW	SkifW	k?
<g/>
1	#num#	k4
<g/>
x	x	k?
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Christine	Christin	k1gInSc5
Scheiblichová	Scheiblichová	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
46,52	46,52	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Genovaitė	Genovaitė	k1gMnSc2
Ramoškienė	Ramoškienė	k1gMnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
52,38	52,38	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
Christine	Christin	k1gInSc5
Wasterlainová	Wasterlainová	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
53,08	53,08	k4
</s>
<s>
DvojskifW	DvojskifW	k?
<g/>
2	#num#	k4
<g/>
x	x	k?
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Jelena	Jelen	k1gMnSc2
Antonovová	Antonovová	k1gFnSc1
Galina	Galina	k1gFnSc1
Jermolajevová	Jermolajevová	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24,00	24,00	k4
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Astrid	Astrid	k1gInSc1
Hohlová	Hohlová	k1gFnSc1
Regine	Regin	k1gInSc5
Adamová	Adamová	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
26,43	26,43	k4
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Gisela	Gisela	k1gFnSc1
Medefindtová	Medefindtová	k1gFnSc1
Rita	Rita	k1gFnSc1
Schmidtová	Schmidtová	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
28,78	28,78	k4
</s>
<s>
Párová	párový	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
s	s	k7c7
kormidelnicíW	kormidelnicíW	k?
<g/>
4	#num#	k4
<g/>
x	x	k?
<g/>
+	+	kIx~
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Roswietha	Roswietha	k1gFnSc1
Reichelová	Reichelová	k1gFnSc1
Ursula	Ursula	k1gFnSc1
Wagnerová	Wagnerová	k1gFnSc1
Jutta	Jutta	k1gFnSc1
Lauová	Lauová	k1gFnSc1
Sybille	Sybille	k1gFnSc1
Tietzeová	Tietzeová	k1gFnSc1
Liane	Lian	k1gInSc5
Buhrová	Buhrový	k2eAgFnSc1d1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
19,81	19,81	k4
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Ioana	Ioana	k1gFnSc1
Tudoranová	Tudoranová	k1gFnSc1
Elisabeta	Elisabeta	k1gFnSc1
Lazărová	Lazărová	k1gFnSc1
Doina	Doien	k2eAgFnSc1d1
Bardasová	Bardasový	k2eAgFnSc1d1
Teodora	Teodora	k1gFnSc1
Boicu	Boicus	k1gInSc2
Elena	Elena	k1gFnSc1
Giurcă	Giurcă	k1gFnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21,92	21,92	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Naděžda	Naděžda	k1gFnSc1
Šerbaková	Šerbakový	k2eAgFnSc1d1
Natalia	Natalia	k1gFnSc1
Gorodilovová	Gorodilovová	k1gFnSc1
Valentina	Valentina	k1gFnSc1
Ivčenková	Ivčenkový	k2eAgFnSc1d1
Antonina	Antonin	k2eAgFnSc1d1
Mariškinová	Mariškinový	k2eAgFnSc1d1
Irina	Irina	k1gFnSc1
Mojsejenková	Mojsejenkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22,64	22,64	k4
</s>
<s>
Dvojka	dvojka	k1gFnSc1
bez	bez	k7c2
kormidelniceW	kormidelniceW	k?
<g/>
2	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Marilena	Marilen	k2eAgFnSc1d1
Ghita	Ghita	k1gFnSc1
Cornelia	Cornelium	k1gNnSc2
Neascu	Neascus	k1gInSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
43,12	43,12	k4
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Renate	Renat	k1gInSc5
Bänschová	Bänschová	k1gFnSc1
Bergit	Bergit	k1gInSc1
Heinzeová	Heinzeová	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
45,18	45,18	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Janina	Janin	k2eAgFnSc1d1
Čigovská	Čigovská	k1gFnSc1
Ruta	rout	k5eAaImNgFnS
Weinzergová	Weinzergový	k2eAgFnSc1d1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
45,43	45,43	k4
</s>
<s>
Čtyřka	čtyřka	k1gFnSc1
s	s	k7c7
kormidelnicíW	kormidelnicíW	k?
<g/>
4	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Rosel	rosela	k1gFnPc2
Nitscheová	Nitscheová	k1gFnSc1
Angelika	Angelika	k1gFnSc1
Noacková	Noacková	k1gFnSc1
Renate	Renat	k1gInSc5
Schlenzigová	Schlenzigový	k2eAgFnSc1d1
Sabine	Sabin	k1gInSc5
Dähneová	Dähneová	k1gFnSc1
Christa	Christ	k1gMnSc2
Karnathová	Karnathová	k1gFnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
28,99	28,99	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
Liesbeth	Liesbeth	k1gMnSc1
de	de	k?
Bruinová	Bruinová	k1gFnSc1
Ingrid	Ingrid	k1gFnSc1
Munneke-Dusseldorp	Munneke-Dusseldorp	k1gInSc1
Maria	Maria	k1gFnSc1
Steenmanová	Steenmanová	k1gFnSc1
Louise	Louis	k1gMnSc2
de	de	k?
Graaffová	Graaffový	k2eAgFnSc1d1
M.	M.	kA
Kraayenhof	Kraayenhof	k1gInSc4
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
31,19	31,19	k4
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Marlene	Marlen	k1gInSc5
Predescu	Predescus	k1gInSc6
Kabat	Kabat	k2eAgInSc4d1
Avram	Avram	k1gInSc4
Chertic	Chertice	k1gFnPc2
Aneta	Aneta	k1gFnSc1
Matei	Matei	k1gNnSc2
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
32,71	32,71	k4
</s>
<s>
OsmaW	OsmaW	k?
<g/>
8	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Henrietta	Henrietta	k1gFnSc1
Doblerová	Doblerová	k1gFnSc1
Helma	helma	k1gFnSc1
Lehmannová	Lehmannová	k1gFnSc1
Ilona	Ilona	k1gFnSc1
Richterová	Richterová	k1gFnSc1
Bianka	Bianko	k1gNnSc2
Schwedeová	Schwedeový	k2eAgFnSc1d1
Brigitte	Brigitte	k1gFnSc1
Ahrenholzová	Ahrenholzový	k2eAgFnSc1d1
Irina	Irien	k2eAgFnSc1d1
Müllerová	Müllerová	k1gFnSc1
Gunhild	Gunhilda	k1gFnPc2
Blankeová	Blankeová	k1gFnSc1
Doris	Doris	k1gFnSc1
Mosigová	Mosigový	k2eAgFnSc1d1
Sabine	Sabin	k1gInSc5
Brinckerová	Brinckerový	k2eAgFnSc1d1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4,82	4,82	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Nina	Nina	k1gFnSc1
Bistrovová	Bistrovová	k1gFnSc1
Valentina	Valentina	k1gFnSc1
Rubcovová	Rubcovová	k1gFnSc1
Nina	Nina	k1gFnSc1
Abramovová	Abramovová	k1gFnSc1
Sofia	Sofia	k1gFnSc1
Šurkalovová	Šurkalovová	k1gFnSc1
Valentina	Valentina	k1gFnSc1
Jermakovová	Jermakovová	k1gFnSc1
Sergejewa	Sergejewa	k1gFnSc1
Věra	Věra	k1gFnSc1
Alexejevová	Alexejevová	k1gFnSc1
Nina	Nina	k1gFnSc1
Filatovová	Filatovová	k1gFnSc1
Nina	Nina	k1gFnSc1
Frolovová	Frolovová	k1gFnSc1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5,05	5,05	k4
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Elena	Elena	k1gFnSc1
Oprea	Oprea	k1gFnSc1
Florica	Florica	k1gFnSc1
Petcu	Petcus	k1gInSc2
Georgeta	Georget	k2eAgFnSc1d1
Militaru-Maș	Militaru-Maș	k1gFnSc1
Cristel	Cristela	k1gFnPc2
Wienerová	Wienerový	k2eAgFnSc1d1
Aurelia	Aurelia	k1gFnSc1
Marinescu	Marinescus	k1gInSc2
Luliana	Luliana	k1gFnSc1
Balabanová	Balabanová	k1gFnSc1
Avram	Avram	k1gInSc4
Chertic	Chertice	k1gFnPc2
Aneta	Aneta	k1gFnSc1
Matei	Matei	k1gNnSc2
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,31	8,31	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
–	–	k?
„	„	k?
<g/>
těžké	těžký	k2eAgFnSc2d1
<g/>
“	“	k?
váhy	váha	k1gFnSc2
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
–	–	k?
lehké	lehký	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
veslování	veslování	k1gNnSc6
</s>
<s>
Luzern	Luzern	k1gInSc1
1962	#num#	k4
•	•	k?
Bled	Bled	k1gInSc1
1966	#num#	k4
•	•	k?
St.	st.	kA
Catherines	Catherines	k1gInSc4
1970	#num#	k4
•	•	k?
Luzern	Luzern	k1gNnSc1
1974	#num#	k4
•	•	k?
Nottingham	Nottingham	k1gInSc1
1975	#num#	k4
•	•	k?
Villach	Villach	k1gInSc1
1976	#num#	k4
•	•	k?
Amsterdam	Amsterdam	k1gInSc1
1977	#num#	k4
•	•	k?
Karapiro	Karapiro	k1gNnSc1
/	/	kIx~
Kodaň	Kodaň	k1gFnSc1
1978	#num#	k4
•	•	k?
Bled	Bled	k1gInSc1
1979	#num#	k4
•	•	k?
Hazewinkel	Hazewinkel	k1gInSc1
1980	#num#	k4
•	•	k?
Mnichov	Mnichov	k1gInSc1
1981	#num#	k4
•	•	k?
Luzern	Luzern	k1gNnSc1
1982	#num#	k4
•	•	k?
Duisburg	Duisburg	k1gInSc1
1983	#num#	k4
•	•	k?
Montréal	Montréal	k1gInSc1
1984	#num#	k4
•	•	k?
Hazewinkel	Hazewinkel	k1gInSc1
1985	#num#	k4
•	•	k?
Nottingham	Nottingham	k1gInSc1
1986	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kodaň	Kodaň	k1gFnSc1
1987	#num#	k4
•	•	k?
Milán	Milán	k1gInSc1
1988	#num#	k4
•	•	k?
Bled	Bled	k1gInSc1
1989	#num#	k4
•	•	k?
Lake	Lak	k1gInPc1
Barrington	Barrington	k1gInSc1
1990	#num#	k4
•	•	k?
Vídeň	Vídeň	k1gFnSc1
1991	#num#	k4
•	•	k?
Montréal	Montréal	k1gInSc1
1992	#num#	k4
•	•	k?
Račice	račice	k1gFnSc1
1993	#num#	k4
•	•	k?
Indianapolis	Indianapolis	k1gInSc1
1994	#num#	k4
•	•	k?
Tampere	Tamper	k1gInSc5
1995	#num#	k4
•	•	k?
Motherwell	Motherwell	k1gInSc1
1996	#num#	k4
•	•	k?
Aiguebelette	Aiguebelett	k1gInSc5
1997	#num#	k4
•	•	k?
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
1998	#num#	k4
•	•	k?
St.	st.	kA
Catharines	Catharines	k1gInSc4
1999	#num#	k4
•	•	k?
Záhřeb	Záhřeb	k1gInSc1
2000	#num#	k4
•	•	k?
Luzern	Luzern	k1gNnSc1
2001	#num#	k4
•	•	k?
Sevilla	Sevilla	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
2002	#num#	k4
•	•	k?
Milán	Milán	k1gInSc1
2003	#num#	k4
•	•	k?
Banyoles	Banyoles	k1gInSc1
2004	#num#	k4
•	•	k?
Kaizu	Kaiz	k1gInSc2
2005	#num#	k4
•	•	k?
Eton	Eton	k1gInSc1
Lake	Lake	k1gFnSc1
2006	#num#	k4
•	•	k?
Mnichov	Mnichov	k1gInSc1
2007	#num#	k4
•	•	k?
Ottensheim	Ottensheim	k1gInSc1
2008	#num#	k4
•	•	k?
Poznaň	Poznaň	k1gFnSc1
2009	#num#	k4
•	•	k?
Karapiro	Karapiro	k1gNnSc1
2010	#num#	k4
•	•	k?
Bled	Bled	k1gInSc1
2011	#num#	k4
•	•	k?
Plovdiv	Plovdiv	k1gInSc1
2012	#num#	k4
•	•	k?
Čchungdžu	Čchungdžu	k1gFnPc2
2013	#num#	k4
•	•	k?
Amsterdam	Amsterdam	k1gInSc1
2014	#num#	k4
•	•	k?
Aiguebelette	Aiguebelett	k1gInSc5
2015	#num#	k4
•	•	k?
Rotterdam	Rotterdam	k1gInSc1
2016	#num#	k4
•	•	k?
Sarasota	Sarasota	k1gFnSc1
2017	#num#	k4
•	•	k?
Plovdiv	Plovdiv	k1gInSc1
2018	#num#	k4
•	•	k?
Ottensheim	Ottensheim	k1gMnSc1
2019	#num#	k4
</s>
