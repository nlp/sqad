<s>
Rumunština	rumunština	k1gFnSc1	rumunština
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
limba	limba	k1gFnSc1	limba
română	română	k?	română
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
s	s	k7c7	s
asi	asi	k9	asi
25	[number]	k4	25
miliony	milion	k4xCgInPc7	milion
mluvčími	mluvčí	k1gMnPc7	mluvčí
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Vojvodina	Vojvodina	k1gFnSc1	Vojvodina
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
nazývá	nazývat	k5eAaImIp3nS	nazývat
moldavština	moldavština	k1gFnSc1	moldavština
<g/>
.	.	kIx.	.
</s>
