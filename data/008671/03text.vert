<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
limba	limba	k1gFnSc1	limba
română	română	k?	română
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
s	s	k7c7	s
asi	asi	k9	asi
25	[number]	k4	25
miliony	milion	k4xCgInPc7	milion
mluvčími	mluvčí	k1gMnPc7	mluvčí
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Vojvodina	Vojvodina	k1gFnSc1	Vojvodina
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
nazývá	nazývat	k5eAaImIp3nS	nazývat
moldavština	moldavština	k1gFnSc1	moldavština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
106	[number]	k4	106
dobyli	dobýt	k5eAaPmAgMnP	dobýt
zemi	zem	k1gFnSc6	zem
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
udělali	udělat	k5eAaPmAgMnP	udělat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
svoji	svůj	k3xOyFgFnSc4	svůj
provincii	provincie	k1gFnSc4	provincie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
obydleno	obydlet	k5eAaPmNgNnS	obydlet
indoevropskými	indoevropský	k2eAgMnPc7d1	indoevropský
Dáky	Dák	k1gMnPc7	Dák
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Římanů	Říman	k1gMnPc2	Říman
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
lidová	lidový	k2eAgFnSc1d1	lidová
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
271	[number]	k4	271
až	až	k9	až
275	[number]	k4	275
ztratili	ztratit	k5eAaPmAgMnP	ztratit
Římané	Říman	k1gMnPc1	Říman
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
latinsky	latinsky	k6eAd1	latinsky
mluvícím	mluvící	k2eAgNnSc7d1	mluvící
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
dělo	dít	k5eAaImAgNnS	dít
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
geografické	geografický	k2eAgFnSc3d1	geografická
izolaci	izolace	k1gFnSc3	izolace
je	být	k5eAaImIp3nS	být
rumunština	rumunština	k1gFnSc1	rumunština
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
první	první	k4xOgInSc1	první
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
od	od	k7c2	od
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
nebyl	být	k5eNaImAgInS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
jinými	jiný	k2eAgInPc7d1	jiný
románskými	románský	k2eAgInPc7d1	románský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jmenná	jmenný	k2eAgFnSc1d1	jmenná
morfologie	morfologie	k1gFnSc1	morfologie
rumunštiny	rumunština	k1gFnSc2	rumunština
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
románským	románský	k2eAgMnPc3d1	románský
jazykům	jazyk	k1gMnPc3	jazyk
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Zachovala	zachovat	k5eAaPmAgFnS	zachovat
si	se	k3xPyFc3	se
skloňování	skloňování	k1gNnSc4	skloňování
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
střední	střední	k2eAgInSc4d1	střední
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
<s>
Slovesná	slovesný	k2eAgFnSc1d1	slovesná
morfologie	morfologie	k1gFnSc1	morfologie
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
stejný	stejný	k2eAgInSc4d1	stejný
posun	posun	k1gInSc4	posun
ke	k	k7c3	k
složenému	složený	k2eAgNnSc3d1	složené
perfektu	perfektum	k1gNnSc3	perfektum
a	a	k8xC	a
futuru	futurum	k1gNnSc3	futurum
jako	jako	k8xC	jako
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dialekty	dialekt	k1gInPc1	dialekt
rumunštiny	rumunština	k1gFnSc2	rumunština
byly	být	k5eAaImAgInP	být
sjednoceny	sjednotit	k5eAaPmNgInP	sjednotit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mezi	mezi	k7c7	mezi
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
byzantskou	byzantský	k2eAgFnSc7d1	byzantská
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
slovanskými	slovanský	k2eAgInPc7d1	slovanský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nebyl	být	k5eNaImAgInS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
tureckou	turecký	k2eAgFnSc7d1	turecká
a	a	k8xC	a
slovanskou	slovanský	k2eAgFnSc7d1	Slovanská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Romanizace	romanizace	k1gFnSc2	romanizace
jazyka	jazyk	k1gInSc2	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
vědomé	vědomý	k2eAgFnSc2d1	vědomá
romanizace	romanizace	k1gFnSc2	romanizace
jazyka	jazyk	k1gInSc2	jazyk
nacházíme	nacházet	k5eAaImIp1nP	nacházet
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
sedmihradských	sedmihradský	k2eAgMnPc2d1	sedmihradský
řeckokatolických	řeckokatolický	k2eAgMnPc2d1	řeckokatolický
(	(	kIx(	(
<g/>
uniátských	uniátský	k2eAgMnPc2d1	uniátský
<g/>
)	)	kIx)	)
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
Jana	Jan	k1gMnSc2	Jan
Samuela	Samuel	k1gMnSc2	Samuel
Kleina	Klein	k1gMnSc2	Klein
(	(	kIx(	(
<g/>
též	též	k9	též
Clain	Clain	k1gInSc1	Clain
<g/>
,	,	kIx,	,
romanizováno	romanizován	k2eAgNnSc1d1	romanizován
na	na	k7c4	na
Ioan	Ioan	k1gInSc4	Ioan
Micu	Micus	k1gInSc2	Micus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
byla	být	k5eAaImAgFnS	být
církevní	církevní	k2eAgFnSc1d1	církevní
unie	unie	k1gFnSc1	unie
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
podřízení	podřízení	k1gNnSc1	podřízení
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
pod	pod	k7c4	pod
římskou	římský	k2eAgFnSc4d1	římská
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
(	(	kIx(	(
<g/>
asi	asi	k9	asi
pět	pět	k4xCc4	pět
procent	procento	k1gNnPc2	procento
rumunských	rumunský	k2eAgMnPc2d1	rumunský
věřících	věřící	k1gMnPc2	věřící
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
uniáty	uniát	k1gInPc4	uniát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Propagace	propagace	k1gFnSc1	propagace
"	"	kIx"	"
<g/>
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
rumunské	rumunský	k2eAgFnSc2d1	rumunská
uniátské	uniátský	k2eAgFnSc2d1	uniátský
elity	elita	k1gFnSc2	elita
<g/>
,	,	kIx,	,
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
totiž	totiž	k9	totiž
možnost	možnost	k1gFnSc1	možnost
zatlačovat	zatlačovat	k5eAaImF	zatlačovat
řeckou	řecký	k2eAgFnSc4d1	řecká
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
ortodoxii	ortodoxie	k1gFnSc4	ortodoxie
a	a	k8xC	a
stavět	stavět	k5eAaImF	stavět
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
rovnocenná	rovnocenný	k2eAgFnSc1d1	rovnocenná
<g/>
"	"	kIx"	"
čelem	čelo	k1gNnSc7	čelo
ke	k	k7c3	k
katolickým	katolický	k2eAgMnPc3d1	katolický
Maďarům	Maďar	k1gMnPc3	Maďar
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
právního	právní	k2eAgNnSc2d1	právní
postavení	postavení	k1gNnSc2	postavení
"	"	kIx"	"
<g/>
Valachů	valach	k1gInPc2	valach
<g/>
"	"	kIx"	"
v	v	k7c6	v
habsburské	habsburský	k2eAgFnSc6d1	habsburská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
církevní	církevní	k2eAgFnSc1d1	církevní
unie	unie	k1gFnSc1	unie
i	i	k9	i
"	"	kIx"	"
<g/>
znovunalezené	znovunalezený	k2eAgNnSc1d1	znovunalezené
románství	románství	k1gNnSc1	románství
<g/>
"	"	kIx"	"
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gramatice	gramatika	k1gFnSc6	gramatika
sepsané	sepsaný	k2eAgFnSc2d1	sepsaná
Janem	Jan	k1gMnSc7	Jan
Kleinem-Micu	Kleinem-Micus	k1gInSc2	Kleinem-Micus
i	i	k8xC	i
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
slovnících	slovník	k1gInPc6	slovník
jsou	být	k5eAaImIp3nP	být
místo	místo	k1gNnSc4	místo
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
odvozených	odvozený	k2eAgInPc2d1	odvozený
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
slovanských	slovanský	k2eAgInPc2d1	slovanský
<g/>
,	,	kIx,	,
maďarštiny	maďarština	k1gFnPc4	maďarština
a	a	k8xC	a
turečtiny	turečtina	k1gFnPc4	turečtina
<g/>
,	,	kIx,	,
novotvary	novotvar	k1gInPc4	novotvar
(	(	kIx(	(
<g/>
neologismy	neologismus	k1gInPc4	neologismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnPc1d1	tvořená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
(	(	kIx(	(
<g/>
Transylvánii	Transylvánie	k1gFnSc6	Transylvánie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
prosadila	prosadit	k5eAaPmAgFnS	prosadit
i	i	k9	i
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
a	a	k8xC	a
Valašsku	Valašsko	k1gNnSc6	Valašsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
úplně	úplně	k6eAd1	úplně
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
dosud	dosud	k6eAd1	dosud
používanou	používaný	k2eAgFnSc4d1	používaná
cyrilici	cyrilice	k1gFnSc4	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
rumunština	rumunština	k1gFnSc1	rumunština
(	(	kIx(	(
<g/>
valaština	valaština	k1gFnSc1	valaština
<g/>
,	,	kIx,	,
moldavština	moldavština	k1gFnSc1	moldavština
<g/>
)	)	kIx)	)
natolik	natolik	k6eAd1	natolik
očištěna	očištěn	k2eAgFnSc1d1	očištěna
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgMnSc2d1	francouzský
a	a	k8xC	a
italského	italský	k2eAgInSc2d1	italský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
téměř	téměř	k6eAd1	téměř
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
konstruování	konstruování	k1gNnSc6	konstruování
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
rumunštině	rumunština	k1gFnSc6	rumunština
zní	znět	k5eAaImIp3nS	znět
tak	tak	k9	tak
zřetelně	zřetelně	k6eAd1	zřetelně
"	"	kIx"	"
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
antickým	antický	k2eAgNnSc7d1	antické
dědictvím	dědictví	k1gNnSc7	dědictví
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
novou	nový	k2eAgFnSc7d1	nová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
,	,	kIx,	,
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
i	i	k8xC	i
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
dlouho	dlouho	k6eAd1	dlouho
přetrvával	přetrvávat	k5eAaImAgMnS	přetrvávat
fenomén	fenomén	k1gInSc4	fenomén
dvou	dva	k4xCgInPc2	dva
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
umělého	umělý	k2eAgInSc2d1	umělý
spisovného	spisovný	k2eAgInSc2d1	spisovný
a	a	k8xC	a
literárního	literární	k2eAgInSc2d1	literární
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
tradičního	tradiční	k2eAgInSc2d1	tradiční
lidového	lidový	k2eAgInSc2d1	lidový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
literární	literární	k2eAgInSc1d1	literární
jazyk	jazyk	k1gInSc1	jazyk
prosadil	prosadit	k5eAaPmAgInS	prosadit
jako	jako	k9	jako
jazyk	jazyk	k1gInSc1	jazyk
státní	státní	k2eAgInSc1d1	státní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
romanizace	romanizace	k1gFnSc2	romanizace
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
názvu	název	k1gInSc6	název
"	"	kIx"	"
<g/>
rumunský	rumunský	k2eAgInSc4d1	rumunský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Limba	limba	k1gFnSc1	limba
rumânească	rumânească	k?	rumânească
–	–	k?	–
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Limba	limba	k1gFnSc1	limba
românească	românească	k?	românească
–	–	k?	–
nahrazení	nahrazení	k1gNnSc4	nahrazení
"	"	kIx"	"
<g/>
u	u	k7c2	u
<g/>
"	"	kIx"	"
za	za	k7c7	za
"	"	kIx"	"
<g/>
o	o	k7c4	o
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
podobnější	podobný	k2eAgNnSc4d2	podobnější
slovu	slout	k5eAaImIp1nS	slout
"	"	kIx"	"
<g/>
Roma	Rom	k1gMnSc4	Rom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Limba	limba	k1gFnSc1	limba
română	română	k?	română
–	–	k?	–
odstranění	odstranění	k1gNnSc2	odstranění
slovanského	slovanský	k2eAgInSc2d1	slovanský
gramatického	gramatický	k2eAgInSc2d1	gramatický
tvaru	tvar	k1gInSc2	tvar
"	"	kIx"	"
<g/>
-ască	scă	k?	-ască
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
současný	současný	k2eAgInSc1d1	současný
názevMoldavština	názevMoldavština	k1gFnSc1	názevMoldavština
oproti	oproti	k7c3	oproti
rumunštině	rumunština	k1gFnSc3	rumunština
zachovala	zachovat	k5eAaPmAgFnS	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
originální	originální	k2eAgInSc4d1	originální
název	název	k1gInSc4	název
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Limba	limba	k1gFnSc1	limba
moldovenească	moldovenească	k?	moldovenească
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontakty	kontakt	k1gInPc1	kontakt
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dáčtina	Dáčtin	k2eAgNnPc4d1	Dáčtin
===	===	k?	===
</s>
</p>
<p>
<s>
Dáčtina	Dáčtina	k1gFnSc1	Dáčtina
byl	být	k5eAaImAgInS	být
indoevropský	indoevropský	k2eAgInSc1d1	indoevropský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
mluvili	mluvit	k5eAaImAgMnP	mluvit
staří	starý	k2eAgMnPc1d1	starý
Dákové	Dák	k1gMnPc1	Dák
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
latinu	latina	k1gFnSc4	latina
v	v	k7c6	v
Dákii	Dákie	k1gFnSc6	Dákie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
rumunštině	rumunština	k1gFnSc6	rumunština
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
300	[number]	k4	300
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
dáčtiny	dáčtina	k1gFnSc2	dáčtina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
balaur	balaur	k1gMnSc1	balaur
<g/>
=	=	kIx~	=
<g/>
drak	drak	k1gMnSc1	drak
<g/>
,	,	kIx,	,
brânză	brânză	k?	brânză
<g/>
=	=	kIx~	=
<g/>
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
mal	málit	k5eAaImRp2nS	málit
<g/>
=	=	kIx~	=
<g/>
břeh	břeh	k1gInSc1	břeh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Balkánský	balkánský	k2eAgInSc1d1	balkánský
jazykový	jazykový	k2eAgInSc1d1	jazykový
svaz	svaz	k1gInSc1	svaz
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
rumunské	rumunský	k2eAgFnSc2d1	rumunská
gramatiky	gramatika	k1gFnSc2	gramatika
a	a	k8xC	a
morfologie	morfologie	k1gFnSc1	morfologie
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
latině	latina	k1gFnSc6	latina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sdílí	sdílet	k5eAaImIp3nP	sdílet
jen	jen	k9	jen
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
jazyky	jazyk	k1gInPc7	jazyk
v	v	k7c6	v
balkánském	balkánský	k2eAgInSc6d1	balkánský
jazykovém	jazykový	k2eAgInSc6d1	jazykový
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
sdílené	sdílený	k2eAgInPc4d1	sdílený
jevy	jev	k1gInPc4	jev
patří	patřit	k5eAaImIp3nS	patřit
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
následující	následující	k2eAgInSc1d1	následující
po	po	k7c6	po
řídícím	řídící	k2eAgNnSc6d1	řídící
slovu	slovo	k1gNnSc6	slovo
<g/>
,	,	kIx,	,
splynutí	splynutí	k1gNnSc1	splynutí
genitivu	genitiv	k1gInSc2	genitiv
a	a	k8xC	a
dativu	dativ	k1gInSc2	dativ
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
tvoření	tvoření	k1gNnSc2	tvoření
futura	futurum	k1gNnSc2	futurum
a	a	k8xC	a
perfekta	perfektum	k1gNnSc2	perfektum
a	a	k8xC	a
absence	absence	k1gFnSc2	absence
infinitivu	infinitiv	k1gInSc2	infinitiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
slovanského	slovanský	k2eAgInSc2d1	slovanský
vlivu	vliv	k1gInSc2	vliv
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
míře	míra	k1gFnSc6	míra
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
liturgickým	liturgický	k2eAgInSc7d1	liturgický
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
bulharština	bulharština	k1gFnSc1	bulharština
<g/>
,	,	kIx,	,
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
a	a	k8xC	a
srbština	srbština	k1gFnSc1	srbština
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
je	být	k5eAaImIp3nS	být
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
iubi	iubi	k6eAd1	iubi
<g/>
=	=	kIx~	=
<g/>
milovat	milovat	k5eAaImF	milovat
<g/>
,	,	kIx,	,
glas	glas	k1gInSc1	glas
<g/>
=	=	kIx~	=
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
nevoie	nevoie	k1gFnSc1	nevoie
<g/>
=	=	kIx~	=
<g/>
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
prieten	prieten	k2eAgMnSc1d1	prieten
<g/>
=	=	kIx~	=
<g/>
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
da	da	k?	da
<g/>
=	=	kIx~	=
<g/>
ano	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mnoho	mnoho	k4c1	mnoho
slovanských	slovanský	k2eAgNnPc2d1	slovanské
slov	slovo	k1gNnPc2	slovo
jsou	být	k5eAaImIp3nP	být
archaizmy	archaizmus	k1gInPc1	archaizmus
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
představuje	představovat	k5eAaImIp3nS	představovat
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
jen	jen	k9	jen
10	[number]	k4	10
%	%	kIx~	%
moderního	moderní	k2eAgInSc2d1	moderní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
slovanské	slovanský	k2eAgInPc4d1	slovanský
vlivy	vliv	k1gInPc4	vliv
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
zachování	zachování	k1gNnSc1	zachování
původního	původní	k2eAgInSc2d1	původní
latinského	latinský	k2eAgInSc2d1	latinský
fonému	foném	k1gInSc2	foném
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
dnešním	dnešní	k2eAgInSc6d1	dnešní
románském	románský	k2eAgInSc6d1	románský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
vlivy	vliv	k1gInPc1	vliv
===	===	k?	===
</s>
</p>
<p>
<s>
řečtina	řečtina	k1gFnSc1	řečtina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
folos	folos	k1gMnSc1	folos
<	<	kIx(	<
ófelos	ófelos	k1gMnSc1	ófelos
=	=	kIx~	=
užitek	užitek	k1gInSc1	užitek
<g/>
,	,	kIx,	,
buzunar	buzunar	k1gInSc1	buzunar
<	<	kIx(	<
buzunára	buzunár	k1gMnSc2	buzunár
=	=	kIx~	=
kapsa	kapsa	k1gFnSc1	kapsa
<g/>
,	,	kIx,	,
proaspăt	proaspăt	k1gInSc1	proaspăt
<	<	kIx(	<
prósfatos	prósfatos	k1gInSc1	prósfatos
=	=	kIx~	=
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
maďarština	maďarština	k1gFnSc1	maďarština
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
oraş	oraş	k?	oraş
<	<	kIx(	<
város	város	k1gInSc1	város
=	=	kIx~	=
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
cheltui	cheltu	k1gMnPc1	cheltu
<	<	kIx(	<
költeni	költen	k2eAgMnPc1d1	költen
=	=	kIx~	=
utrácet	utrácet	k5eAaImF	utrácet
<g/>
,	,	kIx,	,
a	a	k8xC	a
făgădui	făgădui	k6eAd1	făgădui
<	<	kIx(	<
fogadni	fogadnout	k5eAaPmRp2nS	fogadnout
=	=	kIx~	=
slíbit	slíbit	k5eAaPmF	slíbit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
turečtina	turečtina	k1gFnSc1	turečtina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
cafea	cafea	k1gMnSc1	cafea
<	<	kIx(	<
kahve	kahvat	k5eAaPmIp3nS	kahvat
=	=	kIx~	=
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
cutie	cutie	k1gFnSc1	cutie
<	<	kIx(	<
kuta	kut	k2eAgFnSc1d1	Kuta
=	=	kIx~	=
krabice	krabice	k1gFnSc1	krabice
<g/>
,	,	kIx,	,
papuc	papuc	k1gFnSc1	papuc
<	<	kIx(	<
papuç	papuç	k?	papuç
=	=	kIx~	=
papuče	papuče	k1gFnSc1	papuče
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
němčina	němčina	k1gFnSc1	němčina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
cartof	cartof	k1gMnSc1	cartof
<	<	kIx(	<
Kartoffel	Kartoffel	k1gMnSc1	Kartoffel
=	=	kIx~	=
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
<	<	kIx(	<
Bier	Bier	k1gInSc1	Bier
=	=	kIx~	=
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
ș	ș	k?	ș
<	<	kIx(	<
Schraube	Schraub	k1gInSc5	Schraub
=	=	kIx~	=
šroub	šroub	k1gInSc4	šroub
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgNnPc1d1	mezinárodní
slova	slovo	k1gNnPc1	slovo
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
rumunština	rumunština	k1gFnSc1	rumunština
vypůjčuje	vypůjčovat	k5eAaImIp3nS	vypůjčovat
mnoho	mnoho	k4c4	mnoho
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
italštiny	italština	k1gFnSc2	italština
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rumunštině	rumunština	k1gFnSc6	rumunština
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
38	[number]	k4	38
%	%	kIx~	%
slov	slovo	k1gNnPc2	slovo
vypůjčených	vypůjčený	k2eAgMnPc2d1	vypůjčený
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
domácího	domácí	k2eAgInSc2d1	domácí
původu	původ	k1gInSc2	původ
tvoří	tvořit	k5eAaImIp3nS	tvořit
kolem	kolem	k7c2	kolem
80	[number]	k4	80
%	%	kIx~	%
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
slova	slovo	k1gNnSc2	slovo
s	s	k7c7	s
původem	původ	k1gInSc7	původ
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
latinská	latinský	k2eAgNnPc1d1	latinské
slova	slovo	k1gNnPc1	slovo
byla	být	k5eAaImAgNnP	být
do	do	k7c2	do
rumunštiny	rumunština	k1gFnSc2	rumunština
přijata	přijmout	k5eAaPmNgFnS	přijmout
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
původní	původní	k2eAgFnSc2d1	původní
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
jako	jako	k9	jako
přejatá	přejatý	k2eAgNnPc4d1	přejaté
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Např	např	kA	např
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
bratr	bratr	k1gMnSc1	bratr
<g/>
/	/	kIx~	/
<g/>
bratrský	bratrský	k2eAgMnSc1d1	bratrský
<g/>
:	:	kIx,	:
frate	frate	k5eAaPmIp2nP	frate
<g/>
/	/	kIx~	/
<g/>
fratern	fratern	k1gInSc1	fratern
</s>
</p>
<p>
<s>
prst	prst	k1gInSc1	prst
<g/>
/	/	kIx~	/
<g/>
prstní	prstní	k2eAgMnSc1d1	prstní
<g/>
:	:	kIx,	:
deget	deget	k1gMnSc1	deget
<g/>
/	/	kIx~	/
<g/>
digital	digital	k1gMnSc1	digital
</s>
</p>
<p>
<s>
voda	voda	k1gFnSc1	voda
<g/>
/	/	kIx~	/
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
<g/>
:	:	kIx,	:
apă	apă	k?	apă
<g/>
/	/	kIx~	/
<g/>
acvatic	acvatice	k1gFnPc2	acvatice
</s>
</p>
<p>
<s>
oko	oko	k1gNnSc1	oko
<g/>
/	/	kIx~	/
<g/>
oční	oční	k2eAgNnPc1d1	oční
<g/>
:	:	kIx,	:
ochi	ochi	k1gNnPc1	ochi
<g/>
/	/	kIx~	/
<g/>
ocular	ocular	k1gInSc1	ocular
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rumunštinou	rumunština	k1gFnSc7	rumunština
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
postavení	postavení	k1gNnSc1	postavení
===	===	k?	===
</s>
</p>
<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Moldavské	moldavský	k2eAgFnSc2d1	Moldavská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
moldavština	moldavština	k1gFnSc1	moldavština
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
školním	školní	k2eAgNnSc6d1	školní
vysvědčení	vysvědčení	k1gNnSc6	vysvědčení
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
rumunština	rumunština	k1gFnSc1	rumunština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
jazykových	jazykový	k2eAgInPc2d1	jazykový
zákonů	zákon	k1gInPc2	zákon
je	být	k5eAaImIp3nS	být
rumunština	rumunština	k1gFnSc1	rumunština
a	a	k8xC	a
moldavština	moldavština	k1gFnSc1	moldavština
stejný	stejný	k2eAgInSc4d1	stejný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
Moldavské	moldavský	k2eAgFnSc6d1	Moldavská
podněsterské	podněsterský	k2eAgFnSc6d1	Podněsterská
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
Podněstří	Podněstří	k1gFnSc1	Podněstří
<g/>
)	)	kIx)	)
trvají	trvat	k5eAaImIp3nP	trvat
na	na	k7c4	na
užívání	užívání	k1gNnSc4	užívání
ruské	ruský	k2eAgFnSc2d1	ruská
azbuky	azbuka	k1gFnSc2	azbuka
místo	místo	k7c2	místo
latinky	latinka	k1gFnSc2	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyučují	vyučovat	k5eAaImIp3nP	vyučovat
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Lpění	lpění	k1gNnSc1	lpění
na	na	k7c6	na
užívání	užívání	k1gNnSc6	užívání
rumunštiny	rumunština	k1gFnSc2	rumunština
v	v	k7c6	v
azbuce	azbuka	k1gFnSc6	azbuka
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
rusifikace	rusifikace	k1gFnSc2	rusifikace
rumunskojazyčného	rumunskojazyčný	k2eAgNnSc2d1	rumunskojazyčný
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
raději	rád	k6eAd2	rád
posílají	posílat	k5eAaImIp3nP	posílat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
ruských	ruský	k2eAgFnPc2d1	ruská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dostane	dostat	k5eAaPmIp3nS	dostat
výuky	výuka	k1gFnSc2	výuka
standardní	standardní	k2eAgFnSc2d1	standardní
ruštiny	ruština	k1gFnSc2	ruština
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
děti	dítě	k1gFnPc4	dítě
naučí	naučit	k5eAaPmIp3nS	naučit
málo	málo	k6eAd1	málo
využitelný	využitelný	k2eAgInSc1d1	využitelný
pravopis	pravopis	k1gInSc1	pravopis
rumunštiny	rumunština	k1gFnSc2	rumunština
v	v	k7c6	v
azbuce	azbuka	k1gFnSc6	azbuka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
má	mít	k5eAaImIp3nS	mít
oficiálně	oficiálně	k6eAd1	oficiálně
stejný	stejný	k2eAgInSc1d1	stejný
status	status	k1gInSc1	status
jako	jako	k8xS	jako
srbština	srbština	k1gFnSc1	srbština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
méně	málo	k6eAd2	málo
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
mají	mít	k5eAaImIp3nP	mít
rumunské	rumunský	k2eAgFnSc2d1	rumunská
komunity	komunita	k1gFnSc2	komunita
malá	malý	k2eAgNnPc1d1	malé
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
jejich	jejich	k3xOp3gInSc2	jejich
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
na	na	k7c6	na
Hoře	hora	k1gFnSc6	hora
Athos	Athos	k1gInSc4	Athos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dialekty	dialekt	k1gInPc4	dialekt
===	===	k?	===
</s>
</p>
<p>
<s>
De	De	k?	De
facto	facto	k1gNnSc1	facto
existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc4	čtyři
"	"	kIx"	"
<g/>
rumunské	rumunský	k2eAgInPc4d1	rumunský
<g/>
"	"	kIx"	"
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
dva	dva	k4xCgMnPc4	dva
(	(	kIx(	(
<g/>
rumunština	rumunština	k1gFnSc1	rumunština
/	/	kIx~	/
moldavština	moldavština	k1gFnSc1	moldavština
a	a	k8xC	a
makedorumunština	makedorumunština	k1gFnSc1	makedorumunština
<g/>
)	)	kIx)	)
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Dako	Dako	k1gNnSc1	Dako
<g/>
)	)	kIx)	)
<g/>
rumunština	rumunština	k1gFnSc1	rumunština
–	–	k?	–
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
(	(	kIx(	(
<g/>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dako	Dako	k1gNnSc1	Dako
<g/>
)	)	kIx)	)
<g/>
rumunština	rumunština	k1gFnSc1	rumunština
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
moldavské	moldavský	k2eAgInPc4d1	moldavský
(	(	kIx(	(
<g/>
považované	považovaný	k2eAgInPc4d1	považovaný
za	za	k7c4	za
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
limba	limba	k1gFnSc1	limba
moldovenească	moldovenească	k?	moldovenească
<g/>
)	)	kIx)	)
–	–	k?	–
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
,	,	kIx,	,
Podněstří	Podněstří	k1gFnSc1	Podněstří
<g/>
,	,	kIx,	,
Bukovina	Bukovina	k1gFnSc1	Bukovina
a	a	k8xC	a
východní	východní	k2eAgNnSc1d1	východní
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
</s>
</p>
<p>
<s>
valašské	valašský	k2eAgFnPc4d1	Valašská
(	(	kIx(	(
<g/>
dialect	dialect	k2eAgInSc4d1	dialect
"	"	kIx"	"
<g/>
popular	popular	k1gInSc4	popular
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Valašsko	Valašsko	k1gNnSc1	Valašsko
<g/>
,	,	kIx,	,
Dobrudža	Dobrudža	k1gFnSc1	Dobrudža
a	a	k8xC	a
jižní	jižní	k2eAgNnSc1d1	jižní
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
</s>
</p>
<p>
<s>
marmarošské	marmarošský	k2eAgNnSc1d1	marmarošský
(	(	kIx(	(
<g/>
dialect	dialect	k2eAgMnSc1d1	dialect
de	de	k?	de
Maramureş	Maramureş	k1gMnSc1	Maramureş
<g/>
)	)	kIx)	)
–	–	k?	–
Marmaroš	Marmaroš	k1gMnSc1	Marmaroš
</s>
</p>
<p>
<s>
krišanské	krišanský	k2eAgNnSc1d1	krišanský
(	(	kIx(	(
<g/>
dialect	dialect	k2eAgMnSc1d1	dialect
de	de	k?	de
Criș	Criș	k1gMnSc1	Criș
<g/>
)	)	kIx)	)
–	–	k?	–
Krišana	Krišana	k1gFnSc1	Krišana
a	a	k8xC	a
západní	západní	k2eAgNnSc1d1	západní
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
</s>
</p>
<p>
<s>
banacké	banacké	k2eAgInSc1d1	banacké
(	(	kIx(	(
<g/>
dialect	dialect	k1gInSc1	dialect
de	de	k?	de
Banat	Banat	k1gInSc1	Banat
<g/>
)	)	kIx)	)
–	–	k?	–
Banát	Banát	k1gInSc1	Banát
<g/>
.	.	kIx.	.
<g/>
Makedorumunština	Makedorumunština	k1gFnSc1	Makedorumunština
(	(	kIx(	(
<g/>
Arumunština	Arumunština	k1gFnSc1	Arumunština
<g/>
)	)	kIx)	)
–	–	k?	–
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meglenorumunština	Meglenorumunština	k1gFnSc1	Meglenorumunština
–	–	k?	–
severní	severní	k2eAgNnSc4d1	severní
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Istrorumunština	Istrorumunština	k1gFnSc1	Istrorumunština
–	–	k?	–
Istrie	Istrie	k1gFnSc2	Istrie
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Rumunská	rumunský	k2eAgNnPc4d1	rumunské
jména	jméno	k1gNnPc4	jméno
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
3	[number]	k4	3
rody	rod	k1gInPc4	rod
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
a	a	k8xC	a
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
a	a	k8xC	a
vokativ	vokativ	k1gInSc1	vokativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
adjektiv	adjektivum	k1gNnPc2	adjektivum
a	a	k8xC	a
zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
řídícím	řídící	k2eAgNnSc7d1	řídící
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
ne	ne	k9	ne
před	před	k7c4	před
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
slovesné	slovesný	k2eAgFnPc4d1	slovesná
třídy	třída	k1gFnPc4	třída
a	a	k8xC	a
pět	pět	k4xCc4	pět
způsobů	způsob	k1gInPc2	způsob
(	(	kIx(	(
<g/>
indikativ	indikativ	k1gInSc1	indikativ
<g/>
,	,	kIx,	,
konjunktiv	konjunktiv	k1gInSc1	konjunktiv
<g/>
,	,	kIx,	,
imperativ	imperativ	k1gInSc1	imperativ
<g/>
,	,	kIx,	,
prezumptiv	prezumptiv	k1gInSc1	prezumptiv
a	a	k8xC	a
supinum	supinum	k1gNnSc1	supinum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Fonologie	fonologie	k1gFnSc2	fonologie
==	==	k?	==
</s>
</p>
<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
má	mít	k5eAaImIp3nS	mít
7	[number]	k4	7
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
20	[number]	k4	20
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hláskové	hláskový	k2eAgFnPc1d1	hlásková
změny	změna	k1gFnPc1	změna
===	===	k?	===
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
izolaci	izolace	k1gFnSc3	izolace
rumunštiny	rumunština	k1gFnSc2	rumunština
je	být	k5eAaImIp3nS	být
hláskový	hláskový	k2eAgInSc1d1	hláskový
vývoj	vývoj	k1gInSc1	vývoj
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
vývoje	vývoj	k1gInSc2	vývoj
ostatních	ostatní	k2eAgInPc2d1	ostatní
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
změny	změna	k1gFnPc1	změna
sdílí	sdílet	k5eAaImIp3nP	sdílet
s	s	k7c7	s
italštinou	italština	k1gFnSc7	italština
(	(	kIx(	(
<g/>
např.	např.	kA	např.
[	[	kIx(	[
<g/>
kl	kl	kA	kl
<g/>
]	]	kIx)	]
>	>	kIx)	>
[	[	kIx(	[
<g/>
kj	kj	k?	kj
<g/>
]	]	kIx)	]
–	–	k?	–
lat.	lat.	k?	lat.
clarus	clarus	k1gInSc1	clarus
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
chiar	chiar	k1gInSc1	chiar
<g/>
,	,	kIx,	,
it.	it.	k?	it.
chiaro	chiara	k1gFnSc5	chiara
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalmátštinou	dalmátština	k1gFnSc7	dalmátština
(	(	kIx(	(
<g/>
např.	např.	kA	např.
[	[	kIx(	[
<g/>
gn	gn	k?	gn
<g/>
]	]	kIx)	]
>	>	kIx)	>
[	[	kIx(	[
<g/>
mn	mn	k?	mn
<g/>
]	]	kIx)	]
–	–	k?	–
lat.	lat.	k?	lat.
cognatus	cognatus	k1gInSc1	cognatus
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
cumnat	cumnat	k5eAaImF	cumnat
<g/>
,	,	kIx,	,
dalm	dalm	k1gMnSc1	dalm
<g/>
.	.	kIx.	.
comnut	comnut	k1gMnSc1	comnut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgFnPc4d1	důležitá
hláskové	hláskový	k2eAgFnPc4d1	hlásková
změny	změna	k1gFnPc4	změna
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
diftongizace	diftongizace	k1gFnSc1	diftongizace
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
o	o	k7c6	o
–	–	k?	–
lat.	lat.	k?	lat.
cera	cera	k1gFnSc1	cera
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
ceară	ceară	k?	ceară
(	(	kIx(	(
<g/>
vosk	vosk	k1gInSc4	vosk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jotacizmus	jotacizmus	k1gInSc1	jotacizmus
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
>	>	kIx)	>
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
–	–	k?	–
lat.	lat.	k?	lat.
herba	herba	k1gFnSc1	herba
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
iarbă	iarbă	k?	iarbă
(	(	kIx(	(
<g/>
tráva	tráva	k1gFnSc1	tráva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
veláry	velára	k1gFnSc2	velára
[	[	kIx(	[
<g/>
k	k	k7c3	k
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
]	]	kIx)	]
>	>	kIx)	>
labiály	labiála	k1gFnPc1	labiála
[	[	kIx(	[
<g/>
p	p	k?	p
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
b	b	k?	b
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
]	]	kIx)	]
–	–	k?	–
lat.	lat.	k?	lat.
octo	octo	k1gMnSc1	octo
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
opt	opt	k?	opt
(	(	kIx(	(
<g/>
osm	osm	k4xCc1	osm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rotacizmus	rotacizmus	k1gInSc1	rotacizmus
[	[	kIx(	[
<g/>
l	l	kA	l
<g/>
]	]	kIx)	]
>	>	kIx)	>
[	[	kIx(	[
<g/>
r	r	kA	r
<g/>
]	]	kIx)	]
–	–	k?	–
lat.	lat.	k?	lat.
caelum	caelum	k1gInSc1	caelum
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
cer	cer	k1gInSc1	cer
(	(	kIx(	(
<g/>
nebe	nebe	k1gNnSc2	nebe
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
palatalizace	palatalizace	k1gFnSc1	palatalizace
dentál	dentála	k1gFnPc2	dentála
[	[	kIx(	[
<g/>
d	d	k?	d
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
t	t	k?	t
<g/>
]	]	kIx)	]
před	před	k7c7	před
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
–	–	k?	–
lat.	lat.	k?	lat.
deus	deus	k1gInSc1	deus
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
zeu	zeu	k?	zeu
(	(	kIx(	(
<g/>
bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
tenem	tenem	k1gInSc1	tenem
>	>	kIx)	>
rum	rum	k1gInSc1	rum
<g/>
.	.	kIx.	.
ţine	ţine	k1gInSc1	ţine
(	(	kIx(	(
<g/>
držet	držet	k5eAaImF	držet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Psaná	psaný	k2eAgFnSc1d1	psaná
podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
psaný	psaný	k2eAgInSc1d1	psaný
záznam	záznam	k1gInSc1	záznam
románského	románský	k2eAgInSc2d1	románský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
mluvilo	mluvit	k5eAaImAgNnS	mluvit
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
zapsána	zapsat	k5eAaPmNgNnP	zapsat
byzantským	byzantský	k2eAgMnSc7d1	byzantský
kronikářem	kronikář	k1gMnSc7	kronikář
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
o	o	k7c6	o
vojenské	vojenský	k2eAgFnSc6d1	vojenská
expedici	expedice	k1gFnSc6	expedice
proti	proti	k7c3	proti
Avarům	Avar	k1gMnPc3	Avar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
věta	věta	k1gFnSc1	věta
mezkaře	mezkař	k1gMnSc2	mezkař
doprovázejícího	doprovázející	k2eAgInSc2d1	doprovázející
byzantská	byzantský	k2eAgNnPc1d1	byzantské
vojska	vojsko	k1gNnPc1	vojsko
"	"	kIx"	"
<g/>
Torna	torna	k1gFnSc1	torna
<g/>
,	,	kIx,	,
torna	torna	k1gFnSc1	torna
<g/>
,	,	kIx,	,
fratre	fratr	k1gMnSc5	fratr
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Vrať	vrátit	k5eAaPmRp2nS	vrátit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vrať	vrátit	k5eAaPmRp2nS	vrátit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
bratře	bratr	k1gMnSc5	bratr
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zavolal	zavolat	k5eAaPmAgMnS	zavolat
na	na	k7c4	na
společníka	společník	k1gMnSc4	společník
<g/>
,	,	kIx,	,
když	když	k8xS	když
uviděl	uvidět	k5eAaPmAgMnS	uvidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
mezka	mezek	k1gMnSc2	mezek
spadl	spadnout	k5eAaPmAgInS	spadnout
náklad	náklad	k1gInSc1	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
psaný	psaný	k2eAgInSc1d1	psaný
text	text	k1gInSc1	text
v	v	k7c6	v
rumunštině	rumunština	k1gFnSc6	rumunština
je	být	k5eAaImIp3nS	být
dopis	dopis	k1gInSc4	dopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Neacș	Neacș	k1gFnSc1	Neacș
z	z	k7c2	z
Câmpulungu	Câmpulung	k1gInSc2	Câmpulung
píše	psát	k5eAaImIp3nS	psát
starostovi	starosta	k1gMnSc3	starosta
Brašova	Brašův	k2eAgNnSc2d1	Brašův
o	o	k7c6	o
hrozícím	hrozící	k2eAgInSc6d1	hrozící
útoku	útok	k1gInSc6	útok
Turků	turek	k1gInPc2	turek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
cyrilicí	cyrilice	k1gFnSc7	cyrilice
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
text	text	k1gInSc1	text
latinkou	latinka	k1gFnSc7	latinka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
napsán	napsat	k5eAaBmNgInS	napsat
v	v	k7c6	v
Transylvánii	Transylvánie	k1gFnSc6	Transylvánie
podle	podle	k7c2	podle
konvencí	konvence	k1gFnPc2	konvence
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rumunští	rumunský	k2eAgMnPc1d1	rumunský
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
majíce	mít	k5eAaImSgFnP	mít
na	na	k7c4	na
paměti	paměť	k1gFnSc2	paměť
původ	původ	k1gInSc4	původ
rumunštiny	rumunština	k1gFnSc2	rumunština
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
,	,	kIx,	,
převzali	převzít	k5eAaPmAgMnP	převzít
italský	italský	k2eAgInSc4d1	italský
pravopis	pravopis	k1gInSc4	pravopis
a	a	k8xC	a
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
ho	on	k3xPp3gMnSc4	on
rumunštině	rumunština	k1gFnSc3	rumunština
<g/>
.	.	kIx.	.
</s>
<s>
Cyrilice	cyrilice	k1gFnSc1	cyrilice
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
používala	používat	k5eAaImAgFnS	používat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
pravopis	pravopis	k1gInSc1	pravopis
poprvé	poprvé	k6eAd1	poprvé
normován	normovat	k5eAaBmNgInS	normovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Moldavské	moldavský	k2eAgFnSc6d1	Moldavská
SSR	SSR	kA	SSR
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
používala	používat	k5eAaImAgFnS	používat
upravená	upravený	k2eAgFnSc1d1	upravená
ruská	ruský	k2eAgFnSc1d1	ruská
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Abeceda	abeceda	k1gFnSc1	abeceda
===	===	k?	===
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ce	ce	k?	ce
<g/>
,	,	kIx,	,
ci	ci	k0	ci
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
če	če	k?	če
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
či	či	k8xC	či
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
che	che	k0	che
<g/>
,	,	kIx,	,
chi	chi	k0	chi
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
k	k	k7c3	k
<g/>
́	́	k?	́
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
k	k	k7c3	k
<g/>
́	́	k?	́
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
ca	ca	kA	ca
<g/>
,	,	kIx,	,
co	co	k9	co
<g/>
,	,	kIx,	,
cu	cu	k?	cu
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
ka	ka	k?	ka
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ko	ko	k?	ko
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ku	k	k7c3	k
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
ge	ge	k?	ge
<g/>
,	,	kIx,	,
gi	gi	k?	gi
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
dže	dže	k?	dže
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
dži	dži	k?	dži
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
ghe	ghe	k?	ghe
<g/>
,	,	kIx,	,
ghi	ghi	k?	ghi
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
́	́	k?	́
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
́	́	k?	́
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
ga	ga	k?	ga
<g/>
,	,	kIx,	,
go	go	k?	go
<g/>
,	,	kIx,	,
gu	gu	k?	gu
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
ga	ga	k?	ga
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
go	go	k?	go
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
gu	gu	k?	gu
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
nepřízvučné	přízvučný	k2eNgNnSc1d1	nepřízvučné
i	i	k9	i
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
tak	tak	k6eAd1	tak
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
že	že	k8xS	že
netvoří	tvořit	k5eNaImIp3nS	tvořit
slabiku	slabika	k1gFnSc4	slabika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
palatalizuje	palatalizovat	k5eAaBmIp3nS	palatalizovat
případnou	případný	k2eAgFnSc4d1	případná
předchozí	předchozí	k2eAgFnSc4d1	předchozí
souhlásku	souhláska	k1gFnSc4	souhláska
</s>
</p>
<p>
<s>
ă	ă	k?	ă
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
neurčitá	určitý	k2eNgFnSc1d1	neurčitá
souhláska	souhláska	k1gFnSc1	souhláska
[	[	kIx(	[
<g/>
ə	ə	k?	ə
<g/>
]	]	kIx)	]
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
</s>
</p>
<p>
<s>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
î	î	k?	î
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
â	â	k?	â
(	(	kIx(	(
<g/>
do	do	k7c2	do
reformy	reforma	k1gFnSc2	reforma
pravopisu	pravopis	k1gInSc2	pravopis
v	v	k7c6	v
r.	r.	kA	r.
1993	[number]	k4	1993
se	se	k3xPyFc4	se
všude	všude	k6eAd1	všude
psalo	psát	k5eAaImAgNnS	psát
î	î	k?	î
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
u	u	k7c2	u
bez	bez	k7c2	bez
zaokrouhlení	zaokrouhlení	k1gNnPc2	zaokrouhlení
rtů	ret	k1gInPc2	ret
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Otčenáš	otčenáš	k1gInSc4	otčenáš
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rumunština	rumunština	k1gFnSc1	rumunština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rumunština	rumunština	k1gFnSc1	rumunština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Rumunsko-český	rumunsko-český	k2eAgInSc1d1	rumunsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
