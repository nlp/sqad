<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
neboli	neboli	k8xC	neboli
strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
je	být	k5eAaImIp3nS	být
strojníkem	strojník	k1gMnSc7	strojník
a	a	k8xC	a
řidičem	řidič	k1gInSc7	řidič
železničních	železniční	k2eAgNnPc2d1	železniční
hnacích	hnací	k2eAgNnPc2d1	hnací
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
hnacími	hnací	k2eAgNnPc7d1	hnací
vozidly	vozidlo	k1gNnPc7	vozidlo
vedeny	vést	k5eAaImNgInP	vést
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vlaky	vlak	k1gInPc4	vlak
patří	patřit	k5eAaImIp3nS	patřit
podle	podle	k7c2	podle
české	český	k2eAgFnSc2d1	Česká
legislativy	legislativa	k1gFnSc2	legislativa
i	i	k8xC	i
vlaky	vlak	k1gInPc1	vlak
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
-	-	kIx~	-
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
řídí	řídit	k5eAaImIp3nS	řídit
vlaky	vlak	k1gInPc4	vlak
ze	z	k7c2	z
stanoviště	stanoviště	k1gNnSc2	stanoviště
v	v	k7c6	v
hnacím	hnací	k2eAgNnSc6d1	hnací
vozidle	vozidlo	k1gNnSc6	vozidlo
(	(	kIx(	(
<g/>
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
<g/>
,	,	kIx,	,
motorový	motorový	k2eAgInSc1d1	motorový
vůz	vůz	k1gInSc1	vůz
nebo	nebo	k8xC	nebo
speciální	speciální	k2eAgNnSc1d1	speciální
hnací	hnací	k2eAgNnSc1d1	hnací
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
řídicím	řídicí	k2eAgInSc6d1	řídicí
voze	vůz	k1gInSc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
legislativě	legislativa	k1gFnSc6	legislativa
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
výraz	výraz	k1gInSc1	výraz
strojvůdce	strojvůdce	k1gMnSc2	strojvůdce
nahrazen	nahradit	k5eAaPmNgInS	nahradit
slovem	slovo	k1gNnSc7	slovo
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jazykového	jazykový	k2eAgNnSc2d1	jazykové
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
výrazy	výraz	k1gInPc1	výraz
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
a	a	k8xC	a
srovnatelně	srovnatelně	k6eAd1	srovnatelně
užívané	užívaný	k2eAgNnSc1d1	užívané
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc4d1	původní
tvar	tvar	k1gInSc4	tvar
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
názvech	název	k1gInPc6	název
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Federace	federace	k1gFnSc2	federace
strojvůdců	strojvůdce	k1gMnPc2	strojvůdce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Cech	cech	k1gInSc1	cech
strojvůdců	strojvůdce	k1gMnPc2	strojvůdce
<g/>
,	,	kIx,	,
Strojvůdci	strojvůdce	k1gMnSc3	strojvůdce
CZ	CZ	kA	CZ
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
publicistice	publicistika	k1gFnSc6	publicistika
a	a	k8xC	a
zpravodajství	zpravodajství	k1gNnSc6	zpravodajství
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
výraz	výraz	k1gInSc4	výraz
strojvůdce	strojvůdce	k1gMnSc2	strojvůdce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
jedinečnosti	jedinečnost	k1gFnSc2	jedinečnost
vůdce	vůdce	k1gMnSc2	vůdce
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
obourodé	obourodý	k2eAgNnSc4d1	obourodý
slovo	slovo	k1gNnSc4	slovo
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
u	u	k7c2	u
novějšího	nový	k2eAgInSc2d2	novější
tvaru	tvar	k1gInSc2	tvar
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
řídce	řídce	k6eAd1	řídce
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
strojníky	strojník	k1gMnPc4	strojník
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
lepenkového	lepenkový	k2eAgInSc2d1	lepenkový
stroje	stroj	k1gInSc2	stroj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
strojvedoucí	strojvedoucí	k1gMnPc4	strojvedoucí
jsou	být	k5eAaImIp3nP	být
kladeny	klást	k5eAaImNgFnP	klást
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
a	a	k8xC	a
kvalifikační	kvalifikační	k2eAgInPc1d1	kvalifikační
požadavky	požadavek	k1gInPc1	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
uchazeč	uchazeč	k1gMnSc1	uchazeč
musí	muset	k5eAaImIp3nS	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
náročná	náročný	k2eAgFnSc1d1	náročná
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
vyšetření	vyšetření	k1gNnSc4	vyšetření
-	-	kIx~	-
psychologické	psychologický	k2eAgNnSc4d1	psychologické
<g/>
,	,	kIx,	,
neurologické	neurologický	k2eAgNnSc4d1	neurologický
<g/>
,	,	kIx,	,
psychotesty	psychotest	k1gInPc4	psychotest
<g/>
,	,	kIx,	,
oční	oční	k2eAgInPc4d1	oční
perimetry	perimetr	k1gInPc4	perimetr
<g/>
,	,	kIx,	,
barvocit	barvocit	k1gInSc4	barvocit
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několikaměsíční	několikaměsíční	k2eAgFnSc6d1	několikaměsíční
praxi	praxe	k1gFnSc6	praxe
v	v	k7c6	v
dílnách	dílna	k1gFnPc6	dílna
na	na	k7c6	na
opravě	oprava	k1gFnSc6	oprava
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
mají	mít	k5eAaImIp3nP	mít
nejméně	málo	k6eAd3	málo
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
zácvik	zácvik	k1gInSc4	zácvik
na	na	k7c6	na
hnacím	hnací	k2eAgNnSc6d1	hnací
vozidle	vozidlo	k1gNnSc6	vozidlo
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
kolegy	kolega	k1gMnSc2	kolega
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
musí	muset	k5eAaImIp3nS	muset
úspěšně	úspěšně	k6eAd1	úspěšně
absolvovat	absolvovat	k5eAaPmF	absolvovat
náročné	náročný	k2eAgFnPc4d1	náročná
zkoušky	zkouška	k1gFnPc4	zkouška
u	u	k7c2	u
drážního	drážní	k2eAgInSc2d1	drážní
úřadu	úřad	k1gInSc2	úřad
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
z	z	k7c2	z
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
elektra	elektrum	k1gNnSc2	elektrum
<g/>
,	,	kIx,	,
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
praktické	praktický	k2eAgInPc4d1	praktický
aj.	aj.	kA	aj.
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
licence	licence	k1gFnSc2	licence
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
<g/>
,	,	kIx,	,
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
psychotestech	psychotest	k1gInPc6	psychotest
<g/>
,	,	kIx,	,
zkoušky	zkouška	k1gFnPc1	zkouška
drážního	drážní	k2eAgInSc2d1	drážní
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
místně	místně	k6eAd1	místně
příslušného	příslušný	k2eAgNnSc2d1	příslušné
depa	depo	k1gNnSc2	depo
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
dalších	další	k2eAgFnPc2d1	další
potřebných	potřebný	k2eAgFnPc2d1	potřebná
řad	řada	k1gFnPc2	řada
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zkoušky	zkouška	k1gFnPc1	zkouška
jsou	být	k5eAaImIp3nP	být
dopravní	dopravní	k2eAgInPc1d1	dopravní
z	z	k7c2	z
platných	platný	k2eAgInPc2d1	platný
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
seznání	seznání	k1gNnSc1	seznání
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
místní	místní	k2eAgInPc4d1	místní
výkony	výkon	k1gInPc4	výkon
na	na	k7c6	na
posunu	posun	k1gInSc6	posun
nejméně	málo	k6eAd3	málo
další	další	k2eAgInSc4d1	další
půlrok	půlrok	k1gInSc4	půlrok
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
vlaku	vlak	k1gInSc2	vlak
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
trati	trať	k1gFnSc6	trať
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
měl	mít	k5eAaImAgMnS	mít
zkoušky	zkouška	k1gFnPc4	zkouška
z	z	k7c2	z
ovládání	ovládání	k1gNnSc2	ovládání
vozidla	vozidlo	k1gNnSc2	vozidlo
dané	daný	k2eAgFnSc2d1	daná
trakce	trakce	k1gFnSc2	trakce
i	i	k8xC	i
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
zkoušky	zkouška	k1gFnPc1	zkouška
z	z	k7c2	z
návěstních	návěstní	k2eAgInPc2d1	návěstní
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
seznání	seznání	k1gNnSc1	seznání
(	(	kIx(	(
<g/>
znalost	znalost	k1gFnSc1	znalost
<g/>
)	)	kIx)	)
dané	daný	k2eAgFnSc2d1	daná
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
platné	platný	k2eAgNnSc1d1	platné
zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
vyšetření	vyšetření	k1gNnSc1	vyšetření
včetně	včetně	k7c2	včetně
psychotestů	psychotest	k1gInPc2	psychotest
<g/>
.	.	kIx.	.
</s>
<s>
Donedávna	donedávna	k6eAd1	donedávna
se	se	k3xPyFc4	se
například	například	k6eAd1	například
nemohl	moct	k5eNaImAgMnS	moct
stát	stát	k5eAaImF	stát
strojvedoucím	strojvedoucí	k1gMnPc3	strojvedoucí
nikdo	nikdo	k3yNnSc1	nikdo
s	s	k7c7	s
vadou	vada	k1gFnSc7	vada
zraku	zrak	k1gInSc2	zrak
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nutnosti	nutnost	k1gFnSc2	nutnost
nosit	nosit	k5eAaImF	nosit
brýle	brýle	k1gFnPc4	brýle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
předpis	předpis	k1gInSc1	předpis
připouští	připouštět	k5eAaImIp3nS	připouštět
korekci	korekce	k1gFnSc3	korekce
brýlemi	brýle	k1gFnPc7	brýle
do	do	k7c2	do
2	[number]	k4	2
dioptrií	dioptrie	k1gFnPc2	dioptrie
(	(	kIx(	(
<g/>
korekce	korekce	k1gFnSc1	korekce
kontaktními	kontaktní	k2eAgFnPc7d1	kontaktní
čočkami	čočka	k1gFnPc7	čočka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
a	a	k8xC	a
také	také	k9	také
platné	platný	k2eAgNnSc1d1	platné
seznání	seznání	k1gNnSc1	seznání
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
<g/>
kdy	kdy	k6eAd1	kdy
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
druhou	druhý	k4xOgFnSc4	druhý
podmínku	podmínka	k1gFnSc4	podmínka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
nemá	mít	k5eNaImIp3nS	mít
platné	platný	k2eAgNnSc1d1	platné
seznání	seznání	k1gNnSc1	seznání
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
vlaku	vlak	k1gInSc2	vlak
odklonem	odklon	k1gInSc7	odklon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
na	na	k7c6	na
řídícím	řídící	k2eAgNnSc6d1	řídící
stanovišti	stanoviště	k1gNnSc6	stanoviště
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
je	být	k5eAaImIp3nS	být
ovládána	ovládán	k2eAgFnSc1d1	ovládána
jízda	jízda	k1gFnSc1	jízda
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
druhý	druhý	k4xOgMnSc1	druhý
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
platné	platný	k2eAgNnSc4d1	platné
seznání	seznání	k1gNnSc4	seznání
daného	daný	k2eAgInSc2d1	daný
úseku	úsek	k1gInSc2	úsek
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Seznání	seznání	k1gNnSc1	seznání
trati	trať	k1gFnSc2	trať
má	mít	k5eAaImIp3nS	mít
časově	časově	k6eAd1	časově
omezenou	omezený	k2eAgFnSc4d1	omezená
platnost	platnost	k1gFnSc4	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
úseku	úsek	k1gInSc6	úsek
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
nejel	jet	k5eNaImAgMnS	jet
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
vykonávat	vykonávat	k5eAaImF	vykonávat
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
seznání	seznání	k1gNnSc4	seznání
tohoto	tento	k3xDgInSc2	tento
úseku	úsek	k1gInSc2	úsek
obnovit	obnovit	k5eAaPmF	obnovit
několikerým	několikerý	k4xRyIgNnSc7	několikerý
projetím	projetí	k1gNnSc7	projetí
na	na	k7c6	na
řídícím	řídící	k2eAgNnSc6d1	řídící
stanovišti	stanoviště	k1gNnSc6	stanoviště
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
oprávnění	oprávnění	k1gNnSc4	oprávnění
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
dané	daný	k2eAgFnSc2d1	daná
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
ji	on	k3xPp3gFnSc4	on
řídit	řídit	k5eAaImF	řídit
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
vlaku	vlak	k1gInSc6	vlak
právě	právě	k6eAd1	právě
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c6	na
vlaku	vlak	k1gInSc6	vlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
závislé	závislý	k2eAgFnSc2d1	závislá
elektrické	elektrický	k2eAgFnSc2d1	elektrická
trakce	trakce	k1gFnSc2	trakce
<g/>
,	,	kIx,	,
jede	jet	k5eAaImIp3nS	jet
pilot	pilot	k1gMnSc1	pilot
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
kolejích	kolej	k1gFnPc6	kolej
tento	tento	k3xDgMnSc1	tento
druhý	druhý	k4xOgMnSc1	druhý
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
nazýván	nazývat	k5eAaImNgMnS	nazývat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
i	i	k9	i
oprávnění	oprávnění	k1gNnSc4	oprávnění
řídit	řídit	k5eAaImF	řídit
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
závislé	závislý	k2eAgFnSc2d1	závislá
trakce	trakce	k1gFnSc2	trakce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
seznání	seznání	k1gNnSc3	seznání
elektrifikované	elektrifikovaný	k2eAgFnSc2d1	elektrifikovaná
trati	trať	k1gFnSc2	trať
nutně	nutně	k6eAd1	nutně
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
znalost	znalost	k1gFnSc1	znalost
provozních	provozní	k2eAgFnPc2d1	provozní
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
poměrů	poměr	k1gInPc2	poměr
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
napájecího	napájecí	k2eAgNnSc2d1	napájecí
trakčního	trakční	k2eAgNnSc2d1	trakční
vedení	vedení	k1gNnSc2	vedení
(	(	kIx(	(
<g/>
troleje	trolej	k1gFnSc2	trolej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pilot	pilot	k1gMnSc1	pilot
vždy	vždy	k6eAd1	vždy
plně	plně	k6eAd1	plně
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
za	za	k7c4	za
vedení	vedení	k1gNnSc4	vedení
vlaku	vlak	k1gInSc2	vlak
po	po	k7c6	po
dopravní	dopravní	k2eAgFnSc6d1	dopravní
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vlak	vlak	k1gInSc4	vlak
řídí	řídit	k5eAaImIp3nS	řídit
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
jen	jen	k9	jen
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
řídit	řídit	k5eAaImF	řídit
daný	daný	k2eAgInSc4d1	daný
typ	typ	k1gInSc4	typ
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vlak	vlak	k1gInSc1	vlak
řídí	řídit	k5eAaImIp3nS	řídit
prvý	prvý	k4xOgMnSc1	prvý
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
plnit	plnit	k5eAaImF	plnit
pokyny	pokyn	k1gInPc4	pokyn
pilota	pilot	k1gMnSc2	pilot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
chyby	chyba	k1gFnPc4	chyba
při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
projetí	projetí	k1gNnSc1	projetí
vlaku	vlak	k1gInSc2	vlak
bez	bez	k7c2	bez
zastavení	zastavení	k1gNnPc2	zastavení
okolo	okolo	k7c2	okolo
návěstidla	návěstidlo	k1gNnSc2	návěstidlo
s	s	k7c7	s
návěstí	návěst	k1gFnSc7	návěst
StůJ	stát	k5eAaImRp2nS	stát
<g/>
,	,	kIx,	,
zodpovídá	zodpovídat	k5eAaPmIp3nS	zodpovídat
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
pilot	pilot	k1gMnSc1	pilot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgMnSc1	druhý
strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
<g/>
,	,	kIx,	,
znalý	znalý	k2eAgMnSc1d1	znalý
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Lotse	Lotse	k1gFnPc4	Lotse
-	-	kIx~	-
lodivod	lodivod	k1gMnSc1	lodivod
<g/>
.	.	kIx.	.
</s>
<s>
Podrobné	podrobný	k2eAgInPc4d1	podrobný
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
odbornou	odborný	k2eAgFnSc4d1	odborná
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
způsobilost	způsobilost	k1gFnSc4	způsobilost
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
strojvedoucích	strojvedoucí	k1gMnPc2	strojvedoucí
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
Řádu	řád	k1gInSc6	řád
pro	pro	k7c4	pro
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
a	a	k8xC	a
odbornou	odborný	k2eAgFnSc4d1	odborná
způsobilost	způsobilost	k1gFnSc4	způsobilost
osob	osoba	k1gFnPc2	osoba
při	při	k7c6	při
provozování	provozování	k1gNnSc6	provozování
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
obsahem	obsah	k1gInSc7	obsah
vyhlášky	vyhláška	k1gFnSc2	vyhláška
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
č.	č.	k?	č.
101	[number]	k4	101
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
platném	platný	k2eAgNnSc6d1	platné
znění	znění	k1gNnSc6	znění
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
parních	parní	k2eAgFnPc2d1	parní
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
kromě	kromě	k7c2	kromě
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
na	na	k7c6	na
řídícím	řídící	k2eAgNnSc6d1	řídící
stanovišti	stanoviště	k1gNnSc6	stanoviště
přítomen	přítomen	k2eAgMnSc1d1	přítomen
také	také	k9	také
pomocník	pomocník	k1gMnSc1	pomocník
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
oheň	oheň	k1gInSc4	oheň
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
a	a	k8xC	a
napájení	napájení	k1gNnSc6	napájení
kotle	kotel	k1gInSc2	kotel
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
<g/>
Zpravidla	zpravidla	k6eAd1	zpravidla
býval	bývat	k5eAaImAgInS	bývat
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohlo	moct	k5eAaImAgNnS	moct
jich	on	k3xPp3gInPc2	on
být	být	k5eAaImF	být
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
těžkých	těžký	k2eAgInPc2d1	těžký
vlaků	vlak	k1gInPc2	vlak
vedených	vedený	k2eAgInPc2d1	vedený
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
parní	parní	k2eAgFnSc7d1	parní
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
<g/>
.	.	kIx.	.
<g/>
Topiči	topič	k1gMnPc1	topič
sloužili	sloužit	k5eAaImAgMnP	sloužit
ve	v	k7c4	v
dvojici	dvojice	k1gFnSc4	dvojice
např.	např.	kA	např.
na	na	k7c6	na
lokomotivách	lokomotiva	k1gFnPc6	lokomotiva
řady	řada	k1gFnSc2	řada
486.0	[number]	k4	486.0
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
nebyly	být	k5eNaImAgInP	být
tehdy	tehdy	k6eAd1	tehdy
vybaveny	vybavit	k5eAaPmNgInP	vybavit
mechanickým	mechanický	k2eAgInSc7d1	mechanický
přikladačem	přikladač	k1gInSc7	přikladač
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
ruční	ruční	k2eAgFnSc1d1	ruční
obsluha	obsluha	k1gFnSc1	obsluha
topeniště	topeniště	k1gNnSc2	topeniště
byla	být	k5eAaImAgFnS	být
fyzicky	fyzicky	k6eAd1	fyzicky
velmi	velmi	k6eAd1	velmi
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
vozbě	vozba	k1gFnSc6	vozba
těžkých	těžký	k2eAgInPc2d1	těžký
rychlíků	rychlík	k1gInPc2	rychlík
na	na	k7c6	na
hlavních	hlavní	k2eAgFnPc6d1	hlavní
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
projížděly	projíždět	k5eAaImAgFnP	projíždět
bez	bez	k7c2	bez
zastavení	zastavení	k1gNnPc2	zastavení
a	a	k8xC	a
plným	plný	k2eAgInSc7d1	plný
výkonem	výkon	k1gInSc7	výkon
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
traťové	traťový	k2eAgFnPc1d1	traťová
úseky	úsek	k1gInPc4	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc4	ten
nejvýkonnější	výkonný	k2eAgFnPc1d3	nejvýkonnější
parní	parní	k2eAgFnPc1d1	parní
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
našich	náš	k3xOp1gFnPc2	náš
drah	draha	k1gFnPc2	draha
a	a	k8xC	a
výkonem	výkon	k1gInSc7	výkon
je	být	k5eAaImIp3nS	být
překonaly	překonat	k5eAaPmAgFnP	překonat
až	až	k9	až
novější	nový	k2eAgInPc4d2	novější
stroje	stroj	k1gInPc4	stroj
řady	řada	k1gFnSc2	řada
498.0	[number]	k4	498.0
a	a	k8xC	a
498.1	[number]	k4	498.1
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
známé	známý	k2eAgFnSc2d1	známá
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
čekávaly	čekávat	k5eAaImAgFnP	čekávat
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
nádražích	nádraží	k1gNnPc6	nádraží
čety	četa	k1gFnSc2	četa
s	s	k7c7	s
lopatami	lopata	k1gFnPc7	lopata
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
vybraného	vybraný	k2eAgInSc2d1	vybraný
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
vylezly	vylézt	k5eAaPmAgInP	vylézt
na	na	k7c4	na
tendr	tendr	k1gInSc4	tendr
a	a	k8xC	a
nahrnuly	nahrnout	k5eAaPmAgFnP	nahrnout
uhlí	uhlí	k1gNnSc4	uhlí
ze	z	k7c2	z
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
tendru	tendr	k1gInSc2	tendr
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
topič	topič	k1gInSc1	topič
mohl	moct	k5eAaImAgInS	moct
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
vlaku	vlak	k1gInSc2	vlak
snadněji	snadno	k6eAd2	snadno
přikládat	přikládat	k5eAaImF	přikládat
uhlí	uhlí	k1gNnSc4	uhlí
do	do	k7c2	do
topeniště	topeniště	k1gNnSc2	topeniště
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
sestoupily	sestoupit	k5eAaPmAgFnP	sestoupit
a	a	k8xC	a
čekaly	čekat	k5eAaImAgFnP	čekat
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vlak	vlak	k1gInSc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
topiče	topič	k1gInSc2	topič
při	při	k7c6	při
obsluze	obsluha	k1gFnSc6	obsluha
topeniště	topeniště	k1gNnSc2	topeniště
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
unadněna	unadnit	k5eAaPmNgFnS	unadnit
mechanickým	mechanický	k2eAgInSc7d1	mechanický
(	(	kIx(	(
<g/>
šnekovým	šnekový	k2eAgInSc7d1	šnekový
<g/>
)	)	kIx)	)
přikladačem	přikladač	k1gInSc7	přikladač
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dopravil	dopravit	k5eAaPmAgMnS	dopravit
palivo	palivo	k1gNnSc4	palivo
z	z	k7c2	z
tendru	tendr	k1gInSc2	tendr
do	do	k7c2	do
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
motorové	motorový	k2eAgFnPc1d1	motorová
a	a	k8xC	a
elektrické	elektrický	k2eAgFnPc1d1	elektrická
trakce	trakce	k1gFnPc1	trakce
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
u	u	k7c2	u
ČSD	ČSD	kA	ČSD
(	(	kIx(	(
Československých	československý	k2eAgFnPc2d1	Československá
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
obsazovány	obsazovat	k5eAaImNgFnP	obsazovat
strojvedoucím	strojvedoucí	k1gMnSc7	strojvedoucí
a	a	k8xC	a
pomocníkem	pomocník	k1gMnSc7	pomocník
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
(	(	kIx(	(
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
i	i	k9	i
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
mechanik	mechanika	k1gFnPc2	mechanika
<g/>
"	"	kIx"	"
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
zaváděna	zavádět	k5eAaImNgFnS	zavádět
tzv.	tzv.	kA	tzv.
jednomužka	jednomužka	k1gFnSc1	jednomužka
-	-	kIx~	-
jednomužná	jednomužný	k2eAgFnSc1d1	jednomužná
obsluha	obsluha	k1gFnSc1	obsluha
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
,	,	kIx,	,
strojvedoucí	strojvedoucí	k1gMnPc4	strojvedoucí
pak	pak	k6eAd1	pak
sloužil	sloužit	k5eAaImAgInS	sloužit
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
mechanika	mechanik	k1gMnSc2	mechanik
a	a	k8xC	a
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
liniovým	liniový	k2eAgInSc7d1	liniový
vlakovým	vlakový	k2eAgInSc7d1	vlakový
zabezpečovačem	zabezpečovač	k1gInSc7	zabezpečovač
LVZ	LVZ	kA	LVZ
-	-	kIx~	-
hovorově	hovorově	k6eAd1	hovorově
zvaným	zvaný	k2eAgInSc7d1	zvaný
"	"	kIx"	"
<g/>
elvézetka	elvézetka	k1gFnSc1	elvézetka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vézetka	vézetka	k1gFnSc1	vézetka
<g/>
"	"	kIx"	"
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
živák	živák	k1gInSc1	živák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Strojvedoucí	strojvedoucí	k1gMnPc1	strojvedoucí
v	v	k7c6	v
periodickém	periodický	k2eAgInSc6d1	periodický
intervalu	interval	k1gInSc6	interval
obsluhoval	obsluhovat	k5eAaImAgInS	obsluhovat
tlačítko	tlačítko	k1gNnSc4	tlačítko
bdělosti	bdělost	k1gFnSc2	bdělost
(	(	kIx(	(
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
živák	živák	k1gInSc1	živák
<g/>
"	"	kIx"	"
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
tlačítko	tlačítko	k1gNnSc4	tlačítko
nestiskl	stisknout	k5eNaPmAgMnS	stisknout
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
intervalu	interval	k1gInSc2	interval
se	se	k3xPyFc4	se
ozval	ozvat	k5eAaPmAgInS	ozvat
výstražný	výstražný	k2eAgInSc1d1	výstražný
tón	tón	k1gInSc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
strojvedoucí	strojvedoucí	k1gMnPc1	strojvedoucí
ani	ani	k8xC	ani
potom	potom	k6eAd1	potom
nereagoval	reagovat	k5eNaBmAgInS	reagovat
stiskem	stisk	k1gInSc7	stisk
tlačítka	tlačítko	k1gNnSc2	tlačítko
bdělosti	bdělost	k1gFnSc2	bdělost
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
brzdění	brzdění	k1gNnSc3	brzdění
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
při	při	k7c6	při
"	"	kIx"	"
<g/>
stržení	stržení	k1gNnSc6	stržení
<g/>
"	"	kIx"	"
rukojeti	rukojeť	k1gFnPc4	rukojeť
záchranné	záchranný	k2eAgFnSc2d1	záchranná
brzdy	brzda	k1gFnSc2	brzda
kdekoliv	kdekoliv	k6eAd1	kdekoliv
ve	v	k7c6	v
vlakové	vlakový	k2eAgFnSc6d1	vlaková
soupravě	souprava	k1gFnSc6	souprava
<g/>
.	.	kIx.	.
(	(	kIx(	(
Už	už	k6eAd1	už
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
před	před	k7c7	před
tím	ten	k3xDgMnSc7	ten
sloužili	sloužit	k5eAaImAgMnP	sloužit
strojvedoucí	strojvedoucí	k1gMnPc1	strojvedoucí
"	"	kIx"	"
<g/>
jednomužně	jednomužně	k6eAd1	jednomužně
<g/>
"	"	kIx"	"
na	na	k7c6	na
motorových	motorový	k2eAgInPc6d1	motorový
vozech	vůz	k1gInPc6	vůz
v	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
strojvedoucím	strojvedoucí	k1gMnPc3	strojvedoucí
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
řídícím	řídící	k2eAgNnSc6d1	řídící
stanovišti	stanoviště	k1gNnSc6	stanoviště
motorového	motorový	k2eAgInSc2d1	motorový
vozu	vůz	k1gInSc2	vůz
i	i	k8xC	i
vlakvedoucí	vlakvedoucí	k1gMnSc1	vlakvedoucí
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
neměl	mít	k5eNaImAgMnS	mít
všechny	všechen	k3xTgFnPc4	všechen
povinnosti	povinnost	k1gFnPc4	povinnost
pomocníka	pomocník	k1gMnSc2	pomocník
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
motorových	motorový	k2eAgInPc6d1	motorový
vozech	vůz	k1gInPc6	vůz
nebylo	být	k5eNaImAgNnS	být
zařízení	zařízení	k1gNnSc4	zařízení
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
automatického	automatický	k2eAgNnSc2d1	automatické
zastavení	zastavení	k1gNnSc2	zastavení
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Vlakvedoucí	vlakvedoucí	k1gFnSc4	vlakvedoucí
byl	být	k5eAaImAgInS	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
nadřízeným	nadřízený	k1gMnPc3	nadřízený
všech	všecek	k3xTgMnPc2	všecek
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
na	na	k7c6	na
vlaku	vlak	k1gInSc6	vlak
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
po	po	k7c6	po
dopravní	dopravní	k2eAgFnSc6d1	dopravní
stránce	stránka	k1gFnSc6	stránka
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
práci	práce	k1gFnSc4	práce
průvodčího	průvodčí	k1gMnSc2	průvodčí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vlak	vlak	k1gInSc1	vlak
veden	vést	k5eAaImNgInS	vést
několika	několik	k4yIc7	několik
lokomotivami	lokomotiva	k1gFnPc7	lokomotiva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
stroji	stroj	k1gInSc6	stroj
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
ovládány	ovládat	k5eAaImNgFnP	ovládat
ze	z	k7c2	z
stanoviště	stanoviště	k1gNnSc2	stanoviště
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mnohočlenného	mnohočlenný	k2eAgNnSc2d1	mnohočlenné
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obsluhu	obsluha	k1gFnSc4	obsluha
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
ji	on	k3xPp3gFnSc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
termín	termín	k1gInSc1	termín
lokomotivní	lokomotivní	k2eAgFnSc1d1	lokomotivní
četa	četa	k1gFnSc1	četa
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
tramvaje	tramvaj	k1gFnSc2	tramvaj
Řidič	řidič	k1gMnSc1	řidič
trolejbusu	trolejbus	k1gInSc2	trolejbus
Strojník	strojník	k1gMnSc1	strojník
lanové	lanový	k2eAgFnPc4d1	lanová
dráhy	dráha	k1gFnPc4	dráha
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Strojvedoucí	strojvedoucí	k1gMnPc4	strojvedoucí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Průvodce	průvodce	k1gMnSc1	průvodce
světem	svět	k1gInSc7	svět
povolání	povolání	k1gNnSc2	povolání
<g/>
:	:	kIx,	:
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Hlavsová	Hlavsová	k1gFnSc1	Hlavsová
<g/>
:	:	kIx,	:
Fýrer	Fýrer	k?	Fýrer
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
,	,	kIx,	,
strojvedoucí	strojvedoucí	k1gMnPc4	strojvedoucí
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
<g/>
,	,	kIx,	,
Rozmlouvání	rozmlouvání	k1gNnSc1	rozmlouvání
o	o	k7c6	o
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Týdeník	týdeník	k1gInSc1	týdeník
Rozhlas	rozhlas	k1gInSc1	rozhlas
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
týdeníku	týdeník	k1gInSc2	týdeník
nezjištěno	zjištěn	k2eNgNnSc1d1	nezjištěno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
na	na	k7c4	na
Slavistik-portal	Slavistikortal	k1gMnPc4	Slavistik-portal
<g/>
,	,	kIx,	,
Bibliographie	Bibliographius	k1gMnPc4	Bibliographius
der	drát	k5eAaImRp2nS	drát
tschechischen	tschechischna	k1gFnPc2	tschechischna
Linguistik	Linguistika	k1gFnPc2	Linguistika
(	(	kIx(	(
<g/>
BibCzechLing	BibCzechLing	k1gInSc1	BibCzechLing
<g/>
)	)	kIx)	)
</s>
