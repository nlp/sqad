<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
neboli	neboli	k8xC	neboli
strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
je	být	k5eAaImIp3nS	být
strojníkem	strojník	k1gMnSc7	strojník
a	a	k8xC	a
řidičem	řidič	k1gInSc7	řidič
železničních	železniční	k2eAgNnPc2d1	železniční
hnacích	hnací	k2eAgNnPc2d1	hnací
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
hnacími	hnací	k2eAgNnPc7d1	hnací
vozidly	vozidlo	k1gNnPc7	vozidlo
vedeny	vést	k5eAaImNgInP	vést
<g/>
.	.	kIx.	.
</s>
