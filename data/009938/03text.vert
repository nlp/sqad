<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1963	[number]	k4	1963
Poprad	Poprad	k1gInSc1	Poprad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
filantrop	filantrop	k1gMnSc1	filantrop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
byl	být	k5eAaImAgMnS	být
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
prezidentem	prezident	k1gMnSc7	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
porazil	porazit	k5eAaPmAgMnS	porazit
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
přímé	přímý	k2eAgFnSc2d1	přímá
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
Roberta	Robert	k1gMnSc2	Robert
Fica	Fico	k1gMnSc2	Fico
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
životní	životní	k2eAgFnSc6d1	životní
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
úspěchu	úspěch	k1gInSc3	úspěch
a	a	k8xC	a
štěstí	štěstí	k1gNnSc4	štěstí
napsal	napsat	k5eAaBmAgMnS	napsat
dvě	dva	k4xCgFnPc4	dva
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
schopnostech	schopnost	k1gFnPc6	schopnost
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
též	též	k9	též
přednášky	přednáška	k1gFnPc4	přednáška
středoškolákům	středoškolák	k1gMnPc3	středoškolák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Elektrotechnické	elektrotechnický	k2eAgFnSc6d1	elektrotechnická
fakultě	fakulta	k1gFnSc6	fakulta
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
technické	technický	k2eAgFnSc2d1	technická
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
projektant	projektant	k1gMnSc1	projektant
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
a	a	k8xC	a
půl	půl	k1xP	půl
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
přivezl	přivézt	k5eAaPmAgInS	přivézt
chuť	chuť	k1gFnSc4	chuť
podnikat	podnikat	k5eAaImF	podnikat
a	a	k8xC	a
odvahu	odvaha	k1gFnSc4	odvaha
vydat	vydat	k5eAaPmF	vydat
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
spoluzakládal	spoluzakládat	k5eAaImAgMnS	spoluzakládat
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
splátkovou	splátkový	k2eAgFnSc4d1	splátková
společnost	společnost	k1gFnSc4	společnost
Tatracredit	Tatracredit	k1gFnSc2	Tatracredit
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
krátce	krátce	k6eAd1	krátce
i	i	k9	i
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
založil	založit	k5eAaPmAgMnS	založit
splátkovou	splátkový	k2eAgFnSc4d1	splátková
společnost	společnost	k1gFnSc4	společnost
Quatro	Quatro	k1gNnSc1	Quatro
<g/>
.	.	kIx.	.
</s>
<s>
Podnikáním	podnikání	k1gNnSc7	podnikání
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
svých	svůj	k3xOyFgFnPc2	svůj
společností	společnost	k1gFnPc2	společnost
vydělal	vydělat	k5eAaPmAgInS	vydělat
stovky	stovka	k1gFnPc4	stovka
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
založil	založit	k5eAaPmAgMnS	založit
neziskovou	ziskový	k2eNgFnSc4d1	nezisková
organizaci	organizace	k1gFnSc4	organizace
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
Anjel	anjel	k1gMnSc1	anjel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
rodinám	rodina	k1gFnPc3	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
životních	životní	k2eAgFnPc6d1	životní
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
kandidaturou	kandidatura	k1gFnSc7	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
<g/>
,	,	kIx,	,
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
se	se	k3xPyFc4	se
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Martina	Martina	k1gFnSc1	Martina
Kisková	Kisková	k1gFnSc1	Kisková
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
české	český	k2eAgFnSc2d1	Česká
nadace	nadace	k1gFnSc2	nadace
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
anděl	anděl	k1gMnSc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
v	v	k7c4	v
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
16	[number]	k4	16
minut	minuta	k1gFnPc2	minuta
převzal	převzít	k5eAaPmAgInS	převzít
Velkou	velký	k2eAgFnSc4d1	velká
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
ocenění	ocenění	k1gNnSc1	ocenění
této	tento	k3xDgFnSc2	tento
instituce	instituce	k1gFnSc2	instituce
<g/>
,	,	kIx,	,
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
rektora	rektor	k1gMnSc2	rektor
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Beka	bek	k1gMnSc2	bek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kandidatura	kandidatura	k1gFnSc1	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
první	první	k4xOgFnSc7	první
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
24	[number]	k4	24
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
se	se	k3xPyFc4	se
tak	tak	k9	tak
druhý	druhý	k4xOgInSc1	druhý
za	za	k7c7	za
Robertem	Robert	k1gMnSc7	Robert
Ficem	Fic	k1gMnSc7	Fic
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
tak	tak	k6eAd1	tak
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nad	nad	k7c7	nad
Ficem	Fic	k1gMnSc7	Fic
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
59	[number]	k4	59
%	%	kIx~	%
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
4	[number]	k4	4
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
postu	post	k1gInSc6	post
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Ivana	Ivan	k1gMnSc4	Ivan
Gašparoviče	Gašparovič	k1gMnSc4	Gašparovič
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
mandát	mandát	k1gInSc1	mandát
skončil	skončit	k5eAaPmAgInS	skončit
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgInPc1d1	politický
postoje	postoj	k1gInPc1	postoj
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
Kiska	Kiska	k1gMnSc1	Kiska
podpořil	podpořit	k5eAaPmAgMnS	podpořit
uprchlické	uprchlický	k2eAgFnPc4d1	uprchlická
kvóty	kvóta	k1gFnPc4	kvóta
navržené	navržený	k2eAgFnPc4d1	navržená
Evropskou	evropský	k2eAgFnSc7d1	Evropská
komisí	komise	k1gFnSc7	komise
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Přijetí	přijetí	k1gNnSc1	přijetí
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
nebo	nebo	k8xC	nebo
i	i	k9	i
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
prchajících	prchající	k2eAgMnPc2d1	prchající
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
násilím	násilí	k1gNnSc7	násilí
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
silách	síla	k1gFnPc6	síla
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
by	by	kYmCp3nP	by
o	o	k7c4	o
žádné	žádný	k3yNgInPc4	žádný
ohrožení	ohrožení	k1gNnSc1	ohrožení
Slovenska	Slovensko	k1gNnSc2	Slovensko
ani	ani	k8xC	ani
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
nemělo	mít	k5eNaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
lepší	dobrý	k2eAgInSc4d2	lepší
nebo	nebo	k8xC	nebo
horší	zlý	k2eAgInSc4d2	horší
život	život	k1gInSc4	život
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
slovenských	slovenský	k2eAgMnPc2d1	slovenský
politiků	politik	k1gMnPc2	politik
otevřeně	otevřeně	k6eAd1	otevřeně
podporuje	podporovat	k5eAaImIp3nS	podporovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Kosova	Kosův	k2eAgFnSc1d1	Kosova
na	na	k7c6	na
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezávislost	nezávislost	k1gFnSc1	nezávislost
Kosova	Kosův	k2eAgFnSc1d1	Kosova
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
stabilitě	stabilita	k1gFnSc3	stabilita
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Kisky	Kiska	k1gFnSc2	Kiska
je	být	k5eAaImIp3nS	být
Turecko	Turecko	k1gNnSc1	Turecko
důležitým	důležitý	k2eAgNnPc3d1	důležité
politickým	politický	k2eAgMnPc3d1	politický
<g/>
,	,	kIx,	,
ekonomickým	ekonomický	k2eAgMnPc3d1	ekonomický
a	a	k8xC	a
bezpečnostním	bezpečnostní	k2eAgMnSc7d1	bezpečnostní
partnerem	partner	k1gMnSc7	partner
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Kiska	Kiska	k1gMnSc1	Kiska
podpořil	podpořit	k5eAaPmAgMnS	podpořit
přístupové	přístupový	k2eAgInPc4d1	přístupový
rozhovory	rozhovor	k1gInPc4	rozhovor
Turecka	Turecko	k1gNnSc2	Turecko
s	s	k7c7	s
EU	EU	kA	EU
<g/>
,	,	kIx,	,
ocenil	ocenit	k5eAaPmAgMnS	ocenit
význam	význam	k1gInSc4	význam
Turecka	Turecko	k1gNnSc2	Turecko
jako	jako	k8xC	jako
spojence	spojenec	k1gMnSc2	spojenec
v	v	k7c6	v
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
sú	sú	k?	sú
nielen	nielna	k1gFnPc2	nielna
partneri	partneri	k6eAd1	partneri
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aj	aj	kA	aj
priatelia	priatelia	k1gFnSc1	priatelia
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Kiska	Kiska	k1gMnSc1	Kiska
je	být	k5eAaImIp3nS	být
dlouhodobým	dlouhodobý	k2eAgMnSc7d1	dlouhodobý
kritikem	kritik	k1gMnSc7	kritik
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Rusko	Rusko	k1gNnSc4	Rusko
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
zajaté	zajatý	k2eAgFnSc2d1	zajatá
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
pilotky	pilotka	k1gFnSc2	pilotka
Nadi	Naďa	k1gFnSc2	Naďa
Savčenkové	Savčenkový	k2eAgFnSc2d1	Savčenková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
Rusko	Rusko	k1gNnSc4	Rusko
obvinil	obvinit	k5eAaPmAgMnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
rozbít	rozbít	k5eAaPmF	rozbít
jednotu	jednota	k1gFnSc4	jednota
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
"	"	kIx"	"
<g/>
naší	náš	k3xOp1gFnSc2	náš
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
naší	náš	k3xOp1gFnSc2	náš
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
mandátu	mandát	k1gInSc2	mandát
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
se	se	k3xPyFc4	se
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nekandidovat	kandidovat	k5eNaImF	kandidovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
založení	založení	k1gNnSc4	založení
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
Za	za	k7c4	za
ľudí	ľudí	k1gFnSc4	ľudí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
mandát	mandát	k1gInSc1	mandát
vypršel	vypršet	k5eAaPmAgInS	vypršet
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gInSc4	on
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Slovenska	Slovensko	k1gNnSc2	Slovensko
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čaputová	Čaputový	k2eAgFnSc1d1	Čaputová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
je	být	k5eAaImIp3nS	být
podruhé	podruhé	k6eAd1	podruhé
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Márií	Márie	k1gFnSc7	Márie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Andreje	Andrej	k1gMnSc4	Andrej
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Natálii	Natálie	k1gFnSc4	Natálie
(	(	kIx(	(
<g/>
*	*	kIx~	*
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
současnou	současný	k2eAgFnSc7d1	současná
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
Martina	Martina	k1gFnSc1	Martina
Kisková	Kisková	k1gFnSc1	Kisková
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Živorová	Živorová	k1gFnSc1	Živorová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Veroniku	Veronika	k1gFnSc4	Veronika
(	(	kIx(	(
<g/>
*	*	kIx~	*
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
syny	syn	k1gMnPc7	syn
Viktora	Viktor	k1gMnSc2	Viktor
(	(	kIx(	(
<g/>
*	*	kIx~	*
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
(	(	kIx(	(
<g/>
*	*	kIx~	*
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Popradu	Poprad	k1gInSc6	Poprad
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
Kiska	Kiska	k1gMnSc1	Kiska
pravidelně	pravidelně	k6eAd1	pravidelně
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kontroverze	kontroverze	k1gFnSc1	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Kiskova	Kiskův	k2eAgFnSc1d1	Kiskův
firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zapletena	zaplést	k5eAaPmNgFnS	zaplést
do	do	k7c2	do
krácení	krácení	k1gNnSc2	krácení
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
a	a	k8xC	a
případ	případ	k1gInSc4	případ
vyšetřovala	vyšetřovat	k5eAaImAgFnS	vyšetřovat
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
projevena	projeven	k2eAgFnSc1d1	projevena
"	"	kIx"	"
<g/>
účinná	účinný	k2eAgFnSc1d1	účinná
lítost	lítost	k1gFnSc1	lítost
<g/>
"	"	kIx"	"
a	a	k8xC	a
daň	daň	k1gFnSc1	daň
byla	být	k5eAaImAgFnS	být
dodatečně	dodatečně	k6eAd1	dodatečně
doplacena	doplacen	k2eAgFnSc1d1	doplacena
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
tureckého	turecký	k2eAgMnSc2d1	turecký
prezidenta	prezident	k1gMnSc2	prezident
Recepa	Recep	k1gMnSc2	Recep
Tayyipa	Tayyip	k1gMnSc2	Tayyip
Erdoğ	Erdoğ	k1gMnSc2	Erdoğ
se	se	k3xPyFc4	se
Kiska	Kiska	k1gMnSc1	Kiska
vyjářil	vyjářit	k5eAaPmAgMnS	vyjářit
o	o	k7c6	o
Turecku	Turecko	k1gNnSc6	Turecko
jako	jako	k8xC	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
ostrove	ostrov	k1gInSc5	ostrov
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aj	aj	kA	aj
štítom	štítom	k1gInSc4	štítom
vôči	vôč	k1gFnSc2	vôč
hrozbám	hrozba	k1gFnPc3	hrozba
pre	pre	k?	pre
našu	naša	k1gFnSc4	naša
bezpečnosť	bezpečnostit	k5eAaPmRp2nS	bezpečnostit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
příjezdem	příjezd	k1gInSc7	příjezd
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
Erdoğ	Erdoğ	k1gFnSc2	Erdoğ
podepsal	podepsat	k5eAaPmAgInS	podepsat
zákon	zákon	k1gInSc1	zákon
povolující	povolující	k2eAgFnSc4d1	povolující
ostrou	ostrý	k2eAgFnSc4d1	ostrá
střelbu	střelba	k1gFnSc4	střelba
do	do	k7c2	do
demonstrantů	demonstrant	k1gMnPc2	demonstrant
<g/>
.	.	kIx.	.
<g/>
Poslanec	poslanec	k1gMnSc1	poslanec
Peter	Peter	k1gMnSc1	Peter
Pčolinský	Pčolinský	k2eAgMnSc1d1	Pčolinský
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Kiskovo	Kiskův	k2eAgNnSc4d1	Kiskův
mlčení	mlčení	k1gNnSc4	mlčení
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
čistkám	čistka	k1gFnPc3	čistka
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
zahájila	zahájit	k5eAaPmAgFnS	zahájit
turecká	turecký	k2eAgFnSc1d1	turecká
vláda	vláda	k1gFnSc1	vláda
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pčolinského	Pčolinský	k2eAgInSc2d1	Pčolinský
"	"	kIx"	"
<g/>
Pred	Preda	k1gFnPc2	Preda
časom	časom	k1gInSc1	časom
prezident	prezident	k1gMnSc1	prezident
Kiska	Kiska	k1gMnSc1	Kiska
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
ostrovom	ostrovom	k1gInSc4	ostrovom
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
,	,	kIx,	,
keď	keď	k?	keď
Erdogan	Erdogan	k1gMnSc1	Erdogan
dal	dát	k5eAaPmAgMnS	dát
pozatýkať	pozatýkatit	k5eAaImRp2nS	pozatýkatit
tisíce	tisíc	k4xCgInPc1	tisíc
nevinných	vinný	k2eNgMnPc2d1	nevinný
ľudí	ľudí	k1gFnSc2	ľudí
<g/>
,	,	kIx,	,
ktorých	ktorý	k2eAgMnPc2d1	ktorý
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
pokuse	pokus	k1gInSc6	pokus
o	o	k7c4	o
prevrat	prevrat	k1gInSc4	prevrat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
manažéra	manažér	k1gMnSc2	manažér
z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Vezmi	vzít	k5eAaPmRp2nS	vzít
život	život	k1gInSc4	život
do	do	k7c2	do
svojich	svojicha	k1gFnPc2	svojicha
rúk	rúk	k?	rúk
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kisek	k1gInSc2	Kisek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
Anjel	anjel	k1gMnSc1	anjel
-	-	kIx~	-
zakladatelé	zakladatel	k1gMnPc1	zakladatel
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
SME	SME	k?	SME
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
-	-	kIx~	-
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
ktorému	ktorý	k2eAgInSc3d1	ktorý
sa	sa	k?	sa
málilo	málit	k5eAaImAgNnS	málit
byť	byť	k8xS	byť
anjelom	anjelom	k1gInSc4	anjelom
<g/>
.	.	kIx.	.
</s>
<s>
Profil	profil	k1gInSc1	profil
Andreja	Andreja	k?	Andreja
Kisku	Kisk	k1gInSc2	Kisk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Kiska	Kiska	k1gFnSc1	Kiska
<g/>
:	:	kIx,	:
Dobré	dobrý	k2eAgInPc1d1	dobrý
nápady	nápad	k1gInPc1	nápad
a	a	k8xC	a
pokojnejšiu	pokojnejšius	k1gMnSc3	pokojnejšius
myseľ	myseľ	k?	myseľ
</s>
</p>
