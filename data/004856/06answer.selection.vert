<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Isao	Isao	k6eAd1	Isao
Takahata	Takahe	k1gNnPc4	Takahe
graduoval	graduovat	k5eAaBmAgMnS	graduovat
na	na	k7c6	na
Tokijské	tokijský	k2eAgFnSc6d1	Tokijská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
nově	nově	k6eAd1	nově
vytvořeného	vytvořený	k2eAgNnSc2d1	vytvořené
animátorského	animátorský	k2eAgNnSc2d1	animátorské
studia	studio	k1gNnSc2	studio
Tóei	Tóe	k1gFnSc2	Tóe
dóga	dóg	k1gInSc2	dóg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
dlouhodobým	dlouhodobý	k2eAgMnSc7d1	dlouhodobý
kolegou	kolega	k1gMnSc7	kolega
Mijazakim	Mijazaki	k1gNnSc7	Mijazaki
a	a	k8xC	a
kde	kde	k6eAd1	kde
také	také	k9	také
režíroval	režírovat	k5eAaImAgMnS	režírovat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
Taijó	Taijó	k1gFnSc2	Taijó
no	no	k9	no
ódži	ódži	k6eAd1	ódži
<g/>
:	:	kIx,	:
Horus	Horus	k1gInSc1	Horus
no	no	k9	no
daibóken	daibóken	k1gInSc1	daibóken
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Horus	Horus	k1gMnSc1	Horus
<g/>
:	:	kIx,	:
Princ	princ	k1gMnSc1	princ
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
