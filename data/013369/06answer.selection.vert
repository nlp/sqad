<s>
Jak	jak	k6eAd1	jak
dosvědčil	dosvědčit	k5eAaPmAgMnS	dosvědčit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
pravým	pravý	k2eAgMnSc7d1	pravý
autorem	autor	k1gMnSc7	autor
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
robot	robot	k1gInSc1	robot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
používaných	používaný	k2eAgInPc2d1	používaný
výrazů	výraz	k1gInPc2	výraz
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
moderních	moderní	k2eAgFnPc2d1	moderní
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
