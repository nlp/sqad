<s>
Tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
intestinum	intestinum	k1gNnSc1	intestinum
crassum	crassum	k1gNnSc1	crassum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
konečný	konečný	k2eAgInSc1d1	konečný
úsek	úsek	k1gInSc1	úsek
trávicí	trávicí	k2eAgFnSc2d1	trávicí
trubice	trubice	k1gFnSc2	trubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
konečné	konečný	k2eAgNnSc4d1	konečné
zahušťování	zahušťování	k1gNnSc4	zahušťování
a	a	k8xC	a
vyměšování	vyměšování	k1gNnSc4	vyměšování
potravy	potrava	k1gFnSc2	potrava
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
se	se	k3xPyFc4	se
vstřebávají	vstřebávat	k5eAaImIp3nP	vstřebávat
zbylé	zbylý	k2eAgInPc4d1	zbylý
vitamíny	vitamín	k1gInPc4	vitamín
a	a	k8xC	a
minerály	minerál	k1gInPc4	minerál
a	a	k8xC	a
také	také	k9	také
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nestravitelné	stravitelný	k2eNgInPc1d1	nestravitelný
zbytky	zbytek	k1gInPc1	zbytek
jsou	být	k5eAaImIp3nP	být
zahušťovány	zahušťován	k2eAgInPc1d1	zahušťován
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
výkaly	výkal	k1gInPc1	výkal
odchází	odcházet	k5eAaImIp3nS	odcházet
konečníkem	konečník	k1gInSc7	konečník
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
procesu	proces	k1gInSc6	proces
tvorby	tvorba	k1gFnSc2	tvorba
stolice	stolice	k1gFnSc2	stolice
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nS	podílet
zde	zde	k6eAd1	zde
usídlené	usídlený	k2eAgFnSc2d1	usídlená
symbiotické	symbiotický	k2eAgFnSc2d1	symbiotická
bakterie	bakterie	k1gFnSc2	bakterie
–	–	k?	–
tzv.	tzv.	kA	tzv.
střevní	střevní	k2eAgFnSc1d1	střevní
mikroflóra	mikroflóra	k1gFnSc1	mikroflóra
<g/>
.	.	kIx.	.
</s>
<s>
Tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Určit	určit	k5eAaPmF	určit
délku	délka	k1gFnSc4	délka
je	být	k5eAaImIp3nS	být
však	však	k9	však
složité	složitý	k2eAgNnSc1d1	složité
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
aktuálním	aktuální	k2eAgInSc6d1	aktuální
tonu	tonus	k1gInSc6	tonus
tračníků	tračník	k1gInPc2	tračník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
tonus	tonus	k1gInSc1	tonus
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
ústí	ústit	k5eAaImIp3nS	ústit
kyčelník	kyčelník	k1gInSc1	kyčelník
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Tenké	tenký	k2eAgNnSc1d1	tenké
střevo	střevo	k1gNnSc1	střevo
ale	ale	k9	ale
neústí	ústit	k5eNaImIp3nS	ústit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
níže	níže	k1gFnSc2	níže
od	od	k7c2	od
ostium	ostium	k1gNnSc1	ostium
oleocaecale	oleocaecale	k6eAd1	oleocaecale
(	(	kIx(	(
<g/>
vyústění	vyústění	k1gNnSc1	vyústění
kyčelníku	kyčelník	k1gInSc2	kyčelník
do	do	k7c2	do
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgNnSc2	jenž
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
červovitý	červovitý	k2eAgInSc1d1	červovitý
výběžek	výběžek	k1gInSc1	výběžek
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
(	(	kIx(	(
<g/>
apendix	apendix	k1gInSc1	apendix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
imunitní	imunitní	k2eAgInSc4d1	imunitní
orgán	orgán	k1gInSc4	orgán
<g/>
:	:	kIx,	:
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tak	tak	k6eAd1	tak
velké	velký	k2eAgNnSc1d1	velké
nakupení	nakupení	k1gNnSc1	nakupení
lymfatických	lymfatický	k2eAgInPc2d1	lymfatický
uzlíků	uzlík	k1gInPc2	uzlík
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
tonsila	tonsila	k1gFnSc1	tonsila
abdominalis	abdominalis	k1gFnSc1	abdominalis
(	(	kIx(	(
<g/>
břišní	břišní	k2eAgFnSc2d1	břišní
mandle	mandle	k1gFnSc2	mandle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
zásadní	zásadní	k2eAgFnPc1d1	zásadní
<g/>
:	:	kIx,	:
k	k	k7c3	k
chirurgickému	chirurgický	k2eAgNnSc3d1	chirurgické
odstranění	odstranění	k1gNnSc3	odstranění
(	(	kIx(	(
<g/>
apendektomie	apendektomie	k1gFnSc1	apendektomie
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
relativně	relativně	k6eAd1	relativně
často	často	k6eAd1	často
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
zánětu	zánět	k1gInSc6	zánět
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
apendicitida	apendicitida	k1gFnSc1	apendicitida
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
nezanechává	zanechávat	k5eNaImIp3nS	zanechávat
závažné	závažný	k2eAgInPc4d1	závažný
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
a	a	k8xC	a
šelem	šelma	k1gFnPc2	šelma
<g/>
)	)	kIx)	)
rudimentární	rudimentární	k2eAgInSc1d1	rudimentární
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
býložravců	býložravec	k1gMnPc2	býložravec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
u	u	k7c2	u
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
trávení	trávení	k1gNnSc6	trávení
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
<s>
Tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
–	–	k?	–
tračník	tračník	k1gInSc1	tračník
a	a	k8xC	a
konečník	konečník	k1gInSc1	konečník
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
plynule	plynule	k6eAd1	plynule
přecházejí	přecházet	k5eAaImIp3nP	přecházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tračníku	tračník	k1gInSc6	tračník
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vstřebávání	vstřebávání	k1gNnSc3	vstřebávání
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
solí	sůl	k1gFnPc2	sůl
a	a	k8xC	a
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
stolice	stolice	k1gFnSc2	stolice
<g/>
,	,	kIx,	,
v	v	k7c6	v
konečníku	konečník	k1gInSc6	konečník
se	se	k3xPyFc4	se
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
výkaly	výkal	k1gInPc1	výkal
hromadí	hromadit	k5eAaImIp3nP	hromadit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
konečník	konečník	k1gInSc4	konečník
navazuje	navazovat	k5eAaImIp3nS	navazovat
krátký	krátký	k2eAgInSc4d1	krátký
řitní	řitní	k2eAgInSc4d1	řitní
kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Sliznice	sliznice	k1gFnSc1	sliznice
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
netvoří	tvořit	k5eNaImIp3nP	tvořit
řasy	řasa	k1gFnPc1	řasa
ani	ani	k8xC	ani
klky	klk	k1gInPc1	klk
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
pohárkových	pohárkový	k2eAgFnPc2d1	pohárkový
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
hlen	hlen	k1gInSc4	hlen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
se	se	k3xPyFc4	se
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
žádné	žádný	k3yNgInPc1	žádný
trávicí	trávicí	k2eAgInPc1d1	trávicí
enzymy	enzym	k1gInPc1	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
následující	následující	k2eAgInPc1d1	následující
oddíly	oddíl	k1gInPc1	oddíl
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
:	:	kIx,	:
slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
(	(	kIx(	(
<g/>
intestinum	intestinum	k1gNnSc1	intestinum
caecum	caecum	k1gNnSc1	caecum
<g/>
)	)	kIx)	)
–	–	k?	–
nejširší	široký	k2eAgFnSc1d3	nejširší
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
uložena	uložen	k2eAgFnSc1d1	uložena
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
jámě	jáma	k1gFnSc6	jáma
kyčelní	kyčelní	k2eAgFnSc6d1	kyčelní
a	a	k8xC	a
ileocaekálním	ileocaekální	k2eAgNnSc7d1	ileocaekální
vyústěním	vyústění	k1gNnSc7	vyústění
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
boku	bok	k1gInSc6	bok
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
červovitý	červovitý	k2eAgInSc1d1	červovitý
výběžek	výběžek	k1gInSc1	výběžek
(	(	kIx(	(
<g/>
appendix	appendix	k1gInSc1	appendix
vermiformis	vermiformis	k1gFnSc2	vermiformis
<g/>
)	)	kIx)	)
–	–	k?	–
připojen	připojen	k2eAgInSc4d1	připojen
na	na	k7c4	na
slepě	slepě	k6eAd1	slepě
zakončený	zakončený	k2eAgInSc4d1	zakončený
dolní	dolní	k2eAgInSc4d1	dolní
konec	konec	k1gInSc4	konec
céka	cékum	k1gNnSc2	cékum
<g />
.	.	kIx.	.
</s>
<s>
tračník	tračník	k1gInSc1	tračník
(	(	kIx(	(
<g/>
colon	colon	k1gNnSc1	colon
<g/>
)	)	kIx)	)
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
:	:	kIx,	:
tračník	tračník	k1gInSc1	tračník
vzestupný	vzestupný	k2eAgInSc1d1	vzestupný
(	(	kIx(	(
<g/>
colon	colon	k1gNnSc1	colon
ascendens	ascendensa	k1gFnPc2	ascendensa
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
vzhůru	vzhůru	k6eAd1	vzhůru
pod	pod	k7c4	pod
játra	játra	k1gNnPc4	játra
tračník	tračník	k1gInSc1	tračník
příčný	příčný	k2eAgInSc1d1	příčný
(	(	kIx(	(
<g/>
colon	colon	k1gNnSc1	colon
transversum	transversum	k1gNnSc1	transversum
<g/>
)	)	kIx)	)
–	–	k?	–
zprava	zprava	k6eAd1	zprava
nalevo	nalevo	k6eAd1	nalevo
pod	pod	k7c7	pod
játry	játra	k1gNnPc7	játra
a	a	k8xC	a
žaludkem	žaludek	k1gInSc7	žaludek
ke	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
slezině	slezina	k1gFnSc3	slezina
tračník	tračník	k1gInSc4	tračník
sestupný	sestupný	k2eAgInSc4d1	sestupný
(	(	kIx(	(
<g/>
colon	colon	k1gNnSc1	colon
descendens	descendensa	k1gFnPc2	descendensa
<g/>
)	)	kIx)	)
–	–	k?	–
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc2d1	břišní
od	od	k7c2	od
sleziny	slezina	k1gFnSc2	slezina
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
jámy	jáma	k1gFnSc2	jáma
kyčelní	kyčelní	k2eAgFnSc1d1	kyčelní
esovitá	esovitý	k2eAgFnSc1d1	esovitá
klička	klička	k1gFnSc1	klička
(	(	kIx(	(
<g/>
colon	colon	k1gNnSc1	colon
sigmoideum	sigmoideum	k1gNnSc1	sigmoideum
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
konce	konec	k1gInSc2	konec
colon	colon	k1gNnSc4	colon
descendens	descendensa	k1gFnPc2	descendensa
do	do	k7c2	do
středu	střed	k1gInSc2	střed
malé	malý	k2eAgFnSc2d1	malá
pánve	pánev	k1gFnSc2	pánev
konečník	konečník	k1gInSc1	konečník
(	(	kIx(	(
<g/>
rectum	rectum	k1gNnSc1	rectum
<g/>
)	)	kIx)	)
-	-	kIx~	-
poslední	poslední	k2eAgInSc1d1	poslední
úsek	úsek	k1gInSc1	úsek
střeva	střevo	k1gNnSc2	střevo
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
pánvi	pánev	k1gFnSc6	pánev
<g/>
,	,	kIx,	,
navenek	navenek	k6eAd1	navenek
vyúsťuje	vyúsťovat	k5eAaImIp3nS	vyúsťovat
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
(	(	kIx(	(
<g/>
anus	anus	k1gInSc1	anus
<g/>
)	)	kIx)	)
Stěna	stěna	k1gFnSc1	stěna
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
svalovina	svalovina	k1gFnSc1	svalovina
je	být	k5eAaImIp3nS	být
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
<g/>
.	.	kIx.	.
</s>
<s>
Stěnu	stěna	k1gFnSc4	stěna
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
tvoří	tvořit	k5eAaImIp3nP	tvořit
čtyři	čtyři	k4xCgFnPc1	čtyři
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
Sliznice	sliznice	k1gFnPc1	sliznice
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
bledá	bledý	k2eAgFnSc1d1	bledá
<g/>
,	,	kIx,	,
nenese	nést	k5eNaImIp3nS	nést
klky	klk	k1gInPc4	klk
kryta	kryt	k2eAgNnPc4d1	kryto
jednovrstevným	jednovrstevný	k2eAgInSc7d1	jednovrstevný
cylindrickým	cylindrický	k2eAgInSc7d1	cylindrický
epitelem	epitel	k1gInSc7	epitel
četné	četný	k2eAgFnSc2d1	četná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
Lieberkühnovy	Lieberkühnův	k2eAgFnSc2d1	Lieberkühnův
krypty	krypta	k1gFnSc2	krypta
slizniční	slizniční	k2eAgNnSc1d1	slizniční
vazivo	vazivo	k1gNnSc1	vazivo
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
folliculi	follicule	k1gFnSc4	follicule
lymphatici	lymphatice	k1gFnSc4	lymphatice
solitarii	solitarie	k1gFnSc4	solitarie
–	–	k?	–
nejvíce	nejvíce	k6eAd1	nejvíce
ve	v	k7c6	v
slepém	slepý	k2eAgNnSc6d1	slepé
střevu	střevo	k1gNnSc6	střevo
<g/>
,	,	kIx,	,
nahromadění	nahromadění	k1gNnSc6	nahromadění
lymfatické	lymfatický	k2eAgFnSc2d1	lymfatická
tkáně	tkáň	k1gFnSc2	tkáň
v	v	k7c6	v
appendixu	appendix	k1gInSc6	appendix
lamina	lamin	k2eAgFnSc1d1	lamina
muscularis	muscularis	k1gFnSc1	muscularis
mucosae	mucosa	k1gFnSc2	mucosa
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
dobře	dobře	k6eAd1	dobře
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
<g/>
,	,	kIx,	,
podílné	podílný	k2eAgInPc4d1	podílný
i	i	k8xC	i
cirkulární	cirkulární	k2eAgInPc4d1	cirkulární
snopce	snopec	k1gInPc4	snopec
Podslizniční	podslizniční	k2eAgNnSc4d1	podslizniční
vazivo	vazivo	k1gNnSc4	vazivo
řídké	řídký	k2eAgNnSc4d1	řídké
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
cévní	cévní	k2eAgFnSc4d1	cévní
a	a	k8xC	a
nervovou	nervový	k2eAgFnSc4d1	nervová
pleteň	pleteň	k1gFnSc4	pleteň
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
něho	on	k3xPp3gInSc2	on
shluky	shluk	k1gInPc1	shluk
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
(	(	kIx(	(
<g/>
uzlíky	uzlík	k1gInPc4	uzlík
<g/>
)	)	kIx)	)
ze	z	k7c2	z
sliznice	sliznice	k1gFnSc2	sliznice
Svalovina	svalovina	k1gFnSc1	svalovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
cirkulární	cirkulární	k2eAgFnSc1d1	cirkulární
<g/>
,	,	kIx,	,
zevní	zevní	k2eAgFnSc1d1	zevní
longitudinální	longitudinální	k2eAgFnSc1d1	longitudinální
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
,	,	kIx,	,
v	v	k7c6	v
taeniích	taenie	k1gFnPc6	taenie
zahuštěná	zahuštěný	k2eAgFnSc1d1	zahuštěná
<g/>
)	)	kIx)	)
sfinktery	sfinkter	k1gInPc1	sfinkter
–	–	k?	–
místní	místní	k2eAgNnSc4d1	místní
nahromadění	nahromadění	k1gNnSc4	nahromadění
cirkulárních	cirkulární	k2eAgInPc2d1	cirkulární
snopců	snopec	k1gInPc2	snopec
Seróza	seróza	k1gFnSc1	seróza
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
peritoneální	peritoneální	k2eAgInSc4d1	peritoneální
povlak	povlak	k1gInSc4	povlak
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
appendices	appendices	k1gInSc1	appendices
epiploicae	epiploica	k1gFnSc2	epiploica
</s>
