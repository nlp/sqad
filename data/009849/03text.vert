<p>
<s>
The	The	k?	The
Social	Social	k1gInSc1	Social
Network	network	k1gInSc1	network
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
dramatický	dramatický	k2eAgInSc1d1	dramatický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Davida	David	k1gMnSc2	David
Finchera	Fincher	k1gMnSc2	Fincher
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
námětem	námět	k1gInSc7	námět
je	být	k5eAaImIp3nS	být
založení	založení	k1gNnSc4	založení
a	a	k8xC	a
začátky	začátek	k1gInPc4	začátek
Facebooku	Facebook	k1gInSc2	Facebook
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
právní	právní	k2eAgInPc4d1	právní
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
pozitivně	pozitivně	k6eAd1	pozitivně
přijat	přijmout	k5eAaPmNgInS	přijmout
kritiky	kritik	k1gMnPc7	kritik
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dramatický	dramatický	k2eAgInSc4d1	dramatický
film	film	k1gInSc4	film
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
glóby	glóbus	k1gInPc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
tři	tři	k4xCgFnPc4	tři
Oscary	Oscara	k1gFnPc4	Oscara
-	-	kIx~	-
za	za	k7c4	za
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
pět	pět	k4xCc4	pět
nominací	nominace	k1gFnPc2	nominace
neproměnil	proměnit	k5eNaPmAgInS	proměnit
<g/>
.	.	kIx.	.
</s>
</p>
