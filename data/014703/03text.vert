<s>
Řasa	řasa	k1gFnSc1
(	(	kIx(
<g/>
oko	oko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Řasy	řasa	k1gFnPc1
s	s	k7c7
řasenkou	řasenka	k1gFnSc7
</s>
<s>
Oční	oční	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
(	(	kIx(
<g/>
lat.	lat.	k?
cilia	cilia	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
kožní	kožní	k2eAgInPc1d1
deriváty	derivát	k1gInPc1
podobné	podobný	k2eAgInPc1d1
chlupům	chlup	k1gInPc3
a	a	k8xC
vlasům	vlas	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostou	růst	k5eAaImIp3nP
na	na	k7c6
horním	horní	k2eAgInSc6d1
i	i	k8xC
dolním	dolní	k2eAgNnSc6d1
očním	oční	k2eAgNnSc6d1
víčku	víčko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrání	chránit	k5eAaImIp3nS
oko	oko	k1gNnSc4
před	před	k7c7
vnikáním	vnikání	k1gNnSc7
jemných	jemný	k2eAgFnPc2d1
částic	částice	k1gFnPc2
(	(	kIx(
<g/>
nečistot	nečistota	k1gFnPc2
<g/>
,	,	kIx,
prachu	prach	k1gInSc2
<g/>
,	,	kIx,
malého	malý	k2eAgInSc2d1
hmyzu	hmyz	k1gInSc2
<g/>
)	)	kIx)
do	do	k7c2
oka	oko	k1gNnSc2
a	a	k8xC
zabraňují	zabraňovat	k5eAaImIp3nP
tak	tak	k9
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
poškození	poškození	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oční	oční	k2eAgFnSc1d1
řasa	řasa	k1gFnSc1
má	mít	k5eAaImIp3nS
válcovitý	válcovitý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
s	s	k7c7
zaoblenými	zaoblený	k2eAgInPc7d1
konci	konec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbarvení	zbarvení	k1gNnSc1
řas	řasa	k1gFnPc2
u	u	k7c2
lidí	člověk	k1gMnPc2
je	být	k5eAaImIp3nS
rozdílné	rozdílný	k2eAgNnSc1d1
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
lidských	lidský	k2eAgInPc2d1
vlasů	vlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
embrya	embryo	k1gNnSc2
se	se	k3xPyFc4
řasy	řasa	k1gFnPc1
vyvíjí	vyvíjet	k5eAaImIp3nP
mezi	mezi	k7c7
22	#num#	k4
<g/>
.	.	kIx.
až	až	k9
26	#num#	k4
<g/>
.	.	kIx.
týdnem	týden	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
mnohých	mnohý	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
symbol	symbol	k1gInSc4
ženskosti	ženskost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
zvýraznění	zvýraznění	k1gNnSc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
řasenka	řasenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
také	také	k9
umělé	umělý	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
nalepují	nalepovat	k5eAaImIp3nP
na	na	k7c4
oční	oční	k2eAgNnPc4d1
víčka	víčko	k1gNnPc4
místo	místo	k7c2
přírodních	přírodní	k2eAgFnPc2d1
řas	řasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
mnohých	mnohý	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
kůň	kůň	k1gMnSc1
<g/>
,	,	kIx,
velbloud	velbloud	k1gMnSc1
<g/>
)	)	kIx)
fungují	fungovat	k5eAaImIp3nP
oční	oční	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Řasy	řasa	k1gFnPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
také	také	k9
kosmeticky	kosmeticky	k6eAd1
upravovat	upravovat	k5eAaImF
<g/>
,	,	kIx,
dají	dát	k5eAaPmIp3nP
se	se	k3xPyFc4
barvit	barvit	k5eAaImF
<g/>
,	,	kIx,
či	či	k8xC
prodlužovat	prodlužovat	k5eAaImF
či	či	k8xC
prohustit	prohustit	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Řasa	řasa	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
řasa	řasa	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85046703	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85046703	#num#	k4
</s>
