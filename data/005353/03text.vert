<s>
Ropa	ropa	k1gFnSc1	ropa
(	(	kIx(	(
<g/>
též	též	k9	též
(	(	kIx(	(
<g/>
surová	surový	k2eAgFnSc1d1	surová
<g/>
)	)	kIx)	)
nafta	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
černé	černý	k2eAgNnSc1d1	černé
zlato	zlato	k1gNnSc1	zlato
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
až	až	k8xS	až
nazelenalá	nazelenalý	k2eAgFnSc1d1	nazelenalá
hořlavá	hořlavý	k2eAgFnSc1d1	hořlavá
kapalina	kapalina	k1gFnSc1	kapalina
tvořená	tvořený	k2eAgFnSc1d1	tvořená
směsí	směs	k1gFnSc7	směs
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
alkanů	alkan	k1gInPc2	alkan
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
patři	patřit	k5eAaImRp2nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kaustobiolity	kaustobiolit	k1gInPc1	kaustobiolit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Geochemiky	geochemik	k1gMnPc4	geochemik
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
ropy	ropa	k1gFnSc2	ropa
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přeměnou	přeměna	k1gFnSc7	přeměna
živočišného	živočišný	k2eAgInSc2d1	živočišný
a	a	k8xC	a
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
obsaženého	obsažený	k2eAgInSc2d1	obsažený
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
svrchních	svrchní	k2eAgFnPc6d1	svrchní
vrstvách	vrstva	k1gFnPc6	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedimentárních	sedimentární	k2eAgFnPc6d1	sedimentární
pánvích	pánev	k1gFnPc6	pánev
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
kontinentální	kontinentální	k2eAgInPc1d1	kontinentální
šelfy	šelf	k1gInPc1	šelf
<g/>
,	,	kIx,	,
hlubokovodní	hlubokovodní	k2eAgFnPc1d1	hlubokovodní
pánve	pánev	k1gFnPc1	pánev
zahrnující	zahrnující	k2eAgInPc4d1	zahrnující
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
svahy	svah	k1gInPc4	svah
i	i	k8xC	i
abysál	abysál	k1gInSc1	abysál
<g/>
,	,	kIx,	,
riftové	riftový	k2eAgFnSc2d1	riftový
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
predpolní	predpolní	k2eAgFnSc2d1	predpolní
pánve	pánev	k1gFnSc2	pánev
horských	horský	k2eAgInPc2d1	horský
řetězců	řetězec	k1gInPc2	řetězec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
deformované	deformovaný	k2eAgFnPc4d1	deformovaná
vrásové	vrásový	k2eAgFnPc4d1	vrásová
struktury	struktura	k1gFnPc4	struktura
některých	některý	k3yIgNnPc2	některý
horstev	horstvo	k1gNnPc2	horstvo
samých	samý	k3xTgMnPc6	samý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
petrochemického	petrochemický	k2eAgInSc2d1	petrochemický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Naleziště	naleziště	k1gNnPc1	naleziště
ropy	ropa	k1gFnSc2	ropa
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
nepropustnými	propustný	k2eNgFnPc7d1	nepropustná
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
,	,	kIx,	,
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
od	od	k7c2	od
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
až	až	k6eAd1	až
do	do	k7c2	do
hloubek	hloubka	k1gFnPc2	hloubka
přes	přes	k7c4	přes
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
buď	buď	k8xC	buď
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
čerpána	čerpán	k2eAgFnSc1d1	čerpána
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
se	s	k7c7	s
zemním	zemní	k2eAgInSc7d1	zemní
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
ropa	ropa	k1gFnSc1	ropa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
polštiny	polština	k1gFnSc2	polština
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
hnis	hnis	k1gInSc1	hnis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
původní	původní	k2eAgNnSc4d1	původní
staré	starý	k2eAgNnSc4d1	staré
označení	označení	k1gNnSc4	označení
tamních	tamní	k2eAgInPc2d1	tamní
solných	solný	k2eAgInPc2d1	solný
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
geochemickými	geochemický	k2eAgInPc7d1	geochemický
procesy	proces	k1gInPc7	proces
z	z	k7c2	z
matečných	matečný	k2eAgFnPc2d1	matečná
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
geologickém	geologický	k2eAgInSc6d1	geologický
čase	čas	k1gInSc6	čas
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
několika	několik	k4yIc2	několik
miliónů	milión	k4xCgInPc2	milión
nebo	nebo	k8xC	nebo
desítek	desítka	k1gFnPc2	desítka
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
lidského	lidský	k2eAgNnSc2d1	lidské
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
neobnovitelný	obnovitelný	k2eNgInSc4d1	neobnovitelný
zdroj	zdroj	k1gInSc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnSc7	směs
tekutých	tekutý	k2eAgInPc2d1	tekutý
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
jsou	být	k5eAaImIp3nP	být
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
složené	složený	k2eAgFnPc1d1	složená
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
uhlíku	uhlík	k1gInSc2	uhlík
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
uhlíku	uhlík	k1gInSc2	uhlík
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
různě	různě	k6eAd1	různě
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
řetězce	řetězec	k1gInPc1	řetězec
<g/>
;	;	kIx,	;
atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
řetězce	řetězec	k1gInPc4	řetězec
napojeny	napojen	k2eAgInPc4d1	napojen
<g/>
.	.	kIx.	.
</s>
<s>
Alkany	Alkan	k1gInPc1	Alkan
<g/>
,	,	kIx,	,
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
s	s	k7c7	s
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
vazbou	vazba	k1gFnSc7	vazba
mezi	mezi	k7c4	mezi
atomy	atom	k1gInPc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
C1	C1	k4	C1
až	až	k8xS	až
C4	C4	k1gMnSc1	C4
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
sloučeniny	sloučenina	k1gFnPc4	sloučenina
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
až	až	k8xS	až
čtyřmi	čtyři	k4xCgInPc7	čtyři
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
plynné	plynný	k2eAgFnPc4d1	plynná
<g/>
,	,	kIx,	,
C5	C5	k1gFnPc4	C5
až	až	k8xS	až
C14	C14	k1gFnPc4	C14
kapalné	kapalný	k2eAgFnPc4d1	kapalná
<g/>
,	,	kIx,	,
C15	C15	k1gFnPc4	C15
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc4d2	vyšší
jsou	být	k5eAaImIp3nP	být
tuhé	tuhý	k2eAgFnPc1d1	tuhá
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
technologické	technologický	k2eAgFnPc4d1	technologická
charakteristiky	charakteristika	k1gFnPc4	charakteristika
ropy	ropa	k1gFnSc2	ropa
patří	patřit	k5eAaImIp3nS	patřit
hustota	hustota	k1gFnSc1	hustota
stanovená	stanovený	k2eAgFnSc1d1	stanovená
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
730	[number]	k4	730
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
000	[number]	k4	000
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
0,61	[number]	k4	0,61
<g/>
–	–	k?	–
<g/>
0,85	[number]	k4	0,85
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
pro	pro	k7c4	pro
lehké	lehký	k2eAgNnSc4d1	lehké
ropy	ropa	k1gFnPc4	ropa
<g/>
,	,	kIx,	,
0,85	[number]	k4	0,85
<g/>
–	–	k?	–
<g/>
0,93	[number]	k4	0,93
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
pro	pro	k7c4	pro
středně	středně	k6eAd1	středně
těžké	těžký	k2eAgInPc4d1	těžký
až	až	k9	až
0,93	[number]	k4	0,93
<g/>
–	–	k?	–
<g/>
1,05	[number]	k4	1,05
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgFnPc4d1	těžká
ropy	ropa	k1gFnPc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hustota	hustota	k1gFnSc1	hustota
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
proporcemi	proporce	k1gFnPc7	proporce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
<g/>
.	.	kIx.	.
</s>
<s>
Lehké	Lehké	k2eAgFnPc1d1	Lehké
ropy	ropa	k1gFnPc1	ropa
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
více	hodně	k6eAd2	hodně
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
s	s	k7c7	s
kratšími	krátký	k2eAgInPc7d2	kratší
řetězci	řetězec	k1gInPc7	řetězec
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
těžké	těžký	k2eAgFnPc1d1	těžká
ropy	ropa	k1gFnPc1	ropa
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
více	hodně	k6eAd2	hodně
sloučenin	sloučenina	k1gFnPc2	sloučenina
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
řetězci	řetězec	k1gInPc7	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgFnPc1d3	nejtěžší
ropy	ropa	k1gFnPc1	ropa
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
ropné	ropný	k2eAgInPc1d1	ropný
písky	písek	k1gInPc1	písek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tuhé	tuhý	k2eAgInPc1d1	tuhý
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
je	on	k3xPp3gInPc4	on
dolovat	dolovat	k5eAaImF	dolovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tradiční	tradiční	k2eAgFnSc1d1	tradiční
těžba	těžba	k1gFnSc1	těžba
pomoci	pomoc	k1gFnSc2	pomoc
vrtů	vrt	k1gInPc2	vrt
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
objemu	objem	k1gInSc2	objem
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
míry	míra	k1gFnSc2	míra
1	[number]	k4	1
barel	barel	k1gInSc1	barel
=	=	kIx~	=
42	[number]	k4	42
amerických	americký	k2eAgInPc2d1	americký
galonů	galon	k1gInPc2	galon
=	=	kIx~	=
35	[number]	k4	35
britských	britský	k2eAgInPc2d1	britský
galonů	galon	k1gInPc2	galon
=	=	kIx~	=
158,97	[number]	k4	158,97
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
1	[number]	k4	1
barel	barel	k1gInSc1	barel
ropy	ropa	k1gFnSc2	ropa
tak	tak	k6eAd1	tak
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
ropy	ropa	k1gFnSc2	ropa
váží	vážit	k5eAaImIp3nS	vážit
od	od	k7c2	od
96,972	[number]	k4	96,972
kg	kg	kA	kg
do	do	k7c2	do
166,92	[number]	k4	166,92
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
tunách	tuna	k1gFnPc6	tuna
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
tuna	tuna	k1gFnSc1	tuna
ropy	ropa	k1gFnSc2	ropa
tak	tak	k6eAd1	tak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
-	-	kIx~	-
10,32	[number]	k4	10,32
barelu	barel	k1gInSc2	barel
<g/>
.	.	kIx.	.
</s>
<s>
Přibližnou	přibližný	k2eAgFnSc4d1	přibližná
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
ropy	ropa	k1gFnSc2	ropa
dávají	dávat	k5eAaImIp3nP	dávat
následující	následující	k2eAgInPc1d1	následující
hmotnostní	hmotnostní	k2eAgInPc1d1	hmotnostní
podíly	podíl	k1gInPc1	podíl
<g/>
:	:	kIx,	:
Uhlík	uhlík	k1gInSc1	uhlík
<g/>
:	:	kIx,	:
84	[number]	k4	84
<g/>
–	–	k?	–
<g/>
87	[number]	k4	87
%	%	kIx~	%
Vodík	vodík	k1gInSc1	vodík
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
%	%	kIx~	%
Kyslík	kyslík	k1gInSc1	kyslík
až	až	k6eAd1	až
1	[number]	k4	1
%	%	kIx~	%
Síra	síra	k1gFnSc1	síra
až	až	k9	až
4	[number]	k4	4
%	%	kIx~	%
Dusík	dusík	k1gInSc1	dusík
až	až	k6eAd1	až
1	[number]	k4	1
%	%	kIx~	%
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
organicky	organicky	k6eAd1	organicky
a	a	k8xC	a
anorganicky	anorganicky	k6eAd1	anorganicky
<g/>
.	.	kIx.	.
</s>
<s>
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
který	který	k3yIgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
například	například	k6eAd1	například
při	při	k7c6	při
tuhnutí	tuhnutí	k1gNnSc6	tuhnutí
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
v	v	k7c6	v
komerčních	komerční	k2eAgNnPc6d1	komerční
nalezištích	naleziště	k1gNnPc6	naleziště
je	být	k5eAaImIp3nS	být
však	však	k9	však
organického	organický	k2eAgInSc2d1	organický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
buď	buď	k8xC	buď
biogenezi	biogeneze	k1gFnSc4	biogeneze
nebo	nebo	k8xC	nebo
termogenezi	termogeneze	k1gFnSc4	termogeneze
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
Anorganický	anorganický	k2eAgInSc4d1	anorganický
původ	původ	k1gInSc4	původ
ropy	ropa	k1gFnSc2	ropa
Anorganický	anorganický	k2eAgInSc4d1	anorganický
původ	původ	k1gInSc4	původ
ropy	ropa	k1gFnSc2	ropa
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
působením	působení	k1gNnSc7	působení
přehřáté	přehřátý	k2eAgFnSc2d1	přehřátá
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
karbidy	karbid	k1gInPc4	karbid
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
petrochemii	petrochemie	k1gFnSc6	petrochemie
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
blízko	blízko	k7c2	blízko
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
svědčí	svědčit	k5eAaImIp3nS	svědčit
jednak	jednak	k8xC	jednak
laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
příprava	příprava	k1gFnSc1	příprava
pevných	pevný	k2eAgInPc2d1	pevný
<g/>
,	,	kIx,	,
kapalných	kapalný	k2eAgInPc2d1	kapalný
i	i	k8xC	i
plynných	plynný	k2eAgInPc2d1	plynný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
z	z	k7c2	z
karbidů	karbid	k1gInPc2	karbid
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
lanthanu	lanthan	k1gInSc2	lanthan
i	i	k8xC	i
ceru	cer	k1gInSc2	cer
a	a	k8xC	a
také	také	k9	také
neustálý	neustálý	k2eAgInSc4d1	neustálý
únik	únik	k1gInSc4	únik
metanu	metan	k1gInSc2	metan
ze	z	k7c2	z
zemského	zemský	k2eAgNnSc2d1	zemské
nitra	nitro	k1gNnSc2	nitro
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
výzkum	výzkum	k1gInSc1	výzkum
dokázal	dokázat	k5eAaPmAgInS	dokázat
přítomnost	přítomnost	k1gFnSc4	přítomnost
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
v	v	k7c6	v
oceánských	oceánský	k2eAgInPc6d1	oceánský
hřbetech	hřbet	k1gInPc6	hřbet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
nová	nový	k2eAgFnSc1d1	nová
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
prokazatelně	prokazatelně	k6eAd1	prokazatelně
nejsou	být	k5eNaImIp3nP	být
z	z	k7c2	z
organického	organický	k2eAgInSc2d1	organický
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Nalezená	nalezený	k2eAgNnPc1d1	nalezené
množství	množství	k1gNnPc1	množství
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anorganického	anorganický	k2eAgInSc2d1	anorganický
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
však	však	k9	však
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ropa	ropa	k1gFnSc1	ropa
vzniká	vznikat	k5eAaImIp3nS	vznikat
převážně	převážně	k6eAd1	převážně
abioticky	abioticky	k6eAd1	abioticky
<g/>
.	.	kIx.	.
</s>
<s>
Organický	organický	k2eAgInSc1d1	organický
původ	původ	k1gInSc1	původ
ropy	ropa	k1gFnSc2	ropa
Organická	organický	k2eAgFnSc1d1	organická
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
uznávána	uznávat	k5eAaImNgFnS	uznávat
naprostou	naprostý	k2eAgFnSc7d1	naprostá
většinou	většina	k1gFnSc7	většina
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ropa	ropa	k1gFnSc1	ropa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
termogenickým	termogenický	k2eAgInSc7d1	termogenický
rozkladem	rozklad	k1gInSc7	rozklad
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
živočišných	živočišný	k2eAgInPc2d1	živočišný
a	a	k8xC	a
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
zbytků	zbytek	k1gInPc2	zbytek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organický	organický	k2eAgInSc1d1	organický
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
kerogen	kerogen	k1gInSc4	kerogen
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
ropu	ropa	k1gFnSc4	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
tvořit	tvořit	k5eAaImF	tvořit
při	při	k7c6	při
cca	cca	kA	cca
60	[number]	k4	60
stupních	stupeň	k1gInPc6	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
termogenickým	termogenický	k2eAgInSc7d1	termogenický
rozpadem	rozpad	k1gInSc7	rozpad
(	(	kIx(	(
<g/>
krakováním	krakování	k1gNnPc3	krakování
<g/>
)	)	kIx)	)
kerogenu	kerogen	k1gInSc3	kerogen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
do	do	k7c2	do
cca	cca	kA	cca
120	[number]	k4	120
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cca	cca	kA	cca
100	[number]	k4	100
stupních	stupeň	k1gInPc6	stupeň
začíná	začínat	k5eAaImIp3nS	začínat
tvorba	tvorba	k1gFnSc1	tvorba
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
200	[number]	k4	200
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Teplotnímu	teplotní	k2eAgInSc3d1	teplotní
intervalu	interval	k1gInSc3	interval
tvorby	tvorba	k1gFnSc2	tvorba
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ropné	ropný	k2eAgNnSc1d1	ropné
okno	okno	k1gNnSc1	okno
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplotnímu	teplotní	k2eAgInSc3d1	teplotní
intervalu	interval	k1gInSc3	interval
tvorby	tvorba	k1gFnSc2	tvorba
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
plynové	plynový	k2eAgNnSc4d1	plynové
okno	okno	k1gNnSc4	okno
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
<g/>
+	+	kIx~	+
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tepelného	tepelný	k2eAgInSc2d1	tepelný
toku	tok	k1gInSc2	tok
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
sedimentární	sedimentární	k2eAgFnSc6d1	sedimentární
pánvi	pánev	k1gFnSc6	pánev
se	se	k3xPyFc4	se
hloubka	hloubka	k1gFnSc1	hloubka
ropného	ropný	k2eAgNnSc2d1	ropné
okna	okno	k1gNnSc2	okno
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
km	km	kA	km
a	a	k8xC	a
hloubka	hloubka	k1gFnSc1	hloubka
plynového	plynový	k2eAgNnSc2d1	plynové
okna	okno	k1gNnSc2	okno
mezi	mezi	k7c7	mezi
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
teplotní	teplotní	k2eAgFnPc4d1	teplotní
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
zdrojové	zdrojový	k2eAgFnPc1d1	zdrojová
horniny	hornina	k1gFnPc1	hornina
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
různých	různý	k2eAgInPc2d1	různý
typu	typ	k1gInSc2	typ
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
uhlí	uhlí	k1gNnSc2	uhlí
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zdrojovou	zdrojový	k2eAgFnSc4d1	zdrojová
horninu	hornina	k1gFnSc4	hornina
náchylnou	náchylný	k2eAgFnSc4d1	náchylná
ke	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
typu	typ	k1gInSc6	typ
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
i	i	k9	i
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
organická	organický	k2eAgFnSc1d1	organická
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
,	,	kIx,	,
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c6	v
hlubokých	hluboký	k2eAgInPc6d1	hluboký
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
výbornou	výborný	k2eAgFnSc4d1	výborná
zdrojovou	zdrojový	k2eAgFnSc4d1	zdrojová
horninu	hornina	k1gFnSc4	hornina
náchylnou	náchylný	k2eAgFnSc4d1	náchylná
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
typ	typ	k1gInSc4	typ
organického	organický	k2eAgInSc2d1	organický
materiálu	materiál	k1gInSc2	materiál
byl	být	k5eAaImAgInS	být
uložen	uložen	k2eAgInSc1d1	uložen
a	a	k8xC	a
jaké	jaký	k3yIgFnPc1	jaký
panovaly	panovat	k5eAaImAgFnP	panovat
v	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
biomarkery	biomarkera	k1gFnSc2	biomarkera
jsou	být	k5eAaImIp3nP	být
skupinou	skupina	k1gFnSc7	skupina
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
,	,	kIx,	,
nalezených	nalezený	k2eAgInPc2d1	nalezený
v	v	k7c6	v
ropě	ropa	k1gFnSc6	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Biomarkery	Biomarkera	k1gFnPc1	Biomarkera
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
sloučenin	sloučenina	k1gFnPc2	sloučenina
v	v	k7c6	v
ropě	ropa	k1gFnSc6	ropa
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
biomarkery	biomarkera	k1gFnPc4	biomarkera
lze	lze	k6eAd1	lze
nazvat	nazvat	k5eAaBmF	nazvat
"	"	kIx"	"
<g/>
molekulární	molekulární	k2eAgFnSc1d1	molekulární
fosilie	fosilie	k1gFnSc1	fosilie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Biomarkery	Biomarkera	k1gFnPc1	Biomarkera
jsou	být	k5eAaImIp3nP	být
strukturálně	strukturálně	k6eAd1	strukturálně
podobné	podobný	k2eAgFnPc1d1	podobná
specifickým	specifický	k2eAgFnPc3d1	specifická
sloučeninám	sloučenina	k1gFnPc3	sloučenina
produkovaným	produkovaný	k2eAgFnPc3d1	produkovaná
živými	živá	k1gFnPc7	živá
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Biomarkery	Biomarkero	k1gNnPc7	Biomarkero
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
nebo	nebo	k8xC	nebo
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
skeletu	skelet	k1gInSc2	skelet
původní	původní	k2eAgFnSc2d1	původní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
biomarkeru	biomarker	k1gInSc2	biomarker
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
ropného	ropný	k2eAgInSc2d1	ropný
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Geochemik	geochemik	k1gMnSc1	geochemik
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyčíst	vyčíst	k5eAaPmF	vyčíst
zda	zda	k8xS	zda
ropa	ropa	k1gFnSc1	ropa
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
z	z	k7c2	z
organického	organický	k2eAgInSc2d1	organický
materiálu	materiál	k1gInSc2	materiál
suchozemského	suchozemský	k2eAgInSc2d1	suchozemský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
mořského	mořský	k2eAgInSc2d1	mořský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
řasy	řasa	k1gFnPc1	řasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezerního	jezerní	k2eAgInSc2d1	jezerní
nebo	nebo	k8xC	nebo
uhelného	uhelný	k2eAgInSc2d1	uhelný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
zdrojová	zdrojový	k2eAgFnSc1d1	zdrojová
hornina	hornina	k1gFnSc1	hornina
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
zralosti	zralost	k1gFnSc3	zralost
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
migrace	migrace	k1gFnSc1	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
migrují	migrovat	k5eAaImIp3nP	migrovat
buď	buď	k8xC	buď
podél	podél	k7c2	podél
geologických	geologický	k2eAgInPc2d1	geologický
zlomů	zlom	k1gInPc2	zlom
(	(	kIx(	(
<g/>
vertikální	vertikální	k2eAgFnSc1d1	vertikální
migrace	migrace	k1gFnSc1	migrace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podél	podél	k7c2	podél
porézních	porézní	k2eAgFnPc2d1	porézní
sedimentárních	sedimentární	k2eAgFnPc2d1	sedimentární
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
laterální	laterální	k2eAgFnSc1d1	laterální
migrace	migrace	k1gFnSc1	migrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
příklady	příklad	k1gInPc1	příklad
nosných	nosný	k2eAgFnPc2d1	nosná
vrstev	vrstva	k1gFnPc2	vrstva
při	při	k7c6	při
laterální	laterální	k2eAgFnSc6d1	laterální
migraci	migrace	k1gFnSc6	migrace
jsou	být	k5eAaImIp3nP	být
porézní	porézní	k2eAgInPc1d1	porézní
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
vápence	vápenec	k1gInPc4	vápenec
nebo	nebo	k8xC	nebo
i	i	k9	i
zvětralé	zvětralý	k2eAgFnSc2d1	zvětralá
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
je	být	k5eAaImIp3nS	být
zachycení	zachycení	k1gNnSc4	zachycení
migrující	migrující	k2eAgFnSc2d1	migrující
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
ropné	ropný	k2eAgFnPc4d1	ropná
pasti	past	k1gFnPc4	past
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
jejich	jejich	k3xOp3gNnPc4	jejich
současná	současný	k2eAgNnPc4d1	současné
naleziště	naleziště	k1gNnPc4	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Ropná	ropný	k2eAgFnSc1d1	ropná
past	past	k1gFnSc1	past
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
porézních	porézní	k2eAgFnPc2d1	porézní
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
utěsněné	utěsněný	k2eAgFnSc2d1	utěsněná
horninami	hornina	k1gFnPc7	hornina
nepropustnými	propustný	k2eNgFnPc7d1	nepropustná
<g/>
.	.	kIx.	.
</s>
<s>
Ropné	ropný	k2eAgFnPc1d1	ropná
pasti	past	k1gFnPc1	past
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
jako	jako	k9	jako
geologické	geologický	k2eAgFnPc1d1	geologická
struktury	struktura	k1gFnPc1	struktura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
antiklinály	antiklinála	k1gFnSc2	antiklinála
<g/>
,	,	kIx,	,
zlomové	zlomový	k2eAgFnSc2d1	zlomová
struktury	struktura	k1gFnSc2	struktura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stratigraficky	stratigraficky	k6eAd1	stratigraficky
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyklinováním	vyklinování	k1gNnSc7	vyklinování
pískovce	pískovec	k1gInSc2	pískovec
a	a	k8xC	a
faciálním	faciální	k2eAgInSc7d1	faciální
přechodem	přechod	k1gInSc7	přechod
do	do	k7c2	do
nepropustných	propustný	k2eNgFnPc2d1	nepropustná
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těsnící	těsnící	k2eAgFnPc1d1	těsnící
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
břidlice	břidlice	k1gFnSc2	břidlice
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
jílu	jíl	k1gInSc2	jíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
zvětralé	zvětralý	k2eAgFnPc1d1	zvětralá
<g/>
.	.	kIx.	.
</s>
<s>
Srdcem	srdce	k1gNnSc7	srdce
ropné	ropný	k2eAgFnSc2d1	ropná
pasti	past	k1gFnSc2	past
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vlastní	vlastní	k2eAgFnSc1d1	vlastní
porézní	porézní	k2eAgFnSc1d1	porézní
hornina	hornina	k1gFnSc1	hornina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kolektorová	kolektorový	k2eAgFnSc1d1	kolektorová
hornina	hornina	k1gFnSc1	hornina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
migrující	migrující	k2eAgFnSc1d1	migrující
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
nahromadí	nahromadit	k5eAaPmIp3nS	nahromadit
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
kolektorové	kolektorový	k2eAgFnPc1d1	kolektorová
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
porézní	porézní	k2eAgInPc4d1	porézní
pískovce	pískovec	k1gInPc4	pískovec
nebo	nebo	k8xC	nebo
vápence	vápenec	k1gInPc4	vápenec
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
parametrem	parametr	k1gInSc7	parametr
kolektorových	kolektorový	k2eAgFnPc2d1	kolektorová
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
porozita	porozita	k1gFnSc1	porozita
a	a	k8xC	a
propustnost	propustnost	k1gFnSc1	propustnost
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
porozita	porozita	k1gFnSc1	porozita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
%	%	kIx~	%
a	a	k8xC	a
typická	typický	k2eAgFnSc1d1	typická
propustnost	propustnost	k1gFnSc1	propustnost
od	od	k7c2	od
100	[number]	k4	100
Millidarcy	Millidarca	k1gFnSc2	Millidarca
(	(	kIx(	(
<g/>
mD	mD	k?	mD
<g/>
)	)	kIx)	)
do	do	k7c2	do
několika	několik	k4yIc2	několik
Darcy	Darcum	k1gNnPc7	Darcum
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojové	zdrojový	k2eAgFnSc2d1	zdrojová
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
maturace	maturace	k1gFnSc2	maturace
<g/>
,	,	kIx,	,
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
ropné	ropný	k2eAgFnSc2d1	ropná
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
kolektorové	kolektorový	k2eAgFnSc2d1	kolektorová
a	a	k8xC	a
těsnicí	těsnicí	k2eAgFnSc2d1	těsnicí
horniny	hornina	k1gFnSc2	hornina
jsou	být	k5eAaImIp3nP	být
souhrnně	souhrnně	k6eAd1	souhrnně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
ropným	ropný	k2eAgInSc7d1	ropný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
naleziště	naleziště	k1gNnSc4	naleziště
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
sedimentární	sedimentární	k2eAgFnSc6d1	sedimentární
pánvi	pánev	k1gFnSc6	pánev
existovat	existovat	k5eAaImF	existovat
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
elementy	element	k1gInPc4	element
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
musí	muset	k5eAaImIp3nS	muset
jejich	jejich	k3xOp3gFnSc1	jejich
tvorba	tvorba	k1gFnSc1	tvorba
proběhnout	proběhnout	k5eAaPmF	proběhnout
ve	v	k7c6	v
správné	správný	k2eAgFnSc6d1	správná
sekvenci	sekvence	k1gFnSc6	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tvorba	tvorba	k1gFnSc1	tvorba
ropných	ropný	k2eAgFnPc2d1	ropná
pastí	past	k1gFnPc2	past
musí	muset	k5eAaImIp3nP	muset
proběhnout	proběhnout	k5eAaPmF	proběhnout
před	před	k7c7	před
migrací	migrace	k1gFnSc7	migrace
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
budou	být	k5eAaImBp3nP	být
migrovat	migrovat	k5eAaImF	migrovat
až	až	k9	až
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
uniknou	uniknout	k5eAaPmIp3nP	uniknout
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
elementů	element	k1gInPc2	element
ropného	ropný	k2eAgInSc2d1	ropný
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
moderního	moderní	k2eAgInSc2d1	moderní
ropného	ropný	k2eAgInSc2d1	ropný
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
ropný	ropný	k2eAgInSc1d1	ropný
průzkum	průzkum	k1gInSc1	průzkum
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
integraci	integrace	k1gFnSc6	integrace
mnoha	mnoho	k4c2	mnoho
technických	technický	k2eAgFnPc2d1	technická
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
:	:	kIx,	:
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
geofyziky	geofyzika	k1gFnSc2	geofyzika
<g/>
,	,	kIx,	,
paleontologie	paleontologie	k1gFnSc2	paleontologie
<g/>
,	,	kIx,	,
geochemie	geochemie	k1gFnSc2	geochemie
<g/>
,	,	kIx,	,
petrofyziky	petrofyzika	k1gFnSc2	petrofyzika
<g/>
,	,	kIx,	,
ropného	ropný	k2eAgNnSc2d1	ropné
inženýrství	inženýrství	k1gNnSc2	inženýrství
a	a	k8xC	a
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
týmu	tým	k1gInSc3	tým
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nichž	jenž	k3xRgFnPc2	jenž
uvedené	uvedený	k2eAgFnPc1d1	uvedená
disciplíny	disciplína	k1gFnPc1	disciplína
těsně	těsně	k6eAd1	těsně
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
dobrá	dobrý	k2eAgFnSc1d1	dobrá
znalost	znalost	k1gFnSc1	znalost
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
strukturní	strukturní	k2eAgFnSc2d1	strukturní
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
vybuduje	vybudovat	k5eAaPmIp3nS	vybudovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
seismických	seismický	k2eAgFnPc2d1	seismická
profilu	profil	k1gInSc6	profil
buď	buď	k8xC	buď
ve	v	k7c6	v
2D	[number]	k4	2D
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
že	že	k8xS	že
satelitních	satelitní	k2eAgInPc2d1	satelitní
snímků	snímek	k1gInPc2	snímek
nebo	nebo	k8xC	nebo
z	z	k7c2	z
terénní	terénní	k2eAgFnSc2d1	terénní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Geochemická	geochemický	k2eAgFnSc1d1	geochemická
<g/>
,	,	kIx,	,
geologická	geologický	k2eAgFnSc1d1	geologická
a	a	k8xC	a
paleontologická	paleontologický	k2eAgNnPc4d1	paleontologické
data	datum	k1gNnPc4	datum
ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
vrtů	vrt	k1gInPc2	vrt
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
integrovat	integrovat	k5eAaBmF	integrovat
do	do	k7c2	do
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
modelu	model	k1gInSc2	model
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
co	co	k9	co
nejpřesnější	přesný	k2eAgInSc1d3	nejpřesnější
obrázek	obrázek	k1gInSc1	obrázek
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Geochemik	geochemik	k1gMnSc1	geochemik
provede	provést	k5eAaPmIp3nS	provést
analýzu	analýza	k1gFnSc4	analýza
zdrojových	zdrojový	k2eAgFnPc2d1	zdrojová
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
existují	existovat	k5eAaImIp3nP	existovat
vzorky	vzorek	k1gInPc4	vzorek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
analýzu	analýza	k1gFnSc4	analýza
vzorků	vzorek	k1gInPc2	vzorek
ropy	ropa	k1gFnSc2	ropa
buď	buď	k8xC	buď
z	z	k7c2	z
existujících	existující	k2eAgInPc2d1	existující
vrtů	vrt	k1gInPc2	vrt
nebo	nebo	k8xC	nebo
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
kde	kde	k6eAd1	kde
ropa	ropa	k1gFnSc1	ropa
prosakuje	prosakovat	k5eAaImIp3nS	prosakovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
věk	věk	k1gInSc1	věk
a	a	k8xC	a
typ	typ	k1gInSc1	typ
zdrojové	zdrojový	k2eAgFnSc2d1	zdrojová
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
stupeň	stupeň	k1gInSc1	stupeň
termální	termální	k2eAgFnSc2d1	termální
maturace	maturace	k1gFnSc2	maturace
<g/>
.	.	kIx.	.
</s>
<s>
Paleontolog	paleontolog	k1gMnSc1	paleontolog
provede	provést	k5eAaPmIp3nS	provést
analýzu	analýza	k1gFnSc4	analýza
podpovrchových	podpovrchový	k2eAgInPc2d1	podpovrchový
vzorků	vzorek	k1gInPc2	vzorek
z	z	k7c2	z
vrtů	vrt	k1gInPc2	vrt
a	a	k8xC	a
povrchových	povrchový	k2eAgInPc2d1	povrchový
vzorků	vzorek	k1gInPc2	vzorek
z	z	k7c2	z
výchozů	výchoz	k1gInPc2	výchoz
<g/>
.	.	kIx.	.
</s>
<s>
Určí	určit	k5eAaPmIp3nS	určit
věk	věk	k1gInSc1	věk
hornin	hornina	k1gFnPc2	hornina
ve	v	k7c6	v
studované	studovaný	k2eAgFnSc6d1	studovaná
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
jejich	jejich	k3xOp3gInSc4	jejich
sedimentární	sedimentární	k2eAgInSc4d1	sedimentární
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
fosilních	fosilní	k2eAgNnPc2d1	fosilní
společenstev	společenstvo	k1gNnPc2	společenstvo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
určit	určit	k5eAaPmF	určit
v	v	k7c6	v
jakém	jaký	k3yIgNnSc6	jaký
prostředí	prostředí	k1gNnSc6	prostředí
byla	být	k5eAaImAgFnS	být
tak	tak	k9	tak
která	který	k3yRgFnSc1	který
hornina	hornina	k1gFnSc1	hornina
uložena	uložit	k5eAaPmNgFnS	uložit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
mělkém	mělký	k2eAgNnSc6d1	mělké
moři	moře	k1gNnSc6	moře
nebo	nebo	k8xC	nebo
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
vodě	voda	k1gFnSc6	voda
atd.	atd.	kA	atd.
Geofyzik	geofyzik	k1gMnSc1	geofyzik
a	a	k8xC	a
geolog	geolog	k1gMnSc1	geolog
integrují	integrovat	k5eAaBmIp3nP	integrovat
výsledky	výsledek	k1gInPc7	výsledek
paleontologické	paleontologický	k2eAgFnSc2d1	paleontologická
analýzy	analýza	k1gFnSc2	analýza
dohromady	dohromady	k6eAd1	dohromady
se	s	k7c7	s
seismickými	seismický	k2eAgInPc7d1	seismický
profily	profil	k1gInPc7	profil
<g/>
,	,	kIx,	,
profily	profil	k1gInPc1	profil
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
a	a	k8xC	a
společně	společně	k6eAd1	společně
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
zmapují	zmapovat	k5eAaPmIp3nP	zmapovat
<g/>
.	.	kIx.	.
</s>
<s>
Připraví	připravit	k5eAaPmIp3nS	připravit
strukturní	strukturní	k2eAgFnPc4d1	strukturní
podpovrchové	podpovrchový	k2eAgFnPc4d1	podpovrchová
mapy	mapa	k1gFnPc4	mapa
klíčových	klíčový	k2eAgInPc2d1	klíčový
horizontů	horizont	k1gInPc2	horizont
a	a	k8xC	a
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
možné	možný	k2eAgInPc1d1	možný
prospekty	prospekt	k1gInPc1	prospekt
k	k	k7c3	k
vrtání	vrtání	k1gNnSc3	vrtání
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
úkolem	úkol	k1gInSc7	úkol
geofyzika	geofyzika	k1gFnSc1	geofyzika
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
ze	z	k7c2	z
seismických	seismický	k2eAgInPc2d1	seismický
profilů	profil	k1gInPc2	profil
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
porozitě	porozita	k1gFnSc6	porozita
a	a	k8xC	a
kapalinovém	kapalinový	k2eAgInSc6d1	kapalinový
obsahu	obsah	k1gInSc6	obsah
studovaných	studovaný	k2eAgInPc2d1	studovaný
horizontů	horizont	k1gInPc2	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
potom	potom	k6eAd1	potom
společně	společně	k6eAd1	společně
s	s	k7c7	s
ropným	ropný	k2eAgMnSc7d1	ropný
inženýrem	inženýr	k1gMnSc7	inženýr
a	a	k8xC	a
ekonomem	ekonom	k1gMnSc7	ekonom
odhadne	odhadnout	k5eAaPmIp3nS	odhadnout
možné	možný	k2eAgFnPc4d1	možná
rezervy	rezerva	k1gFnPc4	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Ropný	ropný	k2eAgInSc1d1	ropný
průzkum	průzkum	k1gInSc1	průzkum
neodvratně	odvratně	k6eNd1	odvratně
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
element	element	k1gInSc4	element
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
průzkumný	průzkumný	k2eAgInSc1d1	průzkumný
vrt	vrt	k1gInSc1	vrt
má	mít	k5eAaImIp3nS	mít
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
mezi	mezi	k7c7	mezi
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
toto	tento	k3xDgNnSc1	tento
riziko	riziko	k1gNnSc1	riziko
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
vrty	vrt	k1gInPc1	vrt
jsou	být	k5eAaImIp3nP	být
prvním	první	k4xOgNnSc7	první
stadiem	stadion	k1gNnSc7	stadion
hledání	hledání	k1gNnSc2	hledání
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nové	nový	k2eAgNnSc1d1	nové
ložisko	ložisko	k1gNnSc1	ložisko
objeveno	objeven	k2eAgNnSc1d1	objeveno
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgNnSc7	druhý
stadiem	stadion	k1gNnSc7	stadion
jsou	být	k5eAaImIp3nP	být
podpůrné	podpůrný	k2eAgInPc1d1	podpůrný
vrty	vrt	k1gInPc1	vrt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
přesně	přesně	k6eAd1	přesně
vymezit	vymezit	k5eAaPmF	vymezit
ložisko	ložisko	k1gNnSc4	ložisko
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
parametrů	parametr	k1gInPc2	parametr
o	o	k7c6	o
kolektorových	kolektorový	k2eAgFnPc6d1	kolektorová
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
tým	tým	k1gInSc1	tým
použije	použít	k5eAaPmIp3nS	použít
v	v	k7c6	v
modelování	modelování	k1gNnSc6	modelování
kolektorů	kolektor	k1gInPc2	kolektor
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
připraví	připravit	k5eAaPmIp3nP	připravit
3D	[number]	k4	3D
statický	statický	k2eAgInSc4d1	statický
model	model	k1gInSc4	model
kolektorů	kolektor	k1gMnPc2	kolektor
že	že	k8xS	že
všech	všecek	k3xTgNnPc2	všecek
dostupných	dostupný	k2eAgNnPc2d1	dostupné
geologických	geologický	k2eAgNnPc2d1	geologické
a	a	k8xC	a
geofyzikálních	geofyzikální	k2eAgNnPc2d1	geofyzikální
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
podrobí	podrobit	k5eAaPmIp3nS	podrobit
dynamické	dynamický	k2eAgFnSc3d1	dynamická
simulaci	simulace	k1gFnSc3	simulace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
tok	tok	k1gInSc1	tok
tekutin	tekutina	k1gFnPc2	tekutina
(	(	kIx(	(
<g/>
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
skrz	skrz	k7c4	skrz
kolektor	kolektor	k1gInSc4	kolektor
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
produkce	produkce	k1gFnSc2	produkce
až	až	k9	až
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
dynamické	dynamický	k2eAgFnSc2d1	dynamická
simulace	simulace	k1gFnSc2	simulace
tým	tým	k1gInSc1	tým
potom	potom	k6eAd1	potom
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
optimální	optimální	k2eAgNnSc4d1	optimální
umístění	umístění	k1gNnSc4	umístění
těžebních	těžební	k2eAgInPc2d1	těžební
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stádiu	stádium	k1gNnSc6	stádium
tým	tým	k1gInSc4	tým
těžebních	těžební	k2eAgMnPc2d1	těžební
inženýrů	inženýr	k1gMnPc2	inženýr
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
těžební	těžební	k2eAgFnSc1d1	těžební
povrchová	povrchový	k2eAgFnSc1d1	povrchová
těžební	těžební	k2eAgNnSc4d1	těžební
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
studií	studie	k1gFnPc2	studie
ekonomové	ekonom	k1gMnPc1	ekonom
vypracují	vypracovat	k5eAaPmIp3nP	vypracovat
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
model	model	k1gInSc4	model
peněžního	peněžní	k2eAgInSc2d1	peněžní
toku	tok	k1gInSc2	tok
během	během	k7c2	během
života	život	k1gInSc2	život
ropného	ropný	k2eAgNnSc2d1	ropné
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
management	management	k1gInSc1	management
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
dále	daleko	k6eAd2	daleko
investovat	investovat	k5eAaBmF	investovat
a	a	k8xC	a
přikročit	přikročit	k5eAaPmF	přikročit
k	k	k7c3	k
těžbě	těžba	k1gFnSc3	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
kladné	kladný	k2eAgNnSc1d1	kladné
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
těžebních	těžební	k2eAgMnPc2d1	těžební
inženýrů	inženýr	k1gMnPc2	inženýr
a	a	k8xC	a
postaví	postavit	k5eAaPmIp3nP	postavit
povrchová	povrchový	k2eAgNnPc4d1	povrchové
těžební	těžební	k2eAgNnPc4d1	těžební
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
vrtni	vrtnout	k5eAaPmRp2nS	vrtnout
inženýři	inženýr	k1gMnPc1	inženýr
vyvrtají	vyvrtat	k5eAaPmIp3nP	vyvrtat
těžební	těžební	k2eAgInPc4d1	těžební
vrty	vrt	k1gInPc4	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
vrty	vrt	k1gInPc1	vrt
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
nehodí	hodit	k5eNaPmIp3nS	hodit
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hned	hned	k6eAd1	hned
po	po	k7c6	po
vyvrtání	vyvrtání	k1gNnSc6	vyvrtání
a	a	k8xC	a
ukončení	ukončení	k1gNnSc6	ukončení
karotážních	karotážní	k2eAgNnPc2d1	karotážní
měření	měření	k1gNnPc2	měření
zacementovány	zacementován	k2eAgFnPc1d1	zacementován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
existovaly	existovat	k5eAaImAgFnP	existovat
lokality	lokalita	k1gFnPc1	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ropa	ropa	k1gFnSc1	ropa
v	v	k7c6	v
komerčních	komerční	k2eAgNnPc6d1	komerční
množstvích	množství	k1gNnPc6	množství
přirozeně	přirozeně	k6eAd1	přirozeně
vyvěrala	vyvěrat	k5eAaImAgFnS	vyvěrat
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
ropy	ropa	k1gFnSc2	ropa
získává	získávat	k5eAaImIp3nS	získávat
pomocí	pomocí	k7c2	pomocí
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
těžby	těžba	k1gFnSc2	těžba
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
povrchového	povrchový	k2eAgNnSc2d1	povrchové
dolování	dolování	k1gNnSc2	dolování
(	(	kIx(	(
<g/>
kanadské	kanadský	k2eAgInPc1d1	kanadský
ropné	ropný	k2eAgInPc1d1	ropný
písky	písek	k1gInPc1	písek
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Alberta	Alberta	k1gFnSc1	Alberta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgNnSc1d1	povrchové
dolování	dolování	k1gNnSc1	dolování
je	být	k5eAaImIp3nS	být
aplikovatelné	aplikovatelný	k2eAgNnSc1d1	aplikovatelné
jen	jen	k9	jen
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
těžkou	těžký	k2eAgFnSc4d1	těžká
ropu	ropa	k1gFnSc4	ropa
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
environmentální	environmentální	k2eAgInSc4d1	environmentální
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
dané	daný	k2eAgNnSc4d1	dané
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zajistí	zajistit	k5eAaPmIp3nS	zajistit
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
<g/>
%	%	kIx~	%
vytěžení	vytěžení	k1gNnPc2	vytěžení
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Konvenčním	konvenční	k2eAgInSc7d1	konvenční
způsobem	způsob	k1gInSc7	způsob
získávání	získávání	k1gNnSc2	získávání
zásob	zásoba	k1gFnPc2	zásoba
je	být	k5eAaImIp3nS	být
pomoci	pomoct	k5eAaPmF	pomoct
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
póry	pór	k1gInPc4	pór
v	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
tvořící	tvořící	k2eAgInSc4d1	tvořící
kolektor	kolektor	k1gInSc4	kolektor
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
těchto	tento	k3xDgInPc2	tento
pórů	pór	k1gInPc2	pór
je	být	k5eAaImIp3nS	být
naplněno	naplnit	k5eAaPmNgNnS	naplnit
ropou	ropa	k1gFnSc7	ropa
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ropu	ropa	k1gFnSc4	ropa
těžit	těžit	k5eAaImF	těžit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ropa	ropa	k1gFnSc1	ropa
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
pórech	pór	k1gInPc6	pór
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
jinou	jiný	k2eAgFnSc7d1	jiná
tekutinou	tekutina	k1gFnSc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
tak	tak	k9	tak
stát	stát	k5eAaImF	stát
průsakem	průsak	k1gInSc7	průsak
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
hlubších	hluboký	k2eAgMnPc2d2	hlubší
části	část	k1gFnPc4	část
kolektoru	kolektor	k1gInSc2	kolektor
<g/>
,	,	kIx,	,
rozpínáním	rozpínání	k1gNnSc7	rozpínání
plynové	plynový	k2eAgFnSc2d1	plynová
čepičky	čepička	k1gFnSc2	čepička
atd.	atd.	kA	atd.
Část	část	k1gFnSc1	část
ropy	ropa	k1gFnSc2	ropa
však	však	k9	však
nadále	nadále	k6eAd1	nadále
zůstane	zůstat	k5eAaPmIp3nS	zůstat
v	v	k7c6	v
pórech	pór	k1gInPc6	pór
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
velký	velký	k2eAgInSc1d1	velký
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
zbytek	zbytek	k1gInSc1	zbytek
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
horniny	hornina	k1gFnSc2	hornina
a	a	k8xC	a
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
tekutiny	tekutina	k1gFnSc2	tekutina
která	který	k3yQgFnSc1	který
ropu	ropa	k1gFnSc4	ropa
vytlačuje	vytlačovat	k5eAaImIp3nS	vytlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Vytlačení	vytlačení	k1gNnSc1	vytlačení
vodou	voda	k1gFnSc7	voda
obecně	obecně	k6eAd1	obecně
dává	dávat	k5eAaImIp3nS	dávat
nižší	nízký	k2eAgFnSc4d2	nižší
zbytkovou	zbytkový	k2eAgFnSc4d1	zbytková
saturaci	saturace	k1gFnSc4	saturace
než	než	k8xS	než
vytlačení	vytlačení	k1gNnSc4	vytlačení
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
nebude	být	k5eNaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
stejně	stejně	k6eAd1	stejně
efektivně	efektivně	k6eAd1	efektivně
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
kolektorů	kolektor	k1gInPc2	kolektor
<g/>
.	.	kIx.	.
</s>
<s>
Budou	být	k5eAaImBp3nP	být
části	část	k1gFnPc4	část
kolektorů	kolektor	k1gInPc2	kolektor
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vytlačující	vytlačující	k2eAgFnSc1d1	vytlačující
tekutina	tekutina	k1gFnSc1	tekutina
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
rozsahem	rozsah	k1gInSc7	rozsah
kolektorů	kolektor	k1gMnPc2	kolektor
<g/>
,	,	kIx,	,
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
a	a	k8xC	a
umístěním	umístění	k1gNnSc7	umístění
produkčních	produkční	k2eAgInPc2d1	produkční
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
primárním	primární	k2eAgInSc6d1	primární
způsobu	způsob	k1gInSc6	způsob
těžby	těžba	k1gFnSc2	těžba
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
vytlačení	vytlačení	k1gNnSc3	vytlačení
ropy	ropa	k1gFnSc2	ropa
z	z	k7c2	z
pórů	pór	k1gInPc2	pór
několika	několik	k4yIc2	několik
přírodních	přírodní	k2eAgInPc2d1	přírodní
pohonů	pohon	k1gInPc2	pohon
<g/>
:	:	kIx,	:
Přítok	přítok	k1gInSc1	přítok
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
hlubších	hluboký	k2eAgFnPc2d2	hlubší
částí	část	k1gFnPc2	část
ropné	ropný	k2eAgFnSc2d1	ropná
pasti	past	k1gFnSc2	past
Uvolnění	uvolnění	k1gNnSc1	uvolnění
a	a	k8xC	a
rozpínání	rozpínání	k1gNnSc1	rozpínání
plynu	plyn	k1gInSc2	plyn
rozpuštěného	rozpuštěný	k2eAgInSc2d1	rozpuštěný
v	v	k7c6	v
ropě	ropa	k1gFnSc6	ropa
Rozpínání	rozpínání	k1gNnSc2	rozpínání
plynové	plynový	k2eAgFnSc2d1	plynová
čepičky	čepička	k1gFnSc2	čepička
Kontrakce	kontrakce	k1gFnSc2	kontrakce
horninového	horninový	k2eAgInSc2d1	horninový
skeletu	skelet	k1gInSc2	skelet
kolektoru	kolektor	k1gInSc2	kolektor
Gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
pohonu	pohon	k1gInSc2	pohon
daného	daný	k2eAgInSc2d1	daný
kolektoru	kolektor	k1gInSc2	kolektor
během	během	k7c2	během
těžby	těžba	k1gFnSc2	těžba
určí	určit	k5eAaPmIp3nS	určit
jeho	jeho	k3xOp3gFnSc1	jeho
výtěžnost	výtěžnost	k1gFnSc1	výtěžnost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ropných	ropný	k2eAgInPc2d1	ropný
kolektorů	kolektor	k1gInPc2	kolektor
se	se	k3xPyFc4	se
primární	primární	k2eAgFnSc1d1	primární
výtěžnost	výtěžnost	k1gFnSc1	výtěžnost
typicky	typicky	k6eAd1	typicky
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
maximum	maximum	k1gNnSc4	maximum
75	[number]	k4	75
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ropu	ropa	k1gFnSc4	ropa
je	být	k5eAaImIp3nS	být
optimální	optimální	k2eAgFnSc1d1	optimální
výtěžnost	výtěžnost	k1gFnSc1	výtěžnost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
kolektorů	kolektor	k1gMnPc2	kolektor
s	s	k7c7	s
vodním	vodní	k2eAgInSc7d1	vodní
pohonem	pohon	k1gInSc7	pohon
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
mechanismy	mechanismus	k1gInPc1	mechanismus
rozpínání	rozpínání	k1gNnSc2	rozpínání
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
kolektorů	kolektor	k1gInPc2	kolektor
dají	dát	k5eAaPmIp3nP	dát
výtěžnost	výtěžnost	k1gFnSc4	výtěžnost
menší	malý	k2eAgMnSc1d2	menší
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
tlak	tlak	k1gInSc1	tlak
klesá	klesat	k5eAaImIp3nS	klesat
až	až	k9	až
k	k	k7c3	k
bodu	bod	k1gInSc3	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musí	muset	k5eAaImIp3nS	muset
nastoupit	nastoupit	k5eAaPmF	nastoupit
sekundární	sekundární	k2eAgFnPc4d1	sekundární
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
čerpání	čerpání	k1gNnSc1	čerpání
ropy	ropa	k1gFnSc2	ropa
pomocí	pomocí	k7c2	pomocí
pump	pumpa	k1gFnPc2	pumpa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
udržování	udržování	k1gNnSc1	udržování
podzemního	podzemní	k2eAgInSc2d1	podzemní
tlaku	tlak	k1gInSc2	tlak
vodní	vodní	k2eAgFnSc7d1	vodní
injektáží	injektáž	k1gFnSc7	injektáž
<g/>
,	,	kIx,	,
zpětným	zpětný	k2eAgNnSc7d1	zpětné
pumpováním	pumpování	k1gNnSc7	pumpování
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sekundárními	sekundární	k2eAgFnPc7d1	sekundární
metodami	metoda	k1gFnPc7	metoda
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
vytěžit	vytěžit	k5eAaPmF	vytěžit
dalších	další	k2eAgFnPc2d1	další
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
%	%	kIx~	%
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
kolektoru	kolektor	k1gInSc6	kolektor
<g/>
.	.	kIx.	.
</s>
<s>
Terciární	terciární	k2eAgFnPc1d1	terciární
metody	metoda	k1gFnPc1	metoda
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
ani	ani	k8xC	ani
sekundární	sekundární	k2eAgFnPc1d1	sekundární
metody	metoda	k1gFnPc1	metoda
nestačí	stačit	k5eNaBmIp3nP	stačit
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
těžba	těžba	k1gFnSc1	těžba
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
aktuální	aktuální	k2eAgFnSc6d1	aktuální
ceně	cena	k1gFnSc6	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
výši	výše	k1gFnSc4	výše
těžebních	těžební	k2eAgInPc2d1	těžební
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
viskozity	viskozita	k1gFnSc2	viskozita
zbývající	zbývající	k2eAgFnSc2d1	zbývající
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
injektáží	injektáž	k1gFnSc7	injektáž
horké	horký	k2eAgFnSc2d1	horká
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
získávané	získávaný	k2eAgFnSc2d1	získávaná
často	často	k6eAd1	často
kogenerací	kogenerace	k1gFnSc7	kogenerace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektřina	elektřina	k1gFnSc1	elektřina
a	a	k8xC	a
odpadní	odpadní	k2eAgNnSc1d1	odpadní
teplo	teplo	k1gNnSc1	teplo
je	být	k5eAaImIp3nS	být
využito	využít	k5eAaPmNgNnS	využít
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
ropa	ropa	k1gFnSc1	ropa
rozehřívá	rozehřívat	k5eAaImIp3nS	rozehřívat
zapálením	zapálení	k1gNnSc7	zapálení
části	část	k1gFnSc2	část
ropného	ropný	k2eAgNnSc2d1	ropné
ložiska	ložisko	k1gNnSc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
injektáž	injektáž	k1gFnSc4	injektáž
detergentů	detergent	k1gInPc2	detergent
<g/>
.	.	kIx.	.
</s>
<s>
Terciární	terciární	k2eAgFnPc1d1	terciární
metody	metoda	k1gFnPc1	metoda
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
vytěžit	vytěžit	k5eAaPmF	vytěžit
dalších	další	k2eAgFnPc2d1	další
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
%	%	kIx~	%
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
nalezišti	naleziště	k1gNnSc6	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgNnPc1d1	uvedené
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
průměrná	průměrný	k2eAgNnPc1d1	průměrné
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
celková	celkový	k2eAgFnSc1d1	celková
vytěžitelnost	vytěžitelnost	k1gFnSc1	vytěžitelnost
naleziště	naleziště	k1gNnSc2	naleziště
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
80	[number]	k4	80
%	%	kIx~	%
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
lehkou	lehký	k2eAgFnSc4d1	lehká
ropu	ropa	k1gFnSc4	ropa
<g/>
)	)	kIx)	)
do	do	k7c2	do
5	[number]	k4	5
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
těžké	těžký	k2eAgFnSc2d1	těžká
ropy	ropa	k1gFnSc2	ropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Design	design	k1gInSc1	design
těžebních	těžební	k2eAgNnPc2d1	těžební
zařízení	zařízení	k1gNnPc2	zařízení
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
pole	pole	k1gNnSc1	pole
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
nebo	nebo	k8xC	nebo
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
o	o	k7c4	o
pole	pole	k1gFnPc4	pole
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
,	,	kIx,	,
těžební	těžební	k2eAgInPc4d1	těžební
vrty	vrt	k1gInPc4	vrt
jsou	být	k5eAaImIp3nP	být
typický	typický	k2eAgMnSc1d1	typický
vrtány	vrtán	k2eAgFnPc1d1	vrtána
kolmo	kolmo	k6eAd1	kolmo
<g/>
.	.	kIx.	.
</s>
<s>
Kazdy	Kazda	k1gMnPc4	Kazda
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
vrtnou	vrtný	k2eAgFnSc4d1	vrtná
hlavici	hlavice	k1gFnSc4	hlavice
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yQgFnSc2	který
vede	vést	k5eAaImIp3nS	vést
potrubí	potrubí	k1gNnSc1	potrubí
k	k	k7c3	k
centrální	centrální	k2eAgFnSc3d1	centrální
stanici	stanice	k1gFnSc3	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
tekutiny	tekutina	k1gFnPc1	tekutina
odděleny	oddělit	k5eAaPmNgFnP	oddělit
a	a	k8xC	a
zpracovány	zpracovat	k5eAaPmNgFnP	zpracovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Vytěžená	vytěžený	k2eAgFnSc1d1	vytěžená
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
vstřikuje	vstřikovat	k5eAaImIp3nS	vstřikovat
zpět	zpět	k6eAd1	zpět
pod	pod	k7c4	pod
zem	zem	k1gFnSc4	zem
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
velmi	velmi	k6eAd1	velmi
slaná	slaný	k2eAgFnSc1d1	slaná
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
poslány	poslat	k5eAaPmNgFnP	poslat
dalším	další	k2eAgNnSc7d1	další
potrubím	potrubí	k1gNnSc7	potrubí
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pole	pole	k1gNnSc4	pole
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
k	k	k7c3	k
těžbě	těžba	k1gFnSc3	těžba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
platformy	platforma	k1gFnPc1	platforma
buď	buď	k8xC	buď
plovoucí	plovoucí	k2eAgNnSc1d1	plovoucí
nebo	nebo	k8xC	nebo
zabudované	zabudovaný	k2eAgNnSc1d1	zabudované
do	do	k7c2	do
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
podmořské	podmořský	k2eAgInPc1d1	podmořský
těžební	těžební	k2eAgInPc1d1	těžební
systémy	systém	k1gInPc1	systém
sestávající	sestávající	k2eAgInPc1d1	sestávající
z	z	k7c2	z
individuálních	individuální	k2eAgFnPc2d1	individuální
hlavic	hlavice	k1gFnPc2	hlavice
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
centrální	centrální	k2eAgFnSc7d1	centrální
stanicí	stanice	k1gFnSc7	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tekutiny	tekutina	k1gFnPc1	tekutina
separují	separovat	k5eAaBmIp3nP	separovat
a	a	k8xC	a
buď	buď	k8xC	buď
skladují	skladovat	k5eAaImIp3nP	skladovat
nebo	nebo	k8xC	nebo
posílají	posílat	k5eAaImIp3nP	posílat
potrubím	potrubí	k1gNnSc7	potrubí
k	k	k7c3	k
pevnině	pevnina	k1gFnSc3	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
ropný	ropný	k2eAgInSc1d1	ropný
průmysl	průmysl	k1gInSc1	průmysl
definuje	definovat	k5eAaBmIp3nS	definovat
množství	množství	k1gNnSc4	množství
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
obsazených	obsazený	k2eAgInPc2d1	obsazený
v	v	k7c6	v
pórech	pór	k1gInPc6	pór
kolektorové	kolektorový	k2eAgFnSc2d1	kolektorová
horniny	hornina	k1gFnSc2	hornina
jako	jako	k8xS	jako
Zdroje	zdroj	k1gInPc1	zdroj
(	(	kIx(	(
<g/>
resource	resourka	k1gFnSc6	resourka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
termín	termín	k1gInSc1	termín
Zásoby	zásoba	k1gFnSc2	zásoba
(	(	kIx(	(
<g/>
reserves	reserves	k1gInSc1	reserves
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ta	ten	k3xDgFnSc1	ten
množství	množství	k1gNnSc4	množství
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
komerčně	komerčně	k6eAd1	komerčně
vytěžena	vytěžit	k5eAaPmNgFnS	vytěžit
ze	z	k7c2	z
známého	známý	k2eAgNnSc2d1	známé
ložiska	ložisko	k1gNnSc2	ložisko
od	od	k7c2	od
určitého	určitý	k2eAgNnSc2d1	určité
data	datum	k1gNnSc2	datum
dopředu	dopředu	k6eAd1	dopředu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zásoby	zásoba	k1gFnPc4	zásoba
se	se	k3xPyFc4	se
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
skladované	skladovaný	k2eAgFnSc2d1	skladovaná
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
zásob	zásoba	k1gFnPc2	zásoba
jsou	být	k5eAaImIp3nP	být
revidovány	revidován	k2eAgInPc1d1	revidován
s	s	k7c7	s
přibýváním	přibývání	k1gNnSc7	přibývání
geologických	geologický	k2eAgFnPc2d1	geologická
dát	dát	k5eAaPmF	dát
a	a	k8xC	a
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
ložisku	ložisko	k1gNnSc6	ložisko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
podle	podle	k7c2	podle
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
podmínkách	podmínka	k1gFnPc6	podmínka
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
daná	daný	k2eAgFnSc1d1	daná
společnost	společnost	k1gFnSc1	společnost
musí	muset	k5eAaImIp3nS	muset
zrevidovat	zrevidovat	k5eAaPmF	zrevidovat
svůj	svůj	k3xOyFgInSc4	svůj
stav	stav	k1gInSc4	stav
zásob	zásoba	k1gFnPc2	zásoba
na	na	k7c4	na
nižší	nízký	k2eAgFnSc4d2	nižší
<g/>
,	,	kIx,	,
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
<g/>
-li	i	k?	-li
cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
část	část	k1gFnSc4	část
zdrojů	zdroj	k1gInPc2	zdroj
v	v	k7c6	v
ložisku	ložisko	k1gNnSc6	ložisko
již	již	k9	již
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
rezervami	rezerva	k1gFnPc7	rezerva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odporuje	odporovat	k5eAaImIp3nS	odporovat
definici	definice	k1gFnSc4	definice
ekonomičnosti	ekonomičnost	k1gFnSc2	ekonomičnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
zásoby	zásoba	k1gFnSc2	zásoba
dokázané	dokázaná	k1gFnSc2	dokázaná
<g/>
,	,	kIx,	,
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
a	a	k8xC	a
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Dokázané	dokázaný	k2eAgFnPc1d1	dokázaná
zásoby	zásoba	k1gFnPc1	zásoba
(	(	kIx(	(
<g/>
zásoby	zásoba	k1gFnPc1	zásoba
P	P	kA	P
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ta	ten	k3xDgNnPc1	ten
množství	množství	k1gNnPc1	množství
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
odhadnout	odhadnout	k5eAaPmF	odhadnout
s	s	k7c7	s
přiměřenou	přiměřený	k2eAgFnSc7d1	přiměřená
mírou	míra	k1gFnSc7	míra
jistoty	jistota	k1gFnSc2	jistota
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dostupných	dostupný	k2eAgNnPc2d1	dostupné
dat	datum	k1gNnPc2	datum
jako	jako	k8xS	jako
komerčně	komerčně	k6eAd1	komerčně
vytěžitelné	vytěžitelný	k2eAgInPc1d1	vytěžitelný
od	od	k7c2	od
určitého	určitý	k2eAgNnSc2d1	určité
data	datum	k1gNnSc2	datum
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
s	s	k7c7	s
existující	existující	k2eAgFnSc7d1	existující
těžební	těžební	k2eAgFnSc7d1	těžební
technologií	technologie	k1gFnSc7	technologie
<g/>
,	,	kIx,	,
z	z	k7c2	z
již	již	k6eAd1	již
objevených	objevený	k2eAgInPc2d1	objevený
kolektorů	kolektor	k1gInPc2	kolektor
<g/>
,	,	kIx,	,
za	za	k7c2	za
současných	současný	k2eAgFnPc2d1	současná
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
vládních	vládní	k2eAgInPc2d1	vládní
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Dokázané	dokázaný	k2eAgFnPc1d1	dokázaná
zásoby	zásoba	k1gFnPc1	zásoba
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgFnPc2	který
trhy	trh	k1gInPc1	trh
danou	daný	k2eAgFnSc4d1	daná
společnost	společnost	k1gFnSc4	společnost
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobné	pravděpodobný	k2eAgFnPc1d1	pravděpodobná
zásoby	zásoba	k1gFnPc1	zásoba
(	(	kIx(	(
<g/>
zásoby	zásoba	k1gFnPc1	zásoba
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
neprokázané	prokázaný	k2eNgFnPc1d1	neprokázaná
rezervy	rezerva	k1gFnPc1	rezerva
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
existence	existence	k1gFnSc1	existence
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
vytěžitelnost	vytěžitelnost	k1gFnSc1	vytěžitelnost
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
dostupných	dostupný	k2eAgNnPc2d1	dostupné
dat	datum	k1gNnPc2	datum
odhadnuty	odhadnout	k5eAaPmNgFnP	odhadnout
na	na	k7c6	na
nejméně	málo	k6eAd3	málo
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Možné	možný	k2eAgFnPc1d1	možná
zásoby	zásoba	k1gFnPc1	zásoba
(	(	kIx(	(
<g/>
zásoby	zásoba	k1gFnPc1	zásoba
P	P	kA	P
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
neprokázané	prokázaný	k2eNgFnPc1d1	neprokázaná
rezervy	rezerva	k1gFnPc1	rezerva
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
existence	existence	k1gFnSc1	existence
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
vytěžitelnost	vytěžitelnost	k1gFnSc1	vytěžitelnost
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
dostupných	dostupný	k2eAgNnPc2d1	dostupné
dat	datum	k1gNnPc2	datum
odhadnuty	odhadnut	k2eAgInPc1d1	odhadnut
jako	jako	k8xS	jako
méně	málo	k6eAd2	málo
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
(	(	kIx(	(
<g/>
do	do	k7c2	do
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
praxi	praxe	k1gFnSc4	praxe
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
minimální	minimální	k2eAgFnSc4d1	minimální
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
alespoň	alespoň	k9	alespoň
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
průzkumu	průzkum	k1gInSc2	průzkum
a	a	k8xC	a
těžby	těžba	k1gFnSc2	těžba
z	z	k7c2	z
nekonvenčních	konvenční	k2eNgNnPc2d1	nekonvenční
ložisek	ložisko	k1gNnPc2	ložisko
přišla	přijít	k5eAaPmAgFnS	přijít
Společnost	společnost	k1gFnSc1	společnost
Ropných	ropný	k2eAgMnPc2d1	ropný
Inženýrů	inženýr	k1gMnPc2	inženýr
(	(	kIx(	(
<g/>
SPE	SPE	kA	SPE
<g/>
)	)	kIx)	)
s	s	k7c7	s
definicemi	definice	k1gFnPc7	definice
tzv.	tzv.	kA	tzv.
Potenciálních	potenciální	k2eAgInPc2d1	potenciální
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
Prospektivních	prospektivní	k2eAgInPc2d1	prospektivní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgInPc1d1	potenciální
zdroje	zdroj	k1gInPc1	zdroj
jsou	být	k5eAaImIp3nP	být
ta	ten	k3xDgNnPc1	ten
množství	množství	k1gNnPc1	množství
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
datu	datum	k1gNnSc3	datum
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
vytěžitelná	vytěžitelný	k2eAgFnSc1d1	vytěžitelná
z	z	k7c2	z
již	již	k6eAd1	již
objevených	objevený	k2eAgNnPc2d1	objevené
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
která	který	k3yRgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
komerčně	komerčně	k6eAd1	komerčně
realizovatelná	realizovatelný	k2eAgFnSc1d1	realizovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
ropu	ropa	k1gFnSc4	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
technicky	technicky	k6eAd1	technicky
vytěžitelné	vytěžitelný	k2eAgNnSc1d1	vytěžitelné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
Prospektivní	prospektivní	k2eAgInPc1d1	prospektivní
zdroje	zdroj	k1gInPc1	zdroj
jsou	být	k5eAaImIp3nP	být
ta	ten	k3xDgNnPc1	ten
množství	množství	k1gNnPc1	množství
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
datu	datum	k1gNnSc3	datum
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
vytěžitelná	vytěžitelný	k2eAgFnSc1d1	vytěžitelná
z	z	k7c2	z
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
budou	být	k5eAaImBp3nP	být
objevena	objevit	k5eAaPmNgNnP	objevit
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
i	i	k8xC	i
ropné	ropný	k2eAgInPc1d1	ropný
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
základním	základní	k2eAgNnSc7d1	základní
palivem	palivo	k1gNnSc7	palivo
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
plastů	plast	k1gInPc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
i	i	k9	i
některé	některý	k3yIgInPc1	některý
léky	lék	k1gInPc1	lék
a	a	k8xC	a
pesticidy	pesticid	k1gInPc1	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
chudší	chudý	k2eAgFnPc4d2	chudší
země	zem	k1gFnPc4	zem
používají	používat	k5eAaImIp3nP	používat
ropné	ropný	k2eAgInPc1d1	ropný
produkty	produkt	k1gInPc1	produkt
také	také	k9	také
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
(	(	kIx(	(
<g/>
asi	asi	k9	asi
7	[number]	k4	7
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
samotným	samotný	k2eAgNnSc7d1	samotné
zpracováním	zpracování	k1gNnSc7	zpracování
je	být	k5eAaImIp3nS	být
ropa	ropa	k1gFnSc1	ropa
po	po	k7c6	po
transportu	transport	k1gInSc6	transport
do	do	k7c2	do
rafinérie	rafinérie	k1gFnSc2	rafinérie
kontinuálně	kontinuálně	k6eAd1	kontinuálně
odsolována	odsolován	k2eAgFnSc1d1	odsolován
praním	praní	k1gNnSc7	praní
upravenou	upravený	k2eAgFnSc7d1	upravená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
oddělením	oddělení	k1gNnSc7	oddělení
solanky	solanka	k1gFnSc2	solanka
v	v	k7c6	v
elektrostatickém	elektrostatický	k2eAgInSc6d1	elektrostatický
odlučovači	odlučovač	k1gInSc6	odlučovač
za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
i	i	k8xC	i
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
zpracování	zpracování	k1gNnSc2	zpracování
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
kontinuální	kontinuální	k2eAgFnSc2d1	kontinuální
rektifikace	rektifikace	k1gFnSc2	rektifikace
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
uváděná	uváděný	k2eAgFnSc1d1	uváděná
"	"	kIx"	"
<g/>
frakční	frakční	k2eAgFnSc1d1	frakční
destilace	destilace	k1gFnSc1	destilace
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
ropy	ropa	k1gFnSc2	ropa
dostatečně	dostatečně	k6eAd1	dostatečně
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kolonách	kolona	k1gFnPc6	kolona
odděleny	oddělit	k5eAaPmNgInP	oddělit
při	při	k7c6	při
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
skupiny	skupina	k1gFnPc1	skupina
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInPc2	jejich
bodů	bod	k1gInPc2	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Nejlehčí	lehký	k2eAgInPc1d3	nejlehčí
plynné	plynný	k2eAgInPc1d1	plynný
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
jsou	být	k5eAaImIp3nP	být
methan	methan	k1gInSc1	methan
<g/>
,	,	kIx,	,
ethan	ethan	k1gInSc1	ethan
<g/>
,	,	kIx,	,
propan	propan	k1gInSc1	propan
<g/>
,	,	kIx,	,
butan	butan	k1gInSc1	butan
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
automobilového	automobilový	k2eAgNnSc2d1	automobilové
paliva	palivo	k1gNnSc2	palivo
LPG	LPG	kA	LPG
<g/>
.	.	kIx.	.
</s>
<s>
Petroléter	Petroléter	k1gMnSc1	Petroléter
tvoří	tvořit	k5eAaImIp3nS	tvořit
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
řetězce	řetězec	k1gInSc2	řetězec
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
tv	tv	k?	tv
asi	asi	k9	asi
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
rozpouštědla	rozpouštědlo	k1gNnPc4	rozpouštědlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
chemickém	chemický	k2eAgNnSc6d1	chemické
čištění	čištění	k1gNnSc6	čištění
oděvů	oděv	k1gInPc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
frakce	frakce	k1gFnPc1	frakce
jsou	být	k5eAaImIp3nP	být
benzín	benzín	k1gInSc4	benzín
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
petrolej	petrolej	k1gInSc1	petrolej
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
letecké	letecký	k2eAgNnSc1d1	letecké
palivo	palivo	k1gNnSc1	palivo
pro	pro	k7c4	pro
trysková	tryskový	k2eAgNnPc4d1	tryskové
letadla	letadlo	k1gNnPc4	letadlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
plynový	plynový	k2eAgInSc1d1	plynový
olej	olej	k1gInSc1	olej
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
nafta	nafta	k1gFnSc1	nafta
a	a	k8xC	a
lehký	lehký	k2eAgInSc1d1	lehký
topný	topný	k2eAgInSc1d1	topný
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mazut	mazut	k1gInSc1	mazut
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podrobuje	podrobovat	k5eAaImIp3nS	podrobovat
vakuové	vakuový	k2eAgFnSc3d1	vakuová
destilaci	destilace	k1gFnSc3	destilace
za	za	k7c2	za
sníženého	snížený	k2eAgInSc2d1	snížený
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
těžké	těžký	k2eAgInPc1d1	těžký
topné	topný	k2eAgInPc1d1	topný
oleje	olej	k1gInPc1	olej
od	od	k7c2	od
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
.	.	kIx.	.
</s>
<s>
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
řetězci	řetězec	k1gInPc7	řetězec
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
35	[number]	k4	35
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hydrokrakováním	hydrokrakování	k1gNnSc7	hydrokrakování
rozštěpeny	rozštěpit	k5eAaPmNgInP	rozštěpit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
mazací	mazací	k2eAgInPc4d1	mazací
oleje	olej	k1gInPc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
získané	získaný	k2eAgInPc1d1	získaný
produkty	produkt	k1gInPc1	produkt
jsou	být	k5eAaImIp3nP	být
nejprve	nejprve	k6eAd1	nejprve
podrobeny	podroben	k2eAgFnPc1d1	podrobena
hydrorafinaci	hydrorafinace	k1gFnSc6	hydrorafinace
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
selektivně	selektivně	k6eAd1	selektivně
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
organicky	organicky	k6eAd1	organicky
vázaná	vázaný	k2eAgFnSc1d1	vázaná
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
odpovídající	odpovídající	k2eAgFnPc1d1	odpovídající
sloučeniny	sloučenina	k1gFnPc1	sloučenina
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
NH3	NH3	k1gFnSc2	NH3
a	a	k8xC	a
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
odstraněny	odstraněn	k2eAgFnPc4d1	odstraněna
např.	např.	kA	např.
výpirkou	výpirka	k1gFnSc7	výpirka
MEA	MEA	kA	MEA
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgInPc1d1	získaný
produkty	produkt	k1gInPc1	produkt
se	se	k3xPyFc4	se
podrobují	podrobovat	k5eAaImIp3nP	podrobovat
mnoha	mnoho	k4c3	mnoho
dalším	další	k2eAgInPc3d1	další
procesům	proces	k1gInPc3	proces
např.	např.	kA	např.
reformování	reformování	k1gNnSc1	reformování
benzínů	benzín	k1gInPc2	benzín
(	(	kIx(	(
<g/>
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
reformát	reformát	k1gMnSc1	reformát
–	–	k?	–
tj.	tj.	kA	tj.
zvýšení	zvýšení	k1gNnSc4	zvýšení
obsah	obsah	k1gInSc1	obsah
aromátů	aromát	k1gInPc2	aromát
<g/>
,	,	kIx,	,
složka	složka	k1gFnSc1	složka
benzínového	benzínový	k2eAgInSc2d1	benzínový
poolu	pool	k1gInSc2	pool
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hydrokrakování	hydrokrakování	k1gNnSc1	hydrokrakování
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jen	jen	k9	jen
kapalné	kapalný	k2eAgInPc4d1	kapalný
produkty	produkt	k1gInPc4	produkt
výhodných	výhodný	k2eAgFnPc2d1	výhodná
vlastností	vlastnost	k1gFnPc2	vlastnost
např.	např.	kA	např.
pro	pro	k7c4	pro
navazující	navazující	k2eAgFnSc4d1	navazující
ethylenovou	ethylenový	k2eAgFnSc4d1	ethylenová
pyrolýzu	pyrolýza	k1gFnSc4	pyrolýza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fluidní	fluidní	k2eAgNnSc4d1	fluidní
katalytické	katalytický	k2eAgNnSc4d1	katalytické
krakování	krakování	k1gNnSc4	krakování
(	(	kIx(	(
<g/>
=	=	kIx~	=
FCC	FCC	kA	FCC
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
zpracováním	zpracování	k1gNnSc7	zpracování
např.	např.	kA	např.
některých	některý	k3yIgFnPc2	některý
olejových	olejový	k2eAgFnPc2d1	olejová
frakcí	frakce	k1gFnPc2	frakce
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
benzíny	benzín	k1gInPc4	benzín
a	a	k8xC	a
též	též	k9	též
plyny	plyn	k1gInPc1	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izomerace	izomerace	k1gFnSc1	izomerace
benzínů	benzín	k1gInPc2	benzín
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgMnSc1d1	tvořící
tzv.	tzv.	kA	tzv.
izomerát	izomerát	k1gInSc4	izomerát
=	=	kIx~	=
složka	složka	k1gFnSc1	složka
benzínového	benzínový	k2eAgInSc2d1	benzínový
poolu	pool	k1gInSc2	pool
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgInPc4d1	důležitý
procesy	proces	k1gInPc4	proces
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
visbreaking	visbreaking	k1gInSc1	visbreaking
a	a	k8xC	a
TCC	TCC	kA	TCC
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jmenované	jmenovaný	k2eAgInPc1d1	jmenovaný
procesy	proces	k1gInPc1	proces
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
zpracování	zpracování	k1gNnSc2	zpracování
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zpracování	zpracování	k1gNnSc1	zpracování
těžkých	těžký	k2eAgFnPc2d1	těžká
ropných	ropný	k2eAgFnPc2d1	ropná
frakcí	frakce	k1gFnPc2	frakce
(	(	kIx(	(
<g/>
těžké	těžký	k2eAgInPc1d1	těžký
oleje	olej	k1gInPc1	olej
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
transformace	transformace	k1gFnSc1	transformace
na	na	k7c4	na
frakce	frakce	k1gFnPc4	frakce
lehčí	lehčit	k5eAaImIp3nP	lehčit
(	(	kIx(	(
<g/>
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
benzíny	benzín	k1gInPc1	benzín
<g/>
,	,	kIx,	,
atmosférické	atmosférický	k2eAgInPc1d1	atmosférický
oleje	olej	k1gInPc1	olej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provozování	provozování	k1gNnSc1	provozování
procesů	proces	k1gInPc2	proces
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
zpracování	zpracování	k1gNnSc2	zpracování
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
"	"	kIx"	"
<g/>
vytěžování	vytěžování	k1gNnSc4	vytěžování
<g/>
"	"	kIx"	"
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zastoupení	zastoupení	k1gNnSc1	zastoupení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
frakcí	frakce	k1gFnPc2	frakce
v	v	k7c6	v
ropě	ropa	k1gFnSc6	ropa
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
potřebám	potřeba	k1gFnPc3	potřeba
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Asfalty	asfalt	k1gInPc1	asfalt
jsou	být	k5eAaImIp3nP	být
oxidovány	oxidovat	k5eAaBmNgInP	oxidovat
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
oxidace	oxidace	k1gFnSc2	oxidace
asfaltů	asfalt	k1gInPc2	asfalt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
vhodnějších	vhodný	k2eAgFnPc2d2	vhodnější
vlastností	vlastnost	k1gFnPc2	vlastnost
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těžkých	těžký	k2eAgFnPc2d1	těžká
frakcí	frakce	k1gFnPc2	frakce
jsou	být	k5eAaImIp3nP	být
extrahovány	extrahován	k2eAgInPc4d1	extrahován
parafiny	parafin	k1gInPc4	parafin
superkritickou	superkritický	k2eAgFnSc7d1	superkritická
extrakcí	extrakce	k1gFnSc7	extrakce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
procesu	proces	k1gInSc2	proces
extrakce	extrakce	k1gFnSc2	extrakce
parafinů	parafin	k1gInPc2	parafin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poslední	poslední	k2eAgInSc4d1	poslední
důležitý	důležitý	k2eAgInSc4d1	důležitý
proces	proces	k1gInSc4	proces
uveďme	uvést	k5eAaPmRp1nP	uvést
parciální	parciální	k2eAgFnSc4d1	parciální
oxidaci	oxidace	k1gFnSc4	oxidace
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
též	též	k9	též
zplyňování	zplyňování	k1gNnSc1	zplyňování
mazutu	mazut	k1gInSc2	mazut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
využívá	využívat	k5eAaPmIp3nS	využívat
výhradně	výhradně	k6eAd1	výhradně
jen	jen	k9	jen
na	na	k7c4	na
likvidaci	likvidace	k1gFnSc4	likvidace
těch	ten	k3xDgMnPc2	ten
nejtěžších	těžký	k2eAgMnPc2d3	nejtěžší
zcela	zcela	k6eAd1	zcela
nezpracovatelných	zpracovatelný	k2eNgMnPc2d1	nezpracovatelný
zbytků	zbytek	k1gInPc2	zbytek
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc4d1	různá
smoly	smola	k1gFnPc4	smola
z	z	k7c2	z
visbreakingu	visbreaking	k1gInSc2	visbreaking
<g/>
)	)	kIx)	)
a	a	k8xC	a
produkuje	produkovat	k5eAaImIp3nS	produkovat
syntézní	syntézní	k2eAgInSc4d1	syntézní
plyn	plyn	k1gInSc4	plyn
bohatý	bohatý	k2eAgInSc4d1	bohatý
především	především	k6eAd1	především
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
rafinérie	rafinérie	k1gFnSc1	rafinérie
bez	bez	k7c2	bez
vodíku	vodík	k1gInSc2	vodík
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
provozu	provoz	k1gInSc2	provoz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
95	[number]	k4	95
%	%	kIx~	%
veškerých	veškerý	k3xTgFnPc2	veškerý
potravin	potravina	k1gFnPc2	potravina
je	být	k5eAaImIp3nS	být
pěstováno	pěstovat	k5eAaImNgNnS	pěstovat
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
ropy	ropa	k1gFnSc2	ropa
90	[number]	k4	90
%	%	kIx~	%
dopravy	doprava	k1gFnSc2	doprava
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
ropné	ropný	k2eAgInPc1d1	ropný
deriváty	derivát	k1gInPc1	derivát
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
používají	používat	k5eAaImIp3nP	používat
benzín	benzín	k1gInSc4	benzín
či	či	k8xC	či
naftu	nafta	k1gFnSc4	nafta
(	(	kIx(	(
<g/>
oboje	oboj	k1gFnSc2	oboj
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
silniční	silniční	k2eAgNnPc1d1	silniční
či	či	k8xC	či
terénní	terénní	k2eAgNnPc1d1	terénní
vozidla	vozidlo	k1gNnPc1	vozidlo
se	s	k7c7	s
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
(	(	kIx(	(
<g/>
automobil	automobil	k1gInSc1	automobil
<g/>
,	,	kIx,	,
autobus	autobus	k1gInSc1	autobus
<g/>
,	,	kIx,	,
motorka	motorka	k1gFnSc1	motorka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dieselové	dieselový	k2eAgInPc4d1	dieselový
vlaky	vlak	k1gInPc4	vlak
<g/>
,	,	kIx,	,
loďě	loďě	k1gMnPc4	loďě
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnPc1	letadlo
nebo	nebo	k8xC	nebo
vrtulníky	vrtulník	k1gInPc1	vrtulník
95	[number]	k4	95
%	%	kIx~	%
veškerého	veškerý	k3xTgNnSc2	veškerý
vyráběného	vyráběný	k2eAgNnSc2d1	vyráběné
zboží	zboží	k1gNnSc2	zboží
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
výrobu	výroba	k1gFnSc4	výroba
ropu	ropa	k1gFnSc4	ropa
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
kalorii	kalorie	k1gFnSc4	kalorie
běžně	běžně	k6eAd1	běžně
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
potravin	potravina	k1gFnPc2	potravina
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
10	[number]	k4	10
kalorií	kalorie	k1gFnPc2	kalorie
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
jednoho	jeden	k4xCgInSc2	jeden
typického	typický	k2eAgInSc2d1	typický
počítače	počítač	k1gInSc2	počítač
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
ropa	ropa	k1gFnSc1	ropa
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
desetinásobku	desetinásobek	k1gInSc2	desetinásobek
jeho	jeho	k3xOp3gFnSc2	jeho
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Ropa	ropa	k1gFnSc1	ropa
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
využívána	využívat	k5eAaImNgFnS	využívat
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
masové	masový	k2eAgFnSc2d1	masová
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
přepravy	přeprava	k1gFnSc2	přeprava
a	a	k8xC	a
pěstování	pěstování	k1gNnSc2	pěstování
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
zemědělství	zemědělství	k1gNnSc1	zemědělství
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
petrochemického	petrochemický	k2eAgInSc2d1	petrochemický
průmyslu	průmysl	k1gInSc2	průmysl
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
stalo	stát	k5eAaPmAgNnS	stát
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
použití	použití	k1gNnSc6	použití
herbicidů	herbicid	k1gInPc2	herbicid
<g/>
,	,	kIx,	,
pesticidů	pesticid	k1gInPc2	pesticid
a	a	k8xC	a
umělých	umělý	k2eAgNnPc2d1	umělé
hnojiv	hnojivo	k1gNnPc2	hnojivo
vyráběných	vyráběný	k2eAgNnPc2d1	vyráběné
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
běžné	běžný	k2eAgNnSc1d1	běžné
použití	použití	k1gNnSc1	použití
přírodních	přírodní	k2eAgNnPc2d1	přírodní
hnojiv	hnojivo	k1gNnPc2	hnojivo
a	a	k8xC	a
přirozených	přirozený	k2eAgInPc2d1	přirozený
pesticidů	pesticid	k1gInPc2	pesticid
a	a	k8xC	a
herbicidů	herbicid	k1gInPc2	herbicid
–	–	k?	–
tedy	tedy	k8xC	tedy
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
ropných	ropný	k2eAgInPc2d1	ropný
a	a	k8xC	a
chemických	chemický	k2eAgInPc2d1	chemický
produktů	produkt	k1gInPc2	produkt
–	–	k?	–
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
metodou	metoda	k1gFnSc7	metoda
tzv.	tzv.	kA	tzv.
biozemědělství	biozemědělství	k1gNnSc2	biozemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
velmi	velmi	k6eAd1	velmi
levné	levný	k2eAgFnSc2d1	levná
energie	energie	k1gFnSc2	energie
měla	mít	k5eAaImAgFnS	mít
zejména	zejména	k9	zejména
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
desetiletí	desetiletí	k1gNnPc2	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
architekturu	architektura	k1gFnSc4	architektura
měst	město	k1gNnPc2	město
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc4d1	dopravní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
zemí	zem	k1gFnPc2	zem
Prvního	první	k4xOgInSc2	první
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Urbanistické	urbanistický	k2eAgInPc4d1	urbanistický
návrhy	návrh	k1gInPc4	návrh
měst	město	k1gNnPc2	město
s	s	k7c7	s
typickými	typický	k2eAgInPc7d1	typický
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
obytnými	obytný	k2eAgNnPc7d1	obytné
předměstími	předměstí	k1gNnPc7	předměstí
(	(	kIx(	(
<g/>
nejvýrazněji	výrazně	k6eAd3	výrazně
viděnými	viděný	k2eAgFnPc7d1	viděná
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
braly	brát	k5eAaImAgFnP	brát
dostupnost	dostupnost	k1gFnSc4	dostupnost
levné	levný	k2eAgFnSc2d1	levná
transportní	transportní	k2eAgFnSc2d1	transportní
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ropy	ropa	k1gFnSc2	ropa
do	do	k7c2	do
úvahy	úvaha	k1gFnSc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
také	také	k9	také
měla	mít	k5eAaImAgFnS	mít
potírat	potírat	k5eAaImF	potírat
hlad	hlad	k1gInSc4	hlad
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
food	food	k1gMnSc1	food
from	from	k1gMnSc1	from
oil	oil	k?	oil
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
ideu	idea	k1gFnSc4	idea
levných	levný	k2eAgFnPc2d1	levná
bílkovin	bílkovina	k1gFnPc2	bílkovina
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
Alfred	Alfred	k1gMnSc1	Alfred
Champagnat	Champagnat	k1gMnSc1	Champagnat
dostal	dostat	k5eAaPmAgMnS	dostat
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
cenu	cena	k1gFnSc4	cena
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Ropný	ropný	k2eAgInSc1d1	ropný
průmysl	průmysl	k1gInSc1	průmysl
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
ropu	ropa	k1gFnSc4	ropa
podle	podle	k7c2	podle
jejího	její	k3xOp3gInSc2	její
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
West	West	k2eAgInSc1d1	West
Texas	Texas	k1gInSc1	Texas
Intermediate	Intermediat	k1gInSc5	Intermediat
<g/>
,	,	kIx,	,
WTI	WTI	kA	WTI
nebo	nebo	k8xC	nebo
Brent	Brent	k?	Brent
<g/>
)	)	kIx)	)
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
hustoty	hustota	k1gFnSc2	hustota
(	(	kIx(	(
<g/>
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
,	,	kIx,	,
light	light	k1gInSc1	light
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
,	,	kIx,	,
intermediate	intermediat	k1gInSc5	intermediat
a	a	k8xC	a
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
,	,	kIx,	,
heavy	heava	k1gFnPc1	heava
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
rafinérie	rafinérie	k1gFnPc4	rafinérie
ji	on	k3xPp3gFnSc4	on
<g />
.	.	kIx.	.
</s>
<s>
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
sladkou	sladký	k2eAgFnSc7d1	sladká
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sweet	sweet	k1gInSc1	sweet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
relativně	relativně	k6eAd1	relativně
málo	málo	k4c1	málo
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kyselou	kyselá	k1gFnSc4	kyselá
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sour	sour	k1gInSc1	sour
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
ropa	ropa	k1gFnSc1	ropa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
0,5	[number]	k4	0,5
%	%	kIx~	%
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
náročnější	náročný	k2eAgNnSc1d2	náročnější
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhověla	vyhovět	k5eAaPmAgFnS	vyhovět
současným	současný	k2eAgFnPc3d1	současná
normám	norma	k1gFnPc3	norma
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
světové	světový	k2eAgInPc1d1	světový
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
směsná	směsný	k2eAgFnSc1d1	směsná
ropa	ropa	k1gFnSc1	ropa
Brent	Brent	k?	Brent
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
ropy	ropa	k1gFnSc2	ropa
z	z	k7c2	z
nalezišť	naleziště	k1gNnPc2	naleziště
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
prodávána	prodáván	k2eAgFnSc1d1	prodávána
ropa	ropa	k1gFnSc1	ropa
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
spotřebu	spotřeba	k1gFnSc4	spotřeba
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k2eAgInSc1d1	West
Texas	Texas	k1gInSc1	Texas
Intermediate	Intermediat	k1gInSc5	Intermediat
(	(	kIx(	(
<g/>
WTI	WTI	kA	WTI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Dubai	Dubai	k6eAd1	Dubai
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
blízkovýchodní	blízkovýchodní	k2eAgFnSc1d1	blízkovýchodní
ropa	ropa	k1gFnSc1	ropa
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
asijsko-pacifickou	asijskoacifický	k2eAgFnSc4d1	asijsko-pacifická
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tapis	Tapis	k1gFnSc1	Tapis
(	(	kIx(	(
<g/>
z	z	k7c2	z
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
lehká	lehký	k2eAgFnSc1d1	lehká
ropa	ropa	k1gFnSc1	ropa
z	z	k7c2	z
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Minas	Minas	k1gInSc1	Minas
(	(	kIx(	(
<g/>
z	z	k7c2	z
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
těžká	těžký	k2eAgFnSc1d1	těžká
ropa	ropa	k1gFnSc1	ropa
z	z	k7c2	z
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Koš	koš	k1gInSc1	koš
OPEC	opéct	k5eAaPmRp2nSwK	opéct
zahrnující	zahrnující	k2eAgInPc4d1	zahrnující
druhy	druh	k1gInPc4	druh
Arab	Arab	k1gMnSc1	Arab
Light	Light	k1gMnSc1	Light
(	(	kIx(	(
<g/>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bonny	Bonn	k1gInPc1	Bonn
Light	Light	k1gInSc1	Light
(	(	kIx(	(
<g/>
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fateh	Fateh	k1gInSc1	Fateh
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Isthmus	Isthmus	k1gInSc1	Isthmus
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Minas	Minas	k1gMnSc1	Minas
(	(	kIx(	(
<g/>
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saharan	Saharan	k1gInSc1	Saharan
Blend	Blend	k1gInSc1	Blend
(	(	kIx(	(
<g/>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tia	Tia	k1gMnSc1	Tia
Juana	Juan	k1gMnSc2	Juan
Light	Light	k1gMnSc1	Light
(	(	kIx(	(
<g/>
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
OPEC	opéct	k5eAaPmRp2nSwK	opéct
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
udržet	udržet	k5eAaPmF	udržet
cenu	cena	k1gFnSc4	cena
koše	koš	k1gInSc2	koš
OPEC	opéct	k5eAaPmRp2nSwK	opéct
v	v	k7c6	v
předem	předem	k6eAd1	předem
daném	daný	k2eAgNnSc6d1	dané
rozmezí	rozmezí	k1gNnSc6	rozmezí
pomocí	pomocí	k7c2	pomocí
zvyšování	zvyšování	k1gNnSc2	zvyšování
a	a	k8xC	a
snižování	snižování	k1gNnSc2	snižování
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
typu	typ	k1gInSc2	typ
koš	koš	k1gInSc1	koš
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
jak	jak	k8xS	jak
z	z	k7c2	z
lehkých	lehký	k2eAgNnPc2d1	lehké
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
těžkých	těžký	k2eAgInPc2d1	těžký
druhů	druh	k1gInPc2	druh
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
Brent	Brent	k?	Brent
i	i	k9	i
WTI	WTI	kA	WTI
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc4d2	veliký
obsah	obsah	k1gInSc4	obsah
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgInPc1d1	uvedený
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
klasickou	klasický	k2eAgFnSc4d1	klasická
"	"	kIx"	"
<g/>
konvenční	konvenční	k2eAgFnSc4d1	konvenční
<g/>
"	"	kIx"	"
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
nekonvenční	konvenční	k2eNgFnSc2d1	nekonvenční
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
živice	živice	k1gFnSc1	živice
v	v	k7c6	v
dehtových	dehtový	k2eAgInPc6d1	dehtový
píscích	písek	k1gInPc6	písek
a	a	k8xC	a
kerogen	kerogen	k1gInSc4	kerogen
v	v	k7c6	v
ropných	ropný	k2eAgFnPc6d1	ropná
břidlicích	břidlice	k1gFnPc6	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Venezuele	Venezuela	k1gFnSc6	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
podíl	podíl	k1gInSc1	podíl
zatím	zatím	k6eAd1	zatím
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k9	jen
5	[number]	k4	5
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vysoké	vysoký	k2eAgFnSc2d1	vysoká
energetické	energetický	k2eAgFnSc2d1	energetická
náročnosti	náročnost	k1gFnSc2	náročnost
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
se	se	k3xPyFc4	se
ale	ale	k9	ale
staly	stát	k5eAaPmAgFnP	stát
akceptovatelné	akceptovatelný	k2eAgFnPc1d1	akceptovatelná
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
výrazně	výrazně	k6eAd1	výrazně
cena	cena	k1gFnSc1	cena
konvenční	konvenční	k2eAgFnSc2d1	konvenční
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
dekáda	dekáda	k1gFnSc1	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
těžba	těžba	k1gFnSc1	těžba
nekonvenční	konvenční	k2eNgFnSc2d1	nekonvenční
ropy	ropa	k1gFnSc2	ropa
rychle	rychle	k6eAd1	rychle
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ropný	ropný	k2eAgInSc4d1	ropný
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Hubbertova	Hubbertův	k2eAgFnSc1d1	Hubbertova
teorie	teorie	k1gFnSc1	teorie
ropného	ropný	k2eAgInSc2d1	ropný
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
peak	peak	k1gInSc1	peak
oil	oil	k?	oil
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
dlouhodobými	dlouhodobý	k2eAgFnPc7d1	dlouhodobá
předpověďmi	předpověď	k1gFnPc7	předpověď
spotřeby	spotřeba	k1gFnSc2	spotřeba
a	a	k8xC	a
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jelikož	jelikož	k8xS	jelikož
zdroje	zdroj	k1gInPc1	zdroj
ropy	ropa	k1gFnSc2	ropa
jsou	být	k5eAaImIp3nP	být
neobnovitelné	obnovitelný	k2eNgFnPc1d1	neobnovitelná
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
úroveň	úroveň	k1gFnSc4	úroveň
těžby	těžba	k1gFnSc2	těžba
ropy	ropa	k1gFnSc2	ropa
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
a	a	k8xC	a
poté	poté	k6eAd1	poté
začít	začít	k5eAaPmF	začít
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
sleduje	sledovat	k5eAaImIp3nS	sledovat
tzv.	tzv.	kA	tzv.
Hubbertovu	Hubbertův	k2eAgFnSc4d1	Hubbertova
křivku	křivka	k1gFnSc4	křivka
(	(	kIx(	(
<g/>
podobnou	podobný	k2eAgFnSc4d1	podobná
Gaussově	Gaussův	k2eAgFnSc6d1	Gaussova
křivce	křivka	k1gFnSc6	křivka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
diskutované	diskutovaný	k2eAgNnSc1d1	diskutované
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
teorii	teorie	k1gFnSc6	teorie
datum	datum	k1gNnSc4	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc4	tento
vrchol	vrchol	k1gInSc4	vrchol
nastat	nastat	k5eAaPmF	nastat
<g/>
.	.	kIx.	.
</s>
<s>
Geolog	geolog	k1gMnSc1	geolog
M.	M.	kA	M.
King	King	k1gMnSc1	King
Hubbert	Hubbert	k1gMnSc1	Hubbert
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
sledoval	sledovat	k5eAaImAgMnS	sledovat
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zpozoroval	zpozorovat	k5eAaPmAgMnS	zpozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvíc	nejvíc	k6eAd1	nejvíc
amerických	americký	k2eAgNnPc2d1	americké
ropných	ropný	k2eAgNnPc2d1	ropné
nalezišť	naleziště	k1gNnPc2	naleziště
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
počátkem	počátkem	k7c2	počátkem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
USA	USA	kA	USA
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
vrcholu	vrchol	k1gInSc6	vrchol
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
skutečně	skutečně	k6eAd1	skutečně
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
USA	USA	kA	USA
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
převážně	převážně	k6eAd1	převážně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Dna	dna	k1gFnSc1	dna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
těžba	těžba	k1gFnSc1	těžba
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
však	však	k9	však
opět	opět	k6eAd1	opět
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
USA	USA	kA	USA
začala	začít	k5eAaPmAgFnS	začít
upadat	upadat	k5eAaImF	upadat
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
ceny	cena	k1gFnPc4	cena
ropy	ropa	k1gFnSc2	ropa
diktovat	diktovat	k5eAaImF	diktovat
ropný	ropný	k2eAgInSc4d1	ropný
kartel	kartel	k1gInSc4	kartel
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
třetí	třetí	k4xOgFnSc3	třetí
ropné	ropný	k2eAgFnSc3d1	ropná
krizi	krize	k1gFnSc3	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
vrcholu	vrchol	k1gInSc6	vrchol
těžby	těžba	k1gFnSc2	těžba
mnoho	mnoho	k4c4	mnoho
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Severní	severní	k2eAgNnSc1d1	severní
moře	moře	k1gNnSc1	moře
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
klesá	klesat	k5eAaImIp3nS	klesat
produkce	produkce	k1gFnSc1	produkce
tempem	tempo	k1gNnSc7	tempo
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
%	%	kIx~	%
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
i	i	k9	i
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvě	dva	k4xCgFnPc1	dva
její	její	k3xOp3gNnPc4	její
největší	veliký	k2eAgNnPc4d3	veliký
ropná	ropný	k2eAgNnPc4d1	ropné
pole	pole	k1gNnPc4	pole
začínají	začínat	k5eAaImIp3nP	začínat
být	být	k5eAaImF	být
vytěžena	vytěžen	k2eAgFnSc1d1	vytěžena
a	a	k8xC	a
mexická	mexický	k2eAgFnSc1d1	mexická
ropná	ropný	k2eAgFnSc1d1	ropná
společnost	společnost	k1gFnSc1	společnost
Pemex	Pemex	k1gInSc1	Pemex
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cantarellské	Cantarellský	k2eAgNnSc1d1	Cantarellský
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
ropných	ropný	k2eAgFnPc2d1	ropná
polí	pole	k1gFnPc2	pole
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
a	a	k8xC	a
poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
těžba	těžba	k1gFnSc1	těžba
klesat	klesat	k5eAaImF	klesat
o	o	k7c4	o
14	[number]	k4	14
%	%	kIx~	%
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nebude	být	k5eNaImBp3nS	být
pokles	pokles	k1gInSc1	pokles
těžby	těžba	k1gFnSc2	těžba
tak	tak	k6eAd1	tak
prudký	prudký	k2eAgInSc1d1	prudký
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
ropném	ropný	k2eAgInSc6d1	ropný
vrcholu	vrchol	k1gInSc6	vrchol
ropa	ropa	k1gFnSc1	ropa
náhle	náhle	k6eAd1	náhle
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
těžba	těžba	k1gFnSc1	těžba
bude	být	k5eAaImBp3nS	být
postupně	postupně	k6eAd1	postupně
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ropa	ropa	k1gFnSc1	ropa
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
začala	začít	k5eAaPmAgNnP	začít
těžit	těžit	k5eAaImF	těžit
asi	asi	k9	asi
před	před	k7c7	před
150	[number]	k4	150
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
pokračovat	pokračovat	k5eAaImF	pokračovat
i	i	k9	i
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
v	v	k7c6	v
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zmenšujících	zmenšující	k2eAgInPc6d1	zmenšující
objemech	objem	k1gInPc6	objem
těžené	těžený	k2eAgFnSc2d1	těžená
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
znamenají	znamenat	k5eAaImIp3nP	znamenat
budoucí	budoucí	k2eAgInSc4d1	budoucí
razantní	razantní	k2eAgInSc4d1	razantní
nárůst	nárůst	k1gInSc4	nárůst
její	její	k3xOp3gFnSc2	její
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ropnému	ropný	k2eAgInSc3d1	ropný
vrcholu	vrchol	k1gInSc3	vrchol
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
také	také	k9	také
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
Konec	konec	k1gInSc1	konec
levné	levný	k2eAgFnSc2d1	levná
ropy	ropa	k1gFnSc2	ropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
nejvíc	nejvíc	k6eAd1	nejvíc
asi	asi	k9	asi
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
zmapování	zmapování	k1gNnSc2	zmapování
světových	světový	k2eAgFnPc2d1	světová
ropných	ropný	k2eAgFnPc2d1	ropná
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
předpovědět	předpovědět	k5eAaPmF	předpovědět
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastane	nastat	k5eAaPmIp3nS	nastat
vrchol	vrchol	k1gInSc4	vrchol
těžby	těžba	k1gFnSc2	těžba
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Vycházejíce	vycházet	k5eAaImSgFnP	vycházet
z	z	k7c2	z
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
o	o	k7c4	o
produkci	produkce	k1gFnSc4	produkce
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
stoupenci	stoupenec	k1gMnPc1	stoupenec
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
předpovídali	předpovídat	k5eAaImAgMnP	předpovídat
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrchol	vrchol	k1gInSc1	vrchol
světové	světový	k2eAgFnSc2d1	světová
těžby	těžba	k1gFnSc2	těžba
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
nebo	nebo	k8xC	nebo
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
předpovědi	předpověď	k1gFnPc1	předpověď
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
recesí	recese	k1gFnSc7	recese
začátku	začátek	k1gInSc2	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
důsledku	důsledek	k1gInSc6	důsledek
se	se	k3xPyFc4	se
růst	růst	k1gInSc1	růst
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
ropě	ropa	k1gFnSc6	ropa
snížil	snížit	k5eAaPmAgMnS	snížit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
oddálil	oddálit	k5eAaPmAgInS	oddálit
vrchol	vrchol	k1gInSc4	vrchol
její	její	k3xOp3gFnSc2	její
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
Hubbertova	Hubbertův	k2eAgFnSc1d1	Hubbertova
lokální	lokální	k2eAgFnSc1d1	lokální
teorie	teorie	k1gFnSc1	teorie
vzata	vzat	k2eAgFnSc1d1	vzata
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
až	až	k6eAd1	až
po	po	k7c6	po
vrcholu	vrchol	k1gInSc6	vrchol
těžby	těžba	k1gFnSc2	těžba
v	v	k7c6	v
USA	USA	kA	USA
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
teorie	teorie	k1gFnSc1	teorie
útlumu	útlum	k1gInSc2	útlum
světové	světový	k2eAgFnSc2d1	světová
těžby	těžba	k1gFnSc2	těžba
bude	být	k5eAaImBp3nS	být
uznána	uznat	k5eAaPmNgFnS	uznat
teprve	teprve	k6eAd1	teprve
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
tak	tak	k9	tak
skutečně	skutečně	k6eAd1	skutečně
stane	stanout	k5eAaPmIp3nS	stanout
i	i	k9	i
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
podporuje	podporovat	k5eAaImIp3nS	podporovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
křivka	křivka	k1gFnSc1	křivka
objevů	objev	k1gInPc2	objev
nových	nový	k2eAgNnPc2d1	nové
ropných	ropný	k2eAgNnPc2d1	ropné
nalezišť	naleziště	k1gNnPc2	naleziště
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc6	vrchol
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neustále	neustále	k6eAd1	neustále
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
lidstvo	lidstvo	k1gNnSc4	lidstvo
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc4	rok
více	hodně	k6eAd2	hodně
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
než	než	k8xS	než
činí	činit	k5eAaImIp3nS	činit
nové	nový	k2eAgInPc4d1	nový
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
argumentem	argument	k1gInSc7	argument
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
začala	začít	k5eAaPmAgFnS	začít
dramaticky	dramaticky	k6eAd1	dramaticky
klesat	klesat	k5eAaImF	klesat
příprava	příprava	k1gFnSc1	příprava
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
začít	začít	k5eAaPmF	začít
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
a	a	k8xC	a
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ropa	ropa	k1gFnSc1	ropa
je	být	k5eAaImIp3nS	být
těžitelná	těžitelný	k2eAgFnSc1d1	těžitelná
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
dostupných	dostupný	k2eAgNnPc6d1	dostupné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
statistik	statistika	k1gFnPc2	statistika
kartelu	kartel	k1gInSc2	kartel
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
,	,	kIx,	,
že	že	k8xS	že
tzv.	tzv.	kA	tzv.
lehká	lehký	k2eAgFnSc1d1	lehká
sladká	sladký	k2eAgFnSc1d1	sladká
ropa	ropa	k1gFnSc1	ropa
(	(	kIx(	(
<g/>
light	light	k2eAgInSc1d1	light
sweet	sweet	k1gInSc1	sweet
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nyní	nyní	k6eAd1	nyní
nachází	nacházet	k5eAaImIp3nS	nacházet
za	za	k7c7	za
vrcholem	vrchol	k1gInSc7	vrchol
těžby	těžba	k1gFnSc2	těžba
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
produkce	produkce	k1gFnSc2	produkce
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
nejžádanější	žádaný	k2eAgNnSc1d3	nejžádanější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nejsnáze	snadno	k6eAd3	snadno
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
bude	být	k5eAaImBp3nS	být
také	také	k9	také
nejdříve	dříve	k6eAd3	dříve
vyčerpán	vyčerpat	k5eAaPmNgInS	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
nedostatek	nedostatek	k1gInSc1	nedostatek
rafinérských	rafinérský	k2eAgFnPc2d1	rafinérská
kapacit	kapacita	k1gFnPc2	kapacita
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
zdrojů	zdroj	k1gInPc2	zdroj
dán	dát	k5eAaPmNgMnS	dát
právě	právě	k9	právě
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rafinérie	rafinérie	k1gFnPc1	rafinérie
nejsou	být	k5eNaImIp3nP	být
připraveny	připravit	k5eAaPmNgFnP	připravit
na	na	k7c4	na
zpracovávání	zpracovávání	k1gNnSc4	zpracovávání
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
těžší	těžký	k2eAgFnSc2d2	těžší
ropy	ropa	k1gFnSc2	ropa
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
podotýkají	podotýkat	k5eAaImIp3nP	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
předpověď	předpověď	k1gFnSc1	předpověď
ropného	ropný	k2eAgInSc2d1	ropný
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
USA	USA	kA	USA
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
žádnou	žádný	k3yNgFnSc4	žádný
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
tamních	tamní	k2eAgFnPc2d1	tamní
ropných	ropný	k2eAgFnPc2d1	ropná
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
odráží	odrážet	k5eAaImIp3nS	odrážet
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
nevýhodnost	nevýhodnost	k1gFnSc4	nevýhodnost
těžby	těžba	k1gFnSc2	těžba
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
importem	import	k1gInSc7	import
ropy	ropa	k1gFnSc2	ropa
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
opakovaně	opakovaně	k6eAd1	opakovaně
nesprávné	správný	k2eNgFnPc4d1	nesprávná
předpovědi	předpověď	k1gFnPc4	předpověď
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
ropného	ropný	k2eAgInSc2d1	ropný
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
je	být	k5eAaImIp3nS	být
aplikace	aplikace	k1gFnSc1	aplikace
teorie	teorie	k1gFnSc2	teorie
na	na	k7c4	na
svět	svět	k1gInSc4	svět
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
neproveditelná	proveditelný	k2eNgNnPc4d1	neproveditelné
kvůli	kvůli	k7c3	kvůli
složitým	složitý	k2eAgInPc3d1	složitý
obchodním	obchodní	k2eAgInPc3d1	obchodní
a	a	k8xC	a
politickým	politický	k2eAgInPc3d1	politický
vztahům	vztah	k1gInPc3	vztah
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Cenou	cena	k1gFnSc7	cena
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
rozumí	rozumět	k5eAaImIp3nS	rozumět
cena	cena	k1gFnSc1	cena
WTI	WTI	kA	WTI
<g/>
/	/	kIx~	/
<g/>
Light	Light	k1gMnSc1	Light
Crude	Crud	k1gInSc5	Crud
ropy	ropa	k1gFnPc1	ropa
obchodované	obchodovaný	k2eAgFnPc4d1	obchodovaná
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
komoditní	komoditní	k2eAgFnSc6d1	komoditní
burze	burza	k1gFnSc6	burza
(	(	kIx(	(
<g/>
NYMEX	NYMEX	kA	NYMEX
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
typu	typ	k1gInSc2	typ
Brent	Brent	k?	Brent
obchodované	obchodovaný	k2eAgNnSc1d1	obchodované
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
ropné	ropný	k2eAgFnSc6d1	ropná
burze	burza	k1gFnSc6	burza
(	(	kIx(	(
<g/>
International	International	k1gFnSc6	International
Petroleum	Petroleum	k1gNnSc4	Petroleum
Exchange	Exchange	k1gFnPc2	Exchange
<g/>
,	,	kIx,	,
IPE	IPE	kA	IPE
<g/>
)	)	kIx)	)
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
velmi	velmi	k6eAd1	velmi
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
druhu	druh	k1gInSc6	druh
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
například	například	k6eAd1	například
její	její	k3xOp3gFnSc7	její
hustotou	hustota	k1gFnSc7	hustota
a	a	k8xC	a
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
původu	původ	k1gInSc6	původ
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
neprodává	prodávat	k5eNaImIp3nS	prodávat
na	na	k7c6	na
burzách	burza	k1gFnPc6	burza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
přímých	přímý	k2eAgFnPc2d1	přímá
transakcí	transakce	k1gFnPc2	transakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
cenami	cena	k1gFnPc7	cena
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
IPE	IPE	kA	IPE
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
65	[number]	k4	65
%	%	kIx~	%
veškerých	veškerý	k3xTgInPc2	veškerý
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
burzovní	burzovní	k2eAgFnSc2d1	burzovní
ceny	cena	k1gFnSc2	cena
ropy	ropa	k1gFnSc2	ropa
typu	typ	k1gInSc2	typ
Brent	Brent	k?	Brent
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
důležité	důležitý	k2eAgFnPc1d1	důležitá
referenční	referenční	k2eAgFnPc1d1	referenční
burzovní	burzovní	k2eAgFnPc1d1	burzovní
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
Dubai	Dubai	k1gNnSc1	Dubai
<g/>
,	,	kIx,	,
Tapis	Tapis	k1gInSc1	Tapis
a	a	k8xC	a
koš	koš	k1gInSc1	koš
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cenu	cena	k1gFnSc4	cena
ropy	ropa	k1gFnSc2	ropa
určuje	určovat	k5eAaImIp3nS	určovat
kartel	kartel	k1gInSc1	kartel
OPEC	opéct	k5eAaPmRp2nSwK	opéct
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
skutečná	skutečný	k2eAgFnSc1d1	skutečná
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
2,1	[number]	k4	2,1
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
barel	barel	k1gInSc4	barel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názory	názor	k1gInPc1	názor
však	však	k9	však
nezohledňují	zohledňovat	k5eNaImIp3nP	zohledňovat
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
hledání	hledání	k1gNnSc4	hledání
nových	nový	k2eAgNnPc2d1	nové
ropných	ropný	k2eAgNnPc2d1	ropné
nalezišť	naleziště	k1gNnPc2	naleziště
a	a	k8xC	a
investice	investice	k1gFnPc4	investice
nutné	nutný	k2eAgFnPc1d1	nutná
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
ropě	ropa	k1gFnSc6	ropa
velmi	velmi	k6eAd1	velmi
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
globální	globální	k2eAgFnSc6d1	globální
makroekonomické	makroekonomický	k2eAgFnSc6d1	makroekonomická
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
také	také	k9	také
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
činitelem	činitel	k1gInSc7	činitel
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
ekonomů	ekonom	k1gMnPc2	ekonom
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysoké	vysoký	k2eAgFnPc1d1	vysoká
ceny	cena	k1gFnPc1	cena
ropy	ropa	k1gFnSc2	ropa
mají	mít	k5eAaImIp3nP	mít
zpětně	zpětně	k6eAd1	zpětně
velký	velký	k2eAgInSc4d1	velký
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
jiní	jiný	k2eAgMnPc1d1	jiný
ekonomičtí	ekonomický	k2eAgMnPc1d1	ekonomický
odborníci	odborník	k1gMnPc1	odborník
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
světové	světový	k2eAgNnSc1d1	světové
hospodářství	hospodářství	k1gNnSc1	hospodářství
méně	málo	k6eAd2	málo
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
ropě	ropa	k1gFnSc6	ropa
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
ropných	ropný	k2eAgInPc2d1	ropný
šoků	šok	k1gInPc2	šok
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
historického	historický	k2eAgNnSc2d1	historické
maxima	maximum	k1gNnSc2	maximum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
ropného	ropný	k2eAgInSc2d1	ropný
šoku	šok	k1gInSc2	šok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
cenách	cena	k1gFnPc6	cena
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
očištěna	očistit	k5eAaPmNgFnS	očistit
od	od	k7c2	od
inflace	inflace	k1gFnSc2	inflace
<g/>
)	)	kIx)	)
až	až	k9	až
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
90	[number]	k4	90
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
barel	barel	k1gInSc4	barel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
klesla	klesnout	k5eAaPmAgFnS	klesnout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
otevření	otevření	k1gNnSc2	otevření
nových	nový	k2eAgFnPc2d1	nová
ropných	ropný	k2eAgFnPc2d1	ropná
polí	pole	k1gFnPc2	pole
mimo	mimo	k7c4	mimo
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgNnSc1d1	nedávné
minimum	minimum	k1gNnSc1	minimum
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
12	[number]	k4	12
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
barel	barel	k1gInSc4	barel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
asijská	asijský	k2eAgFnSc1d1	asijská
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
snížila	snížit	k5eAaPmAgFnS	snížit
poptávku	poptávka	k1gFnSc4	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
začala	začít	k5eAaPmAgFnS	začít
stoupat	stoupat	k5eAaImF	stoupat
až	až	k9	až
na	na	k7c4	na
maximum	maximum	k1gNnSc4	maximum
70,85	[number]	k4	70,85
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
barel	barel	k1gInSc4	barel
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hurikánu	hurikán	k1gInSc2	hurikán
Katrina	Katrina	k1gFnSc1	Katrina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Současná	současný	k2eAgFnSc1d1	současná
ropná	ropný	k2eAgFnSc1d1	ropná
krize	krize	k1gFnSc1	krize
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
způsobena	způsobit	k5eAaPmNgFnS	způsobit
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
poptávkou	poptávka	k1gFnSc7	poptávka
<g/>
,	,	kIx,	,
nejvíc	hodně	k6eAd3	hodně
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
kapacitou	kapacita	k1gFnSc7	kapacita
rafinérií	rafinérie	k1gFnSc7	rafinérie
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
Hubbertovy	Hubbertův	k2eAgFnSc2d1	Hubbertova
teorie	teorie	k1gFnSc2	teorie
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
přibližujeme	přibližovat	k5eAaImIp1nP	přibližovat
datu	datum	k1gNnSc3	datum
konečného	konečný	k2eAgInSc2d1	konečný
vrcholu	vrchol	k1gInSc2	vrchol
těžby	těžba	k1gFnSc2	těžba
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
tzv.	tzv.	kA	tzv.
rezervní	rezervní	k2eAgFnSc1d1	rezervní
těžební	těžební	k2eAgFnSc1d1	těžební
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
množství	množství	k1gNnSc4	množství
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
mohou	moct	k5eAaImIp3nP	moct
těžařské	těžařský	k2eAgFnPc4d1	těžařská
firmy	firma	k1gFnPc4	firma
krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
zvýšit	zvýšit	k5eAaPmF	zvýšit
svoji	svůj	k3xOyFgFnSc4	svůj
produkci	produkce	k1gFnSc4	produkce
v	v	k7c6	v
případě	případ	k1gInSc6	případ
převisu	převis	k1gInSc2	převis
poptávky	poptávka	k1gFnSc2	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rezervní	rezervní	k2eAgFnSc1d1	rezervní
těžební	těžební	k2eAgFnSc1d1	těžební
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
skutečně	skutečně	k6eAd1	skutečně
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
historickém	historický	k2eAgNnSc6d1	historické
minimu	minimum	k1gNnSc6	minimum
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouhá	pouhý	k2eAgNnPc4d1	pouhé
dvě	dva	k4xCgNnPc4	dva
procenta	procento	k1gNnPc4	procento
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
produkce	produkce	k1gFnSc2	produkce
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	let	k1gInPc7	let
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
lokální	lokální	k2eAgInPc1d1	lokální
výpadky	výpadek	k1gInPc1	výpadek
těžby	těžba	k1gFnSc2	těžba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
hurikán	hurikán	k1gInSc1	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
velké	velký	k2eAgInPc4d1	velký
výkyvy	výkyv	k1gInPc4	výkyv
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
Hubbertovy	Hubbertův	k2eAgFnSc2d1	Hubbertova
teorie	teorie	k1gFnSc2	teorie
pokles	pokles	k1gInSc4	pokles
rezervní	rezervní	k2eAgFnSc1d1	rezervní
těžební	těžební	k2eAgFnSc1d1	těžební
i	i	k8xC	i
rafinérské	rafinérský	k2eAgFnPc1d1	rafinérská
kapacity	kapacita	k1gFnPc1	kapacita
připisují	připisovat	k5eAaImIp3nP	připisovat
chronicky	chronicky	k6eAd1	chronicky
nedostatečným	dostatečný	k2eNgFnPc3d1	nedostatečná
investicím	investice	k1gFnPc3	investice
kvůli	kvůli	k7c3	kvůli
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nízké	nízký	k2eAgFnSc3d1	nízká
ceně	cena	k1gFnSc3	cena
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
očekávaného	očekávaný	k2eAgInSc2d1	očekávaný
zásahu	zásah	k1gInSc2	zásah
USA	USA	kA	USA
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
117	[number]	k4	117
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
barel	barel	k1gInSc4	barel
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgFnPc4d1	rozhodující
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
datu	datum	k1gNnSc3	datum
8.9	[number]	k4	8.9
<g/>
.2013	.2013	k4	.2013
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
cena	cena	k1gFnSc1	cena
na	na	k7c4	na
cca	cca	kA	cca
116	[number]	k4	116
USD	USD	kA	USD
za	za	k7c4	za
barel	barel	k1gInSc4	barel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
cen	cena	k1gFnPc2	cena
byl	být	k5eAaImAgInS	být
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
za	za	k7c4	za
týden	týden	k1gInSc4	týden
o	o	k7c6	o
cca	cca	kA	cca
3	[number]	k4	3
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
i	i	k9	i
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
kursu	kurs	k1gInSc6	kurs
USD	USD	kA	USD
proti	proti	k7c3	proti
Euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
klesla	klesnout	k5eAaPmAgFnS	klesnout
cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
na	na	k7c4	na
cca	cca	kA	cca
90	[number]	k4	90
USD	USD	kA	USD
za	za	k7c4	za
barel	barel	k1gInSc4	barel
a	a	k8xC	a
do	do	k7c2	do
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
cca	cca	kA	cca
60	[number]	k4	60
USD	USD	kA	USD
za	za	k7c4	za
barel	barel	k1gInSc4	barel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
dějin	dějiny	k1gFnPc2	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
bylo	být	k5eAaImAgNnS	být
vytěženo	vytěžit	k5eAaPmNgNnS	vytěžit
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
miliard	miliarda	k4xCgFnPc2	miliarda
barelů	barel	k1gInPc2	barel
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
současného	současný	k2eAgInSc2d1	současný
objemu	objem	k1gInSc2	objem
těžby	těžba	k1gFnSc2	těžba
vystačí	vystačit	k5eAaBmIp3nP	vystačit
známé	známý	k2eAgFnPc1d1	známá
zásoby	zásoba	k1gFnPc1	zásoba
ropy	ropa	k1gFnSc2	ropa
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
42	[number]	k4	42
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
v	v	k7c6	v
předpokladu	předpoklad	k1gInSc6	předpoklad
stálé	stálý	k2eAgFnSc2d1	stálá
úrovně	úroveň	k1gFnSc2	úroveň
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
tvořila	tvořit	k5eAaImAgFnS	tvořit
celková	celkový	k2eAgFnSc1d1	celková
těžba	těžba	k1gFnSc1	těžba
3	[number]	k4	3
821	[number]	k4	821
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ropné	ropný	k2eAgFnSc2d1	ropná
země	zem	k1gFnPc1	zem
ve	v	k7c6	v
sdružení	sdružení	k1gNnSc6	sdružení
OPEC	opéct	k5eAaPmRp2nSwK	opéct
vyprodukovaly	vyprodukovat	k5eAaPmAgFnP	vyprodukovat
41,2	[number]	k4	41,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Lehká	lehký	k2eAgFnSc1d1	lehká
ropa	ropa	k1gFnSc1	ropa
tvořila	tvořit	k5eAaImAgFnS	tvořit
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
%	%	kIx~	%
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
těžká	těžkat	k5eAaImIp3nS	těžkat
53	[number]	k4	53
%	%	kIx~	%
a	a	k8xC	a
těžká	těžký	k2eAgFnSc1d1	těžká
ropa	ropa	k1gFnSc1	ropa
14	[number]	k4	14
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kyselá	kyselý	k2eAgFnSc1d1	kyselá
ropa	ropa	k1gFnSc1	ropa
(	(	kIx(	(
<g/>
sour	sour	k1gInSc1	sour
<g/>
)	)	kIx)	)
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
tvořila	tvořit	k5eAaImAgFnS	tvořit
59	[number]	k4	59
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
spotřebitelé	spotřebitel	k1gMnPc1	spotřebitel
ropy	ropa	k1gFnSc2	ropa
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
USA	USA	kA	USA
(	(	kIx(	(
<g/>
842,9	[number]	k4	842,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evropská	evropský	k2eAgFnSc1d1	Evropská
Unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
670,8	[number]	k4	670,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
404,6	[number]	k4	404,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
197,6	[number]	k4	197,6
<g />
.	.	kIx.	.
</s>
<s>
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
148,5	[number]	k4	148,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
124,9	[number]	k4	124,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
(	(	kIx(	(
<g/>
121,8	[number]	k4	121,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
113,9	[number]	k4	113,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc1	Brazílie
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
104,3	[number]	k4	104,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
ropě	ropa	k1gFnSc6	ropa
stoupá	stoupat	k5eAaImIp3nS	stoupat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
asi	asi	k9	asi
o	o	k7c4	o
2	[number]	k4	2
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
v	v	k7c6	v
USA	USA	kA	USA
spotřeba	spotřeba	k1gFnSc1	spotřeba
ropy	ropa	k1gFnSc2	ropa
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dovezla	dovézt	k5eAaPmAgFnS	dovézt
7	[number]	k4	7
187	[number]	k4	187
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
65	[number]	k4	65
%	%	kIx~	%
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
27	[number]	k4	27
%	%	kIx~	%
z	z	k7c2	z
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
,	,	kIx,	,
4	[number]	k4	4
%	%	kIx~	%
z	z	k7c2	z
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
,	,	kIx,	,
3	[number]	k4	3
%	%	kIx~	%
z	z	k7c2	z
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
a	a	k8xC	a
1	[number]	k4	1
%	%	kIx~	%
z	z	k7c2	z
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
benzínu	benzín	k1gInSc2	benzín
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
rafinériích	rafinérie	k1gFnPc6	rafinérie
činila	činit	k5eAaImAgFnS	činit
1	[number]	k4	1
555	[number]	k4	555
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
motorové	motorový	k2eAgFnSc2d1	motorová
nafty	nafta	k1gFnSc2	nafta
2	[number]	k4	2
830	[number]	k4	830
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
dovoz	dovoz	k1gInSc1	dovoz
benzínu	benzín	k1gInSc2	benzín
činil	činit	k5eAaImAgInS	činit
494	[number]	k4	494
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
motorové	motorový	k2eAgFnSc2d1	motorová
nafty	nafta	k1gFnSc2	nafta
1	[number]	k4	1
236	[number]	k4	236
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náročnost	náročnost	k1gFnSc1	náročnost
těžby	těžba	k1gFnSc2	těžba
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
finanční	finanční	k2eAgFnSc3d1	finanční
náročnosti	náročnost	k1gFnSc3	náročnost
její	její	k3xOp3gFnSc2	její
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Saudské	saudský	k2eAgFnSc6d1	Saudská
Arábii	Arábie	k1gFnSc6	Arábie
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
relativně	relativně	k6eAd1	relativně
snadná	snadný	k2eAgFnSc1d1	snadná
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytěžit	vytěžit	k5eAaPmF	vytěžit
jeden	jeden	k4xCgInSc1	jeden
barel	barel	k1gInSc1	barel
ropy	ropa	k1gFnSc2	ropa
vyjde	vyjít	k5eAaPmIp3nS	vyjít
na	na	k7c4	na
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
