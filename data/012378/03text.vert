<p>
<s>
Ploskoroh	ploskoroh	k1gMnSc1	ploskoroh
pestrý	pestrý	k2eAgMnSc1d1	pestrý
(	(	kIx(	(
<g/>
Libelloides	Libelloides	k1gMnSc1	Libelloides
macaronius	macaronius	k1gMnSc1	macaronius
<g/>
;	;	kIx,	;
Scopoli	Scopole	k1gFnSc6	Scopole
<g/>
,	,	kIx,	,
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc4	hmyz
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ploskorohovití	ploskorohovitý	k2eAgMnPc1d1	ploskorohovitý
<g/>
.	.	kIx.	.
</s>
<s>
Ploskoroh	ploskoroh	k1gMnSc1	ploskoroh
pestrý	pestrý	k2eAgMnSc1d1	pestrý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c4	na
zachovalý	zachovalý	k2eAgInSc4d1	zachovalý
skalních	skalní	k2eAgInPc6d1	skalní
a	a	k8xC	a
sprašových	sprašový	k2eAgInPc6d1	sprašový
stepních	stepní	k2eAgInPc6d1	stepní
trávnících	trávník	k1gInPc6	trávník
<g/>
.	.	kIx.	.
</s>
<s>
Poletují	poletovat	k5eAaImIp3nP	poletovat
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
teplých	teplý	k2eAgInPc2d1	teplý
slunečných	slunečný	k2eAgInPc2d1	slunečný
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
usedají	usedat	k5eAaImIp3nP	usedat
na	na	k7c4	na
vegetaci	vegetace	k1gFnSc4	vegetace
se	s	k7c7	s
střechovitě	střechovitě	k6eAd1	střechovitě
složenými	složený	k2eAgNnPc7d1	složené
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ploskoroh	ploskoroh	k1gMnSc1	ploskoroh
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgMnSc1d1	charakteristický
třepotavým	třepotavý	k2eAgInSc7d1	třepotavý
letem	let	k1gInSc7	let
<g/>
,	,	kIx,	,
během	během	k7c2	během
letu	let	k1gInSc2	let
loví	lovit	k5eAaImIp3nP	lovit
drobný	drobný	k2eAgInSc4d1	drobný
hmyz	hmyz	k1gInSc4	hmyz
i	i	k9	i
kopuluje	kopulovat	k5eAaImIp3nS	kopulovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dravce	dravec	k1gMnPc4	dravec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dospělci	dospělec	k1gMnPc1	dospělec
mají	mít	k5eAaImIp3nP	mít
černě	černě	k6eAd1	černě
chlupaté	chlupatý	k2eAgNnSc4d1	chlupaté
tělo	tělo	k1gNnSc4	tělo
25-35	[number]	k4	25-35
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
se	s	k7c7	s
žlutými	žlutý	k2eAgFnPc7d1	žlutá
a	a	k8xC	a
černohnědými	černohnědý	k2eAgFnPc7d1	černohnědá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgNnPc1d1	charakteristické
jsou	být	k5eAaImIp3nP	být
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
nitkovitá	nitkovitý	k2eAgNnPc1d1	nitkovitý
tykadla	tykadlo	k1gNnPc1	tykadlo
zakončená	zakončený	k2eAgFnSc1d1	zakončená
paličkou	palička	k1gFnSc7	palička
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
samců	samec	k1gMnPc2	samec
je	být	k5eAaImIp3nS	být
zakončeno	zakončit	k5eAaPmNgNnS	zakončit
klíšťkami	klíšťky	k1gFnPc7	klíšťky
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
přidržování	přidržování	k1gNnSc3	přidržování
samice	samice	k1gFnSc2	samice
při	při	k7c6	při
kopulaci	kopulace	k1gFnSc6	kopulace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
kladou	klást	k5eAaImIp3nP	klást
40-50	[number]	k4	40-50
vajíček	vajíčko	k1gNnPc2	vajíčko
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
na	na	k7c6	na
vegetaci	vegetace	k1gFnSc6	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
oválná	oválný	k2eAgNnPc1d1	oválné
načervenalá	načervenalý	k2eAgNnPc1d1	načervenalé
a	a	k8xC	a
asi	asi	k9	asi
2	[number]	k4	2
<g/>
mm	mm	kA	mm
velká	velká	k1gFnSc1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
larvy	larva	k1gFnPc1	larva
podobné	podobný	k2eAgFnPc1d1	podobná
larvám	larva	k1gFnPc3	larva
mravkolvů	mravkolev	k1gMnPc2	mravkolev
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
leží	ležet	k5eAaImIp3nP	ležet
nehybně	hybně	k6eNd1	hybně
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
různými	různý	k2eAgMnPc7d1	různý
bezobratlými	bezobratlí	k1gMnPc7	bezobratlí
<g/>
,	,	kIx,	,
potravu	potrava	k1gFnSc4	potrava
tráví	trávit	k5eAaImIp3nP	trávit
mimotělně	mimotělně	k6eAd1	mimotělně
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
dvakrát	dvakrát	k6eAd1	dvakrát
přezimují	přezimovat	k5eAaBmIp3nP	přezimovat
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
instar	instar	k1gInSc4	instar
si	se	k3xPyFc3	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
na	na	k7c6	na
vegetaci	vegetace	k1gFnSc6	vegetace
zámotek	zámotek	k1gInSc4	zámotek
obalený	obalený	k2eAgInSc4d1	obalený
detritem	detrit	k1gInSc7	detrit
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
líhne	líhnout	k5eAaImIp3nS	líhnout
dospělec	dospělec	k1gMnSc1	dospělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Ploskoroh	ploskoroh	k1gMnSc1	ploskoroh
pestrý	pestrý	k2eAgMnSc1d1	pestrý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
oddělených	oddělený	k2eAgFnPc6d1	oddělená
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ploskoroh	ploskoroh	k1gMnSc1	ploskoroh
pestrý	pestrý	k2eAgMnSc1d1	pestrý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Libelloides	Libelloides	k1gInSc1	Libelloides
macaronius	macaronius	k1gInSc1	macaronius
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Ploskoroh	ploskoroh	k1gMnSc1	ploskoroh
pestrý	pestrý	k2eAgMnSc1d1	pestrý
na	na	k7c4	na
BioLib	BioLib	k1gInSc4	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Ploskoroh	ploskoroh	k1gMnSc1	ploskoroh
pestrý	pestrý	k2eAgMnSc1d1	pestrý
na	na	k7c4	na
Naturbohemica	Naturbohemicum	k1gNnPc4	Naturbohemicum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
