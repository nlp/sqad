<s>
Hobiti	hobit	k1gMnPc1	hobit
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
malým	malý	k2eAgInSc7d1	malý
vzrůstem	vzrůst	k1gInSc7	vzrůst
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
polovičním	poloviční	k2eAgInSc7d1	poloviční
oproti	oproti	k7c3	oproti
běžnému	běžný	k2eAgMnSc3d1	běžný
člověku	člověk	k1gMnSc3	člověk
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
rovněž	rovněž	k9	rovněž
říká	říkat	k5eAaImIp3nS	říkat
půlčíci	půlčík	k1gMnPc1	půlčík
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
halflings	halflings	k6eAd1	halflings
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
výšky	výška	k1gFnSc2	výška
přibližně	přibližně	k6eAd1	přibližně
tří	tři	k4xCgFnPc2	tři
až	až	k9	až
čtyř	čtyři	k4xCgFnPc2	čtyři
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
