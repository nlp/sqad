<p>
<s>
Boreás	Boreás	k1gInSc1	Boreás
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Β	Β	k?	Β
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Boreas	Boreas	k1gInSc1	Boreas
nebo	nebo	k8xC	nebo
Aquilo	Aquila	k1gFnSc5	Aquila
<g/>
)	)	kIx)	)
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Titána	Titán	k1gMnSc2	Titán
Astraia	Astraius	k1gMnSc2	Astraius
a	a	k8xC	a
bohyně	bohyně	k1gFnSc2	bohyně
ranních	ranní	k2eAgInPc2d1	ranní
červánků	červánek	k1gInPc2	červánek
Éós	Éós	k1gFnSc2	Éós
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bohem	bůh	k1gMnSc7	bůh
severního	severní	k2eAgInSc2d1	severní
větru	vítr	k1gInSc2	vítr
nebo	nebo	k8xC	nebo
též	též	k9	též
severní	severní	k2eAgInSc4d1	severní
vítr	vítr	k1gInSc4	vítr
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
bratry	bratr	k1gMnPc7	bratr
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Euros	Euros	k1gInSc4	Euros
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
též	též	k9	též
Argestés	Argestés	k1gInSc4	Argestés
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Hésioda	Hésiod	k1gMnSc2	Hésiod
<g/>
)	)	kIx)	)
-	-	kIx~	-
bůh	bůh	k1gMnSc1	bůh
východního	východní	k2eAgInSc2d1	východní
nebo	nebo	k8xC	nebo
jihovýchodního	jihovýchodní	k2eAgInSc2d1	jihovýchodní
větru	vítr	k1gInSc2	vítr
</s>
</p>
<p>
<s>
Zefyros	Zefyrosa	k1gFnPc2	Zefyrosa
-	-	kIx~	-
bůh	bůh	k1gMnSc1	bůh
mírného	mírný	k2eAgInSc2d1	mírný
západního	západní	k2eAgInSc2d1	západní
větru	vítr	k1gInSc2	vítr
</s>
</p>
<p>
<s>
Notos	Notos	k1gMnSc1	Notos
-	-	kIx~	-
bůh	bůh	k1gMnSc1	bůh
jižního	jižní	k2eAgInSc2d1	jižní
větruBoreás	větruBoreás	k6eAd1	větruBoreás
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
větrů	vítr	k1gInPc2	vítr
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
a	a	k8xC	a
nejprudší	prudký	k2eAgFnPc1d3	nejprudší
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
domovské	domovský	k2eAgNnSc1d1	domovské
sídlo	sídlo	k1gNnSc1	sídlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
,	,	kIx,	,
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
křídlech	křídlo	k1gNnPc6	křídlo
se	se	k3xPyFc4	se
však	však	k9	však
prohání	prohánět	k5eAaImIp3nS	prohánět
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Óreithýia	Óreithýia	k1gFnSc1	Óreithýia
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
athénského	athénský	k2eAgMnSc2d1	athénský
krále	král	k1gMnSc2	král
Erechthea	Erechtheus	k1gMnSc2	Erechtheus
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
nechtěla	chtít	k5eNaImAgFnS	chtít
provdat	provdat	k5eAaPmF	provdat
<g/>
,	,	kIx,	,
unesl	unést	k5eAaPmAgMnS	unést
ji	on	k3xPp3gFnSc4	on
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
dva	dva	k4xCgInPc4	dva
okřídlené	okřídlený	k2eAgFnPc1d1	okřídlená
syny	syn	k1gMnPc7	syn
jménem	jméno	k1gNnSc7	jméno
Kalaís	Kalaísa	k1gFnPc2	Kalaísa
a	a	k8xC	a
Zétés	Zétésa	k1gFnPc2	Zétésa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
mnoha	mnoho	k4c7	mnoho
hrdinskými	hrdinský	k2eAgInPc7d1	hrdinský
činy	čin	k1gInPc7	čin
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
výpravy	výprava	k1gFnSc2	výprava
Argonautů	argonaut	k1gMnPc2	argonaut
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
pro	pro	k7c4	pro
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
měli	mít	k5eAaImAgMnP	mít
Boreás	Boreás	k1gInSc4	Boreás
a	a	k8xC	a
Órethýia	Órethýia	k1gFnSc1	Órethýia
také	také	k6eAd1	také
dcery	dcera	k1gFnPc4	dcera
Chioné	Chioná	k1gFnPc4	Chioná
a	a	k8xC	a
Kleopatru	Kleopatra	k1gFnSc4	Kleopatra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
thráckého	thrácký	k2eAgMnSc4d1	thrácký
krále	král	k1gMnSc4	král
Fínea	Fíneus	k1gMnSc4	Fíneus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnPc1	antika
</s>
</p>
<p>
<s>
Publius	Publius	k1gMnSc1	Publius
Ovidius	Ovidius	k1gMnSc1	Ovidius
Naso	Naso	k1gMnSc1	Naso
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
,	,	kIx,	,
Starověké	starověký	k2eAgFnPc1d1	starověká
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
