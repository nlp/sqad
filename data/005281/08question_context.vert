<s>
Boreás	Boreás	k1gInSc1	Boreás
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Β	Β	k?	Β
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Boreas	Boreas	k1gInSc1	Boreas
nebo	nebo	k8xC	nebo
Aquilo	Aquila	k1gFnSc5	Aquila
<g/>
)	)	kIx)	)
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Titána	Titán	k1gMnSc2	Titán
Astraia	Astraius	k1gMnSc2	Astraius
a	a	k8xC	a
bohyně	bohyně	k1gFnSc2	bohyně
ranních	ranní	k2eAgInPc2d1	ranní
červánků	červánek	k1gInPc2	červánek
Éós	Éós	k1gFnSc2	Éós
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bohem	bůh	k1gMnSc7	bůh
severního	severní	k2eAgInSc2d1	severní
větru	vítr	k1gInSc2	vítr
nebo	nebo	k8xC	nebo
též	též	k9	též
severní	severní	k2eAgInSc4d1	severní
vítr	vítr	k1gInSc4	vítr
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
