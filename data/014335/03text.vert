<s>
Haltýř	haltýř	k1gInSc1
(	(	kIx(
<g/>
rybník	rybník	k1gInSc1
<g/>
,	,	kIx,
Radonice	Radonice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Haltýř	haltýř	k1gInSc1
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc4
od	od	k7c2
hrázePoloha	hrázePoloh	k1gMnSc2
Světadíl	světadíl	k1gInSc4
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozměry	rozměr	k1gInPc1
Rok	rok	k1gInSc1
</s>
<s>
po	po	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
Délka	délka	k1gFnSc1
</s>
<s>
50	#num#	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
15	#num#	k4
m	m	kA
Ostatní	ostatní	k2eAgInSc1d1
Odtok	odtok	k1gInSc1
vody	voda	k1gFnSc2
</s>
<s>
Radonický	Radonický	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Haltýř	haltýř	k1gInSc1
je	být	k5eAaImIp3nS
název	název	k1gInSc4
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
v	v	k7c6
centru	centrum	k1gNnSc6
vsi	ves	k1gFnSc2
Radonice	Radonice	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Praha-východ	Praha-východ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétně	konkrétně	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
ulice	ulice	k1gFnSc2
U	u	k7c2
Haltýře	haltýř	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
podlouhlý	podlouhlý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
délky	délka	k1gFnSc2
asi	asi	k9
50	#num#	k4
m	m	kA
a	a	k8xC
šířky	šířka	k1gFnPc1
15	#num#	k4
m.	m.	kA
Je	být	k5eAaImIp3nS
orientován	orientovat	k5eAaBmNgInS
z	z	k7c2
jihovýchodu	jihovýchod	k1gInSc2
na	na	k7c4
severozápad	severozápad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
zeleň	zeleň	k1gFnSc1
<g/>
,	,	kIx,
cesta	cesta	k1gFnSc1
a	a	k8xC
tenisové	tenisový	k2eAgInPc4d1
kurty	kurt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
odtéká	odtékat	k5eAaImIp3nS
Radonickým	Radonický	k2eAgInSc7d1
potokem	potok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
po	po	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
do	do	k7c2
ulice	ulice	k1gFnSc2
U	u	k7c2
Haltýře	haltýř	k1gInSc2
</s>
<s>
Břeh	břeh	k1gInSc1
</s>
<s>
Hráz	hráz	k1gFnSc1
a	a	k8xC
přepad	přepad	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Haltýř	haltýř	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
