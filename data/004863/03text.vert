<s>
Bradavice	bradavice	k1gFnSc1	bradavice
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
verruca	verruc	k1gInSc2	verruc
<g/>
,	,	kIx,	,
počeštěně	počeštěně	k6eAd1	počeštěně
veruka	veruk	k1gMnSc2	veruk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nezhoubný	zhoubný	k2eNgInSc4d1	nezhoubný
výrůstek	výrůstek	k1gInSc4	výrůstek
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
nebo	nebo	k8xC	nebo
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Bradavice	bradavice	k1gFnPc1	bradavice
jsou	být	k5eAaImIp3nP	být
nezhoubné	zhoubný	k2eNgInPc1d1	nezhoubný
nádorky	nádorek	k1gInPc1	nádorek
(	(	kIx(	(
<g/>
novotvary	novotvar	k1gInPc1	novotvar
<g/>
)	)	kIx)	)
způsobené	způsobený	k2eAgNnSc1d1	způsobené
lidským	lidský	k2eAgInSc7d1	lidský
kožním	kožní	k2eAgInSc7d1	kožní
papilomavirem	papilomavir	k1gInSc7	papilomavir
HPV	HPV	kA	HPV
<g/>
.	.	kIx.	.
</s>
<s>
Papilomaviry	Papilomavira	k1gFnPc1	Papilomavira
se	se	k3xPyFc4	se
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
populaci	populace	k1gFnSc6	populace
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nevyvolává	vyvolávat	k5eNaImIp3nS	vyvolávat
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
žádné	žádný	k3yNgNnSc1	žádný
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Viry	vir	k1gInPc1	vir
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
mohou	moct	k5eAaImIp3nP	moct
ale	ale	k9	ale
způsobovat	způsobovat	k5eAaImF	způsobovat
i	i	k9	i
závažnější	závažný	k2eAgNnSc4d2	závažnější
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
rakovina	rakovina	k1gFnSc1	rakovina
děložního	děložní	k2eAgInSc2d1	děložní
čípku	čípek	k1gInSc2	čípek
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
infekční	infekční	k2eAgFnSc2d1	infekční
kožní	kožní	k2eAgFnSc2d1	kožní
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
HPV	HPV	kA	HPV
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvouřetězcovou	dvouřetězcový	k2eAgFnSc7d1	dvouřetězcový
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
populaci	populace	k1gFnSc6	populace
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
HPV	HPV	kA	HPV
viry	vira	k1gFnPc1	vira
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
vysoce	vysoce	k6eAd1	vysoce
druhově	druhově	k6eAd1	druhově
specifické	specifický	k2eAgFnPc1d1	specifická
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
přenášet	přenášet	k5eAaImF	přenášet
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
je	být	k5eAaImIp3nS	být
přenosný	přenosný	k2eAgInSc4d1	přenosný
dotekem	dotek	k1gInSc7	dotek
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
podkoží	podkoží	k1gNnSc2	podkoží
například	například	k6eAd1	například
malým	malý	k2eAgNnSc7d1	malé
poraněním	poranění	k1gNnSc7	poranění
<g/>
.	.	kIx.	.
</s>
<s>
Bradavice	bradavice	k1gFnPc1	bradavice
mají	mít	k5eAaImIp3nP	mít
individuální	individuální	k2eAgFnSc4d1	individuální
inkubační	inkubační	k2eAgFnSc4d1	inkubační
dobu	doba	k1gFnSc4	doba
od	od	k7c2	od
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
měsíce	měsíc	k1gInPc4	měsíc
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
bradavice	bradavice	k1gFnSc1	bradavice
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
sám	sám	k3xTgInSc1	sám
nějaké	nějaký	k3yIgNnSc4	nějaký
měl	mít	k5eAaImAgInS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Bradavice	bradavice	k1gFnSc1	bradavice
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
nebo	nebo	k8xC	nebo
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
partiích	partie	k1gFnPc6	partie
nachází	nacházet	k5eAaImIp3nS	nacházet
vysoké	vysoký	k2eAgNnSc1d1	vysoké
množství	množství	k1gNnSc1	množství
potních	potní	k2eAgFnPc2d1	potní
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
může	moct	k5eAaImIp3nS	moct
HPV	HPV	kA	HPV
virus	virus	k1gInSc1	virus
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
již	již	k6eAd1	již
vytvořenými	vytvořený	k2eAgInPc7d1	vytvořený
novotvary	novotvar	k1gInPc7	novotvar
neodborně	odborně	k6eNd1	odborně
manipulováno	manipulován	k2eAgNnSc1d1	manipulováno
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
krev	krev	k1gFnSc1	krev
snadno	snadno	k6eAd1	snadno
nakazit	nakazit	k5eAaPmF	nakazit
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k6eAd1	tak
další	další	k2eAgFnPc4d1	další
bradavice	bradavice	k1gFnPc4	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
i	i	k9	i
v	v	k7c6	v
ordinaci	ordinace	k1gFnSc6	ordinace
dermatologa	dermatolog	k1gMnSc2	dermatolog
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lékař	lékař	k1gMnSc1	lékař
bradavici	bradavice	k1gFnSc4	bradavice
dostatečně	dostatečně	k6eAd1	dostatečně
neodstraní	odstranit	k5eNaPmIp3nS	odstranit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInSc4d1	negativní
následek	následek	k1gInSc4	následek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
diseminace	diseminace	k1gFnSc2	diseminace
bradavice	bradavice	k1gFnSc2	bradavice
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
růst	růst	k1gInSc1	růst
nových	nový	k2eAgInPc2d1	nový
novotvarů	novotvar	k1gInPc2	novotvar
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Výrazná	výrazný	k2eAgFnSc1d1	výrazná
bradavice	bradavice	k1gFnSc1	bradavice
na	na	k7c6	na
nose	nos	k1gInSc6	nos
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
stereotypního	stereotypní	k2eAgNnSc2d1	stereotypní
znázornění	znázornění	k1gNnSc2	znázornění
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Odstraňování	odstraňování	k1gNnSc1	odstraňování
bradavic	bradavice	k1gFnPc2	bradavice
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lokálním	lokální	k2eAgNnSc7d1	lokální
zmrazením	zmrazení	k1gNnSc7	zmrazení
–	–	k?	–
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
kapalného	kapalný	k2eAgInSc2d1	kapalný
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejúčinnější	účinný	k2eAgFnSc4d3	nejúčinnější
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Nejbezpečnější	bezpečný	k2eAgFnSc1d3	nejbezpečnější
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
aplikace	aplikace	k1gFnSc1	aplikace
dusíku	dusík	k1gInSc2	dusík
pomocí	pomocí	k7c2	pomocí
bezkontaktního	bezkontaktní	k2eAgNnSc2d1	bezkontaktní
mražení	mražení	k1gNnSc2	mražení
kryosprejem	kryosprej	k1gInSc7	kryosprej
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
přenosu	přenos	k1gInSc3	přenos
viru	vir	k1gInSc2	vir
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
velké	velký	k2eAgNnSc4d1	velké
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
přenosu	přenos	k1gInSc2	přenos
viru	vir	k1gInSc2	vir
hrozí	hrozit	k5eAaImIp3nS	hrozit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
použití	použití	k1gNnSc2	použití
vatových	vatový	k2eAgFnPc2d1	vatová
štětiček	štětička	k1gFnPc2	štětička
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
virus	virus	k1gInSc1	virus
odolá	odolat	k5eAaPmIp3nS	odolat
i	i	k9	i
velmi	velmi	k6eAd1	velmi
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
skrz	skrz	k7c4	skrz
tyto	tento	k3xDgFnPc4	tento
štětičky	štětička	k1gFnPc4	štětička
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
nádoba	nádoba	k1gFnSc1	nádoba
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
namáčí	namáčet	k5eAaImIp3nS	namáčet
štětičky	štětička	k1gFnSc2	štětička
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
hromadí	hromadit	k5eAaImIp3nS	hromadit
viry	vir	k1gInPc4	vir
od	od	k7c2	od
předchozích	předchozí	k2eAgMnPc2d1	předchozí
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
štětička	štětička	k1gFnSc1	štětička
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
i	i	k9	i
tyto	tento	k3xDgInPc4	tento
viry	vir	k1gInPc4	vir
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bradavice	bradavice	k1gFnSc1	bradavice
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
2	[number]	k4	2
mm	mm	kA	mm
lze	lze	k6eAd1	lze
ničit	ničit	k5eAaImF	ničit
pouze	pouze	k6eAd1	pouze
kapalným	kapalný	k2eAgInSc7d1	kapalný
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ostatní	ostatní	k2eAgInPc1d1	ostatní
kryogenní	kryogenní	k2eAgInPc1d1	kryogenní
plyny	plyn	k1gInPc1	plyn
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
dostatečně	dostatečně	k6eAd1	dostatečně
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bezpečně	bezpečně	k6eAd1	bezpečně
zničily	zničit	k5eAaPmAgFnP	zničit
větší	veliký	k2eAgInPc4d2	veliký
novotvary	novotvar	k1gInPc4	novotvar
(	(	kIx(	(
<g/>
při	při	k7c6	při
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
destrukci	destrukce	k1gFnSc6	destrukce
hrozí	hrozit	k5eAaImIp3nS	hrozit
recidiva	recidiva	k1gFnSc1	recidiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
léčiv	léčivo	k1gNnPc2	léčivo
obsahujících	obsahující	k2eAgNnPc2d1	obsahující
aktivní	aktivní	k2eAgFnSc4d1	aktivní
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cytostatika	cytostatikum	k1gNnPc4	cytostatikum
ničící	ničící	k2eAgFnSc2d1	ničící
chorobně	chorobně	k6eAd1	chorobně
se	se	k3xPyFc4	se
množící	množící	k2eAgFnPc1d1	množící
buňky	buňka	k1gFnPc1	buňka
či	či	k8xC	či
kyselina	kyselina	k1gFnSc1	kyselina
mléčná	mléčný	k2eAgFnSc1d1	mléčná
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
salicylová	salicylový	k2eAgFnSc1d1	salicylová
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnSc7d1	lokální
destrukcí	destrukce	k1gFnSc7	destrukce
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
potírání	potírání	k1gNnSc2	potírání
tyčinkou	tyčinka	k1gFnSc7	tyčinka
z	z	k7c2	z
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
masy	masa	k1gFnSc2	masa
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k1gInSc2	stříbrný
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Lapis	lapis	k1gInSc1	lapis
infernalis	infernalis	k1gInSc1	infernalis
-	-	kIx~	-
97	[number]	k4	97
%	%	kIx~	%
AgNO	AgNO	k1gFnSc1	AgNO
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
%	%	kIx~	%
KNO	KNO	kA	KNO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
kolem	kolem	k7c2	kolem
9	[number]	k4	9
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
<g/>
:	:	kIx,	:
43	[number]	k4	43
%	%	kIx~	%
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
11	[number]	k4	11
%	%	kIx~	%
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
placeba	placebo	k1gNnSc2	placebo
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
léčivých	léčivý	k2eAgFnPc2d1	léčivá
bylin	bylina	k1gFnPc2	bylina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
šťávy	šťáva	k1gFnSc2	šťáva
vlaštovičníku	vlaštovičník	k1gInSc2	vlaštovičník
<g/>
.	.	kIx.	.
</s>
<s>
Chirurgické	chirurgický	k2eAgNnSc1d1	chirurgické
odstranění	odstranění	k1gNnSc1	odstranění
nebo	nebo	k8xC	nebo
ozařování	ozařování	k1gNnSc1	ozařování
postiženého	postižený	k2eAgNnSc2d1	postižené
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Česnek	česnek	k1gInSc1	česnek
vkládaný	vkládaný	k2eAgInSc1d1	vkládaný
na	na	k7c6	na
bradavici	bradavice	k1gFnSc6	bradavice
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
znaménko	znaménko	k1gNnSc1	znaménko
Piha	piha	k1gFnSc1	piha
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bradavice	bradavice	k1gFnSc2	bradavice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bradavice	bradavice	k1gFnSc2	bradavice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
