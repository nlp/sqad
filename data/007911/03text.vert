<s>
Miloš	Miloš	k1gMnSc1	Miloš
(	(	kIx(	(
<g/>
řidčeji	řídce	k6eAd2	řídce
také	také	k9	také
Milouš	Milouš	k1gMnSc1	Milouš
či	či	k8xC	či
Miloň	Miloň	k1gMnSc1	Miloň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
milovaný	milovaný	k1gMnSc1	milovaný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
občanského	občanský	k2eAgInSc2d1	občanský
kalendáře	kalendář	k1gInSc2	kalendář
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc1	svátek
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
−	−	k?	−
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
−	−	k?	−
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Milda	Milda	k1gMnSc1	Milda
Miloš	Miloš	k1gMnSc1	Miloš
Alexander	Alexandra	k1gFnPc2	Alexandra
Bazovský	Bazovský	k1gMnSc1	Bazovský
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
malíř	malíř	k1gMnSc1	malíř
Miloň	Miloň	k1gFnSc2	Miloň
Čepelka	čepelka	k1gFnSc1	čepelka
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Miloš	Miloš	k1gMnSc1	Miloš
Čižmář	čižmář	k1gMnSc1	čižmář
–	–	k?	–
český	český	k2eAgMnSc1d1	český
archeolog	archeolog	k1gMnSc1	archeolog
Miloš	Miloš	k1gMnSc1	Miloš
Crnjanski	Crnjansk	k1gFnSc2	Crnjansk
–	–	k?	–
srbský	srbský	k2eAgMnSc1d1	srbský
básník	básník	k1gMnSc1	básník
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
–	–	k?	–
český	český	k2eAgMnSc1d1	český
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Miloš	Miloš	k1gMnSc1	Miloš
Glonek	Glonek	k1gMnSc1	Glonek
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Milouš	Milouš	k1gMnSc1	Milouš
Jakeš	Jakeš	k1gMnSc1	Jakeš
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
KSČ	KSČ	kA	KSČ
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1987	[number]	k4	1987
–	–	k?	–
1989	[number]	k4	1989
Miloš	Miloš	k1gMnSc1	Miloš
Kirschner	Kirschner	k1gMnSc1	Kirschner
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
český	český	k2eAgMnSc1d1	český
loutkař	loutkařit	k5eAaImRp2nS	loutkařit
Miloš	Miloš	k1gMnSc1	Miloš
Karišik	Karišik	k1gMnSc1	Karišik
–	–	k?	–
srbský	srbský	k2eAgMnSc1d1	srbský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Miloš	Miloš	k1gMnSc1	Miloš
Karadaglić	Karadaglić	k1gMnSc1	Karadaglić
–	–	k?	–
černohorský	černohorský	k2eAgMnSc1d1	černohorský
kytarista	kytarista	k1gMnSc1	kytarista
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Miloš	Miloš	k1gMnSc1	Miloš
Lačný	lačný	k2eAgMnSc1d1	lačný
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Miloš	Miloš	k1gMnSc1	Miloš
Macourek	Macourek	k1gMnSc1	Macourek
–	–	k?	–
český	český	k2eAgMnSc1d1	český
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
Miloš	Miloš	k1gMnSc1	Miloš
Nedbal	Nedbal	k1gMnSc1	Nedbal
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
pedagog	pedagog	k1gMnSc1	pedagog
Miloň	Miloň	k1gFnSc2	Miloň
Novotný	Novotný	k1gMnSc1	Novotný
–	–	k?	–
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
fotograf	fotograf	k1gMnSc1	fotograf
Miloš	Miloš	k1gMnSc1	Miloš
Nesvadba	Nesvadba	k1gMnSc1	Nesvadba
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Obilić	Obilić	k1gMnSc1	Obilić
–	–	k?	–
srbský	srbský	k2eAgMnSc1d1	srbský
rytíř	rytíř	k1gMnSc1	rytíř
Miloš	Miloš	k1gMnSc1	Miloš
Pokorný	Pokorný	k1gMnSc1	Pokorný
–	–	k?	–
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
Těžkýho	Těžkýho	k?	Těžkýho
Pokondra	Pokondra	k1gFnSc1	Pokondra
Milos	Milosa	k1gFnPc2	Milosa
Raonic	Raonice	k1gFnPc2	Raonice
–	–	k?	–
kanadský	kanadský	k2eAgMnSc1d1	kanadský
tenista	tenista	k1gMnSc1	tenista
Miloš	Miloš	k1gMnSc1	Miloš
Sádlo	sádlo	k1gNnSc1	sádlo
–	–	k?	–
český	český	k2eAgMnSc1d1	český
violoncellista	violoncellista	k1gMnSc1	violoncellista
Miloš	Miloš	k1gMnSc1	Miloš
Šejn	Šejn	k1gMnSc1	Šejn
–	–	k?	–
český	český	k2eAgMnSc1d1	český
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
performer	performer	k1gMnSc1	performer
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
Miloš	Miloš	k1gMnSc1	Miloš
Štědroň	Štědroň	k1gMnSc1	Štědroň
–	–	k?	–
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
muzikolog	muzikolog	k1gMnSc1	muzikolog
Miloš	Miloš	k1gMnSc1	Miloš
Tichý	Tichý	k1gMnSc1	Tichý
–	–	k?	–
český	český	k2eAgMnSc1d1	český
astronom	astronom	k1gMnSc1	astronom
Miloš	Miloš	k1gMnSc1	Miloš
Urban	Urban	k1gMnSc1	Urban
–	–	k?	–
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
redaktor	redaktor	k1gMnSc1	redaktor
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
–	–	k?	–
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Milouš	Milouš	k1gMnSc1	Milouš
–	–	k?	–
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
románu	román	k1gInSc2	román
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirotky	Jirotka	k1gFnSc2	Jirotka
Saturnin	Saturnin	k1gMnSc1	Saturnin
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
"	"	kIx"	"
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Miloš	Miloš	k1gMnSc1	Miloš
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
