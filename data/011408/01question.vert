<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
