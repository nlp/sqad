<s>
Reproduktory	reproduktor	k1gInPc1	reproduktor
jsou	být	k5eAaImIp3nP	být
elektro-akustické	elektrokustický	k2eAgInPc1d1	elektro-akustický
měniče	měnič	k1gInPc1	měnič
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
elektrické	elektrický	k2eAgInPc1d1	elektrický
stroje	stroj	k1gInPc1	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
