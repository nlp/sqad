<p>
<s>
Reproduktory	reproduktor	k1gInPc1	reproduktor
jsou	být	k5eAaImIp3nP	být
elektro-akustické	elektrokustický	k2eAgInPc1d1	elektro-akustický
měniče	měnič	k1gInPc1	měnič
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
elektrické	elektrický	k2eAgInPc1d1	elektrický
stroje	stroj	k1gInPc1	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
z	z	k7c2	z
pohonné	pohonný	k2eAgFnSc2d1	pohonná
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
přiváděn	přiváděn	k2eAgInSc1d1	přiváděn
vstupní	vstupní	k2eAgInSc1d1	vstupní
signál	signál	k1gInSc1	signál
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
malých	malý	k2eAgInPc2d1	malý
reproduktorů	reproduktor	k1gInPc2	reproduktor
jsou	být	k5eAaImIp3nP	být
sluchátka	sluchátko	k1gNnPc4	sluchátko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
reproduktorů	reproduktor	k1gInPc2	reproduktor
dle	dle	k7c2	dle
způsobu	způsob	k1gInSc2	způsob
vyzařování	vyzařování	k1gNnSc2	vyzařování
==	==	k?	==
</s>
</p>
<p>
<s>
Přímovyzařující	Přímovyzařující	k2eAgFnSc1d1	Přímovyzařující
–	–	k?	–
kmitající	kmitající	k2eAgFnSc1d1	kmitající
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
navázána	navázán	k2eAgFnSc1d1	navázána
na	na	k7c4	na
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
akustická	akustický	k2eAgFnSc1d1	akustická
energie	energie	k1gFnSc1	energie
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
účinnost	účinnost	k1gFnSc1	účinnost
nepřevyšuje	převyšovat	k5eNaImIp3nS	převyšovat
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepřímovyzařující	Nepřímovyzařující	k2eAgInPc4d1	Nepřímovyzařující
(	(	kIx(	(
<g/>
tlakové	tlakový	k2eAgInPc4d1	tlakový
<g/>
)	)	kIx)	)
–	–	k?	–
mezi	mezi	k7c7	mezi
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
akustická	akustický	k2eAgFnSc1d1	akustická
energie	energie	k1gFnSc1	energie
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vložen	vložen	k2eAgInSc4d1	vložen
zvukovod	zvukovod	k1gInSc4	zvukovod
a	a	k8xC	a
popřípadě	popřípadě	k6eAd1	popřípadě
další	další	k2eAgInPc1d1	další
pomocné	pomocný	k2eAgInPc1d1	pomocný
akustické	akustický	k2eAgInPc1d1	akustický
obvody	obvod	k1gInPc1	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
obvykle	obvykle	k6eAd1	obvykle
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dosahovat	dosahovat	k5eAaImF	dosahovat
vysokých	vysoký	k2eAgInPc2d1	vysoký
vyzářených	vyzářený	k2eAgInPc2d1	vyzářený
výkonů	výkon	k1gInPc2	výkon
<g/>
,	,	kIx,	,
tvarovat	tvarovat	k5eAaImF	tvarovat
směrový	směrový	k2eAgInSc4d1	směrový
diagram	diagram	k1gInSc4	diagram
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
reproduktorů	reproduktor	k1gInPc2	reproduktor
dle	dle	k7c2	dle
pohonu	pohon	k1gInSc2	pohon
==	==	k?	==
</s>
</p>
<p>
<s>
elektrodynamické	elektrodynamický	k2eAgNnSc1d1	elektrodynamické
</s>
</p>
<p>
<s>
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
</s>
</p>
<p>
<s>
elektrostatické	elektrostatický	k2eAgInPc4d1	elektrostatický
</s>
</p>
<p>
<s>
piezoelektrické	piezoelektrický	k2eAgNnSc1d1	piezoelektrické
</s>
</p>
<p>
<s>
plazmové	plazmový	k2eAgNnSc1d1	plazmové
</s>
</p>
<p>
<s>
pneumatické	pneumatický	k2eAgNnSc1d1	pneumatické
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
</s>
</p>
<p>
<s>
===	===	k?	===
Elektrodynamické	elektrodynamický	k2eAgMnPc4d1	elektrodynamický
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
pohonu	pohon	k1gInSc2	pohon
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
těchto	tento	k3xDgInPc2	tento
reproduktorů	reproduktor	k1gInPc2	reproduktor
je	být	k5eAaImIp3nS	být
cívka	cívka	k1gFnSc1	cívka
a	a	k8xC	a
permanentní	permanentní	k2eAgInSc1d1	permanentní
magnet	magnet	k1gInSc1	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Cívka	cívka	k1gFnSc1	cívka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
ve	v	k7c6	v
válcové	válcový	k2eAgFnSc6d1	válcová
štěrbině	štěrbina	k1gFnSc6	štěrbina
mezi	mezi	k7c7	mezi
pólovými	pólový	k2eAgInPc7d1	pólový
nástavci	nástavec	k1gInPc7	nástavec
magnetického	magnetický	k2eAgInSc2d1	magnetický
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
činnosti	činnost	k1gFnSc2	činnost
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
působení	působení	k1gNnSc6	působení
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
vodič	vodič	k1gInSc4	vodič
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
protéká	protékat	k5eAaImIp3nS	protékat
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
membránu	membrána	k1gFnSc4	membrána
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
její	její	k3xOp3gInSc4	její
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
konvenčních	konvenční	k2eAgInPc2d1	konvenční
typů	typ	k1gInPc2	typ
existují	existovat	k5eAaImIp3nP	existovat
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
provedení	provedení	k1gNnSc4	provedení
:	:	kIx,	:
</s>
</p>
<p>
<s>
Páskový	páskový	k2eAgInSc1d1	páskový
reproduktor	reproduktor	k1gInSc1	reproduktor
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
typ	typ	k1gInSc1	typ
elektrodynamického	elektrodynamický	k2eAgInSc2d1	elektrodynamický
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgFnPc4d1	vysoká
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
frekvence	frekvence	k1gFnPc4	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
pásek	pásek	k1gInSc4	pásek
z	z	k7c2	z
elektricky	elektricky	k6eAd1	elektricky
vodivého	vodivý	k2eAgInSc2d1	vodivý
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
membrána	membrána	k1gFnSc1	membrána
páskového	páskový	k2eAgInSc2d1	páskový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
silného	silný	k2eAgInSc2d1	silný
magnetu	magnet	k1gInSc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
obdobné	obdobný	k2eAgNnSc1d1	obdobné
jako	jako	k9	jako
u	u	k7c2	u
páskového	páskový	k2eAgInSc2d1	páskový
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plošné	plošný	k2eAgInPc1d1	plošný
elektrodynamické	elektrodynamický	k2eAgInPc1d1	elektrodynamický
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
membránou	membrána	k1gFnSc7	membrána
velké	velká	k1gFnSc2	velká
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
ukotvenou	ukotvený	k2eAgFnSc7d1	ukotvená
v	v	k7c6	v
napínacím	napínací	k2eAgInSc6d1	napínací
rámu	rám	k1gInSc6	rám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
membránu	membrána	k1gFnSc4	membrána
bývá	bývat	k5eAaImIp3nS	bývat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
upevněn	upevněn	k2eAgInSc4d1	upevněn
vodič	vodič	k1gInSc4	vodič
cívky	cívka	k1gFnSc2	cívka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
tenký	tenký	k2eAgInSc4d1	tenký
hliníkový	hliníkový	k2eAgInSc4d1	hliníkový
drát	drát	k1gInSc4	drát
nebo	nebo	k8xC	nebo
fólie	fólie	k1gFnPc4	fólie
<g/>
,	,	kIx,	,
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
do	do	k7c2	do
meandru	meandr	k1gInSc2	meandr
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
ploše	plocha	k1gFnSc6	plocha
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
plošného	plošný	k2eAgInSc2d1	plošný
magnetu	magnet	k1gInSc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
označovány	označován	k2eAgInPc1d1	označován
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
magnetostatické	magnetostatický	k2eAgNnSc1d1	magnetostatický
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
podobu	podoba	k1gFnSc4	podoba
s	s	k7c7	s
elektrostatickými	elektrostatický	k2eAgInPc7d1	elektrostatický
měniči	měnič	k1gInPc7	měnič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reproduktor	reproduktor	k1gInSc1	reproduktor
s	s	k7c7	s
ohybovou	ohybový	k2eAgFnSc7d1	ohybová
vlnou	vlna	k1gFnSc7	vlna
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
používá	používat	k5eAaImIp3nS	používat
trochu	trochu	k6eAd1	trochu
odlišný	odlišný	k2eAgInSc1d1	odlišný
princip	princip	k1gInSc1	princip
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
běžných	běžný	k2eAgInPc2d1	běžný
elektrodynamických	elektrodynamický	k2eAgInPc2d1	elektrodynamický
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
u	u	k7c2	u
ideálního	ideální	k2eAgNnSc2d1	ideální
provedení	provedení	k1gNnSc2	provedení
pístový	pístový	k2eAgInSc4d1	pístový
pohyb	pohyb	k1gInSc4	pohyb
tuhé	tuhý	k2eAgFnSc2d1	tuhá
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
membrána	membrána	k1gFnSc1	membrána
pružná	pružný	k2eAgFnSc1d1	pružná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
frekvenci	frekvence	k1gFnSc4	frekvence
kmitá	kmitat	k5eAaImIp3nS	kmitat
pouze	pouze	k6eAd1	pouze
určitá	určitý	k2eAgFnSc1d1	určitá
část	část	k1gFnSc1	část
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
čím	čí	k3xOyRgNnSc7	čí
je	být	k5eAaImIp3nS	být
frekvence	frekvence	k1gFnSc1	frekvence
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
plocha	plocha	k1gFnSc1	plocha
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnPc4d1	nízká
frekvence	frekvence	k1gFnPc4	frekvence
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
membrána	membrána	k1gFnSc1	membrána
celou	celá	k1gFnSc4	celá
plochou	plochý	k2eAgFnSc4d1	plochá
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgInPc4d1	vysoký
jenom	jenom	k9	jenom
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
plochy	plocha	k1gFnSc2	plocha
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kmitací	kmitací	k2eAgFnSc2d1	kmitací
cívky	cívka	k1gFnSc2	cívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Elektromagnetické	elektromagnetický	k2eAgMnPc4d1	elektromagnetický
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
není	být	k5eNaImIp3nS	být
dnes	dnes	k6eAd1	dnes
příliš	příliš	k6eAd1	příliš
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
tenkého	tenký	k2eAgInSc2d1	tenký
železného	železný	k2eAgInSc2d1	železný
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
pevně	pevně	k6eAd1	pevně
umístěná	umístěný	k2eAgFnSc1d1	umístěná
cívka	cívka	k1gFnSc1	cívka
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
(	(	kIx(	(
<g/>
elektromagnet	elektromagnet	k1gInSc1	elektromagnet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
malý	malý	k2eAgInSc1d1	malý
magnet	magnet	k1gInSc1	magnet
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
budící	budící	k2eAgFnSc2d1	budící
cívky	cívka	k1gFnSc2	cívka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
s	s	k7c7	s
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
např.	např.	kA	např.
sluchátka	sluchátko	k1gNnSc2	sluchátko
pro	pro	k7c4	pro
spojaře	spojař	k1gMnSc4	spojař
nebo	nebo	k8xC	nebo
telefonii	telefonie	k1gFnSc4	telefonie
a	a	k8xC	a
také	také	k9	také
reproduktory	reproduktor	k1gInPc4	reproduktor
k	k	k7c3	k
radiopřijímačům	radiopřijímač	k1gInPc3	radiopřijímač
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
většinou	většina	k1gFnSc7	většina
značné	značný	k2eAgNnSc1d1	značné
zkreslení	zkreslení	k1gNnSc1	zkreslení
a	a	k8xC	a
omezený	omezený	k2eAgInSc1d1	omezený
frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
rozsah	rozsah	k1gInSc1	rozsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Elektrostatické	elektrostatický	k2eAgMnPc4d1	elektrostatický
===	===	k?	===
</s>
</p>
<p>
<s>
Membrána	membrána	k1gFnSc1	membrána
z	z	k7c2	z
tenké	tenký	k2eAgFnSc2d1	tenká
fólie	fólie	k1gFnSc2	fólie
s	s	k7c7	s
vodivou	vodivý	k2eAgFnSc7d1	vodivá
vrstvou	vrstva	k1gFnSc7	vrstva
bývá	bývat	k5eAaImIp3nS	bývat
umístěna	umístěn	k2eAgFnSc1d1	umístěna
mezi	mezi	k7c4	mezi
dvě	dva	k4xCgFnPc4	dva
pevné	pevný	k2eAgFnPc4d1	pevná
elektrody	elektroda	k1gFnPc4	elektroda
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
sítěk	síťka	k1gFnPc2	síťka
<g/>
.	.	kIx.	.
</s>
<s>
Reproduktor	reproduktor	k1gInSc1	reproduktor
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
přitahování	přitahování	k1gNnSc2	přitahování
a	a	k8xC	a
odpuzování	odpuzování	k1gNnSc2	odpuzování
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgFnPc2d1	nabitá
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
elektrod	elektroda	k1gFnPc2	elektroda
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
značně	značně	k6eAd1	značně
velké	velký	k2eAgInPc4d1	velký
provozní	provozní	k2eAgInPc4d1	provozní
a	a	k8xC	a
polarizační	polarizační	k2eAgNnSc4d1	polarizační
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
stovky	stovka	k1gFnPc4	stovka
až	až	k6eAd1	až
tisíce	tisíc	k4xCgInSc2	tisíc
Voltů	volt	k1gInPc2	volt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
jako	jako	k9	jako
vysokotónové	vysokotónový	k2eAgInPc1d1	vysokotónový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
širokopásmové	širokopásmový	k2eAgFnPc1d1	širokopásmová
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
značné	značný	k2eAgInPc4d1	značný
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
se	se	k3xPyFc4	se
konstruují	konstruovat	k5eAaImIp3nP	konstruovat
i	i	k9	i
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgNnPc4d1	kvalitní
sluchátka	sluchátko	k1gNnPc4	sluchátko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Piezoelektrické	piezoelektrický	k2eAgMnPc4d1	piezoelektrický
===	===	k?	===
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
piezoelektrického	piezoelektrický	k2eAgInSc2d1	piezoelektrický
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Destička	destička	k1gFnSc1	destička
z	z	k7c2	z
piezomateriálu	piezomateriál	k1gInSc2	piezomateriál
je	být	k5eAaImIp3nS	být
mechanicky	mechanicky	k6eAd1	mechanicky
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
tvoří	tvořit	k5eAaImIp3nP	tvořit
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
levné	levný	k2eAgFnPc4d1	levná
vysokotónové	vysokotónový	k2eAgFnPc4d1	vysokotónový
jednotky	jednotka	k1gFnPc4	jednotka
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
výchylka	výchylka	k1gFnSc1	výchylka
membrány	membrána	k1gFnSc2	membrána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
tlakové	tlakový	k2eAgInPc4d1	tlakový
měniče	měnič	k1gInPc4	měnič
i	i	k8xC	i
poměrně	poměrně	k6eAd1	poměrně
velkých	velký	k2eAgInPc2d1	velký
výkonů	výkon	k1gInPc2	výkon
(	(	kIx(	(
<g/>
malé	malý	k2eAgFnPc1d1	malá
sirény	siréna	k1gFnPc1	siréna
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
zásadní	zásadní	k2eAgFnSc7d1	zásadní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nerovnoměrná	rovnoměrný	k2eNgFnSc1d1	nerovnoměrná
frekvenční	frekvenční	k2eAgFnSc1d1	frekvenční
charakteristika	charakteristika	k1gFnSc1	charakteristika
a	a	k8xC	a
větší	veliký	k2eAgNnSc1d2	veliký
zkreslení	zkreslení	k1gNnSc1	zkreslení
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
bývá	bývat	k5eAaImIp3nS	bývat
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
,	,	kIx,	,
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
konstrukce	konstrukce	k1gFnSc1	konstrukce
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plazmové	plazmový	k2eAgInPc1d1	plazmový
reproduktory	reproduktor	k1gInPc1	reproduktor
===	===	k?	===
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
reproduktory	reproduktor	k1gInPc1	reproduktor
nemají	mít	k5eNaImIp3nP	mít
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
změn	změna	k1gFnPc2	změna
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
vyvolaných	vyvolaný	k2eAgInPc2d1	vyvolaný
koronou	korona	k1gFnSc7	korona
nebo	nebo	k8xC	nebo
obloukovým	obloukový	k2eAgInSc7d1	obloukový
výbojem	výboj	k1gInSc7	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
realizovat	realizovat	k5eAaBmF	realizovat
převážně	převážně	k6eAd1	převážně
vysokotónové	vysokotónový	k2eAgInPc4d1	vysokotónový
měniče	měnič	k1gInPc4	měnič
<g/>
,	,	kIx,	,
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
kmitočtový	kmitočtový	k2eAgInSc4d1	kmitočtový
rozsah	rozsah	k1gInSc4	rozsah
<g/>
,	,	kIx,	,
neomezovaný	omezovaný	k2eNgMnSc1d1	neomezovaný
hmotností	hmotnost	k1gFnSc7	hmotnost
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
princip	princip	k1gInSc1	princip
znám	znám	k2eAgInSc1d1	znám
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
experimenty	experiment	k1gInPc1	experiment
se	se	k3xPyFc4	se
prováděly	provádět	k5eAaImAgInP	provádět
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
reproduktorů	reproduktor	k1gInPc2	reproduktor
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
velmi	velmi	k6eAd1	velmi
okrajové	okrajový	k2eAgNnSc1d1	okrajové
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
měniče	měnič	k1gInPc4	měnič
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
ionofon	ionofon	k1gInSc1	ionofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pneumatické	pneumatický	k2eAgMnPc4d1	pneumatický
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
tohoto	tento	k3xDgInSc2	tento
principu	princip	k1gInSc2	princip
běžně	běžně	k6eAd1	běžně
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Tradují	tradovat	k5eAaImIp3nP	tradovat
se	se	k3xPyFc4	se
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
sporadickém	sporadický	k2eAgNnSc6d1	sporadické
použití	použití	k1gNnSc6	použití
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
extrémně	extrémně	k6eAd1	extrémně
vysokých	vysoký	k2eAgFnPc2d1	vysoká
zvukových	zvukový	k2eAgFnPc2d1	zvuková
hladin	hladina	k1gFnPc2	hladina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
simulaci	simulace	k1gFnSc4	simulace
hluku	hluk	k1gInSc2	hluk
při	při	k7c6	při
testech	test	k1gInPc6	test
dílů	díl	k1gInPc2	díl
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
modulace	modulace	k1gFnSc1	modulace
unikajícího	unikající	k2eAgInSc2d1	unikající
stlačeného	stlačený	k2eAgInSc2d1	stlačený
vzduchu	vzduch	k1gInSc2	vzduch
z	z	k7c2	z
kompresoru	kompresor	k1gInSc2	kompresor
pomocí	pomocí	k7c2	pomocí
ventilu	ventil	k1gInSc2	ventil
<g/>
,	,	kIx,	,
ovládaného	ovládaný	k2eAgInSc2d1	ovládaný
budícím	budící	k2eAgInSc7d1	budící
signálem	signál	k1gInSc7	signál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
s	s	k7c7	s
ventilem	ventil	k1gInSc7	ventil
ovládaným	ovládaný	k2eAgInSc7d1	ovládaný
nikoliv	nikoliv	k9	nikoliv
elektricky	elektricky	k6eAd1	elektricky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mechanicky	mechanicky	k6eAd1	mechanicky
–	–	k?	–
přenoskou	přenoska	k1gFnSc7	přenoska
–	–	k?	–
byly	být	k5eAaImAgInP	být
počátkem	počátek	k1gInSc7	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
i	i	k9	i
gramofony	gramofon	k1gInPc4	gramofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
principy	princip	k1gInPc1	princip
===	===	k?	===
</s>
</p>
<p>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
princip	princip	k1gInSc1	princip
reproduktoru	reproduktor	k1gInSc2	reproduktor
představuje	představovat	k5eAaImIp3nS	představovat
tzv.	tzv.	kA	tzv.
rotary	rotara	k1gFnSc2	rotara
woofer	woofer	k1gInSc1	woofer
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
rotujícími	rotující	k2eAgInPc7d1	rotující
reproduktory	reproduktor	k1gInPc7	reproduktor
–	–	k?	–
Leslie	Leslie	k1gFnSc1	Leslie
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otvoru	otvor	k1gInSc6	otvor
ozvučnice	ozvučnice	k1gFnSc2	ozvučnice
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
lopatky	lopatka	k1gFnSc2	lopatka
ventilátoru	ventilátor	k1gInSc2	ventilátor
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
náklon	náklon	k1gInSc1	náklon
lopatek	lopatka	k1gFnPc2	lopatka
mění	měnit	k5eAaImIp3nS	měnit
budícím	budící	k2eAgInSc7d1	budící
signálem	signál	k1gInSc7	signál
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
připomíná	připomínat	k5eAaImIp3nS	připomínat
fenestron	fenestron	k1gInSc4	fenestron
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc4d1	nízká
frekvence	frekvence	k1gFnPc4	frekvence
a	a	k8xC	a
generování	generování	k1gNnSc4	generování
infrazvuku	infrazvuk	k1gInSc2	infrazvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
méně	málo	k6eAd2	málo
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
principů	princip	k1gInPc2	princip
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pohon	pohon	k1gInSc1	pohon
membrány	membrána	k1gFnSc2	membrána
pomocí	pomocí	k7c2	pomocí
servomotoru	servomotor	k1gInSc2	servomotor
<g/>
.	.	kIx.	.
</s>
<s>
Membrány	membrána	k1gFnPc1	membrána
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
více	hodně	k6eAd2	hodně
membrán	membrána	k1gFnPc2	membrána
<g/>
,	,	kIx,	,
např.	např.	kA	např.
2	[number]	k4	2
nebo	nebo	k8xC	nebo
4	[number]	k4	4
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
táhly	táhnout	k5eAaImAgInP	táhnout
napojeny	napojit	k5eAaPmNgInP	napojit
na	na	k7c4	na
obvod	obvod	k1gInSc4	obvod
kotouče	kotouč	k1gInSc2	kotouč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
pootáčen	pootáčet	k5eAaImNgInS	pootáčet
pomocí	pomocí	k7c2	pomocí
servomotoru	servomotor	k1gInSc2	servomotor
<g/>
.	.	kIx.	.
</s>
<s>
Membrány	membrána	k1gFnPc1	membrána
tak	tak	k9	tak
konají	konat	k5eAaImIp3nP	konat
pístový	pístový	k2eAgInSc4d1	pístový
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
vynucovaný	vynucovaný	k2eAgMnSc1d1	vynucovaný
natáčením	natáčení	k1gNnSc7	natáčení
serva	serva	k1gFnSc1	serva
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
nízké	nízký	k2eAgInPc4d1	nízký
kmitočty	kmitočet	k1gInPc4	kmitočet
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgInPc4d1	velký
výkony	výkon	k1gInPc4	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgInPc4d1	další
fyzikálně	fyzikálně	k6eAd1	fyzikálně
možné	možný	k2eAgInPc4d1	možný
principy	princip	k1gInPc4	princip
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
</s>
</p>
<p>
<s>
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
využití	využití	k1gNnSc1	využití
magnetostrikčního	magnetostrikční	k2eAgInSc2d1	magnetostrikční
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
měničů	měnič	k1gInPc2	měnič
pro	pro	k7c4	pro
ultrazvuk	ultrazvuk	k1gInSc4	ultrazvuk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
využití	využití	k1gNnSc1	využití
Johnsen-Rahbeckova	Johnsen-Rahbeckův	k2eAgInSc2d1	Johnsen-Rahbeckův
jevu	jev	k1gInSc2	jev
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
součinitele	součinitel	k1gInSc2	součinitel
tření	tření	k1gNnPc2	tření
mezi	mezi	k7c7	mezi
vodičem	vodič	k1gInSc7	vodič
a	a	k8xC	a
polovodičem	polovodič	k1gInSc7	polovodič
vlivem	vlivem	k7c2	vlivem
změny	změna	k1gFnSc2	změna
elektrického	elektrický	k2eAgInSc2d1	elektrický
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
reproduktorů	reproduktor	k1gInPc2	reproduktor
dle	dle	k7c2	dle
účelu	účel	k1gInSc2	účel
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
použití	použití	k1gNnSc2	použití
většina	většina	k1gFnSc1	většina
výrobců	výrobce	k1gMnPc2	výrobce
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
několik	několik	k4yIc1	několik
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
domácí	domácí	k2eAgNnSc4d1	domácí
použití	použití	k1gNnSc4	použití
(	(	kIx(	(
<g/>
spotřební	spotřební	k2eAgFnSc1d1	spotřební
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
HiFi	Hifi	k1gNnSc1	Hifi
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
měniče	měnič	k1gInPc1	měnič
pro	pro	k7c4	pro
studiové	studiový	k2eAgInPc4d1	studiový
monitory	monitor	k1gInPc4	monitor
<g/>
,	,	kIx,	,
HiEnd	HiEnd	k1gInSc4	HiEnd
audio	audio	k2eAgInSc4d1	audio
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
profesionální	profesionální	k2eAgNnSc4d1	profesionální
použití	použití	k1gNnSc4	použití
(	(	kIx(	(
<g/>
výkonné	výkonný	k2eAgInPc4d1	výkonný
PA	Pa	kA	Pa
systémy	systém	k1gInPc4	systém
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ozvučení	ozvučení	k1gNnSc4	ozvučení
automobilů	automobil	k1gInPc2	automobil
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
elektrické	elektrický	k2eAgInPc4d1	elektrický
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
speciálně	speciálně	k6eAd1	speciálně
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
reproduktory	reproduktor	k1gInPc4	reproduktor
pro	pro	k7c4	pro
aparatury	aparatura	k1gFnPc4	aparatura
zvláště	zvláště	k6eAd1	zvláště
elektrické	elektrický	k2eAgFnSc2d1	elektrická
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
baskytary	baskytara	k1gFnSc2	baskytara
<g/>
,	,	kIx,	,
klávesových	klávesový	k2eAgInPc2d1	klávesový
nástrojů	nástroj	k1gInPc2	nástroj
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
reproduktory	reproduktor	k1gInPc1	reproduktor
a	a	k8xC	a
reprosoustavy	reprosoustava	k1gFnPc1	reprosoustava
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
mluveného	mluvený	k2eAgNnSc2d1	mluvené
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
městský	městský	k2eAgInSc1d1	městský
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
sirény	siréna	k1gFnPc1	siréna
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
tlakové	tlakový	k2eAgInPc1d1	tlakový
reproduktory	reproduktor	k1gInPc1	reproduktor
se	s	k7c7	s
skládaným	skládaný	k2eAgInSc7d1	skládaný
(	(	kIx(	(
<g/>
reentrantním	reentrantní	k2eAgInSc7d1	reentrantní
<g/>
)	)	kIx)	)
zvukovodem	zvukovod	k1gInSc7	zvukovod
</s>
</p>
<p>
<s>
speciální	speciální	k2eAgFnSc1d1	speciální
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ozvučení	ozvučení	k1gNnSc1	ozvučení
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vědecké	vědecký	k2eAgInPc4d1	vědecký
pokusy	pokus	k1gInPc4	pokus
<g/>
,	,	kIx,	,
jednotky	jednotka	k1gFnPc4	jednotka
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
výkonem	výkon	k1gInSc7	výkon
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgNnSc4d1	vojenské
použití	použití	k1gNnSc4	použití
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
Dělení	dělení	k1gNnSc1	dělení
není	být	k5eNaImIp3nS	být
jednotné	jednotný	k2eAgNnSc1d1	jednotné
ani	ani	k8xC	ani
závazné	závazný	k2eAgNnSc1d1	závazné
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
výrobky	výrobek	k1gInPc1	výrobek
mají	mít	k5eAaImIp3nP	mít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
použití	použití	k1gNnSc4	použití
pro	pro	k7c4	pro
různý	různý	k2eAgInSc4d1	různý
účel	účel	k1gInSc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
kterém	který	k3yQgNnSc6	který
výrobci	výrobce	k1gMnPc1	výrobce
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
výrobním	výrobní	k2eAgInSc6d1	výrobní
programu	program	k1gInSc6	program
reproduktory	reproduktor	k1gInPc4	reproduktor
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
reproduktorů	reproduktor	k1gInPc2	reproduktor
podle	podle	k7c2	podle
frekvenčního	frekvenční	k2eAgInSc2d1	frekvenční
rozsahu	rozsah	k1gInSc2	rozsah
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
frekvenčního	frekvenční	k2eAgInSc2d1	frekvenční
rozsahu	rozsah	k1gInSc2	rozsah
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
reproduktory	reproduktor	k1gInPc1	reproduktor
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
jednak	jednak	k8xC	jednak
jejich	jejich	k3xOp3gInSc7	jejich
účelem	účel	k1gInSc7	účel
jednak	jednak	k8xC	jednak
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
reproduktory	reproduktor	k1gInPc1	reproduktor
se	se	k3xPyFc4	se
provozují	provozovat	k5eAaImIp3nP	provozovat
jen	jen	k9	jen
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
frekvenční	frekvenční	k2eAgNnSc1d1	frekvenční
pásmu	pásmo	k1gNnSc3	pásmo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
jen	jen	k9	jen
jedné	jeden	k4xCgFnSc2	jeden
oktávy	oktáva	k1gFnSc2	oktáva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
sirénách	siréna	k1gFnPc6	siréna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
řeči	řeč	k1gFnSc2	řeč
v	v	k7c6	v
telekomunikační	telekomunikační	k2eAgFnSc6d1	telekomunikační
technice	technika	k1gFnSc6	technika
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
rozsah	rozsah	k1gInSc4	rozsah
od	od	k7c2	od
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
Hz	Hz	kA	Hz
po	po	k7c6	po
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
levné	levný	k2eAgFnSc6d1	levná
spotřební	spotřební	k2eAgFnSc6d1	spotřební
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
,	,	kIx,	,
domácích	domácí	k2eAgInPc6d1	domácí
Hi-Fi	Hi-Fi	k1gNnSc7	Hi-Fi
soupravách	souprava	k1gFnPc6	souprava
nebo	nebo	k8xC	nebo
profesionálních	profesionální	k2eAgInPc6d1	profesionální
ozvučovacích	ozvučovací	k2eAgInPc6d1	ozvučovací
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
výkon	výkon	k1gInSc1	výkon
<g/>
,	,	kIx,	,
rozměry	rozměr	k1gInPc1	rozměr
<g/>
,	,	kIx,	,
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
,	,	kIx,	,
frekvenční	frekvenční	k2eAgFnSc1d1	frekvenční
charakteristika	charakteristika	k1gFnSc1	charakteristika
<g/>
,	,	kIx,	,
zkreslení	zkreslení	k1gNnSc1	zkreslení
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
daly	dát	k5eAaPmAgInP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
široké	široký	k2eAgFnSc3d1	široká
škále	škála	k1gFnSc3	škála
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
přenos	přenos	k1gInSc4	přenos
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
požaduje	požadovat	k5eAaImIp3nS	požadovat
co	co	k9	co
nejširší	široký	k2eAgInSc1d3	nejširší
přenášený	přenášený	k2eAgInSc1d1	přenášený
frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
rozsah	rozsah	k1gInSc1	rozsah
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
slyšitelném	slyšitelný	k2eAgNnSc6d1	slyšitelné
pásmu	pásmo	k1gNnSc6	pásmo
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
Hz	Hz	kA	Hz
až	až	k9	až
20	[number]	k4	20
<g/>
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
celé	celý	k2eAgNnSc4d1	celé
takto	takto	k6eAd1	takto
široké	široký	k2eAgNnSc4d1	široké
pásmo	pásmo	k1gNnSc4	pásmo
přenesl	přenést	k5eAaPmAgInS	přenést
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
takový	takový	k3xDgInSc4	takový
reproduktor	reproduktor	k1gInSc4	reproduktor
jsou	být	k5eAaImIp3nP	být
kladeny	klást	k5eAaImNgInP	klást
zcela	zcela	k6eAd1	zcela
protichůdné	protichůdný	k2eAgInPc1d1	protichůdný
požadavky	požadavek	k1gInPc1	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
nízkých	nízký	k2eAgFnPc2d1	nízká
frekvencí	frekvence	k1gFnPc2	frekvence
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
reproduktor	reproduktor	k1gInSc1	reproduktor
měl	mít	k5eAaImAgInS	mít
tuhou	tuhý	k2eAgFnSc4d1	tuhá
membránu	membrána	k1gFnSc4	membrána
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
i	i	k9	i
dosti	dosti	k6eAd1	dosti
značnou	značný	k2eAgFnSc4d1	značná
hmotnost	hmotnost	k1gFnSc4	hmotnost
kmitajících	kmitající	k2eAgFnPc2d1	kmitající
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
vysokých	vysoký	k2eAgFnPc2d1	vysoká
frekvencí	frekvence	k1gFnPc2	frekvence
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
membránu	membrána	k1gFnSc4	membrána
velmi	velmi	k6eAd1	velmi
lehkou	lehký	k2eAgFnSc4d1	lehká
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
přenášet	přenášet	k5eAaImF	přenášet
celý	celý	k2eAgInSc4d1	celý
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
frekvenční	frekvenční	k2eAgInSc4d1	frekvenční
rozsah	rozsah	k1gInSc4	rozsah
pomocí	pomocí	k7c2	pomocí
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
měničů	měnič	k1gInPc2	měnič
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
optimálně	optimálně	k6eAd1	optimálně
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
pro	pro	k7c4	pro
dílčí	dílčí	k2eAgFnSc4d1	dílčí
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
úzká	úzký	k2eAgNnPc4d1	úzké
frekvenční	frekvenční	k2eAgNnPc4d1	frekvenční
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
reproduktorů	reproduktor	k1gInPc2	reproduktor
se	se	k3xPyFc4	se
sestavují	sestavovat	k5eAaImIp3nP	sestavovat
reprosoustavy	reprosoustava	k1gFnPc1	reprosoustava
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
kromě	kromě	k7c2	kromě
reproduktorů	reproduktor	k1gInPc2	reproduktor
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
často	často	k6eAd1	často
pomocné	pomocný	k2eAgInPc1d1	pomocný
akustické	akustický	k2eAgInPc1d1	akustický
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
další	další	k2eAgFnSc2d1	další
elektronické	elektronický	k2eAgFnSc2d1	elektronická
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
přívodní	přívodní	k2eAgInPc1d1	přívodní
konektory	konektor	k1gInPc1	konektor
<g/>
,	,	kIx,	,
výhybky	výhybka	k1gFnPc1	výhybka
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
zesilovače	zesilovač	k1gInPc1	zesilovač
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Širokopásmové	širokopásmový	k2eAgMnPc4d1	širokopásmový
===	===	k?	===
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
např.	např.	kA	např.
univerzální	univerzální	k2eAgInPc1d1	univerzální
reproduktory	reproduktor	k1gInPc1	reproduktor
určené	určený	k2eAgInPc1d1	určený
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
nenáročné	náročný	k2eNgNnSc4d1	nenáročné
použití	použití	k1gNnSc4	použití
např.	např.	kA	např.
v	v	k7c6	v
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
,	,	kIx,	,
televizorech	televizor	k1gInPc6	televizor
a	a	k8xC	a
levné	levný	k2eAgFnSc6d1	levná
spotřební	spotřební	k2eAgFnSc6d1	spotřební
elektronice	elektronika	k1gFnSc6	elektronika
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
maximální	maximální	k2eAgInSc1d1	maximální
frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
rozsah	rozsah	k1gInSc1	rozsah
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
45	[number]	k4	45
–	–	k?	–
15	[number]	k4	15
000	[number]	k4	000
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
ale	ale	k9	ale
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
širokopásmovými	širokopásmový	k2eAgInPc7d1	širokopásmový
reproduktory	reproduktor	k1gInPc7	reproduktor
s	s	k7c7	s
frekvenčním	frekvenční	k2eAgInSc7d1	frekvenční
rozsahem	rozsah	k1gInSc7	rozsah
55	[number]	k4	55
–	–	k?	–
13	[number]	k4	13
500	[number]	k4	500
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
vyrobit	vyrobit	k5eAaPmF	vyrobit
speciální	speciální	k2eAgInPc4d1	speciální
reproduktory	reproduktor	k1gInPc4	reproduktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
pokrýt	pokrýt	k5eAaPmF	pokrýt
celé	celý	k2eAgNnSc4d1	celé
akustické	akustický	k2eAgNnSc4d1	akustické
pásmo	pásmo	k1gNnSc4	pásmo
20	[number]	k4	20
Hz	Hz	kA	Hz
až	až	k9	až
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
širokopásmové	širokopásmový	k2eAgInPc1d1	širokopásmový
reproduktory	reproduktor	k1gInPc1	reproduktor
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgInPc4d1	vysoký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
přenos	přenos	k1gInSc4	přenos
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bývá	bývat	k5eAaImIp3nS	bývat
omezená	omezený	k2eAgFnSc1d1	omezená
zatížitelnost	zatížitelnost	k1gFnSc1	zatížitelnost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
====	====	k?	====
Koaxiální	koaxiální	k2eAgInPc1d1	koaxiální
reproduktory	reproduktor	k1gInPc1	reproduktor
====	====	k?	====
</s>
</p>
<p>
<s>
Za	za	k7c4	za
širokopásmové	širokopásmový	k2eAgInPc4d1	širokopásmový
se	se	k3xPyFc4	se
často	často	k6eAd1	často
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
označují	označovat	k5eAaImIp3nP	označovat
i	i	k9	i
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
měničů	měnič	k1gInPc2	měnič
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
je	být	k5eAaImIp3nS	být
montáž	montáž	k1gFnSc1	montáž
malého	malý	k2eAgInSc2d1	malý
vysokotónového	vysokotónový	k2eAgInSc2d1	vysokotónový
reproduktoru	reproduktor	k1gInSc2	reproduktor
na	na	k7c4	na
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
držák	držák	k1gInSc4	držák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
koše	koš	k1gInSc2	koš
<g/>
,	,	kIx,	,
před	před	k7c4	před
membránu	membrána	k1gFnSc4	membrána
hlubokotónového	hlubokotónový	k2eAgInSc2d1	hlubokotónový
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
bývá	bývat	k5eAaImIp3nS	bývat
časté	častý	k2eAgNnSc1d1	časté
u	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
pro	pro	k7c4	pro
ozvučení	ozvučení	k1gNnSc4	ozvučení
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
profesionální	profesionální	k2eAgNnSc4d1	profesionální
a	a	k8xC	a
studiové	studiový	k2eAgNnSc4d1	studiové
použití	použití	k1gNnSc4	použití
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tlaková	tlakový	k2eAgFnSc1d1	tlaková
vysokotónová	vysokotónový	k2eAgFnSc1d1	vysokotónový
jednotka	jednotka	k1gFnSc1	jednotka
umístěna	umístit	k5eAaPmNgFnS	umístit
za	za	k7c7	za
magnetickým	magnetický	k2eAgInSc7d1	magnetický
obvodem	obvod	k1gInSc7	obvod
hlubokotónového	hlubokotónový	k2eAgInSc2d1	hlubokotónový
reproduktoru	reproduktor	k1gInSc2	reproduktor
a	a	k8xC	a
otvor	otvor	k1gInSc4	otvor
v	v	k7c6	v
pólovém	pólový	k2eAgInSc6d1	pólový
nástavci	nástavec	k1gInSc6	nástavec
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc1	součást
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
středovým	středový	k2eAgInSc7d1	středový
pólovým	pólový	k2eAgInSc7d1	pólový
nástavcem	nástavec	k1gInSc7	nástavec
a	a	k8xC	a
kupolkou	kupolka	k1gFnSc7	kupolka
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Výhybka	výhybka	k1gFnSc1	výhybka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
reproduktor	reproduktor	k1gInSc1	reproduktor
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
vyvedeny	vyveden	k2eAgInPc1d1	vyveden
samostatně	samostatně	k6eAd1	samostatně
svorky	svorka	k1gFnPc4	svorka
obou	dva	k4xCgMnPc2	dva
měničů	měnič	k1gMnPc2	měnič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výhybka	výhybka	k1gFnSc1	výhybka
součástí	součást	k1gFnPc2	součást
koaxiálního	koaxiální	k2eAgInSc2d1	koaxiální
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlubokotónové	Hlubokotónový	k2eAgInPc4d1	Hlubokotónový
(	(	kIx(	(
<g/>
basové	basový	k2eAgInPc4d1	basový
<g/>
)	)	kIx)	)
reproduktory	reproduktor	k1gInPc4	reproduktor
===	===	k?	===
</s>
</p>
<p>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
20	[number]	k4	20
–	–	k?	–
1	[number]	k4	1
500	[number]	k4	500
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
nízkorezonanční	nízkorezonanční	k2eAgFnSc4d1	nízkorezonanční
o	o	k7c6	o
velkém	velký	k2eAgInSc6d1	velký
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
35	[number]	k4	35
–	–	k?	–
5	[number]	k4	5
000	[number]	k4	000
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
běžný	běžný	k2eAgInSc1d1	běžný
basový	basový	k2eAgInSc1d1	basový
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
středobasový	středobasový	k2eAgMnSc1d1	středobasový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
předpoklad	předpoklad	k1gInSc1	předpoklad
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
zdvih	zdvih	k1gInSc1	zdvih
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
tuhost	tuhost	k1gFnSc1	tuhost
zavěšení	zavěšení	k1gNnSc2	zavěšení
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
vlastní	vlastní	k2eAgFnSc1d1	vlastní
rezonance	rezonance	k1gFnSc1	rezonance
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Výchylky	výchylka	k1gFnPc1	výchylka
membrány	membrána	k1gFnSc2	membrána
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
5	[number]	k4	5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
zavěs	zavěsit	k5eAaPmRp2nS	zavěsit
membrány	membrána	k1gFnSc2	membrána
bývá	bývat	k5eAaImIp3nS	bývat
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
z	z	k7c2	z
gumy	guma	k1gFnSc2	guma
<g/>
,	,	kIx,	,
pěnové	pěnový	k2eAgFnSc2d1	pěnová
gumy	guma	k1gFnSc2	guma
<g/>
,	,	kIx,	,
polyuretanu	polyuretan	k1gInSc2	polyuretan
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
z	z	k7c2	z
impregnovaného	impregnovaný	k2eAgInSc2d1	impregnovaný
textilu	textil	k1gInSc2	textil
<g/>
.	.	kIx.	.
</s>
<s>
Membrány	membrána	k1gFnPc1	membrána
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
papírové	papírový	k2eAgInPc1d1	papírový
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
kovové	kovový	k2eAgInPc1d1	kovový
nebo	nebo	k8xC	nebo
sendvičové	sendvičový	k2eAgInPc1d1	sendvičový
<g/>
.	.	kIx.	.
</s>
<s>
Koše	koš	k1gInPc1	koš
reproduktorů	reproduktor	k1gInPc2	reproduktor
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
hliníkových	hliníkový	k2eAgFnPc2d1	hliníková
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
malých	malý	k2eAgInPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměry	průměr	k1gInPc1	průměr
reproduktorů	reproduktor	k1gInPc2	reproduktor
bývají	bývat	k5eAaImIp3nP	bývat
150	[number]	k4	150
–	–	k?	–
600	[number]	k4	600
mm	mm	kA	mm
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
udávají	udávat	k5eAaImIp3nP	udávat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
palcích	palec	k1gInPc6	palec
</s>
</p>
<p>
<s>
Výkon	výkon	k1gInSc1	výkon
na	na	k7c6	na
nižších	nízký	k2eAgFnPc6d2	nižší
frekvencích	frekvence	k1gFnPc6	frekvence
je	být	k5eAaImIp3nS	být
úměrný	úměrný	k2eAgInSc1d1	úměrný
druhé	druhý	k4xOgFnSc3	druhý
mocnině	mocnina	k1gFnSc3	mocnina
součinu	součin	k1gInSc2	součin
efektivní	efektivní	k2eAgFnSc2d1	efektivní
plochy	plocha	k1gFnSc2	plocha
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
výchylky	výchylka	k1gFnSc2	výchylka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
reproduktorů	reproduktor	k1gInPc2	reproduktor
mívá	mívat	k5eAaImIp3nS	mívat
střední	střední	k2eAgInSc1d1	střední
pólový	pólový	k2eAgInSc1d1	pólový
nástavec	nástavec	k1gInSc1	nástavec
otvor	otvor	k1gInSc4	otvor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzduch	vzduch	k1gInSc1	vzduch
mohl	moct	k5eAaImAgInS	moct
proudit	proudit	k5eAaPmF	proudit
kolem	kolem	k7c2	kolem
kmitací	kmitací	k2eAgFnSc2d1	kmitací
cívky	cívka	k1gFnSc2	cívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středopásmové	Středopásmový	k2eAgInPc4d1	Středopásmový
(	(	kIx(	(
<g/>
středotónové	středotónový	k2eAgInPc4d1	středotónový
<g/>
)	)	kIx)	)
reproduktory	reproduktor	k1gInPc4	reproduktor
===	===	k?	===
</s>
</p>
<p>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
80	[number]	k4	80
–	–	k?	–
12	[number]	k4	12
000	[number]	k4	000
Hz	Hz	kA	Hz
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Materiál	materiál	k1gInSc1	materiál
membrán	membrána	k1gFnPc2	membrána
bývá	bývat	k5eAaImIp3nS	bývat
papírovina	papírovina	k1gFnSc1	papírovina
<g/>
,	,	kIx,	,
kevlar	kevlar	k1gInSc1	kevlar
<g/>
,	,	kIx,	,
polypropylen	polypropylen	k1gInSc1	polypropylen
<g/>
,	,	kIx,	,
sendvič	sendvič	k1gInSc1	sendvič
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
středotónových	středotónový	k2eAgInPc2d1	středotónový
reproduktorů	reproduktor	k1gInPc2	reproduktor
je	být	k5eAaImIp3nS	být
kladen	kladen	k2eAgInSc1d1	kladen
důraz	důraz	k1gInSc1	důraz
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
nízké	nízký	k2eAgNnSc4d1	nízké
zkreslení	zkreslení	k1gNnSc4	zkreslení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
lidský	lidský	k2eAgInSc4d1	lidský
sluch	sluch	k1gInSc4	sluch
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
nejcitlivější	citlivý	k2eAgMnSc1d3	nejcitlivější
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klasických	klasický	k2eAgFnPc2d1	klasická
kónusových	kónusový	k2eAgFnPc2d1	kónusová
membrán	membrána	k1gFnPc2	membrána
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kalotové	kalotový	k2eAgInPc1d1	kalotový
(	(	kIx(	(
<g/>
membrána	membrána	k1gFnSc1	membrána
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
vrchlíku	vrchlík	k1gInSc2	vrchlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměry	průměr	k1gInPc1	průměr
membrán	membrána	k1gFnPc2	membrána
reproduktorů	reproduktor	k1gInPc2	reproduktor
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
50	[number]	k4	50
–	–	k?	–
180	[number]	k4	180
mm	mm	kA	mm
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
HiFi	Hifi	k1gNnSc6	Hifi
soustavách	soustava	k1gFnPc6	soustava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
profesionální	profesionální	k2eAgInPc4d1	profesionální
účely	účel	k1gInPc4	účel
i	i	k9	i
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
montovány	montovat	k5eAaImNgInP	montovat
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
ozvučnici	ozvučnice	k1gFnSc6	ozvučnice
s	s	k7c7	s
hlubokotónovým	hlubokotónový	k2eAgInSc7d1	hlubokotónový
reproduktorem	reproduktor	k1gInSc7	reproduktor
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
koš	koš	k1gInSc4	koš
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
montují	montovat	k5eAaImIp3nP	montovat
do	do	k7c2	do
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
krytu	kryt	k1gInSc2	kryt
<g/>
,	,	kIx,	,
aby	aby	k9	aby
je	být	k5eAaImIp3nS	být
hlubokotónový	hlubokotónový	k2eAgInSc1d1	hlubokotónový
reproduktor	reproduktor	k1gInSc1	reproduktor
neovlivňoval	ovlivňovat	k5eNaImAgInS	ovlivňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vysokotónové	Vysokotónový	k2eAgInPc4d1	Vysokotónový
(	(	kIx(	(
<g/>
výškové	výškový	k2eAgInPc4d1	výškový
<g/>
)	)	kIx)	)
reproduktory	reproduktor	k1gInPc4	reproduktor
===	===	k?	===
</s>
</p>
<p>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
2	[number]	k4	2
000	[number]	k4	000
do	do	k7c2	do
20	[number]	k4	20
000	[number]	k4	000
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
více	hodně	k6eAd2	hodně
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Membrána	membrána	k1gFnSc1	membrána
nemá	mít	k5eNaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
požadavku	požadavek	k1gInSc2	požadavek
co	co	k9	co
nejširšího	široký	k2eAgNnSc2d3	nejširší
vyzařování	vyzařování	k1gNnSc2	vyzařování
tvar	tvar	k1gInSc4	tvar
kužele	kužel	k1gInSc2	kužel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kulového	kulový	k2eAgInSc2d1	kulový
vrchlíku	vrchlík	k1gInSc2	vrchlík
–	–	k?	–
kaloty	kalota	k1gFnSc2	kalota
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
membrány	membrána	k1gFnSc2	membrána
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
do	do	k7c2	do
30	[number]	k4	30
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Materiály	materiál	k1gInPc1	materiál
membrán	membrána	k1gFnPc2	membrána
jsou	být	k5eAaImIp3nP	být
plasty	plast	k1gInPc4	plast
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInPc4d1	textilní
úplety	úplet	k1gInPc4	úplet
(	(	kIx(	(
<g/>
hedvábné	hedvábný	k2eAgInPc4d1	hedvábný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sendvičové	sendvičový	k2eAgFnPc1d1	sendvičová
konstrukce	konstrukce	k1gFnPc1	konstrukce
(	(	kIx(	(
<g/>
na	na	k7c4	na
textilní	textilní	k2eAgInSc4d1	textilní
základ	základ	k1gInSc4	základ
se	se	k3xPyFc4	se
napařuje	napařovat	k5eAaImIp3nS	napařovat
vrstva	vrstva	k1gFnSc1	vrstva
kovu	kov	k1gInSc2	kov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kovové	kovový	k2eAgInPc1d1	kovový
(	(	kIx(	(
<g/>
titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
dural	dural	k1gInSc1	dural
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
<g/>
,	,	kIx,	,
aerogel	aerogel	k1gInSc1	aerogel
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
vysokotónové	vysokotónový	k2eAgInPc1d1	vysokotónový
reproduktory	reproduktor	k1gInPc1	reproduktor
mají	mít	k5eAaImIp3nP	mít
kmitací	kmitací	k2eAgFnSc4d1	kmitací
cívku	cívka	k1gFnSc4	cívka
chlazenou	chlazený	k2eAgFnSc4d1	chlazená
ferrofluidem	ferrofluid	k1gInSc7	ferrofluid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
tlumí	tlumit	k5eAaImIp3nS	tlumit
rezonance	rezonance	k1gFnSc1	rezonance
systému	systém	k1gInSc2	systém
</s>
</p>
<p>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
i	i	k9	i
páskové	páskový	k2eAgInPc1d1	páskový
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
zvuková	zvukový	k2eAgFnSc1d1	zvuková
vlna	vlna	k1gFnSc1	vlna
vyzařována	vyzařován	k2eAgFnSc1d1	vyzařována
pomocí	pomocí	k7c2	pomocí
kaloty	kalota	k1gFnSc2	kalota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kmitajícího	kmitající	k2eAgInSc2d1	kmitající
pásku	pásek	k1gInSc2	pásek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
nenáročné	náročný	k2eNgNnSc4d1	nenáročné
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
levné	levný	k2eAgFnSc6d1	levná
spotřební	spotřební	k2eAgFnSc6d1	spotřební
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
přenosných	přenosný	k2eAgInPc6d1	přenosný
radiomagnetofonech	radiomagnetofon	k1gInPc6	radiomagnetofon
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
levných	levný	k2eAgInPc6d1	levný
ozvučovacích	ozvučovací	k2eAgInPc6d1	ozvučovací
systémech	systém	k1gInPc6	systém
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
piezoelektrické	piezoelektrický	k2eAgFnPc1d1	piezoelektrická
vysokotónové	vysokotónový	k2eAgFnPc1d1	vysokotónový
jednotky	jednotka	k1gFnPc1	jednotka
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
profesionální	profesionální	k2eAgNnSc4d1	profesionální
použití	použití	k1gNnSc4	použití
(	(	kIx(	(
<g/>
PA	Pa	kA	Pa
systémy	systém	k1gInPc1	systém
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vysokotónové	vysokotónový	k2eAgInPc1d1	vysokotónový
reproduktory	reproduktor	k1gInPc1	reproduktor
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
nejčastěji	často	k6eAd3	často
jako	jako	k9	jako
tlakové	tlakový	k2eAgInPc1d1	tlakový
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
účinností	účinnost	k1gFnSc7	účinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tlakové	tlakový	k2eAgInPc1d1	tlakový
reproduktory	reproduktor	k1gInPc1	reproduktor
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgNnSc4d1	různé
provedení	provedení	k1gNnSc4	provedení
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rotačně	rotačně	k6eAd1	rotačně
symetrické	symetrický	k2eAgFnPc1d1	symetrická
<g/>
,	,	kIx,	,
tak	tak	k9	tak
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
nebo	nebo	k8xC	nebo
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Zvukovod	zvukovod	k1gInSc1	zvukovod
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
podílí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
tvarování	tvarování	k1gNnSc6	tvarování
směrového	směrový	k2eAgInSc2d1	směrový
diagramu	diagram	k1gInSc2	diagram
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
reproduktory	reproduktor	k1gInPc1	reproduktor
mají	mít	k5eAaImIp3nP	mít
úmyslně	úmyslně	k6eAd1	úmyslně
jiný	jiný	k2eAgInSc4d1	jiný
vyzařovací	vyzařovací	k2eAgInSc4d1	vyzařovací
úhel	úhel	k1gInSc4	úhel
ve	v	k7c6	v
vertikální	vertikální	k2eAgFnSc6d1	vertikální
a	a	k8xC	a
horizontální	horizontální	k2eAgFnSc3d1	horizontální
rovině	rovina	k1gFnSc3	rovina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
90	[number]	k4	90
x	x	k?	x
60	[number]	k4	60
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvukovod	zvukovod	k1gInSc1	zvukovod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
reproduktory	reproduktor	k1gInPc1	reproduktor
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
jako	jako	k9	jako
samostatné	samostatný	k2eAgFnPc1d1	samostatná
jednotky	jednotka	k1gFnPc1	jednotka
bez	bez	k7c2	bez
zvukovodu	zvukovod	k1gInSc2	zvukovod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
drivery	driver	k1gInPc1	driver
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvukovody	zvukovod	k1gInPc1	zvukovod
(	(	kIx(	(
<g/>
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dodávají	dodávat	k5eAaImIp3nP	dodávat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
kombinací	kombinace	k1gFnSc7	kombinace
driveru	driver	k1gInSc2	driver
a	a	k8xC	a
zvukovodu	zvukovod	k1gInSc2	zvukovod
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnPc4	část
elektrodynamického	elektrodynamický	k2eAgInSc2d1	elektrodynamický
reproduktoru	reproduktor	k1gInSc2	reproduktor
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Koš	koš	k1gInSc1	koš
reproduktoru	reproduktor	k1gInSc2	reproduktor
===	===	k?	===
</s>
</p>
<p>
<s>
Koš	koš	k1gInSc1	koš
reproduktoru	reproduktor	k1gInSc2	reproduktor
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
nosnou	nosný	k2eAgFnSc7d1	nosná
konstrukcí	konstrukce	k1gFnSc7	konstrukce
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
připevnění	připevnění	k1gNnSc4	připevnění
reproduktoru	reproduktor	k1gInSc2	reproduktor
do	do	k7c2	do
skříňky	skříňka	k1gFnSc2	skříňka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nese	nést	k5eAaImIp3nS	nést
elektromagnetický	elektromagnetický	k2eAgInSc1d1	elektromagnetický
obvod	obvod	k1gInSc1	obvod
<g/>
,	,	kIx,	,
závěsy	závěs	k1gInPc1	závěs
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
připojovací	připojovací	k2eAgInSc4d1	připojovací
terminál	terminál	k1gInSc4	terminál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
výškových	výškový	k2eAgInPc2d1	výškový
reproduktorů	reproduktor	k1gInPc2	reproduktor
bývá	bývat	k5eAaImIp3nS	bývat
bez	bez	k7c2	bez
otvorů	otvor	k1gInPc2	otvor
<g/>
,	,	kIx,	,
u	u	k7c2	u
hlubokotónových	hlubokotónový	k2eAgMnPc2d1	hlubokotónový
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
co	co	k9	co
největšími	veliký	k2eAgInPc7d3	veliký
otvory	otvor	k1gInPc7	otvor
aby	aby	kYmCp3nS	aby
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
vzduch	vzduch	k1gInSc1	vzduch
pod	pod	k7c7	pod
membránou	membrána	k1gFnSc7	membrána
negativně	negativně	k6eAd1	negativně
nezvyšoval	zvyšovat	k5eNaImAgInS	zvyšovat
tuhost	tuhost	k1gFnSc4	tuhost
zavěšení	zavěšení	k1gNnSc2	zavěšení
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
nezhoršoval	zhoršovat	k5eNaImAgInS	zhoršovat
tak	tak	k6eAd1	tak
akustické	akustický	k2eAgFnPc4d1	akustická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Ohraničení	ohraničení	k1gNnSc1	ohraničení
otvorů	otvor	k1gInPc2	otvor
mívá	mívat	k5eAaImIp3nS	mívat
aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
tvar	tvar	k1gInSc1	tvar
pro	pro	k7c4	pro
co	co	k3yQnSc4	co
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
proudění	proudění	k1gNnSc4	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
koše	koš	k1gInSc2	koš
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
co	co	k3yRnSc4	co
nejpevnější	pevný	k2eAgFnSc1d3	nejpevnější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
antirezonanční	antirezonanční	k2eAgFnPc4d1	antirezonanční
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
levných	levný	k2eAgInPc2d1	levný
reproduktorů	reproduktor	k1gInPc2	reproduktor
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
koše	koš	k1gInPc1	koš
vylisované	vylisovaný	k2eAgInPc1d1	vylisovaný
z	z	k7c2	z
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
polyamid	polyamid	k1gInSc1	polyamid
či	či	k8xC	či
jiný	jiný	k2eAgInSc1d1	jiný
plast	plast	k1gInSc1	plast
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
vlhku	vlhko	k1gNnSc6	vlhko
<g/>
.	.	kIx.	.
</s>
<s>
Dražší	drahý	k2eAgInPc1d2	dražší
reproduktory	reproduktor	k1gInPc1	reproduktor
mívají	mívat	k5eAaImIp3nP	mívat
koš	koš	k1gInSc1	koš
ze	z	k7c2	z
silnostěnného	silnostěnný	k2eAgInSc2d1	silnostěnný
odlitku	odlitek	k1gInSc2	odlitek
slitin	slitina	k1gFnPc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koš	koš	k1gInSc1	koš
mívá	mívat	k5eAaImIp3nS	mívat
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
otvory	otvor	k1gInPc7	otvor
pro	pro	k7c4	pro
upevňovací	upevňovací	k2eAgInPc4d1	upevňovací
šrouby	šroub	k1gInPc4	šroub
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
je	být	k5eAaImIp3nS	být
reproduktor	reproduktor	k1gInSc1	reproduktor
uchycen	uchycen	k2eAgInSc1d1	uchycen
k	k	k7c3	k
ozvučnici	ozvučnice	k1gFnSc3	ozvučnice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
reproduktorů	reproduktor	k1gInPc2	reproduktor
tyto	tento	k3xDgInPc1	tento
otvory	otvor	k1gInPc1	otvor
často	často	k6eAd1	často
chybí	chybět	k5eAaImIp3nP	chybět
<g/>
,	,	kIx,	,
reproduktory	reproduktor	k1gInPc1	reproduktor
se	se	k3xPyFc4	se
upevňují	upevňovat	k5eAaImIp3nP	upevňovat
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgInPc2d1	různý
úchytů	úchyt	k1gInPc2	úchyt
<g/>
,	,	kIx,	,
rámečků	rámeček	k1gInPc2	rámeček
nebo	nebo	k8xC	nebo
i	i	k9	i
lepením	lepení	k1gNnSc7	lepení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
vysokotónových	vysokotónový	k2eAgInPc2d1	vysokotónový
reproduktorů	reproduktor	k1gInPc2	reproduktor
a	a	k8xC	a
leckterých	leckterý	k3yIgFnPc2	leckterý
nepřímovyzařujících	přímovyzařující	k2eNgFnPc2d1	přímovyzařující
jednotek	jednotka	k1gFnPc2	jednotka
koš	koš	k1gInSc1	koš
jako	jako	k8xS	jako
takový	takový	k3xDgInSc4	takový
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Membránový	membránový	k2eAgInSc1d1	membránový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
zvukovod	zvukovod	k1gInSc1	zvukovod
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
montovány	montován	k2eAgFnPc1d1	montována
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
magnetickému	magnetický	k2eAgInSc3d1	magnetický
obvodu	obvod	k1gInSc3	obvod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ozvučnici	ozvučnice	k1gFnSc3	ozvučnice
se	se	k3xPyFc4	se
reproduktor	reproduktor	k1gMnSc1	reproduktor
často	často	k6eAd1	často
uchycuje	uchycovat	k5eAaImIp3nS	uchycovat
pomocí	pomocí	k7c2	pomocí
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
reproduktorů	reproduktor	k1gInPc2	reproduktor
s	s	k7c7	s
membránou	membrána	k1gFnSc7	membrána
a	a	k8xC	a
košem	koš	k1gInSc7	koš
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
reproduktory	reproduktor	k1gInPc1	reproduktor
eliptické	eliptický	k2eAgInPc1d1	eliptický
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
koš	koš	k1gInSc1	koš
i	i	k8xC	i
membrána	membrána	k1gFnSc1	membrána
eliptického	eliptický	k2eAgInSc2d1	eliptický
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
to	ten	k3xDgNnSc1	ten
bývalo	bývat	k5eAaImAgNnS	bývat
u	u	k7c2	u
širokopásmových	širokopásmový	k2eAgMnPc2d1	širokopásmový
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
TV	TV	kA	TV
přijímače	přijímač	k1gMnSc2	přijímač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Magnetický	magnetický	k2eAgInSc1d1	magnetický
obvod	obvod	k1gInSc1	obvod
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
základní	základní	k2eAgFnSc1d1	základní
součást	součást	k1gFnSc1	součást
pohonu	pohon	k1gInSc2	pohon
elektrodynamického	elektrodynamický	k2eAgInSc2d1	elektrodynamický
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
ve	v	k7c6	v
válcové	válcový	k2eAgFnSc6d1	válcová
vzduchové	vzduchový	k2eAgFnSc6d1	vzduchová
mezeře	mezera	k1gFnSc6	mezera
mezi	mezi	k7c7	mezi
pólovými	pólový	k2eAgInPc7d1	pólový
nástavci	nástavec	k1gInPc7	nástavec
je	být	k5eAaImIp3nS	být
vybuzováno	vybuzován	k2eAgNnSc1d1	vybuzován
permanentním	permanentní	k2eAgInSc7d1	permanentní
magnetem	magnet	k1gInSc7	magnet
uloženým	uložený	k2eAgInSc7d1	uložený
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
vnějším	vnější	k2eAgInSc6d1	vnější
plášti	plášť	k1gInSc6	plášť
magnetického	magnetický	k2eAgInSc2d1	magnetický
obvodu	obvod	k1gInSc2	obvod
nebo	nebo	k8xC	nebo
tvoří	tvořit	k5eAaImIp3nP	tvořit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
součást	součást	k1gFnSc4	součást
středového	středový	k2eAgInSc2d1	středový
sloupku	sloupek	k1gInSc2	sloupek
magnetického	magnetický	k2eAgInSc2d1	magnetický
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Magnet	magnet	k1gInSc1	magnet
bývá	bývat	k5eAaImIp3nS	bývat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
složen	složit	k5eAaPmNgInS	složit
i	i	k9	i
z	z	k7c2	z
několika	několik	k4yIc2	několik
shodných	shodný	k2eAgInPc2d1	shodný
<g/>
,	,	kIx,	,
umístěných	umístěný	k2eAgInPc2d1	umístěný
na	na	k7c6	na
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
segmentů	segment	k1gInPc2	segment
<g/>
,	,	kIx,	,
umístěných	umístěný	k2eAgFnPc2d1	umístěná
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnety	magnet	k1gInPc1	magnet
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
nejčastěji	často	k6eAd3	často
feritové	feritový	k2eAgInPc1d1	feritový
<g/>
,	,	kIx,	,
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
AlNiCo	AlNiCo	k6eAd1	AlNiCo
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
drahých	drahý	k2eAgInPc2d1	drahý
reproduktorů	reproduktor	k1gInPc2	reproduktor
pro	pro	k7c4	pro
profesionální	profesionální	k2eAgNnSc4d1	profesionální
použití	použití	k1gNnSc4	použití
<g/>
)	)	kIx)	)
neodymové	odymový	k2eNgInPc1d1	neodymový
magnety	magnet	k1gInPc1	magnet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
až	až	k9	až
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bývalo	bývat	k5eAaImAgNnS	bývat
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
elektromagnet	elektromagnet	k1gInSc1	elektromagnet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
tlumivka	tlumivka	k1gFnSc1	tlumivka
napájecího	napájecí	k2eAgInSc2d1	napájecí
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Pólové	pólový	k2eAgInPc1d1	pólový
nástavce	nástavec	k1gInPc1	nástavec
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
vybaveny	vybaven	k2eAgInPc4d1	vybaven
zkratovacími	zkratovací	k2eAgInPc7d1	zkratovací
prstenci	prstenec	k1gInPc7	prstenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
reproduktorů	reproduktor	k1gInPc2	reproduktor
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
pólový	pólový	k2eAgInSc1d1	pólový
nástavec	nástavec	k1gInSc1	nástavec
plný	plný	k2eAgInSc1d1	plný
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
má	mít	k5eAaImIp3nS	mít
otvor	otvor	k1gInSc1	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
otvor	otvor	k1gInSc1	otvor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
zvukovodu	zvukovod	k1gInSc2	zvukovod
některých	některý	k3yIgInPc2	některý
výškových	výškový	k2eAgInPc2d1	výškový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
kmitačky	kmitačka	k1gFnSc2	kmitačka
(	(	kIx(	(
<g/>
chlazení	chlazení	k1gNnSc1	chlazení
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc1	zvýšení
poddajnosti	poddajnost	k1gFnSc2	poddajnost
celého	celý	k2eAgInSc2d1	celý
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
instalaci	instalace	k1gFnSc4	instalace
do	do	k7c2	do
klasických	klasický	k2eAgInPc2d1	klasický
televizorů	televizor	k1gInPc2	televizor
s	s	k7c7	s
CRT	CRT	kA	CRT
obrazovkou	obrazovka	k1gFnSc7	obrazovka
a	a	k8xC	a
pro	pro	k7c4	pro
soustavy	soustava	k1gFnPc4	soustava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
televizorů	televizor	k1gInPc2	televizor
a	a	k8xC	a
monitorů	monitor	k1gInPc2	monitor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
magnetický	magnetický	k2eAgInSc1d1	magnetický
obvod	obvod	k1gInSc1	obvod
stíněný	stíněný	k2eAgInSc1d1	stíněný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
omezen	omezen	k2eAgInSc1d1	omezen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vnější	vnější	k2eAgFnSc1d1	vnější
mag	mag	k?	mag
<g/>
.	.	kIx.	.
pole	pole	k1gNnSc1	pole
bylo	být	k5eAaImAgNnS	být
co	co	k3yRnSc1	co
nejmenší	malý	k2eAgNnSc1d3	nejmenší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kmitací	kmitací	k2eAgFnSc1d1	kmitací
cívka	cívka	k1gFnSc1	cívka
===	===	k?	===
</s>
</p>
<p>
<s>
Kmitací	kmitací	k2eAgFnSc1d1	kmitací
cívka	cívka	k1gFnSc1	cívka
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
částí	část	k1gFnSc7	část
pohonu	pohon	k1gInSc2	pohon
elektrodynamického	elektrodynamický	k2eAgInSc2d1	elektrodynamický
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
vinutí	vinutí	k1gNnSc4	vinutí
z	z	k7c2	z
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
vodiče	vodič	k1gInSc2	vodič
(	(	kIx(	(
<g/>
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
poměděný	poměděný	k2eAgInSc1d1	poměděný
hliník	hliník	k1gInSc1	hliník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navinuté	navinutý	k2eAgInPc1d1	navinutý
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
formeru	former	k1gInSc6	former
(	(	kIx(	(
<g/>
cívkové	cívkový	k2eAgNnSc1d1	cívkové
těleso	těleso	k1gNnSc1	těleso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Former	Former	k1gMnSc1	Former
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
i	i	k9	i
jako	jako	k8xS	jako
část	část	k1gFnSc1	část
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vodič	vodič	k1gMnSc1	vodič
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
i	i	k9	i
profilovaného	profilovaný	k2eAgInSc2d1	profilovaný
drátu	drát	k1gInSc2	drát
(	(	kIx(	(
<g/>
čtvercový	čtvercový	k2eAgInSc1d1	čtvercový
průřez	průřez	k1gInSc1	průřez
nebo	nebo	k8xC	nebo
pásek	pásek	k1gInSc1	pásek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vinutí	vinutí	k1gNnSc1	vinutí
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
středových	středový	k2eAgInPc2d1	středový
a	a	k8xC	a
vysokotónových	vysokotónový	k2eAgInPc2d1	vysokotónový
tlakových	tlakový	k2eAgInPc2d1	tlakový
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
4	[number]	k4	4
vrstvy	vrstva	k1gFnPc1	vrstva
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
basové	basový	k2eAgInPc1d1	basový
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Former	Former	k1gMnSc1	Former
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
papíroviny	papírovina	k1gFnSc2	papírovina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgInPc4d2	vyšší
výkony	výkon	k1gInPc4	výkon
z	z	k7c2	z
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
kaptonu	kapton	k1gInSc2	kapton
<g/>
,	,	kIx,	,
skelných	skelný	k2eAgNnPc2d1	skelné
vláken	vlákno	k1gNnPc2	vlákno
apod.	apod.	kA	apod.
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
bývá	bývat	k5eAaImIp3nS	bývat
samonosné	samonosný	k2eAgNnSc1d1	samonosné
vinutí	vinutí	k1gNnSc1	vinutí
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc1d1	vlastní
vinutí	vinutí	k1gNnSc1	vinutí
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
prosyceno	prosytit	k5eAaPmNgNnS	prosytit
vhodným	vhodný	k2eAgNnSc7d1	vhodné
lepidlem	lepidlo	k1gNnSc7	lepidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mají	mít	k5eAaImIp3nP	mít
kmitací	kmitací	k2eAgFnPc1d1	kmitací
cívky	cívka	k1gFnPc1	cívka
2	[number]	k4	2
samostatná	samostatný	k2eAgNnPc4d1	samostatné
vinutí	vinutí	k1gNnPc4	vinutí
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
dvoucívkové	dvoucívkový	k2eAgInPc1d1	dvoucívkový
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
subwoofery	subwoofer	k1gMnPc4	subwoofer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
cívku	cívka	k1gFnSc4	cívka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
výkonných	výkonný	k2eAgInPc2d1	výkonný
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kladeny	klást	k5eAaImNgInP	klást
značné	značný	k2eAgInPc1d1	značný
nároky	nárok	k1gInPc1	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nP	muset
snášet	snášet	k5eAaImF	snášet
teploty	teplota	k1gFnPc1	teplota
vznikající	vznikající	k2eAgFnPc1d1	vznikající
při	při	k7c6	při
vysokém	vysoký	k2eAgNnSc6d1	vysoké
zatížení	zatížení	k1gNnSc6	zatížení
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
tuhá	tuhý	k2eAgNnPc4d1	tuhé
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
deformovat	deformovat	k5eAaImF	deformovat
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
–	–	k?	–
zejména	zejména	k9	zejména
u	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgFnPc4d1	vysoká
frekvence	frekvence	k1gFnPc4	frekvence
–	–	k?	–
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
co	co	k9	co
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Membrána	membrána	k1gFnSc1	membrána
===	===	k?	===
</s>
</p>
<p>
<s>
Tvar	tvar	k1gInSc1	tvar
membrány	membrána	k1gFnSc2	membrána
basových	basový	k2eAgInPc2d1	basový
a	a	k8xC	a
středových	středový	k2eAgInPc2d1	středový
reproduktorů	reproduktor	k1gInPc2	reproduktor
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
kuželový	kuželový	k2eAgMnSc1d1	kuželový
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
rozvinutelném	rozvinutelný	k2eAgInSc6d1	rozvinutelný
nebo	nebo	k8xC	nebo
nerozvinutelném	rozvinutelný	k2eNgInSc6d1	rozvinutelný
tvaru	tvar	k1gInSc6	tvar
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
NAWI	NAWI	kA	NAWI
tvar-	tvar-	k?	tvar-
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
něm.	něm.	k?	něm.
nicht	nicht	k2eAgMnSc1d1	nicht
abwickelbar	abwickelbar	k1gMnSc1	abwickelbar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
širokopásmových	širokopásmový	k2eAgInPc2d1	širokopásmový
reproduktorů	reproduktor	k1gInPc2	reproduktor
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
lehká	lehký	k2eAgFnSc1d1	lehká
pomocná	pomocný	k2eAgFnSc1d1	pomocná
membrána	membrána	k1gFnSc1	membrána
menšího	malý	k2eAgInSc2d2	menší
průměru	průměr	k1gInSc2	průměr
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
upevněna	upevněn	k2eAgFnSc1d1	upevněna
před	před	k7c7	před
hlavní	hlavní	k2eAgFnSc7d1	hlavní
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
vyzařování	vyzařování	k1gNnSc1	vyzařování
vysokých	vysoký	k2eAgFnPc2d1	vysoká
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
reproduktorů	reproduktor	k1gInPc2	reproduktor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
membráně	membrána	k1gFnSc6	membrána
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
vlnek	vlnka	k1gFnPc2	vlnka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
membránu	membrána	k1gFnSc4	membrána
na	na	k7c4	na
několik	několik	k4yIc4	několik
koncentrických	koncentrický	k2eAgNnPc2d1	koncentrické
mezikruží	mezikruží	k1gNnPc2	mezikruží
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnPc4d1	nízká
frekvence	frekvence	k1gFnPc4	frekvence
pak	pak	k6eAd1	pak
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
celá	celý	k2eAgFnSc1d1	celá
plocha	plocha	k1gFnSc1	plocha
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
jen	jen	k9	jen
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
mezikruží	mezikruží	k1gNnSc1	mezikruží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
výškových	výškový	k2eAgFnPc2d1	výšková
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
tvar	tvar	k1gInSc4	tvar
vrchlíku	vrchlík	k1gInSc2	vrchlík
(	(	kIx(	(
<g/>
kalota	kalota	k1gFnSc1	kalota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mezikruží	mezikruží	k1gNnSc1	mezikruží
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
tlakových	tlakový	k2eAgFnPc2d1	tlaková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Materiály	materiál	k1gInPc4	materiál
membrán	membrána	k1gFnPc2	membrána
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
<g/>
:	:	kIx,	:
Nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
nanášením	nanášení	k1gNnSc7	nanášení
papírové	papírový	k2eAgFnSc2d1	papírová
suspenze	suspenze	k1gFnSc2	suspenze
na	na	k7c4	na
speciální	speciální	k2eAgNnPc4d1	speciální
síta	síto	k1gNnPc4	síto
nástřikem	nástřik	k1gInSc7	nástřik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
levný	levný	k2eAgInSc1d1	levný
<g/>
,	,	kIx,	,
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
basových	basový	k2eAgInPc2d1	basový
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zvýšení	zvýšení	k1gNnSc2	zvýšení
tuhosti	tuhost	k1gFnSc2	tuhost
<g/>
,	,	kIx,	,
odolnosti	odolnost	k1gFnSc6	odolnost
vůči	vůči	k7c3	vůči
vlhku	vlhko	k1gNnSc3	vlhko
a	a	k8xC	a
k	k	k7c3	k
utlumení	utlumení	k1gNnSc3	utlumení
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
vlastních	vlastní	k2eAgInPc2d1	vlastní
kmitů	kmit	k1gInPc2	kmit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
papírovou	papírový	k2eAgFnSc4d1	papírová
membránu	membrána	k1gFnSc4	membrána
většinou	většina	k1gFnSc7	většina
nanáší	nanášet	k5eAaImIp3nP	nanášet
speciální	speciální	k2eAgInSc4d1	speciální
lak	lak	k1gInSc4	lak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polypropylen	polypropylen	k1gInSc1	polypropylen
<g/>
:	:	kIx,	:
Levná	levný	k2eAgFnSc1d1	levná
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
tuhost	tuhost	k1gFnSc1	tuhost
membrány	membrána	k1gFnSc2	membrána
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
dalších	další	k2eAgFnPc2d1	další
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
vlhkosti	vlhkost	k1gFnSc3	vlhkost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tkaniny	tkanina	k1gFnPc1	tkanina
prosycené	prosycený	k2eAgInPc1d1	prosycený
fenolickými	fenolický	k2eAgFnPc7d1	fenolická
pryskyřicemi	pryskyřice	k1gFnPc7	pryskyřice
–	–	k?	–
časté	častý	k2eAgNnSc1d1	časté
u	u	k7c2	u
levných	levný	k2eAgInPc2d1	levný
tlakových	tlakový	k2eAgInPc2d1	tlakový
reproduktorů	reproduktor	k1gInPc2	reproduktor
</s>
</p>
<p>
<s>
Kevlar	kevlar	k1gInSc1	kevlar
nebo	nebo	k8xC	nebo
uhlíková	uhlíkový	k2eAgNnPc1d1	uhlíkové
vlákna	vlákno	k1gNnPc1	vlákno
spojená	spojený	k2eAgFnSc1d1	spojená
speciální	speciální	k2eAgFnSc7d1	speciální
pryskyřicí	pryskyřice	k1gFnSc7	pryskyřice
–	–	k?	–
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgNnSc1d1	drahé
(	(	kIx(	(
<g/>
výrobcem	výrobce	k1gMnSc7	výrobce
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
firma	firma	k1gFnSc1	firma
B	B	kA	B
<g/>
&	&	k?	&
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
kevlarových	kevlarův	k2eAgNnPc2d1	kevlarův
vláken	vlákno	k1gNnPc2	vlákno
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
lacinější	laciný	k2eAgNnPc1d2	lacinější
vlákna	vlákno	k1gNnPc1	vlákno
skelná	skelný	k2eAgNnPc1d1	skelné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kovové	kovový	k2eAgFnPc4d1	kovová
membrány	membrána	k1gFnPc4	membrána
<g/>
:	:	kIx,	:
Většinou	většinou	k6eAd1	většinou
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
nebo	nebo	k8xC	nebo
titanové	titanový	k2eAgInPc1d1	titanový
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
tuhost	tuhost	k1gFnSc1	tuhost
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyšší	vysoký	k2eAgFnSc1d2	vyšší
cena	cena	k1gFnSc1	cena
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
vlastní	vlastní	k2eAgFnSc2d1	vlastní
rezonance	rezonance	k1gFnSc2	rezonance
na	na	k7c6	na
vyšších	vysoký	k2eAgFnPc6d2	vyšší
frekvencích	frekvence	k1gFnPc6	frekvence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sendvičové	sendvičový	k2eAgFnPc1d1	sendvičová
membrány	membrána	k1gFnPc1	membrána
<g/>
:	:	kIx,	:
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
více	hodně	k6eAd2	hodně
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
papír	papír	k1gInSc1	papír
+	+	kIx~	+
kov	kov	k1gInSc1	kov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
okrajově	okrajově	k6eAd1	okrajově
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
drahé	drahá	k1gFnPc4	drahá
HiFi	Hifi	k1gNnPc2	Hifi
reproduktory	reproduktor	k1gInPc7	reproduktor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
další	další	k2eAgInPc1d1	další
materiály	materiál	k1gInPc1	materiál
–	–	k?	–
např.	např.	kA	např.
kaloty	kalota	k1gFnSc2	kalota
pro	pro	k7c4	pro
zpevnění	zpevnění	k1gNnSc4	zpevnění
potažené	potažený	k2eAgFnSc2d1	potažená
oxidem	oxid	k1gInSc7	oxid
hlinitým	hlinitý	k2eAgInSc7d1	hlinitý
<g/>
,	,	kIx,	,
sloučeninami	sloučenina	k1gFnPc7	sloučenina
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
membrány	membrána	k1gFnSc2	membrána
z	z	k7c2	z
aerogelu	aerogel	k1gInSc2	aerogel
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zavěšení	zavěšení	k1gNnPc4	zavěšení
membrány	membrána	k1gFnSc2	membrána
===	===	k?	===
</s>
</p>
<p>
<s>
Membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
připevněna	připevnit	k5eAaPmNgFnS	připevnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
pístově	pístově	k6eAd1	pístově
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
reproduktorů	reproduktor	k1gInPc2	reproduktor
s	s	k7c7	s
kónickou	kónický	k2eAgFnSc7d1	kónická
membránou	membrána	k1gFnSc7	membrána
bývá	bývat	k5eAaImIp3nS	bývat
membrána	membrána	k1gFnSc1	membrána
zavěšena	zavěsit	k5eAaPmNgFnS	zavěsit
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
vnějším	vnější	k2eAgInSc6d1	vnější
obvodu	obvod	k1gInSc6	obvod
závěsem	závěs	k1gInSc7	závěs
ke	k	k7c3	k
koši	koš	k1gInSc3	koš
a	a	k8xC	a
středícími	středící	k2eAgInPc7d1	středící
prvky	prvek	k1gInPc7	prvek
k	k	k7c3	k
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
části	část	k1gFnSc3	část
koše	koš	k1gInSc2	koš
nebo	nebo	k8xC	nebo
k	k	k7c3	k
magnetu	magneto	k1gNnSc3	magneto
<g/>
.	.	kIx.	.
</s>
<s>
Závěs	závěs	k1gInSc1	závěs
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
materiálu	materiál	k1gInSc2	materiál
jako	jako	k8xS	jako
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
např.	např.	kA	např.
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
několika	několik	k4yIc7	několik
vlnkami	vlnka	k1gFnPc7	vlnka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
deformují	deformovat	k5eAaImIp3nP	deformovat
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pístový	pístový	k2eAgInSc4d1	pístový
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Papírový	papírový	k2eAgInSc1d1	papírový
závěs	závěs	k1gInSc1	závěs
obvykle	obvykle	k6eAd1	obvykle
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
velký	velký	k2eAgInSc1d1	velký
pohyb	pohyb	k1gInSc1	pohyb
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
středotónových	středotónový	k2eAgInPc2d1	středotónový
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
basových	basový	k2eAgInPc2d1	basový
reproduktorů	reproduktor	k1gInPc2	reproduktor
bývá	bývat	k5eAaImIp3nS	bývat
závěs	závěs	k1gInSc1	závěs
často	často	k6eAd1	často
z	z	k7c2	z
impregnovaného	impregnovaný	k2eAgInSc2d1	impregnovaný
textilu	textil	k1gInSc2	textil
<g/>
,	,	kIx,	,
gumy	guma	k1gFnSc2	guma
<g/>
,	,	kIx,	,
pěnové	pěnový	k2eAgFnSc2d1	pěnová
pryže	pryž	k1gFnSc2	pryž
<g/>
,	,	kIx,	,
molitanu	molitan	k1gInSc2	molitan
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
poddajných	poddajný	k2eAgInPc2d1	poddajný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
kmitací	kmitací	k2eAgFnSc1d1	kmitací
cívka	cívka	k1gFnSc1	cívka
pohybovat	pohybovat	k5eAaImF	pohybovat
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
mezeře	mezera	k1gFnSc6	mezera
magnetického	magnetický	k2eAgInSc2d1	magnetický
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zejména	zejména	k9	zejména
u	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
membrána	membrána	k1gFnSc1	membrána
zavěšena	zavěsit	k5eAaPmNgFnS	zavěsit
i	i	k9	i
kolem	kolem	k7c2	kolem
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
středící	středící	k2eAgInSc1d1	středící
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
montovaný	montovaný	k2eAgInSc1d1	montovaný
ke	k	k7c3	k
koši	koš	k1gInSc3	koš
nebo	nebo	k8xC	nebo
magnetickému	magnetický	k2eAgInSc3d1	magnetický
obvodu	obvod	k1gInSc3	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
díl	díl	k1gInSc1	díl
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
impregnovaného	impregnovaný	k2eAgInSc2d1	impregnovaný
textilu	textil	k1gInSc2	textil
<g/>
,	,	kIx,	,
mívá	mívat	k5eAaImIp3nS	mívat
soustředně	soustředně	k6eAd1	soustředně
vylisované	vylisovaný	k2eAgFnPc4d1	vylisovaná
vlnky	vlnka	k1gFnPc4	vlnka
(	(	kIx(	(
<g/>
vlnovec	vlnovec	k1gInSc1	vlnovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brání	bránit	k5eAaImIp3nS	bránit
zároveň	zároveň	k6eAd1	zároveň
vnikání	vnikání	k1gNnSc1	vnikání
nečistot	nečistota	k1gFnPc2	nečistota
do	do	k7c2	do
mezery	mezera	k1gFnSc2	mezera
mag	mag	k?	mag
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
poddajností	poddajnost	k1gFnSc7	poddajnost
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rozměrově	rozměrově	k6eAd1	rozměrově
malých	malý	k2eAgInPc2d1	malý
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
sluchátek	sluchátko	k1gNnPc2	sluchátko
a	a	k8xC	a
měničů	měnič	k1gInPc2	měnič
s	s	k7c7	s
kalotovou	kalotový	k2eAgFnSc7d1	kalotový
membránou	membrána	k1gFnSc7	membrána
tento	tento	k3xDgInSc4	tento
prvek	prvek	k1gInSc1	prvek
nebývá	bývat	k5eNaImIp3nS	bývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývody	vývod	k1gInPc4	vývod
a	a	k8xC	a
připojovací	připojovací	k2eAgFnPc4d1	připojovací
svorky	svorka	k1gFnPc4	svorka
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
zdroji	zdroj	k1gInSc3	zdroj
signálu	signál	k1gInSc2	signál
jsou	být	k5eAaImIp3nP	být
reproduktory	reproduktor	k1gInPc1	reproduktor
vybaveny	vybavit	k5eAaPmNgInP	vybavit
různými	různý	k2eAgFnPc7d1	různá
svorkami	svorka	k1gFnPc7	svorka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
levných	levný	k2eAgInPc2d1	levný
a	a	k8xC	a
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
reproduktorů	reproduktor	k1gInPc2	reproduktor
to	ten	k3xDgNnSc1	ten
bývají	bývat	k5eAaImIp3nP	bývat
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
pájecí	pájecí	k2eAgNnPc1d1	pájecí
oka	oko	k1gNnPc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
fastony	faston	k1gInPc1	faston
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
různého	různý	k2eAgInSc2d1	různý
rozměru	rozměr	k1gInSc2	rozměr
pro	pro	k7c4	pro
nezáměnnost	nezáměnnost	k1gFnSc4	nezáměnnost
připojení	připojení	k1gNnSc2	připojení
–	–	k?	–
polarita	polarita	k1gFnSc1	polarita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
dražších	drahý	k2eAgInPc2d2	dražší
i	i	k8xC	i
tlačné	tlačný	k2eAgFnPc4d1	tlačná
nebo	nebo	k8xC	nebo
šroubovací	šroubovací	k2eAgFnPc4d1	šroubovací
svorky	svorka	k1gFnPc4	svorka
<g/>
.	.	kIx.	.
</s>
<s>
Kladná	kladný	k2eAgFnSc1d1	kladná
svorka	svorka	k1gFnSc1	svorka
bývá	bývat	k5eAaImIp3nS	bývat
označena	označit	k5eAaPmNgFnS	označit
obvykle	obvykle	k6eAd1	obvykle
červenou	červený	k2eAgFnSc4d1	červená
barvou	barva	k1gFnSc7	barva
nebo	nebo	k8xC	nebo
znaménkem	znaménko	k1gNnSc7	znaménko
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Smluvně	smluvně	k6eAd1	smluvně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
přivedení	přivedení	k1gNnSc3	přivedení
napětí	napětí	k1gNnSc2	napětí
souhlasné	souhlasný	k2eAgFnSc2d1	souhlasná
polarity	polarita	k1gFnSc2	polarita
se	se	k3xPyFc4	se
membrána	membrána	k1gFnSc1	membrána
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývody	vývod	k1gInPc1	vývod
od	od	k7c2	od
kmitací	kmitací	k2eAgFnSc2d1	kmitací
cívky	cívka	k1gFnSc2	cívka
ke	k	k7c3	k
svorkám	svorka	k1gFnPc3	svorka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
vodičem	vodič	k1gInSc7	vodič
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
bývá	bývat	k5eAaImIp3nS	bývat
vinuta	vinut	k2eAgFnSc1d1	vinuta
vlastní	vlastní	k2eAgFnSc1d1	vlastní
cívka	cívka	k1gFnSc1	cívka
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
vysokotónových	vysokotónový	k2eAgInPc2d1	vysokotónový
měničů	měnič	k1gInPc2	měnič
a	a	k8xC	a
levných	levný	k2eAgInPc2d1	levný
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
však	však	k9	však
vývod	vývod	k1gInSc4	vývod
z	z	k7c2	z
mechanických	mechanický	k2eAgInPc2d1	mechanický
důvodů	důvod	k1gInPc2	důvod
zesílen	zesílit	k5eAaPmNgInS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dražších	drahý	k2eAgInPc2d2	dražší
vysokotónových	vysokotónový	k2eAgInPc2d1	vysokotónový
měničů	měnič	k1gInPc2	měnič
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
páskové	páskový	k2eAgInPc1d1	páskový
vývody	vývod	k1gInPc1	vývod
<g/>
,	,	kIx,	,
napojené	napojený	k2eAgInPc1d1	napojený
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
kmitací	kmitací	k2eAgFnSc2d1	kmitací
cívky	cívka	k1gFnSc2	cívka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
vlastním	vlastní	k2eAgInSc6d1	vlastní
formeru	former	k1gInSc6	former
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
laněné	laněný	k2eAgInPc1d1	laněný
vývody	vývod	k1gInPc1	vývod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
výchylkou	výchylka	k1gFnSc7	výchylka
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
měkká	měkký	k2eAgFnSc1d1	měkká
ohebná	ohebný	k2eAgFnSc1d1	ohebná
lanka	lanko	k1gNnPc4	lanko
mezi	mezi	k7c7	mezi
připojovacími	připojovací	k2eAgFnPc7d1	připojovací
svorkami	svorka	k1gFnPc7	svorka
a	a	k8xC	a
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
letmo	letmo	k6eAd1	letmo
ukotvena	ukotven	k2eAgFnSc1d1	ukotvena
ke	k	k7c3	k
středicímu	středicí	k2eAgInSc3d1	středicí
vlnovci	vlnovec	k1gInSc3	vlnovec
<g/>
.	.	kIx.	.
</s>
<s>
Lanka	lanko	k1gNnPc1	lanko
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
membráně	membrána	k1gFnSc3	membrána
umožnila	umožnit	k5eAaPmAgFnS	umožnit
plnou	plný	k2eAgFnSc4d1	plná
výchylku	výchylka	k1gFnSc4	výchylka
a	a	k8xC	a
současně	současně	k6eAd1	současně
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
namáhání	namáhání	k1gNnSc3	namáhání
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pazvukům	pazvuk	k1gInPc3	pazvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kvalita	kvalita	k1gFnSc1	kvalita
reproduktorů	reproduktor	k1gInPc2	reproduktor
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
použití	použití	k1gNnSc2	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
reproduktoru	reproduktor	k1gInSc2	reproduktor
určují	určovat	k5eAaImIp3nP	určovat
následující	následující	k2eAgFnPc4d1	následující
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jmenovitá	jmenovitý	k2eAgFnSc1d1	jmenovitá
impedance	impedance	k1gFnSc1	impedance
–	–	k?	–
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
cca	cca	kA	cca
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
odpor	odpor	k1gInSc4	odpor
kmitací	kmitací	k2eAgFnSc2d1	kmitací
cívky	cívka	k1gFnSc2	cívka
<g/>
,	,	kIx,	,
běžná	běžný	k2eAgFnSc1d1	běžná
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
jednotek	jednotka	k1gFnPc2	jednotka
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
ohmů	ohm	k1gInPc2	ohm
<g/>
.	.	kIx.	.
</s>
<s>
Impedance	impedance	k1gFnSc1	impedance
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
,	,	kIx,	,
jmenovitá	jmenovitý	k2eAgFnSc1d1	jmenovitá
impedance	impedance	k1gFnSc1	impedance
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
impedance	impedance	k1gFnSc1	impedance
v	v	k7c6	v
pracovním	pracovní	k2eAgNnSc6d1	pracovní
pásmu	pásmo	k1gNnSc6	pásmo
(	(	kIx(	(
<g/>
t.j.	t.j.	k?	t.j.
nad	nad	k7c7	nad
rezonančním	rezonanční	k2eAgInSc7d1	rezonanční
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1	běžná
hodnoty	hodnota	k1gFnPc1	hodnota
jmenovité	jmenovitý	k2eAgFnSc2d1	jmenovitá
impedance	impedance	k1gFnSc2	impedance
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
4,6	[number]	k4	4,6
<g/>
,	,	kIx,	,
8	[number]	k4	8
Ohmů	ohm	k1gInPc2	ohm
<g/>
,	,	kIx,	,
reproduktory	reproduktor	k1gInPc1	reproduktor
do	do	k7c2	do
kytarových	kytarový	k2eAgInPc2d1	kytarový
reproboxů	reprobox	k1gInPc2	reprobox
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
výkonné	výkonný	k2eAgInPc1d1	výkonný
výškové	výškový	k2eAgInPc1d1	výškový
systémy	systém	k1gInPc1	systém
mívají	mívat	k5eAaImIp3nP	mívat
často	často	k6eAd1	často
16	[number]	k4	16
Ohmů	ohm	k1gInPc2	ohm
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
reproduktory	reproduktor	k1gInPc1	reproduktor
pro	pro	k7c4	pro
ozvučování	ozvučování	k1gNnSc4	ozvučování
automobilů	automobil	k1gInPc2	automobil
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
jmenovitou	jmenovitý	k2eAgFnSc7d1	jmenovitá
impedancí	impedance	k1gFnSc7	impedance
<g/>
,	,	kIx,	,
3.2	[number]	k4	3.2
či	či	k8xC	či
2	[number]	k4	2
Ohmy	ohm	k1gInPc7	ohm
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
rozsah	rozsah	k1gInSc1	rozsah
–	–	k?	–
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
frekvenční	frekvenční	k2eAgFnSc7d1	frekvenční
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
definuje	definovat	k5eAaBmIp3nS	definovat
závislost	závislost	k1gFnSc4	závislost
(	(	kIx(	(
<g/>
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uváděn	uvádět	k5eAaImNgInS	uvádět
ve	v	k7c6	v
zjednodušené	zjednodušený	k2eAgFnSc6d1	zjednodušená
formě	forma	k1gFnSc6	forma
číselně	číselně	k6eAd1	číselně
(	(	kIx(	(
<g/>
od-do	odo	k1gNnSc1	od-do
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
definovaný	definovaný	k2eAgInSc4d1	definovaný
pokles	pokles	k1gInSc4	pokles
citlivosti	citlivost	k1gFnSc2	citlivost
při	při	k7c6	při
krajních	krajní	k2eAgFnPc6d1	krajní
frekvencích	frekvence	k1gFnPc6	frekvence
(	(	kIx(	(
<g/>
např.	např.	kA	např.
-3	-3	k4	-3
nebo	nebo	k8xC	nebo
-10	-10	k4	-10
<g/>
dB	db	kA	db
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Příkon	příkon	k1gInSc1	příkon
–	–	k?	–
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
velký	velký	k2eAgInSc4d1	velký
elektrický	elektrický	k2eAgInSc4d1	elektrický
příkon	příkon	k1gInSc4	příkon
je	být	k5eAaImIp3nS	být
reproduktor	reproduktor	k1gMnSc1	reproduktor
schopen	schopen	k2eAgMnSc1d1	schopen
zpracovat	zpracovat	k5eAaPmF	zpracovat
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
přeměně	přeměna	k1gFnSc6	přeměna
v	v	k7c4	v
akustický	akustický	k2eAgInSc4d1	akustický
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
až	až	k8xS	až
stovkách	stovka	k1gFnPc6	stovka
wattů	watt	k1gInPc2	watt
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgMnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
basových	basový	k2eAgInPc2d1	basový
a	a	k8xC	a
středobasových	středobasův	k2eAgInPc2d1	středobasův
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgMnSc1d2	nižší
pak	pak	k6eAd1	pak
u	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
středotónových	středotónový	k2eAgInPc2d1	středotónový
a	a	k8xC	a
vysokotónových	vysokotónový	k2eAgInPc2d1	vysokotónový
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgFnSc3d1	nízká
účinnosti	účinnost	k1gFnSc3	účinnost
zařízení	zařízení	k1gNnSc2	zařízení
je	být	k5eAaImIp3nS	být
vyzářený	vyzářený	k2eAgInSc4d1	vyzářený
akustický	akustický	k2eAgInSc4d1	akustický
výkon	výkon	k1gInSc4	výkon
obvykle	obvykle	k6eAd1	obvykle
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
i	i	k9	i
při	při	k7c6	při
vysokém	vysoký	k2eAgInSc6d1	vysoký
příkonu	příkon	k1gInSc6	příkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
citlivost	citlivost	k1gFnSc1	citlivost
–	–	k?	–
určuje	určovat	k5eAaImIp3nS	určovat
vyzářený	vyzářený	k2eAgInSc4d1	vyzářený
akustický	akustický	k2eAgInSc4d1	akustický
výkon	výkon	k1gInSc4	výkon
(	(	kIx(	(
<g/>
hladinu	hladina	k1gFnSc4	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
při	při	k7c6	při
daném	daný	k2eAgInSc6d1	daný
příkonu	příkon	k1gInSc6	příkon
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
při	při	k7c6	při
příkonu	příkon	k1gInSc6	příkon
1	[number]	k4	1
Wattu	watt	k1gInSc2	watt
měřené	měřený	k2eAgFnPc1d1	měřená
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
metru	metr	k1gInSc2	metr
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgFnSc3d1	charakteristická
citlivosti	citlivost	k1gFnSc3	citlivost
102	[number]	k4	102
dB	db	kA	db
na	na	k7c4	na
1	[number]	k4	1
W	W	kA	W
(	(	kIx(	(
<g/>
1	[number]	k4	1
m	m	kA	m
<g/>
)	)	kIx)	)
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
účinnost	účinnost	k1gFnSc4	účinnost
cca	cca	kA	cca
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Směrová	směrový	k2eAgFnSc1d1	směrová
charakteristika	charakteristika	k1gFnSc1	charakteristika
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
vyzařovací	vyzařovací	k2eAgInSc1d1	vyzařovací
diagram	diagram	k1gInSc1	diagram
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
závislost	závislost	k1gFnSc4	závislost
akustického	akustický	k2eAgInSc2d1	akustický
výkonu	výkon	k1gInSc2	výkon
na	na	k7c6	na
směru	směr	k1gInSc6	směr
vyzařování	vyzařování	k1gNnSc2	vyzařování
<g/>
.	.	kIx.	.
</s>
<s>
Směrová	směrový	k2eAgFnSc1d1	směrová
charakteristika	charakteristika	k1gFnSc1	charakteristika
je	být	k5eAaImIp3nS	být
proměřována	proměřovat	k5eAaImNgFnS	proměřovat
pro	pro	k7c4	pro
horizontální	horizontální	k2eAgFnSc4d1	horizontální
i	i	k8xC	i
vertikální	vertikální	k2eAgFnSc4d1	vertikální
poslechovou	poslechový	k2eAgFnSc4d1	poslechová
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
frekvence	frekvence	k1gFnSc1	frekvence
–	–	k?	–
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
frekvencí	frekvence	k1gFnSc7	frekvence
obvykle	obvykle	k6eAd1	obvykle
vyzářený	vyzářený	k2eAgInSc1d1	vyzářený
výkon	výkon	k1gInSc1	výkon
reproduktoru	reproduktor	k1gInSc2	reproduktor
strmě	strmě	k6eAd1	strmě
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
reproduktorů	reproduktor	k1gInPc2	reproduktor
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
tlumením	tlumení	k1gNnSc7	tlumení
se	se	k3xPyFc4	se
při	při	k7c6	při
rezonanci	rezonance	k1gFnSc6	rezonance
značně	značně	k6eAd1	značně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
výchylka	výchylka	k1gFnSc1	výchylka
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
většinou	většinou	k6eAd1	většinou
znamená	znamenat	k5eAaImIp3nS	znamenat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
zkreslení	zkreslení	k1gNnSc4	zkreslení
a	a	k8xC	a
sníženou	snížený	k2eAgFnSc4d1	snížená
zatížitelnost	zatížitelnost	k1gFnSc4	zatížitelnost
kolem	kolem	k7c2	kolem
rezonanční	rezonanční	k2eAgFnSc2d1	rezonanční
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
basových	basový	k2eAgInPc2d1	basový
reproduktorů	reproduktor	k1gInPc2	reproduktor
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
frekvence	frekvence	k1gFnSc1	frekvence
pohybovat	pohybovat	k5eAaImF	pohybovat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zabudování	zabudování	k1gNnSc6	zabudování
basového	basový	k2eAgInSc2d1	basový
reproduktoru	reproduktor	k1gInSc2	reproduktor
do	do	k7c2	do
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
ozvučnice	ozvučnice	k1gFnSc2	ozvučnice
se	se	k3xPyFc4	se
rezonanční	rezonanční	k2eAgFnSc4d1	rezonanční
frekvenci	frekvence	k1gFnSc4	frekvence
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
středotónových	středotónový	k2eAgInPc2d1	středotónový
a	a	k8xC	a
vysokotónových	vysokotónový	k2eAgInPc2d1	vysokotónový
měničů	měnič	k1gInPc2	měnič
bývá	bývat	k5eAaImIp3nS	bývat
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
frekvence	frekvence	k1gFnSc1	frekvence
obvykle	obvykle	k6eAd1	obvykle
mimo	mimo	k7c4	mimo
pásmo	pásmo	k1gNnSc4	pásmo
pracovních	pracovní	k2eAgFnPc2d1	pracovní
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
zkušenosti	zkušenost	k1gFnPc1	zkušenost
elektroakustických	elektroakustický	k2eAgMnPc2d1	elektroakustický
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
,	,	kIx,	,
pečlivost	pečlivost	k1gFnSc4	pečlivost
a	a	k8xC	a
zručnost	zručnost	k1gFnSc4	zručnost
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnPc4	tradice
výroby	výroba	k1gFnSc2	výroba
</s>
</p>
<p>
<s>
===	===	k?	===
Thiele-Smallovy	Thiele-Smallův	k2eAgInPc1d1	Thiele-Smallův
parametry	parametr	k1gInPc1	parametr
reproduktoru	reproduktor	k1gInSc2	reproduktor
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
a	a	k8xC	a
simulaci	simulace	k1gFnSc4	simulace
chování	chování	k1gNnSc2	chování
elektrodynamických	elektrodynamický	k2eAgInPc2d1	elektrodynamický
reproduktorů	reproduktor	k1gInPc2	reproduktor
na	na	k7c6	na
nízkých	nízký	k2eAgFnPc6d1	nízká
frekvencích	frekvence	k1gFnPc6	frekvence
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tzv.	tzv.	kA	tzv.
Thiele-Smallovy	Thiele-Smallův	k2eAgInPc4d1	Thiele-Smallův
parametry	parametr	k1gInPc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
omezení	omezení	k1gNnSc1	omezení
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
membrána	membrána	k1gFnSc1	membrána
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
ideální	ideální	k2eAgInSc1d1	ideální
píst	píst	k1gInSc1	píst
a	a	k8xC	a
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
deformaci	deformace	k1gFnSc3	deformace
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
zjednodušujících	zjednodušující	k2eAgInPc6d1	zjednodušující
předpokladech	předpoklad	k1gInPc6	předpoklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sd	Sd	k?	Sd
–	–	k?	–
efektivní	efektivní	k2eAgFnSc1d1	efektivní
plocha	plocha	k1gFnSc1	plocha
membrány	membrána	k1gFnSc2	membrána
</s>
</p>
<p>
<s>
Mms	Mms	k?	Mms
–	–	k?	–
hmotnost	hmotnost	k1gFnSc1	hmotnost
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
spolukmitajícího	spolukmitající	k2eAgInSc2d1	spolukmitající
sloupce	sloupec	k1gInSc2	sloupec
vzduchu	vzduch	k1gInSc2	vzduch
</s>
</p>
<p>
<s>
Cms	Cms	k?	Cms
–	–	k?	–
poddajnost	poddajnost	k1gFnSc1	poddajnost
zavěšení	zavěšení	k1gNnPc2	zavěšení
kmitacího	kmitací	k2eAgInSc2d1	kmitací
systému	systém	k1gInSc2	systém
</s>
</p>
<p>
<s>
Rms	Rms	k?	Rms
–	–	k?	–
mechanický	mechanický	k2eAgInSc1d1	mechanický
odpor	odpor	k1gInSc1	odpor
kmitacího	kmitací	k2eAgInSc2d1	kmitací
systému	systém	k1gInSc2	systém
N	N	kA	N
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
</s>
</p>
<p>
<s>
Le	Le	k?	Le
–	–	k?	–
indukčnost	indukčnost	k1gFnSc4	indukčnost
kmitací	kmitací	k2eAgFnSc2d1	kmitací
cívky	cívka	k1gFnSc2	cívka
</s>
</p>
<p>
<s>
Re	re	k9	re
–	–	k?	–
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
odpor	odpor	k1gInSc4	odpor
kmitací	kmitací	k2eAgFnSc2d1	kmitací
cívky	cívka	k1gFnSc2	cívka
</s>
</p>
<p>
<s>
Bl	Bl	k?	Bl
–	–	k?	–
součin	součin	k1gInSc4	součin
indukce	indukce	k1gFnSc2	indukce
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
mezeře	mezera	k1gFnSc6	mezera
magnetického	magnetický	k2eAgInSc2d1	magnetický
obvodu	obvod	k1gInSc2	obvod
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
vodiče	vodič	k1gInSc2	vodič
v	v	k7c6	v
mag	mag	k?	mag
<g/>
.	.	kIx.	.
poli	pole	k1gNnSc6	pole
</s>
</p>
<p>
<s>
Fs	Fs	k?	Fs
–	–	k?	–
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
frekvence	frekvence	k1gFnSc1	frekvence
reproduktoru	reproduktor	k1gInSc2	reproduktor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
C_	C_	k1gMnSc1	C_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
M_	M_	k1gMnSc1	M_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}}}}}}	}}}}}}}	k?	}}}}}}}
</s>
</p>
<p>
</p>
<p>
<s>
Qes	Qes	k?	Qes
–	–	k?	–
elektrický	elektrický	k2eAgInSc4d1	elektrický
činitel	činitel	k1gInSc4	činitel
jakosti	jakost	k1gFnSc2	jakost
(	(	kIx(	(
<g/>
ideálně	ideálně	k6eAd1	ideálně
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
okolo	okolo	k7c2	okolo
0,45	[number]	k4	0,45
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q_	Q_	k1gMnPc6	Q_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
es	es	k1gNnPc2	es
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
M_	M_	k1gMnSc1	M_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
Bl	Bl	k1gFnSc1	Bl
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
<s>
Qms	Qms	k?	Qms
–	–	k?	–
mechanický	mechanický	k2eAgMnSc1d1	mechanický
činitel	činitel	k1gMnSc1	činitel
jakosti	jakost	k1gFnSc2	jakost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q_	Q_	k1gMnPc6	Q_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
M_	M_	k1gMnSc1	M_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}}}}	}}}}}	k?	}}}}}
</s>
</p>
<p>
</p>
<p>
<s>
Qts	Qts	k?	Qts
–	–	k?	–
celkový	celkový	k2eAgInSc4d1	celkový
činitel	činitel	k1gInSc4	činitel
jakosti	jakost	k1gFnSc2	jakost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q_	Q_	k1gMnPc6	Q_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ts	ts	k0	ts
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
Q_	Q_	k1gFnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
es	es	k1gNnPc2	es
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
Q_	Q_	k1gFnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
Q_	Q_	k1gFnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
es	es	k1gNnPc2	es
<g/>
}}}}}	}}}}}	k?	}}}}}
</s>
</p>
<p>
</p>
<p>
<s>
Vas	Vas	k?	Vas
–	–	k?	–
ekvivalentní	ekvivalentní	k2eAgInSc4d1	ekvivalentní
objem	objem	k1gInSc4	objem
(	(	kIx(	(
<g/>
t.j.	t.j.	k?	t.j.
objem	objem	k1gInSc4	objem
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
ozvučnice	ozvučnice	k1gFnSc2	ozvučnice
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
frekvence	frekvence	k1gFnSc1	frekvence
reproduktoru	reproduktor	k1gInSc2	reproduktor
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
na	na	k7c4	na
druhá	druhý	k4xOgFnSc1	druhý
odmocnina	odmocnina	k1gFnSc1	odmocnina
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
–	–	k?	–
násobek	násobek	k1gInSc4	násobek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V_	V_	k1gMnPc6	V_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
as	as	k1gInSc1	as
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
C_	C_	k1gMnSc1	C_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
ρ	ρ	k?	ρ
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
1.184	[number]	k4	1.184
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
při	při	k7c6	při
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
c	c	k0	c
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
(	(	kIx(	(
<g/>
346.1	[number]	k4	346.1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
při	při	k7c6	při
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Xmax	Xmax	k1gInSc1	Xmax
–	–	k?	–
maximální	maximální	k2eAgFnSc1d1	maximální
lineární	lineární	k2eAgFnSc1d1	lineární
výchylka	výchylka	k1gFnSc1	výchylka
kmitacího	kmitací	k2eAgInSc2d1	kmitací
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
ale	ale	k9	ale
jako	jako	k8xS	jako
špička-špička	špička-špička	k1gFnSc1	špička-špička
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Xmech	Xmech	k1gInSc1	Xmech
–	–	k?	–
maximální	maximální	k2eAgFnSc1d1	maximální
výchylka	výchylka	k1gFnSc1	výchylka
kmitacího	kmitací	k2eAgInSc2d1	kmitací
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
překročení	překročení	k1gNnSc6	překročení
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pe	Pe	k?	Pe
</s>
</p>
<p>
<s>
VdVd	VdVd	k1gMnSc1	VdVd
=	=	kIx~	=
Sd	Sd	k1gMnSc1	Sd
<g/>
·	·	k?	·
<g/>
Xmax	Xmax	k1gInSc1	Xmax
</s>
</p>
<p>
<s>
EBP	EBP	kA	EBP
–	–	k?	–
Efficiency	Efficienca	k1gFnSc2	Efficienca
Bandwidth	Bandwidth	k1gMnSc1	Bandwidth
Product	Product	k2eAgMnSc1d1	Product
–	–	k?	–
odvozený	odvozený	k2eAgInSc1d1	odvozený
parametr	parametr	k1gInSc1	parametr
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
vhodnost	vhodnost	k1gFnSc1	vhodnost
použití	použití	k1gNnSc2	použití
reproduktoru	reproduktor	k1gInSc2	reproduktor
do	do	k7c2	do
otevřené	otevřený	k2eAgFnSc2d1	otevřená
ozvučnice	ozvučnice	k1gFnSc2	ozvučnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
EBP	EBP	kA	EBP
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
Q_	Q_	k1gFnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
es	es	k1gNnPc2	es
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
<s>
Znom	Znom	k6eAd1	Znom
–	–	k?	–
jmenovitá	jmenovitý	k2eAgFnSc1d1	jmenovitá
impedance	impedance	k1gFnSc1	impedance
reproduktoru	reproduktor	k1gInSc2	reproduktor
</s>
</p>
<p>
<s>
η	η	k?	η
<g/>
0	[number]	k4	0
–	–	k?	–
jmenovitá	jmenovitý	k2eAgFnSc1d1	jmenovitá
energetická	energetický	k2eAgFnSc1d1	energetická
účinnost	účinnost	k1gFnSc1	účinnost
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
druhy	druh	k1gInPc1	druh
reproduktorů	reproduktor	k1gInPc2	reproduktor
==	==	k?	==
</s>
</p>
<p>
<s>
Zemní	zemní	k2eAgInPc1d1	zemní
reproduktory	reproduktor	k1gInPc1	reproduktor
</s>
</p>
<p>
<s>
Deskové	deskový	k2eAgInPc4d1	deskový
reproduktory	reproduktor	k1gInPc4	reproduktor
(	(	kIx(	(
<g/>
hrající	hrající	k2eAgInPc4d1	hrající
obrazy	obraz	k1gInPc4	obraz
a	a	k8xC	a
zdi	zeď	k1gFnPc4	zeď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Štefan	Štefan	k1gMnSc1	Štefan
:	:	kIx,	:
Reproduktory	reproduktor	k1gInPc1	reproduktor
a	a	k8xC	a
reproduktorové	reproduktorový	k2eAgFnSc2d1	reproduktorová
soustavy	soustava	k1gFnSc2	soustava
</s>
</p>
<p>
<s>
C.	C.	kA	C.
Smetana	smetana	k1gFnSc1	smetana
:	:	kIx,	:
Ozvučování	ozvučování	k1gNnSc1	ozvučování
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Toman	Toman	k1gMnSc1	Toman
:	:	kIx,	:
Reproduktory	reproduktor	k1gInPc1	reproduktor
a	a	k8xC	a
reprosoustavy	reprosoustava	k1gFnPc1	reprosoustava
</s>
</p>
<p>
<s>
B.	B.	kA	B.
Sýkora	Sýkora	k1gMnSc1	Sýkora
:	:	kIx,	:
Reproduktory	reproduktor	k1gInPc1	reproduktor
a	a	k8xC	a
reproduktorové	reproduktorový	k2eAgFnPc1d1	reproduktorová
soustavy	soustava	k1gFnPc1	soustava
trochu	trochu	k6eAd1	trochu
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
ARB	arba	k1gFnPc2	arba
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
Borwick	Borwick	k1gMnSc1	Borwick
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
:	:	kIx,	:
Loudspeaker	Loudspeaker	k1gMnSc1	Loudspeaker
and	and	k?	and
Headphone	Headphon	k1gInSc5	Headphon
Handbook	handbook	k1gInSc4	handbook
by	by	kYmCp3nS	by
John	John	k1gMnSc1	John
Borwick	Borwick	k1gMnSc1	Borwick
<g/>
,	,	kIx,	,
Focal	Focal	k1gMnSc1	Focal
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
:	:	kIx,	:
Věrný	věrný	k2eAgInSc1d1	věrný
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Reproduktorová	reproduktorový	k2eAgFnSc1d1	reproduktorová
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
reproduktor	reproduktor	k1gInSc1	reproduktor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
reproduktor	reproduktor	k1gInSc1	reproduktor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Reproduktory	reproduktor	k1gInPc1	reproduktor
–	–	k?	–
parametry	parametr	k1gInPc1	parametr
<g/>
,	,	kIx,	,
měření	měření	k1gNnPc1	měření
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnPc1	databáze
technických	technický	k2eAgInPc2d1	technický
údajů	údaj	k1gInPc2	údaj
reproduktorů	reproduktor	k1gInPc2	reproduktor
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Reproduktory	reproduktor	k1gInPc1	reproduktor
s	s	k7c7	s
ohybovou	ohybový	k2eAgFnSc7d1	ohybová
vlnou	vlna	k1gFnSc7	vlna
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elektrostatický	elektrostatický	k2eAgInSc1d1	elektrostatický
reproduktor	reproduktor	k1gInSc1	reproduktor
–	–	k?	–
postup	postup	k1gInSc4	postup
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hiend	Hiend	k1gInSc1	Hiend
sestava	sestava	k1gFnSc1	sestava
s	s	k7c7	s
plazmovým	plazmový	k2eAgInSc7d1	plazmový
reproduktorem	reproduktor	k1gInSc7	reproduktor
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pneumatický	pneumatický	k2eAgInSc1d1	pneumatický
reproduktor	reproduktor	k1gInSc1	reproduktor
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
