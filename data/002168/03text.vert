<s>
Columbia	Columbia	k1gFnSc1	Columbia
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Columbia	Columbia	k1gFnSc1	Columbia
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Richland	Richlando	k1gNnPc2	Richlando
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
Lexington	Lexington	k1gInSc4	Lexington
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
129	[number]	k4	129
272	[number]	k4	272
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
51,7	[number]	k4	51,7
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
42,2	[number]	k4	42,2
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,0	[number]	k4	2,0
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
4,3	[number]	k4	4,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Mark	Mark	k1gMnSc1	Mark
Baldwin	Baldwin	k1gMnSc1	Baldwin
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
-	-	kIx~	-
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Stanley	Stanlea	k1gFnSc2	Stanlea
Donen	Donen	k2eAgMnSc1d1	Donen
(	(	kIx(	(
<g/>
*	*	kIx~	*
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
choreograf	choreograf	k1gMnSc1	choreograf
Kristin	Kristina	k1gFnPc2	Kristina
Davisová	Davisový	k2eAgFnSc1d1	Davisová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
producentka	producentka	k1gFnSc1	producentka
Čeljabinsk	Čeljabinsk	k1gInSc1	Čeljabinsk
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Kaiserslautern	Kaiserslauterna	k1gFnPc2	Kaiserslauterna
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Kluž	Kluž	k1gFnSc1	Kluž
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
Plovdiv	Plovdiv	k1gInSc1	Plovdiv
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Carolina	Carolina	k1gFnSc1	Carolina
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Columbia	Columbia	k1gFnSc1	Columbia
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
