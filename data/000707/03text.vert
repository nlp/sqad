<s>
Kolja	Kolja	k6eAd1	Kolja
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jana	Jan	k1gMnSc2	Jan
Svěráka	Svěrák	k1gMnSc2	Svěrák
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ho	on	k3xPp3gMnSc4	on
vidělo	vidět	k5eAaImAgNnS	vidět
1	[number]	k4	1
346	[number]	k4	346
669	[number]	k4	669
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejnavštěvovanějším	navštěvovaný	k2eAgInSc7d3	nejnavštěvovanější
českým	český	k2eAgInSc7d1	český
filmem	film	k1gInSc7	film
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
šest	šest	k4xCc4	šest
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
toho	ten	k3xDgNnSc2	ten
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Oscar	Oscara	k1gFnPc2	Oscara
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
neanglicky	anglicky	k6eNd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
ve	v	k7c6	v
40	[number]	k4	40
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
viděly	vidět	k5eAaImAgInP	vidět
asi	asi	k9	asi
tři	tři	k4xCgNnPc4	tři
miliony	milion	k4xCgInPc4	milion
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
filmu	film	k1gInSc2	film
-	-	kIx~	-
starý	starý	k2eAgMnSc1d1	starý
mládenec	mládenec	k1gMnSc1	mládenec
Louka	louka	k1gFnSc1	louka
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
peníze	peníz	k1gInPc4	peníz
fingovaně	fingovaně	k6eAd1	fingovaně
ožení	oženit	k5eAaPmIp3nP	oženit
s	s	k7c7	s
Ruskou	Ruska	k1gFnSc7	Ruska
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
zamotá	zamotat	k5eAaPmIp3nS	zamotat
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
krku	krk	k1gInSc6	krk
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
emigraci	emigrace	k1gFnSc6	emigrace
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
zůstane	zůstat	k5eAaPmIp3nS	zůstat
její	její	k3xOp3gMnSc1	její
malý	malý	k2eAgMnSc1d1	malý
syn	syn	k1gMnSc1	syn
Kolja	Kolja	k1gMnSc1	Kolja
(	(	kIx(	(
<g/>
Andrej	Andrej	k1gMnSc1	Andrej
Chalimon	Chalimon	k1gMnSc1	Chalimon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
neumí	umět	k5eNaImIp3nS	umět
česky	česky	k6eAd1	česky
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
panuje	panovat	k5eAaImIp3nS	panovat
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
chlapcem	chlapec	k1gMnSc7	chlapec
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
