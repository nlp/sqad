<p>
<s>
Lednička	lednička	k1gFnSc1	lednička
(	(	kIx(	(
<g/>
též	též	k9	též
lednice	lednice	k1gFnSc1	lednice
<g/>
,	,	kIx,	,
chladnička	chladnička	k1gFnSc1	chladnička
<g/>
,	,	kIx,	,
mraznička	mraznička	k1gFnSc1	mraznička
<g/>
,	,	kIx,	,
mrazák	mrazák	k1gInSc1	mrazák
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skříň	skříň	k1gFnSc4	skříň
obsahující	obsahující	k2eAgInSc4d1	obsahující
chladicí	chladicí	k2eAgInSc4d1	chladicí
stroj	stroj	k1gInSc4	stroj
a	a	k8xC	a
sloužící	sloužící	k1gFnSc4	sloužící
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
potravin	potravina	k1gFnPc2	potravina
při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
(	(	kIx(	(
<g/>
v	v	k7c6	v
chladničce	chladnička	k1gFnSc6	chladnička
asi	asi	k9	asi
4	[number]	k4	4
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
mrazicím	mrazicí	k2eAgInSc6d1	mrazicí
boxu	box	k1gInSc6	box
až	až	k9	až
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Principy	princip	k1gInPc1	princip
chlazení	chlazení	k1gNnSc1	chlazení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
lednice	lednice	k1gFnSc1	lednice
skutečně	skutečně	k6eAd1	skutečně
chlazena	chladit	k5eAaImNgFnS	chladit
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tepelně	tepelně	k6eAd1	tepelně
izolovanou	izolovaný	k2eAgFnSc4d1	izolovaná
dvouplášťovou	dvouplášťový	k2eAgFnSc4d1	dvouplášťová
skříň	skříň	k1gFnSc4	skříň
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vkládal	vkládat	k5eAaImAgInS	vkládat
přírodní	přírodní	k2eAgInSc1d1	přírodní
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
postupně	postupně	k6eAd1	postupně
tál	tát	k5eAaImAgInS	tát
a	a	k8xC	a
ochlazoval	ochlazovat	k5eAaImAgInS	ochlazovat
vnitřek	vnitřek	k1gInSc1	vnitřek
ledničky	lednička	k1gFnSc2	lednička
na	na	k7c4	na
stálou	stálý	k2eAgFnSc4d1	stálá
teplotu	teplota	k1gFnSc4	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
lednice	lednice	k1gFnSc1	lednice
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používal	používat	k5eAaImAgMnS	používat
jak	jak	k8xC	jak
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
chladicí	chladicí	k2eAgNnSc4d1	chladicí
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
sklady	sklad	k1gInPc4	sklad
přírodního	přírodní	k2eAgInSc2d1	přírodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
<g/>
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
chladnička	chladnička	k1gFnSc1	chladnička
může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
na	na	k7c6	na
principu	princip	k1gInSc6	princip
odparu	odpar	k1gInSc2	odpar
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
chlazené	chlazený	k2eAgFnSc2d1	chlazená
nádoby	nádoba	k1gFnSc2	nádoba
je	být	k5eAaImIp3nS	být
zvlhčen	zvlhčit	k5eAaPmNgInS	zvlhčit
pracovní	pracovní	k2eAgFnSc7d1	pracovní
látkou	látka	k1gFnSc7	látka
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
odpařuje	odpařovat	k5eAaImIp3nS	odpařovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
odnímá	odnímat	k5eAaImIp3nS	odnímat
nádobě	nádoba	k1gFnSc6	nádoba
skupenské	skupenský	k2eAgNnSc1d1	skupenské
teplo	teplo	k1gNnSc1	teplo
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
lze	lze	k6eAd1	lze
obvykle	obvykle	k6eAd1	obvykle
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jen	jen	k9	jen
malého	malý	k2eAgNnSc2d1	malé
snížení	snížení	k1gNnSc2	snížení
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ale	ale	k9	ale
použít	použít	k5eAaPmF	použít
i	i	k9	i
v	v	k7c6	v
primitivních	primitivní	k2eAgFnPc6d1	primitivní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
lednice	lednice	k1gFnSc1	lednice
pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
byly	být	k5eAaImAgFnP	být
dány	dát	k5eAaPmNgFnP	dát
do	do	k7c2	do
komerčního	komerční	k2eAgInSc2d1	komerční
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
odlišné	odlišný	k2eAgInPc1d1	odlišný
principy	princip	k1gInPc1	princip
chlazení	chlazení	k1gNnSc1	chlazení
ledniček	lednička	k1gFnPc2	lednička
<g/>
:	:	kIx,	:
kompresorové	kompresorový	k2eAgFnSc2d1	kompresorová
<g/>
,	,	kIx,	,
absorpční	absorpční	k2eAgFnSc2d1	absorpční
<g/>
,	,	kIx,	,
adsorpční	adsorpční	k2eAgFnSc2d1	adsorpční
a	a	k8xC	a
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
Peltierova	Peltierův	k2eAgInSc2d1	Peltierův
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Chladnička	chladnička	k1gFnSc1	chladnička
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
elektrická	elektrický	k2eAgFnSc1d1	elektrická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
typy	typ	k1gInPc1	typ
plynové	plynový	k2eAgInPc1d1	plynový
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
využívající	využívající	k2eAgInSc4d1	využívající
jiných	jiný	k2eAgMnPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kompresorová	kompresorový	k2eAgFnSc1d1	kompresorová
lednička	lednička	k1gFnSc1	lednička
===	===	k?	===
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
okruh	okruh	k1gInSc1	okruh
s	s	k7c7	s
chladivem	chladivo	k1gNnSc7	chladivo
(	(	kIx(	(
<g/>
kapalina	kapalina	k1gFnSc1	kapalina
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
stupňů	stupeň	k1gInPc2	stupeň
kolem	kolem	k7c2	kolem
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
kompresor	kompresor	k1gInSc1	kompresor
<g/>
.	.	kIx.	.
</s>
<s>
Kompresor	kompresor	k1gInSc1	kompresor
vtlačuje	vtlačovat	k5eAaImIp3nS	vtlačovat
chladivo	chladivo	k1gNnSc4	chladivo
v	v	k7c6	v
plynném	plynný	k2eAgInSc6d1	plynný
stavu	stav	k1gInSc6	stav
do	do	k7c2	do
výměníku	výměník	k1gInSc2	výměník
(	(	kIx(	(
<g/>
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
tlustostěnnou	tlustostěnný	k2eAgFnSc7d1	tlustostěnná
kovovou	kovový	k2eAgFnSc7d1	kovová
trubicí	trubice	k1gFnSc7	trubice
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
mřížka	mřížka	k1gFnSc1	mřížka
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
ledničky	lednička	k1gFnSc2	lednička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výměníku	výměník	k1gInSc6	výměník
se	se	k3xPyFc4	se
plyn	plyn	k1gInSc1	plyn
ochladí	ochladit	k5eAaPmIp3nS	ochladit
a	a	k8xC	a
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
kapalinu	kapalina	k1gFnSc4	kapalina
(	(	kIx(	(
<g/>
kondenzace	kondenzace	k1gFnSc1	kondenzace
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Přebytečné	přebytečný	k2eAgNnSc1d1	přebytečné
teplo	teplo	k1gNnSc1	teplo
odevzdává	odevzdávat	k5eAaImIp3nS	odevzdávat
kapalina	kapalina	k1gFnSc1	kapalina
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
kapalina	kapalina	k1gFnSc1	kapalina
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
výparníku	výparník	k1gInSc2	výparník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
stěnách	stěna	k1gFnPc6	stěna
trubici	trubice	k1gFnSc4	trubice
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
průřezem	průřez	k1gInSc7	průřez
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
výměníku	výměník	k1gInSc6	výměník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
kapalinu	kapalina	k1gFnSc4	kapalina
prudce	prudko	k6eAd1	prudko
sníží	snížit	k5eAaPmIp3nS	snížit
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
a	a	k8xC	a
kapalina	kapalina	k1gFnSc1	kapalina
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
vypařovat	vypařovat	k5eAaImF	vypařovat
<g/>
.	.	kIx.	.
</s>
<s>
Potřebné	potřebný	k2eAgNnSc1d1	potřebné
skupenské	skupenský	k2eAgNnSc1d1	skupenské
teplo	teplo	k1gNnSc1	teplo
odebírá	odebírat	k5eAaImIp3nS	odebírat
z	z	k7c2	z
vnitřku	vnitřek	k1gInSc2	vnitřek
ledničky	lednička	k1gFnSc2	lednička
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc1	plyn
přiváděn	přivádět	k5eAaImNgInS	přivádět
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
kompresoru	kompresor	k1gInSc3	kompresor
a	a	k8xC	a
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Absorpční	absorpční	k2eAgFnSc1d1	absorpční
lednička	lednička	k1gFnSc1	lednička
===	===	k?	===
</s>
</p>
<p>
<s>
Absorpční	absorpční	k2eAgFnSc1d1	absorpční
lednička	lednička	k1gFnSc1	lednička
používá	používat	k5eAaImIp3nS	používat
plynné	plynný	k2eAgNnSc4d1	plynné
nebo	nebo	k8xC	nebo
kapalné	kapalný	k2eAgNnSc4d1	kapalné
chladicí	chladicí	k2eAgNnSc4d1	chladicí
médium	médium	k1gNnSc4	médium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bromid	bromid	k1gInSc4	bromid
lithný	lithný	k2eAgInSc1d1	lithný
<g/>
,	,	kIx,	,
čpavek	čpavek	k1gInSc1	čpavek
<g/>
)	)	kIx)	)
z	z	k7c2	z
chladicího	chladicí	k2eAgInSc2d1	chladicí
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
absorbéru	absorbér	k1gInSc6	absorbér
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
chladné	chladný	k2eAgFnSc6d1	chladná
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
odnímá	odnímat	k5eAaImIp3nS	odnímat
okolí	okolí	k1gNnSc1	okolí
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
i	i	k9	i
s	s	k7c7	s
rozpuštěným	rozpuštěný	k2eAgInSc7d1	rozpuštěný
plynem	plyn	k1gInSc7	plyn
proudí	proudit	k5eAaImIp3nP	proudit
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vařiče	vařič	k1gInPc1	vařič
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zahřáta	zahřát	k5eAaPmNgFnS	zahřát
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
plyn	plyn	k1gInSc1	plyn
opět	opět	k6eAd1	opět
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Ohřátý	ohřátý	k2eAgInSc1d1	ohřátý
plyn	plyn	k1gInSc1	plyn
o	o	k7c6	o
vyšším	vysoký	k2eAgInSc6d2	vyšší
tlaku	tlak	k1gInSc6	tlak
<g/>
,	,	kIx,	,
než	než	k8xS	než
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
absorbéru	absorbér	k1gInSc6	absorbér
<g/>
,	,	kIx,	,
proudí	proudit	k5eAaPmIp3nS	proudit
do	do	k7c2	do
chladiče	chladič	k1gInSc2	chladič
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
klesne	klesnout	k5eAaPmIp3nS	klesnout
a	a	k8xC	a
při	při	k7c6	při
daném	daný	k2eAgInSc6d1	daný
vyšším	vysoký	k2eAgInSc6d2	vyšší
tlaku	tlak	k1gInSc6	tlak
případně	případně	k6eAd1	případně
zkapalní	zkapalnit	k5eAaPmIp3nS	zkapalnit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
proudí	proudit	k5eAaImIp3nS	proudit
médium	médium	k1gNnSc4	médium
opět	opět	k6eAd1	opět
do	do	k7c2	do
absorbéru	absorbér	k1gInSc2	absorbér
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
zchlazené	zchlazený	k2eAgFnSc2d1	zchlazená
v	v	k7c6	v
samostatném	samostatný	k2eAgInSc6d1	samostatný
chladiči	chladič	k1gInSc6	chladič
a	a	k8xC	a
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Absorpční	absorpční	k2eAgFnSc1d1	absorpční
lednička	lednička	k1gFnSc1	lednička
má	mít	k5eAaImIp3nS	mít
proti	proti	k7c3	proti
kompresorové	kompresorový	k2eAgInPc1d1	kompresorový
nižší	nízký	k2eAgFnSc4d2	nižší
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
tak	tak	k6eAd1	tak
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
stačí	stačit	k5eAaBmIp3nS	stačit
její	její	k3xOp3gFnSc4	její
výkonnost	výkonnost	k1gFnSc4	výkonnost
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
běžných	běžný	k2eAgFnPc2d1	běžná
domácnostních	domácnostní	k2eAgFnPc2d1	domácnostní
chladniček	chladnička	k1gFnPc2	chladnička
s	s	k7c7	s
mrazicím	mrazicí	k2eAgInSc7d1	mrazicí
oddílem	oddíl	k1gInSc7	oddíl
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
velkých	velký	k2eAgInPc2d1	velký
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
hluboké	hluboký	k2eAgNnSc4d1	hluboké
mražení	mražení	k1gNnSc4	mražení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
má	mít	k5eAaImIp3nS	mít
bezhlučný	bezhlučný	k2eAgInSc1d1	bezhlučný
chod	chod	k1gInSc1	chod
<g/>
,	,	kIx,	,
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
pohyblivé	pohyblivý	k2eAgFnPc4d1	pohyblivá
součásti	součást	k1gFnPc4	součást
(	(	kIx(	(
<g/>
teoreticky	teoreticky	k6eAd1	teoreticky
vyšší	vysoký	k2eAgFnSc4d2	vyšší
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
napájena	napájen	k2eAgFnSc1d1	napájena
elektřinou	elektřina	k1gFnSc7	elektřina
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poháněna	pohánět	k5eAaImNgFnS	pohánět
pouze	pouze	k6eAd1	pouze
teplem	teplo	k1gNnSc7	teplo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
plynového	plynový	k2eAgInSc2d1	plynový
hořáku	hořák	k1gInSc2	hořák
nebo	nebo	k8xC	nebo
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgInSc7d1	postupný
vývojem	vývoj	k1gInSc7	vývoj
spolehlivějších	spolehlivý	k2eAgInPc2d2	spolehlivější
a	a	k8xC	a
tišších	tichý	k2eAgInPc2d2	tišší
kompresorů	kompresor	k1gInPc2	kompresor
a	a	k8xC	a
všeobecnou	všeobecný	k2eAgFnSc7d1	všeobecná
dostupností	dostupnost	k1gFnSc7	dostupnost
elektřiny	elektřina	k1gFnSc2	elektřina
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
využívají	využívat	k5eAaPmIp3nP	využívat
převážně	převážně	k6eAd1	převážně
kompresorové	kompresorový	k2eAgFnPc1d1	kompresorová
ledničky	lednička	k1gFnPc1	lednička
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
energetická	energetický	k2eAgFnSc1d1	energetická
úspornost	úspornost	k1gFnSc1	úspornost
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
tento	tento	k3xDgInSc4	tento
trend	trend	k1gInSc4	trend
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existovaly	existovat	k5eAaImAgFnP	existovat
i	i	k9	i
ledničky	lednička	k1gFnPc1	lednička
se	s	k7c7	s
suchým	suchý	k2eAgInSc7d1	suchý
absorbérem	absorbér	k1gInSc7	absorbér
(	(	kIx(	(
<g/>
CaCl	CaCl	k1gInSc1	CaCl
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pracovaly	pracovat	k5eAaImAgInP	pracovat
v	v	k7c6	v
cyklickém	cyklický	k2eAgInSc6d1	cyklický
režimu	režim	k1gInSc6	režim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
střídavému	střídavý	k2eAgNnSc3d1	střídavé
ohřívání	ohřívání	k1gNnSc3	ohřívání
a	a	k8xC	a
ochlazování	ochlazování	k1gNnSc3	ochlazování
absorbéru	absorbér	k1gInSc2	absorbér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Adsorpční	adsorpční	k2eAgFnSc1d1	adsorpční
lednička	lednička	k1gFnSc1	lednička
===	===	k?	===
</s>
</p>
<p>
<s>
Princip	princip	k1gInSc1	princip
adsorpční	adsorpční	k2eAgFnSc2d1	adsorpční
ledničky	lednička	k1gFnSc2	lednička
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xS	jako
u	u	k7c2	u
absorpční	absorpční	k2eAgFnSc2d1	absorpční
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
absorpčního	absorpční	k2eAgInSc2d1	absorpční
agregátu	agregát	k1gInSc2	agregát
se	se	k3xPyFc4	se
odnímá	odnímat	k5eAaImIp3nS	odnímat
okolí	okolí	k1gNnSc1	okolí
teplo	teplo	k6eAd1	teplo
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
chladicího	chladicí	k2eAgNnSc2d1	chladicí
média	médium	k1gNnSc2	médium
v	v	k7c6	v
absorbéru	absorbér	k1gInSc6	absorbér
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
tak	tak	k9	tak
děje	dít	k5eAaImIp3nS	dít
jeho	jeho	k3xOp3gNnSc7	jeho
přilnutím	přilnutí	k1gNnSc7	přilnutí
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
adsorbentu	adsorbent	k1gInSc2	adsorbent
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
houbovitě	houbovitě	k6eAd1	houbovitě
porézní	porézní	k2eAgFnSc7d1	porézní
hmotou	hmota	k1gFnSc7	hmota
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
velké	velký	k2eAgFnSc2d1	velká
povrchové	povrchový	k2eAgFnSc2d1	povrchová
plochy	plocha	k1gFnSc2	plocha
při	při	k7c6	při
malém	malý	k2eAgInSc6d1	malý
objemu	objem	k1gInSc6	objem
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
silikagel	silikagel	k1gInSc1	silikagel
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
podstatně	podstatně	k6eAd1	podstatně
lepších	dobrý	k2eAgInPc2d2	lepší
výsledků	výsledek	k1gInPc2	výsledek
se	s	k7c7	s
zeolitem	zeolit	k1gInSc7	zeolit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
chladicí	chladicí	k2eAgNnSc1d1	chladicí
médium	médium	k1gNnSc1	médium
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
různé	různý	k2eAgInPc1d1	různý
plyny	plyn	k1gInPc1	plyn
a	a	k8xC	a
kapaliny	kapalina	k1gFnPc1	kapalina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
i	i	k8xC	i
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
transportní	transportní	k2eAgFnPc1d1	transportní
chladničky	chladnička	k1gFnPc1	chladnička
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
energetického	energetický	k2eAgInSc2d1	energetický
zdroje	zdroj	k1gInSc2	zdroj
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
chlazení	chlazení	k1gNnSc2	chlazení
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
chladicí	chladicí	k2eAgNnSc1d1	chladicí
médium	médium	k1gNnSc1	médium
na	na	k7c6	na
vhodném	vhodný	k2eAgNnSc6d1	vhodné
místě	místo	k1gNnSc6	místo
zahřátím	zahřátí	k1gNnSc7	zahřátí
oddělí	oddělit	k5eAaPmIp3nS	oddělit
od	od	k7c2	od
adsorbentu	adsorbent	k1gInSc2	adsorbent
a	a	k8xC	a
po	po	k7c6	po
odděleném	oddělený	k2eAgInSc6d1	oddělený
transportu	transport	k1gInSc6	transport
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vrátí	vrátit	k5eAaPmIp3nS	vrátit
až	až	k9	až
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
chlazení	chlazení	k1gNnSc4	chlazení
třeba	třeba	k6eAd1	třeba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
fungují	fungovat	k5eAaImIp3nP	fungovat
například	například	k6eAd1	například
samochladící	samochladící	k2eAgInPc1d1	samochladící
sudy	sud	k1gInPc1	sud
na	na	k7c4	na
pivo	pivo	k1gNnSc4	pivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lednička	lednička	k1gFnSc1	lednička
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
Peltierova	Peltierův	k2eAgInSc2d1	Peltierův
článku	článek	k1gInSc2	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Peltierův	Peltierův	k2eAgInSc1d1	Peltierův
článek	článek	k1gInSc1	článek
využívá	využívat	k5eAaImIp3nS	využívat
Peltierova	Peltierův	k2eAgInSc2d1	Peltierův
jevu	jev	k1gInSc2	jev
–	–	k?	–
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
dvěma	dva	k4xCgFnPc7	dva
sériově	sériově	k6eAd1	sériově
zapojenými	zapojený	k2eAgMnPc7d1	zapojený
vodiči	vodič	k1gMnPc7	vodič
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
styčná	styčný	k2eAgFnSc1d1	styčná
plocha	plocha	k1gFnSc1	plocha
těchto	tento	k3xDgInPc2	tento
vodičů	vodič	k1gInPc2	vodič
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukční	konstrukční	k2eAgNnSc1d1	konstrukční
řešení	řešení	k1gNnSc1	řešení
bývá	bývat	k5eAaImIp3nS	bývat
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
sériově	sériově	k6eAd1	sériově
zapojených	zapojený	k2eAgInPc2d1	zapojený
článků	článek	k1gInPc2	článek
složena	složit	k5eAaPmNgFnS	složit
do	do	k7c2	do
bloku	blok	k1gInSc2	blok
s	s	k7c7	s
ohřívanou	ohřívaný	k2eAgFnSc7d1	ohřívaná
a	a	k8xC	a
ochlazovanou	ochlazovaný	k2eAgFnSc7d1	ochlazovaná
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bloku	blok	k1gInSc3	blok
přiléhají	přiléhat	k5eAaImIp3nP	přiléhat
žebra	žebro	k1gNnPc4	žebro
například	například	k6eAd1	například
z	z	k7c2	z
hliníku	hliník	k1gInSc2	hliník
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
snazší	snadný	k2eAgInSc4d2	snadnější
přenos	přenos	k1gInSc4	přenos
tepla	teplo	k1gNnSc2	teplo
mezi	mezi	k7c7	mezi
blokem	blok	k1gInSc7	blok
a	a	k8xC	a
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
tepelná	tepelný	k2eAgFnSc1d1	tepelná
výměna	výměna	k1gFnSc1	výměna
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
ventilátorem	ventilátor	k1gInSc7	ventilátor
<g/>
.	.	kIx.	.
</s>
<s>
Výhodami	výhoda	k1gFnPc7	výhoda
této	tento	k3xDgFnSc2	tento
technologie	technologie	k1gFnSc2	technologie
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc4d1	malý
rozměry	rozměr	k1gInPc4	rozměr
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
bezhlučného	bezhlučný	k2eAgInSc2d1	bezhlučný
provozu	provoz	k1gInSc2	provoz
(	(	kIx(	(
<g/>
žádné	žádný	k3yNgFnSc2	žádný
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
životnost	životnost	k1gFnSc1	životnost
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
regulace	regulace	k1gFnSc1	regulace
výkonu	výkon	k1gInSc2	výkon
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
přepínání	přepínání	k1gNnSc2	přepínání
chlazení	chlazení	k1gNnSc2	chlazení
<g/>
/	/	kIx~	/
<g/>
ohřev	ohřev	k1gInSc1	ohřev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
naopak	naopak	k6eAd1	naopak
patří	patřit	k5eAaImIp3nS	patřit
nízká	nízký	k2eAgFnSc1d1	nízká
účinnost	účinnost	k1gFnSc1	účinnost
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
<g/>
Ledničky	lednička	k1gFnPc1	lednička
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
Peltierova	Peltierův	k2eAgInSc2d1	Peltierův
článku	článek	k1gInSc2	článek
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
automobilech	automobil	k1gInPc6	automobil
a	a	k8xC	a
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
mobilita	mobilita	k1gFnSc1	mobilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Označení	označení	k1gNnSc1	označení
hvězdičkami	hvězdička	k1gFnPc7	hvězdička
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mraznicích	mraznice	k1gFnPc6	mraznice
a	a	k8xC	a
ledničkách	lednička	k1gFnPc6	lednička
vybavených	vybavený	k2eAgInPc2d1	vybavený
mraznicí	mraznice	k1gFnSc7	mraznice
je	být	k5eAaImIp3nS	být
mrazicí	mrazicí	k2eAgInSc1d1	mrazicí
výkon	výkon	k1gInSc1	výkon
označen	označit	k5eAaPmNgInS	označit
pomocí	pomoc	k1gFnSc7	pomoc
hvězdiček	hvězdička	k1gFnPc2	hvězdička
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
až	až	k9	až
čtyři	čtyři	k4xCgFnPc4	čtyři
hvězdičky	hvězdička	k1gFnPc4	hvězdička
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hvězdiček	hvězdička	k1gFnPc2	hvězdička
udává	udávat	k5eAaImIp3nS	udávat
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
užití	užití	k1gNnSc2	užití
mrazicí	mrazicí	k2eAgFnSc2d1	mrazicí
části	část	k1gFnSc2	část
lednice	lednice	k1gFnSc2	lednice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Energetické	energetický	k2eAgFnPc1d1	energetická
třídy	třída	k1gFnPc1	třída
a	a	k8xC	a
spotřeba	spotřeba	k1gFnSc1	spotřeba
elektřiny	elektřina	k1gFnSc2	elektřina
==	==	k?	==
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
lednice	lednice	k1gFnSc1	lednice
prodávaná	prodávaný	k2eAgFnSc1d1	prodávaná
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
energetickým	energetický	k2eAgInSc7d1	energetický
štítkem	štítek	k1gInSc7	štítek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
energetické	energetický	k2eAgFnSc6d1	energetická
třídě	třída	k1gFnSc6	třída
spotřebiče	spotřebič	k1gInSc2	spotřebič
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
energetické	energetický	k2eAgFnSc6d1	energetická
náročnosti	náročnost	k1gFnSc6	náročnost
při	při	k7c6	při
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
dělily	dělit	k5eAaImAgInP	dělit
od	od	k7c2	od
A	A	kA	A
+++	+++	k?	+++
až	až	k9	až
po	po	k7c6	po
E	E	kA	E
<g/>
,	,	kIx,	,
F	F	kA	F
či	či	k8xC	či
G.	G.	kA	G.
Aktuálně	aktuálně	k6eAd1	aktuálně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
prodávat	prodávat	k5eAaImF	prodávat
pouze	pouze	k6eAd1	pouze
lednice	lednice	k1gFnPc4	lednice
s	s	k7c7	s
energetických	energetický	k2eAgFnPc2d1	energetická
tříd	třída	k1gFnPc2	třída
A	A	kA	A
<g/>
+++	+++	k?	+++
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
++	++	k?	++
a	a	k8xC	a
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
lednic	lednice	k1gFnPc2	lednice
s	s	k7c7	s
třídou	třída	k1gFnSc7	třída
A	a	k8xC	a
a	a	k8xC	a
horší	zlý	k2eAgMnSc1d2	horší
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
energetická	energetický	k2eAgFnSc1d1	energetická
třída	třída	k1gFnSc1	třída
lednice	lednice	k1gFnSc2	lednice
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
promítá	promítat	k5eAaImIp3nS	promítat
do	do	k7c2	do
odlišné	odlišný	k2eAgFnSc2d1	odlišná
spotřeby	spotřeba	k1gFnSc2	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
výše	výše	k1gFnSc2	výše
účtů	účet	k1gInPc2	účet
za	za	k7c4	za
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
roční	roční	k2eAgInSc4d1	roční
provoz	provoz	k1gInSc4	provoz
lednice	lednice	k1gFnSc2	lednice
nejúspornější	úsporný	k2eAgFnSc2d3	nejúspornější
třídy	třída	k1gFnSc2	třída
A	A	kA	A
<g/>
+++	+++	k?	+++
stojí	stát	k5eAaImIp3nS	stát
běžnou	běžný	k2eAgFnSc4d1	běžná
domácnost	domácnost	k1gFnSc4	domácnost
do	do	k7c2	do
600	[number]	k4	600
Kč	Kč	kA	Kč
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
třídy	třída	k1gFnSc2	třída
A	a	k9	a
stejná	stejný	k2eAgFnSc1d1	stejná
domácnost	domácnost	k1gFnSc1	domácnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
odebírá	odebírat	k5eAaImIp3nS	odebírat
proud	proud	k1gInSc4	proud
v	v	k7c6	v
distribuční	distribuční	k2eAgFnSc6d1	distribuční
sazbě	sazba	k1gFnSc6	sazba
D	D	kA	D
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
d	d	k?	d
(	(	kIx(	(
<g/>
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
sazba	sazba	k1gFnSc1	sazba
pro	pro	k7c4	pro
byty	byt	k1gInPc4	byt
<g/>
)	)	kIx)	)
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
přes	přes	k7c4	přes
1000	[number]	k4	1000
Kč	Kč	kA	Kč
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
starší	starý	k2eAgFnSc1d2	starší
lednice	lednice	k1gFnSc1	lednice
tříd	třída	k1gFnPc2	třída
B	B	kA	B
či	či	k8xC	či
C	C	kA	C
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
už	už	k9	už
se	se	k3xPyFc4	se
neprodávají	prodávat	k5eNaImIp3nP	prodávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
je	on	k3xPp3gInPc4	on
mohou	moct	k5eAaImIp3nP	moct
domácnosti	domácnost	k1gFnPc1	domácnost
využívat	využívat	k5eAaImF	využívat
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
spotřebovat	spotřebovat	k5eAaPmF	spotřebovat
elektřinu	elektřina	k1gFnSc4	elektřina
za	za	k7c4	za
tisíce	tisíc	k4xCgInPc4	tisíc
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
večerní	večerní	k2eAgNnSc4d1	večerní
vyjídání	vyjídání	k1gNnSc4	vyjídání
ledničky	lednička	k1gFnSc2	lednička
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označované	označovaný	k2eAgFnPc1d1	označovaná
také	také	k9	také
jako	jako	k9	jako
orgasmus	orgasmus	k1gInSc4	orgasmus
před	před	k7c7	před
ledničkou	lednička	k1gFnSc7	lednička
jako	jako	k8xS	jako
vyjádření	vyjádření	k1gNnSc2	vyjádření
blaženého	blažený	k2eAgInSc2d1	blažený
pocitu	pocit	k1gInSc2	pocit
po	po	k7c6	po
vyjmutí	vyjmutí	k1gNnSc1	vyjmutí
a	a	k8xC	a
snězení	snězený	k2eAgMnPc1d1	snězený
něčeho	něco	k3yInSc2	něco
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
obsahu	obsah	k1gInSc2	obsah
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc4d1	bílý
sex	sex	k1gInSc4	sex
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lednice	lednice	k1gFnSc2	lednice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lednička	lednička	k1gFnSc1	lednička
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
