<s>
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
přes	přes	k7c4	přes
65,5	[number]	k4	65,5
metrů	metr	k1gInPc2	metr
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
dominant	dominanta	k1gFnPc2	dominanta
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
základna	základna	k1gFnSc1	základna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
324	[number]	k4	324
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
U	u	k7c2	u
rozhledny	rozhledna	k1gFnSc2	rozhledna
na	na	k7c6	na
temeni	temeno	k1gNnSc6	temeno
kopce	kopec	k1gInSc2	kopec
Petřín	Petřín	k1gInSc1	Petřín
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1889	[number]	k4	1889
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tak	tak	k9	tak
nadchli	nadchnout	k5eAaPmAgMnP	nadchnout
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
slavnou	slavný	k2eAgFnSc4d1	slavná
Eiffelovu	Eiffelův	k2eAgFnSc4d1	Eiffelova
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
podobnou	podobný	k2eAgFnSc4d1	podobná
dominantu	dominanta	k1gFnSc4	dominanta
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
Družstvo	družstvo	k1gNnSc4	družstvo
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
Petřínské	petřínský	k2eAgFnSc2d1	Petřínská
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
první	první	k4xOgInPc4	první
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
od	od	k7c2	od
magistrátu	magistrát	k1gInSc2	magistrát
získali	získat	k5eAaPmAgMnP	získat
pozemek	pozemek	k1gInSc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pětkrát	pětkrát	k6eAd1	pětkrát
menší	malý	k2eAgFnSc4d2	menší
napodobeninu	napodobenina	k1gFnSc4	napodobenina
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
vrch	vrch	k1gInSc1	vrch
Petřín	Petřín	k1gInSc1	Petřín
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
318	[number]	k4	318
m.	m.	k?	m.
V	v	k7c6	v
r.	r.	kA	r.
1890	[number]	k4	1890
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
projektová	projektový	k2eAgFnSc1d1	projektová
příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
zajištěny	zajištěn	k2eAgInPc1d1	zajištěn
potřebné	potřebný	k2eAgInPc1d1	potřebný
prostředky	prostředek	k1gInPc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1891	[number]	k4	1891
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Vratislava	Vratislava	k1gFnSc1	Vratislava
Pasovského	Pasovského	k2eAgNnPc2d1	Pasovského
<g/>
,	,	kIx,	,
autory	autor	k1gMnPc7	autor
konstrukce	konstrukce	k1gFnSc2	konstrukce
byli	být	k5eAaImAgMnP	být
Ing.	ing.	kA	ing.
František	František	k1gMnSc1	František
Prášil	Prášil	k1gMnSc1	Prášil
a	a	k8xC	a
Ing.	ing.	kA	ing.
Julius	Julius	k1gMnSc1	Julius
Souček	Souček	k1gMnSc1	Souček
z	z	k7c2	z
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
strojírny	strojírna	k1gFnSc2	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
pro	pro	k7c4	pro
Zemskou	zemský	k2eAgFnSc4d1	zemská
jubilejní	jubilejní	k2eAgFnSc4d1	jubilejní
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Viléma	Vilém	k1gMnSc2	Vilém
Kurze	kurz	k1gInSc6	kurz
a	a	k8xC	a
Vratislava	Vratislava	k1gFnSc1	Vratislava
Pasovského	Pasovského	k2eAgFnSc1d1	Pasovského
jako	jako	k8xS	jako
volná	volný	k2eAgFnSc1d1	volná
kopie	kopie	k1gFnSc1	kopie
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
projektem	projekt	k1gInSc7	projekt
seznámil	seznámit	k5eAaPmAgMnS	seznámit
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Vilém	Vilém	k1gMnSc1	Vilém
Kurz	Kurz	k1gMnSc1	Kurz
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
r.	r.	kA	r.
1890	[number]	k4	1890
českou	český	k2eAgFnSc4d1	Česká
veřejnost	veřejnost	k1gFnSc4	veřejnost
článkem	článek	k1gInSc7	článek
"	"	kIx"	"
<g/>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
<g/>
,	,	kIx,	,
obrázek	obrázek	k1gInSc1	obrázek
z	z	k7c2	z
blízké	blízký	k2eAgFnSc2d1	blízká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
Prahy	Praha	k1gFnSc2	Praha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
inženýrů	inženýr	k1gMnPc2	inženýr
Františka	František	k1gMnSc2	František
Prášila	Prášil	k1gMnSc2	Prášil
a	a	k8xC	a
Julia	Julius	k1gMnSc2	Julius
Součka	Souček	k1gMnSc2	Souček
byly	být	k5eAaImAgFnP	být
započaty	započat	k2eAgFnPc4d1	započata
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
dokončeny	dokončen	k2eAgInPc1d1	dokončen
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
.	.	kIx.	.
</s>
