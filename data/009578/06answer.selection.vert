<s>
Práce	práce	k1gFnPc1	práce
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
inženýrů	inženýr	k1gMnPc2	inženýr
Františka	František	k1gMnSc2	František
Prášila	Prášil	k1gMnSc2	Prášil
a	a	k8xC	a
Julia	Julius	k1gMnSc2	Julius
Součka	Souček	k1gMnSc2	Souček
byly	být	k5eAaImAgFnP	být
započaty	započat	k2eAgFnPc4d1	započata
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
dokončeny	dokončen	k2eAgInPc1d1	dokončen
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
.	.	kIx.	.
</s>
