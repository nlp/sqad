<s>
Dórský	dórský	k2eAgInSc1d1	dórský
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mužský	mužský	k2eAgInSc1d1	mužský
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
antický	antický	k2eAgInSc4d1	antický
stavební	stavební	k2eAgInSc4d1	stavební
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
se	se	k3xPyFc4	se
monumentální	monumentální	k2eAgFnSc7d1	monumentální
střízlivostí	střízlivost	k1gFnSc7	střízlivost
a	a	k8xC	a
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
<g/>
.	.	kIx.	.
</s>
