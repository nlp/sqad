<p>
<s>
Dórský	dórský	k2eAgInSc1d1	dórský
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mužský	mužský	k2eAgInSc1d1	mužský
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
antický	antický	k2eAgInSc4d1	antický
stavební	stavební	k2eAgInSc4d1	stavební
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
se	se	k3xPyFc4	se
monumentální	monumentální	k2eAgFnSc7d1	monumentální
střízlivostí	střízlivost	k1gFnSc7	střízlivost
a	a	k8xC	a
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
<g/>
.	.	kIx.	.
</s>
<s>
Užíval	užívat	k5eAaImAgInS	užívat
se	se	k3xPyFc4	se
na	na	k7c6	na
pevninské	pevninský	k2eAgFnSc6d1	pevninská
části	část	k1gFnSc6	část
Řecka	Řecko	k1gNnSc2	Řecko
–	–	k?	–
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Peloponnés	Peloponnésa	k1gFnPc2	Peloponnésa
–	–	k?	–
i	i	k9	i
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Paestum	Paestum	k1gNnSc1	Paestum
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
(	(	kIx(	(
<g/>
Agrigento	Agrigento	k1gNnSc1	Agrigento
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
i	i	k9	i
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
i	i	k9	i
často	často	k6eAd1	často
napodobován	napodobován	k2eAgMnSc1d1	napodobován
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
klasické	klasický	k2eAgFnSc2d1	klasická
řádové	řádový	k2eAgFnSc2d1	řádová
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Dórský	dórský	k2eAgInSc1d1	dórský
řád	řád	k1gInSc1	řád
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mykénského	mykénský	k2eAgMnSc2d1	mykénský
megara	megar	k1gMnSc2	megar
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
zřetelné	zřetelný	k2eAgFnPc4d1	zřetelná
stopy	stopa	k1gFnPc4	stopa
dřívější	dřívější	k2eAgFnSc2d1	dřívější
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
v	v	k7c6	v
detailech	detail	k1gInPc6	detail
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
I	I	kA	I
když	když	k8xS	když
každý	každý	k3xTgInSc1	každý
dórský	dórský	k2eAgInSc1d1	dórský
chrám	chrám	k1gInSc1	chrám
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
znaky	znak	k1gInPc4	znak
dórského	dórský	k2eAgInSc2d1	dórský
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
dodržovaly	dodržovat	k5eAaImAgInP	dodržovat
s	s	k7c7	s
překvapující	překvapující	k2eAgFnSc7d1	překvapující
přesností	přesnost	k1gFnSc7	přesnost
a	a	k8xC	a
důsledností	důslednost	k1gFnSc7	důslednost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Znaky	znak	k1gInPc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Sloupy	sloup	k1gInPc1	sloup
<g/>
,	,	kIx,	,
architráv	architráv	k1gInSc1	architráv
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
štítu	štít	k1gInSc2	štít
i	i	k8xC	i
triglyfy	triglyf	k1gInPc4	triglyf
(	(	kIx(	(
<g/>
vyčnělé	vyčnělý	k2eAgInPc4d1	vyčnělý
konce	konec	k1gInPc4	konec
stropních	stropní	k2eAgInPc2d1	stropní
trámů	trám	k1gInPc2	trám
<g/>
)	)	kIx)	)
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
dřevěné	dřevěný	k2eAgFnSc3d1	dřevěná
architektuře	architektura	k1gFnSc3	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Dórský	dórský	k2eAgInSc1d1	dórský
řád	řád	k1gInSc1	řád
vyniká	vynikat	k5eAaImIp3nS	vynikat
monumentální	monumentální	k2eAgFnSc7d1	monumentální
prostotou	prostota	k1gFnSc7	prostota
až	až	k8xS	až
strohostí	strohost	k1gFnSc7	strohost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
vyvažují	vyvažovat	k5eAaImIp3nP	vyvažovat
jen	jen	k9	jen
plastické	plastický	k2eAgFnPc1d1	plastická
výzdoby	výzdoba	k1gFnPc1	výzdoba
tympanonu	tympanon	k1gInSc2	tympanon
a	a	k8xC	a
metóp	metóp	k1gInSc4	metóp
<g/>
.	.	kIx.	.
</s>
<s>
Proporčně	proporčně	k6eAd1	proporčně
jsou	být	k5eAaImIp3nP	být
dórské	dórský	k2eAgInPc4d1	dórský
sloupy	sloup	k1gInPc4	sloup
nejmohutnější	mohutný	k2eAgInPc4d3	nejmohutnější
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
antických	antický	k2eAgInPc2d1	antický
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
iónského	iónský	k2eAgInSc2d1	iónský
–	–	k?	–
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
mužským	mužský	k2eAgMnSc7d1	mužský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dórské	dórský	k2eAgInPc1d1	dórský
chrámy	chrám	k1gInPc1	chrám
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
barevné	barevný	k2eAgInPc1d1	barevný
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
červené	červený	k2eAgNnSc1d1	červené
a	a	k8xC	a
modré	modrý	k2eAgNnSc1d1	modré
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Joachim	Joachim	k1gMnSc1	Joachim
Winckelmann	Winckelmann	k1gMnSc1	Winckelmann
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
jen	jen	k9	jen
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Části	část	k1gFnSc6	část
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Štít	štít	k1gInSc4	štít
====	====	k?	====
</s>
</p>
<p>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
antického	antický	k2eAgInSc2d1	antický
chrámu	chrám	k1gInSc2	chrám
vrcholí	vrcholit	k5eAaImIp3nP	vrcholit
trojúhelníkovým	trojúhelníkový	k2eAgInSc7d1	trojúhelníkový
štítem	štít	k1gInSc7	štít
se	s	k7c7	s
šikmou	šikma	k1gFnSc7	šikma
(	(	kIx(	(
<g/>
sima	sima	k1gFnSc1	sima
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodorovnou	vodorovný	k2eAgFnSc7d1	vodorovná
(	(	kIx(	(
<g/>
geison	geisona	k1gFnPc2	geisona
<g/>
)	)	kIx)	)
římsou	římsa	k1gFnSc7	římsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výplň	výplň	k1gFnSc1	výplň
čili	čili	k8xC	čili
tympanon	tympanon	k1gInSc1	tympanon
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zdoben	zdobit	k5eAaImNgInS	zdobit
reliéfem	reliéf	k1gInSc7	reliéf
a	a	k8xC	a
osazen	osadit	k5eAaPmNgInS	osadit
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
a	a	k8xC	a
na	na	k7c6	na
rozích	roh	k1gInPc6	roh
akrotérii	akrotérie	k1gFnSc3	akrotérie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
tvar	tvar	k1gInSc4	tvar
palmety	palmeta	k1gFnSc2	palmeta
<g/>
,	,	kIx,	,
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
postavy	postava	k1gFnSc2	postava
nebo	nebo	k8xC	nebo
gryfa	gryf	k1gMnSc2	gryf
–	–	k?	–
lva	lev	k1gInSc2	lev
s	s	k7c7	s
orlí	orlí	k2eAgFnSc7d1	orlí
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tašky	taška	k1gFnPc1	taška
byly	být	k5eAaImAgFnP	být
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
pálené	pálený	k2eAgFnSc2d1	pálená
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
z	z	k7c2	z
mramoru	mramor	k1gInSc2	mramor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kladí	kladí	k1gNnSc2	kladí
====	====	k?	====
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
římsa	římsa	k1gFnSc1	římsa
s	s	k7c7	s
kapkami	kapka	k1gFnPc7	kapka
–	–	k?	–
guttae	gutta	k1gInSc2	gutta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
vlysem	vlys	k1gInSc7	vlys
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
složen	složen	k2eAgInSc1d1	složen
ze	z	k7c2	z
střídavě	střídavě	k6eAd1	střídavě
položených	položený	k2eAgFnPc2d1	položená
obdélných	obdélný	k2eAgFnPc2d1	obdélná
desek	deska	k1gFnPc2	deska
–	–	k?	–
triglyfů	triglyf	k1gInPc2	triglyf
se	s	k7c7	s
žlábkovým	žlábkový	k2eAgInSc7d1	žlábkový
ornamentem	ornament	k1gInSc7	ornament
a	a	k8xC	a
reliéfně	reliéfně	k6eAd1	reliéfně
zdobenými	zdobený	k2eAgFnPc7d1	zdobená
metopami	metopa	k1gFnPc7	metopa
<g/>
.	.	kIx.	.
</s>
<s>
Nejspodnější	spodní	k2eAgFnSc7d3	nejspodnější
částí	část	k1gFnSc7	část
kladí	kladí	k1gNnSc2	kladí
je	být	k5eAaImIp3nS	být
hladký	hladký	k2eAgInSc4d1	hladký
architráv	architráv	k1gInSc4	architráv
tvořený	tvořený	k2eAgInSc1d1	tvořený
souvislými	souvislý	k2eAgInPc7d1	souvislý
kvádry	kvádr	k1gInPc7	kvádr
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
sloupy	sloup	k1gInPc7	sloup
====	====	k?	====
</s>
</p>
<p>
<s>
Abakus	abakus	k1gInSc1	abakus
(	(	kIx(	(
<g/>
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
deska	deska	k1gFnSc1	deska
<g/>
)	)	kIx)	)
a	a	k8xC	a
okrouhlý	okrouhlý	k2eAgInSc1d1	okrouhlý
echinus	echinus	k1gInSc1	echinus
(	(	kIx(	(
<g/>
poduška	poduška	k1gFnSc1	poduška
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
prostou	prostý	k2eAgFnSc4d1	prostá
dvoudílnou	dvoudílný	k2eAgFnSc4d1	dvoudílná
hlavici	hlavice	k1gFnSc4	hlavice
podle	podle	k7c2	podle
mykénského	mykénský	k2eAgInSc2d1	mykénský
vzoru	vzor	k1gInSc2	vzor
a	a	k8xC	a
napojuje	napojovat	k5eAaImIp3nS	napojovat
architráv	architráv	k1gInSc4	architráv
na	na	k7c4	na
sloup	sloup	k1gInSc4	sloup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masivní	masivní	k2eAgInPc1d1	masivní
mírně	mírně	k6eAd1	mírně
kónické	kónický	k2eAgInPc1d1	kónický
sloupy	sloup	k1gInPc1	sloup
mají	mít	k5eAaImIp3nP	mít
dřík	dřík	k1gInSc1	dřík
kanelovaný	kanelovaný	k2eAgInSc1d1	kanelovaný
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
svislými	svislý	k2eAgInPc7d1	svislý
žlábky	žlábek	k1gInPc7	žlábek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
v	v	k7c6	v
ostré	ostrý	k2eAgFnSc6d1	ostrá
hraně	hrana	k1gFnSc6	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
žlábky	žlábek	k1gInPc1	žlábek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
dříku	dřík	k1gInSc2	dřík
přerušeny	přerušit	k5eAaPmNgInP	přerušit
několika	několik	k4yIc2	několik
prstenci	prstenec	k1gInPc7	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
vzhůru	vzhůru	k6eAd1	vzhůru
zužuje	zužovat	k5eAaImIp3nS	zužovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
mírně	mírně	k6eAd1	mírně
vydutý	vydutý	k2eAgInSc1d1	vydutý
do	do	k7c2	do
soudku	soudek	k1gInSc2	soudek
(	(	kIx(	(
<g/>
entase	entase	k6eAd1	entase
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzhled	vzhled	k1gInSc1	vzhled
stavby	stavba	k1gFnSc2	stavba
odlehčil	odlehčit	k5eAaPmAgInS	odlehčit
<g/>
.	.	kIx.	.
</s>
<s>
Dórský	dórský	k2eAgInSc1d1	dórský
sloup	sloup	k1gInSc1	sloup
nemá	mít	k5eNaImIp3nS	mít
patku	patka	k1gFnSc4	patka
a	a	k8xC	a
spočívá	spočívat	k5eAaImIp3nS	spočívat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
podstavě	podstava	k1gFnSc6	podstava
–	–	k?	–
stylobatu	stylobat	k1gInSc2	stylobat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Stereobat	Stereobat	k1gMnPc5	Stereobat
====	====	k?	====
</s>
</p>
<p>
<s>
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
podstava	podstava	k1gFnSc1	podstava
celé	celý	k2eAgFnSc2d1	celá
stavby	stavba	k1gFnSc2	stavba
čili	čili	k8xC	čili
stereobat	stereobat	k5eAaImF	stereobat
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vysokých	vysoký	k2eAgInPc2d1	vysoký
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
krepidů	krepid	k1gInPc2	krepid
<g/>
,	,	kIx,	,
a	a	k8xC	a
podzemních	podzemní	k2eAgInPc2d1	podzemní
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
<g/>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
stupeň	stupeň	k1gInSc1	stupeň
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgNnP	nazývat
stylobat	stylobat	k5eAaImF	stylobat
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
stupeň	stupeň	k1gInSc1	stupeň
základů	základ	k1gInPc2	základ
euthyntérie	euthyntérie	k1gFnSc2	euthyntérie
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
krepidy	krepida	k1gFnPc1	krepida
byly	být	k5eAaImAgFnP	být
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
klasické	klasický	k2eAgInPc1d1	klasický
stupně	stupeň	k1gInPc1	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
stereobatu	stereobat	k1gInSc6	stereobat
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
vždy	vždy	k6eAd1	vždy
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Doplňujícím	doplňující	k2eAgInSc7d1	doplňující
prvkem	prvek	k1gInSc7	prvek
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
někdy	někdy	k6eAd1	někdy
jen	jen	k6eAd1	jen
přikládáno	přikládán	k2eAgNnSc1d1	přikládáno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
vtesáváno	vtesávat	k5eAaImNgNnS	vtesávat
do	do	k7c2	do
krepidů	krepid	k1gInPc2	krepid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podstavcem	podstavec	k1gInSc7	podstavec
(	(	kIx(	(
<g/>
stylobatem	stylobat	k1gInSc7	stylobat
<g/>
)	)	kIx)	)
konstrukce	konstrukce	k1gFnSc1	konstrukce
chrámu	chrám	k1gInSc2	chrám
začínala	začínat	k5eAaImAgFnS	začínat
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
vyhlazený	vyhlazený	k2eAgInSc4d1	vyhlazený
povrch	povrch	k1gInSc4	povrch
si	se	k3xPyFc3	se
kameníci	kameník	k1gMnPc1	kameník
rýsovali	rýsovat	k5eAaImAgMnP	rýsovat
tvary	tvar	k1gInPc4	tvar
sloupů	sloup	k1gInPc2	sloup
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
detailů	detail	k1gInPc2	detail
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srovnání	srovnání	k1gNnSc1	srovnání
s	s	k7c7	s
iónským	iónský	k2eAgInSc7d1	iónský
řádem	řád	k1gInSc7	řád
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Estetika	estetik	k1gMnSc2	estetik
a	a	k8xC	a
optické	optický	k2eAgInPc4d1	optický
klamy	klam	k1gInPc4	klam
===	===	k?	===
</s>
</p>
<p>
<s>
Ideální	ideální	k2eAgFnPc1d1	ideální
proporce	proporce	k1gFnPc1	proporce
<g/>
,	,	kIx,	,
vyváženost	vyváženost	k1gFnSc1	vyváženost
horizontál	horizontála	k1gFnPc2	horizontála
a	a	k8xC	a
vertikál	vertikála	k1gFnPc2	vertikála
spojených	spojený	k2eAgFnPc2d1	spojená
šikmými	šikmý	k2eAgFnPc7d1	šikmá
liniemi	linie	k1gFnPc7	linie
štítů	štít	k1gInPc2	štít
je	být	k5eAaImIp3nS	být
udivující	udivující	k2eAgNnPc4d1	udivující
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jí	on	k3xPp3gFnSc3	on
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
geometrizací	geometrizace	k1gFnSc7	geometrizace
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
estetický	estetický	k2eAgInSc1d1	estetický
kánon	kánon	k1gInSc1	kánon
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
filosofické	filosofický	k2eAgFnSc2d1	filosofická
představy	představa	k1gFnSc2	představa
o	o	k7c6	o
ideálních	ideální	k2eAgInPc6d1	ideální
geometrických	geometrický	k2eAgInPc6d1	geometrický
tvarech	tvar	k1gInPc6	tvar
a	a	k8xC	a
proporcích	proporce	k1gFnPc6	proporce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
takového	takový	k3xDgInSc2	takový
kánonu	kánon	k1gInSc2	kánon
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
modul	modul	k1gInSc1	modul
jako	jako	k8xS	jako
poměrný	poměrný	k2eAgInSc1d1	poměrný
princip	princip	k1gInSc1	princip
používaný	používaný	k2eAgInSc1d1	používaný
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
antických	antický	k2eAgInPc2d1	antický
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dórský	dórský	k2eAgInSc1d1	dórský
sloup	sloup	k1gInSc1	sloup
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
11	[number]	k4	11
takových	takový	k3xDgInPc2	takový
modulů	modul	k1gInPc2	modul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zejména	zejména	k9	zejména
dórské	dórský	k2eAgInPc1d1	dórský
chrámy	chrám	k1gInPc1	chrám
záměrně	záměrně	k6eAd1	záměrně
využívaly	využívat	k5eAaImAgInP	využívat
různé	různý	k2eAgInPc1d1	různý
optické	optický	k2eAgInPc1d1	optický
klamy	klam	k1gInPc1	klam
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
kladly	klást	k5eAaImAgFnP	klást
nesmírné	smírný	k2eNgInPc4d1	nesmírný
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
přesnost	přesnost	k1gFnSc4	přesnost
kamenické	kamenický	k2eAgFnSc2d1	kamenická
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nepatrně	patrně	k6eNd1	patrně
soudkovitém	soudkovitý	k2eAgNnSc6d1	soudkovité
vydutí	vydutí	k1gNnSc6	vydutí
sloupů	sloup	k1gInPc2	sloup
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
už	už	k6eAd1	už
zmínili	zmínit	k5eAaPmAgMnP	zmínit
<g/>
,	,	kIx,	,
podlaha	podlaha	k1gFnSc1	podlaha
chrámu	chrám	k1gInSc2	chrám
byla	být	k5eAaImAgFnS	být
nepatrně	patrně	k6eNd1	patrně
vypuklá	vypuklý	k2eAgFnSc1d1	vypuklá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prostor	prostor	k1gInSc1	prostor
vypadal	vypadat	k5eAaImAgInS	vypadat
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Rohové	rohový	k2eAgInPc1d1	rohový
sloupy	sloup	k1gInPc1	sloup
nebyly	být	k5eNaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
kolmo	kolmo	k6eAd1	kolmo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lehce	lehko	k6eAd1	lehko
šikmo	šikmo	k6eAd1	šikmo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
zrak	zrak	k1gInSc4	zrak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
optického	optický	k2eAgInSc2d1	optický
klamu	klam	k1gInSc2	klam
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
svislé	svislý	k2eAgFnPc1d1	svislá
(	(	kIx(	(
<g/>
Platón	Platón	k1gMnSc1	Platón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělci	umělec	k1gMnPc1	umělec
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
komplex	komplex	k1gInSc1	komplex
dórských	dórský	k2eAgFnPc2d1	dórská
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
Akropolis	Akropolis	k1gFnSc1	Akropolis
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
vybudovat	vybudovat	k5eAaPmF	vybudovat
i	i	k9	i
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
svých	svůj	k3xOyFgMnPc2	svůj
nepřátel	nepřítel	k1gMnPc2	nepřítel
Periklés	Periklésa	k1gFnPc2	Periklésa
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
447	[number]	k4	447
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
výstavbou	výstavba	k1gFnSc7	výstavba
Parthenónu	Parthenón	k1gInSc2	Parthenón
a	a	k8xC	a
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
438	[number]	k4	438
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jeho	jeho	k3xOp3gMnSc7	jeho
architekty	architekt	k1gMnPc4	architekt
byli	být	k5eAaImAgMnP	být
Iktinos	Iktinos	k1gMnSc1	Iktinos
a	a	k8xC	a
Kallikrates	Kallikrates	k1gMnSc1	Kallikrates
<g/>
,	,	kIx,	,
sochařskou	sochařský	k2eAgFnSc4d1	sochařská
výzdobu	výzdoba	k1gFnSc4	výzdoba
provedl	provést	k5eAaPmAgInS	provést
Feidiás	Feidiás	k1gInSc1	Feidiás
–	–	k?	–
včetně	včetně	k7c2	včetně
dávno	dávno	k6eAd1	dávno
zničené	zničený	k2eAgFnSc2d1	zničená
sochy	socha	k1gFnSc2	socha
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dominovala	dominovat	k5eAaImAgFnS	dominovat
uprostřed	uprostřed	k7c2	uprostřed
Parthenónu	Parthenón	k1gInSc2	Parthenón
<g/>
.	.	kIx.	.
</s>
<s>
Mnésiklés	Mnésiklés	k6eAd1	Mnésiklés
postavil	postavit	k5eAaPmAgMnS	postavit
Propylaje	Propylaje	k1gMnSc1	Propylaje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
stavby	stavba	k1gFnPc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Héřin	Héřin	k2eAgInSc1d1	Héřin
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
(	(	kIx(	(
<g/>
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diův	Diův	k2eAgInSc1d1	Diův
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
(	(	kIx(	(
<g/>
470	[number]	k4	470
<g/>
–	–	k?	–
<g/>
457	[number]	k4	457
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Apollónův	Apollónův	k2eAgInSc1d1	Apollónův
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
(	(	kIx(	(
<g/>
asi	asi	k9	asi
560	[number]	k4	560
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
–	–	k?	–
harmonicky	harmonicky	k6eAd1	harmonicky
zasazen	zasadit	k5eAaPmNgInS	zasadit
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
fragmenty	fragment	k1gInPc1	fragment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paestum	Paestum	k1gNnSc1	Paestum
(	(	kIx(	(
<g/>
Pestum	Pestum	k1gNnSc1	Pestum
<g/>
)	)	kIx)	)
u	u	k7c2	u
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Poseidonia	Poseidonium	k1gNnSc2	Poseidonium
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Héřin	Héřin	k2eAgInSc1d1	Héřin
chrám	chrám	k1gInSc1	chrám
s	s	k7c7	s
devíti	devět	k4xCc7	devět
sloupy	sloup	k1gInPc7	sloup
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
550	[number]	k4	550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Athénin	Athénin	k2eAgInSc1d1	Athénin
chrám	chrám	k1gInSc1	chrám
se	s	k7c7	s
šesti	šest	k4xCc7	šest
sloupy	sloup	k1gInPc7	sloup
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Cereřin	Cereřin	k2eAgInSc1d1	Cereřin
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poseidónův	Poseidónův	k2eAgInSc1d1	Poseidónův
či	či	k8xC	či
Apollónův	Apollónův	k2eAgInSc1d1	Apollónův
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
zachovaný	zachovaný	k2eAgMnSc1d1	zachovaný
<g/>
,	,	kIx,	,
se	s	k7c7	s
šesti	šest	k4xCc7	šest
sloupy	sloup	k1gInPc7	sloup
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
(	(	kIx(	(
<g/>
hexastylos	hexastylos	k1gMnSc1	hexastylos
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
450	[number]	k4	450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
stál	stát	k5eAaImAgInS	stát
původně	původně	k6eAd1	původně
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
vítal	vítat	k5eAaImAgMnS	vítat
mořeplavce	mořeplavec	k1gMnSc4	mořeplavec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Athény	Athéna	k1gFnPc1	Athéna
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Parthenón	Parthenón	k1gInSc1	Parthenón
na	na	k7c6	na
Akropoli	Akropole	k1gFnSc6	Akropole
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
chrám	chrám	k1gInSc1	chrám
bohyně	bohyně	k1gFnSc2	bohyně
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
dórský	dórský	k2eAgInSc1d1	dórský
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tureckých	turecký	k2eAgFnPc6d1	turecká
dobách	doba	k1gFnPc6	doba
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
prachárna	prachárna	k1gFnSc1	prachárna
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
Akropolí	Akropole	k1gFnSc7	Akropole
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
,	,	kIx,	,
když	když	k8xS	když
Benátčané	Benátčan	k1gMnPc1	Benátčan
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
Parthenón	Parthenón	k1gInSc4	Parthenón
granátem	granát	k1gInSc7	granát
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Propylaje	Propylat	k5eAaImSgMnS	Propylat
jsou	být	k5eAaImIp3nP	být
vstupem	vstup	k1gInSc7	vstup
na	na	k7c4	na
Akropolis	Akropolis	k1gFnSc4	Akropolis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Héfaistův	Héfaistův	k2eAgInSc1d1	Héfaistův
chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
agoře	agora	k1gFnSc6	agora
pod	pod	k7c7	pod
Akropolí	Akropole	k1gFnSc7	Akropole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Černá	Černá	k1gFnSc1	Černá
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Idea	idea	k1gFnSc1	idea
servis	servis	k1gInSc1	servis
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85970	[number]	k4	85970
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Lonely	Lonel	k1gInPc1	Lonel
Planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7237-628-4	[number]	k4	80-7237-628-4
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Starověká	starověký	k2eAgFnSc1d1	starověká
řecká	řecký	k2eAgFnSc1d1	řecká
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
řádová	řádový	k2eAgFnSc1d1	řádová
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
Iónský	iónský	k2eAgInSc1d1	iónský
řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
Korintský	korintský	k2eAgInSc1d1	korintský
řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
Kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dórský	dórský	k2eAgInSc4d1	dórský
řád	řád	k1gInSc4	řád
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
