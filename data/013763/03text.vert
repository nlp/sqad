<s>
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
</s>
<s>
Lída	Lída	k1gFnSc1
Durdíková	Durdíkový	k2eAgFnSc1d1
Narození	narození	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1899	#num#	k4
Praha	Praha	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1955	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
55	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Paříž	Paříž	k1gFnSc1
Pseudonym	pseudonym	k1gInSc1
</s>
<s>
Lida	Lido	k1gNnPc1
Povolání	povolání	k1gNnPc2
</s>
<s>
spisovatelka	spisovatelka	k1gFnSc1
<g/>
,	,	kIx,
pedagožka	pedagožka	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Paul	Paul	k1gMnSc1
Faucher	Fauchra	k1gFnPc2
Děti	dítě	k1gFnPc1
</s>
<s>
François	François	k1gFnSc7
Faucher	Fauchra	k1gFnPc2
Rodiče	rodič	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Durdík	Durdík	k1gMnSc1
<g/>
,	,	kIx,
Pavlína	Pavlína	k1gFnSc1
Durdíková-Havlová	Durdíková-Havlová	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
<g/>
,	,	kIx,
provdaná	provdaný	k2eAgFnSc1d1
Faucher	Faucher	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
Knize	kniha	k1gFnSc6
pokřtěných	pokřtěný	k2eAgFnPc2d1
Ludmila	Ludmila	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1899	#num#	k4
Praha-Smíchov	Praha-Smíchov	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1955	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
česko-francouzská	česko-francouzský	k2eAgFnSc1d1
spisovatelka	spisovatelka	k1gFnSc1
<g/>
,	,	kIx,
překladatelka	překladatelka	k1gFnSc1
a	a	k8xC
pedagožka	pedagožka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
v	v	k7c6
rodině	rodina	k1gFnSc6
evangelíka	evangelík	k1gMnSc2
MUDr.	MUDr.	kA
Václava	Václav	k1gMnSc2
Durdíka	Durdík	k1gMnSc2
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
katoličky	katolička	k1gFnSc2
Pavlíny	Pavlína	k1gFnSc2
Durdíkové-Havlové	Durdíkové-Havlová	k1gFnSc2
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzali	vzít	k5eAaPmAgMnP
se	s	k7c7
r.	r.	kA
1894	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
dva	dva	k4xCgMnPc4
sourozence	sourozenec	k1gMnPc4
<g/>
:	:	kIx,
Vlastu	Vlasta	k1gMnSc4
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Viléma	Vilém	k1gMnSc2
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1918	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
spolupracovala	spolupracovat	k5eAaImAgFnS
s	s	k7c7
pedagogem	pedagog	k1gMnSc7
Františkem	František	k1gMnSc7
Bakulem	Bakul	k1gMnSc7
jako	jako	k8xS,k8xC
asistentka	asistentka	k1gFnSc1
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
v	v	k7c6
Jedličkově	Jedličkův	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
s	s	k7c7
tělesným	tělesný	k2eAgNnSc7d1
postižením	postižení	k1gNnSc7
<g/>
,	,	kIx,
po	po	k7c6
Bakulově	Bakulův	k2eAgInSc6d1
odchodu	odchod	k1gInSc6
z	z	k7c2
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
družině	družina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakule	bakule	k1gFnSc1
byl	být	k5eAaImAgInS
předchůdce	předchůdce	k1gMnSc1
a	a	k8xC
průkopník	průkopník	k1gMnSc1
moderní	moderní	k2eAgFnSc2d1
integrativní	integrativní	k2eAgFnSc2d1
pedagogiky	pedagogika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
R.	R.	kA
1929	#num#	k4
se	se	k3xPyFc4
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
setkala	setkat	k5eAaPmAgFnS
s	s	k7c7
Paulem	Paul	k1gMnSc7
Faucherem	Faucher	k1gMnSc7
(	(	kIx(
<g/>
1898	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
francouzským	francouzský	k2eAgMnSc7d1
vydavatelem	vydavatel	k1gMnSc7
<g/>
,	,	kIx,
učitelem	učitel	k1gMnSc7
a	a	k8xC
autorem	autor	k1gMnSc7
dětské	dětský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
organizoval	organizovat	k5eAaBmAgMnS
koncertní	koncertní	k2eAgNnSc4d1
turné	turné	k1gNnSc4
Bakulových	Bakulův	k2eAgMnPc2d1
zpěváčků	zpěváček	k1gMnPc2
po	po	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1932	#num#	k4
se	se	k3xPyFc4
za	za	k7c4
něho	on	k3xPp3gMnSc4
provdala	provdat	k5eAaPmAgFnS
a	a	k8xC
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodil	narodit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
François	François	k1gFnSc2
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1933	#num#	k4
odjela	odjet	k5eAaPmAgFnS
s	s	k7c7
manželem	manžel	k1gMnSc7
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc7
spolupracovnicí	spolupracovnice	k1gFnSc7
a	a	k8xC
pobývala	pobývat	k5eAaImAgFnS
s	s	k7c7
ním	on	k3xPp3gMnSc7
převážně	převážně	k6eAd1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
se	se	k3xPyFc4
zajímala	zajímat	k5eAaImAgFnS
o	o	k7c4
literární	literární	k2eAgNnSc4d1
dění	dění	k1gNnSc4
<g/>
,	,	kIx,
stýkala	stýkat	k5eAaImAgFnS
se	se	k3xPyFc4
mj.	mj.	kA
s	s	k7c7
Vítězslavem	Vítězslav	k1gMnSc7
Nezvalem	Nezval	k1gMnSc7
<g/>
,	,	kIx,
Františkem	František	k1gMnSc7
Halasem	Halas	k1gMnSc7
<g/>
,	,	kIx,
Janem	Jan	k1gMnSc7
Čepem	Čep	k1gMnSc7
a	a	k8xC
Toyen	Toyen	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c4
Francii	Francie	k1gFnSc4
patřili	patřit	k5eAaImAgMnP
k	k	k7c3
jejím	její	k3xOp3gMnPc3
přátelům	přítel	k1gMnPc3
André	André	k1gMnPc1
Breton	Breton	k1gMnSc1
a	a	k8xC
Paul	Paul	k1gMnSc1
Fort	Fort	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispívala	přispívat	k5eAaImAgFnS
do	do	k7c2
řady	řada	k1gFnSc2
periodik	periodikum	k1gNnPc2
jak	jak	k8xC,k8xS
drobnými	drobný	k2eAgFnPc7d1
prózami	próza	k1gFnPc7
a	a	k8xC
publicistickými	publicistický	k2eAgInPc7d1
texty	text	k1gInPc7
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
překlady	překlad	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
propagovala	propagovat	k5eAaImAgFnS
české	český	k2eAgMnPc4d1
autory	autor	k1gMnPc7
literatury	literatura	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
Ferdinanda	Ferdinand	k1gMnSc2
Krcha	Krch	k1gMnSc2
<g/>
,	,	kIx,
Ladislava	Ladislav	k1gMnSc2
Havránka	Havránek	k1gMnSc2
a	a	k8xC
Josefa	Josef	k1gMnSc2
Ladu	lad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
manželem	manžel	k1gMnSc7
vytvořila	vytvořit	k5eAaPmAgFnS
mj.	mj.	kA
album	album	k1gNnSc4
řady	řada	k1gFnSc2
Le	Le	k1gMnSc1
Roman	Roman	k1gMnSc1
des	des	k1gNnSc1
bê	bê	k1gMnSc1
(	(	kIx(
<g/>
Román	Román	k1gMnSc1
o	o	k7c6
zvířatech	zvíře	k1gNnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
r.	r.	kA
1934	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
"	"	kIx"
<g/>
Panache	Panache	k1gInSc1
l	l	kA
<g/>
'	'	kIx"
<g/>
écureuil	écureuil	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
bydlela	bydlet	k5eAaImAgFnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
na	na	k7c4
16	#num#	k4
Rue	Rue	k1gFnSc2
de	de	k?
l	l	kA
<g/>
’	’	k?
<g/>
arbalete	arbalést	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
manželem	manžel	k1gMnSc7
měla	mít	k5eAaImAgFnS
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
a	a	k8xC
dvě	dva	k4xCgFnPc4
dcery	dcera	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
François	François	k1gInSc1
Faucher	Fauchra	k1gFnPc2
vybudoval	vybudovat	k5eAaPmAgInS
na	na	k7c4
památku	památka	k1gFnSc4
rodičů	rodič	k1gMnPc2
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
v	v	k7c6
obci	obec	k1gFnSc6
Meuzac	Meuzac	k1gFnSc1
u	u	k7c2
Limoges	Limogesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Spisy	spis	k1gInPc1
</s>
<s>
Šarkán	Šarkán	k2eAgMnSc1d1
<g/>
:	:	kIx,
Psychologické	psychologický	k2eAgInPc4d1
obrázky	obrázek	k1gInPc4
nejmladšího	mladý	k2eAgInSc2d3
z	z	k7c2
Bakulovy	Bakulův	k2eAgFnSc2d1
družiny	družina	k1gFnSc2
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Bakulova	Bakulův	k2eAgFnSc1d1
družina	družina	k1gFnSc1
<g/>
,	,	kIx,
1921	#num#	k4
—	—	k?
přeloženo	přeložen	k2eAgNnSc4d1
do	do	k7c2
dánštiny	dánština	k1gFnSc2
1927	#num#	k4
</s>
<s>
Tuláci	tulák	k1gMnPc1
<g/>
:	:	kIx,
historky	historka	k1gFnPc4
čtyř	čtyři	k4xCgMnPc2
z	z	k7c2
Bakulovy	Bakulův	k2eAgFnSc2d1
družiny	družina	k1gFnSc2
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Bakulův	Bakulův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
1926	#num#	k4
—	—	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1988	#num#	k4
</s>
<s>
Děti	dítě	k1gFnPc1
zhaslých	zhaslý	k2eAgNnPc2d1
očí	oko	k1gNnPc2
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Bakulův	Bakulův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
1929	#num#	k4
—	—	k?
Les	les	k1gInSc1
enfants	enfants	k1gInSc1
aux	aux	k?
yeux	yeux	k1gInSc1
éteints	éteints	k1gInSc1
–	–	k?
traduction	traduction	k1gInSc1
de	de	k?
Zuza	Zuza	k1gFnSc1
Hanouche	Hanouche	k1gFnSc1
<g/>
;	;	kIx,
ill	ill	k?
<g/>
.	.	kIx.
de	de	k?
Sarkan	Sarkan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1931	#num#	k4
—	—	k?
Dzieci	Dziece	k1gFnSc4
o	o	k7c4
wygasłych	wygasłych	k1gInSc4
oczach	oczach	k1gMnSc1
–	–	k?
tłumaczyła	tłumaczyła	k1gMnSc1
Alinalan	Alinalan	k1gMnSc1
<g/>
;	;	kIx,
ilustrował	ilustrował	k?
S.	S.	kA
Sarkan	Sarkan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warszawa	Warszawum	k1gNnPc1
<g/>
:	:	kIx,
Nasza	Nasza	k1gFnSc1
księgarnia	księgarnium	k1gNnSc2
<g/>
,	,	kIx,
1935	#num#	k4
—	—	k?
Les	les	k1gInSc1
enfants	enfants	k1gInSc1
aux	aux	k?
yeux	yeux	k1gInSc1
éteints	éteints	k1gInSc1
–	–	k?
traduit	traduit	k1gInSc1
du	du	k?
tchè	tchè	k1gInSc2
par	para	k1gFnPc2
Milena	Milena	k1gFnSc1
Braud	Braud	k1gInSc1
<g/>
;	;	kIx,
ill	ill	k?
<g/>
.	.	kIx.
de	de	k?
Solvej	Solvej	k1gMnSc1
Crevelier	Crevelier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paříž	Paříž	k1gFnSc1
<g/>
:	:	kIx,
Castor	Castor	k1gInSc1
Poche	Poche	k1gNnSc1
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
</s>
<s>
Zde	zde	k6eAd1
se	se	k3xPyFc4
vraždí	vraždit	k5eAaImIp3nP
<g/>
...	...	k?
Případ	případ	k1gInSc1
Bakulův	Bakulův	k2eAgInSc1d1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sdružení	sdružení	k1gNnSc1
přátel	přítel	k1gMnPc2
<g/>
,	,	kIx,
1934	#num#	k4
</s>
<s>
La	la	k1gNnSc1
Ferme	Ferm	k1gInSc5
du	du	k?
Pè	Pè	k1gInSc5
Castor	Castor	k1gMnSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hélè	Hélè	k1gInSc5
Guertik	Guertik	k1gMnSc1
<g/>
.	.	kIx.
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
—	—	k?
Statek	statek	k1gInSc1
táty	táta	k1gMnSc2
Bobra	bobr	k1gMnSc2
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československá	československý	k2eAgFnSc1d1
grafická	grafický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
ČGU	ČGU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Panache	Panache	k1gFnSc1
l	l	kA
<g/>
'	'	kIx"
<g/>
Écureuil	Écureuil	k1gMnSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fjodor	Fjodor	k1gInSc1
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1934	#num#	k4
—	—	k?
Čtverák	čtverák	k1gMnSc1
<g/>
,	,	kIx,
malý	malý	k2eAgMnSc1d1
veverčák	veverčák	k1gMnSc1
–	–	k?
maloval	malovat	k5eAaImAgMnS
F.	F.	kA
Rojankovski	Rojankovsk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
1939	#num#	k4
</s>
<s>
Le	Le	k?
royaume	royaum	k1gInSc5
des	des	k1gNnSc7
abeilles	abeilles	k1gInSc1
–	–	k?
dessins	dessins	k1gInSc1
de	de	k?
Ruda	ruda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1935	#num#	k4
</s>
<s>
Froux	Froux	k1gInSc1
le	le	k?
Liè	Liè	k1gInSc5
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1935	#num#	k4
—	—	k?
Ušák	ušák	k1gMnSc1
zajíček	zajíček	k1gMnSc1
–	–	k?
Ilustroval	ilustrovat	k5eAaBmAgMnS
F.	F.	kA
Rojankovski	Rojankovsk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
1939	#num#	k4
</s>
<s>
Plouf	Plouf	k1gMnSc1
le	le	k?
Canard	Canard	k1gMnSc1
sauvage	sauvag	k1gFnSc2
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1935	#num#	k4
—	—	k?
Žbluňk	žbluňk	k0
divoká	divoký	k2eAgFnSc1d1
kachna	kachna	k1gFnSc1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
</s>
<s>
Scaf	Scaf	k1gMnSc1
<g/>
,	,	kIx,
le	le	k?
phoque	phoque	k1gInSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1936	#num#	k4
—	—	k?
Skaf	Skaf	k1gMnSc1
Tuleň	tuleň	k1gMnSc1
–	–	k?
ilustroval	ilustrovat	k5eAaBmAgMnS
F.	F.	kA
Rojankovski	Rojankovsk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
1939	#num#	k4
</s>
<s>
Bourru	Bourra	k1gFnSc4
l	l	kA
<g/>
'	'	kIx"
<g/>
Ours	Ours	k1gInSc1
brun	brun	k1gInSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1936	#num#	k4
—	—	k?
Medvěd	medvěd	k1gMnSc1
Chloupek	Chloupek	k1gMnSc1
–	–	k?
obrázky	obrázek	k1gInPc1
kreslil	kreslit	k5eAaImAgInS
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
1941	#num#	k4
</s>
<s>
Quipic	Quipic	k1gMnSc1
le	le	k?
Hérisson	Hérisson	k1gMnSc1
–	–	k?
dessins	dessins	k1gInSc1
de	de	k?
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1937	#num#	k4
—	—	k?
Ježek	Ježek	k1gMnSc1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
Kipik	Kipik	k1gMnSc1
</s>
<s>
Včelí	včelí	k2eAgFnSc1d1
obec	obec	k1gFnSc1
–	–	k?
napsali	napsat	k5eAaPmAgMnP,k5eAaBmAgMnP
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Bakule	bakule	k1gFnSc2
<g/>
;	;	kIx,
maloval	malovat	k5eAaImAgMnS
Rudolf	Rudolf	k1gMnSc1
Jarušek	Jaruška	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
1937	#num#	k4
</s>
<s>
Frou	Frou	k6eAd1
<g/>
,	,	kIx,
the	the	k?
Hare	Hare	k1gInSc1
by	by	kYmCp3nS
Lida	Lido	k1gNnSc2
–	–	k?
litographs	litographs	k6eAd1
by	by	kYmCp3nS
F.	F.	kA
Rojankovski	Rojankovsk	k1gMnPc1
<g/>
;	;	kIx,
translated	translated	k1gMnSc1
from	from	k1gMnSc1
French	French	k1gMnSc1
by	by	kYmCp3nS
Rose	Rose	k1gMnSc1
Fyleman	Fyleman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
George	George	k1gFnSc1
Allen	Allen	k1gMnSc1
&	&	k?
Unwin	Unwin	k1gMnSc1
LTD	ltd	kA
<g/>
,	,	kIx,
1938	#num#	k4
</s>
<s>
Plouf	Plouf	k1gMnSc1
<g/>
,	,	kIx,
canard	canard	k1gMnSc1
sauvage	sauvag	k1gFnSc2
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1938	#num#	k4
—	—	k?
Ploof	Ploof	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
wild	wild	k1gInSc1
Duck	Ducka	k1gFnPc2
by	by	kYmCp3nS
Lida	Lido	k1gNnSc2
–	–	k?
lithographs	lithographs	k6eAd1
by	by	kYmCp3nS
F.	F.	kA
Rojankovski	Rojankovsk	k1gMnPc1
<g/>
;	;	kIx,
translated	translated	k1gMnSc1
from	from	k1gMnSc1
French	French	k1gMnSc1
by	by	kYmCp3nS
Rose	Rose	k1gMnSc1
Fyleman	Fyleman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
George	George	k1gFnSc1
Allen	Allen	k1gMnSc1
&	&	k?
Unwin	Unwin	k1gMnSc1
LTD	ltd	kA
<g/>
,	,	kIx,
1938	#num#	k4
</s>
<s>
Quipic	Quipic	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Hedgehog	Hedgehog	k1gInSc1
by	by	kYmCp3nS
Lida	Lido	k1gNnSc2
–	–	k?
lithographs	lithographs	k6eAd1
by	by	kYmCp3nS
F.	F.	kA
Rojankovski	Rojankovsk	k1gMnPc1
<g/>
;	;	kIx,
translated	translated	k1gMnSc1
[	[	kIx(
<g/>
from	from	k1gMnSc1
French	French	k1gMnSc1
<g/>
]	]	kIx)
by	by	kYmCp3nS
Rose	Rose	k1gMnSc1
Fyleman	Fyleman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
George	George	k1gFnSc1
Allen	Allen	k1gMnSc1
&	&	k?
Unwin	Unwin	k1gMnSc1
LTD	ltd	kA
<g/>
,	,	kIx,
1938	#num#	k4
</s>
<s>
Bourru	Bourr	k1gMnSc3
l	l	kA
<g/>
’	’	k?
<g/>
ours	ours	k1gInSc1
brun	brun	k1gInSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1938	#num#	k4
</s>
<s>
Mischief	Mischief	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Squirrel	Squirrel	k1gInSc1
–	–	k?
litographs	litographs	k1gInSc1
Fiodor	Fiodora	k1gFnPc2
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
;	;	kIx,
translated	translated	k1gMnSc1
[	[	kIx(
<g/>
from	from	k1gMnSc1
French	French	k1gMnSc1
<g/>
]	]	kIx)
by	by	kYmCp3nS
Rose	Rose	k1gMnSc1
Fyleman	Fyleman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
George	George	k1gFnSc1
Allen	Allen	k1gMnSc1
&	&	k?
Unwin	Unwin	k1gMnSc1
LTD	ltd	kA
<g/>
,	,	kIx,
1938	#num#	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Pê	Pê	k1gMnSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1938	#num#	k4
—	—	k?
Rybář	Rybář	k1gMnSc1
Martin	Martin	k1gMnSc1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
</s>
<s>
Pörrö-Pojan	Pörrö-Pojan	k1gInSc1
<g/>
,	,	kIx,
seikkailut	seikkailut	k1gInSc1
–	–	k?
kuvittanut	kuvittanout	k5eAaPmNgInS
F.	F.	kA
Rojankovski	Rojankovsk	k1gInSc3
<g/>
;	;	kIx,
suomentanut	suomentanut	k2eAgInSc1d1
Lauri	Laure	k1gFnSc4
Hirvensalo	Hirvensalo	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helsinki	Helsinki	k1gNnSc1
<g/>
;	;	kIx,
Porvoo	Porvoo	k1gMnSc1
<g/>
:	:	kIx,
Werner	Werner	k1gMnSc1
Söderström	Söderström	k1gMnSc1
Osakeyhtiö	Osakeyhtiö	k1gMnSc1
<g/>
,	,	kIx,
1938	#num#	k4
</s>
<s>
Coucou	Coucou	k6eAd1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1939	#num#	k4
—	—	k?
Kukačka	kukačka	k1gFnSc1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
</s>
<s>
Les	les	k1gInSc1
Animaux	Animaux	k1gInSc1
du	du	k?
zoo	zoo	k1gFnSc2
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Rojankovski	Rojankovsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1941	#num#	k4
—	—	k?
Zvířata	zvíře	k1gNnPc1
ze	z	k7c2
zoo	zoo	k1gFnSc2
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
</s>
<s>
Le	Le	k?
Bouquet	bouquet	k1gNnSc1
du	du	k?
jardinier	jardinier	k1gMnSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angè	Angè	k1gNnSc7
Malclè	Malclè	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1941	#num#	k4
</s>
<s>
Poulerousse	Poulerousse	k1gFnSc1
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romain	Romain	k1gMnSc1
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1949	#num#	k4
</s>
<s>
La	la	k1gNnSc1
grande	grand	k1gMnSc5
nuit	nuit	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
été	été	k?
–	–	k?
ill	ill	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romain	Romain	k1gMnSc1
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Flammarion	Flammarion	k1gInSc1
<g/>
,	,	kIx,
1957	#num#	k4
—	—	k?
Dlouhá	dlouhý	k2eAgFnSc1d1
letní	letní	k2eAgFnSc1d1
noc	noc	k1gFnSc1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČGU	ČGU	kA
<g/>
,	,	kIx,
</s>
<s>
Roman	Roman	k1gMnSc1
des	des	k1gNnSc2
bê	bê	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nS
Panache	Panache	k1gInSc1
l	l	kA
<g/>
’	’	k?
<g/>
écureuil	écureuil	k1gInSc1
<g/>
,	,	kIx,
Froux	Froux	k1gInSc1
le	le	k?
liè	liè	k1gMnSc5
<g/>
,	,	kIx,
Plouf	Plouf	k1gInSc4
canard	canarda	k1gFnPc2
sauvage	sauvag	k1gInSc2
<g/>
,	,	kIx,
Bourru	Bourr	k1gMnSc3
l	l	kA
<g/>
’	’	k?
<g/>
ours	ours	k1gInSc1
brun	brun	k1gInSc1
<g/>
,	,	kIx,
Scaf	Scaf	k1gInSc1
le	le	k?
phoque	phoque	k1gInSc1
<g/>
,	,	kIx,
Quipic	Quipic	k1gMnSc1
le	le	k?
hérisson	hérisson	k1gMnSc1
<g/>
,	,	kIx,
Martin-Pê	Martin-Pê	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Překlady	překlad	k1gInPc4
</s>
<s>
Cestu	cesta	k1gFnSc4
okřídlenému	okřídlený	k2eAgInSc3d1
Erosu	Eros	k1gInSc3
–	–	k?
Alexandra	Alexandra	k1gFnSc1
Michajlovna	Michajlovna	k1gFnSc1
Kollontajova	Kollontajův	k2eAgFnSc1d1
<g/>
;	;	kIx,
z	z	k7c2
ruštiny	ruština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Družstevní	družstevní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
Kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
1925	#num#	k4
</s>
<s>
Zápisky	zápiska	k1gFnPc1
z	z	k7c2
Mrtvého	mrtvý	k2eAgInSc2d1
domu	dům	k1gInSc2
–	–	k?
F.	F.	kA
M.	M.	kA
Dostojevskij	Dostojevskij	k1gFnPc2
<g/>
;	;	kIx,
z	z	k7c2
ruštiny	ruština	k1gFnSc2
<g/>
,	,	kIx,
ilustroval	ilustrovat	k5eAaBmAgMnS
František	František	k1gMnSc1
Kobliha	kobliha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rodinná	rodinný	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
1930	#num#	k4
</s>
<s>
Údolí	údolí	k1gNnSc1
Tba	Tba	k1gFnSc2
<g/>
:	:	kIx,
[	[	kIx(
<g/>
cesta	cesta	k1gFnSc1
do	do	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nebyla	být	k5eNaImAgFnS
ještě	ještě	k6eAd1
objevena	objevit	k5eAaPmNgFnS
<g/>
]	]	kIx)
<g/>
:	:	kIx,
román	román	k1gInSc1
–	–	k?
Vsevolod	Vsevolod	k1gInSc1
Ivanov	Ivanov	k1gInSc1
<g/>
;	;	kIx,
z	z	k7c2
ruštiny	ruština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Družstevní	družstevní	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1931	#num#	k4
</s>
<s>
Vojna	vojna	k1gFnSc1
a	a	k8xC
mír	mír	k1gInSc1
–	–	k?
Lev	Lev	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Tolstoj	Tolstoj	k1gMnSc1
<g/>
;	;	kIx,
z	z	k7c2
ruského	ruský	k2eAgInSc2d1
originálu	originál	k1gInSc2
Vojna	vojna	k1gFnSc1
i	i	k8xC
mir	mir	k1gInSc1
...	...	k?
přeložili	přeložit	k5eAaPmAgMnP
Jan	Jan	k1gMnSc1
Mareš	Mareš	k1gMnSc1
<g/>
,	,	kIx,
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
König	König	k1gMnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
Ilek	Ilek	k1gMnSc1
<g/>
;	;	kIx,
ilustroval	ilustrovat	k5eAaBmAgMnS
Michajl	Michajl	k1gMnSc1
Romberg	Romberg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1949	#num#	k4
</s>
<s>
Gramofonová	gramofonový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
</s>
<s>
V	v	k7c6
lidovém	lidový	k2eAgInSc6d1
tónu	tón	k1gInSc6
–	–	k?
hudba	hudba	k1gFnSc1
Celestina	Celestin	k2eAgNnSc2d1
Rypla	Ryplo	k1gNnSc2
<g/>
;	;	kIx,
slova	slovo	k1gNnSc2
L.	L.	kA
Durdíkové	Durdíková	k1gFnSc2
<g/>
,	,	kIx,
J.	J.	kA
V.	V.	kA
Sládka	Sládek	k1gMnSc2
<g/>
,	,	kIx,
Theodora	Theodor	k1gMnSc2
Storma	Storm	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ultraphon	Ultraphon	k1gNnSc1
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
?	?	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
https://fr.wikipedia.org/wiki/Lida_Durdikova	https://fr.wikipedia.org/wiki/Lida_Durdikův	k2eAgInSc2d1
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
https://fr.wikipedia.org/wiki/Paul_Faucher_(%C3%A9diteur	https://fr.wikipedia.org/wiki/Paul_Faucher_(%C3%A9diteura	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
https://fr.wikipedia.org/wiki/Fran%C3%A7ois_Faucher_(%C3%A9diteur	https://fr.wikipedia.org/wiki/Fran%C3%A7ois_Faucher_(%C3%A9diteura	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kniha	kniha	k1gFnSc1
pokřtěných	pokřtěný	k2eAgMnPc2d1
<g/>
.	.	kIx.
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
<g/>
ahmp	ahmp	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Kulturní	kulturní	k2eAgInSc1d1
adresář	adresář	k1gInSc1
ČSR	ČSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
žijících	žijící	k2eAgMnPc2d1
kulturních	kulturní	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
a	a	k8xC
pracovnic	pracovnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Antonín	Antonín	k1gMnSc1
Dolenský	Dolenský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Josef	Josef	k1gMnSc1
Zeibrdlich	Zeibrdlich	k1gMnSc1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
587	#num#	k4
s.	s.	k?
S.	S.	kA
80	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠUBRTOVÁ	Šubrtová	k1gFnSc1
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
autorka	autorka	k1gFnSc1
spoluzakladatelkou	spoluzakladatelka	k1gFnSc7
moderní	moderní	k2eAgFnSc2d1
ediční	ediční	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
francouzské	francouzský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Durdíková	Durdíková	k1gFnSc1
<g/>
,	,	kIx,
Lída	Lída	k1gFnSc1
<g/>
,	,	kIx,
1899-1955	1899-1955	k4
-	-	kIx~
Portaro	Portara	k1gFnSc5
-	-	kIx~
katalog	katalog	k1gInSc4
knihovny	knihovna	k1gFnSc2
<g/>
.	.	kIx.
provenio	provenio	k1gMnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KRAJÍČKOVÁ	Krajíčková	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geneze	geneze	k1gFnSc1
díla	dílo	k1gNnSc2
Ludmily	Ludmila	k1gFnSc2
Durdíkové-Faucher	Durdíkové-Fauchra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Froux	Froux	k1gInSc1
le	le	k?
liè	liè	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masaryková	Masaryková	k1gFnSc1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Lída	Lída	k1gFnSc1
Durdíková	Durdíkový	k2eAgFnSc1d1
</s>
<s>
Soupis	soupis	k1gInSc1
pražského	pražský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Durdík	Durdík	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1023348	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
123453569	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2138	#num#	k4
3686	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2010026692	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
69419814	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2010026692	#num#	k4
</s>
