<s>
Lída	Lída	k1gFnSc1
Durdíková	Durdíková	k1gFnSc1
<g/>
,	,	kIx,
provdaná	provdaný	k2eAgFnSc1d1
Faucher	Faucher	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
Knize	kniha	k1gFnSc6
pokřtěných	pokřtěný	k2eAgFnPc2d1
Ludmila	Ludmila	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1899	#num#	k4
Praha-Smíchov	Praha-Smíchov	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1955	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
česko-francouzská	česko-francouzský	k2eAgFnSc1d1
spisovatelka	spisovatelka	k1gFnSc1
<g/>
,	,	kIx,
překladatelka	překladatelka	k1gFnSc1
a	a	k8xC
pedagožka	pedagožka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>