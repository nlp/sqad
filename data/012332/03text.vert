<p>
<s>
Samopal	samopal	k1gInSc1	samopal
je	být	k5eAaImIp3nS	být
ruční	ruční	k2eAgFnSc1d1	ruční
samočinná	samočinný	k2eAgFnSc1d1	samočinná
zbraň	zbraň	k1gFnSc1	zbraň
střílející	střílející	k2eAgFnSc2d1	střílející
zpravidla	zpravidla	k6eAd1	zpravidla
dávkou	dávka	k1gFnSc7	dávka
a	a	k8xC	a
používající	používající	k2eAgInPc4d1	používající
pistolové	pistolový	k2eAgInPc4d1	pistolový
náboje	náboj	k1gInPc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
samopaly	samopal	k1gInPc1	samopal
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
největší	veliký	k2eAgInSc1d3	veliký
rozmach	rozmach	k1gInSc1	rozmach
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
v	v	k7c6	v
období	období	k1gNnSc6	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
značnou	značný	k2eAgFnSc7d1	značná
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
použití	použití	k1gNnSc1	použití
pistolového	pistolový	k2eAgInSc2d1	pistolový
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
malý	malý	k2eAgInSc1d1	malý
účinný	účinný	k2eAgInSc1d1	účinný
dostřel	dostřel	k1gInSc1	dostřel
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nízký	nízký	k2eAgInSc1d1	nízký
účinek	účinek	k1gInSc1	účinek
pistolové	pistolový	k2eAgFnSc2d1	pistolová
munice	munice	k1gFnSc2	munice
se	se	k3xPyFc4	se
v	v	k7c6	v
boji	boj	k1gInSc6	boj
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
projevit	projevit	k5eAaPmF	projevit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
kadence	kadence	k1gFnSc1	kadence
a	a	k8xC	a
malé	malý	k2eAgInPc1d1	malý
rozměry	rozměr	k1gInPc1	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Samopal	samopal	k1gInSc4	samopal
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
terminologii	terminologie	k1gFnSc6	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
také	také	k6eAd1	také
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
útočné	útočný	k2eAgFnPc4d1	útočná
pušky	puška	k1gFnPc4	puška
jako	jako	k8xS	jako
např.	např.	kA	např.
Samopal	samopal	k1gInSc1	samopal
vzor	vzor	k1gInSc1	vzor
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
terminologická	terminologický	k2eAgFnSc1d1	terminologická
anomálie	anomálie	k1gFnSc1	anomálie
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
původ	původ	k1gInSc4	původ
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenská	vojenský	k2eAgFnSc1d1	vojenská
odborná	odborný	k2eAgFnSc1d1	odborná
terminologie	terminologie	k1gFnSc1	terminologie
rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
samopaly	samopal	k1gInPc4	samopal
</s>
</p>
<p>
<s>
lehké	lehký	k2eAgFnPc1d1	lehká
–	–	k?	–
určené	určený	k2eAgFnPc1d1	určená
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
pistolovou	pistolový	k2eAgFnSc7d1	pistolová
municí	munice	k1gFnSc7	munice
<g/>
,	,	kIx,	,
a	a	k8xC	a
</s>
</p>
<p>
<s>
těžké	těžký	k2eAgFnPc4d1	těžká
–	–	k?	–
komorované	komorovaný	k2eAgFnPc4d1	komorovaný
pro	pro	k7c4	pro
náboj	náboj	k1gInSc4	náboj
střední	střední	k2eAgFnSc2d1	střední
balistické	balistický	k2eAgFnSc2d1	balistická
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
pušek	puška	k1gFnPc2	puška
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
těžké	těžký	k2eAgInPc1d1	těžký
samopaly	samopal	k1gInPc1	samopal
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
nazývány	nazývat	k5eAaImNgFnP	nazývat
termínem	termín	k1gInSc7	termín
útočná	útočný	k2eAgFnSc1d1	útočná
puška	puška	k1gFnSc1	puška
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
politické	politický	k2eAgFnSc6d1	politická
atmosféře	atmosféra	k1gFnSc6	atmosféra
nevyhovoval	vyhovovat	k5eNaImAgMnS	vyhovovat
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
imperialistický	imperialistický	k2eAgInSc4d1	imperialistický
<g/>
"	"	kIx"	"
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
<g/>
Tradiční	tradiční	k2eAgInSc4d1	tradiční
ruský	ruský	k2eAgInSc4d1	ruský
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
А	А	k?	А
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
automat	automat	k1gInSc1	automat
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
zase	zase	k9	zase
při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
příliš	příliš	k6eAd1	příliš
široký	široký	k2eAgInSc4d1	široký
rozsah	rozsah	k1gInSc4	rozsah
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zůstalo	zůstat	k5eAaPmAgNnS	zůstat
tedy	tedy	k9	tedy
u	u	k7c2	u
"	"	kIx"	"
<g/>
samopalu	samopal	k1gInSc2	samopal
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
dělení	dělení	k1gNnSc1	dělení
těchto	tento	k3xDgFnPc2	tento
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c4	na
lehké	lehký	k2eAgNnSc4d1	lehké
a	a	k8xC	a
těžké	těžký	k2eAgNnSc4d1	těžké
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přestalo	přestat	k5eAaPmAgNnS	přestat
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
vzneseny	vznesen	k2eAgInPc1d1	vznesen
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
palebné	palebný	k2eAgFnSc2d1	palebná
síly	síla	k1gFnSc2	síla
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nepřehledných	přehledný	k2eNgInPc6d1	nepřehledný
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
a	a	k8xC	a
při	při	k7c6	při
výpadech	výpad	k1gInPc6	výpad
malých	malý	k2eAgFnPc2d1	malá
útočných	útočný	k2eAgFnPc2d1	útočná
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
potřeba	potřeba	k1gFnSc1	potřeba
lehké	lehký	k2eAgFnSc2d1	lehká
zbraně	zbraň	k1gFnSc2	zbraň
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
hustotou	hustota	k1gFnSc7	hustota
palby	palba	k1gFnSc2	palba
<g/>
,	,	kIx,	,
obsluhované	obsluhovaný	k2eAgFnSc2d1	obsluhovaná
jedním	jeden	k4xCgMnSc7	jeden
mužem	muž	k1gMnSc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosažení	dosažení	k1gNnSc1	dosažení
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
použití	použití	k1gNnSc2	použití
nově	nově	k6eAd1	nově
zavedených	zavedený	k2eAgFnPc2d1	zavedená
rychlopalných	rychlopalný	k2eAgFnPc2d1	rychlopalná
automatických	automatický	k2eAgFnPc2d1	automatická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
druhem	druh	k1gInSc7	druh
automatické	automatický	k2eAgFnPc1d1	automatická
palné	palný	k2eAgFnPc1d1	palná
zbraně	zbraň	k1gFnPc1	zbraň
byly	být	k5eAaImAgFnP	být
doposud	doposud	k6eAd1	doposud
pouze	pouze	k6eAd1	pouze
kulomety	kulomet	k1gInPc1	kulomet
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
značného	značný	k2eAgNnSc2d1	značné
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
odpadla	odpadnout	k5eAaPmAgFnS	odpadnout
nutnost	nutnost	k1gFnSc1	nutnost
střelby	střelba	k1gFnSc2	střelba
z	z	k7c2	z
pušek	puška	k1gFnPc2	puška
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostřel	dostřel	k1gInSc1	dostřel
standardních	standardní	k2eAgFnPc2d1	standardní
opakovacích	opakovací	k2eAgFnPc2d1	opakovací
pušek	puška	k1gFnPc2	puška
a	a	k8xC	a
výkonnost	výkonnost	k1gFnSc4	výkonnost
jejich	jejich	k3xOp3gInPc2	jejich
nábojů	náboj	k1gInPc2	náboj
jsou	být	k5eAaImIp3nP	být
zbytečně	zbytečně	k6eAd1	zbytečně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Lehké	Lehké	k2eAgInPc1d1	Lehké
kulomety	kulomet	k1gInPc1	kulomet
vyžadovaly	vyžadovat	k5eAaImAgInP	vyžadovat
ale	ale	k9	ale
dva	dva	k4xCgMnPc4	dva
muže	muž	k1gMnPc4	muž
obsluhy	obsluha	k1gFnSc2	obsluha
a	a	k8xC	a
nedalo	dát	k5eNaPmAgNnS	dát
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
efektivně	efektivně	k6eAd1	efektivně
střílet	střílet	k5eAaImF	střílet
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
za	za	k7c2	za
chodu	chod	k1gInSc2	chod
<g/>
.	.	kIx.	.
</s>
<s>
Opakovací	opakovací	k2eAgFnPc1d1	opakovací
pušky	puška	k1gFnPc1	puška
měly	mít	k5eAaImAgFnP	mít
zase	zase	k9	zase
velké	velký	k2eAgInPc4d1	velký
rozměry	rozměr	k1gInPc4	rozměr
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc4d1	nízká
zásobu	zásoba	k1gFnSc4	zásoba
střeliva	střelivo	k1gNnSc2	střelivo
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
nedostatkem	nedostatek	k1gInSc7	nedostatek
opakovaček	opakovačka	k1gFnPc2	opakovačka
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
rychlost	rychlost	k1gFnSc1	rychlost
palby	palba	k1gFnSc2	palba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
odrážení	odrážení	k1gNnSc6	odrážení
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
nové	nový	k2eAgFnSc2d1	nová
a	a	k8xC	a
účinnější	účinný	k2eAgFnSc2d2	účinnější
hlavní	hlavní	k2eAgFnSc2d1	hlavní
zbraně	zbraň	k1gFnSc2	zbraň
pěchoty	pěchota	k1gFnSc2	pěchota
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
sestrojení	sestrojení	k1gNnSc6	sestrojení
samočinné	samočinný	k2eAgFnSc2d1	samočinná
pušky	puška	k1gFnSc2	puška
střílející	střílející	k2eAgFnSc7d1	střílející
dávkou	dávka	k1gFnSc7	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
však	však	k9	však
nikde	nikde	k6eAd1	nikde
neskončily	skončit	k5eNaPmAgFnP	skončit
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
nové	nový	k2eAgInPc1d1	nový
vzory	vzor	k1gInPc1	vzor
samočinných	samočinný	k2eAgFnPc2d1	samočinná
pušek	puška	k1gFnPc2	puška
zaostávaly	zaostávat	k5eAaImAgFnP	zaostávat
za	za	k7c4	za
opakovačkami	opakovačka	k1gFnPc7	opakovačka
v	v	k7c6	v
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
,	,	kIx,	,
kompaktnosti	kompaktnost	k1gFnSc6	kompaktnost
a	a	k8xC	a
zejména	zejména	k9	zejména
ve	v	k7c6	v
spolehlivosti	spolehlivost	k1gFnSc6	spolehlivost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
neosvědčily	osvědčit	k5eNaPmAgFnP	osvědčit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
cestou	cesta	k1gFnSc7	cesta
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
zbraň	zbraň	k1gFnSc4	zbraň
využívající	využívající	k2eAgFnSc4d1	využívající
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
pistolových	pistolový	k2eAgInPc2d1	pistolový
nábojů	náboj	k1gInPc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
zbraň	zbraň	k1gFnSc1	zbraň
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
samopal	samopal	k1gInSc1	samopal
<g/>
.	.	kIx.	.
</s>
<s>
Samopaly	samopal	k1gInPc1	samopal
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
opakovacími	opakovací	k2eAgFnPc7d1	opakovací
puškami	puška	k1gFnPc7	puška
měly	mít	k5eAaImAgFnP	mít
poměrně	poměrně	k6eAd1	poměrně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
rychlost	rychlost	k1gFnSc4	rychlost
palby	palba	k1gFnSc2	palba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
značně	značně	k6eAd1	značně
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
zaostávaly	zaostávat	k5eAaImAgInP	zaostávat
v	v	k7c6	v
dostřelu	dostřel	k1gInSc6	dostřel
<g/>
,	,	kIx,	,
průbojnosti	průbojnost	k1gFnSc6	průbojnost
a	a	k8xC	a
přesnosti	přesnost	k1gFnSc6	přesnost
střelby	střelba	k1gFnSc2	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
nemohly	moct	k5eNaImAgFnP	moct
zcela	zcela	k6eAd1	zcela
nahradit	nahradit	k5eAaPmF	nahradit
pušku	puška	k1gFnSc4	puška
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgInP	sloužit
spíše	spíše	k9	spíše
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
palebné	palebný	k2eAgFnSc2d1	palebná
síly	síla	k1gFnSc2	síla
pěchoty	pěchota	k1gFnSc2	pěchota
při	při	k7c6	při
boji	boj	k1gInSc6	boj
zblízka	zblízka	k6eAd1	zblízka
<g/>
.	.	kIx.	.
</s>
<s>
Samopaly	samopal	k1gInPc4	samopal
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
proto	proto	k8xC	proto
vyzbrojena	vyzbrojit	k5eAaPmNgFnS	vyzbrojit
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
používal	používat	k5eAaImAgInS	používat
opakovací	opakovací	k2eAgFnPc4d1	opakovací
pušky	puška	k1gFnPc4	puška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
zkonstruován	zkonstruovat	k5eAaPmNgInS	zkonstruovat
Italem	Ital	k1gMnSc7	Ital
Abielem	Abiel	k1gMnSc7	Abiel
Revellim	Revellim	k1gInSc4	Revellim
první	první	k4xOgFnSc6	první
prototyp	prototyp	k1gInSc1	prototyp
samopalu	samopal	k1gInSc2	samopal
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Villar-Perosa	Villar-Perosa	k1gFnSc1	Villar-Perosa
M	M	kA	M
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Určen	určen	k2eAgMnSc1d1	určen
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
dva	dva	k4xCgInPc1	dva
kulomety	kulomet	k1gInPc1	kulomet
spojené	spojený	k2eAgInPc1d1	spojený
do	do	k7c2	do
páru	pár	k1gInSc2	pár
a	a	k8xC	a
střílející	střílející	k2eAgInPc1d1	střílející
pistolové	pistolový	k2eAgInPc1d1	pistolový
náboje	náboj	k1gInPc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
schránkové	schránkový	k2eAgInPc1d1	schránkový
zásobníky	zásobník	k1gInPc1	zásobník
na	na	k7c4	na
25	[number]	k4	25
nábojů	náboj	k1gInPc2	náboj
ráže	ráže	k1gFnSc2	ráže
9	[number]	k4	9
mm	mm	kA	mm
byly	být	k5eAaImAgInP	být
nasazovány	nasazovat	k5eAaImNgInP	nasazovat
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
první	první	k4xOgInSc1	první
samopal	samopal	k1gInSc1	samopal
měl	mít	k5eAaImAgInS	mít
řadu	řada	k1gFnSc4	řada
nedostatků	nedostatek	k1gInPc2	nedostatek
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jej	on	k3xPp3gInSc4	on
italská	italský	k2eAgFnSc1d1	italská
armáda	armáda	k1gFnSc1	armáda
zavedla	zavést	k5eAaPmAgFnS	zavést
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
a	a	k8xC	a
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
byl	být	k5eAaImAgInS	být
koncem	koncem	k7c2	koncem
války	válka	k1gFnSc2	válka
kopírován	kopírovat	k5eAaImNgInS	kopírovat
jako	jako	k9	jako
Sturmpistole	Sturmpistola	k1gFnSc3	Sturmpistola
M	M	kA	M
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
zákopových	zákopový	k2eAgInPc6d1	zákopový
bojích	boj	k1gInPc6	boj
a	a	k8xC	a
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
buď	buď	k8xC	buď
podepřený	podepřený	k2eAgInSc1d1	podepřený
dvojnožkou	dvojnožka	k1gFnSc7	dvojnožka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nesený	nesený	k2eAgInSc1d1	nesený
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
postroji	postroj	k1gInSc6	postroj
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
.	.	kIx.	.
</s>
<s>
Nejpodstatnější	podstatný	k2eAgInPc4d3	nejpodstatnější
problémy	problém	k1gInPc4	problém
představovala	představovat	k5eAaImAgFnS	představovat
příliš	příliš	k6eAd1	příliš
velká	velký	k2eAgFnSc1d1	velká
rychlost	rychlost	k1gFnSc1	rychlost
střelby	střelba	k1gFnSc2	střelba
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
ran	rána	k1gFnPc2	rána
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
rychlé	rychlý	k2eAgNnSc4d1	rychlé
přehřívání	přehřívání	k1gNnSc4	přehřívání
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
špatná	špatný	k2eAgFnSc1d1	špatná
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnSc1d1	minimální
stabilita	stabilita	k1gFnSc1	stabilita
a	a	k8xC	a
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
8,14	[number]	k4	8,14
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
objevuje	objevovat	k5eAaImIp3nS	objevovat
samopal	samopal	k1gInSc1	samopal
Bergmann	Bergmann	k1gInSc4	Bergmann
MP	MP	kA	MP
18	[number]	k4	18
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Konstrukčně	konstrukčně	k6eAd1	konstrukčně
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
zdařilý	zdařilý	k2eAgInSc1d1	zdařilý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nebyl	být	k5eNaImAgInS	být
prověřen	prověřit	k5eAaPmNgInS	prověřit
v	v	k7c6	v
bojových	bojový	k2eAgFnPc6d1	bojová
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
tohoto	tento	k3xDgInSc2	tento
samopalu	samopal	k1gInSc2	samopal
a	a	k8xC	a
složení	složení	k1gNnSc2	složení
mechanismů	mechanismus	k1gInPc2	mechanismus
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
bez	bez	k7c2	bez
podstatných	podstatný	k2eAgFnPc2d1	podstatná
změn	změna	k1gFnPc2	změna
pro	pro	k7c4	pro
vzory	vzor	k1gInPc4	vzor
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
se	se	k3xPyFc4	se
rovněž	rovněž	k6eAd1	rovněž
pokusy	pokus	k1gInPc4	pokus
spojit	spojit	k5eAaPmF	spojit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
vzoru	vzor	k1gInSc2	vzor
výhody	výhoda	k1gFnSc2	výhoda
pušky	puška	k1gFnSc2	puška
i	i	k8xC	i
samopalu	samopal	k1gInSc2	samopal
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
voják	voják	k1gMnSc1	voják
vyzbrojený	vyzbrojený	k2eAgMnSc1d1	vyzbrojený
puškou	puška	k1gFnSc7	puška
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
změnit	změnit	k5eAaPmF	změnit
pušku	puška	k1gFnSc4	puška
střílející	střílející	k2eAgFnSc7d1	střílející
puškovými	puškový	k2eAgInPc7d1	puškový
náboji	náboj	k1gInPc7	náboj
na	na	k7c4	na
zbraň	zbraň	k1gFnSc4	zbraň
střílející	střílející	k2eAgInPc4d1	střílející
pistolové	pistolový	k2eAgInPc4d1	pistolový
náboje	náboj	k1gInPc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
podstatné	podstatný	k2eAgInPc4d1	podstatný
nedostatky	nedostatek	k1gInPc4	nedostatek
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
tato	tento	k3xDgFnSc1	tento
idea	idea	k1gFnSc1	idea
nerozšířila	rozšířit	k5eNaPmAgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
docházelo	docházet	k5eAaImAgNnS	docházet
spíše	spíše	k9	spíše
k	k	k7c3	k
útlumu	útlum	k1gInSc3	útlum
v	v	k7c6	v
konstrukcích	konstrukce	k1gFnPc6	konstrukce
nových	nový	k2eAgInPc2d1	nový
samopalů	samopal	k1gInPc2	samopal
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
lokálních	lokální	k2eAgInPc2d1	lokální
konfliktů	konflikt	k1gInPc2	konflikt
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jejich	jejich	k3xOp3gFnPc1	jejich
výhody	výhoda	k1gFnPc1	výhoda
pro	pro	k7c4	pro
moderní	moderní	k2eAgInSc4d1	moderní
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
způsob	způsob	k1gInSc4	způsob
vedení	vedení	k1gNnSc2	vedení
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zkušeností	zkušenost	k1gFnPc2	zkušenost
poučila	poučit	k5eAaPmAgFnS	poučit
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zavedla	zavést	k5eAaPmAgFnS	zavést
samopaly	samopal	k1gInPc4	samopal
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
rozšíření	rozšíření	k1gNnSc3	rozšíření
samopalů	samopal	k1gInPc2	samopal
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zbraně	zbraň	k1gFnPc1	zbraň
zavedené	zavedený	k2eAgFnPc1d1	zavedená
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
byly	být	k5eAaImAgFnP	být
většinou	většina	k1gFnSc7	většina
značně	značně	k6eAd1	značně
výrobně	výrobně	k6eAd1	výrobně
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
vznikaly	vznikat	k5eAaImAgInP	vznikat
nové	nový	k2eAgInPc1d1	nový
<g/>
,	,	kIx,	,
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
vzory	vzor	k1gInPc1	vzor
produkované	produkovaný	k2eAgInPc1d1	produkovaný
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Rudé	rudý	k2eAgFnSc6d1	rudá
armádě	armáda	k1gFnSc6	armáda
byly	být	k5eAaImAgFnP	být
vytvářeny	vytvářen	k2eAgFnPc1d1	vytvářena
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
jednotky	jednotka	k1gFnPc1	jednotka
samopalníků	samopalník	k1gMnPc2	samopalník
vyzbrojené	vyzbrojený	k2eAgFnSc2d1	vyzbrojená
pouze	pouze	k6eAd1	pouze
touto	tento	k3xDgFnSc7	tento
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Samopaly	samopal	k1gInPc1	samopal
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
a	a	k8xC	a
široce	široko	k6eAd1	široko
používaným	používaný	k2eAgInSc7d1	používaný
typem	typ	k1gInSc7	typ
zbraně	zbraň	k1gFnSc2	zbraň
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kompaktnost	kompaktnost	k1gFnSc4	kompaktnost
a	a	k8xC	a
pohotovost	pohotovost	k1gFnSc4	pohotovost
při	při	k7c6	při
boji	boj	k1gInSc6	boj
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
<g/>
,	,	kIx,	,
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
lesních	lesní	k2eAgInPc6d1	lesní
porostech	porost	k1gInPc6	porost
a	a	k8xC	a
pro	pro	k7c4	pro
značně	značně	k6eAd1	značně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
rychlost	rychlost	k1gFnSc4	rychlost
palby	palba	k1gFnSc2	palba
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
měly	mít	k5eAaImAgFnP	mít
snadnou	snadný	k2eAgFnSc4d1	snadná
a	a	k8xC	a
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
obsluhu	obsluha	k1gFnSc4	obsluha
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nepříliš	příliš	k6eNd1	příliš
složité	složitý	k2eAgFnSc6d1	složitá
konstrukci	konstrukce	k1gFnSc6	konstrukce
byla	být	k5eAaImAgFnS	být
levná	levný	k2eAgFnSc1d1	levná
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc1	jejich
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
samopaly	samopal	k1gInPc4	samopal
jako	jako	k8xC	jako
armádní	armádní	k2eAgFnSc4d1	armádní
zbraň	zbraň	k1gFnSc4	zbraň
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
novými	nový	k2eAgFnPc7d1	nová
útočnými	útočný	k2eAgFnPc7d1	útočná
puškami	puška	k1gFnPc7	puška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
sobě	se	k3xPyFc3	se
spojily	spojit	k5eAaPmAgFnP	spojit
výhody	výhoda	k1gFnPc1	výhoda
samopalů	samopal	k1gInPc2	samopal
i	i	k8xC	i
pušek	puška	k1gFnPc2	puška
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
především	především	k9	především
jako	jako	k9	jako
policejní	policejní	k2eAgFnPc4d1	policejní
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
značnému	značný	k2eAgInSc3d1	značný
rozvoji	rozvoj	k1gInSc3	rozvoj
samopalů	samopal	k1gInPc2	samopal
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vzory	vzor	k1gInPc1	vzor
byly	být	k5eAaImAgInP	být
vnějším	vnější	k2eAgInSc7d1	vnější
vzhledem	vzhled	k1gInSc7	vzhled
sice	sice	k8xC	sice
naprosto	naprosto	k6eAd1	naprosto
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mechanicky	mechanicky	k6eAd1	mechanicky
vycházely	vycházet	k5eAaImAgFnP	vycházet
prakticky	prakticky	k6eAd1	prakticky
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
samopalů	samopal	k1gInPc2	samopal
totiž	totiž	k9	totiž
existovaly	existovat	k5eAaImAgInP	existovat
reálné	reálný	k2eAgInPc4d1	reálný
předpoklady	předpoklad	k1gInPc4	předpoklad
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
optimálního	optimální	k2eAgNnSc2d1	optimální
schématu	schéma	k1gNnSc2	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
použití	použití	k1gNnSc1	použití
nábojů	náboj	k1gInPc2	náboj
s	s	k7c7	s
nevelkou	velký	k2eNgFnSc7d1	nevelká
energií	energie	k1gFnSc7	energie
zpětného	zpětný	k2eAgInSc2d1	zpětný
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
maximální	maximální	k2eAgFnSc4d1	maximální
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
konstrukce	konstrukce	k1gFnSc2	konstrukce
zbraňových	zbraňový	k2eAgInPc2d1	zbraňový
mechanizmů	mechanizmus	k1gInPc2	mechanizmus
a	a	k8xC	a
také	také	k9	také
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
návrhy	návrh	k1gInPc7	návrh
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
samočinných	samočinný	k2eAgFnPc2d1	samočinná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
konstrukce	konstrukce	k1gFnSc1	konstrukce
samopalu	samopal	k1gInSc2	samopal
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgInPc4d1	následující
hlavní	hlavní	k2eAgInPc4d1	hlavní
rysy	rys	k1gInPc4	rys
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hlaveň	hlaveň	k1gFnSc1	hlaveň
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
pistolová	pistolový	k2eAgFnSc1d1	pistolová
a	a	k8xC	a
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
pušková	puškový	k2eAgFnSc1d1	pušková
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
pouzdrem	pouzdro	k1gNnSc7	pouzdro
závěru	závěr	k1gInSc2	závěr
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
masívní	masívní	k2eAgInSc1d1	masívní
závěr	závěr	k1gInSc1	závěr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
pružina	pružina	k1gFnSc1	pružina
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
povrch	povrch	k1gInSc1	povrch
hlavně	hlavně	k9	hlavně
bývá	bývat	k5eAaImIp3nS	bývat
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
žebrovitý	žebrovitý	k2eAgMnSc1d1	žebrovitý
(	(	kIx(	(
<g/>
lepší	dobrý	k2eAgNnSc1d2	lepší
chlazení	chlazení	k1gNnSc1	chlazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
hlaveň	hlaveň	k1gFnSc1	hlaveň
uložena	uložen	k2eAgFnSc1d1	uložena
uvnitř	uvnitř	k7c2	uvnitř
ochranného	ochranný	k2eAgInSc2d1	ochranný
pláště	plášť	k1gInSc2	plášť
s	s	k7c7	s
ventilačními	ventilační	k2eAgInPc7d1	ventilační
kruhovými	kruhový	k2eAgInPc7d1	kruhový
nebo	nebo	k8xC	nebo
podélnými	podélný	k2eAgInPc7d1	podélný
otvory	otvor	k1gInPc7	otvor
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
také	také	k9	také
zlevňuje	zlevňovat	k5eAaImIp3nS	zlevňovat
výroba	výroba	k1gFnSc1	výroba
samopalů	samopal	k1gInPc2	samopal
zhotovených	zhotovený	k2eAgInPc2d1	zhotovený
z	z	k7c2	z
plechových	plechový	k2eAgInPc2d1	plechový
výlisků	výlisek	k1gInPc2	výlisek
<g/>
.	.	kIx.	.
<g/>
Spoušťová	spoušťový	k2eAgFnSc1d1	spoušťová
ustrojí	ustrojit	k5eAaPmIp3nS	ustrojit
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
přepínače	přepínač	k1gInPc4	přepínač
umožňující	umožňující	k2eAgNnSc1d1	umožňující
střílet	střílet	k5eAaImF	střílet
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
ranami	rána	k1gFnPc7	rána
nebo	nebo	k8xC	nebo
dávkami	dávka	k1gFnPc7	dávka
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
modely	model	k1gInPc1	model
mají	mít	k5eAaImIp3nP	mít
režim	režim	k1gInSc4	režim
střelby	střelba	k1gFnSc2	střelba
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
rannou	ranný	k2eAgFnSc7d1	ranná
dávkou	dávka	k1gFnSc7	dávka
či	či	k8xC	či
regulátor	regulátor	k1gMnSc1	regulátor
rychlosti	rychlost	k1gFnSc2	rychlost
střelby	střelba	k1gFnSc2	střelba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zásobování	zásobování	k1gNnSc2	zásobování
náboji	náboj	k1gInSc6	náboj
je	být	k5eAaImIp3nS	být
zprostředkováno	zprostředkovat	k5eAaPmNgNnS	zprostředkovat
zakřivenými	zakřivený	k2eAgInPc7d1	zakřivený
<g/>
,	,	kIx,	,
schránkovými	schránkův	k2eAgInPc7d1	schránkův
nebo	nebo	k8xC	nebo
diskovými	diskový	k2eAgInPc7d1	diskový
zásobníky	zásobník	k1gInPc7	zásobník
<g/>
,	,	kIx,	,
umístěnými	umístěný	k2eAgInPc7d1	umístěný
zdola	zdola	k6eAd1	zdola
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
samopalů	samopal	k1gInPc2	samopal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
z	z	k7c2	z
boku	bok	k1gInSc2	bok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
samopal	samopal	k1gInSc1	samopal
Sten	sten	k1gInSc1	sten
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
výjimečně	výjimečně	k6eAd1	výjimečně
shora	shora	k6eAd1	shora
(	(	kIx(	(
<g/>
samopal	samopal	k1gInSc1	samopal
Owen	Owen	k1gNnSc1	Owen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Hledí	hledí	k1gNnPc1	hledí
jsou	být	k5eAaImIp3nP	být
různá	různý	k2eAgNnPc1d1	různé
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pevná	pevný	k2eAgFnSc1d1	pevná
nebo	nebo	k8xC	nebo
stavitelná	stavitelný	k2eAgFnSc1d1	stavitelná
puškového	puškový	k2eAgInSc2d1	puškový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
překlápěcí	překlápěcí	k2eAgFnPc1d1	překlápěcí
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc1	dva
různě	různě	k6eAd1	různě
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
,	,	kIx,	,
na	na	k7c4	na
různou	různý	k2eAgFnSc4d1	různá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
určené	určený	k2eAgInPc4d1	určený
plátky	plátek	k1gInPc4	plátek
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
nebo	nebo	k8xC	nebo
zářezy	zářez	k1gInPc7	zářez
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
různé	různý	k2eAgInPc1d1	různý
kompenzátory	kompenzátor	k1gInPc1	kompenzátor
zpětného	zpětný	k2eAgInSc2d1	zpětný
rázu	ráz	k1gInSc2	ráz
a	a	k8xC	a
úsťové	úsťový	k2eAgFnPc1d1	úsťová
brzdy	brzda	k1gFnPc1	brzda
zmenšující	zmenšující	k2eAgFnPc1d1	zmenšující
pohyb	pohyb	k1gInSc4	pohyb
zbraně	zbraň	k1gFnSc2	zbraň
při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zvyšující	zvyšující	k2eAgFnSc1d1	zvyšující
její	její	k3xOp3gFnSc4	její
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
.	.	kIx.	.
<g/>
Pažby	pažba	k1gFnSc2	pažba
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
rozměrů	rozměr	k1gInPc2	rozměr
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
sklopnými	sklopný	k2eAgFnPc7d1	sklopná
či	či	k8xC	či
výsuvnými	výsuvný	k2eAgFnPc7d1	výsuvná
ramenními	ramenní	k2eAgFnPc7d1	ramenní
opěrkami	opěrka	k1gFnPc7	opěrka
<g/>
.	.	kIx.	.
<g/>
Princip	princip	k1gInSc1	princip
činnosti	činnost	k1gFnSc2	činnost
mechanizmu	mechanizmus	k1gInSc2	mechanizmus
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
samopalů	samopal	k1gInPc2	samopal
shodný	shodný	k2eAgInSc1d1	shodný
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
zpětného	zpětný	k2eAgInSc2d1	zpětný
rázu	ráz	k1gInSc2	ráz
neuzamčeného	uzamčený	k2eNgInSc2d1	neuzamčený
závěru	závěr	k1gInSc2	závěr
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
vzory	vzor	k1gInPc1	vzor
mají	mít	k5eAaImIp3nP	mít
uzamčený	uzamčený	k2eAgInSc4d1	uzamčený
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
naprosté	naprostý	k2eAgFnSc2d1	naprostá
většiny	většina	k1gFnSc2	většina
samopalů	samopal	k1gInPc2	samopal
probíhá	probíhat	k5eAaImIp3nS	probíhat
střelba	střelba	k1gFnSc1	střelba
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zbraň	zbraň	k1gFnSc1	zbraň
připravená	připravený	k2eAgFnSc1d1	připravená
k	k	k7c3	k
palbě	palba	k1gFnSc3	palba
nemá	mít	k5eNaImIp3nS	mít
náboj	náboj	k1gInSc1	náboj
v	v	k7c6	v
nábojové	nábojový	k2eAgFnSc6d1	nábojová
komoře	komora	k1gFnSc6	komora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zásobníku	zásobník	k1gInSc6	zásobník
<g/>
,	,	kIx,	,
hlaveň	hlaveň	k1gFnSc1	hlaveň
je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
a	a	k8xC	a
závěr	závěr	k1gInSc4	závěr
otevřený	otevřený	k2eAgInSc4d1	otevřený
<g/>
.	.	kIx.	.
</s>
<s>
Masívní	masívní	k2eAgFnSc1d1	masívní
součástka	součástka	k1gFnSc1	součástka
tvořící	tvořící	k2eAgFnSc1d1	tvořící
zároveň	zároveň	k6eAd1	zároveň
závěr	závěr	k1gInSc4	závěr
i	i	k8xC	i
úderník	úderník	k1gInSc4	úderník
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
spouště	spoušť	k1gFnSc2	spoušť
hnaná	hnaný	k2eAgFnSc1d1	hnaná
vpřed	vpřed	k6eAd1	vpřed
pružinou	pružina	k1gFnSc7	pružina
vsune	vsunout	k5eAaPmIp3nS	vsunout
náboj	náboj	k1gInSc1	náboj
ze	z	k7c2	z
zásobníku	zásobník	k1gInSc2	zásobník
do	do	k7c2	do
nábojové	nábojový	k2eAgFnSc2d1	nábojová
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
hlaveň	hlaveň	k1gFnSc4	hlaveň
a	a	k8xC	a
úderem	úder	k1gInSc7	úder
do	do	k7c2	do
zápalky	zápalka	k1gFnSc2	zápalka
náboje	náboj	k1gInSc2	náboj
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
výstřel	výstřel	k1gInSc1	výstřel
<g/>
.	.	kIx.	.
<g/>
Přesnost	přesnost	k1gFnSc1	přesnost
při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
ranami	rána	k1gFnPc7	rána
negativně	negativně	k6eAd1	negativně
značně	značně	k6eAd1	značně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
prudký	prudký	k2eAgInSc1d1	prudký
pohyb	pohyb	k1gInSc1	pohyb
masivního	masivní	k2eAgInSc2d1	masivní
závěru	závěr	k1gInSc2	závěr
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
odchýlení	odchýlení	k1gNnSc4	odchýlení
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
zbraně	zbraň	k1gFnSc2	zbraň
po	po	k7c4	po
vypuštění	vypuštění	k1gNnSc4	vypuštění
závěru	závěr	k1gInSc2	závěr
ze	z	k7c2	z
zadní	zadní	k2eAgFnSc2d1	zadní
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ještě	ještě	k9	ještě
před	před	k7c7	před
výstřelem	výstřel	k1gInSc7	výstřel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavné	slavný	k2eAgInPc4d1	slavný
samopaly	samopal	k1gInPc4	samopal
==	==	k?	==
</s>
</p>
<p>
<s>
Hafdasa	Hafdasa	k1gFnSc1	Hafdasa
C-4	C-4	k1gFnSc2	C-4
</s>
</p>
<p>
<s>
Halcon	Halcon	k1gMnSc1	Halcon
M-1943	M-1943	k1gMnSc1	M-1943
</s>
</p>
<p>
<s>
MP40	MP40	k4	MP40
</s>
</p>
<p>
<s>
Samopal	samopal	k1gInSc1	samopal
PPŠ-41	PPŠ-41	k1gMnSc1	PPŠ-41
Špagin	Špagin	k1gMnSc1	Špagin
</s>
</p>
<p>
<s>
Sten	sten	k1gInSc1	sten
</s>
</p>
<p>
<s>
Samopal	samopal	k1gInSc1	samopal
vzor	vzor	k1gInSc1	vzor
61	[number]	k4	61
(	(	kIx(	(
<g/>
Škorpion	škorpion	k1gMnSc1	škorpion
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thompson	Thompson	k1gMnSc1	Thompson
</s>
</p>
<p>
<s>
Uzi	Uzi	k?	Uzi
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ŽUK	ŽUK	kA	ŽUK
<g/>
,	,	kIx,	,
A.	A.	kA	A.
B.	B.	kA	B.
Pušky	puška	k1gFnSc2	puška
a	a	k8xC	a
samopaly	samopal	k1gInPc4	samopal
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
Vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
150	[number]	k4	150
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
116	[number]	k4	116
-	-	kIx~	-
119	[number]	k4	119
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
samopal	samopal	k1gInSc1	samopal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
samopal	samopal	k1gInSc1	samopal
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
