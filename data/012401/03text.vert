<p>
<s>
Sýček	sýček	k1gMnSc1	sýček
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Athene	Athen	k1gInSc5	Athen
noctua	noctuum	k1gNnPc1	noctuum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
sýc	sýc	k1gMnSc1	sýc
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
sova	sova	k1gFnSc1	sova
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
puštíkovití	puštíkovitý	k2eAgMnPc1d1	puštíkovitý
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
kromě	kromě	k7c2	kromě
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
až	až	k9	až
po	po	k7c4	po
Koreu	Korea	k1gFnSc4	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
uměle	uměle	k6eAd1	uměle
vysazena	vysadit	k5eAaPmNgFnS	vysadit
i	i	k9	i
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
také	také	k6eAd1	také
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
23	[number]	k4	23
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
56	[number]	k4	56
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
gSýček	gSýček	k1gInSc1	gSýček
obecný	obecný	k2eAgInSc1d1	obecný
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
sotva	sotva	k6eAd1	sotva
velikosti	velikost	k1gFnSc3	velikost
holuba	holub	k1gMnSc2	holub
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
robustní	robustní	k2eAgNnSc1d1	robustní
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
širokou	široký	k2eAgFnSc4d1	široká
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc4d1	nízká
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
silné	silný	k2eAgFnPc4d1	silná
končetiny	končetina	k1gFnPc4	končetina
s	s	k7c7	s
ostrými	ostrý	k2eAgInPc7d1	ostrý
a	a	k8xC	a
silnými	silný	k2eAgInPc7d1	silný
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
má	mít	k5eAaImIp3nS	mít
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgNnSc1d1	hnědé
se	s	k7c7	s
světlejším	světlý	k2eAgNnSc7d2	světlejší
skvrněním	skvrnění	k1gNnSc7	skvrnění
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
hnědě	hnědě	k6eAd1	hnědě
pruhované	pruhovaný	k2eAgNnSc1d1	pruhované
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
ocasu	ocas	k1gInSc2	ocas
a	a	k8xC	a
končetiny	končetina	k1gFnPc4	končetina
čistě	čistě	k6eAd1	čistě
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgInPc1d1	výrazný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
velké	velká	k1gFnPc1	velká
žluté	žlutý	k2eAgFnPc1d1	žlutá
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
bílý	bílý	k2eAgInSc4d1	bílý
pruh	pruh	k1gInSc4	pruh
nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
sýčkovi	sýček	k1gMnSc3	sýček
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
vážný	vážný	k2eAgInSc1d1	vážný
výraz	výraz	k1gInSc1	výraz
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	s	k7c7	s
zbarvením	zbarvení	k1gNnSc7	zbarvení
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
jednotvárnější	jednotvárný	k2eAgMnPc1d2	jednotvárnější
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
viditelný	viditelný	k2eAgInSc1d1	viditelný
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Sýčka	sýček	k1gMnSc4	sýček
celoročně	celoročně	k6eAd1	celoročně
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
především	především	k9	především
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
krajinách	krajina	k1gFnPc6	krajina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
nebo	nebo	k8xC	nebo
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
listnatých	listnatý	k2eAgInPc2d1	listnatý
lesů	les	k1gInPc2	les
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
docela	docela	k6eAd1	docela
hojně	hojně	k6eAd1	hojně
i	i	k9	i
na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
a	a	k8xC	a
ve	v	k7c6	v
městech	město	k1gNnPc6	město
s	s	k7c7	s
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
stromovým	stromový	k2eAgInSc7d1	stromový
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
zejména	zejména	k9	zejména
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
krajině	krajina	k1gFnSc6	krajina
často	často	k6eAd1	často
již	již	k6eAd1	již
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
denní	denní	k2eAgFnSc4d1	denní
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
dne	den	k1gInSc2	den
tráví	trávit	k5eAaImIp3nS	trávit
jako	jako	k9	jako
přikrčený	přikrčený	k2eAgInSc1d1	přikrčený
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
vyvýšených	vyvýšený	k2eAgFnPc6d1	vyvýšená
pozorovatelnách	pozorovatelna	k1gFnPc6	pozorovatelna
a	a	k8xC	a
bedlivě	bedlivě	k6eAd1	bedlivě
sleduje	sledovat	k5eAaImIp3nS	sledovat
svým	svůj	k3xOyFgInSc7	svůj
káravým	káravý	k2eAgInSc7d1	káravý
pohledem	pohled	k1gInSc7	pohled
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
výrazným	výrazný	k2eAgMnSc7d1	výrazný
"	"	kIx"	"
<g/>
uhuí	uhuí	k6eAd1	uhuí
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
kliuú	kliuú	k?	kliuú
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
při	při	k7c6	při
toku	tok	k1gInSc6	tok
ozývá	ozývat	k5eAaImIp3nS	ozývat
silným	silný	k2eAgMnSc7d1	silný
"	"	kIx"	"
<g/>
guuk	guuk	k6eAd1	guuk
<g/>
"	"	kIx"	"
a	a	k8xC	a
jako	jako	k9	jako
varovný	varovný	k2eAgInSc4d1	varovný
signál	signál	k1gInSc4	signál
používá	používat	k5eAaImIp3nS	používat
svižné	svižný	k2eAgNnSc1d1	svižné
"	"	kIx"	"
<g/>
kju	kju	k?	kju
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
drobnými	drobný	k2eAgMnPc7d1	drobný
hlodavci	hlodavec	k1gMnPc7	hlodavec
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
hraboši	hraboš	k1gMnPc1	hraboš
polními	polní	k2eAgMnPc7d1	polní
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgMnPc7d1	jiný
menšími	malý	k2eAgMnPc7d2	menší
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
ještěrkami	ještěrka	k1gFnPc7	ještěrka
<g/>
,	,	kIx,	,
žábami	žába	k1gFnPc7	žába
nebo	nebo	k8xC	nebo
hady	had	k1gMnPc4	had
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc1	mládě
hrají	hrát	k5eAaImIp3nP	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
potravy	potrava	k1gFnSc2	potrava
především	především	k9	především
žížaly	žížala	k1gFnPc1	žížala
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spatření	spatření	k1gNnSc6	spatření
kořisti	kořist	k1gFnSc2	kořist
ji	on	k3xPp3gFnSc4	on
sýček	sýček	k1gMnSc1	sýček
překvapuje	překvapovat	k5eAaImIp3nS	překvapovat
rychlým	rychlý	k2eAgInSc7d1	rychlý
výpadem	výpad	k1gInSc7	výpad
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pozorovatelny	pozorovatelna	k1gFnSc2	pozorovatelna
a	a	k8xC	a
následně	následně	k6eAd1	následně
ji	on	k3xPp3gFnSc4	on
uchopuje	uchopovat	k5eAaImIp3nS	uchopovat
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
silných	silný	k2eAgInPc2d1	silný
drápů	dráp	k1gInPc2	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
kořist	kořist	k1gFnSc1	kořist
požírá	požírat	k5eAaImIp3nS	požírat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
si	se	k3xPyFc3	se
většinou	většina	k1gFnSc7	většina
vynáší	vynášet	k5eAaImIp3nP	vynášet
na	na	k7c4	na
vyvýšené	vyvýšený	k2eAgNnSc4d1	vyvýšené
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
zahnizďuje	zahnizďovat	k5eAaImIp3nS	zahnizďovat
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
většinou	většina	k1gFnSc7	většina
starých	starý	k2eAgInPc2d1	starý
a	a	k8xC	a
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
stodolách	stodola	k1gFnPc6	stodola
<g/>
,	,	kIx,	,
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
v	v	k7c6	v
opuštěných	opuštěný	k2eAgFnPc6d1	opuštěná
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
i	i	k9	i
v	v	k7c6	v
ptačích	ptačí	k2eAgFnPc6d1	ptačí
budkách	budka	k1gFnPc6	budka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnSc1	samice
mívá	mívat	k5eAaImIp3nS	mívat
ročně	ročně	k6eAd1	ročně
jednu	jeden	k4xCgFnSc4	jeden
snůšku	snůška	k1gFnSc4	snůška
obsahující	obsahující	k2eAgFnSc4d1	obsahující
4	[number]	k4	4
až	až	k8xS	až
5	[number]	k4	5
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
33,5	[number]	k4	33,5
×	×	k?	×
28,5	[number]	k4	28,5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
sedí	sedit	k5eAaImIp3nS	sedit
28	[number]	k4	28
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
sov	sova	k1gFnPc2	sova
začíná	začínat	k5eAaImIp3nS	začínat
samice	samice	k1gFnSc1	samice
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedět	sedět	k5eAaImF	sedět
až	až	k9	až
po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
posledního	poslední	k2eAgNnSc2d1	poslední
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
mláďaty	mládě	k1gNnPc7	mládě
nejsou	být	k5eNaImIp3nP	být
značné	značný	k2eAgInPc4d1	značný
věkové	věkový	k2eAgInPc4d1	věkový
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
začínají	začínat	k5eAaImIp3nP	začínat
již	již	k6eAd1	již
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
létat	létat	k5eAaImF	létat
a	a	k8xC	a
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
života	život	k1gInSc2	život
opouštějí	opouštět	k5eAaImIp3nP	opouštět
teritorium	teritorium	k1gNnSc4	teritorium
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
plně	plně	k6eAd1	plně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mytologie	mytologie	k1gFnPc4	mytologie
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc4d1	kulturní
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
lidem	člověk	k1gMnPc3	člověk
představoval	představovat	k5eAaImAgMnS	představovat
jako	jako	k9	jako
posvátný	posvátný	k2eAgMnSc1d1	posvátný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
bohyni	bohyně	k1gFnSc4	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
a	a	k8xC	a
boje	boj	k1gInSc2	boj
Pallas	Pallas	k1gMnSc1	Pallas
Athénu	Athéna	k1gFnSc4	Athéna
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
také	také	k9	také
jeho	jeho	k3xOp3gNnSc4	jeho
rodové	rodový	k2eAgNnSc4d1	rodové
latinské	latinský	k2eAgNnSc4d1	latinské
jméno	jméno	k1gNnSc4	jméno
Athene	Athen	k1gInSc5	Athen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
sýček	sýček	k1gMnSc1	sýček
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
již	již	k6eAd1	již
poslem	posel	k1gMnSc7	posel
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
špatných	špatný	k2eAgFnPc2d1	špatná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vzácněji	vzácně	k6eAd2	vzácně
používá	používat	k5eAaImIp3nS	používat
odrazující	odrazující	k2eAgInSc1d1	odrazující
termín	termín	k1gInSc1	termín
nesýčkuj	sýčkovat	k5eNaImRp2nS	sýčkovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
využívali	využívat	k5eAaPmAgMnP	využívat
čižbáři	čižbář	k1gMnPc1	čižbář
averze	averze	k1gFnSc2	averze
drobných	drobný	k2eAgMnPc2d1	drobný
ptáků	pták	k1gMnPc2	pták
k	k	k7c3	k
sovám	sova	k1gFnPc3	sova
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
přirozenému	přirozený	k2eAgMnSc3d1	přirozený
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zcela	zcela	k6eAd1	zcela
běžný	běžný	k2eAgMnSc1d1	běžný
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
(	(	kIx(	(
<g/>
=	=	kIx~	=
nenáročný	náročný	k2eNgMnSc1d1	nenáročný
<g/>
)	)	kIx)	)
sýček	sýček	k1gMnSc1	sýček
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
ptáčníky	ptáčník	k1gMnPc4	ptáčník
ideální	ideální	k2eAgNnSc1d1	ideální
"	"	kIx"	"
<g/>
návnada	návnada	k1gFnSc1	návnada
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
takový	takový	k3xDgInSc4	takový
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
přítomností	přítomnost	k1gFnSc7	přítomnost
působil	působit	k5eAaImAgInS	působit
vystavený	vystavený	k2eAgMnSc1d1	vystavený
sýček	sýček	k1gMnSc1	sýček
zděšení	zděšení	k1gNnSc2	zděšení
a	a	k8xC	a
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
reakci	reakce	k1gFnSc4	reakce
–	–	k?	–
útok	útok	k1gInSc1	útok
<g/>
.	.	kIx.	.
</s>
<s>
Dorážející	dorážející	k2eAgMnPc1d1	dorážející
ptáci	pták	k1gMnPc1	pták
upadali	upadat	k5eAaPmAgMnP	upadat
snadno	snadno	k6eAd1	snadno
na	na	k7c6	na
čihadle	čihadlo	k1gNnSc6	čihadlo
do	do	k7c2	do
nastražených	nastražený	k2eAgFnPc2d1	nastražená
léček	léčka	k1gFnPc2	léčka
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
sedali	sedat	k5eAaImAgMnP	sedat
na	na	k7c4	na
lep	lep	k1gInSc4	lep
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
chytali	chytat	k5eAaImAgMnP	chytat
do	do	k7c2	do
nastražených	nastražený	k2eAgFnPc2d1	nastražená
ok	oka	k1gFnPc2	oka
a	a	k8xC	a
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
početnost	početnost	k1gFnSc1	početnost
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgMnS	být
sýček	sýček	k1gMnSc1	sýček
obecný	obecný	k2eAgMnSc1d1	obecný
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
velice	velice	k6eAd1	velice
hojnou	hojný	k2eAgFnSc7d1	hojná
sovou	sova	k1gFnSc7	sova
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
však	však	k9	však
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
především	především	k6eAd1	především
masivní	masivní	k2eAgFnPc4d1	masivní
ztráty	ztráta	k1gFnPc4	ztráta
jeho	on	k3xPp3gInSc2	on
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biotopu	biotop	k1gInSc2	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
pokles	pokles	k1gInSc1	pokles
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
odhadnut	odhadnout	k5eAaPmNgInS	odhadnout
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
250	[number]	k4	250
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
stavy	stav	k1gInPc7	stav
dále	daleko	k6eAd2	daleko
klesaly	klesat	k5eAaImAgInP	klesat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stav	stav	k1gInSc1	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
aktuálním	aktuální	k2eAgInSc6d1	aktuální
stavu	stav	k1gInSc6	stav
druhu	druh	k1gInSc2	druh
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Martin	Martin	k1gMnSc1	Martin
Šálek	Šálek	k1gMnSc1	Šálek
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
vymření	vymření	k1gNnSc1	vymření
sýčka	sýček	k1gMnSc2	sýček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
již	již	k6eAd1	již
jen	jen	k9	jen
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
uvedl	uvést	k5eAaPmAgMnS	uvést
Petr	Petr	k1gMnSc1	Petr
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
novojičínské	novojičínský	k2eAgFnSc2d1	novojičínská
organizace	organizace	k1gFnSc2	organizace
ČSOP	ČSOP	kA	ČSOP
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
ročním	roční	k2eAgNnSc6d1	roční
regionálním	regionální	k2eAgNnSc6d1	regionální
mapování	mapování	k1gNnSc6	mapování
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
obava	obava	k1gFnSc1	obava
ze	z	k7c2	z
špatného	špatný	k2eAgInSc2d1	špatný
stavu	stav	k1gInSc2	stav
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
ohrožení	ohrožení	k1gNnSc3	ohrožení
sýčka	sýček	k1gMnSc4	sýček
vyhynutím	vyhynutí	k1gNnSc7	vyhynutí
ředitel	ředitel	k1gMnSc1	ředitel
České	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
ornitologické	ornitologický	k2eAgFnSc2d1	ornitologická
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vermouzek	Vermouzek	k1gInSc1	Vermouzek
a	a	k8xC	a
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
důvod	důvod	k1gInSc4	důvod
uvedl	uvést	k5eAaPmAgMnS	uvést
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
užití	užití	k1gNnSc4	užití
hnojiv	hnojivo	k1gNnPc2	hnojivo
a	a	k8xC	a
pesticidů	pesticid	k1gInPc2	pesticid
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sýček	sýček	k1gMnSc1	sýček
obecný	obecný	k2eAgMnSc1d1	obecný
Ptákem	pták	k1gMnSc7	pták
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nejohroženější	ohrožený	k2eAgFnSc1d3	nejohroženější
sova	sova	k1gFnSc1	sova
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
se	s	k7c7	s
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
zbývajícími	zbývající	k2eAgInPc7d1	zbývající
páry	pár	k1gInPc7	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
sýčka	sýček	k1gMnSc2	sýček
obecného	obecný	k2eAgMnSc2d1	obecný
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
7	[number]	k4	7
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A.	A.	kA	A.
noctua	noctua	k1gFnSc1	noctua
noctua	noctu	k1gInSc2	noctu
–	–	k?	–
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
noctua	noctua	k1gMnSc1	noctua
sarda	sarda	k1gMnSc1	sarda
–	–	k?	–
Sardinie	Sardinie	k1gFnSc1	Sardinie
<g/>
,	,	kIx,	,
Korsika	Korsika	k1gFnSc1	Korsika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
noctua	noctua	k6eAd1	noctua
vidalii	vidalie	k1gFnSc4	vidalie
–	–	k?	–
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
vysazen	vysazen	k2eAgInSc1d1	vysazen
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
noctua	noctua	k1gFnSc1	noctua
indigena	indigena	k1gFnSc1	indigena
–	–	k?	–
jižní	jižní	k2eAgFnSc1d1	jižní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc1d1	střední
Rusko	Rusko	k1gNnSc1	Rusko
až	až	k9	až
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
noctua	noctua	k1gFnSc1	noctua
desertae	deserta	k1gInSc2	deserta
–	–	k?	–
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
noctua	noctua	k1gFnSc1	noctua
lilith	lilith	k1gInSc1	lilith
–	–	k?	–
Blízký	blízký	k2eAgInSc1d1	blízký
východ	východ	k1gInSc1	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
noctua	noctua	k1gFnSc1	noctua
bactriana	bactriana	k1gFnSc1	bactriana
–	–	k?	–
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Little	Little	k1gFnSc2	Little
Owl	Owl	k1gFnSc2	Owl
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Pójdźka	Pójdźka	k1gFnSc1	Pójdźka
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BEZZEL	BEZZEL	kA	BEZZEL
<g/>
,	,	kIx,	,
Einhard	Einhard	k1gInSc1	Einhard
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Čestlice	Čestlice	k1gFnSc1	Čestlice
<g/>
:	:	kIx,	:
Rebo	Rebo	k1gNnSc1	Rebo
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7234	[number]	k4	7234
<g/>
-	-	kIx~	-
<g/>
292	[number]	k4	292
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sýček	sýček	k1gMnSc1	sýček
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
sýček	sýček	k1gMnSc1	sýček
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Athene	Athen	k1gInSc5	Athen
noctua	noctu	k2eAgNnPc1d1	noctu
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sýček	sýček	k1gMnSc1	sýček
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Sýček	sýček	k1gMnSc1	sýček
obecný	obecný	k2eAgMnSc1d1	obecný
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
pražské	pražský	k2eAgFnSc2d1	Pražská
zoo	zoo	k1gFnSc2	zoo
</s>
</p>
<p>
<s>
Sýček	sýček	k1gMnSc1	sýček
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Hlas	hlas	k1gInSc1	hlas
sýčka	sýček	k1gMnSc2	sýček
obecného	obecný	k2eAgMnSc2d1	obecný
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
Rádia	rádio	k1gNnSc2	rádio
Junior	junior	k1gMnSc1	junior
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Rádio	rádio	k1gNnSc1	rádio
Junior	junior	k1gMnSc1	junior
<g/>
,	,	kIx,	,
Host	host	k1gMnSc1	host
Martin	Martin	k1gMnSc1	Martin
Šálek	Šálek	k1gMnSc1	Šálek
<g/>
.	.	kIx.	.
</s>
<s>
Sýček	sýček	k1gMnSc1	sýček
<g/>
,	,	kIx,	,
syčet	syčet	k5eAaImF	syčet
<g/>
,	,	kIx,	,
sypat	sypat	k5eAaImF	sypat
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgFnPc2d3	nejmenší
sov	sova	k1gFnPc2	sova
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
...	...	k?	...
</s>
</p>
<p>
<s>
Planetárium	planetárium	k1gNnSc1	planetárium
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
9.04	[number]	k4	9.04
<g/>
.2016	.2016	k4	.2016
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Sever	sever	k1gInSc1	sever
<g/>
,	,	kIx,	,
Za	za	k7c4	za
sýčky	sýček	k1gMnPc4	sýček
v	v	k7c6	v
Radešíně	Radešína	k1gFnSc6	Radešína
na	na	k7c6	na
Litoměřicku	Litoměřicko	k1gNnSc6	Litoměřicko
<g/>
:	:	kIx,	:
biolog	biolog	k1gMnSc1	biolog
Martin	Martin	k1gMnSc1	Martin
Šálek	Šálek	k1gMnSc1	Šálek
</s>
</p>
