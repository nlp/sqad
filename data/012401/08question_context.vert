<s>
Sýček	sýček	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Athene	Athen	k1gInSc5
noctua	noctuum	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
známý	známý	k2eAgInSc1d1
také	také	k9
jako	jako	k9
sýc	sýc	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
malá	malý	k2eAgFnSc1d1
sova	sova	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
puštíkovití	puštíkovitý	k2eAgMnPc1d1
rozšířená	rozšířený	k2eAgFnSc1d1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
kromě	kromě	k7c2
Skandinávského	skandinávský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
a	a	k8xC
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
až	až	k9
po	po	k7c4
Koreu	Korea	k1gFnSc4
<g/>
.	.	kIx.
</s>