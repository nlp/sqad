<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
astronomický	astronomický	k2eAgInSc4d1	astronomický
objekt	objekt	k1gInSc4	objekt
vznikající	vznikající	k2eAgFnSc4d1	vznikající
zhroucením	zhroucení	k1gNnSc7	zhroucení
hvězdy	hvězda	k1gFnSc2	hvězda
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
nebo	nebo	k8xC	nebo
podprůměrné	podprůměrný	k2eAgFnSc6d1	podprůměrná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
hmotné	hmotný	k2eAgFnPc1d1	hmotná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jádře	jádro	k1gNnSc6	jádro
teplot	teplota	k1gFnPc2	teplota
potřebných	potřebný	k2eAgFnPc2d1	potřebná
k	k	k7c3	k
fúzi	fúze	k1gFnSc3	fúze
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
rudým	rudý	k1gMnSc7	rudý
obrem	obr	k1gMnSc7	obr
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
fáze	fáze	k1gFnSc2	fáze
spalování	spalování	k1gNnSc2	spalování
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
odhodí	odhodit	k5eAaPmIp3nP	odhodit
své	svůj	k3xOyFgFnPc4	svůj
vnější	vnější	k2eAgFnPc4d1	vnější
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
planetární	planetární	k2eAgFnSc4d1	planetární
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
hvězdy	hvězda	k1gFnSc2	hvězda
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jen	jen	k9	jen
neaktivní	aktivní	k2eNgNnSc1d1	neaktivní
jádro	jádro	k1gNnSc1	jádro
skládající	skládající	k2eAgMnSc1d1	skládající
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jádro	jádro	k1gNnSc1	jádro
nemá	mít	k5eNaImIp3nS	mít
další	další	k2eAgInSc4d1	další
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zvolna	zvolna	k6eAd1	zvolna
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
energii	energie	k1gFnSc4	energie
nashromážděnou	nashromážděný	k2eAgFnSc4d1	nashromážděná
za	za	k7c2	za
aktivního	aktivní	k2eAgInSc2d1	aktivní
života	život	k1gInSc2	život
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
chladne	chladnout	k5eAaImIp3nS	chladnout
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
před	před	k7c7	před
gravitačním	gravitační	k2eAgInSc7d1	gravitační
kolapsem	kolaps	k1gInSc7	kolaps
fúzními	fúzní	k2eAgFnPc7d1	fúzní
reakcemi	reakce	k1gFnPc7	reakce
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
extrémně	extrémně	k6eAd1	extrémně
hustým	hustý	k2eAgInSc7d1	hustý
–	–	k?	–
typicky	typicky	k6eAd1	typicky
je	být	k5eAaImIp3nS	být
polovina	polovina	k1gFnSc1	polovina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
obsažena	obsažen	k2eAgFnSc1d1	obsažena
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
odpovídajícím	odpovídající	k2eAgInSc6d1	odpovídající
objemu	objem	k1gInSc3	objem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
udržován	udržovat	k5eAaImNgInS	udržovat
tlakem	tlak	k1gInSc7	tlak
degenerovaného	degenerovaný	k2eAgInSc2d1	degenerovaný
elektronového	elektronový	k2eAgInSc2d1	elektronový
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hmotnost	hmotnost	k1gFnSc1	hmotnost
bílého	bílý	k1gMnSc2	bílý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
překročení	překročení	k1gNnSc6	překročení
již	již	k6eAd1	již
degenerační	degenerační	k2eAgInSc1d1	degenerační
tlak	tlak	k1gInSc1	tlak
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
odolat	odolat	k5eAaPmF	odolat
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,4	[number]	k4	1,4
hmotností	hmotnost	k1gFnPc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
známou	známá	k1gFnSc7	známá
jako	jako	k8xC	jako
Chandrasekharova	Chandrasekharův	k2eAgFnSc1d1	Chandrasekharova
mez	mez	k1gFnSc1	mez
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
přenosem	přenos	k1gInSc7	přenos
hmoty	hmota	k1gFnSc2	hmota
ze	z	k7c2	z
svého	svůj	k3xOyFgMnSc2	svůj
hvězdného	hvězdný	k2eAgMnSc2d1	hvězdný
průvodce	průvodce	k1gMnSc2	průvodce
<g/>
,	,	kIx,	,
exploduje	explodovat	k5eAaBmIp3nS	explodovat
jako	jako	k9	jako
supernova	supernova	k1gFnSc1	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
ochladí	ochladit	k5eAaPmIp3nS	ochladit
se	se	k3xPyFc4	se
za	za	k7c4	za
stovky	stovka	k1gFnPc4	stovka
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nebude	být	k5eNaImBp3nS	být
viditelný	viditelný	k2eAgMnSc1d1	viditelný
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
černým	černý	k2eAgMnSc7d1	černý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Vezmeme	vzít	k5eAaPmIp1nP	vzít
<g/>
-li	i	k?	-li
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
celou	celý	k2eAgFnSc4d1	celá
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
historii	historie	k1gFnSc4	historie
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
asi	asi	k9	asi
13,8	[number]	k4	13,8
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
i	i	k9	i
ten	ten	k3xDgMnSc1	ten
nejstarší	starý	k2eAgMnSc1d3	nejstarší
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
stále	stále	k6eAd1	stále
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
na	na	k7c6	na
teplotách	teplota	k1gFnPc6	teplota
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
dvojhvězdném	dvojhvězdný	k2eAgInSc6d1	dvojhvězdný
páru	pár	k1gInSc6	pár
s	s	k7c7	s
rudým	rudý	k1gMnSc7	rudý
obrem	obr	k1gMnSc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
obíhají	obíhat	k5eAaImIp3nP	obíhat
okolo	okolo	k7c2	okolo
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
gravitačně	gravitačně	k6eAd1	gravitačně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
a	a	k8xC	a
bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
hromadí	hromadit	k5eAaImIp3nP	hromadit
hmotu	hmota	k1gFnSc4	hmota
svého	svůj	k3xOyFgMnSc2	svůj
hvězdného	hvězdný	k2eAgMnSc2d1	hvězdný
souseda	soused	k1gMnSc2	soused
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
prudce	prudko	k6eAd1	prudko
zažehne	zažehnout	k5eAaPmIp3nS	zažehnout
termonukleární	termonukleární	k2eAgFnSc4d1	termonukleární
reakci	reakce	k1gFnSc4	reakce
a	a	k8xC	a
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
novu	nova	k1gFnSc4	nova
<g/>
.	.	kIx.	.
</s>
