<p>
<s>
Sálim	Sálim	k1gMnSc1	Sálim
Davsárí	Davsárí	k1gMnSc1	Davsárí
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
saúdskoarabský	saúdskoarabský	k2eAgMnSc1d1	saúdskoarabský
fotbalista	fotbalista	k1gMnSc1	fotbalista
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c6	na
postu	post	k1gInSc6	post
záložníka	záložník	k1gMnSc2	záložník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
saúdskoarabském	saúdskoarabský	k2eAgInSc6d1	saúdskoarabský
klubu	klub	k1gInSc6	klub
Al	ala	k1gFnPc2	ala
Hilal	Hilal	k1gInSc4	Hilal
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sálim	Sálim	k1gMnSc1	Sálim
Davsárí	Davsárí	k2eAgMnSc1d1	Davsárí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
