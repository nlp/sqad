<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
především	především	k9	především
antickými	antický	k2eAgInPc7d1	antický
vzory	vzor	k1gInPc7	vzor
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
střízlivý	střízlivý	k2eAgInSc4d1	střízlivý
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
uměřenost	uměřenost	k1gFnSc4	uměřenost
a	a	k8xC	a
jasný	jasný	k2eAgInSc4d1	jasný
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
