<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
především	především	k9	především
antickými	antický	k2eAgInPc7d1	antický
vzory	vzor	k1gInPc7	vzor
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
střízlivý	střízlivý	k2eAgInSc4d1	střízlivý
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
uměřenost	uměřenost	k1gFnSc4	uměřenost
a	a	k8xC	a
jasný	jasný	k2eAgInSc4d1	jasný
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
citové	citový	k2eAgNnSc4d1	citové
až	až	k8xS	až
vášnivé	vášnivý	k2eAgNnSc4d1	vášnivé
baroko	baroko	k1gNnSc4	baroko
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
napodoboval	napodobovat	k5eAaImAgMnS	napodobovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
architekturu	architektura	k1gFnSc4	architektura
a	a	k8xC	a
sochy	socha	k1gFnPc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
klasicismu	klasicismus	k1gInSc2	klasicismus
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
barokní	barokní	k2eAgInSc4d1	barokní
klasicismus	klasicismus	k1gInSc4	klasicismus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
barokní	barokní	k2eAgInPc1d1	barokní
prvky	prvek	k1gInPc1	prvek
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
klasicistními	klasicistní	k2eAgInPc7d1	klasicistní
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
řadí	řadit	k5eAaImIp3nS	řadit
ještě	ještě	k9	ještě
do	do	k7c2	do
kultury	kultura	k1gFnSc2	kultura
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
rokoko	rokoko	k1gNnSc1	rokoko
se	se	k3xPyFc4	se
od	od	k7c2	od
klasicismu	klasicismus	k1gInSc2	klasicismus
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
odvrací	odvracet	k5eAaImIp3nS	odvracet
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
nástup	nástup	k1gInSc1	nástup
klasicismu	klasicismus	k1gInSc2	klasicismus
(	(	kIx(	(
<g/>
také	také	k9	také
louis-seize	louiseize	k6eAd1	louis-seize
či	či	k8xC	či
sloh	sloh	k1gInSc4	sloh
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
přinesl	přinést	k5eAaPmAgInS	přinést
osvícenský	osvícenský	k2eAgInSc1d1	osvícenský
absolutismus	absolutismus	k1gInSc1	absolutismus
(	(	kIx(	(
<g/>
josefinismus	josefinismus	k1gInSc1	josefinismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
klasicismus	klasicismus	k1gInSc1	klasicismus
stal	stát	k5eAaPmAgInS	stát
slohem	sloh	k1gInSc7	sloh
osvícenských	osvícenský	k2eAgInPc2d1	osvícenský
panovnických	panovnický	k2eAgInPc2d1	panovnický
dvorů	dvůr	k1gInPc2	dvůr
<g/>
,	,	kIx,	,
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
také	také	k9	také
k	k	k7c3	k
bohatým	bohatý	k2eAgMnPc3d1	bohatý
měšťanům	měšťan	k1gMnPc3	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
vlnu	vlna	k1gFnSc4	vlna
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
přinesl	přinést	k5eAaPmAgInS	přinést
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
označení	označení	k1gNnSc1	označení
empír	empír	k1gInSc1	empír
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
empire	empir	k1gInSc5	empir
<g/>
,	,	kIx,	,
císařství	císařství	k1gNnSc1	císařství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
architektura	architektura	k1gFnSc1	architektura
stala	stát	k5eAaPmAgFnS	stát
slohem	sloh	k1gInSc7	sloh
běžné	běžný	k2eAgFnSc2d1	běžná
městské	městský	k2eAgFnSc2d1	městská
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
vrátila	vrátit	k5eAaPmAgFnS	vrátit
jako	jako	k9	jako
výraz	výraz	k1gInSc4	výraz
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
i	i	k9	i
jako	jako	k8xC	jako
sloh	sloh	k1gInSc4	sloh
diktátorských	diktátorský	k2eAgInPc2d1	diktátorský
a	a	k8xC	a
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
neoklasicismus	neoklasicismus	k1gInSc1	neoklasicismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc3d1	francouzská
literatuře	literatura	k1gFnSc3	literatura
často	často	k6eAd1	často
znamená	znamenat	k5eAaImIp3nS	znamenat
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
klasicismus	klasicismus	k1gInSc1	klasicismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
se	se	k3xPyFc4	se
od	od	k7c2	od
lat.	lat.	k?	lat.
classicus	classicus	k1gMnSc1	classicus
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgMnSc1d1	patřící
k	k	k7c3	k
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
římské	římský	k2eAgFnSc3d1	římská
třídě	třída	k1gFnSc3	třída
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
classis	classis	k1gInSc1	classis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
prvotřídní	prvotřídní	k2eAgNnSc4d1	prvotřídní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
označení	označení	k1gNnSc6	označení
klasický	klasický	k2eAgInSc4d1	klasický
<g/>
,	,	kIx,	,
klasika	klasika	k1gFnSc1	klasika
pro	pro	k7c4	pro
uznávané	uznávaný	k2eAgInPc4d1	uznávaný
vzory	vzor	k1gInPc4	vzor
a	a	k8xC	a
vrcholy	vrchol	k1gInPc4	vrchol
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
použití	použití	k1gNnSc6	použití
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
umění	umění	k1gNnSc2	umění
znamená	znamenat	k5eAaImIp3nS	znamenat
klasicismus	klasicismus	k1gInSc1	klasicismus
příklon	příklon	k1gInSc1	příklon
ke	k	k7c3	k
starověkým	starověký	k2eAgFnPc3d1	starověká
řeckým	řecký	k2eAgFnPc3d1	řecká
a	a	k8xC	a
římským	římský	k2eAgFnPc3d1	římská
formám	forma	k1gFnPc3	forma
jako	jako	k8xC	jako
k	k	k7c3	k
uznávaným	uznávaný	k2eAgInPc3d1	uznávaný
vzorům	vzor	k1gInPc3	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
představuje	představovat	k5eAaImIp3nS	představovat
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
citově	citově	k6eAd1	citově
působivé	působivý	k2eAgNnSc4d1	působivé
baroko	baroko	k1gNnSc4	baroko
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
tématy	téma	k1gNnPc7	téma
lidské	lidský	k2eAgFnSc2d1	lidská
naděje	naděje	k1gFnSc2	naděje
a	a	k8xC	a
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
utrpení	utrpení	k1gNnSc2	utrpení
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
i	i	k9	i
na	na	k7c4	na
hravé	hravý	k2eAgNnSc4d1	hravé
a	a	k8xC	a
ozdobné	ozdobný	k2eAgNnSc4d1	ozdobné
rokoko	rokoko	k1gNnSc4	rokoko
<g/>
,	,	kIx,	,
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
střízlivý	střízlivý	k2eAgInSc4d1	střízlivý
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
racionalitu	racionalita	k1gFnSc4	racionalita
<g/>
,	,	kIx,	,
uměřenost	uměřenost	k1gFnSc4	uměřenost
a	a	k8xC	a
strohý	strohý	k2eAgInSc4d1	strohý
až	až	k6eAd1	až
přísný	přísný	k2eAgInSc4d1	přísný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srozumitelný	srozumitelný	k2eAgInSc4d1	srozumitelný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
přirozeným	přirozený	k2eAgInSc7d1	přirozený
protějškem	protějšek	k1gInSc7	protějšek
centralizované	centralizovaný	k2eAgFnSc2d1	centralizovaná
až	až	k8xS	až
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
veřejného	veřejný	k2eAgInSc2d1	veřejný
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
i	i	k8xC	i
jejich	jejich	k3xOp3gNnPc1	jejich
city	city	k1gNnPc1	city
musí	muset	k5eAaImIp3nP	muset
podřídit	podřídit	k5eAaPmF	podřídit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
renesance	renesance	k1gFnSc2	renesance
hledal	hledat	k5eAaImAgInS	hledat
klasicismus	klasicismus	k1gInSc1	klasicismus
inspiraci	inspirace	k1gFnSc4	inspirace
zejména	zejména	k9	zejména
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
malíři	malíř	k1gMnPc1	malíř
a	a	k8xC	a
sochaři	sochař	k1gMnPc1	sochař
čerpali	čerpat	k5eAaImAgMnP	čerpat
i	i	k9	i
svá	svůj	k3xOyFgNnPc4	svůj
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
architekti	architekt	k1gMnPc1	architekt
přebírali	přebírat	k5eAaImAgMnP	přebírat
mnoho	mnoho	k4c4	mnoho
prvků	prvek	k1gInPc2	prvek
klasické	klasický	k2eAgFnSc2d1	klasická
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
renesance	renesance	k1gFnSc2	renesance
se	se	k3xPyFc4	se
však	však	k9	však
klasicismus	klasicismus	k1gInSc1	klasicismus
příliš	příliš	k6eAd1	příliš
nevěnuje	věnovat	k5eNaImIp3nS	věnovat
touhám	touha	k1gFnPc3	touha
a	a	k8xC	a
nadějím	naděje	k1gFnPc3	naděje
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
a	a	k8xC	a
spíš	spíš	k9	spíš
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinec	jedinec	k1gMnSc1	jedinec
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
podrobit	podrobit	k5eAaPmF	podrobit
veřejnému	veřejný	k2eAgInSc3d1	veřejný
řádu	řád	k1gInSc3	řád
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
svazuje	svazovat	k5eAaImIp3nS	svazovat
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
tvorbu	tvorba	k1gFnSc4	tvorba
poměrně	poměrně	k6eAd1	poměrně
přísnými	přísný	k2eAgNnPc7d1	přísné
pravidly	pravidlo	k1gNnPc7	pravidlo
-	-	kIx~	-
tak	tak	k9	tak
jako	jako	k9	jako
řecká	řecký	k2eAgFnSc1d1	řecká
architektura	architektura	k1gFnSc1	architektura
-	-	kIx~	-
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
strohé	strohý	k2eAgFnSc6d1	strohá
uměřenosti	uměřenost	k1gFnSc6	uměřenost
je	být	k5eAaImIp3nS	být
trvalá	trvalý	k2eAgFnSc1d1	trvalá
<g/>
,	,	kIx,	,
nadčasová	nadčasový	k2eAgFnSc1d1	nadčasová
krása	krása	k1gFnSc1	krása
<g/>
.	.	kIx.	.
</s>
<s>
Filosofickým	filosofický	k2eAgInSc7d1	filosofický
protějškem	protějšek	k1gInSc7	protějšek
klasicismu	klasicismus	k1gInSc2	klasicismus
jsou	být	k5eAaImIp3nP	být
jednak	jednak	k8xC	jednak
teorie	teorie	k1gFnSc1	teorie
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jen	jen	k9	jen
silná	silný	k2eAgFnSc1d1	silná
vláda	vláda	k1gFnSc1	vláda
může	moct	k5eAaImIp3nS	moct
lidem	člověk	k1gMnPc3	člověk
zajistit	zajistit	k5eAaPmF	zajistit
mír	mír	k1gInSc4	mír
a	a	k8xC	a
bezpečí	bezpečí	k1gNnSc4	bezpečí
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
racionalismus	racionalismus	k1gInSc4	racionalismus
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
"	"	kIx"	"
<g/>
jasnost	jasnost	k1gFnSc4	jasnost
a	a	k8xC	a
zřetelnost	zřetelnost	k1gFnSc4	zřetelnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
myslitelem	myslitel	k1gMnSc7	myslitel
období	období	k1gNnSc2	období
klasicismu	klasicismus	k1gInSc2	klasicismus
byl	být	k5eAaImAgMnS	být
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
<g/>
.	.	kIx.	.
</s>
<s>
Mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
bezpečí	bezpečí	k1gNnSc1	bezpečí
a	a	k8xC	a
prosperita	prosperita	k1gFnSc1	prosperita
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
pevný	pevný	k2eAgInSc4d1	pevný
veřejný	veřejný	k2eAgInSc4d1	veřejný
řád	řád	k1gInSc4	řád
a	a	k8xC	a
silného	silný	k2eAgMnSc4d1	silný
panovníka	panovník	k1gMnSc4	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
diktátorské	diktátorský	k2eAgInPc1d1	diktátorský
režimy	režim	k1gInPc1	režim
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
opakovaně	opakovaně	k6eAd1	opakovaně
vracely	vracet	k5eAaImAgInP	vracet
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
vyprázdněným	vyprázdněný	k2eAgFnPc3d1	vyprázdněná
formám	forma	k1gFnPc3	forma
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
italského	italský	k2eAgInSc2d1	italský
fašismu	fašismus	k1gInSc2	fašismus
<g/>
,	,	kIx,	,
nacismu	nacismus	k1gInSc2	nacismus
i	i	k8xC	i
v	v	k7c6	v
socialistickém	socialistický	k2eAgInSc6d1	socialistický
realismu	realismus	k1gInSc6	realismus
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadněji	nápadně	k6eAd3	nápadně
se	se	k3xPyFc4	se
klasicismus	klasicismus	k1gInSc1	klasicismus
projevoval	projevovat	k5eAaImAgInS	projevovat
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
renesanční	renesanční	k2eAgFnSc4d1	renesanční
architekturu	architektura	k1gFnSc4	architektura
A.	A.	kA	A.
Palladia	palladion	k1gNnSc2	palladion
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
měly	mít	k5eAaImAgFnP	mít
"	"	kIx"	"
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
starověkého	starověký	k2eAgNnSc2d1	starověké
umění	umění	k1gNnSc2	umění
<g/>
"	"	kIx"	"
J.	J.	kA	J.
J.	J.	kA	J.
Winckelmanna	Winckelmanna	k1gFnSc1	Winckelmanna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1764	[number]	k4	1764
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
starověké	starověký	k2eAgNnSc1d1	starověké
umění	umění	k1gNnSc4	umění
roztřídil	roztřídit	k5eAaPmAgInS	roztřídit
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Stavby	stavba	k1gFnPc1	stavba
jsou	být	k5eAaImIp3nP	být
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
přímé	přímý	k2eAgFnPc1d1	přímá
a	a	k8xC	a
čisté	čistý	k2eAgFnPc1d1	čistá
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mohutná	mohutný	k2eAgNnPc4d1	mohutné
sloupořadí	sloupořadí	k1gNnPc4	sloupořadí
a	a	k8xC	a
trojúhelníkové	trojúhelníkový	k2eAgInPc4d1	trojúhelníkový
štíty	štít	k1gInPc4	štít
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
střídmou	střídmý	k2eAgFnSc4d1	střídmá
výzdobu	výzdoba	k1gFnSc4	výzdoba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
působí	působit	k5eAaImIp3nS	působit
strohou	strohý	k2eAgFnSc7d1	strohá
vznešeností	vznešenost	k1gFnSc7	vznešenost
a	a	k8xC	a
mohutností	mohutnost	k1gFnSc7	mohutnost
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaPmIp3nS	stavit
se	se	k3xPyFc4	se
především	především	k6eAd1	především
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pařížský	pařížský	k2eAgInSc1d1	pařížský
Louvre	Louvre	k1gInSc1	Louvre
nebo	nebo	k8xC	nebo
Černínský	Černínský	k2eAgInSc1d1	Černínský
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
a	a	k8xC	a
veřejné	veřejný	k2eAgFnSc2d1	veřejná
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celé	celý	k2eAgFnPc1d1	celá
čtvrti	čtvrt	k1gFnPc1	čtvrt
a	a	k8xC	a
plánovitá	plánovitý	k2eAgNnPc1d1	plánovité
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
Terezín	Terezín	k1gInSc1	Terezín
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Rakousku	Rakousko	k1gNnSc6	Rakousko
i	i	k8xC	i
množství	množství	k1gNnSc6	množství
josefinských	josefinský	k2eAgInPc2d1	josefinský
kostelů	kostel	k1gInPc2	kostel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Na	na	k7c6	na
příkopě	příkop	k1gInSc6	příkop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kasárny	kasárny	k1gFnPc4	kasárny
a	a	k8xC	a
první	první	k4xOgFnPc4	první
továrny	továrna	k1gFnPc4	továrna
i	i	k8xC	i
činžovní	činžovní	k2eAgInPc4d1	činžovní
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
schematickém	schematický	k2eAgInSc6d1	schematický
neoklasicistním	neoklasicistní	k2eAgInSc6d1	neoklasicistní
slohu	sloh	k1gInSc6	sloh
staví	stavit	k5eAaBmIp3nS	stavit
celé	celý	k2eAgFnPc4d1	celá
obytné	obytný	k2eAgFnPc4d1	obytná
čtvrti	čtvrt	k1gFnPc4	čtvrt
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
Karlín	Karlín	k1gInSc1	Karlín
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
radnic	radnice	k1gFnPc2	radnice
<g/>
,	,	kIx,	,
soudních	soudní	k2eAgFnPc2d1	soudní
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
klasicistní	klasicistní	k2eAgFnSc2d1	klasicistní
architektury	architektura	k1gFnSc2	architektura
jsou	být	k5eAaImIp3nP	být
francouzské	francouzský	k2eAgInPc1d1	francouzský
parky	park	k1gInPc1	park
a	a	k8xC	a
zahrady	zahrada	k1gFnPc1	zahrada
s	s	k7c7	s
přísně	přísně	k6eAd1	přísně
geometrickým	geometrický	k2eAgInSc7d1	geometrický
plánem	plán	k1gInSc7	plán
<g/>
,	,	kIx,	,
symetrickým	symetrický	k2eAgInSc7d1	symetrický
systémem	systém	k1gInSc7	systém
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
altánů	altán	k1gInPc2	altán
a	a	k8xC	a
alejí	alej	k1gFnPc2	alej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
keře	keř	k1gInPc1	keř
a	a	k8xC	a
stromy	strom	k1gInPc1	strom
sestříhány	sestříhán	k2eAgInPc1d1	sestříhán
do	do	k7c2	do
geometrických	geometrický	k2eAgInPc2d1	geometrický
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
Květná	květný	k2eAgFnSc1d1	Květná
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
Lednice	Lednice	k1gFnSc1	Lednice
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Dobříš	Dobříš	k1gFnSc4	Dobříš
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc4	zámek
Veltrusy	Veltrusy	k1gInPc4	Veltrusy
nebo	nebo	k8xC	nebo
Krásný	krásný	k2eAgInSc1d1	krásný
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
si	se	k3xPyFc3	se
umělci	umělec	k1gMnPc1	umělec
často	často	k6eAd1	často
volí	volit	k5eAaImIp3nP	volit
témata	téma	k1gNnPc1	téma
z	z	k7c2	z
klasického	klasický	k2eAgInSc2d1	klasický
starověku	starověk	k1gInSc2	starověk
(	(	kIx(	(
<g/>
N.	N.	kA	N.
Poussin	Poussin	k1gMnSc1	Poussin
<g/>
,	,	kIx,	,
Cl	Cl	k1gMnSc1	Cl
<g/>
.	.	kIx.	.
</s>
<s>
Lorrain	Lorrain	k1gInSc1	Lorrain
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
i	i	k9	i
vážná	vážný	k2eAgNnPc4d1	vážné
a	a	k8xC	a
hrdinská	hrdinský	k2eAgNnPc4d1	hrdinské
témata	téma	k1gNnPc4	téma
ze	z	k7c2	z
současnosti	současnost	k1gFnSc2	současnost
(	(	kIx(	(
<g/>
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
L.	L.	kA	L.
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Ingres	Ingres	k1gInSc1	Ingres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sochařství	sochařství	k1gNnSc4	sochařství
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
obliba	obliba	k1gFnSc1	obliba
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
dělají	dělat	k5eAaImIp3nP	dělat
dokonale	dokonale	k6eAd1	dokonale
propracované	propracovaný	k2eAgFnPc4d1	propracovaná
busty	busta	k1gFnPc4	busta
a	a	k8xC	a
pomníky	pomník	k1gInPc4	pomník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgMnPc4d1	hlavní
představitele	představitel	k1gMnPc4	představitel
klasicismu	klasicismus	k1gInSc2	klasicismus
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
italský	italský	k2eAgMnSc1d1	italský
sochař	sochař	k1gMnSc1	sochař
Antonio	Antonio	k1gMnSc1	Antonio
Canova	Canov	k1gInSc2	Canov
a	a	k8xC	a
Dán	Dán	k1gMnSc1	Dán
Bertel	Bertel	k1gMnSc1	Bertel
Thorvaldsen	Thorvaldsen	k1gInSc4	Thorvaldsen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
např.	např.	kA	např.
Jean-Antoine	Jean-Antoin	k1gInSc5	Jean-Antoin
Houdon	Houdon	k1gInSc1	Houdon
a	a	k8xC	a
Etienne	Etienn	k1gInSc5	Etienn
Maurice	Maurika	k1gFnSc3	Maurika
Falconet	Falconet	k1gInSc1	Falconet
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Christian	Christian	k1gMnSc1	Christian
Daniel	Daniel	k1gMnSc1	Daniel
Rauch	Rauch	k1gMnSc1	Rauch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jsou	být	k5eAaImIp3nP	být
zástupci	zástupce	k1gMnPc1	zástupce
druhého	druhý	k4xOgInSc2	druhý
klasicismu	klasicismus	k1gInSc2	klasicismus
a	a	k8xC	a
empíru	empír	k1gInSc2	empír
Josef	Josef	k1gMnSc1	Josef
Malínský	malínský	k2eAgMnSc1d1	malínský
a	a	k8xC	a
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Lederer	Lederer	k1gMnSc1	Lederer
a	a	k8xC	a
bratři	bratr	k1gMnPc1	bratr
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
Emanuel	Emanuel	k1gMnSc1	Emanuel
Maxovi	Max	k1gMnSc3	Max
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Literatura	literatura	k1gFnSc1	literatura
evropského	evropský	k2eAgInSc2d1	evropský
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
klasicismu	klasicismus	k1gInSc2	klasicismus
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
zejména	zejména	k9	zejména
epická	epický	k2eAgFnSc1d1	epická
poezie	poezie	k1gFnSc1	poezie
a	a	k8xC	a
dramatická	dramatický	k2eAgFnSc1d1	dramatická
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
divadlo	divadlo	k1gNnSc1	divadlo
Comédie-Française	Comédie-Française	k1gFnSc2	Comédie-Française
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
klasické	klasický	k2eAgNnSc1d1	klasické
drama	drama	k1gNnSc1	drama
dodnes	dodnes	k6eAd1	dodnes
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Dramata	drama	k1gNnPc1	drama
musela	muset	k5eAaImAgNnP	muset
dodržovat	dodržovat	k5eAaImF	dodržovat
trojjednotu	trojjednota	k1gFnSc4	trojjednota
(	(	kIx(	(
<g/>
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
děje	děj	k1gInSc2	děj
a	a	k8xC	a
místa	místo	k1gNnSc2	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
psala	psát	k5eAaImAgFnS	psát
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
"	"	kIx"	"
<g/>
alexandrinských	alexandrinský	k2eAgInPc6d1	alexandrinský
<g/>
"	"	kIx"	"
verších	verš	k1gInPc6	verš
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
požadavek	požadavek	k1gInSc1	požadavek
stylové	stylový	k2eAgFnSc2d1	stylová
vybroušenosti	vybroušenost	k1gFnSc2	vybroušenost
<g/>
,	,	kIx,	,
srozumitelnosti	srozumitelnost	k1gFnSc2	srozumitelnost
a	a	k8xC	a
jasnosti	jasnost	k1gFnSc2	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
literatura	literatura	k1gFnSc1	literatura
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
nutnost	nutnost	k1gFnSc4	nutnost
společenského	společenský	k2eAgInSc2d1	společenský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgFnSc2d1	pevná
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
i	i	k9	i
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
vládce	vládce	k1gMnSc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
baroka	baroko	k1gNnSc2	baroko
je	být	k5eAaImIp3nS	být
zastáncem	zastánce	k1gMnSc7	zastánce
střídmosti	střídmost	k1gFnSc2	střídmost
<g/>
,	,	kIx,	,
kázně	kázeň	k1gFnSc2	kázeň
a	a	k8xC	a
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
klasicismus	klasicismus	k1gInSc4	klasicismus
jsou	být	k5eAaImIp3nP	být
literární	literární	k2eAgInPc4d1	literární
salony	salon	k1gInPc4	salon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
předčítala	předčítat	k5eAaImAgNnP	předčítat
i	i	k9	i
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
tyto	tento	k3xDgInPc4	tento
salony	salon	k1gInPc4	salon
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c4	na
utváření	utváření	k1gNnSc4	utváření
uměleckého	umělecký	k2eAgInSc2d1	umělecký
vkusu	vkus	k1gInSc2	vkus
a	a	k8xC	a
ovlivňovaly	ovlivňovat	k5eAaImAgInP	ovlivňovat
veřejné	veřejný	k2eAgInPc1d1	veřejný
názory	názor	k1gInPc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
nich	on	k3xPp3gInPc2	on
vznikaly	vznikat	k5eAaImAgFnP	vznikat
také	také	k9	také
literární	literární	k2eAgFnPc1d1	literární
akademie	akademie	k1gFnPc1	akademie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
také	také	k9	také
preciznost	preciznost	k1gFnSc1	preciznost
jejich	jejich	k3xOp3gFnSc2	jejich
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnSc1d1	literární
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
proudy	proud	k1gInPc4	proud
<g/>
:	:	kIx,	:
vysoká	vysoká	k1gFnSc1	vysoká
–	–	k?	–
eposy	epos	k1gInPc1	epos
<g/>
,	,	kIx,	,
ódy	ód	k1gInPc1	ód
<g/>
,	,	kIx,	,
hymny	hymnus	k1gInPc1	hymnus
<g/>
,	,	kIx,	,
tragédie	tragédie	k1gFnPc1	tragédie
<g/>
,	,	kIx,	,
zpracovávaly	zpracovávat	k5eAaImAgFnP	zpracovávat
vznešená	vznešený	k2eAgNnPc4d1	vznešené
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
líčily	líčit	k5eAaImAgInP	líčit
urozené	urozený	k2eAgMnPc4d1	urozený
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Náměty	námět	k1gInPc1	námět
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
antiky	antika	k1gFnSc2	antika
nízká	nízký	k2eAgFnSc1d1	nízká
–	–	k?	–
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
bajka	bajka	k1gFnSc1	bajka
<g/>
,	,	kIx,	,
satira	satira	k1gFnSc1	satira
<g/>
,	,	kIx,	,
zobrazovaly	zobrazovat	k5eAaImAgInP	zobrazovat
život	život	k1gInSc4	život
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
i	i	k8xC	i
venkovanů	venkovan	k1gMnPc2	venkovan
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pojaty	pojmout	k5eAaPmNgFnP	pojmout
komicky	komicky	k6eAd1	komicky
<g/>
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
(	(	kIx(	(
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Poquelin	Poquelin	k2eAgInSc1d1	Poquelin
<g/>
)	)	kIx)	)
Pierre	Pierr	k1gMnSc5	Pierr
Corneille	Corneill	k1gMnSc5	Corneill
Jean	Jean	k1gMnSc1	Jean
Racine	Racin	k1gInSc5	Racin
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
la	la	k1gNnPc2	la
Fontaine	Fontain	k1gInSc5	Fontain
Carlo	Carlo	k1gNnSc1	Carlo
Goldoni	Goldoň	k1gFnSc6	Goldoň
Johann	Johanno	k1gNnPc2	Johanno
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goeth	k1gInSc2	Goeth
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasicismus	klasicismus	k1gInSc1	klasicismus
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
trojicí	trojice	k1gFnSc7	trojice
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
:	:	kIx,	:
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
a	a	k8xC	a
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc4	dílo
těchto	tento	k3xDgMnPc2	tento
skladatelů	skladatel	k1gMnPc2	skladatel
ustavilo	ustavit	k5eAaPmAgNnS	ustavit
vzor	vzor	k1gInSc4	vzor
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgFnSc2d1	klasická
<g/>
"	"	kIx"	"
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
vyvážené	vyvážený	k2eAgFnSc2d1	vyvážená
a	a	k8xC	a
melodické	melodický	k2eAgFnSc2d1	melodická
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
klasické	klasický	k2eAgFnPc1d1	klasická
formy	forma	k1gFnPc1	forma
(	(	kIx(	(
<g/>
sonáta	sonáta	k1gFnSc1	sonáta
<g/>
,	,	kIx,	,
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
tři	tři	k4xCgFnPc1	tři
až	až	k8xS	až
čtyři	čtyři	k4xCgFnPc1	čtyři
věty	věta	k1gFnPc1	věta
s	s	k7c7	s
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
střídáním	střídání	k1gNnSc7	střídání
tempa	tempo	k1gNnSc2	tempo
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
má	mít	k5eAaImIp3nS	mít
stálé	stálý	k2eAgNnSc4d1	stálé
složení	složení	k1gNnSc4	složení
se	s	k7c7	s
smyčcovými	smyčcový	k2eAgFnPc7d1	smyčcová
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
dechovými	dechový	k2eAgInPc7d1	dechový
nástroji	nástroj	k1gInPc7	nástroj
a	a	k8xC	a
v	v	k7c6	v
komorní	komorní	k2eAgFnSc6d1	komorní
hudbě	hudba	k1gFnSc6	hudba
převládá	převládat	k5eAaImIp3nS	převládat
trio	trio	k1gNnSc1	trio
a	a	k8xC	a
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
.	.	kIx.	.
opera	opera	k1gFnSc1	opera
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
se	se	k3xPyFc4	se
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
symfonie	symfonie	k1gFnSc1	symfonie
sonáta	sonáta	k1gFnSc1	sonáta
<g />
.	.	kIx.	.
</s>
<s>
melodram	melodram	k1gInSc1	melodram
–	–	k?	–
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc1	slovo
v	v	k7c6	v
rovnocenné	rovnocenný	k2eAgFnSc6d1	rovnocenná
funkci	funkce	k1gFnSc6	funkce
komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
Antonio	Antonio	k1gMnSc1	Antonio
Salieri	Salier	k1gFnSc2	Salier
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
Vaňhal	Vaňhal	k1gMnSc1	Vaňhal
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Dušek	Dušek	k1gMnSc1	Dušek
Josef	Josef	k1gMnSc1	Josef
Mysliveček	Mysliveček	k1gMnSc1	Mysliveček
Na	na	k7c4	na
prosazení	prosazení	k1gNnSc4	prosazení
klasicismu	klasicismus	k1gInSc2	klasicismus
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
měli	mít	k5eAaImAgMnP	mít
rovněž	rovněž	k9	rovněž
(	(	kIx(	(
<g/>
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
<g/>
)	)	kIx)	)
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
synové	syn	k1gMnPc1	syn
největšího	veliký	k2eAgMnSc2d3	veliký
skladatele	skladatel	k1gMnSc2	skladatel
baroka	baroko	k1gNnSc2	baroko
Johana	Johan	k1gMnSc2	Johan
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
