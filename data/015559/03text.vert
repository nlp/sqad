<s>
Frank	Frank	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
RutherfordOsobní	RutherfordOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Narození	narození	k1gNnSc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1964	#num#	k4
(	(	kIx(
<g/>
56	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Atletika	atletika	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
LOH	LOH	kA
1992	#num#	k4
</s>
<s>
trojskok	trojskok	k1gInSc1
</s>
<s>
Halové	halový	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
HMS	HMS	kA
1987	#num#	k4
</s>
<s>
trojskok	trojskok	k1gInSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Garfield	Garfield	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1964	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
bahamský	bahamský	k2eAgMnSc1d1
trojskokan	trojskokan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
osobní	osobní	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
jsou	být	k5eAaImIp3nP
17,41	17,41	k4
m	m	kA
venku	venku	k6eAd1
a	a	k8xC
17,09	17,09	k4
m	m	kA
v	v	k7c6
hale	hala	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Připravoval	připravovat	k5eAaImAgInS
se	se	k3xPyFc4
v	v	k7c6
USA	USA	kA
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
Houston	Houston	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
akademický	akademický	k2eAgInSc4d1
titul	titul	k1gInSc4
v	v	k7c6
oborech	obor	k1gInPc6
ekonomie	ekonomie	k1gFnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třikrát	třikrát	k6eAd1
získal	získat	k5eAaPmAgInS
prvenství	prvenství	k1gNnSc4
v	v	k7c6
National	National	k1gFnSc6
Collegiate	Collegiat	k1gInSc5
Athletic	Athletice	k1gFnPc2
Association	Association	k1gInSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
halovým	halový	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
obsadil	obsadit	k5eAaPmAgMnS
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
trojskokanské	trojskokanský	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
na	na	k7c6
Středoamerických	středoamerický	k2eAgFnPc6d1
a	a	k8xC
karibských	karibský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
získal	získat	k5eAaPmAgMnS
bronzové	bronzový	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
na	na	k7c6
halovém	halový	k2eAgNnSc6d1
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
i	i	k8xC
na	na	k7c6
Panamerických	panamerický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1988	#num#	k4
i	i	k8xC
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
1991	#num#	k4
byl	být	k5eAaImAgMnS
vyřazen	vyřadit	k5eAaPmNgMnS
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgMnS
druhý	druhý	k4xOgMnSc1
na	na	k7c6
Kontinentálním	kontinentální	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
skončil	skončit	k5eAaPmAgMnS
v	v	k7c6
trojskokanském	trojskokanský	k2eAgNnSc6d1
finále	finále	k1gNnSc6
díky	díky	k7c3
výkonu	výkon	k1gInSc3
17,36	17,36	k4
m	m	kA
třetí	třetí	k4xOgInSc4
za	za	k7c4
reprezentanty	reprezentant	k1gMnPc4
USA	USA	kA
Mikem	Mik	k1gMnSc7
Conleym	Conleym	k1gInSc4
a	a	k8xC
Charlesem	Charles	k1gMnSc7
Simpkinsem	Simpkins	k1gMnSc7
a	a	k8xC
získal	získat	k5eAaPmAgMnS
tak	tak	k6eAd1
pro	pro	k7c4
Bahamy	Bahamy	k1gFnPc4
první	první	k4xOgFnSc4
atletickou	atletický	k2eAgFnSc4d1
olympijskou	olympijský	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
MS	MS	kA
1993	#num#	k4
i	i	k9
1995	#num#	k4
nepostoupil	postoupit	k5eNaPmAgInS
z	z	k7c2
kvalifikace	kvalifikace	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
LOH	LOH	kA
1996	#num#	k4
obsadil	obsadit	k5eAaPmAgMnS
jedenácté	jedenáctý	k4xOgNnSc4
místo	místo	k1gNnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vicemistrem	vicemistr	k1gMnSc7
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
Karibiku	Karibik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
aktivní	aktivní	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
založil	založit	k5eAaPmAgMnS
nadaci	nadace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pomáhá	pomáhat	k5eAaImIp3nS
mladým	mladý	k2eAgMnPc3d1
bahamským	bahamský	k2eAgMnPc3d1
sportovcům	sportovec	k1gMnPc3
získat	získat	k5eAaPmF
stipendia	stipendium	k1gNnPc4
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
převzal	převzít	k5eAaPmAgInS
Řád	řád	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
World	Worldo	k1gNnPc2
Athletics	Athletics	k1gInSc1
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Bahamas	Bahamas	k1gInSc1
Olympic	Olympice	k1gFnPc2
Comittee	Comitte	k1gFnSc2
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
The	The	k1gMnSc5
Tribune	tribun	k1gMnSc5
Dostupné	dostupný	k2eAgNnSc4d1
online	onlin	k1gInSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Frank	Frank	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
