<s>
Obec	obec	k1gFnSc1	obec
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Středočeský	středočeský	k2eAgInSc1d1	středočeský
<g/>
,	,	kIx,	,
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
v	v	k7c6	v
kopcovitém	kopcovitý	k2eAgInSc6d1	kopcovitý
terénu	terén	k1gInSc6	terén
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Pražské	pražský	k2eAgFnSc2d1	Pražská
plošiny	plošina	k1gFnSc2	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
145	[number]	k4	145
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
historických	historický	k2eAgFnPc2d1	historická
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
Skochovic	Skochovice	k1gFnPc2	Skochovice
a	a	k8xC	a
Vraného	vraný	k2eAgNnSc2d1	Vrané
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
území	území	k1gNnSc2	území
Vraného	vraný	k2eAgNnSc2d1	Vrané
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
i	i	k8xC	i
zástavba	zástavba	k1gFnSc1	zástavba
navazující	navazující	k2eAgFnSc1d1	navazující
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
Březovou	březový	k2eAgFnSc4d1	Březová
(	(	kIx(	(
<g/>
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Březová-Oleško	Březová-Oleška	k1gFnSc5	Březová-Oleška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
nesla	nést	k5eAaImAgFnS	nést
obec	obec	k1gFnSc1	obec
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
osad	osada	k1gFnPc2	osada
Skochovice	Skochovice	k1gFnSc2	Skochovice
a	a	k8xC	a
Vrané	vraný	k2eAgInPc1d1	vraný
název	název	k1gInSc1	název
Skochovice	Skochovice	k1gFnSc2	Skochovice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
993	[number]	k4	993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
zakládací	zakládací	k2eAgFnSc1d1	zakládací
listina	listina	k1gFnSc1	listina
břevnovského	břevnovský	k2eAgInSc2d1	břevnovský
kláštera	klášter	k1gInSc2	klášter
jako	jako	k8xS	jako
o	o	k7c6	o
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
klášter	klášter	k1gInSc1	klášter
obdržel	obdržet	k5eAaPmAgInS	obdržet
od	od	k7c2	od
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
statky	statek	k1gInPc1	statek
k	k	k7c3	k
obživě	obživa	k1gFnSc3	obživa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1407	[number]	k4	1407
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
majitele	majitel	k1gMnSc2	majitel
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obce	obec	k1gFnPc1	obec
byly	být	k5eAaImAgFnP	být
prodány	prodat	k5eAaPmNgFnP	prodat
klášteru	klášter	k1gInSc3	klášter
zbraslavskému	zbraslavský	k2eAgNnSc3d1	Zbraslavské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1930	[number]	k4	1930
až	až	k9	až
1936	[number]	k4	1936
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
zbudována	zbudován	k2eAgFnSc1d1	zbudována
přehrada	přehrada	k1gFnSc1	přehrada
s	s	k7c7	s
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
provedena	proveden	k2eAgFnSc1d1	provedena
celková	celkový	k2eAgFnSc1d1	celková
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
bývala	bývat	k5eAaImAgFnS	bývat
také	také	k9	také
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
tradice	tradice	k1gFnSc1	tradice
výroby	výroba	k1gFnSc2	výroba
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
místních	místní	k2eAgFnPc2d1	místní
papíren	papírna	k1gFnPc2	papírna
byla	být	k5eAaImAgFnS	být
rozprodána	rozprodat	k5eAaPmNgFnS	rozprodat
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
osady	osada	k1gFnSc2	osada
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
1850	[number]	k4	1850
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
1855	[number]	k4	1855
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
1868	[number]	k4	1868
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
1927	[number]	k4	1927
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Praha-venkov	Prahaenkov	k1gInSc1	Praha-venkov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
1939	[number]	k4	1939
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Praha-venkov	Prahaenkov	k1gInSc1	Praha-venkov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
1942	[number]	k4	1942
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Praha-venkov-jih	Prahaenkovih	k1gInSc1	Praha-venkov-jih
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
1945	[number]	k4	1945
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgInSc1d1	správní
okres	okres	k1gInSc1	okres
Praha-venkov-jih	Prahaenkovih	k1gInSc1	Praha-venkov-jih
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
1949	[number]	k4	1949
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Praha-jih	Prahaih	k1gInSc1	Praha-jih
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
2003	[number]	k4	2003
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Černošice	Černošice	k1gFnSc2	Černošice
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Skochovice	Skochovice	k1gFnSc2	Skochovice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Vrané	vraný	k2eAgFnPc1d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
969	[number]	k4	969
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
ves	ves	k1gFnSc1	ves
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Vraného	vraný	k2eAgNnSc2d1	Vrané
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
biograf	biograf	k1gMnSc1	biograf
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
2	[number]	k4	2
holiči	holič	k1gMnPc7	holič
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
hostinců	hostinec	k1gInPc2	hostinec
<g/>
,	,	kIx,	,
2	[number]	k4	2
kapelníci	kapelník	k1gMnPc1	kapelník
<g/>
,	,	kIx,	,
konsum	konsum	k1gInSc1	konsum
Včela	včela	k1gFnSc1	včela
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
mědikovec	mědikovec	k1gMnSc1	mědikovec
<g/>
,	,	kIx,	,
2	[number]	k4	2
obchody	obchod	k1gInPc4	obchod
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
<g/>
,	,	kIx,	,
3	[number]	k4	3
obuvníci	obuvník	k1gMnPc1	obuvník
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
papírnická	papírnický	k2eAgFnSc1d1	Papírnická
<g/>
,	,	kIx,	,
2	[number]	k4	2
pekaři	pekař	k1gMnPc7	pekař
<g/>
,	,	kIx,	,
porodní	porodní	k2eAgFnSc1d1	porodní
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
,	,	kIx,	,
3	[number]	k4	3
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
8	[number]	k4	8
obchodů	obchod	k1gInPc2	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgNnSc4d1	stavební
družstvo	družstvo	k1gNnSc4	družstvo
Lidový	lidový	k2eAgInSc1d1	lidový
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
střižním	střižní	k2eAgNnSc7d1	střižní
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
3	[number]	k4	3
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
Barokní	barokní	k2eAgInSc4d1	barokní
Kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vrané	vraný	k2eAgInPc1d1	vraný
Dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
Pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
Do	do	k7c2	do
obce	obec	k1gFnSc2	obec
vedou	vést	k5eAaImIp3nP	vést
silnice	silnice	k1gFnPc1	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
silnice	silnice	k1gFnSc2	silnice
z	z	k7c2	z
Vraného	vraný	k2eAgInSc2d1	vraný
přes	přes	k7c4	přes
Zvoli	Zvole	k1gFnSc4	Zvole
a	a	k8xC	a
Ohrobec	Ohrobec	k1gInSc4	Ohrobec
do	do	k7c2	do
Dolních	dolní	k2eAgInPc2d1	dolní
Břežan	Břežany	k1gInPc2	Břežany
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
silnice	silnice	k1gFnSc1	silnice
okolo	okolo	k7c2	okolo
řeky	řeka	k1gFnSc2	řeka
přes	přes	k7c4	přes
Jarov	Jarov	k1gInSc4	Jarov
a	a	k8xC	a
Zbraslavský	zbraslavský	k2eAgInSc4d1	zbraslavský
most	most	k1gInSc4	most
na	na	k7c4	na
Zbraslav	Zbraslav	k1gFnSc4	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
210	[number]	k4	210
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
-	-	kIx~	-
Čerčany	Čerčan	k1gMnPc7	Čerčan
<g/>
/	/	kIx~	/
<g/>
Dobříš	Dobříš	k1gFnSc4	Dobříš
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
regionální	regionální	k2eAgFnSc1d1	regionální
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
Vrané	vraný	k2eAgInPc1d1	vraný
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
se	se	k3xPyFc4	se
trať	trať	k1gFnSc1	trať
dopravně	dopravně	k6eAd1	dopravně
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
<g/>
,	,	kIx,	,
do	do	k7c2	do
Jílového	Jílové	k1gNnSc2	Jílové
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Čerčan	Čerčan	k1gMnSc1	Čerčan
a	a	k8xC	a
do	do	k7c2	do
Dobříše	Dobříš	k1gFnSc2	Dobříš
<g/>
.	.	kIx.	.
</s>
<s>
Stavebně	stavebně	k6eAd1	stavebně
se	se	k3xPyFc4	se
trať	trať	k1gFnSc1	trať
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
až	až	k9	až
za	za	k7c7	za
železniční	železniční	k2eAgFnSc7d1	železniční
zastávkou	zastávka	k1gFnSc7	zastávka
Skochovice	Skochovice	k1gFnSc2	Skochovice
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
2011	[number]	k4	2011
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
měla	mít	k5eAaImAgFnS	mít
zastávku	zastávka	k1gFnSc4	zastávka
příměstská	příměstský	k2eAgFnSc1d1	příměstská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
331	[number]	k4	331
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
<g/>
Budějovická	budějovický	k2eAgFnSc1d1	Budějovická
-	-	kIx~	-
Jílové	jílový	k2eAgInPc1d1	jílový
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
<g/>
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
3	[number]	k4	3
spoje	spoj	k1gInSc2	spoj
do	do	k7c2	do
Zvole	Zvole	k1gFnSc2	Zvole
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dopravce	dopravce	k1gMnSc2	dopravce
Veolia	Veolia	k1gFnSc1	Veolia
Transport	transport	k1gInSc1	transport
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
210	[number]	k4	210
vedou	vést	k5eAaImIp3nP	vést
linky	linka	k1gFnPc1	linka
S8	S8	k1gFnPc2	S8
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
-	-	kIx~	-
Čerčany	Čerčan	k1gMnPc7	Čerčan
<g/>
)	)	kIx)	)
a	a	k8xC	a
S80	S80	k1gFnSc1	S80
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
-	-	kIx~	-
Dobříš	Dobříš	k1gFnSc1	Dobříš
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pražského	pražský	k2eAgInSc2d1	pražský
systému	systém	k1gInSc2	systém
Esko	eska	k1gFnSc5	eska
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
jezdilo	jezdit	k5eAaImAgNnS	jezdit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
23	[number]	k4	23
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
19	[number]	k4	19
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
železniční	železniční	k2eAgFnSc7d1	železniční
zastávkou	zastávka	k1gFnSc7	zastávka
Skochovice	Skochovice	k1gFnSc2	Skochovice
jezdilo	jezdit	k5eAaImAgNnS	jezdit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Dobříš	dobřit	k5eAaImIp2nS	dobřit
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
12	[number]	k4	12
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
9	[number]	k4	9
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Jílové	Jílové	k1gNnSc1	Jílové
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
denně	denně	k6eAd1	denně
12	[number]	k4	12
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Přívoz	přívoz	k1gInSc4	přívoz
Obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
brodu	brod	k1gInSc2	brod
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přívozem	přívoz	k1gInSc7	přívoz
<g/>
.	.	kIx.	.
</s>
<s>
Přívoz	přívoz	k1gInSc1	přívoz
z	z	k7c2	z
Vraného	vraný	k2eAgMnSc2d1	vraný
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
zbraslavských	zbraslavský	k2eAgFnPc2d1	Zbraslavská
Strnad	strnad	k1gInSc4	strnad
funguje	fungovat	k5eAaImIp3nS	fungovat
dosud	dosud	k6eAd1	dosud
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
jej	on	k3xPp3gMnSc4	on
obec	obec	k1gFnSc1	obec
Vrané	vraný	k2eAgFnSc2d1	Vraná
<g/>
.	.	kIx.	.
</s>
<s>
Vraným	vraný	k2eAgMnSc7d1	vraný
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
jméno	jméno	k1gNnSc1	jméno
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Vejvody	Vejvoda	k1gMnSc2	Vejvoda
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
slavné	slavný	k2eAgFnSc2d1	slavná
písně	píseň	k1gFnSc2	píseň
Škoda	škoda	k1gFnSc1	škoda
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Voříšková	Voříšková	k1gFnSc1	Voříšková
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
??	??	k?	??
<g/>
)	)	kIx)	)
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc1	mládež
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
