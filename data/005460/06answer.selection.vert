<s>
Obec	obec	k1gFnSc1	obec
Vrané	vraný	k2eAgFnSc2d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Středočeský	středočeský	k2eAgInSc1d1	středočeský
<g/>
,	,	kIx,	,
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
v	v	k7c6	v
kopcovitém	kopcovitý	k2eAgInSc6d1	kopcovitý
terénu	terén	k1gInSc6	terén
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Pražské	pražský	k2eAgFnSc2d1	Pražská
plošiny	plošina	k1gFnSc2	plošina
<g/>
.	.	kIx.	.
</s>
