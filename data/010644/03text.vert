<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Třebové	Třebová	k1gFnSc6	Třebová
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
basketbalista	basketbalista	k1gMnSc1	basketbalista
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
i	i	k9	i
trenér	trenér	k1gMnSc1	trenér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
basketbal	basketbal	k1gInSc4	basketbal
za	za	k7c7	za
kluby	klub	k1gInPc7	klub
Slavia	Slavium	k1gNnSc2	Slavium
VŠ	vš	k0	vš
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
BK	BK	kA	BK
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
sezón	sezóna	k1gFnPc2	sezóna
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
domácí	domácí	k2eAgFnSc6d1	domácí
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hráč	hráč	k1gMnSc1	hráč
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
Československo	Československo	k1gNnSc4	Československo
a	a	k8xC	a
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
za	za	k7c4	za
reprezentaci	reprezentace	k1gFnSc4	reprezentace
odehrál	odehrát	k5eAaPmAgMnS	odehrát
15	[number]	k4	15
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
hráčské	hráčský	k2eAgFnSc2d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
funkci	funkce	k1gFnSc6	funkce
trenéra	trenér	k1gMnSc2	trenér
u	u	k7c2	u
týmů	tým	k1gInPc2	tým
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
dorostenců	dorostenec	k1gMnPc2	dorostenec
Sparty	Sparta	k1gFnSc2	Sparta
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
6	[number]	k4	6
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
asistentem	asistent	k1gMnSc7	asistent
trenéra	trenér	k1gMnSc2	trenér
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
družstva	družstvo	k1gNnSc2	družstvo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hráčská	hráčský	k2eAgNnPc4d1	hráčské
===	===	k?	===
</s>
</p>
<p>
<s>
kluby	klub	k1gInPc1	klub
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1986-1992	[number]	k4	1986-1992
Slavia	Slavia	k1gFnSc1	Slavia
VŠ	vš	k0	vš
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1992-1997	[number]	k4	1992-1997
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
vicemistr	vicemistr	k1gMnSc1	vicemistr
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
sezon	sezona	k1gFnPc2	sezona
a	a	k8xC	a
2126	[number]	k4	2126
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
BK	BK	kA	BK
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
<p>
<s>
FIBA	FIBA	kA	FIBA
Pohár	pohár	k1gInSc1	pohár
Korač	Korač	k1gInSc1	Korač
<g/>
:	:	kIx,	:
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
5	[number]	k4	5
ročnících	ročník	k1gInPc6	ročník
soutěže	soutěž	k1gFnSc2	soutěž
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
93	[number]	k4	93
(	(	kIx(	(
<g/>
Sparta	Sparta	k1gFnSc1	Sparta
doma	doma	k6eAd1	doma
porazila	porazit	k5eAaPmAgFnS	porazit
Fenerbahce	Fenerbahce	k1gFnSc1	Fenerbahce
Istanbul	Istanbul	k1gInSc1	Istanbul
96	[number]	k4	96
<g/>
-	-	kIx~	-
<g/>
87	[number]	k4	87
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
95	[number]	k4	95
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
137	[number]	k4	137
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
18	[number]	k4	18
zápasech	zápas	k1gInPc6	zápas
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
x	x	k?	x
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Československo	Československo	k1gNnSc4	Československo
a	a	k8xC	a
11	[number]	k4	11
<g/>
x	x	k?	x
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Trenérská	trenérský	k2eAgNnPc4d1	trenérské
===	===	k?	===
</s>
</p>
<p>
<s>
klub	klub	k1gInSc1	klub
:	:	kIx,	:
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc2	trenér
<g/>
,	,	kIx,	,
liga	liga	k1gFnSc1	liga
muži	muž	k1gMnSc3	muž
(	(	kIx(	(
<g/>
NBL	NBL	kA	NBL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004-2006	[number]	k4	2004-2006
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
liga	liga	k1gFnSc1	liga
muži	muž	k1gMnSc3	muž
(	(	kIx(	(
<g/>
NBL	NBL	kA	NBL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
od	od	k7c2	od
2007	[number]	k4	2007
BA	ba	k9	ba
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
liga	liga	k1gFnSc1	liga
muži	muž	k1gMnSc6	muž
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
2006-2012	[number]	k4	2006-2012
asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc2	trenér
reprezentace	reprezentace	k1gFnSc2	reprezentace
mužů	muž	k1gMnPc2	muž
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
U	u	k7c2	u
<g/>
20	[number]	k4	20
<g/>
:	:	kIx,	:
divize	divize	k1gFnSc2	divize
A	A	kA	A
<g/>
:	:	kIx,	:
2010	[number]	k4	2010
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divize	divize	k1gFnSc1	divize
B	B	kA	B
<g/>
:	:	kIx,	:
2006	[number]	k4	2006
Lisabon	Lisabon	k1gInSc1	Lisabon
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Varšava	Varšava	k1gFnSc1	Varšava
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Makedonie	Makedonie	k1gFnSc1	Makedonie
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
postup	postup	k1gInSc4	postup
do	do	k7c2	do
divize	divize	k1gFnSc2	divize
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Bosna	Bosna	k1gFnSc1	Bosna
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
Mattoni	Matton	k1gMnPc1	Matton
NBL	NBL	kA	NBL
</s>
</p>
<p>
<s>
BC	BC	kA	BC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
basketbalu	basketbal	k1gInSc2	basketbal
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
tabulka	tabulka	k1gFnSc1	tabulka
střelců	střelec	k1gMnPc2	střelec
Sparty	Sparta	k1gFnSc2	Sparta
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
5	[number]	k4	5
sezon	sezona	k1gFnPc2	sezona
<g/>
,	,	kIx,	,
2126	[number]	k4	2126
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
Statistiky	statistika	k1gFnPc1	statistika
hráčů	hráč	k1gMnPc2	hráč
Sparty	Sparta	k1gFnSc2	Sparta
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
klubových	klubový	k2eAgInPc6d1	klubový
pohárech	pohár	k1gInPc6	pohár
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Geršl	Geršl	k1gMnSc1	Geršl
18	[number]	k4	18
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
137	[number]	k4	137
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
25	[number]	k4	25
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
střeleckých	střelecký	k2eAgMnPc2d1	střelecký
výkonů	výkon	k1gInPc2	výkon
za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
</s>
</p>
<p>
<s>
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
</s>
</p>
<p>
<s>
Basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
akademie	akademie	k1gFnSc1	akademie
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
</s>
</p>
<p>
<s>
FIBA	FIBA	kA	FIBA
Europe	Europ	k1gInSc5	Europ
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Geršl	Geršl	k1gMnSc1	Geršl
</s>
</p>
