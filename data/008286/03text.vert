<p>
<s>
Lakam	Lakam	k1gInSc1	Lakam
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
ל	ל	k?	ל
מ	מ	k?	מ
<g/>
,	,	kIx,	,
Liška	Liška	k1gMnSc1	Liška
le-Kišrej	le-Kišrat	k5eAaImRp2nS	le-Kišrat
Madaj	Madaj	k1gInSc1	Madaj
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
Oddělení	oddělení	k1gNnSc4	oddělení
vědeckých	vědecký	k2eAgInPc2d1	vědecký
styků	styk	k1gInPc2	styk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izraelská	izraelský	k2eAgFnSc1d1	izraelská
výzvědná	výzvědný	k2eAgFnSc1d1	výzvědná
služba	služba	k1gFnSc1	služba
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
izraelského	izraelský	k2eAgInSc2d1	izraelský
jaderného	jaderný	k2eAgInSc2d1	jaderný
programu	program	k1gInSc2	program
a	a	k8xC	a
na	na	k7c4	na
sběr	sběr	k1gInSc4	sběr
tajných	tajný	k2eAgFnPc2d1	tajná
informací	informace	k1gFnPc2	informace
vědeckého	vědecký	k2eAgInSc2d1	vědecký
a	a	k8xC	a
technického	technický	k2eAgInSc2d1	technický
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
zněl	znět	k5eAaImAgInS	znět
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Založení	založení	k1gNnSc1	založení
==	==	k?	==
</s>
</p>
<p>
<s>
Lakam	Lakam	k1gInSc1	Lakam
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
Šimonem	Šimon	k1gMnSc7	Šimon
Peresem	Peres	k1gMnSc7	Peres
<g/>
,	,	kIx,	,
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
náměstkem	náměstek	k1gMnSc7	náměstek
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
totiž	totiž	k9	totiž
Izrael	Izrael	k1gInSc1	Izrael
získal	získat	k5eAaPmAgInS	získat
od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
jaderný	jaderný	k2eAgInSc4d1	jaderný
reaktor	reaktor	k1gInSc4	reaktor
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zachovat	zachovat	k5eAaPmF	zachovat
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
měl	mít	k5eAaImAgInS	mít
Lakam	Lakam	k1gInSc1	Lakam
také	také	k9	také
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
rozvědka	rozvědka	k1gFnSc1	rozvědka
pro	pro	k7c4	pro
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
špionáž	špionáž	k1gFnSc4	špionáž
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
Mosadu	Mosad	k1gInSc2	Mosad
I.	I.	kA	I.
Harel	Harel	k1gMnSc1	Harel
odmítal	odmítat	k5eAaImAgMnS	odmítat
taková	takový	k3xDgNnPc4	takový
zadání	zadání	k1gNnPc4	zadání
přijímat	přijímat	k5eAaImF	přijímat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
Lakamu	Lakam	k1gInSc2	Lakam
vědělo	vědět	k5eAaImAgNnS	vědět
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc4	jeho
informátory	informátor	k1gMnPc4	informátor
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
například	například	k6eAd1	například
vědečtí	vědecký	k2eAgMnPc1d1	vědecký
přidělenci	přidělenec	k1gMnPc1	přidělenec
izraelských	izraelský	k2eAgFnPc2d1	izraelská
ambasád	ambasáda	k1gFnPc2	ambasáda
v	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
či	či	k8xC	či
izraelští	izraelský	k2eAgMnPc1d1	izraelský
vědci	vědec	k1gMnPc1	vědec
působící	působící	k2eAgMnPc1d1	působící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
působnosti	působnost	k1gFnSc2	působnost
Lakamu	Lakam	k1gInSc2	Lakam
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
dohled	dohled	k1gInSc4	dohled
na	na	k7c4	na
izraelský	izraelský	k2eAgInSc4d1	izraelský
jaderný	jaderný	k2eAgInSc4d1	jaderný
reaktor	reaktor	k1gInSc4	reaktor
v	v	k7c6	v
Negevské	Negevský	k2eAgFnSc6d1	Negevská
poušti	poušť	k1gFnSc6	poušť
(	(	kIx(	(
Dimuna	Dimuna	k1gFnSc1	Dimuna
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
vyslovily	vyslovit	k5eAaPmAgInP	vyslovit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
SDECE	SDECE	kA	SDECE
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
CIA	CIA	kA	CIA
podezření	podezření	k1gNnPc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
Izrael	Izrael	k1gInSc1	Izrael
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Francie	Francie	k1gFnSc1	Francie
rázně	rázně	k6eAd1	rázně
omezila	omezit	k5eAaPmAgFnS	omezit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
na	na	k7c6	na
jaderném	jaderný	k2eAgInSc6d1	jaderný
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
zjistila	zjistit	k5eAaPmAgFnS	zjistit
americká	americký	k2eAgFnSc1d1	americká
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
skladu	sklad	k1gInSc2	sklad
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
jaderná	jaderný	k2eAgNnPc4d1	jaderné
paliva	palivo	k1gNnPc4	palivo
NUMEC	NUMEC	kA	NUMEC
v	v	k7c6	v
Apollu	Apollo	k1gNnSc6	Apollo
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
kdosi	kdosi	k3yInSc1	kdosi
odcizil	odcizit	k5eAaPmAgInS	odcizit
91	[number]	k4	91
kg	kg	kA	kg
obohaceného	obohacený	k2eAgInSc2d1	obohacený
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
sklad	sklad	k1gInSc4	sklad
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
krádeží	krádež	k1gFnSc7	krádež
navštívil	navštívit	k5eAaPmAgMnS	navštívit
izraelský	izraelský	k2eAgMnSc1d1	izraelský
vědecký	vědecký	k2eAgMnSc1d1	vědecký
přidělenec	přidělenec	k1gMnSc1	přidělenec
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
agent	agent	k1gMnSc1	agent
Lakamu	Lakam	k1gInSc2	Lakam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
americké	americký	k2eAgFnPc1d1	americká
zpravodajské	zpravodajský	k2eAgFnPc1d1	zpravodajská
služby	služba	k1gFnPc1	služba
považovaly	považovat	k5eAaImAgFnP	považovat
dohady	dohad	k1gInPc4	dohad
o	o	k7c6	o
snahách	snaha	k1gFnPc6	snaha
Izraele	Izrael	k1gInSc2	Izrael
o	o	k7c6	o
získání	získání	k1gNnSc6	získání
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
za	za	k7c4	za
potvrzené	potvrzená	k1gFnPc4	potvrzená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lakamu	Lakam	k1gInSc3	Lakam
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Mosadem	Mosad	k1gInSc7	Mosad
získat	získat	k5eAaPmF	získat
technické	technický	k2eAgInPc4d1	technický
výkresy	výkres	k1gInPc4	výkres
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
francouzských	francouzský	k2eAgInPc2d1	francouzský
letounů	letoun	k1gInPc2	letoun
Mirage	Mirag	k1gFnSc2	Mirag
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
inženýra	inženýr	k1gMnSc2	inženýr
A.	A.	kA	A.
Frauenknechta	Frauenknecht	k1gMnSc2	Frauenknecht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Operace	operace	k1gFnSc1	operace
Plumbat	Plumbat	k1gMnPc2	Plumbat
vyzvedla	vyzvednout	k5eAaPmAgFnS	vyzvednout
obchodní	obchodní	k2eAgFnSc1d1	obchodní
loď	loď	k1gFnSc1	loď
Scheersberg	Scheersberg	k1gInSc1	Scheersberg
plující	plující	k2eAgInSc4d1	plující
pod	pod	k7c7	pod
německou	německý	k2eAgFnSc7d1	německá
vlajkou	vlajka	k1gFnSc7	vlajka
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
uran	uran	k1gInSc4	uran
pro	pro	k7c4	pro
jistou	jistý	k2eAgFnSc4d1	jistá
německou	německý	k2eAgFnSc4d1	německá
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Janova	Janov	k1gInSc2	Janov
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Scheersberg	Scheersberg	k1gMnSc1	Scheersberg
nikdy	nikdy	k6eAd1	nikdy
nedoplul	doplout	k5eNaPmAgMnS	doplout
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
čase	čas	k1gInSc6	čas
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
obchodní	obchodní	k2eAgFnSc1d1	obchodní
loď	loď	k1gFnSc1	loď
nalezena	nalezen	k2eAgFnSc1d1	nalezena
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
tureckém	turecký	k2eAgInSc6d1	turecký
přístavu	přístav	k1gInSc6	přístav
s	s	k7c7	s
prázdným	prázdný	k2eAgInSc7d1	prázdný
nákladovým	nákladový	k2eAgInSc7d1	nákladový
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
byl	být	k5eAaImAgInS	být
kdesi	kdesi	k6eAd1	kdesi
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
přeložen	přeložen	k2eAgInSc1d1	přeložen
na	na	k7c4	na
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neúspěchy	neúspěch	k1gInPc4	neúspěch
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
neúspěchům	neúspěch	k1gInPc3	neúspěch
patřila	patřit	k5eAaImAgFnS	patřit
tzv.	tzv.	kA	tzv.
Pollardova	Pollardův	k2eAgFnSc1d1	Pollardova
aféra	aféra	k1gFnSc1	aféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	nízce	k6eAd2	nízce
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
v	v	k7c6	v
USA	USA	kA	USA
zatčen	zatknout	k5eAaPmNgMnS	zatknout
agent	agent	k1gMnSc1	agent
Lakamu	Lakam	k1gInSc2	Lakam
Jonathan	Jonathan	k1gMnSc1	Jonathan
J.	J.	kA	J.
Pollard	Pollard	k1gMnSc1	Pollard
<g/>
,	,	kIx,	,
zpravodajský	zpravodajský	k2eAgMnSc1d1	zpravodajský
analytik	analytik	k1gMnSc1	analytik
amerického	americký	k2eAgNnSc2d1	americké
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
během	během	k7c2	během
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
předal	předat	k5eAaPmAgMnS	předat
Izraeli	Izrael	k1gInSc6	Izrael
asi	asi	k9	asi
1000	[number]	k4	1000
dokumentů	dokument	k1gInPc2	dokument
označených	označený	k2eAgInPc2d1	označený
jako	jako	k8xS	jako
Tajné	tajný	k1gMnPc4	tajný
či	či	k8xC	či
Přísně	přísně	k6eAd1	přísně
tajné	tajný	k2eAgFnPc1d1	tajná
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dokumentů	dokument	k1gInPc2	dokument
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
přímo	přímo	k6eAd1	přímo
netýkaly	týkat	k5eNaImAgFnP	týkat
izraelské	izraelský	k2eAgMnPc4d1	izraelský
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
díky	díky	k7c3	díky
některým	některý	k3yIgMnPc3	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dokonce	dokonce	k9	dokonce
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
prozrazení	prozrazení	k1gNnSc3	prozrazení
totožnosti	totožnost	k1gFnSc2	totožnost
agentů	agent	k1gMnPc2	agent
nasazených	nasazený	k2eAgMnPc2d1	nasazený
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
či	či	k8xC	či
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pollard	Pollard	k1gInSc1	Pollard
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
odsouzen	odsouzen	k2eAgInSc1d1	odsouzen
za	za	k7c4	za
špionáž	špionáž	k1gFnSc4	špionáž
pro	pro	k7c4	pro
cizí	cizí	k2eAgFnSc4d1	cizí
mocnost	mocnost	k1gFnSc4	mocnost
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
byla	být	k5eAaImAgFnS	být
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
izraelském	izraelský	k2eAgNnSc6d1	izraelské
prohlášení	prohlášení	k1gNnSc6	prohlášení
se	se	k3xPyFc4	se
pravilo	pravit	k5eAaImAgNnS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
špionáž	špionáž	k1gFnSc1	špionáž
proti	proti	k7c3	proti
spojenci	spojenec	k1gMnSc3	spojenec
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
neautorizovaná	autorizovaný	k2eNgFnSc1d1	neautorizovaná
a	a	k8xC	a
pirátská	pirátský	k2eAgFnSc1d1	pirátská
akce	akce	k1gFnSc1	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lakam	Lakam	k1gInSc1	Lakam
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ředitelé	ředitel	k1gMnPc1	ředitel
Lakamu	Lakam	k1gInSc2	Lakam
==	==	k?	==
</s>
</p>
<p>
<s>
Binjamin	Binjamin	k2eAgInSc1d1	Binjamin
Blumberg	Blumberg	k1gInSc1	Blumberg
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
-	-	kIx~	-
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rafi	Rafi	k1gNnSc1	Rafi
Ejtan	Ejtana	k1gFnPc2	Ejtana
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
-	-	kIx~	-
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pollardův	Pollardův	k2eAgInSc1d1	Pollardův
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zpráva	zpráva	k1gFnSc1	zpráva
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
o	o	k7c6	o
případu	případ	k1gInSc6	případ
Pollard	Pollarda	k1gFnPc2	Pollarda
</s>
</p>
