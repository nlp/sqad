<s>
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
rekordů	rekord	k1gInPc2
</s>
<s>
Guinnessovy	Guinnessův	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
Guinnessovo	Guinnessův	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Hollywoodu	Hollywood	k1gInSc6
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
Původní	původní	k2eAgFnSc1d1
název	název	k1gInSc4
</s>
<s>
Guinness	Guinness	k6eAd1
World	World	k1gMnSc1
Records	Recordsa	k1gFnPc2
Ilustrátor	ilustrátor	k1gMnSc1
</s>
<s>
Ian	Ian	k?
Bull	bulla	k1gFnPc2
<g/>
,	,	kIx,
Trudi	Trud	k1gMnPc1
Webb	Webb	k1gMnSc1
Obálku	obálka	k1gFnSc4
navrhl	navrhnout	k5eAaPmAgMnS
</s>
<s>
Yeung	Yeung	k1gInSc1
Poon	Poona	k1gFnPc2
Země	zem	k1gFnSc2
</s>
<s>
Irsko	Irsko	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
arabština	arabština	k1gFnSc1
<g/>
,	,	kIx,
brazilsko-portugalština	brazilsko-portugalština	k1gFnSc1
<g/>
,	,	kIx,
portugalština	portugalština	k1gFnSc1
<g/>
,	,	kIx,
čínština	čínština	k1gFnSc1
<g/>
,	,	kIx,
chorvatština	chorvatština	k1gFnSc1
<g/>
,	,	kIx,
čeština	čeština	k1gFnSc1
<g/>
,	,	kIx,
dánština	dánština	k1gFnSc1
<g/>
,	,	kIx,
nizozemština	nizozemština	k1gFnSc1
<g/>
,	,	kIx,
finština	finština	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
<g/>
,	,	kIx,
němčina	němčina	k1gFnSc1
<g/>
,	,	kIx,
řečtina	řečtina	k1gFnSc1
<g/>
,	,	kIx,
hebrejština	hebrejština	k1gFnSc1
<g/>
,	,	kIx,
maďarština	maďarština	k1gFnSc1
<g/>
,	,	kIx,
islandština	islandština	k1gFnSc1
<g/>
,	,	kIx,
italština	italština	k1gFnSc1
<g/>
,	,	kIx,
japonština	japonština	k1gFnSc1
<g/>
,	,	kIx,
norština	norština	k1gFnSc1
<g/>
,	,	kIx,
ruština	ruština	k1gFnSc1
<g/>
,	,	kIx,
slovenština	slovenština	k1gFnSc1
<g/>
,	,	kIx,
slovinština	slovinština	k1gFnSc1
<g/>
,	,	kIx,
španělština	španělština	k1gFnSc1
<g/>
,	,	kIx,
švédština	švédština	k1gFnSc1
a	a	k8xC
turečtina	turečtina	k1gFnSc1
Edice	edice	k1gFnSc2
</s>
<s>
Guinnessovy	Guinnessův	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
Námět	námět	k1gInSc1
</s>
<s>
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
Žánr	žánr	k1gInSc1
</s>
<s>
informace	informace	k1gFnSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Hit	hit	k1gInSc4
Entertainment	Entertainment	k1gInSc1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
1955	#num#	k4
<g/>
-současnost	-současnost	k1gFnSc1
Počet	počet	k1gInSc1
stran	strana	k1gFnPc2
</s>
<s>
320	#num#	k4
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
320	#num#	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
312	#num#	k4
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
320	#num#	k4
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
320	#num#	k4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
320	#num#	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
320	#num#	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
320	#num#	k4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
352	#num#	k4
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
352	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
656	#num#	k4
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
287	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
289	#num#	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
288	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
287	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
287	#num#	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
272	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
256	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
256	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
256	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
256	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
256	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
</s>
<s>
978-1-904994-37-4	978-1-904994-37-4	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Guinnessovy	Guinnessův	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
(	(	kIx(
<g/>
před	před	k7c7
rokem	rok	k1gInSc7
2000	#num#	k4
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
rekordů	rekord	k1gInPc2
a	a	k8xC
ještě	ještě	k9
dříve	dříve	k6eAd2
v	v	k7c6
americké	americký	k2eAgFnSc6d1
edici	edice	k1gFnSc6
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
světových	světový	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
encyklopedie	encyklopedie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
shrnuje	shrnovat	k5eAaImIp3nS
<g/>
,	,	kIx,
zaznamenává	zaznamenávat	k5eAaImIp3nS
a	a	k8xC
kategorizuje	kategorizovat	k5eAaBmIp3nS
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
z	z	k7c2
oblasti	oblast	k1gFnSc2
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
i	i	k8xC
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1951	#num#	k4
sir	sir	k1gMnSc1
Hugh	Hugh	k1gMnSc1
Beaver	Beaver	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
pivovaru	pivovar	k1gInSc2
Guinness	Guinness	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
na	na	k7c4
setkání	setkání	k1gNnPc4
myslivců	myslivec	k1gMnPc2
ve	v	k7c6
Wexfordské	Wexfordský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
u	u	k7c2
řeky	řeka	k1gFnSc2
Slaney	Slanea	k1gFnSc2
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
večírku	večírek	k1gInSc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
sporu	spor	k1gInSc2
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
lovné	lovný	k2eAgNnSc1d1
ptactvo	ptactvo	k1gNnSc1
je	být	k5eAaImIp3nS
nejrychlejší	rychlý	k2eAgMnSc1d3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestli	jestli	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
kulík	kulík	k1gMnSc1
zlatý	zlatý	k1gInSc1
nebo	nebo	k8xC
koroptev	koroptev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
večer	večer	k6eAd1
v	v	k7c6
Castlebridge	Castlebridge	k1gNnSc6
Beaver	Beavra	k1gFnPc2
dospěl	dochvít	k5eAaPmAgInS
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nemožné	možný	k2eNgNnSc1d1
získat	získat	k5eAaPmF
spolehlivý	spolehlivý	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
pro	pro	k7c4
potvrzení	potvrzení	k1gNnSc4
<g/>
,	,	kIx,
jestli	jestli	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
nebo	nebo	k8xC
není	být	k5eNaImIp3nS
kulík	kulík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napadlo	napadnout	k5eAaPmAgNnS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
že	že	k8xS
musí	muset	k5eAaImIp3nS
existovat	existovat	k5eAaImF
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
různých	různý	k2eAgInPc2d1
"	"	kIx"
<g/>
sporů	spor	k1gInPc2
<g/>
"	"	kIx"
a	a	k8xC
sázek	sázka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
během	během	k7c2
nocí	noc	k1gFnPc2
ve	v	k7c6
všech	všecek	k3xTgInPc6
81	#num#	k4
400	#num#	k4
hostincích	hostinec	k1gInPc6
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
nebo	nebo	k8xC
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
neexistuje	existovat	k5eNaImIp3nS
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
sebrány	sebrán	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
potvrdit	potvrdit	k5eAaPmF
nebo	nebo	k8xC
vyvrátit	vyvrátit	k5eAaPmF
argumenty	argument	k1gInPc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
je	být	k5eAaImIp3nS
a	a	k8xC
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
není	být	k5eNaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvědomil	uvědomit	k5eAaPmAgInS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
taková	takový	k3xDgFnSc1
kniha	kniha	k1gFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
velmi	velmi	k6eAd1
lehko	lehko	k6eAd1
stát	stát	k5eAaPmF,k5eAaImF
populární	populární	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Beaverův	Beaverův	k2eAgInSc1d1
nápad	nápad	k1gInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
realizovat	realizovat	k5eAaBmF
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
Christopher	Christophra	k1gFnPc2
Chataway	Chatawaa	k1gFnSc2
<g/>
,	,	kIx,
zaměstnanec	zaměstnanec	k1gMnSc1
pivovaru	pivovar	k1gInSc2
Guinness	Guinnessa	k1gFnPc2
<g/>
,	,	kIx,
doporučil	doporučit	k5eAaPmAgInS
dva	dva	k4xCgMnPc4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
dvojčata	dvojče	k1gNnPc1
Norrise	Norrise	k1gFnSc1
a	a	k8xC
Rosse	Rosse	k1gFnPc1
McWhirterovy	McWhirterův	k2eAgFnPc1d1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
rozběhli	rozběhnout	k5eAaPmAgMnP
v	v	k7c6
Londýně	Londýn	k1gInSc6
informační	informační	k2eAgFnSc4d1
agenturu	agentura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
dva	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
pověření	pověření	k1gNnSc4
shromáždit	shromáždit	k5eAaPmF
informace	informace	k1gFnSc1
pro	pro	k7c4
první	první	k4xOgFnSc4
Guinnessovu	Guinnessův	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
rekordů	rekord	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
srpnu	srpen	k1gInSc6
1954	#num#	k4
v	v	k7c6
nákladu	náklad	k1gInSc6
tisíc	tisíc	k4xCgInPc2
výtisků	výtisk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
má	mít	k5eAaImIp3nS
znaky	znak	k1gInPc4
reklamy	reklama	k1gFnSc2
<g/>
,	,	kIx,
politické	politický	k2eAgFnSc2d1
agitace	agitace	k1gFnSc2
<g/>
,	,	kIx,
propagace	propagace	k1gFnSc2
<g/>
,	,	kIx,
sebepropagace	sebepropagace	k1gFnSc2
apod.	apod.	kA
</s>
<s>
Můžete	moct	k5eAaImIp2nP
pomoci	pomoct	k5eAaPmF
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
obsah	obsah	k1gInSc1
upravíte	upravit	k5eAaPmIp2nP
podle	podle	k7c2
nezaujatého	zaujatý	k2eNgInSc2d1
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
článku	článek	k1gInSc6
lze	lze	k6eAd1
diskutovat	diskutovat	k5eAaImF
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
vyřešení	vyřešení	k1gNnSc2
<g/>
,	,	kIx,
prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
tuto	tento	k3xDgFnSc4
zprávu	zpráva	k1gFnSc4
neodstraňujte	odstraňovat	k5eNaImRp2nP
<g/>
.	.	kIx.
</s>
<s>
Instituce	instituce	k1gFnPc1
GUINNESS	GUINNESS	kA
WORLD	WORLD	kA
REKORDS	REKORDS	kA
LIMITED	limited	k2eAgMnSc1d1
(	(	kIx(
<g/>
vydavatelství	vydavatelství	k1gNnSc1
Guinnessovy	Guinnessův	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
světových	světový	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
<g/>
)	)	kIx)
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Českých	český	k2eAgFnPc2d1
světových	světový	k2eAgFnPc2d1
pozoruhodností	pozoruhodnost	k1gFnPc2
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
knize	kniha	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
obvykle	obvykle	k6eAd1
méně	málo	k6eAd2
než	než	k8xS
pět	pět	k4xCc1
<g/>
,	,	kIx,
proslavil	proslavit	k5eAaPmAgMnS
nás	my	k3xPp1nPc4
např.	např.	kA
Pražský	pražský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
nebo	nebo	k8xC
rekordní	rekordní	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českých	český	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
bývá	bývat	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
méně	málo	k6eAd2
než	než	k8xS
deset	deset	k4xCc4
a	a	k8xC
většinou	většinou	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
slavné	slavný	k2eAgMnPc4d1
sportovce-světové	sportovce-světový	k2eAgMnPc4d1
rekordmany	rekordman	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historicky	historicky	k6eAd1
vítězí	vítězit	k5eAaImIp3nS
atletika	atletika	k1gFnSc1
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
např.	např.	kA
Jan	Jan	k1gMnSc1
Železný	Železný	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
Šebrle	Šebrle	k1gFnSc1
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
Kratochvílová	Kratochvílová	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
Fibingerová	Fibingerová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kapitole	kapitola	k1gFnSc6
fotbal	fotbal	k1gInSc4
se	se	k3xPyFc4
např.	např.	kA
prosadil	prosadit	k5eAaPmAgMnS
brankář	brankář	k1gMnSc1
Petr	Petr	k1gMnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
zajímavostí	zajímavost	k1gFnPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
řadu	řada	k1gFnSc4
let	léto	k1gNnPc2
figuroval	figurovat	k5eAaImAgMnS
v	v	k7c6
knize	kniha	k1gFnSc6
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
nejpopulárnějšího	populární	k2eAgInSc2d3
sportu	sport	k1gInSc2
planety	planeta	k1gFnSc2
světový	světový	k2eAgMnSc1d1
rekordman	rekordman	k1gMnSc1
ve	v	k7c6
fotbalových	fotbalový	k2eAgFnPc6d1
dovednostech	dovednost	k1gFnPc6
Jan	Jan	k1gMnSc1
Skorkovský	Skorkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
dokázal	dokázat	k5eAaPmAgInS
fotbalově	fotbalově	k6eAd1
non-stop	non-stop	k1gInSc1
<g/>
,	,	kIx,
bez	bez	k7c2
doteku	dotek	k1gInSc2
fotbalového	fotbalový	k2eAgInSc2d1
míče	míč	k1gInSc2
se	s	k7c7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
urazit	urazit	k5eAaPmF
klasickou	klasický	k2eAgFnSc4d1
maratónskou	maratónský	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
Praze	Praha	k1gFnSc6
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kouzelník	kouzelník	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Bradáč	Bradáč	k1gMnSc1
je	být	k5eAaImIp3nS
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
českým	český	k2eAgInSc7d1
lamačem	lamač	k1gInSc7
Guinnessových	Guinnessová	k1gFnPc2
rekordů	rekord	k1gInPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
knihy	kniha	k1gFnSc2
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgMnS
již	již	k6eAd1
ve	v	k7c6
dvaceti	dvacet	k4xCc6
pěti	pět	k4xCc6
různých	různý	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgInS
dokonce	dokonce	k9
zařazen	zařadit	k5eAaPmNgMnS
mezi	mezi	k7c7
10	#num#	k4
největších	veliký	k2eAgMnPc2d3
rekordmanů	rekordman	k1gMnPc2
desetiletí	desetiletí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
rekordov	rekordovo	k1gNnPc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Guinness	Guinness	k1gInSc4
World	Worlda	k1gFnPc2
Records	Recordsa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://jablonecky.denik.cz/zpravy_region/bradac-je-nejmladsim-rekordmanem20110224.html	http://jablonecky.denik.cz/zpravy_region/bradac-je-nejmladsim-rekordmanem20110224.html	k1gMnSc1
<g/>
↑	↑	k?
http://tn.nova.cz/zpravy/regionalni/cesi-lamou-guinnessovy-rekordy-nejvice-jich-ma-zdenek-bradac.html	http://tn.nova.cz/zpravy/regionalni/cesi-lamou-guinnessovy-rekordy-nejvice-jich-ma-zdenek-bradac.html	k1gMnSc1
<g/>
↑	↑	k?
Bradáč	Bradáč	k1gMnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejúspěšnější	úspěšný	k2eAgMnPc4d3
rekordmany	rekordman	k1gMnPc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
magazin	magazin	k1gInSc1
<g/>
.	.	kIx.
<g/>
ceskenoviny	ceskenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
<g/>
:	:	kIx,
20.10	20.10	k4
<g/>
.2009	.2009	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
,	,	kIx,
aktualizace	aktualizace	k1gFnSc1
<g/>
:	:	kIx,
20.10	20.10	k4
<g/>
.2009	.2009	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
49	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
rekordů	rekord	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Guinnessových	Guinnessův	k2eAgInPc2d1
světových	světový	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
</s>
<s>
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Největší	veliký	k2eAgInSc4d3
<g/>
,	,	kIx,
nejdelší	dlouhý	k2eAgInSc4d3
<g/>
,	,	kIx,
nejrychlejší	rychlý	k2eAgInSc4d3
<g/>
,	,	kIx,
nejlepší	dobrý	k2eAgInSc4d3
<g/>
...	...	k?
</s>
<s>
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Češi	Čech	k1gMnPc1
v	v	k7c6
Guinnessově	Guinnessův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
rekordů	rekord	k1gInPc2
</s>
<s>
skorkovsky	skorkovsky	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
v	v	k7c6
Guinnessově	Guinnessův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
rekordů	rekord	k1gInPc2
2004	#num#	k4
</s>
<s>
skorkovsky	skorkovsky	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Guinness	Guinness	k1gInSc1
World	World	k1gInSc1
Records	Records	k1gInSc1
2005	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1040250-0	1040250-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1958	#num#	k4
8367	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2007090730	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
159006941	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2007090730	#num#	k4
</s>
