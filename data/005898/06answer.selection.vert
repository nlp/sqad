<s>
Dnes	dnes	k6eAd1	dnes
považujeme	považovat	k5eAaImIp1nP	považovat
za	za	k7c4	za
obecně	obecně	k6eAd1	obecně
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
centrech	centr	k1gInPc6	centr
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
aktivních	aktivní	k2eAgFnPc2d1	aktivní
galaktických	galaktický	k2eAgInPc6d1	galaktický
jádrech	jádro	k1gNnPc6	jádro
<g/>
,	,	kIx,	,
kvasarech	kvasar	k1gInPc6	kvasar
i	i	k8xC	i
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
některých	některý	k3yIgFnPc2	některý
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
