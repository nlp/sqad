<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zabít	zabít	k5eAaPmF	zabít
Sekala	Sekal	k1gMnSc4	Sekal
je	být	k5eAaImIp3nS	být
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
film	film	k1gInSc1	film
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Michálka	Michálek	k1gMnSc2	Michálek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
podle	podle	k7c2	podle
námětu	námět	k1gInSc2	námět
a	a	k8xC	a
scénáře	scénář	k1gInPc4	scénář
Jiřího	Jiří	k1gMnSc4	Jiří
Křižana	Křižan	k1gMnSc4	Křižan
získal	získat	k5eAaPmAgMnS	získat
10	[number]	k4	10
Českých	český	k2eAgMnPc2d1	český
lvů	lev	k1gMnPc2	lev
a	a	k8xC	a
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
českou	český	k2eAgFnSc4d1	Česká
kinematografii	kinematografie	k1gFnSc4	kinematografie
v	v	k7c6	v
klání	klání	k1gNnSc6	klání
o	o	k7c4	o
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
filmu	film	k1gInSc2	film
jsou	být	k5eAaImIp3nP	být
události	událost	k1gFnPc1	událost
na	na	k7c6	na
hanácké	hanácký	k2eAgFnSc6d1	Hanácká
vesnici	vesnice	k1gFnSc6	vesnice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
okupační	okupační	k2eAgNnSc1d1	okupační
drama	drama	k1gNnSc1	drama
je	být	k5eAaImIp3nS	být
však	však	k9	však
zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
poměrně	poměrně	k6eAd1	poměrně
netradičně	tradičně	k6eNd1	tradičně
-	-	kIx~	-
autoři	autor	k1gMnPc1	autor
si	se	k3xPyFc3	se
vypůjčili	vypůjčit	k5eAaPmAgMnP	vypůjčit
vyjadřovací	vyjadřovací	k2eAgInPc4d1	vyjadřovací
prostředky	prostředek	k1gInPc4	prostředek
jak	jak	k8xC	jak
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
chór	chór	k1gInSc1	chór
dvanácti	dvanáct	k4xCc2	dvanáct
vesnických	vesnický	k2eAgMnPc2d1	vesnický
starců	stařec	k1gMnPc2	stařec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
westernu	western	k1gInSc2	western
<g/>
:	:	kIx,	:
nechybí	chybět	k5eNaImIp3nS	chybět
tajemný	tajemný	k2eAgMnSc1d1	tajemný
cizinec	cizinec	k1gMnSc1	cizinec
ani	ani	k8xC	ani
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
souboj	souboj	k1gInSc1	souboj
muže	muž	k1gMnSc2	muž
proti	proti	k7c3	proti
muži	muž	k1gMnSc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
téma	téma	k1gNnSc4	téma
viny	vina	k1gFnSc2	vina
zdánlivě	zdánlivě	k6eAd1	zdánlivě
bezúhonných	bezúhonný	k2eAgMnPc2d1	bezúhonný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
o	o	k7c6	o
německých	německý	k2eAgMnPc6d1	německý
okupantech	okupant	k1gMnPc6	okupant
pouze	pouze	k6eAd1	pouze
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
v	v	k7c6	v
jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
Varvažově	Varvažův	k2eAgInSc6d1	Varvažův
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
začíná	začínat	k5eAaImIp3nS	začínat
příchodem	příchod	k1gInSc7	příchod
valašského	valašský	k2eAgMnSc2d1	valašský
kováře	kovář	k1gMnSc2	kovář
Jury	Jura	k1gMnSc2	Jura
Barana	Baran	k1gMnSc2	Baran
(	(	kIx(	(
<g/>
Olaf	Olaf	k1gMnSc1	Olaf
Lubaszenko	Lubaszenka	k1gFnSc5	Lubaszenka
<g/>
)	)	kIx)	)
do	do	k7c2	do
Lakotic	Lakotice	k1gFnPc2	Lakotice
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ukrýt	ukrýt	k5eAaPmF	ukrýt
před	před	k7c7	před
gestapem	gestapo	k1gNnSc7	gestapo
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
doporučující	doporučující	k2eAgInSc1d1	doporučující
dopis	dopis	k1gInSc1	dopis
pro	pro	k7c4	pro
zdejšího	zdejší	k2eAgMnSc4d1	zdejší
starostu	starosta	k1gMnSc4	starosta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
evangelické	evangelický	k2eAgFnSc3d1	evangelická
víře	víra	k1gFnSc3	víra
ho	on	k3xPp3gMnSc4	on
místní	místní	k2eAgMnPc1d1	místní
hospodáři	hospodář	k1gMnPc1	hospodář
přijímají	přijímat	k5eAaImIp3nP	přijímat
poměrně	poměrně	k6eAd1	poměrně
nepřátelsky	přátelsky	k6eNd1	přátelsky
<g/>
.	.	kIx.	.
</s>
<s>
Baran	Baran	k1gMnSc1	Baran
záhy	záhy	k6eAd1	záhy
poznává	poznávat	k5eAaImIp3nS	poznávat
poměry	poměr	k1gInPc4	poměr
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
-	-	kIx~	-
hospodáři	hospodář	k1gMnPc1	hospodář
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
strachu	strach	k1gInSc6	strach
před	před	k7c7	před
udavačem	udavač	k1gMnSc7	udavač
Ivanem	Ivan	k1gMnSc7	Ivan
Sekalem	Sekal	k1gMnSc7	Sekal
(	(	kIx(	(
<g/>
Bogusław	Bogusław	k1gFnSc1	Bogusław
Linda	Linda	k1gFnSc1	Linda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
od	od	k7c2	od
okupantů	okupant	k1gMnPc2	okupant
získává	získávat	k5eAaImIp3nS	získávat
grunty	grunt	k1gInPc4	grunt
zatčených	zatčený	k2eAgMnPc2d1	zatčený
sousedů	soused	k1gMnPc2	soused
<g/>
.	.	kIx.	.
</s>
<s>
Sedláci	Sedlák	k1gMnPc1	Sedlák
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
využít	využít	k5eAaPmF	využít
Baranovy	Baranův	k2eAgFnPc4d1	Baranova
svízelné	svízelný	k2eAgFnPc4d1	svízelná
situace	situace	k1gFnPc4	situace
(	(	kIx(	(
<g/>
starosta	starosta	k1gMnSc1	starosta
z	z	k7c2	z
dopisu	dopis	k1gInSc2	dopis
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
<g/>
)	)	kIx)	)
a	a	k8xC	a
vydíráním	vydírání	k1gNnSc7	vydírání
ho	on	k3xPp3gMnSc4	on
přinutit	přinutit	k5eAaPmF	přinutit
k	k	k7c3	k
vraždě	vražda	k1gFnSc3	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sekalova	Sekalův	k2eAgFnSc1d1	Sekalův
zrůdnost	zrůdnost	k1gFnSc1	zrůdnost
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
ponižování	ponižování	k1gNnSc2	ponižování
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
dostalo	dostat	k5eAaPmAgNnS	dostat
pro	pro	k7c4	pro
nemanželský	manželský	k2eNgInSc4d1	nemanželský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
na	na	k7c4	na
nože	nůž	k1gInPc4	nůž
Baran	Baran	k1gMnSc1	Baran
Sekala	Sekal	k1gMnSc2	Sekal
skutečně	skutečně	k6eAd1	skutečně
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
však	však	k9	však
těžce	těžce	k6eAd1	těžce
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Sedláci	Sedlák	k1gMnPc1	Sedlák
mu	on	k3xPp3gMnSc3	on
odmítnou	odmítnout	k5eAaPmIp3nP	odmítnout
poskytnout	poskytnout	k5eAaPmF	poskytnout
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
nechávají	nechávat	k5eAaImIp3nP	nechávat
ho	on	k3xPp3gMnSc4	on
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
ocenění	ocenění	k1gNnSc4	ocenění
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
v	v	k7c6	v
deseti	deset	k4xCc6	deset
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
držel	držet	k5eAaImAgMnS	držet
rekord	rekord	k1gInSc4	rekord
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
film	film	k1gInSc1	film
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
získal	získat	k5eAaPmAgInS	získat
11	[number]	k4	11
cen	cena	k1gFnPc2	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
za	za	k7c4	za
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Křižan	Křižan	k1gMnSc1	Křižan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Olaf	Olaf	k1gInSc1	Olaf
Lubaszenko	Lubaszenka	k1gFnSc5	Lubaszenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Sitek	Sitky	k1gFnPc2	Sitky
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kameru	kamera	k1gFnSc4	kamera
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
Štrba	Štrba	k1gFnSc1	Štrba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Lorenc	Lorenc	k1gMnSc1	Lorenc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Brožek	Brožek	k1gMnSc1	Brožek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
(	(	kIx(	(
<g/>
Radim	Radim	k1gMnSc1	Radim
Hladík	Hladík	k1gMnSc1	Hladík
ml.	ml.	kA	ml.
<g/>
)	)	kIx)	)
a	a	k8xC	a
výtvarný	výtvarný	k2eAgInSc1d1	výtvarný
počin	počin	k1gInSc1	počin
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Sternwald	Sternwald	k1gMnSc1	Sternwald
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zabít	zabít	k5eAaPmF	zabít
Sekala	Sekal	k1gMnSc4	Sekal
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zabít	zabít	k5eAaPmF	zabít
Sekala	Sekal	k1gMnSc4	Sekal
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
