<s>
Producentem	producent	k1gMnSc7	producent
jejich	jejich	k3xOp3gNnSc2	jejich
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Phantom	Phantom	k1gInSc1	Phantom
Agony	agon	k1gInPc1	agon
byl	být	k5eAaImAgMnS	být
Sascha	Sascha	k1gMnSc1	Sascha
Paeth	Paeth	k1gMnSc1	Paeth
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
producentem	producent	k1gMnSc7	producent
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xC	jako
Angra	Angro	k1gNnSc2	Angro
<g/>
,	,	kIx,	,
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
of	of	k?	of
Fire	Fire	k1gInSc1	Fire
<g/>
,	,	kIx,	,
Kamelot	kamelot	k1gInSc1	kamelot
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
