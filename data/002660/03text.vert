<s>
Sunnité	sunnita	k1gMnPc1	sunnita
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
po	po	k7c6	po
Mohamedově	Mohamedův	k2eAgFnSc6d1	Mohamedova
smrti	smrt	k1gFnSc6	smrt
uznali	uznat	k5eAaPmAgMnP	uznat
za	za	k7c4	za
nástupce	nástupce	k1gMnSc4	nástupce
Abú	abú	k1gMnSc4	abú
Bakra	Bakr	k1gMnSc4	Bakr
a	a	k8xC	a
oddělili	oddělit	k5eAaPmAgMnP	oddělit
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
šíitů	šíita	k1gMnPc2	šíita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
za	za	k7c4	za
nástupce	nástupce	k1gMnSc4	nástupce
považovali	považovat	k5eAaImAgMnP	považovat
člena	člen	k1gMnSc4	člen
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
-	-	kIx~	-
zetě	zeť	k1gMnSc2	zeť
Alího	Alí	k1gMnSc2	Alí
<g/>
.	.	kIx.	.
</s>
<s>
Šiíté	Šiíta	k1gMnPc1	Šiíta
a	a	k8xC	a
sunnité	sunnita	k1gMnPc1	sunnita
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
větve	větev	k1gFnPc1	větev
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Sunnité	sunnita	k1gMnPc1	sunnita
tvoří	tvořit	k5eAaImIp3nP	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
75	[number]	k4	75
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
muslimských	muslimský	k2eAgInPc6d1	muslimský
státech	stát	k1gInPc6	stát
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
,	,	kIx,	,
Ománu	Omán	k1gInSc2	Omán
a	a	k8xC	a
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
muslimských	muslimský	k2eAgFnPc2d1	muslimská
přistěhovanců	přistěhovanců	k?	přistěhovanců
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
patří	patřit	k5eAaImIp3nS	patřit
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
větvi	větev	k1gFnSc3	větev
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označený	k2eAgMnPc1d1	označený
sunnité	sunnita	k1gMnPc1	sunnita
je	on	k3xPp3gNnSc4	on
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
arabského	arabský	k2eAgNnSc2d1	arabské
slova	slovo	k1gNnSc2	slovo
sunna	sunna	k1gFnSc1	sunna
(	(	kIx(	(
<g/>
zvyk	zvyk	k1gInSc1	zvyk
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
používaného	používaný	k2eAgInSc2d1	používaný
pro	pro	k7c4	pro
způsob	způsob	k1gInSc4	způsob
Mohamedova	Mohamedův	k2eAgInSc2d1	Mohamedův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Sunna	sunna	k1gFnSc1	sunna
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
živý	živý	k2eAgInSc4d1	živý
Korán	korán	k1gInSc4	korán
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
islámské	islámský	k2eAgNnSc4d1	islámské
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
etiku	etika	k1gFnSc4	etika
<g/>
.	.	kIx.	.
</s>
<s>
Sunna	sunna	k1gFnSc1	sunna
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
uznávána	uznáván	k2eAgFnSc1d1	uznávána
i	i	k8xC	i
druhou	druhý	k4xOgFnSc7	druhý
hlavní	hlavní	k2eAgFnSc7d1	hlavní
islámskou	islámský	k2eAgFnSc7d1	islámská
větví	větev	k1gFnSc7	větev
-	-	kIx~	-
šíity	šíita	k1gMnSc2	šíita
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
sbírky	sbírka	k1gFnPc4	sbírka
hadísů	hadís	k1gInPc2	hadís
(	(	kIx(	(
<g/>
výroky	výrok	k1gInPc1	výrok
a	a	k8xC	a
činy	čin	k1gInPc1	čin
Mohameda	Mohamed	k1gMnSc2	Mohamed
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
i	i	k9	i
jednání	jednání	k1gNnSc3	jednání
celé	celá	k1gFnSc2	celá
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
nástupců	nástupce	k1gMnPc2	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
muslimů	muslim	k1gMnPc2	muslim
na	na	k7c4	na
sunnity	sunnita	k1gMnPc4	sunnita
a	a	k8xC	a
šíity	šíita	k1gMnPc4	šíita
nebylo	být	k5eNaImAgNnS	být
okamžité	okamžitý	k2eAgNnSc1d1	okamžité
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
sunnity	sunnita	k1gMnSc2	sunnita
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
ti	ten	k3xDgMnPc1	ten
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
632	[number]	k4	632
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
10	[number]	k4	10
podle	podle	k7c2	podle
islámského	islámský	k2eAgInSc2d1	islámský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Mohameda	Mohamed	k1gMnSc4	Mohamed
uznali	uznat	k5eAaPmAgMnP	uznat
za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
nástupce	nástupce	k1gMnSc4	nástupce
Abú	abú	k1gMnSc4	abú
Bakra	Bakr	k1gMnSc4	Bakr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
protipólem	protipól	k1gInSc7	protipól
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
šíité	šíita	k1gMnPc1	šíita
(	(	kIx(	(
<g/>
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
Alí	Alí	k1gFnSc1	Alí
-	-	kIx~	-
strana	strana	k1gFnSc1	strana
Alího	Alí	k1gMnSc2	Alí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
titul	titul	k1gInSc1	titul
chalífy	chalífa	k1gMnSc2	chalífa
by	by	kYmCp3nP	by
měl	mít	k5eAaImAgInS	mít
dědičně	dědičně	k6eAd1	dědičně
připadnout	připadnout	k5eAaPmF	připadnout
Mohamedově	Mohamedův	k2eAgFnSc3d1	Mohamedova
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
jeho	jeho	k3xOp3gFnSc3	jeho
zeti	zet	k5eAaImF	zet
Alímu	Alíma	k1gFnSc4	Alíma
<g/>
.	.	kIx.	.
</s>
<s>
Alí	Alí	k?	Alí
sám	sám	k3xTgMnSc1	sám
proti	proti	k7c3	proti
volbě	volba	k1gFnSc3	volba
Abú	abú	k1gMnSc1	abú
Bakra	Bakra	k1gMnSc1	Bakra
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodnikal	podnikat	k5eNaImAgMnS	podnikat
žádné	žádný	k3yNgInPc4	žádný
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
uchopení	uchopení	k1gNnSc3	uchopení
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
jej	on	k3xPp3gInSc4	on
za	za	k7c4	za
chalífu	chalífa	k1gMnSc4	chalífa
uznal	uznat	k5eAaPmAgInS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Chalífou	chalífa	k1gMnSc7	chalífa
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
656	[number]	k4	656
skutečně	skutečně	k6eAd1	skutečně
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
661	[number]	k4	661
byl	být	k5eAaImAgInS	být
také	také	k9	také
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
cháridžovským	cháridžovský	k1gMnSc7	cháridžovský
povstalcem	povstalec	k1gMnSc7	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
dědičná	dědičný	k2eAgFnSc1d1	dědičná
dynastie	dynastie	k1gFnSc1	dynastie
Umajjovců	Umajjovec	k1gInPc2	Umajjovec
a	a	k8xC	a
islámské	islámský	k2eAgNnSc1d1	islámské
schizma	schizma	k1gNnSc1	schizma
bylo	být	k5eAaImAgNnS	být
dovršeno	dovršit	k5eAaPmNgNnS	dovršit
<g/>
.	.	kIx.	.
</s>
