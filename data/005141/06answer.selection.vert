<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
Fobos	Fobos	k1gInSc1	Fobos
2	[number]	k4	2
stala	stát	k5eAaPmAgFnS	stát
kvazisatelitem	kvazisatelit	k1gInSc7	kvazisatelit
Phobosu	Phobos	k1gInSc2	Phobos
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc4	měsíc
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
poloosou	poloosa	k1gFnSc7	poloosa
dráhy	dráha	k1gFnSc2	dráha
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
Phobosu	Phobos	k1gInSc2	Phobos
<g/>
.	.	kIx.	.
</s>
