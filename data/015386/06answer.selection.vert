<s>
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
נ	נ	k?
<g/>
ֵ	ֵ	k?
<g/>
י	י	k?
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
ר	ר	k?
<g/>
ַ	ַ	k?
<g/>
ק	ק	k?
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
Bene	bene	k6eAd1
Beraq	Beraq	k1gMnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
přepisováno	přepisován	k2eAgNnSc1d1
též	též	k9
Bnei	Bnei	k1gNnSc1
Brak	braka	k1gFnPc2
nebo	nebo	k8xC
Bene	bene	k6eAd1
Berak	Berak	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
v	v	k7c6
Telavivském	telavivský	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
.	.	kIx.
</s>