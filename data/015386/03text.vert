<s>
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
</s>
<s>
Bnej	Bnej	k1gInSc1
Brak	brak	k1gInSc1
ב	ב	k?
ב	ב	k?
Radnice	radnice	k1gFnSc1
v	v	k7c4
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
34	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
10	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
distrikt	distrikt	k1gInSc1
</s>
<s>
Telavivský	telavivský	k2eAgInSc1d1
</s>
<s>
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
7,343	7,343	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
193	#num#	k4
800	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
26	#num#	k4
368,4	368,4	k4
(	(	kIx(
<g/>
r.	r.	kA
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1924	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.bnei-brak.muni.il	www.bnei-brak.muni.il	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
נ	נ	k?
<g/>
ֵ	ֵ	k?
<g/>
י	י	k?
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
ר	ר	k?
<g/>
ַ	ַ	k?
<g/>
ק	ק	k?
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
Bene	bene	k6eAd1
Beraq	Beraq	k1gMnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
přepisováno	přepisován	k2eAgNnSc1d1
též	též	k9
Bnei	Bnei	k1gNnSc1
Brak	braka	k1gFnPc2
nebo	nebo	k8xC
Bene	bene	k6eAd1
Berak	Berak	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
v	v	k7c6
Telavivském	telavivský	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
30	#num#	k4
m	m	kA
v	v	k7c6
Izraelské	izraelský	k2eAgFnSc6d1
pobřežní	pobřežní	k2eAgFnSc6d1
planině	planina	k1gFnSc6
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
6	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
telavivské	telavivský	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
a	a	k8xC
na	na	k7c6
všech	všecek	k3xTgFnPc6
stranách	strana	k1gFnPc6
souvisle	souvisle	k6eAd1
obklopeno	obklopit	k5eAaPmNgNnS
zastavěným	zastavěný	k2eAgNnSc7d1
územím	území	k1gNnSc7
dalších	další	k2eAgNnPc2d1
měst	město	k1gNnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
této	tento	k3xDgFnSc2
aglomerace	aglomerace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Ramat	Ramat	k2eAgMnSc1d1
Gan	Gan	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
Petach	Petach	k1gInSc1
Tikva	Tikvo	k1gNnSc2
a	a	k8xC
Giv	Giv	k1gFnSc2
<g/>
'	'	kIx"
<g/>
at	at	k?
Šmu	Šmu	k1gFnSc1
<g/>
'	'	kIx"
<g/>
el.	el.	k?
Pouze	pouze	k6eAd1
na	na	k7c6
severu	sever	k1gInSc6
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
hranici	hranice	k1gFnSc4
města	město	k1gNnSc2
s	s	k7c7
Tel	tel	kA
Avivem	Aviv	k1gInSc7
v	v	k7c4
jinak	jinak	k6eAd1
souvisle	souvisle	k6eAd1
zastavěné	zastavěný	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
klín	klín	k1gInSc4
údolí	údolí	k1gNnSc2
řeky	řeka	k1gFnSc2
Jarkon	Jarkona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
hustě	hustě	k6eAd1
osídlené	osídlený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
etnicky	etnicky	k6eAd1
zcela	zcela	k6eAd1
židovská	židovský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
Bnej	Bnej	k1gMnSc1
Brak	brak	k1gInSc1
<g/>
:	:	kIx,
P	P	kA
<g/>
:	:	kIx,
Ponevežská	Ponevežský	k2eAgFnSc1d1
ješiva	ješiva	k1gFnSc1
•	•	k?
S	s	k7c7
<g/>
:	:	kIx,
ješiva	ješiva	k1gFnSc1
Slobodka	Slobodka	k1gFnSc1
•	•	k?
C	C	kA
<g/>
:	:	kIx,
kolel	kolel	k1gMnSc1
Chazona	Chazon	k1gMnSc2
Iše	Iše	k1gFnSc2
•	•	k?
L	L	kA
<g/>
:	:	kIx,
ješiva	ješiva	k1gFnSc1
Lublinských	lublinský	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
(	(	kIx(
<g/>
Chachmej	Chachmej	k1gInSc1
Lublin	Lublin	k1gInSc1
<g/>
)	)	kIx)
T	T	kA
<g/>
:	:	kIx,
ješiva	ješiva	k1gFnSc1
Nádhera	nádhera	k1gFnSc1
Sionu	Sion	k1gInSc2
(	(	kIx(
<g/>
Tif	Tif	k1gMnSc1
<g/>
'	'	kIx"
<g/>
eret	eret	k1gMnSc1
Cijon	Cijon	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c4
dopravní	dopravní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
napojeno	napojen	k2eAgNnSc1d1
pomocí	pomocí	k7c2
četných	četný	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
aglomerace	aglomerace	k1gFnSc2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
dálnice	dálnice	k1gFnSc1
číslo	číslo	k1gNnSc1
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
severním	severní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
města	město	k1gNnSc2
rovněž	rovněž	k9
prochází	procházet	k5eAaImIp3nS
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Tel	tel	kA
Aviv	Avivum	k1gNnPc2
–	–	k?
Ra	ra	k0
<g/>
'	'	kIx"
<g/>
anana	anano	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funguje	fungovat	k5eAaImIp3nS
tu	tu	k6eAd1
železniční	železniční	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Bnej	Bnej	k1gMnSc1
Brak	brak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
v	v	k7c6
r.	r.	kA
1928	#num#	k4
</s>
<s>
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1924	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jméno	jméno	k1gNnSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
biblické	biblický	k2eAgNnSc4d1
město	město	k1gNnSc4
Bené-berak	Bené-berak	k1gInSc1
citované	citovaný	k2eAgFnSc2d1
v	v	k7c6
knize	kniha	k1gFnSc6
Jozue	Jozue	k1gMnSc1
jako	jako	k8xS,k8xC
jedno	jeden	k4xCgNnSc1
z	z	k7c2
měst	město	k1gNnPc2
v	v	k7c6
podílu	podíl	k1gInSc6
kmene	kmen	k1gInSc2
Dan	Dan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Starověké	starověký	k2eAgNnSc1d1
město	město	k1gNnSc1
leželo	ležet	k5eAaImAgNnS
asi	asi	k9
5	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
stála	stát	k5eAaImAgFnS
arabská	arabský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
nazvaná	nazvaný	k2eAgFnSc1d1
Ibn	Ibn	k1gFnSc1
Abrák	Abrák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
také	také	k9
získány	získán	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
pro	pro	k7c4
založení	založení	k1gNnSc4
novodobého	novodobý	k2eAgNnSc2d1
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sederová	Sederový	k2eAgFnSc1d1
hostina	hostina	k1gFnSc1
rabi	rabi	k1gMnSc1
Akivy	Akiva	k1gFnPc4
v	v	k7c4
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
vyobrazená	vyobrazený	k2eAgFnSc1d1
v	v	k7c6
Pesachové	Pesachové	k2eAgInSc6d1
hagadě	hagad	k1gInSc6
z	z	k7c2
r.	r.	kA
1740	#num#	k4
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
antiky	antika	k1gFnSc2
si	se	k3xPyFc3
Bnej	Bnej	k1gMnSc1
Brak	braka	k1gFnPc2
získalo	získat	k5eAaPmAgNnS
pověst	pověst	k1gFnSc4
centra	centrum	k1gNnSc2
rabínské	rabínský	k2eAgFnSc2d1
učenosti	učenost	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
sídlil	sídlit	k5eAaImAgMnS
rabi	rabi	k1gMnSc1
Akiva	Akivo	k1gNnSc2
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gMnSc7
jeho	jeho	k3xOp3gFnSc4
významní	významný	k2eAgMnPc1d1
žáci	žák	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
rabi	rabi	k1gMnSc1
Šim	Šim	k1gMnSc1
<g/>
'	'	kIx"
<g/>
on	on	k3xPp3gMnSc1
bar	bar	k1gInSc1
Jochaj	Jochaj	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známá	známá	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
pasáž	pasáž	k1gFnSc1
z	z	k7c2
Pesachové	Pesachový	k2eAgFnSc2d1
hagady	hagada	k1gFnSc2
popisující	popisující	k2eAgFnSc4d1
učenou	učený	k2eAgFnSc4d1
rozmluvu	rozmluva	k1gFnSc4
rabi	rabi	k1gMnSc1
Akivy	Akiva	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
při	při	k7c6
sederové	sederový	k2eAgFnSc6d1
hostině	hostina	k1gFnSc6
držené	držený	k2eAgFnSc6d1
v	v	k7c4
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
získalo	získat	k5eAaPmAgNnS
status	status	k1gInSc4
místní	místní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
malého	malý	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1949	#num#	k4
bylo	být	k5eAaImAgNnS
povýšeno	povýšit	k5eAaPmNgNnS
na	na	k7c4
město	město	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgNnSc7d1
izraelským	izraelský	k2eAgNnSc7d1
velkoměstem	velkoměsto	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nP
ultraortodoxní	ultraortodoxní	k2eAgMnPc1d1
Židé	Žid	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
podíl	podíl	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
odhadoval	odhadovat	k5eAaImAgMnS
na	na	k7c6
80	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
pomyslně	pomyslně	k6eAd1
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
části	část	k1gFnPc4
podle	podle	k7c2
příslušníků	příslušník	k1gMnPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
chasidských	chasidský	k2eAgInPc2d1
dvorů	dvůr	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
v	v	k7c6
nich	on	k3xPp3gInPc6
žijí	žít	k5eAaImIp3nP
(	(	kIx(
<g/>
např.	např.	kA
Vižničtí	Vižnický	k2eAgMnPc1d1
chasidé	chasid	k1gMnPc1
<g/>
,	,	kIx,
Belzští	Belzský	k2eAgMnPc1d1
chasidé	chasid	k1gMnPc1
atp.	atp.	kA
<g/>
)	)	kIx)
Vzhledem	vzhledem	k7c3
k	k	k7c3
povaze	povaha	k1gFnSc3
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
ve	v	k7c6
městě	město	k1gNnSc6
žije	žít	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
velká	velký	k2eAgFnSc1d1
koncentrace	koncentrace	k1gFnSc1
<g/>
,	,	kIx,
synagog	synagoga	k1gFnPc2
<g/>
,	,	kIx,
ješiv	ješiva	k1gFnPc2
<g/>
,	,	kIx,
kolelů	kolel	k1gInPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
náboženských	náboženský	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
koncentrace	koncentrace	k1gFnSc1
ultraortodoxních	ultraortodoxní	k2eAgMnPc2d1
židů	žid	k1gMnPc2
v	v	k7c6
tomto	tento	k3xDgInSc6
městě	město	k1gNnSc6
dává	dávat	k5eAaImIp3nS
Bnej	Bnej	k1gMnSc1
Braku	brak	k1gInSc2
osobitý	osobitý	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Bnej	Bnej	k1gInSc4
Brak	braka	k1gFnPc2
například	například	k6eAd1
nenajdeme	najít	k5eNaPmIp1nP
kina	kino	k1gNnPc1
<g/>
,	,	kIx,
divadla	divadlo	k1gNnPc1
<g/>
,	,	kIx,
drahé	drahý	k2eAgFnPc1d1
módní	módní	k2eAgFnPc4d1
restaurace	restaurace	k1gFnPc4
<g/>
,	,	kIx,
butiky	butik	k1gInPc4
s	s	k7c7
moderním	moderní	k2eAgNnSc7d1
oblečením	oblečení	k1gNnSc7
atp.	atp.	kA
Během	během	k7c2
šabatu	šabat	k1gInSc2
a	a	k8xC
židovských	židovský	k2eAgInPc2d1
svátků	svátek	k1gInPc2
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
přísně	přísně	k6eAd1
dodržovány	dodržován	k2eAgInPc1d1
zákazy	zákaz	k1gInPc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
například	například	k6eAd1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
zavřeny	zavřít	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
obchody	obchod	k1gInPc1
a	a	k8xC
ve	v	k7c6
městě	město	k1gNnSc6
je	být	k5eAaImIp3nS
přísně	přísně	k6eAd1
dodržován	dodržovat	k5eAaImNgInS
zákaz	zákaz	k1gInSc1
vjezdu	vjezd	k1gInSc2
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Bnej	Bnej	k1gInSc4
Braku	brak	k1gInSc2
najdeme	najít	k5eAaPmIp1nP
také	také	k9
jediný	jediný	k2eAgInSc4d1
obchodní	obchodní	k2eAgInSc4d1
dům	dům	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgInSc2
mají	mít	k5eAaImIp3nP
přístup	přístup	k1gInSc4
pouze	pouze	k6eAd1
ženy	žena	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
má	mít	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc4d1
sekulární	sekulární	k2eAgFnSc4d1
čtvrť	čtvrť	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
města	město	k1gNnSc2
a	a	k8xC
nese	nést	k5eAaImIp3nS
název	název	k1gInSc4
Pardes	Pardes	k1gMnSc1
Kac	Kac	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS
tu	tu	k6eAd1
velká	velký	k2eAgFnSc1d1
Nemocnice	nemocnice	k1gFnSc1
Ma	Ma	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ajnej	ajnej	k1gInSc1
ha-ješua	ha-ješu	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
naprostou	naprostý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
obyvatel	obyvatel	k1gMnSc1
Židé	Žid	k1gMnPc1
–	–	k?
přibližně	přibližně	k6eAd1
152	#num#	k4
400	#num#	k4
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
statistické	statistický	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
ostatní	ostatní	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
nearabské	arabský	k2eNgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
ale	ale	k8xC
bez	bez	k7c2
formální	formální	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
židovskému	židovský	k2eAgNnSc3d1
náboženství	náboženství	k1gNnSc3
<g/>
,	,	kIx,
154	#num#	k4
400	#num#	k4
osob	osoba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
Centrálního	centrální	k2eAgInSc2d1
statistického	statistický	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
(	(	kIx(
<g/>
CBS	CBS	kA
<g/>
)	)	kIx)
žilo	žít	k5eAaImAgNnS
v	v	k7c4
Bnej	Bnej	k1gInSc4
Brak	braka	k1gFnPc2
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc3
2017	#num#	k4
193	#num#	k4
800	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
město	město	k1gNnSc1
s	s	k7c7
nejdelší	dlouhý	k2eAgFnSc7d3
průměrnou	průměrný	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
života	život	k1gInSc2
u	u	k7c2
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
81,1	81,1	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
i	i	k9
mužů	muž	k1gMnPc2
(	(	kIx(
<g/>
77,4	77,4	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
má	mít	k5eAaImIp3nS
také	také	k9
nejvyšší	vysoký	k2eAgFnSc4d3
hustotu	hustota	k1gFnSc4
zalidnění	zalidnění	k1gNnSc2
ze	z	k7c2
všech	všecek	k3xTgNnPc2
izraelských	izraelský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeden	jeden	k4xCgInSc4
kilometr	kilometr	k1gInSc4
čtvereční	čtvereční	k2eAgFnSc2d1
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
víc	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
ale	ale	k8xC
přesto	přesto	k8xC
stále	stále	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jisté	jistý	k2eAgFnSc6d1
stagnaci	stagnace	k1gFnSc6
koncem	koncem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
letech	léto	k1gNnPc6
2000-2009	2000-2009	k4
zvýšil	zvýšit	k5eAaPmAgInS
o	o	k7c6
dalších	další	k2eAgInPc6d1
20	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ultraortodoxní	ultraortodoxní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
Bnej	Bnej	k1gFnSc4
Braku	brak	k1gInSc2
zároveň	zároveň	k6eAd1
pronikají	pronikat	k5eAaImIp3nP
do	do	k7c2
sousedních	sousední	k2eAgFnPc2d1
čtvrtí	čtvrt	k1gFnPc2
města	město	k1gNnSc2
Ramat	Ramat	k2eAgMnSc1d1
Gan	Gan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
i	i	k9
podíl	podíl	k1gInSc1
ultraortodoxních	ultraortodoxní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
dosud	dosud	k6eAd1
sekulární	sekulární	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Pardes	Pardes	k1gMnSc1
Kac	Kac	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
starosta	starosta	k1gMnSc1
Bnej	Bnej	k1gMnSc1
Braku	brak	k1gInSc2
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akov	akov	k1gMnSc1
Ašer	Ašer	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
maximálního	maximální	k2eAgNnSc2d1
možného	možný	k2eAgNnSc2d1
využití	využití	k1gNnSc2
zastavěných	zastavěný	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
a	a	k8xC
že	že	k8xS
město	město	k1gNnSc1
připravuje	připravovat	k5eAaImIp3nS
jen	jen	k6eAd1
poslední	poslední	k2eAgInPc4d1
dva	dva	k4xCgInPc4
nebo	nebo	k8xC
tři	tři	k4xCgInPc4
obytné	obytný	k2eAgInPc4d1
soubory	soubor	k1gInPc4
o	o	k7c6
kapacitě	kapacita	k1gFnSc6
750	#num#	k4
bytových	bytový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
se	se	k3xPyFc4
v	v	k7c6
dekádě	dekáda	k1gFnSc6
2007	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
populace	populace	k1gFnSc2
města	město	k1gNnSc2
zahušťováním	zahušťování	k1gNnPc3
osídlení	osídlení	k1gNnSc2
zvýšila	zvýšit	k5eAaPmAgFnS
z	z	k7c2
přibližně	přibližně	k6eAd1
150	#num#	k4
000	#num#	k4
na	na	k7c4
téměř	téměř	k6eAd1
195	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatelData	obyvatelDat	k1gMnSc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
Wikidat	Wikidat	k1gFnSc2
</s>
<s>
RokObyvatelé	RokObyvatel	k1gMnPc1
<g/>
19489	#num#	k4
300195014	#num#	k4
300195118	#num#	k4
000195220	#num#	k4
500195322	#num#	k4
500195425	#num#	k4
000195528	#num#	k4
000195632	#num#	k4
700195734	#num#	k4
300195838	#num#	k4
300	#num#	k4
<g/>
RokObyvatelé	RokObyvatel	k1gMnPc1
<g/>
195941	#num#	k4
600196045	#num#	k4
200196147	#num#	k4
000196251	#num#	k4
700196354	#num#	k4
100196457	#num#	k4
300196560	#num#	k4
400196663	#num#	k4
100196764	#num#	k4
700196867	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
RokObyvatelé	RokObyvatel	k1gMnPc1
<g/>
196969	#num#	k4
700197072	#num#	k4
100197174	#num#	k4
200197275	#num#	k4
700197379	#num#	k4
300197481	#num#	k4
000197583	#num#	k4
000197684	#num#	k4
400197785	#num#	k4
900197887	#num#	k4
300	#num#	k4
<g/>
RokObyvatelé	RokObyvatel	k1gMnPc1
<g/>
197989	#num#	k4
600198091	#num#	k4
400198192	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
800198294	#num#	k4
800198396	#num#	k4
1001984100	#num#	k4
4001985102	#num#	k4
4001986104	#num#	k4
7001987107	#num#	k4
4001988109	#num#	k4
400	#num#	k4
<g/>
RokObyvatelé	RokObyvatel	k1gMnPc1
<g/>
1989111	#num#	k4
8001990116	#num#	k4
7001991121	#num#	k4
2001992124	#num#	k4
4001993125	#num#	k4
0001994127	#num#	k4
1001995128	#num#	k4
6001996130	#num#	k4
5001997133	#num#	k4
9001998133	#num#	k4
900	#num#	k4
<g/>
RokObyvatelé	RokObyvatel	k1gMnPc1
<g/>
1999134	#num#	k4
7002000136	#num#	k4
9002001139	#num#	k4
0002002138	#num#	k4
9002003139	#num#	k4
6002004142	#num#	k4
3002005145	#num#	k4
1002006147	#num#	k4
9002007151	#num#	k4
0002008153	#num#	k4
300	#num#	k4
<g/>
RokObyvatelé	RokObyvatel	k1gMnPc1
<g/>
2009154	#num#	k4
4002010158	#num#	k4
9002011163	#num#	k4
3002012168	#num#	k4
8002013172	#num#	k4
5002014178	#num#	k4
3002015182	#num#	k4
8002016189	#num#	k4
0002017193	#num#	k4
8002018193	#num#	k4
774	#num#	k4
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Ulice	ulice	k1gFnSc1
Rabi	rabi	k1gMnSc1
Akivy	Akiva	k1gFnPc4
v	v	k7c4
Bnej	Bnej	k1gInSc4
Brak	braka	k1gFnPc2
během	během	k7c2
šabatu	šabat	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ustává	ustávat	k5eAaImIp3nS
doprava	doprava	k1gFnSc1
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1
Giv	Giv	k1gFnSc1
<g/>
'	'	kIx"
<g/>
at	at	k?
Sokolov	Sokolov	k1gInSc1
v	v	k7c4
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
</s>
<s>
Válečný	válečný	k2eAgInSc4d1
pomník	pomník	k1gInSc4
v	v	k7c4
Bnej	Bnej	k1gFnSc4
Brak	braka	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
נ	נ	k?
פ	פ	k?
-	-	kIx~
ע	ע	k?
ו	ו	k?
<g/>
.	.	kIx.
<g/>
מ	מ	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
POPULATION	POPULATION	kA
AND	Anda	k1gFnPc2
DENSITY	DENSITY	kA
PER	pero	k1gNnPc2
SQ	SQ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
KM	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
IN	IN	kA
LOCALITIES	LOCALITIES	kA
NUMBERING	NUMBERING	kA
5,000	5,000	k4
RESIDENTS	RESIDENTS	kA
AND	Anda	k1gFnPc2
MORE	mor	k1gInSc5
ON	on	k3xPp3gMnSc1
31.12	31.12	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročenka	ročenka	k1gFnSc1
Centrálního	centrální	k2eAgInSc2d1
statistického	statistický	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
י	י	k?
2009	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ו	ו	k?
<g/>
&	&	k?
<g/>
,	,	kIx,
ז	ז	k?
<g/>
.	.	kIx.
א	א	k?
ל	ל	k?
א	א	k?
י	י	k?
<g/>
.	.	kIx.
ת	ת	k?
א	א	k?
<g/>
:	:	kIx,
ע	ע	k?
ע	ע	k?
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
נ	נ	k?
<g/>
ֵ	ֵ	k?
<g/>
י	י	k?
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
ר	ר	k?
<g/>
ַ	ַ	k?
<g/>
ק	ק	k?
<g/>
,	,	kIx,
s.	s.	k?
946	#num#	k4
<g/>
–	–	k?
<g/>
948	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Joz	Joza	k1gFnPc2
19	#num#	k4
<g/>
,	,	kIx,
45	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Bene	bene	k6eAd1
Beraq	Beraq	k1gMnPc1
(	(	kIx(
<g/>
Israel	Israel	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flags	Flags	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Bar	bar	k1gInSc1
Eli	Eli	k1gFnSc2
<g/>
,	,	kIx,
Avi	Avi	k1gFnSc2
<g/>
:	:	kIx,
Natural	Natural	k?
expansion	expansion	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haaretz	Haaretz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
www.jewishmediaresources.com	www.jewishmediaresources.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.jewishmediaresources.com	www.jewishmediaresources.com	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Greenwald	Greenwald	k1gInSc1
<g/>
,	,	kIx,
Shlomo	Shloma	k1gFnSc5
<g/>
:	:	kIx,
Bnei	Bnei	k1gNnPc1
Brak	braka	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
City	city	k1gNnSc1
Worth	Wortha	k1gFnPc2
Building	Building	k1gInSc1
For	forum	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Jewish	Jewish	k1gMnSc1
Press	Press	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bnej	Bnej	k1gInSc1
Brak	brak	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Telavivský	telavivský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
Města	město	k1gNnSc2
</s>
<s>
Bat	Bat	k?
Jam	jam	k1gInSc1
</s>
<s>
Bnej	Bnej	k1gFnSc1
Brak	braka	k1gFnPc2
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
atajim	atajim	k1gMnSc1
</s>
<s>
Herzlija	Herzlija	k6eAd1
</s>
<s>
Cholon	Cholon	k1gInSc1
</s>
<s>
Jaffa	Jaffa	k1gFnSc1
(	(	kIx(
<g/>
součást	součást	k1gFnSc1
Tel	tel	kA
Avivu	Aviv	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Kirjat	Kirjat	k1gInSc1
Ono	onen	k3xDgNnSc1,k3xPp3gNnSc1
</s>
<s>
Or	Or	k?
Jehuda	Jehuda	k1gMnSc1
</s>
<s>
Ramat	Ramat	k2eAgMnSc1d1
Gan	Gan	k1gMnSc1
</s>
<s>
Ramat	Ramat	k2eAgInSc1d1
ha-Šaron	ha-Šaron	k1gInSc1
</s>
<s>
Tel	tel	kA
Aviv	Aviv	k1gInSc1
Místní	místní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Azor	Azor	k1gMnSc1
</s>
<s>
Kfar	Kfar	k1gMnSc1
Šmarjahu	Šmarjah	k1gInSc2
Související	související	k2eAgFnSc2d1
</s>
<s>
Guš	Guš	k?
Dan	Dan	k1gMnSc1
Další	další	k2eAgInPc4d1
distrikty	distrikt	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Centrální	centrální	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Haifský	haifský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7704595-6	7704595-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82024988	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
155956582	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82024988	#num#	k4
</s>
