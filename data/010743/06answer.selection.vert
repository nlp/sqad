<s>
Jako	jako	k9	jako
ovoce	ovoce	k1gNnPc1	ovoce
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
zpravidla	zpravidla	k6eAd1	zpravidla
sladké	sladký	k2eAgInPc1d1	sladký
jedlé	jedlý	k2eAgInPc1d1	jedlý
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
plodenství	plodenství	k1gNnPc1	plodenství
nebo	nebo	k8xC	nebo
semena	semeno	k1gNnPc1	semeno
převážně	převážně	k6eAd1	převážně
víceletých	víceletý	k2eAgFnPc2d1	víceletá
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
