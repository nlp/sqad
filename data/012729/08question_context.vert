<s>
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc1	palach
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1948	[number]	k4	1948
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
student	student	k1gMnSc1	student
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
ekonomie	ekonomie	k1gFnSc2	ekonomie
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obětoval	obětovat	k5eAaBmAgMnS	obětovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
potlačování	potlačování	k1gNnSc3	potlačování
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
pasivnímu	pasivní	k2eAgInSc3d1	pasivní
přístupu	přístup	k1gInSc3	přístup
veřejnosti	veřejnost	k1gFnSc2	veřejnost
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
armádami	armáda	k1gFnPc7	armáda
států	stát	k1gInPc2	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
se	se	k3xPyFc4	se
zapálil	zapálit	k5eAaPmAgMnS	zapálit
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>

