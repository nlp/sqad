<p>
<s>
Temno	temno	k6eAd1	temno
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
vyšel	vyjít	k5eAaPmAgInS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1915	[number]	k4	1915
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
J.	J.	kA	J.
Otty	Otta	k1gMnSc2	Otta
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
naplnil	naplnit	k5eAaPmAgInS	naplnit
scottovský	scottovský	k2eAgInSc1d1	scottovský
románový	románový	k2eAgInSc1d1	románový
typ	typ	k1gInSc1	typ
(	(	kIx(	(
<g/>
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
spisovatele	spisovatel	k1gMnSc2	spisovatel
Waltera	Walter	k1gMnSc2	Walter
Scotta	Scott	k1gMnSc2	Scott
<g/>
,	,	kIx,	,
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
bohatou	bohatý	k2eAgFnSc7d1	bohatá
dějovostí	dějovost	k1gFnSc7	dějovost
<g/>
,	,	kIx,	,
napínavým	napínavý	k2eAgInSc7d1	napínavý
příběhem	příběh	k1gInSc7	příběh
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
poloanonymních	poloanonymní	k2eAgFnPc2d1	poloanonymní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
osudy	osud	k1gInPc1	osud
utváří	utvářit	k5eAaPmIp3nP	utvářit
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
i	i	k9	i
skutečné	skutečný	k2eAgFnPc1d1	skutečná
historické	historický	k2eAgFnPc1d1	historická
postavy	postava	k1gFnPc1	postava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Temno	temno	k6eAd1	temno
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
synonymem	synonymum	k1gNnSc7	synonymum
pobělohorské	pobělohorský	k2eAgFnSc2d1	pobělohorská
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
svatořečení	svatořečení	k1gNnSc4	svatořečení
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
;	;	kIx,	;
začíná	začínat	k5eAaImIp3nS	začínat
korunovací	korunovace	k1gFnPc2	korunovace
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1723	[number]	k4	1723
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
Hoře	hora	k1gFnSc6	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
svatořečením	svatořečení	k1gNnSc7	svatořečení
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
tedy	tedy	k9	tedy
popisuje	popisovat	k5eAaImIp3nS	popisovat
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
pobělohorské	pobělohorský	k2eAgFnSc2d1	pobělohorská
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
Skalce	skalka	k1gFnSc6	skalka
u	u	k7c2	u
Opočna	Opočno	k1gNnSc2	Opočno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
regent	regent	k1gMnSc1	regent
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgMnSc1d1	vrchní
správce	správce	k1gMnSc1	správce
panství	panství	k1gNnSc2	panství
<g/>
)	)	kIx)	)
Lhotský	Lhotský	k1gMnSc1	Lhotský
ze	z	k7c2	z
Skalky	skalka	k1gFnSc2	skalka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
zástupci	zástupce	k1gMnPc7	zástupce
slavných	slavný	k2eAgInPc2d1	slavný
rodů	rod	k1gInPc2	rod
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
korunovace	korunovace	k1gFnPc4	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c4	na
Skalku	skalka	k1gFnSc4	skalka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
neteří	neteř	k1gFnSc7	neteř
<g/>
,	,	kIx,	,
bohabojnou	bohabojný	k2eAgFnSc7d1	bohabojná
paní	paní	k1gFnSc7	paní
Polexinou	Polexina	k1gFnSc7	Polexina
Mladotovnou	Mladotovna	k1gFnSc7	Mladotovna
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vrchností	vrchnost	k1gFnSc7	vrchnost
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Skalce	skalka	k1gFnSc6	skalka
mnoho	mnoho	k4c4	mnoho
poddaných	poddaná	k1gFnPc2	poddaná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
příběh	příběh	k1gInSc4	příběh
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgMnSc1d1	důležitý
hlavně	hlavně	k9	hlavně
myslivec	myslivec	k1gMnSc1	myslivec
Machovec	Machovec	k1gMnSc1	Machovec
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
(	(	kIx(	(
<g/>
Helenkou	Helenka	k1gFnSc7	Helenka
a	a	k8xC	a
Tomášem	Tomáš	k1gMnSc7	Tomáš
<g/>
)	)	kIx)	)
a	a	k8xC	a
nevrlý	nevrlý	k2eAgMnSc1d1	nevrlý
správce	správce	k1gMnSc1	správce
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Lhotský	Lhotský	k1gMnSc1	Lhotský
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
rodinu	rodina	k1gFnSc4	rodina
myslivce	myslivec	k1gMnSc2	myslivec
<g/>
,	,	kIx,	,
s	s	k7c7	s
Machovcem	Machovec	k1gMnSc7	Machovec
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
pořádá	pořádat	k5eAaImIp3nS	pořádat
hony	hon	k1gInPc4	hon
<g/>
.	.	kIx.	.
</s>
<s>
Helenka	Helenka	k1gFnSc1	Helenka
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
služka	služka	k1gFnSc1	služka
u	u	k7c2	u
paní	paní	k1gFnSc2	paní
Polexiny	Polexina	k1gFnSc2	Polexina
<g/>
.	.	kIx.	.
</s>
<s>
Machovec	Machovec	k1gMnSc1	Machovec
si	se	k3xPyFc3	se
ale	ale	k9	ale
znepřátelí	znepřátelit	k5eAaPmIp3nP	znepřátelit
správce	správce	k1gMnSc2	správce
Čermáka	Čermák	k1gMnSc2	Čermák
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
vysměje	vysmát	k5eAaPmIp3nS	vysmát
peklu	peklo	k1gNnSc3	peklo
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
správce	správce	k1gMnSc1	správce
rád	rád	k6eAd1	rád
straší	strašit	k5eAaImIp3nS	strašit
poddané	poddaný	k2eAgNnSc1d1	poddané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vyznavači	vyznavač	k1gMnPc1	vyznavač
zakázané	zakázaný	k2eAgFnSc2d1	zakázaná
evangelické	evangelický	k2eAgFnSc2d1	evangelická
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
všemi	všecek	k3xTgInPc7	všecek
možnými	možný	k2eAgInPc7d1	možný
prostředky	prostředek	k1gInPc7	prostředek
bojuje	bojovat	k5eAaImIp3nS	bojovat
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
řád	řád	k1gInSc1	řád
<g/>
;	;	kIx,	;
v	v	k7c6	v
potírání	potírání	k1gNnSc6	potírání
nekatolíků	nekatolík	k1gMnPc2	nekatolík
zvláště	zvláště	k6eAd1	zvláště
vyniká	vynikat	k5eAaImIp3nS	vynikat
páter	páter	k1gMnSc1	páter
Koniáš	Koniáš	k1gMnSc1	Koniáš
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
chodí	chodit	k5eAaImIp3nS	chodit
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
správce	správce	k1gMnSc1	správce
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedodržují	dodržovat	k5eNaImIp3nP	dodržovat
půst	půst	k1gInSc4	půst
<g/>
,	,	kIx,	,
na	na	k7c4	na
večer	večer	k1gInSc4	večer
zavírají	zavírat	k5eAaImIp3nP	zavírat
okenice	okenice	k1gFnPc4	okenice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
pojme	pojmout	k5eAaPmIp3nS	pojmout
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
vyznavači	vyznavač	k1gMnPc7	vyznavač
protestantské	protestantský	k2eAgFnSc2d1	protestantská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
obviněním	obvinění	k1gNnSc7	obvinění
ale	ale	k9	ale
u	u	k7c2	u
Lhotského	Lhotský	k1gMnSc2	Lhotský
neuspěje	uspět	k5eNaPmIp3nS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Vostrým	Vostrý	k2eAgMnSc7d1	Vostrý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
hledán	hledat	k5eAaImNgMnS	hledat
jakožto	jakožto	k8xS	jakožto
predikant	predikant	k1gMnSc1	predikant
(	(	kIx(	(
<g/>
protestantský	protestantský	k2eAgMnSc1d1	protestantský
kazatel	kazatel	k1gMnSc1	kazatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
šiřitel	šiřitel	k1gMnSc1	šiřitel
nekatolických	katolický	k2eNgFnPc2d1	nekatolická
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Katolické	katolický	k2eAgFnPc1d1	katolická
misie	misie	k1gFnPc1	misie
chtějí	chtít	k5eAaImIp3nP	chtít
vyznavače	vyznavač	k1gMnPc4	vyznavač
přijímání	přijímání	k1gNnSc2	přijímání
pod	pod	k7c7	pod
obojí	oboj	k1gFnSc7	oboj
obrátit	obrátit	k5eAaPmF	obrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
taková	takový	k3xDgFnSc1	takový
misie	misie	k1gFnSc1	misie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
jezuita	jezuita	k1gMnSc1	jezuita
Mateřovský	Mateřovský	k1gMnSc1	Mateřovský
<g/>
,	,	kIx,	,
zavítá	zavítat	k5eAaPmIp3nS	zavítat
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
Skalky	skalka	k1gFnSc2	skalka
a	a	k8xC	a
správce	správce	k1gMnSc1	správce
Čermák	Čermák	k1gMnSc1	Čermák
ihned	ihned	k6eAd1	ihned
udá	udat	k5eAaPmIp3nS	udat
Machovce	Machovec	k1gMnSc4	Machovec
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
podaří	podařit	k5eAaPmIp3nS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Tomáše	Tomáš	k1gMnSc4	Tomáš
a	a	k8xC	a
Helenku	Helenka	k1gFnSc4	Helenka
nemůže	moct	k5eNaImIp3nS	moct
vzít	vzít	k5eAaPmF	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
zastihnou	zastihnout	k5eAaPmIp3nP	zastihnout
v	v	k7c6	v
domě	dům	k1gInSc6	dům
jen	jen	k9	jen
myslivcovy	myslivcův	k2eAgFnPc4d1	myslivcova
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
prohlídce	prohlídka	k1gFnSc6	prohlídka
nalézají	nalézat	k5eAaImIp3nP	nalézat
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
zakázané	zakázaný	k2eAgFnPc1d1	zakázaná
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
slavnostně	slavnostně	k6eAd1	slavnostně
spáleny	spálen	k2eAgInPc1d1	spálen
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Helenka	Helenka	k1gFnSc1	Helenka
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
nemilosti	nemilost	k1gFnSc2	nemilost
paní	paní	k1gFnSc2	paní
Polexiny	Polexina	k1gFnSc2	Polexina
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
kacíře	kacíř	k1gMnPc4	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
z	z	k7c2	z
myslivny	myslivna	k1gFnSc2	myslivna
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
tvrdě	tvrdě	k6eAd1	tvrdě
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
poddanými	poddaný	k1gMnPc7	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
je	být	k5eAaImIp3nS	být
paní	paní	k1gFnSc1	paní
Polexina	Polexina	k1gFnSc1	Polexina
pošle	poslat	k5eAaPmIp3nS	poslat
na	na	k7c4	na
převýchovu	převýchova	k1gFnSc4	převýchova
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
příbuznému	příbuzný	k1gMnSc3	příbuzný
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
vítáni	vítat	k5eAaImNgMnP	vítat
a	a	k8xC	a
následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
prodání	prodání	k1gNnSc4	prodání
do	do	k7c2	do
poddanství	poddanství	k1gNnPc2	poddanství
k	k	k7c3	k
sládkovi	sládek	k1gMnSc3	sládek
Březinovi	Březina	k1gMnSc3	Březina
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
vinicích	vinice	k1gFnPc6	vinice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
i	i	k8xC	i
se	se	k3xPyFc4	se
zastánci	zastánce	k1gMnPc1	zastánce
nekatolické	katolický	k2eNgFnSc2d1	nekatolická
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Helenka	Helenka	k1gFnSc1	Helenka
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
příbuzné	příbuzná	k1gFnSc3	příbuzná
sládka	sládek	k1gMnSc2	sládek
Březiny	Březina	k1gFnSc2	Březina
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc2	paní
Lerchové	Lerchová	k1gFnSc2	Lerchová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
donucením	donucení	k1gNnSc7	donucení
převychovávána	převychovávat	k5eAaImNgNnP	převychovávat
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
(	(	kIx(	(
<g/>
rekatolizována	rekatolizován	k2eAgFnSc1d1	rekatolizován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Helenka	Helenka	k1gFnSc1	Helenka
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
jejich	jejich	k3xOp3gInSc4	jejich
odjezd	odjezd	k1gInSc4	odjezd
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
služby	služba	k1gFnSc2	služba
u	u	k7c2	u
paní	paní	k1gFnSc2	paní
Lerchové	Lerchová	k1gFnSc2	Lerchová
se	se	k3xPyFc4	se
však	však	k9	však
Helenka	Helenka	k1gFnSc1	Helenka
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
vnukem	vnuk	k1gMnSc7	vnuk
Jiřím	Jiří	k1gMnSc7	Jiří
Březinou	Březina	k1gMnSc7	Březina
<g/>
,	,	kIx,	,
studentem	student	k1gMnSc7	student
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
přestože	přestože	k8xS	přestože
jejich	jejich	k3xOp3gFnSc1	jejich
víra	víra	k1gFnSc1	víra
je	být	k5eAaImIp3nS	být
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
<g/>
,	,	kIx,	,
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Helenka	Helenka	k1gFnSc1	Helenka
ostatně	ostatně	k6eAd1	ostatně
už	už	k6eAd1	už
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
o	o	k7c6	o
evangelické	evangelický	k2eAgFnSc6d1	evangelická
víře	víra	k1gFnSc6	víra
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
i	i	k9	i
vnitřně	vnitřně	k6eAd1	vnitřně
kloní	klonit	k5eAaImIp3nS	klonit
ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
se	se	k3xPyFc4	se
netrpělivě	trpělivě	k6eNd1	trpělivě
očekává	očekávat	k5eAaImIp3nS	očekávat
svatořečení	svatořečení	k1gNnSc4	svatořečení
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
jezuita	jezuita	k1gMnSc1	jezuita
Mateřovský	Mateřovský	k1gMnSc1	Mateřovský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
jediný	jediný	k2eAgInSc1d1	jediný
cíl	cíl	k1gInSc1	cíl
–	–	k?	–
zcela	zcela	k6eAd1	zcela
vymýtit	vymýtit	k5eAaPmF	vymýtit
před	před	k7c7	před
svatořečením	svatořečení	k1gNnSc7	svatořečení
nekatolickou	katolický	k2eNgFnSc4d1	nekatolická
víru	víra	k1gFnSc4	víra
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
překvapit	překvapit	k5eAaPmF	překvapit
vinaře	vinař	k1gMnPc4	vinař
při	při	k7c6	při
tajné	tajný	k2eAgFnSc6d1	tajná
protestantské	protestantský	k2eAgFnSc6d1	protestantská
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
predikant	predikant	k1gMnSc1	predikant
Vostrý	Vostrý	k2eAgMnSc1d1	Vostrý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
Machovce	Machovec	k1gMnSc4	Machovec
přišel	přijít	k5eAaPmAgInS	přijít
odvést	odvést	k5eAaPmF	odvést
Tomáše	Tomáš	k1gMnSc4	Tomáš
a	a	k8xC	a
Helenku	Helenka	k1gFnSc4	Helenka
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Tomášovi	Tomáš	k1gMnSc3	Tomáš
i	i	k8xC	i
Vostrému	Vostrý	k2eAgMnSc3d1	Vostrý
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Helenka	Helenka	k1gFnSc1	Helenka
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
odmítá	odmítat	k5eAaImIp3nS	odmítat
utéct	utéct	k5eAaPmF	utéct
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
miluje	milovat	k5eAaImIp3nS	milovat
Jiřího	Jiří	k1gMnSc4	Jiří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Hubatia	Hubatius	k1gMnSc2	Hubatius
se	se	k3xPyFc4	se
Jiří	Jiří	k1gMnSc1	Jiří
Březina	Březina	k1gMnSc1	Březina
občas	občas	k6eAd1	občas
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
sousedem	soused	k1gMnSc7	soused
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
deklamátor	deklamátor	k1gMnSc1	deklamátor
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
pomocný	pomocný	k2eAgMnSc1d1	pomocný
úředník	úředník	k1gMnSc1	úředník
<g/>
)	)	kIx)	)
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
sběratel	sběratel	k1gMnSc1	sběratel
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
tajný	tajný	k2eAgMnSc1d1	tajný
evangelík	evangelík	k1gMnSc1	evangelík
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Vostrý	Vostrý	k2eAgMnSc1d1	Vostrý
prchali	prchat	k5eAaImAgMnP	prchat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
,	,	kIx,	,
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
jim	on	k3xPp3gMnPc3	on
pomoc	pomoc	k1gFnSc1	pomoc
i	i	k8xC	i
bratr	bratr	k1gMnSc1	bratr
deklamátora	deklamátor	k1gMnSc2	deklamátor
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
myslivcem	myslivec	k1gMnSc7	myslivec
na	na	k7c6	na
jičínském	jičínský	k2eAgNnSc6d1	Jičínské
panství	panství	k1gNnSc6	panství
a	a	k8xC	a
který	který	k3yQgMnSc1	který
před	před	k7c7	před
časem	čas	k1gInSc7	čas
musel	muset	k5eAaImAgInS	muset
odpřisáhnout	odpřisáhnout	k5eAaPmF	odpřisáhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
evangelické	evangelický	k2eAgFnSc2d1	evangelická
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
útěku	útěk	k1gInSc6	útěk
Tomáše	Tomáš	k1gMnSc2	Tomáš
a	a	k8xC	a
Vostrého	Vostrý	k2eAgMnSc2d1	Vostrý
byl	být	k5eAaImAgInS	být
myslivec	myslivec	k1gMnSc1	myslivec
Svoboda	Svoboda	k1gMnSc1	Svoboda
udán	udat	k5eAaPmNgMnS	udat
pro	pro	k7c4	pro
rouhání	rouhání	k1gNnSc4	rouhání
(	(	kIx(	(
<g/>
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
o	o	k7c6	o
zázracích	zázrak	k1gInPc6	zázrak
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
)	)	kIx)	)
a	a	k8xC	a
našly	najít	k5eAaPmAgInP	najít
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
kacířské	kacířský	k2eAgFnPc1d1	kacířská
knihy	kniha	k1gFnPc1	kniha
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
tentokrát	tentokrát	k6eAd1	tentokrát
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
nezapřel	zapřít	k5eNaPmAgMnS	zapřít
<g/>
,	,	kIx,	,
neodvolal	odvolat	k5eNaPmAgMnS	odvolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
mučen	mučen	k2eAgMnSc1d1	mučen
<g/>
,	,	kIx,	,
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
a	a	k8xC	a
potom	potom	k6eAd1	potom
i	i	k9	i
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
Březinů	Březina	k1gMnPc2	Březina
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc4	jaký
povolání	povolání	k1gNnSc4	povolání
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
touží	toužit	k5eAaImIp3nS	toužit
mít	mít	k5eAaImF	mít
ze	z	k7c2	z
syna	syn	k1gMnSc2	syn
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nesetká	setkat	k5eNaPmIp3nS	setkat
s	s	k7c7	s
Jiříkovým	Jiříkův	k2eAgNnSc7d1	Jiříkovo
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
Helenku	Helenka	k1gFnSc4	Helenka
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
si	se	k3xPyFc3	se
ale	ale	k9	ale
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
spokojeně	spokojeně	k6eAd1	spokojeně
žít	žít	k5eAaImF	žít
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
prozradilo	prozradit	k5eAaPmAgNnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
kacířského	kacířský	k2eAgInSc2d1	kacířský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
pomyslila	pomyslit	k5eAaPmAgFnS	pomyslit
na	na	k7c4	na
syna	syn	k1gMnSc4	syn
bohatého	bohatý	k2eAgMnSc2d1	bohatý
měšťana	měšťan	k1gMnSc2	měšťan
<g/>
,	,	kIx,	,
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
domu	dům	k1gInSc2	dům
Březinových	Březinová	k1gFnPc2	Březinová
a	a	k8xC	a
za	za	k7c4	za
trest	trest	k1gInSc4	trest
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
odvezena	odvézt	k5eAaPmNgFnS	odvézt
do	do	k7c2	do
poddanství	poddanství	k1gNnSc2	poddanství
na	na	k7c4	na
Skalku	skalka	k1gFnSc4	skalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přijímá	přijímat	k5eAaImIp3nS	přijímat
tedy	tedy	k9	tedy
nabídku	nabídka	k1gFnSc4	nabídka
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
tajně	tajně	k6eAd1	tajně
přijel	přijet	k5eAaPmAgMnS	přijet
<g/>
,	,	kIx,	,
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
za	za	k7c7	za
Tomášem	Tomáš	k1gMnSc7	Tomáš
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Deklamátor	deklamátor	k1gMnSc1	deklamátor
Svoboda	Svoboda	k1gMnSc1	Svoboda
navštívil	navštívit	k5eAaPmAgMnS	navštívit
svého	svůj	k3xOyFgMnSc4	svůj
uvězněného	uvězněný	k2eAgMnSc4d1	uvězněný
bratra	bratr	k1gMnSc4	bratr
myslivce	myslivec	k1gMnSc4	myslivec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
popravu	poprava	k1gFnSc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
je	být	k5eAaImIp3nS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
přistižen	přistižen	k2eAgMnSc1d1	přistižen
při	při	k7c6	při
nekatolické	katolický	k2eNgFnSc6d1	nekatolická
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Březina	Březina	k1gMnSc1	Březina
<g/>
,	,	kIx,	,
zklamaný	zklamaný	k2eAgInSc1d1	zklamaný
nenaplněnou	naplněný	k2eNgFnSc7d1	nenaplněná
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
Helence	Helenka	k1gFnSc3	Helenka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
alumnem	alumnus	k1gMnSc7	alumnus
(	(	kIx(	(
<g/>
seminaristou	seminarista	k1gMnSc7	seminarista
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
bílém	bílý	k2eAgNnSc6d1	bílé
rouchu	roucho	k1gNnSc6	roucho
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
slavnosti	slavnost	k1gFnPc1	slavnost
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
nového	nový	k2eAgMnSc2d1	nový
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
končí	končit	k5eAaImIp3nS	končit
oslavami	oslava	k1gFnPc7	oslava
svatořečení	svatořečení	k1gNnSc2	svatořečení
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
kapitole	kapitola	k1gFnSc6	kapitola
se	se	k3xPyFc4	se
dovídáme	dovídat	k5eAaImIp1nP	dovídat
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
:	:	kIx,	:
Machovce	Machovec	k1gMnSc2	Machovec
<g/>
,	,	kIx,	,
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
,	,	kIx,	,
Helenky	Helenka	k1gFnSc2	Helenka
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
popraveného	popravený	k2eAgMnSc2d1	popravený
myslivce	myslivec	k1gMnSc2	myslivec
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fasciklu	fascikl	k1gInSc6	fascikl
studijních	studijní	k2eAgInPc2d1	studijní
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
v	v	k7c6	v
Jiráskově	Jiráskův	k2eAgFnSc6d1	Jiráskova
pozůstalosti	pozůstalost	k1gFnSc6	pozůstalost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
výpisků	výpisek	k1gInPc2	výpisek
k	k	k7c3	k
Temnu	temno	k1gNnSc3	temno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
psal	psát	k5eAaImAgInS	psát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
v	v	k7c6	v
důchodu	důchod	k1gInSc6	důchod
<g/>
,	,	kIx,	,
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
s	s	k7c7	s
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
svědomitostí	svědomitost	k1gFnSc7	svědomitost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
výpisy	výpis	k1gInPc1	výpis
ve	v	k7c6	v
studijních	studijní	k2eAgInPc6d1	studijní
materiálech	materiál	k1gInPc6	materiál
<g/>
,	,	kIx,	,
znal	znát	k5eAaImAgMnS	znát
Jirásek	Jirásek	k1gMnSc1	Jirásek
všechnu	všechen	k3xTgFnSc4	všechen
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
historickou	historický	k2eAgFnSc4d1	historická
literaturu	literatura	k1gFnSc4	literatura
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ze	z	k7c2	z
srovnání	srovnání	k1gNnSc2	srovnání
historických	historický	k2eAgInPc2d1	historický
dokumentů	dokument	k1gInPc2	dokument
s	s	k7c7	s
Jiráskovým	Jiráskův	k2eAgInSc7d1	Jiráskův
románem	román	k1gInSc7	román
Temno	temno	k6eAd1	temno
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
velmi	velmi	k6eAd1	velmi
pečlivě	pečlivě	k6eAd1	pečlivě
studoval	studovat	k5eAaImAgMnS	studovat
archiválie	archiválie	k1gFnPc4	archiválie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
realistický	realistický	k2eAgInSc1d1	realistický
obraz	obraz	k1gInSc1	obraz
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Postavu	postava	k1gFnSc4	postava
myslivce	myslivec	k1gMnSc2	myslivec
Svobody	Svoboda	k1gMnSc2	Svoboda
nalezl	naleznout	k5eAaPmAgMnS	naleznout
Jirásek	Jirásek	k1gMnSc1	Jirásek
v	v	k7c6	v
díle	díl	k1gInSc6	díl
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Antonína	Antonín	k1gMnSc2	Antonín
Rezka	Rezek	k1gMnSc2	Rezek
Dějiny	dějiny	k1gFnPc1	dějiny
prostonárodního	prostonárodní	k2eAgNnSc2d1	prostonárodní
hnutí	hnutí	k1gNnSc2	hnutí
náboženského	náboženský	k2eAgNnSc2d1	náboženské
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
a	a	k8xC	a
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
T.	T.	kA	T.
V.	V.	kA	V.
Bílka	Bílek	k1gMnSc2	Bílek
Reformace	reformace	k1gFnSc2	reformace
katolická	katolický	k2eAgFnSc1d1	katolická
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
prameny	pramen	k1gInPc1	pramen
vypravují	vypravovat	k5eAaImIp3nP	vypravovat
o	o	k7c6	o
myslivci	myslivec	k1gMnSc6	myslivec
Svobodovi	Svoboda	k1gMnSc6	Svoboda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
na	na	k7c4	na
panství	panství	k1gNnSc4	panství
hraběte	hrabě	k1gMnSc2	hrabě
Šlika	šlika	k1gFnSc1	šlika
a	a	k8xC	a
kterého	který	k3yRgMnSc4	který
dvakrát	dvakrát	k6eAd1	dvakrát
přistihli	přistihnout	k5eAaPmAgMnP	přistihnout
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
nekatolických	katolický	k2eNgFnPc2d1	nekatolická
knih	kniha	k1gFnPc2	kniha
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
hrabětem	hrabě	k1gMnSc7	hrabě
odevzdán	odevzdat	k5eAaPmNgInS	odevzdat
soudu	soud	k1gInSc3	soud
a	a	k8xC	a
pro	pro	k7c4	pro
křivou	křivý	k2eAgFnSc4d1	křivá
přísahu	přísaha	k1gFnSc4	přísaha
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
a	a	k8xC	a
dcery	dcera	k1gFnPc1	dcera
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
nekatolíci	nekatolík	k1gMnPc1	nekatolík
<g/>
,	,	kIx,	,
nemohli	moct	k5eNaImAgMnP	moct
snést	snést	k5eAaPmF	snést
útisk	útisk	k1gInSc4	útisk
a	a	k8xC	a
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
Žitavy	Žitava	k1gFnSc2	Žitava
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
postava	postava	k1gFnSc1	postava
Machovce	Machovec	k1gMnSc2	Machovec
byla	být	k5eAaImAgFnS	být
–	–	k?	–
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
úpravami	úprava	k1gFnPc7	úprava
–	–	k?	–
převzata	převzat	k2eAgNnPc4d1	převzato
z	z	k7c2	z
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
díla	dílo	k1gNnSc2	dílo
Rezkova	Rezkův	k2eAgNnSc2d1	Rezkovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
utekl	utéct	k5eAaPmAgMnS	utéct
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jezuité	jezuita	k1gMnPc1	jezuita
Firmus	Firmus	k1gMnSc1	Firmus
a	a	k8xC	a
Mateřovský	Mateřovský	k2eAgMnSc1d1	Mateřovský
nejsou	být	k5eNaImIp3nP	být
postavy	postav	k1gInPc1	postav
vymyšlené	vymyšlený	k2eAgInPc1d1	vymyšlený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
v	v	k7c6	v
díle	díl	k1gInSc6	díl
Rezkově	Rezkův	k2eAgInSc6d1	Rezkův
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
popsán	popsán	k2eAgInSc1d1	popsán
i	i	k8xC	i
nelidský	lidský	k2eNgInSc1d1	nelidský
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
přiváděli	přivádět	k5eAaImAgMnP	přivádět
nekatolíky	nekatolík	k1gMnPc7	nekatolík
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
lůna	lůno	k1gNnSc2	lůno
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
ze	z	k7c2	z
života	život	k1gInSc2	život
pátera	páter	k1gMnSc2	páter
Koniáše	Koniáš	k1gMnSc2	Koniáš
čerpal	čerpat	k5eAaImAgMnS	čerpat
Jirásek	Jirásek	k1gMnSc1	Jirásek
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Antonína	Antonín	k1gMnSc2	Antonín
Podlahy	podlaha	k1gFnSc2	podlaha
a	a	k8xC	a
u	u	k7c2	u
téhož	týž	k3xTgMnSc2	týž
autora	autor	k1gMnSc2	autor
našel	najít	k5eAaPmAgMnS	najít
též	též	k6eAd1	též
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
dlouho	dlouho	k6eAd1	dlouho
marném	marný	k2eAgNnSc6d1	marné
úsilí	úsilí	k1gNnSc6	úsilí
získat	získat	k5eAaPmF	získat
obyvatele	obyvatel	k1gMnSc2	obyvatel
pražských	pražský	k2eAgFnPc2d1	Pražská
vinic	vinice	k1gFnPc2	vinice
znovu	znovu	k6eAd1	znovu
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Temno	temno	k6eAd1	temno
četli	číst	k5eAaImAgMnP	číst
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
čeští	český	k2eAgMnPc1d1	český
vojáci	voják	k1gMnPc1	voják
na	na	k7c6	na
frontách	fronta	k1gFnPc6	fronta
i	i	k8xC	i
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
a	a	k8xC	a
všude	všude	k6eAd1	všude
posilovalo	posilovat	k5eAaImAgNnS	posilovat
vůli	vůle	k1gFnSc4	vůle
k	k	k7c3	k
odboji	odboj	k1gInSc3	odboj
proti	proti	k7c3	proti
útisku	útisk	k1gInSc3	útisk
a	a	k8xC	a
k	k	k7c3	k
boji	boj	k1gInSc3	boj
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
svébytnost	svébytnost	k1gFnSc4	svébytnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1915	[number]	k4	1915
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
rozebráno	rozebrán	k2eAgNnSc1d1	rozebráno
a	a	k8xC	a
již	již	k6eAd1	již
4	[number]	k4	4
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1915	[number]	k4	1915
vychází	vycházet	k5eAaImIp3nS	vycházet
vydání	vydání	k1gNnSc6	vydání
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
vyprodáno	vyprodat	k5eAaPmNgNnS	vyprodat
ještě	ještě	k9	ještě
před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1916	[number]	k4	1916
vychází	vycházet	k5eAaImIp3nS	vycházet
vydání	vydání	k1gNnSc2	vydání
třetí	třetí	k4xOgFnSc2	třetí
a	a	k8xC	a
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
tři	tři	k4xCgMnPc4	tři
do	do	k7c2	do
konce	konec	k1gInSc2	konec
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Květy	Květa	k1gFnSc2	Květa
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jedno	jeden	k4xCgNnSc1	jeden
jest	být	k5eAaImIp3nS	být
nepochybno	nepochybna	k1gFnSc5	nepochybna
<g/>
:	:	kIx,	:
udeřila	udeřit	k5eAaPmAgFnS	udeřit
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
také	také	k9	také
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
Jiráska	Jirásek	k1gMnSc4	Jirásek
jako	jako	k8xS	jako
umělce	umělec	k1gMnSc4	umělec
a	a	k8xC	a
básníka	básník	k1gMnSc4	básník
přijímal	přijímat	k5eAaImAgMnS	přijímat
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
výhradami	výhrada	k1gFnPc7	výhrada
<g/>
,	,	kIx,	,
přimyká	přimykat	k5eAaImIp3nS	přimykat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
vděčně	vděčně	k6eAd1	vděčně
jako	jako	k9	jako
k	k	k7c3	k
učiteli	učitel	k1gMnSc3	učitel
národní	národní	k2eAgFnSc2d1	národní
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jím	on	k3xPp3gNnSc7	on
jest	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
"	"	kIx"	"
<g/>
Temnu	temno	k1gNnSc6	temno
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
paradoxem	paradox	k1gInSc7	paradox
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
rekové	rek	k1gMnPc1	rek
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
trpní	trpný	k2eAgMnPc1d1	trpný
<g/>
,	,	kIx,	,
volá	volat	k5eAaImIp3nS	volat
k	k	k7c3	k
činu	čin	k1gInSc3	čin
a	a	k8xC	a
učí	učit	k5eAaImIp3nP	učit
stránkami	stránka	k1gFnPc7	stránka
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
šeří	šeřit	k5eAaImIp3nS	šeřit
zánik	zánik	k1gInSc1	zánik
češství	češství	k1gNnSc2	češství
<g/>
,	,	kIx,	,
věřiti	věřit	k5eAaImF	věřit
v	v	k7c4	v
mravní	mravní	k2eAgFnSc4d1	mravní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc4d2	lepší
budoucnost	budoucnost	k1gFnSc4	budoucnost
národa	národ	k1gInSc2	národ
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-uherské	rakouskoherský	k2eAgInPc4d1	rakousko-uherský
úřady	úřad	k1gInPc4	úřad
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
Jiráskova	Jiráskův	k2eAgInSc2d1	Jiráskův
románu	román	k1gInSc2	román
reagovaly	reagovat	k5eAaBmAgFnP	reagovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zemská	zemský	k2eAgFnSc1d1	zemská
školní	školní	k2eAgFnSc1d1	školní
rada	rada	k1gFnSc1	rada
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1917	[number]	k4	1917
zákaz	zákaz	k1gInSc1	zákaz
nákupu	nákup	k1gInSc2	nákup
Temna	temno	k1gNnSc2	temno
pro	pro	k7c4	pro
školní	školní	k2eAgFnPc4d1	školní
knihovny	knihovna	k1gFnPc4	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Pokyn	pokyn	k1gInSc1	pokyn
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jí	on	k3xPp3gFnSc3	on
dalo	dát	k5eAaPmAgNnS	dát
Místodržitelství	místodržitelství	k1gNnPc2	místodržitelství
přípisem	přípis	k1gInSc7	přípis
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
s	s	k7c7	s
názvem	název	k1gInSc7	název
Temno	temno	k1gNnSc1	temno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
režii	režie	k1gFnSc6	režie
Karela	Karel	k1gMnSc2	Karel
Steklého	Steklý	k1gMnSc2	Steklý
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Boháč	Boháč	k1gMnSc1	Boháč
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Švorcová	švorcový	k2eAgFnSc1d1	Švorcová
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Cupák	Cupák	k1gMnSc1	Cupák
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
Pištěk	Pištěk	k1gMnSc1	Pištěk
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Friedlová	Friedlový	k2eAgFnSc1d1	Friedlová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Jiráskovo	Jiráskův	k2eAgNnSc1d1	Jiráskovo
Temno	temno	k1gNnSc1	temno
vycházelo	vycházet	k5eAaImAgNnS	vycházet
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
září	září	k1gNnSc2	září
1912	[number]	k4	1912
do	do	k7c2	do
září	září	k1gNnSc2	září
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
kapitoly	kapitola	k1gFnSc2	kapitola
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
do	do	k7c2	do
března	březen	k1gInSc2	březen
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
kapitoly	kapitola	k1gFnSc2	kapitola
53	[number]	k4	53
<g/>
–	–	k?	–
<g/>
76	[number]	k4	76
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
bylo	být	k5eAaImAgNnS	být
publikováno	publikovat	k5eAaBmNgNnS	publikovat
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
několika	několik	k4yIc2	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
slovenštiny	slovenština	k1gFnPc1	slovenština
<g/>
,	,	kIx,	,
polštiny	polština	k1gFnPc1	polština
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mroki	Mroki	k1gNnSc1	Mroki
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maďarštiny	maďarština	k1gFnPc4	maďarština
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sötétség	Sötétség	k1gInSc1	Sötétség
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
českých	český	k2eAgNnPc2d1	české
vydání	vydání	k1gNnPc2	vydání
vybíráme	vybírat	k5eAaImIp1nP	vybírat
dvě	dva	k4xCgFnPc4	dva
novější	nový	k2eAgFnPc4d2	novější
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Temno	temno	k6eAd1	temno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Tribunu	tribuna	k1gFnSc4	tribuna
EU	EU	kA	EU
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Tribun	tribun	k1gMnSc1	tribun
EU	EU	kA	EU
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
555	[number]	k4	555
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7399	[number]	k4	7399
<g/>
-	-	kIx~	-
<g/>
315	[number]	k4	315
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Temno	temno	k6eAd1	temno
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
637	[number]	k4	637
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85637	[number]	k4	85637
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
Vycházela	vycházet	k5eAaImAgFnS	vycházet
i	i	k9	i
vydání	vydání	k1gNnSc4	vydání
s	s	k7c7	s
poznámkami	poznámka	k1gFnPc7	poznámka
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Mimočítanková	mimočítankový	k2eAgFnSc1d1	mimočítanková
četba	četba	k1gFnSc1	četba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Temno	temno	k1gNnSc1	temno
<g/>
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
obraz	obraz	k1gInSc1	obraz
<g/>
:	:	kIx,	:
Mimočítanková	mimočítankový	k2eAgFnSc1d1	mimočítanková
četba	četba	k1gFnSc1	četba
pro	pro	k7c4	pro
odborné	odborný	k2eAgFnPc4d1	odborná
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nezm	nezm	k1gMnSc1	nezm
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
v	v	k7c6	v
SPN	SPN	kA	SPN
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
598	[number]	k4	598
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
s.	s.	k?	s.
<g/>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
též	též	k9	též
zdramatizován	zdramatizován	k2eAgInSc1d1	zdramatizován
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
MAREK	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Temno	temno	k1gNnSc1	temno
<g/>
:	:	kIx,	:
Dramatizace	dramatizace	k1gFnSc1	dramatizace
[	[	kIx(	[
<g/>
stejnojm	stejnojm	k1gInSc1	stejnojm
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
románu	román	k1gInSc2	román
Aloise	Alois	k1gMnSc4	Alois
Jiráska	Jirásek	k1gMnSc4	Jirásek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dilia	Dilia	k1gFnSc1	Dilia
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
45	[number]	k4	45
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
203	[number]	k4	203
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
67	[number]	k4	67
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
veřejně	veřejně	k6eAd1	veřejně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
řada	řada	k1gFnSc1	řada
děl	dělo	k1gNnPc2	dělo
Al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
několik	několik	k4yIc4	několik
vydání	vydání	k1gNnPc2	vydání
románu	román	k1gInSc2	román
Temno	temno	k1gNnSc1	temno
<g/>
.	.	kIx.	.
</s>
<s>
Uvádíme	uvádět	k5eAaImIp1nP	uvádět
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Temno	temno	k1gNnSc1	temno
<g/>
:	:	kIx,	:
historický	historický	k2eAgInSc1d1	historický
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
768	[number]	k4	768
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Literatura	literatura	k1gFnSc1	literatura
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
/	/	kIx~	/
Redaktor	redaktor	k1gMnSc1	redaktor
svazku	svazek	k1gInSc2	svazek
Miloš	Miloš	k1gMnSc1	Miloš
Pohorský	pohorský	k2eAgMnSc1d1	pohorský
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
631	[number]	k4	631
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
kapitolu	kapitola	k1gFnSc4	kapitola
"	"	kIx"	"
<g/>
Pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
doba	doba	k1gFnSc1	doba
–	–	k?	–
doba	doba	k1gFnSc1	doba
temna	temno	k1gNnSc2	temno
<g/>
"	"	kIx"	"
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
459	[number]	k4	459
<g/>
–	–	k?	–
<g/>
460	[number]	k4	460
<g/>
;	;	kIx,	;
napsal	napsat	k5eAaBmAgMnS	napsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pešat	Pešat	k2eAgMnSc1d1	Pešat
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Temno	temno	k1gNnSc1	temno
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
Koniášova	Koniášův	k2eAgFnSc1d1	Koniášova
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Benešov	Benešov	k1gInSc1	Benešov
<g/>
:	:	kIx,	:
EMAN	eman	k1gInSc1	eman
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
324	[number]	k4	324
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86211	[number]	k4	86211
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Autor	autor	k1gMnSc1	autor
mj.	mj.	kA	mj.
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
i	i	k9	i
Jiráskovo	Jiráskův	k2eAgNnSc1d1	Jiráskovo
"	"	kIx"	"
<g/>
Temno	temno	k6eAd1	temno
<g/>
"	"	kIx"	"
a	a	k8xC	a
líčí	líčit	k5eAaImIp3nP	líčit
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
vedly	vést	k5eAaImAgInP	vést
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
JANÁČKOVÁ	Janáčková	k1gFnSc1	Janáčková
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
monografie	monografie	k1gFnPc1	monografie
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
581	[number]	k4	581
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
kapitolu	kapitola	k1gFnSc4	kapitola
"	"	kIx"	"
<g/>
Temno	temno	k6eAd1	temno
<g/>
"	"	kIx"	"
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
421	[number]	k4	421
<g/>
–	–	k?	–
<g/>
436	[number]	k4	436
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
KLEINSCHNITZOVÁ	KLEINSCHNITZOVÁ	kA	KLEINSCHNITZOVÁ
<g/>
,	,	kIx,	,
Flora	Flora	k1gFnSc1	Flora
<g/>
,	,	kIx,	,
Prameny	pramen	k1gInPc1	pramen
"	"	kIx"	"
<g/>
Temna	temno	k1gNnPc1	temno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
HÝSEK	Hýsek	k1gMnSc1	Hýsek
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
a	a	k8xC	a
MÁDL	MÁDL	kA	MÁDL
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Boromejský	Boromejský	k2eAgMnSc1d1	Boromejský
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
jeho	jeho	k3xOp3gFnPc2	jeho
sedmdesátých	sedmdesátý	k4xOgFnPc2	sedmdesátý
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
510	[number]	k4	510
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Studie	studie	k1gFnSc1	studie
"	"	kIx"	"
<g/>
Prameny	pramen	k1gInPc1	pramen
"	"	kIx"	"
<g/>
Temna	temno	k1gNnPc1	temno
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
otištěna	otisknout	k5eAaPmNgFnS	otisknout
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
159	[number]	k4	159
<g/>
–	–	k?	–
<g/>
171	[number]	k4	171
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
–	–	k?	–
dostupné	dostupný	k2eAgNnSc1d1	dostupné
též	též	k9	též
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
