<s>
Aperitiv	aperitiv	k1gInSc1	aperitiv
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
apéritif	apéritif	k1gMnSc1	apéritif
nebo	nebo	k8xC	nebo
ital	ital	k1gMnSc1	ital
<g/>
.	.	kIx.	.
aperitivo	aperitiva	k1gFnSc5	aperitiva
-	-	kIx~	-
"	"	kIx"	"
<g/>
otvírající	otvírající	k2eAgMnSc1d1	otvírající
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
