<s>
Aperitiv	aperitiv	k1gInSc1	aperitiv
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
apéritif	apéritif	k1gMnSc1	apéritif
nebo	nebo	k8xC	nebo
ital	ital	k1gMnSc1	ital
<g/>
.	.	kIx.	.
aperitivo	aperitiva	k1gFnSc5	aperitiva
-	-	kIx~	-
"	"	kIx"	"
<g/>
otvírající	otvírající	k2eAgMnSc1d1	otvírající
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Zpříjemňuje	zpříjemňovat	k5eAaImIp3nS	zpříjemňovat
čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
pokrm	pokrm	k1gInSc4	pokrm
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
či	či	k8xC	či
probouzí	probouzet	k5eAaImIp3nS	probouzet
větší	veliký	k2eAgFnSc4d2	veliký
chuť	chuť	k1gFnSc4	chuť
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
být	být	k5eAaImF	být
skutečně	skutečně	k6eAd1	skutečně
jen	jen	k9	jen
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
o	o	k7c4	o
společenskou	společenský	k2eAgFnSc4d1	společenská
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Protějškem	protějšek	k1gInSc7	protějšek
aperitivu	aperitiv	k1gInSc2	aperitiv
je	být	k5eAaImIp3nS	být
digestiv	digestit	k5eAaPmDgInS	digestit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
1	[number]	k4	1
decilitr	decilitr	k1gInSc4	decilitr
ve	v	k7c6	v
skleničkách	sklenička	k1gFnPc6	sklenička
nebo	nebo	k8xC	nebo
miskách	miska	k1gFnPc6	miska
s	s	k7c7	s
tenkým	tenký	k2eAgInSc7d1	tenký
plátkem	plátek	k1gInSc7	plátek
citronu	citron	k1gInSc2	citron
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
jiný	jiný	k2eAgInSc4d1	jiný
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgInSc4d1	jiný
způsob	způsob	k1gInSc4	způsob
podávání	podávání	k1gNnSc2	podávání
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
pil	pít	k5eAaImAgMnS	pít
aperitiv	aperitiv	k1gInSc4	aperitiv
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připravil	připravit	k5eAaPmAgInS	připravit
trávicí	trávicí	k2eAgInSc1d1	trávicí
trakt	trakt	k1gInSc1	trakt
na	na	k7c4	na
požitky	požitek	k1gInPc4	požitek
z	z	k7c2	z
nadcházející	nadcházející	k2eAgFnSc2d1	nadcházející
hostiny	hostina	k1gFnSc2	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
z	z	k7c2	z
aperitivu	aperitiv	k1gInSc2	aperitiv
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
sofistikovaná	sofistikovaný	k2eAgFnSc1d1	sofistikovaná
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
mají	mít	k5eAaImIp3nP	mít
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
právě	právě	k9	právě
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
zrodily	zrodit	k5eAaPmAgInP	zrodit
aperitivy	aperitiv	k1gInPc1	aperitiv
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Campari	campari	k1gNnSc1	campari
<g/>
,	,	kIx,	,
Martini	martini	k1gNnSc1	martini
nebo	nebo	k8xC	nebo
Cinzano	Cinzana	k1gFnSc5	Cinzana
<g/>
.	.	kIx.	.
</s>
<s>
Aperitiv	aperitiv	k1gInSc1	aperitiv
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
lehký	lehký	k2eAgInSc1d1	lehký
–	–	k?	–
sekt	sekt	k1gInSc1	sekt
<g/>
,	,	kIx,	,
šampaňské	šampaňské	k1gNnSc1	šampaňské
<g/>
,	,	kIx,	,
suché	suchý	k2eAgNnSc1d1	suché
bílé	bílý	k2eAgNnSc1d1	bílé
nebo	nebo	k8xC	nebo
červené	červený	k2eAgNnSc1d1	červené
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sladký	sladký	k2eAgInSc4d1	sladký
–	–	k?	–
cinzano	cinzana	k1gFnSc5	cinzana
<g/>
,	,	kIx,	,
becherovka	becherovka	k1gFnSc1	becherovka
<g/>
,	,	kIx,	,
portské	portský	k2eAgNnSc1d1	portské
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
martini	martini	k1gNnSc1	martini
<g/>
,	,	kIx,	,
sherry	sherry	k1gNnSc1	sherry
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
také	také	k9	také
řadíme	řadit	k5eAaImIp1nP	řadit
lihoviny	lihovina	k1gFnPc4	lihovina
–	–	k?	–
vodka	vodka	k1gFnSc1	vodka
<g/>
,	,	kIx,	,
hořké	hořký	k2eAgInPc4d1	hořký
nebo	nebo	k8xC	nebo
hořkosladké	hořkosladký	k2eAgInPc4d1	hořkosladký
likéry	likér	k1gInPc4	likér
–	–	k?	–
Fernet	fernet	k1gInSc4	fernet
Stock	Stocka	k1gFnPc2	Stocka
<g/>
,	,	kIx,	,
Becherovka	becherovka	k1gFnSc1	becherovka
<g/>
.	.	kIx.	.
</s>
<s>
Lihoviny	lihovina	k1gFnPc1	lihovina
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
dobře	dobře	k6eAd1	dobře
vychlazené	vychlazený	k2eAgFnPc1d1	vychlazená
0,05	[number]	k4	0,05
l	l	kA	l
bez	bez	k7c2	bez
přidané	přidaný	k2eAgFnSc2d1	přidaná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Digestiv	Digestit	k5eAaPmDgInS	Digestit
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
aperitiv	aperitiv	k1gInSc4	aperitiv
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Podávání	podávání	k1gNnSc2	podávání
aperitivů	aperitiv	k1gInPc2	aperitiv
</s>
