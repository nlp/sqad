<s>
Isoxazol	Isoxazol	k1gInSc1
</s>
<s>
Isoxazol	Isoxazol	k1gInSc1
</s>
<s>
Strukturní	strukturní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Model	model	k1gInSc1
molekuly	molekula	k1gFnSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
1,2	1,2	k4
<g/>
-oxazol	-oxazol	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C3H3NO	C3H3NO	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
288-14-2	288-14-2	k4
</s>
<s>
EC-no	EC-no	k1gNnSc1
(	(	kIx(
<g/>
EINECS	EINECS	kA
<g/>
/	/	kIx~
<g/>
ELINCS	ELINCS	kA
<g/>
/	/	kIx~
<g/>
NLP	NLP	kA
<g/>
)	)	kIx)
</s>
<s>
206-018-7	206-018-7	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
9254	#num#	k4
</s>
<s>
ChEBI	ChEBI	k?
</s>
<s>
33193	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
C	C	kA
<g/>
1	#num#	k4
<g/>
=	=	kIx~
<g/>
CON	CON	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
1	#num#	k4
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
3	#num#	k4
<g/>
H	H	kA
<g/>
3	#num#	k4
<g/>
NO	no	k9
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
H	H	kA
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
69,062	69,062	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
95	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
368	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
1,075	1,075	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Tlak	tlak	k1gInSc1
páry	pára	k1gFnSc2
</s>
<s>
6,06	6,06	k4
kPa	kPa	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
GHS02	GHS02	k4
GHS	GHS	kA
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
H-věty	H-věta	k1gFnPc1
</s>
<s>
H225	H225	k4
H314	H314	k1gFnSc1
H	H	kA
<g/>
318	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
P-věty	P-věta	k1gFnPc1
</s>
<s>
P	P	kA
<g/>
210	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
233	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
240	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
241	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
242	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
243	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
260	#num#	k4
<g/>
,	,	kIx,
P264	P264	k1gFnSc1
P	P	kA
<g/>
301	#num#	k4
<g/>
+	+	kIx~
<g/>
330	#num#	k4
<g/>
+	+	kIx~
<g/>
331	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
303	#num#	k4
<g/>
+	+	kIx~
<g/>
361	#num#	k4
<g/>
+	+	kIx~
<g/>
353	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
304	#num#	k4
<g/>
+	+	kIx~
<g/>
340	#num#	k4
P	P	kA
<g/>
305	#num#	k4
<g/>
+	+	kIx~
<g/>
351	#num#	k4
<g/>
+	+	kIx~
<g/>
338	#num#	k4
P310	P310	k1gMnSc1
P321	P321	k1gMnSc1
P363	P363	k1gMnSc1
P	P	kA
<g/>
370	#num#	k4
<g/>
+	+	kIx~
<g/>
378	#num#	k4
<g/>
,	,	kIx,
P	P	kA
<g/>
403	#num#	k4
<g/>
+	+	kIx~
<g/>
235	#num#	k4
<g/>
,	,	kIx,
P405	P405	k1gFnSc1
P	P	kA
<g/>
501	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Isoxazol	Isoxazol	k1gInSc1
je	být	k5eAaImIp3nS
heterocyklická	heterocyklický	k2eAgFnSc1d1
organická	organický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
s	s	k7c7
pětičlenným	pětičlenný	k2eAgInSc7d1
cyklem	cyklus	k1gInSc7
obsahujícím	obsahující	k2eAgMnSc7d1
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
dusíku	dusík	k1gInSc2
a	a	k8xC
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
azol	azol	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
jeden	jeden	k4xCgMnSc1
kyslíkový	kyslíkový	k2eAgInSc1d1
atom	atom	k1gInSc1
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
dusíkovým	dusíkový	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Pojmem	pojem	k1gInSc7
isoxazoly	isoxazola	k1gFnSc2
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
také	také	k9
deriváty	derivát	k1gInPc1
této	tento	k3xDgFnSc2
sloučeniny	sloučenina	k1gFnSc2
<g/>
;	;	kIx,
jednovazný	jednovazný	k2eAgInSc1d1
radikál	radikál	k1gInSc1
odvozený	odvozený	k2eAgInSc1d1
od	od	k7c2
isoxazolu	isoxazol	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
isoxazolyl	isoxazolyl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Isoxazolové	Isoxazolový	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
některých	některý	k3yIgFnPc2
přírodních	přírodní	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
kyseliny	kyselina	k1gFnSc2
ibotenové	ibotenová	k1gFnSc2
a	a	k8xC
muscimolu	muscimol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Léčiva	léčivo	k1gNnPc1
a	a	k8xC
herbicidy	herbicid	k1gInPc1
</s>
<s>
Isoxazolové	Isoxazolový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
obsaženy	obsáhnout	k5eAaPmNgInP
v	v	k7c6
molekulách	molekula	k1gFnPc6
několika	několik	k4yIc2
léčiv	léčivo	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
valdecoxib	valdecoxib	k1gInSc4
<g/>
,	,	kIx,
inhibitor	inhibitor	k1gInSc4
COX-	COX-	k1gFnPc2
<g/>
2	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
AMPA	AMPA	kA
<g/>
,	,	kIx,
fungující	fungující	k2eAgMnSc1d1
jako	jako	k9
agonista	agonista	k1gMnSc1
neurotransmiterů	neurotransmiter	k1gInPc2
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
derivát	derivát	k1gInSc1
furoxan	furoxany	k1gInPc2
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
donor	donor	k1gInSc1
oxidu	oxid	k1gInSc2
dusnatého	dusnatý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Isoxazolylovou	Isoxazolylový	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
lze	lze	k6eAd1
též	též	k9
nalézt	nalézt	k5eAaPmF,k5eAaBmF
v	v	k7c6
řadě	řada	k1gFnSc6
antibiotik	antibiotikum	k1gNnPc2
odolných	odolný	k2eAgNnPc2d1
vůči	vůči	k7c3
beta-laktamázám	beta-laktamáza	k1gFnPc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
kloxacilinu	kloxacilin	k2eAgFnSc4d1
<g/>
,	,	kIx,
dikloxacilinu	dikloxacilin	k2eAgFnSc4d1
a	a	k8xC
flukloxacilinu	flukloxacilin	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
anabolickým	anabolický	k2eAgInPc3d1
steroidům	steroid	k1gInPc3
obsahujícím	obsahující	k2eAgMnSc7d1
isoxazolové	isoxazolový	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
patří	patřit	k5eAaImIp3nP
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
danazol	danazola	k1gFnPc2
a	a	k8xC
androisoxazol	androisoxazola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
isoxazolům	isoxazol	k1gInPc3
patří	patřit	k5eAaImIp3nS
také	také	k6eAd1
řada	řada	k1gFnSc1
pesticidů	pesticid	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Isoxaben	Isoxaben	k2eAgInSc1d1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
z	z	k7c2
isoxazolů	isoxazol	k1gInPc2
používaných	používaný	k2eAgInPc2d1
jako	jako	k8xS,k8xC
herbicidy	herbicid	k1gInPc5
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Oxazol	Oxazol	k1gInSc1
</s>
<s>
Pyrrol	pyrrol	k1gInSc1
</s>
<s>
Furan	furan	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Isoxazole	Isoxazole	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
https://pubchem.ncbi.nlm.nih.gov/compound/9254	https://pubchem.ncbi.nlm.nih.gov/compound/9254	k4
<g/>
↑	↑	k?
Jie	Jie	k1gMnSc1
Zhu	Zhu	k1gMnSc1
<g/>
;	;	kIx,
Jun	jun	k1gMnSc1
Mo	Mo	k1gMnSc1
<g/>
;	;	kIx,
Hong-zhi	Hong-zhi	k1gNnSc1
Lin	Lina	k1gFnPc2
<g/>
;	;	kIx,
Yao	Yao	k1gMnSc1
Chen	Chen	k1gMnSc1
<g/>
;	;	kIx,
Hao-peng	Hao-peng	k1gMnSc1
Sun	Sun	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
recent	recent	k1gMnSc1
progress	progress	k6eAd1
of	of	k?
isoxazole	isoxazole	k1gFnSc2
in	in	k?
medicinal	medicinat	k5eAaImAgInS,k5eAaPmAgInS
chemistry	chemistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bioorganic	Bioorganice	k1gFnPc2
&	&	k?
Medicinal	Medicinal	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
3065	#num#	k4
<g/>
–	–	k?
<g/>
3075	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
bmc	bmc	k?
<g/>
.2018	.2018	k4
<g/>
.05	.05	k4
<g/>
.013	.013	k4
<g/>
.	.	kIx.
↑	↑	k?
Clemens	Clemens	k1gInSc1
Lamberth	Lamberth	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxazole	Oxazole	k1gFnSc1
and	and	k?
Isoxazole	Isoxazole	k1gFnSc1
Chemistry	Chemistr	k1gMnPc4
in	in	k?
Crop	Crop	k1gInSc1
Protection	Protection	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Heterocyclic	Heterocyclice	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2035	#num#	k4
<g/>
–	–	k?
<g/>
2045	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
jhet	jhet	k1gInSc1
<g/>
.3252	.3252	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
