<s>
Isoxazol	Isoxazol	k1gInSc1
je	být	k5eAaImIp3nS
heterocyklická	heterocyklický	k2eAgFnSc1d1
organická	organický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
s	s	k7c7
pětičlenným	pětičlenný	k2eAgInSc7d1
cyklem	cyklus	k1gInSc7
obsahujícím	obsahující	k2eAgMnSc7d1
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
dusíku	dusík	k1gInSc2
a	a	k8xC
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>