<s>
Ainština	Ainština	k1gFnSc1	Ainština
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Ainu	Aina	k1gFnSc4	Aina
(	(	kIx(	(
<g/>
ア	ア	k?	ア
イ	イ	k?	イ
<g/>
,	,	kIx,	,
Aynu	Aynus	k1gInSc2	Aynus
Itak	Itaka	k1gFnPc2	Itaka
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
ア	ア	k?	ア
<g/>
,	,	kIx,	,
Ainu-go	Ainuo	k1gMnSc1	Ainu-go
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mluví	mluvit	k5eAaImIp3nS	mluvit
etnická	etnický	k2eAgFnSc1d1	etnická
skupina	skupina	k1gFnSc1	skupina
Ainu	Ainus	k1gInSc2	Ainus
na	na	k7c6	na
severu	sever	k1gInSc6	sever
japonského	japonský	k2eAgInSc2d1	japonský
ostrova	ostrov	k1gInSc2	ostrov
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
mluvilo	mluvit	k5eAaImAgNnS	mluvit
také	také	k9	také
na	na	k7c6	na
Kurilských	kurilský	k2eAgInPc6d1	kurilský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Honšú	Honšú	k1gFnSc2	Honšú
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
polovině	polovina	k1gFnSc6	polovina
Sachalinu	Sachalin	k1gInSc2	Sachalin
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
typologicky	typologicky	k6eAd1	typologicky
podobný	podobný	k2eAgInSc1d1	podobný
japonštině	japonština	k1gFnSc3	japonština
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ainu	ainu	k6eAd1	ainu
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
jazyk	jazyk	k1gInSc4	jazyk
bez	bez	k7c2	bez
známých	známý	k2eAgInPc2d1	známý
vztahů	vztah	k1gInPc2	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
k	k	k7c3	k
paleoasijským	paleoasijský	k2eAgInPc3d1	paleoasijský
jazykům	jazyk	k1gInPc3	jazyk
(	(	kIx(	(
<g/>
paleosibiřským	paleosibiřský	k2eAgMnPc3d1	paleosibiřský
jazykům	jazyk	k1gMnPc3	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
pojem	pojem	k1gInSc1	pojem
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
a	a	k8xC	a
malých	malý	k2eAgFnPc2d1	malá
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
mluvilo	mluvit	k5eAaImAgNnS	mluvit
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
tureckých	turecký	k2eAgMnPc2d1	turecký
a	a	k8xC	a
tunguských	tunguský	k2eAgMnPc2d1	tunguský
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
;	;	kIx,	;
není	být	k5eNaImIp3nS	být
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lingvistů	lingvista	k1gMnPc2	lingvista
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sdílený	sdílený	k2eAgInSc1d1	sdílený
slovník	slovník	k1gInSc1	slovník
mezi	mezi	k7c7	mezi
ainu	ainu	k6eAd1	ainu
a	a	k8xC	a
nivchštinou	nivchština	k1gFnSc7	nivchština
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
přejímáním	přejímání	k1gNnSc7	přejímání
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovaná	navrhovaný	k2eAgFnSc1d1	navrhovaná
příbuznost	příbuznost	k1gFnSc1	příbuznost
s	s	k7c7	s
japonštinou	japonština	k1gFnSc7	japonština
a	a	k8xC	a
altajskými	altajský	k2eAgInPc7d1	altajský
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
Patrie	patrie	k1gFnSc1	patrie
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k6eAd1	jen
malou	malý	k2eAgFnSc4d1	malá
podporu	podpora	k1gFnSc4	podpora
mezi	mezi	k7c7	mezi
odborníky	odborník	k1gMnPc7	odborník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgNnPc6d1	dřívější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
někteří	některý	k3yIgMnPc1	některý
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
austronéskými	austronéský	k2eAgInPc7d1	austronéský
jazyky	jazyk	k1gInPc7	jazyk
na	na	k7c6	na
základě	základ	k1gInSc6	základ
slovníkových	slovníkový	k2eAgFnPc2d1	slovníková
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
srovnání	srovnání	k1gNnPc2	srovnání
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
japonský	japonský	k2eAgMnSc1d1	japonský
lingvista	lingvista	k1gMnSc1	lingvista
Šičiro	Šičiro	k1gNnSc4	Šičiro
Murajama	Murajam	k1gMnSc2	Murajam
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vovin	Vovin	k1gMnSc1	Vovin
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
předložil	předložit	k5eAaPmAgMnS	předložit
důkazy	důkaz	k1gInPc4	důkaz
naznačující	naznačující	k2eAgNnSc4d1	naznačující
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
propojení	propojení	k1gNnSc4	propojení
s	s	k7c7	s
austroasijskými	austroasijský	k2eAgInPc7d1	austroasijský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
;	;	kIx,	;
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
označuje	označovat	k5eAaImIp3nS	označovat
svou	svůj	k3xOyFgFnSc4	svůj
hypotézu	hypotéza	k1gFnSc4	hypotéza
jako	jako	k8xC	jako
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
<g/>
.	.	kIx.	.
</s>
<s>
Ainu	Ainu	k6eAd1	Ainu
je	být	k5eAaImIp3nS	být
skomírající	skomírající	k2eAgInSc4d1	skomírající
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nibutani	Nibutaň	k1gFnSc6	Nibutaň
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Biratori	Birator	k1gFnSc2	Birator
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
100	[number]	k4	100
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
používalo	používat	k5eAaImAgNnS	používat
jazyk	jazyk	k1gInSc4	jazyk
denně	denně	k6eAd1	denně
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Hokkaidó	Hokkaidó	k1gMnPc7	Hokkaidó
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1	[number]	k4	1
000	[number]	k4	000
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
několika	několik	k4yIc7	několik
výjimkami	výjimka	k1gFnPc7	výjimka
nejsou	být	k5eNaImIp3nP	být
mladší	mladý	k2eAgFnSc1d2	mladší
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
mezi	mezi	k7c7	mezi
rodilými	rodilý	k2eAgMnPc7d1	rodilý
mluvčími	mluvčí	k1gMnPc7	mluvčí
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
už	už	k6eAd1	už
asi	asi	k9	asi
nebude	být	k5eNaImBp3nS	být
přesné	přesný	k2eAgNnSc1d1	přesné
říkat	říkat	k5eAaImF	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
lidí	člověk	k1gMnPc2	člověk
jej	on	k3xPp3gInSc2	on
užívá	užívat	k5eAaImIp3nS	užívat
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
hnutí	hnutí	k1gNnSc1	hnutí
snažící	snažící	k2eAgNnSc1d1	snažící
se	se	k3xPyFc4	se
zvrátit	zvrátit	k5eAaPmF	zvrátit
úbytek	úbytek	k1gInSc1	úbytek
mluvčích	mluvčí	k1gMnPc2	mluvčí
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ze	z	k7c2	z
150	[number]	k4	150
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
prohlašujících	prohlašující	k2eAgMnPc2d1	prohlašující
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
za	za	k7c4	za
etnické	etnický	k2eAgFnPc4d1	etnická
Ainu	Aina	k1gFnSc4	Aina
(	(	kIx(	(
<g/>
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
Ainu	Ain	k1gMnSc6	Ain
si	se	k3xPyFc3	se
není	být	k5eNaImIp3nS	být
vědomo	vědom	k2eAgNnSc4d1	vědomo
svých	svůj	k3xOyFgMnPc2	svůj
kořenů	kořen	k1gInPc2	kořen
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
tají	tají	k6eAd1	tají
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
diskriminací	diskriminace	k1gFnSc7	diskriminace
<g/>
)	)	kIx)	)
mluví	mluvit	k5eAaImIp3nS	mluvit
pouze	pouze	k6eAd1	pouze
japonsky	japonsky	k6eAd1	japonsky
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
však	však	k9	však
vzrůstající	vzrůstající	k2eAgNnSc1d1	vzrůstající
množství	množství	k1gNnSc1	množství
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
není	být	k5eNaImIp3nS	být
mateřštinou	mateřština	k1gFnSc7	mateřština
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
Hokkaidó	Hokkaidó	k1gFnSc6	Hokkaidó
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc2	vliv
aktivistů	aktivista	k1gMnPc2	aktivista
ainu	ainus	k1gInSc2	ainus
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc4d1	bývalý
člena	člen	k1gMnSc4	člen
japonského	japonský	k2eAgInSc2d1	japonský
parlamentu	parlament	k1gInSc2	parlament
Šigeru	Šiger	k1gInSc2	Šiger
Kajano	Kajana	k1gFnSc5	Kajana
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
rodilým	rodilý	k2eAgMnSc7d1	rodilý
mluvčím	mluvčí	k1gMnSc7	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Ainu	Ainu	k6eAd1	Ainu
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
dnešní	dnešní	k2eAgInSc1d1	dnešní
počet	počet	k1gInSc1	počet
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
už	už	k6eAd1	už
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
používáme	používat	k5eAaImIp1nP	používat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
definici	definice	k1gFnSc4	definice
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
mluvčími	mluvčí	k1gMnPc7	mluvčí
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
širší	široký	k2eAgFnSc2d2	širší
definice	definice	k1gFnSc2	definice
<g/>
)	)	kIx)	)
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nad	nad	k7c7	nad
rodilými	rodilý	k2eAgMnPc7d1	rodilý
převažují	převažovat	k5eAaImIp3nP	převažovat
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
naučili	naučit	k5eAaPmAgMnP	naučit
jako	jako	k8xC	jako
další	další	k2eAgInSc4d1	další
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Slabiky	slabika	k1gFnPc1	slabika
ainu	ainu	k6eAd1	ainu
jsou	být	k5eAaImIp3nP	být
typu	typa	k1gFnSc4	typa
CV	CV	kA	CV
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
tvořené	tvořený	k2eAgNnSc1d1	tvořené
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
samohláskou	samohláska	k1gFnSc7	samohláska
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
další	další	k2eAgFnSc7d1	další
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
vzniká	vznikat	k5eAaImIp3nS	vznikat
málo	málo	k4c1	málo
skupin	skupina	k1gFnPc2	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Sekvence	sekvence	k1gFnSc1	sekvence
/	/	kIx~	/
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
jako	jako	k9	jako
[	[	kIx(	[
<g/>
ʧ	ʧ	k?	ʧ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
před	před	k7c7	před
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slabik	slabika	k1gFnPc2	slabika
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dialekty	dialekt	k1gInPc4	dialekt
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
;	;	kIx,	;
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sachalinu	Sachalin	k1gInSc2	Sachalin
přecházejí	přecházet	k5eAaImIp3nP	přecházet
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
/	/	kIx~	/
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slabik	slabika	k1gFnPc2	slabika
v	v	k7c6	v
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
systém	systém	k1gInSc1	systém
melodického	melodický	k2eAgInSc2d1	melodický
přízvuku	přízvuk	k1gInSc2	přízvuk
<g/>
;	;	kIx,	;
slova	slovo	k1gNnPc4	slovo
včetně	včetně	k7c2	včetně
afixů	afix	k1gInPc2	afix
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
intonaci	intonace	k1gFnSc4	intonace
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
nebo	nebo	k8xC	nebo
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zavřená	zavřený	k2eAgFnSc1d1	zavřená
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
dvojhlásku	dvojhláska	k1gFnSc4	dvojhláska
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1	jiné
slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
intonaci	intonace	k1gFnSc4	intonace
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Ainu	Ainu	k6eAd1	Ainu
je	být	k5eAaImIp3nS	být
SOV	sova	k1gFnPc2	sova
jazyk	jazyk	k1gMnSc1	jazyk
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
slova	slovo	k1gNnPc4	slovo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
mají	mít	k5eAaImIp3nP	mít
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
:	:	kIx,	:
podmět	podmět	k1gInSc1	podmět
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
přísudek	přísudek	k1gInSc1	přísudek
<g/>
)	)	kIx)	)
s	s	k7c7	s
příklonkami	příklonka	k1gFnPc7	příklonka
<g/>
.	.	kIx.	.
</s>
<s>
Podmět	podmět	k1gInSc1	podmět
a	a	k8xC	a
předmět	předmět	k1gInSc1	předmět
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
označovány	označovat	k5eAaImNgFnP	označovat
právě	právě	k9	právě
příklonkami	příklonka	k1gFnPc7	příklonka
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
sdružováním	sdružování	k1gNnPc3	sdružování
modifikují	modifikovat	k5eAaBmIp3nP	modifikovat
jedno	jeden	k4xCgNnSc1	jeden
druhé	druhý	k4xOgFnSc6	druhý
<g/>
;	;	kIx,	;
klíčové	klíčový	k2eAgNnSc1d1	klíčové
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
vnitřně	vnitřně	k6eAd1	vnitřně
buďto	buďto	k8xC	buďto
přechodná	přechodný	k2eAgFnSc1d1	přechodná
nebo	nebo	k8xC	nebo
nepřechodná	přechodný	k2eNgFnSc1d1	nepřechodná
<g/>
,	,	kIx,	,
přijímají	přijímat	k5eAaImIp3nP	přijímat
různé	různý	k2eAgFnPc4d1	různá
odvozovací	odvozovací	k2eAgFnPc4d1	odvozovací
přípony	přípona	k1gFnPc4	přípona
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
polysyntetický	polysyntetický	k2eAgInSc4d1	polysyntetický
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
ainu	ainu	k6eAd1	ainu
psán	psán	k2eAgInSc1d1	psán
v	v	k7c6	v
modifikované	modifikovaný	k2eAgFnSc6d1	modifikovaná
verzi	verze	k1gFnSc6	verze
japonské	japonský	k2eAgFnSc6d1	japonská
slabikové	slabikový	k2eAgFnSc3d1	slabiková
abecedě	abeceda	k1gFnSc3	abeceda
katakana	katakana	k1gFnSc1	katakana
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
také	také	k9	také
abeceda	abeceda	k1gFnSc1	abeceda
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
latince	latinka	k1gFnSc6	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Ainu	Aina	k1gFnSc4	Aina
Times	Times	k1gInSc1	Times
jsou	být	k5eAaImIp3nP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Unicode	Unicod	k1gInSc5	Unicod
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Katakana	Katakana	k1gFnSc1	Katakana
Phonetic	Phonetice	k1gFnPc2	Phonetice
Extensions	Extensionsa	k1gFnPc2	Extensionsa
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
F	F	kA	F
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
FF	ff	kA	ff
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
znaky	znak	k1gInPc4	znak
katakana	katakana	k1gFnSc1	katakana
určené	určený	k2eAgFnPc1d1	určená
především	především	k9	především
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
ainu	ainus	k1gInSc2	ainus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ainu	ainus	k1gInSc6	ainus
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
katakana	katakana	k1gFnSc1	katakana
pro	pro	k7c4	pro
koncové	koncový	k2eAgFnPc4d1	koncová
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
však	však	k9	však
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
jazyk	jazyk	k1gInSc1	jazyk
Ainu	Ainus	k1gInSc2	Ainus
nezná	neznat	k5eAaImIp3nS	neznat
psanou	psaný	k2eAgFnSc4d1	psaná
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
lid	lid	k1gInSc4	lid
Ainu	Ainus	k1gInSc2	Ainus
nepoužíval	používat	k5eNaImAgMnS	používat
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ústní	ústní	k2eAgFnSc4d1	ústní
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
heslo	heslo	k1gNnSc4	heslo
Yukar	Yukara	k1gFnPc2	Yukara
<g/>
.	.	kIx.	.
</s>
<s>
Ainu	Ainu	k6eAd1	Ainu
má	mít	k5eAaImIp3nS	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
ústní	ústní	k2eAgFnSc4d1	ústní
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
hrdinské	hrdinský	k2eAgFnSc6d1	hrdinská
epice	epika	k1gFnSc6	epika
zvané	zvaný	k2eAgInPc1d1	zvaný
Yukar	Yukar	k1gInSc1	Yukar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
uchovalo	uchovat	k5eAaPmAgNnS	uchovat
mnoho	mnoho	k4c1	mnoho
gramatických	gramatický	k2eAgMnPc2d1	gramatický
a	a	k8xC	a
lexikálních	lexikální	k2eAgMnPc2d1	lexikální
archaismů	archaismus	k1gInPc2	archaismus
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
známá	známý	k2eAgNnPc4d1	známé
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnSc2	slovo
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
díky	díky	k7c3	díky
japonské	japonský	k2eAgFnSc3d1	japonská
počítačové	počítačový	k2eAgFnSc3d1	počítačová
hře	hra	k1gFnSc3	hra
Final	Final	k1gInSc4	Final
Fantasy	fantas	k1gInPc4	fantas
X	X	kA	X
<g/>
,	,	kIx,	,
ve	v	k7c4	v
která	který	k3yRgNnPc4	který
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
předměty	předmět	k1gInPc1	předmět
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Yuna	Yuna	k1gFnSc1	Yuna
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
květiny	květina	k1gFnSc2	květina
a	a	k8xC	a
Wakka	Wakko	k1gNnSc2	Wakko
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
