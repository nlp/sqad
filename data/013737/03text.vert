<s>
LOL	LOL	kA
(	(	kIx(
<g/>
Laughing	Laughing	k1gInSc1
Out	Out	k1gFnSc2
Loud	louda	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
LOL	LOL	kA
(	(	kIx(
<g/>
Laughing	Laughing	k1gInSc1
Out	Out	k1gFnSc2
Loud	louda	k1gFnPc2
<g/>
)	)	kIx)
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
LOL	LOL	kA
(	(	kIx(
<g/>
Laughing	Laughing	k1gInSc1
Out	Out	k1gFnSc2
Loud	louda	k1gFnPc2
<g/>
)	)	kIx)
Země	země	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
107	#num#	k4
minut	minuta	k1gFnPc2
Žánr	žánr	k1gInSc4
</s>
<s>
Komedie	komedie	k1gFnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Lisa	Lisa	k1gFnSc1
AzuelosDelgado	AzuelosDelgada	k1gFnSc5
Nans	Nansa	k1gFnPc2
Režie	režie	k1gFnSc1
</s>
<s>
Lisa	Lisa	k1gFnSc1
Azuelos	Azuelosa	k1gFnPc2
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Sophie	Sophie	k1gFnSc1
MarceauChrista	MarceauChrista	k1gMnSc1
TheretJérémy	TheretJérém	k1gInPc1
KaponeAlexandre	KaponeAlexandr	k1gInSc5
AstierJocelyn	AstierJocelyn	k1gInSc4
Quivrin	Quivrin	k1gInSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Lisa	Lisa	k1gFnSc1
Azuelos	Azuelosa	k1gFnPc2
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
Produkční	produkční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Pathé	Pathý	k2eAgInPc1d1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
€	€	k?
9	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
000	#num#	k4
Tržby	tržba	k1gFnPc1
</s>
<s>
€	€	k?
34	#num#	k4
<g/>
,	,	kIx,
807	#num#	k4
<g/>
,	,	kIx,
545	#num#	k4
(	(	kIx(
<g/>
celosvětově	celosvětově	k6eAd1
<g/>
)	)	kIx)
LOL	LOL	kA
(	(	kIx(
<g/>
Laughing	Laughing	k1gInSc1
Out	Out	k1gFnSc2
Loud	louda	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c4
ČSFDNěkterá	ČSFDNěkterý	k2eAgNnPc4d1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
LOL	LOL	kA
(	(	kIx(
<g/>
Laughing	Laughing	k1gInSc1
Out	Out	k6tL
Loud	Loud	k6tQ
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzská	francouzský	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
režírovaná	režírovaný	k2eAgFnSc1d1
Lisou	Lisa	k1gFnSc7
Azuelos	Azuelos	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
se	se	k3xPyFc4
ujala	ujmout	k5eAaPmAgFnS
Sophie	Sophie	k1gFnSc1
Marceau	Marceaa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
hraje	hrát	k5eAaImIp3nS
matku	matka	k1gFnSc4
dívky	dívka	k1gFnSc2
jménem	jméno	k1gNnSc7
Lola	Lola	k1gFnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
všichni	všechen	k3xTgMnPc1
známí	známý	k1gMnPc1
přezdívají	přezdívat	k5eAaImIp3nP
Lol	Lol	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
16	#num#	k4
a	a	k8xC
její	její	k3xOp3gInSc1
život	život	k1gInSc1
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
mezi	mezi	k7c7
studiemi	studie	k1gFnPc7
na	na	k7c6
prestižní	prestižní	k2eAgFnSc6d1
francouzské	francouzský	k2eAgFnSc6d1
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
tajným	tajný	k2eAgInSc7d1
deníčkem	deníček	k1gInSc7
<g/>
,	,	kIx,
jejími	její	k3xOp3gMnPc7
přáteli	přítel	k1gMnPc7
<g/>
,	,	kIx,
jejím	její	k3xOp3gMnSc7
klukem	kluk	k1gMnSc7
<g/>
,	,	kIx,
jejími	její	k3xOp3gMnPc7
rozvedenými	rozvedený	k2eAgMnPc7d1
rodiči	rodič	k1gMnPc7
<g/>
,	,	kIx,
drogami	droga	k1gFnPc7
a	a	k8xC
její	její	k3xOp3gFnSc7
sexualitou	sexualita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
film	film	k1gInSc1
je	být	k5eAaImIp3nS
remakem	remak	k1gInSc7
filmu	film	k1gInSc2
Večírek	večírek	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
Sophie	Sophie	k1gFnSc1
Marceau	Marceaa	k1gFnSc4
nehrála	hrát	k5eNaImAgFnS
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
právě	právě	k9
onu	onen	k3xDgFnSc4
dospívající	dospívající	k2eAgFnSc4d1
dceru	dcera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Sophie	Sophie	k1gFnSc1
Marceau	Marceaus	k1gInSc2
-	-	kIx~
Anne	Anne	k1gFnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
Loly	Lola	k1gFnSc2
</s>
<s>
Christa	Christa	k1gMnSc1
Theret	Theret	k1gMnSc1
-	-	kIx~
Lola	Lola	k1gMnSc1
</s>
<s>
Jérémy	Jérém	k1gInPc1
Kapone	Kapon	k1gInSc5
-	-	kIx~
Maël	Maël	k1gMnSc1
<g/>
,	,	kIx,
kluk	kluk	k1gMnSc1
Loly	Lola	k1gFnSc2
</s>
<s>
Alexandre	Alexandr	k1gInSc5
Astier	Astier	k1gMnSc1
-	-	kIx~
Alain	Alain	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Loly	Lola	k1gFnSc2
</s>
<s>
Jocelyn	Jocelyn	k1gInSc1
Quivrin	Quivrin	k1gInSc1
-	-	kIx~
Lucas	Lucas	k1gMnSc1
<g/>
,	,	kIx,
přítel	přítel	k1gMnSc1
Anne	Ann	k1gFnSc2
</s>
<s>
Lou	Lou	k?
Lesage	Lesage	k1gNnSc1
-	-	kIx~
Stephane	Stephan	k1gMnSc5
<g/>
,	,	kIx,
kamarádka	kamarádek	k1gMnSc2
Loly	Lola	k1gMnSc2
</s>
<s>
Marion	Marion	k1gInSc1
Chabassol	Chabassol	k1gInSc1
-	-	kIx~
Charlotte	Charlott	k1gMnSc5
<g/>
,	,	kIx,
kamarádka	kamarádek	k1gMnSc2
Loly	Lola	k1gMnSc2
</s>
<s>
Félix	Félix	k1gInSc1
Moati	Moať	k1gFnSc2
-	-	kIx~
Arthur	Arthur	k1gMnSc1
<g/>
,	,	kIx,
ex-přítel	ex-přítel	k1gMnSc1
Loly	Lola	k1gFnSc2
</s>
<s>
Remake	Remake	k6eAd1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Remake	Remake	k1gNnSc1
tohoto	tento	k3xDgInSc2
filmu	film	k1gInSc2
s	s	k7c7
názvem	název	k1gInSc7
LOL	LOL	kA
bude	být	k5eAaImBp3nS
vypuštěn	vypuštěn	k2eAgInSc1d1
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budou	být	k5eAaImBp3nP
v	v	k7c6
něm	on	k3xPp3gInSc6
hrát	hrát	k5eAaImF
hvězdy	hvězda	k1gFnPc4
jako	jako	k8xS,k8xC
např.	např.	kA
Demi	Dem	k1gMnSc5
Moore	Moor	k1gMnSc5
<g/>
,	,	kIx,
Miley	Mileum	k1gNnPc7
Cyrus	Cyrus	k1gInSc1
<g/>
,	,	kIx,
Ashley	Ashley	k1gInPc1
Greene	Green	k1gInSc5
a	a	k8xC
Adam	Adam	k1gMnSc1
Sevani	Sevan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
