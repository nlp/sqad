<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Nigeru	Niger	k1gInSc2	Niger
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
(	(	kIx(	(
<g/>
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
<g/>
)	)	kIx)	)
oranžové	oranžový	k2eAgFnPc4d1	oranžová
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc4d1	bílá
a	a	k8xC	a
zelené	zelený	k2eAgFnPc4d1	zelená
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oranžová	oranžový	k2eAgFnSc1d1	oranžová
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
mír	mír	k1gInSc4	mír
a	a	k8xC	a
čistotu	čistota	k1gFnSc4	čistota
a	a	k8xC	a
zelená	zelenat	k5eAaImIp3nS	zelenat
naději	naděje	k1gFnSc4	naděje
a	a	k8xC	a
též	též	k9	též
úrodná	úrodný	k2eAgNnPc4d1	úrodné
území	území	k1gNnPc4	území
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Nigeru	Niger	k1gInSc2	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
prostředního	prostřední	k2eAgInSc2d1	prostřední
pruhu	pruh	k1gInSc2	pruh
je	být	k5eAaImIp3nS	být
oranžový	oranžový	k2eAgInSc1d1	oranžový
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
představující	představující	k2eAgNnSc1d1	představující
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
netradiční	tradiční	k2eNgInSc4d1	netradiční
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
pramenech	pramen	k1gInPc6	pramen
nebylo	být	k5eNaImAgNnS	být
dohledáno	dohledán	k2eAgNnSc1d1	dohledán
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc4	tento
poměr	poměr	k1gInSc4	poměr
nějaký	nějaký	k3yIgInSc4	nějaký
význam	význam	k1gInSc4	význam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
však	však	k9	však
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
nigerskou	nigerský	k2eAgFnSc7d1	Nigerská
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
tiskových	tiskový	k2eAgInPc6d1	tiskový
výstupech	výstup	k1gInPc6	výstup
nekonzistentně	konzistentně	k6eNd1	konzistentně
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Flag	flag	k1gInSc1	flag
of	of	k?	of
Niger	Niger	k1gInSc1	Niger
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Nigeru	Niger	k1gInSc2	Niger
</s>
</p>
<p>
<s>
Nigerská	nigerský	k2eAgFnSc1d1	Nigerská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nigerská	nigerský	k2eAgFnSc1d1	Nigerská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
