<p>
<s>
Léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
(	(	kIx(	(
<g/>
léčivka	léčivka	k1gFnSc1	léčivka
či	či	k8xC	či
léčivá	léčivý	k2eAgFnSc1d1	léčivá
bylina	bylina	k1gFnSc1	bylina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
účinné	účinný	k2eAgFnPc4d1	účinná
látky	látka	k1gFnPc4	látka
příznivě	příznivě	k6eAd1	příznivě
ovlivňující	ovlivňující	k2eAgInSc4d1	ovlivňující
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
stav	stav	k1gInSc4	stav
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
pomocí	pomocí	k7c2	pomocí
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
fytoterapie	fytoterapie	k1gFnSc1	fytoterapie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
jsou	být	k5eAaImIp3nP	být
užívány	užíván	k2eAgInPc1d1	užíván
tzv.	tzv.	kA	tzv.
drogy	droga	k1gFnPc1	droga
–	–	k?	–
určité	určitý	k2eAgFnPc1d1	určitá
části	část	k1gFnPc1	část
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Databázi	databáze	k1gFnSc4	databáze
v	v	k7c6	v
lékárnictví	lékárnictví	k1gNnSc6	lékárnictví
takto	takto	k6eAd1	takto
využívaných	využívaný	k2eAgFnPc2d1	využívaná
drog	droga	k1gFnPc2	droga
popisují	popisovat	k5eAaImIp3nP	popisovat
lékopisy	lékopis	k1gInPc1	lékopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
parametry	parametr	k1gInPc4	parametr
oficiální	oficiální	k2eAgFnSc2d1	oficiální
drogy	droga	k1gFnSc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Lékopisy	lékopis	k1gInPc1	lékopis
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
léčivé	léčivý	k2eAgFnPc4d1	léčivá
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
700	[number]	k4	700
taxonů	taxon	k1gInPc2	taxon
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc4	staletí
k	k	k7c3	k
léčebným	léčebný	k2eAgInPc3d1	léčebný
účelům	účel	k1gInPc3	účel
užívalo	užívat	k5eAaImAgNnS	užívat
asi	asi	k9	asi
800	[number]	k4	800
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
takto	takto	k6eAd1	takto
používá	používat	k5eAaImIp3nS	používat
asi	asi	k9	asi
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
250	[number]	k4	250
000	[number]	k4	000
známých	známý	k2eAgInPc2d1	známý
druhů	druh	k1gInPc2	druh
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
k	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
používáno	používán	k2eAgNnSc1d1	používáno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alkaloidy	alkaloid	k1gInPc4	alkaloid
===	===	k?	===
</s>
</p>
<p>
<s>
Alkaloidy	alkaloid	k1gInPc1	alkaloid
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgFnPc4d1	dusíkatá
látky	látka	k1gFnPc4	látka
zásadité	zásaditý	k2eAgFnSc2d1	zásaditá
povahy	povaha	k1gFnSc2	povaha
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
metabolity	metabolit	k1gInPc1	metabolit
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atomů	atom	k1gInPc2	atom
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
většinou	většinou	k6eAd1	většinou
nejsou	být	k5eNaImIp3nP	být
volné	volný	k2eAgFnPc1d1	volná
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k8xC	jako
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
silné	silný	k2eAgInPc4d1	silný
fyziologické	fyziologický	k2eAgInPc4d1	fyziologický
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
jsou	být	k5eAaImIp3nP	být
jedovaté	jedovatý	k2eAgInPc4d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
zpravidla	zpravidla	k6eAd1	zpravidla
nebývá	bývat	k5eNaImIp3nS	bývat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
alkaloid	alkaloid	k1gInSc1	alkaloid
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
strukturálně	strukturálně	k6eAd1	strukturálně
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
významného	významný	k2eAgInSc2d1	významný
farmakologického	farmakologický	k2eAgInSc2d1	farmakologický
účinku	účinek	k1gInSc2	účinek
jsou	být	k5eAaImIp3nP	být
alkaloidy	alkaloid	k1gInPc1	alkaloid
poměrně	poměrně	k6eAd1	poměrně
detailně	detailně	k6eAd1	detailně
prozkoumány	prozkoumat	k5eAaPmNgInP	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
řádu	řád	k1gInSc2	řád
liliotvarých	liliotvarý	k2eAgInPc2d1	liliotvarý
<g/>
,	,	kIx,	,
ocún	ocún	k1gInSc1	ocún
<g/>
,	,	kIx,	,
kýchavice	kýchavice	k1gFnSc1	kýchavice
<g/>
,	,	kIx,	,
u	u	k7c2	u
čeledi	čeleď	k1gFnSc2	čeleď
makovitých	makovití	k1gMnPc2	makovití
<g/>
,	,	kIx,	,
mák	mák	k1gMnSc1	mák
–	–	k?	–
alkaloidy	alkaloid	k1gInPc1	alkaloid
morfin	morfin	k1gInSc1	morfin
<g/>
,	,	kIx,	,
papaverin	papaverin	k1gInSc1	papaverin
<g/>
,	,	kIx,	,
kodein	kodein	k1gInSc1	kodein
<g/>
,	,	kIx,	,
tebain	tebain	k1gInSc1	tebain
a	a	k8xC	a
cca	cca	kA	cca
20	[number]	k4	20
dalších	další	k2eAgInPc2d1	další
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
;	;	kIx,	;
vlaštovičník	vlaštovičník	k1gInSc1	vlaštovičník
–	–	k?	–
alkaloid	alkaloid	k1gInSc1	alkaloid
chelidonin	chelidonin	k2eAgMnSc1d1	chelidonin
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
15	[number]	k4	15
dalších	další	k2eAgFnPc2d1	další
<g/>
;	;	kIx,	;
pryskyřníkovitých	pryskyřníkovitá	k1gFnPc2	pryskyřníkovitá
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
oměj	oměj	k1gInSc1	oměj
–	–	k?	–
silně	silně	k6eAd1	silně
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
alkaloid	alkaloid	k1gInSc4	alkaloid
aconitin	aconitina	k1gFnPc2	aconitina
<g/>
;	;	kIx,	;
lilkovitých	lilkovitý	k2eAgFnPc2d1	lilkovitá
<g/>
,	,	kIx,	,
lilek	lilek	k1gInSc1	lilek
<g/>
,	,	kIx,	,
rulík	rulík	k1gInSc1	rulík
–	–	k?	–
alkaloid	alkaloid	k1gInSc1	alkaloid
atropin	atropin	k1gInSc1	atropin
<g/>
,	,	kIx,	,
<g/>
scopolamin	scopolamin	k1gInSc1	scopolamin
<g/>
;	;	kIx,	;
blín	blín	k1gInSc1	blín
<g/>
,	,	kIx,	,
paprika	paprika	k1gFnSc1	paprika
<g/>
;	;	kIx,	;
hvězdnicovitých	hvězdnicovitý	k2eAgMnPc2d1	hvězdnicovitý
bělotrn	bělotrn	k1gInSc4	bělotrn
–	–	k?	–
nejedovatý	jedovatý	k2eNgInSc1d1	nejedovatý
alkaloid	alkaloid	k1gInSc1	alkaloid
echinopsin	echinopsin	k2eAgInSc1d1	echinopsin
<g/>
;	;	kIx,	;
Největší	veliký	k2eAgInSc1d3	veliký
obsah	obsah	k1gInSc1	obsah
alkaloidů	alkaloid	k1gInPc2	alkaloid
mají	mít	k5eAaImIp3nP	mít
rostliny	rostlina	k1gFnPc1	rostlina
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
rozkvětem	rozkvět	k1gInSc7	rozkvět
nebo	nebo	k8xC	nebo
v	v	k7c6	v
začátku	začátek	k1gInSc6	začátek
květu	květ	k1gInSc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
alkaloidy	alkaloid	k1gInPc4	alkaloid
nejbohatšími	bohatý	k2eAgFnPc7d3	nejbohatší
částmi	část	k1gFnPc7	část
jsou	být	k5eAaImIp3nP	být
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
kůra	kůra	k1gFnSc1	kůra
a	a	k8xC	a
kořeny	kořen	k1gInPc1	kořen
<g/>
;	;	kIx,	;
květy	květ	k1gInPc1	květ
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
mají	mít	k5eAaImIp3nP	mít
obsah	obsah	k1gInSc4	obsah
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsah	obsah	k1gInSc1	obsah
alkaloidů	alkaloid	k1gInPc2	alkaloid
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
i	i	k8xC	i
pohyby	pohyb	k1gInPc1	pohyb
alkaloidů	alkaloid	k1gInPc2	alkaloid
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
částmi	část	k1gFnPc7	část
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
aktivní	aktivní	k2eAgFnSc1d1	aktivní
úloha	úloha	k1gFnSc1	úloha
alkaloidů	alkaloid	k1gInPc2	alkaloid
v	v	k7c6	v
metabolizmu	metabolizmus	k1gInSc6	metabolizmus
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
funkce	funkce	k1gFnSc1	funkce
alkaloidů	alkaloid	k1gInPc2	alkaloid
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
ještě	ještě	k6eAd1	ještě
zcela	zcela	k6eAd1	zcela
plně	plně	k6eAd1	plně
objasněna	objasnit	k5eAaPmNgFnS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Alkaloidy	alkaloid	k1gInPc4	alkaloid
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
sklerocium	sklerocium	k1gNnSc4	sklerocium
paličkovice	paličkovice	k1gFnSc2	paličkovice
nachové-námel	nachovéámel	k1gInSc4	nachové-námel
–	–	k?	–
ergolinové	ergolinový	k2eAgInPc4d1	ergolinový
alkaloidy	alkaloid	k1gInPc4	alkaloid
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
alkaloid	alkaloid	k1gInSc1	alkaloid
úspěšně	úspěšně	k6eAd1	úspěšně
izoloval	izolovat	k5eAaBmAgInS	izolovat
Friedrich	Friedrich	k1gMnSc1	Friedrich
W.	W.	kA	W.
A.	A.	kA	A.
Sertürner	Sertürner	k1gMnSc1	Sertürner
v	v	k7c6	v
r.	r.	kA	r.
1803	[number]	k4	1803
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
morfin	morfin	k1gInSc4	morfin
jakožto	jakožto	k8xS	jakožto
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
alkaloid	alkaloid	k1gInSc4	alkaloid
z	z	k7c2	z
máku	mák	k1gInSc2	mák
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
alkaloidy	alkaloid	k1gInPc1	alkaloid
<g/>
:	:	kIx,	:
strychnin	strychnin	k1gInSc1	strychnin
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
chinin	chinin	k1gInSc1	chinin
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
koniin	koniin	k1gInSc1	koniin
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nikotin	nikotin	k1gInSc1	nikotin
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
atropin	atropin	k1gInSc1	atropin
<g/>
,	,	kIx,	,
kolchicin	kolchicin	k1gInSc1	kolchicin
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Glykosidy	glykosid	k1gInPc4	glykosid
===	===	k?	===
</s>
</p>
<p>
<s>
Glykosidy	glykosid	k1gInPc1	glykosid
jsou	být	k5eAaImIp3nP	být
esterové	esterový	k2eAgInPc1d1	esterový
deriváty	derivát	k1gInPc1	derivát
sacharidů	sacharid	k1gInPc2	sacharid
působící	působící	k2eAgInPc1d1	působící
především	především	k6eAd1	především
svou	svůj	k3xOyFgFnSc7	svůj
nesacharidovou	sacharidový	k2eNgFnSc7d1	sacharidový
složkou	složka	k1gFnSc7	složka
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
glykosidů	glykosid	k1gInPc2	glykosid
v	v	k7c6	v
droze	droga	k1gFnSc6	droga
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
době	doba	k1gFnSc6	doba
sklizně	sklizeň	k1gFnSc2	sklizeň
a	a	k8xC	a
vhodném	vhodný	k2eAgInSc6d1	vhodný
způsobu	způsob	k1gInSc6	způsob
sušení	sušení	k1gNnSc2	sušení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srdeční	srdeční	k2eAgInPc1d1	srdeční
glykosidy	glykosid	k1gInPc1	glykosid
–	–	k?	–
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
<g/>
které	který	k3yQgNnSc4	který
je	on	k3xPp3gNnSc4	on
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jsou	být	k5eAaImIp3nP	být
prudce	prudko	k6eAd1	prudko
jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
(	(	kIx(	(
<g/>
náprstník	náprstník	k1gInSc1	náprstník
<g/>
,	,	kIx,	,
konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
<g/>
,	,	kIx,	,
hlaváček	hlaváček	k1gInSc1	hlaváček
jarní	jarní	k2eAgInSc1d1	jarní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fenolické	fenolický	k2eAgInPc1d1	fenolický
glykosidy	glykosid	k1gInPc1	glykosid
–	–	k?	–
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
desinfekci	desinfekce	k1gFnSc3	desinfekce
močových	močový	k2eAgFnPc2d1	močová
cest	cesta	k1gFnPc2	cesta
při	při	k7c6	při
zánětech	zánět	k1gInPc6	zánět
(	(	kIx(	(
<g/>
medvědice	medvědice	k1gFnPc1	medvědice
<g/>
,	,	kIx,	,
brusinka	brusinka	k1gFnSc1	brusinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antrachinonové	Antrachinonový	k2eAgInPc1d1	Antrachinonový
glykosidy	glykosid	k1gInPc1	glykosid
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
projímavé	projímavý	k2eAgInPc1d1	projímavý
účinky	účinek	k1gInPc1	účinek
(	(	kIx(	(
<g/>
krušina	krušina	k1gFnSc1	krušina
olšová	olšový	k2eAgFnSc1d1	olšová
<g/>
,	,	kIx,	,
reveň	reveň	k1gFnSc1	reveň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Thioglykosidy	Thioglykosida	k1gFnPc1	Thioglykosida
–	–	k?	–
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
trávení	trávení	k1gNnSc1	trávení
(	(	kIx(	(
<g/>
křen	křen	k1gInSc1	křen
<g/>
,	,	kIx,	,
hořčice	hořčice	k1gFnSc1	hořčice
<g/>
,	,	kIx,	,
lichořeřišnice	lichořeřišnice	k1gFnSc1	lichořeřišnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flavonoidy	flavonoid	k1gInPc4	flavonoid
===	===	k?	===
</s>
</p>
<p>
<s>
Flavonoidy	flavonoid	k1gInPc1	flavonoid
jsou	být	k5eAaImIp3nP	být
fenolytické	fenolytický	k2eAgFnPc4d1	fenolytický
látky	látka	k1gFnPc4	látka
náležející	náležející	k2eAgFnPc4d1	náležející
mezi	mezi	k7c4	mezi
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
sekundární	sekundární	k2eAgInPc4d1	sekundární
metabolity	metabolit	k1gInPc4	metabolit
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podobné	podobný	k2eAgFnPc4d1	podobná
vitamínům	vitamín	k1gInPc3	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celek	k1gInSc7	celek
k	k	k7c3	k
flavonoidům	flavonoid	k1gInPc3	flavonoid
patří	patřit	k5eAaImIp3nS	patřit
přes	přes	k7c4	přes
60	[number]	k4	60
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
výrazné	výrazný	k2eAgInPc4d1	výrazný
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
biologické	biologický	k2eAgInPc4d1	biologický
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
bioflavonoidy	bioflavonoida	k1gFnPc1	bioflavonoida
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
chemické	chemický	k2eAgFnSc2d1	chemická
struktury	struktura	k1gFnSc2	struktura
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
flavony	flavon	k1gInPc4	flavon
<g/>
,	,	kIx,	,
flavonoly	flavonol	k1gInPc4	flavonol
<g/>
,	,	kIx,	,
flavanony	flavanon	k1gInPc4	flavanon
<g/>
,	,	kIx,	,
antokyanidiny	antokyanidin	k1gInPc4	antokyanidin
<g/>
,	,	kIx,	,
katechiny	katechina	k1gFnPc4	katechina
atd.	atd.	kA	atd.
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
biologických	biologický	k2eAgInPc2d1	biologický
účinků	účinek	k1gInPc2	účinek
flavonoidů	flavonoid	k1gInPc2	flavonoid
jsou	být	k5eAaImIp3nP	být
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
zejména	zejména	k9	zejména
účinky	účinek	k1gInPc1	účinek
antisklerotické	antisklerotický	k2eAgInPc1d1	antisklerotický
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kladné	kladný	k2eAgNnSc1d1	kladné
působení	působení	k1gNnSc1	působení
na	na	k7c4	na
pružnost	pružnost	k1gFnSc4	pružnost
a	a	k8xC	a
pevnost	pevnost	k1gFnSc4	pevnost
cévních	cévní	k2eAgFnPc2d1	cévní
stěn	stěna	k1gFnPc2	stěna
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zlepšování	zlepšování	k1gNnSc2	zlepšování
jejich	jejich	k3xOp3gFnSc2	jejich
prostupnosti	prostupnost	k1gFnSc2	prostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
o	o	k7c6	o
kancerostatickém	kancerostatický	k2eAgNnSc6d1	kancerostatické
působení	působení	k1gNnSc6	působení
některých	některý	k3yIgInPc2	některý
flavonoidů	flavonoid	k1gInPc2	flavonoid
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
je	být	k5eAaImIp3nS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
regenerační	regenerační	k2eAgInSc1d1	regenerační
účinek	účinek	k1gInSc1	účinek
na	na	k7c4	na
jaterní	jaterní	k2eAgFnPc4d1	jaterní
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
účinek	účinek	k1gInSc1	účinek
uroseptický	uroseptický	k2eAgInSc1d1	uroseptický
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
jako	jako	k9	jako
významná	významný	k2eAgFnSc1d1	významná
spasmolytika	spasmolytika	k1gFnSc1	spasmolytika
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
pak	pak	k6eAd1	pak
obecně	obecně	k6eAd1	obecně
jako	jako	k8xC	jako
geriatrika	geriatrika	k1gFnSc1	geriatrika
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
protektivní	protektivní	k2eAgInSc4d1	protektivní
a	a	k8xC	a
regenerační	regenerační	k2eAgInSc4d1	regenerační
účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
stárnoucí	stárnoucí	k2eAgInSc4d1	stárnoucí
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zdroje	zdroj	k1gInPc4	zdroj
flavonoidů	flavonoid	k1gInPc2	flavonoid
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
nať	nať	k1gFnSc4	nať
routy	routa	k1gFnSc2	routa
vonné	vonný	k2eAgFnPc1d1	vonná
<g/>
,	,	kIx,	,
nať	nať	k1gFnSc1	nať
pohanky	pohanka	k1gFnPc1	pohanka
<g/>
,	,	kIx,	,
květ	květ	k1gInSc1	květ
černého	černý	k2eAgInSc2d1	černý
bezu	bez	k1gInSc2	bez
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
drogy	droga	k1gFnPc1	droga
z	z	k7c2	z
hlohu	hloh	k1gInSc2	hloh
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
flavonoidy	flavonoid	k1gInPc1	flavonoid
působí	působit	k5eAaImIp3nP	působit
silně	silně	k6eAd1	silně
antioxidačně	antioxidačně	k6eAd1	antioxidačně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zdroje	zdroj	k1gInPc4	zdroj
nejsilnějších	silný	k2eAgInPc2d3	nejsilnější
antioxidantů	antioxidant	k1gInPc2	antioxidant
patří	patřit	k5eAaImIp3nP	patřit
borůvky	borůvka	k1gFnPc1	borůvka
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
ostružiny	ostružina	k1gFnPc1	ostružina
<g/>
,	,	kIx,	,
maliny	malina	k1gFnPc1	malina
<g/>
,	,	kIx,	,
malinoostružiny	malinoostružina	k1gFnPc1	malinoostružina
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
bez	bez	k1gInSc1	bez
<g/>
,	,	kIx,	,
hloh	hloh	k1gInSc1	hloh
<g/>
,	,	kIx,	,
třezalka	třezalka	k1gFnSc1	třezalka
<g/>
,	,	kIx,	,
lékořice	lékořice	k1gFnSc1	lékořice
<g/>
,	,	kIx,	,
aronie	aronie	k1gFnSc1	aronie
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
grapefruity	grapefruit	k1gInPc1	grapefruit
<g/>
,	,	kIx,	,
nerafinovaný	rafinovaný	k2eNgInSc1d1	nerafinovaný
olivový	olivový	k2eAgInSc1d1	olivový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
pohanka	pohanka	k1gFnSc1	pohanka
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
rajčata	rajče	k1gNnPc4	rajče
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
cibule	cibule	k1gFnSc2	cibule
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
například	například	k6eAd1	například
má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
antioxidantu	antioxidant	k1gInSc2	antioxidant
kvercetinu	kvercetin	k2eAgFnSc4d1	kvercetin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
brzdí	brzdit	k5eAaImIp3nS	brzdit
růst	růst	k1gInSc1	růst
rakovinných	rakovinný	k2eAgFnPc2d1	rakovinná
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
protizánětlivé	protizánětlivý	k2eAgInPc4d1	protizánětlivý
<g/>
,	,	kIx,	,
antivirové	antivirový	k2eAgInPc4d1	antivirový
a	a	k8xC	a
antibiotické	antibiotický	k2eAgInPc4d1	antibiotický
účinky	účinek	k1gInPc4	účinek
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
snižuje	snižovat	k5eAaImIp3nS	snižovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
ředí	ředit	k5eAaImIp3nP	ředit
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
omezuje	omezovat	k5eAaImIp3nS	omezovat
riziko	riziko	k1gNnSc1	riziko
embolií	embolie	k1gFnPc2	embolie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hormony	hormon	k1gInPc4	hormon
===	===	k?	===
</s>
</p>
<p>
<s>
Hormony	hormon	k1gInPc1	hormon
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
chemickými	chemický	k2eAgInPc7d1	chemický
regulátory	regulátor	k1gInPc7	regulátor
funkcí	funkce	k1gFnPc2	funkce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tělesných	tělesný	k2eAgInPc2d1	tělesný
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
vyváženého	vyvážený	k2eAgNnSc2d1	vyvážené
působení	působení	k1gNnSc2	působení
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1	mnohobuněčný
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
rostlin	rostlina	k1gFnPc2	rostlina
i	i	k8xC	i
živočichů	živočich	k1gMnPc2	živočich
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
chemický	chemický	k2eAgMnSc1d1	chemický
posel	posel	k1gMnSc1	posel
informace	informace	k1gFnSc2	informace
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnPc1	skupina
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hlediska	hledisko	k1gNnSc2	hledisko
je	on	k3xPp3gNnPc4	on
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
deriváty	derivát	k1gInPc4	derivát
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
oligopeptidy	oligopeptid	k1gInPc1	oligopeptid
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc1d2	menší
polypeptidy	polypeptid	k1gInPc1	polypeptid
bílkovinného	bílkovinný	k2eAgInSc2d1	bílkovinný
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
steroidy	steroid	k1gInPc1	steroid
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc1	hormon
mají	mít	k5eAaImIp3nP	mít
příznivý	příznivý	k2eAgInSc4d1	příznivý
biologický	biologický	k2eAgInSc4d1	biologický
účinek	účinek	k1gInSc4	účinek
i	i	k9	i
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
podmínkou	podmínka	k1gFnSc7	podmínka
správné	správný	k2eAgFnSc2d1	správná
funkce	funkce	k1gFnSc2	funkce
celého	celý	k2eAgInSc2d1	celý
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc2	on
dobrého	dobrý	k2eAgInSc2d1	dobrý
fyzického	fyzický	k2eAgInSc2d1	fyzický
i	i	k8xC	i
psychického	psychický	k2eAgInSc2d1	psychický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
podobné	podobný	k2eAgFnPc1d1	podobná
ženským	ženský	k2eAgInPc3d1	ženský
pohlavním	pohlavní	k2eAgInPc3d1	pohlavní
hormonům	hormon	k1gInPc3	hormon
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
například	například	k6eAd1	například
v	v	k7c6	v
lékořici	lékořice	k1gFnSc6	lékořice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
chmelu	chmel	k1gInSc6	chmel
<g/>
,	,	kIx,	,
v	v	k7c6	v
květu	květ	k1gInSc6	květ
jeřábu	jeřáb	k1gInSc2	jeřáb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
je	on	k3xPp3gFnPc4	on
i	i	k8xC	i
brambory	brambor	k1gInPc4	brambor
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
podobné	podobný	k2eAgFnPc1d1	podobná
mužským	mužský	k2eAgInPc3d1	mužský
pohlavním	pohlavní	k2eAgInPc3d1	pohlavní
hormonům	hormon	k1gInPc3	hormon
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
např.	např.	kA	např.
plod	plod	k1gInSc4	plod
kaštanu	kaštan	k1gInSc2	kaštan
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
podobné	podobný	k2eAgFnPc1d1	podobná
dalším	další	k2eAgMnPc3d1	další
steroidům	steroid	k1gInPc3	steroid
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
v	v	k7c6	v
lékořici	lékořice	k1gFnSc6	lékořice
<g/>
,	,	kIx,	,
kořeni	kořen	k1gInSc6	kořen
jehlice	jehlice	k1gFnSc2	jehlice
trnité	trnitý	k2eAgNnSc1d1	trnité
(	(	kIx(	(
<g/>
Ononis	Ononis	k1gFnSc1	Ononis
spinosa	spinosa	k1gFnSc1	spinosa
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Enzymy	enzym	k1gInPc1	enzym
===	===	k?	===
</s>
</p>
<p>
<s>
Enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc4d1	důležitá
látky	látka	k1gFnPc4	látka
bílkovinného	bílkovinný	k2eAgInSc2d1	bílkovinný
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
katalyzátory	katalyzátor	k1gInPc1	katalyzátor
katalyzátory	katalyzátor	k1gInPc7	katalyzátor
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
biologických	biologický	k2eAgInPc6d1	biologický
procesech	proces	k1gInPc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
známý	známý	k2eAgInSc1d1	známý
pepsin	pepsin	k1gInSc1	pepsin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
reguluje	regulovat	k5eAaImIp3nS	regulovat
trávící	trávící	k2eAgInPc4d1	trávící
pochody	pochod	k1gInPc4	pochod
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
peristaltiku	peristaltika	k1gFnSc4	peristaltika
<g/>
;	;	kIx,	;
trypsin	trypsin	k1gInSc1	trypsin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
odumřelé	odumřelý	k2eAgFnPc4d1	odumřelá
tkáně	tkáň	k1gFnPc4	tkáň
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
detoxikačně	detoxikačně	k6eAd1	detoxikačně
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
trombin	trombin	k1gInSc4	trombin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
srážlivost	srážlivost	k1gFnSc4	srážlivost
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
enzymy	enzym	k1gInPc1	enzym
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
při	při	k7c6	při
rozkladu	rozklad	k1gInSc6	rozklad
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
protázy	protáza	k1gFnPc1	protáza
a	a	k8xC	a
proteinázy	proteináza	k1gFnPc1	proteináza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
v	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
ovoci	ovoce	k1gNnSc6	ovoce
jako	jako	k8xC	jako
ananas	ananas	k1gInSc1	ananas
<g/>
,	,	kIx,	,
papaya	papayum	k1gNnPc1	papayum
<g/>
,	,	kIx,	,
fíky	fík	k1gInPc1	fík
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
nazýváme	nazývat	k5eAaImIp1nP	nazývat
podle	podle	k7c2	podle
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
vyhrazena	vyhrazen	k2eAgFnSc1d1	vyhrazena
koncovka	koncovka	k1gFnSc1	koncovka
-asa	sa	k1gFnSc1	-asa
(	(	kIx(	(
<g/>
áza	áza	k?	áza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
glukosidázy	glukosidáza	k1gFnPc1	glukosidáza
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
katalyzátory	katalyzátor	k1gInPc1	katalyzátor
při	při	k7c6	při
štěpení	štěpení	k1gNnSc6	štěpení
polysacharidů	polysacharid	k1gInPc2	polysacharid
atd.	atd.	kA	atd.
Enzymy	enzym	k1gInPc1	enzym
se	se	k3xPyFc4	se
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
zúčastňují	zúčastňovat	k5eAaImIp3nP	zúčastňovat
většiny	většina	k1gFnPc1	většina
životních	životní	k2eAgInPc2d1	životní
dějů	děj	k1gInPc2	děj
a	a	k8xC	a
pochodů	pochod	k1gInPc2	pochod
<g/>
;	;	kIx,	;
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tělo	tělo	k1gNnSc1	tělo
enzymy	enzym	k1gInPc4	enzym
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
efektivně	efektivně	k6eAd1	efektivně
využilo	využít	k5eAaPmAgNnS	využít
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
bychom	by	kYmCp1nP	by
přednostně	přednostně	k6eAd1	přednostně
používat	používat	k5eAaImF	používat
byliny	bylina	k1gFnPc4	bylina
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
v	v	k7c6	v
surovém	surový	k2eAgInSc6d1	surový
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
šťávy	šťáva	k1gFnSc2	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Správným	správný	k2eAgNnSc7d1	správné
sušením	sušení	k1gNnSc7	sušení
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
většina	většina	k1gFnSc1	většina
enzymů	enzym	k1gInPc2	enzym
zachová	zachovat	k5eAaPmIp3nS	zachovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
již	již	k6eAd1	již
nižší	nízký	k2eAgFnSc4d2	nižší
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
varem	var	k1gInSc7	var
léčivé	léčivý	k2eAgFnPc4d1	léčivá
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
čaje	čaj	k1gInSc2	čaj
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
enzymy	enzym	k1gInPc1	enzym
zničí	zničit	k5eAaPmIp3nP	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Silice	silice	k1gFnPc1	silice
neboli	neboli	k8xC	neboli
éterické	éterický	k2eAgInPc1d1	éterický
oleje	olej	k1gInPc1	olej
===	===	k?	===
</s>
</p>
<p>
<s>
Silice	silice	k1gFnPc1	silice
jsou	být	k5eAaImIp3nP	být
vonné	vonný	k2eAgFnPc1d1	vonná
olejovité	olejovitý	k2eAgFnPc1d1	olejovitá
látky	látka	k1gFnPc1	látka
nerozpustné	rozpustný	k2eNgFnPc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Siličné	siličný	k2eAgFnPc1d1	siličná
drogy	droga	k1gFnPc1	droga
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
jako	jako	k9	jako
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
trávení	trávení	k1gNnSc1	trávení
–	–	k?	–
(	(	kIx(	(
<g/>
fenykl	fenykl	k1gInSc1	fenykl
<g/>
,	,	kIx,	,
anýz	anýz	k1gInSc1	anýz
<g/>
,	,	kIx,	,
kmín	kmín	k1gInSc1	kmín
kořenný	kořenný	k2eAgInSc1d1	kořenný
<g/>
,	,	kIx,	,
hřebíček	hřebíček	k1gInSc1	hřebíček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Močopudné	močopudný	k2eAgInPc1d1	močopudný
–	–	k?	–
(	(	kIx(	(
<g/>
jalovec	jalovec	k1gInSc1	jalovec
<g/>
,	,	kIx,	,
celer	celer	k1gInSc1	celer
<g/>
,	,	kIx,	,
petržel	petržel	k1gFnSc1	petržel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uklidňující	uklidňující	k2eAgFnSc1d1	uklidňující
–	–	k?	–
(	(	kIx(	(
<g/>
meduňka	meduňka	k1gFnSc1	meduňka
<g/>
,	,	kIx,	,
kozlík	kozlík	k1gInSc1	kozlík
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
,	,	kIx,	,
dobromysl	dobromysl	k1gFnSc1	dobromysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Desinfekční	desinfekční	k2eAgFnSc1d1	desinfekční
–	–	k?	–
(	(	kIx(	(
<g/>
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
<g/>
,	,	kIx,	,
šalvěj	šalvěj	k1gFnSc1	šalvěj
<g/>
,	,	kIx,	,
heřmánek	heřmánek	k1gInSc1	heřmánek
<g/>
,	,	kIx,	,
myrta	myrta	k1gFnSc1	myrta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
parazitům	parazit	k1gMnPc3	parazit
–	–	k?	–
(	(	kIx(	(
<g/>
česnek	česnek	k1gInSc1	česnek
<g/>
,	,	kIx,	,
pelyněk	pelyněk	k1gInSc1	pelyněk
<g/>
,	,	kIx,	,
řimbaba	řimbaba	k1gFnSc1	řimbaba
<g/>
)	)	kIx)	)
<g/>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
siličné	siličný	k2eAgFnPc1d1	siličná
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
také	také	k9	také
jako	jako	k9	jako
koření	kořenit	k5eAaImIp3nS	kořenit
(	(	kIx(	(
<g/>
badyán	badyán	k1gInSc1	badyán
<g/>
,	,	kIx,	,
skořice	skořice	k1gFnSc1	skořice
<g/>
,	,	kIx,	,
pepř	pepř	k1gInSc1	pepř
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
zázvor	zázvor	k1gInSc1	zázvor
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třísloviny	tříslovina	k1gFnSc2	tříslovina
===	===	k?	===
</s>
</p>
<p>
<s>
Třísloviny	tříslovina	k1gFnPc4	tříslovina
jsou	být	k5eAaImIp3nP	být
deriváty	derivát	k1gInPc1	derivát
kyseliny	kyselina	k1gFnSc2	kyselina
gallové	gallový	k2eAgInPc1d1	gallový
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
svíravou	svíravý	k2eAgFnSc4d1	svíravá
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
srážet	srážet	k5eAaImF	srážet
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
alkaloidy	alkaloid	k1gInPc4	alkaloid
a	a	k8xC	a
těžké	těžký	k2eAgInPc4d1	těžký
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
při	při	k7c6	při
průjmech	průjem	k1gInPc6	průjem
<g/>
,	,	kIx,	,
otravách	otrava	k1gFnPc6	otrava
alkaloidy	alkaloid	k1gInPc1	alkaloid
<g/>
,	,	kIx,	,
drobných	drobný	k2eAgNnPc6d1	drobné
poraněních	poranění	k1gNnPc6	poranění
apod.	apod.	kA	apod.
Jsou	být	k5eAaImIp3nP	být
účinnými	účinný	k2eAgFnPc7d1	účinná
látkami	látka	k1gFnPc7	látka
dubu	dub	k1gInSc2	dub
<g/>
,	,	kIx,	,
borůvky	borůvka	k1gFnSc2	borůvka
<g/>
,	,	kIx,	,
ostružiníku	ostružiník	k1gInSc2	ostružiník
<g/>
,	,	kIx,	,
řepíku	řepík	k1gInSc2	řepík
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
===	===	k?	===
Hořčiny	hořčina	k1gFnSc2	hořčina
===	===	k?	===
</s>
</p>
<p>
<s>
Hořčiny	hořčina	k1gFnPc1	hořčina
(	(	kIx(	(
<g/>
amara	amara	k6eAd1	amara
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
chemicky	chemicky	k6eAd1	chemicky
nejednotné	jednotný	k2eNgFnPc4d1	nejednotná
látky	látka	k1gFnPc4	látka
hořké	hořký	k2eAgFnSc2d1	hořká
chuti	chuť	k1gFnSc2	chuť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dráždí	dráždit	k5eAaImIp3nP	dráždit
chuťové	chuťový	k2eAgFnPc1d1	chuťová
buňky	buňka	k1gFnPc1	buňka
a	a	k8xC	a
povzbuzují	povzbuzovat	k5eAaImIp3nP	povzbuzovat
činnost	činnost	k1gFnSc4	činnost
trávicího	trávicí	k2eAgNnSc2d1	trávicí
ústrojí	ústrojí	k1gNnSc2	ústrojí
a	a	k8xC	a
žláz	žláza	k1gFnPc2	žláza
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
vyměšováním	vyměšování	k1gNnSc7	vyměšování
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
u	u	k7c2	u
hořce	hořec	k1gInSc2	hořec
<g/>
,	,	kIx,	,
pelyňku	pelyněk	k1gInSc2	pelyněk
<g/>
,	,	kIx,	,
zeměžluči	zeměžluč	k1gFnSc2	zeměžluč
či	či	k8xC	či
puškvorce	puškvorec	k1gInSc2	puškvorec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fytoncidy	Fytoncid	k1gInPc4	Fytoncid
===	===	k?	===
</s>
</p>
<p>
<s>
Fytoncidy	Fytoncid	k1gInPc1	Fytoncid
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyšší	vysoký	k2eAgFnPc1d2	vyšší
rostliny	rostlina	k1gFnPc1	rostlina
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jako	jako	k9	jako
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
cizím	cizí	k2eAgMnPc3d1	cizí
organismům	organismus	k1gInPc3	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
antibiotický	antibiotický	k2eAgInSc4d1	antibiotický
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
česneku	česnek	k1gInSc6	česnek
<g/>
,	,	kIx,	,
cibuli	cibule	k1gFnSc6	cibule
<g/>
,	,	kIx,	,
tymiánu	tymián	k1gInSc6	tymián
<g/>
,	,	kIx,	,
křenu	křen	k1gInSc6	křen
<g/>
,	,	kIx,	,
bezu	bez	k1gInSc6	bez
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
===	===	k?	===
Vitamíny	vitamín	k1gInPc4	vitamín
===	===	k?	===
</s>
</p>
<p>
<s>
Vitamíny	vitamín	k1gInPc1	vitamín
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc4	látka
nezbytné	zbytný	k2eNgFnPc4d1	zbytný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
sám	sám	k3xTgMnSc1	sám
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
nedostatek	nedostatek	k1gInSc1	nedostatek
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
avitaminózami	avitaminóza	k1gFnPc7	avitaminóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
léčivých	léčivý	k2eAgFnPc6d1	léčivá
rostlinách	rostlina	k1gFnPc6	rostlina
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
tyto	tento	k3xDgInPc4	tento
vitamíny	vitamín	k1gInPc4	vitamín
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
A	a	k9	a
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
provitamínu	provitamín	k1gInSc2	provitamín
A	A	kA	A
<g/>
)	)	kIx)	)
–	–	k?	–
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
rajče	rajče	k1gNnSc1	rajče
<g/>
,	,	kIx,	,
brusinka	brusinka	k1gFnSc1	brusinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
vitamínů	vitamín	k1gInPc2	vitamín
B	B	kA	B
–	–	k?	–
obilniny	obilnina	k1gFnSc2	obilnina
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
–	–	k?	–
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
a	a	k8xC	a
zelenině	zelenina	k1gFnSc6	zelenina
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
obsah	obsah	k1gInSc4	obsah
mají	mít	k5eAaImIp3nP	mít
šípky	šípka	k1gFnPc1	šípka
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
<g/>
brokolice	brokolice	k1gFnPc1	brokolice
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
E	E	kA	E
–	–	k?	–
sója	sója	k1gFnSc1	sója
<g/>
,	,	kIx,	,
salát	salát	k1gInSc1	salát
<g/>
,	,	kIx,	,
podzemnice	podzemnice	k1gFnSc1	podzemnice
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
–	–	k?	–
listová	listový	k2eAgFnSc1d1	listová
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
oleje	olej	k1gInPc4	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sacharidy	sacharid	k1gInPc4	sacharid
===	===	k?	===
</s>
</p>
<p>
<s>
Sacharidy	sacharid	k1gInPc1	sacharid
jsou	být	k5eAaImIp3nP	být
produkty	produkt	k1gInPc1	produkt
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
v	v	k7c6	v
zelených	zelený	k2eAgFnPc6d1	zelená
částech	část	k1gFnPc6	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
cukry	cukr	k1gInPc1	cukr
jako	jako	k8xS	jako
glukóza	glukóza	k1gFnSc1	glukóza
a	a	k8xC	a
fruktóza	fruktóza	k1gFnSc1	fruktóza
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zdroje	zdroj	k1gInPc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
složitější	složitý	k2eAgInPc1d2	složitější
polysacharidy	polysacharid	k1gInPc1	polysacharid
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
stavební	stavební	k2eAgFnPc1d1	stavební
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
celulóza	celulóza	k1gFnSc1	celulóza
<g/>
)	)	kIx)	)
či	či	k8xC	či
látky	látka	k1gFnSc2	látka
zásobní	zásobní	k2eAgInPc1d1	zásobní
(	(	kIx(	(
<g/>
škroby	škrob	k1gInPc1	škrob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oleje	olej	k1gInPc1	olej
a	a	k8xC	a
tuky	tuk	k1gInPc1	tuk
===	===	k?	===
</s>
</p>
<p>
<s>
Oleje	olej	k1gInPc1	olej
a	a	k8xC	a
tuky	tuk	k1gInPc1	tuk
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
glycerolu	glycerol	k1gInSc2	glycerol
a	a	k8xC	a
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Dělíme	dělit	k5eAaImIp1nP	dělit
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nevysýchavé	vysýchavý	k2eNgFnPc4d1	vysýchavý
(	(	kIx(	(
<g/>
olivový	olivový	k2eAgInSc4d1	olivový
<g/>
,	,	kIx,	,
mandlový	mandlový	k2eAgInSc4d1	mandlový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polovysýchavé	polovysýchavý	k2eAgFnPc4d1	polovysýchavý
(	(	kIx(	(
<g/>
řepkový	řepkový	k2eAgInSc4d1	řepkový
<g/>
,	,	kIx,	,
podzemnicový	podzemnicový	k2eAgInSc4d1	podzemnicový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysýchavé	vysýchavý	k2eAgFnPc4d1	vysýchavá
(	(	kIx(	(
<g/>
lněný	lněný	k2eAgInSc4d1	lněný
<g/>
,	,	kIx,	,
kakaový	kakaový	k2eAgInSc4d1	kakaový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgInSc4d1	speciální
(	(	kIx(	(
<g/>
ricinový	ricinový	k2eAgInSc4d1	ricinový
<g/>
,	,	kIx,	,
krotonový	krotonový	k2eAgInSc4d1	krotonový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírané	sbíraný	k2eAgFnSc6d1	sbíraná
části	část	k1gFnSc6	část
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nať	nať	k1gFnSc4	nať
===	===	k?	===
</s>
</p>
<p>
<s>
Nať	nať	k1gFnSc1	nať
(	(	kIx(	(
<g/>
herba	herba	k1gFnSc1	herba
<g/>
)	)	kIx)	)
–	–	k?	–
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
část	část	k1gFnSc1	část
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
kořen	kořen	k1gInSc1	kořen
s	s	k7c7	s
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Nať	nať	k1gFnSc4	nať
sbíráme	sbírat	k5eAaImIp1nP	sbírat
např.	např.	kA	např.
u	u	k7c2	u
pelyňků	pelyněk	k1gInPc2	pelyněk
<g/>
,	,	kIx,	,
mateřídoušek	mateřídouška	k1gFnPc2	mateřídouška
či	či	k8xC	či
routy	routa	k1gFnSc2	routa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
List	list	k1gInSc1	list
===	===	k?	===
</s>
</p>
<p>
<s>
List	list	k1gInSc1	list
(	(	kIx(	(
<g/>
folium	folium	k1gNnSc1	folium
<g/>
)	)	kIx)	)
–	–	k?	–
postranní	postranní	k2eAgInSc4d1	postranní
orgán	orgán	k1gInSc4	orgán
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Listovou	listový	k2eAgFnSc4d1	listová
drogu	droga	k1gFnSc4	droga
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
např.	např.	kA	např.
<g/>
:	:	kIx,	:
ostružiník	ostružiník	k1gInSc1	ostružiník
<g/>
,	,	kIx,	,
medvědice	medvědice	k1gFnSc1	medvědice
<g/>
,	,	kIx,	,
brusinka	brusinka	k1gFnSc1	brusinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Květ	květ	k1gInSc4	květ
===	===	k?	===
</s>
</p>
<p>
<s>
Květ	květ	k1gInSc1	květ
(	(	kIx(	(
<g/>
flos	flos	k1gInSc1	flos
<g/>
)	)	kIx)	)
–	–	k?	–
rozmnožovací	rozmnožovací	k2eAgInSc4d1	rozmnožovací
orgán	orgán	k1gInSc4	orgán
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přeměnou	přeměna	k1gFnSc7	přeměna
listů	list	k1gInPc2	list
a	a	k8xC	a
koncové	koncový	k2eAgFnSc6d1	koncová
části	část	k1gFnSc6	část
stonku	stonek	k1gInSc2	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
hloh	hloh	k1gInSc1	hloh
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
třezalka	třezalka	k1gFnSc1	třezalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plod	plod	k1gInSc4	plod
===	===	k?	===
</s>
</p>
<p>
<s>
Plod	plod	k1gInSc1	plod
(	(	kIx(	(
<g/>
fructus	fructus	k1gInSc1	fructus
<g/>
)	)	kIx)	)
–	–	k?	–
konečná	konečný	k2eAgFnSc1d1	konečná
část	část	k1gFnSc1	část
vývoje	vývoj	k1gInSc2	vývoj
rozmnožovacích	rozmnožovací	k2eAgInPc2d1	rozmnožovací
orgánů	orgán	k1gInPc2	orgán
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Sbíráme	sbírat	k5eAaImIp1nP	sbírat
např.	např.	kA	např.
u	u	k7c2	u
černého	černý	k2eAgInSc2d1	černý
rybízu	rybíz	k1gInSc2	rybíz
<g/>
,	,	kIx,	,
bezu	bez	k1gInSc2	bez
černého	černé	k1gNnSc2	černé
<g/>
,	,	kIx,	,
moruše	moruše	k1gFnSc2	moruše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Semeno	semeno	k1gNnSc1	semeno
===	===	k?	===
</s>
</p>
<p>
<s>
Semeno	semeno	k1gNnSc1	semeno
(	(	kIx(	(
<g/>
semen	semeno	k1gNnPc2	semeno
<g/>
)	)	kIx)	)
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
přeměnou	přeměna	k1gFnSc7	přeměna
vajíček	vajíčko	k1gNnPc2	vajíčko
v	v	k7c6	v
semeníku	semeník	k1gInSc6	semeník
<g/>
.	.	kIx.	.
</s>
<s>
Semenné	semenný	k2eAgFnPc1d1	semenná
drogy	droga	k1gFnPc1	droga
jsou	být	k5eAaImIp3nP	být
mák	mák	k1gInSc4	mák
setý	setý	k2eAgInSc4d1	setý
<g/>
,	,	kIx,	,
kmín	kmín	k1gInSc1	kmín
<g/>
,	,	kIx,	,
kaštan	kaštan	k1gInSc1	kaštan
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kůra	kůra	k1gFnSc1	kůra
===	===	k?	===
</s>
</p>
<p>
<s>
Kůra	kůra	k1gFnSc1	kůra
(	(	kIx(	(
<g/>
cortex	cortex	k1gInSc1	cortex
<g/>
)	)	kIx)	)
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
od	od	k7c2	od
tenké	tenký	k2eAgFnSc2d1	tenká
několikamilimetrové	několikamilimetrový	k2eAgFnSc2d1	několikamilimetrová
<g/>
,	,	kIx,	,
až	až	k8xS	až
po	po	k7c4	po
rozbrázděnou	rozbrázděný	k2eAgFnSc4d1	rozbrázděná
mnohavrstevnou	mnohavrstevný	k2eAgFnSc4d1	mnohavrstevná
kůru	kůra	k1gFnSc4	kůra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
rozbrázděná	rozbrázděný	k2eAgFnSc1d1	rozbrázděná
kůra	kůra	k1gFnSc1	kůra
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
borka	borka	k1gFnSc1	borka
vrby	vrba	k1gFnSc2	vrba
<g/>
,	,	kIx,	,
dubu	dub	k1gInSc2	dub
<g/>
,	,	kIx,	,
trnky	trnka	k1gFnPc1	trnka
<g/>
,	,	kIx,	,
břízy	bříza	k1gFnPc1	bříza
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Dřevo	dřevo	k1gNnSc1	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
(	(	kIx(	(
<g/>
lignum	lignum	k1gNnSc1	lignum
<g/>
)	)	kIx)	)
–	–	k?	–
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
větví	větev	k1gFnPc2	větev
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
drogy	droga	k1gFnPc1	droga
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
např.	např.	kA	např.
jalovec	jalovec	k1gInSc4	jalovec
či	či	k8xC	či
santal	santat	k5eAaImAgMnS	santat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kořen	kořen	k2eAgMnSc1d1	kořen
===	===	k?	===
</s>
</p>
<p>
<s>
Kořen	kořen	k1gInSc1	kořen
(	(	kIx(	(
<g/>
radix	radix	k1gInSc1	radix
<g/>
)	)	kIx)	)
–	–	k?	–
podzemní	podzemní	k2eAgFnSc1d1	podzemní
část	část	k1gFnSc1	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
rostlinu	rostlina	k1gFnSc4	rostlina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
živiny	živina	k1gFnPc4	živina
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kořenových	kořenový	k2eAgFnPc2d1	kořenová
drog	droga	k1gFnPc2	droga
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
např.	např.	kA	např.
kostival	kostival	k1gInSc4	kostival
<g/>
,	,	kIx,	,
křen	křen	k1gInSc1	křen
<g/>
,	,	kIx,	,
proskurník	proskurník	k1gInSc1	proskurník
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oddenek	oddenek	k1gInSc4	oddenek
===	===	k?	===
</s>
</p>
<p>
<s>
Oddenek	oddenek	k1gInSc1	oddenek
(	(	kIx(	(
<g/>
rhizoma	rhizoma	k1gFnSc1	rhizoma
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
podzemní	podzemní	k2eAgFnSc4d1	podzemní
část	část	k1gFnSc4	část
stonku	stonek	k1gInSc2	stonek
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgNnSc2	jenž
rostlina	rostlina	k1gFnSc1	rostlina
překonává	překonávat	k5eAaImIp3nS	překonávat
nepříznivé	příznivý	k2eNgNnSc4d1	nepříznivé
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Oddenky	oddenek	k1gInPc4	oddenek
používáme	používat	k5eAaImIp1nP	používat
z	z	k7c2	z
kosatce	kosatec	k1gInSc2	kosatec
<g/>
,	,	kIx,	,
pýru	pýr	k1gInSc2	pýr
<g/>
,	,	kIx,	,
puškvorce	puškvorec	k1gInSc2	puškvorec
<g/>
,	,	kIx,	,
zázvoru	zázvor	k1gInSc2	zázvor
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cibule	cibule	k1gFnSc2	cibule
===	===	k?	===
</s>
</p>
<p>
<s>
Cibule	cibule	k1gFnSc1	cibule
(	(	kIx(	(
<g/>
bulbus	bulbus	k1gInSc1	bulbus
<g/>
)	)	kIx)	)
–	–	k?	–
zkrácený	zkrácený	k2eAgInSc4d1	zkrácený
stonek	stonek	k1gInSc4	stonek
se	se	k3xPyFc4	se
zdužnatělou	zdužnatělý	k2eAgFnSc4d1	zdužnatělá
šupinami	šupina	k1gFnPc7	šupina
nebo	nebo	k8xC	nebo
spodinami	spodina	k1gFnPc7	spodina
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Cibule	cibule	k1gFnPc1	cibule
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
česnek	česnek	k1gInSc4	česnek
medvědí	medvědí	k2eAgInSc4d1	medvědí
<g/>
,	,	kIx,	,
česnek	česnek	k1gInSc1	česnek
kuchyňský	kuchyňský	k2eAgInSc1d1	kuchyňský
či	či	k8xC	či
cibule	cibule	k1gFnSc1	cibule
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlíza	hlíza	k1gFnSc1	hlíza
===	===	k?	===
</s>
</p>
<p>
<s>
Hlíza	hlíza	k1gFnSc1	hlíza
(	(	kIx(	(
<g/>
tuber	tuber	k1gInSc1	tuber
<g/>
)	)	kIx)	)
–	–	k?	–
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
orgán	orgán	k1gInSc4	orgán
různého	různý	k2eAgInSc2d1	různý
původu	původ	k1gInSc2	původ
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
živiny	živina	k1gFnPc1	živina
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
hlízových	hlízový	k2eAgFnPc2d1	Hlízová
drog	droga	k1gFnPc2	droga
jsou	být	k5eAaImIp3nP	být
slunečnice	slunečnice	k1gFnSc1	slunečnice
topinambur	topinambur	k1gInSc4	topinambur
a	a	k8xC	a
brambory	brambor	k1gInPc4	brambor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plodnice	plodnice	k1gFnSc2	plodnice
===	===	k?	===
</s>
</p>
<p>
<s>
Plodnice	plodnice	k1gFnSc1	plodnice
(	(	kIx(	(
<g/>
carposoma	carposoma	k1gFnSc1	carposoma
<g/>
)	)	kIx)	)
–	–	k?	–
část	část	k1gFnSc4	část
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
spor	spora	k1gFnPc2	spora
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
sbíraných	sbíraný	k2eAgFnPc2d1	sbíraná
plodnic	plodnice	k1gFnPc2	plodnice
jsou	být	k5eAaImIp3nP	být
rezavec	rezavec	k1gInSc1	rezavec
šikmý	šikmý	k2eAgInSc1d1	šikmý
<g/>
,	,	kIx,	,
penízovka	penízovka	k1gFnSc1	penízovka
sametonohá	sametonohý	k2eAgFnSc1d1	sametonohá
a	a	k8xC	a
pečárka	pečárka	k1gFnSc1	pečárka
polní	polní	k2eAgFnSc1d1	polní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pupen	pupen	k1gInSc4	pupen
===	===	k?	===
</s>
</p>
<p>
<s>
Pupen	pupen	k1gInSc1	pupen
(	(	kIx(	(
<g/>
gemma	gemma	k1gFnSc1	gemma
<g/>
)	)	kIx)	)
–	–	k?	–
nejmladší	mladý	k2eAgNnSc4d3	nejmladší
stadium	stadium	k1gNnSc4	stadium
stonku	stonek	k1gInSc2	stonek
nebo	nebo	k8xC	nebo
květu	květ	k1gInSc2	květ
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pupeny	pupen	k1gInPc4	pupen
sbíráme	sbírat	k5eAaImIp1nP	sbírat
u	u	k7c2	u
buku	buk	k1gInSc2	buk
lesního	lesní	k2eAgInSc2d1	lesní
<g/>
,	,	kIx,	,
jedle	jedle	k6eAd1	jedle
bělokoré	bělokorý	k2eAgInPc4d1	bělokorý
<g/>
,	,	kIx,	,
topolu	topol	k1gInSc2	topol
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Léčením	léčení	k1gNnSc7	léčení
pomocí	pomocí	k7c2	pomocí
pupenů	pupen	k1gInPc2	pupen
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
gemmoterapie	gemmoterapie	k1gFnSc1	gemmoterapie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sběr	sběr	k1gInSc4	sběr
<g/>
,	,	kIx,	,
sušení	sušení	k1gNnSc4	sušení
a	a	k8xC	a
skladování	skladování	k1gNnSc4	skladování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sběr	sběr	k1gInSc4	sběr
===	===	k?	===
</s>
</p>
<p>
<s>
Sběr	sběr	k1gInSc1	sběr
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
léčivé	léčivý	k2eAgFnSc2d1	léčivá
drogy	droga	k1gFnSc2	droga
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
nutno	nutno	k6eAd1	nutno
dodržovat	dodržovat	k5eAaImF	dodržovat
určité	určitý	k2eAgFnPc4d1	určitá
zásady	zásada	k1gFnPc4	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgFnPc4d1	nutná
sbíranou	sbíraný	k2eAgFnSc4d1	sbíraná
rostlinu	rostlina	k1gFnSc4	rostlina
bezpečně	bezpečně	k6eAd1	bezpečně
znát	znát	k5eAaImF	znát
<g/>
.	.	kIx.	.
</s>
<s>
Sbírají	sbírat	k5eAaImIp3nP	sbírat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
zdravé	zdravý	k2eAgInPc1d1	zdravý
<g/>
,	,	kIx,	,
chorobami	choroba	k1gFnPc7	choroba
a	a	k8xC	a
hmyzem	hmyz	k1gInSc7	hmyz
nenapadené	napadený	k2eNgFnSc2d1	nenapadená
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nesmíchalo	smíchat	k5eNaPmAgNnS	smíchat
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Nasbírané	nasbíraný	k2eAgFnPc1d1	nasbíraná
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
nemačkají	mačkat	k5eNaImIp3nP	mačkat
se	se	k3xPyFc4	se
a	a	k8xC	a
suší	sušit	k5eAaImIp3nS	sušit
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
po	po	k7c6	po
sběru	sběr	k1gInSc6	sběr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
si	se	k3xPyFc3	se
počínat	počínat	k5eAaImF	počínat
ohleduplně	ohleduplně	k6eAd1	ohleduplně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rostlina	rostlina	k1gFnSc1	rostlina
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
sběru	sběr	k1gInSc2	sběr
nebyla	být	k5eNaImAgFnS	být
vyhubena	vyhubit	k5eAaPmNgFnS	vyhubit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
sbírá	sbírat	k5eAaImIp3nS	sbírat
pouze	pouze	k6eAd1	pouze
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
hojná	hojný	k2eAgFnSc1d1	hojná
a	a	k8xC	a
část	část	k1gFnSc1	část
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
nechává	nechávat	k5eAaImIp3nS	nechávat
nedotčena	dotčen	k2eNgFnSc1d1	nedotčena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
není	být	k5eNaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
nadzemních	nadzemní	k2eAgFnPc2d1	nadzemní
částí	část	k1gFnPc2	část
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
kvetení	kvetení	k1gNnSc2	kvetení
do	do	k7c2	do
tvorby	tvorba	k1gFnSc2	tvorba
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnPc1d1	podzemní
části	část	k1gFnPc1	část
rostliny	rostlina	k1gFnSc2	rostlina
mají	mít	k5eAaImIp3nP	mít
naopak	naopak	k6eAd1	naopak
největší	veliký	k2eAgInSc4d3	veliký
obsah	obsah	k1gInSc4	obsah
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vegetačního	vegetační	k2eAgInSc2d1	vegetační
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sušení	sušení	k1gNnSc2	sušení
===	===	k?	===
</s>
</p>
<p>
<s>
Sušení	sušení	k1gNnSc1	sušení
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
věnovat	věnovat	k5eAaPmF	věnovat
velká	velký	k2eAgFnSc1d1	velká
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
právě	právě	k9	právě
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
zpracování	zpracování	k1gNnSc4	zpracování
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
získaná	získaný	k2eAgFnSc1d1	získaná
droga	droga	k1gFnSc1	droga
bude	být	k5eAaImBp3nS	být
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
45	[number]	k4	45
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
sušení	sušení	k1gNnSc1	sušení
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
provádět	provádět	k5eAaImF	provádět
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
v	v	k7c6	v
tenké	tenký	k2eAgFnSc6d1	tenká
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
při	při	k7c6	při
sušení	sušení	k1gNnSc6	sušení
droga	droga	k1gFnSc1	droga
zhnědne	zhnědnout	k5eAaPmIp3nS	zhnědnout
či	či	k8xC	či
zčerná	zčernat	k5eAaPmIp3nS	zčernat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hrubé	hrubý	k2eAgNnSc4d1	hrubé
znehodnocení	znehodnocení	k1gNnSc4	znehodnocení
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
vyřadit	vyřadit	k5eAaPmF	vyřadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zamrazení	zamrazení	k1gNnSc2	zamrazení
===	===	k?	===
</s>
</p>
<p>
<s>
Bylinky	bylinka	k1gFnPc4	bylinka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
skladovat	skladovat	k5eAaImF	skladovat
i	i	k9	i
zamrazené	zamrazený	k2eAgInPc1d1	zamrazený
<g/>
.	.	kIx.	.
</s>
<s>
Alespoň	alespoň	k9	alespoň
polovina	polovina	k1gFnSc1	polovina
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
zachová	zachovat	k5eAaPmIp3nS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skladování	skladování	k1gNnSc2	skladování
===	===	k?	===
</s>
</p>
<p>
<s>
Rostlinné	rostlinný	k2eAgFnPc1d1	rostlinná
drogy	droga	k1gFnPc1	droga
se	se	k3xPyFc4	se
skladují	skladovat	k5eAaImIp3nP	skladovat
v	v	k7c6	v
suchu	sucho	k1gNnSc6	sucho
<g/>
,	,	kIx,	,
tmě	tma	k1gFnSc6	tma
a	a	k8xC	a
chladu	chlad	k1gInSc6	chlad
v	v	k7c6	v
hermeticky	hermeticky	k6eAd1	hermeticky
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
skleněných	skleněný	k2eAgFnPc6d1	skleněná
nádobách	nádoba	k1gFnPc6	nádoba
či	či	k8xC	či
plechových	plechový	k2eAgFnPc6d1	plechová
krabicích	krabice	k1gFnPc6	krabice
<g/>
.	.	kIx.	.
</s>
<s>
Skladování	skladování	k1gNnSc1	skladování
v	v	k7c6	v
plátěných	plátěný	k2eAgInPc6d1	plátěný
pytlících	pytlík	k1gInPc6	pytlík
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
droga	droga	k1gFnSc1	droga
není	být	k5eNaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
před	před	k7c7	před
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
a	a	k8xC	a
ztrátě	ztráta	k1gFnSc3	ztráta
aromatických	aromatický	k2eAgFnPc2d1	aromatická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
dobře	dobře	k6eAd1	dobře
uskladněných	uskladněný	k2eAgFnPc2d1	uskladněná
drog	droga	k1gFnPc2	droga
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
<g/>
"	"	kIx"	"
drogy	droga	k1gFnPc4	droga
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
dřeva	dřevo	k1gNnPc1	dřevo
<g/>
,	,	kIx,	,
kůry	kůra	k1gFnPc1	kůra
či	či	k8xC	či
kořeny	kořen	k1gInPc1	kořen
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgFnSc4d2	delší
životnost	životnost	k1gFnSc4	životnost
než	než	k8xS	než
drogy	droga	k1gFnPc4	droga
"	"	kIx"	"
<g/>
měkké	měkký	k2eAgInPc4d1	měkký
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
drogy	droga	k1gFnPc4	droga
květové	květový	k2eAgFnPc4d1	květová
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vydrží	vydržet	k5eAaPmIp3nP	vydržet
sotva	sotva	k6eAd1	sotva
do	do	k7c2	do
příští	příští	k2eAgFnSc2d1	příští
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DLOUHÝ	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
Herbář	herbář	k1gInSc1	herbář
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jejich	jejich	k3xOp3gInSc1	jejich
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc1	pěstování
a	a	k8xC	a
upotřebení	upotřebení	k1gNnSc1	upotřebení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
182	[number]	k4	182
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GRUBCOV	GRUBCOV	kA	GRUBCOV
<g/>
,	,	kIx,	,
V.	V.	kA	V.
G.	G.	kA	G.
<g/>
,	,	kIx,	,
BENEŠ	Beneš	k1gMnSc1	Beneš
K.	K.	kA	K.
Zelená	Zelená	k1gFnSc1	Zelená
lékárna	lékárna	k1gFnSc1	lékárna
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANČA	Janča	k1gMnSc1	Janča
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
ZENTRICH	ZENTRICH	kA	ZENTRICH
Josef	Josef	k1gMnSc1	Josef
A.	A.	kA	A.
Herbář	herbář	k1gInSc1	herbář
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Eminent	Eminent	k1gMnSc1	Eminent
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85876	[number]	k4	85876
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PŘÍHODA	Příhoda	k1gMnSc1	Příhoda
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
THURZOVÁ	THURZOVÁ	kA	THURZOVÁ
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
atlas	atlas	k1gInSc1	atlas
liečivých	liečivý	k2eAgFnPc2d1	liečivý
rastlín	rastlína	k1gFnPc2	rastlína
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
Obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
Doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
STARÝ	Starý	k1gMnSc1	Starý
Fr.	Fr.	k1gMnSc1	Fr.
(	(	kIx(	(
<g/>
RNDr.	RNDr.	kA	RNDr.
<g/>
PhMr	PhMr	k1gInSc1	PhMr
<g/>
.	.	kIx.	.
</s>
<s>
CSc	CSc	kA	CSc
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
SPN	SPN	kA	SPN
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
320	[number]	k4	320
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MAYER	Mayer	k1gMnSc1	Mayer
Miroslav	Miroslav	k1gMnSc1	Miroslav
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Babiččiny	babiččin	k2eAgFnPc1d1	babiččina
bylinky	bylinka	k1gFnPc1	bylinka
<g/>
.	.	kIx.	.
</s>
<s>
Agentura	agentura	k1gFnSc1	agentura
V.	V.	kA	V.
P.	P.	kA	P.
K.	K.	kA	K.
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
34	[number]	k4	34
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TREBEN	TREBEN	kA	TREBEN
Maria	Maria	k1gFnSc1	Maria
<g/>
:	:	kIx,	:
Zdraví	zdraví	k1gNnSc1	zdraví
z	z	k7c2	z
boží	boží	k2eAgFnSc2d1	boží
lékárny	lékárna	k1gFnSc2	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Nakl	Nakl	k1gMnSc1	Nakl
<g/>
.	.	kIx.	.
</s>
<s>
Dona	Don	k1gMnSc4	Don
České	český	k2eAgFnSc2d1	Česká
Budějovice	Budějovice	k1gInPc1	Budějovice
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
98	[number]	k4	98
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SENFT	senft	k1gInSc1	senft
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
.	.	kIx.	.
</s>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
:	:	kIx,	:
návod	návod	k1gInSc1	návod
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
a	a	k8xC	a
sbírání	sbírání	k1gNnSc3	sbírání
našich	náš	k3xOp1gFnPc2	náš
domácích	domácí	k2eAgFnPc2d1	domácí
a	a	k8xC	a
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
léčivých	léčivý	k2eAgFnPc2d1	léčivá
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Emanuel	Emanuel	k1gMnSc1	Emanuel
Senft	senfta	k1gFnPc2	senfta
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STACH	Stach	k1gMnSc1	Stach
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
a	a	k8xC	a
léčivé	léčivý	k2eAgFnPc1d1	léčivá
rostliny	rostlina	k1gFnPc1	rostlina
tropické	tropický	k2eAgFnPc1d1	tropická
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
</s>
</p>
<p>
<s>
Droga	droga	k1gFnSc1	droga
(	(	kIx(	(
<g/>
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fytoterapie	fytoterapie	k1gFnSc1	fytoterapie
</s>
</p>
<p>
<s>
Homeopatie	homeopatie	k1gFnSc1	homeopatie
</s>
</p>
<p>
<s>
Aromaterapie	aromaterapie	k1gFnSc1	aromaterapie
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
medicína	medicína	k1gFnSc1	medicína
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Florius	Florius	k1gMnSc1	Florius
</s>
</p>
