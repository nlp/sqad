<s>
Léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
(	(	kIx(	(
<g/>
léčivka	léčivka	k1gFnSc1	léčivka
či	či	k8xC	či
léčivá	léčivý	k2eAgFnSc1d1	léčivá
bylina	bylina	k1gFnSc1	bylina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
účinné	účinný	k2eAgFnPc4d1	účinná
látky	látka	k1gFnPc4	látka
příznivě	příznivě	k6eAd1	příznivě
ovlivňující	ovlivňující	k2eAgInSc4d1	ovlivňující
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
stav	stav	k1gInSc4	stav
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
