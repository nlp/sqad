<s>
James	James	k1gMnSc1	James
Franck	Franck	k1gMnSc1	Franck
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1882	[number]	k4	1882
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Göttingen	Göttingen	k1gInSc1	Göttingen
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jeho	jeho	k3xOp3gFnSc4	jeho
kolega	kolega	k1gMnSc1	kolega
Gustav	Gustav	k1gMnSc1	Gustav
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hertz	hertz	k1gInSc4	hertz
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Franck	Franck	k1gMnSc1	Franck
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
bohaté	bohatý	k2eAgFnSc6d1	bohatá
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
sice	sice	k8xC	sice
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
studovat	studovat	k5eAaImF	studovat
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svá	svůj	k3xOyFgNnPc4	svůj
univerzitní	univerzitní	k2eAgNnPc4d1	univerzitní
studia	studio	k1gNnPc4	studio
navázal	navázat	k5eAaPmAgInS	navázat
fyzikou	fyzika	k1gFnSc7	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
i	i	k9	i
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc1	doktorát
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
profesuru	profesura	k1gFnSc4	profesura
<g/>
.	.	kIx.	.
</s>
<s>
Profesorem	profesor	k1gMnSc7	profesor
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
<g/>
,	,	kIx,	,
než	než	k8xS	než
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
USA	USA	kA	USA
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opět	opět	k6eAd1	opět
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
Franck	Franck	k1gMnSc1	Franck
sledoval	sledovat	k5eAaImAgMnS	sledovat
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hertzem	hertz	k1gInSc7	hertz
<g/>
,	,	kIx,	,
závislost	závislost	k1gFnSc4	závislost
stupně	stupeň	k1gInSc2	stupeň
ionizace	ionizace	k1gFnSc2	ionizace
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
parách	para	k1gFnPc6	para
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomy	atom	k1gInPc1	atom
rtuti	rtuť	k1gFnSc2	rtuť
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
energii	energie	k1gFnSc4	energie
elektronů	elektron	k1gInPc2	elektron
po	po	k7c6	po
kvantech	kvantum	k1gNnPc6	kvantum
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
studoval	studovat	k5eAaImAgMnS	studovat
kvantový	kvantový	k2eAgInSc4d1	kvantový
charakter	charakter	k1gInSc4	charakter
fotochemických	fotochemický	k2eAgFnPc2d1	fotochemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
k	k	k7c3	k
výkladu	výklad	k1gInSc3	výklad
mezimolekulárních	mezimolekulární	k2eAgFnPc2d1	mezimolekulární
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
významná	významný	k2eAgFnSc1d1	významná
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
i	i	k9	i
experimentálně	experimentálně	k6eAd1	experimentálně
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xS	jako
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
získal	získat	k5eAaPmAgMnS	získat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Hertzem	hertz	k1gInSc7	hertz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
protestujících	protestující	k2eAgMnPc2d1	protestující
proti	proti	k7c3	proti
použití	použití	k1gNnSc3	použití
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
1925	[number]	k4	1925
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
1951	[number]	k4	1951
Medaile	medaile	k1gFnSc2	medaile
Maxe	Max	k1gMnSc2	Max
Plancka	Plancko	k1gNnSc2	Plancko
1953	[number]	k4	1953
Čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
Göttingenu	Göttingen	k1gInSc2	Göttingen
1955	[number]	k4	1955
Rumfordova	Rumfordův	k2eAgFnSc1d1	Rumfordova
Medaile	medaile	k1gFnSc1	medaile
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
1964	[number]	k4	1964
Zvolen	Zvolen	k1gInSc4	Zvolen
zahraničním	zahraniční	k2eAgInSc7d1	zahraniční
členem	člen	k1gInSc7	člen
Royal	Royal	k1gInSc1	Royal
Society	societa	k1gFnSc2	societa
of	of	k?	of
London	London	k1gMnSc1	London
</s>
