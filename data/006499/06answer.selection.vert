<s>
Evoluci	evoluce	k1gFnSc4	evoluce
Spencer	Spencra	k1gFnPc2	Spencra
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
First	First	k1gMnSc1	First
Principles	Principles	k1gMnSc1	Principles
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
změnu	změna	k1gFnSc4	změna
ze	z	k7c2	z
stavu	stav	k1gInSc2	stav
relativně	relativně	k6eAd1	relativně
nevymezeného	vymezený	k2eNgInSc2d1	nevymezený
<g/>
,	,	kIx,	,
nekoherentního	koherentní	k2eNgInSc2d1	nekoherentní
<g/>
,	,	kIx,	,
homogenního	homogenní	k2eAgInSc2d1	homogenní
ke	k	k7c3	k
stavu	stav	k1gInSc2	stav
relativně	relativně	k6eAd1	relativně
vymezenému	vymezený	k2eAgMnSc3d1	vymezený
<g/>
,	,	kIx,	,
koherentnímu	koherentní	k2eAgMnSc3d1	koherentní
a	a	k8xC	a
heterogennímu	heterogenní	k2eAgMnSc3d1	heterogenní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Keller	Keller	k1gMnSc1	Keller
2005	[number]	k4	2005
:	:	kIx,	:
146	[number]	k4	146
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
regrese	regrese	k1gFnSc1	regrese
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
komplexních	komplexní	k2eAgFnPc2d1	komplexní
a	a	k8xC	a
diferencovaných	diferencovaný	k2eAgFnPc2d1	diferencovaná
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
