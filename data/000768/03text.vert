<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
sešitý	sešitý	k2eAgInSc4d1	sešitý
nebo	nebo	k8xC	nebo
slepený	slepený	k2eAgInSc4d1	slepený
svazek	svazek	k1gInSc4	svazek
listů	list	k1gInPc2	list
nebo	nebo	k8xC	nebo
skládaný	skládaný	k2eAgInSc1d1	skládaný
arch	arch	k1gInSc1	arch
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
kartonu	karton	k1gInSc2	karton
<g/>
,	,	kIx,	,
pergamenu	pergamen	k1gInSc2	pergamen
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgInSc1d1	popsaný
<g/>
,	,	kIx,	,
potištěný	potištěný	k2eAgInSc1d1	potištěný
nebo	nebo	k8xC	nebo
prázdný	prázdný	k2eAgInSc1d1	prázdný
s	s	k7c7	s
vazbou	vazba	k1gFnSc7	vazba
a	a	k8xC	a
opatřený	opatřený	k2eAgMnSc1d1	opatřený
přebalem	přebal	k1gInSc7	přebal
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
též	též	k9	též
literární	literární	k2eAgFnPc4d1	literární
práce	práce	k1gFnPc4	práce
nebo	nebo	k8xC	nebo
hlavní	hlavní	k2eAgInSc1d1	hlavní
oddíl	oddíl	k1gInSc1	oddíl
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
elektronická	elektronický	k2eAgFnSc1d1	elektronická
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
e-book	eook	k1gInSc4	e-book
<g/>
.	.	kIx.	.
</s>
<s>
Knihovní	knihovní	k2eAgFnSc1d1	knihovní
a	a	k8xC	a
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
definuje	definovat	k5eAaBmIp3nS	definovat
knihu	kniha	k1gFnSc4	kniha
jako	jako	k8xC	jako
monografii	monografie	k1gFnSc4	monografie
pro	pro	k7c4	pro
její	její	k3xOp3gNnPc4	její
rozlišení	rozlišení	k1gNnSc4	rozlišení
od	od	k7c2	od
periodických	periodický	k2eAgFnPc2d1	periodická
publikací	publikace	k1gFnPc2	publikace
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
časopisy	časopis	k1gInPc1	časopis
nebo	nebo	k8xC	nebo
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
se	se	k3xPyFc4	se
zaujetím	zaujetí	k1gNnSc7	zaujetí
pro	pro	k7c4	pro
knihy	kniha	k1gFnPc4	kniha
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
bibliofil	bibliofil	k1gMnSc1	bibliofil
nebo	nebo	k8xC	nebo
knihomil	knihomil	k1gMnSc1	knihomil
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
chorobnou	chorobný	k2eAgFnSc7d1	chorobná
touhou	touha	k1gFnSc7	touha
psát	psát	k5eAaImF	psát
knihy	kniha	k1gFnPc4	kniha
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xC	jako
grafoman	grafoman	k1gMnSc1	grafoman
<g/>
,	,	kIx,	,
s	s	k7c7	s
nutkáním	nutkání	k1gNnSc7	nutkání
vydat	vydat	k5eAaPmF	vydat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
knihu	kniha	k1gFnSc4	kniha
typoman	typoman	k1gMnSc1	typoman
<g/>
.	.	kIx.	.
</s>
<s>
Ústní	ústní	k2eAgNnSc1d1	ústní
podání	podání	k1gNnSc1	podání
(	(	kIx(	(
<g/>
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
doslech	doslech	k1gInSc1	doslech
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
způsob	způsob	k1gInSc1	způsob
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
písma	písmo	k1gNnSc2	písmo
v	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
civilizacích	civilizace	k1gFnPc6	civilizace
byly	být	k5eAaImAgFnP	být
informace	informace	k1gFnPc1	informace
zapisovány	zapisován	k2eAgFnPc1d1	zapisována
na	na	k7c4	na
hliněné	hliněný	k2eAgFnPc4d1	hliněná
destičky	destička	k1gFnPc4	destička
<g/>
,	,	kIx,	,
ostraka	ostrak	k1gMnSc4	ostrak
<g/>
,	,	kIx,	,
papyrus	papyrus	k1gInSc1	papyrus
(	(	kIx(	(
<g/>
Alexandrie	Alexandrie	k1gFnSc1	Alexandrie
<g/>
,	,	kIx,	,
Byblos	Byblos	k1gInSc1	Byblos
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pergamenové	pergamenový	k2eAgInPc1d1	pergamenový
svitky	svitek	k1gInPc1	svitek
(	(	kIx(	(
<g/>
Pergamon	pergamon	k1gInSc1	pergamon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgFnPc1	první
knihovny	knihovna	k1gFnPc1	knihovna
(	(	kIx(	(
<g/>
bibliotéky	bibliotéka	k1gFnPc1	bibliotéka
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
skladování	skladování	k1gNnSc4	skladování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Alexandrijská	alexandrijský	k2eAgFnSc1d1	Alexandrijská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pergamenové	pergamenový	k2eAgInPc1d1	pergamenový
svitky	svitek	k1gInPc1	svitek
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
postupně	postupně	k6eAd1	postupně
nahrazovány	nahrazovat	k5eAaImNgFnP	nahrazovat
kodexem	kodex	k1gInSc7	kodex
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
svázaná	svázaný	k2eAgFnSc1d1	svázaná
kniha	kniha	k1gFnSc1	kniha
se	s	k7c7	s
stránkami	stránka	k1gFnPc7	stránka
a	a	k8xC	a
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
forma	forma	k1gFnSc1	forma
většiny	většina	k1gFnSc2	většina
knih	kniha	k1gFnPc2	kniha
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svitky	svitek	k1gInPc4	svitek
byl	být	k5eAaImAgInS	být
nejčastěji	často	k6eAd3	často
používán	používat	k5eAaImNgInS	používat
papyrus	papyrus	k1gInSc1	papyrus
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kodexy	kodex	k1gInPc4	kodex
pergamen	pergamen	k1gInSc4	pergamen
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nahrazen	nahradit	k5eAaPmNgInS	nahradit
lacinějším	laciný	k2eAgInSc7d2	lacinější
papírem	papír	k1gInSc7	papír
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
objevem	objev	k1gInSc7	objev
knihtisku	knihtisk	k1gInSc2	knihtisk
(	(	kIx(	(
<g/>
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavedením	zavedení	k1gNnSc7	zavedení
tiskařského	tiskařský	k2eAgInSc2d1	tiskařský
lisu	lis	k1gInSc2	lis
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
knihy	kniha	k1gFnPc1	kniha
psány	psát	k5eAaImNgFnP	psát
ručně	ručně	k6eAd1	ručně
(	(	kIx(	(
<g/>
rukopis	rukopis	k1gInSc1	rukopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
činilo	činit	k5eAaImAgNnS	činit
drahými	drahá	k1gFnPc7	drahá
a	a	k8xC	a
vzácnými	vzácný	k2eAgInPc7d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
si	se	k3xPyFc3	se
knihy	kniha	k1gFnSc2	kniha
mohla	moct	k5eAaImAgFnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
jen	jen	k9	jen
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
univerzity	univerzita	k1gFnPc1	univerzita
a	a	k8xC	a
bohatí	bohatý	k2eAgMnPc1d1	bohatý
a	a	k8xC	a
často	často	k6eAd1	často
bývaly	bývat	k5eAaImAgInP	bývat
připevněny	připevněn	k2eAgInPc1d1	připevněn
řetězy	řetěz	k1gInPc1	řetěz
k	k	k7c3	k
polici	police	k1gFnSc3	police
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
proti	proti	k7c3	proti
odcizení	odcizení	k1gNnSc3	odcizení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
knihy	kniha	k1gFnPc4	kniha
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
vyráběny	vyráběn	k2eAgMnPc4d1	vyráběn
blokovým	blokový	k2eAgInSc7d1	blokový
tiskem	tisk	k1gInSc7	tisk
(	(	kIx(	(
<g/>
technika	technika	k1gFnSc1	technika
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
známá	známý	k2eAgFnSc1d1	známá
už	už	k6eAd1	už
staletí	staletí	k1gNnPc2	staletí
předtím	předtím	k6eAd1	předtím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
blokovém	blokový	k2eAgInSc6d1	blokový
tisku	tisk	k1gInSc6	tisk
byl	být	k5eAaImAgInS	být
obrysový	obrysový	k2eAgInSc1d1	obrysový
obraz	obraz	k1gInSc1	obraz
celé	celý	k2eAgFnSc2d1	celá
stránky	stránka	k1gFnSc2	stránka
vyřezán	vyřezat	k5eAaPmNgMnS	vyřezat
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Matrice	matrice	k1gFnSc1	matrice
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
natřena	natřít	k5eAaPmNgFnS	natřít
inkoustem	inkoust	k1gInSc7	inkoust
a	a	k8xC	a
použita	použit	k2eAgNnPc1d1	použito
na	na	k7c4	na
reprodukci	reprodukce	k1gFnSc4	reprodukce
kopií	kopie	k1gFnPc2	kopie
dané	daný	k2eAgFnSc2d1	daná
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
celé	celý	k2eAgFnSc2d1	celá
knihy	kniha	k1gFnSc2	kniha
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
náročný	náročný	k2eAgInSc1d1	náročný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
vyžadující	vyžadující	k2eAgMnSc1d1	vyžadující
ručně	ručně	k6eAd1	ručně
řezanou	řezaný	k2eAgFnSc4d1	řezaná
desku	deska	k1gFnSc4	deska
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
desky	deska	k1gFnPc1	deska
také	také	k9	také
nebyly	být	k5eNaImAgFnP	být
velmi	velmi	k6eAd1	velmi
trvanlivé	trvanlivý	k2eAgFnPc1d1	trvanlivá
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
opotřebovaly	opotřebovat	k5eAaPmAgInP	opotřebovat
nebo	nebo	k8xC	nebo
poškodily	poškodit	k5eAaPmAgInP	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
datovaný	datovaný	k2eAgInSc1d1	datovaný
exemplář	exemplář	k1gInSc1	exemplář
kopie	kopie	k1gFnSc2	kopie
pořízené	pořízený	k2eAgFnSc2d1	pořízená
blokovým	blokový	k1gMnSc7	blokový
tiskem	tisek	k1gMnSc7	tisek
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
Britská	britský	k2eAgFnSc1d1	britská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Desku	deska	k1gFnSc4	deska
nalezl	nalézt	k5eAaBmAgMnS	nalézt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
archeolog	archeolog	k1gMnSc1	archeolog
Marc	Marc	k1gFnSc4	Marc
Aurel	Aurel	k1gMnSc1	Aurel
Stein	Stein	k1gMnSc1	Stein
v	v	k7c6	v
ohrazené	ohrazený	k2eAgFnSc6d1	ohrazená
jeskyni	jeskyně	k1gFnSc6	jeskyně
u	u	k7c2	u
Tun-chuangu	Tunhuang	k1gInSc2	Tun-chuang
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Tiráž	tiráž	k1gFnSc1	tiráž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
:	:	kIx,	:
Uctivě	uctivě	k6eAd1	uctivě
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
pro	pro	k7c4	pro
obecnou	obecný	k2eAgFnSc4d1	obecná
volnou	volný	k2eAgFnSc4d1	volná
distribuci	distribuce	k1gFnSc4	distribuce
Wang	Wang	k1gMnSc1	Wang
Jie	Jie	k1gMnSc1	Jie
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svých	svůj	k3xOyFgMnPc2	svůj
dvou	dva	k4xCgMnPc2	dva
rodičů	rodič	k1gMnPc2	rodič
třináctého	třináctý	k4xOgNnSc2	třináctý
dne	den	k1gInSc2	den
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
měsíce	měsíc	k1gInSc2	měsíc
devátého	devátý	k4xOgInSc2	devátý
roku	rok	k1gInSc2	rok
Xiantongu	Xiantong	k1gInSc2	Xiantong
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
868	[number]	k4	868
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1	čínský
vynálezce	vynálezce	k1gMnSc1	vynálezce
Pi	pi	k0	pi
Sheng	Sheng	k1gInSc1	Sheng
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1045	[number]	k4	1045
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
pohyblivé	pohyblivý	k2eAgFnPc4d1	pohyblivá
litery	litera	k1gFnPc4	litera
z	z	k7c2	z
kameniny	kamenina	k1gFnSc2	kamenina
(	(	kIx(	(
<g/>
žádné	žádný	k3yNgInPc4	žádný
příklady	příklad	k1gInPc4	příklad
jeho	on	k3xPp3gInSc2	on
tisku	tisk	k1gInSc2	tisk
se	se	k3xPyFc4	se
ale	ale	k9	ale
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
a	a	k8xC	a
zarovnávací	zarovnávací	k2eAgFnSc2d1	zarovnávací
výplně	výplň	k1gFnSc2	výplň
vkládal	vkládat	k5eAaImAgInS	vkládat
do	do	k7c2	do
mělké	mělký	k2eAgFnSc2d1	mělká
přihrádky	přihrádka	k1gFnSc2	přihrádka
a	a	k8xC	a
zaléval	zalévat	k5eAaImAgMnS	zalévat
je	být	k5eAaImIp3nS	být
horkým	horký	k2eAgInSc7d1	horký
voskem	vosk	k1gInSc7	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztuhnutí	ztuhnutí	k1gNnSc6	ztuhnutí
vosku	vosk	k1gInSc2	vosk
šla	jít	k5eAaImAgFnS	jít
přihrádka	přihrádka	k1gFnSc1	přihrádka
s	s	k7c7	s
písmeny	písmeno	k1gNnPc7	písmeno
používat	používat	k5eAaImF	používat
na	na	k7c4	na
tisk	tisk	k1gInSc4	tisk
celých	celý	k2eAgFnPc2d1	celá
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Johann	Johann	k1gMnSc1	Johann
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zavedl	zavést	k5eAaPmAgInS	zavést
knihtisk	knihtisk	k1gInSc1	knihtisk
s	s	k7c7	s
kovovými	kovový	k2eAgFnPc7d1	kovová
pohyblivými	pohyblivý	k2eAgFnPc7d1	pohyblivá
literami	litera	k1gFnPc7	litera
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc1	kniha
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
levnější	levný	k2eAgFnPc1d2	levnější
a	a	k8xC	a
široce	široko	k6eAd1	široko
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
tiskařského	tiskařský	k2eAgInSc2d1	tiskařský
lisu	lis	k1gInSc2	lis
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
ročně	ročně	k6eAd1	ročně
jen	jen	k9	jen
asi	asi	k9	asi
1000	[number]	k4	1000
různých	různý	k2eAgFnPc2d1	různá
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Knihtisk	knihtisk	k1gInSc1	knihtisk
představoval	představovat	k5eAaImAgInS	představovat
radikální	radikální	k2eAgFnSc4d1	radikální
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedné	jeden	k4xCgFnSc2	jeden
knihy	kniha	k1gFnSc2	kniha
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
najednou	najednou	k6eAd1	najednou
vytištěny	vytištěn	k2eAgInPc4d1	vytištěn
statisíce	statisíce	k1gInPc4	statisíce
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
jen	jen	k9	jen
dílna	dílna	k1gFnSc1	dílna
Hanse	Hans	k1gMnSc2	Hans
Luffta	Lufft	k1gMnSc2	Lufft
<g/>
,	,	kIx,	,
tiskaře	tiskař	k1gMnSc2	tiskař
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
,	,	kIx,	,
vytiskla	vytisknout	k5eAaPmAgFnS	vytisknout
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1534	[number]	k4	1534
a	a	k8xC	a
1574	[number]	k4	1574
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
Lutherovy	Lutherův	k2eAgFnSc2d1	Lutherova
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
staletí	staletí	k1gNnPc4	staletí
znamenaly	znamenat	k5eAaImAgFnP	znamenat
zlepšení	zlepšení	k1gNnSc4	zlepšení
tiskařského	tiskařský	k2eAgInSc2d1	tiskařský
lisu	lis	k1gInSc2	lis
i	i	k9	i
současně	současně	k6eAd1	současně
i	i	k8xC	i
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
svobodu	svoboda	k1gFnSc4	svoboda
tisku	tisk	k1gInSc2	tisk
postupným	postupný	k2eAgNnSc7d1	postupné
uvolňováním	uvolňování	k1gNnSc7	uvolňování
přísných	přísný	k2eAgMnPc2d1	přísný
zákonů	zákon	k1gInPc2	zákon
cenzury	cenzura	k1gFnSc2	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
knižní	knižní	k2eAgFnSc2d1	knižní
produkce	produkce	k1gFnSc2	produkce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
přes	přes	k7c4	přes
200	[number]	k4	200
000	[number]	k4	000
titulů	titul	k1gInPc2	titul
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
druhého	druhý	k4xOgNnSc2	druhý
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
byl	být	k5eAaImAgInS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
internet	internet	k1gInSc1	internet
a	a	k8xC	a
rozširuje	rozširovat	k5eAaImIp3nS	rozširovat
se	se	k3xPyFc4	se
elektronická	elektronický	k2eAgFnSc1d1	elektronická
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
přesně	přesně	k6eAd1	přesně
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
její	její	k3xOp3gFnSc1	její
součást	součást	k1gFnSc1	součást
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mnoho	mnoho	k4c4	mnoho
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
nedrží	držet	k5eNaImIp3nS	držet
této	tento	k3xDgFnSc2	tento
podoby	podoba	k1gFnSc2	podoba
knih	kniha	k1gFnPc2	kniha
při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
úpravy	úprava	k1gFnSc2	úprava
knihy	kniha	k1gFnSc2	kniha
poznat	poznat	k5eAaPmF	poznat
kvalitu	kvalita	k1gFnSc4	kvalita
nakladatelské	nakladatelský	k2eAgFnSc2d1	nakladatelská
a	a	k8xC	a
knihařské	knihařský	k2eAgFnSc2d1	knihařská
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
knižní	knižní	k2eAgInSc1d1	knižní
blok	blok	k1gInSc1	blok
-	-	kIx~	-
označuje	označovat	k5eAaImIp3nS	označovat
svázané	svázaný	k2eAgFnPc4d1	svázaná
a	a	k8xC	a
oříznuté	oříznutý	k2eAgFnPc4d1	oříznutá
listy	lista	k1gFnPc4	lista
knihy	kniha	k1gFnSc2	kniha
bez	bez	k7c2	bez
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
blok	blok	k1gInSc1	blok
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
výroby	výroba	k1gFnSc2	výroba
přilepen	přilepen	k2eAgMnSc1d1	přilepen
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zavěšen	zavěšen	k2eAgMnSc1d1	zavěšen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
do	do	k7c2	do
desek	deska	k1gFnPc2	deska
obálky	obálka	k1gFnSc2	obálka
pomocí	pomocí	k7c2	pomocí
předsádky	předsádka	k1gFnSc2	předsádka
<g/>
.	.	kIx.	.
signet	signet	k1gInSc1	signet
-	-	kIx~	-
značka	značka	k1gFnSc1	značka
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
nebo	nebo	k8xC	nebo
edice	edice	k1gFnSc1	edice
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
na	na	k7c6	na
první	první	k4xOgFnSc6	první
liché	lichý	k2eAgFnSc6d1	lichá
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
patitul	patitul	k1gInSc1	patitul
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předchází	předcházet	k5eAaImIp3nS	předcházet
hlavní	hlavní	k2eAgInSc1d1	hlavní
titul	titul	k1gInSc1	titul
a	a	k8xC	a
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
autor	autor	k1gMnSc1	autor
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
titul	titul	k1gInSc4	titul
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
titulu	titul	k1gInSc3	titul
je	být	k5eAaImIp3nS	být
vysázen	vysázen	k2eAgInSc1d1	vysázen
menším	malý	k2eAgNnSc7d2	menší
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
tohoto	tento	k3xDgNnSc2	tento
zdvojování	zdvojování	k1gNnSc2	zdvojování
titulního	titulní	k2eAgInSc2d1	titulní
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
knihy	kniha	k1gFnPc1	kniha
ještě	ještě	k6eAd1	ještě
těžké	těžký	k2eAgFnPc1d1	těžká
a	a	k8xC	a
při	při	k7c6	při
častém	častý	k2eAgNnSc6d1	časté
používání	používání	k1gNnSc6	používání
se	se	k3xPyFc4	se
vytrhávaly	vytrhávat	k5eAaImAgFnP	vytrhávat
z	z	k7c2	z
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
deskami	deska	k1gFnPc7	deska
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
ztratil	ztratit	k5eAaPmAgMnS	ztratit
i	i	k9	i
titulní	titulní	k2eAgInSc4d1	titulní
list	list	k1gInSc4	list
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zjistit	zjistit	k5eAaPmF	zjistit
název	název	k1gInSc4	název
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Signet	signet	k1gInSc1	signet
a	a	k8xC	a
patitul	patitul	k1gInSc1	patitul
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
frontispis	frontispis	k1gInSc1	frontispis
-	-	kIx~	-
(	(	kIx(	(
<g/>
též	též	k9	též
protititul	protititul	k1gInSc1	protititul
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
sudou	sudý	k2eAgFnSc4d1	sudá
stránku	stránka	k1gFnSc4	stránka
naproti	naproti	k7c3	naproti
titulnímu	titulní	k2eAgInSc3d1	titulní
listu	list	k1gInSc3	list
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
ilustrace	ilustrace	k1gFnPc1	ilustrace
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgInSc4d1	hlavní
titul	titul	k1gInSc4	titul
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
liché	lichý	k2eAgFnSc6d1	lichá
stránce	stránka	k1gFnSc6	stránka
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
titul	titul	k1gInSc4	titul
a	a	k8xC	a
podtitul	podtitul	k1gInSc4	podtitul
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
také	také	k9	také
obsahovat	obsahovat	k5eAaImF	obsahovat
rok	rok	k1gInSc4	rok
vydání	vydání	k1gNnSc2	vydání
a	a	k8xC	a
název	název	k1gInSc1	název
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
.	.	kIx.	.
hřbet	hřbet	k1gInSc4	hřbet
knihy	kniha	k1gFnSc2	kniha
-	-	kIx~	-
u	u	k7c2	u
lepených	lepený	k2eAgFnPc2d1	lepená
vazeb	vazba	k1gFnPc2	vazba
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
,	,	kIx,	,
u	u	k7c2	u
vázaných	vázaný	k2eAgFnPc2d1	vázaná
knih	kniha	k1gFnPc2	kniha
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovný	rovný	k2eAgInSc4d1	rovný
nebo	nebo	k8xC	nebo
oblý	oblý	k2eAgInSc4d1	oblý
<g/>
.	.	kIx.	.
impresum	impresum	k1gInSc4	impresum
-	-	kIx~	-
(	(	kIx(	(
<g/>
též	též	k9	též
autorská	autorský	k2eAgFnSc1d1	autorská
tiráž	tiráž	k1gFnSc1	tiráž
<g/>
,	,	kIx,	,
copyrightová	copyrightový	k2eAgFnSc1d1	copyrightová
stránka	stránka	k1gFnSc1	stránka
<g/>
,	,	kIx,	,
nakladatelský	nakladatelský	k2eAgInSc1d1	nakladatelský
záznam	záznam	k1gInSc1	záznam
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
titulního	titulní	k2eAgInSc2d1	titulní
listu	list	k1gInSc2	list
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
copyright	copyright	k1gInSc4	copyright
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
ilustrací	ilustrace	k1gFnPc2	ilustrace
<g/>
)	)	kIx)	)
a	a	k8xC	a
ISBN	ISBN	kA	ISBN
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
umístit	umístit	k5eAaPmF	umístit
laudatio	laudatio	k1gNnSc4	laudatio
(	(	kIx(	(
<g/>
poděkování	poděkování	k1gNnSc4	poděkování
<g/>
)	)	kIx)	)
sponzorům	sponzor	k1gMnPc3	sponzor
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Tiráž	tiráž	k1gFnSc1	tiráž
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
listu	list	k1gInSc6	list
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
technické	technický	k2eAgInPc4d1	technický
a	a	k8xC	a
nakladatelské	nakladatelský	k2eAgInPc4d1	nakladatelský
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
v	v	k7c6	v
samostatném	samostatný	k2eAgNnSc6d1	samostatné
hesle	heslo	k1gNnSc6	heslo
tiráž	tiráž	k1gFnSc1	tiráž
</s>
<s>
předsádka	předsádka	k1gFnSc1	předsádka
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
dvojlist	dvojlist	k1gInSc1	dvojlist
(	(	kIx(	(
<g/>
pevnějšího	pevný	k2eAgInSc2d2	pevnější
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
knižní	knižní	k2eAgInSc1d1	knižní
blok	blok	k1gInSc1	blok
s	s	k7c7	s
deskami	deska	k1gFnPc7	deska
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
předsádka	předsádka	k1gFnSc1	předsádka
přední	přední	k2eAgFnSc2d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
potištěna	potisknout	k5eAaPmNgFnS	potisknout
ilustrací	ilustrace	k1gFnSc7	ilustrace
<g/>
.	.	kIx.	.
přídeští	přídeští	k1gNnSc4	přídeští
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
listů	list	k1gInPc2	list
předsádky	předsádka	k1gFnSc2	předsádka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nalepený	nalepený	k2eAgInSc1d1	nalepený
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
(	(	kIx(	(
<g/>
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
surovou	surový	k2eAgFnSc4d1	surová
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
stranu	strana	k1gFnSc4	strana
desky	deska	k1gFnSc2	deska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgMnSc1d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnPc1d1	zadní
<g/>
.	.	kIx.	.
desky	deska	k1gFnPc1	deska
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
obalem	obal	k1gInSc7	obal
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
případě	případ	k1gInSc6	případ
tzv.	tzv.	kA	tzv.
pevné	pevný	k2eAgFnPc4d1	pevná
vazby	vazba	k1gFnPc4	vazba
(	(	kIx(	(
<g/>
hardback	hardback	k6eAd1	hardback
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
titul	titul	k1gInSc4	titul
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Desky	deska	k1gFnPc1	deska
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
zpravidla	zpravidla	k6eAd1	zpravidla
z	z	k7c2	z
lepenky	lepenka	k1gFnSc2	lepenka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
potahovány	potahovat	k5eAaImNgFnP	potahovat
papírem	papír	k1gInSc7	papír
<g/>
,	,	kIx,	,
textilem	textil	k1gInSc7	textil
<g/>
,	,	kIx,	,
koženkou	koženka	k1gFnSc7	koženka
nebo	nebo	k8xC	nebo
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
ořízka	ořízka	k1gFnSc1	ořízka
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
(	(	kIx(	(
<g/>
oříznuté	oříznutý	k2eAgFnPc4d1	oříznutá
<g/>
)	)	kIx)	)
strany	strana	k1gFnPc4	strana
knižního	knižní	k2eAgInSc2d1	knižní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
vlepena	vlepit	k5eAaPmNgFnS	vlepit
do	do	k7c2	do
hřbetu	hřbet	k1gInSc2	hřbet
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ořízka	ořízka	k1gFnSc1	ořízka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obarvená	obarvený	k2eAgFnSc1d1	obarvená
nebo	nebo	k8xC	nebo
zlacená	zlacený	k2eAgFnSc1d1	zlacená
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
prachu	prach	k1gInSc3	prach
<g/>
.	.	kIx.	.
obálka	obálka	k1gFnSc1	obálka
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
obalem	obal	k1gInSc7	obal
knižního	knižní	k2eAgInSc2d1	knižní
bloku	blok	k1gInSc2	blok
v	v	k7c6	v
případě	případ	k1gInSc6	případ
měkké	měkký	k2eAgFnSc2d1	měkká
vazby	vazba	k1gFnSc2	vazba
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
paperback	paperback	k1gInSc1	paperback
<g/>
,	,	kIx,	,
brožovaná	brožovaný	k2eAgFnSc1d1	brožovaná
vazba	vazba	k1gFnSc1	vazba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
plní	plnit	k5eAaImIp3nS	plnit
roli	role	k1gFnSc4	role
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
přebalu	přebal	k1gInSc2	přebal
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
<g/>
.	.	kIx.	.
přebal	přebal	k1gInSc4	přebal
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
list	list	k1gInSc4	list
papíru	papír	k1gInSc2	papír
obalující	obalující	k2eAgFnSc2d1	obalující
desky	deska	k1gFnSc2	deska
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
jak	jak	k8xC	jak
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
informační	informační	k2eAgInSc1d1	informační
(	(	kIx(	(
<g/>
propagační	propagační	k2eAgInSc1d1	propagační
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
potah	potah	k1gInSc1	potah
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
papír	papír	k1gInSc4	papír
nebo	nebo	k8xC	nebo
textil	textil	k1gInSc4	textil
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
zakrytí	zakrytí	k1gNnSc3	zakrytí
lepenky	lepenka	k1gFnSc2	lepenka
knižních	knižní	k2eAgFnPc2d1	knižní
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
spojení	spojení	k1gNnSc3	spojení
záložka	záložka	k1gFnSc1	záložka
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vložená	vložený	k2eAgFnSc1d1	vložená
papírová	papírový	k2eAgFnSc1d1	papírová
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
textilní	textilní	k2eAgInSc4d1	textilní
či	či	k8xC	či
kožený	kožený	k2eAgInSc4d1	kožený
proužek	proužek	k1gInSc4	proužek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
vlepen	vlepit	k5eAaPmNgMnS	vlepit
do	do	k7c2	do
hřbetu	hřbet	k1gInSc2	hřbet
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
odborná	odborný	k2eAgFnSc1d1	odborná
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
více	hodně	k6eAd2	hodně
textilních	textilní	k2eAgFnPc2d1	textilní
záložek	záložka	k1gFnPc2	záložka
<g/>
.	.	kIx.	.
</s>
<s>
Správně	správně	k6eAd1	správně
provedená	provedený	k2eAgFnSc1d1	provedená
textilní	textilní	k2eAgFnSc1d1	textilní
či	či	k8xC	či
kožená	kožený	k2eAgFnSc1d1	kožená
záložka	záložka	k1gFnSc1	záložka
se	se	k3xPyFc4	se
pozná	poznat	k5eAaPmIp3nS	poznat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	pře	k1gFnSc3	pře
uchopení	uchopení	k1gNnSc2	uchopení
jejího	její	k3xOp3gInSc2	její
konce	konec	k1gInSc2	konec
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
pohodlně	pohodlně	k6eAd1	pohodlně
projedete	projet	k5eAaPmIp2nP	projet
celý	celý	k2eAgInSc4d1	celý
knižní	knižní	k2eAgInSc4d1	knižní
blok	blok	k1gInSc4	blok
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
byste	by	kYmCp2nP	by
ji	on	k3xPp3gFnSc4	on
museli	muset	k5eAaImAgMnP	muset
pustit	pustit	k5eAaPmF	pustit
<g/>
.	.	kIx.	.
kapitálek	kapitálek	k1gInSc4	kapitálek
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
proužek	proužek	k1gInSc1	proužek
textilie	textilie	k1gFnSc2	textilie
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
kožený	kožený	k2eAgMnSc1d1	kožený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
především	především	k6eAd1	především
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vlepen	vlepit	k5eAaPmNgMnS	vlepit
do	do	k7c2	do
hřbetu	hřbet	k1gInSc2	hřbet
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zakrýval	zakrývat	k5eAaImAgMnS	zakrývat
mezeru	mezera	k1gFnSc4	mezera
mezi	mezi	k7c7	mezi
blokem	blok	k1gInSc7	blok
a	a	k8xC	a
deskami	deska	k1gFnPc7	deska
a	a	k8xC	a
chránil	chránit	k5eAaImAgMnS	chránit
tak	tak	k6eAd1	tak
hřbet	hřbet	k1gMnSc1	hřbet
zavřené	zavřený	k2eAgFnSc2d1	zavřená
knihy	kniha	k1gFnSc2	kniha
před	před	k7c7	před
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
vakát	vakát	k1gInSc1	vakát
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
záměrná	záměrný	k2eAgFnSc1d1	záměrná
nebo	nebo	k8xC	nebo
nezamýšlená	zamýšlený	k2eNgFnSc1d1	nezamýšlená
<g/>
)	)	kIx)	)
kdekoliv	kdekoliv	k6eAd1	kdekoliv
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
.	.	kIx.	.
paginace	paginace	k1gFnSc1	paginace
-	-	kIx~	-
číslování	číslování	k1gNnSc1	číslování
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
dolním	dolní	k2eAgInSc6d1	dolní
rohu	roh	k1gInSc6	roh
stran	strana	k1gFnPc2	strana
nebo	nebo	k8xC	nebo
uprostřed	uprostřed	k6eAd1	uprostřed
nebo	nebo	k8xC	nebo
v	v	k7c6	v
záhlaví	záhlaví	k1gNnSc6	záhlaví
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
oddíl	oddíl	k1gInSc1	oddíl
textu	text	k1gInSc2	text
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
samostatné	samostatný	k2eAgNnSc4d1	samostatné
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kapitola	kapitola	k1gFnSc1	kapitola
musí	muset	k5eAaImIp3nS	muset
začínat	začínat	k5eAaImF	začínat
na	na	k7c6	na
liché	lichý	k2eAgFnSc6d1	lichá
stránce	stránka	k1gFnSc6	stránka
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
názvem	název	k1gInSc7	název
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
číslicí	číslice	k1gFnSc7	číslice
<g/>
.	.	kIx.	.
obsah	obsah	k1gInSc1	obsah
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc4	seznam
všech	všecek	k3xTgFnPc2	všecek
kapitol	kapitola	k1gFnPc2	kapitola
a	a	k8xC	a
podkapitol	podkapitola	k1gFnPc2	podkapitola
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
odborných	odborný	k2eAgFnPc2d1	odborná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
u	u	k7c2	u
odborných	odborný	k2eAgFnPc2d1	odborná
publikací	publikace	k1gFnPc2	publikace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěn	umístit	k5eAaPmNgInS	umístit
i	i	k9	i
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
za	za	k7c7	za
impresem	impres	k1gInSc7	impres
<g/>
.	.	kIx.	.
rejstřík	rejstřík	k1gInSc1	rejstřík
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
místní	místní	k2eAgInSc1d1	místní
<g/>
,	,	kIx,	,
věcný	věcný	k2eAgInSc1d1	věcný
nebo	nebo	k8xC	nebo
personální	personální	k2eAgInSc1d1	personální
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
také	také	k9	také
být	být	k5eAaImF	být
nerozlišený	rozlišený	k2eNgMnSc1d1	nerozlišený
<g/>
.	.	kIx.	.
</s>
<s>
Věci	věc	k1gFnPc1	věc
nebo	nebo	k8xC	nebo
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
autor	autor	k1gMnSc1	autor
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
rejstříku	rejstřík	k1gInSc6	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
zároveň	zároveň	k6eAd1	zároveň
uvedena	uvést	k5eAaPmNgFnS	uvést
každá	každý	k3xTgFnSc1	každý
strana	strana	k1gFnSc1	strana
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
v	v	k7c6	v
textu	text	k1gInSc6	text
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Rejstřík	rejstřík	k1gInSc1	rejstřík
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
odborné	odborný	k2eAgFnSc2d1	odborná
nebo	nebo	k8xC	nebo
populárně	populárně	k6eAd1	populárně
naučné	naučný	k2eAgFnSc2d1	naučná
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
bibliografie	bibliografie	k1gFnSc1	bibliografie
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
soupis	soupis	k1gInSc1	soupis
jiných	jiný	k2eAgFnPc2d1	jiná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
ex	ex	k6eAd1	ex
libris	libris	k1gFnSc1	libris
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
ozdobné	ozdobný	k2eAgNnSc1d1	ozdobné
označení	označení	k1gNnSc1	označení
vlastníka	vlastník	k1gMnSc2	vlastník
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
s	s	k7c7	s
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
(	(	kIx(	(
<g/>
pevnou	pevný	k2eAgFnSc7d1	pevná
<g/>
)	)	kIx)	)
vazbou	vazba	k1gFnSc7	vazba
(	(	kIx(	(
<g/>
též	též	k9	též
vázané	vázaný	k2eAgFnPc1d1	vázaná
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
polotuhou	polotuhý	k2eAgFnSc7d1	polotuhá
vazbou	vazba	k1gFnSc7	vazba
(	(	kIx(	(
<g/>
leporela	leporelo	k1gNnPc4	leporelo
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgFnPc4d1	dětská
skládačky	skládačka	k1gFnPc4	skládačka
<g/>
)	)	kIx)	)
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
lepenka	lepenka	k1gFnSc1	lepenka
do	do	k7c2	do
600	[number]	k4	600
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
s	s	k7c7	s
měkkou	měkký	k2eAgFnSc7d1	měkká
vazbou	vazba	k1gFnSc7	vazba
(	(	kIx(	(
<g/>
též	též	k9	též
brožury	brožura	k1gFnPc4	brožura
<g/>
,	,	kIx,	,
paperbacky	paperback	k1gInPc4	paperback
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c4	na
obálku	obálka	k1gFnSc4	obálka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
těžší	těžký	k2eAgInSc4d2	těžší
papír	papír	k1gInSc4	papír
než	než	k8xS	než
na	na	k7c4	na
blok	blok	k1gInSc4	blok
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
lepenka	lepenka	k1gFnSc1	lepenka
<g />
.	.	kIx.	.
</s>
<s>
V1	V1	k4	V1
-	-	kIx~	-
měkké	měkký	k2eAgFnSc2d1	měkká
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
sešité	sešitý	k2eAgNnSc1d1	sešité
drátěnou	drátěný	k2eAgFnSc7d1	drátěná
sponkou	sponka	k1gFnSc7	sponka
V2	V2	k1gFnSc2	V2
-	-	kIx~	-
měkké	měkký	k2eAgFnSc2d1	měkká
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
lepená	lepený	k2eAgFnSc1d1	lepená
<g/>
,	,	kIx,	,
oříznutá	oříznutý	k2eAgFnSc1d1	oříznutá
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
stranách	strana	k1gFnPc6	strana
(	(	kIx(	(
<g/>
i	i	k8xC	i
s	s	k7c7	s
obálkou	obálka	k1gFnSc7	obálka
<g/>
)	)	kIx)	)
V	v	k7c4	v
<g/>
2	[number]	k4	2
<g/>
a	a	k8xC	a
-	-	kIx~	-
lepená	lepený	k2eAgFnSc1d1	lepená
brožura	brožura	k1gFnSc1	brožura
s	s	k7c7	s
přesahem	přesah	k1gInSc7	přesah
obálky	obálka	k1gFnSc2	obálka
V	v	k7c6	v
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
-	-	kIx~	-
lepená	lepený	k2eAgFnSc1d1	lepená
brožura	brožura	k1gFnSc1	brožura
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
předsádka	předsádka	k1gFnSc1	předsádka
je	být	k5eAaImIp3nS	být
přilepena	přilepit	k5eAaPmNgFnS	přilepit
k	k	k7c3	k
obálce	obálka	k1gFnSc3	obálka
V3	V3	k1gFnPc3	V3
-	-	kIx~	-
lepená	lepený	k2eAgFnSc1d1	lepená
brožura	brožura	k1gFnSc1	brožura
sešitá	sešitý	k2eAgFnSc1d1	sešitá
drátem	drát	k1gInSc7	drát
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
blokovaná	blokovaný	k2eAgFnSc1d1	blokovaná
brožura	brožura	k1gFnSc1	brožura
V4	V4	k1gFnSc1	V4
-	-	kIx~	-
šitá	šitý	k2eAgFnSc1d1	šitá
brožura	brožura	k1gFnSc1	brožura
(	(	kIx(	(
<g/>
vázaný	vázaný	k2eAgInSc1d1	vázaný
blok	blok	k1gInSc1	blok
přilepený	přilepený	k2eAgInSc1d1	přilepený
k	k	k7c3	k
měkké	měkký	k2eAgFnSc3d1	měkká
obálce	obálka	k1gFnSc3	obálka
<g/>
)	)	kIx)	)
V5	V5	k1gFnSc1	V5
-	-	kIx~	-
polotuhá	polotuhý	k2eAgFnSc1d1	polotuhá
vazba	vazba	k1gFnSc1	vazba
s	s	k7c7	s
deskami	deska	k1gFnPc7	deska
z	z	k7c2	z
lehčí	lehký	k2eAgFnSc2d2	lehčí
lepenky	lepenka	k1gFnSc2	lepenka
-	-	kIx~	-
knižní	knižní	k2eAgInSc1d1	knižní
blok	blok	k1gInSc1	blok
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
lepen	lepen	k2eAgMnSc1d1	lepen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
sešit	sešit	k1gInSc1	sešit
nití	nit	k1gFnPc2	nit
<g/>
,	,	kIx,	,
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
V	v	k7c4	v
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nepoužívá	používat	k5eNaImIp3nS	používat
V6	V6	k1gFnSc4	V6
-	-	kIx~	-
leporela	leporelo	k1gNnPc1	leporelo
V7	V7	k1gFnPc2	V7
-	-	kIx~	-
poloplátěná	poloplátěný	k2eAgFnSc1d1	poloplátěná
vazba	vazba	k1gFnSc1	vazba
(	(	kIx(	(
<g/>
šitý	šitý	k2eAgInSc1d1	šitý
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
plátěný	plátěný	k2eAgInSc1d1	plátěný
hřbet	hřbet	k1gInSc1	hřbet
<g/>
,	,	kIx,	,
papírový	papírový	k2eAgInSc1d1	papírový
potah	potah	k1gInSc1	potah
desek	deska	k1gFnPc2	deska
<g/>
)	)	kIx)	)
V8	V8	k1gFnPc2	V8
-	-	kIx~	-
pevné	pevný	k2eAgFnSc2d1	pevná
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
šitý	šitý	k2eAgInSc1d1	šitý
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
plátěný	plátěný	k2eAgInSc1d1	plátěný
<g />
.	.	kIx.	.
</s>
<s>
potah	potah	k1gInSc1	potah
<g/>
,	,	kIx,	,
papírový	papírový	k2eAgInSc1d1	papírový
přebal	přebal	k1gInSc1	přebal
V	v	k7c6	v
<g/>
8	[number]	k4	8
<g/>
a	a	k8xC	a
-	-	kIx~	-
pevné	pevný	k2eAgFnPc4d1	pevná
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
šitý	šitý	k2eAgInSc1d1	šitý
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
papírový	papírový	k2eAgInSc1d1	papírový
(	(	kIx(	(
<g/>
laminovaný	laminovaný	k2eAgInSc1d1	laminovaný
<g/>
)	)	kIx)	)
potah	potah	k1gInSc1	potah
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
přebalu	přebal	k1gInSc2	přebal
V	v	k7c6	v
<g/>
8	[number]	k4	8
<g/>
b	b	k?	b
-	-	kIx~	-
pevné	pevný	k2eAgFnSc2d1	pevná
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
šitý	šitý	k2eAgInSc1d1	šitý
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
papírový	papírový	k2eAgInSc1d1	papírový
potah	potah	k1gInSc1	potah
<g/>
,	,	kIx,	,
papírový	papírový	k2eAgInSc1d1	papírový
přebal	přebal	k1gInSc1	přebal
V9	V9	k1gFnSc2	V9
-	-	kIx~	-
plastové	plastový	k2eAgFnSc2d1	plastová
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
šitý	šitý	k2eAgInSc1d1	šitý
blok	blok	k1gInSc1	blok
Potah	potah	k1gInSc1	potah
obálek	obálka	k1gFnPc2	obálka
(	(	kIx(	(
<g/>
knižních	knižní	k2eAgFnPc2d1	knižní
desek	deska	k1gFnPc2	deska
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
papíru	papír	k1gInSc2	papír
často	často	k6eAd1	často
laminován	laminován	k2eAgInSc1d1	laminován
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
papíru	papír	k1gInSc2	papír
je	být	k5eAaImIp3nS	být
potažen	potáhnout	k5eAaPmNgInS	potáhnout
slabou	slabý	k2eAgFnSc7d1	slabá
fólií	fólie	k1gFnSc7	fólie
-	-	kIx~	-
lesklou	lesklý	k2eAgFnSc7d1	lesklá
<g/>
,	,	kIx,	,
matnou	matný	k2eAgFnSc7d1	matná
nebo	nebo	k8xC	nebo
strukturovanou	strukturovaný	k2eAgFnSc7d1	strukturovaná
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
lakování	lakování	k1gNnSc1	lakování
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Laik	laik	k1gMnSc1	laik
rozdíl	rozdíl	k1gInSc1	rozdíl
téměř	téměř	k6eAd1	téměř
nepozná	poznat	k5eNaPmIp3nS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
efektivitu	efektivita	k1gFnSc4	efektivita
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nalakovat	nalakovat	k5eAaPmF	nalakovat
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
obálky	obálka	k1gFnSc2	obálka
<g/>
,	,	kIx,	,
přebalu	přebal	k1gInSc2	přebal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
parciálním	parciální	k2eAgInSc7d1	parciální
lakem	lak	k1gInSc7	lak
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
obálka	obálka	k1gFnSc1	obálka
zdobí	zdobit	k5eAaImIp3nS	zdobit
slepotiskem	slepotisk	k1gInSc7	slepotisk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vytlačení	vytlačení	k1gNnSc1	vytlačení
nápisu	nápis	k1gInSc2	nápis
nebo	nebo	k8xC	nebo
obrazce	obrazec	k1gInPc4	obrazec
do	do	k7c2	do
obálky	obálka	k1gFnSc2	obálka
(	(	kIx(	(
<g/>
přebalu	přebal	k1gInSc2	přebal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ražba	ražba	k1gFnSc1	ražba
je	být	k5eAaImIp3nS	být
vyražení	vyražení	k1gNnSc4	vyražení
nápisu	nápis	k1gInSc2	nápis
nebo	nebo	k8xC	nebo
obrazce	obrazec	k1gInPc4	obrazec
do	do	k7c2	do
obálky	obálka	k1gFnSc2	obálka
knihy	kniha	k1gFnSc2	kniha
přes	přes	k7c4	přes
slabou	slabý	k2eAgFnSc4d1	slabá
fólii	fólie	k1gFnSc4	fólie
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
na	na	k7c4	na
obálku	obálka	k1gFnSc4	obálka
přenese	přenést	k5eAaPmIp3nS	přenést
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
ISSN	ISSN	kA	ISSN
Knihtisk	knihtisk	k1gInSc1	knihtisk
Neperiodická	periodický	k2eNgFnSc1d1	neperiodická
publikace	publikace	k1gFnSc1	publikace
Knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
knihovna	knihovna	k1gFnSc1	knihovna
Svaz	svaz	k1gInSc1	svaz
českých	český	k2eAgMnPc2d1	český
knihkupců	knihkupec	k1gMnPc2	knihkupec
a	a	k8xC	a
nakladatelů	nakladatel	k1gMnPc2	nakladatel
-	-	kIx~	-
SČKN	SČKN	kA	SČKN
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
kniha	kniha	k1gFnSc1	kniha
Hybridní	hybridní	k2eAgFnSc1d1	hybridní
kniha	kniha	k1gFnSc1	kniha
Biblioterapie	Biblioterapie	k1gFnSc1	Biblioterapie
Bibliofilie	bibliofilie	k1gFnSc1	bibliofilie
Manuskript	manuskript	k1gInSc4	manuskript
Svitek	svitek	k1gInSc1	svitek
"	"	kIx"	"
<g/>
Kniha	kniha	k1gFnSc1	kniha
knih	kniha	k1gFnPc2	kniha
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Knižní	knižní	k2eAgInSc4d1	knižní
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
knihy	kniha	k1gFnSc2	kniha
<g/>
"	"	kIx"	"
Pistorius	Pistorius	k1gMnSc1	Pistorius
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Paseka	paseka	k1gFnSc1	paseka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
2003	[number]	k4	2003
DOLENSKÝ	DOLENSKÝ	kA	DOLENSKÝ
<g/>
,	,	kIx,	,
Ctibor	Ctibor	k1gMnSc1	Ctibor
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgNnSc1d1	domácí
knihařství	knihařství	k1gNnSc1	knihařství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Šolc	Šolc	k1gMnSc1	Šolc
a	a	k8xC	a
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
CHYTIL	Chytil	k1gMnSc1	Chytil
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
knihařství	knihařství	k1gNnSc2	knihařství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Společenstvo	společenstvo	k1gNnSc1	společenstvo
knihařů	knihař	k1gMnPc2	knihař
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KRIEBEL	KRIEBEL	kA	KRIEBEL
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Vazba	vazba	k1gFnSc1	vazba
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Ústřední	ústřední	k2eAgInSc1d1	ústřední
spolek	spolek	k1gInSc1	spolek
učitelský	učitelský	k2eAgInSc1d1	učitelský
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
HRABENOVSKÝ	HRABENOVSKÝ	kA	HRABENOVSKÝ
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgMnSc1d1	domácí
knihař	knihař	k1gMnSc1	knihař
<g/>
.	.	kIx.	.
</s>
<s>
Ruda	ruda	k1gFnSc1	ruda
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
:	:	kIx,	:
J.	J.	kA	J.
Vlad	Vlad	k1gInSc1	Vlad
<g/>
.	.	kIx.	.
</s>
<s>
Mangl	mangl	k1gInSc1	mangl
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kniha	kniha	k1gFnSc1	kniha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
kniha	kniha	k1gFnSc1	kniha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kniha	kniha	k1gFnSc1	kniha
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gFnSc1	téma
Kniha	kniha	k1gFnSc1	kniha
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Kategorie	kategorie	k1gFnSc2	kategorie
Čítárna	čítárna	k1gFnSc1	čítárna
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
Kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
knihy	kniha	k1gFnSc2	kniha
ve	v	k7c6	v
Žďáře	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
co	co	k9	co
dělá	dělat	k5eAaImIp3nS	dělat
-	-	kIx~	-
Knihy	kniha	k1gFnPc1	kniha
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
kniha	kniha	k1gFnSc1	kniha
-	-	kIx~	-
Technický	technický	k2eAgInSc1d1	technický
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
knihovníky	knihovník	k1gMnPc4	knihovník
Jak	jak	k8xC	jak
vydat	vydat	k5eAaPmF	vydat
knihu	kniha	k1gFnSc4	kniha
Ekolist	Ekolist	k1gInSc1	Ekolist
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
zelenější	zelený	k2eAgFnSc1d2	zelenější
-	-	kIx~	-
klasická	klasický	k2eAgFnSc1d1	klasická
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
elektronická	elektronický	k2eAgFnSc1d1	elektronická
čtečka	čtečka	k1gFnSc1	čtečka
<g/>
?	?	kIx.	?
</s>
