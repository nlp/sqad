<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Budapest	Budapest	k1gFnSc1	Budapest
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgInPc4d1	dopravní
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
Pešťské	pešťský	k2eAgFnSc2d1	pešťská
župy	župa	k1gFnSc2	župa
(	(	kIx(	(
<g/>
Pest	Pest	k2eAgInSc1d1	Pest
megye	megye	k1gInSc1	megye
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
sloučením	sloučení	k1gNnSc7	sloučení
tří	tři	k4xCgFnPc2	tři
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
samostatných	samostatný	k2eAgFnPc2d1	samostatná
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
Budína	Budín	k1gInSc2	Budín
<g/>
,	,	kIx,	,
Starého	Starého	k2eAgInSc2d1	Starého
Budína	Budín	k1gInSc2	Budín
a	a	k8xC	a
Pešti	Pešť	k1gFnSc2	Pešť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
525	[number]	k4	525
km2	km2	k4	km2
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
1,74	[number]	k4	1,74
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
pak	pak	k6eAd1	pak
2,45	[number]	k4	2,45
milionu	milion	k4xCgInSc2	milion
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
podél	podél	k7c2	podél
břehů	břeh	k1gInPc2	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
Budínský	budínský	k2eAgInSc4d1	budínský
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
Andrássyho	Andrássy	k1gMnSc2	Andrássy
třída	třída	k1gFnSc1	třída
jsou	být	k5eAaImIp3nP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
28	[number]	k4	28
km	km	kA	km
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
několik	několik	k4yIc4	několik
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
;	;	kIx,	;
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
Markétin	Markétin	k2eAgInSc4d1	Markétin
(	(	kIx(	(
<g/>
Margit-sziget	Margitziget	k1gInSc4	Margit-sziget
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
Csepelský	Csepelský	k2eAgInSc1d1	Csepelský
(	(	kIx(	(
<g/>
Csepel-sziget	Csepelziget	k1gInSc1	Csepel-sziget
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jižní	jižní	k2eAgFnSc2d1	jižní
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
Dunaje	Dunaj	k1gInSc2	Dunaj
v	v	k7c6	v
maďarské	maďarský	k2eAgFnSc6d1	maďarská
metropoli	metropol	k1gFnSc6	metropol
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
330	[number]	k4	330
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
dělí	dělit	k5eAaImIp3nS	dělit
město	město	k1gNnSc4	město
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
výrazně	výrazně	k6eAd1	výrazně
odlišného	odlišný	k2eAgInSc2d1	odlišný
krajinného	krajinný	k2eAgInSc2d1	krajinný
rázu	ráz	k1gInSc2	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
levobřežní	levobřežní	k2eAgFnSc1d1	levobřežní
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
Pešť	Pešť	k1gFnSc1	Pešť
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
)	)	kIx)	)
značně	značně	k6eAd1	značně
kopcovitý	kopcovitý	k2eAgMnSc1d1	kopcovitý
-	-	kIx~	-
Budínské	Budínské	k2eAgInPc4d1	Budínské
vrchy	vrch	k1gInPc4	vrch
(	(	kIx(	(
<g/>
Budai-hegység	Budaiegység	k1gInSc4	Budai-hegység
<g/>
)	)	kIx)	)
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
výšky	výška	k1gFnSc2	výška
527	[number]	k4	527
m	m	kA	m
(	(	kIx(	(
<g/>
János-hegy	Jánoseg	k1gInPc1	János-heg
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nejnižším	nízký	k2eAgNnSc7d3	nejnižší
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
Dunaje	Dunaj	k1gInSc2	Dunaj
(	(	kIx(	(
<g/>
98	[number]	k4	98
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
105	[number]	k4	105
m.	m.	k?	m.
V	v	k7c6	v
Budínských	Budínských	k2eAgInPc6d1	Budínských
vrších	vrch	k1gInPc6	vrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
turisticky	turisticky	k6eAd1	turisticky
přístupných	přístupný	k2eAgFnPc2d1	přístupná
jeskyní	jeskyně	k1gFnPc2	jeskyně
(	(	kIx(	(
<g/>
Pálvölgyi-barlang	Pálvölgyiarlang	k1gMnSc1	Pálvölgyi-barlang
<g/>
,	,	kIx,	,
Mátyás-barlang	Mátyásarlang	k1gMnSc1	Mátyás-barlang
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
přírodním	přírodní	k2eAgNnSc7d1	přírodní
bohatstvím	bohatství	k1gNnSc7	bohatství
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
termální	termální	k2eAgInPc1d1	termální
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgMnPc3	jenž
je	být	k5eAaImIp3nS	být
Budapešť	Budapešť	k1gFnSc1	Budapešť
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
lázeňských	lázeňský	k2eAgNnPc2d1	lázeňské
měst	město	k1gNnPc2	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
středoevropské	středoevropský	k2eAgNnSc1d1	středoevropské
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
lednová	lednový	k2eAgFnSc1d1	lednová
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
-2	-2	k4	-2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
22	[number]	k4	22
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
620	[number]	k4	620
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
u	u	k7c2	u
pramenů	pramen	k1gInPc2	pramen
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Gellértova	Gellértův	k2eAgInSc2d1	Gellértův
vrchu	vrch	k1gInSc2	vrch
sídlili	sídlit	k5eAaImAgMnP	sídlit
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
89	[number]	k4	89
n.	n.	k?	n.
l.	l.	k?	l.
založili	založit	k5eAaPmAgMnP	založit
Římané	Říman	k1gMnPc1	Říman
severně	severně	k6eAd1	severně
od	od	k7c2	od
dnešního	dnešní	k2eAgNnSc2d1	dnešní
centra	centrum	k1gNnSc2	centrum
osadu	osada	k1gFnSc4	osada
Aquincum	Aquincum	k1gNnSc1	Aquincum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
zdejšího	zdejší	k2eAgInSc2d1	zdejší
přechodu	přechod	k1gInSc2	přechod
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
na	na	k7c6	na
protějším	protější	k2eAgInSc6d1	protější
(	(	kIx(	(
<g/>
pešťském	pešťský	k2eAgInSc6d1	pešťský
<g/>
)	)	kIx)	)
břehu	břeh	k1gInSc6	břeh
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
opevněné	opevněný	k2eAgNnSc1d1	opevněné
Contra	Contrum	k1gNnSc2	Contrum
Aquincum	Aquincum	k1gNnSc4	Aquincum
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Náměstí	náměstí	k1gNnSc2	náměstí
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Aquincu	Aquincum	k1gNnSc6	Aquincum
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
vojenská	vojenský	k2eAgFnSc1d1	vojenská
posádka	posádka	k1gFnSc1	posádka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
106	[number]	k4	106
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
Pannonia	Pannonium	k1gNnSc2	Pannonium
Inferior	Inferiora	k1gFnPc2	Inferiora
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
dosti	dosti	k6eAd1	dosti
velké	velký	k2eAgNnSc4d1	velké
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
;	;	kIx,	;
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
období	období	k1gNnSc6	období
stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
znamenaly	znamenat	k5eAaImAgFnP	znamenat
také	také	k9	také
vylidnění	vylidnění	k1gNnSc2	vylidnění
Aquinca	Aquinc	k1gInSc2	Aquinc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Panonii	Panonie	k1gFnSc6	Panonie
se	se	k3xPyFc4	se
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
Hunové	Hun	k1gMnPc1	Hun
<g/>
,	,	kIx,	,
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
,	,	kIx,	,
Langobardi	Langobard	k1gMnPc1	Langobard
<g/>
,	,	kIx,	,
Avaři	Avar	k1gMnPc1	Avar
<g/>
,	,	kIx,	,
Slované	Slovan	k1gMnPc1	Slovan
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
počátkem	počátkem	k7c2	počátkem
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
Maďaři	Maďar	k1gMnPc1	Maďar
vedení	vedení	k1gNnSc2	vedení
knížetem	kníže	k1gMnSc7	kníže
Arpádem	Arpád	k1gMnSc7	Arpád
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
dunajský	dunajský	k2eAgInSc1d1	dunajský
ostrov	ostrov	k1gInSc1	ostrov
Csepel	Csepela	k1gFnPc2	Csepela
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
počátkem	počátek	k1gInSc7	počátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Panonii	Panonie	k1gFnSc6	Panonie
usadili	usadit	k5eAaPmAgMnP	usadit
Maďaři	Maďar	k1gMnPc1	Maďar
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
opěrných	opěrný	k2eAgInPc2d1	opěrný
bodů	bod	k1gInPc2	bod
Budín	Budín	k1gMnSc1	Budín
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
Starý	Starý	k1gMnSc1	Starý
Budín	Budín	k1gMnSc1	Budín
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Óbuda	Óbuda	k1gFnSc1	Óbuda
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Alt-Ofen	Alt-Ofen	k2eAgInSc1d1	Alt-Ofen
<g/>
)	)	kIx)	)
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
jižním	jižní	k2eAgNnSc6d1	jižní
sousedství	sousedství	k1gNnSc6	sousedství
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Aquinca	Aquinc	k1gInSc2	Aquinc
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
se	se	k3xPyFc4	se
vykládá	vykládat	k5eAaImIp3nS	vykládat
různě	různě	k6eAd1	různě
-	-	kIx~	-
buď	buď	k8xC	buď
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
Arpádova	Arpádův	k2eAgMnSc2d1	Arpádův
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Budy	Buda	k1gMnSc2	Buda
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
slovanského	slovanský	k2eAgInSc2d1	slovanský
výrazu	výraz	k1gInSc2	výraz
<g/>
,	,	kIx,	,
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
příbytek	příbytek	k1gInSc1	příbytek
<g/>
,	,	kIx,	,
stavení	stavení	k1gNnSc1	stavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
byl	být	k5eAaImAgMnS	být
Budín	Budín	k1gMnSc1	Budín
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
sídel	sídlo	k1gNnPc2	sídlo
uherských	uherský	k2eAgMnPc2d1	uherský
panovníků	panovník	k1gMnPc2	panovník
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
Ostřihomi	Ostřiho	k1gFnPc7	Ostřiho
(	(	kIx(	(
<g/>
Esztergom	Esztergom	k1gInSc1	Esztergom
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stoličného	stoličný	k1gMnSc2	stoličný
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
(	(	kIx(	(
<g/>
Székesfehérvár	Székesfehérvár	k1gInSc1	Székesfehérvár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pohanské	pohanský	k2eAgFnSc2d1	pohanská
vzpoury	vzpoura	k1gFnSc2	vzpoura
roku	rok	k1gInSc2	rok
1046	[number]	k4	1046
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
u	u	k7c2	u
Budína	Budín	k1gMnSc2	Budín
umučen	umučen	k2eAgMnSc1d1	umučen
csanádský	csanádský	k2eAgMnSc1d1	csanádský
biskup	biskup	k1gMnSc1	biskup
Gerhard	Gerhard	k1gMnSc1	Gerhard
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Gellért	Gellért	k1gInSc1	Gellért
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc1	vrch
nazýván	nazývat	k5eAaImNgInS	nazývat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
Pešť	Pešť	k1gFnSc1	Pešť
poprvé	poprvé	k6eAd1	poprvé
připomíná	připomínat	k5eAaImIp3nS	připomínat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1148	[number]	k4	1148
<g/>
;	;	kIx,	;
samo	sám	k3xTgNnSc1	sám
osídlení	osídlení	k1gNnSc1	osídlení
však	však	k9	však
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
již	již	k6eAd1	již
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pec	pec	k1gFnSc1	pec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slibný	slibný	k2eAgInSc1d1	slibný
vývoj	vývoj	k1gInSc1	vývoj
však	však	k9	však
roku	rok	k1gInSc2	rok
1242	[number]	k4	1242
narušil	narušit	k5eAaPmAgInS	narušit
mongolský	mongolský	k2eAgInSc1d1	mongolský
vpád	vpád	k1gInSc1	vpád
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zpustošil	zpustošit	k5eAaPmAgInS	zpustošit
celou	celý	k2eAgFnSc4d1	celá
středovýchodní	středovýchodní	k2eAgFnSc4d1	středovýchodní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Béla	Béla	k1gMnSc1	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
přikázal	přikázat	k5eAaPmAgInS	přikázat
stavět	stavět	k5eAaImF	stavět
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
pevné	pevný	k2eAgInPc4d1	pevný
kamenné	kamenný	k2eAgInPc4d1	kamenný
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
takových	takový	k3xDgFnPc2	takový
silných	silný	k2eAgFnPc2d1	silná
pevností	pevnost	k1gFnPc2	pevnost
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
na	na	k7c6	na
vysokém	vysoký	k2eAgInSc6d1	vysoký
kopci	kopec	k1gInSc6	kopec
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
(	(	kIx(	(
<g/>
Starého	Starého	k2eAgInSc2d1	Starého
<g/>
)	)	kIx)	)
Budína	Budín	k1gInSc2	Budín
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgInSc6d1	dnešní
Hradním	hradní	k2eAgInSc6d1	hradní
vrchu	vrch	k1gInSc6	vrch
(	(	kIx(	(
<g/>
Várhegy	Várhega	k1gFnSc2	Várhega
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1261	[number]	k4	1261
zde	zde	k6eAd1	zde
až	až	k6eAd1	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sídlili	sídlit	k5eAaImAgMnP	sídlit
uherští	uherský	k2eAgMnPc1d1	uherský
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
nového	nový	k2eAgInSc2d1	nový
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
převzalo	převzít	k5eAaPmAgNnS	převzít
název	název	k1gInSc4	název
staršího	starší	k1gMnSc2	starší
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
sousedství	sousedství	k1gNnSc6	sousedství
-	-	kIx~	-
Budín	Budín	k1gMnSc1	Budín
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Buda	Bud	k2eAgFnSc1d1	Buda
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Ofen	Ofen	k1gNnSc1	Ofen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budín	Budín	k1gInSc1	Budín
i	i	k8xC	i
Pešť	Pešť	k1gFnSc1	Pešť
vzkvétaly	vzkvétat	k5eAaImAgFnP	vzkvétat
zejména	zejména	k9	zejména
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
(	(	kIx(	(
<g/>
zřízení	zřízení	k1gNnSc2	zřízení
univerzity	univerzita	k1gFnSc2	univerzita
1395	[number]	k4	1395
<g/>
)	)	kIx)	)
a	a	k8xC	a
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
tiskárna	tiskárna	k1gFnSc1	tiskárna
1473	[number]	k4	1473
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
budínském	budínský	k2eAgInSc6d1	budínský
hradě	hrad	k1gInSc6	hrad
sídlili	sídlit	k5eAaImAgMnP	sídlit
také	také	k9	také
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1490	[number]	k4	1490
až	až	k9	až
1526	[number]	k4	1526
čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jagellonců	Jagellonec	k1gInPc2	Jagellonec
čítalo	čítat	k5eAaImAgNnS	čítat
budapešťské	budapešťský	k2eAgNnSc1d1	budapešťské
trojměstí	trojměstí	k1gNnSc1	trojměstí
kolem	kolem	k7c2	kolem
25-30	[number]	k4	25-30
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
patřilo	patřit	k5eAaImAgNnS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
Vídní	Vídeň	k1gFnSc7	Vídeň
a	a	k8xC	a
Krakovem	Krakov	k1gInSc7	Krakov
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
sídlům	sídlo	k1gNnPc3	sídlo
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
střediskem	středisko	k1gNnSc7	středisko
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
dobytkem	dobytek	k1gInSc7	dobytek
a	a	k8xC	a
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
změnila	změnit	k5eAaPmAgFnS	změnit
turecká	turecký	k2eAgFnSc1d1	turecká
expanze	expanze	k1gFnSc1	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
(	(	kIx(	(
<g/>
Mohács	Mohács	k1gInSc1	Mohács
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
Turci	Turek	k1gMnPc1	Turek
brzy	brzy	k6eAd1	brzy
opanovali	opanovat	k5eAaPmAgMnP	opanovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Budín	Budín	k1gMnSc1	Budín
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
a	a	k8xC	a
půl	půl	k6eAd1	půl
století	století	k1gNnSc1	století
z	z	k7c2	z
metropole	metropol	k1gFnSc2	metropol
uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
stal	stát	k5eAaPmAgInS	stát
provinčním	provinční	k2eAgNnSc7d1	provinční
městem	město	k1gNnSc7	město
-	-	kIx~	-
sídlem	sídlo	k1gNnSc7	sídlo
budínského	budínský	k2eAgInSc2d1	budínský
pašaliku	pašalik	k1gInSc2	pašalik
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
kostelů	kostel	k1gInPc2	kostel
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
v	v	k7c4	v
mešity	mešita	k1gFnPc4	mešita
či	či	k8xC	či
zbořena	zbořen	k2eAgNnPc4d1	zbořeno
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
německých	německý	k2eAgMnPc2d1	německý
a	a	k8xC	a
maďarských	maďarský	k2eAgMnPc2d1	maďarský
měšťanů	měšťan	k1gMnPc2	měšťan
odešel	odejít	k5eAaPmAgInS	odejít
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
hojně	hojně	k6eAd1	hojně
usazovali	usazovat	k5eAaImAgMnP	usazovat
Srbové	Srb	k1gMnPc1	Srb
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
-	-	kIx~	-
porobené	porobený	k2eAgInPc1d1	porobený
národy	národ	k1gInPc1	národ
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
Srbů	Srb	k1gMnPc2	Srb
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Taban	Tabany	k1gInPc2	Tabany
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
srbskou	srbský	k2eAgFnSc4d1	Srbská
minulost	minulost	k1gFnSc4	minulost
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
Szentendre	Szentendr	k1gInSc5	Szentendr
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
připomíná	připomínat	k5eAaImIp3nS	připomínat
řada	řada	k1gFnSc1	řada
pravoslavných	pravoslavný	k2eAgInPc2d1	pravoslavný
kostelů	kostel	k1gInPc2	kostel
či	či	k8xC	či
místní	místní	k2eAgInPc1d1	místní
názvy	název	k1gInPc1	název
začínající	začínající	k2eAgInPc1d1	začínající
na	na	k7c4	na
Rác	Rác	k?	Rác
(	(	kIx(	(
<g/>
Rác	Rác	k?	Rác
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgNnSc4d1	staré
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
označení	označení	k1gNnSc4	označení
Srbů	Srb	k1gMnPc2	Srb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
Turcích	turek	k1gInPc6	turek
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
stopy	stopa	k1gFnPc4	stopa
-	-	kIx~	-
muslimským	muslimský	k2eAgNnSc7d1	muslimské
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
derviše	derviš	k1gMnSc2	derviš
Güla	Gülus	k1gMnSc2	Gülus
Báby	bába	k1gFnSc2	bába
(	(	kIx(	(
<g/>
Gül	Gül	k1gFnSc1	Gül
Baba	baba	k1gFnSc1	baba
türbe	türbat	k5eAaPmIp3nS	türbat
<g/>
)	)	kIx)	)
ze	z	k7c2	z
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Rózsadomb	Rózsadomba	k1gFnPc2	Rózsadomba
severně	severně	k6eAd1	severně
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
lázeňství	lázeňství	k1gNnSc1	lázeňství
se	se	k3xPyFc4	se
protklo	protknout	k5eAaPmAgNnS	protknout
s	s	k7c7	s
osmanským	osmanský	k2eAgMnSc7d1	osmanský
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
nemalého	malý	k2eNgInSc2d1	nemalý
počtu	počet	k1gInSc2	počet
koupelí	koupel	k1gFnPc2	koupel
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
dodnes	dodnes	k6eAd1	dodnes
fungující	fungující	k2eAgFnPc1d1	fungující
lázně	lázeň	k1gFnPc1	lázeň
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ještě	ještě	k9	ještě
z	z	k7c2	z
časů	čas	k1gInPc2	čas
tureckého	turecký	k2eAgNnSc2d1	turecké
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
byl	být	k5eAaImAgInS	být
Budín	Budín	k1gInSc1	Budín
po	po	k7c6	po
těžkém	těžký	k2eAgNnSc6d1	těžké
šestitýdenním	šestitýdenní	k2eAgNnSc6d1	šestitýdenní
obléhání	obléhání	k1gNnSc6	obléhání
dobyt	dobyt	k2eAgInSc1d1	dobyt
habsburským	habsburský	k2eAgNnSc7d1	habsburské
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
Uher	Uhry	k1gFnPc2	Uhry
tak	tak	k9	tak
vysvobozena	vysvobozen	k2eAgFnSc1d1	vysvobozena
z	z	k7c2	z
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
z	z	k7c2	z
válek	válka	k1gFnPc2	válka
brzy	brzy	k6eAd1	brzy
vzpamatovalo	vzpamatovat	k5eAaPmAgNnS	vzpamatovat
a	a	k8xC	a
opět	opět	k6eAd1	opět
začalo	začít	k5eAaPmAgNnS	začít
růst	růst	k5eAaImF	růst
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Budína	Budín	k1gInSc2	Budín
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Bratislavy	Bratislava	k1gFnSc2	Bratislava
přeneseny	přenést	k5eAaPmNgInP	přenést
hlavní	hlavní	k2eAgInPc1d1	hlavní
uherské	uherský	k2eAgInPc1d1	uherský
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měla	mít	k5eAaImAgFnS	mít
tři	tři	k4xCgNnPc4	tři
města	město	k1gNnPc4	město
dohromady	dohromady	k6eAd1	dohromady
kolem	kolem	k7c2	kolem
40	[number]	k4	40
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
postrádala	postrádat	k5eAaImAgFnS	postrádat
větší	veliký	k2eAgInSc4d2	veliký
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
modernizaci	modernizace	k1gFnSc6	modernizace
Pešti	Pešť	k1gFnSc2	Pešť
a	a	k8xC	a
Budína	Budín	k1gInSc2	Budín
měl	mít	k5eAaImAgMnS	mít
velkou	velký	k2eAgFnSc4d1	velká
zásluhu	zásluha	k1gFnSc4	zásluha
hrabě	hrabě	k1gMnSc1	hrabě
István	István	k2eAgMnSc1d1	István
Széchenyi	Szécheny	k1gFnPc4	Szécheny
-	-	kIx~	-
kupříkladu	kupříkladu	k6eAd1	kupříkladu
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
postavení	postavení	k1gNnSc4	postavení
prvního	první	k4xOgInSc2	první
(	(	kIx(	(
<g/>
řetězového	řetězový	k2eAgInSc2d1	řetězový
<g/>
)	)	kIx)	)
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
maďarské	maďarský	k2eAgFnSc2d1	maďarská
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
<g/>
/	/	kIx~	/
<g/>
1849	[number]	k4	1849
byla	být	k5eAaImAgFnS	být
tři	tři	k4xCgNnPc4	tři
města	město	k1gNnPc4	město
nakrátko	nakrátko	k6eAd1	nakrátko
sjednocena	sjednocen	k2eAgNnPc4d1	sjednoceno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
povstání	povstání	k1gNnSc2	povstání
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
nařízení	nařízení	k1gNnSc1	nařízení
odvoláno	odvolat	k5eAaPmNgNnS	odvolat
<g/>
,	,	kIx,	,
ba	ba	k9	ba
co	co	k9	co
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
na	na	k7c6	na
Gellértově	Gellértův	k2eAgInSc6d1	Gellértův
vrchu	vrch	k1gInSc6	vrch
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
mohutná	mohutný	k2eAgFnSc1d1	mohutná
citadela	citadela	k1gFnSc1	citadela
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
děla	dělo	k1gNnPc1	dělo
měla	mít	k5eAaImAgNnP	mít
zastrašit	zastrašit	k5eAaPmF	zastrašit
vzpurné	vzpurný	k2eAgMnPc4d1	vzpurný
Maďary	Maďar	k1gMnPc4	Maďar
<g/>
.	.	kIx.	.
</s>
<s>
Maďaři	Maďar	k1gMnPc1	Maďar
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
rakousko-maďarském	rakouskoaďarský	k2eAgNnSc6d1	rakousko-maďarský
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
požadovali	požadovat	k5eAaImAgMnP	požadovat
demolici	demolice	k1gFnSc4	demolice
Citadely	citadela	k1gFnSc2	citadela
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
rakouská	rakouský	k2eAgFnSc1d1	rakouská
posádka	posádka	k1gFnSc1	posádka
opustila	opustit	k5eAaPmAgFnS	opustit
pevnost	pevnost	k1gFnSc4	pevnost
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
symbolicky	symbolicky	k6eAd1	symbolicky
poškozena	poškodit	k5eAaPmNgFnS	poškodit
zeď	zeď	k1gFnSc1	zeď
nad	nad	k7c7	nad
hlavní	hlavní	k2eAgFnSc7d1	hlavní
bránou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
vybráním	vybrání	k1gNnSc7	vybrání
zdiva	zdivo	k1gNnSc2	zdivo
do	do	k7c2	do
písmene	písmeno	k1gNnSc2	písmeno
V.	V.	kA	V.
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
existence	existence	k1gFnSc1	existence
maďarského	maďarský	k2eAgInSc2d1	maďarský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nic	nic	k3yNnSc1	nic
nebránilo	bránit	k5eNaImAgNnS	bránit
sjednocení	sjednocení	k1gNnSc3	sjednocení
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pešť	Pešť	k1gFnSc1	Pešť
<g/>
,	,	kIx,	,
Budín	Budín	k1gInSc1	Budín
a	a	k8xC	a
Starý	starý	k2eAgInSc4d1	starý
Budín	Budín	k1gInSc4	Budín
byly	být	k5eAaImAgInP	být
sloučeny	sloučen	k2eAgInPc1d1	sloučen
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
Pešť	Pešť	k1gFnSc1	Pešť
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
evropské	evropský	k2eAgNnSc4d1	Evropské
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
soupeřící	soupeřící	k2eAgNnSc4d1	soupeřící
s	s	k7c7	s
Vídní	Vídeň	k1gFnSc7	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
široké	široký	k2eAgInPc1d1	široký
bulváry	bulvár	k1gInPc1	bulvár
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Nagykörút	Nagykörút	k1gMnSc1	Nagykörút
<g/>
,	,	kIx,	,
Sugár	Sugár	k1gMnSc1	Sugár
út	út	k?	út
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Andrássy	Andrássa	k1gFnPc1	Andrássa
út	út	k?	út
<g/>
))	))	k?	))
a	a	k8xC	a
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hősök	Hősök	k1gInSc1	Hősök
tere	tere	k1gInSc1	tere
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významné	významný	k2eAgFnPc4d1	významná
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
předměstí	předměstí	k1gNnPc1	předměstí
se	s	k7c7	s
zelenými	zelený	k2eAgInPc7d1	zelený
parky	park	k1gInPc7	park
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Városliget	Városliget	k1gInSc1	Városliget
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkorysá	velkorysý	k2eAgFnSc1d1	velkorysá
výstavba	výstavba	k1gFnSc1	výstavba
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
oslavami	oslava	k1gFnPc7	oslava
tisícího	tisící	k4xOgNnSc2	tisící
výročí	výročí	k1gNnSc2	výročí
"	"	kIx"	"
<g/>
záboru	zábor	k1gInSc2	zábor
země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pešti	Pešť	k1gFnSc2	Pešť
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Maďarů	Maďar	k1gMnPc2	Maďar
tvořili	tvořit	k5eAaImAgMnP	tvořit
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
byl	být	k5eAaImAgInS	být
i	i	k9	i
příliv	příliv	k1gInSc1	příliv
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
příslušníků	příslušník	k1gMnPc2	příslušník
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přibývaly	přibývat	k5eAaImAgFnP	přibývat
nové	nový	k2eAgFnPc1d1	nová
továrny	továrna	k1gFnPc1	továrna
a	a	k8xC	a
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
se	se	k3xPyFc4	se
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
<g/>
;	;	kIx,	;
Budapešť	Budapešť	k1gFnSc1	Budapešť
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
rozpad	rozpad	k1gInSc1	rozpad
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc2	Rakousko-Uhersek
znamenaly	znamenat	k5eAaImAgFnP	znamenat
těžký	těžký	k2eAgInSc4d1	těžký
otřes	otřes	k1gInSc4	otřes
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
moci	moct	k5eAaImF	moct
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nakrátko	nakrátko	k6eAd1	nakrátko
chopili	chopit	k5eAaPmAgMnP	chopit
komunisté	komunista	k1gMnPc1	komunista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
z	z	k7c2	z
trosek	troska	k1gFnPc2	troska
Uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rozloze	rozloha	k1gFnSc6	rozloha
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
zabíralo	zabírat	k5eAaImAgNnS	zabírat
třetinu	třetina	k1gFnSc4	třetina
rozlohy	rozloha	k1gFnSc2	rozloha
z	z	k7c2	z
mnohonárodnostního	mnohonárodnostní	k2eAgInSc2d1	mnohonárodnostní
uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
jako	jako	k8xS	jako
metropole	metropole	k1gFnSc1	metropole
velkého	velký	k2eAgInSc2d1	velký
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
jevila	jevit	k5eAaImAgFnS	jevit
poněkud	poněkud	k6eAd1	poněkud
zbytnělou	zbytnělý	k2eAgFnSc7d1	zbytnělá
hlavou	hlava	k1gFnSc7	hlava
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
rozvoj	rozvoj	k1gInSc4	rozvoj
Budapešti	Budapešť	k1gFnSc2	Budapešť
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Přibývala	přibývat	k5eAaImAgFnS	přibývat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
předměstí	předměstí	k1gNnSc6	předměstí
běžných	běžný	k2eAgInPc2d1	běžný
činžovních	činžovní	k2eAgInPc2d1	činžovní
domů	dům	k1gInPc2	dům
<g/>
;	;	kIx,	;
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
hodnotná	hodnotný	k2eAgNnPc1d1	hodnotné
urbanistická	urbanistický	k2eAgNnPc1d1	Urbanistické
řešení	řešení	k1gNnPc1	řešení
<g/>
,	,	kIx,	,
jako	jako	k9	jako
Wekerleho	Wekerle	k1gMnSc2	Wekerle
sídliště	sídliště	k1gNnSc2	sídliště
(	(	kIx(	(
<g/>
Wekerletelep	Wekerletelep	k1gInSc1	Wekerletelep
<g/>
)	)	kIx)	)
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
překročil	překročit	k5eAaPmAgInS	překročit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
hranici	hranice	k1gFnSc6	hranice
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zanechala	zanechat	k5eAaPmAgFnS	zanechat
obrovské	obrovský	k2eAgFnPc4d1	obrovská
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zdejších	zdejší	k2eAgMnPc2d1	zdejší
Židů	Žid	k1gMnPc2	Žid
byla	být	k5eAaImAgFnS	být
odvlečena	odvléct	k5eAaPmNgFnS	odvléct
do	do	k7c2	do
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
(	(	kIx(	(
<g/>
navzdory	navzdory	k7c3	navzdory
úsilí	úsilí	k1gNnSc3	úsilí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
záchranu	záchrana	k1gFnSc4	záchrana
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
švédský	švédský	k2eAgMnSc1d1	švédský
diplomat	diplomat	k1gMnSc1	diplomat
Raoul	Raoula	k1gFnPc2	Raoula
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
maďar	maďar	k1gInSc1	maďar
-	-	kIx~	-
Miklós	Miklós	k1gInSc1	Miklós
Horthy	Hortha	k1gFnSc2	Hortha
-	-	kIx~	-
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
,	,	kIx,	,
agresor	agresor	k1gMnSc1	agresor
<g/>
,	,	kIx,	,
Hitlerův	Hitlerův	k2eAgMnSc1d1	Hitlerův
komplic	komplic	k1gMnSc1	komplic
<g/>
.	.	kIx.	.
</s>
<s>
Výčet	výčet	k1gInSc1	výčet
jeho	jeho	k3xOp3gInPc2	jeho
pochybných	pochybný	k2eAgInPc2d1	pochybný
kroků	krok	k1gInPc2	krok
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednu	jeden	k4xCgFnSc4	jeden
zásluhu	zásluha	k1gFnSc4	zásluha
mu	on	k3xPp3gMnSc3	on
upřít	upřít	k5eAaPmF	upřít
nelze	lze	k6eNd1	lze
<g/>
:	:	kIx,	:
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
Židy	Žid	k1gMnPc4	Žid
chránit	chránit	k5eAaImF	chránit
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
brzdit	brzdit	k5eAaImF	brzdit
jejich	jejich	k3xOp3gFnSc4	jejich
perzekuci	perzekuce	k1gFnSc4	perzekuce
<g/>
.	.	kIx.	.
<g/>
Obrat	obrat	k1gInSc1	obrat
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
se	se	k3xPyFc4	se
Horthy	Horth	k1gMnPc7	Horth
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vzepřít	vzepřít	k5eAaPmF	vzepřít
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
a	a	k8xC	a
transporty	transporta	k1gFnPc1	transporta
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
"	"	kIx"	"
<g/>
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
<g/>
"	"	kIx"	"
osobně	osobně	k6eAd1	osobně
Adolf	Adolf	k1gMnSc1	Adolf
Eichmann	Eichmann	k1gMnSc1	Eichmann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
odesláno	odeslat	k5eAaPmNgNnS	odeslat
<g/>
"	"	kIx"	"
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
a	a	k8xC	a
vyhlazovacího	vyhlazovací	k2eAgInSc2d1	vyhlazovací
tábora	tábor	k1gInSc2	tábor
Auschwitz	Auschwitz	k1gInSc1	Auschwitz
Birkenau	Birkenaus	k1gInSc2	Birkenaus
(	(	kIx(	(
<g/>
Oswietim	Oswietim	k1gInSc1	Oswietim
a	a	k8xC	a
Březinka	březinka	k1gFnSc1	březinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
438	[number]	k4	438
000	[number]	k4	000
maďarských	maďarský	k2eAgMnPc2d1	maďarský
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
zpátky	zpátky	k6eAd1	zpátky
jen	jen	k9	jen
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
ustupující	ustupující	k2eAgFnSc1d1	ustupující
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
vyhodila	vyhodit	k5eAaPmAgFnS	vyhodit
do	do	k7c2	do
povětří	povětří	k1gNnSc6	povětří
všechny	všechen	k3xTgInPc4	všechen
mosty	most	k1gInPc4	most
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
a	a	k8xC	a
následujících	následující	k2eAgInPc2d1	následující
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
zuřivě	zuřivě	k6eAd1	zuřivě
bránila	bránit	k5eAaImAgFnS	bránit
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Dunaje	Dunaj	k1gInSc2	Dunaj
-	-	kIx~	-
Hradní	hradní	k2eAgInSc1d1	hradní
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Citadela	citadela	k1gFnSc1	citadela
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
Hradní	hradní	k2eAgFnSc1d1	hradní
čtvrť	čtvrť	k1gFnSc1	čtvrť
byla	být	k5eAaImAgFnS	být
dělostřeleckou	dělostřelecký	k2eAgFnSc7d1	dělostřelecká
palbou	palba	k1gFnSc7	palba
téměř	téměř	k6eAd1	téměř
srovnána	srovnán	k2eAgFnSc1d1	srovnána
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
byly	být	k5eAaImAgFnP	být
nejhorší	zlý	k2eAgFnPc1d3	nejhorší
rány	rána	k1gFnPc1	rána
zaceleny	zacelen	k2eAgFnPc1d1	zacelen
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
například	například	k6eAd1	například
obnova	obnova	k1gFnSc1	obnova
mostů	most	k1gInPc2	most
byla	být	k5eAaImAgFnS	být
časově	časově	k6eAd1	časově
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
finančně	finančně	k6eAd1	finančně
velmi	velmi	k6eAd1	velmi
nákladná	nákladný	k2eAgFnSc1d1	nákladná
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
od	od	k7c2	od
nacismu	nacismus	k1gInSc2	nacismus
a	a	k8xC	a
fašismu	fašismus	k1gInSc2	fašismus
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
dodnes	dodnes	k6eAd1	dodnes
platné	platný	k2eAgNnSc1d1	platné
správní	správní	k2eAgNnSc1d1	správní
členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1956	[number]	k4	1956
se	se	k3xPyFc4	se
právě	právě	k9	právě
Budapešť	Budapešť	k1gFnSc1	Budapešť
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c6	o
odstranění	odstranění	k1gNnSc6	odstranění
komunistické	komunistický	k2eAgFnSc2d1	komunistická
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgNnSc1d1	lidové
povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
krvavě	krvavě	k6eAd1	krvavě
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
sovětskými	sovětský	k2eAgInPc7d1	sovětský
tanky	tank	k1gInPc7	tank
<g/>
.	.	kIx.	.
</s>
<s>
Konsolidovaný	konsolidovaný	k2eAgInSc1d1	konsolidovaný
režim	režim	k1gInSc1	režim
Jánose	Jánosa	k1gFnSc6	Jánosa
Kádára	Kádár	k1gMnSc2	Kádár
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
podnikl	podniknout	k5eAaPmAgInS	podniknout
určité	určitý	k2eAgInPc4d1	určitý
liberální	liberální	k2eAgInPc4d1	liberální
ústupky	ústupek	k1gInPc4	ústupek
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
gulášový	gulášový	k2eAgInSc1d1	gulášový
socialismus	socialismus	k1gInSc1	socialismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Budapešť	Budapešť	k1gFnSc1	Budapešť
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
pomalu	pomalu	k6eAd1	pomalu
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
éru	éra	k1gFnSc4	éra
definitivně	definitivně	k6eAd1	definitivně
ukončily	ukončit	k5eAaPmAgFnP	ukončit
svobodné	svobodný	k2eAgFnPc1d1	svobodná
volby	volba	k1gFnPc1	volba
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
správního	správní	k2eAgNnSc2d1	správní
je	být	k5eAaImIp3nS	být
Budapešť	Budapešť	k1gFnSc1	Budapešť
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
23	[number]	k4	23
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
kerületek	kerületka	k1gFnPc2	kerületka
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc4d1	jednotné
číslo	číslo	k1gNnSc4	číslo
kerület	kerület	k1gInSc4	kerület
<g/>
)	)	kIx)	)
označených	označený	k2eAgFnPc2d1	označená
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Číslování	číslování	k1gNnSc1	číslování
jde	jít	k5eAaImIp3nS	jít
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
od	od	k7c2	od
středu	střed	k1gInSc2	střed
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
22	[number]	k4	22
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ještě	ještě	k9	ještě
obvod	obvod	k1gInSc1	obvod
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
vyčleněním	vyčlenění	k1gNnSc7	vyčlenění
z	z	k7c2	z
obvodu	obvod	k1gInSc2	obvod
XX	XX	kA	XX
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
főváros	fővárosa	k1gFnPc2	fővárosa
<g/>
)	)	kIx)	)
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
40	[number]	k4	40
územněsprávních	územněsprávní	k2eAgFnPc2d1	územněsprávní
částí	část	k1gFnPc2	část
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
19	[number]	k4	19
žup	župa	k1gFnPc2	župa
+	+	kIx~	+
20	[number]	k4	20
statutárních	statutární	k2eAgNnPc2d1	statutární
měst	město	k1gNnPc2	město
+	+	kIx~	+
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Primátorem	primátor	k1gMnSc7	primátor
Budapešti	Budapešť	k1gFnSc2	Budapešť
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
István	István	k2eAgInSc1d1	István
Tarlós	Tarlós	k1gInSc1	Tarlós
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
dopravní	dopravní	k2eAgFnSc7d1	dopravní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
břehy	břeh	k1gInPc1	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
zde	zde	k6eAd1	zde
spojuje	spojovat	k5eAaImIp3nS	spojovat
9	[number]	k4	9
silničních	silniční	k2eAgMnPc2d1	silniční
a	a	k8xC	a
2	[number]	k4	2
železniční	železniční	k2eAgInPc1d1	železniční
mosty	most	k1gInPc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
a	a	k8xC	a
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Széchényiho	Széchényiha	k1gFnSc5	Széchényiha
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Széchenyi	Széchenyi	k1gNnSc1	Széchenyi
Lánchíd	Lánchída	k1gFnPc2	Lánchída
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
<g/>
..	..	k?	..
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
mosty	most	k1gInPc4	most
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
Margaretin	Margaretin	k2eAgInSc1d1	Margaretin
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
Alžbětin	Alžbětin	k2eAgInSc1d1	Alžbětin
most	most	k1gInSc1	most
a	a	k8xC	a
Most	most	k1gInSc1	most
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
autobusové	autobusový	k2eAgNnSc1d1	autobusové
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
Stadiónok	Stadiónok	k1gInSc4	Stadiónok
<g/>
;	;	kIx,	;
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
Budapešti	Budapešť	k1gFnSc2	Budapešť
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
terminálů	terminál	k1gInPc2	terminál
pro	pro	k7c4	pro
regionální	regionální	k2eAgFnSc4d1	regionální
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
je	být	k5eAaImIp3nS	být
dálniční	dálniční	k2eAgInSc4d1	dálniční
okruh	okruh	k1gInSc4	okruh
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
<g/>
%	%	kIx~	%
okruhu	okruh	k1gInSc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
se	se	k3xPyFc4	se
paprskovitě	paprskovitě	k6eAd1	paprskovitě
rozbíhají	rozbíhat	k5eAaImIp3nP	rozbíhat
hlavní	hlavní	k2eAgInPc4d1	hlavní
silniční	silniční	k2eAgInPc4d1	silniční
i	i	k8xC	i
dálniční	dálniční	k2eAgInPc4d1	dálniční
tahy	tah	k1gInPc4	tah
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
dálnice	dálnice	k1gFnSc2	dálnice
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
Tatabánya	Tatabánya	k1gFnSc1	Tatabánya
-	-	kIx~	-
Győr	Győr	k1gInSc1	Győr
-	-	kIx~	-
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
;	;	kIx,	;
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
Dálnice	dálnice	k1gFnSc2	dálnice
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
Hatvan	Hatvan	k1gMnSc1	Hatvan
-	-	kIx~	-
Gyöngyös	Gyöngyös	k1gInSc1	Gyöngyös
<g/>
;	;	kIx,	;
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
dálnice	dálnice	k1gFnSc2	dálnice
M	M	kA	M
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
Kecskemét	Kecskemét	k1gInSc1	Kecskemét
-	-	kIx~	-
Segedín	Segedín	k1gInSc1	Segedín
<g/>
;	;	kIx,	;
na	na	k7c4	na
jih	jih	k1gInSc4	jih
dálnice	dálnice	k1gFnSc2	dálnice
M	M	kA	M
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
Dunaújváros	Dunaújvárosa	k1gFnPc2	Dunaújvárosa
-	-	kIx~	-
Pécs	Pécs	k1gInSc1	Pécs
<g/>
;	;	kIx,	;
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
dálnice	dálnice	k1gFnSc2	dálnice
M	M	kA	M
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
Balaton	Balaton	k1gInSc1	Balaton
-	-	kIx~	-
Nagykanizsa	Nagykanizsa	k1gFnSc1	Nagykanizsa
-Záhřeb	-Záhřba	k1gFnPc2	-Záhřba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parčíku	parčík	k1gInSc6	parčík
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
předpolí	předpolí	k1gNnSc6	předpolí
Řetězového	řetězový	k2eAgInSc2d1	řetězový
mostu	most	k1gInSc2	most
(	(	kIx(	(
<g/>
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Clark	Clark	k1gInSc1	Clark
Adám	Adám	k1gMnSc1	Adám
tér	tér	k1gInSc1	tér
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
kuriózní	kuriózní	k2eAgInSc4d1	kuriózní
pomník	pomník	k1gInSc4	pomník
-	-	kIx~	-
symbolický	symbolický	k2eAgInSc4d1	symbolický
"	"	kIx"	"
<g/>
nultý	nultý	k4xOgInSc4	nultý
<g/>
"	"	kIx"	"
kilometr	kilometr	k1gInSc4	kilometr
maďarské	maďarský	k2eAgFnSc2d1	maďarská
silniční	silniční	k2eAgFnSc2d1	silniční
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
Vác	Vác	k1gFnSc1	Vác
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
je	být	k5eAaImIp3nS	být
sedm	sedm	k4xCc1	sedm
větších	veliký	k2eAgNnPc2d2	veliký
nádraží	nádraží	k1gNnPc2	nádraží
a	a	k8xC	a
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
vlakových	vlakový	k2eAgFnPc2d1	vlaková
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgFnPc4	každý
z	z	k7c2	z
trojice	trojice	k1gFnSc2	trojice
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
nádraží	nádraží	k1gNnPc2	nádraží
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
podle	podle	k7c2	podle
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
vypravována	vypravován	k2eAgFnSc1d1	vypravována
většina	většina	k1gFnSc1	většina
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Nyugati	Nyugat	k1gMnPc1	Nyugat
(	(	kIx(	(
<g/>
Západní	západní	k2eAgMnPc1d1	západní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Keleti	Kelet	k1gMnPc1	Kelet
(	(	kIx(	(
<g/>
Východní	východní	k2eAgInPc1d1	východní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Déli	dél	k1gFnSc6wB	dél
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příměstská	příměstský	k2eAgFnSc1d1	příměstská
železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
HÉV	HÉV	kA	HÉV
<g/>
)	)	kIx)	)
spojuje	spojovat	k5eAaImIp3nS	spojovat
Budapešť	Budapešť	k1gFnSc1	Budapešť
s	s	k7c7	s
několika	několik	k4yIc7	několik
blízkými	blízký	k2eAgNnPc7d1	blízké
městy	město	k1gNnPc7	město
a	a	k8xC	a
okrajovými	okrajový	k2eAgFnPc7d1	okrajová
částmi	část	k1gFnPc7	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Szentendre	Szentendr	k1gInSc5	Szentendr
<g/>
,	,	kIx,	,
Gödöllő	Gödöllő	k1gFnPc2	Gödöllő
<g/>
,	,	kIx,	,
Csepel	Csepela	k1gFnPc2	Csepela
a	a	k8xC	a
Ráckeve	Ráckeev	k1gFnSc2	Ráckeev
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
říčním	říční	k2eAgInSc7d1	říční
přístavem	přístav	k1gInSc7	přístav
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
,	,	kIx,	,
spojujícím	spojující	k2eAgInSc6d1	spojující
střední	střední	k2eAgFnPc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
s	s	k7c7	s
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nákladní	nákladní	k2eAgNnSc1d1	nákladní
přístaviště	přístaviště	k1gNnSc1	přístaviště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Csepel	Csepela	k1gFnPc2	Csepela
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnPc1d1	osobní
lodě	loď	k1gFnPc1	loď
kotví	kotvit	k5eAaImIp3nP	kotvit
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Pešti	Pešť	k1gFnSc2	Pešť
-	-	kIx~	-
"	"	kIx"	"
<g/>
rakety	raketa	k1gFnPc1	raketa
<g/>
"	"	kIx"	"
spojující	spojující	k2eAgFnSc1d1	spojující
Budapešť	Budapešť	k1gFnSc1	Budapešť
s	s	k7c7	s
Vídní	Vídeň	k1gFnSc7	Vídeň
u	u	k7c2	u
Bělehradského	bělehradský	k2eAgNnSc2d1	Bělehradské
nábřeží	nábřeží	k1gNnSc2	nábřeží
(	(	kIx(	(
<g/>
Belgrád	Belgráda	k1gFnPc2	Belgráda
rakpart	rakpart	k1gInSc1	rakpart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc1	loď
do	do	k7c2	do
Visegrádu	Visegrád	k1gInSc2	Visegrád
a	a	k8xC	a
Ostřihomi	Ostřiho	k1gFnPc7	Ostřiho
(	(	kIx(	(
<g/>
Esztergom	Esztergom	k1gInSc1	Esztergom
<g/>
)	)	kIx)	)
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
u	u	k7c2	u
Reduty	reduta	k1gFnSc2	reduta
(	(	kIx(	(
<g/>
Vigadó	Vigadó	k1gMnSc1	Vigadó
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Ference	Ferenc	k1gMnSc2	Ferenc
Liszta	Liszta	k1gFnSc1	Liszta
Budapešť	Budapešť	k1gFnSc1	Budapešť
(	(	kIx(	(
<g/>
Budapest	Budapest	k1gMnSc1	Budapest
Liszt	Liszt	k1gMnSc1	Liszt
Ferenc	Ferenc	k1gMnSc1	Ferenc
Nemzetközi	Nemzetköh	k1gMnPc1	Nemzetköh
Repülőtér	Repülőtér	k1gMnSc1	Repülőtér
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
situováno	situovat	k5eAaBmNgNnS	situovat
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
terminály	terminála	k1gFnPc4	terminála
<g/>
:	:	kIx,	:
Ferihegy	Feriheg	k1gInPc7	Feriheg
1	[number]	k4	1
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ferihegy	Feriheg	k1gInPc1	Feriheg
2A	[number]	k4	2A
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
lety	let	k1gInPc7	let
domácí	domácí	k2eAgFnSc2d1	domácí
společnosti	společnost	k1gFnSc2	společnost
Malév	Maléva	k1gFnPc2	Maléva
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ferihegy	Feriheg	k1gInPc1	Feriheg
2B	[number]	k4	2B
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
aerolinií	aerolinie	k1gFnPc2	aerolinie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
ročně	ročně	k6eAd1	ročně
odbaví	odbavit	k5eAaPmIp3nS	odbavit
přes	přes	k7c4	přes
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
má	mít	k5eAaImIp3nS	mít
hustou	hustý	k2eAgFnSc4d1	hustá
a	a	k8xC	a
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
síť	síť	k1gFnSc4	síť
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
jsou	být	k5eAaImIp3nP	být
rozmanité	rozmanitý	k2eAgNnSc4d1	rozmanité
<g/>
:	:	kIx,	:
Metro	metro	k1gNnSc4	metro
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
sestává	sestávat	k5eAaImIp3nS	sestávat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
a	a	k8xC	a
páteřní	páteřní	k2eAgFnSc7d1	páteřní
sítí	síť	k1gFnSc7	síť
celé	celý	k2eAgFnSc2d1	celá
budapeštské	budapeštský	k2eAgFnSc2d1	budapeštský
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc1	čtyři
linky	linka	k1gFnPc1	linka
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
hovoru	hovor	k1gInSc6	hovor
nazývány	nazývat	k5eAaImNgFnP	nazývat
spíše	spíše	k9	spíše
svými	svůj	k3xOyFgFnPc7	svůj
barvami	barva	k1gFnPc7	barva
-	-	kIx~	-
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
linka	linka	k1gFnSc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
linka	linka	k1gFnSc1	linka
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc1	druhý
nejstarší	starý	k2eAgNnSc1d3	nejstarší
metro	metro	k1gNnSc1	metro
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
první	první	k4xOgNnPc1	první
metro	metro	k1gNnSc1	metro
bylo	být	k5eAaImAgNnS	být
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
-	-	kIx~	-
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
-	-	kIx~	-
M1	M1	k1gFnSc1	M1
žlutá	žlutý	k2eAgFnSc1d1	žlutá
-	-	kIx~	-
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Földalatti	Földalatti	k1gNnSc1	Földalatti
Vasút	Vasúta	k1gFnPc2	Vasúta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podzemní	podzemní	k2eAgFnSc1d1	podzemní
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
linky	linka	k1gFnPc1	linka
nesou	nést	k5eAaImIp3nP	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
METRO	metro	k1gNnSc1	metro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
M1	M1	k1gFnSc1	M1
-	-	kIx~	-
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
staré	starý	k2eAgInPc4d1	starý
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
vozy	vůz	k1gInPc4	vůz
žlutě	žlutě	k6eAd1	žlutě
natřené	natřený	k2eAgInPc1d1	natřený
<g/>
,	,	kIx,	,
koleje	kolej	k1gFnPc1	kolej
jsou	být	k5eAaImIp3nP	být
uložené	uložený	k2eAgInPc1d1	uložený
nízko	nízko	k6eAd1	nízko
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
cca	cca	kA	cca
do	do	k7c2	do
5	[number]	k4	5
<g/>
m	m	kA	m
<g/>
;	;	kIx,	;
až	až	k6eAd1	až
se	se	k3xPyFc4	se
třese	třást	k5eAaImIp3nS	třást
zem	zem	k1gFnSc1	zem
<g/>
,	,	kIx,	,
když	když	k8xS	když
projíždí	projíždět	k5eAaImIp3nS	projíždět
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
pak	pak	k6eAd1	pak
v	v	k7c6	v
kopcovitých	kopcovitý	k2eAgFnPc6d1	kopcovitá
čtvrtích	čtvrt	k1gFnPc6	čtvrt
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pešti	Pešť	k1gFnSc6	Pešť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
též	též	k9	též
15	[number]	k4	15
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
výletní	výletní	k2eAgFnSc7d1	výletní
atrakcí	atrakce	k1gFnSc7	atrakce
v	v	k7c6	v
Budínských	Budínských	k2eAgInPc6d1	Budínských
vrších	vrch	k1gInPc6	vrch
je	být	k5eAaImIp3nS	být
ozubnicová	ozubnicový	k2eAgFnSc1d1	ozubnicová
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
Fogaskerekű	Fogaskerekű	k1gMnSc1	Fogaskerekű
vasút	vasút	k1gMnSc1	vasút
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dětská	dětský	k2eAgFnSc1d1	dětská
železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
Gyermekvasút	Gyermekvasút	k1gInSc1	Gyermekvasút
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
systému	systém	k1gInSc2	systém
MHD	MHD	kA	MHD
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
pozemní	pozemní	k2eAgFnSc1d1	pozemní
lanovka	lanovka	k1gFnSc1	lanovka
z	z	k7c2	z
Budínského	Budínského	k2eAgNnSc2d1	Budínského
předmostí	předmostí	k1gNnSc2	předmostí
Széchényiho	Széchényi	k1gMnSc2	Széchényi
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
Clark	Clark	k1gInSc1	Clark
Adám	Adám	k1gInSc1	Adám
tér	tér	k1gInSc4	tér
na	na	k7c4	na
budínský	budínský	k2eAgInSc4d1	budínský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
také	také	k9	také
výborně	výborně	k6eAd1	výborně
funguje	fungovat	k5eAaImIp3nS	fungovat
systém	systém	k1gInSc1	systém
sdílení	sdílení	k1gNnSc2	sdílení
městský	městský	k2eAgInSc1d1	městský
kol	kol	k7c2	kol
-	-	kIx~	-
BuBi	BuB	k1gFnSc2	BuB
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
zájemci	zájemce	k1gMnPc1	zájemce
vypůčit	vypůčit	k5eAaImF	vypůčit
na	na	k7c4	na
1	[number]	k4	1
150	[number]	k4	150
kol	kolo	k1gNnPc2	kolo
ze	z	k7c2	z
99	[number]	k4	99
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
kolo	kolo	k1gNnSc1	kolo
váží	vážit	k5eAaImIp3nS	vážit
25	[number]	k4	25
kg	kg	kA	kg
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
malým	malý	k2eAgInSc7d1	malý
košem	koš	k1gInSc7	koš
na	na	k7c4	na
tašku	taška	k1gFnSc4	taška
a	a	k8xC	a
zámkem	zámek	k1gInSc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
vychází	vycházet	k5eAaImIp3nS	vycházet
vstříc	vstříc	k6eAd1	vstříc
také	také	k9	také
turistům	turist	k1gMnPc3	turist
a	a	k8xC	a
občasným	občasný	k2eAgMnPc3d1	občasný
zájemcům	zájemce	k1gMnPc3	zájemce
-	-	kIx~	-
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
vypůjčit	vypůjčit	k5eAaPmF	vypůjčit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
stanovišti	stanoviště	k1gNnSc6	stanoviště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
kreditní	kreditní	k2eAgFnSc4d1	kreditní
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tří	tři	k4xCgFnPc2	tři
desítek	desítka	k1gFnPc2	desítka
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Univerzita	univerzita	k1gFnSc1	univerzita
Loránda	Loránda	k1gFnSc1	Loránda
Eötvöse	Eötvöse	k1gFnSc1	Eötvöse
(	(	kIx(	(
<g/>
Eötvös	Eötvös	k1gInSc1	Eötvös
Loránd	Loránd	k1gInSc1	Loránd
Tudományegyetem	Tudományegye	k1gNnSc7	Tudományegye
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ELTE	ELTE	kA	ELTE
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
Trnavě	Trnava	k1gFnSc6	Trnava
roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
a	a	k8xC	a
do	do	k7c2	do
nynějšího	nynější	k2eAgNnSc2d1	nynější
působiště	působiště	k1gNnSc2	působiště
přenesená	přenesený	k2eAgFnSc1d1	přenesená
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
a	a	k8xC	a
Katolická	katolický	k2eAgFnSc1d1	katolická
univerzita	univerzita	k1gFnSc1	univerzita
Petra	Petra	k1gFnSc1	Petra
Pázmánye	Pázmánye	k1gFnSc1	Pázmánye
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vyčleněním	vyčlenění	k1gNnSc7	vyčlenění
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
Loránda	Loránd	k1gMnSc2	Loránd
Eötvöse	Eötvös	k1gMnSc2	Eötvös
<g/>
.	.	kIx.	.
</s>
<s>
Cikánská	cikánský	k2eAgFnSc1d1	cikánská
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
pikantní	pikantní	k2eAgNnPc1d1	pikantní
jídla	jídlo	k1gNnPc1	jídlo
<g/>
,	,	kIx,	,
čardáš	čardáš	k1gInSc1	čardáš
a	a	k8xC	a
tokajské	tokajský	k2eAgNnSc1d1	tokajské
víno	víno	k1gNnSc1	víno
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
cestují	cestovat	k5eAaImIp3nP	cestovat
obvykle	obvykle	k6eAd1	obvykle
návštěvníci	návštěvník	k1gMnPc1	návštěvník
do	do	k7c2	do
maďarské	maďarský	k2eAgFnSc2d1	maďarská
metropole	metropol	k1gFnSc2	metropol
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
šedesát	šedesát	k4xCc4	šedesát
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
opery	opera	k1gFnPc4	opera
<g/>
,	,	kIx,	,
čtyřicet	čtyřicet	k4xCc4	čtyřicet
sedm	sedm	k4xCc4	sedm
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
šestnáct	šestnáct	k4xCc1	šestnáct
koncertních	koncertní	k2eAgFnPc2d1	koncertní
síní	síň	k1gFnPc2	síň
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
amfiteátrů	amfiteátr	k1gInPc2	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Pravému	pravý	k2eAgNnSc3d1	pravé
<g/>
,	,	kIx,	,
budínskému	budínský	k2eAgNnSc3d1	budínský
<g/>
,	,	kIx,	,
břehu	břeh	k1gInSc6	břeh
dominuje	dominovat	k5eAaImIp3nS	dominovat
(	(	kIx(	(
<g/>
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
<g/>
)	)	kIx)	)
vrch	vrch	k1gInSc1	vrch
Gellért	Gellért	k1gInSc1	Gellért
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
,	,	kIx,	,
citadelou	citadela	k1gFnSc7	citadela
a	a	k8xC	a
sochou	socha	k1gFnSc7	socha
sv.	sv.	kA	sv.
Gellérta	Gellért	k1gMnSc2	Gellért
v	v	k7c6	v
úbočí	úbočí	k1gNnSc6	úbočí
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hradní	hradní	k2eAgInSc1d1	hradní
vrch	vrch	k1gInSc1	vrch
Várhegy	Várhega	k1gFnSc2	Várhega
s	s	k7c7	s
mohutným	mohutný	k2eAgInSc7d1	mohutný
královským	královský	k2eAgInSc7d1	královský
palácem	palác	k1gInSc7	palác
obehnaným	obehnaný	k2eAgInSc7d1	obehnaný
kamennou	kamenný	k2eAgFnSc7d1	kamenná
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
obloze	obloha	k1gFnSc3	obloha
tyčí	tyč	k1gFnPc2	tyč
věže	věž	k1gFnSc2	věž
Rybářské	rybářský	k2eAgFnSc2d1	rybářská
bašty	bašta	k1gFnSc2	bašta
a	a	k8xC	a
Matyášova	Matyášův	k2eAgInSc2d1	Matyášův
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgNnSc1d1	doplněné
novodobým	novodobý	k2eAgInSc7d1	novodobý
hotelem	hotel	k1gInSc7	hotel
Hilton	Hilton	k1gInSc1	Hilton
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgInPc1d1	pozoruhodný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
kostely	kostel	k1gInPc1	kostel
pod	pod	k7c7	pod
Hradním	hradní	k2eAgInSc7d1	hradní
vrchem	vrch	k1gInSc7	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadnější	nápadní	k2eAgFnSc7d3	nápadní
stavbou	stavba	k1gFnSc7	stavba
levého	levý	k2eAgMnSc2d1	levý
<g/>
,	,	kIx,	,
pešťského	pešťský	k2eAgMnSc2d1	pešťský
<g/>
,	,	kIx,	,
břehu	břeh	k1gInSc2	břeh
je	být	k5eAaImIp3nS	být
neogotická	ogotický	k2eNgFnSc1d1	neogotická
budova	budova	k1gFnSc1	budova
Parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Országház	Országháza	k1gFnPc2	Országháza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostavěná	dostavěný	k2eAgFnSc1d1	dostavěná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
268	[number]	k4	268
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
96	[number]	k4	96
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
700	[number]	k4	700
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
výzdobě	výzdoba	k1gFnSc6	výzdoba
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
umělci	umělec	k1gMnPc1	umělec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
vládních	vládní	k2eAgFnPc2d1	vládní
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
zástavby	zástavba	k1gFnSc2	zástavba
vysoko	vysoko	k6eAd1	vysoko
ční	čnět	k5eAaImIp3nS	čnět
kopule	kopule	k1gFnSc1	kopule
Baziliky	bazilika	k1gFnSc2	bazilika
svatého	svatý	k2eAgMnSc4d1	svatý
Štěpána	Štěpán	k1gMnSc4	Štěpán
-	-	kIx~	-
kostela	kostel	k1gInSc2	kostel
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
prvnímu	první	k4xOgNnSc3	první
maďarskému	maďarský	k2eAgMnSc3d1	maďarský
křesťanskému	křesťanský	k2eAgMnSc3d1	křesťanský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
tisíciletého	tisíciletý	k2eAgNnSc2d1	tisícileté
výročí	výročí	k1gNnSc2	výročí
země	země	k1gFnSc1	země
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Pešti	Pešť	k1gFnSc2	Pešť
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
náměstí	náměstí	k1gNnSc1	náměstí
Hrdinů	Hrdina	k1gMnPc2	Hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgMnSc1d1	věnovaný
nejdůležitějším	důležitý	k2eAgFnPc3d3	nejdůležitější
postavám	postava	k1gFnPc3	postava
maďarských	maďarský	k2eAgFnPc2d1	maďarská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Váci	Váci	k6eAd1	Váci
utca	utc	k2eAgFnSc1d1	utc
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
ulic	ulice	k1gFnPc2	ulice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
rušnou	rušný	k2eAgFnSc4d1	rušná
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ukázkou	ukázka	k1gFnSc7	ukázka
měnících	měnící	k2eAgInPc2d1	měnící
se	se	k3xPyFc4	se
architektonických	architektonický	k2eAgInPc2d1	architektonický
stylů	styl	k1gInPc2	styl
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
také	také	k9	také
významným	významný	k2eAgNnSc7d1	významné
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
trávení	trávení	k1gNnSc4	trávení
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nP	sídlet
např.	např.	kA	např.
módní	módní	k2eAgInPc4d1	módní
obchody	obchod	k1gInPc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
budapešťským	budapešťský	k2eAgInSc7d1	budapešťský
bulvárem	bulvár	k1gInSc7	bulvár
je	být	k5eAaImIp3nS	být
Andrássyho	Andrássy	k1gMnSc4	Andrássy
třída	třída	k1gFnSc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
nepřijdou	přijít	k5eNaPmIp3nP	přijít
zkrátka	zkrátka	k6eAd1	zkrátka
ani	ani	k8xC	ani
milovníci	milovník	k1gMnPc1	milovník
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
;	;	kIx,	;
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
parků	park	k1gInPc2	park
a	a	k8xC	a
lesů	les	k1gInPc2	les
<g/>
:	:	kIx,	:
Markétin	Markétin	k2eAgInSc1d1	Markétin
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Margitsziget	Margitsziget	k1gInSc1	Margitsziget
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
celý	celý	k2eAgInSc1d1	celý
pruh	pruh	k1gInSc1	pruh
země	zem	k1gFnSc2	zem
ležící	ležící	k2eAgFnSc2d1	ležící
uprostřed	uprostřed	k7c2	uprostřed
Dunaje	Dunaj	k1gInSc2	Dunaj
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
velkým	velký	k2eAgInSc7d1	velký
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
soch	socha	k1gFnPc2	socha
(	(	kIx(	(
<g/>
Szoborpark	Szoborpark	k1gInSc1	Szoborpark
<g/>
)	)	kIx)	)
-	-	kIx~	-
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgFnP	být
přesunuty	přesunut	k2eAgFnPc1d1	přesunuta
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
monumenty	monument	k1gInPc1	monument
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sochy	socha	k1gFnPc1	socha
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
Marxe	Marx	k1gMnSc4	Marx
<g/>
,	,	kIx,	,
Engelse	Engels	k1gMnSc4	Engels
<g/>
,	,	kIx,	,
Lenina	Lenin	k1gMnSc4	Lenin
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
maďarské	maďarský	k2eAgMnPc4d1	maďarský
následovníky	následovník	k1gMnPc4	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
Népliget	Népliget	k1gInSc1	Népliget
(	(	kIx(	(
<g/>
Lidový	lidový	k2eAgInSc1d1	lidový
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
-	-	kIx~	-
největší	veliký	k2eAgFnPc1d3	veliký
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
Budapešťských	budapešťský	k2eAgInPc2d1	budapešťský
parků	park	k1gInPc2	park
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
i	i	k9	i
planetáriem	planetárium	k1gNnSc7	planetárium
<g/>
.	.	kIx.	.
</s>
<s>
Orlí	orlí	k2eAgInSc4d1	orlí
vrch	vrch	k1gInSc4	vrch
-	-	kIx~	-
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
vzácných	vzácný	k2eAgFnPc2d1	vzácná
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
výskytem	výskyt	k1gInSc7	výskyt
krátkonožky	krátkonožka	k1gFnSc2	krátkonožka
evropské	evropský	k2eAgFnSc2d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
Nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
Vidám	Vidám	k?	Vidám
park	park	k1gInSc1	park
a	a	k8xC	a
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Městském	městský	k2eAgInSc6d1	městský
sadu	sad	k1gInSc6	sad
<g/>
.	.	kIx.	.
</s>
<s>
Városliget	Városliget	k1gInSc1	Városliget
-	-	kIx~	-
Městský	městský	k2eAgInSc1d1	městský
sad	sad	k1gInSc1	sad
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
umělým	umělý	k2eAgNnSc7d1	umělé
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
hrad	hrad	k1gInSc1	hrad
kopie	kopie	k1gFnSc2	kopie
sedmihradského	sedmihradský	k2eAgInSc2d1	sedmihradský
hradu	hrad	k1gInSc2	hrad
Vajdahunyad	Vajdahunyad	k1gInSc1	Vajdahunyad
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
románská	románský	k2eAgFnSc1d1	románská
<g/>
,	,	kIx,	,
gotická	gotický	k2eAgFnSc1d1	gotická
<g/>
,	,	kIx,	,
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
zde	zde	k6eAd1	zde
i	i	k9	i
renesanci	renesance	k1gFnSc3	renesance
<g/>
,	,	kIx,	,
či	či	k8xC	či
baroko	baroko	k1gNnSc1	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
pavilon	pavilon	k1gInSc1	pavilon
nese	nést	k5eAaImIp3nS	nést
kopie	kopie	k1gFnPc4	kopie
autentických	autentický	k2eAgInPc2d1	autentický
detailů	detail	k1gInPc2	detail
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc4d1	jediné
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
stovka	stovka	k1gFnSc1	stovka
termálních	termální	k2eAgInPc2d1	termální
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
studní	studna	k1gFnPc2	studna
<g/>
.	.	kIx.	.
</s>
<s>
Metropole	metropole	k1gFnSc1	metropole
stojí	stát	k5eAaImIp3nS	stát
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
na	na	k7c6	na
bublající	bublající	k2eAgFnSc6d1	bublající
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
123	[number]	k4	123
pramenů	pramen	k1gInPc2	pramen
zde	zde	k6eAd1	zde
denně	denně	k6eAd1	denně
vychrlí	vychrlit	k5eAaPmIp3nS	vychrlit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
litrů	litr	k1gInPc2	litr
vody	voda	k1gFnSc2	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
26-76	[number]	k4	26-76
°	°	k?	°
<g/>
C.	C.	kA	C.
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
pramenů	pramen	k1gInPc2	pramen
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
každé	každý	k3xTgFnPc1	každý
lázně	lázeň	k1gFnPc1	lázeň
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgFnPc1d1	královská
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc1	lázeň
Raitzen	Raitzna	k1gFnPc2	Raitzna
a	a	k8xC	a
Rudas	Rudasa	k1gFnPc2	Rudasa
jsou	být	k5eAaImIp3nP	být
provedeny	provést	k5eAaPmNgInP	provést
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tureckém	turecký	k2eAgInSc6d1	turecký
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
lázně	lázeň	k1gFnPc1	lázeň
Széchenyi	Széchenyi	k1gNnSc2	Széchenyi
byly	být	k5eAaImAgFnP	být
zbudovány	zbudovat	k5eAaPmNgFnP	zbudovat
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
neoklasicistním	neoklasicistní	k2eAgInSc6d1	neoklasicistní
<g/>
.	.	kIx.	.
</s>
