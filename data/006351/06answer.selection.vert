<s>
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Filippo	Filippa	k1gFnSc5	Filippa
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
též	též	k9	též
Nolan	Nolan	k1gMnSc1	Nolan
nebo	nebo	k8xC	nebo
Nolanus	Nolanus	k1gMnSc1	Nolanus
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1548	[number]	k4	1548
<g/>
,	,	kIx,	,
Nola	Nola	k1gFnSc1	Nola
u	u	k7c2	u
Neapole	Neapol	k1gFnSc2	Neapol
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
dominikánský	dominikánský	k2eAgMnSc1d1	dominikánský
mnich	mnich	k1gMnSc1	mnich
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
(	(	kIx(	(
<g/>
představitel	představitel	k1gMnSc1	představitel
renesančního	renesanční	k2eAgInSc2d1	renesanční
hermetismu	hermetismus	k1gInSc2	hermetismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
komediograf	komediograf	k1gMnSc1	komediograf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teoretický	teoretický	k2eAgMnSc1d1	teoretický
kosmolog	kosmolog	k1gMnSc1	kosmolog
<g/>
,	,	kIx,	,
zabýval	zabývat	k5eAaImAgMnS	zabývat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
mnemotechnikou	mnemotechnika	k1gFnSc7	mnemotechnika
<g/>
.	.	kIx.	.
</s>
