<s>
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Filippo	Filippa	k1gFnSc5	Filippa
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
též	též	k9	též
Nolan	Nolan	k1gMnSc1	Nolan
nebo	nebo	k8xC	nebo
Nolanus	Nolanus	k1gMnSc1	Nolanus
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1548	[number]	k4	1548
<g/>
,	,	kIx,	,
Nola	Nola	k1gFnSc1	Nola
u	u	k7c2	u
Neapole	Neapol	k1gFnSc2	Neapol
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
dominikánský	dominikánský	k2eAgMnSc1d1	dominikánský
mnich	mnich	k1gMnSc1	mnich
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
(	(	kIx(	(
<g/>
představitel	představitel	k1gMnSc1	představitel
renesančního	renesanční	k2eAgInSc2d1	renesanční
hermetismu	hermetismus	k1gInSc2	hermetismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
komediograf	komediograf	k1gMnSc1	komediograf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teoretický	teoretický	k2eAgMnSc1d1	teoretický
kosmolog	kosmolog	k1gMnSc1	kosmolog
<g/>
,	,	kIx,	,
zabýval	zabývat	k5eAaImAgMnS	zabývat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
mnemotechnikou	mnemotechnika	k1gFnSc7	mnemotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivněn	ovlivněn	k2eAgMnSc1d1	ovlivněn
byl	být	k5eAaImAgMnS	být
Mikulášem	mikuláš	k1gInSc7	mikuláš
Kusánským	Kusánský	k2eAgInSc7d1	Kusánský
a	a	k8xC	a
Bernardem	Bernard	k1gMnSc7	Bernard
Telesiem	Telesius	k1gMnSc7	Telesius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
tezemi	teze	k1gFnPc7	teze
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
ani	ani	k8xC	ani
Slunce	slunce	k1gNnSc1	slunce
nejsou	být	k5eNaImIp3nP	být
středem	středem	k7c2	středem
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1593	[number]	k4	1593
byly	být	k5eAaImAgFnP	být
některé	některý	k3yIgInPc1	některý
jeho	jeho	k3xOp3gInPc1	jeho
názory	názor	k1gInPc1	názor
šetřeny	šetřen	k2eAgInPc1d1	šetřen
jako	jako	k8xC	jako
hereze	hereze	k1gFnSc1	hereze
římskou	římska	k1gFnSc7	římska
inkvizicí	inkvizice	k1gFnSc7	inkvizice
–	–	k?	–
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
popírání	popírání	k1gNnSc2	popírání
několika	několik	k4yIc2	několik
katolikých	katoliký	k2eAgFnPc2d1	katoliký
doktrín	doktrína	k1gFnPc2	doktrína
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
věčného	věčný	k2eAgNnSc2d1	věčné
zatracení	zatracení	k1gNnSc2	zatracení
<g/>
,	,	kIx,	,
boží	boží	k2eAgFnSc2d1	boží
trojjedinosti	trojjedinost	k1gFnSc2	trojjedinost
<g/>
,	,	kIx,	,
božské	božský	k2eAgFnSc2d1	božská
podstaty	podstata	k1gFnSc2	podstata
Kristovy	Kristův	k2eAgFnPc1d1	Kristova
<g/>
,	,	kIx,	,
panenství	panenství	k1gNnSc1	panenství
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
transubstanciace	transubstanciace	k1gFnSc2	transubstanciace
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeho	jeho	k3xOp3gInSc1	jeho
panteismus	panteismus	k1gInSc1	panteismus
vzbuzoval	vzbuzovat	k5eAaImAgInS	vzbuzovat
vážné	vážný	k2eAgFnPc4d1	vážná
obavy	obava	k1gFnPc4	obava
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
ho	on	k3xPp3gMnSc4	on
shledala	shledat	k5eAaPmAgFnS	shledat
vinným	vinný	k1gMnPc3	vinný
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
upálen	upálit	k5eAaPmNgMnS	upálit
v	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
Campo	Campa	k1gFnSc5	Campa
dei	dei	k?	dei
Fiori	Fiori	k1gNnPc1	Fiori
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
násilného	násilný	k2eAgInSc2d1	násilný
útlaku	útlak	k1gInSc2	útlak
svobodného	svobodný	k2eAgNnSc2d1	svobodné
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
idolů	idol	k1gInPc2	idol
volnomyšlenkářů	volnomyšlenkář	k1gMnPc2	volnomyšlenkář
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
jeho	jeho	k3xOp3gInSc4	jeho
rozsudek	rozsudek	k1gInSc4	rozsudek
za	za	k7c4	za
teologické	teologický	k2eAgFnPc4d1	teologická
hereze	hereze	k1gFnPc4	hereze
a	a	k8xC	a
filosofické	filosofický	k2eAgInPc4d1	filosofický
náhledy	náhled	k1gInPc4	náhled
vztahovat	vztahovat	k5eAaImF	vztahovat
i	i	k9	i
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
kosmologické	kosmologický	k2eAgFnPc4d1	kosmologická
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Historička	historička	k1gFnSc1	historička
Frances	Francesa	k1gFnPc2	Francesa
Yatesová	Yatesový	k2eAgFnSc1d1	Yatesová
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
spíše	spíše	k9	spíše
za	za	k7c4	za
obhajobu	obhajoba	k1gFnSc4	obhajoba
hermetické	hermetický	k2eAgFnSc2d1	hermetická
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
hluboce	hluboko	k6eAd1	hluboko
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
arabskou	arabský	k2eAgFnSc7d1	arabská
astrologií	astrologie	k1gFnSc7	astrologie
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
Averroesovou	Averroesový	k2eAgFnSc7d1	Averroesový
filosofií	filosofie	k1gFnSc7	filosofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neoplatonismem	Neoplatonismus	k1gInSc7	Neoplatonismus
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc7d1	renesanční
hermeneutikou	hermeneutika	k1gFnSc7	hermeneutika
a	a	k8xC	a
legendami	legenda	k1gFnPc7	legenda
opřádajícími	opřádající	k2eAgInPc7d1	opřádající
egyptského	egyptský	k2eAgMnSc4d1	egyptský
boha	bůh	k1gMnSc4	bůh
Thovta	Thovt	k1gMnSc4	Thovt
<g/>
.	.	kIx.	.
</s>
<s>
Teologická	teologický	k2eAgFnSc1d1	teologická
heterodoxnost	heterodoxnost	k1gFnSc1	heterodoxnost
vedla	vést	k5eAaImAgFnS	vést
Bruna	Bruno	k1gMnSc4	Bruno
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
úlohy	úloha	k1gFnSc2	úloha
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
hodí	hodit	k5eAaImIp3nS	hodit
jen	jen	k9	jen
k	k	k7c3	k
uspokojení	uspokojení	k1gNnSc3	uspokojení
nevzdělanců	nevzdělanec	k1gMnPc2	nevzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Nola	Nolus	k1gMnSc2	Nolus
nedaleko	nedaleko	k7c2	nedaleko
Neapole	Neapol	k1gFnSc2	Neapol
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Filippo	Filippa	k1gFnSc5	Filippa
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
zvolil	zvolit	k5eAaPmAgMnS	zvolit
jméno	jméno	k1gNnSc4	jméno
Nolanus	Nolanus	k1gInSc1	Nolanus
nebo	nebo	k8xC	nebo
Nolan	Nolan	k1gInSc1	Nolan
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
svého	svůj	k3xOyFgNnSc2	svůj
narození	narození	k1gNnSc2	narození
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
i	i	k9	i
svoji	svůj	k3xOyFgFnSc4	svůj
filozofii	filozofie	k1gFnSc4	filozofie
nazýval	nazývat	k5eAaImAgMnS	nazývat
nolanistickou	nolanistický	k2eAgFnSc7d1	nolanistický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
jako	jako	k9	jako
patnáctiletý	patnáctiletý	k2eAgMnSc1d1	patnáctiletý
do	do	k7c2	do
dominikánského	dominikánský	k2eAgInSc2d1	dominikánský
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1565	[number]	k4	1565
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
dominikánského	dominikánský	k2eAgInSc2d1	dominikánský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijal	přijmout	k5eAaPmAgMnS	přijmout
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
Giordano	Giordana	k1gFnSc5	Giordana
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
kacířství	kacířství	k1gNnSc2	kacířství
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1576	[number]	k4	1576
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
Neapole	Neapol	k1gFnSc2	Neapol
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
přes	přes	k7c4	přes
Chambéry	Chambéra	k1gFnPc4	Chambéra
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
začal	začít	k5eAaPmAgInS	začít
jeho	jeho	k3xOp3gNnSc4	jeho
neustálý	neustálý	k2eAgInSc1d1	neustálý
život	život	k1gInSc1	život
štvance	štvanec	k1gMnSc2	štvanec
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
své	svůj	k3xOyFgFnSc3	svůj
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
míst	místo	k1gNnPc2	místo
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
byl	být	k5eAaImAgMnS	být
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ženevy	Ženeva	k1gFnSc2	Ženeva
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Toulouse	Toulouse	k1gInSc2	Toulouse
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odsud	odsud	k6eAd1	odsud
musel	muset	k5eAaImAgMnS	muset
kvůli	kvůli	k7c3	kvůli
hugenotským	hugenotský	k2eAgFnPc3d1	hugenotská
válkám	válka	k1gFnPc3	válka
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
však	však	k9	však
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
útokům	útok	k1gInPc3	útok
proti	proti	k7c3	proti
aristotelovské	aristotelovský	k2eAgFnSc3d1	aristotelovská
filozofii	filozofie	k1gFnSc3	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
nejšťastnější	šťastný	k2eAgNnPc4d3	nejšťastnější
léta	léto	k1gNnPc4	léto
pak	pak	k6eAd1	pak
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1583	[number]	k4	1583
až	až	k9	až
1585	[number]	k4	1585
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
francouzského	francouzský	k2eAgMnSc2d1	francouzský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
Michela	Michel	k1gMnSc2	Michel
de	de	k?	de
Castelnau	Castelnaa	k1gFnSc4	Castelnaa
kde	kde	k6eAd1	kde
mj.	mj.	kA	mj.
roku	rok	k1gInSc2	rok
1584	[number]	k4	1584
vydal	vydat	k5eAaPmAgInS	vydat
významné	významný	k2eAgInPc4d1	významný
spisy	spis	k1gInPc4	spis
La	la	k1gNnSc2	la
Cena	cena	k1gFnSc1	cena
de	de	k?	de
le	le	k?	le
Ceneri	Cener	k1gFnSc2	Cener
(	(	kIx(	(
<g/>
Večeře	večeře	k1gFnSc1	večeře
na	na	k7c4	na
popeleční	popeleční	k2eAgFnSc4d1	popeleční
středu	středa	k1gFnSc4	středa
<g/>
)	)	kIx)	)
a	a	k8xC	a
De	De	k?	De
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
infinito	infinit	k2eAgNnSc4d1	infinito
<g/>
,	,	kIx,	,
universo	universa	k1gFnSc5	universa
e	e	k0	e
mondi	mond	k1gMnPc1	mond
(	(	kIx(	(
<g/>
O	o	k7c6	o
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
,	,	kIx,	,
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
světech	svět	k1gInPc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
korigoval	korigovat	k5eAaBmAgInS	korigovat
Koperníkův	Koperníkův	k2eAgInSc1d1	Koperníkův
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
a	a	k8xC	a
vyložil	vyložit	k5eAaPmAgMnS	vyložit
svoji	svůj	k3xOyFgFnSc4	svůj
kosmologickou	kosmologický	k2eAgFnSc4d1	kosmologická
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
nekonečnosti	nekonečnost	k1gFnSc6	nekonečnost
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1585	[number]	k4	1585
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
vyslancem	vyslanec	k1gMnSc7	vyslanec
de	de	k?	de
Castelnauem	Castelnauem	k1gInSc1	Castelnauem
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
musel	muset	k5eAaImAgMnS	muset
rovněž	rovněž	k9	rovněž
opustit	opustit	k5eAaPmF	opustit
kvůli	kvůli	k7c3	kvůli
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
aristotelovskou	aristotelovský	k2eAgFnSc4d1	aristotelovská
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
univerzitních	univerzitní	k2eAgNnPc6d1	univerzitní
městech	město	k1gNnPc6	město
Marburgu	Marburg	k1gInSc2	Marburg
a	a	k8xC	a
Wittenbergu	Wittenberg	k1gInSc2	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1588	[number]	k4	1588
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
u	u	k7c2	u
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Helmstedtu	Helmstedt	k1gInSc2	Helmstedt
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
dostal	dostat	k5eAaPmAgMnS	dostat
katedru	katedra	k1gFnSc4	katedra
matematiky	matematika	k1gFnSc2	matematika
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
brzo	brzo	k6eAd1	brzo
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Galileem	Galileus	k1gMnSc7	Galileus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1591	[number]	k4	1591
<g/>
,	,	kIx,	,
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
pozvání	pozvání	k1gNnSc4	pozvání
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Moceniga	Mocenig	k1gMnSc2	Mocenig
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ztratil	ztratit	k5eAaPmAgMnS	ztratit
možnost	možnost	k1gFnSc4	možnost
získat	získat	k5eAaPmF	získat
natrvalo	natrvalo	k6eAd1	natrvalo
katedru	katedra	k1gFnSc4	katedra
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
,	,	kIx,	,
pozvání	pozvánět	k5eAaImIp3nS	pozvánět
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1592	[number]	k4	1592
však	však	k9	však
byl	být	k5eAaImAgMnS	být
svým	svůj	k3xOyFgMnSc7	svůj
žákem	žák	k1gMnSc7	žák
udán	udat	k5eAaPmNgMnS	udat
<g/>
,	,	kIx,	,
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
inkvizicí	inkvizice	k1gFnPc2	inkvizice
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1593	[number]	k4	1593
vydán	vydat	k5eAaPmNgInS	vydat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
důvodech	důvod	k1gInPc6	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
vlastně	vlastně	k9	vlastně
Mocenigo	Mocenigo	k1gMnSc1	Mocenigo
Bruna	Bruna	k1gMnSc1	Bruna
udal	udat	k5eAaPmAgMnS	udat
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
agentem	agent	k1gMnSc7	agent
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bruna	Bruna	k1gMnSc1	Bruna
podezříval	podezřívat	k5eAaImAgMnS	podezřívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nechce	chtít	k5eNaImIp3nS	chtít
prozradit	prozradit	k5eAaPmF	prozradit
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ví	vědět	k5eAaImIp3nS	vědět
z	z	k7c2	z
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
atd.	atd.	kA	atd.
Rovněž	rovněž	k9	rovněž
jeho	on	k3xPp3gNnSc2	on
vydání	vydání	k1gNnSc2	vydání
Benátkami	Benátky	k1gFnPc7	Benátky
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
nebyla	být	k5eNaImAgFnS	být
zcela	zcela	k6eAd1	zcela
samozřejmá	samozřejmý	k2eAgFnSc1d1	samozřejmá
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Benátky	Benátky	k1gFnPc1	Benátky
si	se	k3xPyFc3	se
zakládaly	zakládat	k5eAaImAgFnP	zakládat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
;	;	kIx,	;
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
případě	případ	k1gInSc6	případ
však	však	k9	však
Řím	Řím	k1gInSc1	Řím
mohl	moct	k5eAaImAgInS	moct
svou	svůj	k3xOyFgFnSc4	svůj
žádost	žádost	k1gFnSc4	žádost
opřít	opřít	k5eAaPmF	opřít
o	o	k7c4	o
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
vydávání	vydávání	k1gNnSc4	vydávání
uprchlých	uprchlý	k2eAgMnPc2d1	uprchlý
mnichů	mnich	k1gMnPc2	mnich
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
vedení	vedení	k1gNnSc1	vedení
republiky	republika	k1gFnSc2	republika
snažilo	snažit	k5eAaImAgNnS	snažit
o	o	k7c4	o
vstřícnost	vstřícnost	k1gFnSc4	vstřícnost
vůči	vůči	k7c3	vůči
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
se	se	k3xPyFc4	se
tak	tak	k9	tak
vlastně	vlastně	k9	vlastně
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
"	"	kIx"	"
<g/>
vysoké	vysoký	k2eAgFnSc2d1	vysoká
<g/>
"	"	kIx"	"
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
souzen	souzen	k2eAgMnSc1d1	souzen
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
protikatolické	protikatolický	k2eAgFnPc4d1	protikatolická
a	a	k8xC	a
rouhačské	rouhačský	k2eAgFnPc4d1	rouhačská
myšlenky	myšlenka	k1gFnPc4	myšlenka
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
mučen	mučen	k2eAgMnSc1d1	mučen
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tvrdošíjně	tvrdošíjně	k6eAd1	tvrdošíjně
odmítal	odmítat	k5eAaImAgMnS	odmítat
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
upálením	upálení	k1gNnSc7	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Robert	Robert	k1gMnSc1	Robert
Bellarmino	Bellarmin	k2eAgNnSc4d1	Bellarmino
definoval	definovat	k5eAaBmAgInS	definovat
osm	osm	k4xCc4	osm
tvrzení	tvrzení	k1gNnPc2	tvrzení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
měl	mít	k5eAaImAgMnS	mít
Bruno	Bruno	k1gMnSc1	Bruno
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
Bruno	Bruno	k1gMnSc1	Bruno
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objasnil	objasnit	k5eAaPmAgMnS	objasnit
příčiny	příčina	k1gFnPc4	příčina
pohybu	pohyb	k1gInSc2	pohyb
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
nehybnosti	nehybnost	k1gFnSc2	nehybnost
oblohy	obloha	k1gFnSc2	obloha
jistými	jistý	k2eAgInPc7d1	jistý
důvody	důvod	k1gInPc7	důvod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
prý	prý	k9	prý
neodporují	odporovat	k5eNaImIp3nP	odporovat
božskému	božský	k2eAgNnSc3d1	božské
Písmu	písmo	k1gNnSc3	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Marně	marně	k6eAd1	marně
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
předloženy	předložen	k2eAgFnPc1d1	předložena
verše	verš	k1gInSc2	verš
Eklesiasty	Eklesiast	k1gInPc1	Eklesiast
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Terra	Terra	k1gFnSc1	Terra
autem	aut	k1gInSc7	aut
in	in	k?	in
aeternum	aeternum	k1gInSc1	aeternum
stat	stat	k1gInSc1	stat
<g/>
;	;	kIx,	;
Sol	sol	k1gInSc1	sol
oritur	oritura	k1gFnPc2	oritura
<g/>
,	,	kIx,	,
et	et	k?	et
occidit	occidita	k1gFnPc2	occidita
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
ale	ale	k8xC	ale
Země	zem	k1gFnPc1	zem
na	na	k7c4	na
věky	věk	k1gInPc4	věk
stojí	stát	k5eAaImIp3nS	stát
<g/>
;	;	kIx,	;
vychází	vycházet	k5eAaImIp3nS	vycházet
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
i	i	k9	i
zapadá	zapadat	k5eAaPmIp3nS	zapadat
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Písmo	písmo	k1gNnSc1	písmo
svaté	svatá	k1gFnSc2	svatá
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
věřícím	věřící	k2eAgInSc7d1	věřící
<g/>
,	,	kIx,	,
a	a	k8xC	a
neobrací	obracet	k5eNaImIp3nS	obracet
se	se	k3xPyFc4	se
k	k	k7c3	k
vědcům	vědec	k1gMnPc3	vědec
jako	jako	k8xC	jako
takovým	takový	k3xDgMnPc3	takový
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tentýž	týž	k3xTgInSc4	týž
citát	citát	k1gInSc4	citát
z	z	k7c2	z
bible	bible	k1gFnSc2	bible
předložil	předložit	k5eAaPmAgMnS	předložit
později	pozdě	k6eAd2	pozdě
kardinál	kardinál	k1gMnSc1	kardinál
Bellarmino	Bellarmin	k2eAgNnSc4d1	Bellarmino
rovněž	rovněž	k9	rovněž
Galileimu	Galileim	k1gInSc3	Galileim
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dalším	další	k2eAgInSc7d1	další
<g />
.	.	kIx.	.
</s>
<s>
závadným	závadný	k2eAgNnSc7d1	závadné
tvrzením	tvrzení	k1gNnSc7	tvrzení
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bruno	Bruno	k1gMnSc1	Bruno
kladl	klást	k5eAaImAgMnS	klást
proti	proti	k7c3	proti
ideji	idea	k1gFnSc3	idea
stvoření	stvoření	k1gNnSc2	stvoření
svou	svůj	k3xOyFgFnSc4	svůj
doktrínu	doktrína	k1gFnSc4	doktrína
nekonečného	konečný	k2eNgInSc2d1	nekonečný
a	a	k8xC	a
věčného	věčný	k2eAgInSc2d1	věčný
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
složeného	složený	k2eAgMnSc2d1	složený
z	z	k7c2	z
nesčetných	sčetný	k2eNgInPc2d1	nesčetný
světů	svět	k1gInPc2	svět
<g/>
...	...	k?	...
Profesor	profesor	k1gMnSc1	profesor
Tretera	Treter	k1gMnSc2	Treter
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
Existuje	existovat	k5eAaImIp3nS	existovat
<g/>
-li	i	k?	-li
více	hodně	k6eAd2	hodně
slunečních	sluneční	k2eAgFnPc2d1	sluneční
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
planet	planeta	k1gFnPc2	planeta
sourodých	sourodý	k2eAgInPc2d1	sourodý
s	s	k7c7	s
naší	náš	k3xOp1gFnSc7	náš
planetou	planeta	k1gFnSc7	planeta
a	a	k8xC	a
obydlených	obydlený	k2eAgFnPc2d1	obydlená
rozumnými	rozumný	k2eAgFnPc7d1	rozumná
bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potom	potom	k6eAd1	potom
s	s	k7c7	s
prvotním	prvotní	k2eAgInSc7d1	prvotní
hříchem	hřích	k1gInSc7	hřích
<g/>
,	,	kIx,	,
s	s	k7c7	s
Kristovým	Kristův	k2eAgNnSc7d1	Kristovo
vykupitelstvím	vykupitelství	k1gNnSc7	vykupitelství
<g/>
?	?	kIx.	?
</s>
<s>
Takto	takto	k6eAd1	takto
jsme	být	k5eAaImIp1nP	být
na	na	k7c6	na
stopě	stopa	k1gFnSc6	stopa
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
ragovala	ragovat	k5eAaBmAgFnS	ragovat
na	na	k7c4	na
Brunovo	Brunův	k2eAgNnSc4d1	Brunovo
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vědecky	vědecky	k6eAd1	vědecky
odtažité	odtažitý	k2eAgNnSc4d1	odtažité
vystoupení	vystoupení	k1gNnSc4	vystoupení
s	s	k7c7	s
takovým	takový	k3xDgNnSc7	takový
rozčilením	rozčilení	k1gNnSc7	rozčilení
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
surovou	surový	k2eAgFnSc7d1	surová
rozhodností	rozhodnost	k1gFnSc7	rozhodnost
<g/>
.	.	kIx.	.
</s>
<s>
Viděla	vidět	k5eAaImAgFnS	vidět
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
základě	základ	k1gInSc6	základ
podkopána	podkopán	k2eAgNnPc4d1	podkopáno
dogmata	dogma	k1gNnPc4	dogma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
odvozovala	odvozovat	k5eAaImAgFnS	odvozovat
svou	svůj	k3xOyFgFnSc4	svůj
duchovní	duchovní	k2eAgFnSc4d1	duchovní
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Rozsudek	rozsudek	k1gInSc1	rozsudek
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
8	[number]	k4	8
<g/>
.	.	kIx.	.
únorem	únor	k1gInSc7	únor
1600	[number]	k4	1600
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
devíti	devět	k4xCc7	devět
kardinály	kardinál	k1gMnPc4	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rozsudku	rozsudek	k1gInSc2	rozsudek
bylo	být	k5eAaImAgNnS	být
i	i	k8xC	i
zařazení	zařazení	k1gNnSc1	zařazení
veškerých	veškerý	k3xTgInPc2	veškerý
spisů	spis	k1gInPc2	spis
Giordana	Giordan	k1gMnSc2	Giordan
Bruna	Bruno	k1gMnSc2	Bruno
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
omnia	omnium	k1gNnSc2	omnium
<g/>
)	)	kIx)	)
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
řekl	říct	k5eAaPmAgMnS	říct
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
svým	svůj	k3xOyFgMnPc3	svůj
soudcům	soudce	k1gMnPc3	soudce
<g/>
:	:	kIx,	:
Vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mě	já	k3xPp1nSc4	já
odsuzujete	odsuzovat	k5eAaImIp2nP	odsuzovat
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
máte	mít	k5eAaImIp2nP	mít
větší	veliký	k2eAgInSc4d2	veliký
strach	strach	k1gInSc4	strach
než	než	k8xS	než
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
smrt	smrt	k1gFnSc4	smrt
podstupuji	podstupovat	k5eAaImIp1nS	podstupovat
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Campo	Campa	k1gFnSc5	Campa
de	de	k?	de
<g/>
'	'	kIx"	'
Fiori	Fiori	k1gNnSc1	Fiori
(	(	kIx(	(
<g/>
Náměstí	náměstí	k1gNnSc1	náměstí
květin	květina	k1gFnPc2	květina
<g/>
)	)	kIx)	)
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
milostivého	milostivý	k2eAgNnSc2d1	milostivé
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
přihlížející	přihlížející	k2eAgMnPc1d1	přihlížející
poutníci	poutník	k1gMnPc1	poutník
se	se	k3xPyFc4	se
těšili	těšit	k5eAaImAgMnP	těšit
odpustkům	odpustek	k1gInPc3	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
byl	být	k5eAaImAgMnS	být
svlečen	svlečen	k2eAgMnSc1d1	svlečen
<g/>
,	,	kIx,	,
nahý	nahý	k2eAgMnSc1d1	nahý
připoután	připoután	k2eAgMnSc1d1	připoután
ke	k	k7c3	k
sloupu	sloup	k1gInSc3	sloup
a	a	k8xC	a
upálen	upálen	k2eAgInSc1d1	upálen
zaživa	zaživa	k6eAd1	zaživa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
se	se	k3xPyFc4	se
odvracel	odvracet	k5eAaImAgMnS	odvracet
od	od	k7c2	od
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
podáván	podáván	k2eAgInSc1d1	podáván
<g/>
.	.	kIx.	.
</s>
<s>
Popel	popel	k1gInSc1	popel
byl	být	k5eAaImAgInS	být
vhozen	vhodit	k5eAaPmNgInS	vhodit
do	do	k7c2	do
Tibery	Tibera	k1gFnSc2	Tibera
<g/>
.	.	kIx.	.
</s>
<s>
Cenné	cenný	k2eAgNnSc4d1	cenné
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
popravě	poprava	k1gFnSc6	poprava
přinesl	přinést	k5eAaPmAgInS	přinést
Kaspar	Kaspar	k1gInSc1	Kaspar
Schoppe	Schopp	k1gMnSc5	Schopp
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
podobizna	podobizna	k1gFnSc1	podobizna
Bruna	Bruno	k1gMnSc2	Bruno
je	být	k5eAaImIp3nS	být
rytina	rytina	k1gFnSc1	rytina
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1715	[number]	k4	1715
a	a	k8xC	a
citovaná	citovaný	k2eAgFnSc1d1	citovaná
Salvestrinim	Salvestrinim	k1gInSc1	Salvestrinim
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
portrét	portrét	k1gInSc1	portrét
Bruna	Bruno	k1gMnSc2	Bruno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Salvestrini	Salvestrin	k1gMnPc1	Salvestrin
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přetisk	přetisk	k1gInSc4	přetisk
ze	z	k7c2	z
ztraceného	ztracený	k2eAgInSc2d1	ztracený
originálu	originál	k1gInSc2	originál
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rytina	rytina	k1gFnSc1	rytina
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgFnPc4d2	pozdější
Brunovy	Brunův	k2eAgFnPc4d1	Brunova
podobizny	podobizna	k1gFnPc4	podobizna
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gNnSc4	jeho
uvěznění	uvěznění	k1gNnSc4	uvěznění
Benátskou	benátský	k2eAgFnSc7d1	Benátská
inkvizicí	inkvizice	k1gFnSc7	inkvizice
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1592	[number]	k4	1592
ho	on	k3xPp3gMnSc4	on
popisují	popisovat	k5eAaImIp3nP	popisovat
jako	jako	k8xC	jako
muže	muž	k1gMnSc4	muž
"	"	kIx"	"
<g/>
průměrné	průměrný	k2eAgFnSc2d1	průměrná
výšky	výška	k1gFnSc2	výška
s	s	k7c7	s
ořechově	ořechově	k6eAd1	ořechově
zbarvenými	zbarvený	k2eAgInPc7d1	zbarvený
vousy	vous	k1gInPc7	vous
a	a	k8xC	a
vzezřením	vzezření	k1gNnSc7	vzezření
kolem	kolem	k7c2	kolem
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
pasáž	pasáž	k1gFnSc1	pasáž
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
George	Georg	k1gMnSc2	Georg
Abbota	Abbot	k1gMnSc2	Abbot
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bruno	Bruno	k1gMnSc1	Bruno
byl	být	k5eAaImAgMnS	být
drobné	drobný	k2eAgFnPc4d1	drobná
postavy	postava	k1gFnPc4	postava
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
italský	italský	k2eAgMnSc1d1	italský
tulák	tulák	k1gMnSc1	tulák
(	(	kIx(	(
<g/>
v	v	k7c6	v
angl.	angl.	k?	angl.
originále	originál	k1gInSc6	originál
didapper	didappra	k1gFnPc2	didappra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
tituloval	titulovat	k5eAaImAgMnS	titulovat
Philotheus	Philotheus	k1gInSc4	Philotheus
Iordanus	Iordanus	k1gMnSc1	Iordanus
Brunus	Brunus	k1gMnSc1	Brunus
Nolanus	Nolanus	k1gMnSc1	Nolanus
<g/>
,	,	kIx,	,
magis	magis	k1gFnSc1	magis
elaborata	elaborata	k1gFnSc1	elaborata
Theologia	Theologia	k1gFnSc1	Theologia
Doctor	Doctor	k1gInSc4	Doctor
<g/>
,	,	kIx,	,
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
delším	dlouhý	k2eAgNnSc7d2	delší
než	než	k8xS	než
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
didapper	didapper	k1gInSc1	didapper
<g/>
"	"	kIx"	"
pejorativně	pejorativně	k6eAd1	pejorativně
použité	použitý	k2eAgFnPc1d1	použitá
Abbotem	Abbot	k1gInSc7	Abbot
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
výrazů	výraz	k1gInPc2	výraz
pro	pro	k7c4	pro
potápku	potápka	k1gFnSc4	potápka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
znamenat	znamenat	k5eAaImF	znamenat
něco	něco	k3yInSc4	něco
jako	jako	k9	jako
"	"	kIx"	"
<g/>
tulák	tulák	k1gMnSc1	tulák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Retrospektivní	retrospektivní	k2eAgFnSc1d1	retrospektivní
"	"	kIx"	"
<g/>
vědecká	vědecký	k2eAgFnSc1d1	vědecká
<g/>
"	"	kIx"	"
ikonografie	ikonografie	k1gFnSc1	ikonografie
Bruna	Bruno	k1gMnSc2	Bruno
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
v	v	k7c6	v
mnišské	mnišský	k2eAgFnSc6d1	mnišská
dominikánské	dominikánský	k2eAgFnSc6d1	Dominikánská
kutně	kutna	k1gFnSc6	kutna
ale	ale	k8xC	ale
bez	bez	k7c2	bez
tonzury	tonzura	k1gFnSc2	tonzura
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Gosselin	Gosselin	k2eAgMnSc1d1	Gosselin
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Bruno	Bruno	k1gMnSc1	Bruno
tonzuru	tonzura	k1gFnSc4	tonzura
ponechal	ponechat	k5eAaPmAgMnS	ponechat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
nosil	nosit	k5eAaImAgInS	nosit
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
dovedl	dovést	k5eAaPmAgMnS	dovést
Koperníkovu	Koperníkův	k2eAgFnSc4d1	Koperníkova
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
vesmír	vesmír	k1gInSc4	vesmír
ještě	ještě	k6eAd1	ještě
dál	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInPc2	jeho
názorů	názor	k1gInPc2	názor
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
byly	být	k5eAaImAgInP	být
jen	jen	k9	jen
spekulací	spekulace	k1gFnPc2	spekulace
bez	bez	k7c2	bez
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
vědeckých	vědecký	k2eAgInPc2d1	vědecký
důkazů	důkaz	k1gInPc2	důkaz
a	a	k8xC	a
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
ani	ani	k8xC	ani
Slunce	slunce	k1gNnSc1	slunce
středem	středem	k7c2	středem
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
;	;	kIx,	;
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Bruna	Bruno	k1gMnSc4	Bruno
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c4	mnoho
sluncí	slunce	k1gNnPc2	slunce
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
obydlené	obydlený	k2eAgInPc1d1	obydlený
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dělá	dělat	k5eAaImIp3nS	dělat
dobré	dobrý	k2eAgFnPc4d1	dobrá
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
nekonečně	konečně	k6eNd1	konečně
světů	svět	k1gInPc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Bůh	bůh	k1gMnSc1	bůh
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
automaticky	automaticky	k6eAd1	automaticky
nekonečně	konečně	k6eNd1	konečně
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
.	.	kIx.	.
</s>
<s>
Církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
prakticky	prakticky	k6eAd1	prakticky
znamenalo	znamenat	k5eAaImAgNnS	znamenat
i	i	k9	i
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
malost	malost	k1gFnSc4	malost
a	a	k8xC	a
zanedbatelnost	zanedbatelnost	k1gFnSc4	zanedbatelnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
tak	tak	k6eAd1	tak
předběhl	předběhnout	k5eAaPmAgMnS	předběhnout
svoji	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pohledu	pohled	k1gInSc6	pohled
o	o	k7c4	o
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
představě	představa	k1gFnSc6	představa
stavby	stavba	k1gFnSc2	stavba
vesmíru	vesmír	k1gInSc2	vesmír
sice	sice	k8xC	sice
převzal	převzít	k5eAaPmAgInS	převzít
Aristotelovu	Aristotelův	k2eAgFnSc4d1	Aristotelova
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
éteru	éter	k1gInSc6	éter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
volný	volný	k2eAgInSc1d1	volný
prostor	prostor	k1gInSc1	prostor
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Aristotelovo	Aristotelův	k2eAgNnSc4d1	Aristotelovo
dělení	dělení	k1gNnSc4	dělení
vesmíru	vesmír	k1gInSc2	vesmír
na	na	k7c4	na
translunární	translunární	k2eAgFnSc4d1	translunární
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
nad	nad	k7c7	nad
Měsícem	měsíc	k1gInSc7	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
sublunární	sublunární	k2eAgMnPc1d1	sublunární
(	(	kIx(	(
<g/>
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
Země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
zrušil	zrušit	k5eAaPmAgInS	zrušit
i	i	k9	i
rozdělení	rozdělení	k1gNnSc4	rozdělení
na	na	k7c4	na
"	"	kIx"	"
<g/>
božskou	božská	k1gFnSc4	božská
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
lidskou	lidský	k2eAgFnSc4d1	lidská
<g/>
"	"	kIx"	"
část	část	k1gFnSc4	část
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
teologická	teologický	k2eAgFnSc1d1	teologická
koncepce	koncepce	k1gFnSc1	koncepce
byla	být	k5eAaImAgFnS	být
panteistická	panteistický	k2eAgFnSc1d1	panteistická
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
de	de	k?	de
facto	facto	k1gNnSc4	facto
ateismus	ateismus	k1gInSc1	ateismus
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
skrytou	skrytý	k2eAgFnSc4d1	skrytá
formu	forma	k1gFnSc4	forma
ateismu	ateismus	k1gInSc2	ateismus
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yRnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
vitalistická	vitalistický	k2eAgFnSc1d1	vitalistická
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnPc1	jeho
planety	planeta	k1gFnPc1	planeta
i	i	k8xC	i
cizí	cizí	k2eAgInPc1d1	cizí
světy	svět	k1gInPc1	svět
jsou	být	k5eAaImIp3nP	být
prodchnuté	prodchnutý	k2eAgMnPc4d1	prodchnutý
božským	božský	k2eAgInSc7d1	božský
životem	život	k1gInSc7	život
a	a	k8xC	a
plují	plout	k5eAaImIp3nP	plout
tak	tak	k9	tak
vesmírem	vesmír	k1gInSc7	vesmír
jako	jako	k8xC	jako
velké	velký	k2eAgFnPc1d1	velká
živé	živý	k2eAgFnPc1d1	živá
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
uspořádání	uspořádání	k1gNnSc4	uspořádání
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
nekonečné	konečný	k2eNgFnSc3d1	nekonečná
matérii	matérie	k1gFnSc3	matérie
a	a	k8xC	a
nekonečné	konečný	k2eNgFnSc3d1	nekonečná
činné	činný	k2eAgFnSc3d1	činná
božské	božský	k2eAgFnSc3d1	božská
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
už	už	k6eAd1	už
nemáme	mít	k5eNaImIp1nP	mít
hledat	hledat	k5eAaImF	hledat
jiný	jiný	k2eAgInSc4d1	jiný
základ	základ	k1gInSc4	základ
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
satyře	satyra	k1gFnSc6	satyra
předhazuje	předhazovat	k5eAaImIp3nS	předhazovat
pedantskému	pedantský	k2eAgMnSc3d1	pedantský
protestantskému	protestantský	k2eAgMnSc3d1	protestantský
doktorovi	doktor	k1gMnSc3	doktor
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Než	než	k8xS	než
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
filozofie	filozofie	k1gFnSc1	filozofie
vyhovující	vyhovující	k2eAgFnSc1d1	vyhovující
vašemu	váš	k3xOp2gInSc3	váš
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
chaldejské	chaldejský	k2eAgFnPc1d1	Chaldejská
<g/>
,	,	kIx,	,
egyptské	egyptský	k2eAgFnSc2d1	egyptská
<g/>
,	,	kIx,	,
magické	magický	k2eAgFnSc2d1	magická
<g/>
,	,	kIx,	,
orfické	orfický	k2eAgFnSc2d1	orfická
<g/>
,	,	kIx,	,
pythagorejské	pythagorejský	k2eAgFnSc2d1	pythagorejská
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
nauky	nauka	k1gFnSc2	nauka
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
zase	zase	k9	zase
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
naší	náš	k3xOp1gFnSc3	náš
hlavě	hlava	k1gFnSc3	hlava
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
rodokmenu	rodokmen	k1gInSc6	rodokmen
prisca	prisc	k2eAgFnSc1d1	prisc
magia	magia	k1gFnSc1	magia
nevypočítává	vypočítávat	k5eNaImIp3nS	vypočítávat
jen	jen	k9	jen
jisté	jistý	k2eAgFnPc4d1	jistá
předchozí	předchozí	k2eAgFnPc4d1	předchozí
formy	forma	k1gFnPc4	forma
heliocentrismu	heliocentrismus	k1gInSc2	heliocentrismus
v	v	k7c6	v
astronomickém	astronomický	k2eAgInSc6d1	astronomický
smyslu	smysl	k1gInSc6	smysl
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
matematické	matematický	k2eAgFnPc4d1	matematická
hypotézy	hypotéza	k1gFnPc4	hypotéza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvádí	uvádět	k5eAaImIp3nS	uvádět
nový	nový	k2eAgInSc4d1	nový
hermetický	hermetický	k2eAgInSc4d1	hermetický
vhled	vhled	k1gInSc4	vhled
do	do	k7c2	do
božské	božský	k2eAgFnSc2d1	božská
podstaty	podstata	k1gFnSc2	podstata
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
formu	forma	k1gFnSc4	forma
gnóze	gnóze	k1gFnSc2	gnóze
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
Koperníka	Koperník	k1gMnSc2	Koperník
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
problému	problém	k1gInSc3	problém
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
jako	jako	k9	jako
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jeho	jeho	k3xOp3gInPc4	jeho
objevy	objev	k1gInPc4	objev
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
mělké	mělký	k2eAgInPc1d1	mělký
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
předjal	předejmout	k5eAaPmAgMnS	předejmout
myšlenky	myšlenka	k1gFnSc2	myšlenka
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
Leibnize	Leibnize	k1gFnSc2	Leibnize
a	a	k8xC	a
Barucha	Barucha	k1gFnSc1	Barucha
Spinozy	Spinoz	k1gInPc4	Spinoz
<g/>
.	.	kIx.	.
</s>
<s>
Teorií	teorie	k1gFnSc7	teorie
tzv.	tzv.	kA	tzv.
monád	monáda	k1gFnPc2	monáda
rovněž	rovněž	k9	rovněž
obnovil	obnovit	k5eAaPmAgInS	obnovit
atomovou	atomový	k2eAgFnSc4d1	atomová
teorii	teorie	k1gFnSc4	teorie
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
netýkala	týkat	k5eNaImAgFnS	týkat
jen	jen	k9	jen
hmoty	hmota	k1gFnPc1	hmota
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
rovněž	rovněž	k9	rovněž
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
od	od	k7c2	od
něj	on	k3xPp3gMnSc4	on
převzal	převzít	k5eAaPmAgMnS	převzít
a	a	k8xC	a
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
Leibniz	Leibniz	k1gMnSc1	Leibniz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
Brunových	Brunových	k2eAgInPc2d1	Brunových
dialogů	dialog	k1gInPc2	dialog
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
později	pozdě	k6eAd2	pozdě
proslavila	proslavit	k5eAaPmAgFnS	proslavit
Francise	Francise	k1gFnSc1	Francise
Bacona	Bacona	k1gFnSc1	Bacona
<g/>
:	:	kIx,	:
scientia	scientia	k1gFnSc1	scientia
potestas	potestas	k1gInSc1	potestas
est	est	k?	est
[	[	kIx(	[
<g/>
vědění	vědění	k1gNnSc1	vědění
je	být	k5eAaImIp3nS	být
moc	moc	k6eAd1	moc
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Brunovy	Brunův	k2eAgInPc1d1	Brunův
názory	názor	k1gInPc1	názor
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
též	též	k9	též
německého	německý	k2eAgMnSc2d1	německý
filozofa	filozof	k1gMnSc2	filozof
Schellinga	Schelling	k1gMnSc2	Schelling
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
Brunových	Brunových	k2eAgInPc2d1	Brunových
traktátů	traktát	k1gInPc2	traktát
je	být	k5eAaImIp3nS	být
věnováno	věnovat	k5eAaImNgNnS	věnovat
magii	magie	k1gFnSc3	magie
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
uměním	umění	k1gNnSc7	umění
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
mnemotechnikou	mnemotechnika	k1gFnSc7	mnemotechnika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
metoda	metoda	k1gFnSc1	metoda
pamatování	pamatování	k1gNnSc2	pamatování
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
během	během	k7c2	během
inkvizičního	inkviziční	k2eAgInSc2d1	inkviziční
procesu	proces	k1gInSc2	proces
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
schopen	schopen	k2eAgMnSc1d1	schopen
citovat	citovat	k5eAaBmF	citovat
celé	celý	k2eAgFnPc4d1	celá
stránky	stránka	k1gFnPc4	stránka
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
spisů	spis	k1gInPc2	spis
nazpaměť	nazpaměť	k6eAd1	nazpaměť
<g/>
.	.	kIx.	.
</s>
<s>
Ars	Ars	k?	Ars
memoriae	memoriae	k1gFnSc1	memoriae
(	(	kIx(	(
<g/>
Umění	umění	k1gNnSc1	umění
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1582	[number]	k4	1582
<g/>
)	)	kIx)	)
De	De	k?	De
umbris	umbris	k1gInSc1	umbris
idearum	idearum	k1gInSc1	idearum
(	(	kIx(	(
<g/>
O	o	k7c6	o
stínu	stín	k1gInSc6	stín
idejí	idea	k1gFnPc2	idea
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1582	[number]	k4	1582
<g/>
)	)	kIx)	)
Cantus	Cantus	k1gMnSc1	Cantus
Circaeus	Circaeus	k1gMnSc1	Circaeus
(	(	kIx(	(
<g/>
Kruhové	kruhový	k2eAgNnSc1d1	kruhové
zaříkávání	zaříkávání	k1gNnSc1	zaříkávání
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1582	[number]	k4	1582
<g/>
)	)	kIx)	)
De	De	k?	De
compendiosa	compendiosa	k1gFnSc1	compendiosa
architectura	architectura	k1gFnSc1	architectura
(	(	kIx(	(
<g/>
1582	[number]	k4	1582
<g/>
)	)	kIx)	)
Candelaio	Candelaio	k6eAd1	Candelaio
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Svíčkař	svíčkař	k1gMnSc1	svíčkař
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc2d1	divadelní
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
1582	[number]	k4	1582
<g/>
)	)	kIx)	)
Ars	Ars	k1gFnPc2	Ars
reminiscendi	reminiscend	k1gMnPc1	reminiscend
(	(	kIx(	(
<g/>
1583	[number]	k4	1583
<g/>
)	)	kIx)	)
Explicatio	Explicatio	k1gNnSc1	Explicatio
triginta	trigint	k1gInSc2	trigint
sigillorum	sigillorum	k1gInSc1	sigillorum
(	(	kIx(	(
<g/>
1583	[number]	k4	1583
<g/>
)	)	kIx)	)
Sigillus	Sigillus	k1gInSc1	Sigillus
sigillorum	sigillorum	k1gNnSc1	sigillorum
(	(	kIx(	(
<g/>
Pečeť	pečeť	k1gFnSc1	pečeť
pečetí	pečeť	k1gFnSc7	pečeť
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1583	[number]	k4	1583
<g/>
)	)	kIx)	)
La	la	k1gNnSc2	la
Cena	cena	k1gFnSc1	cena
de	de	k?	de
le	le	k?	le
Ceneri	Cener	k1gFnSc2	Cener
(	(	kIx(	(
<g/>
Večeře	večeře	k1gFnSc1	večeře
na	na	k7c4	na
Popeleční	popeleční	k2eAgFnSc4d1	popeleční
středu	středa	k1gFnSc4	středa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
De	De	k?	De
la	la	k1gNnSc1	la
Causa	causa	k1gFnSc1	causa
<g/>
,	,	kIx,	,
Principio	Principio	k1gMnSc1	Principio
et	et	k?	et
Uno	Uno	k1gMnSc1	Uno
(	(	kIx(	(
<g/>
O	o	k7c6	o
příčině	příčina	k1gFnSc6	příčina
<g/>
,	,	kIx,	,
principu	princip	k1gInSc6	princip
a	a	k8xC	a
jednom	jeden	k4xCgInSc6	jeden
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
De	De	k?	De
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Infinito	Infinit	k2eAgNnSc1d1	Infinito
Universo	Universa	k1gFnSc5	Universa
et	et	k?	et
Mondi	Mond	k1gMnPc1	Mond
(	(	kIx(	(
<g/>
O	o	k7c6	o
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
,	,	kIx,	,
univerzu	univerzum	k1gNnSc6	univerzum
a	a	k8xC	a
světech	svět	k1gInPc6	svět
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
Spaccio	Spaccio	k1gNnSc4	Spaccio
de	de	k?	de
la	la	k1gNnSc2	la
Bestia	Bestia	k?	Bestia
Trionfante	Trionfant	k1gMnSc5	Trionfant
(	(	kIx(	(
<g/>
Vyhnání	vyhnání	k1gNnSc3	vyhnání
vítězné	vítězný	k2eAgFnSc2d1	vítězná
bestie	bestie	k1gFnSc2	bestie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
Cabala	Cabala	k1gFnSc1	Cabala
del	del	k?	del
cavallo	cavalnout	k5eAaImAgNnS	cavalnout
Pegaseocon	Pegaseocon	k1gNnSc1	Pegaseocon
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
aggiunta	aggiunta	k1gMnSc1	aggiunta
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
<g/>
Asino	Asien	k2eAgNnSc1d1	Asien
Cillenico	Cillenico	k1gNnSc1	Cillenico
(	(	kIx(	(
<g/>
Pegasova	Pegasův	k2eAgFnSc1d1	Pegasova
Kabala	kabala	k1gFnSc1	kabala
s	s	k7c7	s
dodatkem	dodatek	k1gInSc7	dodatek
Kyllénského	Kyllénský	k1gMnSc2	Kyllénský
osla	osel	k1gMnSc2	osel
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1585	[number]	k4	1585
<g/>
)	)	kIx)	)
De	De	k?	De
gl	gl	k?	gl
<g/>
'	'	kIx"	'
heroici	heroic	k1gMnSc3	heroic
furori	furor	k1gMnSc3	furor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
hrdinském	hrdinský	k2eAgNnSc6d1	hrdinské
nadšení	nadšení	k1gNnSc6	nadšení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
taky	taky	k6eAd1	taky
"	"	kIx"	"
<g/>
O	o	k7c6	o
heroickém	heroický	k2eAgNnSc6d1	heroické
nadšení	nadšení	k1gNnSc6	nadšení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1585	[number]	k4	1585
<g/>
)	)	kIx)	)
Figuratio	Figuratio	k6eAd1	Figuratio
Aristotelici	aristotelik	k1gMnPc1	aristotelik
Physici	Physice	k1gFnSc3	Physice
auditus	auditus	k1gInSc1	auditus
(	(	kIx(	(
<g/>
1585	[number]	k4	1585
<g/>
)	)	kIx)	)
Dialogi	Dialogi	k1gNnSc6	Dialogi
duo	duo	k1gNnSc1	duo
de	de	k?	de
Fabricii	Fabricie	k1gFnSc3	Fabricie
Mordentis	Mordentis	k1gFnPc2	Mordentis
<g />
.	.	kIx.	.
</s>
<s>
Salernitani	Salernitan	k1gMnPc1	Salernitan
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
Idiota	idiot	k1gMnSc4	idiot
triumphans	triumphans	k6eAd1	triumphans
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
De	De	k?	De
somni	somn	k1gMnPc1	somn
interpretatione	interpretation	k1gInSc5	interpretation
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
Animadversiones	Animadversiones	k1gMnSc1	Animadversiones
circa	circa	k1gMnSc1	circa
lampadem	lampad	k1gInSc7	lampad
lullianam	lullianam	k6eAd1	lullianam
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
Lampas	lampas	k1gInSc1	lampas
triginta	triginta	k1gMnSc1	triginta
statuarum	statuarum	k1gInSc1	statuarum
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
Centum	Centum	k1gNnSc1	Centum
et	et	k?	et
viginti	viginť	k1gFnSc2	viginť
articuli	articule	k1gFnSc3	articule
de	de	k?	de
natura	natura	k1gFnSc1	natura
et	et	k?	et
mundo	mundo	k1gNnSc1	mundo
adversus	adversus	k1gMnSc1	adversus
peripateticos	peripateticos	k1gMnSc1	peripateticos
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
Delampade	Delampad	k1gInSc5	Delampad
combinatoria	combinatorium	k1gNnSc2	combinatorium
Lulliana	Lulliana	k1gFnSc1	Lulliana
(	(	kIx(	(
<g/>
1587	[number]	k4	1587
<g/>
)	)	kIx)	)
De	De	k?	De
progressu	progress	k1gInSc2	progress
et	et	k?	et
lampade	lampást	k5eAaPmIp3nS	lampást
venatoria	venatorium	k1gNnSc2	venatorium
logicorum	logicorum	k1gNnSc4	logicorum
(	(	kIx(	(
<g/>
1587	[number]	k4	1587
<g/>
)	)	kIx)	)
Oratio	Oratio	k1gNnSc1	Oratio
valedictoria	valedictorium	k1gNnSc2	valedictorium
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
De	De	k?	De
specierum	specierum	k1gInSc1	specierum
scrutinio	scrutinio	k1gMnSc1	scrutinio
et	et	k?	et
lampade	lampást	k5eAaPmIp3nS	lampást
combinatorio	combinatorio	k6eAd1	combinatorio
Raymundi	Raymund	k1gMnPc1	Raymund
Lullii	Lullium	k1gNnPc7	Lullium
(	(	kIx(	(
<g/>
O	o	k7c6	o
zkoumání	zkoumání	k1gNnSc6	zkoumání
zvláštností	zvláštnost	k1gFnPc2	zvláštnost
a	a	k8xC	a
o	o	k7c6	o
světle	světlo	k1gNnSc6	světlo
kombinatoriky	kombinatorika	k1gFnSc2	kombinatorika
Raymonda	Raymond	k1gMnSc2	Raymond
Lulla	Lull	k1gMnSc2	Lull
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Articuli	Articule	k1gFnSc4	Articule
centum	centum	k1gNnSc1	centum
et	et	k?	et
sexaginta	sexaginta	k1gMnSc1	sexaginta
adversus	adversus	k1gMnSc1	adversus
huius	huius	k1gMnSc1	huius
tempestatis	tempestatis	k1gFnPc2	tempestatis
mathematicos	mathematicos	k1gMnSc1	mathematicos
atque	atqu	k1gFnSc2	atqu
philosophos	philosophos	k1gMnSc1	philosophos
(	(	kIx(	(
<g/>
Sto	sto	k4xCgNnSc1	sto
šedesát	šedesát	k4xCc1	šedesát
článků	článek	k1gInPc2	článek
proti	proti	k7c3	proti
současným	současný	k2eAgMnPc3d1	současný
matematikům	matematik	k1gMnPc3	matematik
a	a	k8xC	a
filozofům	filozof	k1gMnPc3	filozof
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
věnováno	věnovat	k5eAaImNgNnS	věnovat
Rudolfovi	Rudolf	k1gMnSc3	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Camoeracensis	Camoeracensis	k1gInSc1	Camoeracensis
Acrotismus	Acrotismus	k1gInSc1	Acrotismus
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
De	De	k?	De
specierum	specierum	k1gInSc1	specierum
scrutinio	scrutinio	k1gMnSc1	scrutinio
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
Oratio	Oratio	k1gNnSc1	Oratio
consolatoria	consolatorium	k1gNnSc2	consolatorium
(	(	kIx(	(
<g/>
1589	[number]	k4	1589
<g/>
)	)	kIx)	)
De	De	k?	De
vinculis	vinculis	k1gInSc1	vinculis
in	in	k?	in
genere	genrat	k5eAaPmIp3nS	genrat
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
<g/>
)	)	kIx)	)
De	De	k?	De
triplici	triplice	k1gFnSc4	triplice
minimo	minimo	k6eAd1	minimo
et	et	k?	et
mensura	mensura	k1gFnSc1	mensura
(	(	kIx(	(
<g/>
O	o	k7c6	o
trojím	trojí	k4xRgInSc6	trojí
minimu	minimum	k1gNnSc6	minimum
a	a	k8xC	a
míře	míra	k1gFnSc6	míra
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
De	De	k?	De
monade	monást	k5eAaPmIp3nS	monást
numero	numero	k?	numero
et	et	k?	et
figura	figura	k1gFnSc1	figura
(	(	kIx(	(
<g/>
O	o	k7c6	o
monadě	monada	k1gFnSc6	monada
<g/>
,	,	kIx,	,
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
tvaru	tvar	k1gInSc6	tvar
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
<g/>
)	)	kIx)	)
De	De	k?	De
innumerabilibus	innumerabilibus	k1gInSc1	innumerabilibus
<g/>
,	,	kIx,	,
immenso	immensa	k1gFnSc5	immensa
et	et	k?	et
infigurabili	infigurabit	k5eAaImAgMnP	infigurabit
(	(	kIx(	(
<g/>
O	o	k7c6	o
nezměrném	změrný	k2eNgNnSc6d1	nezměrné
a	a	k8xC	a
nespočetném	spočetný	k2eNgNnSc6d1	nespočetné
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
<g/>
)	)	kIx)	)
De	De	k?	De
imaginum	imaginum	k1gInSc1	imaginum
<g/>
,	,	kIx,	,
signorum	signorum	k?	signorum
et	et	k?	et
idearum	idearum	k1gInSc1	idearum
compositione	composition	k1gInSc5	composition
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1591	[number]	k4	1591
<g/>
)	)	kIx)	)
Summa	Summa	k1gFnSc1	Summa
terminorum	terminorum	k1gInSc1	terminorum
metaphisicorum	metaphisicorum	k1gInSc1	metaphisicorum
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
)	)	kIx)	)
Artificium	Artificium	k1gNnSc1	Artificium
perorandi	perorand	k1gMnPc1	perorand
(	(	kIx(	(
<g/>
1612	[number]	k4	1612
<g/>
)	)	kIx)	)
Dialogy	dialog	k1gInPc1	dialog
<g/>
,	,	kIx,	,
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
B.	B.	kA	B.
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
;	;	kIx,	;
předmluva	předmluva	k1gFnSc1	předmluva
a	a	k8xC	a
stať	stať	k1gFnSc1	stať
Jiřiny	Jiřina	k1gFnSc2	Jiřina
Popelové-Otáhalové	Popelové-Otáhalová	k1gFnSc2	Popelové-Otáhalová
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
Giordana	Giordan	k1gMnSc2	Giordan
Bruna	Bruno	k1gMnSc2	Bruno
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přeložená	přeložený	k2eAgNnPc4d1	přeložené
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
Večeře	večeře	k1gFnSc1	večeře
na	na	k7c4	na
Popeleční	popeleční	k2eAgFnSc4d1	popeleční
středu	středa	k1gFnSc4	středa
<g/>
,	,	kIx,	,
O	o	k7c6	o
příčině	příčina	k1gFnSc6	příčina
<g/>
,	,	kIx,	,
principu	princip	k1gInSc6	princip
a	a	k8xC	a
jednom	jeden	k4xCgNnSc6	jeden
<g/>
,	,	kIx,	,
O	o	k7c6	o
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
,	,	kIx,	,
universu	universum	k1gNnSc6	universum
a	a	k8xC	a
světech	svět	k1gInPc6	svět
<g/>
;	;	kIx,	;
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
;	;	kIx,	;
revidovaný	revidovaný	k2eAgInSc1d1	revidovaný
překlad	překlad	k1gInSc1	překlad
v	v	k7c4	v
nakl	nakl	k1gInSc4	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-200-1668-3	[number]	k4	978-80-200-1668-3
Božskému	božský	k2eAgMnSc3d1	božský
Rudolfovi	Rudolf	k1gMnSc3	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
=	=	kIx~	=
To	ten	k3xDgNnSc1	ten
divine	divinout	k5eAaPmIp3nS	divinout
Rudolphus	Rudolphus	k1gInSc1	Rudolphus
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kalivoda	Kalivoda	k1gMnSc1	Kalivoda
<g/>
;	;	kIx,	;
anglický	anglický	k2eAgInSc1d1	anglický
překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Potůček	Potůček	k1gMnSc1	Potůček
<g/>
;	;	kIx,	;
stať	stať	k1gFnSc1	stať
Štvanec	štvanec	k1gMnSc1	štvanec
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Štoll	Štoll	k1gMnSc1	Štoll
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
Čítanka	čítanka	k1gFnSc1	čítanka
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Halada	Halada	k1gFnSc1	Halada
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
Geuss	Geuss	k1gInSc1	Geuss
<g/>
;	;	kIx,	;
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
ukázky	ukázka	k1gFnPc4	ukázka
z	z	k7c2	z
Brunových	Brunových	k2eAgNnPc2d1	Brunových
děl	dělo	k1gNnPc2	dělo
<g/>
)	)	kIx)	)
Mučedník	mučedník	k1gMnSc1	mučedník
římské	římský	k2eAgFnSc2d1	římská
inkvisice	inkvisice	k1gFnSc2	inkvisice
/	/	kIx~	/
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
;	;	kIx,	;
Životopis	životopis	k1gInSc1	životopis
a	a	k8xC	a
výtah	výtah	k1gInSc1	výtah
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
"	"	kIx"	"
<g/>
O	o	k7c6	o
nekonečném	konečný	k2eNgInSc6d1	nekonečný
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
světlech	světlo	k1gNnPc6	světlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josef	Josef	k1gMnSc1	Josef
Staněk	Staněk	k1gMnSc1	Staněk
(	(	kIx(	(
<g/>
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
346071	[number]	k4	346071
<g/>
)	)	kIx)	)
Magie	magie	k1gFnPc4	magie
<g/>
,	,	kIx,	,
pouta	pouto	k1gNnPc4	pouto
a	a	k8xC	a
dialog	dialog	k1gInSc4	dialog
renesančního	renesanční	k2eAgMnSc2d1	renesanční
filozofa	filozof	k1gMnSc2	filozof
<g/>
;	;	kIx,	;
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
traktáty	traktát	k1gInPc1	traktát
O	o	k7c6	o
magii	magie	k1gFnSc6	magie
<g/>
,	,	kIx,	,
O	o	k7c6	o
poutech	pouto	k1gNnPc6	pouto
a	a	k8xC	a
Kabala	kabala	k1gFnSc1	kabala
pegasovského	pegasovský	k2eAgMnSc2d1	pegasovský
koně	kůň	k1gMnSc2	kůň
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
o	o	k7c6	o
kyllénském	kyllénský	k2eAgMnSc6d1	kyllénský
oslu	osel	k1gMnSc6	osel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7203-760-5	[number]	k4	978-80-7203-760-5
Leopold	Leopolda	k1gFnPc2	Leopolda
Schefer	Schefra	k1gFnPc2	Schefra
<g/>
:	:	kIx,	:
Göttliche	Göttliche	k1gFnSc1	Göttliche
Komödie	Komödie	k1gFnSc2	Komödie
in	in	k?	in
Rom	Rom	k1gMnSc1	Rom
(	(	kIx(	(
<g/>
Božská	božský	k2eAgFnSc1d1	božská
komedie	komedie	k1gFnSc1	komedie
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
o	o	k7c6	o
procesu	proces	k1gInSc6	proces
a	a	k8xC	a
popravě	poprava	k1gFnSc6	poprava
Girodana	Girodan	k1gMnSc2	Girodan
Bruna	Bruno	k1gMnSc2	Bruno
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
:	:	kIx,	:
povídka	povídka	k1gFnSc1	povídka
Kacířův	kacířův	k2eAgInSc1d1	kacířův
plášť	plášť	k1gInSc1	plášť
v	v	k7c6	v
Kalendářových	kalendářový	k2eAgFnPc6d1	kalendářová
historkách	historka	k1gFnPc6	historka
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
(	(	kIx(	(
<g/>
beletrie	beletrie	k1gFnSc1	beletrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
Karel	Karel	k1gMnSc1	Karel
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
,	,	kIx,	,
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Zakázané	zakázaný	k2eAgNnSc4d1	zakázané
ovoce	ovoce	k1gNnSc4	ovoce
vědění	vědění	k1gNnSc2	vědění
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
Benjamina	Benjamin	k1gMnSc2	Benjamin
Kurase	Kuras	k1gInSc6	Kuras
Eugen	Eugen	k2eAgInSc1d1	Eugen
Drewermann	Drewermann	k1gInSc1	Drewermann
<g/>
:	:	kIx,	:
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
oder	odra	k1gFnPc2	odra
Der	drát	k5eAaImRp2nS	drát
Spiegel	Spiegel	k1gInSc1	Spiegel
des	des	k1gNnSc6	des
Unendlichen	Unendlichen	k1gInSc1	Unendlichen
(	(	kIx(	(
<g/>
románová	románový	k2eAgFnSc1d1	románová
biografie	biografie	k1gFnSc1	biografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
dtv	dtv	k?	dtv
TB	TB	kA	TB
<g/>
:	:	kIx,	:
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
423	[number]	k4	423
<g/>
-	-	kIx~	-
<g/>
30747	[number]	k4	30747
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Czesław	Czesław	k1gMnSc1	Czesław
Miłosz	Miłosz	k1gMnSc1	Miłosz
<g/>
:	:	kIx,	:
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Campo	Campa	k1gFnSc5	Campa
di	di	k?	di
Fiori	Fiori	k1gNnSc4	Fiori
<g/>
"	"	kIx"	"
srovnává	srovnávat	k5eAaImIp3nS	srovnávat
lhostejnost	lhostejnost	k1gFnSc1	lhostejnost
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
Říma	Řím	k1gInSc2	Řím
k	k	k7c3	k
Brunově	Brunův	k2eAgFnSc3d1	Brunova
upálení	upálení	k1gNnSc2	upálení
a	a	k8xC	a
lhostejností	lhostejnost	k1gFnPc2	lhostejnost
Poláků	Polák	k1gMnPc2	Polák
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
povstání	povstání	k1gNnSc2	povstání
ve	v	k7c6	v
Varšavském	varšavský	k2eAgNnSc6d1	Varšavské
ghettu	ghetto	k1gNnSc6	ghetto
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Jamese	Jamese	k1gFnSc1	Jamese
Joyce	Joyce	k1gFnSc1	Joyce
"	"	kIx"	"
<g/>
Portrét	portrét	k1gInSc1	portrét
umělce	umělec	k1gMnSc2	umělec
v	v	k7c6	v
jinošských	jinošský	k2eAgNnPc6d1	jinošské
letech	léto	k1gNnPc6	léto
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
Bruno	Bruno	k1gMnSc1	Bruno
zmíněn	zmíněn	k2eAgMnSc1d1	zmíněn
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
;	;	kIx,	;
Bruno	Bruno	k1gMnSc1	Bruno
je	být	k5eAaImIp3nS	být
zmíněn	zmínit	k5eAaPmNgMnS	zmínit
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
Joyceově	Joyceův	k2eAgInSc6d1	Joyceův
díle	díl	k1gInSc6	díl
Finnegans	Finnegans	k1gInSc1	Finnegans
Wake	Wak	k1gInPc1	Wak
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Plačky	plačky	k6eAd1	plačky
nad	nad	k7c7	nad
Finneganem	Finnegan	k1gInSc7	Finnegan
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Williama	Williama	k1gFnSc1	Williama
Heinesena	Heinesen	k2eAgFnSc1d1	Heinesen
"	"	kIx"	"
<g/>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
naděje	naděje	k1gFnSc1	naděje
<g/>
"	"	kIx"	"
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
osmé	osmý	k4xOgFnSc6	osmý
kapitole	kapitola	k1gFnSc6	kapitola
(	(	kIx(	(
<g/>
psáno	psán	k2eAgNnSc1d1	psáno
jako	jako	k9	jako
deník	deník	k1gInSc4	deník
a	a	k8xC	a
označeno	označit	k5eAaPmNgNnS	označit
datumem	datum	k1gInSc7	datum
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Novembris	Novembris	k1gInSc1	Novembris
A.	A.	kA	A.
D.	D.	kA	D.
1669	[number]	k4	1669
et	et	k?	et
c.	c.	k?	c.
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
vydání	vydání	k1gNnSc6	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
251	[number]	k4	251
<g/>
)	)	kIx)	)
Heinesen	Heinesen	k2eAgInSc1d1	Heinesen
Giordana	Giordan	k1gMnSc4	Giordan
Bruna	Bruno	k1gMnSc4	Bruno
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
...	...	k?	...
Neboť	neboť	k8xC	neboť
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
veliký	veliký	k2eAgMnSc1d1	veliký
pošetilec	pošetilec	k1gMnSc1	pošetilec
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
před	před	k7c7	před
sebou	se	k3xPyFc7	se
blázen	blázen	k1gMnSc1	blázen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
chováš	chovat	k5eAaImIp2nS	chovat
démona	démon	k1gMnSc4	démon
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
chtivého	chtivý	k2eAgMnSc2d1	chtivý
smyslníka	smyslník	k1gMnSc2	smyslník
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
onen	onen	k3xDgInSc1	onen
veliký	veliký	k2eAgInSc1d1	veliký
heterodoxus	heterodoxus	k1gInSc1	heterodoxus
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
můžeš	moct	k5eAaImIp2nS	moct
volat	volat	k5eAaImF	volat
k	k	k7c3	k
nebesům	nebesa	k1gNnPc3	nebesa
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ani	ani	k8xC	ani
sněhové	sněhový	k2eAgFnPc1d1	sněhová
Kavkazu	Kavkaz	k1gInSc6	Kavkaz
nezchladí	zchladit	k5eNaPmIp3nS	zchladit
pramen	pramen	k1gInSc1	pramen
mého	můj	k3xOp1gInSc2	můj
chtíče	chtíč	k1gInSc2	chtíč
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Giordano	Giordana	k1gFnSc5	Giordana
<g/>
,	,	kIx,	,
muzikál	muzikál	k1gInSc1	muzikál
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
ruského	ruský	k2eAgMnSc2d1	ruský
básníka	básník	k1gMnSc2	básník
Vladimira	Vladimir	k1gMnSc2	Vladimir
Kostrova	Kostrův	k2eAgMnSc2d1	Kostrův
a	a	k8xC	a
skladatelky	skladatelka	k1gFnPc4	skladatelka
Lory	Lora	k1gFnSc2	Lora
Kvint	kvinta	k1gFnPc2	kvinta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
29	[number]	k4	29
<g/>
x	x	k?	x
uveden	uveden	k2eAgInSc1d1	uveden
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Leningradě	Leningrad	k1gInSc6	Leningrad
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
italsko-francouzský	italskorancouzský	k2eAgInSc1d1	italsko-francouzský
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Giuliano	Giuliana	k1gFnSc5	Giuliana
Montaldo	Montaldo	k1gNnSc1	Montaldo
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
Lucio	Lucio	k1gMnSc1	Lucio
<g />
.	.	kIx.	.
</s>
<s>
de	de	k?	de
Caro	Caro	k1gMnSc1	Caro
Bruno	Bruno	k1gMnSc1	Bruno
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Galileo	Galilea	k1gFnSc5	Galilea
Janka	Janek	k1gMnSc4	Janek
Ledeckého	Ledecký	k2eAgMnSc4d1	Ledecký
<g/>
,	,	kIx,	,
postavu	postava	k1gFnSc4	postava
zde	zde	k6eAd1	zde
zahráli	zahrát	k5eAaPmAgMnP	zahrát
Bohouš	Bohouš	k1gMnSc1	Bohouš
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Kolář	Kolář	k1gMnSc1	Kolář
Písnička	písnička	k1gFnSc1	písnička
skupiny	skupina	k1gFnSc2	skupina
Olympic	Olympice	k1gFnPc2	Olympice
(	(	kIx(	(
<g/>
z	z	k7c2	z
Brejle	brejle	k1gFnPc1	brejle
a	a	k8xC	a
Stejskání	Stejskání	k1gNnSc1	Stejskání
<g/>
)	)	kIx)	)
Písnička	písnička	k1gFnSc1	písnička
skupiny	skupina	k1gFnSc2	skupina
Nová	nový	k2eAgFnSc1d1	nová
Růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
)	)	kIx)	)
album	album	k1gNnSc1	album
Nová	Nová	k1gFnSc1	Nová
Růže	růže	k1gFnSc1	růže
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
Vilém	Vilém	k1gMnSc1	Vilém
Čok	Čok	k1gMnSc1	Čok
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Písnička	písnička	k1gFnSc1	písnička
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
UDG	UDG	kA	UDG
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
zmíněn	zmíněn	k2eAgMnSc1d1	zmíněn
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
a	a	k8xC	a
nepodlehl	podlehnout	k5eNaPmAgMnS	podlehnout
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
svých	svůj	k3xOyFgInPc2	svůj
názorů	názor	k1gInPc2	názor
se	se	k3xPyFc4	se
nevzdávat	vzdávat	k5eNaImF	vzdávat
<g/>
)	)	kIx)	)
Po	po	k7c6	po
Brunovi	Bruna	k1gMnSc6	Bruna
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
<g/>
:	:	kIx,	:
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
)	)	kIx)	)
německá	německý	k2eAgFnSc1d1	německá
nadace	nadace	k1gFnSc1	nadace
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
Stiftung	Stiftung	k1gInSc1	Stiftung
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
"	"	kIx"	"
<g/>
evoluční	evoluční	k2eAgInSc4d1	evoluční
humanizmus	humanizmus	k1gInSc4	humanizmus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
dílo	dílo	k1gNnSc1	dílo
Karlheinze	Karlheinze	k1gFnSc2	Karlheinze
Deschnera	Deschner	k1gMnSc2	Deschner
<g/>
.	.	kIx.	.
</s>
