<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
MOV	MOV	kA	MOV
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Comité	Comitý	k2eAgFnSc2d1	Comitý
International	International	k1gFnSc2	International
Olympique	Olympiqu	k1gFnSc2	Olympiqu
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
International	International	k1gFnSc1	International
Olympic	Olympice	k1gInPc2	Olympice
Committee	Committe	k1gMnSc2	Committe
<g/>
,	,	kIx,	,
IOC	IOC	kA	IOC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
Pierrem	Pierr	k1gMnSc7	Pierr
de	de	k?	de
Coubertinem	Coubertin	k1gMnSc7	Coubertin
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Demetriusem	Demetrius	k1gMnSc7	Demetrius
Vikelasem	Vikelas	k1gMnSc7	Vikelas
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Guthem	Guth	k1gInSc7	Guth
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1894	[number]	k4	1894
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
obnovit	obnovit	k5eAaPmF	obnovit
tradici	tradice	k1gFnSc4	tradice
antických	antický	k2eAgFnPc2d1	antická
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
MOV	MOV	kA	MOV
patří	patřit	k5eAaImIp3nP	patřit
205	[number]	k4	205
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
uznávané	uznávaný	k2eAgFnPc1d1	uznávaná
stanovy	stanova	k1gFnPc1	stanova
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
známy	znám	k2eAgInPc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
charta	charta	k1gFnSc1	charta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
Němec	Němec	k1gMnSc1	Němec
Thomas	Thomas	k1gMnSc1	Thomas
Bach	Bach	k1gInSc4	Bach
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
olympijský	olympijský	k2eAgInSc4d1	olympijský
výbor	výbor	k1gInSc4	výbor
organizuje	organizovat	k5eAaBmIp3nS	organizovat
letní	letní	k2eAgFnSc1d1	letní
a	a	k8xC	a
zimní	zimní	k2eAgFnSc1d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
moderní	moderní	k2eAgFnPc1d1	moderní
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Olympismus	olympismus	k1gInSc1	olympismus
je	být	k5eAaImIp3nS	být
životní	životní	k2eAgFnSc1d1	životní
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgFnSc1d1	spojující
vyrovnanost	vyrovnanost	k1gFnSc1	vyrovnanost
a	a	k8xC	a
kvality	kvalita	k1gFnSc2	kvalita
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Sport	sport	k1gInSc1	sport
se	se	k3xPyFc4	se
míchá	míchat	k5eAaImIp3nS	míchat
s	s	k7c7	s
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
výchovou	výchova	k1gFnSc7	výchova
<g/>
,	,	kIx,	,
olympismus	olympismus	k1gInSc1	olympismus
hledá	hledat	k5eAaImIp3nS	hledat
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
principu	princip	k1gInSc6	princip
radosti	radost	k1gFnSc2	radost
z	z	k7c2	z
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
,	,	kIx,	,
výchovné	výchovný	k2eAgFnSc2d1	výchovná
hodnoty	hodnota	k1gFnSc2	hodnota
dobrého	dobrý	k2eAgInSc2d1	dobrý
příkladu	příklad	k1gInSc2	příklad
a	a	k8xC	a
respektování	respektování	k1gNnSc1	respektování
morálních	morální	k2eAgInPc2d1	morální
principů	princip	k1gInPc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
MOV	MOV	kA	MOV
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
MOV	MOV	kA	MOV
se	se	k3xPyFc4	se
také	také	k9	také
smířil	smířit	k5eAaPmAgInS	smířit
s	s	k7c7	s
cenzurou	cenzura	k1gFnSc7	cenzura
Internetu	Internet	k1gInSc2	Internet
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
během	během	k7c2	během
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zásadou	zásada	k1gFnSc7	zásada
olympismu	olympismus	k1gInSc2	olympismus
<g/>
,	,	kIx,	,
definovanou	definovaný	k2eAgFnSc7d1	definovaná
v	v	k7c6	v
Olympijské	olympijský	k2eAgFnSc6d1	olympijská
chartě	charta	k1gFnSc6	charta
v	v	k7c6	v
části	část	k1gFnSc6	část
Základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
olympismu	olympismus	k1gInSc2	olympismus
<g/>
.	.	kIx.	.
</s>
<s>
Demetrius	Demetrius	k1gMnSc1	Demetrius
Vikelas	Vikelas	k1gMnSc1	Vikelas
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k1gInSc1	Coubertin
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Henri	Henri	k1gNnSc1	Henri
de	de	k?	de
Baillet-Latour	Baillet-Latour	k1gMnSc1	Baillet-Latour
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Sigfrid	Sigfrid	k1gInSc1	Sigfrid
Edström	Edström	k1gInSc1	Edström
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Avery	Avera	k1gFnSc2	Avera
Brundage	Brundag	k1gFnSc2	Brundag
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Lord	lord	k1gMnSc1	lord
Killanin	Killanina	k1gFnPc2	Killanina
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Juan	Juan	k1gMnSc1	Juan
Antonio	Antonio	k1gMnSc1	Antonio
Samaranch	Samaranch	k1gMnSc1	Samaranch
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Jacques	Jacques	k1gMnSc1	Jacques
Rogge	Rogg	k1gMnSc2	Rogg
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Thomas	Thomas	k1gMnSc1	Thomas
Bach	Bach	k1gMnSc1	Bach
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
současný	současný	k2eAgMnSc1d1	současný
předseda	předseda	k1gMnSc1	předseda
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
mládeže	mládež	k1gFnSc2	mládež
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
mládeže	mládež	k1gFnSc2	mládež
</s>
