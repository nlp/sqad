<p>
<s>
Sekvoj	Sekvoj	k1gInSc1	Sekvoj
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
(	(	kIx(	(
<g/>
Sequoia	Sequoia	k1gFnSc1	Sequoia
sempervirens	sempervirensa	k1gFnPc2	sempervirensa
D.	D.	kA	D.
Don	Don	k1gMnSc1	Don
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
české	český	k2eAgInPc1d1	český
jazykové	jazykový	k2eAgInPc1d1	jazykový
slovníky	slovník	k1gInPc1	slovník
uvádějí	uvádět	k5eAaImIp3nP	uvádět
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
pádu	pád	k1gInSc6	pád
jako	jako	k9	jako
přípustný	přípustný	k2eAgMnSc1d1	přípustný
pouze	pouze	k6eAd1	pouze
tvar	tvar	k1gInSc4	tvar
sekvoje	sekvoje	k1gFnSc2	sekvoje
<g/>
,	,	kIx,	,
v	v	k7c6	v
botanických	botanický	k2eAgInPc6d1	botanický
slovnících	slovník	k1gInPc6	slovník
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
název	název	k1gInSc1	název
sekvoja	sekvojum	k1gNnSc2	sekvojum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stálezelený	stálezelený	k2eAgInSc1d1	stálezelený
jehličnatý	jehličnatý	k2eAgInSc1d1	jehličnatý
strom	strom	k1gInSc1	strom
s	s	k7c7	s
úzce	úzko	k6eAd1	úzko
kuželovitou	kuželovitý	k2eAgFnSc7d1	kuželovitá
korunou	koruna	k1gFnSc7	koruna
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
až	až	k9	až
116	[number]	k4	116
m	m	kA	m
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
až	až	k9	až
2200	[number]	k4	2200
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
recentní	recentní	k2eAgInSc1d1	recentní
druh	druh	k1gInSc1	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
sekvoj	sekvoj	k1gInSc1	sekvoj
(	(	kIx(	(
<g/>
Sequoia	Sequoia	k1gFnSc1	Sequoia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
některých	některý	k3yIgInPc2	některý
stromů	strom	k1gInPc2	strom
poražených	poražený	k2eAgInPc2d1	poražený
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
144	[number]	k4	144
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
žijícím	žijící	k2eAgInSc7d1	žijící
stromem	strom	k1gInSc7	strom
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
sekvoj	sekvoj	k1gInSc1	sekvoj
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
115,5	[number]	k4	115,5
m	m	kA	m
a	a	k8xC	a
jménem	jméno	k1gNnSc7	jméno
Hyperion	Hyperion	k1gInSc1	Hyperion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Průměr	průměr	k1gInSc1	průměr
kmene	kmen	k1gInSc2	kmen
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
6,5	[number]	k4	6,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jehlice	jehlice	k1gFnPc1	jehlice
jsou	být	k5eAaImIp3nP	být
čárkovité	čárkovitý	k2eAgFnPc1d1	čárkovitá
<g/>
,	,	kIx,	,
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
špičaté	špičatý	k2eAgNnSc1d1	špičaté
<g/>
,	,	kIx,	,
na	na	k7c6	na
líci	líc	k1gFnSc6	líc
tmavozelené	tmavozelený	k2eAgFnSc6d1	tmavozelená
<g/>
,	,	kIx,	,
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
bílými	bílý	k2eAgInPc7d1	bílý
pásy	pás	k1gInPc7	pás
<g/>
,	,	kIx,	,
roztroušené	roztroušený	k2eAgInPc1d1	roztroušený
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
větévky	větévka	k1gFnSc2	větévka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Borka	borka	k1gFnSc1	borka
je	být	k5eAaImIp3nS	být
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
<g/>
,	,	kIx,	,
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
se	s	k7c7	s
širokými	široký	k2eAgInPc7d1	široký
hřebeny	hřeben	k1gInPc7	hřeben
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
tloušťky	tloušťka	k1gFnSc2	tloušťka
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
strom	strom	k1gInSc1	strom
činí	činit	k5eAaImIp3nS	činit
odolným	odolný	k2eAgInSc7d1	odolný
proti	proti	k7c3	proti
požárům	požár	k1gInPc3	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strom	strom	k1gInSc1	strom
produkuje	produkovat	k5eAaImIp3nS	produkovat
tanin	tanin	k1gInSc4	tanin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
ho	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
proti	proti	k7c3	proti
hmyzu	hmyz	k1gInSc3	hmyz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květy	Květa	k1gFnPc1	Květa
-	-	kIx~	-
samčí	samčí	k2eAgInPc1d1	samčí
mají	mít	k5eAaImIp3nP	mít
barvu	barva	k1gFnSc4	barva
žlutohnědou	žlutohnědý	k2eAgFnSc4d1	žlutohnědá
<g/>
,	,	kIx,	,
samičí	samičí	k2eAgFnSc4d1	samičí
jsou	být	k5eAaImIp3nP	být
zelené	zelené	k1gNnSc4	zelené
<g/>
,	,	kIx,	,
chomáče	chomáč	k1gInPc4	chomáč
jsou	být	k5eAaImIp3nP	být
oddělené	oddělený	k2eAgInPc4d1	oddělený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
rostlině	rostlina	k1gFnSc6	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sekvoj	Sekvoj	k1gInSc1	Sekvoj
kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
konce	konec	k1gInSc2	konec
zimy	zima	k1gFnSc2	zima
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
sudovitá	sudovitý	k2eAgFnSc1d1	sudovitá
až	až	k8xS	až
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
šiška	šiška	k1gFnSc1	šiška
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zraje	zrát	k5eAaImIp3nS	zrát
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Místa	místo	k1gNnSc2	místo
rozšíření	rozšíření	k1gNnSc2	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
sekvojí	sekvoje	k1gFnPc2	sekvoje
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
–	–	k?	–
pobřeží	pobřeží	k1gNnSc1	pobřeží
jižního	jižní	k2eAgInSc2d1	jižní
Oregonu	Oregon	k1gInSc2	Oregon
a	a	k8xC	a
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Červeně	červeně	k6eAd1	červeně
rezavě	rezavě	k6eAd1	rezavě
hnědá	hnědý	k2eAgFnSc1d1	hnědá
barva	barva	k1gFnSc1	barva
mladých	mladý	k2eAgMnPc2d1	mladý
výhonků	výhonek	k1gInPc2	výhonek
dala	dát	k5eAaPmAgFnS	dát
název	název	k1gInSc4	název
obecnému	obecný	k2eAgNnSc3d1	obecné
označení	označení	k1gNnSc3	označení
sekvojí	sekvoje	k1gFnPc2	sekvoje
–	–	k?	–
redwood	redwooda	k1gFnPc2	redwooda
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
červené	červený	k2eAgNnSc1d1	červené
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
i	i	k9	i
Národnímu	národní	k2eAgInSc3d1	národní
parku	park	k1gInSc2	park
Redwood	Redwooda	k1gFnPc2	Redwooda
<g/>
.	.	kIx.	.
</s>
<s>
Sekvoj	Sekvoj	k1gInSc1	Sekvoj
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
stromem	strom	k1gInSc7	strom
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sekvojovec	Sekvojovec	k1gInSc1	Sekvojovec
obrovský	obrovský	k2eAgInSc1d1	obrovský
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
Mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
mezi	mezi	k7c7	mezi
rostlinami	rostlina	k1gFnPc7	rostlina
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Příroda	příroda	k1gFnSc1	příroda
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
obrázky	obrázek	k1gInPc1	obrázek
sekvoje	sekvoje	k1gFnSc2	sekvoje
vždyzelené	vždyzelený	k2eAgInPc1d1	vždyzelený
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sekvoj	sekvoj	k1gInSc1	sekvoj
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Sequoia	Sequoium	k1gNnSc2	Sequoium
sempervirens	sempervirensa	k1gFnPc2	sempervirensa
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
