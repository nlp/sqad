<s>
Sekvoj	Sekvoj	k1gInSc1	Sekvoj
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
(	(	kIx(	(
<g/>
Sequoia	Sequoia	k1gFnSc1	Sequoia
sempervirens	sempervirensa	k1gFnPc2	sempervirensa
D.	D.	kA	D.
Don	Don	k1gMnSc1	Don
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
české	český	k2eAgInPc1d1	český
jazykové	jazykový	k2eAgInPc1d1	jazykový
slovníky	slovník	k1gInPc1	slovník
uvádějí	uvádět	k5eAaImIp3nP	uvádět
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
pádu	pád	k1gInSc6	pád
jako	jako	k9	jako
přípustný	přípustný	k2eAgMnSc1d1	přípustný
pouze	pouze	k6eAd1	pouze
tvar	tvar	k1gInSc4	tvar
sekvoje	sekvoje	k1gFnSc2	sekvoje
<g/>
,	,	kIx,	,
v	v	k7c6	v
botanických	botanický	k2eAgInPc6d1	botanický
slovnících	slovník	k1gInPc6	slovník
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
název	název	k1gInSc1	název
sekvoja	sekvojum	k1gNnSc2	sekvojum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stálezelený	stálezelený	k2eAgInSc1d1	stálezelený
jehličnatý	jehličnatý	k2eAgInSc1d1	jehličnatý
strom	strom	k1gInSc1	strom
s	s	k7c7	s
úzce	úzko	k6eAd1	úzko
kuželovitou	kuželovitý	k2eAgFnSc7d1	kuželovitá
korunou	koruna	k1gFnSc7	koruna
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
až	až	k9	až
116	[number]	k4	116
m	m	kA	m
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
až	až	k9	až
2200	[number]	k4	2200
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
