<s>
Ronnie	Ronnie	k1gFnSc1	Ronnie
James	James	k1gMnSc1	James
Dio	Dio	k1gMnSc1	Dio
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
jako	jako	k8xC	jako
Ronald	Ronald	k1gMnSc1	Ronald
James	James	k1gMnSc1	James
Padavona	Padavona	k1gFnSc1	Padavona
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Portsmouth	Portsmouth	k1gInSc1	Portsmouth
<g/>
,	,	kIx,	,
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
metalový	metalový	k2eAgMnSc1d1	metalový
zpěvák	zpěvák	k1gMnSc1	zpěvák
s	s	k7c7	s
italskými	italský	k2eAgInPc7d1	italský
kořeny	kořen	k1gInPc7	kořen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
účinkováním	účinkování	k1gNnSc7	účinkování
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
Rainbow	Rainbow	k1gMnSc1	Rainbow
<g/>
,	,	kIx,	,
Elf	elf	k1gMnSc1	elf
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
skupinou	skupina	k1gFnSc7	skupina
Dio	Dio	k1gFnSc2	Dio
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
dalším	další	k2eAgInSc7d1	další
projektem	projekt	k1gInSc7	projekt
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
Hear	Heara	k1gFnPc2	Heara
'	'	kIx"	'
<g/>
n	n	k0	n
Aid	Aida	k1gFnPc2	Aida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejschopnějších	schopný	k2eAgMnPc2d3	nejschopnější
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
nejnadanějších	nadaný	k2eAgMnPc2d3	nejnadanější
heavymetalových	heavymetalový	k2eAgMnPc2d1	heavymetalový
zpěváků	zpěvák	k1gMnPc2	zpěvák
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Proslulý	proslulý	k2eAgInSc1d1	proslulý
silným	silný	k2eAgInSc7d1	silný
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
zpopularizováním	zpopularizování	k1gNnSc7	zpopularizování
znamení	znamení	k1gNnSc2	znamení
"	"	kIx"	"
<g/>
rohů	roh	k1gInPc2	roh
ďáblových	ďáblův	k2eAgInPc2d1	ďáblův
<g/>
"	"	kIx"	"
v	v	k7c6	v
metalové	metalový	k2eAgFnSc6d1	metalová
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
byl	být	k5eAaImAgMnS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
Tony	Tony	k1gFnSc3	Tony
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
a	a	k8xC	a
Vinny	vinen	k2eAgMnPc4d1	vinen
Appice	Appic	k1gMnPc4	Appic
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Heaven	Heavna	k1gFnPc2	Heavna
and	and	k?	and
Hell	Hella	k1gFnPc2	Hella
<g/>
.	.	kIx.	.
</s>
<s>
Ronnie	Ronnie	k1gFnPc4	Ronnie
zemřel	zemřít	k5eAaPmAgInS	zemřít
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
například	například	k6eAd1	například
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Králové	Králová	k1gFnSc2	Králová
ro	ro	k?	ro
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
ku	k	k7c3	k
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
sebe	sebe	k3xPyFc4	sebe
samého	samý	k3xTgMnSc4	samý
-	-	kIx~	-
metalového	metalový	k2eAgMnSc4d1	metalový
boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
modlil	modlil	k1gMnSc1	modlil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Festivalu	festival	k1gInSc6	festival
Masters	Masters	k1gInSc1	Masters
of	of	k?	of
Rock	rock	k1gInSc1	rock
2010	[number]	k4	2010
ve	v	k7c6	v
Vizovicích	Vizovice	k1gFnPc6	Vizovice
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
pódium	pódium	k1gNnSc1	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Diova	Diův	k2eAgFnSc1d1	Diova
hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
když	když	k8xS	když
několik	několik	k4yIc1	několik
Cortlandských	Cortlandský	k2eAgMnPc2d1	Cortlandský
hudebníků	hudebník	k1gMnPc2	hudebník
založilo	založit	k5eAaPmAgNnS	založit
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gMnPc2	The
Vegas	Vegas	k1gInSc4	Vegas
Kings	Kingsa	k1gFnPc2	Kingsa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Ronnie	Ronnie	k1gFnPc4	Ronnie
and	and	k?	and
the	the	k?	the
Rumblers	Rumblers	k1gInSc1	Rumblers
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Ronald	Ronald	k1gMnSc1	Ronald
James	James	k1gMnSc1	James
Padavona	Padavona	k1gFnSc1	Padavona
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Ronnie	Ronnie	k1gFnSc1	Ronnie
James	James	k1gMnSc1	James
Dio	Dio	k1gMnSc1	Dio
)	)	kIx)	)
jako	jako	k8xC	jako
basový	basový	k2eAgMnSc1d1	basový
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Billy	Bill	k1gMnPc4	Bill
DeWolfe	DeWolf	k1gInSc5	DeWolf
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Nick	Nick	k1gMnSc1	Nick
Pantas	Pantas	k1gMnSc1	Pantas
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Tom	Tom	k1gMnSc1	Tom
Rogers	Rogers	k1gInSc1	Rogers
a	a	k8xC	a
saxofonista	saxofonista	k1gMnSc1	saxofonista
Jack	Jack	k1gMnSc1	Jack
Musci	Musce	k1gFnSc4	Musce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
několika	několik	k4yIc7	několik
personálními	personální	k2eAgFnPc7d1	personální
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nS	měnit
název	název	k1gInSc1	název
na	na	k7c4	na
Ronnie	Ronnie	k1gFnPc4	Ronnie
and	and	k?	and
the	the	k?	the
Red	Red	k1gFnSc2	Red
Caps	Caps	kA	Caps
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
již	již	k6eAd1	již
Padavona	Padavon	k1gMnSc2	Padavon
nahradil	nahradit	k5eAaPmAgMnS	nahradit
zpěváka	zpěvák	k1gMnSc4	zpěvák
DeWolfa	DeWolf	k1gMnSc4	DeWolf
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
také	také	k9	také
opustil	opustit	k5eAaPmAgMnS	opustit
saxofonista	saxofonista	k1gMnSc1	saxofonista
Jack	Jack	k1gMnSc1	Jack
Musci	Musce	k1gFnSc4	Musce
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
vydala	vydat	k5eAaPmAgFnS	vydat
dva	dva	k4xCgInPc4	dva
singly	singl	k1gInPc4	singl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lover	Lover	k1gInSc1	Lover
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Conquest	Conquest	k1gInSc1	Conquest
<g/>
"	"	kIx"	"
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
"	"	kIx"	"
<g/>
An	An	k1gMnSc1	An
Angel	angel	k1gMnSc1	angel
Is	Is	k1gMnSc1	Is
Missing	Missing	k1gInSc1	Missing
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
I	i	k8xC	i
Say	Say	k1gFnSc2	Say
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
skupina	skupina	k1gFnSc1	skupina
znovu	znovu	k6eAd1	znovu
mění	měnit	k5eAaImIp3nS	měnit
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c4	na
Ronnie	Ronnie	k1gFnPc4	Ronnie
Dio	Dio	k1gFnPc2	Dio
and	and	k?	and
the	the	k?	the
Prophets	Prophets	k1gInSc1	Prophets
<g/>
.	.	kIx.	.
</s>
<s>
Skupině	skupina	k1gFnSc3	skupina
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
vydržel	vydržet	k5eAaPmAgInS	vydržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
několik	několik	k4yIc4	několik
singlů	singl	k1gInPc2	singl
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Electric	Electric	k1gMnSc1	Electric
Elves	Elves	k1gMnSc1	Elves
později	pozdě	k6eAd2	pozdě
jen	jen	k9	jen
The	The	k1gMnSc1	The
Elves	Elves	k1gMnSc1	Elves
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kapelu	kapela	k1gFnSc4	kapela
nejvíce	nejvíce	k6eAd1	nejvíce
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
<g/>
,	,	kIx,	,
Elf	elf	k1gMnSc1	elf
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
zformování	zformování	k1gNnSc6	zformování
Rainbow	Rainbow	k1gFnSc2	Rainbow
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
přizval	přizvat	k5eAaPmAgMnS	přizvat
dřívější	dřívější	k2eAgMnSc1d1	dřívější
kytarista	kytarista	k1gMnSc1	kytarista
skupiny	skupina	k1gFnSc2	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
Ronnieho	Ronnie	k1gMnSc4	Ronnie
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
Elf	elf	k1gMnSc1	elf
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nové	nový	k2eAgFnSc2d1	nová
kapely	kapela	k1gFnSc2	kapela
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rainbow	Rainbow	k1gFnSc2	Rainbow
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Rainbow	Rainbow	k1gMnPc7	Rainbow
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
3	[number]	k4	3
studiová	studiový	k2eAgFnSc1d1	studiová
alba	alba	k1gFnSc1	alba
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Graham	Graham	k1gMnSc1	Graham
Bonnet	Bonnet	k1gMnSc1	Bonnet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
<g/>
/	/	kIx~	/
<g/>
vyhazovu	vyhazov	k1gInSc2	vyhazov
Ozzy	Ozza	k1gFnSc2	Ozza
Osbournea	Osbournea	k1gMnSc1	Osbournea
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
do	do	k7c2	do
heavy	heava	k1gFnSc2	heava
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
kapelou	kapela	k1gFnSc7	kapela
bylo	být	k5eAaImAgNnS	být
Heaven	Heaven	k2eAgMnSc1d1	Heaven
and	and	k?	and
Hell	Hell	k1gMnSc1	Hell
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
odešel	odejít	k5eAaPmAgInS	odejít
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k8xC	i
bubeník	bubeník	k1gMnSc1	bubeník
Vinny	vinen	k2eAgInPc4d1	vinen
Appice	Appice	k1gInPc4	Appice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgInS	vrátit
aby	aby	kYmCp3nP	aby
nahrál	nahrát	k5eAaPmAgInS	nahrát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejtvrdší	tvrdý	k2eAgNnSc4d3	nejtvrdší
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Dehumanizer	Dehumanizer	k1gInSc1	Dehumanizer
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dio	Dio	k1gFnSc2	Dio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbath	k1gMnSc1	Sabbath
založil	založit	k5eAaPmAgMnS	založit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vinny	vinen	k2eAgFnPc1d1	vinna
Appicem	Appic	k1gMnSc7	Appic
a	a	k8xC	a
Jimmy	Jimm	k1gMnPc7	Jimm
Bainem	Baino	k1gNnSc7	Baino
skupinu	skupina	k1gFnSc4	skupina
Dio	Dio	k1gFnSc7	Dio
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
,	,	kIx,	,
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Heaven	Heavna	k1gFnPc2	Heavna
and	and	k?	and
Hell	Hell	k1gMnSc1	Hell
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Vegas	Vegas	k1gInSc1	Vegas
Kings	Kings	k1gInSc1	Kings
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Ronnie	Ronnie	k1gFnSc1	Ronnie
&	&	k?	&
The	The	k1gFnSc2	The
Rumblers	Rumblersa	k1gFnPc2	Rumblersa
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Ronnie	Ronnie	k1gFnSc1	Ronnie
and	and	k?	and
the	the	k?	the
Red	Red	k1gMnSc7	Red
Caps	Caps	kA	Caps
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Ronnie	Ronnie	k1gFnSc1	Ronnie
Dio	Dio	k1gFnSc2	Dio
and	and	k?	and
the	the	k?	the
Prophets	Prophets	k1gInSc1	Prophets
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Electric	Electric	k1gMnSc1	Electric
Elves	Elves	k1gMnSc1	Elves
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Elves	Elves	k1gMnSc1	Elves
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Elf	elf	k1gMnSc1	elf
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Rainbow	Rainbow	k1gFnSc2	Rainbow
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1982,1992	[number]	k4	1982,1992
<g/>
)	)	kIx)	)
Dio	Dio	k1gFnSc2	Dio
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Dio	Dio	k1gFnSc2	Dio
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Heaven	Heaven	k2eAgInSc1d1	Heaven
and	and	k?	and
Hell	Hell	k1gInSc1	Hell
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Dio	Dio	k1gFnSc1	Dio
At	At	k1gFnSc2	At
Domino	domino	k1gNnSc1	domino
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
At	At	k1gMnSc1	At
The	The	k1gMnSc1	The
Beacon	Beacon	k1gMnSc1	Beacon
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
At	At	k1gFnSc2	At
The	The	k1gFnPc2	The
Bank	bank	k1gInSc1	bank
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Elf	elf	k1gMnSc1	elf
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
<g/>
!	!	kIx.	!
</s>
<s>
And	Anda	k1gFnPc2	Anda
My	my	k3xPp1nPc1	my
Soul	Soul	k1gInSc1	Soul
Shall	Shall	k1gMnSc1	Shall
Be	Be	k1gMnSc1	Be
Lifted	Lifted	k1gMnSc1	Lifted
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Carolina	Carolina	k1gFnSc1	Carolina
County	Counta	k1gFnSc2	Counta
Ball	Balla	k1gFnPc2	Balla
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
History	Histor	k1gInPc1	Histor
Of	Of	k1gFnSc2	Of
Syracuse	Syracuse	k1gFnSc2	Syracuse
Music	Musice	k1gFnPc2	Musice
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
VI	VI	kA	VI
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Trying	Trying	k1gInSc4	Trying
to	ten	k3xDgNnSc1	ten
Burn	Burn	k1gNnSc1	Burn
the	the	k?	the
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Gargantuan	Gargantuan	k1gMnSc1	Gargantuan
Elf	elf	k1gMnSc1	elf
Album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
20	[number]	k4	20
Years	Years	k1gInSc1	Years
Of	Of	k1gFnSc2	Of
Syracuse	Syracuse	k1gFnSc2	Syracuse
Rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Elf	elf	k1gMnSc1	elf
Albums	Albums	k1gInSc1	Albums
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rainbow	Rainbow	k1gFnSc7	Rainbow
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Rising	Rising	k1gInSc1	Rising
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
On	on	k3xPp3gInSc1	on
Stage	Stage	k1gInSc1	Stage
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Long	Long	k1gInSc1	Long
Live	Live	k1gNnSc1	Live
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Rollum	k1gNnPc2	Rollum
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Finyl	Finyl	k1gInSc1	Finyl
Vinyl	vinyl	k1gInSc1	vinyl
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Live	Live	k1gNnSc2	Live
in	in	k?	in
Germany	German	k1gInPc7	German
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Rock	rock	k1gInSc1	rock
Profile	profil	k1gInSc5	profil
Volume	volum	k1gInSc5	volum
Two	Two	k1gMnSc6	Two
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
In	In	k1gFnSc2	In
Munich	Munich	k1gInSc1	Munich
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Deutschland	Deutschland	k1gInSc1	Deutschland
Tournee	Tournee	k1gNnSc1	Tournee
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Live	Livus	k1gMnSc5	Livus
In	In	k1gMnSc5	In
Cologne	Cologn	k1gMnSc5	Cologn
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
In	In	k1gMnSc1	In
Düsseldorf	Düsseldorf	k1gMnSc1	Düsseldorf
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
In	In	k1gMnSc1	In
Nurnberg	Nurnberg	k1gMnSc1	Nurnberg
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Polydor	Polydor	k1gMnSc1	Polydor
Years	Years	k1gInSc1	Years
<g/>
:	:	kIx,	:
1975-1986	[number]	k4	1975-1986
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Heaven	Heaven	k2eAgInSc1d1	Heaven
and	and	k?	and
Hell	Hell	k1gInSc1	Hell
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Black	Black	k1gInSc1	Black
And	Anda	k1gFnPc2	Anda
Blue	Blu	k1gFnSc2	Blu
(	(	kIx(	(
<g/>
VHS	VHS	kA	VHS
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Heavy	Heava	k1gFnSc2	Heava
Metal	metal	k1gInSc1	metal
<g/>
:	:	kIx,	:
Music	Music	k1gMnSc1	Music
from	from	k1gMnSc1	from
the	the	k?	the
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
jedna	jeden	k4xCgFnSc1	jeden
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Mob	Mob	k1gMnSc1	Mob
Rules	Rules	k1gMnSc1	Rules
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Mob	Mob	k1gMnSc1	Mob
Rules	Rules	k1gMnSc1	Rules
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
Evil	Evil	k1gInSc1	Evil
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Dehumanizer	Dehumanizra	k1gFnPc2	Dehumanizra
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dio	Dio	k1gFnSc2	Dio
Years	Years	k1gInSc1	Years
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
at	at	k?	at
Hammersmith	Hammersmith	k1gInSc1	Hammersmith
Odeon	odeon	k1gInSc1	odeon
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Rules	Rules	k1gMnSc1	Rules
of	of	k?	of
Hell	Hell	k1gMnSc1	Hell
(	(	kIx(	(
<g/>
Boxed	Boxed	k1gInSc1	Boxed
Set	set	k1gInSc1	set
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Holy	hola	k1gFnSc2	hola
Diver	Diver	k1gInSc1	Diver
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Last	Last	k1gInSc1	Last
in	in	k?	in
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Sacred	Sacred	k1gInSc1	Sacred
Heart	Heart	k1gInSc1	Heart
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Intermission	Intermission	k1gInSc1	Intermission
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Dream	Dream	k1gInSc1	Dream
Evil	Evil	k1gInSc1	Evil
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Lock	Lock	k1gInSc1	Lock
up	up	k?	up
the	the	k?	the
Wolves	Wolves	k1gInSc1	Wolves
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Dio	Dio	k1gFnSc1	Dio
-	-	kIx~	-
Sacred	Sacred	k1gInSc1	Sacred
Heart	Heart	k1gInSc1	Heart
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Diamonds	Diamondsa	k1gFnPc2	Diamondsa
–	–	k?	–
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Dio	Dio	k1gMnSc1	Dio
Strange	Strang	k1gInSc2	Strang
Highways	Highwaysa	k1gFnPc2	Highwaysa
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Angry	Angra	k1gFnSc2	Angra
Machines	Machines	k1gInSc1	Machines
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Inferno	inferno	k1gNnSc1	inferno
-	-	kIx~	-
Last	Last	k1gInSc1	Last
in	in	k?	in
Live	Live	k1gInSc1	Live
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Magica	Magic	k1gInSc2	Magic
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Beast	Beast	k1gMnSc1	Beast
of	of	k?	of
Dio	Dio	k1gMnSc1	Dio
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Killing	Killing	k1gInSc1	Killing
the	the	k?	the
Dragon	Dragon	k1gMnSc1	Dragon
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Master	master	k1gMnSc1	master
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
We	We	k1gFnSc1	We
Rock	rock	k1gInSc1	rock
~	~	kIx~	~
Dio	Dio	k1gMnSc1	Dio
(	(	kIx(	(
<g/>
DVD	DVD	kA	DVD
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Evil	Evil	k1gInSc1	Evil
or	or	k?	or
Divine	Divin	k1gInSc5	Divin
-	-	kIx~	-
Live	Live	k1gNnSc7	Live
In	In	k1gFnSc2	In
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
City	City	k1gFnPc2	City
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Holy	hola	k1gFnSc2	hola
Diver	Diver	k1gInSc1	Diver
-	-	kIx~	-
Live	Live	k1gInSc1	Live
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Live	Liv	k1gFnPc4	Liv
from	from	k6eAd1	from
Radio	radio	k1gNnSc4	radio
City	City	k1gFnSc2	City
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Devil	Devil	k1gMnSc1	Devil
You	You	k1gMnSc1	You
Know	Know	k1gMnSc1	Know
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
