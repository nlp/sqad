<s>
Neil	Neil	k1gMnSc1	Neil
Alden	Aldna	k1gFnPc2	Aldna
Armstrong	Armstrong	k1gMnSc1	Armstrong
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1930	[number]	k4	1930
Wapakoneta	Wapakoneta	k1gFnSc1	Wapakoneta
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
Cincinnati	Cincinnati	k1gFnPc2	Cincinnati
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
a	a	k8xC	a
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
astronautem	astronaut	k1gMnSc7	astronaut
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
pilot	pilot	k1gMnSc1	pilot
v	v	k7c6	v
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bojoval	bojovat	k5eAaImAgMnS	bojovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
letecké	letecký	k2eAgNnSc1d1	letecké
inženýrství	inženýrství	k1gNnSc1	inženýrství
na	na	k7c6	na
Purdueově	Purdueův	k2eAgFnSc6d1	Purdueův
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
zkušebním	zkušební	k2eAgMnSc7d1	zkušební
pilotem	pilot	k1gMnSc7	pilot
Národního	národní	k2eAgInSc2d1	národní
poradního	poradní	k2eAgInSc2d1	poradní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Armstrongově	Armstrongův	k2eAgNnSc6d1	Armstrongovo
leteckém	letecký	k2eAgNnSc6d1	letecké
výzkumném	výzkumný	k2eAgNnSc6d1	výzkumné
středisku	středisko	k1gNnSc6	středisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
při	při	k7c6	při
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
letech	léto	k1gNnPc6	léto
účastnil	účastnit	k5eAaImAgMnS	účastnit
testů	test	k1gInPc2	test
řady	řada	k1gFnSc2	řada
typů	typ	k1gInPc2	typ
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
programu	program	k1gInSc2	program
letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
Man	mana	k1gFnPc2	mana
In	In	k1gFnSc1	In
Space	Space	k1gFnSc1	Space
Soonest	Soonest	k1gFnSc1	Soonest
<g/>
,	,	kIx,	,
testování	testování	k1gNnSc1	testování
raketového	raketový	k2eAgInSc2d1	raketový
letounu	letoun	k1gInSc2	letoun
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
X-	X-	k1gMnSc1	X-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
raketoplánu	raketoplán	k1gInSc2	raketoplán
Boeing	boeing	k1gInSc1	boeing
X-20	X-20	k1gMnSc1	X-20
Dyna-Soar	Dyna-Soar	k1gMnSc1	Dyna-Soar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
oddílu	oddíl	k1gInSc2	oddíl
astronautů	astronaut	k1gMnPc2	astronaut
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
kosmickým	kosmický	k2eAgInSc7d1	kosmický
letem	let	k1gInSc7	let
byla	být	k5eAaImAgFnS	být
mise	mise	k1gFnSc1	mise
Gemini	Gemin	k2eAgMnPc1d1	Gemin
8	[number]	k4	8
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
Gemini	Gemin	k2eAgMnPc1d1	Gemin
8	[number]	k4	8
setkala	setkat	k5eAaPmAgFnS	setkat
a	a	k8xC	a
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
bezpilotním	bezpilotní	k2eAgInSc7d1	bezpilotní
raketovým	raketový	k2eAgInSc7d1	raketový
stupněm	stupeň	k1gInSc7	stupeň
Agena	Ageno	k1gNnSc2	Ageno
TV-	TV-	k1gFnSc2	TV-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
David	David	k1gMnSc1	David
Scott	Scott	k1gMnSc1	Scott
tak	tak	k6eAd1	tak
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
první	první	k4xOgFnSc4	první
spojení	spojení	k1gNnSc2	spojení
dvou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
a	a	k8xC	a
posledním	poslední	k2eAgInSc7d1	poslední
letem	let	k1gInSc7	let
Armstronga	Armstrong	k1gMnSc2	Armstrong
byla	být	k5eAaImAgFnS	být
mise	mise	k1gFnSc1	mise
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Michael	Michael	k1gMnSc1	Michael
Collins	Collins	k1gInSc4	Collins
kroužil	kroužit	k5eAaImAgMnS	kroužit
ve	v	k7c6	v
velitelském	velitelský	k2eAgInSc6d1	velitelský
modulu	modul	k1gInSc6	modul
kolem	kolem	k7c2	kolem
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Buzz	Buzz	k1gMnSc1	Buzz
Aldrin	aldrin	k1gInSc4	aldrin
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
přistáli	přistát	k5eAaPmAgMnP	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tím	ten	k3xDgNnSc7	ten
světovou	světový	k2eAgFnSc4d1	světová
proslulost	proslulost	k1gFnSc1	proslulost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
NASA	NASA	kA	NASA
a	a	k8xC	a
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
na	na	k7c6	na
Cincinnatské	Cincinnatský	k2eAgFnSc6d1	Cincinnatský
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
mluvčím	mluvčí	k1gMnSc7	mluvčí
a	a	k8xC	a
členem	člen	k1gInSc7	člen
správních	správní	k2eAgFnPc2d1	správní
rad	rada	k1gFnPc2	rada
několika	několik	k4yIc2	několik
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
na	na	k7c4	na
komplikace	komplikace	k1gFnPc4	komplikace
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1930	[number]	k4	1930
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Wapakoneta	Wapakoneto	k1gNnSc2	Wapakoneto
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Koenig	Koenig	k1gMnSc1	Koenig
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Viola	Viola	k1gFnSc1	Viola
Louise	Louis	k1gMnSc2	Louis
Engelová	Engelová	k1gFnSc1	Engelová
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
dva	dva	k4xCgMnPc4	dva
mladší	mladý	k2eAgMnPc4d2	mladší
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
,	,	kIx,	,
June	jun	k1gMnSc5	jun
a	a	k8xC	a
Deana	Dean	k1gMnSc4	Dean
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
auditorem	auditor	k1gMnSc7	auditor
pracujícím	pracující	k1gMnPc3	pracující
pro	pro	k7c4	pro
ohijskou	ohijský	k2eAgFnSc4d1	Ohijská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
často	často	k6eAd1	často
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
zapálil	zapálit	k5eAaPmAgMnS	zapálit
pro	pro	k7c4	pro
létání	létání	k1gNnSc4	létání
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
viděl	vidět	k5eAaImAgInS	vidět
letadla	letadlo	k1gNnPc4	letadlo
jako	jako	k8xS	jako
dvouletý	dvouletý	k2eAgInSc4d1	dvouletý
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
letecké	letecký	k2eAgInPc4d1	letecký
závody	závod	k1gInPc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
seděl	sedět	k5eAaImAgInS	sedět
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
při	při	k7c6	při
vyhlídkovém	vyhlídkový	k2eAgInSc6d1	vyhlídkový
letu	let	k1gInSc6	let
ve	v	k7c6	v
Fordu	ford	k1gInSc6	ford
Tri-Motor	Tri-Motor	k1gInSc1	Tri-Motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
vydělal	vydělat	k5eAaPmAgMnS	vydělat
první	první	k4xOgInPc4	první
peníze	peníz	k1gInPc4	peníz
-	-	kIx~	-
za	za	k7c4	za
sekání	sekání	k1gNnSc4	sekání
trávy	tráva	k1gFnSc2	tráva
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
pochůzkami	pochůzka	k1gFnPc7	pochůzka
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
drogerii	drogerie	k1gFnSc4	drogerie
<g/>
.	.	kIx.	.
</s>
<s>
Stavěl	stavět	k5eAaImAgMnS	stavět
letecké	letecký	k2eAgInPc4d1	letecký
modely	model	k1gInPc4	model
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
docházel	docházet	k5eAaImAgInS	docházet
do	do	k7c2	do
leteckého	letecký	k2eAgInSc2d1	letecký
kurzu	kurz	k1gInSc2	kurz
a	a	k8xC	a
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgInS	získat
pilotní	pilotní	k2eAgInSc1d1	pilotní
průkaz	průkaz	k1gInSc1	průkaz
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
řidičské	řidičský	k2eAgNnSc4d1	řidičské
oprávnění	oprávnění	k1gNnSc4	oprávnění
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
aktivním	aktivní	k2eAgMnSc7d1	aktivní
skautem	skaut	k1gMnSc7	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
letu	let	k1gInSc6	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
poslal	poslat	k5eAaPmAgMnS	poslat
pozdrav	pozdrav	k1gInSc4	pozdrav
zrovna	zrovna	k6eAd1	zrovna
probíhajícímu	probíhající	k2eAgNnSc3d1	probíhající
národnímu	národní	k2eAgNnSc3d1	národní
setkání	setkání	k1gNnSc3	setkání
skautů	skaut	k1gMnPc2	skaut
<g/>
;	;	kIx,	;
skautský	skautský	k2eAgInSc1d1	skautský
emblém	emblém	k1gInSc1	emblém
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
osobními	osobní	k2eAgFnPc7d1	osobní
věcmi	věc	k1gFnPc7	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
do	do	k7c2	do
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Purdueově	Purdueův	k2eAgFnSc6d1	Purdueův
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
Purdue	Purdue	k1gInSc1	Purdue
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
letecké	letecký	k2eAgNnSc4d1	letecké
inženýrství	inženýrství	k1gNnSc4	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
teprve	teprve	k6eAd1	teprve
druhým	druhý	k4xOgInSc7	druhý
členem	člen	k1gInSc7	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Massachusettský	massachusettský	k2eAgInSc4d1	massachusettský
technologický	technologický	k2eAgInSc4d1	technologický
institut	institut	k1gInSc4	institut
(	(	kIx(	(
<g/>
MIT	MIT	kA	MIT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeden	jeden	k4xCgMnSc1	jeden
jeho	jeho	k3xOp3gMnSc1	jeho
známý	známý	k1gMnSc1	známý
-	-	kIx~	-
absolvent	absolvent	k1gMnSc1	absolvent
institutu	institut	k1gInSc2	institut
-	-	kIx~	-
ho	on	k3xPp3gNnSc4	on
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
vzdělání	vzdělání	k1gNnSc4	vzdělání
nemusí	muset	k5eNaImIp3nS	muset
odcházet	odcházet	k5eAaImF	odcházet
do	do	k7c2	do
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Purdueově	Purdueův	k2eAgFnSc6d1	Purdueův
univerzitě	univerzita	k1gFnSc6	univerzita
létal	létat	k5eAaImAgMnS	létat
v	v	k7c6	v
tamním	tamní	k2eAgInSc6d1	tamní
leteckém	letecký	k2eAgInSc6d1	letecký
klubu	klub	k1gInSc6	klub
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ročníku	ročník	k1gInSc6	ročník
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
koncertním	koncertní	k2eAgInSc6d1	koncertní
souboru	soubor	k1gInSc6	soubor
na	na	k7c4	na
barytonový	barytonový	k2eAgInSc4d1	barytonový
roh	roh	k1gInSc4	roh
<g/>
;	;	kIx,	;
k	k	k7c3	k
souboru	soubor	k1gInSc3	soubor
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
i	i	k9	i
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
NASA	NASA	kA	NASA
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
mu	on	k3xPp3gMnSc3	on
nemalý	malý	k2eNgInSc4d1	nemalý
finanční	finanční	k2eAgInSc4d1	finanční
dar	dar	k1gInSc4	dar
<g/>
.	.	kIx.	.
</s>
<s>
Armstrongovo	Armstrongův	k2eAgNnSc1d1	Armstrongovo
vzdělání	vzdělání	k1gNnSc1	vzdělání
financovalo	financovat	k5eAaBmAgNnS	financovat
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Hollowayova	Hollowayův	k2eAgInSc2d1	Hollowayův
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úhradu	úhrada	k1gFnSc4	úhrada
nákladů	náklad	k1gInPc2	náklad
studia	studio	k1gNnSc2	studio
student	student	k1gMnSc1	student
mezi	mezi	k7c7	mezi
druhým	druhý	k4xOgInSc7	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc7	třetí
ročníkem	ročník	k1gInSc7	ročník
školy	škola	k1gFnSc2	škola
odsloužit	odsloužit	k5eAaPmF	odsloužit
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
v	v	k7c6	v
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
ho	on	k3xPp3gMnSc4	on
povolalo	povolat	k5eAaPmAgNnS	povolat
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Pensacola	Pensacola	k1gFnSc1	Pensacola
prošel	projít	k5eAaPmAgInS	projít
osmnáctiměsíčním	osmnáctiměsíční	k2eAgInSc7d1	osmnáctiměsíční
leteckým	letecký	k2eAgInSc7d1	letecký
výcvikem	výcvik	k1gInSc7	výcvik
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgInSc2	který
získal	získat	k5eAaPmAgInS	získat
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
námořního	námořní	k2eAgMnSc2d1	námořní
letce	letec	k1gMnSc2	letec
<g/>
,	,	kIx,	,
schopného	schopný	k2eAgMnSc2d1	schopný
sloužit	sloužit	k5eAaImF	sloužit
na	na	k7c6	na
letadlových	letadlový	k2eAgFnPc6d1	letadlová
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
jeho	jeho	k3xOp3gFnPc6	jeho
dvacátých	dvacátý	k4xOgFnPc6	dvacátý
narozeninách	narozeniny	k1gFnPc6	narozeniny
<g/>
,	,	kIx,	,
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
Armstronga	Armstrong	k1gMnSc2	Armstrong
vyrozumělo	vyrozumět	k5eAaPmAgNnS	vyrozumět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
kvalifikovaný	kvalifikovaný	k2eAgMnSc1d1	kvalifikovaný
námořní	námořní	k2eAgMnSc1d1	námořní
letec	letec	k1gMnSc1	letec
<g/>
.	.	kIx.	.
</s>
<s>
Přidělen	přidělen	k2eAgInSc1d1	přidělen
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
San	San	k1gFnSc6	San
Diegu	Dieg	k1gInSc2	Dieg
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
51	[number]	k4	51
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc2d1	stíhací
peruti	peruť	k1gFnSc2	peruť
(	(	kIx(	(
<g/>
VF-	VF-	k1gFnSc1	VF-
<g/>
51	[number]	k4	51
<g/>
)	)	kIx)	)
vyzbrojené	vyzbrojený	k2eAgInPc1d1	vyzbrojený
proudovými	proudový	k2eAgFnPc7d1	proudová
F9F	F9F	k1gFnPc7	F9F
Panther	Panthra	k1gFnPc2	Panthra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pantheru	Panthero	k1gNnSc6	Panthero
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1951	[number]	k4	1951
poprvé	poprvé	k6eAd1	poprvé
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
letadlové	letadlový	k2eAgFnSc6d1	letadlová
lodi	loď	k1gFnSc6	loď
USS	USS	kA	USS
Essex	Essex	k1gInSc1	Essex
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
eskadra	eskadra	k1gFnSc1	eskadra
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodě	loď	k1gFnSc2	loď
Essex	Essex	k1gInSc1	Essex
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
do	do	k7c2	do
korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
bojovému	bojový	k2eAgInSc3d1	bojový
letu	let	k1gInSc3	let
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1951	[number]	k4	1951
<g/>
;	;	kIx,	;
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
průzkumné	průzkumný	k2eAgNnSc4d1	průzkumné
letadlo	letadlo	k1gNnSc4	letadlo
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
Sŏ	Sŏ	k1gFnSc2	Sŏ
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
skladů	sklad	k1gInPc2	sklad
západně	západně	k6eAd1	západně
od	od	k7c2	od
Wonsanu	Wonsan	k1gInSc2	Wonsan
v	v	k7c6	v
nízké	nízký	k2eAgFnSc3d1	nízká
výšce	výška	k1gFnSc3	výška
jeho	jeho	k3xOp3gNnSc4	jeho
letadlo	letadlo	k1gNnSc4	letadlo
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
palba	palba	k1gFnSc1	palba
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
protiletadlové	protiletadlový	k2eAgFnSc2d1	protiletadlová
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
vrazil	vrazit	k5eAaPmAgMnS	vrazit
křídlem	křídlo	k1gNnSc7	křídlo
do	do	k7c2	do
asi	asi	k9	asi
šestimetrové	šestimetrový	k2eAgFnSc2d1	šestimetrová
tyče	tyč	k1gFnSc2	tyč
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
křídle	křídlo	k1gNnSc6	křídlo
prorazila	prorazit	k5eAaPmAgFnS	prorazit
asi	asi	k9	asi
metrovou	metrový	k2eAgFnSc4d1	metrová
trhlinu	trhlina	k1gFnSc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vrátit	vrátit	k5eAaPmF	vrátit
nad	nad	k7c4	nad
vlastní	vlastní	k2eAgNnSc4d1	vlastní
území	území	k1gNnSc4	území
a	a	k8xC	a
katapultovat	katapultovat	k5eAaBmF	katapultovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nasazení	nasazení	k1gNnSc2	nasazení
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
provedl	provést	k5eAaPmAgInS	provést
78	[number]	k4	78
bojových	bojový	k2eAgInPc2d1	bojový
letů	let	k1gInPc2	let
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
121	[number]	k4	121
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Obdržel	obdržet	k5eAaPmAgInS	obdržet
Leteckou	letecký	k2eAgFnSc4d1	letecká
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
dvacet	dvacet	k4xCc4	dvacet
bojových	bojový	k2eAgInPc2d1	bojový
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
hvězdu	hvězda	k1gFnSc4	hvězda
za	za	k7c4	za
dalších	další	k2eAgFnPc2d1	další
dvacet	dvacet	k4xCc4	dvacet
a	a	k8xC	a
Medaili	medaile	k1gFnSc4	medaile
Za	za	k7c4	za
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
odešel	odejít	k5eAaPmAgInS	odejít
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
mladšího	mladý	k2eAgMnSc2d2	mladší
poručíka	poručík	k1gMnSc2	poručík
(	(	kIx(	(
<g/>
Lieutenant	Lieutenant	k1gMnSc1	Lieutenant
(	(	kIx(	(
<g/>
junior	junior	k1gMnSc1	junior
grade	grad	k1gInSc5	grad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LTJG	LTJG	kA	LTJG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
,	,	kIx,	,
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
služby	služba	k1gFnSc2	služba
u	u	k7c2	u
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
dokončil	dokončit	k5eAaPmAgMnS	dokončit
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
bakalář	bakalář	k1gMnSc1	bakalář
(	(	kIx(	(
<g/>
Bachelor	Bachelor	k1gMnSc1	Bachelor
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Jižní	jižní	k2eAgFnSc2d1	jižní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
(	(	kIx(	(
<g/>
University	universita	k1gFnPc1	universita
of	of	k?	of
Southern	Southern	k1gNnSc1	Southern
California	Californium	k1gNnSc2	Californium
<g/>
)	)	kIx)	)
magisterské	magisterský	k2eAgNnSc1d1	magisterské
studium	studium	k1gNnSc1	studium
(	(	kIx(	(
<g/>
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Master	master	k1gMnSc1	master
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
leteckém	letecký	k2eAgNnSc6d1	letecké
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
studentkou	studentka	k1gFnSc7	studentka
ekonomie	ekonomie	k1gFnSc2	ekonomie
Janet	Janeta	k1gFnPc2	Janeta
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Shearonovou	Shearonův	k2eAgFnSc7d1	Shearonův
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Janet	Janet	k1gInSc1	Janet
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
a	a	k8xC	a
nedokončila	dokončit	k5eNaPmAgFnS	dokončit
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
později	pozdě	k6eAd2	pozdě
litovala	litovat	k5eAaImAgFnS	litovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
civilním	civilní	k2eAgInSc7d1	civilní
zkušebním	zkušební	k2eAgInSc7d1	zkušební
pilotem	pilot	k1gInSc7	pilot
Národního	národní	k2eAgInSc2d1	národní
poradního	poradní	k2eAgInSc2d1	poradní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
(	(	kIx(	(
<g/>
NACA	NACA	kA	NACA
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zažádal	zažádat	k5eAaPmAgMnS	zažádat
si	se	k3xPyFc3	se
o	o	k7c4	o
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
v	v	k7c4	v
High-Speed	High-Speed	k1gInSc4	High-Speed
Flight	Flight	k2eAgInSc4d1	Flight
Station	station	k1gInSc4	station
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Armstrongovo	Armstrongův	k2eAgNnSc1d1	Armstrongovo
letecké	letecký	k2eAgNnSc1d1	letecké
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
středisko	středisko	k1gNnSc1	středisko
<g/>
)	)	kIx)	)
na	na	k7c6	na
Edwardsově	Edwardsův	k2eAgFnSc6d1	Edwardsova
základně	základna	k1gFnSc6	základna
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
zde	zde	k6eAd1	zde
volné	volný	k2eAgNnSc4d1	volné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
středisku	středisko	k1gNnSc6	středisko
NACA	NACA	kA	NACA
<g/>
,	,	kIx,	,
Lewisově	Lewisův	k2eAgFnSc6d1	Lewisova
laboratoři	laboratoř	k1gFnSc6	laboratoř
leteckého	letecký	k2eAgInSc2d1	letecký
pohonu	pohon	k1gInSc2	pohon
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
od	od	k7c2	od
března	březen	k1gInSc2	březen
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1955	[number]	k4	1955
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Edwardsovu	Edwardsův	k2eAgFnSc4d1	Edwardsova
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Edwardsově	Edwardsův	k2eAgFnSc6d1	Edwardsova
základně	základna	k1gFnSc6	základna
zprvu	zprvu	k6eAd1	zprvu
pilotoval	pilotovat	k5eAaImAgInS	pilotovat
doprovodná	doprovodný	k2eAgNnPc4d1	doprovodné
letadla	letadlo	k1gNnPc4	letadlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
sledovala	sledovat	k5eAaImAgFnS	sledovat
experimentální	experimentální	k2eAgInPc4d1	experimentální
letouny	letoun	k1gInPc4	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Létal	létat	k5eAaImAgMnS	létat
i	i	k9	i
na	na	k7c6	na
upravených	upravený	k2eAgInPc6d1	upravený
bombardérech	bombardér	k1gInPc6	bombardér
<g/>
,	,	kIx,	,
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
letů	let	k1gInPc2	let
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
letecká	letecký	k2eAgFnSc1d1	letecká
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1956	[number]	k4	1956
měla	mít	k5eAaImAgFnS	mít
posádka	posádka	k1gFnSc1	posádka
Boeingu	boeing	k1gInSc2	boeing
B-29	B-29	k1gFnSc2	B-29
Superfortress	Superfortressa	k1gFnPc2	Superfortressa
<g/>
,	,	kIx,	,
s	s	k7c7	s
Armstrongem	Armstrong	k1gMnSc7	Armstrong
jako	jako	k9	jako
druhým	druhý	k4xOgMnSc7	druhý
pilotem	pilot	k1gMnSc7	pilot
<g/>
,	,	kIx,	,
vypustit	vypustit	k5eAaPmF	vypustit
podvěšený	podvěšený	k2eAgInSc4d1	podvěšený
experimentální	experimentální	k2eAgInSc4d1	experimentální
proudový	proudový	k2eAgInSc4d1	proudový
letoun	letoun	k1gInSc4	letoun
Douglas	Douglas	k1gMnSc1	Douglas
D-558-2	D-558-2	k1gMnSc1	D-558-2
Skyrocket	Skyrocket	k1gMnSc1	Skyrocket
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
9,1	[number]	k4	9,1
km	km	kA	km
<g/>
,	,	kIx,	,
motor	motor	k1gInSc1	motor
č.	č.	k?	č.
4	[number]	k4	4
Boeingu	boeing	k1gInSc2	boeing
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Přistát	přistát	k5eAaImF	přistát
s	s	k7c7	s
podvěšeným	podvěšený	k2eAgMnSc7d1	podvěšený
Skyrocketem	Skyrocket	k1gMnSc7	Skyrocket
nemohli	moct	k5eNaImAgMnP	moct
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
rychlost	rychlost	k1gFnSc4	rychlost
(	(	kIx(	(
<g/>
338	[number]	k4	338
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vypuštění	vypuštění	k1gNnSc3	vypuštění
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Boeingu	boeing	k1gInSc2	boeing
Stan	stan	k1gInSc1	stan
Butchart	Butchart	k1gInSc1	Butchart
proto	proto	k8xC	proto
stočil	stočit	k5eAaPmAgMnS	stočit
letadlo	letadlo	k1gNnSc4	letadlo
do	do	k7c2	do
klesání	klesání	k1gNnSc2	klesání
<g/>
,	,	kIx,	,
když	když	k8xS	když
získali	získat	k5eAaPmAgMnP	získat
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
vypustili	vypustit	k5eAaPmAgMnP	vypustit
Skyrocket	Skyrocket	k1gInSc4	Skyrocket
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
vrtule	vrtule	k1gFnSc1	vrtule
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
motoru	motor	k1gInSc2	motor
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
úlomky	úlomek	k1gInPc4	úlomek
poškodily	poškodit	k5eAaPmAgInP	poškodit
motory	motor	k1gInPc1	motor
č.	č.	k?	č.
3	[number]	k4	3
a	a	k8xC	a
trochu	trochu	k6eAd1	trochu
i	i	k9	i
č.	č.	k?	č.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Butchart	Butchart	k1gInSc1	Butchart
a	a	k8xC	a
Armstrong	Armstrong	k1gMnSc1	Armstrong
museli	muset	k5eAaImAgMnP	muset
vypnout	vypnout	k5eAaPmF	vypnout
poškozený	poškozený	k2eAgInSc4d1	poškozený
motor	motor	k1gInSc4	motor
č.	č.	k?	č.
3	[number]	k4	3
a	a	k8xC	a
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
momentu	moment	k1gInSc2	moment
motor	motor	k1gInSc1	motor
č.	č.	k?	č.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
motorem	motor	k1gInSc7	motor
v	v	k7c6	v
chodu	chod	k1gInSc6	chod
poté	poté	k6eAd1	poté
pomalu	pomalu	k6eAd1	pomalu
krouživým	krouživý	k2eAgInSc7d1	krouživý
letem	let	k1gInSc7	let
snížili	snížit	k5eAaPmAgMnP	snížit
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
bezpečně	bezpečně	k6eAd1	bezpečně
přistáli	přistát	k5eAaImAgMnP	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgInSc2	první
letu	let	k1gInSc2	let
v	v	k7c6	v
raketovém	raketový	k2eAgNnSc6d1	raketové
letadle	letadlo	k1gNnSc6	letadlo
se	se	k3xPyFc4	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
dočkal	dočkat	k5eAaPmAgMnS	dočkat
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c4	v
Bell	bell	k1gInSc4	bell
X-1B	X-1B	k1gFnSc2	X-1B
vylétl	vylétnout	k5eAaPmAgMnS	vylétnout
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
18,3	[number]	k4	18,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přistání	přistání	k1gNnSc6	přistání
ulomil	ulomit	k5eAaPmAgMnS	ulomit
příďové	příďový	k2eAgNnSc4d1	příďové
kolo	kolo	k1gNnSc4	kolo
podvozku	podvozek	k1gInSc2	podvozek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
X-1	X-1	k1gFnSc2	X-1
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
stala	stát	k5eAaPmAgFnS	stát
asi	asi	k9	asi
tucetkrát	tucetkrát	k6eAd1	tucetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
příčinou	příčina	k1gFnSc7	příčina
byla	být	k5eAaImAgFnS	být
vada	vada	k1gFnSc1	vada
konstrukce	konstrukce	k1gFnSc2	konstrukce
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
testování	testování	k1gNnSc4	testování
raketového	raketový	k2eAgInSc2d1	raketový
letounu	letoun	k1gInSc2	letoun
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
X-	X-	k1gMnSc1	X-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1960	[number]	k4	1960
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1962	[number]	k4	1962
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
s	s	k7c7	s
letouny	letoun	k1gMnPc7	letoun
X-15	X-15	k1gFnSc1	X-15
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc1	sedm
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
X-15-3	X-15-3	k1gFnPc2	X-15-3
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
výšky	výška	k1gFnSc2	výška
63,2	[number]	k4	63,2
km	km	kA	km
a	a	k8xC	a
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
X-15-1	X-15-1	k1gMnSc1	X-15-1
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
rychlosti	rychlost	k1gFnSc3	rychlost
Mach	Mach	k1gMnSc1	Mach
5,74	[number]	k4	5,74
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
6	[number]	k4	6
615	[number]	k4	615
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
byl	být	k5eAaImAgMnS	být
zapleten	zapleten	k2eAgInSc4d1	zapleten
do	do	k7c2	do
několika	několik	k4yIc2	několik
incidentů	incident	k1gInPc2	incident
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
Edwardsově	Edwardsův	k2eAgFnSc6d1	Edwardsova
základně	základna	k1gFnSc6	základna
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
tamního	tamní	k2eAgInSc2d1	tamní
folklóru	folklór	k1gInSc2	folklór
a	a	k8xC	a
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
pilotů	pilot	k1gMnPc2	pilot
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
nastal	nastat	k5eAaPmAgMnS	nastat
během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
šestého	šestý	k4xOgInSc2	šestý
letu	let	k1gInSc2	let
s	s	k7c7	s
X-15	X-15	k1gFnSc7	X-15
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
testoval	testovat	k5eAaImAgInS	testovat
automatický	automatický	k2eAgInSc1d1	automatický
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
systém	systém	k1gInSc1	systém
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Stoupal	stoupat	k5eAaImAgMnS	stoupat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
více	hodně	k6eAd2	hodně
než	než	k8xS	než
63	[number]	k4	63
km	km	kA	km
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
jaké	jaký	k3yQgFnSc2	jaký
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
před	před	k7c7	před
kosmickými	kosmický	k2eAgNnPc7d1	kosmické
lety	léto	k1gNnPc7	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
sestupu	sestup	k1gInSc2	sestup
příď	příď	k1gFnSc4	příď
letadla	letadlo	k1gNnSc2	letadlo
zvedl	zvednout	k5eAaPmAgMnS	zvednout
příliš	příliš	k6eAd1	příliš
a	a	k8xC	a
X-15	X-15	k1gMnSc1	X-15
se	se	k3xPyFc4	se
odrazil	odrazit	k5eAaPmAgMnS	odrazit
od	od	k7c2	od
atmosféry	atmosféra	k1gFnSc2	atmosféra
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
43	[number]	k4	43
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
domovským	domovský	k2eAgNnSc7d1	domovské
letištěm	letiště	k1gNnSc7	letiště
prolétl	prolétnout	k5eAaPmAgInS	prolétnout
trojnásobkem	trojnásobek	k1gInSc7	trojnásobek
rychlosti	rychlost	k1gFnSc2	rychlost
zvuku	zvuk	k1gInSc2	zvuk
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
30	[number]	k4	30
km	km	kA	km
<g/>
,	,	kIx,	,
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	s	k7c7	s
64	[number]	k4	64
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Edwards	Edwardsa	k1gFnPc2	Edwardsa
(	(	kIx(	(
<g/>
legendy	legenda	k1gFnPc1	legenda
základny	základna	k1gFnSc2	základna
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
doletěl	doletět	k5eAaPmAgInS	doletět
až	až	k9	až
ke	k	k7c3	k
stadiónu	stadión	k1gInSc3	stadión
Rose	Rose	k1gMnSc1	Rose
Bowl	Bowl	k1gMnSc1	Bowl
v	v	k7c6	v
Pasadeně	Pasadena	k1gFnSc6	Pasadena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dostatečném	dostatečný	k2eAgInSc6d1	dostatečný
sestupu	sestup	k1gInSc6	sestup
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
Edwards	Edwards	k1gInSc4	Edwards
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jen	jen	k9	jen
taktak	taktak	k6eAd1	taktak
dokázal	dokázat	k5eAaPmAgMnS	dokázat
přistát	přistát	k5eAaImF	přistát
bez	bez	k7c2	bez
nárazu	náraz	k1gInSc2	náraz
do	do	k7c2	do
juk	juka	k1gFnPc2	juka
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
let	let	k1gInSc1	let
X-15	X-15	k1gFnSc2	X-15
jak	jak	k8xC	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
Armstrong	Armstrong	k1gMnSc1	Armstrong
účastníkem	účastník	k1gMnSc7	účastník
jiného	jiný	k2eAgInSc2d1	jiný
incidentu	incident	k1gInSc2	incident
<g/>
,	,	kIx,	,
při	při	k7c6	při
jediné	jediný	k2eAgFnSc6d1	jediná
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
letěl	letět	k5eAaImAgMnS	letět
s	s	k7c7	s
Chuckem	Chucko	k1gNnSc7	Chucko
Yeagerem	Yeager	k1gMnSc7	Yeager
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
s	s	k7c7	s
Lockheedem	Lockheed	k1gInSc7	Lockheed
T-33	T-33	k1gFnSc2	T-33
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
vhodnost	vhodnost	k1gFnSc4	vhodnost
vyschlého	vyschlý	k2eAgNnSc2d1	vyschlé
jezera	jezero	k1gNnSc2	jezero
Smith	Smith	k1gMnSc1	Smith
Ranch	Ranch	k1gMnSc1	Ranch
Dry	Dry	k1gMnSc7	Dry
Lake	Lake	k1gNnSc2	Lake
jako	jako	k8xC	jako
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
nouzového	nouzový	k2eAgNnSc2d1	nouzové
přistání	přistání	k1gNnSc2	přistání
v	v	k7c6	v
programu	program	k1gInSc6	program
X-	X-	k1gFnSc2	X-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
Yeager	Yeager	k1gMnSc1	Yeager
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezero	jezero	k1gNnSc1	jezero
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
nedávných	dávný	k2eNgInPc6d1	nedávný
deštích	dešť	k1gInPc6	dešť
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
pro	pro	k7c4	pro
přistání	přistání	k1gNnSc4	přistání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Armstrong	Armstrong	k1gMnSc1	Armstrong
trval	trvat	k5eAaImAgMnS	trvat
na	na	k7c6	na
letu	let	k1gInSc6	let
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
letmé	letmý	k2eAgNnSc4d1	letmé
přistání	přistání	k1gNnSc4	přistání
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
kola	kola	k1gFnSc1	kola
zabořila	zabořit	k5eAaPmAgFnS	zabořit
do	do	k7c2	do
bahna	bahno	k1gNnSc2	bahno
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Armstrongovy	Armstrongův	k2eAgFnSc2d1	Armstrongova
verze	verze	k1gFnSc2	verze
událostí	událost	k1gFnPc2	událost
se	se	k3xPyFc4	se
Yeager	Yeager	k1gMnSc1	Yeager
nikdy	nikdy	k6eAd1	nikdy
nepokoušel	pokoušet	k5eNaImAgMnS	pokoušet
rozmluvit	rozmluvit	k5eAaPmF	rozmluvit
mu	on	k3xPp3gMnSc3	on
let	léto	k1gNnPc2	léto
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
přistáli	přistát	k5eAaImAgMnP	přistát
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Yeager	Yeager	k1gMnSc1	Yeager
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
to	ten	k3xDgNnSc4	ten
zkusili	zkusit	k5eAaPmAgMnP	zkusit
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
trochu	trochu	k6eAd1	trochu
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgNnSc6	druhý
přistání	přistání	k1gNnSc6	přistání
se	se	k3xPyFc4	se
přilepili	přilepit	k5eAaPmAgMnP	přilepit
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
a	a	k8xC	a
Yeager	Yeager	k1gInSc1	Yeager
podle	podle	k7c2	podle
Armstronga	Armstrong	k1gMnSc2	Armstrong
reagoval	reagovat	k5eAaBmAgMnS	reagovat
záchvatem	záchvat	k1gInSc7	záchvat
smíchu	smích	k1gInSc2	smích
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zkušebních	zkušební	k2eAgMnPc2d1	zkušební
pilotů	pilot	k1gMnPc2	pilot
na	na	k7c6	na
Edwardsově	Edwardsův	k2eAgFnSc6d1	Edwardsova
základně	základna	k1gFnSc6	základna
chválilo	chválit	k5eAaImAgNnS	chválit
Armstrongovy	Armstrongův	k2eAgFnPc4d1	Armstrongova
inženýrské	inženýrský	k2eAgFnPc4d1	inženýrská
dovednosti	dovednost	k1gFnPc4	dovednost
<g/>
.	.	kIx.	.
</s>
<s>
Milt	Milt	k1gMnSc1	Milt
Thompson	Thompson	k1gMnSc1	Thompson
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
technicky	technicky	k6eAd1	technicky
nejschopnější	schopný	k2eAgInPc1d3	nejschopnější
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
pilotů	pilot	k1gInPc2	pilot
X-	X-	k1gFnSc4	X-
<g/>
15	[number]	k4	15
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Dana	Dana	k1gFnSc1	Dana
o	o	k7c6	o
Armstrongovi	Armstrong	k1gMnSc6	Armstrong
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
mysl	mysl	k1gFnSc1	mysl
vstřebávala	vstřebávat	k5eAaImAgFnS	vstřebávat
informace	informace	k1gFnPc4	informace
jako	jako	k8xC	jako
houba	houba	k1gFnSc1	houba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vojenští	vojenský	k2eAgMnPc1d1	vojenský
piloti	pilot	k1gMnPc1	pilot
měli	mít	k5eAaImAgMnP	mít
tendenci	tendence	k1gFnSc4	tendence
mít	mít	k5eAaImF	mít
jiný	jiný	k2eAgInSc4d1	jiný
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Yeager	Yeager	k1gInSc1	Yeager
a	a	k8xC	a
Pete	Pete	k1gInSc1	Pete
Knight	Knighta	k1gFnPc2	Knighta
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
inženýrské	inženýrský	k2eAgInPc4d1	inženýrský
diplomy	diplom	k1gInPc4	diplom
<g/>
.	.	kIx.	.
</s>
<s>
Knight	Knight	k1gMnSc1	Knight
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
piloti	pilot	k1gMnPc1	pilot
<g/>
-	-	kIx~	-
<g/>
inženýři	inženýr	k1gMnPc1	inženýr
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
dostávali	dostávat	k5eAaImAgMnP	dostávat
do	do	k7c2	do
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
mechanickém	mechanický	k2eAgInSc6d1	mechanický
přístupu	přístup	k1gInSc6	přístup
neměli	mít	k5eNaImAgMnP	mít
přirozený	přirozený	k2eAgInSc4d1	přirozený
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
létání	létání	k1gNnSc4	létání
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1962	[number]	k4	1962
se	se	k3xPyFc4	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
stal	stát	k5eAaPmAgMnS	stát
účastníkem	účastník	k1gMnSc7	účastník
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
folklóru	folklór	k1gInSc2	folklór
Edwardsovy	Edwardsův	k2eAgFnSc2d1	Edwardsova
základny	základna	k1gFnSc2	základna
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Nelliská	Nelliský	k2eAgFnSc1d1	Nelliský
aféra	aféra	k1gFnSc1	aféra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
v	v	k7c4	v
Lockheed	Lockheed	k1gInSc4	Lockheed
F-104	F-104	k1gMnSc1	F-104
Starfighter	Starfighter	k1gMnSc1	Starfighter
obhlédnout	obhlédnout	k5eAaPmF	obhlédnout
další	další	k2eAgFnSc4d1	další
plochu	plocha	k1gFnSc4	plocha
pro	pro	k7c4	pro
nouzové	nouzový	k2eAgNnSc4d1	nouzové
přistání	přistání	k1gNnSc4	přistání
<g/>
,	,	kIx,	,
Delamar	Delamar	k1gInSc4	Delamar
Dry	Dry	k1gFnSc2	Dry
Lake	Lak	k1gFnSc2	Lak
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
při	při	k7c6	při
přistávání	přistávání	k1gNnSc6	přistávání
podcenil	podcenit	k5eAaPmAgMnS	podcenit
svou	svůj	k3xOyFgFnSc4	svůj
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
neuvědomil	uvědomit	k5eNaPmAgInS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
úplně	úplně	k6eAd1	úplně
vysunutý	vysunutý	k2eAgInSc1d1	vysunutý
podvozek	podvozek	k1gInSc1	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dosednutí	dosednutí	k1gNnSc6	dosednutí
se	se	k3xPyFc4	se
podvozek	podvozek	k1gInSc1	podvozek
začal	začít	k5eAaPmAgInS	začít
zatahovat	zatahovat	k5eAaImF	zatahovat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přerušit	přerušit	k5eAaPmF	přerušit
přistání	přistání	k1gNnSc4	přistání
a	a	k8xC	a
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
poškozeným	poškozený	k2eAgInSc7d1	poškozený
podvozkem	podvozek	k1gInSc7	podvozek
a	a	k8xC	a
nefunkční	funkční	k2eNgFnSc7d1	nefunkční
vysílačkou	vysílačka	k1gFnSc7	vysílačka
<g/>
.	.	kIx.	.
</s>
<s>
Přistál	přistát	k5eAaPmAgMnS	přistát
na	na	k7c6	na
blízké	blízký	k2eAgFnSc6d1	blízká
Nellisově	Nellisův	k2eAgFnSc6d1	Nellisův
letecké	letecký	k2eAgFnSc6d1	letecká
základně	základna	k1gFnSc6	základna
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
poškodil	poškodit	k5eAaPmAgInS	poškodit
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
ho	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
dopravit	dopravit	k5eAaPmF	dopravit
Milt	Milt	k2eAgInSc1d1	Milt
Thompson	Thompson	k1gInSc1	Thompson
v	v	k7c6	v
dvoumístném	dvoumístný	k2eAgInSc6d1	dvoumístný
F-	F-	k1gMnSc2	F-
<g/>
104	[number]	k4	104
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
pilotoval	pilotovat	k5eAaImAgInS	pilotovat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgNnSc1d1	jiné
vícemístné	vícemístný	k2eAgNnSc1d1	vícemístné
letadlo	letadlo	k1gNnSc1	letadlo
zrovna	zrovna	k6eAd1	zrovna
nebylo	být	k5eNaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Thompson	Thompson	k1gMnSc1	Thompson
se	se	k3xPyFc4	se
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
potížemi	potíž	k1gFnPc7	potíž
dostal	dostat	k5eAaPmAgMnS	dostat
nad	nad	k7c4	nad
Nellisovu	Nellisův	k2eAgFnSc4d1	Nellisův
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
silnému	silný	k2eAgInSc3d1	silný
bočnímu	boční	k2eAgInSc3d1	boční
větru	vítr	k1gInSc3	vítr
přistál	přistát	k5eAaPmAgInS	přistát
příliš	příliš	k6eAd1	příliš
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
praskla	prasknout	k5eAaPmAgFnS	prasknout
mu	on	k3xPp3gMnSc3	on
levá	levý	k2eAgFnSc1d1	levá
pneumatika	pneumatika	k1gFnSc1	pneumatika
a	a	k8xC	a
dráha	dráha	k1gFnSc1	dráha
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
znovu	znovu	k6eAd1	znovu
uklízet	uklízet	k5eAaImF	uklízet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
piloty	pilot	k1gInPc4	pilot
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
kolega	kolega	k1gMnSc1	kolega
Bill	Bill	k1gMnSc1	Bill
Dana	Dana	k1gFnSc1	Dana
v	v	k7c6	v
T-33	T-33	k1gFnSc6	T-33
Shooting	Shooting	k1gInSc1	Shooting
Star	Star	kA	Star
<g/>
,	,	kIx,	,
když	když	k8xS	když
nelliský	nelliský	k1gMnSc1	nelliský
řídící	řídící	k2eAgFnSc2d1	řídící
letů	let	k1gInPc2	let
viděl	vidět	k5eAaImAgMnS	vidět
jeho	jeho	k3xOp3gMnPc3	jeho
(	(	kIx(	(
<g/>
příliš	příliš	k6eAd1	příliš
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
<g/>
)	)	kIx)	)
přistání	přistání	k1gNnSc4	přistání
<g/>
,	,	kIx,	,
radši	rád	k6eAd2	rád
zajistil	zajistit	k5eAaPmAgMnS	zajistit
zkušebním	zkušební	k2eAgMnSc7d1	zkušební
pilotům	pilot	k1gMnPc3	pilot
z	z	k7c2	z
Edwards	Edwardsa	k1gFnPc2	Edwardsa
odvoz	odvoz	k1gInSc4	odvoz
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
zkušební	zkušební	k2eAgInSc1d1	zkušební
pilot	pilot	k1gInSc1	pilot
se	se	k3xPyFc4	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
účastnil	účastnit	k5eAaImAgMnS	účastnit
testů	test	k1gInPc2	test
letadel	letadlo	k1gNnPc2	letadlo
F-100A	F-100A	k1gMnSc7	F-100A
a	a	k8xC	a
F-	F-	k1gFnSc7	F-
<g/>
100	[number]	k4	100
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
F-101	F-101	k1gMnSc3	F-101
a	a	k8xC	a
F-	F-	k1gMnSc3	F-
<g/>
104	[number]	k4	104
<g/>
A.	A.	kA	A.
Létal	létat	k5eAaImAgMnS	létat
i	i	k9	i
na	na	k7c6	na
F-	F-	k1gFnSc6	F-
<g/>
105	[number]	k4	105
<g/>
,	,	kIx,	,
F-	F-	k1gFnSc1	F-
<g/>
106	[number]	k4	106
<g/>
,	,	kIx,	,
B-	B-	k1gFnSc1	B-
<g/>
47	[number]	k4	47
<g/>
,	,	kIx,	,
KC-	KC-	k1gFnSc1	KC-
<g/>
135	[number]	k4	135
<g/>
,	,	kIx,	,
experimentálních	experimentální	k2eAgFnPc2d1	experimentální
X-1B	X-1B	k1gFnPc2	X-1B
a	a	k8xC	a
X-	X-	k1gFnSc7	X-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
z	z	k7c2	z
High-Speed	High-Speed	k1gInSc4	High-Speed
Flight	Flight	k2eAgInSc1d1	Flight
Station	station	k1gInSc1	station
odešel	odejít	k5eAaPmAgInS	odejít
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
nalétáno	nalétán	k2eAgNnSc4d1	nalétáno
přes	přes	k7c4	přes
2450	[number]	k4	2450
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
létal	létat	k5eAaImAgInS	létat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgNnPc6	dva
stech	sto	k4xCgNnPc6	sto
typech	typ	k1gInPc6	typ
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
,	,	kIx,	,
kosmických	kosmický	k2eAgFnPc2d1	kosmická
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
létacích	létací	k2eAgInPc2d1	létací
aparátů	aparát	k1gInPc2	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
chvílích	chvíle	k1gFnPc6	chvíle
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
bezmotorovému	bezmotorový	k2eAgNnSc3d1	bezmotorové
létání	létání	k1gNnSc3	létání
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
také	také	k9	také
vedl	vést	k5eAaImAgInS	vést
oddíl	oddíl	k1gInSc1	oddíl
skautů	skaut	k1gMnPc2	skaut
při	při	k7c6	při
místní	místní	k2eAgFnSc6d1	místní
metodistické	metodistický	k2eAgFnSc6d1	metodistická
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
však	však	k9	však
označoval	označovat	k5eAaImAgInS	označovat
za	za	k7c4	za
deistu	deista	k1gMnSc4	deista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1958	[number]	k4	1958
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
mezi	mezi	k7c4	mezi
devět	devět	k4xCc4	devět
budoucích	budoucí	k2eAgMnPc2d1	budoucí
astronautů	astronaut	k1gMnPc2	astronaut
programu	program	k1gInSc2	program
letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
Man	mana	k1gFnPc2	mana
In	In	k1gMnSc1	In
Space	Space	k1gMnSc1	Space
Soonest	Soonest	k1gMnSc1	Soonest
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgInS	být
brzký	brzký	k2eAgInSc1d1	brzký
-	-	kIx~	-
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
-	-	kIx~	-
pilotovaný	pilotovaný	k2eAgInSc1d1	pilotovaný
kosmický	kosmický	k2eAgInSc1d1	kosmický
let	let	k1gInSc1	let
v	v	k7c4	v
malé	malý	k2eAgFnPc4d1	malá
jednomístné	jednomístný	k2eAgFnPc4d1	jednomístná
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1958	[number]	k4	1958
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
Národní	národní	k2eAgInSc1d1	národní
poradní	poradní	k2eAgInSc1d1	poradní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
(	(	kIx(	(
<g/>
NACA	NACA	kA	NACA
<g/>
)	)	kIx)	)
reorganizován	reorganizovat	k5eAaBmNgInS	reorganizovat
v	v	k7c4	v
Národní	národní	k2eAgInSc4d1	národní
úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
prezident	prezident	k1gMnSc1	prezident
uložil	uložit	k5eAaPmAgInS	uložit
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
první	první	k4xOgInPc4	první
pilotované	pilotovaný	k2eAgInPc4d1	pilotovaný
kosmické	kosmický	k2eAgInPc4d1	kosmický
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
proto	proto	k8xC	proto
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1958	[number]	k4	1958
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
Man	mana	k1gFnPc2	mana
In	In	k1gFnPc2	In
Space	Space	k1gMnSc2	Space
Soonest	Soonest	k1gFnSc4	Soonest
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1960	[number]	k4	1960
patřil	patřit	k5eAaImAgMnS	patřit
Armstrong	Armstrong	k1gMnSc1	Armstrong
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
sedmi	sedm	k4xCc2	sedm
astronautů	astronaut	k1gMnPc2	astronaut
zařazených	zařazený	k2eAgMnPc2d1	zařazený
do	do	k7c2	do
programu	program	k1gInSc2	program
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vyvinutí	vyvinutí	k1gNnSc1	vyvinutí
kosmoplánu	kosmoplán	k1gInSc2	kosmoplán
Boeing	boeing	k1gInSc1	boeing
X-20	X-20	k1gMnSc1	X-20
Dyna-Soar	Dyna-Soar	k1gMnSc1	Dyna-Soar
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
se	se	k3xPyFc4	se
při	při	k7c6	při
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
upravenými	upravený	k2eAgFnPc7d1	upravená
stíhačkami	stíhačka	k1gFnPc7	stíhačka
F-102A	F-102A	k1gFnSc2	F-102A
Delta	delta	k1gNnSc2	delta
Dagger	Dagger	k1gMnSc1	Dagger
a	a	k8xC	a
F5D	F5D	k1gMnSc1	F5D
Skylancer	Skylancer	k1gMnSc1	Skylancer
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
určování	určování	k1gNnSc6	určování
způsobu	způsob	k1gInSc2	způsob
přistání	přistání	k1gNnSc2	přistání
kosmoplánu	kosmoplán	k1gInSc2	kosmoplán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
perspektivnější	perspektivní	k2eAgInSc4d2	perspektivnější
však	však	k8xC	však
považoval	považovat	k5eAaImAgMnS	považovat
kosmický	kosmický	k2eAgInSc4d1	kosmický
program	program	k1gInSc4	program
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1962	[number]	k4	1962
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
oddílu	oddíl	k1gInSc2	oddíl
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodování	rozhodování	k1gNnSc1	rozhodování
nebylo	být	k5eNaImAgNnS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
X-15	X-15	k1gFnSc2	X-15
měl	mít	k5eAaImAgInS	mít
reálnou	reálný	k2eAgFnSc4d1	reálná
šanci	šance	k1gFnSc4	šance
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
hlavním	hlavní	k2eAgMnSc7d1	hlavní
pilotem	pilot	k1gMnSc7	pilot
a	a	k8xC	a
X-20	X-20	k1gFnSc7	X-20
se	se	k3xPyFc4	se
z	z	k7c2	z
"	"	kIx"	"
<g/>
papírové	papírový	k2eAgFnSc2d1	papírová
<g/>
"	"	kIx"	"
lodě	loď	k1gFnSc2	loď
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kosmických	kosmický	k2eAgInPc2d1	kosmický
letů	let	k1gInPc2	let
bylo	být	k5eAaImAgNnS	být
zase	zase	k9	zase
podle	podle	k7c2	podle
Armstronga	Armstrong	k1gMnSc2	Armstrong
menší	malý	k2eAgNnSc4d2	menší
riziko	riziko	k1gNnSc4	riziko
<g/>
,	,	kIx,	,
v	v	k7c6	v
kosmonautice	kosmonautika	k1gFnSc6	kosmonautika
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
záloh	záloha	k1gFnPc2	záloha
a	a	k8xC	a
analýz	analýza	k1gFnPc2	analýza
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
zkušební	zkušební	k2eAgMnPc1d1	zkušební
piloti	pilot	k1gMnPc1	pilot
létali	létat	k5eAaImAgMnP	létat
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
možností	možnost	k1gFnPc2	možnost
svých	svůj	k3xOyFgInPc2	svůj
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
se	se	k3xPyFc4	se
opozdil	opozdit	k5eAaPmAgInS	opozdit
týden	týden	k1gInSc1	týden
po	po	k7c6	po
posledním	poslední	k2eAgInSc6d1	poslední
termínu	termín	k1gInSc6	termín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
štěstí	štěstí	k1gNnSc4	štěstí
-	-	kIx~	-
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
předtím	předtím	k6eAd1	předtím
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
Edwardsově	Edwardsův	k2eAgFnSc6d1	Edwardsova
základně	základna	k1gFnSc6	základna
<g/>
,	,	kIx,	,
přesunul	přesunout	k5eAaPmAgMnS	přesunout
v	v	k7c6	v
nestřeženém	střežený	k2eNgInSc6d1	nestřežený
okamžiku	okamžik	k1gInSc6	okamžik
jeho	jeho	k3xOp3gFnSc4	jeho
přihlášku	přihláška	k1gFnSc4	přihláška
na	na	k7c4	na
správnou	správný	k2eAgFnSc4d1	správná
hromádku	hromádka	k1gFnSc4	hromádka
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
prošel	projít	k5eAaPmAgInS	projít
lékařskými	lékařský	k2eAgInPc7d1	lékařský
testy	test	k1gInPc7	test
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
později	pozdě	k6eAd2	pozdě
popsal	popsat	k5eAaPmAgInS	popsat
jako	jako	k9	jako
obtížné	obtížný	k2eAgNnSc1d1	obtížné
a	a	k8xC	a
občas	občas	k6eAd1	občas
zdánlivě	zdánlivě	k6eAd1	zdánlivě
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
astronautů	astronaut	k1gMnPc2	astronaut
druhého	druhý	k4xOgInSc2	druhý
výběru	výběr	k1gInSc2	výběr
NASA	NASA	kA	NASA
(	(	kIx(	(
<g/>
tiskem	tisek	k1gMnSc7	tisek
nazvaných	nazvaný	k2eAgFnPc6d1	nazvaná
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
devítka	devítka	k1gFnSc1	devítka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
-	-	kIx~	-
sedmičlenná	sedmičlenný	k2eAgFnSc1d1	sedmičlenná
-	-	kIx~	-
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
členy	člen	k1gMnPc7	člen
oddílu	oddíl	k1gInSc2	oddíl
NASA	NASA	kA	NASA
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Elliottem	Elliotto	k1gNnSc7	Elliotto
See	See	k1gFnSc2	See
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
bývalým	bývalý	k2eAgMnSc7d1	bývalý
námořním	námořní	k2eAgMnSc7d1	námořní
letcem	letec	k1gMnSc7	letec
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
prvními	první	k4xOgMnPc7	první
civilisty	civilista	k1gMnSc2	civilista
mezi	mezi	k7c7	mezi
astronauty	astronaut	k1gMnPc7	astronaut
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
postihla	postihnout	k5eAaPmAgFnS	postihnout
Armstronga	Armstrong	k1gMnSc4	Armstrong
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
těžká	těžký	k2eAgFnSc1d1	těžká
ztráta	ztráta	k1gFnSc1	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Janet	Janet	k1gMnSc1	Janet
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
-	-	kIx~	-
Erica	Erica	k1gMnSc1	Erica
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karen	Karen	k1gInSc1	Karen
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Marka	Marek	k1gMnSc4	Marek
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1961	[number]	k4	1961
lékaři	lékař	k1gMnPc1	lékař
u	u	k7c2	u
Karen	Karna	k1gFnPc2	Karna
diagnostikovali	diagnostikovat	k5eAaBmAgMnP	diagnostikovat
nádor	nádor	k1gInSc4	nádor
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
léčba	léčba	k1gFnSc1	léčba
neuspěla	uspět	k5eNaPmAgFnS	uspět
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1962	[number]	k4	1962
Karen	Karno	k1gNnPc2	Karno
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Program	program	k1gInSc1	program
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1965	[number]	k4	1965
NASA	NASA	kA	NASA
oznámila	oznámit	k5eAaPmAgFnS	oznámit
jeho	jeho	k3xOp3gNnSc4	jeho
jmenování	jmenování	k1gNnSc4	jmenování
velícím	velící	k2eAgMnSc7d1	velící
pilotem	pilot	k1gMnSc7	pilot
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
velitelem	velitel	k1gMnSc7	velitel
lodi	loď	k1gFnSc6	loď
<g/>
)	)	kIx)	)
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
mise	mise	k1gFnSc2	mise
Gemini	Gemin	k2eAgMnPc1d1	Gemin
5	[number]	k4	5
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
kolegou	kolega	k1gMnSc7	kolega
v	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
byl	být	k5eAaImAgMnS	být
Elliot	Elliot	k1gMnSc1	Elliot
See	See	k1gMnSc1	See
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc4	let
Gemini	Gemin	k1gMnPc1	Gemin
5	[number]	k4	5
úspěšně	úspěšně	k6eAd1	úspěšně
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1965	[number]	k4	1965
NASA	NASA	kA	NASA
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
složení	složení	k1gNnSc4	složení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
a	a	k8xC	a
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
letu	let	k1gInSc2	let
Gemini	Gemin	k1gMnPc1	Gemin
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
posádku	posádka	k1gFnSc4	posádka
tvořil	tvořit	k5eAaImAgMnS	tvořit
Armstrong	Armstrong	k1gMnSc1	Armstrong
(	(	kIx(	(
<g/>
velící	velící	k2eAgMnSc1d1	velící
pilot	pilot	k1gMnSc1	pilot
<g/>
)	)	kIx)	)
a	a	k8xC	a
David	David	k1gMnSc1	David
Scott	Scott	k1gMnSc1	Scott
(	(	kIx(	(
<g/>
pilot	pilot	k1gMnSc1	pilot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
záložní	záložní	k2eAgFnSc4d1	záložní
posádku	posádka	k1gFnSc4	posádka
Charles	Charles	k1gMnSc1	Charles
Conrad	Conrada	k1gFnPc2	Conrada
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
komplexnější	komplexní	k2eAgInSc1d2	komplexnější
než	než	k8xS	než
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
<g/>
,	,	kIx,	,
plánovalo	plánovat	k5eAaImAgNnS	plánovat
se	se	k3xPyFc4	se
uskutečnění	uskutečnění	k1gNnSc1	uskutečnění
prvního	první	k4xOgNnSc2	první
setkání	setkání	k1gNnSc2	setkání
a	a	k8xC	a
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
vesmírným	vesmírný	k2eAgNnSc7d1	vesmírné
tělesem	těleso	k1gNnSc7	těleso
-	-	kIx~	-
raketovým	raketový	k2eAgInSc7d1	raketový
stupněm	stupeň	k1gInSc7	stupeň
Agena	Ageen	k2eAgFnSc1d1	Agena
TV-8	TV-8	k1gFnSc1	TV-8
-	-	kIx~	-
a	a	k8xC	a
poté	poté	k6eAd1	poté
měl	mít	k5eAaImAgMnS	mít
Scott	Scott	k1gMnSc1	Scott
provést	provést	k5eAaPmF	provést
druhý	druhý	k4xOgInSc4	druhý
americký	americký	k2eAgInSc4d1	americký
výstup	výstup	k1gInSc4	výstup
do	do	k7c2	do
otevřeného	otevřený	k2eAgInSc2d1	otevřený
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
měl	mít	k5eAaImAgInS	mít
trvat	trvat	k5eAaImF	trvat
75	[number]	k4	75
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
55	[number]	k4	55
oběhů	oběh	k1gInPc2	oběh
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Agena	Agena	k1gFnSc1	Agena
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
v	v	k7c4	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
Gemini	Gemin	k2eAgMnPc1d1	Gemin
8	[number]	k4	8
s	s	k7c7	s
astronauty	astronaut	k1gMnPc7	astronaut
o	o	k7c4	o
necelé	celý	k2eNgFnPc4d1	necelá
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
a	a	k8xC	a
spojení	spojení	k1gNnSc1	spojení
bylo	být	k5eAaImAgNnS	být
úspěšně	úspěšně	k6eAd1	úspěšně
završeno	završit	k5eAaPmNgNnS	završit
6,5	[number]	k4	6,5
hodiny	hodina	k1gFnSc2	hodina
po	po	k7c6	po
startu	start	k1gInSc6	start
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
soulodí	soulodí	k1gNnSc1	soulodí
prolétalo	prolétat	k5eAaImAgNnS	prolétat
nad	nad	k7c7	nad
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
bez	bez	k7c2	bez
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
řídícím	řídící	k2eAgNnSc7d1	řídící
střediskem	středisko	k1gNnSc7	středisko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
otáčet	otáčet	k5eAaImF	otáčet
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
rotaci	rotace	k1gFnSc3	rotace
zastavit	zastavit	k5eAaPmF	zastavit
pomocí	pomocí	k7c2	pomocí
orbitálního	orbitální	k2eAgInSc2d1	orbitální
manévrovacího	manévrovací	k2eAgInSc2d1	manévrovací
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
OAMS	OAMS	kA	OAMS
<g/>
)	)	kIx)	)
Gemini	Gemin	k2eAgMnPc1d1	Gemin
a	a	k8xC	a
vypnutí	vypnutí	k1gNnSc6	vypnutí
systémů	systém	k1gInPc2	systém
orientace	orientace	k1gFnSc2	orientace
Ageny	Agena	k1gFnSc2	Agena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
přechodným	přechodný	k2eAgInSc7d1	přechodný
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
rotace	rotace	k1gFnSc2	rotace
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
tělesa	těleso	k1gNnSc2	těleso
rozpojit	rozpojit	k5eAaPmF	rozpojit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
rotace	rotace	k1gFnSc1	rotace
Gemini	Gemin	k2eAgMnPc1d1	Gemin
začala	začít	k5eAaPmAgNnP	začít
růst	růst	k1gInSc4	růst
až	až	k9	až
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
jedné	jeden	k4xCgFnSc2	jeden
otáčky	otáčka	k1gFnSc2	otáčka
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
přetížení	přetížení	k1gNnSc1	přetížení
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
únosnosti	únosnost	k1gFnSc2	únosnost
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
se	s	k7c7	s
Scottem	Scott	k1gMnSc7	Scott
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
aktivovat	aktivovat	k5eAaBmF	aktivovat
motory	motor	k1gInPc4	motor
systému	systém	k1gInSc2	systém
řízení	řízení	k1gNnSc2	řízení
lodi	loď	k1gFnSc2	loď
během	během	k7c2	během
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
(	(	kIx(	(
<g/>
RCS	RCS	kA	RCS
<g/>
)	)	kIx)	)
a	a	k8xC	a
stabilizovali	stabilizovat	k5eAaBmAgMnP	stabilizovat
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zkoušením	zkoušení	k1gNnSc7	zkoušení
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
problém	problém	k1gInSc4	problém
způsobila	způsobit	k5eAaPmAgFnS	způsobit
zaseknutá	zaseknutý	k2eAgFnSc1d1	zaseknutá
tryska	tryska	k1gFnSc1	tryska
č.	č.	k?	č.
8	[number]	k4	8
systému	systém	k1gInSc2	systém
OAMS	OAMS	kA	OAMS
<g/>
.	.	kIx.	.
</s>
<s>
Řídící	řídící	k2eAgNnSc1d1	řídící
středisko	středisko	k1gNnSc1	středisko
nařídilo	nařídit	k5eAaPmAgNnS	nařídit
zrušení	zrušení	k1gNnSc4	zrušení
dalšího	další	k2eAgInSc2d1	další
programu	program	k1gInSc2	program
-	-	kIx~	-
Scottova	Scottův	k2eAgInSc2d1	Scottův
výstupu	výstup	k1gInSc2	výstup
-	-	kIx~	-
a	a	k8xC	a
předčasné	předčasný	k2eAgNnSc1d1	předčasné
přistání	přistání	k1gNnSc1	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Přistání	přistání	k1gNnSc1	přistání
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
hladce	hladko	k6eAd1	hladko
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
asi	asi	k9	asi
800	[number]	k4	800
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Okinawy	Okinawa	k1gFnSc2	Okinawa
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
trval	trvat	k5eAaImAgInS	trvat
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
41	[number]	k4	41
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letu	let	k1gInSc6	let
byl	být	k5eAaImAgMnS	být
Armstrong	Armstrong	k1gMnSc1	Armstrong
deprimovaný	deprimovaný	k2eAgMnSc1d1	deprimovaný
ze	z	k7c2	z
zkrácení	zkrácení	k1gNnSc2	zkrácení
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
nedosažení	dosažený	k2eNgMnPc1d1	dosažený
většiny	většina	k1gFnSc2	většina
cílů	cíl	k1gInPc2	cíl
mise	mise	k1gFnSc2	mise
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
Scottova	Scottův	k2eAgInSc2d1	Scottův
výstupu	výstup	k1gInSc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velícím	velící	k2eAgMnSc7d1	velící
pilotem	pilot	k1gMnSc7	pilot
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
letu	let	k1gInSc2	let
Gemini	Gemin	k1gMnPc1	Gemin
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
Armstrong	Armstrong	k1gMnSc1	Armstrong
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
v	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
<g/>
,	,	kIx,	,
nováčka	nováček	k1gMnSc2	nováček
Williama	William	k1gMnSc2	William
Anderse	Anderse	k1gFnSc2	Anderse
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
září	září	k1gNnSc6	září
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
capcom	capcom	k1gInSc1	capcom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
spojař	spojař	k1gMnSc1	spojař
komunikující	komunikující	k2eAgFnSc2d1	komunikující
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
<g/>
)	)	kIx)	)
v	v	k7c6	v
řídícím	řídící	k2eAgNnSc6d1	řídící
středisku	středisko	k1gNnSc6	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letu	let	k1gInSc6	let
byl	být	k5eAaImAgMnS	být
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
spoluastronaut	spoluastronaut	k1gMnSc1	spoluastronaut
Richard	Richard	k1gMnSc1	Richard
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
ředitele	ředitel	k1gMnSc2	ředitel
Střediska	středisko	k1gNnSc2	středisko
pilotovaných	pilotovaný	k2eAgInPc2d1	pilotovaný
kosmických	kosmický	k2eAgInPc2d1	kosmický
letů	let	k1gInPc2	let
NASA	NASA	kA	NASA
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
George	Georg	k1gMnSc4	Georg
Low	Low	k1gMnSc4	Low
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
s	s	k7c7	s
manželkami	manželka	k1gFnPc7	manželka
vysláni	vyslán	k2eAgMnPc1d1	vyslán
na	na	k7c4	na
několikatýdenní	několikatýdenní	k2eAgNnSc4d1	několikatýdenní
propagační	propagační	k2eAgNnSc4d1	propagační
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
navštívili	navštívit	k5eAaPmAgMnP	navštívit
čtrnáct	čtrnáct	k4xCc1	čtrnáct
měst	město	k1gNnPc2	město
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
Apollo	Apollo	k1gMnSc1	Apollo
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1967	[number]	k4	1967
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
Apolla	Apollo	k1gNnSc2	Apollo
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
změně	změna	k1gFnSc6	změna
obsahu	obsah	k1gInSc2	obsah
letů	let	k1gInPc2	let
Apolla	Apollo	k1gNnPc4	Apollo
8	[number]	k4	8
a	a	k8xC	a
9	[number]	k4	9
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
lunárních	lunární	k2eAgInPc2d1	lunární
modulů	modul	k1gInPc2	modul
přešel	přejít	k5eAaPmAgInS	přejít
(	(	kIx(	(
<g/>
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
velitele	velitel	k1gMnSc2	velitel
zálohy	záloha	k1gFnSc2	záloha
pro	pro	k7c4	pro
Apollo	Apollo	k1gNnSc4	Apollo
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
systému	systém	k1gInSc2	systém
nasazování	nasazování	k1gNnSc4	nasazování
posádek	posádka	k1gFnPc2	posádka
měla	mít	k5eAaImAgFnS	mít
záloha	záloha	k1gFnSc1	záloha
Apolla	Apollo	k1gNnSc2	Apollo
8	[number]	k4	8
letět	letět	k5eAaImF	letět
v	v	k7c6	v
Apollu	Apollo	k1gNnSc6	Apollo
11	[number]	k4	11
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
přistání	přistání	k1gNnSc1	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
zůstal	zůstat	k5eAaPmAgMnS	zůstat
velitelem	velitel	k1gMnSc7	velitel
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc7	jeho
kolegy	kolega	k1gMnPc7	kolega
byli	být	k5eAaImAgMnP	být
pilot	pilot	k1gInSc4	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Fred	Fred	k1gMnSc1	Fred
Haise	Haise	k1gFnSc1	Haise
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
nahrazený	nahrazený	k2eAgInSc4d1	nahrazený
Michaelem	Michael	k1gMnSc7	Michael
Collinsem	Collins	k1gMnSc7	Collins
<g/>
,	,	kIx,	,
a	a	k8xC	a
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
Edwin	Edwin	k2eAgInSc1d1	Edwin
"	"	kIx"	"
<g/>
Buzz	Buzz	k1gInSc1	Buzz
<g/>
"	"	kIx"	"
Aldrin	aldrin	k1gInSc1	aldrin
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
novináři	novinář	k1gMnPc1	novinář
i	i	k8xC	i
někteří	některý	k3yIgMnPc1	některý
pracovníci	pracovník	k1gMnPc1	pracovník
NASA	NASA	kA	NASA
soudili	soudit	k5eAaImAgMnP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvním	první	k4xOgMnSc7	první
mužem	muž	k1gMnSc7	muž
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
bude	být	k5eAaImBp3nS	být
Aldrin	aldrin	k1gInSc1	aldrin
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analogie	analogie	k1gFnSc2	analogie
s	s	k7c7	s
loděmi	loď	k1gFnPc7	loď
Gemini	Gemin	k2eAgMnPc1d1	Gemin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výstupy	výstup	k1gInPc1	výstup
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
prováděl	provádět	k5eAaImAgMnS	provádět
pilot	pilot	k1gMnSc1	pilot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1969	[number]	k4	1969
se	se	k3xPyFc4	se
však	však	k9	však
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
pracovníci	pracovník	k1gMnPc1	pracovník
NASA	NASA	kA	NASA
Deke	Deke	k1gFnSc1	Deke
Slayton	Slayton	k1gInSc1	Slayton
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Low	Low	k1gMnSc1	Low
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Gilruth	Gilruth	k1gMnSc1	Gilruth
a	a	k8xC	a
Chris	Chris	k1gInSc1	Chris
Kraft	Kraft	k1gMnSc1	Kraft
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
Armstrong	Armstrong	k1gMnSc1	Armstrong
bude	být	k5eAaImBp3nS	být
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
skromnosti	skromnost	k1gFnSc3	skromnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
zdůvodnili	zdůvodnit	k5eAaPmAgMnP	zdůvodnit
Armstrongovo	Armstrongův	k2eAgNnSc4d1	Armstrongovo
prvenství	prvenství	k1gNnSc4	prvenství
konstrukcí	konstrukce	k1gFnPc2	konstrukce
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
-	-	kIx~	-
venkovní	venkovní	k2eAgInSc1d1	venkovní
průlez	průlez	k1gInSc1	průlez
se	se	k3xPyFc4	se
otvíral	otvírat	k5eAaImAgInS	otvírat
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vpravo	vpravo	k6eAd1	vpravo
sedícího	sedící	k2eAgMnSc4d1	sedící
pilota	pilot	k1gMnSc4	pilot
by	by	kYmCp3nS	by
proto	proto	k6eAd1	proto
v	v	k7c6	v
těsném	těsný	k2eAgInSc6d1	těsný
modulu	modul	k1gInSc6	modul
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
vylézt	vylézt	k5eAaPmF	vylézt
před	před	k7c7	před
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Slayton	Slayton	k1gInSc1	Slayton
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
by	by	kYmCp3nS	by
velitel	velitel	k1gMnSc1	velitel
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
první	první	k4xOgInSc4	první
i	i	k8xC	i
z	z	k7c2	z
protokolárních	protokolární	k2eAgInPc2d1	protokolární
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
čtveřice	čtveřice	k1gFnSc1	čtveřice
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
důsledky	důsledek	k1gInPc7	důsledek
umístění	umístění	k1gNnSc2	umístění
průlezu	průlez	k1gInSc2	průlez
si	se	k3xPyFc3	se
neuvědomovala	uvědomovat	k5eNaImAgFnS	uvědomovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
trénování	trénování	k1gNnSc6	trénování
přistání	přistání	k1gNnSc2	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
s	s	k7c7	s
letovým	letový	k2eAgInSc7d1	letový
trenažérem	trenažér	k1gInSc7	trenažér
LLRV	LLRV	kA	LLRV
(	(	kIx(	(
<g/>
Lunar	Lunara	k1gFnPc2	Lunara
Landing	Landing	k1gInSc1	Landing
Research	Research	k1gMnSc1	Research
Vehicles	Vehicles	k1gMnSc1	Vehicles
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1968	[number]	k4	1968
k	k	k7c3	k
selhání	selhání	k1gNnSc3	selhání
tohoto	tento	k3xDgInSc2	tento
trenažéru	trenažér	k1gInSc2	trenažér
a	a	k8xC	a
jen	jen	k9	jen
pohotová	pohotový	k2eAgFnSc1d1	pohotová
katapultace	katapultace	k1gFnSc1	katapultace
zachránila	zachránit	k5eAaPmAgFnS	zachránit
Armstrongovi	Armstrongův	k2eAgMnPc1d1	Armstrongův
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
přestálé	přestálý	k2eAgNnSc4d1	přestálé
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
Armstrong	Armstrong	k1gMnSc1	Armstrong
trval	trvat	k5eAaImAgMnS	trvat
na	na	k7c6	na
výcviku	výcvik	k1gInSc6	výcvik
s	s	k7c7	s
LLRV	LLRV	kA	LLRV
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
je	být	k5eAaImIp3nS	být
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
začala	začít	k5eAaPmAgFnS	začít
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
v	v	k7c4	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
UTC	UTC	kA	UTC
startem	start	k1gInSc7	start
z	z	k7c2	z
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
střediska	středisko	k1gNnSc2	středisko
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
necelých	celý	k2eNgInPc6d1	necelý
dvou	dva	k4xCgInPc6	dva
obězích	oběh	k1gInPc6	oběh
Země	zem	k1gFnSc2	zem
třetí	třetí	k4xOgInSc1	třetí
stupeň	stupeň	k1gInSc1	stupeň
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
Saturn	Saturn	k1gMnSc1	Saturn
V	V	kA	V
uvedl	uvést	k5eAaPmAgMnS	uvést
Apollo	Apollo	k1gMnSc1	Apollo
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
loď	loď	k1gFnSc4	loď
-	-	kIx~	-
s	s	k7c7	s
velitelským	velitelský	k2eAgInSc7d1	velitelský
modulem	modul	k1gInSc7	modul
pojmenovaným	pojmenovaný	k2eAgInSc7d1	pojmenovaný
Columbia	Columbia	k1gFnSc1	Columbia
-	-	kIx~	-
se	se	k3xPyFc4	se
odpojili	odpojit	k5eAaPmAgMnP	odpojit
<g/>
,	,	kIx,	,
otočili	otočit	k5eAaPmAgMnP	otočit
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
lunárnímu	lunární	k2eAgInSc3d1	lunární
modulu	modul	k1gInSc3	modul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nesl	nést	k5eAaImAgInS	nést
jméno	jméno	k1gNnSc4	jméno
Eagle	Eagle	k1gFnSc2	Eagle
(	(	kIx(	(
<g/>
Orel	Orel	k1gMnSc1	Orel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odhodili	odhodit	k5eAaPmAgMnP	odhodit
nepotřebný	potřebný	k2eNgInSc4d1	nepotřebný
třetí	třetí	k4xOgInSc4	třetí
stupeň	stupeň	k1gInSc4	stupeň
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
Apollo	Apollo	k1gMnSc1	Apollo
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
malou	malá	k1gFnSc7	malá
Gemini	Gemin	k1gMnPc1	Gemin
relativně	relativně	k6eAd1	relativně
prostorná	prostorný	k2eAgFnSc1d1	prostorná
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
právě	právě	k9	právě
možnost	možnost	k1gFnSc4	možnost
pohybu	pohyb	k1gInSc2	pohyb
po	po	k7c6	po
lodi	loď	k1gFnSc6	loď
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
posádky	posádka	k1gFnSc2	posádka
netrpěl	trpět	k5eNaImAgMnS	trpět
nevolnostmi	nevolnost	k1gFnPc7	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
byl	být	k5eAaImAgMnS	být
zvláště	zvláště	k6eAd1	zvláště
spokojený	spokojený	k2eAgMnSc1d1	spokojený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
byl	být	k5eAaImAgMnS	být
náchylný	náchylný	k2eAgMnSc1d1	náchylný
ke	k	k7c3	k
kinetóze	kinetóza	k1gFnSc3	kinetóza
a	a	k8xC	a
trpěl	trpět	k5eAaImAgMnS	trpět
nevolnostmi	nevolnost	k1gFnPc7	nevolnost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
cvičil	cvičit	k5eAaImAgInS	cvičit
leteckou	letecký	k2eAgFnSc4d1	letecká
akrobacii	akrobacie	k1gFnSc4	akrobacie
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
trval	trvat	k5eAaImAgInS	trvat
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
chvílích	chvíle	k1gFnPc6	chvíle
astronauti	astronaut	k1gMnPc1	astronaut
odpočívali	odpočívat	k5eAaImAgMnP	odpočívat
při	při	k7c6	při
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
si	se	k3xPyFc3	se
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
dvě	dva	k4xCgFnPc4	dva
skladby	skladba	k1gFnPc4	skladba
-	-	kIx~	-
Novosvětskou	novosvětský	k2eAgFnSc4d1	Novosvětská
symfonii	symfonie	k1gFnSc4	symfonie
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
už	už	k6eAd1	už
když	když	k8xS	když
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
koncertním	koncertní	k2eAgInSc6d1	koncertní
souboru	soubor	k1gInSc6	soubor
<g/>
,	,	kIx,	,
a	a	k8xC	a
album	album	k1gNnSc1	album
Music	Musice	k1gFnPc2	Musice
out	out	k?	out
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
Samuela	Samuel	k1gMnSc2	Samuel
Hoffmana	Hoffman	k1gMnSc2	Hoffman
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
cesty	cesta	k1gFnSc2	cesta
přišel	přijít	k5eAaPmAgMnS	přijít
čas	čas	k1gInSc4	čas
na	na	k7c4	na
korekci	korekce	k1gFnSc4	korekce
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
UTC	UTC	kA	UTC
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
odvrácenou	odvrácený	k2eAgFnSc7d1	odvrácená
stranou	strana	k1gFnSc7	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gNnSc1	Apollo
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Aldrin	aldrin	k1gInSc1	aldrin
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
odpojili	odpojit	k5eAaPmAgMnP	odpojit
od	od	k7c2	od
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
Collins	Collins	k1gInSc1	Collins
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
motor	motor	k1gInSc1	motor
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
(	(	kIx(	(
<g/>
DPS	DPS	kA	DPS
<g/>
)	)	kIx)	)
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c4	na
sestupovou	sestupový	k2eAgFnSc4d1	sestupová
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
15	[number]	k4	15
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
zapálili	zapálit	k5eAaPmAgMnP	zapálit
DPS	DPS	kA	DPS
podruhé	podruhé	k6eAd1	podruhé
a	a	k8xC	a
zahájili	zahájit	k5eAaPmAgMnP	zahájit
vlastní	vlastní	k2eAgInSc4d1	vlastní
sestup	sestup	k1gInSc4	sestup
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
astronauty	astronaut	k1gMnPc4	astronaut
znepokojilo	znepokojit	k5eAaPmAgNnS	znepokojit
červené	červený	k2eAgNnSc4d1	červené
světlo	světlo	k1gNnSc4	světlo
poplachu	poplach	k1gInSc2	poplach
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
operátor	operátor	k1gInSc4	operátor
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
jim	on	k3xPp3gMnPc3	on
doporučil	doporučit	k5eAaPmAgMnS	doporučit
výstrahu	výstraha	k1gFnSc4	výstraha
ignorovat	ignorovat	k5eAaImF	ignorovat
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
přetížení	přetížení	k1gNnSc6	přetížení
počítače	počítač	k1gInSc2	počítač
způsobené	způsobený	k2eAgMnPc4d1	způsobený
zapnutým	zapnutý	k2eAgInSc7d1	zapnutý
setkávacím	setkávací	k2eAgInSc7d1	setkávací
radarem	radar	k1gInSc7	radar
<g/>
,	,	kIx,	,
při	při	k7c6	při
přistání	přistání	k1gNnSc6	přistání
nepotřebným	potřebný	k2eNgMnPc3d1	nepotřebný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
přistání	přistání	k1gNnSc2	přistání
řídil	řídit	k5eAaImAgMnS	řídit
ručně	ručně	k6eAd1	ručně
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
kamenitému	kamenitý	k2eAgInSc3d1	kamenitý
terénu	terén	k1gInSc3	terén
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
UTC	UTC	kA	UTC
přistál	přistát	k5eAaPmAgInS	přistát
s	s	k7c7	s
méně	málo	k6eAd2	málo
než	než	k8xS	než
třicetivteřinovou	třicetivteřinový	k2eAgFnSc7d1	třicetivteřinový
zásobou	zásoba	k1gFnSc7	zásoba
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Přistáli	přistát	k5eAaPmAgMnP	přistát
v	v	k7c6	v
Mare	Mare	k1gFnSc6	Mare
Tranquillitatis	Tranquillitatis	k1gFnSc2	Tranquillitatis
<g/>
,	,	kIx,	,
na	na	k7c4	na
0	[number]	k4	0
<g/>
°	°	k?	°
<g/>
40	[number]	k4	40
<g/>
'	'	kIx"	'
<g/>
26,69	[number]	k4	26,69
<g/>
"	"	kIx"	"
N	N	kA	N
a	a	k8xC	a
23	[number]	k4	23
<g/>
°	°	k?	°
<g/>
28	[number]	k4	28
<g/>
'	'	kIx"	'
<g/>
22,69	[number]	k4	22,69
<g/>
"	"	kIx"	"
E.	E.	kA	E.
O	o	k7c4	o
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
ve	v	k7c6	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Armstrong	Armstrong	k1gMnSc1	Armstrong
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pronesl	pronést	k5eAaPmAgMnS	pronést
slavnou	slavný	k2eAgFnSc4d1	slavná
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
V	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
Armstrongův	Armstrongův	k2eAgInSc1d1	Armstrongův
sestup	sestup	k1gInSc1	sestup
po	po	k7c6	po
žebříku	žebřík	k1gInSc6	žebřík
a	a	k8xC	a
první	první	k4xOgInPc4	první
kroky	krok	k1gInPc4	krok
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
vidělo	vidět	k5eAaImAgNnS	vidět
a	a	k8xC	a
slyšelo	slyšet	k5eAaImAgNnS	slyšet
odhadem	odhad	k1gInSc7	odhad
na	na	k7c4	na
600	[number]	k4	600
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
pětina	pětina	k1gFnSc1	pětina
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Buzz	Buzz	k1gMnSc1	Buzz
Aldrin	aldrin	k1gInSc4	aldrin
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
vztyčili	vztyčit	k5eAaPmAgMnP	vztyčit
americkou	americký	k2eAgFnSc4d1	americká
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
plnili	plnit	k5eAaImAgMnP	plnit
připravený	připravený	k2eAgInSc4d1	připravený
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Fotografovali	fotografovat	k5eAaImAgMnP	fotografovat
<g/>
,	,	kIx,	,
sbírali	sbírat	k5eAaImAgMnP	sbírat
vzorky	vzorek	k1gInPc4	vzorek
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gMnPc2	on
přivezli	přivézt	k5eAaPmAgMnP	přivézt
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
22	[number]	k4	22
kg	kg	kA	kg
<g/>
,	,	kIx,	,
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozmístili	rozmístit	k5eAaPmAgMnP	rozmístit
přístroje	přístroj	k1gInPc4	přístroj
sady	sada	k1gFnSc2	sada
EASEP	EASEP	kA	EASEP
-	-	kIx~	-
koutový	koutový	k2eAgInSc4d1	koutový
odražeč	odražeč	k1gInSc4	odražeč
a	a	k8xC	a
seismograf	seismograf	k1gInSc4	seismograf
<g/>
.	.	kIx.	.
</s>
<s>
Fotografování	fotografování	k1gNnPc4	fotografování
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
několika	několik	k4yIc6	několik
snímcích	snímek	k1gInPc6	snímek
pořízených	pořízený	k2eAgInPc6d1	pořízený
Aldrinem	aldrin	k1gInSc7	aldrin
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
aparát	aparát	k1gInSc1	aparát
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
půjčil	půjčit	k5eAaPmAgMnS	půjčit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
UTC	UTC	kA	UTC
Aldrin	aldrin	k1gInSc4	aldrin
a	a	k8xC	a
v	v	k7c4	v
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
33	[number]	k4	33
UTC	UTC	kA	UTC
i	i	k8xC	i
Armstrong	Armstrong	k1gMnSc1	Armstrong
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
do	do	k7c2	do
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
odpočívali	odpočívat	k5eAaImAgMnP	odpočívat
(	(	kIx(	(
<g/>
Armstrong	Armstrong	k1gMnSc1	Armstrong
v	v	k7c6	v
nepohodlné	pohodlný	k2eNgFnSc6d1	nepohodlná
pozici	pozice	k1gFnSc6	pozice
nemohl	moct	k5eNaImAgMnS	moct
usnout	usnout	k5eAaPmF	usnout
a	a	k8xC	a
vrtěl	vrtět	k5eAaImAgMnS	vrtět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
seismograf	seismograf	k1gInSc1	seismograf
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
citlivý	citlivý	k2eAgInSc1d1	citlivý
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachytil	zachytit	k5eAaPmAgMnS	zachytit
jeho	jeho	k3xOp3gInPc4	jeho
pohyby	pohyb	k1gInPc4	pohyb
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
programu	program	k1gInSc6	program
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
velitelský	velitelský	k2eAgInSc4d1	velitelský
modul	modul	k1gInSc4	modul
Columbia	Columbia	k1gFnSc1	Columbia
ke	k	k7c3	k
Collinsovi	Collins	k1gMnSc3	Collins
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Měsíce	měsíc	k1gInSc2	měsíc
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
moduly	modul	k1gInPc1	modul
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgInP	spojit
ve	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
UTC	UTC	kA	UTC
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
astronauti	astronaut	k1gMnPc1	astronaut
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
přistáli	přistát	k5eAaPmAgMnP	přistát
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
si	se	k3xPyFc3	se
vysloužili	vysloužit	k5eAaPmAgMnP	vysloužit
celosvětové	celosvětový	k2eAgNnSc4d1	celosvětové
uznání	uznání	k1gNnSc4	uznání
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
ovšem	ovšem	k9	ovšem
napřed	napřed	k6eAd1	napřed
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
prožít	prožít	k5eAaPmF	prožít
v	v	k7c6	v
karanténě	karanténa	k1gFnSc6	karanténa
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
proti	proti	k7c3	proti
zavlečení	zavlečení	k1gNnSc3	zavlečení
mimozemských	mimozemský	k2eAgInPc2d1	mimozemský
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Armstrongův	Armstrongův	k2eAgInSc1d1	Armstrongův
kosmický	kosmický	k2eAgInSc1d1	kosmický
let	let	k1gInSc1	let
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
délky	délka	k1gFnSc2	délka
8	[number]	k4	8
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
3	[number]	k4	3
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
18	[number]	k4	18
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
35	[number]	k4	35
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
jeho	jeho	k3xOp3gInPc1	jeho
lety	léto	k1gNnPc7	léto
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
dohromady	dohromady	k6eAd1	dohromady
trvaly	trvat	k5eAaImAgInP	trvat
8	[number]	k4	8
dní	den	k1gInPc2	den
a	a	k8xC	a
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
<g/>
;	;	kIx,	;
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
17	[number]	k4	17
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
44	[number]	k4	44
sekundy	sekunda	k1gFnSc2	sekunda
strávil	strávit	k5eAaPmAgInS	strávit
vycházkou	vycházka	k1gFnSc7	vycházka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
vláda	vláda	k1gFnSc1	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
neopomněla	opomnět	k5eNaPmAgFnS	opomnět
využít	využít	k5eAaPmF	využít
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
propagaci	propagace	k1gFnSc3	propagace
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
procestovali	procestovat	k5eAaPmAgMnP	procestovat
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
a	a	k8xC	a
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc4d1	velký
skok	skok	k1gInSc4	skok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pětačtyřicetidenní	pětačtyřicetidenní	k2eAgNnSc4d1	pětačtyřicetidenní
putování	putování	k1gNnSc4	putování
po	po	k7c6	po
ne	ne	k9	ne
méně	málo	k6eAd2	málo
než	než	k8xS	než
třiadvaceti	třiadvacet	k4xCc6	třiadvacet
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vůli	vůle	k1gFnSc4	vůle
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
předvést	předvést	k5eAaPmF	předvést
program	program	k1gInSc4	program
Apollo	Apollo	k1gMnSc1	Apollo
jako	jako	k9	jako
úspěch	úspěch	k1gInSc4	úspěch
celého	celý	k2eAgNnSc2d1	celé
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
množství	množství	k1gNnSc2	množství
setkání	setkání	k1gNnSc2	setkání
posádku	posádka	k1gFnSc4	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
vidělo	vidět	k5eAaImAgNnS	vidět
snad	snad	k9	snad
100	[number]	k4	100
<g/>
,	,	kIx,	,
snad	snad	k9	snad
150	[number]	k4	150
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
25	[number]	k4	25
tisíc	tisíc	k4xCgInSc4	tisíc
si	se	k3xPyFc3	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
podalo	podat	k5eAaPmAgNnS	podat
ruku	ruka	k1gFnSc4	ruka
nebo	nebo	k8xC	nebo
dostalo	dostat	k5eAaPmAgNnS	dostat
autogram	autogram	k1gInSc4	autogram
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mimořádně	mimořádně	k6eAd1	mimořádně
populárním	populární	k2eAgMnSc6d1	populární
<g/>
,	,	kIx,	,
i	i	k8xC	i
ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Američany	Američan	k1gMnPc4	Američan
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
hrdinů	hrdina	k1gMnPc2	hrdina
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
slávě	sláva	k1gFnSc3	sláva
si	se	k3xPyFc3	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
zachoval	zachovat	k5eAaPmAgMnS	zachovat
pokoru	pokora	k1gFnSc4	pokora
a	a	k8xC	a
skromnost	skromnost	k1gFnSc4	skromnost
<g/>
,	,	kIx,	,
popularitu	popularita	k1gFnSc4	popularita
se	se	k3xPyFc4	se
nesnažil	snažit	k5eNaImAgMnS	snažit
využívat	využívat	k5eAaPmF	využívat
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Říkali	říkat	k5eAaImAgMnP	říkat
mu	on	k3xPp3gMnSc3	on
"	"	kIx"	"
<g/>
zdráhající	zdráhající	k2eAgNnSc4d1	zdráhající
se	se	k3xPyFc4	se
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgInS	mít
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
slavným	slavný	k2eAgMnSc7d1	slavný
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
potom	potom	k6eAd1	potom
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
slavné	slavný	k2eAgNnSc1d1	slavné
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
nepředváděl	předvádět	k5eNaImAgMnS	předvádět
<g/>
,	,	kIx,	,
Cincinnati	Cincinnati	k1gMnPc1	Cincinnati
Post	posta	k1gFnPc2	posta
ho	on	k3xPp3gNnSc4	on
vystihl	vystihnout	k5eAaPmAgInS	vystihnout
titulkem	titulek	k1gInSc7	titulek
článku	článek	k1gInSc2	článek
k	k	k7c3	k
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
letu	let	k1gInSc3	let
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cincinatský	Cincinatský	k2eAgMnSc1d1	Cincinatský
neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
Armstrong	Armstrong	k1gMnSc1	Armstrong
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
delegace	delegace	k1gFnSc2	delegace
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
výroční	výroční	k2eAgFnSc6d1	výroční
konferenci	konference	k1gFnSc6	konference
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
COSPAR	COSPAR	kA	COSPAR
<g/>
)	)	kIx)	)
v	v	k7c6	v
Leningradu	Leningrad	k1gInSc2	Leningrad
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
byl	být	k5eAaImAgInS	být
uvítán	uvítat	k5eAaPmNgInS	uvítat
bouřlivými	bouřlivý	k2eAgFnPc7d1	bouřlivá
ovacemi	ovace	k1gFnPc7	ovace
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
přednášku	přednáška	k1gFnSc4	přednáška
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
letu	let	k1gInSc6	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
konference	konference	k1gFnSc2	konference
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
prodloužení	prodloužení	k1gNnSc4	prodloužení
návštěvy	návštěva	k1gFnSc2	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Zastavil	zastavit	k5eAaPmAgInS	zastavit
se	se	k3xPyFc4	se
v	v	k7c6	v
Novosibirsku	Novosibirsk	k1gInSc6	Novosibirsk
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
přijal	přijmout	k5eAaPmAgMnS	přijmout
sovětský	sovětský	k2eAgMnSc1d1	sovětský
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Alexej	Alexej	k1gMnSc1	Alexej
Kosygin	Kosygin	k1gMnSc1	Kosygin
a	a	k8xC	a
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
i	i	k9	i
v	v	k7c6	v
Akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Měsíce	měsíc	k1gInSc2	měsíc
Armstrong	Armstrong	k1gMnSc1	Armstrong
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neplánuje	plánovat	k5eNaImIp3nS	plánovat
další	další	k2eAgInSc4d1	další
kosmický	kosmický	k2eAgInSc4d1	kosmický
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oddílu	oddíl	k1gInSc2	oddíl
astronautů	astronaut	k1gMnPc2	astronaut
NASA	NASA	kA	NASA
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
zástupcem	zástupce	k1gMnSc7	zástupce
vedoucího	vedoucí	k1gMnSc2	vedoucí
oddělení	oddělení	k1gNnSc2	oddělení
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
v	v	k7c6	v
centrále	centrála	k1gFnSc6	centrála
NASA	NASA	kA	NASA
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
místě	místo	k1gNnSc6	místo
dohlížel	dohlížet	k5eAaImAgInS	dohlížet
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
NASA	NASA	kA	NASA
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
oboru	obor	k1gInSc6	obor
zodpovídal	zodpovídat	k5eAaImAgMnS	zodpovídat
za	za	k7c4	za
koordinaci	koordinace	k1gFnSc4	koordinace
mezi	mezi	k7c7	mezi
NASA	NASA	kA	NASA
a	a	k8xC	a
průmyslem	průmysl	k1gInSc7	průmysl
i	i	k8xC	i
zainteresovanými	zainteresovaný	k2eAgFnPc7d1	zainteresovaná
vládními	vládní	k2eAgFnPc7d1	vládní
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgFnSc1d1	získaná
autorita	autorita	k1gFnSc1	autorita
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
i	i	k9	i
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
komisi	komise	k1gFnSc6	komise
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
šetřila	šetřit	k5eAaImAgFnS	šetřit
nehodu	nehoda	k1gFnSc4	nehoda
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
ale	ale	k8xC	ale
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
a	a	k8xC	a
řešení	řešení	k1gNnSc4	řešení
následků	následek	k1gInPc2	následek
nehody	nehoda	k1gFnSc2	nehoda
již	již	k9	již
neovlivňoval	ovlivňovat	k5eNaImAgMnS	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kancelářská	kancelářská	k1gFnSc1	kancelářská
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nelíbila	líbit	k5eNaImAgFnS	líbit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1971	[number]	k4	1971
z	z	k7c2	z
NASA	NASA	kA	NASA
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
NASA	NASA	kA	NASA
neztratil	ztratit	k5eNaPmAgMnS	ztratit
ani	ani	k8xC	ani
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
místopředsedou	místopředseda	k1gMnSc7	místopředseda
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšetřovala	vyšetřovat	k5eAaImAgFnS	vyšetřovat
nehodu	nehoda	k1gFnSc4	nehoda
raketoplánu	raketoplán	k1gInSc2	raketoplán
Challenger	Challengero	k1gNnPc2	Challengero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
až	až	k9	až
1979	[number]	k4	1979
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
letecké	letecký	k2eAgNnSc4d1	letecké
inženýrství	inženýrství	k1gNnSc4	inženýrství
na	na	k7c6	na
Cincinnattské	Cincinnattský	k2eAgFnSc6d1	Cincinnattský
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
z	z	k7c2	z
množství	množství	k1gNnSc2	množství
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
mohl	moct	k5eAaImAgMnS	moct
očekávat	očekávat	k5eAaImF	očekávat
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
správní	správní	k2eAgFnSc6d1	správní
radě	rada	k1gFnSc6	rada
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Cincinatti	Cincinatť	k1gFnSc6	Cincinatť
dostal	dostat	k5eAaPmAgMnS	dostat
profesuru	profesura	k1gFnSc4	profesura
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
neměl	mít	k5eNaImAgMnS	mít
doktorát	doktorát	k1gInSc4	doktorát
-	-	kIx~	-
všechny	všechen	k3xTgInPc1	všechen
jeho	jeho	k3xOp3gInPc1	jeho
doktorské	doktorský	k2eAgInPc1d1	doktorský
tituly	titul	k1gInPc1	titul
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
čestné	čestný	k2eAgInPc1d1	čestný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
politikem	politikum	k1gNnSc7	politikum
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
NASA	NASA	kA	NASA
ho	on	k3xPp3gMnSc4	on
oslovili	oslovit	k5eAaPmAgMnP	oslovit
zástupci	zástupce	k1gMnPc1	zástupce
obou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
-	-	kIx~	-
demokratické	demokratický	k2eAgFnPc1d1	demokratická
i	i	k8xC	i
republikánské	republikánský	k2eAgFnPc1d1	republikánská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
kolegů	kolega	k1gMnPc2	kolega
a	a	k8xC	a
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
senátorů	senátor	k1gMnPc2	senátor
Johna	John	k1gMnSc2	John
Glenna	Glenn	k1gMnSc2	Glenn
a	a	k8xC	a
Harrisona	Harrison	k1gMnSc2	Harrison
Schmitta	Schmitt	k1gInSc2	Schmitt
všechny	všechen	k3xTgFnPc4	všechen
nabídky	nabídka	k1gFnPc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
práv	právo	k1gNnPc2	právo
států	stát	k1gInPc2	stát
a	a	k8xC	a
proti	proti	k7c3	proti
úloze	úloha	k1gFnSc3	úloha
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
světového	světový	k2eAgMnSc2d1	světový
četníka	četník	k1gMnSc2	četník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Lebanon	Lebanona	k1gFnPc2	Lebanona
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tváří	tvář	k1gFnSc7	tvář
reklamních	reklamní	k2eAgInPc2d1	reklamní
kampaní	kampaň	k1gFnSc7	kampaň
několika	několik	k4yIc2	několik
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
především	především	k9	především
Chrysler	Chrysler	k1gInSc1	Chrysler
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
i	i	k9	i
pro	pro	k7c4	pro
General	General	k1gFnSc4	General
Time	Tim	k1gFnSc2	Tim
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
Bankers	Bankers	k1gInSc1	Bankers
Association	Association	k1gInSc1	Association
of	of	k?	of
America	Americ	k1gInSc2	Americ
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
ryze	ryze	k6eAd1	ryze
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
vedl	vést	k5eAaImAgInS	vést
společnost	společnost	k1gFnSc4	společnost
Computing	Computing	k1gInSc1	Computing
Technologies	Technologies	k1gMnSc1	Technologies
for	forum	k1gNnPc2	forum
Aviation	Aviation	k1gInSc1	Aviation
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
až	až	k9	až
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
ředitelem	ředitel	k1gMnSc7	ředitel
AIL	AIL	kA	AIL
Technologies	Technologiesa	k1gFnPc2	Technologiesa
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc2	společnost
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
letecké	letecký	k2eAgFnSc2d1	letecká
komponenty	komponenta	k1gFnSc2	komponenta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
předsedal	předsedat	k5eAaImAgMnS	předsedat
správní	správní	k2eAgFnSc3d1	správní
radě	rada	k1gFnSc3	rada
EDO	Eda	k1gMnSc5	Eda
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
správních	správní	k2eAgFnPc6d1	správní
radách	rada	k1gFnPc6	rada
různých	různý	k2eAgFnPc2d1	různá
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
byla	být	k5eAaImAgFnS	být
Learjet	Learjet	k1gInSc4	Learjet
<g/>
,	,	kIx,	,
dalšími	další	k2eAgMnPc7d1	další
např.	např.	kA	např.
Marathon	Marathon	k1gMnSc1	Marathon
Oil	Oil	k1gMnSc1	Oil
<g/>
,	,	kIx,	,
Cincinnati	Cincinnati	k1gMnSc1	Cincinnati
Gas	Gas	k1gMnSc1	Gas
&	&	k?	&
Electric	Electric	k1gMnSc1	Electric
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
Taft	taft	k1gInSc1	taft
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
Eaton	Eaton	k1gMnSc1	Eaton
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
AIL	AIL	kA	AIL
Systems	Systems	k1gInSc1	Systems
a	a	k8xC	a
Thiokol	Thiokol	k1gInSc1	Thiokol
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
po	po	k7c6	po
38	[number]	k4	38
letech	léto	k1gNnPc6	léto
manželství	manželství	k1gNnSc2	manželství
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Carol	Carol	k1gInSc4	Carol
Held	Held	k1gMnSc1	Held
Knightovou	Knightův	k2eAgFnSc7d1	Knightova
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
poznal	poznat	k5eAaPmAgMnS	poznat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
cincinnatském	cincinnatský	k2eAgNnSc6d1	cincinnatský
předměstí	předměstí	k1gNnSc6	předměstí
Indian	Indiana	k1gFnPc2	Indiana
Hill	Hillum	k1gNnPc2	Hillum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
žil	žít	k5eAaImAgMnS	žít
v	v	k7c4	v
ústraní	ústraní	k1gNnSc4	ústraní
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
nedával	dávat	k5eNaImAgMnS	dávat
interview	interview	k1gNnSc4	interview
médiím	médium	k1gNnPc3	médium
a	a	k8xC	a
veřejné	veřejný	k2eAgNnSc4d1	veřejné
dění	dění	k1gNnSc4	dění
komentoval	komentovat	k5eAaBmAgInS	komentovat
pouze	pouze	k6eAd1	pouze
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgNnSc7d1	poslední
významným	významný	k2eAgNnSc7d1	významné
vystoupením	vystoupení	k1gNnSc7	vystoupení
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
kritika	kritika	k1gFnSc1	kritika
zrušení	zrušení	k1gNnSc2	zrušení
programu	program	k1gInSc2	program
Constellation	Constellation	k1gInSc1	Constellation
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
organizace	organizace	k1gFnSc1	organizace
australských	australský	k2eAgMnPc2d1	australský
účetních	účetní	k1gMnPc2	účetní
neobvykle	obvykle	k6eNd1	obvykle
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
<g/>
,	,	kIx,	,
hodinový	hodinový	k2eAgInSc1d1	hodinový
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Armstrongem	Armstrong	k1gMnSc7	Armstrong
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
operaci	operace	k1gFnSc4	operace
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
dostal	dostat	k5eAaPmAgMnS	dostat
bypass	bypass	k1gInSc4	bypass
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
se	s	k7c7	s
srdcem	srdce	k1gNnSc7	srdce
měl	mít	k5eAaImAgInS	mít
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
devět	devět	k4xCc1	devět
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c4	po
úmrtí	úmrtí	k1gNnSc4	úmrtí
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
slabý	slabý	k2eAgInSc4d1	slabý
srdeční	srdeční	k2eAgInSc4d1	srdeční
záchvat	záchvat	k1gInSc4	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zprvu	zprvu	k6eAd1	zprvu
dařilo	dařit	k5eAaImAgNnS	dařit
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Rozloučení	rozloučení	k1gNnSc1	rozloučení
s	s	k7c7	s
Armstrongem	Armstrong	k1gMnSc7	Armstrong
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
kolegů	kolega	k1gMnPc2	kolega
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
zaplněné	zaplněný	k2eAgFnSc6d1	zaplněná
Národní	národní	k2eAgFnSc6d1	národní
katedrále	katedrála	k1gFnSc6	katedrála
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Armstrongovy	Armstrongův	k2eAgInPc1d1	Armstrongův
zpopelněné	zpopelněný	k2eAgInPc1d1	zpopelněný
ostatky	ostatek	k1gInPc1	ostatek
rozptýleny	rozptýlen	k2eAgInPc1d1	rozptýlen
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
během	během	k7c2	během
pohřební	pohřební	k2eAgFnSc2d1	pohřební
ceremonie	ceremonie	k1gFnSc2	ceremonie
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
raketového	raketový	k2eAgInSc2d1	raketový
křižníku	křižník	k1gInSc2	křižník
USS	USS	kA	USS
Philippine	Philippin	k1gMnSc5	Philippin
Sea	Sea	k1gMnSc5	Sea
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
kráter	kráter	k1gInSc1	kráter
Sabine	Sabin	k1gInSc5	Sabin
E	E	kA	E
poblíž	poblíž	k7c2	poblíž
místa	místo	k1gNnSc2	místo
přistání	přistání	k1gNnSc2	přistání
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
na	na	k7c4	na
Armstrong	Armstrong	k1gMnSc1	Armstrong
podle	podle	k7c2	podle
astronautova	astronautův	k2eAgNnSc2d1	astronautův
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
blízké	blízký	k2eAgInPc1d1	blízký
krátery	kráter	k1gInPc1	kráter
Sabine	Sabin	k1gInSc5	Sabin
B	B	kA	B
na	na	k7c4	na
Aldrin	aldrin	k1gInSc4	aldrin
a	a	k8xC	a
Sabine	Sabin	k1gInSc5	Sabin
D	D	kA	D
na	na	k7c4	na
Collins	Collins	k1gInSc4	Collins
podle	podle	k7c2	podle
kolegů	kolega	k1gMnPc2	kolega
astronautů	astronaut	k1gMnPc2	astronaut
z	z	k7c2	z
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
asteroid	asteroid	k1gInSc4	asteroid
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
(	(	kIx(	(
<g/>
6469	[number]	k4	6469
<g/>
)	)	kIx)	)
Armstrong	Armstrong	k1gMnSc1	Armstrong
objevený	objevený	k2eAgInSc1d1	objevený
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
českým	český	k2eAgMnSc7d1	český
astronomem	astronom	k1gMnSc7	astronom
Antonínem	Antonín	k1gMnSc7	Antonín
Mrkosem	Mrkos	k1gMnSc7	Mrkos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
muzeum	muzeum	k1gNnSc1	muzeum
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
množství	množství	k1gNnSc1	množství
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
míst	místo	k1gNnPc2	místo
<g/>
;	;	kIx,	;
jen	jen	k9	jen
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
nese	nést	k5eAaImIp3nS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tucet	tucet	k1gInSc4	tucet
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
místních	místní	k2eAgFnPc2d1	místní
do	do	k7c2	do
skotského	skotský	k2eAgNnSc2d1	skotské
města	město	k1gNnSc2	město
Langholm	Langholmo	k1gNnPc2	Langholmo
<g/>
,	,	kIx,	,
tradičního	tradiční	k2eAgNnSc2d1	tradiční
centra	centrum	k1gNnSc2	centrum
klanu	klan	k1gInSc2	klan
Armstrongů	Armstrong	k1gMnPc2	Armstrong
<g/>
,	,	kIx,	,
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
jmenování	jmenování	k1gNnSc4	jmenování
občanem	občan	k1gMnSc7	občan
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
obdržel	obdržet	k5eAaPmAgInS	obdržet
nejméně	málo	k6eAd3	málo
devatenáct	devatenáct	k4xCc4	devatenáct
čestných	čestný	k2eAgInPc2d1	čestný
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
medailí	medaile	k1gFnSc7	medaile
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
Kosmickou	kosmický	k2eAgFnSc7d1	kosmická
medailí	medaile	k1gFnSc7	medaile
cti	čest	k1gFnSc2	čest
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Zlatou	zlatá	k1gFnSc4	zlatá
medailí	medaile	k1gFnPc2	medaile
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgFnPc2d1	další
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
byl	být	k5eAaImAgMnS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
do	do	k7c2	do
National	National	k1gFnSc2	National
Aviation	Aviation	k1gInSc1	Aviation
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gFnSc1	Fame
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
letecká	letecký	k2eAgFnSc1d1	letecká
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
floridské	floridský	k2eAgFnSc2d1	floridská
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
astronautů	astronaut	k1gMnPc2	astronaut
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Purdueova	Purdueův	k2eAgFnSc1d1	Purdueův
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nově	nov	k1gInSc6	nov
stavěnou	stavěný	k2eAgFnSc4d1	stavěná
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Engineering	Engineering	k1gInSc1	Engineering
<g/>
,	,	kIx,	,
slavnostně	slavnostně	k6eAd1	slavnostně
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
patnácti	patnáct	k4xCc2	patnáct
dalších	další	k2eAgMnPc2d1	další
astronautů	astronaut	k1gMnPc2	astronaut
-	-	kIx~	-
absolventů	absolvent	k1gMnPc2	absolvent
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
Wapakonetě	Wapakonet	k1gInSc6	Wapakonet
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Muzeum	muzeum	k1gNnSc1	muzeum
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
vesmíru	vesmír	k1gInSc2	vesmír
Neila	Neil	k1gMnSc2	Neil
Armstronga	Armstrong	k1gMnSc2	Armstrong
<g/>
,	,	kIx,	,
dokumentující	dokumentující	k2eAgInSc1d1	dokumentující
ohijský	ohijský	k2eAgInSc1d1	ohijský
a	a	k8xC	a
zejména	zejména	k9	zejména
Armstrongův	Armstrongův	k2eAgInSc4d1	Armstrongův
příspěvek	příspěvek	k1gInSc4	příspěvek
k	k	k7c3	k
letectví	letectví	k1gNnSc3	letectví
a	a	k8xC	a
kosmonautice	kosmonautika	k1gFnSc3	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
New	New	k1gFnSc6	New
Knoxville	Knoxville	k1gFnSc2	Knoxville
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
loď	loď	k1gFnSc1	loď
nové	nový	k2eAgFnSc2d1	nová
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
třídy	třída	k1gFnSc2	třída
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
plavidel	plavidlo	k1gNnPc2	plavidlo
ponese	ponést	k5eAaPmIp3nS	ponést
jméno	jméno	k1gNnSc1	jméno
RV	RV	kA	RV
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
(	(	kIx(	(
<g/>
AGOR-	AGOR-	k1gFnSc1	AGOR-
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrobce	výrobce	k1gMnSc1	výrobce
ji	on	k3xPp3gFnSc4	on
předal	předat	k5eAaPmAgMnS	předat
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Určena	určen	k2eAgFnSc1d1	určena
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
komplexnímu	komplexní	k2eAgInSc3d1	komplexní
vědeckému	vědecký	k2eAgInSc3d1	vědecký
výzkumu	výzkum	k1gInSc3	výzkum
moří	mořit	k5eAaImIp3nS	mořit
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mapování	mapování	k1gNnSc2	mapování
nejhlubších	hluboký	k2eAgFnPc2d3	nejhlubší
částí	část	k1gFnPc2	část
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
namluvil	namluvit	k5eAaPmAgInS	namluvit
roli	role	k1gFnSc4	role
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jacka	Jacka	k1gMnSc1	Jacka
Morrowa	Morrowa	k1gMnSc1	Morrowa
v	v	k7c4	v
Quantum	Quantum	k1gNnSc4	Quantum
Quest	Quest	k1gInSc4	Quest
<g/>
:	:	kIx,	:
A	a	k9	a
Cassini	Cassin	k2eAgMnPc1d1	Cassin
Space	Space	k1gMnSc1	Space
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
,	,	kIx,	,
animovaném	animovaný	k2eAgNnSc6d1	animované
vzdělávacím	vzdělávací	k2eAgNnSc6d1	vzdělávací
dobrodružném	dobrodružný	k2eAgNnSc6d1	dobrodružné
sci-fi	scii	k1gNnSc6	sci-fi
filmu	film	k1gInSc2	film
finančně	finančně	k6eAd1	finančně
podporovaném	podporovaný	k2eAgInSc6d1	podporovaný
Jet	jet	k5eAaImF	jet
Propulsion	Propulsion	k1gInSc4	Propulsion
Laboratory	Laborator	k1gInPc7	Laborator
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
měl	mít	k5eAaImAgMnS	mít
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
nadšený	nadšený	k2eAgMnSc1d1	nadšený
skaut	skaut	k1gMnSc1	skaut
<g/>
,	,	kIx,	,
poslat	poslat	k5eAaPmF	poslat
měsíční	měsíční	k2eAgInSc4d1	měsíční
kámen	kámen	k1gInSc4	kámen
na	na	k7c4	na
skautskou	skautský	k2eAgFnSc4d1	skautská
mohylu	mohyla	k1gFnSc4	mohyla
Ivančenu	Ivančen	k2eAgFnSc4d1	Ivančena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
shoda	shoda	k1gFnSc1	shoda
jména	jméno	k1gNnSc2	jméno
Armstrongova	Armstrongův	k2eAgNnSc2d1	Armstrongovo
bydliště	bydliště	k1gNnSc2	bydliště
Lebanonu	Lebanon	k1gInSc2	Lebanon
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
s	s	k7c7	s
názvem	název	k1gInSc7	název
arabského	arabský	k2eAgInSc2d1	arabský
Libanonu	Libanon	k1gInSc2	Libanon
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
také	také	k9	také
Lebanon	Lebanon	k1gInSc1	Lebanon
<g/>
)	)	kIx)	)
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
fámy	fáma	k1gFnSc2	fáma
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc6	Malajsie
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
fámu	fáma	k1gFnSc4	fáma
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
prohlášení	prohlášení	k1gNnSc6	prohlášení
popřelo	popřít	k5eAaPmAgNnS	popřít
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
