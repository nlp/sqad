<s>
Singapur	Singapur	k1gInSc1	Singapur
je	být	k5eAaImIp3nS	být
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
54	[number]	k4	54
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
při	při	k7c6	při
jižní	jižní	k2eAgFnSc6d1	jižní
výspě	výspa	k1gFnSc6	výspa
Malajského	malajský	k2eAgInSc2d1	malajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
pěti	pět	k4xCc2	pět
a	a	k8xC	a
půl	půl	k1xP	půl
milionům	milion	k4xCgInPc3	milion
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
4	[number]	k4	4
117	[number]	k4	117
700	[number]	k4	700
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgMnPc7d1	úřední
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
malajština	malajština	k1gFnSc1	malajština
a	a	k8xC	a
tamilština	tamilština	k1gFnSc1	tamilština
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
sanskrtu	sanskrt	k1gInSc2	sanskrt
a	a	k8xC	a
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
Lví	lví	k2eAgNnSc1d1	lví
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc4	Singapur
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
Sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
Stamford	Stamford	k1gMnSc1	Stamford
Raffles	Raffles	k1gMnSc1	Raffles
z	z	k7c2	z
Britské	britský	k2eAgFnSc2d1	britská
východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
britskou	britský	k2eAgFnSc7d1	britská
korunní	korunní	k2eAgFnSc7d1	korunní
kolonií	kolonie	k1gFnSc7	kolonie
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
Průlivových	Průlivový	k2eAgFnPc2d1	Průlivový
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
strategické	strategický	k2eAgNnSc4d1	strategické
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
zde	zde	k6eAd1	zde
udržovali	udržovat	k5eAaImAgMnP	udržovat
početnou	početný	k2eAgFnSc4d1	početná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
posádku	posádka	k1gFnSc4	posádka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
přístav	přístav	k1gInSc1	přístav
dobyt	dobýt	k5eAaPmNgInS	dobýt
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
s	s	k7c7	s
blížící	blížící	k2eAgFnSc7d1	blížící
se	se	k3xPyFc4	se
porážkou	porážka	k1gFnSc7	porážka
Japonska	Japonsko	k1gNnSc2	Japonsko
získali	získat	k5eAaPmAgMnP	získat
přístav	přístav	k1gInSc4	přístav
opět	opět	k6eAd1	opět
Britové	Brit	k1gMnPc1	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
Malajské	malajský	k2eAgFnSc2d1	malajská
federace	federace	k1gFnSc2	federace
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
závažným	závažný	k2eAgFnPc3d1	závažná
neshodám	neshoda	k1gFnPc3	neshoda
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
dodnes	dodnes	k6eAd1	dodnes
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
81	[number]	k4	81
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
malajského	malajský	k2eAgInSc2d1	malajský
poloostrova	poloostrov	k1gInSc2	poloostrov
úzkým	úzký	k2eAgInSc7d1	úzký
Johorským	Johorský	k2eAgInSc7d1	Johorský
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
zvlněný	zvlněný	k2eAgMnSc1d1	zvlněný
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
uprostřed	uprostřed	k6eAd1	uprostřed
kopec	kopec	k1gInSc1	kopec
Bukit	Bukita	k1gFnPc2	Bukita
Timah	Timaha	k1gFnPc2	Timaha
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
výšky	výška	k1gFnPc1	výška
163,63	[number]	k4	163,63
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
rozšiřovány	rozšiřovat	k5eAaImNgInP	rozšiřovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
okolní	okolní	k2eAgNnSc1d1	okolní
moře	moře	k1gNnSc1	moře
zasypáváno	zasypávat	k5eAaImNgNnS	zasypávat
pískem	písek	k1gInSc7	písek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
rozloha	rozloha	k1gFnSc1	rozloha
Singapuru	Singapur	k1gInSc2	Singapur
z	z	k7c2	z
580	[number]	k4	580
km2	km2	k4	km2
na	na	k7c4	na
710	[number]	k4	710
km2	km2	k4	km2
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
zvětšení	zvětšení	k1gNnSc1	zvětšení
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
55	[number]	k4	55
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
velmi	velmi	k6eAd1	velmi
teplé	teplý	k2eAgFnPc1d1	teplá
a	a	k8xC	a
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vlhkostí	vlhkost	k1gFnSc7	vlhkost
nepříjemné	příjemný	k2eNgFnSc3d1	nepříjemná
<g/>
.	.	kIx.	.
</s>
<s>
Monzuny	monzun	k1gInPc1	monzun
přinášejí	přinášet	k5eAaImIp3nP	přinášet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
rostliny	rostlina	k1gFnSc2	rostlina
durian	duriana	k1gFnPc2	duriana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
kvůli	kvůli	k7c3	kvůli
pověstnému	pověstný	k2eAgInSc3d1	pověstný
zápachu	zápach	k1gInSc3	zápach
vnášet	vnášet	k5eAaImF	vnášet
do	do	k7c2	do
vládních	vládní	k2eAgFnPc2d1	vládní
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vůbec	vůbec	k9	vůbec
nejhustěji	husto	k6eAd3	husto
zalidněných	zalidněný	k2eAgInPc2d1	zalidněný
států	stát	k1gInPc2	stát
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
rychlý	rychlý	k2eAgInSc4d1	rychlý
růst	růst	k1gInSc4	růst
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
imigraci	imigrace	k1gFnSc3	imigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
činil	činit	k5eAaImAgInS	činit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
5	[number]	k4	5
312	[number]	k4	312
400	[number]	k4	400
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
rozlohou	rozloha	k1gFnSc7	rozloha
většími	veliký	k2eAgInPc7d2	veliký
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
a	a	k8xC	a
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnná	úhrnný	k2eAgFnSc1d1	úhrnná
plodnost	plodnost	k1gFnSc1	plodnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
imigrace	imigrace	k1gFnSc1	imigrace
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Singapur	Singapur	k1gInSc4	Singapur
jediným	jediný	k2eAgInSc7d1	jediný
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zabránit	zabránit	k5eAaPmF	zabránit
populačnímu	populační	k2eAgInSc3d1	populační
poklesu	pokles	k1gInSc3	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgNnSc1d1	náboženské
složení	složení	k1gNnSc1	složení
oficiálních	oficiální	k2eAgMnPc2d1	oficiální
rezidentů	rezident	k1gMnPc2	rezident
v	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
55	[number]	k4	55
<g/>
%	%	kIx~	%
buddhisté	buddhista	k1gMnPc1	buddhista
<g/>
,	,	kIx,	,
z	z	k7c2	z
15,5	[number]	k4	15,5
<g/>
%	%	kIx~	%
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
z	z	k7c2	z
12,5	[number]	k4	12,5
<g/>
%	%	kIx~	%
křesťané	křesťan	k1gMnPc1	křesťan
a	a	k8xC	a
z	z	k7c2	z
3,5	[number]	k4	3,5
<g/>
%	%	kIx~	%
hinduisté	hinduista	k1gMnPc1	hinduista
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
o	o	k7c6	o
náboženském	náboženský	k2eAgMnSc6d1	náboženský
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
etnickém	etnický	k2eAgMnSc6d1	etnický
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc3	složení
ale	ale	k8xC	ale
nezahrnují	zahrnovat	k5eNaImIp3nP	zahrnovat
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
dlouhodobými	dlouhodobý	k2eAgMnPc7d1	dlouhodobý
rezidenty	rezident	k1gMnPc7	rezident
(	(	kIx(	(
<g/>
občany	občan	k1gMnPc7	občan
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
osobami	osoba	k1gFnPc7	osoba
s	s	k7c7	s
povolením	povolení	k1gNnSc7	povolení
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
pobytu	pobyt	k1gInSc3	pobyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
populaci	populace	k1gFnSc6	populace
Singapuru	Singapur	k1gInSc2	Singapur
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
růstu	růst	k1gInSc6	růst
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
podíl	podíl	k1gInSc4	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvyspělejší	vyspělý	k2eAgFnPc4d3	nejvyspělejší
ekonomiky	ekonomika	k1gFnPc4	ekonomika
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
finančním	finanční	k2eAgMnSc7d1	finanční
<g/>
,	,	kIx,	,
obchodním	obchodní	k2eAgNnSc7d1	obchodní
a	a	k8xC	a
dopravním	dopravní	k2eAgNnSc7d1	dopravní
centrem	centrum	k1gNnSc7	centrum
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
přístavem	přístav	k1gInSc7	přístav
kontejnerové	kontejnerový	k2eAgFnSc2d1	kontejnerová
námořní	námořní	k2eAgFnSc2d1	námořní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
vývozu	vývoz	k1gInSc6	vývoz
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uplatňována	uplatňován	k2eAgFnSc1d1	uplatňována
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úloha	úloha	k1gFnSc1	úloha
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
zásadní	zásadní	k2eAgInPc4d1	zásadní
podíly	podíl	k1gInPc4	podíl
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
tvořících	tvořící	k2eAgFnPc2d1	tvořící
asi	asi	k9	asi
až	až	k9	až
60	[number]	k4	60
<g/>
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
jako	jako	k9	jako
vlastník	vlastník	k1gMnSc1	vlastník
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jiných	jiný	k2eAgFnPc2d1	jiná
entit	entita	k1gFnPc2	entita
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
investičních	investiční	k2eAgInPc2d1	investiční
fondů	fond	k1gInPc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nízkou	nízký	k2eAgFnSc7d1	nízká
mírou	míra	k1gFnSc7	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
potom	potom	k6eAd1	potom
asi	asi	k9	asi
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
výjimečnou	výjimečný	k2eAgFnSc7d1	výjimečná
kvalitou	kvalita	k1gFnSc7	kvalita
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
výzkum	výzkum	k1gInSc1	výzkum
od	od	k7c2	od
The	The	k1gFnSc2	The
Economist	Economist	k1gInSc1	Economist
Inteligence	inteligence	k1gFnSc2	inteligence
Unit	Unita	k1gFnPc2	Unita
dokonce	dokonce	k9	dokonce
označil	označit	k5eAaPmAgMnS	označit
Singapur	Singapur	k1gInSc4	Singapur
jako	jako	k8xS	jako
zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
vůbec	vůbec	k9	vůbec
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
kvalitou	kvalita	k1gFnSc7	kvalita
života	život	k1gInSc2	život
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc4	Singapur
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
vyspělost	vyspělost	k1gFnSc4	vyspělost
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
nebývale	nebývale	k6eAd1	nebývale
rychlé	rychlý	k2eAgNnSc4d1	rychlé
tempo	tempo	k1gNnSc4	tempo
růstu	růst	k1gInSc2	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
činilo	činit	k5eAaImAgNnS	činit
7,9	[number]	k4	7,9
<g/>
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
dokonce	dokonce	k9	dokonce
14,7	[number]	k4	14,7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
dopravní	dopravní	k2eAgInPc4d1	dopravní
uzly	uzel	k1gInPc4	uzel
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
přístavem	přístav	k1gInSc7	přístav
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Changi	change	k1gFnSc3	change
potom	potom	k8xC	potom
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
letecký	letecký	k2eAgInSc1d1	letecký
uzel	uzel	k1gInSc1	uzel
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
ESSEC	ESSEC	kA	ESSEC
Business	business	k1gInSc1	business
School	School	k1gInSc1	School
INSEAD	INSEAD	kA	INSEAD
Singapur	Singapur	k1gInSc1	Singapur
má	mít	k5eAaImIp3nS	mít
armádu	armáda	k1gFnSc4	armáda
čítající	čítající	k2eAgFnSc4d1	čítající
přes	přes	k7c4	přes
50	[number]	k4	50
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
činí	činit	k5eAaImIp3nS	činit
3,7	[number]	k4	3,7
%	%	kIx~	%
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
investuje	investovat	k5eAaBmIp3nS	investovat
do	do	k7c2	do
zbrojení	zbrojení	k1gNnSc2	zbrojení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeho	jeho	k3xOp3gMnPc1	jeho
mnohem	mnohem	k6eAd1	mnohem
lidnatější	lidnatý	k2eAgMnPc1d2	lidnatější
sousedé	soused	k1gMnPc1	soused
Malajsie	Malajsie	k1gFnSc2	Malajsie
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Singapur	Singapur	k1gInSc1	Singapur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
oddělil	oddělit	k5eAaPmAgMnS	oddělit
od	od	k7c2	od
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
sdílel	sdílet	k5eAaImAgMnS	sdílet
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
podobnou	podobný	k2eAgFnSc4d1	podobná
situaci	situace	k1gFnSc4	situace
jako	jako	k8xS	jako
neuznaný	uznaný	k2eNgInSc1d1	neuznaný
stát	stát	k1gInSc1	stát
obklopený	obklopený	k2eAgInSc1d1	obklopený
státy	stát	k1gInPc4	stát
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
muslimským	muslimský	k2eAgNnSc7d1	muslimské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gMnSc1	Izrael
Singapuru	Singapur	k1gInSc2	Singapur
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgFnSc3d1	malá
rozloze	rozloha	k1gFnSc3	rozloha
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
bezpečnostní	bezpečnostní	k2eAgFnSc6d1	bezpečnostní
koncepci	koncepce	k1gFnSc6	koncepce
především	především	k9	především
na	na	k7c4	na
odstrašení	odstrašení	k1gNnSc4	odstrašení
a	a	k8xC	a
případný	případný	k2eAgInSc4d1	případný
preventivní	preventivní	k2eAgInSc4d1	preventivní
úder	úder	k1gInSc4	úder
na	na	k7c6	na
území	území	k1gNnSc6	území
protivníka	protivník	k1gMnSc2	protivník
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
boji	boj	k1gInSc3	boj
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
možnosti	možnost	k1gFnPc1	možnost
obrany	obrana	k1gFnSc2	obrana
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Mýtný	mýtný	k1gMnSc1	mýtný
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
congestion	congestion	k1gInSc1	congestion
charge	charge	k1gInSc1	charge
<g/>
)	)	kIx)	)
zavedl	zavést	k5eAaPmAgInS	zavést
Singapur	Singapur	k1gInSc4	Singapur
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
formou	forma	k1gFnSc7	forma
vydávání	vydávání	k1gNnSc2	vydávání
licencí	licence	k1gFnSc7	licence
za	za	k7c4	za
vjezd	vjezd	k1gInSc4	vjezd
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Sinagapur	Sinagapur	k1gMnSc1	Sinagapur
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
stolního	stolní	k2eAgInSc2d1	stolní
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
2012	[number]	k4	2012
dvě	dva	k4xCgFnPc4	dva
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
a	a	k8xC	a
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
2008	[number]	k4	2008
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrná	k1gFnSc4	stříbrná
<g/>
.	.	kIx.	.
</s>
