<s>
A-1	A-1	k4	A-1
Pictures	Pictures	k1gInSc1	Pictures
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
株	株	k?	株
<g/>
・	・	k?	・
<g/>
ワ	ワ	k?	ワ
<g/>
・	・	k?	・
<g/>
ピ	ピ	k?	ピ
<g/>
,	,	kIx,	,
Kabušiki	Kabušiki	k1gNnSc1	Kabušiki
kaiša	kaiš	k1gInSc2	kaiš
É	É	kA	É
Wan	Wan	k1gFnSc4	Wan
Pikučázu	Pikučáza	k1gFnSc4	Pikučáza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
animační	animační	k2eAgNnSc4d1	animační
studio	studio	k1gNnSc4	studio
založené	založený	k2eAgNnSc4d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
zaměřující	zaměřující	k2eAgMnSc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
anime	animat	k5eAaPmIp3nS	animat
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
produkční	produkční	k2eAgFnSc2d1	produkční
společnosti	společnost	k1gFnSc2	společnost
Aniplex	Aniplex	k1gInSc1	Aniplex
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Sony	Sony	kA	Sony
Music	Music	k1gMnSc1	Music
Entertainment	Entertainment	k1gMnSc1	Entertainment
Japan	japan	k1gInSc4	japan
<g/>
.	.	kIx.	.
</s>
<s>
Zenmai	Zenmai	k6eAd1	Zenmai
zamurai	zamurai	k6eAd1	zamurai
(	(	kIx(	(
<g/>
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
No-Side	No-Sid	k1gInSc5	No-Sid
<g/>
;	;	kIx,	;
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Ókiku	Ókik	k1gInSc2	Ókik
furikabutte	furikabutte	k5eAaPmIp2nP	furikabutte
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Robby	Robba	k1gFnSc2	Robba
&	&	k?	&
Kerobby	Kerobba	k1gFnSc2	Kerobba
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Persona	persona	k1gFnSc1	persona
<g/>
:	:	kIx,	:
Trinity	Trinita	k1gFnSc2	Trinita
Soul	Soul	k1gInSc1	Soul
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Tecuwan	Tecuwan	k1gInSc1	Tecuwan
Birdy	Birda	k1gFnSc2	Birda
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Kannagi	Kannagi	k1gNnSc2	Kannagi
(	(	kIx(	(
<g/>
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
Ordet	Ordeta	k1gFnPc2	Ordeta
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Kurošicudži	Kurošicudž	k1gFnSc6	Kurošicudž
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Valkyria	Valkyrium	k1gNnPc1	Valkyrium
Chronicles	Chroniclesa	k1gFnPc2	Chroniclesa
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Fairy	Faira	k1gFnSc2	Faira
Tail	Tail	k1gInSc1	Tail
(	(	kIx(	(
<g/>
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
Satelight	Satelighta	k1gFnPc2	Satelighta
<g/>
;	;	kIx,	;
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Working	Working	k1gInSc1	Working
<g/>
<g />
.	.	kIx.	.
</s>
<s>
!!	!!	k?	!!
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
So	So	kA	So
ra	ra	k0	ra
no	no	k9	no
wo	wo	k?	wo
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Ókiku	Ókik	k1gInSc2	Ókik
furikabutte	furikabutte	k5eAaPmIp2nP	furikabutte
<g/>
:	:	kIx,	:
Nacu	Naca	k1gFnSc4	Naca
no	no	k9	no
taikai-hen	taikaien	k1gInSc4	taikai-hen
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Senkó	Senkó	k1gFnSc2	Senkó
no	no	k9	no
Night	Night	k2eAgInSc1d1	Night
Raid	raid	k1gInSc1	raid
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Kurošicudži	Kurošicudž	k1gFnSc6	Kurošicudž
II	II	kA	II
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Seikimacu	Seikimacus	k1gInSc2	Seikimacus
okaruto	okarut	k2eAgNnSc1d1	okarut
gakuin	gakuin	k1gMnSc1	gakuin
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Togainu	Togain	k1gMnSc3	Togain
no	no	k9	no
či	či	k8xC	či
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Fractale	Fractal	k1gMnSc5	Fractal
(	(	kIx(	(
<g/>
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
Ordet	Ordeta	k1gFnPc2	Ordeta
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Ao	Ao	k1gFnSc7	Ao
no	no	k9	no
Exorcist	Exorcist	k1gInSc1	Exorcist
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Ano	ano	k9	ano
hi	hi	k0	hi
mita	mit	k2eAgFnSc1d1	mit
hana	hana	k1gFnSc1	hana
no	no	k9	no
namae	namaat	k5eAaPmIp3nS	namaat
o	o	k7c6	o
bokutači	bokutač	k1gInSc6	bokutač
wa	wa	k?	wa
mada	mad	k1gInSc2	mad
širanai	širana	k1gFnSc2	širana
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Uta	Uta	k1gFnSc1	Uta
<g />
.	.	kIx.	.
</s>
<s>
no	no	k9	no
Prince-sama	Princeama	k1gFnSc1	Prince-sama
<g/>
:	:	kIx,	:
Madži	Madž	k1gFnSc3	Madž
Love	lov	k1gInSc5	lov
1000	[number]	k4	1000
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
IDOLM	IDOLM	kA	IDOLM
<g/>
@	@	kIx~	@
<g/>
STER	stero	k1gNnPc2	stero
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Working	Working	k1gInSc1	Working
<g/>
'	'	kIx"	'
<g/>
!!	!!	k?	!!
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Učú	Učú	k1gFnSc2	Učú
kjódai	kjóda	k1gFnSc2	kjóda
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Curitama	Curitama	k?	Curitama
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Šinsekai	Šinsekai	k1gNnSc3	Šinsekai
jori	jori	k1gNnSc3	jori
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Čó	Čó	k1gMnSc1	Čó
soku	sok	k1gMnSc3	sok
henkei	henkei	k6eAd1	henkei
Gyrozetter	Gyrozettrum	k1gNnPc2	Gyrozettrum
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Magi	Magi	k1gNnSc6	Magi
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Ore	Ore	k1gFnSc1	Ore
no	no	k9	no
kanodžo	kanodžo	k6eAd1	kanodžo
to	ten	k3xDgNnSc1	ten
osananadžimi	osananadži	k1gFnPc7	osananadži
ga	ga	k?	ga
šuraba	šurab	k1gMnSc4	šurab
sugiru	sugira	k1gMnSc4	sugira
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Vivid	Vivid	k1gInSc1	Vivid
Red	Red	k1gMnSc1	Red
Operation	Operation	k1gInSc1	Operation
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Ore	Ore	k1gFnSc1	Ore
no	no	k9	no
imóto	imóto	k1gNnSc4	imóto
ga	ga	k?	ga
konna	konn	k1gInSc2	konn
ni	on	k3xPp3gFnSc4	on
kawaii	kawaie	k1gFnSc4	kawaie
wake	wakat	k5eAaPmIp3nS	wakat
ga	ga	k?	ga
nai	nai	k?	nai
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Uta	Uta	k1gFnSc1	Uta
no	no	k9	no
Prince-sama	Princeama	k1gFnSc1	Prince-sama
<g/>
:	:	kIx,	:
Madži	Madž	k1gFnSc3	Madž
Love	lov	k1gInSc5	lov
2000	[number]	k4	2000
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Servant	servanta	k1gFnPc2	servanta
x	x	k?	x
Service	Service	k1gFnSc2	Service
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Gin	gin	k1gInSc1	gin
no	no	k9	no
sadži	sadzat	k5eAaPmIp1nS	sadzat
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Magi	Mag	k1gMnPc5	Mag
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
Magic	Magice	k1gFnPc2	Magice
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Galileo	Galilea	k1gFnSc5	Galilea
Donna	donna	k1gFnSc1	donna
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Gin	gin	k1gInSc1	gin
no	no	k9	no
sadži	sadzat	k5eAaPmIp1nS	sadzat
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Sekai	Seka	k1gInSc3	Seka
seifuku	seifuk	k1gInSc2	seifuk
<g/>
:	:	kIx,	:
Bórjaku	Bórjak	k1gInSc2	Bórjak
no	no	k9	no
Zvezda	Zvezdo	k1gNnPc4	Zvezdo
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Fairy	Faira	k1gFnSc2	Faira
Tail	Tail	k1gInSc1	Tail
(	(	kIx(	(
<g/>
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
<g />
.	.	kIx.	.
</s>
<s>
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
;	;	kIx,	;
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Rjúgadžó	Rjúgadžó	k1gFnSc2	Rjúgadžó
Nanana	Nanan	k1gMnSc4	Nanan
no	no	k9	no
maizókin	maizókin	k1gMnSc1	maizókin
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
II	II	kA	II
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Aldnoah	Aldnoah	k1gMnSc1	Aldnoah
Zero	Zero	k1gMnSc1	Zero
(	(	kIx(	(
<g/>
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
Troyca	Troycum	k1gNnSc2	Troycum
<g/>
;	;	kIx,	;
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Persona	persona	k1gFnSc1	persona
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Golden	Goldna	k1gFnPc2	Goldna
Animation	Animation	k1gInSc1	Animation
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Kurošicudži	Kurošicudž	k1gFnSc6	Kurošicudž
<g/>
:	:	kIx,	:
Book	Book	k1gInSc1	Book
of	of	k?	of
Circus	Circus	k1gInSc1	Circus
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Šigacu	Šigacus	k1gInSc2	Šigacus
wa	wa	k?	wa
kimi	kimi	k6eAd1	kimi
no	no	k9	no
uso	uso	k?	uso
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Nanacu	Nanacus	k1gInSc2	Nanacus
no	no	k9	no
taizai	taizai	k6eAd1	taizai
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Magic	Magice	k1gFnPc2	Magice
Kaito	Kaito	k1gNnSc1	Kaito
1412	[number]	k4	1412
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Saenai	Saena	k1gFnSc2	Saena
Heroine	heroin	k1gInSc5	heroin
no	no	k9	no
sodatekata	sodateke	k1gNnPc4	sodateke
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Idolmaster	Idolmaster	k1gMnSc1	Idolmaster
Cinderella	Cinderella	k1gMnSc1	Cinderella
Girls	girl	k1gFnPc2	girl
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Uta	Uta	k1gFnSc1	Uta
no	no	k9	no
Prince-sama	Princeama	k1gFnSc1	Prince-sama
<g/>
:	:	kIx,	:
Madži	Madž	k1gFnSc3	Madž
Love	lov	k1gInSc5	lov
Revolutions	Revolutionsa	k1gFnPc2	Revolutionsa
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Mahó	Mahó	k1gMnSc1	Mahó
šódžo	šódžo	k1gMnSc1	šódžo
Lyrical	Lyrical	k1gMnSc1	Lyrical
Nanoha	Nanoha	k1gMnSc1	Nanoha
ViVid	ViVida	k1gFnPc2	ViVida
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Gunslinger	Gunslinger	k1gMnSc1	Gunslinger
Stratos	Stratos	k1gMnSc1	Stratos
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Animation	Animation	k1gInSc1	Animation
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Denpa	Denpa	k1gFnSc1	Denpa
kjóši	kjósat	k5eAaPmIp1nSwK	kjósat
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Gate	Gat	k1gInSc2	Gat
<g/>
:	:	kIx,	:
Džieitai	Džieita	k1gMnPc1	Džieita
kano	kano	k1gMnSc1	kano
či	či	k8xC	či
nite	nit	k1gInSc5	nit
<g/>
,	,	kIx,	,
kaku	kak	k1gMnSc6	kak
tatakaeri	tatakaer	k1gMnSc6	tatakaer
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Working	Working	k1gInSc1	Working
<g/>
!!!	!!!	k?	!!!
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Idolmaster	Idolmaster	k1gMnSc1	Idolmaster
Cinderella	Cinderella	k1gMnSc1	Cinderella
Girls	girl	k1gFnPc2	girl
2	[number]	k4	2
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Subete	Sube	k1gNnSc2	Sube
ga	ga	k?	ga
F	F	kA	F
ni	on	k3xPp3gFnSc4	on
naru	nara	k1gFnSc4	nara
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Gakusen	Gakusen	k1gInSc1	Gakusen
toši	toši	k1gNnSc1	toši
Asterisk	Asterisk	k1gInSc1	Asterisk
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Boku	bok	k1gInSc2	bok
dake	dake	k1gInSc1	dake
ga	ga	k?	ga
inai	inai	k1gNnSc2	inai
mači	mač	k1gInSc6	mač
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Hai	Hai	k1gFnSc4	Hai
to	ten	k3xDgNnSc1	ten
gensó	gensó	k?	gensó
no	no	k9	no
Grimgar	Grimgar	k1gMnSc1	Grimgar
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Gjakuten	Gjakuten	k2eAgInSc1d1	Gjakuten
saiban	saiban	k1gInSc1	saiban
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Qualidea	Qualide	k2eAgFnSc1d1	Qualide
Code	Code	k1gFnSc1	Code
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Nanacu	Nanacus	k1gInSc2	Nanacus
no	no	k9	no
taizai	taizai	k6eAd1	taizai
<g/>
:	:	kIx,	:
Seisen	Seisen	k1gInSc1	Seisen
no	no	k9	no
širuši	širuše	k1gFnSc4	širuše
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Uta	Uta	k1gFnSc1	Uta
no	no	k9	no
Prince-sama	Princeama	k1gFnSc1	Prince-sama
<g/>
:	:	kIx,	:
Madži	Madž	k1gFnSc3	Madž
Love	lov	k1gInSc5	lov
Legend	legenda	k1gFnPc2	legenda
Star	Star	kA	Star
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Granblue	Granblu	k1gInSc2	Granblu
Fantasy	fantas	k1gInPc1	fantas
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Eromanga	Eromanga	k1gFnSc1	Eromanga
sensei	sense	k1gFnSc2	sense
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
WWW	WWW	kA	WWW
Working	Working	k1gInSc1	Working
<g/>
!!	!!	k?	!!
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Učú	Učú	k1gFnPc3	Učú
šó	šó	k?	šó
e	e	k0	e
jókoso	jókoso	k6eAd1	jókoso
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Gekidžóban	Gekidžóban	k1gMnSc1	Gekidžóban
Fairy	Faira	k1gFnSc2	Faira
Tail	Tail	k1gMnSc1	Tail
<g/>
:	:	kIx,	:
Hóó	Hóó	k1gMnSc1	Hóó
no	no	k9	no
miko	miko	k6eAd1	miko
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Gekidžóban	Gekidžóban	k1gMnSc1	Gekidžóban
Ao	Ao	k1gMnSc1	Ao
no	no	k9	no
Exorcist	Exorcist	k1gMnSc1	Exorcist
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Planetarium	Planetarium	k1gNnSc4	Planetarium
Učú	Učú	k1gFnSc2	Učú
kjódai	kjóda	k1gFnSc2	kjóda
<g/>
:	:	kIx,	:
Itten	Itten	k1gInSc1	Itten
no	no	k9	no
hikari	hikari	k6eAd1	hikari
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Saint	Saint	k1gMnSc1	Saint
onii-san	oniian	k1gMnSc1	onii-san
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Ano	ano	k9	ano
hi	hi	k0	hi
mita	mit	k2eAgFnSc1d1	mit
hana	hana	k1gFnSc1	hana
no	no	k9	no
namae	namaat	k5eAaPmIp3nS	namaat
o	o	k7c6	o
bokutači	bokutač	k1gInSc6	bokutač
wa	wa	k?	wa
mada	mad	k1gInSc2	mad
širanai	širana	k1gFnSc2	širana
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Idolmaster	Idolmaster	k1gInSc1	Idolmaster
Movie	Movie	k1gFnSc1	Movie
<g/>
:	:	kIx,	:
Kagajaki	Kagajak	k1gMnSc3	Kagajak
no	no	k9	no
mukógawa	mukógawa	k6eAd1	mukógawa
e	e	k0	e
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Ókii	Ókii	k1gNnSc2	Ókii
ičinensei	ičinense	k1gFnSc2	ičinense
to	ten	k3xDgNnSc1	ten
čiisana	čiisana	k1gFnSc1	čiisana
ninensei	ninense	k1gFnSc2	ninense
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Persona	persona	k1gFnSc1	persona
3	[number]	k4	3
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
<g/>
:	:	kIx,	:
#	#	kIx~	#
<g/>
2	[number]	k4	2
Midsummer	Midsummer	k1gMnSc1	Midsummer
Knight	Knight	k1gMnSc1	Knight
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dream	Dream	k1gInSc1	Dream
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Učú	Učú	k1gFnSc2	Učú
kjódai	kjóda	k1gFnSc2	kjóda
#	#	kIx~	#
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Persona	persona	k1gFnSc1	persona
3	[number]	k4	3
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
#	#	kIx~	#
<g/>
3	[number]	k4	3
Falling	Falling	k1gInSc1	Falling
Down	Down	k1gInSc4	Down
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Kokoro	Kokora	k1gFnSc5	Kokora
ga	ga	k?	ga
sakebitagatterun	sakebitagatterun	k1gInSc1	sakebitagatterun
da	da	k?	da
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Glass	Glass	k1gInSc4	Glass
no	no	k9	no
hana	hana	k1gFnSc1	hana
to	ten	k3xDgNnSc4	ten
kowasu	kowasa	k1gFnSc4	kowasa
sekai	sekae	k1gFnSc4	sekae
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Dókjúsei	Dókjúsei	k1gNnSc6	Dókjúsei
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Tecuwan	Tecuwan	k1gInSc1	Tecuwan
Birdy	Birda	k1gFnSc2	Birda
Decode	Decod	k1gInSc5	Decod
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Cipher	Ciphra	k1gFnPc2	Ciphra
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Tonari	Tonari	k6eAd1	Tonari
no	no	k9	no
801	[number]	k4	801
<g/>
-čan	-čana	k1gFnPc2	-čana
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Fairy	Faira	k1gFnSc2	Faira
Tail	Tail	k1gInSc1	Tail
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Valkyria	Valkyrium	k1gNnSc2	Valkyrium
Chronicles	Chroniclesa	k1gFnPc2	Chroniclesa
III	III	kA	III
<g/>
:	:	kIx,	:
Tagatame	Tagatam	k1gInSc5	Tagatam
no	no	k9	no
džúsó	džúsó	k?	džúsó
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Saint	Saint	k1gMnSc1	Saint
onii-san	oniian	k1gMnSc1	onii-san
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Idolmaster	Idolmaster	k1gMnSc1	Idolmaster
<g/>
:	:	kIx,	:
Shiny	Shin	k1gInPc1	Shin
Festa	Festa	k?	Festa
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Kurošicudži	Kurošicudž	k1gFnSc6	Kurošicudž
<g/>
:	:	kIx,	:
Book	Book	k1gInSc1	Book
of	of	k?	of
Murder	Murder	k1gInSc1	Murder
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Nanacu	Nanacus	k1gInSc2	Nanacus
no	no	k9	no
taizai	taizai	k6eAd1	taizai
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Šigacu	Šigacus	k1gInSc2	Šigacus
wa	wa	k?	wa
kimi	kimi	k6eAd1	kimi
no	no	k9	no
uso	uso	k?	uso
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
