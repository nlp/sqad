<s>
Dedukce	dedukce	k1gFnSc1	dedukce
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
deductio	deductio	k1gNnSc1	deductio
-	-	kIx~	-
odvození	odvození	k1gNnSc1	odvození
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
usuzování	usuzování	k1gNnSc2	usuzování
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
od	od	k7c2	od
předpokladů	předpoklad	k1gInPc2	předpoklad
(	(	kIx(	(
<g/>
premis	premisa	k1gFnPc2	premisa
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
předpokladů	předpoklad	k1gInPc2	předpoklad
vyplývajícího	vyplývající	k2eAgInSc2d1	vyplývající
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odvozování	odvozování	k1gNnSc1	odvozování
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
základní	základní	k2eAgInSc4d1	základní
postup	postup	k1gInSc4	postup
při	při	k7c6	při
dokazování	dokazování	k1gNnSc6	dokazování
<g/>
.	.	kIx.	.
</s>
<s>
Standardy	standard	k1gInPc1	standard
deduktivního	deduktivní	k2eAgNnSc2d1	deduktivní
usuzování	usuzování	k1gNnSc2	usuzování
formuluje	formulovat	k5eAaImIp3nS	formulovat
logika	logika	k1gFnSc1	logika
<g/>
.	.	kIx.	.
</s>
<s>
Logicky	logicky	k6eAd1	logicky
správná	správný	k2eAgFnSc1d1	správná
dedukce	dedukce	k1gFnSc1	dedukce
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
posloupnosti	posloupnost	k1gFnSc2	posloupnost
kroků	krok	k1gInPc2	krok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
splňují	splňovat	k5eAaImIp3nP	splňovat
přesně	přesně	k6eAd1	přesně
stanovená	stanovený	k2eAgNnPc4d1	stanovené
kritéria	kritérion	k1gNnPc4	kritérion
zabezpečující	zabezpečující	k2eAgNnPc4d1	zabezpečující
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
premisy	premisa	k1gFnSc2	premisa
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
i	i	k8xC	i
závěr	závěr	k1gInSc1	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Dedukce	dedukce	k1gFnSc1	dedukce
představuje	představovat	k5eAaImIp3nS	představovat
konstitutivní	konstitutivní	k2eAgInSc4d1	konstitutivní
metodologický	metodologický	k2eAgInSc4d1	metodologický
postup	postup	k1gInSc4	postup
tzv.	tzv.	kA	tzv.
deduktivních	deduktivní	k2eAgFnPc2d1	deduktivní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
empirických	empirický	k2eAgFnPc6d1	empirická
vědách	věda	k1gFnPc6	věda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
indukce	indukce	k1gFnSc1	indukce
čili	čili	k8xC	čili
zobecňování	zobecňování	k1gNnSc1	zobecňování
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
poznatků	poznatek	k1gInPc2	poznatek
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc1	měření
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
obecné	obecný	k2eAgFnPc1d1	obecná
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
dedukce	dedukce	k1gFnSc2	dedukce
je	být	k5eAaImIp3nS	být
sylogismus	sylogismus	k1gInSc1	sylogismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
smrtelní	smrtelní	k2eAgMnSc1d1	smrtelní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
premisa	premisa	k1gFnSc1	premisa
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Sókratés	Sókratés	k1gInSc4	Sókratés
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
premisa	premisa	k1gFnSc1	premisa
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Takže	takže	k9	takže
Sókratés	Sókratés	k1gInSc1	Sókratés
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dokázaný	dokázaný	k2eAgInSc1d1	dokázaný
závěr	závěr	k1gInSc1	závěr
<g/>
)	)	kIx)	)
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
induktivním	induktivní	k2eAgNnSc6d1	induktivní
uvažování	uvažování	k1gNnSc1	uvažování
by	by	kYmCp3nP	by
šlo	jít	k5eAaImAgNnS	jít
vyslovit	vyslovit	k5eAaPmF	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sókratés	Sókratés	k1gInSc1	Sókratés
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
premisa	premisa	k1gFnSc1	premisa
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Sókratés	Sókratés	k1gInSc4	Sókratés
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
premisa	premisa	k1gFnSc1	premisa
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
smrtelní	smrtelní	k2eAgMnSc1d1	smrtelní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hypotéza	hypotéza	k1gFnSc1	hypotéza
o	o	k7c6	o
obecném	obecný	k2eAgMnSc6d1	obecný
<g/>
)	)	kIx)	)
Hypotéza	hypotéza	k1gFnSc1	hypotéza
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
splnitelná	splnitelný	k2eAgFnSc1d1	splnitelná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
/	/	kIx~	/
vyvrácení	vyvrácení	k1gNnSc3	vyvrácení
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
vyvrácení	vyvrácení	k1gNnSc6	vyvrácení
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
možné	možný	k2eAgNnSc1d1	možné
použití	použití	k1gNnSc1	použití
tvrzení	tvrzení	k1gNnSc2	tvrzení
jako	jako	k9	jako
omezeně	omezeně	k6eAd1	omezeně
splnitelného	splnitelný	k2eAgInSc2d1	splnitelný
předpokladu	předpoklad	k1gInSc2	předpoklad
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
alespoň	alespoň	k9	alespoň
částečná	částečný	k2eAgFnSc1d1	částečná
platnost	platnost	k1gFnSc1	platnost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
už	už	k6eAd1	už
premisou	premisa	k1gFnSc7	premisa
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
příkladů	příklad	k1gInPc2	příklad
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rozdílných	rozdílný	k2eAgNnPc6d1	rozdílné
postaveních	postavení	k1gNnPc6	postavení
univerzálních	univerzální	k2eAgInPc2d1	univerzální
a	a	k8xC	a
existenčních	existenční	k2eAgInPc2d1	existenční
kvantifikátorů	kvantifikátor	k1gInPc2	kvantifikátor
v	v	k7c6	v
premisách	premisa	k1gFnPc6	premisa
a	a	k8xC	a
v	v	k7c6	v
závěrech	závěr	k1gInPc6	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Deduktivní	deduktivní	k2eAgFnSc1d1	deduktivní
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
vytváření	vytváření	k1gNnSc4	vytváření
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
teorií	teorie	k1gFnPc2	teorie
vyvozovaných	vyvozovaný	k2eAgFnPc2d1	vyvozovaná
pouze	pouze	k6eAd1	pouze
dedukcemi	dedukce	k1gFnPc7	dedukce
z	z	k7c2	z
axiomů	axiom	k1gInPc2	axiom
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
již	již	k6eAd1	již
dokázaných	dokázaný	k2eAgFnPc2d1	dokázaná
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
logického	logický	k2eAgNnSc2d1	logické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
řetězec	řetězec	k1gInSc1	řetězec
postupuje	postupovat	k5eAaImIp3nS	postupovat
od	od	k7c2	od
obecných	obecný	k2eAgFnPc2d1	obecná
teorií	teorie	k1gFnPc2	teorie
k	k	k7c3	k
jednotlivostem	jednotlivost	k1gFnPc3	jednotlivost
<g/>
,	,	kIx,	,
zvláštnostem	zvláštnost	k1gFnPc3	zvláštnost
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
shromážděn	shromážděn	k2eAgInSc1d1	shromážděn
empirický	empirický	k2eAgInSc1d1	empirický
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
je	být	k5eAaImIp3nS	být
indukcí	indukce	k1gFnSc7	indukce
definován	definovat	k5eAaBmNgInS	definovat
obecný	obecný	k2eAgInSc1d1	obecný
princip	princip	k1gInSc1	princip
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
podrobováno	podrobovat	k5eAaImNgNnS	podrobovat
deduktivním	deduktivní	k2eAgNnSc7d1	deduktivní
odvozením	odvození	k1gNnSc7	odvození
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
důsledků	důsledek	k1gInPc2	důsledek
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
takovýto	takovýto	k3xDgInSc1	takovýto
postup	postup	k1gInSc1	postup
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
axiomatického	axiomatický	k2eAgInSc2d1	axiomatický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Odůvodnění	odůvodnění	k1gNnSc1	odůvodnění
jevů	jev	k1gInPc2	jev
pomocí	pomocí	k7c2	pomocí
obecných	obecný	k2eAgInPc2d1	obecný
principů	princip	k1gInPc2	princip
využíval	využívat	k5eAaImAgMnS	využívat
zejména	zejména	k9	zejména
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
stavěl	stavět	k5eAaImAgMnS	stavět
dedukci	dedukce	k1gFnSc4	dedukce
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
jistého	jistý	k2eAgNnSc2d1	jisté
poznání	poznání	k1gNnSc2	poznání
proti	proti	k7c3	proti
nejisté	jistý	k2eNgFnSc3d1	nejistá
intuici	intuice	k1gFnSc3	intuice
<g/>
.	.	kIx.	.
</s>
<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
považoval	považovat	k5eAaImAgMnS	považovat
dedukci	dedukce	k1gFnSc4	dedukce
za	za	k7c4	za
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
způsob	způsob	k1gInSc4	způsob
rozumového	rozumový	k2eAgNnSc2d1	rozumové
poznávání	poznávání	k1gNnSc2	poznávání
a	a	k8xC	a
priori	priori	k6eAd1	priori
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
indukce	indukce	k1gFnSc1	indukce
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
poznávání	poznávání	k1gNnSc4	poznávání
a	a	k8xC	a
posteriori	posteriori	k1gNnSc4	posteriori
(	(	kIx(	(
<g/>
ze	z	k7c2	z
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
filozofii	filozofie	k1gFnSc4	filozofie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
důkazový	důkazový	k2eAgInSc1d1	důkazový
prostředek	prostředek	k1gInSc1	prostředek
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
hypoteticko-deduktivní	hypotetickoeduktivní	k2eAgFnSc4d1	hypoteticko-deduktivní
metodu	metoda	k1gFnSc4	metoda
K.	K.	kA	K.
R.	R.	kA	R.
Poppera	Popper	k1gMnSc2	Popper
<g/>
.	.	kIx.	.
</s>
