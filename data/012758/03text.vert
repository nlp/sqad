<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gMnSc1	Grünberg
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
Jülich	Jülicha	k1gFnPc2	Jülicha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
společně	společně	k6eAd1	společně
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Fertem	Fert	k1gMnSc7	Fert
za	za	k7c4	za
objev	objev	k1gInSc4	objev
jevu	jev	k1gInSc2	jev
obří	obří	k2eAgFnSc2d1	obří
magnetorezistence	magnetorezistence	k1gFnSc2	magnetorezistence
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
umožnil	umožnit	k5eAaPmAgInS	umožnit
výrobu	výroba	k1gFnSc4	výroba
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
řádu	řád	k1gInSc2	řád
gigabytů	gigabyt	k1gInPc2	gigabyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
narození	narození	k1gNnSc2	narození
do	do	k7c2	do
odsunu	odsun	k1gInSc2	odsun
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
žil	žít	k5eAaImAgInS	žít
Grünberg	Grünberg	k1gInSc1	Grünberg
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
obyvatelem	obyvatel	k1gMnSc7	obyvatel
Dýšiny	Dýšina	k1gFnSc2	Dýšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gMnSc1	Grünberg
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
během	během	k7c2	během
okupace	okupace	k1gFnSc2	okupace
otci	otec	k1gMnSc3	otec
Fjodoru	Fjodor	k1gMnSc3	Fjodor
Grinbergrovi	Grinbergr	k1gMnSc3	Grinbergr
<g/>
,	,	kIx,	,
ruskému	ruský	k2eAgMnSc3d1	ruský
emigrantovi	emigrant	k1gMnSc3	emigrant
pracujícímu	pracující	k1gMnSc3	pracující
v	v	k7c6	v
plzeňské	plzeňský	k2eAgFnSc6d1	Plzeňská
Škodovce	škodovka	k1gFnSc6	škodovka
<g/>
,	,	kIx,	,
a	a	k8xC	a
matce	matka	k1gFnSc3	matka
Anně	Anna	k1gFnSc3	Anna
Petermannové	Petermannová	k1gFnSc2	Petermannová
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
Dolních	dolní	k2eAgInPc2d1	dolní
Sekyřan	Sekyřany	k1gInPc2	Sekyřany
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
přijal	přijmout	k5eAaPmAgMnS	přijmout
německé	německý	k2eAgNnSc4d1	německé
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
změnil	změnit	k5eAaPmAgMnS	změnit
své	svůj	k3xOyFgNnSc4	svůj
příjmení	příjmení	k1gNnSc4	příjmení
na	na	k7c4	na
Grünberg	Grünberg	k1gInSc4	Grünberg
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
internačním	internační	k2eAgInSc6d1	internační
táboře	tábor	k1gInSc6	tábor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
hromadném	hromadný	k2eAgInSc6d1	hromadný
hrobě	hrob	k1gInSc6	hrob
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
100	[number]	k4	100
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgMnS	být
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gMnSc1	Grünberg
odsunut	odsunout	k5eAaPmNgMnS	odsunout
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
se	se	k3xPyFc4	se
usídlili	usídlit	k5eAaPmAgMnP	usídlit
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
městě	město	k1gNnSc6	město
Lauterbach	Lauterbacha	k1gFnPc2	Lauterbacha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vystudování	vystudování	k1gNnSc6	vystudování
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Lauterbachu	Lauterbach	k1gInSc6	Lauterbach
Peter	Peter	k1gMnSc1	Peter
studoval	studovat	k5eAaImAgMnS	studovat
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Johanna	Johann	k1gMnSc4	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Goetheho	Goethe	k1gMnSc2	Goethe
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
a	a	k8xC	a
Technické	technický	k2eAgFnSc3d1	technická
univerzitě	univerzita	k1gFnSc3	univerzita
v	v	k7c6	v
Darmstadtu	Darmstadt	k1gInSc6	Darmstadt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
na	na	k7c4	na
kanadské	kanadský	k2eAgInPc4d1	kanadský
Carleton	Carleton	k1gInSc4	Carleton
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
do	do	k7c2	do
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
výzkumném	výzkumný	k2eAgNnSc6d1	výzkumné
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
Jülichu	Jülich	k1gInSc6	Jülich
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Marries	Marriesa	k1gFnPc2	Marriesa
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gInSc4	Grünberg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
