<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc4	pojem
mohamedán	mohamedán	k1gMnSc1	mohamedán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sami	sám	k3xTgMnPc1	sám
muslimové	muslim	k1gMnPc1	muslim
chápou	chápat	k5eAaImIp3nP	chápat
jako	jako	k9	jako
nesprávný	správný	k2eNgInSc4d1	nesprávný
a	a	k8xC	a
hanlivý	hanlivý	k2eAgInSc4d1	hanlivý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uctívají	uctívat	k5eAaImIp3nP	uctívat
Alláha	Alláh	k1gMnSc4	Alláh
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
proroka	prorok	k1gMnSc4	prorok
Muhammada	Muhammada	k1gFnSc1	Muhammada
<g/>
.	.	kIx.	.
</s>
