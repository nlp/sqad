<p>
<s>
Muslim	muslim	k1gMnSc1	muslim
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
م	م	k?	م
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyznavač	vyznavač	k1gMnSc1	vyznavač
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
monoteistického	monoteistický	k2eAgNnSc2d1	monoteistické
abrahámovského	abrahámovský	k2eAgNnSc2d1	abrahámovské
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Femininum	femininum	k1gNnSc1	femininum
pro	pro	k7c4	pro
muslimku	muslimka	k1gFnSc4	muslimka
zní	znět	k5eAaImIp3nS	znět
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
"	"	kIx"	"
<g/>
muslima	muslim	k1gMnSc2	muslim
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
arabského	arabský	k2eAgNnSc2d1	arabské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
muslim	muslim	k1gMnSc1	muslim
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
oddaný	oddaný	k2eAgMnSc1d1	oddaný
(	(	kIx(	(
<g/>
Alláhu	Alláh	k1gMnSc6	Alláh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Perská	perský	k2eAgFnSc1d1	perská
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
musalmán	musalmán	k2eAgInSc1d1	musalmán
(	(	kIx(	(
<g/>
م	م	k?	م
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pochází	pocházet	k5eAaImIp3nS	pocházet
české	český	k2eAgNnSc1d1	české
řídce	řídce	k6eAd1	řídce
užívané	užívaný	k2eAgNnSc1d1	užívané
a	a	k8xC	a
zastaralé	zastaralý	k2eAgNnSc1d1	zastaralé
musulman	musulman	k1gMnSc1	musulman
<g/>
,	,	kIx,	,
musulmanka	musulmanka	k1gFnSc1	musulmanka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
nádech	nádech	k1gInSc1	nádech
archaismu	archaismus	k1gInSc2	archaismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vyznavače	vyznavač	k1gMnSc4	vyznavač
islámu	islám	k1gInSc2	islám
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
řada	řada	k1gFnSc1	řada
zastaralých	zastaralý	k2eAgFnPc2d1	zastaralá
a	a	k8xC	a
hanlivých	hanlivý	k2eAgFnPc2d1	hanlivá
označení	označení	k1gNnPc2	označení
<g/>
,	,	kIx,	,
v	v	k7c6	v
novověkých	novověký	k2eAgInPc6d1	novověký
cestopisech	cestopis	k1gInPc6	cestopis
někdy	někdy	k6eAd1	někdy
pojem	pojem	k1gInSc1	pojem
muslim	muslim	k1gMnSc1	muslim
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
Turek	Turek	k1gMnSc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
starší	starší	k1gMnPc4	starší
hanlivé	hanlivý	k2eAgInPc1d1	hanlivý
výrazy	výraz	k1gInPc1	výraz
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
označení	označení	k1gNnSc2	označení
saracéni	saracéen	k2eAgMnPc1d1	saracéen
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zkomoleninu	zkomolenina	k1gFnSc4	zkomolenina
slova	slovo	k1gNnSc2	slovo
šarqíjún	šarqíjúna	k1gFnPc2	šarqíjúna
(	(	kIx(	(
<g/>
Orientálci	orientálec	k1gMnPc1	orientálec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
haragéni	haragén	k1gMnPc1	haragén
či	či	k8xC	či
izmaelité	izmaelitý	k2eAgFnPc1d1	izmaelitý
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
označení	označení	k1gNnSc4	označení
naráží	narážet	k5eAaPmIp3nS	narážet
na	na	k7c4	na
legendu	legenda	k1gFnSc4	legenda
o	o	k7c6	o
Abrahámovské	abrahámovský	k2eAgFnSc6d1	abrahámovská
geneaologii	geneaologie	k1gFnSc6	geneaologie
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Izmaele	Izmael	k1gInSc2	Izmael
<g/>
,	,	kIx,	,
Abrahamova	Abrahamův	k2eAgMnSc2d1	Abrahamův
prvorozeného	prvorozený	k2eAgMnSc2d1	prvorozený
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
mu	on	k3xPp3gMnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
otrokyně	otrokyně	k1gFnSc1	otrokyně
Hagar	Hagara	k1gFnPc2	Hagara
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc4	pojem
mohamedán	mohamedán	k1gMnSc1	mohamedán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sami	sám	k3xTgMnPc1	sám
muslimové	muslim	k1gMnPc1	muslim
chápou	chápat	k5eAaImIp3nP	chápat
jako	jako	k9	jako
nesprávný	správný	k2eNgInSc4d1	nesprávný
a	a	k8xC	a
hanlivý	hanlivý	k2eAgInSc4d1	hanlivý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uctívají	uctívat	k5eAaImIp3nP	uctívat
Alláha	Alláh	k1gMnSc4	Alláh
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
proroka	prorok	k1gMnSc4	prorok
Muhammada	Muhammada	k1gFnSc1	Muhammada
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
zastaralým	zastaralý	k2eAgNnSc7d1	zastaralé
obecným	obecný	k2eAgNnSc7d1	obecné
a	a	k8xC	a
expresivním	expresivní	k2eAgNnSc7d1	expresivní
označením	označení	k1gNnSc7	označení
je	být	k5eAaImIp3nS	být
machometán	machometán	k?	machometán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Muslimové	muslim	k1gMnPc1	muslim
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
muslimů	muslim	k1gMnPc2	muslim
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
asi	asi	k9	asi
na	na	k7c4	na
1,6	[number]	k4	1,6
miliardy	miliarda	k4xCgFnSc2	miliarda
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
tak	tak	k6eAd1	tak
necelou	celý	k2eNgFnSc4d1	necelá
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
po	po	k7c6	po
křesťanech	křesťan	k1gMnPc6	křesťan
(	(	kIx(	(
<g/>
2,2	[number]	k4	2,2
miliardy	miliarda	k4xCgFnSc2	miliarda
<g/>
)	)	kIx)	)
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
skupinu	skupina	k1gFnSc4	skupina
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
muslimů	muslim	k1gMnPc2	muslim
jsou	být	k5eAaImIp3nP	být
sunnité	sunnita	k1gMnPc1	sunnita
(	(	kIx(	(
<g/>
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itskému	itský	k2eAgInSc3d1	itský
islámu	islám	k1gInSc2	islám
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
větví	větev	k1gFnPc2	větev
islámu	islám	k1gInSc2	islám
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
cháridža	cháridža	k6eAd1	cháridža
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovem	slovem	k6eAd1	slovem
Muslim	muslim	k1gMnSc1	muslim
<g/>
,	,	kIx,	,
Muslimové	muslim	k1gMnPc1	muslim
(	(	kIx(	(
<g/>
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
etnická	etnický	k2eAgFnSc1d1	etnická
skupina	skupina	k1gFnSc1	skupina
Bosňáci	Bosňáci	k?	Bosňáci
(	(	kIx(	(
<g/>
odlišujte	odlišovat	k5eAaImRp2nP	odlišovat
od	od	k7c2	od
Bosňanů	Bosňan	k1gMnPc2	Bosňan
<g/>
,	,	kIx,	,
obyvatel	obyvatel	k1gMnSc1	obyvatel
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Muslimové	muslim	k1gMnPc1	muslim
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Významným	významný	k2eAgMnSc7d1	významný
Čechem	Čech	k1gMnSc7	Čech
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Alois	Alois	k1gMnSc1	Alois
Bohdan	Bohdan	k1gMnSc1	Bohdan
Brixius	Brixius	k1gMnSc1	Brixius
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
islámu	islám	k1gInSc2	islám
Hadži	hadži	k1gMnSc1	hadži
Mohamed	Mohamed	k1gMnSc1	Mohamed
Abdalláh	Abdalláh	k1gMnSc1	Abdalláh
Brikcius	Brikcius	k1gMnSc1	Brikcius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
české	český	k2eAgFnSc2d1	Česká
Náboženské	náboženský	k2eAgFnSc2d1	náboženská
rady	rada	k1gFnSc2	rada
muslimské	muslimský	k2eAgFnSc2d1	muslimská
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
asi	asi	k9	asi
700	[number]	k4	700
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
OSTŘANSKÝ	OSTŘANSKÝ	kA	OSTŘANSKÝ
<g/>
,	,	kIx,	,
Bronislav	Bronislav	k1gMnSc1	Bronislav
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
muslimských	muslimský	k2eAgMnPc2d1	muslimský
strašáků	strašák	k1gInPc2	strašák
aneb	aneb	k?	aneb
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
kapitoly	kapitola	k1gFnPc1	kapitola
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
"	"	kIx"	"
<g/>
mediálního	mediální	k2eAgInSc2d1	mediální
islámu	islám	k1gInSc2	islám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
190	[number]	k4	190
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2428	[number]	k4	2428
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
pilířů	pilíř	k1gInPc2	pilíř
islámu	islám	k1gInSc2	islám
</s>
</p>
<p>
<s>
Islám	islám	k1gInSc1	islám
</s>
</p>
<p>
<s>
Mešita	mešita	k1gFnSc1	mešita
</s>
</p>
<p>
<s>
Korán	korán	k1gInSc1	korán
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Bohdan	Bohdan	k1gMnSc1	Bohdan
Brixius	Brixius	k1gMnSc1	Brixius
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
muslim	muslim	k1gMnSc1	muslim
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
muslim	muslim	k1gMnSc1	muslim
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Muslim	muslim	k1gMnSc1	muslim
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
muslimů	muslim	k1gMnPc2	muslim
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
</s>
</p>
<p>
<s>
Muslim	muslim	k1gMnSc1	muslim
<g/>
,	,	kIx,	,
žid	žid	k1gMnSc1	žid
a	a	k8xC	a
bosňák	bosňák	k?	bosňák
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
?	?	kIx.	?
</s>
</p>
