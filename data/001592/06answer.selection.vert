<s>
Jukawa	Jukawa	k1gFnSc1	Jukawa
Hideki	Hidek	k1gFnSc2	Hidek
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
湯	湯	k?	湯
秀	秀	k?	秀
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Yukawa	Yukaw	k2eAgNnPc4d1	Yukaw
Hideki	Hideki	k1gNnPc4	Hideki
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1907	[number]	k4	1907
Tokio	Tokio	k1gNnSc1	Tokio
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1981	[number]	k4	1981
Kjóto	Kjóto	k1gNnSc1	Kjóto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
japonský	japonský	k2eAgMnSc1d1	japonský
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
předpověď	předpověď	k1gFnSc4	předpověď
existence	existence	k1gFnSc2	existence
mezonů	mezon	k1gInPc2	mezon
na	na	k7c6	na
základě	základ	k1gInSc6	základ
teoretického	teoretický	k2eAgInSc2d1	teoretický
výzkumu	výzkum	k1gInSc2	výzkum
jaderných	jaderný	k2eAgFnPc2d1	jaderná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
