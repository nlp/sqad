<s>
Ostrov	ostrov	k1gInSc1	ostrov
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
Jean-François	Jean-François	k1gFnSc1	Jean-François
de	de	k?	de
La	la	k1gNnSc7	la
Pérouse	Pérouse	k1gFnSc2	Pérouse
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gNnSc4	on
podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Jacquese	Jacques	k1gMnSc2	Jacques
Neckera	Necker	k1gMnSc2	Necker
<g/>
.	.	kIx.	.
</s>
