<p>
<s>
Neckerův	Neckerův	k2eAgInSc4d1	Neckerův
ostrov	ostrov	k1gInSc4	ostrov
neboli	neboli	k8xC	neboli
Mokumanamana	Mokumanaman	k1gMnSc2	Mokumanaman
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
690	[number]	k4	690
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Honolulu	Honolulu	k1gNnSc2	Honolulu
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Papahā	Papahā	k1gFnSc2	Papahā
Marine	Marin	k1gInSc5	Marin
National	National	k1gMnSc1	National
Monument	monument	k1gInSc1	monument
(	(	kIx(	(
<g/>
Národního	národní	k2eAgInSc2d1	národní
památníku	památník	k1gInSc2	památník
Severozápadní	severozápadní	k2eAgInPc1d1	severozápadní
Havajské	havajský	k2eAgInPc1d1	havajský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
Světovému	světový	k2eAgNnSc3d1	světové
dědictví	dědictví	k1gNnSc3	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
rybářského	rybářský	k2eAgInSc2d1	rybářský
háčku	háček	k1gInSc2	háček
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kilometr	kilometr	k1gInSc4	kilometr
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
sto	sto	k4xCgNnSc1	sto
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
ostrova	ostrov	k1gInSc2	ostrov
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
84	[number]	k4	84
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
čedičem	čedič	k1gInSc7	čedič
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
zdroje	zdroj	k1gInPc1	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
hnízdištěm	hnízdiště	k1gNnSc7	hnízdiště
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
neobydlený	obydlený	k2eNgMnSc1d1	neobydlený
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
ale	ale	k8xC	ale
kamenné	kamenný	k2eAgFnPc1d1	kamenná
stavby	stavba	k1gFnPc1	stavba
svědčící	svědčící	k2eAgFnPc1d1	svědčící
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
Polynésané	Polynésan	k1gMnPc1	Polynésan
ostrov	ostrov	k1gInSc4	ostrov
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
kultovní	kultovní	k2eAgNnSc4d1	kultovní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vyhnání	vyhnání	k1gNnSc6	vyhnání
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
trpasličí	trpasličí	k2eAgInSc1d1	trpasličí
národ	národ	k1gInSc1	národ
Menehune	Menehun	k1gInSc5	Menehun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
Jean-François	Jean-François	k1gFnSc1	Jean-François
de	de	k?	de
La	la	k1gNnSc7	la
Pérouse	Pérouse	k1gFnSc2	Pérouse
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gNnSc4	on
podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Jacquese	Jacques	k1gMnSc2	Jacques
Neckera	Necker	k1gMnSc2	Necker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Neckerův	Neckerův	k2eAgInSc4d1	Neckerův
ostrov	ostrov	k1gInSc4	ostrov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.janeresture.com/necker/	[url]	k?	http://www.janeresture.com/necker/
</s>
</p>
<p>
<s>
https://web.archive.org/web/20090618065801/http://www.hawaiianatolls.org/about/mokumanamana.php	[url]	k1gMnSc1	https://web.archive.org/web/20090618065801/http://www.hawaiianatolls.org/about/mokumanamana.php
</s>
</p>
