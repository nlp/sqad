<s>
Hodina	hodina	k1gFnSc1	hodina
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
h	h	k?	h
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
hodina	hodina	k1gFnSc1	hodina
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
vymykající	vymykající	k2eAgFnSc7d1	vymykající
se	se	k3xPyFc4	se
desítkovému	desítkový	k2eAgInSc3d1	desítkový
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
povolena	povolen	k2eAgFnSc1d1	povolena
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
1	[number]	k4	1
h	h	k?	h
=	=	kIx~	=
60	[number]	k4	60
min	mina	k1gFnPc2	mina
=	=	kIx~	=
3600	[number]	k4	3600
s	s	k7c7	s
1	[number]	k4	1
d	d	k?	d
(	(	kIx(	(
<g/>
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
=	=	kIx~	=
24	[number]	k4	24
h	h	k?	h
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
hodiny	hodina	k1gFnSc2	hodina
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
označení	označení	k1gNnSc1	označení
hodiny	hodina	k1gFnSc2	hodina
značkou	značka	k1gFnSc7	značka
"	"	kIx"	"
<g/>
h	h	k?	h
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hodina	hodina	k1gFnSc1	hodina
je	být	k5eAaImIp3nS	být
také	také	k9	také
jednotkou	jednotka	k1gFnSc7	jednotka
rektascenze	rektascenze	k1gFnSc2	rektascenze
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jednotkou	jednotka	k1gFnSc7	jednotka
času	čas	k1gInSc2	čas
a	a	k8xC	a
úhlu	úhel	k1gInSc2	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Hodina	hodina	k1gFnSc1	hodina
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
definována	definovat	k5eAaBmNgFnS	definovat
starověkými	starověký	k2eAgFnPc7d1	starověká
civilizacemi	civilizace	k1gFnPc7	civilizace
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
takových	takový	k3xDgFnPc2	takový
jako	jako	k9	jako
byl	být	k5eAaImAgInS	být
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Sumer	Sumer	k1gInSc1	Sumer
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
buď	buď	k8xC	buď
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
dvanáctina	dvanáctina	k1gFnSc1	dvanáctina
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
východem	východ	k1gInSc7	východ
a	a	k8xC	a
západem	západ	k1gInSc7	západ
slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
jedna	jeden	k4xCgFnSc1	jeden
čtyřiadvacetina	čtyřiadvacetin	k2eAgFnSc1d1	čtyřiadvacetina
celého	celý	k2eAgInSc2d1	celý
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
odráželo	odrážet	k5eAaImAgNnS	odrážet
rozšíření	rozšíření	k1gNnSc3	rozšíření
používání	používání	k1gNnSc2	používání
dvanáctkové	dvanáctkový	k2eAgFnSc2d1	dvanáctková
číselné	číselný	k2eAgFnSc2d1	číselná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Důležitost	důležitost	k1gFnSc1	důležitost
číslu	číslo	k1gNnSc3	číslo
12	[number]	k4	12
byla	být	k5eAaImAgFnS	být
připisována	připisován	k2eAgFnSc1d1	připisována
díky	díky	k7c3	díky
počtu	počet	k1gInSc3	počet
měsíčních	měsíční	k2eAgInPc2d1	měsíční
cyklů	cyklus	k1gInPc2	cyklus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
a	a	k8xC	a
také	také	k9	také
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
12	[number]	k4	12
prstních	prstní	k2eAgFnPc2d1	prstní
kůstek	kůstka	k1gFnPc2	kůstka
(	(	kIx(	(
<g/>
článků	článek	k1gInPc2	článek
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
3	[number]	k4	3
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
prstů	prst	k1gInPc2	prst
s	s	k7c7	s
vyloučením	vyloučení	k1gNnSc7	vyloučení
palce	palec	k1gInSc2	palec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
počítat	počítat	k5eAaImF	počítat
do	do	k7c2	do
12	[number]	k4	12
dotýkáním	dotýkání	k1gNnPc3	dotýkání
se	se	k3xPyFc4	se
palce	palec	k1gInPc4	palec
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
článků	článek	k1gInPc2	článek
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Rozšířeny	rozšířen	k2eAgFnPc1d1	rozšířena
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
různé	různý	k2eAgFnPc1d1	různá
analogie	analogie	k1gFnPc1	analogie
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
množinami	množina	k1gFnPc7	množina
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
12	[number]	k4	12
zvířetníkových	zvířetníkový	k2eAgNnPc2d1	zvířetníkové
znamení	znamení	k1gNnPc2	znamení
<g/>
,	,	kIx,	,
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
tucet	tucet	k1gInSc1	tucet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1	starověká
egyptská	egyptský	k2eAgFnSc1d1	egyptská
civilizace	civilizace	k1gFnSc1	civilizace
byla	být	k5eAaImAgFnS	být
známá	známá	k1gFnSc1	známá
zavedením	zavedení	k1gNnSc7	zavedení
dělení	dělení	k1gNnSc2	dělení
noci	noc	k1gFnSc2	noc
na	na	k7c4	na
12	[number]	k4	12
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
různé	různý	k2eAgFnPc1d1	různá
varianty	varianta	k1gFnPc1	varianta
tohoto	tento	k3xDgNnSc2	tento
dělení	dělení	k1gNnSc2	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
Střední	střední	k2eAgFnSc2d1	střední
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
9	[number]	k4	9
a	a	k8xC	a
10	[number]	k4	10
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
zabývali	zabývat	k5eAaImAgMnP	zabývat
pozorováním	pozorování	k1gNnSc7	pozorování
36	[number]	k4	36
hvězd	hvězda	k1gFnPc2	hvězda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dekanů	dekan	k1gInPc2	dekan
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
tabulky	tabulka	k1gFnPc1	tabulka
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
na	na	k7c6	na
víkách	víko	k1gNnPc6	víko
rakví	rakev	k1gFnPc2	rakev
z	z	k7c2	z
daného	daný	k2eAgNnSc2d1	dané
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Vstupem	vstup	k1gInSc7	vstup
slunce	slunce	k1gNnSc2	slunce
do	do	k7c2	do
následujícího	následující	k2eAgInSc2d1	následující
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
dekanu	dekan	k1gInSc2	dekan
začínal	začínat	k5eAaImAgInS	začínat
nový	nový	k2eAgInSc1d1	nový
civilní	civilní	k2eAgInSc1d1	civilní
týden	týden	k1gInSc1	týden
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
úsek	úsek	k1gInSc1	úsek
mezi	mezi	k7c7	mezi
západem	západ	k1gInSc7	západ
a	a	k8xC	a
východem	východ	k1gInSc7	východ
Slunce	slunce	k1gNnSc2	slunce
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
18	[number]	k4	18
dekanskými	dekanský	k2eAgFnPc7d1	dekanský
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
hvězd	hvězda	k1gFnPc2	hvězda
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
přiřazeny	přiřazen	k2eAgInPc1d1	přiřazen
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
soumračných	soumračný	k2eAgFnPc2d1	Soumračná
period	perioda	k1gFnPc2	perioda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
samotnou	samotný	k2eAgFnSc4d1	samotná
periodu	perioda	k1gFnSc4	perioda
noci	noc	k1gFnSc2	noc
připadlo	připadnout	k5eAaPmAgNnS	připadnout
zbylých	zbylý	k2eAgInPc2d1	zbylý
12	[number]	k4	12
hvězd	hvězda	k1gFnPc2	hvězda
dekanu	dekan	k1gInSc2	dekan
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
dělení	dělení	k1gNnSc3	dělení
noci	noc	k1gFnSc2	noc
na	na	k7c4	na
12	[number]	k4	12
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
objevením	objevení	k1gNnSc7	objevení
se	se	k3xPyFc4	se
každé	každý	k3xTgNnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
hvězd	hvězda	k1gFnPc2	hvězda
dekanu	dekan	k1gInSc2	dekan
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
noci	noc	k1gFnSc2	noc
činila	činit	k5eAaImAgFnS	činit
asi	asi	k9	asi
40	[number]	k4	40
současných	současný	k2eAgFnPc2d1	současná
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
éry	éra	k1gFnSc2	éra
Nového	Nového	k2eAgNnSc2d1	Nového
království	království	k1gNnSc2	království
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
zjednodušen	zjednodušit	k5eAaPmNgInS	zjednodušit
používáním	používání	k1gNnSc7	používání
24	[number]	k4	24
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
12	[number]	k4	12
označovalo	označovat	k5eAaImAgNnS	označovat
období	období	k1gNnSc1	období
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgFnPc1d1	dřívější
definice	definice	k1gFnPc1	definice
hodiny	hodina	k1gFnPc1	hodina
jsou	být	k5eAaImIp3nP	být
různými	různý	k2eAgFnPc7d1	různá
variacemi	variace	k1gFnPc7	variace
následujících	následující	k2eAgInPc2d1	následující
parametrů	parametr	k1gInPc2	parametr
<g/>
:	:	kIx,	:
Jedna	jeden	k4xCgFnSc1	jeden
dvanáctina	dvanáctina	k1gFnSc1	dvanáctina
doby	doba	k1gFnSc2	doba
od	od	k7c2	od
východu	východ	k1gInSc2	východ
po	po	k7c4	po
západ	západ	k1gInSc4	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgInSc2	ten
byly	být	k5eAaImAgFnP	být
hodiny	hodina	k1gFnPc1	hodina
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
dnech	den	k1gInPc6	den
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
ve	v	k7c6	v
dnech	den	k1gInPc6	den
zimních	zimní	k2eAgInPc2d1	zimní
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
délka	délka	k1gFnSc1	délka
kolísala	kolísat	k5eAaImAgFnS	kolísat
se	s	k7c7	s
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
malinko	malinko	k6eAd1	malinko
lišily	lišit	k5eAaImAgFnP	lišit
díky	díky	k7c3	díky
lokálním	lokální	k2eAgInPc3d1	lokální
výkyvům	výkyv	k1gInPc3	výkyv
počasí	počasí	k1gNnSc2	počasí
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
na	na	k7c4	na
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
takové	takový	k3xDgFnPc1	takový
hodiny	hodina	k1gFnPc1	hodina
někdy	někdy	k6eAd1	někdy
nazývají	nazývat	k5eAaImIp3nP	nazývat
temporální	temporální	k2eAgInPc4d1	temporální
<g/>
,	,	kIx,	,
sezónní	sezónní	k2eAgInPc4d1	sezónní
nebo	nebo	k8xC	nebo
nestejné	stejný	k2eNgInPc4d1	nestejný
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
definici	definice	k1gFnSc4	definice
používali	používat	k5eAaImAgMnP	používat
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
starověcí	starověký	k2eAgMnPc1d1	starověký
Číňané	Číňan	k1gMnPc1	Číňan
a	a	k8xC	a
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
také	také	k9	také
rozdělovali	rozdělovat	k5eAaImAgMnP	rozdělovat
noc	noc	k1gFnSc4	noc
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
nebo	nebo	k8xC	nebo
čtyř	čtyři	k4xCgFnPc2	čtyři
hlídek	hlídka	k1gFnPc2	hlídka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
noc	noc	k1gFnSc4	noc
(	(	kIx(	(
<g/>
čas	čas	k1gInSc4	čas
mezi	mezi	k7c7	mezi
západem	západ	k1gInSc7	západ
a	a	k8xC	a
východem	východ	k1gInSc7	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
také	také	k9	také
dělena	dělen	k2eAgFnSc1d1	dělena
do	do	k7c2	do
dvanácti	dvanáct	k4xCc2	dvanáct
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k6eAd1	pokud
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
hodiny	hodina	k1gFnPc4	hodina
ukazovat	ukazovat	k5eAaImF	ukazovat
takovýto	takovýto	k3xDgInSc4	takovýto
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
měnit	měnit	k5eAaImF	měnit
každé	každý	k3xTgNnSc1	každý
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
večer	večer	k6eAd1	večer
perioda	perioda	k1gFnSc1	perioda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výměnou	výměna	k1gFnSc7	výměna
délky	délka	k1gFnSc2	délka
jejich	jejich	k3xOp3gNnSc2	jejich
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
schopny	schopen	k2eAgInPc1d1	schopen
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
aktuální	aktuální	k2eAgFnSc4d1	aktuální
polohu	poloha	k1gFnSc4	poloha
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c6	na
ekliptice	ekliptika	k1gFnSc6	ekliptika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
čtyřiadvacetina	čtyřiadvacetin	k2eAgFnSc1d1	čtyřiadvacetina
zdánlivého	zdánlivý	k2eAgInSc2d1	zdánlivý
slunečního	sluneční	k2eAgInSc2d1	sluneční
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
jedním	jeden	k4xCgNnSc7	jeden
a	a	k8xC	a
následujícím	následující	k2eAgNnSc7d1	následující
polednem	poledne	k1gNnSc7	poledne
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jedním	jeden	k4xCgNnSc7	jeden
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
délka	délka	k1gFnSc1	délka
hodiny	hodina	k1gFnSc2	hodina
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
během	během	k7c2	během
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tyto	tento	k3xDgFnPc4	tento
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
adjustován	adjustovat	k5eAaBmNgMnS	adjustovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hodiny	hodina	k1gFnPc1	hodina
byly	být	k5eAaImAgFnP	být
někdy	někdy	k6eAd1	někdy
nazývány	nazývat	k5eAaImNgFnP	nazývat
ekvinokciální	ekvinokciální	k2eAgFnPc1d1	ekvinokciální
hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
čtyřiadvacetina	čtyřiadvacetin	k2eAgFnSc1d1	čtyřiadvacetina
středního	střední	k2eAgInSc2d1	střední
slunečního	sluneční	k2eAgInSc2d1	sluneční
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Podívejte	podívat	k5eAaPmRp2nP	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
střední	střední	k2eAgInSc4d1	střední
sluneční	sluneční	k2eAgInSc4d1	sluneční
čas	čas	k1gInSc4	čas
ohledně	ohledně	k7c2	ohledně
rozdílu	rozdíl	k1gInSc2	rozdíl
vůči	vůči	k7c3	vůči
zdánlivému	zdánlivý	k2eAgInSc3d1	zdánlivý
slunečnímu	sluneční	k2eAgInSc3d1	sluneční
dni	den	k1gInSc3	den
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
přesné	přesný	k2eAgFnPc1d1	přesná
hodiny	hodina	k1gFnPc1	hodina
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
nemusejí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
fakticky	fakticky	k6eAd1	fakticky
nikdy	nikdy	k6eAd1	nikdy
adjustovány	adjustován	k2eAgFnPc1d1	adjustován
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k6eAd1	přesto
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
zpomalování	zpomalování	k1gNnSc3	zpomalování
rotace	rotace	k1gFnSc2	rotace
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
opouštěna	opouštěn	k2eAgFnSc1d1	opouštěna
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
způsobů	způsob	k1gInPc2	způsob
jak	jak	k6eAd1	jak
počítat	počítat	k5eAaImF	počítat
hodiny	hodina	k1gFnPc4	hodina
<g/>
:	:	kIx,	:
V	v	k7c6	v
kulturách	kultura	k1gFnPc6	kultura
starověkého	starověký	k2eAgInSc2d1	starověký
a	a	k8xC	a
středověkého	středověký	k2eAgInSc2d1	středověký
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
více	hodně	k6eAd2	hodně
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c4	na
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
než	než	k8xS	než
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
používajících	používající	k2eAgInPc2d1	používající
umělé	umělý	k2eAgNnSc4d1	umělé
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
,	,	kIx,	,
počítání	počítání	k1gNnSc1	počítání
hodin	hodina	k1gFnPc2	hodina
začínalo	začínat	k5eAaImAgNnS	začínat
východem	východ	k1gInSc7	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Východ	východ	k1gInSc1	východ
Slunce	slunce	k1gNnSc2	slunce
tak	tak	k6eAd1	tak
vždy	vždy	k6eAd1	vždy
připadl	připadnout	k5eAaPmAgMnS	připadnout
na	na	k7c4	na
první	první	k4xOgFnSc4	první
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
poledne	poledne	k1gNnSc4	poledne
na	na	k7c4	na
konec	konec	k1gInSc4	konec
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
západ	západ	k1gInSc1	západ
Slunce	slunce	k1gNnSc2	slunce
vycházel	vycházet	k5eAaImAgInS	vycházet
na	na	k7c4	na
konec	konec	k1gInSc4	konec
hodiny	hodina	k1gFnSc2	hodina
dvanácté	dvanáctý	k4xOgFnSc6	dvanáctý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
délka	délka	k1gFnSc1	délka
hodiny	hodina	k1gFnSc2	hodina
kolísala	kolísat	k5eAaImAgFnS	kolísat
podle	podle	k7c2	podle
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
počítání	počítání	k1gNnSc2	počítání
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
astrolábech	astroláb	k1gInPc6	astroláb
nebo	nebo	k8xC	nebo
orlojích	orloj	k1gInPc6	orloj
např.	např.	kA	např.
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Babylónské	babylónský	k2eAgFnPc4d1	Babylónská
<g/>
"	"	kIx"	"
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
židovském	židovský	k2eAgNnSc6d1	Židovské
náboženském	náboženský	k2eAgNnSc6d1	náboženské
právu	právo	k1gNnSc6	právo
(	(	kIx(	(
<g/>
Halacha	Halacha	k1gMnSc1	Halacha
<g/>
)	)	kIx)	)
a	a	k8xC	a
židovských	židovský	k2eAgInPc6d1	židovský
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
italském	italský	k2eAgInSc6d1	italský
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
italských	italský	k2eAgFnPc6d1	italská
hodinách	hodina	k1gFnPc6	hodina
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
vlašských	vlašský	k2eAgFnPc6d1	vlašská
hodinách	hodina	k1gFnPc6	hodina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
začínala	začínat	k5eAaImAgFnS	začínat
první	první	k4xOgFnSc1	první
hodina	hodina	k1gFnSc1	hodina
navečer	navečer	k6eAd1	navečer
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
šera	šero	k1gNnSc2	šero
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
po	po	k7c6	po
západu	západ	k1gInSc6	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
místních	místní	k2eAgFnPc6d1	místní
zvyklostech	zvyklost	k1gFnPc6	zvyklost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
kanonický	kanonický	k2eAgInSc4d1	kanonický
začátek	začátek	k1gInSc4	začátek
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
byly	být	k5eAaImAgFnP	být
číslovány	číslovat	k5eAaImNgFnP	číslovat
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Luganu	Lugan	k1gInSc6	Lugan
svítání	svítání	k1gNnSc2	svítání
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
připadlo	připadnout	k5eAaPmAgNnS	připadnout
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
poledne	poledne	k1gNnSc4	poledne
pak	pak	k6eAd1	pak
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
hodinu	hodina	k1gFnSc4	hodina
<g/>
;	;	kIx,	;
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
svítání	svítání	k1gNnSc2	svítání
připadlo	připadnout	k5eAaPmAgNnS	připadnout
na	na	k7c4	na
7	[number]	k4	7
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
poledne	poledne	k1gNnSc4	poledne
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
Slunce	slunce	k1gNnSc2	slunce
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
24	[number]	k4	24
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
počítání	počítání	k1gNnSc1	počítání
hodin	hodina	k1gFnPc2	hodina
mělo	mít	k5eAaImAgNnS	mít
tu	tu	k6eAd1	tu
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
měl	mít	k5eAaImAgMnS	mít
přehled	přehled	k1gInSc4	přehled
kolik	kolika	k1gFnPc2	kolika
času	čas	k1gInSc2	čas
mu	on	k3xPp3gMnSc3	on
zbývá	zbývat	k5eAaImIp3nS	zbývat
na	na	k7c6	na
dokončení	dokončení	k1gNnSc6	dokončení
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
časové	časový	k2eAgFnSc6d1	časová
mzdě	mzda	k1gFnSc6	mzda
za	za	k7c2	za
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
počítání	počítání	k1gNnSc2	počítání
hodin	hodina	k1gFnPc2	hodina
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
dokonce	dokonce	k9	dokonce
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
nazýván	nazývat	k5eAaImNgInS	nazývat
českými	český	k2eAgFnPc7d1	Česká
hodinami	hodina	k1gFnPc7	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
staletích	staletí	k1gNnPc6	staletí
byl	být	k5eAaImAgInS	být
vytlačován	vytlačován	k2eAgInSc1d1	vytlačován
hodinami	hodina	k1gFnPc7	hodina
německými	německý	k2eAgFnPc7d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
italských	italský	k2eAgFnPc2d1	italská
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
hodinách	hodina	k1gFnPc6	hodina
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gInSc1	jejich
číselník	číselník	k1gInSc1	číselník
je	být	k5eAaImIp3nS	být
číslován	číslovat	k5eAaImNgInS	číslovat
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
24	[number]	k4	24
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
nebo	nebo	k8xC	nebo
arabských	arabský	k2eAgFnPc6d1	arabská
číslicích	číslice	k1gFnPc6	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Hodinové	hodinový	k2eAgInPc1d1	hodinový
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
upravovány	upravován	k2eAgInPc4d1	upravován
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
začátek	začátek	k1gInSc1	začátek
první	první	k4xOgFnSc2	první
hodiny	hodina	k1gFnSc2	hodina
odchyloval	odchylovat	k5eAaImAgInS	odchylovat
od	od	k7c2	od
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
alespoň	alespoň	k9	alespoň
o	o	k7c4	o
čtvrt	čtvrt	k1gFnSc4	čtvrt
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
byly	být	k5eAaImAgFnP	být
sestavovány	sestavován	k2eAgFnPc4d1	sestavována
speciální	speciální	k2eAgFnPc4d1	speciální
tabulky	tabulka	k1gFnPc4	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
umístěné	umístěný	k2eAgFnSc2d1	umístěná
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Středověký	středověký	k2eAgInSc1d1	středověký
islámský	islámský	k2eAgInSc1d1	islámský
den	den	k1gInSc1	den
začínal	začínat	k5eAaImAgInS	začínat
se	s	k7c7	s
západem	západ	k1gInSc7	západ
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
modlitba	modlitba	k1gFnSc1	modlitba
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
maghrib	maghrib	k1gInSc1	maghrib
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
mezi	mezi	k7c7	mezi
západem	západ	k1gInSc7	západ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
koncem	koncem	k7c2	koncem
soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
jihoněmeckých	jihoněmecký	k2eAgNnPc6d1	jihoněmecké
městech	město	k1gNnPc6	město
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
či	či	k8xC	či
Řezně	Řezno	k1gNnSc6	Řezno
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
používány	používat	k5eAaImNgFnP	používat
tzv.	tzv.	kA	tzv.
velké	velký	k2eAgFnPc4d1	velká
hodiny	hodina	k1gFnPc4	hodina
(	(	kIx(	(
<g/>
Große	Große	k1gNnSc4	Große
Uhr	Uhr	k1gFnSc2	Uhr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počítaly	počítat	k5eAaImAgFnP	počítat
se	se	k3xPyFc4	se
zvlášť	zvlášť	k6eAd1	zvlášť
hodiny	hodina	k1gFnPc1	hodina
denní	denní	k2eAgFnPc1d1	denní
a	a	k8xC	a
zvlášť	zvlášť	k6eAd1	zvlášť
hodiny	hodina	k1gFnPc1	hodina
noční	noční	k2eAgFnPc1d1	noční
<g/>
.	.	kIx.	.
</s>
<s>
Zvoněním	zvonění	k1gNnSc7	zvonění
zvaným	zvaný	k2eAgInSc7d1	zvaný
garaus	garaus	k1gInSc1	garaus
pak	pak	k6eAd1	pak
den	den	k1gInSc4	den
začínal	začínat	k5eAaImAgMnS	začínat
a	a	k8xC	a
také	také	k6eAd1	také
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
ukončoval	ukončovat	k5eAaImAgMnS	ukončovat
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
a	a	k8xC	a
noc	noc	k1gFnSc1	noc
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
lišily	lišit	k5eAaImAgFnP	lišit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
ročních	roční	k2eAgFnPc6d1	roční
dobách	doba	k1gFnPc6	doba
počtem	počet	k1gInSc7	počet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
měl	mít	k5eAaImAgInS	mít
den	den	k1gInSc1	den
pouze	pouze	k6eAd1	pouze
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
věžní	věžní	k2eAgFnPc1d1	věžní
hodiny	hodina	k1gFnPc1	hodina
tak	tak	k6eAd1	tak
odbíjely	odbíjet	k5eAaImAgFnP	odbíjet
přes	přes	k7c4	přes
den	den	k1gInSc4	den
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
měl	mít	k5eAaImAgInS	mít
den	den	k1gInSc1	den
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
věžní	věžní	k2eAgFnPc1d1	věžní
hodiny	hodina	k1gFnPc1	hodina
odbíjely	odbíjet	k5eAaImAgFnP	odbíjet
přes	přes	k7c4	přes
den	den	k1gInSc4	den
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
pak	pak	k6eAd1	pak
hodiny	hodina	k1gFnPc1	hodina
odbíjely	odbíjet	k5eAaImAgFnP	odbíjet
opět	opět	k6eAd1	opět
od	od	k7c2	od
1	[number]	k4	1
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Trvání	trvání	k1gNnSc1	trvání
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
se	se	k3xPyFc4	se
měnilo	měnit	k5eAaImAgNnS	měnit
asi	asi	k9	asi
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
a	a	k8xC	a
půl	půl	k1xP	půl
týdnech	týden	k1gInPc6	týden
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nařízení	nařízení	k1gNnSc1	nařízení
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
moderního	moderní	k2eAgInSc2d1	moderní
dvanáctihodinového	dvanáctihodinový	k2eAgInSc2d1	dvanáctihodinový
hodinového	hodinový	k2eAgInSc2d1	hodinový
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hodiny	hodina	k1gFnPc1	hodina
začínají	začínat	k5eAaImIp3nP	začínat
počítat	počítat	k5eAaImF	počítat
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
a	a	k8xC	a
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
jedničky	jednička	k1gFnSc2	jednička
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
jsou	být	k5eAaImIp3nP	být
číslovány	číslován	k2eAgFnPc1d1	číslována
12	[number]	k4	12
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnSc4d1	sluneční
poledne	poledne	k1gNnSc4	poledne
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k6eAd1	poblíž
poledne	poledne	k1gNnSc4	poledne
ve	v	k7c4	v
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
podle	podle	k7c2	podle
časové	časový	k2eAgFnSc2d1	časová
rovnice	rovnice	k1gFnSc2	rovnice
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
připadá	připadat	k5eAaPmIp3nS	připadat
východ	východ	k1gInSc1	východ
slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
6	[number]	k4	6
A.	A.	kA	A.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
ante	antat	k5eAaPmIp3nS	antat
meridiem	meridium	k1gNnSc7	meridium
<g/>
,	,	kIx,	,
před	před	k7c7	před
polednem	poledne	k1gNnSc7	poledne
<g/>
)	)	kIx)	)
a	a	k8xC	a
západ	západ	k1gInSc1	západ
slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
6	[number]	k4	6
P.	P.	kA	P.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
post	post	k1gInSc1	post
meridiem	meridium	k1gNnSc7	meridium
<g/>
,	,	kIx,	,
po	po	k7c6	po
poledni	poledne	k1gNnSc6	poledne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
počítání	počítání	k1gNnSc1	počítání
pronikal	pronikat	k5eAaImAgInS	pronikat
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stal	stát	k5eAaPmAgInS	stát
nejobvyklejším	obvyklý	k2eAgInSc7d3	nejobvyklejší
způsobem	způsob	k1gInSc7	způsob
určování	určování	k1gNnSc2	určování
denního	denní	k2eAgInSc2d1	denní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgMnS	šířit
do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahrazoval	nahrazovat	k5eAaImAgInS	nahrazovat
postupně	postupně	k6eAd1	postupně
italské	italský	k2eAgFnPc1d1	italská
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
nazýván	nazýván	k2eAgInSc1d1	nazýván
německým	německý	k2eAgInSc7d1	německý
časem	čas	k1gInSc7	čas
nebo	nebo	k8xC	nebo
německými	německý	k2eAgFnPc7d1	německá
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
moderních	moderní	k2eAgInPc6d1	moderní
hodinových	hodinový	k2eAgInPc6d1	hodinový
strojích	stroj	k1gInPc6	stroj
o	o	k7c6	o
24	[number]	k4	24
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
hodiny	hodina	k1gFnPc1	hodina
začínají	začínat	k5eAaImIp3nP	začínat
počítat	počítat	k5eAaImF	počítat
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
číslovány	číslovat	k5eAaImNgInP	číslovat
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnSc4d1	sluneční
poledne	poledne	k1gNnSc4	poledne
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
okolo	okolo	k7c2	okolo
12	[number]	k4	12
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
časové	časový	k2eAgFnSc2d1	časová
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
slunce	slunce	k1gNnSc2	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
počítání	počítání	k1gNnSc2	počítání
okolo	okolo	k7c2	okolo
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
zapadá	zapadat	k5eAaImIp3nS	zapadat
v	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
počítali	počítat	k5eAaImAgMnP	počítat
astronomové	astronom	k1gMnPc1	astronom
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
dny	den	k1gInPc4	den
od	od	k7c2	od
poledne	poledne	k1gNnSc2	poledne
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
usnadněno	usnadněn	k2eAgNnSc1d1	usnadněno
přesné	přesný	k2eAgNnSc1d1	přesné
měření	měření	k1gNnSc1	měření
slunečních	sluneční	k2eAgFnPc2d1	sluneční
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
(	(	kIx(	(
<g/>
užívané	užívaný	k2eAgNnSc1d1	užívané
v	v	k7c6	v
Juliánském	juliánský	k2eAgInSc6d1	juliánský
datovacím	datovací	k2eAgInSc6d1	datovací
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nový	nový	k2eAgInSc4d1	nový
den	den	k1gInSc4	den
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
datum	datum	k1gNnSc1	datum
dne	den	k1gInSc2	den
neměnilo	měnit	k5eNaImAgNnS	měnit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
nočního	noční	k2eAgNnSc2d1	noční
pozorování	pozorování	k1gNnSc2	pozorování
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Východ	východ	k1gInSc4	východ
a	a	k8xC	a
západ	západ	k1gInSc4	západ
slunce	slunce	k1gNnSc2	slunce
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
snáze	snadno	k6eAd2	snadno
rozeznatelné	rozeznatelný	k2eAgInPc4d1	rozeznatelný
časové	časový	k2eAgInPc4d1	časový
body	bod	k1gInPc4	bod
dne	den	k1gInSc2	den
než	než	k8xS	než
poledne	poledne	k1gNnSc4	poledne
nebo	nebo	k8xC	nebo
půlnoc	půlnoc	k1gFnSc4	půlnoc
<g/>
;	;	kIx,	;
začátek	začátek	k1gInSc1	začátek
počítání	počítání	k1gNnSc1	počítání
hodin	hodina	k1gFnPc2	hodina
od	od	k7c2	od
těchto	tento	k3xDgInPc2	tento
bodů	bod	k1gInPc2	bod
bylo	být	k5eAaImAgNnS	být
proto	proto	k6eAd1	proto
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
většiny	většina	k1gFnSc2	většina
společností	společnost	k1gFnPc2	společnost
mnohem	mnohem	k6eAd1	mnohem
snazší	snadný	k2eAgNnSc4d2	snazší
než	než	k8xS	než
počítání	počítání	k1gNnSc4	počítání
hodin	hodina	k1gFnPc2	hodina
od	od	k7c2	od
poledne	poledne	k1gNnSc2	poledne
nebo	nebo	k8xC	nebo
od	od	k7c2	od
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
s	s	k7c7	s
moderním	moderní	k2eAgNnSc7d1	moderní
astronomickým	astronomický	k2eAgNnSc7d1	astronomické
vybavením	vybavení	k1gNnSc7	vybavení
(	(	kIx(	(
<g/>
a	a	k8xC	a
telegrafem	telegraf	k1gInSc7	telegraf
či	či	k8xC	či
podobným	podobný	k2eAgNnSc7d1	podobné
zařízením	zařízení	k1gNnSc7	zařízení
schopným	schopný	k2eAgNnSc7d1	schopné
přenášet	přenášet	k5eAaImF	přenášet
časové	časový	k2eAgInPc4d1	časový
signály	signál	k1gInPc4	signál
ve	v	k7c6	v
zlomcích	zlomek	k1gInPc6	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
přestal	přestat	k5eAaPmAgMnS	přestat
být	být	k5eAaImF	být
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
závažným	závažný	k2eAgInSc7d1	závažný
<g/>
.	.	kIx.	.
</s>
<s>
Astroláby	Astrolába	k1gFnPc1	Astrolába
<g/>
,	,	kIx,	,
sluneční	sluneční	k2eAgFnPc1d1	sluneční
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
orloje	orloj	k1gInPc1	orloj
občas	občas	k6eAd1	občas
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
délku	délka	k1gFnSc4	délka
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
počítají	počítat	k5eAaImIp3nP	počítat
je	on	k3xPp3gFnPc4	on
podle	podle	k7c2	podle
starých	starý	k2eAgFnPc2d1	stará
definic	definice	k1gFnPc2	definice
a	a	k8xC	a
počítacích	počítací	k2eAgFnPc2d1	počítací
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
hodina	hodina	k1gFnSc1	hodina
se	se	k3xPyFc4	se
také	také	k9	také
požívá	požívat	k5eAaImIp3nS	požívat
jako	jako	k9	jako
následující	následující	k2eAgInPc4d1	následující
termíny	termín	k1gInPc4	termín
<g/>
:	:	kIx,	:
Vyučovací	vyučovací	k2eAgFnSc1d1	vyučovací
hodina	hodina	k1gFnSc1	hodina
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
např.	např.	kA	např.
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
)	)	kIx)	)
Temporální	temporální	k2eAgFnSc1d1	temporální
hodina	hodina	k1gFnSc1	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
mající	mající	k2eAgFnSc4d1	mající
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
délku	délka	k1gFnSc4	délka
ve	v	k7c6	v
dne	den	k1gInSc2	den
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
také	také	k9	také
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc4d1	vznikající
dělením	dělení	k1gNnSc7	dělení
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
svítáním	svítání	k1gNnSc7	svítání
a	a	k8xC	a
západem	západ	k1gInSc7	západ
slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
12	[number]	k4	12
stejných	stejný	k2eAgFnPc2d1	stejná
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
revoluční	revoluční	k2eAgInSc1d1	revoluční
kalendář	kalendář	k1gInSc1	kalendář
dělil	dělit	k5eAaImAgInS	dělit
den	den	k1gInSc4	den
na	na	k7c4	na
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
skládajících	skládající	k2eAgFnPc2d1	skládající
se	s	k7c7	s
ze	z	k7c2	z
100	[number]	k4	100
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
100	[number]	k4	100
sekundách	sekunda	k1gFnPc6	sekunda
<g/>
,	,	kIx,	,
revoluční	revoluční	k2eAgFnSc1d1	revoluční
hodina	hodina	k1gFnSc1	hodina
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
zcela	zcela	k6eAd1	zcela
jiným	jiný	k2eAgInSc7d1	jiný
časovým	časový	k2eAgInSc7d1	časový
intervalem	interval	k1gInSc7	interval
než	než	k8xS	než
naše	náš	k3xOp1gFnSc1	náš
dnešní	dnešní	k2eAgFnSc1d1	dnešní
hodina	hodina	k1gFnSc1	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecném	obecný	k2eAgNnSc6d1	obecné
jazykovém	jazykový	k2eAgNnSc6d1	jazykové
pojetí	pojetí	k1gNnSc6	pojetí
označující	označující	k2eAgMnSc1d1	označující
(	(	kIx(	(
<g/>
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
)	)	kIx)	)
časový	časový	k2eAgInSc1d1	časový
úsek	úsek	k1gInSc1	úsek
<g/>
,	,	kIx,	,
okamžik	okamžik	k1gInSc1	okamžik
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
významu	význam	k1gInSc2	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
hodina	hodina	k1gFnSc1	hodina
pravdy	pravda	k1gFnSc2	pravda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
udeřila	udeřit	k5eAaPmAgFnS	udeřit
něčí	něčí	k3xOyIgFnSc1	něčí
hodina	hodina	k1gFnSc1	hodina
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Kanonické	kanonický	k2eAgFnSc2d1	kanonická
hodiny	hodina	k1gFnSc2	hodina
24	[number]	k4	24
<g/>
-hodinové	odinový	k2eAgNnSc1d1	-hodinový
počítání	počítání	k1gNnSc1	počítání
Den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
Noc	noc	k1gFnSc1	noc
Týden	týden	k1gInSc1	týden
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
Rok	rok	k1gInSc1	rok
Minuta	minuta	k1gFnSc1	minuta
<g/>
,	,	kIx,	,
Sekunda	sekunda	k1gFnSc1	sekunda
Čas	čas	k1gInSc1	čas
S.	S.	kA	S.
I.	I.	kA	I.
Selešnikov	Selešnikov	k1gInSc1	Selešnikov
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
čas	čas	k1gInSc1	čas
-	-	kIx~	-
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Eva	Eva	k1gFnSc1	Eva
Kotulová	Kotulový	k2eAgFnSc1d1	Kotulová
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalendář	kalendář	k1gInSc1	kalendář
aneb	aneb	k?	aneb
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
věčnosti	věčnost	k1gFnSc6	věčnost
času	čas	k1gInSc2	čas
-	-	kIx~	-
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Marie	Marie	k1gFnSc1	Marie
Bláhová	Bláhová	k1gFnSc1	Bláhová
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historická	historický	k2eAgFnSc1d1	historická
chronologie	chronologie	k1gFnSc1	chronologie
-	-	kIx~	-
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Libri	Libri	k6eAd1	Libri
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7277-024-1	[number]	k4	80-7277-024-1
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hodina	hodina	k1gFnSc1	hodina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hodina	hodina	k1gFnSc1	hodina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
