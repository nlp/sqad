<s>
Gay	gay	k2gMnPc1
Američané	Američan	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
a	a	k8xC
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
potýkali	potýkat	k5eAaImAgMnP
s	s	k7c7
anti-gay	anti-gay	k2gMnSc7
právním	právní	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
P	P	kA
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</ref>
Rané	raný	k2eAgFnSc2d1
homofilní	homofilní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
gay	gay	k2gMnPc1
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
dokážou	dokázat	k5eAaPmIp3nP
asimilovat	asimilovat	k5eAaBmF
do	do	k7c2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
preferovaly	preferovat	k5eAaImAgFnP
nekonfliktní	konfliktní	k2eNgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
pro	pro	k7c4
homosexuály	homosexuál	k1gMnPc4
stejně	stejně	k6eAd1
jako	jako	k9
pro	pro	k7c4
heterosexuály	heterosexuál	k1gMnPc4
<g/>
.	.	kIx.
</s>