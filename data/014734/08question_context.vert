<s hack="1">
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgFnPc1
vládní	vládní	k2eAgFnPc1d1
zpravodajské	zpravodajský	k2eAgFnPc1d1
služby	služba	k1gFnPc1
„	„	k?
<g/>
jednomyslně	jednomyslně	k6eAd1
souhlasí	souhlasit	k5eAaImIp3nS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
sexuální	sexuální	k2eAgMnPc1d1
zvrhlíci	zvrhlík	k1gMnPc1
ve	v	k7c6
vládě	vláda	k1gFnSc6
představují	představovat	k5eAaImIp3nP
bezpečnostní	bezpečnostní	k2eAgNnSc4d1
riziko	riziko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1947	#num#	k4
a	a	k8xC
1950	#num#	k4
bylo	být	k5eAaImAgNnS
zamítnuto	zamítnout	k5eAaPmNgNnS
1	#num#	k4
700	#num#	k4
federálních	federální	k2eAgFnPc2d1
žádostí	žádost	k1gFnPc2
o	o	k7c6
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
4	#num#	k4
380	#num#	k4
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
propuštěno	propustit	k5eAaPmNgNnS
z	z	k7c2
armády	armáda	k1gFnSc2
a	a	k8xC
420	#num#	k4
bylo	být	k5eAaImAgNnS
vyhozeno	vyhozen	k2eAgNnSc1d1
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
vládních	vládní	k2eAgFnPc2d1
prací	práce	k1gFnPc2
kvůli	kvůli	k7c3
podezření	podezření	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
homosexuály	homosexuál	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
celé	celý	k2eAgNnSc4d1
období	období	k1gNnSc4
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
si	se	k3xPyFc3
federální	federální	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
vyšetřování	vyšetřování	k1gNnSc4
(	(	kIx(
<g/>
FBI	FBI	kA
<g/>
)	)	kIx)
a	a	k8xC
policejní	policejní	k2eAgMnPc1d1
oddělení	oddělený	k2eAgMnPc1d1
vedli	vést	k5eAaImAgMnP
seznamy	seznam	k1gInPc4
známých	známý	k2eAgMnPc2d1
homosexuálů	homosexuál	k1gMnPc2
<g/>
,	,	kIx,
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
upřednostňovali	upřednostňovat	k5eAaImAgMnP
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
přátel	přítel	k1gMnPc2
<g/>
;	;	kIx,
poštovní	poštovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
sledoval	sledovat	k5eAaImAgInS
<g/>
,	,	kIx,
kam	kam	k6eAd1
jsou	být	k5eAaImIp3nP
posílány	posílán	k2eAgInPc4d1
materiály	materiál	k1gInPc4
týkající	týkající	k2eAgInPc4d1
se	se	k3xPyFc4
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Státní	státní	k2eAgFnSc2d1
a	a	k8xC
místní	místní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
tento	tento	k3xDgInSc4
příklad	příklad	k1gInSc4
následovaly	následovat	k5eAaImAgInP
<g/>
:	:	kIx,
bary	bar	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
sloužily	sloužit	k5eAaImAgInP
homosexuálům	homosexuál	k1gMnPc3
byly	být	k5eAaImAgInP
zavřené	zavřený	k2eAgInPc4d1
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
zákazníci	zákazník	k1gMnPc1
byli	být	k5eAaImAgMnP
zatčení	zatčený	k2eAgMnPc1d1
a	a	k8xC
vystaveni	vystavit	k5eAaPmNgMnP
v	v	k7c6
novinách	novina	k1gFnPc6
<g/>
.	.	kIx.
</s>