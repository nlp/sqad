<s>
Stonewallské	Stonewallský	k2eAgInPc1d1
nepokoje	nepokoj	k1gInPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
Velmi	velmi	k6eAd1
špatný	špatný	k2eAgInSc1d1
překlad	překlad	k1gInSc1
<g/>
,	,	kIx,
stylisticky	stylisticky	k6eAd1
<g/>
,	,	kIx,
místy	místo	k1gNnPc7
i	i	k8xC
významově	významově	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Stonewallské	Stonewallský	k2eAgInPc1d1
nepokoje	nepokoj	k1gInPc1
(	(	kIx(
<g/>
známé	známý	k2eAgNnSc1d1
taky	taky	k6eAd1
jako	jako	k8xS,k8xC
Stonewallské	Stonewallský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
nebo	nebo	k8xC
Stonewallská	Stonewallský	k2eAgFnSc1d1
vzpoura	vzpoura	k1gFnSc1
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
sérií	série	k1gFnSc7
spontánních	spontánní	k2eAgFnPc2d1
<g/>
,	,	kIx,
násilných	násilný	k2eAgFnPc2d1
demonstrací	demonstrace	k1gFnPc2
členů	člen	k1gMnPc2
LGBT	LGBT	kA
<g/>
+	+	kIx~
komunity	komunita	k1gFnSc2
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
proti	proti	k7c3
policejní	policejní	k2eAgFnSc3d1
razii	razie	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
brzkých	brzký	k2eAgFnPc6d1
ranních	ranní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
v	v	k7c4
Stonewall	Stonewall	k1gInSc4
Inn	Inn	k1gFnSc2
v	v	k7c6
sousedství	sousedství	k1gNnSc6
Greenwich	Greenwich	k1gInSc4
Village	Villag	k1gInSc2
v	v	k7c6
Manhattanu	Manhattan	k1gInSc6
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouliční	pouliční	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
trvaly	trvat	k5eAaImAgFnP
několik	několik	k4yIc1
dní	den	k1gInPc2
<g/>
,	,	kIx,
do	do	k7c2
bojů	boj	k1gInPc2
se	se	k3xPyFc4
zapojilo	zapojit	k5eAaPmAgNnS
přes	přes	k7c4
2000	#num#	k4
protestujících	protestující	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgNnSc4
americké	americký	k2eAgNnSc4d1
(	(	kIx(
<g/>
a	a	k8xC
zřejmě	zřejmě	k6eAd1
i	i	k9
světové	světový	k2eAgFnSc3d1
<g/>
)	)	kIx)
vystoupení	vystoupení	k1gNnSc1
gayů	gay	k1gMnPc2
a	a	k8xC
leseb	lesba	k1gFnPc2
v	v	k7c6
boji	boj	k1gInSc6
za	za	k7c7
jejich	jejich	k3xOp3gInPc7
požadavky	požadavek	k1gInPc7
a	a	k8xC
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
uznáváno	uznávat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
nejdůležitější	důležitý	k2eAgFnSc1d3
událost	událost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
hnutí	hnutí	k1gNnSc3
osvobození	osvobození	k1gNnSc2
gayů	gay	k1gMnPc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
modernímu	moderní	k2eAgInSc3d1
boji	boj	k1gInSc3
za	za	k7c2
práva	právo	k1gNnSc2
LGBT	LGBT	kA
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
Allen	Allen	k1gMnSc1
Ginsberg	Ginsberg	k1gMnSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
ikonických	ikonický	k2eAgFnPc2d1
postav	postava	k1gFnPc2
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
komentoval	komentovat	k5eAaBmAgMnS
nepokoje	nepokoj	k1gInPc4
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
buzíci	buzík	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
ustrašenou	ustrašený	k2eAgFnSc4d1
tvář	tvář	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
měli	mít	k5eAaImAgMnP
po	po	k7c6
desetiletí	desetiletí	k1gNnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gay	gay	k2gMnPc1
Američané	Američan	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
a	a	k8xC
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
potýkali	potýkat	k5eAaImAgMnP
s	s	k7c7
anti-gay	anti-gay	k2gMnSc7
právním	právní	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
P	P	kA
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</ref>
Rané	raný	k2eAgFnSc2d1
homofilní	homofilní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
gay	gay	k2gMnPc1
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
dokážou	dokázat	k5eAaPmIp3nP
asimilovat	asimilovat	k5eAaBmF
do	do	k7c2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
preferovaly	preferovat	k5eAaImAgFnP
nekonfliktní	konfliktní	k2eNgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
pro	pro	k7c4
homosexuály	homosexuál	k1gMnPc4
stejně	stejně	k6eAd1
jako	jako	k9
pro	pro	k7c4
heterosexuály	heterosexuál	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěrečné	závěrečný	k2eAgNnSc4d1
období	období	k1gNnSc4
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
bylo	být	k5eAaImAgNnS
však	však	k9
velmi	velmi	k6eAd1
kontroverzní	kontroverzní	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
bylo	být	k5eAaImAgNnS
aktivních	aktivní	k2eAgFnPc2d1
mnoho	mnoho	k4c1
sociálních	sociální	k2eAgFnPc2d1
<g/>
/	/	kIx~
<g/>
politických	politický	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Afroamerického	afroamerický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
za	za	k7c4
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
kontrakultury	kontrakultura	k1gFnPc1
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
proti	proti	k7c3
válce	válka	k1gFnSc3
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
vlivy	vliv	k1gInPc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
prostředím	prostředí	k1gNnSc7
Greenwich	Greenwich	k1gInSc1
Village	Village	k1gInSc1
sloužily	sloužit	k5eAaImAgFnP
jako	jako	k8xC,k8xS
katalyzátor	katalyzátor	k1gInSc4
Stonewallských	Stonewallský	k2eAgInPc2d1
nepokojů	nepokoj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jen	jen	k9
málo	málo	k4c1
podniků	podnik	k1gInPc2
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
otevřeně	otevřeně	k6eAd1
vítalo	vítat	k5eAaImAgNnS
gay	gay	k1gMnSc1
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
ano	ano	k9
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
často	často	k6eAd1
bary	bar	k1gInPc1
<g/>
,	,	kIx,
přestože	přestože	k8xS
majitelé	majitel	k1gMnPc1
barů	bar	k1gInPc2
a	a	k8xC
manažeři	manažer	k1gMnPc1
byli	být	k5eAaImAgMnP
zřídka	zřídka	k6eAd1
sami	sám	k3xTgMnPc1
o	o	k7c4
sobě	se	k3xPyFc3
gayi	gay	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgInSc6
čase	čas	k1gInSc6
vlastnila	vlastnit	k5eAaImAgFnS
bar	bar	k1gInSc4
Stonewall	Stonewallum	k1gNnPc2
Inn	Inn	k1gMnSc2
mafie	mafie	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Sloužil	sloužit	k5eAaImAgMnS
široké	široký	k2eAgFnSc3d1
škále	škála	k1gFnSc3
klientů	klient	k1gMnPc2
a	a	k8xC
byl	být	k5eAaImAgMnS
známý	známý	k1gMnSc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
populární	populární	k2eAgInSc1d1
mezi	mezi	k7c7
nejchudšími	chudý	k2eAgFnPc7d3
a	a	k8xC
nejvíc	hodně	k6eAd3,k6eAd1
přehlíženými	přehlížený	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
v	v	k7c6
gay	gay	k1gMnSc1
komunitě	komunita	k1gFnSc3
<g/>
:	:	kIx,
drag	drag	k1gInSc1
queens	queensa	k1gFnPc2
<g/>
,	,	kIx,
transgender	transgendra	k1gFnPc2
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
zženštilými	zženštilý	k2eAgMnPc7d1
mladými	mladý	k2eAgMnPc7d1
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
lesbickými	lesbický	k2eAgFnPc7d1
mužatkami	mužatka	k1gFnPc7
<g/>
,	,	kIx,
mužskými	mužský	k2eAgInPc7d1
prostituty	prostitut	k1gMnPc7
a	a	k8xC
mladými	mladý	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
bez	bez	k7c2
domova	domov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policejní	policejní	k2eAgFnSc1d1
razie	razie	k1gFnSc1
byly	být	k5eAaImAgInP
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
v	v	k7c6
gay	gay	k1gMnSc1
barech	bar	k1gInPc6
běžnou	běžný	k2eAgFnSc4d1
praxí	praxe	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c4
Stonewall	Stonewall	k1gInSc4
Inn	Inn	k1gFnPc2
policisté	policista	k1gMnPc1
rychle	rychle	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
situací	situace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svojí	svojit	k5eAaImIp3nS
neobratností	neobratnost	k1gFnSc7
přilákali	přilákat	k5eAaPmAgMnP
dav	dav	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stal	stát	k5eAaPmAgMnS
nepřátelským	přátelský	k2eNgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
newyorskou	newyorský	k2eAgFnSc7d1
policií	policie	k1gFnSc7
a	a	k8xC
gay	gay	k1gMnSc1
obyvateli	obyvatel	k1gMnSc3
Greenwich	Greenwich	k1gInSc4
Village	Village	k1gNnSc1
vypuklo	vypuknout	k5eAaPmAgNnS
do	do	k7c2
dalších	další	k2eAgInPc2d1
protestů	protest	k1gInPc2
následující	následující	k2eAgInSc4d1
večer	večer	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
znova	znova	k6eAd1
o	o	k7c4
několik	několik	k4yIc4
nocí	noc	k1gFnPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
týdnů	týden	k1gInPc2
se	se	k3xPyFc4
Village	Village	k1gFnSc7
obyvatelé	obyvatel	k1gMnPc1
rychle	rychle	k6eAd1
zorganizovali	zorganizovat	k5eAaPmAgMnP
do	do	k7c2
aktivistických	aktivistický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
zaměřit	zaměřit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
úsilí	úsilí	k1gNnSc4
na	na	k7c4
zřízení	zřízení	k1gNnSc4
míst	místo	k1gNnPc2
pro	pro	k7c4
gaye	gay	k1gMnPc4
a	a	k8xC
lesby	lesba	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
ohledem	ohled	k1gInSc7
své	svůj	k3xOyFgFnSc2
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
otevřeni	otevřít	k5eAaPmNgMnP
bez	bez	k7c2
strachu	strach	k1gInSc2
ze	z	k7c2
zatčení	zatčení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Stonewallských	Stonewallský	k2eAgInPc6d1
nepokojích	nepokoj	k1gInPc6
gayové	gay	k1gMnPc1
a	a	k8xC
lesby	lesba	k1gFnPc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
čelili	čelit	k5eAaImAgMnP
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
soudržné	soudržný	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
rodovým	rodový	k2eAgFnPc3d1
<g/>
,	,	kIx,
rasovým	rasový	k2eAgFnPc3d1
<g/>
,	,	kIx,
třídním	třídní	k2eAgFnPc3d1
a	a	k8xC
generačním	generační	k2eAgFnPc3d1
překážkám	překážka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
šesti	šest	k4xCc2
měsíců	měsíc	k1gInPc2
byly	být	k5eAaImAgFnP
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
založeny	založen	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
gay	gay	k1gMnSc1
organizace	organizace	k1gFnSc2
aktivistů	aktivista	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
zaměřovaly	zaměřovat	k5eAaImAgFnP
na	na	k7c4
konfrontační	konfrontační	k2eAgFnPc4d1
taktiky	taktika	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
troje	troje	k4xRgFnPc4
noviny	novina	k1gFnPc4
na	na	k7c6
prosazování	prosazování	k1gNnSc6
práv	právo	k1gNnPc2
gayů	gay	k1gMnPc2
a	a	k8xC
leseb	lesba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
několika	několik	k4yIc2
let	léto	k1gNnPc2
byly	být	k5eAaImAgFnP
napříč	napříč	k6eAd1
celými	celá	k1gFnPc7
USA	USA	kA
a	a	k8xC
světem	svět	k1gInSc7
založeny	založen	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
za	za	k7c4
práva	právo	k1gNnPc4
gayů	gay	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomínajíc	připomínat	k5eAaImSgFnS
výročí	výročí	k1gNnSc3
nepokojů	nepokoj	k1gInPc2
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1970	#num#	k4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
,	,	kIx,
San	San	k1gMnSc1
Franciscu	Franciscus	k1gInSc2
a	a	k8xC
Chicagu	Chicago	k1gNnSc6
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
proběhly	proběhnout	k5eAaPmAgInP
první	první	k4xOgMnSc1
gay	gay	k1gMnSc1
pride	pride	k6eAd1
pochody	pochod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgInPc1d1
pochody	pochod	k1gInPc1
byly	být	k5eAaImAgInP
organizované	organizovaný	k2eAgInPc1d1
i	i	k8xC
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
Gay	gay	k1gMnSc1
Pride	Prid	k1gInSc5
události	událost	k1gFnPc1
konají	konat	k5eAaImIp3nP
na	na	k7c4
znak	znak	k1gInSc4
Stonewallských	Stonewallský	k2eAgInPc2d1
nepokojů	nepokoj	k1gInPc2
ročně	ročně	k6eAd1
koncem	koncem	k7c2
června	červen	k1gInSc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Stonewallský	Stonewallský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
monument	monument	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
místě	místo	k1gNnSc6
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
USA	USA	kA
v	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Po	po	k7c6
společenských	společenský	k2eAgInPc6d1
převratech	převrat	k1gInPc6
druhé	druhý	k4xOgFnPc4
světové	světový	k2eAgFnPc4d1
války	válka	k1gFnPc4
mnozí	mnohý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
podle	podle	k7c2
historika	historik	k1gMnSc2
Barryho	Barry	k1gMnSc2
Adama	Adam	k1gMnSc2
vášnivě	vášnivě	k6eAd1
toužili	toužit	k5eAaImAgMnP
po	po	k7c6
„	„	k?
<g/>
navrácení	navrácení	k1gNnSc6
předválečného	předválečný	k2eAgInSc2d1
společenského	společenský	k2eAgInSc2d1
řádu	řád	k1gInSc2
a	a	k8xC
odolání	odolání	k1gNnSc2
silám	síla	k1gFnPc3
změn	změna	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Pobízen	pobízet	k5eAaImNgInS
národním	národní	k2eAgInSc7d1
důrazem	důraz	k1gInSc7
na	na	k7c4
antikomunismus	antikomunismus	k1gInSc4
prováděl	provádět	k5eAaImAgMnS
senátor	senátor	k1gMnSc1
Joseph	Joseph	k1gMnSc1
McCarthy	McCartha	k1gMnSc2
slyšení	slyšení	k1gNnSc2
hledaje	hledat	k5eAaImSgInS
komunisty	komunista	k1gMnPc4
ve	v	k7c6
vládě	vláda	k1gFnSc6
<g/>
,	,	kIx,
armádě	armáda	k1gFnSc6
a	a	k8xC
jiných	jiný	k2eAgInPc6d1
vládou	vláda	k1gFnSc7
financovaných	financovaný	k2eAgInPc6d1
úřadech	úřad	k1gInPc6
a	a	k8xC
institucích	instituce	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
celonárodní	celonárodní	k2eAgFnSc3d1
paranoii	paranoia	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anarchisté	anarchista	k1gMnPc1
<g/>
,	,	kIx,
komunisté	komunista	k1gMnPc1
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
označení	označení	k1gNnSc4
za	za	k7c4
neamerické	americký	k2eNgInPc4d1
a	a	k8xC
podvratné	podvratný	k2eAgInPc4d1
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
bezpečnostní	bezpečnostní	k2eAgNnPc4d1
riziko	riziko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homosexuálové	homosexuál	k1gMnPc1
byli	být	k5eAaImAgMnP
do	do	k7c2
tohoto	tento	k3xDgInSc2
seznamu	seznam	k1gInSc2
zahrnuti	zahrnout	k5eAaPmNgMnP
Ministerstvem	ministerstvo	k1gNnSc7
zahraničí	zahraničí	k1gNnSc2
USA	USA	kA
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
náchylní	náchylný	k2eAgMnPc1d1
k	k	k7c3
vydíraní	vydíraný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřování	vyšetřování	k1gNnSc1
senátu	senát	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
předsedal	předsedat	k5eAaImAgInS
Clyde	Clyd	k1gInSc5
R.	R.	kA
Hoey	Hoeum	k1gNnPc7
mělo	mít	k5eAaImAgNnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
zprávě	zpráva	k1gFnSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
Všeobecně	všeobecně	k6eAd1
se	se	k3xPyFc4
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
otevřeně	otevřeně	k6eAd1
provozují	provozovat	k5eAaImIp3nP
zvrhlé	zvrhlý	k2eAgInPc1d1
akty	akt	k1gInPc1
nemají	mít	k5eNaImIp3nP
emocionální	emocionální	k2eAgFnSc4d1
stabilitu	stabilita	k1gFnSc4
normálních	normální	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
<g/>
“	“	k?
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
prohlásilo	prohlásit	k5eAaPmAgNnS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgFnPc1
vládní	vládní	k2eAgFnPc1d1
zpravodajské	zpravodajský	k2eAgFnPc1d1
služby	služba	k1gFnPc1
„	„	k?
<g/>
jednomyslně	jednomyslně	k6eAd1
souhlasí	souhlasit	k5eAaImIp3nS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
sexuální	sexuální	k2eAgMnPc1d1
zvrhlíci	zvrhlík	k1gMnPc1
ve	v	k7c6
vládě	vláda	k1gFnSc6
představují	představovat	k5eAaImIp3nP
bezpečnostní	bezpečnostní	k2eAgNnSc4d1
riziko	riziko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1947	#num#	k4
a	a	k8xC
1950	#num#	k4
bylo	být	k5eAaImAgNnS
zamítnuto	zamítnout	k5eAaPmNgNnS
1	#num#	k4
700	#num#	k4
federálních	federální	k2eAgFnPc2d1
žádostí	žádost	k1gFnPc2
o	o	k7c6
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
4	#num#	k4
380	#num#	k4
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
propuštěno	propustit	k5eAaPmNgNnS
z	z	k7c2
armády	armáda	k1gFnSc2
a	a	k8xC
420	#num#	k4
bylo	být	k5eAaImAgNnS
vyhozeno	vyhozen	k2eAgNnSc1d1
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
vládních	vládní	k2eAgFnPc2d1
prací	práce	k1gFnPc2
kvůli	kvůli	k7c3
podezření	podezření	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
homosexuály	homosexuál	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
celé	celý	k2eAgNnSc4d1
období	období	k1gNnSc4
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
si	se	k3xPyFc3
federální	federální	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
vyšetřování	vyšetřování	k1gNnSc4
(	(	kIx(
<g/>
FBI	FBI	kA
<g/>
)	)	kIx)
a	a	k8xC
policejní	policejní	k2eAgMnPc1d1
oddělení	oddělený	k2eAgMnPc1d1
vedli	vést	k5eAaImAgMnP
seznamy	seznam	k1gInPc4
známých	známý	k2eAgMnPc2d1
homosexuálů	homosexuál	k1gMnPc2
<g/>
,	,	kIx,
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
upřednostňovali	upřednostňovat	k5eAaImAgMnP
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
přátel	přítel	k1gMnPc2
<g/>
;	;	kIx,
poštovní	poštovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
sledoval	sledovat	k5eAaImAgInS
<g/>
,	,	kIx,
kam	kam	k6eAd1
jsou	být	k5eAaImIp3nP
posílány	posílán	k2eAgInPc4d1
materiály	materiál	k1gInPc4
týkající	týkající	k2eAgInPc4d1
se	se	k3xPyFc4
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Státní	státní	k2eAgFnSc2d1
a	a	k8xC
místní	místní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
tento	tento	k3xDgInSc4
příklad	příklad	k1gInSc4
následovaly	následovat	k5eAaImAgInP
<g/>
:	:	kIx,
bary	bar	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
sloužily	sloužit	k5eAaImAgInP
homosexuálům	homosexuál	k1gMnPc3
byly	být	k5eAaImAgInP
zavřené	zavřený	k2eAgInPc4d1
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
zákazníci	zákazník	k1gMnPc1
byli	být	k5eAaImAgMnP
zatčení	zatčený	k2eAgMnPc1d1
a	a	k8xC
vystaveni	vystavit	k5eAaPmNgMnP
v	v	k7c6
novinách	novina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Města	město	k1gNnSc2
prováděla	provádět	k5eAaImAgFnS
„	„	k?
<g/>
kontrolní	kontrolní	k2eAgFnPc4d1
prohlídky	prohlídka	k1gFnPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zbavila	zbavit	k5eAaPmAgFnS
čtvrti	čtvrt	k1gFnPc4
<g/>
,	,	kIx,
parky	park	k1gInPc4
<g/>
,	,	kIx,
bary	bar	k1gInPc4
a	a	k8xC
pláže	pláž	k1gFnPc4
gay	gay	k1gMnSc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nošení	nošení	k1gNnPc1
oblečení	oblečení	k1gNnSc2
opačného	opačný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
bylo	být	k5eAaImAgNnS
prohlášeno	prohlásit	k5eAaPmNgNnS
za	za	k7c4
nezákonné	zákonný	k2eNgFnPc4d1
a	a	k8xC
univerzity	univerzita	k1gFnPc1
vyhazovaly	vyhazovat	k5eAaImAgFnP
učitele	učitel	k1gMnPc4
podezřelé	podezřelý	k2eAgMnPc4d1
z	z	k7c2
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tisíce	tisíc	k4xCgInPc1
gay	gay	k1gMnSc1
mužů	muž	k1gMnPc2
a	a	k8xC
žen	žena	k1gFnPc2
byly	být	k5eAaImAgFnP
veřejně	veřejně	k6eAd1
ponižovány	ponižován	k2eAgFnPc1d1
<g/>
,	,	kIx,
fyzicky	fyzicky	k6eAd1
obtěžovány	obtěžován	k2eAgInPc1d1
<g/>
,	,	kIx,
vyhazováni	vyhazován	k2eAgMnPc1d1
z	z	k7c2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
zatýkány	zatýkán	k2eAgInPc4d1
nebo	nebo	k8xC
hospitalizovány	hospitalizován	k2eAgInPc4d1
v	v	k7c6
psychiatrických	psychiatrický	k2eAgFnPc6d1
léčebnách	léčebna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
žili	žít	k5eAaImAgMnP
dvojitý	dvojitý	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
osobní	osobní	k2eAgInSc4d1
život	život	k1gInSc4
drželi	držet	k5eAaImAgMnP
v	v	k7c6
práci	práce	k1gFnSc6
v	v	k7c6
tajnosti	tajnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
psychiatrická	psychiatrický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
homosexualitu	homosexualita	k1gFnSc4
v	v	k7c6
Diagnostickém	diagnostický	k2eAgInSc6d1
a	a	k8xC
statistickém	statistický	k2eAgInSc6d1
manuálu	manuál	k1gInSc6
mentálních	mentální	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
DSM	DSM	kA
<g/>
,	,	kIx,
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
názvu	název	k1gInSc2
Diagnostic	Diagnostice	k1gFnPc2
and	and	k?
Statistic	Statistice	k1gFnPc2
Manual	Manual	k1gMnSc1
of	of	k?
Mental	Mental	k1gMnSc1
Disorders	Disorders	k1gInSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
duševní	duševní	k2eAgFnSc4d1
poruchu	porucha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
studie	studie	k1gFnSc1
homosexuality	homosexualita	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
k	k	k7c3
odůvodnění	odůvodnění	k1gNnSc3
zařazení	zařazení	k1gNnSc2
poruchy	porucha	k1gFnSc2
jako	jako	k8xC,k8xS
údajný	údajný	k2eAgInSc1d1
patologický	patologický	k2eAgInSc1d1
skrytý	skrytý	k2eAgInSc1d1
strach	strach	k1gInSc1
z	z	k7c2
druhého	druhý	k4xOgNnSc2
pohlaví	pohlaví	k1gNnSc2
způsobený	způsobený	k2eAgInSc1d1
traumatickými	traumatický	k2eAgInPc7d1
vztahy	vztah	k1gInPc7
mezi	mezi	k7c7
rodiči	rodič	k1gMnPc7
a	a	k8xC
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tohle	tenhle	k3xDgNnSc1
vnímání	vnímání	k1gNnSc1
homosexuality	homosexualita	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
medicíně	medicína	k1gFnSc6
velmi	velmi	k6eAd1
vlivné	vlivný	k2eAgNnSc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
však	však	k9
psycholožka	psycholožka	k1gFnSc1
Evelyn	Evelyna	k1gFnPc2
Hooker	Hookra	k1gFnPc2
uskutečnila	uskutečnit	k5eAaPmAgFnS
studii	studie	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
porovnávala	porovnávat	k5eAaImAgFnS
štěstí	štěstí	k1gNnSc4
a	a	k8xC
povahovou	povahový	k2eAgFnSc4d1
vyrovnanost	vyrovnanost	k1gFnSc4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
sami	sám	k3xTgMnPc1
sebe	sebe	k3xPyFc4
označili	označit	k5eAaPmAgMnP
za	za	k7c4
homosexuály	homosexuál	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
heterosexuálních	heterosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
a	a	k8xC
nenašla	najít	k5eNaPmAgFnS
žádný	žádný	k3yNgInSc4
rozdíl	rozdíl	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
studie	studie	k1gFnSc1
ohromila	ohromit	k5eAaPmAgFnS
lékařskou	lékařský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
a	a	k8xC
udělala	udělat	k5eAaPmAgFnS
z	z	k7c2
ní	on	k3xPp3gFnSc2
hrdinku	hrdinka	k1gFnSc4
v	v	k7c6
očích	oko	k1gNnPc6
mnoha	mnoho	k4c2
gayů	gay	k1gMnPc2
a	a	k8xC
leseb	lesba	k1gFnPc2
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
homosexualita	homosexualita	k1gFnSc1
i	i	k9
nadále	nadále	k6eAd1
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
DSM	DSM	kA
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Homofilní	Homofilní	k2eAgInSc1d1
aktivismus	aktivismus	k1gInSc1
</s>
<s>
Jako	jako	k9
odpověď	odpověď	k1gFnSc4
na	na	k7c4
tento	tento	k3xDgInSc4
trend	trend	k1gInSc4
vznikly	vzniknout	k5eAaPmAgFnP
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	sebe	k3xPyFc6
dvě	dva	k4xCgFnPc4
organizace	organizace	k1gFnPc1
pro	pro	k7c4
podporu	podpora	k1gFnSc4
záležitostí	záležitost	k1gFnPc2
homosexuálů	homosexuál	k1gMnPc2
a	a	k8xC
k	k	k7c3
poskytnutí	poskytnutí	k1gNnSc3
společenských	společenský	k2eAgFnPc2d1
příležitostí	příležitost	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
gayové	gay	k1gMnPc1
a	a	k8xC
lesby	lesba	k1gFnPc4
mohli	moct	k5eAaImAgMnP
vést	vést	k5eAaImF
společenský	společenský	k2eAgInSc4d1
život	život	k1gInSc4
bez	bez	k7c2
strachu	strach	k1gInSc2
ze	z	k7c2
zatčení	zatčení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homosexuálové	homosexuál	k1gMnPc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
Los	los	k1gInSc1
Angeles	Angeles	k1gMnSc1
vytvořili	vytvořit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
v	v	k7c6
domě	dům	k1gInSc6
komunistického	komunistický	k2eAgMnSc2d1
aktivisty	aktivista	k1gMnSc2
Harryho	Harry	k1gMnSc2
Haye	Hay	k1gMnSc2
spolek	spolek	k1gInSc1
Mattachine	Mattachin	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
homosexuály	homosexuál	k1gMnPc4
sjednotit	sjednotit	k5eAaPmF
<g/>
,	,	kIx,
vzdělat	vzdělat	k5eAaPmF
<g/>
,	,	kIx,
poskytnout	poskytnout	k5eAaPmF
jim	on	k3xPp3gFnPc3
vedení	vedení	k1gNnSc4
a	a	k8xC
pomoct	pomoct	k5eAaPmF
„	„	k?
<g/>
sexuálním	sexuální	k2eAgMnPc3d1
deviantům	deviant	k1gMnPc3
<g/>
“	“	k?
s	s	k7c7
právními	právní	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
jejich	jejich	k3xOp3gInSc1
radikální	radikální	k2eAgInSc1d1
přístup	přístup	k1gInSc1
vyvolával	vyvolávat	k5eAaImAgInS
obrovský	obrovský	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
Mattachine	Mattachin	k1gInSc5
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
změnil	změnit	k5eAaPmAgMnS
svoje	svůj	k3xOyFgNnSc4
zaměření	zaměření	k1gNnSc4
na	na	k7c6
přizpůsobení	přizpůsobení	k1gNnSc6
se	se	k3xPyFc4
a	a	k8xC
řádnost	řádnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odůvodnili	odůvodnit	k5eAaPmAgMnP
to	ten	k3xDgNnSc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
víc	hodně	k6eAd2
lidí	člověk	k1gMnPc2
změní	změnit	k5eAaPmIp3nS
názor	názor	k1gInSc4
na	na	k7c4
homosexualitu	homosexualita	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
dokážou	dokázat	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
gayové	gay	k1gMnPc1
a	a	k8xC
lesby	lesba	k1gFnPc1
jsou	být	k5eAaImIp3nP
normální	normální	k2eAgMnSc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
stejní	stejný	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
heterosexuálové	heterosexuál	k1gMnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
San	San	k1gFnSc6
Franciscu	Francisca	k1gFnSc4
setkalo	setkat	k5eAaPmAgNnS
<g />
.	.	kIx.
</s>
<s hack="1">
několik	několik	k4yIc1
žen	žena	k1gFnPc2
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
obývacích	obývací	k2eAgInPc6d1
pokojích	pokoj	k1gInPc6
a	a	k8xC
vytvořili	vytvořit	k5eAaPmAgMnP
Dcery	dcera	k1gFnPc4
Bilitis	Bilitis	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Daughters	Daughters	k1gInSc1
of	of	k?
Bilitis	Bilitis	k1gFnSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
DOB	doba	k1gFnPc2
<g/>
)	)	kIx)
pro	pro	k7c4
lesby	lesba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
těch	ten	k3xDgFnPc2
osm	osm	k4xCc1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
vytvořilo	vytvořit	k5eAaPmAgNnS
DOB	doba	k1gFnPc2
se	se	k3xPyFc4
původně	původně	k6eAd1
setkalo	setkat	k5eAaPmAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
měli	mít	k5eAaImAgMnP
bezpečné	bezpečný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
k	k	k7c3
tanci	tanec	k1gInSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
DOB	doba	k1gFnPc2
rostlo	růst	k5eAaImAgNnS
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc2
cíle	cíl	k1gInSc2
se	se	k3xPyFc4
stávaly	stávat	k5eAaImAgFnP
podobnými	podobný	k2eAgFnPc7d1
těm	ten	k3xDgNnPc3
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
měl	mít	k5eAaImAgInS
spolek	spolek	k1gInSc1
Mattachine	Mattachin	k1gMnSc5
<g/>
,	,	kIx,
a	a	k8xC
pobízely	pobízet	k5eAaImAgInP
své	svůj	k3xOyFgFnPc4
členky	členka	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
začlenily	začlenit	k5eAaPmAgFnP
do	do	k7c2
celkové	celkový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
výzev	výzva	k1gFnPc2
pro	pro	k7c4
vládní	vládní	k2eAgNnSc4d1
utlačování	utlačování	k1gNnSc4
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
s	s	k7c7
názvem	název	k1gInSc7
ONE	ONE	kA
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
vydala	vydat	k5eAaPmAgFnS
časopis	časopis	k1gInSc4
ONE	ONE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poštovní	poštovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
USA	USA	kA
odmítl	odmítnout	k5eAaPmAgInS
rozeslat	rozeslat	k5eAaPmF
srpnové	srpnový	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
zabývalo	zabývat	k5eAaImAgNnS
homosexuály	homosexuál	k1gMnPc7
v	v	k7c6
heterosexuálních	heterosexuální	k2eAgNnPc6d1
manželstvích	manželství	k1gNnPc6
s	s	k7c7
odůvodněním	odůvodnění	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
byl	být	k5eAaImAgInS
nemravný	mravný	k2eNgInSc1d1
i	i	k9
přestože	přestože	k8xS
byl	být	k5eAaImAgInS
časopis	časopis	k1gInSc1
zakryt	zakrýt	k5eAaPmNgInS
hnědým	hnědý	k2eAgInSc7d1
papírovým	papírový	k2eAgInSc7d1
obalem	obal	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případ	případ	k1gInSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
dostal	dostat	k5eAaPmAgMnS
až	až	k9
na	na	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
soud	soud	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
ONE	ONE	kA
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
své	svůj	k3xOyFgInPc4
materiály	materiál	k1gInPc4
přes	přes	k7c4
poštovní	poštovní	k2eAgInSc4d1
úřad	úřad	k1gInSc4
posílat	posílat	k5eAaImF
může	moct	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
homofilních	homofilní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
—	—	k?
jak	jak	k8xC,k8xS
byly	být	k5eAaImAgFnP
homosexuální	homosexuální	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
nazývány	nazývat	k5eAaImNgFnP
<g/>
—	—	k?
<g/>
rostl	růst	k5eAaImAgInS
a	a	k8xC
rozšiřovaly	rozšiřovat	k5eAaImAgFnP
se	se	k3xPyFc4
i	i	k9
na	na	k7c4
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
členové	člen	k1gMnPc1
těchto	tento	k3xDgFnPc2
organizací	organizace	k1gFnPc2
stávaly	stávat	k5eAaImAgFnP
odvážnějšími	odvážný	k2eAgFnPc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frank	Frank	k1gMnSc1
Kameny	kámen	k1gInPc4
založil	založit	k5eAaPmAgMnS
Mattachine	Mattachin	k1gInSc5
Washingtonu	Washington	k1gInSc6
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc6
Kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
homosexualitě	homosexualita	k1gFnSc3
byl	být	k5eAaImAgInS
vyhozen	vyhodit	k5eAaPmNgInS
z	z	k7c2
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
Map	mapa	k1gFnPc2
Service	Service	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
neúspěšně	úspěšně	k6eNd1
se	se	k3xPyFc4
soudil	soudit	k5eAaImAgMnS
o	o	k7c6
vrácení	vrácení	k1gNnSc6
do	do	k7c2
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kameny	kámen	k1gInPc7
psal	psát	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
homosexuálové	homosexuál	k1gMnPc1
se	se	k3xPyFc4
od	od	k7c2
heterosexuálů	heterosexuál	k1gMnPc2
nijak	nijak	k6eAd1
neliší	lišit	k5eNaImIp3nP
a	a	k8xC
často	často	k6eAd1
své	svůj	k3xOyFgFnSc2
snahy	snaha	k1gFnSc2
orientoval	orientovat	k5eAaBmAgInS
směrem	směr	k1gInSc7
k	k	k7c3
odborníkům	odborník	k1gMnPc3
na	na	k7c4
duševní	duševní	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
,	,	kIx,
z	z	k7c2
kterých	který	k3yRgMnPc2,k3yIgMnPc2,k3yQgMnPc2
někteří	některý	k3yIgMnPc1
se	se	k3xPyFc4
účastnili	účastnit	k5eAaImAgMnP
schůzek	schůzka	k1gFnPc2
Mattachine	Mattachin	k1gInSc5
a	a	k8xC
DOB	doba	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
členům	člen	k1gMnPc3
tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
abnormální	abnormální	k2eAgMnSc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Kameny	kámen	k1gInPc1
<g/>
,	,	kIx,
inspirován	inspirován	k2eAgInSc1d1
Afroamerickým	afroamerický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
za	za	k7c4
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
zorganizoval	zorganizovat	k5eAaPmAgMnS
protesty	protest	k1gInPc4
proti	proti	k7c3
diskriminaci	diskriminace	k1gFnSc3
v	v	k7c6
zaměstnání	zaměstnání	k1gNnSc6
před	před	k7c7
Bílým	bílý	k2eAgInSc7d1
domem	dům	k1gInSc7
a	a	k8xC
jinými	jiný	k2eAgFnPc7d1
parlamentními	parlamentní	k2eAgFnPc7d1
budovami	budova	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
protesty	protest	k1gInPc4
mnoho	mnoho	k4c4
gayů	gay	k1gMnPc2
šokovaly	šokovat	k5eAaBmAgFnP
a	a	k8xC
znepokojily	znepokojit	k5eAaPmAgFnP
část	část	k1gFnSc4
vedení	vedení	k1gNnSc2
Mattachine	Mattachin	k1gInSc5
a	a	k8xC
DOB	doba	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
té	ten	k3xDgFnSc6
samé	samý	k3xTgFnSc6
době	doba	k1gFnSc6
rostla	růst	k5eAaImAgFnS
důležitost	důležitost	k1gFnSc1
<g/>
,	,	kIx,
frekvence	frekvence	k1gFnSc1
a	a	k8xC
kritičnost	kritičnost	k1gFnSc1
demonstrací	demonstrace	k1gFnPc2
v	v	k7c6
Afroamerickém	afroamerický	k2eAgNnSc6d1
hnutí	hnutí	k1gNnSc6
za	za	k7c4
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
opozice	opozice	k1gFnSc1
Vietnamské	vietnamský	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
jejich	jejich	k3xOp3gFnSc1
konfrontace	konfrontace	k1gFnSc1
s	s	k7c7
policií	policie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nepokoje	nepokoj	k1gInPc1
v	v	k7c4
Compton	Compton	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cafeteria	Cafeterium	k1gNnSc2
</s>
<s>
Na	na	k7c6
okraji	okraj	k1gInSc6
několika	několik	k4yIc2
malých	malý	k1gMnPc2
gay	gay	k1gMnSc1
komunit	komunita	k1gFnPc2
byli	být	k5eAaImAgMnP
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
výzvou	výzva	k1gFnSc7
pro	pro	k7c4
genderová	genderový	k2eAgNnPc4d1
očekávaní	očekávaný	k2eAgMnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
zženštilí	zženštilý	k2eAgMnPc1d1
muži	muž	k1gMnPc1
a	a	k8xC
mužné	mužný	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
nebo	nebo	k8xC
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
byl	být	k5eAaImAgMnS
při	při	k7c6
narození	narození	k1gNnSc6
přiřazen	přiřazen	k2eAgInSc1d1
mužský	mužský	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
oblékali	oblékat	k5eAaImAgMnP
a	a	k8xC
žili	žít	k5eAaImAgMnP
jako	jako	k8xC,k8xS
ženy	žena	k1gFnPc1
a	a	k8xC
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
byl	být	k5eAaImAgMnS
při	při	k7c6
narození	narození	k1gNnSc6
přiřazen	přiřazen	k2eAgInSc1d1
ženský	ženský	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
oblékali	oblékat	k5eAaImAgMnP
a	a	k8xC
žili	žít	k5eAaImAgMnP
jako	jako	k9
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
buď	buď	k8xC
jen	jen	k9
někdy	někdy	k6eAd1
nebo	nebo	k8xC
pořád	pořád	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudobá	soudobý	k2eAgFnSc1d1
nomenklatura	nomenklatura	k1gFnSc1
je	on	k3xPp3gNnSc4
klasifikovala	klasifikovat	k5eAaImAgFnS
jako	jako	k9
transvestity	transvestit	k1gMnPc4
a	a	k8xC
právě	právě	k9
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
byli	být	k5eAaImAgMnP
nejviditelnějšími	viditelný	k2eAgMnPc7d3
reprezentanty	reprezentant	k1gMnPc7
sexuálních	sexuální	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
pečlivě	pečlivě	k6eAd1
vytvořeným	vytvořený	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
prezentovali	prezentovat	k5eAaBmAgMnP
Mattachine	Mattachin	k1gInSc5
a	a	k8xC
DOB	doba	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
homosexuálové	homosexuál	k1gMnPc1
jsou	být	k5eAaImIp3nP
řádní	řádný	k2eAgMnPc1d1
a	a	k8xC
normální	normální	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Mattachine	Mattachin	k1gInSc5
a	a	k8xC
DOB	doba	k1gFnPc2
považovali	považovat	k5eAaImAgMnP
strasti	strast	k1gFnSc2
zatčení	zatčení	k1gNnSc2
kvůli	kvůli	k7c3
nošení	nošení	k1gNnSc3
oblečení	oblečení	k1gNnSc2
opačného	opačný	k2eAgInSc2d1
rodu	rod	k1gInSc2
za	za	k7c4
paralelní	paralelní	k2eAgInSc4d1
s	s	k7c7
potížemi	potíž	k1gFnPc7
homofilních	homofilní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
–	–	k?
podobné	podobný	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
jasně	jasně	k6eAd1
oddělené	oddělený	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gay	gay	k1gMnSc1
a	a	k8xC
transgender	transgendra	k1gFnPc2
lidé	člověk	k1gMnPc1
zorganizovali	zorganizovat	k5eAaPmAgMnP
v	v	k7c4
Cooper	Cooper	k1gInSc4
Donuts	Donuts	k1gInSc4
cafe	cafe	k1gNnPc2
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
malý	malý	k2eAgInSc4d1
protest	protest	k1gInSc4
jako	jako	k8xC,k8xS
odpověď	odpověď	k1gFnSc4
na	na	k7c4
policejní	policejní	k2eAgNnPc4d1
pronásledování	pronásledování	k1gNnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
události	událost	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
v	v	k7c6
San	San	k1gFnSc6
Franciscu	Francisca	k1gFnSc4
<g/>
,	,	kIx,
drag	drag	k1gInSc4
queens	queensa	k1gFnPc2
<g/>
,	,	kIx,
mužští	mužský	k2eAgMnPc1d1
prostituti	prostitut	k1gMnPc1
a	a	k8xC
transvestité	transvestit	k1gMnPc1
seděli	sedět	k5eAaImAgMnP
v	v	k7c4
Compton	Compton	k1gInSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
Cafeteria	Cafeterium	k1gNnPc4
<g/>
,	,	kIx,
když	když	k8xS
přišla	přijít	k5eAaPmAgFnS
policie	policie	k1gFnSc1
zatknout	zatknout	k5eAaPmF
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
byl	být	k5eAaImAgMnS
při	při	k7c6
narození	narození	k1gNnSc6
přiřazen	přiřazen	k2eAgInSc1d1
mužský	mužský	k2eAgInSc1d1
rod	rod	k1gInSc1
oblečené	oblečený	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
ženy	žena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgInP
nepokoje	nepokoj	k1gInPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
zákazníci	zákazník	k1gMnPc1
bufetu	bufet	k1gInSc2
vrhali	vrhat	k5eAaImAgMnP
šálky	šálka	k1gFnPc4
<g/>
,	,	kIx,
talíře	talíř	k1gInPc4
a	a	k8xC
podšálky	podšálek	k1gInPc4
<g/>
,	,	kIx,
rozbili	rozbít	k5eAaPmAgMnP
okna	okno	k1gNnSc2
restaurace	restaurace	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
vrátili	vrátit	k5eAaPmAgMnP
se	se	k3xPyFc4
o	o	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgNnP
okna	okno	k1gNnPc1
opravena	opravit	k5eAaPmNgNnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
je	on	k3xPp3gNnPc4
rozbili	rozbít	k5eAaPmAgMnP
znova	znova	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Profesorka	profesorka	k1gFnSc1
Susan	Susan	k1gMnSc1
Stryker	Stryker	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
klasifikovala	klasifikovat	k5eAaImAgFnS
nepokoje	nepokoj	k1gInPc4
v	v	k7c4
Comton	Comton	k1gInSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
Cafeteria	Cafeterium	k1gNnPc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
akt	akt	k1gInSc4
proti	proti	k7c3
transgenderové	transgenderový	k2eAgFnSc3d1
diskriminaci	diskriminace	k1gFnSc3
<g/>
,	,	kIx,
spíš	spíš	k9
než	než	k8xS
akt	akt	k1gInSc4
diskriminace	diskriminace	k1gFnSc2
vůči	vůči	k7c3
sexuální	sexuální	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
<g/>
“	“	k?
a	a	k8xC
připojila	připojit	k5eAaPmAgFnS
povstání	povstání	k1gNnSc4
k	k	k7c3
otázkám	otázka	k1gFnPc3
genderu	gender	k1gInSc2
<g/>
,	,	kIx,
rasy	rasa	k1gFnSc2
a	a	k8xC
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
homofilními	homofilní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
zlehčované	zlehčovaný	k2eAgFnSc2d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Značí	značit	k5eAaImIp3nS
to	ten	k3xDgNnSc1
začátek	začátek	k1gInSc1
transgenderového	transgenderový	k2eAgInSc2d1
aktivismu	aktivismus	k1gInSc2
v	v	k7c6
San	San	k1gFnSc6
Fransciscu	Fransciscus	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Greenwich	Greenwich	k1gInSc1
Village	Villag	k1gFnSc2
</s>
<s>
Washington	Washington	k1gInSc1
Square	square	k1gInSc1
Park	park	k1gInSc1
v	v	k7c4
Greenwich	Greenwich	k1gInSc4
Village	Villag	k1gFnSc2
</s>
<s>
Manhattanská	manhattanský	k2eAgNnPc4d1
sousedství	sousedství	k1gNnPc4
Greenwich	Greenwich	k1gInSc1
Village	Villag	k1gInSc2
a	a	k8xC
Harlem	Harlo	k1gNnSc7
byla	být	k5eAaImAgFnS
domovem	domov	k1gInSc7
pro	pro	k7c4
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
homosexuální	homosexuální	k2eAgFnSc2d1
populace	populace	k1gFnSc2
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
muži	muž	k1gMnPc1
a	a	k8xC
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
sloužili	sloužit	k5eAaImAgMnP
ve	v	k7c6
válce	válka	k1gFnSc6
využili	využít	k5eAaPmAgMnP
příležitost	příležitost	k1gFnSc4
usadit	usadit	k5eAaPmF
se	se	k3xPyFc4
ve	v	k7c6
větších	veliký	k2eAgNnPc6d2
městech	město	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Prohibice	prohibice	k1gFnSc1
neúmyslně	úmyslně	k6eNd1
prospěla	prospět	k5eAaPmAgFnS
gay	gay	k1gMnSc1
podnikům	podnik	k1gInPc3
<g/>
,	,	kIx,
protože	protože	k8xS
po	po	k7c6
alkoholu	alkohol	k1gInSc6
byla	být	k5eAaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
a	a	k8xC
byl	být	k5eAaImAgMnS
odsunut	odsunout	k5eAaPmNgMnS
do	do	k7c2
ilegality	ilegalita	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
činnostmi	činnost	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
nemorální	morální	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc2
York	York	k1gInSc4
City	city	k1gNnSc2
zavedlo	zavést	k5eAaPmAgNnS
zákony	zákon	k1gInPc4
proti	proti	k7c3
homosexualitě	homosexualita	k1gFnSc3
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
a	a	k8xC
v	v	k7c6
soukromých	soukromý	k2eAgInPc6d1
podnicích	podnik	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
protože	protože	k8xS
alkohol	alkohol	k1gInSc1
byl	být	k5eAaImAgInS
vysoce	vysoce	k6eAd1
žádaný	žádaný	k2eAgInSc1d1
<g/>
,	,	kIx,
prodejen	prodejna	k1gFnPc2
lihovin	lihovina	k1gFnPc2
a	a	k8xC
improvizovaných	improvizovaný	k2eAgInPc2d1
picích	picí	k2eAgInPc2d1
podniků	podnik	k1gInPc2
bylo	být	k5eAaImAgNnS
tak	tak	k9
mnoho	mnoho	k4c1
<g/>
,	,	kIx,
a	a	k8xC
byly	být	k5eAaImAgInP
tak	tak	k6eAd1
dočasné	dočasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
autority	autorita	k1gFnPc1
nebyly	být	k5eNaImAgFnP
s	s	k7c7
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
všechny	všechen	k3xTgInPc4
ustřežit	ustřežit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
kampaň	kampaň	k1gFnSc4
na	na	k7c6
zbavení	zbavení	k1gNnSc6
New	New	k1gFnSc2
York	York	k1gInSc1
City	City	k1gFnSc2
gay	gay	k1gMnSc1
barů	bar	k1gInPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
plném	plný	k2eAgInSc6d1
efektu	efekt	k1gInSc6
na	na	k7c4
příkaz	příkaz	k1gInSc4
primátora	primátor	k1gMnSc2
Roberta	Robert	k1gMnSc2
F.	F.	kA
Wagnera	Wagner	k1gMnSc2
Jr	Jr	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
odebralo	odebrat	k5eAaPmAgNnS
barům	bar	k1gInPc3
licenci	licence	k1gFnSc4
na	na	k7c4
alkohol	alkohol	k1gInSc4
a	a	k8xC
policisté	policista	k1gMnPc1
v	v	k7c6
utajení	utajení	k1gNnSc6
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
polapit	polapit	k5eAaPmF
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
nejvíc	nejvíc	k6eAd1,k6eAd3
homosexuálních	homosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
dělo	dít	k5eAaImAgNnS,k5eAaBmAgNnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
policista	policista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
našel	najít	k5eAaPmAgMnS
muže	muž	k1gMnPc4
v	v	k7c6
baru	bar	k1gInSc6
nebo	nebo	k8xC
parku	park	k1gInSc6
s	s	k7c7
ním	on	k3xPp3gMnSc7
navázal	navázat	k5eAaPmAgMnS
konverzaci	konverzace	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
ta	ten	k3xDgFnSc1
směrovala	směrovat	k5eAaImAgFnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
odejít	odejít	k5eAaPmF
spolu	spolu	k6eAd1
nebo	nebo	k8xC
když	když	k8xS
policista	policista	k1gMnSc1
koupil	koupit	k5eAaPmAgMnS
muži	muž	k1gMnSc3
drink	drink	k1gInSc4
<g/>
,	,	kIx,
muž	muž	k1gMnSc1
byl	být	k5eAaImAgMnS
zatčen	zatknout	k5eAaPmNgMnS
za	za	k7c4
solicitaci	solicitace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Spolku	spolek	k1gInSc3
Mattachine	Mattachin	k1gInSc5
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
tuto	tento	k3xDgFnSc4
kampaň	kampaň	k1gFnSc4
ukončit	ukončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
New	New	k1gFnSc7
York	York	k1gInSc1
State	status	k1gInSc5
Liquor	Liquor	k1gInSc1
Authority	Authorit	k1gInPc1
(	(	kIx(
<g/>
SLA	SLA	kA
<g/>
)	)	kIx)
už	už	k6eAd1
tolik	tolik	k6eAd1
štěstí	štěstit	k5eAaImIp3nP
neměli	mít	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistovaly	existovat	k5eNaImAgInP
sice	sice	k8xC
žádné	žádný	k3yNgInPc1
zákony	zákon	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
by	by	kYmCp3nP
obsluhování	obsluhování	k1gNnPc4
homosexuálů	homosexuál	k1gMnPc2
zakazovaly	zakazovat	k5eAaImAgInP
<g/>
,	,	kIx,
soudy	soud	k1gInPc7
však	však	k8xC
povolovaly	povolovat	k5eAaImAgInP
SLA	SLA	kA
diskrétnost	diskrétnost	k1gFnSc4
v	v	k7c6
schvalování	schvalování	k1gNnSc6
a	a	k8xC
odebírání	odebírání	k1gNnSc6
licencí	licence	k1gFnPc2
na	na	k7c4
alkohol	alkohol	k1gInSc4
pro	pro	k7c4
podniky	podnik	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgInP
„	„	k?
<g/>
narušovat	narušovat	k5eAaImF
veřejný	veřejný	k2eAgInSc4d1
pořádek	pořádek	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přes	přes	k7c4
velký	velký	k2eAgInSc4d1
počet	počet	k1gInSc4
gayů	gay	k1gMnPc2
a	a	k8xC
leseb	lesba	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
Greenwich	Greenwich	k1gInSc4
Village	Village	k1gNnPc2
nazývali	nazývat	k5eAaImAgMnP
domovem	domov	k1gInSc7
<g/>
,	,	kIx,
existovalo	existovat	k5eAaImAgNnS
kromě	kromě	k7c2
barů	bar	k1gInPc2
jen	jen	k9
málo	málo	k4c1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
otevřeně	otevřeně	k6eAd1
shromažďovat	shromažďovat	k5eAaImF
bez	bez	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byli	být	k5eAaImAgMnP
obtěžováni	obtěžovat	k5eAaImNgMnP
nebo	nebo	k8xC
zatčeni	zatknout	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Žádný	žádný	k3yNgInSc1
bar	bar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
gayové	gay	k1gMnPc1
a	a	k8xC
lesby	lesba	k1gFnPc1
často	často	k6eAd1
navštěvovali	navštěvovat	k5eAaImAgMnP
nebyl	být	k5eNaImAgMnS
vlastněn	vlastněn	k2eAgMnSc1d1
gay	gay	k1gMnSc1
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgMnPc4
byli	být	k5eAaImAgMnP
vlastněny	vlastněn	k2eAgMnPc4d1
a	a	k8xC
kontrolovány	kontrolován	k2eAgMnPc4d1
organizovaným	organizovaný	k2eAgInSc7d1
zločinem	zločin	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
zákazníky	zákazník	k1gMnPc4
zacházel	zacházet	k5eAaImAgMnS
chabě	chabě	k6eAd1
<g/>
,	,	kIx,
ředil	ředit	k5eAaImAgMnS
alkohol	alkohol	k1gInSc4
vodou	voda	k1gFnSc7
a	a	k8xC
ceny	cena	k1gFnPc4
stavěl	stavět	k5eAaImAgMnS
vysoko	vysoko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
ale	ale	k8xC
platil	platit	k5eAaImAgMnS
policii	policie	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabránil	zabránit	k5eAaPmAgInS
častým	častý	k2eAgFnPc3d1
raziím	razie	k1gFnPc3
<g/>
.	.	kIx.
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stonewall	Stonewall	k1gMnSc1
Inn	Inn	k1gMnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Stonewall	Stonewalla	k1gFnPc2
Inn	Inn	k1gFnSc1
v	v	k7c4
Greenwich	Greenwich	k1gInSc4
Village	Villag	k1gFnSc2
</s>
<s>
Stonewall	Stonewall	k1gInSc1
Inn	Inn	k1gMnPc2
patřil	patřit	k5eAaImAgInS
zločinecké	zločinecký	k2eAgFnSc3d1
rodině	rodina	k1gFnSc3
Genovese	Genovese	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
tři	tři	k4xCgMnPc1
členové	člen	k1gMnPc1
Mafie	mafie	k1gFnSc2
investovali	investovat	k5eAaBmAgMnP
$	$	kIx~
<g/>
3,500	3,500	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
změnili	změnit	k5eAaPmAgMnP
Stonewall	Stonewall	k1gInSc4
Inn	Inn	k1gFnSc2
na	na	k7c6
gay	gay	k1gMnSc1
bar	bar	k1gInSc1
(	(	kIx(
<g/>
předtím	předtím	k6eAd1
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
restaurace	restaurace	k1gFnSc1
a	a	k8xC
noční	noční	k2eAgInSc1d1
klub	klub	k1gInSc1
pro	pro	k7c4
heterosexuály	heterosexuál	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
týdně	týdně	k6eAd1
si	se	k3xPyFc3
policie	policie	k1gFnSc1
vyzvedla	vyzvednout	k5eAaPmAgFnS
obálku	obálka	k1gFnSc4
s	s	k7c7
penězi	peníze	k1gInPc7
jako	jako	k9
úplatek	úplatek	k1gInSc1
—	—	k?
Stonewall	Stonewall	k1gMnSc1
Inn	Inn	k1gMnSc1
neměl	mít	k5eNaImAgMnS
žádnou	žádný	k3yNgFnSc4
licenci	licence	k1gFnSc4
na	na	k7c4
alkohol	alkohol	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Neměl	mít	k5eNaImAgMnS
ani	ani	k8xC
tekoucí	tekoucí	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
za	za	k7c7
barem	bar	k1gInSc7
—	—	k?
použité	použitý	k2eAgFnSc2d1
skleničky	sklenička	k1gFnSc2
byly	být	k5eAaImAgInP
namočené	namočený	k2eAgInPc1d1
do	do	k7c2
kádě	káď	k1gFnSc2
s	s	k7c7
vodou	voda	k1gFnSc7
a	a	k8xC
hned	hned	k6eAd1
znova	znova	k6eAd1
použity	použít	k5eAaPmNgFnP
<g/>
.	.	kIx.
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Nebyly	být	k5eNaImAgInP
tam	tam	k6eAd1
žádné	žádný	k3yNgInPc1
únikové	únikový	k2eAgInPc1d1
východy	východ	k1gInPc1
a	a	k8xC
toalety	toaleta	k1gFnPc1
neustále	neustále	k6eAd1
přetékaly	přetékat	k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
bar	bar	k1gInSc1
nebyl	být	k5eNaImAgInS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
k	k	k7c3
prostituci	prostituce	k1gFnSc3
<g/>
,	,	kIx,
probíhal	probíhat	k5eAaImAgInS
tam	tam	k6eAd1
prodej	prodej	k1gInSc1
drog	droga	k1gFnPc2
a	a	k8xC
jiné	jiný	k2eAgNnSc4d1
„	„	k?
<g/>
hotovostní	hotovostní	k2eAgFnSc2d1
transakce	transakce	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
jediný	jediný	k2eAgInSc1d1
bar	bar	k1gInSc1
pro	pro	k7c4
gaye	gay	k1gMnPc4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
povoleno	povolen	k2eAgNnSc1d1
tancovat	tancovat	k5eAaImF
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
lákadlem	lákadlo	k1gNnSc7
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
znovuotevření	znovuotevření	k1gNnSc6
jako	jako	k9
gay	gay	k1gMnSc1
baru	bar	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Policejní	policejní	k2eAgFnSc1d1
razie	razie	k1gFnSc1
gay	gay	k1gMnSc1
barů	bar	k1gInPc2
byly	být	k5eAaImAgFnP
časté	častý	k2eAgFnPc1d1
<g/>
,	,	kIx,
průměrně	průměrně	k6eAd1
jednou	jeden	k4xCgFnSc7
do	do	k7c2
měsíce	měsíc	k1gInSc2
v	v	k7c6
každém	každý	k3xTgInSc6
baru	bar	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
barů	bar	k1gInPc2
drželo	držet	k5eAaImAgNnS
extra	extra	k6eAd1
alkohol	alkohol	k1gInSc4
v	v	k7c6
tajné	tajný	k2eAgFnSc6d1
přihrádce	přihrádka	k1gFnSc6
za	za	k7c7
barem	bar	k1gInSc7
nebo	nebo	k8xC
v	v	k7c6
autě	auto	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
usnadnilo	usnadnit	k5eAaPmAgNnS
pokračování	pokračování	k1gNnSc1
co	co	k9
nejrychleji	rychle	k6eAd3
to	ten	k3xDgNnSc1
šlo	jít	k5eAaImAgNnS
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
alkohol	alkohol	k1gInSc1
zabaven	zabavit	k5eAaPmNgInS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Management	management	k1gInSc1
baru	bar	k1gInSc2
obvykle	obvykle	k6eAd1
o	o	k7c6
raziích	razie	k1gFnPc6
díky	díky	k7c3
policejním	policejní	k2eAgInPc3d1
tipům	tip	k1gInPc3
věděl	vědět	k5eAaImAgMnS
dopředu	dopředu	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
razie	razie	k1gFnSc1
probíhaly	probíhat	k5eAaImAgFnP
brzy	brzy	k6eAd1
večer	večer	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
po	po	k7c6
skončení	skončení	k1gNnSc6
mohl	moct	k5eAaImAgInS
prodej	prodej	k1gInSc1
pokračovat	pokračovat	k5eAaImF
<g/>
.	.	kIx.
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
typické	typický	k2eAgFnSc2d1
razie	razie	k1gFnSc2
se	se	k3xPyFc4
rozsvítila	rozsvítit	k5eAaPmAgFnS
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
zákazníci	zákazník	k1gMnPc1
byli	být	k5eAaImAgMnP
postaveni	postavit	k5eAaPmNgMnP
do	do	k7c2
řady	řada	k1gFnSc2
a	a	k8xC
proběhla	proběhnout	k5eAaPmAgFnS
kontrola	kontrola	k1gFnSc1
občanských	občanský	k2eAgInPc2d1
průkazů	průkaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
je	on	k3xPp3gNnSc4
neměli	mít	k5eNaImAgMnP
<g/>
,	,	kIx,
nebo	nebo	k8xC
drag	drag	k1gInSc1
queens	queensa	k1gFnPc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
zatčeni	zatknout	k5eAaPmNgMnP
<g/>
;	;	kIx,
ostatní	ostatní	k2eAgMnPc4d1
mohli	moct	k5eAaImAgMnP
odejít	odejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
i	i	k8xC
drag	drag	k1gInSc1
queens	queensa	k1gFnPc2
<g/>
,	,	kIx,
využívali	využívat	k5eAaImAgMnP,k5eAaPmAgMnP
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
průkazy	průkaz	k1gInPc1
z	z	k7c2
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženy	žena	k1gFnPc1
musely	muset	k5eAaImAgFnP
mít	mít	k5eAaImF
na	na	k7c4
sobě	se	k3xPyFc3
tři	tři	k4xCgInPc4
kusy	kus	k1gInPc1
ženského	ženský	k2eAgNnSc2d1
oblečení	oblečení	k1gNnSc2
a	a	k8xC
když	když	k8xS
je	on	k3xPp3gFnPc4
neměly	mít	k5eNaImAgFnP
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zatčené	zatčený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměstnanci	zaměstnanec	k1gMnPc1
a	a	k8xC
management	management	k1gInSc1
byl	být	k5eAaImAgInS
obvykle	obvykle	k6eAd1
také	také	k9
zatčen	zatknout	k5eAaPmNgMnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Období	období	k1gNnSc1
těsně	těsně	k6eAd1
před	před	k7c7
28	#num#	k4
<g/>
.	.	kIx.
červnem	červen	k1gInSc7
1969	#num#	k4
bylo	být	k5eAaImAgNnS
plné	plný	k2eAgNnSc1d1
razií	razie	k1gFnSc7
na	na	k7c4
místní	místní	k2eAgInPc4d1
bary	bar	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
razie	razie	k1gFnSc2
v	v	k7c4
Stonewall	Stonewall	k1gInSc4
Inn	Inn	k1gFnSc2
v	v	k7c6
úterý	úterý	k1gNnSc6
před	před	k7c7
povstáním	povstání	k1gNnSc7
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
uzavřením	uzavření	k1gNnSc7
Checkerboard	Checkerboarda	k1gFnPc2
<g/>
,	,	kIx,
Tele-Star	Tele-Stara	k1gFnPc2
a	a	k8xC
dvou	dva	k4xCgInPc2
jiných	jiný	k2eAgInPc2d1
barů	bar	k1gInPc2
v	v	k7c4
Greenwich	Greenwich	k1gInSc4
Village	Villag	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zásah	zásah	k1gInSc1
ve	v	k7c4
Stonewall	Stonewall	k1gInSc4
Inn	Inn	k1gFnSc2
</s>
<s>
Stonewall	Stonewall	k1gMnSc1
Inn	Inn	k1gMnSc1
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
</s>
<s>
V	v	k7c4
sobotu	sobota	k1gFnSc4
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
v	v	k7c4
1.20	1.20	k4
hod	hod	k1gInSc4
<g/>
.	.	kIx.
ráno	ráno	k6eAd1
zahájila	zahájit	k5eAaPmAgFnS
policie	policie	k1gFnSc1
v	v	k7c6
klubu	klub	k1gInSc6
zásah	zásah	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bar	bar	k1gInSc1
neměl	mít	k5eNaImAgInS
mj.	mj.	kA
povolení	povolení	k1gNnPc4
k	k	k7c3
prodeji	prodej	k1gInSc3
lihovin	lihovina	k1gFnPc2
a	a	k8xC
rovněž	rovněž	k9
hygienické	hygienický	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
v	v	k7c6
něm	on	k3xPp3gInSc6
byly	být	k5eAaImAgFnP
diskutabilní	diskutabilní	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Policejní	policejní	k2eAgInSc1d1
zásah	zásah	k1gInSc1
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
měli	mít	k5eAaImAgMnP
být	být	k5eAaImF
mj.	mj.	kA
transvestité	transvestit	k1gMnPc1
rozeznáni	rozeznán	k2eAgMnPc1d1
od	od	k7c2
žen	žena	k1gFnPc2
a	a	k8xC
zatčeni	zatčen	k2eAgMnPc1d1
<g/>
,	,	kIx,
příslušníci	příslušník	k1gMnPc1
mafie	mafie	k1gFnSc2
eskortováni	eskortován	k2eAgMnPc1d1
a	a	k8xC
všichni	všechen	k3xTgMnPc1
přítomní	přítomný	k1gMnPc1
identifikováni	identifikovat	k5eAaBmNgMnP
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
vymkl	vymknout	k5eAaPmAgInS
z	z	k7c2
ruky	ruka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvášněný	rozvášněný	k2eAgInSc1d1
dav	dav	k1gInSc1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
složený	složený	k2eAgInSc1d1
z	z	k7c2
návštěvníků	návštěvník	k1gMnPc2
samotného	samotný	k2eAgInSc2d1
Stonewall	Stonewalla	k1gFnPc2
Inn	Inn	k1gFnPc4
a	a	k8xC
částečně	částečně	k6eAd1
z	z	k7c2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
seběhli	seběhnout	k5eAaPmAgMnP
z	z	k7c2
okolí	okolí	k1gNnSc2
<g/>
,	,	kIx,
napadl	napadnout	k5eAaPmAgMnS
policii	policie	k1gFnSc4
a	a	k8xC
ta	ten	k3xDgFnSc1
mu	on	k3xPp3gMnSc3
stěží	stěží	k6eAd1
čelila	čelit	k5eAaImAgFnS
i	i	k9
přes	přes	k7c4
přivolané	přivolaný	k2eAgFnPc4d1
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
skandovali	skandovat	k5eAaImAgMnP
hesla	heslo	k1gNnSc2
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
Gay	gay	k1gMnSc1
power	power	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
a	a	k8xC
házeli	házet	k5eAaImAgMnP
po	po	k7c6
policistech	policista	k1gMnPc6
popelnice	popelnice	k1gFnPc4
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
předměty	předmět	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
čtyři	čtyři	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
ráno	ráno	k6eAd1
se	se	k3xPyFc4
situace	situace	k1gFnSc1
zklidnila	zklidnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Pravě	pravě	k6eAd1
v	v	k7c4
sobotu	sobota	k1gFnSc4
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
vzdala	vzdát	k5eAaPmAgFnS
homosexuální	homosexuální	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
hold	hold	k1gInSc4
své	svůj	k3xOyFgFnSc3
ikoně	ikona	k1gFnSc3
<g/>
,	,	kIx,
zpívající	zpívající	k2eAgFnSc3d1
hvězdě	hvězda	k1gFnSc3
hollywoodské	hollywoodský	k2eAgFnPc4d1
„	„	k?
<g/>
zlaté	zlatý	k2eAgFnPc4d1
éry	éra	k1gFnPc4
<g/>
“	“	k?
Judy	judo	k1gNnPc7
Garlandové	Garlandový	k2eAgFnSc2d1
(	(	kIx(
<g/>
Čaroděj	čaroděj	k1gMnSc1
ze	z	k7c2
země	zem	k1gFnSc2
Oz	Oz	k1gFnSc2
–	–	k?
Oscar	Oscar	k1gInSc1
pro	pro	k7c4
dětské	dětský	k2eAgInPc4d1
herce	herc	k1gInPc4
<g/>
)	)	kIx)
velkou	velký	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
na	na	k7c6
jejím	její	k3xOp3gInSc6
pohřbu	pohřeb	k1gInSc6
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
20	#num#	k4
000	#num#	k4
přítomných	přítomný	k1gMnPc2
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
kolem	kolem	k7c2
12	#num#	k4
000	#num#	k4
gayů	gay	k1gMnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
odhadu	odhad	k1gInSc2
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reakce	reakce	k1gFnSc1
na	na	k7c4
vzpouru	vzpoura	k1gFnSc4
</s>
<s>
V	v	k7c6
následujících	následující	k2eAgInPc6d1
dnech	den	k1gInPc6
zaplnily	zaplnit	k5eAaPmAgFnP
Greenwich	Greenwich	k1gInSc4
Village	Villag	k1gInSc2
protestní	protestní	k2eAgNnSc4d1
grafitti	grafitti	k1gNnSc4
požadující	požadující	k2eAgFnSc4d1
legalizaci	legalizace	k1gFnSc4
gay	gay	k1gMnSc1
barů	bar	k1gInPc2
a	a	k8xC
rovnost	rovnost	k1gFnSc4
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Události	událost	k1gFnPc4
ve	v	k7c4
Stonewall	Stonewall	k1gInSc4
Inn	Inn	k1gMnPc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
hlavním	hlavní	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
významných	významný	k2eAgFnPc2d1
tiskovin	tiskovina	k1gFnPc2
už	už	k6eAd1
v	v	k7c6
pondělí	pondělí	k1gNnSc6
<g/>
;	;	kIx,
nepokoje	nepokoj	k1gInSc2
formou	forma	k1gFnSc7
demonstrací	demonstrace	k1gFnSc7
v	v	k7c6
řádu	řád	k1gInSc6
stovek	stovka	k1gFnPc2
lidí	člověk	k1gMnPc2
trvaly	trvat	k5eAaImAgFnP
do	do	k7c2
úterý	úterý	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
nepokoje	nepokoj	k1gInSc2
neschvalovali	schvalovat	k5eNaImAgMnP
všichni	všechen	k3xTgMnPc1
homosexuálové	homosexuál	k1gMnPc1
<g/>
,	,	kIx,
události	událost	k1gFnPc1
aktivizovaly	aktivizovat	k5eAaImAgFnP
již	již	k6eAd1
existující	existující	k2eAgFnPc1d1
homosexuální	homosexuální	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
k	k	k7c3
razantnějšímu	razantní	k2eAgInSc3d2
postupu	postup	k1gInSc3
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
diskriminaci	diskriminace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Téma	téma	k1gNnSc1
homosexuality	homosexualita	k1gFnSc2
od	od	k7c2
tohoto	tento	k3xDgInSc2
momentu	moment	k1gInSc2
rezonuje	rezonovat	k5eAaImIp3nS
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
i	i	k8xC
v	v	k7c6
médiích	médium	k1gNnPc6
výrazněji	výrazně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepokoje	nepokoj	k1gInSc2
vedly	vést	k5eAaImAgFnP
mj.	mj.	kA
k	k	k7c3
založení	založení	k1gNnSc3
osvobozenecké	osvobozenecký	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
gayů	gay	k1gMnPc2
a	a	k8xC
leseb	lesba	k1gFnPc2
Gay	gay	k1gMnSc1
Liberation	Liberation	k1gInSc4
Front	front	k1gInSc1
(	(	kIx(
<g/>
GLF	GLF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
v	v	k7c6
médiích	médium	k1gNnPc6
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Before	Befor	k1gMnSc5
Stonewall	Stonewall	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Making	Making	k1gInSc1
of	of	k?
a	a	k8xC
Gay	gay	k1gMnSc1
and	and	k?
Lesbian	Lesbian	k1gMnSc1
Community	Communita	k1gFnSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
o	o	k7c6
desetiletích	desetiletí	k1gNnPc6
předcházejících	předcházející	k2eAgNnPc6d1
Stonewall	Stonewall	k1gInSc4
</s>
<s>
Stonewall	Stonewall	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zbeletrizovaná	zbeletrizovaný	k2eAgFnSc1d1
prezentace	prezentace	k1gFnSc1
událostí	událost	k1gFnPc2
předcházejících	předcházející	k2eAgFnPc2d1
nepokoje	nepokoj	k1gInPc4
</s>
<s>
After	After	k1gMnSc1
Stonewall	Stonewall	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
o	o	k7c6
letech	léto	k1gNnPc6
od	od	k7c2
Stonewallu	Stonewall	k1gInSc2
do	do	k7c2
konce	konec	k1gInSc2
století	století	k1gNnSc2
</s>
<s>
Stonewall	Stonewall	k1gInSc1
Uprising	Uprising	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokumentární	dokumentární	k2eAgFnPc1d1
prezentace	prezentace	k1gFnPc1
využívající	využívající	k2eAgFnPc4d1
archivní	archivní	k2eAgFnPc4d1
nahrávky	nahrávka	k1gFnPc4
<g/>
,	,	kIx,
fotografie	fotografia	k1gFnPc4
<g/>
,	,	kIx,
dokumenty	dokument	k1gInPc4
a	a	k8xC
svědecké	svědecký	k2eAgFnPc4d1
výpovědi	výpověď	k1gFnPc4
</s>
<s>
Stonewall	Stonewall	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
další	další	k2eAgNnSc4d1
zbeletrizované	zbeletrizovaný	k2eAgNnSc4d1
drama	drama	k1gNnSc4
o	o	k7c4
dnes	dnes	k6eAd1
před	před	k7c7
nepokoji	nepokoj	k1gInPc7
</s>
<s>
Happy	Happa	k1gFnPc1
Birthday	Birthdaa	k1gMnSc2
<g/>
,	,	kIx,
Marsha	Marsh	k1gMnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
krátký	krátký	k2eAgInSc1d1
experimentální	experimentální	k2eAgInSc1d1
film	film	k1gInSc1
o	o	k7c6
průkopnících	průkopnící	k2eAgFnPc6d1
transgender	transgender	k1gInSc4
práv	právo	k1gNnPc2
Marshe	Marsh	k1gMnSc2
P.	P.	kA
Johnson	Johnson	k1gMnSc1
a	a	k8xC
Silvii	Silvia	k1gFnSc4
Rivera	Rivero	k1gNnSc2
<g/>
,	,	kIx,
odehrávající	odehrávající	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c4
noc	noc	k1gFnSc4
nepokojů	nepokoj	k1gInPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
V	v	k7c6
tom	ten	k3xDgInSc6
čase	čas	k1gInSc6
se	se	k3xPyFc4
termín	termín	k1gInSc1
„	„	k?
<g/>
gay	gay	k1gMnSc1
<g/>
“	“	k?
běžně	běžně	k6eAd1
používal	používat	k5eAaImAgMnS
na	na	k7c4
označení	označení	k1gNnSc4
všech	všecek	k3xTgInPc2
LGBT	LGBT	kA
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Illinois	Illinois	k1gInSc1
dekriminalizoval	dekriminalizovat	k5eAaPmAgInS
sodomii	sodomie	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
době	doba	k1gFnSc6
Stonewallského	Stonewallský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
všechny	všechen	k3xTgFnPc4
ostatní	ostatní	k2eAgInPc1d1
státy	stát	k1gInPc1
kriminalizovaly	kriminalizovat	k5eAaImAgInP
homosexuální	homosexuální	k2eAgInSc1d1
jednání	jednání	k1gNnSc2
i	i	k8xC
mezi	mezi	k7c7
plnoletými	plnoletý	k2eAgMnPc7d1
jedinci	jedinec	k1gMnPc7
jednajícími	jednající	k2eAgMnPc7d1
v	v	k7c6
soukromí	soukromí	k1gNnSc6
<g/>
.	.	kIx.
„	„	k?
<g/>
Dospělý	dospělý	k1gMnSc1
člověk	člověk	k1gMnSc1
odsouzený	odsouzený	k1gMnSc1
za	za	k7c4
zločin	zločin	k1gInSc4
sexu	sex	k1gInSc2
s	s	k7c7
jiným	jiný	k2eAgMnSc7d1
plnoletým	plnoletý	k2eAgMnSc7d1
člověkem	člověk	k1gMnSc7
v	v	k7c4
soukromí	soukromí	k1gNnSc4
svého	svůj	k3xOyFgInSc2
domova	domov	k1gInSc2
mohl	moct	k5eAaImAgInS
dostat	dostat	k5eAaPmF
malou	malý	k2eAgFnSc4d1
pokutu	pokuta	k1gFnSc4
<g/>
,	,	kIx,
pět	pět	k4xCc4
<g/>
,	,	kIx,
deset	deset	k4xCc4
nebo	nebo	k8xC
dvacet	dvacet	k4xCc4
—	—	k?
nebo	nebo	k8xC
i	i	k9
doživotí	doživotí	k1gNnSc4
—	—	k?
ve	v	k7c6
vězení	vězení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvacet	dvacet	k4xCc4
států	stát	k1gInPc2
mělo	mít	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
zákony	zákon	k1gInPc1
o	o	k7c4
«	«	k?
<g/>
sexuálních	sexuální	k2eAgInPc6d1
psychopatech	psychopat	k1gMnPc6
<g/>
»	»	k?
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
povolovaly	povolovat	k5eAaImAgInP
zadržení	zadržení	k1gNnSc4
homosexuálů	homosexuál	k1gMnPc2
jen	jen	k9
z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Pensylvánii	Pensylvánie	k1gFnSc6
a	a	k8xC
Kalifornii	Kalifornie	k1gFnSc6
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
sexuální	sexuální	k2eAgMnPc1d1
delikventi	delikvent	k1gMnPc1
umístěni	umístit	k5eAaPmNgMnP
do	do	k7c2
psychiatrické	psychiatrický	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
na	na	k7c4
doživotí	doživotí	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
několika	několik	k4yIc6
státech	stát	k1gInPc6
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
vykastrováni	vykastrovat	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
15	#num#	k4
<g/>
)	)	kIx)
Po	po	k7c4
dobu	doba	k1gFnSc4
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
byly	být	k5eAaImAgFnP
kastrace	kastrace	k1gFnPc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
dávící	dávící	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
<g/>
,	,	kIx,
hypnóza	hypnóza	k1gFnSc1
<g/>
,	,	kIx,
elektrokonvulzivní	elektrokonvulzivní	k2eAgFnSc1d1
terapie	terapie	k1gFnSc1
a	a	k8xC
lobotomie	lobotomie	k1gFnSc1
používány	používán	k2eAgMnPc4d1
psychiatry	psychiatr	k1gMnPc4
k	k	k7c3
„	„	k?
<g/>
léčení	léčení	k1gNnSc2
<g/>
“	“	k?
homosexuálů	homosexuál	k1gMnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Katz	Katz	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
181	#num#	k4
<g/>
–	–	k?
<g/>
197	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
60	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Stonewall	Stonewall	k1gInSc1
riots	riots	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Brief	Brief	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Gay	gay	k1gMnSc1
and	and	k?
Lesbian	Lesbian	k1gInSc1
Rights	Rights	k1gInSc1
Movement	Movement	k1gInSc1
in	in	k?
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Kentucky	Kentucka	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nell	Nell	k1gMnSc1
Frizzell	Frizzell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feature	Featur	k1gMnSc5
<g/>
:	:	kIx,
How	How	k1gFnSc3
the	the	k?
Stonewall	Stonewall	k1gInSc1
riots	riots	k1gInSc1
started	started	k1gInSc4
the	the	k?
LGBT	LGBT	kA
rights	rights	k1gInSc1
movement	movement	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pink	pink	k2eAgInSc1d1
News	News	k1gInSc1
UK	UK	kA
<g/>
,	,	kIx,
June	jun	k1gMnSc5
28	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stonewall	Stonewall	k1gInSc1
riots	riots	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
National	National	k1gFnSc1
Park	park	k1gInSc1
Service	Service	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Civil	civil	k1gMnSc1
Rights	Rights	k1gInSc1
at	at	k?
Stonewall	Stonewall	k1gInSc1
National	National	k1gMnSc1
Monument	monument	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
the	the	k?
Interior	Interior	k1gInSc1
<g/>
,	,	kIx,
October	October	k1gInSc1
17	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Obama	Obama	k?
inaugural	inaugurat	k5eAaImAgMnS,k5eAaPmAgMnS
speech	speech	k1gInSc4
references	references	k1gMnSc1
Stonewall	Stonewall	k1gMnSc1
gay-rights	gay-rights	k6eAd1
riots	riots	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Allen	Allen	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Young	Younga	k1gFnPc2
a	a	k8xC
Allen	Allen	k1gMnSc1
Ginsberg	Ginsberg	k1gMnSc1
-	-	kIx~
Interview	interview	k1gNnSc1
pro	pro	k7c4
Gay	gay	k1gMnSc1
Sunshine	Sunshin	k1gInSc5
<g/>
,	,	kIx,
Votobia	Votobia	k1gFnSc1
1996	#num#	k4
<g/>
↑	↑	k?
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
15.1	15.1	k4
2	#num#	k4
3	#num#	k4
Duberman	Duberman	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
183	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Carter	Carter	k1gInSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
79	#num#	k4
<g/>
–	–	k?
<g/>
83	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Heritage	Heritage	k1gInSc1
|	|	kIx~
1970	#num#	k4
Christopher	Christophra	k1gFnPc2
Street	Streeta	k1gFnPc2
Liberation	Liberation	k1gInSc1
Day	Day	k1gMnSc1
Gay-In	Gay-In	k1gMnSc1
<g/>
,	,	kIx,
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SF	SF	kA
Pride	Prid	k1gMnSc5
<g/>
,	,	kIx,
June	jun	k1gMnSc5
28	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
October	October	k1gMnSc1
22	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Pride	Prid	k1gInSc5
Marches	Marches	k1gMnSc1
and	and	k?
Parades	Parades	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
"	"	kIx"
<g/>
,	,	kIx,
in	in	k?
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Lesbian	Lesbian	k1gMnSc1
<g/>
,	,	kIx,
Gay	gay	k1gMnSc1
<g/>
,	,	kIx,
Bisexual	Bisexual	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Transgender	Transgender	k1gInSc1
History	Histor	k1gInPc1
in	in	k?
America	Americ	k1gInSc2
<g/>
,	,	kIx,
Marc	Marc	k1gFnSc1
Stein	Stein	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Scribner	Scribner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Sons	Sonsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nakamura	Nakamura	k1gFnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
EILPERIN	EILPERIN	kA
<g/>
,	,	kIx,
JULIET	JULIET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
With	With	k1gMnSc1
Stonewall	Stonewall	k1gMnSc1
<g/>
,	,	kIx,
Obama	Obama	k?
designates	designates	k1gMnSc1
first	first	k1gMnSc1
national	nationat	k5eAaPmAgMnS,k5eAaImAgMnS
monument	monument	k1gInSc4
to	ten	k3xDgNnSc1
gay	gay	k1gMnSc1
rights	rightsa	k1gFnPc2
movement	movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
June	jun	k1gMnSc5
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
56	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Edsall	Edsall	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
277	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
David	David	k1gMnSc1
K.	K.	kA
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Lavender	Lavender	k1gMnSc1
Scare	Scar	k1gMnSc5
<g/>
:	:	kIx,
The	The	k1gMnSc6
Cold	Cold	k1gMnSc1
War	War	k1gFnSc2
Persecution	Persecution	k1gInSc1
of	of	k?
Gays	Gays	k1gInSc1
and	and	k?
Lesbians	Lesbians	k1gInSc1
in	in	k?
the	the	k?
Federal	Federal	k1gMnSc1
Government	Government	k1gMnSc1
(	(	kIx(
<g/>
University	universita	k1gFnPc1
of	of	k?
Chicago	Chicago	k1gNnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
101	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
114	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
58	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Edsall	Edsall	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
278	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
59	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Edsall	Edsall	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
247	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Edsall	Edsall	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
310	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
58	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
24	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
62	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
63	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
64	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
42	#num#	k4
<g/>
–	–	k?
<g/>
43	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
21	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gallo	Gallo	k1gNnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
pp	pp	k?
<g/>
.	.	kIx.
47	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
80	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marcus	Marcus	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
105	#num#	k4
<g/>
–	–	k?
<g/>
108	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DiGuglielmo	DiGuglielma	k1gFnSc5
<g/>
,	,	kIx,
Joey	Joey	k1gInPc1
(	(	kIx(
<g/>
October	October	k1gInSc1
20	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Steps	Steps	k1gInSc1
to	ten	k3xDgNnSc4
Stonewall	Stonewall	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
Blade	Blad	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc4
on	on	k3xPp3gMnSc1
November	November	k1gMnSc1
5	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
72	#num#	k4
<g/>
–	–	k?
<g/>
73.1	73.1	k4
2	#num#	k4
Stryker	Strykra	k1gFnPc2
<g/>
,	,	kIx,
Susan	Susan	k1gMnSc1
(	(	kIx(
<g/>
Winter	Winter	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Transgender	Transgender	k1gMnSc1
History	Histor	k1gInPc4
<g/>
,	,	kIx,
Homonormativity	Homonormativita	k1gFnPc4
<g/>
,	,	kIx,
and	and	k?
Disciplinarity	Disciplinarita	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radical	Radical	k1gFnPc2
History	Histor	k1gInPc1
Review	Review	k1gFnSc2
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
145	#num#	k4
<g/>
–	–	k?
<g/>
157	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Faderman	Faderman	k1gMnSc1
and	and	k?
Timmons	Timmons	k1gInSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
2	#num#	k4
Boyd	Boyda	k1gFnPc2
<g/>
,	,	kIx,
Nan	Nan	k1gMnSc1
Alamilla	Alamilla	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
<g/>
"	"	kIx"
in	in	k?
the	the	k?
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Lesbian	Lesbian	k1gMnSc1
<g/>
,	,	kIx,
Gay	gay	k1gMnSc1
<g/>
,	,	kIx,
Bisexual	Bisexual	k1gMnSc1
and	and	k?
Transgendered	Transgendered	k1gInSc1
History	Histor	k1gInPc1
in	in	k?
America	Americus	k1gMnSc4
<g/>
,	,	kIx,
Ed	Ed	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marc	Marc	k1gInSc4
Stein	Stein	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charles	Charles	k1gMnSc1
Scribner	Scribner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Sons	Sonsa	k1gFnPc2
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
71	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Edsall	Edsall	k1gInSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
253	#num#	k4
<g/>
–	–	k?
<g/>
254	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Edsall	Edsall	k1gInSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
255	#num#	k4
<g/>
–	–	k?
<g/>
256	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
29	#num#	k4
<g/>
–	–	k?
<g/>
37	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Carter	Carter	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
48.1	48.1	k4
2	#num#	k4
Duberman	Duberman	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
181	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duberman	Duberman	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
185	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Carter	Carter	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
68	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Carter	Carter	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
p.	p.	k?
80	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duberman	Duberman	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
182	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Carter	Carter	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
71.1	71.1	k4
2	#num#	k4
Duberman	Duberman	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
192	#num#	k4
<g/>
–	–	k?
<g/>
193	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Carter	Carter	k1gInSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
124	#num#	k4
<g/>
–	–	k?
<g/>
125	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ESKOW	ESKOW	kA
<g/>
,	,	kIx,
Dennis	Dennis	k1gFnSc1
<g/>
.	.	kIx.
4	#num#	k4
Policemen	Policemen	k1gInSc1
Hurt	Hurt	k1gMnSc1
in	in	k?
'	'	kIx"
<g/>
Village	Village	k1gFnSc1
<g/>
'	'	kIx"
Raid	raid	k1gInSc1
<g/>
:	:	kIx,
Melee	Melee	k1gFnSc1
Near	Near	k1gInSc4
Sheridan	Sheridan	k1gInSc1
Square	square	k1gInSc1
Follows	Follows	k1gInSc1
Action	Action	k1gInSc1
at	at	k?
Bar	bar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
June	jun	k1gMnSc5
29	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ADAM	Adam	k1gMnSc1
<g/>
,	,	kIx,
Barry	Barra	k1gFnPc1
D.	D.	kA
The	The	k1gMnSc4
rise	ris	k1gMnSc4
of	of	k?
a	a	k8xC
gay	gay	k1gMnSc1
and	and	k?
lesbian	lesbian	k1gMnSc1
movement	movement	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boston	Boston	k1gInSc1
<g/>
:	:	kIx,
Twayne	Twayn	k1gInSc5
Publishers	Publishers	k1gInSc1
<g/>
,	,	kIx,
c	c	k0
<g/>
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
805797149	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CARTER	CARTER	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stonewall	Stonewall	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
riots	riots	k1gInSc1
that	that	k1gMnSc1
sparked	sparked	k1gMnSc1
the	the	k?
gay	gay	k1gMnSc1
revolution	revolution	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
312342691	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DUBERMAN	DUBERMAN	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
B.	B.	kA
Stonewall	Stonewall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
N.	N.	kA
<g/>
Y.	Y.	kA
<g/>
,	,	kIx,
U.	U.	kA
<g/>
S.	S.	kA
<g/>
A.	A.	kA
<g/>
:	:	kIx,
Dutton	Dutton	k1gInSc1
<g/>
,	,	kIx,
c	c	k0
<g/>
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
525936025	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
EDSALL	EDSALL	kA
<g/>
,	,	kIx,
Nicholas	Nicholas	k1gMnSc1
C.	C.	kA
Toward	Toward	k1gMnSc1
Stonewall	Stonewall	k1gMnSc1
<g/>
:	:	kIx,
homosexuality	homosexualita	k1gFnPc1
and	and	k?
society	societa	k1gFnSc2
in	in	k?
the	the	k?
modern	modern	k1gInSc1
western	western	k1gInSc1
world	world	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charlottesville	Charlottesville	k1gNnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Virginia	Virginium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
c	c	k0
<g/>
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
813922119	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FADERMAN	FADERMAN	kA
<g/>
,	,	kIx,
Lillian	Lillian	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odd	odd	kA
girls	girl	k1gFnPc2
and	and	k?
twilight	twilight	k5eAaPmF
lovers	lovers	k6eAd1
<g/>
:	:	kIx,
a	a	k8xC
history	histor	k1gInPc1
of	of	k?
lesbian	lesbian	k1gInSc1
life	life	k1gInSc1
in	in	k?
twentieth-century	twentieth-centura	k1gFnSc2
America	America	k1gMnSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
print	print	k1gInSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
N.	N.	kA
<g/>
Y	Y	kA
<g/>
:	:	kIx,
Penguin	Penguin	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
140171223	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FADERMAN	FADERMAN	kA
<g/>
,	,	kIx,
Lillian	Lillian	k1gInSc1
<g/>
.	.	kIx.
a	a	k8xC
Stuart	Stuart	k1gInSc4
TIMMONS	TIMMONS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gay	gay	k1gMnSc1
L.A.	L.A.	k1gMnSc1
<g/>
:	:	kIx,
a	a	k8xC
history	histor	k1gInPc1
of	of	k?
sexual	sexual	k1gInSc1
outlaws	outlaws	k1gInSc1
<g/>
,	,	kIx,
power	power	k1gInSc1
politics	politics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
lipstick	lipstick	k1gInSc1
lesbians	lesbians	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Basic	Basic	kA
Books	Books	k1gInSc1
<g/>
,	,	kIx,
c	c	k0
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
46502288	#num#	k4
<g/>
X.	X.	kA
</s>
<s>
MARCIA	MARCIA	kA
M.	M.	kA
GALLO	GALLO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Different	Different	k1gInSc1
daughters	daughters	k1gInSc4
<g/>
:	:	kIx,
a	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
Daughters	Daughters	k1gInSc1
of	of	k?
Bilitis	Bilitis	k1gFnSc1
and	and	k?
the	the	k?
rise	rise	k1gInSc1
of	of	k?
the	the	k?
lesbian	lesbian	k1gInSc1
rights	rights	k1gInSc1
movement	movement	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Seal	Seal	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1580052525	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KATZ	KATZ	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gay	gay	k1gMnSc1
American	American	k1gMnSc1
history	histor	k1gInPc7
<g/>
:	:	kIx,
lesbians	lesbians	k6eAd1
and	and	k?
gay	gay	k1gMnSc1
men	men	k?
in	in	k?
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
<g/>
A.	A.	kA
:	:	kIx,
A	A	kA
documentary	documentar	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Crowell	Crowell	k1gInSc1
<g/>
,	,	kIx,
c	c	k0
<g/>
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
690011652	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MARCUS	MARCUS	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Making	Making	k1gInSc1
gay	gay	k1gMnSc1
history	histor	k1gInPc7
<g/>
:	:	kIx,
the	the	k?
half-century	half-centura	k1gFnSc2
fight	fighta	k1gFnPc2
for	forum	k1gNnPc2
lesbian	lesbian	k1gMnSc1
and	and	k?
gay	gay	k1gMnSc1
equal	equat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
rights	rights	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Perennial	Perennial	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
60933917	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stonewallské	Stonewallský	k2eAgInPc1d1
nepokoje	nepokoj	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
92005716	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
92005716	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
