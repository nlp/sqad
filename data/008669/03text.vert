<p>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
house	house	k1gNnSc4	house
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
forma	forma	k1gFnSc1	forma
house	house	k1gNnSc1	house
music	musice	k1gFnPc2	musice
<g/>
.	.	kIx.	.
</s>
<s>
Vlastně	vlastně	k9	vlastně
to	ten	k3xDgNnSc1	ten
ani	ani	k9	ani
není	být	k5eNaImIp3nS	být
označení	označení	k1gNnSc1	označení
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
spíše	spíše	k9	spíše
termín	termín	k1gInSc1	termín
zastřešující	zastřešující	k2eAgFnSc2d1	zastřešující
nahrávky	nahrávka	k1gFnSc2	nahrávka
house	house	k1gNnSc1	house
music	musice	k1gFnPc2	musice
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
v	v	k7c4	v
první	první	k4xOgNnSc4	první
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
nahrávek	nahrávka	k1gFnPc2	nahrávka
spadají	spadat	k5eAaImIp3nP	spadat
spíše	spíše	k9	spíše
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
stylu	styl	k1gInSc2	styl
Acid	Acid	k1gInSc4	Acid
house	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
house	house	k1gNnSc1	house
music	musice	k1gFnPc2	musice
byl	být	k5eAaImAgInS	být
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Chicagu	Chicago	k1gNnSc6	Chicago
v	v	k7c6	v
nočním	noční	k2eAgInSc6d1	noční
klubu	klub	k1gInSc6	klub
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Warehouse	Warehouse	k1gFnSc2	Warehouse
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
house	house	k1gNnSc1	house
music	musice	k1gFnPc2	musice
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
zrovna	zrovna	k6eAd1	zrovna
"	"	kIx"	"
<g/>
house	house	k1gNnSc1	house
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgNnSc4d3	nejpopulárnější
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
teorie	teorie	k1gFnSc1	teorie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nahrávky	nahrávka	k1gFnPc1	nahrávka
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
domácích	domácí	k2eAgFnPc6d1	domácí
studiích	studie	k1gFnPc6	studie
hudebníku	hudebník	k1gMnSc5	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
undergroundu	underground	k1gInSc6	underground
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc4	žánr
zpopularizovali	zpopularizovat	k5eAaPmAgMnP	zpopularizovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
například	například	k6eAd1	například
americký	americký	k2eAgMnSc1d1	americký
post-disco	postisco	k1gMnSc1	post-disco
DJ	DJ	kA	DJ
Frankie	Frankie	k1gFnSc1	Frankie
Knuckles	Knuckles	k1gInSc1	Knuckles
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
