<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaBmAgInS	napsat
sbírku	sbírka	k1gFnSc4	sbírka
veršů	verš	k1gInPc2	verš
na	na	k7c4	na
Miróova	Miróův	k2eAgNnPc4d1	Miróův
díla	dílo	k1gNnPc4	dílo
<g/>
?	?	kIx.	?
</s>
