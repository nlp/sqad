<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Tuhý	tuhý	k2eAgMnSc1d1	tuhý
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
Město	město	k1gNnSc4	město
Albrechtice	Albrechtice	k1gFnPc1	Albrechtice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
policista	policista	k1gMnSc1	policista
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
až	až	k9	až
2018	[number]	k4	2018
prezident	prezident	k1gMnSc1	prezident
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
ČR	ČR	kA	ČR
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
Bc.	Bc.	k1gFnSc2	Bc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
polsky	polsky	k6eAd1	polsky
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
prověrky	prověrka	k1gFnSc2	prověrka
na	na	k7c4	na
stupeň	stupeň	k1gInSc4	stupeň
utajení	utajení	k1gNnSc2	utajení
"	"	kIx"	"
<g/>
Tajné	tajný	k2eAgFnPc1d1	tajná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Tuhý	tuhý	k2eAgMnSc1d1	tuhý
je	být	k5eAaImIp3nS	být
rozvedený	rozvedený	k2eAgInSc1d1	rozvedený
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Policejní	policejní	k2eAgFnSc1d1	policejní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
jako	jako	k9	jako
řadový	řadový	k2eAgMnSc1d1	řadový
policista	policista	k1gMnSc1	policista
na	na	k7c6	na
Obvodním	obvodní	k2eAgNnSc6d1	obvodní
oddělení	oddělení	k1gNnSc6	oddělení
ve	v	k7c6	v
Městě	město	k1gNnSc6	město
Albrechtice	Albrechtice	k1gFnPc4	Albrechtice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
později	pozdě	k6eAd2	pozdě
také	také	k9	také
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zástupcem	zástupce	k1gMnSc7	zástupce
ředitele	ředitel	k1gMnSc2	ředitel
Okresního	okresní	k2eAgNnSc2d1	okresní
ředitelství	ředitelství	k1gNnSc2	ředitelství
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
Okresního	okresní	k2eAgNnSc2d1	okresní
ředitelství	ředitelství	k1gNnSc2	ředitelství
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
Bruntál	Bruntál	k1gInSc1	Bruntál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
jeho	jeho	k3xOp3gMnPc2	jeho
kariéra	kariéra	k1gFnSc1	kariéra
akcelerovala	akcelerovat	k5eAaImAgFnS	akcelerovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
náměstkem	náměstek	k1gMnSc7	náměstek
ředitele	ředitel	k1gMnSc2	ředitel
Správy	správa	k1gFnSc2	správa
Severomoravského	severomoravský	k2eAgInSc2d1	severomoravský
kraje	kraj	k1gInSc2	kraj
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
náměstkem	náměstek	k1gMnSc7	náměstek
ředitele	ředitel	k1gMnSc2	ředitel
Krajského	krajský	k2eAgNnSc2d1	krajské
ředitelství	ředitelství	k1gNnSc2	ředitelství
policie	policie	k1gFnSc2	policie
Severomoravského	severomoravský	k2eAgInSc2d1	severomoravský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
Krajského	krajský	k2eAgNnSc2d1	krajské
ředitelství	ředitelství	k1gNnSc2	ředitelství
policie	policie	k1gFnSc2	policie
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
náměstkem	náměstek	k1gMnSc7	náměstek
policejního	policejní	k2eAgMnSc2d1	policejní
prezidenta	prezident	k1gMnSc2	prezident
pro	pro	k7c4	pro
vnější	vnější	k2eAgFnSc4d1	vnější
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
náměstkem	náměstek	k1gMnSc7	náměstek
policejního	policejní	k2eAgMnSc2d1	policejní
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Petra	Petr	k1gMnSc2	Petr
Lessyho	Lessy	k1gMnSc2	Lessy
dočasně	dočasně	k6eAd1	dočasně
pověřen	pověřit	k5eAaPmNgInS	pověřit
vedením	vedení	k1gNnSc7	vedení
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
na	na	k7c4	na
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
místo	místo	k1gNnSc4	místo
policejního	policejní	k2eAgMnSc2d1	policejní
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
sedmičlenná	sedmičlenný	k2eAgFnSc1d1	sedmičlenná
odborná	odborný	k2eAgFnSc1d1	odborná
komise	komise	k1gFnSc1	komise
jednomyslně	jednomyslně	k6eAd1	jednomyslně
doporučila	doporučit	k5eAaPmAgFnS	doporučit
ministrovi	ministr	k1gMnSc3	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Milanu	Milan	k1gMnSc3	Milan
Chovancovi	Chovanec	k1gMnSc3	Chovanec
jeho	jeho	k3xOp3gNnSc4	jeho
jmenování	jmenování	k1gNnSc4	jmenování
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
také	také	k6eAd1	také
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
učinil	učinit	k5eAaImAgMnS	učinit
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
jej	on	k3xPp3gInSc4	on
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
brigádním	brigádní	k2eAgMnSc7d1	brigádní
<g />
.	.	kIx.	.
</s>
<s>
generálem	generál	k1gMnSc7	generál
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
získal	získat	k5eAaPmAgMnS	získat
hodnost	hodnost	k1gFnSc4	hodnost
generálmajora	generálmajor	k1gMnSc2	generálmajor
a	a	k8xC	a
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
Zemanem	Zeman	k1gMnSc7	Zeman
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
generálporučíka	generálporučík	k1gMnSc2	generálporučík
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
politické	politický	k2eAgInPc1d1	politický
tlaky	tlak	k1gInPc1	tlak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
ČR	ČR	kA	ČR
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
