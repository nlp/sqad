<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
vítá	vítat	k5eAaImIp3nS	vítat
ohleduplné	ohleduplný	k2eAgMnPc4d1	ohleduplný
řidiče	řidič	k1gMnPc4	řidič
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Red	Red	k1gMnSc1	Red
Dwarf	Dwarf	k1gMnSc1	Dwarf
<g/>
:	:	kIx,	:
Infinity	Infinit	k1gInPc1	Infinit
Welcomes	Welcomesa	k1gFnPc2	Welcomesa
Careful	Carefula	k1gFnPc2	Carefula
Drivers	Drivers	k1gInSc1	Drivers
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
humoristických	humoristický	k2eAgInPc2d1	humoristický
vědeckofantastických	vědeckofantastický	k2eAgInPc2d1	vědeckofantastický
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
napsali	napsat	k5eAaBmAgMnP	napsat
Rob	roba	k1gFnPc2	roba
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
Doug	Doug	k1gMnSc1	Doug
Naylor	Naylor	k1gMnSc1	Naylor
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
originále	originál	k1gInSc6	originál
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
sériích	série	k1gFnPc6	série
televizního	televizní	k2eAgInSc2d1	televizní
sitcomu	sitcom	k1gInSc2	sitcom
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
přináší	přinášet	k5eAaImIp3nS	přinášet
čtenáři	čtenář	k1gMnSc3	čtenář
především	především	k6eAd1	především
hlubší	hluboký	k2eAgInSc4d2	hlubší
náhled	náhled	k1gInSc4	náhled
do	do	k7c2	do
charakterů	charakter	k1gInPc2	charakter
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
–	–	k?	–
Listera	Lister	k1gMnSc2	Lister
a	a	k8xC	a
Rimmera	Rimmer	k1gMnSc2	Rimmer
–	–	k?	–
a	a	k8xC	a
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
také	také	k9	také
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
lidstvu	lidstvo	k1gNnSc6	lidstvo
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
i	i	k9	i
několik	několik	k4yIc1	několik
příhod	příhoda	k1gFnPc2	příhoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
nebyly	být	k5eNaImAgFnP	být
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
okolnosti	okolnost	k1gFnSc2	okolnost
Listerova	Listerův	k2eAgInSc2d1	Listerův
příchodu	příchod	k1gInSc2	příchod
k	k	k7c3	k
těžební	těžební	k2eAgFnSc3d1	těžební
společnosti	společnost	k1gFnSc3	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběhová	příběhový	k2eAgFnSc1d1	příběhová
linie	linie	k1gFnSc1	linie
ubíhá	ubíhat	k5eAaImIp3nS	ubíhat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
epizod	epizoda	k1gFnPc2	epizoda
Konec	konec	k1gInSc1	konec
<g/>
,	,	kIx,	,
Ozvěny	ozvěna	k1gFnPc1	ozvěna
budoucnosti	budoucnost	k1gFnSc3	budoucnost
<g/>
,	,	kIx,	,
Kryton	Kryton	k1gInSc4	Kryton
<g/>
,	,	kIx,	,
Lepší	lepší	k1gNnSc4	lepší
než	než	k8xS	než
život	život	k1gInSc4	život
a	a	k8xC	a
Já	já	k3xPp1nSc1	já
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
–	–	k?	–
oproti	oproti	k7c3	oproti
epizodickému	epizodický	k2eAgInSc3d1	epizodický
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
ucelenému	ucelený	k2eAgInSc3d1	ucelený
toku	tok	k1gInSc3	tok
děje	děj	k1gInSc2	děj
seriálu	seriál	k1gInSc2	seriál
podává	podávat	k5eAaImIp3nS	podávat
kniha	kniha	k1gFnSc1	kniha
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
překlad	překlad	k1gInSc1	překlad
Ladislava	Ladislav	k1gMnSc2	Ladislav
Šenkyříka	Šenkyřík	k1gMnSc2	Šenkyřík
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Argo	Argo	k6eAd1	Argo
pod	pod	k7c7	pod
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
411	[number]	k4	411
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
první	první	k4xOgFnSc1	první
<g/>
:	:	kIx,	:
Vaše	váš	k3xOp2gFnSc1	váš
vlastní	vlastní	k2eAgFnSc1d1	vlastní
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
Část	část	k1gFnSc1	část
druhá	druhý	k4xOgFnSc1	druhý
<g/>
:	:	kIx,	:
Sám	sám	k3xTgMnSc1	sám
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
bez	bez	k7c2	bez
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
instantních	instantní	k2eAgFnPc2d1	instantní
polévek	polévka	k1gFnPc2	polévka
s	s	k7c7	s
návodem	návod	k1gInSc7	návod
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
snadné	snadný	k2eAgNnSc1d1	snadné
/	/	kIx~	/
aké	aké	k?	aké
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
Část	část	k1gFnSc1	část
třetí	třetí	k4xOgFnSc1	třetí
<g/>
:	:	kIx,	:
Země	země	k1gFnSc1	země
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Archimédés	Archimédésa	k1gFnPc2	Archimédésa
-	-	kIx~	-
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
<g />
.	.	kIx.	.
</s>
<s>
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
–	–	k?	–
první	první	k4xOgNnSc4	první
technik	technikum	k1gNnPc2	technikum
na	na	k7c6	na
Červeném	červený	k2eAgMnSc6d1	červený
trpaslíkovi	trpaslík	k1gMnSc6	trpaslík
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
nadřízený	nadřízený	k1gMnSc1	nadřízený
Listera	Lister	k1gMnSc2	Lister
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
hologram	hologram	k1gInSc1	hologram
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
2	[number]	k4	2
–	–	k?	–
zkopírovaná	zkopírovaný	k2eAgFnSc1d1	zkopírovaná
verze	verze	k1gFnSc1	verze
Rimmera	Rimmera	k1gFnSc1	Rimmera
do	do	k7c2	do
hologramové	hologramový	k2eAgFnSc2d1	hologramová
simulační	simulační	k2eAgFnSc2d1	simulační
jednotky	jednotka	k1gFnSc2	jednotka
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Nova	novum	k1gNnSc2	novum
5	[number]	k4	5
Bexley	Bexlea	k1gFnSc2	Bexlea
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
syn	syn	k1gMnSc1	syn
Listera	Lister	k1gMnSc2	Lister
Bůh	bůh	k1gMnSc1	bůh
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Burd	Burd	k1gMnSc1	Burd
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
<g />
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
Caldicott	Caldicott	k1gMnSc1	Caldicott
–	–	k?	–
náborový	náborový	k2eAgMnSc1d1	náborový
úředník	úředník	k1gMnSc1	úředník
Jupiterské	jupiterský	k2eAgFnSc2d1	Jupiterská
důlní	důlní	k2eAgFnSc2d1	důlní
společnosti	společnost	k1gFnSc2	společnost
Carole	Carole	k1gFnSc2	Carole
–	–	k?	–
manželka	manželka	k1gFnSc1	manželka
Saunderse	Saunderse	k1gFnSc1	Saunderse
Dave	Dav	k1gInSc5	Dav
Lister	Lister	k1gMnSc1	Lister
–	–	k?	–
třetí	třetí	k4xOgNnSc4	třetí
technik	technikum	k1gNnPc2	technikum
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
těžební	těžební	k2eAgFnSc2d1	těžební
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jediný	jediný	k2eAgMnSc1d1	jediný
žijící	žijící	k2eAgMnSc1d1	žijící
člověk	člověk	k1gMnSc1	člověk
Denis	Denisa	k1gFnPc2	Denisa
–	–	k?	–
narkoman	narkoman	k1gMnSc1	narkoman
na	na	k7c6	na
Mimasu	Mimas	k1gInSc6	Mimas
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
droze	droga	k1gFnSc6	droga
Blaženost	blaženost	k1gFnSc1	blaženost
Dooley	Doolea	k1gFnSc2	Doolea
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
Z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
Elaine	elain	k1gInSc5	elain
Schumanová	Schumanový	k2eAgFnSc1d1	Schumanový
–	–	k?	–
letová	letový	k2eAgFnSc1d1	letová
koordinátorka	koordinátorka	k1gFnSc1	koordinátorka
z	z	k7c2	z
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
Ernie	Ernie	k1gFnSc1	Ernie
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
taxikář	taxikář	k1gMnSc1	taxikář
v	v	k7c4	v
Bedford	Bedford	k1gInSc4	Bedford
Falls	Fallsa	k1gFnPc2	Fallsa
Frank	Frank	k1gMnSc1	Frank
Rimmer	Rimmer	k1gMnSc1	Rimmer
–	–	k?	–
bratr	bratr	k1gMnSc1	bratr
Arnolda	Arnold	k1gMnSc2	Arnold
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
důstojník	důstojník	k1gMnSc1	důstojník
Vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
sborů	sbor	k1gInPc2	sbor
Frank	Frank	k1gMnSc1	Frank
Saunders	Saundersa	k1gFnPc2	Saundersa
–	–	k?	–
inženýr	inženýr	k1gMnSc1	inženýr
na	na	k7c6	na
Červeném	červený	k2eAgMnSc6d1	červený
trpaslíku	trpaslík	k1gMnSc6	trpaslík
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
(	(	kIx(	(
<g/>
zabije	zabít	k5eAaPmIp3nS	zabít
ho	on	k3xPp3gMnSc4	on
demoliční	demoliční	k2eAgFnSc1d1	demoliční
koule	koule	k1gFnSc2	koule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
chvíli	chvíle	k1gFnSc4	chvíle
hologram	hologram	k1gInSc4	hologram
George	Georg	k1gFnSc2	Georg
McIntyre	McIntyr	k1gInSc5	McIntyr
–	–	k?	–
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
letecký	letecký	k2eAgMnSc1d1	letecký
koordinátor	koordinátor	k1gMnSc1	koordinátor
na	na	k7c6	na
Červeném	červený	k2eAgMnSc6d1	červený
trpaslíku	trpaslík	k1gMnSc6	trpaslík
<g/>
,	,	kIx,	,
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
zadlužil	zadlužit	k5eAaPmAgMnS	zadlužit
v	v	k7c6	v
barech	bar	k1gInPc6	bar
na	na	k7c4	na
Phoebe	Phoeb	k1gMnSc5	Phoeb
<g/>
,	,	kIx,	,
Dione	Dion	k1gMnSc5	Dion
a	a	k8xC	a
Rhea	Rheum	k1gNnPc1	Rheum
při	při	k7c6	při
hazardní	hazardní	k2eAgFnSc6d1	hazardní
hře	hra	k1gFnSc6	hra
Trubka	trubka	k1gFnSc1	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
chvíli	chvíle	k1gFnSc4	chvíle
hologram	hologram	k1gInSc1	hologram
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gInSc1	George
S.	S.	kA	S.
Patton	Patton	k1gInSc1	Patton
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Hendersonová	Hendersonová	k1gFnSc1	Hendersonová
–	–	k?	–
důstojnice	důstojnice	k1gFnSc1	důstojnice
Pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
hlídky	hlídka	k1gFnPc1	hlídka
na	na	k7c6	na
Mimasu	Mimas	k1gInSc6	Mimas
Holly	Holla	k1gFnSc2	Holla
–	–	k?	–
palubní	palubní	k2eAgInSc1d1	palubní
počítač	počítač	k1gInSc1	počítač
Červeného	Červený	k1gMnSc2	Červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
s	s	k7c7	s
IQ	iq	kA	iq
6	[number]	k4	6
000	[number]	k4	000
Horác	Horác	k1gMnSc1	Horác
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
bankéř	bankéř	k1gMnSc1	bankéř
v	v	k7c4	v
Bedford	Bedford	k1gInSc4	Bedford
Falls	Fallsa	k1gFnPc2	Fallsa
Howard	Howard	k1gMnSc1	Howard
Rimmer	Rimmer	k1gMnSc1	Rimmer
–	–	k?	–
bratr	bratr	k1gMnSc1	bratr
Arnolda	Arnold	k1gMnSc2	Arnold
<g/>
,	,	kIx,	,
zkušební	zkušební	k2eAgMnSc1d1	zkušební
pilot	pilot	k1gMnSc1	pilot
Hugo	Hugo	k1gMnSc1	Hugo
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
správce	správce	k1gMnSc1	správce
Rimmerova	Rimmerův	k2eAgInSc2d1	Rimmerův
bazénu	bazén	k1gInSc2	bazén
Jim	on	k3xPp3gMnPc3	on
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
syn	syn	k1gMnSc1	syn
Listera	Lister	k1gMnSc2	Lister
Joachim	Joachim	k1gMnSc1	Joachim
Popol	Popola	k1gFnPc2	Popola
–	–	k?	–
ruský	ruský	k2eAgMnSc1d1	ruský
kantilénový	kantilénový	k2eAgMnSc1d1	kantilénový
zpěvák	zpěvák	k1gMnSc1	zpěvák
Johny	John	k1gMnPc4	John
Rimmer	Rimmra	k1gFnPc2	Rimmra
–	–	k?	–
bratr	bratr	k1gMnSc1	bratr
Arnolda	Arnold	k1gMnSc2	Arnold
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
Vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
sborů	sbor	k1gInPc2	sbor
Josie	Josie	k1gFnSc2	Josie
–	–	k?	–
milenka	milenka	k1gFnSc1	milenka
Denise	Denisa	k1gFnSc6	Denisa
<g/>
,	,	kIx,	,
fixovaná	fixovaný	k2eAgFnSc1d1	fixovaná
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
život	život	k1gInSc1	život
Juanita	Juanita	k1gFnSc1	Juanita
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
brazilská	brazilský	k2eAgFnSc1d1	brazilská
topmodelka	topmodelka	k1gFnSc1	topmodelka
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Rimmera	Rimmer	k1gMnSc2	Rimmer
Kapitánka	kapitánek	k1gMnSc2	kapitánek
–	–	k?	–
kapitánka	kapitánek	k1gMnSc2	kapitánek
Červeného	Červený	k1gMnSc2	Červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
malá	malý	k2eAgFnSc1d1	malá
zavalitá	zavalitý	k2eAgFnSc1d1	zavalitá
Američanka	Američanka	k1gFnSc1	Američanka
s	s	k7c7	s
rodným	rodný	k2eAgNnSc7d1	rodné
příjmením	příjmení	k1gNnSc7	příjmení
Tchahounová	Tchahounový	k2eAgFnSc1d1	Tchahounový
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Kirsty	Kirsta	k1gMnSc2	Kirsta
Fantoziová	Fantoziový	k2eAgFnSc1d1	Fantoziový
–	–	k?	–
inženýrka	inženýrka	k1gFnSc1	inženýrka
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
demolicí	demolice	k1gFnPc2	demolice
z	z	k7c2	z
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
Krissie	Krissie	k1gFnSc1	Krissie
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
manželka	manželka	k1gFnSc1	manželka
Listera	Lister	k1gMnSc2	Lister
v	v	k7c4	v
Bedford	Bedford	k1gInSc4	Bedford
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
,	,	kIx,	,
dávný	dávný	k2eAgMnSc1d1	dávný
potomek	potomek	k1gMnSc1	potomek
Kristiny	Kristina	k1gFnSc2	Kristina
Kochanské	Kochanský	k2eAgFnSc2d1	Kochanská
Kristina	Kristina	k1gFnSc1	Kristina
Kochanská	Kochanský	k2eAgFnSc1d1	Kochanská
–	–	k?	–
důstojnice	důstojnice	k1gFnSc1	důstojnice
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
krátký	krátký	k2eAgInSc4d1	krátký
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Listerem	Lister	k1gInSc7	Lister
Kryton	Kryton	k1gInSc4	Kryton
–	–	k?	–
služební	služební	k2eAgMnSc1d1	služební
robot	robot	k1gMnSc1	robot
z	z	k7c2	z
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
McHullock	McHullock	k1gMnSc1	McHullock
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
Z	Z	kA	Z
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Norman	Norman	k1gMnSc1	Norman
Wisdom	Wisdom	k1gInSc1	Wisdom
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Palmer	Palmer	k1gMnSc1	Palmer
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
Z	Z	kA	Z
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmra	k1gFnPc2	Rimmra
Petersen	Petersen	k1gInSc4	Petersen
–	–	k?	–
naverbovaný	naverbovaný	k2eAgMnSc1d1	naverbovaný
technik	technik	k1gMnSc1	technik
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
Listera	Lister	k1gMnSc2	Lister
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Dán	dán	k2eAgMnSc1d1	dán
Petrovitch	Petrovitch	k1gMnSc1	Petrovitch
–	–	k?	–
první	první	k4xOgNnSc4	první
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
směny	směna	k1gFnSc2	směna
A	a	k9	a
na	na	k7c6	na
Červeném	červený	k2eAgMnSc6d1	červený
trpaslíkovi	trpaslík	k1gMnSc6	trpaslík
Pierre	Pierr	k1gInSc5	Pierr
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
absolvent	absolvent	k1gMnSc1	absolvent
prestižní	prestižní	k2eAgFnSc2d1	prestižní
VŠ	vš	k0	vš
<g/>
,	,	kIx,	,
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
u	u	k7c2	u
Rimmera	Rimmero	k1gNnSc2	Rimmero
jako	jako	k8xS	jako
obsluha	obsluha	k1gFnSc1	obsluha
výtahu	výtah	k1gInSc2	výtah
Pierre	Pierr	k1gInSc5	Pierr
Chomsky	Chomsky	k1gMnSc7	Chomsky
–	–	k?	–
naverbovaný	naverbovaný	k2eAgMnSc1d1	naverbovaný
technik	technik	k1gMnSc1	technik
na	na	k7c4	na
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g />
.	.	kIx.	.
</s>
<s>
Phil	Phil	k1gMnSc1	Phil
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
–	–	k?	–
naverbovaný	naverbovaný	k2eAgMnSc1d1	naverbovaný
technik	technik	k1gMnSc1	technik
na	na	k7c4	na
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
vysokoškolák	vysokoškolák	k1gMnSc1	vysokoškolák
Pixon	Pixon	k1gMnSc1	Pixon
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
Z	Z	kA	Z
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
Rogerson	Rogerson	k1gMnSc1	Rogerson
–	–	k?	–
člen	člen	k1gMnSc1	člen
posádky	posádka	k1gFnSc2	posádka
Červeného	Červený	k1gMnSc2	Červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
Saxon	Saxon	k1gInSc1	Saxon
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
Z	Z	kA	Z
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
Schmidt	Schmidt	k1gMnSc1	Schmidt
–	–	k?	–
naverbovaný	naverbovaný	k2eAgMnSc1d1	naverbovaný
technik	technik	k1gMnSc1	technik
na	na	k7c4	na
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g />
.	.	kIx.	.
</s>
<s>
Turner	turner	k1gMnSc1	turner
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
Z	Z	kA	Z
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
–	–	k?	–
v	v	k7c6	v
LNŽ	LNŽ	kA	LNŽ
Rimmerův	Rimmerův	k2eAgMnSc1d1	Rimmerův
host	host	k1gMnSc1	host
Weinerová	Weinerová	k1gFnSc1	Weinerová
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
–	–	k?	–
člen	člen	k1gMnSc1	člen
směny	směna	k1gFnSc2	směna
Z	Z	kA	Z
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
velí	velet	k5eAaImIp3nS	velet
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
Yvette	Yvett	k1gInSc5	Yvett
Richardsová	Richardsový	k2eAgFnSc1d1	Richardsová
–	–	k?	–
kapitánka	kapitánka	k1gFnSc1	kapitánka
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
Dave	Dav	k1gInSc5	Dav
Lister	Lister	k1gMnSc1	Lister
se	se	k3xPyFc4	se
po	po	k7c6	po
pořádném	pořádný	k2eAgInSc6d1	pořádný
flámu	flám	k1gInSc6	flám
probere	probrat	k5eAaPmIp3nS	probrat
na	na	k7c6	na
Mimasu	Mimas	k1gInSc6	Mimas
<g/>
,	,	kIx,	,
měsíci	měsíc	k1gInSc6	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Pít	pít	k5eAaImF	pít
začal	začít	k5eAaPmAgInS	začít
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
Mimas	Mimas	k1gInSc4	Mimas
si	se	k3xPyFc3	se
nevzpomíná	vzpomínat	k5eNaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
probudil	probudit	k5eAaPmAgMnS	probudit
se	se	k3xPyFc4	se
s	s	k7c7	s
růžovým	růžový	k2eAgInSc7d1	růžový
dámským	dámský	k2eAgInSc7d1	dámský
kloboučkem	klobouček	k1gInSc7	klobouček
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
žlutými	žlutý	k2eAgFnPc7d1	žlutá
gumákami	gumáka	k1gFnPc7	gumáka
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
s	s	k7c7	s
pasem	pas	k1gInSc7	pas
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
Emily	Emil	k1gMnPc4	Emil
Berkensteinová	Berkensteinový	k2eAgFnSc1d1	Berkensteinový
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
vydělat	vydělat	k5eAaPmF	vydělat
si	se	k3xPyFc3	se
800	[number]	k4	800
librodolarů	librodolar	k1gInPc2	librodolar
na	na	k7c4	na
lístek	lístek	k1gInSc4	lístek
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedaří	dařit	k5eNaImIp3nS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ušetřit	ušetřit	k5eAaPmF	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Mimas	Mimas	k1gInSc1	Mimas
jej	on	k3xPp3gMnSc4	on
deprimuje	deprimovat	k5eAaBmIp3nS	deprimovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
opíjí	opíjet	k5eAaImIp3nS	opíjet
<g/>
.	.	kIx.	.
</s>
<s>
Přespává	přespávat	k5eAaImIp3nS	přespávat
v	v	k7c6	v
pronajaté	pronajatý	k2eAgFnSc6d1	pronajatá
skříňce	skříňka	k1gFnSc6	skříňka
na	na	k7c4	na
zavazadla	zavazadlo	k1gNnPc4	zavazadlo
na	na	k7c6	na
mimaském	mimaský	k2eAgNnSc6d1	mimaský
nádraží	nádraží	k1gNnSc6	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc4	peníz
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
krádeží	krádež	k1gFnSc7	krádež
hopíků	hopík	k1gMnPc2	hopík
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
taxikářskou	taxikářský	k2eAgFnSc7d1	taxikářská
činností	činnost	k1gFnSc7	činnost
načerno	načerno	k6eAd1	načerno
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
naverbovat	naverbovat	k5eAaPmF	naverbovat
na	na	k7c4	na
těžební	těžební	k2eAgFnSc4d1	těžební
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
plán	plán	k1gInSc1	plán
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
zadarmo	zadarmo	k6eAd1	zadarmo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Netuší	tušit	k5eNaImIp3nS	tušit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
loď	loď	k1gFnSc1	loď
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
Neptunův	Neptunův	k2eAgInSc4d1	Neptunův
měsíc	měsíc	k1gInSc4	měsíc
Triton	triton	k1gMnSc1	triton
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
protáhne	protáhnout	k5eAaPmIp3nS	protáhnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
to	ten	k3xDgNnSc1	ten
až	až	k9	až
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
prozradí	prozradit	k5eAaPmIp3nS	prozradit
Petersen	Petersen	k2eAgMnSc1d1	Petersen
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgMnSc1d1	dánský
alkoholik	alkoholik	k1gMnSc1	alkoholik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
také	také	k9	také
opět	opět	k6eAd1	opět
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Arnoldem	Arnold	k1gMnSc7	Arnold
Rimmerem	Rimmer	k1gMnSc7	Rimmer
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
tu	tu	k6eAd1	tu
čest	čest	k1gFnSc4	čest
již	již	k9	již
na	na	k7c4	na
Mimasu	Mimasa	k1gFnSc4	Mimasa
<g/>
.	.	kIx.	.
</s>
<s>
Rimmer	Rimmer	k1gMnSc1	Rimmer
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zakomplexovaný	zakomplexovaný	k2eAgMnSc1d1	zakomplexovaný
jedinec	jedinec	k1gMnSc1	jedinec
s	s	k7c7	s
utkvělou	utkvělý	k2eAgFnSc7d1	utkvělá
představou	představa	k1gFnSc7	představa
udělat	udělat	k5eAaPmF	udělat
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
kariéru	kariéra	k1gFnSc4	kariéra
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc1	jeho
tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gMnSc3	on
rodiči	rodič	k1gMnPc7	rodič
dáváni	dáván	k2eAgMnPc1d1	dáván
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Rimmer	Rimmer	k1gInSc4	Rimmer
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
propadne	propadnout	k5eAaPmIp3nS	propadnout
u	u	k7c2	u
astronavigačních	astronavigační	k2eAgFnPc2d1	astronavigační
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Dopracoval	dopracovat	k5eAaPmAgMnS	dopracovat
to	ten	k3xDgNnSc4	ten
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
třetího	třetí	k4xOgInSc2	třetí
na	na	k7c4	na
prvního	první	k4xOgMnSc4	první
technika	technik	k1gMnSc4	technik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
povel	povel	k1gInSc4	povel
směnu	směna	k1gFnSc4	směna
Z	z	k7c2	z
<g/>
,	,	kIx,	,
partu	part	k1gInSc6	part
budižkničemů	budižkničem	k1gMnPc2	budižkničem
<g/>
,	,	kIx,	,
opilců	opilec	k1gMnPc2	opilec
a	a	k8xC	a
pochybných	pochybný	k2eAgNnPc2d1	pochybné
individuí	individuum	k1gNnPc2	individuum
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
členem	člen	k1gInSc7	člen
je	být	k5eAaImIp3nS	být
i	i	k9	i
Lister	Lister	k1gInSc1	Lister
a	a	k8xC	a
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
starat	starat	k5eAaImF	starat
o	o	k7c4	o
údržbu	údržba	k1gFnSc4	údržba
jídelních	jídelní	k2eAgInPc2d1	jídelní
automatů	automat	k1gInPc2	automat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
podřízených	podřízený	k1gMnPc2	podřízený
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
absolutně	absolutně	k6eAd1	absolutně
žádnou	žádný	k3yNgFnSc4	žádný
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Lister	Lister	k1gMnSc1	Lister
se	se	k3xPyFc4	se
bláznivě	bláznivě	k6eAd1	bláznivě
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
řídící	řídící	k2eAgFnSc2d1	řídící
důstojnice	důstojnice	k1gFnSc2	důstojnice
Kristiny	Kristina	k1gFnSc2	Kristina
Kochanské	Kochanský	k2eAgFnSc2d1	Kochanská
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
navždy	navždy	k6eAd1	navždy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kochanská	Kochanský	k2eAgFnSc1d1	Kochanská
ho	on	k3xPp3gMnSc4	on
bere	brát	k5eAaImIp3nS	brát
jen	jen	k9	jen
jako	jako	k9	jako
náplast	náplast	k1gFnSc1	náplast
na	na	k7c4	na
zklamání	zklamání	k1gNnSc4	zklamání
z	z	k7c2	z
rozchodu	rozchod	k1gInSc2	rozchod
s	s	k7c7	s
leteckým	letecký	k2eAgMnSc7d1	letecký
důstojníkem	důstojník	k1gMnSc7	důstojník
a	a	k8xC	a
po	po	k7c6	po
3	[number]	k4	3
<g/>
1⁄	1⁄	k?	1⁄
týdnech	týden	k1gInPc6	týden
randění	randění	k1gNnSc1	randění
ho	on	k3xPp3gMnSc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
<g/>
.	.	kIx.	.
</s>
<s>
Listerovi	Lister	k1gMnSc3	Lister
se	se	k3xPyFc4	se
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Mirandě	Miranda	k1gFnSc6	Miranda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
třítýdenní	třítýdenní	k2eAgFnSc4d1	třítýdenní
zastávku	zastávka	k1gFnSc4	zastávka
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
pořídí	pořídit	k5eAaPmIp3nS	pořídit
kočku	kočka	k1gFnSc4	kočka
a	a	k8xC	a
pojmenuje	pojmenovat	k5eAaPmIp3nS	pojmenovat
ji	on	k3xPp3gFnSc4	on
Frankenstein	Frankenstein	k2eAgInSc1d1	Frankenstein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lodních	lodní	k2eAgInPc6d1	lodní
předpisech	předpis	k1gInPc6	předpis
jsou	být	k5eAaImIp3nP	být
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
neprojdou	projít	k5eNaPmIp3nP	projít
karanténou	karanténa	k1gFnSc7	karanténa
<g/>
,	,	kIx,	,
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Lister	Lister	k1gInSc1	Lister
kalkuluje	kalkulovat	k5eAaImIp3nS	kalkulovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
přistižen	přistihnout	k5eAaPmNgInS	přistihnout
a	a	k8xC	a
potrestán	potrestat	k5eAaPmNgInS	potrestat
pobytem	pobyt	k1gInSc7	pobyt
ve	v	k7c6	v
stázové	stázový	k2eAgFnSc6d1	stázový
komoře	komora	k1gFnSc6	komora
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zastaví	zastavit	k5eAaPmIp3nS	zastavit
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebude	být	k5eNaImBp3nS	být
se	se	k3xPyFc4	se
tak	tak	k9	tak
muset	muset	k5eAaImF	muset
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
cesty	cesta	k1gFnSc2	cesta
užírat	užírat	k5eAaImF	užírat
steskem	stesk	k1gInSc7	stesk
po	po	k7c4	po
Kochanské	Kochanský	k2eAgNnSc4d1	Kochanské
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
plán	plán	k1gInSc1	plán
tentokrát	tentokrát	k6eAd1	tentokrát
vyjde	vyjít	k5eAaPmIp3nS	vyjít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
stázové	stázový	k2eAgFnSc2d1	stázový
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
zemřou	zemřít	k5eAaPmIp3nP	zemřít
při	při	k7c6	při
poruše	porucha	k1gFnSc6	porucha
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
radiace	radiace	k1gFnSc1	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Holly	Holla	k1gFnPc4	Holla
otevře	otevřít	k5eAaPmIp3nS	otevřít
stázovou	stázový	k2eAgFnSc4d1	stázový
komoru	komora	k1gFnSc4	komora
s	s	k7c7	s
Listerem	Lister	k1gInSc7	Lister
až	až	k9	až
po	po	k7c4	po
3	[number]	k4	3
000	[number]	k4	000
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
radiace	radiace	k1gFnSc1	radiace
klesne	klesnout	k5eAaPmIp3nS	klesnout
na	na	k7c4	na
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidstvo	lidstvo	k1gNnSc1	lidstvo
vyhynulo	vyhynout	k5eAaPmAgNnS	vyhynout
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Lister	Lister	k1gInSc4	Lister
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
poslední	poslední	k2eAgMnSc1d1	poslední
žijící	žijící	k2eAgMnSc1d1	žijící
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lister	Lister	k1gInSc1	Lister
<g/>
:	:	kIx,	:
Takže	takže	k9	takže
jsem	být	k5eAaImIp1nS	být
poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zůstal	zůstat	k5eAaPmAgMnS	zůstat
naživu	naživu	k6eAd1	naživu
<g/>
?	?	kIx.	?
</s>
<s>
Holly	Holla	k1gFnPc1	Holla
<g/>
:	:	kIx,	:
Jo	jo	k9	jo
<g/>
.	.	kIx.	.
</s>
<s>
Mysleli	myslet	k5eAaImAgMnP	myslet
jste	být	k5eAaImIp2nP	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
vašemu	váš	k3xOp2gInSc3	váš
živočišnýmu	živočišnýmu	k?	živočišnýmu
druhu	druh	k1gMnSc6	druh
nemůže	moct	k5eNaImIp3nS	moct
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
jen	jen	k6eAd1	jen
někomu	někdo	k3yInSc3	někdo
jinýmu	jinýmu	k?	jinýmu
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
?	?	kIx.	?
</s>
<s>
Listera	Lister	k1gMnSc4	Lister
šokuje	šokovat	k5eAaBmIp3nS	šokovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Holly	Holl	k1gMnPc4	Holl
vzkřísil	vzkřísit	k5eAaPmAgMnS	vzkřísit
jako	jako	k8xS	jako
hologram	hologram	k1gInSc4	hologram
Rimmera	Rimmero	k1gNnSc2	Rimmero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
Kocoura	kocour	k1gMnSc4	kocour
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
felis	felis	k1gFnSc2	felis
sapiens	sapiensa	k1gFnPc2	sapiensa
<g/>
,	,	kIx,	,
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
Listerovy	Listerův	k2eAgFnSc2d1	Listerova
kočky	kočka	k1gFnSc2	kočka
Frankenstein	Frankensteina	k1gFnPc2	Frankensteina
<g/>
.	.	kIx.	.
</s>
<s>
Kocour	kocour	k1gInSc1	kocour
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
sebestředný	sebestředný	k2eAgInSc1d1	sebestředný
<g/>
.	.	kIx.	.
</s>
<s>
Lister	Lister	k1gInSc1	Lister
nařídí	nařídit	k5eAaPmIp3nS	nařídit
Hollymu	Hollym	k1gInSc2	Hollym
nastavit	nastavit	k5eAaPmF	nastavit
kurs	kurs	k1gInSc1	kurs
lodi	loď	k1gFnSc2	loď
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
stále	stále	k6eAd1	stále
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
až	až	k8xS	až
prolomí	prolomit	k5eAaPmIp3nS	prolomit
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
poté	poté	k6eAd1	poté
cestuje	cestovat	k5eAaImIp3nS	cestovat
superluminální	superluminální	k2eAgFnSc7d1	superluminální
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podivným	podivný	k2eAgFnPc3d1	podivná
událostem	událost	k1gFnPc3	událost
<g/>
,	,	kIx,	,
zúčastnění	zúčastnění	k1gNnSc4	zúčastnění
vidí	vidět	k5eAaImIp3nS	vidět
tzv.	tzv.	kA	tzv.
echa	echo	k1gNnSc2	echo
budoucnosti	budoucnost	k1gFnSc3	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Rimmer	Rimmer	k1gMnSc1	Rimmer
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
viděl	vidět	k5eAaImAgInS	vidět
Listera	Lister	k1gMnSc4	Lister
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
těžko	těžko	k6eAd1	těžko
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nebude	být	k5eNaImBp3nS	být
Lister	Lister	k1gInSc1	Lister
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Baxleyho	Baxley	k1gMnSc2	Baxley
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
lodi	loď	k1gFnSc2	loď
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
měla	mít	k5eAaImAgFnS	mít
odpálit	odpálit	k5eAaPmF	odpálit
nebulární	nebulární	k2eAgFnSc4d1	nebulární
střelu	střela	k1gFnSc4	střela
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
pomoci	pomoct	k5eAaPmF	pomoct
ji	on	k3xPp3gFnSc4	on
tak	tak	k6eAd1	tak
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
supernovy	supernova	k1gFnSc2	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
128	[number]	k4	128
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
všechny	všechen	k3xTgFnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
odpálit	odpálit	k5eAaPmF	odpálit
své	svůj	k3xOyFgFnPc4	svůj
střely	střela	k1gFnPc4	střela
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
světlo	světlo	k1gNnSc4	světlo
z	z	k7c2	z
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
supernov	supernova	k1gFnPc2	supernova
doletělo	doletět	k5eAaPmAgNnS	doletět
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
ve	v	k7c4	v
shodný	shodný	k2eAgInSc4d1	shodný
moment	moment	k1gInSc4	moment
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
reklamní	reklamní	k2eAgInSc4d1	reklamní
slogan	slogan	k1gInSc4	slogan
ŽIJTE	žít	k5eAaImRp2nP	žít
S	s	k7c7	s
COCA-COLOU	cocaola	k1gFnSc7	coca-cola
<g/>
!	!	kIx.	!
</s>
<s>
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
po	po	k7c6	po
odpalu	odpal	k1gInSc6	odpal
havaruje	havarovat	k5eAaPmIp3nS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Havárii	havárie	k1gFnSc4	havárie
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
služební	služební	k2eAgInSc1d1	služební
robot	robot	k1gInSc1	robot
Kryton	Kryton	k1gInSc1	Kryton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyčistil	vyčistit	k5eAaPmAgInS	vyčistit
mýdlovou	mýdlový	k2eAgFnSc7d1	mýdlová
vodou	voda	k1gFnSc7	voda
palubní	palubní	k2eAgInSc1d1	palubní
počítač	počítač	k1gInSc1	počítač
i	i	k9	i
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ženy	žena	k1gFnPc1	žena
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
tam	tam	k6eAd1	tam
nebyli	být	k5eNaImAgMnP	být
<g/>
)	)	kIx)	)
zahynou	zahynout	k5eAaPmIp3nP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc4	signál
SOS	sos	k1gInSc1	sos
zachytí	zachytit	k5eAaPmIp3nS	zachytit
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
letí	letět	k5eAaImIp3nS	letět
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
zde	zde	k6eAd1	zde
však	však	k9	však
nalezne	nalézt	k5eAaBmIp3nS	nalézt
pouze	pouze	k6eAd1	pouze
zmateného	zmatený	k2eAgMnSc4d1	zmatený
Krytona	Kryton	k1gMnSc4	Kryton
<g/>
.	.	kIx.	.
</s>
<s>
Rimmer	Rimmer	k1gInSc1	Rimmer
najde	najít	k5eAaPmIp3nS	najít
na	na	k7c6	na
Nově	nov	k1gInSc6	nov
5	[number]	k4	5
hologramovou	hologramový	k2eAgFnSc4d1	hologramová
simulační	simulační	k2eAgFnSc4d1	simulační
jednotku	jednotka	k1gFnSc4	jednotka
a	a	k8xC	a
napadne	napadnout	k5eAaPmIp3nS	napadnout
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
zkopírovat	zkopírovat	k5eAaPmF	zkopírovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Listera	Lister	k1gMnSc4	Lister
zase	zase	k9	zase
vzrušuje	vzrušovat	k5eAaImIp3nS	vzrušovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
zařízení	zařízení	k1gNnSc2	zařízení
umožňující	umožňující	k2eAgFnSc7d1	umožňující
dělat	dělat	k5eAaImF	dělat
tzv.	tzv.	kA	tzv.
dualitní	dualitní	k2eAgInPc4d1	dualitní
skoky	skok	k1gInPc4	skok
<g/>
,	,	kIx,	,
skoky	skok	k1gInPc4	skok
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Krytonových	Krytonový	k2eAgInPc2d1	Krytonový
odhadů	odhad	k1gInPc2	odhad
by	by	kYmCp3nS	by
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
mohl	moct	k5eAaImAgMnS	moct
trvat	trvat	k5eAaImF	trvat
dva	dva	k4xCgInPc4	dva
či	či	k8xC	či
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
zapotřebí	zapotřebí	k6eAd1	zapotřebí
palivo	palivo	k1gNnSc4	palivo
–	–	k?	–
233U	[number]	k4	233U
(	(	kIx(	(
<g/>
izotop	izotop	k1gInSc1	izotop
uranu	uran	k1gInSc2	uran
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lister	Lister	k1gInSc1	Lister
s	s	k7c7	s
Kocourem	kocour	k1gInSc7	kocour
a	a	k8xC	a
Krytonem	Kryton	k1gInSc7	Kryton
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
natěžit	natěžit	k5eAaPmF	natěžit
rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
oba	dva	k4xCgMnPc1	dva
Rimmerové	Rimmer	k1gMnPc1	Rimmer
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
zprovoznění	zprovoznění	k1gNnPc2	zprovoznění
lodi	loď	k1gFnSc2	loď
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
hladká	hladký	k2eAgFnSc1d1	hladká
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	se	k3xPyFc4	se
zvrtne	zvrtnout	k5eAaPmIp3nS	zvrtnout
v	v	k7c4	v
zášť	zášť	k1gFnSc4	zášť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oba	dva	k4xCgMnPc1	dva
spolu	spolu	k6eAd1	spolu
nesmyslně	smyslně	k6eNd1	smyslně
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
<g/>
.	.	kIx.	.
</s>
<s>
Palivo	palivo	k1gNnSc1	palivo
je	být	k5eAaImIp3nS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
a	a	k8xC	a
Lister	Lister	k1gInSc1	Lister
informuje	informovat	k5eAaBmIp3nS	informovat
oba	dva	k4xCgInPc4	dva
Rimmery	Rimmer	k1gInPc4	Rimmer
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
musí	muset	k5eAaImIp3nS	muset
vypnout	vypnout	k5eAaPmF	vypnout
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
neutáhne	utáhnout	k5eNaPmIp3nS	utáhnout
dva	dva	k4xCgInPc4	dva
hologramy	hologram	k1gInPc4	hologram
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
padne	padnout	k5eAaPmIp3nS	padnout
na	na	k7c4	na
původního	původní	k2eAgMnSc4d1	původní
Rimmera	Rimmer	k1gMnSc4	Rimmer
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
udělá	udělat	k5eAaPmIp3nS	udělat
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
vědět	vědět	k5eAaImF	vědět
proč	proč	k6eAd1	proč
Rimmera	Rimmera	k1gFnSc1	Rimmera
tak	tak	k6eAd1	tak
straší	strašit	k5eAaImIp3nS	strašit
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
polévce	polévka	k1gFnSc6	polévka
gazpacho	gazpacho	k6eAd1	gazpacho
<g/>
.	.	kIx.	.
</s>
<s>
Rimmer	Rimmer	k1gInSc1	Rimmer
mu	on	k3xPp3gMnSc3	on
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
předmětem	předmět	k1gInSc7	předmět
jeho	jeho	k3xOp3gFnSc2	jeho
faux	faux	k1gInSc1	faux
pas	pas	k1gInSc4	pas
na	na	k7c6	na
večírku	večírek	k1gInSc6	večírek
u	u	k7c2	u
kapitánky	kapitánka	k1gFnSc2	kapitánka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
ohřát	ohřát	k5eAaPmF	ohřát
(	(	kIx(	(
<g/>
španělská	španělský	k2eAgFnSc1d1	španělská
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
polévka	polévka	k1gFnSc1	polévka
gazpacho	gazpacho	k6eAd1	gazpacho
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
studená	studený	k2eAgFnSc1d1	studená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lister	Lister	k1gInSc1	Lister
mezitím	mezitím	k6eAd1	mezitím
vypl	vypl	k1gInSc1	vypl
druhého	druhý	k4xOgMnSc2	druhý
Rimmera	Rimmer	k1gMnSc2	Rimmer
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
Lister	Lister	k1gMnSc1	Lister
stane	stanout	k5eAaPmIp3nS	stanout
otcem	otec	k1gMnSc7	otec
dvou	dva	k4xCgMnPc2	dva
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
spokojeně	spokojeně	k6eAd1	spokojeně
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
maloměstě	maloměsto	k1gNnSc6	maloměsto
Bedford	Bedfordo	k1gNnPc2	Bedfordo
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
.	.	kIx.	.
</s>
<s>
Rimmer	Rimmer	k1gInSc1	Rimmer
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
mužů	muž	k1gMnPc2	muž
světa	svět	k1gInSc2	svět
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
topmodelkou	topmodelka	k1gFnSc7	topmodelka
Juanitou	Juanita	k1gFnSc7	Juanita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
zahýbá	zahýbat	k5eAaImIp3nS	zahýbat
se	s	k7c7	s
správcem	správce	k1gMnSc7	správce
bazénu	bazén	k1gInSc2	bazén
Hugem	Hugo	k1gMnSc7	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Listerovi	Listerův	k2eAgMnPc1d1	Listerův
přijdou	přijít	k5eAaPmIp3nP	přijít
některé	některý	k3yIgFnPc4	některý
věci	věc	k1gFnPc4	věc
podezřelé	podezřelý	k2eAgFnPc4d1	podezřelá
a	a	k8xC	a
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Rimmera	Rimmera	k1gFnSc1	Rimmera
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
jedou	jet	k5eAaImIp3nP	jet
za	za	k7c7	za
Kocourem	kocour	k1gInSc7	kocour
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
ostrově	ostrov	k1gInSc6	ostrov
nedaleko	nedaleko	k7c2	nedaleko
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Lister	Lister	k1gMnSc1	Lister
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
oběťmi	oběť	k1gFnPc7	oběť
hry	hra	k1gFnSc2	hra
zvané	zvaný	k2eAgNnSc4d1	zvané
Lepší	lepší	k1gNnSc4	lepší
než	než	k8xS	než
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
LNŽ	LNŽ	kA	LNŽ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
smrtelně	smrtelně	k6eAd1	smrtelně
návyková	návykový	k2eAgFnSc1d1	návyková
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
si	se	k3xPyFc3	se
čelenku	čelenka	k1gFnSc4	čelenka
s	s	k7c7	s
elektrodami	elektroda	k1gFnPc7	elektroda
zavedl	zavést	k5eAaPmAgInS	zavést
nejprve	nejprve	k6eAd1	nejprve
Kocour	kocour	k1gInSc1	kocour
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgInS	následovat
ho	on	k3xPp3gInSc4	on
Lister	Lister	k1gInSc4	Lister
a	a	k8xC	a
pak	pak	k6eAd1	pak
Rimmer	Rimmra	k1gFnPc2	Rimmra
<g/>
.	.	kIx.	.
</s>
<s>
Potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
Kryton	Kryton	k1gInSc4	Kryton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
LNŽ	LNŽ	kA	LNŽ
nechal	nechat	k5eAaPmAgInS	nechat
nalogovat	nalogovat	k5eAaPmF	nalogovat
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
bude	být	k5eAaImBp3nS	být
hru	hra	k1gFnSc4	hra
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
kosmická	kosmický	k2eAgFnSc1d1	kosmická
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Arthur	Arthura	k1gFnPc2	Arthura
C.	C.	kA	C.
Clarke	Clark	k1gFnSc2	Clark
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
velšský	velšský	k2eAgInSc1d1	velšský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Swansea	Swanseum	k1gNnSc2	Swanseum
City	City	k1gFnSc2	City
AFC	AFC	kA	AFC
<g/>
.	.	kIx.	.
</s>
