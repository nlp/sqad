<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
vítá	vítat	k5eAaImIp3nS	vítat
ohleduplné	ohleduplný	k2eAgMnPc4d1	ohleduplný
řidiče	řidič	k1gMnPc4	řidič
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Red	Red	k1gMnSc1	Red
Dwarf	Dwarf	k1gMnSc1	Dwarf
<g/>
:	:	kIx,	:
Infinity	Infinit	k1gInPc1	Infinit
Welcomes	Welcomesa	k1gFnPc2	Welcomesa
Careful	Carefula	k1gFnPc2	Carefula
Drivers	Drivers	k1gInSc1	Drivers
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
humoristických	humoristický	k2eAgInPc2d1	humoristický
vědeckofantastických	vědeckofantastický	k2eAgInPc2d1	vědeckofantastický
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
napsali	napsat	k5eAaPmAgMnP	napsat
Rob	roba	k1gFnPc2	roba
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
Doug	Doug	k1gMnSc1	Doug
Naylor	Naylor	k1gMnSc1	Naylor
<g/>
.	.	kIx.	.
</s>
