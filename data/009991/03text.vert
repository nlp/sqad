<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
výzkum	výzkum	k1gInSc4	výzkum
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
též	též	k9	též
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
CERN	CERN	kA	CERN
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k6eAd1	franc
<g/>
.	.	kIx.	.
Conseil	Conseit	k5eAaBmAgMnS	Conseit
Européen	Européen	k2eAgMnSc1d1	Européen
pour	pour	k1gMnSc1	pour
la	la	k1gNnSc2	la
recherche	recherchus	k1gMnSc5	recherchus
nucléaire	nucléair	k1gMnSc5	nucléair
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
spolupráce	spolupráce	k1gFnSc1	spolupráce
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
čistě	čistě	k6eAd1	čistě
vědeckého	vědecký	k2eAgInSc2d1	vědecký
a	a	k8xC	a
základního	základní	k2eAgInSc2d1	základní
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k8xC	i
výzkumu	výzkum	k1gInSc2	výzkum
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
souvisejícího	související	k2eAgNnSc2d1	související
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
nezabývá	zabývat	k5eNaImIp3nS	zabývat
činností	činnost	k1gFnSc7	činnost
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc4	výsledek
jejích	její	k3xOp3gFnPc2	její
experimentálních	experimentální	k2eAgFnPc2d1	experimentální
a	a	k8xC	a
teoretických	teoretický	k2eAgFnPc2d1	teoretická
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
zveřejňují	zveřejňovat	k5eAaImIp3nP	zveřejňovat
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
zpřístupňují	zpřístupňovat	k5eAaImIp3nP	zpřístupňovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
její	její	k3xOp3gFnSc2	její
činnosti	činnost	k1gFnSc2	činnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Momentálně	momentálně	k6eAd1	momentálně
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
prováděny	prováděn	k2eAgInPc4d1	prováděn
různé	různý	k2eAgInPc4d1	různý
pokusy	pokus	k1gInPc4	pokus
a	a	k8xC	a
zkoumání	zkoumání	k1gNnSc4	zkoumání
částic	částice	k1gFnPc2	částice
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
atom	atom	k1gInSc4	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Zakládajícími	zakládající	k2eAgMnPc7d1	zakládající
členy	člen	k1gMnPc7	člen
byly	být	k5eAaImAgFnP	být
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
(	(	kIx(	(
<g/>
do	do	k7c2	do
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
a	a	k8xC	a
od	od	k7c2	od
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
a	a	k8xC	a
</s>
</p>
<p>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Status	status	k1gInSc1	status
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
má	mít	k5eAaImIp3nS	mít
<g/>
:	:	kIx,	:
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
pak	pak	k9	pak
EU	EU	kA	EU
a	a	k8xC	a
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
laboratoř	laboratoř	k1gFnSc1	laboratoř
pro	pro	k7c4	pro
fyziku	fyzika	k1gFnSc4	fyzika
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
nejrozsáhlejší	rozsáhlý	k2eAgNnSc1d3	nejrozsáhlejší
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgMnSc6	první
takovým	takový	k3xDgMnSc7	takový
evropským	evropský	k2eAgInSc7d1	evropský
společným	společný	k2eAgInSc7d1	společný
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
zářným	zářný	k2eAgInSc7d1	zářný
příkladem	příklad	k1gInSc7	příklad
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
12	[number]	k4	12
signatářů	signatář	k1gMnPc2	signatář
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
CERN	CERN	kA	CERN
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Laboratoř	laboratoř	k1gFnSc1	laboratoř
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
francouzsko-švýcarské	francouzsko-švýcarský	k2eAgFnSc6d1	francouzsko-švýcarská
hranici	hranice	k1gFnSc6	hranice
západně	západně	k6eAd1	západně
od	od	k7c2	od
Ženevy	Ženeva	k1gFnSc2	Ženeva
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
pohoří	pohoří	k1gNnSc6	pohoří
Jura	Jura	k1gMnSc1	Jura
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zdejším	zdejší	k2eAgNnSc7d1	zdejší
zařízením	zařízení	k1gNnSc7	zařízení
pracuje	pracovat	k5eAaImIp3nS	pracovat
okolo	okolo	k7c2	okolo
9500	[number]	k4	9500
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgMnPc2	všecek
částicových	částicový	k2eAgMnPc2d1	částicový
fyziků	fyzik	k1gMnPc2	fyzik
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
500	[number]	k4	500
univerzit	univerzita	k1gFnPc2	univerzita
či	či	k8xC	či
jiných	jiný	k2eAgNnPc2d1	jiné
odborných	odborný	k2eAgNnPc2d1	odborné
pracovišť	pracoviště	k1gNnPc2	pracoviště
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
čistou	čistý	k2eAgFnSc7d1	čistá
vědou	věda	k1gFnSc7	věda
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
nejpřirozenější	přirozený	k2eAgFnPc4d3	nejpřirozenější
otázky	otázka	k1gFnPc4	otázka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hmota	hmota	k1gFnSc1	hmota
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
hmota	hmota	k1gFnSc1	hmota
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
jsou	být	k5eAaImIp3nP	být
utvářeny	utvářen	k2eAgInPc4d1	utvářen
složité	složitý	k2eAgInPc4d1	složitý
hmotné	hmotný	k2eAgInPc4d1	hmotný
objekty	objekt	k1gInPc4	objekt
jako	jako	k8xC	jako
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
planety	planeta	k1gFnPc4	planeta
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
laboratoř	laboratoř	k1gFnSc1	laboratoř
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
složení	složení	k1gNnSc4	složení
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
technologie	technologie	k1gFnSc2	technologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
prováděná	prováděný	k2eAgFnSc1d1	prováděná
zdejšími	zdejší	k2eAgMnPc7d1	zdejší
vědci	vědec	k1gMnPc7	vědec
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
testovacím	testovací	k2eAgNnSc7d1	testovací
polem	pole	k1gNnSc7	pole
i	i	k8xC	i
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
právě	právě	k9	právě
částicová	částicový	k2eAgFnSc1d1	částicová
fyzika	fyzika	k1gFnSc1	fyzika
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
přesnost	přesnost	k1gFnSc4	přesnost
provedení	provedení	k1gNnSc2	provedení
všech	všecek	k3xTgInPc2	všecek
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
prvotřídní	prvotřídní	k2eAgFnSc3d1	prvotřídní
technické	technický	k2eAgFnSc3d1	technická
vybavenosti	vybavenost	k1gFnSc3	vybavenost
hraje	hrát	k5eAaImIp3nS	hrát
laboratoř	laboratoř	k1gFnSc1	laboratoř
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
i	i	k9	i
ve	v	k7c6	v
zlepšování	zlepšování	k1gNnSc6	zlepšování
technické	technický	k2eAgFnSc2d1	technická
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
rozsah	rozsah	k1gInSc1	rozsah
programu	program	k1gInSc2	program
odborné	odborný	k2eAgFnSc2d1	odborná
přípravy	příprava	k1gFnSc2	příprava
a	a	k8xC	a
kvalifikované	kvalifikovaný	k2eAgNnSc1d1	kvalifikované
vedení	vedení	k1gNnSc1	vedení
láká	lákat	k5eAaImIp3nS	lákat
do	do	k7c2	do
laboratoře	laboratoř	k1gFnSc2	laboratoř
mnoho	mnoho	k4c1	mnoho
talentovaných	talentovaný	k2eAgMnPc2d1	talentovaný
mladých	mladý	k2eAgMnPc2d1	mladý
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
najde	najít	k5eAaPmIp3nS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
ceněny	cenit	k5eAaImNgFnP	cenit
jejich	jejich	k3xOp3gFnPc1	jejich
zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
v	v	k7c6	v
mnohonárodním	mnohonárodní	k2eAgNnSc6d1	mnohonárodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslání	poslání	k1gNnSc2	poslání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
čele	čelo	k1gNnSc6	čelo
lidského	lidský	k2eAgNnSc2d1	lidské
hledání	hledání	k1gNnSc2	hledání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
staré	staré	k1gNnSc4	staré
jako	jako	k8xS	jako
sám	sám	k3xTgMnSc1	sám
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
laboratoře	laboratoř	k1gFnSc2	laboratoř
je	být	k5eAaImIp3nS	být
porozumět	porozumět	k5eAaPmF	porozumět
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakých	jaký	k3yRgFnPc2	jaký
součástí	součást	k1gFnPc2	součást
je	být	k5eAaImIp3nS	být
hmota	hmota	k1gFnSc1	hmota
složena	složit	k5eAaPmNgFnS	složit
a	a	k8xC	a
jak	jak	k6eAd1	jak
tyto	tento	k3xDgFnPc4	tento
součásti	součást	k1gFnPc4	součást
spolu	spolu	k6eAd1	spolu
interagují	interagovat	k5eAaPmIp3nP	interagovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
urychlovače	urychlovač	k1gInPc1	urychlovač
a	a	k8xC	a
detektory	detektor	k1gInPc1	detektor
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
a	a	k8xC	a
nejsložitější	složitý	k2eAgNnPc4d3	nejsložitější
vědecká	vědecký	k2eAgNnPc4d1	vědecké
zařízení	zařízení	k1gNnPc4	zařízení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
částicový	částicový	k2eAgInSc4d1	částicový
detektor	detektor	k1gInSc4	detektor
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
L	L	kA	L
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
třetím	třetí	k4xOgInSc7	třetí
detektorem	detektor	k1gInSc7	detektor
navrženým	navržený	k2eAgInSc7d1	navržený
pro	pro	k7c4	pro
urychlovač	urychlovač	k1gInSc4	urychlovač
LEP	lep	k1gInSc1	lep
(	(	kIx(	(
<g/>
Large	Large	k1gInSc1	Large
Electron-Positron	Electron-Positron	k1gInSc1	Electron-Positron
Collider	Collider	k1gInSc4	Collider
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
asi	asi	k9	asi
Velký	velký	k2eAgInSc1d1	velký
srážeč	srážeč	k1gInSc1	srážeč
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
pozitronů	pozitron	k1gInPc2	pozitron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
L3	L3	k4	L3
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
detektorů	detektor	k1gInPc2	detektor
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
sedmadvacetikilometrového	sedmadvacetikilometrový	k2eAgInSc2d1	sedmadvacetikilometrový
kruhu	kruh	k1gInSc2	kruh
LEP	lep	k1gInSc1	lep
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
jako	jako	k8xC	jako
třípatrový	třípatrový	k2eAgInSc4d1	třípatrový
dům	dům	k1gInSc4	dům
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
komplikovanou	komplikovaný	k2eAgFnSc7d1	komplikovaná
elektronikou	elektronika	k1gFnSc7	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
LEP	lep	k1gInSc1	lep
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
detektory	detektor	k1gInPc1	detektor
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
uvnitř	uvnitř	k7c2	uvnitř
tunelu	tunel	k1gInSc2	tunel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
50	[number]	k4	50
až	až	k9	až
150	[number]	k4	150
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
poskytovat	poskytovat	k5eAaImF	poskytovat
svazky	svazek	k1gInPc4	svazek
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
energií	energie	k1gFnSc7	energie
fyzikům	fyzik	k1gMnPc3	fyzik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gFnPc4	on
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
experimentech	experiment	k1gInPc6	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Laboratoř	laboratoř	k1gFnSc1	laboratoř
vděčí	vděčit	k5eAaImIp3nS	vděčit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
využívá	využívat	k5eAaPmIp3nS	využívat
největší	veliký	k2eAgFnSc4d3	veliký
soustavu	soustava	k1gFnSc4	soustava
propojených	propojený	k2eAgInPc2d1	propojený
urychlovačů	urychlovač	k1gInPc2	urychlovač
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
urychlovače	urychlovač	k1gInPc1	urychlovač
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
druhy	druh	k1gMnPc7	druh
částic	částice	k1gFnPc2	částice
potřebných	potřebný	k2eAgFnPc2d1	potřebná
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
experimentů	experiment	k1gInPc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
LEP	lep	k1gInSc4	lep
jsou	být	k5eAaImIp3nP	být
urychlovány	urychlován	k2eAgInPc1d1	urychlován
elektrony	elektron	k1gInPc1	elektron
a	a	k8xC	a
pozitrony	pozitron	k1gInPc1	pozitron
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
srážejí	srážet	k5eAaImIp3nP	srážet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
částice	částice	k1gFnSc2	částice
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
energií	energie	k1gFnSc7	energie
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
miony	miona	k1gFnSc2	miona
otevírají	otevírat	k5eAaImIp3nP	otevírat
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
jako	jako	k8xC	jako
lovec	lovec	k1gMnSc1	lovec
perel	perla	k1gFnPc2	perla
perlorodku	perlorodka	k1gFnSc4	perlorodka
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
podívat	podívat	k5eAaPmF	podívat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
urychlovačů	urychlovač	k1gInPc2	urychlovač
CERN	CERN	kA	CERN
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dokonce	dokonce	k9	dokonce
urychlovat	urychlovat	k5eAaImF	urychlovat
i	i	k8xC	i
jádra	jádro	k1gNnPc1	jádro
atomů	atom	k1gInPc2	atom
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozbíjejí	rozbíjet	k5eAaImIp3nP	rozbíjet
na	na	k7c6	na
nepohyblivých	pohyblivý	k2eNgInPc6d1	nepohyblivý
terčících	terčík	k1gInPc6	terčík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
centra	centrum	k1gNnSc2	centrum
srážky	srážka	k1gFnSc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
po	po	k7c4	po
kratičký	kratičký	k2eAgInSc4d1	kratičký
okamžik	okamžik	k1gInSc4	okamžik
panují	panovat	k5eAaImIp3nP	panovat
poměry	poměr	k1gInPc4	poměr
blízké	blízký	k2eAgInPc4d1	blízký
stavu	stav	k1gInSc3	stav
vesmíru	vesmír	k1gInSc2	vesmír
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vynoří	vynořit	k5eAaPmIp3nP	vynořit
stovky	stovka	k1gFnPc1	stovka
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumníci	výzkumník	k1gMnPc1	výzkumník
zde	zde	k6eAd1	zde
studují	studovat	k5eAaImIp3nP	studovat
velké	velký	k2eAgInPc4d1	velký
počty	počet	k1gInPc4	počet
takových	takový	k3xDgFnPc2	takový
pozoruhodných	pozoruhodný	k2eAgFnPc2d1	pozoruhodná
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
porozumět	porozumět	k5eAaPmF	porozumět
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
za	za	k7c4	za
13	[number]	k4	13
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
vesmír	vesmír	k1gInSc1	vesmír
stal	stát	k5eAaPmAgMnS	stát
takovým	takový	k3xDgNnSc7	takový
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
vidíme	vidět	k5eAaImIp1nP	vidět
dnes	dnes	k6eAd1	dnes
kolem	kolem	k7c2	kolem
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Částicová	částicový	k2eAgFnSc1d1	částicová
fyzika	fyzika	k1gFnSc1	fyzika
studuje	studovat	k5eAaImIp3nS	studovat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
vlasu	vlas	k1gInSc2	vlas
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
přibližně	přibližně	k6eAd1	přibližně
milion	milion	k4xCgInSc1	milion
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
něco	něco	k3yInSc4	něco
tak	tak	k9	tak
malého	malý	k2eAgNnSc2d1	malé
jako	jako	k8xC	jako
atom	atom	k1gInSc1	atom
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
a	a	k8xC	a
studují	studovat	k5eAaImIp3nP	studovat
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Kdybychom	kdyby	kYmCp1nP	kdyby
si	se	k3xPyFc3	se
představili	představit	k5eAaPmAgMnP	představit
atom	atom	k1gInSc4	atom
zvětšený	zvětšený	k2eAgInSc4d1	zvětšený
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
stadionu	stadion	k1gInSc2	stadion
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
jádro	jádro	k1gNnSc1	jádro
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
velikost	velikost	k1gFnSc4	velikost
malého	malý	k2eAgInSc2d1	malý
míčku	míček	k1gInSc2	míček
či	či	k8xC	či
kuličky	kulička	k1gFnSc2	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
z	z	k7c2	z
99,99	[number]	k4	99,99
<g/>
%	%	kIx~	%
prázdné	prázdná	k1gFnSc2	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
CERN	CERN	kA	CERN
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
zbývajících	zbývající	k2eAgInPc2d1	zbývající
0,01	[number]	k4	0,01
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poznávání	poznávání	k1gNnSc3	poznávání
tak	tak	k6eAd1	tak
malých	malý	k2eAgFnPc2d1	malá
věcí	věc	k1gFnPc2	věc
jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
extrémně	extrémně	k6eAd1	extrémně
silné	silný	k2eAgInPc1d1	silný
"	"	kIx"	"
<g/>
mikroskopy	mikroskop	k1gInPc1	mikroskop
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgFnPc7	ten
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
fyziky	fyzika	k1gFnSc2	fyzika
urychlovače	urychlovač	k1gInSc2	urychlovač
a	a	k8xC	a
detektory	detektor	k1gInPc4	detektor
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ty	ten	k3xDgFnPc1	ten
používané	používaný	k2eAgFnPc1d1	používaná
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Urychlovače	urychlovač	k1gInPc4	urychlovač
částic	částice	k1gFnPc2	částice
==	==	k?	==
</s>
</p>
<p>
<s>
Urychlením	urychlení	k1gNnSc7	urychlení
získají	získat	k5eAaPmIp3nP	získat
částice	částice	k1gFnPc1	částice
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc2d1	vysoká
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
následné	následný	k2eAgNnSc1d1	následné
střílení	střílení	k1gNnSc1	střílení
do	do	k7c2	do
terčů	terč	k1gInPc2	terč
nebo	nebo	k8xC	nebo
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
srážky	srážka	k1gFnSc2	srážka
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
fyzikům	fyzik	k1gMnPc3	fyzik
rozluštit	rozluštit	k5eAaPmF	rozluštit
tajemství	tajemství	k1gNnSc4	tajemství
sil	síla	k1gFnPc2	síla
působící	působící	k2eAgFnSc1d1	působící
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
urychlovače	urychlovač	k1gInPc4	urychlovač
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgInPc1d1	lineární
a	a	k8xC	a
kruhové	kruhový	k2eAgInPc1d1	kruhový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
CERN	CERN	kA	CERN
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
oběma	dva	k4xCgInPc7	dva
<g/>
.	.	kIx.	.
</s>
<s>
Urychlovače	urychlovač	k1gInPc1	urychlovač
používají	používat	k5eAaImIp3nP	používat
silné	silný	k2eAgNnSc4d1	silné
elektrické	elektrický	k2eAgNnSc4d1	elektrické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
"	"	kIx"	"
<g/>
nahustí	nahustit	k5eAaPmIp3nS	nahustit
<g/>
"	"	kIx"	"
energii	energie	k1gFnSc4	energie
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
svazek	svazek	k1gInSc1	svazek
přesně	přesně	k6eAd1	přesně
zaostřuje	zaostřovat	k5eAaImIp3nS	zaostřovat
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
částic	částice	k1gFnPc2	částice
na	na	k7c6	na
kruhové	kruhový	k2eAgFnSc6d1	kruhová
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lineární	lineární	k2eAgInPc1d1	lineární
urychlovače	urychlovač	k1gInPc1	urychlovač
předávají	předávat	k5eAaImIp3nP	předávat
energii	energie	k1gFnSc4	energie
svazku	svazek	k1gInSc2	svazek
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
postupném	postupný	k2eAgInSc6d1	postupný
pohybu	pohyb	k1gInSc6	pohyb
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yQnSc7	co
delší	dlouhý	k2eAgInSc1d2	delší
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgFnSc1d2	vyšší
konečná	konečný	k2eAgFnSc1d1	konečná
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kruhových	kruhový	k2eAgInPc6d1	kruhový
urychlovačích	urychlovač	k1gInPc6	urychlovač
částice	částice	k1gFnPc1	částice
létají	létat	k5eAaImIp3nP	létat
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
kolem	kolem	k6eAd1	kolem
dokola	dokola	k6eAd1	dokola
a	a	k8xC	a
hromadí	hromadit	k5eAaImIp3nP	hromadit
tak	tak	k9	tak
energii	energie	k1gFnSc4	energie
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
oběhem	oběh	k1gInSc7	oběh
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ale	ale	k8xC	ale
rychlost	rychlost	k1gFnSc1	rychlost
částic	částice	k1gFnPc2	částice
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
"	"	kIx"	"
<g/>
vyletět	vyletět	k5eAaPmF	vyletět
<g/>
"	"	kIx"	"
ven	ven	k6eAd1	ven
z	z	k7c2	z
kruhu	kruh	k1gInSc2	kruh
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
auto	auto	k1gNnSc4	auto
projíždějící	projíždějící	k2eAgFnSc4d1	projíždějící
ostrou	ostrý	k2eAgFnSc4d1	ostrá
zatáčku	zatáčka	k1gFnSc4	zatáčka
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
LEP	lep	k1gInSc1	lep
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zakřivení	zakřivení	k1gNnSc1	zakřivení
kruhu	kruh	k1gInSc2	kruh
bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Částice	částice	k1gFnSc1	částice
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
vzájemně	vzájemně	k6eAd1	vzájemně
srážejí	srážet	k5eAaImIp3nP	srážet
(	(	kIx(	(
<g/>
vstřícné	vstřícný	k2eAgInPc1d1	vstřícný
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jako	jako	k8xS	jako
v	v	k7c4	v
LEP	lep	k1gInSc4	lep
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
narážejí	narážet	k5eAaImIp3nP	narážet
na	na	k7c4	na
nepohyblivé	pohyblivý	k2eNgInPc4d1	nepohyblivý
terče	terč	k1gInPc4	terč
vně	vně	k7c2	vně
urychlovače	urychlovač	k1gInSc2	urychlovač
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgFnPc1d1	nová
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
hmota	hmota	k1gFnSc1	hmota
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
energii	energie	k1gFnSc6	energie
a	a	k8xC	a
obráceně	obráceně	k6eAd1	obráceně
podle	podle	k7c2	podle
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
známé	známý	k2eAgFnSc2d1	známá
rovnice	rovnice	k1gFnSc2	rovnice
E	E	kA	E
=	=	kIx~	=
m.	m.	k?	m.
<g/>
c2	c2	k4	c2
<g/>
.	.	kIx.	.
</s>
<s>
E	E	kA	E
označuje	označovat	k5eAaImIp3nS	označovat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
m	m	kA	m
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
c	c	k0	c
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
E	E	kA	E
=	=	kIx~	=
m.	m.	k?	m.
<g/>
c2	c2	k4	c2
nám	my	k3xPp1nPc3	my
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
1	[number]	k4	1
gram	gram	k1gInSc1	gram
hmoty	hmota	k1gFnSc2	hmota
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
úžasných	úžasný	k2eAgInPc2d1	úžasný
20.10	[number]	k4	20.10
<g/>
1	[number]	k4	1
kalorií	kalorie	k1gFnPc2	kalorie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
kilojoulů	kilojoule	k1gInPc2	kilojoule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
studované	studovaný	k2eAgNnSc1d1	studované
v	v	k7c6	v
CERN	CERN	kA	CERN
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
tak	tak	k6eAd1	tak
maličké	maličký	k2eAgNnSc1d1	maličké
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
urychlené	urychlený	k2eAgFnSc6d1	urychlená
částici	částice	k1gFnSc6	částice
v	v	k7c4	v
LEP	lep	k1gInSc4	lep
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
asi	asi	k9	asi
10-9	[number]	k4	10-9
kalorie	kalorie	k1gFnPc4	kalorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
srážkách	srážka	k1gFnPc6	srážka
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tak	tak	k6eAd1	tak
vysoké	vysoký	k2eAgFnSc3d1	vysoká
koncentraci	koncentrace	k1gFnSc3	koncentrace
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
produkují	produkovat	k5eAaImIp3nP	produkovat
další	další	k2eAgFnPc1d1	další
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
můžeme	moct	k5eAaImIp1nP	moct
studovat	studovat	k5eAaImF	studovat
a	a	k8xC	a
jež	jenž	k3xRgInPc1	jenž
nám	my	k3xPp1nPc3	my
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
nový	nový	k2eAgInSc1d1	nový
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
nejniternější	niterní	k2eAgNnSc4d3	niterní
tajemství	tajemství	k1gNnSc4	tajemství
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tunelu	tunel	k1gInSc6	tunel
LEP	lep	k1gInSc1	lep
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
demontáži	demontáž	k1gFnSc6	demontáž
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
instalován	instalován	k2eAgInSc1d1	instalován
nový	nový	k2eAgInSc1d1	nový
urychlovač	urychlovač	k1gInSc1	urychlovač
LHC	LHC	kA	LHC
(	(	kIx(	(
<g/>
Large	Larg	k1gMnSc2	Larg
Hadron	Hadron	k1gMnSc1	Hadron
Collider	Collider	k1gMnSc1	Collider
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Velký	velký	k2eAgInSc1d1	velký
srážeč	srážeč	k1gInSc1	srážeč
hadronů	hadron	k1gInPc2	hadron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zkušebního	zkušební	k2eAgInSc2d1	zkušební
provozu	provoz	k1gInSc2	provoz
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
a	a	k8xC	a
po	po	k7c6	po
několikadenním	několikadenní	k2eAgInSc6d1	několikadenní
provozu	provoz	k1gInSc6	provoz
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
poruchy	porucha	k1gFnSc2	porucha
chlazení	chlazení	k1gNnSc2	chlazení
odstaven	odstavit	k5eAaPmNgInS	odstavit
až	až	k6eAd1	až
do	do	k7c2	do
půlky	půlka	k1gFnSc2	půlka
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
září	září	k1gNnSc2	září
2009	[number]	k4	2009
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
probíhá	probíhat	k5eAaImIp3nS	probíhat
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
odstávka	odstávka	k1gFnSc1	odstávka
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
urychlovač	urychlovač	k1gInSc1	urychlovač
rozběhnout	rozběhnout	k5eAaPmF	rozběhnout
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
při	při	k7c6	při
srážkách	srážka	k1gFnPc6	srážka
v	v	k7c6	v
LHC	LHC	kA	LHC
je	být	k5eAaImIp3nS	být
produkováno	produkován	k2eAgNnSc1d1	produkováno
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
v	v	k7c4	v
LEP	lep	k1gInSc4	lep
<g/>
,	,	kIx,	,
i	i	k8xC	i
detektory	detektor	k1gInPc1	detektor
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
být	být	k5eAaImF	být
větší	veliký	k2eAgInPc4d2	veliký
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
ATLAS	Atlas	k1gInSc1	Atlas
je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc1d1	veliký
asi	asi	k9	asi
jako	jako	k8xC	jako
šestipatrová	šestipatrový	k2eAgFnSc1d1	šestipatrová
budova	budova	k1gFnSc1	budova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
paradoxní	paradoxní	k2eAgMnSc1d1	paradoxní
<g/>
,	,	kIx,	,
že	že	k8xS	že
zkoumání	zkoumání	k1gNnSc6	zkoumání
něčeho	něco	k3yInSc2	něco
tak	tak	k8xS	tak
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
malého	malý	k2eAgNnSc2d1	malé
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
tak	tak	k6eAd1	tak
velké	velký	k2eAgInPc4d1	velký
detektory	detektor	k1gInPc4	detektor
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
energií	energie	k1gFnSc7	energie
vznikající	vznikající	k2eAgFnSc7d1	vznikající
při	při	k7c6	při
srážkách	srážka	k1gFnPc6	srážka
mohou	moct	k5eAaImIp3nP	moct
pronikat	pronikat	k5eAaImF	pronikat
silnější	silný	k2eAgFnSc7d2	silnější
vrstvou	vrstva	k1gFnSc7	vrstva
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
detektory	detektor	k1gInPc1	detektor
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
ohromné	ohromný	k2eAgNnSc1d1	ohromné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Detektory	detektor	k1gInPc4	detektor
částic	částice	k1gFnPc2	částice
==	==	k?	==
</s>
</p>
<p>
<s>
Úkolem	úkol	k1gInSc7	úkol
detektorů	detektor	k1gInPc2	detektor
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
L3	L3	k1gFnSc4	L3
nebo	nebo	k8xC	nebo
ATLAS	Atlas	k1gInSc4	Atlas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Detektory	detektor	k1gInPc1	detektor
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
ruským	ruský	k2eAgFnPc3d1	ruská
panenkám	panenka	k1gFnPc3	panenka
matrjoškám	matrjoška	k1gFnPc3	matrjoška
–	–	k?	–
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
vložených	vložený	k2eAgFnPc2d1	vložená
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
srážkám	srážka	k1gFnPc3	srážka
dochází	docházet	k5eAaImIp3nS	docházet
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
vrstva	vrstva	k1gFnSc1	vrstva
měří	měřit	k5eAaImIp3nS	měřit
různé	různý	k2eAgFnPc4d1	různá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
nově	nova	k1gFnSc3	nova
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
detektorů	detektor	k1gInPc2	detektor
na	na	k7c6	na
urychlovači	urychlovač	k1gInSc6	urychlovač
LEP	lep	k1gInSc1	lep
je	být	k5eAaImIp3nS	být
DELPHI	DELPHI	kA	DELPHI
–	–	k?	–
detekční	detekční	k2eAgNnSc1d1	detekční
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
získávají	získávat	k5eAaImIp3nP	získávat
podrobné	podrobný	k2eAgInPc1d1	podrobný
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
drahách	draha	k1gFnPc6	draha
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyletují	vyletovat	k5eAaImIp3nP	vyletovat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
srážky	srážka	k1gFnSc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přicházejí	přicházet	k5eAaImIp3nP	přicházet
zařízení	zařízení	k1gNnPc1	zařízení
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kalorimetry	kalorimetr	k1gInPc1	kalorimetr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
většina	většina	k1gFnSc1	většina
částic	částice	k1gFnPc2	částice
ukončí	ukončit	k5eAaPmIp3nS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
skvrnky	skvrnka	k1gFnPc1	skvrnka
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
částice	částice	k1gFnSc1	částice
přinesly	přinést	k5eAaPmAgFnP	přinést
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
zde	zde	k6eAd1	zde
odevzdaly	odevzdat	k5eAaPmAgFnP	odevzdat
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
vrstva	vrstva	k1gFnSc1	vrstva
celého	celý	k2eAgNnSc2d1	celé
"	"	kIx"	"
<g/>
cibulového	cibulový	k2eAgNnSc2d1	cibulové
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pásem	pás	k1gInSc7	pás
detektorů	detektor	k1gInPc2	detektor
zaznamenávajících	zaznamenávající	k2eAgInPc2d1	zaznamenávající
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
proniknout	proniknout	k5eAaPmF	proniknout
až	až	k9	až
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
magnetu	magnet	k1gInSc2	magnet
uvnitř	uvnitř	k7c2	uvnitř
detektoru	detektor	k1gInSc2	detektor
je	být	k5eAaImIp3nS	být
zakřivovat	zakřivovat	k5eAaImF	zakřivovat
dráhy	dráha	k1gFnPc4	dráha
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
identifikaci	identifikace	k1gFnSc6	identifikace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
studia	studio	k1gNnSc2	studio
částic	částice	k1gFnPc2	částice
==	==	k?	==
</s>
</p>
<p>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nás	my	k3xPp1nPc2	my
samotných	samotný	k2eAgMnPc2d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zdejší	zdejší	k2eAgFnSc3d1	zdejší
práci	práce	k1gFnSc3	práce
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
laboratoří	laboratoř	k1gFnPc2	laboratoř
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zabývajících	zabývající	k2eAgInPc2d1	zabývající
se	se	k3xPyFc4	se
částicovou	částicový	k2eAgFnSc7d1	částicová
fyzikou	fyzika	k1gFnSc7	fyzika
nyní	nyní	k6eAd1	nyní
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
vysvětlili	vysvětlit	k5eAaPmAgMnP	vysvětlit
složení	složení	k1gNnSc4	složení
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
pouhé	pouhý	k2eAgInPc1d1	pouhý
čtyři	čtyři	k4xCgInPc1	čtyři
druhy	druh	k1gInPc1	druh
částic	částice	k1gFnPc2	částice
–	–	k?	–
stavebních	stavební	k2eAgInPc2d1	stavební
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kvark	kvark	k1gInSc1	kvark
u	u	k7c2	u
<g/>
,	,	kIx,	,
kvark	kvark	k1gInSc1	kvark
d	d	k?	d
<g/>
,	,	kIx,	,
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
elektronové	elektronový	k2eAgNnSc1d1	elektronové
neutrino	neutrino	k1gNnSc1	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
a	a	k8xC	a
všechno	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
planety	planeta	k1gFnSc2	planeta
i	i	k8xC	i
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
hvězdy	hvězda	k1gFnPc1	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
za	za	k7c2	za
stejných	stejný	k2eAgFnPc2d1	stejná
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
stavebnice	stavebnice	k1gFnSc1	stavebnice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
u	u	k7c2	u
a	a	k8xC	a
d	d	k?	d
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
společně	společně	k6eAd1	společně
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnPc1	jádro
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
elektrony	elektron	k1gInPc7	elektron
v	v	k7c6	v
obalech	obal	k1gInPc6	obal
tvoří	tvořit	k5eAaImIp3nP	tvořit
atomy	atom	k1gInPc1	atom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
různě	různě	k6eAd1	různě
spojují	spojovat	k5eAaImIp3nP	spojovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
složitější	složitý	k2eAgInPc4d2	složitější
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
členem	člen	k1gMnSc7	člen
rodiny	rodina	k1gFnSc2	rodina
je	být	k5eAaImIp3nS	být
elektronové	elektronový	k2eAgNnSc1d1	elektronové
neutrino	neutrino	k1gNnSc1	neutrino
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
interaguje	interagovat	k5eAaImIp3nS	interagovat
s	s	k7c7	s
ostatní	ostatní	k2eAgFnSc7d1	ostatní
hmotou	hmota	k1gFnSc7	hmota
tak	tak	k6eAd1	tak
slabě	slabě	k6eAd1	slabě
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
částice	částice	k1gFnPc1	částice
hmoty	hmota	k1gFnSc2	hmota
představují	představovat	k5eAaImIp3nP	představovat
jen	jen	k9	jen
polovinu	polovina	k1gFnSc4	polovina
celého	celý	k2eAgInSc2d1	celý
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
držely	držet	k5eAaImAgFnP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
něco	něco	k3yInSc4	něco
dalšího	další	k2eAgMnSc4d1	další
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Lepidlo	lepidlo	k1gNnSc1	lepidlo
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
tvořící	tvořící	k2eAgFnSc4d1	tvořící
sílu	síla	k1gFnSc4	síla
se	se	k3xPyFc4	se
však	však	k9	však
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
částic	částice	k1gFnPc2	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
přenášet	přenášet	k5eAaImF	přenášet
interakce	interakce	k1gFnPc4	interakce
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
částice	částice	k1gFnSc2	částice
hmoty	hmota	k1gFnSc2	hmota
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gravitace	gravitace	k1gFnSc1	gravitace
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc1d3	nejznámější
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
nejslabší	slabý	k2eAgFnSc7d3	nejslabší
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgMnPc4	který
se	se	k3xPyFc4	se
domníváme	domnívat	k5eAaImIp1nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gInSc7	její
nosičem	nosič	k1gInSc7	nosič
<g/>
,	,	kIx,	,
graviton	graviton	k1gInSc4	graviton
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
objevena	objevit	k5eAaPmNgFnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
měřítka	měřítko	k1gNnSc2	měřítko
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
nositelem	nositel	k1gMnSc7	nositel
jsou	být	k5eAaImIp3nP	být
gluony	gluon	k1gInPc4	gluon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
drží	držet	k5eAaImIp3nP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
kvarky	kvark	k1gInPc1	kvark
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k9	tak
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
má	mít	k5eAaImIp3nS	mít
kuriózní	kuriózní	k2eAgFnSc1d1	kuriózní
vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
:	:	kIx,	:
Čím	co	k3yInSc7	co
jsou	být	k5eAaImIp3nP	být
kvarky	kvark	k1gInPc1	kvark
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
síly	síla	k1gFnSc2	síla
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
větší	veliký	k2eAgInPc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvarky	kvark	k1gInPc1	kvark
jsou	být	k5eAaImIp3nP	být
uvězněny	uvěznit	k5eAaPmNgInP	uvěznit
uvnitř	uvnitř	k7c2	uvnitř
protonů	proton	k1gInPc2	proton
či	či	k8xC	či
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
krajními	krajní	k2eAgInPc7d1	krajní
póly	pól	k1gInPc7	pól
co	co	k9	co
do	do	k7c2	do
intenzity	intenzita	k1gFnSc2	intenzita
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
interakcí	interakce	k1gFnPc2	interakce
<g/>
:	:	kIx,	:
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
s	s	k7c7	s
nosiči	nosič	k1gInSc3	nosič
fotony	foton	k1gInPc4	foton
a	a	k8xC	a
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
nosiči	nosič	k1gMnPc7	nosič
jsou	být	k5eAaImIp3nP	být
částice	částice	k1gFnPc1	částice
W	W	kA	W
a	a	k8xC	a
Z.	Z.	kA	Z.
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
interakce	interakce	k1gFnSc1	interakce
udržuje	udržovat	k5eAaImIp3nS	udržovat
elektrony	elektron	k1gInPc4	elektron
na	na	k7c6	na
orbitách	orbita	k1gFnPc6	orbita
okolo	okolo	k7c2	okolo
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
drží	držet	k5eAaImIp3nP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
atomy	atom	k1gInPc1	atom
v	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
či	či	k8xC	či
biochemických	biochemický	k2eAgFnPc6d1	biochemická
molekulách	molekula	k1gFnPc6	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgFnSc1d1	slabá
interakce	interakce	k1gFnSc1	interakce
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
hvězdám	hvězda	k1gFnPc3	hvězda
svítit	svítit	k5eAaImF	svítit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
příčinou	příčina	k1gFnSc7	příčina
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
(	(	kIx(	(
<g/>
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
beta	beta	k1gNnSc1	beta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
interakce	interakce	k1gFnPc1	interakce
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
vysvětlovány	vysvětlovat	k5eAaImNgInP	vysvětlovat
pomocí	pomocí	k7c2	pomocí
jedné	jeden	k4xCgFnSc2	jeden
společné	společný	k2eAgFnSc2d1	společná
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
elektroslabá	elektroslabat	k5eAaImIp3nS	elektroslabat
teorie	teorie	k1gFnPc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
důležitý	důležitý	k2eAgInSc4d1	důležitý
krok	krok	k1gInSc4	krok
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
cílů	cíl	k1gInPc2	cíl
současné	současný	k2eAgFnSc2d1	současná
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
:	:	kIx,	:
Najít	najít	k5eAaPmF	najít
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
teorii	teorie	k1gFnSc4	teorie
popisující	popisující	k2eAgNnSc1d1	popisující
všechny	všechen	k3xTgFnPc4	všechen
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpřekvapivějších	překvapivý	k2eAgFnPc2d3	nejpřekvapivější
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
částicové	částicový	k2eAgFnSc6d1	částicová
fyzice	fyzika	k1gFnSc6	fyzika
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
dalších	další	k2eAgFnPc2d1	další
dvou	dva	k4xCgFnPc2	dva
rodin	rodina	k1gFnPc2	rodina
částic	částice	k1gFnPc2	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
čtveřici	čtveřice	k1gFnSc4	čtveřice
kvark	kvark	k1gInSc1	kvark
u	u	k7c2	u
<g/>
,	,	kIx,	,
kvark	kvark	k1gInSc1	kvark
d	d	k?	d
<g/>
,	,	kIx,	,
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
elektronové	elektronový	k2eAgNnSc1d1	elektronové
neutrino	neutrino	k1gNnSc1	neutrino
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgInSc7d1	přirozený
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jedině	jedině	k6eAd1	jedině
na	na	k7c6	na
exotických	exotický	k2eAgNnPc6d1	exotické
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
horká	horký	k2eAgNnPc1d1	horké
centra	centrum	k1gNnPc1	centrum
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
produkovány	produkovat	k5eAaImNgInP	produkovat
na	na	k7c6	na
urychlovačích	urychlovač	k1gInPc6	urychlovač
v	v	k7c6	v
CERN	CERN	kA	CERN
a	a	k8xC	a
podobných	podobný	k2eAgFnPc6d1	podobná
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbírku	sbírka	k1gFnSc4	sbírka
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
antihmota	antihmota	k1gFnSc1	antihmota
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zrcadlový	zrcadlový	k2eAgInSc1d1	zrcadlový
obraz	obraz	k1gInSc1	obraz
<g/>
"	"	kIx"	"
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Antihmotu	antihmota	k1gFnSc4	antihmota
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gFnSc1	Dirac
<g/>
.	.	kIx.	.
</s>
<s>
Brzo	brzo	k6eAd1	brzo
poté	poté	k6eAd1	poté
Američan	Američan	k1gMnSc1	Američan
Carl	Carl	k1gMnSc1	Carl
David	David	k1gMnSc1	David
Anderson	Anderson	k1gMnSc1	Anderson
antihmotu	antihmota	k1gFnSc4	antihmota
skutečně	skutečně	k6eAd1	skutečně
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
srážkách	srážka	k1gFnPc6	srážka
vysokoenergetických	vysokoenergetický	k2eAgFnPc2d1	vysokoenergetická
částic	částice	k1gFnPc2	částice
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Volná	volný	k2eAgFnSc1d1	volná
antihmota	antihmota	k1gFnSc1	antihmota
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vesmír	vesmír	k1gInSc1	vesmír
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
antihmota	antihmota	k1gFnSc1	antihmota
existovaly	existovat	k5eAaImAgFnP	existovat
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
stejně	stejně	k6eAd1	stejně
antihmoty	antihmota	k1gFnSc2	antihmota
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hmoty	hmota	k1gFnPc1	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
popisu	popis	k1gInSc2	popis
částic	částice	k1gFnPc2	částice
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
částic	částice	k1gFnPc2	částice
–	–	k?	–
nosičů	nosič	k1gInPc2	nosič
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
standardní	standardní	k2eAgInSc1d1	standardní
model	model	k1gInSc1	model
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
už	už	k9	už
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
úspěšně	úspěšně	k6eAd1	úspěšně
prochází	procházet	k5eAaImIp3nS	procházet
všemi	všecek	k3xTgInPc7	všecek
experimentálními	experimentální	k2eAgInPc7d1	experimentální
testy	test	k1gInPc7	test
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
standardní	standardní	k2eAgInSc4d1	standardní
model	model	k1gInSc4	model
úplným	úplný	k2eAgInSc7d1	úplný
popisem	popis	k1gInSc7	popis
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikové	fyzik	k1gMnPc1	fyzik
v	v	k7c6	v
CERN	CERN	kA	CERN
přispívají	přispívat	k5eAaImIp3nP	přispívat
svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
dokonalejší	dokonalý	k2eAgFnSc2d2	dokonalejší
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vesmír	vesmír	k1gInSc1	vesmír
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
model	model	k1gInSc1	model
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
stále	stále	k6eAd1	stále
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
nezodpovězených	zodpovězený	k2eNgFnPc2d1	nezodpovězená
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
konečnou	konečný	k2eAgFnSc7d1	konečná
teorií	teorie	k1gFnSc7	teorie
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
mají	mít	k5eAaImIp3nP	mít
částice	částice	k1gFnPc1	částice
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
?	?	kIx.	?
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc4d1	různá
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jenom	jenom	k9	jenom
jiným	jiný	k2eAgInSc7d1	jiný
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
věc	věc	k1gFnSc4	věc
<g/>
?	?	kIx.	?
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
žádná	žádný	k3yNgFnSc1	žádný
antihmota	antihmota	k1gFnSc1	antihmota
<g/>
?	?	kIx.	?
</s>
<s>
To	to	k9	to
standardní	standardní	k2eAgInSc4d1	standardní
model	model	k1gInSc4	model
neříká	říkat	k5eNaImIp3nS	říkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
urychlovač	urychlovač	k1gInSc1	urychlovač
LHC	LHC	kA	LHC
umožní	umožnit	k5eAaPmIp3nS	umožnit
pustit	pustit	k5eAaPmF	pustit
se	se	k3xPyFc4	se
do	do	k7c2	do
hledání	hledání	k1gNnSc2	hledání
odpovědí	odpověď	k1gFnPc2	odpověď
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
důležité	důležitý	k2eAgFnPc4d1	důležitá
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
LHC	LHC	kA	LHC
byla	být	k5eAaImAgFnS	být
započata	započnout	k5eAaPmNgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
program	program	k1gInSc1	program
svádí	svádět	k5eAaImIp3nS	svádět
dohromady	dohromady	k6eAd1	dohromady
tisíce	tisíc	k4xCgInPc1	tisíc
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgInSc1d1	uveden
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
protonových	protonový	k2eAgInPc2d1	protonový
svazků	svazek	k1gInPc2	svazek
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
v	v	k7c6	v
LHC	LHC	kA	LHC
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
nový	nový	k2eAgInSc1d1	nový
silný	silný	k2eAgInSc1d1	silný
supravodivý	supravodivý	k2eAgInSc1d1	supravodivý
magnet	magnet	k1gInSc1	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
detektorů	detektor	k1gInPc2	detektor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
budou	být	k5eAaImBp3nP	být
studovat	studovat	k5eAaImF	studovat
srážky	srážka	k1gFnPc1	srážka
na	na	k7c4	na
LHC	LHC	kA	LHC
a	a	k8xC	a
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInSc4d2	veliký
a	a	k8xC	a
komplexnější	komplexní	k2eAgInSc4d2	komplexnější
než	než	k8xS	než
kdykoli	kdykoli	k6eAd1	kdykoli
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
už	už	k6eAd1	už
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
týmy	tým	k1gInPc1	tým
spolupracujících	spolupracující	k2eAgMnPc2d1	spolupracující
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tyto	tento	k3xDgInPc4	tento
detektory	detektor	k1gInPc4	detektor
konstruují	konstruovat	k5eAaImIp3nP	konstruovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnPc1d3	veliký
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
kdy	kdy	k6eAd1	kdy
částicová	částicový	k2eAgFnSc1d1	částicová
fyzika	fyzika	k1gFnSc1	fyzika
poznala	poznat	k5eAaPmAgFnS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4400	[number]	k4	4400
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
CERN	CERN	kA	CERN
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
se	se	k3xPyFc4	se
tím	ten	k3xDgMnSc7	ten
standard	standard	k1gInSc4	standard
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
spolupráci	spolupráce	k1gFnSc4	spolupráce
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
LHC	LHC	kA	LHC
se	se	k3xPyFc4	se
CERN	CERN	kA	CERN
stává	stávat	k5eAaImIp3nS	stávat
první	první	k4xOgFnSc1	první
skutečně	skutečně	k6eAd1	skutečně
komplexní	komplexní	k2eAgFnSc1d1	komplexní
světovou	světový	k2eAgFnSc7d1	světová
laboratoří	laboratoř	k1gFnSc7	laboratoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evropská	evropský	k2eAgFnSc1d1	Evropská
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
výzkum	výzkum	k1gInSc4	výzkum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
CERNu	CERNus	k1gInSc2	CERNus
</s>
</p>
<p>
<s>
CERN	CERN	kA	CERN
-	-	kIx~	-
Programy	program	k1gInPc1	program
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
a	a	k8xC	a
absolventy	absolvent	k1gMnPc4	absolvent
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgFnPc1d1	pracovní
příležitosti	příležitost	k1gFnPc1	příležitost
</s>
</p>
<p>
<s>
CERN	CERN	kA	CERN
Document	Document	k1gInSc1	Document
Server	server	k1gInSc1	server
–	–	k?	–
dokumentový	dokumentový	k2eAgInSc1d1	dokumentový
a	a	k8xC	a
multimediální	multimediální	k2eAgInSc1d1	multimediální
archiv	archiv	k1gInSc1	archiv
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgNnSc1d1	speciální
vysílání	vysílání	k1gNnSc1	vysílání
pořadu	pořad	k1gInSc2	pořad
Hyde	Hyd	k1gFnSc2	Hyd
Park	park	k1gInSc1	park
Civilizace	civilizace	k1gFnSc2	civilizace
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
CERNu	CERNus	k1gInSc2	CERNus
5.1	[number]	k4	5.1
<g/>
.2013	.2013	k4	.2013
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Large	Large	k6eAd1	Large
Hadron	Hadron	k1gMnSc1	Hadron
Collider	Collider	k1gMnSc1	Collider
</s>
</p>
<p>
<s>
Urychlovač	urychlovač	k1gInSc1	urychlovač
</s>
</p>
