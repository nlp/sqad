<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc7d3	nejznámější
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
bauxit	bauxit	k1gInSc4	bauxit
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
.	.	kIx.	.
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
(	(	kIx(	(
<g/>
dihydrát	dihydrát	k1gInSc1	dihydrát
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
