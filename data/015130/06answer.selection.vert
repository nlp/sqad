<s desamb="1">
Bavorem	Bavor	k1gMnSc7
a	a	k8xC
Fridrichem	Fridrich	k1gMnSc7
I.	I.	kA
Sličným	sličný	k2eAgFnPc3d1
se	se	k3xPyFc4
přidržoval	přidržovat	k5eAaImAgMnS
Ludvíkovy	Ludvíkův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
Ludvíkově	Ludvíkův	k2eAgInSc6d1
sporu	spor	k1gInSc6
s	s	k7c7
papežem	papež	k1gMnSc7
Janem	Jan	k1gMnSc7
XXII	XXII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
na	na	k7c4
Mikuláše	Mikuláš	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
diecézi	diecéze	k1gFnSc4
uvrhl	uvrhnout	k5eAaPmAgInS
interdikt	interdikt	k1gInSc1
<g/>
.	.	kIx.
</s>