<s>
Mikuláš	Mikuláš	k1gMnSc1
z	z	k7c2
Ybbs	Ybbsa	k1gFnPc2
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
z	z	k7c2
Ybbs	Ybbsa	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
1283	#num#	k4
<g/>
Ybbs	Ybbs	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Donau	donau	k1gInSc7
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1340	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
56	#num#	k4
<g/>
–	–	k?
<g/>
57	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Oberalteich	Oberalteich	k1gInSc1
Abbey	Abbea	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
diecézní	diecézní	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1313	#num#	k4
<g/>
)	)	kIx)
<g/>
katolický	katolický	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
z	z	k7c2
Ybbs	Ybbsa	k1gFnPc2
(	(	kIx(
<g/>
před	před	k7c7
1283	#num#	k4
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1340	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
řezenský	řezenský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
patricijské	patricijský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
z	z	k7c2
Ybbs	Ybbsa	k1gFnPc2
v	v	k7c6
Dolních	dolní	k2eAgInPc6d1
Rakousích	Rakousy	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
práva	právo	k1gNnPc4
a	a	k8xC
působil	působit	k5eAaImAgMnS
v	v	k7c6
kanceláři	kancelář	k1gFnSc6
římskoněmeckého	římskoněmecký	k2eAgMnSc4d1
krále	král	k1gMnSc4
Albrechta	Albrecht	k1gMnSc4
I.	I.	kA
Habsburského	habsburský	k2eAgMnSc4d1
a	a	k8xC
poté	poté	k6eAd1
i	i	k9
v	v	k7c6
kanceláři	kancelář	k1gFnSc6
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
následníka	následník	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucemburského	lucemburský	k2eAgNnSc2d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1310	#num#	k4
a	a	k8xC
1313	#num#	k4
doprovázel	doprovázet	k5eAaImAgMnS
na	na	k7c6
římské	římský	k2eAgFnSc6d1
jízdě	jízda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1313	#num#	k4
skonal	skonat	k5eAaPmAgMnS
<g/>
,	,	kIx,
Mikuláš	Mikuláš	k1gMnSc1
přestoupil	přestoupit	k5eAaPmAgMnS
na	na	k7c4
dvůr	dvůr	k1gInSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
syna	syn	k1gMnSc2
Jana	Jan	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Mikuláš	Mikuláš	k1gMnSc1
při	při	k7c6
působení	působení	k1gNnSc6
v	v	k7c6
královských	královský	k2eAgFnPc6d1
kancelářích	kancelář	k1gFnPc6
získal	získat	k5eAaPmAgInS
nejprve	nejprve	k6eAd1
post	post	k1gInSc1
eichstättského	eichstättský	k2eAgMnSc2d1
a	a	k8xC
řezenského	řezenský	k2eAgMnSc2d1
kanovníka	kanovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1313	#num#	k4
byl	být	k5eAaImAgInS
dokonce	dokonce	k9
jmenován	jmenovat	k5eAaImNgInS,k5eAaBmNgInS
řezenským	řezenský	k2eAgInSc7d1
biskupem	biskup	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
válkách	válka	k1gFnPc6
mezi	mezi	k7c7
dvěma	dva	k4xCgMnPc7
římskoněmeckými	římskoněmecký	k2eAgMnPc7d1
králi	král	k1gMnPc7
Ludvíkem	Ludvík	k1gMnSc7
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorem	Bavor	k1gMnSc7
a	a	k8xC
Fridrichem	Fridrich	k1gMnSc7
I.	I.	kA
Sličným	sličný	k2eAgFnPc3d1
se	se	k3xPyFc4
přidržoval	přidržovat	k5eAaImAgMnS
Ludvíkovy	Ludvíkův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
Ludvíkově	Ludvíkův	k2eAgInSc6d1
sporu	spor	k1gInSc6
s	s	k7c7
papežem	papež	k1gMnSc7
Janem	Jan	k1gMnSc7
XXII	XXII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
na	na	k7c4
Mikuláše	Mikuláš	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
diecézi	diecéze	k1gFnSc4
uvrhl	uvrhnout	k5eAaPmAgInS
interdikt	interdikt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikuláš	Mikuláš	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1340	#num#	k4
a	a	k8xC
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
Klášteře	klášter	k1gInSc6
Oberalteich	Oberalteich	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
významné	významný	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
řezenské	řezenský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
SCHMID	SCHMID	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikolaus	Nikolaus	k1gInSc1
von	von	k1gInSc4
Ybbs	Ybbsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Neue	Neue	k1gFnSc1
Deutsche	Deutsche	k1gFnSc1
Biographie	Biographie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Duncker	Duncker	k1gMnSc1
&	&	k?
Humblot	Humblot	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
428	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
201	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
268	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
BOBKOVÁ	Bobková	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
<g/>
:	:	kIx,
Otec	otec	k1gMnSc1
slavného	slavný	k2eAgMnSc2d1
syna	syn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
581	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
742	#num#	k4
<g/>
-	-	kIx~
<g/>
9342	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ACHT	acht	k1gInSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ein	Ein	k1gMnSc1
Registerbuch	Registerbuch	k1gMnSc1
des	des	k1gNnSc7
Bischofs	Bischofs	k1gInSc1
Nikolaus	Nikolaus	k1gInSc1
von	von	k1gInSc1
Regensburg	Regensburg	k1gInSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
−	−	k?
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
itteilungen	itteilungen	k1gInSc1
des	des	k1gNnSc1
Instituts	Instituts	k1gInSc1
für	für	k?
österreichische	österreichische	k1gInSc1
Geschichtsforschung	Geschichtsforschung	k1gInSc1
<g/>
.	.	kIx.
1951	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
98	#num#	k4
<g/>
–	–	k?
<g/>
117	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BUCHBERGER	BUCHBERGER	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
1200	#num#	k4
Jahre	Jahr	k1gInSc5
Bistum	Bistum	k1gNnSc1
Regensburg	Regensburg	k1gMnSc1
<g/>
:	:	kIx,
Festschrift	Festschrift	k1gMnSc1
zur	zur	k?
Zwölfhundertjahrfeier	Zwölfhundertjahrfeier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regensburg	Regensburg	k1gInSc1
<g/>
:	:	kIx,
Komms	Komms	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verl	Verl	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
.	.	kIx.
307	#num#	k4
s.	s.	k?
S.	S.	kA
40	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HAUSBERGER	HAUSBERGER	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikolaus	Nikolaus	k1gInSc1
von	von	k1gInSc4
Ybbs	Ybbs	k1gInSc4
(	(	kIx(
<g/>
†	†	k?
1340	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1313	#num#	k4
<g/>
–	–	k?
<g/>
1340	#num#	k4
Bischof	Bischof	k1gInSc1
von	von	k1gInSc4
Regensburg	Regensburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
GATZ	GATZ	kA
<g/>
,	,	kIx,
Erwin	Erwin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Bischöfe	Bischöf	k1gInSc5
des	des	k1gNnSc7
Heiligen	Heiligen	k1gInSc1
Römischen	Römischen	k1gInSc1
Reiches	Reiches	k1gInSc1
1198	#num#	k4
bis	bis	k?
1448	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ein	Ein	k1gMnSc1
biographisches	biographisches	k1gMnSc1
Lexikon	lexikon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Duncker	Duncker	k1gMnSc1
&	&	k?
Humblot	Humblot	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
428	#num#	k4
<g/>
-	-	kIx~
<g/>
10303	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
629	#num#	k4
<g/>
–	–	k?
<g/>
631	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MORENZ	MORENZ	kA
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magister	magister	k1gMnSc1
Nikolaus	Nikolaus	k1gMnSc1
von	von	k1gInSc4
Ybbs	Ybbsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verhandlungen	Verhandlungen	k1gInSc1
des	des	k1gNnSc2
Historischen	Historischen	k2eAgInSc1d1
Vereins	Vereins	k1gInSc1
für	für	k?
Oberpfalz	Oberpfalz	k1gInSc1
und	und	k?
Regensburg	Regensburg	k1gInSc1
<g/>
.	.	kIx.
1957	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
98	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
221	#num#	k4
<g/>
–	–	k?
<g/>
308	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
POPP	POPP	kA
<g/>
,	,	kIx,
Marianne	Mariann	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Das	Das	k1gMnSc1
Handbuch	Handbuch	k1gMnSc1
der	drát	k5eAaImRp2nS
Kanzlei	Kanzlei	k1gNnSc2
des	des	k1gNnSc1
Bischofs	Bischofs	k1gInSc1
Nikolaus	Nikolaus	k1gMnSc1
von	von	k1gInSc1
Regensburg	Regensburg	k1gInSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
−	−	k?
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
München	München	k1gInSc1
<g/>
:	:	kIx,
C.H.	C.H.	k1gMnSc1
Beck	Beck	k1gMnSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
STABER	STABER	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kirchengeschichte	Kirchengeschicht	k1gInSc5
des	des	k1gNnSc7
Bistums	Bistums	k1gInSc1
Regensburg	Regensburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regensburg	Regensburg	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
65	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Řezenský	řezenský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Konrád	Konrád	k1gMnSc1
z	z	k7c2
Luppurgu	Luppurg	k1gInSc2
</s>
<s>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1340	#num#	k4
Mikuláš	mikuláš	k1gInSc1
z	z	k7c2
Ybbs	Ybbsa	k1gFnPc2
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Fridrich	Fridrich	k1gMnSc1
Norimberský	norimberský	k2eAgMnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
119470535	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
40189324	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
