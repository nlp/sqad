<s>
Radiometrie	radiometrie	k1gFnSc1	radiometrie
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
měřením	měření	k1gNnSc7	měření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
