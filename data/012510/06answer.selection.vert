<s>
Gravitace	gravitace	k1gFnSc1	gravitace
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgFnSc1d1	gravitační
interakce	interakce	k1gFnSc1	interakce
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgNnSc1d1	univerzální
silové	silový	k2eAgNnSc1d1	silové
působení	působení	k1gNnSc1	působení
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
formami	forma	k1gFnPc7	forma
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
právě	právě	k9	právě
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
interakcí	interakce	k1gFnPc2	interakce
<g/>
.	.	kIx.	.
</s>
