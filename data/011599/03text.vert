<p>
<s>
Eyragues	Eyragues	k1gInSc1	Eyragues
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
departmentu	department	k1gInSc6	department
Bouches-du-Rhône	Bouchesu-Rhôn	k1gInSc5	Bouches-du-Rhôn
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
15	[number]	k4	15
kilometrů	kilometr	k1gInPc2	kilometr
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Maxima	Maxima	k1gFnSc1	Maxima
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
gotickou	gotický	k2eAgFnSc7d1	gotická
apsidou	apsida	k1gFnSc7	apsida
a	a	k8xC	a
se	s	k7c7	s
zvonicí	zvonice	k1gFnSc7	zvonice
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
kaple	kaple	k1gFnSc2	kaple
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnPc4	století
zasvěcená	zasvěcený	k2eAgNnPc4d1	zasvěcené
sv.	sv.	kA	sv.
Bonitovi	Bonita	k1gMnSc3	Bonita
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eyragues	Eyragues	k1gInSc1	Eyragues
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Châteaurenard	Châteaurenard	k1gInSc1	Châteaurenard
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Noves	Noves	k1gInSc1	Noves
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
Saint-Rémy-de-Provence	Saint-Rémye-Provence	k1gFnSc1	Saint-Rémy-de-Provence
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Maillane	Maillan	k1gMnSc5	Maillan
a	a	k8xC	a
s	s	k7c7	s
Graveson	Gravesona	k1gFnPc2	Gravesona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Bouches-du-Rhône	Bouchesu-Rhôn	k1gInSc5	Bouches-du-Rhôn
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eyragues	Eyraguesa	k1gFnPc2	Eyraguesa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Eyraguesu	Eyragues	k1gInSc2	Eyragues
</s>
</p>
