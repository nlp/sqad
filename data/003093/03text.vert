<s>
Orogeneze	orogeneze	k1gFnSc1	orogeneze
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k8xS	jako
horotvorba	horotvorba	k1gFnSc1	horotvorba
či	či	k8xC	či
horotvoření	horotvoření	k1gNnSc1	horotvoření
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
oros	orosit	k5eAaPmRp2nS	orosit
"	"	kIx"	"
<g/>
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
a	a	k8xC	a
genesis	genesis	k1gFnSc1	genesis
"	"	kIx"	"
<g/>
vznik	vznik	k1gInSc1	vznik
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
horotvorný	horotvorný	k2eAgInSc1d1	horotvorný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
pásemných	pásemný	k2eAgFnPc2d1	pásemná
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
vznikajících	vznikající	k2eAgInPc2d1	vznikající
většinou	většinou	k6eAd1	většinou
vlivem	vlivem	k7c2	vlivem
procesů	proces	k1gInPc2	proces
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
a	a	k8xC	a
který	který	k3yQgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
milióny	milión	k4xCgInPc4	milión
až	až	k8xS	až
desítky	desítka	k1gFnPc4	desítka
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Horskou	horský	k2eAgFnSc4d1	horská
soustavu	soustava	k1gFnSc4	soustava
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
orogenezí	orogeneze	k1gFnSc7	orogeneze
nazýváme	nazývat	k5eAaImIp1nP	nazývat
orogén	orogén	k1gInSc4	orogén
<g/>
.	.	kIx.	.
</s>
<s>
Orogeneze	orogeneze	k1gFnSc1	orogeneze
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
dílčích	dílčí	k2eAgFnPc2d1	dílčí
maxim	maxima	k1gFnPc2	maxima
neboli	neboli	k8xC	neboli
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
orogeneze	orogeneze	k1gFnSc2	orogeneze
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
geologických	geologický	k2eAgInPc2d1	geologický
útvarů	útvar	k1gInPc2	útvar
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
flyše	flyš	k1gInSc2	flyš
a	a	k8xC	a
molasy	molasa	k1gFnSc2	molasa
<g/>
.	.	kIx.	.
</s>
<s>
Orogeneze	orogeneze	k1gFnSc1	orogeneze
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
vzájemnými	vzájemný	k2eAgFnPc7d1	vzájemná
kolizemi	kolize	k1gFnPc7	kolize
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
orogenezi	orogeneze	k1gFnSc6	orogeneze
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
litosférické	litosférický	k2eAgFnPc1d1	litosférická
desky	deska	k1gFnPc1	deska
srazí	srazit	k5eAaPmIp3nP	srazit
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
podsouvat	podsouvat	k5eAaImF	podsouvat
pod	pod	k7c7	pod
druhou	druhý	k4xOgFnSc7	druhý
(	(	kIx(	(
<g/>
kolize	kolize	k1gFnPc1	kolize
<g/>
,	,	kIx,	,
subdukce	subdukce	k1gFnPc1	subdukce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
procesu	proces	k1gInSc2	proces
dochází	docházet	k5eAaImIp3nS	docházet
vlivem	vlivem	k7c2	vlivem
ohromných	ohromný	k2eAgInPc2d1	ohromný
tlaků	tlak	k1gInPc2	tlak
a	a	k8xC	a
teplot	teplota	k1gFnPc2	teplota
ke	k	k7c3	k
vrásnění	vrásnění	k1gNnSc3	vrásnění
a	a	k8xC	a
tavení	tavení	k1gNnSc3	tavení
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vyzdvihování	vyzdvihování	k1gNnSc2	vyzdvihování
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
pohořími	pohoří	k1gNnPc7	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
4	[number]	k4	4
základní	základní	k2eAgFnPc1d1	základní
orogenetické	orogenetický	k2eAgFnPc1d1	orogenetický
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
kadomské	kadomský	k2eAgNnSc1d1	kadomské
vrásnění	vrásnění	k1gNnSc1	vrásnění
kaledonské	kaledonský	k2eAgNnSc1d1	kaledonské
vrásnění	vrásnění	k1gNnSc1	vrásnění
hercynské	hercynský	k2eAgNnSc1d1	hercynské
(	(	kIx(	(
<g/>
variské	variský	k2eAgNnSc4d1	variské
<g/>
)	)	kIx)	)
vrásnění	vrásnění	k1gNnSc4	vrásnění
alpinské	alpinský	k2eAgNnSc4d1	alpinské
vrásnění	vrásnění	k1gNnSc4	vrásnění
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
zvané	zvaný	k2eAgFnSc3d1	zvaná
alpsko-himálajské	alpskoimálajský	k2eAgFnSc3d1	alpsko-himálajský
<g/>
)	)	kIx)	)
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
celých	celý	k2eAgFnPc2d1	celá
geologických	geologický	k2eAgFnPc2d1	geologická
epoch	epocha	k1gFnPc2	epocha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
planety	planeta	k1gFnSc2	planeta
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
horotvorným	horotvorný	k2eAgInPc3d1	horotvorný
pohybům	pohyb	k1gInPc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
uvedené	uvedený	k2eAgInPc1d1	uvedený
názvy	název	k1gInPc1	název
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
orogenezi	orogeneze	k1gFnSc4	orogeneze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dané	daný	k2eAgFnSc2d1	daná
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hercynské	hercynský	k2eAgNnSc1d1	hercynské
vrásnění	vrásnění	k1gNnSc1	vrásnění
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
označuje	označovat	k5eAaImIp3nS	označovat
vznik	vznik	k1gInSc4	vznik
hercynských	hercynský	k2eAgNnPc2d1	hercynské
pohoří	pohoří	k1gNnPc2	pohoří
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
Eurameriky	Euramerika	k1gFnSc2	Euramerika
a	a	k8xC	a
Gondwany	Gondwana	k1gFnSc2	Gondwana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
jsou	být	k5eAaImIp3nP	být
hercynského	hercynský	k2eAgNnSc2d1	hercynské
stáří	stáří	k1gNnSc2	stáří
i	i	k8xC	i
mnohá	mnohý	k2eAgNnPc1d1	mnohé
další	další	k2eAgNnPc1d1	další
horstva	horstvo	k1gNnPc1	horstvo
(	(	kIx(	(
<g/>
hercynidy	hercynidy	k1gInPc1	hercynidy
<g/>
)	)	kIx)	)
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
jinými	jiný	k2eAgFnPc7d1	jiná
srážkami	srážka	k1gFnPc7	srážka
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
epoše	epocha	k1gFnSc6	epocha
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
Ural	Ural	k1gInSc1	Ural
nebo	nebo	k8xC	nebo
Ťan-šan	Ťan-šan	k1gInSc1	Ťan-šan
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
dalších	další	k2eAgFnPc2d1	další
orogenezí	orogeneze	k1gFnPc2	orogeneze
<g/>
:	:	kIx,	:
Grenvillské	Grenvillský	k2eAgNnSc1d1	Grenvillský
vrásnění	vrásnění	k1gNnSc1	vrásnění
</s>
