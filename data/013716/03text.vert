<s>
Prérie	prérie	k1gFnSc1
</s>
<s>
Prérie	prérie	k1gFnSc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Dakotě	Dakota	k1gFnSc6
</s>
<s>
Prérie	prérie	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
prairie	prairie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stepní	stepní	k2eAgNnSc1d1
rostlinné	rostlinný	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
a	a	k8xC
západní	západní	k2eAgFnSc6d1
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickým	charakteristický	k2eAgInSc7d1
porostem	porost	k1gInSc7
jsou	být	k5eAaImIp3nP
traviny	travina	k1gFnPc1
vysokého	vysoký	k2eAgInSc2d1
vzrůstu	vzrůst	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prérii	prérie	k1gFnSc4
lze	lze	k6eAd1
také	také	k9
chápat	chápat	k5eAaImF
jako	jako	k9
geomorfologickou	geomorfologický	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
rozkládající	rozkládající	k2eAgFnSc2d1
se	se	k3xPyFc4
od	od	k7c2
Alberty	Alberta	k1gFnSc2
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
až	až	k9
po	po	k7c6
Texas	Texas	kA
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prérie	prérie	k1gFnSc1
má	mít	k5eAaImIp3nS
kontinentální	kontinentální	k2eAgInSc1d1
podnebí	podnebí	k1gNnSc2
s	s	k7c7
horkým	horký	k2eAgNnSc7d1
suchým	suchý	k2eAgNnSc7d1
létem	léto	k1gNnSc7
a	a	k8xC
dlouhou	dlouhý	k2eAgFnSc7d1
suchou	suchý	k2eAgFnSc7d1
zimou	zima	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekosystém	ekosystém	k1gInSc1
</s>
<s>
Půdy	půda	k1gFnPc1
prérií	prérie	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nP
úrodné	úrodný	k2eAgFnPc1d1
černozemě	černozem	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInPc1d1
travinné	travinný	k2eAgInPc1d1
porosty	porost	k1gInPc1
dosahující	dosahující	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
až	až	k9
2	#num#	k4
metrů	metr	k1gInPc2
byly	být	k5eAaImAgInP
osidlováním	osidlování	k1gNnSc7
území	území	k1gNnPc2
a	a	k8xC
zemědělskou	zemědělský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
zničeny	zničen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejrozšířenějším	rozšířený	k2eAgFnPc3d3
prérijním	prérijní	k2eAgFnPc3d1
travinám	travina	k1gFnPc3
vousatky	vousatka	k1gFnSc2
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc2
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnSc2d1
sasanky	sasanka	k1gFnSc2
a	a	k8xC
modré	modrý	k2eAgFnSc2d1
baptisie	baptisie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prérii	prérie	k1gFnSc6
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
kukuřice	kukuřice	k1gFnSc1
<g/>
,	,	kIx,
pšenice	pšenice	k1gFnSc1
<g/>
,	,	kIx,
sója	sója	k1gFnSc1
<g/>
,	,	kIx,
oves	oves	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
oblasti	oblast	k1gFnSc2
pícniny	pícnina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
MATĚJČEK	Matějček	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
geografický	geografický	k2eAgInSc1d1
a	a	k8xC
ekologický	ekologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geografická	geografický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
136	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86034	#num#	k4
<g/>
-	-	kIx~
<g/>
68	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
88	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Universum	universum	k1gNnSc4
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
655	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
1069	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
494	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
prérie	prérie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4047015-5	4047015-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85106080	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85106080	#num#	k4
</s>
