<s>
Titanic	Titanic	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
parníku	parník	k1gInSc6
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
ztroskotání	ztroskotání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Titanic	Titanic	k1gInSc1
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
opouští	opouštět	k5eAaImIp3nS
Southampton	Southampton	k1gInSc4
<g/>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Typ	typa	k1gFnPc2
</s>
<s>
Zaoceánský	zaoceánský	k2eAgInSc1d1
parník	parník	k1gInSc1
–	–	k?
osobní	osobní	k2eAgFnSc1d1
Třída	třída	k1gFnSc1
</s>
<s>
Olympic	Olympic	k1gMnSc1
Majitel	majitel	k1gMnSc1
</s>
<s>
White	Whiit	k5eAaPmRp2nP,k5eAaBmRp2nP,k5eAaImRp2nP
Star	star	k1gFnSc1
Line	linout	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Domovský	domovský	k2eAgInSc1d1
přístav	přístav	k1gInSc1
</s>
<s>
Liverpool	Liverpool	k1gInSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Číslo	číslo	k1gNnSc4
trupu	trup	k1gInSc2
</s>
<s>
401	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
390904	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Stavitel	stavitel	k1gMnSc1
</s>
<s>
Harland	Harland	k1gInSc1
and	and	k?
Wolff	Wolff	k1gInSc1
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnSc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1909	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Spuštěna	spustit	k5eAaPmNgFnS
na	na	k7c4
vodu	voda	k1gFnSc4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1911	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
služby	služba	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
Panenská	panenský	k2eAgFnSc1d1
plavba	plavba	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
potopil	potopit	k5eAaPmAgInS
se	s	k7c7
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
po	po	k7c6
srážce	srážka	k1gFnSc6
s	s	k7c7
ledovcem	ledovec	k1gInSc7
Poloha	poloha	k1gFnSc1
vraku	vrak	k1gInSc2
</s>
<s>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
49	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Sesterské	sesterský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
</s>
<s>
Olympic	Olympice	k1gFnPc2
<g/>
,	,	kIx,
Britannic	Britannice	k1gFnPc2
Technická	technický	k2eAgFnSc1d1
data	datum	k1gNnPc4
Prostornost	prostornost	k1gFnSc1
</s>
<s>
46	#num#	k4
328	#num#	k4
BRT	BRT	k?
(	(	kIx(
<g/>
131	#num#	k4
190	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
Výtlak	výtlak	k1gInSc1
</s>
<s>
52	#num#	k4
310	#num#	k4
t	t	k?
Délka	délka	k1gFnSc1
</s>
<s>
269,9	269,9	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Šířka	šířka	k1gFnSc1
</s>
<s>
28,25	28,25	k4
m	m	kA
Ponor	ponor	k1gInSc1
</s>
<s>
10,54	10,54	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
3	#num#	k4
třílisté	třílistý	k2eAgInPc4d1
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
<g/>
29	#num#	k4
parních	parní	k2eAgInPc2d1
kotlů	kotel	k1gInPc2
<g/>
50	#num#	k4
000	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
37	#num#	k4
MW	MW	kA
<g/>
)	)	kIx)
Palivo	palivo	k1gNnSc1
</s>
<s>
uhlí	uhlí	k1gNnSc1
Rychlost	rychlost	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
43	#num#	k4
<g/>
–	–	k?
<g/>
46	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
Posádka	posádka	k1gFnSc1
</s>
<s>
885	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
/	/	kIx~
944	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Kapacita	kapacita	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
tř	tř	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
833	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
0	#num#	k4
<g/>
34	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
/	/	kIx~
905	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
os	osa	k1gFnPc2
<g/>
.2	.2	k4
<g/>
.	.	kIx.
<g/>
tř	tř	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
510	#num#	k4
<g/>
–	–	k?
<g/>
614	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
/	/	kIx~
564	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
os	osa	k1gFnPc2
<g/>
.3	.3	k4
<g/>
.	.	kIx.
<g/>
tř	tř	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
1	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
0	#num#	k4
<g/>
22	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
/	/	kIx~
1	#num#	k4
134	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
os	osa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
celk	celk	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
2	#num#	k4
453	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
566	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
/	/	kIx~
2	#num#	k4
603	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
os	osa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
P.	P.	kA
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
R.	R.	kA
<g/>
M.	M.	kA
<g/>
S.	S.	kA
Titanic	Titanic	k1gInSc1
<g/>
[	[	kIx(
<g/>
P.	P.	kA
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
tytanyk	tytanyk	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
anglická	anglický	k2eAgFnSc1d1
[	[	kIx(
<g/>
taiˈ	taiˈ	k1gMnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
potopený	potopený	k2eAgInSc1d1
zaoceánský	zaoceánský	k2eAgInSc1d1
parník	parník	k1gInSc1
třídy	třída	k1gFnSc2
Olympic	Olympice	k1gFnPc2
patřící	patřící	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
(	(	kIx(
<g/>
WSL	WSL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
šlo	jít	k5eAaImAgNnS
o	o	k7c4
největší	veliký	k2eAgInSc4d3
parník	parník	k1gInSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
určen	určit	k5eAaPmNgMnS
pro	pro	k7c4
převoz	převoz	k1gInSc4
cestujících	cestující	k1gMnPc2
a	a	k8xC
pošty	pošta	k1gFnSc2
mezi	mezi	k7c7
Evropou	Evropa	k1gFnSc7
a	a	k8xC
Severní	severní	k2eAgFnSc7d1
Amerikou	Amerika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
trase	trasa	k1gFnSc6
měl	mít	k5eAaImAgMnS
konkurovat	konkurovat	k5eAaImF
podobným	podobný	k2eAgInPc3d1
parníkům	parník	k1gInPc3
společnosti	společnost	k1gFnSc2
Cunard	Cunard	k1gInSc4
Line	linout	k5eAaImIp3nS
–	–	k?
Mauretanii	Mauretanie	k1gFnSc4
a	a	k8xC
Lusitanii	Lusitanie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
lodi	loď	k1gFnSc2
dovolovala	dovolovat	k5eAaImAgFnS
převážet	převážet	k5eAaImF
až	až	k9
2	#num#	k4
603	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
omezený	omezený	k2eAgInSc1d1
počet	počet	k1gInSc1
kočárů	kočár	k1gInPc2
nebo	nebo	k8xC
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
provoz	provoz	k1gInSc4
lodě	loď	k1gFnSc2
a	a	k8xC
o	o	k7c4
pohodlí	pohodlí	k1gNnSc4
cestujících	cestující	k1gFnPc2
se	se	k3xPyFc4
staralo	starat	k5eAaImAgNnS
až	až	k9
899	#num#	k4
členů	člen	k1gInPc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Titanic	Titanic	k1gInSc1
však	však	k9
ztroskotal	ztroskotat	k5eAaPmAgInS
již	již	k6eAd1
během	během	k7c2
své	svůj	k3xOyFgFnSc2
první	první	k4xOgFnSc2
plavby	plavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
ve	v	k7c4
23	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
se	se	k3xPyFc4
parník	parník	k1gInSc1
srazil	srazit	k5eAaPmAgInS
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
necelých	celý	k2eNgFnPc6d1
třech	tři	k4xCgFnPc6
hodinách	hodina	k1gFnPc6
<g/>
,	,	kIx,
nad	nad	k7c7
ránem	ráno	k1gNnSc7
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
v	v	k7c6
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
,	,	kIx,
klesl	klesnout	k5eAaPmAgInS
ke	k	k7c3
dnu	dno	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahynulo	zahynout	k5eAaPmAgNnS
přes	přes	k7c4
1	#num#	k4
500	#num#	k4
cestujících	cestující	k1gMnPc2
a	a	k8xC
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
Příčinou	příčina	k1gFnSc7
srážky	srážka	k1gFnSc2
byla	být	k5eAaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
nedostatečně	dostatečně	k6eNd1
jižně	jižně	k6eAd1
vychýlený	vychýlený	k2eAgInSc1d1
kurz	kurz	k1gInSc1
určený	určený	k2eAgInSc1d1
kpt.	kpt.	k?
Smithem	Smith	k1gInSc7
<g/>
,	,	kIx,
nevhodné	vhodný	k2eNgFnPc4d1
světelné	světelný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
a	a	k8xC
absence	absence	k1gFnPc4
pátracího	pátrací	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
zákazu	zákaz	k1gInSc2
pro	pro	k7c4
britské	britský	k2eAgFnPc4d1
civilní	civilní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
vysokého	vysoký	k2eAgInSc2d1
počtu	počet	k1gInSc2
obětí	oběť	k1gFnPc2
byla	být	k5eAaImAgFnS
zejména	zejména	k9
špatná	špatný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
záchranných	záchranný	k2eAgFnPc2d1
prací	práce	k1gFnPc2
a	a	k8xC
nedostatek	nedostatek	k1gInSc1
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
moderním	moderní	k2eAgInSc7d1
<g/>
,	,	kIx,
ale	ale	k8xC
stále	stále	k6eAd1
nedostatečným	dostatečný	k2eNgInSc7d1
stavem	stav	k1gInSc7
technologie	technologie	k1gFnSc2
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zkáze	zkáza	k1gFnSc3
Titanicu	Titanicus	k1gInSc2
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
široké	široký	k2eAgFnPc4d1
publicity	publicita	k1gFnPc4
pro	pro	k7c4
velký	velký	k2eAgInSc4d1
počet	počet	k1gInSc4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
byli	být	k5eAaImAgMnP
i	i	k9
někteří	některý	k3yIgMnPc1
z	z	k7c2
nejbohatších	bohatý	k2eAgMnPc2d3
lidí	člověk	k1gMnPc2
světa	svět	k1gInSc2
a	a	k8xC
mnoho	mnoho	k4c4
známých	známý	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
,	,	kIx,
i	i	k8xC
kvůli	kvůli	k7c3
legendám	legenda	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
kolem	kolem	k7c2
příčiny	příčina	k1gFnSc2
a	a	k8xC
průběhu	průběh	k1gInSc2
potopení	potopení	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
později	pozdě	k6eAd2
také	také	k9
po	po	k7c6
objevení	objevení	k1gNnSc6
zachovalého	zachovalý	k2eAgInSc2d1
vraku	vrak	k1gInSc2
lodi	loď	k1gFnSc2
v	v	k7c6
hlubinách	hlubina	k1gFnPc6
Atlantiku	Atlantik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Potopení	potopení	k1gNnSc1
Titaniku	Titanic	k1gInSc2
má	mít	k5eAaImIp3nS
rovněž	rovněž	k9
velký	velký	k2eAgInSc4d1
technický	technický	k2eAgInSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgInSc4d1
význam	význam	k1gInSc4
díky	díky	k7c3
značným	značný	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
bezpečnosti	bezpečnost	k1gFnSc6
lodní	lodní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
tuto	tento	k3xDgFnSc4
katastrofu	katastrofa	k1gFnSc4
provedeny	provést	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
a	a	k8xC
stavba	stavba	k1gFnSc1
</s>
<s>
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
kresba	kresba	k1gFnSc1
</s>
<s>
Model	model	k1gInSc1
Titanicu	Titanicus	k1gInSc2
na	na	k7c6
výstavě	výstava	k1gFnSc6
</s>
<s>
Srovnání	srovnání	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
(	(	kIx(
<g/>
tmavý	tmavý	k2eAgInSc1d1
obrys	obrys	k1gInSc1
<g/>
)	)	kIx)
<g/>
s	s	k7c7
Queen	Queen	k1gInSc1
Mary	Mary	k1gFnSc1
2	#num#	k4
a	a	k8xC
dalšími	další	k2eAgInPc7d1
objekty	objekt	k1gInPc7
</s>
<s>
Thomas	Thomas	k1gMnSc1
Andrews	Andrewsa	k1gFnPc2
<g/>
,	,	kIx,
šéfkonstruktér	šéfkonstruktér	k1gMnSc1
</s>
<s>
Příď	příď	k1gFnSc1
Titanicu	Titanicus	k1gInSc2
při	při	k7c6
stavbě	stavba	k1gFnSc6
v	v	k7c6
loděnicích	loděnice	k1gFnPc6
</s>
<s>
Levý	levý	k2eAgInSc1d1
a	a	k8xC
střední	střední	k2eAgInSc1d1
lodní	lodní	k2eAgInSc1d1
šroub	šroub	k1gInSc1
sesterské	sesterský	k2eAgFnSc2d1
RMS	RMS	kA
Olympic	Olympic	k1gMnSc1
–	–	k?
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
byl	být	k5eAaImAgInS
i	i	k9
prostřední	prostřední	k2eAgInSc1d1
šroub	šroub	k1gInSc1
třílistý	třílistý	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
Belfastu	Belfast	k1gInSc6
v	v	k7c6
loděnicích	loděnice	k1gFnPc6
Harland	Harland	k1gInSc1
&	&	k?
Wolff	Wolff	k1gInSc1
na	na	k7c4
objednávku	objednávka	k1gFnSc4
společnosti	společnost	k1gFnSc2
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
druhý	druhý	k4xOgMnSc1
ze	z	k7c2
série	série	k1gFnSc2
tří	tři	k4xCgFnPc2
lodí	loď	k1gFnPc2
třídy	třída	k1gFnSc2
Olympic	Olympic	k1gMnSc1
(	(	kIx(
<g/>
RMS	RMS	kA
Olympic	Olympic	k1gMnSc1
<g/>
,	,	kIx,
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
a	a	k8xC
HMHS	HMHS	kA
Britannic	Britannice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
těchto	tento	k3xDgFnPc2
lodí	loď	k1gFnPc2
byla	být	k5eAaImAgFnS
odpovědí	odpověď	k1gFnSc7
konkurenční	konkurenční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Cunard	Cunard	k1gMnSc1
Line	linout	k5eAaImIp3nS
vlastnící	vlastnící	k2eAgInPc4d1
parníky	parník	k1gInPc4
RMS	RMS	kA
Lusitania	Lusitanium	k1gNnSc2
a	a	k8xC
RMS	RMS	kA
Mauretania	Mauretanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc4
Titanicu	Titanicus	k1gInSc2
navrhl	navrhnout	k5eAaPmAgMnS
Lord	lord	k1gMnSc1
Pirrie	Pirrie	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
ředitelem	ředitel	k1gMnSc7
loděnic	loděnice	k1gFnPc2
Harland	Harland	k1gInSc1
and	and	k?
Wolff	Wolff	k1gInSc1
i	i	k8xC
plavební	plavební	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
White	Whit	k1gInSc5
Star	Star	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stavbě	stavba	k1gFnSc6
a	a	k8xC
konstrukci	konstrukce	k1gFnSc6
pracoval	pracovat	k5eAaImAgMnS
šéfkonstruktér	šéfkonstruktér	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Andrews	Andrews	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
stavbyvedoucí	stavbyvedoucí	k1gMnSc1
a	a	k8xC
vedoucí	vedoucí	k1gMnSc1
konstrukce	konstrukce	k1gFnSc2
Alexandr	Alexandr	k1gMnSc1
Carlisle	Carlisle	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
generální	generální	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
a	a	k8xC
vedoucí	vedoucí	k1gMnSc1
projektant	projektant	k1gMnSc1
odpovědný	odpovědný	k2eAgMnSc1d1
nejen	nejen	k6eAd1
za	za	k7c4
výzdobu	výzdoba	k1gFnSc4
a	a	k8xC
interiér	interiér	k1gInSc4
plavidla	plavidlo	k1gNnSc2
a	a	k8xC
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
taktéž	taktéž	k?
navrhl	navrhnout	k5eAaPmAgInS
mechanismus	mechanismus	k1gInSc1
spouštění	spouštění	k1gNnSc2
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carlisle	Carlisle	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
ale	ale	k9
odešel	odejít	k5eAaPmAgMnS
ke	k	k7c3
společnosti	společnost	k1gFnSc3
Welin	Welin	k2eAgInSc1d1
Davit	Davit	k1gInSc1
&	&	k?
Engineering	Engineering	k1gInSc1
Company	Compana	k1gFnSc2
Ltd	ltd	kA
<g/>
.	.	kIx.
a	a	k8xC
dalších	další	k2eAgFnPc2d1
prací	práce	k1gFnPc2
se	se	k3xPyFc4
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
neúčastnil	účastnit	k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
byl	být	k5eAaImAgInS
269,1	269,1	k4
metrů	metr	k1gInPc2
dlouhý	dlouhý	k2eAgMnSc1d1
a	a	k8xC
28,25	28,25	k4
metrů	metr	k1gInPc2
široký	široký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrubá	Hrubá	k1gFnSc1
tonáž	tonáž	k1gFnSc1
představovala	představovat	k5eAaImAgFnS
46	#num#	k4
328	#num#	k4
BRT	BRT	k?
(	(	kIx(
<g/>
131	#num#	k4
190	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
výtlak	výtlak	k1gInSc1
52	#num#	k4
310	#num#	k4
tun	tuna	k1gFnPc2
při	při	k7c6
maximálním	maximální	k2eAgInSc6d1
ponoru	ponor	k1gInSc6
10,54	10,54	k4
m.	m.	k?
Výška	výška	k1gFnSc1
paluby	paluba	k1gFnSc2
od	od	k7c2
čáry	čára	k1gFnSc2
ponoru	ponor	k1gInSc2
byla	být	k5eAaImAgFnS
18	#num#	k4
m.	m.	k?
Tyto	tento	k3xDgFnPc1
hodnoty	hodnota	k1gFnPc1
vypovídají	vypovídat	k5eAaPmIp3nP,k5eAaImIp3nP
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Titanic	Titanic	k1gInSc1
byl	být	k5eAaImAgInS
větší	veliký	k2eAgFnSc4d2
než	než	k8xS
jeho	jeho	k3xOp3gMnSc1
předchůdce	předchůdce	k1gMnSc1
Olympic	Olympic	k1gMnSc1
a	a	k8xC
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
největším	veliký	k2eAgNnSc7d3
námořním	námořní	k2eAgNnSc7d1
plavidlem	plavidlo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Trup	trup	k1gInSc1
byl	být	k5eAaImAgInS
z	z	k7c2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
16	#num#	k4
záplavových	záplavový	k2eAgFnPc2d1
komor	komora	k1gFnPc2
a	a	k8xC
dno	dno	k1gNnSc1
lodi	loď	k1gFnSc2
bylo	být	k5eAaImAgNnS
zdvojeno	zdvojit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
konstrukce	konstrukce	k1gFnSc1
měla	mít	k5eAaImAgFnS
Titanicu	Titanic	k1gMnSc3
zajistit	zajistit	k5eAaPmF
nejvyšší	vysoký	k2eAgFnSc4d3
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
představě	představa	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nepotopitelný	potopitelný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
domněnku	domněnka	k1gFnSc4
zveličil	zveličit	k5eAaPmAgInS
i	i	k9
odborný	odborný	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Shipbuilder	Shipbuilder	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Po	po	k7c6
uzavření	uzavření	k1gNnSc6
vodotěsných	vodotěsný	k2eAgFnPc2d1
dveří	dveře	k1gFnPc2
v	v	k7c6
přepážkách	přepážka	k1gFnPc6
se	se	k3xPyFc4
loď	loď	k1gFnSc1
stává	stávat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
nepotopitelnou	potopitelný	k2eNgFnSc4d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
udržet	udržet	k5eAaPmF
na	na	k7c6
hladině	hladina	k1gFnSc6
i	i	k8xC
v	v	k7c6
případě	případ	k1gInSc6
zaplavení	zaplavení	k1gNnSc2
až	až	k9
4	#num#	k4
vodotěsných	vodotěsný	k2eAgFnPc2d1
komor	komora	k1gFnPc2
současně	současně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Toto	tento	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
kořen	kořen	k1gInSc4
v	v	k7c6
kontrastováním	kontrastování	k1gNnPc3
s	s	k7c7
dodnes	dodnes	k6eAd1
používaným	používaný	k2eAgInSc7d1
dvojkomorovým	dvojkomorový	k2eAgInSc7d1
standardem	standard	k1gInSc7
a	a	k8xC
dříve	dříve	k6eAd2
používaným	používaný	k2eAgInSc7d1
systémem	systém	k1gInSc7
horizontálních	horizontální	k2eAgFnPc2d1
vodotěsných	vodotěsný	k2eAgFnPc2d1
dveří	dveře	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
srážce	srážka	k1gFnSc6
lodí	loď	k1gFnPc2
byl	být	k5eAaImAgMnS
úder	úder	k1gInSc4
na	na	k7c4
přepážku	přepážka	k1gFnSc4
schopen	schopen	k2eAgMnSc1d1
ohrozit	ohrozit	k5eAaPmF
jen	jen	k6eAd1
dvě	dva	k4xCgFnPc1
komory	komora	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
selhání	selhání	k1gNnSc3
-	-	kIx~
často	často	k6eAd1
manuálně	manuálně	k6eAd1
operovaných	operovaný	k2eAgFnPc2d1
-	-	kIx~
dveří	dveře	k1gFnPc2
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
stěny	stěna	k1gFnPc1
jedné	jeden	k4xCgFnSc2
komory	komora	k1gFnSc2
se	se	k3xPyFc4
loď	loď	k1gFnSc1
stejně	stejně	k6eAd1
potopila	potopit	k5eAaPmAgFnS
(	(	kIx(
<g/>
Empress	Empressa	k1gFnPc2
of	of	k?
Ireland	Irelanda	k1gFnPc2
<g/>
,	,	kIx,
Luisitania	Luisitanium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
měl	mít	k5eAaImAgInS
naopak	naopak	k6eAd1
na	na	k7c6
většině	většina	k1gFnSc6
míst	místo	k1gNnPc2
tříkomorový	tříkomorový	k2eAgMnSc1d1
(	(	kIx(
<g/>
místy	místy	k6eAd1
dvojkomorový	dvojkomorový	k2eAgInSc1d1
<g/>
,	,	kIx,
čtyřkomorový	čtyřkomorový	k2eAgInSc1d1
standard	standard	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
automatické	automatický	k2eAgNnSc1d1
<g/>
,	,	kIx,
vertikálně	vertikálně	k6eAd1
se	se	k3xPyFc4
zavírající	zavírající	k2eAgFnPc1d1
dveře	dveře	k1gFnPc1
v	v	k7c6
kombinací	kombinace	k1gFnSc7
s	s	k7c7
dosednutím	dosednutí	k1gNnSc7
volným	volný	k2eAgInSc7d1
pádem	pád	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
šel	jít	k5eAaImAgInS
vysoce	vysoce	k6eAd1
nad	nad	k7c4
rámec	rámec	k1gInSc4
norem	norma	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
tedy	tedy	k9
vskutku	vskutku	k9
nepotopitelný	potopitelný	k2eNgInSc1d1
(	(	kIx(
<g/>
v	v	k7c6
kontextu	kontext	k1gInSc6
srážky	srážka	k1gFnPc4
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
dominantní	dominantní	k2eAgInSc4d1
důvod	důvod	k1gInSc4
potopení	potopení	k1gNnSc2
lodí	loď	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
inženýrů	inženýr	k1gMnPc2
ale	ale	k8xC
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
věděla	vědět	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
loď	loď	k1gFnSc1
nejde	jít	k5eNaImIp3nS
uchránit	uchránit	k5eAaPmF
před	před	k7c7
srážkou	srážka	k1gFnSc7
s	s	k7c7
ledovcem	ledovec	k1gInSc7
(	(	kIx(
<g/>
i	i	k9
malé	malý	k2eAgInPc1d1
ledovce	ledovec	k1gInPc1
váží	vážit	k5eAaImIp3nP
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
největší	veliký	k2eAgFnPc1d3
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgInSc3
nelze	lze	k6eNd1
ledovec	ledovec	k1gInSc4
odhodit	odhodit	k5eAaPmF
srážkou	srážka	k1gFnSc7
a	a	k8xC
hrozí	hrozit	k5eAaImIp3nS
tedy	tedy	k9
penetrace	penetrace	k1gFnSc1
celého	celý	k2eAgInSc2d1
boku	bok	k1gInSc2
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
při	při	k7c6
takové	takový	k3xDgFnSc6
srážce	srážka	k1gFnSc6
hrozí	hrozit	k5eAaImIp3nS
kompletní	kompletní	k2eAgFnSc1d1
deformace	deformace	k1gFnSc1
rámu	rám	k1gInSc2
lodi	loď	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
nezavření	nezavření	k1gNnSc1
se	s	k7c7
dveří	dveře	k1gFnPc2
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
při	při	k7c6
přímém	přímý	k2eAgInSc6d1
nárazu	náraz	k1gInSc6
tak	tak	k6eAd1
i	i	k9
při	při	k7c6
přejetí	přejetí	k1gNnSc6
nad	nad	k7c7
nízce	nízce	k6eAd1
potopeným	potopený	k2eAgInSc7d1
lemem	lem	k1gInSc7
ledovce	ledovec	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
dveře	dveře	k1gFnPc4
Titanicu	Titanicus	k1gInSc2
pevně	pevně	k6eAd1
jely	jet	k5eAaImAgFnP
v	v	k7c6
drážkách	drážka	k1gFnPc6
přepážek	přepážka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
pevně	pevně	k6eAd1
spojeny	spojit	k5eAaPmNgInP
s	s	k7c7
bokem	bok	k1gInSc7
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
osud	osud	k1gInSc1
(	(	kIx(
<g/>
zkřížení	zkřížení	k1gNnSc1
rámu	rám	k1gInSc2
a	a	k8xC
nedovření	nedovření	k1gNnSc1
se	s	k7c7
dveří	dveře	k1gFnPc2
<g/>
)	)	kIx)
potopil	potopit	k5eAaPmAgInS
během	během	k7c2
hodiny	hodina	k1gFnSc2
sesterskou	sesterský	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Titanicu	Titanicus	k1gInSc2
–	–	k?
Britannic	Britannice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
pohon	pohon	k1gInSc4
lodních	lodní	k2eAgInPc2d1
šroubů	šroub	k1gInPc2
<g/>
,	,	kIx,
pomocných	pomocný	k2eAgInPc2d1
agregátů	agregát	k1gInPc2
a	a	k8xC
dodávku	dodávka	k1gFnSc4
energie	energie	k1gFnSc2
bylo	být	k5eAaImAgNnS
instalováno	instalovat	k5eAaBmNgNnS
celkem	celkem	k6eAd1
29	#num#	k4
parních	parní	k2eAgInPc2d1
kotlů	kotel	k1gInPc2
(	(	kIx(
<g/>
24	#num#	k4
oboustranných	oboustranný	k2eAgInPc2d1
parních	parní	k2eAgInPc2d1
kotlů	kotel	k1gInPc2
se	s	k7c7
6	#num#	k4
topeništi	topeniště	k1gNnPc7
a	a	k8xC
5	#num#	k4
jednostranných	jednostranný	k2eAgInPc2d1
kotlů	kotel	k1gInPc2
se	s	k7c7
3	#num#	k4
topeništi	topeniště	k1gNnPc7
<g/>
)	)	kIx)
v	v	k7c6
6	#num#	k4
samostatných	samostatný	k2eAgFnPc6d1
kotelnách	kotelna	k1gFnPc6
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
se	s	k7c7
159	#num#	k4
topeništi	topeniště	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
kotle	kotel	k1gInSc2
měly	mít	k5eAaImAgFnP
průměr	průměr	k1gInSc4
4,79	4,79	k4
m.	m.	k?
Délka	délka	k1gFnSc1
oboustranných	oboustranný	k2eAgInPc2d1
kotlů	kotel	k1gInPc2
byla	být	k5eAaImAgFnS
6,08	6,08	k4
m	m	kA
a	a	k8xC
jednostranných	jednostranný	k2eAgInPc2d1
3,5	3,5	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Kouř	kouř	k1gInSc1
z	z	k7c2
kotlů	kotel	k1gInPc2
byl	být	k5eAaImAgInS
odváděn	odvádět	k5eAaImNgInS
do	do	k7c2
tří	tři	k4xCgFnPc2
<g/>
,	,	kIx,
19	#num#	k4
metrů	metr	k1gInPc2
vysokých	vysoký	k2eAgInPc2d1
<g/>
,	,	kIx,
komínů	komín	k1gInPc2
s	s	k7c7
přirozeným	přirozený	k2eAgInSc7d1
tahem	tah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
ventilátorů	ventilátor	k1gInPc2
se	se	k3xPyFc4
vháněl	vhánět	k5eAaImAgInS
vzduch	vzduch	k1gInSc1
pod	pod	k7c7
rošty	rošt	k1gInPc7
topeniště	topeniště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
tří	tři	k4xCgInPc2
hlavních	hlavní	k2eAgInPc2d1
komínů	komín	k1gInPc2
byl	být	k5eAaImAgInS
instalován	instalovat	k5eAaBmNgInS
i	i	k9
jeden	jeden	k4xCgInSc1
falešný	falešný	k2eAgInSc1d1
komín	komín	k1gInSc1
na	na	k7c6
zádi	záď	k1gFnSc6
sloužící	sloužící	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgInSc1d1
vývod	vývod	k1gInSc1
ventilace	ventilace	k1gFnSc2
a	a	k8xC
komín	komín	k1gInSc4
kuchyně	kuchyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc4
pak	pak	k6eAd1
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
komíny	komín	k1gInPc7
vypadala	vypadat	k5eAaPmAgFnS,k5eAaImAgFnS
větší	veliký	k2eAgFnSc1d2
a	a	k8xC
důstojnější	důstojný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kotlích	kotel	k1gInPc6
se	se	k3xPyFc4
vyvíjela	vyvíjet	k5eAaImAgFnS
pára	pára	k1gFnSc1
pro	pro	k7c4
2	#num#	k4
čtyřválcové	čtyřválcový	k2eAgInPc4d1
sdružené	sdružený	k2eAgInPc4d1
parní	parní	k2eAgInPc4d1
stroje	stroj	k1gInPc4
s	s	k7c7
třístupňovou	třístupňový	k2eAgFnSc7d1
postupnou	postupný	k2eAgFnSc7d1
expanzí	expanze	k1gFnSc7
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
o	o	k7c6
výkonu	výkon	k1gInSc6
15	#num#	k4
000	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
11	#num#	k4
MW	MW	kA
<g/>
)	)	kIx)
s	s	k7c7
možností	možnost	k1gFnSc7
reverzace	reverzace	k1gFnSc2
chodu	chod	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jednu	jeden	k4xCgFnSc4
nízkotlakou	nízkotlaký	k2eAgFnSc4d1
turbínu	turbína	k1gFnSc4
o	o	k7c6
výkonu	výkon	k1gInSc6
16	#num#	k4
000	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
12	#num#	k4
MW	MW	kA
<g/>
)	)	kIx)
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
reverzace	reverzace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Parní	parní	k2eAgFnSc1d1
turbína	turbína	k1gFnSc1
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
páté	pátý	k4xOgFnSc6
vodotěsné	vodotěsný	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
od	od	k7c2
zádi	záď	k1gFnSc2
<g/>
,	,	kIx,
pístové	pístový	k2eAgInPc1d1
stroje	stroj	k1gInPc1
v	v	k7c6
šesté	šestý	k4xOgFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Registrovaný	registrovaný	k2eAgInSc1d1
výkon	výkon	k1gInSc1
byl	být	k5eAaImAgInS
50	#num#	k4
000	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
37	#num#	k4
MW	MW	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
sdružený	sdružený	k2eAgInSc1d1
výkon	výkon	k1gInSc1
všech	všecek	k3xTgNnPc2
soustrojí	soustrojí	k1gNnPc2
byl	být	k5eAaImAgInS
odhadován	odhadovat	k5eAaImNgInS
až	až	k9
na	na	k7c4
55	#num#	k4
000	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
41	#num#	k4
MW	MW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Loď	loď	k1gFnSc1
měla	mít	k5eAaImAgFnS
tři	tři	k4xCgInPc4
třílisté	třílistý	k2eAgInPc4d1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
lodní	lodní	k2eAgInPc1d1
šrouby	šroub	k1gInPc1
<g/>
,	,	kIx,
krajní	krajní	k2eAgInPc1d1
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
38	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
lodi	loď	k1gFnSc2
byla	být	k5eAaImAgFnS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
24	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
44	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tou	ten	k3xDgFnSc7
však	však	k9
Titanic	Titanic	k1gInSc1
nikdy	nikdy	k6eAd1
neplul	plout	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kormidlo	kormidlo	k1gNnSc4
měl	mít	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
bylo	být	k5eAaImAgNnS
krátké	krátké	k1gNnSc1
a	a	k8xC
malé	malá	k1gFnPc1
plochou	plocha	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P.	P.	kA
4	#num#	k4
<g/>
]	]	kIx)
Kormidlo	kormidlo	k1gNnSc1
se	se	k3xPyFc4
pohybovalo	pohybovat	k5eAaImAgNnS
ve	v	k7c6
čtvrtkruhu	čtvrtkruh	k1gInSc6
(	(	kIx(
<g/>
±	±	k?
<g/>
45	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
a	a	k8xC
bylo	být	k5eAaImAgNnS
ovládáno	ovládat	k5eAaImNgNnS
převodem	převod	k1gInSc7
přes	přes	k7c4
ozubené	ozubený	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
omezení	omezení	k1gNnSc4
vibrací	vibrace	k1gFnPc2
vznikajících	vznikající	k2eAgFnPc2d1
prouděním	proudění	k1gNnSc7
vody	voda	k1gFnSc2
za	za	k7c7
lodními	lodní	k2eAgInPc7d1
šrouby	šroub	k1gInPc7
a	a	k8xC
pro	pro	k7c4
případ	případ	k1gInSc4
rychlé	rychlý	k2eAgFnSc2d1
změny	změna	k1gFnSc2
polohy	poloha	k1gFnSc2
byly	být	k5eAaImAgInP
do	do	k7c2
mechanismu	mechanismus	k1gInSc2
vloženy	vložen	k2eAgFnPc1d1
tuhé	tuhý	k2eAgFnPc1d1
pružiny	pružina	k1gFnPc1
<g/>
,	,	kIx,
tlumící	tlumící	k2eAgNnSc1d1
tyto	tento	k3xDgFnPc4
vibrace	vibrace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kormidlo	kormidlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
ovládáno	ovládat	k5eAaImNgNnS
dvěma	dva	k4xCgInPc7
samostatnými	samostatný	k2eAgInPc7d1
parními	parní	k2eAgInPc7d1
stroji	stroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
provozu	provoz	k1gInSc6
byl	být	k5eAaImAgInS
ale	ale	k9
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgMnSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
byl	být	k5eAaImAgInS
záložní	záložní	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
pracovaly	pracovat	k5eAaImAgFnP
na	na	k7c4
společný	společný	k2eAgInSc4d1
hřídel	hřídel	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
případě	případ	k1gInSc6
nouze	nouze	k1gFnSc2
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
kormidlo	kormidlo	k1gNnSc1
ovládat	ovládat	k5eAaImF
lanovými	lanový	k2eAgInPc7d1
převody	převod	k1gInPc7
přes	přes	k7c4
dva	dva	k4xCgInPc4
vrátky	vrátek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Kormidlo	kormidlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
umístěno	umístit	k5eAaPmNgNnS
u	u	k7c2
lodního	lodní	k2eAgInSc2d1
šroubu	šroub	k1gInSc2
poháněného	poháněný	k2eAgInSc2d1
parní	parní	k2eAgInPc4d1
turbínou	turbína	k1gFnSc7
neumožňující	umožňující	k2eNgFnSc7d1
reverzaci	reverzace	k1gFnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
při	při	k7c6
zpětném	zpětný	k2eAgInSc6d1
chodu	chod	k1gInSc6
nebylo	být	k5eNaImAgNnS
obtékáno	obtékat	k5eAaImNgNnS
vodou	voda	k1gFnSc7
a	a	k8xC
bylo	být	k5eAaImAgNnS
nefunkční	funkční	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
stavbou	stavba	k1gFnSc7
RMS	RMS	kA
Titanicu	Titanicum	k1gNnSc3
bylo	být	k5eAaImAgNnS
započato	započnout	k5eAaPmNgNnS
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1909	#num#	k4
a	a	k8xC
stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
financována	financovat	k5eAaBmNgFnS
J.	J.	kA
P.	P.	kA
Morganem	morgan	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnPc7
společností	společnost	k1gFnSc7
International	International	k1gMnSc5
Mercantile	Mercantil	k1gMnSc5
Marine	Marin	k1gMnSc5
Co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trup	trup	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c4
vodu	voda	k1gFnSc4
spuštěn	spustit	k5eAaPmNgInS
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1911	#num#	k4
ve	v	k7c4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
práce	práce	k1gFnPc4
na	na	k7c4
dokončení	dokončení	k1gNnSc4
trvaly	trvat	k5eAaImAgFnP
téměř	téměř	k6eAd1
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
a	a	k8xC
byly	být	k5eAaImAgInP
dokončeny	dokončit	k5eAaPmNgInP
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1912	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
následovala	následovat	k5eAaImAgFnS
zkušební	zkušební	k2eAgFnSc1d1
plavba	plavba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vybavení	vybavení	k1gNnSc1
</s>
<s>
Komfort	komfort	k1gInSc1
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
chodby	chodba	k1gFnSc2
na	na	k7c4
Titanicu	Titanica	k1gFnSc4
</s>
<s>
Le	Le	k?
Café	café	k1gNnSc1
Parisien	Parisino	k1gNnPc2
<g/>
,	,	kIx,
Pařížská	pařížský	k2eAgFnSc1d1
kavárna	kavárna	k1gFnSc1
</s>
<s>
Přední	přední	k2eAgNnSc4d1
velké	velký	k2eAgNnSc4d1
schodiště	schodiště	k1gNnSc4
v	v	k7c6
první	první	k4xOgFnSc6
třídě	třída	k1gFnSc6
</s>
<s>
Luxusní	luxusní	k2eAgFnSc1d1
kajuta	kajuta	k1gFnSc1
B59	B59	k1gFnSc2
</s>
<s>
Tělocvična	tělocvična	k1gFnSc1
</s>
<s>
Již	již	k6eAd1
při	při	k7c6
návrhu	návrh	k1gInSc6
interiéru	interiér	k1gInSc2
a	a	k8xC
vybavení	vybavení	k1gNnSc2
pro	pro	k7c4
Titanic	Titanic	k1gInSc4
se	se	k3xPyFc4
počítalo	počítat	k5eAaImAgNnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
představovat	představovat	k5eAaImF
luxus	luxus	k1gInSc4
a	a	k8xC
bohatství	bohatství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
byla	být	k5eAaImAgFnS
vybavena	vybavit	k5eAaPmNgFnS
palubním	palubní	k2eAgInSc7d1
telefonním	telefonní	k2eAgInSc7d1
komunikačním	komunikační	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
knihovnou	knihovna	k1gFnSc7
i	i	k8xC
holičstvím	holičství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
cestující	cestující	k1gFnSc4
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
byl	být	k5eAaImAgInS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k8xC
bazén	bazén	k1gInSc1
<g/>
,	,	kIx,
tělocvična	tělocvična	k1gFnSc1
<g/>
,	,	kIx,
hřiště	hřiště	k1gNnSc1
na	na	k7c4
squash	squash	k1gInSc4
<g/>
,	,	kIx,
turecké	turecký	k2eAgFnPc1d1
lázně	lázeň	k1gFnPc1
<g/>
,	,	kIx,
elektrické	elektrický	k2eAgFnPc1d1
lázně	lázeň	k1gFnPc1
a	a	k8xC
Veranda	veranda	k1gFnSc1
Cafe	Cafe	k1gFnSc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Taktéž	Taktéž	k?
Café	café	k1gNnPc2
Parisien	Parisino	k1gNnPc2
nabízelo	nabízet	k5eAaImAgNnS
speciality	specialita	k1gFnPc4
pro	pro	k7c4
cestující	cestující	k2eAgFnPc4d1
první	první	k4xOgFnPc4
třídy	třída	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
mohli	moct	k5eAaImAgMnP
konzumovat	konzumovat	k5eAaBmF
na	na	k7c6
prosluněné	prosluněný	k2eAgFnSc6d1
verandě	veranda	k1gFnSc6
s	s	k7c7
dekorovaným	dekorovaný	k2eAgNnSc7d1
mřížovím	mřížoví	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Společenské	společenský	k2eAgFnSc2d1
místnosti	místnost	k1gFnSc2
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
byly	být	k5eAaImAgInP
obloženy	obložit	k5eAaPmNgInP
vyřezávaným	vyřezávaný	k2eAgNnSc7d1
dřevěným	dřevěný	k2eAgNnSc7d1
obložením	obložení	k1gNnSc7
<g/>
,	,	kIx,
drahým	drahý	k2eAgInSc7d1
nábytkem	nábytek	k1gInSc7
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
dekoracemi	dekorace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
třetí	třetí	k4xOgFnSc6
třídě	třída	k1gFnSc6
bylo	být	k5eAaImAgNnS
vybavení	vybavení	k1gNnSc1
skromné	skromný	k2eAgNnSc1d1
<g/>
,	,	kIx,
borovicové	borovicový	k2eAgNnSc1d1
obložení	obložení	k1gNnSc1
a	a	k8xC
robustní	robustní	k2eAgInSc1d1
teakový	teakový	k2eAgInSc1d1
nábytek	nábytek	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
rozdílu	rozdíl	k1gInSc6
vybavenosti	vybavenost	k1gFnSc2
kajut	kajuta	k1gFnPc2
a	a	k8xC
poskytovaného	poskytovaný	k2eAgInSc2d1
komfortu	komfort	k1gInSc2
svědčí	svědčit	k5eAaImIp3nS
i	i	k9
cenové	cenový	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
lodních	lodní	k2eAgInPc2d1
lístků	lístek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
salónní	salónní	k2eAgFnSc2d1
kajuty	kajuta	k1gFnSc2
<g/>
:	:	kIx,
4	#num#	k4
350	#num#	k4
USD	USD	kA
/	/	kIx~
870	#num#	k4
liber	libra	k1gFnPc2
šterlinků	šterlink	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
dnešních	dnešní	k2eAgFnPc6d1
cenách	cena	k1gFnPc6
128	#num#	k4
000	#num#	k4
USD	USD	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
kajuta	kajuta	k1gFnSc1
v	v	k7c6
první	první	k4xOgFnSc6
třídě	třída	k1gFnSc6
<g/>
:	:	kIx,
150	#num#	k4
USD	USD	kA
/	/	kIx~
30	#num#	k4
liber	libra	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
dnešních	dnešní	k2eAgFnPc6d1
cenách	cena	k1gFnPc6
4	#num#	k4
400	#num#	k4
USD	USD	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
kajuta	kajuta	k1gFnSc1
v	v	k7c4
druhé	druhý	k4xOgInPc4
<g />
.	.	kIx.
</s>
<s hack="1">
třídě	třída	k1gFnSc6
<g/>
:	:	kIx,
60	#num#	k4
USD	USD	kA
/	/	kIx~
13	#num#	k4
liber	libra	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
dnešních	dnešní	k2eAgFnPc6d1
cenách	cena	k1gFnPc6
1	#num#	k4
900	#num#	k4
USD	USD	kA
<g/>
)	)	kIx)
a	a	k8xC
palubní	palubní	k2eAgInSc4d1
lístek	lístek	k1gInSc4
pro	pro	k7c4
třetí	třetí	k4xOgFnSc4
třídu	třída	k1gFnSc4
<g/>
:	:	kIx,
20	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
USD	USD	kA
/	/	kIx~
3	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
liber	libra	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
dnešních	dnešní	k2eAgFnPc6d1
cenách	cena	k1gFnPc6
440	#num#	k4
<g/>
–	–	k?
<g/>
1170	#num#	k4
USD	USD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Loď	loď	k1gFnSc1
byla	být	k5eAaImAgFnS
vybavena	vybavit	k5eAaPmNgFnS
tehdy	tehdy	k6eAd1
špičkovou	špičkový	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgFnPc4
vymoženosti	vymoženost	k1gFnPc4
patřily	patřit	k5eAaImAgInP
i	i	k8xC
tři	tři	k4xCgInPc1
elektrické	elektrický	k2eAgInPc1d1
výtahy	výtah	k1gInPc1
v	v	k7c6
první	první	k4xOgFnSc6
třídě	třída	k1gFnSc6
a	a	k8xC
jeden	jeden	k4xCgMnSc1
ve	v	k7c6
třídě	třída	k1gFnSc6
druhé	druhý	k4xOgFnSc2
a	a	k8xC
loď	loď	k1gFnSc1
byla	být	k5eAaImAgFnS
kompletně	kompletně	k6eAd1
vybavena	vybavit	k5eAaPmNgFnS
elektrickým	elektrický	k2eAgNnSc7d1
osvětlením	osvětlení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunikaci	komunikace	k1gFnSc3
lodě	loď	k1gFnSc2
s	s	k7c7
okolním	okolní	k2eAgInSc7d1
světem	svět	k1gInSc7
zajišťovaly	zajišťovat	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
rádiové	rádiový	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
Marconi	Marcon	k1gMnPc1
o	o	k7c6
výkonu	výkon	k1gInSc6
5	#num#	k4
000	#num#	k4
wattů	watt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jedné	jeden	k4xCgFnSc2
stanice	stanice	k1gFnSc2
se	se	k3xPyFc4
ve	v	k7c6
směnách	směna	k1gFnPc6
střídali	střídat	k5eAaImAgMnP
dva	dva	k4xCgMnPc1
operátoři	operátor	k1gMnPc1
společnosti	společnost	k1gFnSc2
Marconi	Marcon	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
odesílání	odesílání	k1gNnSc1
a	a	k8xC
přijímání	přijímání	k1gNnSc1
osobních	osobní	k2eAgFnPc2d1
i	i	k8xC
navigačních	navigační	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
telegrafická	telegrafický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
cestující	cestující	k1gFnSc4
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdražší	drahý	k2eAgInSc1d3
jednosměrný	jednosměrný	k2eAgInSc1d1
transatlantický	transatlantický	k2eAgInSc1d1
přenos	přenos	k1gInSc1
stál	stát	k5eAaImAgInS
870	#num#	k4
liber	libra	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
63	#num#	k4
837	#num#	k4
librám	libra	k1gFnPc3
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
1	#num#	k4
363	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
30	#num#	k4
917	#num#	k4
dolarů	dolar	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
dalšímu	další	k2eAgNnSc3d1
vybavení	vybavení	k1gNnSc3
patřil	patřit	k5eAaImAgInS
prostor	prostor	k1gInSc1
pro	pro	k7c4
venčení	venčení	k1gNnSc4
psů	pes	k1gMnPc2
<g/>
,	,	kIx,
nemocnice	nemocnice	k1gFnPc1
s	s	k7c7
operačním	operační	k2eAgInSc7d1
sálem	sál	k1gInSc7
místo	místo	k7c2
běžné	běžný	k2eAgFnSc2d1
lodní	lodní	k2eAgFnSc2d1
ošetřovny	ošetřovna	k1gFnSc2
<g/>
,	,	kIx,
mrazicí	mrazicí	k2eAgNnSc1d1
skladiště	skladiště	k1gNnSc1
řezaných	řezaný	k2eAgFnPc2d1
květin	květina	k1gFnPc2
<g/>
,	,	kIx,
ubytovací	ubytovací	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
pro	pro	k7c4
služebnictvo	služebnictvo	k1gNnSc4
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
lodě	loď	k1gFnSc2
omezovala	omezovat	k5eAaImAgFnS
houpání	houpání	k1gNnSc4
vlivem	vlivem	k7c2
vln	vlna	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgInSc7
snižovala	snižovat	k5eAaImAgFnS
riziko	riziko	k1gNnSc4
mořské	mořský	k2eAgFnSc2d1
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
konkurenčním	konkurenční	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
Cunardu	Cunard	k1gInSc2
netrpěla	trpět	k5eNaImAgFnS
vysokými	vysoký	k2eAgFnPc7d1
vibracemi	vibrace	k1gFnPc7
od	od	k7c2
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Záchranné	záchranný	k2eAgInPc1d1
čluny	člun	k1gInPc1
</s>
<s>
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
jiná	jiný	k2eAgNnPc4d1
plavidla	plavidlo	k1gNnPc4
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
cestujících	cestující	k1gMnPc2
po	po	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
vybaven	vybavit	k5eAaPmNgInS
záchrannými	záchranný	k2eAgInPc7d1
čluny	člun	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
při	při	k7c6
schvalování	schvalování	k1gNnSc6
počtu	počet	k1gInSc2
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
kapacity	kapacita	k1gFnSc2
vycházela	vycházet	k5eAaImAgNnP
z	z	k7c2
tehdy	tehdy	k6eAd1
platných	platný	k2eAgInPc2d1
námořních	námořní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
a	a	k8xC
požadavků	požadavek	k1gInPc2
Board	Boarda	k1gFnPc2
of	of	k?
Trade	Trad	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
předpisy	předpis	k1gInPc4
vyžadovaly	vyžadovat	k5eAaImAgInP
pouze	pouze	k6eAd1
takovou	takový	k3xDgFnSc4
kapacitu	kapacita	k1gFnSc4
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
výtlaku	výtlak	k1gInSc2
plavidla	plavidlo	k1gNnSc2
a	a	k8xC
předpokládanému	předpokládaný	k2eAgNnSc3d1
množství	množství	k1gNnSc3
přepravovaných	přepravovaný	k2eAgMnPc2d1
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
toto	tento	k3xDgNnSc4
určení	určení	k1gNnSc4
byly	být	k5eAaImAgFnP
ale	ale	k8xC
nesprávně	správně	k6eNd1
použity	použit	k2eAgInPc4d1
údaje	údaj	k1gInPc4
od	od	k7c2
sesterské	sesterský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Olympic	Olympice	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
počet	počet	k1gInSc1
cestujících	cestující	k1gMnPc2
na	na	k7c4
Olympicu	Olympica	k1gFnSc4
nepřesáhl	přesáhnout	k5eNaPmAgMnS
1	#num#	k4
100	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
instalace	instalace	k1gFnSc1
pouze	pouze	k6eAd1
20	#num#	k4
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
kapacitě	kapacita	k1gFnSc6
1	#num#	k4
178	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
celkové	celkový	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P.	P.	kA
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
katastrofy	katastrofa	k1gFnSc2
bylo	být	k5eAaImAgNnS
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
přes	přes	k7c4
2	#num#	k4
200	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
platných	platný	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
Board	Boarda	k1gFnPc2
of	of	k?
Trade	Trad	k1gInSc5
bylo	být	k5eAaImAgNnS
stanovení	stanovení	k1gNnSc1
počtu	počet	k1gInSc2
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
správné	správný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
britské	britský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
o	o	k7c6
výtlaku	výtlak	k1gInSc6
nad	nad	k7c7
10	#num#	k4
000	#num#	k4
tun	tuna	k1gFnPc2
měly	mít	k5eAaImAgFnP
nést	nést	k5eAaImF
16	#num#	k4
člunů	člun	k1gInPc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
prostornosti	prostornost	k1gFnSc6
5	#num#	k4
500	#num#	k4
ft³	ft³	k?
(	(	kIx(
<g/>
krychlových	krychlový	k2eAgFnPc2d1
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
160	#num#	k4
m³	m³	k?
a	a	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
další	další	k2eAgInPc1d1
dostatečnou	dostatečný	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
jiných	jiný	k2eAgNnPc2d1
záchranných	záchranný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
75	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
snížena	snížit	k5eAaPmNgFnS
na	na	k7c4
50	#num#	k4
%	%	kIx~
u	u	k7c2
lodí	loď	k1gFnPc2
s	s	k7c7
vodotěsnými	vodotěsný	k2eAgFnPc7d1
přepážkami	přepážka	k1gFnPc7
<g/>
,	,	kIx,
oproti	oproti	k7c3
kapacitě	kapacita	k1gFnSc3
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
mohla	moct	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
využít	využít	k5eAaPmF
výjimky	výjimka	k1gFnPc4
pro	pro	k7c4
lodě	loď	k1gFnPc4
s	s	k7c7
vodotěsnými	vodotěsný	k2eAgFnPc7d1
přepážkami	přepážka	k1gFnPc7
a	a	k8xC
celkovou	celkový	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
v	v	k7c6
záchranných	záchranný	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
snížit	snížit	k5eAaPmF
až	až	k6eAd1
na	na	k7c4
756	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
těchto	tento	k3xDgInPc2
předpisů	předpis	k1gInPc2
mohla	moct	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
instalovat	instalovat	k5eAaBmF
i	i	k9
méně	málo	k6eAd2
člunů	člun	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
poskytla	poskytnout	k5eAaPmAgFnS
jich	on	k3xPp3gFnPc2
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
jí	on	k3xPp3gFnSc3
bylo	být	k5eAaImAgNnS
předpisy	předpis	k1gInPc7
ukládáno	ukládat	k5eAaImNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Ani	ani	k8xC
toto	tento	k3xDgNnSc4
navýšení	navýšení	k1gNnSc4
<g/>
,	,	kIx,
nad	nad	k7c4
požadavek	požadavek	k1gInSc4
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
posléze	posléze	k6eAd1
dostačující	dostačující	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Jeden	jeden	k4xCgInSc1
ze	z	k7c2
Engelhardtových	Engelhardtův	k2eAgInPc2d1
skládacích	skládací	k2eAgInPc2d1
člunů	člun	k1gInPc2
Titanicu	Titanicus	k1gInSc2
s	s	k7c7
trosečníky	trosečník	k1gMnPc7
<g/>
,	,	kIx,
foto	foto	k1gNnSc4
z	z	k7c2
paluby	paluba	k1gFnSc2
Carpathie	Carpathie	k1gFnSc2
</s>
<s>
Z	z	k7c2
20	#num#	k4
člunů	člun	k1gInPc2
bylo	být	k5eAaImAgNnS
14	#num#	k4
velkých	velký	k2eAgInPc2d1
dřevěných	dřevěný	k2eAgInPc2d1
člunů	člun	k1gInPc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
65	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
tzv.	tzv.	kA
pohotovostní	pohotovostní	k2eAgInPc4d1
kutry	kutr	k1gInPc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
40	#num#	k4
osob	osoba	k1gFnPc2
a	a	k8xC
4	#num#	k4
Engelhardtovy	Engelhardtův	k2eAgInPc4d1
skládací	skládací	k2eAgInPc4d1
čluny	člun	k1gInPc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
47	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
měly	mít	k5eAaImAgFnP
plátěné	plátěný	k2eAgFnPc1d1
bočnice	bočnice	k1gFnPc1
s	s	k7c7
korkovou	korkový	k2eAgFnSc7d1
obrubou	obruba	k1gFnSc7
a	a	k8xC
mohly	moct	k5eAaImAgFnP
sloužit	sloužit	k5eAaImF
buď	buď	k8xC
jako	jako	k9
vor	vor	k1gInSc4
při	při	k7c6
spuštění	spuštění	k1gNnSc6
bočnic	bočnice	k1gFnPc2
nebo	nebo	k8xC
jako	jako	k9
člun	člun	k1gInSc1
při	při	k7c6
jejich	jejich	k3xOp3gNnSc6
zvednutí	zvednutí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
ale	ale	k8xC
potřeba	potřeba	k6eAd1
si	se	k3xPyFc3
uvědomit	uvědomit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
nominální	nominální	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
byla	být	k5eAaImAgFnS
reálně	reálně	k6eAd1
minimálně	minimálně	k6eAd1
o	o	k7c4
pětinu	pětina	k1gFnSc4
nižší	nízký	k2eAgFnSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spuštění	spuštění	k1gNnSc1
člunu	člun	k1gInSc2
se	s	k7c7
65	#num#	k4
lidmi	člověk	k1gMnPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
klade	klást	k5eAaImIp3nS
velmi	velmi	k6eAd1
vysoké	vysoký	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
obsluhu	obsluha	k1gFnSc4
lan	lano	k1gNnPc2
na	na	k7c6
výkladníku	výkladník	k1gInSc6
<g/>
,	,	kIx,
trhavé	trhavý	k2eAgInPc4d1
pohyby	pohyb	k1gInPc4
mohou	moct	k5eAaImIp3nP
jednoduše	jednoduše	k6eAd1
přetrhnout	přetrhnout	k5eAaPmF
používaná	používaný	k2eAgNnPc4d1
lana	lano	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
ocel	ocel	k1gFnSc4
ramen	rameno	k1gNnPc2
výkladníku	výkladník	k1gInSc2
v	v	k7c6
mrazu	mráz	k1gInSc6
křehne	křehnout	k5eAaImIp3nS
a	a	k8xC
se	s	k7c7
65	#num#	k4
lidmi	člověk	k1gMnPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
zlomit	zlomit	k5eAaPmF
(	(	kIx(
<g/>
toto	tento	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
při	při	k7c6
testech	test	k1gInPc6
na	na	k7c6
sesterské	sesterský	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
Olympic	Olympice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
při	při	k7c6
obsazení	obsazení	k1gNnSc6
65	#num#	k4
lidmi	člověk	k1gMnPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
musí	muset	k5eAaImIp3nP
někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
stát	stát	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
může	moct	k5eAaImIp3nS
i	i	k9
při	při	k7c6
velmi	velmi	k6eAd1
malých	malý	k2eAgFnPc6d1
vlnách	vlna	k1gFnPc6
způsobit	způsobit	k5eAaPmF
převrhnutí	převrhnutí	k1gNnSc2
lodě	loď	k1gFnSc2
(	(	kIx(
<g/>
nelze	lze	k6eNd1
dopředu	dopředu	k6eAd1
odhadnout	odhadnout	k5eAaPmF
čas	čas	k1gInSc4
záchrany	záchrana	k1gFnSc2
a	a	k8xC
počasí	počasí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reálná	reálný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
člunů	člun	k1gInPc2
na	na	k7c4
Titanicu	Titanica	k1gFnSc4
byla	být	k5eAaImAgFnS
tedy	tedy	k9
kolem	kolem	k7c2
850	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Představy	představa	k1gFnPc1
o	o	k7c4
záchranně	záchranně	k6eAd1
2	#num#	k4
200	#num#	k4
lidí	člověk	k1gMnPc2
z	z	k7c2
paluby	paluba	k1gFnSc2
Titanicu	Titanicus	k1gInSc2
během	během	k7c2
asi	asi	k9
30	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
minut	minuta	k1gFnPc2
(	(	kIx(
<g/>
předpokládaná	předpokládaný	k2eAgFnSc1d1
dynamika	dynamika	k1gFnSc1
potopení	potopení	k1gNnSc2
lodi	loď	k1gFnSc2
vlivem	vlivem	k7c2
efektu	efekt	k1gInSc2
volné	volný	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
<g/>
,	,	kIx,
čas	čas	k1gInSc4
a	a	k8xC
způsob	způsob	k1gInSc4
potopení	potopení	k1gNnSc2
Titanicu	Titanica	k1gMnSc4
byl	být	k5eAaImAgInS
vzhledem	vzhled	k1gInSc7
k	k	k7c3
potlačení	potlačení	k1gNnSc3
tohoto	tento	k3xDgInSc2
efektu	efekt	k1gInSc2
klidným	klidný	k2eAgNnSc7d1
počasím	počasí	k1gNnSc7
a	a	k8xC
geometrií	geometrie	k1gFnSc7
lodi	loď	k1gFnSc2
silně	silně	k6eAd1
anomální	anomální	k2eAgFnSc1d1
<g/>
)	)	kIx)
s	s	k7c7
pomocí	pomoc	k1gFnSc7
tehdy	tehdy	k6eAd1
dostupných	dostupný	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
je	být	k5eAaImIp3nS
nereálná	reálný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
spíše	spíše	k9
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
tehdy	tehdy	k6eAd1
používané	používaný	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
byly	být	k5eAaImAgFnP
obvykle	obvykle	k6eAd1
dvoumetrové	dvoumetrový	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
zcela	zcela	k6eAd1
znemožňovaly	znemožňovat	k5eAaImAgInP
spustit	spustit	k5eAaPmF
čluny	člun	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
by	by	kYmCp3nS
potřebovala	potřebovat	k5eAaImAgFnS
nést	nést	k5eAaImF
desítky	desítka	k1gFnPc4
uzavřených	uzavřený	k2eAgInPc2d1
člunů	člun	k1gInPc2
s	s	k7c7
motory	motor	k1gInPc7
<g/>
,	,	kIx,
motory	motor	k1gInPc1
poháněné	poháněný	k2eAgInPc1d1
výkladníky	výkladník	k1gInPc1
a	a	k8xC
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgInPc4d1
materiály	materiál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
bohužel	bohužel	k6eAd1
nebylo	být	k5eNaImAgNnS
na	na	k7c6
trhu	trh	k1gInSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
dostupné	dostupný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgInPc1d1
čluny	člun	k1gInPc1
a	a	k8xC
kutry	kutr	k1gInPc1
byly	být	k5eAaImAgInP
označeny	označit	k5eAaPmNgInP
čísly	číslo	k1gNnPc7
1	#num#	k4
až	až	k8xS
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čluny	člun	k1gInPc4
se	s	k7c7
sudými	sudý	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
byly	být	k5eAaImAgInP
upevněny	upevněn	k2eAgInPc1d1
na	na	k7c6
levoboku	levobok	k1gInSc6
<g/>
,	,	kIx,
s	s	k7c7
lichými	lichý	k2eAgInPc7d1
na	na	k7c6
pravoboku	pravobok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládací	skládací	k2eAgInPc1d1
čluny	člun	k1gInPc1
byly	být	k5eAaImAgInP
označeny	označit	k5eAaPmNgInP
písmeny	písmeno	k1gNnPc7
A	A	kA
<g/>
,	,	kIx,
B	B	kA
<g/>
,	,	kIx,
C	C	kA
a	a	k8xC
D.	D.	kA
Dva	dva	k4xCgInPc1
pohotovostní	pohotovostní	k2eAgInPc1d1
kutry	kutr	k1gInPc1
byly	být	k5eAaImAgInP
úplně	úplně	k6eAd1
vpředu	vpředu	k6eAd1
vedle	vedle	k7c2
kormidelny	kormidelna	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
jednom	jeden	k4xCgMnSc6
na	na	k7c6
pravoboku	pravobok	k1gInSc2
a	a	k8xC
jednom	jeden	k4xCgNnSc6
na	na	k7c6
levoboku	levobok	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtrnáct	čtrnáct	k4xCc4
velkých	velký	k2eAgInPc2d1
člunů	člun	k1gInPc2
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
po	po	k7c6
sedmi	sedm	k4xCc6
na	na	k7c4
každý	každý	k3xTgInSc4
bok	bok	k1gInSc4
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
tři	tři	k4xCgMnPc1
vpředu	vpředu	k6eAd1
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
vzadu	vzadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládací	skládací	k2eAgInPc1d1
čluny	člun	k1gInPc1
byly	být	k5eAaImAgInP
rozděleny	rozdělit	k5eAaPmNgInP
po	po	k7c6
dvou	dva	k4xCgInPc2
na	na	k7c4
pravobok	pravobok	k1gInSc4
a	a	k8xC
levobok	levobok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čluny	člun	k1gInPc7
C	C	kA
a	a	k8xC
D	D	kA
byly	být	k5eAaImAgFnP
uloženy	uložit	k5eAaPmNgFnP
vedle	vedle	k7c2
kutrů	kutr	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
druhé	druhý	k4xOgFnSc6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
spuštěny	spuštěn	k2eAgInPc1d1
stejnými	stejný	k2eAgInPc7d1
výložníky	výložník	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládací	skládací	k2eAgInPc1d1
čluny	člun	k1gInPc1
A	A	kA
a	a	k8xC
B	B	kA
byly	být	k5eAaImAgFnP
nevýhodně	výhodně	k6eNd1
umístěny	umístit	k5eAaPmNgInP
až	až	k9
na	na	k7c6
střeše	střecha	k1gFnSc6
důstojnických	důstojnický	k2eAgFnPc2d1
kajut	kajuta	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
třetí	třetí	k4xOgFnSc6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
za	za	k7c2
normálních	normální	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
bylo	být	k5eAaImAgNnS
obtížné	obtížný	k2eAgInPc4d1
tyto	tento	k3xDgInPc4
čluny	člun	k1gInPc4
<g/>
,	,	kIx,
vážící	vážící	k2eAgInPc4d1
asi	asi	k9
1	#num#	k4
350	#num#	k4
kilogramů	kilogram	k1gInPc2
<g/>
,	,	kIx,
dostat	dostat	k5eAaPmF
na	na	k7c4
člunovou	člunový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
mezi	mezi	k7c4
ramena	rameno	k1gNnPc4
výložníků	výložník	k1gInPc2
a	a	k8xC
spustit	spustit	k5eAaPmF
na	na	k7c4
hladinu	hladina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
byl	být	k5eAaImAgInS
vybaven	vybavit	k5eAaPmNgInS
moderními	moderní	k2eAgInPc7d1
Welinovými	Welinův	k2eAgInPc7d1
dvojitými	dvojitý	k2eAgInPc7d1
výložníky	výložník	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
ramena	rameno	k1gNnPc1
mohla	moct	k5eAaImAgNnP
být	být	k5eAaImF
vykloněna	vykloněn	k2eAgNnPc1d1
na	na	k7c4
obě	dva	k4xCgFnPc4
dvě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
obsluhovat	obsluhovat	k5eAaImF
druhou	druhý	k4xOgFnSc4
řadu	řada	k1gFnSc4
záchranných	záchranný	k2eAgMnPc2d1
člunů	člun	k1gInPc2
umístěnou	umístěný	k2eAgFnSc7d1
uvnitř	uvnitř	k6eAd1
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
toho	ten	k3xDgNnSc2
však	však	k9
bylo	být	k5eAaImAgNnS
upuštěno	upuštěn	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
člunová	člunový	k2eAgFnSc1d1
paluba	paluba	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
přeplněná	přeplněný	k2eAgFnSc1d1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
zmenšil	zmenšit	k5eAaPmAgInS
prostor	prostor	k1gInSc1
pro	pro	k7c4
promenádu	promenáda	k1gFnSc4
cestujících	cestující	k1gMnPc2
a	a	k8xC
zhoršil	zhoršit	k5eAaPmAgInS
výhled	výhled	k1gInSc4
na	na	k7c4
oceán	oceán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
32	#num#	k4
lodí	loď	k1gFnPc2
ve	v	k7c6
dvou	dva	k4xCgFnPc6
řadách	řada	k1gFnPc6
by	by	kYmCp3nP
nestačilo	stačit	k5eNaBmAgNnS
(	(	kIx(
<g/>
reálná	reálný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
1	#num#	k4
500	#num#	k4
osob	osoba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
stav	stav	k1gInSc1
technologie	technologie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
(	(	kIx(
<g/>
rok	rok	k1gInSc4
uzavření	uzavření	k1gNnSc2
designu	design	k1gInSc2
<g/>
)	)	kIx)
obecně	obecně	k6eAd1
znemožňoval	znemožňovat	k5eAaImAgMnS
rychlou	rychlý	k2eAgFnSc4d1
evakuaci	evakuace	k1gFnSc4
pasažérů	pasažér	k1gMnPc2
uprostřed	uprostřed	k7c2
Atlantiku	Atlantik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Skutečným	skutečný	k2eAgInSc7d1
problémem	problém	k1gInSc7
byla	být	k5eAaImAgFnS
absence	absence	k1gFnSc1
světel	světlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
byla	být	k5eAaImAgFnS
Britskou	britský	k2eAgFnSc7d1
Admiralitou	admiralita	k1gFnSc7
civilním	civilní	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
zakázána	zakázat	k5eAaPmNgFnS
s	s	k7c7
použitím	použití	k1gNnSc7
chabých	chabý	k2eAgInPc2d1
argumentů	argument	k1gInPc2
tři	tři	k4xCgInPc1
desetiletí	desetiletí	k1gNnSc4
let	léto	k1gNnPc2
před	před	k7c7
osudovou	osudový	k2eAgFnSc7d1
plavbou	plavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zákaz	zákaz	k1gInSc4
nebyl	být	k5eNaImAgInS
ani	ani	k8xC
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
ke	k	k7c3
změně	změna	k1gFnSc3
velikostí	velikost	k1gFnPc2
lodí	loď	k1gFnPc2
zmírněn	zmírněn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
hlavním	hlavní	k2eAgInSc7d1
skandálem	skandál	k1gInSc7
z	z	k7c2
pohledu	pohled	k1gInSc2
tehdejší	tehdejší	k2eAgFnSc2d1
odborné	odborný	k2eAgFnSc2d1
<g/>
,	,	kIx,
obecné	obecný	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
tisku	tisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
po	po	k7c6
katastrofě	katastrofa	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
byly	být	k5eAaImAgInP
naopak	naopak	k6eAd1
přikázána	přikázán	k2eAgNnPc1d1
světla	světlo	k1gNnPc1
hned	hned	k6eAd1
dvě	dva	k4xCgFnPc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
příkaz	příkaz	k1gInSc1
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
doporučení	doporučení	k1gNnSc4
a	a	k8xC
opět	opět	k6eAd1
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
příkaz	příkaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučení	doporučení	k1gNnSc1
a	a	k8xC
obecné	obecný	k2eAgNnSc1d1
kroucení	kroucení	k1gNnSc1
hlavou	hlava	k1gFnSc7
nad	nad	k7c7
zločinnými	zločinný	k2eAgFnPc7d1
akcemi	akce	k1gFnPc7
Britské	britský	k2eAgFnSc2d1
admirality	admiralita	k1gFnSc2
(	(	kIx(
<g/>
zákaz	zákaz	k1gInSc1
měl	mít	k5eAaImAgInS
spíše	spíše	k9
vojenský	vojenský	k2eAgInSc1d1
–	–	k?
než	než	k8xS
deklarovaný	deklarovaný	k2eAgInSc4d1
praktický	praktický	k2eAgInSc4d1
–	–	k?
podtext	podtext	k1gInSc4
<g/>
)	)	kIx)
přišlo	přijít	k5eAaPmAgNnS
hlavně	hlavně	k9
ze	z	k7c2
strany	strana	k1gFnSc2
USA	USA	kA
(	(	kIx(
<g/>
i	i	k8xC
americké	americký	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
na	na	k7c6
lince	linka	k1gFnSc6
USA	USA	kA
<g/>
/	/	kIx~
<g/>
UK	UK	kA
se	se	k3xPyFc4
zákazu	zákaz	k1gInSc6
musely	muset	k5eAaImAgInP
podřídit	podřídit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čerpadla	čerpadlo	k1gNnPc1
</s>
<s>
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
byl	být	k5eAaImAgInS
vybaven	vybavit	k5eAaPmNgInS
systémem	systém	k1gInSc7
5	#num#	k4
balastních	balastní	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
s	s	k7c7
čerpadly	čerpadlo	k1gNnPc7
a	a	k8xC
dalšími	další	k2eAgNnPc7d1
3	#num#	k4
čerpadly	čerpadlo	k1gNnPc7
<g/>
,	,	kIx,
každé	každý	k3xTgInPc4
s	s	k7c7
přečerpávací	přečerpávací	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
150	#num#	k4
tun	tuna	k1gFnPc2
<g/>
/	/	kIx~
<g/>
hod	hod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
všech	všecek	k3xTgNnPc2
současně	současně	k6eAd1
pracujících	pracující	k2eAgNnPc2d1
čerpadel	čerpadlo	k1gNnPc2
dosahovala	dosahovat	k5eAaImAgFnS
celkové	celkový	k2eAgFnPc4d1
výtlačné	výtlačný	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
1	#num#	k4
700	#num#	k4
tun	tuna	k1gFnPc2
mořské	mořský	k2eAgFnSc2d1
vody	voda	k1gFnSc2
za	za	k7c4
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Dvě	dva	k4xCgNnPc4
hlavní	hlavní	k2eAgNnPc4d1
potrubí	potrubí	k1gNnPc4
balastních	balastní	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
o	o	k7c6
průměru	průměr	k1gInSc6
10	#num#	k4
<g/>
″	″	k?
(	(	kIx(
<g/>
~	~	kIx~
<g/>
250	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
ventily	ventil	k1gInPc1
rozvodu	rozvod	k1gInSc2
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
položena	položit	k5eAaPmNgFnS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
trupu	trup	k1gInSc2
nad	nad	k7c7
přepážkami	přepážka	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
průběhu	průběh	k1gInSc6
katastrofy	katastrofa	k1gFnSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
pomocí	pomocí	k7c2
čerpadel	čerpadlo	k1gNnPc2
zpomalit	zpomalit	k5eAaPmF
zaplavení	zaplavení	k1gNnSc4
6	#num#	k4
<g/>
.	.	kIx.
kotelny	kotelna	k1gFnPc1
v	v	k7c6
prvních	první	k4xOgInPc6
deseti	deset	k4xCc6
minutách	minuta	k1gFnPc6
po	po	k7c6
srážce	srážka	k1gFnSc6
s	s	k7c7
ledovcem	ledovec	k1gInSc7
a	a	k8xC
snížit	snížit	k5eAaPmF
rychlost	rychlost	k1gFnSc4
zaplavování	zaplavování	k1gNnPc2
5	#num#	k4
<g/>
.	.	kIx.
kotelny	kotelna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
odčerpávání	odčerpávání	k1gNnSc4
vody	voda	k1gFnSc2
nemohlo	moct	k5eNaImAgNnS
zachovat	zachovat	k5eAaPmF
potřebný	potřebný	k2eAgInSc4d1
vztlak	vztlak	k1gInSc4
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
zpomalilo	zpomalit	k5eAaPmAgNnS
však	však	k9
zaplavování	zaplavování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čerpadla	čerpadlo	k1gNnPc1
pracovala	pracovat	k5eAaImAgNnP
až	až	k6eAd1
do	do	k7c2
23	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
zaplavena	zaplavit	k5eAaPmNgFnS
a	a	k8xC
vyřazena	vyřadit	k5eAaPmNgFnS
z	z	k7c2
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Olympic	Olympic	k1gMnSc1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
v	v	k7c6
loděnicích	loděnice	k1gFnPc6
v	v	k7c6
Belfastu	Belfast	k1gInSc6
</s>
<s>
Srovnání	srovnání	k1gNnSc1
s	s	k7c7
Olympicem	Olympic	k1gMnSc7
</s>
<s>
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
a	a	k8xC
RMS	RMS	kA
Olympic	Olympice	k1gInPc2
byly	být	k5eAaImAgFnP
sesterské	sesterský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Olympic	Olympice	k1gInPc2
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
dříve	dříve	k6eAd2
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byly	být	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
lodě	loď	k1gFnPc1
vyráběny	vyrábět	k5eAaImNgFnP
současně	současně	k6eAd1
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
loděnici	loděnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
Titanicu	Titanicus	k1gInSc2
byla	být	k5eAaImAgFnS
poněkud	poněkud	k6eAd1
zpožděna	zpozdit	k5eAaPmNgFnS
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
musel	muset	k5eAaImAgMnS
ustoupit	ustoupit	k5eAaPmF
nutné	nutný	k2eAgFnSc3d1
opravě	oprava	k1gFnSc3
Olympicu	Olympicus	k1gInSc2
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
nešťastné	šťastný	k2eNgFnSc6d1
srážce	srážka	k1gFnSc6
s	s	k7c7
křižníkem	křižník	k1gInSc7
Hawkee	Hawke	k1gFnSc2
–	–	k?
z	z	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
pocházejí	pocházet	k5eAaImIp3nP
také	také	k9
společné	společný	k2eAgFnPc4d1
fotografie	fotografia	k1gFnPc4
obou	dva	k4xCgFnPc2
lodí	loď	k1gFnPc2
v	v	k7c6
Belfastu	Belfast	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
Titanicem	Titanic	k1gMnSc7
a	a	k8xC
Olympicem	Olympic	k1gMnSc7
byl	být	k5eAaImAgMnS
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
patrný	patrný	k2eAgInSc4d1
pouze	pouze	k6eAd1
ze	z	k7c2
zábradlí	zábradlí	k1gNnSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
A	a	k9
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
změněno	změnit	k5eAaPmNgNnS
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
lodě	loď	k1gFnPc1
měly	mít	k5eAaImAgFnP
trup	trup	k1gInSc4
stejně	stejně	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
(	(	kIx(
<g/>
rozdíl	rozdíl	k1gInSc1
činil	činit	k5eAaImAgInS
10	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jen	jen	k9
Titanic	Titanic	k1gInSc1
měl	mít	k5eAaImAgInS
větší	veliký	k2eAgFnSc4d2
registrovanou	registrovaný	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
(	(	kIx(
<g/>
BRT	BRT	k?
<g/>
)	)	kIx)
a	a	k8xC
obě	dva	k4xCgFnPc1
lodě	loď	k1gFnPc1
měly	mít	k5eAaImAgFnP
i	i	k9
velmi	velmi	k6eAd1
podobný	podobný	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lišily	lišit	k5eAaImAgInP
se	se	k3xPyFc4
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
přední	přední	k2eAgFnSc1d1
promenáda	promenáda	k1gFnSc1
na	na	k7c6
palubě	paluba	k1gFnSc6
A	a	k9
Titanicu	Titanicus	k1gInSc3
byla	být	k5eAaImAgFnS
uzavřena	uzavřen	k2eAgFnSc1d1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
cestujících	cestující	k1gFnPc2
před	před	k7c7
počasím	počasí	k1gNnSc7
<g/>
,	,	kIx,
paluby	paluba	k1gFnPc1
B	B	kA
měly	mít	k5eAaImAgFnP
jiné	jiný	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
a	a	k8xC
na	na	k7c4
Olympicu	Olympica	k1gFnSc4
nebyla	být	k5eNaImAgFnS
„	„	k?
<g/>
Café	café	k1gNnPc2
Parisien	Parisino	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
na	na	k7c6
Olympic	Olympice	k1gFnPc2
přestavěna	přestavěn	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
<g/>
,	,	kIx,
díky	díky	k7c3
oblibě	obliba	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
si	se	k3xPyFc3
získala	získat	k5eAaPmAgFnS
na	na	k7c6
Titaniku	Titanic	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
stavbě	stavba	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
byly	být	k5eAaImAgFnP
odstraněny	odstranit	k5eAaPmNgFnP
i	i	k9
některé	některý	k3yIgInPc4
konstrukční	konstrukční	k2eAgInPc4d1
nedostatky	nedostatek	k1gInPc4
zjištěné	zjištěný	k2eAgInPc4d1
na	na	k7c4
Olympicu	Olympica	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světla	světlo	k1gNnPc1
na	na	k7c6
palubě	paluba	k1gFnSc6
A	a	k9
Titanicu	Titanicus	k1gInSc3
byla	být	k5eAaImAgFnS
kulatá	kulatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c4
Olympicu	Olympica	k1gFnSc4
oválná	oválný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kormidelna	kormidelna	k1gFnSc1
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
byla	být	k5eAaImAgFnS
protáhlejší	protáhlý	k2eAgFnSc4d2
–	–	k?
užší	úzký	k2eAgFnSc4d2
a	a	k8xC
delší	dlouhý	k2eAgFnSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
provedené	provedený	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
při	při	k7c6
spuštění	spuštění	k1gNnSc6
na	na	k7c4
vodu	voda	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
o	o	k7c4
1	#num#	k4
004	#num#	k4
BRT	BRT	k?
větší	veliký	k2eAgMnSc1d2
<g/>
,	,	kIx,
a	a	k8xC
loď	loď	k1gFnSc1
se	se	k3xPyFc4
jevila	jevit	k5eAaImAgFnS
užší	úzký	k2eAgFnSc1d2
a	a	k8xC
delší	dlouhý	k2eAgFnSc1d2
(	(	kIx(
<g/>
šlo	jít	k5eAaImAgNnS
ovšem	ovšem	k9
pouze	pouze	k6eAd1
o	o	k7c4
optický	optický	k2eAgInSc4d1
dojem	dojem	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spuštění	spuštění	k1gNnSc1
na	na	k7c4
vodu	voda	k1gFnSc4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1912	#num#	k4
Olympic	Olympice	k1gFnPc2
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Titanic	Titanic	k1gInSc4
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
době	doba	k1gFnSc6
dokončovacích	dokončovací	k2eAgFnPc2d1
prací	práce	k1gFnPc2
</s>
<s>
Trup	trup	k1gInSc1
Titanicu	Titanicum	k1gNnSc3
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
na	na	k7c4
vodu	voda	k1gFnSc4
řeky	řeka	k1gFnSc2
Lagan	Lagan	k1gInSc4
dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1911	#num#	k4
ve	v	k7c4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dal	dát	k5eAaPmAgMnS
předseda	předseda	k1gMnSc1
správní	správní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
loděnice	loděnice	k1gFnSc2
William	William	k1gInSc1
James	James	k1gMnSc1
Pirrie	Pirrie	k1gFnSc1
pokyn	pokyn	k1gInSc4
k	k	k7c3
zahájení	zahájení	k1gNnSc3
spuštění	spuštění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Události	událost	k1gFnPc1
přihlíželo	přihlížet	k5eAaImAgNnS
množství	množství	k1gNnSc4
pozvaných	pozvaný	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
a	a	k8xC
zvědavých	zvědavý	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spuštění	spuštění	k1gNnSc1
lodi	loď	k1gFnSc3
ale	ale	k8xC
proběhlo	proběhnout	k5eAaPmAgNnS
bez	bez	k7c2
obvyklých	obvyklý	k2eAgInPc2d1
obřadů	obřad	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
křest	křest	k1gInSc4
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
sice	sice	k8xC
nikdy	nikdy	k6eAd1
své	svůj	k3xOyFgFnPc4
lodě	loď	k1gFnPc4
nekřtila	křtít	k5eNaImAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
absence	absence	k1gFnSc1
křtu	křest	k1gInSc2
laiky	laik	k1gMnPc4
pokládána	pokládat	k5eAaImNgFnS
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
příčin	příčina	k1gFnPc2
katastrofy	katastrofa	k1gFnSc2
–	–	k?
společnost	společnost	k1gFnSc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
však	však	k9
nikdy	nikdy	k6eAd1
nepokřtila	pokřtít	k5eNaPmAgFnS
žádnou	žádný	k3yNgFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
nekřtila	křtít	k5eNaImAgFnS
své	svůj	k3xOyFgFnPc4
lodě	loď	k1gFnPc4
konkurenční	konkurenční	k2eAgFnPc4d1
státem	stát	k1gInSc7
subvencovaná	subvencovaný	k2eAgFnSc1d1
Cunard	Cunard	k1gMnSc1
Line	linout	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
spuštění	spuštění	k1gNnSc6
zůstal	zůstat	k5eAaPmAgInS
Titanic	Titanic	k1gInSc4
na	na	k7c6
pozemcích	pozemek	k1gInPc6
loděnic	loděnice	k1gFnPc2
Harland	Harland	k1gInSc1
and	and	k?
Wolff	Wolff	k1gInSc4
k	k	k7c3
dokončení	dokončení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
nutno	nutno	k6eAd1
osadit	osadit	k5eAaPmF
kotle	kotel	k1gInPc4
<g/>
,	,	kIx,
parní	parní	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
,	,	kIx,
namontovat	namontovat	k5eAaPmF
stožáry	stožár	k1gInPc4
<g/>
,	,	kIx,
komíny	komín	k1gInPc4
a	a	k8xC
zařídit	zařídit	k5eAaPmF
a	a	k8xC
vybavit	vybavit	k5eAaPmF
interiéry	interiér	k1gInPc4
a	a	k8xC
jiné	jiný	k2eAgNnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
práce	práce	k1gFnPc1
trvaly	trvat	k5eAaImAgFnP
skoro	skoro	k6eAd1
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zkoušky	zkouška	k1gFnPc1
</s>
<s>
Titanic	Titanic	k1gInSc1
proplouvající	proplouvající	k2eAgFnSc2d1
kanálemke	kanálemkat	k5eAaPmIp3nS
zkouškám	zkouška	k1gFnPc3
v	v	k7c6
Irském	irský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
</s>
<s>
Následně	následně	k6eAd1
<g/>
,	,	kIx,
po	po	k7c6
dohotovení	dohotovení	k1gNnSc6
dokončovacích	dokončovací	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
zahájeny	zahájen	k2eAgFnPc1d1
zatěžkávací	zatěžkávací	k2eAgFnPc1d1
zkoušky	zkouška	k1gFnPc1
v	v	k7c6
Irském	irský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
podle	podle	k7c2
tehdejších	tehdejší	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
–	–	k?
rozjetí	rozjetí	k1gNnSc4
z	z	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
zastavení	zastavení	k1gNnSc3
<g/>
,	,	kIx,
nejkratší	krátký	k2eAgInSc1d3
oblouk	oblouk	k1gInSc1
v	v	k7c6
„	„	k?
<g/>
plné	plný	k2eAgFnSc3d1
rychlosti	rychlost	k1gFnSc3
<g/>
“	“	k?
(	(	kIx(
<g/>
šlo	jít	k5eAaImAgNnS
o	o	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
rychlost	rychlost	k1gFnSc4
Titanikem	Titanic	k1gInSc7
dosaženou	dosažený	k2eAgFnSc4d1
21,5	21,5	k4
<g />
.	.	kIx.
</s>
<s hack="1">
uzlu	uzel	k1gInSc2
–	–	k?
což	což	k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
obdivuhodné	obdivuhodný	k2eAgNnSc1d1
i	i	k9
dnes	dnes	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
byl	být	k5eAaImAgMnS
Titanic	Titanic	k1gInSc4
po	po	k7c6
zkouškách	zkouška	k1gFnPc6
uznán	uznat	k5eAaPmNgInS
plavby	plavba	k1gFnSc2
schopným	schopný	k2eAgMnPc3d1
na	na	k7c4
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
povolení	povolení	k1gNnSc4
k	k	k7c3
plavbě	plavba	k1gFnSc6
Victoriiným	Victoriin	k2eAgInSc7d1
kanálem	kanál	k1gInSc7
z	z	k7c2
Belfastu	Belfast	k1gInSc2
do	do	k7c2
Irského	irský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
krátce	krátce	k6eAd1
po	po	k7c6
půlnoci	půlnoc	k1gFnSc6
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
vplul	vplout	k5eAaPmAgInS
Titanic	Titanic	k1gInSc1
do	do	k7c2
Southamptonu	Southampton	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zkoušky	zkouška	k1gFnPc1
probíhaly	probíhat	k5eAaImAgFnP
bez	bez	k7c2
obsluhujícího	obsluhující	k2eAgInSc2d1
personálu	personál	k1gInSc2
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
palubě	paluba	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
bylo	být	k5eAaImAgNnS
78	#num#	k4
topičů	topič	k1gMnPc2
a	a	k8xC
strojníků	strojník	k1gMnPc2
a	a	k8xC
41	#num#	k4
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
lodní	lodní	k2eAgFnSc6d1
palubě	paluba	k1gFnSc6
byli	být	k5eAaImAgMnP
přítomni	přítomen	k2eAgMnPc1d1
zástupci	zástupce	k1gMnPc1
různých	různý	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Harold	Harold	k1gMnSc1
A.	A.	kA
Sanderson	Sanderson	k1gMnSc1
z	z	k7c2
I.	I.	kA
<g/>
M.	M.	kA
<g/>
M	M	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Andrews	Andrews	k1gInSc1
a	a	k8xC
Edward	Edward	k1gMnSc1
Wilding	Wilding	k1gInSc1
z	z	k7c2
Harland	Harlanda	k1gFnPc2
&	&	k?
Wolff	Wolff	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
nemoc	nemoc	k1gFnSc4
se	se	k3xPyFc4
omluvili	omluvit	k5eAaPmAgMnP
J.	J.	kA
Bruce	Bruce	k1gMnSc2
Ismay	Ismaa	k1gMnSc2
a	a	k8xC
Lord	lord	k1gMnSc1
Pirrie	Pirrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jack	Jacka	k1gFnPc2
Phillips	Phillipsa	k1gFnPc2
a	a	k8xC
Harold	Harolda	k1gFnPc2
Bride	Brid	k1gInSc5
byli	být	k5eAaImAgMnP
operátoři	operátor	k1gMnPc1
radiostanice	radiostanice	k1gFnSc2
Marconi	Marcon	k1gMnPc1
a	a	k8xC
prováděli	provádět	k5eAaImAgMnP
jemné	jemný	k2eAgNnSc4d1
doladění	doladění	k1gNnSc4
stanice	stanice	k1gFnSc2
–	–	k?
což	což	k3yRnSc1,k3yQnSc1
vyžadovalo	vyžadovat	k5eAaImAgNnS
několik	několik	k4yIc1
složitých	složitý	k2eAgInPc2d1
obratů	obrat	k1gInPc2
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
byl	být	k5eAaImAgInS
Francis	Francis	k1gInSc1
Carruthers	Carruthersa	k1gFnPc2
z	z	k7c2
Board	Boarda	k1gFnPc2
of	of	k?
Trade	Trad	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
po	po	k7c6
prověření	prověření	k1gNnSc6
lodě	loď	k1gFnSc2
podepsal	podepsat	k5eAaPmAgInS
dokument	dokument	k1gInSc1
Agreement	Agreement	k1gMnSc1
and	and	k?
Account	Account	k1gMnSc1
of	of	k?
Voyages	Voyages	k1gMnSc1
and	and	k?
Crew	Crew	k1gMnSc1
<g/>
,	,	kIx,
opravňující	opravňující	k2eAgFnSc4d1
loď	loď	k1gFnSc4
k	k	k7c3
plavbě	plavba	k1gFnSc3
s	s	k7c7
pasažéry	pasažér	k1gMnPc7
po	po	k7c4
dobu	doba	k1gFnSc4
dvanácti	dvanáct	k4xCc2
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
šesti	šest	k4xCc6
hodinách	hodina	k1gFnPc6
plavebních	plavební	k2eAgFnPc2d1
zkoušek	zkouška	k1gFnPc2
na	na	k7c6
moři	moře	k1gNnSc6
opustil	opustit	k5eAaPmAgInS
Titanic	Titanic	k1gInSc4
v	v	k7c4
poledne	poledne	k1gNnSc4
Belfast	Belfast	k1gInSc1
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
550	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
890	#num#	k4
km	km	kA
<g/>
)	)	kIx)
dlouhou	dlouhý	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
Southamptonu	Southampton	k1gInSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
kapitána	kapitán	k1gMnSc2
Herberta	Herbert	k1gMnSc2
Haddocka	Haddocka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
provedl	provést	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
konstruktér	konstruktér	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Andrews	Andrews	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plavba	plavba	k1gFnSc1
</s>
<s>
Edward	Edward	k1gMnSc1
J.	J.	kA
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
</s>
<s>
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
poslední	poslední	k2eAgFnSc1d1
plavba	plavba	k1gFnSc1
směřovala	směřovat	k5eAaImAgFnS
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Southamptonu	Southampton	k1gInSc2
přes	přes	k7c4
francouzský	francouzský	k2eAgInSc4d1
Cherbourg	Cherbourg	k1gInSc4
a	a	k8xC
irský	irský	k2eAgInSc1d1
Queenstown	Queenstown	k1gInSc1
(	(	kIx(
<g/>
Cobh	Cobh	k1gInSc1
<g/>
)	)	kIx)
do	do	k7c2
amerického	americký	k2eAgMnSc2d1
New	New	k1gMnSc2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
plavbu	plavba	k1gFnSc4
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
vydal	vydat	k5eAaPmAgInS
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
pod	pod	k7c7
velením	velení	k1gNnSc7
kapitána	kapitán	k1gMnSc2
Edwarda	Edward	k1gMnSc2
J.	J.	kA
Smithe	Smith	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
komodorem	komodor	k1gMnSc7
flotily	flotila	k1gFnSc2
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
a	a	k8xC
předtím	předtím	k6eAd1
sloužil	sloužit	k5eAaImAgInS
na	na	k7c4
Olympicu	Olympica	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Lord	lord	k1gMnSc1
Pirrie	Pirrie	k1gFnSc2
a	a	k8xC
Bruce	Bruce	k1gFnSc2
Ismay	Ismaa	k1gFnSc2
na	na	k7c4
Titanicu	Titanica	k1gFnSc4
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
V	v	k7c6
Anglii	Anglie	k1gFnSc6
bylo	být	k5eAaImAgNnS
tradicí	tradice	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
vypravovala	vypravovat	k5eAaImAgFnS
zvláštní	zvláštní	k2eAgInSc4d1
vlak	vlak	k1gInSc4
z	z	k7c2
Londýna	Londýn	k1gInSc2
do	do	k7c2
přístavu	přístav	k1gInSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
cestující	cestující	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
strávili	strávit	k5eAaPmAgMnP
poslední	poslední	k2eAgFnSc4d1
noc	noc	k1gFnSc4
na	na	k7c6
pevnině	pevnina	k1gFnSc6
v	v	k7c6
hotelu	hotel	k1gInSc6
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
zajištěnu	zajištěn	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
k	k	k7c3
jejím	její	k3xOp3gFnPc3
lodím	loď	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc1
plavba	plavba	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Titanicu	Titanicus	k1gInSc2
byla	být	k5eAaImAgFnS
málem	málem	k6eAd1
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
odložena	odložit	k5eAaPmNgFnS
kvůli	kvůli	k7c3
probíhající	probíhající	k2eAgFnSc3d1
stávce	stávka	k1gFnSc3
horníků	horník	k1gMnPc2
–	–	k?
na	na	k7c4
Titanic	Titanic	k1gInSc4
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
přeložena	přeložit	k5eAaPmNgFnS
zásoba	zásoba	k1gFnSc1
uhlí	uhlí	k1gNnSc2
ze	z	k7c2
všech	všecek	k3xTgFnPc2
dostupných	dostupný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
v	v	k7c6
Southamptonu	Southampton	k1gInSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyhověl	vyhovět	k5eAaPmAgMnS
úřednímu	úřední	k2eAgNnSc3d1
zjištění	zjištění	k1gNnSc3
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yRgMnSc2,k3yQgMnSc2,k3yIgMnSc2
je	být	k5eAaImIp3nS
vybaven	vybaven	k2eAgInSc1d1
dostatečným	dostatečný	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
uhlí	uhlí	k1gNnSc2
pro	pro	k7c4
plavbu	plavba	k1gFnSc4
přes	přes	k7c4
Atlantik	Atlantik	k1gInSc4
–	–	k?
není	být	k5eNaImIp3nS
bez	bez	k7c2
zajímavosti	zajímavost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc4
limit	limit	k1gInSc4
<g/>
,	,	kIx,
nastavený	nastavený	k2eAgInSc4d1
pro	pro	k7c4
daleko	daleko	k6eAd1
menší	malý	k2eAgFnPc4d2
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
překročil	překročit	k5eAaPmAgMnS
Titanic	Titanic	k1gInSc4
jen	jen	k9
o	o	k7c4
několik	několik	k4yIc4
set	set	k1gInSc4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
palubu	paluba	k1gFnSc4
se	se	k3xPyFc4
nalodilo	nalodit	k5eAaPmAgNnS
přes	přes	k7c4
1	#num#	k4
300	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
v	v	k7c6
první	první	k4xOgFnSc6
třídě	třída	k1gFnSc6
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
300	#num#	k4
osob	osoba	k1gFnPc2
v	v	k7c6
druhé	druhý	k4xOgFnSc6
třídě	třída	k1gFnSc6
a	a	k8xC
přes	přes	k7c4
700	#num#	k4
ve	v	k7c6
třídě	třída	k1gFnSc6
třetí	třetí	k4xOgFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
Titanic	Titanic	k1gInSc1
tedy	tedy	k9
nebyl	být	k5eNaImAgInS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
plavbě	plavba	k1gFnSc6
kapacitně	kapacitně	k6eAd1
zcela	zcela	k6eAd1
vytížen	vytížit	k5eAaPmNgInS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
společně	společně	k6eAd1
s	s	k7c7
posádkou	posádka	k1gFnSc7
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
na	na	k7c6
palubě	paluba	k1gFnSc6
až	až	k9
2	#num#	k4
223	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velení	velení	k1gNnSc1
lodi	loď	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
rukou	ruka	k1gFnPc6
kapitána	kapitán	k1gMnSc2
Edwarda	Edward	k1gMnSc2
J.	J.	kA
Smithe	Smith	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
k	k	k7c3
ruce	ruka	k1gFnSc3
7	#num#	k4
důstojníků	důstojník	k1gMnPc2
(	(	kIx(
<g/>
vrchní	vrchní	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
H.	H.	kA
T.	T.	kA
Wilde	Wild	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
William	William	k1gInSc4
McMaster	McMaster	k1gMnSc1
Murdoch	Murdoch	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charles	Charles	k1gMnSc1
Herbert	Herbert	k1gMnSc1
Lightoller	Lightoller	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herbert	Herbert	k1gMnSc1
John	John	k1gMnSc1
Pitman	Pitman	k1gMnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joseph	Joseph	k1gInSc1
Grove	Groev	k1gFnSc2
Boxhall	Boxhalla	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harold	Harold	k1gInSc1
Godfrey	Godfrea	k1gFnSc2
Lowe	Low	k1gFnSc2
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
James	James	k1gMnSc1
Pell	Pell	k1gMnSc1
Moody	Mooda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
palubě	paluba	k1gFnSc6
lodi	loď	k1gFnSc6
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
i	i	k9
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
lodní	lodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
lord	lord	k1gMnSc1
J.	J.	kA
Bruce	Bruce	k1gMnSc1
Ismay	Ismaa	k1gFnSc2
a	a	k8xC
šéfkonstruktér	šéfkonstruktér	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Andrews	Andrews	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
projektant	projektant	k1gMnSc1
R.	R.	kA
R.	R.	kA
C.	C.	kA
Chisholm	Chisholm	k1gMnSc1
<g/>
,	,	kIx,
elektrikáři	elektrikář	k1gMnSc3
W.	W.	kA
H.	H.	kA
M.	M.	kA
Parr	Parr	k1gInSc4
a	a	k8xC
E.	E.	kA
H.	H.	kA
Watson	Watson	k1gMnSc1
<g/>
,	,	kIx,
zámečníci	zámečník	k1gMnPc1
A.	A.	kA
F.	F.	kA
Cunnigham	Cunnigham	k1gInSc1
<g/>
,	,	kIx,
A.	A.	kA
W.	W.	kA
Frost	Frost	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
J.	J.	kA
Knight	Knight	k1gMnSc1
a	a	k8xC
instalatér	instalatér	k1gMnSc1
Francis	Francis	k1gFnSc2
Milan	Milan	k1gMnSc1
„	„	k?
<g/>
Frank	Frank	k1gMnSc1
<g/>
“	“	k?
Parkers	Parkers	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
plavba	plavba	k1gFnSc1
byla	být	k5eAaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
pouze	pouze	k6eAd1
pro	pro	k7c4
několik	několik	k4yIc4
málo	málo	k4c4
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
plavili	plavit	k5eAaImAgMnP
ze	z	k7c2
Southamptonu	Southampton	k1gInSc2
do	do	k7c2
Cherbourgu	Cherbourg	k1gInSc2
nebo	nebo	k8xC
Queenstownu	Queenstown	k1gInSc2
a	a	k8xC
další	další	k2eAgFnSc2d1
plavby	plavba	k1gFnSc2
se	se	k3xPyFc4
neúčastnili	účastnit	k5eNaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Přístav	přístav	k1gInSc1
v	v	k7c6
Queenstownu	Queenstown	k1gInSc6
nebyl	být	k5eNaImAgInS
stavebně	stavebně	k6eAd1
vybaven	vybavit	k5eAaPmNgInS
pro	pro	k7c4
tak	tak	k6eAd1
velkou	velký	k2eAgFnSc4d1
loď	loď	k1gFnSc4
a	a	k8xC
naloďování	naloďování	k1gNnSc1
a	a	k8xC
vyloďování	vyloďování	k1gNnSc1
neprobíhalo	probíhat	k5eNaImAgNnS
u	u	k7c2
mola	molo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pomocí	pomocí	k7c2
lodí	loď	k1gFnPc2
(	(	kIx(
<g/>
SS	SS	kA
Nomadic	Nomadice	k1gInPc2
<g/>
)	)	kIx)
speciálně	speciálně	k6eAd1
pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
společností	společnost	k1gFnPc2
zakoupených	zakoupený	k2eAgMnPc2d1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
pasažéry	pasažér	k1gMnPc7
převážely	převážet	k5eAaImAgFnP
mezi	mezi	k7c7
lodí	loď	k1gFnSc7
a	a	k8xC
pevninou	pevnina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyplutí	vyplutí	k1gNnSc6
z	z	k7c2
Queenstownu	Queenstown	k1gInSc2
směr	směr	k1gInSc4
New	New	k1gFnSc4
York	York	k1gInSc4
bylo	být	k5eAaImAgNnS
na	na	k7c6
palubě	paluba	k1gFnSc6
2	#num#	k4
201	#num#	k4
osob	osoba	k1gFnPc2
<g/>
;	;	kIx,
1	#num#	k4
316	#num#	k4
cestujících	cestující	k1gMnPc2
a	a	k8xC
855	#num#	k4
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
první	první	k4xOgFnSc6
plavbě	plavba	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
přes	přes	k7c4
Atlantský	atlantský	k2eAgInSc4d1
oceán	oceán	k1gInSc4
bylo	být	k5eAaImAgNnS
mnoho	mnoho	k4c1
významných	významný	k2eAgMnPc2d1
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
milionář	milionář	k1gMnSc1
John	John	k1gMnSc1
Jacob	Jacoba	k1gFnPc2
Astor	Astora	k1gFnPc2
IV	IV	kA
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Madeleine	Madeleine	k1gFnSc1
Force	force	k1gFnSc1
Astorová	Astorová	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
průmyslník	průmyslník	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
Guggenheim	Guggenheim	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
majitel	majitel	k1gMnSc1
obchodního	obchodní	k2eAgInSc2d1
řetězce	řetězec	k1gInSc2
Macy	Maca	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
;	;	kIx,
Isidor	Isidor	k1gMnSc1
Straus	Straus	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Ida	Ida	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
milionářka	milionářka	k1gFnSc1
Margaret	Margareta	k1gFnPc2
„	„	k?
<g/>
Molly	Molla	k1gFnSc2
<g/>
“	“	k?
Brownová	Brownová	k1gFnSc1
z	z	k7c2
Denveru	Denver	k1gInSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
přezdívaná	přezdívaný	k2eAgFnSc1d1
„	„	k?
<g/>
Nepotopitelná	potopitelný	k2eNgFnSc1d1
Molly	Molla	k1gFnPc4
<g/>
“	“	k?
pro	pro	k7c4
pomoc	pomoc	k1gFnSc4
ostatním	ostatní	k2eAgInPc3d1
při	při	k7c6
ztroskotání	ztroskotání	k1gNnSc6
<g/>
,	,	kIx,
</s>
<s>
sir	sir	k1gMnSc1
Cosmo	Cosma	k1gFnSc5
Duff-Gordon	Duff-Gordon	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
<g/>
,	,	kIx,
módní	módní	k2eAgFnSc1d1
návrhářka	návrhářka	k1gFnSc1
Lucy	Luca	k1gFnSc2
Duffová-Gordonová	Duffová-Gordonová	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
George	Georgat	k5eAaPmIp3nS
Dunton	Dunton	k1gInSc4
Widener	Widenra	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc7
manželka	manželka	k1gFnSc1
Eleanor	Eleanora	k1gFnPc2
se	s	k7c7
synem	syn	k1gMnSc7
Harrym	Harrym	k1gInSc4
<g/>
,	,	kIx,
hráčem	hráč	k1gMnSc7
kriketu	kriket	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
obchodník	obchodník	k1gMnSc1
John	John	k1gMnSc1
Borland	Borland	kA
Thayer	Thayer	k1gMnSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
Marian	Mariana	k1gFnPc2
a	a	k8xC
sedmnáctiletým	sedmnáctiletý	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Jackem	Jacek	k1gMnSc7
<g/>
,	,	kIx,
</s>
<s>
novinář	novinář	k1gMnSc1
William	William	k1gInSc1
Thomas	Thomas	k1gMnSc1
Stead	Stead	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
o	o	k7c6
zaoceánském	zaoceánský	k2eAgInSc6d1
parníku	parník	k1gInSc6
potopeném	potopený	k2eAgInSc6d1
ledovou	ledový	k2eAgFnSc4d1
krou	kra	k1gFnSc7
<g/>
,	,	kIx,
</s>
<s>
hraběnka	hraběnka	k1gFnSc1
Rothes	Rothesa	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
asistent	asistent	k1gMnSc1
prezidenta	prezident	k1gMnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgFnPc2d1
Archibald	Archibalda	k1gFnPc2
Butt	Butta	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
spisovatelka	spisovatelka	k1gFnSc1
a	a	k8xC
socioložka	socioložka	k1gFnSc1
Helen	Helena	k1gFnPc2
Churchill	Churchilla	k1gFnPc2
Candeeová	Candeeový	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Jacques	Jacques	k1gMnSc1
Futrelle	Futrelle	k1gInSc4
s	s	k7c7
manželkou	manželka	k1gFnSc7
May	May	k1gMnSc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
přátelé	přítel	k1gMnPc1
<g/>
,	,	kIx,
producenti	producent	k1gMnPc1
z	z	k7c2
Broadwaye	Broadway	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
Henry	henry	k1gInSc1
a	a	k8xC
Rene	Rene	k1gInSc1
Harrisovi	Harris	k1gMnSc3
<g/>
,	,	kIx,
</s>
<s>
herečka	herečka	k1gFnSc1
němého	němý	k2eAgInSc2d1
filmu	film	k1gInSc2
Dorothy	Dorotha	k1gFnSc2
Gibsonová	Gibsonová	k1gFnSc1
</s>
<s>
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plavby	plavba	k1gFnPc1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
neúčastnil	účastnit	k5eNaImAgMnS
bankéř	bankéř	k1gMnSc1
J.	J.	kA
P.	P.	kA
Morgan	morgan	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
plavbu	plavba	k1gFnSc4
zrušil	zrušit	k5eAaPmAgInS
–	–	k?
podle	podle	k7c2
tehdejších	tehdejší	k2eAgNnPc2d1
měřítek	měřítko	k1gNnPc2
nebyla	být	k5eNaImAgFnS
společnost	společnost	k1gFnSc1
na	na	k7c6
panenské	panenský	k2eAgFnSc6d1
plavbě	plavba	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
–	–	k?
navzdory	navzdory	k7c3
pozdějším	pozdní	k2eAgInPc3d2
výmyslům	výmysl	k1gInPc3
–	–	k?
nijak	nijak	k6eAd1
výjimečná	výjimečný	k2eAgFnSc1d1
či	či	k8xC
vybraná	vybraný	k2eAgFnSc1d1
a	a	k8xC
podle	podle	k7c2
záznamů	záznam	k1gInPc2
rovněž	rovněž	k9
loď	loď	k1gFnSc1
nepřevážela	převážet	k5eNaImAgFnS
žádné	žádný	k3yNgFnPc4
významnější	významný	k2eAgFnPc4d2
cennosti	cennost	k1gFnPc4
či	či	k8xC
poklady	poklad	k1gInPc4
(	(	kIx(
<g/>
tím	ten	k3xDgNnSc7
méně	málo	k6eAd2
často	často	k6eAd1
zmiňovanou	zmiňovaný	k2eAgFnSc4d1
mumii	mumie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
bezpečně	bezpečně	k6eAd1
uložena	uložit	k5eAaPmNgFnS
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
tři	tři	k4xCgFnPc4
desítky	desítka	k1gFnPc1
lidí	člověk	k1gMnPc2
pak	pak	k8xC
plavbu	plavba	k1gFnSc4
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
odřekly	odřeknout	k5eAaPmAgInP
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
nechyběly	chybět	k5eNaImAgFnP
ani	ani	k8xC
zlé	zlý	k2eAgFnPc1d1
předtuchy	předtucha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
ovšem	ovšem	k9
panovaly	panovat	k5eAaImAgFnP
i	i	k9
na	na	k7c6
lodi	loď	k1gFnSc6
–	–	k?
několik	několik	k4yIc4
přeživších	přeživší	k2eAgMnPc2d1
vzpomínalo	vzpomínat	k5eAaImAgNnS
na	na	k7c4
kokrhání	kokrhání	k1gNnSc4
kohouta	kohout	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
v	v	k7c6
Cornwalu	Cornwal	k1gInSc6
a	a	k8xC
Walesu	Wales	k1gInSc2
ohlašuje	ohlašovat	k5eAaImIp3nS
smrt	smrt	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drůbež	drůbež	k1gFnSc1
ušlechtilého	ušlechtilý	k2eAgInSc2d1
chovu	chov	k1gInSc2
byla	být	k5eAaImAgFnS
ovšem	ovšem	k9
na	na	k7c6
seznamu	seznam	k1gInSc6
nákladu	náklad	k1gInSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
ubytována	ubytován	k2eAgFnSc1d1
vedle	vedle	k7c2
psů	pes	k1gMnPc2
nedaleko	nedaleko	k7c2
kuchyně	kuchyně	k1gFnSc2
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
–	–	k?
v	v	k7c6
těsném	těsný	k2eAgNnSc6d1
sousedství	sousedství	k1gNnSc6
kajut	kajuta	k1gFnPc2
druhé	druhý	k4xOgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
rovněž	rovněž	k9
bez	bez	k7c2
zajímavosti	zajímavost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Černého	Černého	k2eAgInSc2d1
gangu	gang	k1gInSc2
<g/>
)	)	kIx)
dezertoval	dezertovat	k5eAaBmAgInS
ze	z	k7c2
služby	služba	k1gFnSc2
v	v	k7c6
Cóbhu	Cóbh	k1gInSc6
(	(	kIx(
<g/>
Queenstownu	Queenstowno	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gNnSc7
bydlištěm	bydliště	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
Titanic	Titanic	k1gInSc4
mu	on	k3xPp3gMnSc3
posloužil	posloužit	k5eAaPmAgMnS
pravděpodobně	pravděpodobně	k6eAd1
jen	jen	k9
jako	jako	k9
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
zadarmo	zadarmo	k6eAd1
dostat	dostat	k5eAaPmF
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
z	z	k7c2
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
pak	pak	k6eAd1
ztratil	ztratit	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
námořnickou	námořnický	k2eAgFnSc4d1
knížku	knížka	k1gFnSc4
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
zahynul	zahynout	k5eAaPmAgMnS
na	na	k7c4
Titanicu	Titanica	k1gFnSc4
neznámý	známý	k2eNgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Čechy	Čech	k1gMnPc4
není	být	k5eNaImIp3nS
bez	bez	k7c2
zajímavosti	zajímavost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
jistý	jistý	k2eAgMnSc1d1
„	„	k?
<g/>
Francouz	Francouz	k1gMnSc1
<g/>
“	“	k?
se	s	k7c7
jménem	jméno	k1gNnSc7
Michel	Michel	k1gMnSc1
Navratil	Navratil	k1gMnSc1
cestoval	cestovat	k5eAaImAgMnS
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgFnPc7
malými	malý	k2eAgFnPc7d1
dětmi	dítě	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
unesl	unést	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zachránil	zachránit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
rozpadlé	rozpadlý	k2eAgNnSc4d1
manželství	manželství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
dokázal	dokázat	k5eAaPmAgMnS
dostat	dostat	k5eAaPmF
do	do	k7c2
člunu	člun	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
sám	sám	k3xTgMnSc1
na	na	k7c6
lodi	loď	k1gFnSc6
zahynul	zahynout	k5eAaPmAgMnS
–	–	k?
jeho	jeho	k3xOp3gFnSc4
tělo	tělo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vyloveno	vyloven	k2eAgNnSc1d1
a	a	k8xC
halifaxský	halifaxský	k2eAgMnSc1d1
rabín	rabín	k1gMnSc1
o	o	k7c6
ostatcích	ostatek	k1gInPc6
česko-francouzského	česko-francouzský	k2eAgMnSc2d1
katolíka	katolík	k1gMnSc2
rozhodl	rozhodnout	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
Žida	Žid	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
Michel	Michel	k1gMnSc1
Navrátil	navrátit	k5eAaPmAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
na	na	k7c6
židovském	židovský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
v	v	k7c6
Halifaxu	Halifax	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
cestujícími	cestující	k1gFnPc7
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
Brity	Brit	k1gMnPc4
<g/>
,	,	kIx,
Iry	Ir	k1gMnPc4
ale	ale	k8xC
především	především	k9
Skandinávce	Skandinávec	k1gMnSc2
–	–	k?
ve	v	k7c6
Skandinávii	Skandinávie	k1gFnSc6
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
mohutně	mohutně	k6eAd1
inzerovala	inzerovat	k5eAaImAgFnS
a	a	k8xC
řada	řada	k1gFnSc1
především	především	k6eAd1
švédských	švédský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
koupila	koupit	k5eAaPmAgFnS
lístek	lístek	k1gInSc4
„	„	k?
<g/>
na	na	k7c4
první	první	k4xOgFnSc4
volnou	volný	k2eAgFnSc4d1
loď	loď	k1gFnSc4
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Mezi	mezi	k7c7
cestujícími	cestující	k1gMnPc7
byli	být	k5eAaImAgMnP
však	však	k9
také	také	k9
Syřané	Syřan	k1gMnPc1
<g/>
,	,	kIx,
Arméni	Armén	k1gMnPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
národnosti	národnost	k1gFnPc1
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
,	,	kIx,
nastoupivší	nastoupivší	k2eAgFnSc1d1
v	v	k7c6
Cherbourgu	Cherbourg	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
dopravili	dopravit	k5eAaPmAgMnP
vlakem	vlak	k1gInSc7
z	z	k7c2
Marseille	Marseille	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posádce	posádka	k1gFnSc6
převažovali	převažovat	k5eAaImAgMnP
Britové	Brit	k1gMnPc1
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
nebylo	být	k5eNaImAgNnS
na	na	k7c6
britských	britský	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
pravidlem	pravidlem	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
„	„	k?
<g/>
Černém	černé	k1gNnSc6
gangu	gang	k1gInSc2
<g/>
“	“	k?
topičů	topič	k1gInPc2
a	a	k8xC
strojníků	strojník	k1gMnPc2
nalézáme	nalézat	k5eAaImIp1nP
především	především	k9
irská	irský	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Titanic	Titanic	k1gInSc4
opouštěl	opouštět	k5eAaImAgMnS
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
Queenstown	Queenstown	k1gInSc1
na	na	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
záhuby	záhuba	k1gFnSc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
irských	irský	k2eAgMnPc2d1
emigrantů	emigrant	k1gMnPc2
Eugene	Eugen	k1gInSc5
Dally	Dall	k1gInPc7
hrál	hrát	k5eAaImAgInS
na	na	k7c4
irské	irský	k2eAgFnPc4d1
dudy	dudy	k1gFnPc4
píseň	píseň	k1gFnSc4
„	„	k?
<g/>
Irish	Irish	k1gMnSc1
Lament	Lament	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
Irský	irský	k2eAgInSc1d1
nářek	nářek	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
tentýž	týž	k3xTgInSc1
Dally	Dalla	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
katastrofu	katastrofa	k1gFnSc4
přežil	přežít	k5eAaPmAgMnS
<g/>
,	,	kIx,
pak	pak	k6eAd1
žaloval	žalovat	k5eAaImAgInS
mezi	mezi	k7c7
stovkami	stovka	k1gFnPc7
jiných	jiný	k2eAgFnPc2d1
společnost	společnost	k1gFnSc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
o	o	k7c4
náhradu	náhrada	k1gFnSc4
škody	škoda	k1gFnSc2
–	–	k?
potopené	potopený	k2eAgFnPc1d1
dudy	dudy	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Středa	středa	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Titanic	Titanic	k1gInSc1
se	se	k3xPyFc4
těsně	těsně	k6eAd1
míjí	míjet	k5eAaImIp3nS
s	s	k7c7
parníkem	parník	k1gInSc7
New	New	k1gFnSc2
York	York	k1gInSc1
</s>
<s>
Plavba	plavba	k1gFnSc1
Titanicu	Titanicus	k1gInSc2
začínala	začínat	k5eAaImAgFnS
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
ve	v	k7c4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
vyvedením	vyvedení	k1gNnSc7
lodě	loď	k1gFnSc2
z	z	k7c2
přístavních	přístavní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
v	v	k7c6
Southamptonu	Southampton	k1gInSc6
pod	pod	k7c7
velením	velení	k1gNnSc7
lodivoda	lodivod	k1gMnSc2
George	Georg	k1gMnSc2
Bowyera	Bowyer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
byl	být	k5eAaImAgInS
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
a	a	k8xC
neohrabaný	neohrabaný	k2eAgInSc1d1,k2eNgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgMnS
sám	sám	k3xTgInSc4
vymanévrovat	vymanévrovat	k5eAaPmF
z	z	k7c2
přístavu	přístav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
mu	on	k3xPp3gNnSc3
zde	zde	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
přístavech	přístav	k1gInPc6
<g/>
,	,	kIx,
pomáhalo	pomáhat	k5eAaImAgNnS
několik	několik	k4yIc1
remorkérů	remorkér	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
incidentu	incident	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
mohl	moct	k5eAaImAgInS
předčasně	předčasně	k6eAd1
plavbu	plavba	k1gFnSc4
ukončit	ukončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
plavební	plavební	k2eAgInPc1d1
trasy	tras	k1gInPc1
Titanicu	Titanicus	k1gInSc2
byly	být	k5eAaImAgInP
ukotveny	ukotven	k2eAgInPc1d1
další	další	k2eAgInPc1d1
dva	dva	k4xCgInPc1
parníky	parník	k1gInPc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
a	a	k8xC
Oceanic	Oceanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
míjení	míjení	k1gNnSc6
New	New	k1gFnSc2
Yorku	York	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
vlivem	vlivem	k7c2
proudění	proudění	k1gNnSc2
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
hnané	hnaný	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
sacímu	sací	k2eAgInSc3d1
efektu	efekt	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
natolik	natolik	k6eAd1
silný	silný	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přetrhla	přetrhnout	k5eAaPmAgNnP
kotvící	kotvící	k2eAgNnPc1d1
lana	lano	k1gNnPc1
New	New	k1gMnSc2
Yorku	York	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
nebezpečně	bezpečně	k6eNd1
přibližovat	přibližovat	k5eAaImF
k	k	k7c3
Titanicu	Titanicus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozící	hrozící	k2eAgFnSc6d1
kolizi	kolize	k1gFnSc6
odvrátil	odvrátit	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
E.	E.	kA
J.	J.	kA
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nechal	nechat	k5eAaPmAgMnS
zastavit	zastavit	k5eAaPmF
stroje	stroj	k1gInPc4
na	na	k7c4
Titanicu	Titanica	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
jeden	jeden	k4xCgInSc1
z	z	k7c2
remorkérů	remorkér	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
obeplul	obeplout	k5eAaPmAgInS
New	New	k1gFnSc4
York	York	k1gInSc1
a	a	k8xC
pomocí	pomocí	k7c2
lana	lano	k1gNnSc2
jej	on	k3xPp3gMnSc4
táhl	táhnout	k5eAaImAgInS
zpět	zpět	k6eAd1
ke	k	k7c3
břehu	břeh	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
lodě	loď	k1gFnPc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
minuly	minout	k5eAaImAgFnP
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
desítek	desítka	k1gFnPc2
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolizi	kolize	k1gFnSc4
s	s	k7c7
Titanicem	Titanic	k1gMnSc7
bylo	být	k5eAaImAgNnS
sice	sice	k8xC
zabráněno	zabránit	k5eAaPmNgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
New	New	k1gFnSc1
York	York	k1gInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
opřel	opřít	k5eAaPmAgMnS
o	o	k7c4
bok	bok	k1gInSc4
Oceanicu	Oceanicus	k1gInSc2
<g/>
,	,	kIx,
naštěstí	naštěstí	k6eAd1
bez	bez	k7c2
větších	veliký	k2eAgFnPc2d2
škod	škoda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
plavbě	plavba	k1gFnSc6
přes	přes	k7c4
Lamanšský	lamanšský	k2eAgInSc4d1
průliv	průliv	k1gInSc4
do	do	k7c2
Francie	Francie	k1gFnSc2
zjistil	zjistit	k5eAaPmAgInS
George	George	k1gInSc1
Symons	Symons	k1gInSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
hlídkové	hlídkový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
pozorovacím	pozorovací	k2eAgInSc6d1
koši	koš	k1gInSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
vraním	vraní	k2eAgNnSc6d1
hnízdě	hnízdo	k1gNnSc6
<g/>
,	,	kIx,
<g/>
“	“	k?
na	na	k7c6
předním	přední	k2eAgInSc6d1
stožáru	stožár	k1gInSc6
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
dalekohledy	dalekohled	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhledal	vyhledat	k5eAaPmAgInS
a	a	k8xC
požádal	požádat	k5eAaPmAgInS
2	#num#	k4
<g/>
.	.	kIx.
důstojníka	důstojník	k1gMnSc4
Lightollera	Lightoller	k1gMnSc4
o	o	k7c4
zajištění	zajištění	k1gNnSc4
dalekohledů	dalekohled	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
ten	ten	k3xDgInSc1
tento	tento	k3xDgInSc1
požadavek	požadavek	k1gInSc1
dále	daleko	k6eAd2
předal	předat	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
důstojníkovi	důstojník	k1gMnSc3
Murdochovi	Murdoch	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prozatím	prozatím	k6eAd1
byl	být	k5eAaImAgInS
George	George	k1gInSc1
Symons	Symons	k1gInSc1
obeznámen	obeznámen	k2eAgInSc1d1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
věc	věc	k1gFnSc1
se	se	k3xPyFc4
řeší	řešit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
plavbě	plavba	k1gFnSc6
z	z	k7c2
Belfastu	Belfast	k1gInSc2
byl	být	k5eAaImAgInS
pozorovací	pozorovací	k2eAgInSc1d1
koš	koš	k1gInSc1
ještě	ještě	k9
dalekohledy	dalekohled	k1gInPc4
vybaven	vybaven	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
když	když	k8xS
2	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
Blair	Blair	k1gMnSc1
opouštěl	opouštět	k5eAaImAgMnS
v	v	k7c6
Southamptonu	Southampton	k1gInSc6
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
tyto	tento	k3xDgInPc4
dalekohledy	dalekohled	k1gInPc4
uložil	uložit	k5eAaPmAgInS
v	v	k7c6
kajutě	kajuta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
Lightoller	Lightoller	k1gMnSc1
však	však	k9
o	o	k7c6
tomto	tento	k3xDgInSc6
nevěděl	vědět	k5eNaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
hlídky	hlídka	k1gFnPc1
v	v	k7c6
pozorovacím	pozorovací	k2eAgInSc6d1
koši	koš	k1gInSc6
zůstaly	zůstat	k5eAaPmAgFnP
bez	bez	k7c2
dalekohledů	dalekohled	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
P.	P.	kA
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c4
podvečer	podvečer	k1gInSc4
doplul	doplout	k5eAaPmAgInS
Titanic	Titanic	k1gInSc1
do	do	k7c2
francouzského	francouzský	k2eAgInSc2d1
Cherbourgu	Cherbourg	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
doplněny	doplnit	k5eAaPmNgFnP
další	další	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
a	a	k8xC
naloděni	naloděn	k2eAgMnPc1d1
další	další	k2eAgMnPc1d1
cestující	cestující	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krátké	krátký	k2eAgFnSc6d1
zastávce	zastávka	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
v	v	k7c6
21	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
vydal	vydat	k5eAaPmAgMnS
k	k	k7c3
pobřeží	pobřeží	k1gNnSc3
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtek	čtvrtek	k1gInSc1
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
měl	mít	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
poslední	poslední	k2eAgFnSc4d1
zastávku	zastávka	k1gFnSc4
u	u	k7c2
evropského	evropský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
irském	irský	k2eAgInSc6d1
přístavu	přístav	k1gInSc6
Queenstown	Queenstowna	k1gFnPc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Cobh	Cobh	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přístavu	přístav	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
menší	malý	k2eAgFnSc1d2
hloubka	hloubka	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
opět	opět	k6eAd1
projevila	projevit	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
sací	sací	k2eAgFnSc1d1
síla	síla	k1gFnSc1
lodních	lodní	k2eAgInPc2d1
šroubů	šroub	k1gInPc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
zvířeno	zvířen	k2eAgNnSc4d1
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
písku	písek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
jev	jev	k1gInSc1
znepokojil	znepokojit	k5eAaPmAgInS
pasažéry	pasažér	k1gMnPc7
a	a	k8xC
posádka	posádka	k1gFnSc1
musela	muset	k5eAaImAgFnS
vysvětlovat	vysvětlovat	k5eAaImF
příčinu	příčina	k1gFnSc4
tohoto	tento	k3xDgInSc2
jevu	jev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Titanic	Titanic	k1gInSc1
při	při	k7c6
plavbě	plavba	k1gFnSc6
v	v	k7c6
queenstownském	queenstownský	k2eAgInSc6d1
přístavu	přístav	k1gInSc6
dokonce	dokonce	k9
táhl	táhnout	k5eAaImAgMnS
potopený	potopený	k2eAgInSc4d1
vlečný	vlečný	k2eAgInSc4d1
člun	člun	k1gInSc4
asi	asi	k9
800	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
při	při	k7c6
předchozí	předchozí	k2eAgFnSc6d1
zastávce	zastávka	k1gFnSc6
v	v	k7c6
Cherbourgu	Cherbourg	k1gInSc6
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
i	i	k9
zde	zde	k6eAd1
doplněny	doplnit	k5eAaPmNgFnP
zásoby	zásoba	k1gFnPc1
<g/>
,	,	kIx,
naloděno	naloděn	k2eAgNnSc1d1
posledních	poslední	k2eAgMnPc2d1
130	#num#	k4
cestujících	cestující	k1gMnPc2
a	a	k8xC
1	#num#	k4
400	#num#	k4
pytlů	pytel	k1gInPc2
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
12	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
vyplul	vyplout	k5eAaPmAgInS
západním	západní	k2eAgInSc7d1
směrem	směr	k1gInSc7
do	do	k7c2
Atlantiku	Atlantik	k1gInSc2
<g/>
,	,	kIx,
směr	směr	k1gInSc1
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přes	přes	k7c4
Atlantik	Atlantik	k1gInSc4
</s>
<s>
Ještě	ještě	k9
odpoledne	odpoledne	k6eAd1
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
plul	plout	k5eAaImAgMnS
Titanic	Titanic	k1gInSc4
poblíž	poblíž	k7c2
pobřeží	pobřeží	k1gNnSc2
Irska	Irsko	k1gNnSc2
a	a	k8xC
až	až	k9
k	k	k7c3
večeru	večer	k1gInSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
proplul	proplout	k5eAaPmAgInS
kolem	kolem	k7c2
Fastnet	Fastneta	k1gFnPc2
Rocku	rock	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
definitivně	definitivně	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
vod	voda	k1gFnPc2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pátek	pátek	k1gInSc1
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Plavba	plavba	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
bez	bez	k7c2
komplikací	komplikace	k1gFnPc2
<g/>
,	,	kIx,
cestující	cestující	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
si	se	k3xPyFc3
dopřávali	dopřávat	k5eAaImAgMnP
komfortu	komfort	k1gInSc2
poskytovaného	poskytovaný	k2eAgInSc2d1
lodní	lodní	k2eAgFnSc7d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
komfortu	komfort	k1gInSc2
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
si	se	k3xPyFc3
zaplatili	zaplatit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
i	i	k9
o	o	k7c6
cestující	cestující	k1gFnSc6
3	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnPc1
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
postaráno	postarán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
cestující	cestující	k1gMnPc4
nevěděli	vědět	k5eNaImAgMnP
a	a	k8xC
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
hlavně	hlavně	k9
určeno	určit	k5eAaPmNgNnS
pro	pro	k7c4
kapitána	kapitán	k1gMnSc4
a	a	k8xC
důstojníky	důstojník	k1gMnPc4
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zprávy	zpráva	k1gFnPc1
o	o	k7c6
ledovém	ledový	k2eAgNnSc6d1
poli	pole	k1gNnSc6
před	před	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
zprávy	zpráva	k1gFnPc1
přicházely	přicházet	k5eAaImAgFnP
rádiodepešemi	rádiodepeše	k1gFnPc7
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
z	z	k7c2
parníku	parník	k1gInSc2
Touraine	Tourain	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
o	o	k7c6
výskytu	výskyt	k1gInSc6
ledu	led	k1gInSc2
byly	být	k5eAaImAgFnP
zaznamenány	zaznamenat	k5eAaPmNgFnP
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
důstojníci	důstojník	k1gMnPc1
se	se	k3xPyFc4
ale	ale	k8xC
shodli	shodnout	k5eAaBmAgMnP,k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
neleží	ležet	k5eNaImIp3nS
v	v	k7c6
plánované	plánovaný	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
plavby	plavba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
poledne	poledne	k1gNnSc2
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
do	do	k7c2
poledne	poledne	k1gNnSc2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
urazil	urazit	k5eAaPmAgMnS
Titanic	Titanic	k1gInSc4
386	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
621	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sobota	sobota	k1gFnSc1
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Plavba	plavba	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
stejně	stejně	k6eAd1
poklidně	poklidně	k6eAd1
jako	jako	k9
předešlého	předešlý	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
poledne	poledne	k1gNnSc2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
do	do	k7c2
poledne	poledne	k1gNnSc2
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
urazil	urazit	k5eAaPmAgMnS
Titanic	Titanic	k1gInSc4
519	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
835	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neděle	neděle	k1gFnSc1
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Oblast	oblast	k1gFnSc1
Atlantiku	Atlantik	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
Titanic	Titanic	k1gInSc1
proplouval	proplouvat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobře	dobře	k6eAd1
jsou	být	k5eAaImIp3nP
patrné	patrný	k2eAgInPc1d1
šelfy	šelf	k1gInPc1
<g/>
,	,	kIx,
křížek	křížek	k1gInSc1
označuje	označovat	k5eAaImIp3nS
místo	místo	k7c2
tragédie	tragédie	k1gFnSc2
</s>
<s>
Počasí	počasí	k1gNnSc1
bylo	být	k5eAaImAgNnS
stále	stále	k6eAd1
dobré	dobrý	k2eAgNnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
dopoledních	dopolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
nechal	nechat	k5eAaPmAgMnS
sloužit	sloužit	k5eAaImF
kapitán	kapitán	k1gMnSc1
E.	E.	kA
J.	J.	kA
Smith	Smith	k1gMnSc1
bohoslužbu	bohoslužba	k1gFnSc4
v	v	k7c6
jídelně	jídelna	k1gFnSc6
<g/>
[	[	kIx(
<g/>
P.	P.	kA
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
poté	poté	k6eAd1
byla	být	k5eAaImAgFnS
jídelna	jídelna	k1gFnSc1
opět	opět	k6eAd1
připravena	připravit	k5eAaPmNgFnS
ke	k	k7c3
stolování	stolování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zaznamenávaných	zaznamenávaný	k2eAgInPc2d1
rychlostních	rychlostní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
se	se	k3xPyFc4
rychlost	rychlost	k1gFnSc1
Titanicu	Titanicus	k1gInSc2
stále	stále	k6eAd1
zvyšovala	zvyšovat	k5eAaImAgFnS
a	a	k8xC
k	k	k7c3
poledni	poledne	k1gNnSc3
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
urazil	urazit	k5eAaPmAgMnS
dalších	další	k2eAgFnPc2d1
546	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
879	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
J.	J.	kA
Bruce	Bruce	k1gFnSc1
Ismaye	Ismaye	k1gFnSc1
a	a	k8xC
kapitán	kapitán	k1gMnSc1
E.	E.	kA
J.	J.	kA
Smith	Smith	k1gMnSc1
sice	sice	k8xC
v	v	k7c6
prvních	první	k4xOgInPc6
dnech	den	k1gInPc6
po	po	k7c6
vyplutí	vyplutí	k1gNnSc6
předpokládali	předpokládat	k5eAaImAgMnP
vyšší	vysoký	k2eAgFnSc4d2
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
kapitán	kapitán	k1gMnSc1
při	při	k7c6
zachování	zachování	k1gNnSc6
dobrého	dobrý	k2eAgNnSc2d1
počasí	počasí	k1gNnSc2
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dosáhne	dosáhnout	k5eAaPmIp3nS
rekordního	rekordní	k2eAgInSc2d1
času	čas	k1gInSc2
plavby	plavba	k1gFnSc2
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
zapálení	zapálení	k1gNnSc2
pod	pod	k7c7
dalšími	další	k2eAgFnPc7d1
kotli	kotel	k1gInSc3
<g/>
,	,	kIx,
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
rychlosti	rychlost	k1gFnSc2
<g/>
,	,	kIx,
plánovalo	plánovat	k5eAaImAgNnS
až	až	k9
na	na	k7c4
15	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
pod	pod	k7c7
kotli	kotel	k1gInSc6
zapáleno	zapálit	k5eAaPmNgNnS
již	již	k6eAd1
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
a	a	k8xC
Titanic	Titanic	k1gInSc1
již	již	k6eAd1
plul	plout	k5eAaImAgInS
rychlostí	rychlost	k1gFnSc7
21	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
39	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
dopoledne	dopoledne	k1gNnSc2
přicházely	přicházet	k5eAaImAgFnP
další	další	k2eAgFnPc1d1
radiodepeše	radiodepeše	k1gFnPc1
–	–	k?
„	„	k?
<g/>
marconigramy	marconigram	k1gInPc1
<g/>
“	“	k?
se	s	k7c7
znepokojujícími	znepokojující	k2eAgFnPc7d1
informacemi	informace	k1gFnPc7
o	o	k7c6
výskytu	výskyt	k1gInSc6
ledového	ledový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
zpráva	zpráva	k1gFnSc1
lodi	loď	k1gFnSc2
Caronia	Caronium	k1gNnSc2
<g/>
:	:	kIx,
ledové	ledový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
42	#num#	k4
<g/>
°	°	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
z.	z.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
d.	d.	k?
až	až	k9
42	#num#	k4
<g/>
°	°	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
51	#num#	k4
<g/>
°	°	k?
z.	z.	k?
d.	d.	k?
<g/>
,	,	kIx,
11	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
potvrzuje	potvrzovat	k5eAaImIp3nS
parník	parník	k1gInSc1
Noordam	Noordam	k1gInSc1
předchozí	předchozí	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
13	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
zpráva	zpráva	k1gFnSc1
parníku	parník	k1gInSc2
Baltic	Baltice	k1gFnPc2
<g/>
:	:	kIx,
Mírný	mírný	k2eAgInSc1d1
<g/>
,	,	kIx,
proměnlivý	proměnlivý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
<g/>
,	,	kIx,
jasné	jasný	k2eAgNnSc1d1
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
dobré	dobré	k1gNnSc1
počasí	počasí	k1gNnSc1
a	a	k8xC
předal	předat	k5eAaPmAgMnS
zprávu	zpráva	k1gFnSc4
řecké	řecký	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Athenai	Athena	k1gFnSc2
<g/>
:	:	kIx,
ledové	ledový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
41	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
z.	z.	k?
d.	d.	k?
<g/>
,	,	kIx,
v	v	k7c6
13	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
zpráva	zpráva	k1gFnSc1
německého	německý	k2eAgInSc2d1
parníku	parník	k1gInSc2
Amerika	Amerika	k1gFnSc1
<g/>
:	:	kIx,
setkání	setkání	k1gNnSc1
se	s	k7c7
dvěma	dva	k4xCgInPc7
velkými	velký	k2eAgInPc7d1
ledovci	ledovec	k1gInPc7
na	na	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
pozici	pozice	k1gFnSc6
41	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
50	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
plul	plout	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
rychlostí	rychlost	k1gFnSc7
přibližně	přibližně	k6eAd1
22	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
41	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
a	a	k8xC
blížil	blížit	k5eAaImAgMnS
se	se	k3xPyFc4
k	k	k7c3
mělčinám	mělčina	k1gFnPc3
v	v	k7c6
kurzu	kurz	k1gInSc6
242	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
změnit	změnit	k5eAaPmF
kurz	kurz	k1gInSc1
na	na	k7c4
265	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
200	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
320	#num#	k4
km	km	kA
<g/>
)	)	kIx)
severně	severně	k6eAd1
od	od	k7c2
této	tento	k3xDgFnSc2
trasy	trasa	k1gFnSc2
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
ledová	ledový	k2eAgFnSc1d1
pole	pole	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
upozornila	upozornit	k5eAaPmAgFnS
loď	loď	k1gFnSc4
Caronia	Caronium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
informace	informace	k1gFnPc1
ukazovaly	ukazovat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ledová	ledový	k2eAgNnPc1d1
pole	pole	k1gNnPc1
vyskytují	vyskytovat	k5eAaImIp3nP
neobvykle	obvykle	k6eNd1
jižněji	jižně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
očekávalo	očekávat	k5eAaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poloha	poloha	k1gFnSc1
nahlášených	nahlášený	k2eAgInPc2d1
ledovců	ledovec	k1gInPc2
před	před	k7c7
kolizí	kolize	k1gFnSc7
a	a	k8xC
polohy	poloha	k1gFnSc2
lodí	loď	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
po	po	k7c6
kolizi	kolize	k1gFnSc6
podle	podle	k7c2
záznamů	záznam	k1gInPc2
<g/>
.	.	kIx.
•	•	k?
křížek	křížek	k1gInSc1
–	–	k?
vrak	vrak	k1gInSc1
Titanicu	Titanicus	k1gInSc2
•	•	k?
tmavý	tmavý	k2eAgInSc1d1
bod	bod	k1gInSc1
–	–	k?
vysílání	vysílání	k1gNnSc2
chybné	chybný	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
•	•	k?
trojúhelník	trojúhelník	k1gInSc1
–	–	k?
hlášené	hlášený	k2eAgInPc1d1
ledovce	ledovec	k1gInPc1
•	•	k?
žlutý	žlutý	k2eAgInSc4d1
bod	bod	k1gInSc4
–	–	k?
ostatní	ostatní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
</s>
<s>
Velení	velení	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
mělo	mít	k5eAaImAgNnS
dostatek	dostatek	k1gInSc4
informací	informace	k1gFnPc2
o	o	k7c6
výskytu	výskyt	k1gInSc6
ledovců	ledovec	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
se	s	k7c7
zprávami	zpráva	k1gFnPc7
nebylo	být	k5eNaImAgNnS
účelně	účelně	k6eAd1
nakládáno	nakládat	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
zprávu	zpráva	k1gFnSc4
z	z	k7c2
parníku	parník	k1gInSc2
Baltic	Baltice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
taktéž	taktéž	k?
patřil	patřit	k5eAaImAgInS
společnosti	společnost	k1gFnSc3
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
Smith	Smith	k1gMnSc1
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
ponechal	ponechat	k5eAaPmAgMnS
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
u	u	k7c2
sebe	sebe	k3xPyFc4
a	a	k8xC
po	po	k7c6
obědě	oběd	k1gInSc6
ji	on	k3xPp3gFnSc4
předal	předat	k5eAaPmAgMnS
J.	J.	kA
B.	B.	kA
Ismayovi	Ismaya	k1gMnSc3
<g/>
,	,	kIx,
řediteli	ředitel	k1gMnSc3
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
sice	sice	k8xC
přečetl	přečíst	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
ponechal	ponechat	k5eAaPmAgInS
taktéž	taktéž	k?
u	u	k7c2
sebe	se	k3xPyFc2
a	a	k8xC
odpoledne	odpoledne	k6eAd1
ji	on	k3xPp3gFnSc4
ukázal	ukázat	k5eAaPmAgMnS
některým	některý	k3yIgMnPc3
cestujícím	cestující	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
zapomněl	zapomnět	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpráva	zpráva	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
nedostala	dostat	k5eNaPmAgFnS
na	na	k7c4
kapitánský	kapitánský	k2eAgInSc4d1
můstek	můstek	k1gInSc4
k	k	k7c3
velícímu	velící	k2eAgMnSc3d1
důstojníkovi	důstojník	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
P.	P.	kA
8	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
zprávy	zpráva	k1gFnSc2
z	z	k7c2
parníku	parník	k1gInSc2
Amerika	Amerika	k1gFnSc1
není	být	k5eNaImIp3nS
taktéž	taktéž	k?
zcela	zcela	k6eAd1
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
<g/>
-li	-li	k?
doručena	doručen	k2eAgFnSc1d1
až	až	k9
na	na	k7c4
kapitánský	kapitánský	k2eAgInSc4d1
můstek	můstek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Zpráva	zpráva	k1gFnSc1
byla	být	k5eAaImAgFnS
totiž	totiž	k9
zachycena	zachytit	k5eAaPmNgFnS
na	na	k7c4
radiostanici	radiostanice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
„	„	k?
<g/>
občanský	občanský	k2eAgInSc1d1
provoz	provoz	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
na	na	k7c6
vlnách	vlna	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nebyly	být	k5eNaImAgFnP
určeny	určit	k5eAaPmNgFnP
pro	pro	k7c4
námořní	námořní	k2eAgInSc4d1
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
večerním	večerní	k2eAgNnSc7d1
střídáním	střídání	k1gNnSc7
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
17	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
změnil	změnit	k5eAaPmAgInS
Titanic	Titanic	k1gInSc1
kurz	kurz	k1gInSc1
na	na	k7c6
289	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
šest	šest	k4xCc4
hodin	hodina	k1gFnPc2
večer	večer	k6eAd1
proběhlo	proběhnout	k5eAaPmAgNnS
střídání	střídání	k1gNnSc1
důstojníků	důstojník	k1gMnPc2
na	na	k7c6
můstku	můstek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
služby	služba	k1gFnSc2
nastoupil	nastoupit	k5eAaPmAgInS
2	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
Lightoller	Lightoller	k1gMnSc1
<g/>
,	,	kIx,
důstojníci	důstojník	k1gMnPc1
H.	H.	kA
G.	G.	kA
Lowe	Lowe	k1gInSc4
a	a	k8xC
H.	H.	kA
J.	J.	kA
Pitman	Pitman	k1gMnSc1
<g/>
,	,	kIx,
ke	k	k7c3
kormidlu	kormidlo	k1gNnSc3
nastoupil	nastoupit	k5eAaPmAgInS
2	#num#	k4
<g/>
.	.	kIx.
kormidelník	kormidelník	k1gMnSc1
Robert	Robert	k1gMnSc1
Hitchens	Hitchens	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
plul	plout	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
v	v	k7c6
kurzu	kurz	k1gInSc6
289	#num#	k4
<g/>
°	°	k?
a	a	k8xC
kurz	kurz	k1gInSc1
měl	mít	k5eAaImAgInS
udržovat	udržovat	k5eAaImF
i	i	k9
nadále	nadále	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
střídání	střídání	k1gNnSc6
služeb	služba	k1gFnPc2
nastoupili	nastoupit	k5eAaPmAgMnP
do	do	k7c2
pozorovacího	pozorovací	k2eAgInSc2d1
koše	koš	k1gInSc2
na	na	k7c6
předním	přední	k2eAgInSc6d1
stožáru	stožár	k1gInSc6
Jewell	Jewell	k1gInSc1
a	a	k8xC
Symons	Symons	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
Lowe	Low	k1gFnSc2
se	se	k3xPyFc4
po	po	k7c6
nástupu	nástup	k1gInSc6
do	do	k7c2
služby	služba	k1gFnSc2
stavil	stavit	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
v	v	k7c6
mapovně	mapovna	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
zahlédl	zahlédnout	k5eAaPmAgMnS
lístek	lístek	k1gInSc4
s	s	k7c7
poznámkou	poznámka	k1gFnSc7
„	„	k?
<g/>
led	led	k1gInSc4
<g/>
“	“	k?
s	s	k7c7
udanými	udaný	k2eAgFnPc7d1
souřadnicemi	souřadnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provedl	provést	k5eAaPmAgMnS
výpočet	výpočet	k1gInSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgNnSc2,k3yRgNnSc2,k3yQgNnSc2
mu	on	k3xPp3gMnSc3
vyšlo	vyjít	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
místu	místo	k1gNnSc3
setkání	setkání	k1gNnSc2
s	s	k7c7
ledem	led	k1gInSc7
dojde	dojít	k5eAaPmIp3nS
až	až	k9
po	po	k7c6
ukončení	ukončení	k1gNnSc6
jeho	jeho	k3xOp3gFnSc2
služby	služba	k1gFnSc2
a	a	k8xC
dále	daleko	k6eAd2
se	se	k3xPyFc4
o	o	k7c4
toto	tento	k3xDgNnSc4
nezajímal	zajímat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
sedmé	sedmý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
přišel	přijít	k5eAaPmAgInS
na	na	k7c4
můstek	můstek	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
Murdoch	Murdoch	k1gMnSc1
a	a	k8xC
vystřídal	vystřídat	k5eAaPmAgMnS
Lightollera	Lightoller	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
odešel	odejít	k5eAaPmAgMnS
na	na	k7c6
večeři	večeře	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murdoch	Murdoch	k1gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
k	k	k7c3
ledovému	ledový	k2eAgNnSc3d1
poli	pole	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zlepšení	zlepšení	k1gNnSc4
pozorování	pozorování	k1gNnSc2
požádal	požádat	k5eAaPmAgMnS
námořníka	námořník	k1gMnSc4
Samuela	Samuel	k1gMnSc4
Hemminga	Hemming	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
uzavřel	uzavřít	k5eAaPmAgInS
všechny	všechen	k3xTgInPc4
otvory	otvor	k1gInPc4
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
na	na	k7c4
přední	přední	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
pronikalo	pronikat	k5eAaImAgNnS
světlo	světlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
byla	být	k5eAaImAgFnS
zachycena	zachytit	k5eAaPmNgFnS
zpráva	zpráva	k1gFnSc1
parníku	parník	k1gInSc2
Californian	Californiany	k1gInPc2
a	a	k8xC
parníku	parník	k1gInSc6
Antillian	Antilliana	k1gFnPc2
<g/>
:	:	kIx,
tři	tři	k4xCgFnPc1
velké	velká	k1gFnPc1
ledovce	ledovec	k1gInSc2
na	na	k7c4
pozici	pozice	k1gFnSc4
42	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ledovec	ledovec	k1gInSc1
poblíž	poblíž	k7c2
ztroskotání	ztroskotání	k1gNnSc2
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jeho	jeho	k3xOp3gFnSc2
paty	pata	k1gFnSc2
byla	být	k5eAaImAgFnS
nalezena	naleznout	k5eAaPmNgFnS,k5eAaBmNgFnS
červená	červený	k2eAgFnSc1d1
barva	barva	k1gFnSc1
–	–	k?
důkaz	důkaz	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nedávno	nedávno	k6eAd1
otřel	otřít	k5eAaPmAgMnS
o	o	k7c4
nějakou	nějaký	k3yIgFnSc4
loď	loď	k1gFnSc4
</s>
<s>
Kolem	kolem	k7c2
osmé	osmý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
večer	večer	k6eAd1
proběhlo	proběhnout	k5eAaPmAgNnS
další	další	k2eAgNnSc1d1
střídání	střídání	k1gNnSc1
a	a	k8xC
na	na	k7c4
místa	místo	k1gNnPc4
mladších	mladý	k2eAgMnPc2d2
důstojníků	důstojník	k1gMnPc2
nastoupili	nastoupit	k5eAaPmAgMnP
Boxhall	Boxhalla	k1gFnPc2
a	a	k8xC
Moody	Mooda	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
vystřídány	vystřídán	k2eAgFnPc1d1
hlídky	hlídka	k1gFnPc1
ve	v	k7c6
vraním	vraní	k2eAgNnSc6d1
hnízdě	hnízdo	k1gNnSc6
a	a	k8xC
taktéž	taktéž	k?
kormidelník	kormidelník	k1gMnSc1
Robert	Robert	k1gMnSc1
Hitchens	Hitchens	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
můstku	můstek	k1gInSc6
zůstal	zůstat	k5eAaPmAgMnS
důstojník	důstojník	k1gMnSc1
Lightoller	Lightoller	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
končila	končit	k5eAaImAgFnS
služba	služba	k1gFnSc1
až	až	k6eAd1
ve	v	k7c6
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moody	Mood	k1gInPc7
byl	být	k5eAaImAgMnS
Lightollerem	Lightoller	k1gMnSc7
požádán	požádat	k5eAaPmNgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podle	podle	k7c2
posledních	poslední	k2eAgMnPc2d1
údajů	údaj	k1gInPc2
provedl	provést	k5eAaPmAgInS
přepočet	přepočet	k1gInSc1
vzájemné	vzájemný	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
lodě	loď	k1gFnSc2
a	a	k8xC
oblasti	oblast	k1gFnSc2
ledovců	ledovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výsledku	výsledek	k1gInSc2
mělo	mít	k5eAaImAgNnS
k	k	k7c3
setkání	setkání	k1gNnSc3
dojít	dojít	k5eAaPmF
až	až	k6eAd1
před	před	k7c7
jedenáctou	jedenáctý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Kolem	kolem	k7c2
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
hovořil	hovořit	k5eAaImAgMnS
kapitán	kapitán	k1gMnSc1
Smith	Smith	k1gMnSc1
s	s	k7c7
2	#num#	k4
<g/>
.	.	kIx.
důstojníkem	důstojník	k1gMnSc7
Lightollerem	Lightoller	k1gMnSc7
o	o	k7c6
vývoji	vývoj	k1gInSc6
počasí	počasí	k1gNnSc1
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc1
teploty	teplota	k1gFnSc2
nad	nad	k7c7
oceánem	oceán	k1gInSc7
a	a	k8xC
o	o	k7c6
pravděpodobném	pravděpodobný	k2eAgNnSc6d1
setkání	setkání	k1gNnSc6
s	s	k7c7
plovoucím	plovoucí	k2eAgInSc7d1
ledem	led	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
nevál	vát	k5eNaImAgInS
vítr	vítr	k1gInSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
hladina	hladina	k1gFnSc1
oceánu	oceán	k1gInSc2
klidná	klidný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
vln	vlna	k1gFnPc2
na	na	k7c6
hladině	hladina	k1gFnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
tříštily	tříštit	k5eAaImAgFnP
o	o	k7c4
ledovec	ledovec	k1gInSc4
a	a	k8xC
vzniklá	vzniklý	k2eAgFnSc1d1
pěna	pěna	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
lépe	dobře	k6eAd2
pozorovatelná	pozorovatelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
tmavý	tmavý	k2eAgInSc1d1
ledovec	ledovec	k1gInSc1
na	na	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
další	další	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
výskytu	výskyt	k1gInSc6
ledu	led	k1gInSc2
z	z	k7c2
parníku	parník	k1gInSc2
Mesaba	Mesaba	k1gFnSc1
<g/>
:	:	kIx,
led	led	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
41	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
až	až	k9
42	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
<g/>
,	,	kIx,
velké	velký	k2eAgInPc1d1
kusy	kus	k1gInPc1
ledu	led	k1gInSc2
a	a	k8xC
značný	značný	k2eAgInSc4d1
počet	počet	k1gInSc4
velkých	velký	k2eAgFnPc2d1
ledových	ledový	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
ledová	ledový	k2eAgNnPc4d1
pole	pole	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
22	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
se	se	k3xPyFc4
parník	parník	k1gInSc1
Californian	Californiana	k1gFnPc2
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
ledového	ledový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
22	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
přišla	přijít	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
z	z	k7c2
parníku	parník	k1gInSc2
Rappahannock	Rappahannocka	k1gFnPc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Propluli	proplout	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
ledovým	ledový	k2eAgNnSc7d1
polem	pole	k1gNnSc7
a	a	k8xC
mezi	mezi	k7c7
několika	několik	k4yIc7
ledovými	ledový	k2eAgFnPc7d1
horami	hora	k1gFnPc7
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
23	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
zpozoroval	zpozorovat	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
parníku	parník	k1gInSc2
Californian	Californian	k1gMnSc1
na	na	k7c6
obzoru	obzor	k1gInSc6
světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
3	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
světla	světlo	k1gNnPc4
osobního	osobní	k2eAgInSc2d1
parníku	parník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
23	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
se	se	k3xPyFc4
radista	radista	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Evans	Evans	k1gInSc4
na	na	k7c6
palubě	paluba	k1gFnSc6
parníku	parník	k1gInSc2
Californian	Californian	k1gMnSc1
snažil	snažit	k5eAaImAgMnS
varovat	varovat	k5eAaImF
Titanic	Titanic	k1gInSc4
před	před	k7c7
ledovými	ledový	k2eAgNnPc7d1
poli	pole	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radista	radista	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
jej	on	k3xPp3gNnSc4
však	však	k9
přerušil	přerušit	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
zpráva	zpráva	k1gFnSc1
nebyla	být	k5eNaImAgFnS
předcházena	předcházet	k5eAaImNgFnS
kódem	kód	k1gInSc7
pro	pro	k7c4
doručení	doručení	k1gNnSc4
na	na	k7c4
můstek	můstek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čase	čas	k1gInSc6
23	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
Evans	Evans	k1gInSc1
ukončil	ukončit	k5eAaPmAgInS
vysílání	vysílání	k1gNnSc4
a	a	k8xC
unavený	unavený	k2eAgMnSc1d1
šel	jít	k5eAaImAgMnS
spát	spát	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Není	být	k5eNaImIp3nS
bez	bez	k7c2
zajímavosti	zajímavost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
ten	ten	k3xDgInSc1
den	den	k1gInSc1
fungoval	fungovat	k5eAaImAgInS
telegram	telegram	k1gInSc4
jedině	jedině	k6eAd1
díky	díky	k7c3
radistovi	radista	k1gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
zdařilé	zdařilý	k2eAgFnSc3d1
opravě	oprava	k1gFnSc3
<g/>
,	,	kIx,
přestože	přestože	k8xS
to	ten	k3xDgNnSc1
nebylo	být	k5eNaImAgNnS
v	v	k7c6
popisu	popis	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
pluje	plout	k5eAaImIp3nS
stále	stále	k6eAd1
rychlostí	rychlost	k1gFnSc7
kolem	kolem	k7c2
21	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
39	#num#	k4
<g/>
–	–	k?
<g/>
41	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
směrem	směr	k1gInSc7
k	k	k7c3
ledovému	ledový	k2eAgNnSc3d1
poli	pole	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Ledovec	ledovec	k1gInSc1
</s>
<s>
Plavba	plavba	k1gFnSc1
Titanicu	Titanicus	k1gInSc2
a	a	k8xC
poloha	poloha	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
vraku	vrak	k1gInSc2
</s>
<s>
Pár	pár	k4xCyI
minut	minuta	k1gFnPc2
po	po	k7c4
varování	varování	k1gNnSc4
od	od	k7c2
Californianu	Californian	k1gInSc2
hlídka	hlídka	k1gFnSc1
v	v	k7c6
předním	přední	k2eAgNnSc6d1
vraním	vraní	k2eAgNnSc6d1
hnízdě	hnízdo	k1gNnSc6
(	(	kIx(
<g/>
ve	v	k7c6
složení	složení	k1gNnSc6
Reginald	Reginald	k1gMnSc1
Robinson	Robinson	k1gMnSc1
Lee	Lea	k1gFnSc6
a	a	k8xC
Frederick	Frederick	k1gMnSc1
Fleet	Fleet	k1gMnSc1
<g/>
)	)	kIx)
zpozorovala	zpozorovat	k5eAaPmAgFnS
mlžný	mlžný	k2eAgInSc4d1
opar	opar	k1gInSc4
vpředu	vpředu	k6eAd1
<g/>
,	,	kIx,
před	před	k7c7
přídí	příď	k1gFnSc7
(	(	kIx(
<g/>
známka	známka	k1gFnSc1
nebo	nebo	k8xC
rovnou	rovnou	k6eAd1
potvrzení	potvrzení	k1gNnSc4
blízkosti	blízkost	k1gFnSc2
ledovce	ledovec	k1gInSc2
<g/>
,	,	kIx,
význam	význam	k1gInSc1
výpovědi	výpověď	k1gFnSc2
o	o	k7c6
oparu	opar	k1gInSc6
je	být	k5eAaImIp3nS
ale	ale	k9
nejasný	jasný	k2eNgInSc1d1
–	–	k?
oba	dva	k4xCgMnPc1
dostali	dostat	k5eAaPmAgMnP
po	po	k7c6
katastrofě	katastrofa	k1gFnSc6
doživotní	doživotní	k2eAgNnSc1d1
pracovní	pracovní	k2eAgNnSc1d1
místo	místo	k1gNnSc1
a	a	k8xC
společnost	společnost	k1gFnSc1
WSL	WSL	kA
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
instruovala	instruovat	k5eAaBmAgFnS
o	o	k7c6
obsahu	obsah	k1gInSc6
a	a	k8xC
způsobu	způsob	k1gInSc6
výpovědi	výpověď	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důstojník	důstojník	k1gMnSc1
Murdoch	Murdoch	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
měl	mít	k5eAaImAgInS
hlídku	hlídka	k1gFnSc4
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
můstku	můstek	k1gInSc2
<g/>
,	,	kIx,
zaznamenal	zaznamenat	k5eAaPmAgInS
ledovec	ledovec	k1gInSc1
asi	asi	k9
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
700	#num#	k4
m	m	kA
<g/>
,	,	kIx,
ledovec	ledovec	k1gInSc1
chvíli	chvíle	k1gFnSc4
pozoroval	pozorovat	k5eAaImAgInS
pro	pro	k7c4
zjištění	zjištění	k1gNnSc4
vzájemné	vzájemný	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
běžel	běžet	k5eAaImAgMnS
ke	k	k7c3
kormidlu	kormidlo	k1gNnSc3
<g/>
,	,	kIx,
nařídil	nařídit	k5eAaPmAgInS
kormidelníkovi	kormidelník	k1gMnSc3
Hitchensovi	Hitchens	k1gMnSc3
„	„	k?
<g/>
docela	docela	k6eAd1
vpravo	vpravo	k6eAd1
<g/>
“	“	k?
–	–	k?
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
lodní	lodní	k2eAgFnSc6d1
terminologii	terminologie	k1gFnSc6
roku	rok	k1gInSc2
1912	#num#	k4
znamená	znamenat	k5eAaImIp3nS
záď	záď	k1gFnSc1
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
příď	příď	k1gFnSc1
doleva	doleva	k6eAd1
a	a	k8xC
na	na	k7c6
telegrafu	telegraf	k1gInSc6
signalizoval	signalizovat	k5eAaImAgMnS
strojovně	strojovna	k1gFnSc6
plné	plný	k2eAgNnSc4d1
zastavení	zastavení	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
správný	správný	k2eAgInSc4d1
a	a	k8xC
učebnicový	učebnicový	k2eAgInSc4d1
postup	postup	k1gInSc4
pro	pro	k7c4
vyhnutí	vyhnutí	k1gNnSc4
se	se	k3xPyFc4
kolizi	kolize	k1gFnSc4
(	(	kIx(
<g/>
navíc	navíc	k6eAd1
bylo	být	k5eAaImAgNnS
vyhnutí	vyhnutí	k1gNnSc1
se	s	k7c7
směrem	směr	k1gInSc7
doleva	doleva	k6eAd1
díky	díky	k7c3
orientaci	orientace	k1gFnSc3
listů	list	k1gInPc2
šroubů	šroub	k1gInPc2
o	o	k7c4
něco	něco	k3yInSc4
rychlejší	rychlý	k2eAgNnSc1d2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fleet	Fleet	k1gInSc1
posléze	posléze	k6eAd1
rozeznal	rozeznat	k5eAaPmAgInS
ledovou	ledový	k2eAgFnSc4d1
masu	masa	k1gFnSc4
před	před	k7c7
nimi	on	k3xPp3gInPc7
a	a	k8xC
v	v	k7c6
23	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
třemi	tři	k4xCgInPc7
údery	úder	k1gInPc4
zvonce	zvonec	k1gInSc2
signalizoval	signalizovat	k5eAaImAgMnS
„	„	k?
<g/>
Předmět	předmět	k1gInSc4
před	před	k7c7
lodí	loď	k1gFnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
pomocí	pomocí	k7c2
telefonu	telefon	k1gInSc2
předal	předat	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
důstojníkovi	důstojník	k1gMnSc3
J.	J.	kA
P.	P.	kA
Moodymu	Moodym	k1gInSc6
na	na	k7c4
můstek	můstek	k1gInSc4
hlášení	hlášení	k1gNnSc2
„	„	k?
<g/>
Led	led	k1gInSc1
přímo	přímo	k6eAd1
před	před	k7c7
námi	my	k3xPp1nPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moody	Moodo	k1gNnPc7
se	se	k3xPyFc4
na	na	k7c6
telefonu	telefon	k1gInSc6
zeptal	zeptat	k5eAaPmAgMnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
What	What	k1gInSc1
did	did	k?
you	you	k?
see	see	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
=	=	kIx~
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
jste	být	k5eAaImIp2nP
viděli	vidět	k5eAaImAgMnP
a	a	k8xC
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Thank	Thank	k1gInSc1
you	you	k?
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Děkuji	děkovat	k5eAaImIp1nS
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
se	se	k3xPyFc4
už	už	k6eAd1
totiž	totiž	k9
loď	loď	k1gFnSc1
asi	asi	k9
5	#num#	k4
sekund	sekunda	k1gFnPc2
stáčela	stáčet	k5eAaImAgFnS
a	a	k8xC
tak	tak	k6eAd1
nebylo	být	k5eNaImAgNnS
třeba	třeba	k6eAd1
spěchat	spěchat	k5eAaImF
(	(	kIx(
<g/>
Moody	Mooda	k1gFnSc2
viděl	vidět	k5eAaImAgMnS
Hitchense	Hitchense	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřovací	vyšetřovací	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
byla	být	k5eAaImAgFnS
poněkud	poněkud	k6eAd1
zveličena	zveličen	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
nepřítomnosti	nepřítomnost	k1gFnSc2
dalekohledů	dalekohled	k1gInPc2
ve	v	k7c6
skříňce	skříňka	k1gFnSc6
strážného	strážný	k2eAgInSc2d1
koše	koš	k1gInSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
–	–	k?
byly	být	k5eAaImAgInP
zde	zde	k6eAd1
nejpozději	pozdě	k6eAd3
při	při	k7c6
plavebních	plavební	k2eAgFnPc6d1
zkouškách	zkouška	k1gFnPc6
Titanicu	Titanicus	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
–	–	k?
pak	pak	k6eAd1
se	se	k3xPyFc4
však	však	k9
z	z	k7c2
dnes	dnes	k6eAd1
neznámých	známý	k2eNgInPc2d1
důvodů	důvod	k1gInPc2
ztratily	ztratit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalekohled	dalekohled	k1gInSc4
však	však	k9
především	především	k6eAd1
slouží	sloužit	k5eAaImIp3nS
(	(	kIx(
<g/>
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
)	)	kIx)
na	na	k7c6
moři	moře	k1gNnSc6
ke	k	k7c3
krátkodobému	krátkodobý	k2eAgNnSc3d1
ulevování	ulevování	k1gNnSc3
očím	oko	k1gNnPc3
(	(	kIx(
<g/>
hlídka	hlídka	k1gFnSc1
na	na	k7c6
vraním	vraní	k2eAgNnSc6d1
hnízdě	hnízdo	k1gNnSc6
čelila	čelit	k5eAaImAgFnS
40	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
rychlému	rychlý	k2eAgInSc3d1
mrazivému	mrazivý	k2eAgInSc3d1
větru	vítr	k1gInSc3
bez	bez	k7c2
brýlí	brýle	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Použití	použití	k1gNnSc1
dalekohledu	dalekohled	k1gInSc2
k	k	k7c3
detekci	detekce	k1gFnSc3
neosvětlených	osvětlený	k2eNgInPc2d1
objektů	objekt	k1gInPc2
není	být	k5eNaImIp3nS
ideální	ideální	k2eAgInSc1d1
(	(	kIx(
<g/>
zrak	zrak	k1gInSc1
je	být	k5eAaImIp3nS
vhodnější	vhodný	k2eAgInSc1d2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ledovec	ledovec	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
ocitl	ocitnout	k5eAaPmAgInS
v	v	k7c6
plavební	plavební	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
údajně	údajně	k6eAd1
před	před	k7c7
několika	několik	k4yIc7
málo	málo	k6eAd1
hodinami	hodina	k1gFnPc7
ve	v	k7c6
vodě	voda	k1gFnSc6
převrátil	převrátit	k5eAaPmAgInS
a	a	k8xC
stékající	stékající	k2eAgFnSc1d1
voda	voda	k1gFnSc1
jej	on	k3xPp3gMnSc4
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
tmavým	tmavý	k2eAgNnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
pro	pro	k7c4
hlídku	hlídka	k1gFnSc4
neviditelným	viditelný	k2eNgFnPc3d1
(	(	kIx(
<g/>
toto	tento	k3xDgNnSc4
tvrdil	tvrdit	k5eAaImAgMnS
před	před	k7c7
vyšetřovací	vyšetřovací	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
důstojník	důstojník	k1gMnSc1
Lightoller	Lightoller	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
„	„	k?
<g/>
zpracovala	zpracovat	k5eAaPmAgFnS
<g/>
“	“	k?
WSL	WSL	kA
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
nerozlomil	rozlomit	k5eNaPmAgInS
<g/>
;	;	kIx,
všichni	všechen	k3xTgMnPc1
ostatní	ostatní	k2eAgMnPc1d1
popisovali	popisovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
ledovec	ledovec	k1gInSc1
byl	být	k5eAaImAgInS
nádherně	nádherně	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frederic	Frederic	k1gMnSc1
Fleet	Fleet	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
ho	on	k3xPp3gMnSc4
v	v	k7c6
neobyčejně	obyčejně	k6eNd1
jasné	jasný	k2eAgFnSc6d1
noci	noc	k1gFnSc6
zpozoroval	zpozorovat	k5eAaPmAgMnS
ze	z	k7c2
strážního	strážní	k2eAgInSc2d1
koše	koš	k1gInSc2
jako	jako	k8xC,k8xS
první	první	k4xOgInSc4
<g/>
,	,	kIx,
ho	on	k3xPp3gMnSc4
rozeznal	rozeznat	k5eAaPmAgInS
jen	jen	k9
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
silueta	silueta	k1gFnSc1
zakrývala	zakrývat	k5eAaImAgFnS
hvězdy	hvězda	k1gFnPc4
(	(	kIx(
<g/>
důstojník	důstojník	k1gMnSc1
Murdoch	Murdoch	k1gMnSc1
zpozoroval	zpozorovat	k5eAaPmAgMnS
díky	díky	k7c3
tomuto	tento	k3xDgInSc3
efektu	efekt	k1gInSc3
ledovec	ledovec	k1gInSc4
před	před	k7c7
hlídkou	hlídka	k1gFnSc7
na	na	k7c6
vraním	vraní	k2eAgNnSc6d1
hnízdě	hnízdo	k1gNnSc6
<g/>
,	,	kIx,
nižší	nízký	k2eAgFnSc1d2
výška	výška	k1gFnSc1
můstku	můstek	k1gInSc2
totiž	totiž	k9
lépe	dobře	k6eAd2
kontrastovala	kontrastovat	k5eAaImAgFnS
oblohu	obloha	k1gFnSc4
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Z	z	k7c2
následně	následně	k6eAd1
provedených	provedený	k2eAgInPc2d1
pokusů	pokus	k1gInPc2
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Murdoch	Murdoch	k1gMnSc1
zahlédl	zahlédnout	k5eAaPmAgMnS
ledovec	ledovec	k1gInSc4
na	na	k7c6
samé	samý	k3xTgFnSc6
hranici	hranice	k1gFnSc6
zdravého	zdravý	k2eAgInSc2d1
lidského	lidský	k2eAgInSc2d1
zraku	zrak	k1gInSc2
a	a	k8xC
že	že	k8xS
práce	práce	k1gFnSc1
pětice	pětice	k1gFnSc1
Murdoch	Murdoch	k1gInSc4
<g/>
,	,	kIx,
Fleet	Fleet	k1gInSc4
<g/>
,	,	kIx,
Lee	Lea	k1gFnSc3
<g/>
,	,	kIx,
Hitchens	Hitchens	k1gInSc4
a	a	k8xC
Moody	Mooda	k1gMnSc2
byla	být	k5eAaImAgFnS
naprosto	naprosto	k6eAd1
profesionální	profesionální	k2eAgFnSc1d1
(	(	kIx(
<g/>
k	k	k7c3
pokusům	pokus	k1gInPc3
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
i	i	k9
sesterská	sesterský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Olympic	Olympic	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Srážka	srážka	k1gFnSc1
</s>
<s>
Vyhýbací	Vyhýbací	k2eAgInSc1d1
manévr	manévr	k1gInSc1
a	a	k8xC
srážka	srážka	k1gFnSc1
s	s	k7c7
ledovcem	ledovec	k1gInSc7
</s>
<s>
Od	od	k7c2
hlášení	hlášení	k1gNnSc2
z	z	k7c2
vraního	vraní	k2eAgNnSc2d1
hnízda	hnízdo	k1gNnSc2
do	do	k7c2
kolize	kolize	k1gFnSc2
s	s	k7c7
ledovcem	ledovec	k1gInSc7
uplynulo	uplynout	k5eAaPmAgNnS
37	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
prvního	první	k4xOgNnSc2
zahlédnutí	zahlédnutí	k1gNnSc2
Murdochem	Murdoch	k1gInSc7
asi	asi	k9
60	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Titanic	Titanic	k1gInSc1
měl	mít	k5eAaImAgInS
velkou	velký	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
<g/>
,	,	kIx,
obrovskou	obrovský	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
a	a	k8xC
tedy	tedy	k9
i	i	k9
velkou	velký	k2eAgFnSc4d1
setrvačnost	setrvačnost	k1gFnSc4
<g/>
,	,	kIx,
malé	malý	k2eAgNnSc4d1
ale	ale	k8xC
na	na	k7c4
tehdejší	tehdejší	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
dostatečné	dostatečný	k2eAgNnSc1d1
kormidlo	kormidlo	k1gNnSc1
<g/>
,	,	kIx,
ledovec	ledovec	k1gInSc1
byl	být	k5eAaImAgInS
zpozorován	zpozorovat	k5eAaPmNgInS
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murdoch	Murdoch	k1gInSc1
vydal	vydat	k5eAaPmAgInS
povely	povel	k1gInPc7
zcela	zcela	k6eAd1
logické	logický	k2eAgNnSc1d1
–	–	k?
vydal	vydat	k5eAaPmAgInS
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabránil	zabránit	k5eAaPmAgInS
čelní	čelní	k2eAgFnSc3d1
srážce	srážka	k1gFnSc3
s	s	k7c7
neznámým	známý	k2eNgInSc7d1
předmětem	předmět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
výsledné	výsledný	k2eAgNnSc1d1
roztržení	roztržení	k1gNnSc1
boku	bok	k1gInSc2
lodi	loď	k1gFnSc2
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
Titanic	Titanic	k1gInSc4
daleko	daleko	k6eAd1
více	hodně	k6eAd2
devastující	devastující	k2eAgInSc4d1
než	než	k8xS
čelní	čelní	k2eAgInSc4d1
náraz	náraz	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
otázkou	otázka	k1gFnSc7
rychlosti	rychlost	k1gFnSc2
lodi	loď	k1gFnSc2
(	(	kIx(
<g/>
určuje	určovat	k5eAaImIp3nS
kapitán	kapitán	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
)	)	kIx)
i	i	k8xC
konstrukce	konstrukce	k1gFnSc1
lodi	loď	k1gFnSc2
a	a	k8xC
nikoliv	nikoliv	k9
povelů	povel	k1gInPc2
důstojníka	důstojník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
Murdoch	Murdoch	k1gMnSc1
byl	být	k5eAaImAgMnS
nejčastěji	často	k6eAd3
obviňován	obviňovat	k5eAaImNgMnS
z	z	k7c2
celé	celý	k2eAgFnSc2d1
katastrofy	katastrofa	k1gFnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
neprávem	neprávo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
reakce	reakce	k1gFnSc1
vyhnutí	vyhnutí	k1gNnSc2
se	se	k3xPyFc4
překážce	překážka	k1gFnSc3
byla	být	k5eAaImAgFnS
přímo	přímo	k6eAd1
učebnicová	učebnicový	k2eAgFnSc1d1
–	–	k?
nemohl	moct	k5eNaImAgMnS
za	za	k7c4
nedostatek	nedostatek	k1gInSc4
času	čas	k1gInSc2
na	na	k7c4
takový	takový	k3xDgInSc4
manévr	manévr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
je	být	k5eAaImIp3nS
zodpovědný	zodpovědný	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
(	(	kIx(
<g/>
určuje	určovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
<g/>
)	)	kIx)
porušením	porušení	k1gNnSc7
tři	tři	k4xCgNnPc4
desetiletí	desetiletí	k1gNnPc2
platného	platný	k2eAgInSc2d1
zákazu	zákaz	k1gInSc2
používat	používat	k5eAaImF
navigační	navigační	k2eAgNnPc4d1
světla	světlo	k1gNnPc4
(	(	kIx(
<g/>
od	od	k7c2
britské	britský	k2eAgFnSc2d1
admirality	admiralita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
velmi	velmi	k6eAd1
pomalu	pomalu	k6eAd1
začal	začít	k5eAaPmAgInS
odklánět	odklánět	k5eAaImF
od	od	k7c2
svého	svůj	k3xOyFgInSc2
původního	původní	k2eAgInSc2d1
kurzu	kurz	k1gInSc2
<g/>
,	,	kIx,
přídí	příď	k1gFnPc2
doleva	doleva	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
kurzu	kurz	k1gInSc2
nestačila	stačit	k5eNaBmAgFnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
ledovci	ledovec	k1gInPc7
vyhnul	vyhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
narazil	narazit	k5eAaPmAgInS
do	do	k7c2
ledovce	ledovec	k1gInSc2
pravým	pravý	k2eAgInSc7d1
bokem	bok	k1gInSc7
hned	hned	k6eAd1
za	za	k7c2
přídí	příď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
poté	poté	k6eAd1
Murdoch	Murdoch	k1gMnSc1
zazvonil	zazvonit	k5eAaPmAgMnS
varovný	varovný	k2eAgInSc4d1
signál	signál	k1gInSc4
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgMnS
vodotěsné	vodotěsný	k2eAgFnSc2d1
přepážky	přepážka	k1gFnSc2
(	(	kIx(
<g/>
šlo	jít	k5eAaImAgNnS
je	on	k3xPp3gFnPc4
přelézt	přelézt	k5eAaPmF
žebříky	žebřík	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
posádka	posádka	k1gFnSc1
a	a	k8xC
cestující	cestující	k1gMnPc1
nepocítili	pocítit	k5eNaPmAgMnP
přímý	přímý	k2eAgInSc4d1
náraz	náraz	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
chvění	chvění	k1gNnSc4
lodi	loď	k1gFnSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
10	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
série	série	k1gFnSc1
trhlin	trhlina	k1gFnPc2
pod	pod	k7c7
čarou	čára	k1gFnSc7
ponoru	ponor	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nad	nad	k7c7
úrovní	úroveň	k1gFnSc7
dvojitého	dvojitý	k2eAgNnSc2d1
dna	dno	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
délce	délka	k1gFnSc6
90	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zasahovala	zasahovat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
vodotěsné	vodotěsný	k2eAgFnSc2d1
komory	komora	k1gFnSc2
č.	č.	k?
6	#num#	k4
<g/>
,	,	kIx,
zásobníku	zásobník	k1gInSc2
uhlí	uhlí	k1gNnSc2
pro	pro	k7c4
pátou	pátý	k4xOgFnSc4
kotelnu	kotelna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trhliny	trhlina	k1gFnSc2
se	se	k3xPyFc4
táhly	táhnout	k5eAaImAgInP
přibližně	přibližně	k6eAd1
ve	v	k7c6
výšce	výška	k1gFnSc6
3	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
kýlem	kýl	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
když	když	k8xS
na	na	k7c6
horních	horní	k2eAgFnPc6d1
palubách	paluba	k1gFnPc6
nevypadal	vypadat	k5eNaPmAgInS,k5eNaImAgInS
náraz	náraz	k1gInSc1
nijak	nijak	k6eAd1
dramaticky	dramaticky	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
podpalubí	podpalubí	k1gNnSc6
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
palubě	paluba	k1gFnSc6
D	D	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
ubytovací	ubytovací	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
topičů	topič	k1gInPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
náraz	náraz	k1gInSc1
cítit	cítit	k5eAaImF
velmi	velmi	k6eAd1
silně	silně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Topič	topič	k1gMnSc1
John	John	k1gMnSc1
Thompson	Thompson	k1gMnSc1
později	pozdě	k6eAd2
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nárazem	náraz	k1gInSc7
někteří	některý	k3yIgMnPc1
vypadli	vypadnout	k5eAaPmAgMnP
z	z	k7c2
lůžek	lůžko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
topič	topič	k1gInSc1
William	William	k1gInSc1
Small	Smalla	k1gFnPc2
všem	všecek	k3xTgInPc3
přikázal	přikázat	k5eAaPmAgInS
„	„	k?
<g/>
dolů	dolů	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
do	do	k7c2
kotelny	kotelna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
toto	tento	k3xDgNnSc1
již	již	k6eAd1
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
prostory	prostor	k1gInPc1
pod	pod	k7c7
průlezy	průlez	k1gInPc7
již	již	k6eAd1
zaplavovala	zaplavovat	k5eAaImAgFnS
voda	voda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ihned	ihned	k6eAd1
po	po	k7c6
nárazu	náraz	k1gInSc6
přišel	přijít	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
Smith	Smith	k1gMnSc1
na	na	k7c4
můstek	můstek	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
uzavřel	uzavřít	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
vodotěsné	vodotěsný	k2eAgFnPc4d1
dveře	dveře	k1gFnPc4
v	v	k7c6
přepážkách	přepážka	k1gFnPc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
komor	komora	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Komory	komora	k1gFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
vodotěsné	vodotěsný	k2eAgFnPc1d1
pouze	pouze	k6eAd1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
ale	ale	k8xC
shora	shora	k6eAd1
<g/>
,	,	kIx,
směrem	směr	k1gInSc7
k	k	k7c3
palubám	paluba	k1gFnPc3
<g/>
,	,	kIx,
tomu	ten	k3xDgNnSc3
tak	tak	k9
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plnící	plnící	k2eAgFnSc7d1
se	se	k3xPyFc4
přední	přední	k2eAgFnSc2d1
komory	komora	k1gFnSc2
zatěžovaly	zatěžovat	k5eAaImAgFnP
trup	trup	k1gInSc4
lodi	loď	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
ten	ten	k3xDgMnSc1
začal	začít	k5eAaPmAgMnS
klesat	klesat	k5eAaImF
přídí	příď	k1gFnSc7
pod	pod	k7c4
hladinu	hladina	k1gFnSc4
a	a	k8xC
voda	voda	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
začala	začít	k5eAaPmAgFnS
přelévat	přelévat	k5eAaImF
do	do	k7c2
dalších	další	k2eAgFnPc2d1
komor	komora	k1gFnPc2
přes	přes	k7c4
horní	horní	k2eAgFnSc4d1
hranu	hrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Boční	boční	k2eAgInSc1d1
nákres	nákres	k1gInSc1
lodi	loď	k1gFnSc2
s	s	k7c7
popisem	popis	k1gInSc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
podpalubí	podpalubí	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
vodotěsných	vodotěsný	k2eAgFnPc2d1
přepážek	přepážka	k1gFnPc2
(	(	kIx(
<g/>
červeně	červeně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
ledovcem	ledovec	k1gInSc7
způsobených	způsobený	k2eAgFnPc2d1
trhlin	trhlina	k1gFnPc2
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Pondělí	pondělí	k1gNnSc1
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Prohlídka	prohlídka	k1gFnSc1
poškozené	poškozený	k2eAgFnSc2d1
přídě	příď	k1gFnSc2
přinesla	přinést	k5eAaPmAgNnP
zjištění	zjištění	k1gNnPc1
<g/>
,	,	kIx,
že	že	k8xS
Titanic	Titanic	k1gInSc1
nelze	lze	k6eNd1
udržet	udržet	k5eAaPmF
na	na	k7c6
hladině	hladina	k1gFnSc6
ani	ani	k8xC
při	při	k7c6
odčerpávání	odčerpávání	k1gNnSc6
vody	voda	k1gFnSc2
balastními	balastní	k2eAgNnPc7d1
čerpadly	čerpadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1
Andrews	Andrews	k1gInSc4
odhadoval	odhadovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
loď	loď	k1gFnSc1
udrží	udržet	k5eAaPmIp3nS
na	na	k7c6
hladině	hladina	k1gFnSc6
přibližně	přibližně	k6eAd1
jednu	jeden	k4xCgFnSc4
až	až	k8xS
jednu	jeden	k4xCgFnSc4
a	a	k8xC
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
a	a	k8xC
že	že	k8xS
čerpadla	čerpadlo	k1gNnPc1
mohou	moct	k5eAaImIp3nP
přidat	přidat	k5eAaPmF
jen	jen	k9
pár	pár	k4xCyI
minut	minuta	k1gFnPc2
před	před	k7c7
potopením	potopení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkon	výkon	k1gInSc1
čerpadel	čerpadlo	k1gNnPc2
sice	sice	k8xC
dovoloval	dovolovat	k5eAaImAgInS
přečerpání	přečerpání	k1gNnSc4
1	#num#	k4
700	#num#	k4
tun	tuna	k1gFnPc2
vody	voda	k1gFnSc2
za	za	k7c4
hodinu	hodina	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
10	#num#	k4
minut	minuta	k1gFnPc2
po	po	k7c6
nárazu	náraz	k1gInSc6
dosahovala	dosahovat	k5eAaImAgFnS
úroveň	úroveň	k1gFnSc1
zatopení	zatopení	k1gNnSc2
přídě	příď	k1gFnSc2
až	až	k9
14	#num#	k4
metrů	metr	k1gInPc2
v	v	k7c6
5	#num#	k4
předních	přední	k2eAgInPc6d1
oddílech	oddíl	k1gInPc6
a	a	k8xC
hladina	hladina	k1gFnSc1
rychle	rychle	k6eAd1
stoupala	stoupat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
množství	množství	k1gNnSc1
vody	voda	k1gFnSc2
již	již	k9
nemohla	moct	k5eNaImAgFnS
čerpadla	čerpadlo	k1gNnSc2
pojmout	pojmout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záchranné	záchranný	k2eAgInPc1d1
čluny	člun	k1gInPc1
</s>
<s>
Záchranný	záchranný	k2eAgInSc1d1
člun	člun	k1gInSc1
B	B	kA
dnem	dnem	k7c2
vzhůru	vzhůru	k6eAd1
</s>
<s>
Kolem	kolem	k7c2
půlnoci	půlnoc	k1gFnSc2
<g/>
,	,	kIx,
cca	cca	kA
20	#num#	k4
minut	minuta	k1gFnPc2
po	po	k7c6
srážce	srážka	k1gFnSc6
již	již	k6eAd1
pronikla	proniknout	k5eAaPmAgFnS
voda	voda	k1gFnSc1
do	do	k7c2
obytných	obytný	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
3	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
s	s	k7c7
Andrewsem	Andrews	k1gMnSc7
se	se	k3xPyFc4
shodli	shodnout	k5eAaBmAgMnP,k5eAaPmAgMnP
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
vzniknout	vzniknout	k5eAaPmF
panika	panika	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
počet	počet	k1gInSc1
míst	místo	k1gNnPc2
v	v	k7c6
záchranných	záchranný	k2eAgInPc6d1
člunech	člun	k1gInPc6
nebyl	být	k5eNaImAgInS
dostačující	dostačující	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
cestující	cestující	k1gMnPc1
i	i	k8xC
posádka	posádka	k1gFnSc1
dostali	dostat	k5eAaPmAgMnP
záchranné	záchranný	k2eAgFnPc4d1
vesty	vesta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
chladnému	chladný	k2eAgNnSc3d1
počasí	počasí	k1gNnSc3
a	a	k8xC
ledové	ledový	k2eAgFnSc3d1
vodě	voda	k1gFnSc3
severního	severní	k2eAgInSc2d1
Atlantiku	Atlantik	k1gInSc2
mohli	moct	k5eAaImAgMnP
cestující	cestující	k1gMnPc1
přežít	přežít	k5eAaPmF
pouze	pouze	k6eAd1
5	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
odolnější	odolný	k2eAgMnPc1d2
jedinci	jedinec	k1gMnPc1
i	i	k9
20	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pracoviště	pracoviště	k1gNnPc1
radistů	radista	k1gMnPc2
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
</s>
<s>
Pět	pět	k4xCc1
minut	minuta	k1gFnPc2
po	po	k7c6
půlnoci	půlnoc	k1gFnSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
kapitán	kapitán	k1gMnSc1
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
přípravě	příprava	k1gFnSc3
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
vyklonění	vyklonění	k1gNnSc1
člunů	člun	k1gInPc2
a	a	k8xC
v	v	k7c6
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
opuštění	opuštění	k1gNnSc3
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
lodních	lodní	k2eAgFnPc2d1
zvyklostí	zvyklost	k1gFnPc2
„	„	k?
<g/>
ženy	žena	k1gFnPc1
a	a	k8xC
děti	dítě	k1gFnPc1
první	první	k4xOgMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
se	se	k3xPyFc4
první	první	k4xOgInPc4
čluny	člun	k1gInPc1
začaly	začít	k5eAaPmAgInP
plnit	plnit	k5eAaImF
dětmi	dítě	k1gFnPc7
a	a	k8xC
ženami	žena	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
dosedl	dosednout	k5eAaPmAgInS
na	na	k7c4
hladinu	hladina	k1gFnSc4
první	první	k4xOgInSc1
záchranný	záchranný	k2eAgInSc1d1
člun	člun	k1gInSc1
číslo	číslo	k1gNnSc1
7	#num#	k4
na	na	k7c6
pravoboku	pravobok	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
řídili	řídit	k5eAaImAgMnP
nástup	nástup	k1gInSc4
důstojníci	důstojník	k1gMnPc1
Lowe	Lowe	k1gFnSc1
a	a	k8xC
Pitman	Pitman	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
65	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
naplněný	naplněný	k2eAgInSc4d1
pouze	pouze	k6eAd1
28	#num#	k4
cestujícími	cestující	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
levoboku	levobok	k1gInSc6
nastupování	nastupování	k1gNnSc2
do	do	k7c2
člunů	člun	k1gInPc2
řídil	řídit	k5eAaImAgInS
2	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
C.	C.	kA
H.	H.	kA
Lightoller	Lightoller	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
podle	podle	k7c2
rozkazu	rozkaz	k1gInSc2
ženy	žena	k1gFnSc2
a	a	k8xC
děti	dítě	k1gFnPc4
první	první	k4xOgInSc4
raději	rád	k6eAd2
spustil	spustit	k5eAaPmAgMnS
člun	člun	k1gInSc4
neúplně	úplně	k6eNd1
obsazený	obsazený	k2eAgMnSc1d1
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nS
dovolil	dovolit	k5eAaPmAgMnS
nastoupit	nastoupit	k5eAaPmF
mužům	muž	k1gMnPc3
<g/>
,	,	kIx,
i	i	k8xC
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
další	další	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
ani	ani	k8xC
děti	dítě	k1gFnPc1
právě	právě	k9
nečekaly	čekat	k5eNaImAgFnP
na	na	k7c6
nastoupení	nastoupení	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Záchranný	záchranný	k2eAgInSc1d1
člun	člun	k1gInSc1
č.	č.	k?
6	#num#	k4
a	a	k8xC
5	#num#	k4
na	na	k7c6
levoboku	levobok	k1gInSc6
byly	být	k5eAaImAgFnP
spuštěny	spuštěn	k2eAgFnPc1d1
o	o	k7c4
deset	deset	k4xCc4
minut	minuta	k1gFnPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člun	člun	k1gInSc1
č.	č.	k?
1	#num#	k4
na	na	k7c6
pravoboku	pravobok	k1gInSc6
byl	být	k5eAaImAgInS
pátým	pátý	k4xOgNnSc7
spuštěným	spuštěný	k2eAgInSc7d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
s	s	k7c7
12	#num#	k4
cestujícími	cestující	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
<g/>
,	,	kIx,
člun	člun	k1gInSc1
č.	č.	k?
11	#num#	k4
byl	být	k5eAaImAgInS
přeplněn	přeplnit	k5eAaPmNgInS
70	#num#	k4
cestujícími	cestující	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládací	skládací	k2eAgInSc1d1
člun	člun	k1gInSc1
D	D	kA
byl	být	k5eAaImAgInS
posledním	poslední	k2eAgInSc7d1
spuštěným	spuštěný	k2eAgInSc7d1
člunem	člun	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Spouštění	spouštění	k1gNnSc1
člunů	člun	k1gInPc2
se	se	k3xPyFc4
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
neobešlo	neobešlo	k1gNnSc1
bez	bez	k7c2
potíží	potíž	k1gFnPc2
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
posádkou	posádka	k1gFnSc7
nacvičeno	nacvičen	k2eAgNnSc4d1
a	a	k8xC
ani	ani	k9
cestující	cestující	k1gMnPc4
<g/>
,	,	kIx,
pod	pod	k7c7
vlivem	vliv	k1gInSc7
stále	stále	k6eAd1
svítících	svítící	k2eAgNnPc2d1
světel	světlo	k1gNnPc2
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
,	,	kIx,
nechtěli	chtít	k5eNaImAgMnP
nastupovat	nastupovat	k5eAaImF
do	do	k7c2
člunů	člun	k1gInPc2
a	a	k8xC
ponořit	ponořit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
tmy	tma	k1gFnSc2
nad	nad	k7c7
oceánem	oceán	k1gInSc7
a	a	k8xC
taktéž	taktéž	k?
se	se	k3xPyFc4
některé	některý	k3yIgInPc1
manželské	manželský	k2eAgInPc1d1
páry	pár	k1gInPc1
nechtěly	chtít	k5eNaImAgInP
rozdělit	rozdělit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastupování	nastupování	k1gNnSc1
ztěžovala	ztěžovat	k5eAaImAgFnS
i	i	k9
nevhodná	vhodný	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
záchranných	záchranný	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
když	když	k8xS
stevardi	stevard	k1gMnPc1
John	John	k1gMnSc1
Edward	Edward	k1gMnSc1
Hart	Hart	k1gMnSc1
a	a	k8xC
William	William	k1gInSc1
Denton	Denton	k1gInSc1
Cox	Cox	k1gFnPc2
úspěšně	úspěšně	k6eAd1
vedli	vést	k5eAaImAgMnP
skupiny	skupina	k1gFnSc2
cestujících	cestující	k1gMnPc2
z	z	k7c2
třetí	třetí	k4xOgFnSc2
třídy	třída	k1gFnSc2
do	do	k7c2
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
spustit	spustit	k5eAaPmF
skládací	skládací	k2eAgInPc4d1
čluny	člun	k1gInPc4
A	A	kA
a	a	k8xC
B.	B.	kA
Z	z	k7c2
Titanicu	Titanicus	k1gInSc2
se	se	k3xPyFc4
ve	v	k7c6
člunech	člun	k1gInPc6
zachránilo	zachránit	k5eAaPmAgNnS
706	#num#	k4
cestujících	cestující	k1gMnPc2
a	a	k8xC
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
dva	dva	k4xCgInPc1
záchranné	záchranný	k2eAgInPc1d1
čluny	člun	k1gInPc1
již	již	k6eAd1
nebyly	být	k5eNaImAgInP
použitelné	použitelný	k2eAgInPc1d1
<g/>
,	,	kIx,
skládací	skládací	k2eAgInSc1d1
člun	člun	k1gInSc1
B	B	kA
byl	být	k5eAaImAgInS
dnem	den	k1gInSc7
vzhůru	vzhůru	k6eAd1
<g/>
,	,	kIx,
další	další	k2eAgFnPc1d1
částečně	částečně	k6eAd1
zaplaveny	zaplaven	k2eAgFnPc1d1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
bylo	být	k5eAaImAgNnS
plátno	plátno	k1gNnSc1
bočnic	bočnice	k1gFnPc2
protrženo	protrhnout	k5eAaPmNgNnS
pádem	pád	k1gInSc7
ze	z	k7c2
střechy	střecha	k1gFnSc2
kajut	kajuta	k1gFnPc2
důstojníků	důstojník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radisté	radista	k1gMnPc1
</s>
<s>
Mezitím	mezitím	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
radisté	radista	k1gMnPc1
Jack	Jacko	k1gNnPc2
Phillips	Phillipsa	k1gFnPc2
a	a	k8xC
Harold	Harolda	k1gFnPc2
Bride	Brid	k1gInSc5
vysílat	vysílat	k5eAaImF
mezinárodní	mezinárodní	k2eAgInSc4d1
nouzový	nouzový	k2eAgInSc4d1
signál	signál	k1gInSc4
„	„	k?
<g/>
CQD	CQD	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
postupně	postupně	k6eAd1
zachytilo	zachytit	k5eAaPmAgNnS
několik	několik	k4yIc1
lodí	loď	k1gFnPc2
jako	jako	k8xS,k8xC
Mount	Mounta	k1gFnPc2
Temple	templ	k1gInSc5
společnosti	společnost	k1gFnSc2
Canadian	Canadian	k1gMnSc1
Pacific	Pacific	k1gMnSc1
<g/>
,	,	kIx,
Frankfurt	Frankfurt	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
společnosti	společnost	k1gFnSc2
Norddeutscher	Norddeutschra	k1gFnPc2
Lloyd	Lloyd	k1gMnSc1
<g/>
,	,	kIx,
Virginian	Virginian	k1gMnSc1
společnosti	společnost	k1gFnSc2
Allan	Allan	k1gMnSc1
Line	linout	k5eAaImIp3nS
<g/>
,	,	kIx,
sesterská	sesterský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Olympic	Olympice	k1gFnPc2
<g/>
,	,	kIx,
ruský	ruský	k2eAgInSc1d1
parník	parník	k1gInSc1
Birma	Birma	k1gFnSc1
i	i	k9
japonská	japonský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Ypiranga	Ypiranga	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Vedle	vedle	k7c2
tradičních	tradiční	k2eAgInPc2d1
znaků	znak	k1gInPc2
CQD	CQD	kA
vysílal	vysílat	k5eAaImAgInS
Jack	Jack	k1gInSc1
Phillips	Phillips	k1gInSc4
i	i	k9
nový	nový	k2eAgInSc4d1
signál	signál	k1gInSc4
„	„	k?
<g/>
SOS	sos	k1gInSc1
<g/>
“	“	k?
–	–	k?
na	na	k7c6
upozornění	upozornění	k1gNnSc6
druhého	druhý	k4xOgMnSc2
radisty	radista	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
signál	signál	k1gInSc1
byl	být	k5eAaImAgInS
již	již	k6eAd1
dříve	dříve	k6eAd2
u	u	k7c2
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
použit	použít	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
při	při	k7c6
ztroskotání	ztroskotání	k1gNnSc6
lodě	loď	k1gFnSc2
SS	SS	kA
Florida	Florida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc4
toto	tento	k3xDgNnSc4
volání	volání	k1gNnSc4
zachytil	zachytit	k5eAaPmAgMnS
radista	radista	k1gMnSc1
Cottam	Cottam	k1gInSc4
na	na	k7c6
parníku	parník	k1gInSc6
Carpathia	Carpathius	k1gMnSc2
v	v	k7c6
témže	týž	k3xTgInSc6
čase	čas	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
odvysíláno	odvysílat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
informoval	informovat	k5eAaBmAgInS
kapitána	kapitán	k1gMnSc4
Arthura	Arthur	k1gMnSc4
Rostrona	Rostron	k1gMnSc4
o	o	k7c6
nouzovém	nouzový	k2eAgNnSc6d1
vysílání	vysílání	k1gNnSc6
a	a	k8xC
ten	ten	k3xDgMnSc1
okamžitě	okamžitě	k6eAd1
nařídil	nařídit	k5eAaPmAgMnS
změnu	změna	k1gFnSc4
kurzu	kurz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carpathia	Carpathia	k1gFnSc1
byla	být	k5eAaImAgFnS
nejbližší	blízký	k2eAgFnSc7d3
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
mohla	moct	k5eAaImAgFnS
přispěchat	přispěchat	k5eAaPmF
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálenost	vzdálenost	k1gFnSc1
58	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
93	#num#	k4
km	km	kA
<g/>
)	)	kIx)
nemohla	moct	k5eNaImAgFnS
však	však	k9
překonat	překonat	k5eAaPmF
dříve	dříve	k6eAd2
než	než	k8xS
za	za	k7c4
4	#num#	k4
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
vysílali	vysílat	k5eAaImAgMnP
radisté	radista	k1gMnPc1
opravenou	opravený	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
Titanicu	Titanicus	k1gInSc2
a	a	k8xC
žádali	žádat	k5eAaImAgMnP
o	o	k7c4
pomoc	pomoc	k1gFnSc4
parník	parník	k1gInSc1
Frankfurt	Frankfurt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdělili	sdělit	k5eAaPmAgMnP
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
potápí	potápět	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Lodě	loď	k1gFnSc2
v	v	k7c6
dosahu	dosah	k1gInSc6
odpověděly	odpovědět	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
připlují	připlout	k5eAaPmIp3nP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
žádná	žádný	k3yNgFnSc1
nebyla	být	k5eNaImAgFnS
tak	tak	k6eAd1
blízko	blízko	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dosáhla	dosáhnout	k5eAaPmAgFnS
Titanicu	Titanica	k1gFnSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
potopil	potopit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Jediným	jediný	k2eAgNnSc7d1
suchozemským	suchozemský	k2eAgNnSc7d1
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
zachytilo	zachytit	k5eAaPmAgNnS
nouzové	nouzový	k2eAgNnSc1d1
volání	volání	k1gNnSc1
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
radiová	radiový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Cape	capat	k5eAaImIp3nS
Race	Race	k1gInSc4
na	na	k7c4
Newfoundlandu	Newfoundlanda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
na	na	k7c6
některých	některý	k3yIgFnPc6
lodích	loď	k1gFnPc6
pochopili	pochopit	k5eAaPmAgMnP
závažnost	závažnost	k1gFnSc4
situace	situace	k1gFnSc2
okamžitě	okamžitě	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
jiných	jiný	k2eAgInPc6d1
ještě	ještě	k6eAd1
v	v	k7c4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
90	#num#	k4
minut	minuta	k1gFnPc2
po	po	k7c6
prvním	první	k4xOgNnSc6
vysílání	vysílání	k1gNnSc6
CQD	CQD	kA
<g/>
,	,	kIx,
neměli	mít	k5eNaImAgMnP
o	o	k7c6
situaci	situace	k1gFnSc6
reálný	reálný	k2eAgInSc4d1
obraz	obraz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Olympicu	Olympicus	k1gInSc2
<g/>
,	,	kIx,
sesterské	sesterský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
tázali	tázat	k5eAaImAgMnP
<g/>
,	,	kIx,
pluje	plout	k5eAaImIp3nS
<g/>
-li	-li	k?
Titanic	Titanic	k1gInSc4
jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
a	a	k8xC
z	z	k7c2
parníku	parník	k1gInSc2
Frankfurt	Frankfurt	k1gInSc4
stále	stále	k6eAd1
žádali	žádat	k5eAaImAgMnP
o	o	k7c4
více	hodně	k6eAd2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
kapitán	kapitán	k1gMnSc1
Smith	Smith	k1gMnSc1
osobně	osobně	k6eAd1
navštívil	navštívit	k5eAaPmAgMnS
radisty	radista	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jim	on	k3xPp3gFnPc3
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
udělali	udělat	k5eAaPmAgMnP
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
mohli	moct	k5eAaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
nařídil	nařídit	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
opustit	opustit	k5eAaPmF
kabinu	kabina	k1gFnSc4
a	a	k8xC
pokusit	pokusit	k5eAaPmF
se	se	k3xPyFc4
o	o	k7c4
záchranu	záchrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totéž	týž	k3xTgNnSc4
zopakoval	zopakovat	k5eAaPmAgMnS
svým	svůj	k3xOyFgMnPc3
důstojníkům	důstojník	k1gMnPc3
a	a	k8xC
všem	všecek	k3xTgMnPc3
členům	člen	k1gMnPc3
posádky	posádka	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
potkal	potkat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
ten	ten	k3xDgInSc4
moment	moment	k1gInSc4
zbývalo	zbývat	k5eAaImAgNnS
Titanicu	Titanica	k1gFnSc4
do	do	k7c2
potopení	potopení	k1gNnSc2
už	už	k6eAd1
jen	jen	k9
15	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zdůraznit	zdůraznit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
záchrana	záchrana	k1gFnSc1
již	již	k6eAd1
zcela	zcela	k6eAd1
nemožná	možný	k2eNgFnSc1d1
(	(	kIx(
<g/>
přestože	přestože	k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
radistů	radista	k1gMnPc2
se	s	k7c7
šťastnou	šťastný	k2eAgFnSc7d1
náhodou	náhoda	k1gFnSc7
zachránil	zachránit	k5eAaPmAgInS
<g/>
)	)	kIx)
–	–	k?
své	své	k1gNnSc4
hrdiny	hrdina	k1gMnSc2
si	se	k3xPyFc3
tak	tak	k9
dodnes	dodnes	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
jak	jak	k6eAd1
britská	britský	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
americká	americký	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
(	(	kIx(
<g/>
poštovní	poštovní	k2eAgMnPc1d1
zaměstnanci	zaměstnanec	k1gMnPc1
neopustili	opustit	k5eNaPmAgMnP
svou	svůj	k3xOyFgFnSc4
povinnost	povinnost	k1gFnSc4
pečovat	pečovat	k5eAaImF
o	o	k7c4
zásilky	zásilka	k1gFnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
do	do	k7c2
úplného	úplný	k2eAgInSc2d1
konce	konec	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
britští	britský	k2eAgMnPc1d1
námořníci	námořník	k1gMnPc1
–	–	k?
strojníci	strojník	k1gMnPc1
dodnes	dodnes	k6eAd1
nosí	nosit	k5eAaImIp3nP
svá	svůj	k3xOyFgNnPc4
hodnostní	hodnostní	k2eAgNnPc4d1
označení	označení	k1gNnPc4
na	na	k7c6
královském	královský	k2eAgInSc6d1
purpuru	purpur	k1gInSc6
na	na	k7c4
počest	počest	k1gFnSc4
svých	svůj	k3xOyFgMnPc2
kolegů	kolega	k1gMnPc2
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
neopustili	opustit	k5eNaPmAgMnP
svá	svůj	k3xOyFgNnPc4
místa	místo	k1gNnPc4
a	a	k8xC
do	do	k7c2
posledního	poslední	k2eAgInSc2d1
okamžiku	okamžik	k1gInSc2
zajišťovali	zajišťovat	k5eAaImAgMnP
hroutící	hroutící	k2eAgFnSc4d1
se	se	k3xPyFc4
elektrickou	elektrický	k2eAgFnSc4d1
síť	síť	k1gFnSc4
–	–	k?
a	a	k8xC
všichni	všechen	k3xTgMnPc1
při	při	k7c6
plnění	plnění	k1gNnSc6
svých	svůj	k3xOyFgFnPc2
povinností	povinnost	k1gFnPc2
na	na	k7c6
lodi	loď	k1gFnSc6
zahynuli	zahynout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
těmto	tento	k3xDgMnPc3
lidem	člověk	k1gMnPc3
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
Titanic	Titanic	k1gInSc1
považován	považován	k2eAgInSc1d1
nejen	nejen	k6eAd1
za	za	k7c4
katastrofu	katastrofa	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
za	za	k7c4
symbol	symbol	k1gInSc4
síly	síla	k1gFnSc2
lidského	lidský	k2eAgMnSc2d1
ducha	duch	k1gMnSc2
a	a	k8xC
„	„	k?
<g/>
britské	britský	k2eAgFnSc2d1
<g/>
“	“	k?
morálky	morálka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Potopení	potopení	k1gNnSc1
</s>
<s>
Potápějící	potápějící	k2eAgMnSc1d1
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
pozdější	pozdní	k2eAgFnSc1d2
kresba	kresba	k1gFnSc1
očitého	očitý	k2eAgMnSc2d1
svědka	svědek	k1gMnSc2
Willyho	Willy	k1gMnSc2
Stöwera	Stöwer	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Typ	typ	k1gInSc1
možného	možný	k2eAgNnSc2d1
poškození	poškození	k1gNnSc2
po	po	k7c6
nárazu	náraz	k1gInSc6
do	do	k7c2
ledovce	ledovec	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
Titanic	Titanic	k1gInSc1
utrpěl	utrpět	k5eAaPmAgInS
ve	v	k7c6
23	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
hodin	hodina	k1gFnPc2
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
</s>
<s>
Přibližně	přibližně	k6eAd1
130	#num#	k4
minut	minuta	k1gFnPc2
po	po	k7c6
nárazu	náraz	k1gInSc6
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
voda	voda	k1gFnSc1
přelévat	přelévat	k5eAaImF
ze	z	k7c2
šesté	šestý	k4xOgFnSc2
do	do	k7c2
sedmé	sedmý	k4xOgFnSc2
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
se	se	k3xPyFc4
začala	začít	k5eAaPmAgNnP
zvedat	zvedat	k5eAaImF
z	z	k7c2
vody	voda	k1gFnSc2
záď	záď	k1gFnSc4
a	a	k8xC
začaly	začít	k5eAaPmAgFnP
se	se	k3xPyFc4
vynořovat	vynořovat	k5eAaImF
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
hladina	hladina	k1gFnSc1
lodní	lodní	k2eAgFnSc2d1
paluby	paluba	k1gFnSc2
a	a	k8xC
krátce	krátce	k6eAd1
poté	poté	k6eAd1
se	se	k3xPyFc4
zhroutil	zhroutit	k5eAaPmAgInS
přední	přední	k2eAgInSc1d1
komín	komín	k1gInSc1
<g/>
,	,	kIx,
když	když	k8xS
popraskala	popraskat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnPc4
kotvící	kotvící	k2eAgNnPc4d1
lana	lano	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komín	Komín	k1gInSc1
pádem	pád	k1gInSc7
rozdrtil	rozdrtit	k5eAaPmAgInS
několik	několik	k4yIc4
lidí	člověk	k1gMnPc2
na	na	k7c6
palubě	paluba	k1gFnSc6
a	a	k8xC
i	i	k9
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
palubě	paluba	k1gFnSc6
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
snažili	snažit	k5eAaImAgMnP
dostat	dostat	k5eAaPmF
na	na	k7c4
záď	záď	k1gFnSc4
Titanicu	Titanicus	k1gInSc2
nebo	nebo	k8xC
skákali	skákat	k5eAaImAgMnP
přes	přes	k7c4
palubu	paluba	k1gFnSc4
v	v	k7c6
naději	naděje	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ještě	ještě	k6eAd1
dostanou	dostat	k5eAaPmIp3nP
do	do	k7c2
některého	některý	k3yIgInSc2
záchranného	záchranný	k2eAgInSc2d1
člunu	člun	k1gInSc2
na	na	k7c6
hladině	hladina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
zvyšujícím	zvyšující	k2eAgMnSc7d1
se	se	k3xPyFc4
náklonem	náklon	k1gInSc7
začaly	začít	k5eAaPmAgInP
padat	padat	k5eAaImF
nezajištěné	zajištěný	k2eNgInPc1d1
předměty	předmět	k1gInPc1
na	na	k7c6
palubě	paluba	k1gFnSc6
i	i	k8xC
ve	v	k7c6
vnitřku	vnitřek	k1gInSc6
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
dalším	další	k2eAgNnSc7d1
zaplavováním	zaplavování	k1gNnSc7
podpalubí	podpalubí	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zaplavení	zaplavení	k1gNnSc3
posledních	poslední	k2eAgFnPc2d1
kotelen	kotelna	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
byly	být	k5eAaImAgFnP
vyřazeny	vyřadit	k5eAaPmNgInP
i	i	k9
generátory	generátor	k1gInPc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
–	–	k?
Titanic	Titanic	k1gInSc1
se	se	k3xPyFc4
ponořil	ponořit	k5eAaPmAgInS
do	do	k7c2
tmy	tma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
se	se	k3xPyFc4
trup	trup	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
rozlomil	rozlomit	k5eAaPmAgMnS
za	za	k7c7
polovinou	polovina	k1gFnSc7
délky	délka	k1gFnSc2
trupu	trup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgFnSc1d1
část	část	k1gFnSc1
šla	jít	k5eAaImAgFnS
okamžitě	okamžitě	k6eAd1
pod	pod	k7c4
hladinu	hladina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
odlehčení	odlehčení	k1gNnSc6
na	na	k7c4
chvíli	chvíle	k1gFnSc4
narovnala	narovnat	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
roztrženým	roztržený	k2eAgInSc7d1
trupem	trup	k1gInSc7
drala	drát	k5eAaImAgFnS
voda	voda	k1gFnSc1
dovnitř	dovnitř	k6eAd1
<g/>
,	,	kIx,
po	po	k7c6
chvíli	chvíle	k1gFnSc6
se	se	k3xPyFc4
počala	počnout	k5eAaPmAgFnS
potápět	potápět	k5eAaImF
a	a	k8xC
ve	v	k7c6
svislé	svislý	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
se	se	k3xPyFc4
ponořila	ponořit	k5eAaPmAgFnS
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
ve	v	k7c4
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
zmizel	zmizet	k5eAaPmAgInS
Titanic	Titanic	k1gInSc1
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
potopení	potopení	k1gNnSc6
Titanicu	Titanicus	k1gInSc2
se	se	k3xPyFc4
pouze	pouze	k6eAd1
dva	dva	k4xCgMnPc1
z	z	k7c2
20	#num#	k4
člunů	člun	k1gInPc2
vrátily	vrátit	k5eAaPmAgInP
na	na	k7c4
místo	místo	k1gNnSc4
potopení	potopení	k1gNnSc2
a	a	k8xC
hledaly	hledat	k5eAaImAgFnP
přeživší	přeživší	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záchranný	záchranný	k2eAgInSc1d1
člun	člun	k1gInSc1
číslo	číslo	k1gNnSc1
4	#num#	k4
byl	být	k5eAaImAgMnS
poměrně	poměrně	k6eAd1
blízko	blízko	k6eAd1
a	a	k8xC
vyzvedl	vyzvednout	k5eAaPmAgMnS
pět	pět	k4xCc4
lidí	člověk	k1gMnPc2
<g/>
;	;	kIx,
dva	dva	k4xCgMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
později	pozdě	k6eAd2
zemřeli	zemřít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
o	o	k7c4
hodinu	hodina	k1gFnSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
ještě	ještě	k9
záchranný	záchranný	k2eAgInSc1d1
člun	člun	k1gInSc1
číslo	číslo	k1gNnSc1
14	#num#	k4
a	a	k8xC
zachránil	zachránit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jeden	jeden	k4xCgInSc4
později	pozdě	k6eAd2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
skočili	skočit	k5eAaPmAgMnP
z	z	k7c2
paluby	paluba	k1gFnSc2
a	a	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
dostat	dostat	k5eAaPmF
do	do	k7c2
člunů	člun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některým	některý	k3yIgInPc3
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
povedlo	povést	k5eAaPmAgNnS
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
byli	být	k5eAaImAgMnP
odmítáni	odmítat	k5eAaImNgMnP
v	v	k7c6
obavě	obava	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
přeplnění	přeplnění	k1gNnSc3
člunu	člun	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
převrácení	převrácení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádky	posádka	k1gFnSc2
člunů	člun	k1gInPc2
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
odplout	odplout	k5eAaPmF
co	co	k9
nejdále	daleko	k6eAd3
od	od	k7c2
místa	místo	k1gNnSc2
potopení	potopení	k1gNnSc2
z	z	k7c2
obavy	obava	k1gFnSc2
ze	z	k7c2
sacího	sací	k2eAgInSc2d1
efektu	efekt	k1gInSc2
<g/>
;	;	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
později	pozdě	k6eAd2
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
efekt	efekt	k1gInSc1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
malý	malý	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neznámá	známý	k2eNgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
Poblíž	poblíž	k7c2
Titanicu	Titanicus	k1gInSc2
byla	být	k5eAaImAgFnS
spatřena	spatřen	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
asi	asi	k9
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
5	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
8,0	8,0	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
stála	stát	k5eAaImAgFnS
přes	přes	k7c4
noc	noc	k1gFnSc4
v	v	k7c6
ledovém	ledový	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
loď	loď	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
záhadu	záhada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
paluby	paluba	k1gFnSc2
Titanicu	Titanicus	k1gInSc2
bylo	být	k5eAaImAgNnS
vidět	vidět	k5eAaImF
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
nemohli	moct	k5eNaImAgMnP
identifikovat	identifikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohlo	moct	k5eAaImAgNnS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
o	o	k7c4
parník	parník	k1gInSc4
Californian	Californiany	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
o	o	k7c4
norskou	norský	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Samson	Samson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
loď	loď	k1gFnSc4
nereagovala	reagovat	k5eNaBmAgFnS
na	na	k7c4
rádiové	rádiový	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
a	a	k8xC
tak	tak	k9
se	s	k7c7
4	#num#	k4
<g/>
.	.	kIx.
proviantní	proviantní	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
Joseph	Joseph	k1gMnSc1
Grove	Groev	k1gFnSc2
Boxhall	Boxhall	k1gMnSc1
<g/>
,	,	kIx,
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
přímé	přímý	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
Morseovou	Morseův	k2eAgFnSc7d1
lampou	lampa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
loď	loď	k1gFnSc1
nereagovala	reagovat	k5eNaBmAgFnS
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
vystřelovat	vystřelovat	k5eAaImF
světlice	světlice	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
mezitím	mezitím	k6eAd1
přinesl	přinést	k5eAaPmAgMnS
kormidelník	kormidelník	k1gMnSc1
George	Georg	k1gMnSc2
Rowe	Rowe	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
Rowe	Rowe	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c4
signalizování	signalizování	k1gNnSc4
Morseovou	Morseův	k2eAgFnSc7d1
lampou	lampa	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záchrana	záchrana	k1gFnSc1
</s>
<s>
Vytahování	vytahování	k1gNnSc1
člunu	člun	k1gInSc2
č.	č.	k?
12	#num#	k4
na	na	k7c4
palubu	paluba	k1gFnSc4
Carpathie	Carpathie	k1gFnSc2
</s>
<s>
Carpathia	Carpathia	k1gFnSc1
</s>
<s>
Přeživší	přeživší	k2eAgMnSc1d1
na	na	k7c6
palubě	paluba	k1gFnSc6
Carpathie	Carpathie	k1gFnSc2
</s>
<s>
Nejblíže	blízce	k6eAd3
reagující	reagující	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
byla	být	k5eAaImAgFnS
Carpathia	Carpathia	k1gFnSc1
kapitána	kapitán	k1gMnSc2
Arthura	Arthur	k1gMnSc2
A.	A.	kA
Rostona	Roston	k1gMnSc2
<g/>
,	,	kIx,
patřící	patřící	k2eAgNnSc1d1
rejdařství	rejdařství	k1gNnSc1
Cunard	Cunarda	k1gFnPc2
<g/>
,	,	kIx,
o	o	k7c6
výtlaku	výtlak	k1gInSc6
8	#num#	k4
600	#num#	k4
tun	tuna	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
plula	plout	k5eAaImAgFnS
z	z	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
směrem	směr	k1gInSc7
ke	k	k7c3
Gibraltarskému	gibraltarský	k2eAgInSc3d1
průlivu	průliv	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
<g/>
,	,	kIx,
i	i	k8xC
zde	zde	k6eAd1
radisté	radista	k1gMnPc1
přijali	přijmout	k5eAaPmAgMnP
zprávy	zpráva	k1gFnPc4
o	o	k7c6
výskytu	výskyt	k1gInSc6
ledovců	ledovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radista	radista	k1gMnSc1
Cottam	Cottam	k1gInSc1
se	se	k3xPyFc4
již	již	k6eAd1
chystal	chystat	k5eAaImAgMnS
vypnout	vypnout	k5eAaPmF
radiostanici	radiostanice	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
zaslechl	zaslechnout	k5eAaPmAgMnS
první	první	k4xOgNnSc4
volání	volání	k1gNnSc4
Titanicu	Titanicus	k1gInSc2
o	o	k7c6
pomoc	pomoc	k1gFnSc1
–	–	k?
CQD	CQD	kA
a	a	k8xC
souřadnice	souřadnice	k1gFnSc1
41	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
50	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Zpráva	zpráva	k1gFnSc1
byla	být	k5eAaImAgFnS
neprodleně	prodleně	k6eNd1
doručena	doručit	k5eAaPmNgFnS
až	až	k9
ke	k	k7c3
kapitánovi	kapitán	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carpathia	Carpathia	k1gFnSc1
byla	být	k5eAaImAgFnS
vzdálena	vzdálit	k5eAaPmNgFnS
58	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
93	#num#	k4
km	km	kA
<g/>
)	)	kIx)
od	od	k7c2
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
okamžitě	okamžitě	k6eAd1
nařídil	nařídit	k5eAaPmAgMnS
kurz	kurz	k1gInSc4
308	#num#	k4
<g/>
°	°	k?
a	a	k8xC
rozestavil	rozestavit	k5eAaPmAgMnS
hlídky	hlídka	k1gFnPc4
pro	pro	k7c4
sledování	sledování	k1gNnSc4
hladiny	hladina	k1gFnSc2
a	a	k8xC
hlášení	hlášení	k1gNnSc2
pozorovaných	pozorovaný	k2eAgInPc2d1
ledovců	ledovec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
si	se	k3xPyFc3
dobře	dobře	k6eAd1
vědom	vědom	k2eAgMnSc1d1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
čeká	čekat	k5eAaImIp3nS
cestující	cestující	k1gMnPc4
v	v	k7c6
ledové	ledový	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
udělal	udělat	k5eAaPmAgMnS
vše	všechen	k3xTgNnSc4
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
k	k	k7c3
nim	on	k3xPp3gFnPc3
<g />
.	.	kIx.
</s>
<s hack="1">
dorazil	dorazit	k5eAaPmAgMnS
co	co	k9
nejdříve	dříve	k6eAd3
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
si	se	k3xPyFc3
vysloužil	vysloužit	k5eAaPmAgMnS
jak	jak	k8xS,k8xC
pochvalu	pochvala	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
kritiku	kritika	k1gFnSc4
za	za	k7c4
podstoupení	podstoupení	k1gNnSc4
přílišného	přílišný	k2eAgNnSc2d1
rizika	riziko	k1gNnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
s	s	k7c7
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
14	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
26	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prokličkoval	prokličkovat	k5eAaPmAgInS
ledovým	ledový	k2eAgNnSc7d1
polem	pole	k1gNnSc7
a	a	k8xC
mezi	mezi	k7c7
krami	kra	k1gFnPc7
rychlostí	rychlost	k1gFnPc2
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
3	#num#	k4
uzly	uzel	k1gInPc1
vyšší	vysoký	k2eAgInPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
rádiová	rádiový	k2eAgFnSc1d1
depeše	depeše	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c4
Carpathii	Carpathie	k1gFnSc4
zachycena	zachytit	k5eAaPmNgFnS
v	v	k7c6
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rychlostí	rychlost	k1gFnPc2
17,5	17,5	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
32,4	32,4	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
proplula	proplout	k5eAaPmAgFnS
Carpathia	Carpathia	k1gFnSc1
k	k	k7c3
místu	místo	k1gNnSc3
ztroskotání	ztroskotání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
byla	být	k5eAaImAgFnS
Carpathia	Carpathia	k1gFnSc1
téměř	téměř	k6eAd1
na	na	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
před	před	k7c7
čtvrtou	čtvrtý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
ranní	ranní	k1gFnSc2
nechal	nechat	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
zastavit	zastavit	k5eAaPmF
stroje	stroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
ranní	ranní	k1gFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
Carpathie	Carpathie	k1gFnSc2
spatřena	spatřen	k2eAgFnSc1d1
signální	signální	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
z	z	k7c2
jednoho	jeden	k4xCgInSc2
ze	z	k7c2
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
400	#num#	k4
metrů	metr	k1gInPc2
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
necelých	celý	k2eNgNnPc2d1
deset	deset	k4xCc4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c4
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
námořníci	námořník	k1gMnPc1
vyzvedávat	vyzvedávat	k5eAaImF
první	první	k4xOgMnPc4
trosečníky	trosečník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
vyzvednutý	vyzvednutý	k2eAgInSc1d1
člun	člun	k1gInSc1
a	a	k8xC
zachránění	zachráněný	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
ze	z	k7c2
člunu	člun	k1gInSc2
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
velel	velet	k5eAaImAgInS
4	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
Boxhall	Boxhall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
následovalo	následovat	k5eAaImAgNnS
vyzvednutí	vyzvednutí	k1gNnSc1
dalších	další	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situaci	situace	k1gFnSc4
komplikovaly	komplikovat	k5eAaBmAgFnP
vlny	vlna	k1gFnPc1
a	a	k8xC
ledovce	ledovec	k1gInPc1
plující	plující	k2eAgInPc1d1
po	po	k7c6
hladině	hladina	k1gFnSc6
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
se	se	k3xPyFc4
musely	muset	k5eAaImAgInP
čluny	člun	k1gInPc1
i	i	k9
Carpathia	Carpathia	k1gFnSc1
vyhýbat	vyhýbat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
člunem	člun	k1gInSc7
byl	být	k5eAaImAgInS
člun	člun	k1gInSc1
č.	č.	k?
12	#num#	k4
pod	pod	k7c7
velením	velení	k1gNnSc7
2	#num#	k4
<g/>
.	.	kIx.
důstojníka	důstojník	k1gMnSc4
Lightollera	Lightoller	k1gMnSc4
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člun	člun	k1gInSc1
byl	být	k5eAaImAgInS
přetížen	přetížen	k2eAgMnSc1d1
<g/>
,	,	kIx,
nemohl	moct	k5eNaImAgMnS
dobře	dobře	k6eAd1
manévrovat	manévrovat	k5eAaImF
ve	v	k7c6
vlnách	vlna	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
převrhne	převrhnout	k5eAaPmIp3nS
několik	několik	k4yIc1
stovek	stovka	k1gFnPc2
metrů	metr	k1gInPc2
od	od	k7c2
Carpathie	Carpathie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
Rostron	Rostron	k1gInSc1
proto	proto	k8xC
otočil	otočit	k5eAaPmAgInS
Carpathii	Carpathie	k1gFnSc4
přídí	příď	k1gFnPc2
směrem	směr	k1gInSc7
ke	k	k7c3
člunu	člun	k1gInSc3
a	a	k8xC
tak	tak	k6eAd1
zkrátil	zkrátit	k5eAaPmAgMnS
vzdálenost	vzdálenost	k1gFnSc4
na	na	k7c6
„	„	k?
<g/>
pouhých	pouhý	k2eAgInPc2d1
<g/>
“	“	k?
100	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tak	tak	k6eAd1
ale	ale	k8xC
trosečníci	trosečník	k1gMnPc1
neměli	mít	k5eNaImAgMnP
záchranu	záchrana	k1gFnSc4
jistou	jistý	k2eAgFnSc4d1
<g/>
,	,	kIx,
během	během	k7c2
přibližování	přibližování	k1gNnSc2
se	se	k3xPyFc4
několik	několik	k4yIc1
vln	vlna	k1gFnPc2
přelilo	přelít	k5eAaPmAgNnS
přes	přes	k7c4
příď	příď	k1gFnSc4
člunu	člun	k1gInSc2
a	a	k8xC
museli	muset	k5eAaImAgMnP
obeplout	obeplout	k5eAaPmF
příď	příď	k1gFnSc4
Carpathie	Carpathie	k1gFnSc2
na	na	k7c4
závětrnou	závětrný	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
hladina	hladina	k1gFnSc1
klidnější	klidný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
byli	být	k5eAaImAgMnP
zachráněni	zachráněn	k2eAgMnPc1d1
poslední	poslední	k2eAgMnPc1d1
ztroskotanci	ztroskotanec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
se	se	k3xPyFc4
Carpathia	Carpathia	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
směrem	směr	k1gInSc7
k	k	k7c3
New	New	k1gFnSc3
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
původnímu	původní	k2eAgInSc3d1
cíli	cíl	k1gInSc3
plavby	plavba	k1gFnSc2
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oběti	oběť	k1gFnPc1
a	a	k8xC
zachránění	zachránění	k1gNnPc1
</s>
<s>
Z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
přesahujícího	přesahující	k2eAgInSc2d1
2	#num#	k4
200	#num#	k4
osob	osoba	k1gFnPc2
na	na	k7c6
palubě	paluba	k1gFnSc6
zahynulo	zahynout	k5eAaPmAgNnS
následkem	následkem	k7c2
potopení	potopení	k1gNnSc2
Titanicu	Titanicus	k1gInSc2
kolem	kolem	k7c2
1	#num#	k4
500	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
přibližně	přibližně	k6eAd1
představuje	představovat	k5eAaImIp3nS
⅔	⅔	k?
Hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
lidských	lidský	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
bylo	být	k5eAaImAgNnS
podchlazení	podchlazení	k1gNnSc4
ve	v	k7c6
studené	studený	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
o	o	k7c6
teplotě	teplota	k1gFnSc6
−	−	k?
°	°	k?
<g/>
C	C	kA
severního	severní	k2eAgInSc2d1
Atlantiku	Atlantik	k1gInSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
ledovců	ledovec	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
kdy	kdy	k6eAd1
k	k	k7c3
úmrtí	úmrtí	k1gNnSc3
na	na	k7c4
podchlazení	podchlazení	k1gNnSc4
dochází	docházet	k5eAaImIp3nS
během	během	k7c2
15	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
oběťmi	oběť	k1gFnPc7
byli	být	k5eAaImAgMnP
kapitán	kapitán	k1gMnSc1
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
a	a	k8xC
šestý	šestý	k4xOgMnSc1
důstojník	důstojník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
šéfkonstruktér	šéfkonstruktér	k1gMnSc1
lodi	loď	k1gFnSc2
se	s	k7c7
svými	svůj	k3xOyFgInPc7
8	#num#	k4
spolupracovníky	spolupracovník	k1gMnPc7
z	z	k7c2
loděnice	loděnice	k1gFnSc2
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgMnSc1d1
strojmistr	strojmistr	k1gMnSc1
se	s	k7c7
všemi	všecek	k3xTgMnPc7
strojníky	strojník	k1gMnPc7
a	a	k8xC
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřela	zemřít	k5eAaPmAgFnS
většina	většina	k1gFnSc1
z	z	k7c2
57	#num#	k4
milionářů	milionář	k1gMnPc2
cestujících	cestující	k1gMnPc6
první	první	k4xOgFnSc7
třídou	třída	k1gFnSc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
multimilionářů	multimilionář	k1gMnPc2
Johna	John	k1gMnSc2
Jacoba	Jacoba	k1gFnSc1
Astora	Astora	k1gFnSc1
<g/>
,	,	kIx,
Benjamina	Benjamin	k1gMnSc2
Guggenheima	Guggenheim	k1gMnSc2
<g/>
,	,	kIx,
Idy	Ida	k1gFnSc2
a	a	k8xC
Isidora	Isidor	k1gMnSc2
Strausových	Strausový	k2eAgFnPc2d1
a	a	k8xC
stovky	stovka	k1gFnPc4
dalších	další	k2eAgMnPc2d1
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
chladných	chladný	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
zahynulo	zahynout	k5eAaPmAgNnS
i	i	k9
všech	všecek	k3xTgInPc2
5	#num#	k4
poštovních	poštovní	k2eAgMnPc2d1
úředníků	úředník	k1gMnPc2
<g/>
;	;	kIx,
W.	W.	kA
L.	L.	kA
Gwynn	Gwynn	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
S.	S.	kA
March	March	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
R.	R.	kA
J.	J.	kA
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
B.	B.	kA
Williamson	Williamson	k1gInSc4
a	a	k8xC
O.	O.	kA
S.	S.	kA
Woody	Wooda	k1gMnSc2
<g/>
,	,	kIx,
celý	celý	k2eAgInSc1d1
lodní	lodní	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
<g/>
;	;	kIx,
T.	T.	kA
R.	R.	kA
Brailey	Brailea	k1gMnSc2
<g/>
,	,	kIx,
R.	R.	kA
M.	M.	kA
Bricoux	Bricoux	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
J.	J.	kA
F.	F.	kA
P.	P.	kA
Clarke	Clark	k1gMnSc2
<g/>
,	,	kIx,
W.	W.	kA
H.	H.	kA
Hartley	Hartlea	k1gMnSc2
<g/>
,	,	kIx,
J.	J.	kA
L.	L.	kA
Hume	Hum	k1gMnSc2
<g/>
,	,	kIx,
G.	G.	kA
A.	A.	kA
Krins	Krinsa	k1gFnPc2
<g/>
,	,	kIx,
P.	P.	kA
C.	C.	kA
Taylor	Taylor	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
W.	W.	kA
Woodward	Woodward	k1gInSc4
i	i	k8xC
J.	J.	kA
G.	G.	kA
Phillips	Phillipsa	k1gFnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
telegrafistů	telegrafista	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
chvíle	chvíle	k1gFnSc2
rádiem	rádio	k1gNnSc7
žádali	žádat	k5eAaImAgMnP
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1
přeživší	přeživší	k2eAgInSc1d1
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c4
událost	událost	k1gFnSc4
vzpomínky	vzpomínka	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
sedmiletá	sedmiletý	k2eAgFnSc1d1
holčička	holčička	k1gFnSc1
Eva	Eva	k1gFnSc1
Hartová	Hartová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zemřela	zemřít	k5eAaPmAgFnS
ve	v	k7c6
věku	věk	k1gInSc6
91	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1996	#num#	k4
(	(	kIx(
<g/>
rok	rok	k1gInSc4
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
kin	kino	k1gNnPc2
oscarový	oscarový	k2eAgInSc1d1
film	film	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c4
vzpomínání	vzpomínání	k1gNnSc4
poslední	poslední	k2eAgFnSc2d1
přeživší	přeživší	k2eAgFnSc2d1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
tehdy	tehdy	k6eAd1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
17	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
ještě	ještě	k6eAd1
sedm	sedm	k4xCc1
dětí	dítě	k1gFnPc2
mladších	mladý	k2eAgMnPc2d2
než	než	k8xS
Hartová	Hartová	k1gFnSc1
<g/>
,	,	kIx,
ty	ty	k3xPp2nSc1
si	se	k3xPyFc3
ale	ale	k9
na	na	k7c4
událost	událost	k1gFnSc4
vědomě	vědomě	k6eAd1
nepamatovaly	pamatovat	k5eNaImAgInP
<g/>
:	:	kIx,
Edith	Edith	k1gInSc1
Brown	Brown	k1gMnSc1
Haismanová	Haismanová	k1gFnSc1
<g/>
,	,	kIx,
Barbara	Barbara	k1gFnSc1
Westová	Westový	k2eAgFnSc1d1
a	a	k8xC
Millvina	Millvin	k2eAgFnSc1d1
Deanová	Deanová	k1gFnSc1
(	(	kIx(
<g/>
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Michael	Michael	k1gMnSc1
Navratil	Navratil	k1gMnSc2
<g/>
,	,	kIx,
Louise	Louis	k1gMnSc2
Larocheová	Larocheová	k1gFnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Eleanor	Eleanor	k1gMnSc1
Shumanová	Shumanová	k1gFnSc1
<g/>
,	,	kIx,
Winnifred	Winnifred	k1gMnSc1
Tongerloo	Tongerloo	k1gMnSc1
a	a	k8xC
Lillian	Lilliana	k1gFnPc2
Asplundová	Asplundový	k2eAgFnSc1d1
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmladší	mladý	k2eAgInSc4d3
a	a	k8xC
současně	současně	k6eAd1
poslední	poslední	k2eAgMnSc1d1
přeživší	přeživší	k2eAgMnSc1d1
cestující	cestující	k1gMnSc1
Titaniku	Titanic	k1gInSc2
<g/>
,	,	kIx,
Millvina	Millvin	k2eAgFnSc1d1
Deanová	Deanová	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1912	#num#	k4
a	a	k8xC
v	v	k7c6
době	doba	k1gFnSc6
plavby	plavba	k1gFnPc1
jí	on	k3xPp3gFnSc3
byly	být	k5eAaImAgFnP
2	#num#	k4
měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřela	zemřít	k5eAaPmAgFnS
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osob	osoba	k1gFnPc2
na	na	k7c6
palubě	paluba	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
/	/	kIx~
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
/	/	kIx~
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
Zachránění	zachránění	k1gNnSc1
</s>
<s>
Oběti	oběť	k1gFnPc1
</s>
<s>
I.	I.	kA
třídaděti	třídadět	k5eAaPmF,k5eAaImF
</s>
<s>
325	#num#	k4
/	/	kIx~
322	#num#	k4
/	/	kIx~
3296	#num#	k4
/	/	kIx~
5	#num#	k4
/	/	kIx~
*	*	kIx~
<g/>
203	#num#	k4
/	/	kIx~
202	#num#	k4
/	/	kIx~
1996	#num#	k4
/	/	kIx~
5	#num#	k4
/	/	kIx~
*	*	kIx~
<g/>
122	#num#	k4
/	/	kIx~
120	#num#	k4
/	/	kIx~
1300	#num#	k4
/	/	kIx~
0	#num#	k4
/	/	kIx~
*	*	kIx~
</s>
<s>
ženy	žena	k1gFnPc1
</s>
<s>
144	#num#	k4
/	/	kIx~
144	#num#	k4
/	/	kIx~
156	#num#	k4
*	*	kIx~
<g/>
140	#num#	k4
/	/	kIx~
139	#num#	k4
/	/	kIx~
199	#num#	k4
*	*	kIx~
<g/>
4	#num#	k4
/	/	kIx~
5	#num#	k4
/	/	kIx~
11	#num#	k4
*	*	kIx~
</s>
<s>
muži	muž	k1gMnPc1
</s>
<s>
175	#num#	k4
/	/	kIx~
173	#num#	k4
/	/	kIx~
17357	#num#	k4
/	/	kIx~
58	#num#	k4
/	/	kIx~
54118	#num#	k4
/	/	kIx~
115	#num#	k4
/	/	kIx~
119	#num#	k4
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
třídaděti	třídadět	k5eAaImF,k5eAaPmF
</s>
<s>
285	#num#	k4
/	/	kIx~
277	#num#	k4
/	/	kIx~
28524	#num#	k4
/	/	kIx~
24	#num#	k4
/	/	kIx~
*	*	kIx~
<g/>
118	#num#	k4
/	/	kIx~
115	#num#	k4
/	/	kIx~
11924	#num#	k4
/	/	kIx~
24	#num#	k4
/	/	kIx~
*	*	kIx~
<g/>
167	#num#	k4
/	/	kIx~
162	#num#	k4
/	/	kIx~
1660	#num#	k4
/	/	kIx~
0	#num#	k4
/	/	kIx~
*	*	kIx~
</s>
<s>
ženy	žena	k1gFnPc1
</s>
<s>
93	#num#	k4
/	/	kIx~
93	#num#	k4
/	/	kIx~
119	#num#	k4
*	*	kIx~
<g/>
80	#num#	k4
/	/	kIx~
78	#num#	k4
/	/	kIx~
104	#num#	k4
*	*	kIx~
<g/>
13	#num#	k4
/	/	kIx~
15	#num#	k4
/	/	kIx~
24	#num#	k4
*	*	kIx~
</s>
<s>
muži	muž	k1gMnPc1
</s>
<s>
168	#num#	k4
/	/	kIx~
160	#num#	k4
/	/	kIx~
15714	#num#	k4
/	/	kIx~
13	#num#	k4
/	/	kIx~
15154	#num#	k4
/	/	kIx~
147	#num#	k4
/	/	kIx~
142	#num#	k4
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
třídaděti	třídadět	k5eAaPmF,k5eAaImF
</s>
<s>
706	#num#	k4
/	/	kIx~
709	#num#	k4
/	/	kIx~
71079	#num#	k4
/	/	kIx~
76	#num#	k4
/	/	kIx~
–	–	k?
<g/>
178	#num#	k4
/	/	kIx~
176	#num#	k4
/	/	kIx~
17427	#num#	k4
/	/	kIx~
23	#num#	k4
/	/	kIx~
*	*	kIx~
<g/>
528	#num#	k4
/	/	kIx~
533	#num#	k4
/	/	kIx~
53652	#num#	k4
/	/	kIx~
53	#num#	k4
/	/	kIx~
–	–	k?
</s>
<s>
ženy	žena	k1gFnPc1
</s>
<s>
165	#num#	k4
/	/	kIx~
179	#num#	k4
/	/	kIx~
–	–	k?
<g/>
76	#num#	k4
/	/	kIx~
98	#num#	k4
/	/	kIx~
105	#num#	k4
*	*	kIx~
<g/>
89	#num#	k4
/	/	kIx~
81	#num#	k4
/	/	kIx~
–	–	k?
</s>
<s>
muži	muž	k1gMnPc1
</s>
<s>
462	#num#	k4
/	/	kIx~
454	#num#	k4
/	/	kIx~
–	–	k?
<g/>
75	#num#	k4
/	/	kIx~
55	#num#	k4
/	/	kIx~
69387	#num#	k4
/	/	kIx~
399	#num#	k4
/	/	kIx~
–	–	k?
</s>
<s>
posádkaženy	posádkažen	k2eAgFnPc1d1
</s>
<s>
885	#num#	k4
/	/	kIx~
898	#num#	k4
/	/	kIx~
89923	#num#	k4
/	/	kIx~
23	#num#	k4
/	/	kIx~
23212	#num#	k4
/	/	kIx~
210	#num#	k4
/	/	kIx~
21420	#num#	k4
/	/	kIx~
21	#num#	k4
/	/	kIx~
20673	#num#	k4
/	/	kIx~
688	#num#	k4
/	/	kIx~
6853	#num#	k4
/	/	kIx~
2	#num#	k4
/	/	kIx~
3	#num#	k4
</s>
<s>
muži	muž	k1gMnPc1
</s>
<s>
862	#num#	k4
/	/	kIx~
875	#num#	k4
/	/	kIx~
876192	#num#	k4
/	/	kIx~
189	#num#	k4
/	/	kIx~
194670	#num#	k4
/	/	kIx~
686	#num#	k4
/	/	kIx~
682	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
2	#num#	k4
201	#num#	k4
/	/	kIx~
2	#num#	k4
206	#num#	k4
/	/	kIx~
2	#num#	k4
223711	#num#	k4
/	/	kIx~
703	#num#	k4
/	/	kIx~
7061	#num#	k4
490	#num#	k4
/	/	kIx~
1	#num#	k4
503	#num#	k4
/	/	kIx~
1	#num#	k4
517	#num#	k4
</s>
<s>
vysvětlivky	vysvětlivka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
*	*	kIx~
=	=	kIx~
děti	dítě	k1gFnPc4
a	a	k8xC
ženy	žena	k1gFnPc4
dohromady	dohromady	k6eAd1
<g/>
–	–	k?
=	=	kIx~
údaj	údaj	k1gInSc1
nedostupný	dostupný	k2eNgInSc1d1
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1
</s>
<s>
Noviny	novina	k1gFnPc1
The	The	k1gFnSc2
New	New	k1gFnPc2
York	York	k1gInSc1
Herald	Herald	k1gMnSc1
přinášejí	přinášet	k5eAaImIp3nP
zprávu	zpráva	k1gFnSc4
o	o	k7c6
katastrofě	katastrofa	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
</s>
<s>
Ještě	ještě	k6eAd1
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
Carpathia	Carpathia	k1gFnSc1
se	s	k7c7
zachráněnými	zachráněná	k1gFnPc7
doplula	doplout	k5eAaPmAgFnS
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
vzhledem	vzhledem	k7c3
k	k	k7c3
rozsahu	rozsah	k1gInSc3
katastrofy	katastrofa	k1gFnSc2
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
vyšetření	vyšetření	k1gNnSc6
celé	celý	k2eAgFnSc2d1
události	událost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
zahájil	zahájit	k5eAaPmAgInS
vyšetřování	vyšetřování	k1gNnSc4
následující	následující	k2eAgInSc1d1
den	den	k1gInSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
Carpathia	Carpathia	k1gFnSc1
připlula	připlout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřování	vyšetřování	k1gNnSc2
vedl	vést	k5eAaImAgMnS
senátor	senátor	k1gMnSc1
William	William	k1gInSc4
Alden	Alden	k2eAgInSc4d1
Smith	Smith	k1gInSc4
až	až	k6eAd1
do	do	k7c2
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1912	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
vyšetřování	vyšetřování	k1gNnSc2
byly	být	k5eAaImAgFnP
zapsány	zapsán	k2eAgFnPc1d1
výpovědi	výpověď	k1gFnPc1
všech	všecek	k3xTgFnPc2
přeživších	přeživší	k2eAgFnPc2d1
a	a	k8xC
ostatní	ostatní	k2eAgNnSc4d1
svědectví	svědectví	k1gNnSc4
s	s	k7c7
případem	případ	k1gInSc7
související	související	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
ukončení	ukončení	k1gNnSc6
podání	podání	k1gNnSc1
svědectví	svědectví	k1gNnSc2
bylo	být	k5eAaImAgNnS
britským	britský	k2eAgMnPc3d1
občanům	občan	k1gMnPc3
povoleno	povolen	k2eAgNnSc4d1
opustit	opustit	k5eAaPmF
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezitím	mezitím	k6eAd1
britský	britský	k2eAgInSc1d1
tisk	tisk	k1gInSc1
očernil	očernit	k5eAaPmAgInS
senátora	senátor	k1gMnSc4
Smitha	Smith	k1gMnSc4
jako	jako	k8xS,k8xC
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
z	z	k7c2
neštěstí	neštěstí	k1gNnSc2
chtěla	chtít	k5eAaImAgFnS
vytlouci	vytlouct	k5eAaPmF
politický	politický	k2eAgInSc4d1
kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smith	Smith	k1gMnSc1
však	však	k9
měl	mít	k5eAaImAgMnS
spíše	spíše	k9
pověst	pověst	k1gFnSc4
zastánce	zastánce	k1gMnSc2
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
na	na	k7c6
železnici	železnice	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
chtěl	chtít	k5eAaImAgMnS
prozkoumat	prozkoumat	k5eAaPmF
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
společností	společnost	k1gFnSc7
Railroad	Railroad	k1gInSc4
Tycoon	Tycoon	k1gNnSc1
a	a	k8xC
Titanicem	Titanic	k1gMnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
měly	mít	k5eAaImAgFnP
společného	společný	k2eAgMnSc4d1
majitele	majitel	k1gMnSc4
<g/>
:	:	kIx,
J.	J.	kA
P.	P.	kA
Morgana	morgan	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taktéž	Taktéž	k?
na	na	k7c6
britské	britský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
byla	být	k5eAaImAgFnS
ustanovena	ustanoven	k2eAgFnSc1d1
vyšetřovací	vyšetřovací	k2eAgFnSc1d1
komise	komise	k1gFnSc1
British	British	k1gMnSc1
Board	Board	k1gMnSc1
of	of	k?
Trade	Trad	k1gMnSc5
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
vedl	vést	k5eAaImAgMnS
lord	lord	k1gMnSc1
Mersey	Mersea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
pracovala	pracovat	k5eAaImAgFnS
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1912	#num#	k4
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
vyslechla	vyslechnout	k5eAaPmAgFnS
přeživší	přeživší	k2eAgFnSc4d1
cestující	cestující	k1gFnSc4
i	i	k8xC
členy	člen	k1gMnPc4
posádky	posádka	k1gFnSc2
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
kapitána	kapitán	k1gMnSc2
Arthura	Arthur	k1gMnSc2
Rostrona	Rostron	k1gMnSc2
lodi	loď	k1gFnSc2
Carpathia	Carpathius	k1gMnSc2
<g/>
,	,	kIx,
posádku	posádka	k1gFnSc4
lodě	loď	k1gFnSc2
společnosti	společnost	k1gFnSc2
Leyland	Leylanda	k1gFnPc2
Line	linout	k5eAaImIp3nS
Californian	Californian	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Smith	Smith	k1gMnSc1
–	–	k?
senátor	senátor	k1gMnSc1
vnitrozemského	vnitrozemský	k2eAgInSc2d1
státu	stát	k1gInSc2
Unie	unie	k1gFnSc2
(	(	kIx(
<g/>
Michigan	Michigan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
vycítil	vycítit	k5eAaPmAgMnS
možnost	možnost	k1gFnSc4
se	se	k3xPyFc4
zviditelnit	zviditelnit	k5eAaPmF
<g/>
,	,	kIx,
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
učinit	učinit	k5eAaImF,k5eAaPmF
hlavního	hlavní	k2eAgMnSc4d1
viníka	viník	k1gMnSc4
katastrofy	katastrofa	k1gFnSc2
z	z	k7c2
Bruce	Bruec	k1gInSc2
Ismaye	Ismay	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
formální	formální	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
vybudované	vybudovaný	k2eAgNnSc1d1
jeho	jeho	k3xOp3gMnPc7
otcem	otec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
White	Whit	k1gInSc5
Star	star	k1gInSc1
Line	linout	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
však	však	k9
již	již	k6eAd1
dávno	dávno	k6eAd1
nepatřila	patřit	k5eNaImAgFnS
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
dávno	dávno	k6eAd1
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
amerického	americký	k2eAgInSc2d1
Morganova	morganův	k2eAgInSc2d1
trustu	trust	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ismayovou	Ismayový	k2eAgFnSc7d1
vinou	vina	k1gFnSc7
bylo	být	k5eAaImAgNnS
pak	pak	k6eAd1
patrně	patrně	k6eAd1
pouze	pouze	k6eAd1
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
přežil	přežít	k5eAaPmAgMnS
–	–	k?
média	médium	k1gNnPc1
jej	on	k3xPp3gNnSc4
vykreslila	vykreslit	k5eAaPmAgFnS
jako	jako	k9
sobeckého	sobecký	k2eAgMnSc4d1
britského	britský	k2eAgMnSc4d1
miliardáře	miliardář	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zabral	zabrat	k5eAaPmAgMnS
místo	místo	k6eAd1
dětem	dítě	k1gFnPc3
a	a	k8xC
ženám	žena	k1gFnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zachoval	zachovat	k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šek	šek	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
Ismay	Ismay	k1gInPc4
vystavil	vystavit	k5eAaPmAgMnS
všem	všecek	k3xTgMnPc3
námořníkům	námořník	k1gMnPc3
v	v	k7c6
záchranném	záchranný	k2eAgInSc6d1
člunu	člun	k1gInSc6
„	„	k?
<g/>
na	na	k7c4
úhradu	úhrada	k1gFnSc4
výstroje	výstroj	k1gFnSc2
<g/>
“	“	k?
byl	být	k5eAaImAgInS
dezinterpretován	dezinterpretovat	k5eAaImNgInS
jako	jako	k9
úplatek	úplatek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bruce	Bruec	k1gInSc2
Ismay	Ismaa	k1gFnSc2
byl	být	k5eAaImAgMnS
flegmatický	flegmatický	k2eAgMnSc1d1
<g/>
,	,	kIx,
znuděný	znuděný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
absolutně	absolutně	k6eAd1
nezajímal	zajímat	k5eNaImAgMnS
o	o	k7c4
lodě	loď	k1gFnPc4
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
navigaci	navigace	k1gFnSc4
–	–	k?
jeho	jeho	k3xOp3gNnSc4
zasahování	zasahování	k1gNnSc4
do	do	k7c2
vedení	vedení	k1gNnSc2
lodi	loď	k1gFnSc2
je	být	k5eAaImIp3nS
spíše	spíše	k9
absurdní	absurdní	k2eAgInSc1d1
výmysl	výmysl	k1gInSc4
živený	živený	k2eAgInSc4d1
filmy	film	k1gInPc4
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
je	být	k5eAaImIp3nS
nacistický	nacistický	k2eAgInSc1d1
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
nástup	nástup	k1gInSc4
do	do	k7c2
záchranného	záchranný	k2eAgInSc2d1
člunu	člun	k1gInSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
obětavě	obětavě	k6eAd1
pomáhal	pomáhat	k5eAaImAgMnS
posádce	posádka	k1gFnSc3
jej	on	k3xPp3gMnSc4
naplnit	naplnit	k5eAaPmF
a	a	k8xC
dále	daleko	k6eAd2
neviděl	vidět	k5eNaImAgMnS
nikoho	nikdo	k3yNnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
upřednostněn	upřednostnit	k5eAaPmNgInS
<g/>
,	,	kIx,
rovněž	rovněž	k9
nevykazuje	vykazovat	k5eNaImIp3nS
známky	známka	k1gFnPc4
nějakého	nějaký	k3yIgNnSc2
sobectví	sobectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ismay	Ismaum	k1gNnPc7
se	se	k3xPyFc4
v	v	k7c6
Americe	Amerika	k1gFnSc6
stal	stát	k5eAaPmAgMnS
obětním	obětní	k2eAgMnSc7d1
beránkem	beránek	k1gMnSc7
(	(	kIx(
<g/>
byl	být	k5eAaImAgInS
dokonce	dokonce	k9
zadržen	zadržen	k2eAgInSc1d1
<g/>
)	)	kIx)
–	–	k?
ovšem	ovšem	k9
neprávem	neprávo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobného	podobný	k2eAgInSc2d1
osudu	osud	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
Británii	Británie	k1gFnSc6
později	pozdě	k6eAd2
dočkal	dočkat	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
SS	SS	kA
Californian	Californian	k1gMnSc1
Stanley	Stanlea	k1gFnSc2
Lord	lord	k1gMnSc1
–	–	k?
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
dokonce	dokonce	k9
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
několik	několik	k4yIc4
knih	kniha	k1gFnPc2
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
obhajobu	obhajoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Případ	případ	k1gInSc1
SS	SS	kA
Californian	Californian	k1gInSc1
</s>
<s>
SS	SS	kA
Californian	Californian	k1gMnSc1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
nepomohl	pomoct	k5eNaPmAgInS
obětem	oběť	k1gFnPc3
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
varoval	varovat	k5eAaImAgMnS
jej	on	k3xPp3gNnSc4
před	před	k7c7
ledovci	ledovec	k1gInPc7
<g/>
,	,	kIx,
radista	radista	k1gMnSc1
na	na	k7c4
Titanicu	Titanica	k1gFnSc4
však	však	k9
jeho	jeho	k3xOp3gNnSc4
varování	varování	k1gNnSc4
pominul	pominout	k5eAaPmAgMnS
</s>
<s>
Případ	případ	k1gInSc1
lodi	loď	k1gFnSc2
SS	SS	kA
Californian	Californian	k1gInSc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
součástí	součást	k1gFnSc7
šetření	šetření	k1gNnSc2
britské	britský	k2eAgFnSc2d1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
podaných	podaný	k2eAgNnPc2d1
svědectví	svědectví	k1gNnPc2
kapitána	kapitán	k1gMnSc2
Stanley	Stanlea	k1gMnSc2
Lorda	lord	k1gMnSc2
a	a	k8xC
třetího	třetí	k4xOgMnSc2
důstojníka	důstojník	k1gMnSc2
C.	C.	kA
V.	V.	kA
Grovese	Grovese	k1gFnSc1
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
paluby	paluba	k1gFnSc2
lodi	loď	k1gFnSc2
byla	být	k5eAaImAgFnS
jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
pozorována	pozorovat	k5eAaImNgFnS
světla	světlo	k1gNnSc2
jiné	jiný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
osobního	osobní	k2eAgInSc2d1
parníku	parník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
C.	C.	kA
V.	V.	kA
Groves	Groves	k1gMnSc1
v	v	k7c6
23	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
ukončil	ukončit	k5eAaPmAgMnS
službu	služba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
23	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
se	se	k3xPyFc4
radista	radista	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Evans	Evans	k1gInSc4
na	na	k7c6
palubě	paluba	k1gFnSc6
parníku	parník	k1gInSc2
Californian	Californiana	k1gFnPc2
snažil	snažit	k5eAaImAgMnS
Titanic	Titanic	k1gInSc4
varovat	varovat	k5eAaImF
před	před	k7c7
ledovci	ledovec	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
varování	varování	k1gNnSc4
nereagoval	reagovat	k5eNaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
paluby	paluba	k1gFnSc2
Californianu	Californian	k1gMnSc3
pozorovali	pozorovat	k5eAaImAgMnP
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
světla	světlo	k1gNnPc1
poblikávala	poblikávat	k5eAaImAgNnP
a	a	k8xC
zdálo	zdát	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
otočila	otočit	k5eAaPmAgFnS
zádí	záď	k1gFnSc7
k	k	k7c3
pozorovatelům	pozorovatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
na	na	k7c4
rozkaz	rozkaz	k1gInSc4
kapitána	kapitán	k1gMnSc2
S.	S.	kA
Lorda	lord	k1gMnSc2
byly	být	k5eAaImAgFnP
z	z	k7c2
Californianu	Californian	k1gInSc2
odvysílány	odvysílán	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
pomocí	pomocí	k7c2
Morseovy	Morseův	k2eAgFnSc2d1
lampy	lampa	k1gFnSc2
v	v	k7c6
čase	čas	k1gInSc6
23	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
a	a	k8xC
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tyto	tento	k3xDgFnPc4
zprávy	zpráva	k1gFnPc4
však	však	k9
neznámá	známý	k2eNgFnSc1d1
loď	loď	k1gFnSc1
neodpověděla	odpovědět	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zápisu	zápis	k1gInSc6
je	být	k5eAaImIp3nS
taktéž	taktéž	k?
uvedeno	uvést	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Morseova	Morseův	k2eAgFnSc1d1
lampa	lampa	k1gFnSc1
na	na	k7c4
Californianu	Californiana	k1gFnSc4
byla	být	k5eAaImAgFnS
viditelná	viditelný	k2eAgFnSc1d1
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
4	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
asi	asi	k9
6	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
takže	takže	k8xS
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
vidět	vidět	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
23	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
šel	jít	k5eAaImAgMnS
kapitán	kapitán	k1gMnSc1
Lord	lord	k1gMnSc1
spát	spát	k5eAaImF
a	a	k8xC
na	na	k7c6
palubě	paluba	k1gFnSc6
zůstal	zůstat	k5eAaPmAgInS
2	#num#	k4
<g/>
.	.	kIx.
důstojník	důstojník	k1gMnSc1
Herbert	Herbert	k1gMnSc1
Stone	ston	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
kapitánovi	kapitán	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
zahlédl	zahlédnout	k5eAaPmAgMnS
světlici	světlice	k1gFnSc4
a	a	k8xC
po	po	k7c6
ní	on	k3xPp3gFnSc6
další	další	k2eAgFnSc6d1
čtyři	čtyři	k4xCgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
světlice	světlice	k1gFnPc1
však	však	k9
měly	mít	k5eAaImAgFnP
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
podle	podle	k7c2
barvy	barva	k1gFnSc2
světlic	světlice	k1gFnPc2
nebylo	být	k5eNaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
loď	loď	k1gFnSc4
lodní	lodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
rakety	raketa	k1gFnPc4
vystřelila	vystřelit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
Lord	lord	k1gMnSc1
pouze	pouze	k6eAd1
nařídil	nařídit	k5eAaPmAgMnS
další	další	k2eAgNnSc4d1
sledování	sledování	k1gNnSc4
neznámé	známý	k2eNgFnSc2d1
lodě	loď	k1gFnSc2
a	a	k8xC
odvysílání	odvysílání	k1gNnSc2
další	další	k2eAgFnSc2d1
Morseovy	Morseův	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
lampou	lampa	k1gFnSc7
a	a	k8xC
šel	jít	k5eAaImAgInS
opět	opět	k6eAd1
spát	spát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
byly	být	k5eAaImAgInP
z	z	k7c2
paluby	paluba	k1gFnSc2
Californianu	Californian	k1gInSc2
pozorovány	pozorován	k2eAgFnPc4d1
další	další	k2eAgFnPc4d1
světlice	světlice	k1gFnPc4
<g/>
,	,	kIx,
důstojník	důstojník	k1gMnSc1
Stone	ston	k1gInSc5
přitom	přitom	k6eAd1
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
neznámá	známý	k2eNgFnSc1d1
loď	loď	k1gFnSc1
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
podivně	podivně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
nakloněná	nakloněný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
se	se	k3xPyFc4
kapitán	kapitán	k1gMnSc1
Lord	lord	k1gMnSc1
opět	opět	k6eAd1
dotázal	dotázat	k5eAaPmAgMnS
na	na	k7c4
neznámou	známý	k2eNgFnSc4d1
loď	loď	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gFnPc4
světlice	světlice	k1gFnPc4
<g/>
,	,	kIx,
opět	opět	k6eAd1
dostal	dostat	k5eAaPmAgMnS
odpověď	odpověď	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
bílé	bílý	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Californian	Californian	k1gInSc1
se	se	k3xPyFc4
do	do	k7c2
záchranné	záchranný	k2eAgFnSc2d1
akce	akce	k1gFnSc2
zapojil	zapojit	k5eAaPmAgInS
teprve	teprve	k6eAd1
v	v	k7c6
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
kdy	kdy	k6eAd1
radista	radista	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Furmstone	Furmston	k1gInSc5
Evans	Evans	k1gInSc1
opět	opět	k6eAd1
zapnul	zapnout	k5eAaPmAgInS
radiostanici	radiostanice	k1gFnSc4
a	a	k8xC
Californian	Californian	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
rádiový	rádiový	k2eAgInSc4d1
příkaz	příkaz	k1gInSc4
od	od	k7c2
generálního	generální	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
George	Georg	k1gMnSc2
Stewarta	Stewart	k1gMnSc2
k	k	k7c3
pomoci	pomoc	k1gFnSc3
Titanicu	Titanicus	k1gInSc2
<g/>
;	;	kIx,
teprve	teprve	k6eAd1
tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
kapitán	kapitán	k1gMnSc1
Lord	lord	k1gMnSc1
dozvěděl	dozvědět	k5eAaPmAgMnS
o	o	k7c4
potopení	potopení	k1gNnSc4
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záchranná	záchranný	k2eAgFnSc1d1
vesta	vesta	k1gFnSc1
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2
vyšetřování	vyšetřování	k1gNnSc1
britské	britský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
ztotožnilo	ztotožnit	k5eAaPmAgNnS
tuto	tento	k3xDgFnSc4
neznámou	známý	k2eNgFnSc4d1
loď	loď	k1gFnSc4
s	s	k7c7
parníkem	parník	k1gInSc7
SS	SS	kA
Californian	Californian	k1gInSc1
a	a	k8xC
komise	komise	k1gFnSc1
obvinila	obvinit	k5eAaPmAgFnS
kapitána	kapitán	k1gMnSc4
Lorda	lord	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnPc4
důstojníky	důstojník	k1gMnPc4
z	z	k7c2
hrubého	hrubý	k2eAgNnSc2d1
pochybení	pochybení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
Lord	lord	k1gMnSc1
toto	tento	k3xDgNnSc4
odmítal	odmítat	k5eAaImAgMnS
a	a	k8xC
po	po	k7c4
celý	celý	k2eAgInSc4d1
zbytek	zbytek	k1gInSc4
života	život	k1gInSc2
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
očistit	očistit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
od	od	k7c2
rozsudku	rozsudek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
mu	on	k3xPp3gMnSc3
zničil	zničit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Odmítal	odmítat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
on	on	k3xPp3gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
důstojníci	důstojník	k1gMnPc1
viděli	vidět	k5eAaImAgMnP
z	z	k7c2
můstku	můstek	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
mezi	mezi	k7c7
Californianem	Californian	k1gMnSc7
a	a	k8xC
Titanicem	Titanic	k1gMnSc7
nacházela	nacházet	k5eAaImAgFnS
třetí	třetí	k4xOgFnSc4
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
viděly	vidět	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
odplula	odplout	k5eAaPmAgFnS
ještě	ještě	k6eAd1
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
potopil	potopit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
neexistují	existovat	k5eNaImIp3nP
přímé	přímý	k2eAgInPc4d1
důkazy	důkaz	k1gInPc4
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
obhajobu	obhajoba	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2
než	než	k8xS
závěr	závěr	k1gInSc1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chybou	Chyba	k1gMnSc7
však	však	k9
zůstává	zůstávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kapitán	kapitán	k1gMnSc1
Lord	lord	k1gMnSc1
nenechal	nechat	k5eNaPmAgMnS
vzbudit	vzbudit	k5eAaPmF
radistu	radista	k1gMnSc4
a	a	k8xC
nepřikázal	přikázat	k5eNaPmAgMnS
mu	on	k3xPp3gMnSc3
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
v	v	k7c6
okolí	okolí	k1gNnSc6
děje	děj	k1gInSc2
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
opomenutí	opomenutí	k1gNnSc1
stálo	stát	k5eAaImAgNnS
životy	život	k1gInPc4
mnoho	mnoho	k4c4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parník	parník	k1gInSc1
SS	SS	kA
Californian	Californian	k1gInSc1
byl	být	k5eAaImAgMnS
nejbližší	blízký	k2eAgFnSc7d3
lodí	loď	k1gFnSc7
k	k	k7c3
potápějícímu	potápějící	k2eAgMnSc3d1
se	se	k3xPyFc4
Titanicu	Titanicus	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
19,5	19,5	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
31,4	31,4	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
a	a	k8xC
předpisy	předpis	k1gInPc1
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
vyšetřování	vyšetřování	k1gNnSc2
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
zastaralých	zastaralý	k2eAgInPc2d1
a	a	k8xC
nevyhovujících	vyhovující	k2eNgInPc2d1
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
aktualizaci	aktualizace	k1gFnSc3
předpisů	předpis	k1gInPc2
podle	podle	k7c2
nových	nový	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
řadu	řada	k1gFnSc4
inovovaných	inovovaný	k2eAgInPc2d1
a	a	k8xC
nových	nový	k2eAgInPc2d1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
pro	pro	k7c4
záoceánské	záoceánský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
ukládaly	ukládat	k5eAaImAgFnP
vylepšení	vylepšení	k1gNnSc4
vodotěsných	vodotěsný	k2eAgFnPc2d1
přepážek	přepážka	k1gFnPc2
v	v	k7c6
trupu	trup	k1gInSc6
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
konstrukci	konstrukce	k1gFnSc4
<g/>
,	,	kIx,
technickou	technický	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
pro	pro	k7c4
nástup	nástup	k1gInSc4
a	a	k8xC
výstup	výstup	k1gInSc4
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
nové	nový	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
na	na	k7c4
záchranné	záchranný	k2eAgInPc4d1
čluny	člun	k1gInPc4
<g/>
,	,	kIx,
lepší	dobrý	k2eAgNnSc4d2
provedení	provedení	k1gNnSc4
a	a	k8xC
funkčnost	funkčnost	k1gFnSc4
záchranných	záchranný	k2eAgFnPc2d1
vest	vesta	k1gFnPc2
<g/>
,	,	kIx,
pořádání	pořádání	k1gNnSc1
cvičení	cvičení	k1gNnPc2
k	k	k7c3
opuštění	opuštění	k1gNnSc3
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
lepší	dobrý	k2eAgNnSc1d2
předávání	předávání	k1gNnSc1
zpráv	zpráva	k1gFnPc2
a	a	k8xC
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
radiokomunikační	radiokomunikační	k2eAgInPc4d1
zákony	zákon	k1gInPc4
a	a	k8xC
další	další	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřovatelé	vyšetřovatel	k1gMnPc1
také	také	k9
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
Titanicu	Titanicus	k1gInSc6
byly	být	k5eAaImAgInP
kapacitně	kapacitně	k6eAd1
odpovídající	odpovídající	k2eAgInPc1d1
záchranné	záchranný	k2eAgInPc1d1
čluny	člun	k1gInPc1
pouze	pouze	k6eAd1
pro	pro	k7c4
cestující	cestující	k1gFnSc4
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
pro	pro	k7c4
nižší	nízký	k2eAgFnPc4d2
třídy	třída	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vyšetřování	vyšetřování	k1gNnSc6
se	se	k3xPyFc4
také	také	k9
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
cestující	cestující	k1gMnSc1
třetí	třetí	k4xOgFnSc2
třídy	třída	k1gFnSc2
ani	ani	k8xC
nevěděli	vědět	k5eNaImAgMnP
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
palubě	paluba	k1gFnSc6
Titanicu	Titanicus	k1gInSc2
záchranné	záchranný	k2eAgInPc1d1
čluny	člun	k1gInPc1
nacházejí	nacházet	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
ani	ani	k8xC
nevěděli	vědět	k5eNaImAgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
na	na	k7c4
horní	horní	k2eAgFnSc4d1
<g/>
,	,	kIx,
člunovou	člunový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Imigrační	imigrační	k2eAgInPc1d1
zákony	zákon	k1gInPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
taktéž	taktéž	k?
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nařizovaly	nařizovat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
cestující	cestující	k1gMnSc1
třetí	třetí	k4xOgFnSc2
třídy	třída	k1gFnSc2
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
odděleni	oddělit	k5eAaPmNgMnP
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
cestujících	cestující	k1gMnPc2
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
pak	pak	k6eAd1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
ztíženému	ztížený	k2eAgInSc3d1
přístupu	přístup	k1gInSc3
z	z	k7c2
třetí	třetí	k4xOgFnSc2
třídy	třída	k1gFnSc2
na	na	k7c4
horní	horní	k2eAgFnPc4d1
paluby	paluba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Možné	možný	k2eAgFnPc1d1
příčiny	příčina	k1gFnPc1
katastrofy	katastrofa	k1gFnSc2
</s>
<s>
Je	být	k5eAaImIp3nS
evidentní	evidentní	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
hlavní	hlavní	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
ztroskotání	ztroskotání	k1gNnSc2
Titanicu	Titanicus	k1gInSc6
byla	být	k5eAaImAgFnS
kolize	kolize	k1gFnSc1
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
způsobil	způsobit	k5eAaPmAgInS
trhlinu	trhlina	k1gFnSc4
pod	pod	k7c7
čarou	čára	k1gFnSc7
ponoru	ponor	k1gInSc2
procházející	procházející	k2eAgFnSc1d1
přes	přes	k7c4
několik	několik	k4yIc4
vodotěsných	vodotěsný	k2eAgFnPc2d1
komor	komora	k1gFnPc2
v	v	k7c6
přední	přední	k2eAgFnSc6d1
části	část	k1gFnSc6
na	na	k7c6
pravoboku	pravobok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
je	být	k5eAaImIp3nS
ale	ale	k9
důsledkem	důsledek	k1gInSc7
předcházejících	předcházející	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
i	i	k8xC
konstrukce	konstrukce	k1gFnSc2
Titanicu	Titanicus	k1gInSc2
samotného	samotný	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
potopení	potopení	k1gNnSc3
Titanicu	Titanicus	k1gInSc2
mohla	moct	k5eAaImAgFnS
značnou	značný	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
přispět	přispět	k5eAaPmF
i	i	k9
nezvyklá	zvyklý	k2eNgFnSc1d1
konstelace	konstelace	k1gFnSc1
Měsíce	měsíc	k1gInSc2
a	a	k8xC
Slunce	slunce	k1gNnSc2
vůči	vůči	k7c3
Zemi	zem	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
silný	silný	k2eAgInSc4d1
příliv	příliv	k1gInSc4
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgInSc4d1
průnik	průnik	k1gInSc4
statisticky	statisticky	k6eAd1
výrazně	výrazně	k6eAd1
nadprůměrného	nadprůměrný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
velkých	velký	k2eAgInPc2d1
ledovců	ledovec	k1gInPc2
do	do	k7c2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
a	a	k8xC
metalurgie	metalurgie	k1gFnSc1
</s>
<s>
Ledovec	ledovec	k1gInSc1
prolomil	prolomit	k5eAaPmAgInS
a	a	k8xC
pokřivil	pokřivit	k5eAaPmAgInS
pláty	plát	k1gInPc4
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
trhlinami	trhlina	k1gFnPc7
pod	pod	k7c7
čarou	čára	k1gFnSc7
ponoru	ponor	k1gInSc2
může	moct	k5eAaImIp3nS
pronikat	pronikat	k5eAaImF
voda	voda	k1gFnSc1
do	do	k7c2
nitra	nitro	k1gNnSc2
trupu	trup	k1gInSc2
Titanicu	Titanicus	k1gInSc2
</s>
<s>
První	první	k4xOgFnPc1
úvahy	úvaha	k1gFnPc1
o	o	k7c6
trhlině	trhlina	k1gFnSc6
způsobené	způsobený	k2eAgFnSc6d1
ledovcem	ledovec	k1gInSc7
byly	být	k5eAaImAgFnP
takové	takový	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
ledovec	ledovec	k1gInSc1
doslova	doslova	k6eAd1
prořízl	proříznout	k5eAaPmAgInS
trup	trup	k1gInSc1
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
sonarových	sonarův	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
vraku	vrak	k1gInSc2
<g/>
,	,	kIx,
provedených	provedený	k2eAgInPc2d1
později	pozdě	k6eAd2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
kolize	kolize	k1gFnSc1
mezi	mezi	k7c7
ledovcem	ledovec	k1gInSc7
a	a	k8xC
trupem	trup	k1gInSc7
způsobila	způsobit	k5eAaPmAgFnS
„	„	k?
<g/>
pouze	pouze	k6eAd1
<g/>
“	“	k?
deformaci	deformace	k1gFnSc3
lodních	lodní	k2eAgInPc2d1
plátů	plát	k1gInPc2
trupu	trup	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
ocelové	ocelový	k2eAgInPc1d1
pláty	plát	k1gInPc1
měly	mít	k5eAaImAgInP
tloušťku	tloušťka	k1gFnSc4
1	#num#	k4
až	až	k9
1	#num#	k4
<g/>
½	½	k?
palce	palec	k1gInSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
tj.	tj.	kA
2,5	2,5	k4
až	až	k9
3,8	3,8	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Podrobnou	podrobný	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
malých	malý	k2eAgInPc2d1
kousků	kousek	k1gInPc2
oceli	ocel	k1gFnSc2
z	z	k7c2
trupu	trup	k1gInSc2
Titanicu	Titanicus	k1gInSc2
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
uhlíková	uhlíkový	k2eAgFnSc1d1
ocel	ocel	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ztrácí	ztrácet	k5eAaImIp3nS
houževnatost	houževnatost	k1gFnSc4
a	a	k8xC
zkřehne	zkřehnout	k5eAaPmIp3nS
ve	v	k7c6
studené	studený	k2eAgFnSc6d1
nebo	nebo	k8xC
ledové	ledový	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
ztrácí	ztrácet	k5eAaImIp3nS
pružnost	pružnost	k1gFnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
náchylná	náchylný	k2eAgFnSc1d1
k	k	k7c3
protržení	protržení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemickou	chemický	k2eAgFnSc7d1
analýzou	analýza	k1gFnSc7
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
ocel	ocel	k1gFnSc1
měla	mít	k5eAaImAgFnS
vysoký	vysoký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
fosforu	fosfor	k1gInSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
a	a	k8xC
síry	síra	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
moderní	moderní	k2eAgFnSc7d1
ocelí	ocel	k1gFnSc7
a	a	k8xC
poměr	poměr	k1gInSc1
manganu	mangan	k1gInSc2
a	a	k8xC
síry	síra	k1gFnSc2
byl	být	k5eAaImAgInS
6,8	6,8	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
u	u	k7c2
moderních	moderní	k2eAgFnPc2d1
ocelí	ocel	k1gFnPc2
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
poměr	poměr	k1gInSc1
větší	veliký	k2eAgFnSc4d2
než	než	k8xS
200	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrn	souhrn	k1gInSc1
těchto	tento	k3xDgInPc2
poměrů	poměr	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
fosforu	fosfor	k1gInSc2
<g/>
,	,	kIx,
napomáhá	napomáhat	k5eAaImIp3nS,k5eAaBmIp3nS
vzniku	vznik	k1gInSc3
lomu	lom	k1gInSc2
<g/>
,	,	kIx,
síra	síra	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nP
zrna	zrno	k1gNnPc1
sulfidu	sulfid	k1gInSc2
železnatého	železnatý	k2eAgInSc2d1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
usnadňují	usnadňovat	k5eAaImIp3nP
šíření	šíření	k1gNnSc4
trhlin	trhlina	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
nedostatek	nedostatek	k1gInSc1
manganu	mangan	k1gInSc2
snižuje	snižovat	k5eAaImIp3nS
tvárnost	tvárnost	k1gFnSc4
oceli	ocel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
toto	tento	k3xDgNnSc4
byla	být	k5eAaImAgFnS
použitá	použitý	k2eAgFnSc1d1
ocel	ocel	k1gFnSc1
nejkvalitnější	kvalitní	k2eAgFnSc1d3
<g/>
,	,	kIx,
jaká	jaký	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
stavby	stavba	k1gFnSc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k9
při	při	k7c6
použití	použití	k1gNnSc6
v	v	k7c6
nízkých	nízký	k2eAgFnPc6d1
teplotách	teplota	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Chování	chování	k1gNnSc1
oceli	ocel	k1gFnSc2
při	při	k7c6
namáhání	namáhání	k1gNnSc6
taktéž	taktéž	k?
ovlivňuje	ovlivňovat	k5eAaImIp3nS
směr	směr	k1gInSc1
<g/>
,	,	kIx,
jakým	jaký	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
je	být	k5eAaImIp3nS
válcována	válcován	k2eAgFnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
směrem	směr	k1gInSc7
je	být	k5eAaImIp3nS
vytvořena	vytvořen	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
<g/>
,	,	kIx,
směr	směr	k1gInSc1
zrn	zrno	k1gNnPc2
<g/>
,	,	kIx,
oceli	ocel	k1gFnPc4
při	při	k7c6
výrobě	výroba	k1gFnSc6
a	a	k8xC
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
směrem	směr	k1gInSc7
působí	působit	k5eAaImIp3nP
síly	síla	k1gFnPc4
při	při	k7c6
zatížení	zatížení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocelové	ocelový	k2eAgInPc1d1
pláty	plát	k1gInPc1
pro	pro	k7c4
Titanic	Titanic	k1gInSc4
byly	být	k5eAaImAgInP
dodány	dodat	k5eAaPmNgInP
firmou	firma	k1gFnSc7
David	David	k1gMnSc1
Colville	Colville	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
z	z	k7c2
oceláren	ocelárna	k1gFnPc2
Dalzell	Dalzell	k1gMnSc1
Steel	Steel	k1gMnSc1
and	and	k?
Iron	iron	k1gInSc1
Works	Works	kA
v	v	k7c6
Motherwellu	Motherwell	k1gInSc6
nedaleko	nedaleko	k7c2
Glasgow	Glasgow	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vysvětlilo	vysvětlit	k5eAaPmAgNnS
vysoký	vysoký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
fosforu	fosfor	k1gInSc2
a	a	k8xC
síry	síra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
šesti	šest	k4xCc2
malých	malý	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
trupu	trup	k1gInSc2
při	při	k7c6
pokojové	pokojový	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
a	a	k8xC
při	při	k7c6
teplotě	teplota	k1gFnSc6
0	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
32	#num#	k4
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
ale	ale	k8xC
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
použitá	použitý	k2eAgFnSc1d1
ocel	ocel	k1gFnSc1
nebyla	být	k5eNaImAgFnS
křehká	křehký	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
při	při	k7c6
teplotě	teplota	k1gFnSc6
kolem	kolem	k7c2
0	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Dřívější	dřívější	k2eAgInPc1d1
testy	test	k1gInPc1
byly	být	k5eAaImAgInP
totiž	totiž	k9
prováděny	prováděn	k2eAgInPc1d1
se	s	k7c7
simulovaným	simulovaný	k2eAgInSc7d1
nárazem	náraz	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
simulované	simulovaný	k2eAgNnSc4d1
„	„	k?
<g/>
pomalé	pomalý	k2eAgNnSc1d1
prohýbání	prohýbání	k1gNnSc1
<g/>
“	“	k?
více	hodně	k6eAd2
odpovídá	odpovídat	k5eAaImIp3nS
skutečnému	skutečný	k2eAgInSc3d1
průběhu	průběh	k1gInSc3
deformace	deformace	k1gFnSc2
při	při	k7c6
nárazu	náraz	k1gInSc6
na	na	k7c4
ledovec	ledovec	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalším	další	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
ovlivňujícím	ovlivňující	k2eAgInSc7d1
rozsah	rozsah	k1gInSc4
trhliny	trhlina	k1gFnPc1
byly	být	k5eAaImAgFnP
nýty	nýt	k1gInPc4
<g/>
,	,	kIx,
držící	držící	k2eAgInSc4d1
trup	trup	k1gInSc4
pohromadě	pohromadě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
nýty	nýt	k1gInPc1
totiž	totiž	k9
byly	být	k5eAaImAgInP
křehčí	křehký	k2eAgInPc4d2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Použité	použitý	k2eAgInPc1d1
nýty	nýt	k1gInPc1
byly	být	k5eAaImAgInP
osazovány	osazovat	k5eAaImNgInP
na	na	k7c4
trup	trup	k1gInSc4
pomocí	pomocí	k7c2
nýtovacího	nýtovací	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgInSc4
nebylo	být	k5eNaImAgNnS
možno	možno	k6eAd1
použít	použít	k5eAaPmF
v	v	k7c6
malých	malý	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
příď	příď	k1gFnSc4
a	a	k8xC
záď	záď	k1gFnSc4
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
používalo	používat	k5eAaImAgNnS
ruční	ruční	k2eAgNnSc1d1
nýtování	nýtování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
několika	několik	k4yIc2
milionů	milion	k4xCgInPc2
nýtů	nýt	k1gInPc2
držících	držící	k2eAgInPc2d1
Titanic	Titanic	k1gInSc4
pohromadě	pohromadě	k6eAd1
bylo	být	k5eAaImAgNnS
vyloveno	vylovit	k5eAaPmNgNnS
čtyřicet	čtyřicet	k4xCc1
osm	osm	k4xCc1
<g/>
;	;	kIx,
šest	šest	k4xCc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
identifikováno	identifikovat	k5eAaBmNgNnS
jako	jako	k9
nýty	nýt	k1gInPc4
z	z	k7c2
trupu	trup	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
Američané	Američan	k1gMnPc1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
experimentů	experiment	k1gInPc2
s	s	k7c7
nýty	nýt	k1gInPc7
vyrobenými	vyrobený	k2eAgInPc7d1
co	co	k9
nejpřesněji	přesně	k6eAd3
podle	podle	k7c2
nýtů	nýt	k1gInPc2
na	na	k7c4
Titanicu	Titanica	k1gFnSc4
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nýty	nýt	k1gInPc1
obsahovaly	obsahovat	k5eAaImAgInP
velkou	velký	k2eAgFnSc4d1
koncentraci	koncentrace	k1gFnSc4
strusky	struska	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
způsobila	způsobit	k5eAaPmAgFnS
jejich	jejich	k3xOp3gFnSc4
větší	veliký	k2eAgFnSc4d2
křehkost	křehkost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
při	při	k7c6
nárazu	náraz	k1gInSc6
Titanicu	Titanicus	k1gInSc2
na	na	k7c4
ledovec	ledovec	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
podle	podle	k7c2
výpočtů	výpočet	k1gInPc2
na	na	k7c4
každý	každý	k3xTgInSc4
nýt	nýt	k1gInSc4
vyvinut	vyvinut	k2eAgInSc1d1
tlak	tlak	k1gInSc1
asi	asi	k9
6	#num#	k4
350	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
nýty	nýt	k1gInPc1
praskaly	praskat	k5eAaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zřejmě	zřejmě	k6eAd1
již	již	k6eAd1
při	při	k7c6
tlaku	tlak	k1gInSc6
4	#num#	k4
530	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
i	i	k9
když	když	k8xS
měly	mít	k5eAaImAgFnP
vydržet	vydržet	k5eAaPmF
až	až	k9
dvojnásobek	dvojnásobek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
archivech	archiv	k1gInPc6
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
kování	kování	k1gNnSc4
nýtů	nýt	k1gInPc2
bylo	být	k5eAaImAgNnS
nakoupeno	nakoupen	k2eAgNnSc1d1
železo	železo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
P.	P.	kA
9	#num#	k4
<g/>
]	]	kIx)
č.	č.	k?
3	#num#	k4
„	„	k?
<g/>
best	best	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
nejlepší	dobrý	k2eAgNnSc1d3
<g/>
)	)	kIx)
s	s	k7c7
pevností	pevnost	k1gFnSc7
v	v	k7c6
tahu	tah	k1gInSc6
cca	cca	kA
73	#num#	k4
%	%	kIx~
pevnosti	pevnost	k1gFnSc3
oceli	ocel	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
správně	správně	k6eAd1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
<g />
.	.	kIx.
</s>
<s hack="1">
nakoupeno	nakoupen	k2eAgNnSc4d1
železo	železo	k1gNnSc4
č.	č.	k?
4	#num#	k4
„	„	k?
<g/>
best-best	best-best	k1gInSc1
<g/>
“	“	k?
s	s	k7c7
pevností	pevnost	k1gFnSc7
v	v	k7c6
tahu	tah	k1gInSc6
80	#num#	k4
%	%	kIx~
pevnosti	pevnost	k1gFnSc3
oceli	ocel	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přes	přes	k7c4
nevhodné	vhodný	k2eNgInPc4d1
nýty	nýt	k1gInPc4
na	na	k7c6
přídi	příď	k1gFnSc6
největší	veliký	k2eAgFnSc2d3
škody	škoda	k1gFnSc2
utrpěly	utrpět	k5eAaPmAgFnP
prostory	prostora	k1gFnPc1
s	s	k7c7
kotelnami	kotelna	k1gFnPc7
5	#num#	k4
a	a	k8xC
6	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgInP
použity	použít	k5eAaPmNgInP
ocelové	ocelový	k2eAgInPc1d1
nýty	nýt	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kormidlo	kormidlo	k1gNnSc1
a	a	k8xC
možnost	možnost	k1gFnSc1
manévrování	manévrování	k1gNnSc2
</s>
<s>
Nevyvážené	vyvážený	k2eNgNnSc1d1
kormidlo	kormidlo	k1gNnSc1
lodi	loď	k1gFnSc2
Olympic	Olympice	k1gFnPc2
a	a	k8xC
levý	levý	k2eAgInSc4d1
a	a	k8xC
střední	střední	k2eAgInSc4d1
lodní	lodní	k2eAgInSc4d1
šroub	šroub	k1gInSc4
(	(	kIx(
<g/>
podobná	podobný	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
jako	jako	k8xC,k8xS
u	u	k7c2
Titanicu	Titanicus	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Kapitánský	kapitánský	k2eAgInSc1d1
můstek	můstek	k1gInSc1
na	na	k7c4
Olympicu	Olympica	k1gFnSc4
<g/>
,	,	kIx,
sesterské	sesterský	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
vlevo	vlevo	k6eAd1
a	a	k8xC
vpravo	vpravo	k6eAd1
od	od	k7c2
kormidla	kormidlo	k1gNnSc2
jsou	být	k5eAaImIp3nP
stojany	stojan	k1gInPc4
s	s	k7c7
lodními	lodní	k2eAgInPc7d1
telegrafy	telegraf	k1gInPc7
</s>
<s>
Kormidlo	kormidlo	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
mělo	mít	k5eAaImAgNnS
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
rozměrovou	rozměrový	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
zdánlivě	zdánlivě	k6eAd1
dostačující	dostačující	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
směru	směr	k1gInSc2
plavby	plavba	k1gFnSc2
a	a	k8xC
manévrování	manévrování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
takto	takto	k6eAd1
velkou	velký	k2eAgFnSc4d1
loď	loď	k1gFnSc4
bylo	být	k5eAaImAgNnS
kormidlo	kormidlo	k1gNnSc1
poddimenzované	poddimenzovaný	k2eAgNnSc1d1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
návrhu	návrh	k1gInSc6
nebyly	být	k5eNaImAgInP
provedeny	provést	k5eAaPmNgInP
žádné	žádný	k3yNgInPc1
výpočty	výpočet	k1gInPc1
nebo	nebo	k8xC
pokusy	pokus	k1gInPc1
s	s	k7c7
modelem	model	k1gInSc7
v	v	k7c6
malém	malý	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nP
ověřily	ověřit	k5eAaPmAgInP
ovladatelnost	ovladatelnost	k1gFnSc4
lodě	loď	k1gFnSc2
dlouhé	dlouhý	k2eAgFnSc2d1
269,10	269,10	k4
metrů	metr	k1gInPc2
v	v	k7c6
případě	případ	k1gInSc6
nouzového	nouzový	k2eAgNnSc2d1
manévrování	manévrování	k1gNnSc2
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
Achillovu	Achillův	k2eAgFnSc4d1
patu	pata	k1gFnSc4
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reálně	reálně	k6eAd1
<g/>
,	,	kIx,
při	při	k7c6
návrhu	návrh	k1gInSc6
kormidla	kormidlo	k1gNnSc2
byl	být	k5eAaImAgInS
neuváženě	uváženě	k6eNd1
použit	použít	k5eAaPmNgInS
model	model	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
použití	použití	k1gNnSc1
vysokého	vysoký	k2eAgNnSc2d1
kormidla	kormidlo	k1gNnSc2
je	být	k5eAaImIp3nS
účelné	účelný	k2eAgNnSc1d1
pro	pro	k7c4
lodě	loď	k1gFnPc4
s	s	k7c7
běžnou	běžný	k2eAgFnSc7d1
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
)	)	kIx)
rychlostí	rychlost	k1gFnSc7
plavby	plavba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
kratší	krátký	k2eAgNnSc1d2
kormidlo	kormidlo	k1gNnSc1
je	být	k5eAaImIp3nS
výhodnější	výhodný	k2eAgNnSc1d2
při	při	k7c6
manévrování	manévrování	k1gNnSc6
při	při	k7c6
malých	malý	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
porovnání	porovnání	k1gNnSc6
velikosti	velikost	k1gFnSc2
kormidla	kormidlo	k1gNnSc2
podle	podle	k7c2
dnešních	dnešní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
požadováno	požadován	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
kormidlo	kormidlo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
velikost	velikost	k1gFnSc4
1,5	1,5	k4
%	%	kIx~
až	až	k9
5	#num#	k4
%	%	kIx~
z	z	k7c2
profilu	profil	k1gInSc2
lodi	loď	k1gFnSc2
pod	pod	k7c7
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
poměr	poměr	k1gInSc1
u	u	k7c2
Titanicu	Titanicus	k1gInSc2
1,9	1,9	k4
%	%	kIx~
<g/>
,	,	kIx,
tedy	tedy	k9
blíže	blízce	k6eAd2
k	k	k7c3
dolní	dolní	k2eAgFnSc3d1
mezi	mezi	k7c7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
uspořádání	uspořádání	k1gNnSc1
je	být	k5eAaImIp3nS
však	však	k9
výhodnější	výhodný	k2eAgMnSc1d2
pro	pro	k7c4
lepší	dobrý	k2eAgNnSc4d2
laminární	laminární	k2eAgNnSc4d1
proudění	proudění	k1gNnSc4
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgMnS
Olympic	Olympic	k1gMnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
většina	většina	k1gFnSc1
významných	významný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
vybaven	vybaven	k2eAgInSc1d1
stejným	stejný	k2eAgNnSc7d1
polooválným	polooválný	k2eAgNnSc7d1
kormidlem	kormidlo	k1gNnSc7
jako	jako	k8xS,k8xC
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohly	moct	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
lodě	loď	k1gFnPc1
dosáhnout	dosáhnout	k5eAaPmF
vyšší	vysoký	k2eAgFnPc4d2
rychlosti	rychlost	k1gFnPc4
a	a	k8xC
uniknout	uniknout	k5eAaPmF
ponorkám	ponorka	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
tragédii	tragédie	k1gFnSc3
částečně	částečně	k6eAd1
přispěla	přispět	k5eAaPmAgFnS
i	i	k9
konstrukce	konstrukce	k1gFnSc1
pohonu	pohon	k1gInSc2
lodních	lodní	k2eAgInPc2d1
šroubů	šroub	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
dva	dva	k4xCgInPc4
postranní	postranní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
s	s	k7c7
možností	možnost	k1gFnSc7
reverzace	reverzace	k1gFnSc2
chodu	chod	k1gInSc2
<g/>
,	,	kIx,
poháněné	poháněný	k2eAgInPc1d1
parními	parní	k2eAgInPc7d1
stroji	stroj	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
prostřední	prostřední	k2eAgInSc1d1
šroub	šroub	k1gInSc1
byl	být	k5eAaImAgInS
poháněný	poháněný	k2eAgInSc1d1
parní	parní	k2eAgFnSc7d1
turbínou	turbína	k1gFnSc7
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
reverzace	reverzace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
4	#num#	k4
<g/>
.	.	kIx.
důstojníka	důstojník	k1gMnSc4
Josepha	Joseph	k1gMnSc4
Boxhalla	Boxhall	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
přišel	přijít	k5eAaPmAgMnS
na	na	k7c4
můstek	můstek	k1gInSc4
po	po	k7c6
srážce	srážka	k1gFnSc6
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
důstojník	důstojník	k1gMnSc1
Murdoch	Murdoch	k1gMnSc1
sice	sice	k8xC
vydal	vydat	k5eAaPmAgInS
strojovně	strojovna	k1gFnSc3
povel	povel	k1gInSc1
„	„	k?
<g/>
zpětný	zpětný	k2eAgInSc1d1
chod	chod	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odvrátil	odvrátit	k5eAaPmAgMnS
srážku	srážka	k1gFnSc4
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
protože	protože	k8xS
střední	střední	k2eAgInSc4d1
šroub	šroub	k1gInSc4
byl	být	k5eAaImAgInS
poháněný	poháněný	k2eAgInSc1d1
turbínou	turbína	k1gFnSc7
<g/>
,	,	kIx,
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
tato	tento	k3xDgFnSc1
reverzována	reverzován	k2eAgFnSc1d1
na	na	k7c4
zpětný	zpětný	k2eAgInSc4d1
chod	chod	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
zastavena	zastavit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obrázku	obrázek	k1gInSc2
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
že	že	k8xS
střední	střední	k2eAgInSc1d1
šroub	šroub	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
ose	osa	k1gFnSc6
kormidla	kormidlo	k1gNnSc2
a	a	k8xC
při	při	k7c6
zastavení	zastavení	k1gNnSc6
chodu	chod	k1gInSc2
šroubu	šroub	k1gInSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
podstatnému	podstatný	k2eAgNnSc3d1
snížení	snížení	k1gNnSc3
obtékání	obtékání	k1gNnSc1
kormidla	kormidlo	k1gNnSc2
proudící	proudící	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
pro	pro	k7c4
vyvození	vyvození	k1gNnSc4
síly	síla	k1gFnSc2
otáčející	otáčející	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
Murdoch	Murdoch	k1gInSc1
vydal	vydat	k5eAaPmAgInS
pouze	pouze	k6eAd1
povel	povel	k1gInSc1
„	„	k?
<g/>
docela	docela	k6eAd1
vpravo	vpravo	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k9
bez	bez	k7c2
rozkazu	rozkaz	k1gInSc2
„	„	k?
<g/>
zpětný	zpětný	k2eAgInSc1d1
chod	chod	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgInS
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
ještě	ještě	k6eAd1
ledovci	ledovec	k1gInSc3
vyhnout	vyhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
Zachráněný	zachráněný	k2eAgMnSc1d1
strojník	strojník	k1gMnSc1
Frederick	Frederick	k1gMnSc1
Scott	Scott	k1gMnSc1
ale	ale	k8xC
při	při	k7c6
výpovědi	výpověď	k1gFnSc6
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgFnPc1
čtyři	čtyři	k4xCgFnPc1
sady	sada	k1gFnPc1
telegrafů	telegraf	k1gInPc2
se	se	k3xPyFc4
změnily	změnit	k5eAaPmAgFnP
na	na	k7c6
„	„	k?
<g/>
STOP	stop	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
až	až	k9
po	po	k7c6
kolizi	kolize	k1gFnSc6
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
P.	P.	kA
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyhýbací	Vyhýbací	k2eAgInSc1d1
manévr	manévr	k1gInSc1
</s>
<s>
Vlastní	vlastní	k2eAgInSc4d1
vyhýbací	vyhýbací	k2eAgInSc4d1
manévr	manévr	k1gInSc4
řídil	řídit	k5eAaImAgMnS
první	první	k4xOgMnSc1
důstojník	důstojník	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
William	William	k1gInSc1
M.	M.	kA
Murdoch	Murdoch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zjištění	zjištění	k1gNnSc4
průběhu	průběh	k1gInSc2
vyhýbacího	vyhýbací	k2eAgInSc2d1
manévru	manévr	k1gInSc2
byly	být	k5eAaImAgInP
později	pozdě	k6eAd2
prováděny	provádět	k5eAaImNgInP
výpočty	výpočet	k1gInPc1
a	a	k8xC
zkoušky	zkouška	k1gFnPc1
se	s	k7c7
sesterskou	sesterský	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
Olympic	Olympice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
vykonání	vykonání	k1gNnSc4
rozkazu	rozkaz	k1gInSc2
„	„	k?
<g/>
zpětný	zpětný	k2eAgInSc1d1
chod	chod	k1gInSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
20	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
povel	povel	k1gInSc1
nebyl	být	k5eNaImAgInS
ani	ani	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
plavby	plavba	k1gFnSc2
předtím	předtím	k6eAd1
vydán	vydat	k5eAaPmNgInS
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
brát	brát	k5eAaImF
v	v	k7c4
úvahu	úvaha	k1gFnSc4
i	i	k9
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
strojníci	strojník	k1gMnPc1
vykonávali	vykonávat	k5eAaImAgMnP
i	i	k9
další	další	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
<g/>
,	,	kIx,
potřebné	potřebný	k2eAgInPc4d1
k	k	k7c3
chodu	chod	k1gInSc3
lodních	lodní	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
pokud	pokud	k8xS
by	by	kYmCp3nS
k	k	k7c3
předání	předání	k1gNnSc3
rozkazu	rozkaz	k1gInSc2
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
pouze	pouze	k6eAd1
10	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
technicky	technicky	k6eAd1
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
včasnému	včasný	k2eAgNnSc3d1
zastavení	zastavení	k1gNnSc3
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
provedení	provedení	k1gNnSc3
reverzace	reverzace	k1gFnSc2
chodu	chod	k1gInSc2
a	a	k8xC
vyvinutí	vyvinutí	k1gNnSc2
zpětného	zpětný	k2eAgInSc2d1
tahu	tah	k1gInSc2
lodních	lodní	k2eAgInPc2d1
šroubů	šroub	k1gInPc2
před	před	k7c7
srážkou	srážka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
ke	k	k7c3
zpětnému	zpětný	k2eAgInSc3d1
chodu	chod	k1gInSc3
před	před	k7c7
srážkou	srážka	k1gFnSc7
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
hovoří	hovořit	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
fakta	faktum	k1gNnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
zpětném	zpětný	k2eAgInSc6d1
chodu	chod	k1gInSc6
strojů	stroj	k1gInPc2
by	by	kYmCp3nP
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
silným	silný	k2eAgFnPc3d1
vibracím	vibrace	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
cítit	cítit	k5eAaImF
na	na	k7c6
celé	celý	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
pouze	pouze	k6eAd1
vibrace	vibrace	k1gFnPc1
na	na	k7c6
přídi	příď	k1gFnSc6
<g/>
,	,	kIx,
způsobené	způsobený	k2eAgFnPc4d1
nárazem	náraz	k1gInSc7
na	na	k7c4
ledovec	ledovec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
svědectví	svědectví	k1gNnSc2
lodního	lodní	k2eAgMnSc2d1
strojníka	strojník	k1gMnSc2
Fredericka	Fredericka	k1gFnSc1
Scotta	Scotta	k1gFnSc1
přišel	přijít	k5eAaPmAgInS
povel	povel	k1gInSc1
telegrafem	telegraf	k1gInSc7
k	k	k7c3
zastavení	zastavení	k1gNnSc3
až	až	k6eAd1
po	po	k7c6
nárazu	náraz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1
topič	topič	k1gMnSc1
Frederick	Frederick	k1gMnSc1
Barrett	Barrett	k1gMnSc1
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
v	v	k7c6
kotelně	kotelna	k1gFnSc6
telegrafy	telegraf	k1gInPc7
indikovaly	indikovat	k5eAaBmAgFnP
stejný	stejný	k2eAgInSc4d1
povel	povel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
úvahách	úvaha	k1gFnPc6
o	o	k7c6
možném	možný	k2eAgNnSc6d1
odvrácení	odvrácení	k1gNnSc6
kolize	kolize	k1gFnSc2
se	se	k3xPyFc4
hovořilo	hovořit	k5eAaImAgNnS
i	i	k9
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
mohl	moct	k5eAaImAgMnS
Murdoch	Murdoch	k1gMnSc1
srážce	srážka	k1gFnSc3
předejít	předejít	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
vydal	vydat	k5eAaPmAgInS
povel	povel	k1gInSc1
ke	k	k7c3
zpětnému	zpětný	k2eAgInSc3d1
chodu	chod	k1gInSc3
pouze	pouze	k6eAd1
pro	pro	k7c4
levý	levý	k2eAgInSc4d1
lodní	lodní	k2eAgInSc4d1
šroub	šroub	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tento	tento	k3xDgInSc1
povel	povel	k1gInSc1
by	by	kYmCp3nS
však	však	k9
nevedl	vést	k5eNaImAgMnS
k	k	k7c3
odvrácení	odvrácení	k1gNnSc3
katastrofy	katastrofa	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
pro	pro	k7c4
reverzaci	reverzace	k1gFnSc4
chodu	chod	k1gInSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
delší	dlouhý	k2eAgInSc4d2
čas	čas	k1gInSc4
<g/>
,	,	kIx,
než	než	k8xS
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zbýval	zbývat	k5eAaImAgInS
do	do	k7c2
nárazu	náraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1
vyhýbací	vyhýbací	k2eAgInSc1d1
manévr	manévr	k1gInSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
vyšetřovací	vyšetřovací	k2eAgFnSc1d1
komise	komise	k1gFnSc1
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Titanic	Titanic	k1gInSc1
byl	být	k5eAaImAgInS
vůči	vůči	k7c3
ledovci	ledovec	k1gInSc3
natočen	natočen	k2eAgMnSc1d1
vlevo	vlevo	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
úhlu	úhel	k1gInSc6
22,5	22,5	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
pokusů	pokus	k1gInPc2
<g/>
,	,	kIx,
prováděných	prováděný	k2eAgFnPc2d1
se	se	k3xPyFc4
sesterskou	sesterský	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
Olympic	Olympice	k1gInPc2
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
plné	plný	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
<g/>
,	,	kIx,
plnému	plný	k2eAgNnSc3d1
vychýlení	vychýlení	k1gNnSc3
kormidla	kormidlo	k1gNnSc2
a	a	k8xC
době	doba	k1gFnSc6
k	k	k7c3
nárazu	náraz	k1gInSc3
37	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
odpovídala	odpovídat	k5eAaImAgFnS
vzdálenost	vzdálenost	k1gFnSc1
od	od	k7c2
ledovce	ledovec	k1gInSc2
v	v	k7c6
okamžiku	okamžik	k1gInSc6
zpozorování	zpozorování	k1gNnSc2
přibližně	přibližně	k6eAd1
410	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
Kdyby	kdyby	k9
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
při	při	k7c6
vyhýbacím	vyhýbací	k2eAgInSc6d1
manévru	manévr	k1gInSc6
natočil	natočit	k5eAaBmAgMnS
více	hodně	k6eAd2
doleva	doleva	k6eAd1
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
by	by	kYmCp3nS
ke	k	k7c3
střetu	střet	k1gInSc3
s	s	k7c7
ledovcem	ledovec	k1gInSc7
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
trupu	trup	k1gInSc2
na	na	k7c6
pravoboku	pravobok	k1gInSc6
v	v	k7c6
důsledku	důsledek	k1gInSc6
jiné	jiný	k2eAgFnSc2d1
trajektorie	trajektorie	k1gFnSc2
zádi	záď	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
delší	dlouhý	k2eAgFnSc1d2
a	a	k8xC
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
trajektorie	trajektorie	k1gFnSc2
přídě	příď	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
rozkazu	rozkaz	k1gInSc6
„	„	k?
<g/>
docela	docela	k6eAd1
vpravo	vpravo	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
vydaném	vydaný	k2eAgNnSc6d1
Murdochem	Murdoch	k1gInSc7
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
přijít	přijít	k5eAaPmF
ve	v	k7c4
vhodný	vhodný	k2eAgInSc4d1
okamžik	okamžik	k1gInSc4
rozkaz	rozkaz	k1gInSc1
„	„	k?
<g/>
docela	docela	k6eAd1
vlevo	vlevo	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
zádí	záď	k1gFnPc2
pryč	pryč	k6eAd1
od	od	k7c2
ledovce	ledovec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murdoch	Murdoch	k1gInSc4
sice	sice	k8xC
použil	použít	k5eAaPmAgMnS
učebnicový	učebnicový	k2eAgInSc4d1
model	model	k1gInSc4
vyhýbacího	vyhýbací	k2eAgInSc2d1
manévru	manévr	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
problém	problém	k1gInSc1
u	u	k7c2
Titanicu	Titanicus	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
dané	daný	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
měl	mít	k5eAaImAgInS
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
poloměr	poloměr	k1gInSc1
otáčení	otáčení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
zde	zde	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
řada	řada	k1gFnSc1
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
tento	tento	k3xDgInSc4
manévr	manévr	k1gInSc4
kritizovaly	kritizovat	k5eAaImAgFnP
<g/>
:	:	kIx,
podle	podle	k7c2
nich	on	k3xPp3gFnPc2
měl	mít	k5eAaImAgMnS
Murdoch	Murdoch	k1gMnSc1
zastavit	zastavit	k5eAaPmF
stroje	stroj	k1gInPc4
a	a	k8xC
přepnout	přepnout	k5eAaPmF
je	on	k3xPp3gNnSc4
na	na	k7c4
zpětný	zpětný	k2eAgInSc4d1
chod	chod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc4
by	by	kYmCp3nS
pak	pak	k6eAd1
do	do	k7c2
ledovce	ledovec	k1gInSc2
narazil	narazit	k5eAaPmAgMnS
přídí	příď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přímém	přímý	k2eAgInSc6d1
nárazu	náraz	k1gInSc6
přídí	příď	k1gFnPc2
na	na	k7c4
ledovec	ledovec	k1gInSc4
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
poškozeno	poškodit	k5eAaPmNgNnS
přibližně	přibližně	k6eAd1
30	#num#	k4
stop	stopa	k1gFnPc2
trupu	trup	k1gInSc2
od	od	k7c2
hrany	hrana	k1gFnSc2
přídě	příď	k1gFnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
by	by	k9
proraženy	prorazit	k5eAaPmNgFnP
nanejvýše	nanejvýše	k6eAd1
3	#num#	k4
vodotěsné	vodotěsný	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
by	by	kYmCp3nP
pravděpodobně	pravděpodobně	k6eAd1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
život	život	k1gInSc4
i	i	k9
členové	člen	k1gMnPc1
posádky	posádka	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
své	svůj	k3xOyFgFnPc4
kajuty	kajuta	k1gFnPc4
na	na	k7c6
přídi	příď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kritika	kritika	k1gFnSc1
ovšem	ovšem	k9
nebere	brát	k5eNaImIp3nS
v	v	k7c4
úvahu	úvaha	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
Murdoch	Murdoch	k1gMnSc1
neměl	mít	k5eNaImAgMnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
přesnou	přesný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
mezi	mezi	k7c7
Titanicem	Titanic	k1gMnSc7
a	a	k8xC
ledovcem	ledovec	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
mohl	moct	k5eAaImAgMnS
odvodit	odvodit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
srážce	srážka	k1gFnSc3
nevyhne	vyhnout	k5eNaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
ani	ani	k8xC
nezohledňuje	zohledňovat	k5eNaImIp3nS
morální	morální	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
za	za	k7c4
usmrcení	usmrcení	k1gNnSc4
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
při	při	k7c6
přímém	přímý	k2eAgInSc6d1
střetu	střet	k1gInSc6
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vodotěsné	vodotěsný	k2eAgFnPc1d1
přepážky	přepážka	k1gFnPc1
</s>
<s>
Kritika	kritika	k1gFnSc1
zpochybnila	zpochybnit	k5eAaPmAgFnS
i	i	k9
uzavření	uzavření	k1gNnSc4
vodotěsných	vodotěsný	k2eAgFnPc2d1
přepážek	přepážka	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
rychle	rychle	k6eAd1
změnil	změnit	k5eAaPmAgInS
náklon	náklon	k1gInSc1
vlivem	vlivem	k7c2
extrémního	extrémní	k2eAgNnSc2d1
zatížení	zatížení	k1gNnSc2
přídě	příď	k1gFnSc2
<g/>
;	;	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
Titanic	Titanic	k1gInSc1
se	se	k3xPyFc4
údajně	údajně	k6eAd1
mohl	moct	k5eAaImAgInS
potápět	potápět	k5eAaImF
pomaleji	pomale	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
Murdoch	Murdoch	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
znát	znát	k5eAaImF
rozsah	rozsah	k1gInSc4
poškození	poškození	k1gNnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
rychlost	rychlost	k1gFnSc4
zaplavování	zaplavování	k1gNnSc2
podpalubí	podpalubí	k1gNnSc2
a	a	k8xC
podle	podle	k7c2
standardního	standardní	k2eAgInSc2d1
postupu	postup	k1gInSc2
při	při	k7c6
kolizi	kolize	k1gFnSc6
uzavřel	uzavřít	k5eAaPmAgMnS
přepážky	přepážka	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
pozdější	pozdní	k2eAgNnSc1d2
uzavření	uzavření	k1gNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
opravdu	opravdu	k6eAd1
již	již	k6eAd1
pozdní	pozdní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
konstruktér	konstruktér	k1gMnSc1
lodí	loď	k1gFnPc2
by	by	kYmCp3nS
nepředpokládal	předpokládat	k5eNaImAgInS
opačný	opačný	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
varianta	varianta	k1gFnSc1
později	pozdě	k6eAd2
zkoumána	zkoumat	k5eAaImNgFnS
počítačovou	počítačový	k2eAgFnSc7d1
simulací	simulace	k1gFnSc7
na	na	k7c6
modelu	model	k1gInSc6
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
zjištění	zjištění	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
potopil	potopit	k5eAaPmAgInS
dokonce	dokonce	k9
o	o	k7c4
40	#num#	k4
minut	minuta	k1gFnPc2
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabránění	zabránění	k1gNnSc3
zavření	zavření	k1gNnSc2
přepážek	přepážka	k1gFnPc2
ani	ani	k9
nebylo	být	k5eNaImAgNnS
technicky	technicky	k6eAd1
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
dosažení	dosažení	k1gNnSc6
kritické	kritický	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
vody	voda	k1gFnSc2
uvnitř	uvnitř	k7c2
lodě	loď	k1gFnSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
aktivovalo	aktivovat	k5eAaBmAgNnS
automatické	automatický	k2eAgNnSc1d1
uzavírání	uzavírání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
plavby	plavba	k1gFnSc2
</s>
<s>
V	v	k7c6
závěru	závěr	k1gInSc6
britské	britský	k2eAgFnSc2d1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
rozsah	rozsah	k1gInSc1
ztrát	ztráta	k1gFnPc2
po	po	k7c6
kolizi	kolize	k1gFnSc6
s	s	k7c7
ledovcem	ledovec	k1gInSc7
ovlivnila	ovlivnit	k5eAaPmAgFnS
i	i	k9
nadprůměrná	nadprůměrný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
lodi	loď	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
záznamů	záznam	k1gInPc2
plul	plout	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
v	v	k7c6
době	doba	k1gFnSc6
srážky	srážka	k1gFnSc2
normální	normální	k2eAgFnSc1d1
rychlostí	rychlost	k1gFnSc7
kolem	kolem	k7c2
21	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
39	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
o	o	k7c4
něco	něco	k3yInSc4
méně	málo	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
maximální	maximální	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
23	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
43	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
rychlost	rychlost	k1gFnSc4
nebyla	být	k5eNaImAgFnS
sice	sice	k8xC
obvyklá	obvyklý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
nebyla	být	k5eNaImAgFnS
nijak	nijak	k6eAd1
výjimečná	výjimečný	k2eAgFnSc1d1
ani	ani	k8xC
v	v	k7c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
očekávat	očekávat	k5eAaImF
výskyt	výskyt	k1gInSc4
ledovců	ledovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
ledovec	ledovec	k1gInSc1
bude	být	k5eAaImBp3nS
dostatečně	dostatečně	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jej	on	k3xPp3gMnSc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
zahlédnout	zahlédnout	k5eAaPmF
v	v	k7c6
dostatečném	dostatečný	k2eAgInSc6d1
předstihu	předstih	k1gInSc6
a	a	k8xC
provést	provést	k5eAaPmF
úhybný	úhybný	k2eAgInSc4d1
manévr	manévr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Bruce	Bruce	k1gFnSc1
Ismayovi	Ismaya	k1gMnSc3
a	a	k8xC
kapitánu	kapitán	k1gMnSc3
Smithovi	Smith	k1gMnSc3
bylo	být	k5eAaImAgNnS
potom	potom	k6eAd1
dáváno	dávat	k5eAaImNgNnS
za	za	k7c4
vinu	vina	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
přikázal	přikázat	k5eAaPmAgInS
plavbu	plavba	k1gFnSc4
takovou	takový	k3xDgFnSc7
rychlostí	rychlost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
sporný	sporný	k2eAgInSc4d1
bod	bod	k1gInSc4
příčiny	příčina	k1gFnPc1
katastrofy	katastrofa	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
často	často	k6eAd1
zmiňován	zmiňován	k2eAgMnSc1d1
a	a	k8xC
zdůrazňován	zdůrazňován	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
například	například	k6eAd1
ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
filmu	film	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vítr	vítr	k1gInSc1
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
kolize	kolize	k1gFnSc2
bylo	být	k5eAaImAgNnS
moře	moře	k1gNnSc1
klidné	klidný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
i	i	k9
bezměsíčná	bezměsíčný	k2eAgFnSc1d1
noc	noc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
normálních	normální	k2eAgInPc2d1
<g/>
,	,	kIx,
obvyklých	obvyklý	k2eAgInPc2d1
<g/>
,	,	kIx,
podmínek	podmínka	k1gFnPc2
v	v	k7c6
severním	severní	k2eAgInSc6d1
Atlantiku	Atlantik	k1gInSc6
fouká	foukat	k5eAaImIp3nS
vítr	vítr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
žene	hnát	k5eAaImIp3nS
vlny	vlna	k1gFnSc2
na	na	k7c4
základnu	základna	k1gFnSc4
ledovců	ledovec	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
tříští	tříštit	k5eAaImIp3nS
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
pěnu	pěna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
viditelná	viditelný	k2eAgFnSc1d1
a	a	k8xC
upozorňuje	upozorňovat	k5eAaImIp3nS
na	na	k7c4
ledovce	ledovec	k1gInPc4
i	i	k9
za	za	k7c4
bezměsíčné	bezměsíčný	k2eAgFnPc4d1
noci	noc	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
Vítr	vítr	k1gInSc1
vanoucí	vanoucí	k2eAgInSc1d1
od	od	k7c2
severozápadu	severozápad	k1gInSc2
a	a	k8xC
nízká	nízký	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
blízko	blízko	k7c2
nuly	nula	k1gFnSc2
jsou	být	k5eAaImIp3nP
nepříjemné	příjemný	k2eNgFnPc1d1
pro	pro	k7c4
pozorovací	pozorovací	k2eAgFnPc4d1
hlídky	hlídka	k1gFnPc4
<g/>
;	;	kIx,
taktéž	taktéž	k?
vítr	vítr	k1gInSc1
mohl	moct	k5eAaImAgInS
hnát	hnát	k1gInSc4
ledovce	ledovec	k1gInSc2
ve	v	k7c6
směru	směr	k1gInSc6
proti	proti	k7c3
lodi	loď	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zhroucení	zhroucení	k1gNnSc1
trupu	trup	k1gInSc2
</s>
<s>
Srážka	srážka	k1gFnSc1
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
způsobila	způsobit	k5eAaPmAgFnS
dlouhou	dlouhý	k2eAgFnSc4d1
trhlinu	trhlina	k1gFnSc4
v	v	k7c6
trupu	trup	k1gInSc6
Titanicu	Titanicus	k1gInSc2
v	v	k7c6
délce	délka	k1gFnSc6
šesti	šest	k4xCc2
komor	komora	k1gFnPc2
<g/>
,	,	kIx,
způsobila	způsobit	k5eAaPmAgFnS
poškození	poškození	k1gNnSc3
trupu	trup	k1gInSc2
v	v	k7c6
rozsahu	rozsah	k1gInSc6
větším	veliký	k2eAgInSc6d2
<g/>
,	,	kIx,
než	než	k8xS
konstruktéři	konstruktér	k1gMnPc1
při	při	k7c6
projektování	projektování	k1gNnSc6
trupu	trup	k1gInSc2
očekávali	očekávat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Požár	požár	k1gInSc1
</s>
<s>
Dle	dle	k7c2
dokumentu	dokument	k1gInSc2
„	„	k?
<g/>
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
New	New	k1gFnSc2
Evidence	evidence	k1gFnSc2
<g/>
“	“	k?
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
hlavní	hlavní	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
potopení	potopení	k1gNnSc2
plavidla	plavidlo	k1gNnSc2
požár	požár	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
několik	několik	k4yIc1
dní	den	k1gInPc2
před	před	k7c7
vyplutím	vyplutí	k1gNnSc7
ze	z	k7c2
Southamptonu	Southampton	k1gInSc2
vypukl	vypuknout	k5eAaPmAgInS
v	v	k7c6
podpalubním	podpalubní	k2eAgInSc6d1
uhelném	uhelný	k2eAgInSc6d1
bunkru	bunkr	k1gInSc6
č.	č.	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požárem	požár	k1gInSc7
způsobené	způsobený	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
mohly	moct	k5eAaImAgFnP
snížit	snížit	k5eAaPmF
pevnost	pevnost	k1gFnSc4
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
následně	následně	k6eAd1
fatálně	fatálně	k6eAd1
poškodil	poškodit	k5eAaPmAgInS
ledovec	ledovec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
ze	z	k7c2
zachráněných	zachráněný	k2eAgInPc2d1
topičů	topič	k1gInPc2
J.	J.	kA
Dilley	Dillea	k1gFnSc2
později	pozdě	k6eAd2
vypověděl	vypovědět	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nemohli	moct	k5eNaImAgMnP
jsme	být	k5eAaImIp1nP
dostat	dostat	k5eAaPmF
oheň	oheň	k1gInSc4
pod	pod	k7c4
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
mezi	mezi	k7c7
topiči	topič	k1gMnPc7
se	se	k3xPyFc4
říkalo	říkat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
až	až	k9
přistaneme	přistat	k5eAaPmIp1nP
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
musíme	muset	k5eAaImIp1nP
po	po	k7c6
odchodu	odchod	k1gInSc6
pasažérů	pasažér	k1gMnPc2
hned	hned	k6eAd1
vyprázdnit	vyprázdnit	k5eAaPmF
ostatní	ostatní	k2eAgFnPc4d1
velké	velký	k2eAgFnPc4d1
uhelny	uhelna	k1gFnPc4
a	a	k8xC
pak	pak	k6eAd1
přivolat	přivolat	k5eAaPmF
na	na	k7c4
pomoc	pomoc	k1gFnSc4
hasičské	hasičský	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
z	z	k7c2
tamního	tamní	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Objevení	objevení	k1gNnSc1
vraku	vrak	k1gInSc2
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
potopení	potopení	k1gNnSc6
lodi	loď	k1gFnSc2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
první	první	k4xOgFnPc1
snahy	snaha	k1gFnPc1
o	o	k7c4
nalezení	nalezení	k1gNnSc4
lodi	loď	k1gFnSc2
a	a	k8xC
vyzvednutí	vyzvednutí	k1gNnSc1
jejího	její	k3xOp3gInSc2
trupu	trup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hloubka	hloubka	k1gFnSc1
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
čtyř	čtyři	k4xCgInPc2
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
tyto	tento	k3xDgInPc1
plány	plán	k1gInPc1
nebyly	být	k5eNaImAgInP
vzhledem	vzhledem	k7c3
k	k	k7c3
úrovni	úroveň	k1gFnSc3
tehdejší	tehdejší	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
realizovatelné	realizovatelný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
vážný	vážný	k2eAgInSc1d1
pokus	pokus	k1gInSc1
o	o	k7c4
nalezení	nalezení	k1gNnSc4
vraku	vrak	k1gInSc2
Titanicu	Titanicus	k1gInSc2
podnikl	podniknout	k5eAaPmAgMnS
texaský	texaský	k2eAgMnSc1d1
naftař	naftař	k1gMnSc1
a	a	k8xC
multimilionář	multimilionář	k1gMnSc1
Jack	Jack	k1gMnSc1
Grimm	Grimm	k1gMnSc1
v	v	k7c6
červenci	červenec	k1gInSc6
1980	#num#	k4
<g/>
,	,	kIx,
neuspěl	uspět	k5eNaPmAgInS
však	však	k9
ani	ani	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
ani	ani	k8xC
v	v	k7c6
žádném	žádný	k3yNgInSc6
ze	z	k7c2
dvou	dva	k4xCgInPc2
dalších	další	k2eAgInPc2d1
pokusů	pokus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následoval	následovat	k5eAaImAgMnS
jej	on	k3xPp3gNnSc4
oceánograf	oceánograf	k1gMnSc1
a	a	k8xC
geolog	geolog	k1gMnSc1
Robert	Robert	k1gMnSc1
D.	D.	kA
Ballard	Ballard	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
expedice	expedice	k1gFnSc1
objevila	objevit	k5eAaPmAgFnS
první	první	k4xOgFnPc4
trosky	troska	k1gFnPc4
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1985	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
Finanční	finanční	k2eAgNnSc1d1
i	i	k8xC
technické	technický	k2eAgNnSc1d1
zajištění	zajištění	k1gNnSc1
celé	celý	k2eAgFnSc2d1
operace	operace	k1gFnSc2
spadalo	spadat	k5eAaImAgNnS,k5eAaPmAgNnS
pod	pod	k7c4
americké	americký	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
expedice	expedice	k1gFnSc2
nebylo	být	k5eNaImAgNnS
pátrání	pátrání	k1gNnSc1
po	po	k7c6
Titanicu	Titanicus	k1gInSc6
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ale	ale	k8xC
především	především	k6eAd1
nalezení	nalezení	k1gNnSc3
a	a	k8xC
prozkoumání	prozkoumání	k1gNnSc3
vraků	vrak	k1gInPc2
dvou	dva	k4xCgFnPc6
potopených	potopený	k2eAgFnPc2d1
amerických	americký	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
–	–	k?
USS	USS	kA
Thresher	Threshra	k1gFnPc2
a	a	k8xC
USS	USS	kA
Scorpion	Scorpion	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zmizely	zmizet	k5eAaPmAgFnP
i	i	k9
s	s	k7c7
posádkami	posádka	k1gFnPc7
(	(	kIx(
<g/>
Thresher	Threshra	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
a	a	k8xC
Scorpion	Scorpion	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Robert	Robert	k1gMnSc1
D.	D.	kA
Ballard	Ballard	k1gMnSc1
poskytl	poskytnout	k5eAaPmAgMnS
námořnictvu	námořnictvo	k1gNnSc3
své	svůj	k3xOyFgFnSc2
bohaté	bohatý	k2eAgFnSc2d1
zkušenosti	zkušenost	k1gFnSc2
oceánografa	oceánograf	k1gMnSc2
a	a	k8xC
především	především	k6eAd1
nové	nový	k2eAgNnSc4d1
video	video	k1gNnSc4
technologie	technologie	k1gFnSc1
k	k	k7c3
průzkumu	průzkum	k1gInSc3
mořského	mořský	k2eAgNnSc2d1
dna	dno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
vysokou	vysoký	k2eAgFnSc4d1
rozlišovací	rozlišovací	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
a	a	k8xC
umožňovaly	umožňovat	k5eAaImAgFnP
detailní	detailní	k2eAgInSc4d1
průzkum	průzkum	k1gInSc4
a	a	k8xC
mapování	mapování	k1gNnSc4
sledované	sledovaný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vedením	vedení	k1gNnSc7
námořnictva	námořnictvo	k1gNnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
Ballard	Ballard	k1gInSc1
dohodl	dohodnout	k5eAaPmAgInS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podaří	podařit	k5eAaPmIp3nS
objevit	objevit	k5eAaPmF
a	a	k8xC
prozkoumat	prozkoumat	k5eAaPmF
obě	dva	k4xCgFnPc4
potopené	potopený	k2eAgFnPc4d1
jaderné	jaderný	k2eAgFnPc4d1
ponorky	ponorka	k1gFnPc4
a	a	k8xC
nepřekročí	překročit	k5eNaPmIp3nP
při	při	k7c6
tom	ten	k3xDgNnSc6
povolený	povolený	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc4
<g/>
,	,	kIx,
získá	získat	k5eAaPmIp3nS
možnost	možnost	k1gFnSc4
ve	v	k7c6
zbylém	zbylý	k2eAgInSc6d1
čase	čas	k1gInSc6
expedice	expedice	k1gFnSc1
pátrat	pátrat	k5eAaImF
i	i	k9
po	po	k7c6
vraku	vrak	k1gInSc6
Titanicu	Titanicus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Námořnictvo	námořnictvo	k1gNnSc1
USA	USA	kA
poskytlo	poskytnout	k5eAaPmAgNnS
loď	loď	k1gFnSc4
i	i	k8xC
posádku	posádka	k1gFnSc4
<g/>
,	,	kIx,
financovalo	financovat	k5eAaBmAgNnS
výrobu	výroba	k1gFnSc4
vlečené	vlečený	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
byly	být	k5eAaImAgFnP
umístěny	umístit	k5eAaPmNgFnP
videokamery	videokamera	k1gFnPc1
a	a	k8xC
osvětlení	osvětlení	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
finančně	finančně	k6eAd1
zajistilo	zajistit	k5eAaPmAgNnS
celou	celá	k1gFnSc4
přísně	přísně	k6eAd1
utajenou	utajený	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důstojník	důstojník	k1gMnSc1
v	v	k7c6
záloze	záloha	k1gFnSc6
Robert	Robert	k1gMnSc1
D.	D.	kA
Ballard	Ballard	k1gMnSc1
byl	být	k5eAaImAgMnS
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
aktivní	aktivní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
a	a	k8xC
vázán	vázán	k2eAgMnSc1d1
přísahou	přísaha	k1gFnSc7
mlčenlivosti	mlčenlivost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Vraky	vrak	k1gInPc1
obou	dva	k4xCgFnPc2
ponorek	ponorka	k1gFnPc2
se	se	k3xPyFc4
týmu	tým	k1gInSc2
nakonec	nakonec	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
objevit	objevit	k5eAaPmF
a	a	k8xC
prozkoumat	prozkoumat	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
hledání	hledání	k1gNnSc6
Titanicu	Titanicus	k1gInSc2
zbylo	zbýt	k5eAaPmAgNnS
jen	jen	k9
posledních	poslední	k2eAgInPc2d1
čtrnáct	čtrnáct	k4xCc4
dnů	den	k1gInPc2
pátrání	pátrání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
Ballard	Ballard	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
cílovou	cílový	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
pátrání	pátrání	k1gNnSc2
prohledávat	prohledávat	k5eAaImF
v	v	k7c6
navazujících	navazující	k2eAgInPc6d1
pásech	pás	k1gInPc6
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
ale	ale	k8xC
nechával	nechávat	k5eAaImAgMnS
mílové	mílový	k2eAgInPc4d1
rozestupy	rozestup	k1gInPc4
a	a	k8xC
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
na	na	k7c4
stopu	stopa	k1gFnSc4
vraku	vrak	k1gInSc2
přivede	přivést	k5eAaPmIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
pás	pás	k1gInSc1
trosek	troska	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
podle	podle	k7c2
Ballardova	Ballardův	k2eAgInSc2d1
odhadu	odhad	k1gInSc2
musel	muset	k5eAaImAgMnS
vytvořit	vytvořit	k5eAaPmF
po	po	k7c4
rozlomení	rozlomení	k1gNnSc4
potápějící	potápějící	k2eAgFnSc2d1
se	se	k3xPyFc4
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
taktika	taktika	k1gFnSc1
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
být	být	k5eAaImF
správná	správný	k2eAgFnSc1d1
a	a	k8xC
pouhé	pouhý	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
dny	den	k1gInPc4
před	před	k7c7
ukončením	ukončení	k1gNnSc7
expedice	expedice	k1gFnSc1
zaznamenal	zaznamenat	k5eAaPmAgMnS
robot	robot	k1gMnSc1
Argo	Argo	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
kamery	kamera	k1gFnPc1
první	první	k4xOgFnSc2
stopy	stopa	k1gFnSc2
po	po	k7c6
vraku	vrak	k1gInSc6
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Ballard	Ballard	k1gMnSc1
na	na	k7c6
pozdější	pozdní	k2eAgFnSc6d2
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
objevil	objevit	k5eAaPmAgInS
vrak	vrak	k1gInSc4
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
nucen	nutit	k5eAaImNgMnS
zatajit	zatajit	k5eAaPmF
informace	informace	k1gFnPc4
o	o	k7c6
pravém	pravý	k2eAgInSc6d1
účelu	účel	k1gInSc6
expedice	expedice	k1gFnSc2
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
americké	americký	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
pouze	pouze	k6eAd1
testoval	testovat	k5eAaImAgMnS
nové	nový	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vrak	vrak	k1gInSc1
lodi	loď	k1gFnSc2
</s>
<s>
Vrak	vrak	k1gInSc1
Titaniku	Titanic	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
</s>
<s>
Model	model	k1gInSc1
vraku	vrak	k1gInSc2
Titanicu	Titanicus	k1gInSc2
</s>
<s>
Lodní	lodní	k2eAgInSc1d1
zvon	zvon	k1gInSc1
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
</s>
<s>
Po	po	k7c6
rozlomení	rozlomení	k1gNnSc6
trupu	trup	k1gInSc2
se	se	k3xPyFc4
každá	každý	k3xTgFnSc1
část	část	k1gFnSc1
potápěla	potápět	k5eAaImAgFnS
odlišně	odlišně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgFnSc1d1
část	část	k1gFnSc1
poměrně	poměrně	k6eAd1
měkce	měkko	k6eAd1
dosedla	dosednout	k5eAaPmAgFnS
na	na	k7c4
dno	dno	k1gNnSc4
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
klesala	klesat	k5eAaImAgFnS
ke	k	k7c3
dnu	dno	k1gNnSc3
daleko	daleko	k6eAd1
rychleji	rychle	k6eAd2
<g/>
,	,	kIx,
i	i	k8xC
vlivem	vliv	k1gInSc7
imploze	imploze	k1gFnSc2
stlačeného	stlačený	k2eAgInSc2d1
vzduchu	vzduch	k1gInSc2
ve	v	k7c6
vodotěsných	vodotěsný	k2eAgFnPc6d1
komorách	komora	k1gFnPc6
na	na	k7c6
zádi	záď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
se	se	k3xPyFc4
zaryla	zarýt	k5eAaPmAgFnS
hluboko	hluboko	k6eAd1
do	do	k7c2
bahna	bahno	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejzajímavější	zajímavý	k2eAgNnSc1d3
částí	část	k1gFnSc7
trosek	troska	k1gFnPc2
<g/>
,	,	kIx,
objevených	objevený	k2eAgFnPc2d1
týmem	tým	k1gInSc7
Roberta	Robert	k1gMnSc2
D.	D.	kA
Ballarda	Ballard	k1gMnSc2
<g/>
,	,	kIx,
představoval	představovat	k5eAaImAgInS
lodní	lodní	k2eAgInSc1d1
kotel	kotel	k1gInSc1
na	na	k7c6
souřadnicích	souřadnice	k1gFnPc6
41	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
49	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
O	o	k7c4
den	den	k1gInSc4
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
<g />
.	.	kIx.
</s>
<s hack="1">
nalezena	nalezen	k2eAgFnSc1d1
přední	přední	k2eAgFnSc1d1
část	část	k1gFnSc1
trupu	trup	k1gInSc2
(	(	kIx(
<g/>
prokázalo	prokázat	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
měla	mít	k5eAaImAgFnS
pravdu	pravda	k1gFnSc4
ta	ten	k3xDgNnPc4
svědectví	svědectví	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
vyšetřovací	vyšetřovací	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
odmítnuta	odmítnut	k2eAgFnSc1d1
a	a	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tvrdila	tvrdit	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
trup	trup	k1gInSc1
lodi	loď	k1gFnSc2
se	se	k3xPyFc4
krátce	krátce	k6eAd1
před	před	k7c7
potopením	potopení	k1gNnSc7
rozlomil	rozlomit	k5eAaPmAgMnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
)	)	kIx)
na	na	k7c6
pozici	pozice	k1gFnSc6
41	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
57	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Ještě	ještě	k6eAd1
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
nalezena	naleznout	k5eAaPmNgFnS,k5eAaBmNgFnS
i	i	k9
záď	záď	k1gFnSc1
ležící	ležící	k2eAgFnSc1d1
600	#num#	k4
stop	stopa	k1gFnPc2
od	od	k7c2
přídě	příď	k1gFnSc2
na	na	k7c6
souřadnicích	souřadnice	k1gFnPc6
41	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
49	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Za	za	k7c4
místo	místo	k1gNnSc4
potopení	potopení	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
brát	brát	k5eAaImF
polohu	poloha	k1gFnSc4
nalezeného	nalezený	k2eAgInSc2d1
kotle	kotel	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
něhož	jenž	k3xRgNnSc2
je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
uvolnil	uvolnit	k5eAaPmAgInS
při	při	k7c6
rozlomení	rozlomení	k1gNnSc6
Titanicu	Titanicus	k1gInSc2
a	a	k8xC
klesal	klesat	k5eAaImAgInS
přímo	přímo	k6eAd1
ke	k	k7c3
dnu	dno	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
P.	P.	kA
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bohužel	bohužel	k9
neexistuje	existovat	k5eNaImIp3nS
přímý	přímý	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
fázi	fáze	k1gFnSc6
potápění	potápění	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
úplnému	úplný	k2eAgNnSc3d1
oddělení	oddělení	k1gNnSc3
obou	dva	k4xCgInPc2
částí	část	k1gFnPc2
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
na	na	k7c6
hladině	hladina	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
jaké	jaký	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
hloubce	hloubka	k1gFnSc6
během	během	k7c2
potápění	potápění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
příď	příď	k1gFnSc1
je	být	k5eAaImIp3nS
zabořena	zabořit	k5eAaPmNgFnS
v	v	k7c6
bahně	bahno	k1gNnSc6
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
možno	možno	k6eAd1
prozkoumat	prozkoumat	k5eAaPmF
celou	celý	k2eAgFnSc4d1
trhlinu	trhlina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
třetí	třetí	k4xOgFnSc2
přepážky	přepážka	k1gFnSc2
dál	daleko	k6eAd2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
inkriminovaná	inkriminovaný	k2eAgFnSc1d1
část	část	k1gFnSc1
trupu	trup	k1gInSc2
přístupná	přístupný	k2eAgFnSc1d1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenán	k2eAgNnSc1d1
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
poškození	poškození	k1gNnSc1
zjevně	zjevně	k6eAd1
související	související	k2eAgMnSc1d1
s	s	k7c7
nárazem	náraz	k1gInSc7
na	na	k7c4
kru	kra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nešlo	jít	k5eNaImAgNnS
o	o	k7c4
celistvou	celistvý	k2eAgFnSc4d1
trhlinu	trhlina	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
vyrvané	vyrvaný	k2eAgInPc4d1
nýty	nýt	k1gInPc4
a	a	k8xC
rozestoupení	rozestoupení	k1gNnSc4
a	a	k8xC
zprohýbání	zprohýbání	k1gNnSc4
ocelových	ocelový	k2eAgInPc2d1
plátů	plát	k1gInPc2
tvořících	tvořící	k2eAgFnPc2d1
trup	trupa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Experti	expert	k1gMnPc1
došli	dojít	k5eAaPmAgMnP
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
vrak	vrak	k1gInSc1
nelze	lze	k6eNd1
vyzvednout	vyzvednout	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
poškozen	poškodit	k5eAaPmNgInS
korozí	koroze	k1gFnSc7
a	a	k8xC
při	při	k7c6
zvedání	zvedání	k1gNnSc6
by	by	kYmCp3nP
se	se	k3xPyFc4
nutně	nutně	k6eAd1
rozpadl	rozpadnout	k5eAaPmAgInS
na	na	k7c4
kusy	kus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výzkumů	výzkum	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
přispívají	přispívat	k5eAaImIp3nP
k	k	k7c3
rozpadu	rozpad	k1gInSc3
vraku	vrak	k1gInSc2
významně	významně	k6eAd1
i	i	k9
nově	nova	k1gFnSc3
objevené	objevený	k2eAgFnSc2d1
bakterie	bakterie	k1gFnSc2
Halmonas	Halmonas	k1gMnSc1
titanicae	titanicae	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
vrak	vrak	k1gInSc1
navštíven	navštívit	k5eAaPmNgInS
ještě	ještě	k6eAd1
několikrát	několikrát	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
z	z	k7c2
něj	on	k3xPp3gInSc2
francouzská	francouzský	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
vyzvedla	vyzvednout	k5eAaPmAgFnS
několik	několik	k4yIc4
set	sto	k4xCgNnPc2
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
setkalo	setkat	k5eAaPmAgNnS
s	s	k7c7
hlubokým	hluboký	k2eAgInSc7d1
nesouhlasem	nesouhlas	k1gInSc7
zejména	zejména	k9
americké	americký	k2eAgFnSc2d1
a	a	k8xC
britské	britský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
<g/>
,	,	kIx,
stále	stále	k6eAd1
žijících	žijící	k2eAgMnPc2d1
účastníků	účastník	k1gMnPc2
katastrofy	katastrofa	k1gFnSc2
a	a	k8xC
příbuzných	příbuzný	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
byla	být	k5eAaImAgFnS
označena	označit	k5eAaPmNgFnS
za	za	k7c4
vykradače	vykradač	k1gMnPc4
hrobů	hrob	k1gInPc2
a	a	k8xC
byly	být	k5eAaImAgFnP
vzneseny	vznést	k5eAaPmNgInP
požadavky	požadavek	k1gInPc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
Titanicu	Titanic	k1gMnSc3
dostalo	dostat	k5eAaPmAgNnS
oficiálního	oficiální	k2eAgNnSc2d1
prohlášení	prohlášení	k1gNnSc2
za	za	k7c4
hrob	hrob	k1gInSc4
a	a	k8xC
památník	památník	k1gInSc4
neštěstí	neštěstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
ale	ale	k9
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
leží	ležet	k5eAaImIp3nS
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
a	a	k8xC
nemá	mít	k5eNaImIp3nS
majitele	majitel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgMnSc7d1
oficiálním	oficiální	k2eAgMnSc7d1
držitelem	držitel	k1gMnSc7
práv	právo	k1gNnPc2
na	na	k7c4
vyzvedávání	vyzvedávání	k1gNnSc4
artefaktů	artefakt	k1gInPc2
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc1
pracovníci	pracovník	k1gMnPc1
se	se	k3xPyFc4
k	k	k7c3
vraku	vrak	k1gInSc3
potápějí	potápět	k5eAaImIp3nP
a	a	k8xC
zkoumají	zkoumat	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
vrak	vrak	k1gInSc1
od	od	k7c2
předešlých	předešlý	k2eAgNnPc2d1
let	léto	k1gNnPc2
změnil	změnit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongres	kongres	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
schválil	schválit	k5eAaPmAgInS
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zakazuje	zakazovat	k5eAaImIp3nS
prodej	prodej	k1gInSc1
a	a	k8xC
vystavování	vystavování	k1gNnSc1
předmětů	předmět	k1gInPc2
vyzvednutých	vyzvednutý	k2eAgInPc2d1
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
USA	USA	kA
a	a	k8xC
vyzval	vyzvat	k5eAaPmAgInS
k	k	k7c3
respektování	respektování	k1gNnSc3
vraku	vrak	k1gInSc2
jako	jako	k8xC,k8xS
mezinárodního	mezinárodní	k2eAgInSc2d1
památníku	památník	k1gInSc2
a	a	k8xC
hrobu	hrob	k1gInSc2
1	#num#	k4
517	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Titanic	Titanic	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
hloubce	hloubka	k1gFnSc6
13	#num#	k4
000	#num#	k4
stop	stopa	k1gFnPc2
na	na	k7c6
mírně	mírně	k6eAd1
se	se	k3xPyFc4
svažujícím	svažující	k2eAgNnSc6d1
dně	dno	k1gNnSc6
alpského	alpský	k2eAgInSc2d1
typu	typ	k1gInSc2
s	s	k7c7
výhledem	výhled	k1gInSc7
na	na	k7c4
malý	malý	k2eAgInSc4d1
kaňon	kaňon	k1gInSc4
opodál	opodál	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
příď	příď	k1gFnSc4
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
severu	sever	k1gInSc3
a	a	k8xC
loď	loď	k1gFnSc1
sedí	sedit	k5eAaImIp3nS
vzpřímeně	vzpřímeně	k6eAd1
na	na	k7c6
dně	dno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
tam	tam	k6eAd1
žádné	žádný	k3yNgNnSc4
světlo	světlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
proniklo	proniknout	k5eAaPmAgNnS
tak	tak	k6eAd1
hluboko	hluboko	k6eAd1
a	a	k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
pouze	pouze	k6eAd1
nepatrný	patrný	k2eNgInSc4d1,k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tiché	tichý	k2eAgNnSc1d1
a	a	k8xC
klidné	klidný	k2eAgNnSc1d1
místo	místo	k1gNnSc1
vhodné	vhodný	k2eAgNnSc1d1
odpočinku	odpočinek	k1gInSc2
památky	památka	k1gFnSc2
největšího	veliký	k2eAgNnSc2d3
námořního	námořní	k2eAgNnSc2d1
neštěstí	neštěstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xS,k8xC
to	ten	k3xDgNnSc1
tak	tak	k9
navždy	navždy	k6eAd1
zůstane	zůstat	k5eAaPmIp3nS
a	a	k8xC
Bůh	bůh	k1gMnSc1
požehná	požehnat	k5eAaPmIp3nS
těmto	tento	k3xDgFnPc3
duším	duše	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
Dr	dr	kA
<g/>
.	.	kIx.
Robert	Robert	k1gMnSc1
Ballard	Ballard	k1gMnSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1985	#num#	k4
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
P.	P.	kA
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legendy	legenda	k1gFnPc1
<g/>
,	,	kIx,
mýty	mýtus	k1gInPc1
<g/>
,	,	kIx,
teorie	teorie	k1gFnPc1
</s>
<s>
Jako	jako	k9
každá	každý	k3xTgFnSc1
velká	velký	k2eAgFnSc1d1
tragédie	tragédie	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
pozornosti	pozornost	k1gFnSc2
a	a	k8xC
publicity	publicita	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
ztroskotání	ztroskotání	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
přitáhlo	přitáhnout	k5eAaPmAgNnS
k	k	k7c3
sobě	se	k3xPyFc3
velkou	velký	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
<g/>
,	,	kIx,
objeví	objevit	k5eAaPmIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
dříve	dříve	k6eAd2
napsaná	napsaný	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
nebo	nebo	k8xC
povídka	povídka	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
obsahu	obsah	k1gInSc6
je	být	k5eAaImIp3nS
popsáno	popsán	k2eAgNnSc1d1
ztroskotání	ztroskotání	k1gNnSc1
podobné	podobný	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
vysledovat	vysledovat	k5eAaImF,k5eAaPmF
mystické	mystický	k2eAgFnPc4d1
souvislosti	souvislost	k1gFnPc4
mezi	mezi	k7c7
událostmi	událost	k1gFnPc7
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
…	…	k?
</s>
<s>
„	„	k?
<g/>
no	no	k9
pope	pop	k1gMnSc5
<g/>
“	“	k?
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Morgan	morgan	k1gMnSc1
Robertson	Robertson	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
námořník	námořník	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
nakladatelství	nakladatelství	k1gNnSc6
M.	M.	kA
F.	F.	kA
Mansfield	Mansfield	k1gMnSc1
povídku	povídka	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
popisováno	popisovat	k5eAaImNgNnS
ztroskotání	ztroskotání	k1gNnSc1
„	„	k?
<g/>
nepotopitelné	potopitelný	k2eNgFnSc2d1
<g/>
“	“	k?
a	a	k8xC
největší	veliký	k2eAgFnPc4d3
dosud	dosud	k6eAd1
postavené	postavený	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
Titan	titan	k1gInSc1
podobných	podobný	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
<g/>
,	,	kIx,
parametrů	parametr	k1gInPc2
a	a	k8xC
konstrukce	konstrukce	k1gFnSc1
jako	jako	k8xS,k8xC
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
povídce	povídka	k1gFnSc6
loď	loď	k1gFnSc1
na	na	k7c6
plavbě	plavba	k1gFnSc6
(	(	kIx(
<g/>
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
dubnu	duben	k1gInSc6
<g/>
)	)	kIx)
částečně	částečně	k6eAd1
najela	najet	k5eAaPmAgFnS
na	na	k7c4
ledovec	ledovec	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozsáhlému	rozsáhlý	k2eAgNnSc3d1
popraskání	popraskání	k1gNnSc3
trupu	trup	k1gInSc2
a	a	k8xC
velmi	velmi	k6eAd1
rapidnímu	rapidní	k2eAgNnSc3d1
potopení	potopení	k1gNnSc3
lodě	loď	k1gFnSc2
a	a	k8xC
smrti	smrt	k1gFnSc2
drtivé	drtivý	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
pasažérů	pasažér	k1gMnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
když	když	k8xS
se	se	k3xPyFc4
spojení	spojení	k1gNnSc1
Titanic	Titanic	k1gInSc1
–	–	k?
„	„	k?
<g/>
nepotopitelný	potopitelný	k2eNgInSc4d1
</s>
<s>
<g/>
“	“	k?
po	po	k7c6
katastrofě	katastrofa	k1gFnSc6
objevovalo	objevovat	k5eAaImAgNnS
téměř	téměř	k6eAd1
neustále	neustále	k6eAd1
a	a	k8xC
přežívá	přežívat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
pravdou	pravda	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
před	před	k7c7
svou	svůj	k3xOyFgFnSc7
první	první	k4xOgFnSc7
plavbou	plavba	k1gFnSc7
takto	takto	k6eAd1
označován	označován	k2eAgMnSc1d1
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k6eAd1
společností	společnost	k1gFnPc2
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
nebo	nebo	k8xC
loděnicí	loděnice	k1gFnSc7
Harland	Harlanda	k1gFnPc2
and	and	k?
Wolff	Wolff	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
často	často	k6eAd1
zmiňovaného	zmiňovaný	k2eAgInSc2d1
lodního	lodní	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
je	být	k5eAaImIp3nS
sporné	sporný	k2eAgNnSc1d1
<g/>
,	,	kIx,
hrál	hrát	k5eAaImAgMnS
<g/>
-li	-li	k?
až	až	k8xS
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
chvíle	chvíle	k1gFnSc2
na	na	k7c6
člunové	člunový	k2eAgFnSc6d1
palubě	paluba	k1gFnSc6
a	a	k8xC
hrál	hrát	k5eAaImAgMnS
<g/>
-li	-li	k?
v	v	k7c6
průběhu	průběh	k1gInSc6
katastrofy	katastrofa	k1gFnSc2
vůbec	vůbec	k9
<g/>
,	,	kIx,
případně	případně	k6eAd1
jaké	jaký	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
hrál	hrát	k5eAaImAgMnS
skladby	skladba	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
i	i	k9
když	když	k8xS
věta	věta	k1gFnSc1
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
hudebníků	hudebník	k1gMnPc2
„	„	k?
<g/>
Pánové	pán	k1gMnPc1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
mi	já	k3xPp1nSc3
ctí	ctít	k5eAaImIp3nP
s	s	k7c7
vámi	vy	k3xPp2nPc7
dnes	dnes	k6eAd1
hrát	hrát	k5eAaImF
<g/>
“	“	k?
ve	v	k7c6
filmu	film	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
je	být	k5eAaImIp3nS
působivá	působivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irští	irský	k2eAgMnPc1d1
katoličtí	katolický	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
si	se	k3xPyFc3
s	s	k7c7
hrůzou	hrůza	k1gFnSc7
šeptali	šeptat	k5eAaImAgMnP
o	o	k7c6
údajném	údajný	k2eAgNnSc6d1
sériovém	sériový	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
Titanicu	Titanicus	k1gInSc2
3909	#num#	k4
04	#num#	k4
(	(	kIx(
<g/>
Titanic	Titanic	k1gInSc1
měl	mít	k5eAaImAgInS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
sériové	sériový	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
401	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pohledu	pohled	k1gInSc6
z	z	k7c2
paluby	paluba	k1gFnSc2
pak	pak	k6eAd1
zrcadlově	zrcadlově	k6eAd1
odražené	odražený	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
od	od	k7c2
vody	voda	k1gFnSc2
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
jako	jako	k9
nápis	nápis	k1gInSc4
„	„	k?
<g/>
no	no	k9
pope	pop	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
volně	volně	k6eAd1
přeloženo	přeložen	k2eAgNnSc4d1
„	„	k?
<g/>
papež	papež	k1gMnSc1
neexistuje	existovat	k5eNaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číslo	číslo	k1gNnSc1
3909	#num#	k4
04	#num#	k4
si	se	k3xPyFc3
vymysleli	vymyslet	k5eAaPmAgMnP
protestantští	protestantský	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
loděnice	loděnice	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
postrašili	postrašit	k5eAaPmAgMnP
katolické	katolický	k2eAgMnPc4d1
dělníky	dělník	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Omylem	omylem	k6eAd1
je	být	k5eAaImIp3nS
i	i	k9
údajně	údajně	k6eAd1
první	první	k4xOgNnSc1
vysílání	vysílání	k1gNnSc1
signálu	signál	k1gInSc2
„	„	k?
<g/>
SOS	sos	k1gInSc1
<g/>
“	“	k?
právě	právě	k6eAd1
z	z	k7c2
paluby	paluba	k1gFnSc2
potápějícího	potápějící	k2eAgInSc2d1
se	se	k3xPyFc4
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
kterému	který	k3yRgNnSc3,k3yQgNnSc3,k3yIgNnSc3
okolní	okolní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
nerozuměly	rozumět	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volání	volání	k1gNnSc4
„	„	k?
<g/>
SOS	sos	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
v	v	k7c6
Morseově	Morseův	k2eAgFnSc6d1
abecedě	abeceda	k1gFnSc6
·	·	k?
·	·	k?
·	·	k?
–	–	k?
–	–	k?
–	–	k?
·	·	k?
·	·	k?
·	·	k?
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
mezinárodně	mezinárodně	k6eAd1
používáno	používat	k5eAaImNgNnS
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1908	#num#	k4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
souběžně	souběžně	k6eAd1
používalo	používat	k5eAaImAgNnS
i	i	k9
starší	starý	k2eAgNnSc1d2
volání	volání	k1gNnSc1
„	„	k?
<g/>
CQD	CQD	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
v	v	k7c6
Morseově	Morseův	k2eAgFnSc6d1
abecedě	abeceda	k1gFnSc6
–	–	k?
·	·	k?
–	–	k?
·	·	k?
–	–	k?
–	–	k?
·	·	k?
–	–	k?
–	–	k?
·	·	k?
·	·	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
Titanicem	Titanic	k1gMnSc7
je	být	k5eAaImIp3nS
spojováno	spojovat	k5eAaImNgNnS
i	i	k9
prokletí	prokletí	k1gNnSc1
pro	pro	k7c4
údajnou	údajný	k2eAgFnSc4d1
přepravu	přeprava	k1gFnSc4
starodávné	starodávný	k2eAgFnSc2d1
mumie	mumie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
jednat	jednat	k5eAaImF
i	i	k9
o	o	k7c4
pojistný	pojistný	k2eAgInSc4d1
podvod	podvod	k1gInSc4
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
–	–	k?
potopen	potopen	k2eAgMnSc1d1
neměl	mít	k5eNaImAgMnS
být	být	k5eAaImF
Titanic	Titanic	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Olympic	Olympic	k1gMnSc1
–	–	k?
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
doku	dok	k1gInSc6
<g/>
,	,	kIx,
mělo	mít	k5eAaImAgNnS
totiž	totiž	k9
dojít	dojít	k5eAaPmF
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
záměně	záměna	k1gFnSc3
co	co	k9
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnPc2
insignií	insignie	k1gFnPc2
<g/>
,	,	kIx,
nápisů	nápis	k1gInPc2
<g/>
,	,	kIx,
značení	značení	k1gNnSc1
na	na	k7c6
jejich	jejich	k3xOp3gFnPc6
palubách	paluba	k1gFnPc6
atd.	atd.	kA
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
Této	tento	k3xDgFnSc6
teorii	teorie	k1gFnSc6
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
knihu	kniha	k1gFnSc4
spisovatel	spisovatel	k1gMnSc1
Robin	Robina	k1gFnPc2
Gardiner	Gardiner	k1gInSc4
s	s	k7c7
názvem	název	k1gInSc7
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
ship	ship	k1gMnSc1
that	that	k1gMnSc1
never	never	k1gMnSc1
sank	sank	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
První	první	k4xOgNnSc1
filmové	filmový	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
katastrofy	katastrofa	k1gFnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
němého	němý	k2eAgInSc2d1
filmu	film	k1gInSc2
Saved	Saved	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
Zachráněná	zachráněná	k1gFnSc1
z	z	k7c2
Titanicu	Titanicus	k1gInSc2
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
již	již	k9
několik	několik	k4yIc1
měsíců	měsíc	k1gInPc2
po	po	k7c6
katastrofě	katastrofa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
rolí	role	k1gFnPc2
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
herečka	herečka	k1gFnSc1
Dorothy	Dorotha	k1gFnSc2
Gibsonová	Gibsonová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
katastrofu	katastrofa	k1gFnSc4
přežila	přežít	k5eAaPmAgFnS
ve	v	k7c6
člunu	člun	k1gInSc6
7	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
i	i	k9
autorkou	autorka	k1gFnSc7
scénáře	scénář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
měl	mít	k5eAaImAgInS
premiéru	premiéra	k1gFnSc4
14	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP
další	další	k2eAgInPc1d1
filmy	film	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Atlantik	Atlantik	k1gInSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
,	,	kIx,
Oscar	Oscar	k1gInSc1
za	za	k7c4
scénář	scénář	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Zkáza	zkáza	k1gFnSc1
Titanicu	Titanicus	k1gInSc2
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
S.	S.	kA
<g/>
O.S.	O.S.	k1gFnSc1
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vyzvednutí	vyzvednutí	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Titanica	Titanica	k1gFnSc1
(	(	kIx(
<g/>
94	#num#	k4
<g/>
minutový	minutový	k2eAgInSc1d1
dokument	dokument	k1gInSc1
se	s	k7c7
záběry	záběr	k1gInPc7
z	z	k7c2
vraku	vrak	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
,	,	kIx,
velkofilm	velkofilm	k1gInSc4
režiséra	režisér	k1gMnSc2
Jamese	Jamese	k1gFnSc2
Camerona	Cameron	k1gMnSc2
<g/>
,	,	kIx,
11	#num#	k4
Oscarů	Oscar	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
,	,	kIx,
čtyřdílná	čtyřdílný	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
minisérie	minisérie	k1gFnSc1
<g/>
,	,	kIx,
koprodukce	koprodukce	k1gFnSc2
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
/	/	kIx~
Kanada	Kanada	k1gFnSc1
/	/	kIx~
Maďarsko	Maďarsko	k1gNnSc1
/	/	kIx~
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Výlety	výlet	k1gInPc1
k	k	k7c3
vraku	vrak	k1gInSc3
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Londýnská	londýnský	k2eAgFnSc1d1
cestovní	cestovní	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
Blue	Bluus	k1gMnSc5
Marble	Marble	k1gMnSc5
Private	Privat	k1gMnSc5
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
nabízí	nabízet	k5eAaImIp3nS
movitým	movitý	k2eAgMnPc3d1
zájemcům	zájemce	k1gMnPc3
cestu	cesta	k1gFnSc4
k	k	k7c3
vraku	vrak	k1gInSc3
Titaniku	Titanic	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
chystá	chystat	k5eAaImIp3nS
na	na	k7c4
květen	květen	k1gInSc4
roku	rok	k1gInSc2
2018	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
obsazená	obsazený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
trvat	trvat	k5eAaImF
osm	osm	k4xCc4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
cíli	cíl	k1gInSc3
turisty	turist	k1gMnPc4
dopraví	dopravit	k5eAaPmIp3nS
miniponorka	miniponorka	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c4
průzkum	průzkum	k1gInSc4
vraku	vrak	k1gInSc2
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
slibu	slib	k1gInSc2
společnosti	společnost	k1gFnSc2
si	se	k3xPyFc3
zájemci	zájemce	k1gMnPc1
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
prohlédnout	prohlédnout	k5eAaPmF
přední	přední	k2eAgFnPc4d1
velké	velká	k1gFnPc4
schodiště	schodiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
na	na	k7c4
osobu	osoba	k1gFnSc4
je	být	k5eAaImIp3nS
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
105	#num#	k4
129	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
<g/>
,	,	kIx,
po	po	k7c6
započtení	započtení	k1gNnSc6
inflace	inflace	k1gFnSc2
<g/>
,	,	kIx,
ceně	cena	k1gFnSc3
původního	původní	k2eAgInSc2d1
lístku	lístek	k1gInSc2
za	za	k7c4
4	#num#	k4
350	#num#	k4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
za	za	k7c4
luxusní	luxusní	k2eAgFnSc4d1
kajutu	kajuta	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
dvojnásobek	dvojnásobek	k1gInSc4
sumy	suma	k1gFnSc2
účtované	účtovaný	k2eAgFnSc2d1
firmou	firma	k1gFnSc7
Deep	Deep	k1gMnSc1
Ocean	Ocean	k1gMnSc1
Expeditions	Expeditions	k1gInSc1
<g/>
;	;	kIx,
ta	ten	k3xDgFnSc1
roku	rok	k1gInSc2
2012	#num#	k4
realizovala	realizovat	k5eAaBmAgFnS
poslední	poslední	k2eAgFnSc4d1
turistickou	turistický	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
k	k	k7c3
vraku	vrak	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Údaje	údaj	k1gInSc2
se	se	k3xPyFc4
ve	v	k7c6
zdrojích	zdroj	k1gInPc6
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Některé	některý	k3yIgFnPc1
kajuty	kajuta	k1gFnPc1
1	#num#	k4
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnPc1
byly	být	k5eAaImAgFnP
variabilní	variabilní	k2eAgFnSc4d1
s	s	k7c7
možností	možnost	k1gFnSc7
změny	změna	k1gFnSc2
třídy	třída	k1gFnSc2
a	a	k8xC
počtu	počet	k1gInSc2
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
RMS	RMS	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
z	z	k7c2
angl.	angl.	k?
Royal	Royal	k1gInSc1
Mail	mail	k1gInSc1
Ship	Ship	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k9
Poštovní	poštovní	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Jejího	její	k3xOp3gInSc2
(	(	kIx(
<g/>
královského	královský	k2eAgMnSc2d1
<g/>
)	)	kIx)
Veličenstva	veličenstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Titanic	Titanic	k1gInSc1
pak	pak	k6eAd1
znamená	znamenat	k5eAaImIp3nS
Titánská	titánský	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jeden	jeden	k4xCgInSc1
z	z	k7c2
vlivů	vliv	k1gInPc2
na	na	k7c4
rozsah	rozsah	k1gInSc4
katastrofy	katastrofa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Všechny	všechen	k3xTgInPc4
čluny	člun	k1gInPc4
plně	plně	k6eAd1
obsazeny	obsadit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Událost	událost	k1gFnSc1
mající	mající	k2eAgFnSc1d1
vliv	vliv	k1gInSc4
na	na	k7c4
kolizi	kolize	k1gFnSc4
s	s	k7c7
ledovcem	ledovec	k1gInSc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Na	na	k7c6
lodích	loď	k1gFnPc6
nebyly	být	k5eNaImAgInP
kostely	kostel	k1gInPc1
<g/>
,	,	kIx,
kaple	kaple	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zpráva	zpráva	k1gFnSc1
obsahovala	obsahovat	k5eAaImAgFnS
informace	informace	k1gFnPc4
o	o	k7c6
kolizním	kolizní	k2eAgInSc6d1
kurzu	kurz	k1gInSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zde	zde	k6eAd1
se	se	k3xPyFc4
respektuje	respektovat	k5eAaImIp3nS
anglické	anglický	k2eAgNnSc1d1
rozlišení	rozlišení	k1gNnSc1
iron	iron	k1gInSc1
–	–	k?
steel	steel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
české	český	k2eAgFnSc2d1
terminologie	terminologie	k1gFnSc2
šlo	jít	k5eAaImAgNnS
o	o	k7c4
svářkovou	svářkový	k2eAgFnSc4d1
ocel	ocel	k1gFnSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
Veškeré	veškerý	k3xTgInPc1
technickými	technický	k2eAgInPc7d1
pochody	pochod	k1gInPc7
zkujněné	zkujněná	k1gFnSc2
technické	technický	k2eAgNnSc4d1
železo	železo	k1gNnSc4
nazýváme	nazývat	k5eAaImIp1nP
podle	podle	k7c2
nyní	nyní	k6eAd1
platných	platný	k2eAgFnPc2d1
norem	norma	k1gFnPc2
ocel	ocel	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
TEYSSLER	TEYSSLER	kA
<g/>
;	;	kIx,
KOTYŠKA	KOTYŠKA	kA
<g/>
:	:	kIx,
Technický	technický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Borský	Borský	k1gMnSc1
a	a	k8xC
Šulc	Šulc	k1gMnSc1
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Železo	železo	k1gNnSc1
<g/>
,	,	kIx,
odst	odst	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ž.	Ž.	kA
technické	technický	k2eAgInPc4d1
<g/>
,	,	kIx,
s.	s.	k?
1115	#num#	k4
<g/>
↑	↑	k?
Telegraf	telegraf	k1gInSc1
–	–	k?
lodní	lodní	k2eAgNnSc1d1
dorozumívací	dorozumívací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
mezi	mezi	k7c7
můstkem	můstek	k1gInSc7
a	a	k8xC
strojovnou	strojovna	k1gFnSc7
<g/>
↑	↑	k?
sš	sš	k?
<g/>
:	:	kIx,
severní	severní	k2eAgFnSc1d1
šířka	šířka	k1gFnSc1
(	(	kIx(
<g/>
N	N	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zd	zd	k?
<g/>
:	:	kIx,
západní	západní	k2eAgFnSc1d1
délka	délka	k1gFnSc1
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Citát	citát	k1gInSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
lodnímu	lodní	k2eAgNnSc3d1
neštěstí	neštěstí	k1gNnSc3
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Titanic	Titanic	k1gInSc1
ztroskotal	ztroskotat	k5eAaPmAgInS
<g/>
,	,	kIx,
následovaly	následovat	k5eAaImAgFnP
další	další	k2eAgNnSc4d1
neštěstí	neštěstí	k1gNnSc4
i	i	k9
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
počtem	počet	k1gInSc7
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
lodi	loď	k1gFnPc1
Wilhelm	Wilhelm	k1gMnSc1
Gustloff	Gustloff	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Description	Description	k1gInSc1
of	of	k?
Ship	Ship	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WILSON	WILSON	kA
<g/>
,	,	kIx,
Timothy	Timoth	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flags	Flags	k1gInSc1
at	at	k?
Sea	Sea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Her	hra	k1gFnPc2
Majesty	Majest	k1gInPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Stationery	Stationer	k1gMnPc7
Office	Office	kA
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
290389	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Flags	Flagsa	k1gFnPc2
of	of	k?
British	British	k1gInSc1
Ships	Shipsa	k1gFnPc2
other	other	k1gMnSc1
than	than	k1gMnSc1
the	the	k?
Royal	Royal	k1gMnSc1
Navy	Navy	k?
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BEVERIDGE	BEVERIDGE	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
<g/>
;	;	kIx,
ANDREWS	ANDREWS	kA
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
<g/>
;	;	kIx,
HALL	HALL	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
;	;	kIx,
KLISTORNER	KLISTORNER	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Ship	Ship	k1gMnSc1
Magnificent	Magnificent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Art	Art	k1gFnSc2
Braunschweiger	Braunschweigra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gloucestershire	Gloucestershir	k1gInSc5
<g/>
,	,	kIx,
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
<g/>
:	:	kIx,
History	Histor	k1gInPc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
752446061	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780752446066	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inception	Inception	k1gInSc1
&	&	k?
Construction	Construction	k1gInSc1
Plans	Plans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
24	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
časové	časový	k2eAgInPc4d1
údaje	údaj	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Titanic	Titanic	k1gInSc1
Myths	Myths	k1gInSc4
<g/>
:	:	kIx,
No	no	k9
Pope	pop	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
titanic-titanic	titanic-titanice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PLÁČEK	PLÁČEK	kA
<g/>
,	,	kIx,
Štěpán	Štěpán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unikátní	unikátní	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
Titanic	Titanic	k1gInSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
KINKOROVÁ	KINKOROVÁ	kA
<g/>
,	,	kIx,
Zoša	Zoša	k1gFnSc1
<g/>
;	;	kIx,
KUBINA	KUBINA	kA
<g/>
,	,	kIx,
Zdeno	Zdena	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cinemax	Cinemax	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85933	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Poloha	poloha	k1gFnSc1
vraku	vrak	k1gInSc2
Titanicu	Titanicus	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
titanic-titanic	titanic-titanice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
paluby	paluba	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Steam	Steam	k1gInSc1
Ship	Ship	k1gMnSc1
TITANIC	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Owner	Owner	k1gInSc1
<g/>
,	,	kIx,
Managing	Managing	k1gInSc1
Officer	Officer	k1gInSc1
<g/>
,	,	kIx,
or	or	k?
Agent	agent	k1gMnSc1
<g/>
,	,	kIx,
THE	THE	kA
OCEANIC	OCEANIC	kA
STEAM	STEAM	kA
NAVIGATION	NAVIGATION	kA
COMPANY	COMPANY	kA
<g/>
,	,	kIx,
LIMITED	limited	k2eAgFnSc1d1
<g/>
,	,	kIx,
LIVERPOOL	Liverpool	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
CHIRNSIDE	CHIRNSIDE	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
mystery	myster	k1gInPc1
of	of	k?
Titanic	Titanic	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
central	centrat	k5eAaImAgMnS,k5eAaPmAgMnS
propeller	propeller	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnPc1
Titanica	Titanica	k1gMnSc1
<g/>
,	,	kIx,
2008-05-05	2008-05-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MOSS	MOSS	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
S.	S.	kA
Oxford	Oxford	k1gInSc1
Dictionary	Dictionar	k1gInPc1
of	of	k?
National	National	k1gMnSc4
Biography	Biographa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
England	England	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
William	William	k1gInSc1
James	James	k1gMnSc1
Pirrie	Pirrie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
BULLOCK	BULLOCK	kA
<g/>
,	,	kIx,
Shan	Shan	k1gMnSc1
F.	F.	kA
Thomas	Thomas	k1gMnSc1
Andrews	Andrews	k1gInSc1
<g/>
,	,	kIx,
Shipbuilder	Shipbuilder	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dublin	Dublin	k1gInSc1
<g/>
:	:	kIx,
Maunsel	Maunsel	k1gInSc1
&	&	k?
Co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JENKINS	JENKINS	kA
<g/>
,	,	kIx,
Stanley	Stanley	k1gInPc7
C.	C.	kA
Alexander	Alexandra	k1gFnPc2
Carlisle	Carlisle	k1gFnPc2
Obituary	Obituara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1926	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Testimony	Testimona	k1gFnSc2
of	of	k?
Alexander	Alexandra	k1gFnPc2
Carlisle	Carlisle	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
pohon	pohon	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
stavba	stavba	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
In	In	k1gMnPc2
Weak	Weak	k1gInSc4
Rivets	Rivetsa	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
Possible	Possible	k1gMnSc3
Key	Key	k1gMnSc3
to	ten	k3xDgNnSc1
Titanic	Titanic	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Doom	Doo	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
A	a	k8xC
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
121	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BRAUNSCHWEIGER	BRAUNSCHWEIGER	kA
<g/>
,	,	kIx,
Art	Art	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
steering	steering	k1gInSc1
engines	engines	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Research	Research	k1gInSc4
&	&	k?
Modeling	Modeling	k1gInSc1
Association	Association	k1gInSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Titanic	Titanic	k1gInSc1
launch	launcha	k1gFnPc2
centenary	centenara	k1gFnSc2
to	ten	k3xDgNnSc1
be	be	k?
marked	marked	k1gInSc1
in	in	k?
Belfast	Belfast	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WELS	WELS	kA
<g/>
,	,	kIx,
Susan	Susan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Legacy	Legaca	k1gFnPc1
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Greatest	Greatest	k1gMnSc1
Ocean	Ocean	k1gMnSc1
Liner	Liner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Del	Del	k1gFnSc1
Mar	Mar	k1gFnSc2
<g/>
,	,	kIx,
California	Californium	k1gNnSc2
<g/>
:	:	kIx,
Tehabi	Tehabi	k1gNnSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
783552610	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
34	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
<g/>
rd	rd	k?
Class	Class	k1gInSc1
General	General	k1gMnSc1
Room	Room	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Museums	Museumsa	k1gFnPc2
Northern	Northerna	k1gFnPc2
Ireland	Ireland	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
1	#num#	k4
<g/>
st	st	kA
Class	Classa	k1gFnPc2
Cafe	Cafe	k1gFnSc1
Parisien	Parisien	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Museums	Museumsa	k1gFnPc2
Northern	Northerna	k1gFnPc2
Ireland	Ireland	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
cestující	cestující	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
STEPHENSON	STEPHENSON	kA
<g/>
,	,	kIx,
Parks	Parks	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Marconi	Marcoň	k1gFnSc6
Wireless	Wirelessa	k1gFnPc2
Installation	Installation	k1gInSc1
in	in	k?
R.	R.	kA
<g/>
M.	M.	kA
<g/>
S.	S.	kA
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Old	Olda	k1gFnPc2
Timer	Timra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Bulletin	bulletin	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Antique	Antiqu	k1gFnSc2
Wireless	Wirelessa	k1gFnPc2
Association	Association	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2001	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ADAMS	ADAMS	kA
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
DK	DK	kA
Eyewitness	Eyewitness	k1gInSc1
Books	Books	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hardcover	Hardcover	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
72	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
756607329	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
57	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
konstrukce	konstrukce	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EATON	EATON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
;	;	kIx,
HAAS	HAAS	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falling	Falling	k1gInSc1
Star	Star	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Misadventures	Misadventures	k1gInSc1
of	of	k?
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
Ships	Ships	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BUTLER	BUTLER	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Allen	Allen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unsinkable	Unsinkable	k1gFnSc1
<g/>
:	:	kIx,
the	the	k?
full	full	k1gInSc1
story	story	k1gFnSc1
of	of	k?
the	the	k?
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechanicsburg	Mechanicsburg	k1gInSc1
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
:	:	kIx,
Stackpole	Stackpole	k1gFnSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8117	#num#	k4
<g/>
-	-	kIx~
<g/>
1814	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
člunová	člunový	k2eAgFnSc1d1
paluba	paluba	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Titanic	Titanic	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
life	life	k1gNnSc7
saving	saving	k1gInSc1
appliances	appliances	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.titanic-cad-plans.com/wp-content/uploads/2010/06/Davit-Failure-Article1.pdf	http://www.titanic-cad-plans.com/wp-content/uploads/2010/06/Davit-Failure-Article1.pdf	k1gMnSc1
<g/>
↑	↑	k?
LORD	lord	k1gMnSc1
<g/>
,	,	kIx,
Warter	Warter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Night	Night	k1gMnSc1
to	ten	k3xDgNnSc4
Remember	Remember	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Henry	henry	k1gInSc1
Holt	Holt	k?
and	and	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
553	#num#	k4
<g/>
-	-	kIx~
<g/>
27827	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
78	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://timesmachine.nytimes.com/timesmachine/1912/06/01/100536472.pdf	https://timesmachine.nytimes.com/timesmachine/1912/06/01/100536472.pdf	k1gInSc1
<g/>
↑	↑	k?
https://books.google.cz/books?id=o35hXKZrOc8C&	https://books.google.cz/books?id=o35hXKZrOc8C&	k?
2	#num#	k4
Detailed	Detailed	k1gInSc1
Description	Description	k1gInSc4
Machinery	Machiner	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Detailed	Detailed	k1gInSc1
Description	Description	k1gInSc1
Pumping	Pumping	k1gInSc1
Arrangements	Arrangements	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BROWN	BROWN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
G.	G.	kA
The	The	k1gMnSc1
Last	Last	k1gMnSc1
Log	log	kA
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
McGraw-Hill	McGraw-Hill	k1gMnSc1
Professional	Professional	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
71364471	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
109	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BEVERIDGE	BEVERIDGE	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
<g/>
;	;	kIx,
HALL	HALL	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympic	Olympic	k1gMnSc1
&	&	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Truth	Truth	k1gMnSc1
Behind	Behind	k1gMnSc1
the	the	k?
Conspiracy	Conspiraca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haverford	Haverford	k1gInSc1
<g/>
,	,	kIx,
Pennsylvania	Pennsylvanium	k1gNnPc1
<g/>
:	:	kIx,
Infinity	Infinita	k1gFnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
741419491	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780741419491	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
spuštění	spuštění	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
SPIGNESI	SPIGNESI	kA
<g/>
,	,	kIx,
Stephen	Stephen	k1gInSc4
J.	J.	kA
The	The	k1gFnSc7
Complete	Comple	k1gNnSc2
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
From	From	k1gInSc1
the	the	k?
Ship	Ship	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earliest	Earliest	k1gInSc1
Blueprints	Blueprints	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Epic	Epic	k1gInSc1
Film	film	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Secaucus	Secaucus	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
Jersey	Jersea	k1gFnSc2
<g/>
:	:	kIx,
Birch	Birch	k1gInSc1
Lane	Lane	k1gNnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
55972	#num#	k4
<g/>
-	-	kIx~
<g/>
483	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EATON	EATON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
HAAS	HAAS	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
A.	A.	kA
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Triumph	Triumph	k1gInSc1
and	and	k?
Tragedy	Trageda	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.	W.	kA
W.	W.	kA
Norton	Norton	k1gInSc1
&	&	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3697	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BARRATT	BARRATT	kA
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lost	Lost	k1gMnSc1
Voices	Voices	k1gMnSc1
From	From	k1gMnSc1
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Definitive	Definitiv	k1gInSc5
Oral	orat	k5eAaImAgMnS
History	Histor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84809	#num#	k4
<g/>
-	-	kIx~
<g/>
151	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
83	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
plavba	plavba	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
KINKOROVÁ	KINKOROVÁ	kA
<g/>
,	,	kIx,
Zoša	Zoša	k1gFnSc1
<g/>
;	;	kIx,
KUBINA	KUBINA	kA
<g/>
,	,	kIx,
Zdeno	Zdena	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cinemax	Cinemax	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85933	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
20	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Titanic	Titanic	k1gInSc1
Passenger	Passengra	k1gFnPc2
List	list	k1gInSc1
First	First	k1gMnSc1
Class	Classa	k1gFnPc2
Passengers	Passengers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
Titanica	Titanicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Titanic	Titanic	k1gInSc1
Crew	Crew	k1gFnSc2
Lists	Listsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eencyclopedia	Eencyclopedium	k1gNnSc2
Titanica	Titanicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Titanic	Titanic	k1gInSc1
Passenger	Passenger	k1gMnSc1
List	list	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
encyclopedia	encyclopedium	k1gNnSc2
titanica	titanic	k1gInSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TIP	tip	k1gInSc1
–	–	k?
British	British	k1gInSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
Report	report	k1gInSc4
–	–	k?
Detailed	Detailed	k1gInSc1
Description	Description	k1gInSc1
–	–	k?
Crew	Crew	k1gFnSc2
and	and	k?
Passengers	Passengers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
Project	Projecta	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Chernow	Chernow	k1gFnSc2
2001	#num#	k4
Kapitola	kapitola	k1gFnSc1
<g/>
:	:	kIx,
81	#num#	k4
2	#num#	k4
3	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
středa	středa	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
čtvrtek	čtvrtek	k1gInSc1
11	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
pátek	pátek	k1gInSc1
12	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
neděle	neděle	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
sobota	sobota	k1gFnSc1
13	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
KINKOROVÁ	KINKOROVÁ	kA
<g/>
,	,	kIx,
Zoša	Zoša	k1gFnSc1
<g/>
;	;	kIx,
KUBINA	KUBINA	kA
<g/>
,	,	kIx,
Zdeno	Zdena	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cinemax	Cinemax	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85933	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ryan	Ryan	k1gNnSc1
1985	#num#	k4
<g/>
↑	↑	k?
MCCLUSKIE	MCCLUSKIE	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
;	;	kIx,
SHARPE	sharp	k1gInSc5
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
<g/>
;	;	kIx,
LEO	Leo	k1gMnSc1
<g/>
,	,	kIx,
Marriott	Marriott	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
&	&	k?
Her	hra	k1gFnPc2
Sisters	Sisters	k1gInSc1
Olympic	Olympic	k1gMnSc1
&	&	k?
Britannic	Britannice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mishawaka	Mishawak	k1gMnSc2
USA	USA	kA
<g/>
:	:	kIx,
Revival	revival	k1gInSc1
Books	Books	k1gInSc1
Ltd	ltd	kA
ISBN	ISBN	kA
1902616103	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
490	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Butler	Butler	k1gInSc4
1998	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
61	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
↑	↑	k?
BALLARD	BALLARD	kA
<g/>
,	,	kIx,
R.	R.	kA
D.	D.	kA
<g/>
;	;	kIx,
ARCHBOLD	ARCHBOLD	kA
<g/>
,	,	kIx,
R.	R.	kA
The	The	k1gFnSc1
Discovery	Discovera	k1gFnSc2
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
230	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
446	#num#	k4
<g/>
-	-	kIx~
<g/>
51385	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ryan	Ryan	k1gNnSc1
1985	#num#	k4
<g/>
↑	↑	k?
The	The	k1gFnSc1
Collision	Collision	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
ledovec	ledovec	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barratt	Barratt	k1gInSc4
2010	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
132	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
čluny	člun	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Boat	Boata	k1gFnPc2
Reports	Reportsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lord	lord	k1gMnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1997	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
88	#num#	k4
<g/>
↑	↑	k?
Testimony	Testimona	k1gFnSc2
of	of	k?
John	John	k1gMnSc1
E.	E.	kA
Hart	Hart	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
GARZKE	GARZKE	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Anatomy	anatom	k1gMnPc4
of	of	k?
a	a	k8xC
Disaster	Disaster	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
The	The	k1gFnSc1
Society	societa	k1gFnSc2
of	of	k?
Naval	navalit	k5eAaPmRp2nS
Architects	Architects	k1gInSc1
and	and	k?
Marine	Marin	k1gInSc5
Engineers	Engineersa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
KINKOROVÁ	KINKOROVÁ	kA
<g/>
,	,	kIx,
Zoša	Zoša	k1gFnSc1
<g/>
;	;	kIx,
KUBINA	KUBINA	kA
<g/>
,	,	kIx,
Zdeno	Zdena	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cinemax	Cinemax	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85933	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
ledovec	ledovec	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BARCZEWSKI	BARCZEWSKI	kA
<g/>
,	,	kIx,
Stephanie	Stephanie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
a	a	k8xC
night	night	k2eAgInSc1d1
remembered	remembered	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Continuum	Continuum	k1gInSc1
International	International	k1gFnPc2
Publishing	Publishing	k1gInSc1
Group	Group	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1905636687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Distress	Distressa	k1gFnPc2
calls	callsa	k1gFnPc2
sent	sent	k2eAgInSc1d1
and	and	k?
received	received	k1gInSc1
<g/>
,	,	kIx,
timeline	timelin	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Greatships	Greatships	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pleas	Pleas	k1gInSc1
For	forum	k1gNnPc2
Help	help	k1gInSc1
–	–	k?
Distress	Distress	k1gInSc1
Calls	Calls	k1gInSc1
Heard	Heard	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Senate	Senat	k1gInSc5
Inquiry	Inquir	k1gInPc4
Report	report	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
PROCEEDINGS	PROCEEDINGS	kA
ON	on	k3xPp3gMnSc1
A	a	k9
FORMAL	FORMAL	kA
INVESTIGATION	INVESTIGATION	kA
ORDERED	ORDERED	kA
BY	by	k9
THE	THE	kA
BOARD	BOARD	kA
OF	OF	kA
TRADE	TRADE	kA
INTO	INTO	kA
THE	THE	kA
LOSS	LOSS	kA
OF	OF	kA
THE	THE	kA
S.	S.	kA
<g/>
S.	S.	kA
TITANIC	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Wreck	Wreck	k1gInSc1
Commissioners	Commissioners	k1gInSc1
<g/>
'	'	kIx"
Court	Court	k1gInSc1
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Wreck	Wrecka	k1gFnPc2
Commissioners	Commissionersa	k1gFnPc2
<g/>
'	'	kIx"
Court	Court	k1gInSc1
<g/>
:	:	kIx,
PROCEEDINGS	PROCEEDINGS	kA
ON	on	k3xPp3gMnSc1
A	a	k9
FORMAL	FORMAL	kA
INVESTIGATION	INVESTIGATION	kA
ORDERED	ORDERED	kA
BY	by	k9
THE	THE	kA
BOARD	BOARD	kA
OF	OF	kA
TRADE	TRADE	kA
INTO	INTO	kA
THE	THE	kA
LOSS	LOSS	kA
OF	OF	kA
THE	THE	kA
S.	S.	kA
<g/>
S.	S.	kA
“	“	k?
<g/>
TITANIC	Titanic	k1gInSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
London	London	k1gMnSc1
19121	#num#	k4
2	#num#	k4
3	#num#	k4
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
Carpathia	Carpathium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
Carpathia	Carpathius	k1gMnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Spitz	Spitz	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
J.	J.	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Investigation	Investigation	k1gInSc1
of	of	k?
Bodies	Bodies	k1gInSc1
in	in	k?
Water	Water	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Spitz	Spitz	k1gMnSc1
<g/>
,	,	kIx,
W.	W.	kA
U.	U.	kA
&	&	k?
Spitz	Spitz	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
J.	J.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Spitz	Spitz	k1gInSc1
and	and	k?
Fisher	Fishra	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Medicolegal	Medicolegal	k1gMnSc1
Investigation	Investigation	k1gInSc1
of	of	k?
Death	Death	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guideline	Guidelin	k1gInSc5
for	forum	k1gNnPc2
the	the	k?
Application	Application	k1gInSc1
of	of	k?
Pathology	Patholog	k1gMnPc4
to	ten	k3xDgNnSc1
Crime	Crim	k1gInSc5
Investigations	Investigations	k1gInSc1
(	(	kIx(
<g/>
Fourth	Fourth	k1gInSc1
edition	edition	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
C.	C.	kA
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
<g/>
:	:	kIx,
846	#num#	k4
<g/>
–	–	k?
<g/>
881	#num#	k4
<g/>
;	;	kIx,
Springfield	Springfield	k1gInSc1
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PIANTADOSI	PIANTADOSI	kA
<g/>
,	,	kIx,
Claude	Claud	k1gInSc5
A.	A.	kA
The	The	k1gMnSc1
biology	biolog	k1gMnPc4
of	of	k?
human	human	k1gMnSc1
survival	survivat	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
:	:	kIx,
life	lifat	k5eAaPmIp3nS
and	and	k?
death	death	k1gInSc1
in	in	k?
extreme	extrit	k5eAaImRp1nP,k5eAaPmRp1nP
environments	environments	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
195165012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zemřela	zemřít	k5eAaPmAgFnS
poslední	poslední	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
přežila	přežít	k5eAaPmAgFnS
potopení	potopení	k1gNnSc4
Titanicu	Titanicum	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Account	Account	k1gInSc1
of	of	k?
the	the	k?
Saving	Saving	k1gInSc1
and	and	k?
Rescue	Rescue	k1gInSc1
of	of	k?
those	those	k6eAd1
who	who	k?
Survived	Survived	k1gInSc1
Numbers	Numbers	k1gInSc1
Saved	Saved	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gInPc7
<g/>
,	,	kIx,
Appendix	Appendix	k1gInSc1
–	–	k?
10	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gInPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
Senate	Senat	k1gInSc5
Inquiry	Inquir	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Titanic	Titanic	k1gInSc1
–	–	k?
The	The	k1gFnSc2
Senatorial	Senatorial	k1gMnSc1
Investigation	Investigation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Senate	Senat	k1gInSc5
Inquiry	Inquira	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Butler	Butler	k1gInSc4
1998	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
180	#num#	k4
<g/>
–	–	k?
<g/>
186	#num#	k4
<g/>
↑	↑	k?
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gInPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
Project	Projecta	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Circumstances	Circumstances	k1gMnSc1
in	in	k?
Connection	Connection	k1gInSc1
with	with	k1gInSc1
the	the	k?
SS	SS	kA
Californian	Californian	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Inquiry	Inquira	k1gFnSc2
Project	Projecta	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
KINKOROVÁ	KINKOROVÁ	kA
<g/>
,	,	kIx,
Zoša	Zoša	k1gFnSc1
<g/>
;	;	kIx,
KUBINA	KUBINA	kA
<g/>
,	,	kIx,
Zdeno	Zdena	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cinemax	Cinemax	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85933	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
66	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Board	Board	k1gInSc1
of	of	k?
Trade	Trad	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Administration	Administration	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Butler	Butler	k1gInSc4
1998	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
104	#num#	k4
<g/>
–	–	k?
<g/>
105	#num#	k4
<g/>
,	,	kIx,
1221	#num#	k4
2	#num#	k4
VICKI	VICKI	kA
<g/>
,	,	kIx,
Bassett	Bassett	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Causes	Causes	k1gInSc1
and	and	k?
Effects	Effects	k1gInSc1
of	of	k?
the	the	k?
Rapid	rapid	k1gInSc1
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ke	k	k7c3
zkáze	zkáza	k1gFnSc3
Titaniku	Titanic	k1gInSc2
prý	prý	k9
přispělo	přispět	k5eAaPmAgNnS
nezvyklé	zvyklý	k2eNgNnSc1d1
postavení	postavení	k1gNnSc1
Měsíce	měsíc	k1gInSc2
Týden	týden	k1gInSc1
12	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
↑	↑	k?
Did	Did	k1gMnSc1
the	the	k?
moon	moon	k1gMnSc1
sink	sink	k1gMnSc1
the	the	k?
The	The	k1gMnSc1
iceberg	iceberg	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
accomplice	accomplice	k1gFnSc1
<g/>
:	:	kIx,
Did	Did	k1gMnSc1
the	the	k?
moon	moon	k1gMnSc1
sink	sink	k1gMnSc1
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
/	/	kIx~
<g/>
Texas	Texas	k1gInSc1
University	universita	k1gFnSc2
News	Newsa	k1gFnPc2
Service	Service	k1gFnSc2
<g/>
/	/	kIx~
<g/>
March	March	k1gInSc1
5	#num#	k4
<g/>
,	,	kIx,
20121	#num#	k4
2	#num#	k4
3	#num#	k4
MCCARTY	MCCARTY	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gMnSc1
Hooper	Hooper	k1gMnSc1
<g/>
,	,	kIx,
Foecke	Foecke	k1gFnSc1
<g/>
,	,	kIx,
Tim	Tim	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k1gInSc1
Really	Realla	k1gFnSc2
Sank	Sank	k1gMnSc1
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
New	New	k1gMnSc1
Forensic	Forensic	k1gMnSc1
Discoveries	Discoveries	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Citadel	citadela	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
806528958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FELKINS	FELKINS	kA
<g/>
,	,	kIx,
Katherine	Katherin	k1gInSc5
<g/>
,	,	kIx,
H.	H.	kA
P.	P.	kA
Leighly	Leighly	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
;	;	kIx,
A.	A.	kA
Jankovic	Jankovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Royal	Royal	k1gInSc4
Mail	mail	k1gInSc1
Ship	Ship	k1gInSc1
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Did	Did	k1gFnSc1
a	a	k8xC
Metallurgical	Metallurgical	k1gFnSc1
Failure	Failur	k1gMnSc5
Cause	causa	k1gFnSc6
a	a	k8xC
Night	Night	k1gMnSc1
to	ten	k3xDgNnSc4
Remember	Remember	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
JOM	JOM	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warrendale	Warrendala	k1gFnSc3
PA	Pa	kA
<g/>
:	:	kIx,
The	The	k1gFnSc1
Minerals	Minerals	k1gInSc1
<g/>
,	,	kIx,
Metals	Metals	k1gInSc1
&	&	k?
Materials	Materials	k1gInSc1
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
January	Januara	k1gFnSc2
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
50	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
11837	#num#	k4
<g/>
-	-	kIx~
<g/>
998	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
62	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MCCARTY	MCCARTY	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gMnSc1
Hooper	Hooper	k1gMnSc1
<g/>
,	,	kIx,
Foecke	Foecke	k1gFnSc1
<g/>
,	,	kIx,
Tim	Tim	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k1gInSc1
Really	Realla	k1gFnSc2
Sank	Sank	k1gMnSc1
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
New	New	k1gMnSc1
Forensic	Forensic	k1gMnSc1
Discoveries	Discoveries	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Citadel	citadela	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
806528958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
136,137	136,137	k4
<g/>
.	.	kIx.
↑	↑	k?
McCarty	McCarta	k1gFnSc2
<g/>
;	;	kIx,
Foecke	Foeck	k1gFnSc2
2008	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
148	#num#	k4
a	a	k8xC
další	další	k2eAgInSc4d1
<g/>
1	#num#	k4
2	#num#	k4
TITANIC	Titanic	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2011-07-30	2011-07-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ADAMS	ADAMS	kA
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Cassel	Cassel	k1gMnSc1
and	and	k?
Company	Compana	k1gFnSc2
Ltd	ltd	kA
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
316100301	#num#	k4
S.	S.	kA
114	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
In	In	k1gMnSc1
Weak	Weak	k1gMnSc1
Rivets	Rivets	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
Possible	Possible	k1gMnSc3
Key	Key	k1gMnSc3
to	ten	k3xDgNnSc1
Titanic	Titanic	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Doom	Doo	k1gNnSc7
<g/>
,	,	kIx,
The	The	k1gFnSc7
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
15	#num#	k4
April	April	k1gInSc1
2008	#num#	k4
<g/>
:	:	kIx,
Figure	Figur	k1gMnSc5
3	#num#	k4
<g/>
,	,	kIx,
“	“	k?
<g/>
Not	nota	k1gFnPc2
So	So	kA
Unsinkable	Unsinkable	k1gMnSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
clearly	clearla	k1gFnPc1
shows	shows	k1gInSc1
the	the	k?
steel	steel	k1gInSc1
<g/>
/	/	kIx~
<g/>
iron	iron	k1gInSc1
rivet	rivet	k1gMnSc1
boundaries	boundaries	k1gMnSc1
and	and	k?
the	the	k?
tears	tears	k1gInSc1
in	in	k?
the	the	k?
hull	hull	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Brown	Brown	k1gInSc4
2000	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
77	#num#	k4
<g/>
↑	↑	k?
GIBSON	GIBSON	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Henry	Henry	k1gMnSc1
<g/>
,	,	kIx,
Prendergast	Prendergast	k1gMnSc1
<g/>
,	,	kIx,
Maurice	Maurika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
German	German	k1gMnSc1
submarine	submarin	k1gInSc5
war	war	k?
<g/>
,	,	kIx,
1914	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Constable	Constable	k1gMnSc1
<g/>
,	,	kIx,
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781591143147	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
304	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Maltin	Maltin	k2eAgInSc4d1
Aston	Aston	k1gInSc4
2010	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
87	#num#	k4
<g/>
↑	↑	k?
Testimony	Testimona	k1gFnSc2
of	of	k?
Joseph	Joseph	k1gMnSc1
G.	G.	kA
Boxhall	Boxhall	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BEVERIDGE	BEVERIDGE	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
–	–	k?
The	The	k1gFnSc2
Ship	Ship	k1gMnSc1
Magnificent	Magnificent	k1gMnSc1
(	(	kIx(
<g/>
Volume	volum	k1gInSc5
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stroud	Stroud	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc2
History	Histor	k1gInPc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780752446066	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
334	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BARCZEWSKI	BARCZEWSKI	kA
<g/>
,	,	kIx,
Stephanie	Stephanie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Night	Night	k2eAgInSc4d1
Remember	Remember	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Hambledon	Hambledon	k1gInSc1
Continuum	Continuum	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1852855002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
194	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Testimony	Testimona	k1gFnSc2
of	of	k?
Frederick	Frederick	k1gMnSc1
Scott	Scott	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
CAWLEY	CAWLEY	kA
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
GRIFFITHS	GRIFFITHS	kA
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Power	Power	k1gMnSc1
of	of	k?
the	the	k?
Great	Great	k2eAgInSc4d1
Liners	Liners	k1gInSc4
<g/>
:	:	kIx,
History	Histor	k1gInPc1
of	of	k?
Atlantic	Atlantice	k1gFnPc2
Marine	Marin	k1gInSc5
Engineering	Engineering	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Patrick	Patrick	k1gInSc1
Stephens	Stephens	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85260	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Wreck	Wreck	k1gInSc1
Commissioners	Commissioners	k1gInSc1
<g/>
'	'	kIx"
Court	Court	k1gInSc1
<g/>
↑	↑	k?
UNITED	UNITED	kA
STATES	STATES	kA
SENATE	SENATE	kA
<g/>
:	:	kIx,
TITANIC	Titanic	k1gInSc1
DISASTER	DISASTER	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
NewYork	NewYork	k1gInSc1
<g/>
:	:	kIx,
SUBCOMMITTEE	SUBCOMMITTEE	kA
OF	OF	kA
THE	THE	kA
COMMITTEE	COMMITTEE	kA
ON	on	k3xPp3gMnSc1
COMMERCE	COMMERCE	kA
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HALPERN	HALPERN	kA
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
She	She	k1gMnSc1
Turned	Turned	k1gMnSc1
Two	Two	k1gFnPc2
Points	Points	k1gInSc4
In	In	k1gFnSc4
37	#num#	k4
Seconds	Secondsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Research	Research	k1gInSc4
and	and	k?
Modeling	Modeling	k1gInSc1
Association	Association	k1gInSc1
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Garzke	Garzke	k1gFnPc2
W.	W.	kA
1997	#num#	k4
<g/>
↑	↑	k?
BEESLEY	BEESLEY	kA
<g/>
,	,	kIx,
Lawrence	Lawrence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Loss	Lossa	k1gFnPc2
of	of	k?
the	the	k?
S.	S.	kA
<g/>
S.	S.	kA
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Heinemann	Heinemann	k1gMnSc1
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
56	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Howells	Howells	k1gInSc4
1999	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
31	#num#	k4
<g/>
↑	↑	k?
Titanic	Titanic	k1gInSc1
quotes	quotes	k1gMnSc1
from	from	k1gMnSc1
IMDB	IMDB	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CAMPBELL	CAMPBELL	kA
<g/>
,	,	kIx,
Ballard	Ballard	k1gMnSc1
C.	C.	kA
Disaster	Disaster	k1gMnSc1
<g/>
,	,	kIx,
Accidents	Accidents	k1gInSc1
and	and	k?
Crises	Crises	k1gInSc1
in	in	k?
American	American	k1gInSc1
History	Histor	k1gInPc1
<g/>
:	:	kIx,
A	a	k8xC
Reference	reference	k1gFnPc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Nation	Nation	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Most	most	k1gInSc1
Catastrophic	Catastrophic	k1gMnSc1
Events	Events	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Infobase	Infobas	k1gInSc6
Publishing	Publishing	k1gInSc4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Facts	Facts	k1gInSc1
on	on	k3xPp3gInSc1
File	File	k1gFnSc4
Library	Librara	k1gFnSc2
of	of	k?
American	American	k1gInSc1
History	Histor	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780816066032	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1912	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EATON	EATON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
HAAS	HAAS	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
A.	A.	kA
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Destination	Destination	k1gInSc1
Disaster	Disaster	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
Legends	Legends	k1gInSc1
and	and	k?
the	the	k?
Reality	realita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.	W.	kA
W.	W.	kA
Norton	Norton	k1gInSc1
&	&	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3697	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Eaton	Eaton	k1gInSc1
<g/>
;	;	kIx,
Haas	Haas	k1gInSc1
1996	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
92	#num#	k4
<g/>
↑	↑	k?
CASSIDY	CASSIDY	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
J.	J.	kA
Handbook	handbook	k1gInSc1
of	of	k?
Transportation	Transportation	k1gInSc1
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amsterdam	Amsterdam	k1gInSc1
Netherlands	Netherlands	k1gInSc4
<g/>
:	:	kIx,
Wolters	Wolters	k1gInSc4
Kluwer	Kluwra	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1402072465	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
68	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Testimony	Testimona	k1gFnSc2
of	of	k?
Edward	Edward	k1gMnSc1
Wilding	Wilding	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1912	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
See	See	k1gMnSc1
in	in	k?
particular	particular	k1gMnSc1
20269	#num#	k4
ff	ff	kA
<g/>
.	.	kIx.
of	of	k?
Wilding	Wilding	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
testimony	testimon	k1gInPc7
<g/>
,	,	kIx,
cited	cited	k1gMnSc1
above	aboev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilding	Wilding	k1gInSc1
was	was	k?
of	of	k?
the	the	k?
view	view	k?
that	thata	k1gFnPc2
the	the	k?
momentum	momentum	k1gNnSc4
of	of	k?
the	the	k?
ship	ship	k1gInSc1
would	would	k1gMnSc1
have	have	k1gFnPc2
crushed	crushed	k1gMnSc1
in	in	k?
the	the	k?
bows	bowsa	k1gFnPc2
for	forum	k1gNnPc2
80	#num#	k4
or	or	k?
perhaps	perhapsa	k1gFnPc2
100	#num#	k4
feet	feeta	k1gFnPc2
<g/>
,	,	kIx,
thus	thus	k6eAd1
slowing	slowing	k1gInSc1
the	the	k?
ship	ship	k1gInSc1
gradually	gradualla	k1gMnSc2
<g/>
,	,	kIx,
about	about	k1gInSc1
the	the	k?
same	samat	k5eAaPmIp3nS
as	as	k9
a	a	k8xC
car	car	k1gMnSc1
braking	braking	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
This	This	k1gInSc1
part	part	k1gInSc4
of	of	k?
his	his	k1gNnSc2
testimony	testimona	k1gFnSc2
is	is	k?
at	at	k?
TIP	tip	k1gInSc1
|	|	kIx~
British	British	k1gMnSc1
Wreck	Wreck	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Inquiry	Inquir	k1gInPc7
|	|	kIx~
Day	Day	k1gFnSc1
19	#num#	k4
|	|	kIx~
Testimony	Testimona	k1gFnSc2
of	of	k?
Edward	Edward	k1gMnSc1
Wilding	Wilding	k1gInSc1
<g/>
,	,	kIx,
cont	cont	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nové	Nové	k2eAgInPc4d1
důkazy	důkaz	k1gInPc4
<g/>
:	:	kIx,
Prvotní	prvotní	k2eAgFnSc7d1
a	a	k8xC
hlavní	hlavní	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
zkázy	zkáza	k1gFnSc2
Titaniku	Titanic	k1gInSc2
byl	být	k5eAaImAgInS
oheň	oheň	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Respekt	respekt	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A	a	k8xC
Coal	Coal	k1gMnSc1
Fire	Fir	k1gFnSc2
May	May	k1gMnSc1
Have	Have	k1gFnPc2
Helped	Helped	k1gMnSc1
Sink	Sink	k1gMnSc1
the	the	k?
'	'	kIx"
<g/>
Titanic	Titanic	k1gInSc1
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smithsonianmag	Smithsonianmag	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
KINKOROVÁ	KINKOROVÁ	kA
<g/>
,	,	kIx,
Zoša	Zoša	k1gFnSc1
<g/>
;	;	kIx,
KUBINA	KUBINA	kA
<g/>
,	,	kIx,
Zdeno	Zdena	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cinemax	Cinemax	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85933	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
67	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Dokumentární	dokumentární	k2eAgInSc4d1
film	film	k1gInSc4
z	z	k7c2
produkce	produkce	k1gFnSc2
National	National	k1gFnSc2
Geographic	Geographice	k1gFnPc2
Vrak	vrak	k1gInSc1
Titaniku	Titanic	k1gInSc2
a	a	k8xC
studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
Titanic	Titanic	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Nuclear	Nuclear	k1gMnSc1
Secret	Secret	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
51	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vrak	vrak	k1gInSc4
Titaniku	Titanic	k1gInSc2
požírají	požírat	k5eAaImIp3nP
bakterie	bakterie	k1gFnPc1
<g/>
,	,	kIx,
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
z	z	k7c2
něj	on	k3xPp3gNnSc2
prý	prý	k9
zbude	zbýt	k5eAaPmIp3nS
rezavá	rezavý	k2eAgFnSc1d1
skvrna	skvrna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALL	MALL	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MARÁŠEK	MARÁŠEK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TITANIC	Titanic	k1gInSc1
World	World	k1gInSc4
–	–	k?
Předtucha	předtucha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Of	Of	k1gFnSc1
Banks	Banks	k1gInSc1
and	and	k?
Icebergs	Icebergs	k1gInSc1
<g/>
↑	↑	k?
STAFF	STAFF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lead	Lead	k1gInSc1
Article	Article	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Engineer	Engineer	k1gMnSc1
(	(	kIx(
<g/>
magazine	magazinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
19	#num#	k4
April	April	k1gInSc1
1912	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
How	How	k1gMnSc1
the	the	k?
Mail	mail	k1gInSc1
Steamer	Steamer	k1gMnSc1
Went	Went	k1gMnSc1
Down	Downa	k1gFnPc2
in	in	k?
Mid-Atlantic	Mid-Atlantice	k1gFnPc2
<g/>
,	,	kIx,
by	by	kYmCp3nS
a	a	k8xC
Survivor	Survivor	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pall	palla	k1gFnPc2
Mall	Malla	k1gFnPc2
Gazette	Gazett	k1gInSc5
<g/>
,	,	kIx,
22	#num#	k4
March	March	k1gInSc1
1886	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
From	From	k1gInSc1
the	the	k?
Old	Olda	k1gFnPc2
World	World	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
New	New	k1gFnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Review	Review	k1gMnSc1
of	of	k?
Reviews	Reviews	k1gInSc1
<g/>
,	,	kIx,
Christmas	Christmas	k1gInSc1
edition	edition	k1gInSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
1892	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOWELLS	HOWELLS	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Parton	Parton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Myth	Myth	k1gMnSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Macmillan	Macmillan	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
213	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
333725972	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Campbell	Campbell	k1gInSc4
2008	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Did	Did	k1gFnSc1
faith	faith	k1gInSc1
drive	drive	k1gInSc1
Titanic	Titanic	k1gInSc1
musicians	musicians	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
United	United	k1gMnSc1
Methodist	Methodist	k1gMnSc1
Church	Church	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Eaton	Eaton	k1gInSc1
<g/>
;	;	kIx,
Haas	Haas	k1gInSc1
1996	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
95	#num#	k4
<g/>
↑	↑	k?
The	The	k1gFnSc1
Titanic	Titanic	k1gInSc1
and	and	k?
Olympic	Olympice	k1gInPc2
were	wer	k1gInSc2
“	“	k?
<g/>
switched	switched	k1gMnSc1
<g/>
”	”	k?
in	in	k?
an	an	k?
insurance-fraud	insurance-fraud	k6eAd1
scheme	schat	k5eAaImIp1nP,k5eAaBmIp1nP,k5eAaPmIp1nP
<g/>
↑	↑	k?
CAMERON	CAMERON	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Conspiracies	Conspiracies	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
Universe	Universe	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dive	div	k1gInSc5
the	the	k?
Titanic	Titanic	k1gInSc1
•	•	k?
Blue	Bluus	k1gMnSc5
Marble	Marble	k1gMnSc5
Private	Privat	k1gMnSc5
<g/>
.	.	kIx.
www.bluemarbleprivate.com	www.bluemarbleprivate.com	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CNN	CNN	kA
<g/>
;	;	kIx,
doh	doh	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výlety	výlet	k1gInPc7
ponorkou	ponorka	k1gFnSc7
k	k	k7c3
vraku	vrak	k1gInSc3
Titaniku	Titanic	k1gInSc2
začnou	začít	k5eAaPmIp3nP
příští	příští	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borgis	borgis	k1gInSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
2119	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ALDRIDGE	ALDRIDGE	kA
<g/>
,	,	kIx,
Rebecca	Rebeccum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Sinking	Sinking	k1gInSc4
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Infobase	Infobas	k1gInSc6
Publishing	Publishing	k1gInSc4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
112	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
791096432	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780791096437	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BARCZEWSKI	BARCZEWSKI	kA
<g/>
,	,	kIx,
Stephanie	Stephanie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Night	Night	k2eAgInSc4d1
Remembered	Remembered	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Continuum	Continuum	k1gNnSc1
International	International	k1gFnSc2
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
382	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1852855002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781852855000	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BARRATT	BARRATT	kA
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lost	Lost	k1gMnSc1
Voices	Voices	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Definitive	Definitiv	k1gInSc5
Oral	orat	k5eAaImAgMnS
History	Histor	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Palgrave	Palgrav	k1gInSc5
Macmillan	Macmillan	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
286	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
230622305	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780230622302	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BEESLEY	BEESLEY	kA
<g/>
,	,	kIx,
Lawrence	Lawrence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Loss	Lossa	k1gFnPc2
of	of	k?
the	the	k?
S.	S.	kA
<g/>
S.	S.	kA
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Its	Its	k1gFnSc1
Story	story	k1gFnSc2
and	and	k?
Its	Its	k1gFnSc2
Lessons	Lessonsa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Forgotten	Forgotten	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
.	.	kIx.
150	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1606800442	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781606800447	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BEHE	BEHE	kA
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Safety	Safeta	k1gFnPc1
<g/>
,	,	kIx,
Speed	Speed	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Sacrifice	Sacrifice	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Transportation	Transportation	k1gInSc1
Trails	Trailsa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
88	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
933449313	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780933449312	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BOYD-SMITH	BOYD-SMITH	k?
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
From	From	k1gInSc1
Rare	Rar	k1gFnSc2
Historical	Historical	k1gMnSc2
Reports	Reportsa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Brooks	Brooks	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
244	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BUTLER	BUTLER	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Allen	Allen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unsinkable	Unsinkable	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Full	Full	k1gInSc1
Story	story	k1gFnSc1
of	of	k?
the	the	k?
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Stackpole	Stackpole	k1gFnSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
292	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
81171814	#num#	k4
<g/>
X	X	kA
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780811718141	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CAPLAN	CAPLAN	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Logan	Logan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Sinking	Sinking	k1gInSc4
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Seattle	Seattle	k1gFnSc1
Miracle	Miracle	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
222	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgNnSc4d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
964461013	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780964461017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CARNEY	CARNEY	kA
<g/>
,	,	kIx,
Kevin	Kevin	k1gMnSc1
Wright	Wright	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Cold	Cold	k1gMnSc1
Night	Night	k1gMnSc1
in	in	k?
the	the	k?
Atlantic	Atlantice	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
AuthorHouse	AuthorHouse	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
228	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1935028014	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781935028017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CLARY	Clara	k1gFnPc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
G.	G.	kA
The	The	k1gMnSc1
Last	Last	k1gMnSc1
True	True	k1gFnSc1
Story	story	k1gFnSc2
of	of	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Domhan	Domhan	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
151	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1583450122	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781583450123	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
COLE	cola	k1gFnSc3
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
D.	D.	kA
The	The	k1gFnSc1
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Disaster	Disaster	k1gInSc1
at	at	k?
Sea	Sea	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Enslow	Enslow	k1gFnSc1
Publishers	Publishersa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
48	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
766015572	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780766015579	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
COLLINS	COLLINS	kA
<g/>
,	,	kIx,
L.	L.	kA
Marmaduke	Marmaduke	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Sinking	Sinking	k1gInSc4
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gMnSc1
Ice	Ice	k1gMnSc1
Pilot	pilot	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Perspective	Perspectiv	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Breakwater	Breakwater	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
196	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1550811738	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781550811735	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
COX	COX	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Titanic	Titanic	k1gInSc4
Story	story	k1gFnSc4
<g/>
:	:	kIx,
Hard	Hard	k1gMnSc1
Choices	Choices	k1gMnSc1
<g/>
,	,	kIx,
Dangerous	Dangerous	k1gMnSc1
Decisions	Decisions	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Open	Open	k1gInSc1
Court	Courta	k1gFnPc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
152	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
812693965	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780812693966	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
DAVIE	DAVIE	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
Death	Death	k1gInSc1
and	and	k?
Life	Life	k1gInSc1
of	of	k?
a	a	k8xC
Legend	legenda	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Knopf	Knopf	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
394558162	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780394558165	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
EATON	EATON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
HAAS	HAAS	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
A.	A.	kA
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
plavba	plavba	k1gFnSc1
do	do	k7c2
záhuby	záhuba	k1gFnSc2
<g/>
:	:	kIx,
legendy	legenda	k1gFnPc1
a	a	k8xC
skutečnost	skutečnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Otakar	Otakar	k1gMnSc1
Lanc	Lanc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Iris	iris	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
231	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85893	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
EATON	EATON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
HAAS	HAAS	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
A.	A.	kA
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
Triumph	Triumph	k1gInSc1
and	and	k?
Tragedy	Trageda	k1gMnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Norton	Norton	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
393036979	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780393036978	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GELLER	GELLER	kA
<g/>
,	,	kIx,
Judith	Judith	k1gInSc1
B.	B.	kA
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
artifact	artifact	k2eAgInSc4d1
exhibit	exhibit	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
47	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
970899408	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780970899408	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
EATON	EATON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
HAAS	HAAS	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
A.	A.	kA
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Destination	Destination	k1gInSc1
disaster	disaster	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Legends	Legends	k1gInSc1
and	and	k?
the	the	k?
Reality	realita	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
184	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
393315134	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780393315134	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GRACIE	GRACIE	kA
<g/>
,	,	kIx,
Archibald	Archibald	k1gMnSc1
<g/>
;	;	kIx,
THAYER	THAYER	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
B.	B.	kA
TITANIC	Titanic	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Survivor	Survivor	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Story	story	k1gFnSc7
and	and	k?
The	The	k1gFnSc1
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
S.	S.	kA
<g/>
S.	S.	kA
TITANIC	Titanic	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Academy	Academ	k1gInPc1
Chicago	Chicago	k1gNnSc1
Publishers	Publishers	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
356	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
897334523	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780897334525	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HUBÁČEK	Hubáček	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
439	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
486	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LORD	lord	k1gMnSc1
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
night	night	k1gMnSc1
to	ten	k3xDgNnSc4
remember	remember	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Bantam	bantam	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
.	.	kIx.
209	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
553278274	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780553278279	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LORD	lord	k1gMnSc1
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
night	night	k1gMnSc1
lives	lives	k1gMnSc1
on	on	k3xPp3gMnSc1
<g/>
:	:	kIx,
New	New	k1gMnSc1
Ideas	Ideas	k1gMnSc1
on	on	k3xPp3gMnSc1
Titanic	Titanic	k1gInSc4
Disaster	Disaster	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Morrow	Morrow	k1gMnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
688049397	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780688049393	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LORD	lord	k1gMnSc1
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
<g/>
;	;	kIx,
PHILBRICK	PHILBRICK	kA
<g/>
,	,	kIx,
Nathaniel	Nathaniel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Night	Night	k1gMnSc1
to	ten	k3xDgNnSc4
Remember	Remember	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Henry	Henry	k1gMnSc1
<g/>
,	,	kIx,
Holt	Holt	k?
<g/>
,	,	kIx,
and	and	k?
Co	co	k9
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
805077642	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780805077643	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MACFIE	MACFIE	kA
<g/>
,	,	kIx,
Ronald	Ronald	k1gMnSc1
Campbell	Campbell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
An	An	k1gFnSc1
Ode	ode	k7c2
of	of	k?
Immortality	Immortalita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Nabu	Naba	k1gFnSc4
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
42	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1178294110	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781178294118	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MARCUS	MARCUS	kA
<g/>
,	,	kIx,
Geoffrey	Geoffrea	k1gFnSc2
Jules	Julesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
maiden	maidna	k1gFnPc2
voyage	voyagat	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Taylor	Taylor	k1gMnSc1
&	&	k?
Francis	Francis	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
319	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MARRIOTT	MARRIOTT	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Based	Based	k1gMnSc1
on	on	k3xPp3gMnSc1
an	an	k?
Account	Account	k1gMnSc1
from	from	k1gMnSc1
Disaster	Disaster	k1gMnSc1
at	at	k?
Sea	Sea	k1gMnSc1
by	by	kYmCp3nS
John	John	k1gMnSc1
Marriott	Marriott	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Heinemann	Heinemann	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
435104462	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780435104467	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Logan	Logan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
and	and	k?
Great	Great	k1gInSc1
Sea	Sea	k1gMnSc1
Disasters	Disasters	k1gInSc1
-	-	kIx~
As	as	k9
Told	Told	k1gInSc4
by	by	kYmCp3nS
First	First	k1gMnSc1
Hand	Hand	k1gMnSc1
Account	Account	k1gMnSc1
of	of	k?
Survivors	Survivors	k1gInSc1
and	and	k?
Initial	Initial	k1gInSc1
Investigations	Investigations	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Arc	Arc	k1gMnPc1
Manor	Manora	k1gFnPc2
LLC	LLC	kA
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
156	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1604502819	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781604502817	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Logan	Logan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
and	and	k?
Great	Great	k1gInSc1
Sea	Sea	k1gMnSc1
Disasters	Disasters	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Kessinger	Kessinger	k1gInSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
LLC	LLC	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1419247352	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781419247354	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MARSHALL-GREEN	MARSHALL-GREEN	k?
<g/>
,	,	kIx,
Logan	Logan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
and	and	k?
Great	Great	k1gInSc1
Sea	Sea	k1gMnSc1
Disasters	Disasters	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Detailed	Detailed	k1gInSc1
and	and	k?
Accurate	Accurat	k1gInSc5
Account	Accounta	k1gFnPc2
of	of	k?
the	the	k?
Most	most	k1gInSc1
Awful	Awful	k1gInSc1
Marine	Marin	k1gInSc5
Disaster	Disaster	k1gInSc4
in	in	k?
History	Histor	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Forgotten	Forgotten	k2eAgInSc1d1
Books	Books	k1gInSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1606801546	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781606801543	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MCKEOWN	MCKEOWN	kA
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
<g/>
;	;	kIx,
HOGAN	HOGAN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Aladdin	Aladdina	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
48	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
689824769	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780689824760	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MOWBRAY	MOWBRAY	kA
<g/>
,	,	kIx,
Jay	Jay	k1gMnSc1
Henry	Henry	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Eyewitness	Eyewitness	k1gInSc1
Accounts	Accounts	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Courier	Courier	k1gMnSc1
Dover	Dover	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
336	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
486402983	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780486402987	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
O	O	kA
<g/>
'	'	kIx"
<g/>
DONNELL	DONNELL	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Eugene	Eugen	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Father	Fathra	k1gFnPc2
Browne	Brown	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Titanic	Titanic	k1gInSc1
album	album	k1gNnSc1
<g/>
:	:	kIx,
a	a	k8xC
passenger	passenger	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
photographs	photographs	k1gInSc1
and	and	k?
personal	personat	k5eAaImAgInS,k5eAaPmAgInS
memoir	memoir	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wolfhound	Wolfhound	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
120	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
863275982	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780863275982	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PALAZZO-CRAIG	PALAZZO-CRAIG	k?
<g/>
,	,	kIx,
Janet	Janet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
book	book	k1gInSc1
of	of	k?
fascinating	fascinating	k1gInSc1
facts	facts	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Reoll	Reoll	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
32	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
816749868	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780816749867	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PELLEGRINO	PELLEGRINO	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
R.	R.	kA
Ghosts	Ghosts	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Avon	Avon	k1gNnSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
339	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
380724723	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780380724727	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
С	С	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
.	.	kIx.
«	«	k?
<g/>
Т	Т	k?
<g/>
»	»	k?
<g/>
:	:	kIx,
л	л	k?
и	и	k?
д	д	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
С	С	k?
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
С	С	k?
<g/>
,	,	kIx,
Л	Л	k?
Н	Н	k?
<g/>
.	.	kIx.
П	П	k?
SOS	sos	k1gInSc1
«	«	k?
<g/>
В	В	k?
<g/>
»	»	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
М	М	k?
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
269	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
THOMAS	Thomas	k1gMnSc1
<g/>
,	,	kIx,
Joseph	Joseph	k1gMnSc1
L.	L.	kA
Grandma	Grandma	k1gFnSc1
Survived	Survived	k1gInSc1
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Authorhouse	Authorhouse	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
156	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1425921922	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781425921927	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TIBBALLS	TIBBALLS	kA
<g/>
,	,	kIx,
Geoff	Geoff	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Mammoth	Mammoth	k1gMnSc1
Book	Book	k1gMnSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Contemporary	Contemporara	k1gFnSc2
Accounts	Accounts	k1gInSc1
from	from	k1gMnSc1
Survivors	Survivorsa	k1gFnPc2
and	and	k?
the	the	k?
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Running	Running	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
786710055	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780786710058	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WADE	WADE	k?
<g/>
,	,	kIx,
Wyn	Wyn	k1gMnSc1
Craig	Craig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
End	End	k1gFnSc1
of	of	k?
a	a	k8xC
Dream	Dream	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
358	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
140166912	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780140166910	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WILSON	WILSON	kA
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
H.	H.	kA
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnPc1
Rape	rape	k1gNnSc2
of	of	k?
a	a	k8xC
Queen	Queen	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Authorhouse	Authorhouse	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
140	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
75964862	#num#	k4
<g/>
X	X	kA
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780759648623	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WINOCOUR	WINOCOUR	kA
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
<g/>
;	;	kIx,
BEESLEY	BEESLEY	kA
<g/>
,	,	kIx,
Lawrence	Lawrence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
story	story	k1gFnSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
as	as	k9
told	told	k6eAd1
by	by	kYmCp3nS
its	its	k?
survivors	survivors	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Courier	Courier	k1gMnSc1
Dover	Dover	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
320	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
486206106	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780486206103	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Wreck	Wreck	k1gInSc1
and	and	k?
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Forgotten	Forgotten	k2eAgInSc1d1
Books	Books	k1gInSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1440090203	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781440090202	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sinking	Sinking	k1gInSc1
of	of	k?
the	the	k?
Titanic	Titanic	k1gInSc1
and	and	k?
Great	Great	k1gInSc1
Sea	Sea	k1gMnSc1
Disasters	Disasters	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Publishing	Publishing	k1gInSc4
Hardpress	Hardpress	k1gInSc1
Publishing	Publishing	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
HardPress	HardPress	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
214	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
140764601	#num#	k4
<g/>
X	X	kA
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781407646015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnPc1
Official	Official	k1gInSc4
Inquiry	Inquira	k1gFnSc2
<g/>
:	:	kIx,
Court	Courta	k1gFnPc2
of	of	k?
Enquiry	Enquir	k1gInPc1
Report	report	k1gInSc1
and	and	k?
Evidence	evidence	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
His	his	k1gNnSc1
Majesty	Majest	k1gInPc7
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Stationery	Stationer	k1gMnPc7
Office	Office	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
972	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1873162707	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781873162705	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
National	National	k1gFnSc1
Geographic	Geographice	k1gInPc2
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
199	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
32	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
lodě	loď	k1gFnPc1
</s>
<s>
HMHS	HMHS	kA
Britannic	Britannice	k1gFnPc2
–	–	k?
sesterská	sesterský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
RMS	RMS	kA
Olympic	Olympic	k1gMnSc1
–	–	k?
sesterská	sesterský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
RMS	RMS	kA
Carpathia	Carpathia	k1gFnSc1
–	–	k?
loď	loď	k1gFnSc1
trosečníků	trosečník	k1gMnPc2
</s>
<s>
společnosti	společnost	k1gFnPc1
a	a	k8xC
osoby	osoba	k1gFnPc1
</s>
<s>
White	Whiit	k5eAaImRp2nP,k5eAaPmRp2nP,k5eAaBmRp2nP
Star	star	k1gFnSc1
Line	linout	k5eAaImIp3nS
–	–	k?
lodní	lodní	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
,	,	kIx,
pod	pod	k7c7
jejíž	jejíž	k3xOyRp3gFnSc7
vlajkou	vlajka	k1gFnSc7
plul	plout	k5eAaImAgInS
Titanic	Titanic	k1gInSc1
</s>
<s>
John	John	k1gMnSc1
Pierpont	Pierpont	k1gMnSc1
Morgan	morgan	k1gMnSc1
–	–	k?
americký	americký	k2eAgMnSc1d1
bankéř	bankéř	k1gMnSc1
a	a	k8xC
finančník	finančník	k1gMnSc1
</s>
<s>
Edward	Edward	k1gMnSc1
J.	J.	kA
Smith	Smith	k1gMnSc1
–	–	k?
kapitán	kapitán	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
</s>
<s>
Thomas	Thomas	k1gMnSc1
Andrews	Andrewsa	k1gFnPc2
–	–	k?
konstruktér	konstruktér	k1gMnSc1
Titanicu	Titanicus	k1gInSc2
</s>
<s>
Violet	Violeta	k1gFnPc2
Jessop	Jessop	k1gInSc1
–	–	k?
stevardka	stevardka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
sloužila	sloužit	k5eAaImAgFnS
na	na	k7c6
lodích	loď	k1gFnPc6
RMS	RMS	kA
Olympic	Olympice	k1gFnPc2
<g/>
,	,	kIx,
RMS	RMS	kA
Titanic	Titanic	k1gInSc1
a	a	k8xC
HMHS	HMHS	kA
Britannic	Britannice	k1gFnPc2
</s>
<s>
podobné	podobný	k2eAgFnPc1d1
katastrofy	katastrofa	k1gFnPc1
</s>
<s>
Ztroskotání	ztroskotání	k1gNnSc1
lodi	loď	k1gFnSc2
Costa	Costo	k1gNnSc2
Concordia	Concordium	k1gNnSc2
(	(	kIx(
<g/>
shodná	shodný	k2eAgFnSc1d1
příčina	příčina	k1gFnSc1
<g/>
;	;	kIx,
protržení	protržení	k1gNnSc1
trupu	trup	k1gInSc2
pod	pod	k7c7
čarou	čára	k1gFnSc7
ponoru	ponor	k1gInSc2
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Titanic	Titanic	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Titanic	Titanic	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
Noci	noc	k1gFnSc2
chiméry	chiméra	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Kupec	kupec	k1gMnSc1
hrůzy	hrůza	k1gFnSc2
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Titanic	Titanic	k1gInSc1
World	World	k1gInSc1
(	(	kIx(
<g/>
titanicworld	titanicworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
–	–	k?
kompletní	kompletní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
o	o	k7c6
Titanicu	Titanicum	k1gNnSc6
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Konec	konec	k1gInSc1
stanice	stanice	k1gFnSc2
MGY	MGY	kA
–	–	k?
události	událost	k1gFnSc6
vedoucí	vedoucí	k2eAgFnSc6d1
ke	k	k7c3
katastrofě	katastrofa	k1gFnSc3
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
průběh	průběh	k1gInSc4
z	z	k7c2
pohledu	pohled	k1gInSc2
radiokomunikace	radiokomunikace	k1gFnSc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
R.	R.	kA
<g/>
M.	M.	kA
<g/>
S.	S.	kA
Titanic	Titanic	k1gInSc1
–	–	k?
fanouškovská	fanouškovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
Universe	Universe	k1gFnSc2
–	–	k?
fanouškovská	fanouškovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
Articles	Articles	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Aftermath	Aftermath	k1gMnSc1
of	of	k?
the	the	k?
Sinking	Sinking	k1gInSc1
of	of	k?
R.	R.	kA
<g/>
M.	M.	kA
<g/>
S.	S.	kA
Titanic	Titanic	k1gInSc1
–	–	k?
fanouškovská	fanouškovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Titanik	Titanik	k1gInSc1
<g/>
:	:	kIx,
Peklo	peklo	k1gNnSc1
před	před	k7c7
katastrofou	katastrofa	k1gFnSc7
<g/>
:	:	kIx,
dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
YouTube	YouTub	k1gInSc5
<g/>
;	;	kIx,
max	max	kA
<g/>
.	.	kIx.
kvalita	kvalita	k1gFnSc1
obrazu	obraz	k1gInSc2
720	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Titanic	Titanic	k1gInSc1
<g/>
,	,	kIx,
prokletá	prokletý	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
:	:	kIx,
dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
YouTube	YouTub	k1gInSc5
<g/>
;	;	kIx,
max	max	kA
<g/>
.	.	kIx.
kvalita	kvalita	k1gFnSc1
obrazu	obraz	k1gInSc2
480	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Ztroskotání	ztroskotání	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
<g/>
:	:	kIx,
Mimo	mimo	k7c4
kontrolu	kontrola	k1gFnSc4
<g/>
:	:	kIx,
dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
YouTube	YouTub	k1gInSc5
<g/>
;	;	kIx,
max	max	kA
<g/>
.	.	kIx.
kvalita	kvalita	k1gFnSc1
obrazu	obraz	k1gInSc2
480	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Odkrývání	odkrývání	k1gNnSc1
Titanicu	Titanicus	k1gInSc2
<g/>
:	:	kIx,
dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
YouTube	YouTub	k1gInSc5
<g/>
;	;	kIx,
max	max	kA
<g/>
.	.	kIx.
kvalita	kvalita	k1gFnSc1
obrazu	obraz	k1gInSc2
360	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Časová	časový	k2eAgFnSc1d1
posloupnost	posloupnost	k1gFnSc1
největších	veliký	k2eAgFnPc2d3
osobních	osobní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
</s>
<s>
Royal	Royal	k1gInSc1
William	William	k1gInSc1
(	(	kIx(
<g/>
1831	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Great	Great	k1gInSc1
Western	Western	kA
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
•	•	k?
British	British	k1gInSc1
Queen	Queen	k1gInSc1
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
)	)	kIx)
•	•	k?
President	president	k1gMnSc1
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Great	Great	k1gInSc1
Britain	Britain	k1gInSc1
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atrato	Atrat	k2eAgNnSc1d1
(	(	kIx(
<g/>
1854	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Great	Great	k1gInSc1
Eastern	Eastern	k1gInSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Celtic	Celtice	k1gFnPc2
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baltic	Baltice	k1gFnPc2
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Empress	Empress	k1gInSc1
of	of	k?
Scotland	Scotland	k1gInSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lusitania	Lusitanium	k1gNnSc2
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mauretania	Mauretanium	k1gNnSc2
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Olympic	Olympic	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Imperator	Imperator	k1gInSc1
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leviathan	Leviathan	k1gInSc1
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Majestic	Majestice	k1gFnPc2
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Normandie	Normandie	k1gFnSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Queen	Queen	k1gInSc1
Elizabeth	Elizabeth	k1gFnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carnival	Carnival	k1gFnSc1
Destiny	Destina	k1gFnSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Grand	grand	k1gMnSc1
Princess	Princess	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Voyager	Voyager	k1gInSc1
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Explorer	Explorer	k1gInSc1
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Navigator	Navigator	k1gInSc1
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Queen	Queen	k1gInSc1
Mary	Mary	k1gFnSc1
2	#num#	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Freedom	Freedom	k1gInSc1
of	of	k?
the	the	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Liberty	Libert	k1gInPc1
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Independence	Independence	k1gFnSc1
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oasis	Oasis	k1gInSc1
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Allure	Allur	k1gMnSc5
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Harmony	Harmona	k1gFnSc2
of	of	k?
the	the	k?
Seas	Seas	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lodě	loď	k1gFnPc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
třídy	třída	k1gFnPc4
Olympic	Olympice	k1gInPc2
</s>
<s>
Olympic	Olympic	k1gMnSc1
•	•	k?
Titanic	Titanic	k1gInSc1
•	•	k?
Britannic	Britannice	k1gFnPc2
</s>
<s>
Lodě	loď	k1gFnPc1
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
Zachované	zachovaný	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
</s>
<s>
Nomadic	Nomadic	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
Plánované	plánovaný	k2eAgNnSc1d1
</s>
<s>
Oceanic	Oceanice	k1gFnPc2
(	(	kIx(
<g/>
nedokončený	dokončený	k2eNgInSc1d1
<g/>
)	)	kIx)
Bývalé	bývalý	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
</s>
<s>
Red	Red	k?
Jacket	Jacket	k1gInSc1
(	(	kIx(
<g/>
1854	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tayleur	Tayleur	k1gMnSc1
(	(	kIx(
<g/>
1854	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oceanic	Oceanice	k1gFnPc2
(	(	kIx(
<g/>
1870	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlantic	Atlantice	k1gFnPc2
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baltic	Baltice	k1gFnPc2
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tropic	Tropic	k1gMnSc1
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Asiatic	Asiatice	k1gFnPc2
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Republic	Republice	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Adriatic	Adriatice	k1gFnPc2
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Celtic	Celtice	k1gFnPc2
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Traffic	Traffic	k1gMnSc1
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Belgic	Belgic	k1gMnSc1
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gaelic	Gaelice	k1gFnPc2
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Britannic	Britannice	k1gFnPc2
(	(	kIx(
<g/>
1874	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Germanic	Germanice	k1gFnPc2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arabic	Arabic	k1gMnSc1
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Coptic	Coptice	k1gFnPc2
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ionic	Ionic	k1gMnSc1
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Doric	Doric	k1gMnSc1
(	(	kIx(
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Belgic	Belgic	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gaelic	Gaelice	k1gFnPc2
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cufic	Cufic	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1885	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Runic	Runic	k1gMnSc1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Teutonic	Teutonice	k1gFnPc2
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Majestic	Majestice	k1gFnPc2
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nomadic	Nomadic	k1gMnSc1
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tauric	Tauric	k1gMnSc1
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Magnetic	Magnetice	k1gFnPc2
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Naronic	Naronice	k1gFnPc2
(	(	kIx(
<g/>
1892	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Bovic	Bovic	k1gMnSc1
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gothic	Gothic	k1gMnSc1
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cevic	Cevic	k1gMnSc1
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pontic	Pontice	k1gFnPc2
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Georgic	Georgic	k1gMnSc1
(	(	kIx(
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Delphic	Delphic	k1gMnSc1
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cymric	Cymric	k1gMnSc1
(	(	kIx(
<g/>
1898	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Afric	Afric	k1gMnSc1
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Medic	Medic	k1gMnSc1
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Persic	Persic	k1gMnSc1
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oceanic	Oceanice	k1gFnPc2
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Runic	Runic	k1gMnSc1
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Suevic	Suevic	k1gMnSc1
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Celtic	Celtice	k1gFnPc2
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Athenic	Athenice	k1gFnPc2
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Corinthic	Corinthic	k1gMnSc1
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ionic	Ionic	k1gMnSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cedric	Cedric	k1gMnSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Victorian	Victorian	k1gInSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Armenian	Armenian	k1gInSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arabic	Arabic	k1gMnSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Romanic	Romanice	k1gFnPc2
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cretic	Cretice	k1gFnPc2
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Republic	Republice	k1gFnPc2
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Canopic	Canopic	k1gMnSc1
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cufic	Cufic	k1gMnSc1
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baltic	Baltice	k1gFnPc2
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tropic	Tropic	k1gMnSc1
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gallic	Gallice	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Adriatic	Adriatice	k1gFnPc2
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurentic	Laurentice	k1gFnPc2
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Megantic	Megantice	k1gFnPc2
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zeeland	Zeeland	k1gInSc1
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Traffic	Traffic	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Olympic	Olympic	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Belgic	Belgic	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zealandic	Zealandic	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Titanic	Titanic	k1gInSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ceramic	Ceramic	k1gMnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vaderland	Vaderland	k1gInSc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lapland	Lapland	k1gInSc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Britannic	Britannice	k1gFnPc2
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Belgic	Belgic	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1917	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Justicia	Justicia	k1gFnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vedic	Vedic	k1gMnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bardic	Bardic	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gallic	Gallice	k1gFnPc2
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mobile	mobile	k1gNnSc4
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arabic	Arabic	k1gMnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Homeric	Homeric	k1gMnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Haverford	Haverford	k1gInSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Poland	Poland	k1gInSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Majestic	Majestice	k1gFnPc2
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pittsburgh	Pittsburgh	k1gInSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Doric	Doric	k1gMnSc1
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Delphic	Delphic	k1gMnSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Regina	Regina	k1gFnSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Albertic	Albertice	k1gFnPc2
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Calgaric	Calgaric	k1gMnSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurentic	Laurentice	k1gFnPc2
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Britannic	Britannice	k1gFnPc2
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Georgic	Georgic	k1gMnSc1
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roky	rok	k1gInPc1
označují	označovat	k5eAaImIp3nP
počátek	počátek	k1gInSc4
služby	služba	k1gFnSc2
pro	pro	k7c4
White	Whit	k1gInSc5
Star	Star	kA
Line	linout	k5eAaImIp3nS
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
olak	olak	k1gInSc1
<g/>
2002111888	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4121777-9	4121777-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50082026	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
264840228	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50082026	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
