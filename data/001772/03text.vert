<s>
Beetlejuice	Beetlejuice	k1gFnSc1	Beetlejuice
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
režiséra	režisér	k1gMnSc2	režisér
Tima	Timus	k1gMnSc2	Timus
Burtona	Burton	k1gMnSc2	Burton
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
hororu	horor	k1gInSc2	horor
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
masky	maska	k1gFnPc4	maska
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
mladého	mladý	k2eAgInSc2d1	mladý
páru	pár	k1gInSc2	pár
-	-	kIx~	-
Barbary	Barbara	k1gFnSc2	Barbara
a	a	k8xC	a
Adama	Adam	k1gMnSc2	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
autonehodě	autonehoda	k1gFnSc6	autonehoda
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začaly	začít	k5eAaPmAgInP	začít
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gInPc2	on
dít	dít	k5eAaBmF	dít
divné	divný	k2eAgFnSc3d1	divná
věci	věc	k1gFnSc3	věc
-	-	kIx~	-
nejsou	být	k5eNaImIp3nP	být
např.	např.	kA	např.
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nalezení	nalezení	k1gNnSc6	nalezení
Příručky	příručka	k1gFnSc2	příručka
pro	pro	k7c4	pro
nedávno	nedávno	k6eAd1	nedávno
zesnulé	zesnulá	k1gFnPc4	zesnulá
pochopili	pochopit	k5eAaPmAgMnP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domu	dům	k1gInSc2	dům
se	se	k3xPyFc4	se
nastěhovala	nastěhovat	k5eAaPmAgFnS	nastěhovat
nová	nový	k2eAgFnSc1d1	nová
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
se	s	k7c7	s
zálibou	záliba	k1gFnSc7	záliba
ve	v	k7c6	v
fotografování	fotografování	k1gNnSc6	fotografování
a	a	k8xC	a
oblékaná	oblékaný	k2eAgFnSc1d1	oblékaná
celá	celý	k2eAgFnSc1d1	celá
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
říká	říkat	k5eAaImIp3nS	říkat
umělkyně	umělkyně	k1gFnSc1	umělkyně
a	a	k8xC	a
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přivedla	přivést	k5eAaPmAgFnS	přivést
silně	silně	k6eAd1	silně
nesympatického	sympatický	k2eNgMnSc4d1	nesympatický
Otha	Othus	k1gMnSc4	Othus
<g/>
,	,	kIx,	,
podporujícího	podporující	k2eAgMnSc4d1	podporující
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
rozvíjení	rozvíjení	k1gNnSc6	rozvíjení
její	její	k3xOp3gFnSc2	její
"	"	kIx"	"
<g/>
umělecké	umělecký	k2eAgFnSc2d1	umělecká
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
Barbara	Barbara	k1gFnSc1	Barbara
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snažili	snažit	k5eAaImAgMnP	snažit
dostat	dostat	k5eAaPmF	dostat
je	on	k3xPp3gInPc4	on
z	z	k7c2	z
domu	dům	k1gInSc2	dům
a	a	k8xC	a
život	život	k1gInSc4	život
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
začal	začít	k5eAaPmAgMnS	začít
stávat	stávat	k5eAaImF	stávat
noční	noční	k2eAgFnSc7d1	noční
můrou	můra	k1gFnSc7	můra
-	-	kIx~	-
nikdo	nikdo	k3yNnSc1	nikdo
je	být	k5eAaImIp3nS	být
neviděl	vidět	k5eNaImAgMnS	vidět
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
dům	dům	k1gInSc1	dům
přestavován	přestavován	k2eAgInSc1d1	přestavován
<g/>
.	.	kIx.	.
</s>
<s>
Příručku	příručka	k1gFnSc4	příručka
pořád	pořád	k6eAd1	pořád
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
přečíst	přečíst	k5eAaPmF	přečíst
až	až	k9	až
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
-	-	kIx~	-
návod	návod	k1gInSc4	návod
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
čekárně	čekárna	k1gFnSc6	čekárna
plné	plný	k2eAgFnSc6d1	plná
mrtvých	mrtvý	k1gMnPc2	mrtvý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
nakonec	nakonec	k6eAd1	nakonec
odešli	odejít	k5eAaPmAgMnP	odejít
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
sociální	sociální	k2eAgFnSc7d1	sociální
poradkyní	poradkyně	k1gFnSc7	poradkyně
Junonou	Juno	k1gFnSc7	Juno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
dověděli	dovědět	k5eAaPmAgMnP	dovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
té	ten	k3xDgFnSc2	ten
rodiny	rodina	k1gFnSc2	rodina
mají	mít	k5eAaImIp3nP	mít
zbavit	zbavit	k5eAaPmF	zbavit
jako	jako	k9	jako
strašidla	strašidlo	k1gNnPc4	strašidlo
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
proto	proto	k8xC	proto
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
vystrašit	vystrašit	k5eAaPmF	vystrašit
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
přimět	přimět	k5eAaPmF	přimět
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Beetlejuice	Beetlejuice	k1gFnSc2	Beetlejuice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Beetlejuice	Beetlejuice	k1gFnSc1	Beetlejuice
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Beetlejuice	Beetlejuice	k1gFnSc1	Beetlejuice
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc6	Databasa
</s>
