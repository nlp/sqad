<s>
Coldplay	Coldpla	k1gMnPc4	Coldpla
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
hrající	hrající	k2eAgInSc4d1	hrající
alternativní	alternativní	k2eAgInSc4d1	alternativní
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgFnPc7	svůj
rockovými	rockový	k2eAgFnPc7d1	rocková
melodiemi	melodie	k1gFnPc7	melodie
a	a	k8xC	a
introspektivními	introspektivní	k2eAgInPc7d1	introspektivní
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
zařazováni	zařazovat	k5eAaImNgMnP	zařazovat
do	do	k7c2	do
škatulky	škatulka	k1gFnSc2	škatulka
"	"	kIx"	"
<g/>
piano-rock	pianoock	k1gInSc1	piano-rock
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pop	pop	k1gMnSc1	pop
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
získala	získat	k5eAaPmAgFnS	získat
několik	několik	k4yIc4	několik
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
šesti	šest	k4xCc2	šest
Brit	Brit	k1gMnSc1	Brit
Awards	Awardsa	k1gFnPc2	Awardsa
–	–	k?	–
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
,	,	kIx,	,
a	a	k8xC	a
7	[number]	k4	7
prestižních	prestižní	k2eAgFnPc2d1	prestižní
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
(	(	kIx(	(
<g/>
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
dvaceti	dvacet	k4xCc2	dvacet
nominací	nominace	k1gFnPc2	nominace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chris	Chris	k1gFnSc1	Chris
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgMnSc1d1	hlavní
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
/	/	kIx~	/
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Jon	Jon	k1gMnSc1	Jon
Buckland	Buckland	k1gInSc1	Buckland
<g/>
:	:	kIx,	:
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
Guy	Guy	k1gFnSc2	Guy
Berryman	Berryman	k1gMnSc1	Berryman
<g/>
:	:	kIx,	:
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
Will	Willa	k1gFnPc2	Willa
Champion	Champion	k1gInSc1	Champion
<g/>
:	:	kIx,	:
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
Coldplay	Coldplaa	k1gFnSc2	Coldplaa
jsou	být	k5eAaImIp3nP	být
britská	britský	k2eAgFnSc1d1	britská
alternative-rocková	alternativeockový	k2eAgFnSc1d1	alternative-rockový
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
její	její	k3xOp3gInPc4	její
frontman	frontman	k1gMnSc1	frontman
Chris	Chris	k1gFnSc2	Chris
Martin	Martin	k1gMnSc1	Martin
se	s	k7c7	s
spolužákem	spolužák	k1gMnSc7	spolužák
Johnym	Johnym	k1gInSc4	Johnym
Bucklandem	Buckland	k1gInSc7	Buckland
během	během	k7c2	během
studií	studie	k1gFnPc2	studie
na	na	k7c4	na
londýnské	londýnský	k2eAgFnPc4d1	londýnská
University	universita	k1gFnPc4	universita
College	College	k1gNnSc2	College
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
si	se	k3xPyFc3	se
chtěli	chtít	k5eAaImAgMnP	chtít
říkat	říkat	k5eAaImF	říkat
Pectoralz	Pectoralz	k1gInSc4	Pectoralz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
po	po	k7c6	po
pár	pár	k4xCyI	pár
týdnech	týden	k1gInPc6	týden
přidal	přidat	k5eAaPmAgMnS	přidat
Guy	Guy	k1gFnSc4	Guy
Berryman	Berryman	k1gMnSc1	Berryman
jakožto	jakožto	k8xS	jakožto
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
se	se	k3xPyFc4	se
na	na	k7c4	na
Starfish	Starfish	k1gInSc4	Starfish
<g/>
.	.	kIx.	.
</s>
<s>
Multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
Will	Willa	k1gFnPc2	Willa
Champion	Champion	k1gInSc1	Champion
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
jako	jako	k8xS	jako
poslední	poslední	k2eAgFnSc3d1	poslední
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
bicí	bicí	k2eAgInSc4d1	bicí
během	během	k7c2	během
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
tak	tak	k6eAd1	tak
celou	celý	k2eAgFnSc4d1	celá
sestavu	sestava	k1gFnSc4	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
manažer	manažer	k1gMnSc1	manažer
Phil	Phil	k1gMnSc1	Phil
Harvey	Harvea	k1gFnSc2	Harvea
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
neoficiální	neoficiální	k2eAgMnSc1d1	neoficiální
pátý	pátý	k4xOgMnSc1	pátý
člen	člen	k1gMnSc1	člen
Coldplay	Coldplaa	k1gFnSc2	Coldplaa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
čtyři	čtyři	k4xCgMnPc1	čtyři
mladíci	mladík	k1gMnPc1	mladík
přijali	přijmout	k5eAaPmAgMnP	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
nahráním	nahrání	k1gNnSc7	nahrání
a	a	k8xC	a
vydáním	vydání	k1gNnSc7	vydání
tří	tři	k4xCgFnPc2	tři
EPː	EPː	k1gFnPc2	EPː
Safety	Safeta	k1gFnSc2	Safeta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Brothers	Brothers	k1gInSc1	Brothers
and	and	k?	and
Sisters	Sisters	k1gInSc1	Sisters
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
a	a	k8xC	a
The	The	k1gMnSc1	The
Blue	Blue	k1gFnPc2	Blue
Room	Room	k1gMnSc1	Room
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
podvědomí	podvědomí	k1gNnSc2	podvědomí
posluchačů	posluchač	k1gMnPc2	posluchač
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
po	po	k7c6	po
upsání	upsání	k1gNnSc6	upsání
se	se	k3xPyFc4	se
britské	britský	k2eAgFnPc1d1	britská
nahrávací	nahrávací	k2eAgFnPc1d1	nahrávací
společnosti	společnost	k1gFnPc1	společnost
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
slávy	sláva	k1gFnSc2	sláva
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vydáním	vydání	k1gNnSc7	vydání
singlu	singl	k1gInSc2	singl
Yellow	Yellow	k1gFnSc2	Yellow
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
následovaným	následovaný	k2eAgMnPc3d1	následovaný
jejich	jejich	k3xOp3gFnSc4	jejich
prvním	první	k4xOgMnSc6	první
studiovým	studiový	k2eAgInSc7d1	studiový
albem	album	k1gNnSc7	album
jménem	jméno	k1gNnSc7	jméno
Parachutes	Parachutes	k1gInSc1	Parachutes
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
A	a	k9	a
Rush	Rush	k1gInSc4	Rush
of	of	k?	of
Blood	Blood	k1gInSc1	Blood
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Head	Head	k1gInSc1	Head
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
dostálo	dostát	k5eAaPmAgNnS	dostát
<g/>
,	,	kIx,	,
Coldplay	Coldpla	k2eAgFnPc1d1	Coldpla
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
měli	mít	k5eAaImAgMnP	mít
velmi	velmi	k6eAd1	velmi
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
ohlasy	ohlas	k1gInPc4	ohlas
a	a	k8xC	a
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
i	i	k9	i
několik	několik	k4yIc4	několik
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ceny	cena	k1gFnSc2	cena
NME	NME	kA	NME
Album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
X	X	kA	X
<g/>
&	&	k?	&
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgFnPc3d3	nejprodávanější
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnPc1	recenze
a	a	k8xC	a
názory	názor	k1gInPc1	názor
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byly	být	k5eAaImAgInP	být
sice	sice	k8xC	sice
především	především	k9	především
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mluvilo	mluvit	k5eAaImAgNnS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
stereotypnosti	stereotypnost	k1gFnSc6	stereotypnost
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Viva	Viva	k1gMnSc1	Viva
la	la	k1gNnSc2	la
Vida	Vida	k?	Vida
or	or	k?	or
Death	Death	k1gInSc1	Death
and	and	k?	and
All	All	k1gFnSc2	All
His	his	k1gNnSc2	his
Friends	Friendsa	k1gFnPc2	Friendsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
produkováno	produkovat	k5eAaImNgNnS	produkovat
věhlasným	věhlasný	k2eAgInSc7d1	věhlasný
Brianem	Brian	k1gInSc7	Brian
Eno	Eno	k1gFnSc2	Eno
a	a	k8xC	a
setkalo	setkat	k5eAaPmAgNnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
nadšenými	nadšený	k2eAgFnPc7d1	nadšená
kritikami	kritika	k1gFnPc7	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Získalo	získat	k5eAaPmAgNnS	získat
několik	několik	k4yIc1	několik
nominací	nominace	k1gFnPc2	nominace
i	i	k8xC	i
výher	výhra	k1gFnPc2	výhra
na	na	k7c4	na
51	[number]	k4	51
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
udělování	udělování	k1gNnSc2	udělování
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
vydali	vydat	k5eAaPmAgMnP	vydat
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
album	album	k1gNnSc1	album
Mylo	mýt	k5eAaImAgNnS	mýt
Xyloto	Xylota	k1gFnSc5	Xylota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Ghost	Ghost	k1gMnSc1	Ghost
Stories	Stories	k1gMnSc1	Stories
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
zatím	zatím	k6eAd1	zatím
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
A	a	k9	a
Head	Head	k1gMnSc1	Head
Full	Full	k1gMnSc1	Full
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
první	první	k4xOgFnSc7	první
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
třikrát	třikrát	k6eAd1	třikrát
obdržet	obdržet	k5eAaPmF	obdržet
cenu	cena	k1gFnSc4	cena
Kapela	kapela	k1gFnSc1	kapela
roku	rok	k1gInSc2	rok
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Brit	Brit	k1gMnSc1	Brit
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
na	na	k7c4	na
Martinovy	Martinův	k2eAgFnPc4d1	Martinova
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
)	)	kIx)	)
Coldplay	Coldplaa	k1gMnSc2	Coldplaa
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vyjde	vyjít	k5eAaPmIp3nS	vyjít
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kaleidoscope	Kaleidoscop	k1gInSc5	Kaleidoscop
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
píseň	píseň	k1gFnSc4	píseň
z	z	k7c2	z
tohoto	tento	k3xDgMnSc2	tento
nadcházejícího	nadcházející	k2eAgMnSc2d1	nadcházející
EPu	EPu	k1gMnSc2	EPu
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hypnotized	Hypnotized	k1gInSc1	Hypnotized
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
titulkového	titulkový	k2eAgNnSc2d1	titulkové
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
<s>
Live	Live	k1gInSc1	Live
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
Vydáno	vydán	k2eAgNnSc1d1	vydáno
<g/>
:	:	kIx,	:
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2003	[number]	k4	2003
LeftRightLeftRightLeft	LeftRightLeftRightLeftum	k1gNnPc2	LeftRightLeftRightLeftum
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
Vydáno	vydán	k2eAgNnSc1d1	vydáno
<g/>
:	:	kIx,	:
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
Live	Liv	k1gInSc2	Liv
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
Vydáno	vydán	k2eAgNnSc1d1	vydáno
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
The	The	k1gMnSc1	The
Singles	Singles	k1gMnSc1	Singles
1999-2006	[number]	k4	1999-2006
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Safety	Safeta	k1gFnSc2	Safeta
EP	EP	kA	EP
(	(	kIx(	(
<g/>
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
500	[number]	k4	500
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Brothers	Brothersa	k1gFnPc2	Brothersa
&	&	k?	&
Sisters	Sistersa	k1gFnPc2	Sistersa
EP	EP	kA	EP
(	(	kIx(	(
<g/>
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
2500	[number]	k4	2500
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
dostání	dostání	k1gNnSc1	dostání
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Blue	Blu	k1gFnSc2	Blu
Room	Room	k1gMnSc1	Room
EP	EP	kA	EP
(	(	kIx(	(
<g/>
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
:	:	kIx,	:
EMI	EMI	kA	EMI
a	a	k8xC	a
Parlophone	Parlophon	k1gInSc5	Parlophon
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Trouble	Trouble	k1gMnSc1	Trouble
(	(	kIx(	(
<g/>
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
:	:	kIx,	:
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Sparks	Sparks	k1gInSc1	Sparks
EP	EP	kA	EP
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
:	:	kIx,	:
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
promo	promo	k6eAd1	promo
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Prospekt	prospekt	k1gInSc1	prospekt
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
march	marcha	k1gFnPc2	marcha
EP	EP	kA	EP
(	(	kIx(	(
<g/>
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
:	:	kIx,	:
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Coldplay	Coldplaa	k1gFnSc2	Coldplaa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
skupiny	skupina	k1gFnSc2	skupina
Coldplay	Coldplaa	k1gFnSc2	Coldplaa
Guitar	Guitar	k1gInSc1	Guitar
Chords	Chords	k1gInSc1	Chords
České	český	k2eAgFnSc2d1	Česká
Coldplay	Coldplaa	k1gFnSc2	Coldplaa
forum	forum	k1gNnSc1	forum
a	a	k8xC	a
videa	video	k1gNnSc2	video
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
</s>
