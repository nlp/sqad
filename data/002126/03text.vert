<s>
Marseille	Marseille	k1gFnSc1	Marseille
(	(	kIx(	(
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
maʁ	maʁ	k?	maʁ
<g/>
.	.	kIx.	.
<g/>
sɛ	sɛ	k?	sɛ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
výsl	výsl	k1gInSc1	výsl
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
mɑ	mɑ	k?	mɑ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Provence	Provence	k1gFnSc2	Provence
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Paříži	Paříž	k1gFnSc6	Paříž
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Lvím	lví	k2eAgInSc6d1	lví
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
mělo	mít	k5eAaImAgNnS	mít
Marseille	Marseille	k1gFnSc7	Marseille
852	[number]	k4	852
516	[number]	k4	516
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
jako	jako	k8xS	jako
aglomerace	aglomerace	k1gFnSc1	aglomerace
1	[number]	k4	1
350	[number]	k4	350
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
1	[number]	k4	1
516	[number]	k4	516
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
francouzský	francouzský	k2eAgInSc4d1	francouzský
obchodní	obchodní	k2eAgInSc4d1	obchodní
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
sídlo	sídlo	k1gNnSc1	sídlo
marseilleského	marseilleský	k2eAgNnSc2d1	marseilleské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgInPc2	všecek
národů	národ	k1gInPc2	národ
žijících	žijící	k2eAgInPc2d1	žijící
okolo	okolo	k7c2	okolo
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
včetně	včetně	k7c2	včetně
mnoha	mnoho	k4c2	mnoho
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k9	jako
Massalia	Massalia	k1gFnSc1	Massalia
(	(	kIx(	(
<g/>
Μ	Μ	k?	Μ
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
iónskými	iónský	k2eAgMnPc7d1	iónský
kolonisty	kolonista	k1gMnPc7	kolonista
z	z	k7c2	z
maloasijského	maloasijský	k2eAgInSc2d1	maloasijský
přístavu	přístav	k1gInSc2	přístav
Fókaia	Fókaium	k1gNnSc2	Fókaium
<g/>
.	.	kIx.	.
</s>
<s>
Rozkvétat	rozkvétat	k5eAaImF	rozkvétat
začalo	začít	k5eAaPmAgNnS	začít
asi	asi	k9	asi
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc7	jeho
největšími	veliký	k2eAgMnPc7d3	veliký
soupeři	soupeř	k1gMnPc7	soupeř
Etruskové	Etrusk	k1gMnPc1	Etrusk
a	a	k8xC	a
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
49	[number]	k4	49
před	před	k7c4	před
K.	K.	kA	K.
obsadili	obsadit	k5eAaPmAgMnP	obsadit
město	město	k1gNnSc4	město
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
Marseille	Marseille	k1gFnPc4	Marseille
stalo	stát	k5eAaPmAgNnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přepravovalo	přepravovat	k5eAaImAgNnS	přepravovat
zboží	zboží	k1gNnSc1	zboží
dovozené	dovozený	k2eAgNnSc1d1	dovozené
z	z	k7c2	z
Orientu	Orient	k1gInSc2	Orient
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
odhalily	odhalit	k5eAaPmAgInP	odhalit
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
řeckého	řecký	k2eAgNnSc2d1	řecké
i	i	k8xC	i
římského	římský	k2eAgNnSc2d1	římské
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
část	část	k1gFnSc1	část
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
starou	starý	k2eAgFnSc4d1	stará
silnici	silnice	k1gFnSc4	silnice
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
i	i	k9	i
část	část	k1gFnSc4	část
římského	římský	k2eAgInSc2d1	římský
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
opatství	opatství	k1gNnSc1	opatství
Saint-Victor	Saint-Victor	k1gInSc4	Saint-Victor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rekonstruované	rekonstruovaný	k2eAgMnPc4d1	rekonstruovaný
benediktiny	benediktin	k1gMnPc4	benediktin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rozvoj	rozvoj	k1gInSc1	rozvoj
Marseille	Marseille	k1gFnSc2	Marseille
jako	jako	k8xC	jako
přístavu	přístav	k1gInSc2	přístav
přinesly	přinést	k5eAaPmAgFnP	přinést
křížové	křížový	k2eAgFnPc1d1	křížová
výpravy	výprava	k1gFnPc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Marseille	Marseille	k1gFnSc1	Marseille
začala	začít	k5eAaPmAgFnS	začít
soupeřit	soupeřit	k5eAaImF	soupeřit
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
námořní	námořní	k2eAgFnSc7d1	námořní
velmocí	velmoc	k1gFnSc7	velmoc
Janovem	Janov	k1gInSc7	Janov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1481	[number]	k4	1481
byly	být	k5eAaImAgFnP	být
Marseille	Marseille	k1gFnPc1	Marseille
společně	společně	k6eAd1	společně
s	s	k7c7	s
Provence	Provence	k1gFnPc1	Provence
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1720	[number]	k4	1720
postihl	postihnout	k5eAaPmAgInS	postihnout
město	město	k1gNnSc4	město
mor	mora	k1gFnPc2	mora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
vyslali	vyslat	k5eAaPmAgMnP	vyslat
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Marseille	Marseille	k1gFnSc2	Marseille
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
500	[number]	k4	500
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
s	s	k7c7	s
bojovou	bojový	k2eAgFnSc7d1	bojová
písní	píseň	k1gFnSc7	píseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Marseillaisa	Marseillaisa	k1gFnSc1	Marseillaisa
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
státní	státní	k2eAgFnSc7d1	státní
hymnou	hymna	k1gFnSc7	hymna
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
pol.	pol.	k?	pol.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
dvakrát	dvakrát	k6eAd1	dvakrát
bombardováno	bombardovat	k5eAaImNgNnS	bombardovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
Marseille	Marseille	k1gFnSc2	Marseille
je	být	k5eAaImIp3nS	být
Vieux-Port	Vieux-Port	k1gInSc1	Vieux-Port
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
zátoce	zátoka	k1gFnSc6	zátoka
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
obklopené	obklopený	k2eAgInPc1d1	obklopený
domy	dům	k1gInPc1	dům
s	s	k7c7	s
restauracemi	restaurace	k1gFnPc7	restaurace
a	a	k8xC	a
bary	bar	k1gInPc7	bar
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
zátoka	zátoka	k1gFnSc1	zátoka
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přístaviště	přístaviště	k1gNnSc1	přístaviště
jachet	jachta	k1gFnPc2	jachta
a	a	k8xC	a
rybářských	rybářský	k2eAgFnPc2d1	rybářská
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
Vieux-Portu	Vieux-Port	k1gInSc2	Vieux-Port
stojí	stát	k5eAaImIp3nP	stát
dvě	dva	k4xCgFnPc1	dva
pevnosti	pevnost	k1gFnPc1	pevnost
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
St.	st.	kA	st.
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
St.	st.	kA	st.
Nichola	Nichola	k1gFnSc1	Nichola
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dominant	dominanta	k1gFnPc2	dominanta
Starého	Starého	k2eAgInSc2d1	Starého
přístavu	přístav	k1gInSc2	přístav
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
stojící	stojící	k2eAgFnSc1d1	stojící
bazilika	bazilika	k1gFnSc1	bazilika
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
de	de	k?	de
la	la	k1gNnSc2	la
Garde	garde	k1gFnSc2	garde
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
v	v	k7c6	v
románsko-byzantském	románskoyzantský	k2eAgInSc6d1	románsko-byzantský
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Starého	Starého	k2eAgInSc2d1	Starého
přístavu	přístav	k1gInSc2	přístav
najdeme	najít	k5eAaPmIp1nP	najít
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Viktora	Viktor	k1gMnSc2	Viktor
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
opatství	opatství	k1gNnSc1	opatství
zničené	zničený	k2eAgNnSc1d1	zničené
během	během	k7c2	během
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
pol.	pol.	k?	pol.
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
krypta	krypta	k1gFnSc1	krypta
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
založení	založení	k1gNnSc2	založení
opatství	opatství	k1gNnSc2	opatství
<g/>
,	,	kIx,	,
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
budova	budova	k1gFnSc1	budova
marseillské	marseillský	k2eAgFnSc2d1	marseillská
radnice	radnice	k1gFnSc2	radnice
(	(	kIx(	(
<g/>
Hotel	hotel	k1gInSc1	hotel
de	de	k?	de
Ville	Ville	k1gInSc1	Ville
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Vieux-Portu	Vieux-Port	k1gInSc2	Vieux-Port
<g/>
,	,	kIx,	,
za	za	k7c7	za
opevněním	opevnění	k1gNnSc7	opevnění
St.	st.	kA	st.
Jeane	Jean	k1gMnSc5	Jean
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
marseillská	marseillský	k2eAgFnSc1d1	marseillská
katedrála	katedrála	k1gFnSc1	katedrála
(	(	kIx(	(
<g/>
Cathédrale	Cathédral	k1gMnSc5	Cathédral
de	de	k?	de
la	la	k1gNnSc1	la
Major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
románsko-byzantském	románskoyzantský	k2eAgInSc6d1	románsko-byzantský
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
140	[number]	k4	140
m	m	kA	m
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
70	[number]	k4	70
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
St.	st.	kA	st.
Laurent	Laurent	k1gInSc1	Laurent
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Starého	Starého	k2eAgInSc2d1	Starého
přístavu	přístav	k1gInSc2	přístav
ústí	ústit	k5eAaImIp3nS	ústit
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
marseillských	marseillský	k2eAgFnPc2d1	marseillská
tříd	třída	k1gFnPc2	třída
La	la	k1gNnSc2	la
Canebiere	Canebier	k1gMnSc5	Canebier
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
kaváren	kavárna	k1gFnPc2	kavárna
a	a	k8xC	a
hotelů	hotel	k1gInPc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
východním	východní	k2eAgInSc6d1	východní
konci	konec	k1gInSc6	konec
pak	pak	k6eAd1	pak
najdeme	najít	k5eAaPmIp1nP	najít
neogotický	ogotický	k2eNgInSc4d1	neogotický
kostel	kostel	k1gInSc4	kostel
St.	st.	kA	st.
Vincent	Vincent	k1gMnSc1	Vincent
de	de	k?	de
Paul	Paul	k1gMnSc1	Paul
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Marseille	Marseille	k1gFnSc2	Marseille
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
vrchovina	vrchovina	k1gFnSc1	vrchovina
Calanques	Calanques	k1gInSc1	Calanques
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
Mont	Mont	k1gInSc1	Mont
Puget	Puget	k?	Puget
(	(	kIx(	(
<g/>
563	[number]	k4	563
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhohorní	druhohorní	k2eAgInPc4d1	druhohorní
vápencové	vápencový	k2eAgInPc4d1	vápencový
útesy	útes	k1gInPc4	útes
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
km	km	kA	km
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
4	[number]	k4	4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
francouzského	francouzský	k2eAgInSc2d1	francouzský
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Calanques	Calanquesa	k1gFnPc2	Calanquesa
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Marseille	Marseille	k1gFnSc2	Marseille
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Frioulské	Frioulský	k2eAgInPc1d1	Frioulský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
ostrov	ostrov	k1gInSc1	ostrov
If	If	k1gFnSc2	If
se	s	k7c7	s
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
hradem	hrad	k1gInSc7	hrad
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dlouho	dlouho	k6eAd1	dlouho
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
věznice	věznice	k1gFnPc4	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
vězně	vězeň	k1gMnPc4	vězeň
podle	podle	k7c2	podle
literárního	literární	k2eAgInSc2d1	literární
příběhu	příběh	k1gInSc2	příběh
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
hrabě	hrabě	k1gMnSc5	hrabě
Monte	Mont	k1gMnSc5	Mont
Christo	Christa	k1gMnSc5	Christa
<g/>
,	,	kIx,	,
hrdina	hrdina	k1gMnSc1	hrdina
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dumase	Dumasa	k1gFnSc6	Dumasa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
tenisový	tenisový	k2eAgInSc4d1	tenisový
turnaj	turnaj	k1gInSc4	turnaj
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Olympique	Olympiqu	k1gFnSc2	Olympiqu
de	de	k?	de
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
,	,	kIx,	,
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
vítěz	vítěz	k1gMnSc1	vítěz
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Třetinu	třetina	k1gFnSc4	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
Pytheas	Pytheas	k1gMnSc1	Pytheas
(	(	kIx(	(
<g/>
380	[number]	k4	380
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
310	[number]	k4	310
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
řecký	řecký	k2eAgMnSc1d1	řecký
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
geograf	geograf	k1gMnSc1	geograf
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
Jean-Henry	Jean-Henra	k1gFnSc2	Jean-Henra
Gourgaud	Gourgaud	k1gMnSc1	Gourgaud
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dugazon	Dugazon	k1gMnSc1	Dugazon
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
-	-	kIx~	-
<g/>
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Júlie	Júlie	k1gFnSc2	Júlie
Clary	Clara	k1gFnSc2	Clara
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1771	[number]	k4	1771
<g/>
-	-	kIx~	-
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neapolská	neapolský	k2eAgFnSc1d1	neapolská
<g/>
,	,	kIx,	,
sicilská	sicilský	k2eAgFnSc1d1	sicilská
a	a	k8xC	a
španělská	španělský	k2eAgFnSc1d1	španělská
královna	královna	k1gFnSc1	královna
Désirée	Désiré	k1gFnSc2	Désiré
Clary	Clara	k1gFnSc2	Clara
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
Adolphe	Adolph	k1gInSc2	Adolph
Thiers	Thiersa	k1gFnPc2	Thiersa
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
-	-	kIx~	-
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
<g />
.	.	kIx.	.
</s>
<s>
třetí	třetí	k4xOgFnPc1	třetí
republiky	republika	k1gFnPc1	republika
Étienne	Étienn	k1gInSc5	Étienn
Joseph	Josepha	k1gFnPc2	Josepha
Louis	Louis	k1gMnSc1	Louis
Garnier-Pages	Garnier-Pages	k1gMnSc1	Garnier-Pages
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Honoré	Honorý	k2eAgFnSc2d1	Honorý
Daumier	Daumier	k1gMnSc1	Daumier
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
<g/>
-	-	kIx~	-
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
karikaturista	karikaturista	k1gMnSc1	karikaturista
Joseph	Josepha	k1gFnPc2	Josepha
Autran	Autrana	k1gFnPc2	Autrana
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
-	-	kIx~	-
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Arthur	Arthur	k1gMnSc1	Arthur
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
Charleville-Méziè	Charleville-Méziè	k1gFnPc2	Charleville-Méziè
-	-	kIx~	-
1891	[number]	k4	1891
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gInSc4	Joseph
Garibaldi	Garibald	k1gMnPc1	Garibald
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
Paul	Paul	k1gMnSc1	Paul
Signac	Signac	k1gFnSc1	Signac
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
Charles	Charles	k1gMnSc1	Charles
Fabry	Fabra	k1gFnSc2	Fabra
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Edmond	Edmond	k1gMnSc1	Edmond
Rostand	Rostanda	k1gFnPc2	Rostanda
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
François	François	k1gFnSc2	François
Coli	Col	k1gFnSc2	Col
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
průkopník	průkopník	k1gMnSc1	průkopník
letectví	letectví	k1gNnSc2	letectví
Henri	Henr	k1gMnSc5	Henr
Fabre	Fabr	k1gMnSc5	Fabr
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
průkopník	průkopník	k1gMnSc1	průkopník
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
prvního	první	k4xOgInSc2	první
hydroplánu	hydroplán	k1gInSc2	hydroplán
Otakar	Otakar	k1gMnSc1	Otakar
Kubín	Kubín	k1gMnSc1	Kubín
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
Boskovice	Boskovice	k1gInPc4	Boskovice
-	-	kIx~	-
1969	[number]	k4	1969
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Marcel	Marcel	k1gMnSc1	Marcel
Pagnol	Pagnola	k1gFnPc2	Pagnola
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
Antonin	Antonin	k2eAgInSc1d1	Antonin
Artaud	Artaud	k1gInSc1	Artaud
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Zino	Zina	k1gFnSc5	Zina
Francescatti	Francescatti	k1gNnPc1	Francescatti
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
Fernandel	Fernandlo	k1gNnPc2	Fernandlo
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Louis	Louis	k1gMnSc1	Louis
Jourdan	Jourdan	k1gMnSc1	Jourdan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
César	César	k1gMnSc1	César
Baldaccini	Baldaccin	k2eAgMnPc1d1	Baldaccin
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochařit	k5eAaImRp2nS	sochařit
Gaston	Gaston	k1gInSc4	Gaston
Rébuffat	Rébuffat	k1gMnSc2	Rébuffat
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
horolezec	horolezec	k1gMnSc1	horolezec
a	a	k8xC	a
horský	horský	k2eAgMnSc1d1	horský
vůdce	vůdce	k1gMnSc1	vůdce
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Rampal	Rampal	k1gMnSc6	Rampal
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
flétnista	flétnista	k1gMnSc1	flétnista
Maurice	Maurika	k1gFnSc3	Maurika
Béjart	Béjart	k1gInSc1	Béjart
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baletní	baletní	k2eAgMnSc1d1	baletní
choreograf	choreograf	k1gMnSc1	choreograf
Georges	Georges	k1gMnSc1	Georges
Chappe	Chapp	k1gInSc5	Chapp
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Jean-Claude	Jean-Claud	k1gMnSc5	Jean-Claud
Izzo	Izza	k1gMnSc5	Izza
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Chantal	Chantal	k1gMnSc1	Chantal
Poullain-Polívková	Poullain-Polívková	k1gFnSc1	Poullain-Polívková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Eric	Eric	k1gFnSc1	Eric
Cantona	Cantona	k1gFnSc1	Cantona
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Patrick	Patrick	k1gMnSc1	Patrick
Fiori	Fiore	k1gFnSc4	Fiore
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zpěvák	zpěvák	k1gMnSc1	zpěvák
Zinedine	Zinedin	k1gMnSc5	Zinedin
Zidane	Zidan	k1gMnSc5	Zidan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Romain	Romain	k1gMnSc1	Romain
Barnier	Barnier	k1gMnSc1	Barnier
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavec	plavec	k1gMnSc1	plavec
Sébastien	Sébastina	k1gFnPc2	Sébastina
Grosjean	Grosjean	k1gMnSc1	Grosjean
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Mathieu	Mathieus	k1gInSc2	Mathieus
Flamini	Flamin	k2eAgMnPc1d1	Flamin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Mathieu	Mathieus	k1gInSc2	Mathieus
Ganio	Ganio	k1gMnSc1	Ganio
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
baletu	balet	k1gInSc2	balet
Rémy	Réma	k1gFnSc2	Réma
Di	Di	k1gMnSc1	Di
Gregorio	Gregorio	k1gMnSc1	Gregorio
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Samir	Samir	k1gMnSc1	Samir
Nasri	Nasre	k1gFnSc4	Nasre
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Jordan	Jordan	k1gMnSc1	Jordan
Ayew	Ayew	k1gMnSc1	Ayew
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Na	na	k7c6	na
ozdravném	ozdravný	k2eAgInSc6d1	ozdravný
pobytu	pobyt	k1gInSc6	pobyt
zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
český	český	k2eAgMnSc1d1	český
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Ignát	Ignát	k1gMnSc1	Ignát
Hořica	Hořica	k1gMnSc1	Hořica
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
návštěvě	návštěva	k1gFnSc6	návštěva
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgInS	být
r.	r.	kA	r.
1934	[number]	k4	1934
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
jugoslávský	jugoslávský	k2eAgMnSc1d1	jugoslávský
král	král	k1gMnSc1	král
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
Karađorđević	Karađorđević	k1gMnSc1	Karađorđević
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
hostitel	hostitel	k1gMnSc1	hostitel
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Louis	Louis	k1gMnSc1	Louis
Barthou	Bartha	k1gMnSc7	Bartha
<g/>
.	.	kIx.	.
</s>
<s>
Abidžan	Abidžan	k1gInSc1	Abidžan
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
Dakar	Dakar	k1gInSc1	Dakar
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
Glasgow	Glasgow	k1gInSc1	Glasgow
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
Haifa	Haifa	k1gFnSc1	Haifa
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Kóbe	Kóbe	k1gNnSc2	Kóbe
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
Marrákeš	Marrákeš	k1gInSc1	Marrákeš
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
Oděsa	Oděsa	k1gFnSc1	Oděsa
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Piraeus	Piraeus	k1gInSc1	Piraeus
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
</s>
