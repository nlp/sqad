<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
prvotina	prvotina	k1gFnSc1	prvotina
Klub	klub	k1gInSc1	klub
rváčů	rváč	k1gMnPc2	rváč
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Fight	Fight	k2eAgInSc1d1	Fight
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zfilmovaná	zfilmovaný	k2eAgFnSc1d1	zfilmovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
režisérem	režisér	k1gMnSc7	režisér
Davidem	David	k1gMnSc7	David
Fincherem	Fincher	k1gMnSc7	Fincher
<g/>
.	.	kIx.	.
</s>
