<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Michael	Michael	k1gMnSc1	Michael
Palahniuk	Palahniuk	k1gMnSc1	Palahniuk
<g/>
,	,	kIx,	,
známější	známý	k2eAgMnSc1d2	známější
jako	jako	k8xC	jako
Chuck	Chuck	k1gMnSc1	Chuck
Palahniuk	Palahniuk	k1gMnSc1	Palahniuk
[	[	kIx(	[
<g/>
pólanyk	pólanyk	k1gMnSc1	pólanyk
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
postmoderní	postmoderní	k2eAgMnSc1d1	postmoderní
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
horroru	horror	k1gInSc2	horror
<g/>
)	)	kIx)	)
depresivním	depresivní	k2eAgNnPc3d1	depresivní
tématům	téma	k1gNnPc3	téma
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
současné	současný	k2eAgFnSc2d1	současná
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
čerpá	čerpat	k5eAaImIp3nS	čerpat
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
vlastních	vlastní	k2eAgFnPc2d1	vlastní
frustrací	frustrace	k1gFnPc2	frustrace
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc2d1	vlastní
novinářské	novinářský	k2eAgFnSc2d1	novinářská
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
končí	končit	k5eAaImIp3nS	končit
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
tragicky	tragicky	k6eAd1	tragicky
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
romány	román	k1gInPc1	román
byly	být	k5eAaImAgInP	být
zprvu	zprvu	k6eAd1	zprvu
nakladateli	nakladatel	k1gMnPc7	nakladatel
odmítány	odmítán	k2eAgInPc1d1	odmítán
jako	jako	k9	jako
příliš	příliš	k6eAd1	příliš
temné	temný	k2eAgFnPc1d1	temná
a	a	k8xC	a
depresivní	depresivní	k2eAgFnPc1d1	depresivní
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ale	ale	k9	ale
staly	stát	k5eAaPmAgFnP	stát
kultovními	kultovní	k2eAgFnPc7d1	kultovní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
prvotina	prvotina	k1gFnSc1	prvotina
Klub	klub	k1gInSc1	klub
rváčů	rváč	k1gMnPc2	rváč
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Fight	Fight	k2eAgInSc1d1	Fight
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zfilmovaná	zfilmovaný	k2eAgFnSc1d1	zfilmovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
režisérem	režisér	k1gMnSc7	režisér
Davidem	David	k1gMnSc7	David
Fincherem	Fincher	k1gMnSc7	Fincher
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
autora	autor	k1gMnSc2	autor
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
největším	veliký	k2eAgMnPc3d3	veliký
a	a	k8xC	a
nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
měnícím	měnící	k2eAgInSc7d1	měnící
autorským	autorský	k2eAgInSc7d1	autorský
webem	web	k1gInSc7	web
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
Pascu	Pascus	k1gInSc2	Pascus
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
jmenují	jmenovat	k5eAaImIp3nP	jmenovat
Carol	Carol	k1gInSc1	Carol
a	a	k8xC	a
Fred	Fred	k1gMnSc1	Fred
Palahniukovi	Palahniuk	k1gMnSc3	Palahniuk
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
mobilním	mobilní	k2eAgInSc6d1	mobilní
domě	dům	k1gInSc6	dům
v	v	k7c6	v
přilehlém	přilehlý	k2eAgNnSc6d1	přilehlé
městě	město	k1gNnSc6	město
Burbank	Burbanka	k1gFnPc2	Burbanka
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Chuck	Chuck	k1gMnSc1	Chuck
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
tři	tři	k4xCgMnPc1	tři
sourozenci	sourozenec	k1gMnPc1	sourozenec
trávili	trávit	k5eAaImAgMnP	trávit
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
prarodiči	prarodič	k1gMnPc7	prarodič
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
dobytkářské	dobytkářský	k2eAgFnSc6d1	dobytkářská
farmě	farma	k1gFnSc6	farma
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Wahshingtonu	Wahshington	k1gInSc6	Wahshington
<g/>
.	.	kIx.	.
<g/>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
žurnalistiku	žurnalistika	k1gFnSc4	žurnalistika
na	na	k7c6	na
Oregonské	Oregonský	k2eAgFnSc6d1	Oregonská
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
KLCC	KLCC	kA	KLCC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přísluší	příslušet	k5eAaImIp3nS	příslušet
k	k	k7c3	k
National	National	k1gFnSc3	National
Public	publicum	k1gNnPc2	publicum
Radio	radio	k1gNnSc4	radio
v	v	k7c6	v
městě	město	k1gNnSc6	město
Eugene	Eugen	k1gInSc5	Eugen
v	v	k7c6	v
Oregonu	Oregona	k1gFnSc4	Oregona
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Portlandu	Portland	k1gInSc2	Portland
<g/>
.	.	kIx.	.
</s>
<s>
Krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
psal	psát	k5eAaImAgInS	psát
pro	pro	k7c4	pro
místní	místní	k2eAgFnPc4d1	místní
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
kamiony	kamion	k1gInPc4	kamion
Freightliner	Freightlinra	k1gFnPc2	Freightlinra
jako	jako	k8xC	jako
mechanik	mechanika	k1gFnPc2	mechanika
dieselových	dieselový	k2eAgInPc2d1	dieselový
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
práci	práce	k1gFnSc6	práce
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
jeho	jeho	k3xOp3gFnSc1	jeho
spisovatelská	spisovatelský	k2eAgFnSc1d1	spisovatelská
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
psal	psát	k5eAaImAgMnS	psát
návody	návod	k1gInPc4	návod
jak	jak	k8xC	jak
opravovat	opravovat	k5eAaImF	opravovat
kamiony	kamion	k1gInPc4	kamion
a	a	k8xC	a
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
psal	psát	k5eAaImAgMnS	psát
pro	pro	k7c4	pro
noviny	novina	k1gFnPc4	novina
(	(	kIx(	(
<g/>
k	k	k7c3	k
novinařině	novinařina	k1gFnSc3	novinařina
se	se	k3xPyFc4	se
však	však	k9	však
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náhodné	náhodný	k2eAgFnSc6d1	náhodná
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
úvodním	úvodní	k2eAgInSc6d1	úvodní
semináři	seminář	k1gInSc6	seminář
organizace	organizace	k1gFnSc2	organizace
zvané	zvaný	k2eAgFnSc2d1	zvaná
Landmark	Landmark	k1gInSc1	Landmark
Education	Education	k1gInSc4	Education
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnSc2	svůj
novinářské	novinářský	k2eAgFnSc2d1	novinářská
práce	práce	k1gFnSc2	práce
definitivně	definitivně	k6eAd1	definitivně
zanechal	zanechat	k5eAaPmAgMnS	zanechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
chtěl	chtít	k5eAaImAgMnS	chtít
dělat	dělat	k5eAaImF	dělat
něco	něco	k3yInSc4	něco
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
své	svůj	k3xOyFgNnSc4	svůj
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
dobrovolníkem	dobrovolník	k1gMnSc7	dobrovolník
pro	pro	k7c4	pro
útulek	útulek	k1gInSc4	útulek
pro	pro	k7c4	pro
bezdomovce	bezdomovec	k1gMnPc4	bezdomovec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
také	také	k9	také
dobrovolníkem	dobrovolník	k1gMnSc7	dobrovolník
v	v	k7c6	v
hospici	hospic	k1gInSc6	hospic
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vozil	vozit	k5eAaImAgInS	vozit
smrtelně	smrtelně	k6eAd1	smrtelně
nemocné	mocný	k2eNgMnPc4d1	nemocný
pacienty	pacient	k1gMnPc4	pacient
na	na	k7c4	na
sezení	sezení	k1gNnSc4	sezení
jejich	jejich	k3xOp3gFnPc2	jejich
terapeutických	terapeutický	k2eAgFnPc2d1	terapeutická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dobrovolnictvím	dobrovolnictví	k1gNnSc7	dobrovolnictví
přestal	přestat	k5eAaPmAgMnS	přestat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jednoho	jeden	k4xCgMnSc2	jeden
pacienta	pacient	k1gMnSc2	pacient
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgMnSc3	který
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
začal	začít	k5eAaPmAgMnS	začít
cítit	cítit	k5eAaImF	cítit
oddanost	oddanost	k1gFnSc4	oddanost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
rebelistické	rebelistický	k2eAgFnSc2d1	rebelistický
organizace	organizace	k1gFnSc2	organizace
Cacophony	Cacophona	k1gFnSc2	Cacophona
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pravidelným	pravidelný	k2eAgMnSc7d1	pravidelný
účastníkem	účastník	k1gMnSc7	účastník
akcí	akce	k1gFnPc2	akce
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
každoroční	každoroční	k2eAgFnSc1d1	každoroční
tzv.	tzv.	kA	tzv.
Santa	Santa	k1gFnSc1	Santa
Rampage	Rampage	k1gFnSc1	Rampage
(	(	kIx(	(
<g/>
běsnění	běsnění	k1gNnSc1	běsnění
Santů	Sant	k1gInPc2	Sant
-	-	kIx~	-
veřejná	veřejný	k2eAgFnSc1d1	veřejná
vánoční	vánoční	k2eAgFnSc1d1	vánoční
zábava	zábava	k1gFnSc1	zábava
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
vylomeniny	vylomenina	k1gFnSc2	vylomenina
a	a	k8xC	a
opilost	opilost	k1gFnSc4	opilost
<g/>
)	)	kIx)	)
v	v	k7c6	v
Portlandu	Portland	k1gInSc6	Portland
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
společnosti	společnost	k1gFnSc6	společnost
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
události	událost	k1gFnPc1	událost
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dílech	díl	k1gInPc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
použil	použít	k5eAaPmAgMnS	použít
Cacophony	Cacophona	k1gFnPc4	Cacophona
Society	societa	k1gFnPc4	societa
jako	jako	k8xC	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
Project	Projectum	k1gNnPc2	Projectum
Mayhem	Mayh	k1gInSc7	Mayh
v	v	k7c6	v
Klubu	klub	k1gInSc6	klub
rváčů	rváč	k1gMnPc2	rváč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgFnSc1d1	literární
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
literatury	literatura	k1gFnSc2	literatura
začal	začít	k5eAaPmAgInS	začít
až	až	k9	až
po	po	k7c6	po
třicítce	třicítka	k1gFnSc6	třicítka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgInPc2	svůj
začátků	začátek	k1gInPc2	začátek
také	také	k9	také
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
spisovatelské	spisovatelský	k2eAgInPc4d1	spisovatelský
workshopy	workshop	k1gInPc4	workshop
Toma	Tom	k1gMnSc2	Tom
Spanbauera	Spanbauer	k1gMnSc2	Spanbauer
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
chodil	chodit	k5eAaImAgMnS	chodit
aby	aby	k9	aby
poznal	poznat	k5eAaPmAgMnS	poznat
nové	nový	k2eAgMnPc4d1	nový
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Spanbauer	Spanbauer	k1gMnSc1	Spanbauer
jej	on	k3xPp3gMnSc4	on
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
minimalistickém	minimalistický	k2eAgInSc6d1	minimalistický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
Insomnia	Insomnium	k1gNnPc1	Insomnium
<g/>
:	:	kIx,	:
If	If	k1gFnSc1	If
You	You	k1gMnSc1	You
Lived	Lived	k1gMnSc1	Lived
Here	Here	k1gFnSc1	Here
<g/>
,	,	kIx,	,
You	You	k1gFnSc1	You
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
Be	Be	k1gMnPc4	Be
Home	Hom	k1gFnSc2	Hom
Already	Alreada	k1gFnSc2	Alreada
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
publikována	publikován	k2eAgFnSc1d1	publikována
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gNnSc3	jeho
zklamání	zklamání	k1gNnSc3	zklamání
z	z	k7c2	z
příběhu	příběh	k1gInSc2	příběh
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Klubu	klub	k1gInSc6	klub
rváčů	rváč	k1gMnPc2	rváč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
Invisible	Invisible	k1gMnSc1	Invisible
Monsters	Monsters	k1gInSc1	Monsters
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Neviditelné	viditelný	k2eNgFnSc2d1	neviditelná
nestvůry	nestvůra	k1gFnSc2	nestvůra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
odmítnuto	odmítnout	k5eAaPmNgNnS	odmítnout
vydavateli	vydavatel	k1gMnPc7	vydavatel
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
pobuřující	pobuřující	k2eAgInSc4d1	pobuřující
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
přimělo	přimět	k5eAaPmAgNnS	přimět
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
Klubu	klub	k1gInSc6	klub
rváčů	rváč	k1gMnPc2	rváč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
aby	aby	k9	aby
rozrušil	rozrušit	k5eAaPmAgMnS	rozrušit
vydavatele	vydavatel	k1gMnSc4	vydavatel
ještě	ještě	k6eAd1	ještě
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
psal	psát	k5eAaImAgMnS	psát
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Freightliner	Freightliner	k1gInSc4	Freightliner
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
publikoval	publikovat	k5eAaBmAgMnS	publikovat
část	část	k1gFnSc4	část
jako	jako	k8xS	jako
povídku	povídka	k1gFnSc4	povídka
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
šestá	šestý	k4xOgFnSc1	šestý
kapitola	kapitola	k1gFnSc1	kapitola
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Pursuit	Pursuit	k1gMnSc1	Pursuit
of	of	k?	of
Happiness	Happiness	k1gInSc1	Happiness
(	(	kIx(	(
<g/>
Honba	honba	k1gFnSc1	honba
za	za	k7c7	za
štěstěnou	štěstěna	k1gFnSc7	štěstěna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
román	román	k1gInSc1	román
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
byl	být	k5eAaImAgMnS	být
vydavatel	vydavatel	k1gMnSc1	vydavatel
navzdory	navzdory	k7c3	navzdory
autorovým	autorův	k2eAgInPc3d1	autorův
předpokladům	předpoklad	k1gInPc3	předpoklad
ochoten	ochoten	k2eAgMnSc1d1	ochoten
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
původní	původní	k2eAgNnSc4d1	původní
vydání	vydání	k1gNnSc4	vydání
knihy	kniha	k1gFnSc2	kniha
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
kladná	kladný	k2eAgFnSc1d1	kladná
kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
nějaké	nějaký	k3yIgFnPc4	nějaký
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
nevydrželo	vydržet	k5eNaPmAgNnS	vydržet
na	na	k7c6	na
pultech	pult	k1gInPc6	pult
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
měl	mít	k5eAaImAgInS	mít
Palahniuk	Palahniuk	k1gInSc1	Palahniuk
problém	problém	k1gInSc1	problém
najít	najít	k5eAaPmF	najít
agenta	agent	k1gMnSc4	agent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
publikace	publikace	k1gFnSc2	publikace
Klubu	klub	k1gInSc2	klub
rváčů	rváč	k1gMnPc2	rváč
žádného	žádný	k3yNgMnSc4	žádný
neměl	mít	k5eNaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
začalo	začít	k5eAaPmAgNnS	začít
zajímat	zajímat	k5eAaImF	zajímat
studio	studio	k1gNnSc4	studio
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc4	fox
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
agentem	agent	k1gMnSc7	agent
Edward	Edward	k1gMnSc1	Edward
Hibbert	Hibbert	k1gMnSc1	Hibbert
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
především	především	k9	především
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
Gila	Gilus	k1gMnSc4	Gilus
Chestertona	Chesterton	k1gMnSc4	Chesterton
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
sitcomu	sitcom	k1gInSc6	sitcom
Frasier	Frasier	k1gInSc1	Frasier
<g/>
.	.	kIx.	.
</s>
<s>
Hibbert	Hibbert	k1gMnSc1	Hibbert
vedl	vést	k5eAaImAgMnS	vést
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přivedla	přivést	k5eAaPmAgFnS	přivést
Klub	klub	k1gInSc4	klub
rváčů	rváč	k1gMnPc2	rváč
na	na	k7c4	na
stříbrná	stříbrný	k2eAgNnPc4d1	stříbrné
plátna	plátno	k1gNnPc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
Davidem	David	k1gMnSc7	David
Fincherem	Fincher	k1gMnSc7	Fincher
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnPc4	jeho
producenty	producent	k1gMnPc4	producent
zklamáním	zklamání	k1gNnSc7	zklamání
(	(	kIx(	(
<g/>
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
číslem	číslo	k1gNnSc7	číslo
jedna	jeden	k4xCgFnSc1	jeden
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
promítání	promítání	k1gNnSc2	promítání
<g/>
)	)	kIx)	)
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
byla	být	k5eAaImAgFnS	být
rozporuplná	rozporuplný	k2eAgFnSc1d1	rozporuplná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
DVD	DVD	kA	DVD
stal	stát	k5eAaPmAgInS	stát
kultovním	kultovní	k2eAgNnSc7d1	kultovní
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
novým	nový	k2eAgInSc7d1	nový
úvodem	úvod	k1gInSc7	úvod
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přibyl	přibýt	k5eAaPmAgInS	přibýt
doslov	doslov	k1gInSc1	doslov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
verze	verze	k1gFnSc1	verze
Neviditelné	viditelný	k2eNgFnSc2d1	neviditelná
nestvůr	nestvůra	k1gFnPc2	nestvůra
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Survivor	Survivora	k1gFnPc2	Survivora
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Program	program	k1gInSc1	program
pro	pro	k7c4	pro
přeživší	přeživší	k2eAgFnSc4d1	přeživší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
čemž	což	k3yQnSc6	což
sám	sám	k3xTgMnSc1	sám
jeho	jeho	k3xOp3gMnSc1	jeho
autor	autor	k1gMnSc1	autor
stal	stát	k5eAaPmAgMnS	stát
kultovním	kultovní	k2eAgMnSc7d1	kultovní
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
napsat	napsat	k5eAaPmF	napsat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
bestseller	bestseller	k1gInSc4	bestseller
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
Choke	Chok	k1gFnSc2	Chok
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Zalknutí	zalknutí	k1gNnSc1	zalknutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
knihy	kniha	k1gFnPc1	kniha
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
vyjíždět	vyjíždět	k5eAaImF	vyjíždět
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
propagaci	propagace	k1gFnSc3	propagace
svých	svůj	k3xOyFgFnPc2	svůj
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předčítal	předčítat	k5eAaImAgInS	předčítat
z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
a	a	k8xC	a
také	také	k9	také
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
ještě	ještě	k6eAd1	ještě
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1999	[number]	k4	1999
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgMnS	přinést
sérii	série	k1gFnSc4	série
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
udály	udát	k5eAaPmAgFnP	udát
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
tobě	ty	k3xPp2nSc3	ty
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Palahniuk	Palahniuk	k1gMnSc1	Palahniuk
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
ženou	žena	k1gFnSc7	žena
jménem	jméno	k1gNnSc7	jméno
Donna	donna	k1gFnSc1	donna
Fontaine	Fontain	k1gInSc5	Fontain
<g/>
.	.	kIx.	.
</s>
<s>
Setkali	setkat	k5eAaPmAgMnP	setkat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
inzerát	inzerát	k1gInSc4	inzerát
s	s	k7c7	s
titulkem	titulek	k1gInSc7	titulek
"	"	kIx"	"
<g/>
Kismet	kismet	k1gInSc1	kismet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
bývalý	bývalý	k2eAgMnSc1d1	bývalý
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
Dale	Dale	k1gFnSc1	Dale
Shackleford	Shackleford	k1gInSc1	Shackleford
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgInS	zatknout
za	za	k7c4	za
sexuální	sexuální	k2eAgNnSc4d1	sexuální
obtěžování	obtěžování	k1gNnSc4	obtěžování
a	a	k8xC	a
přísahal	přísahat	k5eAaImAgMnS	přísahat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
jakmile	jakmile	k8xS	jakmile
vyjde	vyjít	k5eAaPmIp3nS	vyjít
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Palahniuk	Palahniuk	k6eAd1	Palahniuk
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Donna	donna	k1gFnSc1	donna
Fontaine	Fontain	k1gInSc5	Fontain
hledala	hledat	k5eAaImAgFnS	hledat
přes	přes	k7c4	přes
inzerát	inzerát	k1gInSc4	inzerát
"	"	kIx"	"
<g/>
největšího	veliký	k2eAgMnSc4d3	veliký
chlapa	chlap	k1gMnSc4	chlap
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
mohla	moct	k5eAaImAgFnS	moct
sehnat	sehnat	k5eAaPmF	sehnat
<g/>
"	"	kIx"	"
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
ochránil	ochránit	k5eAaPmAgMnS	ochránit
před	před	k7c7	před
Shacklefordem	Shackleford	k1gInSc7	Shackleford
a	a	k8xC	a
Palahniukuv	Palahniukuv	k1gMnSc1	Palahniukuv
otec	otec	k1gMnSc1	otec
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
její	její	k3xOp3gFnSc3	její
představě	představa	k1gFnSc3	představa
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
byl	být	k5eAaImAgInS	být
Shackleford	Shackleford	k1gInSc1	Shackleford
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
sledoval	sledovat	k5eAaImAgMnS	sledovat
Palahniuka	Palahniuk	k1gMnSc4	Palahniuk
staršího	starší	k1gMnSc4	starší
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
domů	dům	k1gInPc2	dům
v	v	k7c4	v
Kendrick	Kendrick	k1gInSc4	Kendrick
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc4	Ida
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
a	a	k8xC	a
těla	tělo	k1gNnPc4	tělo
zatáhl	zatáhnout	k5eAaPmAgMnS	zatáhnout
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
posléze	posléze	k6eAd1	posléze
zapálil	zapálit	k5eAaPmAgInS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Shackleford	Shackleford	k1gMnSc1	Shackleford
shledán	shledat	k5eAaPmNgMnS	shledat
vinným	vinný	k1gMnSc7	vinný
za	za	k7c4	za
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
vraždu	vražda	k1gFnSc4	vražda
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
začal	začít	k5eAaPmAgInS	začít
Palahniuk	Palahniuk	k1gInSc1	Palahniuk
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
románu	román	k1gInSc6	román
Ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
knihu	kniha	k1gFnSc4	kniha
psal	psát	k5eAaImAgMnS	psát
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
pomohla	pomoct	k5eAaPmAgFnS	pomoct
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
Shackleforda	Shackleford	k1gMnSc4	Shackleford
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Karen	Karen	k1gInSc4	Karen
Valby	valba	k1gFnSc2	valba
rozhovor	rozhovor	k1gInSc4	rozhovor
pro	pro	k7c4	pro
Entertainment	Entertainment	k1gInSc4	Entertainment
Weekly	Weekl	k1gInPc1	Weekl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
rozhovoru	rozhovor	k1gInSc2	rozhovor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
důvěrně	důvěrně	k6eAd1	důvěrně
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
partnerovi	partner	k1gMnSc6	partner
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
domnívalo	domnívat	k5eAaImAgNnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgInSc1d1	ženatý
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
novináři	novinář	k1gMnPc1	novinář
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
ženu	žena	k1gFnSc4	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karen	Karen	k1gInSc1	Karen
Valby	valba	k1gFnSc2	valba
přidá	přidat	k5eAaPmIp3nS	přidat
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
článku	článek	k1gInSc2	článek
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gInSc2	jeho
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odpovědi	odpověď	k1gFnSc6	odpověď
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
stránky	stránka	k1gFnPc4	stránka
audio	audio	k2eAgFnSc4d1	audio
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
v	v	k7c6	v
roznícení	roznícení	k1gNnSc6	roznícení
odhalil	odhalit	k5eAaPmAgInS	odhalit
nejen	nejen	k6eAd1	nejen
svou	svůj	k3xOyFgFnSc4	svůj
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
urážel	urážet	k5eAaImAgMnS	urážet
reportérku	reportérka	k1gFnSc4	reportérka
a	a	k8xC	a
člena	člen	k1gMnSc4	člen
její	její	k3xOp3gFnSc2	její
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnPc1	jeho
obavy	obava	k1gFnPc1	obava
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
neopodstatněné	opodstatněný	k2eNgFnPc1d1	neopodstatněná
a	a	k8xC	a
článek	článek	k1gInSc1	článek
neuváděl	uvádět	k5eNaImAgInS	uvádět
žádné	žádný	k3yNgFnPc4	žádný
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
ze	z	k7c2	z
stránky	stránka	k1gFnSc2	stránka
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
některé	některý	k3yIgMnPc4	některý
fanoušky	fanoušek	k1gMnPc4	fanoušek
k	k	k7c3	k
pocitu	pocit	k1gInSc3	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stydí	stydět	k5eAaImIp3nP	stydět
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Dennise	Dennise	k1gFnSc2	Dennise
Widmeyera	Widmeyero	k1gNnSc2	Widmeyero
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
jeho	jeho	k3xOp3gFnPc2	jeho
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
nahrávka	nahrávka	k1gFnSc1	nahrávka
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
kvůli	kvůli	k7c3	kvůli
výrokům	výrok	k1gInPc3	výrok
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
orientaci	orientace	k1gFnSc6	orientace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
výrokům	výrok	k1gInPc3	výrok
o	o	k7c4	o
Karen	Karen	k1gInSc4	Karen
Valby	valba	k1gFnSc2	valba
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
přidal	přidat	k5eAaPmAgMnS	přidat
novou	nový	k2eAgFnSc4d1	nová
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žádá	žádat	k5eAaImIp3nS	žádat
fanoušky	fanoušek	k1gMnPc4	fanoušek
aby	aby	k9	aby
byli	být	k5eAaImAgMnP	být
shovívaví	shovívavý	k2eAgMnPc1d1	shovívavý
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
událostem	událost	k1gFnPc3	událost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
aby	aby	k9	aby
předchozí	předchozí	k2eAgFnSc4d1	předchozí
nahrávku	nahrávka	k1gFnSc4	nahrávka
nezveřejnil	zveřejnit	k5eNaPmAgMnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Palahniuk	Palahniuk	k6eAd1	Palahniuk
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
otevřeně	otevřeně	k6eAd1	otevřeně
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
homosexualitě	homosexualita	k1gFnSc3	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
partnerem	partner	k1gMnSc7	partner
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
rozhovoru	rozhovor	k1gInSc2	rozhovor
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Advocate	Advocat	k1gInSc5	Advocat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
v	v	k7c6	v
"	"	kIx"	"
<g/>
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
církevní	církevní	k2eAgFnSc6d1	církevní
čtvrti	čtvrt	k1gFnSc6	čtvrt
před	před	k7c7	před
Vancouverem	Vancouvero	k1gNnSc7	Vancouvero
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Na	na	k7c6	na
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
k	k	k7c3	k
románu	román	k1gInSc3	román
Diary	Diara	k1gFnSc2	Diara
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Deník	deník	k1gInSc1	deník
<g/>
)	)	kIx)	)
předčítal	předčítat	k5eAaImAgMnS	předčítat
divákům	divák	k1gMnPc3	divák
povídku	povídka	k1gFnSc4	povídka
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
Guts	Guts	k1gInSc1	Guts
(	(	kIx(	(
<g/>
Vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
nehodách	nehoda	k1gFnPc6	nehoda
při	při	k7c6	při
masturbaci	masturbace	k1gFnSc6	masturbace
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
románu	román	k1gInSc6	román
Haunted	Haunted	k1gInSc1	Haunted
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Strašidla	strašidlo	k1gNnSc2	strašidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
čtení	čtení	k1gNnSc2	čtení
omdlelo	omdlet	k5eAaPmAgNnS	omdlet
již	již	k6eAd1	již
40	[number]	k4	40
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Playboy	playboy	k1gMnSc1	playboy
v	v	k7c6	v
březnovém	březnový	k2eAgNnSc6d1	březnové
čísle	číslo	k1gNnSc6	číslo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
;	;	kIx,	;
Palahniuk	Palahniuk	k1gInSc1	Palahniuk
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
povídku	povídka	k1gFnSc4	povídka
k	k	k7c3	k
publikaci	publikace	k1gFnSc3	publikace
společně	společně	k6eAd1	společně
s	s	k7c7	s
Guts	Guts	k1gInSc1	Guts
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydavatelé	vydavatel	k1gMnPc1	vydavatel
ji	on	k3xPp3gFnSc4	on
shledali	shledat	k5eAaPmAgMnP	shledat
příliš	příliš	k6eAd1	příliš
pobuřující	pobuřující	k2eAgMnPc1d1	pobuřující
a	a	k8xC	a
nevydali	vydat	k5eNaPmAgMnP	vydat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
při	při	k7c6	při
propagaci	propagace	k1gFnSc6	propagace
Stranger	Strangra	k1gFnPc2	Strangra
Than	Thana	k1gFnPc2	Thana
Fiction	Fiction	k1gInSc1	Fiction
<g/>
:	:	kIx,	:
True	True	k1gInSc1	True
Stories	Stories	k1gInSc1	Stories
(	(	kIx(	(
<g/>
Podivnější	podivný	k2eAgFnSc1d2	podivnější
než	než	k8xS	než
fikce	fikce	k1gFnSc1	fikce
<g/>
:	:	kIx,	:
pravdivé	pravdivý	k2eAgInPc1d1	pravdivý
příběhy	příběh	k1gInPc1	příběh
<g/>
)	)	kIx)	)
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
povídku	povídka	k1gFnSc4	povídka
předčítal	předčítat	k5eAaImAgInS	předčítat
divákům	divák	k1gMnPc3	divák
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
počet	počet	k1gInSc1	počet
omdlených	omdlený	k2eAgInPc2d1	omdlený
se	se	k3xPyFc4	se
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
53	[number]	k4	53
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
až	až	k9	až
k	k	k7c3	k
60	[number]	k4	60
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
Diary	Diara	k1gFnPc4	Diara
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
propagací	propagace	k1gFnSc7	propagace
románu	román	k1gInSc2	román
Haunted	Haunted	k1gInSc1	Haunted
a	a	k8xC	a
také	také	k9	také
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
se	s	k7c7	s
čtením	čtení	k1gNnSc7	čtení
povídky	povídka	k1gFnSc2	povídka
Guts	Gutsa	k1gFnPc2	Gutsa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnPc6	jeho
dalších	další	k2eAgFnPc6d1	další
čtení	čtení	k1gNnSc3	čtení
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
v	v	k7c6	v
Boulderu	Boulder	k1gInSc6	Boulder
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
Palahniuk	Palahniuk	k1gMnSc1	Palahniuk
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
doposud	doposud	k6eAd1	doposud
omdleli	omdlet	k5eAaPmAgMnP	omdlet
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
povídky	povídka	k1gFnSc2	povídka
Guts	Gutsa	k1gFnPc2	Gutsa
čítá	čítat	k5eAaImIp3nS	čítat
již	již	k6eAd1	již
68	[number]	k4	68
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
zatím	zatím	k6eAd1	zatím
omdlel	omdlet	k5eAaPmAgMnS	omdlet
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
ve	v	k7c6	v
Victorii	Victorie	k1gFnSc6	Victorie
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
omdlelo	omdlet	k5eAaPmAgNnS	omdlet
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc1	pět
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
opustit	opustit	k5eAaPmF	opustit
posluchárnu	posluchárna	k1gFnSc4	posluchárna
upadl	upadnout	k5eAaPmAgMnS	upadnout
u	u	k7c2	u
východu	východ	k1gInSc2	východ
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uhodil	uhodit	k5eAaPmAgMnS	uhodit
hlavou	hlava	k1gFnSc7	hlava
o	o	k7c4	o
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Jemu	on	k3xPp3gMnSc3	on
samotnému	samotný	k2eAgNnSc3d1	samotné
zjevně	zjevně	k6eAd1	zjevně
tyto	tento	k3xDgInPc1	tento
incidenty	incident	k1gInPc1	incident
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
nevadí	vadit	k5eNaImIp3nS	vadit
a	a	k8xC	a
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
fanoušci	fanoušek	k1gMnPc1	fanoušek
stále	stále	k6eAd1	stále
čtou	číst	k5eAaImIp3nP	číst
povídku	povídka	k1gFnSc4	povídka
Guts	Gutsa	k1gFnPc2	Gutsa
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
ostatní	ostatní	k2eAgNnPc1d1	ostatní
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
také	také	k9	také
objevily	objevit	k5eAaPmAgInP	objevit
zvukové	zvukový	k2eAgInPc4d1	zvukový
záznamy	záznam	k1gInPc4	záznam
z	z	k7c2	z
čtení	čtení	k1gNnSc2	čtení
povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doslovu	doslov	k1gInSc6	doslov
k	k	k7c3	k
poslednímu	poslední	k2eAgNnSc3d1	poslední
vydání	vydání	k1gNnSc3	vydání
Haunted	Haunted	k1gMnSc1	Haunted
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povídka	povídka	k1gFnSc1	povídka
Guts	Gutsa	k1gFnPc2	Gutsa
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
73	[number]	k4	73
omdlení	omdlení	k1gNnPc2	omdlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
románu	román	k1gInSc3	román
Haunted	Haunted	k1gInSc1	Haunted
<g/>
,	,	kIx,	,
Palahniuk	Palahniuk	k1gMnSc1	Palahniuk
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
posledním	poslední	k2eAgMnSc6d1	poslední
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
"	"	kIx"	"
<g/>
horrorové	horrorový	k2eAgFnPc4d1	horrorová
trilogie	trilogie	k1gFnPc4	trilogie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Lullaby	Lullab	k1gInPc7	Lullab
(	(	kIx(	(
<g/>
Ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Diary	Diara	k1gFnSc2	Diara
(	(	kIx(	(
<g/>
Deník	deník	k1gInSc1	deník
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
následující	následující	k2eAgFnSc1d1	následující
kniha	kniha	k1gFnSc1	kniha
Rant	Ranta	k1gFnPc2	Ranta
bude	být	k5eAaImBp3nS	být
první	první	k4xOgMnSc1	první
z	z	k7c2	z
"	"	kIx"	"
<g/>
sci	sci	k?	sci
<g/>
–	–	k?	–
<g/>
fi	fi	k0	fi
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
instruktora	instruktor	k1gMnSc2	instruktor
pro	pro	k7c4	pro
Clarion	Clarion	k1gInSc4	Clarion
West	Westa	k1gFnPc2	Westa
Writers	Writersa	k1gFnPc2	Writersa
Workshop	workshop	k1gInSc1	workshop
a	a	k8xC	a
strávil	strávit	k5eAaPmAgInS	strávit
týden	týden	k1gInSc1	týden
vyučováním	vyučování	k1gNnSc7	vyučování
svých	svůj	k3xOyFgInPc2	svůj
postupů	postup	k1gInPc2	postup
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
literární	literární	k2eAgFnSc2d1	literární
fikce	fikce	k1gFnSc2	fikce
osmnácti	osmnáct	k4xCc3	osmnáct
studenům	studeno	k1gNnPc3	studeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Názory	názor	k1gInPc1	názor
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vlivy	vliv	k1gInPc4	vliv
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
příjmení	příjmení	k1gNnSc1	příjmení
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
chybně	chybně	k6eAd1	chybně
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
tvrzení	tvrzení	k1gNnSc2	tvrzení
jeho	on	k3xPp3gInSc2	on
samotného	samotný	k2eAgInSc2d1	samotný
je	být	k5eAaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
pólanyk	pólanyk	k6eAd1	pólanyk
<g/>
]	]	kIx)	]
podle	podle	k7c2	podle
křestních	křestní	k2eAgNnPc2d1	křestní
jmen	jméno	k1gNnPc2	jméno
Paula	Paul	k1gMnSc2	Paul
a	a	k8xC	a
Nick	Nick	k1gInSc4	Nick
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jména	jméno	k1gNnSc2	jméno
jeho	jeho	k3xOp3gMnPc2	jeho
prarodičů	prarodič	k1gMnPc2	prarodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
také	také	k9	také
složili	složit	k5eAaPmAgMnP	složit
současnou	současný	k2eAgFnSc4d1	současná
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
rodiny	rodina	k1gFnSc2	rodina
Palahniuků	Palahniuk	k1gMnPc2	Palahniuk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
patrně	patrně	k6eAd1	patrně
o	o	k7c4	o
podivně	podivně	k6eAd1	podivně
zapsanou	zapsaný	k2eAgFnSc4d1	zapsaná
zkomoleninu	zkomolenina	k1gFnSc4	zkomolenina
slovanského	slovanský	k2eAgNnSc2d1	slovanské
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
vyslovovaného	vyslovovaný	k2eAgInSc2d1	vyslovovaný
původně	původně	k6eAd1	původně
snad	snad	k9	snad
[	[	kIx(	[
<g/>
palahňůk	palahňůk	k1gInSc1	palahňůk
|	|	kIx~	|
<g/>
п	п	k?	п
<g/>
'	'	kIx"	'
<g/>
у	у	k?	у
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
i	i	k8xC	i
neanglicky	anglicky	k6eNd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
nepříliš	příliš	k6eNd1	příliš
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
romány	román	k1gInPc1	román
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
temné	temný	k2eAgFnPc1d1	temná
a	a	k8xC	a
depresivní	depresivní	k2eAgFnPc1d1	depresivní
<g/>
.	.	kIx.	.
</s>
<s>
Odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
velkoměstě	velkoměsto	k1gNnSc6	velkoměsto
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
Lullaby	Lullaba	k1gFnPc4	Lullaba
<g/>
)	)	kIx)	)
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
nadpřirozené	nadpřirozený	k2eAgInPc4d1	nadpřirozený
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
hrdina	hrdina	k1gMnSc1	hrdina
má	mít	k5eAaImIp3nS	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
psychickou	psychický	k2eAgFnSc4d1	psychická
poruchu	porucha	k1gFnSc4	porucha
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc1	komplex
či	či	k8xC	či
závislost	závislost	k1gFnSc1	závislost
(	(	kIx(	(
<g/>
častým	častý	k2eAgInSc7d1	častý
motivem	motiv	k1gInSc7	motiv
je	být	k5eAaImIp3nS	být
terapeutická	terapeutický	k2eAgFnSc1d1	terapeutická
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
o	o	k7c4	o
člověka	člověk	k1gMnSc4	člověk
bez	bez	k7c2	bez
ctižádosti	ctižádost	k1gFnSc2	ctižádost
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
smyslu	smysl	k1gInSc2	smysl
a	a	k8xC	a
náplně	náplň	k1gFnSc2	náplň
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vydělává	vydělávat	k5eAaImIp3nS	vydělávat
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
podřadnou	podřadný	k2eAgFnSc4d1	podřadná
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
placenou	placený	k2eAgFnSc7d1	placená
prací	práce	k1gFnSc7	práce
(	(	kIx(	(
<g/>
herec	herec	k1gMnSc1	herec
ve	v	k7c6	v
skanzenu	skanzen	k1gInSc6	skanzen
v	v	k7c6	v
Choke	Choke	k1gNnSc6	Choke
<g/>
,	,	kIx,	,
promítač	promítač	k1gMnSc1	promítač
a	a	k8xC	a
úředník	úředník	k1gMnSc1	úředník
v	v	k7c6	v
nejmenované	jmenovaný	k2eNgFnSc6d1	nejmenovaná
firmě	firma	k1gFnSc6	firma
ve	v	k7c4	v
Fight	Fight	k2eAgInSc4d1	Fight
Club	club	k1gInSc4	club
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
relativně	relativně	k6eAd1	relativně
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
novinář	novinář	k1gMnSc1	novinář
Streator	Streator	k1gMnSc1	Streator
a	a	k8xC	a
realitní	realitní	k2eAgFnSc1d1	realitní
agentka	agentka	k1gFnSc1	agentka
v	v	k7c4	v
Lullaby	Lullab	k1gMnPc4	Lullab
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nějaký	nějaký	k3yIgInSc4	nějaký
zcela	zcela	k6eAd1	zcela
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
charakterový	charakterový	k2eAgInSc4d1	charakterový
rys	rys	k1gInSc4	rys
<g/>
,	,	kIx,	,
zálibu	záliba	k1gFnSc4	záliba
či	či	k8xC	či
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zcela	zcela	k6eAd1	zcela
amorální	amorální	k2eAgInPc1d1	amorální
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Tender	tender	k1gInSc1	tender
Branson	Branson	k1gNnSc1	Branson
ze	z	k7c2	z
Survivor	Survivora	k1gFnPc2	Survivora
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
telefonickým	telefonický	k2eAgNnSc7d1	telefonické
doháněním	dohánění	k1gNnSc7	dohánění
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
,	,	kIx,	,
nepojmenovaný	pojmenovaný	k2eNgMnSc1d1	nepojmenovaný
hrdina	hrdina	k1gMnSc1	hrdina
z	z	k7c2	z
Fight	Fighta	k1gFnPc2	Fighta
club	club	k1gInSc1	club
jako	jako	k8xC	jako
číšník	číšník	k1gMnSc1	číšník
v	v	k7c6	v
luxusní	luxusní	k2eAgFnSc6d1	luxusní
restauraci	restaurace	k1gFnSc6	restaurace
močí	močit	k5eAaImIp3nS	močit
do	do	k7c2	do
polévky	polévka	k1gFnSc2	polévka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
románů	román	k1gInPc2	román
je	být	k5eAaImIp3nS	být
vyprávěna	vyprávět	k5eAaImNgFnS	vyprávět
v	v	k7c6	v
první	první	k4xOgFnSc6	první
osobě	osoba	k1gFnSc6	osoba
přímo	přímo	k6eAd1	přímo
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
bezútěšném	bezútěšný	k2eAgInSc6d1	bezútěšný
životě	život	k1gInSc6	život
medituje	meditovat	k5eAaImIp3nS	meditovat
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
filosofických	filosofický	k2eAgFnPc2d1	filosofická
úvah	úvaha	k1gFnPc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Viníkem	viník	k1gMnSc7	viník
osobní	osobní	k2eAgFnSc2d1	osobní
krize	krize	k1gFnSc2	krize
postavy	postava	k1gFnSc2	postava
není	být	k5eNaImIp3nS	být
většinou	většinou	k6eAd1	většinou
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
společnost	společnost	k1gFnSc1	společnost
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Romány	román	k1gInPc1	román
končí	končit	k5eAaImIp3nP	končit
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
tragicky	tragicky	k6eAd1	tragicky
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc1	žánr
anglicky	anglicky	k6eAd1	anglicky
nazýván	nazývat	k5eAaImNgInS	nazývat
transgressive	transgressivat	k5eAaPmIp3nS	transgressivat
fiction	fiction	k1gInSc1	fiction
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
příběhy	příběh	k1gInPc4	příběh
o	o	k7c4	o
překročení	překročení	k1gNnSc4	překročení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
obdoba	obdoba	k1gFnSc1	obdoba
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
romány	román	k1gInPc1	román
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
či	či	k8xC	či
onak	onak	k6eAd1	onak
inspirovány	inspirován	k2eAgInPc1d1	inspirován
osobní	osobní	k2eAgFnSc7d1	osobní
zkušeností	zkušenost	k1gFnSc7	zkušenost
autora	autor	k1gMnSc2	autor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Fight	Fight	k2eAgInSc4d1	Fight
Club	club	k1gInSc4	club
rvačkou	rvačka	k1gFnSc7	rvačka
v	v	k7c6	v
campingu	camping	k1gInSc6	camping
<g/>
,	,	kIx,	,
Lullaby	Lullaba	k1gFnPc4	Lullaba
vraždou	vražda	k1gFnSc7	vražda
autorova	autorův	k2eAgMnSc2d1	autorův
otce	otec	k1gMnSc2	otec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
základním	základní	k2eAgNnSc7d1	základní
tématem	téma	k1gNnSc7	téma
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
autorových	autorův	k2eAgFnPc2d1	autorova
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
současná	současný	k2eAgFnSc1d1	současná
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
,	,	kIx,	,
znemožňující	znemožňující	k2eAgNnSc1d1	znemožňující
prosazení	prosazení	k1gNnSc1	prosazení
individuality	individualita	k1gFnSc2	individualita
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
bránící	bránící	k2eAgNnPc4d1	bránící
dosažení	dosažení	k1gNnSc4	dosažení
smyslu	smysl	k1gInSc2	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
samozřejmě	samozřejmě	k6eAd1	samozřejmě
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k8xC	i
tématům	téma	k1gNnPc3	téma
jiným	jiný	k2eAgNnPc3d1	jiné
(	(	kIx(	(
<g/>
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
v	v	k7c4	v
Diary	Diara	k1gFnPc4	Diara
a	a	k8xC	a
Haunted	Haunted	k1gInSc4	Haunted
<g/>
,	,	kIx,	,
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
slova	slovo	k1gNnSc2	slovo
v	v	k7c4	v
Lullaby	Lullaba	k1gFnPc4	Lullaba
<g/>
,	,	kIx,	,
o	o	k7c6	o
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
medializaci	medializace	k1gFnSc6	medializace
a	a	k8xC	a
odlišnosti	odlišnost	k1gFnSc6	odlišnost
v	v	k7c4	v
Survivor	Survivor	k1gInSc4	Survivor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
často	často	k6eAd1	často
věnuje	věnovat	k5eAaPmIp3nS	věnovat
nechutným	chutný	k2eNgInPc3d1	nechutný
a	a	k8xC	a
odpudivým	odpudivý	k2eAgNnPc3d1	odpudivé
tématům	téma	k1gNnPc3	téma
(	(	kIx(	(
<g/>
senilita	senilita	k1gFnSc1	senilita
v	v	k7c6	v
Choke	Choke	k1gFnSc6	Choke
<g/>
,	,	kIx,	,
nekrofilie	nekrofilie	k1gFnSc1	nekrofilie
v	v	k7c4	v
Lullaby	Lullab	k1gInPc4	Lullab
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
nevyhýbá	vyhýbat	k5eNaImIp3nS	vyhýbat
jejich	jejich	k3xOp3gInPc3	jejich
detailním	detailní	k2eAgInPc3d1	detailní
popisům	popis	k1gInPc3	popis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
romány	román	k1gInPc1	román
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pociťovány	pociťovat	k5eAaImNgInP	pociťovat
jako	jako	k9	jako
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
literární	literární	k2eAgInSc1d1	literární
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
samým	samý	k3xTgInSc7	samý
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
jako	jako	k8xS	jako
minimalistický	minimalistický	k2eAgInSc1d1	minimalistický
<g/>
,	,	kIx,	,
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgInPc1d1	krátký
a	a	k8xC	a
jednoduše	jednoduše	k6eAd1	jednoduše
členěné	členěný	k2eAgFnPc4d1	členěná
věty	věta	k1gFnPc4	věta
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
ale	ale	k9	ale
mnoho	mnoho	k4c1	mnoho
metafor	metafora	k1gFnPc2	metafora
či	či	k8xC	či
metaforických	metaforický	k2eAgFnPc2d1	metaforická
paralel	paralela	k1gFnPc2	paralela
k	k	k7c3	k
textu	text	k1gInSc3	text
(	(	kIx(	(
<g/>
jakýchsi	jakýsi	k3yIgFnPc2	jakýsi
"	"	kIx"	"
<g/>
názorných	názorný	k2eAgInPc2d1	názorný
příkladů	příklad	k1gInPc2	příklad
<g/>
"	"	kIx"	"
k	k	k7c3	k
tezím	teze	k1gFnPc3	teze
obsaženým	obsažený	k2eAgFnPc3d1	obsažená
v	v	k7c6	v
románu	román	k1gInSc6	román
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
celkovou	celkový	k2eAgFnSc4d1	celková
depresivitu	depresivita	k1gFnSc4	depresivita
a	a	k8xC	a
tragičnost	tragičnost	k1gFnSc4	tragičnost
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
velmi	velmi	k6eAd1	velmi
cynickým	cynický	k2eAgInSc7d1	cynický
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
Choke	Choke	k1gFnSc6	Choke
-	-	kIx~	-
týká	týkat	k5eAaImIp3nS	týkat
se	s	k7c7	s
sanatoria	sanatorium	k1gNnSc2	sanatorium
pro	pro	k7c4	pro
senilní	senilní	k2eAgFnSc4d1	senilní
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Voní	vonět	k5eAaImIp3nS	vonět
to	ten	k3xDgNnSc1	ten
tu	tu	k6eAd1	tu
čistotou	čistota	k1gFnSc7	čistota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
neznamená	znamenat	k5eNaImIp3nS	znamenat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
čucháte	čuchat	k5eAaImIp2nP	čuchat
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
,	,	kIx,	,
čisticí	čisticí	k2eAgInPc4d1	čisticí
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
voňavky	voňavka	k1gFnPc1	voňavka
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
vám	vy	k3xPp2nPc3	vy
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc4d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůně	vůně	k1gFnSc1	vůně
borové	borový	k2eAgFnSc2d1	Borová
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
překrývá	překrývat	k5eAaImIp3nS	překrývat
nějaké	nějaký	k3yIgFnPc4	nějaký
sračky	sračka	k1gFnPc4	sračka
<g/>
.	.	kIx.	.
</s>
<s>
Citron	citron	k1gInSc1	citron
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
pozvracel	pozvracet	k5eAaPmAgMnS	pozvracet
<g/>
.	.	kIx.	.
</s>
<s>
Růže	růže	k1gFnSc1	růže
rovná	rovný	k2eAgFnSc1d1	rovná
se	se	k3xPyFc4	se
moč	moč	k1gFnSc1	moč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odpoledni	odpoledne	k1gNnSc6	odpoledne
u	u	k7c2	u
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
nebudete	být	k5eNaImBp2nP	být
chtít	chtít	k5eAaImF	chtít
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
životě	život	k1gInSc6	život
znovu	znovu	k6eAd1	znovu
přivonět	přivonět	k5eAaPmF	přivonět
k	k	k7c3	k
růži	růž	k1gFnSc3	růž
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
refrénu	refrén	k1gInSc2	refrén
-	-	kIx~	-
úsloví	úsloví	k1gNnSc1	úsloví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
románu	román	k1gInSc6	román
opakuje	opakovat	k5eAaImIp3nS	opakovat
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
významu	význam	k1gInSc6	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
románu	román	k1gInSc6	román
Choke	Chok	k1gFnSc2	Chok
jsou	být	k5eAaImIp3nP	být
refrénem	refrén	k1gInSc7	refrén
například	například	k6eAd1	například
věty	věta	k1gFnSc2	věta
"	"	kIx"	"
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Co	co	k9	co
by	by	kYmCp3nS	by
Ježíš	Ježíš	k1gMnSc1	Ježíš
rozhodně	rozhodně	k6eAd1	rozhodně
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
románu	román	k1gInSc6	román
Lullaby	Lullaba	k1gFnSc2	Lullaba
například	například	k6eAd1	například
různé	různý	k2eAgFnPc4d1	různá
parafráze	parafráze	k1gFnPc4	parafráze
anglické	anglický	k2eAgFnSc2d1	anglická
říkanky	říkanka	k1gFnSc2	říkanka
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
a	a	k8xC	a
používáním	používání	k1gNnSc7	používání
různých	různý	k2eAgFnPc2d1	různá
kuriózních	kuriózní	k2eAgFnPc2d1	kuriózní
znalostí	znalost	k1gFnPc2	znalost
(	(	kIx(	(
<g/>
v	v	k7c6	v
románu	román	k1gInSc6	román
Fight	Fight	k2eAgInSc4d1	Fight
Club	club	k1gInSc4	club
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
návody	návod	k1gInPc4	návod
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
bomb	bomba	k1gFnPc2	bomba
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
triky	trik	k1gInPc1	trik
promítačů	promítač	k1gMnPc2	promítač
<g/>
,	,	kIx,	,
v	v	k7c6	v
románu	román	k1gInSc6	román
Choke	Chok	k1gFnSc2	Chok
praktiky	praktika	k1gFnSc2	praktika
v	v	k7c6	v
domovech	domov	k1gInPc6	domov
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
,	,	kIx,	,
nemocnicích	nemocnice	k1gFnPc6	nemocnice
a	a	k8xC	a
terapeutických	terapeutický	k2eAgFnPc6d1	terapeutická
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
urban	urban	k1gInSc1	urban
legends	legends	k1gInSc1	legends
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
v	v	k7c6	v
románu	román	k1gInSc6	román
Diary	Diara	k1gFnSc2	Diara
anatomie	anatomie	k1gFnSc2	anatomie
lidského	lidský	k2eAgInSc2d1	lidský
obličeje	obličej	k1gInSc2	obličej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
autor	autor	k1gMnSc1	autor
získal	získat	k5eAaPmAgMnS	získat
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
neobvyklého	obvyklý	k2eNgInSc2d1	neobvyklý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Insomnia	Insomnium	k1gNnPc4	Insomnium
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Nespavost	nespavost	k1gFnSc1	nespavost
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nepublikováno	publikován	k2eNgNnSc4d1	nepublikováno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Invisible	Invisible	k6eAd1	Invisible
Monsters	Monsters	k1gInSc1	Monsters
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Neviditelné	viditelný	k2eNgFnSc2d1	neviditelná
nestvůry	nestvůra	k1gFnSc2	nestvůra
<g/>
,	,	kIx,	,
napsáno	napsán	k2eAgNnSc1d1	napsáno
před	před	k7c4	před
Fight	Fight	k1gInSc4	Fight
Clubem	club	k1gInSc7	club
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
přepracované	přepracovaný	k2eAgFnSc6d1	přepracovaná
verzi	verze	k1gFnSc6	verze
publikováno	publikovat	k5eAaBmNgNnS	publikovat
až	až	k9	až
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fight	Fight	k2eAgInSc1d1	Fight
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Bojový	bojový	k2eAgInSc1d1	bojový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
autorův	autorův	k2eAgInSc4d1	autorův
první	první	k4xOgInSc4	první
vydaný	vydaný	k2eAgInSc4d1	vydaný
<g/>
,	,	kIx,	,
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
i	i	k9	i
umělecky	umělecky	k6eAd1	umělecky
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
román	román	k1gInSc1	román
</s>
</p>
<p>
<s>
Survivor	Survivor	k1gInSc1	Survivor
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Přeživší	přeživší	k2eAgFnSc1d1	přeživší
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Choke	Choke	k1gFnSc1	Choke
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Dušení	dušení	k1gNnSc1	dušení
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lullaby	Lullaba	k1gFnPc1	Lullaba
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diary	Diara	k1gFnPc1	Diara
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fugitives	Fugitives	k1gMnSc1	Fugitives
and	and	k?	and
Refugees	Refugees	k1gMnSc1	Refugees
<g/>
:	:	kIx,	:
A	A	kA	A
Walk	Walk	k1gInSc1	Walk
in	in	k?	in
Portland	Portland	k1gInSc1	Portland
<g/>
,	,	kIx,	,
Oregon	Oregon	k1gInSc1	Oregon
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Utečenci	utečenec	k1gMnPc1	utečenec
a	a	k8xC	a
uprchlíci	uprchlík	k1gMnPc1	uprchlík
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
Portlandu	Portland	k1gInSc6	Portland
<g/>
,	,	kIx,	,
Oregon	Oregon	k1gNnSc1	Oregon
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
cestopis	cestopis	k1gInSc1	cestopis
</s>
</p>
<p>
<s>
Stranger	Stranger	k1gInSc1	Stranger
Than	Than	k1gNnSc1	Than
Fiction	Fiction	k1gInSc1	Fiction
<g/>
:	:	kIx,	:
True	True	k1gNnSc1	True
Stories	Storiesa	k1gFnPc2	Storiesa
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Podivnější	podivný	k2eAgFnSc1d2	podivnější
než	než	k8xS	než
fikce	fikce	k1gFnSc1	fikce
<g/>
:	:	kIx,	:
pravdivé	pravdivý	k2eAgInPc1d1	pravdivý
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
esejů	esej	k1gInPc2	esej
</s>
</p>
<p>
<s>
Haunted	Haunted	k1gInSc1	Haunted
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
obcházeni	obcházen	k2eAgMnPc1d1	obcházen
strašidly	strašidlo	k1gNnPc7	strašidlo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
horrorových	horrorův	k2eAgFnPc2d1	horrorův
povídek	povídka	k1gFnPc2	povídka
spojených	spojený	k2eAgFnPc2d1	spojená
rámcovou	rámcový	k2eAgFnSc7d1	rámcová
novelou	novela	k1gFnSc7	novela
</s>
</p>
<p>
<s>
Rant	Rant	k1gInSc1	Rant
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
titulní	titulní	k2eAgFnSc2d1	titulní
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
biografie	biografie	k1gFnSc1	biografie
sériového	sériový	k2eAgMnSc2d1	sériový
vraha	vrah	k1gMnSc2	vrah
</s>
</p>
<p>
<s>
Snuff	Snuff	k1gMnSc1	Snuff
(	(	kIx(	(
<g/>
Snuff	Snuff	k1gMnSc1	Snuff
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
pornografie	pornografie	k1gFnSc2	pornografie
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
společnosti	společnost	k1gFnSc6	společnost
</s>
</p>
<p>
<s>
Pygmy	Pygma	k1gFnPc1	Pygma
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Zakrslík	zakrslík	k1gInSc1	zakrslík
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
o	o	k7c6	o
komunistickém	komunistický	k2eAgInSc6d1	komunistický
dětském	dětský	k2eAgInSc6d1	dětský
agentovi	agentův	k2eAgMnPc1d1	agentův
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
rodině	rodina	k1gFnSc6	rodina
</s>
</p>
<p>
<s>
Tell-All	Tell-All	k1gInSc1	Tell-All
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
Životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
detektivka	detektivka	k1gFnSc1	detektivka
ze	z	k7c2	z
zlatých	zlatý	k2eAgInPc2d1	zlatý
časů	čas	k1gInPc2	čas
Hollywoodu	Hollywood	k1gInSc2	Hollywood
</s>
</p>
<p>
<s>
Damned	Damned	k1gInSc1	Damned
(	(	kIx(	(
<g/>
Prokletí	prokletí	k1gNnSc1	prokletí
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
-	-	kIx~	-
satirický	satirický	k2eAgInSc1d1	satirický
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
životě	život	k1gInSc6	život
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
</s>
</p>
<p>
<s>
Doomed	Doomed	k1gInSc1	Doomed
(	(	kIx(	(
<g/>
Zatracení	zatracení	k1gNnSc1	zatracení
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beautiful	Beautiful	k1gInSc1	Beautiful
You	You	k1gFnSc2	You
(	(	kIx(	(
<g/>
Tvé	tvůj	k3xOp2gNnSc1	tvůj
překrásné	překrásný	k2eAgNnSc1d1	překrásné
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Make	Make	k6eAd1	Make
Something	Something	k1gInSc1	Something
Up	Up	k1gFnSc2	Up
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
</s>
</p>
<p>
<s>
Fight	Fight	k2eAgInSc1d1	Fight
Club	club	k1gInSc1	club
2	[number]	k4	2
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
-	-	kIx~	-
komiks	komiks	k1gInSc1	komiks
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Cameronem	Cameron	k1gMnSc7	Cameron
Stewartem	Stewart	k1gMnSc7	Stewart
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bait	Bait	k1gInSc1	Bait
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
povídková	povídkový	k2eAgFnSc1d1	povídková
sbírka	sbírka	k1gFnSc1	sbírka
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
omalovánek	omalovánka	k1gFnPc2	omalovánka
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
rváčů	rváč	k1gMnPc2	rváč
(	(	kIx(	(
<g/>
Fight	Fight	k2eAgInSc1d1	Fight
Club	club	k1gInSc1	club
<g/>
)	)	kIx)	)
-	-	kIx~	-
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Manďák	Manďák	k1gMnSc1	Manďák
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
román	román	k1gInSc4	román
totéž	týž	k3xTgNnSc1	týž
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgInS	vydat
román	román	k1gInSc1	román
Odeon	odeon	k1gInSc1	odeon
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Richarda	Richarda	k1gFnSc1	Richarda
Podaného	podaný	k2eAgInSc2d1	podaný
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
pro	pro	k7c4	pro
přeživší	přeživší	k2eAgInSc4d1	přeživší
(	(	kIx(	(
<g/>
Survivor	Survivor	k1gInSc4	Survivor
<g/>
)	)	kIx)	)
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Zalknutí	zalknutí	k1gNnSc1	zalknutí
(	(	kIx(	(
<g/>
Choke	Choke	k1gFnSc1	Choke
<g/>
)	)	kIx)	)
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
(	(	kIx(	(
<g/>
Lullaby	Lullab	k1gInPc1	Lullab
<g/>
)	)	kIx)	)
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Deník	deník	k1gInSc1	deník
(	(	kIx(	(
<g/>
Diary	Diara	k1gFnPc1	Diara
<g/>
)	)	kIx)	)
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Strašidla	strašidlo	k1gNnPc1	strašidlo
(	(	kIx(	(
<g/>
Haunted	Haunted	k1gInSc1	Haunted
<g/>
)	)	kIx)	)
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Snuff	Snuff	k1gInSc1	Snuff
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Neviditelné	viditelný	k2eNgFnPc1d1	neviditelná
nestvůry	nestvůra	k1gFnPc1	nestvůra
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Pygmej	Pygmej	k1gMnSc1	Pygmej
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Prokletí	prokletí	k1gNnSc1	prokletí
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
Zatracení	zatracení	k1gNnSc1	zatracení
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-207-1580-7	[number]	k4	978-80-207-1580-7
</s>
</p>
<p>
<s>
Tvé	tvůj	k3xOp2gNnSc1	tvůj
překrásné	překrásný	k2eAgNnSc1d1	překrásné
já	já	k3xPp1nSc1	já
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chuck	Chucka	k1gFnPc2	Chucka
Palahniuk	Palahniuko	k1gNnPc2	Palahniuko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Chuck	Chucko	k1gNnPc2	Chucko
Palahniuk	Palahniuko	k1gNnPc2	Palahniuko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Chuck	Chuck	k1gMnSc1	Chuck
Palahniuk	Palahniuk	k1gMnSc1	Palahniuk
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
autora	autor	k1gMnSc2	autor
na	na	k7c6	na
iLiteratura	iLiteratura	k1gFnSc1	iLiteratura
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
1.9	[number]	k4	1.9
<g/>
.2014	.2014	k4	.2014
<g/>
.	.	kIx.	.
</s>
</p>
