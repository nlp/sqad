<s>
Žížala	žížala	k1gFnSc1
obecná	obecná	k1gFnSc1
</s>
<s>
Žížala	žížala	k1gFnSc1
obecná	obecná	k1gFnSc1
Žížala	žížala	k1gFnSc1
obecná	obecná	k1gFnSc1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
kroužkovci	kroužkovec	k1gMnPc1
(	(	kIx(
<g/>
Annelida	Annelida	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
opaskovci	opaskovec	k1gMnPc1
(	(	kIx(
<g/>
Clitellata	Clitelle	k1gNnPc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
máloštětinatci	máloštětinatec	k1gMnPc1
(	(	kIx(
<g/>
Oligochaeta	Oligochaeta	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
žížaly	žížala	k1gFnPc1
(	(	kIx(
<g/>
Opisthopora	Opisthopora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
žížalovití	žížalovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Lumbricidae	Lumbricidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
žížala	žížala	k1gFnSc1
(	(	kIx(
<g/>
Lumbricus	Lumbricus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Lumbricus	Lumbricus	k1gMnSc1
terrestrisLinné	terrestrisLinný	k2eAgFnSc2d1
<g/>
,	,	kIx,
1758	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Žížala	žížala	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
lidově	lidově	k6eAd1
řečeno	říct	k5eAaPmNgNnS
dešťovka	dešťovka	k1gFnSc1
či	či	k8xC
rousnice	rousnice	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Lumbricus	Lumbricus	k1gInSc1
terrestris	terrestris	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prvoústý	prvoústý	k2eAgMnSc1d1
článkovaný	článkovaný	k2eAgMnSc1d1
živočich	živočich	k1gMnSc1
s	s	k7c7
coelomovou	coelomový	k2eAgFnSc7d1
dutinou	dutina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
kmene	kmen	k1gInSc2
kroužkovců	kroužkovec	k1gMnPc2
<g/>
,	,	kIx,
podkmene	podkmen	k1gInSc2
opaskovců	opaskovec	k1gInPc2
a	a	k8xC
třídy	třída	k1gFnSc2
máloštětinatců	máloštětinatec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
zásaditých	zásaditý	k2eAgFnPc6d1
nebo	nebo	k8xC
neutrálních	neutrální	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
<g/>
,	,	kIx,
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
už	už	k6eAd1
v	v	k7c6
kyselých	kyselý	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
rašelinišť	rašeliniště	k1gNnPc2
Eurasie	Eurasie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dorůstá	dorůstat	k5eAaImIp3nS
délky	délka	k1gFnPc4
přibližně	přibližně	k6eAd1
9	#num#	k4
až	až	k9
30	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnPc1
příbuzní	příbuzný	k1gMnPc1
však	však	k9
mohou	moct	k5eAaImIp3nP
dosahovat	dosahovat	k5eAaImF
mnohem	mnohem	k6eAd1
větších	veliký	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ekosystému	ekosystém	k1gInSc6
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
pro	pro	k7c4
hnůj	hnůj	k1gInSc4
a	a	k8xC
humus	humus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Anatomie	anatomie	k1gFnSc1
</s>
<s>
Tělo	tělo	k1gNnSc1
žížaly	žížala	k1gFnSc2
je	být	k5eAaImIp3nS
stejnocenně	stejnocenně	k6eAd1
článkované	článkovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
články	článek	k1gInPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
odděleny	oddělit	k5eAaPmNgInP
vazivovými	vazivový	k2eAgFnPc7d1
přepážkami	přepážka	k1gFnPc7
</s>
<s>
Tělo	tělo	k1gInSc1
žížaly	žížala	k1gFnSc2
má	mít	k5eAaImIp3nS
červovitý	červovitý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
příčném	příčný	k2eAgInSc6d1
řezu	řez	k1gInSc6
je	být	k5eAaImIp3nS
kruhovité	kruhovitý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
přední	přední	k2eAgFnSc4d1
část	část	k1gFnSc4
je	být	k5eAaImIp3nS
užší	úzký	k2eAgNnSc1d2
a	a	k8xC
vybíhá	vybíhat	k5eAaImIp3nS
v	v	k7c4
hmatový	hmatový	k2eAgInSc4d1
prstík	prstík	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
napomáhá	napomáhat	k5eAaImIp3nS,k5eAaBmIp3nS
při	při	k7c6
orientaci	orientace	k1gFnSc6
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
zaoblená	zaoblený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
hřbetní	hřbetní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
vyklenutější	vyklenutý	k2eAgFnSc1d2
a	a	k8xC
tmavší	tmavý	k2eAgFnSc1d2
<g/>
,	,	kIx,
prosvítá	prosvítat	k5eAaImIp3nS
zde	zde	k6eAd1
hřbetní	hřbetní	k2eAgFnSc1d1
céva	céva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
homonomně	homonomně	k6eAd1
segmentované	segmentovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
kromě	kromě	k7c2
předústní	předústní	k2eAgFnSc2d1
a	a	k8xC
pygidiální	pygidiální	k2eAgFnSc2d1
části	část	k1gFnSc2
(	(	kIx(
<g/>
konec	konec	k1gInSc1
těla	tělo	k1gNnSc2
vzniklý	vzniklý	k2eAgInSc1d1
srůstem	srůst	k1gInSc7
několika	několik	k4yIc2
článků	článek	k1gInPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c4
stejné	stejný	k2eAgInPc4d1
články	článek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Články	článek	k1gInPc7
jsou	být	k5eAaImIp3nP
od	od	k7c2
sebe	sebe	k3xPyFc4
navzájem	navzájem	k6eAd1
odděleny	oddělit	k5eAaPmNgInP
vazivovými	vazivový	k2eAgFnPc7d1
přepážkami	přepážka	k1gFnPc7
(	(	kIx(
<g/>
dissepimenty	dissepiment	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
orgány	orgán	k1gInPc4
(	(	kIx(
<g/>
např.	např.	kA
metanefridie	metanefridie	k1gFnSc2
a	a	k8xC
nervová	nervový	k2eAgNnPc1d1
ganglia	ganglion	k1gNnPc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
segmentálně	segmentálně	k6eAd1
uspořádány	uspořádat	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Povrch	povrch	k1gInSc1
těla	tělo	k1gNnSc2
</s>
<s>
Na	na	k7c6
povrchu	povrch	k1gInSc6
těla	tělo	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jednovrstevná	jednovrstevný	k2eAgFnSc1d1
kolagenní	kolagenní	k2eAgFnSc1d1
pokožka	pokožka	k1gFnSc1
<g/>
,	,	kIx,
hustě	hustě	k6eAd1
protkaná	protkaný	k2eAgFnSc1d1
vlásečnicemi	vlásečnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pokožce	pokožka	k1gFnSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
slizové	slizový	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
vylučující	vylučující	k2eAgInSc1d1
hlen	hlen	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
usnadňuje	usnadňovat	k5eAaImIp3nS
pohyb	pohyb	k1gInSc4
po	po	k7c6
nerovném	rovný	k2eNgInSc6d1
povrchu	povrch	k1gInSc6
<g/>
,	,	kIx,
pomáhá	pomáhat	k5eAaImIp3nS
při	při	k7c6
dýchání	dýchání	k1gNnSc6
a	a	k8xC
zároveň	zároveň	k6eAd1
chrání	chránit	k5eAaImIp3nS
pokožku	pokožka	k1gFnSc4
před	před	k7c7
vysycháním	vysychání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pokožka	pokožka	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
hmatové	hmatový	k2eAgInPc4d1
receptory	receptor	k1gInPc4
a	a	k8xC
také	také	k9
světločivné	světločivný	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
(	(	kIx(
<g/>
faosomy	faosom	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
nichž	jenž	k3xRgFnPc2
je	být	k5eAaImIp3nS
schopná	schopný	k2eAgFnSc1d1
rozlišovat	rozlišovat	k5eAaImF
světlo	světlo	k1gNnSc4
a	a	k8xC
tmu	tma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žížaly	žížala	k1gFnSc2
jsou	být	k5eAaImIp3nP
fotofóbní	fotofóbný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bocích	bok	k1gInPc6
každého	každý	k3xTgInSc2
článku	článek	k1gInSc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
příústního	příústní	k2eAgInSc2d1
a	a	k8xC
pygidiálního	pygidiální	k2eAgInSc2d1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
čtyři	čtyři	k4xCgInPc4
váčky	váček	k1gInPc4
s	s	k7c7
párem	pár	k1gInSc7
štětinek	štětinka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
pohybu	pohyb	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
ochrany	ochrana	k1gFnSc2
těla	tělo	k1gNnSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
pokožka	pokožka	k1gFnSc1
i	i	k9
funkci	funkce	k1gFnSc4
dýchací	dýchací	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Kožně	kožně	k6eAd1
svalový	svalový	k2eAgInSc1d1
vak	vak	k1gInSc1
</s>
<s>
Kožně	kožně	k6eAd1
svalový	svalový	k2eAgInSc1d1
vak	vak	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
pohybu	pohyb	k1gInSc3
a	a	k8xC
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
hladkou	hladký	k2eAgFnSc7d1
svalovinou	svalovina	k1gFnSc7
uspořádanou	uspořádaný	k2eAgFnSc4d1
ve	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
vrstvách	vrstva	k1gFnPc6
<g/>
:	:	kIx,
podélně	podélně	k6eAd1
<g/>
,	,	kIx,
okružně	okružně	k6eAd1
<g/>
,	,	kIx,
příčně	příčně	k6eAd1
a	a	k8xC
kose	kose	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohybu	pohyb	k1gInSc2
napomáhají	napomáhat	k5eAaImIp3nP,k5eAaBmIp3nP
také	také	k9
párové	párový	k2eAgFnPc1d1
štětinky	štětinka	k1gFnPc1
v	v	k7c6
pokožce	pokožka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Opora	opora	k1gFnSc1
těla	tělo	k1gNnSc2
</s>
<s>
Funkci	funkce	k1gFnSc4
kostry	kostra	k1gFnSc2
plní	plnit	k5eAaImIp3nS
hydroskelet	hydroskelet	k5eAaPmF,k5eAaImF
v	v	k7c6
podobě	podoba	k1gFnSc6
coelomové	coelomový	k2eAgFnSc2d1
tekutiny	tekutina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyplňuje	vyplňovat	k5eAaImIp3nS
coelomové	coelomový	k2eAgInPc4d1
váčky	váček	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
segmentálně	segmentálně	k6eAd1
uspořádány	uspořádat	k5eAaPmNgInP
<g/>
,	,	kIx,
kromě	kromě	k7c2
příústního	příústní	k2eAgInSc2d1
a	a	k8xC
pygidiálního	pygidiální	k2eAgInSc2d1
se	se	k3xPyFc4
v	v	k7c6
každém	každý	k3xTgInSc6
článku	článek	k1gInSc6
po	po	k7c6
stranách	strana	k1gFnPc6
střeva	střevo	k1gNnSc2
nachází	nacházet	k5eAaImIp3nS
coelomové	coelomový	k2eAgInPc4d1
články	článek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Cévní	cévní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Cévní	cévní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
je	být	k5eAaImIp3nS
uzavřená	uzavřený	k2eAgFnSc1d1
<g/>
,	,	kIx,
mezodermálního	mezodermální	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkci	funkce	k1gFnSc4
srdce	srdce	k1gNnSc2
plní	plnit	k5eAaImIp3nS
stažitelná	stažitelný	k2eAgFnSc1d1
hřbetní	hřbetní	k2eAgFnSc1d1
céva	céva	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
svým	svůj	k3xOyFgNnSc7
pulzováním	pulzování	k1gNnSc7
přivádí	přivádět	k5eAaImIp3nS
krev	krev	k1gFnSc1
do	do	k7c2
přední	přední	k2eAgFnSc2d1
části	část	k1gFnSc2
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
céva	céva	k1gFnSc1
příčnými	příčný	k2eAgFnPc7d1
pulzujícími	pulzující	k2eAgFnPc7d1
spojkami	spojka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k8xS,k8xC
postranní	postranní	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
propojena	propojit	k5eAaPmNgFnS
s	s	k7c7
cévou	céva	k1gFnSc7
břišní	břišní	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
žene	hnát	k5eAaImIp3nS
krev	krev	k1gFnSc4
do	do	k7c2
zadní	zadní	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krevní	krevní	k2eAgFnSc6d1
plazmě	plazma	k1gFnSc6
je	být	k5eAaImIp3nS
rozpuštěno	rozpustit	k5eAaPmNgNnS
několik	několik	k4yIc1
krevních	krevní	k2eAgFnPc2d1
barviv	barvivo	k1gNnPc2
<g/>
,	,	kIx,
především	především	k9
hemoglobin	hemoglobin	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
krev	krev	k1gFnSc4
zbarvuje	zbarvovat	k5eAaImIp3nS
červeně	červeně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Anatomie	anatomie	k1gFnSc1
žížaly	žížala	k1gFnSc2
obecné	obecný	k2eAgFnSc2d1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
–	–	k?
mozkové	mozkový	k2eAgNnSc1d1
ganglium	ganglium	k1gNnSc1
<g/>
,	,	kIx,
2	#num#	k4
–	–	k?
hltan	hltan	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
–	–	k?
srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
4	#num#	k4
–	–	k?
mezičlánková	mezičlánkový	k2eAgFnSc1d1
přepážka	přepážka	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
–	–	k?
semenné	semenný	k2eAgInPc1d1
váčky	váček	k1gInPc1
<g/>
,	,	kIx,
6	#num#	k4
–	–	k?
žláznatý	žláznatý	k2eAgInSc1d1
žaludek	žaludek	k1gInSc1
(	(	kIx(
<g/>
vole	vole	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
–	–	k?
svalnatý	svalnatý	k2eAgInSc4d1
žaludek	žaludek	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
8	#num#	k4
–	–	k?
střevo	střevo	k1gNnSc1
</s>
<s>
Nervovou	nervový	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
nervová	nervový	k2eAgNnPc1d1
ganglia	ganglion	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
segmentálně	segmentálně	k6eAd1
uspořádaná	uspořádaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovoříme	hovořit	k5eAaImIp1nP
o	o	k7c6
gangliové	gangliový	k2eAgFnSc6d1
nervové	nervový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
žebříčkovitého	žebříčkovitý	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přední	přední	k2eAgFnSc6d1
části	část	k1gFnSc6
těla	tělo	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
nadjícnové	nadjícnový	k2eAgNnSc1d1
a	a	k8xC
pod	pod	k7c4
jícnové	jícnový	k2eAgFnPc4d1
gangliem	ganglion	k1gNnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
propojená	propojený	k2eAgNnPc4d1
nervovým	nervový	k2eAgInSc7d1
prstencem	prstenec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
podjícnového	podjícnový	k2eAgNnSc2d1
ganglia	ganglion	k1gNnSc2
pak	pak	k6eAd1
vybíhají	vybíhat	k5eAaImIp3nP
na	na	k7c4
břišní	břišní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
těla	tělo	k1gNnSc2
dva	dva	k4xCgInPc4
nervové	nervový	k2eAgInPc4d1
pásy	pás	k1gInPc4
tvořící	tvořící	k2eAgNnSc1d1
ganglia	ganglion	k1gNnPc1
v	v	k7c6
každém	každý	k3xTgInSc6
tělním	tělní	k2eAgInSc6d1
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
ganglia	ganglion	k1gNnPc4
jsou	být	k5eAaImIp3nP
propojena	propojen	k2eAgFnSc1d1
příčnými	příčný	k2eAgFnPc7d1
a	a	k8xC
podélnými	podélný	k2eAgFnPc7d1
spojkami	spojka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
konektivy	konektiv	k1gInPc1
(	(	kIx(
<g/>
podélné	podélný	k2eAgFnSc3d1
<g/>
)	)	kIx)
a	a	k8xC
komisury	komisura	k1gFnPc1
(	(	kIx(
<g/>
příčné	příčný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dýchací	dýchací	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Žížaly	žížala	k1gFnPc1
dýchají	dýchat	k5eAaImIp3nP
celým	celý	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
funkci	funkce	k1gFnSc3
dýchání	dýchání	k1gNnSc2
plní	plnit	k5eAaImIp3nS
hustě	hustě	k6eAd1
prokrvená	prokrvený	k2eAgFnSc1d1
jednovrstevná	jednovrstevný	k2eAgFnSc1d1
pokožka	pokožka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
je	být	k5eAaImIp3nS
entodermálního	entodermální	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
trubicovitá	trubicovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
trávicí	trávicí	k2eAgFnSc1d1
trubice	trubice	k1gFnSc1
prochází	procházet	k5eAaImIp3nS
celým	celý	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
ústy	ústa	k1gNnPc7
<g/>
,	,	kIx,
na	na	k7c4
ně	on	k3xPp3gInPc4
navazuje	navazovat	k5eAaImIp3nS
hltan	hltan	k1gInSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
ústí	ústit	k5eAaImIp3nS
slinné	slinný	k2eAgFnPc4d1
žlázy	žláza	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
jícnem	jícen	k1gInSc7
pokračuje	pokračovat	k5eAaImIp3nS
do	do	k7c2
žláznatého	žláznatý	k2eAgInSc2d1
žaludku	žaludek	k1gInSc2
<g/>
,	,	kIx,
nazývaného	nazývaný	k2eAgInSc2d1
též	též	k9
vole	vole	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
volete	vole	k1gNnSc2
ústí	ústit	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
chylové	chylový	k2eAgInPc4d1
váčky	váček	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
obsahují	obsahovat	k5eAaImIp3nP
uhličitan	uhličitan	k1gInSc1
vápenatý	vápenatý	k2eAgInSc1d1
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
neutralizaci	neutralizace	k1gFnSc3
huminových	huminův	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
v	v	k7c6
pozřené	pozřený	k2eAgFnSc6d1
potravě	potrava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
následuje	následovat	k5eAaImIp3nS
svalnatý	svalnatý	k2eAgInSc1d1
žaludek	žaludek	k1gInSc1
a	a	k8xC
střevo	střevo	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
vnitřní	vnitřní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vybíhá	vybíhat	k5eAaImIp3nS
v	v	k7c4
prokrvenou	prokrvený	k2eAgFnSc4d1
epiteliální	epiteliální	k2eAgFnSc4d1
řasu	řasa	k1gFnSc4
tyflosolis	tyflosolis	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zvětšuje	zvětšovat	k5eAaImIp3nS
trávicí	trávicí	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
střeva	střevo	k1gNnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
umožňuje	umožňovat	k5eAaImIp3nS
dokonalejší	dokonalý	k2eAgNnSc1d2
využití	využití	k1gNnSc1
potravy	potrava	k1gFnSc2
chudé	chudý	k1gMnPc4
na	na	k7c4
živiny	živina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
povrchu	povrch	k1gInSc6
střeva	střevo	k1gNnSc2
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
epiteliální	epiteliální	k2eAgFnPc1d1
chloragogenní	chloragogenní	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
v	v	k7c6
sobě	sebe	k3xPyFc6
ukládají	ukládat	k5eAaImIp3nP
tělu	tělo	k1gNnSc3
škodlivé	škodlivý	k2eAgFnPc1d1
látky	látka	k1gFnPc1
<g/>
,	,	kIx,
syntetizují	syntetizovat	k5eAaImIp3nP
tuky	tuk	k1gInPc4
a	a	k8xC
glykogen	glykogen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zaplnění	zaplnění	k1gNnSc6
škodlivinami	škodlivina	k1gFnPc7
se	se	k3xPyFc4
odlupují	odlupovat	k5eAaImIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
následně	následně	k6eAd1
vyloučeny	vyloučen	k2eAgFnPc1d1
metanefridiemi	metanefridie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trávicí	trávicí	k2eAgFnSc1d1
trubice	trubice	k1gFnSc1
je	být	k5eAaImIp3nS
zakončená	zakončený	k2eAgNnPc4d1
řitním	řitní	k2eAgInSc7d1
otvorem	otvor	k1gInSc7
na	na	k7c6
pygidiálním	pygidiální	k2eAgInSc6d1
tělním	tělní	k2eAgInSc6d1
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vylučovací	vylučovací	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
K	k	k7c3
vylučování	vylučování	k1gNnSc3
slouží	sloužit	k5eAaImIp3nS
párové	párový	k2eAgFnSc2d1
metanefridie	metanefridie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
každém	každý	k3xTgInSc6
tělním	tělní	k2eAgInSc6d1
článku	článek	k1gInSc6
(	(	kIx(
<g/>
kromě	kromě	k7c2
předústního	předústní	k2eAgInSc2d1
a	a	k8xC
pygidiálního	pygidiální	k2eAgInSc2d1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
pár	pár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metanefridium	Metanefridium	k1gNnSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
obrvené	obrvený	k2eAgFnSc2d1
nálevky	nálevka	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
vinutý	vinutý	k2eAgInSc1d1
kanálek	kanálek	k1gInSc1
prostupuje	prostupovat	k5eAaImIp3nS
dissepimentem	dissepiment	k1gInSc7
do	do	k7c2
dalšího	další	k2eAgInSc2d1
článku	článek	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ústí	ústit	k5eAaImIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Způsob	způsob	k1gInSc1
života	život	k1gInSc2
a	a	k8xC
potrava	potrava	k1gFnSc1
</s>
<s>
Žížaly	žížala	k1gFnPc1
jsou	být	k5eAaImIp3nP
významnou	významný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
potravního	potravní	k2eAgInSc2d1
řetězce	řetězec	k1gInSc2
</s>
<s>
Potravu	potrava	k1gFnSc4
žížaly	žížala	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
především	především	k9
tlející	tlející	k2eAgNnSc1d1
listí	listí	k1gNnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
drobní	drobný	k2eAgMnPc1d1
uhynulí	uhynulý	k2eAgMnPc1d1
živočichové	živočich	k1gMnPc1
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
trávicím	trávicí	k2eAgInSc7d1
traktem	trakt	k1gInSc7
tedy	tedy	k9
projde	projít	k5eAaPmIp3nS
poměrně	poměrně	k6eAd1
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
potravy	potrava	k1gFnSc2
s	s	k7c7
nízkým	nízký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
živin	živina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestrávené	strávený	k2eNgInPc1d1
zbytky	zbytek	k1gInPc1
pak	pak	k8xC
žížaly	žížala	k1gFnPc1
vynášejí	vynášet	k5eAaImIp3nP
na	na	k7c4
zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
ve	v	k7c6
formě	forma	k1gFnSc6
malých	malý	k2eAgFnPc2d1
hromádek	hromádka	k1gFnPc2
trusu	trus	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
hmotnost	hmotnost	k1gFnSc4
za	za	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
několika	několik	k4yIc2
kilogramů	kilogram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Predace	Predace	k1gFnSc1
</s>
<s>
Žížala	žížala	k1gFnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
obživou	obživa	k1gFnSc7
pro	pro	k7c4
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
různých	různý	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
obojživelníků	obojživelník	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
různých	různý	k2eAgMnPc2d1
bezobratlých	bezobratlý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
například	například	k6eAd1
o	o	k7c4
kosy	kos	k1gMnPc4
<g/>
,	,	kIx,
drozdy	drozd	k1gMnPc4
a	a	k8xC
špačky	špaček	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
opatrně	opatrně	k6eAd1
kráčejí	kráčet	k5eAaImIp3nP
po	po	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
naklánějí	naklánět	k5eAaImIp3nP
se	se	k3xPyFc4
dopředu	dopředu	k6eAd1
a	a	k8xC
loví	lovit	k5eAaImIp3nP
žížaly	žížala	k1gFnPc1
ukryté	ukrytý	k2eAgFnPc1d1
těsně	těsně	k6eAd1
pod	pod	k7c7
povrchem	povrch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
i	i	k9
potravou	potrava	k1gFnSc7
ježků	ježek	k1gMnPc2
<g/>
,	,	kIx,
jezevců	jezevec	k1gMnPc2
<g/>
,	,	kIx,
krtků	krtek	k1gMnPc2
<g/>
,	,	kIx,
několika	několik	k4yIc3
druhů	druh	k1gInPc2
hadů	had	k1gMnPc2
a	a	k8xC
žab	žába	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
bezobratlých	bezobratlí	k1gMnPc2
jsou	být	k5eAaImIp3nP
hlavními	hlavní	k2eAgMnPc7d1
nepřáteli	nepřítel	k1gMnPc7
krtonožky	krtonožka	k1gFnSc2
<g/>
,	,	kIx,
slimáci	slimák	k1gMnPc1
a	a	k8xC
některé	některý	k3yIgFnSc2
ploštěnky	ploštěnka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
invazi	invaze	k1gFnSc3
ploštěnek	ploštěnka	k1gFnPc2
druhů	druh	k1gInPc2
Arthurdendyus	Arthurdendyus	k1gMnSc1
triangulatus	triangulatus	k1gMnSc1
a	a	k8xC
Australoplana	Australoplana	k1gFnSc1
sanguinea	sanguine	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
rozšířily	rozšířit	k5eAaPmAgFnP
z	z	k7c2
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
loví	lovit	k5eAaImIp3nP
žížaly	žížala	k1gFnPc1
ve	v	k7c6
velkém	velký	k2eAgNnSc6d1
<g/>
,	,	kIx,
ohrožuje	ohrožovat	k5eAaImIp3nS
žížaly	žížala	k1gFnSc2
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
zásadní	zásadní	k2eAgInSc1d1
úbytek	úbytek	k1gInSc1
populace	populace	k1gFnSc2
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
plynoucí	plynoucí	k2eAgInPc1d1
ekologické	ekologický	k2eAgInPc1d1
následky	následek	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svou	svůj	k3xOyFgFnSc7
činností	činnost	k1gFnSc7
žížaly	žížala	k1gFnSc2
převrstvují	převrstvovat	k5eAaImIp3nP
a	a	k8xC
provzdušňují	provzdušňovat	k5eAaImIp3nP
půdu	půda	k1gFnSc4
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
žížaly	žížala	k1gFnPc4
velká	velký	k2eAgFnSc1d1
regenerační	regenerační	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	on	k3xPp3gFnPc4
nepřítel	nepřítel	k1gMnSc1
polapí	polapit	k5eAaPmIp3nS
<g/>
,	,	kIx,
zůstane	zůstat	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
jen	jen	k6eAd1
zadní	zadní	k2eAgFnSc4d1
část	část	k1gFnSc4
těla	tělo	k1gNnSc2
žížaly	žížala	k1gFnSc2
za	za	k7c7
opaskem	opasek	k1gInSc7
(	(	kIx(
<g/>
světlá	světlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ztluštělá	ztluštělý	k2eAgFnSc1d1
část	část	k1gFnSc1
v	v	k7c6
přední	přední	k2eAgFnSc6d1
třetině	třetina	k1gFnSc6
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ústí	ústit	k5eAaImIp3nP
četné	četný	k2eAgFnPc1d1
žlázy	žláza	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opasek	opasek	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mezi	mezi	k7c7
32	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
37	#num#	k4
<g/>
.	.	kIx.
článkem	článek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
těla	tělo	k1gNnSc2
žížale	žížala	k1gFnSc3
doroste	dorůst	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
ale	ale	k8xC
přetržena	přetrhnout	k5eAaPmNgFnS
přesně	přesně	k6eAd1
uprostřed	uprostřed	k7c2
opasku	opasek	k1gInSc2
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
zahyne	zahynout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Půdotvorná	půdotvorný	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
mají	mít	k5eAaImIp3nP
rovněž	rovněž	k9
zvláštní	zvláštní	k2eAgInSc4d1
ekologický	ekologický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohybem	pohyb	k1gInSc7
v	v	k7c6
půdě	půda	k1gFnSc6
převrstvují	převrstvovat	k5eAaImIp3nP
a	a	k8xC
provzdušňují	provzdušňovat	k5eAaImIp3nP
půdu	půda	k1gFnSc4
a	a	k8xC
podílí	podílet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
vytváření	vytváření	k1gNnSc4
humusu	humus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojnost	hojnost	k1gFnSc1
žížal	žížala	k1gFnPc2
v	v	k7c6
půdě	půda	k1gFnSc6
je	být	k5eAaImIp3nS
tedy	tedy	k9
zárukou	záruka	k1gFnSc7
vysoké	vysoký	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
provzdušněná	provzdušněný	k2eAgFnSc1d1
<g/>
,	,	kIx,
výživná	výživný	k2eAgFnSc1d1
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
úrodná	úrodný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Páření	páření	k1gNnSc1
žížaly	žížala	k1gFnSc2
obecné	obecný	k2eAgFnSc2d1
</s>
<s>
Žížaly	Žížala	k1gMnPc4
jsou	být	k5eAaImIp3nP
hermafrodité	hermafrodit	k1gMnPc1
s	s	k7c7
přímým	přímý	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
dva	dva	k4xCgInPc4
páry	pár	k1gInPc4
varlat	varle	k1gNnPc2
umístěné	umístěný	k2eAgFnSc2d1
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
tělním	tělní	k2eAgInSc6d1
článku	článek	k1gInSc6
a	a	k8xC
jeden	jeden	k4xCgInSc4
párový	párový	k2eAgInSc4d1
vaječník	vaječník	k1gInSc4
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
článku	článek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ústí	ústit	k5eAaImIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Páří	pářit	k5eAaImIp3nS
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
za	za	k7c2
teplých	teplý	k2eAgFnPc2d1
letních	letní	k2eAgFnPc2d1
nocí	noc	k1gFnPc2
na	na	k7c6
zemském	zemský	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
partnera	partner	k1gMnSc4
přilákají	přilákat	k5eAaPmIp3nP
svými	svůj	k3xOyFgInPc7
pachovými	pachový	k2eAgInPc7d1
signály	signál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělí	dospělý	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
kroužek	kroužek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
rozmnožování	rozmnožování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
kopulaci	kopulace	k1gFnSc6
si	se	k3xPyFc3
vyměňují	vyměňovat	k5eAaImIp3nP
spermie	spermie	k1gFnPc4
obalené	obalený	k2eAgFnSc2d1
spermatoforem	spermatofor	k1gInSc7
a	a	k8xC
ukládají	ukládat	k5eAaImIp3nP
je	on	k3xPp3gNnSc4
v	v	k7c6
zásobních	zásobní	k2eAgInPc6d1
váčcích	váček	k1gInPc6
<g/>
,	,	kIx,
dokud	dokud	k8xS
vajíčka	vajíčko	k1gNnPc1
nedozrají	dozrát	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oplozená	oplozený	k2eAgNnPc1d1
vajíčka	vajíčko	k1gNnPc1
obalují	obalovat	k5eAaImIp3nP
tuhnoucím	tuhnoucí	k2eAgInSc7d1
sekretem	sekret	k1gInSc7
z	z	k7c2
opaskových	opaskový	k2eAgFnPc2d1
žláz	žláza	k1gFnPc2
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
tak	tak	k9
kokon	kokon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
stahují	stahovat	k5eAaImIp3nP
přes	přes	k7c4
přední	přední	k2eAgFnSc4d1
část	část	k1gFnSc4
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kokon	kokon	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
ochraně	ochrana	k1gFnSc3
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
až	až	k6eAd1
dvacet	dvacet	k4xCc4
vajíček	vajíčko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
uzavřeného	uzavřený	k2eAgInSc2d1
kokonu	kokon	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vydrží	vydržet	k5eAaPmIp3nS
i	i	k9
v	v	k7c6
extrémně	extrémně	k6eAd1
nepříznivém	příznivý	k2eNgNnSc6d1
počasí	počasí	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
vylíhne	vylíhnout	k5eAaPmIp3nS
jen	jen	k9
jediná	jediný	k2eAgFnSc1d1
žížala	žížala	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ochrana	ochrana	k1gFnSc1
</s>
<s>
Zákonem	zákon	k1gInSc7
není	být	k5eNaImIp3nS
zvláště	zvláště	k6eAd1
chráněna	chránit	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KRUS	KRUS	kA
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rousnice	rousnice	k1gFnSc1
je	být	k5eAaImIp3nS
skvělá	skvělý	k2eAgFnSc1d1
nástraha	nástraha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nasbírejte	nasbírat	k5eAaPmRp2nP
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
na	na	k7c6
zahradě	zahrada	k1gFnSc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-05-28	2012-05-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
Zealand	Zealand	k1gInSc1
and	and	k?
other	other	k1gInSc1
flatworms	flatworms	k1gInSc1
/	/	kIx~
RHS	RHS	kA
Gardening	Gardening	k1gInSc1
<g/>
.	.	kIx.
www.rhs.org.uk	www.rhs.org.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HANZÁK	HANZÁK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
HALÍK	HALÍK	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
;	;	kIx,
MIKULOVÁ	Mikulová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světem	svět	k1gInSc7
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezobratlí	bezobratlý	k2eAgMnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
díl	díl	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
.	.	kIx.
325	#num#	k4
s.	s.	k?
S.	S.	kA
221	#num#	k4
<g/>
–	–	k?
<g/>
222	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
ZICHÁČEK	ZICHÁČEK	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biologie	biologie	k1gFnSc1
pro	pro	k7c4
gymnázia	gymnázium	k1gNnPc4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
:	:	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
574	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-7182-159-4	80-7182-159-4	k4
S.	S.	kA
111	#num#	k4
<g/>
–	–	k?
<g/>
112	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PIŽL	PIŽL	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
utvářeno	utvářen	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
žížaly	žížala	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živa	Živa	k1gFnSc1
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
44	#num#	k4
(	(	kIx(
<g/>
82	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
88	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0044-4812	0044-4812	k4
</s>
<s>
PIŽL	PIŽL	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žížaly	žížala	k1gFnPc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
:	:	kIx,
Earthworms	Earthworms	k1gInSc1
of	of	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
Přírodovědného	přírodovědný	k2eAgInSc2d1
klubu	klub	k1gInSc2
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supplementum	Supplementum	k1gNnSc4
č.	č.	k?
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
<g/>
.	.	kIx.
154	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-86485-04-8	80-86485-04-8	k4
S.	S.	kA
17	#num#	k4
<g/>
,	,	kIx,
18	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
<g/>
,	,	kIx,
35	#num#	k4
<g/>
,	,	kIx,
120	#num#	k4
<g/>
–	–	k?
<g/>
121	#num#	k4
<g/>
,	,	kIx,
142	#num#	k4
<g/>
,	,	kIx,
151	#num#	k4
<g/>
,	,	kIx,
150	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
žížala	žížala	k1gFnSc1
obecná	obecný	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Lumbricus	Lumbricus	k1gInSc1
terrestris	terrestris	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
box-sizing	box-sizing	k1gInSc1
<g/>
:	:	kIx,
<g/>
border-box	border-box	k1gInSc1
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
<g/>
;	;	kIx,
<g/>
width	widtha	k1gFnPc2
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
clear	clear	k1gInSc1
<g/>
:	:	kIx,
<g/>
both	both	k1gInSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
em	em	k?
auto	auto	k1gNnSc1
0	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-inner	-innra	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
width	width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-group	-group	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-titlat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.25	0.25	k4
<g/>
em	em	k?
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
<g/>
text-align	text-aligno	k1gNnPc2
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc1
<g/>
{	{	kIx(
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gMnSc1
<g/>
{	{	kIx(
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-image	-imagat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gInSc1
<g/>
{	{	kIx(
<g/>
border-top	border-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g />
.	.	kIx.
</s>
<s hack="1">
solid	solid	k1gInSc1
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
th	th	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
f	f	k?
<g/>
0	#num#	k4
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-even	-evna	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
-odd	-odda	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
transparent	transparent	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.125	0.125	k4
<g/>
em	em	k?
0	#num#	k4
<g/>
}	}	kIx)
<g/>
Identifikátory	identifikátor	k1gInPc1
taxonu	taxon	k1gInSc2
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
Q30092	Q30092	k1gFnSc1
</s>
<s>
Wikidruhy	Wikidruha	k1gFnPc1
<g/>
:	:	kIx,
Lumbricus	Lumbricus	k1gInSc1
terrestris	terrestris	k1gFnSc2
</s>
<s>
ADW	ADW	kA
<g/>
:	:	kIx,
Lumbricus_terrestris	Lumbricus_terrestris	k1gFnSc1
</s>
<s>
BioLib	BioLib	k1gInSc1
<g/>
:	:	kIx,
44029	#num#	k4
</s>
<s>
BOLD	BOLD	kA
<g/>
:	:	kIx,
25197	#num#	k4
</s>
<s>
NDOP	NDOP	kA
<g/>
:	:	kIx,
67660	#num#	k4
</s>
<s>
EPPO	EPPO	kA
<g/>
:	:	kIx,
LUMBTE	LUMBTE	kA
</s>
<s>
EUNIS	EUNIS	kA
<g/>
:	:	kIx,
223698	#num#	k4
</s>
<s>
Fauna	fauna	k1gFnSc1
Europaea	Europaea	k1gFnSc1
<g/>
:	:	kIx,
178345	#num#	k4
</s>
<s>
Fauna	fauna	k1gFnSc1
Europaea	Europaea	k1gFnSc1
(	(	kIx(
<g/>
new	new	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
0	#num#	k4
<g/>
ac	ac	k?
<g/>
83	#num#	k4
<g/>
c	c	k0
<g/>
5	#num#	k4
<g/>
b-	b-	k?
<g/>
0	#num#	k4
<g/>
714	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
ff	ff	kA
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
85	#num#	k4
<g/>
c	c	k0
<g/>
4	#num#	k4
<g/>
-c	-c	k?
<g/>
0	#num#	k4
<g/>
3593	#num#	k4
<g/>
bea	bea	k?
<g/>
186	#num#	k4
</s>
<s>
GBIF	GBIF	kA
<g/>
:	:	kIx,
4410657	#num#	k4
</s>
<s>
GISD	GISD	kA
<g/>
:	:	kIx,
1555	#num#	k4
</s>
<s>
iNaturalist	iNaturalist	k1gInSc1
<g/>
:	:	kIx,
81545	#num#	k4
</s>
<s>
IRMNG	IRMNG	kA
<g/>
:	:	kIx,
11318075	#num#	k4
</s>
<s>
ISC	ISC	kA
<g/>
:	:	kIx,
109385	#num#	k4
</s>
<s>
ITIS	ITIS	kA
<g/>
:	:	kIx,
977384	#num#	k4
</s>
<s>
NBN	NBN	kA
<g/>
:	:	kIx,
NBNSYS0000022358	NBNSYS0000022358	k1gMnSc1
</s>
<s>
NCBI	NCBI	kA
<g/>
:	:	kIx,
6398	#num#	k4
</s>
<s>
NZOR	NZOR	kA
<g/>
:	:	kIx,
a	a	k8xC
<g/>
8950684	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
f	f	k?
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
43	#num#	k4
<g/>
e	e	k0
<g/>
4	#num#	k4
<g/>
-a	-a	k?
<g/>
850	#num#	k4
<g/>
-	-	kIx~
<g/>
102	#num#	k4
<g/>
bbed	bbed	k6eAd1
<g/>
8766	#num#	k4
<g/>
b	b	k?
</s>
<s>
WoRMS	WoRMS	k?
<g/>
:	:	kIx,
1069627	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4130770-7	4130770-7	k4
</s>
