<p>
<s>
Želary	Želara	k1gFnPc4	Želara
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc1d1	české
filmové	filmový	k2eAgNnSc1d1	filmové
drama	drama	k1gNnSc1	drama
režiséra	režisér	k1gMnSc2	režisér
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Trojana	Trojan	k1gMnSc2	Trojan
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
mladé	mladý	k2eAgFnSc2d1	mladá
Elišky	Eliška	k1gFnSc2	Eliška
v	v	k7c6	v
období	období	k1gNnSc6	období
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Eliška	Eliška	k1gFnSc1	Eliška
je	být	k5eAaImIp3nS	být
nedostudovaná	dostudovaný	k2eNgFnSc1d1	nedostudovaná
lékařka	lékařka	k1gFnSc1	lékařka
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgFnP	být
nuceně	nuceně	k6eAd1	nuceně
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
členka	členka	k1gFnSc1	členka
tajného	tajný	k2eAgInSc2d1	tajný
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
ukrýt	ukrýt	k5eAaPmF	ukrýt
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Želary	Želara	k1gFnSc2	Želara
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
vesnice	vesnice	k1gFnSc2	vesnice
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
a	a	k8xC	a
vesnice	vesnice	k1gFnSc1	vesnice
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Drsný	drsný	k2eAgInSc1d1	drsný
romantický	romantický	k2eAgInSc1d1	romantický
příběh	příběh	k1gInSc1	příběh
získal	získat	k5eAaPmAgInS	získat
mnoho	mnoho	k6eAd1	mnoho
ocenění	ocenění	k1gNnSc4	ocenění
i	i	k8xC	i
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Želary	Želar	k1gInPc1	Želar
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Želary	Želara	k1gFnPc1	Želara
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Želary	Želara	k1gFnPc1	Želara
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
