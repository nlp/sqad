<s>
Zahraniční	zahraniční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
Číny	Čína	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
nebo	nebo	k8xC
jeho	jeho	k3xOp3gFnSc1
část	část	k1gFnSc1
potřebuje	potřebovat	k5eAaImIp3nS
stylistické	stylistický	k2eAgFnPc4d1
(	(	kIx(
<g/>
slohové	slohový	k2eAgFnPc4d1
<g/>
)	)	kIx)
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspiraci	inspirace	k1gFnSc4
můžete	moct	k5eAaImIp2nP
hledat	hledat	k5eAaImF
v	v	k7c6
radách	rada	k1gFnPc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Diplomatické	diplomatický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
udržují	udržovat	k5eAaImIp3nP
diplomatické	diplomatický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
s	s	k7c7
Čínskou	čínský	k2eAgFnSc7d1
lidovou	lidový	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
Státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
neudržují	udržovat	k5eNaImIp3nP
diplomatické	diplomatický	k2eAgInPc1d1
vtahy	vtah	k1gInPc1
s	s	k7c7
Čínskou	čínský	k2eAgFnSc7d1
lidovou	lidový	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
nebo	nebo	k8xC
udržují	udržovat	k5eAaImIp3nP
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
Tchaj-wanem	Tchaj-wan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sporná	sporný	k2eAgFnSc1d1
území	území	k1gNnSc6
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
Číny	Čína	k1gFnSc2
představují	představovat	k5eAaImIp3nP
diplomatické	diplomatický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Americko-čínská	americko-čínský	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
uvalení	uvalení	k1gNnSc3
cel	cela	k1gFnPc2
na	na	k7c4
čínský	čínský	k2eAgInSc4d1
dovoz	dovoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trump	Trump	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
důvodem	důvod	k1gInSc7
k	k	k7c3
zavedení	zavedení	k1gNnSc3
cel	cela	k1gFnPc2
jsou	být	k5eAaImIp3nP
údajné	údajný	k2eAgFnPc1d1
čínské	čínský	k2eAgFnPc1d1
krádeže	krádež	k1gFnPc1
amerického	americký	k2eAgNnSc2d1
duševního	duševní	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
díky	díky	k7c3
zavedení	zavedení	k1gNnSc3
cel	clo	k1gNnPc2
se	se	k3xPyFc4
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
stanou	stanout	k5eAaPmIp3nP
„	„	k?
<g/>
mnohem	mnohem	k6eAd1
silnější	silný	k2eAgMnSc1d2
a	a	k8xC
mnohem	mnohem	k6eAd1
bohatší	bohatý	k2eAgFnSc4d2
<g/>
“	“	k?
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Investice	investice	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Čínské	čínský	k2eAgFnPc1d1
investice	investice	k1gFnPc1
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Čínské	čínský	k2eAgFnPc1d1
investice	investice	k1gFnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Železnice	železnice	k1gFnSc1
TAZARA	TAZARA	kA
–	–	k?
Tanzansko-zambijská	tanzansko-zambijský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
</s>
<s>
Čína	Čína	k1gFnSc1
investovala	investovat	k5eAaBmAgFnS
do	do	k7c2
mnoha	mnoho	k4c2
infrastrukturních	infrastrukturní	k2eAgInPc2d1
projektů	projekt	k1gInPc2
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Asii	Asie	k1gFnSc3
i	i	k8xC
Africe	Afrika	k1gFnSc3
<g/>
,	,	kIx,
např.	např.	kA
do	do	k7c2
euroasijského	euroasijský	k2eAgInSc2d1
pozemního	pozemní	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
,	,	kIx,
Tanzansko-zambijské	tanzansko-zambijský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
,	,	kIx,
železniční	železniční	k2eAgFnSc2d1
trati	trať	k1gFnSc2
spojující	spojující	k2eAgNnPc4d1
pákistánská	pákistánský	k2eAgNnPc4d1
města	město	k1gNnPc4
Dali	dát	k5eAaPmAgMnP
a	a	k8xC
Ruili	Ruil	k1gMnPc1
<g/>
,	,	kIx,
či	či	k8xC
Gwádarského	Gwádarský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
Gwádar	Gwádara	k1gFnPc2
taktéž	taktéž	k?
v	v	k7c6
Pákistánu	Pákistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Financovala	financovat	k5eAaBmAgFnS
také	také	k9
Přístav	přístav	k1gInSc1
Mahindy	Mahinda	k1gFnSc2
Radžapaksy	Radžapaksa	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Hambantota	Hambantota	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
Srí	Srí	k1gFnSc2
Lanky	lanko	k1gNnPc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
potýkal	potýkat	k5eAaImAgMnS
se	s	k7c7
spoustou	spousta	k1gFnSc7
dluhů	dluh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Srí	Srí	k1gFnSc1
Lanka	lanko	k1gNnSc2
nesplatila	splatit	k5eNaPmAgFnS
své	svůj	k3xOyFgFnPc4
půjčky	půjčka	k1gFnPc4
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
převzala	převzít	k5eAaPmAgFnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
přístavem	přístav	k1gInSc7
na	na	k7c4
dobu	doba	k1gFnSc4
99	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
New	New	k1gFnPc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
to	ten	k3xDgNnSc4
charakterizoval	charakterizovat	k5eAaBmAgMnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Čína	Čína	k1gFnSc1
přiměla	přimět	k5eAaPmAgFnS
Srí	Srí	k1gFnSc4
Lanku	lanko	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
„	„	k?
<g/>
vykašlala	vykašlat	k5eAaPmAgFnS
na	na	k7c4
přístav	přístav	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plány	plán	k1gInPc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Hedvábná	hedvábný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Nová	nový	k2eAgFnSc1d1
Hedvábná	hedvábný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Hedvábná	hedvábný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
je	být	k5eAaImIp3nS
hospodářská	hospodářský	k2eAgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
usnadnit	usnadnit	k5eAaPmF
obchodování	obchodování	k1gNnSc4
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vize	vize	k1gFnSc1
Nové	Nové	k2eAgFnSc2d1
Hedvábné	hedvábný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
obchodní	obchodní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
po	po	k7c6
souši	souš	k1gFnSc6
<g/>
,	,	kIx,
kopíruje	kopírovat	k5eAaImIp3nS
starověkou	starověký	k2eAgFnSc4d1
Hedvábnou	hedvábný	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
a	a	k8xC
vede	vést	k5eAaImIp3nS
z	z	k7c2
Číny	Čína	k1gFnSc2
přes	přes	k7c4
střední	střední	k2eAgFnSc4d1
Asii	Asie	k1gFnSc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
<g/>
,	,	kIx,
námořní	námořní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
vést	vést	k5eAaImF
přes	přes	k7c4
Indický	indický	k2eAgInSc4d1
subkontinent	subkontinent	k1gInSc4
<g/>
,	,	kIx,
točit	točit	k5eAaImF
se	se	k3xPyFc4
kolem	kolem	k7c2
Bengálského	bengálský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
přes	přes	k7c4
Indický	indický	k2eAgInSc4d1
oceán	oceán	k1gInSc4
k	k	k7c3
východním	východní	k2eAgInPc3d1
břehům	břeh	k1gInPc3
Afriky	Afrika	k1gFnSc2
a	a	k8xC
dále	daleko	k6eAd2
skrz	skrz	k7c4
Suezský	suezský	k2eAgInSc4d1
průplav	průplav	k1gInSc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
a	a	k8xC
Střední	střední	k2eAgInSc1d1
Východ	východ	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
uznal	uznat	k5eAaPmAgInS
Čínskou	čínský	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
jako	jako	k8xS,k8xC
legitimní	legitimní	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
Číny	Čína	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k8xC,k8xS
první	první	k4xOgFnSc7
zemí	zem	k1gFnSc7
na	na	k7c6
Středním	střední	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
takto	takto	k6eAd1
učinila	učinit	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
země	země	k1gFnSc1
však	však	k9
spolu	spolu	k6eAd1
navázaly	navázat	k5eAaPmAgInP
vztahy	vztah	k1gInPc1
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
celosvětově	celosvětově	k6eAd1
třetím	třetí	k4xOgMnSc7
největším	veliký	k2eAgMnSc7d3
obchodním	obchodní	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
Izraele	Izrael	k1gInSc2
a	a	k8xC
největším	veliký	k2eAgMnSc7d3
obchodním	obchodní	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Írán	Írán	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
Íránem	Írán	k1gInSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
započaly	započnout	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2019	#num#	k4
Írán	Írán	k1gInSc1
schválil	schválit	k5eAaPmAgInS
bezvízový	bezvízový	k2eAgInSc1d1
vstup	vstup	k1gInSc1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
čínské	čínský	k2eAgMnPc4d1
občany	občan	k1gMnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
občanů	občan	k1gMnPc2
Hongkongu	Hongkong	k1gInSc2
a	a	k8xC
Macaa	Macao	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k8xC,k8xS
dvanáctou	dvanáctý	k4xOgFnSc7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
občané	občan	k1gMnPc1
mají	mít	k5eAaImIp3nP
bezvízový	bezvízový	k2eAgInSc4d1
vstup	vstup	k1gInSc4
do	do	k7c2
Íránu	Írán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
</s>
<s>
Během	během	k7c2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
existovaly	existovat	k5eAaImAgFnP
mezi	mezi	k7c7
Íránem	Írán	k1gInSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
neoficiální	oficiální	k2eNgInPc1d1,k2eAgInPc1d1
obchodní	obchodní	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
postupem	postup	k1gInSc7
času	čas	k1gInSc2
neustále	neustále	k6eAd1
zvyšovaly	zvyšovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Jedním	jeden	k4xCgNnSc7
z	z	k7c2
hlavních	hlavní	k2eAgNnPc2d1
odvětví	odvětví	k1gNnPc2
vztahu	vztah	k1gInSc2
je	být	k5eAaImIp3nS
petrochemický	petrochemický	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
přešla	přejít	k5eAaPmAgFnS
na	na	k7c4
ropu	ropa	k1gFnSc4
především	především	k9
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přesunula	přesunout	k5eAaPmAgFnS
své	svůj	k3xOyFgFnPc4
dodávky	dodávka	k1gFnPc4
energie	energie	k1gFnSc2
z	z	k7c2
uhlí	uhlí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
pocházelo	pocházet	k5eAaImAgNnS
přibližně	přibližně	k6eAd1
10	#num#	k4
%	%	kIx~
čínského	čínský	k2eAgInSc2d1
dovozu	dovoz	k1gInSc2
ropy	ropa	k1gFnSc2
z	z	k7c2
Íránu	Írán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Přibližně	přibližně	k6eAd1
80	#num#	k4
%	%	kIx~
celkového	celkový	k2eAgInSc2d1
čínského	čínský	k2eAgInSc2d1
dovozu	dovoz	k1gInSc2
z	z	k7c2
Íránu	Írán	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
ropa	ropa	k1gFnSc1
a	a	k8xC
zbytek	zbytek	k1gInSc1
minerální	minerální	k2eAgInSc1d1
a	a	k8xC
chemické	chemický	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Sankce	sankce	k1gFnPc1
OSN	OSN	kA
vůči	vůči	k7c3
Íránu	Írán	k1gInSc3
</s>
<s>
Zpočátku	zpočátku	k6eAd1
Írán	Írán	k1gInSc1
nepodporoval	podporovat	k5eNaImAgInS
členství	členství	k1gNnSc4
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
OSN	OSN	kA
a	a	k8xC
vetoval	vetovat	k5eAaBmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
projevil	projevit	k5eAaPmAgInS
Írán	Írán	k1gInSc1
otevřenou	otevřený	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
členství	členství	k1gNnSc2
ČLR	ČLR	kA
v	v	k7c6
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
se	se	k3xPyFc4
Írán	Írán	k1gInSc1
spoléhá	spoléhat	k5eAaImIp3nS
na	na	k7c4
členství	členství	k1gNnSc4
Číny	Čína	k1gFnSc2
a	a	k8xC
zejména	zejména	k9
na	na	k7c4
čínské	čínský	k2eAgNnSc4d1
právo	právo	k1gNnSc4
veta	veto	k1gNnSc2
v	v	k7c6
Radě	rada	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
ochránil	ochránit	k5eAaPmAgMnS
před	před	k7c7
sankcemi	sankce	k1gFnPc7
vedenými	vedený	k2eAgFnPc7d1
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
svou	svůj	k3xOyFgFnSc7
preferencí	preference	k1gFnSc7
diplomacie	diplomacie	k1gFnSc2
před	před	k7c7
sankcemi	sankce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
)	)	kIx)
proti	proti	k7c3
sankcím	sankce	k1gFnPc3
vůči	vůči	k7c3
Íránu	Írán	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
Čína	Čína	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
podpořit	podpořit	k5eAaPmF
zbrojní	zbrojní	k2eAgNnSc4d1
embargo	embargo	k1gNnSc4
OSN	OSN	kA
vůči	vůči	k7c3
Íránu	Írán	k1gInSc3
a	a	k8xC
dále	daleko	k6eAd2
se	se	k3xPyFc4
zdržela	zdržet	k5eAaPmAgFnS
hlasování	hlasování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
pod	pod	k7c7
tlakem	tlak	k1gInSc7
USA	USA	kA
připojila	připojit	k5eAaPmAgFnS
Čína	Čína	k1gFnSc1
k	k	k7c3
Rusku	Rusko	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podpořila	podpořit	k5eAaPmAgFnS
tyto	tento	k3xDgFnPc4
sankce	sankce	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Palestina	Palestina	k1gFnSc1
</s>
<s>
V	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Čína	Čína	k1gFnSc1
podporoval	podporovat	k5eAaImAgInS
zánik	zánik	k1gInSc1
Izraele	Izrael	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
nahrazení	nahrazení	k1gNnPc2
Palestinou	Palestina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Čína	Čína	k1gFnSc1
podporovala	podporovat	k5eAaImAgFnS
Palestinskou	palestinský	k2eAgFnSc4d1
deklaraci	deklarace	k1gFnSc4
nezávislosti	nezávislost	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1988	#num#	k4
v	v	k7c6
alžírském	alžírský	k2eAgInSc6d1
Alžíru	Alžír	k1gInSc6
vyhlásil	vyhlásit	k5eAaPmAgMnS
Jásir	Jásir	k1gMnSc1
Arafat	Arafat	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
navzdory	navzdory	k7c3
námitkám	námitka	k1gFnPc3
Izraele	Izrael	k1gInSc2
i	i	k8xC
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc4d1
stát	stát	k1gInSc4
Palestina	Palestina	k1gFnSc1
uznala	uznat	k5eAaPmAgFnS
Čína	Čína	k1gFnSc1
dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1988	#num#	k4
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1989	#num#	k4
s	s	k7c7
ní	on	k3xPp3gFnSc7
navázala	navázat	k5eAaPmAgFnS
úplné	úplný	k2eAgInPc4d1
diplomatické	diplomatický	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Čína	Čína	k1gFnSc1
hlasovala	hlasovat	k5eAaImAgFnS
pro	pro	k7c4
rezoluci	rezoluce	k1gFnSc4
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
2334	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
odsuzuje	odsuzovat	k5eAaImIp3nS
budování	budování	k1gNnSc4
izraelských	izraelský	k2eAgFnPc2d1
osad	osada	k1gFnPc2
na	na	k7c6
Západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Jordánu	Jordán	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
obvykle	obvykle	k6eAd1
zaujímá	zaujímat	k5eAaImIp3nS
pozice	pozice	k1gFnPc4
sympatizující	sympatizující	k2eAgFnPc4d1
s	s	k7c7
záležitostmi	záležitost	k1gFnPc7
Palestiny	Palestina	k1gFnSc2
v	v	k7c4
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
prezident	prezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
na	na	k7c6
schůzce	schůzka	k1gFnSc6
s	s	k7c7
Arabskou	arabský	k2eAgFnSc7d1
ligou	liga	k1gFnSc7
potvrdil	potvrdit	k5eAaPmAgInS
čínskou	čínský	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
„	„	k?
<g/>
založení	založení	k1gNnSc2
palestinského	palestinský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
východní	východní	k2eAgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Česko-čínské	česko-čínský	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Formální	formální	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
mezi	mezi	k7c7
tehdejším	tehdejší	k2eAgNnSc7d1
Československem	Československo	k1gNnSc7
a	a	k8xC
Činou	čina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztahy	vztah	k1gInPc1
se	se	k3xPyFc4
prohloubily	prohloubit	k5eAaPmAgInP
díky	díky	k7c3
obchodu	obchod	k1gInSc3
a	a	k8xC
turismu	turismus	k1gInSc3
mezi	mezi	k7c7
těmito	tento	k3xDgFnPc7
zeměmi	zem	k1gFnPc7
zejména	zejména	k6eAd1
po	po	k7c6
začátku	začátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československo	Československo	k1gNnSc1
bylo	být	k5eAaImAgNnS
první	první	k4xOgFnSc7
zemí	zem	k1gFnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
začala	začít	k5eAaPmAgFnS
s	s	k7c7
Čínou	Čína	k1gFnSc7
jednat	jednat	k5eAaImF
o	o	k7c6
smlouvě	smlouva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mírové	mírový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgNnSc3
setkání	setkání	k1gNnSc3
ministrů	ministr	k1gMnPc2
zahraničí	zahraničí	k1gNnSc4
obou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
domluvili	domluvit	k5eAaPmAgMnP
na	na	k7c6
vzájemném	vzájemný	k2eAgNnSc6d1
uznání	uznání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
tedy	tedy	k8xC
Čína	Čína	k1gFnSc1
uznala	uznat	k5eAaPmAgFnS
nově	nova	k1gFnSc3
vzniklou	vzniklý	k2eAgFnSc7d1
Československou	československý	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
pouze	pouze	k6eAd1
de	de	k?
facto	facto	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
uznání	uznání	k1gNnSc3
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
právem	právo	k1gNnSc7
došlo	dojít	k5eAaPmAgNnS
až	až	k9
o	o	k7c4
jedenáct	jedenáct	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
Česko-slovensko-čínská	Česko-slovensko-čínský	k2eAgFnSc1d1
přátelská	přátelský	k2eAgFnSc1d1
a	a	k8xC
obchodní	obchodní	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
navázání	navázání	k1gNnSc3
diplomatických	diplomatický	k2eAgInPc2d1
styků	styk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
se	se	k3xPyFc4
česko-čínské	česko-čínský	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
zhoršily	zhoršit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vypovězena	vypovědět	k5eAaPmNgFnS
sesterská	sesterský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
mezi	mezi	k7c7
Prahou	Praha	k1gFnSc7
a	a	k8xC
Pekingem	Peking	k1gInSc7
kvůli	kvůli	k7c3
sporům	spor	k1gInPc3
o	o	k7c6
jejím	její	k3xOp3gInSc6
politickém	politický	k2eAgInSc6d1
obsahu	obsah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
následně	následně	k6eAd1
navíc	navíc	k6eAd1
podepsala	podepsat	k5eAaPmAgFnS
partnerství	partnerství	k1gNnSc4
s	s	k7c7
Tchaj-pejí	Tchaj-pej	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
přerušila	přerušit	k5eAaPmAgFnS
s	s	k7c7
Prahou	Praha	k1gFnSc7
styky	styk	k1gInPc1
také	také	k9
Šanghaj	Šanghaj	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
dále	daleko	k6eAd2
kritizovala	kritizovat	k5eAaImAgFnS
i	i	k8xC
plánovanou	plánovaný	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
zesnulého	zesnulý	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
Senátu	senát	k1gInSc2
Jaroslava	Jaroslav	k1gMnSc2
Kubery	Kubera	k1gFnSc2
na	na	k7c4
Tchaj-wan	Tchaj-wan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2020	#num#	k4
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
dopis	dopis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
zaslalo	zaslat	k5eAaPmAgNnS
čínské	čínský	k2eAgNnSc1d1
velvyslanectví	velvyslanectví	k1gNnSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
ledna	leden	k1gInSc2
Jaroslavu	Jaroslav	k1gMnSc3
Kuberovi	Kuber	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dopise	dopis	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
napsán	napsán	k2eAgInSc1d1
velmi	velmi	k6eAd1
nestandardní	standardní	k2eNgFnSc7d1
formou	forma	k1gFnSc7
diplomatické	diplomatický	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
sdělilo	sdělit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
Kubera	Kubera	k1gFnSc1
cestu	cesta	k1gFnSc4
na	na	k7c4
Tchaj-wan	Tchaj-wan	k1gInSc4
uskuteční	uskutečnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
lze	lze	k6eAd1
předpokládat	předpokládat	k5eAaImF
politické	politický	k2eAgInPc4d1
i	i	k8xC
ekonomické	ekonomický	k2eAgInPc4d1
negativní	negativní	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Čínské	čínský	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
zahraniční	zahraniční	k2eAgNnSc1d1
<g/>
,	,	kIx,
agentuře	agentura	k1gFnSc3
Reuters	Reutersa	k1gFnPc2
následně	následně	k6eAd1
sdělilo	sdělit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
informace	informace	k1gFnPc1
vzaly	vzít	k5eAaPmAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Vztahy	vztah	k1gInPc1
Číny	Čína	k1gFnSc2
a	a	k8xC
Německa	Německo	k1gNnSc2
započaly	započnout	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1861	#num#	k4
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
ještě	ještě	k9
mezi	mezi	k7c7
říší	říš	k1gFnSc7
Čching	Čching	k1gInSc1
a	a	k8xC
Pruskem	Prusko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínsko-německá	čínsko-německý	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
se	se	k3xPyFc4
zhroutila	zhroutit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
v	v	k7c6
důsledku	důsledek	k1gInSc6
zahájení	zahájení	k1gNnSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
přinutila	přinutit	k5eAaPmAgFnS
mnoho	mnoho	k4c4
čínských	čínský	k2eAgMnPc2d1
státních	státní	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
opustit	opustit	k5eAaPmF
Německo	Německo	k1gNnSc4
kvůli	kvůli	k7c3
zvýšenému	zvýšený	k2eAgInSc3d1
vládnímu	vládní	k2eAgInSc3d1
dohledu	dohled	k1gInSc3
a	a	k8xC
nátlaku	nátlak	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
znovusjednocení	znovusjednocení	k1gNnSc6
Německa	Německo	k1gNnSc2
se	s	k7c7
vztahy	vztah	k1gInPc7
mezi	mezi	k7c7
Německem	Německo	k1gNnSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
výrazně	výrazně	k6eAd1
zlepšily	zlepšit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Foreign	Foreign	k1gInSc1
relations	relations	k1gInSc1
of	of	k?
China	China	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ECHO	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínská	čínský	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Srí	Srí	k1gFnSc6
Lance	lance	k1gNnSc2
ukrojí	ukrojit	k5eAaPmIp3nS
15	#num#	k4
tisíc	tisíc	k4xCgInPc2
akrů	akr	k1gInPc2
pro	pro	k7c4
milion	milion	k4xCgInSc4
lidí	člověk	k1gMnPc2
-	-	kIx~
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-08-05	2016-08-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ABI-HABIB	ABI-HABIB	k1gMnSc4
<g/>
,	,	kIx,
Maria	Mario	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
China	China	k1gFnSc1
Got	Got	k1gFnSc1
Sri	Sri	k1gFnSc2
Lanka	lanko	k1gNnSc2
to	ten	k3xDgNnSc4
Cough	Cough	k1gMnSc1
Up	Up	k1gMnSc1
a	a	k8xC
Port	port	k1gInSc1
(	(	kIx(
<g/>
Published	Published	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAVIES	DAVIES	kA
<g/>
,	,	kIx,
Gloria	Gloria	k1gFnSc1
<g/>
,	,	kIx,
Jeremy	Jerema	k1gFnPc1
GOLDKORN	GOLDKORN	kA
a	a	k8xC
Luigi	Luigi	k1gNnSc7
TOMBA	TOMBA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
One	One	k1gMnSc1
Belt	Belt	k1gMnSc1
One	One	k1gMnSc1
Road	Road	k1gMnSc1
<g/>
:	:	kIx,
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
development	development	k1gMnSc1
finance	finance	k1gFnSc2
with	with	k1gMnSc1
Chinese	Chinese	k1gFnSc2
characteristics	characteristics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pollution	Pollution	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australia	Australia	k1gFnSc1
<g/>
:	:	kIx,
ANU	ANU	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
245-252	245-252	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
76046	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
69	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
www.jstor.org/	www.jstor.org/	k?
<g/>
…	…	k?
<g/>
934	#num#	k4
<g/>
↑	↑	k?
Sino-Israeli	Sino-Israel	k1gInSc6
Economic	Economice	k1gFnPc2
Ties	Tiesa	k1gFnPc2
Blossoming	Blossoming	k1gInSc1
<g/>
.	.	kIx.
thediplomat	thediplomat	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bloomberg	Bloomberg	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomberg	Bloomberg	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Iran	Iran	k1gMnSc1
approves	approves	k1gMnSc1
visa-free	visa-freat	k5eAaPmIp3nS
travel	travel	k1gInSc1
for	forum	k1gNnPc2
Chinese	Chinese	k1gFnSc2
tourists	tourists	k6eAd1
<g/>
.	.	kIx.
www.chinadaily.com.cn	www.chinadaily.com.cn	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
International	International	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Energy	Energ	k1gInPc1
Information	Information	k1gInSc4
Administration	Administration	k1gInSc1
(	(	kIx(
<g/>
EIA	EIA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.eia.gov	www.eia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DOWNS	DOWNS	kA
<g/>
,	,	kIx,
Erica	Erica	k1gFnSc1
<g/>
;	;	kIx,
MALONEY	MALONEY	kA
<g/>
,	,	kIx,
Suzanne	Suzann	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Getting	Getting	k1gInSc1
China	China	k1gFnSc1
to	ten	k3xDgNnSc1
Sanction	Sanction	k1gInSc4
Iran	Iran	k1gInSc1
<g/>
.	.	kIx.
www.foreignaffairs.com	www.foreignaffairs.com	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
7120	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
China	China	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Xi	Xi	k1gFnSc7
calls	calls	k6eAd1
for	forum	k1gNnPc2
creation	creation	k1gInSc1
of	of	k?
Palestinian	Palestinian	k1gInSc1
state	status	k1gInSc5
<g/>
.	.	kIx.
www.aljazeera.com	www.aljazeera.com	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BAKEŠOVÁ	BAKEŠOVÁ	kA
<g/>
,	,	kIx,
Ivana	Ivana	k1gFnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
KUČERA	Kučera	k1gMnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
LAVIČKA	Lavička	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
448	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
596	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Za	za	k7c4
návštěvu	návštěva	k1gFnSc4
Tchaj-wanu	Tchaj-wan	k1gInSc2
budete	být	k5eAaImBp2nP
platit	platit	k5eAaImF
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Kubera	Kubera	k1gFnSc1
si	se	k3xPyFc3
z	z	k7c2
Hradu	hrad	k1gInSc2
přinesl	přinést	k5eAaPmAgMnS
výhrůžky	výhrůžka	k1gFnPc4
od	od	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-19	2020-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CAREY	CAREY	kA
<g/>
,	,	kIx,
Raphael	Raphael	k1gMnSc1
Satter	Satter	k1gMnSc1
<g/>
,	,	kIx,
Nick	Nick	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
China	China	k1gFnSc1
threatened	threatened	k1gInSc1
to	ten	k3xDgNnSc4
harm	harm	k6eAd1
Czech	Czech	k1gMnSc1
companies	companies	k1gMnSc1
over	over	k1gMnSc1
Taiwan	Taiwan	k1gMnSc1
visit	visita	k1gFnPc2
<g/>
:	:	kIx,
letter	lettra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
</s>
