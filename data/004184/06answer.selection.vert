<s>
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
romantická	romantický	k2eAgFnSc1d1	romantická
komedie	komedie	k1gFnSc1	komedie
režiséra	režisér	k1gMnSc2	režisér
Saši	Saša	k1gMnSc2	Saša
Gedeona	Gedeon	k1gMnSc2	Gedeon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
knížete	kníže	k1gMnSc2	kníže
Lva	Lev	k1gMnSc2	Lev
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Myškina	Myškin	k2eAgInSc2d1	Myškin
románu	román	k1gInSc2	román
Idiot	idiot	k1gMnSc1	idiot
od	od	k7c2	od
ruského	ruský	k2eAgMnSc2d1	ruský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Fjodora	Fjodor	k1gMnSc2	Fjodor
Michajloviče	Michajlovič	k1gMnSc2	Michajlovič
Dostojevského	Dostojevský	k2eAgMnSc2d1	Dostojevský
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
úpřimnou	úpřimný	k2eAgFnSc7d1	úpřimná
nezkaženou	zkažený	k2eNgFnSc7d1	nezkažená
duší	duše	k1gFnSc7	duše
<g/>
.	.	kIx.	.
</s>
