<s>
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
romantická	romantický	k2eAgFnSc1d1	romantická
komedie	komedie	k1gFnSc1	komedie
režiséra	režisér	k1gMnSc2	režisér
Saši	Saša	k1gMnSc2	Saša
Gedeona	Gedeon	k1gMnSc2	Gedeon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
knížete	kníže	k1gMnSc2	kníže
Lva	Lev	k1gMnSc2	Lev
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Myškina	Myškin	k2eAgInSc2d1	Myškin
románu	román	k1gInSc2	román
Idiot	idiot	k1gMnSc1	idiot
od	od	k7c2	od
ruského	ruský	k2eAgMnSc2d1	ruský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Fjodora	Fjodor	k1gMnSc2	Fjodor
Michajloviče	Michajlovič	k1gMnSc2	Michajlovič
Dostojevského	Dostojevský	k2eAgMnSc2d1	Dostojevský
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
úpřimnou	úpřimný	k2eAgFnSc7d1	úpřimná
nezkaženou	zkažený	k2eNgFnSc7d1	nezkažená
duší	duše	k1gFnSc7	duše
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
pěti	pět	k4xCc7	pět
Českými	český	k2eAgInPc7d1	český
lvy	lev	k1gInPc7	lev
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
v	v	k7c6	v
11	[number]	k4	11
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
ocenění	ocenění	k1gNnSc4	ocenění
včetně	včetně	k7c2	včetně
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
ledňáčka	ledňáček	k1gMnSc2	ledňáček
na	na	k7c4	na
Finále	finále	k1gNnSc4	finále
Plzeň	Plzeň	k1gFnSc4	Plzeň
<g/>
,	,	kIx,	,
Kristiána	Kristián	k1gMnSc4	Kristián
<g/>
,	,	kIx,	,
Ceny	cena	k1gFnPc1	cena
českých	český	k2eAgMnPc2d1	český
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
Trilobita	trilobit	k1gMnSc2	trilobit
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmových	filmový	k2eAgInPc6d1	filmový
festivalech	festival	k1gInPc6	festival
Sao	Sao	k1gFnSc2	Sao
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
nebo	nebo	k8xC	nebo
Prix	Prix	k1gInSc4	Prix
Europa	Europa	k1gFnSc1	Europa
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
také	také	k9	také
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gMnSc5	Databas
</s>
