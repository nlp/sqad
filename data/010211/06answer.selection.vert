<s>
Botswana	Botswana	k1gFnSc1	Botswana
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Afriky	Afrika	k1gFnSc2	Afrika
sousedící	sousedící	k2eAgFnPc4d1	sousedící
s	s	k7c7	s
Jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
republikou	republika	k1gFnSc7	republika
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Namibií	Namibie	k1gFnSc7	Namibie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Zambií	Zambie	k1gFnSc7	Zambie
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gFnSc6	Zimbabwe
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
.	.	kIx.	.
</s>
