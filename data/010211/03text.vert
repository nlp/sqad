<p>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Afriky	Afrika	k1gFnSc2	Afrika
sousedící	sousedící	k2eAgFnPc4d1	sousedící
s	s	k7c7	s
Jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
republikou	republika	k1gFnSc7	republika
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Namibií	Namibie	k1gFnSc7	Namibie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Zambií	Zambie	k1gFnSc7	Zambie
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gFnSc6	Zimbabwe
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
Botswany	Botswana	k1gFnSc2	Botswana
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc2	Namibie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
a	a	k8xC	a
Zambie	Zambie	k1gFnPc1	Zambie
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
v	v	k7c6	v
pořadí	pořadí	k1gNnSc2	pořadí
třetí	třetí	k4xOgInSc1	třetí
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejvyspělejší	vyspělý	k2eAgInSc1d3	nejvyspělejší
stát	stát	k1gInSc1	stát
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885	[number]	k4	1885
až	až	k8xS	až
1966	[number]	k4	1966
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Bečuánsko	Bečuánsko	k1gNnSc4	Bečuánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Botswany	Botswana	k1gFnSc2	Botswana
původně	původně	k6eAd1	původně
žili	žít	k5eAaImAgMnP	žít
Sanové	Sanus	k1gMnPc1	Sanus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	on	k3xPp3gNnPc4	on
začali	začít	k5eAaPmAgMnP	začít
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
bantuští	bantuský	k2eAgMnPc1d1	bantuský
Bečuánci	Bečuánek	k1gMnPc1	Bečuánek
<g/>
.	.	kIx.	.
</s>
<s>
Sanové	Sanus	k1gMnPc1	Sanus
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
Kalahari	Kalahar	k1gFnSc2	Kalahar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
působit	působit	k5eAaImF	působit
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
misionářská	misionářský	k2eAgFnSc1d1	misionářská
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
několik	několik	k4yIc1	několik
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Botswanu	Botswana	k1gFnSc4	Botswana
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
poznával	poznávat	k5eAaImAgMnS	poznávat
i	i	k9	i
známý	známý	k2eAgMnSc1d1	známý
cestovatel	cestovatel	k1gMnSc1	cestovatel
D.	D.	kA	D.
Livingstone	Livingston	k1gInSc5	Livingston
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
mezikmenovým	mezikmenův	k2eAgFnPc3d1	mezikmenův
násilnostem	násilnost	k1gFnPc3	násilnost
a	a	k8xC	a
výbojům	výboj	k1gInPc3	výboj
Burů	Bur	k1gMnPc2	Bur
z	z	k7c2	z
Transvaalu	Transvaal	k1gInSc2	Transvaal
<g/>
.	.	kIx.	.
</s>
<s>
Khama	Khama	k1gFnSc1	Khama
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
největšího	veliký	k2eAgInSc2d3	veliký
botswanského	botswanský	k2eAgInSc2d1	botswanský
kmene	kmen	k1gInSc2	kmen
Tswana	Tswana	k1gFnSc1	Tswana
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1885	[number]	k4	1885
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zřídila	zřídit	k5eAaPmAgFnS	zřídit
protektorát	protektorát	k1gInSc4	protektorát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bečuánsko	Bečuánsko	k1gNnSc1	Bečuánsko
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
ke	k	k7c3	k
Kapsku	Kapsko	k1gNnSc3	Kapsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pracovala	pracovat	k5eAaImAgFnS	pracovat
řada	řada	k1gFnSc1	řada
místních	místní	k2eAgMnPc2d1	místní
lidí	člověk	k1gMnPc2	člověk
jako	jako	k8xC	jako
dělníci	dělník	k1gMnPc1	dělník
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
získala	získat	k5eAaPmAgFnS	získat
Botswana	Botswana	k1gFnSc1	Botswana
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Bečuánsko	Bečuánsko	k1gNnSc1	Bečuánsko
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
zněl	znět	k5eAaImAgInS	znět
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Botswany	Botswana	k1gFnSc2	Botswana
<g/>
,	,	kIx,	,
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
byla	být	k5eAaImAgFnS	být
předtím	předtím	k6eAd1	předtím
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
částí	část	k1gFnPc2	část
britského	britský	k2eAgNnSc2d1	Britské
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
zlom	zlom	k1gInSc4	zlom
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Botswany	Botswana	k1gFnSc2	Botswana
přišel	přijít	k5eAaPmAgInS	přijít
již	již	k6eAd1	již
po	po	k7c6	po
roce	rok	k1gInSc6	rok
jejího	její	k3xOp3gInSc2	její
samostatného	samostatný	k2eAgInSc2d1	samostatný
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
objevena	objeven	k2eAgNnPc1d1	objeveno
naleziště	naleziště	k1gNnPc1	naleziště
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1975	[number]	k4	1975
až	až	k9	až
1990	[number]	k4	1990
rostlo	růst	k5eAaImAgNnS	růst
její	její	k3xOp3gNnSc1	její
hospodářství	hospodářství	k1gNnSc1	hospodářství
nejrychleji	rychle	k6eAd3	rychle
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
600	[number]	k4	600
370	[number]	k4	370
km2	km2	k4	km2
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
45	[number]	k4	45
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
po	po	k7c6	po
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
rovinaté	rovinatý	k2eAgNnSc1d1	rovinaté
s	s	k7c7	s
několika	několik	k4yIc7	několik
náhorními	náhorní	k2eAgFnPc7d1	náhorní
plošinami	plošina	k1gFnPc7	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
poušť	poušť	k1gFnSc1	poušť
Kalahari	Kalahar	k1gFnSc2	Kalahar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
%	%	kIx~	%
území	území	k1gNnSc6	území
Botswany	Botswana	k1gFnSc2	Botswana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
delta	delta	k1gFnSc1	delta
řeky	řeka	k1gFnSc2	řeka
Okavango	Okavango	k1gNnSc1	Okavango
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c4	v
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
mokřady	mokřad	k1gInPc4	mokřad
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
vnitrozemskou	vnitrozemský	k2eAgFnSc4d1	vnitrozemská
deltu	delta	k1gFnSc4	delta
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významnější	významný	k2eAgInPc4d2	významnější
toky	tok	k1gInPc4	tok
patří	patřit	k5eAaImIp3nS	patřit
Limpopo	Limpopa	k1gFnSc5	Limpopa
<g/>
,	,	kIx,	,
Shashe	Shashe	k1gFnPc3	Shashe
a	a	k8xC	a
Linyanti	Linyant	k1gMnPc1	Linyant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Tsodilo	Tsodilo	k1gFnSc1	Tsodilo
(	(	kIx(	(
<g/>
1	[number]	k4	1
489	[number]	k4	489
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
zaručena	zaručen	k2eAgFnSc1d1	zaručena
svoboda	svoboda	k1gFnSc1	svoboda
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
zde	zde	k6eAd1	zde
reálná	reálný	k2eAgFnSc1d1	reálná
politická	politický	k2eAgFnSc1d1	politická
opozice	opozice	k1gFnSc1	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
získání	získání	k1gNnSc2	získání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
v	v	k7c6	v
Botswaně	Botswana	k1gFnSc6	Botswana
vládne	vládnout	k5eAaImIp3nS	vládnout
jediná	jediný	k2eAgFnSc1d1	jediná
strana	strana	k1gFnSc1	strana
<g/>
:	:	kIx,	:
Botswanská	botswanský	k2eAgFnSc1d1	Botswanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
BDS	BDS	kA	BDS
opakovaně	opakovaně	k6eAd1	opakovaně
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
demokratických	demokratický	k2eAgFnPc6d1	demokratická
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
kandidovaly	kandidovat	k5eAaImAgFnP	kandidovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
příčinami	příčina	k1gFnPc7	příčina
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
BDS	BDS	kA	BDS
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnPc1	její
zásluhy	zásluha	k1gFnPc1	zásluha
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Botswany	Botswana	k1gFnSc2	Botswana
<g/>
,	,	kIx,	,
zásluhy	zásluha	k1gFnPc4	zásluha
všech	všecek	k3xTgMnPc2	všecek
tří	tři	k4xCgMnPc2	tři
prezidentů	prezident	k1gMnPc2	prezident
vzešlých	vzešlý	k2eAgMnPc2d1	vzešlý
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
řad	řada	k1gFnPc2	řada
(	(	kIx(	(
<g/>
Seretse	Seretse	k1gFnSc1	Seretse
Khama	Khama	k1gFnSc1	Khama
<g/>
,	,	kIx,	,
Quett	Quett	k1gInSc1	Quett
Ketumile	Ketumil	k1gMnSc5	Ketumil
Masire	Masir	k1gMnSc5	Masir
<g/>
,	,	kIx,	,
Festus	Festus	k1gMnSc1	Festus
Mogae	Moga	k1gFnSc2	Moga
<g/>
)	)	kIx)	)
a	a	k8xC	a
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
růstový	růstový	k2eAgInSc1d1	růstový
potenciál	potenciál	k1gInSc1	potenciál
ztělesněný	ztělesněný	k2eAgInSc1d1	ztělesněný
ve	v	k7c6	v
vzdělaných	vzdělaný	k2eAgInPc6d1	vzdělaný
členech	člen	k1gInPc6	člen
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
získala	získat	k5eAaPmAgFnS	získat
BDS	BDS	kA	BDS
54,2	[number]	k4	54,2
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
celých	celý	k2eAgInPc2d1	celý
21	[number]	k4	21
<g/>
%	%	kIx~	%
méně	málo	k6eAd2	málo
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
mírně	mírně	k6eAd1	mírně
posilují	posilovat	k5eAaImIp3nP	posilovat
opoziční	opoziční	k2eAgFnPc1d1	opoziční
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Botswanská	botswanský	k2eAgFnSc1d1	Botswanská
národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vůdčím	vůdčí	k2eAgInSc7d1	vůdčí
prvkem	prvek	k1gInSc7	prvek
politické	politický	k2eAgFnSc2d1	politická
opozice	opozice	k1gFnSc2	opozice
již	již	k6eAd1	již
od	od	k7c2	od
zrodu	zrod	k1gInSc2	zrod
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
Botswanská	botswanský	k2eAgFnSc1d1	Botswanská
kongresová	kongresový	k2eAgFnSc1d1	kongresová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
do	do	k7c2	do
volebních	volební	k2eAgInPc2d1	volební
bojů	boj	k1gInPc2	boj
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
až	až	k9	až
v	v	k7c6	v
předposledních	předposlední	k2eAgFnPc6d1	předposlední
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získala	získat	k5eAaPmAgFnS	získat
11,3	[number]	k4	11,3
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2004	[number]	k4	2004
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
BDS	BDS	kA	BDS
(	(	kIx(	(
<g/>
51,7	[number]	k4	51,7
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
před	před	k7c7	před
BNF	BNF	kA	BNF
(	(	kIx(	(
<g/>
26,1	[number]	k4	26,1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
BKS	BKS	kA	BKS
(	(	kIx(	(
<g/>
16,6	[number]	k4	16,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
voličská	voličský	k2eAgFnSc1d1	voličská
opora	opora	k1gFnSc1	opora
BDS	BDS	kA	BDS
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
opoziční	opoziční	k2eAgFnSc2d1	opoziční
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
především	především	k9	především
BNF	BNF	kA	BNF
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
těší	těšit	k5eAaImIp3nP	těšit
přízni	přízeň	k1gFnSc3	přízeň
zejména	zejména	k9	zejména
u	u	k7c2	u
městského	městský	k2eAgNnSc2d1	Městské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Slabost	slabost	k1gFnSc1	slabost
botswanské	botswanský	k2eAgFnSc2d1	Botswanská
opozice	opozice	k1gFnSc2	opozice
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
důsledek	důsledek	k1gInSc1	důsledek
její	její	k3xOp3gFnSc2	její
značné	značný	k2eAgFnSc2d1	značná
roztříštěnosti	roztříštěnost	k1gFnSc2	roztříštěnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
podařilo	podařit	k5eAaPmAgNnS	podařit
ustavit	ustavit	k5eAaPmF	ustavit
koalici	koalice	k1gFnSc4	koalice
BNF	BNF	kA	BNF
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
menších	malý	k2eAgFnPc2d2	menší
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
atomizace	atomizace	k1gFnSc2	atomizace
opozičních	opoziční	k2eAgFnPc2d1	opoziční
stran	strana	k1gFnPc2	strana
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
doložit	doložit	k5eAaPmF	doložit
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
Nové	Nové	k2eAgFnSc2d1	Nové
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
BNF	BNF	kA	BNF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
má	mít	k5eAaImIp3nS	mít
Botswana	Botswana	k1gFnSc1	Botswana
i	i	k9	i
druhou	druhý	k4xOgFnSc4	druhý
složku	složka	k1gFnSc4	složka
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
moci	moc	k1gFnSc2	moc
–	–	k?	–
na	na	k7c6	na
etnickém	etnický	k2eAgNnSc6d1	etnické
zastoupení	zastoupení	k1gNnSc6	zastoupení
založený	založený	k2eAgInSc4d1	založený
Sněm	sněm	k1gInSc4	sněm
náčelníků	náčelník	k1gMnPc2	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
patnáctičlenný	patnáctičlenný	k2eAgInSc1d1	patnáctičlenný
útvar	útvar	k1gInSc1	útvar
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
osmi	osm	k4xCc2	osm
náčelníků	náčelník	k1gMnPc2	náčelník
tswanských	tswanský	k2eAgInPc2d1	tswanský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
čtyř	čtyři	k4xCgMnPc2	čtyři
zástupců	zástupce	k1gMnPc2	zástupce
menšinových	menšinový	k2eAgInPc2d1	menšinový
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
tří	tři	k4xCgInPc2	tři
členů	člen	k1gInPc2	člen
volených	volený	k2eAgInPc2d1	volený
samotným	samotný	k2eAgInSc7d1	samotný
sněmem	sněm	k1gInSc7	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Dané	daný	k2eAgNnSc1d1	dané
složení	složení	k1gNnSc1	složení
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
menšinovým	menšinový	k2eAgInPc3d1	menšinový
kmenům	kmen	k1gInPc3	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zástupci	zástupce	k1gMnPc1	zástupce
kmene	kmen	k1gInSc2	kmen
Bangwaketse	Bangwaketse	k1gFnSc1	Bangwaketse
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
BDS	BDS	kA	BDS
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
svého	svůj	k3xOyFgMnSc4	svůj
etnického	etnický	k2eAgMnSc4d1	etnický
příslušníka	příslušník	k1gMnSc4	příslušník
do	do	k7c2	do
Sněmu	sněm	k1gInSc2	sněm
náčelníků	náčelník	k1gMnPc2	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Sněm	sněm	k1gInSc1	sněm
náčelníků	náčelník	k1gMnPc2	náčelník
však	však	k9	však
zásluhou	zásluha	k1gFnSc7	zásluha
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
opatření	opatření	k1gNnPc2	opatření
BDS	BDS	kA	BDS
a	a	k8xC	a
Seretse	Seretse	k1gFnSc1	Seretse
Khamy	Khama	k1gFnSc2	Khama
nehraje	hrát	k5eNaImIp3nS	hrát
zásadnější	zásadní	k2eAgFnSc4d2	zásadnější
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jistého	jistý	k2eAgInSc2d1	jistý
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
legislativy	legislativa	k1gFnSc2	legislativa
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
kmenových	kmenový	k2eAgFnPc2d1	kmenová
záležitostí	záležitost	k1gFnPc2	záležitost
a	a	k8xC	a
zvykového	zvykový	k2eAgNnSc2d1	zvykové
práva	právo	k1gNnSc2	právo
plní	plnit	k5eAaImIp3nS	plnit
tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
výlučně	výlučně	k6eAd1	výlučně
poradní	poradní	k2eAgFnSc4d1	poradní
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
svojí	svůj	k3xOyFgFnSc7	svůj
symbolickou	symbolický	k2eAgFnSc7d1	symbolická
povahou	povaha	k1gFnSc7	povaha
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
britské	britský	k2eAgFnSc3d1	britská
Sněmovně	sněmovna	k1gFnSc3	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Seretse	Serets	k1gMnSc2	Serets
Khama	Khamum	k1gNnSc2	Khamum
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
BDP	BDP	kA	BDP
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Quett	Quett	k1gMnSc1	Quett
Ketumile	Ketumil	k1gMnSc5	Ketumil
Joni	Jon	k1gMnSc5	Jon
Masire	Masir	k1gMnSc5	Masir
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
BDP	BDP	kA	BDP
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Quett	Quett	k1gMnSc1	Quett
Ketumile	Ketumil	k1gMnSc5	Ketumil
Joni	Jon	k1gMnSc5	Jon
Masire	Masir	k1gMnSc5	Masir
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
BDP	BDP	kA	BDP
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
Festus	Festus	k1gMnSc1	Festus
Gontebanye	Gontebany	k1gMnSc2	Gontebany
Mogae	Mogae	k1gNnSc2	Mogae
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
BDP	BDP	kA	BDP
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
–	–	k?	–
Seretse	Serets	k1gMnSc4	Serets
Khama	Kham	k1gMnSc4	Kham
Ian	Ian	k1gMnSc4	Ian
Khama	Kham	k1gMnSc4	Kham
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
BDP	BDP	kA	BDP
</s>
</p>
<p>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
–	–	k?	–
Mokgweetsi	Mokgweets	k1gMnSc3	Mokgweets
Eric	Eric	k1gFnSc4	Eric
Keabetswe	Keabetswe	k1gFnSc3	Keabetswe
Masisi	Masise	k1gFnSc3	Masise
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
BDP	BDP	kA	BDP
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Botswany	Botswana	k1gFnSc2	Botswana
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
devíti	devět	k4xCc2	devět
distriktů	distrikt	k1gInPc2	distrikt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
</s>
</p>
<p>
<s>
Ghanzi	Ghanh	k1gMnPc1	Ghanh
</s>
</p>
<p>
<s>
Kgalagadi	Kgalagad	k1gMnPc1	Kgalagad
</s>
</p>
<p>
<s>
Kgatleng	Kgatleng	k1gMnSc1	Kgatleng
</s>
</p>
<p>
<s>
Kweneng	Kweneng	k1gMnSc1	Kweneng
</s>
</p>
<p>
<s>
Severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
</s>
</p>
<p>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
</s>
</p>
<p>
<s>
Jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
</s>
</p>
<p>
<s>
JižníV	JižníV	k?	JižníV
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
distrikty	distrikt	k1gInPc1	distrikt
Chobe	Chob	k1gInSc5	Chob
a	a	k8xC	a
Ngamiland	Ngamilanda	k1gFnPc2	Ngamilanda
spojily	spojit	k5eAaPmAgFnP	spojit
v	v	k7c4	v
dnešní	dnešní	k2eAgInSc4d1	dnešní
Severozápadní	severozápadní	k2eAgInSc4d1	severozápadní
distrikt	distrikt	k1gInSc4	distrikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1975	[number]	k4	1975
až	až	k9	až
1990	[number]	k4	1990
rostlo	růst	k5eAaImAgNnS	růst
hospodářství	hospodářství	k1gNnSc1	hospodářství
Botswany	Botswana	k1gFnSc2	Botswana
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
těžbě	těžba	k1gFnSc3	těžba
diamantů	diamant	k1gInPc2	diamant
nejrychleji	rychle	k6eAd3	rychle
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Političtí	politický	k2eAgMnPc1d1	politický
představitelé	představitel	k1gMnPc1	představitel
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
podporovali	podporovat	k5eAaImAgMnP	podporovat
příliv	příliv	k1gInSc4	příliv
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
kapitálu	kapitál	k1gInSc2	kapitál
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
cizím	cizí	k2eAgMnPc3d1	cizí
investorům	investor	k1gMnPc3	investor
nabízet	nabízet	k5eAaImF	nabízet
bezpečné	bezpečný	k2eAgFnPc4d1	bezpečná
podnikatelské	podnikatelský	k2eAgFnPc4d1	podnikatelská
prostředí	prostředí	k1gNnPc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nemalé	malý	k2eNgFnPc1d1	nemalá
zásluhy	zásluha	k1gFnPc1	zásluha
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
Botswany	Botswana	k1gFnSc2	Botswana
má	mít	k5eAaImIp3nS	mít
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pocházela	pocházet	k5eAaImAgFnS	pocházet
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
britských	britský	k2eAgInPc2d1	britský
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
ODA	ODA	kA	ODA
<g/>
,	,	kIx,	,
OXFAM	OXFAM	kA	OXFAM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
amerických	americký	k2eAgFnPc2d1	americká
(	(	kIx(	(
<g/>
USAID	USAID	kA	USAID
<g/>
)	)	kIx)	)
a	a	k8xC	a
skandinávských	skandinávský	k2eAgInPc2d1	skandinávský
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
DANIDA	DANIDA	kA	DANIDA
<g/>
,	,	kIx,	,
SIDA	SIDA	kA	SIDA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
od	od	k7c2	od
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
až	až	k9	až
1973	[number]	k4	1973
země	země	k1gFnSc1	země
absorbovala	absorbovat	k5eAaBmAgFnS	absorbovat
přibližně	přibližně	k6eAd1	přibližně
22,5	[number]	k4	22,5
milionů	milion	k4xCgInPc2	milion
GBP	GBP	kA	GBP
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
směřovala	směřovat	k5eAaImAgFnS	směřovat
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
projektů	projekt	k1gInPc2	projekt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
infrastruktur	infrastruktura	k1gFnPc2	infrastruktura
(	(	kIx(	(
<g/>
stavby	stavba	k1gFnPc1	stavba
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
telefonních	telefonní	k2eAgFnPc2d1	telefonní
ústředen	ústředna	k1gFnPc2	ústředna
<g/>
,	,	kIx,	,
vysílačů	vysílač	k1gInPc2	vysílač
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Botswanská	botswanský	k2eAgFnSc1d1	Botswanská
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
BDC	BDC	kA	BDC
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
holdingu	holding	k1gInSc2	holding
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
jediným	jediný	k2eAgMnSc7d1	jediný
podílníkem	podílník	k1gMnSc7	podílník
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ponechávala	ponechávat	k5eAaImAgFnS	ponechávat
mu	on	k3xPp3gMnSc3	on
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
volnost	volnost	k1gFnSc4	volnost
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
"	"	kIx"	"
<g/>
investic	investice	k1gFnPc2	investice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
originální	originální	k2eAgInSc4d1	originální
koncept	koncept	k1gInSc4	koncept
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
přítomnost	přítomnost	k1gFnSc1	přítomnost
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
,	,	kIx,	,
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
pracovníků	pracovník	k1gMnPc2	pracovník
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
dobrovolných	dobrovolný	k2eAgFnPc2d1	dobrovolná
<g/>
,	,	kIx,	,
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
rychle	rychle	k6eAd1	rychle
nahrazováni	nahrazován	k2eAgMnPc1d1	nahrazován
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
formující	formující	k2eAgFnSc7d1	formující
botswanskou	botswanský	k2eAgFnSc7d1	Botswanská
elitou	elita	k1gFnSc7	elita
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
různé	různý	k2eAgFnPc1d1	různá
polostátní	polostátní	k2eAgFnPc1d1	polostátní
společnosti	společnost	k1gFnPc1	společnost
kontrolující	kontrolující	k2eAgFnSc4d1	kontrolující
distribuci	distribuce	k1gFnSc4	distribuce
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
produkci	produkce	k1gFnSc4	produkce
potravin	potravina	k1gFnPc2	potravina
<g/>
;	;	kIx,	;
Národní	národní	k2eAgFnSc1d1	národní
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
banka	banka	k1gFnSc1	banka
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
půjčky	půjčka	k1gFnPc4	půjčka
zemědělcům	zemědělec	k1gMnPc3	zemědělec
a	a	k8xC	a
živnostníkům	živnostník	k1gMnPc3	živnostník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžbu	těžba	k1gFnSc4	těžba
diamantů	diamant	k1gInPc2	diamant
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
ovládala	ovládat	k5eAaImAgFnS	ovládat
Debswana	Debswana	k1gFnSc1	Debswana
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc1d1	společný
podnik	podnik	k1gInSc1	podnik
botswanské	botswanský	k2eAgFnSc2d1	Botswanská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
společnosti	společnost	k1gFnSc2	společnost
De	De	k?	De
Beers	Beers	k1gInSc1	Beers
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
podporovala	podporovat	k5eAaImAgFnS	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
nejen	nejen	k6eAd1	nejen
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
sofistikovanějších	sofistikovaný	k2eAgInPc2d2	sofistikovanější
oborů	obor	k1gInPc2	obor
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
krátkodobě	krátkodobě	k6eAd1	krátkodobě
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
i	i	k9	i
vozy	vůz	k1gInPc1	vůz
značky	značka	k1gFnSc2	značka
Hyundai	Hyunda	k1gFnSc2	Hyunda
<g/>
;	;	kIx,	;
společnost	společnost	k1gFnSc1	společnost
MCB	MCB	kA	MCB
(	(	kIx(	(
<g/>
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
of	of	k?	of
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
)	)	kIx)	)
sice	sice	k8xC	sice
musela	muset	k5eAaImAgFnS	muset
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
výrobu	výroba	k1gFnSc4	výroba
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotná	samotný	k2eAgFnSc1d1	samotná
přítomnost	přítomnost	k1gFnSc1	přítomnost
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
dobrou	dobrý	k2eAgFnSc7d1	dobrá
vizitkou	vizitka	k1gFnSc7	vizitka
pro	pro	k7c4	pro
botswanskou	botswanský	k2eAgFnSc4d1	Botswanská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
četných	četný	k2eAgNnPc2d1	četné
prohlášení	prohlášení	k1gNnPc2	prohlášení
vědoma	vědom	k2eAgFnSc1d1	vědoma
omezené	omezený	k2eAgFnSc3d1	omezená
velikosti	velikost	k1gFnSc3	velikost
místního	místní	k2eAgInSc2d1	místní
trhu	trh	k1gInSc2	trh
(	(	kIx(	(
<g/>
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
soustředit	soustředit	k5eAaPmF	soustředit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
investice	investice	k1gFnPc4	investice
podporující	podporující	k2eAgInSc1d1	podporující
vývoz	vývoz	k1gInSc1	vývoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadá	připadat	k5eAaPmIp3nS	připadat
rozvoj	rozvoj	k1gInSc1	rozvoj
zpracování	zpracování	k1gNnSc2	zpracování
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
finančnictví	finančnictví	k1gNnSc1	finančnictví
a	a	k8xC	a
počítačový	počítačový	k2eAgInSc1d1	počítačový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
rozumným	rozumný	k2eAgNnSc7d1	rozumné
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
opatřením	opatření	k1gNnSc7	opatření
brzy	brzy	k6eAd1	brzy
zbavila	zbavit	k5eAaPmAgFnS	zbavit
závislosti	závislost	k1gFnPc4	závislost
na	na	k7c6	na
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
až	až	k9	až
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
minulosti	minulost	k1gFnSc2	minulost
hospodařila	hospodařit	k5eAaImAgFnS	hospodařit
s	s	k7c7	s
přebytky	přebytek	k1gInPc7	přebytek
<g/>
,	,	kIx,	,
Botswanská	botswanský	k2eAgFnSc1d1	Botswanská
pula	pula	k1gFnSc1	pula
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
značnou	značný	k2eAgFnSc4d1	značná
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
disponuje	disponovat	k5eAaBmIp3nS	disponovat
devizovými	devizový	k2eAgFnPc7d1	devizová
rezervami	rezerva	k1gFnPc7	rezerva
přesahujícími	přesahující	k2eAgFnPc7d1	přesahující
sumu	suma	k1gFnSc4	suma
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
příslušníci	příslušník	k1gMnPc1	příslušník
kmene	kmen	k1gInSc2	kmen
Batswana	Batswana	k1gFnSc1	Batswana
(	(	kIx(	(
<g/>
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
kmeny	kmen	k1gInPc7	kmen
jsou	být	k5eAaImIp3nP	být
Kalanga	Kalanga	k1gFnSc1	Kalanga
<g/>
,	,	kIx,	,
Basarwa	Basarwa	k1gFnSc1	Basarwa
a	a	k8xC	a
Kgalagadi	Kgalagad	k1gMnPc1	Kgalagad
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bílí	bílý	k2eAgMnPc1d1	bílý
osadníci	osadník	k1gMnPc1	osadník
představují	představovat	k5eAaImIp3nP	představovat
1	[number]	k4	1
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
původní	původní	k2eAgNnPc4d1	původní
kmenová	kmenový	k2eAgNnPc4d1	kmenové
náboženství	náboženství	k1gNnPc4	náboženství
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epidemie	epidemie	k1gFnSc2	epidemie
AIDS	AIDS	kA	AIDS
===	===	k?	===
</s>
</p>
<p>
<s>
AIDS	AIDS	kA	AIDS
představuje	představovat	k5eAaImIp3nS	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInSc4d1	budoucí
rozvoj	rozvoj	k1gInSc4	rozvoj
botswanského	botswanský	k2eAgNnSc2d1	botswanský
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
nakažena	nakažen	k2eAgFnSc1d1	nakažena
asi	asi	k9	asi
třetina	třetina	k1gFnSc1	třetina
dospělé	dospělý	k2eAgFnSc2d1	dospělá
populace	populace	k1gFnSc2	populace
Botswany	Botswana	k1gFnSc2	Botswana
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
v	v	k7c6	v
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
tento	tento	k3xDgInSc4	tento
podíl	podíl	k1gInSc4	podíl
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DENBOW	DENBOW	kA	DENBOW
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
R.	R.	kA	R.
Culture	Cultur	k1gMnSc5	Cultur
and	and	k?	and
customs	customs	k1gInSc1	customs
of	of	k?	of
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
.	.	kIx.	.
</s>
<s>
Westport	Westport	k1gInSc1	Westport
<g/>
:	:	kIx,	:
Greenwood	Greenwood	k1gInSc1	Greenwood
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
313	[number]	k4	313
<g/>
-	-	kIx~	-
<g/>
33178	[number]	k4	33178
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
;	;	kIx,	;
PROUZA	PROUZA	kA	PROUZA
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
ZÁHOŘÍK	ZÁHOŘÍK	kA	ZÁHOŘÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgNnSc1d1	politické
stranictví	stranictví	k1gNnSc1	stranictví
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
:	:	kIx,	:
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
a	a	k8xC	a
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Iuridicum	Iuridicum	k1gInSc1	Iuridicum
Olomoucense	Olomoucense	k1gFnSc2	Olomoucense
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Právnickou	právnický	k2eAgFnSc7d1	právnická
fakultou	fakulta	k1gFnSc7	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87382	[number]	k4	87382
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PIKNEROVÁ	PIKNEROVÁ	kA	PIKNEROVÁ
<g/>
,	,	kIx,	,
Linda	Linda	k1gFnSc1	Linda
<g/>
.	.	kIx.	.
</s>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
431	[number]	k4	431
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RANDALL	RANDALL	kA	RANDALL
<g/>
,	,	kIx,	,
Will	Will	k1gInSc1	Will
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
Botswany	Botswana	k1gFnSc2	Botswana
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7376	[number]	k4	7376
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
96	[number]	k4	96
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SILLERY	SILLERY	kA	SILLERY
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc7	Anthon
<g/>
.	.	kIx.	.
</s>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
:	:	kIx,	:
a	a	k8xC	a
short	short	k1gInSc1	short
political	politicat	k5eAaPmAgInS	politicat
history	histor	k1gMnPc4	histor
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Methuen	Methuen	k1gInSc1	Methuen
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
416	[number]	k4	416
<g/>
-	-	kIx~	-
<g/>
75650	[number]	k4	75650
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
</s>
</p>
<p>
<s>
Tswana	Tswana	k1gFnSc1	Tswana
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Botswana	Botswana	k1gFnSc1	Botswana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Botswana	Botswana	k1gFnSc1	Botswana
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Botswana	Botswana	k1gFnSc1	Botswana
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
vlády	vláda	k1gFnSc2	vláda
Botswany	Botswana	k1gFnSc2	Botswana
</s>
</p>
<p>
<s>
Geografie	geografie	k1gFnSc1	geografie
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
ekologie	ekologie	k1gFnSc1	ekologie
Botswany	Botswana	k1gFnSc2	Botswana
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Turistika	turistika	k1gFnSc1	turistika
po	po	k7c6	po
Botswaně	Botswana	k1gFnSc6	Botswana
</s>
</p>
<p>
<s>
geohive	geohiev	k1gFnPc1	geohiev
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
-	-	kIx~	-
Botswana	Botswana	k1gFnSc1	Botswana
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Botswana	Botswana	k1gFnSc1	Botswana
Country	country	k2eAgFnSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Botswana	Botswana	k1gFnSc1	Botswana
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-06-29	[number]	k4	2011-06-29
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Botswana	Botswana	k1gFnSc1	Botswana
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2015-10-15	[number]	k4	2015-10-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Botswana	Botswana	k1gFnSc1	Botswana
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-01-24	[number]	k4	2011-01-24
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PARSONS	PARSONS	kA	PARSONS
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
<g/>
.	.	kIx.	.
</s>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
