<s>
Počítač	počítač	k1gInSc1	počítač
Apple	Apple	kA	Apple
II	II	kA	II
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
prototyp	prototyp	k1gInSc1	prototyp
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1977	[number]	k4	1977
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
veletrhu	veletrh	k1gInSc6	veletrh
West	West	k1gInSc1	West
Coast	Coast	k1gInSc1	Coast
Computer	computer	k1gInSc1	computer
Fair	fair	k2eAgInSc1d1	fair
<g/>
,	,	kIx,	,
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
kroků	krok	k1gInPc2	krok
k	k	k7c3	k
osobním	osobní	k2eAgInPc3d1	osobní
počítačům	počítač	k1gInPc3	počítač
<g/>
.	.	kIx.	.
</s>
