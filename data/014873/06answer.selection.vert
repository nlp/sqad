<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Adelaide	Adelaid	k1gInSc5
jediný	jediný	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
A-League	A-Leagu	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
celkem	celkem	k6eAd1
čtyřikrát	čtyřikrát	k4xRc1
do	do	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
AFC	AFC	kA
a	a	k8xC
dokázal	dokázat	k5eAaPmAgInS
postoupit	postoupit	k5eAaPmF
ze	z	k7c2
skupiny	skupina	k1gFnSc2
třikrát	třikrát	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
dělá	dělat	k5eAaImIp3nS
z	z	k7c2
tohoto	tento	k3xDgInSc2
týmu	tým	k1gInSc2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
nejúspěšnějších	úspěšný	k2eAgInPc2d3
australských	australský	k2eAgInPc2d1
fotbalových	fotbalový	k2eAgInPc2d1
klubů	klub	k1gInPc2
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>