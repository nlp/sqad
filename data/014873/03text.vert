<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gMnSc1
FC	FC	kA
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gMnSc1
FCNázev	FCNázev	k1gMnPc2
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gMnSc1
Football	Football	k1gInSc4
Club	club	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Červení	červení	k1gNnSc1
<g/>
,	,	kIx,
United	United	k1gInSc1
Země	zem	k1gFnSc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
Založen	založen	k2eAgInSc1d1
</s>
<s>
2003	#num#	k4
Asociace	asociace	k1gFnSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_AUFC_Kappa_	_AUFC_Kappa_	k?
<g/>
1213	#num#	k4
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_AUFC_Kappa_	_AUFC_Kappa_	k?
<g/>
1213	#num#	k4
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
A-League	A-League	k1gFnSc1
2018	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Hindmarsh	Hindmarsh	k1gMnSc1
Stadium	stadium	k1gNnSc1
<g/>
,	,	kIx,
Adelaide	Adelaid	k1gInSc5
Souřadnice	souřadnice	k1gFnPc5
</s>
<s>
34	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
138	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
8	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
17	#num#	k4
000	#num#	k4
Vedení	vedení	k1gNnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
Greg	Greg	k1gMnSc1
Griffin	Griffin	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Kosmina	Kosmin	k2eAgInSc2d1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Australská	australský	k2eAgFnSc1d1
A-League	A-League	k1gFnSc1
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gMnSc1
FC	FC	kA
je	být	k5eAaImIp3nS
australský	australský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
města	město	k1gNnSc2
Adelaide	Adelaid	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jediným	jediný	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
v	v	k7c6
A-League	A-League	k1gFnSc6
z	z	k7c2
tohoto	tento	k3xDgInSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejúspěšnější	úspěšný	k2eAgInPc4d3
kluby	klub	k1gInPc4
v	v	k7c6
této	tento	k3xDgFnSc6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInSc4d1
zápasy	zápas	k1gInPc4
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
Hindmarsh	Hindmarsh	k1gInSc4
Stadium	stadium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nahradil	nahradit	k5eAaPmAgInS
tým	tým	k1gInSc1
Adelaide	Adelaid	k1gInSc5
City	city	k1gNnSc1
v	v	k7c6
bývalé	bývalý	k2eAgFnSc6d1
National	National	k1gFnSc6
Soccer	Soccra	k1gFnPc2
League	Leagu	k1gFnSc2
(	(	kIx(
<g/>
NLS	NLS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc4
zvítězilo	zvítězit	k5eAaPmAgNnS
v	v	k7c6
základní	základní	k2eAgFnSc6d1
části	část	k1gFnSc6
během	běh	k1gInSc7
zahajovací	zahajovací	k2eAgFnSc3d1
sezóně	sezóna	k1gFnSc3
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
A-League	A-League	k1gNnPc4
s	s	k7c7
náskokem	náskok	k1gInSc7
7	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
ostatní	ostatní	k1gNnSc4
mužstva	mužstvo	k1gNnSc2
<g/>
,	,	kIx,
soutěž	soutěž	k1gFnSc1
dokončilo	dokončit	k5eAaPmAgNnS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reds	Redsa	k1gFnPc2
byli	být	k5eAaImAgMnP
ve	v	k7c6
finále	finále	k1gNnSc6
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2006	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
7	#num#	k4
a	a	k8xC
2008	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
nepodařilo	podařit	k5eNaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
ani	ani	k8xC
jednou	jednou	k6eAd1
zvítězit	zvítězit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2009	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
klub	klub	k1gInSc4
katastrofou	katastrofa	k1gFnSc7
<g/>
,	,	kIx,
špatná	špatný	k2eAgFnSc1d1
forma	forma	k1gFnSc1
mužstva	mužstvo	k1gNnSc2
se	se	k3xPyFc4
podepsala	podepsat	k5eAaPmAgFnS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
tým	tým	k1gInSc1
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
posledním	poslední	k2eAgNnSc6d1
místě	místo	k1gNnSc6
základní	základní	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
vůbec	vůbec	k9
poprvé	poprvé	k6eAd1
od	od	k7c2
založení	založení	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Adelaide	Adelaid	k1gInSc5
jediný	jediný	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
A-League	A-Leagu	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
celkem	celkem	k6eAd1
čtyřikrát	čtyřikrát	k4xRc1
do	do	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
AFC	AFC	kA
a	a	k8xC
dokázal	dokázat	k5eAaPmAgInS
postoupit	postoupit	k5eAaPmF
ze	z	k7c2
skupiny	skupina	k1gFnSc2
třikrát	třikrát	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
dělá	dělat	k5eAaImIp3nS
z	z	k7c2
tohoto	tento	k3xDgInSc2
týmu	tým	k1gInSc2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
nejúspěšnějších	úspěšný	k2eAgInPc2d3
australských	australský	k2eAgInPc2d1
fotbalových	fotbalový	k2eAgInPc2d1
klubů	klub	k1gInPc2
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gMnSc1
patří	patřit	k5eAaImIp3nS
rekord	rekord	k1gInSc4
pro	pro	k7c4
největší	veliký	k2eAgNnSc4d3
vítězství	vítězství	k1gNnSc4
v	v	k7c6
A-League	A-League	k1gFnSc6
a	a	k8xC
také	také	k9
nejvíce	hodně	k6eAd3,k6eAd1
branek	branka	k1gFnPc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
zápase	zápas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adelaide	Adelaid	k1gInSc5
porazilo	porazit	k5eAaPmAgNnS
North	North	k1gInSc4
Queensland	Queensland	k1gInSc1
Fury	Fura	k1gFnSc2
8	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
na	na	k7c4
Hindmarsh	Hindmarsh	k1gInSc4
Stadium	stadium	k1gNnSc1
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2011	#num#	k4
před	před	k7c7
10	#num#	k4
829	#num#	k4
fanoušky	fanoušek	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
také	také	k9
podařilo	podařit	k5eAaPmAgNnS
dvěma	dva	k4xCgFnPc7
hráči	hráč	k1gMnSc3
vstřelit	vstřelit	k5eAaPmF
hattrick	hattrick	k1gInSc4
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
zápase	zápas	k1gInSc6
<g/>
,	,	kIx,
hattrick	hattrick	k1gInSc4
vstřelil	vstřelit	k5eAaPmAgMnS
hráč	hráč	k1gMnSc1
Marcos	Marcos	k1gMnSc1
Flores	Flores	k1gMnSc1
a	a	k8xC
Sergio	Sergio	k1gMnSc1
van	vana	k1gFnPc2
Dijk	Dijk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2003	#num#	k4
Adelaide	Adelaid	k1gInSc5
City	city	k1gNnSc1
opustilo	opustit	k5eAaPmAgNnS
Národní	národní	k2eAgFnSc4d1
fotbalovou	fotbalový	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
(	(	kIx(
<g/>
NSL	NSL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odcházející	odcházející	k2eAgFnSc1d1
Adelaide	Adelaid	k1gInSc5
nemělo	mít	k5eNaImAgNnS
ligovou	ligový	k2eAgFnSc4d1
účast	účast	k1gFnSc4
poprvé	poprvé	k6eAd1
od	od	k7c2
založení	založení	k1gNnSc2
soutěže	soutěž	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reakcí	reakce	k1gFnPc2
na	na	k7c4
tuto	tento	k3xDgFnSc4
událost	událost	k1gFnSc4
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
klub	klub	k1gInSc1
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc1
v	v	k7c6
září	září	k1gNnSc6
2003	#num#	k4
<g/>
,	,	kIx,
tým	tým	k1gInSc1
byl	být	k5eAaImAgInS
složen	složit	k5eAaPmNgInS
během	během	k7c2
pár	pár	k4xCyI
týdnů	týden	k1gInPc2
většinou	většinou	k6eAd1
ze	z	k7c2
zbytků	zbytek	k1gInPc2
hráčského	hráčský	k2eAgInSc2d1
kádru	kádr	k1gInSc2
Adelaide	Adelaid	k1gInSc5
City	city	k1gNnSc1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2003	#num#	k4
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc1
vyhrálo	vyhrát	k5eAaPmAgNnS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
ligový	ligový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
<g/>
,	,	kIx,
proti	proti	k7c3
týmu	tým	k1gInSc3
Brisbane	Brisban	k1gMnSc5
Strikers	Strikers	k1gInSc1
1-0	1-0	k4
před	před	k7c4
více	hodně	k6eAd2
než	než	k8xS
16	#num#	k4
000	#num#	k4
diváky	divák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
strhující	strhující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
zahrnující	zahrnující	k2eAgFnSc7d1
sedmi	sedm	k4xCc2
zápasovou	zápasový	k2eAgFnSc4d1
sérii	série	k1gFnSc3
bez	bez	k7c2
porážky	porážka	k1gFnSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
a	a	k8xC
prosinci	prosinec	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc1
dosáhlo	dosáhnout	k5eAaPmAgNnS
úvodního	úvodní	k2eAgInSc2d1
finále	finále	k1gNnSc7
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
prohrálo	prohrát	k5eAaPmAgNnS
s	s	k7c7
týmem	tým	k1gInSc7
Perth	Perth	k1gMnSc1
Glory	Glora	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NSL	NSL	kA
skončila	skončit	k5eAaPmAgFnS
náhle	náhle	k6eAd1
během	během	k7c2
sezóny	sezóna	k1gFnSc2
2003	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
řízena	řízen	k2eAgFnSc1d1
Australskou	australský	k2eAgFnSc7d1
fotbalovou	fotbalový	k2eAgFnSc7d1
asociací	asociace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zrušila	zrušit	k5eAaPmAgFnS
ligu	liga	k1gFnSc4
v	v	k7c6
přípravě	příprava	k1gFnSc6
na	na	k7c4
start	start	k1gInSc4
profesionální	profesionální	k2eAgFnSc2d1
Hyundai	Hyunda	k1gFnSc2
A-League	A-Leagu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
začala	začít	k5eAaPmAgFnS
o	o	k7c4
necelých	celý	k2eNgInPc2d1
12	#num#	k4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
A-League	A-League	k6eAd1
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gMnSc1
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
osmi	osm	k4xCc2
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
soutěžily	soutěžit	k5eAaImAgInP
v	v	k7c6
historicky	historicky	k6eAd1
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc3
A-League	A-Leagu	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
Perth	Perth	k1gInSc1
Glory	Glora	k1gFnPc1
a	a	k8xC
Newcastle	Newcastle	k1gFnPc1
Jets	Jetsa	k1gFnPc2
jedinými	jediný	k2eAgInPc7d1
třemi	tři	k4xCgInPc7
kluby	klub	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
přežily	přežít	k5eAaPmAgFnP
z	z	k7c2
poslední	poslední	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
NSL	NSL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
připravovat	připravovat	k5eAaImF
dříve	dříve	k6eAd2
než	než	k8xS
většina	většina	k1gFnSc1
ostatních	ostatní	k2eAgInPc2d1
klubů	klub	k1gInPc2
a	a	k8xC
v	v	k7c6
únoru	únor	k1gInSc6
2005	#num#	k4
oznámilo	oznámit	k5eAaPmAgNnS
jména	jméno	k1gNnPc1
hráčů	hráč	k1gMnPc2
ze	z	k7c2
dvou	dva	k4xCgFnPc2
třetin	třetina	k1gFnPc2
celkem	celkem	k6eAd1
20	#num#	k4
<g/>
členného	členný	k2eAgInSc2d1
kádru	kádr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
se	se	k3xPyFc4
zaměřil	zaměřit	k5eAaPmAgInS
na	na	k7c6
přivedení	přivedení	k1gNnSc6
několika	několik	k4yIc2
hráčů	hráč	k1gMnPc2
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
narodili	narodit	k5eAaPmAgMnP
v	v	k7c6
Adelaide	Adelaid	k1gMnSc5
<g/>
,	,	kIx,
například	například	k6eAd1
Angelo	Angela	k1gFnSc5
Costanzo	Costanza	k1gFnSc5
<g/>
,	,	kIx,
Travis	Travis	k1gFnPc3
Dodd	Dodda	k1gFnPc2
a	a	k8xC
Lucas	Lucasa	k1gFnPc2
Pantelis	Pantelis	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
dříve	dříve	k6eAd2
hráli	hrát	k5eAaImAgMnP
za	za	k7c4
Adelaide	Adelaid	k1gInSc5
City	city	k1gNnSc1
v	v	k7c6
NSL	NSL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shengqing	Shengqing	k1gInSc1
Qu	Qu	k1gFnSc2
byl	být	k5eAaImAgInS
přiveden	přivést	k5eAaPmNgInS
v	v	k7c6
březnu	březen	k1gInSc6
2005	#num#	k4
z	z	k7c2
čínského	čínský	k2eAgInSc2d1
klubu	klub	k1gInSc2
Shanghai	Shangha	k1gFnSc2
Shenhua	Shenhua	k1gFnSc1
jako	jako	k8xC,k8xS
drahá	drahý	k2eAgFnSc1d1
posila	posila	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c6
základě	základ	k1gInSc6
rozhodnutí	rozhodnutí	k1gNnSc2
<g/>
,	,	kIx,
dovolující	dovolující	k2eAgFnSc1d1
každému	každý	k3xTgMnSc3
klubu	klub	k1gInSc2
jednoho	jeden	k4xCgMnSc4
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
zaplacen	zaplatit	k5eAaPmNgInS
nad	nad	k7c4
rámec	rámec	k1gInSc4
platového	platový	k2eAgInSc2d1
stropu	strop	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Aurelio	Aurelio	k1gMnSc1
Vidmar	Vidmar	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
jít	jít	k5eAaImF
do	do	k7c2
fotbalové	fotbalový	k2eAgFnSc2d1
penze	penze	k1gFnSc2
dříve	dříve	k6eAd2
než	než	k8xS
A-League	A-League	k1gFnSc1
vůbec	vůbec	k9
začala	začít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
nahrazen	nahradit	k5eAaPmNgMnS
před	před	k7c7
pátým	pátý	k4xOgNnSc7
kolem	kolo	k1gNnSc7
útočníkem	útočník	k1gMnSc7
Fernandem	Fernand	k1gInSc7
z	z	k7c2
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
bývalým	bývalý	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
roku	rok	k1gInSc2
ve	v	k7c6
staré	starý	k2eAgFnSc6d1
NSL	NSL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příchodem	příchod	k1gInSc7
do	do	k7c2
Adelaide	Adelaid	k1gInSc5
se	se	k3xPyFc4
sešel	sejít	k5eAaPmAgMnS
s	s	k7c7
bývalým	bývalý	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Johnem	John	k1gMnSc7
Kosminou	Kosmin	k2eAgFnSc7d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ho	on	k3xPp3gMnSc4
představil	představit	k5eAaPmAgMnS
australským	australský	k2eAgMnSc7d1
divákům	divák	k1gMnPc3
u	u	k7c2
Brisbane	Brisban	k1gMnSc5
Strikers	Strikers	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gMnSc1
má	mít	k5eAaImIp3nS
vztahy	vztah	k1gInPc4
s	s	k7c7
americkým	americký	k2eAgInSc7d1
klubem	klub	k1gInSc7
Miami	Miami	k1gNnSc2
FC	FC	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
sesterský	sesterský	k2eAgInSc4d1
klub	klub	k1gInSc4
a	a	k8xC
odehráli	odehrát	k5eAaPmAgMnP
spolu	spolu	k6eAd1
řadu	řada	k1gFnSc4
přátelských	přátelský	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
<g/>
,	,	kIx,
angažovali	angažovat	k5eAaBmAgMnP
jejich	jejich	k3xOp3gMnSc4
hráče	hráč	k1gMnSc4
Diega	Dieg	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
rovněž	rovněž	k9
angažoval	angažovat	k5eAaBmAgInS
brazilskou	brazilský	k2eAgFnSc4d1
legendu	legenda	k1gFnSc4
Romária	Romárium	k1gNnSc2
na	na	k7c4
5	#num#	k4
zápasů	zápas	k1gInPc2
jako	jako	k8xS,k8xC
hostování	hostování	k1gNnSc2
během	během	k7c2
listopadu	listopad	k1gInSc2
<g/>
/	/	kIx~
<g/>
prosince	prosinec	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2006	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
United	United	k1gInSc1
postoupilo	postoupit	k5eAaPmAgNnS
do	do	k7c2
finále	finále	k1gNnSc2
po	po	k7c6
penaltovém	penaltový	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
4-3	4-3	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
nad	nad	k7c7
Newcastle	Newcastle	k1gFnSc7
Jets	Jetsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc4
následně	následně	k6eAd1
hrálo	hrát	k5eAaImAgNnS
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
ve	v	k7c6
finále	finále	k1gNnSc6
proti	proti	k7c3
Melbourne	Melbourne	k1gNnSc3
Victory	Victor	k1gMnPc7
na	na	k7c6
stadionu	stadion	k1gInSc6
Telstra	Telstrum	k1gNnSc2
Dome	dům	k1gInSc5
kde	kde	k6eAd1
prohrálo	prohrát	k5eAaPmAgNnS
0	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
mužstvo	mužstvo	k1gNnSc1
odehrálo	odehrát	k5eAaPmAgNnS
většinu	většina	k1gFnSc4
zápasu	zápas	k1gInSc2
pouze	pouze	k6eAd1
v	v	k7c6
10	#num#	k4
hráčích	hráč	k1gMnPc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
byl	být	k5eAaImAgMnS
kapitán	kapitán	k1gMnSc1
Ross	Ross	k1gInSc4
Aloisi	Alois	k1gMnSc3
vyloučen	vyloučen	k2eAgInSc1d1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
žluté	žlutý	k2eAgFnSc6d1
kartě	karta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
finále	finále	k1gNnSc6
následoval	následovat	k5eAaImAgInS
kontroverzní	kontroverzní	k2eAgInSc1d1
rozhovor	rozhovor	k1gInSc1
<g/>
,	,	kIx,
Ross	Ross	k1gInSc1
Aloisi	Alois	k1gMnSc3
byl	být	k5eAaImAgInS
zbaven	zbaven	k2eAgMnSc1d1
kapitánské	kapitánský	k2eAgInPc4d1
pásky	pásek	k1gInPc4
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc4
<g/>
,	,	kIx,
opustil	opustit	k5eAaPmAgInS
klub	klub	k1gInSc1
a	a	k8xC
poté	poté	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
mužstva	mužstvo	k1gNnSc2
Wellingtonu	Wellington	k1gInSc2
Phoenix	Phoenix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
kapitána	kapitán	k1gMnSc2
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
Angelem	angel	k1gMnSc7
Costanzou	Costanza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
bylo	být	k5eAaImAgNnS
také	také	k9
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
trenér	trenér	k1gMnSc1
John	John	k1gMnSc1
Kosmina	Kosmin	k2eAgFnSc1d1
bude	být	k5eAaImBp3nS
propuštěn	propuštěn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodně	hodně	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
tom	ten	k3xDgNnSc6
spekulovalo	spekulovat	k5eAaImAgNnS
<g/>
,	,	kIx,
jestli	jestli	k8xS
Kosmina	Kosmin	k2eAgFnSc1d1
rezignuje	rezignovat	k5eAaBmIp3nS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
po	po	k7c6
finále	finále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asistent	asistent	k1gMnSc1
hlavního	hlavní	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
Aurelio	Aurelio	k1gMnSc1
Vidmar	Vidmar	k1gMnSc1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
dočasným	dočasný	k2eAgMnSc7d1
hlavním	hlavní	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
bývalý	bývalý	k2eAgInSc1d1
trenér	trenér	k1gMnSc1
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc1
John	John	k1gMnSc1
Kosmina	Kosmin	k2eAgFnSc1d1
převzal	převzít	k5eAaPmAgInS
roli	role	k1gFnSc4
hlavního	hlavní	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
v	v	k7c6
týmu	tým	k1gInSc6
Sydney	Sydney	k1gNnSc2
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2006	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
7	#num#	k4
také	také	k9
nabídla	nabídnout	k5eAaPmAgFnS
brazilského	brazilský	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
Romária	Romárium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
ke	k	k7c3
klubu	klub	k1gInSc2
připojil	připojit	k5eAaPmAgInS
na	na	k7c6
pěti	pět	k4xCc6
zápasové	zápasový	k2eAgFnPc4d1
hostování	hostování	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2007	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Pro	pro	k7c4
tuto	tento	k3xDgFnSc4
sezónu	sezóna	k1gFnSc4
Adelaide	Adelaid	k1gInSc5
posílilo	posílit	k5eAaPmAgNnS
o	o	k7c4
bývalého	bývalý	k2eAgMnSc4d1
australského	australský	k2eAgMnSc4d1
reprezentanta	reprezentant	k1gMnSc4
Paula	Paul	k1gMnSc4
Agostina	Agostin	k1gMnSc4
<g/>
,	,	kIx,
fotbalistu	fotbalista	k1gMnSc4
z	z	k7c2
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
Jonase	Jonasa	k1gFnSc3
Salleyho	Salley	k1gMnSc2
a	a	k8xC
bývalého	bývalý	k2eAgMnSc2d1
juniorského	juniorský	k2eAgMnSc2d1
reprezentanta	reprezentant	k1gMnSc2
do	do	k7c2
23	#num#	k4
let	léto	k1gNnPc2
Kristiana	Kristian	k1gMnSc2
Sarkiese	Sarkiese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
podepsali	podepsat	k5eAaPmAgMnP
İ	İ	k1gMnSc4
Erdogana	Erdogan	k1gMnSc4
z	z	k7c2
týmu	tým	k1gInSc2
Proston	Proston	k1gInSc1
Lions	Lions	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
úspěšných	úspěšný	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
sezóny	sezóna	k1gFnSc2
bylo	být	k5eAaImAgNnS
angažování	angažování	k1gNnSc1
bývalého	bývalý	k2eAgMnSc2d1
hráče	hráč	k1gMnSc2
z	z	k7c2
týmu	tým	k1gInSc2
Flamengo	flamengo	k1gNnSc4
FC	FC	kA
<g/>
,	,	kIx,
Cássia	Cássium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
přišel	přijít	k5eAaPmAgInS
zdarma	zdarma	k6eAd1
z	z	k7c2
brazilského	brazilský	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cássio	Cássio	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
přestoupil	přestoupit	k5eAaPmAgMnS
z	z	k7c2
klubu	klub	k1gInSc2
Santa	Santo	k1gNnSc2
Cruz	Cruza	k1gFnPc2
FC	FC	kA
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
získal	získat	k5eAaPmAgMnS
klubové	klubový	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shaun	Shaun	k1gMnSc1
Ontong	Ontong	k1gMnSc1
a	a	k8xC
Matthew	Matthew	k1gMnSc1
Mullen	Mullna	k1gFnPc2
z	z	k7c2
AIS	AIS	kA
byli	být	k5eAaImAgMnP
také	také	k6eAd1
podepsáni	podepsán	k2eAgMnPc1d1
aby	aby	kYmCp3nP
doplnili	doplnit	k5eAaPmAgMnP
defenzívu	defenzíva	k1gFnSc4
po	po	k7c6
odchodu	odchod	k1gInSc6
Reese	Reese	k1gFnSc2
<g/>
,	,	kIx,
Dommela	Dommela	k1gFnSc1
a	a	k8xC
Gouldina	Gouldina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2007	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
Adelaide	Adelaid	k1gInSc5
špatnou	špatná	k1gFnSc4
<g/>
,	,	kIx,
spousta	spousta	k1gFnSc1
hráčů	hráč	k1gMnPc2
se	se	k3xPyFc4
během	během	k7c2
sezóny	sezóna	k1gFnSc2
zranila	zranit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zranění	zranění	k1gNnSc4
ukončila	ukončit	k5eAaPmAgFnS
naděje	naděje	k1gFnSc1
na	na	k7c6
finále	finále	k1gNnSc6
a	a	k8xC
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
Adelaide	Adelaid	k1gInSc5
nedostalo	dostat	k5eNaPmAgNnS
mezi	mezi	k7c4
dva	dva	k4xCgInPc4
nejlepší	dobrý	k2eAgInPc4d3
týmy	tým	k1gInPc4
v	v	k7c6
A-League	A-League	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
a	a	k8xC
poznámky	poznámka	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc4
FC	FC	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Adelaide	Adelaid	k1gInSc5
United	United	k1gInSc4
FC	FC	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
