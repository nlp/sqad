<p>
<s>
Gravitace	gravitace	k1gFnSc1	gravitace
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgFnSc1d1	gravitační
interakce	interakce	k1gFnSc1	interakce
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgNnSc1d1	univerzální
silové	silový	k2eAgNnSc1d1	silové
působení	působení	k1gNnSc1	působení
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
formami	forma	k1gFnPc7	forma
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
právě	právě	k9	právě
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
interakcí	interakce	k1gFnPc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
interakce	interakce	k1gFnSc1	interakce
je	být	k5eAaImIp3nS	být
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nekonečný	konečný	k2eNgInSc4d1	nekonečný
dosah	dosah	k1gInSc4	dosah
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamněji	významně	k6eAd3	významně
tedy	tedy	k9	tedy
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
hmotnosti	hmotnost	k1gFnSc6	hmotnost
(	(	kIx(	(
<g/>
makrosvět	makrosvět	k1gInSc1	makrosvět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
rychlosti	rychlost	k1gFnPc4	rychlost
a	a	k8xC	a
slabá	slabý	k2eAgNnPc1d1	slabé
pole	pole	k1gNnPc1	pole
se	se	k3xPyFc4	se
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
gravitační	gravitační	k2eAgFnSc2d1	gravitační
interakce	interakce	k1gFnSc2	interakce
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
a	a	k8xC	a
relativní	relativní	k2eAgFnSc3d1	relativní
přesnosti	přesnost	k1gFnSc3	přesnost
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
rychlosti	rychlost	k1gFnPc4	rychlost
<g/>
,	,	kIx,	,
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
gravitační	gravitační	k2eAgInSc4d1	gravitační
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
silná	silný	k2eAgNnPc4d1	silné
pole	pole	k1gNnPc4	pole
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
rychlosti	rychlost	k1gFnPc4	rychlost
(	(	kIx(	(
<g/>
relativistické	relativistický	k2eAgFnPc4d1	relativistická
<g/>
,	,	kIx,	,
blížící	blížící	k2eAgFnPc4d1	blížící
se	se	k3xPyFc4	se
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
gravitace	gravitace	k1gFnSc2	gravitace
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
slabé	slabý	k2eAgNnSc4d1	slabé
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
malé	malý	k2eAgFnSc2d1	malá
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hledaná	hledaný	k2eAgFnSc1d1	hledaná
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
částici	částice	k1gFnSc4	částice
zvanou	zvaný	k2eAgFnSc4d1	zvaná
graviton	graviton	k1gInSc4	graviton
jako	jako	k8xC	jako
další	další	k2eAgFnSc4d1	další
elementární	elementární	k2eAgFnSc4d1	elementární
částici	částice	k1gFnSc4	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
hmotná	hmotný	k2eAgNnPc4d1	hmotné
tělesa	těleso	k1gNnPc4	těleso
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
ubývá	ubývat	k5eAaImIp3nS	ubývat
se	s	k7c7	s
čtvercem	čtverec	k1gInSc7	čtverec
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ji	on	k3xPp3gFnSc4	on
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
mechanice	mechanika	k1gFnSc6	mechanika
prostor	prostor	k1gInSc4	prostor
kolem	kolem	k7c2	kolem
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
působení	působení	k1gNnSc1	působení
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
dosah	dosah	k1gInSc1	dosah
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
i	i	k8xC	i
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
hranici	hranice	k1gFnSc4	hranice
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přestává	přestávat	k5eAaImIp3nS	přestávat
být	být	k5eAaImF	být
měřitelné	měřitelný	k2eAgNnSc1d1	měřitelné
<g/>
,	,	kIx,	,
či	či	k8xC	či
začíná	začínat	k5eAaImIp3nS	začínat
převládat	převládat	k5eAaImF	převládat
gravitace	gravitace	k1gFnSc1	gravitace
jiného	jiný	k2eAgNnSc2d1	jiné
tělesa	těleso	k1gNnSc2	těleso
nebo	nebo	k8xC	nebo
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
==	==	k?	==
</s>
</p>
<p>
<s>
Gravitaci	gravitace	k1gFnSc4	gravitace
nejlépe	dobře	k6eAd3	dobře
popisuje	popisovat	k5eAaImIp3nS	popisovat
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
coby	coby	k?	coby
zakřivení	zakřivení	k1gNnSc1	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dostatečně	dostatečně	k6eAd1	dostatečně
slabá	slabý	k2eAgNnPc4d1	slabé
gravitační	gravitační	k2eAgNnPc4d1	gravitační
pole	pole	k1gNnPc4	pole
však	však	k9	však
vystačíme	vystačit	k5eAaBmIp1nP	vystačit
s	s	k7c7	s
aproximací	aproximace	k1gFnSc7	aproximace
Newtonovým	Newtonův	k2eAgInSc7d1	Newtonův
gravitačním	gravitační	k2eAgInSc7d1	gravitační
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
kvantování	kvantování	k1gNnSc4	kvantování
<g/>
,	,	kIx,	,
čeká	čekat	k5eAaImIp3nS	čekat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
nástupce	nástupce	k1gMnSc1	nástupce
-	-	kIx~	-
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sjednotí	sjednotit	k5eAaPmIp3nS	sjednotit
gravitaci	gravitace	k1gFnSc4	gravitace
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
silami	síla	k1gFnPc7	síla
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
mít	mít	k5eAaImF	mít
kvantový	kvantový	k2eAgInSc4d1	kvantový
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
gravitační	gravitační	k2eAgInSc1d1	gravitační
zákon	zákon	k1gInSc1	zákon
===	===	k?	===
</s>
</p>
<p>
<s>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
gravitační	gravitační	k2eAgInSc1d1	gravitační
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
popisující	popisující	k2eAgFnSc1d1	popisující
gravitační	gravitační	k2eAgNnSc4d1	gravitační
působení	působení	k1gNnSc4	působení
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
dvě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
hmotné	hmotný	k2eAgInPc4d1	hmotný
body	bod	k1gInPc4	bod
nebo	nebo	k8xC	nebo
homogenní	homogenní	k2eAgFnPc4d1	homogenní
koule	koule	k1gFnPc4	koule
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
Gaussovy	Gaussův	k2eAgFnSc2d1	Gaussova
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
působí	působit	k5eAaImIp3nS	působit
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
G	G	kA	G
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
<s>
Fg	Fg	k?	Fg
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hmotnými	hmotný	k2eAgFnPc7d1	hmotná
tělesy	těleso	k1gNnPc7	těleso
</s>
</p>
<p>
<s>
m	m	kA	m
<g/>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
prvního	první	k4xOgMnSc2	první
tělesa	těleso	k1gNnPc1	těleso
</s>
</p>
<p>
<s>
m	m	kA	m
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
druhého	druhý	k4xOgMnSc2	druhý
tělesa	těleso	k1gNnPc1	těleso
</s>
</p>
<p>
<s>
r	r	kA	r
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
</s>
</p>
<p>
<s>
G	G	kA	G
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
přibližně	přibližně	k6eAd1	přibližně
<g/>
:	:	kIx,	:
G	G	kA	G
=	=	kIx~	=
6,67	[number]	k4	6,67
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
11	[number]	k4	11
N	N	kA	N
m	m	kA	m
<g/>
2	[number]	k4	2
kg-	kg-	k?	kg-
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
publikacích	publikace	k1gFnPc6	publikace
značená	značený	k2eAgNnPc1d1	značené
místo	místo	k7c2	místo
G	G	kA	G
řeckým	řecký	k2eAgNnSc7d1	řecké
písmenem	písmeno	k1gNnSc7	písmeno
κ	κ	k?	κ
<g/>
)	)	kIx)	)
<g/>
Newtonova	Newtonův	k2eAgFnSc1d1	Newtonova
teorie	teorie	k1gFnSc1	teorie
vycházela	vycházet	k5eAaImAgFnS	vycházet
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
Newtonových	Newtonových	k2eAgNnSc2d1	Newtonových
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
Keplerových	Keplerův	k2eAgInPc2d1	Keplerův
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
pole	pole	k1gNnSc2	pole
====	====	k?	====
</s>
</p>
<p>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
lze	lze	k6eAd1	lze
v	v	k7c6	v
Newtonově	Newtonův	k2eAgFnSc6d1	Newtonova
gravitační	gravitační	k2eAgFnSc6d1	gravitační
teorii	teorie	k1gFnSc6	teorie
určovat	určovat	k5eAaImF	určovat
nejen	nejen	k6eAd1	nejen
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
–	–	k?	–
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
jednotkové	jednotkový	k2eAgFnSc2d1	jednotková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgNnSc1d1	gravitační
zrychlení	zrychlení	k1gNnSc1	zrychlení
–	–	k?	–
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tělesům	těleso	k1gNnPc3	těleso
uděluje	udělovat	k5eAaImIp3nS	udělovat
gravitační	gravitační	k2eAgNnSc1d1	gravitační
sílaPokud	sílaPokud	k6eAd1	sílaPokud
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
jiného	jiný	k2eAgNnSc2d1	jiné
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
mu	on	k3xPp3gMnSc3	on
klasická	klasický	k2eAgFnSc1d1	klasická
mechanika	mechanika	k1gFnSc1	mechanika
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
určitou	určitý	k2eAgFnSc4d1	určitá
potenciální	potenciální	k2eAgFnSc4d1	potenciální
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
gravitační	gravitační	k2eAgFnSc1d1	gravitační
potenciální	potenciální	k2eAgFnSc1d1	potenciální
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
OTR	OTR	kA	OTR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
gravitace	gravitace	k1gFnSc1	gravitace
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
zakřivením	zakřivení	k1gNnSc7	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zakřivení	zakřivení	k1gNnSc1	zakřivení
vzniká	vznikat	k5eAaImIp3nS	vznikat
přítomností	přítomnost	k1gFnSc7	přítomnost
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
součet	součet	k1gInSc1	součet
úhlů	úhel	k1gInPc2	úhel
v	v	k7c6	v
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
180	[number]	k4	180
<g/>
°	°	k?	°
nebo	nebo	k8xC	nebo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lokálně	lokálně	k6eAd1	lokálně
nejrovnější	rovný	k2eAgFnPc1d3	nejrovnější
čáry	čára	k1gFnPc1	čára
–	–	k?	–
geodetiky	geodetik	k1gMnPc7	geodetik
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
přímek	přímka	k1gFnPc2	přímka
vždy	vždy	k6eAd1	vždy
"	"	kIx"	"
<g/>
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
geodetikách	geodetikách	k?	geodetikách
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc4	jejich
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
časové	časový	k2eAgFnSc3d1	časová
ose	osa	k1gFnSc3	osa
udává	udávat	k5eAaImIp3nS	udávat
rychlost	rychlost	k1gFnSc1	rychlost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Geodetiky	geodetik	k1gMnPc4	geodetik
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
v	v	k7c6	v
populární	populární	k2eAgFnSc3d1	populární
literatuře	literatura	k1gFnSc3	literatura
označovány	označovat	k5eAaImNgInP	označovat
za	za	k7c4	za
nejkratší	krátký	k2eAgFnPc4d3	nejkratší
spojnice	spojnice	k1gFnPc4	spojnice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
není	být	k5eNaImIp3nS	být
pravda	pravda	k9	pravda
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
slabá	slabý	k2eAgNnPc4d1	slabé
gravitační	gravitační	k2eAgNnPc4d1	gravitační
pole	pole	k1gNnPc4	pole
dává	dávat	k5eAaImIp3nS	dávat
OTR	OTR	kA	OTR
stejné	stejný	k2eAgFnPc4d1	stejná
předpovědi	předpověď	k1gFnPc4	předpověď
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Newtonova	Newtonův	k2eAgFnSc1d1	Newtonova
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Představa	představa	k1gFnSc1	představa
zakřiveného	zakřivený	k2eAgInSc2d1	zakřivený
prostoru	prostor	k1gInSc2	prostor
často	často	k6eAd1	často
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
křivý	křivý	k2eAgInSc1d1	křivý
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
vložen	vložit	k5eAaPmNgInS	vložit
do	do	k7c2	do
vícerozměrného	vícerozměrný	k2eAgInSc2d1	vícerozměrný
rovného	rovný	k2eAgInSc2d1	rovný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Matematický	matematický	k2eAgInSc1d1	matematický
popis	popis	k1gInSc1	popis
OTR	OTR	kA	OTR
však	však	k9	však
takové	takový	k3xDgNnSc4	takový
vložení	vložení	k1gNnSc4	vložení
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
časoprostoru	časoprostor	k1gInSc2	časoprostor
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
jeho	jeho	k3xOp3gInSc6	jeho
bodě	bod	k1gInSc6	bod
definován	definovat	k5eAaBmNgInS	definovat
metrický	metrický	k2eAgInSc1d1	metrický
tenzor	tenzor	k1gInSc1	tenzor
a	a	k8xC	a
takto	takto	k6eAd1	takto
vymezeno	vymezen	k2eAgNnSc1d1	vymezeno
tzv.	tzv.	kA	tzv.
metrické	metrický	k2eAgNnSc1d1	metrické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Metrický	metrický	k2eAgInSc1d1	metrický
tenzor	tenzor	k1gInSc1	tenzor
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
deseti	deset	k4xCc2	deset
bezrozměrných	bezrozměrný	k2eAgFnPc2d1	bezrozměrná
geometrických	geometrický	k2eAgFnPc2d1	geometrická
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
určují	určovat	k5eAaImIp3nP	určovat
metriku	metrika	k1gFnSc4	metrika
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
se	se	k3xPyFc4	se
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
části	část	k1gFnSc6	část
prostoru	prostor	k1gInSc2	prostor
počítají	počítat	k5eAaImIp3nP	počítat
zobecněné	zobecněný	k2eAgFnPc4d1	zobecněná
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
–	–	k?	–
intervaly	interval	k1gInPc1	interval
–	–	k?	–
mezi	mezi	k7c7	mezi
body	bod	k1gInPc7	bod
časoprostoru	časoprostor	k1gInSc2	časoprostor
–	–	k?	–
událostmi	událost	k1gFnPc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
metrických	metrický	k2eAgInPc2d1	metrický
prostorů	prostor	k1gInPc2	prostor
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
zakřivení	zakřivení	k1gNnSc4	zakřivení
daného	daný	k2eAgInSc2d1	daný
prostoru	prostor	k1gInSc2	prostor
pomocí	pomocí	k7c2	pomocí
změn	změna	k1gFnPc2	změna
metrického	metrický	k2eAgInSc2d1	metrický
tenzoru	tenzor	k1gInSc2	tenzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einsteinovi	Einstein	k1gMnSc3	Einstein
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předpokladu	předpoklad	k1gInSc2	předpoklad
o	o	k7c6	o
rovnosti	rovnost	k1gFnSc6	rovnost
setrvačné	setrvačný	k2eAgFnSc6d1	setrvačná
a	a	k8xC	a
gravitační	gravitační	k2eAgFnSc6d1	gravitační
hmotnosti	hmotnost	k1gFnSc6	hmotnost
podařilo	podařit	k5eAaPmAgNnS	podařit
spojit	spojit	k5eAaPmF	spojit
zakřivení	zakřivení	k1gNnSc1	zakřivení
prostoročasu	prostoročas	k1gInSc2	prostoročas
s	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
hmoty	hmota	k1gFnSc2	hmota
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
(	(	kIx(	(
<g/>
ER	ER	kA	ER
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řešením	řešení	k1gNnSc7	řešení
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
se	se	k3xPyFc4	se
získají	získat	k5eAaPmIp3nP	získat
metrické	metrický	k2eAgInPc1d1	metrický
tenzory	tenzor	k1gInPc1	tenzor
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
určeno	určen	k2eAgNnSc1d1	určeno
zakřivení	zakřivení	k1gNnSc1	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
OTR	OTR	kA	OTR
plynou	plynout	k5eAaImIp3nP	plynout
některé	některý	k3yIgInPc4	některý
kvalitativně	kvalitativně	k6eAd1	kvalitativně
nové	nový	k2eAgInPc4d1	nový
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
šíření	šíření	k1gNnSc2	šíření
změn	změna	k1gFnPc2	změna
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
–	–	k?	–
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc2d1	gravitační
vlny	vlna	k1gFnSc2	vlna
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
například	například	k6eAd1	například
dvojice	dvojice	k1gFnSc1	dvojice
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
obíhají	obíhat	k5eAaImIp3nP	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
minimálně	minimálně	k6eAd1	minimálně
dva	dva	k4xCgInPc1	dva
přístroje	přístroj	k1gInPc1	přístroj
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
odhalení	odhalení	k1gNnSc4	odhalení
<g/>
,	,	kIx,	,
americké	americký	k2eAgNnSc4d1	americké
LIGO	liga	k1gFnSc5	liga
a	a	k8xC	a
italské	italský	k2eAgFnSc2d1	italská
VIRGO	VIRGO	kA	VIRGO
<g/>
.	.	kIx.	.
</s>
<s>
Oběma	dva	k4xCgMnPc3	dva
už	už	k6eAd1	už
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
gravitační	gravitační	k2eAgFnPc4d1	gravitační
vlny	vlna	k1gFnPc4	vlna
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc1	projev
vyzařování	vyzařování	k1gNnSc2	vyzařování
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
měřeny	měřen	k2eAgInPc1d1	měřen
v	v	k7c6	v
binárních	binární	k2eAgInPc6d1	binární
hvězdných	hvězdný	k2eAgInPc6d1	hvězdný
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
vesmírnou	vesmírný	k2eAgFnSc7d1	vesmírná
laboratoří	laboratoř	k1gFnSc7	laboratoř
je	být	k5eAaImIp3nS	být
podvojný	podvojný	k2eAgInSc1d1	podvojný
pulzar	pulzar	k1gInSc1	pulzar
PSR	PSR	kA	PSR
1913	[number]	k4	1913
<g/>
+	+	kIx~	+
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
naměřilo	naměřit	k5eAaBmAgNnS	naměřit
zkracování	zkracování	k1gNnSc1	zkracování
oběžné	oběžný	k2eAgFnSc2d1	oběžná
doby	doba	k1gFnSc2	doba
o	o	k7c4	o
7,6	[number]	k4	7,6
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
s	s	k7c7	s
za	za	k7c7	za
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
předpovědi	předpověď	k1gFnPc4	předpověď
OTR	OTR	kA	OTR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgMnSc7d1	další
zcela	zcela	k6eAd1	zcela
novým	nový	k2eAgInSc7d1	nový
jevem	jev	k1gInSc7	jev
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
horizontů	horizont	k1gInPc2	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
překročit	překročit	k5eAaPmF	překročit
jen	jen	k9	jen
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
a	a	k8xC	a
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc1	žádný
dosud	dosud	k6eAd1	dosud
známý	známý	k2eAgInSc1d1	známý
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
získávat	získávat	k5eAaImF	získávat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
existenci	existence	k1gFnSc4	existence
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
a	a	k8xC	a
kosmologických	kosmologický	k2eAgInPc2d1	kosmologický
horizontů	horizont	k1gInPc2	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
také	také	k9	také
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
zakřivování	zakřivování	k1gNnSc1	zakřivování
drah	draha	k1gFnPc2	draha
paprsků	paprsek	k1gInPc2	paprsek
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
dobře	dobře	k6eAd1	dobře
známým	známý	k2eAgInSc7d1	známý
jevem	jev	k1gInSc7	jev
projevujícím	projevující	k2eAgInSc7d1	projevující
se	se	k3xPyFc4	se
jak	jak	k8xC	jak
na	na	k7c6	na
měřítkách	měřítko	k1gNnPc6	měřítko
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
měřítkách	měřítko	k1gNnPc6	měřítko
kup	kup	k1gInSc1	kup
a	a	k8xC	a
nadkup	nadkup	k1gInSc1	nadkup
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Využívají	využívat	k5eAaPmIp3nP	využívat
se	se	k3xPyFc4	se
při	při	k7c6	při
mnoha	mnoho	k4c6	mnoho
pozorováních	pozorování	k1gNnPc6	pozorování
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
např.	např.	kA	např.
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
přítomnosti	přítomnost	k1gFnSc2	přítomnost
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
slabá	slabý	k2eAgNnPc4d1	slabé
gravitační	gravitační	k2eAgNnPc4d1	gravitační
pole	pole	k1gNnPc4	pole
přechází	přecházet	k5eAaImIp3nS	přecházet
OTR	OTR	kA	OTR
k	k	k7c3	k
předpovědím	předpověď	k1gFnPc3	předpověď
shodným	shodný	k2eAgFnPc3d1	shodná
s	s	k7c7	s
Newtonovou	Newtonová	k1gFnSc7	Newtonová
teorií	teorie	k1gFnPc2	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
oprav	oprava	k1gFnPc2	oprava
k	k	k7c3	k
Newtonově	Newtonův	k2eAgFnSc3d1	Newtonova
teorii	teorie	k1gFnSc3	teorie
je	být	k5eAaImIp3nS	být
stáčení	stáčení	k1gNnSc1	stáčení
perihelia	perihelium	k1gNnSc2	perihelium
drah	draha	k1gFnPc2	draha
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nevysvětlených	vysvětlený	k2eNgInPc2d1	nevysvětlený
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
objasnění	objasnění	k1gNnSc4	objasnění
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
úspěchů	úspěch	k1gInPc2	úspěch
OTR	OTR	kA	OTR
<g/>
.	.	kIx.	.
</s>
<s>
Stáčení	stáčení	k1gNnSc1	stáčení
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
patrné	patrný	k2eAgNnSc1d1	patrné
u	u	k7c2	u
planet	planeta	k1gFnPc2	planeta
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
centrálnímu	centrální	k2eAgNnSc3d1	centrální
tělesu	těleso	k1gNnSc3	těleso
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
nejlépe	dobře	k6eAd3	dobře
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
stáčení	stáčení	k1gNnSc1	stáčení
dráhy	dráha	k1gFnSc2	dráha
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polní	polní	k2eAgFnSc1d1	polní
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
===	===	k?	===
</s>
</p>
<p>
<s>
Geometrickou	geometrický	k2eAgFnSc4d1	geometrická
interpretaci	interpretace	k1gFnSc4	interpretace
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
lze	lze	k6eAd1	lze
převést	převést	k5eAaPmF	převést
do	do	k7c2	do
polní	polní	k2eAgFnSc2d1	polní
formy	forma	k1gFnSc2	forma
v	v	k7c6	v
plochém	plochý	k2eAgInSc6d1	plochý
časoprostoru	časoprostor	k1gInSc6	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
zastánce	zastánce	k1gMnPc4	zastánce
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měřitelných	měřitelný	k2eAgFnPc6d1	měřitelná
předpovědích	předpověď	k1gFnPc6	předpověď
u	u	k7c2	u
slabých	slabý	k2eAgFnPc2d1	slabá
polí	pole	k1gFnPc2	pole
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgFnSc3d1	obecná
teorii	teorie	k1gFnSc3	teorie
relativity	relativita	k1gFnSc2	relativita
ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
a	a	k8xC	a
u	u	k7c2	u
silných	silný	k2eAgNnPc2d1	silné
polí	pole	k1gNnPc2	pole
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
singularity	singularita	k1gFnPc4	singularita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
===	===	k?	===
</s>
</p>
<p>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
KTP	KTP	kA	KTP
<g/>
)	)	kIx)	)
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
zatím	zatím	k6eAd1	zatím
nikomu	nikdo	k3yNnSc3	nikdo
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
fyzikové	fyzik	k1gMnPc1	fyzik
snaží	snažit	k5eAaImIp3nP	snažit
již	již	k6eAd1	již
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Gravitace	gravitace	k1gFnSc1	gravitace
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
přírodních	přírodní	k2eAgFnPc2d1	přírodní
sil	síla	k1gFnPc2	síla
natolik	natolik	k6eAd1	natolik
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
neslučitelná	slučitelný	k2eNgFnSc1d1	neslučitelná
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
KTP	KTP	kA	KTP
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
za	za	k7c4	za
výměnnou	výměnný	k2eAgFnSc4d1	výměnná
částici	částice	k1gFnSc4	částice
považuje	považovat	k5eAaImIp3nS	považovat
zatím	zatím	k6eAd1	zatím
neobjevený	objevený	k2eNgInSc1d1	neobjevený
graviton	graviton	k1gInSc1	graviton
se	s	k7c7	s
spinem	spin	k1gInSc7	spin
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Strunová	strunový	k2eAgFnSc1d1	strunová
teorie	teorie	k1gFnSc1	teorie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
strun	struna	k1gFnPc2	struna
je	být	k5eAaImIp3nS	být
graviton	graviton	k1gInSc1	graviton
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
druhem	druh	k1gInSc7	druh
vibrace	vibrace	k1gFnSc2	vibrace
struny	struna	k1gFnSc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
spojeno	spojit	k5eAaPmNgNnS	spojit
se	s	k7c7	s
zakřivením	zakřivení	k1gNnSc7	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
pomocí	pomocí	k7c2	pomocí
ztotožnění	ztotožnění	k1gNnSc2	ztotožnění
struktury	struktura	k1gFnSc2	struktura
časoprostoru	časoprostor	k1gInSc2	časoprostor
s	s	k7c7	s
obrovským	obrovský	k2eAgNnSc7d1	obrovské
množstvím	množství	k1gNnSc7	množství
podobně	podobně	k6eAd1	podobně
(	(	kIx(	(
<g/>
koherentně	koherentně	k6eAd1	koherentně
<g/>
)	)	kIx)	)
vibrujících	vibrující	k2eAgFnPc2d1	vibrující
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Časoprostor	časoprostor	k1gInSc1	časoprostor
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dá	dát	k5eAaPmIp3nS	dát
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
tkanina	tkanina	k1gFnSc1	tkanina
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
ze	z	k7c2	z
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Gravitace	gravitace	k1gFnSc1	gravitace
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
totožná	totožný	k2eAgFnSc1d1	totožná
se	s	k7c7	s
zakřivením	zakřivení	k1gNnSc7	zakřivení
této	tento	k3xDgFnSc2	tento
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc1d1	významný
druhy	druh	k1gInPc1	druh
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Homogenní	homogenní	k2eAgNnSc4d1	homogenní
gravitační	gravitační	k2eAgNnSc4d1	gravitační
pole	pole	k1gNnSc4	pole
===	===	k?	===
</s>
</p>
<p>
<s>
Homogenní	homogenní	k2eAgNnSc1d1	homogenní
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
zjednodušeného	zjednodušený	k2eAgInSc2d1	zjednodušený
matematického	matematický	k2eAgInSc2d1	matematický
popisu	popis	k1gInSc2	popis
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
pole	pole	k1gNnSc2	pole
stejná	stejná	k1gFnSc1	stejná
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc1	velikost
i	i	k8xC	i
směr	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Homogenní	homogenní	k2eAgNnSc1d1	homogenní
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
vhodným	vhodný	k2eAgNnSc7d1	vhodné
přiblížením	přiblížení	k1gNnSc7	přiblížení
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
sledované	sledovaný	k2eAgInPc1d1	sledovaný
děje	děj	k1gInPc1	děj
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
nemění	měnit	k5eNaImIp3nS	měnit
velikost	velikost	k1gFnSc4	velikost
ani	ani	k8xC	ani
směr	směr	k1gInSc4	směr
intenzity	intenzita	k1gFnSc2	intenzita
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
např.	např.	kA	např.
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
uvnitř	uvnitř	k7c2	uvnitř
jedné	jeden	k4xCgFnSc2	jeden
místnosti	místnost	k1gFnSc2	místnost
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
jednotek	jednotka	k1gFnPc2	jednotka
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homogenní	homogenní	k2eAgNnSc1d1	homogenní
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vhodné	vhodný	k2eAgNnSc1d1	vhodné
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
pohybů	pohyb	k1gInPc2	pohyb
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
povrchu	povrch	k1gInSc2	povrch
velkých	velký	k2eAgNnPc2d1	velké
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
trajektorie	trajektorie	k1gFnSc2	trajektorie
sledovaných	sledovaný	k2eAgFnPc2d1	sledovaná
těles	těleso	k1gNnPc2	těleso
malé	malá	k1gFnSc2	malá
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
popisovat	popisovat	k5eAaImF	popisovat
pomocí	pomoc	k1gFnSc7	pomoc
potenciálu	potenciál	k1gInSc2	potenciál
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
mgh	mgh	k?	mgh
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
m	m	kA	m
značí	značit	k5eAaImIp3nS	značit
hmotnost	hmotnost	k1gFnSc4	hmotnost
tělesa	těleso	k1gNnSc2	těleso
pohybujícím	pohybující	k2eAgInPc3d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
g	g	kA	g
je	být	k5eAaImIp3nS	být
intenzita	intenzita	k1gFnSc1	intenzita
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
zrychlení	zrychlení	k1gNnSc1	zrychlení
polem	polem	k6eAd1	polem
působené	působený	k2eAgNnSc1d1	působené
<g/>
)	)	kIx)	)
a	a	k8xC	a
h	h	k?	h
je	být	k5eAaImIp3nS	být
výška	výška	k1gFnSc1	výška
měřená	měřený	k2eAgFnSc1d1	měřená
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
působení	působení	k1gNnSc2	působení
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
rotujících	rotující	k2eAgNnPc2d1	rotující
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
popisujeme	popisovat	k5eAaImIp1nP	popisovat
<g/>
-li	i	k?	-li
děje	děj	k1gInPc1	děj
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
daným	daný	k2eAgNnSc7d1	dané
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šikmý	šikmý	k2eAgInSc1d1	šikmý
vrh	vrh	k1gInSc1	vrh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodnějším	vhodný	k2eAgNnSc7d2	vhodnější
přiblížením	přiblížení	k1gNnSc7	přiblížení
tíhové	tíhový	k2eAgFnSc2d1	tíhová
pole	pole	k1gFnSc2	pole
<g/>
,	,	kIx,	,
zohledňující	zohledňující	k2eAgFnSc2d1	zohledňující
i	i	k8xC	i
setrvačné	setrvačný	k2eAgFnSc2d1	setrvačná
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Radiální	radiální	k2eAgNnSc4d1	radiální
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgNnSc4d1	centrální
<g/>
)	)	kIx)	)
gravitační	gravitační	k2eAgNnSc4d1	gravitační
pole	pole	k1gNnSc4	pole
===	===	k?	===
</s>
</p>
<p>
<s>
Radiální	radiální	k2eAgNnSc4d1	radiální
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgNnSc4d1	centrální
<g/>
)	)	kIx)	)
gravitační	gravitační	k2eAgNnSc4d1	gravitační
pole	pole	k1gNnSc4	pole
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
směr	směr	k1gInSc4	směr
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
pole	pole	k1gNnSc2	pole
míří	mířit	k5eAaImIp3nS	mířit
stále	stále	k6eAd1	stále
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
–	–	k?	–
středu	střed	k1gInSc2	střed
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgInPc4	všechen
body	bod	k1gInPc4	bod
nacházející	nacházející	k2eAgInPc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
kulové	kulový	k2eAgFnSc6d1	kulová
ploše	plocha	k1gFnSc6	plocha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
střed	střed	k1gInSc4	střed
v	v	k7c6	v
těžišti	těžiště	k1gNnSc6	těžiště
tělesa	těleso	k1gNnSc2	těleso
mají	mít	k5eAaImIp3nP	mít
intenzitu	intenzita	k1gFnSc4	intenzita
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgNnSc1d1	centrální
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
idealizovaný	idealizovaný	k2eAgInSc4d1	idealizovaný
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
teoreticky	teoreticky	k6eAd1	teoreticky
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
osamělých	osamělý	k2eAgFnPc2d1	osamělá
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
)	)	kIx)	)
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
těles	těleso	k1gNnPc2	těleso
s	s	k7c7	s
kulovou	kulový	k2eAgFnSc7d1	kulová
symetrií	symetrie	k1gFnSc7	symetrie
a	a	k8xC	a
nerotujících	rotující	k2eNgFnPc2d1	nerotující
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
nesymetrické	symetrický	k2eNgNnSc1d1	nesymetrické
rozložení	rozložení	k1gNnSc1	rozložení
hmot	hmota	k1gFnPc2	hmota
může	moct	k5eAaImIp3nS	moct
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
jemné	jemný	k2eAgFnPc4d1	jemná
směrové	směrový	k2eAgFnPc4d1	směrová
odchylky	odchylka	k1gFnPc4	odchylka
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
velmi	velmi	k6eAd1	velmi
dobrá	dobrý	k2eAgFnSc1d1	dobrá
aproximace	aproximace	k1gFnSc1	aproximace
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
např.	např.	kA	např.
kolem	kolem	k7c2	kolem
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
přibližně	přibližně	k6eAd1	přibližně
kulových	kulový	k2eAgNnPc2d1	kulové
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aproximace	aproximace	k1gFnSc1	aproximace
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
pomocí	pomocí	k7c2	pomocí
radiálního	radiální	k2eAgNnSc2d1	radiální
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
trajektorie	trajektorie	k1gFnSc1	trajektorie
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
dostatečně	dostatečně	k6eAd1	dostatečně
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působení	působení	k1gNnSc1	působení
sféricky	sféricky	k6eAd1	sféricky
symetrického	symetrický	k2eAgNnSc2d1	symetrické
tělesa	těleso	k1gNnSc2	těleso
lze	lze	k6eAd1	lze
ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
nahradit	nahradit	k5eAaPmF	nahradit
ve	v	k7c6	v
výpočtech	výpočet	k1gInPc6	výpočet
podle	podle	k7c2	podle
Newtonových	Newtonová	k1gFnPc2	Newtonová
zákonů	zákon	k1gInPc2	zákon
hmotným	hmotný	k2eAgInSc7d1	hmotný
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
umístěným	umístěný	k2eAgMnSc7d1	umístěný
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc4	jeho
středu	středa	k1gFnSc4	středa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
radiálním	radiální	k2eAgNnSc6d1	radiální
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
můžeme	moct	k5eAaImIp1nP	moct
<g/>
-li	i	k?	-li
zanedbat	zanedbat	k5eAaPmF	zanedbat
relativistické	relativistický	k2eAgInPc4d1	relativistický
jevy	jev	k1gInPc4	jev
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tělesa	těleso	k1gNnPc1	těleso
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
kuželosečkách	kuželosečka	k1gFnPc6	kuželosečka
podle	podle	k7c2	podle
Keplerových	Keplerův	k2eAgInPc2d1	Keplerův
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gravitační	gravitační	k2eAgNnPc1d1	gravitační
pole	pole	k1gNnPc1	pole
planet	planeta	k1gFnPc2	planeta
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
přesného	přesný	k2eAgNnSc2d1	přesné
mapování	mapování	k1gNnSc2	mapování
pohybu	pohyb	k1gInSc2	pohyb
sond	sonda	k1gFnPc2	sonda
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
pomocí	pomocí	k7c2	pomocí
měření	měření	k1gNnSc2	měření
dopplerovského	dopplerovský	k2eAgInSc2d1	dopplerovský
posunu	posun	k1gInSc2	posun
frekvence	frekvence	k1gFnSc2	frekvence
signálu	signál	k1gInSc2	signál
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
sondou	sonda	k1gFnSc7	sonda
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
lokální	lokální	k2eAgFnPc4d1	lokální
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
nerovnoměrným	rovnoměrný	k2eNgNnSc7d1	nerovnoměrné
rozdělením	rozdělení	k1gNnSc7	rozdělení
hmoty	hmota	k1gFnSc2	hmota
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
(	(	kIx(	(
<g/>
v	v	k7c6	v
topografii	topografie	k1gFnSc6	topografie
<g/>
,	,	kIx,	,
podpovrchových	podpovrchový	k2eAgFnPc6d1	podpovrchová
strukturách	struktura	k1gFnPc6	struktura
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
anomáliích	anomálie	k1gFnPc6	anomálie
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelnost	pozorovatelnost	k1gFnSc1	pozorovatelnost
signálu	signál	k1gInSc2	signál
libovolné	libovolný	k2eAgFnSc2d1	libovolná
struktury	struktura	k1gFnSc2	struktura
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
skin	skin	k1gMnSc1	skin
efektu	efekt	k1gInSc2	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zaznamenaných	zaznamenaný	k2eAgFnPc2d1	zaznamenaná
lokálních	lokální	k2eAgFnPc2d1	lokální
variací	variace	k1gFnPc2	variace
v	v	k7c6	v
radiálním	radiální	k2eAgNnSc6d1	radiální
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
zrychlením	zrychlení	k1gNnSc7	zrychlení
lze	lze	k6eAd1	lze
zpětně	zpětně	k6eAd1	zpětně
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
planety	planeta	k1gFnSc2	planeta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
u	u	k7c2	u
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
dané	daný	k2eAgFnSc2d1	daná
průměrné	průměrný	k2eAgFnSc2d1	průměrná
mocnosti	mocnost	k1gFnSc2	mocnost
kůry	kůra	k1gFnSc2	kůra
daří	dařit	k5eAaImIp3nS	dařit
namodelovat	namodelovat	k5eAaPmF	namodelovat
její	její	k3xOp3gFnSc4	její
globální	globální	k2eAgFnSc4d1	globální
strukturu	struktura	k1gFnSc4	struktura
</s>
</p>
<p>
<s>
u	u	k7c2	u
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Venuše	Venuše	k1gFnSc2	Venuše
lze	lze	k6eAd1	lze
z	z	k7c2	z
dlouhovlnné	dlouhovlnný	k2eAgFnSc2d1	dlouhovlnná
charakteristiky	charakteristika	k1gFnSc2	charakteristika
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
odhadnout	odhadnout	k5eAaPmF	odhadnout
parametry	parametr	k1gInPc4	parametr
pláště	plášť	k1gInSc2	plášť
</s>
</p>
<p>
<s>
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
bude	být	k5eAaImBp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
možná	možná	k9	možná
u	u	k7c2	u
Merkuru	Merkur	k1gInSc2	Merkur
dokonce	dokonce	k9	dokonce
přímá	přímý	k2eAgFnSc1d1	přímá
analýza	analýza	k1gFnSc1	analýza
rozhraní	rozhraní	k1gNnSc2	rozhraní
mezi	mezi	k7c7	mezi
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
díky	dík	k1gInPc7	dík
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
poloměr	poloměr	k1gInSc1	poloměr
jádra	jádro	k1gNnSc2	jádro
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
celých	celý	k2eAgInPc2d1	celý
0,8	[number]	k4	0,8
poloměru	poloměr	k1gInSc2	poloměr
planetyObecně	planetyObecně	k6eAd1	planetyObecně
jsou	být	k5eAaImIp3nP	být
nejvýraznějšími	výrazný	k2eAgFnPc7d3	nejvýraznější
komponentami	komponenta	k1gFnPc7	komponenta
planetárních	planetární	k2eAgFnPc2d1	planetární
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
polí	pole	k1gFnPc2	pole
signály	signál	k1gInPc7	signál
velkých	velký	k2eAgFnPc2d1	velká
sopek	sopka	k1gFnPc2	sopka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
riftových	riftův	k2eAgInPc2d1	riftův
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
Valles	Valles	k1gInSc1	Valles
Marineris	Marineris	k1gFnPc2	Marineris
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
impaktních	impaktní	k2eAgFnPc2d1	impaktní
pánví	pánev	k1gFnPc2	pánev
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
tzv.	tzv.	kA	tzv.
mascony	mascon	k1gInPc1	mascon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
globální	globální	k2eAgNnSc4d1	globální
rotační	rotační	k2eAgNnSc4d1	rotační
zploštění	zploštění	k1gNnSc4	zploštění
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znalost	znalost	k1gFnSc1	znalost
přesného	přesný	k2eAgInSc2d1	přesný
tvaru	tvar	k1gInSc2	tvar
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
dané	daný	k2eAgFnSc2d1	daná
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
především	především	k9	především
pak	pak	k6eAd1	pak
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
technický	technický	k2eAgInSc4d1	technický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
i	i	k9	i
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
pod	pod	k7c7	pod
jejím	její	k3xOp3gInSc7	její
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrchové	povrchový	k2eAgFnPc1d1	povrchová
vrstvy	vrstva	k1gFnPc1	vrstva
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
než	než	k8xS	než
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
měřením	měření	k1gNnPc3	měření
zjistil	zjistit	k5eAaPmAgMnS	zjistit
George	Georg	k1gMnSc2	Georg
Biddell	Biddell	k1gMnSc1	Biddell
Airy	Aira	k1gFnPc4	Aira
už	už	k9	už
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
Země	země	k1gFnSc1	země
homogenní	homogenní	k2eAgFnSc1d1	homogenní
koulí	koule	k1gFnSc7	koule
<g/>
,	,	kIx,	,
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
by	by	kYmCp3nS	by
lineárně	lineárně	k6eAd1	lineárně
klesalo	klesat	k5eAaImAgNnS	klesat
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pohyb	pohyb	k1gInSc1	pohyb
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
==	==	k?	==
</s>
</p>
<p>
<s>
Homogenní	homogenní	k2eAgNnSc1d1	homogenní
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
centrálního	centrální	k2eAgNnSc2d1	centrální
(	(	kIx(	(
<g/>
radiálního	radiální	k2eAgNnSc2d1	radiální
<g/>
)	)	kIx)	)
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
pohybem	pohyb	k1gInSc7	pohyb
v	v	k7c6	v
homogenním	homogenní	k2eAgNnSc6d1	homogenní
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
šikmý	šikmý	k2eAgInSc1d1	šikmý
vrh	vrh	k1gInSc1	vrh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
speciálními	speciální	k2eAgInPc7d1	speciální
případy	případ	k1gInPc7	případ
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
<s>
Volný	volný	k2eAgInSc1d1	volný
pád	pád	k1gInSc1	pád
</s>
</p>
<p>
<s>
Vrh	vrh	k1gInSc1	vrh
svislý	svislý	k2eAgInSc1d1	svislý
</s>
</p>
<p>
<s>
Vrh	vrh	k1gInSc1	vrh
vodorovnýDůležitými	vodorovnýDůležitý	k2eAgFnPc7d1	vodorovnýDůležitý
charakteristikami	charakteristika	k1gFnPc7	charakteristika
šikmého	šikmý	k2eAgInSc2d1	šikmý
vrhu	vrh	k1gInSc2	vrh
jsou	být	k5eAaImIp3nP	být
počáteční	počáteční	k2eAgFnSc4d1	počáteční
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
elevační	elevační	k2eAgInSc4d1	elevační
úhel	úhel	k1gInSc4	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tíhové	tíhový	k2eAgNnSc4d1	tíhové
pole	pole	k1gNnSc4	pole
==	==	k?	==
</s>
</p>
<p>
<s>
Tíhové	tíhový	k2eAgNnSc1d1	tíhové
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rotujícího	rotující	k2eAgNnSc2d1	rotující
hmotného	hmotný	k2eAgNnSc2d1	hmotné
tělesa	těleso	k1gNnSc2	těleso
ve	v	k7c6	v
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
spojené	spojený	k2eAgFnSc6d1	spojená
s	s	k7c7	s
daným	daný	k2eAgInSc7d1	daný
bodem	bod	k1gInSc7	bod
tohoto	tento	k3xDgNnSc2	tento
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
určeno	určit	k5eAaPmNgNnS	určit
tzv.	tzv.	kA	tzv.
tíhovou	tíhový	k2eAgFnSc7d1	tíhová
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vektorovým	vektorový	k2eAgInSc7d1	vektorový
součtem	součet	k1gInSc7	součet
gravitační	gravitační	k2eAgFnSc2d1	gravitační
a	a	k8xC	a
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
tělesa	těleso	k1gNnPc4	těleso
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
působí	působit	k5eAaImIp3nS	působit
</s>
</p>
<p>
<s>
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
–	–	k?	–
pravá	pravý	k2eAgFnSc1d1	pravá
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
tělesa	těleso	k1gNnSc2	těleso
</s>
</p>
<p>
<s>
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
–	–	k?	–
setrvačná	setrvačný	k2eAgFnSc1d1	setrvačná
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
směřuje	směřovat	k5eAaImIp3nS	směřovat
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
otáčení	otáčení	k1gNnSc2	otáčení
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
rotaceVýsledná	rotaceVýsledný	k2eAgFnSc1d1	rotaceVýsledný
tíhová	tíhový	k2eAgFnSc1d1	tíhová
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
jako	jako	k9	jako
výslednice	výslednice	k1gFnSc1	výslednice
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
odstředivé	odstředivý	k2eAgFnPc1d1	odstředivá
síly	síla	k1gFnPc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
tíhová	tíhový	k2eAgFnSc1d1	tíhová
síla	síla	k1gFnSc1	síla
obecně	obecně	k6eAd1	obecně
nesměřuje	směřovat	k5eNaImIp3nS	směřovat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
úhel	úhel	k1gInSc1	úhel
mezi	mezi	k7c7	mezi
gravitační	gravitační	k2eAgFnSc7d1	gravitační
a	a	k8xC	a
odstředivou	odstředivý	k2eAgFnSc7d1	odstředivá
silou	síla	k1gFnSc7	síla
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
také	také	k9	také
tíhová	tíhový	k2eAgFnSc1d1	tíhová
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
její	její	k3xOp3gInSc4	její
směr	směr	k1gInSc4	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
je	být	k5eAaImIp3nS	být
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tělesům	těleso	k1gNnPc3	těleso
uděluje	udělovat	k5eAaImIp3nS	udělovat
tíhová	tíhový	k2eAgFnSc1d1	tíhová
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tíhová	tíhový	k2eAgFnSc1d1	tíhová
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
)	)	kIx)	)
nám	my	k3xPp1nPc3	my
určují	určovat	k5eAaImIp3nP	určovat
svislý	svislý	k2eAgInSc1d1	svislý
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
závaží	závaží	k1gNnSc2	závaží
zavěšené	zavěšený	k2eAgFnPc1d1	zavěšená
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
se	se	k3xPyFc4	se
ustálí	ustálit	k5eAaPmIp3nS	ustálit
právě	právě	k9	právě
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
působení	působení	k1gNnSc2	působení
tíhové	tíhový	k2eAgFnSc2d1	tíhová
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
a	a	k8xC	a
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
tíhového	tíhový	k2eAgNnSc2d1	tíhové
zrychlení	zrychlení	k1gNnSc2	zrychlení
g	g	kA	g
=	=	kIx~	=
9,81	[number]	k4	9,81
m	m	kA	m
s-	s-	k?	s-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
trajektorie	trajektorie	k1gFnSc1	trajektorie
pohybu	pohyb	k1gInSc2	pohyb
tělesa	těleso	k1gNnSc2	těleso
dostatečně	dostatečně	k6eAd1	dostatečně
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
lze	lze	k6eAd1	lze
zanedbat	zanedbat	k5eAaPmF	zanedbat
změny	změna	k1gFnPc4	změna
vektoru	vektor	k1gInSc2	vektor
tíhového	tíhový	k2eAgNnSc2d1	tíhové
zrychlení	zrychlení	k1gNnSc2	zrychlení
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
tak	tak	k6eAd1	tak
i	i	k8xC	i
směru	směr	k1gInSc6	směr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
homogenním	homogenní	k2eAgNnSc7d1	homogenní
tíhovým	tíhový	k2eAgNnSc7d1	tíhové
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
homogenního	homogenní	k2eAgNnSc2d1	homogenní
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
tíhovém	tíhový	k2eAgNnSc6d1	tíhové
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
získávají	získávat	k5eAaImIp3nP	získávat
potenciální	potenciální	k2eAgFnSc4d1	potenciální
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
těleso	těleso	k1gNnSc1	těleso
na	na	k7c4	na
podložku	podložka	k1gFnSc4	podložka
či	či	k8xC	či
závěs	závěs	k1gInSc4	závěs
v	v	k7c6	v
tíhovém	tíhový	k2eAgNnSc6d1	tíhové
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tíha	tíha	k1gFnSc1	tíha
<g/>
.	.	kIx.	.
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1	měrná
tíha	tíha	k1gFnSc1	tíha
je	být	k5eAaImIp3nS	být
tíha	tíha	k1gFnSc1	tíha
látky	látka	k1gFnSc2	látka
o	o	k7c6	o
jednotkovém	jednotkový	k2eAgInSc6d1	jednotkový
objemu	objem	k1gInSc6	objem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GREENE	GREENE	kA	GREENE
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
<g/>
.	.	kIx.	.
</s>
<s>
Elegantní	elegantní	k2eAgInSc1d1	elegantní
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Motl	Motl	k1gMnSc1	Motl
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
398	[number]	k4	398
s.	s.	k?	s.
Edice	edice	k1gFnSc1	edice
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
156	[number]	k4	156
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
882	[number]	k4	882
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgInSc1d1	gravitační
manévr	manévr	k1gInSc1	manévr
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
vlna	vlna	k1gFnSc1	vlna
</s>
</p>
<p>
<s>
Keplerovy	Keplerův	k2eAgInPc4d1	Keplerův
zákony	zákon	k1gInPc4	zákon
</s>
</p>
<p>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
rychlost	rychlost	k1gFnSc1	rychlost
</s>
</p>
<p>
<s>
Mechanika	mechanika	k1gFnSc1	mechanika
</s>
</p>
<p>
<s>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
gravitační	gravitační	k2eAgInSc1d1	gravitační
zákon	zákon	k1gInSc1	zákon
</s>
</p>
<p>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
</s>
</p>
<p>
<s>
M-teorie	Meorie	k1gFnSc1	M-teorie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gravitace	gravitace	k1gFnSc2	gravitace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gravitace	gravitace	k1gFnSc2	gravitace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Simulátor	simulátor	k1gInSc1	simulátor
gravitace	gravitace	k1gFnSc2	gravitace
(	(	kIx(	(
<g/>
Flash	Flash	k1gInSc1	Flash
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
na	na	k7c4	na
Aldebaranu	Aldebarana	k1gFnSc4	Aldebarana
</s>
</p>
