<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
je	být	k5eAaImIp3nS	být
romaneto	romaneto	k1gNnSc4	romaneto
Jakuba	Jakub	k1gMnSc2	Jakub
Arbese	Arbes	k1gMnSc2	Arbes
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
nejznámější	známý	k2eAgInPc4d3	nejznámější
a	a	k8xC	a
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
Arbesovo	Arbesův	k2eAgNnSc4d1	Arbesovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vědecko-detektivní	vědeckoetektivní	k2eAgNnSc4d1	vědecko-detektivní
dílo	dílo	k1gNnSc4	dílo
s	s	k7c7	s
fantastickými	fantastický	k2eAgInPc7d1	fantastický
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
děj	děj	k1gInSc4	děj
nastolení	nastolení	k1gNnSc2	nastolení
neuvěřitelné	uvěřitelný	k2eNgFnSc2d1	neuvěřitelná
záhady	záhada	k1gFnSc2	záhada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
však	však	k9	však
vyvrácena	vyvrácen	k2eAgFnSc1d1	vyvrácena
nějakým	nějaký	k3yIgNnSc7	nějaký
logickým	logický	k2eAgNnSc7d1	logické
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
věc	věc	k1gFnSc4	věc
banalizuje	banalizovat	k5eAaImIp3nS	banalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
romantické	romantický	k2eAgInPc4d1	romantický
i	i	k8xC	i
realistické	realistický	k2eAgInPc4d1	realistický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
nikde	nikde	k6eAd1	nikde
neuvádí	uvádět	k5eNaImIp3nS	uvádět
svoje	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
-	-	kIx~	-
neznámý	známý	k2eNgMnSc1d1	neznámý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Arbes	Arbes	k1gMnSc1	Arbes
potká	potkat	k5eAaPmIp3nS	potkat
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
mu	on	k3xPp3gMnSc3	on
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
Svatému	svatý	k2eAgMnSc3d1	svatý
Xaveriovi	Xaverius	k1gMnSc3	Xaverius
(	(	kIx(	(
<g/>
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
je	být	k5eAaImIp3nS	být
racionálně	racionálně	k6eAd1	racionálně
uvažující	uvažující	k2eAgMnSc1d1	uvažující
<g/>
,	,	kIx,	,
cílovědomě	cílovědomě	k6eAd1	cílovědomě
jednající	jednající	k2eAgMnSc1d1	jednající
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
okolo	okolo	k7c2	okolo
obrazu	obraz	k1gInSc2	obraz
Smrti	smrt	k1gFnSc2	smrt
svatého	svatý	k2eAgMnSc2d1	svatý
Xaveria	Xaverius	k1gMnSc2	Xaverius
ve	v	k7c6	v
Svatomikulášském	svatomikulášský	k2eAgInSc6d1	svatomikulášský
chrámu	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
knihy	kniha	k1gFnSc2	kniha
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
právě	právě	k9	právě
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
dějem	děj	k1gInSc7	děj
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
konci	konec	k1gInSc6	konec
Bachova	Bachův	k2eAgInSc2d1	Bachův
absolutismu	absolutismus	k1gInSc2	absolutismus
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
literární	literární	k2eAgInPc4d1	literární
postupy	postup	k1gInPc4	postup
chronologický	chronologický	k2eAgInSc4d1	chronologický
a	a	k8xC	a
retrospektivní	retrospektivní	k2eAgInSc4d1	retrospektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
Svatomikulášském	svatomikulášský	k2eAgInSc6d1	svatomikulášský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
se	se	k3xPyFc4	se
prochází	procházet	k5eAaImIp3nS	procházet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
po	po	k7c6	po
pražských	pražský	k2eAgInPc6d1	pražský
kostelech	kostel	k1gInPc6	kostel
a	a	k8xC	a
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
se	se	k3xPyFc4	se
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Kritizují	kritizovat	k5eAaImIp3nP	kritizovat
zde	zde	k6eAd1	zde
žalostný	žalostný	k2eAgInSc4d1	žalostný
stav	stav	k1gInSc4	stav
českého	český	k2eAgNnSc2d1	české
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pramálo	pramálo	k6eAd1	pramálo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
za	za	k7c4	za
světovou	světový	k2eAgFnSc4d1	světová
neznalost	neznalost	k1gFnSc4	neznalost
viní	vinit	k5eAaImIp3nS	vinit
Němce	Němec	k1gMnPc4	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
publikacích	publikace	k1gFnPc6	publikace
české	český	k2eAgFnSc2d1	Česká
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
díla	dílo	k1gNnPc4	dílo
buďto	buďto	k8xC	buďto
vůbec	vůbec	k9	vůbec
nezmiňují	zmiňovat	k5eNaImIp3nP	zmiňovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
jako	jako	k9	jako
okrajovou	okrajový	k2eAgFnSc4d1	okrajová
poznámku	poznámka	k1gFnSc4	poznámka
<g/>
,	,	kIx,	,
světové	světový	k2eAgFnPc1d1	světová
velmoci	velmoc	k1gFnPc1	velmoc
jako	jako	k8xC	jako
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Anglie	Anglie	k1gFnSc1	Anglie
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
čerpají	čerpat	k5eAaImIp3nP	čerpat
právě	právě	k9	právě
z	z	k7c2	z
německých	německý	k2eAgInPc2d1	německý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
se	se	k3xPyFc4	se
na	na	k7c6	na
plánu	plán	k1gInSc6	plán
<g/>
,	,	kIx,	,
že	že	k8xS	že
pořídí	pořídit	k5eAaPmIp3nS	pořídit
barevné	barevný	k2eAgFnPc4d1	barevná
kopie	kopie	k1gFnPc4	kopie
všech	všecek	k3xTgInPc2	všecek
obrazů	obraz	k1gInPc2	obraz
z	z	k7c2	z
pražských	pražský	k2eAgInPc2d1	pražský
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
vydají	vydat	k5eAaPmIp3nP	vydat
je	on	k3xPp3gFnPc4	on
ve	v	k7c6	v
vázaném	vázané	k1gNnSc6	vázané
barvotisku	barvotisk	k1gInSc2	barvotisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přítel	přítel	k1gMnSc1	přítel
však	však	k9	však
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
a	a	k8xC	a
z	z	k7c2	z
plánu	plán	k1gInSc2	plán
sejde	sejít	k5eAaPmIp3nS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
zavítá	zavítat	k5eAaPmIp3nS	zavítat
vypravěč	vypravěč	k1gMnSc1	vypravěč
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
omylem	omylem	k6eAd1	omylem
zavřen	zavřen	k2eAgMnSc1d1	zavřen
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
Xaveriem	Xaverius	k1gMnSc7	Xaverius
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
zatím	zatím	k6eAd1	zatím
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
nezná	znát	k5eNaImIp3nS	znát
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
setkání	setkání	k1gNnSc4	setkání
navíc	navíc	k6eAd1	navíc
nedopadlo	dopadnout	k5eNaPmAgNnS	dopadnout
přátelsky	přátelsky	k6eAd1	přátelsky
-	-	kIx~	-
když	když	k8xS	když
pro	pro	k7c4	pro
vypravěče	vypravěč	k1gMnSc4	vypravěč
neznámý	známý	k2eNgMnSc1d1	neznámý
muž	muž	k1gMnSc1	muž
přijde	přijít	k5eAaPmIp3nS	přijít
před	před	k7c4	před
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
podezřelý	podezřelý	k2eAgInSc4d1	podezřelý
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
předmět	předmět	k1gInSc4	předmět
a	a	k8xC	a
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
si	se	k3xPyFc3	se
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
obrazu	obraz	k1gInSc3	obraz
<g/>
,	,	kIx,	,
strhne	strhnout	k5eAaPmIp3nS	strhnout
se	se	k3xPyFc4	se
potyčka	potyčka	k1gFnSc1	potyčka
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
drží	držet	k5eAaImIp3nS	držet
nůž	nůž	k1gInSc4	nůž
a	a	k8xC	a
obraz	obraz	k1gInSc4	obraz
chce	chtít	k5eAaImIp3nS	chtít
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
kružítko	kružítko	k1gNnSc4	kružítko
<g/>
,	,	kIx,	,
Xaverius	Xaverius	k1gMnSc1	Xaverius
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
obraz	obraz	k1gInSc1	obraz
překreslit	překreslit	k5eAaPmF	překreslit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
nedůvěřivých	důvěřivý	k2eNgNnPc6d1	nedůvěřivé
setkáních	setkání	k1gNnPc6	setkání
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
stávají	stávat	k5eAaImIp3nP	stávat
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
společné	společný	k2eAgFnSc6d1	společná
návštěvě	návštěva	k1gFnSc6	návštěva
začne	začít	k5eAaPmIp3nS	začít
vypravěči	vypravěč	k1gMnSc3	vypravěč
líčit	líčit	k5eAaImF	líčit
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Xaverius	Xaverius	k1gMnSc1	Xaverius
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
mu	on	k3xPp3gMnSc3	on
vybrala	vybrat	k5eAaPmAgFnS	vybrat
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
bigotní	bigotní	k2eAgFnSc2d1	bigotní
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
i	i	k8xC	i
babička	babička	k1gFnSc1	babička
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
zbožné	zbožný	k2eAgFnPc1d1	zbožná
<g/>
,	,	kIx,	,
zasvětily	zasvětit	k5eAaPmAgFnP	zasvětit
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
obrazu	obraz	k1gInSc2	obraz
svatého	svatý	k2eAgMnSc2d1	svatý
Xaveria	Xaverius	k1gMnSc2	Xaverius
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
Xaveria	Xaverius	k1gMnSc2	Xaverius
jako	jako	k8xS	jako
mladá	mladá	k1gFnSc1	mladá
pracovala	pracovat	k5eAaImAgFnS	pracovat
u	u	k7c2	u
malíře	malíř	k1gMnSc2	malíř
obrazu	obraz	k1gInSc2	obraz
Františka	František	k1gMnSc4	František
Balka	Balek	k1gMnSc4	Balek
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Palko	Palko	k1gNnSc4	Palko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
jí	jíst	k5eAaImIp3nS	jíst
malíř	malíř	k1gMnSc1	malíř
vyzradil	vyzradit	k5eAaPmAgMnS	vyzradit
tajemství	tajemství	k1gNnSc2	tajemství
ukryté	ukrytý	k2eAgFnPc1d1	ukrytá
v	v	k7c6	v
obrazu	obraz	k1gInSc6	obraz
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Obraz	obraz	k1gInSc1	obraz
umírajícího	umírající	k2eAgMnSc2d1	umírající
svatého	svatý	k2eAgMnSc2d1	svatý
Xaveria	Xaverius	k1gMnSc2	Xaverius
nebyl	být	k5eNaImAgInS	být
malován	malovat	k5eAaImNgInS	malovat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
oko	oko	k1gNnSc4	oko
a	a	k8xC	a
pro	pro	k7c4	pro
povznesení	povznesení	k1gNnSc4	povznesení
mysli	mysl	k1gFnSc2	mysl
zbožných	zbožný	k2eAgMnPc2d1	zbožný
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
největších	veliký	k2eAgMnPc2d3	veliký
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
cosi	cosi	k3yInSc1	cosi
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
čímž	což	k3yRnSc7	což
by	by	kYmCp3nS	by
tisícové	tisícový	k2eAgFnPc1d1	tisícová
s	s	k7c7	s
pohrdáním	pohrdání	k1gNnSc7	pohrdání
krčili	krčit	k5eAaImAgMnP	krčit
ramenoma	ramenoma	k?	ramenoma
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
však	však	k9	však
by	by	kYmCp3nS	by
přece	přece	k9	přece
mohla	moct	k5eAaImAgFnS	moct
vzejíti	vzejít	k5eAaPmF	vzejít
spása	spása	k1gFnSc1	spása
milionům	milion	k4xCgInPc3	milion
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
dovede	dovést	k5eAaPmIp3nS	dovést
třeba	třeba	k6eAd1	třeba
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
dlíti	dlít	k5eAaImF	dlít
před	před	k7c7	před
obrazem	obraz	k1gInSc7	obraz
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
upne	upnout	k5eAaPmIp3nS	upnout
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
mysl	mysl	k1gFnSc4	mysl
na	na	k7c4	na
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tajemství	tajemství	k1gNnPc1	tajemství
v	v	k7c6	v
obrazu	obraz	k1gInSc6	obraz
skryté	skrytý	k2eAgNnSc4d1	skryté
vypátral	vypátrat	k5eAaPmAgMnS	vypátrat
a	a	k8xC	a
komu	kdo	k3yInSc3	kdo
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
tajemství	tajemství	k1gNnSc1	tajemství
to	ten	k3xDgNnSc1	ten
vypátrati	vypátrat	k5eAaPmF	vypátrat
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
zůstati	zůstat	k5eAaPmF	zůstat
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
bez	bez	k7c2	bez
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
duch	duch	k1gMnSc1	duch
jeho	jeho	k3xOp3gMnSc1	jeho
nabude	nabýt	k5eAaPmIp3nS	nabýt
oné	onen	k3xDgFnSc2	onen
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
průpravy	průprava	k1gFnSc2	průprava
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
je	být	k5eAaImIp3nS	být
potřebí	potřebí	k6eAd1	potřebí
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
dílu	díl	k1gInSc3	díl
na	na	k7c4	na
prospěch	prospěch	k1gInSc4	prospěch
veškerého	veškerý	k3xTgNnSc2	veškerý
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediné	jediné	k1gNnSc4	jediné
však	však	k9	však
věci	věc	k1gFnSc3	věc
je	být	k5eAaImIp3nS	být
především	především	k9	především
třeba	třeba	k9	třeba
<g/>
:	:	kIx,	:
Vytrvalosti	vytrvalost	k1gFnSc2	vytrvalost
a	a	k8xC	a
železné	železný	k2eAgFnSc2d1	železná
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
těmito	tento	k3xDgFnPc7	tento
obrněn	obrnit	k5eAaPmNgInS	obrnit
bude	být	k5eAaImBp3nS	být
před	před	k7c7	před
obrazem	obraz	k1gInSc7	obraz
dlíti	dlít	k5eAaImF	dlít
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
vypátral	vypátrat	k5eAaPmAgInS	vypátrat
tajemství	tajemství	k1gNnSc4	tajemství
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
svatý	svatý	k1gMnSc1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
zjeví	zjevit	k5eAaPmIp3nS	zjevit
neocenitelný	ocenitelný	k2eNgInSc4d1	neocenitelný
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tak	tak	k6eAd1	tak
zní	znět	k5eAaImIp3nS	znět
poslední	poslední	k2eAgFnPc4d1	poslední
slova	slovo	k1gNnPc4	slovo
malíře	malíř	k1gMnSc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
a	a	k8xC	a
zasvětí	zasvětit	k5eAaPmIp3nS	zasvětit
obrazu	obraz	k1gInSc3	obraz
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
chodí	chodit	k5eAaImIp3nP	chodit
modlit	modlit	k5eAaImF	modlit
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
zbožnost	zbožnost	k1gFnSc4	zbožnost
pak	pak	k6eAd1	pak
předá	předat	k5eAaPmIp3nS	předat
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
víra	víra	k1gFnSc1	víra
přenáší	přenášet	k5eAaImIp3nS	přenášet
i	i	k9	i
na	na	k7c4	na
Xaveria	Xaverius	k1gMnSc4	Xaverius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Xaverius	Xaverius	k1gMnSc1	Xaverius
již	již	k6eAd1	již
však	však	k9	však
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
nevěří	věřit	k5eNaImIp3nP	věřit
<g/>
,	,	kIx,	,
věří	věřit	k5eAaImIp3nS	věřit
spíše	spíše	k9	spíše
na	na	k7c4	na
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
obory	obora	k1gFnPc4	obora
jsou	být	k5eAaImIp3nP	být
logika	logika	k1gFnSc1	logika
a	a	k8xC	a
matematika	matematika	k1gFnSc1	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
snaží	snažit	k5eAaImIp3nP	snažit
vypátrat	vypátrat	k5eAaPmF	vypátrat
pomocí	pomocí	k7c2	pomocí
matematiky	matematika	k1gFnSc2	matematika
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
přinese	přinést	k5eAaPmIp3nS	přinést
veliké	veliký	k2eAgNnSc4d1	veliké
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
práce	práce	k1gFnSc1	práce
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
vypravěčem	vypravěč	k1gMnSc7	vypravěč
skvělí	skvělý	k2eAgMnPc1d1	skvělý
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
vyluští	vyluštit	k5eAaPmIp3nS	vyluštit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
plánek	plánek	k1gInSc4	plánek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
Malvazinské	Malvazinský	k2eAgFnSc2d1	Malvazinský
vinice	vinice	k1gFnSc2	vinice
za	za	k7c7	za
Smíchovem	Smíchov	k1gInSc7	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
najdou	najít	k5eAaPmIp3nP	najít
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
tu	ten	k3xDgFnSc4	ten
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Xaverius	Xaverius	k1gMnSc1	Xaverius
vyluští	vyluštit	k5eAaPmIp3nS	vyluštit
mapku	mapka	k1gFnSc4	mapka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Malvazinek	malvazinka	k1gFnPc2	malvazinka
a	a	k8xC	a
na	na	k7c6	na
určeném	určený	k2eAgNnSc6d1	určené
místě	místo	k1gNnSc6	místo
začnou	začít	k5eAaPmIp3nP	začít
kopat	kopat	k5eAaImF	kopat
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
pár	pár	k1gInSc1	pár
vzácných	vzácný	k2eAgInPc2d1	vzácný
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
tak	tak	k6eAd1	tak
horlivě	horlivě	k6eAd1	horlivě
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Nedopatřením	nedopatření	k1gNnSc7	nedopatření
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
zahozené	zahozený	k2eAgFnSc2d1	zahozená
sirky	sirka	k1gFnSc2	sirka
vznítí	vznítit	k5eAaPmIp3nS	vznítit
suchá	suchý	k2eAgFnSc1d1	suchá
tráva	tráva	k1gFnSc1	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
uhašen	uhašen	k2eAgMnSc1d1	uhašen
<g/>
,	,	kIx,	,
Xaverius	Xaverius	k1gMnSc1	Xaverius
však	však	k9	však
blouzní	blouznit	k5eAaImIp3nS	blouznit
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
postavu	postav	k1gInSc2	postav
Svatého	svatý	k2eAgMnSc4d1	svatý
Xaveria	Xaverius	k1gMnSc4	Xaverius
a	a	k8xC	a
utíká	utíkat	k5eAaImIp3nS	utíkat
i	i	k9	i
s	s	k7c7	s
vykopaným	vykopaný	k2eAgInSc7d1	vykopaný
pokladem	poklad	k1gInSc7	poklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
vypravěč	vypravěč	k1gMnSc1	vypravěč
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
redakce	redakce	k1gFnSc2	redakce
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
některým	některý	k3yIgInPc3	některý
článkům	článek	k1gInPc3	článek
vězněn	vězněn	k2eAgInSc4d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
má	mít	k5eAaImIp3nS	mít
vypravěč	vypravěč	k1gMnSc1	vypravěč
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
věznice	věznice	k1gFnSc2	věznice
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
věznice	věznice	k1gFnSc2	věznice
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
s	s	k7c7	s
vidinou	vidina	k1gFnSc7	vidina
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
amnestie	amnestie	k1gFnSc1	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
se	se	k3xPyFc4	se
potkává	potkávat	k5eAaImIp3nS	potkávat
s	s	k7c7	s
Xaveriem	Xaverius	k1gMnSc7	Xaverius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
vězněn	věznit	k5eAaImNgMnS	věznit
za	za	k7c4	za
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
krádeže	krádež	k1gFnSc2	krádež
v	v	k7c6	v
Svatomikulášském	svatomikulášský	k2eAgInSc6d1	svatomikulášský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Omlouvá	omlouvat	k5eAaImIp3nS	omlouvat
se	se	k3xPyFc4	se
vypravěči	vypravěč	k1gMnSc3	vypravěč
za	za	k7c4	za
úprk	úprk	k1gInSc4	úprk
<g/>
,	,	kIx,	,
že	že	k8xS	že
prý	prý	k9	prý
blouznil	blouznil	k1gMnSc1	blouznil
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
z	z	k7c2	z
ohně	oheň	k1gInSc2	oheň
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Svatý	svatý	k2eAgMnSc1d1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prchl	prchnout	k5eAaPmAgMnS	prchnout
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
truhle	truhla	k1gFnSc6	truhla
nic	nic	k3yNnSc1	nic
cenného	cenný	k2eAgNnSc2d1	cenné
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
si	se	k3xPyFc3	se
po	po	k7c4	po
vyprávění	vyprávění	k1gNnSc4	vyprávění
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
datum	datum	k1gNnSc1	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgMnS	mít
Xaverius	Xaverius	k1gMnSc1	Xaverius
spáchat	spáchat	k5eAaPmF	spáchat
krádež	krádež	k1gFnSc4	krádež
vychází	vycházet	k5eAaImIp3nS	vycházet
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kopali	kopat	k5eAaImAgMnP	kopat
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
propuštění	propuštění	k1gNnSc6	propuštění
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
přítele	přítel	k1gMnSc2	přítel
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
získá	získat	k5eAaPmIp3nS	získat
propuštění	propuštění	k1gNnSc1	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
je	být	k5eAaImIp3nS	být
však	však	k9	však
těžce	těžce	k6eAd1	těžce
nemocný	mocný	k2eNgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
trápí	trápit	k5eAaImIp3nS	trápit
ho	on	k3xPp3gMnSc4	on
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
jejich	jejich	k3xOp3gNnSc2	jejich
společného	společný	k2eAgNnSc2d1	společné
propuštění	propuštění	k1gNnSc2	propuštění
Xaverius	Xaverius	k1gMnSc1	Xaverius
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
vypravěč	vypravěč	k1gMnSc1	vypravěč
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zničit	zničit	k5eAaPmF	zničit
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zapříčinil	zapříčinit	k5eAaPmAgInS	zapříčinit
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gMnSc2	jeho
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
rozhraní	rozhraní	k1gNnSc1	rozhraní
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jak	jak	k6eAd1	jak
romantické	romantický	k2eAgNnSc1d1	romantické
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
realistické	realistický	k2eAgInPc1d1	realistický
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Realistické	realistický	k2eAgInPc1d1	realistický
prvky	prvek	k1gInPc1	prvek
===	===	k?	===
</s>
</p>
<p>
<s>
Vlastenecký	vlastenecký	k2eAgInSc1d1	vlastenecký
úvod	úvod	k1gInSc1	úvod
<g/>
,	,	kIx,	,
oplakávání	oplakávání	k1gNnSc1	oplakávání
českého	český	k2eAgNnSc2d1	české
malířství	malířství	k1gNnSc2	malířství
</s>
</p>
<p>
<s>
Logické	logický	k2eAgNnSc1d1	logické
vyřešení	vyřešení	k1gNnSc1	vyřešení
záhad	záhada	k1gFnPc2	záhada
</s>
</p>
<p>
<s>
Realistické	realistický	k2eAgNnSc1d1	realistické
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
postav	postava	k1gFnPc2	postava
</s>
</p>
<p>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
popisné	popisný	k2eAgFnPc1d1	popisná
pasáže	pasáž	k1gFnPc1	pasáž
</s>
</p>
<p>
<s>
===	===	k?	===
Romantické	romantický	k2eAgInPc1d1	romantický
prvky	prvek	k1gInPc1	prvek
===	===	k?	===
</s>
</p>
<p>
<s>
Rozvrácený	rozvrácený	k2eAgMnSc1d1	rozvrácený
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
</s>
</p>
<p>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
z	z	k7c2	z
pochybné	pochybný	k2eAgFnSc2d1	pochybná
bigotní	bigotní	k2eAgFnSc2d1	bigotní
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
neznámý	známý	k2eNgMnSc1d1	neznámý
otec	otec	k1gMnSc1	otec
i	i	k8xC	i
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
poblouzněný	poblouzněný	k2eAgInSc1d1	poblouzněný
představami	představa	k1gFnPc7	představa
</s>
</p>
<p>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
nočního	noční	k2eAgInSc2d1	noční
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc1d1	noční
výprava	výprava	k1gFnSc1	výprava
do	do	k7c2	do
Malvazinek	malvazinka	k1gFnPc2	malvazinka
</s>
</p>
<p>
<s>
Líčení	líčení	k1gNnSc1	líčení
přírody	příroda	k1gFnSc2	příroda
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
pokladem	poklad	k1gInSc7	poklad
</s>
</p>
<p>
<s>
Tragický	tragický	k2eAgInSc1d1	tragický
konec	konec	k1gInSc1	konec
-	-	kIx~	-
hrdina	hrdina	k1gMnSc1	hrdina
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
poklad	poklad	k1gInSc1	poklad
není	být	k5eNaImIp3nS	být
poklad	poklad	k1gInSc4	poklad
<g/>
,	,	kIx,	,
promarněný	promarněný	k2eAgInSc4d1	promarněný
život	život	k1gInSc4	život
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ARBES	Arbes	k1gMnSc1	Arbes
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Romaneta	romaneto	k1gNnPc1	romaneto
I.	I.	kA	I.
-	-	kIx~	-
Svatý	svatý	k2eAgMnSc1d1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
plný	plný	k2eAgInSc1d1	plný
text	text	k1gInSc1	text
na	na	k7c6	na
wikizdrojích	wikizdroj	k1gInPc6	wikizdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ARBES	Arbes	k1gMnSc1	Arbes
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
<g/>
:	:	kIx,	:
romanetto	romanetto	k1gNnSc1	romanetto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
75	[number]	k4	75
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
digitalizát	digitalizát	k1gInSc4	digitalizát
v	v	k7c6	v
Krameriovi	Kramerius	k1gMnSc6	Kramerius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ARBES	Arbes	k1gMnSc1	Arbes
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
mozek	mozek	k1gInSc1	mozek
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
romaneta	romaneto	k1gNnPc1	romaneto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc2	KMa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
336	[number]	k4	336
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7309	[number]	k4	7309
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
69	[number]	k4	69
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
