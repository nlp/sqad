<s>
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Diana	Diana	k1gFnSc1	Diana
Frances	Frances	k1gInSc1	Frances
Spencerová	Spencerová	k1gFnSc1	Spencerová
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jen	jen	k9	jen
jako	jako	k8xS	jako
princezna	princezna	k1gFnSc1	princezna
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
Sandringham	Sandringham	k1gInSc1	Sandringham
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
následníka	následník	k1gMnSc4	následník
britského	britský	k2eAgInSc2d1	britský
trůnu	trůn	k1gInSc2	trůn
prince	princ	k1gMnSc2	princ
Charlese	Charles	k1gMnSc2	Charles
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
jeho	jeho	k3xOp3gInPc2	jeho
dvou	dva	k4xCgInPc2	dva
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
