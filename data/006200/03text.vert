<s>
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Diana	Diana	k1gFnSc1	Diana
Frances	Frances	k1gInSc1	Frances
Spencerová	Spencerová	k1gFnSc1	Spencerová
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jen	jen	k9	jen
jako	jako	k8xS	jako
princezna	princezna	k1gFnSc1	princezna
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
Sandringham	Sandringham	k1gInSc1	Sandringham
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
následníka	následník	k1gMnSc4	následník
britského	britský	k2eAgInSc2d1	britský
trůnu	trůn	k1gInSc2	trůn
prince	princ	k1gMnSc2	princ
Charlese	Charles	k1gMnSc2	Charles
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
jeho	jeho	k3xOp3gInPc2	jeho
dvou	dva	k4xCgInPc2	dva
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
rozvodu	rozvod	k1gInSc6	rozvod
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
charitativní	charitativní	k2eAgFnSc4d1	charitativní
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
Červený	červený	k2eAgInSc4d1	červený
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
pozemním	pozemní	k2eAgFnPc3d1	pozemní
minám	mina	k1gFnPc3	mina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
a	a	k8xC	a
nejpopulárnější	populární	k2eAgFnPc4d3	nejpopulárnější
světové	světový	k2eAgFnPc4d1	světová
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
přítelem	přítel	k1gMnSc7	přítel
Dodim	Dodima	k1gFnPc2	Dodima
al-Fayedem	al-Fayed	k1gMnSc7	al-Fayed
zahynula	zahynout	k5eAaPmAgFnS	zahynout
při	při	k7c6	při
automobilové	automobilový	k2eAgFnSc6d1	automobilová
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
příčiny	příčina	k1gFnPc1	příčina
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
staly	stát	k5eAaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
spekulací	spekulace	k1gFnPc2	spekulace
a	a	k8xC	a
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
Frances	Francesa	k1gFnPc2	Francesa
Spencerová	Spencerová	k1gFnSc1	Spencerová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
v	v	k7c6	v
aristokratické	aristokratický	k2eAgFnSc6d1	aristokratická
rodině	rodina	k1gFnSc6	rodina
Spencerů	Spencer	k1gInPc2	Spencer
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
John	John	k1gMnSc1	John
Spencer	Spencer	k1gMnSc1	Spencer
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Frances	Frances	k1gInSc1	Frances
Rocheová	Rocheová	k1gFnSc1	Rocheová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
sourozenci	sourozenec	k1gMnPc7	sourozenec
v	v	k7c4	v
Park	park	k1gInSc4	park
House	house	k1gNnSc1	house
na	na	k7c6	na
sandringhamském	sandringhamský	k2eAgInSc6d1	sandringhamský
statku	statek	k1gInSc6	statek
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pokřtěná	pokřtěný	k2eAgFnSc1d1	pokřtěná
farářem	farář	k1gMnSc7	farář
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
biskupem	biskup	k1gMnSc7	biskup
Percym	Percym	k1gInSc1	Percym
Herbertem	Herbert	k1gInSc7	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
Dianu	Diana	k1gFnSc4	Diana
těžce	těžce	k6eAd1	těžce
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
rozvod	rozvod	k1gInSc1	rozvod
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
byli	být	k5eAaImAgMnP	být
svěřeni	svěřen	k2eAgMnPc1d1	svěřen
do	do	k7c2	do
otcovy	otcův	k2eAgFnSc2d1	otcova
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
našli	najít	k5eAaPmAgMnP	najít
nové	nový	k2eAgMnPc4d1	nový
partnery	partner	k1gMnPc4	partner
<g/>
,	,	kIx,	,
Diana	Diana	k1gFnSc1	Diana
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
k	k	k7c3	k
oběma	dva	k4xCgMnPc7	dva
velmi	velmi	k6eAd1	velmi
vřelý	vřelý	k2eAgInSc4d1	vřelý
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
stal	stát	k5eAaPmAgMnS	stát
osmým	osmý	k4xOgMnSc7	osmý
hrabětem	hrabě	k1gMnSc7	hrabě
Spencerem	Spencer	k1gMnSc7	Spencer
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
dědečka	dědeček	k1gMnSc2	dědeček
Alberta	Albert	k1gMnSc2	Albert
Spencera	Spencer	k1gMnSc2	Spencer
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
titul	titul	k1gInSc1	titul
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c6	na
Lady	lady	k1gFnSc6	lady
Diana	Diana	k1gFnSc1	Diana
Spencerová	Spencerová	k1gFnSc1	Spencerová
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
směla	smět	k5eAaImAgFnS	smět
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
Diana	Diana	k1gFnSc1	Diana
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
studovala	studovat	k5eAaImAgFnS	studovat
v	v	k7c4	v
Riddlesworth	Riddlesworth	k1gInSc4	Riddlesworth
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
Norfolku	Norfolek	k1gInSc6	Norfolek
a	a	k8xC	a
poté	poté	k6eAd1	poté
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Heath	Heath	k1gInSc4	Heath
Girls	girl	k1gFnPc2	girl
<g/>
'	'	kIx"	'
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
Sevenoaksu	Sevenoaks	k1gInSc6	Sevenoaks
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
podprůměrnou	podprůměrný	k2eAgFnSc4d1	podprůměrná
studentku	studentka	k1gFnSc4	studentka
<g/>
.	.	kIx.	.
</s>
<s>
Nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
zkoušky	zkouška	k1gFnPc1	zkouška
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
opustila	opustit	k5eAaPmAgFnS	opustit
West	West	k2eAgInSc4d1	West
Heath	Heath	k1gInSc4	Heath
a	a	k8xC	a
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
institut	institut	k1gInSc4	institut
Alpin	alpinum	k1gNnPc2	alpinum
Videmanette	Videmanett	k1gMnSc5	Videmanett
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
dívčí	dívčí	k2eAgFnSc1d1	dívčí
škola	škola	k1gFnSc1	škola
v	v	k7c4	v
Rougemont	Rougemont	k1gInSc4	Rougemont
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nevynikala	vynikat	k5eNaImAgFnS	vynikat
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Heath	Heath	k1gInSc4	Heath
v	v	k7c6	v
Kentu	Kent	k1gInSc6	Kent
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byla	být	k5eAaImAgFnS	být
odměněna	odměnit	k5eAaPmNgFnS	odměnit
speciální	speciální	k2eAgFnSc7d1	speciální
cenou	cena	k1gFnSc7	cena
za	za	k7c2	za
svojí	svůj	k3xOyFgFnSc2	svůj
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
službu	služba	k1gFnSc4	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
popisována	popisovat	k5eAaImNgFnS	popisovat
jako	jako	k9	jako
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dělá	dělat	k5eAaImIp3nS	dělat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
krátkodobě	krátkodobě	k6eAd1	krátkodobě
tam	tam	k6eAd1	tam
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
chůva	chůva	k1gFnSc1	chůva
a	a	k8xC	a
vychovatelka	vychovatelka	k1gFnSc1	vychovatelka
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
učitelkou	učitelka	k1gFnSc7	učitelka
v	v	k7c6	v
mateřské	mateřský	k2eAgFnSc6d1	mateřská
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
byla	být	k5eAaImAgFnS	být
talentovaná	talentovaný	k2eAgFnSc1d1	talentovaná
amatérská	amatérský	k2eAgFnSc1d1	amatérská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
vynikala	vynikat	k5eAaImAgFnS	vynikat
ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
toužila	toužit	k5eAaImAgFnS	toužit
být	být	k5eAaImF	být
tanečnicí	tanečnice	k1gFnSc7	tanečnice
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
sourozenci	sourozenec	k1gMnPc1	sourozenec
vyrůstali	vyrůstat	k5eAaImAgMnP	vyrůstat
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
na	na	k7c6	na
královských	královský	k2eAgInPc6d1	královský
statcích	statek	k1gInPc6	statek
v	v	k7c6	v
Sandringhamu	Sandringham	k1gInSc6	Sandringham
a	a	k8xC	a
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
domě	dům	k1gInSc6	dům
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
v	v	k7c6	v
Althorpu	Althorp	k1gInSc6	Althorp
<g/>
.	.	kIx.	.
</s>
<s>
Znala	znát	k5eAaImAgFnS	znát
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Charlesovými	Charlesův	k2eAgMnPc7d1	Charlesův
mladšími	mladý	k2eAgMnPc7d2	mladší
bratry	bratr	k1gMnPc7	bratr
Edwardem	Edward	k1gMnSc7	Edward
a	a	k8xC	a
Andrewem	Andrewem	k1gInSc4	Andrewem
si	se	k3xPyFc3	se
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Milostný	milostný	k2eAgInSc1d1	milostný
život	život	k1gInSc1	život
prince	princ	k1gMnSc2	princ
Charlese	Charles	k1gMnSc2	Charles
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
předmětem	předmět	k1gInSc7	předmět
spekulací	spekulace	k1gFnPc2	spekulace
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
jeho	jeho	k3xOp3gFnSc2	jeho
budoucí	budoucí	k2eAgFnSc2d1	budoucí
manželky	manželka	k1gFnSc2	manželka
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
kladen	kladen	k2eAgInSc1d1	kladen
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
nevěsta	nevěsta	k1gFnSc1	nevěsta
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
prastrýc	prastrýc	k1gMnSc1	prastrýc
hrabě	hrabě	k1gMnSc1	hrabě
Louis	Louis	k1gMnSc1	Louis
Mountbatten	Mountbatten	k2eAgMnSc1d1	Mountbatten
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
radu	rada	k1gMnSc4	rada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
nevinnou	vinný	k2eNgFnSc7d1	nevinná
mladou	mladý	k2eAgFnSc7d1	mladá
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
vzhlížet	vzhlížet	k5eAaImF	vzhlížet
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
souhlas	souhlas	k1gInSc4	souhlas
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
očekávalo	očekávat	k5eAaImAgNnS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
nevěsta	nevěsta	k1gFnSc1	nevěsta
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
královský	královský	k2eAgInSc4d1	královský
nebo	nebo	k8xC	nebo
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Diana	Diana	k1gFnSc1	Diana
splňuje	splňovat	k5eAaImIp3nS	splňovat
všechny	všechen	k3xTgFnPc4	všechen
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
ale	ale	k9	ale
nejprve	nejprve	k6eAd1	nejprve
chodil	chodit	k5eAaImAgMnS	chodit
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
sestrou	sestra	k1gFnSc7	sestra
Sarah	Sarah	k1gFnSc7	Sarah
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
budoucí	budoucí	k2eAgInSc4d1	budoucí
královský	královský	k2eAgInSc4d1	královský
pár	pár	k1gInSc4	pár
seznámila	seznámit	k5eAaPmAgFnS	seznámit
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
spolu	spolu	k6eAd1	spolu
začali	začít	k5eAaPmAgMnP	začít
chodit	chodit	k5eAaImF	chodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
a	a	k8xC	a
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
utajován	utajovat	k5eAaImNgInS	utajovat
před	před	k7c7	před
médii	médium	k1gNnPc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
kamery	kamera	k1gFnSc2	kamera
a	a	k8xC	a
foťáky	foťáky	k?	foťáky
zachytily	zachytit	k5eAaPmAgFnP	zachytit
"	"	kIx"	"
<g/>
Shy	Shy	k1gFnSc4	Shy
Di	Di	k1gFnSc2	Di
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
plachou	plachý	k2eAgFnSc4d1	plachá
Dianu	Diana	k1gFnSc4	Diana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
svou	svůj	k3xOyFgFnSc7	svůj
krásou	krása	k1gFnSc7	krása
a	a	k8xC	a
nevinností	nevinnost	k1gFnSc7	nevinnost
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
ji	on	k3xPp3gFnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Zásnubní	zásnubní	k2eAgFnSc1d1	zásnubní
fotografie	fotografie	k1gFnSc1	fotografie
ukazující	ukazující	k2eAgFnSc2d1	ukazující
usmívající	usmívající	k2eAgFnSc2d1	usmívající
se	se	k3xPyFc4	se
pár	pár	k4xCyI	pár
a	a	k8xC	a
Dianu	Diana	k1gFnSc4	Diana
ukazujíc	ukazovat	k5eAaImSgFnS	ukazovat
svůj	svůj	k3xOyFgInSc4	svůj
prsten	prsten	k1gInSc4	prsten
–	–	k?	–
18	[number]	k4	18
<g/>
karátový	karátový	k2eAgInSc4d1	karátový
safír	safír	k1gInSc4	safír
vykládaný	vykládaný	k2eAgInSc4d1	vykládaný
diamanty	diamant	k1gInPc1	diamant
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
velmi	velmi	k6eAd1	velmi
populárními	populární	k2eAgMnPc7d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Svatební	svatební	k2eAgFnSc4d1	svatební
cestu	cesta	k1gFnSc4	cesta
Charles	Charles	k1gMnSc1	Charles
s	s	k7c7	s
Dianou	Diana	k1gFnSc7	Diana
strávili	strávit	k5eAaPmAgMnP	strávit
v	v	k7c4	v
Hampshire	Hampshir	k1gInSc5	Hampshir
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c6	na
královské	královský	k2eAgFnSc6d1	královská
jachtě	jachta	k1gFnSc6	jachta
Britannia	Britannium	k1gNnSc2	Britannium
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
prince	princ	k1gMnSc2	princ
a	a	k8xC	a
princezny	princezna	k1gFnSc2	princezna
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
téměř	téměř	k6eAd1	téměř
veřejným	veřejný	k2eAgNnSc7d1	veřejné
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
pár	pár	k1gInSc1	pár
nenechal	nechat	k5eNaPmAgInS	nechat
veřejnost	veřejnost	k1gFnSc4	veřejnost
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
,	,	kIx,	,
že	že	k8xS	že
čekají	čekat	k5eAaImIp3nP	čekat
nástupce	nástupce	k1gMnSc4	nástupce
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
porodila	porodit	k5eAaPmAgFnS	porodit
Williama	Williama	k1gNnSc4	Williama
Arthura	Arthur	k1gMnSc2	Arthur
Philipa	Philip	k1gMnSc2	Philip
Louise	Louis	k1gMnSc2	Louis
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
než	než	k8xS	než
rok	rok	k1gInSc4	rok
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
uviděl	uvidět	k5eAaPmAgInS	uvidět
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
princ	princ	k1gMnSc1	princ
Henry	Henry	k1gMnSc1	Henry
(	(	kIx(	(
<g/>
Henry	Henry	k1gMnSc1	Henry
Charles	Charles	k1gMnSc1	Charles
Albert	Albert	k1gMnSc1	Albert
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
oddanou	oddaný	k2eAgFnSc7d1	oddaná
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
se	se	k3xPyFc4	se
vystavit	vystavit	k5eAaPmF	vystavit
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
také	také	k9	také
životu	život	k1gInSc2	život
mimo	mimo	k7c4	mimo
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
biologickým	biologický	k2eAgMnSc7d1	biologický
otcem	otec	k1gMnSc7	otec
Harryho	Harry	k1gMnSc2	Harry
je	být	k5eAaImIp3nS	být
major	major	k1gMnSc1	major
James	James	k1gMnSc1	James
Hewitt	Hewitt	k1gMnSc1	Hewitt
<g/>
,	,	kIx,	,
instruktor	instruktor	k1gMnSc1	instruktor
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
milenec	milenec	k1gMnSc1	milenec
lady	lady	k1gFnSc2	lady
Diany	Diana	k1gFnSc2	Diana
<g/>
.	.	kIx.	.
</s>
<s>
Major	major	k1gMnSc1	major
a	a	k8xC	a
Diana	Diana	k1gFnSc1	Diana
se	se	k3xPyFc4	se
poznali	poznat	k5eAaPmAgMnP	poznat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
;	;	kIx,	;
třebaže	třebaže	k8xS	třebaže
Diana	Diana	k1gFnSc1	Diana
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
románek	románek	k1gInSc1	románek
započal	započnout	k5eAaPmAgInS	započnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgInS	začít
již	již	k6eAd1	již
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
seznámení	seznámení	k1gNnSc3	seznámení
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
tak	tak	k9	tak
či	či	k8xC	či
tak	tak	k9	tak
<g/>
,	,	kIx,	,
Hewit	Hewit	k1gMnSc1	Hewit
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
rezavé	rezavý	k2eAgInPc1d1	rezavý
vlasy	vlas	k1gInPc1	vlas
Harryho	Harry	k1gMnSc2	Harry
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc1	jeho
<g/>
;	;	kIx,	;
lidé	člověk	k1gMnPc1	člověk
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
podobni	podoben	k2eAgMnPc1d1	podoben
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
poctivě	poctivě	k6eAd1	poctivě
plnila	plnit	k5eAaImAgFnS	plnit
povinnosti	povinnost	k1gFnPc4	povinnost
královského	královský	k2eAgInSc2d1	královský
života	život	k1gInSc2	život
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
povzbuzovala	povzbuzovat	k5eAaImAgFnS	povzbuzovat
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vypadala	vypadat	k5eAaPmAgFnS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
užívá	užívat	k5eAaImIp3nS	užívat
stříhání	stříhání	k1gNnSc4	stříhání
stužek	stužka	k1gFnPc2	stužka
a	a	k8xC	a
sbírání	sbírání	k1gNnSc4	sbírání
peněz	peníze	k1gInPc2	peníze
pro	pro	k7c4	pro
charitu	charita	k1gFnSc4	charita
<g/>
.	.	kIx.	.
</s>
<s>
Rozdávala	rozdávat	k5eAaImAgFnS	rozdávat
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
zájem	zájem	k1gInSc4	zájem
přímým	přímý	k2eAgInSc7d1	přímý
a	a	k8xC	a
upřímným	upřímný	k2eAgInSc7d1	upřímný
způsobem	způsob	k1gInSc7	způsob
jak	jak	k8xC	jak
chudým	chudý	k2eAgInSc7d1	chudý
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
tak	tak	k9	tak
celým	celý	k2eAgInPc3d1	celý
davům	dav	k1gInPc3	dav
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
a	a	k8xC	a
okouzlující	okouzlující	k2eAgFnSc1d1	okouzlující
si	se	k3xPyFc3	se
užívala	užívat	k5eAaImAgFnS	užívat
moderního	moderní	k2eAgNnSc2d1	moderní
oblékání	oblékání	k1gNnSc2	oblékání
a	a	k8xC	a
nosila	nosit	k5eAaImAgFnS	nosit
nejnovější	nový	k2eAgFnSc4d3	nejnovější
britskou	britský	k2eAgFnSc4d1	britská
módu	móda	k1gFnSc4	móda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
fotografie	fotografia	k1gFnPc1	fotografia
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zobrazovaly	zobrazovat	k5eAaImAgInP	zobrazovat
vzor	vzor	k1gInSc4	vzor
moderního	moderní	k2eAgMnSc2d1	moderní
člena	člen	k1gMnSc2	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
začaly	začít	k5eAaPmAgFnP	začít
mezi	mezi	k7c7	mezi
Dianou	Diana	k1gFnSc7	Diana
a	a	k8xC	a
Charlesem	Charles	k1gMnSc7	Charles
objevovat	objevovat	k5eAaImF	objevovat
problémy	problém	k1gInPc1	problém
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
dostával	dostávat	k5eAaImAgInS	dostávat
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
záliby	záliba	k1gFnPc1	záliba
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Charlesovou	Charlesův	k2eAgFnSc7d1	Charlesova
vášní	vášeň	k1gFnSc7	vášeň
byli	být	k5eAaImAgMnP	být
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
a	a	k8xC	a
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
milovala	milovat	k5eAaImAgFnS	milovat
popovou	popový	k2eAgFnSc4d1	popová
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
přepychové	přepychový	k2eAgNnSc4d1	přepychové
oblečení	oblečení	k1gNnSc4	oblečení
a	a	k8xC	a
drby	drb	k1gInPc4	drb
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
začal	začít	k5eAaPmAgMnS	začít
Charles	Charles	k1gMnSc1	Charles
znovu	znovu	k6eAd1	znovu
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
svou	svůj	k3xOyFgFnSc4	svůj
starou	starý	k2eAgFnSc4d1	stará
lásku	láska	k1gFnSc4	láska
Camillu	Camilla	k1gFnSc4	Camilla
Parker	Parkra	k1gFnPc2	Parkra
Bowlesovou	Bowlesový	k2eAgFnSc4d1	Bowlesová
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Diana	Diana	k1gFnSc1	Diana
trpěla	trpět	k5eAaImAgFnS	trpět
bulimií	bulimie	k1gFnSc7	bulimie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
a	a	k8xC	a
Diana	Diana	k1gFnSc1	Diana
se	se	k3xPyFc4	se
i	i	k9	i
přesto	přesto	k6eAd1	přesto
objevovali	objevovat	k5eAaImAgMnP	objevovat
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
žili	žít	k5eAaImAgMnP	žít
každý	každý	k3xTgInSc4	každý
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Zvěsti	zvěst	k1gFnPc1	zvěst
o	o	k7c6	o
nesouladu	nesoulad	k1gInSc6	nesoulad
páru	pár	k1gInSc2	pár
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
staly	stát	k5eAaPmAgFnP	stát
oficiálními	oficiální	k2eAgInPc7d1	oficiální
<g/>
,	,	kIx,	,
když	když	k8xS	když
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
John	John	k1gMnSc1	John
Major	major	k1gMnSc1	major
oznámil	oznámit	k5eAaPmAgMnS	oznámit
parlamentu	parlament	k1gInSc3	parlament
<g/>
,	,	kIx,	,
že	že	k8xS	že
Charles	Charles	k1gMnSc1	Charles
a	a	k8xC	a
Diana	Diana	k1gFnSc1	Diana
žijí	žít	k5eAaImIp3nP	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
řízení	řízení	k1gNnSc6	řízení
byli	být	k5eAaImAgMnP	být
rozvedeni	rozveden	k2eAgMnPc1d1	rozveden
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
však	však	k9	však
Diana	Diana	k1gFnSc1	Diana
oficiálně	oficiálně	k6eAd1	oficiálně
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
členkou	členka	k1gFnSc7	členka
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
jako	jako	k8xS	jako
matka	matka	k1gFnSc1	matka
budoucího	budoucí	k2eAgMnSc2d1	budoucí
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zasnoubení	zasnoubení	k1gNnSc2	zasnoubení
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Charlesem	Charles	k1gMnSc7	Charles
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
automobilové	automobilový	k2eAgFnSc2d1	automobilová
havárie	havárie	k1gFnSc2	havárie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
Diana	Diana	k1gFnSc1	Diana
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
žen	žena	k1gFnPc2	žena
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
nejvíce	hodně	k6eAd3	hodně
fotografovanou	fotografovaný	k2eAgFnSc4d1	fotografovaná
osobu	osoba	k1gFnSc4	osoba
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
Diana	Diana	k1gFnSc1	Diana
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
svého	svůj	k3xOyFgNnSc2	svůj
nového	nový	k2eAgNnSc2d1	nové
veřejného	veřejný	k2eAgNnSc2d1	veřejné
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
nášlapným	nášlapný	k2eAgFnPc3d1	nášlapná
minám	mina	k1gFnPc3	mina
<g/>
,	,	kIx,	,
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Bosnu	Bosna	k1gFnSc4	Bosna
a	a	k8xC	a
Angolu	Angola	k1gFnSc4	Angola
a	a	k8xC	a
také	také	k9	také
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
lidem	člověk	k1gMnPc3	člověk
postiženým	postižený	k1gMnSc7	postižený
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
v	v	k7c6	v
charitách	charita	k1gFnPc6	charita
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Diana	Diana	k1gFnSc1	Diana
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
utěšovala	utěšovat	k5eAaImAgFnS	utěšovat
nemocné	nemocná	k1gFnSc2	nemocná
a	a	k8xC	a
převzala	převzít	k5eAaPmAgFnS	převzít
ochranu	ochrana	k1gFnSc4	ochrana
nad	nad	k7c7	nad
několika	několik	k4yIc7	několik
charitativními	charitativní	k2eAgFnPc7d1	charitativní
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
také	také	k9	také
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dražbu	dražba	k1gFnSc4	dražba
79	[number]	k4	79
oděvů	oděv	k1gInPc2	oděv
posbíraných	posbíraný	k2eAgInPc2d1	posbíraný
za	za	k7c2	za
celých	celý	k2eAgNnPc2d1	celé
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prožila	prožít	k5eAaPmAgFnS	prožít
po	po	k7c6	po
boku	bok	k1gInSc6	bok
prince	princ	k1gMnSc2	princ
Charlese	Charles	k1gMnSc2	Charles
<g/>
;	;	kIx,	;
veškerý	veškerý	k3xTgInSc4	veškerý
výtěžek	výtěžek	k1gInSc4	výtěžek
pak	pak	k6eAd1	pak
věnovala	věnovat	k5eAaPmAgFnS	věnovat
charitě	charita	k1gFnSc3	charita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Angolu	Angola	k1gFnSc4	Angola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužila	sloužit	k5eAaImAgFnS	sloužit
u	u	k7c2	u
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
jako	jako	k8xS	jako
VIP	VIP	kA	VIP
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Šla	jít	k5eAaImAgFnS	jít
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
výbuch	výbuch	k1gInSc4	výbuch
nášlapné	nášlapný	k2eAgFnSc2d1	nášlapná
miny	mina	k1gFnSc2	mina
<g/>
,	,	kIx,	,
účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
projektu	projekt	k1gInSc3	projekt
"	"	kIx"	"
<g/>
uvědomění	uvědomění	k1gNnPc1	uvědomění
si	se	k3xPyFc3	se
nebezpečnosti	nebezpečnost	k1gFnSc2	nebezpečnost
min	mina	k1gFnPc2	mina
okolo	okolo	k7c2	okolo
domovů	domov	k1gInPc2	domov
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1997	[number]	k4	1997
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
celým	celý	k2eAgInSc7d1	celý
světem	svět	k1gInSc7	svět
proletěla	proletět	k5eAaPmAgFnS	proletět
fotografie	fotografie	k1gFnSc1	fotografie
princezny	princezna	k1gFnSc2	princezna
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
bundě	bunda	k1gFnSc6	bunda
a	a	k8xC	a
helmě	helma	k1gFnSc6	helma
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgFnSc6d1	procházející
se	se	k3xPyFc4	se
po	po	k7c6	po
minovém	minový	k2eAgNnSc6d1	minové
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
zájem	zájem	k1gInSc1	zájem
o	o	k7c6	o
miny	mina	k1gFnPc1	mina
byl	být	k5eAaImAgInS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
zranění	zranění	k1gNnSc4	zranění
jimi	on	k3xPp3gMnPc7	on
způsobená	způsobený	k2eAgFnSc1d1	způsobená
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejvíce	hodně	k6eAd3	hodně
na	na	k7c6	na
dětech	dítě	k1gFnPc6	dítě
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
stále	stále	k6eAd1	stále
přibývají	přibývat	k5eAaImIp3nP	přibývat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
válka	válka	k1gFnSc1	válka
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Princeznina	princeznin	k2eAgFnSc1d1	princeznina
snaha	snaha	k1gFnSc1	snaha
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
Organizaci	organizace	k1gFnSc3	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vehementně	vehementně	k6eAd1	vehementně
apelovala	apelovat	k5eAaImAgFnS	apelovat
na	na	k7c4	na
národy	národ	k1gInPc4	národ
jako	jako	k8xC	jako
producenty	producent	k1gMnPc4	producent
velkého	velký	k2eAgInSc2d1	velký
množství	množství	k1gNnSc1	množství
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
i	i	k8xC	i
USA	USA	kA	USA
avšak	avšak	k8xC	avšak
nepodepsaly	podepsat	k5eNaPmAgInP	podepsat
Ottawskou	ottawský	k2eAgFnSc4d1	Ottawská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
užití	užití	k1gNnSc4	užití
protipěchotních	protipěchotní	k2eAgFnPc2d1	protipěchotní
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Další	další	k2eAgFnSc7d1	další
fotkou	fotka	k1gFnSc7	fotka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obletěla	obletět	k5eAaPmAgFnS	obletět
svět	svět	k1gInSc4	svět
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Lady	lady	k1gFnSc1	lady
Diana	Diana	k1gFnSc1	Diana
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
posteli	postel	k1gFnSc6	postel
člověka	člověk	k1gMnSc2	člověk
nakaženého	nakažený	k2eAgInSc2d1	nakažený
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
a	a	k8xC	a
hladící	hladící	k2eAgFnPc1d1	hladící
mu	on	k3xPp3gMnSc3	on
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Chtěla	chtít	k5eAaImAgFnS	chtít
tak	tak	k6eAd1	tak
ukázat	ukázat	k5eAaPmF	ukázat
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
nezaslouží	zasloužit	k5eNaPmIp3nP	zasloužit
žádnou	žádný	k3yNgFnSc4	žádný
izolaci	izolace	k1gFnSc4	izolace
a	a	k8xC	a
opovržení	opovržení	k1gNnSc4	opovržení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soucit	soucit	k1gInSc1	soucit
a	a	k8xC	a
vlídnost	vlídnost	k1gFnSc1	vlídnost
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
tak	tak	k6eAd1	tak
dala	dát	k5eAaPmAgFnS	dát
trpícím	trpící	k2eAgInPc3d1	trpící
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
lepší	dobrý	k2eAgInSc4d2	lepší
život	život	k1gInSc4	život
i	i	k9	i
s	s	k7c7	s
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
změnit	změnit	k5eAaPmF	změnit
názor	názor	k1gInSc4	názor
a	a	k8xC	a
pohled	pohled	k1gInSc4	pohled
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
podnikala	podnikat	k5eAaImAgFnS	podnikat
také	také	k9	také
tajné	tajný	k2eAgFnPc4d1	tajná
návštěvy	návštěva	k1gFnPc4	návštěva
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
u	u	k7c2	u
smrtelně	smrtelně	k6eAd1	smrtelně
nakažených	nakažený	k2eAgMnPc2d1	nakažený
pacientů	pacient	k1gMnPc2	pacient
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výlety	výlet	k1gInPc1	výlet
byly	být	k5eAaImAgInP	být
médiím	médium	k1gNnPc3	médium
skryty	skryt	k2eAgMnPc4d1	skryt
<g/>
.	.	kIx.	.
</s>
<s>
Pozdě	pozdě	k6eAd1	pozdě
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
opustila	opustit	k5eAaPmAgFnS	opustit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
milencem	milenec	k1gMnSc7	milenec
Dodim	Dodim	k1gMnSc1	Dodim
Al-Fayedem	Al-Fayed	k1gMnSc7	Al-Fayed
hotel	hotel	k1gInSc1	hotel
Ritz	Ritz	k1gInSc1	Ritz
v	v	k7c6	v
Palace	Palace	k1gFnPc1	Palace
Vendome	Vendom	k1gInSc5	Vendom
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
uniknout	uniknout	k5eAaPmF	uniknout
devíti	devět	k4xCc3	devět
francouzským	francouzský	k2eAgMnPc3d1	francouzský
novinářům	novinář	k1gMnPc3	novinář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ji	on	k3xPp3gFnSc4	on
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vjelo	vjet	k5eAaPmAgNnS	vjet
auto	auto	k1gNnSc1	auto
do	do	k7c2	do
tunelu	tunel	k1gInSc2	tunel
poblíž	poblíž	k6eAd1	poblíž
Place	plac	k1gInSc6	plac
de	de	k?	de
l	l	kA	l
<g/>
́	́	k?	́
<g/>
Alma	alma	k1gFnSc1	alma
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
opilý	opilý	k2eAgMnSc1d1	opilý
řidič	řidič	k1gMnSc1	řidič
Henri	Henri	k1gNnSc2	Henri
Paul	Paul	k1gMnSc1	Paul
jel	jet	k5eAaImAgMnS	jet
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
bulvárním	bulvární	k2eAgInSc6d1	bulvární
novinářům	novinář	k1gMnPc3	novinář
rychlostí	rychlost	k1gFnSc7	rychlost
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
překročil	překročit	k5eAaPmAgMnS	překročit
povolenou	povolený	k2eAgFnSc4d1	povolená
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Automobil	automobil	k1gInSc4	automobil
značky	značka	k1gFnSc2	značka
Mercedes	mercedes	k1gInSc1	mercedes
(	(	kIx(	(
<g/>
Třída	třída	k1gFnSc1	třída
S	s	k7c7	s
<g/>
,	,	kIx,	,
model	model	k1gInSc1	model
w	w	k?	w
<g/>
140	[number]	k4	140
<g/>
)	)	kIx)	)
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
odrazil	odrazit	k5eAaPmAgMnS	odrazit
se	se	k3xPyFc4	se
a	a	k8xC	a
čelně	čelně	k6eAd1	čelně
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
třináctého	třináctý	k4xOgInSc2	třináctý
z	z	k7c2	z
pilířů	pilíř	k1gInPc2	pilíř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podpíral	podpírat	k5eAaImAgInS	podpírat
střechu	střecha	k1gFnSc4	střecha
tunelu	tunel	k1gInSc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Dodi	Dodi	k1gNnSc7	Dodi
al	ala	k1gFnPc2	ala
Fayed	Fayed	k1gInSc4	Fayed
a	a	k8xC	a
Henri	Henre	k1gFnSc4	Henre
Paul	Paula	k1gFnPc2	Paula
byli	být	k5eAaImAgMnP	být
mrtví	mrtvý	k1gMnPc1	mrtvý
hned	hned	k6eAd1	hned
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Diana	Diana	k1gFnSc1	Diana
ležela	ležet	k5eAaImAgFnS	ležet
uvězněná	uvězněný	k2eAgFnSc1d1	uvězněná
pod	pod	k7c7	pod
troskami	troska	k1gFnPc7	troska
auta	auto	k1gNnSc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
ji	on	k3xPp3gFnSc4	on
záchranáři	záchranář	k1gMnPc1	záchranář
stabilizovali	stabilizovat	k5eAaBmAgMnP	stabilizovat
a	a	k8xC	a
odvezli	odvézt	k5eAaPmAgMnP	odvézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnPc1	její
zranění	zranění	k1gNnPc1	zranění
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
srdce	srdce	k1gNnSc1	srdce
bylo	být	k5eAaImAgNnS	být
přemístěno	přemístit	k5eAaPmNgNnS	přemístit
z	z	k7c2	z
levé	levá	k1gFnSc2	levá
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
stranu	strana	k1gFnSc4	strana
hrudníku	hrudník	k1gInSc2	hrudník
s	s	k7c7	s
rozřízlou	rozřízlý	k2eAgFnSc7d1	rozřízlá
plicní	plicní	k2eAgFnSc7d1	plicní
tepnou	tepna	k1gFnSc7	tepna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
operaci	operace	k1gFnSc4	operace
museli	muset	k5eAaImAgMnP	muset
lékaři	lékař	k1gMnPc1	lékař
nakonec	nakonec	k6eAd1	nakonec
konstatovat	konstatovat	k5eAaBmF	konstatovat
Dianinu	Dianin	k2eAgFnSc4d1	Dianina
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Lionel	Lionel	k1gMnSc1	Lionel
Jospin	Jospin	k1gMnSc1	Jospin
(	(	kIx(	(
<g/>
francouzský	francouzský	k2eAgMnSc1d1	francouzský
premiér	premiér	k1gMnSc1	premiér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bernadette	Bernadett	k1gInSc5	Bernadett
Chirac	Chirac	k1gMnSc1	Chirac
(	(	kIx(	(
<g/>
manželka	manželka	k1gFnSc1	manželka
prezidenta	prezident	k1gMnSc2	prezident
Jacquese	Jacques	k1gMnSc2	Jacques
Chiraca	Chiracus	k1gMnSc2	Chiracus
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bernard	Bernard	k1gMnSc1	Bernard
Kouchner	Kouchner	k1gMnSc1	Kouchner
přišli	přijít	k5eAaPmAgMnP	přijít
vzdát	vzdát	k5eAaPmF	vzdát
princezně	princezna	k1gFnSc3	princezna
poslední	poslední	k2eAgFnSc4d1	poslední
poklonu	poklona	k1gFnSc4	poklona
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
tělo	tělo	k1gNnSc4	tělo
převezl	převézt	k5eAaPmAgMnS	převézt
princ	princ	k1gMnSc1	princ
Charles	Charles	k1gMnSc1	Charles
a	a	k8xC	a
Dianiny	Dianin	k2eAgFnPc4d1	Dianina
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Dianina	Dianin	k2eAgFnSc1d1	Dianina
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
provázena	provázen	k2eAgFnSc1d1	provázena
neobyčejně	obyčejně	k6eNd1	obyčejně
hlubokým	hluboký	k2eAgInSc7d1	hluboký
a	a	k8xC	a
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
smutkem	smutek	k1gInSc7	smutek
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gInSc4	její
pohřeb	pohřeb	k1gInSc4	pohřeb
přišly	přijít	k5eAaPmAgInP	přijít
truchlit	truchlit	k5eAaImF	truchlit
tři	tři	k4xCgNnPc4	tři
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
poslední	poslední	k2eAgNnSc1d1	poslední
rozloučení	rozloučení	k1gNnSc1	rozloučení
sledovalo	sledovat	k5eAaImAgNnS	sledovat
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
diváků	divák	k1gMnPc2	divák
ve	v	k7c6	v
187	[number]	k4	187
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc1	milion
kytic	kytice	k1gFnPc2	kytice
bylo	být	k5eAaImAgNnS	být
přineseno	přinést	k5eAaPmNgNnS	přinést
a	a	k8xC	a
položeno	položit	k5eAaPmNgNnS	položit
před	před	k7c4	před
práh	práh	k1gInSc4	práh
domu	dům	k1gInSc2	dům
Jana	Jan	k1gMnSc2	Jan
Maulera	Mauler	k1gMnSc2	Mauler
<g/>
.	.	kIx.	.
</s>
<s>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
také	také	k6eAd1	také
přetextoval	přetextovat	k5eAaImAgMnS	přetextovat
svou	svůj	k3xOyFgFnSc4	svůj
baladu	balada	k1gFnSc4	balada
"	"	kIx"	"
<g/>
Svíce	svíce	k1gFnSc1	svíce
ve	v	k7c6	v
větru	vítr	k1gInSc6	vítr
<g/>
"	"	kIx"	"
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
ji	on	k3xPp3gFnSc4	on
zesnulé	zesnulý	k2eAgNnSc1d1	zesnulé
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vjezdem	vjezd	k1gInSc7	vjezd
do	do	k7c2	do
tunelu	tunel	k1gInSc2	tunel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Diana	Diana	k1gFnSc1	Diana
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
památník	památník	k1gInSc1	památník
Plamen	plamen	k1gInSc1	plamen
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Flame	Flam	k1gMnSc5	Flam
of	of	k?	of
Liberty	Libert	k1gMnPc7	Libert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
je	být	k5eAaImIp3nS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
v	v	k7c4	v
Althorp	Althorp	k1gInSc4	Althorp
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domově	domov	k1gInSc6	domov
v	v	k7c6	v
Northamptonshire	Northamptonshir	k1gInSc5	Northamptonshir
<g/>
,	,	kIx,	,
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
uprostřed	uprostřed	k7c2	uprostřed
jezera	jezero	k1gNnSc2	jezero
známém	známý	k2eAgNnSc6d1	známé
jako	jako	k8xS	jako
The	The	k1gMnSc1	The
Oval	ovalit	k5eAaPmRp2nS	ovalit
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
jezírku	jezírko	k1gNnSc3	jezírko
lemuje	lemovat	k5eAaImIp3nS	lemovat
36	[number]	k4	36
dubů	dub	k1gInPc2	dub
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejího	její	k3xOp3gInSc2	její
věku	věk	k1gInSc2	věk
v	v	k7c6	v
době	doba	k1gFnSc6	doba
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc4	místo
stráží	strážit	k5eAaImIp3nP	strážit
čtyři	čtyři	k4xCgFnPc1	čtyři
černé	černý	k2eAgFnPc1d1	černá
labutě	labuť	k1gFnPc1	labuť
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k6eAd1	blízko
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
starobylé	starobylý	k2eAgNnSc4d1	starobylé
arboretum	arboretum	k1gNnSc4	arboretum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
sluncem	slunce	k1gNnSc7	slunce
stromy	strom	k1gInPc1	strom
zasazené	zasazený	k2eAgInPc1d1	zasazený
princem	princ	k1gMnSc7	princ
Williamem	William	k1gInSc7	William
<g/>
,	,	kIx,	,
princem	princ	k1gMnSc7	princ
Harrym	Harrym	k1gInSc4	Harrym
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
rodiny	rodina	k1gFnSc2	rodina
i	i	k8xC	i
princeznou	princezna	k1gFnSc7	princezna
samotnou	samotný	k2eAgFnSc7d1	samotná
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
:	:	kIx,	:
Diana	Diana	k1gFnSc1	Diana
Frances	Frances	k1gMnSc1	Frances
Spencer	Spencer	k1gMnSc1	Spencer
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
:	:	kIx,	:
Lady	lady	k1gFnSc4	lady
Diana	Diana	k1gFnSc1	Diana
Frances	Frances	k1gMnSc1	Frances
Spencer	Spencer	k1gMnSc1	Spencer
<g />
.	.	kIx.	.
</s>
<s>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Její	její	k3xOp3gFnSc1	její
královská	královský	k2eAgFnSc1d1	královská
Výsost	výsost	k1gFnSc1	výsost
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
Ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
svatbě	svatba	k1gFnSc6	svatba
i	i	k8xC	i
rozvodu	rozvod	k1gInSc6	rozvod
nejvíce	hodně	k6eAd3	hodně
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
Princezna	princezna	k1gFnSc1	princezna
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
takto	takto	k6eAd1	takto
oslovována	oslovován	k2eAgFnSc1d1	oslovována
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pouze	pouze	k6eAd1	pouze
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
princezny	princezna	k1gFnPc1	princezna
narodily	narodit	k5eAaPmAgFnP	narodit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
mít	mít	k5eAaImF	mít
před	před	k7c7	před
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Princezna	princezna	k1gFnSc1	princezna
Anne	Anne	k1gFnSc1	Anne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
ji	on	k3xPp3gFnSc4	on
náleželo	náležet	k5eAaImAgNnS	náležet
oslovení	oslovení	k1gNnSc1	oslovení
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
;	;	kIx,	;
v	v	k7c6	v
době	doba	k1gFnSc6	doba
manželství	manželství	k1gNnSc2	manželství
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
titulována	titulován	k2eAgFnSc1d1	titulována
jako	jako	k8xC	jako
Její	její	k3xOp3gFnSc1	její
královská	královský	k2eAgFnSc1d1	královská
výsost	výsost	k1gFnSc1	výsost
Princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Chesteru	Chester	k1gInSc2	Chester
<g/>
,	,	kIx,	,
Vévodkyně	vévodkyně	k1gFnPc1	vévodkyně
z	z	k7c2	z
Cornwallu	Cornwall	k1gInSc2	Cornwall
<g/>
,	,	kIx,	,
Vévodkyně	vévodkyně	k1gFnPc1	vévodkyně
z	z	k7c2	z
Rothesay	Rothesaa	k1gFnSc2	Rothesaa
<g/>
,	,	kIx,	,
Hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Carricku	Carrick	k1gInSc2	Carrick
<g/>
,	,	kIx,	,
Baronka	baronka	k1gFnSc1	baronka
z	z	k7c2	z
Renfrew	Renfrew	k1gFnSc2	Renfrew
<g/>
,	,	kIx,	,
Paní	paní	k1gFnSc1	paní
Ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
Skotská	skotská	k1gFnSc1	skotská
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
obdoba	obdoba	k1gFnSc1	obdoba
titulů	titul	k1gInPc2	titul
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
