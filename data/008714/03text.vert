<p>
<s>
TJ	tj	kA	tj
Sokol	Sokol	k1gMnSc1	Sokol
Solopisky	Solopiska	k1gFnSc2	Solopiska
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gInSc4	Sokol
Solopisky	Solopiska	k1gFnSc2	Solopiska
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Třebotov	Třebotov	k1gInSc1	Třebotov
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
Středočeské	středočeský	k2eAgFnSc6d1	Středočeská
krajské	krajský	k2eAgFnSc6d1	krajská
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
páté	pátý	k4xOgFnSc6	pátý
české	český	k2eAgFnSc6d1	Česká
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1	klubová
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Černošicích	Černošice	k1gFnPc6	Černošice
na	na	k7c6	na
tamějším	tamější	k2eAgInSc6d1	tamější
zimním	zimní	k2eAgInSc6d1	zimní
stadionu	stadion	k1gInSc6	stadion
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
300	[number]	k4	300
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
ligové	ligový	k2eAgFnSc2d1	ligová
účasti	účast	k1gFnSc2	účast
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgInSc1d1	okresní
přebor	přebor	k1gInSc1	přebor
-	-	kIx~	-
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Středočeský	středočeský	k2eAgInSc1d1	středočeský
meziokresní	meziokresní	k2eAgInSc1d1	meziokresní
přebor	přebor	k1gInSc1	přebor
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Středočeský	středočeský	k2eAgInSc1d1	středočeský
meziokresní	meziokresní	k2eAgInSc1d1	meziokresní
přebor	přebor	k1gInSc1	přebor
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
Středočeský	středočeský	k2eAgInSc1d1	středočeský
meziokresní	meziokresní	k2eAgInSc1d1	meziokresní
přebor	přebor	k1gInSc1	přebor
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
–	–	k?	–
:	:	kIx,	:
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
krajská	krajský	k2eAgFnSc1d1	krajská
soutěž	soutěž	k1gFnSc1	soutěž
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
ZČ	ZČ	kA	ZČ
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
klubu	klub	k1gInSc2	klub
na	na	k7c4	na
vysledky	vysledek	k1gInPc4	vysledek
<g/>
.	.	kIx.	.
<g/>
lidovky	lidovky	k1gFnPc4	lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
