<s>
Blizard	blizard	k1gInSc1	blizard
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
suchý	suchý	k2eAgInSc4d1	suchý
vítr	vítr	k1gInSc4	vítr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
velmi	velmi	k6eAd1	velmi
studený	studený	k2eAgMnSc1d1	studený
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
doprovázen	doprovázen	k2eAgInSc1d1	doprovázen
silným	silný	k2eAgNnSc7d1	silné
sněžením	sněžení	k1gNnSc7	sněžení
<g/>
,	,	kIx,	,
či	či	k8xC	či
poryvy	poryv	k1gInPc1	poryv
větru	vítr	k1gInSc2	vítr
zvedají	zvedat	k5eAaImIp3nP	zvedat
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
masu	masa	k1gFnSc4	masa
a	a	k8xC	a
tlačí	tlačit	k5eAaImIp3nS	tlačit
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
přes	přes	k7c4	přes
15	[number]	k4	15
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Teplota	teplota	k1gFnSc1	teplota
při	při	k7c6	při
blizardu	blizard	k1gInSc6	blizard
klesá	klesat	k5eAaImIp3nS	klesat
často	často	k6eAd1	často
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Vítr	vítr	k1gInSc1	vítr
vane	vanout	k5eAaImIp3nS	vanout
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
při	při	k7c6	při
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
proudění	proudění	k1gNnSc6	proudění
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
cyklóny	cyklóna	k1gFnSc2	cyklóna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
blizardu	blizard	k1gInSc6	blizard
napadne	napadnout	k5eAaPmIp3nS	napadnout
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
až	až	k9	až
5	[number]	k4	5
nebo	nebo	k8xC	nebo
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
nového	nový	k2eAgInSc2d1	nový
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
nějaká	nějaký	k3yIgFnSc1	nějaký
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
blizard	blizard	k1gInSc1	blizard
zavát	zavát	k5eAaPmF	zavát
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
vesnici	vesnice	k1gFnSc4	vesnice
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
blizard	blizard	k1gInSc1	blizard
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
blizard	blizard	k1gInSc1	blizard
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
