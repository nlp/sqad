<s>
Jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
při	při	k7c6	při
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
proudění	proudění	k1gNnSc6	proudění
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
cyklóny	cyklóna	k1gFnSc2	cyklóna
<g/>
.	.	kIx.	.
</s>
