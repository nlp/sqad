<p>
<s>
Policejní	policejní	k2eAgFnSc1d1	policejní
psychologie	psychologie	k1gFnSc1	psychologie
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
aplikovaných	aplikovaný	k2eAgFnPc2d1	aplikovaná
disciplín	disciplína	k1gFnPc2	disciplína
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
odvětvím	odvětví	k1gNnSc7	odvětví
forenzní	forenzní	k2eAgFnSc2d1	forenzní
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnPc1	práce
==	==	k?	==
</s>
</p>
<p>
<s>
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
profil	profil	k1gInSc1	profil
pachatele	pachatel	k1gMnSc2	pachatel
</s>
</p>
<p>
<s>
spolupráce	spolupráce	k1gFnSc1	spolupráce
u	u	k7c2	u
policejního	policejní	k2eAgInSc2d1	policejní
výslechu	výslech	k1gInSc2	výslech
</s>
</p>
<p>
<s>
přítomný	přítomný	k2eAgMnSc1d1	přítomný
u	u	k7c2	u
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pachateli	pachatel	k1gMnPc7	pachatel
mladiství	mladiství	k1gNnSc2	mladiství
</s>
</p>
<p>
<s>
pomoc	pomoc	k1gFnSc1	pomoc
policistům	policista	k1gMnPc3	policista
-	-	kIx~	-
(	(	kIx(	(
<g/>
úmrtí	úmrtí	k1gNnSc1	úmrtí
kolegy	kolega	k1gMnSc2	kolega
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
krizové	krizový	k2eAgFnSc2d1	krizová
situace	situace	k1gFnSc2	situace
-	-	kIx~	-
hromadné	hromadný	k2eAgNnSc1d1	hromadné
neštěstí	neštěstí	k1gNnSc1	neštěstí
</s>
</p>
<p>
<s>
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
-	-	kIx~	-
u	u	k7c2	u
rukojmích	rukojmí	k1gMnPc2	rukojmí
<g/>
,	,	kIx,	,
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
detekce	detekce	k1gFnSc1	detekce
podvodu	podvod	k1gInSc2	podvod
-	-	kIx~	-
detekce	detekce	k1gFnSc1	detekce
lži	lež	k1gFnSc2	lež
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČÍRTKOVÁ	ČÍRTKOVÁ	kA	ČÍRTKOVÁ
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgFnSc1d1	policejní
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Aleš	Aleš	k1gMnSc1	Aleš
Čeněk	Čeněk	k1gMnSc1	Čeněk
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o	o	k7c4	o
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86898-73-3	[number]	k4	80-86898-73-3
</s>
</p>
