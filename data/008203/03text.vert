<p>
<s>
Brvnište	Brvnišit	k5eAaImRp2nP	Brvnišit
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Boronás	Boronás	k1gInSc1	Boronás
<g/>
,	,	kIx,	,
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1899	[number]	k4	1899
Brnovistye	Brnovistye	k1gNnSc2	Brnovistye
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Považská	Považský	k2eAgFnSc1d1	Považská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
189	[number]	k4	189
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Papradnianky	Papradnianka	k1gFnSc2	Papradnianka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
obec	obec	k1gFnSc1	obec
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
vesnicí	vesnice	k1gFnSc7	vesnice
Papradno	Papradno	k1gNnSc1	Papradno
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
Stupné	Stupný	k2eAgFnPc1d1	Stupná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
obce	obec	k1gFnPc1	obec
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Brvniště	Brvniště	k1gNnSc2	Brvniště
usadili	usadit	k5eAaPmAgMnP	usadit
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Archeologové	archeolog	k1gMnPc1	archeolog
zde	zde	k6eAd1	zde
také	také	k9	také
nalezli	naleznout	k5eAaPmAgMnP	naleznout
poklad	poklad	k1gInSc4	poklad
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Brvništi	Brvniště	k1gNnSc6	Brvniště
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
především	především	k9	především
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
i	i	k9	i
mlýny	mlýn	k1gInPc4	mlýn
a	a	k8xC	a
pily	pila	k1gFnPc4	pila
na	na	k7c4	na
vodní	vodní	k2eAgInSc4d1	vodní
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dominanty	dominanta	k1gFnSc2	dominanta
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1	nejsvětější
Srdce	srdce	k1gNnSc2	srdce
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
-	-	kIx~	-
vybudován	vybudován	k2eAgMnSc1d1	vybudován
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
posvětil	posvětit	k5eAaPmAgMnS	posvětit
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
škola	škola	k1gFnSc1	škola
-	-	kIx~	-
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
jsou	být	k5eAaImIp3nP	být
vystavovány	vystavován	k2eAgInPc4d1	vystavován
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaImNgInP	využívat
prostými	prostý	k2eAgInPc7d1	prostý
obyvateli	obyvatel	k1gMnPc7	obyvatel
Brvnište	Brvnište	k1gMnSc1	Brvnište
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
kaple	kaple	k1gFnSc1	kaple
se	s	k7c7	s
zvonicí	zvonice	k1gFnSc7	zvonice
-	-	kIx~	-
vybudována	vybudován	k2eAgFnSc1d1	vybudována
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Brvnište	Brvnište	k1gFnSc2	Brvnište
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
obce	obec	k1gFnSc2	obec
</s>
</p>
