<s>
Hroch	Hroch	k1gMnSc1	Hroch
obojživelný	obojživelný	k2eAgMnSc1d1	obojživelný
(	(	kIx(	(
<g/>
Hippopotamus	Hippopotamus	k1gMnSc1	Hippopotamus
amphibius	amphibius	k1gMnSc1	amphibius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hrochovitých	hrochovitý	k2eAgInPc2d1	hrochovitý
<g/>
,	,	kIx,	,
adaptovaný	adaptovaný	k2eAgInSc1d1	adaptovaný
k	k	k7c3	k
životu	život	k1gInSc3	život
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
příbuzným	příbuzný	k1gMnSc7	příbuzný
je	být	k5eAaImIp3nS	být
hrošík	hrošík	k1gMnSc1	hrošík
liberijský	liberijský	k2eAgMnSc1d1	liberijský
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byli	být	k5eAaImAgMnP	být
hroši	hroch	k1gMnPc1	hroch
pokládání	pokládání	k1gNnSc2	pokládání
za	za	k7c4	za
příbuzné	příbuzná	k1gFnPc4	příbuzná
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nedávných	dávný	k2eNgInPc2d1	nedávný
výzkumů	výzkum	k1gInPc2	výzkum
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
mnohem	mnohem	k6eAd1	mnohem
bližší	blízký	k2eAgInPc1d2	bližší
příbuzenské	příbuzenský	k2eAgInPc1d1	příbuzenský
vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
předkům	předek	k1gInPc3	předek
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
polština	polština	k1gFnSc1	polština
<g/>
,	,	kIx,	,
bulharština	bulharština	k1gFnSc1	bulharština
<g/>
,	,	kIx,	,
románské	románský	k2eAgInPc1d1	románský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hrocha	hroch	k1gMnSc4	hroch
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
starořeckého	starořecký	k2eAgNnSc2d1	starořecké
jména	jméno	k1gNnSc2	jméno
hippopotamos	hippopotamosa	k1gFnPc2	hippopotamosa
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
říční	říční	k2eAgMnSc1d1	říční
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
slovního	slovní	k2eAgNnSc2d1	slovní
spojení	spojení	k1gNnSc2	spojení
"	"	kIx"	"
<g/>
říční	říční	k2eAgMnSc1d1	říční
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
skandinávské	skandinávský	k2eAgInPc1d1	skandinávský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
arabština	arabština	k1gFnSc1	arabština
<g/>
)	)	kIx)	)
či	či	k8xC	či
nilský	nilský	k2eAgMnSc1d1	nilský
kůň	kůň	k1gMnSc1	kůň
(	(	kIx(	(
<g/>
srbština	srbština	k1gFnSc1	srbština
<g/>
,	,	kIx,	,
chorvatština	chorvatština	k1gFnSc1	chorvatština
<g/>
,	,	kIx,	,
maďarština	maďarština	k1gFnSc1	maďarština
<g/>
,	,	kIx,	,
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
a	a	k8xC	a
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
používají	používat	k5eAaImIp3nP	používat
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
biblického	biblický	k2eAgMnSc2d1	biblický
netvora	netvor	k1gMnSc2	netvor
Behemota	behemot	k1gMnSc2	behemot
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
hroch	hroch	k1gMnSc1	hroch
má	mít	k5eAaImIp3nS	mít
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
přejat	přejmout	k5eAaPmNgInS	přejmout
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Etymologie	etymologie	k1gFnSc1	etymologie
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
staročeským	staročeský	k2eAgNnSc7d1	staročeské
slovem	slovo	k1gNnSc7	slovo
hrochot	hrochot	k1gInSc4	hrochot
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
rachot	rachot	k1gInSc1	rachot
<g/>
,	,	kIx,	,
rámus	rámus	k1gInSc1	rámus
<g/>
,	,	kIx,	,
řev	řev	k1gInSc1	řev
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
hroch	hroch	k1gMnSc1	hroch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
doloženo	doložit	k5eAaPmNgNnS	doložit
již	již	k6eAd1	již
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
jako	jako	k8xC	jako
vlastní	vlastní	k2eAgNnSc4d1	vlastní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	s	k7c7	s
zvířetem	zvíře	k1gNnSc7	zvíře
-	-	kIx~	-
hrochem	hroch	k1gMnSc7	hroch
nesouvisí	souviset	k5eNaImIp3nS	souviset
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
starou	starý	k2eAgFnSc4d1	stará
českou	český	k2eAgFnSc4d1	Česká
variantu	varianta	k1gFnSc4	varianta
jména	jméno	k1gNnSc2	jméno
Roch	roch	k0	roch
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Rochus	Rochus	k1gInSc1	Rochus
<g/>
,	,	kIx,	,
it.	it.	k?	it.
Rocco	Rocco	k1gMnSc1	Rocco
<g/>
)	)	kIx)	)
a	a	k8xC	a
domácí	domácí	k2eAgFnSc4d1	domácí
variantu	varianta	k1gFnSc4	varianta
jména	jméno	k1gNnSc2	jméno
Hroznata	Hroznata	k1gFnSc1	Hroznata
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
v	v	k7c6	v
příjmeních	příjmení	k1gNnPc6	příjmení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
historik	historik	k1gMnSc1	historik
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hroch	Hroch	k1gMnSc1	Hroch
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
názvech	název	k1gInPc6	název
(	(	kIx(	(
<g/>
Hrochův	Hrochův	k2eAgInSc1d1	Hrochův
Týnec	Týnec	k1gInSc1	Týnec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hrocha-zvíře	hrochavíř	k1gMnSc4	hrocha-zvíř
používalo	používat	k5eAaImAgNnS	používat
slovo	slovo	k1gNnSc1	slovo
behemot	behemot	k1gMnSc1	behemot
(	(	kIx(	(
<g/>
doloženo	doložen	k2eAgNnSc1d1	doloženo
např.	např.	kA	např.
v	v	k7c6	v
cestopise	cestopis	k1gInSc6	cestopis
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Haranta	Haranta	k?	Haranta
z	z	k7c2	z
Polžic	Polžice	k1gFnPc2	Polžice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Domorodá	domorodý	k2eAgNnPc1d1	domorodé
jména	jméno	k1gNnPc1	jméno
hrocha	hroch	k1gMnSc2	hroch
v	v	k7c6	v
afrických	africký	k2eAgInPc6d1	africký
jazycích	jazyk	k1gInPc6	jazyk
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozmanitá	rozmanitý	k2eAgNnPc4d1	rozmanité
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
arabským	arabský	k2eAgNnSc7d1	arabské
jménem	jméno	k1gNnSc7	jméno
faras	faras	k1gMnSc1	faras
an-nahr	anahr	k1gMnSc1	an-nahr
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
říční	říční	k2eAgMnSc1d1	říční
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
gumarre	gumarr	k1gInSc5	gumarr
<g/>
,	,	kIx,	,
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
džir	džira	k1gFnPc2	džira
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
ho	on	k3xPp3gMnSc2	on
Bambarové	Bambarové	k2eAgMnSc2d1	Bambarové
nazývají	nazývat	k5eAaImIp3nP	nazývat
mali	mali	k6eAd1	mali
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
svahilský	svahilský	k2eAgInSc1d1	svahilský
název	název	k1gInSc1	název
kiboko	kiboko	k6eAd1	kiboko
<g/>
,	,	kIx,	,
v	v	k7c6	v
nilotských	nilotský	k2eAgInPc6d1	nilotský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
makawu	makawu	k6eAd1	makawu
či	či	k8xC	či
masajsky	masajsky	k6eAd1	masajsky
ol-makau	olakau	k6eAd1	ol-makau
<g/>
,	,	kIx,	,
v	v	k7c6	v
bantuských	bantuský	k2eAgInPc6d1	bantuský
jazycích	jazyk	k1gInPc6	jazyk
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nejčastěji	často	k6eAd3	často
kubu	kubo	k1gNnSc3	kubo
<g/>
,	,	kIx,	,
mwubu	mwubat	k5eAaPmIp1nS	mwubat
<g/>
,	,	kIx,	,
či	či	k8xC	či
mvuvu	mvuvat	k5eAaPmIp1nS	mvuvat
<g/>
.	.	kIx.	.
</s>
<s>
Hroch	Hroch	k1gMnSc1	Hroch
je	být	k5eAaImIp3nS	být
nejtěžším	těžký	k2eAgInSc7d3	nejtěžší
recentním	recentní	k2eAgInSc7d1	recentní
druhem	druh	k1gInSc7	druh
sudokopytníka	sudokopytník	k1gMnSc2	sudokopytník
a	a	k8xC	a
třetím	třetí	k4xOgMnSc6	třetí
nejtěžším	těžký	k2eAgMnSc7d3	nejtěžší
suchozemským	suchozemský	k2eAgMnSc7d1	suchozemský
živočichem	živočich	k1gMnSc7	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
měří	měřit	k5eAaImIp3nS	měřit
až	až	k9	až
360	[number]	k4	360
cm	cm	kA	cm
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
165	[number]	k4	165
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
přes	přes	k7c4	přes
tři	tři	k4xCgFnPc4	tři
tuny	tuna	k1gFnPc4	tuna
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
menší	malý	k2eAgNnPc1d2	menší
(	(	kIx(	(
<g/>
až	až	k9	až
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hroší	hroší	k2eAgMnPc1d1	hroší
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
větší	veliký	k2eAgFnSc4d2	veliký
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgInSc4d2	delší
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc4d2	vyšší
končetiny	končetina	k1gFnPc4	končetina
než	než	k8xS	než
samice	samice	k1gFnPc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
hrocha	hroch	k1gMnSc2	hroch
má	mít	k5eAaImIp3nS	mít
válcovitý	válcovitý	k2eAgInSc4d1	válcovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
,	,	kIx,	,
zakulacené	zakulacený	k2eAgNnSc1d1	zakulacené
<g/>
,	,	kIx,	,
s	s	k7c7	s
objemným	objemný	k2eAgNnSc7d1	objemné
břichem	břicho	k1gNnSc7	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgFnPc1d1	silná
a	a	k8xC	a
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
čtyřmi	čtyři	k4xCgInPc7	čtyři
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
ukončenými	ukončená	k1gFnPc7	ukončená
tupými	tupý	k2eAgNnPc7d1	tupé
kopýtky	kopýtko	k1gNnPc7	kopýtko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
plovací	plovací	k2eAgFnPc4d1	plovací
blány	blána	k1gFnPc4	blána
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
došlapuje	došlapovat	k5eAaImIp3nS	došlapovat
hroch	hroch	k1gMnSc1	hroch
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
prst	prst	k1gInSc4	prst
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgFnPc1d2	silnější
než	než	k8xS	než
dva	dva	k4xCgInPc1	dva
postranní	postranní	k2eAgInPc1d1	postranní
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
hrocha	hroch	k1gMnSc2	hroch
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
měrnou	měrný	k2eAgFnSc4d1	měrná
hmotnost	hmotnost	k1gFnSc4	hmotnost
hrošího	hroší	k2eAgNnSc2d1	hroší
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
hrochům	hroch	k1gMnPc3	hroch
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
potápění	potápění	k1gNnSc2	potápění
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jim	on	k3xPp3gMnPc3	on
i	i	k9	i
chůzi	chůze	k1gFnSc4	chůze
po	po	k7c6	po
dně	dno	k1gNnSc6	dno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
hledají	hledat	k5eAaImIp3nP	hledat
část	část	k1gFnSc4	část
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
opatřený	opatřený	k2eAgInSc1d1	opatřený
tuhým	tuhý	k2eAgInSc7d1	tuhý
střapcem	střapec	k1gInSc7	střapec
chlupů	chlup	k1gInPc2	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Hroší	hroší	k2eAgFnSc1d1	hroší
hlava	hlava	k1gFnSc1	hlava
má	mít	k5eAaImIp3nS	mít
širokou	široký	k2eAgFnSc4d1	široká
<g/>
,	,	kIx,	,
zakulacenou	zakulacený	k2eAgFnSc4d1	zakulacená
obličejovou	obličejový	k2eAgFnSc4d1	obličejová
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
mozkovna	mozkovna	k1gFnSc1	mozkovna
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
a	a	k8xC	a
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
i	i	k8xC	i
nozdry	nozdra	k1gFnPc4	nozdra
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
velmi	velmi	k6eAd1	velmi
vysoko	vysoko	k6eAd1	vysoko
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
adaptace	adaptace	k1gFnSc1	adaptace
pro	pro	k7c4	pro
život	život	k1gInSc4	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nozdry	nozdra	k1gFnPc1	nozdra
a	a	k8xC	a
uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
opatřeny	opatřen	k2eAgFnPc1d1	opatřena
záklopkami	záklopka	k1gFnPc7	záklopka
a	a	k8xC	a
hroch	hroch	k1gMnSc1	hroch
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
ponoření	ponoření	k1gNnSc6	ponoření
zavírá	zavírat	k5eAaImIp3nS	zavírat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
jsou	být	k5eAaImIp3nP	být
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Řezáky	řezák	k1gInPc1	řezák
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
čelisti	čelist	k1gFnSc6	čelist
jsou	být	k5eAaImIp3nP	být
kolíkovité	kolíkovitý	k2eAgInPc1d1	kolíkovitý
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
šikmo	šikmo	k6eAd1	šikmo
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
špičáky	špičák	k1gInPc1	špičák
jsou	být	k5eAaImIp3nP	být
přeměněny	přeměnit	k5eAaPmNgInP	přeměnit
v	v	k7c4	v
kly	kel	k1gInPc4	kel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
úctyhodných	úctyhodný	k2eAgInPc2d1	úctyhodný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dásní	dáseň	k1gFnPc2	dáseň
vynikají	vynikat	k5eAaImIp3nP	vynikat
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
asi	asi	k9	asi
35	[number]	k4	35
cm	cm	kA	cm
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
mohou	moct	k5eAaImIp3nP	moct
měřit	měřit	k5eAaImF	měřit
až	až	k9	až
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
jako	jako	k9	jako
zbraň	zbraň	k1gFnSc4	zbraň
ve	v	k7c6	v
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
soubojích	souboj	k1gInPc6	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Špičáky	špičák	k1gInPc1	špičák
i	i	k8xC	i
řezáky	řezák	k1gInPc1	řezák
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
kolíkovitý	kolíkovitý	k2eAgInSc4d1	kolíkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
špičáky	špičák	k1gInPc4	špičák
a	a	k8xC	a
řezáky	řezák	k1gInPc4	řezák
hrochům	hroch	k1gMnPc3	hroch
rostou	růst	k5eAaImIp3nP	růst
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
stoličky	stolička	k1gFnPc1	stolička
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyměňují	vyměňovat	k5eAaImIp3nP	vyměňovat
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
u	u	k7c2	u
slonů	slon	k1gMnPc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
trhání	trhání	k1gNnSc6	trhání
potravy	potrava	k1gFnSc2	potrava
slouží	sloužit	k5eAaImIp3nS	sloužit
hrochům	hroch	k1gMnPc3	hroch
tuhé	tuhý	k2eAgFnSc2d1	tuhá
<g/>
,	,	kIx,	,
kožovité	kožovitý	k2eAgInPc1d1	kožovitý
pysky	pysk	k1gInPc1	pysk
<g/>
,	,	kIx,	,
zuby	zub	k1gInPc1	zub
používají	používat	k5eAaImIp3nP	používat
jen	jen	k9	jen
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
kořenů	kořen	k1gInPc2	kořen
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Hroší	hroší	k2eAgFnSc1d1	hroší
kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
lysá	lysý	k2eAgFnSc1d1	Lysá
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
tlustá	tlustý	k2eAgNnPc1d1	tlusté
(	(	kIx(	(
<g/>
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chlupy	chlup	k1gInPc1	chlup
rostou	růst	k5eAaImIp3nP	růst
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
ušních	ušní	k2eAgMnPc2d1	ušní
boltců	boltec	k1gInPc2	boltec
a	a	k8xC	a
na	na	k7c6	na
čenichu	čenich	k1gInSc6	čenich
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kůží	kůže	k1gFnSc7	kůže
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
pěticentimetrová	pěticentimetrový	k2eAgFnSc1d1	pěticentimetrová
vrstva	vrstva	k1gFnSc1	vrstva
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
hrocha	hroch	k1gMnSc2	hroch
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
břidlicově	břidlicově	k6eAd1	břidlicově
šedé	šedý	k2eAgFnSc2d1	šedá
<g/>
,	,	kIx,	,
šedohnědé	šedohnědý	k2eAgFnSc2d1	šedohnědá
až	až	k9	až
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
a	a	k8xC	a
končetinách	končetina	k1gFnPc6	končetina
je	být	k5eAaImIp3nS	být
kůže	kůže	k1gFnSc1	kůže
vždy	vždy	k6eAd1	vždy
světlejší	světlý	k2eAgFnSc1d2	světlejší
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
růžova	růžovo	k1gNnSc2	růžovo
<g/>
.	.	kIx.	.
</s>
<s>
Hroší	hroší	k2eAgFnSc1d1	hroší
kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
tloušťce	tloušťka	k1gFnSc3	tloušťka
velmi	velmi	k6eAd1	velmi
citlivá	citlivý	k2eAgFnSc1d1	citlivá
a	a	k8xC	a
v	v	k7c6	v
suchu	sucho	k1gNnSc6	sucho
snadno	snadno	k6eAd1	snadno
popraská	popraskat	k5eAaPmIp3nS	popraskat
<g/>
.	.	kIx.	.
</s>
<s>
Rčení	rčení	k1gNnSc1	rčení
o	o	k7c6	o
hroší	hroší	k2eAgFnSc6d1	hroší
kůži	kůže	k1gFnSc6	kůže
jako	jako	k9	jako
symbolu	symbol	k1gInSc2	symbol
necitelnosti	necitelnost	k1gFnSc2	necitelnost
tedy	tedy	k9	tedy
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
prostoupena	prostoupit	k5eAaPmNgFnS	prostoupit
potními	potní	k2eAgFnPc7d1	potní
žlázami	žláza	k1gFnPc7	žláza
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
žlázami	žláza	k1gFnPc7	žláza
mazovými	mazový	k2eAgFnPc7d1	mazová
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
olejnatý	olejnatý	k2eAgInSc4d1	olejnatý
sekret	sekret	k1gInSc4	sekret
<g/>
,	,	kIx,	,
mající	mající	k2eAgFnSc4d1	mající
růžovou	růžový	k2eAgFnSc4d1	růžová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sekret	sekret	k1gInSc1	sekret
hrocha	hroch	k1gMnSc2	hroch
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
slunečním	sluneční	k2eAgNnSc7d1	sluneční
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
snižuje	snižovat	k5eAaImIp3nS	snižovat
odpor	odpor	k1gInSc4	odpor
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výměšky	výměšek	k1gInPc1	výměšek
mají	mít	k5eAaImIp3nP	mít
růžovou	růžový	k2eAgFnSc4d1	růžová
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zavdalo	zavdat	k5eAaPmAgNnS	zavdat
příčinu	příčina	k1gFnSc4	příčina
pověrám	pověra	k1gFnPc3	pověra
<g/>
,	,	kIx,	,
že	že	k8xS	že
hroši	hroch	k1gMnPc1	hroch
potí	potit	k5eAaImIp3nP	potit
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
letopočtu	letopočet	k1gInSc2	letopočet
se	se	k3xPyFc4	se
hroši	hroch	k1gMnPc1	hroch
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
na	na	k7c6	na
vhodných	vhodný	k2eAgNnPc6d1	vhodné
místech	místo	k1gNnPc6	místo
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Africe	Afrika	k1gFnSc6	Afrika
od	od	k7c2	od
delty	delta	k1gFnSc2	delta
Nilu	Nil	k1gInSc2	Nil
až	až	k9	až
po	po	k7c4	po
Kapsko	Kapsko	k1gNnSc4	Kapsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Nilu	Nil	k1gInSc2	Nil
byli	být	k5eAaImAgMnP	být
hroši	hroch	k1gMnPc1	hroch
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
Kapska	Kapsko	k1gNnSc2	Kapsko
<g/>
,	,	kIx,	,
Núbie	Núbie	k1gFnSc2	Núbie
a	a	k8xC	a
severního	severní	k2eAgInSc2d1	severní
Súdánu	Súdán	k1gInSc2	Súdán
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
od	od	k7c2	od
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
,	,	kIx,	,
Senegalu	Senegal	k1gInSc2	Senegal
a	a	k8xC	a
Čadského	čadský	k2eAgNnSc2d1	Čadské
jezera	jezero	k1gNnSc2	jezero
až	až	k9	až
po	po	k7c4	po
jihoafrický	jihoafrický	k2eAgInSc4d1	jihoafrický
Natal	Natal	k1gInSc4	Natal
se	se	k3xPyFc4	se
hroši	hroch	k1gMnPc1	hroch
udrželi	udržet	k5eAaPmAgMnP	udržet
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
stav	stav	k1gInSc1	stav
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
hrochů	hroch	k1gMnPc2	hroch
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
odhadoval	odhadovat	k5eAaImAgInS	odhadovat
na	na	k7c4	na
150	[number]	k4	150
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
před	před	k7c7	před
5-10	[number]	k4	5-10
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
žili	žít	k5eAaImAgMnP	žít
hroši	hroch	k1gMnPc1	hroch
i	i	k9	i
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
klima	klima	k1gNnSc1	klima
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
vlhčí	vlhký	k2eAgNnSc1d2	vlhčí
než	než	k8xS	než
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
druhy	druh	k1gInPc1	druh
obývaly	obývat	k5eAaImAgInP	obývat
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
jižní	jižní	k2eAgFnSc4d1	jižní
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
do	do	k7c2	do
historické	historický	k2eAgFnSc2d1	historická
doby	doba	k1gFnSc2	doba
rovněž	rovněž	k9	rovněž
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přinejmenším	přinejmenším	k6eAd1	přinejmenším
dva	dva	k4xCgInPc1	dva
malé	malý	k2eAgInPc1d1	malý
druhy	druh	k1gInPc1	druh
hrochů	hroch	k1gMnPc2	hroch
vyhynuly	vyhynout	k5eAaPmAgFnP	vyhynout
až	až	k6eAd1	až
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
lidí	člověk	k1gMnPc2	člověk
počátkem	počátkem	k7c2	počátkem
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Hroši	hroch	k1gMnPc1	hroch
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Preferují	preferovat	k5eAaImIp3nP	preferovat
stojaté	stojatý	k2eAgFnPc1d1	stojatá
nebo	nebo	k8xC	nebo
pomalu	pomalu	k6eAd1	pomalu
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rozsáhlejší	rozsáhlý	k2eAgFnPc1d2	rozsáhlejší
bažiny	bažina	k1gFnPc1	bažina
porosty	porost	k1gInPc7	porost
rákosu	rákos	k1gInSc2	rákos
<g/>
,	,	kIx,	,
šáchoru	šáchor	k1gInSc2	šáchor
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
bahenní	bahenní	k2eAgFnSc2d1	bahenní
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
loveni	loven	k2eAgMnPc1d1	loven
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
hroši	hroch	k1gMnPc1	hroch
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
plaší	plachý	k2eAgMnPc1d1	plachý
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
<g/>
,	,	kIx,	,
protékajících	protékající	k2eAgFnPc6d1	protékající
většími	veliký	k2eAgNnPc7d2	veliký
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Bujumbura	Bujumbura	k1gFnSc1	Bujumbura
<g/>
,	,	kIx,	,
Entebbe	Entebb	k1gInSc5	Entebb
či	či	k8xC	či
N	N	kA	N
<g/>
'	'	kIx"	'
<g/>
Djamena	Djamen	k2eAgFnSc1d1	Djamena
<g/>
.	.	kIx.	.
</s>
<s>
Hroši	hroch	k1gMnPc1	hroch
jsou	být	k5eAaImIp3nP	být
společenská	společenský	k2eAgNnPc4d1	společenské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
různě	různě	k6eAd1	různě
velkých	velký	k2eAgFnPc6d1	velká
tlupách	tlupa	k1gFnPc6	tlupa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
150	[number]	k4	150
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
čítají	čítat	k5eAaImIp3nP	čítat
jen	jen	k9	jen
10-20	[number]	k4	10-20
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
tlupy	tlupa	k1gFnPc1	tlupa
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinou	většinou	k6eAd1	většinou
jeden	jeden	k4xCgInSc4	jeden
samec	samec	k1gInSc4	samec
s	s	k7c7	s
několika	několik	k4yIc7	několik
samicemi	samice	k1gFnPc7	samice
a	a	k8xC	a
mláďaty	mládě	k1gNnPc7	mládě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
bývá	bývat	k5eAaImIp3nS	bývat
samců	samec	k1gInPc2	samec
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
hrochy	hroch	k1gMnPc7	hroch
vládne	vládnout	k5eAaImIp3nS	vládnout
přísná	přísný	k2eAgFnSc1d1	přísná
hierarchie	hierarchie	k1gFnSc1	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
samec	samec	k1gMnSc1	samec
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
teritoria	teritorium	k1gNnPc4	teritorium
tlupy	tlupa	k1gFnSc2	tlupa
o	o	k7c6	o
rozsahu	rozsah	k1gInSc6	rozsah
asi	asi	k9	asi
500	[number]	k4	500
m	m	kA	m
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
a	a	k8xC	a
páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Teritorium	teritorium	k1gNnSc1	teritorium
si	se	k3xPyFc3	se
hroší	hroší	k2eAgMnPc1d1	hroší
samci	samec	k1gMnPc1	samec
označují	označovat	k5eAaImIp3nP	označovat
trusem	trus	k1gInSc7	trus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
i	i	k8xC	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozmetávají	rozmetávat	k5eAaImIp3nP	rozmetávat
pomocí	pomocí	k7c2	pomocí
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
bývají	bývat	k5eAaImIp3nP	bývat
teritoria	teritorium	k1gNnPc4	teritorium
obvykle	obvykle	k6eAd1	obvykle
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
na	na	k7c6	na
stojatých	stojatý	k2eAgFnPc6d1	stojatá
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
hustší	hustý	k2eAgFnSc7d2	hustší
populací	populace	k1gFnSc7	populace
hrochů	hroch	k1gMnPc2	hroch
spolu	spolu	k6eAd1	spolu
samci	samec	k1gMnPc1	samec
často	často	k6eAd1	často
vzájemně	vzájemně	k6eAd1	vzájemně
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
či	či	k8xC	či
o	o	k7c4	o
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
"	"	kIx"	"
<g/>
zívání	zívání	k1gNnSc1	zívání
<g/>
"	"	kIx"	"
hroších	hroší	k2eAgInPc2d1	hroší
samců	samec	k1gInPc2	samec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
projev	projev	k1gInSc4	projev
imponování	imponování	k1gNnSc2	imponování
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
takto	takto	k6eAd1	takto
hroši	hroch	k1gMnPc1	hroch
žebrají	žebrat	k5eAaImIp3nP	žebrat
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ani	ani	k9	ani
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
samců	samec	k1gInPc2	samec
nedá	dát	k5eNaPmIp3nS	dát
imponováním	imponování	k1gNnSc7	imponování
zastrašit	zastrašit	k5eAaPmF	zastrašit
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
souboj	souboj	k1gInSc1	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
koušou	kousat	k5eAaImIp3nP	kousat
tesáky	tesák	k1gInPc1	tesák
<g/>
,	,	kIx,	,
boje	boj	k1gInPc1	boj
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
kruté	krutý	k2eAgNnSc1d1	kruté
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nP	svědčit
časté	častý	k2eAgInPc1d1	častý
šrámy	šrám	k1gInPc1	šrám
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
vůdčí	vůdčí	k2eAgMnSc1d1	vůdčí
samec	samec	k1gMnSc1	samec
poražen	poražen	k2eAgMnSc1d1	poražen
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ovládne	ovládnout	k5eAaPmIp3nS	ovládnout
tlupu	tlupa	k1gFnSc4	tlupa
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
všechna	všechen	k3xTgNnPc4	všechen
sající	sající	k2eAgNnPc4d1	sající
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
zplodil	zplodit	k5eAaPmAgMnS	zplodit
jeho	jeho	k3xOp3gMnSc4	jeho
předchůdce	předchůdce	k1gMnSc4	předchůdce
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
lvů	lev	k1gInPc2	lev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
podřízenosti	podřízenost	k1gFnSc2	podřízenost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
hrochů	hroch	k1gMnPc2	hroch
tzv.	tzv.	kA	tzv.
submisivní	submisivní	k2eAgNnSc1d1	submisivní
kálení	kálení	k1gNnSc1	kálení
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
hroch	hroch	k1gMnSc1	hroch
vystrčí	vystrčit	k5eAaPmIp3nS	vystrčit
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vykálí	vykálet	k5eAaPmIp3nS	vykálet
se	se	k3xPyFc4	se
před	před	k7c7	před
dominantním	dominantní	k2eAgMnSc7d1	dominantní
jedincem	jedinec	k1gMnSc7	jedinec
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doslova	doslova	k6eAd1	doslova
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
čenich	čenich	k1gInSc4	čenich
<g/>
.	.	kIx.	.
</s>
<s>
Vůdčí	vůdčí	k2eAgMnPc1d1	vůdčí
samci	samec	k1gMnPc1	samec
tento	tento	k3xDgInSc4	tento
projev	projev	k1gInSc4	projev
často	často	k6eAd1	často
přímo	přímo	k6eAd1	přímo
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
od	od	k7c2	od
podřízených	podřízený	k2eAgMnPc2d1	podřízený
samců	samec	k1gMnPc2	samec
v	v	k7c6	v
tlupě	tlupa	k1gFnSc6	tlupa
<g/>
.	.	kIx.	.
</s>
<s>
Hroši	hroch	k1gMnPc1	hroch
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
hlučná	hlučný	k2eAgNnPc1d1	hlučné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
ozývají	ozývat	k5eAaImIp3nP	ozývat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
i	i	k8xC	i
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dutým	dutý	k2eAgNnSc7d1	duté
řvaním	řvaní	k1gNnSc7	řvaní
<g/>
,	,	kIx,	,
bučením	bučení	k1gNnSc7	bučení
<g/>
,	,	kIx,	,
funěním	funění	k1gNnSc7	funění
<g/>
,	,	kIx,	,
chrochtáním	chrochtání	k1gNnSc7	chrochtání
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
zvuky	zvuk	k1gInPc7	zvuk
<g/>
,	,	kIx,	,
mládě	mládě	k1gNnSc1	mládě
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
přivolává	přivolávat	k5eAaImIp3nS	přivolávat
matku	matka	k1gFnSc4	matka
kvičením	kvičení	k1gNnPc3	kvičení
<g/>
.	.	kIx.	.
</s>
<s>
Hroši	hroch	k1gMnPc1	hroch
výborně	výborně	k6eAd1	výborně
plavou	plavat	k5eAaImIp3nP	plavat
<g/>
,	,	kIx,	,
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
měrné	měrný	k2eAgFnSc3d1	měrná
hmotnosti	hmotnost	k1gFnSc3	hmotnost
dokáží	dokázat	k5eAaPmIp3nP	dokázat
i	i	k9	i
chodit	chodit	k5eAaImF	chodit
po	po	k7c6	po
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Ponor	ponor	k1gInSc1	ponor
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
nadechnutí	nadechnutí	k1gNnSc4	nadechnutí
trvá	trvat	k5eAaImIp3nS	trvat
většinou	většinou	k6eAd1	většinou
3-5	[number]	k4	3-5
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
poranění	poranění	k1gNnSc6	poranění
<g/>
,	,	kIx,	,
však	však	k9	však
vydrží	vydržet	k5eAaPmIp3nS	vydržet
hroch	hroch	k1gMnSc1	hroch
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
daleko	daleko	k6eAd1	daleko
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
rekordně	rekordně	k6eAd1	rekordně
až	až	k9	až
25	[number]	k4	25
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
těžkopádnost	těžkopádnost	k1gFnSc4	těžkopádnost
dokáží	dokázat	k5eAaPmIp3nP	dokázat
hroši	hroch	k1gMnPc1	hroch
obratně	obratně	k6eAd1	obratně
šplhat	šplhat	k5eAaImF	šplhat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
především	především	k9	především
při	při	k7c6	při
vylézání	vylézání	k1gNnSc6	vylézání
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
strmý	strmý	k2eAgInSc4d1	strmý
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
hluboké	hluboký	k2eAgFnPc4d1	hluboká
blátivé	blátivý	k2eAgFnPc4d1	blátivá
brázdy	brázda	k1gFnPc4	brázda
<g/>
,	,	kIx,	,
jakési	jakýsi	k3yIgInPc4	jakýsi
koridory	koridor	k1gInPc4	koridor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
si	se	k3xPyFc3	se
hroši	hroch	k1gMnPc1	hroch
vyšlapali	vyšlapat	k5eAaPmAgMnP	vyšlapat
do	do	k7c2	do
říčního	říční	k2eAgInSc2d1	říční
břehu	břeh	k1gInSc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
je	být	k5eAaImIp3nS	být
hroch	hroch	k1gMnSc1	hroch
schopen	schopen	k2eAgMnSc1d1	schopen
běžet	běžet	k5eAaImF	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
k	k	k7c3	k
běhu	běh	k1gInSc3	běh
se	se	k3xPyFc4	se
však	však	k9	však
odhodlá	odhodlat	k5eAaPmIp3nS	odhodlat
jen	jen	k9	jen
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
tráví	trávit	k5eAaImIp3nP	trávit
hroši	hroch	k1gMnPc1	hroch
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
sluní	slunit	k5eAaImIp3nS	slunit
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
jejich	jejich	k3xOp3gFnSc1	jejich
aktivita	aktivita	k1gFnSc1	aktivita
narůstá	narůstat	k5eAaImIp3nS	narůstat
a	a	k8xC	a
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
vycházejí	vycházet	k5eAaImIp3nP	vycházet
na	na	k7c4	na
pastvu	pastva	k1gFnSc4	pastva
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
rekordně	rekordně	k6eAd1	rekordně
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
i	i	k9	i
pole	pole	k1gNnSc1	pole
nebo	nebo	k8xC	nebo
plantáže	plantáž	k1gFnPc1	plantáž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
napáchat	napáchat	k5eAaBmF	napáchat
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
hrochem	hroch	k1gMnSc7	hroch
a	a	k8xC	a
některými	některý	k3yIgMnPc7	některý
dalšími	další	k2eAgMnPc7d1	další
živočichy	živočich	k1gMnPc7	živočich
existují	existovat	k5eAaImIp3nP	existovat
mutualistické	mutualistický	k2eAgInPc4d1	mutualistický
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Hrochy	Hroch	k1gMnPc4	Hroch
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
i	i	k8xC	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
volavka	volavka	k1gFnSc1	volavka
rusohlavá	rusohlavý	k2eAgFnSc1d1	rusohlavá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gInPc4	on
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
parazitů	parazit	k1gMnPc2	parazit
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
přítomnosti	přítomnost	k1gFnSc6	přítomnost
loví	lovit	k5eAaImIp3nS	lovit
drobné	drobný	k2eAgMnPc4d1	drobný
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Volavky	volavka	k1gFnPc1	volavka
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
hrocha	hroch	k1gMnSc4	hroch
varovat	varovat	k5eAaImF	varovat
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
klubáci	klubák	k1gMnPc1	klubák
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
hrocha	hroch	k1gMnSc4	hroch
vzácněji	vzácně	k6eAd2	vzácně
než	než	k8xS	než
jiné	jiný	k2eAgMnPc4d1	jiný
velké	velký	k2eAgMnPc4d1	velký
africké	africký	k2eAgMnPc4d1	africký
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
hrochům	hroch	k1gMnPc3	hroch
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
parazity	parazit	k1gMnPc4	parazit
také	také	k9	také
želva	želva	k1gFnSc1	želva
pelomedusa	pelomedus	k1gMnSc2	pelomedus
(	(	kIx(	(
<g/>
Pelomedusa	Pelomedus	k1gMnSc2	Pelomedus
subrufa	subruf	k1gMnSc2	subruf
<g/>
)	)	kIx)	)
a	a	k8xC	a
kaprovitá	kaprovitý	k2eAgFnSc1d1	kaprovitá
ryba	ryba	k1gFnSc1	ryba
labeo	labeo	k6eAd1	labeo
dlouhoploutvé	dlouhoploutvá	k1gFnSc2	dlouhoploutvá
(	(	kIx(	(
<g/>
Labeo	Labeo	k1gNnSc1	Labeo
longipinnis	longipinnis	k1gFnPc2	longipinnis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
masitými	masitý	k2eAgInPc7d1	masitý
pysky	pysk	k1gInPc7	pysk
přisává	přisávat	k5eAaImIp3nS	přisávat
na	na	k7c4	na
horší	zlý	k2eAgFnSc4d2	horší
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
čistí	čistit	k5eAaImIp3nP	čistit
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
pijavek	pijavka	k1gFnPc2	pijavka
i	i	k8xC	i
odumřelé	odumřelý	k2eAgFnSc2d1	odumřelá
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Labeo	Labeo	k6eAd1	Labeo
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
ryby	ryba	k1gFnPc1	ryba
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
rovněž	rovněž	k9	rovněž
hroším	hroší	k2eAgInSc7d1	hroší
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Hroch	Hroch	k1gMnSc1	Hroch
si	se	k3xPyFc3	se
získává	získávat	k5eAaImIp3nS	získávat
část	část	k1gFnSc4	část
potravy	potrava	k1gFnSc2	potrava
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
zubů	zub	k1gInPc2	zub
vyrývá	vyrývat	k5eAaImIp3nS	vyrývat
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
oddenky	oddenek	k1gInPc1	oddenek
a	a	k8xC	a
kořeny	kořen	k1gInPc1	kořen
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lotosu	lotos	k1gInSc2	lotos
<g/>
.	.	kIx.	.
</s>
<s>
Spásá	spásat	k5eAaImIp3nS	spásat
i	i	k9	i
plovoucí	plovoucí	k2eAgFnPc4d1	plovoucí
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
pistie	pistie	k1gFnSc2	pistie
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
potravy	potrava	k1gFnSc2	potrava
ale	ale	k8xC	ale
získává	získávat	k5eAaImIp3nS	získávat
noční	noční	k2eAgFnSc7d1	noční
pastvou	pastva	k1gFnSc7	pastva
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Hroší	hroší	k2eAgInSc1d1	hroší
žaludek	žaludek	k1gInSc1	žaludek
je	být	k5eAaImIp3nS	být
trojdílný	trojdílný	k2eAgInSc1d1	trojdílný
<g/>
,	,	kIx,	,
mimořádně	mimořádně	k6eAd1	mimořádně
objemný	objemný	k2eAgMnSc1d1	objemný
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mnoho	mnoho	k6eAd1	mnoho
druhů	druh	k1gInPc2	druh
symbiotických	symbiotický	k2eAgMnPc2d1	symbiotický
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
hroch	hroch	k1gMnSc1	hroch
schopen	schopen	k2eAgMnSc1d1	schopen
strávit	strávit	k5eAaPmF	strávit
i	i	k9	i
velmi	velmi	k6eAd1	velmi
hrubou	hrubý	k2eAgFnSc4d1	hrubá
a	a	k8xC	a
vláknitou	vláknitý	k2eAgFnSc4d1	vláknitá
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
jiní	jiný	k2eAgMnPc1d1	jiný
býložravci	býložravec	k1gMnPc1	býložravec
nežerou	žrát	k5eNaImIp3nP	žrát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ostřici	ostřice	k1gFnSc4	ostřice
nebo	nebo	k8xC	nebo
šáchor	šáchor	k1gInSc4	šáchor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
mršinou	mršina	k1gFnSc7	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
si	se	k3xPyFc3	se
vybírat	vybírat	k5eAaImF	vybírat
<g/>
,	,	kIx,	,
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
chutnější	chutný	k2eAgInPc4d2	chutnější
druhy	druh	k1gInPc4	druh
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vousatka	vousatka	k1gFnSc1	vousatka
<g/>
,	,	kIx,	,
troskut	troskut	k1gInSc1	troskut
či	či	k8xC	či
divoké	divoký	k2eAgNnSc1d1	divoké
proso	proso	k1gNnSc1	proso
<g/>
.	.	kIx.	.
</s>
<s>
Hroch	Hroch	k1gMnSc1	Hroch
sežere	sežrat	k5eAaPmIp3nS	sežrat
denně	denně	k6eAd1	denně
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
kg	kg	kA	kg
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
velkosti	velkost	k1gFnSc3	velkost
relativně	relativně	k6eAd1	relativně
málo	málo	k6eAd1	málo
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
hmotnosti	hmotnost	k1gFnSc3	hmotnost
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
poloviční	poloviční	k2eAgFnSc1d1	poloviční
spotřeba	spotřeba	k1gFnSc1	spotřeba
potravy	potrava	k1gFnSc2	potrava
než	než	k8xS	než
u	u	k7c2	u
domácího	domácí	k2eAgInSc2d1	domácí
skotu	skot	k1gInSc2	skot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hroši	hroch	k1gMnPc1	hroch
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
většinou	většina	k1gFnSc7	většina
samic	samice	k1gFnPc2	samice
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nS	pářit
vůdčí	vůdčí	k2eAgMnSc1d1	vůdčí
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
často	často	k6eAd1	často
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
páření	páření	k1gNnSc1	páření
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
samicemi	samice	k1gFnPc7	samice
i	i	k8xC	i
ostatním	ostatní	k2eAgMnPc3d1	ostatní
samcům	samec	k1gMnPc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hrošice	Hrošice	k1gFnSc1	Hrošice
je	být	k5eAaImIp3nS	být
březí	březí	k2eAgFnSc1d1	březí
230-240	[number]	k4	230-240
dní	den	k1gInPc2	den
a	a	k8xC	a
rodí	rodit	k5eAaImIp3nS	rodit
většinou	většinou	k6eAd1	většinou
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
nadechlo	nadechnout	k5eAaPmAgNnS	nadechnout
<g/>
.	.	kIx.	.
</s>
<s>
Plavat	plavat	k5eAaImF	plavat
umí	umět	k5eAaImIp3nS	umět
mládě	mládě	k1gNnSc1	mládě
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
chodit	chodit	k5eAaImF	chodit
a	a	k8xC	a
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
kojeno	kojen	k2eAgNnSc1d1	kojeno
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
saje	sát	k5eAaImIp3nS	sát
asi	asi	k9	asi
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
jej	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
až	až	k9	až
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
porodu	porod	k1gInSc2	porod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
samice	samice	k1gFnSc1	samice
vodí	vodit	k5eAaImIp3nS	vodit
první	první	k4xOgNnSc1	první
mládě	mládě	k1gNnSc1	mládě
i	i	k8xC	i
po	po	k7c6	po
vrhu	vrh	k1gInSc6	vrh
dalšího	další	k2eAgMnSc2d1	další
sourozence	sourozenec	k1gMnSc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
vodí	vodit	k5eAaImIp3nP	vodit
mládě	mládě	k1gNnSc4	mládě
obvykle	obvykle	k6eAd1	obvykle
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
malá	malý	k2eAgNnPc1d1	malé
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vozí	vozit	k5eAaImIp3nP	vozit
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
<g/>
Občas	občas	k6eAd1	občas
matce	matka	k1gFnSc3	matka
s	s	k7c7	s
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
mládě	mládě	k1gNnSc4	mládě
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
slonů	slon	k1gMnPc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
hrochů	hroch	k1gMnPc2	hroch
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
zvědavá	zvědavý	k2eAgNnPc1d1	zvědavé
a	a	k8xC	a
hravá	hravý	k2eAgNnPc1d1	hravé
<g/>
,	,	kIx,	,
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
ochočit	ochočit	k5eAaPmF	ochočit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
mladým	mladý	k2eAgMnPc3d1	mladý
hrochům	hroch	k1gMnPc3	hroch
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
především	především	k9	především
od	od	k7c2	od
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
od	od	k7c2	od
dospělých	dospělý	k2eAgMnPc2d1	dospělý
hroších	hroší	k2eAgMnPc2d1	hroší
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
před	před	k7c7	před
nimiž	jenž	k3xRgMnPc7	jenž
matka	matka	k1gFnSc1	matka
mládě	mládě	k1gNnSc1	mládě
zuřivě	zuřivě	k6eAd1	zuřivě
brání	bránit	k5eAaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Plné	plný	k2eAgFnSc2d1	plná
velikosti	velikost	k1gFnSc2	velikost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hroši	hroch	k1gMnPc1	hroch
až	až	k6eAd1	až
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
5	[number]	k4	5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
do	do	k7c2	do
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
nejdříve	dříve	k6eAd3	dříve
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
až	až	k9	až
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
12-15	[number]	k4	12-15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hroch	Hroch	k1gMnSc1	Hroch
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
55	[number]	k4	55
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
to	ten	k3xDgNnSc1	ten
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
asi	asi	k9	asi
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
hroch	hroch	k1gMnSc1	hroch
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
61	[number]	k4	61
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
v	v	k7c6	v
zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Evansville	Evansville	k1gFnSc2	Evansville
<g/>
,	,	kIx,	,
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
nápadný	nápadný	k2eAgInSc4d1	nápadný
vzhled	vzhled	k1gInSc4	vzhled
poutali	poutat	k5eAaImAgMnP	poutat
hroši	hroch	k1gMnPc1	hroch
odedávna	odedávna	k6eAd1	odedávna
pozornost	pozornost	k1gFnSc4	pozornost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hrochy	Hroch	k1gMnPc4	Hroch
lovili	lovit	k5eAaImAgMnP	lovit
už	už	k6eAd1	už
prehistoričtí	prehistorický	k2eAgMnPc1d1	prehistorický
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgMnPc7d1	známý
lovci	lovec	k1gMnPc7	lovec
hrochů	hroch	k1gMnPc2	hroch
byli	být	k5eAaImAgMnP	být
staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
používali	používat	k5eAaImAgMnP	používat
harpuny	harpuna	k1gFnPc4	harpuna
s	s	k7c7	s
plováky	plovák	k1gInPc7	plovák
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
lovili	lovit	k5eAaImAgMnP	lovit
hrochy	hroch	k1gMnPc4	hroch
domorodci	domorodec	k1gMnPc7	domorodec
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Africe	Afrika	k1gFnSc6	Afrika
ještě	ještě	k9	ještě
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
byli	být	k5eAaImAgMnP	být
hroši	hroch	k1gMnPc1	hroch
stříleni	střílen	k2eAgMnPc1d1	střílen
otrávenými	otrávený	k2eAgInPc7d1	otrávený
šípy	šíp	k1gInPc7	šíp
<g/>
,	,	kIx,	,
chytáni	chytán	k2eAgMnPc1d1	chytán
do	do	k7c2	do
jam	jáma	k1gFnPc2	jáma
nebo	nebo	k8xC	nebo
chytacích	chytací	k2eAgFnPc2d1	chytací
ohrad	ohrada	k1gFnPc2	ohrada
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
loveni	lovit	k5eAaImNgMnP	lovit
odstřelem	odstřel	k1gInSc7	odstřel
pomocí	pomoc	k1gFnPc2	pomoc
těžkých	těžký	k2eAgFnPc2d1	těžká
kulovnic	kulovnice	k1gFnPc2	kulovnice
<g/>
.	.	kIx.	.
</s>
<s>
Hroší	hroší	k2eAgNnSc1d1	hroší
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
chutné	chutný	k2eAgNnSc1d1	chutné
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
tučné	tučný	k2eAgInPc1d1	tučný
(	(	kIx(	(
<g/>
tuk	tuk	k1gInSc1	tuk
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
pod	pod	k7c7	pod
kůží	kůže	k1gFnSc7	kůže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
domorodci	domorodec	k1gMnPc1	domorodec
ho	on	k3xPp3gMnSc4	on
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
nejčastěji	často	k6eAd3	často
sušením	sušení	k1gNnSc7	sušení
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
také	také	k9	také
hroší	hroší	k2eAgFnSc1d1	hroší
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
např.	např.	kA	např.
štíty	štít	k1gInPc4	štít
či	či	k8xC	či
biče	bič	k1gInPc4	bič
<g/>
.	.	kIx.	.
</s>
<s>
Hroší	hroší	k2eAgInPc1d1	hroší
zuby	zub	k1gInPc1	zub
lze	lze	k6eAd1	lze
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
<g/>
,	,	kIx,	,
žlutohnědé	žlutohnědý	k2eAgFnSc2d1	žlutohnědá
skloviny	sklovina	k1gFnSc2	sklovina
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
slonovinu	slonovina	k1gFnSc4	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
menší	malý	k2eAgFnPc4d2	menší
<g/>
,	,	kIx,	,
věkem	věk	k1gInSc7	věk
však	však	k9	však
nežloutnou	žloutnout	k5eNaImIp3nP	žloutnout
<g/>
.	.	kIx.	.
</s>
<s>
Využívání	využívání	k1gNnSc1	využívání
hroších	hroší	k2eAgInPc2d1	hroší
klů	kel	k1gInPc2	kel
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
zákaz	zákaz	k1gInSc1	zákaz
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
slonovinou	slonovina	k1gFnSc7	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
z	z	k7c2	z
hroších	hroší	k2eAgInPc2d1	hroší
zubů	zub	k1gInPc2	zub
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
klávesy	klávesa	k1gFnPc1	klávesa
klavírů	klavír	k1gInPc2	klavír
nebo	nebo	k8xC	nebo
zubní	zubní	k2eAgFnSc2d1	zubní
protézy	protéza	k1gFnSc2	protéza
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
hrochovi	hroch	k1gMnSc3	hroch
ambivalentní	ambivalentní	k2eAgInSc4d1	ambivalentní
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
uctívali	uctívat	k5eAaImAgMnP	uctívat
bohyni	bohyně	k1gFnSc4	bohyně
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
ochránkyni	ochránkyně	k1gFnSc4	ochránkyně
žen	žena	k1gFnPc2	žena
Tveret	Tveret	k1gInSc4	Tveret
s	s	k7c7	s
hroší	hroší	k2eAgFnSc7d1	hroší
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
hroch	hroch	k1gMnSc1	hroch
však	však	k9	však
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
spojován	spojován	k2eAgMnSc1d1	spojován
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Sutechem	Sutech	k1gMnSc7	Sutech
a	a	k8xC	a
silami	síla	k1gFnPc7	síla
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Amemait	Amemait	k1gInSc1	Amemait
neboli	neboli	k8xC	neboli
Velká	velký	k2eAgFnSc1d1	velká
požíračka	požíračka	k1gFnSc1	požíračka
<g/>
,	,	kIx,	,
příšera	příšera	k1gFnSc1	příšera
ze	z	k7c2	z
staroegyptského	staroegyptský	k2eAgNnSc2d1	staroegyptské
záhrobí	záhrobí	k1gNnSc2	záhrobí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
polykala	polykat	k5eAaImAgFnS	polykat
nesmrtelné	smrtelný	k2eNgFnPc4d1	nesmrtelná
duše	duše	k1gFnPc4	duše
provinilých	provinilý	k2eAgMnPc2d1	provinilý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
hroší	hroší	k2eAgNnSc4d1	hroší
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
lví	lví	k2eAgFnSc4d1	lví
hřívu	hříva	k1gFnSc4	hříva
a	a	k8xC	a
krokodýlí	krokodýlí	k2eAgFnSc4d1	krokodýlí
tlamu	tlama	k1gFnSc4	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Hroch	Hroch	k1gMnSc1	Hroch
byl	být	k5eAaImAgMnS	být
také	také	k9	také
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
lovnou	lovný	k2eAgFnSc7d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
Egypťanů	Egypťan	k1gMnPc2	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Dochovala	dochovat	k5eAaPmAgFnS	dochovat
se	se	k3xPyFc4	se
zobrazení	zobrazení	k1gNnSc2	zobrazení
egyptských	egyptský	k2eAgInPc2d1	egyptský
faraonů	faraon	k1gInPc2	faraon
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
hrocha	hroch	k1gMnSc2	hroch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
nepoměrně	poměrně	k6eNd1	poměrně
malý	malý	k2eAgInSc1d1	malý
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
faraonovi	faraon	k1gMnSc3	faraon
asi	asi	k9	asi
jako	jako	k8xS	jako
králík	králík	k1gMnSc1	králík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
netvor	netvor	k1gMnSc1	netvor
Behemot	behemot	k1gMnSc1	behemot
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
ztotožňován	ztotožňován	k2eAgMnSc1d1	ztotožňován
s	s	k7c7	s
hrochem	hroch	k1gMnSc7	hroch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Jób	Jób	k1gFnSc2	Jób
je	být	k5eAaImIp3nS	být
Behemot	behemot	k1gMnSc1	behemot
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xS	jako
ohromný	ohromný	k2eAgMnSc1d1	ohromný
vodní	vodní	k2eAgMnSc1d1	vodní
býložravec	býložravec	k1gMnSc1	býložravec
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
tlamou	tlama	k1gFnSc7	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
popis	popis	k1gInSc1	popis
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hrochovi	hroch	k1gMnSc3	hroch
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
biblický	biblický	k2eAgMnSc1d1	biblický
netvor	netvor	k1gMnSc1	netvor
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
špatný	špatný	k2eAgInSc4d1	špatný
popis	popis	k1gInSc4	popis
hrocha	hroch	k1gMnSc2	hroch
<g/>
,	,	kIx,	,
nějakého	nějaký	k3yIgMnSc4	nějaký
jiného	jiný	k2eAgMnSc4d1	jiný
tvora	tvor	k1gMnSc4	tvor
(	(	kIx(	(
<g/>
slon	slon	k1gMnSc1	slon
<g/>
,	,	kIx,	,
krokodýl	krokodýl	k1gMnSc1	krokodýl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
vymyšlené	vymyšlený	k2eAgNnSc4d1	vymyšlené
bájné	bájný	k2eAgNnSc4d1	bájné
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
hrocha	hroch	k1gMnSc4	hroch
odvozen	odvozen	k2eAgMnSc1d1	odvozen
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
Behemot	behemot	k1gMnSc1	behemot
<g/>
.	.	kIx.	.
</s>
<s>
Hroch	Hroch	k1gMnSc1	Hroch
byl	být	k5eAaImAgMnS	být
dobře	dobře	k6eAd1	dobře
znám	znám	k2eAgMnSc1d1	znám
i	i	k8xC	i
starým	starý	k2eAgMnPc3d1	starý
Řekům	Řek	k1gMnPc3	Řek
a	a	k8xC	a
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Psali	psát	k5eAaImAgMnP	psát
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
autoři	autor	k1gMnPc1	autor
jako	jako	k9	jako
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
,	,	kIx,	,
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
a	a	k8xC	a
Plinius	Plinius	k1gMnSc1	Plinius
Starší	starý	k2eAgMnPc1d2	starší
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
upoutal	upoutat	k5eAaPmAgInS	upoutat
svou	svůj	k3xOyFgFnSc7	svůj
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodně	mezinárodně	k6eAd1	mezinárodně
používaný	používaný	k2eAgInSc1d1	používaný
název	název	k1gInSc1	název
hippopotamus	hippopotamus	k1gInSc1	hippopotamus
je	být	k5eAaImIp3nS	být
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
říční	říční	k2eAgMnSc1d1	říční
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Římané	Říman	k1gMnPc1	Říman
hrocha	hroch	k1gMnSc4	hroch
zneužívali	zneužívat	k5eAaImAgMnP	zneužívat
ke	k	k7c3	k
krutým	krutý	k2eAgFnPc3d1	krutá
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
cirku	cirk	k1gInSc6	cirk
<g/>
.	.	kIx.	.
</s>
<s>
Idžové	Idžus	k1gMnPc1	Idžus
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Nigérie	Nigérie	k1gFnSc2	Nigérie
pokládají	pokládat	k5eAaImIp3nP	pokládat
hrocha	hroch	k1gMnSc4	hroch
za	za	k7c2	za
svého	svůj	k3xOyFgMnSc2	svůj
předka	předek	k1gMnSc2	předek
a	a	k8xC	a
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
nádherné	nádherný	k2eAgFnPc1d1	nádherná
hroší	hroší	k2eAgFnPc1d1	hroší
masky	maska	k1gFnPc1	maska
<g/>
.	.	kIx.	.
</s>
<s>
Jihoafričtí	jihoafrický	k2eAgMnPc1d1	jihoafrický
Zuluové	Zulu	k1gMnPc1	Zulu
pokládají	pokládat	k5eAaImIp3nP	pokládat
hrocha	hroch	k1gMnSc4	hroch
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Sanové	Sanus	k1gMnPc1	Sanus
si	se	k3xPyFc3	se
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
vysvětlující	vysvětlující	k2eAgInSc4d1	vysvětlující
hroší	hroší	k2eAgInSc4d1	hroší
zvyk	zvyk	k1gInSc4	zvyk
rozmetávat	rozmetávat	k5eAaImF	rozmetávat
trus	trus	k1gInSc4	trus
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Stvořitel	Stvořitel	k1gMnSc1	Stvořitel
prý	prý	k9	prý
nechtěl	chtít	k5eNaImAgInS	chtít
hrochovi	hroch	k1gMnSc3	hroch
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bál	bát	k5eAaImAgMnS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc7	svůj
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
tlamou	tlama	k1gFnSc7	tlama
spolyká	spolykat	k5eAaPmIp3nS	spolykat
všechny	všechen	k3xTgFnPc4	všechen
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
hroch	hroch	k1gMnSc1	hroch
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
navždy	navždy	k6eAd1	navždy
jíst	jíst	k5eAaImF	jíst
jen	jen	k9	jen
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
dovolil	dovolit	k5eAaPmAgMnS	dovolit
mu	on	k3xPp3gMnSc3	on
stvořitel	stvořitel	k1gMnSc1	stvořitel
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hoch	hoch	k1gMnSc1	hoch
vždy	vždy	k6eAd1	vždy
rozmetá	rozmetat	k5eAaImIp3nS	rozmetat
svůj	svůj	k3xOyFgInSc4	svůj
trus	trus	k1gInSc4	trus
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
stvořitel	stvořitel	k1gMnSc1	stvořitel
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nejsou	být	k5eNaImIp3nP	být
rybí	rybí	k2eAgFnPc4d1	rybí
kostičky	kostička	k1gFnPc4	kostička
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
hrochů	hroch	k1gMnPc2	hroch
měl	mít	k5eAaImAgInS	mít
pro	pro	k7c4	pro
některá	některý	k3yIgNnPc4	některý
africká	africký	k2eAgNnPc4d1	africké
etnika	etnikum	k1gNnPc4	etnikum
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lidé	člověk	k1gMnPc1	člověk
skupiny	skupina	k1gFnSc2	skupina
Weyto	Weyto	k1gNnSc1	Weyto
od	od	k7c2	od
etiopského	etiopský	k2eAgNnSc2d1	etiopské
jezera	jezero	k1gNnSc2	jezero
Tana	tanout	k5eAaImSgInS	tanout
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zcela	zcela	k6eAd1	zcela
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
hrochů	hroch	k1gMnPc2	hroch
a	a	k8xC	a
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
vědců	vědec	k1gMnPc2	vědec
je	být	k5eAaImIp3nS	být
hroch	hroch	k1gMnSc1	hroch
perspektivním	perspektivní	k2eAgNnSc7d1	perspektivní
zvířetem	zvíře	k1gNnSc7	zvíře
pro	pro	k7c4	pro
domestikaci	domestikace	k1gFnSc4	domestikace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rychlých	rychlý	k2eAgInPc2d1	rychlý
váhových	váhový	k2eAgInPc2d1	váhový
přírůstků	přírůstek	k1gInPc2	přírůstek
při	při	k7c6	při
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgFnSc6d1	malá
spotřebě	spotřeba	k1gFnSc6	spotřeba
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
přitom	přitom	k6eAd1	přitom
využívat	využívat	k5eAaImF	využívat
značně	značně	k6eAd1	značně
nekvalitní	kvalitní	k2eNgFnSc4d1	nekvalitní
a	a	k8xC	a
hrubou	hrubý	k2eAgFnSc4d1	hrubá
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
výtečné	výtečný	k2eAgNnSc1d1	výtečné
maso	maso	k1gNnSc1	maso
i	i	k8xC	i
dostatek	dostatek	k1gInSc1	dostatek
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Chovu	chov	k1gInSc3	chov
hrochů	hroch	k1gMnPc2	hroch
brání	bránit	k5eAaImIp3nS	bránit
jejich	jejich	k3xOp3gFnSc1	jejich
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
neovladatelnost	neovladatelnost	k1gFnSc1	neovladatelnost
a	a	k8xC	a
agresivita	agresivita	k1gFnSc1	agresivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
hrochů	hroch	k1gMnPc2	hroch
vysazeno	vysadit	k5eAaPmNgNnS	vysadit
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Magdaleny	Magdalena	k1gFnSc2	Magdalena
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
produkce	produkce	k1gFnSc2	produkce
masa	maso	k1gNnSc2	maso
i	i	k8xC	i
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
invazi	invaze	k1gFnSc3	invaze
vodního	vodní	k2eAgInSc2d1	vodní
hyacintu	hyacint	k1gInSc2	hyacint
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
zabití	zabití	k1gNnPc2	zabití
a	a	k8xC	a
zranění	zranění	k1gNnPc2	zranění
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
hroši	hroch	k1gMnPc1	hroch
nejnebezpečnější	bezpečný	k2eNgMnPc1d3	nejnebezpečnější
savci	savec	k1gMnPc1	savec
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
ročně	ročně	k6eAd1	ročně
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
než	než	k8xS	než
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
levharti	levhart	k1gMnPc1	levhart
<g/>
,	,	kIx,	,
sloni	slon	k1gMnPc1	slon
či	či	k8xC	či
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
přivezen	přivezen	k2eAgInSc4d1	přivezen
slavný	slavný	k2eAgInSc4d1	slavný
Obaysch	Obaysch	k1gInSc4	Obaysch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hroch	hroch	k1gMnSc1	hroch
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
chovancem	chovanec	k1gMnSc7	chovanec
mnoha	mnoho	k4c2	mnoho
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vhodné	vhodný	k2eAgFnSc6d1	vhodná
péči	péče	k1gFnSc6	péče
se	se	k3xPyFc4	se
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
snadno	snadno	k6eAd1	snadno
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hrochy	hroch	k1gMnPc4	hroch
chová	chovat	k5eAaImIp3nS	chovat
Zoo	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
hroch	hroch	k1gMnSc1	hroch
nově	nově	k6eAd1	nově
chová	chovat	k5eAaImIp3nS	chovat
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Dvorec	dvorec	k1gInSc4	dvorec
a	a	k8xC	a
k	k	k7c3	k
chovu	chov	k1gInSc3	chov
se	se	k3xPyFc4	se
také	také	k9	také
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
vrátila	vrátit	k5eAaPmAgFnS	vrátit
Zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
odchovanců	odchovanec	k1gMnPc2	odchovanec
hroch	hroch	k1gMnSc1	hroch
Davídek	Davídek	k1gMnSc1	Davídek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Dva	dva	k4xCgMnPc4	dva
lidi	člověk	k1gMnPc4	člověk
v	v	k7c4	v
zoo	zoo	k1gFnSc4	zoo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ke	k	k7c3	k
spatření	spatření	k1gNnSc3	spatření
v	v	k7c6	v
privátním	privátní	k2eAgInSc6d1	privátní
chovu	chov	k1gInSc6	chov
Ringelland	Ringellanda	k1gFnPc2	Ringellanda
u	u	k7c2	u
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
r.	r.	kA	r.
2016	[number]	k4	2016
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
stáří	stáří	k1gNnSc2	stáří
uhynul	uhynout	k5eAaPmAgMnS	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
zoo	zoo	k1gFnSc1	zoo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
pověřena	pověřit	k5eAaPmNgFnS	pověřit
vedením	vedení	k1gNnSc7	vedení
Evropské	evropský	k2eAgFnSc2d1	Evropská
plemenné	plemenný	k2eAgFnSc2d1	plemenná
knihy	kniha	k1gFnSc2	kniha
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
