<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
diecéze	diecéze	k1gFnSc1	diecéze
San	San	k1gFnSc2	San
Pedro	Pedro	k1gNnSc4	Pedro
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
území	území	k1gNnSc2	území
diecéze	diecéze	k1gFnSc2	diecéze
Concepción	Concepción	k1gInSc1	Concepción
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
.	.	kIx.	.
</s>
