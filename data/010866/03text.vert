<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
diecéze	diecéze	k1gFnSc1	diecéze
San	San	k1gFnSc2	San
Pedro	Pedro	k1gNnSc4	Pedro
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
území	území	k1gNnSc2	území
diecéze	diecéze	k1gFnSc2	diecéze
Concepción	Concepción	k1gInSc1	Concepción
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dřívějších	dřívější	k2eAgMnPc6d1	dřívější
správcích	správce	k1gMnPc6	správce
území	území	k1gNnSc6	území
diecéze	diecéze	k1gFnSc2	diecéze
viz	vidět	k5eAaImRp2nS	vidět
Seznam	seznam	k1gInSc4	seznam
biskupů	biskup	k1gMnPc2	biskup
diecéze	diecéze	k1gFnSc2	diecéze
Concepción	Concepción	k1gInSc1	Concepción
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kompletní	kompletní	k2eAgInSc1d1	kompletní
seznam	seznam	k1gInSc1	seznam
==	==	k?	==
</s>
</p>
<p>
<s>
Oscar	Oscar	k1gInSc1	Oscar
Páez	Páeza	k1gFnPc2	Páeza
Garcete	Garce	k1gNnSc2	Garce
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
biskup	biskup	k1gMnSc1	biskup
diecéze	diecéze	k1gFnSc2	diecéze
Alto	Alto	k1gMnSc1	Alto
Paraná	Paraná	k1gFnSc1	Paraná
</s>
</p>
<p>
<s>
Fernando	Fernando	k6eAd1	Fernando
Armindo	Armindo	k6eAd1	Armindo
Lugo	Lugo	k1gMnSc1	Lugo
Méndez	Méndez	k1gMnSc1	Méndez
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
V.D.	V.D.	k1gFnSc1	V.D.
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Paraguaye	Paraguay	k1gFnSc2	Paraguay
a	a	k8xC	a
laicizován	laicizován	k2eAgInSc1d1	laicizován
</s>
</p>
<p>
<s>
Adalberto	Adalberta	k1gFnSc5	Adalberta
Martínez	Martínez	k1gInSc4	Martínez
Flores	Flores	k1gInSc4	Flores
(	(	kIx(	(
<g/>
od	od	k7c2	od
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
