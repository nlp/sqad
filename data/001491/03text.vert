<s>
Lord	lord	k1gMnSc1	lord
Rayleigh	Rayleigh	k1gMnSc1	Rayleigh
-	-	kIx~	-
John	John	k1gMnSc1	John
William	William	k1gInSc4	William
Strutt	Strutt	k2eAgInSc4d1	Strutt
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Rayleigh	Rayleigh	k1gMnSc1	Rayleigh
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1842	[number]	k4	1842
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
anomálii	anomálie	k1gFnSc4	anomálie
hustoty	hustota	k1gFnSc2	hustota
dusíku	dusík	k1gInSc2	dusík
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
publikoval	publikovat	k5eAaBmAgMnS	publikovat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
přednáškách	přednáška	k1gFnPc6	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
anomálie	anomálie	k1gFnSc1	anomálie
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
Williama	William	k1gMnSc4	William
Ramsaye	Ramsay	k1gMnSc4	Ramsay
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rayleighem	Rayleigh	k1gInSc7	Rayleigh
objevil	objevit	k5eAaPmAgInS	objevit
argon	argon	k1gInSc1	argon
(	(	kIx(	(
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
také	také	k9	také
akustikou	akustika	k1gFnSc7	akustika
<g/>
,	,	kIx,	,
optickým	optický	k2eAgInSc7d1	optický
a	a	k8xC	a
elektromagnetickým	elektromagnetický	k2eAgInSc7d1	elektromagnetický
rozptylem	rozptyl	k1gInSc7	rozptyl
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
objevitelem	objevitel	k1gMnSc7	objevitel
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
vyzařovacích	vyzařovací	k2eAgInPc2d1	vyzařovací
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nobelovské	nobelovský	k2eAgFnSc6d1	nobelovská
přednášce	přednáška	k1gFnSc6	přednáška
Rayleigh	Rayleigh	k1gMnSc1	Rayleigh
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hustota	hustota	k1gFnSc1	hustota
plynů	plyn	k1gInPc2	plyn
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
mé	můj	k3xOp1gFnSc2	můj
pozornosti	pozornost	k1gFnSc2	pozornost
již	již	k6eAd1	již
před	před	k7c7	před
20	[number]	k4	20
lety	léto	k1gNnPc7	léto
<g/>
....	....	k?	....
Zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
jsme	být	k5eAaImIp1nP	být
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
dělal	dělat	k5eAaImAgMnS	dělat
jsem	být	k5eAaImIp1nS	být
sérii	série	k1gFnSc4	série
experimentů	experiment	k1gInPc2	experiment
<g/>
...	...	k?	...
Vzduch	vzduch	k1gInSc1	vzduch
bublal	bublat	k5eAaImAgInS	bublat
přes	přes	k7c4	přes
kapalný	kapalný	k2eAgInSc4d1	kapalný
amoniak	amoniak	k1gInSc4	amoniak
prošel	projít	k5eAaPmAgMnS	projít
trubkou	trubka	k1gFnSc7	trubka
obsahující	obsahující	k2eAgFnSc4d1	obsahující
měď	měď	k1gFnSc4	měď
rozžhavenou	rozžhavený	k2eAgFnSc4d1	rozžhavená
do	do	k7c2	do
rudého	rudý	k2eAgInSc2d1	rudý
žáru	žár	k1gInSc2	žár
<g />
.	.	kIx.	.
</s>
<s>
kde	kde	k6eAd1	kde
vzdušný	vzdušný	k2eAgInSc1d1	vzdušný
kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
pohlcen	pohltit	k5eAaPmNgInS	pohltit
vodíkem	vodík	k1gInSc7	vodík
ze	z	k7c2	z
čpavku	čpavek	k1gInSc2	čpavek
<g/>
,	,	kIx,	,
přemíra	přemíra	k1gFnSc1	přemíra
čpavku	čpavek	k1gInSc2	čpavek
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
odstranila	odstranit	k5eAaPmAgFnS	odstranit
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
....	....	k?	....
Udělal	udělat	k5eAaPmAgMnS	udělat
jsem	být	k5eAaImIp1nS	být
takto	takto	k6eAd1	takto
sérii	série	k1gFnSc4	série
souhlasných	souhlasný	k2eAgNnPc2d1	souhlasné
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
....	....	k?	....
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
Jsem	být	k5eAaImIp1nS	být
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
ortodoxní	ortodoxní	k2eAgFnSc4d1	ortodoxní
proceduru	procedura	k1gFnSc4	procedura
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
obešla	obejít	k5eAaPmAgFnS	obejít
bez	bez	k7c2	bez
průchodu	průchod	k1gInSc2	průchod
vzduchu	vzduch	k1gInSc2	vzduch
přímo	přímo	k6eAd1	přímo
přes	přes	k7c4	přes
červenou	červený	k2eAgFnSc4d1	červená
rozžhavenou	rozžhavený	k2eAgFnSc4d1	rozžhavená
měď	měď	k1gFnSc4	měď
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mému	můj	k3xOp1gNnSc3	můj
překvapení	překvapení	k1gNnSc3	překvapení
se	se	k3xPyFc4	se
výsledky	výsledek	k1gInPc7	výsledek
těch	ten	k3xDgFnPc2	ten
dvou	dva	k4xCgFnPc2	dva
metod	metoda	k1gFnPc2	metoda
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
o	o	k7c4	o
tisící	tisící	k4xOgFnSc4	tisící
část	část	k1gFnSc4	část
-	-	kIx~	-
rozdíl	rozdíl	k1gInSc4	rozdíl
malý	malý	k2eAgInSc4d1	malý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
experimentálními	experimentální	k2eAgFnPc7d1	experimentální
chybami	chyba	k1gFnPc7	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
tvořilo	tvořit	k5eAaImAgNnS	tvořit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
druhy	druh	k1gInPc7	druh
dusíku	dusík	k1gInSc2	dusík
<g/>
?	?	kIx.	?
</s>
<s>
.....	.....	k?	.....
<g/>
nový	nový	k2eAgInSc1d1	nový
plyn	plyn	k1gInSc1	plyn
<g/>
"	"	kIx"	"
Většina	většina	k1gFnSc1	většina
historiků	historik	k1gMnPc2	historik
a	a	k8xC	a
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
jeho	jeho	k3xOp3gFnSc4	jeho
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
důkladnost	důkladnost	k1gFnSc4	důkladnost
a	a	k8xC	a
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Strutt	Strutt	k1gInSc1	Strutt
členem	člen	k1gInSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885-1896	[number]	k4	1885-1896
jako	jako	k8xS	jako
tajemník	tajemník	k1gMnSc1	tajemník
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1905-1908	[number]	k4	1905-1908
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
laureátem	laureát	k1gMnSc7	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
izolování	izolování	k1gNnSc4	izolování
inertního	inertní	k2eAgInSc2d1	inertní
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
argonu	argon	k1gInSc2	argon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Ondřeje	Ondřej	k1gMnSc2	Ondřej
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
byla	být	k5eAaImAgFnS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1921	[number]	k4	1921
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
odhalena	odhalen	k2eAgFnSc1d1	odhalena
mramorová	mramorový	k2eAgFnSc1d1	mramorová
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Unerring	Unerring	k1gInSc1	Unerring
Leader	leader	k1gMnSc1	leader
in	in	k?	in
the	the	k?	the
Natural	Natural	k?	Natural
Knowledge	Knowledge	k1gInSc1	Knowledge
(	(	kIx(	(
<g/>
Neomylný	omylný	k2eNgMnSc1d1	neomylný
vůdce	vůdce	k1gMnSc1	vůdce
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
přírodního	přírodní	k2eAgNnSc2d1	přírodní
poznání	poznání	k1gNnSc2	poznání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
mladý	mladý	k1gMnSc1	mladý
J.	J.	kA	J.
W.	W.	kA	W.
Strutt	Struttum	k1gNnPc2	Struttum
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
do	do	k7c2	do
Cambridge	Cambridge	k1gFnSc2	Cambridge
na	na	k7c4	na
Trinity	Trinit	k2eAgFnPc4d1	Trinita
College	Colleg	k1gFnPc4	Colleg
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
zde	zde	k6eAd1	zde
získává	získávat	k5eAaImIp3nS	získávat
cenu	cena	k1gFnSc4	cena
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
studenta	student	k1gMnSc2	student
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
Smithovu	Smithův	k2eAgFnSc4d1	Smithova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc4d1	další
cambridgeské	cambridgeský	k2eAgNnSc4d1	cambridgeský
studentské	studentský	k2eAgNnSc4d1	studentské
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
také	také	k9	také
začíná	začínat	k5eAaImIp3nS	začínat
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
projevy	projev	k1gInPc4	projev
mediumity	mediumita	k1gFnPc1	mediumita
zvané	zvaný	k2eAgFnPc1d1	zvaná
dnes	dnes	k6eAd1	dnes
telekineze	telekineze	k1gFnPc4	telekineze
a	a	k8xC	a
jako	jako	k8xS	jako
matematik	matematik	k1gMnSc1	matematik
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
později	pozdě	k6eAd2	pozdě
vyhnout	vyhnout	k5eAaPmF	vyhnout
ani	ani	k8xC	ani
otázkám	otázka	k1gFnPc3	otázka
spojeným	spojený	k2eAgFnPc3d1	spojená
se	se	k3xPyFc4	se
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
dimenzí	dimenze	k1gFnSc7	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
metapsychické	metapsychický	k2eAgInPc4d1	metapsychický
jevy	jev	k1gInPc4	jev
pak	pak	k6eAd1	pak
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Evelyn	Evelyn	k1gNnSc1	Evelyn
Balfourovou	Balfourův	k2eAgFnSc7d1	Balfourova
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
bratrem	bratr	k1gMnSc7	bratr
Arthurem	Arthur	k1gMnSc7	Arthur
Jamesem	James	k1gMnSc7	James
Balfourem	Balfour	k1gMnSc7	Balfour
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poznal	poznat	k5eAaPmAgInS	poznat
na	na	k7c6	na
studiích	studio	k1gNnPc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
Strutt	Strutt	k1gInSc1	Strutt
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
sňatku	sňatek	k1gInSc3	sňatek
přichází	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
něho	on	k3xPp3gInSc4	on
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
se	s	k7c7	s
švagrem	švagr	k1gMnSc7	švagr
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Henrym	Henry	k1gMnSc7	Henry
Sidgwickem	Sidgwick	k1gInSc7	Sidgwick
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
psychický	psychický	k2eAgInSc4d1	psychický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
r.	r.	kA	r.
1919	[number]	k4	1919
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
coby	coby	k?	coby
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
člen	člen	k1gMnSc1	člen
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jejím	její	k3xOp3gMnSc7	její
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
kancléř	kancléř	k1gMnSc1	kancléř
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
)	)	kIx)	)
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
John	John	k1gMnSc1	John
William	William	k1gInSc4	William
Strutt	Strutt	k2eAgInSc4d1	Strutt
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Rayleigh	Rayleigh	k1gMnSc1	Rayleigh
<g/>
.	.	kIx.	.
</s>
<s>
Zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
neobjasněné	objasněný	k2eNgInPc4d1	neobjasněný
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zabývala	zabývat	k5eAaImAgFnS	zabývat
tzv.	tzv.	kA	tzv.
věda	věda	k1gFnSc1	věda
metapsychická	metapsychický	k2eAgFnSc1d1	metapsychická
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Arthur	Arthur	k1gMnSc1	Arthur
Balfour	Balfour	k1gMnSc1	Balfour
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1902-1905	[number]	k4	1902-1905
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
státnické	státnický	k2eAgFnSc2d1	státnická
funkce	funkce	k1gFnSc2	funkce
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
Society	societa	k1gFnSc2	societa
for	forum	k1gNnPc2	forum
Psychical	Psychical	k1gMnSc1	Psychical
Research	Research	k1gMnSc1	Research
<g/>
.	.	kIx.	.
</s>
<s>
Eleanora	Eleanora	k1gFnSc1	Eleanora
Sidgwicková	Sidgwicková	k1gFnSc1	Sidgwicková
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
sestra	sestra	k1gFnSc1	sestra
Struttovy	Struttův	k2eAgFnSc2d1	Struttův
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
také	také	k9	také
zvolena	zvolit	k5eAaPmNgFnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Robert	Robert	k1gMnSc1	Robert
John	John	k1gMnSc1	John
Strutt	Strutt	k1gMnSc1	Strutt
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Rayleigh	Rayleigh	k1gMnSc1	Rayleigh
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šel	jít	k5eAaImAgMnS	jít
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937-1938	[number]	k4	1937-1938
se	se	k3xPyFc4	se
v	v	k7c4	v
Society	societa	k1gFnPc4	societa
for	forum	k1gNnPc2	forum
Psychical	Psychical	k1gFnPc2	Psychical
Research	Researcha	k1gFnPc2	Researcha
též	též	k9	též
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
rayleigh	rayleigh	k1gMnSc1	rayleigh
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
Rayleighův	Rayleighův	k2eAgInSc1d1	Rayleighův
rozptyl	rozptyl	k1gInSc1	rozptyl
Rayleighův-Jeansův	Rayleighův-Jeansův	k2eAgInSc4d1	Rayleighův-Jeansův
zákon	zákon	k1gInSc4	zákon
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lord	lord	k1gMnSc1	lord
Rayleigh	Rayleigha	k1gFnPc2	Rayleigha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
John	John	k1gMnSc1	John
William	William	k1gInSc4	William
Strutt	Strutt	k2eAgInSc4d1	Strutt
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
baron	baron	k1gMnSc1	baron
Rayleigh	Rayleigh	k1gMnSc1	Rayleigh
nobelprize	nobelprize	k1gFnSc1	nobelprize
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
