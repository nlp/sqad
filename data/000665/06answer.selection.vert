<s>
Margaret	Margareta	k1gFnPc2	Margareta
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
malých	malý	k2eAgInPc2d1	malý
vnějších	vnější	k2eAgInPc2d1	vnější
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gMnSc1	Uran
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
má	mít	k5eAaImIp3nS	mít
prográdní	prográdní	k2eAgFnPc4d1	prográdní
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
