<s>
Margaret	Margareta	k1gFnPc2	Margareta
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
malých	malý	k2eAgInPc2d1	malý
vnějších	vnější	k2eAgInPc2d1	vnější
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gMnSc1	Uran
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
má	mít	k5eAaImIp3nS	mít
prográdní	prográdní	k2eAgFnPc4d1	prográdní
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
14	[number]	k4	14
345	[number]	k4	345
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
20	[number]	k4	20
km	km	kA	km
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
cca	cca	kA	cca
~	~	kIx~	~
<g/>
1,0	[number]	k4	1,0
<g/>
×	×	k?	×
<g/>
1015	[number]	k4	1015
kg	kg	kA	kg
<g/>
,	,	kIx,	,
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
1687,01	[number]	k4	1687,01
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
astronomy	astronom	k1gMnPc7	astronom
Scottem	Scott	k1gMnSc7	Scott
Sheppardem	Sheppard	k1gMnSc7	Sheppard
a	a	k8xC	a
Davidem	David	k1gMnSc7	David
Jewittem	Jewitt	k1gMnSc7	Jewitt
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
označení	označení	k1gNnSc1	označení
měsíce	měsíc	k1gInSc2	měsíc
bylo	být	k5eAaImAgNnS	být
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
U	u	k7c2	u
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k1gNnSc4	ostatní
měsíce	měsíc	k1gInSc2	měsíc
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
i	i	k9	i
Margaret	Margareta	k1gFnPc2	Margareta
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
díla	dílo	k1gNnSc2	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Mnoho	mnoho	k6eAd1	mnoho
povyků	povyk	k1gInPc2	povyk
pro	pro	k7c4	pro
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
</s>
