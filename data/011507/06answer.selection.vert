<s>
William	William	k1gInSc1	William
"	"	kIx"	"
<g/>
Bill	Bill	k1gMnSc1	Bill
<g/>
"	"	kIx"	"
Jefferson	Jefferson	k1gMnSc1	Jefferson
Clinton	Clinton	k1gMnSc1	Clinton
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1946	[number]	k4	1946
v	v	k7c6	v
Hope	Hope	k1gFnSc6	Hope
<g/>
,	,	kIx,	,
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
William	William	k1gInSc1	William
Jefferson	Jefferson	k1gInSc1	Jefferson
Blythe	Blythe	k1gInSc4	Blythe
III	III	kA	III
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
demokratický	demokratický	k2eAgMnSc1d1	demokratický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
42	[number]	k4	42
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
proces	proces	k1gInSc4	proces
impeachmentu	impeachment	k1gInSc2	impeachment
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
odvolání	odvolání	k1gNnSc4	odvolání
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
