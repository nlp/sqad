<s>
William	William	k1gInSc1	William
"	"	kIx"	"
<g/>
Bill	Bill	k1gMnSc1	Bill
<g/>
"	"	kIx"	"
Jefferson	Jefferson	k1gMnSc1	Jefferson
Clinton	Clinton	k1gMnSc1	Clinton
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1946	[number]	k4	1946
v	v	k7c6	v
Hope	Hope	k1gFnSc6	Hope
<g/>
,	,	kIx,	,
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
William	William	k1gInSc1	William
Jefferson	Jefferson	k1gInSc1	Jefferson
Blythe	Blythe	k1gInSc4	Blythe
III	III	kA	III
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
demokratický	demokratický	k2eAgMnSc1d1	demokratický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
42	[number]	k4	42
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
proces	proces	k1gInSc4	proces
impeachmentu	impeachment	k1gInSc2	impeachment
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
odvolání	odvolání	k1gNnSc4	odvolání
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
administrativa	administrativa	k1gFnSc1	administrativa
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
guvernéra	guvernér	k1gMnSc2	guvernér
Arkansasu	Arkansas	k1gInSc2	Arkansas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
politické	politický	k2eAgFnSc2d1	politická
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
organizace	organizace	k1gFnSc2	organizace
Chlapecký	chlapecký	k2eAgInSc1d1	chlapecký
národ	národ	k1gInSc1	národ
(	(	kIx(	(
<g/>
Boys	boy	k1gMnPc1	boy
Nation	Nation	k1gInSc1	Nation
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
J.	J.	kA	J.
F.	F.	kA	F.
Kennedym	Kennedym	k1gInSc1	Kennedym
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
voleným	volený	k2eAgMnSc7d1	volený
politickým	politický	k2eAgMnSc7d1	politický
funkcionářem	funkcionář	k1gMnSc7	funkcionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
diplomatická	diplomatický	k2eAgNnPc4d1	diplomatické
studia	studio	k1gNnPc4	studio
na	na	k7c4	na
Georgetownskou	Georgetownský	k2eAgFnSc4d1	Georgetownský
univerzitu	univerzita	k1gFnSc4	univerzita
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
guvernérské	guvernérský	k2eAgFnSc6d1	guvernérská
kampani	kampaň	k1gFnSc6	kampaň
demokrata	demokrat	k1gMnSc2	demokrat
Holta	Holt	k1gMnSc2	Holt
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
si	se	k3xPyFc3	se
přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
jako	jako	k9	jako
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
senátora	senátor	k1gMnSc2	senátor
Fulbrighta	Fulbright	k1gInSc2	Fulbright
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
získal	získat	k5eAaPmAgMnS	získat
Rhodesovo	Rhodesův	k2eAgNnSc4d1	Rhodesův
stipendium	stipendium	k1gNnSc4	stipendium
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
předvolebních	předvolební	k2eAgFnPc6d1	předvolební
kampaních	kampaň	k1gFnPc6	kampaň
demokratických	demokratický	k2eAgMnPc2d1	demokratický
kandidátů	kandidát	k1gMnPc2	kandidát
(	(	kIx(	(
<g/>
Fulbright	Fulbright	k1gMnSc1	Fulbright
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
Oxfordu	Oxford	k1gInSc6	Oxford
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
na	na	k7c4	na
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
Yale	Yale	k1gFnSc6	Yale
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
manželkou	manželka	k1gFnSc7	manželka
Hillary	Hillara	k1gFnSc2	Hillara
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
kampani	kampaň	k1gFnSc6	kampaň
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
George	Georg	k1gMnSc2	Georg
McGoverna	McGoverna	k1gFnSc1	McGoverna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
přednášel	přednášet	k5eAaImAgInS	přednášet
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
Arkansaské	arkansaský	k2eAgFnSc6d1	Arkansaská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
vrchním	vrchní	k2eAgMnSc7d1	vrchní
státním	státní	k2eAgMnSc7d1	státní
zástupcem	zástupce	k1gMnSc7	zástupce
v	v	k7c6	v
Arkansasu	Arkansas	k1gInSc6	Arkansas
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
poprvé	poprvé	k6eAd1	poprvé
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
stál	stát	k5eAaImAgInS	stát
se	se	k3xPyFc4	se
arkansaským	arkansaský	k2eAgMnSc7d1	arkansaský
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
,	,	kIx,	,
po	po	k7c6	po
sérii	série	k1gFnSc6	série
nepopulárních	populární	k2eNgInPc2d1	nepopulární
kroků	krok	k1gInPc2	krok
prohrál	prohrát	k5eAaPmAgInS	prohrát
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
guvernéra	guvernér	k1gMnSc2	guvernér
Arkansasu	Arkansas	k1gInSc2	Arkansas
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc2	zvolení
42	[number]	k4	42
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
reformovat	reformovat	k5eAaBmF	reformovat
zaostalé	zaostalý	k2eAgNnSc4d1	zaostalé
školství	školství	k1gNnSc4	školství
a	a	k8xC	a
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
a	a	k8xC	a
vyrovnaný	vyrovnaný	k2eAgInSc4d1	vyrovnaný
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
reformách	reforma	k1gFnPc6	reforma
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prezidentování	prezidentování	k1gNnSc2	prezidentování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
následovníkem	následovník	k1gMnSc7	následovník
George	George	k1gNnSc2	George
H.	H.	kA	H.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
staršího	starší	k1gMnSc2	starší
a	a	k8xC	a
předchůdcem	předchůdce	k1gMnSc7	předchůdce
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
mladšího	mladý	k2eAgMnSc2d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kladům	klad	k1gInPc3	klad
jeho	jeho	k3xOp3gNnSc3	jeho
úřadování	úřadování	k1gNnSc3	úřadování
můžeme	moct	k5eAaImIp1nP	moct
přičíst	přičíst	k5eAaPmF	přičíst
Dohodu	dohoda	k1gFnSc4	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
coby	coby	k?	coby
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
mírové	mírový	k2eAgNnSc4d1	Mírové
řešení	řešení	k1gNnSc4	řešení
izraelsko-palestinského	izraelskoalestinský	k2eAgInSc2d1	izraelsko-palestinský
konfliktu	konflikt	k1gInSc2	konflikt
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
nebo	nebo	k8xC	nebo
zmírnění	zmírnění	k1gNnSc4	zmírnění
amerického	americký	k2eAgInSc2d1	americký
národního	národní	k2eAgInSc2d1	národní
dluhu	dluh	k1gInSc2	dluh
z	z	k7c2	z
období	období	k1gNnSc2	období
jeho	jeho	k3xOp3gMnPc2	jeho
dvou	dva	k4xCgMnPc2	dva
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
demokratických	demokratický	k2eAgFnPc6d1	demokratická
primárkách	primárky	k1gFnPc6	primárky
1992	[number]	k4	1992
obdržel	obdržet	k5eAaPmAgInS	obdržet
celkově	celkově	k6eAd1	celkově
52	[number]	k4	52
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
za	za	k7c2	za
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
si	se	k3xPyFc3	se
proti	proti	k7c3	proti
zvyklostem	zvyklost	k1gFnPc3	zvyklost
(	(	kIx(	(
<g/>
sousední	sousední	k2eAgInSc1d1	sousední
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgInSc1d2	nižší
věk	věk	k1gInSc1	věk
<g/>
)	)	kIx)	)
vybral	vybrat	k5eAaPmAgMnS	vybrat
Ala	ala	k1gFnSc1	ala
Gora	Gor	k2eAgFnSc1d1	Gora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
amerického	americký	k2eAgInSc2d1	americký
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgMnS	podporovat
menšinami	menšina	k1gFnPc7	menšina
a	a	k8xC	a
střední	střední	k2eAgFnSc7d1	střední
třídou	třída	k1gFnSc7	třída
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc4	volba
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
43	[number]	k4	43
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
ku	k	k7c3	k
37,4	[number]	k4	37,4
%	%	kIx~	%
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
volitelů	volitel	k1gMnPc2	volitel
byl	být	k5eAaImAgInS	být
poměr	poměr	k1gInSc1	poměr
370	[number]	k4	370
ku	k	k7c3	k
168	[number]	k4	168
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
podporoval	podporovat	k5eAaImAgMnS	podporovat
Borise	Boris	k1gMnSc4	Boris
Jelcina	Jelcin	k1gMnSc4	Jelcin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
snahu	snaha	k1gFnSc4	snaha
demokratizovat	demokratizovat	k5eAaBmF	demokratizovat
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
osvobození	osvobození	k1gNnSc4	osvobození
Haiti	Haiti	k1gNnSc2	Haiti
od	od	k7c2	od
diktátora	diktátor	k1gMnSc4	diktátor
Raoula	Raoul	k1gMnSc2	Raoul
Cédrase	Cédrasa	k1gFnSc6	Cédrasa
<g/>
.	.	kIx.	.
</s>
<s>
Snahu	snaha	k1gFnSc4	snaha
plnit	plnit	k5eAaImF	plnit
předvolební	předvolební	k2eAgInSc4d1	předvolební
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
daní	daň	k1gFnPc2	daň
střední	střední	k2eAgFnSc3d1	střední
vrstvě	vrstva	k1gFnSc3	vrstva
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
odložit	odložit	k5eAaPmF	odložit
kvůli	kvůli	k7c3	kvůli
velmi	velmi	k6eAd1	velmi
vysokému	vysoký	k2eAgInSc3d1	vysoký
deficitu	deficit	k1gInSc3	deficit
rozpočtu	rozpočet	k1gInSc2	rozpočet
připravovaného	připravovaný	k2eAgInSc2d1	připravovaný
George	Georg	k1gInSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bushem	Bush	k1gMnSc7	Bush
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tak	tak	k9	tak
nucen	nutit	k5eAaImNgMnS	nutit
přepracovat	přepracovat	k5eAaPmF	přepracovat
svoji	svůj	k3xOyFgFnSc4	svůj
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
reformu	reforma	k1gFnSc4	reforma
a	a	k8xC	a
zvýšit	zvýšit	k5eAaPmF	zvýšit
daně	daň	k1gFnPc4	daň
nejbohatším	bohatý	k2eAgMnPc3d3	nejbohatší
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
firmám	firma	k1gFnPc3	firma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
intervencích	intervence	k1gFnPc6	intervence
Ala	ala	k1gFnSc1	ala
Gorea	Gorea	k1gFnSc1	Gorea
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
energetická	energetický	k2eAgFnSc1d1	energetická
daň	daň	k1gFnSc1	daň
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgInS	představit
reformu	reforma	k1gFnSc4	reforma
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
by	by	kYmCp3nS	by
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
občané	občan	k1gMnPc1	občan
povinné	povinný	k2eAgNnSc4d1	povinné
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
však	však	k9	však
nevstoupila	vstoupit	k5eNaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
kvůli	kvůli	k7c3	kvůli
nevýhodnosti	nevýhodnost	k1gFnSc3	nevýhodnost
pro	pro	k7c4	pro
pojišťovny	pojišťovna	k1gFnPc4	pojišťovna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
bránily	bránit	k5eAaImAgFnP	bránit
silnou	silný	k2eAgFnSc7d1	silná
protikampaní	protikampaný	k2eAgMnPc1d1	protikampaný
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zavedl	zavést	k5eAaPmAgMnS	zavést
mnoho	mnoho	k4c4	mnoho
reforem	reforma	k1gFnPc2	reforma
a	a	k8xC	a
zlepšení	zlepšení	k1gNnPc2	zlepšení
-	-	kIx~	-
regulace	regulace	k1gFnSc1	regulace
držení	držení	k1gNnSc2	držení
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
místo	místo	k7c2	místo
nástupu	nástup	k1gInSc2	nástup
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
vykonávat	vykonávat	k5eAaImF	vykonávat
dobrovolnickou	dobrovolnický	k2eAgFnSc4d1	dobrovolnická
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc4	omezení
státní	státní	k2eAgFnSc2d1	státní
byrokracie	byrokracie	k1gFnSc2	byrokracie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velkých	velký	k2eAgInPc6d1	velký
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
Kongresem	kongres	k1gInSc7	kongres
o	o	k7c4	o
rozpočet	rozpočet	k1gInSc4	rozpočet
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1995	[number]	k4	1995
situace	situace	k1gFnSc1	situace
dospěla	dochvít	k5eAaPmAgFnS	dochvít
až	až	k9	až
k	k	k7c3	k
rozpočtovému	rozpočtový	k2eAgNnSc3d1	rozpočtové
provizoriu	provizorium	k1gNnSc3	provizorium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozpočet	rozpočet	k1gInSc4	rozpočet
povedlo	povést	k5eAaPmAgNnS	povést
schválit	schválit	k5eAaPmF	schválit
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
revidovala	revidovat	k5eAaImAgFnS	revidovat
Community	Communit	k2eAgInPc4d1	Communit
Reinvestment	Reinvestment	k1gInSc4	Reinvestment
Act	Act	k1gFnSc2	Act
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
kritéria	kritérion	k1gNnPc1	kritérion
uplatňovala	uplatňovat	k5eAaImAgNnP	uplatňovat
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
bankovních	bankovní	k2eAgFnPc2d1	bankovní
fúzí	fúze	k1gFnPc2	fúze
a	a	k8xC	a
akvizic	akvizice	k1gFnPc2	akvizice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
politický	politický	k2eAgInSc1d1	politický
zásah	zásah	k1gInSc1	zásah
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
posuzuvání	posuzuvání	k1gNnSc4	posuzuvání
rizikovosti	rizikovost	k1gFnSc2	rizikovost
hypoték	hypotéka	k1gFnPc2	hypotéka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
hypotéční	hypotéční	k2eAgFnSc2d1	hypotéční
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Další	další	k2eAgInSc1d1	další
jeho	jeho	k3xOp3gInSc1	jeho
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
výnos	výnos	k1gInSc1	výnos
<g/>
,	,	kIx,	,
Telecommunications	Telecommunications	k1gInSc1	Telecommunications
Act	Act	k1gFnSc2	Act
of	of	k?	of
1996	[number]	k4	1996
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
nejpodstatnější	podstatný	k2eAgFnPc4d3	nejpodstatnější
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
a	a	k8xC	a
médií	médium	k1gNnPc2	médium
v	v	k7c6	v
USA	USA	kA	USA
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
1996	[number]	k4	1996
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
postavil	postavit	k5eAaPmAgMnS	postavit
republikán	republikán	k1gMnSc1	republikán
Bob	Bob	k1gMnSc1	Bob
Dole	dole	k6eAd1	dole
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
negativní	negativní	k2eAgFnSc4d1	negativní
kampaň	kampaň	k1gFnSc4	kampaň
Clinton	Clinton	k1gMnSc1	Clinton
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
49	[number]	k4	49
<g/>
%	%	kIx~	%
:	:	kIx,	:
41	[number]	k4	41
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
379	[number]	k4	379
:	:	kIx,	:
159	[number]	k4	159
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
druhém	druhý	k4xOgNnSc6	druhý
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
zlepšil	zlepšit	k5eAaPmAgInS	zlepšit
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
republikány	republikán	k1gMnPc7	republikán
<g/>
,	,	kIx,	,
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
jeho	jeho	k3xOp3gFnSc2	jeho
reformy	reforma	k1gFnSc2	reforma
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
nových	nový	k2eAgFnPc2d1	nová
pracovních	pracovní	k2eAgFnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
iniciativě	iniciativa	k1gFnSc3	iniciativa
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
se	se	k3xPyFc4	se
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
G7	G7	k1gFnSc2	G7
stala	stát	k5eAaPmAgFnS	stát
G8	G8	k1gFnSc1	G8
díky	díky	k7c3	díky
připojení	připojení	k1gNnSc3	připojení
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
reformy	reforma	k1gFnPc1	reforma
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
vyrovnaným	vyrovnaný	k2eAgInPc3d1	vyrovnaný
či	či	k8xC	či
přebytkovým	přebytkový	k2eAgInPc3d1	přebytkový
rozpočtům	rozpočet	k1gInPc3	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
druhé	druhý	k4xOgNnSc4	druhý
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
aféra	aféra	k1gFnSc1	aféra
s	s	k7c7	s
Monikou	Monika	k1gFnSc7	Monika
Lewinskou	Lewinská	k1gFnSc7	Lewinská
-	-	kIx~	-
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
mít	mít	k5eAaImF	mít
celkem	celkem	k6eAd1	celkem
devětkrát	devětkrát	k6eAd1	devětkrát
orální	orální	k2eAgInSc4d1	orální
sex-	sex-	k?	sex-
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gNnSc3	on
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
hrozilo	hrozit	k5eAaImAgNnS	hrozit
odvolání	odvolání	k1gNnSc1	odvolání
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
proces	proces	k1gInSc4	proces
zvaný	zvaný	k2eAgInSc4d1	zvaný
impeachment	impeachment	k1gInSc4	impeachment
(	(	kIx(	(
<g/>
obžaloba	obžaloba	k1gFnSc1	obžaloba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalších	další	k2eAgFnPc6d1	další
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
podporoval	podporovat	k5eAaImAgInS	podporovat
svého	svůj	k3xOyFgMnSc4	svůj
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
Ala	ala	k1gFnSc1	ala
Gora	Gora	k1gFnSc1	Gora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
předal	předat	k5eAaPmAgInS	předat
úřad	úřad	k1gInSc1	úřad
G.	G.	kA	G.
W.	W.	kA	W.
Bushovi	Bush	k1gMnSc3	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
založil	založit	k5eAaPmAgMnS	založit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domovském	domovský	k2eAgInSc6d1	domovský
státě	stát	k1gInSc6	stát
(	(	kIx(	(
<g/>
Little	Little	k1gFnSc1	Little
Rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
)	)	kIx)	)
svou	svůj	k3xOyFgFnSc4	svůj
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
knihovnu	knihovna	k1gFnSc4	knihovna
a	a	k8xC	a
nadaci	nadace	k1gFnSc4	nadace
pomáhající	pomáhající	k2eAgFnPc4d1	pomáhající
překonat	překonat	k5eAaPmF	překonat
rasové	rasový	k2eAgInPc4d1	rasový
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
rozdíly	rozdíl	k1gInPc4	rozdíl
a	a	k8xC	a
podporující	podporující	k2eAgFnSc4d1	podporující
distribuci	distribuce	k1gFnSc4	distribuce
léků	lék	k1gInPc2	lék
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
školství	školství	k1gNnSc2	školství
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prezidentství	prezidentství	k1gNnPc2	prezidentství
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
vojenským	vojenský	k2eAgInPc3d1	vojenský
střetnutím	střetnutí	k1gNnSc7	střetnutí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vojenské	vojenský	k2eAgNnSc1d1	vojenské
střetnutí	střetnutí	k1gNnSc1	střetnutí
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
somálském	somálský	k2eAgNnSc6d1	somálské
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
během	během	k7c2	během
operace	operace	k1gFnSc2	operace
Gothic	Gothic	k1gMnSc1	Gothic
Serpent	Serpent	k1gMnSc1	Serpent
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zajmout	zajmout	k5eAaPmF	zajmout
Mohameda	Mohamed	k1gMnSc4	Mohamed
Farrah	Farrah	k1gMnSc1	Farrah
Aidida	Aidid	k1gMnSc4	Aidid
somálského	somálský	k2eAgMnSc4d1	somálský
vojenského	vojenský	k2eAgMnSc4d1	vojenský
velitele	velitel	k1gMnSc4	velitel
obviněného	obviněný	k1gMnSc2	obviněný
z	z	k7c2	z
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
operace	operace	k1gFnSc2	operace
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sestřelení	sestřelení	k1gNnSc2	sestřelení
dvou	dva	k4xCgInPc2	dva
amerických	americký	k2eAgInPc2d1	americký
vrtulníků	vrtulník	k1gInPc2	vrtulník
následná	následný	k2eAgFnSc1d1	následná
záchranná	záchranný	k2eAgFnSc1d1	záchranná
akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
městský	městský	k2eAgInSc4d1	městský
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
straně	strana	k1gFnSc6	strana
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
18	[number]	k4	18
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
73	[number]	k4	73
zraněných	zraněný	k1gMnPc2	zraněný
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
zajatého	zajatý	k1gMnSc2	zajatý
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
mezi	mezi	k7c7	mezi
somálskou	somálský	k2eAgFnSc7d1	Somálská
milicí	milice	k1gFnSc7	milice
a	a	k8xC	a
civilisty	civilista	k1gMnPc7	civilista
byly	být	k5eAaImAgFnP	být
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
byla	být	k5eAaImAgFnS	být
operace	operace	k1gFnSc1	operace
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
ze	z	k7c2	z
Somálska	Somálsko	k1gNnSc2	Somálsko
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
americké	americký	k2eAgNnSc1d1	americké
letectvo	letectvo	k1gNnSc1	letectvo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
NATO	NATO	kA	NATO
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
bombardování	bombardování	k1gNnSc1	bombardování
srbských	srbský	k2eAgFnPc2d1	Srbská
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zastavit	zastavit	k5eAaPmF	zastavit
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
bezpečné	bezpečný	k2eAgFnPc4d1	bezpečná
zóny	zóna	k1gFnPc4	zóna
OSN	OSN	kA	OSN
a	a	k8xC	a
tlačit	tlačit	k5eAaImF	tlačit
na	na	k7c4	na
uzavření	uzavření	k1gNnSc4	uzavření
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
Daytonské	daytonský	k2eAgFnSc2d1	Daytonská
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
vyslala	vyslat	k5eAaPmAgFnS	vyslat
Clintonova	Clintonův	k2eAgFnSc1d1	Clintonova
administrativa	administrativa	k1gFnSc1	administrativa
americké	americký	k2eAgMnPc4d1	americký
vojáky	voják	k1gMnPc4	voják
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mise	mise	k1gFnSc2	mise
ISAF	ISAF	kA	ISAF
do	do	k7c2	do
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
americké	americký	k2eAgFnPc4d1	americká
ambasády	ambasáda	k1gFnPc4	ambasáda
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
a	a	k8xC	a
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
desítky	desítka	k1gFnPc1	desítka
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Nařídil	nařídit	k5eAaPmAgMnS	nařídit
Clinton	Clinton	k1gMnSc1	Clinton
raketové	raketový	k2eAgInPc4d1	raketový
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
cílům	cíl	k1gInPc3	cíl
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
považovaných	považovaný	k2eAgInPc2d1	považovaný
za	za	k7c4	za
zařízení	zařízení	k1gNnSc4	zařízení
Al-Káidy	Al-Káida	k1gFnSc2	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
střelby	střelba	k1gFnSc2	střelba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
továrna	továrna	k1gFnSc1	továrna
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
mnohostranně	mnohostranně	k6eAd1	mnohostranně
kritizován	kritizovat	k5eAaImNgInS	kritizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
Clinton	Clinton	k1gMnSc1	Clinton
podepsal	podepsat	k5eAaPmAgMnS	podepsat
zákon	zákon	k1gInSc4	zákon
ustanovující	ustanovující	k2eAgFnSc4d1	ustanovující
politiku	politika	k1gFnSc4	politika
"	"	kIx"	"
<g/>
změny	změna	k1gFnSc2	změna
režimu	režim	k1gInSc2	režim
<g/>
"	"	kIx"	"
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
administrativa	administrativa	k1gFnSc1	administrativa
prezidenta	prezident	k1gMnSc2	prezident
Clintona	Clinton	k1gMnSc2	Clinton
autorizovala	autorizovat	k5eAaBmAgFnS	autorizovat
čtyřdenní	čtyřdenní	k2eAgFnSc1d1	čtyřdenní
bombardovací	bombardovací	k2eAgFnSc1d1	bombardovací
kampaň	kampaň	k1gFnSc1	kampaň
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
Operaci	operace	k1gFnSc4	operace
Pouštní	pouštní	k2eAgFnSc1d1	pouštní
liška	liška	k1gFnSc1	liška
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
americké	americký	k2eAgNnSc1d1	americké
letectvo	letectvo	k1gNnSc1	letectvo
terčem	terč	k1gInSc7	terč
protiletadlové	protiletadlový	k2eAgFnSc2d1	protiletadlová
palby	palba	k1gFnSc2	palba
v	v	k7c6	v
bezletových	bezletový	k2eAgFnPc6d1	bezletová
zónách	zóna	k1gFnPc6	zóna
nad	nad	k7c7	nad
Irákem	Irák	k1gInSc7	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Clintonova	Clintonův	k2eAgFnSc1d1	Clintonova
administrativa	administrativa	k1gFnSc1	administrativa
autorizovala	autorizovat	k5eAaBmAgFnS	autorizovat
použití	použití	k1gNnSc4	použití
síly	síla	k1gFnSc2	síla
proti	proti	k7c3	proti
bývalé	bývalý	k2eAgFnSc3d1	bývalá
Svazové	svazový	k2eAgFnSc3d1	svazová
republice	republika	k1gFnSc3	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
NATO	NATO	kA	NATO
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
kampaně	kampaň	k1gFnSc2	kampaň
bylo	být	k5eAaImAgNnS	být
zastavení	zastavení	k1gNnSc1	zastavení
etnických	etnický	k2eAgFnPc2d1	etnická
čištek	čištka	k1gFnPc2	čištka
a	a	k8xC	a
genocidy	genocida	k1gFnSc2	genocida
v	v	k7c6	v
jugoslávské	jugoslávský	k2eAgFnSc6d1	jugoslávská
provincii	provincie	k1gFnSc6	provincie
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
administrativa	administrativa	k1gFnSc1	administrativa
podvolila	podvolit	k5eAaPmAgFnS	podvolit
podmínkám	podmínka	k1gFnPc3	podmínka
rezoluce	rezoluce	k1gFnSc2	rezoluce
OSN	OSN	kA	OSN
číslo	číslo	k1gNnSc1	číslo
1244	[number]	k4	1244
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nad	nad	k7c7	nad
Kosovem	Kosovo	k1gNnSc7	Kosovo
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
správu	správa	k1gFnSc4	správa
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
a	a	k8xC	a
autorizovala	autorizovat	k5eAaBmAgFnS	autorizovat
jednotky	jednotka	k1gFnPc4	jednotka
KFOR	KFOR	kA	KFOR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
od	od	k7c2	od
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
navštívil	navštívit	k5eAaPmAgInS	navštívit
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Clinton	Clinton	k1gMnSc1	Clinton
se	se	k3xPyFc4	se
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prezidentství	prezidentství	k1gNnSc2	prezidentství
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
izraelsko-palestinského	izraelskoalestinský	k2eAgInSc2d1	izraelsko-palestinský
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
Jásirem	Jásir	k1gMnSc7	Jásir
Arafatem	Arafat	k1gMnSc7	Arafat
a	a	k8xC	a
Jicchakem	Jicchak	k1gMnSc7	Jicchak
Rabinem	Rabin	k1gMnSc7	Rabin
podepsána	podepsat	k5eAaPmNgFnS	podepsat
mírová	mírový	k2eAgFnSc1d1	mírová
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Dosažení	dosažení	k1gNnSc1	dosažení
míru	mír	k1gInSc2	mír
však	však	k9	však
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
Druhá	druhý	k4xOgFnSc1	druhý
intifáda	intifáda	k1gFnSc1	intifáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Aféra	aféra	k1gFnSc1	aféra
Chinagate	Chinagat	k1gInSc5	Chinagat
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
aféra	aféra	k1gFnSc1	aféra
"	"	kIx"	"
<g/>
Chinagate	Chinagat	k1gInSc5	Chinagat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
americkou	americký	k2eAgFnSc4d1	americká
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
zasíláním	zasílání	k1gNnSc7	zasílání
finančních	finanční	k2eAgInPc2d1	finanční
darů	dar	k1gInPc2	dar
volební	volební	k2eAgFnSc3d1	volební
kampani	kampaň	k1gFnSc3	kampaň
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
agenti	agent	k1gMnPc1	agent
FBI	FBI	kA	FBI
si	se	k3xPyFc3	se
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
vyšších	vysoký	k2eAgNnPc2d2	vyšší
míst	místo	k1gNnPc2	místo
bylo	být	k5eAaImAgNnS	být
úmyslě	úmyslě	k1gMnPc4	úmyslě
bráněno	bráněn	k2eAgNnSc1d1	bráněno
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
a	a	k8xC	a
aféra	aféra	k1gFnSc1	aféra
nebyla	být	k5eNaImAgFnS	být
řádně	řádně	k6eAd1	řádně
prošetřena	prošetřen	k2eAgFnSc1d1	prošetřena
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
neměla	mít	k5eNaImAgFnS	mít
ani	ani	k8xC	ani
možnost	možnost	k1gFnSc4	možnost
vyslechnout	vyslechnout	k5eAaPmF	vyslechnout
prezidenta	prezident	k1gMnSc2	prezident
Clintona	Clinton	k1gMnSc2	Clinton
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Ala	ala	k1gFnSc1	ala
Gorea	Gorea	k1gFnSc1	Gorea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Clinton	Clinton	k1gMnSc1	Clinton
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
ženatý	ženatý	k2eAgInSc1d1	ženatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Chelsea	Chelsea	k1gFnSc1	Chelsea
Clintonovou	Clintonová	k1gFnSc7	Clintonová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g/>
února	únor	k1gInSc2	únor
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
Hillary	Hillar	k1gInPc4	Hillar
Clintonová	Clintonová	k1gFnSc1	Clintonová
taktéž	taktéž	k?	taktéž
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
-	-	kIx~	-
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
senátorkou	senátorka	k1gFnSc7	senátorka
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
ministryní	ministryně	k1gFnSc7	ministryně
zahraničí	zahraničí	k1gNnSc2	zahraničí
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
ucházela	ucházet	k5eAaImAgFnS	ucházet
o	o	k7c4	o
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
prezidentku	prezidentka	k1gFnSc4	prezidentka
USA	USA	kA	USA
za	za	k7c4	za
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
na	na	k7c4	na
prezidentku	prezidentka	k1gFnSc4	prezidentka
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>

