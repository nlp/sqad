<s>
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
též	tenž	k3xDgFnSc2	tenž
zvané	zvaný	k2eAgFnSc2d1	zvaná
Velký	velký	k2eAgInSc4d1	velký
rybník	rybník	k1gInSc4	rybník
nebo	nebo	k8xC	nebo
Velký	velký	k2eAgInSc4d1	velký
Dokeský	dokeský	k2eAgInSc4d1	dokeský
rybník	rybník	k1gInSc4	rybník
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
Mácháč	Mácháč	k1gInSc1	Mácháč
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Großteich	Großteich	k1gMnSc1	Großteich
nebo	nebo	k8xC	nebo
Hirschberger	Hirschberger	k1gMnSc1	Hirschberger
Großteich	Großteich	k1gMnSc1	Großteich
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
rybník	rybník	k1gInSc4	rybník
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
osmý	osmý	k4xOgInSc4	osmý
největší	veliký	k2eAgInSc4d3	veliký
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubky	hloubka	k1gFnPc4	hloubka
až	až	k9	až
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
pozvolný	pozvolný	k2eAgMnSc1d1	pozvolný
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
český	český	k2eAgInSc4d1	český
rybník	rybník	k1gInSc4	rybník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
jinde	jinde	k6eAd1	jinde
než	než	k8xS	než
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
266	[number]	k4	266
m	m	kA	m
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
284	[number]	k4	284
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
rekreaci	rekreace	k1gFnSc4	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1366	[number]	k4	1366
Karlem	Karel	k1gMnSc7	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rašelinné	rašelinný	k2eAgFnSc6d1	rašelinná
pánvi	pánev	k1gFnSc6	pánev
na	na	k7c6	na
Robečském	Robečský	k2eAgInSc6d1	Robečský
potoku	potok	k1gInSc6	potok
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc1	přítok
Ploučnice	Ploučnice	k1gFnSc2	Ploučnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1	okolnost
založení	založení	k1gNnSc2	založení
rybníka	rybník	k1gInSc2	rybník
doložil	doložit	k5eAaPmAgMnS	doložit
jednak	jednak	k8xC	jednak
pražský	pražský	k2eAgMnSc1d1	pražský
kanovník	kanovník	k1gMnSc1	kanovník
Beneš	Beneš	k1gMnSc1	Beneš
Krabice	krabice	k1gFnSc1	krabice
z	z	k7c2	z
Veitmile	Veitmila	k1gFnSc6	Veitmila
(	(	kIx(	(
<g/>
udáno	udán	k2eAgNnSc1d1	udáno
založení	založení	k1gNnSc1	založení
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1366	[number]	k4	1366
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
kronikář	kronikář	k1gMnSc1	kronikář
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gubenu	Guben	k1gInSc2	Guben
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
zápis	zápis	k1gInSc1	zápis
byl	být	k5eAaImAgInS	být
sepsán	sepsat	k5eAaPmNgInS	sepsat
v	v	k7c6	v
Žitavě	Žitava	k1gFnSc6	Žitava
roku	rok	k1gInSc2	rok
1367	[number]	k4	1367
a	a	k8xC	a
zmiňoval	zmiňovat	k5eAaImAgInS	zmiňovat
i	i	k9	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
dosud	dosud	k6eAd1	dosud
nevídaný	vídaný	k2eNgInSc4d1	nevídaný
druh	druh	k1gInSc4	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
parmy	parma	k1gFnPc1	parma
neboli	neboli	k8xC	neboli
vousáči	vousáč	k1gMnPc1	vousáč
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
byl	být	k5eAaImAgInS	být
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k9	jako
rybník	rybník	k1gInSc1	rybník
nad	nad	k7c7	nad
Hirschbergem	Hirschberg	k1gInSc7	Hirschberg
(	(	kIx(	(
<g/>
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
Doks	Doksy	k1gInPc2	Doksy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
rybník	rybník	k1gInSc1	rybník
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1272	[number]	k4	1272
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
založení	založení	k1gNnSc3	založení
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
okolí	okolí	k1gNnSc3	okolí
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
řada	řada	k1gFnSc1	řada
legend	legenda	k1gFnPc2	legenda
(	(	kIx(	(
<g/>
o	o	k7c6	o
Daliboru	Dalibor	k1gMnSc6	Dalibor
z	z	k7c2	z
Myšlína	Myšlín	k1gInSc2	Myšlín
<g/>
,	,	kIx,	,
o	o	k7c6	o
Myším	myší	k2eAgInSc6d1	myší
hrádku	hrádek	k1gInSc6	hrádek
<g/>
,	,	kIx,	,
o	o	k7c6	o
mlynáři	mlynář	k1gMnSc6	mlynář
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
Splavech	splav	k1gInPc6	splav
a	a	k8xC	a
vzniku	vznik	k1gInSc6	vznik
vrchu	vrch	k1gInSc2	vrch
Bezdězu	Bezděz	k1gInSc2	Bezděz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
pověstí	pověst	k1gFnPc2	pověst
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
měl	mít	k5eAaImAgMnS	mít
hrabě	hrabě	k1gMnSc1	hrabě
Dalibor	Dalibor	k1gMnSc1	Dalibor
z	z	k7c2	z
Myšlína	Myšlín	k1gInSc2	Myšlín
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
strach	strach	k1gInSc1	strach
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
rybníka	rybník	k1gInSc2	rybník
vystavět	vystavět	k5eAaPmF	vystavět
malý	malý	k2eAgInSc4d1	malý
hrádek	hrádek	k1gInSc4	hrádek
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
poté	poté	k6eAd1	poté
lodí	loď	k1gFnSc7	loď
odvézt	odvézt	k5eAaPmF	odvézt
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
zásobami	zásoba	k1gFnPc7	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
bez	bez	k7c2	bez
úhony	úhona	k1gFnSc2	úhona
přečká	přečkat	k5eAaPmIp3nS	přečkat
všechny	všechen	k3xTgInPc4	všechen
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zásobami	zásoba	k1gFnPc7	zásoba
potravin	potravina	k1gFnPc2	potravina
se	se	k3xPyFc4	se
na	na	k7c4	na
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
dostala	dostat	k5eAaPmAgFnS	dostat
jedna	jeden	k4xCgFnSc1	jeden
malá	malý	k2eAgFnSc1d1	malá
myška	myška	k1gFnSc1	myška
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ve	v	k7c6	v
skladu	sklad	k1gInSc6	sklad
potravin	potravina	k1gFnPc2	potravina
vyvedla	vyvést	k5eAaPmAgFnS	vyvést
mladé	mladý	k1gMnPc4	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
nekontrolovatelné	kontrolovatelný	k2eNgNnSc4d1	nekontrolovatelné
přemnožení	přemnožení	k1gNnSc4	přemnožení
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
nešlo	jít	k5eNaImAgNnS	jít
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Myším	myší	k2eAgInSc7d1	myší
padly	padnout	k5eAaImAgInP	padnout
za	za	k7c4	za
potravu	potrava	k1gFnSc4	potrava
veškeré	veškerý	k3xTgFnPc4	veškerý
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
bez	bez	k7c2	bez
zásob	zásoba	k1gFnPc2	zásoba
nemohl	moct	k5eNaImAgMnS	moct
Dalibor	Dalibor	k1gMnSc1	Dalibor
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
dále	daleko	k6eAd2	daleko
přebývat	přebývat	k5eAaImF	přebývat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
zemřel	zemřít	k5eAaPmAgMnS	zemřít
hlady	hlady	k6eAd1	hlady
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
musel	muset	k5eAaImAgMnS	muset
přeplavat	přeplavat	k5eAaPmF	přeplavat
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
povolat	povolat	k5eAaPmF	povolat
převozníka	převozník	k1gMnSc4	převozník
a	a	k8xC	a
s	s	k7c7	s
hanbou	hanba	k1gFnSc7	hanba
se	se	k3xPyFc4	se
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
neřeklo	říct	k5eNaPmAgNnS	říct
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
Dalibor	Dalibor	k1gMnSc1	Dalibor
z	z	k7c2	z
Myšína	Myšín	k1gInSc2	Myšín
a	a	k8xC	a
ostrůvku	ostrůvek	k1gInSc2	ostrůvek
"	"	kIx"	"
<g/>
Myší	myš	k1gFnSc7	myš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1920	[number]	k4	1920
patřil	patřit	k5eAaImAgInS	patřit
místním	místní	k2eAgMnPc3d1	místní
statkářům	statkář	k1gMnPc3	statkář
<g/>
,	,	kIx,	,
hrabatům	hrabě	k1gNnPc3	hrabě
z	z	k7c2	z
rodu	rod	k1gInSc6	rod
Valdštejnů	Valdštejn	k1gMnPc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Mlýn	mlýn	k1gInSc1	mlýn
s	s	k7c7	s
náhonem	náhon	k1gInSc7	náhon
na	na	k7c6	na
potoce	potok	k1gInSc6	potok
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
Splavech	splav	k1gInPc6	splav
je	být	k5eAaImIp3nS	být
doložen	doložit	k5eAaPmNgInS	doložit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1272	[number]	k4	1272
a	a	k8xC	a
tak	tak	k9	tak
některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc1	pramen
udávají	udávat	k5eAaImIp3nP	udávat
vznik	vznik	k1gInSc4	vznik
prvotního	prvotní	k2eAgInSc2d1	prvotní
rybníka	rybník	k1gInSc2	rybník
již	již	k9	již
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
letních	letní	k2eAgInPc2d1	letní
lázeňských	lázeňský	k2eAgInPc2d1	lázeňský
pobytů	pobyt	k1gInPc2	pobyt
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
předcházela	předcházet	k5eAaImAgFnS	předcházet
i	i	k9	i
rozšíření	rozšíření	k1gNnSc4	rozšíření
kultu	kult	k1gInSc2	kult
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dílu	díl	k1gInSc6	díl
dokské	dokský	k2eAgFnSc2d1	Dokská
kroniky	kronika	k1gFnSc2	kronika
Josefa	Josef	k1gMnSc2	Josef
Quaißera	Quaißer	k1gMnSc2	Quaißer
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
letní	letní	k2eAgMnSc1d1	letní
host	host	k1gMnSc1	host
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
uveden	uvést	k5eAaPmNgMnS	uvést
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
květinář	květinář	k1gMnSc1	květinář
Franz	Franz	k1gMnSc1	Franz
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
těžce	těžce	k6eAd1	těžce
nemocný	mocný	k2eNgMnSc1d1	nemocný
zotavoval	zotavovat	k5eAaImAgMnS	zotavovat
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Angelus	Angelus	k1gMnSc1	Angelus
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
Doksech	Doksy	k1gInPc6	Doksy
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
živému	živý	k2eAgNnSc3d1	živé
dopravnímu	dopravní	k2eAgNnSc3d1	dopravní
spojení	spojení	k1gNnSc3	spojení
(	(	kIx(	(
<g/>
železnice	železnice	k1gFnSc1	železnice
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
Doks	Doksy	k1gInPc2	Doksy
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
a	a	k8xC	a
díky	díky	k7c3	díky
obchodníkům	obchodník	k1gMnPc3	obchodník
s	s	k7c7	s
chmelem	chmel	k1gInSc7	chmel
se	se	k3xPyFc4	se
Doksy	Doksy	k1gInPc1	Doksy
stávaly	stávat	k5eAaImAgInP	stávat
od	od	k7c2	od
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
známé	známá	k1gFnSc2	známá
jako	jako	k8xS	jako
okouzlující	okouzlující	k2eAgInSc1d1	okouzlující
kraj	kraj	k1gInSc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zapsal	zapsat	k5eAaPmAgMnS	zapsat
dokský	dokský	k2eAgMnSc1d1	dokský
kronikář	kronikář	k1gMnSc1	kronikář
Josef	Josef	k1gMnSc1	Josef
Quaißer	Quaißer	k1gMnSc1	Quaißer
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnPc4d1	lesní
cesty	cesta	k1gFnPc4	cesta
ve	v	k7c6	v
vrchnostenském	vrchnostenský	k2eAgNnSc6d1	vrchnostenské
teritoriu	teritorium	k1gNnSc6	teritorium
nebyly	být	k5eNaImAgFnP	být
přístupné	přístupný	k2eAgFnPc1d1	přístupná
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
vyšel	vyjít	k5eAaPmAgMnS	vyjít
Ernest	Ernest	k1gMnSc1	Ernest
hrabě	hrabě	k1gMnSc1	hrabě
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
obecnímu	obecní	k2eAgInSc3d1	obecní
úřadu	úřad	k1gInSc3	úřad
vstříc	vstříc	k6eAd1	vstříc
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
povolil	povolit	k5eAaPmAgMnS	povolit
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
rybníku	rybník	k1gInSc3	rybník
a	a	k8xC	a
na	na	k7c4	na
následujících	následující	k2eAgNnPc2d1	následující
12	[number]	k4	12
let	léto	k1gNnPc2	léto
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
povolil	povolit	k5eAaPmAgMnS	povolit
koupání	koupání	k1gNnSc2	koupání
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
vyhrazených	vyhrazený	k2eAgNnPc6d1	vyhrazené
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
porušování	porušování	k1gNnSc2	porušování
pravidel	pravidlo	k1gNnPc2	pravidlo
by	by	kYmCp3nP	by
vrchnost	vrchnost	k1gFnSc1	vrchnost
mohla	moct	k5eAaImAgFnS	moct
povolení	povolení	k1gNnSc4	povolení
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnPc1	opatření
byla	být	k5eAaImAgNnP	být
porušována	porušovat	k5eAaImNgNnP	porušovat
a	a	k8xC	a
vrchnost	vrchnost	k1gFnSc4	vrchnost
opakovaně	opakovaně	k6eAd1	opakovaně
u	u	k7c2	u
města	město	k1gNnSc2	město
protestovala	protestovat	k5eAaBmAgFnS	protestovat
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
povolení	povolení	k1gNnSc2	povolení
zřejmě	zřejmě	k6eAd1	zřejmě
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
orgán	orgán	k1gInSc4	orgán
spolupráce	spolupráce	k1gFnSc2	spolupráce
města	město	k1gNnSc2	město
s	s	k7c7	s
vlastníkem	vlastník	k1gMnSc7	vlastník
Jezerní	jezerní	k2eAgFnSc2d1	jezerní
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
v	v	k7c4	v
letovisko	letovisko	k1gNnSc4	letovisko
začaly	začít	k5eAaPmAgFnP	začít
proměňovat	proměňovat	k5eAaImF	proměňovat
i	i	k9	i
Staré	Staré	k2eAgInPc1d1	Staré
Splavy	splav	k1gInPc1	splav
<g/>
.	.	kIx.	.
</s>
<s>
Pražští	pražský	k2eAgMnPc1d1	pražský
lékaři	lékař	k1gMnPc1	lékař
začali	začít	k5eAaPmAgMnP	začít
doporučovat	doporučovat	k5eAaImF	doporučovat
rodičům	rodič	k1gMnPc3	rodič
ozdravný	ozdravný	k2eAgInSc4d1	ozdravný
pobyt	pobyt	k1gInSc4	pobyt
dětí	dítě	k1gFnPc2	dítě
u	u	k7c2	u
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
absence	absence	k1gFnSc2	absence
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
z	z	k7c2	z
nevýhody	nevýhoda	k1gFnSc2	nevýhoda
stala	stát	k5eAaPmAgFnS	stát
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
například	například	k6eAd1	například
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
i	i	k8xC	i
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
nejsou	být	k5eNaImIp3nP	být
Doksy	Doksy	k1gInPc1	Doksy
uvedeny	uveden	k2eAgInPc1d1	uveden
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
asi	asi	k9	asi
80	[number]	k4	80
menších	malý	k2eAgNnPc2d2	menší
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgFnPc2d1	tradiční
českých	český	k2eAgFnPc2d1	Česká
lázní	lázeň	k1gFnPc2	lázeň
vyjmenovány	vyjmenován	k2eAgFnPc4d1	vyjmenována
v	v	k7c6	v
ročence	ročenka	k1gFnSc6	ročenka
Lázně	lázeň	k1gFnSc2	lázeň
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc4	místo
léčební	léčební	k2eAgNnPc4d1	léčební
a	a	k8xC	a
sídla	sídlo	k1gNnPc4	sídlo
letní	letní	k2eAgNnPc4d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Čepelského	Čepelský	k2eAgInSc2d1	Čepelský
rybníka	rybník	k1gInSc2	rybník
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
lázeňská	lázeňský	k2eAgFnSc1d1	lázeňská
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
sprch	sprcha	k1gFnPc2	sprcha
a	a	k8xC	a
rašelinnými	rašelinný	k2eAgFnPc7d1	rašelinná
lázněmi	lázeň	k1gFnPc7	lázeň
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc1	lázeň
však	však	k9	však
byly	být	k5eAaImAgFnP	být
silně	silně	k6eAd1	silně
ztrátové	ztrátový	k2eAgFnPc1d1	ztrátová
a	a	k8xC	a
v	v	k7c6	v
dobových	dobový	k2eAgInPc6d1	dobový
průvodcích	průvodek	k1gInPc6	průvodek
a	a	k8xC	a
prospektech	prospekt	k1gInPc6	prospekt
byly	být	k5eAaImAgFnP	být
zmiňovány	zmiňovat	k5eAaImNgFnP	zmiňovat
jen	jen	k6eAd1	jen
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
koupila	koupit	k5eAaPmAgFnS	koupit
obec	obec	k1gFnSc1	obec
Doksy	Doksy	k1gInPc1	Doksy
od	od	k7c2	od
hraběte	hrabě	k1gMnSc4	hrabě
Adolfa	Adolf	k1gMnSc2	Adolf
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
nucen	nutit	k5eAaImNgMnS	nutit
pozemkovou	pozemkový	k2eAgFnSc7d1	pozemková
reformou	reforma	k1gFnSc7	reforma
<g/>
,	,	kIx,	,
pozemky	pozemek	k1gInPc7	pozemek
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Velkého	velký	k2eAgInSc2d1	velký
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
že	že	k8xS	že
prodávající	prodávající	k2eAgMnSc1d1	prodávající
měl	mít	k5eAaImAgInS	mít
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
40	[number]	k4	40
<g/>
%	%	kIx~	%
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
zamýšlené	zamýšlený	k2eAgFnSc6d1	zamýšlená
akciové	akciový	k2eAgFnSc6d1	akciová
společnosti	společnost	k1gFnSc6	společnost
-	-	kIx~	-
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
rybník	rybník	k1gInSc1	rybník
pro	pro	k7c4	pro
rekreaci	rekreace	k1gFnSc4	rekreace
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
právo	právo	k1gNnSc4	právo
provozovat	provozovat	k5eAaImF	provozovat
na	na	k7c6	na
rybníce	rybník	k1gInSc6	rybník
koupání	koupání	k1gNnSc2	koupání
a	a	k8xC	a
plavbu	plavba	k1gFnSc4	plavba
loděk	loďka	k1gFnPc2	loďka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
parníček	parníček	k1gInSc1	parníček
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
dopravu	doprava	k1gFnSc4	doprava
mezi	mezi	k7c7	mezi
Doksy	Doksy	k1gInPc7	Doksy
a	a	k8xC	a
Starými	starý	k2eAgInPc7d1	starý
Splavy	splav	k1gInPc7	splav
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pláží	pláž	k1gFnPc2	pláž
investovalo	investovat	k5eAaBmAgNnS	investovat
město	město	k1gNnSc1	město
s	s	k7c7	s
Valdštejny	Valdštejn	k1gMnPc7	Valdštejn
poměrem	poměr	k1gInSc7	poměr
půl	půl	k1xP	půl
na	na	k7c6	na
půl	půl	k1xP	půl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
lázeňská	lázeňský	k2eAgFnSc1d1	lázeňská
správa	správa	k1gFnSc1	správa
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
5	[number]	k4	5
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
obecní	obecní	k2eAgFnSc1d1	obecní
radě	rada	k1gFnSc3	rada
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
provozy	provoz	k1gInPc4	provoz
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgNnSc1d1	pobřežní
zařízení	zařízení	k1gNnSc1	zařízení
a	a	k8xC	a
plavební	plavební	k2eAgInSc1d1	plavební
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
měl	mít	k5eAaImAgInS	mít
Valdštejnův	Valdštejnův	k2eAgInSc1d1	Valdštejnův
velkostatek	velkostatek	k1gInSc1	velkostatek
poloviční	poloviční	k2eAgInSc4d1	poloviční
podíl	podíl	k1gInSc4	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byl	být	k5eAaImAgInS	být
Velký	velký	k2eAgInSc1d1	velký
rybník	rybník	k1gInSc1	rybník
plně	plně	k6eAd1	plně
otevřen	otevřít	k5eAaPmNgInS	otevřít
rekreaci	rekreace	k1gFnSc3	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
se	se	k3xPyFc4	se
dbalo	dbát	k5eAaImAgNnS	dbát
i	i	k9	i
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
omezením	omezení	k1gNnSc7	omezení
vodní	vodní	k2eAgFnSc2d1	vodní
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zátokách	zátoka	k1gFnPc6	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
rybníku	rybník	k1gInSc6	rybník
byly	být	k5eAaImAgFnP	být
vynaloženy	vynaložit	k5eAaPmNgFnP	vynaložit
mohutné	mohutný	k2eAgFnPc1d1	mohutná
investice	investice	k1gFnPc1	investice
a	a	k8xC	a
reklama	reklama	k1gFnSc1	reklama
<g/>
,	,	kIx,	,
atrakcemi	atrakce	k1gFnPc7	atrakce
byly	být	k5eAaImAgFnP	být
písečné	písečný	k2eAgFnPc4d1	písečná
pláže	pláž	k1gFnPc4	pláž
(	(	kIx(	(
<g/>
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
motivem	motiv	k1gInSc7	motiv
prospektů	prospekt	k1gInPc2	prospekt
byly	být	k5eAaImAgFnP	být
dětmi	dítě	k1gFnPc7	dítě
postavené	postavený	k2eAgInPc1d1	postavený
hrady	hrad	k1gInPc4	hrad
z	z	k7c2	z
písku	písek	k1gInSc2	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
a	a	k8xC	a
panoráma	panoráma	k1gFnSc1	panoráma
s	s	k7c7	s
hradním	hradní	k2eAgInSc7d1	hradní
vrchem	vrch	k1gInSc7	vrch
Bezděz	Bezděz	k1gInSc1	Bezděz
a	a	k8xC	a
členitým	členitý	k2eAgNnSc7d1	členité
pobřežím	pobřeží	k1gNnSc7	pobřeží
(	(	kIx(	(
<g/>
vrchy	vrch	k1gInPc4	vrch
Borný	borný	k2eAgInSc4d1	borný
a	a	k8xC	a
Šroubený	Šroubený	k2eAgInSc4d1	Šroubený
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
atrakce	atrakce	k1gFnSc2	atrakce
i	i	k8xC	i
plavby	plavba	k1gFnSc2	plavba
parníčkem	parníček	k1gInSc7	parníček
<g/>
,	,	kIx,	,
fungovala	fungovat	k5eAaImAgFnS	fungovat
zde	zde	k6eAd1	zde
půjčovna	půjčovna	k1gFnSc1	půjčovna
loděk	loďka	k1gFnPc2	loďka
a	a	k8xC	a
plachetnic	plachetnice	k1gFnPc2	plachetnice
i	i	k8xC	i
kursy	kurs	k1gInPc4	kurs
plavání	plavání	k1gNnSc2	plavání
-	-	kIx~	-
německé	německý	k2eAgInPc1d1	německý
prospekty	prospekt	k1gInPc1	prospekt
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zdůrazňovaly	zdůrazňovat	k5eAaImAgFnP	zdůrazňovat
jiný	jiný	k2eAgInSc4d1	jiný
aspekt	aspekt	k1gInSc4	aspekt
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
tichá	tichý	k2eAgNnPc1d1	tiché
místa	místo	k1gNnPc1	místo
a	a	k8xC	a
tiché	tichý	k2eAgFnPc1d1	tichá
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
a	a	k8xC	a
hrad	hrad	k1gInSc1	hrad
znějí	znět	k5eAaImIp3nP	znět
jako	jako	k9	jako
staroněmecký	staroněmecký	k2eAgInSc4d1	staroněmecký
chorál	chorál	k1gInSc4	chorál
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
trendy	trend	k1gInPc1	trend
vyvolávaly	vyvolávat	k5eAaImAgInP	vyvolávat
i	i	k9	i
pohoršení	pohoršení	k1gNnSc1	pohoršení
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
si	se	k3xPyFc3	se
Anna	Anna	k1gFnSc1	Anna
Hofmannová	Hofmannová	k1gFnSc1	Hofmannová
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
starostovi	starosta	k1gMnSc3	starosta
Doks	Doksy	k1gInPc2	Doksy
Diessnerovi	Diessner	k1gMnSc3	Diessner
na	na	k7c4	na
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
mravnostní	mravnostní	k2eAgFnSc2d1	mravnostní
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
koupe	koupat	k5eAaImIp3nS	koupat
dohromady	dohromady	k6eAd1	dohromady
<g/>
:	:	kIx,	:
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
velké	velký	k2eAgFnPc1d1	velká
i	i	k8xC	i
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
úplně	úplně	k6eAd1	úplně
bez	bez	k7c2	bez
rozpaků	rozpak	k1gInPc2	rozpak
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dámy	dáma	k1gFnPc1	dáma
oblečené	oblečený	k2eAgFnPc1d1	oblečená
v	v	k7c6	v
trikotech	trikot	k1gInPc6	trikot
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
jim	on	k3xPp3gFnPc3	on
jde	jít	k5eAaImIp3nS	jít
vidět	vidět	k5eAaImF	vidět
téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
by	by	kYmCp3nP	by
mužským	mužský	k2eAgNnPc3d1	mužské
očím	oko	k1gNnPc3	oko
mělo	mít	k5eAaImAgNnS	mít
zůstat	zůstat	k5eAaPmF	zůstat
skryto	skrýt	k5eAaPmNgNnS	skrýt
<g/>
,	,	kIx,	,
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
na	na	k7c6	na
sobě	se	k3xPyFc3	se
mají	mít	k5eAaImIp3nP	mít
plavky	plavka	k1gFnPc4	plavka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nanejvýš	nanejvýš	k6eAd1	nanejvýš
primitivní	primitivní	k2eAgMnSc1d1	primitivní
a	a	k8xC	a
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
slušné	slušný	k2eAgNnSc4d1	slušné
ženské	ženský	k2eAgNnSc4d1	ženské
oko	oko	k1gNnSc4	oko
stydí	stydět	k5eAaImIp3nS	stydět
pohlédnout	pohlédnout	k5eAaPmF	pohlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
turistiky	turistika	k1gFnSc2	turistika
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
ke	k	k7c3	k
zvelebení	zvelebení	k1gNnSc3	zvelebení
okolí	okolí	k1gNnSc2	okolí
<g/>
:	:	kIx,	:
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikala	vznikat	k5eAaImAgFnS	vznikat
hřiště	hřiště	k1gNnSc4	hřiště
a	a	k8xC	a
sportovní	sportovní	k2eAgNnSc4d1	sportovní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
okrašlovací	okrašlovací	k2eAgInSc1d1	okrašlovací
spolek	spolek	k1gInSc1	spolek
vysazoval	vysazovat	k5eAaImAgInS	vysazovat
háje	háj	k1gInSc2	háj
<g/>
,	,	kIx,	,
aleje	alej	k1gFnSc2	alej
a	a	k8xC	a
sady	sada	k1gFnSc2	sada
a	a	k8xC	a
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
turistické	turistický	k2eAgFnPc4d1	turistická
trasy	trasa	k1gFnPc4	trasa
<g/>
,	,	kIx,	,
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
kino	kino	k1gNnSc1	kino
Heller	Heller	k1gInSc1	Heller
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
i	i	k8xC	i
pořadatelé	pořadatel	k1gMnPc1	pořadatel
hudebních	hudební	k2eAgInPc2d1	hudební
a	a	k8xC	a
tanečních	taneční	k2eAgFnPc2d1	taneční
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
konaly	konat	k5eAaImAgFnP	konat
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
například	například	k6eAd1	například
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sychravou	sychravý	k2eAgFnSc7d1	sychravá
sezónou	sezóna	k1gFnSc7	sezóna
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
autor	autor	k1gMnSc1	autor
místní	místní	k2eAgFnSc2d1	místní
české	český	k2eAgFnSc2d1	Česká
kroniky	kronika	k1gFnSc2	kronika
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Doksy	Doksy	k1gInPc1	Doksy
samy	sám	k3xTgInPc1	sám
jsou	být	k5eAaImIp3nP	být
nevábného	vábný	k2eNgInSc2d1	nevábný
vzhledu	vzhled	k1gInSc2	vzhled
(	(	kIx(	(
<g/>
cestou	cesta	k1gFnSc7	cesta
od	od	k7c2	od
nádraží	nádraží	k1gNnSc6	nádraží
tři	tři	k4xCgFnPc1	tři
řady	řada	k1gFnPc1	řada
odporných	odporný	k2eAgFnPc2d1	odporná
stodol	stodola	k1gFnPc2	stodola
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
lákalo	lákat	k5eAaImAgNnS	lákat
či	či	k8xC	či
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
trávily	trávit	k5eAaImAgFnP	trávit
nevlídné	vlídný	k2eNgInPc4d1	nevlídný
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
malých	malý	k2eAgNnPc2d1	malé
měst	město	k1gNnPc2	město
učinit	učinit	k5eAaImF	učinit
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
letní	letní	k2eAgNnSc1d1	letní
sídlo	sídlo	k1gNnSc1	sídlo
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stávaly	stávat	k5eAaImAgFnP	stávat
terčem	terč	k1gInSc7	terč
satiry	satira	k1gFnSc2	satira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
satirická	satirický	k2eAgFnSc1d1	satirická
básnička	básnička	k1gFnSc1	básnička
Letovisko	letovisko	k1gNnSc1	letovisko
Doksy	Doksy	k1gInPc1	Doksy
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
poměry	poměr	k1gInPc4	poměr
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
a	a	k8xC	a
o	o	k7c6	o
zcela	zcela	k6eAd1	zcela
novém	nový	k2eAgInSc6d1	nový
parníku	parník	k1gInSc6	parník
Greif	Greif	k1gMnSc1	Greif
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
zezadu	zezadu	k6eAd1	zezadu
nepůsobí	působit	k5eNaImIp3nS	působit
ani	ani	k8xC	ani
pyšně	pyšně	k6eAd1	pyšně
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
krásně	krásně	k6eAd1	krásně
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
posmrkaného	posmrkaný	k2eAgInSc2d1	posmrkaný
kapesníku	kapesník	k1gInSc2	kapesník
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chybějící	chybějící	k2eAgFnSc3d1	chybějící
kanalizaci	kanalizace	k1gFnSc3	kanalizace
<g/>
,	,	kIx,	,
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
dláždění	dláždění	k1gNnSc3	dláždění
<g/>
,	,	kIx,	,
elektrifikaci	elektrifikace	k1gFnSc3	elektrifikace
a	a	k8xC	a
vodovodu	vodovod	k1gInSc2	vodovod
Doksy	Doksy	k1gInPc1	Doksy
neuspěly	uspět	k5eNaPmAgInP	uspět
se	s	k7c7	s
žádostmi	žádost	k1gFnPc7	žádost
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
statusu	status	k1gInSc2	status
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
odebrán	odebrat	k5eAaPmNgInS	odebrat
za	za	k7c2	za
josefinských	josefinský	k2eAgFnPc2d1	josefinská
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
sláva	sláva	k1gFnSc1	sláva
Doks	Doksy	k1gInPc2	Doksy
<g/>
,	,	kIx,	,
Starých	Starých	k2eAgInPc2d1	Starých
Splavů	splav	k1gInPc2	splav
a	a	k8xC	a
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
propukla	propuknout	k5eAaPmAgFnS	propuknout
až	až	k6eAd1	až
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prospektu	prospekt	k1gInSc6	prospekt
nazývá	nazývat	k5eAaImIp3nS	nazývat
největšími	veliký	k2eAgFnPc7d3	veliký
lázněmi	lázeň	k1gFnPc7	lázeň
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
v	v	k7c6	v
československu	československo	k1gNnSc6	československo
a	a	k8xC	a
v	v	k7c6	v
průvodcích	průvodek	k1gInPc6	průvodek
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
přirovnáváno	přirovnáván	k2eAgNnSc4d1	přirovnáváno
k	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
světovým	světový	k2eAgInPc3d1	světový
letoviskům	letovisko	k1gNnPc3	letovisko
<g/>
,	,	kIx,	,
Riviéře	Riviéra	k1gFnSc3	Riviéra
<g/>
,	,	kIx,	,
Sv.	sv.	kA	sv.
Mořici	Mořic	k1gMnPc7	Mořic
či	či	k8xC	či
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
článek	článek	k1gInSc4	článek
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
-	-	kIx~	-
perla	perla	k1gFnSc1	perla
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
autor	autor	k1gMnSc1	autor
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
i	i	k9	i
k	k	k7c3	k
zimnímu	zimní	k2eAgNnSc3d1	zimní
rekreačnímu	rekreační	k2eAgNnSc3d1	rekreační
využití	využití	k1gNnSc3	využití
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
3818	[number]	k4	3818
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
strávili	strávit	k5eAaPmAgMnP	strávit
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
lázně	lázeň	k1gFnSc2	lázeň
u	u	k7c2	u
Čepelského	Čepelský	k2eAgInSc2d1	Čepelský
rybníka	rybník	k1gInSc2	rybník
postupně	postupně	k6eAd1	postupně
skomíraly	skomírat	k5eAaImAgFnP	skomírat
<g/>
.	.	kIx.	.
</s>
<s>
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
vyčištěno	vyčistit	k5eAaPmNgNnS	vyčistit
od	od	k7c2	od
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tam	tam	k6eAd1	tam
naházela	naházet	k5eAaBmAgFnS	naházet
ustupující	ustupující	k2eAgFnSc1d1	ustupující
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Buržoazně	buržoazně	k6eAd1	buržoazně
a	a	k8xC	a
německy	německy	k6eAd1	německy
znějící	znějící	k2eAgNnPc1d1	znějící
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
jezerní	jezerní	k2eAgFnSc1d1	jezerní
lázně	lázeň	k1gFnPc1	lázeň
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
nahrazována	nahrazovat	k5eAaImNgFnS	nahrazovat
novým	nový	k2eAgInSc7d1	nový
termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
rekreační	rekreační	k2eAgNnSc1d1	rekreační
středisko	středisko	k1gNnSc1	středisko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
Máchovo	Máchův	k2eAgNnSc4d1	Máchovo
jezero	jezero	k1gNnSc4	jezero
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgMnS	vžít
postupně	postupně	k6eAd1	postupně
už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vyzdvižením	vyzdvižení	k1gNnSc7	vyzdvižení
památky	památka	k1gFnSc2	památka
na	na	k7c4	na
K.	K.	kA	K.
H.	H.	kA	H.
Máchu	Mácha	k1gMnSc4	Mácha
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
používání	používání	k1gNnSc3	používání
protestovali	protestovat	k5eAaBmAgMnP	protestovat
jak	jak	k8xS	jak
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
odborníci	odborník	k1gMnPc1	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Názvoslovná	názvoslovný	k2eAgFnSc1d1	názvoslovná
komise	komise	k1gFnSc1	komise
při	při	k7c6	při
Geografickém	geografický	k2eAgInSc6d1	geografický
komitétu	komitét	k1gInSc6	komitét
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
badatelské	badatelský	k2eAgFnSc2d1	badatelská
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
zamítavé	zamítavý	k2eAgNnSc4d1	zamítavé
stanovisko	stanovisko	k1gNnSc4	stanovisko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
ve	v	k7c6	v
sborníku	sborník	k1gInSc6	sborník
Bezděz	Bezděz	k1gInSc1	Bezděz
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jej	on	k3xPp3gMnSc4	on
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kinský	Kinský	k1gMnSc1	Kinský
v	v	k7c6	v
názvu	název	k1gInSc6	název
svého	svůj	k3xOyFgMnSc2	svůj
turistického	turistický	k2eAgMnSc2d1	turistický
průvodce	průvodce	k1gMnSc2	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
jezera	jezero	k1gNnSc2	jezero
po	po	k7c6	po
významném	významný	k2eAgMnSc6d1	významný
básníkovi	básník	k1gMnSc6	básník
Máchovi	Mácha	k1gMnSc6	Mácha
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
turistický	turistický	k2eAgInSc1d1	turistický
spolek	spolek	k1gInSc1	spolek
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
<g/>
,	,	kIx,	,
příslušná	příslušný	k2eAgFnSc1d1	příslušná
jazyková	jazykový	k2eAgFnSc1d1	jazyková
komise	komise	k1gFnSc1	komise
však	však	k9	však
návrh	návrh	k1gInSc4	návrh
znovu	znovu	k6eAd1	znovu
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
podnikatelé	podnikatel	k1gMnPc1	podnikatel
zvali	zvát	k5eAaImAgMnP	zvát
své	svůj	k3xOyFgMnPc4	svůj
zákazníky	zákazník	k1gMnPc4	zákazník
v	v	k7c6	v
inzerátech	inzerát	k1gInPc6	inzerát
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
do	do	k7c2	do
Doks	Doksy	k1gInPc2	Doksy
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
oficiálně	oficiálně	k6eAd1	oficiálně
až	až	k9	až
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
po	po	k7c6	po
poválečném	poválečný	k2eAgInSc6d1	poválečný
odsunu	odsun	k1gInSc6	odsun
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgInSc1d1	revoluční
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
nechal	nechat	k5eAaPmAgInS	nechat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
vyrobit	vyrobit	k5eAaPmF	vyrobit
poštovní	poštovní	k2eAgNnSc4d1	poštovní
razítko	razítko	k1gNnSc4	razítko
s	s	k7c7	s
názvem	název	k1gInSc7	název
Doksy	Doksy	k1gInPc4	Doksy
u	u	k7c2	u
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
však	však	k9	však
i	i	k9	i
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
požadavek	požadavek	k1gInSc4	požadavek
obce	obec	k1gFnSc2	obec
o	o	k7c4	o
úřední	úřední	k2eAgNnSc4d1	úřední
používání	používání	k1gNnSc4	používání
přívlastku	přívlastek	k1gInSc2	přívlastek
U	u	k7c2	u
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
Velký	velký	k2eAgInSc1d1	velký
rybník	rybník	k1gInSc1	rybník
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
okolní	okolní	k2eAgInSc1d1	okolní
kraj	kraj	k1gInSc1	kraj
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1813	[number]	k4	1813
km2	km2	k4	km2
označen	označit	k5eAaPmNgMnS	označit
úředně	úředně	k6eAd1	úředně
jako	jako	k8xC	jako
Máchův	Máchův	k2eAgInSc1d1	Máchův
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
vydal	vydat	k5eAaPmAgInS	vydat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
prý	prý	k9	prý
nemohli	moct	k5eNaImAgMnP	moct
natrefit	natrefit	k5eAaPmF	natrefit
na	na	k7c4	na
žádnou	žádný	k3yNgFnSc4	žádný
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
až	až	k9	až
pojednou	pojednou	k6eAd1	pojednou
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
z	z	k7c2	z
houští	houští	k1gNnSc2	houští
statný	statný	k2eAgMnSc1d1	statný
jelen	jelen	k1gMnSc1	jelen
<g/>
.	.	kIx.	.
</s>
<s>
Prchal	Prchal	k1gMnSc1	Prchal
před	před	k7c7	před
lovci	lovec	k1gMnPc7	lovec
do	do	k7c2	do
kraje	kraj	k1gInSc2	kraj
hlubokých	hluboký	k2eAgInPc2d1	hluboký
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
mokřadů	mokřad	k1gInPc2	mokřad
<g/>
,	,	kIx,	,
bažin	bažina	k1gFnPc2	bažina
a	a	k8xC	a
jezírek	jezírko	k1gNnPc2	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jim	on	k3xPp3gMnPc3	on
zmizel	zmizet	k5eAaPmAgMnS	zmizet
z	z	k7c2	z
dohledu	dohled	k1gInSc2	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
byl	být	k5eAaImAgMnS	být
rozmrzelý	rozmrzelý	k2eAgInSc4d1	rozmrzelý
z	z	k7c2	z
neúspěšného	úspěšný	k2eNgInSc2d1	neúspěšný
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
když	když	k8xS	když
vtom	vtom	k6eAd1	vtom
uslyšel	uslyšet	k5eAaPmAgMnS	uslyšet
nádherný	nádherný	k2eAgInSc4d1	nádherný
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
za	za	k7c7	za
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
na	na	k7c4	na
louku	louka	k1gFnSc4	louka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spatřil	spatřit	k5eAaPmAgMnS	spatřit
prozpěvujícího	prozpěvující	k2eAgMnSc4d1	prozpěvující
si	se	k3xPyFc3	se
ovčáka	ovčák	k1gMnSc2	ovčák
se	s	k7c7	s
stádem	stádo	k1gNnSc7	stádo
oveček	ovečka	k1gFnPc2	ovečka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
dorazila	dorazit	k5eAaPmAgFnS	dorazit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
družina	družina	k1gFnSc1	družina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
pronásledování	pronásledování	k1gNnSc6	pronásledování
jelena	jelen	k1gMnSc2	jelen
celá	celý	k2eAgNnPc4d1	celé
promáčená	promáčený	k2eAgNnPc4d1	promáčené
a	a	k8xC	a
plná	plný	k2eAgNnPc4d1	plné
bahna	bahno	k1gNnPc4	bahno
a	a	k8xC	a
bláta	bláto	k1gNnPc4	bláto
z	z	k7c2	z
bažin	bažina	k1gFnPc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Krále	Král	k1gMnSc2	Král
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
příjemné	příjemný	k2eAgNnSc1d1	příjemné
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
vykoupat	vykoupat	k5eAaPmF	vykoupat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
rybník	rybník	k1gInSc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
také	také	k9	také
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Staré	Staré	k2eAgInPc4d1	Staré
Splavy	splav	k1gInPc4	splav
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
stará	starý	k2eAgFnSc1d1	stará
kamenná	kamenný	k2eAgFnSc1d1	kamenná
propusť	propustit	k5eAaPmRp2nS	propustit
a	a	k8xC	a
vedle	vedle	k6eAd1	vedle
ní	on	k3xPp3gFnSc3	on
navršena	navršen	k2eAgFnSc1d1	navršena
hráz	hráz	k1gFnSc1	hráz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zadržela	zadržet	k5eAaPmAgFnS	zadržet
vody	voda	k1gFnPc4	voda
z	z	k7c2	z
potoků	potok	k1gInPc2	potok
Jordán	Jordán	k1gInSc1	Jordán
a	a	k8xC	a
Doks	Doksy	k1gInPc2	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
Hráz	hráz	k1gFnSc1	hráz
a	a	k8xC	a
kamenná	kamenný	k2eAgFnSc1d1	kamenná
výpusť	výpusť	k1gFnSc1	výpusť
tu	tu	k6eAd1	tu
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hráz	hráz	k1gFnSc1	hráz
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
9,4	[number]	k4	9,4
m	m	kA	m
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
130	[number]	k4	130
m	m	kA	m
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
284	[number]	k4	284
ha	ha	kA	ha
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
zadržené	zadržený	k2eAgFnSc2d1	zadržená
vody	voda	k1gFnSc2	voda
6,312	[number]	k4	6,312
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
vzdutí	vzdutí	k1gNnSc1	vzdutí
3,1	[number]	k4	3,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgFnSc1d1	generální
oprava	oprava	k1gFnSc1	oprava
hráze	hráz	k1gFnSc2	hráz
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
potřebné	potřebný	k2eAgFnSc3d1	potřebná
opravě	oprava	k1gFnSc3	oprava
výpusti	výpust	k1gFnSc2	výpust
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
opět	opět	k6eAd1	opět
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
vypustit	vypustit	k5eAaPmF	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
opravu	oprava	k1gFnSc4	oprava
odsunout	odsunout	k5eAaPmF	odsunout
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
důvodů	důvod	k1gInPc2	důvod
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
jezera	jezero	k1gNnSc2	jezero
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
ostrůvky	ostrůvek	k1gInPc4	ostrůvek
Myší	myší	k2eAgInSc4d1	myší
zámek	zámek	k1gInSc4	zámek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Myšlín	Myšlín	k1gInSc1	Myšlín
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kachní	kachní	k2eAgInSc4d1	kachní
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
nepřístupné	přístupný	k2eNgNnSc1d1	nepřístupné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gNnPc6	on
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
ornitologické	ornitologický	k2eAgFnPc4d1	ornitologická
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Myším	myší	k2eAgInSc6d1	myší
ostrově	ostrov	k1gInSc6	ostrov
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
tvrze	tvrz	k1gFnSc2	tvrz
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
části	část	k1gFnSc2	část
pobřeží	pobřeží	k1gNnSc1	pobřeží
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
východní	východní	k2eAgFnSc6d1	východní
(	(	kIx(	(
<g/>
Břehyňské	Břehyňský	k2eAgFnSc6d1	Břehyňský
<g/>
)	)	kIx)	)
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
zátoce	zátoka	k1gFnSc6	zátoka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Swamp	Swamp	k1gMnSc1	Swamp
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
zbytků	zbytek	k1gInPc2	zbytek
původního	původní	k2eAgNnSc2d1	původní
rašeliniště	rašeliniště	k1gNnSc2	rašeliniště
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Swampu	Swamp	k1gInSc2	Swamp
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nedalekými	daleký	k2eNgFnPc7d1	nedaleká
vodními	vodní	k2eAgFnPc7d1	vodní
plochami	plocha	k1gFnPc7	plocha
(	(	kIx(	(
<g/>
Břehyňský	Břehyňský	k2eAgInSc1d1	Břehyňský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Hradčanské	hradčanský	k2eAgInPc1d1	hradčanský
rybníky	rybník	k1gInPc1	rybník
<g/>
,	,	kIx,	,
Novozámecký	Novozámecký	k2eAgInSc1d1	Novozámecký
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Heřmanický	Heřmanický	k2eAgInSc1d1	Heřmanický
rybník	rybník	k1gInSc1	rybník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okolními	okolní	k2eAgInPc7d1	okolní
lesy	les	k1gInPc7	les
a	a	k8xC	a
vyvýšeninami	vyvýšenina	k1gFnPc7	vyvýšenina
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
ptactva	ptactvo	k1gNnSc2	ptactvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
chráněné	chráněný	k2eAgFnSc2d1	chráněná
lokality	lokalita	k1gFnSc2	lokalita
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ptačí	ptačit	k5eAaImIp3nP	ptačit
oblast	oblast	k1gFnSc4	oblast
Českolipsko	Českolipsko	k1gNnSc1	Českolipsko
-	-	kIx~	-
Dokeské	dokeský	k2eAgInPc1d1	dokeský
pískovce	pískovec	k1gInPc1	pískovec
a	a	k8xC	a
mokřady	mokřad	k1gInPc1	mokřad
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
evropské	evropský	k2eAgFnSc2d1	Evropská
soustavy	soustava	k1gFnSc2	soustava
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc4	rybník
napájejí	napájet	k5eAaImIp3nP	napájet
dva	dva	k4xCgInPc1	dva
přítoky	přítok	k1gInPc1	přítok
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgMnSc1d1	hlavní
je	být	k5eAaImIp3nS	být
Robečský	Robečský	k2eAgInSc1d1	Robečský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
místní	místní	k2eAgInPc1d1	místní
názvy	název	k1gInPc1	název
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
toku	tok	k1gInSc6	tok
jsou	být	k5eAaImIp3nP	být
Okenský	Okenský	k2eAgInSc4d1	Okenský
<g/>
,	,	kIx,	,
Dokský	dokský	k2eAgInSc4d1	dokský
či	či	k8xC	či
Dokeský	dokeský	k2eAgInSc4d1	dokeský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
napájí	napájet	k5eAaImIp3nS	napájet
tzv.	tzv.	kA	tzv.
Dokeskou	dokeský	k2eAgFnSc4d1	Dokeská
zátoku	zátoka	k1gFnSc4	zátoka
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
přítok	přítok	k1gInSc1	přítok
<g/>
,	,	kIx,	,
Břehyňský	Břehyňský	k2eAgInSc1d1	Břehyňský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
vytékající	vytékající	k2eAgInSc1d1	vytékající
z	z	k7c2	z
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Břehyňského	Břehyňský	k2eAgInSc2d1	Břehyňský
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
napájející	napájející	k2eAgFnSc1d1	napájející
tzv.	tzv.	kA	tzv.
Břehyňskou	Břehyňský	k2eAgFnSc4d1	Břehyňský
zátoku	zátoka	k1gFnSc4	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Odtok	odtok	k1gInSc1	odtok
z	z	k7c2	z
rybníka	rybník	k1gInSc2	rybník
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
Robečský	Robečský	k2eAgInSc1d1	Robečský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
místní	místní	k2eAgInSc1d1	místní
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
též	též	k9	též
Mlýnský	mlýnský	k2eAgMnSc1d1	mlýnský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
toku	tok	k1gInSc6	tok
napájí	napájet	k5eAaImIp3nP	napájet
Novozámecký	Novozámecký	k2eAgInSc4d1	Novozámecký
rybník	rybník	k1gInSc4	rybník
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
v	v	k7c6	v
Dubici	Dubice	k1gFnSc6	Dubice
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Ploučnice	Ploučnice	k1gFnSc2	Ploučnice
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
řady	řada	k1gFnSc2	řada
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
penzionů	penzion	k1gInPc2	penzion
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
zařízení	zařízení	k1gNnPc2	zařízení
určených	určený	k2eAgNnPc2d1	určené
pro	pro	k7c4	pro
rekreaci	rekreace	k1gFnSc4	rekreace
a	a	k8xC	a
trávení	trávení	k1gNnSc4	trávení
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
a	a	k8xC	a
Starých	Starých	k2eAgInPc6d1	Starých
Splavech	splav	k1gInPc6	splav
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
a	a	k8xC	a
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
kempů	kemp	k1gInPc2	kemp
(	(	kIx(	(
<g/>
Borný	borný	k2eAgInSc1d1	borný
<g/>
,	,	kIx,	,
Klůček	Klůček	k1gInSc1	Klůček
<g/>
,	,	kIx,	,
Bílý	bílý	k2eAgInSc1d1	bílý
Kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pláží	pláž	k1gFnPc2	pláž
a	a	k8xC	a
chatových	chatový	k2eAgFnPc2d1	chatová
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
milovníkům	milovník	k1gMnPc3	milovník
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
Diskotéka	diskotéka	k1gFnSc1	diskotéka
Bílý	bílý	k1gMnSc1	bílý
Kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
festival	festival	k1gInSc1	festival
elektronické	elektronický	k2eAgFnSc2d1	elektronická
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
Mácháč	Mácháč	k1gInSc1	Mácháč
<g/>
,	,	kIx,	,
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
každoročně	každoročně	k6eAd1	každoročně
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
kempu	kemp	k1gInSc2	kemp
Klůček	Klůčka	k1gFnPc2	Klůčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
populární	populární	k2eAgFnPc4d1	populární
činnosti	činnost	k1gFnPc4	činnost
zejména	zejména	k9	zejména
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
turistika	turistika	k1gFnSc1	turistika
<g/>
,	,	kIx,	,
cykloturistika	cykloturistika	k1gFnSc1	cykloturistika
či	či	k8xC	či
houbaření	houbařený	k2eAgMnPc1d1	houbařený
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc1d1	pořádán
cyklus	cyklus	k1gInSc1	cyklus
výletů	výlet	k1gInPc2	výlet
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Máchovi	Mácha	k1gMnSc6	Mácha
v	v	k7c6	v
patách	pata	k1gFnPc6	pata
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
organizovány	organizovat	k5eAaBmNgInP	organizovat
orientační	orientační	k2eAgInPc1d1	orientační
běhy	běh	k1gInPc1	běh
<g/>
,	,	kIx,	,
plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
sportovně	sportovně	k6eAd1	sportovně
rekreačních	rekreační	k2eAgFnPc2d1	rekreační
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgFnPc4d1	tradiční
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgFnPc4d1	společenská
akce	akce	k1gFnPc4	akce
patří	patřit	k5eAaImIp3nS	patřit
jarní	jarní	k2eAgNnSc1d1	jarní
otvírání	otvírání	k1gNnSc1	otvírání
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
a	a	k8xC	a
letní	letní	k2eAgFnSc1d1	letní
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
Splavech	splav	k1gInPc6	splav
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
poutí	pouť	k1gFnSc7	pouť
a	a	k8xC	a
lidovou	lidový	k2eAgFnSc7d1	lidová
zábavou	zábava	k1gFnSc7	zábava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
bývá	bývat	k5eAaImIp3nS	bývat
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
plocha	plocha	k1gFnSc1	plocha
jezera	jezero	k1gNnSc2	jezero
využívána	využívat	k5eAaPmNgFnS	využívat
pro	pro	k7c4	pro
bruslení	bruslení	k1gNnSc4	bruslení
<g/>
,	,	kIx,	,
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
i	i	k9	i
běžkaři	běžkař	k1gMnPc1	běžkař
<g/>
,	,	kIx,	,
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jsou	být	k5eAaImIp3nP	být
stánky	stánek	k1gInPc1	stánek
s	s	k7c7	s
občerstvením	občerstvení	k1gNnSc7	občerstvení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
mezi	mezi	k7c7	mezi
Doksy	Doksy	k1gInPc7	Doksy
a	a	k8xC	a
Starými	starý	k2eAgInPc7d1	starý
Splavy	splav	k1gInPc7	splav
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
nový	nový	k2eAgInSc1d1	nový
hotel	hotel	k1gInSc1	hotel
Port	port	k1gInSc1	port
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
v	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
jako	jako	k8xC	jako
Stavba	stavba	k1gFnSc1	stavba
roku	rok	k1gInSc2	rok
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgInSc7d1	jediný
veřejným	veřejný	k2eAgInSc7d1	veřejný
<g/>
,	,	kIx,	,
nepodnikovým	podnikový	k2eNgInSc7d1	podnikový
kempem	kemp	k1gInSc7	kemp
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Provozoval	provozovat	k5eAaImAgInS	provozovat
jej	on	k3xPp3gNnSc4	on
Okresní	okresní	k2eAgInSc1d1	okresní
podnik	podnik	k1gInSc1	podnik
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
závod	závod	k1gInSc1	závod
05	[number]	k4	05
Doksy	Doksy	k1gInPc4	Doksy
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
kategorie	kategorie	k1gFnPc4	kategorie
B	B	kA	B
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
150	[number]	k4	150
000	[number]	k4	000
m2	m2	k4	m2
vč.	vč.	k?	vč.
<g/>
části	část	k1gFnSc6	část
rezervované	rezervovaný	k2eAgFnSc6d1	rezervovaná
podnikové	podnikový	k2eAgFnSc3d1	podniková
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
rekreaci	rekreace	k1gFnSc3	rekreace
(	(	kIx(	(
<g/>
z	z	k7c2	z
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapacita	kapacita	k1gFnSc1	kapacita
1700	[number]	k4	1700
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bylo	být	k5eAaImAgNnS	být
23	[number]	k4	23
chatek	chatka	k1gFnPc2	chatka
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
se	s	k7c7	s
sociálním	sociální	k2eAgNnSc7d1	sociální
zařízením	zařízení	k1gNnSc7	zařízení
rekreantů	rekreant	k1gMnPc2	rekreant
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Doks	Doksy	k1gInPc2	Doksy
po	po	k7c6	po
asfaltové	asfaltový	k2eAgFnSc6d1	asfaltová
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
první	první	k4xOgFnPc1	první
loďky	loďka	k1gFnPc1	loďka
na	na	k7c6	na
Čepelském	Čepelský	k2eAgInSc6d1	Čepelský
rybníku	rybník	k1gInSc6	rybník
u	u	k7c2	u
centra	centrum	k1gNnSc2	centrum
Doks	Doksy	k1gInPc2	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
rybník	rybník	k1gInSc4	rybník
spuštěn	spustit	k5eAaPmNgInS	spustit
nově	nově	k6eAd1	nově
koupený	koupený	k2eAgInSc1d1	koupený
parníček	parníček	k1gInSc1	parníček
Greif	Greif	k1gInSc1	Greif
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
s	s	k7c7	s
hodinovým	hodinový	k2eAgInSc7d1	hodinový
intervalem	interval	k1gInSc7	interval
pendloval	pendlovat	k5eAaImAgMnS	pendlovat
mezi	mezi	k7c7	mezi
Doksy	Doksy	k1gInPc7	Doksy
a	a	k8xC	a
Starými	starý	k2eAgInPc7d1	starý
Splavy	splav	k1gInPc7	splav
<g/>
.	.	kIx.	.
</s>
<s>
Akciový	akciový	k2eAgInSc1d1	akciový
podnik	podnik	k1gInSc1	podnik
pro	pro	k7c4	pro
rekreační	rekreační	k2eAgNnSc4d1	rekreační
využívání	využívání	k1gNnSc4	využívání
rybníku	rybník	k1gInSc3	rybník
byl	být	k5eAaImAgMnS	být
rozdělen	rozdělit	k5eAaPmNgMnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
provozy	provoz	k1gInPc4	provoz
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgNnSc1d1	pobřežní
zařízení	zařízení	k1gNnSc1	zařízení
a	a	k8xC	a
plavební	plavební	k2eAgInSc1d1	plavební
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
měl	mít	k5eAaImAgInS	mít
Valdštejnův	Valdštejnův	k2eAgInSc1d1	Valdštejnův
velkostatek	velkostatek	k1gInSc1	velkostatek
poloviční	poloviční	k2eAgInSc4d1	poloviční
podíl	podíl	k1gInSc4	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
přibyla	přibýt	k5eAaPmAgFnS	přibýt
motorová	motorový	k2eAgFnSc1d1	motorová
loď	loď	k1gFnSc1	loď
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
byl	být	k5eAaImAgInS	být
parníček	parníček	k1gInSc1	parníček
Greif	Greif	k1gInSc1	Greif
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
motorovou	motorový	k2eAgFnSc7d1	motorová
lodí	loď	k1gFnSc7	loď
Tista	Tista	k?	Tista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
odsunu	odsun	k1gInSc2	odsun
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgFnPc4	tento
lodě	loď	k1gFnPc4	loď
německé	německý	k2eAgFnSc2d1	německá
výroby	výroba	k1gFnSc2	výroba
Maria	Mario	k1gMnSc2	Mario
a	a	k8xC	a
Tista	Tista	k?	Tista
přejmenovány	přejmenovat	k5eAaPmNgInP	přejmenovat
na	na	k7c4	na
Jarmila	Jarmila	k1gFnSc1	Jarmila
a	a	k8xC	a
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
přibyla	přibýt	k5eAaPmAgFnS	přibýt
česká	český	k2eAgFnSc1d1	Česká
loď	loď	k1gFnSc1	loď
Máj	Mája	k1gFnPc2	Mája
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
maďarská	maďarský	k2eAgFnSc1d1	maďarská
loď	loď	k1gFnSc1	loď
Racek	racek	k1gMnSc1	racek
<g/>
.	.	kIx.	.
</s>
<s>
Regata	regata	k1gFnSc1	regata
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
a.	a.	k?	a.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
názvu	název	k1gInSc2	název
REGIO	REGIO	kA	REGIO
Máchova	Máchův	k2eAgInSc2d1	Máchův
kraje	kraj	k1gInSc2	kraj
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
provozuje	provozovat	k5eAaImIp3nS	provozovat
linkovou	linkový	k2eAgFnSc4d1	Linková
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
obousměrné	obousměrný	k2eAgFnSc6d1	obousměrná
okružní	okružní	k2eAgFnSc6d1	okružní
trase	trasa	k1gFnSc6	trasa
Doksy	Doksy	k1gInPc4	Doksy
-	-	kIx~	-
Staré	Staré	k2eAgInPc1d1	Staré
Splavy	splav	k1gInPc1	splav
-	-	kIx~	-
Borný	borný	k2eAgInSc1d1	borný
-	-	kIx~	-
Valdštejnská	valdštejnský	k2eAgFnSc1d1	Valdštejnská
plovárna	plovárna	k1gFnSc1	plovárna
-	-	kIx~	-
Doksy	Doksy	k1gInPc1	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jen	jen	k9	jen
o	o	k7c6	o
letních	letní	k2eAgFnPc6d1	letní
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jezdí	jezdit	k5eAaImIp3nP	jezdit
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
spojů	spoj	k1gInPc2	spoj
(	(	kIx(	(
<g/>
označovaných	označovaný	k2eAgFnPc2d1	označovaná
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
linka	linka	k1gFnSc1	linka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
plavby	plavba	k1gFnPc1	plavba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
minimálního	minimální	k2eAgInSc2d1	minimální
počtu	počet	k1gInSc2	počet
25	[number]	k4	25
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
plavby	plavba	k1gFnSc2	plavba
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
až	až	k9	až
75	[number]	k4	75
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
,	,	kIx,	,
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
plavby	plavba	k1gFnPc1	plavba
jen	jen	k9	jen
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
<g/>
,	,	kIx,	,
konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
též	též	k9	též
příležitostné	příležitostný	k2eAgFnPc1d1	příležitostná
plavby	plavba	k1gFnPc1	plavba
například	například	k6eAd1	například
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
linka	linka	k1gFnSc1	linka
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
na	na	k7c6	na
území	území	k1gNnSc6	území
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdné	k1gNnSc1	jízdné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
linkové	linkový	k2eAgFnSc6d1	Linková
dopravě	doprava	k1gFnSc6	doprava
až	až	k9	až
25	[number]	k4	25
Kč	Kč	kA	Kč
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
trasu	trasa	k1gFnSc4	trasa
linky	linka	k1gFnSc2	linka
pro	pro	k7c4	pro
dospělého	dospělý	k1gMnSc4	dospělý
a	a	k8xC	a
85	[number]	k4	85
Kč	Kč	kA	Kč
na	na	k7c6	na
nelinkové	linkový	k2eNgFnSc6d1	linkový
okružní	okružní	k2eAgFnSc6d1	okružní
plavbě	plavba	k1gFnSc6	plavba
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
pro	pro	k7c4	pro
plavbu	plavba	k1gFnSc4	plavba
jen	jen	k9	jen
v	v	k7c6	v
části	část	k1gFnSc6	část
linky	linka	k1gFnSc2	linka
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
zde	zde	k6eAd1	zde
provozuje	provozovat	k5eAaImIp3nS	provozovat
čtyři	čtyři	k4xCgFnPc4	čtyři
lodě	loď	k1gFnPc4	loď
<g/>
:	:	kIx,	:
Máj	máj	k1gFnSc1	máj
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
krajské	krajský	k2eAgFnSc2d1	krajská
studie	studie	k1gFnSc2	studie
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
informace	informace	k1gFnSc2	informace
dopravce	dopravce	k1gMnSc2	dopravce
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c6	na
vodu	voda	k1gFnSc4	voda
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
v	v	k7c6	v
loděnici	loděnice	k1gFnSc6	loděnice
Štěchovice	Štěchovice	k1gFnPc4	Štěchovice
<g/>
,	,	kIx,	,
obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
250	[number]	k4	250
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
(	(	kIx(	(
<g/>
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
1930	[number]	k4	1930
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Tista	Tista	k?	Tista
<g/>
,	,	kIx,	,
obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
70	[number]	k4	70
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Racek	racek	k1gMnSc1	racek
(	(	kIx(	(
<g/>
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
160	[number]	k4	160
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jarmila	Jarmila	k1gFnSc1	Jarmila
(	(	kIx(	(
<g/>
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
1924	[number]	k4	1924
v	v	k7c6	v
berlínských	berlínský	k2eAgFnPc6d1	Berlínská
loděnicích	loděnice	k1gFnPc6	loděnice
Dekumag	Dekumaga	k1gFnPc2	Dekumaga
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Maria	Maria	k1gFnSc1	Maria
<g/>
,	,	kIx,	,
obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
60	[number]	k4	60
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
se	se	k3xPyFc4	se
roční	roční	k2eAgInSc1d1	roční
počet	počet	k1gInSc1	počet
odbavených	odbavený	k2eAgMnPc2d1	odbavený
cestujících	cestující	k1gMnPc2	cestující
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
mezi	mezi	k7c4	mezi
41	[number]	k4	41
až	až	k9	až
55	[number]	k4	55
tisíci	tisíc	k4xCgInSc3	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
část	část	k1gFnSc1	část
tvořily	tvořit	k5eAaImAgFnP	tvořit
i	i	k8xC	i
plavby	plavba	k1gFnPc1	plavba
objednané	objednaný	k2eAgInPc1d1	objednaný
cestovními	cestovní	k2eAgFnPc7d1	cestovní
kancelářemi	kancelář	k1gFnPc7	kancelář
a	a	k8xC	a
plavby	plavba	k1gFnPc1	plavba
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
prezentací	prezentace	k1gFnSc7	prezentace
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
akce	akce	k1gFnPc1	akce
omezeny	omezen	k2eAgFnPc1d1	omezena
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
kleslo	klesnout	k5eAaPmAgNnS	klesnout
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
skoro	skoro	k6eAd1	skoro
celého	celý	k2eAgNnSc2d1	celé
jezera	jezero	k1gNnSc2	jezero
vede	vést	k5eAaImIp3nS	vést
okružní	okružní	k2eAgFnSc1d1	okružní
žlutá	žlutý	k2eAgFnSc1d1	žlutá
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
břehu	břeh	k1gInSc6	břeh
připojuje	připojovat	k5eAaImIp3nS	připojovat
trasa	trasa	k1gFnSc1	trasa
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
od	od	k7c2	od
severní	severní	k2eAgFnSc2d1	severní
zátoky	zátoka	k1gFnSc2	zátoka
přes	přes	k7c4	přes
vrch	vrch	k1gInSc4	vrch
Šroubený	Šroubený	k2eAgInSc4d1	Šroubený
do	do	k7c2	do
Starých	Starých	k2eAgInPc2d1	Starých
Splavů	splav	k1gInPc2	splav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
trasa	trasa	k1gFnSc1	trasa
pak	pak	k6eAd1	pak
vede	vést	k5eAaImIp3nS	vést
ze	z	k7c2	z
Starých	Starých	k2eAgInPc2d1	Starých
Splavů	splav	k1gInPc2	splav
do	do	k7c2	do
Doks	Doksy	k1gInPc2	Doksy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
žlutou	žlutý	k2eAgFnSc4d1	žlutá
trasu	trasa	k1gFnSc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
sem	sem	k6eAd1	sem
také	také	k9	také
cyklotrasy	cyklotrasa	k1gFnSc2	cyklotrasa
0	[number]	k4	0
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
3045	[number]	k4	3045
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
byly	být	k5eAaImAgFnP	být
kolem	kolem	k7c2	kolem
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
upraveny	upravit	k5eAaPmNgFnP	upravit
nové	nový	k2eAgFnPc1d1	nová
naučné	naučný	k2eAgFnPc1d1	naučná
stezky	stezka	k1gFnPc1	stezka
-	-	kIx~	-
dětská	dětský	k2eAgFnSc1d1	dětská
Se	s	k7c7	s
Čtyřlístkem	čtyřlístek	k1gInSc7	čtyřlístek
okolo	okolo	k7c2	okolo
Blaťáku	Blaťák	k1gMnSc3	Blaťák
(	(	kIx(	(
<g/>
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
předobrazem	předobraz	k1gInSc7	předobraz
rybníku	rybník	k1gInSc6	rybník
Blaťák	Blaťák	k1gMnSc1	Blaťák
v	v	k7c6	v
komiksu	komiks	k1gInSc6	komiks
Čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Swamp	Swamp	k1gMnSc1	Swamp
a	a	k8xC	a
Naučná	naučný	k2eAgFnSc1d1	naučná
Máchova	Máchův	k2eAgFnSc1d1	Máchova
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
díky	díky	k7c3	díky
dotacím	dotace	k1gFnPc3	dotace
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
zároveň	zároveň	k6eAd1	zároveň
vytvořeno	vytvořen	k2eAgNnSc1d1	vytvořeno
Muzeum	muzeum	k1gNnSc1	muzeum
Čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
a	a	k8xC	a
upravena	upraven	k2eAgFnSc1d1	upravena
Jarmilina	Jarmilin	k2eAgFnSc1d1	Jarmilina
stezka	stezka	k1gFnSc1	stezka
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
spojnici	spojnice	k1gFnSc4	spojnice
mezi	mezi	k7c7	mezi
Doksy	Doksy	k1gInPc7	Doksy
a	a	k8xC	a
Starými	starý	k2eAgInPc7d1	starý
Splavy	splav	k1gInPc7	splav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
břehu	břeh	k1gInSc6	břeh
leží	ležet	k5eAaImIp3nP	ležet
město	město	k1gNnSc4	město
Doksy	Doksy	k1gInPc4	Doksy
a	a	k8xC	a
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
břehu	břeh	k1gInSc6	břeh
obec	obec	k1gFnSc4	obec
Staré	Staré	k2eAgInPc1d1	Staré
Splavy	splav	k1gInPc1	splav
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
správní	správní	k2eAgFnSc7d1	správní
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
Doksy	Doksy	k1gInPc1	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
obcích	obec	k1gFnPc6	obec
jsou	být	k5eAaImIp3nP	být
železniční	železniční	k2eAgFnPc1d1	železniční
zastávky	zastávka	k1gFnPc1	zastávka
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
080	[number]	k4	080
od	od	k7c2	od
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
do	do	k7c2	do
Bakova	Bakov	k1gInSc2	Bakov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
břehu	břeh	k1gInSc6	břeh
je	být	k5eAaImIp3nS	být
Jarmilina	Jarmilin	k2eAgFnSc1d1	Jarmilina
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
-	-	kIx~	-
1938	[number]	k4	1938
pomník	pomník	k1gInSc1	pomník
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
.	.	kIx.	.
</s>
