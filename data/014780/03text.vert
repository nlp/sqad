<s>
XaoS	XaoS	k?
</s>
<s>
XaoS	XaoS	k?
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
4.1	4.1	k4
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Cross-platform	Cross-platform	k1gInSc4
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
generování	generování	k1gNnSc1
fraktálů	fraktál	k1gInPc2
Licence	licence	k1gFnSc2
</s>
<s>
GPL	GPL	kA
Lokalizace	lokalizace	k1gFnSc1
</s>
<s>
Anglicky	anglicky	k6eAd1
Web	web	k1gInSc1
</s>
<s>
https://xaos-project.github.io/	https://xaos-project.github.io/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
XaoS	XaoS	k?
je	být	k5eAaImIp3nS
interaktivní	interaktivní	k2eAgInSc1d1
program	program	k1gInSc1
pro	pro	k7c4
zobrazování	zobrazování	k1gNnSc4
fraktálů	fraktál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
uživateli	uživatel	k1gMnSc3
spojitě	spojitě	k6eAd1
přibližovat	přibližovat	k5eAaImF
a	a	k8xC
oddalovat	oddalovat	k5eAaImF
zobrazený	zobrazený	k2eAgInSc4d1
fraktál	fraktál	k1gInSc4
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
XaoS	XaoS	k?
je	být	k5eAaImIp3nS
licencován	licencován	k2eAgInSc1d1
pod	pod	k7c7
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
je	být	k5eAaImIp3nS
víceplatformní	víceplatformní	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
dostupný	dostupný	k2eAgInSc1d1
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
GNU	gnu	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
Windows	Windows	kA
<g/>
,	,	kIx,
Mac	Mac	kA
OS	OS	kA
X	X	kA
<g/>
,	,	kIx,
BeOS	BeOS	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
XaoS	XaoS	k?
umí	umět	k5eAaImIp3nS
zobrazit	zobrazit	k5eAaPmF
Mandelbrotovu	Mandelbrotův	k2eAgFnSc4d1
množinu	množina	k1gFnSc4
(	(	kIx(
<g/>
s	s	k7c7
mocninou	mocnina	k1gFnSc7
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
a	a	k8xC
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Octo	Octo	k6eAd1
fraktál	fraktál	k1gInSc1
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
typy	typ	k1gInPc4
Barnselyho	Barnsely	k1gMnSc4
fraktálů	fraktál	k1gInPc2
<g/>
,	,	kIx,
fraktál	fraktál	k1gInSc1
Newton	newton	k1gInSc1
(	(	kIx(
<g/>
řádu	řád	k1gInSc2
3	#num#	k4
a	a	k8xC
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fraktál	fraktál	k1gInSc1
Phoenix	Phoenix	k1gInSc1
a	a	k8xC
Magnet	magnet	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
XaoS	XaoS	k1gMnSc1
může	moct	k5eAaImIp3nS
zobrazit	zobrazit	k5eAaPmF
Juliovu	Juliův	k2eAgFnSc4d1
množinu	množina	k1gFnSc4
z	z	k7c2
vybraných	vybraný	k2eAgFnPc2d1
částí	část	k1gFnPc2
fraktálu	fraktál	k1gInSc2
a	a	k8xC
také	také	k9
umožňuje	umožňovat	k5eAaImIp3nS
zadání	zadání	k1gNnSc1
vlastních	vlastní	k2eAgInPc2d1
vzorců	vzorec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
XaoS	XaoS	k?
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
zobrazovat	zobrazovat	k5eAaImF
fraktály	fraktál	k1gInPc4
jako	jako	k8xS,k8xC
ASCII	ascii	kA
art	art	k?
pomocí	pomocí	k7c2
knihovny	knihovna	k1gFnSc2
AAlib	AAliba	k1gFnPc2
což	což	k3yQnSc1,k3yRnSc1
<g/>
,	,	kIx,
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
nástroji	nástroj	k1gInPc7
pro	pro	k7c4
sestavování	sestavování	k1gNnSc4
softwaru	software	k1gInSc2
od	od	k7c2
GNU	gnu	k1gNnSc2
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
provozovat	provozovat	k5eAaImF
téměř	téměř	k6eAd1
kdekoli	kdekoli	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
balíčku	balíček	k1gInSc6
je	být	k5eAaImIp3nS
zahrnuta	zahrnut	k2eAgFnSc1d1
interaktivní	interaktivní	k2eAgFnSc1d1
nápověda	nápověda	k1gFnSc1
a	a	k8xC
animovaný	animovaný	k2eAgInSc1d1
úvod	úvod	k1gInSc1
do	do	k7c2
problematiky	problematika	k1gFnSc2
fraktálů	fraktál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
desíti	deset	k4xCc2
kapitol	kapitola	k1gFnPc2
podle	podle	k7c2
dostupných	dostupný	k2eAgInPc2d1
vzorců	vzorec	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
XaoS	XaoS	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
XaoS	XaoS	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
stránka	stránka	k1gFnSc1
na	na	k7c4
GitHub	GitHub	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
