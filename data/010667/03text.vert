<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
psovitá	psovitý	k2eAgFnSc1d1	psovitá
šelma	šelma	k1gFnSc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Postupná	postupný	k2eAgFnSc1d1	postupná
domestikace	domestikace	k1gFnSc1	domestikace
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vydělení	vydělení	k1gNnSc3	vydělení
poddruhu	poddruh	k1gInSc2	poddruh
Canis	Canis	k1gFnSc2	Canis
lupus	lupus	k1gInSc1	lupus
familiaris	familiaris	k1gFnSc2	familiaris
–	–	k?	–
psa	pes	k1gMnSc4	pes
domácího	domácí	k2eAgMnSc4d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
značně	značně	k6eAd1	značně
omezen	omezit	k5eAaPmNgInS	omezit
–	–	k?	–
jeho	jeho	k3xOp3gInPc4	jeho
stavy	stav	k1gInPc4	stav
radikálně	radikálně	k6eAd1	radikálně
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
byl	být	k5eAaImAgInS	být
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
veden	vést	k5eAaImNgMnS	vést
jako	jako	k9	jako
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgMnSc1d1	dotčený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
16	[number]	k4	16
až	až	k9	až
80	[number]	k4	80
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
smečkách	smečka	k1gFnPc6	smečka
tvořených	tvořený	k2eAgFnPc6d1	tvořená
obvykle	obvykle	k6eAd1	obvykle
vedoucím	vedoucí	k2eAgNnSc7d1	vedoucí
rodičovským	rodičovský	k2eAgNnSc7d1	rodičovské
(	(	kIx(	(
<g/>
alfa	alfa	k1gNnSc1	alfa
<g/>
)	)	kIx)	)
párem	pár	k1gInSc7	pár
a	a	k8xC	a
několika	několik	k4yIc7	několik
jejich	jejich	k3xOp3gMnSc7	jejich
potomky	potomek	k1gMnPc7	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
různorodou	různorodý	k2eAgFnSc7d1	různorodá
kořistí	kořist	k1gFnSc7	kořist
od	od	k7c2	od
hmyzu	hmyz	k1gInSc2	hmyz
až	až	k9	až
po	po	k7c4	po
bizony	bizon	k1gMnPc4	bizon
a	a	k8xC	a
pižmoně	pižmoň	k1gMnPc4	pižmoň
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc7	jejich
nepřáteli	nepřítel	k1gMnPc7	nepřítel
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
člověka	člověk	k1gMnSc2	člověk
také	také	k9	také
tygři	tygr	k1gMnPc1	tygr
a	a	k8xC	a
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mají	mít	k5eAaImIp3nP	mít
prominentní	prominentní	k2eAgNnSc4d1	prominentní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
mytologii	mytologie	k1gFnSc6	mytologie
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
množství	množství	k1gNnSc6	množství
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
a	a	k8xC	a
ság	sága	k1gFnPc2	sága
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
vlky	vlk	k1gMnPc7	vlk
od	od	k7c2	od
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	on	k3xPp3gMnPc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
škůdce	škůdce	k1gMnPc4	škůdce
a	a	k8xC	a
báli	bát	k5eAaImAgMnP	bát
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc1	vzhled
<g/>
,	,	kIx,	,
smysly	smysl	k1gInPc1	smysl
<g/>
,	,	kIx,	,
výkony	výkon	k1gInPc1	výkon
==	==	k?	==
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
severních	severní	k2eAgMnPc2d1	severní
vlků	vlk	k1gMnPc2	vlk
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
kg	kg	kA	kg
<g/>
,	,	kIx,	,
jedinci	jedinec	k1gMnPc1	jedinec
nad	nad	k7c4	nad
54	[number]	k4	54
kg	kg	kA	kg
jsou	být	k5eAaImIp3nP	být
neobvyklí	obvyklý	k2eNgMnPc1d1	neobvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzená	potvrzený	k2eAgFnSc1d1	potvrzená
rekordní	rekordní	k2eAgFnSc1d1	rekordní
hmotnost	hmotnost	k1gFnSc1	hmotnost
severoamerického	severoamerický	k2eAgMnSc2d1	severoamerický
vlka	vlk	k1gMnSc2	vlk
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
kg	kg	kA	kg
<g/>
,	,	kIx,	,
euroasijského	euroasijský	k2eAgNnSc2d1	Euroasijské
86	[number]	k4	86
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Nepotvrzený	potvrzený	k2eNgMnSc1d1	nepotvrzený
rekordní	rekordní	k2eAgMnSc1d1	rekordní
jedinec	jedinec	k1gMnSc1	jedinec
vážil	vážit	k5eAaImAgMnS	vážit
údajně	údajně	k6eAd1	údajně
103	[number]	k4	103
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInPc1d1	jižní
poddruhy	poddruh	k1gInPc1	poddruh
váží	vážit	k5eAaImIp3nP	vážit
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
okolo	okolo	k7c2	okolo
30	[number]	k4	30
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Indičtí	indický	k2eAgMnPc1d1	indický
vlci	vlk	k1gMnPc1	vlk
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnPc4	hmotnost
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jen	jen	k9	jen
okolo	okolo	k7c2	okolo
25	[number]	k4	25
kg	kg	kA	kg
<g/>
,	,	kIx,	,
severoafričtí	severoafrický	k2eAgMnPc1d1	severoafrický
a	a	k8xC	a
arabští	arabský	k2eAgMnPc1d1	arabský
pod	pod	k7c4	pod
20	[number]	k4	20
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
<g/>
:	:	kIx,	:
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
115	[number]	k4	115
cm	cm	kA	cm
<g/>
.	.	kIx.	.
<g/>
Vlk	Vlk	k1gMnSc1	Vlk
obecný	obecný	k2eAgMnSc1d1	obecný
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
psovitá	psovitý	k2eAgFnSc1d1	psovitá
šelma	šelma	k1gFnSc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
bývají	bývat	k5eAaImIp3nP	bývat
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
vlka	vlk	k1gMnSc2	vlk
záleží	záležet	k5eAaImIp3nS	záležet
také	také	k9	také
na	na	k7c6	na
poddruhu	poddruh	k1gInSc6	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
splňují	splňovat	k5eAaImIp3nP	splňovat
Bergmannovo	Bergmannův	k2eAgNnSc4d1	Bergmannovo
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
vlci	vlk	k1gMnPc1	vlk
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
poddruhy	poddruh	k1gInPc1	poddruh
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
podobá	podobat	k5eAaImIp3nS	podobat
německému	německý	k2eAgInSc3d1	německý
ovčáckému	ovčácký	k2eAgInSc3d1	ovčácký
psu	pes	k1gMnSc6	pes
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
několika	několik	k4yIc6	několik
drobnostech	drobnost	k1gFnPc6	drobnost
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
širší	široký	k2eAgFnSc4d2	širší
a	a	k8xC	a
zašpičatělejší	zašpičatělý	k2eAgFnSc4d2	zašpičatělý
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
šikměji	šikmo	k6eAd2	šikmo
postavené	postavený	k2eAgNnSc1d1	postavené
oči	oko	k1gNnPc1	oko
a	a	k8xC	a
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
trojúhelníkovité	trojúhelníkovitý	k2eAgFnPc4d1	trojúhelníkovitá
uši	ucho	k1gNnPc4	ucho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
mají	mít	k5eAaImIp3nP	mít
42	[number]	k4	42
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Vlčí	vlčí	k2eAgInSc1d1	vlčí
špičák	špičák	k1gInSc1	špičák
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
až	až	k9	až
6,5	[number]	k4	6,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
silné	silný	k2eAgInPc1d1	silný
trháky	trhák	k1gInPc1	trhák
a	a	k8xC	a
mohutné	mohutný	k2eAgInPc1d1	mohutný
žvýkací	žvýkací	k2eAgInPc1d1	žvýkací
svaly	sval	k1gInPc1	sval
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vlkovi	vlk	k1gMnSc3	vlk
chytit	chytit	k5eAaPmF	chytit
a	a	k8xC	a
zabít	zabít	k5eAaPmF	zabít
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
obecný	obecný	k2eAgMnSc1d1	obecný
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ve	v	k7c6	v
stisku	stisk	k1gInSc6	stisk
vyvinout	vyvinout	k5eAaPmF	vyvinout
sílu	síla	k1gFnSc4	síla
na	na	k7c4	na
špičák	špičák	k1gInSc4	špičák
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
740	[number]	k4	740
N	N	kA	N
(	(	kIx(	(
<g/>
74	[number]	k4	74
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
cm2	cm2	k4	cm2
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
trhácích	trhák	k1gInPc6	trhák
1200-1400	[number]	k4	1200-1400
N	N	kA	N
(	(	kIx(	(
<g/>
120	[number]	k4	120
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
cm2	cm2	k4	cm2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
trhákový	trhákový	k2eAgInSc1d1	trhákový
komplex	komplex	k1gInSc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
pitbulové	pitbulový	k2eAgNnSc4d1	pitbulové
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vlčáci	vlčák	k1gMnPc1	vlčák
kolem	kolem	k7c2	kolem
1300	[number]	k4	1300
N	N	kA	N
(	(	kIx(	(
<g/>
130	[number]	k4	130
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
cm2	cm2	k4	cm2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rekordmany	rekordman	k1gMnPc4	rekordman
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
psi	pes	k1gMnPc1	pes
mastifové	mastifový	k2eAgInPc1d1	mastifový
s	s	k7c7	s
průměrnými	průměrný	k2eAgInPc7d1	průměrný
1700	[number]	k4	1700
N	N	kA	N
(	(	kIx(	(
<g/>
170	[number]	k4	170
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
cm2	cm2	k4	cm2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlčí	vlčí	k2eAgFnSc1d1	vlčí
srst	srst	k1gFnSc1	srst
===	===	k?	===
</s>
</p>
<p>
<s>
Vlčí	vlčí	k2eAgFnSc1d1	vlčí
srst	srst	k1gFnSc1	srst
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
:	:	kIx,	:
vrchní	vrchní	k2eAgFnSc1d1	vrchní
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
hustými	hustý	k2eAgInPc7d1	hustý
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
podsada	podsada	k1gFnSc1	podsada
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Izolační	izolační	k2eAgFnSc4d1	izolační
schopnost	schopnost	k1gFnSc4	schopnost
vlčí	vlčí	k2eAgFnSc2d1	vlčí
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
vlkovi	vlk	k1gMnSc6	vlk
netaje	tát	k5eNaImIp3nS	tát
sníh	sníh	k1gInSc1	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
mají	mít	k5eAaImIp3nP	mít
huňatý	huňatý	k2eAgInSc4d1	huňatý
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
přikrývku	přikrývka	k1gFnSc4	přikrývka
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInPc1d1	severní
poddruhy	poddruh	k1gInPc1	poddruh
mají	mít	k5eAaImIp3nP	mít
srst	srst	k1gFnSc4	srst
také	také	k9	také
podstatně	podstatně	k6eAd1	podstatně
delší	dlouhý	k2eAgMnSc1d2	delší
a	a	k8xC	a
hustší	hustý	k2eAgInSc1d2	hustší
než	než	k8xS	než
jižní	jižní	k2eAgInSc1d1	jižní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
lidech	člověk	k1gMnPc6	člověk
nejrozmanitějším	rozmanitý	k2eAgInSc7d3	nejrozmanitější
druhem	druh	k1gInSc7	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
jejich	jejich	k3xOp3gFnSc4	jejich
srst	srst	k1gFnSc4	srst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
čistě	čistě	k6eAd1	čistě
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
úplně	úplně	k6eAd1	úplně
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
vybarvená	vybarvený	k2eAgFnSc1d1	vybarvená
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
odstínech	odstín	k1gInPc6	odstín
šedé	šedá	k1gFnSc2	šedá
<g/>
,	,	kIx,	,
skořicová	skořicový	k2eAgFnSc1d1	skořicová
<g/>
,	,	kIx,	,
krémová	krémový	k2eAgFnSc1d1	krémová
<g/>
,	,	kIx,	,
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
i	i	k8xC	i
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Možné	možný	k2eAgInPc1d1	možný
jsou	být	k5eAaImIp3nP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
všechny	všechen	k3xTgInPc1	všechen
přechody	přechod	k1gInPc1	přechod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
také	také	k9	také
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1	zimní
srst	srst	k1gFnSc1	srst
bývá	bývat	k5eAaImIp3nS	bývat
světlejší	světlý	k2eAgFnSc1d2	světlejší
a	a	k8xC	a
hustší	hustý	k2eAgFnSc1d2	hustší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
vlků	vlk	k1gMnPc2	vlk
má	mít	k5eAaImIp3nS	mít
tmavší	tmavý	k2eAgInSc4d2	tmavší
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
světlejší	světlý	k2eAgNnSc4d2	světlejší
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mívají	mívat	k5eAaImIp3nP	mívat
tmavší	tmavý	k2eAgFnSc4d2	tmavší
masku	maska	k1gFnSc4	maska
okolo	okolo	k7c2	okolo
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
vlka	vlk	k1gMnSc2	vlk
eurasijského	eurasijský	k2eAgMnSc2d1	eurasijský
<g/>
,	,	kIx,	,
poddruhu	poddruh	k1gInSc2	poddruh
vlka	vlk	k1gMnSc2	vlk
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
podle	podle	k7c2	podle
sezóny	sezóna	k1gFnSc2	sezóna
rezavohnědý	rezavohnědý	k2eAgInSc4d1	rezavohnědý
až	až	k6eAd1	až
šedočerný	šedočerný	k2eAgInSc4d1	šedočerný
odstín	odstín	k1gInSc4	odstín
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
končetin	končetina	k1gFnPc2	končetina
jsou	být	k5eAaImIp3nP	být
nažloutlé	nažloutlý	k2eAgInPc1d1	nažloutlý
až	až	k9	až
bělavé	bělavý	k2eAgInPc1d1	bělavý
a	a	k8xC	a
vnější	vnější	k2eAgInPc1d1	vnější
okraje	okraj	k1gInPc1	okraj
ušních	ušní	k2eAgMnPc2d1	ušní
boltců	boltec	k1gInPc2	boltec
černé	černá	k1gFnSc2	černá
<g/>
.	.	kIx.	.
</s>
<s>
Pruh	pruh	k1gInSc1	pruh
tmavší	tmavý	k2eAgFnSc2d2	tmavší
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
táhne	táhnout	k5eAaImIp3nS	táhnout
i	i	k9	i
středem	střed	k1gInSc7	střed
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
obývající	obývající	k2eAgFnSc4d1	obývající
tundru	tundra	k1gFnSc4	tundra
a	a	k8xC	a
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
bývají	bývat	k5eAaImIp3nP	bývat
i	i	k9	i
celoročně	celoročně	k6eAd1	celoročně
úplně	úplně	k6eAd1	úplně
bílí	bílý	k2eAgMnPc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Změna	změna	k1gFnSc1	změna
barvy	barva	k1gFnSc2	barva
====	====	k?	====
</s>
</p>
<p>
<s>
Byly	být	k5eAaImAgFnP	být
popsány	popsat	k5eAaPmNgInP	popsat
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vlk	vlk	k1gMnSc1	vlk
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
úplně	úplně	k6eAd1	úplně
změnil	změnit	k5eAaPmAgInS	změnit
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Štěňata	štěně	k1gNnPc1	štěně
polárních	polární	k2eAgMnPc2d1	polární
vlků	vlk	k1gMnPc2	vlk
mají	mít	k5eAaImIp3nP	mít
krémovou	krémový	k2eAgFnSc4d1	krémová
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
až	až	k9	až
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
vyblednou	vyblednout	k5eAaPmIp3nP	vyblednout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
úplně	úplně	k6eAd1	úplně
černí	černý	k2eAgMnPc1d1	černý
vlci	vlk	k1gMnPc1	vlk
mohou	moct	k5eAaImIp3nP	moct
postupem	postupem	k7c2	postupem
let	léto	k1gNnPc2	léto
získávat	získávat	k5eAaImF	získávat
stále	stále	k6eAd1	stále
světlejší	světlý	k2eAgFnSc4d2	světlejší
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
z	z	k7c2	z
černého	černé	k1gNnSc2	černé
vlka	vlk	k1gMnSc2	vlk
stane	stanout	k5eAaPmIp3nS	stanout
i	i	k9	i
bílý	bílý	k1gMnSc1	bílý
vlk	vlk	k1gMnSc1	vlk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smysly	smysl	k1gInPc4	smysl
===	===	k?	===
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
průměrný	průměrný	k2eAgInSc1d1	průměrný
zrak	zrak	k1gInSc1	zrak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výborný	výborný	k2eAgInSc4d1	výborný
sluch	sluch	k1gInSc4	sluch
a	a	k8xC	a
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
zřejmě	zřejmě	k6eAd1	zřejmě
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
zrak	zrak	k1gInSc1	zrak
je	být	k5eAaImIp3nS	být
koncentrován	koncentrovat	k5eAaBmNgInS	koncentrovat
na	na	k7c4	na
siluetu	silueta	k1gFnSc4	silueta
a	a	k8xC	a
pohyb	pohyb	k1gInSc4	pohyb
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
zvuky	zvuk	k1gInPc4	zvuk
do	do	k7c2	do
kmitočtu	kmitočet	k1gInSc2	kmitočet
26	[number]	k4	26
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Umějí	umět	k5eAaImIp3nP	umět
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
pohyb	pohyb	k1gInSc4	pohyb
pod	pod	k7c7	pod
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
pokrývkou	pokrývka	k1gFnSc7	pokrývka
či	či	k8xC	či
zvuky	zvuk	k1gInPc7	zvuk
v	v	k7c6	v
lese	les	k1gInSc6	les
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Čich	čich	k1gInSc1	čich
vlků	vlk	k1gMnPc2	vlk
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
citlivý	citlivý	k2eAgInSc1d1	citlivý
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
něj	on	k3xPp3gNnSc2	on
dokáží	dokázat	k5eAaPmIp3nP	dokázat
rozlišit	rozlišit	k5eAaPmF	rozlišit
mnoho	mnoho	k4c4	mnoho
informací	informace	k1gFnPc2	informace
–	–	k?	–
o	o	k7c6	o
potenciální	potenciální	k2eAgFnSc6d1	potenciální
kořisti	kořist	k1gFnSc6	kořist
<g/>
,	,	kIx,	,
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
staly	stát	k5eAaPmAgFnP	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
běhu	běh	k1gInSc2	běh
<g/>
:	:	kIx,	:
58	[number]	k4	58
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
maximální	maximální	k2eAgFnSc4d1	maximální
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
64	[number]	k4	64
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Vlk	Vlk	k1gMnSc1	Vlk
dokáže	dokázat	k5eAaPmIp3nS	dokázat
běžet	běžet	k5eAaImF	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
až	až	k6eAd1	až
okolo	okolo	k7c2	okolo
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
důvodu	důvod	k1gInSc2	důvod
běhu	běh	k1gInSc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
může	moct	k5eAaImIp3nS	moct
urazit	urazit	k5eAaPmF	urazit
mnoho	mnoho	k4c4	mnoho
desítek	desítka	k1gFnPc2	desítka
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
zaznamenané	zaznamenaný	k2eAgNnSc1d1	zaznamenané
maximum	maximum	k1gNnSc1	maximum
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
když	když	k8xS	když
vlk	vlk	k1gMnSc1	vlk
neloví	lovit	k5eNaImIp3nS	lovit
nebo	nebo	k8xC	nebo
neprchá	prchat	k5eNaImIp3nS	prchat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
8-10	[number]	k4	8-10
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
přizpůsobivým	přizpůsobivý	k2eAgInSc7d1	přizpůsobivý
živočišným	živočišný	k2eAgInSc7d1	živočišný
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
žít	žít	k5eAaImF	žít
v	v	k7c6	v
pouštích	poušť	k1gFnPc6	poušť
<g/>
,	,	kIx,	,
suchých	suchý	k2eAgFnPc6d1	suchá
stepích	step	k1gFnPc6	step
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
bažinách	bažina	k1gFnPc6	bažina
i	i	k8xC	i
v	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
poddruzích	poddruh	k1gInPc6	poddruh
obývali	obývat	k5eAaImAgMnP	obývat
celou	celý	k2eAgFnSc4d1	celá
severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc4	rozšíření
vlka	vlk	k1gMnSc2	vlk
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
Asie	Asie	k1gFnSc2	Asie
kromě	kromě	k7c2	kromě
tropického	tropický	k2eAgInSc2d1	tropický
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
také	také	k9	také
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
od	od	k7c2	od
Aljašky	Aljaška	k1gFnSc2	Aljaška
až	až	k9	až
po	po	k7c4	po
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
obýval	obývat	k5eAaImAgInS	obývat
i	i	k9	i
mnohé	mnohý	k2eAgInPc4d1	mnohý
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
nebo	nebo	k8xC	nebo
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
však	však	k9	však
byl	být	k5eAaImAgInS	být
většinou	většina	k1gFnSc7	většina
již	již	k6eAd1	již
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
byl	být	k5eAaImAgInS	být
vlk	vlk	k1gMnSc1	vlk
člověkem	člověk	k1gMnSc7	člověk
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
pronásledování	pronásledování	k1gNnSc2	pronásledování
a	a	k8xC	a
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trvaly	trvat	k5eAaImAgFnP	trvat
až	až	k9	až
do	do	k7c2	do
nedávných	dávný	k2eNgNnPc2d1	nedávné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
zmizel	zmizet	k5eAaPmAgMnS	zmizet
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nenávratně	návratně	k6eNd1	návratně
ztraceno	ztratit	k5eAaPmNgNnS	ztratit
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
poddruhů	poddruh	k1gInPc2	poddruh
vlka	vlk	k1gMnSc2	vlk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Poddruhy	poddruh	k1gInPc1	poddruh
vlka	vlk	k1gMnSc2	vlk
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
eurasijské	eurasijský	k2eAgNnSc4d1	eurasijské
<g/>
,	,	kIx,	,
americké	americký	k2eAgNnSc4d1	americké
<g/>
,	,	kIx,	,
psy	pes	k1gMnPc7	pes
a	a	k8xC	a
dingy	dingo	k1gMnPc7	dingo
(	(	kIx(	(
<g/>
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poddruhy	poddruh	k1gInPc1	poddruh
označené	označený	k2eAgInPc1d1	označený
†	†	k?	†
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
vyhubené	vyhubený	k2eAgNnSc4d1	vyhubené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
eurasijské	eurasijský	k2eAgInPc1d1	eurasijský
poddruhy	poddruh	k1gInPc1	poddruh
vlka	vlk	k1gMnSc2	vlk
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
polární	polární	k2eAgMnSc1d1	polární
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
albus	albus	k1gInSc4	albus
<g/>
)	)	kIx)	)
–	–	k?	–
velký	velký	k2eAgInSc4d1	velký
poddruh	poddruh	k1gInSc4	poddruh
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
téměř	téměř	k6eAd1	téměř
bílý	bílý	k2eAgInSc1d1	bílý
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
arabský	arabský	k2eAgMnSc1d1	arabský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
arabs	arabs	k1gInSc4	arabs
<g/>
)	)	kIx)	)
–	–	k?	–
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgMnPc2d3	nejmenší
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zbarven	zbarven	k2eAgMnSc1d1	zbarven
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
stepní	stepní	k2eAgMnSc1d1	stepní
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
campestris	campestris	k1gFnSc1	campestris
<g/>
)	)	kIx)	)
–	–	k?	–
světlý	světlý	k2eAgInSc1d1	světlý
<g/>
,	,	kIx,	,
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
vlk	vlk	k1gMnSc1	vlk
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
srstí	srst	k1gFnSc7	srst
<g/>
,	,	kIx,	,
ze	z	k7c2	z
stepí	step	k1gFnPc2	step
jižního	jižní	k2eAgNnSc2d1	jižní
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
mongolský	mongolský	k2eAgMnSc1d1	mongolský
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
chanco	chanco	k1gMnSc1	chanco
<g/>
)	)	kIx)	)
–	–	k?	–
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgMnSc1d1	velký
vlk	vlk	k1gMnSc1	vlk
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
srstí	srst	k1gFnSc7	srst
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
barvu	barva	k1gFnSc4	barva
sezónně	sezónně	k6eAd1	sezónně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
a	a	k8xC	a
Tibetu	Tibet	k1gInSc6	Tibet
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
španělský	španělský	k2eAgMnSc1d1	španělský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
deitanus	deitanus	k1gInSc4	deitanus
<g/>
)	)	kIx)	)
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
s	s	k7c7	s
narudlou	narudlý	k2eAgFnSc7d1	narudlá
srstí	srst	k1gFnSc7	srst
<g/>
;	;	kIx,	;
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
ostrovní	ostrovní	k2eAgMnSc1d1	ostrovní
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnPc1	Canis
lupus	lupus	k1gInSc4	lupus
hattai	hatta	k1gFnSc2	hatta
<g/>
)	)	kIx)	)
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
a	a	k8xC	a
Sachalin	Sachalin	k1gInSc1	Sachalin
<g/>
,	,	kIx,	,
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
japonský	japonský	k2eAgMnSc1d1	japonský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
hodophilax	hodophilax	k1gInSc4	hodophilax
<g/>
)	)	kIx)	)
–	–	k?	–
nejmenší	malý	k2eAgFnSc1d3	nejmenší
<g/>
,	,	kIx,	,
žlutavě	žlutavě	k6eAd1	žlutavě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
poddruh	poddruh	k1gInSc4	poddruh
vlka	vlk	k1gMnSc2	vlk
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Honšú	Honšú	k1gFnSc2	Honšú
<g/>
,	,	kIx,	,
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
eurasijský	eurasijský	k2eAgMnSc1d1	eurasijský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
lupus	lupus	k1gInSc4	lupus
<g/>
)	)	kIx)	)
–	–	k?	–
nominátní	nominátní	k2eAgInSc4d1	nominátní
poddruh	poddruh	k1gInSc4	poddruh
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc4d1	velký
<g/>
,	,	kIx,	,
šedohnědě	šedohnědě	k6eAd1	šedohnědě
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
,	,	kIx,	,
obývá	obývat	k5eAaImIp3nS	obývat
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
Sibiř	Sibiř	k1gFnSc4	Sibiř
</s>
</p>
<p>
<s>
†	†	k?	†
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
minor	minor	k2eAgInSc1d1	minor
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
popsaný	popsaný	k2eAgInSc1d1	popsaný
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
pallipes	pallipes	k1gInSc4	pallipes
<g/>
)	)	kIx)	)
–	–	k?	–
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
pískově	pískově	k6eAd1	pískově
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgInSc4d1	obývající
Írán	Írán	k1gInSc4	Írán
<g/>
,	,	kIx,	,
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
a	a	k8xC	a
Indii	Indie	k1gFnSc4	Indie
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
iberský	iberský	k2eAgMnSc1d1	iberský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
signatus	signatus	k1gInSc4	signatus
<g/>
)	)	kIx)	)
–	–	k?	–
tmavě	tmavě	k6eAd1	tmavě
hnědý	hnědý	k2eAgMnSc1d1	hnědý
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrovaamerické	poloostrovaamerický	k2eAgInPc4d1	poloostrovaamerický
poddruhy	poddruh	k1gInPc4	poddruh
vlka	vlk	k1gMnSc2	vlk
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
kenajský	kenajský	k2eAgMnSc1d1	kenajský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
alces	alces	k1gInSc4	alces
<g/>
)	)	kIx)	)
–	–	k?	–
největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc1d1	známý
poddruh	poddruh	k1gInSc1	poddruh
vlka	vlk	k1gMnSc2	vlk
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
vyhubený	vyhubený	k2eAgInSc1d1	vyhubený
(	(	kIx(	(
<g/>
do	do	k7c2	do
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
arktický	arktický	k2eAgMnSc1d1	arktický
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
arctos	arctos	k1gInSc4	arctos
<g/>
)	)	kIx)	)
–	–	k?	–
bíle	bíle	k6eAd1	bíle
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
poddruh	poddruh	k1gInSc4	poddruh
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
ostrovů	ostrov	k1gInPc2	ostrov
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
mexický	mexický	k2eAgMnSc1d1	mexický
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnPc1	Canis
lupus	lupus	k1gInSc4	lupus
baileyi	bailey	k1gFnSc2	bailey
<g/>
)	)	kIx)	)
–	–	k?	–
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
světlý	světlý	k2eAgInSc1d1	světlý
poddruh	poddruh	k1gInSc1	poddruh
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
západního	západní	k2eAgInSc2d1	západní
Texasu	Texas	k1gInSc2	Texas
a	a	k8xC	a
Arizony	Arizona	k1gFnSc2	Arizona
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
novofoundlandský	novofoundlandský	k2eAgMnSc1d1	novofoundlandský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
beothucus	beothucus	k1gInSc4	beothucus
<g/>
)	)	kIx)	)
–	–	k?	–
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
poddruh	poddruh	k1gInSc1	poddruh
s	s	k7c7	s
nápadně	nápadně	k6eAd1	nápadně
velkou	velký	k2eAgFnSc7d1	velká
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Newfoundland	Newfoundlanda	k1gFnPc2	Newfoundlanda
<g/>
;	;	kIx,	;
již	již	k6eAd1	již
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
bernardi	bernard	k1gMnPc1	bernard
–	–	k?	–
poddruh	poddruh	k1gInSc4	poddruh
známý	známý	k2eAgInSc4d1	známý
z	z	k7c2	z
Banksova	Banksův	k2eAgInSc2d1	Banksův
a	a	k8xC	a
Viktoriina	Viktoriin	k2eAgInSc2d1	Viktoriin
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
kolumbijský	kolumbijský	k2eAgMnSc1d1	kolumbijský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
columbianus	columbianus	k1gInSc4	columbianus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
vancouverský	vancouverský	k2eAgMnSc1d1	vancouverský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
crassodon	crassodon	k1gInSc4	crassodon
<g/>
)	)	kIx)	)
–	–	k?	–
endemit	endemit	k1gInSc4	endemit
ostrova	ostrov	k1gInSc2	ostrov
Vancouver	Vancouver	k1gInSc1	Vancouver
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
fuscus	fuscus	k1gInSc4	fuscus
<g/>
)	)	kIx)	)
–	–	k?	–
nápadně	nápadně	k6eAd1	nápadně
tmavý	tmavý	k2eAgMnSc1d1	tmavý
vlk	vlk	k1gMnSc1	vlk
obývající	obývající	k2eAgMnSc1d1	obývající
Kaskádové	kaskádový	k2eAgNnSc4d1	kaskádové
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
;	;	kIx,	;
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
manitobský	manitobský	k2eAgMnSc1d1	manitobský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
griseoalbus	griseoalbus	k1gInSc4	griseoalbus
<g/>
)	)	kIx)	)
–	–	k?	–
vyhubený	vyhubený	k2eAgInSc4d1	vyhubený
poddruh	poddruh	k1gInSc4	poddruh
z	z	k7c2	z
Manitoby	Manitoba	k1gFnSc2	Manitoba
a	a	k8xC	a
severního	severní	k2eAgInSc2d1	severní
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
Hudsonův	Hudsonův	k2eAgMnSc1d1	Hudsonův
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
hudsonicus	hudsonicus	k1gInSc4	hudsonicus
<g/>
)	)	kIx)	)
–	–	k?	–
světlý	světlý	k2eAgMnSc1d1	světlý
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
</s>
</p>
<p>
<s>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
irremotus	irremotus	k1gInSc1	irremotus
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
známý	známý	k2eAgInSc1d1	známý
z	z	k7c2	z
Alberty	Alberta	k1gFnSc2	Alberta
a	a	k8xC	a
Wyomingu	Wyoming	k1gInSc2	Wyoming
<g/>
,	,	kIx,	,
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
labradorský	labradorský	k2eAgMnSc1d1	labradorský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
labradorius	labradorius	k1gInSc4	labradorius
<g/>
)	)	kIx)	)
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Québecu	Québecus	k1gInSc6	Québecus
a	a	k8xC	a
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Labrador	labrador	k1gMnSc1	labrador
</s>
</p>
<p>
<s>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
ligoni	ligoň	k1gFnSc3	ligoň
–	–	k?	–
vlk	vlk	k1gMnSc1	vlk
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
Alexandrově	Alexandrův	k2eAgNnSc6d1	Alexandrovo
souostroví	souostroví	k1gNnSc6	souostroví
u	u	k7c2	u
Aljašky	Aljaška	k1gFnSc2	Aljaška
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
lycaon	lycaon	k1gInSc4	lycaon
<g/>
)	)	kIx)	)
–	–	k?	–
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Ontario	Ontario	k1gNnSc1	Ontario
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnPc1d1	přilehlá
části	část	k1gFnPc1	část
Québecu	Québecus	k1gInSc2	Québecus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Canis	Canis	k1gFnPc2	Canis
lupus	lupus	k1gInSc4	lupus
mackenzii	mackenzie	k1gFnSc4	mackenzie
–	–	k?	–
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
</s>
</p>
<p>
<s>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
manningi	manning	k1gFnSc2	manning
–	–	k?	–
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
Baffinova	Baffinův	k2eAgInSc2d1	Baffinův
ostrova	ostrov	k1gInSc2	ostrov
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
arizonský	arizonský	k2eAgMnSc1d1	arizonský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
mogollonensis	mogollonensis	k1gFnSc1	mogollonensis
<g/>
)	)	kIx)	)
–	–	k?	–
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
tmavý	tmavý	k2eAgInSc1d1	tmavý
poddruh	poddruh	k1gInSc1	poddruh
z	z	k7c2	z
Arizony	Arizona	k1gFnSc2	Arizona
a	a	k8xC	a
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
texaský	texaský	k2eAgMnSc1d1	texaský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
monstrabilis	monstrabilis	k1gFnSc1	monstrabilis
<g/>
)	)	kIx)	)
–	–	k?	–
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
tmavý	tmavý	k2eAgInSc1d1	tmavý
poddruh	poddruh	k1gInSc1	poddruh
z	z	k7c2	z
východního	východní	k2eAgInSc2d1	východní
Texasu	Texas	k1gInSc2	Texas
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
prériový	prériový	k2eAgMnSc1d1	prériový
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
nubilus	nubilus	k1gInSc4	nubilus
<g/>
)	)	kIx)	)
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
takto	takto	k6eAd1	takto
označován	označovat	k5eAaImNgInS	označovat
jen	jen	k9	jen
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
prérií	prérie	k1gFnPc2	prérie
Středozápadu	středozápad	k1gInSc2	středozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
podddruh	podddruh	k1gInSc1	podddruh
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vyhubený	vyhubený	k2eAgInSc4d1	vyhubený
<g/>
;	;	kIx,	;
nové	nový	k2eAgInPc1d1	nový
výzkumy	výzkum	k1gInPc1	výzkum
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
současní	současný	k2eAgMnPc1d1	současný
vlci	vlk	k1gMnPc1	vlk
z	z	k7c2	z
Minnesoty	Minnesota	k1gFnSc2	Minnesota
<g/>
,	,	kIx,	,
Wisconsinu	Wisconsina	k1gFnSc4	Wisconsina
a	a	k8xC	a
Michiganu	Michigan	k1gInSc6	Michigan
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
poddruhu	poddruh	k1gInSc3	poddruh
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
kanadský	kanadský	k2eAgMnSc1d1	kanadský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
occidentalis	occidentalis	k1gFnSc1	occidentalis
<g/>
)	)	kIx)	)
–	–	k?	–
bílý	bílý	k2eAgInSc4d1	bílý
poddruh	poddruh	k1gInSc4	poddruh
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
možná	možná	k9	možná
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
vlkem	vlk	k1gMnSc7	vlk
arktickým	arktický	k2eAgMnSc7d1	arktický
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
grónský	grónský	k2eAgMnSc1d1	grónský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
orion	orion	k1gInSc4	orion
<g/>
)	)	kIx)	)
–	–	k?	–
menší	malý	k2eAgInSc1d2	menší
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgMnSc1d1	bílý
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
,	,	kIx,	,
možná	možná	k9	možná
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
vlkem	vlk	k1gMnSc7	vlk
arktickým	arktický	k2eAgMnSc7d1	arktický
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
pambasileus	pambasileus	k1gInSc1	pambasileus
<g/>
)	)	kIx)	)
–	–	k?	–
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
tmavý	tmavý	k2eAgMnSc1d1	tmavý
vlk	vlk	k1gMnSc1	vlk
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Aljašky	Aljaška	k1gFnSc2	Aljaška
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
polární	polární	k2eAgMnSc1d1	polární
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
tundrarum	tundrarum	k1gInSc4	tundrarum
<g/>
)	)	kIx)	)
–	–	k?	–
bílý	bílý	k1gMnSc1	bílý
vlk	vlk	k1gMnSc1	vlk
ze	z	k7c2	z
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Aljašky	Aljaška	k1gFnSc2	Aljaška
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
koloradský	koloradský	k2eAgMnSc1d1	koloradský
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnPc1	Canis
lupus	lupus	k1gInSc4	lupus
youngi	young	k1gFnSc2	young
<g/>
)	)	kIx)	)
–	–	k?	–
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pes	pes	k1gMnSc1	pes
domácí	domácí	k2eAgMnSc1d1	domácí
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
f.	f.	k?	f.
familiaris	familiaris	k1gInSc1	familiaris
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poddruhy	poddruh	k1gInPc1	poddruh
vlka	vlk	k1gMnSc2	vlk
s	s	k7c7	s
nejistým	jistý	k2eNgNnSc7d1	nejisté
postavením	postavení	k1gNnSc7	postavení
</s>
</p>
<p>
<s>
dingo	dingo	k1gMnSc1	dingo
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
dingo	dingo	k1gMnSc1	dingo
<g/>
)	)	kIx)	)
a	a	k8xC	a
dingo	dingo	k1gMnSc1	dingo
pralesní	pralesní	k2eAgMnSc1d1	pralesní
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
hallstromi	hallstro	k1gFnPc7	hallstro
<g/>
)	)	kIx)	)
–	–	k?	–
australský	australský	k2eAgMnSc1d1	australský
a	a	k8xC	a
novoguinejský	novoguinejský	k2eAgMnSc1d1	novoguinejský
divoký	divoký	k2eAgMnSc1d1	divoký
pes	pes	k1gMnSc1	pes
byli	být	k5eAaImAgMnP	být
dříve	dříve	k6eAd2	dříve
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
poddruhy	poddruh	k1gInPc4	poddruh
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
některé	některý	k3yIgInPc1	některý
výzkumy	výzkum	k1gInPc1	výzkum
je	on	k3xPp3gMnPc4	on
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
;	;	kIx,	;
taxonomie	taxonomie	k1gFnSc1	taxonomie
proto	proto	k8xC	proto
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
poněkud	poněkud	k6eAd1	poněkud
sporná	sporný	k2eAgFnSc1d1	sporná
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
mosbašský	mosbašský	k2eAgMnSc1d1	mosbašský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
mosbachensis	mosbachensis	k1gFnSc1	mosbachensis
<g/>
)	)	kIx)	)
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
známý	známý	k2eAgInSc1d1	známý
z	z	k7c2	z
fosilních	fosilní	k2eAgInPc2d1	fosilní
nálezů	nález	k1gInPc2	nález
na	na	k7c6	na
území	území	k1gNnSc6	území
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
egyptský	egyptský	k2eAgMnSc1d1	egyptský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
lupaster	lupaster	k1gInSc4	lupaster
<g/>
)	)	kIx)	)
–	–	k?	–
poddruh	poddruh	k1gInSc1	poddruh
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
šakala	šakal	k1gMnSc2	šakal
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
i	i	k9	i
o	o	k7c6	o
křížence	kříženka	k1gFnSc6	kříženka
</s>
</p>
<p>
<s>
==	==	k?	==
Populace	populace	k1gFnSc2	populace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Stavy	stav	k1gInPc1	stav
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
světě	svět	k1gInSc6	svět
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
000	[number]	k4	000
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
populace	populace	k1gFnSc1	populace
minimálně	minimálně	k6eAd1	minimálně
desetkrát	desetkrát	k6eAd1	desetkrát
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
<g/>
Vlčí	vlčí	k2eAgFnSc1d1	vlčí
populace	populace	k1gFnSc1	populace
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
nejsou	být	k5eNaImIp3nP	být
chráněné	chráněný	k2eAgInPc1d1	chráněný
místní	místní	k2eAgFnSc7d1	místní
legislativou	legislativa	k1gFnSc7	legislativa
a	a	k8xC	a
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
jich	on	k3xPp3gFnPc2	on
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Křížení	křížení	k1gNnPc1	křížení
se	s	k7c7	s
zdivočelými	zdivočelý	k2eAgMnPc7d1	zdivočelý
psy	pes	k1gMnPc7	pes
snižuje	snižovat	k5eAaImIp3nS	snižovat
genetickou	genetický	k2eAgFnSc4d1	genetická
kvalitu	kvalita	k1gFnSc4	kvalita
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vlci	vlk	k1gMnPc1	vlk
nejsou	být	k5eNaImIp3nP	být
chráněni	chránit	k5eAaImNgMnP	chránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInPc1	jejich
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
nezmenšují	zmenšovat	k5eNaImIp3nP	zmenšovat
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
okolo	okolo	k7c2	okolo
30	[number]	k4	30
000	[number]	k4	000
vlků	vlk	k1gMnPc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
mezi	mezi	k7c7	mezi
10	[number]	k4	10
000	[number]	k4	000
a	a	k8xC	a
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Středoasijské	středoasijský	k2eAgFnPc1d1	středoasijská
republiky	republika	k1gFnPc1	republika
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
a	a	k8xC	a
Uzbekistán	Uzbekistán	k1gInSc4	Uzbekistán
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
životní	životní	k2eAgInSc4d1	životní
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
vlků	vlk	k1gMnPc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
má	mít	k5eAaImIp3nS	mít
populaci	populace	k1gFnSc4	populace
více	hodně	k6eAd2	hodně
než	než	k8xS	než
12	[number]	k4	12
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
jsou	být	k5eAaImIp3nP	být
chráněni	chránit	k5eAaImNgMnP	chránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
asi	asi	k9	asi
1	[number]	k4	1
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
žije	žít	k5eAaImIp3nS	žít
stabilní	stabilní	k2eAgFnSc1d1	stabilní
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
a	a	k8xC	a
v	v	k7c6	v
severních	severní	k2eAgInPc6d1	severní
amerických	americký	k2eAgInPc6d1	americký
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Yellowstonský	Yellowstonský	k2eAgInSc1d1	Yellowstonský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
nejméně	málo	k6eAd3	málo
o	o	k7c4	o
60	[number]	k4	60
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
52	[number]	k4	52
000	[number]	k4	000
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
okolo	okolo	k7c2	okolo
9	[number]	k4	9
000	[number]	k4	000
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
populace	populace	k1gFnSc1	populace
vlků	vlk	k1gMnPc2	vlk
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavy	stav	k1gInPc1	stav
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
vlk	vlk	k1gMnSc1	vlk
prakticky	prakticky	k6eAd1	prakticky
nežije	žít	k5eNaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1486	[number]	k4	1486
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1743	[number]	k4	1743
a	a	k8xC	a
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
běží	běžet	k5eAaImIp3nS	běžet
programy	program	k1gInPc4	program
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
reintrodukci	reintrodukce	k1gFnSc4	reintrodukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
populace	populace	k1gFnPc1	populace
přežívají	přežívat	k5eAaImIp3nP	přežívat
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
270	[number]	k4	270
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
vlk	vlk	k1gMnSc1	vlk
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
však	však	k9	však
znovu	znovu	k6eAd1	znovu
objevují	objevovat	k5eAaImIp3nP	objevovat
vlci	vlk	k1gMnPc1	vlk
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
–	–	k?	–
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
saské	saský	k2eAgFnSc6d1	saská
Horní	horní	k2eAgFnSc6d1	horní
Lužici	Lužice	k1gFnSc6	Lužice
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
částečně	částečně	k6eAd1	částečně
nevyužívaném	využívaný	k2eNgInSc6d1	nevyužívaný
vojenském	vojenský	k2eAgInSc6d1	vojenský
prostoru	prostor	k1gInSc6	prostor
u	u	k7c2	u
Rietschenu	Rietschen	k1gInSc2	Rietschen
v	v	k7c6	v
devíti	devět	k4xCc6	devět
nebo	nebo	k8xC	nebo
deseti	deset	k4xCc6	deset
smečkách	smečka	k1gFnPc6	smečka
kolem	kolem	k7c2	kolem
80	[number]	k4	80
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
nejsou	být	k5eNaImIp3nP	být
chráněni	chráněn	k2eAgMnPc1d1	chráněn
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
místech	místo	k1gNnPc6	místo
sice	sice	k8xC	sice
chráněni	chráněn	k2eAgMnPc1d1	chráněn
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
pytláctví	pytláctví	k1gNnSc2	pytláctví
a	a	k8xC	a
ztráty	ztráta	k1gFnSc2	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
jich	on	k3xPp3gMnPc2	on
také	také	k9	také
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc4d1	velká
populaci	populace	k1gFnSc4	populace
má	mít	k5eAaImIp3nS	mít
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
–	–	k?	–
dohromady	dohromady	k6eAd1	dohromady
asi	asi	k9	asi
4	[number]	k4	4
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c4	v
Bělorusku	Běloruska	k1gFnSc4	Běloruska
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
vlka	vlk	k1gMnSc4	vlk
platí	platit	k5eAaImIp3nS	platit
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
částka	částka	k1gFnSc1	částka
<g/>
.	.	kIx.	.
</s>
<s>
Vlkům	Vlk	k1gMnPc3	Vlk
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
obzvláště	obzvláště	k6eAd1	obzvláště
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
neosídlené	osídlený	k2eNgFnSc6d1	neosídlená
zamořené	zamořený	k2eAgFnSc6d1	zamořená
zóně	zóna	k1gFnSc6	zóna
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Černobylu	Černobyl	k1gInSc2	Černobyl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
2	[number]	k4	2
500	[number]	k4	500
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
chráněni	chránit	k5eAaImNgMnP	chránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavy	stav	k1gInPc1	stav
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
===	===	k?	===
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
se	se	k3xPyFc4	se
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
zejména	zejména	k9	zejména
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
v	v	k7c6	v
lesnatých	lesnatý	k2eAgFnPc6d1	lesnatá
a	a	k8xC	a
hornatých	hornatý	k2eAgFnPc6d1	hornatá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
okolo	okolo	k7c2	okolo
pramenů	pramen	k1gInPc2	pramen
řek	řeka	k1gFnPc2	řeka
Ondava	Ondava	k1gFnSc1	Ondava
<g/>
,	,	kIx,	,
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
Laborec	Laborec	k1gMnSc1	Laborec
a	a	k8xC	a
v	v	k7c6	v
Nízkých	nízký	k2eAgFnPc6d1	nízká
a	a	k8xC	a
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
okolo	okolo	k7c2	okolo
174	[number]	k4	174
jedinců	jedinec	k1gMnPc2	jedinec
stálých	stálý	k2eAgFnPc2d1	stálá
a	a	k8xC	a
cca	cca	kA	cca
100	[number]	k4	100
jedinců	jedinec	k1gMnPc2	jedinec
přebíhavých	přebíhavý	k2eAgMnPc2d1	přebíhavý
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
Až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
snížily	snížit	k5eAaPmAgInP	snížit
až	až	k9	až
na	na	k7c4	na
40	[number]	k4	40
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vlk	vlk	k1gMnSc1	vlk
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
škodnou	škodná	k1gFnSc4	škodná
a	a	k8xC	a
za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
ulovení	ulovení	k1gNnSc2	ulovení
byly	být	k5eAaImAgFnP	být
dokonce	dokonce	k9	dokonce
vypláceny	vyplácet	k5eAaImNgFnP	vyplácet
státní	státní	k2eAgFnPc1d1	státní
odměny	odměna	k1gFnPc1	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
byl	být	k5eAaImAgMnS	být
vlk	vlk	k1gMnSc1	vlk
hájen	hájen	k2eAgMnSc1d1	hájen
vždy	vždy	k6eAd1	vždy
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
hájen	hájit	k5eAaImNgInS	hájit
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
celoročně	celoročně	k6eAd1	celoročně
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
<g/>
,	,	kIx,	,
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
ulovena	uloven	k2eAgFnSc1d1	ulovena
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vlků	vlk	k1gMnPc2	vlk
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
410	[number]	k4	410
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozptyl	rozptyl	k1gInSc1	rozptyl
čísla	číslo	k1gNnSc2	číslo
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
neexistuje	existovat	k5eNaImIp3nS	existovat
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
monitoring	monitoring	k1gInSc4	monitoring
výskytu	výskyt	k1gInSc2	výskyt
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Myslivecká	myslivecký	k2eAgFnSc1d1	myslivecká
lobby	lobby	k1gFnSc1	lobby
udává	udávat	k5eAaImIp3nS	udávat
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
–	–	k?	–
až	až	k6eAd1	až
2065	[number]	k4	2065
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavy	stav	k1gInPc1	stav
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
obecný	obecný	k2eAgMnSc1d1	obecný
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
chráněným	chráněný	k2eAgInSc7d1	chráněný
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
žili	žít	k5eAaImAgMnP	žít
na	na	k7c4	na
území	území	k1gNnSc4	území
Česka	Česko	k1gNnSc2	Česko
v	v	k7c6	v
hojném	hojný	k2eAgInSc6d1	hojný
počtu	počet	k1gInSc6	počet
do	do	k7c2	do
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jejich	jejich	k3xOp3gInSc1	jejich
stav	stav	k1gInSc1	stav
klesal	klesat	k5eAaImAgInS	klesat
kvůli	kvůli	k7c3	kvůli
lovu	lov	k1gInSc3	lov
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zcela	zcela	k6eAd1	zcela
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
vlk	vlk	k1gMnSc1	vlk
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
byl	být	k5eAaImAgInS	být
zastřelen	zastřelen	k2eAgInSc4d1	zastřelen
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Posledního	poslední	k2eAgMnSc2d1	poslední
vlka	vlk	k1gMnSc2	vlk
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
František	František	k1gMnSc1	František
Jež	Jež	k1gMnSc1	Jež
(	(	kIx(	(
<g/>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pamětní	pamětní	k2eAgInSc1d1	pamětní
kámen	kámen	k1gInSc1	kámen
–	–	k?	–
N	N	kA	N
<g/>
49	[number]	k4	49
<g/>
°	°	k?	°
<g/>
31	[number]	k4	31
<g/>
'	'	kIx"	'
<g/>
36.46	[number]	k4	36.46
<g/>
"	"	kIx"	"
E	E	kA	E
<g/>
18	[number]	k4	18
<g/>
°	°	k?	°
<g/>
49	[number]	k4	49
<g/>
'	'	kIx"	'
<g/>
0	[number]	k4	0
<g/>
6.18	[number]	k4	6.18
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
potvrzené	potvrzený	k2eAgInPc1d1	potvrzený
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
vlků	vlk	k1gMnPc2	vlk
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
jedinců	jedinec	k1gMnPc2	jedinec
obývá	obývat	k5eAaImIp3nS	obývat
Beskydy	Beskyd	k1gInPc1	Beskyd
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přišli	přijít	k5eAaPmAgMnP	přijít
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
nelegálním	legální	k2eNgInSc7d1	nelegální
odstřelem	odstřel	k1gInSc7	odstřel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stav	stav	k1gInSc4	stav
vlků	vlk	k1gMnPc2	vlk
v	v	k7c6	v
ČR	ČR	kA	ČR
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
zásadní	zásadní	k2eAgInSc1d1	zásadní
vliv	vliv	k1gInSc1	vliv
odstřel	odstřel	k1gInSc1	odstřel
vlka	vlk	k1gMnSc2	vlk
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
legální	legální	k2eAgMnSc1d1	legální
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
že	že	k8xS	že
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
však	však	k9	však
počet	počet	k1gInSc1	počet
snížil	snížit	k5eAaPmAgInS	snížit
na	na	k7c4	na
10	[number]	k4	10
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
přítomnost	přítomnost	k1gFnSc1	přítomnost
pouze	pouze	k6eAd1	pouze
tří	tři	k4xCgNnPc2	tři
vlků	vlk	k1gMnPc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
vlci	vlk	k1gMnPc1	vlk
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
v	v	k7c6	v
Rychlebských	Rychlebský	k2eAgFnPc6d1	Rychlebská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
<g/>
,	,	kIx,	,
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
a	a	k8xC	a
na	na	k7c6	na
Šluknovsku	Šluknovsko	k1gNnSc6	Šluknovsko
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnSc6	pozorování
osamělých	osamělý	k2eAgMnPc2d1	osamělý
jedinců	jedinec	k1gMnPc2	jedinec
bylo	být	k5eAaImAgNnS	být
hlášeno	hlásit	k5eAaImNgNnS	hlásit
i	i	k9	i
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Břehyně	Břehyně	k1gFnSc2	Břehyně
-	-	kIx~	-
Pecopala	Pecopal	k1gMnSc4	Pecopal
byl	být	k5eAaImAgMnS	být
fotopastí	fotopast	k1gFnSc7	fotopast
zachycen	zachycen	k2eAgMnSc1d1	zachycen
jedinec	jedinec	k1gMnSc1	jedinec
vlka	vlk	k1gMnSc2	vlk
obecného	obecný	k2eAgNnSc2d1	obecné
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
německo-polské	německoolský	k2eAgFnSc2d1	německo-polská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vydala	vydat	k5eAaPmAgFnS	vydat
AOPK	AOPK	kA	AOPK
ČR	ČR	kA	ČR
sérii	série	k1gFnSc4	série
fotografií	fotografia	k1gFnPc2	fotografia
mláděte	mládě	k1gNnSc2	mládě
vlka	vlk	k1gMnSc2	vlk
pořízenou	pořízený	k2eAgFnSc7d1	pořízená
na	na	k7c4	na
území	území	k1gNnSc4	území
CHKO	CHKO	kA	CHKO
Kokořínsko	Kokořínsko	k1gNnSc1	Kokořínsko
–	–	k?	–
Máchův	Máchův	k2eAgInSc1d1	Máchův
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokládá	dokládat	k5eAaImIp3nS	dokládat
opětovné	opětovný	k2eAgNnSc1d1	opětovné
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
vlka	vlk	k1gMnSc2	vlk
obecného	obecný	k2eAgMnSc2d1	obecný
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
smečka	smečka	k1gFnSc1	smečka
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
tři	tři	k4xCgNnPc4	tři
mláďata	mládě	k1gNnPc4	mládě
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
pět	pět	k4xCc1	pět
vlků	vlk	k1gMnPc2	vlk
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
dalšího	další	k2eAgInSc2d1	další
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stav	stav	k1gInSc1	stav
populace	populace	k1gFnSc2	populace
vlků	vlk	k1gMnPc2	vlk
u	u	k7c2	u
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
dle	dle	k7c2	dle
ochránců	ochránce	k1gMnPc2	ochránce
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
o	o	k7c4	o
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
mláďata	mládě	k1gNnPc4	mládě
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
sedm	sedm	k4xCc1	sedm
vlků	vlk	k1gMnPc2	vlk
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
genetického	genetický	k2eAgInSc2d1	genetický
výzkumu	výzkum	k1gInSc2	výzkum
stop	stop	k2eAgMnPc2d1	stop
vlků	vlk	k1gMnPc2	vlk
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
populace	populace	k1gFnSc1	populace
vlka	vlk	k1gMnSc2	vlk
přišla	přijít	k5eAaPmAgFnS	přijít
z	z	k7c2	z
východoněmecko-polské	východoněmeckoolský	k2eAgFnSc2d1	východoněmecko-polský
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
nalezen	nalézt	k5eAaBmNgInS	nalézt
trus	trus	k1gInSc1	trus
a	a	k8xC	a
stopy	stopa	k1gFnSc2	stopa
vlka	vlk	k1gMnSc2	vlk
obecného	obecný	k2eAgInSc2d1	obecný
také	také	k6eAd1	také
na	na	k7c6	na
Broumovsku	Broumovsko	k1gNnSc6	Broumovsko
<g/>
.	.	kIx.	.
</s>
<s>
Fotopasti	Fotopast	k1gFnPc1	Fotopast
umístěné	umístěný	k2eAgFnPc1d1	umístěná
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
republiky	republika	k1gFnSc2	republika
však	však	k9	však
žádné	žádný	k3yNgInPc4	žádný
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
snímky	snímek	k1gInPc4	snímek
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
nepořídily	pořídit	k5eNaPmAgFnP	pořídit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
ochránci	ochránce	k1gMnPc1	ochránce
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
CHKO	CHKO	kA	CHKO
Jeseníky	Jeseník	k1gInPc4	Jeseník
po	po	k7c6	po
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
navrátil	navrátit	k5eAaPmAgMnS	navrátit
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
kus	kus	k1gInSc4	kus
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
byl	být	k5eAaImAgInS	být
zachycen	zachycen	k2eAgInSc1d1	zachycen
fotopastí	fotopastit	k5eAaPmIp3nP	fotopastit
jednoho	jeden	k4xCgMnSc4	jeden
tamějšího	tamější	k2eAgMnSc4d1	tamější
občana	občan	k1gMnSc4	občan
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
existence	existence	k1gFnSc1	existence
vlčí	vlčí	k2eAgFnSc2d1	vlčí
smečky	smečka	k1gFnSc2	smečka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Abertam	Abertam	k1gInSc1	Abertam
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sociální	sociální	k2eAgNnPc1d1	sociální
chování	chování	k1gNnPc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
sociální	sociální	k2eAgNnPc1d1	sociální
zvířata	zvíře	k1gNnPc1	zvíře
žijící	žijící	k2eAgNnPc1d1	žijící
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
organizovaných	organizovaný	k2eAgFnPc6d1	organizovaná
smečkách	smečka	k1gFnPc6	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc4d1	klasická
vlčí	vlčí	k2eAgFnSc4d1	vlčí
smečku	smečka	k1gFnSc4	smečka
tvoří	tvořit	k5eAaImIp3nS	tvořit
vedoucí	vedoucí	k1gMnSc1	vedoucí
rodičovský	rodičovský	k2eAgMnSc1d1	rodičovský
"	"	kIx"	"
<g/>
alfa	alfa	k1gNnSc1	alfa
<g/>
"	"	kIx"	"
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
"	"	kIx"	"
<g/>
beta	beta	k1gNnSc1	beta
<g/>
"	"	kIx"	"
až	až	k9	až
"	"	kIx"	"
<g/>
omega	omega	k1gFnSc1	omega
<g/>
"	"	kIx"	"
status	status	k1gInSc1	status
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
i	i	k9	i
o	o	k7c4	o
nepříbuzné	příbuzný	k2eNgMnPc4d1	nepříbuzný
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
"	"	kIx"	"
<g/>
alfa	alfa	k1gNnSc1	alfa
<g/>
"	"	kIx"	"
vedou	vést	k5eAaImIp3nP	vést
smečku	smečka	k1gFnSc4	smečka
<g/>
,	,	kIx,	,
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
se	se	k3xPyFc4	se
a	a	k8xC	a
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
"	"	kIx"	"
<g/>
beta	beta	k1gNnSc1	beta
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
největší	veliký	k2eAgMnPc1d3	veliký
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
smečku	smečka	k1gFnSc4	smečka
a	a	k8xC	a
"	"	kIx"	"
<g/>
alfa	alfa	k1gNnSc1	alfa
<g/>
"	"	kIx"	"
vlky	vlk	k1gMnPc7	vlk
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Gama	gama	k1gNnSc1	gama
<g/>
"	"	kIx"	"
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
vlci	vlk	k1gMnPc1	vlk
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
především	především	k9	především
o	o	k7c4	o
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
výchovu	výchova	k1gFnSc4	výchova
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
"	"	kIx"	"
<g/>
předstírat	předstírat	k5eAaImF	předstírat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
smečka	smečka	k1gFnSc1	smečka
vypadala	vypadat	k5eAaImAgFnS	vypadat
početnější	početní	k2eAgFnSc1d2	početnější
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
"	"	kIx"	"
<g/>
omega	omega	k1gFnSc1	omega
<g/>
"	"	kIx"	"
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
odvádět	odvádět	k5eAaImF	odvádět
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
hasit	hasit	k5eAaImF	hasit
hrozící	hrozící	k2eAgInPc4d1	hrozící
konflikty	konflikt	k1gInPc4	konflikt
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc4	role
jakýchsi	jakýsi	k3yIgMnPc2	jakýsi
"	"	kIx"	"
<g/>
šašků	šašek	k1gMnPc2	šašek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Alfa	alfa	k1gFnSc1	alfa
vlci	vlk	k1gMnPc1	vlk
v	v	k7c6	v
severoamerických	severoamerický	k2eAgFnPc6d1	severoamerická
smečkách	smečka	k1gFnPc6	smečka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
poznají	poznat	k5eAaPmIp3nP	poznat
podle	podle	k7c2	podle
nejvýraznější	výrazný	k2eAgFnSc2d3	nejvýraznější
tmavé	tmavý	k2eAgFnSc2d1	tmavá
kresby	kresba	k1gFnSc2	kresba
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
neměnným	neměnný	k2eAgNnSc7d1	neměnné
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
jen	jen	k9	jen
vedoucí	vedoucí	k1gFnSc1	vedoucí
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
mívají	mívat	k5eAaImIp3nP	mívat
potomky	potomek	k1gMnPc4	potomek
i	i	k9	i
níže	nízce	k6eAd2	nízce
postavené	postavený	k2eAgFnPc1d1	postavená
vlčice	vlčice	k1gFnPc1	vlčice
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
smeček	smečka	k1gFnPc2	smečka
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
rekord	rekord	k1gInSc1	rekord
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
známá	známý	k2eAgFnSc1d1	známá
Lobova	Lobův	k2eAgFnSc1d1	Lobova
smečka	smečka	k1gFnSc1	smečka
měla	mít	k5eAaImAgFnS	mít
6	[number]	k4	6
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
však	však	k9	však
můžou	můžou	k?	můžou
žít	žít	k5eAaImF	žít
jen	jen	k9	jen
v	v	k7c6	v
párech	pár	k1gInPc6	pár
či	či	k8xC	či
osaměle	osaměle	k6eAd1	osaměle
a	a	k8xC	a
do	do	k7c2	do
smeček	smečka	k1gFnPc2	smečka
se	se	k3xPyFc4	se
sdružovat	sdružovat	k5eAaImF	sdružovat
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
severu	sever	k1gInSc2	sever
tvoří	tvořit	k5eAaImIp3nP	tvořit
kompaktnější	kompaktní	k2eAgFnPc1d2	kompaktnější
smečky	smečka	k1gFnPc1	smečka
<g/>
,	,	kIx,	,
jižněji	jižně	k6eAd2	jižně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnPc1d1	vyskytující
vlci	vlk	k1gMnPc1	vlk
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
tendenci	tendence	k1gFnSc4	tendence
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
volněji	volně	k6eAd2	volně
organizovaných	organizovaný	k2eAgFnPc6d1	organizovaná
smečkách	smečka	k1gFnPc6	smečka
<g/>
,	,	kIx,	,
v	v	k7c6	v
páru	pár	k1gInSc6	pár
či	či	k8xC	či
osaměle	osaměle	k6eAd1	osaměle
<g/>
.	.	kIx.	.
</s>
<s>
Vlčí	vlčí	k2eAgFnPc1d1	vlčí
smečky	smečka	k1gFnPc1	smečka
nejsou	být	k5eNaImIp3nP	být
tak	tak	k9	tak
dobře	dobře	k6eAd1	dobře
stmelené	stmelený	k2eAgNnSc4d1	stmelené
jako	jako	k8xC	jako
smečky	smečka	k1gFnSc2	smečka
psů	pes	k1gMnPc2	pes
hyenovitých	hyenovitý	k2eAgMnPc2d1	hyenovitý
nebo	nebo	k8xC	nebo
hyen	hyena	k1gFnPc2	hyena
skvrnitých	skvrnitý	k2eAgFnPc2d1	skvrnitá
<g/>
.	.	kIx.	.
</s>
<s>
Smečka	smečka	k1gFnSc1	smečka
je	být	k5eAaImIp3nS	být
dynamickým	dynamický	k2eAgInSc7d1	dynamický
útvarem	útvar	k1gInSc7	útvar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
postavení	postavení	k1gNnSc2	postavení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členů	člen	k1gInPc2	člen
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
může	moct	k5eAaImIp3nS	moct
obměňovat	obměňovat	k5eAaImF	obměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
vlci	vlk	k1gMnPc1	vlk
opouštějí	opouštět	k5eAaImIp3nP	opouštět
smečku	smečka	k1gFnSc4	smečka
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
roky	rok	k1gInPc7	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
5	[number]	k4	5
let	léto	k1gNnPc2	léto
staří	starý	k2eAgMnPc1d1	starý
vlci	vlk	k1gMnPc1	vlk
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Hledají	hledat	k5eAaImIp3nP	hledat
si	se	k3xPyFc3	se
partnery	partner	k1gMnPc4	partner
a	a	k8xC	a
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
se	se	k3xPyFc4	se
založit	založit	k5eAaPmF	založit
smečku	smečka	k1gFnSc4	smečka
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
tolerantnější	tolerantní	k2eAgMnPc1d2	tolerantnější
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
uvnitř	uvnitř	k7c2	uvnitř
smeček	smečka	k1gFnPc2	smečka
než	než	k8xS	než
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
strmější	strmý	k2eAgFnSc4d2	strmější
hierarchii	hierarchie	k1gFnSc4	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgMnPc1d1	žijící
vlci	vlk	k1gMnPc1	vlk
mají	mít	k5eAaImIp3nP	mít
volnější	volný	k2eAgFnSc4d2	volnější
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
hierarchizované	hierarchizovaný	k2eAgFnPc1d1	hierarchizovaná
smečky	smečka	k1gFnPc1	smečka
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
méně	málo	k6eAd2	málo
agrese	agrese	k1gFnPc1	agrese
než	než	k8xS	než
vlci	vlk	k1gMnPc1	vlk
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
monogamní	monogamní	k2eAgInPc1d1	monogamní
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
páry	pár	k1gInPc1	pár
spolu	spolu	k6eAd1	spolu
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
hledají	hledat	k5eAaImIp3nP	hledat
jiného	jiné	k1gNnSc2	jiné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
nebo	nebo	k8xC	nebo
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hustotou	hustota	k1gFnSc7	hustota
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
polygamie	polygamie	k1gFnSc1	polygamie
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
zabřezávají	zabřezávat	k5eAaImIp3nP	zabřezávat
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
věku	věk	k1gInSc2	věk
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
pak	pak	k6eAd1	pak
jeden	jeden	k4xCgInSc4	jeden
vrh	vrh	k1gInSc4	vrh
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kojotů	kojot	k1gMnPc2	kojot
jsou	být	k5eAaImIp3nP	být
produktivní	produktivní	k2eAgFnPc1d1	produktivní
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
páření	páření	k1gNnSc2	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
typicky	typicky	k6eAd1	typicky
koncem	koncem	k7c2	koncem
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
obvykle	obvykle	k6eAd1	obvykle
k	k	k7c3	k
dočasnému	dočasný	k2eAgNnSc3d1	dočasné
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
smečky	smečka	k1gFnSc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
březosti	březost	k1gFnSc2	březost
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
62	[number]	k4	62
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
většinou	většinou	k6eAd1	většinou
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
doupěte	doupě	k1gNnSc2	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Vrhají	vrhat	k5eAaImIp3nP	vrhat
nejčastěji	často	k6eAd3	často
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
vlčat	vlče	k1gNnPc2	vlče
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
hluchá	hluchý	k2eAgFnSc1d1	hluchá
a	a	k8xC	a
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
0,3	[number]	k4	0,3
<g/>
–	–	k?	–
<g/>
0,5	[number]	k4	0,5
kg	kg	kA	kg
a	a	k8xC	a
smysly	smysl	k1gInPc1	smysl
získávají	získávat	k5eAaImIp3nP	získávat
po	po	k7c6	po
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
týdnech	týden	k1gInPc6	týden
poprvé	poprvé	k6eAd1	poprvé
opouštějí	opouštět	k5eAaImIp3nP	opouštět
doupata	doupě	k1gNnPc4	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
o	o	k7c4	o
přísun	přísun	k1gInSc4	přísun
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
starají	starat	k5eAaImIp3nP	starat
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
1,5	[number]	k4	1,5
měsíci	měsíc	k1gInPc7	měsíc
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
vlčata	vlče	k1gNnPc4	vlče
dostatečně	dostatečně	k6eAd1	dostatečně
rychlá	rychlý	k2eAgNnPc4d1	rychlé
a	a	k8xC	a
obratná	obratný	k2eAgNnPc4d1	obratné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dokázala	dokázat	k5eAaPmAgFnS	dokázat
uniknout	uniknout	k5eAaPmF	uniknout
například	například	k6eAd1	například
před	před	k7c7	před
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
predátoři	predátor	k1gMnPc1	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
zabíjet	zabíjet	k5eAaImF	zabíjet
zvířata	zvíře	k1gNnPc4	zvíře
větší	veliký	k2eAgNnPc4d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
jeleny	jelen	k1gMnPc7	jelen
<g/>
,	,	kIx,	,
losy	los	k1gMnPc7	los
a	a	k8xC	a
severoamerickými	severoamerický	k2eAgMnPc7d1	severoamerický
soby	sob	k1gMnPc7	sob
karibu	karibu	k1gMnSc1	karibu
<g/>
.	.	kIx.	.
</s>
<s>
Hladová	hladový	k2eAgFnSc1d1	hladová
smečka	smečka	k1gFnSc1	smečka
se	se	k3xPyFc4	se
odváží	odvážet	k5eAaImIp3nS	odvážet
napadnout	napadnout	k5eAaPmF	napadnout
i	i	k9	i
osamoceného	osamocený	k2eAgMnSc4d1	osamocený
bizona	bizon	k1gMnSc4	bizon
nebo	nebo	k8xC	nebo
pižmoně	pižmoň	k1gMnSc4	pižmoň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostatek	dostatek	k1gInSc1	dostatek
velkých	velký	k2eAgMnPc2d1	velký
býložravců	býložravec	k1gMnPc2	býložravec
<g/>
,	,	kIx,	,
vlci	vlk	k1gMnPc1	vlk
žijí	žít	k5eAaImIp3nP	žít
mimo	mimo	k7c4	mimo
smečku	smečka	k1gFnSc4	smečka
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
specializaci	specializace	k1gFnSc4	specializace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
potrava	potrava	k1gFnSc1	potrava
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nP	lovit
srny	srna	k1gFnPc1	srna
<g/>
,	,	kIx,	,
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
králíky	králík	k1gMnPc4	králík
<g/>
,	,	kIx,	,
zajíce	zajíc	k1gMnPc4	zajíc
<g/>
,	,	kIx,	,
sviště	svišť	k1gMnPc4	svišť
<g/>
,	,	kIx,	,
bobry	bobr	k1gMnPc4	bobr
<g/>
,	,	kIx,	,
menší	malý	k2eAgMnPc4d2	menší
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
či	či	k8xC	či
rozličné	rozličný	k2eAgInPc4d1	rozličný
plazy	plaz	k1gInPc4	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
vodní	vodní	k2eAgNnSc4d1	vodní
ptactvo	ptactvo	k1gNnSc4	ptactvo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
i	i	k8xC	i
konkurenční	konkurenční	k2eAgMnPc4d1	konkurenční
menší	malý	k2eAgMnPc4d2	menší
predátory	predátor	k1gMnPc4	predátor
–	–	k?	–
lišky	liška	k1gFnPc4	liška
<g/>
,	,	kIx,	,
kuny	kuna	k1gFnPc4	kuna
<g/>
,	,	kIx,	,
lasičky	lasička	k1gFnPc4	lasička
<g/>
,	,	kIx,	,
jezevce	jezevec	k1gMnPc4	jezevec
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgMnPc4d1	divoký
psy	pes	k1gMnPc4	pes
<g/>
,	,	kIx,	,
šakaly	šakal	k1gMnPc4	šakal
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
nouze	nouze	k1gFnSc2	nouze
nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
ani	ani	k8xC	ani
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
mršinami	mršina	k1gFnPc7	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
doplní	doplnit	k5eAaPmIp3nS	doplnit
svou	svůj	k3xOyFgFnSc4	svůj
stravu	strava	k1gFnSc4	strava
i	i	k8xC	i
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hroznovým	hroznový	k2eAgNnSc7d1	hroznové
vínem	víno	k1gNnSc7	víno
nebo	nebo	k8xC	nebo
jablky	jablko	k1gNnPc7	jablko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jídle	jídlo	k1gNnSc6	jídlo
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
přizpůsobiví	přizpůsobivý	k2eAgMnPc1d1	přizpůsobivý
a	a	k8xC	a
v	v	k7c6	v
lovu	lov	k1gInSc6	lov
vynalézaví	vynalézavý	k2eAgMnPc1d1	vynalézavý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
i	i	k9	i
lidmi	člověk	k1gMnPc7	člověk
chovaný	chovaný	k2eAgInSc4d1	chovaný
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
obávaní	obávaný	k2eAgMnPc1d1	obávaný
a	a	k8xC	a
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
byli	být	k5eAaImAgMnP	být
nenávidění	nenáviděný	k2eAgMnPc1d1	nenáviděný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
může	moct	k5eAaImIp3nS	moct
najednou	najednou	k6eAd1	najednou
sníst	sníst	k5eAaPmF	sníst
zřejmě	zřejmě	k6eAd1	zřejmě
až	až	k9	až
10	[number]	k4	10
kg	kg	kA	kg
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
spotřeba	spotřeba	k1gFnSc1	spotřeba
jedince	jedinec	k1gMnSc2	jedinec
na	na	k7c4	na
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
kg	kg	kA	kg
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
vydrží	vydržet	k5eAaPmIp3nS	vydržet
dlouho	dlouho	k6eAd1	dlouho
hladovět	hladovět	k5eAaImF	hladovět
<g/>
.	.	kIx.	.
</s>
<s>
Nemusí	muset	k5eNaImIp3nP	muset
žrát	žrát	k5eAaImF	žrát
klidně	klidně	k6eAd1	klidně
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
to	ten	k3xDgNnSc1	ten
nějak	nějak	k6eAd1	nějak
oslabilo	oslabit	k5eAaPmAgNnS	oslabit
a	a	k8xC	a
omezilo	omezit	k5eAaPmAgNnS	omezit
jejich	jejich	k3xOp3gFnSc4	jejich
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konkurenti	konkurent	k1gMnPc1	konkurent
a	a	k8xC	a
nepřátelé	nepřítel	k1gMnPc1	nepřítel
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
dominují	dominovat	k5eAaImIp3nP	dominovat
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgFnPc7d1	ostatní
psovitými	psovitý	k2eAgFnPc7d1	psovitá
šelmami	šelma	k1gFnPc7	šelma
<g/>
,	,	kIx,	,
kdekoli	kdekoli	k6eAd1	kdekoli
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
likvidovat	likvidovat	k5eAaBmF	likvidovat
jako	jako	k8xC	jako
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
málokdy	málokdy	k6eAd1	málokdy
je	on	k3xPp3gFnPc4	on
následně	následně	k6eAd1	následně
požírají	požírat	k5eAaImIp3nP	požírat
jako	jako	k8xS	jako
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
lišky	liška	k1gFnPc4	liška
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc4	ten
jediné	jediný	k2eAgFnPc4d1	jediná
občas	občas	k6eAd1	občas
i	i	k8xC	i
žerou	žrát	k5eAaImIp3nP	žrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šakaly	šakal	k1gMnPc7	šakal
<g/>
,	,	kIx,	,
kojoty	kojot	k1gMnPc7	kojot
a	a	k8xC	a
psíky	psík	k1gMnPc7	psík
mývalovité	mývalovitý	k2eAgNnSc1d1	mývalovitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
střetávají	střetávat	k5eAaImIp3nP	střetávat
s	s	k7c7	s
dhouly	dhoul	k1gInPc7	dhoul
–	–	k?	–
ryšavými	ryšavý	k2eAgMnPc7d1	ryšavý
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kočkovité	kočkovitý	k2eAgFnPc1d1	kočkovitá
šelmy	šelma	k1gFnPc1	šelma
jsou	být	k5eAaImIp3nP	být
dalšími	další	k2eAgMnPc7d1	další
konkurenty	konkurent	k1gMnPc7	konkurent
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
vlci	vlk	k1gMnPc1	vlk
snaží	snažit	k5eAaImIp3nP	snažit
vypořádat	vypořádat	k5eAaPmF	vypořádat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
tu	ten	k3xDgFnSc4	ten
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
rysy	rys	k1gInPc1	rys
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
divokých	divoký	k2eAgFnPc2d1	divoká
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
rys	rys	k1gInSc1	rys
se	s	k7c7	s
samotným	samotný	k2eAgMnSc7d1	samotný
vlkem	vlk	k1gMnSc7	vlk
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
vyjít	vyjít	k5eAaPmF	vyjít
z	z	k7c2	z
případného	případný	k2eAgNnSc2d1	případné
střetnutí	střetnutí	k1gNnSc2	střetnutí
jako	jako	k8xC	jako
poražený	poražený	k2eAgMnSc1d1	poražený
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
jeden	jeden	k4xCgInSc4	jeden
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rys	rys	k1gInSc1	rys
vlka	vlk	k1gMnSc2	vlk
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Puma	puma	k1gFnSc1	puma
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vlky	vlk	k1gMnPc4	vlk
poměrně	poměrně	k6eAd1	poměrně
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
vlk	vlk	k1gMnSc1	vlk
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
obvykle	obvykle	k6eAd1	obvykle
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
vlků	vlk	k1gMnPc2	vlk
je	být	k5eAaImIp3nS	být
schopných	schopný	k2eAgMnPc2d1	schopný
ji	on	k3xPp3gFnSc4	on
přinejmenším	přinejmenším	k6eAd1	přinejmenším
odehnat	odehnat	k5eAaPmF	odehnat
od	od	k7c2	od
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
vlk	vlk	k1gMnSc1	vlk
při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
levhartem	levhart	k1gMnSc7	levhart
či	či	k8xC	či
hyenou	hyena	k1gFnSc7	hyena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Medvědi	medvěd	k1gMnPc1	medvěd
a	a	k8xC	a
vlci	vlk	k1gMnPc1	vlk
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
navzájem	navzájem	k6eAd1	navzájem
zabíjet	zabíjet	k5eAaImF	zabíjet
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mají	mít	k5eAaImIp3nP	mít
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
šelmy	šelma	k1gFnPc1	šelma
spolu	spolu	k6eAd1	spolu
často	často	k6eAd1	často
bojují	bojovat	k5eAaImIp3nP	bojovat
a	a	k8xC	a
střety	střet	k1gInPc1	střet
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
hnědí	hnědý	k2eAgMnPc1d1	hnědý
medvědi	medvěd	k1gMnPc1	medvěd
dokáží	dokázat	k5eAaPmIp3nP	dokázat
od	od	k7c2	od
kořisti	kořist	k1gFnSc2	kořist
vlky	vlk	k1gMnPc7	vlk
většinou	většinou	k6eAd1	většinou
odehnat	odehnat	k5eAaPmF	odehnat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
početnosti	početnost	k1gFnPc4	početnost
a	a	k8xC	a
bojovnosti	bojovnost	k1gFnPc4	bojovnost
smečky	smečka	k1gFnSc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
smečka	smečka	k1gFnSc1	smečka
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
schopná	schopný	k2eAgFnSc1d1	schopná
medvěda	medvěd	k1gMnSc4	medvěd
zahnat	zahnat	k5eAaPmF	zahnat
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
samozřejmě	samozřejmě	k6eAd1	samozřejmě
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
věk	věk	k1gInSc1	věk
a	a	k8xC	a
kondice	kondice	k1gFnSc1	kondice
medvěda	medvěd	k1gMnSc2	medvěd
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
skutečnými	skutečný	k2eAgMnPc7d1	skutečný
nepřáteli	nepřítel	k1gMnPc7	nepřítel
a	a	k8xC	a
příležitostnými	příležitostný	k2eAgMnPc7d1	příležitostný
predátory	predátor	k1gMnPc7	predátor
vlků	vlk	k1gMnPc2	vlk
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
jsou	být	k5eAaImIp3nP	být
zdokumentovány	zdokumentován	k2eAgInPc1d1	zdokumentován
případy	případ	k1gInPc1	případ
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
konkurence	konkurence	k1gFnSc2	konkurence
a	a	k8xC	a
občasného	občasný	k2eAgNnSc2d1	občasné
zabití	zabití	k1gNnSc2	zabití
vlků	vlk	k1gMnPc2	vlk
tygry	tygr	k1gMnPc7	tygr
ussurijskými	ussurijský	k2eAgMnPc7d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
obvykle	obvykle	k6eAd1	obvykle
zabité	zabitý	k2eAgFnSc2d1	zabitá
vlky	vlk	k1gMnPc7	vlk
nejedí	jíst	k5eNaImIp3nP	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzácně	vzácně	k6eAd1	vzácně
zabije	zabít	k5eAaPmIp3nS	zabít
vlka	vlk	k1gMnSc4	vlk
i	i	k9	i
tygr	tygr	k1gMnSc1	tygr
bengálský	bengálský	k2eAgMnSc1d1	bengálský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vlci	vlk	k1gMnPc1	vlk
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
===	===	k?	===
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
mají	mít	k5eAaImIp3nP	mít
prominentní	prominentní	k2eAgNnPc4d1	prominentní
postavení	postavení	k1gNnPc4	postavení
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
folkloru	folklor	k1gInSc6	folklor
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
a	a	k8xC	a
japonské	japonský	k2eAgFnSc6d1	japonská
mytologii	mytologie	k1gFnSc6	mytologie
byli	být	k5eAaImAgMnP	být
portrétováni	portrétován	k2eAgMnPc1d1	portrétován
jako	jako	k8xC	jako
bytosti	bytost	k1gFnPc1	bytost
s	s	k7c7	s
božskými	božský	k2eAgFnPc7d1	božská
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
rolníci	rolník	k1gMnPc1	rolník
uctívali	uctívat	k5eAaImAgMnP	uctívat
vlky	vlk	k1gMnPc7	vlk
ve	v	k7c6	v
svatyních	svatyně	k1gFnPc6	svatyně
<g/>
,	,	kIx,	,
nechávali	nechávat	k5eAaImAgMnP	nechávat
obětiny	obětina	k1gFnPc4	obětina
u	u	k7c2	u
vlčích	vlčí	k2eAgNnPc2d1	vlčí
doupat	doupě	k1gNnPc2	doupě
a	a	k8xC	a
žádali	žádat	k5eAaImAgMnP	žádat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ochránili	ochránit	k5eAaPmAgMnP	ochránit
jejich	jejich	k3xOp3gFnSc4	jejich
úrodu	úroda	k1gFnSc4	úroda
před	před	k7c7	před
jeleny	jelen	k1gMnPc7	jelen
a	a	k8xC	a
divokými	divoký	k2eAgNnPc7d1	divoké
prasaty	prase	k1gNnPc7	prase
<g/>
.	.	kIx.	.
</s>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
Fenrir	Fenrir	k1gMnSc1	Fenrir
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
synem	syn	k1gMnSc7	syn
boha	bůh	k1gMnSc2	bůh
Lokiho	Loki	k1gMnSc2	Loki
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
vlk	vlk	k1gMnSc1	vlk
Skoll	Skoll	k1gMnSc1	Skoll
zapadající	zapadající	k2eAgNnSc4d1	zapadající
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
bratr	bratr	k1gMnSc1	bratr
Hati	hať	k1gFnSc2	hať
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
další	další	k2eAgNnPc1d1	další
eurasijská	eurasijský	k2eAgNnPc1d1	eurasijské
etnika	etnikum	k1gNnPc1	etnikum
spojovala	spojovat	k5eAaImAgNnP	spojovat
vlky	vlk	k1gMnPc7	vlk
se	se	k3xPyFc4	se
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Římané	Říman	k1gMnPc1	Říman
připisovali	připisovat	k5eAaImAgMnP	připisovat
slunečnímu	sluneční	k2eAgMnSc3d1	sluneční
bohu	bůh	k1gMnSc3	bůh
Apollónovi	Apollón	k1gMnSc3	Apollón
vlky	vlk	k1gMnPc7	vlk
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
atributů	atribut	k1gInPc2	atribut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zoroastrické	zoroastrický	k2eAgFnSc6d1	zoroastrický
filozofii	filozofie	k1gFnSc6	filozofie
mají	mít	k5eAaImIp3nP	mít
vlci	vlk	k1gMnPc1	vlk
podobu	podoba	k1gFnSc4	podoba
zlých	zlý	k2eAgInPc2d1	zlý
a	a	k8xC	a
krutých	krutý	k2eAgFnPc2d1	krutá
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bibli	bible	k1gFnSc6	bible
se	se	k3xPyFc4	se
vlk	vlk	k1gMnSc1	vlk
objevuje	objevovat	k5eAaImIp3nS	objevovat
celkem	celkem	k6eAd1	celkem
třináctkrát	třináctkrát	k6eAd1	třináctkrát
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
chamtivosti	chamtivost	k1gFnSc2	chamtivost
a	a	k8xC	a
ničitelství	ničitelství	k1gNnSc2	ničitelství
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
je	být	k5eAaImIp3nS	být
přirovnání	přirovnání	k1gNnSc4	přirovnání
k	k	k7c3	k
vlku	vlk	k1gMnSc3	vlk
myšleno	myslet	k5eAaImNgNnS	myslet
pozitivně	pozitivně	k6eAd1	pozitivně
(	(	kIx(	(
<g/>
bojovný	bojovný	k2eAgInSc1d1	bojovný
kmen	kmen	k1gInSc1	kmen
Benjamín	Benjamín	k1gMnSc1	Benjamín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
a	a	k8xC	a
lidových	lidový	k2eAgFnPc6d1	lidová
zkazkách	zkazka	k1gFnPc6	zkazka
Slovanů	Slovan	k1gInPc2	Slovan
hrají	hrát	k5eAaImIp3nP	hrát
vlci	vlk	k1gMnPc1	vlk
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
převážně	převážně	k6eAd1	převážně
zápornou	záporný	k2eAgFnSc4d1	záporná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
úloha	úloha	k1gFnSc1	úloha
v	v	k7c6	v
Červené	Červené	k2eAgFnSc6d1	Červené
karkulce	karkulka	k1gFnSc6	karkulka
a	a	k8xC	a
zkazkách	zkazka	k1gFnPc6	zkazka
o	o	k7c6	o
neposlušných	poslušný	k2eNgNnPc6d1	neposlušné
kůzlátkách	kůzlátko	k1gNnPc6	kůzlátko
či	či	k8xC	či
třech	tři	k4xCgNnPc6	tři
malých	malý	k2eAgNnPc6d1	malé
prasátkách	prasátko	k1gNnPc6	prasátko
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
ruských	ruský	k2eAgFnPc6d1	ruská
pohádkách	pohádka	k1gFnPc6	pohádka
vlk	vlk	k1gMnSc1	vlk
hrdinovi	hrdina	k1gMnSc6	hrdina
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
zakletých	zakletý	k2eAgInPc2d1	zakletý
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
francouzská	francouzský	k2eAgFnSc1d1	francouzská
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Bisclavretovi	Bisclavret	k1gMnSc6	Bisclavret
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Ezopových	Ezopův	k2eAgFnPc6d1	Ezopova
či	či	k8xC	či
La	la	k1gNnPc3	la
Fontainových	Fontainových	k2eAgInSc2d1	Fontainových
bajkách	bajka	k1gFnPc6	bajka
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
vlk	vlk	k1gMnSc1	vlk
jako	jako	k8xC	jako
hloupé	hloupý	k2eAgNnSc1d1	hloupé
<g/>
,	,	kIx,	,
zlé	zlý	k2eAgNnSc1d1	zlé
a	a	k8xC	a
nenasytné	nasytný	k2eNgNnSc1d1	nenasytné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přelstěno	přelstít	k5eAaPmNgNnS	přelstít
liškou	liška	k1gFnSc7	liška
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
hrají	hrát	k5eAaImIp3nP	hrát
vlci	vlk	k1gMnPc1	vlk
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
odkojí	odkojit	k5eAaPmIp3nS	odkojit
vlčice	vlčice	k1gFnSc1	vlčice
(	(	kIx(	(
<g/>
proslavená	proslavený	k2eAgFnSc1d1	proslavená
sochou	socha	k1gFnSc7	socha
kapitolská	kapitolský	k2eAgFnSc1d1	Kapitolská
vlčice	vlčice	k1gFnSc1	vlčice
<g/>
)	)	kIx)	)
budoucí	budoucí	k2eAgFnSc1d1	budoucí
zakladatele	zakladatel	k1gMnSc2	zakladatel
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
Romula	Romulus	k1gMnSc2	Romulus
a	a	k8xC	a
Rema	Remus	k1gMnSc2	Remus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turecké	turecký	k2eAgFnSc6d1	turecká
<g/>
,	,	kIx,	,
mongolské	mongolský	k2eAgFnSc6d1	mongolská
a	a	k8xC	a
ainuské	ainuský	k2eAgFnSc6d1	ainuský
mytologii	mytologie	k1gFnSc6	mytologie
jsou	být	k5eAaImIp3nP	být
vlci	vlk	k1gMnPc1	vlk
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
předky	předek	k1gMnPc4	předek
lidské	lidský	k2eAgFnSc2d1	lidská
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Čečenci	Čečenec	k1gMnPc1	Čečenec
a	a	k8xC	a
Ingušové	Inguš	k1gMnPc1	Inguš
si	se	k3xPyFc3	se
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
vlčí	vlčí	k2eAgFnSc6d1	vlčí
matce	matka	k1gFnSc6	matka
lidských	lidský	k2eAgInPc2d1	lidský
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
příbězích	příběh	k1gInPc6	příběh
Kazachů	Kazach	k1gMnPc2	Kazach
<g/>
,	,	kIx,	,
Kyrgyzů	Kyrgyz	k1gMnPc2	Kyrgyz
a	a	k8xC	a
Altajců	Altajce	k1gMnPc2	Altajce
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
vlčice	vlčice	k1gFnSc1	vlčice
jako	jako	k8xS	jako
ochránkyně	ochránkyně	k1gFnSc1	ochránkyně
válečníka	válečník	k1gMnSc2	válečník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
příběhy	příběh	k1gInPc4	příběh
o	o	k7c4	o
vlčici	vlčice	k1gFnSc4	vlčice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ženou	žena	k1gFnSc7	žena
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
přinesla	přinést	k5eAaPmAgFnS	přinést
mu	on	k3xPp3gMnSc3	on
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
magické	magický	k2eAgFnSc2d1	magická
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
i	i	k9	i
mezi	mezi	k7c7	mezi
Inuity	Inuita	k1gMnPc7	Inuita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
dáváni	dáván	k2eAgMnPc1d1	dáván
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
čarodějnictvím	čarodějnictví	k1gNnSc7	čarodějnictví
jak	jak	k8xC	jak
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
tak	tak	k8xC	tak
indiánských	indiánský	k2eAgFnPc6d1	indiánská
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tanainaové	Tanainaová	k1gFnPc1	Tanainaová
(	(	kIx(	(
<g/>
Dena	Den	k2eAgFnSc1d1	Dena
<g/>
'	'	kIx"	'
<g/>
inaové	inaové	k2eAgFnSc1d1	inaové
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
z	z	k7c2	z
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
,	,	kIx,	,
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlci	vlk	k1gMnPc1	vlk
byli	být	k5eAaImAgMnP	být
dříve	dříve	k6eAd2	dříve
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
je	on	k3xPp3gNnSc4	on
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
bratry	bratr	k1gMnPc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
názor	názor	k1gInSc4	názor
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
Kutenajové	Kutenajový	k2eAgInPc1d1	Kutenajový
ze	z	k7c2	z
Skalistých	skalistý	k2eAgInPc2d1	skalistý
hor.	hor.	k?	hor.
Podle	podle	k7c2	podle
mýtu	mýtus	k1gInSc2	mýtus
stvoření	stvoření	k1gNnSc2	stvoření
kmene	kmen	k1gInSc2	kmen
Póný	Póný	k1gFnPc2	Póný
se	se	k3xPyFc4	se
vlk	vlk	k1gMnSc1	vlk
stal	stát	k5eAaPmAgMnS	stát
první	první	k4xOgFnSc7	první
bytostí	bytost	k1gFnSc7	bytost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zakusila	zakusit	k5eAaPmAgFnS	zakusit
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
klanů	klan	k1gInPc2	klan
Pónyů	Póny	k1gMnPc2	Póny
byl	být	k5eAaImAgMnS	být
vlk	vlk	k1gMnSc1	vlk
zároveň	zároveň	k6eAd1	zároveň
uctíván	uctíván	k2eAgMnSc1d1	uctíván
jako	jako	k8xC	jako
totemové	totemový	k2eAgNnSc1d1	totemové
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nutkové	Nutek	k1gMnPc1	Nutek
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
a	a	k8xC	a
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
nosili	nosit	k5eAaImAgMnP	nosit
při	při	k7c6	při
obřadech	obřad	k1gInPc6	obřad
vlčí	vlčí	k2eAgFnSc2d1	vlčí
masky	maska	k1gFnSc2	maska
a	a	k8xC	a
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc1	jejich
medicinmani	medicinman	k1gMnPc1	medicinman
dokáží	dokázat	k5eAaPmIp3nP	dokázat
proměnit	proměnit	k5eAaPmF	proměnit
ve	v	k7c4	v
vlky	vlk	k1gMnPc4	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Irokézové	Irokéz	k1gMnPc1	Irokéz
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlk	vlk	k1gMnSc1	vlk
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
zvířat	zvíře	k1gNnPc2	zvíře
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc4d1	podobný
indiánskému	indiánský	k2eAgInSc3d1	indiánský
klanovému	klanový	k2eAgInSc3d1	klanový
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Lakoty	lakota	k1gMnPc4	lakota
<g/>
,	,	kIx,	,
Šajeny	Šajen	k1gMnPc4	Šajen
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
prérijní	prérijní	k2eAgMnPc4d1	prérijní
indiány	indián	k1gMnPc4	indián
byl	být	k5eAaImAgMnS	být
vlk	vlk	k1gMnSc1	vlk
symbolem	symbol	k1gInSc7	symbol
zvěda	zvěd	k1gMnSc2	zvěd
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
zvědové	zvěd	k1gMnPc1	zvěd
přímo	přímo	k6eAd1	přímo
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k9	jako
vlci	vlk	k1gMnPc1	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Indiánský	indiánský	k2eAgInSc1d1	indiánský
piktogram	piktogram	k1gInSc1	piktogram
pro	pro	k7c4	pro
zvěda	zvěd	k1gMnSc4	zvěd
je	být	k5eAaImIp3nS	být
znázorněn	znázorněn	k2eAgMnSc1d1	znázorněn
jako	jako	k8xS	jako
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
vlčí	vlčí	k2eAgFnSc7d1	vlčí
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
hrají	hrát	k5eAaImIp3nP	hrát
vlci	vlk	k1gMnPc1	vlk
také	také	k9	také
v	v	k7c6	v
sibiřském	sibiřský	k2eAgInSc6d1	sibiřský
šamanismu	šamanismus	k1gInSc6	šamanismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
Jukagirů	Jukagir	k1gMnPc2	Jukagir
či	či	k8xC	či
Altajců	Altajce	k1gMnPc2	Altajce
byl	být	k5eAaImAgMnS	být
vlk	vlk	k1gMnSc1	vlk
často	často	k6eAd1	často
ochranným	ochranný	k2eAgMnSc7d1	ochranný
duchem	duch	k1gMnSc7	duch
(	(	kIx(	(
<g/>
zvířecí	zvířecí	k2eAgFnSc7d1	zvířecí
matkou	matka	k1gFnSc7	matka
<g/>
)	)	kIx)	)
šamana	šaman	k1gMnSc2	šaman
<g/>
.	.	kIx.	.
</s>
<s>
Domorodí	domorodý	k2eAgMnPc1d1	domorodý
Sibiřané	Sibiřan	k1gMnPc1	Sibiřan
vlka	vlk	k1gMnSc2	vlk
spojovali	spojovat	k5eAaImAgMnP	spojovat
s	s	k7c7	s
nebeským	nebeský	k2eAgInSc7d1	nebeský
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
válečnictím	válečnictí	k2eAgInSc7d1	válečnictí
<g/>
,	,	kIx,	,
lovem	lov	k1gInSc7	lov
<g/>
,	,	kIx,	,
větrem	vítr	k1gInSc7	vítr
nebo	nebo	k8xC	nebo
bouří	bouř	k1gFnSc7	bouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlkodlaci	vlkodlak	k1gMnPc5	vlkodlak
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
vlkodlacích	vlkodlak	k1gMnPc6	vlkodlak
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
u	u	k7c2	u
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
,	,	kIx,	,
Germánů	Germán	k1gMnPc2	Germán
<g/>
,	,	kIx,	,
Baltů	Balt	k1gMnPc2	Balt
<g/>
,	,	kIx,	,
Keltů	Kelt	k1gMnPc2	Kelt
i	i	k8xC	i
Ugrofinů	Ugrofin	k1gMnPc2	Ugrofin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
vlkodlacích	vlkodlak	k1gMnPc6	vlkodlak
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
psal	psát	k5eAaImAgInS	psát
o	o	k7c6	o
skythském	skythský	k2eAgInSc6d1	skythský
kmeni	kmen	k1gInSc6	kmen
Neurů	Neur	k1gMnPc2	Neur
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
příslušníci	příslušník	k1gMnPc1	příslušník
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
měnili	měnit	k5eAaImAgMnP	měnit
ve	v	k7c4	v
vlky	vlk	k1gMnPc4	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ovidiových	Ovidiův	k2eAgFnPc2d1	Ovidiova
Proměn	proměna	k1gFnPc2	proměna
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vlka	vlk	k1gMnSc2	vlk
proměněn	proměněn	k2eAgMnSc1d1	proměněn
arkádský	arkádský	k2eAgMnSc1d1	arkádský
král	král	k1gMnSc1	král
Lykaón	Lykaón	k1gMnSc1	Lykaón
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgMnS	dopustit
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vlkodlaku	vlkodlak	k1gMnSc6	vlkodlak
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
i	i	k9	i
římský	římský	k2eAgMnSc1d1	římský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Petronius	Petronius	k1gMnSc1	Petronius
v	v	k7c6	v
románu	román	k1gInSc6	román
Satyricon	Satyricon	k1gInSc1	Satyricon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Balty	Balt	k1gMnPc4	Balt
(	(	kIx(	(
<g/>
staré	starý	k2eAgMnPc4d1	starý
Litevce	Litevec	k1gMnPc4	Litevec
<g/>
,	,	kIx,	,
Lotyše	Lotyš	k1gMnPc4	Lotyš
a	a	k8xC	a
Prusy	Prus	k1gMnPc4	Prus
<g/>
)	)	kIx)	)
nebyl	být	k5eNaImAgMnS	být
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
negativní	negativní	k2eAgNnSc4d1	negativní
bytostí	bytost	k1gFnSc7	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
pomáhat	pomáhat	k5eAaImF	pomáhat
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
nakrmil	nakrmit	k5eAaPmAgMnS	nakrmit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Baltů	Balt	k1gMnPc2	Balt
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
mohli	moct	k5eAaImAgMnP	moct
proměnit	proměnit	k5eAaPmF	proměnit
ve	v	k7c4	v
vlkodlaka	vlkodlak	k1gMnSc4	vlkodlak
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
úplňku	úplněk	k1gInSc2	úplněk
prolezli	prolézt	k5eAaPmAgMnP	prolézt
mezi	mezi	k7c7	mezi
kořeny	kořen	k1gInPc7	kořen
určitých	určitý	k2eAgInPc2d1	určitý
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Germánů	Germán	k1gMnPc2	Germán
byli	být	k5eAaImAgMnP	být
známi	znám	k2eAgMnPc1d1	znám
ulfhetmar	ulfhetmara	k1gFnPc2	ulfhetmara
<g/>
,	,	kIx,	,
bojovníci	bojovník	k1gMnPc1	bojovník
ve	v	k7c6	v
vlčí	vlčí	k2eAgFnSc6d1	vlčí
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
bojovém	bojový	k2eAgNnSc6d1	bojové
šílenství	šílenství	k1gNnSc6	šílenství
projevovali	projevovat	k5eAaImAgMnP	projevovat
vlčí	vlčí	k2eAgFnSc4d1	vlčí
zuřivost	zuřivost	k1gFnSc4	zuřivost
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
obdobou	obdoba	k1gFnSc7	obdoba
známějších	známý	k2eAgMnPc2d2	známější
berserkrů	berserkr	k1gMnPc2	berserkr
<g/>
,	,	kIx,	,
asociovaných	asociovaný	k2eAgFnPc2d1	asociovaná
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
představy	představa	k1gFnPc4	představa
měli	mít	k5eAaImAgMnP	mít
vlčí	vlčí	k2eAgMnPc1d1	vlčí
bojovníci	bojovník	k1gMnPc1	bojovník
u	u	k7c2	u
starých	starý	k2eAgMnPc2d1	starý
Maďarů	Maďar	k1gMnPc2	Maďar
v	v	k7c6	v
předkřesťanském	předkřesťanský	k2eAgNnSc6d1	předkřesťanské
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Matyáše	Matyáš	k1gMnSc4	Matyáš
Korvína	Korvín	k1gMnSc4	Korvín
používali	používat	k5eAaImAgMnP	používat
maďarští	maďarský	k2eAgMnPc1d1	maďarský
bojovníci	bojovník	k1gMnPc1	bojovník
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
bojový	bojový	k2eAgInSc1d1	bojový
pokřik	pokřik	k1gInSc1	pokřik
farkas	farkas	k1gMnSc1	farkas
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Slovanů	Slovan	k1gMnPc2	Slovan
a	a	k8xC	a
Rumunů	Rumun	k1gMnPc2	Rumun
byl	být	k5eAaImAgMnS	být
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
nemrtvou	mrtvý	k2eNgFnSc4d1	nemrtvá
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
škodící	škodící	k2eAgFnSc4d1	škodící
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vlkodlacích	vlkodlak	k1gMnPc6	vlkodlak
ve	v	k7c6	v
slovanském	slovanský	k2eAgNnSc6d1	slovanské
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Slově	slovo	k1gNnSc6	slovo
o	o	k7c6	o
pluku	pluk	k1gInSc6	pluk
Igorově	Igorův	k2eAgInSc6d1	Igorův
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
a	a	k8xC	a
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
knížete	kníže	k1gMnSc2	kníže
Vseslava	Vseslav	k1gMnSc2	Vseslav
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
měnil	měnit	k5eAaImAgMnS	měnit
ve	v	k7c4	v
vlka	vlk	k1gMnSc4	vlk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
sekta	sekta	k1gFnSc1	sekta
benandantů	benandant	k1gInPc2	benandant
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
příslušníci	příslušník	k1gMnPc1	příslušník
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
měnili	měnit	k5eAaImAgMnP	měnit
ve	v	k7c4	v
vlky	vlk	k1gMnPc4	vlk
a	a	k8xC	a
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
čarodějnicím	čarodějnice	k1gFnPc3	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
představy	představa	k1gFnPc4	představa
měli	mít	k5eAaImAgMnP	mít
i	i	k8xC	i
Slovinci	Slovinec	k1gMnPc1	Slovinec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
zvaní	zvaný	k2eAgMnPc1d1	zvaný
krsnik	krsnik	k1gInSc1	krsnik
měnili	měnit	k5eAaImAgMnP	měnit
v	v	k7c4	v
bílé	bílý	k2eAgMnPc4d1	bílý
vlky	vlk	k1gMnPc4	vlk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
upírům	upír	k1gMnPc3	upír
a	a	k8xC	a
čarodějům	čaroděj	k1gMnPc3	čaroděj
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
rumunská	rumunský	k2eAgFnSc1d1	rumunská
pověst	pověst	k1gFnSc1	pověst
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
vlkodlaků	vlkodlak	k1gMnPc2	vlkodlak
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
Vlad	Vlad	k1gInSc1	Vlad
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Tepes	Tepes	k1gInSc4	Tepes
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
válečném	válečný	k2eAgNnSc6d1	válečné
tažení	tažení	k1gNnSc6	tažení
zničil	zničit	k5eAaPmAgMnS	zničit
i	i	k9	i
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
lidi	člověk	k1gMnPc4	člověk
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
ukryté	ukrytý	k2eAgNnSc1d1	ukryté
nechal	nechat	k5eAaPmAgMnS	nechat
povraždit	povraždit	k5eAaPmF	povraždit
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
proklel	proklít	k5eAaPmAgMnS	proklít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
Dracula	Draculum	k1gNnSc2	Draculum
změněný	změněný	k2eAgInSc1d1	změněný
na	na	k7c4	na
upíra	upír	k1gMnSc4	upír
poprvé	poprvé	k6eAd1	poprvé
napil	napít	k5eAaBmAgMnS	napít
lidské	lidský	k2eAgFnSc2d1	lidská
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
vstali	vstát	k5eAaPmAgMnP	vstát
prý	prý	k9	prý
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
vlkodlaky	vlkodlak	k1gMnPc7	vlkodlak
<g/>
,	,	kIx,	,
neúnavně	únavně	k6eNd1	únavně
bojujícími	bojující	k2eAgInPc7d1	bojující
proti	proti	k7c3	proti
upírům	upír	k1gMnPc3	upír
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vlkodlaka	vlkodlak	k1gMnSc4	vlkodlak
byl	být	k5eAaImAgMnS	být
někdy	někdy	k6eAd1	někdy
pokládán	pokládat	k5eAaImNgMnS	pokládat
i	i	k8xC	i
Draculův	Draculův	k2eAgMnSc1d1	Draculův
úhlavní	úhlavní	k2eAgMnSc1d1	úhlavní
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
János	János	k1gMnSc1	János
Hunyady	Hunyada	k1gFnSc2	Hunyada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
čarodějnických	čarodějnický	k2eAgInPc2d1	čarodějnický
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byli	být	k5eAaImAgMnP	být
především	především	k6eAd1	především
na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
souzeni	soudit	k5eAaImNgMnP	soudit
a	a	k8xC	a
popraveni	popravit	k5eAaPmNgMnP	popravit
jako	jako	k8xS	jako
údajní	údajný	k2eAgMnPc1d1	údajný
vlkodlaci	vlkodlak	k1gMnPc1	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
Peter	Peter	k1gMnSc1	Peter
Stumpp	Stumpp	k1gMnSc1	Stumpp
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
venkově	venkov	k1gInSc6	venkov
bývali	bývat	k5eAaImAgMnP	bývat
z	z	k7c2	z
lykantropie	lykantropie	k1gFnSc2	lykantropie
a	a	k8xC	a
spolčování	spolčování	k1gNnSc2	spolčování
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
podezříváni	podezříván	k2eAgMnPc1d1	podezříván
obecní	obecní	k2eAgMnPc1d1	obecní
pastýři	pastýř	k1gMnPc1	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Pastýři	pastýř	k1gMnPc1	pastýř
používali	používat	k5eAaImAgMnP	používat
magické	magický	k2eAgInPc4d1	magický
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
zvířat	zvíře	k1gNnPc2	zvíře
před	před	k7c7	před
vlky	vlk	k1gMnPc7	vlk
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
oslovován	oslovovat	k5eAaImNgMnS	oslovovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kmotr	kmotr	k1gMnSc1	kmotr
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obětovalo	obětovat	k5eAaBmAgNnS	obětovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nejslabší	slabý	k2eAgNnSc1d3	nejslabší
jehně	jehně	k1gNnSc1	jehně
ze	z	k7c2	z
stáda	stádo	k1gNnSc2	stádo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
umí	umět	k5eAaImIp3nS	umět
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
komunikovat	komunikovat	k5eAaImF	komunikovat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
v	v	k7c4	v
ně	on	k3xPp3gMnPc4	on
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
vlkodlaci	vlkodlak	k1gMnPc1	vlkodlak
vedle	vedle	k7c2	vedle
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
upírů	upír	k1gMnPc2	upír
stali	stát	k5eAaPmAgMnP	stát
nejčastějšími	častý	k2eAgMnPc7d3	nejčastější
hrdiny	hrdina	k1gMnPc7	hrdina
knižních	knižní	k2eAgInPc2d1	knižní
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
filmových	filmový	k2eAgInPc2d1	filmový
hororů	horor	k1gInPc2	horor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlci	vlk	k1gMnPc1	vlk
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
umění	umění	k1gNnSc6	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
obrovském	obrovský	k2eAgNnSc6d1	obrovské
množství	množství	k1gNnSc6	množství
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Rudyarda	Rudyard	k1gMnSc2	Rudyard
Kiplinga	Kipling	k1gMnSc2	Kipling
Knihy	kniha	k1gFnSc2	kniha
džunglí	džungle	k1gFnPc2	džungle
hraje	hrát	k5eAaImIp3nS	hrát
vlčí	vlčí	k2eAgFnSc1d1	vlčí
smečka	smečka	k1gFnSc1	smečka
vedená	vedený	k2eAgFnSc1d1	vedená
samcem	samec	k1gMnSc7	samec
Akélou	Akéla	k1gMnSc7	Akéla
zásadní	zásadní	k2eAgFnSc4d1	zásadní
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
výchově	výchova	k1gFnSc6	výchova
lidského	lidský	k2eAgMnSc2d1	lidský
chlapce	chlapec	k1gMnSc2	chlapec
Mauglího	Mauglí	k2eAgMnSc2d1	Mauglí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
zachrání	zachránit	k5eAaPmIp3nS	zachránit
před	před	k7c7	před
tygrem	tygr	k1gMnSc7	tygr
Šer	šer	k1gInSc4wB	šer
Chánem	chán	k1gMnSc7	chán
<g/>
.	.	kIx.	.
</s>
<s>
Kladnou	kladný	k2eAgFnSc4d1	kladná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
vlk	vlk	k1gMnSc1	vlk
i	i	k9	i
ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
sérii	série	k1gFnSc4	série
Letopisy	letopis	k1gInPc4	letopis
z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
věků	věk	k1gInPc2	věk
britské	britský	k2eAgFnSc2d1	britská
autorky	autorka	k1gFnSc2	autorka
Michelle	Michelle	k1gFnSc2	Michelle
Paverové	Paverová	k1gFnSc2	Paverová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
figuruje	figurovat	k5eAaImIp3nS	figurovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
zvířecího	zvířecí	k2eAgMnSc2d1	zvířecí
dvojníka	dvojník	k1gMnSc2	dvojník
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
Toraka	Torak	k1gMnSc2	Torak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vlk	Vlk	k1gMnSc1	Vlk
ve	v	k7c6	v
frazeologii	frazeologie	k1gFnSc6	frazeologie
====	====	k?	====
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
zmíněni	zmínit	k5eAaPmNgMnP	zmínit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
známých	známý	k2eAgInPc6d1	známý
přirovnáních	přirovnání	k1gNnPc6	přirovnání
<g/>
,	,	kIx,	,
citátech	citát	k1gInPc6	citát
a	a	k8xC	a
příslovích	přísloví	k1gNnPc6	přísloví
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
antického	antický	k2eAgInSc2d1	antický
či	či	k8xC	či
biblického	biblický	k2eAgInSc2d1	biblický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c4	v
negativní	negativní	k2eAgFnSc4d1	negativní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
hlad	hlad	k1gMnSc1	hlad
jako	jako	k8xS	jako
vlk	vlk	k1gMnSc1	vlk
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
o	o	k7c6	o
vlku	vlk	k1gMnSc6	vlk
a	a	k8xC	a
vlk	vlk	k1gMnSc1	vlk
za	za	k7c7	za
humny	humna	k1gNnPc7	humna
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Kdo	kdo	k3yQnSc1	kdo
chce	chtít	k5eAaImIp3nS	chtít
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
žíti	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
výti	výt	k5eAaImF	výt
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlk	vlk	k1gMnSc1	vlk
v	v	k7c6	v
rouše	roucho	k1gNnSc6	roucho
<g />
.	.	kIx.	.
</s>
<s>
beránčím	beránčí	k2eAgMnPc3d1	beránčí
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
člověku	člověk	k1gMnSc3	člověk
vlkem	vlk	k1gMnSc7	vlk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Co	co	k3yQnSc4	co
vlk	vlk	k1gMnSc1	vlk
zchvátí	zchvátit	k5eAaPmIp3nS	zchvátit
<g/>
,	,	kIx,	,
nerad	nerad	k2eAgMnSc1d1	nerad
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
S	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
po	po	k7c6	po
lidsku	lidsek	k1gInSc6	lidsek
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
po	po	k7c6	po
vlčím	vlčí	k2eAgInSc6d1	vlčí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
vlk	vlk	k1gMnSc1	vlk
ovce	ovce	k1gFnSc2	ovce
vláčí	vláčet	k5eAaImIp3nS	vláčet
<g/>
,	,	kIx,	,
až	až	k8xS	až
ho	on	k3xPp3gMnSc4	on
odvlečou	odvléct	k5eAaPmIp3nP	odvléct
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vlka	Vlk	k1gMnSc4	Vlk
živí	živit	k5eAaImIp3nP	živit
nohy	noha	k1gFnPc1	noha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vlkem	vlk	k1gMnSc7	vlk
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
spojeny	spojit	k5eAaPmNgInP	spojit
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
idiomy	idiom	k1gInPc1	idiom
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
chytit	chytit	k5eAaPmF	chytit
vlka	vlk	k1gMnSc4	vlk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mořský	mořský	k2eAgMnSc1d1	mořský
vlk	vlk	k1gMnSc1	vlk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vlčí	vlčet	k5eAaImIp3nS	vlčet
mlha	mlha	k1gFnSc1	mlha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vlčí	vlčet	k5eAaImIp3nS	vlčet
mák	mák	k1gMnSc1	mák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vlčí	vlčet	k5eAaImIp3nS	vlčet
bob	bob	k1gInSc1	bob
<g/>
"	"	kIx"	"
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vlk	Vlk	k1gMnSc1	Vlk
v	v	k7c6	v
lexikologii	lexikologie	k1gFnSc6	lexikologie
====	====	k?	====
</s>
</p>
<p>
<s>
Od	od	k7c2	od
názvu	název	k1gInSc2	název
vlka	vlk	k1gMnSc2	vlk
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
mnoho	mnoho	k6eAd1	mnoho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
germánského	germánský	k2eAgInSc2d1	germánský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
<g/>
,	,	kIx,	,
Wolfhart	Wolfhart	k1gInSc1	Wolfhart
<g/>
,	,	kIx,	,
Ranulf	Ranulf	k1gMnSc1	Ranulf
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
Isengrim	Isengrim	k1gMnSc1	Isengrim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jihoslovanská	jihoslovanský	k2eAgNnPc4d1	jihoslovanské
jména	jméno	k1gNnPc4	jméno
Vuk	Vuk	k1gFnPc2	Vuk
<g/>
,	,	kIx,	,
Vukan	Vukana	k1gFnPc2	Vukana
či	či	k8xC	či
Vukašin	Vukašina	k1gFnPc2	Vukašina
nebo	nebo	k8xC	nebo
staročeské	staročeský	k2eAgNnSc4d1	staročeské
Vlkoš	Vlkoš	k1gMnSc1	Vlkoš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útoky	útok	k1gInPc1	útok
na	na	k7c4	na
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
psy	pes	k1gMnPc4	pes
===	===	k?	===
</s>
</p>
<p>
<s>
Útoky	útok	k1gInPc4	útok
na	na	k7c4	na
dobytek	dobytek	k1gInSc4	dobytek
představují	představovat	k5eAaImIp3nP	představovat
zřejmě	zřejmě	k6eAd1	zřejmě
hlavní	hlavní	k2eAgInSc4d1	hlavní
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
vlk	vlk	k1gMnSc1	vlk
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
lidmi	člověk	k1gMnPc7	člověk
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
nenašel	najít	k5eNaPmAgInS	najít
jiný	jiný	k2eAgInSc1d1	jiný
dostatečně	dostatečně	k6eAd1	dostatečně
účinný	účinný	k2eAgInSc1d1	účinný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
tomu	ten	k3xDgNnSc3	ten
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
než	než	k8xS	než
zabíjení	zabíjení	k1gNnSc1	zabíjení
vlků	vlk	k1gMnPc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
přítomnost	přítomnost	k1gFnSc1	přítomnost
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
dokáže	dokázat	k5eAaPmIp3nS	dokázat
útokům	útok	k1gInPc3	útok
většinou	většinou	k6eAd1	většinou
předejít	předejít	k5eAaPmF	předejít
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
napadají	napadat	k5eAaBmIp3nP	napadat
chovná	chovný	k2eAgNnPc4d1	chovné
domácí	domácí	k2eAgNnPc4d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
jejich	jejich	k3xOp3gInSc3	jejich
přirozené	přirozený	k2eAgFnPc4d1	přirozená
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgFnPc4d1	žijící
kořisti	kořist	k1gFnPc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabít	zabít	k5eAaPmF	zabít
dobytek	dobytek	k1gInSc4	dobytek
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
snazší	snadný	k2eAgNnSc1d2	snazší
<g/>
,	,	kIx,	,
než	než	k8xS	než
nahánět	nahánět	k5eAaImF	nahánět
divokou	divoký	k2eAgFnSc4d1	divoká
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
kořist	kořist	k1gFnSc4	kořist
začít	začít	k5eAaPmF	začít
specializovat	specializovat	k5eAaBmF	specializovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
obětí	oběť	k1gFnSc7	oběť
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
krocani	krocan	k1gMnPc1	krocan
<g/>
,	,	kIx,	,
domestikovaní	domestikovaný	k2eAgMnPc1d1	domestikovaný
sobi	sob	k1gMnPc1	sob
a	a	k8xC	a
především	především	k6eAd1	především
ovce	ovce	k1gFnSc2	ovce
a	a	k8xC	a
kozy	koza	k1gFnSc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
státy	stát	k1gInPc4	stát
chovatelům	chovatel	k1gMnPc3	chovatel
hradí	hradit	k5eAaImIp3nS	hradit
škody	škoda	k1gFnPc4	škoda
vlky	vlk	k1gMnPc7	vlk
způsobené	způsobený	k2eAgNnSc1d1	způsobené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
napadají	napadat	k5eAaPmIp3nP	napadat
psy	pes	k1gMnPc4	pes
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
potírají	potírat	k5eAaImIp3nP	potírat
je	on	k3xPp3gInPc4	on
jako	jako	k8xS	jako
předpokládanou	předpokládaný	k2eAgFnSc4d1	předpokládaná
potravní	potravní	k2eAgFnSc4d1	potravní
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
obránce	obránce	k1gMnSc1	obránce
své	svůj	k3xOyFgFnSc2	svůj
kořisti	kořist	k1gFnSc2	kořist
(	(	kIx(	(
<g/>
dobytek	dobytek	k1gInSc4	dobytek
<g/>
)	)	kIx)	)
a	a	k8xC	a
někde	někde	k6eAd1	někde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
zdroj	zdroj	k1gInSc4	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
zabít	zabít	k5eAaPmF	zabít
i	i	k9	i
velká	velký	k2eAgNnPc4d1	velké
psí	psí	k2eAgNnPc4d1	psí
plemena	plemeno	k1gNnPc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gNnSc7	jejich
relativně	relativně	k6eAd1	relativně
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
čelisti	čelist	k1gFnPc1	čelist
a	a	k8xC	a
"	"	kIx"	"
<g/>
zákeřnější	zákeřní	k2eAgInSc4d2	zákeřní
<g/>
"	"	kIx"	"
způsob	způsob	k1gInSc4	způsob
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
zakousnout	zakousnout	k5eAaPmF	zakousnout
výhradně	výhradně	k6eAd1	výhradně
do	do	k7c2	do
krku	krk	k1gInSc2	krk
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vlákat	vlákat	k5eAaPmF	vlákat
psy	pes	k1gMnPc4	pes
do	do	k7c2	do
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
například	například	k6eAd1	například
jeden	jeden	k4xCgMnSc1	jeden
"	"	kIx"	"
<g/>
pronásledovaný	pronásledovaný	k2eAgMnSc1d1	pronásledovaný
<g/>
"	"	kIx"	"
vlk	vlk	k1gMnSc1	vlk
nažene	nahnat	k5eAaPmIp3nS	nahnat
psa	pes	k1gMnSc2	pes
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
psi	pes	k1gMnPc1	pes
chránící	chránící	k2eAgMnPc1d1	chránící
dobytek	dobytek	k1gInSc4	dobytek
dostávají	dostávat	k5eAaImIp3nP	dostávat
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
"	"	kIx"	"
<g/>
protivlčí	protivlčí	k2eAgInPc4d1	protivlčí
<g/>
"	"	kIx"	"
obojky	obojek	k1gInPc4	obojek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vlci	vlk	k1gMnPc1	vlk
se	se	k3xPyFc4	se
umějí	umět	k5eAaImIp3nP	umět
naučit	naučit	k5eAaPmF	naučit
zabít	zabít	k5eAaPmF	zabít
i	i	k9	i
takto	takto	k6eAd1	takto
vybavené	vybavený	k2eAgMnPc4d1	vybavený
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
používána	používán	k2eAgFnSc1d1	používána
obzvláště	obzvláště	k6eAd1	obzvláště
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
statná	statný	k2eAgNnPc1d1	statné
psí	psí	k2eAgNnPc1d1	psí
plemena	plemeno	k1gNnPc1	plemeno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vlkodav	vlkodav	k1gMnSc1	vlkodav
nebo	nebo	k8xC	nebo
mastif	mastif	k1gMnSc1	mastif
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
výše	výše	k1gFnPc4	výše
řečené	řečený	k2eAgFnPc4d1	řečená
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
velcí	velký	k2eAgMnPc1d1	velký
ovčáčtí	ovčácký	k2eAgMnPc1d1	ovčácký
psi	pes	k1gMnPc1	pes
většinou	většinou	k6eAd1	většinou
vlky	vlk	k1gMnPc4	vlk
odradit	odradit	k5eAaPmF	odradit
od	od	k7c2	od
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
snadnou	snadný	k2eAgFnSc4d1	snadná
kořist	kořist	k1gFnSc4	kořist
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
rizikovou	rizikový	k2eAgFnSc4d1	riziková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útoky	útok	k1gInPc1	útok
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
===	===	k?	===
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
alespoň	alespoň	k9	alespoň
zčásti	zčásti	k6eAd1	zčásti
splněny	splnit	k5eAaPmNgFnP	splnit
tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
relativně	relativně	k6eAd1	relativně
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
dostatek	dostatek	k1gInSc4	dostatek
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
nedostávají	dostávat	k5eNaImIp3nP	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
častého	častý	k2eAgInSc2d1	častý
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
množství	množství	k1gNnSc2	množství
loveni	loven	k2eAgMnPc1d1	loven
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
nakaženi	nakazit	k5eAaPmNgMnP	nakazit
vzteklinou	vzteklina	k1gFnSc7	vzteklina
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vlk	vlk	k1gMnSc1	vlk
stává	stávat	k5eAaImIp3nS	stávat
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
útocích	útok	k1gInPc6	útok
vlků	vlk	k1gMnPc2	vlk
existují	existovat	k5eAaImIp3nP	existovat
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
zvířata	zvíře	k1gNnPc1	zvíře
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
útoků	útok	k1gInPc2	útok
neovlivněných	ovlivněný	k2eNgInPc2d1	neovlivněný
vzteklinou	vzteklina	k1gFnSc7	vzteklina
směřovala	směřovat	k5eAaImAgFnS	směřovat
a	a	k8xC	a
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
na	na	k7c4	na
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
vzácně	vzácně	k6eAd1	vzácně
jsou	být	k5eAaImIp3nP	být
oběťmi	oběť	k1gFnPc7	oběť
i	i	k8xC	i
dospělí	dospělý	k2eAgMnPc1d1	dospělý
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
vyrušeni	vyrušen	k2eAgMnPc1d1	vyrušen
<g/>
,	,	kIx,	,
vlci	vlk	k1gMnPc1	vlk
své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
obvykle	obvykle	k6eAd1	obvykle
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vzteklina	vzteklina	k1gMnSc1	vzteklina
====	====	k?	====
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
nebývají	bývat	k5eNaImIp3nP	bývat
zdrojem	zdroj	k1gInSc7	zdroj
vztekliny	vzteklina	k1gFnSc2	vzteklina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
nakazit	nakazit	k5eAaPmF	nakazit
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
psovitých	psovitý	k2eAgFnPc2d1	psovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
velmi	velmi	k6eAd1	velmi
agresivními	agresivní	k2eAgInPc7d1	agresivní
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
pokousat	pokousat	k5eAaPmF	pokousat
mnoho	mnoho	k4c4	mnoho
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
zavedení	zavedení	k1gNnSc2	zavedení
očkování	očkování	k1gNnSc2	očkování
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pokousání	pokousání	k1gNnSc1	pokousání
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vzteklí	vzteklý	k2eAgMnPc1d1	vzteklý
vlci	vlk	k1gMnPc1	vlk
prakticky	prakticky	k6eAd1	prakticky
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
každoročně	každoročně	k6eAd1	každoročně
minimálně	minimálně	k6eAd1	minimálně
několik	několik	k4yIc4	několik
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
záznamech	záznam	k1gInPc6	záznam
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
států	stát	k1gInPc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgInP	doložit
tisíce	tisíc	k4xCgInPc1	tisíc
případů	případ	k1gInPc2	případ
útoků	útok	k1gInPc2	útok
vlků	vlk	k1gMnPc2	vlk
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
roku	rok	k1gInSc2	rok
1450	[number]	k4	1450
zabila	zabít	k5eAaPmAgFnS	zabít
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
smečka	smečka	k1gFnSc1	smečka
vedená	vedený	k2eAgFnSc1d1	vedená
samcem	samec	k1gInSc7	samec
jménem	jméno	k1gNnSc7	jméno
Courtaud	Courtauda	k1gFnPc2	Courtauda
asi	asi	k9	asi
40	[number]	k4	40
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
vlčí	vlčí	k2eAgInPc4d1	vlčí
útoky	útok	k1gInPc4	útok
"	"	kIx"	"
<g/>
nejbohatší	bohatý	k2eAgFnSc1d3	nejbohatší
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1580	[number]	k4	1580
<g/>
–	–	k?	–
<g/>
1830	[number]	k4	1830
3	[number]	k4	3
069	[number]	k4	069
lidí	člověk	k1gMnPc2	člověk
zabito	zabít	k5eAaPmNgNnS	zabít
vlky	vlk	k1gMnPc7	vlk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1	[number]	k4	1
857	[number]	k4	857
ne	ne	k9	ne
kvůli	kvůli	k7c3	kvůli
vzteklině	vzteklina	k1gFnSc3	vzteklina
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgNnSc7d3	nejznámější
řáděním	řádění	k1gNnSc7	řádění
"	"	kIx"	"
<g/>
vlka	vlk	k1gMnSc2	vlk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jestli	jestli	k8xS	jestli
šlo	jít	k5eAaImAgNnS	jít
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c4	o
vlka	vlk	k1gMnSc4	vlk
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
)	)	kIx)	)
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
útoky	útok	k1gInPc1	útok
tzv.	tzv.	kA	tzv.
gévaudanské	gévaudanský	k2eAgFnPc4d1	gévaudanský
bestie	bestie	k1gFnPc4	bestie
v	v	k7c6	v
letech	let	k1gInPc6	let
1764	[number]	k4	1764
<g/>
–	–	k?	–
<g/>
1767	[number]	k4	1767
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
asi	asi	k9	asi
113	[number]	k4	113
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Pád	Pád	k1gInSc1	Pád
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
uváděno	uvádět	k5eAaImNgNnS	uvádět
440	[number]	k4	440
mrtvých	mrtvý	k1gMnPc2	mrtvý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
útoků	útok	k1gInPc2	útok
vlků	vlk	k1gMnPc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c4	o
život	život	k1gInSc4	život
92	[number]	k4	92
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgFnPc2d2	mladší
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
carském	carský	k2eAgNnSc6d1	carské
Rusku	Rusko	k1gNnSc6	Rusko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
zabito	zabít	k5eAaPmNgNnS	zabít
169	[number]	k4	169
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
7	[number]	k4	7
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
dalších	další	k2eAgMnPc2d1	další
161	[number]	k4	161
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
zemřely	zemřít	k5eAaPmAgInP	zemřít
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
další	další	k2eAgFnSc2d1	další
stovky	stovka	k1gFnSc2	stovka
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
vlků	vlk	k1gMnPc2	vlk
sílily	sílit	k5eAaImAgFnP	sílit
během	během	k7c2	během
válečných	válečný	k2eAgInPc2d1	válečný
konfliktů	konflikt	k1gInPc2	konflikt
nebo	nebo	k8xC	nebo
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1916	[number]	k4	1916
<g/>
/	/	kIx~	/
<g/>
1917	[number]	k4	1917
napadalo	napadat	k5eAaImAgNnS	napadat
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vlků	vlk	k1gMnPc2	vlk
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zoufalého	zoufalý	k2eAgInSc2d1	zoufalý
nedostatku	nedostatek	k1gInSc2	nedostatek
jiné	jiný	k2eAgFnSc2d1	jiná
potravy	potrava	k1gFnSc2	potrava
ruské	ruský	k2eAgMnPc4d1	ruský
a	a	k8xC	a
německé	německý	k2eAgMnPc4d1	německý
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
v	v	k7c6	v
Kirovské	Kirovská	k1gFnSc6	Kirovská
oblasti	oblast	k1gFnSc2	oblast
SSSR	SSSR	kA	SSSR
zardousili	zardousit	k5eAaPmAgMnP	zardousit
vlci	vlk	k1gMnPc1	vlk
22	[number]	k4	22
dětí	dítě	k1gFnPc2	dítě
starých	starý	k2eAgFnPc2d1	stará
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
let	léto	k1gNnPc2	léto
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
zranili	zranit	k5eAaPmAgMnP	zranit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
vlčí	vlčí	k2eAgInPc4d1	vlčí
útoky	útok	k1gInPc4	útok
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
nedochází	docházet	k5eNaImIp3nS	docházet
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Asie	Asie	k1gFnSc2	Asie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
byly	být	k5eAaImAgFnP	být
a	a	k8xC	a
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
stále	stále	k6eAd1	stále
jsou	být	k5eAaImIp3nP	být
útoky	útok	k1gInPc4	útok
vlků	vlk	k1gMnPc2	vlk
relativně	relativně	k6eAd1	relativně
běžné	běžný	k2eAgFnPc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dané	daný	k2eAgFnSc6d1	daná
početností	početnost	k1gFnSc7	početnost
a	a	k8xC	a
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
lidské	lidský	k2eAgFnSc2d1	lidská
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
rozlehlostí	rozlehlost	k1gFnSc7	rozlehlost
území	území	k1gNnSc2	území
a	a	k8xC	a
dřívějším	dřívější	k2eAgNnSc6d1	dřívější
poměrně	poměrně	k6eAd1	poměrně
hojným	hojný	k2eAgNnSc7d1	hojné
rozšířením	rozšíření	k1gNnSc7	rozšíření
vlků	vlk	k1gMnPc2	vlk
na	na	k7c4	na
území	území	k1gNnSc4	území
tohoto	tento	k3xDgInSc2	tento
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
útokům	útok	k1gInPc3	útok
vzteklých	vzteklý	k2eAgInPc2d1	vzteklý
vlků	vlk	k1gMnPc2	vlk
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yRnSc2	což
byla	být	k5eAaImAgFnS	být
smrt	smrt	k1gFnSc1	smrt
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
množství	množství	k1gNnSc1	množství
vlčích	vlčí	k2eAgInPc2d1	vlčí
útoků	útok	k1gInPc2	útok
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Důvodů	důvod	k1gInPc2	důvod
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
–	–	k?	–
subkontinent	subkontinent	k1gInSc1	subkontinent
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
zalidněn	zalidněn	k2eAgInSc1d1	zalidněn
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
lidmi	člověk	k1gMnPc7	člověk
žijícími	žijící	k2eAgInPc7d1	žijící
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
početná	početný	k2eAgFnSc1d1	početná
vlčí	vlčí	k2eAgFnSc1d1	vlčí
populace	populace	k1gFnPc1	populace
a	a	k8xC	a
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
hinduistické	hinduistický	k2eAgFnSc2d1	hinduistická
tradice	tradice	k1gFnSc2	tradice
by	by	k9	by
prolití	prolití	k1gNnSc4	prolití
vlčí	vlčí	k2eAgFnSc2d1	vlčí
krve	krev	k1gFnSc2	krev
znamenalo	znamenat	k5eAaImAgNnS	znamenat
neúrodu	neúroda	k1gFnSc4	neúroda
a	a	k8xC	a
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
lidožraví	lidožravý	k2eAgMnPc1d1	lidožravý
vlci	vlk	k1gMnPc1	vlk
někdy	někdy	k6eAd1	někdy
nebyli	být	k5eNaImAgMnP	být
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
této	tento	k3xDgFnSc2	tento
kombinace	kombinace	k1gFnSc2	kombinace
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1875	[number]	k4	1875
a	a	k8xC	a
1878	[number]	k4	1878
zde	zde	k6eAd1	zde
zardousili	zardousit	k5eAaPmAgMnP	zardousit
lidožraví	lidožravý	k2eAgMnPc1d1	lidožravý
vlci	vlk	k1gMnPc1	vlk
721	[number]	k4	721
a	a	k8xC	a
624	[number]	k4	624
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
zabili	zabít	k5eAaPmAgMnP	zabít
vlci	vlk	k1gMnPc1	vlk
v	v	k7c6	v
indických	indický	k2eAgInPc6d1	indický
státech	stát	k1gInPc6	stát
Bihar	Bihar	k1gMnSc1	Bihar
a	a	k8xC	a
Uttarpradéš	Uttarpradéš	k1gMnSc1	Uttarpradéš
dohromady	dohromady	k6eAd1	dohromady
okolo	okolo	k7c2	okolo
120	[number]	k4	120
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vlky	vlk	k1gMnPc4	vlk
obecné	obecná	k1gFnSc2	obecná
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
útočníkem	útočník	k1gMnSc7	útočník
také	také	k6eAd1	také
dhoul	dhoula	k1gFnPc2	dhoula
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
dříve	dříve	k6eAd2	dříve
jako	jako	k8xS	jako
vlk	vlk	k1gMnSc1	vlk
rudý	rudý	k1gMnSc1	rudý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
jsou	být	k5eAaImIp3nP	být
útoky	útok	k1gInPc4	útok
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgNnSc1d1	časté
avšak	avšak	k8xC	avšak
s	s	k7c7	s
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
obětí	oběť	k1gFnPc2	oběť
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnohdy	mnohdy	k6eAd1	mnohdy
chybějí	chybět	k5eAaImIp3nP	chybět
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
mnoho	mnoho	k4c4	mnoho
případů	případ	k1gInPc2	případ
zřejmě	zřejmě	k6eAd1	zřejmě
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nepodchyceno	podchycen	k2eNgNnSc1d1	podchycen
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
incidentů	incident	k1gInPc2	incident
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
jde	jít	k5eAaImIp3nS	jít
na	na	k7c4	na
vrub	vrub	k1gInSc4	vrub
vzteklině	vzteklina	k1gFnSc3	vzteklina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rekordním	rekordní	k2eAgInSc6d1	rekordní
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
pokousáno	pokousat	k5eAaPmNgNnS	pokousat
vzteklými	vzteklý	k2eAgMnPc7d1	vzteklý
vlky	vlk	k1gMnPc7	vlk
jen	jen	k6eAd1	jen
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
329	[number]	k4	329
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Írán	Írán	k1gInSc1	Írán
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zavedla	zavést	k5eAaPmAgFnS	zavést
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
novou	nový	k2eAgFnSc4d1	nová
metodu	metoda	k1gFnSc4	metoda
ošetřování	ošetřování	k1gNnSc2	ošetřování
nakažených	nakažený	k2eAgFnPc2d1	nakažená
vzteklinou	vzteklina	k1gFnSc7	vzteklina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snížila	snížit	k5eAaPmAgFnS	snížit
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
poranil	poranit	k5eAaPmAgMnS	poranit
vzteklý	vzteklý	k2eAgMnSc1d1	vzteklý
vlk	vlk	k1gMnSc1	vlk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
18	[number]	k4	18
rolníků	rolník	k1gMnPc2	rolník
spících	spící	k2eAgMnPc2d1	spící
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
–	–	k?	–
všichni	všechen	k3xTgMnPc1	všechen
později	pozdě	k6eAd2	pozdě
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
útoky	útok	k1gInPc1	útok
vlků	vlk	k1gMnPc2	vlk
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zdokumentované	zdokumentovaný	k2eAgNnSc1d1	zdokumentované
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
jen	jen	k9	jen
k	k	k7c3	k
několika	několik	k4yIc3	několik
desítkám	desítka	k1gFnPc3	desítka
útoků	útok	k1gInPc2	útok
a	a	k8xC	a
incidentů	incident	k1gInPc2	incident
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
člověka	člověk	k1gMnSc2	člověk
skončilo	skončit	k5eAaPmAgNnS	skončit
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
případů	případ	k1gInPc2	případ
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlci	vlk	k1gMnPc1	vlk
původně	původně	k6eAd1	původně
napadli	napadnout	k5eAaPmAgMnP	napadnout
psy	pes	k1gMnPc7	pes
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
lidi	člověk	k1gMnPc4	člověk
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
několik	několik	k4yIc1	několik
incidentů	incident	k1gInPc2	incident
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzteklí	vzteklý	k2eAgMnPc1d1	vzteklý
vlci	vlk	k1gMnPc1	vlk
pokousali	pokousat	k5eAaPmAgMnP	pokousat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
následně	následně	k6eAd1	následně
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
obětí	oběť	k1gFnPc2	oběť
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
místy	místo	k1gNnPc7	místo
světa	svět	k1gInSc2	svět
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lov	lov	k1gInSc1	lov
vlků	vlk	k1gMnPc2	vlk
===	===	k?	===
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
loví	lovit	k5eAaImIp3nP	lovit
vlky	vlk	k1gMnPc7	vlk
přinejmenším	přinejmenším	k6eAd1	přinejmenším
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
domestikace	domestikace	k1gFnSc2	domestikace
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
,	,	kIx,	,
zisku	zisk	k1gInSc2	zisk
trofeje	trofej	k1gFnSc2	trofej
<g/>
,	,	kIx,	,
ochraně	ochrana	k1gFnSc3	ochrana
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgFnSc2d1	žijící
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
kvůli	kvůli	k7c3	kvůli
bezpečí	bezpečí	k1gNnSc3	bezpečí
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lov	lov	k1gInSc1	lov
na	na	k7c4	na
vlky	vlk	k1gMnPc4	vlk
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
chytrá	chytrá	k1gFnSc1	chytrá
<g/>
,	,	kIx,	,
přizpůsobivá	přizpůsobivý	k2eAgNnPc1d1	přizpůsobivé
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgNnPc1d1	vytrvalé
a	a	k8xC	a
nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
mnoho	mnoho	k4c1	mnoho
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
účinnou	účinný	k2eAgFnSc7d1	účinná
metodou	metoda	k1gFnSc7	metoda
bylo	být	k5eAaImAgNnS	být
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
jejich	jejich	k3xOp3gNnPc2	jejich
doupat	doupě	k1gNnPc2	doupě
a	a	k8xC	a
zabíjení	zabíjení	k1gNnSc2	zabíjení
malých	malý	k2eAgNnPc2d1	malé
vlčat	vlče	k1gNnPc2	vlče
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
k	k	k7c3	k
lovu	lov	k1gInSc2	lov
používali	používat	k5eAaImAgMnP	používat
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
ukázala	ukázat	k5eAaPmAgFnS	ukázat
se	se	k3xPyFc4	se
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kombinace	kombinace	k1gFnSc1	kombinace
chrtů	chrt	k1gMnPc2	chrt
<g/>
,	,	kIx,	,
bloodhoundů	bloodhound	k1gInPc2	bloodhound
(	(	kIx(	(
<g/>
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
velkých	velká	k1gFnPc2	velká
a	a	k8xC	a
silných	silný	k2eAgNnPc2d1	silné
plemen	plemeno	k1gNnPc2	plemeno
<g/>
)	)	kIx)	)
a	a	k8xC	a
foxhoundů	foxhound	k1gInPc2	foxhound
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
tichého	tichý	k2eAgNnSc2d1	tiché
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
,	,	kIx,	,
přepadů	přepad	k1gInPc2	přepad
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
či	či	k8xC	či
stopování	stopování	k1gNnSc2	stopování
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
neosvědčily	osvědčit	k5eNaPmAgFnP	osvědčit
kvůli	kvůli	k7c3	kvůli
vlčí	vlčí	k2eAgFnSc3d1	vlčí
opatrnosti	opatrnost	k1gFnSc3	opatrnost
a	a	k8xC	a
vynikajícímu	vynikající	k2eAgInSc3d1	vynikající
sluchu	sluch	k1gInSc3	sluch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používání	používání	k1gNnSc1	používání
strychninem	strychnin	k1gInSc7	strychnin
otrávených	otrávený	k2eAgFnPc2d1	otrávená
návnad	návnada	k1gFnPc2	návnada
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
také	také	k9	také
praktikovalo	praktikovat	k5eAaImAgNnS	praktikovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
upustilo	upustit	k5eAaPmAgNnS	upustit
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
vlci	vlk	k1gMnPc1	vlk
(	(	kIx(	(
<g/>
či	či	k8xC	či
kojoti	kojot	k1gMnPc1	kojot
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
se	se	k3xPyFc4	se
naučit	naučit	k5eAaPmF	naučit
jed	jed	k1gInSc1	jed
v	v	k7c6	v
návnadě	návnada	k1gFnSc6	návnada
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jed	jed	k1gInSc1	jed
znehodnocuje	znehodnocovat	k5eAaImIp3nS	znehodnocovat
kožešinu	kožešina	k1gFnSc4	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgInSc4d1	účinný
především	především	k6eAd1	především
vůči	vůči	k7c3	vůči
mláďatům	mládě	k1gNnPc3	mládě
a	a	k8xC	a
mladým	mladý	k2eAgMnPc3d1	mladý
nezkušeným	zkušený	k2eNgMnPc3d1	nezkušený
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Nášlapné	nášlapný	k2eAgFnPc1d1	nášlapná
pasti	past	k1gFnPc1	past
jsou	být	k5eAaImIp3nP	být
efektivní	efektivní	k2eAgFnPc1d1	efektivní
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
neulpí	ulpět	k5eNaPmIp3nS	ulpět
lidský	lidský	k2eAgInSc1d1	lidský
pach	pach	k1gInSc1	pach
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgInPc1d1	americký
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
upřednostňovaly	upřednostňovat	k5eAaImAgFnP	upřednostňovat
padací	padací	k2eAgFnPc1d1	padací
pasti	past	k1gFnPc1	past
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
různých	různý	k2eAgFnPc2d1	různá
nástrah	nástraha	k1gFnPc2	nástraha
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
schopnostech	schopnost	k1gFnPc6	schopnost
vlčího	vlčí	k2eAgMnSc2d1	vlčí
jedince	jedinec	k1gMnSc2	jedinec
past	past	k1gFnSc4	past
odhalit	odhalit	k5eAaPmF	odhalit
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
ve	v	k7c6	v
vyhýbání	vyhýbání	k1gNnSc6	vyhýbání
se	se	k3xPyFc4	se
nástrahám	nástraha	k1gFnPc3	nástraha
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
byl	být	k5eAaImAgMnS	být
samec	samec	k1gMnSc1	samec
Lobo	Lobo	k1gMnSc1	Lobo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
lovit	lovit	k5eAaImF	lovit
Ernest	Ernest	k1gMnSc1	Ernest
Thompson	Thompson	k1gMnSc1	Thompson
Seton	Seton	k1gMnSc1	Seton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lovecké	lovecký	k2eAgFnPc1d1	lovecká
skrýše	skrýš	k1gFnPc1	skrýš
a	a	k8xC	a
posedy	posed	k1gInPc1	posed
dokáží	dokázat	k5eAaPmIp3nP	dokázat
být	být	k5eAaImF	být
účinné	účinný	k2eAgInPc1d1	účinný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepoužívají	používat	k5eNaImIp3nP	používat
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
čas	čas	k1gInSc4	čas
a	a	k8xC	a
trpělivost	trpělivost	k1gFnSc4	trpělivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgMnSc1d1	populární
nahnat	nahnat	k5eAaPmF	nahnat
vlky	vlk	k1gMnPc4	vlk
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
území	území	k1gNnSc4	území
pomocí	pomocí	k7c2	pomocí
kolíků	kolík	k1gInPc2	kolík
a	a	k8xC	a
provazů	provaz	k1gInPc2	provaz
natažených	natažený	k2eAgInPc2d1	natažený
asi	asi	k9	asi
metr	metr	k1gInSc4	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
ověšených	ověšený	k2eAgFnPc2d1	ověšená
různobarevnými	různobarevný	k2eAgFnPc7d1	různobarevná
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vlků	vlk	k1gMnPc2	vlk
se	se	k3xPyFc4	se
těchto	tento	k3xDgInPc2	tento
ohradníků	ohradník	k1gInPc2	ohradník
bojí	bát	k5eAaImIp3nS	bát
a	a	k8xC	a
neproběhne	proběhnout	k5eNaPmIp3nS	proběhnout
jimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
pachem	pach	k1gInSc7	pach
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
látkách	látka	k1gFnPc6	látka
ulpěl	ulpět	k5eAaPmAgMnS	ulpět
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lovci	lovec	k1gMnPc1	lovec
lákají	lákat	k5eAaImIp3nP	lákat
vlky	vlk	k1gMnPc7	vlk
na	na	k7c4	na
vytí	vytí	k1gNnSc4	vytí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
účinné	účinný	k2eAgNnSc1d1	účinné
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
se	se	k3xPyFc4	se
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
na	na	k7c4	na
vlky	vlk	k1gMnPc7	vlk
orli	orel	k1gMnPc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
však	však	k9	však
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zkušených	zkušený	k2eAgMnPc2d1	zkušený
sokolníků	sokolník	k1gMnPc2	sokolník
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
účinnou	účinný	k2eAgFnSc7d1	účinná
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
střílení	střílení	k1gNnSc1	střílení
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
z	z	k7c2	z
vrtulníků	vrtulník	k1gInPc2	vrtulník
a	a	k8xC	a
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
mají	mít	k5eAaImIp3nP	mít
vlci	vlk	k1gMnPc1	vlk
šanci	šance	k1gFnSc4	šance
uniknout	uniknout	k5eAaPmF	uniknout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zalesněném	zalesněný	k2eAgInSc6d1	zalesněný
nebo	nebo	k8xC	nebo
členitém	členitý	k2eAgInSc6d1	členitý
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
nejintenzivnějšího	intenzivní	k2eAgInSc2d3	nejintenzivnější
lovu	lov	k1gInSc2	lov
vlků	vlk	k1gMnPc2	vlk
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ohraničit	ohraničit	k5eAaPmF	ohraničit
zhruba	zhruba	k6eAd1	zhruba
léty	léto	k1gNnPc7	léto
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
museli	muset	k5eAaImAgMnP	muset
vlci	vlk	k1gMnPc1	vlk
čelit	čelit	k5eAaImF	čelit
všem	všecek	k3xTgFnPc3	všecek
nejmodernějším	moderní	k2eAgFnPc3d3	nejmodernější
vymoženostem	vymoženost	k1gFnPc3	vymoženost
lidské	lidský	k2eAgFnSc2d1	lidská
likvidační	likvidační	k2eAgFnSc2d1	likvidační
technologie	technologie	k1gFnSc2	technologie
–	–	k?	–
ocelovým	ocelový	k2eAgFnPc3d1	ocelová
pastem	past	k1gFnPc3	past
<g/>
,	,	kIx,	,
jedům	jed	k1gInPc3	jed
<g/>
,	,	kIx,	,
opakovacím	opakovací	k2eAgFnPc3d1	opakovací
puškám	puška	k1gFnPc3	puška
a	a	k8xC	a
následně	následně	k6eAd1	následně
letadlům	letadlo	k1gNnPc3	letadlo
či	či	k8xC	či
vrtulníkům	vrtulník	k1gInPc3	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lovu	lov	k1gInSc2	lov
vlků	vlk	k1gMnPc2	vlk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
sport	sport	k1gInSc1	sport
a	a	k8xC	a
zábava	zábava	k1gFnSc1	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byly	být	k5eAaImAgFnP	být
stovky	stovka	k1gFnPc4	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
zabitých	zabitý	k2eAgMnPc2d1	zabitý
vlků	vlk	k1gMnPc2	vlk
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
byl	být	k5eAaImAgInS	být
vlk	vlk	k1gMnSc1	vlk
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
a	a	k8xC	a
několik	několik	k4yIc4	několik
poddruhů	poddruh	k1gInPc2	poddruh
nenávratně	návratně	k6eNd1	návratně
ztraceno	ztratit	k5eAaPmNgNnS	ztratit
<g/>
.	.	kIx.	.
<g/>
Přestože	přestože	k8xS	přestože
vlci	vlk	k1gMnPc1	vlk
byli	být	k5eAaImAgMnP	být
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
takřka	takřka	k6eAd1	takřka
všude	všude	k6eAd1	všude
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
stále	stále	k6eAd1	stále
loveni	lovit	k5eAaImNgMnP	lovit
<g/>
,	,	kIx,	,
daří	dařit	k5eAaImIp3nS	dařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
stále	stále	k6eAd1	stále
přežívat	přežívat	k5eAaImF	přežívat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
ukázek	ukázka	k1gFnPc2	ukázka
jejich	jejich	k3xOp3gFnPc2	jejich
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgInPc1d1	uvedený
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
tyto	tento	k3xDgFnPc4	tento
šelmy	šelma	k1gFnPc4	šelma
lovit	lovit	k5eAaImF	lovit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
diskutabilní	diskutabilní	k2eAgMnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
údajně	údajně	k6eAd1	údajně
chrání	chránit	k5eAaImIp3nS	chránit
populace	populace	k1gFnSc1	populace
jelenů	jelen	k1gMnPc2	jelen
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
vlčí	vlčí	k2eAgFnSc2d1	vlčí
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
údajně	údajně	k6eAd1	údajně
vlci	vlk	k1gMnPc1	vlk
decimují	decimovat	k5eAaBmIp3nP	decimovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
vyvráceno	vyvrácen	k2eAgNnSc1d1	vyvráceno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Vlci	vlk	k1gMnPc1	vlk
jako	jako	k8xS	jako
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
===	===	k?	===
</s>
</p>
<p>
<s>
Chov	chov	k1gInSc1	chov
vlků	vlk	k1gMnPc2	vlk
jako	jako	k8xS	jako
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc1d1	populární
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
předky	předek	k1gInPc4	předek
domácích	domácí	k2eAgMnPc2d1	domácí
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
existují	existovat	k5eAaImIp3nP	existovat
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
šelmami	šelma	k1gFnPc7	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Vlčata	vlče	k1gNnPc1	vlče
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
odejmuta	odejmut	k2eAgNnPc1d1	odejmuto
matkám	matka	k1gFnPc3	matka
nejlépe	dobře	k6eAd3	dobře
po	po	k7c6	po
14	[number]	k4	14
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
však	však	k9	však
do	do	k7c2	do
21	[number]	k4	21
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
socializace	socializace	k1gFnSc1	socializace
extrémně	extrémně	k6eAd1	extrémně
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Mléčné	mléčný	k2eAgFnPc4d1	mléčná
náhražky	náhražka	k1gFnPc4	náhražka
používané	používaný	k2eAgFnPc4d1	používaná
pro	pro	k7c4	pro
štěňata	štěně	k1gNnPc4	štěně
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
vlčata	vlče	k1gNnPc4	vlče
opatřena	opatřit	k5eAaPmNgFnS	opatřit
přídavkem	přídavek	k1gInSc7	přídavek
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
arginin	arginin	k2eAgMnSc1d1	arginin
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
hrozí	hrozit	k5eAaImIp3nS	hrozit
oční	oční	k2eAgInPc4d1	oční
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
jsou	být	k5eAaImIp3nP	být
vlci	vlk	k1gMnPc1	vlk
chovaní	chovaný	k2eAgMnPc1d1	chovaný
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
nepředvídatelnější	předvídatelný	k2eNgFnPc1d2	nepředvídatelnější
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
především	především	k6eAd1	především
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
objektem	objekt	k1gInSc7	objekt
agresivního	agresivní	k2eAgNnSc2d1	agresivní
chování	chování	k1gNnSc2	chování
"	"	kIx"	"
<g/>
ochočených	ochočený	k2eAgMnPc2d1	ochočený
<g/>
"	"	kIx"	"
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
lidské	lidský	k2eAgFnPc1d1	lidská
děti	dítě	k1gFnPc1	dítě
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
mají	mít	k5eAaImIp3nP	mít
silné	silný	k2eAgInPc4d1	silný
instinkty	instinkt	k1gInPc4	instinkt
pro	pro	k7c4	pro
život	život	k1gInSc4	život
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
(	(	kIx(	(
<g/>
alfa	alfa	k1gNnSc1	alfa
až	až	k8xS	až
omega	omega	k1gFnSc1	omega
status	status	k1gInSc1	status
<g/>
)	)	kIx)	)
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
soupeřit	soupeřit	k5eAaImF	soupeřit
o	o	k7c4	o
"	"	kIx"	"
<g/>
vylepšení	vylepšení	k1gNnSc4	vylepšení
<g/>
"	"	kIx"	"
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc4	ten
uznají	uznat	k5eAaPmIp3nP	uznat
za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
i	i	k9	i
jejich	jejich	k3xOp3gMnPc1	jejich
dospělí	dospělý	k2eAgMnPc1d1	dospělý
chovatelé	chovatel	k1gMnPc1	chovatel
<g/>
.	.	kIx.	.
</s>
<s>
Ochočení	ochočený	k2eAgMnPc1d1	ochočený
vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
vázáni	vázán	k2eAgMnPc1d1	vázán
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
smečku	smečka	k1gFnSc4	smečka
<g/>
"	"	kIx"	"
tak	tak	k6eAd1	tak
silně	silně	k6eAd1	silně
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostatní	ostatní	k2eAgMnPc4d1	ostatní
lidi	člověk	k1gMnPc4	člověk
obvykle	obvykle	k6eAd1	obvykle
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
a	a	k8xC	a
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
anebo	anebo	k8xC	anebo
je	on	k3xPp3gMnPc4	on
mohou	moct	k5eAaImIp3nP	moct
napadnout	napadnout	k5eAaPmF	napadnout
jako	jako	k9	jako
vetřelce	vetřelec	k1gMnSc4	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgNnSc1d1	psí
žrádlo	žrádlo	k1gNnSc1	žrádlo
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vlky	vlk	k1gMnPc4	vlk
nevhodné	vhodný	k2eNgMnPc4d1	nevhodný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
krmit	krmit	k5eAaImF	krmit
je	on	k3xPp3gInPc4	on
částmi	část	k1gFnPc7	část
zvířecích	zvířecí	k2eAgNnPc2d1	zvířecí
těl	tělo	k1gNnPc2	tělo
(	(	kIx(	(
<g/>
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výcvik	výcvik	k1gInSc1	výcvik
vlků	vlk	k1gMnPc2	vlk
je	být	k5eAaImIp3nS	být
náročnější	náročný	k2eAgMnSc1d2	náročnější
než	než	k8xS	než
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
méně	málo	k6eAd2	málo
povelů	povel	k1gInPc2	povel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výcviku	výcvik	k1gInSc6	výcvik
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
začít	začít	k5eAaPmF	začít
nudit	nudit	k5eAaImF	nudit
a	a	k8xC	a
přestanou	přestat	k5eAaPmIp3nP	přestat
se	se	k3xPyFc4	se
o	o	k7c4	o
povely	povel	k1gInPc4	povel
zajímat	zajímat	k5eAaImF	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
kontrolovatelní	kontrolovatelný	k2eAgMnPc1d1	kontrolovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Reagují	reagovat	k5eAaBmIp3nP	reagovat
lépe	dobře	k6eAd2	dobře
na	na	k7c4	na
pohybové	pohybový	k2eAgInPc4d1	pohybový
signály	signál	k1gInPc4	signál
než	než	k8xS	než
na	na	k7c4	na
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
jsou	být	k5eAaImIp3nP	být
vynalézavější	vynalézavý	k2eAgMnPc1d2	vynalézavější
a	a	k8xC	a
dokáží	dokázat	k5eAaPmIp3nP	dokázat
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
například	například	k6eAd1	například
naučit	naučit	k5eAaPmF	naučit
otevírat	otevírat	k5eAaImF	otevírat
různé	různý	k2eAgInPc4d1	různý
západkové	západkový	k2eAgInPc4d1	západkový
mechanismy	mechanismus	k1gInPc4	mechanismus
u	u	k7c2	u
kotců	kotec	k1gInPc2	kotec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
poslušnost	poslušnost	k1gFnSc1	poslušnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
inteligence	inteligence	k1gFnSc1	inteligence
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
domestikovaných	domestikovaný	k2eAgMnPc2d1	domestikovaný
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
chápou	chápat	k5eAaImIp3nP	chápat
také	také	k9	také
lépe	dobře	k6eAd2	dobře
příčinu	příčina	k1gFnSc4	příčina
a	a	k8xC	a
následek	následek	k1gInSc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
po	po	k7c6	po
ochočení	ochočení	k1gNnSc6	ochočení
utekli	utéct	k5eAaPmAgMnP	utéct
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
nebezpečnější	bezpečný	k2eNgFnPc1d2	nebezpečnější
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
než	než	k8xS	než
divocí	divoký	k2eAgMnPc1d1	divoký
vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
postrádají	postrádat	k5eAaImIp3nP	postrádat
strach	strach	k1gInSc4	strach
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pracovní	pracovní	k2eAgNnPc1d1	pracovní
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
tažní	tažní	k2eAgMnPc1d1	tažní
<g/>
,	,	kIx,	,
policejní	policejní	k2eAgMnPc1d1	policejní
<g/>
,	,	kIx,	,
hlídací	hlídací	k2eAgMnPc1d1	hlídací
vlci	vlk	k1gMnPc1	vlk
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
využitelní	využitelný	k2eAgMnPc1d1	využitelný
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nemoci	nemoc	k1gFnPc4	nemoc
a	a	k8xC	a
paraziti	parazit	k1gMnPc1	parazit
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
putují	putovat	k5eAaImIp3nP	putovat
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
tak	tak	k9	tak
stát	stát	k5eAaPmF	stát
přenašeči	přenašeč	k1gMnPc1	přenašeč
různých	různý	k2eAgFnPc2d1	různá
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnPc4d1	infekční
choroby	choroba	k1gFnPc4	choroba
roznášené	roznášený	k2eAgFnPc4d1	roznášená
vlky	vlk	k1gMnPc7	vlk
jsou	být	k5eAaImIp3nP	být
brucelóza	brucelóza	k1gFnSc1	brucelóza
<g/>
,	,	kIx,	,
tularémie	tularémie	k1gFnSc1	tularémie
<g/>
,	,	kIx,	,
listerióza	listerióza	k1gFnSc1	listerióza
a	a	k8xC	a
anthrax	anthrax	k1gInSc1	anthrax
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
rovněž	rovněž	k9	rovněž
nakazit	nakazit	k5eAaPmF	nakazit
vzteklinou	vzteklina	k1gFnSc7	vzteklina
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejsou	být	k5eNaImIp3nP	být
jejími	její	k3xOp3gMnPc7	její
hlavními	hlavní	k2eAgMnPc7d1	hlavní
přenašeči	přenašeč	k1gMnPc7	přenašeč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
trpí	trpět	k5eAaImIp3nS	trpět
virovým	virový	k2eAgNnSc7d1	virové
onemocněním	onemocnění	k1gNnSc7	onemocnění
Canine	Canin	k1gInSc5	Canin
distemper	distempra	k1gFnPc2	distempra
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
střevních	střevní	k2eAgFnPc2d1	střevní
potíží	potíž	k1gFnPc2	potíž
je	být	k5eAaImIp3nS	být
virus	virus	k1gInSc1	virus
Canine	Canin	k1gInSc5	Canin
coronavirus	coronavirus	k1gInSc4	coronavirus
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hostiteli	hostitel	k1gMnPc7	hostitel
asi	asi	k9	asi
50	[number]	k4	50
různých	různý	k2eAgMnPc2d1	různý
parazitů	parazit	k1gMnPc2	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klíšťata	klíště	k1gNnPc4	klíště
<g/>
,	,	kIx,	,
roztoče	roztoč	k1gMnPc4	roztoč
(	(	kIx(	(
<g/>
svrab-prašivina	svrabrašivina	k1gFnSc1	svrab-prašivina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vši	veš	k1gFnPc4	veš
<g/>
,	,	kIx,	,
blechy	blecha	k1gFnPc4	blecha
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc4	červ
(	(	kIx(	(
<g/>
škrkavka	škrkavka	k1gMnSc1	škrkavka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlístice	hlístice	k1gFnSc1	hlístice
(	(	kIx(	(
<g/>
svalovec	svalovec	k1gMnSc1	svalovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tasemnice	tasemnice	k1gFnPc4	tasemnice
aj.	aj.	kA	aj.
Vlčí	vlčí	k2eAgFnPc4d1	vlčí
smečky	smečka	k1gFnPc4	smečka
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
šíření	šíření	k1gNnSc3	šíření
infekčních	infekční	k2eAgFnPc2d1	infekční
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
začne	začít	k5eAaPmIp3nS	začít
jevit	jevit	k5eAaImF	jevit
známky	známka	k1gFnPc4	známka
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
smečku	smečka	k1gFnSc4	smečka
obvykle	obvykle	k6eAd1	obvykle
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Gray	Graa	k1gFnSc2	Graa
wolf	wolf	k1gMnSc1	wolf
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
List	list	k1gInSc1	list
of	of	k?	of
grey	grea	k1gFnSc2	grea
wolf	wolf	k1gMnSc1	wolf
populations	populations	k6eAd1	populations
by	by	k9	by
country	country	k2eAgMnSc1d1	country
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Wolf	Wolf	k1gMnSc1	Wolf
attacks	attacks	k6eAd1	attacks
on	on	k3xPp3gMnSc1	on
humans	humans	k6eAd1	humans
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Wolves	Wolves	k1gMnSc1	Wolves
as	as	k9	as
pets	pets	k1gInSc4	pets
and	and	k?	and
working	working	k1gInSc1	working
animals	animals	k1gInSc1	animals
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkráceno	zkrátit	k5eAaPmNgNnS	zkrátit
a	a	k8xC	a
upraveno	upravit	k5eAaPmNgNnS	upravit
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
přátel	přítel	k1gMnPc2	přítel
vlků	vlk	k1gMnPc2	vlk
</s>
</p>
<p>
<s>
Vlčí	vlčí	k2eAgInSc1d1	vlčí
svět	svět	k1gInSc1	svět
-	-	kIx~	-
www.vlci.info	www.vlci.info	k6eAd1	www.vlci.info
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
na	na	k7c4	na
www.priroda.cz	www.priroda.cz	k1gInSc4	www.priroda.cz
</s>
</p>
<p>
<s>
International	Internationat	k5eAaImAgInS	Internationat
wolf	wolf	k1gInSc1	wolf
center	centrum	k1gNnPc2	centrum
-	-	kIx~	-
engl	engl	k1gInSc1	engl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
na	na	k7c6	na
Biolibu	Biolib	k1gInSc6	Biolib
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
na	na	k7c4	na
www.selmy.cz	www.selmy.cz	k1gInSc4	www.selmy.cz
</s>
</p>
<p>
<s>
Vlčí	vlčet	k5eAaImIp3nS	vlčet
-	-	kIx~	-
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
,	,	kIx,	,
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
smečka	smečka	k1gFnSc1	smečka
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
na	na	k7c6	na
www.graywolf.eu	www.graywolf.eus	k1gInSc6	www.graywolf.eus
-	-	kIx~	-
CZ	CZ	kA	CZ
</s>
</p>
<p>
<s>
AOPK	AOPK	kA	AOPK
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
vlků	vlk	k1gMnPc2	vlk
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ANDĚRA	ANDĚRA	kA	ANDĚRA
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
a	a	k8xC	a
GAISLER	GAISLER	kA	GAISLER
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERVENÝ	Červený	k1gMnSc1	Červený
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
myslivosti	myslivost	k1gFnSc2	myslivost
<g/>
.	.	kIx.	.
</s>
<s>
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠKALOUD	Škaloud	k1gMnSc1	Škaloud
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Liška	Liška	k1gMnSc1	Liška
a	a	k8xC	a
větší	veliký	k2eAgFnPc4d2	veliký
šelmy	šelma	k1gFnPc4	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Brázda	brázda	k1gFnSc1	brázda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TURŇA	TURŇA	kA	TURŇA
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc1	Pavol
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vlk	Vlk	k1gMnSc1	Vlk
-	-	kIx~	-
tajemná	tajemný	k2eAgFnSc1d1	tajemná
a	a	k8xC	a
inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
šelma	šelma	k1gFnSc1	šelma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Naše	náš	k3xOp1gFnSc1	náš
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KVASNICA	KVASNICA	kA	KVASNICA
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Monte	Mont	k1gMnSc5	Mont
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
I	i	k8xC	i
-	-	kIx~	-
Rapsodie	rapsodie	k1gFnSc1	rapsodie
šedých	šedý	k2eAgInPc2d1	šedý
stínů	stín	k1gInPc2	stín
<g/>
.	.	kIx.	.
</s>
<s>
Élysion	Élysion	k1gInSc1	Élysion
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KVASNICA	KVASNICA	kA	KVASNICA
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Monte	Mont	k1gMnSc5	Mont
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
II	II	kA	II
-	-	kIx~	-
Honba	honba	k1gFnSc1	honba
za	za	k7c7	za
přízrakem	přízrak	k1gInSc7	přízrak
Gévaudanu	Gévaudan	k1gInSc2	Gévaudan
<g/>
.	.	kIx.	.
</s>
<s>
Élysion	Élysion	k1gInSc1	Élysion
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KVASNICA	KVASNICA	kA	KVASNICA
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Monte	Mont	k1gInSc5	Mont
<g/>
.	.	kIx.	.
</s>
<s>
Jantarové	jantarový	k2eAgFnPc4d1	jantarová
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Élysion	Élysion	k1gInSc1	Élysion
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filmy	film	k1gInPc1	film
o	o	k7c6	o
vlcích	vlk	k1gMnPc6	vlk
===	===	k?	===
</s>
</p>
<p>
<s>
Vlčí	vlčí	k2eAgFnPc1d1	vlčí
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lobo	Lobo	k1gMnSc1	Lobo
-	-	kIx~	-
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
změnil	změnit	k5eAaPmAgInS	změnit
Ameriku	Amerika	k1gFnSc4	Amerika
(	(	kIx(	(
<g/>
VB	VB	kA	VB
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
indické	indický	k2eAgFnSc2d1	indická
pouště	poušť	k1gFnSc2	poušť
(	(	kIx(	(
<g/>
VB	VB	kA	VB
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
