<p>
<s>
Roberto	Roberta	k1gFnSc5	Roberta
Matta	Matta	k1gFnSc1	Matta
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Roberto	Roberta	k1gFnSc5	Roberta
Sebastián	Sebastián	k1gMnSc1	Sebastián
Antonio	Antonio	k1gMnSc1	Antonio
Matta	Matta	k1gMnSc1	Matta
Echaurren	Echaurrna	k1gFnPc2	Echaurrna
<g/>
;	;	kIx,	;
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Chile	Chile	k1gNnSc1	Chile
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Civitavecchia	Civitavecchia	k1gFnSc1	Civitavecchia
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
chilský	chilský	k2eAgMnSc1d1	chilský
surrealistický	surrealistický	k2eAgMnSc1d1	surrealistický
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
v	v	k7c4	v
Santiago	Santiago	k1gNnSc4	Santiago
de	de	k?	de
Chile	Chile	k1gNnSc2	Chile
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
francouzského	francouzský	k2eAgMnSc2d1	francouzský
a	a	k8xC	a
baskického	baskický	k2eAgInSc2d1	baskický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
studoval	studovat	k5eAaImAgMnS	studovat
architekturu	architektura	k1gFnSc4	architektura
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
studium	studium	k1gNnSc4	studium
dokončil	dokončit	k5eAaPmAgMnS	dokončit
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Architektuře	architektura	k1gFnSc3	architektura
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
přestal	přestat	k5eAaPmAgInS	přestat
věnovat	věnovat	k5eAaPmF	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
kresbě	kresba	k1gFnSc3	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
malovat	malovat	k5eAaImF	malovat
a	a	k8xC	a
po	po	k7c6	po
začátku	začátek	k1gInSc2	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
začal	začít	k5eAaPmAgMnS	začít
počátkem	počátkem	k7c2	počátkem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
přidávat	přidávat	k5eAaImF	přidávat
jíl	jíl	k1gInSc4	jíl
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
bezvýchodnost	bezvýchodnost	k1gFnSc4	bezvýchodnost
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
zde	zde	k6eAd1	zde
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nástěnnou	nástěnný	k2eAgFnSc4d1	nástěnná
malbu	malba	k1gFnSc4	malba
pro	pro	k7c4	pro
zdejší	zdejší	k2eAgNnSc4d1	zdejší
sídlo	sídlo	k1gNnSc4	sídlo
organizace	organizace	k1gFnSc2	organizace
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
městě	město	k1gNnSc6	město
Civitavecchia	Civitavecchium	k1gNnSc2	Civitavecchium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavy	výstava	k1gFnPc1	výstava
==	==	k?	==
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
</s>
</p>
<p>
<s>
1957	[number]	k4	1957
:	:	kIx,	:
Henri	Henr	k1gMnPc1	Henr
Déchanet	Déchanet	k1gMnSc1	Déchanet
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gMnSc1	Oscar
Gauthier	Gauthier	k1gMnSc1	Gauthier
<g/>
,	,	kIx,	,
Henri	Henri	k1gNnSc1	Henri
Goetz	Goetza	k1gFnPc2	Goetza
<g/>
,	,	kIx,	,
Roberto	Roberta	k1gFnSc5	Roberta
Matta	Matta	k1gMnSc1	Matta
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Weiller	Weiller	k1gInSc1	Weiller
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
:	:	kIx,	:
Art	Art	k1gMnSc1	Art
latino-américain	latinoméricain	k1gMnSc1	latino-américain
à	à	k?	à
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
moderne	modernout	k5eAaPmIp3nS	modernout
de	de	k?	de
la	la	k1gNnSc7	la
ville	ville	k1gFnSc2	ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
s	s	k7c7	s
Jorge	Jorge	k1gNnSc7	Jorge
Camachem	Camach	k1gMnSc7	Camach
<g/>
,	,	kIx,	,
Wifredo	Wifredo	k1gNnSc1	Wifredo
Lamem	Lamem	k?	Lamem
<g/>
,	,	kIx,	,
Roberto	Roberta	k1gFnSc5	Roberta
Mattou	Mattá	k1gFnSc4	Mattá
<g/>
,	,	kIx,	,
<g/>
Hervé	Hervý	k2eAgNnSc4d1	Hervé
Télémaquem	Télémaquum	k1gNnSc7	Télémaquum
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
:	:	kIx,	:
Aspects	Aspects	k1gInSc1	Aspects
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
imagerie	imagerie	k1gFnSc1	imagerie
critique	critique	k1gFnSc1	critique
<g/>
,	,	kIx,	,
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
výstava	výstava	k1gFnSc1	výstava
Galerie	galerie	k1gFnSc1	galerie
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
Hauterive	Hauteriev	k1gFnPc1	Hauteriev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1975-1976	[number]	k4	1975-1976
:	:	kIx,	:
Trente	Trent	k1gInSc5	Trent
créateurs	créateurs	k1gInSc1	créateurs
<g/>
,	,	kIx,	,
Roberto	Roberta	k1gFnSc5	Roberta
Matta	Matta	k1gMnSc1	Matta
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
Alechinsky	Alechinsky	k1gMnSc5	Alechinsky
<g/>
,	,	kIx,	,
Olivier	Olivier	k1gMnSc1	Olivier
Debré	Debrý	k2eAgFnSc2d1	Debrý
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Hartung	Hartung	k1gMnSc1	Hartung
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
Heaulmé	Heaulmý	k2eAgFnSc2d1	Heaulmý
<g/>
,	,	kIx,	,
Zoran	Zoran	k1gMnSc1	Zoran
Mušič	Mušič	k1gMnSc1	Mušič
<g/>
,	,	kIx,	,
Edouard	Edouard	k1gMnSc1	Edouard
Pignon	Pignon	k1gMnSc1	Pignon
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
Soulages	Soulages	k1gInSc4	Soulages
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
:	:	kIx,	:
Retrospektiva	retrospektiva	k1gFnSc1	retrospektiva
<g/>
,	,	kIx,	,
MNAM-Musée	MNAM-Musée	k1gInSc1	MNAM-Musée
national	nationat	k5eAaImAgInS	nationat
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
moderne	modernout	k5eAaPmIp3nS	modernout
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
:	:	kIx,	:
Retrospektiva	retrospektiva	k1gFnSc1	retrospektiva
<g/>
,	,	kIx,	,
Musée	Muséus	k1gMnSc5	Muséus
Reine	Rein	k1gMnSc5	Rein
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
:	:	kIx,	:
Matta	Matt	k1gInSc2	Matt
1936-1944	[number]	k4	1936-1944
:	:	kIx,	:
début	début	k1gInSc1	début
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
nouveau	nouveau	k5eAaPmIp1nS	nouveau
monde	mond	k1gInSc5	mond
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
Malingue	Malingue	k1gFnSc1	Malingue
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
:	:	kIx,	:
El	Ela	k1gFnPc2	Ela
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
Matta	Matta	k1gMnSc1	Matta
en	en	k?	en
diálogo	diálogo	k1gMnSc1	diálogo
con	con	k?	con
Gonzalo	Gonzalo	k1gMnSc1	Gonzalo
Rojas	Rojas	k1gMnSc1	Rojas
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Congrè	Congrè	k1gFnSc2	Congrè
international	internationat	k5eAaPmAgInS	internationat
de	de	k?	de
la	la	k1gNnSc3	la
langue	langue	k1gFnSc2	langue
espagnole	espagnole	k1gFnSc2	espagnole
ve	v	k7c6	v
Valparaiso	Valparaisa	k1gFnSc5	Valparaisa
(	(	kIx(	(
<g/>
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
:	:	kIx,	:
Matta	Matt	k1gInSc2	Matt
<g/>
,	,	kIx,	,
centenario	centenario	k6eAd1	centenario
11.11	[number]	k4	11.11
<g/>
.11	.11	k4	.11
<g/>
,	,	kIx,	,
Retrospektiva	retrospektiva	k1gFnSc1	retrospektiva
<g/>
,	,	kIx,	,
Centro	Centro	k1gNnSc1	Centro
Cultural	Cultural	k1gMnSc1	Cultural
Palacio	Palacio	k1gMnSc1	Palacio
La	la	k1gNnSc4	la
Moneda	Moned	k1gMnSc2	Moned
<g/>
,	,	kIx,	,
Santiago	Santiago	k1gNnSc1	Santiago
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
:	:	kIx,	:
Matta	Matt	k1gInSc2	Matt
100	[number]	k4	100
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
National	National	k1gFnSc2	National
des	des	k1gNnSc7	des
Beaux	Beaux	k1gInSc1	Beaux
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
Santiago	Santiago	k1gNnSc1	Santiago
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
:	:	kIx,	:
Matta	Matta	k1gMnSc1	Matta
<g/>
,	,	kIx,	,
du	du	k?	du
surréalisme	surréalismus	k1gInSc5	surréalismus
à	à	k?	à
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Histoire	Histoir	k1gMnSc5	Histoir
<g/>
,	,	kIx,	,
Musée	Musée	k1gNnSc2	Musée
Cantini	Cantin	k2eAgMnPc1d1	Cantin
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnSc1	Marseille
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
:	:	kIx,	:
Matta	Matta	k1gMnSc1	Matta
|	|	kIx~	|
Człowiek	Człowiek	k1gMnSc1	Człowiek
i	i	k8xC	i
Wszechświat	Wszechświat	k2eAgMnSc1d1	Wszechświat
|	|	kIx~	|
Man	Man	k1gMnSc1	Man
and	and	k?	and
Universe	Universe	k1gFnSc1	Universe
|	|	kIx~	|
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gInSc5	Homm
et	et	k?	et
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Univers	universum	k1gNnPc2	universum
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
Krakov	Krakov	k1gInSc1	Krakov
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Roberto	Roberta	k1gFnSc5	Roberta
Matta	Matto	k1gNnPc1	Matto
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
