<s desamb="1">
Mezi	mezi	k7c4
nejpoužívanější	používaný	k2eAgFnPc4d3
patří	patřit	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
zavináč	zavináč	k1gInSc4
(	(	kIx(
<g/>
@	@	kIx~
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Alt	alt	kA
Gr	Gr	kA
+	+	kIx~
V	V	kA
<g/>
,	,	kIx,
resp.	resp.	kA
Ctrl	Ctrl	kA
+	+	kIx~
Alt	Alt	kA
+	+	kIx~
V	V	kA
(	(	kIx(
<g/>
u	u	k7c2
rozložení	rozložení	k1gNnSc2
kláves	klávesa	k1gFnPc2
CS	CS	kA
a	a	k8xC
SK	Sk	kA
QWERTZ	QWERTZ	kA
<g/>
,	,	kIx,
SK	Sk	kA
QWERTY	QWERTY	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
Alt	Alt	kA
Gr	Gr	k1gFnSc1
+	+	kIx~
ě	ě	k?
(	(	kIx(
<g/>
CS	CS	kA
QWERTY	QWERTY	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
Alt	Alt	kA
+	+	kIx~
64	#num#	k4
(	(	kIx(
<g/>
na	na	k7c6
numerické	numerický	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
<g/>
)	)	kIx)
</s>