<s>
Barbar	barbar	k1gMnSc1	barbar
Conan	Conan	k1gMnSc1	Conan
(	(	kIx(	(
<g/>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Barbarian	Barbarian	k1gMnSc1	Barbarian
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ve	v	k7c6	v
svých	svůj	k3xOyFgMnPc6	svůj
fantasy	fantas	k1gInPc4	fantas
povídkách	povídka	k1gFnPc6	povídka
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Robert	Robert	k1gMnSc1	Robert
E.	E.	kA	E.
Howard	Howard	k1gMnSc1	Howard
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Howardově	Howardův	k2eAgFnSc6d1	Howardova
smrti	smrt	k1gFnSc6	smrt
řada	řada	k1gFnSc1	řada
autorů	autor	k1gMnPc2	autor
Conanovy	Conanův	k2eAgInPc4d1	Conanův
příběhy	příběh	k1gInPc4	příběh
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Lyon	Lyon	k1gInSc1	Lyon
Sprague	Sprague	k1gInSc1	Sprague
de	de	k?	de
Camp	camp	k1gInSc1	camp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k8xC	i
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
s	s	k7c7	s
Arnoldem	Arnold	k1gMnSc7	Arnold
Schwarzeneggerem	Schwarzenegger	k1gMnSc7	Schwarzenegger
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
natočeno	natočen	k2eAgNnSc4d1	natočeno
nové	nový	k2eAgNnSc4d1	nové
zpracování	zpracování	k1gNnSc4	zpracování
ve	v	k7c4	v
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Jason	Jason	k1gMnSc1	Jason
Momoa	Momoa	k1gMnSc1	Momoa
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
Barbaru	barbar	k1gMnSc6	barbar
Conanovi	Conan	k1gMnSc6	Conan
začal	začít	k5eAaPmAgMnS	začít
překládat	překládat	k5eAaImF	překládat
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následoval	následovat	k5eAaImAgMnS	následovat
např.	např.	kA	např.
Stanislav	Stanislav	k1gMnSc1	Stanislav
Plášil	Plášil	k1gMnSc1	Plášil
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Mustang	mustang	k1gMnSc1	mustang
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastní	k2eAgNnSc7d1	vlastní
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Viking	Viking	k1gMnSc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
slona	slon	k1gMnSc2	slon
Bůh	bůh	k1gMnSc1	bůh
v	v	k7c6	v
míse	mísa	k1gFnSc6	mísa
Darebáci	darebák	k1gMnPc1	darebák
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Dcera	dcera	k1gFnSc1	dcera
pána	pán	k1gMnSc2	pán
mrazu	mráz	k1gInSc2	mráz
Královna	královna	k1gFnSc1	královna
černého	černý	k2eAgNnSc2d1	černé
pobřeží	pobřeží	k1gNnSc2	pobřeží
Údolí	údolí	k1gNnSc1	údolí
ztracených	ztracený	k2eAgFnPc2d1	ztracená
žen	žena	k1gFnPc2	žena
Černý	Černý	k1gMnSc1	Černý
kolos	kolos	k1gMnSc1	kolos
...	...	k?	...
a	a	k8xC	a
zrodí	zrodit	k5eAaPmIp3nS	zrodit
se	se	k3xPyFc4	se
čarodějka	čarodějka	k1gFnSc1	čarodějka
Zamboulské	Zamboulský	k2eAgFnSc2d1	Zamboulský
stíny	stín	k1gInPc7	stín
Ďábel	ďábel	k1gMnSc1	ďábel
v	v	k7c4	v
železo	železo	k1gNnSc4	železo
vtělený	vtělený	k2eAgInSc1d1	vtělený
Lidé	člověk	k1gMnPc1	člověk
černého	černý	k2eAgInSc2d1	černý
kruhu	kruh	k1gInSc2	kruh
Plíživý	plíživý	k2eAgInSc4d1	plíživý
stín	stín	k1gInSc4	stín
Jezírko	jezírko	k1gNnSc4	jezírko
toho	ten	k3xDgNnSc2	ten
černého	černé	k1gNnSc2	černé
Rudé	rudý	k2eAgInPc1d1	rudý
hřeby	hřeb	k1gInPc1	hřeb
Gwalhurův	Gwalhurův	k2eAgInSc1d1	Gwalhurův
poklad	poklad	k1gInSc4	poklad
Za	za	k7c7	za
černou	černý	k2eAgFnSc7d1	černá
řekou	řeka	k1gFnSc7	řeka
Meč	meč	k1gInSc4	meč
s	s	k7c7	s
fénixem	fénix	k1gMnSc7	fénix
Šarlatová	šarlatový	k2eAgFnSc1d1	šarlatová
citadela	citadela	k1gFnSc1	citadela
Hodina	hodina	k1gFnSc1	hodina
draka	drak	k1gMnSc4	drak
Bubny	Bubny	k1gInPc1	Bubny
Tombalku	Tombalek	k1gInSc6	Tombalek
Síň	síň	k1gFnSc1	síň
mrtvých	mrtvý	k1gMnPc2	mrtvý
Nergalova	Nergalův	k2eAgFnSc1d1	Nergalův
paže	paže	k1gFnSc1	paže
Rypák	rypák	k1gInSc1	rypák
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1955	[number]	k4	1955
oživil	oživit	k5eAaPmAgMnS	oživit
postavu	postava	k1gFnSc4	postava
Conana	Conan	k1gMnSc2	Conan
americký	americký	k2eAgInSc4d1	americký
spisovarel	spisovarel	k1gInSc4	spisovarel
Lyon	Lyon	k1gInSc1	Lyon
Sprague	Sprague	k1gFnSc1	Sprague
de	de	k?	de
Camp	camp	k1gInSc4	camp
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
a	a	k8xC	a
Björnem	Björno	k1gNnSc7	Björno
Nybergem	Nyberg	k1gMnSc7	Nyberg
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
obohatili	obohatit	k5eAaPmAgMnP	obohatit
o	o	k7c4	o
šest	šest	k4xCc4	šest
románů	román	k1gInPc2	román
a	a	k8xC	a
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Conan	Conan	k1gMnSc1	Conan
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
de	de	k?	de
Campem	camp	k1gInSc7	camp
dokončené	dokončený	k2eAgFnSc2d1	dokončená
Howardovy	Howardův	k2eAgFnSc2d1	Howardova
povídky	povídka	k1gFnSc2	povídka
The	The	k1gMnSc1	The
Blood-Stained	Blood-Stained	k1gMnSc1	Blood-Stained
God	God	k1gMnSc1	God
(	(	kIx(	(
<g/>
Krví	krvit	k5eAaImIp3nS	krvit
zbrocený	zbrocený	k2eAgMnSc1d1	zbrocený
bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hawks	Hawks	k1gInSc1	Hawks
over	over	k1gInSc1	over
Shem	Shem	k1gInSc1	Shem
(	(	kIx(	(
<g/>
Jestřábi	jestřáb	k1gMnPc1	jestřáb
nad	nad	k7c7	nad
Shemem	Shem	k1gInSc7	Shem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Road	Road	k1gMnSc1	Road
of	of	k?	of
the	the	k?	the
Eagles	Eagles	k1gInSc1	Eagles
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
orlů	orel	k1gMnPc2	orel
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Flame	Flam	k1gInSc5	Flam
Knife	Knife	k?	Knife
(	(	kIx(	(
<g/>
Plamenný	plamenný	k2eAgInSc1d1	plamenný
nůž	nůž	k1gInSc1	nůž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Adventurer	Adventurer	k1gMnSc1	Adventurer
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
tří	tři	k4xCgFnPc2	tři
původních	původní	k2eAgFnPc2d1	původní
Howardových	Howardův	k2eAgFnPc2d1	Howardova
povídek	povídka	k1gFnPc2	povídka
i	i	k8xC	i
jednu	jeden	k4xCgFnSc4	jeden
de	de	k?	de
Campem	camp	k1gInSc7	camp
dokončenou	dokončený	k2eAgFnSc7d1	dokončená
<g/>
:	:	kIx,	:
Drums	Drumsa	k1gFnPc2	Drumsa
of	of	k?	of
Tombalku	Tombalek	k1gInSc2	Tombalek
(	(	kIx(	(
<g/>
Bubny	Bubny	k1gInPc1	Bubny
Tombalku	Tombalek	k1gInSc2	Tombalek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
osm	osm	k4xCc4	osm
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
napsané	napsaný	k2eAgFnPc1d1	napsaná
společně	společně	k6eAd1	společně
de	de	k?	de
Campem	camp	k1gInSc7	camp
a	a	k8xC	a
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Thing	Thing	k1gInSc1	Thing
in	in	k?	in
the	the	k?	the
Crypt	Crypt	k1gInSc1	Crypt
(	(	kIx(	(
<g/>
Věc	věc	k1gFnSc1	věc
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gMnSc1	The
City	City	k1gFnSc2	City
of	of	k?	of
Skulls	Skulls	k1gInSc1	Skulls
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
lebek	lebka	k1gFnPc2	lebka
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedbu	jedbat	k5eAaPmIp1nS	jedbat
de	de	k?	de
Campem	camp	k1gInSc7	camp
dokončenou	dokončený	k2eAgFnSc7d1	dokončená
The	The	k1gFnSc7	The
Hall	Halla	k1gFnPc2	Halla
of	of	k?	of
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
(	(	kIx(	(
<g/>
Síň	síň	k1gFnSc1	síň
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Usurper	Usurper	k1gMnSc1	Usurper
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
dvou	dva	k4xCgFnPc2	dva
původních	původní	k2eAgFnPc2d1	původní
Howardových	Howardův	k2eAgFnPc2d1	Howardova
povídek	povídka	k1gFnPc2	povídka
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
de	de	k?	de
Campem	camp	k1gInSc7	camp
dokončené	dokončený	k2eAgInPc4d1	dokončený
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Treasure	Treasur	k1gMnSc5	Treasur
of	of	k?	of
Tranicos	Tranicos	k1gInSc1	Tranicos
(	(	kIx(	(
<g/>
Tranicův	Tranicův	k2eAgInSc1d1	Tranicův
poklad	poklad	k1gInSc1	poklad
<g/>
)	)	kIx)	)
a	a	k8xC	a
Wolves	Wolves	k1gMnSc1	Wolves
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
Border	Border	k1gInSc1	Border
(	(	kIx(	(
<g/>
Vlci	vlk	k1gMnPc1	vlk
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Wanderer	Wanderer	k1gMnSc1	Wanderer
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgFnPc4	čtyři
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednu	jeden	k4xCgFnSc4	jeden
novou	nový	k2eAgFnSc4d1	nová
napsanou	napsaný	k2eAgFnSc4d1	napsaná
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
<g/>
:	:	kIx,	:
Black	Black	k1gInSc1	Black
Tears	Tearsa	k1gFnPc2	Tearsa
(	(	kIx(	(
<g/>
Černé	Černé	k2eAgFnSc2d1	Černé
slzy	slza	k1gFnSc2	slza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Freebooter	Freebooter	k1gMnSc1	Freebooter
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
reedice	reedice	k1gFnSc1	reedice
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
of	of	k?	of
Cimmeria	Cimmerium	k1gNnSc2	Cimmerium
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Conan	Conan	k1gInSc1	Conan
z	z	k7c2	z
Cimmerie	Cimmerie	k1gFnSc2	Cimmerie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
osm	osm	k4xCc4	osm
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Curse	Curse	k1gFnSc2	Curse
of	of	k?	of
Monolith	Monolith	k1gInSc1	Monolith
(	(	kIx(	(
<g/>
Kletba	kletba	k1gFnSc1	kletba
monolitu	monolit	k1gInSc2	monolit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Lair	Lair	k1gMnSc1	Lair
of	of	k?	of
the	the	k?	the
Ice	Ice	k1gMnSc1	Ice
Worm	Worm	k1gMnSc1	Worm
(	(	kIx(	(
<g/>
Doupě	doupě	k1gNnSc1	doupě
ledového	ledový	k2eAgMnSc2d1	ledový
červa	červ	k1gMnSc2	červ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Castle	Castle	k1gFnSc2	Castle
of	of	k?	of
Terror	Terror	k1gInSc1	Terror
(	(	kIx(	(
<g/>
Zámek	zámek	k1gInSc1	zámek
hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedbu	jedbat	k5eAaPmIp1nS	jedbat
de	de	k?	de
Campem	camp	k1gInSc7	camp
dokončenou	dokončený	k2eAgFnSc7d1	dokončená
The	The	k1gFnSc7	The
Snout	snout	k5eAaImF	snout
in	in	k?	in
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
(	(	kIx(	(
<g/>
Rypák	rypák	k1gInSc1	rypák
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
of	of	k?	of
Aquilonia	Aquilonium	k1gNnSc2	Aquilonium
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Conan	Conan	k1gInSc1	Conan
z	z	k7c2	z
Aquilonie	Aquilonie	k1gFnSc2	Aquilonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgFnPc4	čtyři
povídky	povídka	k1gFnPc4	povídka
napsané	napsaný	k2eAgInPc1d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
The	The	k1gMnSc1	The
Witch	Witch	k1gMnSc1	Witch
of	of	k?	of
the	the	k?	the
Mists	Mists	k1gInSc1	Mists
(	(	kIx(	(
<g/>
Mlžná	mlžný	k2eAgFnSc1d1	mlžná
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
Sphinx	Sphinx	k1gInSc1	Sphinx
of	of	k?	of
Nebthu	Nebth	k1gInSc2	Nebth
(	(	kIx(	(
<g/>
Černá	černý	k2eAgFnSc1d1	černá
sfinga	sfinga	k1gFnSc1	sfinga
z	z	k7c2	z
Nebthu	Nebth	k1gInSc2	Nebth
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
Moon	Moon	k1gMnSc1	Moon
of	of	k?	of
Zembabwei	Zembabwe	k1gFnSc2	Zembabwe
(	(	kIx(	(
<g/>
Rudý	rudý	k2eAgInSc4d1	rudý
měsíc	měsíc	k1gInSc4	měsíc
Zembabwei	Zembabwe	k1gFnSc2	Zembabwe
<g/>
)	)	kIx)	)
a	a	k8xC	a
Shadows	Shadows	k1gInSc1	Shadows
in	in	k?	in
the	the	k?	the
Skull	Skull	k1gInSc1	Skull
(	(	kIx(	(
<g/>
stíny	stín	k1gInPc1	stín
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Swordsman	Swordsman	k1gMnSc1	Swordsman
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídkyThe	povídkyThe	k6eAd1	povídkyThe
People	People	k1gFnSc1	People
of	of	k?	of
the	the	k?	the
Summit	summit	k1gInSc1	summit
(	(	kIx(	(
<g/>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
vrcholku	vrcholek	k1gInSc2	vrcholek
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gMnSc1	The
Star	Star	kA	Star
of	of	k?	of
Khorala	Khoral	k1gMnSc2	Khoral
(	(	kIx(	(
<g/>
Khoralská	Khoralský	k2eAgFnSc1d1	Khoralský
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
napsané	napsaný	k2eAgInPc1d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Björnem	Björn	k1gInSc7	Björn
Nybergem	Nyberg	k1gInSc7	Nyberg
a	a	k8xC	a
Legions	Legions	k1gInSc1	Legions
of	of	k?	of
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
(	(	kIx(	(
<g/>
Legie	legie	k1gFnSc1	legie
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shadows	Shadows	k1gInSc1	Shadows
in	in	k?	in
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
(	(	kIx(	(
<g/>
Stíny	stín	k1gInPc1	stín
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Gem	gema	k1gFnPc2	gema
in	in	k?	in
the	the	k?	the
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
Drahokam	drahokam	k1gInSc1	drahokam
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Ivory	Ivora	k1gFnSc2	Ivora
Goddess	Goddess	k1gInSc1	Goddess
(	(	kIx(	(
<g/>
Bohyně	bohyně	k1gFnSc1	bohyně
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
)	)	kIx)	)
a	a	k8xC	a
Moon	Moon	k1gInSc1	Moon
of	of	k?	of
Blood	Blood	k1gInSc4	Blood
(	(	kIx(	(
<g/>
Krvavý	krvavý	k2eAgInSc4d1	krvavý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
napsané	napsaný	k2eAgFnPc1d1	napsaná
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
The	The	k1gMnSc1	The
Conan	Conan	k1gMnSc1	Conan
Chronicles	Chronicles	k1gMnSc1	Chronicles
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
tři	tři	k4xCgFnPc4	tři
dříve	dříve	k6eAd2	dříve
vydané	vydaný	k2eAgFnPc4d1	vydaná
povídkové	povídkový	k2eAgFnPc4d1	povídková
sbírky	sbírka	k1gFnPc4	sbírka
Conan	Conany	k1gInPc2	Conany
<g/>
,	,	kIx,	,
Conan	Conan	k1gInSc1	Conan
of	of	k?	of
Cimmeria	Cimmerium	k1gNnSc2	Cimmerium
a	a	k8xC	a
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Freebooter	Freebooter	k1gMnSc1	Freebooter
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Conan	Conan	k1gInSc1	Conan
Chronicles	Chronicles	k1gInSc1	Chronicles
2	[number]	k4	2
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgFnPc4	dva
dříve	dříve	k6eAd2	dříve
vydaní	vydaný	k2eAgMnPc1d1	vydaný
povídkové	povídkový	k2eAgMnPc4d1	povídkový
sbírky	sbírka	k1gFnSc2	sbírka
Conan	Conana	k1gFnPc2	Conana
the	the	k?	the
Adventurer	Adventurer	k1gMnSc1	Adventurer
a	a	k8xC	a
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Wanderer	Wanderer	k1gInSc1	Wanderer
a	a	k8xC	a
román	román	k1gInSc1	román
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Buccaneer	Buccaneer	k1gMnSc1	Buccaneer
<g/>
.	.	kIx.	.
</s>
<s>
Sagas	Sagas	k1gMnSc1	Sagas
of	of	k?	of
Conan	Conan	k1gMnSc1	Conan
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sbírku	sbírka	k1gFnSc4	sbírka
povídek	povídka	k1gFnPc2	povídka
Conan	Conana	k1gFnPc2	Conana
the	the	k?	the
Swordsman	Swordsman	k1gMnSc1	Swordsman
a	a	k8xC	a
romány	román	k1gInPc1	román
Conan	Conana	k1gFnPc2	Conana
the	the	k?	the
Liberator	Liberator	k1gMnSc1	Liberator
<g/>
'	'	kIx"	'
a	a	k8xC	a
Conan	Conan	k1gInSc1	Conan
and	and	k?	and
the	the	k?	the
Spider	Spider	k1gMnSc1	Spider
God	God	k1gMnSc1	God
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Return	Return	k1gMnSc1	Return
of	of	k?	of
Conan	Conan	k1gMnSc1	Conan
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Conanův	Conanův	k2eAgInSc1d1	Conanův
návrat	návrat	k1gInSc1	návrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Björnem	Björno	k1gNnSc7	Björno
Nybergem	Nyberg	k1gMnSc7	Nyberg
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
of	of	k?	of
the	the	k?	the
Isles	Isles	k1gInSc1	Isles
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Conan	Conan	k1gInSc1	Conan
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Buccaneer	Buccaneer	k1gMnSc1	Buccaneer
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Conan	Conan	k1gMnSc1	Conan
bukanýr	bukanýr	k1gMnSc1	bukanýr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Liberator	Liberator	k1gMnSc1	Liberator
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Conan	Conan	k1gMnSc1	Conan
osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
and	and	k?	and
the	the	k?	the
Spider	Spider	k1gMnSc1	Spider
God	God	k1gMnSc1	God
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Conan	Conan	k1gMnSc1	Conan
a	a	k8xC	a
Pavoučí	pavoučí	k2eAgMnSc1d1	pavoučí
bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
napsaný	napsaný	k2eAgInSc4d1	napsaný
román	román	k1gInSc4	román
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Barbarian	Barbarian	k1gMnSc1	Barbarian
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Barbar	barbar	k1gMnSc1	barbar
Conan	Conan	k1gMnSc1	Conan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Linem	Linem	k?	Linem
Carterem	Carter	k1gInSc7	Carter
<g/>
,	,	kIx,	,
románový	románový	k2eAgInSc4d1	románový
přepis	přepis	k1gInSc4	přepis
filmu	film	k1gInSc2	film
Barbar	barbar	k1gMnSc1	barbar
Conan	Conan	k1gMnSc1	Conan
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
<g/>
,	,	kIx,	,
SFK	SFK	kA	SFK
Winston	Winston	k1gInSc1	Winston
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorovy	autorův	k2eAgInPc4d1	autorův
nebo	nebo	k8xC	nebo
autorem	autor	k1gMnSc7	autor
dokončené	dokončený	k2eAgFnSc2d1	dokončená
povídky	povídka	k1gFnSc2	povídka
Legie	legie	k1gFnSc2	legie
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
Věc	věc	k1gFnSc1	věc
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
<g/>
,	,	kIx,	,
Síň	síň	k1gFnSc1	síň
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
vrcholku	vrcholek	k1gInSc2	vrcholek
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
lebek	lebka	k1gFnPc2	lebka
<g/>
,	,	kIx,	,
Kletba	kletba	k1gFnSc1	kletba
monolitu	monolit	k1gInSc2	monolit
<g/>
,	,	kIx,	,
Krví	krvit	k5eAaImIp3nS	krvit
zbrocený	zbrocený	k2eAgMnSc1d1	zbrocený
bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
Doupě	doupě	k1gNnSc1	doupě
ledového	ledový	k2eAgMnSc2d1	ledový
červa	červ	k1gMnSc2	červ
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
a	a	k8xC	a
Pavoučí	pavoučí	k2eAgMnSc1d1	pavoučí
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc1	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
mstitel	mstitel	k1gMnSc1	mstitel
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc1	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc1	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
.	.	kIx.	.
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorovy	autorův	k2eAgFnPc4d1	autorova
povídky	povídka	k1gFnPc4	povídka
Legie	legie	k1gFnSc1	legie
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
vrcholku	vrcholek	k1gInSc2	vrcholek
<g/>
,	,	kIx,	,
Stíny	stín	k1gInPc1	stín
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
<g/>
,	,	kIx,	,
Khoralská	Khoralský	k2eAgFnSc1d1	Khoralský
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
Drahokam	drahokam	k1gInSc1	drahokam
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
Bohyně	bohyně	k1gFnSc1	bohyně
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
Krvavý	krvavý	k2eAgInSc4d1	krvavý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
<g/>
:	:	kIx,	:
Vlci	vlk	k1gMnPc1	vlk
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc1	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorovy	autorův	k2eAgFnSc2d1	autorova
povídky	povídka	k1gFnSc2	povídka
Město	město	k1gNnSc1	město
lebek	lebka	k1gFnPc2	lebka
<g/>
,	,	kIx,	,
Tranicův	Tranicův	k2eAgInSc1d1	Tranicův
poklad	poklad	k1gInSc1	poklad
a	a	k8xC	a
Vlci	vlk	k1gMnPc1	vlk
za	za	k7c4	za
hranicí-	hranicí-	k?	hranicí-
Conan	Conan	k1gMnSc1	Conan
bukanýr	bukanýr	k1gMnSc1	bukanýr
<g/>
,	,	kIx,	,
Mustang	mustang	k1gMnSc1	mustang
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Plášil	Plášil	k1gMnSc1	Plášil
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
z	z	k7c2	z
Aquilonie	Aquilonie	k1gFnSc2	Aquilonie
<g/>
,	,	kIx,	,
,	,	kIx,	,
United	United	k1gInSc4	United
Fans	Fansa	k1gFnPc2	Fansa
a	a	k8xC	a
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc2	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorovy	autorův	k2eAgFnPc4d1	autorova
povídky	povídka	k1gFnPc4	povídka
Mlžná	mlžný	k2eAgFnSc1d1	mlžná
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
sfinga	sfinga	k1gFnSc1	sfinga
z	z	k7c2	z
Nebthu	Nebth	k1gInSc2	Nebth
<g/>
,	,	kIx,	,
Rudý	rudý	k2eAgInSc4d1	rudý
měsíc	měsíc	k1gInSc4	měsíc
Zembabwei	Zembabwe	k1gFnPc4	Zembabwe
a	a	k8xC	a
Stíny	stín	k1gInPc4	stín
v	v	k7c6	v
lebce-	lebce-	k?	lebce-
Conan	Conan	k1gMnSc1	Conan
osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
Fans	Fans	k1gInSc1	Fans
a	a	k8xC	a
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc2	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
a	a	k8xC	a
kletba	kletba	k1gFnSc1	kletba
monolitu	monolit	k1gInSc2	monolit
<g/>
,	,	kIx,	,
United	United	k1gInSc1	United
fans	fans	k1gInSc1	fans
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorovy	autorův	k2eAgFnSc2d1	autorova
povídky	povídka	k1gFnSc2	povídka
Věc	věc	k1gFnSc1	věc
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
<g/>
,	,	kIx,	,
Síň	síň	k1gFnSc1	síň
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
Kletba	kletba	k1gFnSc1	kletba
monolitu	monolit	k1gInSc2	monolit
<g/>
.	.	kIx.	.
</s>
<s>
Krví	krvit	k5eAaImIp3nS	krvit
zbrocený	zbrocený	k2eAgMnSc1d1	zbrocený
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Doupě	doupě	k1gNnSc1	doupě
ledového	ledový	k2eAgMnSc2d1	ledový
červa	červ	k1gMnSc2	červ
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
hrůzy	hrůza	k1gFnSc2	hrůza
a	a	k8xC	a
Rypák	rypák	k1gInSc1	rypák
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
Deus	Deusa	k1gFnPc2	Deusa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Bartoň	Bartoň	k1gMnSc1	Bartoň
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gInSc1	Conan
a	a	k8xC	a
plamenný	plamenný	k2eAgInSc1d1	plamenný
nůž	nůž	k1gInSc1	nůž
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc1	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorovy	autorův	k2eAgFnPc4d1	autorova
povídky	povídka	k1gFnPc4	povídka
Jestřábi	jestřáb	k1gMnPc1	jestřáb
nad	nad	k7c7	nad
Shemem	Shem	k1gInSc7	Shem
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
orlů	orel	k1gMnPc2	orel
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgFnPc1d1	Černé
slzy	slza	k1gFnPc1	slza
<g/>
,	,	kIx,	,
Plamenný	plamenný	k2eAgInSc1d1	plamenný
nůž	nůž	k1gInSc1	nůž
<g/>
,	,	kIx,	,
Bubny	Bubny	k1gInPc1	Bubny
Tombalku	Tombalek	k1gInSc2	Tombalek
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
a	a	k8xC	a
Tarantijský	Tarantijský	k2eAgMnSc1d1	Tarantijský
tigr	tigr	k1gMnSc1	tigr
(	(	kIx(	(
<g/>
Leonard	Leonard	k1gMnSc1	Leonard
Medek	Medek	k1gMnSc1	Medek
<g/>
)	)	kIx)	)
Conan	Conan	k1gMnSc1	Conan
a	a	k8xC	a
krokodýlí	krokodýlí	k2eAgMnSc1d1	krokodýlí
bůh	bůh	k1gMnSc1	bůh
(	(	kIx(	(
<g/>
Christopher	Christophra	k1gFnPc2	Christophra
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
)	)	kIx)	)
Conan	Conan	k1gInSc1	Conan
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
mořských	mořský	k2eAgMnPc2d1	mořský
ďáblů	ďábel	k1gMnPc2	ďábel
(	(	kIx(	(
<g/>
Paul	Paul	k1gMnSc1	Paul
O.	O.	kA	O.
Courtier	Courtier	k1gMnSc1	Courtier
<g/>
)	)	kIx)	)
Conan	Conan	k1gMnSc1	Conan
a	a	k8xC	a
dvanáct	dvanáct	k4xCc4	dvanáct
bran	brána	k1gFnPc2	brána
pekla	peklo	k1gNnSc2	peklo
(	(	kIx(	(
<g/>
Thorleiff	Thorleiff	k1gInSc1	Thorleiff
Larssen	Larssen	k1gInSc1	Larssen
<g/>
)	)	kIx)	)
Conan	Conan	k1gMnSc1	Conan
gladiátor	gladiátor	k1gMnSc1	gladiátor
(	(	kIx(	(
<g/>
Leonard	Leonard	k1gMnSc1	Leonard
Carpenter	Carpenter	k1gMnSc1	Carpenter
<g/>
)	)	kIx)	)
</s>
