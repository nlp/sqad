<s>
Soběslav	Soběslav	k1gFnSc1	Soběslav
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Sobieslau	Sobieslaus	k1gInSc3	Sobieslaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historické	historický	k2eAgNnSc4d1	historické
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Tábor	Tábor	k1gInSc1	Tábor
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Tábora	Tábor	k1gInSc2	Tábor
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Lužnice	Lužnice	k1gFnSc2	Lužnice
a	a	k8xC	a
Černovického	Černovický	k2eAgInSc2d1	Černovický
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obtéká	obtékat	k5eAaImIp3nS	obtékat
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
centrem	centrum	k1gNnSc7	centrum
pro	pro	k7c4	pro
okolní	okolní	k2eAgInSc4d1	okolní
region	region	k1gInSc4	region
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
dálnici	dálnice	k1gFnSc4	dálnice
D3	D3	k1gFnSc4	D3
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
silnici	silnice	k1gFnSc4	silnice
E55	E55	k1gFnSc2	E55
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
a	a	k8xC	a
Lince	Linec	k1gInSc2	Linec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Soběslav	Soběslav	k1gFnSc1	Soběslav
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
historický	historický	k2eAgInSc4d1	historický
půdorys	půdorys	k1gInSc4	půdorys
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
středověkých	středověký	k2eAgMnPc2d1	středověký
valů	val	k1gInPc2	val
a	a	k8xC	a
staveb	stavba	k1gFnPc2	stavba
i	i	k9	i
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
architektonických	architektonický	k2eAgInPc2d1	architektonický
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
zástavby	zástavba	k1gFnSc2	zástavba
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
a	a	k8xC	a
z	z	k7c2	z
blízkého	blízký	k2eAgNnSc2d1	blízké
hradiště	hradiště	k1gNnSc2	hradiště
Svákov	Svákov	k1gInSc1	Svákov
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
blízké	blízký	k2eAgNnSc1d1	blízké
okolí	okolí	k1gNnSc1	okolí
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
již	již	k9	již
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1293	[number]	k4	1293
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
okolní	okolní	k2eAgFnSc1d1	okolní
obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
rodu	rod	k1gInSc2	rod
Rožmberků	Rožmberk	k1gInPc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
práva	práv	k2eAgFnSc1d1	práva
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Soběslav	Soběslav	k1gFnSc1	Soběslav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1390	[number]	k4	1390
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
hradě	hrad	k1gInSc6	hrad
vězněn	vězněn	k2eAgMnSc1d1	vězněn
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1421	[number]	k4	1421
a	a	k8xC	a
1435	[number]	k4	1435
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Soběslav	Soběslav	k1gFnSc1	Soběslav
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
měst	město	k1gNnPc2	město
rožmberského	rožmberský	k2eAgNnSc2d1	rožmberské
panství	panství	k1gNnSc2	panství
a	a	k8xC	a
představovala	představovat	k5eAaImAgFnS	představovat
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
centrum	centrum	k1gNnSc4	centrum
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c4	na
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Petra	Petr	k1gMnSc2	Petr
Voka	Vokus	k1gMnSc2	Vokus
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
získávají	získávat	k5eAaImIp3nP	získávat
město	město	k1gNnSc4	město
Švamberkové	Švamberková	k1gFnSc2	Švamberková
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
znovu	znovu	k6eAd1	znovu
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
i	i	k9	i
za	za	k7c2	za
válek	válka	k1gFnPc2	válka
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
vedoucí	vedoucí	k1gFnSc2	vedoucí
přes	přes	k7c4	přes
Soběslav	Soběslav	k1gFnSc4	Soběslav
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
úsek	úsek	k1gInSc1	úsek
Dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
KFJB	KFJB	kA	KFJB
<g/>
)	)	kIx)	)
z	z	k7c2	z
Českých	český	k2eAgFnPc2d1	Česká
Velenic	Velenice	k1gFnPc2	Velenice
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
měla	mít	k5eAaImAgFnS	mít
Soběslav	Soběslav	k1gFnSc1	Soběslav
442	[number]	k4	442
domů	dům	k1gInPc2	dům
a	a	k8xC	a
3800	[number]	k4	3800
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zde	zde	k6eAd1	zde
tkalcovny	tkalcovna	k1gFnSc2	tkalcovna
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
navzdory	navzdory	k7c3	navzdory
vážnému	vážný	k2eAgNnSc3d1	vážné
narušení	narušení	k1gNnSc3	narušení
severní	severní	k2eAgFnSc2d1	severní
strany	strana	k1gFnSc2	strana
náměstí	náměstí	k1gNnSc2	náměstí
novou	nový	k2eAgFnSc7d1	nová
zástavbou	zástavba	k1gFnSc7	zástavba
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
Smrčkovým	Smrčkův	k2eAgInSc7d1	Smrčkův
domem	dům	k1gInSc7	dům
a	a	k8xC	a
obchodním	obchodní	k2eAgInSc7d1	obchodní
domem	dům	k1gInSc7	dům
<g/>
)	)	kIx)	)
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
starosty	starosta	k1gMnSc2	starosta
města	město	k1gNnSc2	město
Jindřich	Jindřich	k1gMnSc1	Jindřich
Bláha	Bláha	k1gMnSc1	Bláha
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
zástupce	zástupce	k1gMnSc1	zástupce
Vladimír	Vladimír	k1gMnSc1	Vladimír
Drachovský	Drachovský	k2eAgMnSc1d1	Drachovský
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Soběslavi	Soběslav	k1gFnSc6	Soběslav
<g/>
.	.	kIx.	.
</s>
<s>
Děkanský	děkanský	k2eAgInSc1d1	děkanský
kostel	kostel	k1gInSc1	kostel
svatých	svatý	k1gMnPc2	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
kolem	kolem	k7c2	kolem
1280	[number]	k4	1280
<g/>
,	,	kIx,	,
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
a	a	k8xC	a
zaklenut	zaklenout	k5eAaPmNgMnS	zaklenout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1486	[number]	k4	1486
<g/>
-	-	kIx~	-
<g/>
1501	[number]	k4	1501
<g/>
.	.	kIx.	.
</s>
<s>
Dvojlodní	dvojlodní	k2eAgFnSc1d1	dvojlodní
síňová	síňový	k2eAgFnSc1d1	síňová
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
sklípkovou	sklípkový	k2eAgFnSc7d1	sklípková
klenbou	klenba	k1gFnSc7	klenba
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
štíhlé	štíhlý	k2eAgInPc4d1	štíhlý
osmiboké	osmiboký	k2eAgInPc4d1	osmiboký
sloupy	sloup	k1gInPc4	sloup
a	a	k8xC	a
s	s	k7c7	s
barokními	barokní	k2eAgFnPc7d1	barokní
bočními	boční	k2eAgFnPc7d1	boční
kaplemi	kaple	k1gFnPc7	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
od	od	k7c2	od
B.	B.	kA	B.
Kamarýta	Kamarýt	k1gInSc2	Kamarýt
<g/>
,	,	kIx,	,
na	na	k7c6	na
bočním	boční	k2eAgInSc6d1	boční
oltáři	oltář	k1gInSc6	oltář
socha	socha	k1gFnSc1	socha
Madony	Madona	k1gFnSc2	Madona
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnPc1d1	barokní
chórové	chórový	k2eAgFnPc1d1	chórová
lavice	lavice	k1gFnPc1	lavice
a	a	k8xC	a
cínová	cínový	k2eAgFnSc1d1	cínová
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1501	[number]	k4	1501
<g/>
.	.	kIx.	.
68	[number]	k4	68
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
městská	městský	k2eAgFnSc1d1	městská
kostelní	kostelní	k2eAgFnSc1d1	kostelní
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1487	[number]	k4	1487
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
symbolem	symbol	k1gInSc7	symbol
Soběslavi	Soběslav	k1gFnSc2	Soběslav
<g/>
.	.	kIx.	.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
kolem	kolem	k7c2	kolem
1380	[number]	k4	1380
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
síňové	síňový	k2eAgNnSc1d1	síňové
dvojlodí	dvojlodí	k1gNnSc1	dvojlodí
<g/>
,	,	kIx,	,
sklenuté	sklenutý	k2eAgNnSc1d1	sklenuté
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
štíhlé	štíhlý	k2eAgInPc4d1	štíhlý
sloupy	sloup	k1gInPc4	sloup
<g/>
,	,	kIx,	,
s	s	k7c7	s
pětiboce	pětiboce	k6eAd1	pětiboce
zakončeným	zakončený	k2eAgInSc7d1	zakončený
presbytářem	presbytář	k1gInSc7	presbytář
s	s	k7c7	s
vnějšími	vnější	k2eAgInPc7d1	vnější
opěráky	opěrák	k1gInPc7	opěrák
a	a	k8xC	a
štíhlou	štíhlý	k2eAgFnSc7d1	štíhlá
vížkou	vížka	k1gFnSc7	vížka
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
roku	rok	k1gInSc2	rok
1712	[number]	k4	1712
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
obnoven	obnovit	k5eAaPmNgInS	obnovit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Rožmberský	rožmberský	k2eAgInSc4d1	rožmberský
hrad	hrad	k1gInSc4	hrad
založený	založený	k2eAgInSc4d1	založený
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
kolem	kolem	k6eAd1	kolem
1390	[number]	k4	1390
Jindřichem	Jindřich	k1gMnSc7	Jindřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
představoval	představovat	k5eAaImAgMnS	představovat
důležité	důležitý	k2eAgNnSc4d1	důležité
obranné	obranný	k2eAgNnSc4d1	obranné
místo	místo	k1gNnSc4	místo
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
gotická	gotický	k2eAgFnSc1d1	gotická
věž	věž	k1gFnSc1	věž
Hláska	hláska	k1gFnSc1	hláska
se	s	k7c7	s
sklepením	sklepení	k1gNnSc7	sklepení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1394	[number]	k4	1394
vězněn	věznit	k5eAaImNgMnS	věznit
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
v	v	k7c6	v
závěti	závěť	k1gFnSc6	závěť
odkázal	odkázat	k5eAaPmAgInS	odkázat
městu	město	k1gNnSc3	město
Rožmberskou	rožmberský	k2eAgFnSc4d1	Rožmberská
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
hradu	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemž	což	k3yQnSc6	což
ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
husitských	husitský	k2eAgFnPc6d1	husitská
válkách	válka	k1gFnPc6	válka
hrad	hrad	k1gInSc4	hrad
zpustl	zpustnout	k5eAaPmAgInS	zpustnout
<g/>
,	,	kIx,	,
1706	[number]	k4	1706
byl	být	k5eAaImAgMnS	být
proměněn	proměnit	k5eAaPmNgMnS	proměnit
na	na	k7c4	na
pivovar	pivovar	k1gInSc4	pivovar
a	a	k8xC	a
pak	pak	k6eAd1	pak
sýpku	sýpka	k1gFnSc4	sýpka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
hradu	hrad	k1gInSc2	hrad
využíváno	využíván	k2eAgNnSc1d1	využíváno
jako	jako	k8xC	jako
městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
s	s	k7c7	s
několika	několik	k4yIc7	několik
sály	sál	k1gInPc7	sál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
gotickém	gotický	k2eAgInSc6d1	gotický
paláci	palác	k1gInSc6	palác
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgFnSc1d1	unikátní
a	a	k8xC	a
moderní	moderní	k2eAgFnSc1d1	moderní
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
samostatném	samostatný	k2eAgInSc6d1	samostatný
<g/>
,	,	kIx,	,
železo-skleněném	železokleněný	k2eAgInSc6d1	železo-skleněný
objektu	objekt	k1gInSc6	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Sen	sen	k1gInSc1	sen
Petra	Petr	k1gMnSc2	Petr
Voka	Vokus	k1gMnSc2	Vokus
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
naplněn	naplnit	k5eAaPmNgInS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Smrčkův	Smrčkův	k2eAgInSc1d1	Smrčkův
dům	dům	k1gInSc1	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
č.	č.	k?	č.
107	[number]	k4	107
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1565	[number]	k4	1565
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
obloučkovými	obloučkový	k2eAgInPc7d1	obloučkový
štíty	štít	k1gInPc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěno	umístěn	k2eAgNnSc1d1	umístěno
národopisné	národopisný	k2eAgNnSc1d1	Národopisné
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
Soběslavských	soběslavský	k2eAgNnPc2d1	Soběslavské
blat	blata	k1gNnPc2	blata
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
Petra	Petr	k1gMnSc2	Petr
Voka	Vokus	k1gMnSc2	Vokus
(	(	kIx(	(
<g/>
Rožmberský	rožmberský	k2eAgInSc1d1	rožmberský
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc1d1	renesanční
patrová	patrový	k2eAgFnSc1d1	patrová
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
barokně	barokně	k6eAd1	barokně
upravený	upravený	k2eAgInSc1d1	upravený
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
přírodopisné	přírodopisný	k2eAgNnSc1d1	přírodopisné
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
expozice	expozice	k1gFnSc1	expozice
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Rožmberkům	Rožmberk	k1gMnPc3	Rožmberk
a	a	k8xC	a
výstavní	výstavní	k2eAgFnPc1d1	výstavní
prostory	prostora	k1gFnPc1	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgInSc1d1	bývalý
hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
<g/>
,	,	kIx,	,
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
šindelovou	šindelový	k2eAgFnSc7d1	Šindelová
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
soběslavským	soběslavský	k2eAgMnSc7d1	soběslavský
primátorem	primátor	k1gMnSc7	primátor
Zachariášem	Zachariáš	k1gMnSc7	Zachariáš
Markem	Marek	k1gMnSc7	Marek
Markovským	Markovský	k2eAgMnSc7d1	Markovský
<g/>
.	.	kIx.	.
</s>
<s>
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc1d1	barokní
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
několik	několik	k4yIc4	několik
kamenných	kamenný	k2eAgInPc2d1	kamenný
náhrobků	náhrobek	k1gInPc2	náhrobek
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
malované	malovaný	k2eAgInPc4d1	malovaný
epitafy	epitaf	k1gInPc4	epitaf
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
soběslavská	soběslavský	k2eAgFnSc1d1	Soběslavská
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
svatební	svatební	k2eAgFnSc1d1	svatební
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
z	z	k7c2	z
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
barokními	barokní	k2eAgInPc7d1	barokní
štíty	štít	k1gInPc7	štít
<g/>
,	,	kIx,	,
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Společenské	společenský	k2eAgNnSc1d1	společenské
centrum	centrum	k1gNnSc1	centrum
Soběslavska	Soběslavsko	k1gNnSc2	Soběslavsko
-	-	kIx~	-
zrekonstruovaný	zrekonstruovaný	k2eAgInSc1d1	zrekonstruovaný
multifunkční	multifunkční	k2eAgInSc1d1	multifunkční
prostor	prostor	k1gInSc1	prostor
využívaný	využívaný	k2eAgMnSc1d1	využívaný
jako	jako	k8xC	jako
kino	kino	k1gNnSc1	kino
a	a	k8xC	a
kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
digitální	digitální	k2eAgMnSc1d1	digitální
kino	kino	k1gNnSc4	kino
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Tábor	Tábor	k1gInSc1	Tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
s	s	k7c7	s
celoročním	celoroční	k2eAgInSc7d1	celoroční
provozem	provoz	k1gInSc7	provoz
<g/>
,	,	kIx,	,
tenisová	tenisový	k2eAgFnSc1d1	tenisová
hala	hala	k1gFnSc1	hala
<g/>
,	,	kIx,	,
posilovna	posilovna	k1gFnSc1	posilovna
a	a	k8xC	a
multifunkční	multifunkční	k2eAgFnSc1d1	multifunkční
sportovní	sportovní	k2eAgFnSc1d1	sportovní
plocha	plocha	k1gFnSc1	plocha
Městské	městský	k2eAgNnSc1d1	Městské
koupaliště	koupaliště	k1gNnSc1	koupaliště
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
venkovní	venkovní	k2eAgInSc1d1	venkovní
plavecký	plavecký	k2eAgInSc1d1	plavecký
bazém	bazý	k1gMnSc6	bazý
s	s	k7c7	s
brouzdalištěm	brouzdaliště	k1gNnSc7	brouzdaliště
<g/>
,	,	kIx,	,
tobogány	tobogán	k1gInPc7	tobogán
a	a	k8xC	a
divokou	divoký	k2eAgFnSc7d1	divoká
řekou	řeka	k1gFnSc7	řeka
Nový	nový	k2eAgInSc4d1	nový
rybník	rybník	k1gInSc4	rybník
-	-	kIx~	-
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
altánek	altánek	k1gInSc1	altánek
nebo	nebo	k8xC	nebo
odpočinkové	odpočinkový	k2eAgNnSc1d1	odpočinkové
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
Mochomůrka	mochomůrka	k1gFnSc1	mochomůrka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
betonová	betonový	k2eAgFnSc1d1	betonová
houba	houba	k1gFnSc1	houba
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
Svákov	Svákov	k1gInSc1	Svákov
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
se	s	k7c7	s
zachovalými	zachovalý	k2eAgInPc7d1	zachovalý
valy	val	k1gInPc7	val
slovanského	slovanský	k2eAgNnSc2d1	slovanské
hradiště	hradiště	k1gNnSc2	hradiště
<g/>
,	,	kIx,	,
se	s	k7c7	s
studánkou	studánka	k1gFnSc7	studánka
a	a	k8xC	a
s	s	k7c7	s
altánkem	altánek	k1gInSc7	altánek
<g/>
.	.	kIx.	.
</s>
<s>
Lesem	les	k1gInSc7	les
vede	vést	k5eAaImIp3nS	vést
stará	starý	k2eAgFnSc1d1	stará
kupecká	kupecký	k2eAgFnSc1d1	kupecká
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
Soběslav	Soběslav	k1gFnSc4	Soběslav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
nedaleko	nedaleko	k7c2	nedaleko
hradiště	hradiště	k1gNnSc2	hradiště
vybudována	vybudován	k2eAgFnSc1d1	vybudována
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
rozhledna	rozhledna	k1gFnSc1	rozhledna
Svákov	Svákov	k1gInSc1	Svákov
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Bertl	Bertl	k1gMnSc1	Bertl
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Jan	Jan	k1gMnSc1	Jan
Bezděk	Bezděk	k1gMnSc1	Bezděk
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
mykolog	mykolog	k1gMnSc1	mykolog
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
Karel	Karel	k1gMnSc1	Karel
Bodlák	bodlák	k1gInSc1	bodlák
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Brodský	Brodský	k1gMnSc1	Brodský
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vězeň	vězeň	k1gMnSc1	vězeň
režimu	režim	k1gInSc2	režim
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g />
.	.	kIx.	.
</s>
<s>
organizace	organizace	k1gFnSc1	organizace
K	k	k7c3	k
231	[number]	k4	231
<g/>
,	,	kIx,	,
emigrant	emigrant	k1gMnSc1	emigrant
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
Radomír	Radomír	k1gMnSc1	Radomír
Čihák	Čihák	k1gMnSc1	Čihák
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
anatomie	anatomie	k1gFnSc2	anatomie
Fakulty	fakulta	k1gFnSc2	fakulta
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
1	[number]	k4	1
<g/>
.	.	kIx.	.
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Emilie	Emilie	k1gFnSc1	Emilie
Fryšová	Fryšová	k1gFnSc1	Fryšová
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
pedagožka	pedagožka	k1gFnSc1	pedagožka
<g/>
,	,	kIx,	,
ředitelka	ředitelka	k1gFnSc1	ředitelka
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
etnografka	etnografka	k1gFnSc1	etnografka
<g/>
,	,	kIx,	,
sběratelka	sběratelka	k1gFnSc1	sběratelka
zejm.	zejm.	k?	zejm.
blatských	blatský	k2eAgInPc2d1	blatský
krojů	kroj	k1gInPc2	kroj
a	a	k8xC	a
výšivek	výšivka	k1gFnPc2	výšivka
<g/>
,	,	kIx,	,
spolupracovnice	spolupracovnice	k1gFnPc1	spolupracovnice
K.	K.	kA	K.
Lustiga	Lustiga	k1gFnSc1	Lustiga
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hořejš	Hořejš	k1gMnSc1	Hořejš
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
Edmund	Edmund	k1gMnSc1	Edmund
<g />
.	.	kIx.	.
</s>
<s>
Chvalovský	Chvalovský	k2eAgMnSc1d1	Chvalovský
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Soběslavi	Soběslav	k1gFnSc6	Soběslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
místní	místní	k2eAgInSc4d1	místní
divadelní	divadelní	k2eAgInSc4d1	divadelní
život	život	k1gInSc4	život
Helena	Helena	k1gFnSc1	Helena
Johnová	Johnová	k1gFnSc1	Johnová
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
česká	český	k2eAgFnSc1d1	Česká
sochařka	sochařka	k1gFnSc1	sochařka
<g/>
,	,	kIx,	,
keramička	keramička	k1gFnSc1	keramička
<g/>
;	;	kIx,	;
profesorka	profesorka	k1gFnSc1	profesorka
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Kotlaba	Kotlaba	k1gMnSc1	Kotlaba
(	(	kIx(	(
<g/>
*	*	kIx~	*
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
mykolog	mykolog	k1gMnSc1	mykolog
<g/>
,	,	kIx,	,
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
Nár	Nár	k1gFnSc2	Nár
<g/>
.	.	kIx.	.
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
Botanického	botanický	k2eAgInSc2d1	botanický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
Jiří	Jiří	k1gMnSc1	Jiří
Laburda	Laburda	k1gMnSc1	Laburda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
Karel	Karel	k1gMnSc1	Karel
Lustig	Lustig	k1gMnSc1	Lustig
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Soběslavi	Soběslav	k1gFnSc6	Soběslav
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
spolkově	spolkově	k6eAd1	spolkově
i	i	k9	i
veřejně	veřejně	k6eAd1	veřejně
činný	činný	k2eAgMnSc1d1	činný
Otakar	Otakar	k1gMnSc1	Otakar
Ostrčil	Ostrčil	k1gMnSc1	Ostrčil
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
v	v	k7c6	v
ND	ND	kA	ND
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
letní	letní	k2eAgMnSc1d1	letní
host	host	k1gMnSc1	host
Soběslavi	Soběslav	k1gFnSc2	Soběslav
-	-	kIx~	-
Mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
trávil	trávit	k5eAaImAgMnS	trávit
skladatel	skladatel	k1gMnSc1	skladatel
své	svůj	k3xOyFgInPc4	svůj
letní	letní	k2eAgInPc4d1	letní
odpočinky	odpočinek	k1gInPc4	odpočinek
v	v	k7c6	v
Soběslavi	Soběslav	k1gFnSc6	Soběslav
v	v	k7c6	v
pronajatém	pronajatý	k2eAgInSc6d1	pronajatý
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
U	u	k7c2	u
Černovického	Černovický	k2eAgInSc2d1	Černovický
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
269	[number]	k4	269
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
nejen	nejen	k6eAd1	nejen
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Léto	léto	k1gNnSc1	léto
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Křížovou	křížový	k2eAgFnSc4d1	křížová
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
"	"	kIx"	"
<g/>
Honzovo	Honzův	k2eAgNnSc1d1	Honzovo
království	království	k1gNnSc1	království
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
r.	r.	kA	r.
1934	[number]	k4	1934
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Skočdopole	Skočdopole	k1gFnSc2	Skočdopole
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Studnička	Studnička	k1gMnSc1	Studnička
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
vědeckého	vědecký	k2eAgInSc2d1	vědecký
života	život	k1gInSc2	život
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
zabýval	zabývat	k5eAaImAgMnS	zabývat
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
také	také	k9	také
astronomií	astronomie	k1gFnSc7	astronomie
(	(	kIx(	(
<g/>
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
záchraně	záchrana	k1gFnSc6	záchrana
přístrojů	přístroj	k1gInPc2	přístroj
i	i	k8xC	i
písemností	písemnost	k1gFnPc2	písemnost
astronoma	astronom	k1gMnSc2	astronom
Tychona	Tychon	k1gMnSc2	Tychon
Brahe	Brah	k1gMnSc2	Brah
(	(	kIx(	(
<g/>
1546	[number]	k4	1546
<g/>
-	-	kIx~	-
<g/>
1601	[number]	k4	1601
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Špaček	Špaček	k1gMnSc1	Špaček
ze	z	k7c2	z
Starburgu	Starburg	k1gInSc2	Starburg
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
-	-	kIx~	-
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
velkostatkář	velkostatkář	k1gMnSc1	velkostatkář
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgMnSc1d1	císařský
rada	rada	k1gMnSc1	rada
<g/>
;	;	kIx,	;
zakladatel	zakladatel	k1gMnSc1	zakladatel
rodu	rod	k1gInSc2	rod
Špačků	Špaček	k1gMnPc2	Špaček
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
Starburgu	Starburg	k1gInSc2	Starburg
Donát	Donát	k1gMnSc1	Donát
Šajner	Šajner	k1gMnSc1	Šajner
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
funkcionář	funkcionář	k1gMnSc1	funkcionář
v	v	k7c6	v
čs	čs	kA	čs
<g/>
.	.	kIx.	.
kulturní	kulturní	k2eAgFnSc3d1	kulturní
politice	politika	k1gFnSc3	politika
Ladislav	Ladislav	k1gMnSc1	Ladislav
Tikal	Tikal	k1gMnSc1	Tikal
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
gymnasta	gymnasta	k1gMnSc1	gymnasta
<g/>
,	,	kIx,	,
olympionik	olympionik	k1gMnSc1	olympionik
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Valenta	Valenta	k1gMnSc1	Valenta
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
básník	básník	k1gMnSc1	básník
Rudolf	Rudolf	k1gMnSc1	Rudolf
Veselý	Veselý	k1gMnSc1	Veselý
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
<g/>
,	,	kIx,	,
mykolog	mykolog	k1gMnSc1	mykolog
-	-	kIx~	-
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
mykologické	mykologický	k2eAgFnSc2d1	mykologická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
Chlebov	Chlebov	k1gInSc4	Chlebov
Nedvědice	Nedvědice	k1gFnSc2	Nedvědice
Soběslav	Soběslav	k1gFnSc1	Soběslav
I	i	k8xC	i
Soběslav	Soběslav	k1gMnSc1	Soběslav
II	II	kA	II
Soběslav	Soběslav	k1gMnSc1	Soběslav
III	III	kA	III
Sabinov	Sabinov	k1gInSc1	Sabinov
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
