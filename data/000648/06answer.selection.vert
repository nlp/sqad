<s>
Soběslav	Soběslav	k1gFnSc1	Soběslav
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Sobieslau	Sobieslaus	k1gInSc3	Sobieslaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historické	historický	k2eAgNnSc4d1	historické
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Tábor	Tábor	k1gInSc1	Tábor
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Tábora	Tábor	k1gInSc2	Tábor
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Lužnice	Lužnice	k1gFnSc2	Lužnice
a	a	k8xC	a
Černovického	Černovický	k2eAgInSc2d1	Černovický
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obtéká	obtékat	k5eAaImIp3nS	obtékat
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
