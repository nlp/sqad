<s>
Vikariáty	vikariát	k1gInPc1
a	a	k8xC
farnosti	farnost	k1gFnPc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
formát	formát	k1gInSc4
<g/>
,	,	kIx,
aktualizace	aktualizace	k1gFnSc1
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1
arcidiecéze	arcidiecéze	k1gFnSc1
římskokatolické	římskokatolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
je	být	k5eAaImIp3nS
rozčleněna	rozčlenit	k5eAaPmNgFnS
do	do	k7c2
14	#num#	k4
vikariátů	vikariát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vikariáty	vikariát	k1gInPc7
mají	mít	k5eAaImIp3nP
přibližně	přibližně	k6eAd1
velikost	velikost	k1gFnSc4
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
svými	svůj	k3xOyFgFnPc7
hranicemi	hranice	k1gFnPc7
se	se	k3xPyFc4
s	s	k7c7
okresy	okres	k1gInPc7
nekryjí	krýt	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
14	#num#	k4
okrskových	okrskový	k2eAgInPc2d1
vikariátů	vikariát	k1gInPc2
působí	působit	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
i	i	k9
vojenský	vojenský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
s	s	k7c7
celostátní	celostátní	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1
pražská	pražský	k2eAgFnSc1d1
sdružuje	sdružovat	k5eAaImIp3nS
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
vikariátech	vikariát	k1gInPc6
247	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
přímo	přímo	k6eAd1
spravovaných	spravovaný	k2eAgFnPc2d1
(	(	kIx(
<g/>
obsazených	obsazený	k2eAgFnPc2d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
124	#num#	k4
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
spravovány	spravovat	k5eAaImNgInP
excurrendo	excurrendo	k6eAd1
(	(	kIx(
<g/>
dojížděním	dojíždění	k1gNnSc7
faráře	farář	k1gMnSc4
nebo	nebo	k8xC
administrátora	administrátor	k1gMnSc4
z	z	k7c2
obsazené	obsazený	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
má	mít	k5eAaImIp3nS
arcidiecéze	arcidiecéze	k1gFnSc1
260	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
téměř	téměř	k6eAd1
100	#num#	k4
řeholních	řeholní	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
arcidiecézi	arcidiecéze	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
757	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
arcidiecéze	arcidiecéze	k1gFnSc2
činí	činit	k5eAaImIp3nS
8990	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
území	území	k1gNnSc2
byl	být	k5eAaImAgInS
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
2,1	2,1	k4
miliónu	milión	k4xCgInSc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
365	#num#	k4
tisíc	tisíc	k4xCgInPc2
uvedlo	uvést	k5eAaPmAgNnS
při	při	k7c6
sčítání	sčítání	k1gNnSc6
římskokatolické	římskokatolický	k2eAgNnSc4d1
vyznání	vyznání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
bývalých	bývalý	k2eAgNnPc6d1
sídlech	sídlo	k1gNnPc6
farnosti	farnost	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
zřízena	zřízen	k2eAgFnSc1d1
farní	farní	k2eAgFnSc1d1
expozitura	expozitura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Specialitou	specialita	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kromě	kromě	k7c2
územních	územní	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
arcidiecézi	arcidiecéze	k1gFnSc6
nalézá	nalézat	k5eAaImIp3nS
také	také	k9
několik	několik	k4yIc1
personálních	personální	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nejsou	být	k5eNaImIp3nP
vymezeny	vymezit	k5eAaPmNgFnP
územně	územně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgInPc1
–	–	k?
Římskokatolická	římskokatolický	k2eAgFnSc1d1
akademická	akademický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Polská	polský	k2eAgFnSc1d1
římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Slovenská	slovenský	k2eAgFnSc1d1
římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
reformy	reforma	k1gFnSc2
(	(	kIx(
<g/>
obnovy	obnova	k1gFnSc2
<g/>
)	)	kIx)
farností	farnost	k1gFnPc2
zvyšovat	zvyšovat	k5eAaImF
<g/>
,	,	kIx,
uvažuje	uvažovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
například	například	k6eAd1
o	o	k7c6
vietnamské	vietnamský	k2eAgFnSc6d1
nebo	nebo	k8xC
německé	německý	k2eAgFnSc6d1
farnosti	farnost	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slučování	slučování	k1gNnSc1
farností	farnost	k1gFnPc2
</s>
<s>
V	v	k7c6
Pražské	pražský	k2eAgFnSc6d1
arcidiecézi	arcidiecéze	k1gFnSc6
jsou	být	k5eAaImIp3nP
postupně	postupně	k6eAd1
po	po	k7c6
vikariátech	vikariát	k1gInPc6
hromadně	hromadně	k6eAd1
slučovány	slučován	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
zejména	zejména	k9
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
dlouhodobě	dlouhodobě	k6eAd1
neobsazené	obsazený	k2eNgInPc1d1
jsou	být	k5eAaImIp3nP
připojovány	připojovat	k5eAaImNgInP
k	k	k7c3
funkčním	funkční	k2eAgFnPc3d1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
byly	být	k5eAaImAgFnP
již	již	k6eAd1
dlouho	dlouho	k6eAd1
spravovány	spravovat	k5eAaImNgInP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
farnosti	farnost	k1gFnPc1
byly	být	k5eAaImAgFnP
soběstačné	soběstačný	k2eAgFnPc1d1
a	a	k8xC
funkční	funkční	k2eAgFnPc1d1
komunity	komunita	k1gFnPc1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
přizpůsobení	přizpůsobení	k1gNnSc4
organizační	organizační	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
faktickému	faktický	k2eAgInSc3d1
stavu	stav	k1gInSc3
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
přináší	přinášet	k5eAaImIp3nS
zjednodušení	zjednodušení	k1gNnSc4
účetnictví	účetnictví	k1gNnSc2
a	a	k8xC
výkaznictví	výkaznictví	k1gNnSc2
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc2
matrik	matrika	k1gFnPc2
atd.	atd.	kA
Tento	tento	k3xDgInSc4
proces	proces	k1gInSc4
začal	začít	k5eAaPmAgInS
být	být	k5eAaImF
v	v	k7c6
arcidiecézi	arcidiecéze	k1gFnSc6
projednáván	projednávat	k5eAaImNgMnS
na	na	k7c4
podzim	podzim	k1gInSc4
2002	#num#	k4
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
sídla	sídlo	k1gNnSc2
několika	několik	k4yIc2
desítek	desítka	k1gFnPc2
neobsazených	obsazený	k2eNgFnPc2d1
farností	farnost	k1gFnPc2
formálně	formálně	k6eAd1
přemístěna	přemístit	k5eAaPmNgFnS
na	na	k7c4
adresu	adresa	k1gFnSc4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
byly	být	k5eAaImAgFnP
spravovány	spravovat	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s>
Počínaje	počínaje	k7c7
rokem	rok	k1gInSc7
2004	#num#	k4
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
v	v	k7c6
rakovnickém	rakovnický	k2eAgInSc6d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kolínském	kolínský	k2eAgInSc6d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
staroboleslavském	staroboleslavský	k2eAgInSc6d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
roudnickém	roudnický	k2eAgNnSc6d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
podřipském	podřipský	k2eAgNnSc6d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
berounském	berounský	k2eAgInSc6d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
vlašimském	vlašimský	k2eAgInSc6d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
)	)	kIx)
vikariátu	vikariát	k1gInSc2
<g/>
,	,	kIx,
přípravy	příprava	k1gFnSc2
jsou	být	k5eAaImIp3nP
nejpokročilejší	pokročilý	k2eAgFnPc1d3
v	v	k7c6
kladenském	kladenský	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
farností	farnost	k1gFnSc7
zatím	zatím	k6eAd1
klesl	klesnout	k5eAaPmAgInS
z	z	k7c2
378	#num#	k4
na	na	k7c4
274	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Psycholog	psycholog	k1gMnSc1
a	a	k8xC
místní	místní	k2eAgMnSc1d1
farník	farník	k1gMnSc1
Jeroným	Jeroným	k1gMnSc1
Klimeš	Klimeš	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
na	na	k7c6
příkladu	příklad	k1gInSc6
rušení	rušení	k1gNnSc2
farnosti	farnost	k1gFnSc2
Líbeznice	Líbeznice	k1gFnSc2
dokládali	dokládat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
slučování	slučování	k1gNnSc6
farností	farnost	k1gFnPc2
nebyla	být	k5eNaImAgFnS
dodržena	dodržet	k5eAaPmNgFnS
základní	základní	k2eAgFnSc1d1
pravidla	pravidlo	k1gNnPc4
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytýkali	vytýkat	k5eAaImAgMnP
představitelům	představitel	k1gMnPc3
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
dokumenty	dokument	k1gInPc1
o	o	k7c4
zrušení	zrušení	k1gNnSc4
byly	být	k5eAaImAgFnP
zřejmě	zřejmě	k6eAd1
o	o	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
antedatované	antedatovaný	k2eAgFnSc2d1
či	či	k8xC
opožděně	opožděně	k6eAd1
zaslané	zaslaný	k2eAgFnPc1d1
a	a	k8xC
že	že	k8xS
dokonce	dokonce	k9
i	i	k9
farář	farář	k1gMnSc1
se	se	k3xPyFc4
o	o	k7c4
zrušení	zrušení	k1gNnSc4
farnosti	farnost	k1gFnSc2
dozvěděl	dozvědět	k5eAaPmAgMnS
až	až	k9
několik	několik	k4yIc4
dnů	den	k1gInPc2
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
účinnosti	účinnost	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
předem	předem	k6eAd1
s	s	k7c7
farníky	farník	k1gMnPc7
ani	ani	k8xC
knězem	kněz	k1gMnSc7
nikdo	nikdo	k3yNnSc1
změnu	změna	k1gFnSc4
nejen	nejen	k6eAd1
neprojednal	projednat	k5eNaPmAgMnS
(	(	kIx(
<g/>
ačkoliv	ačkoliv	k8xS
sloučení	sloučení	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
bylo	být	k5eAaImAgNnS
navrženo	navrhnout	k5eAaPmNgNnS
již	již	k9
12	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
například	například	k6eAd1
kardinál	kardinál	k1gMnSc1
Vlk	Vlk	k1gMnSc1
při	při	k7c6
návštěvě	návštěva	k1gFnSc6
farnosti	farnost	k1gFnSc2
26	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
se	se	k3xPyFc4
o	o	k7c6
chystaném	chystaný	k2eAgNnSc6d1
sloučení	sloučení	k1gNnSc6
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
ani	ani	k8xC
nezmínil	zmínit	k5eNaPmAgInS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
jim	on	k3xPp3gMnPc3
je	on	k3xPp3gNnSc4
neoznámil	oznámit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgInPc1
mluvčí	mluvčit	k5eAaImIp3nP
včetně	včetně	k7c2
Klimeše	Klimeš	k1gMnSc2
zaslali	zaslat	k5eAaPmAgMnP
na	na	k7c6
arcibiskupství	arcibiskupství	k1gNnSc6
jménem	jméno	k1gNnSc7
farníků	farník	k1gMnPc2
dopis	dopis	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
vyjádřili	vyjádřit	k5eAaPmAgMnP
nesouhlas	nesouhlas	k1gInSc4
se	s	k7c7
sloučením	sloučení	k1gNnSc7
<g/>
,	,	kIx,
k	k	k7c3
dopisu	dopis	k1gInSc3
se	se	k3xPyFc4
pak	pak	k9
svými	svůj	k3xOyFgInPc7
podpisy	podpis	k1gInPc7
připojilo	připojit	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
farníků	farník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
</s>
<s>
I.	I.	kA
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS
4	#num#	k4
historická	historický	k2eAgNnPc1d1
pražská	pražský	k2eAgNnPc1d1
města	město	k1gNnPc1
a	a	k8xC
Strahov	Strahov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
23	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
asi	asi	k9
30	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
kněží	kněz	k1gMnPc2
a	a	k8xC
jáhnů	jáhen	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
řeholních	řeholní	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
slučování	slučování	k1gNnSc3
farností	farnost	k1gFnPc2
dochází	docházet	k5eAaImIp3nS
postupně	postupně	k6eAd1
<g/>
,	,	kIx,
sloučeny	sloučen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
například	například	k6eAd1
farnosti	farnost	k1gFnPc1
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
některým	některý	k3yIgInPc3
klášterním	klášterní	k2eAgInPc3d1
kostelům	kostel	k1gInPc3
byl	být	k5eAaImAgInS
přiznán	přiznán	k2eAgInSc4d1
zvláštní	zvláštní	k2eAgInSc4d1
status	status	k1gInSc4
a	a	k8xC
byly	být	k5eAaImAgFnP
začleněny	začlenit	k5eAaPmNgFnP
do	do	k7c2
větší	veliký	k2eAgFnSc2d2
farnosti	farnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Vladimír	Vladimír	k1gMnSc1
Kelnar	Kelnar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
katedrály	katedrála	k1gFnSc2
sv.	sv.	kA
Víta	Vít	k1gMnSc2
<g/>
,	,	kIx,
Václava	Václav	k1gMnSc2
a	a	k8xC
Vojtěcha	Vojtěch	k1gMnSc2
Praha-Hradčany	Praha-Hradčan	k1gMnPc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Tomáše	Tomáš	k1gMnSc2
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2004	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
a	a	k8xC
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
pod	pod	k7c7
řetězem	řetěz	k1gInSc7
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
duchovní	duchovní	k2eAgFnPc1d1
správy	správa	k1gFnPc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Vítězné	vítězný	k2eAgFnSc2d1
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
Paduánského	paduánský	k2eAgInSc2d1
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
a	a	k8xC
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Matky	matka	k1gFnSc2
ustavičné	ustavičný	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
kostely	kostel	k1gInPc1
změněny	změněn	k2eAgInPc1d1
na	na	k7c4
klášterní	klášterní	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jindřicha	Jindřich	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Sněžné	sněžný	k2eAgFnSc2d1
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Petra	Petr	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Ducha	duch	k1gMnSc2
Praha-Staré	Praha-Starý	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Františka	František	k1gMnSc4
z	z	k7c2
Assisi	Assise	k1gFnSc3
Praha-Staré	Praha-Starý	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Haštala	Haštal	k1gMnSc2
Praha-Staré	Praha-Starý	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jiljí	Jiljí	k1gMnSc2
Praha-Staré	Praha-Starý	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Matky	matka	k1gFnSc2
Boží	boží	k2eAgFnPc4d1
před	před	k7c7
Týnem	Týn	k1gInSc7
Praha-Staré	Praha-Starý	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Praha-Strahov	Praha-Strahov	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Apolináře	Apolinář	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Podskalí	Podskalí	k1gNnSc6
Praha-Nové	Praha-Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Štěpána	Štěpán	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Josefa	Josef	k1gMnSc2
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Kříže	Kříž	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Ignáce	Ignác	k1gMnSc2
Praha-Nové	Praha-Nová	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
Praha-Hradčany	Praha-Hradčan	k1gMnPc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
akademická	akademický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
zřízena	zřízen	k2eAgFnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
zrušena	zrušen	k2eAgFnSc1d1
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nejsv	Nejsva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salvátora	Salvátor	k1gMnSc2
Praha-Staré	Praha-Starý	k2eAgNnSc1d1
Město	město	k1gNnSc1
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Polská	polský	k2eAgFnSc1d1
římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
zřízena	zřízen	k2eAgFnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jiljí	Jiljí	k1gMnSc2
na	na	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přibližně	přibližně	k6eAd1
v	v	k7c6
září	září	k1gNnSc6
2003	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
Praha	Praha	k1gFnSc1
–	–	k?
Hradčany	Hradčany	k1gInPc1
a	a	k8xC
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
Praha	Praha	k1gFnSc1
–	–	k?
Hradčany	Hradčany	k1gInPc1
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
Praha	Praha	k1gFnSc1
–	–	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
klášterní	klášterní	k2eAgNnSc4d1
<g/>
,	,	kIx,
území	území	k1gNnSc6
převedeno	převést	k5eAaPmNgNnS
pod	pod	k7c4
farnost	farnost	k1gFnSc4
u	u	k7c2
Matky	matka	k1gFnSc2
Boží	boží	k2eAgFnPc4d1
před	před	k7c7
Týnem	Týn	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Josefa	Josefa	k1gFnSc1
Praha	Praha	k1gFnSc1
–	–	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
klášterní	klášterní	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2004	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Benedikta	Benedikt	k1gMnSc2
Praha	Praha	k1gFnSc1
–	–	k?
Hradčany	Hradčany	k1gInPc1
<g/>
,	,	kIx,
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
klášterní	klášterní	k2eAgInPc4d1
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Bolestné	bolestný	k2eAgFnSc2d1
(	(	kIx(
<g/>
U	u	k7c2
Alžbětinek	alžbětinka	k1gFnPc2
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
–	–	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
klášterní	klášterní	k2eAgNnSc4d1
<g/>
,	,	kIx,
území	území	k1gNnSc4
bylo	být	k5eAaImAgNnS
začleněno	začlenit	k5eAaPmNgNnS
pod	pod	k7c4
farnost	farnost	k1gFnSc4
u	u	k7c2
kostela	kostel	k1gInSc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Podskalí	Podskalí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Voršily	Voršila	k1gFnSc2
Praha	Praha	k1gFnSc1
–	–	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
klášterní	klášterní	k2eAgInPc4d1
a	a	k8xC
území	území	k1gNnSc4
začleněno	začleněn	k2eAgNnSc4d1
do	do	k7c2
farnosti	farnost	k1gFnSc2
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
Praha	Praha	k1gFnSc1
–	–	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
5	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
několik	několik	k4yIc4
příměstských	příměstský	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
22	#num#	k4
farností	farnost	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
20	#num#	k4
<g/>
,	,	kIx,
jich	on	k3xPp3gInPc2
14	#num#	k4
bylo	být	k5eAaImAgNnS
spravováno	spravovat	k5eAaImNgNnS
přímo	přímo	k6eAd1
a	a	k8xC
zbylé	zbylý	k2eAgNnSc1d1
excurrendo	excurrendo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Mgr.	Mgr.	kA
Josef	Josef	k1gMnSc1
Ptáček	Ptáček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
29	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
jáhen	jáhen	k1gMnSc1
a	a	k8xC
9	#num#	k4
pastoračních	pastorační	k2eAgMnPc2d1
asistentů	asistent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
54	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slučování	slučování	k1gNnSc1
farností	farnost	k1gFnPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
fázi	fáze	k1gFnSc6
vzdálené	vzdálený	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Filipa	Filip	k1gMnSc2
a	a	k8xC
Jakuba	Jakub	k1gMnSc2
Praha-Hlubočepy	Praha-Hlubočepa	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
k	k	k7c3
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
přičleněna	přičleněn	k2eAgFnSc1d1
rušená	rušený	k2eAgFnSc1d1
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
Praha-Jinonice	Praha-Jinonice	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
Praha-Košíře	Praha-Košíř	k1gInSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
Praha-Radotín	Praha-Radotín	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Václava	Václav	k1gMnSc2
Praha-Smíchov	Praha-Smíchov	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
Praha-Stodůlky	Praha-Stodůlka	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
Praha-Zbraslav	Praha-Zbraslava	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
k	k	k7c3
ní	on	k3xPp3gFnSc6
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Vrané	vraný	k2eAgFnPc1d1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
a	a	k8xC
Zlatníky	zlatník	k1gMnPc7
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Markéty	Markéta	k1gFnSc2
Praha-Břevnov	Praha-Břevnov	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Gotharda	Gothard	k1gMnSc2
Praha-Bubeneč	Praha-Bubeneč	k1gMnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Matěje	Matěj	k1gMnSc2
Praha-Dejvice	Praha-Dejvice	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2008	#num#	k4
byl	být	k5eAaImAgInS
Suchdol	Suchdol	k1gInSc1
přeřazen	přeřadit	k5eAaPmNgInS
z	z	k7c2
farnosti	farnost	k1gFnSc2
Únětice	Únětice	k1gFnSc2
do	do	k7c2
farnosti	farnost	k1gFnSc2
u	u	k7c2
sv.	sv.	kA
Matěje	Matěj	k1gMnSc2
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
a	a	k8xC
ZSJ	ZSJ	kA
Starý	starý	k2eAgInSc1d1
Sedlec	Sedlec	k1gInSc1
B	B	kA
z	z	k7c2
farnosti	farnost	k1gFnSc2
Roztoky	roztoka	k1gFnSc2
u	u	k7c2
Prahy	Praha	k1gFnSc2
k	k	k7c3
farnosti	farnost	k1gFnSc3
u	u	k7c2
sv.	sv.	kA
Matěje	Matěj	k1gMnSc2
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Fabiána	Fabián	k1gMnSc2
a	a	k8xC
Šebestiána	Šebestián	k1gMnSc2
Praha-Liboc	Praha-Liboc	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Cyrila	Cyril	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
Praha-Nebušice	Praha-Nebušic	k1gMnSc2
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
Horoměřice	Horoměřice	k1gFnSc1
přeřazena	přeřazen	k2eAgFnSc1d1
z	z	k7c2
farnosti	farnost	k1gFnSc2
Únětice	Únětice	k1gFnSc2
do	do	k7c2
farnosti	farnost	k1gFnSc2
Praha-Nebušice	Praha-Nebušice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Norberta	Norbert	k1gMnSc2
Praha-Střešovice	Praha-Střešovice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Hostivice	Hostivice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Ořech	ořech	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Roztoky	roztoka	k1gFnSc2
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Libčice	Libčice	k1gFnSc2
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
Tursko	Tursko	k1gNnSc1
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
ZSJ	ZSJ	kA
Starý	starý	k2eAgInSc4d1
Sedlec	Sedlec	k1gInSc4
B	B	kA
přeřazena	přeřazen	k2eAgFnSc1d1
z	z	k7c2
farnosti	farnost	k1gFnSc2
Roztoky	roztoka	k1gFnSc2
u	u	k7c2
Prahy	Praha	k1gFnSc2
k	k	k7c3
farnosti	farnost	k1gFnSc3
u	u	k7c2
sv.	sv.	kA
Matěje	Matěj	k1gMnSc2
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
Horoměřice	Horoměřice	k1gFnSc1
přeřazena	přeřazen	k2eAgFnSc1d1
z	z	k7c2
farnosti	farnost	k1gFnSc2
Únětice	Únětice	k1gFnSc2
do	do	k7c2
farnosti	farnost	k1gFnSc2
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Suchdol	Praha-Suchdol	k1gInSc1
byl	být	k5eAaImAgInS
přeřazen	přeřadit	k5eAaPmNgInS
z	z	k7c2
farnosti	farnost	k1gFnSc2
Únětice	Únětice	k1gFnSc2
do	do	k7c2
farnosti	farnost	k1gFnSc2
u	u	k7c2
sv.	sv.	kA
Matěje	Matěj	k1gMnSc2
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc4d1
farnosti	farnost	k1gFnPc4
<g/>
:	:	kIx,
Budeč-Kováry	Budeč-Kovár	k1gMnPc4
<g/>
,	,	kIx,
Koleč	Koleč	k1gInSc1
<g/>
,	,	kIx,
Noutonice	Noutonice	k1gFnSc1
<g/>
,	,	kIx,
Únětice	Únětice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Třebotov	Třebotov	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Tuchoměřice	Tuchoměřice	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
sloučena	sloučen	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Středokluky	Středokluk	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
farnost	farnost	k1gFnSc1
se	se	k3xPyFc4
stála	stát	k5eAaImAgFnS
součástí	součást	k1gFnSc7
II	II	kA
<g/>
.	.	kIx.
pražského	pražský	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Martina	Martin	k1gMnSc2
Praha-Řepy	Praha-Řepa	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Gabriela	Gabriela	k1gFnSc1
Praha-Smíchov	Praha-Smíchov	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Vítězné	vítězný	k2eAgFnSc2d1
Praha-Řepy	Praha-Řepa	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
Praha-Dejvice	Praha-Dejvice	k1gFnSc2
</s>
<s>
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2005	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Václava	Václav	k1gMnSc2
Praha-Dejvice	Praha-Dejvice	k1gFnSc2
<g/>
,	,	kIx,
zřízená	zřízený	k2eAgFnSc1d1
po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
rektorátní	rektorátní	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
4	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
19	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
15	#num#	k4
je	být	k5eAaImIp3nS
spravováno	spravovat	k5eAaImNgNnS
přímo	přímo	k6eAd1
a	a	k8xC
4	#num#	k4
(	(	kIx(
<g/>
Braník	Braník	k1gInSc1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnPc1
<g/>
,	,	kIx,
Koloděje	Koloděje	k1gFnPc1
a	a	k8xC
Jirny	Jirny	k1gInPc1
<g/>
)	)	kIx)
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
ještě	ještě	k9
nedlouho	dlouho	k6eNd1
předtím	předtím	k6eAd1
bylo	být	k5eAaImAgNnS
obsazeno	obsadit	k5eAaPmNgNnS
všech	všecek	k3xTgFnPc2
19	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
P.	P.	kA
Stanisław	Stanisław	k1gFnSc1
Góra	Góra	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
34	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
a	a	k8xC
působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
25	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
4	#num#	k4
jáhni	jáhen	k1gMnPc1
a	a	k8xC
6	#num#	k4
pastoračních	pastorační	k2eAgMnPc2d1
asistentů	asistent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slučování	slučování	k1gNnSc1
farností	farnost	k1gFnPc2
dosud	dosud	k6eAd1
neproběhlo	proběhnout	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Ludmily	Ludmila	k1gFnSc2
Praha-Vinohrady	Praha-Vinohrada	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
Praha-Vyšehrad	Praha-Vyšehrada	k1gFnPc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1
Srdce	srdce	k1gNnSc2
Páně	páně	k2eAgFnSc2d1
Praha-Vinohrady	Praha-Vinohrada	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Prokopa	Prokop	k1gMnSc2
Praha-Braník	Praha-Braník	k1gMnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
z	z	k7c2
Assisi	Assis	k1gMnSc6
Praha-Chodov	Praha-Chodov	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
Praha-Kunratice	Praha-Kunratice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Královny	královna	k1gFnSc2
míru	mír	k1gInSc2
Praha-Lhotka	Praha-Lhotka	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Narození	narození	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Praha-Michle	Praha-Michle	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Praha-Modřany	Praha-Modřan	k1gMnPc7
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
Praha-Nusle	Praha-Nusle	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Michaela	Michael	k1gMnSc2
Archanděla	archanděl	k1gMnSc2
Praha-Podolí	Praha-Podolí	k1gNnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Anežky	Anežka	k1gFnSc2
České	český	k2eAgFnSc2d1
Praha-Spořilov	Praha-Spořilov	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Povýšení	povýšení	k1gNnSc2
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
Praha-Koloděje	Praha-Koloděje	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Stětí	Stětí	k1gNnSc2
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
Praha-Hostivař	Praha-Hostivař	k1gMnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Neposkvrněného	poskvrněný	k2eNgNnSc2d1
Početí	početí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Praha-Strašnice	Praha-Strašnice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Všech	všecek	k3xTgFnPc2
svatých	svatý	k2eAgFnPc2d1
Praha-Uhříněves	Praha-Uhříněvesa	k1gFnPc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostelů	kostel	k1gInPc2
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
a	a	k8xC
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
Praha-Vršovice	Praha-Vršovice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
Praha-Petrovice	Praha-Petrovice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Jirny	Jirny	k1gInPc4
<g/>
:	:	kIx,
do	do	k7c2
III	III	kA
<g/>
.	.	kIx.
pražského	pražský	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
byla	být	k5eAaImAgFnS
připojena	připojit	k5eAaPmNgFnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
u	u	k7c2
Nejsv	Nejsva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srdce	srdce	k1gNnPc1
Ježíšova	Ježíšův	k2eAgFnSc1d1
Praha-Malešice	Praha-Malešice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS
severovýchodní	severovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
8	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
2	#num#	k4
příměstské	příměstský	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
17	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
12	#num#	k4
spravovaných	spravovaný	k2eAgInPc2d1
přímo	přímo	k6eAd1
a	a	k8xC
5	#num#	k4
excurrendo	excurrendo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Miroslav	Miroslav	k1gMnSc1
Cúth	Cúth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
39	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
26	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
jáhni	jáhen	k1gMnPc1
a	a	k8xC
4	#num#	k4
pastorační	pastorační	k2eAgMnPc1d1
asistenti	asistent	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloučeny	sloučen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
zatím	zatím	k6eAd1
jen	jen	k9
tři	tři	k4xCgFnPc1
žižkovské	žižkovský	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
(	(	kIx(
<g/>
sv.	sv.	kA
Roch	roch	k0
a	a	k8xC
sv.	sv.	kA
Anna	Anna	k1gFnSc1
ke	k	k7c3
sv.	sv.	kA
Prokopu	prokop	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Klecany	Klecana	k1gFnSc2
a	a	k8xC
Líbeznice	Líbeznice	k1gFnSc2
a	a	k8xC
připojeny	připojit	k5eAaPmNgFnP
k	k	k7c3
farnosti	farnost	k1gFnSc3
Odolena	Odolen	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
podřipského	podřipský	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
převedena	převést	k5eAaPmNgFnS
do	do	k7c2
IV	IV	kA
<g/>
.	.	kIx.
pražského	pražský	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Prokopa	Prokop	k1gMnSc2
Praha-Žižkov	Praha-Žižkov	k1gInSc4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
sloučena	sloučen	k2eAgFnSc1d1
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Anny	Anna	k1gFnSc2
Praha-Žižkov	Praha-Žižkov	k1gInSc1
a	a	k8xC
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Rocha	Rocha	k1gFnSc1
Praha-Žižkov	Praha-Žižkov	k1gInSc1
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Antonína	Antonín	k1gMnSc2
Padovského	padovský	k2eAgMnSc2d1
Praha-Holešovice	Praha-Holešovice	k1gFnPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
Praha-Bohnice	Praha-Bohnice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Cyrila	Cyril	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
Praha-Karlín	Praha-Karlína	k1gFnPc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Terezie	Terezie	k1gFnPc1
od	od	k7c2
Dítěte	Dítě	k1gMnSc2
Ježíše	Ježíš	k1gMnSc2
Praha-Kobylisy	Praha-Kobylis	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
Praha-Libeň	Praha-Libeň	k1gFnSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Remigia	Remigius	k1gMnSc2
Praha-Čakovice	Praha-Čakovice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Praha-Dolní	Praha-Dolní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
Praha-Hloubětín	Praha-Hloubětín	k1gMnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Ludmily	Ludmila	k1gFnSc2
Praha-Chvaly	Praha-Chval	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc2
Praha-Kyje	Praha-Kyje	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Václava	Václav	k1gMnSc2
Praha-Prosek	Praha-Proska	k1gFnPc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Praha-Třeboradice	Praha-Třeboradice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Povýšení	povýšení	k1gNnSc4
sv.	sv.	kA
Kříže	Kříž	k1gMnSc2
Praha-Vinoř	Praha-Vinoř	k1gFnSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Krista	Kristus	k1gMnSc2
Krále	Král	k1gMnSc2
Praha-Vysočany	Praha-Vysočan	k1gMnPc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
kvazifarnost	kvazifarnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Alžběty	Alžběta	k1gFnSc2
Praha-Kbely	Praha-Kbela	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Odolena	Odolena	k1gFnSc1
Voda	voda	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Klecany	Klecana	k1gFnSc2
<g/>
,	,	kIx,
Líbeznice	Líbeznice	k1gFnSc2
a	a	k8xC
celá	celý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
byla	být	k5eAaImAgFnS
převedena	převést	k5eAaPmNgFnS
z	z	k7c2
Podřipského	podřipský	k2eAgInSc2d1
do	do	k7c2
IV	IV	kA
<g/>
.	.	kIx.
pražského	pražský	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
kvazifarnost	kvazifarnost	k1gFnSc1
Praha-Na	Praha-N	k1gInSc2
Balkáně	Balkán	k1gInSc6
</s>
<s>
Vikariát	vikariát	k1gInSc1
Podřipsko	Podřipsko	k1gNnSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
jsou	být	k5eAaImIp3nP
4	#num#	k4
farnosti	farnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Ptáček	Ptáček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
5	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
jáhen	jáhen	k1gMnSc1
a	a	k8xC
několik	několik	k4yIc1
pastoračních	pastorační	k2eAgMnPc2d1
asistentů	asistent	k1gMnPc2
a	a	k8xC
asistentek	asistentka	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
62	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Farnosti	farnost	k1gFnPc1
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Roudnice	Roudnice	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Bechlín	Bechlín	k1gInSc1
<g/>
,	,	kIx,
Cítov	Cítov	k1gInSc1
u	u	k7c2
Mělníka	Mělník	k1gInSc2
<g/>
,	,	kIx,
Černouček	Černouček	k1gInSc1
<g/>
,	,	kIx,
Hořín	Hořín	k1gInSc1
<g/>
,	,	kIx,
Lužec	Lužec	k1gInSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
Račiněves	Račiněves	k1gInSc1
<g/>
,	,	kIx,
Vliněves	Vliněves	k1gInSc1
<g/>
,	,	kIx,
Vrbno	Vrbno	k6eAd1
u	u	k7c2
Mělníka	Mělník	k1gInSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Budyně	Budyně	k1gFnSc2
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Charvatce	Charvatka	k1gFnSc3
<g/>
,	,	kIx,
Ječovice	Ječovice	k1gFnPc1
<g/>
,	,	kIx,
Nížebohy	Nížeboh	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Kralupy	Kralupy	k1gInPc4
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Nelahozeves	Nelahozeves	k1gInSc1
<g/>
,	,	kIx,
Chržín	Chržín	k1gInSc1
<g/>
,	,	kIx,
Chvatěruby	Chvatěrub	k1gInPc1
<g/>
,	,	kIx,
Veltrusy	Veltrusy	k1gInPc1
<g/>
,	,	kIx,
Velvary	Velvar	k1gInPc1
<g/>
,	,	kIx,
Vepřek	Vepřek	k1gInSc1
<g/>
,	,	kIx,
Zeměchy	Zeměch	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Neratovice	Neratovice	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Hostín	Hostína	k1gFnPc2
<g/>
,	,	kIx,
Chlumín	Chlumína	k1gFnPc2
<g/>
,	,	kIx,
Kojetice	Kojetika	k1gFnSc6
<g/>
,	,	kIx,
Obříství	Obříství	k1gNnSc6
</s>
<s>
Vikariát	vikariát	k1gInSc1
Kladno	Kladno	k1gNnSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
6	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
spravováné	spravováný	k2eAgFnPc1d1
přímo	přímo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
12	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
jáhni	jáhen	k1gMnPc5
(	(	kIx(
<g/>
Kladno	Kladno	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
2	#num#	k4
pastorační	pastorační	k2eAgMnPc1d1
asistenti	asistent	k1gMnPc1
(	(	kIx(
<g/>
Slaný	Slaný	k1gInSc1
a	a	k8xC
Peruc	Peruc	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
96	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Kladno	Kladno	k1gNnSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
kvazifarnost	kvazifarnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
Kladno-Švermov	Kladno-Švermov	k1gInSc4
<g/>
,	,	kIx,
farnost	farnost	k1gFnSc4
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
Kladno-Vrapice	Kladno-Vrapic	k1gMnSc2
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Václava	Václav	k1gMnSc2
Kladno-Rozdělov	Kladno-Rozdělov	k1gInSc4
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Buštěhrad	Buštěhrad	k1gInSc1
<g/>
,	,	kIx,
Dřetovice	Dřetovice	k1gFnSc1
<g/>
,	,	kIx,
Libušín	Libušín	k1gInSc1
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Slaný	Slaný	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Hořešovice	Hořešovice	k1gFnSc1
<g/>
,	,	kIx,
Kvílice	Kvílice	k1gFnSc1
<g/>
,	,	kIx,
Pozdeň	Pozdeň	k1gFnSc1
<g/>
,	,	kIx,
Tuřany	Tuřana	k1gFnPc1
<g/>
,	,	kIx,
Zvoleněves	Zvoleněves	k1gInSc1
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Smečno	Smečno	k1gNnSc4
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Malíkovice	Malíkovice	k1gFnPc1
<g/>
,	,	kIx,
Pchery	Pchera	k1gFnPc1
<g/>
,	,	kIx,
Stochov	Stochov	k1gInSc1
<g/>
,	,	kIx,
Tuchlovice	Tuchlovice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Unhošť	Unhošť	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Družec	Družec	k1gInSc1
<g/>
,	,	kIx,
Hostouň	Hostouň	k1gFnSc1
u	u	k7c2
Kladna	Kladno	k1gNnSc2
<g/>
,	,	kIx,
Svárov	Svárov	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Zlonice	Zlonice	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Budeničky	Budenička	k1gFnPc1
<g/>
,	,	kIx,
Hobšovice	Hobšovice	k1gFnPc1
<g/>
,	,	kIx,
Klobuky	Klobuk	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
Panenský	panenský	k2eAgInSc1d1
Týnec	Týnec	k1gInSc1
<g/>
,	,	kIx,
Peruc	Peruc	k1gInSc1
<g/>
,	,	kIx,
Slavětín	Slavětín	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
,	,	kIx,
Smolnice	smolnice	k1gFnSc1
<g/>
,	,	kIx,
Vraný	vraný	k2eAgInSc1d1
u	u	k7c2
Slaného	Slaný	k1gInSc2
<g/>
,	,	kIx,
Vrbno	Vrbno	k6eAd1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
farnosti	farnost	k1gFnSc2
<g/>
:	:	kIx,
Radonice	Radonice	k1gFnSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
a	a	k8xC
Kmetiněves	Kmetiněves	k1gMnSc1
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vikariát	vikariát	k1gInSc1
Rakovník	Rakovník	k1gInSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
jsou	být	k5eAaImIp3nP
4	#num#	k4
farnosti	farnost	k1gFnPc1
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
spravované	spravovaný	k2eAgFnPc1d1
přímo	přímo	k6eAd1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
se	se	k3xPyFc4
počet	počet	k1gInSc1
farností	farnost	k1gFnPc2
snížil	snížit	k5eAaPmAgInS
z	z	k7c2
26	#num#	k4
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Ján	Ján	k1gMnSc1
Petrovič	Petrovič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
4	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
žádný	žádný	k3yNgInSc4
jáhen	jáhen	k1gMnSc1
ani	ani	k8xC
pastorační	pastorační	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
56	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Nové	Nová	k1gFnSc2
Strašecí	Strašecí	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Hředle	Hředle	k1gMnPc2
<g/>
,	,	kIx,
Lány	lán	k1gInPc1
<g/>
,	,	kIx,
Mšec	Mšec	k1gFnSc1
<g/>
,	,	kIx,
Řevničov	Řevničov	k1gInSc1
<g/>
,	,	kIx,
Srbeč	Srbeč	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Petrovice	Petrovice	k1gFnSc1
u	u	k7c2
Rakovníka	Rakovník	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Děkov	Děkov	k1gInSc1
<g/>
,	,	kIx,
Kněževes	Kněževes	k1gFnSc1
u	u	k7c2
Rakovníka	Rakovník	k1gInSc2
<g/>
,	,	kIx,
Kolešovice	Kolešovice	k1gFnSc1
<g/>
,	,	kIx,
Panoší	panoší	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
<g/>
,	,	kIx,
Rousínov	Rousínov	k1gInSc1
<g/>
,	,	kIx,
Senomaty	Senomat	k1gInPc1
<g/>
,	,	kIx,
Slabce	slabec	k1gMnPc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Rakovník	Rakovník	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Kounov	Kounov	k1gInSc1
<g/>
,	,	kIx,
Lišany	Lišan	k1gInPc1
<g/>
,	,	kIx,
Lužná	Lužná	k1gFnSc1
<g/>
,	,	kIx,
Mutějovice	Mutějovice	k1gFnSc1
<g/>
,	,	kIx,
Olešná	Olešný	k2eAgFnSc1d1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Zbečno	Zbečno	k1gNnSc4
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Bratronice	Bratronice	k1gFnSc2
<g/>
,	,	kIx,
Křivoklát	Křivoklát	k1gInSc1
<g/>
,	,	kIx,
Městečko	městečko	k1gNnSc1
<g/>
,	,	kIx,
Nezabudice	Nezabudice	k1gFnSc1
<g/>
,	,	kIx,
Skryje	skrýt	k5eAaPmIp3nS
</s>
<s>
Vikariát	vikariát	k1gInSc1
Beroun	Beroun	k1gInSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
7	#num#	k4
farností	farnost	k1gFnPc2
(	(	kIx(
<g/>
ke	k	k7c3
sloučení	sloučení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
2006	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
5	#num#	k4
je	on	k3xPp3gMnPc4
spravovaných	spravovaný	k2eAgNnPc2d1
přímo	přímo	k6eAd1
a	a	k8xC
2	#num#	k4
excurrendo	excurrendo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Pecinovský	Pecinovský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
81	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
8	#num#	k4
kněží	kněz	k1gMnPc2
a	a	k8xC
1	#num#	k4
jáhen	jáhen	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Beroun	Beroun	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc6
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Hudlice	Hudlice	k1gFnPc1
<g/>
,	,	kIx,
Králův	Králův	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
<g/>
,	,	kIx,
Loděnice	loděnice	k1gFnSc1
u	u	k7c2
Berouna	Beroun	k1gInSc2
<g/>
,	,	kIx,
Nižbor	Nižbor	k1gMnSc1
<g/>
,	,	kIx,
Svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
pod	pod	k7c7
Skalou	Skala	k1gMnSc7
<g/>
,	,	kIx,
Tetín	Tetín	k1gInSc1
a	a	k8xC
Železná	železný	k2eAgFnSc1d1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Hořovice	Hořovice	k1gFnPc1
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc6
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Bezdědice	Bezdědice	k1gFnSc2
u	u	k7c2
Hořovic	Hořovice	k1gFnPc2
<g/>
,	,	kIx,
Hostomice	Hostomika	k1gFnSc6
pod	pod	k7c7
Brdy	brdo	k1gNnPc7
<g/>
,	,	kIx,
Lochovice	Lochovice	k1gFnSc1
<g/>
,	,	kIx,
Mrtník	Mrtník	k1gInSc1
a	a	k8xC
Praskolesy	Praskoles	k1gInPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
byla	být	k5eAaImAgFnS
připojena	připojit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Svatá	svatat	k5eAaImIp3nS
Dobrotivá-Zaječov	Dobrotivá-Zaječov	k1gInSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
klášterní	klášterní	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Rudná-Hořelice	Rudná-Hořelice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Řevnice	řevnice	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc6
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Dobřichovice-Karlík	Dobřichovice-Karlík	k1gInSc1
<g/>
,	,	kIx,
Karlštejn	Karlštejn	k1gInSc1
<g/>
,	,	kIx,
Liteň	Liteň	k1gFnSc1
<g/>
,	,	kIx,
Osov	Osov	k1gInSc1
<g/>
,	,	kIx,
Všenory	Všenor	k1gInPc1
a	a	k8xC
Všeradice	Všeradice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Karlštejn-hrad	Karlštejn-hrada	k1gFnPc2
<g/>
,	,	kIx,
nástupcem	nástupce	k1gMnSc7
je	být	k5eAaImIp3nS
Kolegiátní	kolegiátní	k2eAgFnSc1d1
kapitula	kapitula	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Tachlovice	Tachlovice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Úhonice	Úhonice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Žebrák	Žebrák	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc6
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
Cerhovice	Cerhovice	k1gFnSc1
<g/>
,	,	kIx,
Neumětely	neumětel	k1gMnPc4
<g/>
,	,	kIx,
Tmaň	Tmaň	k1gFnSc1
<g/>
,	,	kIx,
Velíz-Kublov	Velíz-Kublov	k1gInSc1
<g/>
,	,	kIx,
Zdice	Zdice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Vikariát	vikariát	k1gInSc1
Příbram	Příbram	k1gFnSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
30	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
10	#num#	k4
spravovaných	spravovaný	k2eAgFnPc2d1
přímo	přímo	k6eAd1
(	(	kIx(
<g/>
Dobříš	Dobříš	k1gFnSc1
<g/>
,	,	kIx,
Jince	Jince	k1gInPc1
<g/>
,	,	kIx,
Krásná	krásný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
Mníšek	Mníšek	k1gInSc1
pod	pod	k7c7
Brdy	Brdy	k1gInPc7
<g/>
,	,	kIx,
Petrovice	Petrovice	k1gFnPc4
u	u	k7c2
Sedlčan	Sedlčany	k1gInPc2
<g/>
,	,	kIx,
Sedlčany	Sedlčany	k1gInPc1
<g/>
,	,	kIx,
Starý	starý	k2eAgMnSc1d1
Knín	Knín	k1gMnSc1
a	a	k8xC
všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
farnosti	farnost	k1gFnPc1
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
20	#num#	k4
excurrendo	excurrendo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slučování	slučování	k1gNnSc1
farností	farnost	k1gFnPc2
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Jindřich	Jindřich	k1gMnSc1
Krink	Krink	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
16	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
jáhen	jáhen	k1gMnSc1
a	a	k8xC
5	#num#	k4
pastoračních	pastorační	k2eAgMnPc2d1
asistentů	asistent	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
73	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Dobříš	dobřit	k5eAaImIp2nS
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Jince	Jince	k1gInPc4
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnSc3
Hluboš	Hluboš	k1gMnSc1
a	a	k8xC
Pičín	Pičín	k1gMnSc1
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Krásná	krásný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Dolní	dolní	k2eAgFnSc1d1
Hbity	Hbity	k?
<g/>
,	,	kIx,
Hřimeždice	Hřimeždice	k1gFnSc1
<g/>
,	,	kIx,
Kamýk	Kamýk	k1gInSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
Svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
u	u	k7c2
Sedlčan	Sedlčany	k1gInPc2
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Mníšek	Mníšek	k1gInSc1
pod	pod	k7c7
Brdy	Brdy	k1gInPc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Líšnice	Líšnice	k1gFnSc2
a	a	k8xC
Trnová	trnový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Petrovice	Petrovice	k1gFnSc1
u	u	k7c2
Sedlčan	Sedlčany	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Obděnice	Obděnice	k1gFnSc2
a	a	k8xC
Nechvalice	Nechvalice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
Příbram	Příbram	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
sloučena	sloučen	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Višňová	višňový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
Příbram	Příbram	k1gFnSc1
-	-	kIx~
Svatá	svatý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
Příbram	Příbram	k1gFnSc4
VI	VI	kA
–	–	k?
Březové	březový	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
sloučena	sloučen	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Obecnice	obecnice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sedlčany	Sedlčany	k1gInPc4
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Vojkov	Vojkov	k1gInSc1
<g/>
,	,	kIx,
Dublovice	Dublovice	k1gFnSc1
<g/>
,	,	kIx,
Chlum	chlum	k1gInSc1
u	u	k7c2
Sedlčan	Sedlčany	k1gInPc2
<g/>
,	,	kIx,
Jesenice	Jesenice	k1gFnPc1
u	u	k7c2
Sedlčan	Sedlčany	k1gInPc2
<g/>
,	,	kIx,
Kosova	Kosův	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Počepice	Počepice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Starý	Starý	k1gMnSc1
Knín	Knín	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Borotice	Borotice	k1gFnSc2
a	a	k8xC
Živohošť	Živohošť	k1gFnSc2
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Svaté	svatý	k2eAgFnSc2d1
Pole	pole	k1gFnSc2
</s>
<s>
Vikariát	vikariát	k1gInSc1
Benešov	Benešov	k1gInSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
25	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
9	#num#	k4
je	on	k3xPp3gMnPc4
spravovaných	spravovaný	k2eAgNnPc2d1
přímo	přímo	k6eAd1
a	a	k8xC
16	#num#	k4
excurrendo	excurrendo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slučování	slučování	k1gNnSc1
farností	farnost	k1gFnPc2
je	být	k5eAaImIp3nS
očekáváno	očekávat	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
9	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
jáhni	jáhen	k1gMnPc1
a	a	k8xC
1	#num#	k4
pastorační	pastorační	k2eAgFnSc1d1
asistentka	asistentka	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
59	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
František	František	k1gMnSc1
Masařík	Masařík	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Benešov	Benešov	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
sloučena	sloučen	k2eAgFnSc1d1
farnosti	farnost	k1gFnSc2
Okrouhlice	Okrouhlice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Bystřice	Bystřice	k1gFnSc1
u	u	k7c2
Benešova	Benešov	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Chotýšany	Chotýšana	k1gFnPc1
<g/>
,	,	kIx,
Popovice	Popovice	k1gFnPc1
u	u	k7c2
Benešova	Benešov	k1gInSc2
<g/>
,	,	kIx,
Postupice	Postupice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Křečovice	Křečovice	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Bělice	bělice	k1gFnPc1
<g/>
,	,	kIx,
Maršovice	Maršovice	k1gFnPc1
<g/>
,	,	kIx,
Neveklov	Neveklov	k1gInSc1
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Poříčí	Poříčí	k1gNnSc2
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc4d1
farnosti	farnost	k1gFnPc4
<g/>
:	:	kIx,
Kozmice	Kozmika	k1gFnSc3
u	u	k7c2
Benešova	Benešov	k1gInSc2
a	a	k8xC
Vranov	Vranov	k1gInSc4
u	u	k7c2
Benešova	Benešov	k1gInSc2
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sedlec	Sedlec	k1gInSc1
-	-	kIx~
Prčice	Prčice	k1gFnSc1
(	(	kIx(
<g/>
web	web	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Arnoštovice	Arnoštovice	k1gFnPc1
a	a	k8xC
Červený	červený	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Týnec	Týnec	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Netvořice	Netvořice	k1gFnPc1
<g/>
,	,	kIx,
Václavice	Václavice	k1gFnPc1
u	u	k7c2
Benešova	Benešov	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Vysoký	vysoký	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
u	u	k7c2
Benešova	Benešov	k1gInSc2
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Votice	Votice	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Jankov	Jankov	k1gInSc1
<g/>
,	,	kIx,
Ouběnice	Ouběnice	k1gFnSc1
a	a	k8xC
Vrchotovy	Vrchotův	k2eAgFnPc1d1
Janovice	Janovice	k1gFnPc1
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vikariát	vikariát	k1gInSc1
Vlašim	Vlašim	k1gFnSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
9	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
8	#num#	k4
spravovaných	spravovaný	k2eAgInPc2d1
přímo	přímo	k6eAd1
a	a	k8xC
1	#num#	k4
excurrendo	excurrendo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
10	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
jáhen	jáhen	k1gMnSc1
a	a	k8xC
1	#num#	k4
pastorační	pastorační	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
<g/>
.	.	kIx.
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
69	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
sloučením	sloučení	k1gNnSc7
farností	farnost	k1gFnPc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2008	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
vikariátu	vikariát	k1gInSc6
28	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
11	#num#	k4
kněží	kněz	k1gMnPc2
a	a	k8xC
72	#num#	k4
kostelů	kostel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloučení	sloučení	k1gNnSc1
farností	farnost	k1gFnPc2
proběhlo	proběhnout	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Konečný	Konečný	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Čechtice	Čechtice	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Borovnice	borovnice	k1gFnSc2
<g/>
,	,	kIx,
Křivsoudov	Křivsoudov	k1gInSc4
a	a	k8xC
Zhoř	Zhoř	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Divišov	Divišov	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Třebešice	Třebešice	k1gFnSc2
a	a	k8xC
Zdebuzeves	Zdebuzevesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Hrádek	hrádek	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
ve	v	k7c6
správě	správa	k1gFnSc6
Kongregace	kongregace	k1gFnSc2
kněží	kněz	k1gMnPc2
mariánů	marián	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Radošovice	Radošovice	k1gFnSc2
a	a	k8xC
Trhový	trhový	k2eAgInSc4d1
Štěpánov	Štěpánov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Keblov	Keblov	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Dolní	dolní	k2eAgFnPc1d1
Kralovice	Kralovice	k1gFnPc1
a	a	k8xC
Snět	snět	k1gInSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Louňovice	Louňovice	k1gFnSc2
pod	pod	k7c7
Blaníkem	Blaník	k1gInSc7
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnSc6
Kamberk	Kamberk	k1gInSc4
a	a	k8xC
Šlapánov	Šlapánov	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Veliš	Veliš	k1gInSc4
u	u	k7c2
Vlašimi	Vlašim	k1gFnSc2
<g/>
,	,	kIx,
spravuje	spravovat	k5eAaImIp3nS
excurrendo	excurrendo	k6eAd1
vlašimský	vlašimský	k2eAgMnSc1d1
kaplan	kaplan	k1gMnSc1
<g/>
,	,	kIx,
do	do	k7c2
budoucna	budoucno	k1gNnSc2
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
počítá	počítat	k5eAaImIp3nS
jako	jako	k9
s	s	k7c7
malou	malý	k2eAgFnSc7d1
farností	farnost	k1gFnSc7
pro	pro	k7c4
kněze	kněz	k1gMnPc4
se	s	k7c7
zdravotním	zdravotní	k2eAgNnSc7d1
omezením	omezení	k1gNnSc7
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Vlašim	Vlašim	k1gFnSc1
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Domašín	Domašína	k1gFnPc2
a	a	k8xC
Kondrac	Kondrac	k1gFnSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Zdislavice	Zdislavice	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Pravonín	Pravonína	k1gFnPc2
a	a	k8xC
Načeradec	Načeradec	k1gMnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Zruč	Zruč	k1gFnSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Kácov	Kácovo	k1gNnPc2
a	a	k8xC
Soutice	Soutice	k1gFnSc2
</s>
<s>
Farnosti	farnost	k1gFnSc3
Čestín	Čestín	k1gMnSc1
a	a	k8xC
Petrovice	Petrovice	k1gFnPc1
II	II	kA
byly	být	k5eAaImAgFnP
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
přičleněny	přičleněn	k2eAgFnPc1d1
do	do	k7c2
farností	farnost	k1gFnPc2
Uhlířské	uhlířský	k2eAgFnSc2d1
Janovice	Janovice	k1gFnPc1
ve	v	k7c6
vikariátu	vikariát	k1gInSc6
Kolín	Kolín	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vikariát	vikariát	k1gInSc1
Jílové	jílový	k2eAgFnSc2d1
</s>
<s>
Vikariát	vikariát	k1gInSc1
nemá	mít	k5eNaImIp3nS
přirozené	přirozený	k2eAgNnSc4d1
spádové	spádový	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
území	území	k1gNnSc4
leží	ležet	k5eAaImIp3nS
bezprostředně	bezprostředně	k6eAd1
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
východ	východ	k1gInSc4
od	od	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
jej	on	k3xPp3gMnSc4
tři	tři	k4xCgInPc1
výběžky	výběžek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
26	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
9	#num#	k4
spravovaných	spravovaný	k2eAgInPc2d1
přímo	přímo	k6eAd1
a	a	k8xC
17	#num#	k4
excurrendo	excurrendo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slučování	slučování	k1gNnSc1
farností	farnost	k1gFnPc2
dosud	dosud	k6eAd1
neproběhlo	proběhnout	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
15	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
jáhni	jáhen	k1gMnPc1
a	a	k8xC
2	#num#	k4
pastorační	pastorační	k2eAgMnPc1d1
asistenti	asistent	k1gMnPc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
79	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Miroslav	Miroslav	k1gMnSc1
Malý	Malý	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Čestlice	Čestlice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Hrusice	Hrusice	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Jílové	jílový	k2eAgFnSc2d1
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Dolní	dolní	k2eAgMnPc4d1
Jirčany	Jirčan	k1gMnPc4
a	a	k8xC
Pyšely	Pyšely	k1gInPc4
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Mnichovice	Mnichovice	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Chocerady	Chocerada	k1gFnSc2
a	a	k8xC
Ondřejov	Ondřejov	k1gInSc1
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Říčany	Říčany	k1gInPc1
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Jažlovice	Jažlovice	k1gFnPc1
<g/>
,	,	kIx,
Kostelec	Kostelec	k1gInSc1
u	u	k7c2
Křížků	křížek	k1gInPc2
<g/>
,	,	kIx,
Mukařov	Mukařov	k1gInSc1
<g/>
,	,	kIx,
Popovičky	Popovička	k1gFnPc1
<g/>
,	,	kIx,
Velké	velký	k2eAgFnPc1d1
Popovice	Popovice	k1gFnPc1
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sázava-Černé	Sázava-Černý	k2eAgFnSc2d1
Budy	Buda	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Rataje	Rataje	k1gInPc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
Stříbrná	stříbrný	k2eAgFnSc1d1
Skalice	Skalice	k1gFnSc1
<g/>
,	,	kIx,
Úžice	Úžice	k1gFnSc1
u	u	k7c2
Kutné	kutný	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Štěchovice	Štěchovice	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
sloučena	sloučen	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Slapy	slap	k1gInPc4
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Úvaly	úval	k1gInPc4
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Hradešín	Hradešín	k1gInSc1
<g/>
,	,	kIx,
Sluštice	Sluštice	k1gFnSc1
a	a	k8xC
Tuklaty	Tukle	k1gNnPc7
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Jirny	Jirny	k1gInPc1
byla	být	k5eAaImAgFnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
připojena	připojit	k5eAaPmNgFnS
do	do	k7c2
III	III	kA
<g/>
.	.	kIx.
pražského	pražský	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vikariát	vikariát	k1gInSc1
Kolín	Kolín	k1gInSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
5	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
jsou	být	k5eAaImIp3nP
přímo	přímo	k6eAd1
spravované	spravovaný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
sloučení	sloučení	k1gNnSc3
(	(	kIx(
<g/>
snížení	snížení	k1gNnSc3
počtu	počet	k1gInSc2
z	z	k7c2
37	#num#	k4
na	na	k7c4
5	#num#	k4
<g/>
)	)	kIx)
došlo	dojít	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
12	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
jáhni	jáhen	k1gMnPc1
a	a	k8xC
2	#num#	k4
pastorační	pastorační	k2eAgFnSc2d1
asistentky	asistentka	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
91	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Libor	Libor	k1gMnSc1
Bulín	Bulín	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Farnosti	farnost	k1gFnPc1
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
necelé	celý	k2eNgFnPc1d1
tři	tři	k4xCgFnPc4
farnosti	farnost	k1gFnPc4
z	z	k7c2
jílovského	jílovský	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
byly	být	k5eAaImAgFnP
připojeny	připojit	k5eAaPmNgFnP
ještě	ještě	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Dvě	dva	k4xCgFnPc1
farnosti	farnost	k1gFnPc1
z	z	k7c2
vikariátu	vikariát	k1gInSc2
Vlašim	Vlašim	k1gFnSc1
byly	být	k5eAaImAgFnP
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2008	#num#	k4
sloučeny	sloučen	k2eAgInPc4d1
s	s	k7c7
farností	farnost	k1gFnSc7
Uhlířské	uhlířský	k2eAgFnPc1d1
Janovice	Janovice	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Český	český	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Lstiboř	Lstiboř	k1gFnSc1
<g/>
,	,	kIx,
Přistoupim	Přistoupim	k?
<g/>
,	,	kIx,
Tismice	Tismika	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Kounice	Kounice	k1gFnSc2
<g/>
,	,	kIx,
Poříčany	Poříčan	k1gMnPc4
a	a	k8xC
obce	obec	k1gFnPc4
Bříství	Bříství	k1gNnPc2
a	a	k8xC
Starý	starý	k2eAgInSc1d1
Vestec	Vestec	k1gInSc1
ze	z	k7c2
zrušené	zrušený	k2eAgFnSc2d1
a	a	k8xC
rozdělené	rozdělený	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
Bříství	Bříství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Kolín	Kolín	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Křečhoř	Křečhoř	k1gFnSc1
<g/>
,	,	kIx,
Lošany	Lošan	k1gInPc1
<g/>
,	,	kIx,
Nebovidy	Nebovid	k1gInPc1
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
I	i	k9
<g/>
,	,	kIx,
Ovčáry	Ovčáry	k1gInPc1
<g/>
,	,	kIx,
Předhradí	předhradí	k1gNnPc1
u	u	k7c2
Nymburka	Nymburk	k1gInSc2
<g/>
,	,	kIx,
Ratboř	Ratboř	k1gFnSc1
<g/>
,	,	kIx,
Solopysky	Solopysek	k1gInPc1
<g/>
,	,	kIx,
Starý	starý	k2eAgInSc1d1
Kolín	Kolín	k1gInSc1
<g/>
,	,	kIx,
Suchdol	Suchdol	k1gInSc1
<g/>
,	,	kIx,
Velim	Velim	k?
<g/>
,	,	kIx,
Veltruby	Veltruba	k1gFnPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Černými	černý	k2eAgInPc7d1
Lesy	les	k1gInPc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Konojedy	Konojeda	k1gMnSc2
u	u	k7c2
Kostelce	Kostelec	k1gInSc2
nad	nad	k7c7
Černými	černý	k2eAgInPc7d1
lesy	les	k1gInPc7
<g/>
,	,	kIx,
Kouřim	Kouřim	k1gFnSc1
<g/>
,	,	kIx,
Malotice	Malotice	k1gFnSc1
<g/>
,	,	kIx,
Oleška	Oleška	k1gFnSc1
<g/>
,	,	kIx,
Svojšice	Svojšice	k1gFnSc1
u	u	k7c2
Kostelce	Kostelec	k1gInSc2
nad	nad	k7c7
Černými	černý	k2eAgInPc7d1
lesy	les	k1gInPc7
<g/>
,	,	kIx,
Vitice	Vitice	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Pečky	pečka	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Dobřichov	Dobřichov	k1gInSc1
<g/>
,	,	kIx,
Plaňany	Plaňan	k1gMnPc4
<g/>
,	,	kIx,
Ratenice	Ratenice	k1gFnPc4
<g/>
,	,	kIx,
Skramníky	Skramník	k1gMnPc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Uhlířské	uhlířský	k2eAgFnPc1d1
Janovice	Janovice	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Dolní	dolní	k2eAgFnPc1d1
Chvatliny	Chvatlina	k1gFnPc1
<g/>
,	,	kIx,
Drahobudice	Drahobudice	k1gFnPc1
<g/>
,	,	kIx,
Horní	horní	k2eAgFnPc1d1
Kruty	kruta	k1gFnPc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
<g/>
,	,	kIx,
Sudějov	Sudějov	k1gInSc1
<g/>
,	,	kIx,
Vavřinec	Vavřinec	k1gMnSc1
<g/>
,	,	kIx,
Zásmuky	Zásmuky	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Čestín	Čestína	k1gFnPc2
a	a	k8xC
Petrovice	Petrovice	k1gFnSc2
II	II	kA
<g/>
,	,	kIx,
původně	původně	k6eAd1
z	z	k7c2
vikariátu	vikariát	k1gInSc2
Vlašim	Vlašim	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vikariát	vikariát	k1gInSc1
Stará	starat	k5eAaImIp3nS
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
Ve	v	k7c6
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
6	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
jsou	být	k5eAaImIp3nP
přímo	přímo	k6eAd1
spravované	spravovaný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Michal	Michal	k1gMnSc1
Procházka	Procházka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
12	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
jáhen	jáhen	k1gMnSc1
a	a	k8xC
1	#num#	k4
pastorační	pastorační	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
50	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Farnosti	farnost	k1gFnPc1
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Benátky	Benátky	k1gFnPc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Hlavno	Hlavno	k1gNnSc1
<g/>
,	,	kIx,
Předměřice	Předměřice	k1gFnSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučen	k2eAgFnPc4d1
farnosti	farnost	k1gFnPc4
Dřevčice	Dřevčice	k1gFnPc4
<g/>
,	,	kIx,
Sluhy	sluha	k1gMnPc4
<g/>
,	,	kIx,
Svémyslice	Svémyslice	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Čelákovice	Čelákovice	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Nehvizdy	Nehvizda	k1gFnSc2
<g/>
,	,	kIx,
Přerov	Přerov	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Vyšehořovice	Vyšehořovice	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Lysá	lysat	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Kostomlaty	Kostomle	k1gNnPc7
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Milovice	Milovice	k1gFnPc1
u	u	k7c2
Lysé	Lysá	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Nymburk	Nymburk	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnPc1
Chleby	chléb	k1gInPc4
<g/>
,	,	kIx,
Sadská	sadský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Veleliby	Veleliba	k1gFnPc1
<g/>
,	,	kIx,
Všejany	Všejana	k1gFnPc1
a	a	k8xC
obec	obec	k1gFnSc1
Velenka	Velenka	k1gFnSc1
ze	z	k7c2
zrušené	zrušený	k2eAgFnSc2d1
a	a	k8xC
rozdělené	rozdělený	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
Bříství	Bříství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Stará	starat	k5eAaImIp3nS
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
do	do	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
farnosti	farnost	k1gFnSc2
Kostelec	Kostelec	k1gInSc4
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Kozly	Kozel	k1gMnPc4
<g/>
,	,	kIx,
Skorkov	Skorkov	k1gInSc1
<g/>
,	,	kIx,
Všetaty	Všetat	k1gInPc1
</s>
<s>
Farnosti	farnost	k1gFnPc1
Klecany	Klecana	k1gFnSc2
<g/>
,	,	kIx,
Líbeznice	Líbeznice	k1gFnSc1
a	a	k8xC
Odolena	Odolena	k1gFnSc1
Voda	voda	k1gFnSc1
byly	být	k5eAaImAgFnP
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
sloučeny	sloučen	k2eAgInPc1d1
a	a	k8xC
převedeny	převést	k5eAaPmNgFnP
do	do	k7c2
IV	IV	kA
<g/>
.	.	kIx.
pražského	pražský	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
Arcidiecéze	arcidiecéze	k1gFnSc2
pražské	pražský	k2eAgNnSc4d1
Archivováno	archivován	k2eAgNnSc4d1
22	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
arcibiskupství	arcibiskupství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
září	září	k1gNnSc2
2006	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
2	#num#	k4
3	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
pražské	pražský	k2eAgFnPc4d1
arcidiecéze	arcidiecéze	k1gFnPc4
<g/>
,	,	kIx,
leden	leden	k1gInSc1
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
2	#num#	k4
3	#num#	k4
Obnova	obnova	k1gFnSc1
farností	farnost	k1gFnPc2
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
20061	#num#	k4
2	#num#	k4
Informace	informace	k1gFnSc1
pro	pro	k7c4
věřící	věřící	k1gFnSc4
-	-	kIx~
pastýřský	pastýřský	k2eAgInSc1d1
list	list	k1gInSc1
kardinála	kardinál	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Vlka	Vlk	k1gMnSc2
(	(	kIx(
<g/>
ke	k	k7c3
čtení	čtení	k1gNnSc3
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
20041	#num#	k4
2	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Sládek	Sládek	k1gMnSc1
<g/>
:	:	kIx,
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
kardinálem	kardinál	k1gMnSc7
Miloslavem	Miloslav	k1gMnSc7
Vlkem	Vlk	k1gMnSc7
o	o	k7c6
slučování	slučování	k1gNnSc6
farností	farnost	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
7	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Kolín	Kolín	k1gInSc1
<g/>
,	,	kIx,
nedatováno	datován	k2eNgNnSc1d1
<g/>
,	,	kIx,
cca	cca	kA
20041	#num#	k4
2	#num#	k4
Jeroným	Jeroným	k1gMnSc1
Klimeš	Klimeš	k1gMnSc1
<g/>
:	:	kIx,
Zrušení	zrušení	k1gNnSc1
farnosti	farnost	k1gFnSc2
Líbeznice	Líbeznice	k1gFnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
↑	↑	k?
Vikariáty	vikariát	k1gInPc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známe	znát	k5eAaImIp1nP
je	on	k3xPp3gMnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
I.	I.	kA
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
)	)	kIx)
Zpravodaj	zpravodaj	k1gInSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
↑	↑	k?
I.	I.	kA
pražský	pražský	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc4
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgNnSc2d1
<g/>
1	#num#	k4
2	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2004	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2003	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
2	#num#	k4
3	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
leden	leden	k1gInSc1
2004	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
2004	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc1
2004	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2006	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
↑	↑	k?
Znáte	znát	k5eAaImIp2nP
II	II	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
↑	↑	k?
II	II	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Acta	Act	k1gInSc2
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
č.	č.	k?
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
61	#num#	k4
2	#num#	k4
3	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2006	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
41	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2006	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
↑	↑	k?
https://katalog.apha.cz/web/catalog_detail.php?sablona_soubor=detail/osoby_detail.xml&	https://katalog.apha.cz/web/catalog_detail.php?sablona_soubor=detail/osoby_detail.xml&	k?
<g/>
↑	↑	k?
Znáte	znát	k5eAaImIp2nP
III	III	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
III	III	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
IV	IV	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gInSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
IV	IV	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gInSc4
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
leden	leden	k1gInSc1
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Podřipsko	Podřipsko	k1gNnSc4
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Podřipsko	Podřipsko	k1gNnSc1
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Acta	Act	k1gInSc2
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
12	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
12	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
9	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
12	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
13	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2010	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
↑	↑	k?
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Rakovník	Rakovník	k1gInSc1
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gInSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Rakovník	Rakovník	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Arcibiskupství	arcibiskupství	k1gNnSc1
pražského	pražský	k2eAgNnSc2d1
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Beroun	Beroun	k1gInSc1
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gInSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Beroun	Beroun	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gInSc4
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2007	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Příbram	Příbram	k1gFnSc1
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Příbram	Příbram	k1gFnSc1
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Acta	Act	k1gInSc2
Curiae	Curia	k1gMnSc2
Archiepiscopalis	Archiepiscopalis	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Pragensis	Pragensis	k1gInSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
25	#num#	k4
<g/>
↑	↑	k?
Acta	Act	k2eAgFnSc1d1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Pragensis	Pragensis	k1gInSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
26	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
↑	↑	k?
Acta	Act	k2eAgFnSc1d1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Pragensis	Pragensis	k1gInSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
28	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
↑	↑	k?
Acta	Act	k2eAgFnSc1d1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Pragensis	Pragensis	k1gFnPc2
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc4
pražské	pražský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
30	#num#	k4
<g/>
↑	↑	k?
Acta	Actus	k1gMnSc4
Curiae	Curia	k1gFnSc2
Archiepiscopalis	Archiepiscopalis	k1gFnSc2
Pragensis	Pragensis	k1gFnSc2
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc4
pražské	pražský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
32	#num#	k4
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Benešov	Benešov	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Benešov	Benešov	k1gInSc1
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Acta	Act	k1gInSc2
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc4
pražské	pražský	k2eAgFnSc2d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc4
pražské	pražský	k2eAgFnSc2d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
8	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc4
pražské	pražský	k2eAgFnSc2d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
11	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
12	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc4
pražské	pražský	k2eAgFnSc2d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
141	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
↑	↑	k?
Vikrariáty	Vikrariáta	k1gFnSc2
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známe	znát	k5eAaImIp1nP
je	on	k3xPp3gMnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vikariát	vikariát	k1gInSc1
Vlašim	Vlašim	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravodaj	zpravodaj	k1gInSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Vlašim	Vlašim	k1gFnSc1
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Jílové	jílový	k2eAgFnPc1d1
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Jílové	Jílové	k1gNnSc1
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Acta	Act	k1gInSc2
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
15	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
17	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
20	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
21	#num#	k4
<g/>
↑	↑	k?
Acta	Acta	k1gFnSc1
Curiae	Curiae	k1gFnSc1
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
;	;	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
22	#num#	k4
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Kolín	Kolín	k1gInSc1
Archivováno	archivován	k2eAgNnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Kolín	Kolín	k1gInSc1
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc1
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgInSc2d1
<g/>
↑	↑	k?
Zpravodaj	zpravodaj	k1gInSc4
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
3-41	3-41	k4
2	#num#	k4
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2006	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
↑	↑	k?
JANEČEK	Janeček	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farnost	farnost	k1gFnSc1
Pečky	pečka	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečky	Pečky	k1gFnPc1
<g/>
:	:	kIx,
Farnost	farnost	k1gFnSc1
Pečky	pečka	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Představujeme	představovat	k5eAaImIp1nP
vikariát	vikariát	k1gInSc4
Stará	starat	k5eAaImIp3nS
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Vikariát	vikariát	k1gInSc1
Stará	Stará	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
katalog	katalog	k1gInSc4
Arcibiskupství	arcibiskupství	k1gNnSc2
pražského	pražský	k2eAgNnSc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
Arcidiecéze	arcidiecéze	k1gFnSc2
pražské	pražský	k2eAgFnSc2d1
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
arcibiskupství	arcibiskupství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vikariáty	vikariát	k1gInPc1
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
</s>
<s>
I.	I.	kA
pražský	pražský	k2eAgInSc1d1
•	•	k?
II	II	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgMnSc1d1
•	•	k?
III	III	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgMnSc1d1
•	•	k?
IV	IV	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgMnSc1d1
•	•	k?
benešovský	benešovský	k2eAgInSc1d1
•	•	k?
berounský	berounský	k2eAgInSc1d1
•	•	k?
jílovský	jílovský	k2eAgInSc1d1
•	•	k?
kladenský	kladenský	k2eAgInSc1d1
•	•	k?
kolínský	kolínský	k2eAgInSc1d1
•	•	k?
podřipský	podřipský	k2eAgInSc1d1
•	•	k?
příbramský	příbramský	k2eAgInSc1d1
•	•	k?
rakovnický	rakovnický	k2eAgInSc1d1
•	•	k?
staroboleslavský	staroboleslavský	k2eAgInSc1d1
•	•	k?
vlašimský	vlašimský	k2eAgInSc4d1
</s>
