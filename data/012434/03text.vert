<p>
<s>
Schengenský	schengenský	k2eAgInSc1d1	schengenský
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SIS	SIS	kA	SIS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
databázový	databázový	k2eAgInSc1d1	databázový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
provozují	provozovat	k5eAaImIp3nP	provozovat
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
Schengenské	schengenský	k2eAgFnSc2d1	Schengenská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
nemohly	moct	k5eNaImAgFnP	moct
být	být	k5eAaImF	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
hranice	hranice	k1gFnPc1	hranice
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
Schengenského	schengenský	k2eAgInSc2d1	schengenský
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
existence	existence	k1gFnSc2	existence
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
verze	verze	k1gFnSc1	verze
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
SIS	SIS	kA	SIS
I.	I.	kA	I.
Před	před	k7c7	před
rozšířením	rozšíření	k1gNnSc7	rozšíření
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
bez	bez	k7c2	bez
úprav	úprava	k1gFnPc2	úprava
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
25	[number]	k4	25
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgFnPc1d1	nová
členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
přistoupí	přistoupit	k5eAaPmIp3nP	přistoupit
k	k	k7c3	k
Schengenskému	schengenský	k2eAgInSc3d1	schengenský
prostoru	prostor	k1gInSc3	prostor
až	až	k9	až
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
verze	verze	k1gFnSc2	verze
SIS	SIS	kA	SIS
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
říjen	říjen	k1gInSc4	říjen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
nepodaří	podařit	k5eNaPmIp3nS	podařit
do	do	k7c2	do
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
data	datum	k1gNnSc2	datum
dokončit	dokončit	k5eAaPmF	dokončit
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
spustí	spustit	k5eAaPmIp3nS	spustit
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
unie	unie	k1gFnSc2	unie
počítaly	počítat	k5eAaImAgInP	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
odloží	odložit	k5eAaPmIp3nS	odložit
i	i	k9	i
připojení	připojení	k1gNnSc4	připojení
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
do	do	k7c2	do
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
novým	nový	k2eAgInPc3d1	nový
členským	členský	k2eAgInPc3d1	členský
státům	stát	k1gInPc3	stát
zásadně	zásadně	k6eAd1	zásadně
nelíbilo	líbit	k5eNaImAgNnS	líbit
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
přijat	přijmout	k5eAaPmNgInS	přijmout
kompromisní	kompromisní	k2eAgInSc1d1	kompromisní
návrh	návrh	k1gInSc1	návrh
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
původní	původní	k2eAgInSc1d1	původní
systém	systém	k1gInSc1	systém
modernizuje	modernizovat	k5eAaBmIp3nS	modernizovat
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
název	název	k1gInSc4	název
SISone	SISon	k1gInSc5	SISon
<g/>
4	[number]	k4	4
<g/>
all	all	k?	all
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
přistoupení	přistoupení	k1gNnSc2	přistoupení
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
odsunul	odsunout	k5eAaPmAgInS	odsunout
jen	jen	k9	jen
o	o	k7c4	o
čtvrt	čtvrt	k1gFnSc4	čtvrt
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
SIS	SIS	kA	SIS
I	I	kA	I
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
systém	systém	k1gInSc1	systém
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jména	jméno	k1gNnPc4	jméno
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
základní	základní	k2eAgInPc4d1	základní
identifikační	identifikační	k2eAgInPc4d1	identifikační
údaje	údaj	k1gInPc4	údaj
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
překračujících	překračující	k2eAgFnPc2d1	překračující
hranice	hranice	k1gFnSc2	hranice
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
a	a	k8xC	a
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
ozbrojeny	ozbrojit	k5eAaPmNgFnP	ozbrojit
<g/>
,	,	kIx,	,
páchaly	páchat	k5eAaImAgFnP	páchat
násilnosti	násilnost	k1gFnPc4	násilnost
apod.	apod.	kA	apod.
Dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
např.	např.	kA	např.
přehled	přehled	k1gInSc4	přehled
ztracených	ztracený	k2eAgInPc2d1	ztracený
cestovních	cestovní	k2eAgInPc2d1	cestovní
dokladů	doklad	k1gInPc2	doklad
nebo	nebo	k8xC	nebo
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
verze	verze	k1gFnSc1	verze
systému	systém	k1gInSc2	systém
SIS	SIS	kA	SIS
I	I	kA	I
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
SISone	SISon	k1gInSc5	SISon
<g/>
4	[number]	k4	4
<g/>
all	all	k?	all
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Schengenský	schengenský	k2eAgInSc1d1	schengenský
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
systému	systém	k1gInSc3	systém
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
devět	devět	k4xCc4	devět
nových	nový	k2eAgInPc2d1	nový
členů	člen	k1gInPc2	člen
EU	EU	kA	EU
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
SIS	SIS	kA	SIS
II	II	kA	II
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
biometrické	biometrický	k2eAgInPc4d1	biometrický
údaje	údaj	k1gInPc4	údaj
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fotografie	fotografia	k1gFnSc2	fotografia
nebo	nebo	k8xC	nebo
otisky	otisk	k1gInPc1	otisk
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
a	a	k8xC	a
podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
odcizených	odcizený	k2eAgNnPc6d1	odcizené
vozidlech	vozidlo	k1gNnPc6	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
SIRENE	SIRENE	kA	SIRENE
==	==	k?	==
</s>
</p>
<p>
<s>
SIRENE	SIRENE	kA	SIRENE
(	(	kIx(	(
<g/>
Supplementary	Supplementara	k1gFnSc2	Supplementara
Information	Information	k1gInSc1	Information
Request	Request	k1gMnSc1	Request
at	at	k?	at
the	the	k?	the
National	National	k1gMnSc2	National
Entry	Entra	k1gMnSc2	Entra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
určená	určený	k2eAgNnPc4d1	určené
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
jako	jako	k8xS	jako
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
obsahové	obsahový	k2eAgFnSc6d1	obsahová
stránce	stránka	k1gFnSc6	stránka
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
za	za	k7c4	za
národní	národní	k2eAgInPc4d1	národní
údaje	údaj	k1gInPc4	údaj
vkládané	vkládaný	k2eAgInPc4d1	vkládaný
do	do	k7c2	do
Schengenského	schengenský	k2eAgInSc2d1	schengenský
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Plní	plnit	k5eAaImIp3nS	plnit
všechny	všechen	k3xTgInPc4	všechen
úkoly	úkol	k1gInPc4	úkol
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
získávání	získávání	k1gNnSc2	získávání
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
toku	toka	k1gFnSc4	toka
a	a	k8xC	a
koordinace	koordinace	k1gFnPc4	koordinace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
pátráním	pátrání	k1gNnSc7	pátrání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kontaktním	kontaktní	k2eAgNnSc7d1	kontaktní
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
národní	národní	k2eAgFnPc4d1	národní
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
národní	národní	k2eAgFnSc4d1	národní
kancelář	kancelář	k1gFnSc4	kancelář
SIRENE	SIRENE	kA	SIRENE
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přímo	přímo	k6eAd1	přímo
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
národními	národní	k2eAgFnPc7d1	národní
kancelářemi	kancelář	k1gFnPc7	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
těchto	tento	k3xDgFnPc2	tento
kanceláří	kancelář	k1gFnPc2	kancelář
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
"	"	kIx"	"
<g/>
Příručce	příručka	k1gFnSc6	příručka
SIRENE	SIRENE	kA	SIRENE
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
řekl	říct	k5eAaPmAgMnS	říct
šéf	šéf	k1gMnSc1	šéf
německých	německý	k2eAgInPc2d1	německý
policejních	policejní	k2eAgInPc2d1	policejní
odborů	odbor	k1gInPc2	odbor
Rainer	Rainer	k1gMnSc1	Rainer
Wendt	Wendt	k1gMnSc1	Wendt
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
zaneseni	zanést	k5eAaPmNgMnP	zanést
do	do	k7c2	do
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
německé	německý	k2eAgFnSc2d1	německá
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
schengenském	schengenský	k2eAgInSc6d1	schengenský
informačním	informační	k2eAgInSc6d1	informační
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Schengenská	schengenský	k2eAgFnSc1d1	Schengenská
smlouva	smlouva	k1gFnSc1	smlouva
</s>
</p>
<p>
<s>
Schengenský	schengenský	k2eAgInSc1d1	schengenský
prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Schengenský	schengenský	k2eAgInSc1d1	schengenský
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
–	–	k?	–
technický	technický	k2eAgInSc1d1	technický
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
webu	web	k1gInSc6	web
Integrace	integrace	k1gFnSc1	integrace
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
