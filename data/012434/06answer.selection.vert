<s>
Schengenský	schengenský	k2eAgInSc1d1	schengenský
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SIS	SIS	kA	SIS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
databázový	databázový	k2eAgInSc1d1	databázový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
provozují	provozovat	k5eAaImIp3nP	provozovat
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
Schengenské	schengenský	k2eAgFnSc2d1	Schengenská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
