<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Metternichova	Metternichův	k2eAgInSc2d1	Metternichův
absolutismu	absolutismus	k1gInSc2	absolutismus
(	(	kIx(	(
<g/>
revoluční	revoluční	k2eAgInSc1d1	revoluční
rok	rok	k1gInSc1	rok
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevila	objevit	k5eAaPmAgFnS	objevit
mladá	mladý	k2eAgFnSc1d1	mladá
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaobírala	zaobírat	k5eAaImAgFnS	zaobírat
problematikou	problematika	k1gFnSc7	problematika
městského	městský	k2eAgInSc2d1	městský
života	život	k1gInSc2	život
a	a	k8xC	a
současných	současný	k2eAgInPc2d1	současný
sociálních	sociální	k2eAgInPc2d1	sociální
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
