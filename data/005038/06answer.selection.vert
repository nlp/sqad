<s>
Hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gInSc1	legomenon
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
ἅ	ἅ	k?	ἅ
λ	λ	k?	λ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jedinkrát	jedinkrát	k6eAd1	jedinkrát
řečené	řečený	k2eAgInPc4d1	řečený
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
hapax	hapax	k1gInSc4	hapax
znamená	znamenat	k5eAaImIp3nS	znamenat
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
textovém	textový	k2eAgInSc6d1	textový
souboru	soubor	k1gInSc6	soubor
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
