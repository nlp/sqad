<s>
Hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gInSc1	legomenon
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
ἅ	ἅ	k?	ἅ
λ	λ	k?	λ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jedinkrát	jedinkrát	k6eAd1	jedinkrát
řečené	řečený	k2eAgInPc4d1	řečený
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
hapax	hapax	k1gInSc4	hapax
znamená	znamenat	k5eAaImIp3nS	znamenat
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
textovém	textový	k2eAgInSc6d1	textový
souboru	soubor	k1gInSc6	soubor
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Textovým	textový	k2eAgInSc7d1	textový
souborem	soubor	k1gInSc7	soubor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
veškeré	veškerý	k3xTgFnPc4	veškerý
písemnosti	písemnost	k1gFnPc4	písemnost
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
spisy	spis	k1gInPc1	spis
určitého	určitý	k2eAgMnSc2d1	určitý
autora	autor	k1gMnSc2	autor
apod.	apod.	kA	apod.
Hapax	hapax	k1gInSc1	hapax
legomena	legomen	k2eAgNnPc1d1	legomen
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
obtížně	obtížně	k6eAd1	obtížně
přeložitelná	přeložitelný	k2eAgFnSc1d1	přeložitelná
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
literárních	literární	k2eAgFnPc6d1	literární
památkách	památka	k1gFnPc6	památka
určitého	určitý	k2eAgInSc2d1	určitý
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
mrtvých	mrtvý	k1gMnPc2	mrtvý
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c4	na
zachované	zachovaný	k2eAgFnPc4d1	zachovaná
literární	literární	k2eAgFnPc4d1	literární
památky	památka	k1gFnPc4	památka
a	a	k8xC	a
významy	význam	k1gInPc1	význam
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dají	dát	k5eAaPmIp3nP	dát
určovat	určovat	k5eAaImF	určovat
jen	jen	k9	jen
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInPc2	jejich
různých	různý	k2eAgInPc2d1	různý
kontextů	kontext	k1gInPc2	kontext
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
podle	podle	k7c2	podle
etymologie	etymologie	k1gFnSc2	etymologie
a	a	k8xC	a
slov	slovo	k1gNnPc2	slovo
podobných	podobný	k2eAgMnPc2d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgInPc1d1	důležitý
například	například	k6eAd1	například
v	v	k7c6	v
biblistice	biblistika	k1gFnSc6	biblistika
<g/>
,	,	kIx,	,
při	při	k7c6	při
textové	textový	k2eAgFnSc6d1	textová
kritice	kritika	k1gFnSc6	kritika
a	a	k8xC	a
překladech	překlad	k1gInPc6	překlad
biblických	biblický	k2eAgInPc2d1	biblický
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gInSc4	legomenon
označuje	označovat	k5eAaImIp3nS	označovat
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
slovní	slovní	k2eAgInSc1d1	slovní
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
textu	text	k1gInSc6	text
(	(	kIx(	(
<g/>
v	v	k7c6	v
mluveném	mluvený	k2eAgInSc6d1	mluvený
či	či	k8xC	či
psaném	psaný	k2eAgInSc6d1	psaný
projevu	projev	k1gInSc6	projev
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
jedenkrát	jedenkrát	k6eAd1	jedenkrát
<g/>
.	.	kIx.	.
</s>
<s>
Hapax	hapax	k1gInSc1	hapax
legomena	legomen	k2eAgFnSc1d1	legomen
se	s	k7c7	s
–	–	k?	–
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
nedostatečným	dostatečný	k2eNgInPc3d1	nedostatečný
dokladům	doklad	k1gInPc3	doklad
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
původu	původ	k1gInSc6	původ
–	–	k?	–
zpravidla	zpravidla	k6eAd1	zpravidla
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
ani	ani	k8xC	ani
ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc1d3	nejčastější
výskyt	výskyt	k1gInSc1	výskyt
hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gInSc1	legomenon
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
zejména	zejména	k9	zejména
s	s	k7c7	s
malokorpusovými	malokorpusový	k2eAgInPc7d1	malokorpusový
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
však	však	k9	však
doložitelný	doložitelný	k2eAgMnSc1d1	doložitelný
i	i	k8xC	i
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
slovního	slovní	k2eAgNnSc2d1	slovní
spojení	spojení	k1gNnSc2	spojení
hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gMnSc1	legomenon
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
ještě	ještě	k9	ještě
s	s	k7c7	s
pojmy	pojem	k1gInPc7	pojem
jako	jako	k8xS	jako
dis	dis	k1gNnSc1	dis
legomenon	legomenona	k1gFnPc2	legomenona
<g/>
,	,	kIx,	,
tris	tris	k6eAd1	tris
legomenon	legomenon	k1gInSc1	legomenon
a	a	k8xC	a
tetrakis	tetrakis	k1gFnSc1	tetrakis
legomenon	legomenona	k1gFnPc2	legomenona
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
označení	označení	k1gNnPc1	označení
(	(	kIx(	(
<g/>
dis	dis	k1gNnSc1	dis
<g/>
,	,	kIx,	,
tris	tris	k1gInSc1	tris
<g/>
,	,	kIx,	,
tetrakis	tetrakis	k1gInSc1	tetrakis
<g/>
)	)	kIx)	)
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
četnosti	četnost	k1gFnPc4	četnost
výskytu	výskyt	k1gInSc2	výskyt
daného	daný	k2eAgNnSc2d1	dané
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
násobná	násobný	k2eAgNnPc1d1	násobné
označení	označení	k1gNnPc1	označení
nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
tolik	tolik	k6eAd1	tolik
užívána	užívat	k5eAaImNgFnS	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
následující	následující	k2eAgInPc1d1	následující
faktory	faktor	k1gInPc1	faktor
<g/>
:	:	kIx,	:
délka	délka	k1gFnSc1	délka
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
tematická	tematický	k2eAgFnSc1d1	tematická
bohatost	bohatost	k1gFnSc1	bohatost
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
určen	určit	k5eAaPmNgInS	určit
<g/>
,	,	kIx,	,
a	a	k8xC	a
časové	časový	k2eAgNnSc4d1	časové
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
příklady	příklad	k1gInPc4	příklad
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
uvádí	uvádět	k5eAaImIp3nS	uvádět
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
slovník	slovník	k1gInSc1	slovník
češtiny	čeština	k1gFnSc2	čeština
např.	např.	kA	např.
výrazy	výraz	k1gInPc4	výraz
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
ze	z	k7c2	z
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
<g/>
)	)	kIx)	)
povslún	povslún	k1gInSc1	povslún
<g/>
,	,	kIx,	,
posvázanie	posvázanie	k1gFnSc1	posvázanie
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
ze	z	k7c2	z
slovníků	slovník	k1gInPc2	slovník
Klaretovy	Klaretův	k2eAgFnSc2d1	Klaretova
družiny	družina	k1gFnSc2	družina
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
posvěcen	posvěcen	k2eAgInSc1d1	posvěcen
<g/>
/	/	kIx~	/
<g/>
posviecan	posviecan	k1gInSc1	posviecan
<g/>
,	,	kIx,	,
požlát	požlát	k1gInSc1	požlát
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
z	z	k7c2	z
Rukopisu	rukopis	k1gInSc2	rukopis
královédvorského	královédvorský	k2eAgInSc2d1	královédvorský
<g/>
)	)	kIx)	)
pietný	pietný	k2eAgInSc4d1	pietný
<g/>
,	,	kIx,	,
jarohlavý	jarohlavý	k2eAgInSc4d1	jarohlavý
<g/>
,	,	kIx,	,
dlúhopustý	dlúhopustý	k2eAgInSc4d1	dlúhopustý
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gNnSc1	legomenon
je	být	k5eAaImIp3nS	být
transliterací	transliterace	k1gFnSc7	transliterace
řeckého	řecký	k2eAgInSc2d1	řecký
ἅ	ἅ	k?	ἅ
λ	λ	k?	λ
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
řečený	řečený	k2eAgMnSc1d1	řečený
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gInSc1	legomenon
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
obtížněji	obtížně	k6eAd2	obtížně
zjistitelný	zjistitelný	k2eAgMnSc1d1	zjistitelný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
máme	mít	k5eAaImIp1nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jen	jen	k6eAd1	jen
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
textů	text	k1gInPc2	text
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgNnPc4	některý
slova	slovo	k1gNnPc4	slovo
nacházející	nacházející	k2eAgFnPc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
považujeme	považovat	k5eAaImIp1nP	považovat
za	za	k7c4	za
hapaxy	hapax	k1gInPc4	hapax
nenáležitě	náležitě	k6eNd1	náležitě
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
evidence	evidence	k1gFnSc2	evidence
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
existuje	existovat	k5eAaImIp3nS	existovat
např.	např.	kA	např.
spousta	spousta	k1gFnSc1	spousta
nerozluštěných	rozluštěný	k2eNgNnPc2d1	nerozluštěné
hebrejských	hebrejský	k2eAgNnPc2d1	hebrejské
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
výskyt	výskyt	k1gInSc1	výskyt
hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gInSc1	legomenon
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Biblí	bible	k1gFnSc7	bible
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
slov	slovo	k1gNnPc2	slovo
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
přímá	přímý	k2eAgNnPc4d1	přímé
vyjádření	vyjádření	k1gNnPc4	vyjádření
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgMnSc1d1	legomen
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
hapax	hapax	k1gInSc1	hapax
legomenon	legomenona	k1gFnPc2	legomenona
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
autorství	autorství	k1gNnSc2	autorství
psaných	psaný	k2eAgNnPc2d1	psané
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
výskyt	výskyt	k1gInSc1	výskyt
hapaxů	hapax	k1gInPc2	hapax
bývá	bývat	k5eAaImIp3nS	bývat
spjat	spjat	k2eAgInSc1d1	spjat
s	s	k7c7	s
určitým	určitý	k2eAgNnSc7d1	určité
dílem	dílo	k1gNnSc7	dílo
či	či	k8xC	či
částí	část	k1gFnSc7	část
autorské	autorský	k2eAgFnSc2d1	autorská
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dramatická	dramatický	k2eAgFnSc1d1	dramatická
tvorba	tvorba	k1gFnSc1	tvorba
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
obsahující	obsahující	k2eAgInSc4d1	obsahující
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
své	svůj	k3xOyFgFnSc6	svůj
šíři	šíř	k1gFnSc6	šíř
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgNnSc4d1	stejné
procentuální	procentuální	k2eAgNnSc4d1	procentuální
zastoupení	zastoupení	k1gNnSc4	zastoupení
hapax	hapax	k1gInSc4	hapax
legomen	legomen	k2eAgInSc4d1	legomen
<g/>
.	.	kIx.	.
</s>
<s>
Hapax	hapax	k1gInSc1	hapax
legomena	legomen	k2eAgNnPc1d1	legomen
nejsou	být	k5eNaImIp3nP	být
jen	jen	k9	jen
jakousi	jakýsi	k3yIgFnSc7	jakýsi
statistickou	statistický	k2eAgFnSc7d1	statistická
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
snadno	snadno	k6eAd1	snadno
určována	určovat	k5eAaImNgFnS	určovat
pomocí	pomocí	k7c2	pomocí
matematických	matematický	k2eAgNnPc2d1	matematické
pravidel	pravidlo	k1gNnPc2	pravidlo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
fenomén	fenomén	k1gInSc1	fenomén
hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gInSc1	legomenon
existuje	existovat	k5eAaImIp3nS	existovat
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
se	s	k7c7	s
Zipfovým	Zipfův	k2eAgInSc7d1	Zipfův
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
relativní	relativní	k2eAgNnSc4d1	relativní
pořadí	pořadí	k1gNnSc4	pořadí
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
textu	text	k1gInSc6	text
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Čili	čili	k8xC	čili
platí	platit	k5eAaImIp3nS	platit
r	r	kA	r
<g/>
*	*	kIx~	*
<g/>
f	f	k?	f
=	=	kIx~	=
c	c	k0	c
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc4	slovo
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
četností	četnost	k1gFnSc7	četnost
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
textu	text	k1gInSc6	text
dvakrát	dvakrát	k6eAd1	dvakrát
častěji	často	k6eAd2	často
než	než	k8xS	než
druhé	druhý	k4xOgNnSc1	druhý
nejčastější	častý	k2eAgNnSc1d3	nejčastější
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
častěji	často	k6eAd2	často
než	než	k8xS	než
třetí	třetí	k4xOgInSc4	třetí
<g/>
,	,	kIx,	,
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
častěji	často	k6eAd2	často
než	než	k8xS	než
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
by	by	kYmCp3nS	by
takto	takto	k6eAd1	takto
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
donekonečna	donekonečna	k6eAd1	donekonečna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tvaru	tvar	k1gInSc6	tvar
křivky	křivka	k1gFnSc2	křivka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
Zipfovým	Zipfův	k2eAgInSc7d1	Zipfův
zákonem	zákon	k1gInSc7	zákon
popisována	popisován	k2eAgFnSc1d1	popisována
<g/>
,	,	kIx,	,
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
korpusu	korpus	k1gInSc6	korpus
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
právě	právě	k6eAd1	právě
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
hapax	hapax	k1gInSc4	hapax
legomena	legomit	k5eAaPmNgFnS	legomit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
výskyt	výskyt	k1gInSc1	výskyt
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
hapaxu	hapax	k1gInSc2	hapax
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
textu	text	k1gInSc6	text
nelze	lze	k6eNd1	lze
očekávat	očekávat	k5eAaImF	očekávat
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
výskytu	výskyt	k1gInSc2	výskyt
blíže	blíž	k1gFnSc2	blíž
neurčeného	určený	k2eNgInSc2d1	neurčený
hapax	hapax	k1gInSc1	hapax
legomena	legomen	k2eAgFnSc1d1	legomen
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
typem	typ	k1gInSc7	typ
jazyka	jazyk	k1gInSc2	jazyk
<g/>
;	;	kIx,	;
jazyky	jazyk	k1gInPc1	jazyk
syntetické	syntetický	k2eAgInPc1d1	syntetický
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
než	než	k8xS	než
jazyky	jazyk	k1gInPc1	jazyk
analytické	analytický	k2eAgInPc1d1	analytický
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
analytické	analytický	k2eAgInPc1d1	analytický
jazyky	jazyk	k1gInPc1	jazyk
tvoří	tvořit	k5eAaImIp3nP	tvořit
méně	málo	k6eAd2	málo
slovních	slovní	k2eAgFnPc2d1	slovní
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
tedy	tedy	k9	tedy
větší	veliký	k2eAgFnSc1d2	veliký
šance	šance	k1gFnSc1	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
opakování	opakování	k1gNnSc3	opakování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Bohatství	bohatství	k1gNnSc1	bohatství
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
všemi	všecek	k3xTgInPc7	všecek
slovními	slovní	k2eAgInPc7d1	slovní
tvary	tvar	k1gInPc7	tvar
kromě	kromě	k7c2	kromě
pomocných	pomocný	k2eAgNnPc2d1	pomocné
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
lze	lze	k6eAd1	lze
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
užít	užít	k5eAaPmF	užít
právě	právě	k9	právě
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
bohatosti	bohatost	k1gFnSc2	bohatost
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovní	slovní	k2eAgInPc1d1	slovní
tvary	tvar	k1gInPc1	tvar
nejsou	být	k5eNaImIp3nP	být
lemmatizovány	lemmatizovat	k5eAaPmNgInP	lemmatizovat
<g/>
,	,	kIx,	,
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
spíše	spíše	k9	spíše
bohatost	bohatost	k1gFnSc4	bohatost
forem	forma	k1gFnPc2	forma
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
užití	užití	k1gNnSc1	užití
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
spíše	spíše	k9	spíše
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
syntetičnosti	syntetičnost	k1gFnSc2	syntetičnost
či	či	k8xC	či
analytičnosti	analytičnost	k1gFnSc2	analytičnost
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
homogenních	homogenní	k2eAgInPc6d1	homogenní
textech	text	k1gInPc6	text
střední	střední	k2eAgFnSc2d1	střední
délky	délka	k1gFnSc2	délka
tak	tak	k8xS	tak
hapaxy	hapax	k1gInPc1	hapax
fungují	fungovat	k5eAaImIp3nP	fungovat
spíše	spíše	k9	spíše
jako	jako	k9	jako
indikátory	indikátor	k1gInPc4	indikátor
pozice	pozice	k1gFnSc2	pozice
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
škále	škála	k1gFnSc6	škála
syntetických	syntetický	k2eAgInPc2d1	syntetický
<g/>
–	–	k?	–
<g/>
analytických	analytický	k2eAgInPc2d1	analytický
jazyků	jazyk	k1gInPc2	jazyk
Výskyt	výskyt	k1gInSc1	výskyt
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
v	v	k7c6	v
textu	text	k1gInSc6	text
značně	značně	k6eAd1	značně
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
možnosti	možnost	k1gFnPc4	možnost
automatického	automatický	k2eAgNnSc2d1	automatické
zpracování	zpracování	k1gNnSc2	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
strojům	stroj	k1gInPc3	stroj
možnost	možnost	k1gFnSc1	možnost
porozumět	porozumět	k5eAaPmF	porozumět
textu	text	k1gInSc3	text
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gNnSc1	legomenon
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
vzácnými	vzácný	k2eAgInPc7d1	vzácný
<g />
.	.	kIx.	.
</s>
<s>
jazykovými	jazykový	k2eAgInPc7d1	jazykový
jevy	jev	k1gInPc7	jev
tvoří	tvořit	k5eAaImIp3nS	tvořit
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
problém	problém	k1gInSc1	problém
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
tvořené	tvořený	k2eAgFnPc4d1	tvořená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
korpusů	korpus	k1gInPc2	korpus
<g/>
:	:	kIx,	:
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
frekvenci	frekvence	k1gFnSc4	frekvence
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
dostatečná	dostatečný	k2eAgNnPc1d1	dostatečné
data	datum	k1gNnPc1	datum
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
srovnávání	srovnávání	k1gNnSc1	srovnávání
jazyků	jazyk	k1gInPc2	jazyk
nebo	nebo	k8xC	nebo
strojový	strojový	k2eAgInSc1d1	strojový
překlad	překlad	k1gInSc1	překlad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gMnSc1	legomenon
and	and	k?	and
other	other	k1gMnSc1	other
so	so	k?	so
called	called	k1gMnSc1	called
rare	rarat	k5eAaPmIp3nS	rarat
events	events	k6eAd1	events
present	present	k1gInSc1	present
an	an	k?	an
<g />
.	.	kIx.	.
</s>
<s>
interesting	interesting	k1gInSc1	interesting
problem	probl	k1gInSc7	probl
for	forum	k1gNnPc2	forum
corpus	corpus	k1gNnSc7	corpus
based	based	k1gInSc1	based
applications	applications	k1gInSc1	applications
<g/>
:	:	kIx,	:
due	due	k?	due
to	ten	k3xDgNnSc1	ten
their	their	k1gInSc1	their
low	low	k?	low
frequency	frequenca	k1gFnSc2	frequenca
<g/>
,	,	kIx,	,
they	thea	k1gFnSc2	thea
fail	faila	k1gFnPc2	faila
to	ten	k3xDgNnSc1	ten
provide	provid	k1gInSc5	provid
enough	enough	k1gMnSc1	enough
statistical	statisticat	k5eAaPmAgMnS	statisticat
data	datum	k1gNnPc4	datum
for	forum	k1gNnPc2	forum
applications	applications	k6eAd1	applications
like	likat	k5eAaPmIp3nS	likat
word	word	k1gMnSc1	word
alignment	alignment	k1gMnSc1	alignment
or	or	k?	or
statistical	statisticat	k5eAaPmAgMnS	statisticat
machine	machinout	k5eAaPmIp3nS	machinout
translation	translation	k1gInSc4	translation
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
nonce	nonko	k6eAd1	nonko
word	word	k6eAd1	word
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
záměrné	záměrný	k2eAgNnSc1d1	záměrné
slovo	slovo	k1gNnSc1	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
hapax	hapax	k1gInSc1	hapax
legomenon	legomenona	k1gFnPc2	legomenona
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
určitém	určitý	k2eAgInSc6d1	určitý
textu	text	k1gInSc6	text
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
o	o	k7c6	o
původu	původ	k1gInSc6	původ
slova	slovo	k1gNnSc2	slovo
nebo	nebo	k8xC	nebo
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
četnosti	četnost	k1gFnSc6	četnost
užívání	užívání	k1gNnSc2	užívání
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
nonce	nonko	k6eAd1	nonko
word	word	k6eAd1	word
je	být	k5eAaImIp3nS	být
utvořeno	utvořit	k5eAaPmNgNnS	utvořit
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
určitou	určitý	k2eAgFnSc4d1	určitá
příležitost	příležitost	k1gFnSc4	příležitost
či	či	k8xC	či
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
tak	tak	k6eAd1	tak
stojí	stát	k5eAaImIp3nP	stát
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
napětí	napětí	k1gNnSc6	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
výkladech	výklad	k1gInPc6	výklad
mohou	moct	k5eAaImIp3nP	moct
oba	dva	k4xCgInPc4	dva
termíny	termín	k1gInPc4	termín
splývat	splývat	k5eAaImF	splývat
<g/>
;	;	kIx,	;
např.	např.	kA	např.
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Encyklopedia	Encyklopedium	k1gNnSc2	Encyklopedium
jazykovědy	jazykověda	k1gFnSc2	jazykověda
chápe	chápat	k5eAaImIp3nS	chápat
Jozef	Jozef	k1gMnSc1	Jozef
Mistrik	Mistrik	k1gMnSc1	Mistrik
hapax	hapax	k1gInSc4	hapax
legomenon	legomenona	k1gFnPc2	legomenona
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
nonce	nonko	k6eAd1	nonko
word	word	k6eAd1	word
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
"	"	kIx"	"
<g/>
slouží	sloužit	k5eAaImIp3nS	sloužit
například	například	k6eAd1	například
k	k	k7c3	k
ozvláštnění	ozvláštnění	k1gNnSc3	ozvláštnění
jazykových	jazykový	k2eAgInPc2d1	jazykový
prostředků	prostředek	k1gInPc2	prostředek
v	v	k7c6	v
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
díle	dílo	k1gNnSc6	dílo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
synonymnímu	synonymní	k2eAgNnSc3d1	synonymní
užívání	užívání	k1gNnSc3	užívání
obou	dva	k4xCgMnPc2	dva
pojmů	pojem	k1gInPc2	pojem
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
především	především	k9	především
v	v	k7c6	v
uměleckých	umělecký	k2eAgInPc6d1	umělecký
a	a	k8xC	a
publicistických	publicistický	k2eAgInPc6d1	publicistický
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Autogyos	Autogyos	k1gInSc1	Autogyos
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
,	,	kIx,	,
označující	označující	k2eAgInSc4d1	označující
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
orby	orba	k1gFnSc2	orba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
díle	díl	k1gInSc6	díl
Hésiodově	Hésiodův	k2eAgInSc6d1	Hésiodův
a	a	k8xC	a
nenalézáme	nalézat	k5eNaImIp1nP	nalézat
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
antickém	antický	k2eAgInSc6d1	antický
spise	spis	k1gInSc6	spis
Lilith	Lilitha	k1gFnPc2	Lilitha
(	(	kIx(	(
<g/>
ל	ל	k?	ל
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnSc4d1	označující
patrně	patrně	k6eAd1	patrně
noční	noční	k2eAgFnSc4d1	noční
bytost	bytost	k1gFnSc4	bytost
nebo	nebo	k8xC	nebo
lelka	lelek	k1gMnSc4	lelek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
Izajášově	Izajášův	k2eAgNnSc6d1	Izajášovo
proroctví	proroctví	k1gNnSc6	proroctví
34	[number]	k4	34
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Golem	Golem	k1gMnSc1	Golem
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
<g/>
)	)	kIx)	)
označující	označující	k2eAgInSc1d1	označující
zárodek	zárodek	k1gInSc1	zárodek
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
žalmů	žalm	k1gInPc2	žalm
139	[number]	k4	139
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
kralické	kralický	k2eAgFnSc2d1	Kralická
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
golema	golem	k1gMnSc4	golem
překladové	překladový	k2eAgNnSc4d1	překladové
staročeské	staročeský	k2eAgNnSc4d1	staročeské
slovo	slovo	k1gNnSc4	slovo
trupel	trupel	k1gInSc1	trupel
<g/>
.	.	kIx.	.
eufémos	eufémos	k1gInSc1	eufémos
(	(	kIx(	(
<g/>
ε	ε	k?	ε
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
překládané	překládaný	k2eAgNnSc1d1	překládané
jako	jako	k8xC	jako
dobropověstný	dobropověstný	k2eAgMnSc1d1	dobropověstný
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
v	v	k7c6	v
Dopise	dopis	k1gInSc6	dopis
Filipským	Filipský	k2eAgMnSc7d1	Filipský
4	[number]	k4	4
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
ramogna	ramogen	k2eAgNnPc1d1	ramogen
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
"	"	kIx"	"
<g/>
přání	přání	k1gNnSc4	přání
šťastné	šťastný	k2eAgFnSc2d1	šťastná
cesty	cesta	k1gFnSc2	cesta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Očistec	očistec	k1gInSc1	očistec
11,25	[number]	k4	11,25
<g/>
)	)	kIx)	)
a	a	k8xC	a
trasumanar	trasumanar	k1gMnSc1	trasumanar
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
znadlidštění	znadlidštění	k1gNnSc1	znadlidštění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Ráj	ráj	k1gInSc1	ráj
1,70	[number]	k4	1,70
<g/>
)	)	kIx)	)
v	v	k7c6	v
Dantově	Dantův	k2eAgFnSc6d1	Dantova
Božské	božský	k2eAgFnSc6d1	božská
komedii	komedie	k1gFnSc6	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Clyde	Clyd	k1gInSc5	Clyd
Pharra	Pharro	k1gNnPc4	Pharro
se	se	k3xPyFc4	se
v	v	k7c6	v
Íliadě	Íliad	k1gInSc6	Íliad
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
1097	[number]	k4	1097
hapax	hapax	k1gInSc1	hapax
legomen	legomen	k2eAgInSc1d1	legomen
a	a	k8xC	a
v	v	k7c6	v
Odyssei	Odysse	k1gInSc6	Odysse
865	[number]	k4	865
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
lexikálních	lexikální	k2eAgFnPc2d1	lexikální
i	i	k8xC	i
gramatických	gramatický	k2eAgFnPc2d1	gramatická
hapax	hapax	k1gInSc4	hapax
legomen	legomit	k5eAaPmNgMnS	legomit
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nedoložených	doložený	k2eNgNnPc2d1	nedoložené
slov	slovo	k1gNnPc2	slovo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
argumentem	argument	k1gInSc7	argument
odpůrců	odpůrce	k1gMnPc2	odpůrce
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
RKZ	RKZ	kA	RKZ
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
textu	text	k1gInSc2	text
hesla	heslo	k1gNnSc2	heslo
je	být	k5eAaImIp3nS	být
převzata	převzít	k5eAaPmNgFnS	převzít
z	z	k7c2	z
<g/>
:	:	kIx,	:
Varjassyová	Varjassyová	k1gFnSc1	Varjassyová
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hapax	hapax	k1gInSc1	hapax
legomenon	legomenon	k1gNnSc1	legomenon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Prokopová	Prokopová	k1gFnSc1	Prokopová
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
http://oltk.upol.cz/encyklopedie/index.php5/Hapax_legomenon	[url]	k1gInSc1	http://oltk.upol.cz/encyklopedie/index.php5/Hapax_legomenon
</s>
