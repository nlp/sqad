<s>
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2010	[number]	k4	2010
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Montrealu	Montreal	k1gInSc6	Montreal
a	a	k8xC	a
Calgary	Calgary	k1gNnSc6	Calgary
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gInSc1	Vancouver
stal	stát	k5eAaPmAgInS	stát
třetím	třetí	k4xOgNnSc7	třetí
kanadským	kanadský	k2eAgNnSc7d1	kanadské
olympijským	olympijský	k2eAgNnSc7d1	Olympijské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
vrátily	vrátit	k5eAaPmAgInP	vrátit
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
a	a	k8xC	a
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	let	k1gInPc6	let
od	od	k7c2	od
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Vancouveru	Vancouver	k1gInSc2	Vancouver
se	se	k3xPyFc4	se
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
sportoviště	sportoviště	k1gNnPc1	sportoviště
a	a	k8xC	a
ubytovací	ubytovací	k2eAgFnPc4d1	ubytovací
prostory	prostora	k1gFnPc4	prostora
nacházela	nacházet	k5eAaImAgFnS	nacházet
i	i	k9	i
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Univerzity	univerzita	k1gFnSc2	univerzita
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Callaghan	Callaghany	k1gInPc2	Callaghany
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městech	město	k1gNnPc6	město
West	West	k2eAgMnSc1d1	West
Vancouver	Vancouver	k1gMnSc1	Vancouver
a	a	k8xC	a
Richmond	Richmond	k1gMnSc1	Richmond
a	a	k8xC	a
ve	v	k7c6	v
středisku	středisko	k1gNnSc6	středisko
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
Whistler-Blackcomb	Whistler-Blackcomba	k1gFnPc2	Whistler-Blackcomba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Whistler	Whistler	k1gInSc1	Whistler
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
olympijské	olympijský	k2eAgFnSc2d1	olympijská
tradice	tradice	k1gFnSc2	tradice
přijal	přijmout	k5eAaPmAgMnS	přijmout
vancouverský	vancouverský	k2eAgMnSc1d1	vancouverský
starosta	starosta	k1gMnSc1	starosta
Sam	Sam	k1gMnSc1	Sam
Sullivan	Sullivan	k1gMnSc1	Sullivan
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
vlajku	vlajka	k1gFnSc4	vlajka
na	na	k7c6	na
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zakončení	zakončení	k1gNnSc6	zakončení
XX	XX	kA	XX
<g/>
.	.	kIx.	.
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
vztyčena	vztyčit	k5eAaPmNgFnS	vztyčit
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
vancouverské	vancouverský	k2eAgFnSc2d1	vancouverská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlála	vlát	k5eAaImAgFnS	vlát
až	až	k9	až
do	do	k7c2	do
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
zahájení	zahájení	k1gNnSc2	zahájení
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
her	hra	k1gFnPc2	hra
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tragické	tragický	k2eAgFnSc3d1	tragická
nehodě	nehoda	k1gFnSc3	nehoda
gruzínského	gruzínský	k2eAgMnSc2d1	gruzínský
sáňkaře	sáňkař	k1gMnSc2	sáňkař
Nodara	Nodar	k1gMnSc2	Nodar
Kumaritašviliho	Kumaritašvili	k1gMnSc2	Kumaritašvili
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
tréninku	trénink	k1gInSc6	trénink
po	po	k7c6	po
jezdecké	jezdecký	k2eAgFnSc6d1	jezdecká
chybě	chyba	k1gFnSc6	chyba
vyletěl	vyletět	k5eAaPmAgMnS	vyletět
ze	z	k7c2	z
sáňkařské	sáňkařský	k2eAgFnSc2d1	sáňkařská
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
narazil	narazit	k5eAaPmAgMnS	narazit
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
140	[number]	k4	140
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
do	do	k7c2	do
nechráněného	chráněný	k2eNgInSc2d1	nechráněný
ocelového	ocelový	k2eAgInSc2d1	ocelový
nosníku	nosník	k1gInSc2	nosník
u	u	k7c2	u
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
konečný	konečný	k2eAgInSc4d1	konečný
seznam	seznam	k1gInSc4	seznam
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
pořadatelství	pořadatelství	k1gNnSc4	pořadatelství
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městy	město	k1gNnPc7	město
Pchjongčchang	Pchjongčchang	k1gMnSc1	Pchjongčchang
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
a	a	k8xC	a
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
Vancouveru	Vancouver	k1gInSc2	Vancouver
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
na	na	k7c4	na
115	[number]	k4	115
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
MOV	MOV	kA	MOV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2003	[number]	k4	2003
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Oznámil	oznámit	k5eAaPmAgMnS	oznámit
jej	on	k3xPp3gNnSc4	on
prezident	prezident	k1gMnSc1	prezident
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
Jacques	Jacques	k1gMnSc1	Jacques
Rogge	Rogg	k1gFnSc2	Rogg
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
Organizační	organizační	k2eAgInSc1d1	organizační
výbor	výbor	k1gInSc1	výbor
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
VANOC	VANOC	kA	VANOC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
výška	výška	k1gFnSc1	výška
předběžného	předběžný	k2eAgInSc2d1	předběžný
rozpočtu	rozpočet	k1gInSc2	rozpočet
na	na	k7c4	na
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
ve	v	k7c6	v
Vancouveru	Vancouvero	k1gNnSc6	Vancouvero
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnSc2	miliarda
kanadských	kanadský	k2eAgMnPc2d1	kanadský
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
CAD	CAD	kA	CAD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
předseda	předseda	k1gMnSc1	předseda
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
VANOC	VANOC	kA	VANOC
John	John	k1gMnSc1	John
Furlong	Furlong	k1gMnSc1	Furlong
odhadl	odhadnout	k5eAaPmAgMnS	odhadnout
výšku	výška	k1gFnSc4	výška
rozpočtu	rozpočet	k1gInSc2	rozpočet
na	na	k7c4	na
1,7	[number]	k4	1,7
miliardy	miliarda	k4xCgFnSc2	miliarda
CAD	CAD	kA	CAD
s	s	k7c7	s
navýšením	navýšení	k1gNnSc7	navýšení
z	z	k7c2	z
mimovládních	mimovládní	k2eAgInPc2d1	mimovládní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
především	především	k9	především
ze	z	k7c2	z
sponzorských	sponzorský	k2eAgInPc2d1	sponzorský
darů	dar	k1gInPc2	dar
a	a	k8xC	a
vysílacích	vysílací	k2eAgNnPc2d1	vysílací
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sumy	suma	k1gFnSc2	suma
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
580	[number]	k4	580
miliónů	milión	k4xCgInPc2	milión
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
a	a	k8xC	a
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
sportovišť	sportoviště	k1gNnPc2	sportoviště
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
a	a	k8xC	a
Whistleru	Whistler	k1gInSc6	Whistler
a	a	k8xC	a
200	[number]	k4	200
miliónů	milión	k4xCgInPc2	milión
na	na	k7c6	na
zajištění	zajištění	k1gNnSc6	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
během	během	k7c2	během
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
královská	královský	k2eAgFnSc1d1	královská
jízdní	jízdní	k2eAgFnSc1d1	jízdní
policie	policie	k1gFnSc1	policie
(	(	kIx(	(
<g/>
RCMP	RCMP	kA	RCMP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
</s>
<s>
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
v	v	k7c6	v
celkem	celkem	k6eAd1	celkem
15	[number]	k4	15
sportovních	sportovní	k2eAgNnPc6d1	sportovní
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
:	:	kIx,	:
BC	BC	kA	BC
Place	plac	k1gInSc6	plac
–	–	k?	–
zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
a	a	k8xC	a
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
<g/>
,	,	kIx,	,
noční	noční	k2eAgNnSc4d1	noční
udělování	udělování	k1gNnSc4	udělování
medailí	medaile	k1gFnPc2	medaile
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vesnička	vesnička	k1gFnSc1	vesnička
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
Vancouver	Vancouver	k1gInSc1	Vancouver
Convention	Convention	k1gInSc1	Convention
&	&	k?	&
Exhibition	Exhibition	k1gInSc1	Exhibition
Centre	centr	k1gInSc5	centr
–	–	k?	–
centrála	centrála	k1gFnSc1	centrála
pro	pro	k7c4	pro
média	médium	k1gNnPc4	médium
GM	GM	kA	GM
Place	plac	k1gInSc6	plac
–	–	k?	–
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
kluziště	kluziště	k1gNnSc1	kluziště
<g/>
)	)	kIx)	)
Pacific	Pacifice	k1gFnPc2	Pacifice
Coliseum	Coliseum	k1gInSc1	Coliseum
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
<g/>
,	,	kIx,	,
short	short	k1gInSc1	short
track	track	k1gInSc1	track
Hillcrest	Hillcrest	k1gInSc1	Hillcrest
Park	park	k1gInSc1	park
–	–	k?	–
curling	curling	k1gInSc1	curling
UBC	UBC	kA	UBC
Winter	Winter	k1gMnSc1	Winter
Sports	Sports	k1gInSc1	Sports
Centre	centr	k1gInSc5	centr
–	–	k?	–
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc1	druhý
kluziště	kluziště	k1gNnSc1	kluziště
<g/>
)	)	kIx)	)
Richmond	Richmond	k1gMnSc1	Richmond
Olympic	Olympic	k1gMnSc1	Olympic
Oval	ovalit	k5eAaPmRp2nS	ovalit
–	–	k?	–
rychlobruslení	rychlobruslení	k1gNnSc2	rychlobruslení
Cypress	Cypressa	k1gFnPc2	Cypressa
Mountain	Mountain	k1gMnSc1	Mountain
–	–	k?	–
akrobatické	akrobatický	k2eAgNnSc1d1	akrobatické
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
,	,	kIx,	,
snowboarding	snowboarding	k1gInSc1	snowboarding
<g/>
,	,	kIx,	,
běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vesnička	vesnička	k1gFnSc1	vesnička
ve	v	k7c6	v
Whistleru	Whistler	k1gInSc6	Whistler
Whistler-Blackcomb	Whistler-Blackcomb	k1gMnSc1	Whistler-Blackcomb
–	–	k?	–
alpské	alpský	k2eAgNnSc1d1	alpské
lyžování	lyžování	k1gNnSc4	lyžování
Callaghan	Callaghana	k1gFnPc2	Callaghana
Valley	Vallea	k1gMnSc2	Vallea
–	–	k?	–
biatlon	biatlon	k1gInSc1	biatlon
<g/>
,	,	kIx,	,
běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
skoky	skok	k1gInPc4	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Whistler	Whistler	k1gInSc1	Whistler
Sliding	Sliding	k1gInSc1	Sliding
Centre	centr	k1gInSc5	centr
–	–	k?	–
boby	bob	k1gInPc1	bob
<g/>
,	,	kIx,	,
sáně	sáně	k1gFnPc1	sáně
<g/>
,	,	kIx,	,
skeleton	skeleton	k1gInSc1	skeleton
Whistler	Whistler	k1gMnSc1	Whistler
Nordic	Nordic	k1gMnSc1	Nordic
–	–	k?	–
severská	severský	k2eAgFnSc1d1	severská
kombinace	kombinace	k1gFnSc1	kombinace
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Medailové	medailový	k2eAgNnSc4d1	medailové
pořadí	pořadí	k1gNnSc4	pořadí
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Hokejové	hokejový	k2eAgInPc1d1	hokejový
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
standardní	standardní	k2eAgInPc4d1	standardní
rozměry	rozměr	k1gInPc4	rozměr
pro	pro	k7c4	pro
hřiště	hřiště	k1gNnSc4	hřiště
NHL	NHL	kA	NHL
(	(	kIx(	(
<g/>
61	[number]	k4	61
×	×	k?	×
26	[number]	k4	26
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
rozměrů	rozměr	k1gInPc2	rozměr
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
kluziště	kluziště	k1gNnPc4	kluziště
(	(	kIx(	(
<g/>
61	[number]	k4	61
×	×	k?	×
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
na	na	k7c6	na
hokejovém	hokejový	k2eAgInSc6d1	hokejový
stadionu	stadion	k1gInSc6	stadion
GM	GM	kA	GM
Place	plac	k1gInSc6	plac
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
hrají	hrát	k5eAaImIp3nP	hrát
svoje	svůj	k3xOyFgInPc1	svůj
zápasy	zápas	k1gInPc1	zápas
v	v	k7c6	v
NHL	NHL	kA	NHL
Vancouver	Vancouver	k1gInSc4	Vancouver
Canucks	Canucksa	k1gFnPc2	Canucksa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
ušetřila	ušetřit	k5eAaPmAgFnS	ušetřit
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
CAD	CAD	kA	CAD
na	na	k7c6	na
stavebních	stavební	k2eAgFnPc6d1	stavební
úpravách	úprava	k1gFnPc6	úprava
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nS	umožnit
dalším	další	k2eAgMnPc3d1	další
35	[number]	k4	35
000	[number]	k4	000
divákům	divák	k1gMnPc3	divák
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
hokejové	hokejový	k2eAgInPc4d1	hokejový
zápasy	zápas	k1gInPc4	zápas
o	o	k7c4	o
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
pochodeň	pochodeň	k1gFnSc1	pochodeň
byla	být	k5eAaImAgFnS	být
zapálena	zapálit	k5eAaPmNgFnS	zapálit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ji	on	k3xPp3gFnSc4	on
zapálilo	zapálit	k5eAaPmAgNnS	zapálit
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgFnPc1d1	zlatá
medaile	medaile	k1gFnPc1	medaile
byly	být	k5eAaImAgFnP	být
nejtěžší	těžký	k2eAgFnPc1d3	nejtěžší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
vážila	vážit	k5eAaImAgFnS	vážit
500-576	[number]	k4	500-576
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
měly	mít	k5eAaImAgFnP	mít
nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
1014	[number]	k4	1014
medailí	medaile	k1gFnPc2	medaile
nebyla	být	k5eNaImAgFnS	být
dokonale	dokonale	k6eAd1	dokonale
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebyly	být	k5eNaImAgInP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
2	[number]	k4	2
tvarově	tvarově	k6eAd1	tvarově
naprosto	naprosto	k6eAd1	naprosto
identické	identický	k2eAgFnPc4d1	identická
<g/>
.	.	kIx.	.
</s>
<s>
Haralds	Haralds	k6eAd1	Haralds
Silovs	Silovs	k1gInSc1	Silovs
z	z	k7c2	z
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
sportovcem	sportovec	k1gMnSc7	sportovec
v	v	k7c6	v
olympijské	olympijský	k2eAgFnSc6d1	olympijská
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
závodů	závod	k1gInPc2	závod
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
i	i	k8xC	i
v	v	k7c6	v
short	shorta	k1gFnPc2	shorta
tracku	track	k1gInSc2	track
<g/>
,	,	kIx,	,
a	a	k8xC	a
první	první	k4xOgMnSc1	první
který	který	k3yQgMnSc1	který
soutěžil	soutěžit	k5eAaImAgMnS	soutěžit
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
rozdílných	rozdílný	k2eAgNnPc6d1	rozdílné
sportovních	sportovní	k2eAgNnPc6d1	sportovní
odvětvích	odvětví	k1gNnPc6	odvětví
v	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Závodil	závodit	k5eAaImAgMnS	závodit
na	na	k7c4	na
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
v	v	k7c6	v
Richmondu	Richmond	k1gInSc6	Richmond
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
přemístil	přemístit	k5eAaPmAgInS	přemístit
přes	přes	k7c4	přes
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
do	do	k7c2	do
haly	hala	k1gFnSc2	hala
Pacific	Pacifice	k1gFnPc2	Pacifice
Coliseum	Coliseum	k1gInSc1	Coliseum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
v	v	k7c4	v
Short	Short	k1gInSc4	Short
tracku	track	k1gInSc2	track
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česko	Česko	k1gNnSc4	Česko
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
2010	[number]	k4	2010
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2010	[number]	k4	2010
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
ZOH	ZOH	kA	ZOH
Vancouver	Vancouver	k1gInSc4	Vancouver
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
pro	pro	k7c4	pro
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
pro	pro	k7c4	pro
cestovní	cestovní	k2eAgFnPc4d1	cestovní
<g />
.	.	kIx.	.
</s>
<s>
ruch	ruch	k1gInSc1	ruch
ve	v	k7c6	v
Whistleru	Whistler	k1gInSc6	Whistler
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Kanadské	kanadský	k2eAgFnSc2d1	kanadská
vlády	vláda	k1gFnSc2	vláda
–	–	k?	–
Federální	federální	k2eAgInSc1d1	federální
sekretariát	sekretariát	k1gInSc1	sekretariát
pro	pro	k7c4	pro
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
provincie	provincie	k1gFnSc2	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
–	–	k?	–
Sekretariát	sekretariát	k1gInSc1	sekretariát
pro	pro	k7c4	pro
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
města	město	k1gNnSc2	město
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
-	-	kIx~	-
stránka	stránka	k1gFnSc1	stránka
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Neoficiální	neoficiální	k2eAgFnSc2d1	neoficiální
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
ZOH	ZOH	kA	ZOH
Vancouver	Vancouver	k1gInSc4	Vancouver
2010	[number]	k4	2010
</s>
