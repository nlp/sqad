<p>
<s>
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
jsou	být	k5eAaImIp3nP	být
souborem	soubor	k1gInSc7	soubor
předmětů	předmět	k1gInPc2	předmět
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Svatovítského	svatovítský	k2eAgInSc2d1	svatovítský
pokladu	poklad	k1gInSc2	poklad
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
odznaky	odznak	k1gInPc1	odznak
(	(	kIx(	(
<g/>
insignie	insignie	k1gFnPc1	insignie
<g/>
)	)	kIx)	)
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Udělovaly	udělovat	k5eAaImAgInP	udělovat
se	se	k3xPyFc4	se
při	při	k7c6	při
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soubor	soubor	k1gInSc1	soubor
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
královské	královský	k2eAgNnSc1d1	královské
žezlo	žezlo	k1gNnSc1	žezlo
<g/>
,	,	kIx,	,
královské	královský	k2eAgNnSc1d1	královské
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kožená	kožený	k2eAgNnPc4d1	kožené
pouzdra	pouzdro	k1gNnPc4	pouzdro
na	na	k7c4	na
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
žezlo	žezlo	k1gNnSc4	žezlo
a	a	k8xC	a
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
podušku	poduška	k1gFnSc4	poduška
pod	pod	k7c4	pod
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
korunovační	korunovační	k2eAgInSc4d1	korunovační
plášť	plášť	k1gInSc4	plášť
s	s	k7c7	s
hermelínovými	hermelínový	k2eAgInPc7d1	hermelínový
doplňky	doplněk	k1gInPc7	doplněk
<g/>
,	,	kIx,	,
štólu	štóla	k1gFnSc4	štóla
<g/>
,	,	kIx,	,
pás	pás	k1gInSc4	pás
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
manipul	manipul	k1gInSc1	manipul
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
korunu	koruna	k1gFnSc4	koruna
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1345	[number]	k4	1345
až	až	k9	až
1346	[number]	k4	1346
zhotovit	zhotovit	k5eAaPmF	zhotovit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
korunovaci	korunovace	k1gFnSc3	korunovace
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
dělá	dělat	k5eAaImIp3nS	dělat
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
předměty	předmět	k1gInPc1	předmět
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
korunovačními	korunovační	k2eAgInPc7d1	korunovační
klenoty	klenot	k1gInPc7	klenot
bývá	bývat	k5eAaImIp3nS	bývat
vystavován	vystavován	k2eAgInSc1d1	vystavován
zlatý	zlatý	k2eAgInSc1d1	zlatý
relikviářový	relikviářový	k2eAgInSc1d1	relikviářový
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
korunovační	korunovační	k2eAgInSc1d1	korunovační
<g/>
,	,	kIx,	,
a	a	k8xC	a
obřadní	obřadní	k2eAgInSc1d1	obřadní
korunovační	korunovační	k2eAgInSc1d1	korunovační
meč	meč	k1gInSc1	meč
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
<g/>
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
jsou	být	k5eAaImIp3nP	být
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
byly	být	k5eAaImAgFnP	být
prohlášeny	prohlášen	k2eAgFnPc1d1	prohlášena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
po	po	k7c6	po
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Uloženy	uložen	k2eAgFnPc1d1	uložena
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Korunní	korunní	k2eAgFnSc6d1	korunní
komoře	komora	k1gFnSc6	komora
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
vynášeny	vynášen	k2eAgInPc1d1	vynášen
jen	jen	k9	jen
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
předmětů	předmět	k1gInPc2	předmět
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
===	===	k?	===
</s>
</p>
<p>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
zlaté	zlatý	k2eAgFnSc2d1	zlatá
čelenky	čelenka	k1gFnSc2	čelenka
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dílů	díl	k1gInPc2	díl
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
velkou	velký	k2eAgFnSc4d1	velká
lilii	lilie	k1gFnSc4	lilie
<g/>
.	.	kIx.	.
</s>
<s>
Čelenka	čelenka	k1gFnSc1	čelenka
je	být	k5eAaImIp3nS	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
o	o	k7c6	o
ryzosti	ryzost	k1gFnSc6	ryzost
22	[number]	k4	22
karátů	karát	k1gInPc2	karát
<g/>
.	.	kIx.	.
</s>
<s>
Díly	díl	k1gInPc1	díl
jsou	být	k5eAaImIp3nP	být
nahoře	nahoře	k6eAd1	nahoře
spojeny	spojit	k5eAaPmNgInP	spojit
dvěma	dva	k4xCgMnPc7	dva
příčnými	příčný	k2eAgMnPc7d1	příčný
pásky	pásek	k1gMnPc7	pásek
(	(	kIx(	(
<g/>
kamarami	kamara	k1gFnPc7	kamara
<g/>
)	)	kIx)	)
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
4	[number]	k4	4
centimetry	centimetr	k1gInPc4	centimetr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
svého	svůj	k3xOyFgNnSc2	svůj
překřížení	překřížení	k1gNnSc2	překřížení
(	(	kIx(	(
<g/>
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
)	)	kIx)	)
osazeny	osadit	k5eAaPmNgFnP	osadit
křížkem	křížek	k1gInSc7	křížek
<g/>
.	.	kIx.	.
</s>
<s>
Kamary	Kamar	k1gInPc1	Kamar
jsou	být	k5eAaImIp3nP	být
zhotoveny	zhotovit	k5eAaPmNgInP	zhotovit
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
nižší	nízký	k2eAgFnSc2d2	nižší
ryzosti	ryzost	k1gFnSc2	ryzost
(	(	kIx(	(
<g/>
19	[number]	k4	19
až	až	k9	až
20	[number]	k4	20
karátů	karát	k1gInPc2	karát
<g/>
)	)	kIx)	)
než	než	k8xS	než
čelenka	čelenka	k1gFnSc1	čelenka
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
posázeny	posázet	k5eAaPmNgFnP	posázet
dvojicemi	dvojice	k1gFnPc7	dvojice
30	[number]	k4	30
zelených	zelený	k2eAgInPc2d1	zelený
smaragdů	smaragd	k1gInPc2	smaragd
a	a	k8xC	a
rubínů	rubín	k1gInPc2	rubín
<g/>
.	.	kIx.	.
</s>
<s>
Pásy	pás	k1gInPc1	pás
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
zlatého	zlatý	k2eAgInSc2d1	zlatý
opasku	opasek	k1gInSc2	opasek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
svatebním	svatební	k2eAgInSc7d1	svatební
darem	dar	k1gInSc7	dar
Blanky	Blanka	k1gFnSc2	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
snad	snad	k9	snad
od	od	k7c2	od
jejího	její	k3xOp3gMnSc2	její
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
koruny	koruna	k1gFnSc2	koruna
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
zlatými	zlatý	k2eAgFnPc7d1	zlatá
závlačkami	závlačka	k1gFnPc7	závlačka
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
osazena	osazen	k2eAgFnSc1d1	osazena
96	[number]	k4	96
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
a	a	k8xC	a
perlami	perla	k1gFnPc7	perla
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejvzácnější	vzácný	k2eAgMnSc1d3	nejvzácnější
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
rubelit	rubelit	k1gInSc1	rubelit
(	(	kIx(	(
<g/>
donedávna	donedávna	k6eAd1	donedávna
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
rubín	rubín	k1gInSc4	rubín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
4	[number]	k4	4
×	×	k?	×
3,7	[number]	k4	3,7
cm	cm	kA	cm
na	na	k7c6	na
čelné	čelný	k2eAgFnSc6d1	čelná
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
19	[number]	k4	19
safírů	safír	k1gInPc2	safír
<g/>
,	,	kIx,	,
30	[number]	k4	30
smaragdů	smaragd	k1gInPc2	smaragd
<g/>
,	,	kIx,	,
44	[number]	k4	44
spinelů	spinel	k1gInPc2	spinel
<g/>
,	,	kIx,	,
1	[number]	k4	1
rubín	rubín	k1gInSc1	rubín
<g/>
,	,	kIx,	,
1	[number]	k4	1
rubelit	rubelit	k1gInSc1	rubelit
<g/>
,	,	kIx,	,
1	[number]	k4	1
akvamarín	akvamarín	k1gInSc4	akvamarín
a	a	k8xC	a
20	[number]	k4	20
perel	perla	k1gFnPc2	perla
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
křížku	křížek	k1gInSc2	křížek
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
vložen	vložen	k2eAgInSc1d1	vložen
trn	trn	k1gInSc1	trn
z	z	k7c2	z
koruny	koruna	k1gFnSc2	koruna
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Karlu	Karel	k1gMnSc3	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
daroval	darovat	k5eAaPmAgMnS	darovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zlaté	zlatý	k2eAgFnSc6d1	zlatá
obroučce	obroučka	k1gFnSc6	obroučka
křížku	křížek	k1gInSc2	křížek
je	být	k5eAaImIp3nS	být
vyrytý	vyrytý	k2eAgInSc1d1	vyrytý
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
HIC	HIC	k?	HIC
EST	Est	k1gMnSc1	Est
SPINA	spina	k1gFnSc1	spina
DE	DE	k?	DE
CORONA	CORONA	kA	CORONA
DOMINI	DOMINI	kA	DOMINI
(	(	kIx(	(
<g/>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
trn	trn	k1gInSc4	trn
z	z	k7c2	z
koruny	koruna	k1gFnSc2	koruna
Páně	páně	k2eAgFnSc2d1	páně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
váží	vážit	k5eAaImIp3nS	vážit
2358,04	[number]	k4	2358,04
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
lilie	lilie	k1gFnPc1	lilie
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnSc2	výška
19	[number]	k4	19
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
čelenky	čelenka	k1gFnSc2	čelenka
je	být	k5eAaImIp3nS	být
19	[number]	k4	19
až	až	k9	až
21	[number]	k4	21
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Královské	královský	k2eAgNnSc4d1	královské
žezlo	žezlo	k1gNnSc4	žezlo
===	===	k?	===
</s>
</p>
<p>
<s>
Žezlo	žezlo	k1gNnSc4	žezlo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
augsburský	augsburský	k2eAgMnSc1d1	augsburský
zlatník	zlatník	k1gMnSc1	zlatník
Hans	Hans	k1gMnSc1	Hans
Haller	Haller	k1gMnSc1	Haller
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
pro	pro	k7c4	pro
krále	král	k1gMnSc4	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Habsburského	habsburský	k2eAgInSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zhotoveno	zhotoven	k2eAgNnSc1d1	zhotoveno
z	z	k7c2	z
osmnáctikarátového	osmnáctikarátový	k2eAgNnSc2d1	osmnáctikarátové
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
67	[number]	k4	67
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
1013	[number]	k4	1013
g.	g.	k?	g.
Je	být	k5eAaImIp3nS	být
ozdobeno	ozdobit	k5eAaPmNgNnS	ozdobit
4	[number]	k4	4
safíry	safír	k1gInPc1	safír
<g/>
,	,	kIx,	,
5	[number]	k4	5
spinely	spinel	k1gInPc1	spinel
a	a	k8xC	a
62	[number]	k4	62
perlami	perla	k1gFnPc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
velký	velký	k2eAgInSc1d1	velký
spinel	spinel	k1gInSc1	spinel
je	být	k5eAaImIp3nS	být
osazen	osadit	k5eAaPmNgInS	osadit
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
žezla	žezlo	k1gNnSc2	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Žezlo	žezlo	k1gNnSc1	žezlo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Rukojeť	rukojeť	k1gFnSc1	rukojeť
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
emailovými	emailový	k2eAgInPc7d1	emailový
lístky	lístek	k1gInPc7	lístek
<g/>
,	,	kIx,	,
květy	květ	k1gInPc7	květ
a	a	k8xC	a
větévkami	větévka	k1gFnPc7	větévka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
zakončena	zakončen	k2eAgNnPc4d1	zakončeno
věnečkem	věneček	k1gInSc7	věneček
perel	perla	k1gFnPc2	perla
<g/>
.	.	kIx.	.
</s>
<s>
Dřík	dřík	k1gInSc1	dřík
žezla	žezlo	k1gNnSc2	žezlo
je	být	k5eAaImIp3nS	být
zdoben	zdobit	k5eAaImNgInS	zdobit
rovněž	rovněž	k9	rovněž
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
dekorem	dekor	k1gInSc7	dekor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
žezla	žezlo	k1gNnSc2	žezlo
je	být	k5eAaImIp3nS	být
stylizována	stylizovat	k5eAaImNgFnS	stylizovat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
květu	květ	k1gInSc2	květ
mezi	mezi	k7c7	mezi
spirálkami	spirálka	k1gFnPc7	spirálka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
osazena	osazen	k2eAgFnSc1d1	osazena
perlami	perla	k1gFnPc7	perla
<g/>
,	,	kIx,	,
vrtanými	vrtaný	k2eAgInPc7d1	vrtaný
safíry	safír	k1gInPc7	safír
a	a	k8xC	a
rubíny	rubín	k1gInPc7	rubín
na	na	k7c6	na
trnech	trn	k1gInPc6	trn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Královské	královský	k2eAgNnSc4d1	královské
jablko	jablko	k1gNnSc4	jablko
===	===	k?	===
</s>
</p>
<p>
<s>
Královské	královský	k2eAgNnSc1d1	královské
jablko	jablko	k1gNnSc1	jablko
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
pro	pro	k7c4	pro
krále	král	k1gMnSc4	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Habsburského	habsburský	k2eAgInSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
zhotoveno	zhotovit	k5eAaPmNgNnS	zhotovit
z	z	k7c2	z
osmnáctikarátového	osmnáctikarátový	k2eAgNnSc2d1	osmnáctikarátové
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
zploštělé	zploštělý	k2eAgNnSc1d1	zploštělé
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
11,9	[number]	k4	11,9
cm	cm	kA	cm
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
9,8	[number]	k4	9,8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
22	[number]	k4	22
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
780	[number]	k4	780
g.	g.	k?	g.
Jablko	jablko	k1gNnSc1	jablko
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
a	a	k8xC	a
na	na	k7c6	na
obroučce	obroučka	k1gFnSc6	obroučka
osazeno	osadit	k5eAaPmNgNnS	osadit
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
safíry	safír	k1gInPc4	safír
<g/>
,	,	kIx,	,
6	[number]	k4	6
spinely	spinel	k1gInPc1	spinel
a	a	k8xC	a
31	[number]	k4	31
perlami	perla	k1gFnPc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
části	část	k1gFnSc6	část
pod	pod	k7c7	pod
křížem	kříž	k1gInSc7	kříž
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
DOMINE	DOMINE	kA	DOMINE
IN	IN	kA	IN
VIRTUTE	VIRTUTE	kA	VIRTUTE
TUA	TUA	kA	TUA
LETABITUR	LETABITUR	kA	LETABITUR
REX	REX	kA	REX
ET	ET	kA	ET
SUPER	super	k1gInSc4	super
SALUTARE	SALUTARE	kA	SALUTARE
TUAM	TUAM	kA	TUAM
EXULTABIT	EXULTABIT	kA	EXULTABIT
(	(	kIx(	(
<g/>
Hospodine	Hospodin	k1gMnSc5	Hospodin
<g/>
,	,	kIx,	,
z	z	k7c2	z
tvé	tvůj	k3xOp2gFnSc2	tvůj
moci	moc	k1gFnSc2	moc
raduje	radovat	k5eAaImIp3nS	radovat
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
a	a	k8xC	a
z	z	k7c2	z
pomoci	pomoc	k1gFnSc2	pomoc
tvé	tvůj	k3xOp2gNnSc1	tvůj
jásá	jásat	k5eAaImIp3nS	jásat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
DEUS	DEUS	kA	DEUS
CELUS	CELUS	kA	CELUS
REGNAT	REGNAT	kA	REGNAT
ET	ET	kA	ET
REGES	REGES	kA	REGES
TERRE	TERRE	kA	TERRE
(	(	kIx(	(
<g/>
Bůh	bůh	k1gMnSc1	bůh
vládne	vládnout	k5eAaImIp3nS	vládnout
nebesům	nebesa	k1gNnPc3	nebesa
a	a	k8xC	a
králové	králová	k1gFnSc3	králová
zemi	zem	k1gFnSc3	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Obě	dva	k4xCgFnPc1	dva
polokoule	polokoule	k1gFnPc1	polokoule
jablka	jablko	k1gNnSc2	jablko
jsou	být	k5eAaImIp3nP	být
zdobeny	zdobit	k5eAaImNgFnP	zdobit
tepanými	tepaný	k2eAgFnPc7d1	tepaná
reliéfními	reliéfní	k2eAgFnPc7d1	reliéfní
scénami	scéna	k1gFnPc7	scéna
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
historie	historie	k1gFnSc1	historie
krále	král	k1gMnSc2	král
Davida	David	k1gMnSc2	David
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
pomazání	pomazání	k1gNnSc4	pomazání
Davida	David	k1gMnSc2	David
na	na	k7c4	na
krále	král	k1gMnSc4	král
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
zápas	zápas	k1gInSc1	zápas
Davida	David	k1gMnSc2	David
s	s	k7c7	s
Goliášem	Goliáš	k1gMnSc7	Goliáš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
polokouli	polokoule	k1gFnSc6	polokoule
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc1	Genesis
(	(	kIx(	(
<g/>
Stvoření	stvoření	k1gNnSc1	stvoření
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Adam	Adam	k1gMnSc1	Adam
klečící	klečící	k2eAgMnSc1d1	klečící
před	před	k7c7	před
Bohem	bůh	k1gMnSc7	bůh
Stvořitelem	Stvořitel	k1gMnSc7	Stvořitel
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
uvedení	uvedení	k1gNnSc1	uvedení
Adama	Adam	k1gMnSc2	Adam
do	do	k7c2	do
ráje	ráj	k1gInSc2	ráj
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
Stvořitel	Stvořitel	k1gMnSc1	Stvořitel
varuje	varovat	k5eAaImIp3nS	varovat
Adama	Adam	k1gMnSc4	Adam
a	a	k8xC	a
Evu	Eva	k1gFnSc4	Eva
před	před	k7c7	před
stromem	strom	k1gInSc7	strom
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Korunovační	korunovační	k2eAgInSc1d1	korunovační
plášť	plášť	k1gInSc1	plášť
===	===	k?	===
</s>
</p>
<p>
<s>
Korunovační	korunovační	k2eAgInSc1d1	korunovační
plášť	plášť	k1gInSc1	plášť
byl	být	k5eAaImAgInS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
korunovaci	korunovace	k1gFnSc4	korunovace
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
roku	rok	k1gInSc2	rok
1617	[number]	k4	1617
a	a	k8xC	a
následně	následně	k6eAd1	následně
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
korunovacím	korunovace	k1gFnPc3	korunovace
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
poslední	poslední	k2eAgFnSc1d1	poslední
korunovace	korunovace	k1gFnSc1	korunovace
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Král	Král	k1gMnSc1	Král
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
pluviálu	pluviál	k1gInSc2	pluviál
(	(	kIx(	(
<g/>
pláště	plášť	k1gInSc2	plášť
<g/>
)	)	kIx)	)
oděn	oděn	k2eAgMnSc1d1	oděn
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
korunovačního	korunovační	k2eAgInSc2d1	korunovační
obřadu	obřad	k1gInSc2	obřad
a	a	k8xC	a
přijímal	přijímat	k5eAaImAgMnS	přijímat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
od	od	k7c2	od
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
ostatní	ostatní	k2eAgFnSc2d1	ostatní
královské	královský	k2eAgFnSc2d1	královská
insignie	insignie	k1gFnPc1	insignie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
oblečena	oblečen	k2eAgFnSc1d1	oblečena
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
královské	královský	k2eAgFnSc6d1	královská
korunovaci	korunovace	k1gFnSc6	korunovace
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
manželky	manželka	k1gFnPc4	manželka
habsburských	habsburský	k2eAgMnPc2d1	habsburský
panovníků	panovník	k1gMnPc2	panovník
byly	být	k5eAaImAgFnP	být
korunovány	korunovat	k5eAaBmNgFnP	korunovat
na	na	k7c4	na
české	český	k2eAgFnPc4d1	Česká
královny	královna	k1gFnPc4	královna
v	v	k7c6	v
luxusních	luxusní	k2eAgInPc6d1	luxusní
šatech	šat	k1gInPc6	šat
podle	podle	k7c2	podle
dobové	dobový	k2eAgFnSc2d1	dobová
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
z	z	k7c2	z
drahocenného	drahocenný	k2eAgInSc2d1	drahocenný
hedvábného	hedvábný	k2eAgInSc2d1	hedvábný
materiálu	materiál	k1gInSc2	materiál
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
zvaného	zvaný	k2eAgInSc2d1	zvaný
zlatohlav	zlatohlav	k1gInSc4	zlatohlav
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
lemován	lemovat	k5eAaImNgInS	lemovat
hermelínem	hermelín	k1gInSc7	hermelín
(	(	kIx(	(
<g/>
kožešinou	kožešina	k1gFnSc7	kožešina
z	z	k7c2	z
hranostaje	hranostaj	k1gMnSc2	hranostaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
polokruhový	polokruhový	k2eAgInSc1d1	polokruhový
<g/>
,	,	kIx,	,
vzadu	vzadu	k6eAd1	vzadu
prodloužený	prodloužený	k2eAgMnSc1d1	prodloužený
do	do	k7c2	do
vlečky	vlečka	k1gFnSc2	vlečka
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
rukávy	rukáv	k1gInPc4	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
312	[number]	k4	312
cm	cm	kA	cm
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
236	[number]	k4	236
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Uložen	uložen	k2eAgMnSc1d1	uložen
je	být	k5eAaImIp3nS	být
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
klenotů	klenot	k1gInPc2	klenot
ve	v	k7c6	v
speciálně	speciálně	k6eAd1	speciálně
klimatizovaném	klimatizovaný	k2eAgInSc6d1	klimatizovaný
depozitáři	depozitář	k1gInSc6	depozitář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kožená	kožený	k2eAgNnPc1d1	kožené
pouzdra	pouzdro	k1gNnPc1	pouzdro
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
kožené	kožený	k2eAgNnSc1d1	kožené
pouzdro	pouzdro	k1gNnSc1	pouzdro
na	na	k7c4	na
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zhotovit	zhotovit	k5eAaPmF	zhotovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
<g/>
.	.	kIx.	.
</s>
<s>
Pouzdro	pouzdro	k1gNnSc1	pouzdro
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
horní	horní	k2eAgInSc4d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgInSc4d1	dolní
díl	díl	k1gInSc4	díl
jsou	být	k5eAaImIp3nP	být
bohatě	bohatě	k6eAd1	bohatě
zdobeny	zdoben	k2eAgFnPc1d1	zdobena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pouzdru	pouzdro	k1gNnSc6	pouzdro
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
části	část	k1gFnSc6	část
<g/>
:	:	kIx,	:
říšská	říšský	k2eAgFnSc1d1	říšská
orlice	orlice	k1gFnSc1	orlice
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
rodový	rodový	k2eAgInSc1d1	rodový
erb	erb	k1gInSc1	erb
Arnošta	Arnošt	k1gMnSc2	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
a	a	k8xC	a
znak	znak	k1gInSc1	znak
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
je	být	k5eAaImIp3nS	být
čtyřřádkový	čtyřřádkový	k2eAgInSc1d1	čtyřřádkový
červený	červený	k2eAgInSc1d1	červený
nápis	nápis	k1gInSc1	nápis
ANNO	Anna	k1gFnSc5	Anna
DOMINI	DOMINI	kA	DOMINI
MCCCXLVII	MCCCXLVII	kA	MCCCXLVII
DOMINUS	DOMINUS	kA	DOMINUS
KAROLUS	KAROLUS	kA	KAROLUS
ROMANORUM	ROMANORUM	kA	ROMANORUM
REX	REX	kA	REX
ET	ET	kA	ET
BOHEMIAE	BOHEMIAE	kA	BOHEMIAE
REX	REX	kA	REX
ME	ME	kA	ME
FECIT	FECIT	kA	FECIT
AD	ad	k7c4	ad
HONOREM	honor	k1gInSc7	honor
DEI	DEI	kA	DEI
ET	ET	kA	ET
BEATI	BEATI	kA	BEATI
WENCESLAI	WENCESLAI	kA	WENCESLAI
MARTIRIS	MARTIRIS	kA	MARTIRIS
GLORIOSI	GLORIOSI	kA	GLORIOSI
(	(	kIx(	(
<g/>
Léta	léto	k1gNnPc1	léto
Páně	páně	k2eAgFnSc2d1	páně
1347	[number]	k4	1347
pan	pan	k1gMnSc1	pan
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
mě	já	k3xPp1nSc4	já
udělal	udělat	k5eAaPmAgMnS	udělat
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Boží	božit	k5eAaImIp3nS	božit
a	a	k8xC	a
slávě	sláva	k1gFnSc3	sláva
mučedníka	mučedník	k1gMnSc2	mučedník
blahoslaveného	blahoslavený	k2eAgMnSc2d1	blahoslavený
Václava	Václav	k1gMnSc2	Václav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pouzdro	pouzdro	k1gNnSc1	pouzdro
žezla	žezlo	k1gNnSc2	žezlo
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
válcovitý	válcovitý	k2eAgInSc4d1	válcovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
rozšířený	rozšířený	k2eAgMnSc1d1	rozšířený
pro	pro	k7c4	pro
hlavici	hlavice	k1gFnSc4	hlavice
žezla	žezlo	k1gNnSc2	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zhotoveno	zhotoven	k2eAgNnSc1d1	zhotoveno
z	z	k7c2	z
hladké	hladký	k2eAgFnSc2d1	hladká
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
spojených	spojený	k2eAgFnPc2d1	spojená
drobnými	drobný	k2eAgInPc7d1	drobný
panty	pant	k1gInPc7	pant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgNnSc1d1	původní
pouzdro	pouzdro	k1gNnSc1	pouzdro
na	na	k7c4	na
jablko	jablko	k1gNnSc4	jablko
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
jablka	jablko	k1gNnSc2	jablko
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
době	doba	k1gFnSc6	doba
1	[number]	k4	1
<g/>
.	.	kIx.	.
republiky	republika	k1gFnSc2	republika
nechtěla	chtít	k5eNaImAgFnS	chtít
státní	státní	k2eAgFnSc1d1	státní
moc	moc	k6eAd1	moc
používat	používat	k5eAaImF	používat
"	"	kIx"	"
<g/>
katolické	katolický	k2eAgInPc4d1	katolický
<g/>
"	"	kIx"	"
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
zhotoveno	zhotovit	k5eAaPmNgNnS	zhotovit
pouzdro	pouzdro	k1gNnSc1	pouzdro
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
znak	znak	k1gInSc1	znak
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
jsou	být	k5eAaImIp3nP	být
letopočty	letopočet	k1gInPc1	letopočet
svatováclavského	svatováclavský	k2eAgNnSc2d1	Svatováclavské
milénia	milénium	k1gNnSc2	milénium
929	[number]	k4	929
a	a	k8xC	a
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
DVACÁTÝ	dvacátý	k4xOgInSc1	dvacátý
OSMÝ	osmý	k4xOgInSc1	osmý
ZÁŘÍ	zář	k1gFnPc2	zář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgInPc1d1	ostatní
korunovační	korunovační	k2eAgInPc1d1	korunovační
předměty	předmět	k1gInPc1	předmět
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
korunovačním	korunovační	k2eAgInPc3d1	korunovační
obřadům	obřad	k1gInPc3	obřad
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
předmětů	předmět	k1gInPc2	předmět
používaly	používat	k5eAaImAgInP	používat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
předměty	předmět	k1gInPc1	předmět
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
ze	z	k7c2	z
Svatovítského	svatovítský	k2eAgInSc2d1	svatovítský
pokladu	poklad	k1gInSc2	poklad
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
meč	meč	k1gInSc1	meč
nebo	nebo	k8xC	nebo
korunovační	korunovační	k2eAgInSc1d1	korunovační
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přemyslovská	přemyslovský	k2eAgNnPc1d1	přemyslovské
knížata	kníže	k1gNnPc1	kníže
===	===	k?	===
</s>
</p>
<p>
<s>
Odznaky	odznak	k1gInPc1	odznak
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
prvních	první	k4xOgMnPc2	první
známých	známý	k1gMnPc2	známý
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
byly	být	k5eAaImAgFnP	být
korouhev	korouhev	k1gFnSc1	korouhev
<g/>
,	,	kIx,	,
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
a	a	k8xC	a
přílba	přílba	k1gFnSc1	přílba
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
klobouk	klobouk	k1gInSc1	klobouk
nebo	nebo	k8xC	nebo
čapka	čapka	k1gFnSc1	čapka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1059	[number]	k4	1059
nebo	nebo	k8xC	nebo
1060	[number]	k4	1060
získal	získat	k5eAaPmAgInS	získat
Spytihněv	Spytihněv	k1gFnSc7	Spytihněv
II	II	kA	II
<g/>
.	.	kIx.	.
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
za	za	k7c4	za
roční	roční	k2eAgInSc4d1	roční
poplatek	poplatek	k1gInSc4	poplatek
100	[number]	k4	100
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
(	(	kIx(	(
<g/>
cca	cca	kA	cca
20	[number]	k4	20
kg	kg	kA	kg
<g/>
)	)	kIx)	)
právo	právo	k1gNnSc4	právo
nosit	nosit	k5eAaImF	nosit
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
mitru	mitra	k1gFnSc4	mitra
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc4d1	stejné
právo	právo	k1gNnSc4	právo
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koruny	koruna	k1gFnSc2	koruna
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
králů	král	k1gMnPc2	král
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
osobu	osoba	k1gFnSc4	osoba
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
kníže	kníže	k1gMnSc1	kníže
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1085	[number]	k4	1085
za	za	k7c4	za
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
ho	on	k3xPp3gNnSc4	on
na	na	k7c4	na
shromáždění	shromáždění	k1gNnSc4	shromáždění
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
korunoval	korunovat	k5eAaBmAgMnS	korunovat
císař	císař	k1gMnSc1	císař
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
propůjčenou	propůjčený	k2eAgFnSc7d1	propůjčená
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Plnohodnotná	plnohodnotný	k2eAgFnSc1d1	plnohodnotná
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
připravená	připravený	k2eAgFnSc1d1	připravená
korunovace	korunovace	k1gFnSc1	korunovace
Vratislava	Vratislava	k1gFnSc1	Vratislava
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Svatavy	Svatava	k1gFnSc2	Svatava
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1086	[number]	k4	1086
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
korunovace	korunovace	k1gFnPc1	korunovace
na	na	k7c6	na
témže	týž	k3xTgNnSc6	týž
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1228	[number]	k4	1228
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nechal	nechat	k5eAaPmAgMnS	nechat
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
korunovat	korunovat	k5eAaBmF	korunovat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc4	Václav
I.	I.	kA	I.
"	"	kIx"	"
<g/>
mladším	mladý	k2eAgMnSc7d2	mladší
králem	král	k1gMnSc7	král
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
mu	on	k3xPp3gMnSc3	on
pojistil	pojistit	k5eAaPmAgInS	pojistit
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podruhé	podruhé	k6eAd1	podruhé
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
získal	získat	k5eAaPmAgMnS	získat
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1158	[number]	k4	1158
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
vojenské	vojenský	k2eAgFnSc2d1	vojenská
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
korunoval	korunovat	k5eAaBmAgMnS	korunovat
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
ho	on	k3xPp3gMnSc4	on
císař	císař	k1gMnSc1	císař
korunoval	korunovat	k5eAaBmAgMnS	korunovat
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
dobytém	dobytý	k2eAgInSc6d1	dobytý
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
směl	smět	k5eAaImAgMnS	smět
korunu	koruna	k1gFnSc4	koruna
nosit	nosit	k5eAaImF	nosit
pouze	pouze	k6eAd1	pouze
pětkrát	pětkrát	k6eAd1	pětkrát
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
svátků	svátek	k1gInPc2	svátek
Božích	boží	k2eAgInPc2d1	boží
hodů	hod	k1gInPc2	hod
(	(	kIx(	(
<g/>
vánoční	vánoční	k2eAgInPc4d1	vánoční
<g/>
,	,	kIx,	,
velikonoční	velikonoční	k2eAgInPc4d1	velikonoční
a	a	k8xC	a
svatodušní	svatodušní	k2eAgInPc4d1	svatodušní
<g/>
)	)	kIx)	)
a	a	k8xC	a
svátků	svátek	k1gInPc2	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Dědičný	dědičný	k2eAgInSc1d1	dědičný
královský	královský	k2eAgInSc1d1	královský
titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1198	[number]	k4	1198
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
Dědičnost	dědičnost	k1gFnSc1	dědičnost
titulu	titul	k1gInSc2	titul
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
roku	rok	k1gInSc2	rok
1212	[number]	k4	1212
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
bulou	bula	k1gFnSc7	bula
sicilskou	sicilský	k2eAgFnSc7d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Korunovaci	korunovace	k1gFnSc4	korunovace
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
přikládán	přikládat	k5eAaImNgInS	přikládat
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
</s>
<s>
Korunovace	korunovace	k1gFnSc1	korunovace
byla	být	k5eAaImAgFnS	být
připravována	připravovat	k5eAaImNgFnS	připravovat
dlouho	dlouho	k6eAd1	dlouho
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
používali	používat	k5eAaImAgMnP	používat
titul	titul	k1gInSc4	titul
král	král	k1gMnSc1	král
až	až	k9	až
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
titulovali	titulovat	k5eAaImAgMnP	titulovat
dědic	dědic	k1gMnSc1	dědic
a	a	k8xC	a
pán	pán	k1gMnSc1	pán
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koruny	koruna	k1gFnPc1	koruna
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
králů	král	k1gMnPc2	král
se	se	k3xPyFc4	se
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
nezachovaly	zachovat	k5eNaPmAgInP	zachovat
ani	ani	k9	ani
o	o	k7c6	o
nich	on	k3xPp3gNnPc6	on
neexistují	existovat	k5eNaImIp3nP	existovat
písemné	písemný	k2eAgFnPc4d1	písemná
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
podobu	podoba	k1gFnSc4	podoba
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
z	z	k7c2	z
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
na	na	k7c6	na
pečetích	pečeť	k1gFnPc6	pečeť
<g/>
,	,	kIx,	,
mincích	mince	k1gFnPc6	mince
<g/>
,	,	kIx,	,
nástěnných	nástěnný	k2eAgFnPc6d1	nástěnná
malbách	malba	k1gFnPc6	malba
<g/>
,	,	kIx,	,
kronikách	kronika	k1gFnPc6	kronika
apod.	apod.	kA	apod.
Podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
použita	použít	k5eAaPmNgFnS	použít
koruna	koruna	k1gFnSc1	koruna
Vratislavova	Vratislavův	k2eAgFnSc1d1	Vratislavova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
upravována	upravován	k2eAgFnSc1d1	upravována
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
je	být	k5eAaImIp3nS	být
přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
koruna	koruna	k1gFnSc1	koruna
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
při	při	k7c6	při
korunovaci	korunovace	k1gFnSc6	korunovace
druhé	druhý	k4xOgFnSc2	druhý
manželky	manželka	k1gFnSc2	manželka
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
Beatrix	Beatrix	k1gInSc1	Beatrix
Bourbonské	bourbonský	k2eAgFnPc1d1	Bourbonská
roku	rok	k1gInSc2	rok
1337	[number]	k4	1337
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
objevuje	objevovat	k5eAaImIp3nS	objevovat
označení	označení	k1gNnSc1	označení
corona	corona	k1gFnSc1	corona
regni	regeň	k1gFnSc3	regeň
Bohemiae	Bohemiae	k1gFnSc4	Bohemiae
<g/>
,	,	kIx,	,
koruna	koruna	k1gFnSc1	koruna
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
celého	celý	k2eAgNnSc2d1	celé
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
osud	osud	k1gInSc1	osud
koruny	koruna	k1gFnSc2	koruna
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Posloužila	posloužit	k5eAaPmAgFnS	posloužit
však	však	k9	však
Karlu	Karel	k1gMnSc3	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
inspirace	inspirace	k1gFnSc1	inspirace
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
nové	nový	k2eAgFnSc2d1	nová
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
tvorbě	tvorba	k1gFnSc6	tvorba
také	také	k6eAd1	také
použita	použít	k5eAaPmNgFnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Podobu	podoba	k1gFnSc4	podoba
původních	původní	k2eAgInPc2d1	původní
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
též	též	k9	též
pozlacené	pozlacený	k2eAgFnPc1d1	pozlacená
stříbrné	stříbrná	k1gFnPc1	stříbrná
pohřební	pohřební	k2eAgFnPc1d1	pohřební
klenoty	klenot	k1gInPc4	klenot
<g/>
,	,	kIx,	,
nalezené	nalezený	k2eAgFnPc4d1	nalezená
v	v	k7c6	v
hrobech	hrob	k1gInPc6	hrob
Přemysla	Přemysl	k1gMnSc4	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
korunu	koruna	k1gFnSc4	koruna
nechal	nechat	k5eAaPmAgMnS	nechat
vytvořit	vytvořit	k5eAaPmF	vytvořit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1346	[number]	k4	1346
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
založeno	založit	k5eAaPmNgNnS	založit
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
(	(	kIx(	(
<g/>
1344	[number]	k4	1344
<g/>
)	)	kIx)	)
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
udělil	udělit	k5eAaPmAgMnS	udělit
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupům	arcibiskup	k1gMnPc3	arcibiskup
právo	právo	k1gNnSc4	právo
slavnostně	slavnostně	k6eAd1	slavnostně
pomazat	pomazat	k5eAaPmF	pomazat
a	a	k8xC	a
korunovat	korunovat	k5eAaBmF	korunovat
české	český	k2eAgMnPc4d1	český
krále	král	k1gMnPc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
žádost	žádost	k1gFnSc4	žádost
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1346	[number]	k4	1346
bulu	bula	k1gFnSc4	bula
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
této	tento	k3xDgFnSc2	tento
koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
koruně	koruna	k1gFnSc6	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
korunu	koruna	k1gFnSc4	koruna
zcizit	zcizit	k5eAaPmF	zcizit
<g/>
,	,	kIx,	,
prodat	prodat	k5eAaPmF	prodat
nebo	nebo	k8xC	nebo
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
koruna	koruna	k1gFnSc1	koruna
majetkem	majetek	k1gInSc7	majetek
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
hlavě	hlava	k1gFnSc6	hlava
(	(	kIx(	(
<g/>
relikviářové	relikviářový	k2eAgFnSc6d1	relikviářová
bustě	busta	k1gFnSc6	busta
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
chrámu	chrámat	k5eAaImIp1nS	chrámat
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
trvale	trvale	k6eAd1	trvale
uložena	uložit	k5eAaPmNgFnS	uložit
<g/>
.	.	kIx.	.
</s>
<s>
Králům	Král	k1gMnPc3	Král
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
zapůjčena	zapůjčit	k5eAaPmNgFnS	zapůjčit
v	v	k7c4	v
den	den	k1gInSc4	den
korunovace	korunovace	k1gFnSc2	korunovace
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
večer	večer	k6eAd1	večer
navrácena	navrácen	k2eAgFnSc1d1	navrácena
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zapůjčení	zapůjčení	k1gNnSc4	zapůjčení
koruny	koruna	k1gFnSc2	koruna
měl	mít	k5eAaImAgMnS	mít
každý	každý	k3xTgMnSc1	každý
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
zaplatit	zaplatit	k5eAaPmF	zaplatit
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
kop	kopa	k1gFnPc2	kopa
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
pražské	pražský	k2eAgFnSc2d1	Pražská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
života	život	k1gInSc2	život
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
koruna	koruna	k1gFnSc1	koruna
několikrát	několikrát	k6eAd1	několikrát
přepracována	přepracovat	k5eAaPmNgFnS	přepracovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nP	svědčit
soupisy	soupis	k1gInPc1	soupis
chrámového	chrámový	k2eAgInSc2d1	chrámový
pokladu	poklad	k1gInSc2	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
podoba	podoba	k1gFnSc1	podoba
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
na	na	k7c6	na
soudobých	soudobý	k2eAgInPc6d1	soudobý
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Soupis	soupis	k1gInSc1	soupis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1385	[number]	k4	1385
již	již	k6eAd1	již
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
úprava	úprava	k1gFnSc1	úprava
však	však	k9	však
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
zřejmě	zřejmě	k6eAd1	zřejmě
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Karlovou	Karlův	k2eAgFnSc7d1	Karlova
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
nechal	nechat	k5eAaPmAgMnS	nechat
vytvořit	vytvořit	k5eAaPmF	vytvořit
žezlo	žezlo	k1gNnSc4	žezlo
a	a	k8xC	a
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
součástí	součást	k1gFnPc2	součást
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
uloženy	uložit	k5eAaPmNgInP	uložit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
také	také	k9	také
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
sepsání	sepsání	k1gNnSc4	sepsání
Řádu	řád	k1gInSc2	řád
korunování	korunování	k1gNnSc2	korunování
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
a	a	k8xC	a
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Převezení	převezení	k1gNnPc4	převezení
na	na	k7c4	na
Karlštejn	Karlštejn	k1gInSc4	Karlštejn
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
podle	podle	k7c2	podle
původního	původní	k2eAgNnSc2d1	původní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
neměly	mít	k5eNaImAgInP	mít
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
vůbec	vůbec	k9	vůbec
opustit	opustit	k5eAaPmF	opustit
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
–	–	k?	–
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
převezeny	převézt	k5eAaPmNgInP	převézt
počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
bývaly	bývat	k5eAaImAgInP	bývat
klenoty	klenot	k1gInPc1	klenot
pravidelně	pravidelně	k6eAd1	pravidelně
převáženy	převážen	k2eAgInPc1d1	převážen
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
ke	k	k7c3	k
korunovacím	korunovace	k1gFnPc3	korunovace
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
po	po	k7c6	po
korunovačních	korunovační	k2eAgFnPc6d1	korunovační
slavnostech	slavnost	k1gFnPc6	slavnost
zase	zase	k9	zase
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
nechal	nechat	k5eAaPmAgMnS	nechat
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
po	po	k7c6	po
chvatné	chvatný	k2eAgFnSc6d1	chvatná
korunovaci	korunovace	k1gFnSc6	korunovace
na	na	k7c4	na
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
před	před	k7c7	před
vypukající	vypukající	k2eAgFnSc7d1	vypukající
husitskou	husitský	k2eAgFnSc7d1	husitská
revolucí	revoluce	k1gFnSc7	revoluce
vyvézt	vyvézt	k5eAaPmF	vyvézt
klenoty	klenot	k1gInPc4	klenot
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
do	do	k7c2	do
uherského	uherský	k2eAgInSc2d1	uherský
Visegrádu	Visegrád	k1gInSc2	Visegrád
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
Norimberka	Norimberk	k1gInSc2	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
opětovném	opětovný	k2eAgNnSc6d1	opětovné
uznání	uznání	k1gNnSc6	uznání
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
nechal	nechat	k5eAaPmAgMnS	nechat
Zikmund	Zikmund	k1gMnSc1	Zikmund
klenoty	klenot	k1gInPc4	klenot
přivézt	přivézt	k5eAaPmF	přivézt
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Karlštejn	Karlštejn	k1gInSc4	Karlštejn
r.	r.	kA	r.
1436	[number]	k4	1436
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
zůstaly	zůstat	k5eAaPmAgInP	zůstat
s	s	k7c7	s
krátkými	krátký	k2eAgFnPc7d1	krátká
přestávkami	přestávka	k1gFnPc7	přestávka
uloženy	uložen	k2eAgFnPc4d1	uložena
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1619	[number]	k4	1619
(	(	kIx(	(
<g/>
např.	např.	kA	např.
V	v	k7c6	v
letech	let	k1gInPc6	let
1448	[number]	k4	1448
<g/>
–	–	k?	–
<g/>
1451	[number]	k4	1451
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
byly	být	k5eAaImAgInP	být
díky	díky	k7c3	díky
Mehartovi	Mehart	k1gMnSc3	Mehart
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
uloženy	uložit	k5eAaPmNgFnP	uložit
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Velhartice	Velhartice	k1gFnPc1	Velhartice
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemské	zemský	k2eAgNnSc1d1	zemské
zřízení	zřízení	k1gNnSc1	zřízení
vydané	vydaný	k2eAgNnSc1d1	vydané
králem	král	k1gMnSc7	král
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jagellonským	jagellonský	k2eAgMnSc7d1	jagellonský
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
přísahou	přísaha	k1gFnSc7	přísaha
karlštejnské	karlštejnský	k2eAgMnPc4d1	karlštejnský
purkrabí	purkrabí	k1gMnPc4	purkrabí
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
chránit	chránit	k5eAaImF	chránit
hrad	hrad	k1gInSc4	hrad
i	i	k9	i
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
klenoty	klenot	k1gInPc7	klenot
a	a	k8xC	a
zemskými	zemský	k2eAgNnPc7d1	zemské
privilegii	privilegium	k1gNnPc7	privilegium
pod	pod	k7c7	pod
trestem	trest	k1gInSc7	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
ztráty	ztráta	k1gFnSc2	ztráta
cti	čest	k1gFnSc2	čest
a	a	k8xC	a
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
vyhnání	vyhnání	k1gNnSc1	vyhnání
potomků	potomek	k1gMnPc2	potomek
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Korunu	koruna	k1gFnSc4	koruna
mohli	moct	k5eAaImAgMnP	moct
vydat	vydat	k5eAaPmF	vydat
pouze	pouze	k6eAd1	pouze
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgInS	být
předtím	předtím	k6eAd1	předtím
řádně	řádně	k6eAd1	řádně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
byly	být	k5eAaImAgInP	být
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
direktorů	direktor	k1gMnPc2	direktor
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1619	[number]	k4	1619
převezeny	převézt	k5eAaPmNgFnP	převézt
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
vojenské	vojenský	k2eAgFnSc2d1	vojenská
techniky	technika	k1gFnSc2	technika
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
obranná	obranný	k2eAgFnSc1d1	obranná
funkce	funkce	k1gFnSc1	funkce
hradu	hrad	k1gInSc2	hrad
shledána	shledat	k5eAaPmNgFnS	shledat
již	již	k6eAd1	již
jako	jako	k8xC	jako
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
prchající	prchající	k2eAgMnSc1d1	prchající
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vyvézt	vyvézt	k5eAaPmF	vyvézt
korunu	koruna	k1gFnSc4	koruna
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
klenoty	klenot	k1gInPc7	klenot
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
zanechána	zanechán	k2eAgFnSc1d1	zanechána
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vrácena	vrátit	k5eAaPmNgFnS	vrátit
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
revizi	revize	k1gFnSc6	revize
zemského	zemský	k2eAgNnSc2d1	zemské
zřízení	zřízení	k1gNnSc2	zřízení
roku	rok	k1gInSc2	rok
1625	[number]	k4	1625
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
zrušil	zrušit	k5eAaPmAgInS	zrušit
úřad	úřad	k1gInSc1	úřad
karlštejnských	karlštejnský	k2eAgMnPc2d1	karlštejnský
purkrabí	purkrabí	k1gMnSc1	purkrabí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uložení	uložení	k1gNnSc1	uložení
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
několikrát	několikrát	k6eAd1	několikrát
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
stěhovaly	stěhovat	k5eAaImAgFnP	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1631	[number]	k4	1631
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
saského	saský	k2eAgInSc2d1	saský
vpádu	vpád	k1gInSc2	vpád
odvezeny	odvézt	k5eAaPmNgInP	odvézt
do	do	k7c2	do
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byly	být	k5eAaImAgInP	být
navráceny	navrátit	k5eAaPmNgInP	navrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1633	[number]	k4	1633
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k9	již
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
instrukcí	instrukce	k1gFnPc2	instrukce
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
opět	opět	k6eAd1	opět
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
Švédů	Švéd	k1gMnPc2	Švéd
byly	být	k5eAaImAgFnP	být
tajně	tajně	k6eAd1	tajně
odvezeny	odvézt	k5eAaPmNgFnP	odvézt
do	do	k7c2	do
císařské	císařský	k2eAgFnSc2d1	císařská
klenotnice	klenotnice	k1gFnSc2	klenotnice
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
uložení	uložení	k1gNnSc2	uložení
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
žezla	žezlo	k1gNnSc2	žezlo
a	a	k8xC	a
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
žezlo	žezlo	k1gNnSc1	žezlo
a	a	k8xC	a
jablko	jablko	k1gNnSc1	jablko
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1533	[number]	k4	1533
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
klenoty	klenot	k1gInPc1	klenot
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
klenotnici	klenotnice	k1gFnSc6	klenotnice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žezlo	žezlo	k1gNnSc4	žezlo
a	a	k8xC	a
jablko	jablko	k1gNnSc4	jablko
z	z	k7c2	z
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
klenotnice	klenotnice	k1gFnSc2	klenotnice
(	(	kIx(	(
<g/>
Schatzkammer	Schatzkammer	k1gInSc1	Schatzkammer
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
původními	původní	k2eAgInPc7d1	původní
kusy	kus	k1gInPc7	kus
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
také	také	k9	také
portrét	portrét	k1gInSc1	portrét
císaře	císař	k1gMnSc2	císař
Matyáše	Matyáš	k1gMnSc2	Matyáš
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
jako	jako	k8xC	jako
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
se	s	k7c7	s
Svatováclavskou	svatováclavský	k2eAgFnSc7d1	Svatováclavská
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
těmito	tento	k3xDgInPc7	tento
klenoty	klenot	k1gInPc7	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
výměny	výměna	k1gFnSc2	výměna
je	být	k5eAaImIp3nS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
,	,	kIx,	,
původní	původní	k2eAgInPc4d1	původní
klenoty	klenot	k1gInPc4	klenot
svým	svůj	k3xOyFgInSc7	svůj
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
absencí	absence	k1gFnSc7	absence
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
nesplňovaly	splňovat	k5eNaImAgInP	splňovat
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
reprezentaci	reprezentace	k1gFnSc4	reprezentace
prestižního	prestižní	k2eAgNnSc2d1	prestižní
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Navrácení	navrácení	k1gNnPc4	navrácení
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
navrácením	navrácení	k1gNnSc7	navrácení
českých	český	k2eAgInPc2d1	český
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
císař	císař	k1gMnSc1	císař
Leopold	Leopold	k1gMnSc1	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
dekretem	dekret	k1gInSc7	dekret
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1790	[number]	k4	1790
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
jednání	jednání	k1gNnSc6	jednání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Klenoty	klenot	k1gInPc1	klenot
byly	být	k5eAaImAgInP	být
přivezeny	přivézt	k5eAaPmNgInP	přivézt
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
Leopoldovy	Leopoldův	k2eAgFnSc2d1	Leopoldova
korunovace	korunovace	k1gFnSc2	korunovace
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
sakristii	sakristie	k1gFnSc6	sakristie
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
současné	současný	k2eAgFnSc3d1	současná
Korunní	korunní	k2eAgFnSc3d1	korunní
komoře	komora	k1gFnSc3	komora
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
současná	současný	k2eAgFnSc1d1	současná
tradice	tradice	k1gFnSc1	tradice
sedmi	sedm	k4xCc2	sedm
klíčníků	klíčník	k1gMnPc2	klíčník
<g/>
.	.	kIx.	.
</s>
<s>
Držitelé	držitel	k1gMnPc1	držitel
klíčů	klíč	k1gInPc2	klíč
od	od	k7c2	od
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
měnili	měnit	k5eAaImAgMnP	měnit
podle	podle	k7c2	podle
momentálního	momentální	k2eAgNnSc2d1	momentální
politického	politický	k2eAgNnSc2d1	politické
a	a	k8xC	a
správního	správní	k2eAgNnSc2d1	správní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klenoty	klenot	k1gInPc1	klenot
byly	být	k5eAaImAgInP	být
odvezeny	odvézt	k5eAaPmNgInP	odvézt
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
během	během	k7c2	během
prusko-rakouské	pruskoakouský	k2eAgFnSc2d1	prusko-rakouská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
však	však	k9	však
byly	být	k5eAaImAgInP	být
slavnostně	slavnostně	k6eAd1	slavnostně
navráceny	navrátit	k5eAaPmNgInP	navrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
byla	být	k5eAaImAgFnS	být
Korunní	korunní	k2eAgFnSc1d1	korunní
komora	komora	k1gFnSc1	komora
upravena	upravit	k5eAaPmNgFnS	upravit
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
úprava	úprava	k1gFnSc1	úprava
komory	komora	k1gFnSc2	komora
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
provizorně	provizorně	k6eAd1	provizorně
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
uloženy	uložen	k2eAgFnPc1d1	uložena
ve	v	k7c4	v
sklepení	sklepení	k1gNnSc4	sklepení
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Za	za	k7c2	za
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
mobilizace	mobilizace	k1gFnSc2	mobilizace
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1938	[number]	k4	1938
tajně	tajně	k6eAd1	tajně
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
pobočky	pobočka	k1gFnSc2	pobočka
Národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
(	(	kIx(	(
<g/>
tajná	tajný	k2eAgFnSc1d1	tajná
operace	operace	k1gFnSc1	operace
s	s	k7c7	s
názvem	název	k1gInSc7	název
KFC	KFC	kA	KFC
–	–	k?	–
Klenoty	klenot	k1gInPc1	klenot
fascinující	fascinující	k2eAgFnSc2d1	fascinující
ceny	cena	k1gFnSc2	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úkryt	úkryt	k1gInSc1	úkryt
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
odvezeny	odvézt	k5eAaPmNgInP	odvézt
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klenoty	klenot	k1gInPc1	klenot
neunikly	uniknout	k5eNaPmAgInP	uniknout
ani	ani	k9	ani
pozornosti	pozornost	k1gFnSc2	pozornost
německé	německý	k2eAgFnSc2d1	německá
okupační	okupační	k2eAgFnSc2d1	okupační
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zamýšlela	zamýšlet	k5eAaImAgFnS	zamýšlet
využít	využít	k5eAaPmF	využít
jejich	jejich	k3xOp3gFnPc4	jejich
symboliky	symbolika	k1gFnPc4	symbolika
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
projevili	projevit	k5eAaPmAgMnP	projevit
již	již	k6eAd1	již
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1939	[number]	k4	1939
říšský	říšský	k2eAgInSc1d1	říšský
protektor	protektor	k1gInSc1	protektor
Konstantin	Konstantin	k1gMnSc1	Konstantin
von	von	k1gInSc1	von
Neurath	Neurath	k1gMnSc1	Neurath
a	a	k8xC	a
státní	státní	k2eAgMnSc1d1	státní
tajemník	tajemník	k1gMnSc1	tajemník
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
Frank	Frank	k1gMnSc1	Frank
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
protektora	protektor	k1gMnSc2	protektor
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
přerozděleny	přerozdělen	k2eAgInPc4d1	přerozdělen
klíče	klíč	k1gInPc4	klíč
od	od	k7c2	od
korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
3	[number]	k4	3
klíče	klíč	k1gInSc2	klíč
převzal	převzít	k5eAaPmAgMnS	převzít
státní	státní	k2eAgMnSc1d1	státní
prezident	prezident	k1gMnSc1	prezident
Hácha	Hácha	k1gMnSc1	Hácha
<g/>
,	,	kIx,	,
1	[number]	k4	1
klíč	klíč	k1gInSc1	klíč
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
a	a	k8xC	a
zbylé	zbylý	k2eAgNnSc1d1	zbylé
3	[number]	k4	3
klíče	klíč	k1gInSc2	klíč
říšský	říšský	k2eAgInSc4d1	říšský
protektor	protektor	k1gInSc4	protektor
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
byla	být	k5eAaImAgFnS	být
bezprecedentním	bezprecedentní	k2eAgInSc7d1	bezprecedentní
způsobem	způsob	k1gInSc7	způsob
zneužita	zneužit	k2eAgFnSc1d1	zneužita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
zastupující	zastupující	k2eAgMnPc1d1	zastupující
říšského	říšský	k2eAgMnSc4d1	říšský
protektora	protektor	k1gMnSc4	protektor
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
korunu	koruna	k1gFnSc4	koruna
coby	coby	k?	coby
"	"	kIx"	"
<g/>
symbol	symbol	k1gInSc1	symbol
věrnosti	věrnost	k1gFnSc2	věrnost
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
Říši	říše	k1gFnSc4	říše
<g/>
"	"	kIx"	"
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
než	než	k8xS	než
prohlídka	prohlídka	k1gFnSc1	prohlídka
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
<g/>
,	,	kIx,	,
převzala	převzít	k5eAaPmAgFnS	převzít
okupační	okupační	k2eAgFnSc1d1	okupační
moc	moc	k6eAd1	moc
zbylé	zbylý	k2eAgInPc4d1	zbylý
4	[number]	k4	4
klíče	klíč	k1gInPc4	klíč
<g/>
,	,	kIx,	,
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
neomezený	omezený	k2eNgInSc4d1	neomezený
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
Korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
právě	právě	k9	právě
toto	tento	k3xDgNnSc4	tento
bezostyšné	bezostyšný	k2eAgNnSc4d1	bezostyšné
porušení	porušení	k1gNnSc4	porušení
tradiční	tradiční	k2eAgFnSc2d1	tradiční
zásady	zásada	k1gFnSc2	zásada
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
držení	držení	k1gNnSc2	držení
klíčů	klíč	k1gInPc2	klíč
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
vzniknout	vzniknout	k5eAaPmF	vzniknout
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
historicky	historicky	k6eAd1	historicky
nepotvrzené	potvrzený	k2eNgFnSc3d1	nepotvrzená
legendě	legenda	k1gFnSc3	legenda
o	o	k7c4	o
svévolné	svévolný	k2eAgFnPc4d1	svévolná
korunovaci	korunovace	k1gFnSc4	korunovace
Heydricha	Heydrich	k1gMnSc2	Heydrich
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
osmiletého	osmiletý	k2eAgMnSc2d1	osmiletý
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
svatováclavskou	svatováclavský	k2eAgFnSc7d1	Svatováclavská
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
opovážlivost	opovážlivost	k1gFnSc4	opovážlivost
měl	mít	k5eAaImAgMnS	mít
oba	dva	k4xCgMnPc1	dva
stihnout	stihnout	k5eAaPmF	stihnout
krutý	krutý	k2eAgInSc1d1	krutý
trest	trest	k1gInSc1	trest
–	–	k?	–
Heydrich	Heydrich	k1gMnSc1	Heydrich
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
atentátu	atentát	k1gInSc2	atentát
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Klaus	Klaus	k1gMnSc1	Klaus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
autonehody	autonehoda	k1gFnSc2	autonehoda
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943.11	[number]	k4	1943.11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
nechali	nechat	k5eAaPmAgMnP	nechat
nacisté	nacista	k1gMnPc1	nacista
klenoty	klenot	k1gInPc4	klenot
zazdít	zazdít	k5eAaPmF	zazdít
před	před	k7c7	před
spojeneckými	spojenecký	k2eAgInPc7d1	spojenecký
nálety	nálet	k1gInPc7	nálet
ve	v	k7c6	v
sklepení	sklepení	k1gNnSc6	sklepení
Starého	Starého	k2eAgInSc2d1	Starého
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
Prahy	Praha	k1gFnSc2	Praha
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
vyzvednuty	vyzvednut	k2eAgFnPc1d1	vyzvednuta
a	a	k8xC	a
navráceny	navrácen	k2eAgFnPc1d1	navrácena
do	do	k7c2	do
Korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Střední	střední	k2eAgFnSc6d1	střední
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
kopie	kopie	k1gFnSc1	kopie
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
Expo	Expo	k1gNnSc1	Expo
1967	[number]	k4	1967
v	v	k7c6	v
Montréalu	Montréal	k1gInSc6	Montréal
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
<g/>
,	,	kIx,	,
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Leningradu	Leningrad	k1gInSc2	Leningrad
<g/>
,	,	kIx,	,
Budapešti	Budapešť	k1gFnSc6	Budapešť
a	a	k8xC	a
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
1979	[number]	k4	1979
nechala	nechat	k5eAaPmAgFnS	nechat
Kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
vytvořit	vytvořit	k5eAaPmF	vytvořit
kopie	kopie	k1gFnPc4	kopie
i	i	k8xC	i
ostatních	ostatní	k2eAgInPc2d1	ostatní
klenotů	klenot	k1gInPc2	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Kopie	kopie	k1gFnPc1	kopie
jsou	být	k5eAaImIp3nP	být
trvale	trvale	k6eAd1	trvale
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
expozici	expozice	k1gFnSc6	expozice
Starého	Starého	k2eAgInSc2d1	Starého
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uložení	uložení	k1gNnSc4	uložení
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
==	==	k?	==
</s>
</p>
<p>
<s>
Korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
jsou	být	k5eAaImIp3nP	být
trvale	trvale	k6eAd1	trvale
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
těžko	těžko	k6eAd1	těžko
přístupné	přístupný	k2eAgFnSc6d1	přístupná
Korunní	korunní	k2eAgFnSc6d1	korunní
komoře	komora	k1gFnSc6	komora
nad	nad	k7c7	nad
jižním	jižní	k2eAgInSc7d1	jižní
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
(	(	kIx(	(
<g/>
za	za	k7c7	za
mozaikou	mozaika	k1gFnSc7	mozaika
Posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnPc1d1	vstupní
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
sedmi	sedm	k4xCc2	sedm
zámky	zámek	k1gInPc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Klíče	klíč	k1gInPc1	klíč
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
zámkům	zámek	k1gInPc3	zámek
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
probošt	probošt	k1gMnSc1	probošt
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
kapituly	kapitula	k1gFnSc2	kapitula
u	u	k7c2	u
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
primátor	primátor	k1gMnSc1	primátor
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zámky	zámek	k1gInPc1	zámek
jsou	být	k5eAaImIp3nP	být
odemykány	odemykat	k5eAaImNgInP	odemykat
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
vystaveny	vystaven	k2eAgFnPc1d1	vystavena
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
komory	komora	k1gFnSc2	komora
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
po	po	k7c6	po
úzkém	úzký	k2eAgNnSc6d1	úzké
točitém	točitý	k2eAgNnSc6d1	točité
schodišti	schodiště	k1gNnSc6	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Komora	komora	k1gFnSc1	komora
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
trezorovou	trezorový	k2eAgFnSc7d1	trezorová
skříní	skříň	k1gFnSc7	skříň
na	na	k7c4	na
uložení	uložení	k1gNnSc4	uložení
klenotů	klenot	k1gInPc2	klenot
<g/>
,	,	kIx,	,
dubovým	dubový	k2eAgInSc7d1	dubový
stolem	stol	k1gInSc7	stol
a	a	k8xC	a
osmi	osm	k4xCc7	osm
židlemi	židle	k1gFnPc7	židle
<g/>
.	.	kIx.	.
</s>
<s>
Skříň	skříň	k1gFnSc1	skříň
se	se	k3xPyFc4	se
odemyká	odemykat	k5eAaImIp3nS	odemykat
stejnými	stejný	k2eAgInPc7d1	stejný
klíči	klíč	k1gInPc7	klíč
jako	jako	k8xS	jako
vstupní	vstupní	k2eAgFnPc1d1	vstupní
dveře	dveře	k1gFnPc1	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyobrazení	vyobrazení	k1gNnPc1	vyobrazení
klenotů	klenot	k1gInPc2	klenot
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarším	starý	k2eAgNnSc7d3	nejstarší
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
je	být	k5eAaImIp3nS	být
portrét	portrét	k1gInSc1	portrét
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
chrámu	chrámat	k5eAaImIp1nS	chrámat
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
(	(	kIx(	(
<g/>
asi	asi	k9	asi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
zobrazovány	zobrazovat	k5eAaImNgInP	zobrazovat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
a	a	k8xC	a
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
odchylkami	odchylka	k1gFnPc7	odchylka
od	od	k7c2	od
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
portrétů	portrét	k1gInPc2	portrét
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
malíři	malíř	k1gMnSc3	malíř
neměli	mít	k5eNaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
vidět	vidět	k5eAaImF	vidět
klenoty	klenot	k1gInPc4	klenot
zblízka	zblízka	k6eAd1	zblízka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
je	on	k3xPp3gFnPc4	on
neviděli	vidět	k5eNaImAgMnP	vidět
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
správný	správný	k2eAgInSc1d1	správný
popis	popis	k1gInSc1	popis
a	a	k8xC	a
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
koruny	koruna	k1gFnSc2	koruna
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
vydané	vydaný	k2eAgFnSc2d1	vydaná
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
korunovace	korunovace	k1gFnSc2	korunovace
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Marie	Maria	k1gFnSc2	Maria
Luisy	Luisa	k1gFnSc2	Luisa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
prvnímu	první	k4xOgInSc3	první
vědeckému	vědecký	k2eAgInSc3d1	vědecký
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
sneseny	snesen	k2eAgInPc1d1	snesen
z	z	k7c2	z
Korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
pořídil	pořídit	k5eAaPmAgMnS	pořídit
malíř	malíř	k1gMnSc1	malíř
Josef	Josef	k1gMnSc1	Josef
Scheiwl	Scheiwl	k1gMnSc1	Scheiwl
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
jediné	jediné	k1gNnSc1	jediné
objevovalo	objevovat	k5eAaImAgNnS	objevovat
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
publikacích	publikace	k1gFnPc6	publikace
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
ukládání	ukládání	k1gNnSc2	ukládání
klenotů	klenot	k1gInPc2	klenot
do	do	k7c2	do
nově	nově	k6eAd1	nově
zrekonstruované	zrekonstruovaný	k2eAgFnSc2d1	zrekonstruovaná
Korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
pořídil	pořídit	k5eAaPmAgMnS	pořídit
fotograf	fotograf	k1gMnSc1	fotograf
Jindřich	Jindřich	k1gMnSc1	Jindřich
Lachmann	Lachmann	k1gMnSc1	Lachmann
jejich	jejich	k3xOp3gFnSc2	jejich
první	první	k4xOgFnSc2	první
černobílé	černobílý	k2eAgFnSc2d1	černobílá
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
barevné	barevný	k2eAgFnPc4d1	barevná
fotografie	fotografia	k1gFnPc4	fotografia
pořídili	pořídit	k5eAaPmAgMnP	pořídit
Jan	Jan	k1gMnSc1	Jan
Vidím	vidět	k5eAaImIp1nS	vidět
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
Bautz	Bautz	k1gMnSc1	Bautz
při	při	k7c6	při
odborné	odborný	k2eAgFnSc6d1	odborná
prohlídce	prohlídka	k1gFnSc6	prohlídka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
také	také	k6eAd1	také
populárním	populární	k2eAgInSc7d1	populární
motivem	motiv	k1gInSc7	motiv
na	na	k7c6	na
mnoha	mnoho	k4c2	mnoho
pohlednicích	pohlednice	k1gFnPc6	pohlednice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
příležitosti	příležitost	k1gFnPc1	příležitost
pro	pro	k7c4	pro
fotografování	fotografování	k1gNnSc4	fotografování
a	a	k8xC	a
vědecké	vědecký	k2eAgNnSc1d1	vědecké
zkoumání	zkoumání	k1gNnSc1	zkoumání
klenotů	klenot	k1gInPc2	klenot
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
naskytly	naskytnout	k5eAaPmAgFnP	naskytnout
vždy	vždy	k6eAd1	vždy
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
veřejných	veřejný	k2eAgFnPc2d1	veřejná
výstav	výstava	k1gFnPc2	výstava
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
světovou	světový	k2eAgFnSc7d1	světová
výstavou	výstava	k1gFnSc7	výstava
Expo	Expo	k1gNnSc1	Expo
1958	[number]	k4	1958
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
natočil	natočit	k5eAaBmAgMnS	natočit
Jiří	Jiří	k1gMnSc1	Jiří
Lehovec	Lehovec	k1gMnSc1	Lehovec
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
filmový	filmový	k2eAgInSc1d1	filmový
dokument	dokument	k1gInSc1	dokument
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Zlatý	zlatý	k2eAgInSc1d1	zlatý
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výstavě	výstava	k1gFnSc6	výstava
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
podrobeny	podroben	k2eAgInPc1d1	podroben
moderním	moderní	k2eAgFnPc3d1	moderní
metodám	metoda	k1gFnPc3	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zjišťována	zjišťován	k2eAgFnSc1d1	zjišťována
ryzost	ryzost	k1gFnSc1	ryzost
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
překvapivým	překvapivý	k2eAgNnPc3d1	překvapivé
zjištěním	zjištění	k1gNnPc3	zjištění
týkajícím	týkající	k2eAgFnPc3d1	týkající
se	se	k3xPyFc4	se
některých	některý	k3yIgInPc2	některý
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
jejich	jejich	k3xOp3gInSc2	jejich
původu	původ	k1gInSc2	původ
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgNnSc1d2	podrobnější
zkoumání	zkoumání	k1gNnSc1	zkoumání
také	také	k9	také
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
lépe	dobře	k6eAd2	dobře
datovat	datovat	k5eAaImF	datovat
dobu	doba	k1gFnSc4	doba
vzniku	vznik	k1gInSc2	vznik
žezla	žezlo	k1gNnSc2	žezlo
a	a	k8xC	a
jablka	jablko	k1gNnSc2	jablko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vystavení	vystavení	k1gNnSc6	vystavení
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
moderní	moderní	k2eAgInSc1d1	moderní
nedestruktivní	destruktivní	k2eNgInSc1d1	nedestruktivní
průzkum	průzkum	k1gInSc1	průzkum
a	a	k8xC	a
digitální	digitální	k2eAgFnSc1d1	digitální
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
naskenována	naskenovat	k5eAaPmNgFnS	naskenovat
a	a	k8xC	a
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
přesná	přesný	k2eAgFnSc1d1	přesná
digitální	digitální	k2eAgFnSc1d1	digitální
kopie	kopie	k1gFnSc1	kopie
navždy	navždy	k6eAd1	navždy
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
stav	stav	k1gInSc1	stav
vzácného	vzácný	k2eAgInSc2d1	vzácný
klenotu	klenot	k1gInSc2	klenot
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zatím	zatím	k6eAd1	zatím
nejpřesnější	přesný	k2eAgFnSc1d3	nejpřesnější
a	a	k8xC	a
nejpodrobnější	podrobný	k2eAgFnSc1d3	nejpodrobnější
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odborníkům	odborník	k1gMnPc3	odborník
další	další	k2eAgNnSc4d1	další
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
fyzického	fyzický	k2eAgInSc2d1	fyzický
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
těžko	těžko	k6eAd1	těžko
dostupné	dostupný	k2eAgFnSc3d1	dostupná
památce	památka	k1gFnSc3	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavy	výstava	k1gFnPc4	výstava
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
přání	přání	k1gNnPc2	přání
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
vyjímány	vyjímán	k2eAgInPc1d1	vyjímán
z	z	k7c2	z
úschovy	úschova	k1gFnSc2	úschova
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
výjimečných	výjimečný	k2eAgFnPc6d1	výjimečná
příležitostech	příležitost	k1gFnPc6	příležitost
–	–	k?	–
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
korunovace	korunovace	k1gFnSc1	korunovace
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
vystaveny	vystaven	k2eAgInPc1d1	vystaven
během	během	k7c2	během
pohřbu	pohřeb	k1gInSc2	pohřeb
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k8xC	i
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Císař	Císař	k1gMnSc1	Císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nenechal	nechat	k5eNaPmAgMnS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
českých	český	k2eAgMnPc2d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
korunovace	korunovace	k1gFnSc2	korunovace
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
klenoty	klenot	k1gInPc4	klenot
vystavit	vystavit	k5eAaPmF	vystavit
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
klenotnici	klenotnice	k1gFnSc6	klenotnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
Vídně	Vídeň	k1gFnSc2	Vídeň
i	i	k9	i
251	[number]	k4	251
<g/>
.	.	kIx.	.
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1775	[number]	k4	1775
<g/>
–	–	k?	–
<g/>
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
císař	císař	k1gMnSc1	císař
ukázal	ukázat	k5eAaPmAgMnS	ukázat
říšské	říšský	k2eAgInPc4d1	říšský
a	a	k8xC	a
české	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
veřejná	veřejný	k2eAgFnSc1d1	veřejná
výstava	výstava	k1gFnSc1	výstava
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
smrti	smrt	k1gFnSc2	smrt
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
celkem	celkem	k6eAd1	celkem
devětkrát	devětkrát	k6eAd1	devětkrát
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
výstava	výstava	k1gFnSc1	výstava
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
100	[number]	k4	100
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
může	moct	k5eAaImIp3nS	moct
výstavu	výstava	k1gFnSc4	výstava
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
maximálně	maximálně	k6eAd1	maximálně
5	[number]	k4	5
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posledních	poslední	k2eAgFnPc6d1	poslední
výstavách	výstava	k1gFnPc6	výstava
si	se	k3xPyFc3	se
klenoty	klenot	k1gInPc7	klenot
prohlédlo	prohlédnout	k5eAaPmAgNnS	prohlédnout
30	[number]	k4	30
000	[number]	k4	000
až	až	k9	až
45	[number]	k4	45
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
44	[number]	k4	44
475	[number]	k4	475
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
32	[number]	k4	32
050	[number]	k4	050
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
kratší	krátký	k2eAgFnSc4d2	kratší
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
31	[number]	k4	31
237	[number]	k4	237
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
O	o	k7c4	o
vystavení	vystavení	k1gNnSc4	vystavení
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Výstavy	výstava	k1gFnPc1	výstava
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
území	území	k1gNnSc6	území
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vystavování	vystavování	k1gNnSc3	vystavování
klenotů	klenot	k1gInPc2	klenot
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
využívala	využívat	k5eAaPmAgFnS	využívat
vitrína	vitrína	k1gFnSc1	vitrína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Gočára	Gočár	k1gMnSc2	Gočár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novou	nový	k2eAgFnSc7d1	nová
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
vitrínou	vitrína	k1gFnSc7	vitrína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věrné	věrný	k2eAgFnPc4d1	věrná
repliky	replika	k1gFnPc4	replika
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zpřístupnění	zpřístupnění	k1gNnSc4	zpřístupnění
širšímu	široký	k2eAgNnSc3d2	širší
publiku	publikum	k1gNnSc3	publikum
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
myšlenka	myšlenka	k1gFnSc1	myšlenka
vytvoření	vytvoření	k1gNnSc2	vytvoření
věrných	věrný	k2eAgFnPc2d1	věrná
replik	replika	k1gFnPc2	replika
českých	český	k2eAgInPc2d1	český
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
následující	následující	k2eAgNnSc4d1	následující
(	(	kIx(	(
<g/>
výčet	výčet	k1gInSc4	výčet
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
kompletní	kompletní	k2eAgInPc1d1	kompletní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
pro	pro	k7c4	pro
EXPO	Expo	k1gNnSc4	Expo
67V	[number]	k4	67V
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
Střední	střední	k2eAgFnSc1d1	střední
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
požádána	požádat	k5eAaPmNgFnS	požádat
o	o	k7c6	o
výrobu	výrob	k1gInSc6	výrob
kopie	kopie	k1gFnSc2	kopie
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
pro	pro	k7c4	pro
světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
EXPO	Expo	k1gNnSc4	Expo
67	[number]	k4	67
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
zlatníci	zlatník	k1gMnPc1	zlatník
Jiří	Jiří	k1gMnSc1	Jiří
Belda	Belda	k1gMnSc1	Belda
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jambor	Jambor	k1gMnSc1	Jambor
<g/>
,	,	kIx,	,
stříbrník	stříbrník	k1gMnSc1	stříbrník
Karel	Karel	k1gMnSc1	Karel
Zouplna	Zouplna	k?	Zouplna
a	a	k8xC	a
brusič	brusič	k1gMnSc1	brusič
drahokamů	drahokam	k1gInPc2	drahokam
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Šonský	Šonský	k1gMnSc1	Šonský
(	(	kIx(	(
<g/>
také	také	k9	také
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Prášil	Prášil	k1gMnSc1	Prášil
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Žatečka	žatečka	k1gFnSc1	žatečka
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kopie	kopie	k1gFnSc1	kopie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
originálu	originál	k1gInSc2	originál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
autorům	autor	k1gMnPc3	autor
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
patrně	patrně	k6eAd1	patrně
nejdokonalejší	dokonalý	k2eAgFnSc4d3	nejdokonalejší
existující	existující	k2eAgFnSc4d1	existující
repliku	replika	k1gFnSc4	replika
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výstavě	výstava	k1gFnSc6	výstava
EXPO	Expo	k1gNnSc1	Expo
67	[number]	k4	67
byla	být	k5eAaImAgFnS	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
<g/>
,	,	kIx,	,
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Leningradu	Leningrad	k1gInSc2	Leningrad
<g/>
,	,	kIx,	,
Budapešti	Budapešť	k1gFnSc6	Budapešť
a	a	k8xC	a
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
a	a	k8xC	a
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
koruna	koruna	k1gFnSc1	koruna
doplněna	doplnit	k5eAaPmNgFnS	doplnit
replikou	replika	k1gFnSc7	replika
královského	královský	k2eAgNnSc2d1	královské
žezla	žezlo	k1gNnSc2	žezlo
a	a	k8xC	a
královského	královský	k2eAgNnSc2d1	královské
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Komplet	komplet	k1gInSc1	komplet
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
repliky	replika	k1gFnPc1	replika
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
sbírek	sbírka	k1gFnPc2	sbírka
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2017	[number]	k4	2017
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
kapli	kaple	k1gFnSc6	kaple
Rožmberského	rožmberský	k2eAgInSc2d1	rožmberský
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
vstup	vstup	k1gInSc1	vstup
z	z	k7c2	z
Jiřské	jiřský	k2eAgFnSc2d1	Jiřská
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
hradu	hrad	k1gInSc2	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
realizoval	realizovat	k5eAaBmAgInS	realizovat
kolektiv	kolektiv	k1gInSc1	kolektiv
šperkařů	šperkař	k1gMnPc2	šperkař
Střední	střední	k2eAgFnSc2d1	střední
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
profesora	profesor	k1gMnSc2	profesor
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Prášila	prášit	k5eAaImAgFnS	prášit
repliku	replika	k1gFnSc4	replika
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruny	koruna	k1gFnPc1	koruna
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
19	[number]	k4	19
<g/>
×	×	k?	×
<g/>
19	[number]	k4	19
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
osázena	osázet	k5eAaPmNgFnS	osázet
umělými	umělý	k2eAgInPc7d1	umělý
safíry	safír	k1gInPc7	safír
<g/>
,	,	kIx,	,
rubíny	rubín	k1gInPc7	rubín
a	a	k8xC	a
perlami	perla	k1gFnPc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
replika	replika	k1gFnSc1	replika
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
i	i	k9	i
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
Hrady	hrad	k1gInPc4	hrad
a	a	k8xC	a
zámky	zámek	k1gInPc4	zámek
objevované	objevovaný	k2eAgInPc4d1	objevovaný
a	a	k8xC	a
opěvované	opěvovaný	k2eAgMnPc4d1	opěvovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
bývá	bývat	k5eAaImIp3nS	bývat
zapůjčována	zapůjčován	k2eAgFnSc1d1	zapůjčována
pouze	pouze	k6eAd1	pouze
krátkodobě	krátkodobě	k6eAd1	krátkodobě
zhruba	zhruba	k6eAd1	zhruba
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
rok	rok	k1gInSc4	rok
až	až	k6eAd1	až
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
2000	[number]	k4	2000
<g/>
Repliku	replika	k1gFnSc4	replika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
společně	společně	k6eAd1	společně
zlatníci	zlatník	k1gMnPc1	zlatník
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Jiří	Jiří	k1gMnSc1	Jiří
Beldovi	Belda	k1gMnSc3	Belda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
sbírek	sbírka	k1gFnPc2	sbírka
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
březnu	březen	k1gInSc3	březen
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
expozici	expozice	k1gFnSc6	expozice
Příběh	příběh	k1gInSc1	příběh
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
pro	pro	k7c4	pro
výstavu	výstava	k1gFnSc4	výstava
Praha	Praha	k1gFnSc1	Praha
lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
zlatník	zlatník	k1gMnSc1	zlatník
Jiří	Jiří	k1gMnSc1	Jiří
Urban	Urban	k1gMnSc1	Urban
požádán	požádat	k5eAaPmNgMnS	požádat
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
repliky	replika	k1gFnSc2	replika
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
výstavy	výstava	k1gFnSc2	výstava
Praha	Praha	k1gFnSc1	Praha
lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
připravená	připravený	k2eAgFnSc1d1	připravená
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
přípravu	příprava	k1gFnSc4	příprava
méně	málo	k6eAd2	málo
času	čas	k1gInSc2	čas
než	než	k8xS	než
na	na	k7c4	na
repliku	replika	k1gFnSc4	replika
říšské	říšský	k2eAgFnSc2d1	říšská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
Urban	Urban	k1gMnSc1	Urban
zabýval	zabývat	k5eAaImAgMnS	zabývat
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
korunu	koruna	k1gFnSc4	koruna
doplnil	doplnit	k5eAaPmAgMnS	doplnit
replikou	replika	k1gFnSc7	replika
královského	královský	k2eAgNnSc2d1	královské
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgNnSc4d1	královské
žezlo	žezlo	k1gNnSc4	žezlo
připravili	připravit	k5eAaPmAgMnP	připravit
Blanka	Blanka	k1gFnSc1	Blanka
a	a	k8xC	a
Matúš	Matúš	k1gMnSc1	Matúš
Cepkovi	Cepek	k1gMnSc3	Cepek
působící	působící	k2eAgFnSc2d1	působící
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
repliky	replika	k1gFnPc1	replika
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
zlaceného	zlacený	k2eAgNnSc2d1	zlacené
stříbra	stříbro	k1gNnSc2	stříbro
doplněné	doplněný	k2eAgInPc1d1	doplněný
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
imitacemi	imitace	k1gFnPc7	imitace
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
Pražské	pražský	k2eAgFnSc2d1	Pražská
mincovny	mincovna	k1gFnSc2	mincovna
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
<g/>
)	)	kIx)	)
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
Pražská	pražský	k2eAgFnSc1d1	Pražská
mincovna	mincovna	k1gFnSc1	mincovna
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Obecním	obecní	k2eAgInSc6d1	obecní
domě	dům	k1gInSc6	dům
repliku	replika	k1gFnSc4	replika
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
od	od	k7c2	od
zlatníka	zlatník	k1gMnSc2	zlatník
Jiřího	Jiří	k1gMnSc2	Jiří
Urbana	Urban	k1gMnSc2	Urban
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
replika	replika	k1gFnSc1	replika
byla	být	k5eAaImAgFnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
nebo	nebo	k8xC	nebo
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
pro	pro	k7c4	pro
výstavu	výstava	k1gFnSc4	výstava
České	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
Repliku	replika	k1gFnSc4	replika
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
královského	královský	k2eAgNnSc2d1	královské
žezla	žezlo	k1gNnSc2	žezlo
a	a	k8xC	a
jablka	jablko	k1gNnSc2	jablko
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
OC	OC	kA	OC
šestka	šestka	k1gFnSc1	šestka
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
zlatník	zlatník	k1gMnSc1	zlatník
Jiří	Jiří	k1gMnSc1	Jiří
Urban	Urban	k1gMnSc1	Urban
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgFnP	být
vystavené	vystavená	k1gFnPc1	vystavená
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
a	a	k8xC	a
zámků	zámek	k1gInPc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobeny	vyroben	k2eAgFnPc1d1	vyrobena
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
pozlaceného	pozlacený	k2eAgNnSc2d1	pozlacené
stříbra	stříbro	k1gNnSc2	stříbro
doplněného	doplněný	k2eAgNnSc2d1	doplněné
o	o	k7c4	o
skleněné	skleněný	k2eAgFnPc4d1	skleněná
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
3	[number]	k4	3
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
a	a	k8xC	a
2017	[number]	k4	2017
byly	být	k5eAaImAgFnP	být
doplněné	doplněný	k2eAgInPc1d1	doplněný
replikou	replika	k1gFnSc7	replika
svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
meče	meč	k1gInSc2	meč
od	od	k7c2	od
mečíře	mečíř	k1gMnSc2	mečíř
Patricka	Patricko	k1gNnSc2	Patricko
Bárty	Bárta	k1gMnSc2	Bárta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
pro	pro	k7c4	pro
výstavu	výstava	k1gFnSc4	výstava
Magičtí	magický	k2eAgMnPc1d1	magický
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
Repliku	replika	k1gFnSc4	replika
pro	pro	k7c4	pro
putovní	putovní	k2eAgFnSc4d1	putovní
výstavu	výstava	k1gFnSc4	výstava
Magičtí	magický	k2eAgMnPc1d1	magický
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
zahájenou	zahájený	k2eAgFnSc4d1	zahájená
17	[number]	k4	17
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
objednal	objednat	k5eAaPmAgMnS	objednat
u	u	k7c2	u
českého	český	k2eAgMnSc2d1	český
zlatníka	zlatník	k1gMnSc2	zlatník
Jiřího	Jiří	k1gMnSc2	Jiří
Urbana	Urban	k1gMnSc2	Urban
autor	autor	k1gMnSc1	autor
projektu	projekt	k1gInSc2	projekt
výstavy	výstava	k1gFnSc2	výstava
Oldřich	Oldřich	k1gMnSc1	Oldřich
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
český	český	k2eAgMnSc1d1	český
šperkař	šperkař	k1gMnSc1	šperkař
Jiří	Jiří	k1gMnSc1	Jiří
Belda	Belda	k1gMnSc1	Belda
(	(	kIx(	(
<g/>
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
)	)	kIx)	)
repliku	replika	k1gFnSc4	replika
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
pro	pro	k7c4	pro
Středočeský	středočeský	k2eAgInSc4d1	středočeský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
pozlaceného	pozlacený	k2eAgNnSc2d1	pozlacené
stříbra	stříbro	k1gNnSc2	stříbro
s	s	k7c7	s
ryzostí	ryzost	k1gFnSc7	ryzost
925	[number]	k4	925
tisícin	tisícina	k1gFnPc2	tisícina
osázeného	osázený	k2eAgInSc2d1	osázený
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
imitacemi	imitace	k1gFnPc7	imitace
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
imitacemi	imitace	k1gFnPc7	imitace
čtyř	čtyři	k4xCgFnPc2	čtyři
vrcholových	vrcholový	k2eAgFnPc2d1	vrcholová
perel	perla	k1gFnPc2	perla
a	a	k8xC	a
šestnácti	šestnáct	k4xCc7	šestnáct
pravými	pravý	k2eAgFnPc7d1	pravá
říčními	říční	k2eAgFnPc7d1	říční
perlami	perla	k1gFnPc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
1,8	[number]	k4	1,8
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
,	,	kIx,	,
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
950	[number]	k4	950
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
konečná	konečný	k2eAgFnSc1d1	konečná
včetně	včetně	k7c2	včetně
DPH	DPH	kA	DPH
1,149	[number]	k4	1,149
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
plánuje	plánovat	k5eAaImIp3nS	plánovat
doplnit	doplnit	k5eAaPmF	doplnit
i	i	k9	i
repliku	replika	k1gFnSc4	replika
královského	královský	k2eAgNnSc2d1	královské
jablka	jablko	k1gNnSc2	jablko
a	a	k8xC	a
žezla	žezlo	k1gNnSc2	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Kopiemi	kopie	k1gFnPc7	kopie
klenotů	klenot	k1gInPc2	klenot
obohatí	obohatit	k5eAaPmIp3nP	obohatit
výstavy	výstava	k1gFnPc1	výstava
v	v	k7c6	v
kutnohorské	kutnohorský	k2eAgFnSc6d1	Kutnohorská
galerii	galerie	k1gFnSc6	galerie
a	a	k8xC	a
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
korunovací	korunovace	k1gFnPc2	korunovace
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
a	a	k8xC	a
královen	královna	k1gFnPc2	královna
==	==	k?	==
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
korunovace	korunovace	k1gFnSc1	korunovace
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatováclavskou	svatováclavský	k2eAgFnSc7d1	Svatováclavská
korunou	koruna	k1gFnSc7	koruna
bylo	být	k5eAaImAgNnS	být
korunováno	korunovat	k5eAaBmNgNnS	korunovat
21	[number]	k4	21
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
a	a	k8xC	a
1	[number]	k4	1
česká	český	k2eAgFnSc1d1	Česká
královna-panovnice	královnaanovnice	k1gFnSc1	královna-panovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAUER	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Vládci	vládce	k1gMnPc1	vládce
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
panovnického	panovnický	k2eAgInSc2d1	panovnický
trůnu	trůn	k1gInSc2	trůn
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Beta	beta	k1gNnSc1	beta
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7306	[number]	k4	7306
<g/>
-	-	kIx~	-
<g/>
168	[number]	k4	168
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BĚLINA	Bělina	k1gMnSc1	Bělina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
Print	Printa	k1gFnPc2	Printa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7321	[number]	k4	7321
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BONĚK	BONĚK	kA	BONĚK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
BONĚK	BONĚK	kA	BONĚK
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
:	:	kIx,	:
Neznámá	známý	k2eNgFnSc1d1	neznámá
historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
</s>
<s>
Skrytá	skrytý	k2eAgNnPc1d1	skryté
poselství	poselství	k1gNnPc1	poselství
<g/>
.	.	kIx.	.
</s>
<s>
Zapomenuté	zapomenutý	k2eAgInPc4d1	zapomenutý
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Eminent	Eminent	k1gMnSc1	Eminent
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7281	[number]	k4	7281
<g/>
-	-	kIx~	-
<g/>
219	[number]	k4	219
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
BRAVERMANOVÁ	BRAVERMANOVÁ	kA	BRAVERMANOVÁ
<g/>
,	,	kIx,	,
Milena	Milena	k1gFnSc1	Milena
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
rozš	rozš	k1gInSc1	rozš
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Správa	správa	k1gFnSc1	správa
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
nakl	nakla	k1gFnPc2	nakla
<g/>
.	.	kIx.	.
</s>
<s>
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
88	[number]	k4	88
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7381	[number]	k4	7381
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAROLÍMKOVÁ	JAROLÍMKOVÁ	kA	JAROLÍMKOVÁ
<g/>
,	,	kIx,	,
Stanislava	Stanislav	k1gMnSc2	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
v	v	k7c6	v
učebnicích	učebnice	k1gFnPc6	učebnice
nebývá	bývat	k5eNaImIp3nS	bývat
aneb	aneb	k?	aneb
Čeští	český	k2eAgMnPc1d1	český
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
možná	možná	k9	možná
<g/>
)	)	kIx)	)
neznáte	znát	k5eNaImIp2nP	znát
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Motto	motto	k1gNnSc1	motto
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7246	[number]	k4	7246
<g/>
-	-	kIx~	-
<g/>
310	[number]	k4	310
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAROLÍMKOVÁ	JAROLÍMKOVÁ	kA	JAROLÍMKOVÁ
<g/>
,	,	kIx,	,
Stanislava	Stanislav	k1gMnSc2	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
v	v	k7c6	v
učebnicích	učebnice	k1gFnPc6	učebnice
nebývá	bývat	k5eNaImIp3nS	bývat
aneb	aneb	k?	aneb
Čeští	český	k2eAgMnPc1d1	český
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
možná	možná	k9	možná
<g/>
)	)	kIx)	)
neznáte	neznat	k5eAaImIp2nP	neznat
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Motto	motto	k1gNnSc1	motto
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7246	[number]	k4	7246
<g/>
-	-	kIx~	-
<g/>
312	[number]	k4	312
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHYTIL	Chytil	k1gMnSc1	Chytil
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
VRBA	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
PODLAHA	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Archeologická	archeologický	k2eAgFnSc1d1	archeologická
komise	komise	k1gFnSc1	komise
při	při	k7c6	při
České	český	k2eAgFnSc6d1	Česká
Akademii	akademie	k1gFnSc6	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VÁCHA	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Neue	Neue	k1gFnSc1	Neue
Erkenntnisse	Erkenntnisse	k1gFnSc2	Erkenntnisse
zum	zum	k?	zum
Reichsapfel	Reichsapfel	k1gInSc1	Reichsapfel
Ferdinands	Ferdinands	k1gInSc1	Ferdinands
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
Rudolphina	Rudolphina	k1gFnSc1	Rudolphina
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
53	[number]	k4	53
<g/>
–	–	k?	–
<g/>
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903230	[number]	k4	903230
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VÁCHA	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Repräsentations-	Repräsentations-	k?	Repräsentations-
oder	odra	k1gFnPc2	odra
Krönungsornat	Krönungsornat	k1gMnSc1	Krönungsornat
<g/>
?	?	kIx.	?
</s>
<s>
Zum	Zum	k?	Zum
Ursprung	Ursprung	k1gInSc1	Ursprung
und	und	k?	und
zur	zur	k?	zur
Funktion	Funktion	k1gInSc1	Funktion
des	des	k1gNnSc1	des
Zeremonialgewands	Zeremonialgewandsa	k1gFnPc2	Zeremonialgewandsa
Ferdinands	Ferdinandsa	k1gFnPc2	Ferdinandsa
IV	IV	kA	IV
<g/>
.	.	kIx.	.
aus	aus	k?	aus
dem	dem	k?	dem
Jahre	Jahr	k1gInSc5	Jahr
1653	[number]	k4	1653
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
54	[number]	k4	54
<g/>
,	,	kIx,	,
s.	s.	k?	s.
229	[number]	k4	229
<g/>
–	–	k?	–
<g/>
239	[number]	k4	239
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
5123	[number]	k4	5123
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
a	a	k8xC	a
česky	česky	k6eAd1	česky
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
verze	verze	k1gFnSc1	verze
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
VÁCHA	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
;	;	kIx,	;
VESELÁ	Veselá	k1gFnSc1	Veselá
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
;	;	kIx,	;
VOKÁČOVÁ	Vokáčová	k1gFnSc1	Vokáčová
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
korunovace	korunovace	k1gFnSc1	korunovace
1723	[number]	k4	1723
<g/>
.	.	kIx.	.
</s>
<s>
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7035	[number]	k4	7035
<g/>
-	-	kIx~	-
<g/>
428	[number]	k4	428
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
173	[number]	k4	173
<g/>
–	–	k?	–
<g/>
212	[number]	k4	212
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
výzkumu	výzkum	k1gInSc6	výzkum
drahokamů	drahokam	k1gInPc2	drahokam
J.	J.	kA	J.
Hyršla	Hyršla	k1gFnSc1	Hyršla
a	a	k8xC	a
P.	P.	kA	P.
<g/>
Malíkové	Malíkové	k2eAgMnSc1d1	Malíkové
ohledně	ohledně	k7c2	ohledně
svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Klásek	klásek	k1gInSc1	klásek
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Minerál	minerál	k1gInSc1	minerál
o	o	k7c6	o
podobnostech	podobnost	k1gFnPc6	podobnost
mezi	mezi	k7c7	mezi
vrtanými	vrtaný	k2eAgInPc7d1	vrtaný
starověkými	starověký	k2eAgInPc7d1	starověký
safírovými	safírový	k2eAgInPc7d1	safírový
korály	korál	k1gInPc7	korál
a	a	k8xC	a
kameny	kámen	k1gInPc7	kámen
na	na	k7c6	na
korunovačních	korunovační	k2eAgInPc6d1	korunovační
klenotech	klenot	k1gInPc6	klenot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Tykra	Tykra	k1gMnSc1	Tykra
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
obelisk	obelisk	k1gInSc1	obelisk
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
</s>
</p>
<p>
<s>
CD	CD	kA	CD
záznam	záznam	k1gInSc1	záznam
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Legendy	legenda	k1gFnPc1	legenda
v	v	k7c6	v
díle	díl	k1gInSc6	díl
Al-Birúního	Al-Birúní	k1gMnSc2	Al-Birúní
o	o	k7c6	o
badachšánských	badachšánský	k2eAgInPc6d1	badachšánský
spinelech	spinel	k1gInPc6	spinel
<g/>
,	,	kIx,	,
badachšánském	badachšánský	k2eAgNnSc6d1	badachšánský
lálu	lálum	k1gNnSc6	lálum
a	a	k8xC	a
původ	původ	k1gInSc1	původ
spinelů	spinel	k1gInPc2	spinel
českých	český	k2eAgInPc2d1	český
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
okolo	okolo	k7c2	okolo
řeky	řeka	k1gFnSc2	řeka
Pjandž	Pjandž	k1gFnSc1	Pjandž
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Pamiru	Pamir	k1gInSc2	Pamir
a	a	k8xC	a
Hindukúše	Hindukúše	k1gFnSc2	Hindukúše
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
České	český	k2eAgFnSc2d1	Česká
korunovační	korunovační	k2eAgFnSc2d1	korunovační
klenoty	klenot	k1gInPc7	klenot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
HASLINGEROVÁ	HASLINGEROVÁ	kA	HASLINGEROVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
liturgický	liturgický	k2eAgInSc1d1	liturgický
kontext	kontext	k1gInSc1	kontext
korunovace	korunovace	k1gFnSc1	korunovace
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2008-05-09	[number]	k4	2008-05-09
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
publikace	publikace	k1gFnSc2	publikace
od	od	k7c2	od
Andreje	Andrej	k1gMnSc2	Andrej
Šumbery	Šumbera	k1gFnSc2	Šumbera
</s>
</p>
<p>
<s>
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
výstavou	výstava	k1gFnSc7	výstava
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
iDnes	iDnesa	k1gFnPc2	iDnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
</p>
