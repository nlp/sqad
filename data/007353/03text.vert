<s>
Luminiscence	luminiscence	k1gFnPc4	luminiscence
je	být	k5eAaImIp3nS	být
spontánní	spontánní	k2eAgNnSc1d1	spontánní
(	(	kIx(	(
<g/>
samovolné	samovolný	k2eAgNnSc4d1	samovolné
<g/>
)	)	kIx)	)
záření	záření	k1gNnSc4	záření
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
<g/>
)	)	kIx)	)
pevných	pevný	k2eAgFnPc2d1	pevná
nebo	nebo	k8xC	nebo
kapalných	kapalný	k2eAgFnPc2d1	kapalná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k8xS	jako
přebytek	přebytek	k1gInSc1	přebytek
záření	záření	k1gNnSc2	záření
tělesa	těleso	k1gNnSc2	těleso
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
jeho	jeho	k3xOp3gNnSc2	jeho
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
spektrální	spektrální	k2eAgFnSc6d1	spektrální
oblasti	oblast	k1gFnSc6	oblast
při	při	k7c6	při
dané	daný	k2eAgFnSc6d1	daná
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
má	mít	k5eAaImIp3nS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
doznívání	doznívání	k1gNnSc2	doznívání
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
budícího	budící	k2eAgInSc2d1	budící
účinku	účinek	k1gInSc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
záření	záření	k1gNnSc6	záření
o	o	k7c6	o
kratší	krátký	k2eAgFnSc6d2	kratší
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc4d2	veliký
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
)	)	kIx)	)
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
určitého	určitý	k2eAgNnSc2d1	určité
složení	složení	k1gNnSc2	složení
vznik	vznik	k1gInSc4	vznik
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
delší	dlouhý	k2eAgFnSc6d2	delší
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnSc4d2	nižší
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
)	)	kIx)	)
-	-	kIx~	-
jakýsi	jakýsi	k3yIgInSc1	jakýsi
"	"	kIx"	"
<g/>
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Luminiscence	luminiscence	k1gFnSc1	luminiscence
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
–	–	k?	–
např.	např.	kA	např.
u	u	k7c2	u
světlušek	světluška	k1gFnPc2	světluška
nebo	nebo	k8xC	nebo
medúz	medúza	k1gFnPc2	medúza
–	–	k?	–
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
bioluminiscence	bioluminiscence	k1gFnSc1	bioluminiscence
<g/>
.	.	kIx.	.
</s>
<s>
Luminiscence	luminiscence	k1gFnSc1	luminiscence
vzniká	vznikat	k5eAaImIp3nS	vznikat
excitací	excitace	k1gFnSc7	excitace
atomu	atom	k1gInSc2	atom
působením	působení	k1gNnSc7	působení
jiného	jiný	k2eAgNnSc2d1	jiné
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
elektronů	elektron	k1gInPc2	elektron
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
návratem	návrat	k1gInSc7	návrat
atomu	atom	k1gInSc2	atom
do	do	k7c2	do
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vyzáření	vyzáření	k1gNnSc3	vyzáření
fotonu	foton	k1gInSc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
Luminiscenci	luminiscence	k1gFnSc4	luminiscence
látky	látka	k1gFnSc2	látka
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
pozorovat	pozorovat	k5eAaImF	pozorovat
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
ozáření	ozáření	k1gNnSc6	ozáření
jiným	jiný	k2eAgInSc7d1	jiný
zdrojem	zdroj	k1gInSc7	zdroj
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
zdroje	zdroj	k1gInSc2	zdroj
ozařování	ozařování	k1gNnSc2	ozařování
látky	látka	k1gFnSc2	látka
luminiscence	luminiscence	k1gFnSc2	luminiscence
vymizí	vymizet	k5eAaPmIp3nS	vymizet
(	(	kIx(	(
<g/>
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
nanosekund	nanosekunda	k1gFnPc2	nanosekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
fluorescenci	fluorescence	k1gFnSc6	fluorescence
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
luminiscence	luminiscence	k1gFnSc1	luminiscence
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
i	i	k9	i
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
zdroje	zdroj	k1gInSc2	zdroj
ozařování	ozařování	k1gNnSc2	ozařování
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fosforescenci	fosforescence	k1gFnSc4	fosforescence
<g/>
.	.	kIx.	.
</s>
<s>
Fluorescence	fluorescence	k1gFnSc1	fluorescence
je	být	k5eAaImIp3nS	být
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
povolenými	povolený	k2eAgInPc7d1	povolený
stavy	stav	k1gInPc7	stav
atomu	atom	k1gInSc2	atom
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jí	on	k3xPp3gFnSc3	on
nic	nic	k6eAd1	nic
nebrání	bránit	k5eNaImIp3nS	bránit
ve	v	k7c4	v
vypouštění	vypouštění	k1gNnSc4	vypouštění
fotonů	foton	k1gInPc2	foton
již	již	k6eAd1	již
za	za	k7c4	za
pár	pár	k4xCyI	pár
nanosekund	nanosekunda	k1gFnPc2	nanosekunda
<g/>
.	.	kIx.	.
</s>
<s>
Fosforescence	fosforescence	k1gFnSc1	fosforescence
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
přechod	přechod	k1gInSc1	přechod
tzv.	tzv.	kA	tzv.
zakázaný	zakázaný	k2eAgInSc1d1	zakázaný
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
duálnímu	duální	k2eAgInSc3d1	duální
charakteru	charakter	k1gInSc3	charakter
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
principu	princip	k1gInSc2	princip
neurčitosti	neurčitost	k1gFnSc2	neurčitost
existuje	existovat	k5eAaImIp3nS	existovat
jistá	jistý	k2eAgFnSc1d1	jistá
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektron	elektron	k1gInSc1	elektron
překoná	překonat	k5eAaPmIp3nS	překonat
potenciálovou	potenciálový	k2eAgFnSc4d1	potenciálová
bariéru	bariéra	k1gFnSc4	bariéra
zakázaného	zakázaný	k2eAgInSc2d1	zakázaný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
přesune	přesunout	k5eAaPmIp3nS	přesunout
se	se	k3xPyFc4	se
do	do	k7c2	do
energeticky	energeticky	k6eAd1	energeticky
nižší	nízký	k2eAgFnSc2d2	nižší
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
foton	foton	k1gInSc1	foton
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
se	s	k7c7	s
zvyšujícím	zvyšující	k2eAgMnSc7d1	zvyšující
se	se	k3xPyFc4	se
časem	čas	k1gInSc7	čas
stráveným	strávený	k2eAgInPc3d1	strávený
v	v	k7c6	v
excitovaném	excitovaný	k2eAgInSc6d1	excitovaný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
fosforescence	fosforescence	k1gFnSc1	fosforescence
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zdrojích	zdroj	k1gInPc6	zdroj
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
neuplatňuje	uplatňovat	k5eNaImIp3nS	uplatňovat
jen	jen	k9	jen
tepelné	tepelný	k2eAgNnSc1d1	tepelné
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
luminiscence	luminiscence	k1gFnPc1	luminiscence
<g/>
.	.	kIx.	.
</s>
<s>
Setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
např.	např.	kA	např.
u	u	k7c2	u
zářivek	zářivka	k1gFnPc2	zářivka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
.	.	kIx.	.
</s>
<s>
Zářivka	zářivka	k1gFnSc1	zářivka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
trubicí	trubice	k1gFnSc7	trubice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
výboj	výboj	k1gInSc1	výboj
v	v	k7c6	v
plynu	plyn	k1gInSc6	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
světla	světlo	k1gNnSc2	světlo
zářivky	zářivka	k1gFnSc2	zářivka
však	však	k9	však
není	být	k5eNaImIp3nS	být
samotný	samotný	k2eAgInSc1d1	samotný
výboj	výboj	k1gInSc1	výboj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
oko	oko	k1gNnSc4	oko
neviditelné	viditelný	k2eNgNnSc4d1	neviditelné
<g/>
.	.	kIx.	.
</s>
<s>
Ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
vrstvu	vrstva	k1gFnSc4	vrstva
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
plocha	plocha	k1gFnSc1	plocha
trubice	trubice	k1gFnSc1	trubice
<g/>
,	,	kIx,	,
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
její	její	k3xOp3gFnSc4	její
luminiscenci	luminiscence	k1gFnSc4	luminiscence
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
látka	látka	k1gFnSc1	látka
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
viditelné	viditelný	k2eAgNnSc4d1	viditelné
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
nastává	nastávat	k5eAaImIp3nS	nastávat
luminiscence	luminiscence	k1gFnSc1	luminiscence
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
luminofory	luminofor	k1gInPc1	luminofor
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
pevné	pevný	k2eAgFnPc1d1	pevná
látky	látka	k1gFnPc1	látka
s	s	k7c7	s
příměsmi	příměs	k1gFnPc7	příměs
vytvářejícími	vytvářející	k2eAgFnPc7d1	vytvářející
tzv.	tzv.	kA	tzv.
luminiscenční	luminiscenční	k2eAgNnPc1d1	luminiscenční
centra	centrum	k1gNnPc1	centrum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ZnS	ZnS	k1gMnSc1	ZnS
<g/>
,	,	kIx,	,
CdS	CdS	k1gMnSc1	CdS
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
Ag	Ag	k1gMnSc1	Ag
<g/>
,	,	kIx,	,
Cu	Cu	k1gMnSc1	Cu
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hlinitan	hlinitan	k1gInSc1	hlinitan
strontnatý	strontnatý	k2eAgMnSc1d1	strontnatý
obsahující	obsahující	k2eAgNnSc4d1	obsahující
europium	europium	k1gNnSc4	europium
a	a	k8xC	a
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k9	i
dysprosium	dysprosium	k1gNnSc4	dysprosium
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
excitace	excitace	k1gFnSc2	excitace
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
luminiscence	luminiscence	k1gFnSc2	luminiscence
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
Fotoluminiscence	fotoluminiscence	k1gFnSc1	fotoluminiscence
–	–	k?	–
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
neionizujícím	ionizující	k2eNgNnSc7d1	neionizující
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
zářením	záření	k1gNnSc7	záření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zářivky	zářivka	k1gFnSc2	zářivka
<g/>
)	)	kIx)	)
Elektroluminiscence	elektroluminiscence	k1gFnSc1	elektroluminiscence
–	–	k?	–
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
nebo	nebo	k8xC	nebo
průchodem	průchod	k1gInSc7	průchod
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
např.	např.	kA	např.
elektroluminiscenční	elektroluminiscenční	k2eAgFnPc4d1	elektroluminiscenční
fólie	fólie	k1gFnPc4	fólie
nebo	nebo	k8xC	nebo
LED	LED	kA	LED
<g/>
)	)	kIx)	)
Katodoluminiscence	Katodoluminiscence	k1gFnSc1	Katodoluminiscence
–	–	k?	–
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
dopadajícími	dopadající	k2eAgInPc7d1	dopadající
elektrony	elektron	k1gInPc7	elektron
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stínítko	stínítko	k1gNnSc1	stínítko
televizní	televizní	k2eAgFnSc2d1	televizní
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Chemiluminiscence	chemiluminiscence	k1gFnPc1	chemiluminiscence
–	–	k?	–
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
chemickou	chemický	k2eAgFnSc7d1	chemická
reakcí	reakce	k1gFnSc7	reakce
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
bioluminiscence	bioluminiscence	k1gFnSc1	bioluminiscence
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
emise	emise	k1gFnPc4	emise
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
<g/>
)	)	kIx)	)
Termoluminiscence	Termoluminiscence	k1gFnSc1	Termoluminiscence
–	–	k?	–
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
vzrůstem	vzrůst	k1gInSc7	vzrůst
teploty	teplota	k1gFnSc2	teplota
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
dodání	dodání	k1gNnSc6	dodání
energie	energie	k1gFnSc2	energie
Radioluminiscence	radioluminiscence	k1gFnSc2	radioluminiscence
–	–	k?	–
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
působením	působení	k1gNnSc7	působení
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
Triboluminiscence	triboluminiscence	k1gFnSc2	triboluminiscence
–	–	k?	–
luminiscence	luminiscence	k1gFnSc1	luminiscence
je	být	k5eAaImIp3nS	být
vyvolána	vyvolán	k2eAgFnSc1d1	vyvolána
<g />
.	.	kIx.	.
</s>
<s>
působením	působení	k1gNnSc7	působení
tlaku	tlak	k1gInSc2	tlak
Luminiscence	luminiscence	k1gFnSc2	luminiscence
nalezla	naleznout	k5eAaPmAgFnS	naleznout
široké	široký	k2eAgNnSc4d1	široké
využití	využití	k1gNnSc4	využití
zejména	zejména	k9	zejména
v	v	k7c6	v
optoelektronice	optoelektronika	k1gFnSc6	optoelektronika
(	(	kIx(	(
<g/>
luminiscenční	luminiscenční	k2eAgFnSc2d1	luminiscenční
diody	dioda	k1gFnSc2	dioda
<g/>
,	,	kIx,	,
stínítka	stínítko	k1gNnSc2	stínítko
obrazovek	obrazovka	k1gFnPc2	obrazovka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
(	(	kIx(	(
<g/>
luminiscenční	luminiscenční	k2eAgFnPc1d1	luminiscenční
značky	značka	k1gFnPc1	značka
<g/>
,	,	kIx,	,
luminiscenční	luminiscenční	k2eAgFnPc1d1	luminiscenční
sondy	sonda	k1gFnPc1	sonda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
(	(	kIx(	(
<g/>
ke	k	k7c3	k
kvantitativnímu	kvantitativní	k2eAgNnSc3d1	kvantitativní
a	a	k8xC	a
kvalitativnímu	kvalitativní	k2eAgNnSc3d1	kvalitativní
určení	určení	k1gNnSc3	určení
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záření	záření	k1gNnSc1	záření
Luminofor	luminofor	k1gInSc4	luminofor
Bioluminiscence	Bioluminiscence	k1gFnSc2	Bioluminiscence
Fluorescenční	fluorescenční	k2eAgFnSc2d1	fluorescenční
indukce	indukce	k1gFnSc2	indukce
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
luminiscence	luminiscence	k1gFnSc2	luminiscence
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
luminiscence	luminiscence	k1gFnSc2	luminiscence
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
