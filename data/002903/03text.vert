<s>
KISS	KISS	kA	KISS
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
anglického	anglický	k2eAgInSc2d1	anglický
výrazu	výraz	k1gInSc2	výraz
Keep	Keep	k1gMnSc1	Keep
It	It	k1gMnSc1	It
Simple	Simple	k1gMnSc1	Simple
<g/>
,	,	kIx,	,
Stupid	Stupid	k1gInSc1	Stupid
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Zachovej	zachovat	k5eAaPmRp2nS	zachovat
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
hlupáku	hlupák	k1gMnSc5	hlupák
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nekomplikuj	komplikovat	k5eNaBmRp2nS	komplikovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
blbe	blb	k1gMnSc5	blb
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obecný	obecný	k2eAgInSc1d1	obecný
návrhový	návrhový	k2eAgInSc1d1	návrhový
vzor	vzor	k1gInSc1	vzor
aplikovatelný	aplikovatelný	k2eAgInSc1d1	aplikovatelný
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
řešení	řešení	k1gNnSc4	řešení
v	v	k7c6	v
nejrůznějších	různý	k2eAgNnPc6d3	nejrůznější
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
KISS	KISS	kA	KISS
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
systémů	systém	k1gInPc2	systém
pracuje	pracovat	k5eAaImIp3nS	pracovat
nejlépe	dobře	k6eAd3	dobře
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
udržována	udržovat	k5eAaImNgFnS	udržovat
spíše	spíše	k9	spíše
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
nežli	nežli	k8xS	nežli
komplexnější	komplexní	k2eAgFnSc1d2	komplexnější
-	-	kIx~	-
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
by	by	kYmCp3nS	by
proto	proto	k6eAd1	proto
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
a	a	k8xC	a
zbytečné	zbytečný	k2eAgFnSc6d1	zbytečná
komplexitě	komplexita	k1gFnSc6	komplexita
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vývojáři	vývojář	k1gMnPc1	vývojář
by	by	kYmCp3nP	by
pak	pak	k6eAd1	pak
měli	mít	k5eAaImAgMnP	mít
myslet	myslet	k5eAaImF	myslet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
zaplaceni	zaplacen	k2eAgMnPc1d1	zaplacen
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
měli	mít	k5eAaImAgMnP	mít
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
ne	ne	k9	ne
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
udělali	udělat	k5eAaPmAgMnP	udělat
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
princip	princip	k1gInSc4	princip
poprvé	poprvé	k6eAd1	poprvé
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
americký	americký	k2eAgMnSc1d1	americký
systémový	systémový	k2eAgMnSc1d1	systémový
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
letecký	letecký	k2eAgMnSc1d1	letecký
inovátor	inovátor	k1gMnSc1	inovátor
Kelly	Kella	k1gFnSc2	Kella
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
navrhovat	navrhovat	k5eAaImF	navrhovat
letadla	letadlo	k1gNnPc4	letadlo
tak	tak	k6eAd1	tak
jednoduchá	jednoduchý	k2eAgNnPc4d1	jednoduché
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
byl	být	k5eAaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
opravit	opravit	k5eAaPmF	opravit
každý	každý	k3xTgMnSc1	každý
mechanik	mechanik	k1gMnSc1	mechanik
se	s	k7c7	s
standardní	standardní	k2eAgFnSc7d1	standardní
sadou	sada	k1gFnSc7	sada
nářadí	nářadí	k1gNnPc2	nářadí
na	na	k7c6	na
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
letišti	letiště	k1gNnSc6	letiště
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
KISS	KISS	kA	KISS
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
už	už	k6eAd1	už
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Apollo	Apollo	k1gMnSc1	Apollo
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
;	;	kIx,	;
existuje	existovat	k5eAaImIp3nS	existovat
(	(	kIx(	(
<g/>
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
mimopočítačových	mimopočítačův	k2eAgInPc6d1	mimopočítačův
kontextech	kontext	k1gInPc6	kontext
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
<g/>
)	)	kIx)	)
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
alternativních	alternativní	k2eAgNnPc2d1	alternativní
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
vyhýbajících	vyhýbající	k2eAgNnPc2d1	vyhýbající
se	se	k3xPyFc4	se
urážce	urážka	k1gFnSc6	urážka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
it	it	k?	it
short	short	k1gInSc1	short
and	and	k?	and
simple	simple	k6eAd1	simple
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Zachovej	zachovat	k5eAaPmRp2nS	zachovat
to	ten	k3xDgNnSc4	ten
krátké	krátký	k2eAgNnSc4d1	krátké
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
it	it	k?	it
simple	simple	k6eAd1	simple
sir	sir	k1gMnSc1	sir
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Zachovej	zachovat	k5eAaPmRp2nS	zachovat
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
it	it	k?	it
simple	simple	k6eAd1	simple
or	or	k?	or
be	be	k?	be
stupid	stupid	k1gInSc1	stupid
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Zachovej	zachovat	k5eAaPmRp2nS	zachovat
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
nebo	nebo	k8xC	nebo
buď	buď	k8xC	buď
hloupý	hloupý	k2eAgInSc1d1	hloupý
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
it	it	k?	it
simple	simple	k6eAd1	simple
and	and	k?	and
<g />
.	.	kIx.	.
</s>
<s>
straightforward	straightforward	k1gInSc1	straightforward
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Zachovej	zachovat	k5eAaPmRp2nS	zachovat
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
a	a	k8xC	a
přímočaré	přímočarý	k2eAgNnSc1d1	přímočaré
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
it	it	k?	it
simple	simple	k6eAd1	simple
and	and	k?	and
sincere	sincrat	k5eAaPmIp3nS	sincrat
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Zachovej	zachovat	k5eAaPmRp2nS	zachovat
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
a	a	k8xC	a
upřímné	upřímný	k2eAgNnSc1d1	upřímné
<g/>
"	"	kIx"	"
Podoba	podoba	k1gFnSc1	podoba
zkratky	zkratka	k1gFnSc2	zkratka
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
slovní	slovní	k2eAgInSc1d1	slovní
hříčkou	hříčka	k1gFnSc7	hříčka
-	-	kIx~	-
slovo	slovo	k1gNnSc1	slovo
kiss	kissa	k1gFnPc2	kissa
totiž	totiž	k9	totiž
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
znamená	znamenat	k5eAaImIp3nS	znamenat
také	také	k9	také
polibek	polibek	k1gInSc4	polibek
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
problém	problém	k1gInSc1	problém
dnešních	dnešní	k2eAgMnPc2d1	dnešní
programátorů	programátor	k1gMnPc2	programátor
je	být	k5eAaImIp3nS	být
přílišná	přílišný	k2eAgFnSc1d1	přílišná
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zesložiťování	zesložiťování	k1gNnSc4	zesložiťování
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
<g/>
,	,	kIx,	,
když	když	k8xS	když
dostane	dostat	k5eAaPmIp3nS	dostat
vývojář	vývojář	k1gMnSc1	vývojář
problém	problém	k1gInSc1	problém
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
<g/>
,	,	kIx,	,
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
malé	malý	k2eAgInPc4d1	malý
kousky	kousek	k1gInPc4	kousek
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
problém	problém	k1gInSc1	problém
nakódovat	nakódovat	k5eAaBmF	nakódovat
<g/>
.	.	kIx.	.
8	[number]	k4	8
až	až	k9	až
9	[number]	k4	9
z	z	k7c2	z
10	[number]	k4	10
programátorů	programátor	k1gMnPc2	programátor
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
nerozloží	rozložit	k5eNaPmIp3nS	rozložit
problém	problém	k1gInSc1	problém
na	na	k7c4	na
dost	dost	k6eAd1	dost
malé	malý	k2eAgFnPc4d1	malá
nebo	nebo	k8xC	nebo
dostatečně	dostatečně	k6eAd1	dostatečně
pochopitelné	pochopitelný	k2eAgInPc4d1	pochopitelný
kousky	kousek	k1gInPc4	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
velmi	velmi	k6eAd1	velmi
komplexní	komplexní	k2eAgFnSc2d1	komplexní
implementace	implementace	k1gFnSc2	implementace
i	i	k8xC	i
těch	ten	k3xDgInPc2	ten
nejjednodušších	jednoduchý	k2eAgInPc2d3	nejjednodušší
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
špagetový	špagetový	k2eAgInSc4d1	špagetový
kód	kód	k1gInSc4	kód
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
OOP	OOP	kA	OOP
(	(	kIx(	(
<g/>
objektově	objektově	k6eAd1	objektově
orientovaném	orientovaný	k2eAgNnSc6d1	orientované
programování	programování	k1gNnSc6	programování
<g/>
)	)	kIx)	)
reprezentován	reprezentován	k2eAgInSc1d1	reprezentován
třídami	třída	k1gFnPc7	třída
s	s	k7c7	s
tisíci	tisíc	k4xCgInPc7	tisíc
a	a	k8xC	a
více	hodně	k6eAd2	hodně
řádky	řádek	k1gInPc7	řádek
a	a	k8xC	a
metodami	metoda	k1gFnPc7	metoda
se	s	k7c7	s
stovkami	stovka	k1gFnPc7	stovka
a	a	k8xC	a
více	hodně	k6eAd2	hodně
řádky	řádek	k1gInPc4	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
algoritmy	algoritmus	k1gInPc1	algoritmus
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
nejméně	málo	k6eAd3	málo
řádků	řádek	k1gInPc2	řádek
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
jim	on	k3xPp3gMnPc3	on
jednoduše	jednoduše	k6eAd1	jednoduše
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
řešení	řešení	k1gNnSc2	řešení
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
KISS	KISS	kA	KISS
zní	znět	k5eAaImIp3nS	znět
jako	jako	k8xC	jako
nasadit	nasadit	k5eAaPmF	nasadit
si	se	k3xPyFc3	se
zaslepující	zaslepující	k2eAgFnPc4d1	zaslepující
brýle	brýle	k1gFnPc4	brýle
<g/>
,	,	kIx,	,
vypnout	vypnout	k5eAaPmF	vypnout
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
cvičenou	cvičený	k2eAgFnSc7d1	cvičená
opicí	opice	k1gFnSc7	opice
<g/>
.	.	kIx.	.
</s>
<s>
Zní	znět	k5eAaImIp3nS	znět
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
odstranit	odstranit	k5eAaPmF	odstranit
vše	všechen	k3xTgNnSc4	všechen
zajímavé	zajímavý	k2eAgNnSc4d1	zajímavé
okolo	okolo	k7c2	okolo
hraní	hraní	k1gNnSc2	hraní
si	se	k3xPyFc3	se
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgFnPc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
nějaké	nějaký	k3yIgFnPc1	nějaký
další	další	k2eAgFnPc1d1	další
nudné	nudný	k2eAgFnPc1d1	nudná
komerční	komerční	k2eAgFnPc1d1	komerční
stránky	stránka	k1gFnPc1	stránka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Praxe	praxe	k1gFnSc1	praxe
ovšem	ovšem	k9	ovšem
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvorba	tvorba	k1gFnSc1	tvorba
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
řešení	řešení	k1gNnSc2	řešení
se	s	k7c7	s
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
potřeby	potřeba	k1gFnSc2	potřeba
seberealizace	seberealizace	k1gFnSc2	seberealizace
některých	některý	k3yIgMnPc2	některý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
vyplácí	vyplácet	k5eAaImIp3nS	vyplácet
snad	snad	k9	snad
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Implementovat	implementovat	k5eAaImF	implementovat
novou	nový	k2eAgFnSc4d1	nová
vlastnost	vlastnost	k1gFnSc4	vlastnost
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
možným	možný	k2eAgInSc7d1	možný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bude	být	k5eAaImBp3nS	být
ještě	ještě	k6eAd1	ještě
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Nebudovat	budovat	k5eNaImF	budovat
žádné	žádný	k3yNgFnPc4	žádný
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
a	a	k8xC	a
dokonalé	dokonalý	k2eAgFnPc4d1	dokonalá
struktury	struktura	k1gFnPc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Spustit	spustit	k5eAaPmF	spustit
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
kódu	kód	k1gInSc6	kód
jednotkové	jednotkový	k2eAgInPc4d1	jednotkový
testy	test	k1gInPc4	test
<g/>
.	.	kIx.	.
</s>
<s>
Refaktorovat	Refaktorovat	k5eAaBmF	Refaktorovat
celý	celý	k2eAgInSc4d1	celý
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nového	nový	k2eAgInSc2d1	nový
kódu	kód	k1gInSc2	kód
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
možný	možný	k2eAgInSc1d1	možný
kód	kód	k1gInSc1	kód
<g/>
.	.	kIx.	.
</s>
<s>
Dodržovat	dodržovat	k5eAaImF	dodržovat
pravidla	pravidlo	k1gNnPc4	pravidlo
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
např.	např.	kA	např.
žádné	žádný	k3yNgFnPc4	žádný
duplicity	duplicita	k1gFnPc4	duplicita
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
udělat	udělat	k5eAaPmF	udělat
systém	systém	k1gInSc4	systém
co	co	k9	co
nejčistější	čistý	k2eAgNnSc1d3	nejčistší
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
pokorný	pokorný	k2eAgMnSc1d1	pokorný
<g/>
,	,	kIx,	,
nemysli	myslet	k5eNaImRp2nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
nějaký	nějaký	k3yIgMnSc1	nějaký
génius	génius	k1gMnSc1	génius
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
chyba	chyba	k1gFnSc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Rozděl	rozděl	k1gInSc1	rozděl
svůj	svůj	k3xOyFgInSc4	svůj
problém	problém	k1gInSc4	problém
na	na	k7c4	na
spoustu	spousta	k1gFnSc4	spousta
menších	malý	k2eAgInPc2d2	menší
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
problém	problém	k1gInSc1	problém
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
jít	jít	k5eAaImF	jít
vyřešit	vyřešit	k5eAaPmF	vyřešit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
několika	několik	k4yIc7	několik
málo	málo	k6eAd1	málo
třídách	třída	k1gFnPc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Rozděl	rozděl	k1gInSc1	rozděl
své	svůj	k3xOyFgInPc4	svůj
úkoly	úkol	k1gInPc4	úkol
do	do	k7c2	do
menších	malý	k2eAgMnPc2d2	menší
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
si	se	k3xPyFc3	se
myslíš	myslet	k5eAaImIp2nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
nakódování	nakódování	k1gNnSc1	nakódování
nebude	být	k5eNaImBp3nS	být
trvat	trvat	k5eAaImF	trvat
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
než	než	k8xS	než
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Udržuj	udržovat	k5eAaImRp2nS	udržovat
své	svůj	k3xOyFgNnSc4	svůj
metody	metoda	k1gFnPc4	metoda
krátké	krátký	k2eAgFnPc4d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
maximálně	maximálně	k6eAd1	maximálně
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
řádků	řádek	k1gInPc2	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
metoda	metoda	k1gFnSc1	metoda
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
řešit	řešit	k5eAaImF	řešit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Udržuj	udržovat	k5eAaImRp2nS	udržovat
své	svůj	k3xOyFgNnSc4	svůj
třídy	třída	k1gFnSc2	třída
krátké	krátký	k2eAgNnSc4d1	krátké
<g/>
.	.	kIx.	.
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
si	se	k3xPyFc3	se
problém	problém	k1gInSc1	problém
rozmysli	rozmyslet	k5eAaPmRp2nS	rozmyslet
<g/>
,	,	kIx,	,
až	až	k8xS	až
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
nakóduj	nakódovat	k5eAaBmRp2nS	nakódovat
<g/>
.	.	kIx.	.
</s>
<s>
Refaktoruj	Refaktorovat	k5eAaBmRp2nS	Refaktorovat
svůj	svůj	k3xOyFgInSc4	svůj
kód	kód	k1gInSc4	kód
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Neboj	bát	k5eNaImRp2nS	bát
se	se	k3xPyFc4	se
nějaké	nějaký	k3yIgFnSc2	nějaký
části	část	k1gFnSc2	část
kódu	kód	k1gInSc2	kód
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Snaž	snažit	k5eAaImRp2nS	snažit
se	se	k3xPyFc4	se
udržet	udržet	k5eAaPmF	udržet
řešení	řešení	k1gNnSc4	řešení
co	co	k9	co
nejjednodušší	jednoduchý	k2eAgNnSc1d3	nejjednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Dokončíš	dokončit	k5eAaPmIp2nS	dokončit
práci	práce	k1gFnSc4	práce
rychleji	rychle	k6eAd2	rychle
Tvoje	tvůj	k3xOp2gFnSc1	tvůj
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
Testy	testa	k1gFnSc2	testa
se	se	k3xPyFc4	se
píšou	psát	k5eAaImIp3nP	psát
snadněji	snadno	k6eAd2	snadno
Kód	kód	k1gInSc4	kód
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
na	na	k7c4	na
správu	správa	k1gFnSc4	správa
Máš	mít	k5eAaImIp2nS	mít
menší	malý	k2eAgInSc1d2	menší
stres	stres	k1gInSc1	stres
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
všechny	všechen	k3xTgInPc4	všechen
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgInPc4d1	uvedený
efekty	efekt	k1gInPc4	efekt
Budeš	být	k5eAaImBp2nS	být
schopný	schopný	k2eAgInSc1d1	schopný
řešit	řešit	k5eAaImF	řešit
více	hodně	k6eAd2	hodně
problémů	problém	k1gInPc2	problém
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Budeš	být	k5eAaImBp2nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
produkovat	produkovat	k5eAaImF	produkovat
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bude	být	k5eAaImBp3nS	být
řešit	řešit	k5eAaImF	řešit
komplexní	komplexní	k2eAgInPc4d1	komplexní
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
řádcích	řádek	k1gInPc6	řádek
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Budeš	být	k5eAaImBp2nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
produkovat	produkovat	k5eAaImF	produkovat
kvalitnější	kvalitní	k2eAgInSc4d2	kvalitnější
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Budeš	být	k5eAaImBp2nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
vytvářet	vytvářet	k5eAaImF	vytvářet
větší	veliký	k2eAgInPc4d2	veliký
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
budou	být	k5eAaImBp3nP	být
lépe	dobře	k6eAd2	dobře
spravovány	spravovat	k5eAaImNgInP	spravovat
<g/>
.	.	kIx.	.
</s>
<s>
Tvoje	tvůj	k3xOp2gFnSc1	tvůj
kódová	kódový	k2eAgFnSc1d1	kódová
základna	základna	k1gFnSc1	základna
bude	být	k5eAaImBp3nS	být
více	hodně	k6eAd2	hodně
flexibilní	flexibilní	k2eAgMnSc1d1	flexibilní
<g/>
,	,	kIx,	,
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
,	,	kIx,	,
úpravu	úprava	k1gFnSc4	úprava
nebo	nebo	k8xC	nebo
refaktorování	refaktorování	k1gNnSc4	refaktorování
<g/>
,	,	kIx,	,
když	když	k8xS	když
dorazí	dorazit	k5eAaPmIp3nP	dorazit
nové	nový	k2eAgInPc4d1	nový
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Budeš	být	k5eAaImBp2nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
sis	sis	k?	sis
někdy	někdy	k6eAd1	někdy
dříve	dříve	k6eAd2	dříve
dokázal	dokázat	k5eAaPmAgMnS	dokázat
představit	představit	k5eAaPmF	představit
<g/>
.	.	kIx.	.
</s>
<s>
Budeš	být	k5eAaImBp2nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
vývojových	vývojový	k2eAgFnPc6d1	vývojová
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
všechen	všechen	k3xTgInSc4	všechen
kód	kód	k1gInSc4	kód
"	"	kIx"	"
<g/>
stupid	stupid	k1gInSc1	stupid
simple	simple	k6eAd1	simple
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ukázky	ukázka	k1gFnSc2	ukázka
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
)	)	kIx)	)
níže	nízce	k6eAd2	nízce
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
metoda	metoda	k1gFnSc1	metoda
secti	secť	k1gFnSc2	secť
sčítá	sčítat	k5eAaImIp3nS	sčítat
dvě	dva	k4xCgNnPc4	dva
čísla	číslo	k1gNnPc4	číslo
zadaná	zadaný	k2eAgFnSc1d1	zadaná
v	v	k7c6	v
parametrech	parametr	k1gInPc6	parametr
a	a	k8xC	a
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
"	"	kIx"	"
<g/>
zaloguje	zalogovat	k5eAaImIp3nS	zalogovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chyba	Chyba	k1gMnSc1	Chyba
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
metoda	metoda	k1gFnSc1	metoda
měla	mít	k5eAaImAgFnS	mít
vždy	vždy	k6eAd1	vždy
jmenovat	jmenovat	k5eAaImF	jmenovat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
přesně	přesně	k6eAd1	přesně
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ukázce	ukázka	k1gFnSc6	ukázka
jméno	jméno	k1gNnSc1	jméno
metody	metoda	k1gFnSc2	metoda
ovšem	ovšem	k9	ovšem
avizuje	avizovat	k5eAaBmIp3nS	avizovat
pouze	pouze	k6eAd1	pouze
polovinu	polovina	k1gFnSc4	polovina
funkčnosti	funkčnost	k1gFnSc2	funkčnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
chybou	chyba	k1gFnSc7	chyba
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ukázce	ukázka	k1gFnSc6	ukázka
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
skutečná	skutečný	k2eAgFnSc1d1	skutečná
funkcionalita	funkcionalita	k1gFnSc1	funkcionalita
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
KISS	KISS	kA	KISS
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
navrhování	navrhování	k1gNnSc4	navrhování
co	co	k9	co
nejjednodušších	jednoduchý	k2eAgFnPc2d3	nejjednodušší
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každá	každý	k3xTgFnSc1	každý
metoda	metoda	k1gFnSc1	metoda
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
dělat	dělat	k5eAaImF	dělat
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ukázce	ukázka	k1gFnSc6	ukázka
ovšem	ovšem	k9	ovšem
dělá	dělat	k5eAaImIp3nS	dělat
věci	věc	k1gFnSc6	věc
dvě	dva	k4xCgFnPc1	dva
-	-	kIx~	-
sčítá	sčítat	k5eAaImIp3nS	sčítat
a	a	k8xC	a
loguje	logovat	k5eAaImIp3nS	logovat
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnou	vhodný	k2eAgFnSc7d1	vhodná
úpravou	úprava	k1gFnSc7	úprava
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc4	vytvoření
samostatné	samostatný	k2eAgFnSc2d1	samostatná
metody	metoda	k1gFnSc2	metoda
pro	pro	k7c4	pro
logování	logování	k1gNnSc4	logování
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
návrh	návrh	k1gInSc1	návrh
zjednodušil	zjednodušit	k5eAaPmAgInS	zjednodušit
<g/>
,	,	kIx,	,
zpřehlednil	zpřehlednit	k5eAaPmAgInS	zpřehlednit
a	a	k8xC	a
především	především	k6eAd1	především
by	by	kYmCp3nP	by
novou	nový	k2eAgFnSc4d1	nová
metodu	metoda	k1gFnSc4	metoda
na	na	k7c6	na
logování	logování	k1gNnSc6	logování
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
znovu	znovu	k6eAd1	znovu
použít	použít	k5eAaPmF	použít
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
volat	volat	k5eAaImF	volat
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
metod	metoda	k1gFnPc2	metoda
(	(	kIx(	(
<g/>
reusability	reusabilita	k1gFnPc1	reusabilita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
už	už	k9	už
důvodům	důvod	k1gInPc3	důvod
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
zbytečných	zbytečný	k2eAgFnPc2d1	zbytečná
duplicit	duplicita	k1gFnPc2	duplicita
<g/>
.	.	kIx.	.
http://en.wikipedia.org/wiki/KISS_principle	[url]	k6eAd1	http://en.wikipedia.org/wiki/KISS_principle
http://c2.com/xp/DoTheSimplestThingThatCouldPossiblyWork.html	[url]	k5eAaPmAgInS	http://c2.com/xp/DoTheSimplestThingThatCouldPossiblyWork.html
http://people.apache.org/~fhanik/kiss.html	[url]	k1gInSc1	http://people.apache.org/~fhanik/kiss.html
Occamova	Occamův	k2eAgFnSc1d1	Occamova
břitva	břitva	k1gFnSc1	břitva
DRY	DRY	kA	DRY
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
repeat	repeat	k1gInSc1	repeat
yourself	yourself	k1gMnSc1	yourself
<g/>
)	)	kIx)	)
YAGNI	YAGNI	kA	YAGNI
(	(	kIx(	(
<g/>
You	You	k1gMnSc1	You
ain	ain	k?	ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
gonna	gonno	k1gNnSc2	gonno
need	needa	k1gFnPc2	needa
it	it	k?	it
<g/>
)	)	kIx)	)
POGE	POGE	kA	POGE
(	(	kIx(	(
<g/>
Principle	Principle	k1gMnSc1	Principle
of	of	k?	of
good	good	k1gMnSc1	good
enough	enough	k1gMnSc1	enough
<g/>
)	)	kIx)	)
</s>
