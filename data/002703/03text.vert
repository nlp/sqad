<s>
Praděd	praděd	k1gMnSc1	praděd
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Altvater	Altvater	k1gInSc1	Altvater
<g/>
;	;	kIx,	;
polsky	polsky	k6eAd1	polsky
Pradziad	Pradziad	k1gInSc1	Pradziad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
1491,3	[number]	k4	1491,3
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
tohoto	tento	k3xDgNnSc2	tento
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
i	i	k8xC	i
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pátou	pátý	k4xOgFnSc4	pátý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
také	také	k9	také
druhou	druhý	k4xOgFnSc7	druhý
nejprominentnější	prominentní	k2eAgFnSc1d3	nejprominentnější
a	a	k8xC	a
třetí	třetí	k4xOgFnSc4	třetí
nejizolovanější	izolovaný	k2eAgFnSc1d3	nejizolovanější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nejdrsnější	drsný	k2eAgNnSc1d3	nejdrsnější
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
nepřevyšuje	převyšovat	k5eNaImIp3nS	převyšovat
1	[number]	k4	1
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c6	na
slezské	slezský	k2eAgFnSc6d1	Slezská
části	část	k1gFnSc6	část
vrcholu	vrchol	k1gInSc2	vrchol
stojí	stát	k5eAaImIp3nS	stát
146,5	[number]	k4	146,5
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
měl	mít	k5eAaImAgInS	mít
162	[number]	k4	162
m	m	kA	m
<g/>
)	)	kIx)	)
vysoký	vysoký	k2eAgInSc1d1	vysoký
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
s	s	k7c7	s
rozhlednou	rozhledna	k1gFnSc7	rozhledna
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
horní	horní	k2eAgFnSc1d1	horní
plošina	plošina	k1gFnSc1	plošina
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
umělým	umělý	k2eAgInSc7d1	umělý
<g/>
)	)	kIx)	)
bodem	bod	k1gInSc7	bod
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1	vrchol
hory	hora	k1gFnSc2	hora
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
probíhá	probíhat	k5eAaImIp3nS	probíhat
historická	historický	k2eAgFnSc1d1	historická
zemská	zemský	k2eAgFnSc1d1	zemská
hranice	hranice	k1gFnSc1	hranice
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tudy	tudy	k6eAd1	tudy
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
hranice	hranice	k1gFnSc1	hranice
krajů	kraj	k1gInPc2	kraj
Moravskoslezského	moravskoslezský	k2eAgNnSc2d1	moravskoslezské
a	a	k8xC	a
Olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
<g/>
,	,	kIx,	,
a	a	k8xC	a
okresů	okres	k1gInPc2	okres
Bruntál	Bruntál	k1gInSc1	Bruntál
(	(	kIx(	(
<g/>
Malá	Malá	k1gFnSc1	Malá
Morávka	Morávek	k1gMnSc2	Morávek
<g/>
,	,	kIx,	,
Vrbno	Vrbno	k6eAd1	Vrbno
pod	pod	k7c7	pod
Pradědem	praděd	k1gMnSc7	praděd
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šumperk	Šumperk	k1gInSc1	Šumperk
(	(	kIx(	(
<g/>
Loučná	Loučný	k2eAgFnSc1d1	Loučná
nad	nad	k7c4	nad
Desnou	Desná	k1gFnSc4	Desná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
úbočí	úbočí	k1gNnSc6	úbočí
vrcholu	vrchol	k1gInSc2	vrchol
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
mrazový	mrazový	k2eAgInSc1d1	mrazový
srub	srub	k1gInSc1	srub
Tabulové	tabulový	k2eAgFnSc2d1	Tabulová
skály	skála	k1gFnSc2	skála
s	s	k7c7	s
unikátní	unikátní	k2eAgFnSc7d1	unikátní
květenou	květena	k1gFnSc7	květena
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Slované	Slovan	k1gMnPc1	Slovan
říkali	říkat	k5eAaImAgMnP	říkat
hoře	hoře	k6eAd1	hoře
Stará	starý	k2eAgFnSc1d1	stará
vatra	vatra	k1gFnSc1	vatra
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Keilichter	Keilichtra	k1gFnPc2	Keilichtra
Schneberg	Schneberg	k1gInSc1	Schneberg
neboli	neboli	k8xC	neboli
Keilichtská	Keilichtský	k2eAgFnSc1d1	Keilichtský
Sněžná	sněžný	k2eAgFnSc1d1	sněžná
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
Keilicht	Keilicht	k1gInSc1	Keilicht
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc4	jméno
jiné	jiný	k2eAgFnSc2d1	jiná
hory	hora	k1gFnSc2	hora
nad	nad	k7c7	nad
Červenohorským	červenohorský	k2eAgNnSc7d1	Červenohorské
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
byla	být	k5eAaImAgFnS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
Klínovcová	Klínovcový	k2eAgFnSc1d1	Klínovcový
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Niská	Niský	k2eAgFnSc1d1	Niský
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získala	získat	k5eAaPmAgFnS	získat
po	po	k7c6	po
Pradědovi	praděd	k1gMnSc6	praděd
<g/>
,	,	kIx,	,
legendárním	legendární	k2eAgMnSc6d1	legendární
ochránci	ochránce	k1gMnPc1	ochránce
Jeseníků	Jeseník	k1gInPc2	Jeseník
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
Altvater	Altvatra	k1gFnPc2	Altvatra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
Praděd	praděd	k1gMnSc1	praděd
<g/>
.	.	kIx.	.
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
oblast	oblast	k1gFnSc1	oblast
okolo	okolo	k7c2	okolo
Pradědu	praděd	k1gMnSc6	praděd
je	být	k5eAaImIp3nS	být
širokou	široký	k2eAgFnSc7d1	široká
veřejností	veřejnost	k1gFnSc7	veřejnost
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c2	za
"	"	kIx"	"
<g/>
Slezský	slezský	k2eAgInSc1d1	slezský
ledovec	ledovec	k1gInSc1	ledovec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
dobrým	dobrý	k2eAgFnPc3d1	dobrá
sněhovým	sněhový	k2eAgFnPc3d1	sněhová
podmínkám	podmínka	k1gFnPc3	podmínka
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
zimu	zima	k1gFnSc4	zima
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
celorepublikový	celorepublikový	k2eAgInSc4d1	celorepublikový
unikát	unikát	k1gInSc4	unikát
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Praděd	praděd	k1gMnSc1	praděd
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgMnSc1d1	přístupný
z	z	k7c2	z
několika	několik	k4yIc2	několik
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vede	vést	k5eAaImIp3nS	vést
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
silnice	silnice	k1gFnSc1	silnice
z	z	k7c2	z
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Studánky	studánka	k1gFnSc2	studánka
přes	přes	k7c4	přes
sedlo	sedlo	k1gNnSc4	sedlo
Hvězda	Hvězda	k1gMnSc1	Hvězda
a	a	k8xC	a
chatu	chata	k1gFnSc4	chata
Ovčárna	ovčárna	k1gFnSc1	ovčárna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
i	i	k9	i
zelená	zelený	k2eAgFnSc1d1	zelená
turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
9	[number]	k4	9
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Turisticky	turisticky	k6eAd1	turisticky
je	být	k5eAaImIp3nS	být
Praděd	praděd	k1gMnSc1	praděd
přístupný	přístupný	k2eAgMnSc1d1	přístupný
také	také	k9	také
po	po	k7c6	po
hlavní	hlavní	k2eAgFnSc6d1	hlavní
červeně	červeně	k6eAd1	červeně
značené	značený	k2eAgFnSc6d1	značená
hřebenovce	hřebenovka	k1gFnSc6	hřebenovka
od	od	k7c2	od
Červenohorského	červenohorský	k2eAgNnSc2d1	Červenohorské
sedla	sedlo	k1gNnSc2	sedlo
(	(	kIx(	(
<g/>
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprudší	prudký	k2eAgInSc1d3	nejprudší
výstup	výstup	k1gInSc1	výstup
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
modře	modro	k6eAd1	modro
značené	značený	k2eAgFnSc6d1	značená
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Koutů	kout	k1gInPc2	kout
nad	nad	k7c7	nad
Desnou	Desna	k1gFnSc7	Desna
kolem	kolem	k7c2	kolem
dolní	dolní	k2eAgFnSc2d1	dolní
nádrže	nádrž	k1gFnSc2	nádrž
přečerpávací	přečerpávací	k2eAgFnSc2d1	přečerpávací
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
stráně	stráň	k1gFnSc2	stráň
(	(	kIx(	(
<g/>
asi	asi	k9	asi
14	[number]	k4	14
km	km	kA	km
s	s	k7c7	s
převýšením	převýšení	k1gNnSc7	převýšení
800	[number]	k4	800
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
možnostmi	možnost	k1gFnPc7	možnost
jsou	být	k5eAaImIp3nP	být
cesta	cesta	k1gFnSc1	cesta
od	od	k7c2	od
severovýchodu	severovýchod	k1gInSc2	severovýchod
ze	z	k7c2	z
sedla	sedlo	k1gNnSc2	sedlo
Vidly	Vidla	k1gFnSc2	Vidla
nebo	nebo	k8xC	nebo
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
ze	z	k7c2	z
sedla	sedlo	k1gNnSc2	sedlo
Skřítek	skřítek	k1gMnSc1	skřítek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
původně	původně	k6eAd1	původně
stála	stát	k5eAaImAgFnS	stát
kamenná	kamenný	k2eAgFnSc1d1	kamenná
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
32,5	[number]	k4	32,5
m	m	kA	m
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1903	[number]	k4	1903
až	až	k9	až
1912	[number]	k4	1912
Moravskoslezským	moravskoslezský	k2eAgInSc7d1	moravskoslezský
sudetským	sudetský	k2eAgInSc7d1	sudetský
horským	horský	k2eAgInSc7d1	horský
a	a	k8xC	a
turistickým	turistický	k2eAgInSc7d1	turistický
spolkem	spolek	k1gInSc7	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
duchu	duch	k1gMnSc3	duch
starého	starý	k2eAgInSc2d1	starý
gotického	gotický	k2eAgInSc2d1	gotický
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Habsburgwarte	Habsburgwart	k1gInSc5	Habsburgwart
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Adolf-Hitler-Turm	Adolf-Hitler-Turm	k1gInSc4	Adolf-Hitler-Turm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
chátrala	chátrat	k5eAaImAgFnS	chátrat
a	a	k8xC	a
zřítila	zřítit	k5eAaPmAgFnS	zřítit
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Napodobenina	napodobenina	k1gFnSc1	napodobenina
této	tento	k3xDgFnSc2	tento
rozhledny	rozhledna	k1gFnSc2	rozhledna
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Altvaterturm	Altvaterturm	k1gInSc1	Altvaterturm
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Wetzstein	Wetzsteina	k1gFnPc2	Wetzsteina
u	u	k7c2	u
Lehestenu	Lehesten	k2eAgFnSc4d1	Lehesten
v	v	k7c6	v
Durynském	durynský	k2eAgInSc6d1	durynský
lese	les	k1gInSc6	les
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Thüringer	Thüringer	k1gMnSc1	Thüringer
Wald	Wald	k1gMnSc1	Wald
<g/>
)	)	kIx)	)
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
s	s	k7c7	s
vysílačem	vysílač	k1gInSc7	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
započato	započat	k2eAgNnSc1d1	započato
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
146	[number]	k4	146
m	m	kA	m
vysokého	vysoký	k2eAgInSc2d1	vysoký
vysílače	vysílač	k1gInSc2	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
silnice	silnice	k1gFnSc1	silnice
z	z	k7c2	z
Ovčárny	ovčárna	k1gFnSc2	ovčárna
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Pradědu	praděd	k1gMnSc6	praděd
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
vysílače	vysílač	k1gInSc2	vysílač
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
otevřením	otevření	k1gNnSc7	otevření
restaurace	restaurace	k1gFnSc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
80	[number]	k4	80
m	m	kA	m
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
vidět	vidět	k5eAaImF	vidět
Lysou	lysý	k2eAgFnSc4d1	Lysá
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
Sněžku	Sněžka	k1gFnSc4	Sněžka
a	a	k8xC	a
Radhošť	Radhošť	k1gFnSc4	Radhošť
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
při	pře	k1gFnSc3	pře
dobré	dobrý	k2eAgFnSc2d1	dobrá
viditelnosti	viditelnost	k1gFnSc2	viditelnost
i	i	k8xC	i
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
a	a	k8xC	a
Malou	malý	k2eAgFnSc4d1	malá
Fatru	Fatra	k1gFnSc4	Fatra
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
a	a	k8xC	a
Nízké	nízký	k2eAgFnPc1d1	nízká
Alpy	Alpy	k1gFnPc1	Alpy
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Praděd	praděd	k1gMnSc1	praděd
(	(	kIx(	(
<g/>
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Pradědu	praděd	k1gMnSc6	praděd
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
národní	národní	k2eAgFnPc4d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Euroregion	euroregion	k1gInSc1	euroregion
Praděd	praděd	k1gMnSc1	praděd
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Praděd	praděd	k1gMnSc1	praděd
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Praděd	praděd	k1gMnSc1	praděd
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Praděd	praděd	k1gMnSc1	praděd
na	na	k7c4	na
eJeseniky	eJesenik	k1gInPc4	eJesenik
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Praděd	praděd	k1gMnSc1	praděd
na	na	k7c4	na
Rozhlednyunas	Rozhlednyunas	k1gInSc4	Rozhlednyunas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ski	ski	k1gFnSc1	ski
areál	areál	k1gInSc1	areál
Praděd	praděd	k1gMnSc1	praděd
na	na	k7c4	na
Ceske-sjezdovky	Ceskejezdovka	k1gFnPc4	Ceske-sjezdovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Praděd	praděd	k1gMnSc1	praděd
na	na	k7c4	na
Tisicovky	Tisicovka	k1gFnPc4	Tisicovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
