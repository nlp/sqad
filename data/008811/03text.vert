<p>
<s>
Éric	Éric	k1gFnSc1	Éric
Tabarly	Tabarla	k1gFnSc2	Tabarla
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1931	[number]	k4	1931
Nantes	Nantes	k1gInSc1	Nantes
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
námořník	námořník	k1gMnSc1	námořník
a	a	k8xC	a
jachtař	jachtař	k1gMnSc1	jachtař
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgMnS	nazývat
otcem	otec	k1gMnSc7	otec
francouzského	francouzský	k2eAgInSc2d1	francouzský
jachtingu	jachting	k1gInSc2	jachting
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jachta	jachta	k1gFnSc1	jachta
nesla	nést	k5eAaImAgFnS	nést
vždy	vždy	k6eAd1	vždy
název	název	k1gInSc4	název
Pen	Pen	k1gFnSc2	Pen
Duick	Duicka	k1gFnPc2	Duicka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
jachtě	jachta	k1gFnSc6	jachta
zlomil	zlomit	k5eAaPmAgMnS	zlomit
Tabarly	Tabarla	k1gMnSc2	Tabarla
bezmotorový	bezmotorový	k2eAgInSc4d1	bezmotorový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
překonání	překonání	k1gNnSc6	překonání
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
držel	držet	k5eAaImAgInS	držet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
Charlie	Charlie	k1gMnSc1	Charlie
Barr	Barr	k1gMnSc1	Barr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rekord	rekord	k1gInSc1	rekord
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
tak	tak	k6eAd1	tak
veliký	veliký	k2eAgInSc4d1	veliký
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
rozmach	rozmach	k1gInSc4	rozmach
dálkového	dálkový	k2eAgNnSc2d1	dálkové
jachtaření	jachtaření	k1gNnSc2	jachtaření
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
i	i	k9	i
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
za	za	k7c4	za
rok	rok	k1gInSc4	rok
Tabarlyho	Tabarly	k1gMnSc2	Tabarly
rekord	rekord	k1gInSc1	rekord
překonán	překonán	k2eAgInSc1d1	překonán
Marcem	Marce	k1gMnSc7	Marce
Pajotem	Pajot	k1gMnSc7	Pajot
<g/>
.	.	kIx.	.
</s>
<s>
Tabarly	Tabarla	k1gFnPc4	Tabarla
zahynul	zahynout	k5eAaPmAgInS	zahynout
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
ráhno	ráhno	k1gNnSc1	ráhno
ho	on	k3xPp3gMnSc4	on
srazilo	srazit	k5eAaPmAgNnS	srazit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
nedaleko	nedaleko	k7c2	nedaleko
Walesu	Wales	k1gInSc2	Wales
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
mistrovství	mistrovství	k1gNnSc2	mistrovství
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
pět	pět	k4xCc1	pět
týdnů	týden	k1gInPc2	týden
poté	poté	k6eAd1	poté
u	u	k7c2	u
irského	irský	k2eAgNnSc2d1	irské
pobřeží	pobřeží	k1gNnSc2	pobřeží
francouzskými	francouzský	k2eAgNnPc7d1	francouzské
rybáři	rybář	k1gMnPc1	rybář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
De	De	k?	De
Pen	Pen	k1gMnSc1	Pen
Duick	Duick	k1gMnSc1	Duick
en	en	k?	en
Pen	Pen	k1gMnSc1	Pen
Duick	Duick	k1gMnSc1	Duick
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Le	Le	k?	Le
tour	tour	k1gInSc1	tour
du	du	k?	du
monde	mond	k1gInSc5	mond
de	de	k?	de
Pen	Pen	k1gFnSc3	Pen
Duick	Duick	k1gMnSc1	Duick
VI	VI	kA	VI
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mes	Mes	k?	Mes
bateaux	bateaux	k1gInSc1	bateaux
et	et	k?	et
moi	moi	k?	moi
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Journal	Journat	k5eAaPmAgMnS	Journat
de	de	k?	de
bord	bord	k1gMnSc1	bord
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pen	Pen	k?	Pen
Duick	Duick	k1gInSc1	Duick
VI	VI	kA	VI
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Guide	Guide	k6eAd1	Guide
pratique	pratiquat	k5eAaPmIp3nS	pratiquat
de	de	k?	de
la	la	k1gNnSc4	la
manœ	manœ	k?	manœ
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Atlantique	Atlantique	k1gInSc1	Atlantique
en	en	k?	en
10	[number]	k4	10
jours	jours	k1gInSc1	jours
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Embarque	Embarque	k6eAd1	Embarque
avec	avec	k1gFnSc1	avec
Tabarly	Tabarla	k1gFnSc2	Tabarla
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Apprenez	Apprenez	k1gInSc1	Apprenez
la	la	k1gNnSc2	la
voile	voile	k1gFnSc1	voile
avec	avec	k1gFnSc1	avec
Tabarly	Tabarla	k1gFnSc2	Tabarla
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pen	Pen	k?	Pen
Duick	Duick	k1gInSc1	Duick
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mémoires	Mémoires	k1gInSc1	Mémoires
du	du	k?	du
large	large	k1gInSc1	large
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Du	Du	k?	Du
tour	tour	k1gInSc1	tour
du	du	k?	du
monde	mond	k1gInSc5	mond
à	à	k?	à
la	la	k1gNnSc7	la
Transat	Transat	k1gFnSc2	Transat
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Victoire	Victoir	k1gMnSc5	Victoir
en	en	k?	en
solitaire	solitair	k1gInSc5	solitair
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Yann	Yann	k1gMnSc1	Yann
Queffélec	Queffélec	k1gMnSc1	Queffélec
<g/>
:	:	kIx,	:
Tabarly	Tabarla	k1gFnPc1	Tabarla
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Archipel	archipel	k1gInSc1	archipel
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-2-8098-0034-0	[number]	k4	978-2-8098-0034-0
</s>
</p>
<p>
<s>
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Tabarly	Tabarl	k1gInPc7	Tabarl
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Gilles	Gilles	k1gMnSc1	Gilles
<g/>
:	:	kIx,	:
À	À	k?	À
Éric	Éric	k1gInSc1	Éric
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Éditions	Éditions	k1gInSc1	Éditions
du	du	k?	du
Chê	Chê	k1gFnSc2	Chê
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-2-84277-849-1	[number]	k4	978-2-84277-849-1
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Éric	Éric	k1gFnSc1	Éric
Tabarly	Tabarl	k1gInPc4	Tabarl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Asociace	asociace	k1gFnSc2	asociace
Érica	Érica	k1gFnSc1	Érica
Tabarlyho	Tabarly	k1gMnSc2	Tabarly
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
Cité	Cité	k1gNnSc4	Cité
de	de	k?	de
la	la	k1gNnSc2	la
voile	voile	k6eAd1	voile
</s>
</p>
