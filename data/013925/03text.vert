<s>
Otto	Otto	k1gMnSc1
Forchheimer	Forchheimer	k1gMnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
Forchheimer	Forchheimer	k1gMnSc1
</s>
<s>
poslanec	poslanec	k1gMnSc1
Českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1878	#num#	k4
–	–	kI~
1889	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Ústavní	ústavní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1840	#num#	k4
PlzeňRakouské	PlzeňRakouský	k2eAgFnPc4d1
císařství	císařství	k1gNnSc2
Rakouské	rakouský	k2eAgFnSc6d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1913	#num#	k4
PrahaRakousko-Uhersko	PrahaRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Otto	Otto	k1gMnSc1
Forchheimer	Forchheimer	k1gMnSc1
(	(	kIx(
<g/>
1840	#num#	k4
Plzeň	Plzeň	k1gFnSc1
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1913	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
a	a	k8xC
český	český	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
německé	německý	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
poslanec	poslanec	k1gMnSc1
Českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
pražského	pražský	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
spolku	spolek	k1gInSc2
Deutsches	Deutschesa	k1gFnPc2
Kasino	kasino	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Byl	být	k5eAaImAgMnS
ředitelem	ředitel	k1gMnSc7
firmy	firma	k1gFnSc2
Brüder	Brüder	k1gMnSc1
Forchheimer	Forchheimer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedal	předsedat	k5eAaImAgMnS
pražské	pražský	k2eAgFnSc3d1
obchodní	obchodní	k2eAgFnSc3d1
komoře	komora	k1gFnSc3
a	a	k8xC
byl	být	k5eAaImAgMnS
viceprezidentem	viceprezident	k1gMnSc7
Rakouského	rakouský	k2eAgInSc2d1
ústředního	ústřední	k2eAgInSc2d1
spolku	spolek	k1gInSc2
obchodníků	obchodník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
také	také	k9
jaké	jaký	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
direkční	direkční	k2eAgMnSc1d1
šéf	šéf	k1gMnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
spořitelně	spořitelna	k1gFnSc6
a	a	k8xC
správní	správní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
eskomptní	eskomptní	k2eAgFnSc6d1
bance	banka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1864	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
hlavního	hlavní	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
spolku	spolek	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Deutsches	Deutschesa	k1gFnPc2
Kasino	kasino	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1881	#num#	k4
působil	působit	k5eAaImAgMnS
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
předsednictvu	předsednictvo	k1gNnSc6
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1883	#num#	k4
jako	jako	k8xS,k8xC
zástupce	zástupka	k1gFnSc3
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
předsedy	předseda	k1gMnSc2
a	a	k8xC
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1894	#num#	k4
jako	jako	k8xS,k8xC
předseda	předseda	k1gMnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
setrval	setrvat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
zemské	zemský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1878	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
na	na	k7c4
Český	český	k2eAgInSc4d1
zemský	zemský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
za	za	k7c4
kurii	kurie	k1gFnSc4
obchodních	obchodní	k2eAgFnPc2d1
a	a	k8xC
živnostenských	živnostenský	k2eAgFnPc2d1
komor	komora	k1gFnPc2
(	(	kIx(
<g/>
obvod	obvod	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
jako	jako	k9
člen	člen	k1gMnSc1
takzvané	takzvaný	k2eAgFnSc2d1
Ústavní	ústavní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
liberálně	liberálně	k6eAd1
a	a	k8xC
centralisticky	centralisticky	k6eAd1
orientovaná	orientovaný	k2eAgFnSc1d1
stranická	stranický	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
odmítající	odmítající	k2eAgFnSc2d1
federalistické	federalistický	k2eAgFnSc2d1
aspirace	aspirace	k1gFnSc2
neněmeckých	německý	k2eNgNnPc2d1
etnik	etnikum	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Mandát	mandát	k1gInSc1
obhájil	obhájit	k5eAaPmAgInS
i	i	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
za	za	k7c4
týž	týž	k3xTgInSc4
obvod	obvod	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgMnS
mu	on	k3xPp3gMnSc3
udělen	udělit	k5eAaPmNgInS
Řád	řád	k1gInSc1
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
a	a	k8xC
rytířský	rytířský	k2eAgInSc1d1
Řád	řád	k1gInSc1
železné	železný	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
náhle	náhle	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
bylo	být	k5eAaImAgNnS
převezeno	převézt	k5eAaPmNgNnS
ke	k	k7c3
kremaci	kremace	k1gFnSc3
do	do	k7c2
Drážďan	Drážďany	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Otto	Otto	k1gMnSc1
Forchheimer	Forchheimer	k1gMnSc1
(	(	kIx(
<g/>
1840	#num#	k4
-	-	kIx~
1913	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
geni	geni	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Otto	Otto	k1gMnSc1
Forchheimer	Forchheimer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosinec	prosinec	k1gInSc1
1913	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
86	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
337	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Otto	Otto	k1gMnSc1
Forchheimer	Forchheimer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosinec	prosinec	k1gInSc1
1913	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
86	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
338	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.psp.cz/eknih/1878skc/1/stenprot/002schuz/s002002.htm	http://www.psp.cz/eknih/1878skc/1/stenprot/002schuz/s002002.htm	k6eAd1
<g/>
↑	↑	k?
Národní	národní	k2eAgInPc4d1
listy	list	k1gInPc4
22	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1878	#num#	k4
<g/>
,	,	kIx,
http://kramerius.nkp.cz/kramerius/PShowPageDoc.do?id=5011289&	http://kramerius.nkp.cz/kramerius/PShowPageDoc.do?id=5011289&	k5eAaPmIp1nS
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1883skc/1/stenprot/002schuz/s002003.htm	http://www.psp.cz/eknih/1883skc/1/stenprot/002schuz/s002003.htm	k6eAd1
</s>
