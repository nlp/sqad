<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
barva	barva	k1gFnSc1	barva
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
světla	světlo	k1gNnSc2	světlo
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
470	[number]	k4	470
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
při	při	k7c6	při
aditivním	aditivní	k2eAgNnSc6d1	aditivní
míchání	míchání	k1gNnSc6	míchání
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
RGB	RGB	kA	RGB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
doplňkem	doplněk	k1gInSc7	doplněk
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
tzv.	tzv.	kA	tzv.
studené	studený	k2eAgInPc1d1	studený
bavy	bavy	k1gInPc1	bavy
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kultury	kultura	k1gFnPc1	kultura
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
mezi	mezi	k7c7	mezi
modrou	modrý	k2eAgFnSc7d1	modrá
a	a	k8xC	a
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
má	mít	k5eAaImIp3nS	mít
uklidňující	uklidňující	k2eAgInSc4d1	uklidňující
efekt	efekt	k1gInSc4	efekt
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
též	též	k9	též
evokuje	evokovat	k5eAaBmIp3nS	evokovat
pocit	pocit	k1gInSc4	pocit
autority	autorita	k1gFnSc2	autorita
<g/>
,	,	kIx,	,
bezpečí	bezpečí	k1gNnSc2	bezpečí
nebo	nebo	k8xC	nebo
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
<g/>
:	:	kIx,	:
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
,	,	kIx,	,
důvěryhodnost	důvěryhodnost	k1gFnSc4	důvěryhodnost
důstojnost	důstojnost	k1gFnSc4	důstojnost
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
,	,	kIx,	,
loajalita	loajalita	k1gFnSc1	loajalita
etablovanost	etablovanost	k1gFnSc1	etablovanost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zavedená	zavedený	k2eAgFnSc1d1	zavedená
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgFnSc1d1	fungující
značka	značka	k1gFnSc1	značka
<g/>
)	)	kIx)	)
moc	moc	k1gFnSc1	moc
umírněnost	umírněnost	k1gFnSc1	umírněnost
<g/>
,	,	kIx,	,
klid	klid	k1gInSc1	klid
chlad	chlad	k1gInSc1	chlad
Denní	denní	k2eAgFnSc1d1	denní
obloha	obloha	k1gFnSc1	obloha
má	mít	k5eAaImIp3nS	mít
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
Rayleighovým	Rayleighův	k2eAgInSc7d1	Rayleighův
rozptylem	rozptyl	k1gInSc7	rozptyl
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
molekulách	molekula	k1gFnPc6	molekula
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
tlustá	tlustý	k2eAgFnSc1d1	tlustá
vrstva	vrstva	k1gFnSc1	vrstva
vody	voda	k1gFnSc2	voda
má	mít	k5eAaImIp3nS	mít
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
voda	voda	k1gFnSc1	voda
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
červené	červený	k2eAgNnSc1d1	červené
světlo	světlo	k1gNnSc1	světlo
(	(	kIx(	(
<g/>
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
kolem	kolem	k7c2	kolem
750	[number]	k4	750
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
záření	záření	k1gNnSc1	záření
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gMnPc3	jejich
nositelům	nositel	k1gMnPc3	nositel
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
např.	např.	kA	např.
"	"	kIx"	"
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
armáda	armáda	k1gFnSc1	armáda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
železničáře	železničář	k1gMnSc4	železničář
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
</s>
<s>
Modré	modrý	k2eAgInPc1d1	modrý
límečky	límeček	k1gInPc1	límeček
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
označení	označení	k1gNnSc1	označení
dělníků	dělník	k1gMnPc2	dělník
-	-	kIx~	-
proti	proti	k7c3	proti
bílým	bílý	k2eAgInPc3d1	bílý
límečkům	límeček	k1gInPc3	límeček
úředníků	úředník	k1gMnPc2	úředník
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
obecně	obecně	k6eAd1	obecně
označuje	označovat	k5eAaImIp3nS	označovat
muže	muž	k1gMnPc4	muž
či	či	k8xC	či
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
růžové	růžový	k2eAgFnSc3d1	růžová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
dívky	dívka	k1gFnPc1	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
krví	krev	k1gFnSc7	krev
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
člověka	člověk	k1gMnSc4	člověk
z	z	k7c2	z
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
skrz	skrz	k7c4	skrz
neopálenou	opálený	k2eNgFnSc4d1	neopálená
kůži	kůže	k1gFnSc4	kůže
jsou	být	k5eAaImIp3nP	být
lépe	dobře	k6eAd2	dobře
vidět	vidět	k5eAaImF	vidět
modře	modro	k6eAd1	modro
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
žíly	žíla	k1gFnPc4	žíla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
smrt	smrt	k1gFnSc1	smrt
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
havárie	havárie	k1gFnSc1	havárie
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
chybové	chybový	k2eAgNnSc1d1	chybové
hlášení	hlášení	k1gNnSc1	hlášení
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
modře	modro	k6eAd1	modro
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
obrazovku	obrazovka	k1gFnSc4	obrazovka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
BSOD	BSOD	kA	BSOD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
stuha	stuha	k1gFnSc1	stuha
je	být	k5eAaImIp3nS	být
ocenění	ocenění	k1gNnSc4	ocenění
udělované	udělovaný	k2eAgFnSc2d1	udělovaná
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
nejrychlejšího	rychlý	k2eAgInSc2d3	nejrychlejší
času	čas	k1gInSc2	čas
při	při	k7c6	při
přeplutí	přeplutí	k1gNnSc6	přeplutí
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lékařských	lékařský	k2eAgInPc6d1	lékařský
diagramech	diagram	k1gInPc6	diagram
se	se	k3xPyFc4	se
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
označuje	označovat	k5eAaImIp3nS	označovat
neokysličená	okysličený	k2eNgFnSc1d1	neokysličená
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
žíly	žíla	k1gFnSc2	žíla
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ji	on	k3xPp3gFnSc4	on
vedou	vést	k5eAaImIp3nP	vést
<g/>
.	.	kIx.	.
</s>
<s>
Modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
symbol	symbol	k1gInSc4	symbol
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
jejím	její	k3xOp3gInSc7	její
logem	log	k1gInSc7	log
je	být	k5eAaImIp3nS	být
modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
bývají	bývat	k5eAaImIp3nP	bývat
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
buržoazie	buržoazie	k1gFnSc2	buržoazie
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
pravice	pravice	k1gFnSc2	pravice
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
modrokabátníci	modrokabátník	k1gMnPc1	modrokabátník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
barvu	barva	k1gFnSc4	barva
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
označováni	označován	k2eAgMnPc1d1	označován
vojáci	voják	k1gMnPc1	voják
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Světle	světle	k6eAd1	světle
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
azzurro	azzurro	k1gNnSc1	azzurro
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc1d1	národní
barva	barva	k1gFnSc1	barva
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
např.	např.	kA	např.
na	na	k7c6	na
reprezentačních	reprezentační	k2eAgInPc6d1	reprezentační
dresech	dres	k1gInPc6	dres
jejích	její	k3xOp3gMnPc2	její
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
dáma	dáma	k1gFnSc1	dáma
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
slangové	slangový	k2eAgNnSc1d1	slangové
označení	označení	k1gNnSc1	označení
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Malá	malý	k2eAgFnSc1d1	malá
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
slangové	slangový	k2eAgNnSc1d1	slangové
označení	označení	k1gNnSc1	označení
firmy	firma	k1gFnSc2	firma
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Stopařově	stopařův	k2eAgFnSc6d1	Stopařova
průvodci	průvodce	k1gMnPc1	průvodce
po	po	k7c6	po
Galaxii	galaxie	k1gFnSc6	galaxie
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
postava	postava	k1gFnSc1	postava
"	"	kIx"	"
<g/>
Húlúvú	Húlúvú	k1gFnSc1	Húlúvú
<g/>
"	"	kIx"	"
popisovaná	popisovaný	k2eAgFnSc1d1	popisovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
superinteligentní	superinteligentní	k2eAgInSc1d1	superinteligentní
odstín	odstín	k1gInSc1	odstín
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
6	[number]	k4	6
nebo	nebo	k8xC	nebo
toleranci	tolerance	k1gFnSc4	tolerance
±	±	k?	±
0,25	[number]	k4	0,25
%	%	kIx~	%
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
považují	považovat	k5eAaImIp3nP	považovat
původ	původ	k1gInSc4	původ
názvu	název	k1gInSc2	název
hudebního	hudební	k2eAgInSc2d1	hudební
žánru	žánr	k1gInSc2	žánr
blues	blues	k1gNnSc2	blues
za	za	k7c4	za
odvozen	odvozen	k2eAgInSc4d1	odvozen
od	od	k7c2	od
pojmenování	pojmenování	k1gNnPc2	pojmenování
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
(	(	kIx(	(
<g/>
blue	blue	k1gInSc1	blue
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
idiom	idiom	k1gInSc1	idiom
"	"	kIx"	"
<g/>
feeling	feeling	k1gInSc1	feeling
blue	blu	k1gInSc2	blu
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
cítit	cítit	k5eAaImF	cítit
se	se	k3xPyFc4	se
modře	modro	k6eAd1	modro
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
smutný	smutný	k2eAgMnSc1d1	smutný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
depresi	deprese	k1gFnSc4	deprese
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
spouštěl	spouštět	k5eAaImAgInS	spouštět
Zeus	Zeus	k1gInSc4	Zeus
déšť	déšť	k1gInSc4	déšť
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
smutný	smutný	k2eAgMnSc1d1	smutný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
vodovodní	vodovodní	k2eAgInPc1d1	vodovodní
kohoutky	kohoutek	k1gInPc1	kohoutek
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
teče	téct	k5eAaImIp3nS	téct
studená	studený	k2eAgFnSc1d1	studená
voda	voda	k1gFnSc1	voda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
modrá	modrat	k5eAaImIp3nS	modrat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
modrý	modrý	k2eAgInSc1d1	modrý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
