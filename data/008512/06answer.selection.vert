<s>
Islandština	islandština	k1gFnSc1	islandština
(	(	kIx(	(
<g/>
íslenska	íslensko	k1gNnSc2	íslensko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
mluvčích	mluvčí	k1gMnPc2	mluvčí
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
