<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
(	(	kIx(	(
<g/>
íslenska	íslensko	k1gNnSc2	íslensko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
mluvčích	mluvčí	k1gMnPc2	mluvčí
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
Island	Island	k1gInSc4	Island
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
minimálně	minimálně	k6eAd1	minimálně
a	a	k8xC	a
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
jí	on	k3xPp3gFnSc2	on
nekonkurují	konkurovat	k5eNaImIp3nP	konkurovat
žádné	žádný	k3yNgInPc4	žádný
jiné	jiný	k2eAgInPc4d1	jiný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
zde	zde	k6eAd1	zde
neexistují	existovat	k5eNaImIp3nP	existovat
nářečí	nářečí	k1gNnSc4	nářečí
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
drobné	drobný	k2eAgFnPc4d1	drobná
odchylky	odchylka	k1gFnPc4	odchylka
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
země	zem	k1gFnSc2	zem
podobné	podobný	k2eAgFnSc2d1	podobná
rozlohy	rozloha	k1gFnSc2	rozloha
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
nízké	nízký	k2eAgFnPc1d1	nízká
hustoty	hustota	k1gFnPc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
velmi	velmi	k6eAd1	velmi
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Islanďané	Islanďan	k1gMnPc1	Islanďan
se	se	k3xPyFc4	se
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
učí	učit	k5eAaImIp3nP	učit
dva	dva	k4xCgInPc4	dva
cizí	cizí	k2eAgInPc4d1	cizí
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
angličtinu	angličtina	k1gFnSc4	angličtina
a	a	k8xC	a
dánštinu	dánština	k1gFnSc4	dánština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
výslovnost	výslovnost	k1gFnSc4	výslovnost
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
osídlení	osídlení	k1gNnSc2	osídlení
Islandu	Island	k1gInSc6	Island
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
výrazně	výrazně	k6eAd1	výrazně
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
číst	číst	k5eAaImF	číst
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
staré	starý	k2eAgFnSc2d1	stará
literární	literární	k2eAgFnSc2d1	literární
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
staroseverštiny	staroseverština	k1gFnSc2	staroseverština
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
jazyk	jazyk	k1gInSc1	jazyk
západního	západní	k2eAgNnSc2d1	západní
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
seveřany	seveřan	k1gMnPc7	seveřan
osídlených	osídlený	k2eAgInPc2d1	osídlený
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
gramatikou	gramatika	k1gFnSc7	gramatika
a	a	k8xC	a
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
je	být	k5eAaImIp3nS	být
blízká	blízký	k2eAgFnSc1d1	blízká
staré	starý	k2eAgFnSc3d1	stará
angličtině	angličtina	k1gFnSc3	angličtina
z	z	k7c2	z
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
1000	[number]	k4	1000
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
a	a	k8xC	a
výslovnost	výslovnost	k1gFnSc1	výslovnost
==	==	k?	==
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
latinka	latinka	k1gFnSc1	latinka
obohacená	obohacený	k2eAgFnSc1d1	obohacená
o	o	k7c4	o
několik	několik	k4yIc4	několik
speciálních	speciální	k2eAgInPc2d1	speciální
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Ustálila	ustálit	k5eAaPmAgFnS	ustálit
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
dánského	dánský	k2eAgMnSc2d1	dánský
lingvisty	lingvista	k1gMnSc2	lingvista
Rasmuse	Rasmuse	k1gFnSc1	Rasmuse
Raska	Raska	k1gFnSc1	Raska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Æ	Æ	k?	Æ
<g/>
:	:	kIx,	:
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
[	[	kIx(	[
<g/>
aj	aj	kA	aj
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
:	:	kIx,	:
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
–	–	k?	–
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
E	E	kA	E
<g/>
6	[number]	k4	6
–	–	k?	–
malé	malý	k2eAgFnSc6d1	malá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ð	Ð	k?	Ð
<g/>
:	:	kIx,	:
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
znělé	znělý	k2eAgInPc1d1	znělý
Þ	Þ	k?	Þ
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
anglické	anglický	k2eAgFnSc2d1	anglická
th	th	k?	th
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
this	this	k1gInSc4	this
<g/>
/	/	kIx~	/
<g/>
the	the	k?	the
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
:	:	kIx,	:
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
D	D	kA	D
<g/>
0	[number]	k4	0
–	–	k?	–
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
F	F	kA	F
<g/>
0	[number]	k4	0
–	–	k?	–
malé	malý	k2eAgInPc1d1	malý
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
chorvatským	chorvatský	k2eAgNnSc7d1	Chorvatské
Đ	Đ	kA	Đ
<g/>
/	/	kIx~	/
<g/>
đ	đ	k?	đ
–	–	k?	–
velké	velká	k1gFnSc2	velká
se	se	k3xPyFc4	se
opticky	opticky	k6eAd1	opticky
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malé	malý	k2eAgInPc1d1	malý
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgInPc1d1	odlišný
kódy	kód	k1gInPc1	kód
<g/>
:	:	kIx,	:
velké	velká	k1gFnPc1	velká
–	–	k?	–
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
110	[number]	k4	110
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
–	–	k?	–
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
111	[number]	k4	111
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Þ	Þ	k?	Þ
<g/>
:	:	kIx,	:
písmeno	písmeno	k1gNnSc1	písmeno
–	–	k?	–
"	"	kIx"	"
<g/>
thorn	thorn	k1gMnSc1	thorn
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
po	po	k7c6	po
runovém	runový	k2eAgNnSc6d1	runové
písmu	písmo	k1gNnSc6	písmo
<g/>
,	,	kIx,	,
používaném	používaný	k2eAgInSc6d1	používaný
před	před	k7c7	před
christianizací	christianizace	k1gFnPc2	christianizace
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
u	u	k7c2	u
neznělého	znělý	k2eNgInSc2d1	neznělý
anglického	anglický	k2eAgInSc2d1	anglický
th	th	k?	th
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
jako	jako	k8xS	jako
think	think	k1gInSc1	think
<g/>
/	/	kIx~	/
<g/>
thief	thief	k1gInSc1	thief
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
:	:	kIx,	:
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
DE	DE	k?	DE
–	–	k?	–
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
FE	FE	kA	FE
–	–	k?	–
malé	malý	k2eAgFnSc2d1	malá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fonologie	fonologie	k1gFnSc2	fonologie
==	==	k?	==
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
nářečních	nářeční	k2eAgInPc2d1	nářeční
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
má	mít	k5eAaImIp3nS	mít
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
hlásky	hlásek	k1gInPc1	hlásek
i	i	k8xC	i
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
a	a	k8xC	a
souhlásky	souhláska	k1gFnPc1	souhláska
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
znělé	znělý	k2eAgFnPc1d1	znělá
a	a	k8xC	a
neznělé	znělý	k2eNgFnPc1d1	neznělá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znělost	znělost	k1gFnSc1	znělost
hraje	hrát	k5eAaImIp3nS	hrát
základní	základní	k2eAgFnSc4d1	základní
roli	role	k1gFnSc4	role
v	v	k7c6	v
odlišení	odlišení	k1gNnSc6	odlišení
většiny	většina	k1gFnSc2	většina
souhlásek	souhláska	k1gFnPc2	souhláska
včetně	včetně	k7c2	včetně
souhlásek	souhláska	k1gFnPc2	souhláska
nosových	nosový	k2eAgFnPc2d1	nosová
ale	ale	k8xC	ale
vyjma	vyjma	k7c2	vyjma
plozivních	plozivní	k2eAgFnPc2d1	plozivní
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Plozivní	Plozivní	k2eAgFnPc1d1	Plozivní
souhlásky	souhláska	k1gFnPc1	souhláska
b	b	k?	b
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
a	a	k8xC	a
g	g	kA	g
jsou	být	k5eAaImIp3nP	být
neznělé	znělý	k2eNgFnPc1d1	neznělá
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
p	p	k?	p
<g/>
,	,	kIx,	,
t	t	k?	t
a	a	k8xC	a
k	k	k7c3	k
pouze	pouze	k6eAd1	pouze
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
aspirované	aspirovaný	k2eAgFnPc1d1	aspirovaná
<g/>
.	.	kIx.	.
</s>
<s>
Preaspirace	Preaspirace	k1gFnSc1	Preaspirace
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
při	při	k7c6	při
zdvojení	zdvojení	k1gNnSc6	zdvojení
p	p	k?	p
<g/>
,	,	kIx,	,
t	t	k?	t
a	a	k8xC	a
k.	k.	k?	k.
Preaspirace	Preaspirace	k1gFnSc1	Preaspirace
se	se	k3xPyFc4	se
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
při	při	k7c6	při
zdvojení	zdvojení	k1gNnSc6	zdvojení
b	b	k?	b
<g/>
,	,	kIx,	,
d	d	k?	d
a	a	k8xC	a
g.	g.	k?	g.
Preaspirované	Preaspirovaný	k2eAgNnSc1d1	Preaspirovaný
tt	tt	k?	tt
je	být	k5eAaImIp3nS	být
etymologicky	etymologicky	k6eAd1	etymologicky
a	a	k8xC	a
foneticky	foneticky	k6eAd1	foneticky
analogické	analogický	k2eAgNnSc1d1	analogické
německému	německý	k2eAgNnSc3d1	německé
a	a	k8xC	a
nizozemskému	nizozemský	k2eAgNnSc3d1	Nizozemské
cht	cht	k?	cht
(	(	kIx(	(
<g/>
srovnejte	srovnat	k5eAaPmRp2nP	srovnat
islandská	islandský	k2eAgNnPc1d1	islandské
slova	slovo	k1gNnPc1	slovo
nótt	nótta	k1gFnPc2	nótta
<g/>
,	,	kIx,	,
dóttir	dóttira	k1gFnPc2	dóttira
s	s	k7c7	s
německými	německý	k2eAgNnPc7d1	německé
slovy	slovo	k1gNnPc7	slovo
Nacht	Nachta	k1gFnPc2	Nachta
<g/>
,	,	kIx,	,
Tochter	Tochtra	k1gFnPc2	Tochtra
a	a	k8xC	a
nizozemskými	nizozemský	k2eAgNnPc7d1	Nizozemské
slovy	slovo	k1gNnPc7	slovo
nacht	nacht	k1gInSc1	nacht
<g/>
,	,	kIx,	,
dochter	dochter	k1gInSc1	dochter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Islandské	islandský	k2eAgFnSc2d1	islandská
souhlásky	souhláska	k1gFnSc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Islandské	islandský	k2eAgFnSc2d1	islandská
samohlásky	samohláska	k1gFnSc2	samohláska
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
složitý	složitý	k2eAgInSc1d1	složitý
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
z	z	k7c2	z
germánských	germánský	k2eAgMnPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
nejsložitější	složitý	k2eAgFnSc7d3	nejsložitější
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc7d1	podobná
faerštinou	faerština	k1gFnSc7	faerština
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
,	,	kIx,	,
množstvím	množství	k1gNnSc7	množství
nepravidelností	nepravidelnost	k1gFnPc2	nepravidelnost
<g/>
,	,	kIx,	,
originální	originální	k2eAgInPc1d1	originální
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
a	a	k8xC	a
obtížnou	obtížný	k2eAgFnSc7d1	obtížná
výslovností	výslovnost	k1gFnSc7	výslovnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
islandštině	islandština	k1gFnSc6	islandština
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
3	[number]	k4	3
rody	rod	k1gInPc7	rod
–	–	k?	–
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
a	a	k8xC	a
střední	střední	k2eAgMnSc1d1	střední
<g/>
,	,	kIx,	,
4	[number]	k4	4
pády	pád	k1gInPc1	pád
–	–	k?	–
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
a	a	k8xC	a
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
2	[number]	k4	2
mluvnická	mluvnický	k2eAgNnPc1d1	mluvnické
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
a	a	k8xC	a
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
a	a	k8xC	a
3	[number]	k4	3
slovesné	slovesný	k2eAgFnSc2d1	slovesná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
budoucnosti	budoucnost	k1gFnSc2	budoucnost
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
velmi	velmi	k6eAd1	velmi
lexikalizované	lexikalizovaný	k2eAgInPc1d1	lexikalizovaný
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
neexistuje	existovat	k5eNaImIp3nS	existovat
jeden	jeden	k4xCgInSc4	jeden
univerzální	univerzální	k2eAgInSc4d1	univerzální
gramatický	gramatický	k2eAgInSc4d1	gramatický
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Člen	člen	k1gMnSc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
člen	člen	k1gInSc1	člen
určitý	určitý	k2eAgInSc1d1	určitý
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gInSc4	člen
určitý	určitý	k2eAgInSc4d1	určitý
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
-	-	kIx~	-
buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
člen	člen	k1gInSc1	člen
určitý	určitý	k2eAgInSc1d1	určitý
připojen	připojen	k2eAgInSc1d1	připojen
jako	jako	k8xC	jako
přípona	přípona	k1gFnSc1	přípona
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
jménu	jméno	k1gNnSc3	jméno
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
postpozitivní	postpozitivní	k2eAgInSc1d1	postpozitivní
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgMnS	použít
člen	člen	k1gMnSc1	člen
hinn	hinn	k1gMnSc1	hinn
před	před	k7c7	před
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
člen	člen	k1gInSc1	člen
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
pádech	pád	k1gInPc6	pád
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
číslech	číslo	k1gNnPc6	číslo
korespodujících	korespodující	k2eAgInPc6d1	korespodující
s	s	k7c7	s
pádem	pád	k1gInSc7	pád
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
postpozitivního	postpozitivní	k2eAgInSc2d1	postpozitivní
určitého	určitý	k2eAgInSc2d1	určitý
členu	člen	k1gInSc2	člen
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
tvar	tvar	k1gInSc1	tvar
síminn	síminn	k1gNnSc1	síminn
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
sími	sím	k1gFnSc2	sím
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
druhého	druhý	k4xOgInSc2	druhý
způsobu	způsob	k1gInSc2	způsob
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
hinn	hinn	k1gNnSc1	hinn
gamli	gaml	k1gMnPc1	gaml
mað	mað	k?	mað
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Islandská	islandský	k2eAgNnPc1d1	islandské
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
ve	v	k7c6	v
4	[number]	k4	4
pádech	pád	k1gInPc6	pád
a	a	k8xC	a
2	[number]	k4	2
číslech	číslo	k1gNnPc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Islandština	islandština	k1gFnSc1	islandština
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
čeština	čeština	k1gFnSc1	čeština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tři	tři	k4xCgInPc1	tři
rody	rod	k1gInPc1	rod
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
–	–	k?	–
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
a	a	k8xC	a
střední	střední	k2eAgMnSc1d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
skloňovacích	skloňovací	k2eAgInPc2d1	skloňovací
vzorů	vzor	k1gInPc2	vzor
a	a	k8xC	a
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
slabé	slabý	k2eAgInPc4d1	slabý
a	a	k8xC	a
silné	silný	k2eAgInPc4d1	silný
skloňovací	skloňovací	k2eAgInPc4d1	skloňovací
vzory	vzor	k1gInPc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Slabé	Slabé	k2eAgInPc1d1	Slabé
skloňovací	skloňovací	k2eAgInPc1d1	skloňovací
vzory	vzor	k1gInPc1	vzor
jsou	být	k5eAaImIp3nP	být
snadné	snadný	k2eAgInPc1d1	snadný
k	k	k7c3	k
zapamatování	zapamatování	k1gNnSc3	zapamatování
a	a	k8xC	a
výjimky	výjimka	k1gFnSc2	výjimka
ve	v	k7c6	v
skloňování	skloňování	k1gNnSc6	skloňování
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
slabé	slabý	k2eAgInPc4d1	slabý
skloňovací	skloňovací	k2eAgInPc4d1	skloňovací
vzory	vzor	k1gInPc4	vzor
patří	patřit	k5eAaImIp3nP	patřit
–	–	k?	–
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc1d1	mužský
slova	slovo	k1gNnSc2	slovo
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-i	-i	k?	-i
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tími	tím	k1gFnSc2	tím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
ženský	ženský	k2eAgInSc1d1	ženský
slova	slovo	k1gNnSc2	slovo
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-a	-a	k?	-a
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stelpa	stelpa	k1gFnSc1	stelpa
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
střední	střední	k2eAgNnPc4d1	střední
slova	slovo	k1gNnPc4	slovo
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-a	-a	k?	-a
(	(	kIx(	(
<g/>
např.	např.	kA	např.
auga	auga	k1gFnSc1	auga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
v	v	k7c6	v
rodu	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
čísle	číslo	k1gNnSc6	číslo
<g/>
,	,	kIx,	,
pádu	pád	k1gInSc6	pád
a	a	k8xC	a
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pozitiv	pozitivum	k1gNnPc2	pozitivum
<g/>
)	)	kIx)	)
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
superlativ	superlativ	k1gInSc1	superlativ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
slabě	slabě	k6eAd1	slabě
a	a	k8xC	a
silně	silně	k6eAd1	silně
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
komparativ	komparativ	k1gInSc1	komparativ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
pouze	pouze	k6eAd1	pouze
slabě	slabě	k6eAd1	slabě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
množství	množství	k1gNnSc1	množství
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
neskloňují	skloňovat	k5eNaImIp3nP	skloňovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
přídavná	přídavný	k2eAgFnSc1d1	přídavná
jména	jméno	k1gNnSc2	jméno
končící	končící	k2eAgInPc4d1	končící
na	na	k7c6	na
-a	-a	k?	-a
v	v	k7c4	v
einmana	einman	k1gMnSc4	einman
(	(	kIx(	(
<g/>
osamělý	osamělý	k2eAgInSc4d1	osamělý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
-i	-i	k?	-i
např.	např.	kA	např.
v	v	k7c6	v
hugsi	hugse	k1gFnSc6	hugse
(	(	kIx(	(
<g/>
zamyšlený	zamyšlený	k2eAgMnSc1d1	zamyšlený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
nestupňují	stupňovat	k5eNaImIp3nP	stupňovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dauð	dauð	k?	dauð
(	(	kIx(	(
<g/>
mrtvý	mrtvý	k1gMnSc1	mrtvý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zájmena	zájmeno	k1gNnSc2	zájmeno
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
islandských	islandský	k2eAgNnPc2d1	islandské
zájmen	zájmeno	k1gNnPc2	zájmeno
se	se	k3xPyFc4	se
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
v	v	k7c6	v
rodě	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
pádu	pád	k1gInSc6	pád
a	a	k8xC	a
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Číslovky	číslovka	k1gFnSc2	číslovka
===	===	k?	===
</s>
</p>
<p>
<s>
Číslovky	číslovka	k1gFnPc1	číslovka
základní	základní	k2eAgFnSc1d1	základní
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
4	[number]	k4	4
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
v	v	k7c6	v
pádu	pád	k1gInSc6	pád
<g/>
,	,	kIx,	,
rodě	rod	k1gInSc6	rod
a	a	k8xC	a
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
číslovky	číslovka	k1gFnPc1	číslovka
základní	základní	k2eAgFnPc1d1	základní
se	se	k3xPyFc4	se
neskloňují	skloňovat	k5eNaImIp3nP	skloňovat
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
základní	základní	k2eAgFnSc1d1	základní
číslovka	číslovka	k1gFnSc1	číslovka
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvních	první	k4xOgFnPc2	první
20	[number]	k4	20
číslovek	číslovka	k1gFnPc2	číslovka
základních	základní	k2eAgFnPc2d1	základní
</s>
</p>
<p>
<s>
Číslovky	číslovka	k1gFnPc1	číslovka
řadové	řadový	k2eAgFnPc1d1	řadová
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
řadové	řadový	k2eAgFnPc1d1	řadová
číslovky	číslovka	k1gFnPc1	číslovka
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
mají	mít	k5eAaImIp3nP	mít
též	též	k9	též
silné	silný	k2eAgNnSc4d1	silné
skloňování	skloňování	k1gNnSc4	skloňování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvních	první	k4xOgFnPc2	první
20	[number]	k4	20
číslovek	číslovka	k1gFnPc2	číslovka
řadových	řadový	k2eAgFnPc2d1	řadová
(	(	kIx(	(
<g/>
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
rodě	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
pádu	pád	k1gInSc2	pád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesa	sloveso	k1gNnSc2	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgNnPc1	všechen
islandská	islandský	k2eAgNnPc1d1	islandské
slovesa	sloveso	k1gNnPc1	sloveso
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
intinitivu	intinitiv	k1gInSc6	intinitiv
na	na	k7c6	na
-	-	kIx~	-
<g/>
a.	a.	k?	a.
Některá	některý	k3yIgNnPc1	některý
slovesa	sloveso	k1gNnPc4	sloveso
končí	končit	k5eAaImIp3nP	končit
na	na	k7c4	na
-á	-á	k?	-á
např.	např.	kA	např.
slá	slá	k?	slá
(	(	kIx(	(
<g/>
uhodit	uhodit	k5eAaPmF	uhodit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkami	výjimka	k1gFnPc7	výjimka
jsou	být	k5eAaImIp3nP	být
modální	modální	k2eAgNnPc4d1	modální
slovesa	sloveso	k1gNnPc4	sloveso
munu	muna	k1gFnSc4	muna
a	a	k8xC	a
skulu	skula	k1gFnSc4	skula
<g/>
,	,	kIx,	,
sloveso	sloveso	k1gNnSc4	sloveso
þ	þ	k?	þ
(	(	kIx(	(
<g/>
mýt	mýt	k5eAaImF	mýt
<g/>
)	)	kIx)	)
a	a	k8xC	a
sloveso	sloveso	k1gNnSc4	sloveso
přejaté	přejatý	k2eAgNnSc4d1	přejaté
z	z	k7c2	z
dánštiny	dánština	k1gFnSc2	dánština
ske	ske	k?	ske
(	(	kIx(	(
<g/>
přihodit	přihodit	k5eAaPmF	přihodit
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc4	dva
příčestí	příčestí	k1gNnPc4	příčestí
<g/>
,	,	kIx,	,
přítomné	přítomný	k2eAgInPc4d1	přítomný
a	a	k8xC	a
minulé	minulý	k2eAgInPc4d1	minulý
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
příčestí	příčestí	k1gNnSc2	příčestí
přítomného	přítomný	k2eAgInSc2d1	přítomný
je	on	k3xPp3gNnSc4	on
-andi	nd	k1gMnPc1	-and
<g/>
.	.	kIx.	.
</s>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
přítomné	přítomný	k2eAgNnSc1d1	přítomné
může	moct	k5eAaImIp3nS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
buď	buď	k8xC	buď
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
nebo	nebo	k8xC	nebo
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
příčestí	příčestí	k1gNnSc1	příčestí
přítomné	přítomný	k2eAgFnSc2d1	přítomná
nesklonné	sklonný	k2eNgFnSc2d1	nesklonná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčestí	příčestí	k1gNnSc4	příčestí
minulé	minulý	k2eAgNnSc4d1	Minulé
se	se	k3xPyFc4	se
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
jako	jako	k9	jako
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
jak	jak	k8xC	jak
slabě	slabě	k6eAd1	slabě
tak	tak	k9	tak
silně	silně	k6eAd1	silně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
slovními	slovní	k2eAgInPc7d1	slovní
tvary	tvar	k1gInPc7	tvar
přímo	přímo	k6eAd1	přímo
jen	jen	k6eAd1	jen
přítomný	přítomný	k2eAgInSc4d1	přítomný
a	a	k8xC	a
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
jiných	jiný	k2eAgInPc2d1	jiný
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
předpřítomný	předpřítomný	k2eAgMnSc1d1	předpřítomný
<g/>
,	,	kIx,	,
předminulý	předminulý	k2eAgMnSc1d1	předminulý
<g/>
,	,	kIx,	,
průběhový	průběhový	k2eAgMnSc1d1	průběhový
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
pomocná	pomocný	k2eAgNnPc4d1	pomocné
slovesa	sloveso	k1gNnPc4	sloveso
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
jiný	jiný	k2eAgInSc4d1	jiný
čas	čas	k1gInSc4	čas
nebo	nebo	k8xC	nebo
vid	vid	k1gInSc4	vid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
slovesného	slovesný	k2eAgInSc2d1	slovesný
způsobu	způsob	k1gInSc2	způsob
-	-	kIx~	-
oznamovací	oznamovací	k2eAgFnSc4d1	oznamovací
<g/>
,	,	kIx,	,
spojovací	spojovací	k2eAgFnSc4d1	spojovací
a	a	k8xC	a
rozkazovací	rozkazovací	k2eAgFnSc4d1	rozkazovací
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
tvoří	tvořit	k5eAaImIp3nS	tvořit
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
slovesného	slovesný	k2eAgInSc2d1	slovesný
rodu	rod	k1gInSc2	rod
–	–	k?	–
činný	činný	k2eAgMnSc1d1	činný
(	(	kIx(	(
<g/>
aktivum	aktivum	k1gNnSc1	aktivum
<g/>
,	,	kIx,	,
is	is	k?	is
<g/>
.	.	kIx.	.
germynd	germynd	k1gMnSc1	germynd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trpný	trpný	k2eAgMnSc1d1	trpný
(	(	kIx(	(
<g/>
pasivum	pasivum	k1gNnSc1	pasivum
<g/>
,	,	kIx,	,
is	is	k?	is
<g/>
.	.	kIx.	.
þ	þ	k?	þ
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgInSc1d1	střední
slovesný	slovesný	k2eAgInSc1d1	slovesný
rod	rod	k1gInSc1	rod
(	(	kIx(	(
<g/>
médium	médium	k1gNnSc1	médium
<g/>
,	,	kIx,	,
is	is	k?	is
<g/>
.	.	kIx.	.
mið	mið	k?	mið
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pomocí	pomocí	k7c2	pomocí
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
vera	vera	k1gFnSc1	vera
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
a	a	k8xC	a
verð	verð	k?	verð
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příslovce	příslovce	k1gNnSc2	příslovce
===	===	k?	===
</s>
</p>
<p>
<s>
Islandská	islandský	k2eAgNnPc1d1	islandské
příslovce	příslovce	k1gNnPc1	příslovce
lze	lze	k6eAd1	lze
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
česká	český	k2eAgNnPc4d1	české
příslovce	příslovce	k1gNnPc4	příslovce
stupňovat	stupňovat	k5eAaImF	stupňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
komparativní	komparativní	k2eAgInPc1d1	komparativní
a	a	k8xC	a
superlativní	superlativní	k2eAgInPc1d1	superlativní
tvary	tvar	k1gInPc1	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
djúpt	djúpt	k1gInSc1	djúpt
(	(	kIx(	(
<g/>
hluboce	hluboko	k6eAd1	hluboko
<g/>
)	)	kIx)	)
–	–	k?	–
dýpra	dýpra	k1gFnSc1	dýpra
(	(	kIx(	(
<g/>
hlouběji	hluboko	k6eAd2	hluboko
<g/>
)	)	kIx)	)
–	–	k?	–
dýpst	dýpst	k1gInSc1	dýpst
(	(	kIx(	(
<g/>
nejhlouběji	hluboko	k6eAd3	hluboko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Islanďané	Islanďan	k1gMnPc1	Islanďan
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
chránit	chránit	k5eAaImF	chránit
svůj	svůj	k3xOyFgInSc4	svůj
starobylý	starobylý	k2eAgInSc4d1	starobylý
jazyk	jazyk	k1gInSc4	jazyk
před	před	k7c7	před
cizími	cizí	k2eAgInPc7d1	cizí
vlivy	vliv	k1gInPc7	vliv
a	a	k8xC	a
nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
mnohem	mnohem	k6eAd1	mnohem
raději	rád	k6eAd2	rád
tvoří	tvořit	k5eAaImIp3nS	tvořit
skládáním	skládání	k1gNnSc7	skládání
domácích	domácí	k2eAgInPc2d1	domácí
slovních	slovní	k2eAgInPc2d1	slovní
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
přizpůsobovali	přizpůsobovat	k5eAaImAgMnP	přizpůsobovat
slova	slovo	k1gNnPc4	slovo
cizí	cizí	k2eAgFnPc1d1	cizí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
části	část	k1gFnSc6	část
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
sdílí	sdílet	k5eAaImIp3nS	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
veð	veð	k?	veð
=	=	kIx~	=
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
fræ	fræ	k?	fræ
=	=	kIx~	=
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
veð	veð	k?	veð
=	=	kIx~	=
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Neznamená	znamenat	k5eNaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
úplně	úplně	k6eAd1	úplně
tabu	tabu	k1gNnSc7	tabu
<g/>
,	,	kIx,	,
např.	např.	kA	např.
banani	banan	k1gMnPc1	banan
=	=	kIx~	=
banán	banán	k1gInSc1	banán
<g/>
,	,	kIx,	,
kaffi	kaffi	k1gNnSc1	kaffi
=	=	kIx~	=
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
tóbak	tóbak	k1gInSc1	tóbak
=	=	kIx~	=
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
islandská	islandský	k2eAgNnPc4d1	islandské
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
téměř	téměř	k6eAd1	téměř
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jména	jméno	k1gNnPc1	jméno
po	po	k7c6	po
otci	otec	k1gMnSc3	otec
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
též	též	k6eAd1	též
popř.	popř.	kA	popř.
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Eiríksson	Eiríksson	k1gInSc1	Eiríksson
(	(	kIx(	(
<g/>
Erikův	Erikův	k2eAgMnSc1d1	Erikův
syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eiríksdóttir	Eiríksdóttir	k1gInSc1	Eiríksdóttir
(	(	kIx(	(
<g/>
Erikova	Erikův	k2eAgFnSc1d1	Erikova
dcera	dcera	k1gFnSc1	dcera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslovování	oslovování	k1gNnSc6	oslovování
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
křestní	křestní	k2eAgNnPc1d1	křestní
jména	jméno	k1gNnPc1	jméno
a	a	k8xC	a
podle	podle	k7c2	podle
křestních	křestní	k2eAgNnPc2d1	křestní
jmen	jméno	k1gNnPc2	jméno
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
řazeny	řazen	k2eAgInPc4d1	řazen
telefonní	telefonní	k2eAgInPc4d1	telefonní
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
seznamy	seznam	k1gInPc4	seznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
islandštiny	islandština	k1gFnSc2	islandština
s	s	k7c7	s
norštinou	norština	k1gFnSc7	norština
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
uveden	uveden	k2eAgInSc1d1	uveden
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
islandština	islandština	k1gFnSc1	islandština
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
islandština	islandština	k1gFnSc1	islandština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
islandština	islandština	k1gFnSc1	islandština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Islandština	islandština	k1gFnSc1	islandština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Islandština	islandština	k1gFnSc1	islandština
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
na	na	k7c4	na
Omniglot	Omniglot	k1gInSc4	Omniglot
</s>
</p>
<p>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
brožura	brožura	k1gFnSc1	brožura
o	o	k7c6	o
islandštině	islandština	k1gFnSc6	islandština
vydaná	vydaný	k2eAgNnPc4d1	vydané
Islandským	islandský	k2eAgInSc7d1	islandský
jazykovým	jazykový	k2eAgInSc7d1	jazykový
ústavem	ústav	k1gInSc7	ústav
</s>
</p>
<p>
<s>
Bezplatný	bezplatný	k2eAgInSc1d1	bezplatný
internetový	internetový	k2eAgInSc1d1	internetový
kurz	kurz	k1gInSc1	kurz
islandštiny	islandština	k1gFnSc2	islandština
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
islandské	islandský	k2eAgFnSc2d1	islandská
výslovnosti	výslovnost	k1gFnSc2	výslovnost
(	(	kIx(	(
<g/>
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
poslechu	poslech	k1gInSc2	poslech
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
spilaHvalur	spilaHvalur	k1gMnSc1	spilaHvalur
-	-	kIx~	-
hrací	hrací	k2eAgFnSc1d1	hrací
velryba	velryba	k1gFnSc1	velryba
-	-	kIx~	-
islandské	islandský	k2eAgFnPc1d1	islandská
výukové	výukový	k2eAgFnPc1d1	výuková
hry	hra	k1gFnPc1	hra
</s>
</p>
<p>
<s>
===	===	k?	===
Slovníky	slovník	k1gInPc4	slovník
===	===	k?	===
</s>
</p>
<p>
<s>
Islandsko-český	islandsko-český	k2eAgInSc1d1	islandsko-český
slovník	slovník	k1gInSc1	slovník
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kupča	Kupča	k1gMnSc1	Kupča
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
online	onlinout	k5eAaPmIp3nS	onlinout
podobě	podoba	k1gFnSc6	podoba
i	i	k8xC	i
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
Litera	litera	k1gFnSc1	litera
Proxima	Proxima	k1gFnSc1	Proxima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Islandsko-český	islandsko-český	k2eAgInSc1d1	islandsko-český
studijní	studijní	k2eAgInSc1d1	studijní
slovník	slovník	k1gInSc1	slovník
Aleš	Aleš	k1gMnSc1	Aleš
Chejn	Chejn	k1gMnSc1	Chejn
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Zaťko	Zaťko	k1gNnSc1	Zaťko
<g/>
,	,	kIx,	,
Jón	Jón	k1gMnSc1	Jón
Gíslason	Gíslason	k1gMnSc1	Gíslason
a	a	k8xC	a
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kupča	Kupča	k1gMnSc1	Kupča
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
online	onlinout	k5eAaPmIp3nS	onlinout
podobě	podoba	k1gFnSc6	podoba
i	i	k8xC	i
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Dostupný	dostupný	k2eAgInSc1d1	dostupný
pod	pod	k7c7	pod
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
licencí	licence	k1gFnSc7	licence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Icelandic-English	Icelandic-English	k1gInSc1	Icelandic-English
Dictionary	Dictionara	k1gFnSc2	Dictionara
/	/	kIx~	/
Íslensk-ensk	Íslensknsk	k1gInSc1	Íslensk-ensk
orð	orð	k?	orð
Sverrir	Sverrir	k1gInSc1	Sverrir
Hólmarsson	Hólmarssona	k1gFnPc2	Hólmarssona
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Sanders	Sandersa	k1gFnPc2	Sandersa
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Tucker	Tucker	k1gMnSc1	Tucker
<g/>
.	.	kIx.	.
</s>
<s>
Searchable	Searchable	k6eAd1	Searchable
dictionary	dictionar	k1gInPc1	dictionar
from	froma	k1gFnPc2	froma
the	the	k?	the
University	universita	k1gFnSc2	universita
of	of	k?	of
Wisconsin	Wisconsin	k1gMnSc1	Wisconsin
<g/>
–	–	k?	–
<g/>
Madison	Madison	k1gMnSc1	Madison
Libraries	Libraries	k1gMnSc1	Libraries
</s>
</p>
<p>
<s>
Icelandic	Icelandic	k1gMnSc1	Icelandic
-	-	kIx~	-
English	English	k1gMnSc1	English
Dictionary	Dictionara	k1gFnSc2	Dictionara
<g/>
:	:	kIx,	:
from	from	k1gMnSc1	from
Webster	Webster	k1gMnSc1	Webster
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rosetta	Rosett	k1gMnSc4	Rosett
Edition	Edition	k1gInSc4	Edition
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Old	Olda	k1gFnPc2	Olda
Icelandic-English	Icelandic-English	k1gMnSc1	Icelandic-English
Dictionary	Dictionara	k1gFnSc2	Dictionara
by	by	kYmCp3nS	by
Richard	Richard	k1gMnSc1	Richard
Cleasby	Cleasba	k1gFnSc2	Cleasba
and	and	k?	and
Gudbrand	Gudbrand	k1gInSc1	Gudbrand
Vigfusson	Vigfusson	k1gInSc1	Vigfusson
</s>
</p>
