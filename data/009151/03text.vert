<p>
<s>
Browning	browning	k1gInSc1	browning
M1919	M1919	k1gFnSc2	M1919
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
M	M	kA	M
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
těžký	těžký	k2eAgMnSc1d1	těžký
vzduchem	vzduch	k1gInSc7	vzduch
chlazený	chlazený	k2eAgInSc1d1	chlazený
kulomet	kulomet	k1gInSc1	kulomet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
používali	používat	k5eAaImAgMnP	používat
spojenci	spojenec	k1gMnPc1	spojenec
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
armáda	armáda	k1gFnSc1	armáda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kulomet	kulomet	k1gInSc1	kulomet
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
kulometu	kulomet	k1gInSc2	kulomet
M	M	kA	M
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
hlaveň	hlaveň	k1gFnSc4	hlaveň
chlazenou	chlazený	k2eAgFnSc7d1	chlazená
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
M1917	M1917	k1gFnSc3	M1917
měl	mít	k5eAaImAgMnS	mít
M1919	M1919	k1gFnSc3	M1919
hlaveň	hlaveň	k1gFnSc4	hlaveň
chlazenou	chlazený	k2eAgFnSc4d1	chlazená
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
střílet	střílet	k5eAaImF	střílet
kratšími	krátký	k2eAgFnPc7d2	kratší
dávkami	dávka	k1gFnPc7	dávka
než	než	k8xS	než
M	M	kA	M
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
snadněji	snadno	k6eAd2	snadno
přenosný	přenosný	k2eAgInSc1d1	přenosný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používaný	používaný	k2eAgInSc1d1	používaný
byl	být	k5eAaImAgInS	být
především	především	k9	především
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
USA	USA	kA	USA
rozbíhala	rozbíhat	k5eAaImAgFnS	rozbíhat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
zakázek	zakázka	k1gFnPc2	zakázka
zrušena	zrušen	k2eAgFnSc1d1	zrušena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kulomet	kulomet	k1gInSc1	kulomet
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
M1919	M1919	k4	M1919
byl	být	k5eAaImAgMnS	být
vyráběn	vyrábět	k5eAaImNgMnS	vyrábět
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
verzí	verze	k1gFnPc2	verze
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
–	–	k?	–
jeho	jeho	k3xOp3gFnPc7	jeho
přednostmi	přednost	k1gFnPc7	přednost
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
jakýchkoliv	jakýkoliv	k3yIgFnPc6	jakýkoliv
podmínkách	podmínka	k1gFnPc6	podmínka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
s	s	k7c7	s
trojnožkou	trojnožka	k1gFnSc7	trojnožka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
lafetách	lafeta	k1gFnPc6	lafeta
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
na	na	k7c6	na
vozidlech	vozidlo	k1gNnPc6	vozidlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jeep	jeep	k1gInSc4	jeep
Willys	Willysa	k1gFnPc2	Willysa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
kulomety	kulomet	k1gInPc7	kulomet
M1919	M1919	k1gFnSc2	M1919
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
kulometu	kulomet	k1gInSc2	kulomet
konstrukčně	konstrukčně	k6eAd1	konstrukčně
vychází	vycházet	k5eAaImIp3nS	vycházet
také	také	k9	také
Browning	browning	k1gInSc1	browning
vz.	vz.	k?	vz.
1924	[number]	k4	1924
později	pozdě	k6eAd2	pozdě
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Browning	browning	k1gInSc1	browning
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
hojném	hojný	k2eAgNnSc6d1	hojné
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Kulomet	kulomet	k1gInSc1	kulomet
využívá	využívat	k5eAaImIp3nS	využívat
krátký	krátký	k2eAgInSc4d1	krátký
zákluz	zákluz	k1gInSc4	zákluz
hlavně	hlavně	k6eAd1	hlavně
<g/>
,	,	kIx,	,
nábojová	nábojový	k2eAgFnSc1d1	nábojová
komora	komora	k1gFnSc1	komora
je	být	k5eAaImIp3nS	být
uzamčena	uzamknout	k5eAaPmNgFnS	uzamknout
přímoběžným	přímoběžný	k2eAgInSc7d1	přímoběžný
závěrem	závěr	k1gInSc7	závěr
s	s	k7c7	s
uzamykací	uzamykací	k2eAgFnSc7d1	uzamykací
závorou	závora	k1gFnSc7	závora
vloženou	vložený	k2eAgFnSc7d1	vložená
do	do	k7c2	do
pouzdra	pouzdro	k1gNnSc2	pouzdro
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Závora	závora	k1gFnSc1	závora
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
vertikálně	vertikálně	k6eAd1	vertikálně
posuvného	posuvný	k2eAgInSc2d1	posuvný
klínu	klín	k1gInSc2	klín
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
nosiče	nosič	k1gMnSc2	nosič
závorníku	závorník	k1gInSc2	závorník
oproti	oproti	k7c3	oproti
závorníku	závorník	k1gInSc3	závorník
při	při	k7c6	při
odemykání	odemykání	k1gNnSc6	odemykání
a	a	k8xC	a
pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
nosiče	nosič	k1gMnSc4	nosič
vzad	vzad	k6eAd1	vzad
je	být	k5eAaImIp3nS	být
použit	použit	k2eAgInSc4d1	použit
pákový	pákový	k2eAgInSc4d1	pákový
zrychlovač	zrychlovač	k1gInSc4	zrychlovač
otáčející	otáčející	k2eAgInSc4d1	otáčející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Zásobování	zásobování	k1gNnSc1	zásobování
kulometu	kulomet	k1gInSc2	kulomet
je	být	k5eAaImIp3nS	být
řešeno	řešit	k5eAaImNgNnS	řešit
tkaninovým	tkaninový	k2eAgInSc7d1	tkaninový
nábojovým	nábojový	k2eAgInSc7d1	nábojový
pásem	pás	k1gInSc7	pás
vkládaným	vkládaný	k2eAgInSc7d1	vkládaný
do	do	k7c2	do
kulometu	kulomet	k1gInSc2	kulomet
zleva	zleva	k6eAd1	zleva
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
s	s	k7c7	s
náboji	náboj	k1gInPc7	náboj
s	s	k7c7	s
drážkou	drážka	k1gFnSc7	drážka
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
<g/>
.	.	kIx.	.
</s>
<s>
Použito	použít	k5eAaPmNgNnS	použít
u	u	k7c2	u
kulometů	kulomet	k1gInPc2	kulomet
Maxim	Maxima	k1gFnPc2	Maxima
a	a	k8xC	a
odvozených	odvozený	k2eAgFnPc2d1	odvozená
Vickers	Vickersa	k1gFnPc2	Vickersa
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
ovšem	ovšem	k9	ovšem
používají	používat	k5eAaImIp3nP	používat
náboje	náboj	k1gInPc1	náboj
s	s	k7c7	s
okrajovou	okrajový	k2eAgFnSc7d1	okrajová
nábojnicí	nábojnice	k1gFnSc7	nábojnice
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgInPc1d1	kovový
nábojové	nábojový	k2eAgInPc1d1	nábojový
pásy	pás	k1gInPc1	pás
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
zaváděly	zavádět	k5eAaImAgFnP	zavádět
pro	pro	k7c4	pro
zbraně	zbraň	k1gFnPc4	zbraň
v	v	k7c6	v
letadlech	letadlo	k1gNnPc6	letadlo
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
použitím	použití	k1gNnSc7	použití
u	u	k7c2	u
pěchoty	pěchota	k1gFnSc2	pěchota
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
nepočítalo	počítat	k5eNaImAgNnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylodění	vylodění	k1gNnSc6	vylodění
spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
u	u	k7c2	u
pěchoty	pěchota	k1gFnSc2	pěchota
mohly	moct	k5eAaImAgInP	moct
objevit	objevit	k5eAaPmF	objevit
kovové	kovový	k2eAgInPc1d1	kovový
pásy	pás	k1gInPc1	pás
M1	M1	k1gFnSc3	M1
ale	ale	k8xC	ale
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
o	o	k7c4	o
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnSc1d2	veliký
rozšíření	rozšíření	k1gNnSc1	rozšíření
nastalo	nastat	k5eAaPmAgNnS	nastat
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
objevovaly	objevovat	k5eAaImAgInP	objevovat
látkové	látkový	k2eAgInPc1d1	látkový
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
většině	většina	k1gFnSc3	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
kulometů	kulomet	k1gInPc2	kulomet
střílí	střílet	k5eAaImIp3nS	střílet
M1919	M1919	k1gFnSc1	M1919
z	z	k7c2	z
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
závěru	závěr	k1gInSc2	závěr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
výstřelem	výstřel	k1gInSc7	výstřel
je	být	k5eAaImIp3nS	být
náboj	náboj	k1gInSc1	náboj
vložený	vložený	k2eAgInSc1d1	vložený
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
přesnost	přesnost	k1gFnSc4	přesnost
střelby	střelba	k1gFnSc2	střelba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
samovolné	samovolný	k2eAgFnSc3d1	samovolná
střelbě	střelba	k1gFnSc3	střelba
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
při	při	k7c6	při
iniciaci	iniciace	k1gFnSc6	iniciace
náboje	náboj	k1gInSc2	náboj
teplem	teplo	k1gNnSc7	teplo
v	v	k7c6	v
hlavni	hlaveň	k1gFnSc6	hlaveň
přehřáté	přehřátý	k2eAgNnSc1d1	přehřáté
delší	dlouhý	k2eAgFnSc7d2	delší
střelbou	střelba	k1gFnSc7	střelba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Verze	verze	k1gFnSc2	verze
používané	používaný	k2eAgFnSc2d1	používaná
pozemním	pozemní	k2eAgNnSc7d1	pozemní
vojskem	vojsko	k1gNnSc7	vojsko
==	==	k?	==
</s>
</p>
<p>
<s>
M1919A1	M1919A1	k4	M1919A1
–	–	k?	–
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
úpravu	úprava	k1gFnSc4	úprava
jako	jako	k8xS	jako
lehký	lehký	k2eAgInSc4d1	lehký
kulomet	kulomet	k1gInSc4	kulomet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
proto	proto	k6eAd1	proto
osazen	osadit	k5eAaPmNgInS	osadit
lehkou	lehký	k2eAgFnSc7d1	lehká
hlavní	hlavní	k2eAgFnSc7d1	hlavní
a	a	k8xC	a
dvojnožkou	dvojnožka	k1gFnSc7	dvojnožka
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
rychle	rychle	k6eAd1	rychle
přehřívala	přehřívat	k5eAaImAgFnS	přehřívat
hlaveň	hlaveň	k1gFnSc4	hlaveň
a	a	k8xC	a
proto	proto	k8xC	proto
mohl	moct	k5eAaImAgInS	moct
střílet	střílet	k5eAaImF	střílet
pouze	pouze	k6eAd1	pouze
krátké	krátký	k2eAgFnPc4d1	krátká
dávky	dávka	k1gFnPc4	dávka
a	a	k8xC	a
jako	jako	k9	jako
podpůrná	podpůrný	k2eAgFnSc1d1	podpůrná
zbraň	zbraň	k1gFnSc1	zbraň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
nepoužitelný	použitelný	k2eNgMnSc1d1	nepoužitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M1919A2	M1919A2	k4	M1919A2
–	–	k?	–
používán	používat	k5eAaImNgInS	používat
americkou	americký	k2eAgFnSc7d1	americká
jízdou	jízda	k1gFnSc7	jízda
</s>
</p>
<p>
<s>
M1919A3	M1919A3	k4	M1919A3
</s>
</p>
<p>
<s>
M1919A4	M1919A4	k4	M1919A4
–	–	k?	–
Používán	používat	k5eAaImNgInS	používat
pěchotou	pěchota	k1gFnSc7	pěchota
<g/>
,	,	kIx,	,
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
438971	[number]	k4	438971
ks	ks	kA	ks
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M1919A5	M1919A5	k4	M1919A5
–	–	k?	–
Používán	používán	k2eAgMnSc1d1	používán
na	na	k7c6	na
tancích	tanec	k1gInPc6	tanec
<g/>
,	,	kIx,	,
na	na	k7c6	na
kulometu	kulomet	k1gInSc6	kulomet
byla	být	k5eAaImAgFnS	být
úprava	úprava	k1gFnSc1	úprava
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
nasazení	nasazení	k1gNnSc2	nasazení
na	na	k7c4	na
lafetu	lafeta	k1gFnSc4	lafeta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
lafetován	lafetovat	k5eAaImNgInS	lafetovat
na	na	k7c6	na
tancích	tanec	k1gInPc6	tanec
<g/>
,	,	kIx,	,
obrněných	obrněný	k2eAgInPc6d1	obrněný
transportérech	transportér	k1gInPc6	transportér
<g/>
,	,	kIx,	,
Jeepech	jeep	k1gInPc6	jeep
a	a	k8xC	a
vyloďovacích	vyloďovací	k2eAgInPc6d1	vyloďovací
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M1919A6	M1919A6	k4	M1919A6
–	–	k?	–
Používán	používán	k2eAgInSc4d1	používán
pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
palebné	palebný	k2eAgFnSc2d1	palebná
síly	síla	k1gFnSc2	síla
pěchoty	pěchota	k1gFnSc2	pěchota
zaveden	zavést	k5eAaPmNgInS	zavést
až	až	k6eAd1	až
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
plnit	plnit	k5eAaImF	plnit
úlohu	úloha	k1gFnSc4	úloha
lehkého	lehký	k2eAgInSc2d1	lehký
kulometu	kulomet	k1gInSc2	kulomet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
vybaven	vybavit	k5eAaPmNgInS	vybavit
kuželově	kuželově	k6eAd1	kuželově
zužující	zužující	k2eAgInSc1d1	zužující
se	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
<g/>
,	,	kIx,	,
držadlem	držadlo	k1gNnSc7	držadlo
pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
přenášení	přenášení	k1gNnSc4	přenášení
a	a	k8xC	a
pažbou	pažba	k1gFnSc7	pažba
<g/>
,	,	kIx,	,
jenomže	jenomže	k8xC	jenomže
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
kulomet	kulomet	k1gInSc1	kulomet
Lewis	Lewis	k1gFnPc2	Lewis
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
Brity	Brit	k1gMnPc4	Brit
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
znamenal	znamenat	k5eAaImAgInS	znamenat
zlepšení	zlepšení	k1gNnSc4	zlepšení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
obsluhy	obsluha	k1gFnSc2	obsluha
kulometu	kulomet	k1gInSc2	kulomet
ubyl	ubýt	k5eAaPmAgMnS	ubýt
nosič	nosič	k1gInSc4	nosič
trojnožky	trojnožka	k1gFnSc2	trojnožka
<g/>
.	.	kIx.	.
</s>
<s>
Velkého	velký	k2eAgNnSc2d1	velké
uplatnění	uplatnění	k1gNnSc2	uplatnění
se	se	k3xPyFc4	se
kulomet	kulomet	k1gInSc1	kulomet
dočkal	dočkat	k5eAaPmAgInS	dočkat
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
<g/>
Základní	základní	k2eAgInPc1d1	základní
parametry	parametr	k1gInPc1	parametr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
964	[number]	k4	964
mm	mm	kA	mm
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
1919	[number]	k4	1919
<g/>
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1.346	[number]	k4	1.346
mm	mm	kA	mm
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
1919	[number]	k4	1919
<g/>
A	a	k9	a
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
hlavně	hlavně	k9	hlavně
<g/>
:	:	kIx,	:
609	[number]	k4	609
mm	mm	kA	mm
</s>
</p>
<p>
<s>
ráže	ráže	k1gFnSc1	ráže
<g/>
:	:	kIx,	:
.30	.30	k4	.30
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
Springfield	Springfielda	k1gFnPc2	Springfielda
</s>
</p>
<p>
<s>
kadence	kadence	k1gFnSc1	kadence
<g/>
:	:	kIx,	:
450	[number]	k4	450
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
ran	rána	k1gFnPc2	rána
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
</s>
</p>
<p>
<s>
zásobování	zásobování	k1gNnSc1	zásobování
<g/>
:	:	kIx,	:
tkaný	tkaný	k2eAgInSc1d1	tkaný
pás	pás	k1gInSc1	pás
100	[number]	k4	100
nebo	nebo	k8xC	nebo
250	[number]	k4	250
nábojů	náboj	k1gInPc2	náboj
</s>
</p>
<p>
<s>
==	==	k?	==
Verze	verze	k1gFnPc4	verze
používané	používaný	k2eAgFnPc4d1	používaná
letectvem	letectvo	k1gNnSc7	letectvo
==	==	k?	==
</s>
</p>
<p>
<s>
Existovala	existovat	k5eAaImAgFnS	existovat
také	také	k9	také
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
kadencí	kadence	k1gFnSc7	kadence
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
na	na	k7c4	na
700	[number]	k4	700
ran	rána	k1gFnPc2	rána
/	/	kIx~	/
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Protože	protože	k8xS	protože
ale	ale	k9	ale
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
belgických	belgický	k2eAgMnPc2d1	belgický
zbrojařů	zbrojař	k1gMnPc2	zbrojař
byl	být	k5eAaImAgInS	být
upraven	upraven	k2eAgInSc1d1	upraven
kulomet	kulomet	k1gInSc1	kulomet
M2	M2	k1gMnPc2	M2
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
letadlech	letadlo	k1gNnPc6	letadlo
odlehčením	odlehčení	k1gNnSc7	odlehčení
a	a	k8xC	a
zvýšením	zvýšení	k1gNnSc7	zvýšení
kadence	kadence	k1gFnSc2	kadence
<g/>
:	:	kIx,	:
Vážil	vážit	k5eAaImAgMnS	vážit
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
hm	hm	k?	hm
M1919	M1919	k1gMnSc1	M1919
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
M1919	M1919	k1gFnSc4	M1919
a	a	k8xC	a
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
kadence	kadence	k1gFnSc2	kadence
1	[number]	k4	1
200	[number]	k4	200
ran	rána	k1gFnPc2	rána
/	/	kIx~	/
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Některé	některý	k3yIgFnPc1	některý
verze	verze	k1gFnPc1	verze
údajně	údajně	k6eAd1	údajně
i	i	k8xC	i
1	[number]	k4	1
500	[number]	k4	500
ran	rána	k1gFnPc2	rána
/	/	kIx~	/
min	mina	k1gFnPc2	mina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
letecký	letecký	k2eAgInSc1d1	letecký
M1919	M1919	k1gMnSc2	M1919
dodáván	dodávat	k5eAaImNgInS	dodávat
hlavně	hlavně	k9	hlavně
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
ráže	ráže	k1gFnPc4	ráže
==	==	k?	==
</s>
</p>
<p>
<s>
Kulomety	kulomet	k1gInPc1	kulomet
M1919	M1919	k1gFnPc2	M1919
existovaly	existovat	k5eAaImAgInP	existovat
také	také	k9	také
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
rážích	ráže	k1gFnPc6	ráže
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
.303	.303	k4	.303
British	British	k1gMnSc1	British
–	–	k?	–
Britská	britský	k2eAgFnSc1d1	britská
letecká	letecký	k2eAgFnSc1d1	letecká
a	a	k8xC	a
tanková	tankový	k2eAgFnSc1d1	tanková
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
×	×	k?	×
57	[number]	k4	57
IS	IS	kA	IS
(	(	kIx(	(
<g/>
7,92	[number]	k4	7,92
mm	mm	kA	mm
<g/>
)	)	kIx)	)
–	–	k?	–
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
ukořistěnou	ukořistěný	k2eAgFnSc7d1	ukořistěná
německou	německý	k2eAgFnSc7d1	německá
municí	munice	k1gFnSc7	munice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
partyzánské	partyzánský	k2eAgFnPc4d1	Partyzánská
a	a	k8xC	a
odbojové	odbojový	k2eAgFnPc4d1	odbojová
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6,5	[number]	k4	6,5
×	×	k?	×
55	[number]	k4	55
<g/>
mm	mm	kA	mm
a	a	k8xC	a
8	[number]	k4	8
×	×	k?	×
63	[number]	k4	63
mm	mm	kA	mm
jako	jako	k8xS	jako
Švédský	švédský	k2eAgMnSc1d1	švédský
Carl	Carl	k1gMnSc1	Carl
Gustaf	Gustaf	k1gMnSc1	Gustaf
SGF	SGF	kA	SGF
</s>
</p>
<p>
<s>
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
M1919	M1919	k1gFnSc2	M1919
upravena	upravit	k5eAaPmNgFnS	upravit
na	na	k7c6	na
ráži	ráže	k1gFnSc6	ráže
7,62	[number]	k4	7,62
x	x	k?	x
51	[number]	k4	51
mm	mm	kA	mm
NATO	NATO	kA	NATO
a	a	k8xC	a
používána	používán	k2eAgFnSc1d1	používána
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
některými	některý	k3yIgMnPc7	některý
spojenci	spojenec	k1gMnPc7	spojenec
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uživatelé	uživatel	k1gMnPc1	uživatel
==	==	k?	==
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
</s>
</p>
<p>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
</s>
</p>
<p>
<s>
Kambodža	Kambodža	k1gFnSc1	Kambodža
–	–	k?	–
místní	místní	k2eAgNnSc4d1	místní
označení	označení	k1gNnSc4	označení
M30	M30	k1gFnSc2	M30
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
</s>
</p>
<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
</s>
</p>
<p>
<s>
Egypt	Egypt	k1gInSc1	Egypt
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
<s>
Indie	Indie	k1gFnSc1	Indie
–	–	k?	–
(	(	kIx(	(
<g/>
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
</s>
</p>
<p>
<s>
Myanmar	Myanmar	k1gMnSc1	Myanmar
</s>
</p>
<p>
<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
</s>
</p>
<p>
<s>
JAR	jar	k1gFnSc1	jar
</s>
</p>
<p>
<s>
Thajsko	Thajsko	k1gNnSc1	Thajsko
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
M73	M73	k4	M73
</s>
</p>
<p>
<s>
M2	M2	k4	M2
Browning	browning	k1gInSc1	browning
</s>
</p>
<p>
<s>
M1917	M1917	k4	M1917
Browning	browning	k1gInSc1	browning
</s>
</p>
<p>
<s>
kulomet	kulomet	k1gInSc1	kulomet
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
M1919	M1919	k1gFnSc2	M1919
Browning	browning	k1gInSc1	browning
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
