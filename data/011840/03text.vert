<p>
<s>
Spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
a	a	k8xC	a
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
spekter	spektrum	k1gNnPc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
metoda	metoda	k1gFnSc1	metoda
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
interakci	interakce	k1gFnSc6	interakce
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
se	s	k7c7	s
vzorkem	vzorek	k1gInSc7	vzorek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
je	být	k5eAaImIp3nS	být
sir	sir	k1gMnSc1	sir
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pomocí	pomocí	k7c2	pomocí
optického	optický	k2eAgInSc2d1	optický
hranolu	hranol	k1gInSc2	hranol
objevil	objevit	k5eAaPmAgMnS	objevit
monochromatické	monochromatický	k2eAgNnSc4d1	monochromatické
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	čí	k3xOyRgNnSc7	čí
čistější	čistý	k2eAgNnSc1d2	čistší
monochromatické	monochromatický	k2eAgNnSc1d1	monochromatické
světlo	světlo	k1gNnSc1	světlo
získává	získávat	k5eAaImIp3nS	získávat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
slabší	slabý	k2eAgFnSc1d2	slabší
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
intenzita	intenzita	k1gFnSc1	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
překonaly	překonat	k5eAaPmAgFnP	překonat
až	až	k6eAd1	až
lasery	laser	k1gInPc7	laser
a	a	k8xC	a
synchrotrony	synchrotron	k1gInPc7	synchrotron
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
spektrometr	spektrometr	k1gInSc4	spektrometr
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Kirchhoff	Kirchhoff	k1gInSc4	Kirchhoff
a	a	k8xC	a
Bunsen	Bunsen	k1gInSc4	Bunsen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgNnSc7	první
hlediskem	hledisko	k1gNnSc7	hledisko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
typ	typ	k1gInSc4	typ
interakce	interakce	k1gFnSc2	interakce
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
hmotou	hmota	k1gFnSc7	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Atom	atom	k1gInSc1	atom
nebo	nebo	k8xC	nebo
molekula	molekula	k1gFnSc1	molekula
mohou	moct	k5eAaImIp3nP	moct
záření	zářený	k2eAgMnPc1d1	zářený
pohltit	pohltit	k5eAaPmF	pohltit
(	(	kIx(	(
<g/>
absorpce	absorpce	k1gFnSc1	absorpce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
naopak	naopak	k6eAd1	naopak
uvolnit	uvolnit	k5eAaPmF	uvolnit
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
emise	emise	k1gFnSc1	emise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
záření	záření	k1gNnSc4	záření
pohltit	pohltit	k5eAaPmF	pohltit
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
jej	on	k3xPp3gMnSc4	on
opět	opět	k6eAd1	opět
emitovat	emitovat	k5eAaBmF	emitovat
(	(	kIx(	(
<g/>
fluorescence	fluorescence	k1gFnSc1	fluorescence
a	a	k8xC	a
fosforescence	fosforescence	k1gFnSc1	fosforescence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
vlastností	vlastnost	k1gFnSc7	vlastnost
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
záření	záření	k1gNnSc4	záření
určitých	určitý	k2eAgFnPc2d1	určitá
specifických	specifický	k2eAgFnPc2d1	specifická
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Absorbované	absorbovaný	k2eAgNnSc1d1	absorbované
nebo	nebo	k8xC	nebo
emitované	emitovaný	k2eAgNnSc1d1	emitované
spektrum	spektrum	k1gNnSc1	spektrum
není	být	k5eNaImIp3nS	být
spojité	spojitý	k2eAgNnSc1d1	spojité
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
linií	linie	k1gFnPc2	linie
(	(	kIx(	(
<g/>
čar	čára	k1gFnPc2	čára
nebo	nebo	k8xC	nebo
pásů	pás	k1gInPc2	pás
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
specifické	specifický	k2eAgFnPc1d1	specifická
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
chemicky	chemicky	k6eAd1	chemicky
odlišné	odlišný	k2eAgFnPc1d1	odlišná
látky	látka	k1gFnPc1	látka
mající	mající	k2eAgInPc4d1	mající
stejné	stejný	k2eAgInPc4d1	stejný
absorpční	absorpční	k2eAgInPc4d1	absorpční
nebo	nebo	k8xC	nebo
emisní	emisní	k2eAgNnSc4d1	emisní
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalné	kapalný	k2eAgFnSc6d1	kapalná
a	a	k8xC	a
pevné	pevný	k2eAgFnSc3d1	pevná
fázi	fáze	k1gFnSc3	fáze
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
spektra	spektrum	k1gNnPc4	spektrum
pásová	pásový	k2eAgNnPc4d1	pásové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plynné	plynný	k2eAgFnSc6d1	plynná
fázi	fáze	k1gFnSc6	fáze
mají	mít	k5eAaImIp3nP	mít
spektra	spektrum	k1gNnSc2	spektrum
podobu	podoba	k1gFnSc4	podoba
separovaných	separovaný	k2eAgFnPc2d1	separovaná
linií	linie	k1gFnPc2	linie
v	v	k7c6	v
případě	případ	k1gInSc6	případ
molekul	molekula	k1gFnPc2	molekula
sdružených	sdružený	k2eAgFnPc2d1	sdružená
do	do	k7c2	do
pásů	pás	k1gInPc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
každá	každý	k3xTgFnSc1	každý
linie	linie	k1gFnSc2	linie
změně	změna	k1gFnSc3	změna
rotace	rotace	k1gFnSc2	rotace
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
blízké	blízký	k2eAgFnSc6d1	blízká
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
změně	změna	k1gFnSc6	změna
vibrace	vibrace	k1gFnSc1	vibrace
a	a	k8xC	a
rotace	rotace	k1gFnSc1	rotace
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
a	a	k8xC	a
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
oblasti	oblast	k1gFnSc6	oblast
spekter	spektrum	k1gNnPc2	spektrum
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
elektronovým	elektronový	k2eAgFnPc3d1	elektronová
změnám	změna	k1gFnPc3	změna
(	(	kIx(	(
<g/>
přeskokům	přeskok	k1gInPc3	přeskok
z	z	k7c2	z
orbitalů	orbital	k1gInPc2	orbital
různých	různý	k2eAgFnPc2d1	různá
energií	energie	k1gFnPc2	energie
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vnějších	vnější	k2eAgFnPc6d1	vnější
slupkách	slupka	k1gFnPc6	slupka
molekulových	molekulový	k2eAgMnPc2d1	molekulový
a	a	k8xC	a
atomových	atomový	k2eAgMnPc2d1	atomový
orbitalů	orbital	k1gInPc2	orbital
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
slupkách	slupka	k1gFnPc6	slupka
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
záření	záření	k1gNnSc1	záření
gama	gama	k1gNnSc2	gama
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
přeskokům	přeskok	k1gInPc3	přeskok
jaderných	jaderný	k2eAgFnPc2d1	jaderná
částic	částice	k1gFnPc2	částice
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
energetickými	energetický	k2eAgFnPc7d1	energetická
hladinami	hladina	k1gFnPc7	hladina
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
vlnové	vlnový	k2eAgMnPc4d1	vlnový
délky	délka	k1gFnSc2	délka
linií	linie	k1gFnPc2	linie
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
těmto	tento	k3xDgFnPc3	tento
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
energii	energie	k1gFnSc6	energie
molekul	molekula	k1gFnPc2	molekula
a	a	k8xC	a
atomů	atom	k1gInPc2	atom
jsou	být	k5eAaImIp3nP	být
specifické	specifický	k2eAgInPc1d1	specifický
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
specie	specie	k1gFnSc2	specie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
hlediskem	hledisko	k1gNnSc7	hledisko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použitá	použitý	k2eAgFnSc1d1	použitá
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
typ	typ	k1gInSc1	typ
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
<g/>
Se	s	k7c7	s
zkracující	zkracující	k2eAgFnSc7d1	zkracující
se	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jeho	jeho	k3xOp3gFnSc2	jeho
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
také	také	k9	také
účinek	účinek	k1gInSc1	účinek
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
atomy	atom	k1gInPc4	atom
<g/>
,	,	kIx,	,
či	či	k8xC	či
molekuly	molekula	k1gFnPc1	molekula
je	být	k5eAaImIp3nS	být
znatelnější	znatelný	k2eAgNnSc1d2	znatelnější
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
radiové	radiový	k2eAgFnSc2d1	radiová
vlny	vlna	k1gFnSc2	vlna
svým	svůj	k3xOyFgInSc7	svůj
dopadem	dopad	k1gInSc7	dopad
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
jen	jen	k9	jen
orientaci	orientace	k1gFnSc4	orientace
jaderného	jaderný	k2eAgInSc2d1	jaderný
spinu	spin	k1gInSc2	spin
a	a	k8xC	a
účinek	účinek	k1gInSc1	účinek
na	na	k7c4	na
molekulu	molekula	k1gFnSc4	molekula
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
zničit	zničit	k5eAaPmF	zničit
i	i	k9	i
atomová	atomový	k2eAgNnPc4d1	atomové
jádra	jádro	k1gNnPc4	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
a	a	k8xC	a
forenzní	forenzní	k2eAgFnSc1d1	forenzní
chemie	chemie	k1gFnSc1	chemie
využívají	využívat	k5eAaImIp3nP	využívat
RTG	RTG	kA	RTG
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
UV	UV	kA	UV
<g/>
/	/	kIx~	/
<g/>
VIS	vis	k1gInSc1	vis
nebo	nebo	k8xC	nebo
IR	Ir	k1gMnSc1	Ir
spektroskopii	spektroskopie	k1gFnSc4	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Fluorescence	fluorescence	k1gFnSc1	fluorescence
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
k	k	k7c3	k
zobrazování	zobrazování	k1gNnSc3	zobrazování
otisků	otisk	k1gInPc2	otisk
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
DNA	DNA	kA	DNA
značení	značení	k1gNnSc4	značení
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
–	–	k?	–
měření	měření	k1gNnSc1	měření
intenzity	intenzita	k1gFnSc2	intenzita
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
resp.	resp.	kA	resp.
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
spadá	spadat	k5eAaPmIp3nS	spadat
řada	řada	k1gFnSc1	řada
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
měřicích	měřicí	k2eAgFnPc2d1	měřicí
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
<g/>
Ramanova	Ramanův	k2eAgFnSc1d1	Ramanova
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
spektra	spektrum	k1gNnSc2	spektrum
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
díky	díky	k7c3	díky
Ramanově	Ramanův	k2eAgInSc6d1	Ramanův
jevu	jev	k1gInSc6	jev
(	(	kIx(	(
<g/>
neelastický	elastický	k2eNgInSc4d1	neelastický
rozptyl	rozptyl	k1gInSc4	rozptyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozptýlené	rozptýlený	k2eAgNnSc1d1	rozptýlené
záření	záření	k1gNnSc1	záření
má	mít	k5eAaImIp3nS	mít
mírně	mírně	k6eAd1	mírně
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
od	od	k7c2	od
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
kvůli	kvůli	k7c3	kvůli
předání	předání	k1gNnSc3	předání
části	část	k1gFnSc2	část
energie	energie	k1gFnSc2	energie
na	na	k7c6	na
vibračních	vibrační	k2eAgInPc6d1	vibrační
přechodech	přechod	k1gInPc6	přechod
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spektroskopická	spektroskopický	k2eAgFnSc1d1	spektroskopická
technika	technika	k1gFnSc1	technika
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
a	a	k8xC	a
prostorovém	prostorový	k2eAgNnSc6d1	prostorové
uspořádání	uspořádání	k1gNnSc6	uspořádání
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spektrometrie	spektrometrie	k1gFnSc1	spektrometrie
s	s	k7c7	s
Fourierovou	Fourierův	k2eAgFnSc7d1	Fourierova
transformací	transformace	k1gFnSc7	transformace
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
matematické	matematický	k2eAgFnSc6d1	matematická
transformaci	transformace	k1gFnSc6	transformace
interferogramu	interferogram	k1gInSc2	interferogram
(	(	kIx(	(
<g/>
závislosti	závislost	k1gFnSc2	závislost
intenzity	intenzita	k1gFnSc2	intenzita
signálu	signál	k1gInSc2	signál
na	na	k7c6	na
dráhovém	dráhový	k2eAgInSc6d1	dráhový
rozdílu	rozdíl	k1gInSc6	rozdíl
paprsků	paprsek	k1gInPc2	paprsek
<g/>
)	)	kIx)	)
získaného	získaný	k2eAgMnSc2d1	získaný
detekcí	detekce	k1gFnSc7	detekce
signálu	signál	k1gInSc2	signál
vystupujícího	vystupující	k2eAgInSc2d1	vystupující
z	z	k7c2	z
interferometru	interferometr	k1gInSc2	interferometr
<g/>
.	.	kIx.	.
</s>
<s>
Interferující	interferující	k2eAgInPc1d1	interferující
paprsky	paprsek	k1gInPc1	paprsek
putují	putovat	k5eAaImIp3nP	putovat
přes	přes	k7c4	přes
kyvetu	kyveta	k1gFnSc4	kyveta
se	s	k7c7	s
vzorkem	vzorek	k1gInSc7	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Spektrometry	spektrometr	k1gInPc1	spektrometr
Fourierova	Fourierův	k2eAgInSc2d1	Fourierův
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
poměrně	poměrně	k6eAd1	poměrně
hodně	hodně	k6eAd1	hodně
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Infračervená	infračervený	k2eAgFnSc1d1	infračervená
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
(	(	kIx(	(
<g/>
Zeslabený	zeslabený	k2eAgInSc1d1	zeslabený
úplný	úplný	k2eAgInSc1d1	úplný
odraz	odraz	k1gInSc1	odraz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
UV	UV	kA	UV
<g/>
/	/	kIx~	/
<g/>
VIS	vis	k1gInSc1	vis
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
</s>
</p>
<p>
<s>
Atomová	atomový	k2eAgFnSc1d1	atomová
absorpční	absorpční	k2eAgFnSc1d1	absorpční
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
</s>
</p>
<p>
<s>
Rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
fluorescence	fluorescence	k1gFnSc1	fluorescence
</s>
</p>
<p>
<s>
NMR	NMR	kA	NMR
spektroskopie	spektroskopie	k1gFnPc4	spektroskopie
využívající	využívající	k2eAgFnSc2d1	využívající
jaderné	jaderný	k2eAgFnSc2d1	jaderná
magnetické	magnetický	k2eAgFnSc2d1	magnetická
rezonance	rezonance	k1gFnSc2	rezonance
–	–	k?	–
určuje	určovat	k5eAaImIp3nS	určovat
rozložení	rozložení	k1gNnSc1	rozložení
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jader	jádro	k1gNnPc2	jádro
s	s	k7c7	s
nenulovým	nulový	k2eNgInSc7d1	nenulový
jaderným	jaderný	k2eAgInSc7d1	jaderný
spinem	spin	k1gInSc7	spin
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
využívající	využívající	k2eAgFnSc2d1	využívající
elektronové	elektronový	k2eAgFnSc2d1	elektronová
paramagnetické	paramagnetický	k2eAgFnSc2d1	paramagnetická
rezonance	rezonance	k1gFnSc2	rezonance
–	–	k?	–
měření	měření	k1gNnSc2	měření
částic	částice	k1gFnPc2	částice
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
nepárové	párový	k2eNgInPc4d1	nepárový
elektrony	elektron	k1gInPc4	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Prosser	Prosser	k1gMnSc1	Prosser
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
Experimentální	experimentální	k2eAgFnPc1d1	experimentální
metody	metoda	k1gFnPc1	metoda
biofyziky	biofyzika	k1gFnSc2	biofyzika
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
59	[number]	k4	59
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
o	o	k7c4	o
detektivní	detektivní	k2eAgFnSc4d1	detektivní
chemii	chemie	k1gFnSc4	chemie
v	v	k7c6	v
brožuře	brožura	k1gFnSc6	brožura
KSICHTu	KSICHTu	k?	KSICHTu
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fluorescenční	fluorescenční	k2eAgFnSc1d1	fluorescenční
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
</s>
</p>
<p>
<s>
Řešené	řešený	k2eAgFnPc1d1	řešená
úlohy	úloha	k1gFnPc1	úloha
ze	z	k7c2	z
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
nukleární	nukleární	k2eAgFnSc2d1	nukleární
magnetické	magnetický	k2eAgFnSc2d1	magnetická
resonance	resonance	k1gFnSc2	resonance
</s>
</p>
<p>
<s>
EPR	EPR	kA	EPR
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
</s>
</p>
<p>
<s>
Spektrometrie	spektrometrie	k1gFnSc1	spektrometrie
s	s	k7c7	s
Fourierovou	Fourierův	k2eAgFnSc7d1	Fourierova
transformací	transformace	k1gFnSc7	transformace
</s>
</p>
