<s>
Sjednocené	sjednocený	k2eAgFnPc4d1
provincie	provincie	k1gFnPc1
La	la	k1gNnSc2
Platy	Plata	k1gFnSc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Provincias	Provincias	k1gMnSc1
Unidas	Unidas	k1gMnSc1
del	del	k?
Río	Río	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Plata	Plata	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
název	název	k1gInSc1
pro	pro	k7c4
území	území	k1gNnSc4
dnešních	dnešní	k2eAgInPc2d1
států	stát	k1gInPc2
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
Uruguay	Uruguay	k1gFnSc1
a	a	k8xC
bolivijského	bolivijský	k2eAgInSc2d1
departementu	departement	k1gInSc2
Tarija	Tarija	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
Květnové	květnový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
při	při	k7c6
probíhajících	probíhající	k2eAgFnPc6d1
hispanoamerických	hispanoamerický	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
formování	formování	k1gNnSc4
nových	nový	k2eAgInPc2d1
států	stát	k1gInPc2
na	na	k7c6
jihoamerickém	jihoamerický	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>