<s>
Sjednocené	sjednocený	k2eAgFnPc4d1
provincie	provincie	k1gFnPc4
La	la	k1gNnSc2
Platy	plat	k1gInPc1
</s>
<s>
Znak	znak	k1gInSc1
Sjednocených	sjednocený	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
La	la	k1gNnSc2
Platy	plat	k1gInPc1
</s>
<s>
Sjednocené	sjednocený	k2eAgFnPc4d1
provincie	provincie	k1gFnPc1
La	la	k1gNnSc2
Platy	Plata	k1gFnSc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Provincias	Provincias	k1gMnSc1
Unidas	Unidas	k1gMnSc1
del	del	k?
Río	Río	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Plata	Plata	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
název	název	k1gInSc1
pro	pro	k7c4
území	území	k1gNnSc4
dnešních	dnešní	k2eAgInPc2d1
států	stát	k1gInPc2
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
Uruguay	Uruguay	k1gFnSc1
a	a	k8xC
bolivijského	bolivijský	k2eAgInSc2d1
departementu	departement	k1gInSc2
Tarija	Tarija	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
Květnové	květnový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
při	při	k7c6
probíhajících	probíhající	k2eAgFnPc6d1
hispanoamerických	hispanoamerický	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
formování	formování	k1gNnSc4
nových	nový	k2eAgInPc2d1
států	stát	k1gInPc2
na	na	k7c6
jihoamerickém	jihoamerický	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
název	název	k1gInSc1
nahrazen	nahrazen	k2eAgInSc1d1
názvem	název	k1gInSc7
Argentinská	argentinský	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
vztahoval	vztahovat	k5eAaImAgInS
se	se	k3xPyFc4
již	již	k9
pouze	pouze	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Argentiny	Argentina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
se	se	k3xPyFc4
používalo	používat	k5eAaImAgNnS
do	do	k7c2
roku	rok	k1gInSc2
1861	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nese	nést	k5eAaImIp3nS
tento	tento	k3xDgInSc1
stát	stát	k1gInSc1
název	název	k1gInSc4
Argentinská	argentinský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
nebo	nebo	k8xC
zkráceně	zkráceně	k6eAd1
Argentina	Argentina	k1gFnSc1
(	(	kIx(
<g/>
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Zjednotené	Zjednotený	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
La	la	k1gNnPc2
Platy	plat	k1gInPc7
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7730649-1	7730649-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88244682	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
147574191	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88244682	#num#	k4
</s>
