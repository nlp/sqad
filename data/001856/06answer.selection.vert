<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Mount	Mount	k1gInSc4	Mount
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
ve	v	k7c4	v
Shrewsbury	Shrewsbura	k1gFnPc4	Shrewsbura
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc4	hrabství
Shropshire	Shropshir	k1gMnSc5	Shropshir
<g/>
,	,	kIx,	,
v	v	k7c4	v
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1809	[number]	k4	1809
jako	jako	k8xC	jako
pátý	pátý	k4xOgMnSc1	pátý
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
dětí	dítě	k1gFnPc2	dítě
bohatého	bohatý	k2eAgMnSc2d1	bohatý
lékaře	lékař	k1gMnSc2	lékař
Roberta	Robert	k1gMnSc2	Robert
Darwina	Darwin	k1gMnSc2	Darwin
a	a	k8xC	a
Susannahy	Susannaha	k1gFnSc2	Susannaha
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgNnSc1d1	rozené
Wedgwoodové	Wedgwoodový	k2eAgNnSc1d1	Wedgwoodový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
