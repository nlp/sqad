<p>
<s>
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
(	(	kIx(	(
<g/>
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teologicky	teologicky	k6eAd1	teologicky
nejzávažnější	závažný	k2eAgInSc1d3	nejzávažnější
spis	spis	k1gInSc1	spis
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
sepsán	sepsat	k5eAaPmNgInS	sepsat
latinsky	latinsky	k6eAd1	latinsky
roku	rok	k1gInSc2	rok
1413	[number]	k4	1413
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hus	Hus	k1gMnSc1	Hus
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
silně	silně	k6eAd1	silně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
oxfordského	oxfordský	k2eAgMnSc4d1	oxfordský
reformátora	reformátor	k1gMnSc4	reformátor
Jana	Jan	k1gMnSc4	Jan
Viklefa	Viklef	k1gMnSc4	Viklef
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
hlava	hlava	k1gFnSc1	hlava
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
žít	žít	k5eAaImF	žít
v	v	k7c6	v
hříchu	hřích	k1gInSc6	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Upírá	upírat	k5eAaImIp3nS	upírat
papežům	papež	k1gMnPc3	papež
jejich	jejich	k3xOp3gInPc7	jejich
nárok	nárok	k1gInSc4	nárok
<g/>
,	,	kIx,	,
že	že	k8xS	že
oni	onen	k3xDgMnPc1	onen
jsou	být	k5eAaImIp3nP	být
tou	ten	k3xDgFnSc7	ten
skálou	skála	k1gFnSc7	skála
nebo	nebo	k8xC	nebo
základním	základní	k2eAgMnSc7d1	základní
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
celá	celý	k2eAgFnSc1d1	celá
církev	církev	k1gFnSc1	církev
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
narušení	narušení	k1gNnSc4	narušení
autoritativního	autoritativní	k2eAgInSc2d1	autoritativní
systému	systém	k1gInSc2	systém
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
kardinálové	kardinál	k1gMnPc1	kardinál
vytýkali	vytýkat	k5eAaImAgMnP	vytýkat
především	především	k6eAd1	především
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
zde	zde	k6eAd1	zde
kritiku	kritika	k1gFnSc4	kritika
papežství	papežství	k1gNnSc2	papežství
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
byli	být	k5eAaImAgMnP	být
tři	tři	k4xCgMnPc1	tři
papežové	papež	k1gMnPc1	papež
<g/>
,	,	kIx,	,
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
Řehoř	Řehoř	k1gMnSc1	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
koncil	koncil	k1gInSc4	koncil
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Alexandra	Alexandr	k1gMnSc2	Alexandr
V.	V.	kA	V.
<g/>
Hus	Hus	k1gMnSc1	Hus
nevidí	vidět	k5eNaImIp3nS	vidět
hodnoty	hodnota	k1gFnPc4	hodnota
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
společenském	společenský	k2eAgNnSc6d1	společenské
postavení	postavení	k1gNnSc6	postavení
a	a	k8xC	a
úřadě	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
zbožném	zbožný	k2eAgInSc6d1	zbožný
a	a	k8xC	a
mravném	mravný	k2eAgInSc6d1	mravný
<g/>
,	,	kIx,	,
či	či	k8xC	či
nezbožném	zbožný	k2eNgMnSc6d1	nezbožný
a	a	k8xC	a
nemravném	mravný	k2eNgInSc6d1	nemravný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kritika	kritika	k1gFnSc1	kritika
hluboce	hluboko	k6eAd1	hluboko
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
papežství	papežství	k1gNnSc4	papežství
a	a	k8xC	a
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
schválením	schválení	k1gNnSc7	schválení
přijímání	přijímání	k1gNnSc2	přijímání
pod	pod	k7c7	pod
obojí	oboj	k1gFnSc7	oboj
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc1	základ
nové	nový	k2eAgFnSc2d1	nová
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc6	část
spisu	spis	k1gInSc2	spis
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nejznámější	známý	k2eAgInSc1d3	nejznámější
traktát	traktát	k1gInSc1	traktát
z	z	k7c2	z
Husova	Husův	k2eAgNnSc2d1	Husovo
díla	dílo	k1gNnSc2	dílo
lze	lze	k6eAd1	lze
rozvrhnout	rozvrhnout	k5eAaPmF	rozvrhnout
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
autor	autor	k1gMnSc1	autor
soustavně	soustavně	k6eAd1	soustavně
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeho	jeho	k3xOp3gNnSc2	jeho
polemicky	polemicky	k6eAd1	polemicky
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
vyhrocenou	vyhrocený	k2eAgFnSc4d1	vyhrocená
odpověď	odpověď	k1gFnSc4	odpověď
profesorům	profesor	k1gMnPc3	profesor
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
teologické	teologický	k2eAgFnSc6d1	teologická
fakultě	fakulta	k1gFnSc6	fakulta
a	a	k8xC	a
útoky	útok	k1gInPc7	útok
na	na	k7c4	na
zásady	zásada	k1gFnPc4	zásada
buly	buly	k1gNnSc2	buly
Bonifáce	Bonifác	k1gMnSc2	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Unam	Unam	k6eAd1	Unam
sanctam	sanctam	k6eAd1	sanctam
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zařazeny	zařadit	k5eAaPmNgInP	zařadit
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
papeži	papež	k1gMnSc3	papež
Alexandru	Alexandr	k1gMnSc3	Alexandr
V.	V.	kA	V.
<g/>
,	,	kIx,	,
křížové	křížový	k2eAgFnSc3d1	křížová
výpravě	výprava	k1gFnSc3	výprava
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
proti	proti	k7c3	proti
předvolání	předvolání	k1gNnSc3	předvolání
před	před	k7c4	před
papežskou	papežský	k2eAgFnSc4d1	Papežská
kurii	kurie	k1gFnSc4	kurie
pro	pro	k7c4	pro
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
kacířství	kacířství	k1gNnSc2	kacířství
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
Hus	Hus	k1gMnSc1	Hus
převzal	převzít	k5eAaPmAgMnS	převzít
Viklefovo	Viklefův	k2eAgNnSc4d1	Viklefovo
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
je	být	k5eAaImIp3nS	být
sbor	sbor	k1gInSc4	sbor
vyvolených	vyvolená	k1gFnPc2	vyvolená
ke	k	k7c3	k
spasení	spasení	k1gNnSc3	spasení
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
organizace	organizace	k1gFnSc1	organizace
vedená	vedený	k2eAgFnSc1d1	vedená
a	a	k8xC	a
řízená	řízený	k2eAgFnSc1d1	řízená
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
sám	sám	k3xTgMnSc1	sám
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
vyvodil	vyvodit	k5eAaBmAgMnS	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
i	i	k9	i
po	po	k7c6	po
exkomunikaci	exkomunikace	k1gFnSc6	exkomunikace
papežem	papež	k1gMnSc7	papež
může	moct	k5eAaImIp3nS	moct
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
sám	sám	k3xTgMnSc1	sám
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
papež	papež	k1gMnSc1	papež
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
jiný	jiný	k2eAgMnSc1d1	jiný
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
<g/>
-li	i	k?	-li
ve	v	k7c6	v
hříchu	hřích	k1gInSc6	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
autoritářství	autoritářství	k1gNnSc4	autoritářství
a	a	k8xC	a
namísto	namísto	k7c2	namísto
důstojnosti	důstojnost	k1gFnSc2	důstojnost
plynoucí	plynoucí	k2eAgMnSc1d1	plynoucí
z	z	k7c2	z
postavení	postavení	k1gNnSc2	postavení
položil	položit	k5eAaPmAgMnS	položit
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
měřítko	měřítko	k1gNnSc4	měřítko
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
jejich	jejich	k3xOp3gInSc4	jejich
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
Hus	Hus	k1gMnSc1	Hus
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
spisu	spis	k1gInSc6	spis
také	také	k9	také
učil	učit	k5eAaImAgMnS	učit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
boží	boží	k2eAgInSc1d1	boží
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
regulativní	regulativní	k2eAgInSc1d1	regulativní
princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
přístupný	přístupný	k2eAgInSc1d1	přístupný
lidskému	lidský	k2eAgInSc3d1	lidský
rozumu	rozum	k1gInSc3	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Rozum	rozum	k1gInSc4	rozum
každého	každý	k3xTgMnSc2	každý
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
člověka	člověk	k1gMnSc2	člověk
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
"	"	kIx"	"
<g/>
boží	boží	k2eAgInSc1d1	boží
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
boží	boží	k2eAgFnSc1d1	boží
pravda	pravda	k1gFnSc1	pravda
<g/>
"	"	kIx"	"
správně	správně	k6eAd1	správně
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
uplatňovány	uplatňován	k2eAgInPc1d1	uplatňován
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
poddaný	poddaný	k2eAgMnSc1d1	poddaný
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
posoudit	posoudit	k5eAaPmF	posoudit
svým	svůj	k3xOyFgInSc7	svůj
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jeho	jeho	k3xOp3gMnSc1	jeho
představený	představený	k1gMnSc1	představený
jedná	jednat	k5eAaImIp3nS	jednat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
pravdou	pravda	k1gFnSc7	pravda
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
<g/>
-li	i	k?	-li
jeho	jeho	k3xOp3gInPc4	jeho
rozkazy	rozkaz	k1gInPc4	rozkaz
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
závaznost	závaznost	k1gFnSc1	závaznost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázka	ukázka	k1gFnSc1	ukázka
ze	z	k7c2	z
spisu	spis	k1gInSc2	spis
==	==	k?	==
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
kapitoly	kapitola	k1gFnPc1	kapitola
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc4	vydání
latinského	latinský	k2eAgInSc2d1	latinský
originálu	originál	k1gInSc2	originál
==	==	k?	==
</s>
</p>
<p>
<s>
HUS	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
THOMSON	THOMSON	kA	THOMSON
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Tractatus	Tractatus	k1gInSc1	Tractatus
de	de	k?	de
ecclesia	ecclesia	k1gFnSc1	ecclesia
e	e	k0	e
fontibus	fontibus	k1gInSc4	fontibus
manu	mana	k1gFnSc4	mana
scriptis	scriptis	k1gFnSc2	scriptis
in	in	k?	in
lucem	lucem	k1gInSc1	lucem
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Heffer	Heffer	k1gInSc1	Heffer
&	&	k?	&
Sons	Sons	k1gInSc1	Sons
Ltd	ltd	kA	ltd
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
xxxiv	xxxiv	k6eAd1	xxxiv
<g/>
251	[number]	k4	251
s.	s.	k?	s.
</s>
</p>
<p>
<s>
HUS	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
THOMSON	THOMSON	kA	THOMSON
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Tractatus	Tractatus	k1gInSc1	Tractatus
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Komenského	Komenského	k2eAgFnSc1d1	Komenského
evangelická	evangelický	k2eAgFnSc1d1	evangelická
fakulta	fakulta	k1gFnSc1	fakulta
bohoslovecká	bohoslovecký	k2eAgFnSc1d1	bohoslovecká
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
251	[number]	k4	251
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
182847	[number]	k4	182847
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
HUS	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husi	Hus	k1gFnSc2	Hus
sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
I.	I.	kA	I.
Řada	řada	k1gFnSc1	řada
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
latinské	latinský	k2eAgInPc1d1	latinský
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
I.	I.	kA	I.
418	[number]	k4	418
s.	s.	k?	s.
Překlad	překlad	k1gInSc1	překlad
Milan	Milan	k1gMnSc1	Milan
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Nákladem	náklad	k1gInSc7	náklad
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Bursíka	Bursík	k1gMnSc2	Bursík
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
2631371	[number]	k4	2631371
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
traktátu	traktát	k1gInSc2	traktát
De	De	k?	De
ecclesia	ecclesius	k1gMnSc2	ecclesius
(	(	kIx(	(
<g/>
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
263	[number]	k4	263
<g/>
–	–	k?	–
<g/>
413	[number]	k4	413
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
HUS	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Mrázek	Mrázek	k1gMnSc1	Mrázek
Dobiáš	Dobiáš	k1gMnSc1	Dobiáš
a	a	k8xC	a
Amedeo	Amedeo	k1gMnSc1	Amedeo
Molnár	Molnár	k1gMnSc1	Molnár
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
311	[number]	k4	311
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
481988	[number]	k4	481988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cizojazyčné	cizojazyčný	k2eAgInPc1d1	cizojazyčný
překlady	překlad	k1gInPc1	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
HUS	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
SANTINI	SANTINI	kA	SANTINI
<g/>
,	,	kIx,	,
Luigi	Luigi	k1gNnSc2	Luigi
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Il	Il	k?	Il
primato	primato	k6eAd1	primato
di	di	k?	di
Pietro	Pietro	k1gNnSc4	Pietro
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
dal	dát	k5eAaPmAgInS	dát
"	"	kIx"	"
<g/>
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
a	a	k8xC	a
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Torino	Torino	k1gNnSc1	Torino
<g/>
:	:	kIx,	:
Claudiana	Claudiana	k1gFnSc1	Claudiana
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
102	[number]	k4	102
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
Piccola	Piccola	k1gFnSc1	Piccola
collana	collana	k1gFnSc1	collana
moderna	moderna	k1gFnSc1	moderna
<g/>
.	.	kIx.	.
</s>
<s>
Serie	serie	k1gFnSc1	serie
storica	storica	k1gFnSc1	storica
<g/>
;	;	kIx,	;
126	[number]	k4	126
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
7016	[number]	k4	7016
<g/>
-	-	kIx~	-
<g/>
758	[number]	k4	758
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
HUS	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
kościele	kościela	k1gFnSc6	kościela
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Lublin	Lublin	k1gInSc1	Lublin
<g/>
:	:	kIx,	:
Towarzystwo	Towarzystwo	k1gNnSc1	Towarzystwo
Naukowe	Naukow	k1gInSc2	Naukow
Katolickiego	Katolickiego	k6eAd1	Katolickiego
Uniwersytetu	Uniwersytet	k1gInSc2	Uniwersytet
Lubelskiego	Lubelskiego	k1gMnSc1	Lubelskiego
Jana	Jan	k1gMnSc2	Jan
Pawła	Pawłum	k1gNnSc2	Pawłum
II	II	kA	II
<g/>
,	,	kIx,	,
©	©	k?	©
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
265	[number]	k4	265
s.	s.	k?	s.
Źródła	Źródł	k1gInSc2	Źródł
i	i	k8xC	i
monografie	monografie	k1gFnSc2	monografie
/	/	kIx~	/
Towarzystwo	Towarzystwo	k6eAd1	Towarzystwo
Naukowe	Naukowe	k1gFnSc1	Naukowe
Katolickiego	Katolickiego	k1gMnSc1	Katolickiego
Uniwersytetu	Uniwersytet	k1gInSc2	Uniwersytet
Lubelskiego	Lubelskiego	k1gMnSc1	Lubelskiego
Jana	Jan	k1gMnSc2	Jan
Pawła	Pawłum	k1gNnSc2	Pawłum
II	II	kA	II
<g/>
;	;	kIx,	;
318	[number]	k4	318
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7306	[number]	k4	7306
<g/>
-	-	kIx~	-
<g/>
365	[number]	k4	365
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
HUS	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Church	Church	k1gMnSc1	Church
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Westport	Westport	k1gInSc1	Westport
<g/>
:	:	kIx,	:
Greenwood	Greenwood	k1gInSc1	Greenwood
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
46	[number]	k4	46
<g/>
,	,	kIx,	,
304	[number]	k4	304
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8371	[number]	k4	8371
<g/>
-	-	kIx~	-
<g/>
4242	[number]	k4	4242
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BARTOŠ	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Husova	Husův	k2eAgNnSc2d1	Husovo
díla	dílo	k1gNnSc2	dílo
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
HREJSA	HREJSA	kA	HREJSA
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc4	sborník
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
desetiletí	desetiletí	k1gNnPc4	desetiletí
Husovy	Husův	k2eAgFnSc2d1	Husova
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
:	:	kIx,	:
1919	[number]	k4	1919
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
<g/>
/	/	kIx~	/
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
nákladem	náklad	k1gInSc7	náklad
státním	státní	k2eAgInSc7d1	státní
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
268	[number]	k4	268
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1087409	[number]	k4	1087409
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HLAVÁČEK	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Husův	Husův	k2eAgInSc4d1	Husův
traktát	traktát	k1gInSc4	traktát
De	De	k?	De
ecclesia	ecclesius	k1gMnSc2	ecclesius
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
dochování	dochování	k1gNnPc2	dochování
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
osudů	osud	k1gInPc2	osud
rukopisů	rukopis	k1gInPc2	rukopis
Husových	Husových	k2eAgNnPc2d1	Husových
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
SMRČKA	Smrčka	k1gMnSc1	Smrčka
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
a	a	k8xC	a
VYBÍRAL	Vybíral	k1gMnSc1	Vybíral
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
1415	[number]	k4	1415
a	a	k8xC	a
600	[number]	k4	600
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
:	:	kIx,	:
VII	VII	kA	VII
<g/>
.	.	kIx.	.
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
husitologické	husitologický	k2eAgNnSc1d1	husitologický
sympozium	sympozium	k1gNnSc1	sympozium
<g/>
:	:	kIx,	:
Tábor	Tábor	k1gInSc1	Tábor
23	[number]	k4	23
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
:	:	kIx,	:
Husitské	husitský	k2eAgNnSc1d1	husitské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
440	[number]	k4	440
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Husitský	husitský	k2eAgInSc1d1	husitský
Tábor	Tábor	k1gInSc1	Tábor
<g/>
;	;	kIx,	;
supplementum	supplementum	k1gNnSc1	supplementum
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87516	[number]	k4	87516
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Stať	stať	k1gFnSc1	stať
Ivana	Ivan	k1gMnSc2	Ivan
Hlaváčka	Hlaváček	k1gMnSc2	Hlaváček
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
213	[number]	k4	213
<g/>
–	–	k?	–
<g/>
233	[number]	k4	233
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
