<p>
<s>
Socha	socha	k1gFnSc1	socha
Krista	Kristus	k1gMnSc2	Kristus
Krále	Král	k1gMnSc2	Král
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
socha	socha	k1gFnSc1	socha
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
sochy	socha	k1gFnSc2	socha
započítává	započítávat	k5eAaImIp3nS	započítávat
i	i	k9	i
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Świebodzin	Świebodzina	k1gFnPc2	Świebodzina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sochu	socha	k1gFnSc4	socha
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
objednala	objednat	k5eAaPmAgFnS	objednat
diecéze	diecéze	k1gFnSc1	diecéze
zelenohorsko-gorzowská	zelenohorskoorzowská	k1gFnSc1	zelenohorsko-gorzowská
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
52	[number]	k4	52
m	m	kA	m
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
440	[number]	k4	440
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
byla	být	k5eAaImAgFnS	být
odhalena	odhalen	k2eAgFnSc1d1	odhalena
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
je	být	k5eAaImIp3nS	být
postavený	postavený	k2eAgInSc1d1	postavený
na	na	k7c6	na
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Sulechowskiej	Sulechowskiej	k1gInSc1	Sulechowskiej
v	v	k7c6	v
Świebodzinĕ	Świebodzinĕ	k1gFnSc6	Świebodzinĕ
na	na	k7c4	na
16	[number]	k4	16
<g/>
m	m	kA	m
vysokém	vysoký	k2eAgNnSc6d1	vysoké
návrší	návrší	k1gNnSc6	návrší
z	z	k7c2	z
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
suti	suť	k1gFnSc2	suť
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
36	[number]	k4	36
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
z	z	k7c2	z
vláknobetonu	vláknobeton	k1gInSc2	vláknobeton
vyztuženého	vyztužený	k2eAgInSc2d1	vyztužený
ocelovou	ocelový	k2eAgFnSc4d1	ocelová
výztuží	výztuží	k1gNnSc4	výztuží
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
pozlacená	pozlacený	k2eAgFnSc1d1	pozlacená
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
3,5	[number]	k4	3,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
metry	metr	k1gMnPc7	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
Krista	Kristus	k1gMnSc2	Kristus
je	být	k5eAaImIp3nS	být
4,5	[number]	k4	4,5
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
15	[number]	k4	15
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ruka	ruka	k1gFnSc1	ruka
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
konci	konec	k1gInSc6	konec
prstů	prst	k1gInPc2	prst
činí	činit	k5eAaImIp3nS	činit
24	[number]	k4	24
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
sochař	sochař	k1gMnSc1	sochař
Mirosław	Mirosław	k1gMnSc1	Mirosław
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
Patecki	Patecki	k1gNnPc2	Patecki
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
Královny	královna	k1gFnSc2	královna
Polska	Polsko	k1gNnSc2	Polsko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Socha	socha	k1gFnSc1	socha
Krista	Kristus	k1gMnSc2	Kristus
Krále	Král	k1gMnSc2	Král
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pomnik	Pomnik	k1gInSc1	Pomnik
Chrystusa	Chrystus	k1gMnSc2	Chrystus
Króla	Król	k1gMnSc2	Król
–	–	k?	–
oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
</s>
</p>
