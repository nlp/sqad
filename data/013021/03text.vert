<p>
<s>
The	The	k?	The
Bends	Bends	k1gInSc4	Bends
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
Radiohead	Radiohead	k1gInSc1	Radiohead
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Texty	text	k1gInPc1	text
písní	píseň	k1gFnPc2	píseň
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Bends	Bendsa	k1gFnPc2	Bendsa
</s>
</p>
