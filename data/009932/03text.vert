<p>
<s>
Metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
chemin	chemin	k1gInSc1	chemin
de	de	k?	de
fer	fer	k?	fer
métropolitain	métropolitain	k1gInSc1	métropolitain
-	-	kIx~	-
železnice	železnice	k1gFnSc1	železnice
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
podzemní	podzemní	k2eAgFnSc1d1	podzemní
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
provozované	provozovaný	k2eAgFnSc6d1	provozovaná
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
celá	celý	k2eAgFnSc1d1	celá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
podzemními	podzemní	k2eAgInPc7d1	podzemní
tunely	tunel	k1gInPc7	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
zcela	zcela	k6eAd1	zcela
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
ostatních	ostatní	k2eAgInPc6d1	ostatní
druzích	druh	k1gInPc6	druh
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Slovem	slovem	k6eAd1	slovem
"	"	kIx"	"
<g/>
metro	metro	k1gNnSc1	metro
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
nejen	nejen	k6eAd1	nejen
samotná	samotný	k2eAgFnSc1d1	samotná
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
stavby	stavba	k1gFnPc1	stavba
a	a	k8xC	a
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vlak	vlak	k1gInSc1	vlak
užívaný	užívaný	k2eAgInSc1d1	užívaný
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
metro	metro	k1gNnSc1	metro
přijelo	přijet	k5eAaPmAgNnS	přijet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povrchové	povrchový	k2eAgFnSc2d1	povrchová
městské	městský	k2eAgFnSc2d1	městská
a	a	k8xC	a
příměstské	příměstský	k2eAgFnSc2d1	příměstská
železnice	železnice	k1gFnSc2	železnice
(	(	kIx(	(
<g/>
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
za	za	k7c4	za
metro	metro	k1gNnSc4	metro
nepovažují	považovat	k5eNaImIp3nP	považovat
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
jinými	jiné	k1gNnPc7	jiné
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
RER	RER	kA	RER
(	(	kIx(	(
<g/>
Réseau	Réseaus	k1gInSc2	Réseaus
Express	express	k1gInSc1	express
Régional	Régional	k1gFnSc1	Régional
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
S-Bahn	S-Bahna	k1gFnPc2	S-Bahna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vozy	vůz	k1gInPc1	vůz
metra	metro	k1gNnSc2	metro
jsou	být	k5eAaImIp3nP	být
poháněny	pohánět	k5eAaImNgInP	pohánět
elektřinou	elektřina	k1gFnSc7	elektřina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
pokusy	pokus	k1gInPc1	pokus
i	i	k8xC	i
s	s	k7c7	s
pneumatickým	pneumatický	k2eAgInSc7d1	pneumatický
<g/>
,	,	kIx,	,
parním	parní	k2eAgInSc7d1	parní
či	či	k8xC	či
animálním	animální	k2eAgInSc7d1	animální
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tramvají	tramvaj	k1gFnPc2	tramvaj
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
metra	metro	k1gNnSc2	metro
obvykle	obvykle	k6eAd1	obvykle
nepoužívá	používat	k5eNaImIp3nS	používat
k	k	k7c3	k
odběru	odběr	k1gInSc3	odběr
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
vrchní	vrchní	k2eAgMnSc1d1	vrchní
sběrač	sběrač	k1gMnSc1	sběrač
(	(	kIx(	(
<g/>
pantograf	pantograf	k1gMnSc1	pantograf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
odebírán	odebírat	k5eAaImNgInS	odebírat
z	z	k7c2	z
boční	boční	k2eAgFnSc2d1	boční
napájecí	napájecí	k2eAgFnSc2d1	napájecí
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
snížení	snížení	k1gNnSc2	snížení
výšky	výška	k1gFnSc2	výška
tunelu	tunel	k1gInSc2	tunel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
v	v	k7c6	v
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
vozy	vůz	k1gInPc7	vůz
soupravy	souprava	k1gFnSc2	souprava
umístěno	umístit	k5eAaPmNgNnS	umístit
trolejové	trolejový	k2eAgNnSc1d1	trolejové
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
odolnosti	odolnost	k1gFnSc2	odolnost
proti	proti	k7c3	proti
poruchám	porucha	k1gFnPc3	porucha
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc4d2	delší
životnost	životnost	k1gFnSc4	životnost
napájecí	napájecí	k2eAgFnSc2d1	napájecí
kolejnice	kolejnice	k1gFnSc2	kolejnice
proti	proti	k7c3	proti
troleji	trolej	k1gFnSc3	trolej
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
kolejnice	kolejnice	k1gFnSc1	kolejnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
kolejiště	kolejiště	k1gNnSc2	kolejiště
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
i	i	k8xC	i
úseky	úsek	k1gInPc1	úsek
metra	metro	k1gNnSc2	metro
vedené	vedený	k2eAgFnPc1d1	vedená
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
odděleny	oddělit	k5eAaPmNgInP	oddělit
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
zábranami	zábrana	k1gFnPc7	zábrana
znemožňujícími	znemožňující	k2eAgInPc7d1	znemožňující
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
kolejiště	kolejiště	k1gNnSc2	kolejiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
drahách	draha	k1gFnPc6	draha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
řadí	řadit	k5eAaImIp3nS	řadit
metro	metro	k1gNnSc4	metro
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
speciálních	speciální	k2eAgFnPc2d1	speciální
železničních	železniční	k2eAgFnPc2d1	železniční
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jedinou	jediný	k2eAgFnSc7d1	jediná
existující	existující	k2eAgFnSc7d1	existující
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nS	patřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
předchozího	předchozí	k2eAgInSc2d1	předchozí
drážního	drážní	k2eAgInSc2d1	drážní
zákona	zákon	k1gInSc2	zákon
spadalo	spadat	k5eAaImAgNnS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
městské	městský	k2eAgFnSc2d1	městská
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
<g/>
"	"	kIx"	"
a	a	k8xC	a
nepatřilo	patřit	k5eNaImAgNnS	patřit
mezi	mezi	k7c4	mezi
železniční	železniční	k2eAgFnPc4d1	železniční
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
metra	metro	k1gNnSc2	metro
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
zkrácenina	zkrácenina	k1gFnSc1	zkrácenina
názvu	název	k1gInSc2	název
Metropolitan	metropolitan	k1gInSc1	metropolitan
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
přes	přes	k7c4	přes
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
ruštinu	ruština	k1gFnSc4	ruština
proniklo	proniknout	k5eAaPmAgNnS	proniknout
i	i	k9	i
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
podzemní	podzemní	k2eAgFnSc1d1	podzemní
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
název	název	k1gInSc1	název
U-Bahn	U-Bahn	k1gInSc4	U-Bahn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zkrácením	zkrácení	k1gNnSc7	zkrácení
slova	slovo	k1gNnSc2	slovo
Untergrundbahn	Untergrundbahn	k1gInSc1	Untergrundbahn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
metra	metro	k1gNnSc2	metro
začínají	začínat	k5eAaImIp3nP	začínat
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
spojnici	spojnice	k1gFnSc6	spojnice
dvou	dva	k4xCgNnPc2	dva
nádraží	nádraží	k1gNnPc2	nádraží
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
jezdil	jezdit	k5eAaImAgMnS	jezdit
parní	parní	k2eAgInSc4d1	parní
vlak	vlak	k1gInSc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Parní	parní	k2eAgFnSc1d1	parní
trakce	trakce	k1gFnSc1	trakce
však	však	k9	však
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
problematická	problematický	k2eAgFnSc1d1	problematická
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
nějakého	nějaký	k3yIgInSc2	nějaký
jiného	jiný	k2eAgInSc2d1	jiný
druhu	druh	k1gInSc2	druh
pohonu	pohon	k1gInSc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Mluvilo	mluvit	k5eAaImAgNnS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
nekonvenčních	konvenční	k2eNgInPc6d1	nekonvenční
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
pohon	pohon	k1gInSc1	pohon
lanem	lano	k1gNnSc7	lano
(	(	kIx(	(
<g/>
takový	takový	k3xDgInSc1	takový
byl	být	k5eAaImAgInS	být
zrealizován	zrealizovat	k5eAaPmNgInS	zrealizovat
v	v	k7c6	v
Glasgowském	glasgowský	k2eAgNnSc6d1	glasgowské
metru	metro	k1gNnSc6	metro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
pomocí	pomocí	k7c2	pomocí
boční	boční	k2eAgFnSc2d1	boční
přívodní	přívodní	k2eAgFnSc2d1	přívodní
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
přispěl	přispět	k5eAaPmAgInS	přispět
i	i	k9	i
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
britským	britský	k2eAgInSc7d1	britský
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
parní	parní	k2eAgInSc1d1	parní
provoz	provoz	k1gInSc1	provoz
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
podmínkách	podmínka	k1gFnPc6	podmínka
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Británie	Británie	k1gFnSc2	Británie
se	se	k3xPyFc4	se
však	však	k9	však
podzemní	podzemní	k2eAgFnPc1d1	podzemní
dráhy	dráha	k1gFnPc1	dráha
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
krátké	krátký	k2eAgFnPc4d1	krátká
tratě	trať	k1gFnPc4	trať
<g/>
,	,	kIx,	,
budované	budovaný	k2eAgInPc4d1	budovaný
většinou	většinou	k6eAd1	většinou
hloubením	hloubení	k1gNnPc3	hloubení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
provoz	provoz	k1gInSc1	provoz
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
plně	plně	k6eAd1	plně
elektrifikován	elektrifikován	k2eAgMnSc1d1	elektrifikován
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
animální	animální	k2eAgFnSc4d1	animální
trakci	trakce	k1gFnSc4	trakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
pokroku	pokrok	k1gInSc2	pokrok
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
<g/>
,	,	kIx,	,
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
sídlech	sídlo	k1gNnPc6	sídlo
jím	jíst	k5eAaImIp1nS	jíst
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
elektrická	elektrický	k2eAgFnSc1d1	elektrická
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
s	s	k7c7	s
vybudováním	vybudování	k1gNnSc7	vybudování
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
takový	takový	k3xDgInSc1	takový
plán	plán	k1gInSc1	plán
narazil	narazit	k5eAaPmAgInS	narazit
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
návrhy	návrh	k1gInPc1	návrh
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
i	i	k9	i
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
též	též	k9	též
neuspěly	uspět	k5eNaPmAgInP	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
přibyly	přibýt	k5eAaPmAgFnP	přibýt
nové	nový	k2eAgFnPc4d1	nová
sítě	síť	k1gFnPc4	síť
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
proniklo	proniknout	k5eAaPmAgNnS	proniknout
metro	metro	k1gNnSc1	metro
i	i	k9	i
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
;	;	kIx,	;
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
manifestací	manifestace	k1gFnSc7	manifestace
i	i	k8xC	i
politickou	politický	k2eAgFnSc7d1	politická
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
stylu	styl	k1gInSc6	styl
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
další	další	k2eAgFnSc2d1	další
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
13	[number]	k4	13
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
použitá	použitý	k2eAgFnSc1d1	použitá
technologie	technologie	k1gFnSc1	technologie
byla	být	k5eAaImAgFnS	být
uplatněna	uplatnit	k5eAaPmNgFnS	uplatnit
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
;	;	kIx,	;
revolučním	revoluční	k2eAgNnSc6d1	revoluční
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
nasazení	nasazení	k1gNnSc1	nasazení
do	do	k7c2	do
provozu	provoz	k1gInSc6	provoz
prvních	první	k4xOgInPc2	první
vozů	vůz	k1gInPc2	vůz
s	s	k7c7	s
hliníkovou	hliníkový	k2eAgFnSc7d1	hliníková
kostrou	kostra	k1gFnSc7	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
vlaky	vlak	k1gInPc1	vlak
staly	stát	k5eAaPmAgInP	stát
lehčími	lehký	k2eAgInPc7d2	lehčí
a	a	k8xC	a
odolnějšími	odolný	k2eAgInPc7d2	odolnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
staré	starý	k2eAgNnSc1d1	staré
ocelové	ocelový	k2eAgInPc1d1	ocelový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
metrem	metr	k1gInSc7	metr
na	na	k7c6	na
pneumatikách	pneumatika	k1gFnPc6	pneumatika
(	(	kIx(	(
<g/>
Lyon	Lyon	k1gInSc4	Lyon
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
také	také	k9	také
podobné	podobný	k2eAgInPc1d1	podobný
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
více	hodně	k6eAd2	hodně
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
i	i	k9	i
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
ropné	ropný	k2eAgFnSc6d1	ropná
krizi	krize	k1gFnSc6	krize
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
MHD	MHD	kA	MHD
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zase	zase	k9	zase
začínají	začínat	k5eAaImIp3nP	začínat
zavádět	zavádět	k5eAaImF	zavádět
i	i	k9	i
automatizované	automatizovaný	k2eAgInPc1d1	automatizovaný
vozy	vůz	k1gInPc1	vůz
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
nasazením	nasazení	k1gNnSc7	nasazení
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
nově	nově	k6eAd1	nově
budované	budovaný	k2eAgFnSc6d1	budovaná
lince	linka	k1gFnSc6	linka
metra	metro	k1gNnSc2	metro
D.	D.	kA	D.
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
zažívá	zažívat	k5eAaImIp3nS	zažívat
velký	velký	k2eAgInSc1d1	velký
rozvoj	rozvoj	k1gInSc1	rozvoj
metro	metro	k1gNnSc1	metro
například	například	k6eAd1	například
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
rozvoji	rozvoj	k1gInSc3	rozvoj
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
metro	metro	k1gNnSc1	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
</p>
<p>
<s>
V	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
dvoumilionovém	dvoumilionový	k2eAgInSc6d1	dvoumilionový
Londýně	Londýn	k1gInSc6	Londýn
byl	být	k5eAaImAgInS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1863	[number]	k4	1863
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
první	první	k4xOgInSc1	první
sedmikilometrový	sedmikilometrový	k2eAgInSc1d1	sedmikilometrový
úsek	úsek	k1gInSc1	úsek
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Tube	tubus	k1gInSc5	tubus
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
Bishops	Bishops	k1gInSc4	Bishops
Road	Road	k1gInSc4	Road
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nádraží	nádraží	k1gNnSc1	nádraží
Paddington	Paddington	k1gInSc1	Paddington
<g/>
)	)	kIx)	)
a	a	k8xC	a
Farringdon	Farringdon	k1gMnSc1	Farringdon
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
V	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
bylo	být	k5eAaImAgNnS	být
metro	metro	k1gNnSc1	metro
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
pět	pět	k4xCc4	pět
kilometrů	kilometr	k1gInPc2	kilometr
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
maďarská	maďarský	k2eAgFnSc1d1	maďarská
metropole	metropole	k1gFnSc1	metropole
pře	pře	k1gFnSc2	pře
s	s	k7c7	s
řeckými	řecký	k2eAgFnPc7d1	řecká
Aténami	Atény	k1gFnPc7	Atény
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
fungoval	fungovat	k5eAaImAgInS	fungovat
povrchový	povrchový	k2eAgInSc1d1	povrchový
vlakový	vlakový	k2eAgInSc1d1	vlakový
spoj	spoj	k1gInSc1	spoj
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Atény-Pireus	Atény-Pireus	k1gInSc4	Atény-Pireus
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
1895	[number]	k4	1895
zde	zde	k6eAd1	zde
mezi	mezi	k7c7	mezi
zastávkami	zastávka	k1gFnPc7	zastávka
Thissio	Thissio	k6eAd1	Thissio
a	a	k8xC	a
Omonia	Omonium	k1gNnSc2	Omonium
projížděl	projíždět	k5eAaImAgInS	projíždět
vlak	vlak	k1gInSc1	vlak
tunelem	tunel	k1gInSc7	tunel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
síť	síť	k1gFnSc1	síť
tratí	trať	k1gFnPc2	trať
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
420	[number]	k4	420
km	km	kA	km
stala	stát	k5eAaPmAgFnS	stát
nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
překonala	překonat	k5eAaPmAgFnS	překonat
londýnské	londýnský	k2eAgNnSc4d1	Londýnské
metro	metro	k1gNnSc4	metro
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
408	[number]	k4	408
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
nejvytíženější	vytížený	k2eAgFnPc4d3	nejvytíženější
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
jedna	jeden	k4xCgFnSc1	jeden
nová	nový	k2eAgFnSc1d1	nová
a	a	k8xC	a
různá	různý	k2eAgNnPc4d1	různé
prodloužení	prodloužení	k1gNnPc4	prodloužení
stávajících	stávající	k2eAgFnPc2d1	stávající
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
má	mít	k5eAaImIp3nS	mít
její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
644	[number]	k4	644
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
stanic	stanice	k1gFnPc2	stanice
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
počet	počet	k1gInSc4	počet
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
konkurenci	konkurence	k1gFnSc3	konkurence
metro	metro	k1gNnSc1	metro
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
469	[number]	k4	469
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
277	[number]	k4	277
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
)	)	kIx)	)
na	na	k7c6	na
22	[number]	k4	22
trasách	trasa	k1gFnPc6	trasa
a	a	k8xC	a
síť	síť	k1gFnSc4	síť
383	[number]	k4	383
kilometrů	kilometr	k1gInPc2	kilometr
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
přepraví	přepravit	k5eAaPmIp3nS	přepravit
přes	přes	k7c4	přes
pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvytíženější	vytížený	k2eAgInSc1d3	nejvytíženější
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
Pekingské	pekingský	k2eAgNnSc1d1	pekingské
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
sice	sice	k8xC	sice
největší	veliký	k2eAgNnSc1d3	veliký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
nejvytíženější	vytížený	k2eAgNnSc1d3	nejvytíženější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
ročně	ročně	k6eAd1	ročně
přepraví	přepravit	k5eAaPmIp3nS	přepravit
3,7	[number]	k4	3,7
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
10,3	[number]	k4	10,3
mil	míle	k1gFnPc2	míle
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejmodernější	moderní	k2eAgMnSc1d3	nejmodernější
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
žebříčku	žebříček	k1gInSc6	žebříček
velikosti	velikost	k1gFnSc2	velikost
meter	metro	k1gNnPc2	metro
je	být	k5eAaImIp3nS	být
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
disponuje	disponovat	k5eAaBmIp3nS	disponovat
nejmodernější	moderní	k2eAgFnSc7d3	nejmodernější
trasou	trasa	k1gFnSc7	trasa
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
linka	linka	k1gFnSc1	linka
číslo	číslo	k1gNnSc1	číslo
14	[number]	k4	14
otevřená	otevřený	k2eAgNnPc1d1	otevřené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měří	měřit	k5eAaImIp3nS	měřit
sedm	sedm	k4xCc4	sedm
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
rozšiřující	rozšiřující	k2eAgFnSc3d1	rozšiřující
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
menší	malý	k2eAgNnSc1d2	menší
než	než	k8xS	než
pařížské	pařížský	k2eAgNnSc1d1	pařížské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
během	během	k7c2	během
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
až	až	k9	až
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
síť	síť	k1gFnSc1	síť
tratí	trať	k1gFnPc2	trať
ze	z	k7c2	z
170	[number]	k4	170
kilometrů	kilometr	k1gInPc2	kilometr
až	až	k9	až
na	na	k7c4	na
228	[number]	k4	228
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
58	[number]	k4	58
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
6,4	[number]	k4	6,4
kilometru	kilometr	k1gInSc2	kilometr
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
úsek	úsek	k1gInSc1	úsek
metra	metro	k1gNnSc2	metro
mezi	mezi	k7c7	mezi
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
a	a	k8xC	a
Černým	černý	k2eAgInSc7d1	černý
Mostem	most	k1gInSc7	most
stavěl	stavět	k5eAaImAgMnS	stavět
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
umístěné	umístěný	k2eAgInPc1d1	umístěný
</s>
</p>
<p>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
položenou	položený	k2eAgFnSc7d1	položená
stanicí	stanice	k1gFnSc7	stanice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
konečná	konečná	k1gFnSc1	konečná
Alpin	alpinum	k1gNnPc2	alpinum
na	na	k7c4	na
Mittelallalinu	Mittelallalina	k1gFnSc4	Mittelallalina
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3456	[number]	k4	3456
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
</s>
</p>
<p>
<s>
Nejkratší	krátký	k2eAgNnSc1d3	nejkratší
metro	metro	k1gNnSc1	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
trasa	trasa	k1gFnSc1	trasa
měří	měřit	k5eAaImIp3nS	měřit
573	[number]	k4	573
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Tünel	Tünel	k1gInSc1	Tünel
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
Zlatého	zlatý	k2eAgInSc2d1	zlatý
rohu	roh	k1gInSc2	roh
pod	pod	k7c7	pod
čtvrtí	čtvrt	k1gFnSc7	čtvrt
Karaköy	Karaköa	k1gFnSc2	Karaköa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
lanovou	lanový	k2eAgFnSc4d1	lanová
dráhu	dráha	k1gFnSc4	dráha
vedenou	vedený	k2eAgFnSc4d1	vedená
podzemím	podzemí	k1gNnSc7	podzemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejmenší	malý	k2eAgFnSc1d3	nejmenší
</s>
</p>
<p>
<s>
Nejmenší	malý	k2eAgNnSc1d3	nejmenší
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
Glasgowské	glasgowský	k2eAgNnSc1d1	glasgowské
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
tunelu	tunel	k1gInSc2	tunel
je	být	k5eAaImIp3nS	být
pouhých	pouhý	k2eAgInPc2d1	pouhý
11	[number]	k4	11
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
asi	asi	k9	asi
3,3	[number]	k4	3,3
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
okružní	okružní	k2eAgFnSc1d1	okružní
linka	linka	k1gFnSc1	linka
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
poháněná	poháněný	k2eAgFnSc1d1	poháněná
lanem	lano	k1gNnSc7	lano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
nástupiště	nástupiště	k1gNnSc4	nástupiště
</s>
</p>
<p>
<s>
Nástupiště	nástupiště	k1gNnSc1	nástupiště
stanice	stanice	k1gFnSc2	stanice
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
State	status	k1gInSc5	status
Street	Streeta	k1gFnPc2	Streeta
Center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
Chicaga	Chicago	k1gNnSc2	Chicago
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Stop	stop	k1gInSc1	stop
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
1066	[number]	k4	1066
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejmenší	malý	k2eAgFnSc1d3	nejmenší
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
podzemní	podzemní	k2eAgFnSc7d1	podzemní
dráhou	dráha	k1gFnSc7	dráha
</s>
</p>
<p>
<s>
Dorfbahn	Dorfbahn	k1gMnSc1	Dorfbahn
Serfaus	Serfaus	k1gMnSc1	Serfaus
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
podzemních	podzemní	k2eAgFnPc2d1	podzemní
drah	draha	k1gFnPc2	draha
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
tou	ten	k3xDgFnSc7	ten
druhou	druhý	k4xOgFnSc7	druhý
je	být	k5eAaImIp3nS	být
Metro	metro	k1gNnSc1	metro
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dorfbahn	Dorfbahn	k1gInSc1	Dorfbahn
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
tisícem	tisíc	k4xCgInSc7	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Seznamy	seznam	k1gInPc4	seznam
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
podzemních	podzemní	k2eAgFnPc2d1	podzemní
drah	draha	k1gFnPc2	draha
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
podzemních	podzemní	k2eAgFnPc2d1	podzemní
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
chronologický	chronologický	k2eAgInSc1d1	chronologický
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
Metro	metro	k1gNnSc1	metro
sovětského	sovětský	k2eAgInSc2d1	sovětský
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
Příměstská	příměstský	k2eAgFnSc1d1	příměstská
železnice	železnice	k1gFnSc1	železnice
</s>
</p>
<p>
<s>
Lehké	Lehké	k2eAgNnSc1d1	Lehké
metro	metro	k1gNnSc1	metro
</s>
</p>
<p>
<s>
Metro	metro	k1gNnSc1	metro
na	na	k7c6	na
pneumatikách	pneumatika	k1gFnPc6	pneumatika
</s>
</p>
<p>
<s>
Erektor	Erektor	k1gMnSc1	Erektor
</s>
</p>
<p>
<s>
Eskalátor	eskalátor	k1gInSc1	eskalátor
</s>
</p>
<p>
<s>
Razicí	razicí	k2eAgInSc1d1	razicí
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnPc1	stanice
metra	metro	k1gNnSc2	metro
pražského	pražský	k2eAgInSc2d1	pražský
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnPc1	stanice
metra	metro	k1gNnSc2	metro
petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnPc1	stanice
metra	metro	k1gNnSc2	metro
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
Tunelovací	Tunelovací	k2eAgFnPc1d1	Tunelovací
metody	metoda	k1gFnPc1	metoda
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
metro	metro	k1gNnSc1	metro
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
metro	metro	k1gNnSc4	metro
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Urbanrail	Urbanrail	k1gInSc1	Urbanrail
<g/>
.	.	kIx.	.
net	net	k?	net
-	-	kIx~	-
anglické	anglický	k2eAgFnSc2d1	anglická
stránky	stránka	k1gFnSc2	stránka
s	s	k7c7	s
popisem	popis	k1gInSc7	popis
téměř	téměř	k6eAd1	téměř
každého	každý	k3xTgInSc2	každý
provozu	provoz	k1gInSc2	provoz
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
</p>
<p>
<s>
Metro	metro	k1gNnSc1	metro
Bits	Bitsa	k1gFnPc2	Bitsa
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
Subwaynavigator	Subwaynavigator	k1gInSc1	Subwaynavigator
věnovaná	věnovaný	k2eAgNnPc1d1	věnované
dopravě	doprava	k1gFnSc3	doprava
metrem	metro	k1gNnSc7	metro
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
