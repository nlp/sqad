<s>
Uppsalská	uppsalský	k2eAgFnSc1d1	Uppsalská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
též	též	k9	též
Královská	královský	k2eAgFnSc1d1	královská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
Uppsala	Uppsala	k1gFnSc1	Uppsala
universitet	universitet	k5eAaImF	universitet
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gInSc1	Universitas
regia	regium	k1gNnSc2	regium
upsaliensis	upsaliensis	k1gFnSc2	upsaliensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
švédská	švédský	k2eAgFnSc1d1	švédská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nejstarší	starý	k2eAgFnSc7d3	nejstarší
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
severských	severský	k2eAgFnPc6d1	severská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
devíti	devět	k4xCc2	devět
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Skupiny	skupina	k1gFnSc2	skupina
Coimbra	Coimbr	k1gInSc2	Coimbr
<g/>
.	.	kIx.	.
</s>
<s>
Motto	motto	k1gNnSc1	motto
univerzity	univerzita	k1gFnSc2	univerzita
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
Gratiae	Gratiae	k1gFnSc1	Gratiae
veritas	veritasa	k1gFnPc2	veritasa
naturae	naturaat	k5eAaPmIp3nS	naturaat
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Uppsalská	uppsalský	k2eAgFnSc1d1	Uppsalská
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Uppsalské	uppsalský	k2eAgFnSc2d1	Uppsalská
univerzity	univerzita	k1gFnSc2	univerzita
</s>
