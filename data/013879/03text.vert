<s>
Vlasta	Vlasta	k1gFnSc1
Havelková	Havelková	k1gFnSc1
</s>
<s>
Vlasta	Vlasta	k1gFnSc1
Havelková	Havelková	k1gFnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Wlasta	Wlasta	k1gMnSc1
Barbara	barbar	k1gMnSc2
Elisabeth	Elisabeth	k1gMnSc1
Wankel	Wankel	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1857	#num#	k4
<g/>
Blansko	Blansko	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1939	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
81	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
česká	český	k2eAgNnPc1d1
Povolání	povolání	k1gNnPc1
</s>
<s>
etnografka	etnografka	k1gFnSc1
<g/>
,	,	kIx,
kurátorka	kurátorka	k1gFnSc1
sbírky	sbírka	k1gFnSc2
<g/>
,	,	kIx,
autorka	autorka	k1gFnSc1
a	a	k8xC
sběratelka	sběratelka	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Havelka	Havelka	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Milada	Milada	k1gFnSc1
Schusserová	Schusserový	k2eAgFnSc1d1
Rodiče	rodič	k1gMnSc4
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Wankel	Wankel	k1gMnSc1
<g/>
,	,	kIx,
Eliška	Eliška	k1gFnSc1
Wanklová-Šímová	Wanklová-Šímová	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
manžel	manžel	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Havelka	Havelka	k1gMnSc1
</s>
<s>
tchán	tchán	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Havelka	Havelka	k1gMnSc1
</s>
<s>
dědeček	dědeček	k1gMnSc1
</s>
<s>
Damián	Damián	k1gMnSc1
Wankel	Wankel	k1gMnSc1
</s>
<s>
otec	otec	k1gMnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Wankel	Wankel	k1gMnSc1
</s>
<s>
sestra	sestra	k1gFnSc1
</s>
<s>
Lucie	Lucie	k1gFnSc1
Bakešová	Bakešová	k1gFnSc1
</s>
<s>
sestra	sestra	k1gFnSc1
</s>
<s>
Karla	Karla	k1gFnSc1
Absolonová-Bufková	Absolonová-Bufková	k1gFnSc1
</s>
<s>
sestra	sestra	k1gFnSc1
</s>
<s>
Madlena	Madlena	k1gFnSc1
Wanklová	Wanklová	k1gFnSc1
</s>
<s>
dcera	dcera	k1gFnSc1
</s>
<s>
Milada	Milada	k1gFnSc1
Schusserová	Schusserová	k1gFnSc1
</s>
<s>
Vlasta	Vlasta	k1gFnSc1
Havelková	Havelková	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Wlasta	Wlasta	k1gFnSc1
Barbara	Barbara	k1gFnSc1
Elisabeth	Elisabeth	k1gInSc4
Wankel	Wankel	k1gInSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1857	#num#	k4
Blansko	Blansko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1939	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
česká	český	k2eAgFnSc1d1
etnografka	etnografka	k1gFnSc1
<g/>
,	,	kIx,
zaměřená	zaměřený	k2eAgFnSc1d1
především	především	k9
na	na	k7c4
výzkum	výzkum	k1gInSc4
výšivky	výšivka	k1gFnSc2
a	a	k8xC
ornamentu	ornament	k1gInSc2
ve	v	k7c6
slovanském	slovanský	k2eAgNnSc6d1
lidovém	lidový	k2eAgNnSc6d1
umění	umění	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
muzejnice	muzejnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Byla	být	k5eAaImAgFnS
dcerou	dcera	k1gFnSc7
lékaře	lékař	k1gMnSc2
a	a	k8xC
amatérského	amatérský	k2eAgMnSc2d1
archeologa	archeolog	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
Wankla	Wankla	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
středoškolského	středoškolský	k2eAgMnSc4d1
profesora	profesor	k1gMnSc4
Jana	Jan	k1gMnSc4
Havelku	Havelka	k1gMnSc4
a	a	k8xC
přestěhovala	přestěhovat	k5eAaPmAgFnS
se	se	k3xPyFc4
do	do	k7c2
Olomouce	Olomouc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
spoluzaložila	spoluzaložit	k5eAaPmAgFnS
Vlastivědný	vlastivědný	k2eAgInSc4d1
spolek	spolek	k1gInSc4
musejní	musejní	k2eAgInSc4d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc1d1
časopis	časopis	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
Vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
podílela	podílet	k5eAaImAgFnS
především	především	k9
na	na	k7c6
přípravě	příprava	k1gFnSc6
výstav	výstava	k1gFnPc2
výšivek	výšivka	k1gFnPc2
a	a	k8xC
krojů	kroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
raném	raný	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
publikovala	publikovat	k5eAaBmAgFnS
v	v	k7c6
Časopisu	časopis	k1gInSc6
vlasteneckého	vlastenecký	k2eAgInSc2d1
spolku	spolek	k1gInSc2
musejního	musejní	k2eAgInSc2d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
a	a	k8xC
v	v	k7c6
trojsvazkovém	trojsvazkový	k2eAgInSc6d1
sborníku	sborník	k1gInSc6
Moravské	moravský	k2eAgInPc1d1
ornamenty	ornament	k1gInPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
její	její	k3xOp3gNnSc4
silné	silný	k2eAgNnSc4d1
národnostní	národnostní	k2eAgNnSc4d1
cítění	cítění	k1gNnSc4
a	a	k8xC
nekritický	kritický	k2eNgInSc4d1
obdiv	obdiv	k1gInSc4
k	k	k7c3
lidové	lidový	k2eAgFnSc3d1
výtvarné	výtvarný	k2eAgFnSc3d1
kultuře	kultura	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
dobového	dobový	k2eAgNnSc2d1
vlastenectví	vlastenectví	k1gNnSc2
ji	on	k3xPp3gFnSc4
v	v	k7c6
tom	ten	k3xDgInSc6
ovlivnily	ovlivnit	k5eAaPmAgInP
jak	jak	k6eAd1
názory	názor	k1gInPc1
jejího	její	k3xOp3gMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
považoval	považovat	k5eAaImAgMnS
Slovany	Slovan	k1gMnPc4
za	za	k7c4
praobyvatele	praobyvatel	k1gMnPc4
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
jejího	její	k3xOp3gMnSc4
manžela	manžel	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
věřil	věřit	k5eAaImAgMnS
v	v	k7c4
autochtonnost	autochtonnost	k1gFnSc4
Slovanů	Slovan	k1gInPc2
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svého	svůj	k3xOyFgMnSc2
spolupracovníka	spolupracovník	k1gMnSc2
Ignáta	Ignát	k1gMnSc2
Wurma	Wurm	k1gMnSc2xF
zase	zase	k9
přejala	přejmout	k5eAaPmAgFnS
metodu	metoda	k1gFnSc4
srovnávání	srovnávání	k1gNnSc2
mezi	mezi	k7c7
ornamenty	ornament	k1gInPc7
lidovými	lidový	k2eAgInPc7d1
a	a	k8xC
pocházejícími	pocházející	k2eAgInPc7d1
z	z	k7c2
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgMnSc6
duchu	duch	k1gMnSc6
například	například	k6eAd1
považovala	považovat	k5eAaImAgFnS
halštatskou	halštatský	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
za	za	k7c4
slovanskou	slovanský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Až	až	k9
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
počala	počnout	k5eAaPmAgFnS
opouštět	opouštět	k5eAaImF
názor	názor	k1gInSc4
o	o	k7c6
slovanském	slovanský	k2eAgInSc6d1
původu	původ	k1gInSc6
evropské	evropský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
připustila	připustit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
základní	základní	k2eAgInPc1d1
ornamentální	ornamentální	k2eAgInPc1d1
prvky	prvek	k1gInPc1
mohly	moct	k5eAaImAgInP
existovat	existovat	k5eAaImF
v	v	k7c6
různých	různý	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	sebe	k3xPyFc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
spolupracovala	spolupracovat	k5eAaImAgFnS
na	na	k7c6
moravské	moravský	k2eAgFnSc6d1
kolekci	kolekce	k1gFnSc6
na	na	k7c6
Národopisné	národopisný	k2eAgFnSc6d1
výstavě	výstava	k1gFnSc6
českoslovanské	českoslovanský	k2eAgFnSc6d1
v	v	k7c6
roce	rok	k1gInSc6
1895	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
byla	být	k5eAaImAgFnS
Luborem	Lubor	k1gMnSc7
Niederlem	Niederl	k1gInSc7
ustanovena	ustanoven	k2eAgFnSc1d1
kustodkou	kustodka	k1gFnSc7
Národopisného	národopisný	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
svoji	svůj	k3xOyFgFnSc4
publikační	publikační	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
téměř	téměř	k6eAd1
ukončila	ukončit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
XIX	XIX	kA
<g/>
,	,	kIx,
Dejvice	Dejvice	k1gFnPc1
bydlela	bydlet	k5eAaImAgFnS
na	na	k7c6
adrese	adresa	k1gFnSc6
Dejvická	dejvický	k2eAgFnSc1d1
17	#num#	k4
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Spisy	spis	k1gInPc1
</s>
<s>
Výstava	výstava	k1gFnSc1
národního	národní	k2eAgNnSc2d1
vyšívání	vyšívání	k1gNnSc2
moravského	moravský	k2eAgNnSc2d1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
v	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1885	#num#	k4
uspořádal	uspořádat	k5eAaPmAgInS
muzejní	muzejní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
vlastním	vlastní	k2eAgInSc7d1
nákladem	náklad	k1gInSc7
<g/>
,	,	kIx,
1885	#num#	k4
</s>
<s>
Vyšívání	vyšívání	k1gNnSc1
lidu	lid	k1gInSc2
moravského	moravský	k2eAgInSc2d1
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
v.	v.	k?
n.	n.	k?
<g/>
,	,	kIx,
1889	#num#	k4
</s>
<s>
Obušek	obušek	k1gInSc1
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
v.	v.	k?
n.	n.	k?
<g/>
,	,	kIx,
1894	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
některých	některý	k3yIgInPc2
ornamentů	ornament	k1gInPc2
našich	náš	k3xOp1gMnPc2
lidových	lidový	k2eAgMnPc2d1
ve	v	k7c6
vyšívání	vyšívání	k1gNnSc6
–	–	k?
kresby	kresba	k1gFnSc2
opatřila	opatřit	k5eAaPmAgFnS
Madlenka	Madlenka	k1gFnSc1
Wanklova	Wanklův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Vlastenecký	vlastenecký	k2eAgInSc1d1
muzejní	muzejní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
(	(	kIx(
<g/>
VMS	VMS	kA
<g/>
)	)	kIx)
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
1898	#num#	k4
</s>
<s>
O	o	k7c6
symbolice	symbolika	k1gFnSc6
v	v	k7c6
naší	náš	k3xOp1gFnSc6
ornamentice	ornamentika	k1gFnSc6
<g/>
:	:	kIx,
k	k	k7c3
výstavě	výstava	k1gFnSc3
výšivek	výšivka	k1gFnPc2
a	a	k8xC
krajek	krajka	k1gFnPc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
–	–	k?
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
v.	v.	k?
n.	n.	k?
<g/>
,	,	kIx,
1903	#num#	k4
</s>
<s>
Šata	šata	k1gFnSc1
česko-slovanská	česko-slovanský	k2eAgFnSc1d1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národopisná	národopisný	k2eAgFnSc1d1
Společnost	společnost	k1gFnSc1
Českoslovanská	českoslovanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
NSČ	NSČ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1903	#num#	k4
<g/>
?	?	kIx.
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
v.	v.	k?
n.	n.	k?
<g/>
,	,	kIx,
1929	#num#	k4
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
Moravské	moravský	k2eAgInPc1d1
ornamenty	ornament	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sešit	sešit	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
–	–	k?
na	na	k7c4
kámen	kámen	k1gInSc4
kreslila	kreslit	k5eAaImAgFnS
Madlenka	Madlenka	k1gFnSc1
Vanklova	Vanklův	k2eAgFnSc1d1
<g/>
;	;	kIx,
sepsala	sepsat	k5eAaPmAgFnS
úvod	úvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vídeň	Vídeň	k1gFnSc1
<g/>
:	:	kIx,
VMS	VMS	kA
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
1890	#num#	k4
</s>
<s>
Průvodce	průvodce	k1gMnSc1
po	po	k7c6
Národopisném	národopisný	k2eAgNnSc6d1
museu	museum	k1gNnSc6
českoslovanském	českoslovanský	k2eAgNnSc6d1
v	v	k7c6
zahradě	zahrada	k1gFnSc6
Kinských	Kinských	k2eAgFnSc6d1
v	v	k7c6
Praze-Smíchově	Praze-Smíchův	k2eAgMnSc6d1
–	–	k?
redakcí	redakce	k1gFnSc7
Adolfa	Adolf	k1gMnSc2
Černého	Černý	k1gMnSc2
s	s	k7c7
příspěvky	příspěvek	k1gInPc7
Vlasty	Vlasta	k1gFnSc2
Havelkové	Havelková	k1gFnSc2
<g/>
,	,	kIx,
Madlenky	Madlenka	k1gFnSc2
Wanklovy	Wanklův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NSČ	NSČ	kA
<g/>
,	,	kIx,
1903	#num#	k4
</s>
<s>
Jaký	jaký	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
význam	význam	k1gInSc1
má	mít	k5eAaImIp3nS
dnes	dnes	k6eAd1
lidový	lidový	k2eAgInSc4d1
kroj	kroj	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
Augustin	Augustin	k1gMnSc1
Žalud	Žalud	k1gMnSc1
<g/>
;	;	kIx,
napsala	napsat	k5eAaPmAgFnS,k5eAaBmAgFnS
návod	návod	k1gInSc4
k	k	k7c3
šití	šití	k1gNnSc3
původních	původní	k2eAgInPc2d1
českých	český	k2eAgInPc2d1
krojů	kroj	k1gInPc2
lidových	lidový	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Alois	Alois	k1gMnSc1
Neubert	Neubert	k1gMnSc1
<g/>
,	,	kIx,
1914	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriky	matrika	k1gFnPc1
-	-	kIx~
ACTA	ACTA	kA
PUBLICA	publicum	k1gNnSc2
<g/>
.	.	kIx.
www.mza.cz	www.mza.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Alena	Alena	k1gFnSc1
Křížová	Křížová	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Starobylý	starobylý	k2eAgInSc1d1
<g/>
"	"	kIx"
původ	původ	k1gInSc1
lidového	lidový	k2eAgInSc2d1
ornamentu	ornament	k1gInSc2
(	(	kIx(
<g/>
podle	podle	k7c2
Vlasty	Vlasta	k1gFnSc2
Havelkové	Havelková	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Alena	Alena	k1gFnSc1
Křížová	Křížová	k1gFnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ornament	ornament	k1gInSc1
-	-	kIx~
oděv	oděv	k1gInSc1
-	-	kIx~
šperk	šperk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
4963	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
59	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rodina	rodina	k1gFnSc1
Wanklových	Wanklová	k1gFnPc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vlasta	Vlasta	k1gFnSc1
Havelková	Havelková	k1gFnSc1
–	–	k?
Richard	Richard	k1gMnSc1
Fischer	Fischer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
1940	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Autor	autor	k1gMnSc1
Vlasta	Vlasta	k1gMnSc1
Havelková	Havelková	k1gFnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Vlasta	Vlasta	k1gFnSc1
Havelková	Havelková	k1gFnSc1
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1
Brna	Brno	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1040297	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83725002	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
</s>
