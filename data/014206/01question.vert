<s>
Který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
matematik	matematik	k1gMnSc1
rakouského	rakouský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
logiků	logik	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
?	?	kIx.
</s>