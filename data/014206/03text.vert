<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1906	#num#	k4
<g/>
BrnoRakousko-Uhersko	BrnoRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
71	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
PrincetonUSA	PrincetonUSA	k1gMnPc2
USA	USA	kA
Příčina	příčina	k1gFnSc1
úmrtí	úmrť	k1gFnPc2
</s>
<s>
podvýživa	podvýživa	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Princetonu	Princeton	k1gInSc6
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
informatik	informatik	k1gMnSc1
a	a	k8xC
fyzik	fyzik	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Princetonská	Princetonský	k2eAgFnSc1d1
univerzitaUniversity	univerzitaUniversit	k1gInPc1
of	of	k?
Notre	Notr	k1gInSc5
DameVídeňská	DameVídeňský	k2eAgFnSc1d1
univerzitaInstitut	univerzitaInstitut	k1gInSc4
pro	pro	k7c4
pokročilé	pokročilý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Cena	cena	k1gFnSc1
Alberta	Albert	k1gMnSc2
Einsteina	Einstein	k1gMnSc2
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
Světové	světový	k2eAgNnSc1d1
ocenení	ocenení	k1gNnSc1
Alberta	Albert	k1gMnSc2
Einsteina	Einstein	k1gMnSc2
za	za	k7c4
vědu	věda	k1gFnSc4
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
Josiah	Josiah	k1gInSc1
Willard	Willard	k1gMnSc1
Gibbs	Gibbsa	k1gFnPc2
Lectureship	Lectureship	k1gMnSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
Národní	národní	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
za	za	k7c4
vědu	věda	k1gFnSc4
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
Honorary	Honorara	k1gFnSc2
doctor	doctor	k1gInSc1
of	of	k?
the	the	k?
University	universita	k1gFnSc2
of	of	k?
Vienna	Vienna	k1gFnSc1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Nábož	Nábož	k1gFnSc4
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc4
</s>
<s>
křesťanství	křesťanství	k1gNnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Adele	Adele	k6eAd1
Gödel	Gödel	k1gInSc1
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Gödel	Gödel	k1gInSc1
(	(	kIx(
<g/>
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1906	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
matematik	matematik	k1gMnSc1
rakouského	rakouský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
logiků	logicus	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
jeho	jeho	k3xOp3gInPc4
příspěvky	příspěvek	k1gInPc4
ve	v	k7c6
fyzice	fyzika	k1gFnSc6
a	a	k8xC
ve	v	k7c6
filozofii	filozofie	k1gFnSc6
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
větu	věta	k1gFnSc4
o	o	k7c6
úplnosti	úplnost	k1gFnSc6
predikátové	predikátový	k2eAgFnSc2d1
logiky	logika	k1gFnSc2
prvního	první	k4xOgInSc2
řádu	řád	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
svůj	svůj	k3xOyFgInSc4
zásadní	zásadní	k2eAgInSc4d1
objev	objev	k1gInSc4
–	–	k?
dvě	dva	k4xCgFnPc1
věty	věta	k1gFnPc1
o	o	k7c6
neúplnosti	neúplnost	k1gFnSc6
axiomatických	axiomatický	k2eAgInPc2d1
formálních	formální	k2eAgInPc2d1
systémů	systém	k1gInPc2
s	s	k7c7
aritmetikou	aritmetika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
těchto	tento	k3xDgFnPc2
vět	věta	k1gFnPc2
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
navrhnout	navrhnout	k5eAaPmF
soubor	soubor	k1gInSc4
axiomů	axiom	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgInP
dostačující	dostačující	k2eAgInPc1d1
pro	pro	k7c4
zodpovězení	zodpovězený	k2eAgMnPc1d1
každé	každý	k3xTgFnSc2
otázky	otázka	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
lze	lze	k6eAd1
klást	klást	k5eAaImF
a	a	k8xC
formulovat	formulovat	k5eAaImF
uvnitř	uvnitř	k7c2
formálního	formální	k2eAgInSc2d1
systému	systém	k1gInSc2
s	s	k7c7
aritmetikou	aritmetika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
Gödelovy	Gödelův	k2eAgFnPc1d1
věty	věta	k1gFnPc1
završily	završit	k5eAaPmAgFnP
více	hodně	k6eAd2
než	než	k8xS
padesátileté	padesátiletý	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
logiků	logicus	k1gMnPc2
a	a	k8xC
matematiků	matematik	k1gMnPc2
úplně	úplně	k6eAd1
formalizovat	formalizovat	k5eAaBmF
matematiku	matematika	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ovlivnily	ovlivnit	k5eAaPmAgFnP
i	i	k9
vědecké	vědecký	k2eAgNnSc4d1
a	a	k8xC
filosofické	filosofický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
Kurta	Kurt	k1gMnSc2
Gödela	Gödel	k1gMnSc2
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
rodném	rodný	k2eAgInSc6d1
domě	dům	k1gInSc6
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Pekařská	pekařský	k2eAgFnSc1d1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1906	#num#	k4
jako	jako	k8xS,k8xC
druhé	druhý	k4xOgNnSc4
dítě	dítě	k1gNnSc4
Marianny	Marianna	k1gMnSc2
a	a	k8xC
Rudolfa	Rudolf	k1gMnSc2
Gödelových	Gödelová	k1gFnPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
součástí	součást	k1gFnSc7
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodina	rodina	k1gFnSc1
mluvila	mluvit	k5eAaImAgFnS
německy	německy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudolf	Rudolf	k1gMnSc1
Gödel	Gödel	k1gMnSc1
byl	být	k5eAaImAgMnS
ředitelem	ředitel	k1gMnSc7
v	v	k7c6
textilní	textilní	k2eAgFnSc6d1
továrně	továrna	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
tuto	tento	k3xDgFnSc4
pozici	pozice	k1gFnSc4
se	se	k3xPyFc4
vypracoval	vypracovat	k5eAaPmAgMnS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
píli	píle	k1gFnSc3
a	a	k8xC
umu	um	k1gInSc2
<g/>
;	;	kIx,
patentoval	patentovat	k5eAaBmAgMnS
několik	několik	k4yIc1
vynálezů	vynález	k1gInPc2
v	v	k7c6
oboru	obor	k1gInSc6
textilních	textilní	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
Marianne	Mariann	k1gInSc5
podporovala	podporovat	k5eAaImAgFnS
své	svůj	k3xOyFgMnPc4
syny	syn	k1gMnPc4
Kurta	Kurt	k1gMnSc4
a	a	k8xC
staršího	starý	k2eAgMnSc4d2
Rudolfa	Rudolf	k1gMnSc4
v	v	k7c6
úctě	úcta	k1gFnSc6
ke	k	k7c3
vzdělání	vzdělání	k1gNnSc3
a	a	k8xC
intelektuálních	intelektuální	k2eAgInPc6d1
zájmech	zájem	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
někteří	některý	k3yIgMnPc1
příbuzní	příbuzný	k1gMnPc1
a	a	k8xC
brněnští	brněnský	k2eAgMnPc1d1
předkové	předek	k1gMnPc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
podíleli	podílet	k5eAaImAgMnP
na	na	k7c6
kulturním	kulturní	k2eAgInSc6d1
a	a	k8xC
obchodním	obchodní	k2eAgInSc6d1
životě	život	k1gInSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
navštěvoval	navštěvovat	k5eAaImAgMnS
v	v	k7c6
Brně	Brno	k1gNnSc6
evangelickou	evangelický	k2eAgFnSc4d1
základní	základní	k2eAgFnSc4d1
školu	škola	k1gFnSc4
a	a	k8xC
druhé	druhý	k4xOgNnSc1
německé	německý	k2eAgNnSc1d1
reálné	reálný	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
jej	on	k3xPp3gMnSc4
vybavilo	vybavit	k5eAaPmAgNnS
velmi	velmi	k6eAd1
dobrými	dobrý	k2eAgFnPc7d1
znalostmi	znalost	k1gFnPc7
nejen	nejen	k6eAd1
matematiky	matematik	k1gMnPc4
a	a	k8xC
fyziky	fyzik	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
příznačnou	příznačný	k2eAgFnSc7d1
pečlivostí	pečlivost	k1gFnSc7
a	a	k8xC
duchem	duch	k1gMnSc7
důkladnosti	důkladnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
Univerzitu	univerzita	k1gFnSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
studoval	studovat	k5eAaImAgMnS
fyziku	fyzika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přednášky	přednáška	k1gFnPc4
matematiky	matematika	k1gFnSc2
P.	P.	kA
Furtwanglera	Furtwangler	k1gMnSc4
a	a	k8xC
atmosféra	atmosféra	k1gFnSc1
kolem	kolem	k7c2
Vídeňského	vídeňský	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
jej	on	k3xPp3gMnSc4
přivedly	přivést	k5eAaPmAgFnP
k	k	k7c3
matematice	matematika	k1gFnSc3
a	a	k8xC
logice	logika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disertační	disertační	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
byla	být	k5eAaImAgFnS
úplnost	úplnost	k1gFnSc4
predikátového	predikátový	k2eAgInSc2d1
počtu	počet	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
úspěšně	úspěšně	k6eAd1
ukončil	ukončit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Hanse	Hans	k1gMnSc2
Hahna	Hahn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
své	své	k1gNnSc4
slavné	slavný	k2eAgFnSc2d1
věty	věta	k1gFnSc2
o	o	k7c6
neúplnosti	neúplnost	k1gFnSc6
a	a	k8xC
rok	rok	k1gInSc4
poté	poté	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
soukromým	soukromý	k2eAgMnSc7d1
docentem	docent	k1gMnSc7
a	a	k8xC
působil	působit	k5eAaImAgMnS
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
do	do	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
létech	léto	k1gNnPc6
několikrát	několikrát	k6eAd1
navštívil	navštívit	k5eAaPmAgMnS
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
na	na	k7c6
Institutu	institut	k1gInSc6
pokročilých	pokročilý	k2eAgNnPc2d1
studií	studio	k1gNnPc2
v	v	k7c6
Princetonu	Princeton	k1gInSc6
a	a	k8xC
přednášel	přednášet	k5eAaImAgMnS
také	také	k9
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Washingtonu	Washington	k1gInSc2
aj.	aj.	kA
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
se	se	k3xPyFc4
nervově	nervově	k6eAd1
zhroutil	zhroutit	k5eAaPmAgInS
z	z	k7c2
vyčerpání	vyčerpání	k1gNnSc2
a	a	k8xC
léčil	léčit	k5eAaImAgMnS
se	se	k3xPyFc4
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhoršující	zhoršující	k2eAgFnSc1d1
se	se	k3xPyFc4
politická	politický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
schylující	schylující	k2eAgFnSc1d1
se	se	k3xPyFc4
ke	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
jej	on	k3xPp3gMnSc4
vyhnala	vyhnat	k5eAaPmAgFnS
z	z	k7c2
Evropy	Evropa	k1gFnSc2
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
natrvalo	natrvalo	k6eAd1
odjel	odjet	k5eAaPmAgMnS
s	s	k7c7
manželkou	manželka	k1gFnSc7
Adele	Adele	k1gFnSc2
rok	rok	k1gInSc1
po	po	k7c6
svatbě	svatba	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
docent	docent	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
profesor	profesor	k1gMnSc1
na	na	k7c6
Institutu	institut	k1gInSc6
pokročilých	pokročilý	k2eAgNnPc2d1
studií	studio	k1gNnPc2
v	v	k7c6
Princetonu	Princeton	k1gInSc6
se	se	k3xPyFc4
intenzivně	intenzivně	k6eAd1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
filozofii	filozofie	k1gFnSc4
a	a	k8xC
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Alberta	Albert	k1gMnSc2
Einsteina	Einstein	k1gMnSc2
<g/>
,	,	kIx,
svého	svůj	k3xOyFgMnSc2
tamějšího	tamější	k2eAgMnSc2d1
blízkého	blízký	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
<g/>
,	,	kIx,
i	i	k8xC
fyzice	fyzika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Princetonu	Princeton	k1gInSc6
zažil	zažít	k5eAaPmAgInS
klidná	klidný	k2eAgNnPc4d1
léta	léto	k1gNnPc4
<g/>
:	:	kIx,
Se	s	k7c7
ženou	žena	k1gFnSc7
žili	žít	k5eAaImAgMnP
v	v	k7c4
ústraní	ústraní	k1gNnSc4
a	a	k8xC
vedli	vést	k5eAaImAgMnP
nenáročný	náročný	k2eNgInSc4d1
život	život	k1gInSc4
v	v	k7c6
malém	malý	k2eAgInSc6d1
domku	domek	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
si	se	k3xPyFc3
koupili	koupit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adele	Adele	k1gNnSc7
vedla	vést	k5eAaImAgFnS
domácnost	domácnost	k1gFnSc4
a	a	k8xC
pečovala	pečovat	k5eAaImAgFnS
o	o	k7c4
manžela	manžel	k1gMnSc4
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
byla	být	k5eAaImAgFnS
vždy	vždy	k6eAd1
oporou	opora	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Kurta	Kurt	k1gMnSc2
často	často	k6eAd1
navštěvovala	navštěvovat	k5eAaImAgFnS
rodinu	rodina	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Kurtova	Kurtův	k2eAgFnSc1d1
maminka	maminka	k1gFnSc1
a	a	k8xC
bratr	bratr	k1gMnSc1
jezdili	jezdit	k5eAaImAgMnP
za	za	k7c7
ním	on	k3xPp3gMnSc7
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společenské	společenský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
a	a	k8xC
společenský	společenský	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
Ústavu	ústav	k1gInSc6
kladly	klást	k5eAaImAgFnP
na	na	k7c4
Kurta	Kurt	k1gMnSc2
specifické	specifický	k2eAgInPc1d1
nároky	nárok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
legendou	legenda	k1gFnSc7
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
objevy	objev	k1gInPc4
a	a	k8xC
vyhledávanou	vyhledávaný	k2eAgFnSc4d1
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
očekávaly	očekávat	k5eAaImAgFnP
další	další	k2eAgInPc4d1
převratné	převratný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nemělo	mít	k5eNaImAgNnS
dobrý	dobrý	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
plachého	plachý	k2eAgMnSc4d1
<g/>
,	,	kIx,
uzavřeného	uzavřený	k2eAgMnSc4d1
a	a	k8xC
pečlivého	pečlivý	k2eAgMnSc4d1
až	až	k8xS
puntičkářského	puntičkářský	k2eAgMnSc4d1
samotáře	samotář	k1gMnSc4
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stal	stát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chatrné	chatrný	k2eAgNnSc1d1
zdraví	zdraví	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
mu	on	k3xPp3gMnSc3
rodina	rodina	k1gFnSc1
připisovala	připisovat	k5eAaImAgFnS
<g/>
,	,	kIx,
traumatizující	traumatizující	k2eAgInPc4d1
zážitky	zážitek	k1gInPc4
z	z	k7c2
období	období	k1gNnSc2
nacismu	nacismus	k1gInSc2
i	i	k8xC
tlak	tlak	k1gInSc1
na	na	k7c4
výkon	výkon	k1gInSc4
člověka	člověk	k1gMnSc2
s	s	k7c7
pověstí	pověst	k1gFnSc7
génia	génius	k1gMnSc2
se	se	k3xPyFc4
podepsaly	podepsat	k5eAaPmAgInP
na	na	k7c6
jeho	jeho	k3xOp3gFnPc6
psychosomatických	psychosomatický	k2eAgFnPc6d1
potížích	potíž	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	s	k7c7
stářím	stáří	k1gNnSc7
a	a	k8xC
odchodem	odchod	k1gInSc7
vrstevníků	vrstevník	k1gMnPc2
a	a	k8xC
blízkých	blízký	k2eAgMnPc2d1
přátel	přítel	k1gMnPc2
prohlubovaly	prohlubovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trpěl	trpět	k5eAaImAgInS
obsedantním	obsedantní	k2eAgInSc7d1
strachem	strach	k1gInSc7
z	z	k7c2
jedů	jed	k1gInPc2
a	a	k8xC
byl	být	k5eAaImAgMnS
schopen	schopen	k2eAgMnSc1d1
jíst	jíst	k5eAaImF
pouze	pouze	k6eAd1
jídla	jídlo	k1gNnPc4
připravená	připravený	k2eAgNnPc4d1
jeho	jeho	k3xOp3gFnSc7
ženou	žena	k1gFnSc7
Adélou	Adéla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
1977	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
Adéla	Adéla	k1gFnSc1
po	po	k7c4
dobu	doba	k1gFnSc4
6	#num#	k4
měsíců	měsíc	k1gInPc2
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
Gödel	Gödel	k1gMnSc1
odmítal	odmítat	k5eAaImAgMnS
jakékoliv	jakýkoliv	k3yIgNnSc4
jídlo	jídlo	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
až	až	k9
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
smrti	smrt	k1gFnSc3
hladem	hlad	k1gInSc7
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
v	v	k7c6
Pricetonské	Pricetonský	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
vážil	vážit	k5eAaImAgMnS
pouhých	pouhý	k2eAgFnPc2d1
29	#num#	k4
Kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pochovaný	pochovaný	k2eAgInSc1d1
na	na	k7c6
Pricetonském	Pricetonský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adéla	Adéla	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Matematická	matematický	k2eAgFnSc1d1
logika	logika	k1gFnSc1
</s>
<s>
V	v	k7c6
intelektuálním	intelektuální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
postsecesní	postsecesní	k2eAgFnSc2d1
Vídně	Vídeň	k1gFnSc2
vytvořil	vytvořit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
průlomové	průlomový	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
–	–	k?
objevil	objevit	k5eAaPmAgMnS
a	a	k8xC
formuloval	formulovat	k5eAaImAgMnS
dva	dva	k4xCgInPc4
teorémy	teorém	k1gInPc4
o	o	k7c6
neúplnosti	neúplnost	k1gFnSc6
<g/>
:	:	kIx,
Z	z	k7c2
prvního	první	k4xOgNnSc2
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
žádný	žádný	k3yNgInSc1
formální	formální	k2eAgInSc1d1
systém	systém	k1gInSc1
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
zároveň	zároveň	k6eAd1
úplný	úplný	k2eAgInSc1d1
a	a	k8xC
bezesporný	bezesporný	k2eAgInSc1d1
a	a	k8xC
z	z	k7c2
druhého	druhý	k4xOgMnSc2
<g/>
,	,	kIx,
že	že	k8xS
bezespornost	bezespornost	k1gFnSc1
formálního	formální	k2eAgInSc2d1
systému	systém	k1gInSc2
nelze	lze	k6eNd1
uvnitř	uvnitř	k7c2
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
dokázat	dokázat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
teorémy	teorém	k1gInPc1
se	se	k3xPyFc4
opírají	opírat	k5eAaImIp3nP
o	o	k7c4
důkaz	důkaz	k1gInSc4
existence	existence	k1gFnSc2
nerozhodnutelné	rozhodnutelný	k2eNgFnSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
prostředky	prostředek	k1gInPc4
systému	systém	k1gInSc2
formulovatelná	formulovatelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nedá	dát	k5eNaPmIp3nS
se	se	k3xPyFc4
dokázat	dokázat	k5eAaPmF
prostředky	prostředek	k1gInPc4
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepatří	patřit	k5eNaImIp3nS
do	do	k7c2
množiny	množina	k1gFnSc2
dokazatelných	dokazatelný	k2eAgFnPc2d1
vět	věta	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
pravdivost	pravdivost	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
důkazem	důkaz	k1gInSc7
prokázána	prokázán	k2eAgFnSc1d1
–	–	k?
je	být	k5eAaImIp3nS
nedokazatelná	dokazatelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
ale	ale	k9
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nedokazatelná	dokazatelný	k2eNgFnSc1d1
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
pravdu	pravda	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
proto	proto	k8xC
pravdivá	pravdivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
případem	případ	k1gInSc7
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
prostředky	prostředek	k1gInPc7
systému	systém	k1gInSc2
formulovat	formulovat	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
nikoli	nikoli	k9
dokázat	dokázat	k5eAaPmF
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
systém	systém	k1gInSc1
neúplný	úplný	k2eNgInSc1d1
<g/>
:	:	kIx,
Nedají	dát	k5eNaPmIp3nP
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gMnSc6
dokázat	dokázat	k5eAaPmF
všechny	všechen	k3xTgFnPc1
pravdivé	pravdivý	k2eAgFnPc1d1
věty	věta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
dají	dát	k5eAaPmIp3nP
formulovat	formulovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
důkazu	důkaz	k1gInSc3
vět	věta	k1gFnPc2
Gödel	Gödela	k1gFnPc2
rozvinul	rozvinout	k5eAaPmAgInS
nebo	nebo	k8xC
zcela	zcela	k6eAd1
nově	nově	k6eAd1
vyvinul	vyvinout	k5eAaPmAgInS
několik	několik	k4yIc4
matematických	matematický	k2eAgInPc2d1
postupů	postup	k1gInPc2
či	či	k8xC
technik	technika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
tzv.	tzv.	kA
Gödelovo	Gödelův	k2eAgNnSc1d1
číslování	číslování	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
unikátním	unikátní	k2eAgInSc7d1
kódovacím	kódovací	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
jednoznačný	jednoznačný	k2eAgInSc1d1
převod	převod	k1gInSc1
mezi	mezi	k7c7
formulemi	formule	k1gFnPc7
a	a	k8xC
čísly	číslo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kódování	kódování	k1gNnSc1
spolu	spolu	k6eAd1
se	s	k7c7
zavedením	zavedení	k1gNnSc7
rekurzívních	rekurzívní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
„	„	k?
<g/>
převádí	převádět	k5eAaImIp3nS
logiku	logika	k1gFnSc4
na	na	k7c4
aritmetiku	aritmetika	k1gFnSc4
<g/>
“	“	k?
a	a	k8xC
některé	některý	k3yIgFnPc4
části	část	k1gFnPc4
Gödelova	Gödelův	k2eAgInSc2d1
důkazu	důkaz	k1gInSc2
připomínají	připomínat	k5eAaImIp3nP
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
čemu	co	k3yRnSc3,k3yInSc3,k3yQnSc3
dnes	dnes	k6eAd1
říkáme	říkat	k5eAaImIp1nP
programovací	programovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
počítačů	počítač	k1gMnPc2
(	(	kIx(
<g/>
podobný	podobný	k2eAgInSc1d1
jazyku	jazyk	k1gInSc6
Lisp	Lisp	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Srovnatelný	srovnatelný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
„	„	k?
<g/>
převod	převod	k1gInSc4
geometrie	geometrie	k1gFnSc2
na	na	k7c4
aritmetiku	aritmetika	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
provedl	provést	k5eAaPmAgInS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
René	René	k1gMnSc1
Descartes	Descartes	k1gMnSc1
a	a	k8xC
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
analytická	analytický	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
inovací	inovace	k1gFnSc7
je	být	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgNnSc4d1
použití	použití	k1gNnSc4
Cantorovy	Cantorův	k2eAgFnSc2d1
diagonální	diagonální	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
technik	technika	k1gFnPc2
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
spojenou	spojený	k2eAgFnSc7d1
s	s	k7c7
Cantorovou	Cantorův	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
je	být	k5eAaImIp3nS
postup	postup	k1gInSc1
využívající	využívající	k2eAgInPc1d1
paradoxy	paradox	k1gInPc1
jako	jako	k8xC,k8xS
regulérní	regulérní	k2eAgInPc1d1
matematicko-logické	matematicko-logický	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
v	v	k7c6
logice	logika	k1gFnSc6
hrají	hrát	k5eAaImIp3nP
podobnou	podobný	k2eAgFnSc4d1
roli	role	k1gFnSc4
jako	jako	k8xS,k8xC
Möbiova	Möbiův	k2eAgFnSc1d1
páska	páska	k1gFnSc1
nebo	nebo	k8xC
Kleinova	Kleinův	k2eAgFnSc1d1
láhev	láhev	k1gFnSc1
v	v	k7c6
topologii	topologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gödelovy	Gödelův	k2eAgFnPc1d1
věty	věta	k1gFnPc1
položily	položit	k5eAaPmAgFnP
pevné	pevný	k2eAgInPc4d1
základy	základ	k1gInPc4
matematické	matematický	k2eAgFnSc3d1
logice	logika	k1gFnSc3
<g/>
,	,	kIx,
teorii	teorie	k1gFnSc3
důkazu	důkaz	k1gInSc2
v	v	k7c6
matematice	matematika	k1gFnSc6
<g/>
,	,	kIx,
teorii	teorie	k1gFnSc6
výpočetní	výpočetní	k2eAgFnSc2d1
složitosti	složitost	k1gFnSc2
<g/>
,	,	kIx,
programování	programování	k1gNnSc1
počítačů	počítač	k1gMnPc2
a	a	k8xC
základům	základ	k1gInPc3
matematiky	matematika	k1gFnSc2
skrze	skrze	k?
teorii	teorie	k1gFnSc4
množin	množina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
právě	právě	k6eAd1
rozvinutí	rozvinutí	k1gNnSc1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
Gödel	Gödel	k1gMnSc1
největší	veliký	k2eAgNnSc4d3
úsilí	úsilí	k1gNnSc4
v	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
létech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
úspěšně	úspěšně	k6eAd1
pokusil	pokusit	k5eAaPmAgMnS
prokázat	prokázat	k5eAaPmF
nezávislost	nezávislost	k1gFnSc4
axiomu	axiom	k1gInSc2
výběru	výběr	k1gInSc2
na	na	k7c6
ostatních	ostatní	k2eAgInPc6d1
axiomech	axiom	k1gInPc6
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
a	a	k8xC
jen	jen	k9
částečně	částečně	k6eAd1
úspěšně	úspěšně	k6eAd1
o	o	k7c4
prokázání	prokázání	k1gNnSc4
téhož	týž	k3xTgInSc2
u	u	k7c2
hypotézy	hypotéza	k1gFnSc2
kontinua	kontinuum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdravotní	zdravotní	k2eAgInPc1d1
problémy	problém	k1gInPc1
a	a	k8xC
nešťastné	šťastný	k2eNgFnPc1d1
události	událost	k1gFnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
způsobily	způsobit	k5eAaPmAgInP
změnu	změna	k1gFnSc4
v	v	k7c6
zaměření	zaměření	k1gNnSc6
výzkumu	výzkum	k1gInSc2
a	a	k8xC
první	první	k4xOgNnPc1
léta	léto	k1gNnPc1
v	v	k7c6
Americe	Amerika	k1gFnSc6
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
filozofii	filozofie	k1gFnSc4
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnějšími	významný	k2eAgFnPc7d3
z	z	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
práce	práce	k1gFnPc1
věnující	věnující	k2eAgFnPc1d1
se	se	k3xPyFc4
Russellově	Russellův	k2eAgFnSc3d1
matematické	matematický	k2eAgFnSc3d1
logice	logika	k1gFnSc3
a	a	k8xC
Cantorovu	Cantorův	k2eAgInSc3d1
problému	problém	k1gInSc3
kontinua	kontinuum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Fyzika	fyzika	k1gFnSc1
</s>
<s>
Originálním	originální	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
obohatil	obohatit	k5eAaPmAgMnS
Einsteinovu	Einsteinův	k2eAgFnSc4d1
obecnou	obecný	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
relativity	relativita	k1gFnSc2
formulováním	formulování	k1gNnSc7
a	a	k8xC
nalezením	nalezení	k1gNnSc7
kosmologického	kosmologický	k2eAgInSc2d1
modelu	model	k1gInSc2
rotujícího	rotující	k2eAgInSc2d1
vesmíru	vesmír	k1gInSc2
umožňujícího	umožňující	k2eAgNnSc2d1
cestování	cestování	k1gNnSc2
časem	časem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otevřel	otevřít	k5eAaPmAgMnS
tak	tak	k9
dodnes	dodnes	k6eAd1
probíhající	probíhající	k2eAgFnPc1d1
diskuse	diskuse	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
takové	takový	k3xDgNnSc1
cestování	cestování	k1gNnSc1
neodporuje	odporovat	k5eNaImIp3nS
fyzikálním	fyzikální	k2eAgInPc3d1
či	či	k8xC
filozofickým	filozofický	k2eAgInPc3d1
principům	princip	k1gInPc3
<g/>
,	,	kIx,
popř.	popř.	kA
zda	zda	k8xS
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
technicky	technicky	k6eAd1
realizováno	realizován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
formuloval	formulovat	k5eAaImAgInS
kosmologický	kosmologický	k2eAgInSc1d1
model	model	k1gInSc1
vesmíru	vesmír	k1gInSc2
s	s	k7c7
„	„	k?
<g/>
časovými	časový	k2eAgFnPc7d1
smyčkami	smyčka	k1gFnPc7
<g/>
“	“	k?
<g/>
,	,	kIx,
umožňujícími	umožňující	k2eAgInPc7d1
návrat	návrat	k1gInSc4
do	do	k7c2
vlastní	vlastní	k2eAgFnSc2d1
minulosti	minulost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gInSc6
základě	základ	k1gInSc6
se	se	k3xPyFc4
podrobně	podrobně	k6eAd1
věnoval	věnovat	k5eAaImAgInS,k5eAaPmAgInS
analýze	analýza	k1gFnSc3
pojmu	pojem	k1gInSc2
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecky	vědecky	k6eAd1
významný	významný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
článek	článek	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
popisuje	popisovat	k5eAaImIp3nS
širokou	široký	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
rotujících	rotující	k2eAgInPc2d1
a	a	k8xC
rozpínajících	rozpínající	k2eAgInPc2d1
se	se	k3xPyFc4
vesmírů	vesmír	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Logika	logika	k1gFnSc1
a	a	k8xC
filozofie	filozofie	k1gFnSc1
</s>
<s>
Po	po	k7c6
Einsteinově	Einsteinův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
se	se	k3xPyFc4
obrátil	obrátit	k5eAaPmAgInS
opět	opět	k6eAd1
k	k	k7c3
logice	logika	k1gFnSc3
a	a	k8xC
zajímá	zajímat	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
o	o	k7c4
Husserlovu	Husserlův	k2eAgFnSc4d1
filozofii	filozofie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sklonku	sklonek	k1gInSc6
života	život	k1gInSc2
zpracoval	zpracovat	k5eAaPmAgMnS
v	v	k7c6
podobě	podoba	k1gFnSc6
sledu	sled	k1gInSc2
formulí	formule	k1gFnPc2
s	s	k7c7
minimálním	minimální	k2eAgInSc7d1
komentářem	komentář	k1gInSc7
logický	logický	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
lze	lze	k6eAd1
chápat	chápat	k5eAaImF
jako	jako	k9
zpřesnění	zpřesnění	k1gNnSc4
a	a	k8xC
doplnění	doplnění	k1gNnSc4
úvahy	úvaha	k1gFnSc2
Anselma	Anselmum	k1gNnSc2
z	z	k7c2
Canterbury	Canterbura	k1gFnSc2
označované	označovaný	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
ontologický	ontologický	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
boží	boží	k2eAgFnSc2d1
existence	existence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
„	„	k?
<g/>
Gödelův	Gödelův	k2eAgInSc1d1
ontologický	ontologický	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
<g/>
“	“	k?
byl	být	k5eAaImAgInS
uveřejněn	uveřejněn	k2eAgInSc1d1
až	až	k9
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
předmětem	předmět	k1gInSc7
diskusí	diskuse	k1gFnPc2
logiků	logicus	k1gMnPc2
<g/>
,	,	kIx,
teologů	teolog	k1gMnPc2
i	i	k8xC
filozofů	filozof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Gödel	Gödel	k1gMnSc1
se	se	k3xPyFc4
o	o	k7c6
důkaze	důkaz	k1gInSc6
vyjádřil	vyjádřit	k5eAaPmAgMnS
jako	jako	k9
o	o	k7c6
jistém	jistý	k2eAgNnSc6d1
cvičení	cvičení	k1gNnSc6
použití	použití	k1gNnSc2
moderních	moderní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
modální	modální	k2eAgFnSc2d1
logiky	logika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
má	mít	k5eAaImIp3nS
hluboké	hluboký	k2eAgInPc4d1
filozofické	filozofický	k2eAgInPc4d1
kořeny	kořen	k1gInPc4
<g/>
,	,	kIx,
vedoucí	vedoucí	k2eAgMnPc1d1
až	až	k9
k	k	k7c3
antickým	antický	k2eAgInPc3d1
základům	základ	k1gInPc3
vzdělanosti	vzdělanost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
potenciál	potenciál	k1gInSc1
nebyl	být	k5eNaImAgInS
zdaleka	zdaleka	k6eAd1
vyčerpán	vyčerpat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
účelem	účel	k1gInSc7
rozvíjení	rozvíjení	k1gNnSc2
a	a	k8xC
popularizace	popularizace	k1gFnSc2
díla	dílo	k1gNnSc2
Kurta	Kurt	k1gMnSc2
Gödela	Gödel	k1gMnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
založena	založit	k5eAaPmNgFnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
mezinárodní	mezinárodní	k2eAgInSc1d1
Kurt	kurt	k1gInSc1
Gödel	Gödela	k1gFnPc2
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
jednou	jeden	k4xCgFnSc7
odnoží	odnož	k1gFnSc7
je	být	k5eAaImIp3nS
Společnost	společnost	k1gFnSc1
Kurta	Kurt	k1gMnSc2
Gödela	Gödel	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
z	z	k7c2
publikací	publikace	k1gFnPc2
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
:	:	kIx,
Über	Über	k1gMnSc1
die	die	k?
Vollständigkeit	Vollständigkeit	k1gMnSc1
der	drát	k5eAaImRp2nS
Axiome	axiom	k1gInSc5
des	des	k1gNnSc6
logischen	logischen	k2eAgInSc4d1
Funktionenkalküls	Funktionenkalküls	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dissertation	Dissertation	k1gInSc1
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Monatshefte	Monatsheft	k1gInSc5
für	für	k?
Mathematik	Mathematik	k1gMnSc1
und	und	k?
Physik	Physik	k1gMnSc1
Akademische	Akademische	k1gNnSc1
Verlagsgesellschaft	Verlagsgesellschaft	k1gMnSc1
<g/>
,	,	kIx,
Leipzig	Leipzig	k1gMnSc1
36.193	36.193	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
349	#num#	k4
<g/>
–	–	k?
<g/>
360	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Auch	Auch	k1gInSc1
in	in	k?
<g/>
:	:	kIx,
Erg	erg	k1gInSc1
<g/>
.	.	kIx.
3.193	3.193	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
12	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
:	:	kIx,
Über	Über	k1gMnSc1
formal	formal	k1gMnSc1
unentscheidbare	unentscheidbar	k1gMnSc5
Sätze	Sätze	k1gFnPc1
der	drát	k5eAaImRp2nS
Principia	principium	k1gNnSc2
Mathematica	Mathematica	k1gMnSc1
und	und	k?
verwandter	verwandter	k1gMnSc1
Systeme	Systeme	k1gMnSc1
I.	I.	kA
in	in	k?
<g/>
:	:	kIx,
Monatshefte	Monatsheft	k1gInSc5
für	für	k?
Mathematik	Mathematik	k1gMnSc1
und	und	k?
Physik	Physik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademische	Akademische	k1gInSc1
Verlagsgesellschaft	Verlagsgesellschaft	k2eAgInSc1d1
<g/>
,	,	kIx,
Leipzig	Leipzig	k1gInSc1
38.193	38.193	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
173	#num#	k4
<g/>
–	–	k?
<g/>
198	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
:	:	kIx,
Diskussion	Diskussion	k1gInSc1
zur	zur	k?
Grundlegung	Grundlegung	k1gInSc1
der	drát	k5eAaImRp2nS
Mathematik	Mathematik	k1gMnSc1
<g/>
,	,	kIx,
Erkenntnis	Erkenntnis	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
in	in	k?
<g/>
:	:	kIx,
Monatshefte	Monatsheft	k1gInSc5
für	für	k?
Mathematik	Mathematik	k1gMnSc1
und	und	k?
Physik	Physik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademische	Akademische	k1gInSc1
Verlagsgesellschaft	Verlagsgesellschaft	k2eAgInSc1d1
<g/>
,	,	kIx,
Leipzig	Leipzig	k1gInSc1
39.193	39.193	k4
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
147	#num#	k4
<g/>
–	–	k?
<g/>
148	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Consistency	Consistenca	k1gFnSc2
of	of	k?
the	the	k?
Axiom	axiom	k1gInSc1
of	of	k?
Choice	Choice	k1gFnSc2
and	and	k?
of	of	k?
the	the	k?
Generalized	Generalized	k1gInSc1
Continuum	Continuum	k1gNnSc1
Hypothesis	Hypothesis	k1gInSc1
with	with	k1gMnSc1
the	the	k?
Axioms	Axioms	k1gInSc1
of	of	k?
Set	set	k1gInSc1
Theory	Theora	k1gFnSc2
<g/>
,	,	kIx,
Annals	Annalsa	k1gFnPc2
of	of	k?
Mathematical	Mathematical	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
3	#num#	k4
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc1
<g/>
,	,	kIx,
NJ	NJ	kA
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
:	:	kIx,
Russels	Russels	k1gInSc1
mathematische	mathematische	k1gNnSc2
Logik	logika	k1gFnPc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Whitehead	Whitehead	k1gInSc1
<g/>
/	/	kIx~
<g/>
Russell	Russell	k1gInSc1
<g/>
,	,	kIx,
Principia	principium	k1gNnPc1
Mathematica	Mathematica	k1gFnSc1
<g/>
,	,	kIx,
Vorwort	Vorwort	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
V	v	k7c6
<g/>
–	–	k?
<g/>
XXXIV	XXXIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suhrkamp	Suhrkamp	k1gInSc1
1986	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
518	#num#	k4
<g/>
-	-	kIx~
<g/>
28193	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
:	:	kIx,
My	my	k3xPp1nPc1
philosophical	philosophicat	k5eAaPmAgInS
viewpoint	viewpoint	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
1960	#num#	k4
<g/>
,	,	kIx,
unpublished	unpublished	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
modern	modern	k1gMnSc1
development	development	k1gMnSc1
of	of	k?
the	the	k?
foundations	foundations	k1gInSc1
of	of	k?
mathematics	mathematics	k1gInSc1
in	in	k?
the	the	k?
light	light	k1gInSc1
of	of	k?
philosophy	philosopha	k1gFnSc2
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
,	,	kIx,
unpublished	unpublished	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Hrsg	Hrsg	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Solomon	Solomon	k1gMnSc1
Feferman	Feferman	k1gMnSc1
u.	u.	k?
a.	a.	k?
<g/>
:	:	kIx,
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Collected	Collected	k1gInSc1
Works	Works	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
<g/>
–	–	k?
<g/>
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clarendon	Clarendon	k1gInSc1
Press	Press	k1gInSc4
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc4
1986	#num#	k4
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
514720	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
514721	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
ISBN	ISBN	kA
0-19-514722-7	0-19-514722-7	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc1d1
zápis	zápis	k1gInSc1
německé	německý	k2eAgFnSc2d1
evangelické	evangelický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
Brno	Brno	k1gNnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hilbertův	Hilbertův	k2eAgInSc1d1
program	program	k1gInSc1
</s>
<s>
Gödelova	Gödelův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
o	o	k7c6
úplnosti	úplnost	k1gFnSc6
predikátové	predikátový	k2eAgFnSc2d1
logiky	logika	k1gFnSc2
</s>
<s>
Gödelovy	Gödelův	k2eAgFnPc1d1
věty	věta	k1gFnPc1
o	o	k7c6
neúplnosti	neúplnost	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Kurt	kurta	k1gFnPc2
Gödel	Gödela	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kurt	kurta	k1gFnPc2
Gödel	Gödlo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
Axiomy	axiom	k1gInPc7
</s>
<s>
axiom	axiom	k1gInSc1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc4
spočetného	spočetný	k2eAgInSc2d1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc4
závislého	závislý	k2eAgInSc2d1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc1
extenzionality	extenzionalita	k1gFnSc2
•	•	k?
axiom	axiom	k1gInSc1
nekonečna	nekonečno	k1gNnSc2
•	•	k?
axiom	axiom	k1gInSc1
dvojice	dvojice	k1gFnSc1
•	•	k?
axiom	axiom	k1gInSc4
potenční	potenční	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
•	•	k?
Axiom	axiom	k1gInSc1
regulárnosti	regulárnost	k1gFnSc2
•	•	k?
axiom	axiom	k1gInSc1
sumy	suma	k1gFnSc2
•	•	k?
schéma	schéma	k1gNnSc1
nahrazení	nahrazení	k1gNnSc2
•	•	k?
schéma	schéma	k1gNnSc1
axiomů	axiom	k1gInPc2
vydělení	vydělení	k1gNnSc2
•	•	k?
hypotéza	hypotéza	k1gFnSc1
kontinua	kontinuum	k1gNnSc2
•	•	k?
Martinův	Martinův	k2eAgInSc4d1
axiom	axiom	k1gInSc4
•	•	k?
velké	velký	k2eAgMnPc4d1
kardinály	kardinál	k1gMnPc4
Množinové	množinový	k2eAgFnSc2d1
operace	operace	k1gFnSc2
</s>
<s>
doplněk	doplněk	k1gInSc1
•	•	k?
de	de	k?
Morganovy	morganův	k2eAgInPc4d1
zákony	zákon	k1gInPc4
•	•	k?
kartézský	kartézský	k2eAgInSc1d1
součin	součin	k1gInSc1
•	•	k?
potenční	potenční	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
průnik	průnik	k1gInSc1
•	•	k?
rozdíl	rozdíl	k1gInSc1
množin	množina	k1gFnPc2
•	•	k?
sjednocení	sjednocení	k1gNnSc6
•	•	k?
symetrická	symetrický	k2eAgFnSc1d1
diference	diference	k1gFnSc1
Koncepty	koncept	k1gInPc1
</s>
<s>
bijekce	bijekce	k1gFnSc1
•	•	k?
kardinální	kardinální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
•	•	k?
konstruovatelná	konstruovatelný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
mohutnost	mohutnost	k1gFnSc1
•	•	k?
ordinální	ordinální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
•	•	k?
prvek	prvek	k1gInSc1
množiny	množina	k1gFnSc2
•	•	k?
rodina	rodina	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
transfinitní	transfinitní	k2eAgFnSc2d1
indukce	indukce	k1gFnSc2
•	•	k?
třída	třída	k1gFnSc1
•	•	k?
Vennův	Vennův	k2eAgInSc1d1
diagram	diagram	k1gInSc1
Množiny	množina	k1gFnSc2
</s>
<s>
Cantorova	Cantorův	k2eAgFnSc1d1
diagonální	diagonální	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
•	•	k?
fuzzy	fuzza	k1gFnSc2
množina	množina	k1gFnSc1
•	•	k?
konečná	konečný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
nekonečná	konečný	k2eNgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
nespočetná	spočetný	k2eNgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
podmnožina	podmnožina	k1gFnSc1
•	•	k?
prázdná	prázdný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
rekurzivní	rekurzivní	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
spočetná	spočetný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
tranzitivní	tranzitivní	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
univerzální	univerzální	k2eAgFnSc1d1
množina	množina	k1gFnSc1
Teorie	teorie	k1gFnSc1
</s>
<s>
axiomatická	axiomatický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Cantorova	Cantorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
•	•	k?
forsing	forsing	k1gInSc1
•	•	k?
Kelleyova	Kelleyov	k1gInSc2
<g/>
–	–	k?
<g/>
Morseova	Morseův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
naivní	naivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
•	•	k?
New	New	k1gFnSc2
Foundations	Foundationsa	k1gFnPc2
•	•	k?
paradoxy	paradox	k1gInPc1
naivní	naivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
•	•	k?
Principia	principium	k1gNnSc2
Mathematica	Mathematicus	k1gMnSc2
•	•	k?
Russellův	Russellův	k2eAgInSc4d1
paradox	paradox	k1gInSc4
•	•	k?
Suslinova	Suslinův	k2eAgFnSc1d1
hypotéza	hypotéza	k1gFnSc1
•	•	k?
Von	von	k1gInSc1
Neumannova	Neumannův	k2eAgInSc2d1
<g/>
–	–	k?
<g/>
Bernaysova	Bernaysův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Gödelova	Gödelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Zermelova	Zermelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Zermelova	Zermelův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Fraenkelova	Fraenkelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
alternativní	alternativní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
Lidé	člověk	k1gMnPc1
</s>
<s>
Abraham	Abraham	k1gMnSc1
Fraenkel	Fraenkel	k1gMnSc1
•	•	k?
Bertrand	Bertrand	k1gInSc1
Russell	Russell	k1gMnSc1
•	•	k?
Ernst	Ernst	k1gMnSc1
Zermelo	Zermela	k1gFnSc5
•	•	k?
Georg	Georg	k1gMnSc1
Cantor	Cantor	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
von	von	k1gInSc1
Neumann	Neumann	k1gMnSc1
•	•	k?
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
•	•	k?
Lotfi	Lotf	k1gFnSc2
Zadeh	Zadeh	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Bernays	Bernaysa	k1gFnPc2
•	•	k?
Paul	Paul	k1gMnSc1
Cohen	Cohen	k2eAgMnSc1d1
•	•	k?
Petr	Petr	k1gMnSc1
Vopěnka	Vopěnka	k1gFnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Dedekind	Dedekind	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Jech	Jech	k1gMnSc1
•	•	k?
Willard	Willard	k1gMnSc1
Quine	Quin	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
|	|	kIx~
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000602196	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
11869569X	11869569X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1031	#num#	k4
567X	567X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79007770	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
97851774	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79007770	#num#	k4
</s>
