<s>
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1906	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
matematik	matematik	k1gMnSc1
rakouského	rakouský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
logiků	logicus	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>