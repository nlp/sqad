<s>
Mont	Mont	k2eAgMnSc1d1
Blanc	Blanc	k1gMnSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
nejednotné	jednotný	k2eNgFnSc2d1
metodiky	metodika	k1gFnSc2
pro	pro	k7c4
přesné	přesný	k2eAgNnSc4d1
určení	určení	k1gNnSc4
evropsko-asijské	evropskosijský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
titul	titul	k1gInSc1
připisován	připisovat	k5eAaImNgInS
i	i	k8xC
kavkazské	kavkazský	k2eAgNnSc1d1
hoře	hoře	k1gNnSc1
Elbrus	Elbrus	k1gInSc1
(	(	kIx(
<g/>
5642	[number]	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>