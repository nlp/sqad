<s>
Schutzstaffel	Schutzstaffel	k1gInSc1	Schutzstaffel
(	(	kIx(	(
<g/>
známější	známý	k2eAgInSc1d2	známější
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SS	SS	kA	SS
nebo	nebo	k8xC	nebo
runami	runa	k1gFnPc7	runa
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ochranný	ochranný	k2eAgInSc4d1	ochranný
oddíl	oddíl	k1gInSc4	oddíl
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
organizace	organizace	k1gFnSc1	organizace
NSDAP	NSDAP	kA	NSDAP
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
z	z	k7c2	z
horlivě	horlivě	k6eAd1	horlivě
oddaných	oddaný	k2eAgMnPc2d1	oddaný
přívrženců	přívrženec	k1gMnPc2	přívrženec
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
působících	působící	k2eAgNnPc2d1	působící
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
osobní	osobní	k2eAgFnSc1d1	osobní
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
SS	SS	kA	SS
v	v	k7c6	v
době	doba	k1gFnSc6	doba
její	její	k3xOp3gFnSc2	její
největší	veliký	k2eAgFnSc2d3	veliký
moci	moc	k1gFnSc2	moc
stál	stát	k5eAaImAgMnS	stát
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
jako	jako	k8xC	jako
Reichsführer-SS	Reichsführer-SS	k1gMnSc1	Reichsführer-SS
(	(	kIx(	(
<g/>
říšský	říšský	k2eAgMnSc1d1	říšský
vůdce	vůdce	k1gMnSc1	vůdce
SS	SS	kA	SS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
si	se	k3xPyFc3	se
organizace	organizace	k1gFnSc1	organizace
podřizovala	podřizovat	k5eAaImAgFnS	podřizovat
veškerý	veškerý	k3xTgInSc4	veškerý
státní	státní	k2eAgInSc4d1	státní
i	i	k8xC	i
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
aparát	aparát	k1gInSc4	aparát
nacistického	nacistický	k2eAgInSc2d1	nacistický
státu	stát	k1gInSc2	stát
včetně	včetně	k7c2	včetně
vyhlazovací	vyhlazovací	k2eAgFnSc2d1	vyhlazovací
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
symbolem	symbol	k1gInSc7	symbol
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
starogermánské	starogermánský	k2eAgInPc1d1	starogermánský
runové	runový	k2eAgInPc1d1	runový
znaky	znak	k1gInPc1	znak
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
černém	černé	k1gNnSc6	černé
pozadí	pozadí	k1gNnSc2	pozadí
(	(	kIx(	(
<g/>
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomínají	připomínat	k5eAaImIp3nP	připomínat
písmeno	písmeno	k1gNnSc4	písmeno
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgMnS	být
oddíl	oddíl	k1gInSc4	oddíl
SA	SA	kA	SA
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Stabswache	Stabswache	k1gInSc4	Stabswache
(	(	kIx(	(
<g/>
Štábní	štábní	k2eAgFnSc1d1	štábní
stráž	stráž	k1gFnSc1	stráž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřízený	zřízený	k2eAgInSc1d1	zřízený
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1923	[number]	k4	1923
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
8	[number]	k4	8
mužů	muž	k1gMnPc2	muž
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Emila	Emil	k1gMnSc2	Emil
Mauriceho	Maurice	k1gMnSc2	Maurice
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
chránit	chránit	k5eAaImF	chránit
vedení	vedení	k1gNnSc4	vedení
NSDAP	NSDAP	kA	NSDAP
při	při	k7c6	při
veřejných	veřejný	k2eAgFnPc6d1	veřejná
akcích	akce	k1gFnPc6	akce
(	(	kIx(	(
<g/>
následně	následně	k6eAd1	následně
přejmenovaný	přejmenovaný	k2eAgInSc1d1	přejmenovaný
na	na	k7c4	na
Stoßtrupp	Stoßtrupp	k1gInSc4	Stoßtrupp
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
o	o	k7c4	o
další	další	k2eAgMnPc4d1	další
čtyři	čtyři	k4xCgMnPc4	čtyři
muže	muž	k1gMnPc4	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mnichovském	mnichovský	k2eAgInSc6d1	mnichovský
puči	puč	k1gInSc6	puč
a	a	k8xC	a
zákazu	zákaz	k1gInSc2	zákaz
NSDAP	NSDAP	kA	NSDAP
byl	být	k5eAaImAgInS	být
oddíl	oddíl	k1gInSc1	oddíl
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Dnem	dnem	k7c2	dnem
vzniku	vznik	k1gInSc2	vznik
SS	SS	kA	SS
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
elitní	elitní	k2eAgFnSc1d1	elitní
bojová	bojový	k2eAgFnSc1d1	bojová
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
složka	složka	k1gFnSc1	složka
Schutzstaffel	Schutzstaffel	k1gMnSc1	Schutzstaffel
(	(	kIx(	(
<g/>
SS	SS	kA	SS
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Julia	Julius	k1gMnSc2	Julius
Schrecka	Schrecka	k1gFnSc1	Schrecka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
skupinky	skupinka	k1gFnSc2	skupinka
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
vedené	vedený	k2eAgNnSc1d1	vedené
stále	stále	k6eAd1	stále
jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
oddíl	oddíl	k1gInSc1	oddíl
SA	SA	kA	SA
<g/>
)	)	kIx)	)
postupně	postupně	k6eAd1	postupně
rostla	růst	k5eAaImAgFnS	růst
organizace	organizace	k1gFnSc1	organizace
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
hierarchií	hierarchie	k1gFnSc7	hierarchie
se	se	k3xPyFc4	se
sklony	sklon	k1gInPc1	sklon
k	k	k7c3	k
násilnickému	násilnický	k2eAgNnSc3d1	násilnické
zastrašování	zastrašování	k1gNnSc3	zastrašování
oponentů	oponent	k1gMnPc2	oponent
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1929	[number]	k4	1929
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
čela	čelo	k1gNnSc2	čelo
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1929	[number]	k4	1929
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
čtyřdenní	čtyřdenní	k2eAgInSc1d1	čtyřdenní
sjezd	sjezd	k1gInSc1	sjezd
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vznesen	vznesen	k2eAgInSc4d1	vznesen
požadavek	požadavek	k1gInSc4	požadavek
zavedení	zavedení	k1gNnSc2	zavedení
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
branné	branný	k2eAgFnSc2d1	Branná
povinnosti	povinnost	k1gFnSc2	povinnost
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
zakončoval	zakončovat	k5eAaImAgInS	zakončovat
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
pochod	pochod	k1gInSc1	pochod
oddílů	oddíl	k1gInPc2	oddíl
SS	SS	kA	SS
a	a	k8xC	a
SA	SA	kA	SA
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
60	[number]	k4	60
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Průvod	průvod	k1gInSc1	průvod
se	se	k3xPyFc4	se
neobešel	obešet	k5eNaPmAgInS	obešet
bez	bez	k7c2	bez
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
pouličních	pouliční	k2eAgFnPc2d1	pouliční
bitek	bitka	k1gFnPc2	bitka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
další	další	k2eAgFnSc4d1	další
propagaci	propagace	k1gFnSc4	propagace
síly	síla	k1gFnSc2	síla
SS	SS	kA	SS
respektive	respektive	k9	respektive
celé	celá	k1gFnPc1	celá
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
Rozrůstající	rozrůstající	k2eAgNnSc1d1	rozrůstající
se	se	k3xPyFc4	se
násilí	násilí	k1gNnSc1	násilí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
oddílů	oddíl	k1gInPc2	oddíl
SA	SA	kA	SA
a	a	k8xC	a
SS	SS	kA	SS
vedlo	vést	k5eAaImAgNnS	vést
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1932	[number]	k4	1932
německou	německý	k2eAgFnSc4d1	německá
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
nouzového	nouzový	k2eAgNnSc2d1	nouzové
nařízení	nařízení	k1gNnSc2	nařízení
"	"	kIx"	"
<g/>
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
státní	státní	k2eAgFnSc2d1	státní
autority	autorita	k1gFnSc2	autorita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
činnost	činnost	k1gFnSc1	činnost
organizací	organizace	k1gFnPc2	organizace
SA	SA	kA	SA
a	a	k8xC	a
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
legální	legální	k2eAgFnSc2d1	legální
činnosti	činnost	k1gFnSc2	činnost
SA	SA	kA	SA
a	a	k8xC	a
SS	SS	kA	SS
se	se	k3xPyFc4	se
nacistů	nacista	k1gMnPc2	nacista
dotkl	dotknout	k5eAaPmAgInS	dotknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
budou	být	k5eAaImBp3nP	být
zákony	zákon	k1gInPc1	zákon
o	o	k7c6	o
zákazu	zákaz	k1gInSc6	zákaz
jeho	jeho	k3xOp3gMnPc2	jeho
úderných	úderný	k2eAgMnPc2d1	úderný
oddílů	oddíl	k1gInPc2	oddíl
zrušeny	zrušen	k2eAgFnPc1d1	zrušena
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
respektovat	respektovat	k5eAaImF	respektovat
a	a	k8xC	a
tolerovat	tolerovat	k5eAaImF	tolerovat
Papenův	Papenův	k2eAgInSc4d1	Papenův
kabinet	kabinet	k1gInSc4	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
SA	SA	kA	SA
a	a	k8xC	a
SS	SS	kA	SS
však	však	k9	však
i	i	k9	i
nadále	nadále	k6eAd1	nadále
přetrvávaly	přetrvávat	k5eAaImAgFnP	přetrvávat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
vládní	vládní	k2eAgFnSc3d1	vládní
komisi	komise	k1gFnSc3	komise
pro	pro	k7c4	pro
Sársko	Sársko	k1gNnSc4	Sársko
<g/>
,	,	kIx,	,
že	že	k8xS	že
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1932	[number]	k4	1932
nařídila	nařídit	k5eAaPmAgFnS	nařídit
rozpustit	rozpustit	k5eAaPmF	rozpustit
formaci	formace	k1gFnSc6	formace
obou	dva	k4xCgNnPc2	dva
nacistických	nacistický	k2eAgNnPc2d1	nacistické
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1933	[number]	k4	1933
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
úřadovny	úřadovna	k1gFnSc2	úřadovna
byly	být	k5eAaImAgInP	být
masově	masově	k6eAd1	masově
obsazovány	obsazován	k2eAgInPc4d1	obsazován
členy	člen	k1gInPc4	člen
oddílů	oddíl	k1gInPc2	oddíl
SA	SA	kA	SA
a	a	k8xC	a
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
vnitrostranické	vnitrostranický	k2eAgFnPc4d1	vnitrostranická
krize	krize	k1gFnPc4	krize
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
zbavil	zbavit	k5eAaPmAgMnS	zbavit
svých	svůj	k3xOyFgMnPc2	svůj
dlouhodobých	dlouhodobý	k2eAgMnPc2d1	dlouhodobý
oponentů	oponent	k1gMnPc2	oponent
včetně	včetně	k7c2	včetně
osob	osoba	k1gFnPc2	osoba
nepřijatelných	přijatelný	k2eNgFnPc2d1	nepřijatelná
pro	pro	k7c4	pro
špičky	špička	k1gFnPc4	špička
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
upevnil	upevnit	k5eAaPmAgInS	upevnit
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
i	i	k9	i
navenek	navenek	k6eAd1	navenek
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Noc	noc	k1gFnSc1	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
k	k	k7c3	k
vyvraždění	vyvraždění	k1gNnSc3	vyvraždění
vrcholných	vrcholný	k2eAgMnPc2d1	vrcholný
příslušníků	příslušník	k1gMnPc2	příslušník
SA	SA	kA	SA
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
vraždění	vraždění	k1gNnSc4	vraždění
měly	mít	k5eAaImAgInP	mít
oddíly	oddíl	k1gInPc1	oddíl
SS	SS	kA	SS
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
odměněny	odměnit	k5eAaPmNgInP	odměnit
vyřazením	vyřazení	k1gNnSc7	vyřazení
ze	z	k7c2	z
svazku	svazek	k1gInSc2	svazek
SA	SA	kA	SA
a	a	k8xC	a
postavením	postavení	k1gNnSc7	postavení
samostatné	samostatný	k2eAgFnSc2d1	samostatná
složky	složka	k1gFnSc2	složka
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
podléhající	podléhající	k2eAgMnPc1d1	podléhající
pouze	pouze	k6eAd1	pouze
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1935	[number]	k4	1935
převzaly	převzít	k5eAaPmAgFnP	převzít
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
strážní	strážní	k2eAgFnPc1d1	strážní
a	a	k8xC	a
dozorčí	dozorčí	k2eAgFnSc4d1	dozorčí
službu	služba	k1gFnSc4	služba
ve	v	k7c6	v
zřízených	zřízený	k2eAgInPc6d1	zřízený
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
pozvolna	pozvolna	k6eAd1	pozvolna
začaly	začít	k5eAaPmAgFnP	začít
naplňovat	naplňovat	k5eAaImF	naplňovat
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
o	o	k7c4	o
nastolení	nastolení	k1gNnSc4	nastolení
konečného	konečný	k2eAgNnSc2d1	konečné
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
šéfa	šéf	k1gMnSc2	šéf
německé	německý	k2eAgFnSc2d1	německá
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
položen	položen	k2eAgInSc4d1	položen
základ	základ	k1gInSc4	základ
struktury	struktura	k1gFnSc2	struktura
SS	SS	kA	SS
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
SS	SS	kA	SS
rozčleněny	rozčlenit	k5eAaPmNgInP	rozčlenit
na	na	k7c4	na
<g/>
:	:	kIx,	:
Allgemeine-SS	Allgemeine-SS	k1gFnSc4	Allgemeine-SS
-	-	kIx~	-
původní	původní	k2eAgMnSc1d1	původní
teritoriálně	teritoriálně	k6eAd1	teritoriálně
členěné	členěný	k2eAgFnSc6d1	členěná
SS	SS	kA	SS
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
doby	doba	k1gFnPc1	doba
staly	stát	k5eAaPmAgFnP	stát
jakousi	jakýsi	k3yIgFnSc7	jakýsi
rezervou	rezerva	k1gFnSc7	rezerva
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
složky	složka	k1gFnPc4	složka
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
-	-	kIx~	-
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
jako	jako	k8xC	jako
stranická	stranický	k2eAgFnSc1d1	stranická
paralela	paralela	k1gFnSc1	paralela
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g />
.	.	kIx.	.
</s>
<s>
SS-Totenkopfverbände	SS-Totenkopfverbänd	k1gMnSc5	SS-Totenkopfverbänd
-	-	kIx~	-
oddíly	oddíl	k1gInPc1	oddíl
strážní	strážní	k2eAgFnSc2d1	strážní
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
policejní	policejní	k2eAgFnSc2d1	policejní
jednotky	jednotka	k1gFnSc2	jednotka
-	-	kIx~	-
kromě	kromě	k7c2	kromě
samostatné	samostatný	k2eAgFnSc2d1	samostatná
pořádkové	pořádkový	k2eAgFnSc2d1	pořádková
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
Ordnungspolizei	Ordnungspolize	k1gFnSc2	Ordnungspolize
-	-	kIx~	-
Orpo	Orpo	k6eAd1	Orpo
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
SS	SS	kA	SS
i	i	k8xC	i
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
Sicherheitspolizei	Sicherheitspolize	k1gFnSc2	Sicherheitspolize
-	-	kIx~	-
Sipo	Sipo	k6eAd1	Sipo
<g/>
)	)	kIx)	)
zahrnující	zahrnující	k2eAgFnSc3d1	zahrnující
kriminální	kriminální	k2eAgFnSc3d1	kriminální
policii	policie	k1gFnSc3	policie
(	(	kIx(	(
<g/>
Kriminalpolizei	Kriminalpolizei	k1gNnSc1	Kriminalpolizei
-	-	kIx~	-
Kripo	kripo	k1gNnSc1	kripo
<g/>
)	)	kIx)	)
a	a	k8xC	a
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Sipo	Sipo	k6eAd1	Sipo
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
stranickou	stranický	k2eAgFnSc7d1	stranická
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
službou	služba	k1gFnSc7	služba
Sicherheitsdienst	Sicherheitsdienst	k1gMnSc1	Sicherheitsdienst
v	v	k7c6	v
RSHA	RSHA	kA	RSHA
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Hodnosti	hodnost	k1gFnSc3	hodnost
SS	SS	kA	SS
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
klasickým	klasický	k2eAgFnPc3d1	klasická
vojenským	vojenský	k2eAgFnPc3d1	vojenská
hodnostem	hodnost	k1gFnPc3	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
srovnány	srovnán	k2eAgInPc4d1	srovnán
s	s	k7c7	s
wehrmachtem	wehrmacht	k1gInSc7	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Hlavního	hlavní	k2eAgInSc2d1	hlavní
úřadu	úřad	k1gInSc2	úřad
SS	SS	kA	SS
(	(	kIx(	(
<g/>
šéfoval	šéfovat	k5eAaImAgMnS	šéfovat
mu	on	k3xPp3gInSc3	on
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gInSc1	Himmler
<g/>
)	)	kIx)	)
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
"	"	kIx"	"
<g/>
Inspektorat	Inspektorat	k1gMnSc1	Inspektorat
Verfugungstruppen	Verfugungstruppen	k2eAgMnSc1d1	Verfugungstruppen
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Paula	Paul	k1gMnSc2	Paul
Haussera	Hausser	k1gMnSc2	Hausser
-	-	kIx~	-
bývalého	bývalý	k2eAgMnSc2d1	bývalý
generála	generál	k1gMnSc2	generál
Reichswehru	Reichswehra	k1gFnSc4	Reichswehra
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgNnSc7	svůj
jmenováním	jmenování	k1gNnSc7	jmenování
SS	SS	kA	SS
brigadefuhrerem	brigadefuhrero	k1gNnSc7	brigadefuhrero
se	se	k3xPyFc4	se
Hausser	Hausser	k1gInSc1	Hausser
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
hlavním	hlavní	k2eAgMnSc7d1	hlavní
inspektorem	inspektor	k1gMnSc7	inspektor
Verfügungstruppen	Verfügungstruppen	k2eAgMnSc1d1	Verfügungstruppen
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
stránce	stránka	k1gFnSc6	stránka
slabě	slabě	k6eAd1	slabě
vyškolené	vyškolený	k2eAgFnPc4d1	vyškolená
jednotky	jednotka	k1gFnPc4	jednotka
SS	SS	kA	SS
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
početní	početní	k2eAgInSc1d1	početní
stav	stav	k1gInSc1	stav
tehdy	tehdy	k6eAd1	tehdy
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
asi	asi	k9	asi
8500	[number]	k4	8500
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
přeměnit	přeměnit	k5eAaPmF	přeměnit
v	v	k7c6	v
dokonale	dokonale	k6eAd1	dokonale
vycvičené	vycvičený	k2eAgFnSc6d1	vycvičená
a	a	k8xC	a
moderními	moderní	k2eAgFnPc7d1	moderní
zbraněmi	zbraň	k1gFnPc7	zbraň
vybavené	vybavený	k2eAgInPc1d1	vybavený
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
přísným	přísný	k2eAgNnPc3d1	přísné
kritériím	kritérion	k1gNnPc3	kritérion
nacistického	nacistický	k2eAgInSc2d1	nacistický
světového	světový	k2eAgInSc2d1	světový
názoru	názor	k1gInSc2	názor
a	a	k8xC	a
příslušným	příslušný	k2eAgInPc3d1	příslušný
fyzickým	fyzický	k2eAgInPc3d1	fyzický
požadavkům	požadavek	k1gInPc3	požadavek
(	(	kIx(	(
<g/>
minimální	minimální	k2eAgFnSc1d1	minimální
osobní	osobní	k2eAgFnSc1d1	osobní
výška	výška	k1gFnSc1	výška
pro	pro	k7c4	pro
Leibstandarte	Leibstandart	k1gInSc5	Leibstandart
činila	činit	k5eAaImAgFnS	činit
180	[number]	k4	180
cm	cm	kA	cm
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
jednotky	jednotka	k1gFnPc4	jednotka
175	[number]	k4	175
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
služby	služba	k1gFnSc2	služba
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
pro	pro	k7c4	pro
řadové	řadový	k2eAgMnPc4d1	řadový
příslušníky	příslušník	k1gMnPc4	příslušník
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
poddůstojníky	poddůstojník	k1gMnPc4	poddůstojník
na	na	k7c4	na
12	[number]	k4	12
let	léto	k1gNnPc2	léto
a	a	k8xC	a
pro	pro	k7c4	pro
důstojnický	důstojnický	k2eAgInSc4d1	důstojnický
sbor	sbor	k1gInSc4	sbor
na	na	k7c4	na
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Otevřenou	otevřený	k2eAgFnSc4d1	otevřená
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
útvarů	útvar	k1gInPc2	útvar
měli	mít	k5eAaImAgMnP	mít
především	především	k9	především
vzorní	vzorní	k2eAgMnPc1d1	vzorní
příslušníci	příslušník	k1gMnPc1	příslušník
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byly	být	k5eAaImAgInP	být
součástí	součást	k1gFnSc7	součást
Verfügungstruppen	Verfügungstruppen	k2eAgInSc1d1	Verfügungstruppen
tři	tři	k4xCgFnPc4	tři
divize	divize	k1gFnPc1	divize
<g/>
:	:	kIx,	:
Germania	germanium	k1gNnPc1	germanium
<g/>
,	,	kIx,	,
Leibstandarte	Leibstandart	k1gInSc5	Leibstandart
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
.	.	kIx.	.
</s>
<s>
Důstojnické	důstojnický	k2eAgInPc4d1	důstojnický
kádry	kádr	k1gInPc4	kádr
SS	SS	kA	SS
se	se	k3xPyFc4	se
školily	školit	k5eAaImAgInP	školit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
školách	škola	k1gFnPc6	škola
<g/>
:	:	kIx,	:
Junkerschule	Junkerschule	k1gFnSc1	Junkerschule
"	"	kIx"	"
<g/>
Braunschwedig	Braunschwedig	k1gInSc1	Braunschwedig
<g/>
"	"	kIx"	"
a	a	k8xC	a
Junkerschule	Junkerschule	k1gFnSc1	Junkerschule
"	"	kIx"	"
<g/>
Bad	Bad	k1gMnSc1	Bad
Toltz	Toltz	k1gMnSc1	Toltz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ročně	ročně	k6eAd1	ročně
vychovávaly	vychovávat	k5eAaImAgFnP	vychovávat
celkem	celek	k1gInSc7	celek
400	[number]	k4	400
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
prokazovaly	prokazovat	k5eAaImAgFnP	prokazovat
velkou	velký	k2eAgFnSc4d1	velká
zmužilost	zmužilost	k1gFnSc4	zmužilost
příznačnou	příznačný	k2eAgFnSc4d1	příznačná
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
ctily	ctít	k5eAaImAgInP	ctít
pravidla	pravidlo	k1gNnPc4	pravidlo
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
způsob	způsob	k1gInSc1	způsob
boje	boj	k1gInSc2	boj
byl	být	k5eAaImAgInS	být
leckdy	leckdy	k6eAd1	leckdy
brutální	brutální	k2eAgMnSc1d1	brutální
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
velitelství	velitelství	k1gNnSc1	velitelství
armády	armáda	k1gFnSc2	armáda
zpočátku	zpočátku	k6eAd1	zpočátku
neprojevovalo	projevovat	k5eNaImAgNnS	projevovat
nad	nad	k7c7	nad
výkony	výkon	k1gInPc7	výkon
SS	SS	kA	SS
velké	velký	k2eAgNnSc1d1	velké
nadšení	nadšení	k1gNnSc1	nadšení
a	a	k8xC	a
raději	rád	k6eAd2	rád
se	se	k3xPyFc4	se
zaměřovalo	zaměřovat	k5eAaImAgNnS	zaměřovat
na	na	k7c4	na
záporné	záporný	k2eAgFnPc4d1	záporná
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
vysoké	vysoký	k2eAgNnSc4d1	vysoké
procento	procento	k1gNnSc4	procento
ztrát	ztráta	k1gFnPc2	ztráta
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
jednak	jednak	k8xC	jednak
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
armáda	armáda	k1gFnSc1	armáda
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
přidělovala	přidělovat	k5eAaImAgFnS	přidělovat
zbraním	zbraň	k1gFnPc3	zbraň
SS	SS	kA	SS
ty	ten	k3xDgInPc4	ten
nejtěžší	těžký	k2eAgInPc4d3	nejtěžší
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sami	sám	k3xTgMnPc1	sám
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
nebáli	bát	k5eNaImAgMnP	bát
smrti	smrt	k1gFnSc3	smrt
a	a	k8xC	a
toužili	toužit	k5eAaImAgMnP	toužit
se	se	k3xPyFc4	se
osvědčit	osvědčit	k5eAaPmF	osvědčit
a	a	k8xC	a
splnit	splnit	k5eAaPmF	splnit
těžké	těžký	k2eAgInPc4d1	těžký
bojové	bojový	k2eAgInPc4d1	bojový
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
si	se	k3xPyFc3	se
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
vybudovaly	vybudovat	k5eAaPmAgFnP	vybudovat
skvělou	skvělý	k2eAgFnSc4d1	skvělá
pověst	pověst	k1gFnSc4	pověst
neústupných	ústupný	k2eNgMnPc2d1	neústupný
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
"	"	kIx"	"
<g/>
hasičů	hasič	k1gMnPc2	hasič
fronty	fronta	k1gFnSc2	fronta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
SS	SS	kA	SS
formálně	formálně	k6eAd1	formálně
zabývala	zabývat	k5eAaImAgFnS	zabývat
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
především	především	k6eAd1	především
pověřována	pověřovat	k5eAaImNgFnS	pověřovat
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
úkoly	úkol	k1gInPc7	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Naplňovala	naplňovat	k5eAaImAgFnS	naplňovat
Hitlerovu	Hitlerův	k2eAgFnSc4d1	Hitlerova
ctižádost	ctižádost	k1gFnSc4	ctižádost
zbavit	zbavit	k5eAaPmF	zbavit
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Evropu	Evropa	k1gFnSc4	Evropa
všech	všecek	k3xTgInPc2	všecek
elementů	element	k1gInPc2	element
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
"	"	kIx"	"
<g/>
nezaslouží	zasloužit	k5eNaPmIp3nP	zasloužit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
existovaly	existovat	k5eAaImAgFnP	existovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
natrvalo	natrvalo	k6eAd1	natrvalo
zotročit	zotročit	k5eAaPmF	zotročit
"	"	kIx"	"
<g/>
nižší	nízký	k2eAgFnSc4d2	nižší
lidskou	lidský	k2eAgFnSc4d1	lidská
rasu	rasa	k1gFnSc4	rasa
ze	z	k7c2	z
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
zemí	zem	k1gFnPc2	zem
<g/>
"	"	kIx"	"
a	a	k8xC	a
vymýtit	vymýtit	k5eAaPmF	vymýtit
z	z	k7c2	z
kontinentu	kontinent	k1gInSc2	kontinent
každého	každý	k3xTgMnSc2	každý
Žida	Žid	k1gMnSc2	Žid
-	-	kIx~	-
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnSc2	žena
i	i	k8xC	i
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
SS	SS	kA	SS
při	při	k7c6	při
vykonávání	vykonávání	k1gNnSc6	vykonávání
těchto	tento	k3xDgInPc2	tento
úkolů	úkol	k1gInPc2	úkol
organizovala	organizovat	k5eAaBmAgFnS	organizovat
vyvraždění	vyvraždění	k1gNnSc4	vyvraždění
čtrnácti	čtrnáct	k4xCc2	čtrnáct
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
šesti	šest	k4xCc2	šest
miliónů	milión	k4xCgInPc2	milión
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
pěti	pět	k4xCc2	pět
miliónů	milión	k4xCgInPc2	milión
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
dvou	dva	k4xCgNnPc2	dva
miliónů	milión	k4xCgInPc2	milión
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
půl	půl	k1xP	půl
miliónu	milión	k4xCgInSc2	milión
Romů	Rom	k1gMnPc2	Rom
a	a	k8xC	a
půl	půl	k1xP	půl
miliónu	milión	k4xCgInSc2	milión
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
téměř	téměř	k6eAd1	téměř
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInPc2	tisíc
nežidovských	židovský	k2eNgMnPc2d1	nežidovský
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
Rakušanů	Rakušan	k1gMnPc2	Rakušan
<g/>
,	,	kIx,	,
mentálně	mentálně	k6eAd1	mentálně
nebo	nebo	k8xC	nebo
fyzicky	fyzicky	k6eAd1	fyzicky
handicapovaných	handicapovaný	k2eAgMnPc2d1	handicapovaný
a	a	k8xC	a
takzvaných	takzvaný	k2eAgMnPc2d1	takzvaný
nepřátel	nepřítel	k1gMnPc2	nepřítel
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgMnPc4	tento
nepřátele	nepřítel	k1gMnPc4	nepřítel
patřili	patřit	k5eAaImAgMnP	patřit
často	často	k6eAd1	často
též	též	k9	též
političtí	politický	k2eAgMnPc1d1	politický
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
mniši	mnich	k1gMnPc1	mnich
<g/>
,	,	kIx,	,
svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
,	,	kIx,	,
homosexuálové	homosexuál	k1gMnPc1	homosexuál
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
nežidovské	židovský	k2eNgFnPc1d1	nežidovská
oběti	oběť	k1gFnPc1	oběť
<g/>
.	.	kIx.	.
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
samotná	samotný	k2eAgFnSc1d1	samotná
hodnost	hodnost	k1gFnSc1	hodnost
Reichsführer-SS	Reichsführer-SS	k1gMnSc1	Reichsführer-SS
(	(	kIx(	(
<g/>
říšský	říšský	k2eAgMnSc1d1	říšský
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
Julius	Julius	k1gMnSc1	Julius
Schreck	Schreck	k1gMnSc1	Schreck
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Joseph	Joseph	k1gMnSc1	Joseph
Berchtold	Berchtold	k1gMnSc1	Berchtold
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Erhard	Erhard	k1gInSc1	Erhard
Heiden	Heidna	k1gFnPc2	Heidna
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Karl	Karla	k1gFnPc2	Karla
Hanke	Hanke	k1gInSc1	Hanke
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
-	-	kIx~	-
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
následky	následek	k1gInPc4	následek
atentátu	atentát	k1gInSc2	atentát
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g/>
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Operace	operace	k1gFnSc1	operace
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Jacobi	Jacob	k1gFnSc2	Jacob
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Daluege	Dalueg	k1gFnSc2	Dalueg
-	-	kIx~	-
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
a	a	k8xC	a
popraveni	popravit	k5eAaPmNgMnP	popravit
českou	český	k2eAgFnSc7d1	Česká
justicí	justice	k1gFnSc7	justice
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Přísaha	přísaha	k1gFnSc1	přísaha
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
SS	SS	kA	SS
<g/>
:	:	kIx,	:
Ich	Ich	k1gMnSc5	Ich
schwöre	schwör	k1gMnSc5	schwör
Dir	Dir	k1gMnSc5	Dir
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
als	als	k?	als
Führer	Führer	k1gMnSc1	Führer
und	und	k?	und
Kanzler	Kanzler	k1gMnSc1	Kanzler
des	des	k1gNnSc2	des
Reiches	Reiches	k1gMnSc1	Reiches
Treue	Treu	k1gFnSc2	Treu
und	und	k?	und
Tapferkeit	Tapferkeit	k1gMnSc1	Tapferkeit
<g/>
.	.	kIx.	.
</s>
<s>
Ich	Ich	k?	Ich
gelobe	gelobat	k5eAaPmIp3nS	gelobat
Dir	Dir	k1gFnSc1	Dir
und	und	k?	und
den	den	k1gInSc1	den
von	von	k1gInSc1	von
Dir	Dir	k1gFnSc2	Dir
bestimmten	bestimmten	k2eAgInSc1d1	bestimmten
Vorgesetzten	Vorgesetzten	k2eAgInSc1d1	Vorgesetzten
Gehorsam	Gehorsam	k1gInSc1	Gehorsam
bis	bis	k?	bis
in	in	k?	in
den	den	k1gInSc4	den
Tod	Tod	k1gMnSc2	Tod
<g/>
,	,	kIx,	,
so	so	k?	so
wahr	wahr	k1gInSc1	wahr
mir	mir	k1gInSc1	mir
Gott	Gott	k2eAgInSc1d1	Gott
helfe	helf	k1gInSc5	helf
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
Adolfe	Adolf	k1gMnSc5	Adolf
Hitlere	Hitler	k1gMnSc5	Hitler
<g/>
,	,	kIx,	,
vůdci	vůdce	k1gMnSc3	vůdce
a	a	k8xC	a
kancléři	kancléř	k1gMnSc3	kancléř
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
přísahám	přísahat	k5eAaImIp1nS	přísahat
věrnost	věrnost	k1gFnSc1	věrnost
a	a	k8xC	a
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Tobě	ty	k3xPp2nSc3	ty
a	a	k8xC	a
nadřazeným	nadřazený	k2eAgNnSc7d1	nadřazené
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sám	sám	k3xTgMnSc1	sám
jmenuješ	jmenovat	k5eAaBmIp2nS	jmenovat
<g/>
,	,	kIx,	,
slibuji	slibovat	k5eAaImIp1nS	slibovat
poslušnost	poslušnost	k1gFnSc4	poslušnost
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mi	já	k3xPp1nSc3	já
dopomáhej	dopomáhat	k5eAaImRp2nS	dopomáhat
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Meine	Meinout	k5eAaImIp3nS	Meinout
Ehre	Ehre	k1gNnSc7	Ehre
heißt	heißt	k2eAgInSc1d1	heißt
Treue	Treue	k1gInSc1	Treue
Má	mít	k5eAaImIp3nS	mít
čest	čest	k1gFnSc4	čest
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
věrnost	věrnost	k1gFnSc1	věrnost
Správnější	správní	k2eAgNnSc1d2	správnější
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
<g/>
Treu	Trea	k1gFnSc4	Trea
heißt	heißta	k1gFnPc2	heißta
meine	meinout	k5eAaImIp3nS	meinout
Ehre	Ehre	k1gNnSc1	Ehre
<g/>
.	.	kIx.	.
<g/>
viz	vidět	k5eAaImRp2nS	vidět
nápis	nápis	k1gInSc4	nápis
na	na	k7c6	na
dýce	dýka	k1gFnSc6	dýka
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Karol	Karol	k1gInSc1	Karol
Grünberg	Grünberg	k1gInSc1	Grünberg
<g/>
:	:	kIx,	:
<g/>
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
černá	černý	k2eAgFnSc1d1	černá
garda	garda	k1gFnSc1	garda
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-206-0859-8	[number]	k4	978-80-206-0859-8
KNOPP	KNOPP	kA	KNOPP
<g/>
,	,	kIx,	,
Guido	Guido	k1gNnSc1	Guido
<g/>
.	.	kIx.	.
</s>
<s>
SS	SS	kA	SS
<g/>
:	:	kIx,	:
výstraha	výstraha	k1gFnSc1	výstraha
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
440	[number]	k4	440
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
AILSBY	AILSBY	kA	AILSBY
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-SS	Waffen-SS	k?	Waffen-SS
−	−	k?	−
Zbraně	zbraň	k1gFnSc2	zbraň
SS	SS	kA	SS
<g/>
:	:	kIx,	:
nepublikované	publikovaný	k2eNgFnPc1d1	nepublikovaná
fotografie	fotografia	k1gFnPc1	fotografia
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7181	[number]	k4	7181
<g/>
-	-	kIx~	-
<g/>
435	[number]	k4	435
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Schutzstaffel	Schutzstaffela	k1gFnPc2	Schutzstaffela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
1938	[number]	k4	1938
forum	forum	k1gNnSc1	forum
<g/>
.	.	kIx.	.
<g/>
axishistory	axishistor	k1gInPc1	axishistor
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Axis	Axis	k1gInSc1	Axis
History	Histor	k1gInPc4	Histor
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
SS	SS	kA	SS
The	The	k1gMnSc1	The
German	German	k1gMnSc1	German
SS	SS	kA	SS
<g/>
/	/	kIx~	/
<g/>
Waffen-SS	Waffen-SS	k1gMnSc3	Waffen-SS
in	in	k?	in
WWII	WWII	kA	WWII
feldgrau	feldgrau	k5eAaPmIp1nS	feldgrau
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
