<s>
Žert	žert	k1gInSc1	žert
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
režisérem	režisér	k1gMnSc7	režisér
Jaromilem	Jaromil	k1gMnSc7	Jaromil
Jirešem	Jireš	k1gMnSc7	Jireš
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Kundera	Kunder	k1gMnSc2	Kunder
Další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
černobílý	černobílý	k2eAgMnSc1d1	černobílý
<g/>
,	,	kIx,	,
80	[number]	k4	80
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
Muž	muž	k1gMnSc1	muž
na	na	k7c6	na
služební	služební	k2eAgFnSc6d1	služební
cestě	cesta	k1gFnSc6	cesta
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Somr	Somr	k1gMnSc1	Somr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
chopí	chopit	k5eAaPmIp3nS	chopit
příležitosti	příležitost	k1gFnSc3	příležitost
pomstít	pomstít	k5eAaPmF	pomstít
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgMnSc3	svůj
bývalému	bývalý	k2eAgMnSc3d1	bývalý
příteli	přítel	k1gMnSc3	přítel
(	(	kIx(	(
<g/>
Luděk	Luděk	k1gMnSc1	Luděk
Munzar	Munzar	k1gMnSc1	Munzar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
kdysi	kdysi	k6eAd1	kdysi
ublížil	ublížit	k5eAaPmAgMnS	ublížit
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
svede	svést	k5eAaPmIp3nS	svést
ženu	žena	k1gFnSc4	žena
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Dítětová	Dítětová	k1gFnSc1	Dítětová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
povede	vést	k5eAaImIp3nS	vést
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
nevěra	nevěra	k1gMnSc1	nevěra
má	mít	k5eAaImIp3nS	mít
vyjít	vyjít	k5eAaPmF	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přítel	přítel	k1gMnSc1	přítel
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc7d2	mladší
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
