<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
vnitrostranické	vnitrostranický	k2eAgFnPc4d1	vnitrostranická
krize	krize	k1gFnPc4	krize
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
zbavil	zbavit	k5eAaPmAgMnS	zbavit
svých	svůj	k3xOyFgMnPc2	svůj
dlouhodobých	dlouhodobý	k2eAgMnPc2d1	dlouhodobý
oponentů	oponent	k1gMnPc2	oponent
včetně	včetně	k7c2	včetně
osob	osoba	k1gFnPc2	osoba
nepřijatelných	přijatelný	k2eNgFnPc2d1	nepřijatelná
pro	pro	k7c4	pro
špičky	špička	k1gFnPc4	špička
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
upevnil	upevnit	k5eAaPmAgInS	upevnit
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
i	i	k9	i
navenek	navenek	k6eAd1	navenek
<g/>
.	.	kIx.	.
</s>
