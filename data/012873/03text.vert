<p>
<s>
Ngoni	Ngon	k1gMnPc1	Ngon
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
goni	goni	k1gNnPc6	goni
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc1d1	strunný
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
primitivní	primitivní	k2eAgMnSc1d1	primitivní
předchůdce	předchůdce	k1gMnSc1	předchůdce
banja	banjo	k1gNnSc2	banjo
<g/>
.	.	kIx.	.
</s>
<s>
Velikostí	velikost	k1gFnSc7	velikost
se	se	k3xPyFc4	se
ale	ale	k9	ale
podobá	podobat	k5eAaImIp3nS	podobat
spíše	spíše	k9	spíše
ukulele	ukulele	k1gFnSc1	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
duté	dutý	k2eAgNnSc1d1	duté
<g/>
,	,	kIx,	,
vyrobené	vyrobený	k2eAgNnSc1d1	vyrobené
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
potažené	potažený	k2eAgInPc1d1	potažený
suchou	suchý	k2eAgFnSc7d1	suchá
kůží	kůže	k1gFnSc7	kůže
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
kozí	kozí	k2eAgFnSc1d1	kozí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
bubnu	buben	k1gInSc2	buben
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
napnutá	napnutý	k2eAgFnSc1d1	napnutá
<g/>
.	.	kIx.	.
</s>
<s>
Zkušení	zkušený	k2eAgMnPc1d1	zkušený
afričtí	africký	k2eAgMnPc1d1	africký
hráči	hráč	k1gMnPc1	hráč
dokáží	dokázat	k5eAaPmIp3nP	dokázat
z	z	k7c2	z
ngoni	ngoeň	k1gFnSc3	ngoeň
vyloudit	vyloudit	k5eAaPmF	vyloudit
opravdu	opravdu	k6eAd1	opravdu
velmi	velmi	k6eAd1	velmi
rychlé	rychlý	k2eAgFnPc4d1	rychlá
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Ngoni	Ngoeň	k1gFnSc3	Ngoeň
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
k	k	k7c3	k
doprovodu	doprovod	k1gInSc3	doprovod
slavnostních	slavnostní	k2eAgFnPc2d1	slavnostní
událostí	událost	k1gFnPc2	událost
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
svatby	svatba	k1gFnSc2	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
nástrojem	nástroj	k1gInSc7	nástroj
afrických	africký	k2eAgMnPc2d1	africký
potulných	potulný	k2eAgMnPc2d1	potulný
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
griotů	griot	k1gMnPc2	griot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
hráči	hráč	k1gMnPc1	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
Cheick	Cheick	k6eAd1	Cheick
Hamala	hamat	k5eAaImAgFnS	hamat
Diabate	Diabat	k1gInSc5	Diabat
</s>
</p>
<p>
<s>
Issa	Issa	k1gMnSc1	Issa
Bagayogo	Bagayogo	k1gMnSc1	Bagayogo
</s>
</p>
<p>
<s>
Moriba	Moriba	k1gMnSc1	Moriba
Koï	Koï	k1gMnSc1	Koï
</s>
</p>
<p>
<s>
Bassekou	Basséct	k5eAaPmIp3nPwK	Basséct
Kouyaté	Kouyatá	k1gFnPc1	Kouyatá
</s>
</p>
<p>
<s>
Andra	Andra	k1gFnSc1	Andra
Kouyaté	Kouyatá	k1gFnSc2	Kouyatá
</s>
</p>
<p>
<s>
Baba	baba	k1gFnSc1	baba
Sissoko	Sissoko	k1gNnSc1	Sissoko
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ngoni	Ngon	k1gMnPc1	Ngon
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
