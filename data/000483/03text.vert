<s>
Tajvan	Tajvan	k1gMnSc1	Tajvan
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c6	na
vodní	vodní	k2eAgFnSc6d1	vodní
nádrži	nádrž	k1gFnSc6	nádrž
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
km	km	kA	km
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Horní	horní	k2eAgFnSc1d1	horní
Planá	Planá	k1gFnSc1	Planá
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
ostrohu	ostroh	k1gInSc2	ostroh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
Vltavy	Vltava	k1gFnSc2	Vltava
vlévala	vlévat	k5eAaImAgFnS	vlévat
Ostřice	ostřice	k1gFnSc1	ostřice
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
ostrova	ostrov	k1gInSc2	ostrov
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
737	[number]	k4	737
m.	m.	k?	m.
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
porostlý	porostlý	k2eAgMnSc1d1	porostlý
smíšeným	smíšený	k2eAgInSc7d1	smíšený
lesem	les	k1gInSc7	les
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
bříz	bříza	k1gFnPc2	bříza
a	a	k8xC	a
borovic	borovice	k1gFnPc2	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
sem	sem	k6eAd1	sem
zakázán	zakázán	k2eAgInSc1d1	zakázán
vstup	vstup	k1gInSc1	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
vodního	vodní	k2eAgNnSc2d1	vodní
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
vzácné	vzácný	k2eAgFnSc2d1	vzácná
vodní	vodní	k2eAgFnSc2d1	vodní
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
výkladu	výklad	k1gInSc2	výklad
ostrov	ostrov	k1gInSc4	ostrov
získal	získat	k5eAaPmAgInS	získat
název	název	k1gInSc1	název
podle	podle	k7c2	podle
rybáře	rybář	k1gMnSc2	rybář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
jezdil	jezdit	k5eAaImAgMnS	jezdit
chytat	chytat	k5eAaImF	chytat
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
měl	mít	k5eAaImAgMnS	mít
šikmé	šikmý	k2eAgNnSc4d1	šikmé
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
Číňan	Číňan	k1gMnSc1	Číňan
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
pak	pak	k6eAd1	pak
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
ostrovu	ostrov	k1gInSc3	ostrov
říkat	říkat	k5eAaImF	říkat
podle	podle	k7c2	podle
čínského	čínský	k2eAgInSc2d1	čínský
ostrova	ostrov	k1gInSc2	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
výkladu	výklad	k1gInSc2	výklad
název	název	k1gInSc4	název
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
kartografové	kartograf	k1gMnPc1	kartograf
plánující	plánující	k2eAgFnSc4d1	plánující
Lipenskou	lipenský	k2eAgFnSc4d1	Lipenská
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
bezejmenný	bezejmenný	k2eAgInSc4d1	bezejmenný
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
tvarem	tvar	k1gInSc7	tvar
nápadně	nápadně	k6eAd1	nápadně
připomínal	připomínat	k5eAaImAgInS	připomínat
tvar	tvar	k1gInSc4	tvar
ostrova	ostrov	k1gInSc2	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
ostrova	ostrov	k1gInSc2	ostrov
Fotografie	fotografia	k1gFnSc2	fotografia
ostrova	ostrov	k1gInSc2	ostrov
</s>
