<p>
<s>
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc4d1	československý
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
režírovaný	režírovaný	k2eAgInSc4d1	režírovaný
Věrou	Věra	k1gFnSc7	Věra
Plívovou-Šimkovou	Plívovou-Šimkův	k2eAgFnSc7d1	Plívovou-Šimkův
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Tomáši	Tomáš	k1gMnSc6	Tomáš
Šeredovi	šereda	k1gMnSc6	šereda
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
syn	syn	k1gMnSc1	syn
režisérky	režisérka	k1gFnSc2	režisérka
Tomáš	Tomáš	k1gMnSc1	Tomáš
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
exotické	exotický	k2eAgNnSc4d1	exotické
ptactvo	ptactvo	k1gNnSc4	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kamarády	kamarád	k1gMnPc7	kamarád
vyklízí	vyklízet	k5eAaImIp3nS	vyklízet
les	les	k1gInSc1	les
od	od	k7c2	od
odpadků	odpadek	k1gInPc2	odpadek
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
vrátit	vrátit	k5eAaPmF	vrátit
vyhozené	vyhozený	k2eAgInPc4d1	vyhozený
předměty	předmět	k1gInPc4	předmět
těm	ten	k3xDgNnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
lesa	les	k1gInSc2	les
odnesli	odnést	k5eAaPmAgMnP	odnést
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepřátelí	přátelit	k5eNaImIp3nS	přátelit
se	s	k7c7	s
starším	starý	k2eAgMnSc7d2	starší
spolužákem	spolužák	k1gMnSc7	spolužák
Frantou	Franta	k1gMnSc7	Franta
Metelkou	Metelkou	k?	Metelkou
<g/>
,	,	kIx,	,
ztvárněným	ztvárněný	k2eAgMnSc7d1	ztvárněný
Michaelem	Michael	k1gMnSc7	Michael
Hofbauerem	Hofbauer	k1gMnSc7	Hofbauer
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
sovu	sova	k1gFnSc4	sova
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
ochočenou	ochočený	k2eAgFnSc4d1	ochočená
<g/>
,	,	kIx,	,
střílí	střílet	k5eAaImIp3nS	střílet
v	v	k7c6	v
lese	les	k1gInSc6	les
ptactvo	ptactvo	k1gNnSc1	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
Franta	Franta	k1gMnSc1	Franta
Tomášovi	Tomáš	k1gMnSc3	Tomáš
z	z	k7c2	z
pomsty	pomsta	k1gFnSc2	pomsta
vypustí	vypustit	k5eAaPmIp3nS	vypustit
veškeré	veškerý	k3xTgNnSc4	veškerý
exotické	exotický	k2eAgNnSc4d1	exotické
ptactvo	ptactvo	k1gNnSc4	ptactvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Tomáš	Tomáš	k1gMnSc1	Tomáš
choval	chovat	k5eAaImAgMnS	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Postižený	postižený	k1gMnSc1	postižený
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vypraví	vypravit	k5eAaPmIp3nS	vypravit
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
odchytit	odchytit	k5eAaPmF	odchytit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
voliér	voliéra	k1gFnPc2	voliéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lese	les	k1gInSc6	les
tráví	trávit	k5eAaImIp3nP	trávit
i	i	k9	i
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přidávají	přidávat	k5eAaImIp3nP	přidávat
další	další	k2eAgFnPc4d1	další
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
také	také	k9	také
Franta	Franta	k1gMnSc1	Franta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
napravit	napravit	k5eAaPmF	napravit
napáchanou	napáchaný	k2eAgFnSc4d1	napáchaná
škodu	škoda	k1gFnSc4	škoda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
Tomášovi	Tomášův	k2eAgMnPc1d1	Tomášův
nápomocen	nápomocen	k2eAgMnSc1d1	nápomocen
při	při	k7c6	při
odchytu	odchyt	k1gInSc6	odchyt
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
filmu	film	k1gInSc2	film
již	již	k6eAd1	již
stanuje	stanovat	k5eAaImIp3nS	stanovat
v	v	k7c6	v
lese	les	k1gInSc6	les
celá	celý	k2eAgFnSc1d1	celá
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
celý	celý	k2eAgInSc4d1	celý
les	les	k1gInSc4	les
čistí	čistit	k5eAaImIp3nS	čistit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sesbíraného	sesbíraný	k2eAgNnSc2d1	sesbírané
harampádí	harampádí	k1gNnSc2	harampádí
sestaví	sestavit	k5eAaPmIp3nS	sestavit
svého	svůj	k3xOyFgMnSc4	svůj
maskota	maskot	k1gMnSc4	maskot
Brontosaura	brontosaurus	k1gMnSc4	brontosaurus
(	(	kIx(	(
<g/>
předlohou	předloha	k1gFnSc7	předloha
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
obří	obří	k2eAgMnSc1d1	obří
druhohorní	druhohorní	k2eAgMnSc1d1	druhohorní
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
si	se	k3xPyFc3	se
odvezou	odvézt	k5eAaPmIp3nP	odvézt
z	z	k7c2	z
lesa	les	k1gInSc2	les
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
dále	daleko	k6eAd2	daleko
hrají	hrát	k5eAaImIp3nP	hrát
například	například	k6eAd1	například
Pavel	Pavel	k1gMnSc1	Pavel
Soukup	Soukup	k1gMnSc1	Soukup
(	(	kIx(	(
<g/>
v	v	k7c6	v
roli	role	k1gFnSc6	role
tělocvikáře	tělocvikář	k1gMnSc4	tělocvikář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
(	(	kIx(	(
<g/>
coby	coby	k?	coby
učitel	učitel	k1gMnSc1	učitel
Bobr	bobr	k1gMnSc1	bobr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Veronika	Veronika	k1gFnSc1	Veronika
Jeníková	Jeníková	k1gFnSc1	Jeníková
(	(	kIx(	(
<g/>
pionýrská	pionýrský	k2eAgFnSc1d1	Pionýrská
vedoucí	vedoucí	k1gFnSc1	vedoucí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Somr	Somr	k1gMnSc1	Somr
(	(	kIx(	(
<g/>
inspektor	inspektor	k1gMnSc1	inspektor
<g/>
)	)	kIx)	)
či	či	k8xC	či
Daniela	Daniela	k1gFnSc1	Daniela
Kolářová	Kolářová	k1gFnSc1	Kolářová
(	(	kIx(	(
<g/>
učitelka	učitelka	k1gFnSc1	učitelka
Bižoléčára	Bižoléčár	k1gMnSc2	Bižoléčár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
