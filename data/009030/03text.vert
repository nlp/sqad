<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hajíček	Hajíček	k1gMnSc1	Hajíček
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
původní	původní	k2eAgMnSc1d1	původní
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Krucipüsk	Krucipüsk	k1gInSc1	Krucipüsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
Divadla	divadlo	k1gNnSc2	divadlo
Ta	ten	k3xDgFnSc1	ten
Fantastika	fantastika	k1gFnSc1	fantastika
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
roli	role	k1gFnSc4	role
žebráka	žebrák	k1gMnSc2	žebrák
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc4	Dorian
Graye	Gray	k1gInSc2	Gray
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
roli	role	k1gFnSc4	role
učitele	učitel	k1gMnSc2	učitel
Horáka	Horák	k1gMnSc2	Horák
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Bastardi	bastard	k1gMnPc1	bastard
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
jako	jako	k9	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
i	i	k9	i
na	na	k7c6	na
filmu	film	k1gInSc6	film
Burácení	burácení	k1gNnSc2	burácení
režiséra	režisér	k1gMnSc2	režisér
Adolfa	Adolf	k1gMnSc2	Adolf
Ziky	Zika	k1gMnSc2	Zika
<g/>
,	,	kIx,	,
připravovaném	připravovaný	k2eAgInSc6d1	připravovaný
k	k	k7c3	k
uvedení	uvedený	k2eAgMnPc1d1	uvedený
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Zpívá	zpívat	k5eAaImIp3nS	zpívat
v	v	k7c6	v
části	část	k1gFnSc6	část
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
na	na	k7c4	na
inzerát	inzerát	k1gInSc4	inzerát
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Našim	náš	k3xOp1gMnPc3	náš
klientům	klient	k1gMnPc3	klient
skupiny	skupina	k1gFnSc2	skupina
Wohnout	Wohnout	k1gFnSc2	Wohnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sólová	sólový	k2eAgNnPc4d1	sólové
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Kocour	kocour	k1gMnSc1	kocour
v	v	k7c6	v
troubě	trouba	k1gFnSc6	trouba
</s>
</p>
<p>
<s>
===	===	k?	===
Hubert	Hubert	k1gMnSc1	Hubert
Macháně	Machán	k1gInSc6	Machán
===	===	k?	===
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Zazdili	zazdít	k5eAaPmAgMnP	zazdít
nám	my	k3xPp1nPc3	my
WC	WC	kA	WC
<g/>
!!	!!	k?	!!
</s>
</p>
<p>
<s>
===	===	k?	===
Krucipüsk	Krucipüsk	k1gInSc4	Krucipüsk
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Bastardi	bastard	k1gMnPc1	bastard
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
ředitelův	ředitelův	k2eAgMnSc1d1	ředitelův
zeť	zeť	k1gMnSc1	zeť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
Burácení	burácení	k1gNnSc2	burácení
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hajíček	Hajíček	k1gMnSc1	Hajíček
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hajíček	Hajíček	k1gMnSc1	Hajíček
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hajíček	Hajíček	k1gMnSc1	Hajíček
na	na	k7c4	na
musicserver	musicserver	k1gInSc4	musicserver
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
