<p>
<s>
Strašidla	strašidlo	k1gNnPc1	strašidlo
jsou	být	k5eAaImIp3nP	být
český	český	k2eAgInSc4d1	český
rodinný	rodinný	k2eAgInSc4d1	rodinný
komediální	komediální	k2eAgInSc4d1	komediální
film	film	k1gInSc4	film
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
rodině	rodina	k1gFnSc6	rodina
aristokratických	aristokratický	k2eAgNnPc2d1	aristokratické
strašidel	strašidlo	k1gNnPc2	strašidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
musí	muset	k5eAaImIp3nP	muset
začít	začít	k5eAaPmF	začít
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
platit	platit	k5eAaImF	platit
nájemné	nájemné	k1gNnSc4	nájemné
a	a	k8xC	a
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
rodiny	rodina	k1gFnSc2	rodina
je	být	k5eAaImIp3nS	být
hejkal	hejkal	k1gMnSc1	hejkal
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
čarodějka	čarodějka	k1gFnSc1	čarodějka
Patricie	Patricie	k1gFnSc1	Patricie
<g/>
,	,	kIx,	,
strýček	strýček	k1gMnSc1	strýček
vodník	vodník	k1gMnSc1	vodník
Bubla	Bubla	k1gMnSc1	Bubla
<g/>
,	,	kIx,	,
svěřenkyně	svěřenkyně	k1gFnSc1	svěřenkyně
víla	víla	k1gFnSc1	víla
Jitřenka	Jitřenka	k1gFnSc1	Jitřenka
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
námětu	námět	k1gInSc2	námět
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Kos	Kos	k1gMnSc1	Kos
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
napsal	napsat	k5eAaBmAgMnS	napsat
Marek	Marek	k1gMnSc1	Marek
Kališ	kališ	k1gInSc4	kališ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
již	již	k9	již
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
předchozích	předchozí	k2eAgInPc6d1	předchozí
snímcích	snímek	k1gInPc6	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
rovněž	rovněž	k9	rovněž
Troškův	Troškův	k2eAgMnSc1d1	Troškův
vytrvalý	vytrvalý	k2eAgMnSc1d1	vytrvalý
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Miloš	Miloš	k1gMnSc1	Miloš
Krkoška	krkoška	k1gFnSc1	krkoška
<g/>
,	,	kIx,	,
kameramanem	kameraman	k1gMnSc7	kameraman
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Duba	Duba	k1gMnSc1	Duba
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Falcon	Falcona	k1gFnPc2	Falcona
jej	on	k3xPp3gMnSc4	on
uvedla	uvést	k5eAaPmAgFnS	uvést
do	do	k7c2	do
českých	český	k2eAgNnPc2d1	české
kin	kino	k1gNnPc2	kino
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
a	a	k8xC	a
obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Strašidla	strašidlo	k1gNnPc1	strašidlo
na	na	k7c6	na
webu	web	k1gInSc6	web
společnosti	společnost	k1gFnSc2	společnost
Falcon	Falcona	k1gFnPc2	Falcona
</s>
</p>
