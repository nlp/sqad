<p>
<s>
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Mihok	Mihok	k1gInSc1	Mihok
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
Trstená	Trstený	k2eAgFnSc1d1	Trstená
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
obránce	obránce	k1gMnSc1	obránce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
a	a	k8xC	a
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
Plastiku	plastika	k1gFnSc4	plastika
Nitra	nitro	k1gNnSc2	nitro
a	a	k8xC	a
FK	FK	kA	FK
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
166	[number]	k4	166
utkáních	utkání	k1gNnPc6	utkání
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
3	[number]	k4	3
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Player	Player	k1gInSc1	Player
History	Histor	k1gInPc1	Histor
</s>
</p>
<p>
<s>
FC	FC	kA	FC
Nitra	Nitra	k1gFnSc1	Nitra
</s>
</p>
