<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
tragických	tragický	k2eAgFnPc2d1	tragická
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
období	období	k1gNnSc4	období
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Protektorátě	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
druhého	druhý	k4xOgNnSc2	druhý
výročí	výročí	k1gNnSc3	výročí
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
studentské	studentský	k2eAgFnSc2d1	studentská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
delegáti	delegát	k1gMnPc1	delegát
z	z	k7c2	z
26	[number]	k4	26
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
studentstva	studentstvo	k1gNnSc2	studentstvo
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
dnem	dno	k1gNnSc7	dno
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
český	český	k2eAgInSc4d1	český
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
násilným	násilný	k2eAgNnSc7d1	násilné
potlačením	potlačení	k1gNnSc7	potlačení
poklidné	poklidný	k2eAgFnSc2d1	poklidná
demonstrace	demonstrace	k1gFnSc2	demonstrace
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
21	[number]	k4	21
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc3	vznik
Československa	Československo	k1gNnSc2	Československo
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1939	[number]	k4	1939
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgMnS	být
těžce	těžce	k6eAd1	těžce
zraněn	zraněn	k2eAgMnSc1d1	zraněn
student	student	k1gMnSc1	student
Lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Jan	Jan	k1gMnSc1	Jan
Opletal	Opletal	k1gMnSc1	Opletal
a	a	k8xC	a
zabit	zabit	k2eAgMnSc1d1	zabit
pekařský	pekařský	k2eAgMnSc1d1	pekařský
dělník	dělník	k1gMnSc1	dělník
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
potlačování	potlačování	k1gNnSc6	potlačování
demonstrace	demonstrace	k1gFnSc2	demonstrace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
sil	síla	k1gFnPc2	síla
pořádkové	pořádkový	k2eAgFnSc2d1	pořádková
policie	policie	k1gFnSc2	policie
podílely	podílet	k5eAaImAgFnP	podílet
i	i	k9	i
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
střety	střet	k1gInPc1	střet
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
Němci	Němec	k1gMnPc1	Němec
stříleli	střílet	k5eAaImAgMnP	střílet
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
výstrahu	výstraha	k1gFnSc4	výstraha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
demonstrantů	demonstrant	k1gMnPc2	demonstrant
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
potyček	potyčka	k1gFnPc2	potyčka
byl	být	k5eAaImAgInS	být
postřelen	postřelen	k2eAgMnSc1d1	postřelen
právě	právě	k9	právě
Jan	Jan	k1gMnSc1	Jan
Opletal	Opletal	k1gMnSc1	Opletal
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
svému	svůj	k3xOyFgNnSc3	svůj
zranění	zranění	k1gNnSc3	zranění
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
na	na	k7c6	na
Albertově	Albertův	k2eAgInSc6d1	Albertův
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
rakev	rakev	k1gFnSc1	rakev
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
převezena	převézt	k5eAaPmNgFnS	převézt
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
odeslána	odeslat	k5eAaPmNgFnS	odeslat
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgFnPc1d1	následná
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgInP	zúčastnit
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
mezi	mezi	k7c7	mezi
demonstranty	demonstrant	k1gMnPc7	demonstrant
a	a	k8xC	a
pořádkovými	pořádkový	k2eAgFnPc7d1	pořádková
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
konala	konat	k5eAaImAgFnS	konat
porada	porada	k1gFnSc1	porada
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c4	o
uzavření	uzavření	k1gNnSc4	uzavření
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatčení	zatčení	k1gNnSc2	zatčení
a	a	k8xC	a
popravení	popravení	k1gNnSc2	popravení
9	[number]	k4	9
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
představitelů	představitel	k1gMnPc2	představitel
studentské	studentský	k2eAgFnSc2d1	studentská
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Frauwirth	Frauwirth	k1gMnSc1	Frauwirth
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Koula	Koula	k1gMnSc1	Koula
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Matoušek	Matoušek	k1gMnSc1	Matoušek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Skorkovský	Skorkovský	k2eAgMnSc1d1	Skorkovský
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Šaffránek	Šaffránek	k1gMnSc1	Šaffránek
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Weinert	Weinert	k1gMnSc1	Weinert
<g/>
)	)	kIx)	)
a	a	k8xC	a
internace	internace	k1gFnSc1	internace
stovek	stovka	k1gFnPc2	stovka
studentů	student	k1gMnPc2	student
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc4	listopad
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozsáhlému	rozsáhlý	k2eAgNnSc3d1	rozsáhlé
zatýkání	zatýkání	k1gNnSc3	zatýkání
českých	český	k2eAgMnPc2d1	český
studentů	student	k1gMnPc2	student
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
Příbrami	Příbram	k1gFnSc6	Příbram
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
zatčení	zatčení	k1gNnPc2	zatčení
gestapem	gestapo	k1gNnSc7	gestapo
a	a	k8xC	a
jednotkami	jednotka	k1gFnPc7	jednotka
SS	SS	kA	SS
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byli	být	k5eAaImAgMnP	být
dopraveni	dopravit	k5eAaPmNgMnP	dopravit
do	do	k7c2	do
ruzyňských	ruzyňský	k2eAgFnPc2d1	Ruzyňská
kasáren	kasárny	k1gFnPc2	kasárny
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
9	[number]	k4	9
představitelů	představitel	k1gMnPc2	představitel
studentských	studentský	k2eAgFnPc2d1	studentská
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
1200	[number]	k4	1200
zatčených	zatčený	k2eAgMnPc2d1	zatčený
studentů	student	k1gMnPc2	student
bylo	být	k5eAaImAgNnS	být
poté	poté	k6eAd1	poté
převezeno	převézt	k5eAaPmNgNnS	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Sachsenhausen-Oranienburg	Sachsenhausen-Oranienburg	k1gInSc1	Sachsenhausen-Oranienburg
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
propuštěna	propustit	k5eAaPmNgFnS	propustit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
pak	pak	k6eAd1	pak
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
1200	[number]	k4	1200
studentů	student	k1gMnPc2	student
nepřežilo	přežít	k5eNaPmAgNnS	přežít
útrapy	útrapa	k1gFnPc4	útrapa
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
svaz	svaz	k1gInSc1	svaz
československého	československý	k2eAgNnSc2d1	Československé
studentstva	studentstvo	k1gNnSc2	studentstvo
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
naší	náš	k3xOp1gFnSc2	náš
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
aktivní	aktivní	k2eAgFnSc4d1	aktivní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
byla	být	k5eAaImAgFnS	být
právě	právě	k6eAd1	právě
londýnská	londýnský	k2eAgFnSc1d1	londýnská
schůze	schůze	k1gFnSc1	schůze
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
studentské	studentský	k2eAgFnSc2d1	studentská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
Prohlášení	prohlášení	k1gNnSc4	prohlášení
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
studentů	student	k1gMnPc2	student
k	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
dnem	den	k1gInSc7	den
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
.	.	kIx.	.
</s>
