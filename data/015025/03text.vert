<s>
USS	USS	kA
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
(	(	kIx(
<g/>
LHD-	LHD-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
vrtulníková	vrtulníkový	k2eAgFnSc1d1
výsadková	výsadkový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
Třída	třída	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Wasp	Wasp	k1gMnSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
trupu	trup	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
LHD-8	LHD-8	k4
</s>
<s>
Jméno	jméno	k1gNnSc1
podle	podle	k7c2
<g/>
:	:	kIx,
</s>
<s>
bitva	bitva	k1gFnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Makin	Makina	k1gFnPc2
</s>
<s>
Objednána	objednán	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2002	#num#	k4
</s>
<s>
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
</s>
<s>
Spuštěna	spuštěn	k2eAgNnPc1d1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
září	zářit	k5eAaImIp3nS
2006	#num#	k4
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
říjen	říjen	k1gInSc1
2009	#num#	k4
</s>
<s>
Osud	osud	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
aktivní	aktivní	k2eAgMnSc1d1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Takticko-technická	takticko-technický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Výtlak	výtlak	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
42	#num#	k4
440	#num#	k4
t	t	k?
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
258,2	258,2	k4
m	m	kA
</s>
<s>
Šířka	šířka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
32,3	32,3	k4
m	m	kA
</s>
<s>
Ponor	ponor	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
8,5	8,5	k4
m	m	kA
</s>
<s>
Pohon	pohon	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
CODLOG70	CODLOG70	k4
000	#num#	k4
koní	kůň	k1gMnPc2
</s>
<s>
Rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
28	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
52	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Dosah	dosah	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
9	#num#	k4
500	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
17	#num#	k4
600	#num#	k4
km	km	kA
<g/>
)	)	kIx)
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
20	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
37	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
208	#num#	k4
důstojníků	důstojník	k1gMnPc2
a	a	k8xC
námořníků	námořník	k1gMnPc2
<g/>
1687	#num#	k4
vojáků	voják	k1gMnPc2
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
RIM-7	RIM-7	k1gFnSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
hl	hl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
×	×	k?
RIM-	RIM-	k1gFnSc1
<g/>
1162	#num#	k4
<g/>
×	×	k?
20	#num#	k4
mm	mm	kA
Phalanx	Phalanx	k1gInSc4
<g/>
3	#num#	k4
<g/>
×	×	k?
25	#num#	k4
mm	mm	kA
M242	M242	k1gMnSc1
Bushmaster	Bushmaster	k1gMnSc1
<g/>
4	#num#	k4
<g/>
×	×	k?
12,7	12,7	k4
mm	mm	kA
M2	M2	k1gFnSc1
Browning	browning	k1gInSc1
</s>
<s>
Letadla	letadlo	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
6	#num#	k4
×	×	k?
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
F-35	F-35	k1gMnSc1
Lightning	Lightning	k1gInSc4
II4	II4	k1gFnSc2
×	×	k?
Bell	bell	k1gInSc1
AH-1Z	AH-1Z	k1gFnPc2
Viper	Vipero	k1gNnPc2
<g/>
12	#num#	k4
×	×	k?
Bell	bell	k1gInSc1
Boeing	boeing	k1gInSc1
V-22	V-22	k1gMnSc2
Osprey	Osprea	k1gMnSc2
<g/>
4	#num#	k4
×	×	k?
Sikorsky	Sikorsky	k1gFnSc6
CH-53E	CH-53E	k1gFnPc2
Super	super	k2eAgInSc1d1
Stallion	Stallion	k1gInSc1
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
×	×	k?
Bell	bell	k1gInSc1
UH-1Y	UH-1Y	k1gMnSc1
Venom	Venom	k1gInSc1
</s>
<s>
USS	USS	kA
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
(	(	kIx(
<g/>
LHD-	LHD-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
víceúčelová	víceúčelový	k2eAgFnSc1d1
vrtulníková	vrtulníkový	k2eAgFnSc1d1
výsadková	výsadkový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Námořnictva	námořnictvo	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
2004	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
jako	jako	k9
osmá	osmý	k4xOgFnSc1
a	a	k8xC
poslední	poslední	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
třídy	třída	k1gFnSc2
Wasp	Wasp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
k	k	k7c3
naložení	naložení	k1gNnSc3
<g/>
,	,	kIx,
přepravě	přeprava	k1gFnSc3
<g/>
,	,	kIx,
provedení	provedení	k1gNnSc4
výsadku	výsadek	k1gInSc2
<g/>
,	,	kIx,
velení	velení	k1gNnSc2
a	a	k8xC
následné	následný	k2eAgFnSc6d1
podpoře	podpora	k1gFnSc6
jednotky	jednotka	k1gFnSc2
americké	americký	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
o	o	k7c4
2000	#num#	k4
mužů	muž	k1gMnPc2
(	(	kIx(
<g/>
Marine	Marin	k1gInSc5
Expeditionary	Expeditionara	k1gFnPc1
Unit	Unit	k1gInSc1
–	–	kI
MEU	MEU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
její	její	k3xOp3gFnSc2
paluby	paluba	k1gFnSc2
přepravována	přepravovat	k5eAaImNgNnP
vzduchem	vzduch	k1gInSc7
pomocí	pomoc	k1gFnPc2
vrtulníků	vrtulník	k1gInPc2
<g/>
,	,	kIx,
či	či	k8xC
po	po	k7c6
moři	moře	k1gNnSc6
vyloďovacími	vyloďovací	k2eAgInPc7d1
čluny	člun	k1gInPc7
<g/>
,	,	kIx,
vznášedly	vznášedlo	k1gNnPc7
Landing	Landing	k1gInSc1
Craft	Craft	k1gMnSc1
Air	Air	k1gFnPc2
Cushion	Cushion	k1gInSc4
a	a	k8xC
obojživelnými	obojživelný	k2eAgNnPc7d1
vozidly	vozidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
paluby	paluba	k1gFnSc2
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
také	také	k9
mohou	moct	k5eAaImIp3nP
operovat	operovat	k5eAaImF
kolmostartující	kolmostartující	k2eAgInPc1d1
letouny	letoun	k1gInPc1
AV-8B	AV-8B	k1gFnSc1
Harrier	Harrier	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Oproti	oproti	k7c3
ostatním	ostatní	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
třídy	třída	k1gFnSc2
Wasp	Wasp	k1gInSc1
byla	být	k5eAaImAgFnS
Makin	Makin	k2eAgInSc4d1
Island	Island	k1gInSc4
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
modifikované	modifikovaný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
přechodem	přechod	k1gInSc7
k	k	k7c3
následující	následující	k2eAgFnSc3d1
třídě	třída	k1gFnSc3
America	Americ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
použitým	použitý	k2eAgInSc7d1
pohonným	pohonný	k2eAgInSc7d1
systémem	systém	k1gInSc7
koncepce	koncepce	k1gFnSc2
CODLOG	CODLOG	kA
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yRgNnSc3,k3yQgNnSc3,k3yIgNnSc3
má	mít	k5eAaImIp3nS
plavidlo	plavidlo	k1gNnSc1
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
<g/>
,	,	kIx,
lepší	dobrý	k2eAgFnSc3d2
ekonomii	ekonomie	k1gFnSc3
provozu	provoz	k1gInSc2
a	a	k8xC
o	o	k7c4
cca	cca	kA
90	#num#	k4
mužů	muž	k1gMnPc2
menší	malý	k2eAgFnSc4d2
posádku	posádka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedených	uvedený	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
některé	některý	k3yIgInPc1
prameny	pramen	k1gInPc1
považují	považovat	k5eAaImIp3nP
Makin	Makin	k2eAgInSc4d1
Island	Island	k1gInSc4
za	za	k7c4
samostatnou	samostatný	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
vnitřní	vnitřní	k2eAgFnPc4d1
změny	změna	k1gFnPc4
se	se	k3xPyFc4
loď	loď	k1gFnSc1
od	od	k7c2
svých	svůj	k3xOyFgNnPc2
sesterských	sesterský	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
vnějškově	vnějškově	k6eAd1
liší	lišit	k5eAaImIp3nP
pouze	pouze	k6eAd1
komíny	komín	k1gInPc7
nakloněnými	nakloněný	k2eAgInPc7d1
na	na	k7c4
pravobok	pravobok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Stavba	stavba	k1gFnSc1
USS	USS	kA
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
(	(	kIx(
<g/>
LHD-	LHD-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kontrakt	kontrakt	k1gInSc1
na	na	k7c4
stavbu	stavba	k1gFnSc4
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
byl	být	k5eAaImAgInS
zadán	zadán	k2eAgInSc1d1
roku	rok	k1gInSc2
2002	#num#	k4
loděnici	loděnice	k1gFnSc4
Northrop	Northrop	k1gInSc1
Grumman	Grumman	k1gMnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Litton	Litton	k1gInSc1
Ingalls	Ingallsa	k1gFnPc2
Shipbuilding	Shipbuilding	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
Pascagoule	Pascagoula	k1gFnSc6
ve	v	k7c6
státě	stát	k1gInSc6
Mississippi	Mississippi	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stavba	stavba	k1gFnSc1
plavidla	plavidlo	k1gNnSc2
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
v	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c4
vodu	voda	k1gFnSc4
byl	být	k5eAaImAgInS
trup	trup	k1gInSc1
lodě	loď	k1gFnSc2
spuštěn	spuštěn	k2eAgInSc4d1
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
hotového	hotový	k2eAgNnSc2d1
plavidla	plavidlo	k1gNnSc2
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
v	v	k7c6
prosinci	prosinec	k1gInSc6
2008	#num#	k4
a	a	k8xC
do	do	k7c2
operační	operační	k2eAgFnSc2d1
služby	služba	k1gFnSc2
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zpoždění	zpoždění	k1gNnSc4
stavby	stavba	k1gFnSc2
plavidla	plavidlo	k1gNnSc2
způsobil	způsobit	k5eAaPmAgInS
hurikán	hurikán	k1gInSc1
Katrina	Katrin	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
na	na	k7c4
Makin	Makin	k1gInSc4
Island	Island	k1gInSc1
způsobil	způsobit	k5eAaPmAgInS
škody	škoda	k1gFnPc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
cca	cca	kA
360	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Noční	noční	k2eAgNnSc1d1
doplňování	doplňování	k1gNnSc1
paliva	palivo	k1gNnSc2
vrtulníku	vrtulník	k1gInSc2
AH-1	AH-1	k1gFnSc2
</s>
<s>
Vybavení	vybavení	k1gNnSc1
</s>
<s>
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
je	být	k5eAaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
bojovým	bojový	k2eAgInSc7d1
řídícím	řídící	k2eAgInSc7d1
systémem	systém	k1gInSc7
SSDS	SSDS	kA
Mk	Mk	k1gFnSc1
<g/>
.2	.2	k4
Mod.	Mod.	k1gFnSc1
<g/>
3	#num#	k4
<g/>
A.	A.	kA
Radary	radar	k1gInPc4
dlouhého	dlouhý	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgNnSc1d1
například	například	k6eAd1
k	k	k7c3
navádění	navádění	k1gNnSc3
palubních	palubní	k2eAgFnPc2d1
stíhaček	stíhačka	k1gFnPc2
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
3D	3D	k4
typ	typ	k1gInSc1
SPS-48E	SPS-48E	k1gFnSc2
a	a	k8xC
2D	2D	k4
typ	typ	k1gInSc1
SPS-	SPS-	k1gFnSc2
<g/>
49	#num#	k4
<g/>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Detekci	detekce	k1gFnSc6
nepřátelských	přátelský	k2eNgFnPc2d1
protilodních	protilodní	k2eAgFnPc2d1
střel	střela	k1gFnPc2
zajišťuje	zajišťovat	k5eAaImIp3nS
radar	radar	k1gInSc1
SPQ-	SPQ-	k1gFnSc2
<g/>
9	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
hladinové	hladinový	k2eAgInPc4d1
cíle	cíl	k1gInPc4
monitoruje	monitorovat	k5eAaImIp3nS
radar	radar	k1gInSc1
SPS-	SPS-	k1gFnSc2
<g/>
67	#num#	k4
<g/>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
<g/>
5	#num#	k4
a	a	k8xC
k	k	k7c3
navigaci	navigace	k1gFnSc3
slouží	sloužit	k5eAaImIp3nS
radar	radar	k1gInSc1
SPS-	SPS-	k1gFnSc2
<g/>
73	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonar	sonar	k1gInSc1
je	být	k5eAaImIp3nS
typu	typa	k1gFnSc4
WSC-	WSC-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
systémem	systém	k1gInSc7
radiotechnického	radiotechnický	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
a	a	k8xC
rušení	rušení	k1gNnSc2
SLQ-	SLQ-	k1gFnSc2
<g/>
32	#num#	k4
<g/>
B	B	kA
<g/>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrannou	obranný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
plní	plnit	k5eAaImIp3nS
šest	šest	k4xCc1
vrhačů	vrhač	k1gMnPc2
klamných	klamný	k2eAgInPc2d1
cílů	cíl	k1gInPc2
Mk	Mk	k1gFnSc2
<g/>
.36	.36	k4
SRBOC	SRBOC	kA
<g/>
,	,	kIx,
pasivní	pasivní	k2eAgInPc1d1
koutové	koutový	k2eAgInPc1d1
odražeče	odražeč	k1gInPc1
AN	AN	kA
<g/>
/	/	kIx~
<g/>
SLQ-	SLQ-	k1gFnSc1
<g/>
49	#num#	k4
<g/>
,	,	kIx,
bóje	bóje	k1gFnSc1
SSQ-	SSQ-	k1gFnSc1
<g/>
95	#num#	k4
<g/>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
a	a	k8xC
vlečené	vlečený	k2eAgInPc4d1
cíle	cíl	k1gInPc4
AN	AN	kA
<g/>
/	/	kIx~
<g/>
SLQ-	SLQ-	k1gMnSc1
<g/>
25	#num#	k4
Nixie	Nixie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsadkové	výsadkový	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
</s>
<s>
Vznášedlo	vznášedlo	k1gNnSc1
LCAC	LCAC	kA
opouští	opouštět	k5eAaImIp3nS
palubní	palubní	k2eAgInSc1d1
dok	dok	k1gInSc1
</s>
<s>
Výsadek	výsadek	k1gInSc1
až	až	k9
1687	#num#	k4
vojáků	voják	k1gMnPc2
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
je	být	k5eAaImIp3nS
z	z	k7c2
lodí	loď	k1gFnPc2
přepravován	přepravovat	k5eAaImNgMnS
pomocí	pomoc	k1gFnSc7
vrtulníků	vrtulník	k1gInPc2
<g/>
,	,	kIx,
vyloďovacích	vyloďovací	k2eAgInPc2d1
člunů	člun	k1gInPc2
či	či	k8xC
vznášedel	vznášedlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hangár	hangár	k1gInSc1
pro	pro	k7c4
vrtulníky	vrtulník	k1gInPc4
zabírá	zabírat	k5eAaImIp3nS
zadní	zadní	k2eAgFnSc4d1
třetinu	třetina	k1gFnSc4
plavidla	plavidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
letovou	letový	k2eAgFnSc7d1
palubou	paluba	k1gFnSc7
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
dvěma	dva	k4xCgInPc7
výtahy	výtah	k1gInPc7
o	o	k7c6
nosnosti	nosnost	k1gFnSc6
34	#num#	k4
t.	t.	k?
Jeden	jeden	k4xCgInSc1
výtah	výtah	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
levoboku	levobok	k1gInSc6
a	a	k8xC
druhý	druhý	k4xOgInSc4
na	na	k7c6
pravoboku	pravobok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
sklopné	sklopný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
Makin	Makina	k1gFnPc2
Island	Island	k1gInSc4
činí	činit	k5eAaImIp3nS
42	#num#	k4
vrtulníků	vrtulník	k1gInPc2
CH-	CH-	k1gFnSc2
<g/>
46	#num#	k4
<g/>
,	,	kIx,
či	či	k8xC
28	#num#	k4
letadel	letadlo	k1gNnPc2
Harrier	Harrira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neseny	nesen	k2eAgInPc1d1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
další	další	k2eAgInPc4d1
typy	typ	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
bitevní	bitevní	k2eAgInPc1d1
vrtulníky	vrtulník	k1gInPc1
AH-1	AH-1	k1gMnSc2
Cobra	Cobr	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složení	složení	k1gNnSc1
leteckého	letecký	k2eAgInSc2d1
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
přizpůsobeno	přizpůsoben	k2eAgNnSc4d1
konkrétnímu	konkrétní	k2eAgInSc3d1
úkolu	úkol	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
loď	loď	k1gFnSc4
plní	plnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plánováno	plánován	k2eAgNnSc4d1
je	být	k5eAaImIp3nS
nasazení	nasazení	k1gNnSc4
až	až	k6eAd1
19	#num#	k4
nových	nový	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
F-	F-	k1gFnSc1
<g/>
35	#num#	k4
<g/>
B.	B.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyloďovací	vyloďovací	k2eAgInPc1d1
čluny	člun	k1gInPc1
a	a	k8xC
vznášedla	vznášedlo	k1gNnPc1
operují	operovat	k5eAaImIp3nP
ze	z	k7c2
zaplavitelného	zaplavitelný	k2eAgInSc2d1
palubního	palubní	k2eAgInSc2d1
doku	dok	k1gInSc2
umístěného	umístěný	k2eAgInSc2d1
na	na	k7c6
zádi	záď	k1gFnSc6
pod	pod	k7c7
hangárem	hangár	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dok	dok	k1gInSc1
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
91	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
šířku	šířka	k1gFnSc4
15	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
doku	dok	k1gInSc2
činí	činit	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
vznášedla	vznášedlo	k1gNnPc4
LCAC	LCAC	kA
<g/>
,	,	kIx,
nebo	nebo	k8xC
dva	dva	k4xCgInPc4
čluny	člun	k1gInPc4
LCU	LCU	kA
<g/>
,	,	kIx,
či	či	k8xC
šest	šest	k4xCc4
člunů	člun	k1gInPc2
LCM-	LCM-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
do	do	k7c2
něj	on	k3xPp3gMnSc2
umístit	umístit	k5eAaPmF
i	i	k9
40	#num#	k4
obojživelných	obojživelný	k2eAgInPc2d1
obrněných	obrněný	k2eAgInPc2d1
transportérů	transportér	k1gInPc2
AAV	AAV	kA
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
zaplavení	zaplavení	k1gNnSc6
se	se	k3xPyFc4
výtlak	výtlak	k1gInSc1
lodě	loď	k1gFnSc2
zvětší	zvětšit	k5eAaPmIp3nS
o	o	k7c4
dalších	další	k2eAgInPc2d1
15	#num#	k4
000	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
Kolmostartující	Kolmostartující	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
AV-8B	AV-8B	k1gMnSc1
Harrier	Harrier	k1gMnSc1
</s>
<s>
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
nese	nést	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
obranných	obranný	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgNnPc4
osminásobná	osminásobný	k2eAgNnPc4d1
odpalovací	odpalovací	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
Mk	Mk	k1gFnSc1
<g/>
.29	.29	k4
lze	lze	k6eAd1
využít	využít	k5eAaPmF
jak	jak	k8xS,k8xC
pro	pro	k7c4
starší	starší	k1gMnPc4
protiletadlové	protiletadlový	k2eAgFnSc2d1
řízené	řízený	k2eAgFnSc2d1
střely	střela	k1gFnSc2
krátkého	krátký	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
Sea	Sea	k1gMnSc1
Sparrow	Sparrow	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k9
pro	pro	k7c4
střely	střela	k1gFnPc4
středního	střední	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
ESSM	ESSM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blízkou	blízký	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
proti	proti	k7c3
letadlům	letadlo	k1gNnPc3
a	a	k8xC
protilodním	protilodní	k2eAgFnPc3d1
střelám	střela	k1gFnPc3
zajišťují	zajišťovat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
raketové	raketový	k2eAgInPc4d1
komplety	komplet	k1gInPc4
RAM	RAM	kA
a	a	k8xC
dva	dva	k4xCgInPc4
20	#num#	k4
<g/>
mm	mm	kA
kanónové	kanónový	k2eAgInPc4d1
komplety	komplet	k1gInPc4
Phalanx	Phalanx	k1gInSc1
Block	Block	k1gInSc4
IB	IB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplňkovou	doplňkový	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
tři	tři	k4xCgInPc1
25	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
M242	M242	k1gFnSc2
Bushmaster	Bushmastra	k1gFnPc2
a	a	k8xC
čtyři	čtyři	k4xCgNnPc4
12,7	12,7	k4
<g/>
mm	mm	kA
kulometů	kulomet	k1gInPc2
M	M	kA
<g/>
2	#num#	k4
<g/>
HB	HB	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
starších	starý	k2eAgFnPc2d2
jednotek	jednotka	k1gFnPc2
třídy	třída	k1gFnSc2
Wasp	Wasp	k1gInSc1
pohání	pohánět	k5eAaImIp3nS
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc4
hybridní	hybridní	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
CODLOG	CODLOG	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
tvoří	tvořit	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
plynové	plynový	k2eAgFnPc4d1
turbíny	turbína	k1gFnPc4
General	General	k1gMnPc2
Electric	Electrice	k1gInPc2
LM	LM	kA
<g/>
2500	#num#	k4
<g/>
+	+	kIx~
s	s	k7c7
výkonem	výkon	k1gInSc7
po	po	k7c4
26	#num#	k4
110	#num#	k4
kW	kW	kA
a	a	k8xC
dva	dva	k4xCgInPc4
elektromotory	elektromotor	k1gInPc4
Alstom	Alstom	k1gInSc1
s	s	k7c7
výkonem	výkon	k1gInSc7
po	po	k7c4
3730	#num#	k4
kW	kW	kA
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
energii	energie	k1gFnSc4
dodává	dodávat	k5eAaImIp3nS
šest	šest	k4xCc1
dieselgenerátorů	dieselgenerátor	k1gInPc2
Fairbanks-Morse	Fairbanks-Morse	k1gFnSc2
s	s	k7c7
výkonem	výkon	k1gInSc7
po	po	k7c4
4000	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojice	dvojice	k1gFnSc1
lodních	lodní	k2eAgInPc2d1
šroubů	šroub	k1gInPc2
Rolls	Rollsa	k1gFnPc2
Royce	Royce	k1gFnSc2
má	mít	k5eAaImIp3nS
nastavitelné	nastavitelný	k2eAgFnSc2d1
lopatky	lopatka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
elektromotory	elektromotor	k1gInPc1
loď	loď	k1gFnSc1
pohánějí	pohánět	k5eAaImIp3nP
při	při	k7c6
rychlostech	rychlost	k1gFnPc6
do	do	k7c2
13	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohonné	pohonný	k2eAgInPc1d1
systémy	systém	k1gInPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
ve	v	k7c6
dvou	dva	k4xCgFnPc6
strojovnách	strojovna	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
řešení	řešení	k1gNnSc1
přináší	přinášet	k5eAaImIp3nS
oproti	oproti	k7c3
sesterským	sesterský	k2eAgMnPc3d1
lodím	lodit	k5eAaImIp1nS
značnou	značný	k2eAgFnSc4d1
úsporu	úspora	k1gFnSc4
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
přispívá	přispívat	k5eAaImIp3nS
také	také	k9
profilovaný	profilovaný	k2eAgInSc1d1
výstupek	výstupek	k1gInSc1
na	na	k7c6
zádi	záď	k1gFnSc6
trupu	trup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
je	být	k5eAaImIp3nS
9500	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
20	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
ZAJAC	ZAJAC	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USS	USS	kA
Makin	Makin	k2eAgInSc4d1
Island	Island	k1gInSc4
a	a	k8xC
USS	USS	kA
America	Americ	k1gInSc2
–	–	k?
generační	generační	k2eAgFnSc1d1
obnova	obnova	k1gFnSc1
výsadkových	výsadkový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
USN	USN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
45	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Zajac	Zajac	k1gFnSc1
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
LHD	LHD	kA
Wasp	Wasp	k1gInSc1
Class	Class	k1gInSc1
Amphibious	Amphibious	k1gInSc1
Assault	Assault	k2eAgInSc1d1
Ships	Ships	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval-technology	Naval-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Zajac	Zajac	k1gFnSc1
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
64	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zajac	Zajac	k1gInSc1
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInSc1d1
námořní	námořní	k2eAgInSc1d1
boj	boj	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
1037	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgFnPc2d1
vrtulníkových	vrtulníkový	k2eAgFnPc2d1
výsadkových	výsadkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
USS	USS	kA
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Americké	americký	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
třídy	třída	k1gFnSc2
Wasp	Wasp	k1gMnSc1
</s>
<s>
Wasp	Wasp	k1gInSc1
•	•	k?
Essex	Essex	k1gInSc1
•	•	k?
Kearsarge	Kearsarg	k1gFnSc2
•	•	k?
Boxer	boxer	k1gMnSc1
•	•	k?
Bataan	Bataan	k1gMnSc1
•	•	k?
Bonhomme	bonhomme	k1gMnSc1
Richard	Richard	k1gMnSc1
•	•	k?
Iwo	Iwo	k1gMnSc1
Jima	Jim	k1gInSc2
•	•	k?
Makin	Makin	k2eAgInSc1d1
Island	Island	k1gInSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
Tarawa	Tarawus	k1gMnSc2
•	•	k?
Nástupce	nástupce	k1gMnSc2
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
America	America	k1gMnSc1
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgFnPc2d1
vrtulníkových	vrtulníkový	k2eAgFnPc2d1
výsadkových	výsadkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
