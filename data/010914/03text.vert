<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Nedbal	Nedbal	k1gMnSc1	Nedbal
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Schwarzbach	Schwarzbach	k1gInSc1	Schwarzbach
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
organizátor	organizátor	k1gMnSc1	organizátor
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
českého	český	k2eAgMnSc2d1	český
dirigenta	dirigent	k1gMnSc2	dirigent
a	a	k8xC	a
skladatele	skladatel	k1gMnSc2	skladatel
Oskara	Oskar	k1gMnSc2	Oskar
Nedbala	Nedbal	k1gMnSc2	Nedbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Nedbal	Nedbal	k1gMnSc1	Nedbal
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
početné	početný	k2eAgFnSc2d1	početná
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
12	[number]	k4	12
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
rod	rod	k1gInSc1	rod
Nedbalů	Nedbal	k1gMnPc2	Nedbal
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Lomnice	Lomnice	k1gFnSc2	Lomnice
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
advokátní	advokátní	k2eAgMnSc1d1	advokátní
koncipient	koncipient	k1gMnSc1	koncipient
u	u	k7c2	u
švagra	švagr	k1gMnSc2	švagr
v	v	k7c6	v
Pelhřimově	Pelhřimov	k1gInSc6	Pelhřimov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
složení	složení	k1gNnSc6	složení
advokátní	advokátní	k2eAgFnSc2d1	advokátní
zkoušky	zkouška	k1gFnSc2	zkouška
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
si	se	k3xPyFc3	se
otevřel	otevřít	k5eAaPmAgInS	otevřít
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
právní	právní	k2eAgFnSc1d1	právní
kancelář	kancelář	k1gFnSc1	kancelář
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
zde	zde	k6eAd1	zde
v	v	k7c6	v
Palackého	Palackého	k2eAgFnSc6d1	Palackého
ulici	ulice	k1gFnSc6	ulice
355	[number]	k4	355
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
ředitelství	ředitelství	k1gNnSc2	ředitelství
Městské	městský	k2eAgFnSc2d1	městská
spořitelny	spořitelna	k1gFnSc2	spořitelna
Tábor	Tábor	k1gInSc4	Tábor
a	a	k8xC	a
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
aktivně	aktivně	k6eAd1	aktivně
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
radě	rada	k1gFnSc6	rada
a	a	k8xC	a
obecním	obecní	k2eAgNnSc6d1	obecní
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Annou	Anna	k1gFnSc7	Anna
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Hejnovou	Hejnová	k1gFnSc7	Hejnová
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
vychovali	vychovat	k5eAaPmAgMnP	vychovat
sedm	sedm	k4xCc4	sedm
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
vedli	vést	k5eAaImAgMnP	vést
je	on	k3xPp3gNnSc4	on
ke	k	k7c3	k
vztahu	vztah	k1gInSc3	vztah
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
šedesáti	šedesát	k4xCc6	šedesát
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
pražských	pražský	k2eAgInPc6d1	pražský
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zásluhy	zásluha	k1gFnPc1	zásluha
==	==	k?	==
</s>
</p>
<p>
<s>
JUDr.	JUDr.	kA	JUDr.
Karel	Karel	k1gMnSc1	Karel
Nedbal	Nedbal	k1gMnSc1	Nedbal
<g/>
,	,	kIx,	,
všestranně	všestranně	k6eAd1	všestranně
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
váženým	vážený	k2eAgMnSc7d1	vážený
občanem	občan	k1gMnSc7	občan
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
zejména	zejména	k9	zejména
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
hudebního	hudební	k2eAgNnSc2d1	hudební
dění	dění	k1gNnSc2	dění
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gNnSc4	jeho
domácí	domácí	k2eAgFnPc1d1	domácí
komorní	komorní	k2eAgFnPc1d1	komorní
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
hry	hra	k1gFnPc1	hra
účastnily	účastnit	k5eAaImAgFnP	účastnit
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nedbal	Nedbal	k1gMnSc1	Nedbal
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
Hudební	hudební	k2eAgInSc1d1	hudební
spolek	spolek	k1gInSc1	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Nedbalovou	Nedbalův	k2eAgFnSc7d1	Nedbalova
zásluhou	zásluha	k1gFnSc7	zásluha
rostl	růst	k5eAaImAgInS	růst
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
počet	počet	k1gInSc4	počet
veřejných	veřejný	k2eAgInPc2d1	veřejný
koncertů	koncert	k1gInPc2	koncert
i	i	k8xC	i
zájem	zájem	k1gInSc1	zájem
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Hostem	host	k1gMnSc7	host
rodiny	rodina	k1gFnSc2	rodina
Nedbalovy	Nedbalův	k2eAgFnSc2d1	Nedbalova
býval	bývat	k5eAaImAgMnS	bývat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fibich	Fibich	k1gMnSc1	Fibich
a	a	k8xC	a
také	také	k9	také
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
,	,	kIx,	,
např.	např.	kA	např.
i	i	k9	i
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
divadle	divadlo	k1gNnSc6	divadlo
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
koncert	koncert	k1gInSc1	koncert
a	a	k8xC	a
přítomný	přítomný	k2eAgMnSc1d1	přítomný
skladatel	skladatel	k1gMnSc1	skladatel
sklidil	sklidit	k5eAaPmAgMnS	sklidit
velké	velký	k2eAgFnPc4d1	velká
ovace	ovace	k1gFnPc4	ovace
po	po	k7c6	po
zaznění	zaznění	k1gNnSc6	zaznění
svých	svůj	k3xOyFgInPc2	svůj
Slovanských	slovanský	k2eAgInPc2d1	slovanský
tanců	tanec	k1gInPc2	tanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Nedbal	Nedbal	k1gMnSc1	Nedbal
má	mít	k5eAaImIp3nS	mít
zejména	zejména	k9	zejména
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
podchycení	podchycení	k1gNnSc4	podchycení
talentu	talent	k1gInSc2	talent
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Oskara	Oskar	k1gMnSc2	Oskar
a	a	k8xC	a
umožnění	umožnění	k1gNnSc2	umožnění
jeho	jeho	k3xOp3gNnSc2	jeho
hudebního	hudební	k2eAgNnSc2d1	hudební
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
generacích	generace	k1gFnPc6	generace
Nedbalů	Nedbal	k1gMnPc2	Nedbal
<g/>
:	:	kIx,	:
Před	před	k7c7	před
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
–	–	k?	–
R.	R.	kA	R.
Kukačka	kukačka	k1gFnSc1	kukačka
(	(	kIx(	(
<g/>
Týdeník	týdeník	k1gInSc1	týdeník
Kotnov-Palcát	Kotnov-Palcát	k1gInSc1	Kotnov-Palcát
<g/>
,	,	kIx,	,
č.	č.	k?	č.
41	[number]	k4	41
(	(	kIx(	(
<g/>
26.10	[number]	k4	26.10
<g/>
.2005	.2005	k4	.2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
</s>
</p>
<p>
<s>
Šulc	Šulc	k1gMnSc1	Šulc
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
nesmrtelnosti	nesmrtelnost	k1gFnSc3	nesmrtelnost
Oskara	Oskar	k1gMnSc2	Oskar
Nedbala	Nedbal	k1gMnSc2	Nedbal
<g/>
;	;	kIx,	;
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
–	–	k?	–
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Pokerová	pokerový	k2eAgFnSc1d1	pokerová
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-903601-7-4	[number]	k4	978-80-903601-7-4
</s>
</p>
<p>
<s>
Burghauser	Burghauser	k1gInSc1	Burghauser
Jarmil	Jarmila	k1gFnPc2	Jarmila
<g/>
:	:	kIx,	:
Slavní	slavný	k2eAgMnPc1d1	slavný
čeští	český	k2eAgMnPc1d1	český
dirigenti	dirigent	k1gMnPc1	dirigent
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Státní	státní	k2eAgInSc1d1	státní
hud.	hud.	k?	hud.
vydavatelství	vydavatelství	k1gNnPc2	vydavatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
