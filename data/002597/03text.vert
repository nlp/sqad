<s>
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
brněnská	brněnský	k2eAgFnSc1d1	brněnská
výstavní	výstavní	k2eAgFnSc1d1	výstavní
instituce	instituce	k1gFnSc1	instituce
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
aktuální	aktuální	k2eAgNnSc4d1	aktuální
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelkou	ředitelka	k1gFnSc7	ředitelka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
Terezie	Terezie	k1gFnSc2	Terezie
Petišková	Petišková	k1gFnSc1	Petišková
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
DUMB	DUMB	kA	DUMB
spadá	spadat	k5eAaPmIp3nS	spadat
i	i	k9	i
Galerie	galerie	k1gFnSc1	galerie
G99	G99	k1gFnSc1	G99
a	a	k8xC	a
Dům	dům	k1gInSc1	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
DUMB	DUMB	kA	DUMB
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
Malinovského	Malinovský	k2eAgNnSc2d1	Malinovské
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
secese	secese	k1gFnSc2	secese
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
Künstlerhaus	Künstlerhaus	k1gInSc4	Künstlerhaus
čili	čili	k8xC	čili
Dům	dům	k1gInSc4	dům
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Fuchsem	Fuchs	k1gMnSc7	Fuchs
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
výstavy	výstava	k1gFnPc1	výstava
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
vernisáže	vernisáž	k1gFnSc2	vernisáž
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Chalupeckého	Chalupecký	k2eAgMnSc2d1	Chalupecký
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
instituci	instituce	k1gFnSc6	instituce
koná	konat	k5eAaImIp3nS	konat
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Barbora	Barbora	k1gFnSc1	Barbora
Klímová	Klímová	k1gFnSc1	Klímová
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
diváků	divák	k1gMnPc2	divák
získali	získat	k5eAaPmAgMnP	získat
Rafani	Rafan	k1gMnPc1	Rafan
<g/>
.	.	kIx.	.
</s>
