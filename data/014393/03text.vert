<s>
Hlavatice	hlavatice	k1gFnSc1
(	(	kIx(
<g/>
rozhledna	rozhledna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hlavatice	hlavatice	k1gFnSc1
HlavaticeZákladní	HlavaticeZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc1
Konstrukce	konstrukce	k1gFnSc2
rozhledny	rozhledna	k1gFnSc2
</s>
<s>
železná	železný	k2eAgFnSc1d1
Kóta	kóta	k1gFnSc1
paty	pata	k1gFnSc2
rozhledny	rozhledna	k1gFnSc2
</s>
<s>
380	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rok	rok	k1gInSc1
vzniku	vznik	k1gInSc2
</s>
<s>
1831	#num#	k4
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Semily	Semily	k1gInPc1
Souřadnice	souřadnice	k1gFnPc4
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
10,41	10,41	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
19,73	19,73	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Technické	technický	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc1
schodů	schod	k1gInPc2
</s>
<s>
38	#num#	k4
Stav	stav	k1gInSc1
</s>
<s>
volně	volně	k6eAd1
přístupná	přístupný	k2eAgFnSc1d1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavatice	hlavatice	k1gFnSc1
je	být	k5eAaImIp3nS
vyhlídkové	vyhlídkový	k2eAgNnSc1d1
místo	místo	k1gNnSc1
na	na	k7c6
okraji	okraj	k1gInSc6
skalního	skalní	k2eAgNnSc2d1
města	město	k1gNnSc2
u	u	k7c2
Hrubé	Hrubé	k2eAgFnSc2d1
Skály	skála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pískovcový	pískovcový	k2eAgInSc4d1
skalní	skalní	k2eAgInSc4d1
blok	blok	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yRgInSc4
vede	vést	k5eAaImIp3nS
točité	točitý	k2eAgNnSc1d1
železné	železný	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
s	s	k7c7
36	#num#	k4
stupni	stupeň	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc4
je	být	k5eAaImIp3nS
nahoře	nahoře	k6eAd1
doplněno	doplnit	k5eAaPmNgNnS
2	#num#	k4
schody	schod	k1gInPc7
z	z	k7c2
pískovce	pískovec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozhledna	rozhledna	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Mašov	Mašov	k1gInSc1
u	u	k7c2
Turnova	Turnov	k1gInSc2
poblíž	poblíž	k7c2
červené	červený	k2eAgFnSc2d1
turistické	turistický	k2eAgFnSc2d1
značky	značka	k1gFnSc2
2	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
Turnov	Turnov	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Turnov	Turnov	k1gInSc1
z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
Hlavatice	hlavatice	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Skalní	skalní	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
Hlavatice	hlavatice	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interregion	Interregion	k1gInSc1
Jičín	Jičín	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hruboskalsko	Hruboskalsko	k6eAd1
</s>
<s>
Valdštejn	Valdštejn	k1gInSc1
(	(	kIx(
<g/>
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hlavatice	hlavatice	k1gFnSc2
(	(	kIx(
<g/>
rozhledna	rozhledna	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
