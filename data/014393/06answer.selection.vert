<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pískovcový	pískovcový	k2eAgInSc4d1
skalní	skalní	k2eAgInSc4d1
blok	blok	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yRgInSc4
vede	vést	k5eAaImIp3nS
točité	točitý	k2eAgNnSc1d1
železné	železný	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
s	s	k7c7
36	#num#	k4
stupni	stupeň	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc4
je	být	k5eAaImIp3nS
nahoře	nahoře	k6eAd1
doplněno	doplnit	k5eAaPmNgNnS
2	#num#	k4
schody	schod	k1gInPc7
z	z	k7c2
pískovce	pískovec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozhledna	rozhledna	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Mašov	Mašov	k1gInSc1
u	u	k7c2
Turnova	Turnov	k1gInSc2
poblíž	poblíž	k7c2
červené	červený	k2eAgFnSc2d1
turistické	turistický	k2eAgFnSc2d1
značky	značka	k1gFnSc2
2	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
Turnov	Turnov	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
<g/>
.	.	kIx.
</s>