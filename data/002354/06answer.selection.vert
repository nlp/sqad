<s>
Horečka	horečka	k1gFnSc1	horečka
omladnic	omladnice	k1gFnPc2	omladnice
(	(	kIx(	(
<g/>
jiným	jiný	k2eAgInSc7d1	jiný
názvem	název	k1gInSc7	název
též	též	k6eAd1	též
poporodní	poporodní	k2eAgFnSc1d1	poporodní
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
febris	febris	k1gInSc1	febris
puerperalis	puerperalis	k1gInSc1	puerperalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
do	do	k7c2	do
poznání	poznání	k1gNnSc2	poznání
a	a	k8xC	a
zavedení	zavedení	k1gNnSc2	zavedení
zásad	zásada	k1gFnPc2	zásada
antisepse	antisepse	k1gFnSc2	antisepse
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
často	často	k6eAd1	často
napadala	napadat	k5eAaImAgFnS	napadat
ženy	žena	k1gFnPc4	žena
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
