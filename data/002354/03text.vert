<s>
Horečka	horečka	k1gFnSc1	horečka
omladnic	omladnice	k1gFnPc2	omladnice
(	(	kIx(	(
<g/>
jiným	jiný	k2eAgInSc7d1	jiný
názvem	název	k1gInSc7	název
též	též	k6eAd1	též
poporodní	poporodní	k2eAgFnSc1d1	poporodní
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
febris	febris	k1gInSc1	febris
puerperalis	puerperalis	k1gInSc1	puerperalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
do	do	k7c2	do
poznání	poznání	k1gNnSc2	poznání
a	a	k8xC	a
zavedení	zavedení	k1gNnSc2	zavedení
zásad	zásada	k1gFnPc2	zásada
antisepse	antisepse	k1gFnSc2	antisepse
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
často	často	k6eAd1	často
napadala	napadat	k5eAaBmAgFnS	napadat
ženy	žena	k1gFnPc4	žena
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
infekci	infekce	k1gFnSc4	infekce
(	(	kIx(	(
<g/>
jejím	její	k3xOp3gMnSc7	její
původcem	původce	k1gMnSc7	původce
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pyogenes	pyogenes	k1gMnSc1	pyogenes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ženě	žena	k1gFnSc3	žena
napadené	napadený	k2eAgNnSc1d1	napadené
horečkou	horečka	k1gFnSc7	horečka
omladnic	omladnice	k1gFnPc2	omladnice
výrazně	výrazně	k6eAd1	výrazně
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
vyrážka	vyrážka	k1gFnSc1	vyrážka
a	a	k8xC	a
rány	rána	k1gFnPc1	rána
začnou	začít	k5eAaPmIp3nP	začít
hnisat	hnisat	k5eAaImF	hnisat
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
30	[number]	k4	30
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
pracoval	pracovat	k5eAaImAgMnS	pracovat
maďarský	maďarský	k2eAgMnSc1d1	maďarský
lékař	lékař	k1gMnSc1	lékař
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
Ignác	Ignác	k1gMnSc1	Ignác
Filip	Filip	k1gMnSc1	Filip
Semmelweis	Semmelweis	k1gFnSc4	Semmelweis
ve	v	k7c6	v
Všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
porodním	porodní	k2eAgNnSc6d1	porodní
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působili	působit	k5eAaImAgMnP	působit
studenti	student	k1gMnPc1	student
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
rodiček	rodička	k1gFnPc2	rodička
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
odděleních	oddělení	k1gNnPc6	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Semmelweis	Semmelweis	k1gInSc1	Semmelweis
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgInS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
studenti	student	k1gMnPc1	student
přenášejí	přenášet	k5eAaImIp3nP	přenášet
na	na	k7c6	na
nemytých	mytý	k2eNgFnPc6d1	nemytá
rukou	ruka	k1gFnPc6	ruka
nemoc	nemoc	k1gFnSc1	nemoc
z	z	k7c2	z
pitevny	pitevna	k1gFnSc2	pitevna
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nutil	nutit	k5eAaImAgInS	nutit
studenty	student	k1gMnPc4	student
mýt	mýt	k5eAaImF	mýt
si	se	k3xPyFc3	se
ruce	ruka	k1gFnPc4	ruka
ve	v	k7c6	v
chlorovém	chlorový	k2eAgNnSc6d1	chlorové
vápně	vápno	k1gNnSc6	vápno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
že	že	k9	že
na	na	k7c4	na
horečku	horečka	k1gFnSc4	horečka
omladnic	omladnice	k1gFnPc2	omladnice
začalo	začít	k5eAaPmAgNnS	začít
umírat	umírat	k5eAaImF	umírat
řádově	řádově	k6eAd1	řádově
méně	málo	k6eAd2	málo
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
jej	on	k3xPp3gMnSc4	on
propustili	propustit	k5eAaPmAgMnP	propustit
a	a	k8xC	a
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
opět	opět	k6eAd1	opět
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
dostal	dostat	k5eAaPmAgInS	dostat
Semmelweis	Semmelweis	k1gInSc1	Semmelweis
místo	místo	k7c2	místo
porodníka	porodník	k1gMnSc2	porodník
v	v	k7c6	v
Pešti	Pešť	k1gFnSc6	Pešť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
nutil	nutit	k5eAaImAgMnS	nutit
studenty	student	k1gMnPc4	student
mýt	mýt	k5eAaImF	mýt
si	se	k3xPyFc3	se
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
stejně	stejně	k6eAd1	stejně
dobré	dobrý	k2eAgInPc4d1	dobrý
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
obecného	obecný	k2eAgNnSc2d1	obecné
uznání	uznání	k1gNnSc2	uznání
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lékaři	lékař	k1gMnPc1	lékař
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
odmítali	odmítat	k5eAaImAgMnP	odmítat
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
nemoc	nemoc	k1gFnSc4	nemoc
způsobovali	způsobovat	k5eAaImAgMnP	způsobovat
sami	sám	k3xTgMnPc1	sám
svýma	svůj	k3xOyFgFnPc7	svůj
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Semmelweis	Semmelweis	k1gInSc1	Semmelweis
navíc	navíc	k6eAd1	navíc
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
svá	svůj	k3xOyFgNnPc4	svůj
pozorování	pozorování	k1gNnPc4	pozorování
dostatečně	dostatečně	k6eAd1	dostatečně
vědecky	vědecky	k6eAd1	vědecky
odůvodnit	odůvodnit	k5eAaPmF	odůvodnit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
psychická	psychický	k2eAgFnSc1d1	psychická
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
záhy	záhy	k6eAd1	záhy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
patrně	patrně	k6eAd1	patrně
na	na	k7c4	na
následky	následek	k1gInPc4	následek
hrubého	hrubý	k2eAgNnSc2d1	hrubé
zacházení	zacházení	k1gNnSc2	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
formulované	formulovaný	k2eAgFnPc4d1	formulovaná
zásady	zásada	k1gFnPc4	zásada
asepse	asepse	k1gFnSc2	asepse
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
obecně	obecně	k6eAd1	obecně
přijímány	přijímat	k5eAaImNgInP	přijímat
teprve	teprve	k6eAd1	teprve
o	o	k7c4	o
dekádu	dekáda	k1gFnSc4	dekáda
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Pasteurovým	Pasteurův	k2eAgInSc7d1	Pasteurův
objevem	objev	k1gInSc7	objev
mikroskopických	mikroskopický	k2eAgMnPc2d1	mikroskopický
původců	původce	k1gMnPc2	původce
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Semmelweisem	Semmelweis	k1gInSc7	Semmelweis
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
také	také	k9	také
Wendell	Wendell	k1gMnSc1	Wendell
Holmes	Holmes	k1gMnSc1	Holmes
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mikrobi	mikrob	k1gMnPc1	mikrob
šíří	šířit	k5eAaImIp3nP	šířit
na	na	k7c6	na
nemytých	mytý	k2eNgFnPc6d1	nemytá
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
prevenci	prevence	k1gFnSc3	prevence
nezkoušel	zkoušet	k5eNaImAgInS	zkoušet
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k2eAgMnSc1d1	skotský
lékař	lékař	k1gMnSc1	lékař
Alexander	Alexandra	k1gFnPc2	Alexandra
Gordon	Gordona	k1gFnPc2	Gordona
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
publikoval	publikovat	k5eAaBmAgMnS	publikovat
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
připouštěl	připouštět	k5eAaImAgMnS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc4	sám
jako	jako	k9	jako
porodník	porodník	k1gMnSc1	porodník
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
přenašečem	přenašeč	k1gMnSc7	přenašeč
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Kolečko	kolečko	k1gNnSc4	kolečko
</s>
