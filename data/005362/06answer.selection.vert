<s>
Oproti	oproti	k7c3	oproti
moderním	moderní	k2eAgNnPc3d1	moderní
písmům	písmo	k1gNnPc3	písmo
původem	původ	k1gInSc7	původ
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
alfabety	alfabeta	k1gFnSc2	alfabeta
včetně	včetně	k7c2	včetně
latinky	latinka	k1gFnSc2	latinka
<g/>
,	,	kIx,	,
nezná	neznat	k5eAaImIp3nS	neznat
znaky	znak	k1gInPc4	znak
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
písem	písmo	k1gNnPc2	písmo
vytvořených	vytvořený	k2eAgNnPc2d1	vytvořené
pro	pro	k7c4	pro
semitské	semitský	k2eAgInPc4d1	semitský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
