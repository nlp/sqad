<s>
Fénické	fénický	k2eAgNnSc1d1	fénické
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
písmo	písmo	k1gNnSc4	písmo
používané	používaný	k2eAgFnSc2d1	používaná
pro	pro	k7c4	pro
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
textů	text	k1gInPc2	text
ve	v	k7c6	v
féničtině	féničtina	k1gFnSc6	féničtina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
kanaánských	kanaánský	k2eAgInPc6d1	kanaánský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Fénické	fénický	k2eAgInPc1d1	fénický
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
fénicko-kanaánské	fénickoanaánský	k2eAgInPc1d1	fénicko-kanaánský
<g/>
)	)	kIx)	)
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
protosinajského	protosinajský	k2eAgNnSc2d1	protosinajský
patrně	patrně	k6eAd1	patrně
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
(	(	kIx(	(
<g/>
Fénicie	Fénicie	k1gFnSc1	Fénicie
nebo	nebo	k8xC	nebo
Kanaán	Kanaán	k1gInSc1	Kanaán
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fénické	fénický	k2eAgNnSc1d1	fénické
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgNnSc1d1	vycházející
z	z	k7c2	z
akrofonického	akrofonický	k2eAgNnSc2d1	akrofonický
protosinajského	protosinajský	k2eAgNnSc2d1	protosinajský
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
písmem	písmo	k1gNnSc7	písmo
hláskovým	hláskový	k2eAgNnSc7d1	hláskové
<g/>
,	,	kIx,	,
fonetickým	fonetický	k2eAgNnSc7d1	fonetické
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
znaků	znak	k1gInPc2	znak
označujících	označující	k2eAgFnPc2d1	označující
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
moderním	moderní	k2eAgNnPc3d1	moderní
písmům	písmo	k1gNnPc3	písmo
původem	původ	k1gInSc7	původ
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
alfabety	alfabeta	k1gFnSc2	alfabeta
včetně	včetně	k7c2	včetně
latinky	latinka	k1gFnSc2	latinka
<g/>
,	,	kIx,	,
nezná	neznat	k5eAaImIp3nS	neznat
znaky	znak	k1gInPc4	znak
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
písem	písmo	k1gNnPc2	písmo
vytvořených	vytvořený	k2eAgNnPc2d1	vytvořené
pro	pro	k7c4	pro
semitské	semitský	k2eAgInPc4d1	semitský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Psalo	psát	k5eAaImAgNnS	psát
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
místních	místní	k2eAgFnPc2d1	místní
variant	varianta	k1gFnPc2	varianta
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
písmo	písmo	k1gNnSc1	písmo
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
zpočátku	zpočátku	k6eAd1	zpočátku
velmi	velmi	k6eAd1	velmi
podobných	podobný	k2eAgFnPc2d1	podobná
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Fénické	fénický	k2eAgNnSc1d1	fénické
písmo	písmo	k1gNnSc1	písmo
tedy	tedy	k9	tedy
stojí	stát	k5eAaImIp3nS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
mnoha	mnoho	k4c2	mnoho
dnešních	dnešní	k2eAgNnPc2d1	dnešní
písem	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
písma	písmo	k1gNnPc4	písmo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
starořecké	starořecký	k2eAgNnSc4d1	starořecké
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
dále	daleko	k6eAd2	daleko
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řecká	řecký	k2eAgFnSc1d1	řecká
alfabeta	alfabeta	k1gFnSc1	alfabeta
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vycházející	vycházející	k2eAgFnSc2d1	vycházející
cyrilice	cyrilice	k1gFnSc2	cyrilice
<g/>
,	,	kIx,	,
etrusko-latinské	etruskoatinský	k2eAgNnSc4d1	etrusko-latinský
písmo	písmo	k1gNnSc4	písmo
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
latinka	latinka	k1gFnSc1	latinka
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
moabské	moabský	k2eAgNnSc4d1	moabský
písmo	písmo	k1gNnSc4	písmo
staroaramejské	staroaramejský	k2eAgNnSc1d1	staroaramejský
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
předkem	předek	k1gMnSc7	předek
kvadrátního	kvadrátní	k2eAgInSc2d1	kvadrátní
<g />
.	.	kIx.	.
</s>
<s>
aramejského	aramejský	k2eAgNnSc2d1	aramejské
písma	písmo	k1gNnSc2	písmo
užívaného	užívaný	k2eAgInSc2d1	užívaný
klasickou	klasický	k2eAgFnSc7d1	klasická
i	i	k8xC	i
moderní	moderní	k2eAgFnSc7d1	moderní
hebrejštinou	hebrejština	k1gFnSc7	hebrejština
palmýrského	palmýrský	k2eAgNnSc2d1	palmýrský
a	a	k8xC	a
nabatejského	nabatejský	k2eAgNnSc2d1	nabatejské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
syrské	syrský	k2eAgNnSc1d1	syrské
písmo	písmo	k1gNnSc1	písmo
a	a	k8xC	a
arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
indického	indický	k2eAgMnSc2d1	indický
kharóšthí	kharóšthí	k6eAd1	kharóšthí
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
indických	indický	k2eAgNnPc2d1	indické
písem	písmo	k1gNnPc2	písmo
ale	ale	k8xC	ale
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
bráhmí	bráhmí	k1gNnSc2	bráhmí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
a	a	k8xC	a
inspirace	inspirace	k1gFnPc4	inspirace
aramejským	aramejský	k2eAgFnPc3d1	aramejská
či	či	k8xC	či
spíše	spíše	k9	spíše
fénickým	fénický	k2eAgNnSc7d1	fénické
písmem	písmo	k1gNnSc7	písmo
možná	možná	k9	možná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
starojihoarabské	starojihoarabský	k2eAgNnSc1d1	starojihoarabský
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dalo	dát	k5eAaPmAgNnS	dát
vznik	vznik	k1gInSc4	vznik
etiopskému	etiopský	k2eAgNnSc3d1	etiopské
písmu	písmo	k1gNnSc3	písmo
<g/>
,	,	kIx,	,
starohebrejské	starohebrejský	k2eAgNnSc4d1	starohebrejský
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
písmo	písmo	k1gNnSc1	písmo
samaritánské	samaritánský	k2eAgNnSc1d1	samaritánské
<g/>
.	.	kIx.	.
</s>
