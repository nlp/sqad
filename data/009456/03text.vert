<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Verner	Verner	k1gMnSc1	Verner
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Písek	Písek	k1gInSc1	Písek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
moderátor	moderátor	k1gMnSc1	moderátor
uvádí	uvádět	k5eAaImIp3nS	uvádět
pořad	pořad	k1gInSc4	pořad
Škoda	Škoda	k1gMnSc1	Škoda
olympijské	olympijský	k2eAgFnSc2d1	olympijská
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Olympijského	olympijský	k2eAgInSc2d1	olympijský
magazínu	magazín	k1gInSc2	magazín
na	na	k7c6	na
ČT	ČT	kA	ČT
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
pořad	pořad	k1gInSc1	pořad
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
na	na	k7c4	na
TV	TV	kA	TV
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
předvedl	předvést	k5eAaPmAgInS	předvést
krátký	krátký	k2eAgInSc1d1	krátký
program	program	k1gInSc1	program
na	na	k7c4	na
skladbu	skladba	k1gFnSc4	skladba
Tokáta	tokáta	k1gFnSc1	tokáta
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
d-moll	dolla	k1gFnPc2	d-molla
od	od	k7c2	od
Johanna	Johann	k1gMnSc2	Johann
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
<s>
Startoval	startovat	k5eAaBmAgMnS	startovat
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
32	[number]	k4	32
a	a	k8xC	a
do	do	k7c2	do
krátkého	krátký	k2eAgInSc2d1	krátký
programu	program	k1gInSc2	program
vyjel	vyjet	k5eAaPmAgInS	vyjet
jako	jako	k9	jako
poslední	poslední	k2eAgInSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Náskok	náskok	k1gInSc1	náskok
na	na	k7c4	na
největšího	veliký	k2eAgMnSc4d3	veliký
favorita	favorit	k1gMnSc4	favorit
–	–	k?	–
Francouze	Francouz	k1gMnSc4	Francouz
Briana	Brian	k1gMnSc4	Brian
Jouberta	Joubert	k1gMnSc4	Joubert
(	(	kIx(	(
<g/>
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
však	však	k9	však
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
jízdě	jízda	k1gFnSc6	jízda
neudržel	udržet	k5eNaPmAgMnS	udržet
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
kvůli	kvůli	k7c3	kvůli
pádu	pád	k1gInSc3	pád
při	při	k7c6	při
čtverném	čtverný	k2eAgInSc6d1	čtverný
toeloopu	toeloop	k1gInSc6	toeloop
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
třetí	třetí	k4xOgFnSc3	třetí
nejlepší	dobrý	k2eAgFnSc3d3	nejlepší
volné	volný	k2eAgFnSc3d1	volná
jízdě	jízda	k1gFnSc3	jízda
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
po	po	k7c6	po
Joubertovi	Joubert	k1gMnSc6	Joubert
a	a	k8xC	a
Belgičanu	Belgičan	k1gMnSc6	Belgičan
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Perrenovi	Perrenův	k2eAgMnPc1d1	Perrenův
<g/>
)	)	kIx)	)
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
obsadil	obsadit	k5eAaPmAgMnS	obsadit
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
dosavadním	dosavadní	k2eAgInSc7d1	dosavadní
sportovním	sportovní	k2eAgInSc7d1	sportovní
úspěchem	úspěch	k1gInSc7	úspěch
Tomáše	Tomáš	k1gMnSc2	Tomáš
Vernera	Verner	k1gMnSc2	Verner
je	být	k5eAaImIp3nS	být
zisk	zisk	k1gInSc4	zisk
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
2008	[number]	k4	2008
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
předchozí	předchozí	k2eAgInSc4d1	předchozí
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
úspěch	úspěch	k1gInSc4	úspěch
z	z	k7c2	z
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Ziskem	zisk	k1gInSc7	zisk
232,67	[number]	k4	232,67
bodů	bod	k1gInPc2	bod
porazil	porazit	k5eAaPmAgInS	porazit
Švýcara	Švýcar	k1gMnSc4	Švýcar
Lambiela	Lambiel	k1gMnSc4	Lambiel
i	i	k9	i
třetího	třetí	k4xOgMnSc4	třetí
Francouze	Francouz	k1gMnSc4	Francouz
Jouberta	Joubert	k1gMnSc4	Joubert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2009	[number]	k4	2009
figuroval	figurovat	k5eAaImAgInS	figurovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
krasobruslařů	krasobruslař	k1gMnPc2	krasobruslař
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
2011	[number]	k4	2011
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
získal	získat	k5eAaPmAgMnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bruslil	bruslit	k5eAaImAgMnS	bruslit
na	na	k7c4	na
exhibici	exhibice	k1gFnSc4	exhibice
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
narozenin	narozeniny	k1gFnPc2	narozeniny
severokorejského	severokorejský	k2eAgMnSc2d1	severokorejský
diktátora	diktátor	k1gMnSc2	diktátor
Kim	Kim	k1gMnSc2	Kim
Čong-ila	Čongl	k1gMnSc2	Čong-il
<g/>
.	.	kIx.	.
</s>
<s>
Verner	Verner	k1gMnSc1	Verner
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
KLDR	KLDR	kA	KLDR
jel	jet	k5eAaImAgMnS	jet
"	"	kIx"	"
<g/>
zpestřit	zpestřit	k5eAaPmF	zpestřit
život	život	k1gInSc4	život
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
to	ten	k3xDgNnSc4	ten
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
odloženému	odložený	k2eAgNnSc3d1	odložené
exhibičnímu	exhibiční	k2eAgNnSc3d1	exhibiční
turné	turné	k1gNnSc3	turné
s	s	k7c7	s
Jevgenijem	Jevgenij	k1gMnSc7	Jevgenij
Pljuščenkem	Pljuščenek	k1gMnSc7	Pljuščenek
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
startovat	startovat	k5eAaBmF	startovat
ještě	ještě	k9	ještě
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	s	k7c7	s
závodním	závodní	k2eAgNnSc7d1	závodní
krasobruslením	krasobruslení	k1gNnSc7	krasobruslení
definitivně	definitivně	k6eAd1	definitivně
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
výsledků	výsledek	k1gInPc2	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tomáš	Tomáš	k1gMnSc1	Tomáš
Verner	Verner	k1gMnSc1	Verner
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Tomáše	Tomáš	k1gMnSc2	Tomáš
Vernera	Verner	k1gMnSc2	Verner
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Tomáš	Tomáš	k1gMnSc1	Tomáš
Verner	Verner	k1gMnSc1	Verner
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
bruslařské	bruslařský	k2eAgFnSc2d1	bruslařská
unie	unie	k1gFnSc2	unie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Tomáš	Tomáš	k1gMnSc1	Tomáš
Verner	Verner	k1gMnSc1	Verner
<g/>
,	,	kIx,	,
sports-reference	sportseference	k1gFnSc1	sports-reference
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
