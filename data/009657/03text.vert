<p>
<s>
Glee	Glee	k1gFnSc1	Glee
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
Journey	Journe	k2eAgMnPc4d1	Journe
to	ten	k3xDgNnSc1	ten
Regionals	Regionals	k1gInSc4	Regionals
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
EP	EP	kA	EP
amerického	americký	k2eAgInSc2d1	americký
televizního	televizní	k2eAgInSc2d1	televizní
muzikálového	muzikálový	k2eAgInSc2d1	muzikálový
seriálu	seriál	k1gInSc2	seriál
Glee	Gle	k1gFnSc2	Gle
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
epizody	epizoda	k1gFnSc2	epizoda
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
s	s	k7c7	s
názvem	název	k1gInSc7	název
Cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
epizoda	epizoda	k1gFnSc1	epizoda
poprvé	poprvé	k6eAd1	poprvé
vysílala	vysílat	k5eAaImAgFnS	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
písní	píseň	k1gFnPc2	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
jsou	být	k5eAaImIp3nP	být
cover	cover	k1gInSc4	cover
verze	verze	k1gFnSc2	verze
písní	píseň	k1gFnPc2	píseň
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Journey	Journea	k1gFnSc2	Journea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tracklist	Tracklist	k1gInSc4	Tracklist
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Interpreti	interpret	k1gMnPc5	interpret
==	==	k?	==
</s>
</p>
<p>
<s>
Dianna	Dianna	k1gFnSc1	Dianna
Agron	Agrona	k1gFnPc2	Agrona
</s>
</p>
<p>
<s>
Chris	Chris	k1gFnSc1	Chris
Colfer	Colfra	k1gFnPc2	Colfra
</s>
</p>
<p>
<s>
Jane	Jan	k1gMnSc5	Jan
Lynch	Lynch	k1gMnSc1	Lynch
</s>
</p>
<p>
<s>
Jayma	Jayma	k1gFnSc1	Jayma
Mays	Maysa	k1gFnPc2	Maysa
</s>
</p>
<p>
<s>
Kevin	Kevin	k1gMnSc1	Kevin
McHale	McHala	k1gFnSc3	McHala
</s>
</p>
<p>
<s>
Lea	Lea	k1gFnSc1	Lea
Michele	Michel	k1gInSc2	Michel
</s>
</p>
<p>
<s>
Cory	Cora	k1gFnPc1	Cora
Monteith	Monteitha	k1gFnPc2	Monteitha
</s>
</p>
<p>
<s>
Matthew	Matthew	k?	Matthew
Morrison	Morrison	k1gInSc1	Morrison
</s>
</p>
<p>
<s>
Amber	ambra	k1gFnPc2	ambra
Riley	Rilea	k1gFnSc2	Rilea
</s>
</p>
<p>
<s>
Naya	Nay	k2eAgFnSc1d1	Naya
Rivera	Rivera	k1gFnSc1	Rivera
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Salling	Salling	k1gInSc4	Salling
</s>
</p>
<p>
<s>
Jenna	Jenna	k1gFnSc1	Jenna
Ushkowitz	Ushkowitza	k1gFnPc2	Ushkowitza
</s>
</p>
<p>
<s>
===	===	k?	===
Hostující	hostující	k2eAgMnSc1d1	hostující
interpret	interpret	k1gMnSc1	interpret
===	===	k?	===
</s>
</p>
<p>
<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Groff	Groff	k1gMnSc1	Groff
</s>
</p>
<p>
<s>
==	==	k?	==
Datum	datum	k1gNnSc1	datum
vydání	vydání	k1gNnSc2	vydání
==	==	k?	==
</s>
</p>
