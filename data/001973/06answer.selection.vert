<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
stoletích	století	k1gNnPc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
dále	daleko	k6eAd2	daleko
rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
díky	díky	k7c3	díky
založení	založení	k1gNnSc3	založení
Vilniuské	vilniuský	k2eAgFnSc2d1	Vilniuská
univerzity	univerzita	k1gFnSc2	univerzita
králem	král	k1gMnSc7	král
a	a	k8xC	a
velkoknížetem	velkokníže	k1gMnSc7	velkokníže
Štěpánem	Štěpán	k1gMnSc7	Štěpán
Báthorym	Báthorym	k1gInSc4	Báthorym
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1579	[number]	k4	1579
<g/>
.	.	kIx.	.
</s>
