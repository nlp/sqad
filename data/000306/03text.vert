<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
politická	politický	k2eAgFnSc1d1	politická
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
od	od	k7c2	od
posledního	poslední	k2eAgNnSc2d1	poslední
rozšíření	rozšíření	k1gNnSc2	rozšíření
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
tvoří	tvořit	k5eAaImIp3nS	tvořit
28	[number]	k4	28
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
s	s	k7c7	s
510,3	[number]	k4	510,3
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
;	;	kIx,	;
přibližně	přibližně	k6eAd1	přibližně
7,3	[number]	k4	7,3
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
EU	EU	kA	EU
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Smlouvy	smlouva	k1gFnPc1	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
jako	jako	k8xS	jako
Maastrichtská	maastrichtský	k2eAgFnSc1d1	Maastrichtská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
tak	tak	k6eAd1	tak
Evropské	evropský	k2eAgNnSc1d1	Evropské
společenství	společenství	k1gNnSc1	společenství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
integrace	integrace	k1gFnSc1	integrace
probíhá	probíhat	k5eAaImIp3nS	probíhat
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
odchodu	odchod	k1gInSc6	odchod
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
Smlouvě	smlouva	k1gFnSc6	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
a	a	k8xC	a
na	na	k7c6	na
Smlouvě	smlouva	k1gFnSc6	smlouva
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
a	a	k8xC	a
kterými	který	k3yIgMnPc7	který
na	na	k7c6	na
Unii	unie	k1gFnSc6	unie
přenesly	přenést	k5eAaPmAgFnP	přenést
některé	některý	k3yIgFnPc4	některý
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc2	dosažení
společných	společný	k2eAgInPc2d1	společný
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
3	[number]	k4	3
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
Unie	unie	k1gFnSc2	unie
podporovat	podporovat	k5eAaImF	podporovat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
blahobyt	blahobyt	k1gInSc4	blahobyt
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
zejména	zejména	k9	zejména
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
svým	svůj	k3xOyFgMnPc3	svůj
občanům	občan	k1gMnPc3	občan
prostor	prostor	k1gInSc4	prostor
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
práva	právo	k1gNnPc1	právo
bez	bez	k7c2	bez
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
zaručen	zaručen	k2eAgInSc1d1	zaručen
volný	volný	k2eAgInSc1d1	volný
pohyb	pohyb	k1gInSc1	pohyb
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
trh	trh	k1gInSc4	trh
a	a	k8xC	a
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
udržitelný	udržitelný	k2eAgInSc4d1	udržitelný
rozvoj	rozvoj	k1gInSc4	rozvoj
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
vyváženém	vyvážený	k2eAgInSc6d1	vyvážený
hospodářském	hospodářský	k2eAgInSc6d1	hospodářský
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
konkurenceschopném	konkurenceschopný	k2eAgNnSc6d1	konkurenceschopné
sociálně	sociálně	k6eAd1	sociálně
tržním	tržní	k2eAgNnSc6d1	tržní
hospodářství	hospodářství	k1gNnSc6	hospodářství
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
vědecký	vědecký	k2eAgInSc4d1	vědecký
a	a	k8xC	a
technický	technický	k2eAgInSc4d1	technický
pokrok	pokrok	k1gInSc4	pokrok
a	a	k8xC	a
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
sociálnímu	sociální	k2eAgNnSc3d1	sociální
vyloučení	vyloučení	k1gNnSc3	vyloučení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
cíle	cíl	k1gInPc4	cíl
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nS	patřit
podpora	podpora	k1gFnSc1	podpora
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
a	a	k8xC	a
územní	územní	k2eAgFnSc2d1	územní
soudržnosti	soudržnost	k1gFnSc2	soudržnost
a	a	k8xC	a
solidarity	solidarita	k1gFnSc2	solidarita
mezi	mezi	k7c7	mezi
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
a	a	k8xC	a
měnovou	měnový	k2eAgFnSc4d1	měnová
unii	unie	k1gFnSc4	unie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
Unie	unie	k1gFnSc2	unie
zastává	zastávat	k5eAaImIp3nS	zastávat
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
své	svůj	k3xOyFgFnPc4	svůj
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
zájmy	zájem	k1gInPc4	zájem
a	a	k8xC	a
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
vlastních	vlastní	k2eAgMnPc2d1	vlastní
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
smlouvy	smlouva	k1gFnSc2	smlouva
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
míru	mír	k1gInSc3	mír
<g/>
,	,	kIx,	,
bezpečnosti	bezpečnost	k1gFnPc4	bezpečnost
<g/>
,	,	kIx,	,
udržitelnému	udržitelný	k2eAgInSc3d1	udržitelný
rozvoji	rozvoj	k1gInSc3	rozvoj
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
volnému	volný	k2eAgNnSc3d1	volné
a	a	k8xC	a
spravedlivému	spravedlivý	k2eAgNnSc3d1	spravedlivé
obchodování	obchodování	k1gNnSc3	obchodování
<g/>
,	,	kIx,	,
vymýcení	vymýcení	k1gNnSc3	vymýcení
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
ochraně	ochrana	k1gFnSc3	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
k	k	k7c3	k
dodržování	dodržování	k1gNnSc3	dodržování
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
boj	boj	k1gInSc4	boj
o	o	k7c4	o
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
za	za	k7c4	za
usmíření	usmíření	k1gNnSc4	usmíření
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
sjednocení	sjednocení	k1gNnSc6	sjednocení
kontinentu	kontinent	k1gInSc2	kontinent
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
prevenci	prevence	k1gFnSc6	prevence
hrůz	hrůza	k1gFnPc2	hrůza
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
dohledu	dohled	k1gInSc2	dohled
nad	nad	k7c7	nad
dalším	další	k2eAgNnSc7d1	další
případným	případný	k2eAgNnSc7d1	případné
německým	německý	k2eAgNnSc7d1	německé
zbrojením	zbrojení	k1gNnSc7	zbrojení
<g/>
,	,	kIx,	,
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
šest	šest	k4xCc1	šest
západoevropských	západoevropský	k2eAgInPc2d1	západoevropský
států	stát	k1gInPc2	stát
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1951	[number]	k4	1951
Pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
Evropské	evropský	k2eAgNnSc4d1	Evropské
společenství	společenství	k1gNnSc4	společenství
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
(	(	kIx(	(
<g/>
Montánní	montánní	k2eAgFnSc2d1	montánní
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
ESUO	ESUO	kA	ESUO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
ocel	ocel	k1gFnSc4	ocel
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
hlavní	hlavní	k2eAgFnPc4d1	hlavní
strategické	strategický	k2eAgFnPc4d1	strategická
suroviny	surovina	k1gFnPc4	surovina
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
této	tento	k3xDgFnSc2	tento
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
Schumanův	Schumanův	k2eAgInSc4d1	Schumanův
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1950	[number]	k4	1950
předložil	předložit	k5eAaPmAgMnS	předložit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Robert	Robert	k1gMnSc1	Robert
Schuman	Schuman	k1gMnSc1	Schuman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1957	[number]	k4	1957
pak	pak	k6eAd1	pak
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
další	další	k2eAgInSc4d1	další
–	–	k?	–
tzv.	tzv.	kA	tzv.
Římské	římský	k2eAgFnSc2d1	římská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1958	[number]	k4	1958
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Evropské	evropský	k2eAgNnSc1d1	Evropské
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
společenství	společenství	k1gNnSc1	společenství
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Evropské	evropský	k2eAgNnSc1d1	Evropské
společenství	společenství	k1gNnSc1	společenství
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
Euratom	Euratom	k1gInSc4	Euratom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
koncepce	koncepce	k1gFnSc1	koncepce
nadstátního	nadstátní	k2eAgNnSc2d1	nadstátní
řízení	řízení	k1gNnSc2	řízení
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
činnosti	činnost	k1gFnSc2	činnost
ESUO	ESUO	kA	ESUO
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
stejný	stejný	k2eAgInSc4d1	stejný
model	model	k1gInSc4	model
zvolen	zvolit	k5eAaPmNgMnS	zvolit
i	i	k9	i
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc6d1	vzniklá
organizacích	organizace	k1gFnPc6	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
1967	[number]	k4	1967
se	se	k3xPyFc4	se
po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
tří	tři	k4xCgInPc2	tři
společenství	společenství	k1gNnPc2	společenství
(	(	kIx(	(
<g/>
ESUO	ESUO	kA	ESUO
<g/>
,	,	kIx,	,
EHS	EHS	kA	EHS
a	a	k8xC	a
Euratom	Euratom	k1gInSc1	Euratom
<g/>
)	)	kIx)	)
začalo	začít	k5eAaPmAgNnS	začít
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
Evropských	evropský	k2eAgNnPc6d1	Evropské
společenstvích	společenství	k1gNnPc6	společenství
(	(	kIx(	(
<g/>
ES	es	k1gNnPc2	es
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ropné	ropný	k2eAgFnSc2d1	ropná
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ES	ES	kA	ES
poprvé	poprvé	k6eAd1	poprvé
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Spolkové	spolkový	k2eAgFnSc3d1	spolková
republice	republika	k1gFnSc3	republika
Německo	Německo	k1gNnSc4	Německo
(	(	kIx(	(
<g/>
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
se	se	k3xPyFc4	se
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1973	[number]	k4	1973
přidalo	přidat	k5eAaPmAgNnS	přidat
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Desátým	desátý	k4xOgInSc7	desátý
členem	člen	k1gInSc7	člen
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1981	[number]	k4	1981
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1986	[number]	k4	1986
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
ovšem	ovšem	k9	ovšem
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
opustilo	opustit	k5eAaPmAgNnS	opustit
Grónsko	Grónsko	k1gNnSc4	Grónsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
odchod	odchod	k1gInSc4	odchod
odhlasovalo	odhlasovat	k5eAaPmAgNnS	odhlasovat
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Jednotný	jednotný	k2eAgInSc1d1	jednotný
evropský	evropský	k2eAgInSc1d1	evropský
akt	akt	k1gInSc1	akt
(	(	kIx(	(
<g/>
JEA	JEA	kA	JEA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
východiskem	východisko	k1gNnSc7	východisko
byla	být	k5eAaImAgFnS	být
Bílá	bílý	k2eAgFnSc1d1	bílá
kniha	kniha	k1gFnSc1	kniha
Komise	komise	k1gFnSc1	komise
specifikující	specifikující	k2eAgFnSc1d1	specifikující
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
opatření	opatření	k1gNnPc2	opatření
směřujících	směřující	k2eAgNnPc2d1	směřující
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
jednotného	jednotný	k2eAgInSc2d1	jednotný
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
znamenal	znamenat	k5eAaImAgInS	znamenat
změnu	změna	k1gFnSc4	změna
strategie	strategie	k1gFnSc2	strategie
a	a	k8xC	a
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
harmonizaci	harmonizace	k1gFnSc3	harmonizace
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
SRN	SRN	kA	SRN
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc4	Nizozemsko
a	a	k8xC	a
Lucembursko	Lucembursko	k1gNnSc4	Lucembursko
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
řešit	řešit	k5eAaImF	řešit
otázku	otázka	k1gFnSc4	otázka
fyzického	fyzický	k2eAgNnSc2d1	fyzické
uvolnění	uvolnění	k1gNnSc2	uvolnění
pohybu	pohyb	k1gInSc2	pohyb
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
Schengenská	schengenský	k2eAgFnSc1d1	Schengenská
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
postupném	postupný	k2eAgNnSc6d1	postupné
odstraňování	odstraňování	k1gNnSc6	odstraňování
kontrol	kontrola	k1gFnPc2	kontrola
na	na	k7c6	na
společných	společný	k2eAgFnPc6d1	společná
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
Schengenská	schengenský	k2eAgFnSc1d1	Schengenská
dohoda	dohoda	k1gFnSc1	dohoda
(	(	kIx(	(
<g/>
Schengenská	schengenský	k2eAgFnSc1d1	Schengenská
prováděcí	prováděcí	k2eAgFnSc1d1	prováděcí
úmluva	úmluva	k1gFnSc1	úmluva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
dohody	dohoda	k1gFnPc1	dohoda
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
další	další	k2eAgInPc1d1	další
prováděcí	prováděcí	k2eAgInPc1d1	prováděcí
předpisy	předpis	k1gInPc1	předpis
jsou	být	k5eAaImIp3nP	být
souhrnně	souhrnně	k6eAd1	souhrnně
označovány	označovat	k5eAaImNgInP	označovat
za	za	k7c4	za
Schengenské	schengenský	k2eAgFnPc4d1	Schengenská
acquis	acquis	k1gFnPc4	acquis
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Schengenský	schengenský	k2eAgInSc1d1	schengenský
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
zboží	zboží	k1gNnSc1	zboží
a	a	k8xC	a
osoby	osoba	k1gFnPc1	osoba
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Maastrichtská	maastrichtský	k2eAgFnSc1d1	Maastrichtská
smlouva	smlouva	k1gFnSc1	smlouva
oficiálně	oficiálně	k6eAd1	oficiálně
zavedla	zavést	k5eAaPmAgFnS	zavést
pojem	pojem	k1gInSc4	pojem
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
pro	pro	k7c4	pro
zastřešení	zastřešení	k1gNnSc4	zastřešení
tří	tři	k4xCgFnPc2	tři
Společenství	společenství	k1gNnPc2	společenství
bez	bez	k7c2	bez
založení	založení	k1gNnSc2	založení
její	její	k3xOp3gFnSc2	její
právní	právní	k2eAgFnSc2d1	právní
subjektivity	subjektivita	k1gFnSc2	subjektivita
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
tři	tři	k4xCgInPc4	tři
pilíře	pilíř	k1gInPc4	pilíř
EU	EU	kA	EU
–	–	k?	–
první	první	k4xOgInSc1	první
pilíř	pilíř	k1gInSc1	pilíř
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
otázky	otázka	k1gFnPc4	otázka
komunitární	komunitární	k2eAgFnPc4d1	komunitární
mající	mající	k2eAgMnPc4d1	mající
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
třem	tři	k4xCgNnPc3	tři
Společenstvím	společenství	k1gNnPc3	společenství
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
pilíř	pilíř	k1gInSc1	pilíř
představoval	představovat	k5eAaImAgInS	představovat
společnou	společný	k2eAgFnSc4d1	společná
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
třetí	třetí	k4xOgMnPc1	třetí
policejní	policejní	k2eAgMnPc1d1	policejní
a	a	k8xC	a
justiční	justiční	k2eAgFnSc4d1	justiční
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
pilíř	pilíř	k1gInSc4	pilíř
byly	být	k5eAaImAgFnP	být
nekomunitární	komunitární	k2eNgFnPc1d1	komunitární
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunitární	komunitární	k2eAgInPc1d1	komunitární
orgány	orgán	k1gInPc1	orgán
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
nemohly	moct	k5eNaImAgFnP	moct
přijímat	přijímat	k5eAaImF	přijímat
sekundární	sekundární	k2eAgNnSc4d1	sekundární
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Pilířová	pilířový	k2eAgFnSc1d1	pilířová
struktura	struktura	k1gFnSc1	struktura
EU	EU	kA	EU
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
účinností	účinnost	k1gFnSc7	účinnost
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Maastrichtská	maastrichtský	k2eAgFnSc1d1	Maastrichtská
smlouva	smlouva	k1gFnSc1	smlouva
nastavila	nastavit	k5eAaPmAgFnS	nastavit
novou	nový	k2eAgFnSc4d1	nová
proceduru	procedura	k1gFnSc4	procedura
přijímání	přijímání	k1gNnSc2	přijímání
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
spolurozhodování	spolurozhodování	k1gNnSc2	spolurozhodování
<g/>
,	,	kIx,	,
zavedla	zavést	k5eAaPmAgFnS	zavést
nové	nový	k2eAgFnPc4d1	nová
politiky	politika	k1gFnPc4	politika
a	a	k8xC	a
institut	institut	k1gInSc1	institut
občanství	občanství	k1gNnSc4	občanství
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Přinesla	přinést	k5eAaPmAgFnS	přinést
také	také	k9	také
změnu	změna	k1gFnSc4	změna
do	do	k7c2	do
fungování	fungování	k1gNnSc2	fungování
EHS	EHS	kA	EHS
<g/>
,	,	kIx,	,
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vypuštění	vypuštění	k1gNnSc3	vypuštění
slova	slovo	k1gNnSc2	slovo
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
EU	EU	kA	EU
o	o	k7c6	o
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
postupoval	postupovat	k5eAaImAgInS	postupovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1997	[number]	k4	1997
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
Amsterodamská	amsterodamský	k2eAgFnSc1d1	Amsterodamská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tzv.	tzv.	kA	tzv.
Schengenské	schengenský	k2eAgFnSc2d1	Schengenská
acquis	acquis	k1gFnSc2	acquis
začlenila	začlenit	k5eAaPmAgFnS	začlenit
do	do	k7c2	do
práva	právo	k1gNnSc2	právo
EU	EU	kA	EU
<g/>
,	,	kIx,	,
posílila	posílit	k5eAaPmAgFnS	posílit
pravomoci	pravomoc	k1gFnPc4	pravomoc
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
zavedla	zavést	k5eAaPmAgFnS	zavést
princip	princip	k1gInSc4	princip
flexibility	flexibilita	k1gFnSc2	flexibilita
a	a	k8xC	a
ze	z	k7c2	z
třetího	třetí	k4xOgInSc2	třetí
pilíře	pilíř	k1gInSc2	pilíř
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
přesunula	přesunout	k5eAaPmAgFnS	přesunout
oblast	oblast	k1gFnSc1	oblast
justiční	justiční	k2eAgFnSc2d1	justiční
spolupráce	spolupráce	k1gFnSc2	spolupráce
v	v	k7c6	v
civilních	civilní	k2eAgFnPc6d1	civilní
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k6eAd1	tak
Evropský	evropský	k2eAgInSc1d1	evropský
justiční	justiční	k2eAgInSc1d1	justiční
prostor	prostor	k1gInSc1	prostor
v	v	k7c6	v
civilních	civilní	k2eAgFnPc6d1	civilní
otázkách	otázka	k1gFnPc6	otázka
jako	jako	k8xS	jako
základ	základ	k1gInSc4	základ
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
prostoru	prostor	k1gInSc2	prostor
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
přistoupení	přistoupení	k1gNnSc2	přistoupení
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
Smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
instituce	instituce	k1gFnSc1	instituce
EU	EU	kA	EU
novému	nový	k2eAgInSc3d1	nový
počtu	počet	k1gInSc3	počet
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
snížení	snížení	k1gNnSc1	snížení
počtu	počet	k1gInSc2	počet
komisařů	komisař	k1gMnPc2	komisař
<g/>
,	,	kIx,	,
posílení	posílení	k1gNnSc1	posílení
pozic	pozice	k1gFnPc2	pozice
větších	veliký	k2eAgInPc2d2	veliký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
oblastí	oblast	k1gFnPc2	oblast
rozhodovaných	rozhodovaný	k2eAgFnPc2d1	rozhodovaná
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
většinou	většina	k1gFnSc7	většina
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
předchozí	předchozí	k2eAgFnPc4d1	předchozí
smlouvy	smlouva	k1gFnPc4	smlouva
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
Ústavě	ústava	k1gFnSc6	ústava
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
počítala	počítat	k5eAaImAgFnS	počítat
s	s	k7c7	s
nahrazením	nahrazení	k1gNnSc7	nahrazení
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
právní	právní	k2eAgFnSc4d1	právní
subjektivitu	subjektivita	k1gFnSc4	subjektivita
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
Ústavě	ústava	k1gFnSc6	ústava
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
měla	mít	k5eAaImAgFnS	mít
státoprávní	státoprávní	k2eAgInSc4d1	státoprávní
charakter	charakter	k1gInSc4	charakter
<g/>
:	:	kIx,	:
počítala	počítat	k5eAaImAgFnS	počítat
se	se	k3xPyFc4	se
zavedením	zavedení	k1gNnSc7	zavedení
termínů	termín	k1gInPc2	termín
blízkých	blízký	k2eAgMnPc2d1	blízký
vnitrostátnímu	vnitrostátní	k2eAgNnSc3d1	vnitrostátní
právu	právo	k1gNnSc3	právo
(	(	kIx(	(
<g/>
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
rámcové	rámcový	k2eAgInPc1d1	rámcový
zákony	zákon	k1gInPc1	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
funkcí	funkce	k1gFnPc2	funkce
typických	typický	k2eAgFnPc2d1	typická
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
prezident	prezident	k1gMnSc1	prezident
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
však	však	k9	však
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
referendy	referendum	k1gNnPc7	referendum
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
nová	nový	k2eAgFnSc1d1	nová
smlouva	smlouva	k1gFnSc1	smlouva
–	–	k?	–
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
odmítnutá	odmítnutý	k2eAgFnSc1d1	odmítnutá
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
Ústavě	ústava	k1gFnSc6	ústava
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
novelizační	novelizační	k2eAgFnSc7d1	novelizační
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
Jednotný	jednotný	k2eAgInSc1d1	jednotný
evropský	evropský	k2eAgInSc1d1	evropský
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
Maastrichtská	maastrichtský	k2eAgFnSc1d1	Maastrichtská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
Amsterodamská	amsterodamský	k2eAgFnSc1d1	Amsterodamská
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
Smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
<g/>
.	.	kIx.	.
</s>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
změnila	změnit	k5eAaPmAgFnS	změnit
obsah	obsah	k1gInSc4	obsah
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
a	a	k8xC	a
Smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgNnSc1d1	Evropské
společenství	společenství	k1gNnSc1	společenství
přestalo	přestat	k5eAaPmAgNnS	přestat
existovat	existovat	k5eAaImF	existovat
-	-	kIx~	-
právním	právní	k2eAgMnSc7d1	právní
nástupcem	nástupce	k1gMnSc7	nástupce
je	být	k5eAaImIp3nS	být
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bývalých	bývalý	k2eAgNnPc2d1	bývalé
tří	tři	k4xCgNnPc2	tři
společenství	společenství	k1gNnPc2	společenství
zůstal	zůstat	k5eAaPmAgInS	zůstat
již	již	k6eAd1	již
jen	jen	k9	jen
Euroatom	Euroatom	k1gInSc1	Euroatom
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ESUO	ESUO	kA	ESUO
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
padesátileté	padesátiletý	k2eAgNnSc1d1	padesátileté
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
bylo	být	k5eAaImAgNnS	být
Pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
smlouvou	smlouva	k1gFnSc7	smlouva
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
si	se	k3xPyFc3	se
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
51,89	[number]	k4	51,89
%	%	kIx~	%
britských	britský	k2eAgMnPc2d1	britský
hlasujících	hlasující	k1gMnPc2	hlasující
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
odchod	odchod	k1gInSc4	odchod
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
4,3	[number]	k4	4,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
jsou	být	k5eAaImIp3nP	být
formálně	formálně	k6eAd1	formálně
i	i	k9	i
četná	četný	k2eAgNnPc4d1	četné
zámořská	zámořský	k2eAgNnPc4d1	zámořské
území	území	k1gNnPc4	území
a	a	k8xC	a
závislá	závislý	k2eAgNnPc4d1	závislé
území	území	k1gNnPc4	území
(	(	kIx(	(
<g/>
španělské	španělský	k2eAgInPc4d1	španělský
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Ceuta	Ceuta	k1gFnSc1	Ceuta
a	a	k8xC	a
Melilla	Melilla	k1gFnSc1	Melilla
<g/>
;	;	kIx,	;
za	za	k7c4	za
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
Azory	azor	k1gInPc4	azor
a	a	k8xC	a
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
;	;	kIx,	;
britský	britský	k2eAgInSc1d1	britský
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
území	území	k1gNnSc4	území
Réunion	Réunion	k1gInSc1	Réunion
<g/>
,	,	kIx,	,
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Martinik	Martinik	k1gMnSc1	Martinik
<g/>
,	,	kIx,	,
Guadeloupe	Guadeloupe	k1gMnSc1	Guadeloupe
<g/>
,	,	kIx,	,
Mayotte	Mayott	k1gInSc5	Mayott
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Saint-Martin	Saint-Martin	k1gMnSc1	Saint-Martin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jindy	jindy	k6eAd1	jindy
nejsou	být	k5eNaImIp3nP	být
území	území	k1gNnPc4	území
přidružená	přidružený	k2eAgNnPc4d1	přidružené
k	k	k7c3	k
členským	členský	k2eAgInPc3d1	členský
států	stát	k1gInPc2	stát
součástí	součást	k1gFnPc2	součást
EU	EU	kA	EU
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
zámořských	zámořský	k2eAgNnPc2d1	zámořské
území	území	k1gNnPc2	území
přidružených	přidružený	k2eAgNnPc2d1	přidružené
ke	k	k7c3	k
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
Aruba	Aruba	k1gFnSc1	Aruba
<g/>
,	,	kIx,	,
Curaçao	curaçao	k1gNnSc1	curaçao
<g/>
,	,	kIx,	,
Sint	Sint	k2eAgMnSc1d1	Sint
Maarten	Maarten	k2eAgMnSc1d1	Maarten
<g/>
,	,	kIx,	,
Karibské	karibský	k2eAgNnSc1d1	Karibské
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
<g/>
,	,	kIx,	,
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Polynésie	Polynésie	k1gFnSc1	Polynésie
<g/>
,	,	kIx,	,
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
či	či	k8xC	či
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
<g/>
)	)	kIx)	)
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
statut	statut	k1gInSc4	statut
přidruženého	přidružený	k2eAgNnSc2d1	přidružené
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
řada	řada	k1gFnSc1	řada
území	území	k1gNnSc2	území
asociovaných	asociovaný	k2eAgMnPc2d1	asociovaný
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
území	území	k1gNnSc4	území
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
EU	EU	kA	EU
nepatří	patřit	k5eNaImIp3nS	patřit
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Oblasti	oblast	k1gFnSc2	oblast
EU	EU	kA	EU
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statusem	status	k1gInSc7	status
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
území	území	k1gNnSc6	území
Unie	unie	k1gFnSc2	unie
je	být	k5eAaImIp3nS	být
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
4808	[number]	k4	4808
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
kdyby	kdyby	kYmCp3nP	kdyby
do	do	k7c2	do
EU	EU	kA	EU
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Ararat	Ararat	k1gInSc4	Ararat
s	s	k7c7	s
5166	[number]	k4	5166
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
Vänern	Vänern	k1gInSc4	Vänern
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Švédska	Švédsko	k1gNnSc2	Švédsko
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
5650	[number]	k4	5650
km2	km2	k4	km2
a	a	k8xC	a
hloubkou	hloubka	k1gFnSc7	hloubka
106	[number]	k4	106
m.	m.	k?	m.
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
Dunaj	Dunaj	k1gInSc1	Dunaj
(	(	kIx(	(
<g/>
2850	[number]	k4	2850
km	km	kA	km
<g/>
,	,	kIx,	,
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
pohoří	pohoří	k1gNnSc6	pohoří
Schwarzwald	Schwarzwalda	k1gFnPc2	Schwarzwalda
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
u	u	k7c2	u
rumunsko-ukrajinských	rumunskokrajinský	k2eAgFnPc2d1	rumunsko-ukrajinský
hranic	hranice	k1gFnPc2	hranice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
EU	EU	kA	EU
leží	ležet	k5eAaImIp3nS	ležet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
klimatickém	klimatický	k2eAgNnSc6d1	klimatické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
oblasti	oblast	k1gFnSc6	oblast
Finska	Finsko	k1gNnSc2	Finsko
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
subarktického	subarktický	k2eAgNnSc2d1	subarktické
pásma	pásmo	k1gNnSc2	pásmo
a	a	k8xC	a
Středomoří	středomoří	k1gNnSc2	středomoří
do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
subtropického	subtropický	k2eAgNnSc2d1	subtropické
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
země	zem	k1gFnPc1	zem
EU	EU	kA	EU
mají	mít	k5eAaImIp3nP	mít
rozmanité	rozmanitý	k2eAgFnPc4d1	rozmanitá
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
17,8	[number]	k4	17,8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
12,8	[number]	k4	12,8
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
25,6	[number]	k4	25,6
°	°	k?	°
<g/>
C.	C.	kA	C.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Členský	členský	k2eAgInSc1d1	členský
stát	stát	k1gInSc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvních	první	k4xOgNnPc6	první
rozšířeních	rozšíření	k1gNnPc6	rozšíření
evropských	evropský	k2eAgNnPc6d1	Evropské
společenství	společenství	k1gNnSc2	společenství
nebyla	být	k5eNaImAgNnP	být
stanovena	stanovit	k5eAaPmNgNnP	stanovit
žádná	žádný	k3yNgNnPc1	žádný
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
by	by	kYmCp3nP	by
noví	nový	k2eAgMnPc1d1	nový
členové	člen	k1gMnPc1	člen
museli	muset	k5eAaImAgMnP	muset
splnit	splnit	k5eAaPmF	splnit
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
obecných	obecný	k2eAgFnPc2d1	obecná
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgFnPc2d1	stanovená
v	v	k7c6	v
zakládacích	zakládací	k2eAgFnPc6d1	zakládací
smlouvách	smlouva	k1gFnPc6	smlouva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
odlišovala	odlišovat	k5eAaImAgFnS	odlišovat
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
přistupujících	přistupující	k2eAgFnPc2d1	přistupující
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
proto	proto	k8xC	proto
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1993	[number]	k4	1993
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
určila	určit	k5eAaPmAgFnS	určit
obecné	obecný	k2eAgFnPc4d1	obecná
závazné	závazný	k2eAgFnPc4d1	závazná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kodaňská	kodaňský	k2eAgNnPc1d1	Kodaňské
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
politická	politický	k2eAgNnPc1d1	politické
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
kandidátská	kandidátský	k2eAgFnSc1d1	kandidátská
země	země	k1gFnSc1	země
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
stabilní	stabilní	k2eAgFnSc1d1	stabilní
instituce	instituce	k1gFnSc1	instituce
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
právní	právní	k2eAgInSc4d1	právní
stát	stát	k1gInSc4	stát
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dodržování	dodržování	k1gNnSc1	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
menšin	menšina	k1gFnPc2	menšina
ekonomická	ekonomický	k2eAgNnPc4d1	ekonomické
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
:	:	kIx,	:
země	země	k1gFnSc1	země
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
fungující	fungující	k2eAgFnSc4d1	fungující
tržní	tržní	k2eAgFnSc4d1	tržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
schopnou	schopný	k2eAgFnSc4d1	schopná
se	se	k3xPyFc4	se
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
konkurenčními	konkurenční	k2eAgInPc7d1	konkurenční
tlaky	tlak	k1gInPc7	tlak
uvnitř	uvnitř	k7c2	uvnitř
Unie	unie	k1gFnSc2	unie
kritérium	kritérium	k1gNnSc1	kritérium
přijetí	přijetí	k1gNnSc2	přijetí
acquis	acquis	k1gFnSc2	acquis
communautaire	communautair	k1gInSc5	communautair
<g/>
:	:	kIx,	:
země	zem	k1gFnPc4	zem
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schopná	schopný	k2eAgFnSc1d1	schopná
přijmout	přijmout	k5eAaPmF	přijmout
závazky	závazek	k1gInPc4	závazek
vyplývající	vyplývající	k2eAgInPc4d1	vyplývající
z	z	k7c2	z
členství	členství	k1gNnSc2	členství
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
cílů	cíl	k1gInPc2	cíl
politické	politický	k2eAgFnSc2d1	politická
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
28	[number]	k4	28
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
;	;	kIx,	;
lidnatější	lidnatý	k2eAgInPc1d2	lidnatější
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
1	[number]	k4	1
306	[number]	k4	306
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
1	[number]	k4	1
080	[number]	k4	080
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
sedmkrát	sedmkrát	k6eAd1	sedmkrát
<g/>
:	:	kIx,	:
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
o	o	k7c4	o
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
následováno	následován	k2eAgNnSc4d1	následováno
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
52	[number]	k4	52
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
setrvání	setrvání	k1gNnSc3	setrvání
v	v	k7c6	v
ES	ES	kA	ES
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
členy	člen	k1gInPc1	člen
staly	stát	k5eAaPmAgInP	stát
dosud	dosud	k6eAd1	dosud
neutrální	neutrální	k2eAgNnSc1d1	neutrální
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
10	[number]	k4	10
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gInPc1	člen
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
a	a	k8xC	a
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Brexit	Brexit	k1gInSc1	Brexit
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
EU	EU	kA	EU
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
rozšíření	rozšíření	k1gNnSc2	rozšíření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vyjednané	vyjednaný	k2eAgFnPc4d1	vyjednaná
trvalé	trvalý	k2eAgFnPc4d1	trvalá
výjimky	výjimka	k1gFnPc4	výjimka
z	z	k7c2	z
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
měnové	měnový	k2eAgFnSc6d1	měnová
unii	unie	k1gFnSc6	unie
(	(	kIx(	(
<g/>
eurozóna	eurozóna	k1gFnSc1	eurozóna
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
smlouvách	smlouva	k1gFnPc6	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
referendum	referendum	k1gNnSc1	referendum
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
voliči	volič	k1gMnPc1	volič
zvolili	zvolit	k5eAaPmAgMnP	zvolit
vystoupení	vystoupení	k1gNnSc4	vystoupení
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
z	z	k7c2	z
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Referendum	referendum	k1gNnSc1	referendum
mělo	mít	k5eAaImAgNnS	mít
doporučující	doporučující	k2eAgInSc4d1	doporučující
charakter	charakter	k1gInSc4	charakter
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
zahájení	zahájení	k1gNnSc2	zahájení
procesu	proces	k1gInSc2	proces
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
upravuje	upravovat	k5eAaImIp3nS	upravovat
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
nepoužitý	použitý	k2eNgInSc1d1	nepoužitý
článek	článek	k1gInSc1	článek
50	[number]	k4	50
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
David	David	k1gMnSc1	David
Cameron	Cameron	k1gMnSc1	Cameron
hodlal	hodlat	k5eAaImAgMnS	hodlat
aktivaci	aktivace	k1gFnSc4	aktivace
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
přenechat	přenechat	k5eAaPmF	přenechat
svému	svůj	k3xOyFgMnSc3	svůj
nástupci	nástupce	k1gMnSc3	nástupce
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Theresa	Theresa	k1gFnSc1	Theresa
Mayová	Mayová	k1gFnSc1	Mayová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
záhy	záhy	k6eAd1	záhy
zahájila	zahájit	k5eAaPmAgFnS	zahájit
přípravy	příprava	k1gFnPc4	příprava
k	k	k7c3	k
aktivaci	aktivace	k1gFnSc3	aktivace
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
zaslala	zaslat	k5eAaPmAgFnS	zaslat
předsedovi	předseda	k1gMnSc3	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
Donaldovi	Donald	k1gMnSc3	Donald
Tuskovi	Tusek	k1gMnSc3	Tusek
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
oficiálně	oficiálně	k6eAd1	oficiálně
započala	započnout	k5eAaPmAgFnS	započnout
vystoupení	vystoupení	k1gNnSc4	vystoupení
země	zem	k1gFnSc2	zem
z	z	k7c2	z
EU	EU	kA	EU
dle	dle	k7c2	dle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
50	[number]	k4	50
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
kandidátských	kandidátský	k2eAgFnPc2d1	kandidátská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
k	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Albánie	Albánie	k1gFnSc1	Albánie
(	(	kIx(	(
<g/>
přihláška	přihláška	k1gFnSc1	přihláška
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Makedonie	Makedonie	k1gFnSc1	Makedonie
(	(	kIx(	(
<g/>
přihláška	přihláška	k1gFnSc1	přihláška
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
přihláška	přihláška	k1gFnSc1	přihláška
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
přihláška	přihláška	k1gFnSc1	přihláška
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
přihláška	přihláška	k1gFnSc1	přihláška
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
členství	členství	k1gNnSc6	členství
zažádaly	zažádat	k5eAaPmAgInP	zažádat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
i	i	k9	i
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
své	svůj	k3xOyFgFnPc4	svůj
přihlášky	přihláška	k1gFnPc4	přihláška
následně	následně	k6eAd1	následně
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
či	či	k8xC	či
zamrazily	zamrazit	k5eAaPmAgInP	zamrazit
<g/>
.	.	kIx.	.
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
je	být	k5eAaImIp3nS	být
potenciálním	potenciální	k2eAgInSc7d1	potenciální
kandidátským	kandidátský	k2eAgInSc7d1	kandidátský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
ke	k	k7c3	k
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
vstupu	vstup	k1gInSc6	vstup
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
EU	EU	kA	EU
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
debaty	debata	k1gFnSc2	debata
již	již	k6eAd1	již
od	od	k7c2	od
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
asociační	asociační	k2eAgFnSc1d1	asociační
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
Evropským	evropský	k2eAgNnSc7d1	Evropské
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
společenstvím	společenství	k1gNnSc7	společenství
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Přihlášku	přihláška	k1gFnSc4	přihláška
ke	k	k7c3	k
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
EU	EU	kA	EU
podalo	podat	k5eAaPmAgNnS	podat
Turecko	Turecko	k1gNnSc1	Turecko
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kandidátský	kandidátský	k2eAgInSc4d1	kandidátský
status	status	k1gInSc4	status
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
po	po	k7c6	po
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
rozhovory	rozhovor	k1gInPc1	rozhovor
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
byly	být	k5eAaImAgInP	být
zahájeny	zahájen	k2eAgFnPc4d1	zahájena
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
35	[number]	k4	35
přístupových	přístupový	k2eAgFnPc2d1	přístupová
kapitol	kapitola	k1gFnPc2	kapitola
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
zatím	zatím	k6eAd1	zatím
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
13	[number]	k4	13
a	a	k8xC	a
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
přijetí	přijetí	k1gNnSc4	přijetí
Turecka	Turecko	k1gNnSc2	Turecko
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
stabilizaci	stabilizace	k1gFnSc4	stabilizace
jeho	jeho	k3xOp3gFnPc2	jeho
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
posílilo	posílit	k5eAaPmAgNnS	posílit
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
význam	význam	k1gInSc1	význam
ekonomiky	ekonomika	k1gFnSc2	ekonomika
EU	EU	kA	EU
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nS	by
přinesl	přinést	k5eAaPmAgInS	přinést
také	také	k9	také
výrazné	výrazný	k2eAgNnSc4d1	výrazné
navýšení	navýšení	k1gNnSc4	navýšení
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
populace	populace	k1gFnSc2	populace
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Turecko	Turecko	k1gNnSc1	Turecko
splnilo	splnit	k5eAaPmAgNnS	splnit
všechny	všechen	k3xTgFnPc4	všechen
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
možné	možný	k2eAgFnPc1d1	možná
jeho	on	k3xPp3gInSc4	on
vstup	vstup	k1gInSc4	vstup
dále	daleko	k6eAd2	daleko
oddalovat	oddalovat	k5eAaImF	oddalovat
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
také	také	k9	také
jakousi	jakýsi	k3yIgFnSc7	jakýsi
odměnou	odměna	k1gFnSc7	odměna
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
dlouholeté	dlouholetý	k2eAgNnSc4d1	dlouholeté
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
NATO	NATO	kA	NATO
a	a	k8xC	a
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ochrany	ochrana	k1gFnSc2	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
Turecka	Turecko	k1gNnSc2	Turecko
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
naopak	naopak	k6eAd1	naopak
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinou	většina	k1gFnSc7	většina
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
nestabilními	stabilní	k2eNgFnPc7d1	nestabilní
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
špatné	špatný	k2eAgInPc4d1	špatný
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
Kyprem	Kypr	k1gInSc7	Kypr
(	(	kIx(	(
<g/>
členem	člen	k1gMnSc7	člen
EU	EU	kA	EU
<g/>
;	;	kIx,	;
fakticky	fakticky	k6eAd1	fakticky
okupuje	okupovat	k5eAaBmIp3nS	okupovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
Kypru	Kypr	k1gInSc2	Kypr
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arménií	Arménie	k1gFnSc7	Arménie
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
živá	živý	k2eAgFnSc1d1	živá
genocida	genocida	k1gFnSc1	genocida
etnických	etnický	k2eAgMnPc2d1	etnický
Arménů	Armén	k1gMnPc2	Armén
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
také	také	k9	také
pochybují	pochybovat	k5eAaImIp3nP	pochybovat
o	o	k7c6	o
pokroku	pokrok	k1gInSc6	pokrok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dodržování	dodržování	k1gNnSc6	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
zejména	zejména	k9	zejména
na	na	k7c4	na
etnické	etnický	k2eAgInPc4d1	etnický
spory	spor	k1gInPc4	spor
s	s	k7c7	s
tureckými	turecký	k2eAgMnPc7d1	turecký
Kurdy	Kurd	k1gMnPc7	Kurd
a	a	k8xC	a
potlačování	potlačování	k1gNnSc3	potlačování
jejich	jejich	k3xOp3gNnPc2	jejich
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
přijetí	přijetí	k1gNnSc2	přijetí
Turecka	Turecko	k1gNnSc2	Turecko
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
obávají	obávat	k5eAaImIp3nP	obávat
změny	změna	k1gFnPc1	změna
mocenské	mocenský	k2eAgFnSc2d1	mocenská
rovnováhy	rovnováha	k1gFnSc2	rovnováha
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
–	–	k?	–
muslimské	muslimský	k2eAgFnSc6d1	muslimská
a	a	k8xC	a
národnostně	národnostně	k6eAd1	národnostně
málo	málo	k6eAd1	málo
tolerantní	tolerantní	k2eAgNnSc1d1	tolerantní
Turecko	Turecko	k1gNnSc1	Turecko
by	by	kYmCp3nS	by
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vstupu	vstup	k1gInSc2	vstup
mělo	mít	k5eAaImAgNnS	mít
srovnatelný	srovnatelný	k2eAgInSc4d1	srovnatelný
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
jako	jako	k8xS	jako
nejlidnatější	lidnatý	k2eAgFnSc2d3	nejlidnatější
země	zem	k1gFnSc2	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
v	v	k7c6	v
přístupovém	přístupový	k2eAgInSc6d1	přístupový
procesu	proces	k1gInSc6	proces
Makedonie	Makedonie	k1gFnSc2	Makedonie
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
bylo	být	k5eAaImAgNnS	být
podepsání	podepsání	k1gNnSc1	podepsání
Stabilizační	stabilizační	k2eAgFnSc2d1	stabilizační
a	a	k8xC	a
asociační	asociační	k2eAgFnSc2d1	asociační
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
EU	EU	kA	EU
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
také	také	k9	také
Makedonie	Makedonie	k1gFnSc1	Makedonie
podala	podat	k5eAaPmAgFnS	podat
oficiální	oficiální	k2eAgFnSc4d1	oficiální
přihlášku	přihláška	k1gFnSc4	přihláška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kladném	kladný	k2eAgNnSc6d1	kladné
doporučení	doporučení	k1gNnSc6	doporučení
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgMnS	být
Makedonii	Makedonie	k1gFnSc4	Makedonie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
udělen	udělen	k2eAgInSc4d1	udělen
status	status	k1gInSc4	status
kandidátské	kandidátský	k2eAgFnSc2d1	kandidátská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiální	oficiální	k2eAgNnPc1d1	oficiální
přístupová	přístupový	k2eAgNnPc1d1	přístupové
jednání	jednání	k1gNnPc1	jednání
zatím	zatím	k6eAd1	zatím
nezačala	začít	k5eNaPmAgNnP	začít
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednávání	vyjednávání	k1gNnPc1	vyjednávání
doposud	doposud	k6eAd1	doposud
probíhala	probíhat	k5eAaImAgNnP	probíhat
spíše	spíše	k9	spíše
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
především	především	k6eAd1	především
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
uvolňování	uvolňování	k1gNnSc4	uvolňování
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
vízové	vízový	k2eAgFnSc2d1	vízová
povinnosti	povinnost	k1gFnSc2	povinnost
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
pomalý	pomalý	k2eAgInSc4d1	pomalý
průběh	průběh	k1gInSc4	průběh
přístupových	přístupový	k2eAgNnPc2d1	přístupové
jednání	jednání	k1gNnPc2	jednání
měl	mít	k5eAaImAgMnS	mít
zejména	zejména	k9	zejména
bilaterální	bilaterální	k2eAgInSc4d1	bilaterální
spor	spor	k1gInSc4	spor
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
Jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
a	a	k8xC	a
Řecka	Řecko	k1gNnSc2	Řecko
o	o	k7c4	o
jméno	jméno	k1gNnSc4	jméno
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
koliduje	kolidovat	k5eAaImIp3nS	kolidovat
s	s	k7c7	s
názvem	název	k1gInSc7	název
části	část	k1gFnSc2	část
řeckého	řecký	k2eAgNnSc2d1	řecké
území	území	k1gNnSc2	území
u	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
právě	právě	k6eAd1	právě
s	s	k7c7	s
Makedonií	Makedonie	k1gFnSc7	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
EU	EU	kA	EU
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
stabilizaci	stabilizace	k1gFnSc4	stabilizace
a	a	k8xC	a
přidružení	přidružení	k1gNnSc4	přidružení
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
kandidátské	kandidátský	k2eAgFnSc2d1	kandidátská
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
Černé	Černé	k2eAgFnSc3d1	Černé
Hoře	hora	k1gFnSc3	hora
oficiálně	oficiálně	k6eAd1	oficiálně
udělen	udělit	k5eAaPmNgInS	udělit
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
Srbsko	Srbsko	k1gNnSc4	Srbsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
kandidátem	kandidát	k1gMnSc7	kandidát
oficiálně	oficiálně	k6eAd1	oficiálně
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
získala	získat	k5eAaPmAgFnS	získat
status	status	k1gInSc4	status
kandidátské	kandidátský	k2eAgFnSc2d1	kandidátská
země	zem	k1gFnSc2	zem
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
EHS	EHS	kA	EHS
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
a	a	k8xC	a
1967	[number]	k4	1967
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
přistoupení	přistoupení	k1gNnSc1	přistoupení
vetováno	vetovat	k5eAaBmNgNnS	vetovat
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
přistoupení	přistoupení	k1gNnSc6	přistoupení
dalších	další	k2eAgFnPc2d1	další
4	[number]	k4	4
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
daného	daný	k2eAgNnSc2d1	dané
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
(	(	kIx(	(
<g/>
do	do	k7c2	do
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
a	a	k8xC	a
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
do	do	k7c2	do
EU	EU	kA	EU
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
norští	norský	k2eAgMnPc1d1	norský
občané	občan	k1gMnPc1	občan
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
proti	proti	k7c3	proti
vstupu	vstup	k1gInSc3	vstup
v	v	k7c6	v
referendech	referendum	k1gNnPc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Nor	k1gMnPc1	Nor
se	se	k3xPyFc4	se
obávají	obávat	k5eAaImIp3nP	obávat
ztráty	ztráta	k1gFnPc1	ztráta
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
rybářském	rybářský	k2eAgInSc6d1	rybářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
stát	stát	k5eAaImF	stát
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
ložisky	ložisko	k1gNnPc7	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgFnPc4d1	určitá
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
muselo	muset	k5eAaImAgNnS	muset
neúměrně	úměrně	k6eNd1	úměrně
přispívat	přispívat	k5eAaImF	přispívat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
struktur	struktura	k1gFnPc2	struktura
EU	EU	kA	EU
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
angažuje	angažovat	k5eAaBmIp3nS	angažovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
projektech	projekt	k1gInPc6	projekt
a	a	k8xC	a
agenturách	agentura	k1gFnPc6	agentura
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
donor	donor	k1gInSc1	donor
pro	pro	k7c4	pro
chudší	chudý	k2eAgFnPc4d2	chudší
části	část	k1gFnPc4	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
i	i	k9	i
bez	bez	k7c2	bez
přímého	přímý	k2eAgNnSc2d1	přímé
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
EU	EU	kA	EU
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
do	do	k7c2	do
vícerých	vícerý	k4xRyIgFnPc2	vícerý
evropských	evropský	k2eAgFnPc2d1	Evropská
integračních	integrační	k2eAgFnPc2d1	integrační
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Evropského	evropský	k2eAgNnSc2d1	Evropské
sdružení	sdružení	k1gNnSc2	sdružení
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
součástí	součást	k1gFnPc2	součást
Evropského	evropský	k2eAgInSc2d1	evropský
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
společně	společně	k6eAd1	společně
s	s	k7c7	s
Islandem	Island	k1gInSc7	Island
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
k	k	k7c3	k
Schengenské	schengenský	k2eAgFnSc3d1	Schengenská
dohodě	dohoda	k1gFnSc3	dohoda
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
participujícím	participující	k2eAgMnSc7d1	participující
členem	člen	k1gMnSc7	člen
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
proslulo	proslout	k5eAaPmAgNnS	proslout
tradiční	tradiční	k2eAgFnSc7d1	tradiční
neutralitou	neutralita	k1gFnSc7	neutralita
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
kontinentální	kontinentální	k2eAgFnSc7d1	kontinentální
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
obklopená	obklopený	k2eAgFnSc1d1	obklopená
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
skutečnostem	skutečnost	k1gFnPc3	skutečnost
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
balancovat	balancovat	k5eAaImF	balancovat
mezi	mezi	k7c7	mezi
neutralitou	neutralita	k1gFnSc7	neutralita
a	a	k8xC	a
tlaky	tlak	k1gInPc7	tlak
okolního	okolní	k2eAgInSc2d1	okolní
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
spoluzaložilo	spoluzaložit	k5eAaPmAgNnS	spoluzaložit
Evropské	evropský	k2eAgNnSc1d1	Evropské
sdružení	sdružení	k1gNnSc1	sdružení
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
dokonce	dokonce	k9	dokonce
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
s	s	k7c7	s
EHS	EHS	kA	EHS
bilaterální	bilaterální	k2eAgFnSc4d1	bilaterální
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
účastnilo	účastnit	k5eAaImAgNnS	účastnit
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
ESVO	ESVO	kA	ESVO
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
motivovalo	motivovat	k5eAaBmAgNnS	motivovat
Švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
spolkovou	spolkový	k2eAgFnSc4d1	spolková
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
ke	k	k7c3	k
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
ale	ale	k8xC	ale
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
občané	občan	k1gMnPc1	občan
země	zem	k1gFnSc2	zem
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
vyjednávaní	vyjednávaný	k2eAgMnPc1d1	vyjednávaný
o	o	k7c6	o
EHP	EHP	kA	EHP
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
přistoupení	přistoupení	k1gNnSc4	přistoupení
k	k	k7c3	k
Unii	unie	k1gFnSc3	unie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
období	období	k1gNnSc1	období
charakterizovalo	charakterizovat	k5eAaBmAgNnS	charakterizovat
uzavírání	uzavírání	k1gNnSc2	uzavírání
mnoha	mnoho	k4c2	mnoho
dvojstranných	dvojstranný	k2eAgFnPc2d1	dvojstranná
smluv	smlouva	k1gFnPc2	smlouva
mezi	mezi	k7c7	mezi
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
a	a	k8xC	a
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
až	až	k9	až
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
stovek	stovka	k1gFnPc2	stovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nevýznamnější	významný	k2eNgFnPc1d2	nevýznamnější
byly	být	k5eAaImAgFnP	být
série	série	k1gFnPc1	série
prostě	prostě	k9	prostě
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Bilaterální	bilaterální	k2eAgFnSc2d1	bilaterální
dohody	dohoda	k1gFnSc2	dohoda
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
uzavřené	uzavřený	k2eAgMnPc4d1	uzavřený
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
člen	člen	k1gInSc1	člen
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Island	Island	k1gInSc1	Island
podal	podat	k5eAaPmAgInS	podat
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
oficiální	oficiální	k2eAgFnSc4d1	oficiální
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
v	v	k7c6	v
rekordním	rekordní	k2eAgInSc6d1	rekordní
čase	čas	k1gInSc6	čas
žádost	žádost	k1gFnSc1	žádost
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
vydala	vydat	k5eAaPmAgFnS	vydat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
připravenosti	připravenost	k1gFnSc6	připravenost
Islandu	Island	k1gInSc2	Island
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
EU	EU	kA	EU
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
doporučila	doporučit	k5eAaPmAgFnS	doporučit
Evropské	evropský	k2eAgFnSc2d1	Evropská
radě	rada	k1gFnSc3	rada
udělit	udělit	k5eAaPmF	udělit
Islandu	Island	k1gInSc3	Island
status	status	k1gInSc4	status
kandidátské	kandidátský	k2eAgFnSc2d1	kandidátská
země	zem	k1gFnSc2	zem
a	a	k8xC	a
zahájit	zahájit	k5eAaPmF	zahájit
tak	tak	k6eAd1	tak
oficiální	oficiální	k2eAgNnSc4d1	oficiální
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
kandidátského	kandidátský	k2eAgInSc2d1	kandidátský
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgMnS	být
Islandu	Island	k1gInSc2	Island
udělen	udělen	k2eAgMnSc1d1	udělen
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
členem	člen	k1gInSc7	člen
Evropského	evropský	k2eAgInSc2d1	evropský
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
také	také	k9	také
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
do	do	k7c2	do
svojí	svůj	k3xOyFgFnSc2	svůj
legislativy	legislativa	k1gFnSc2	legislativa
tedy	tedy	k9	tedy
již	již	k6eAd1	již
implementoval	implementovat	k5eAaImAgMnS	implementovat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
legislativy	legislativa	k1gFnSc2	legislativa
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc4	vstup
Islandu	Island	k1gInSc2	Island
provázejí	provázet	k5eAaImIp3nP	provázet
obavy	obava	k1gFnPc1	obava
ze	z	k7c2	z
sporů	spor	k1gInPc2	spor
o	o	k7c4	o
legislativu	legislativa	k1gFnSc4	legislativa
upravující	upravující	k2eAgInSc4d1	upravující
rybolov	rybolov	k1gInSc4	rybolov
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
bilaterální	bilaterální	k2eAgInPc1d1	bilaterální
spory	spor	k1gInPc1	spor
o	o	k7c4	o
zaplacení	zaplacení	k1gNnSc4	zaplacení
dluhů	dluh	k1gInPc2	dluh
insolventní	insolventní	k2eAgFnSc2d1	insolventní
islandské	islandský	k2eAgFnSc2d1	islandská
internetové	internetový	k2eAgFnSc2d1	internetová
banky	banka	k1gFnSc2	banka
Icesave	Icesav	k1gInSc5	Icesav
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
a	a	k8xC	a
Spojenému	spojený	k2eAgNnSc3d1	spojené
Království	království	k1gNnSc3	království
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gInPc7	člen
EU	EU	kA	EU
a	a	k8xC	a
uhradily	uhradit	k5eAaPmAgInP	uhradit
škody	škoda	k1gFnPc4	škoda
svým	svůj	k3xOyFgMnPc3	svůj
občanům	občan	k1gMnPc3	občan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
svoje	svůj	k3xOyFgInPc4	svůj
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
uložené	uložený	k2eAgInPc4d1	uložený
v	v	k7c6	v
Icesave	Icesav	k1gInSc5	Icesav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
motivací	motivace	k1gFnSc7	motivace
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
byl	být	k5eAaImAgInS	být
vliv	vliv	k1gInSc4	vliv
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
se	se	k3xPyFc4	se
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
následky	následek	k1gInPc7	následek
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
EU	EU	kA	EU
a	a	k8xC	a
zejména	zejména	k9	zejména
rychlým	rychlý	k2eAgNnSc7d1	rychlé
přijetím	přijetí	k1gNnSc7	přijetí
eura	euro	k1gNnSc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
po	po	k7c6	po
islandských	islandský	k2eAgFnPc6d1	islandská
volbách	volba	k1gFnPc6	volba
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
stavějící	stavějící	k2eAgFnPc4d1	stavějící
se	se	k3xPyFc4	se
negativně	negativně	k6eAd1	negativně
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
Islandu	Island	k1gInSc2	Island
do	do	k7c2	do
EU	EU	kA	EU
<g/>
)	)	kIx)	)
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
islandský	islandský	k2eAgMnSc1d1	islandský
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
že	že	k8xS	že
Island	Island	k1gInSc1	Island
jednostranně	jednostranně	k6eAd1	jednostranně
pozastavuje	pozastavovat	k5eAaImIp3nS	pozastavovat
přístupové	přístupový	k2eAgInPc4d1	přístupový
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
postup	postup	k1gInSc1	postup
bude	být	k5eAaImBp3nS	být
záležet	záležet	k5eAaImF	záležet
na	na	k7c6	na
výsledku	výsledek	k1gInSc6	výsledek
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Island	Island	k1gInSc1	Island
svou	svůj	k3xOyFgFnSc4	svůj
přihlášku	přihláška	k1gFnSc4	přihláška
k	k	k7c3	k
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
EU	EU	kA	EU
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
podle	podle	k7c2	podle
mluvčí	mluvčí	k1gFnSc2	mluvčí
evropské	evropský	k2eAgFnSc2d1	Evropská
diplomacie	diplomacie	k1gFnSc2	diplomacie
Maji	Maja	k1gFnSc2	Maja
Kocijančičové	Kocijančič	k1gMnPc1	Kocijančič
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
Islandu	Island	k1gInSc6	Island
dveře	dveře	k1gFnPc4	dveře
do	do	k7c2	do
EU	EU	kA	EU
otevřené	otevřený	k2eAgFnPc4d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
bylo	být	k5eAaImAgNnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
do	do	k7c2	do
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
začleněno	začlenit	k5eAaPmNgNnS	začlenit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Grónská	grónský	k2eAgFnSc1d1	grónská
veřejnost	veřejnost	k1gFnSc1	veřejnost
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1982	[number]	k4	1982
těsnou	těsný	k2eAgFnSc7d1	těsná
většinou	většina	k1gFnSc7	většina
(	(	kIx(	(
<g/>
53	[number]	k4	53
%	%	kIx~	%
<g/>
)	)	kIx)	)
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
pro	pro	k7c4	pro
opuštění	opuštění	k1gNnSc4	opuštění
EHS	EHS	kA	EHS
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Instituce	instituce	k1gFnSc2	instituce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
principem	princip	k1gInSc7	princip
fungování	fungování	k1gNnSc2	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
je	být	k5eAaImIp3nS	být
sdílení	sdílení	k1gNnSc4	sdílení
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
kompetenci	kompetence	k1gFnSc6	kompetence
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
společné	společný	k2eAgFnPc1d1	společná
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
institucionálního	institucionální	k2eAgInSc2d1	institucionální
rámce	rámec	k1gInSc2	rámec
tvoří	tvořit	k5eAaImIp3nP	tvořit
sedm	sedm	k4xCc4	sedm
orgánů	orgán	k1gInPc2	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
vyjmenovány	vyjmenovat	k5eAaPmNgInP	vyjmenovat
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
13	[number]	k4	13
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Evropský	evropský	k2eAgInSc4d1	evropský
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
Evropskou	evropský	k2eAgFnSc4d1	Evropská
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
Radu	rada	k1gFnSc4	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Evropskou	evropský	k2eAgFnSc4d1	Evropská
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
Soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Evropskou	evropský	k2eAgFnSc4d1	Evropská
centrální	centrální	k2eAgFnSc4d1	centrální
banku	banka	k1gFnSc4	banka
a	a	k8xC	a
o	o	k7c4	o
Evropský	evropský	k2eAgInSc4d1	evropský
účetní	účetní	k2eAgInSc4d1	účetní
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
poradní	poradní	k2eAgInSc4d1	poradní
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
Berlaymont	Berlaymont	k1gMnSc1	Berlaymont
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některá	některý	k3yIgNnPc1	některý
oddělení	oddělení	k1gNnPc1	oddělení
jsou	být	k5eAaImIp3nP	být
umístěna	umístit	k5eAaPmNgNnP	umístit
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
sídlí	sídlet	k5eAaImIp3nS	sídlet
rovněž	rovněž	k9	rovněž
Rada	rada	k1gFnSc1	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
Justus	Justus	k1gMnSc1	Justus
Lipsius	Lipsius	k1gMnSc1	Lipsius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
června	červen	k1gInSc2	červen
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
schůze	schůze	k1gFnSc1	schůze
rady	rada	k1gFnSc2	rada
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Evropský	evropský	k2eAgInSc1d1	evropský
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
Výbor	výbor	k1gInSc1	výbor
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
Louise	Louis	k1gMnSc2	Louis
Weiss	Weiss	k1gMnSc1	Weiss
<g/>
)	)	kIx)	)
a	a	k8xC	a
hostuje	hostovat	k5eAaImIp3nS	hostovat
dvanáct	dvanáct	k4xCc1	dvanáct
měsíčních	měsíční	k2eAgNnPc2d1	měsíční
plenárních	plenární	k2eAgNnPc2d1	plenární
zasedání	zasedání	k1gNnPc2	zasedání
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
rozpočtového	rozpočtový	k2eAgNnSc2d1	rozpočtové
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
účetní	účetní	k2eAgInSc1d1	účetní
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
investiční	investiční	k2eAgFnSc1d1	investiční
banka	banka	k1gFnSc1	banka
mají	mít	k5eAaImIp3nP	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
také	také	k9	také
hostí	hostit	k5eAaImIp3nS	hostit
Sekretariát	sekretariát	k1gInSc1	sekretariát
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
rámec	rámec	k1gInSc1	rámec
činnosti	činnost	k1gFnSc2	činnost
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
14	[number]	k4	14
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vymezen	vymezit	k5eAaPmNgInS	vymezit
jako	jako	k9	jako
subjekt	subjekt	k1gInSc1	subjekt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
–	–	k?	–
společně	společně	k6eAd1	společně
s	s	k7c7	s
Radou	rada	k1gFnSc7	rada
–	–	k?	–
legislativní	legislativní	k2eAgFnSc4d1	legislativní
a	a	k8xC	a
rozpočtovou	rozpočtový	k2eAgFnSc4d1	rozpočtová
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
občanů	občan	k1gMnPc2	občan
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
posledním	poslední	k2eAgNnSc6d1	poslední
rozšíření	rozšíření	k1gNnSc6	rozšíření
EU	EU	kA	EU
o	o	k7c4	o
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
čítá	čítat	k5eAaImIp3nS	čítat
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
751	[number]	k4	751
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
751	[number]	k4	751
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
EP	EP	kA	EP
jsou	být	k5eAaImIp3nP	být
voleni	volen	k2eAgMnPc1d1	volen
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
<g/>
,	,	kIx,	,
přímých	přímý	k2eAgFnPc6d1	přímá
<g/>
,	,	kIx,	,
tajných	tajný	k2eAgFnPc6d1	tajná
a	a	k8xC	a
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Institucionální	institucionální	k2eAgNnSc1d1	institucionální
vymezení	vymezení	k1gNnSc1	vymezení
EP	EP	kA	EP
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
223	[number]	k4	223
až	až	k9	až
234	[number]	k4	234
Smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
SFEU	SFEU	kA	SFEU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
EP	EP	kA	EP
je	být	k5eAaImIp3nS	být
Štrasburk	Štrasburk	k1gInSc4	Štrasburk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
parlament	parlament	k1gInSc1	parlament
zasedá	zasedat	k5eAaImIp3nS	zasedat
také	také	k9	také
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
a	a	k8xC	a
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
)	)	kIx)	)
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
zájmy	zájem	k1gInPc4	zájem
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
pravomocí	pravomoc	k1gFnSc7	pravomoc
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
Evropským	evropský	k2eAgInSc7d1	evropský
parlamentem	parlament	k1gInSc7	parlament
přijímání	přijímání	k1gNnSc2	přijímání
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
jedná	jednat	k5eAaImIp3nS	jednat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
složeních	složení	k1gNnPc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
úrovní	úroveň	k1gFnSc7	úroveň
je	být	k5eAaImIp3nS	být
Rada	rada	k1gFnSc1	rada
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
ministrů	ministr	k1gMnPc2	ministr
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nP	scházet
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
úrovní	úroveň	k1gFnSc7	úroveň
je	být	k5eAaImIp3nS	být
Výbor	výbor	k1gInSc1	výbor
stálých	stálý	k2eAgMnPc2d1	stálý
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nP	scházet
zástupci	zástupce	k1gMnPc1	zástupce
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
jejich	jejich	k3xOp3gMnPc1	jejich
zástupci	zástupce	k1gMnPc1	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
úrovněmi	úroveň	k1gFnPc7	úroveň
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
výbory	výbor	k1gInPc1	výbor
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc1d1	pracovní
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
buď	buď	k8xC	buď
jednomyslně	jednomyslně	k6eAd1	jednomyslně
<g/>
,	,	kIx,	,
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
nebo	nebo	k8xC	nebo
prostou	prostý	k2eAgFnSc7d1	prostá
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
rozhodování	rozhodování	k1gNnSc2	rozhodování
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kvalifikované	kvalifikovaný	k2eAgFnSc2d1	kvalifikovaná
většiny	většina	k1gFnSc2	většina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hlasy	hlas	k1gInPc1	hlas
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
váhu	váha	k1gFnSc4	váha
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
půlrok	půlrok	k1gInSc1	půlrok
předsedá	předsedat	k5eAaImIp3nS	předsedat
Radě	rada	k1gFnSc3	rada
jiná	jiný	k2eAgFnSc1d1	jiná
země	země	k1gFnSc1	země
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hlav	hlava	k1gFnPc2	hlava
států	stát	k1gInPc2	stát
a	a	k8xC	a
předsedů	předseda	k1gMnPc2	předseda
vlád	vláda	k1gFnPc2	vláda
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
předsedy	předseda	k1gMnSc2	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
nejzávažnějších	závažný	k2eAgFnPc6d3	nejzávažnější
politických	politický	k2eAgFnPc6d1	politická
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
otázkách	otázka	k1gFnPc6	otázka
a	a	k8xC	a
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
Unie	unie	k1gFnSc1	unie
ubírat	ubírat	k5eAaImF	ubírat
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jednomyslnosti	jednomyslnost	k1gFnSc2	jednomyslnost
<g/>
.	.	kIx.	.
</s>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
zřídila	zřídit	k5eAaPmAgFnS	zřídit
stálého	stálý	k2eAgMnSc4d1	stálý
předsedu	předseda	k1gMnSc4	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
2,5	[number]	k4	2,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
Herman	Herman	k1gMnSc1	Herman
Van	van	k1gInSc4	van
Rompuy	Rompua	k1gFnSc2	Rompua
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Donald	Donald	k1gMnSc1	Donald
Tusk	Tusk	k1gMnSc1	Tusk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
sleduje	sledovat	k5eAaImIp3nS	sledovat
zájmy	zájem	k1gInPc4	zájem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
nesmějí	smát	k5eNaImIp3nP	smát
přihlížet	přihlížet	k5eAaImF	přihlížet
k	k	k7c3	k
zájmům	zájem	k1gInPc3	zájem
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
má	mít	k5eAaImIp3nS	mít
výlučné	výlučný	k2eAgNnSc4d1	výlučné
právo	právo	k1gNnSc4	právo
iniciovat	iniciovat	k5eAaBmF	iniciovat
návrhy	návrh	k1gInPc4	návrh
legislativy	legislativa	k1gFnSc2	legislativa
a	a	k8xC	a
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
zakládacích	zakládací	k2eAgFnPc2d1	zakládací
smluv	smlouva	k1gFnPc2	smlouva
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
návrh	návrh	k1gInSc4	návrh
rozpočtu	rozpočet	k1gInSc2	rozpočet
EU	EU	kA	EU
a	a	k8xC	a
provádí	provádět	k5eAaImIp3nS	provádět
kontrolu	kontrola	k1gFnSc4	kontrola
jeho	jeho	k3xOp3gNnSc2	jeho
plnění	plnění	k1gNnSc2	plnění
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Komise	komise	k1gFnSc1	komise
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
EU	EU	kA	EU
při	při	k7c6	při
mezinárodních	mezinárodní	k2eAgNnPc6d1	mezinárodní
jednáních	jednání	k1gNnPc6	jednání
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
sjednávat	sjednávat	k5eAaImF	sjednávat
s	s	k7c7	s
třetími	třetí	k4xOgInPc7	třetí
státy	stát	k1gInPc7	stát
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Smlouvy	smlouva	k1gFnPc1	smlouva
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
země	země	k1gFnSc1	země
jednoho	jeden	k4xCgMnSc2	jeden
komisaře	komisař	k1gMnSc2	komisař
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gNnPc2	on
dvacet	dvacet	k4xCc1	dvacet
osm	osm	k4xCc1	osm
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
komise	komise	k1gFnSc2	komise
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
Lucemburčan	Lucemburčan	k1gMnSc1	Lucemburčan
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Juncker	Junckero	k1gNnPc2	Junckero
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prosté	prostý	k2eAgFnSc2d1	prostá
většiny	většina	k1gFnSc2	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
dbá	dbát	k5eAaImIp3nS	dbát
nad	nad	k7c7	nad
jednotným	jednotný	k2eAgInSc7d1	jednotný
výkladem	výklad	k1gInSc7	výklad
evropského	evropský	k2eAgNnSc2d1	Evropské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
národními	národní	k2eAgFnPc7d1	národní
centrálními	centrální	k2eAgFnPc7d1	centrální
bankami	banka	k1gFnPc7	banka
tvoří	tvořit	k5eAaImIp3nP	tvořit
Evropský	evropský	k2eAgInSc1d1	evropský
systém	systém	k1gInSc1	systém
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
měnovou	měnový	k2eAgFnSc7d1	měnová
bankou	banka	k1gFnSc7	banka
pro	pro	k7c4	pro
Eurozónu	Eurozóna	k1gFnSc4	Eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
účetní	účetní	k2eAgInSc1d1	účetní
dvůr	dvůr	k1gInSc1	dvůr
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
hospodaření	hospodaření	k1gNnSc4	hospodaření
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
Výbor	výbor	k1gInSc1	výbor
regionů	region	k1gInPc2	region
jsou	být	k5eAaImIp3nP	být
poradní	poradní	k2eAgInPc1d1	poradní
a	a	k8xC	a
konzultativní	konzultativní	k2eAgInPc1d1	konzultativní
orgány	orgán	k1gInPc1	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nápomocny	nápomocen	k2eAgInPc1d1	nápomocen
Evropskému	evropský	k2eAgInSc3d1	evropský
parlamentu	parlament	k1gInSc3	parlament
<g/>
,	,	kIx,	,
Radě	rada	k1gFnSc3	rada
a	a	k8xC	a
Komisi	komise	k1gFnSc3	komise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
předepsaných	předepsaný	k2eAgFnPc6d1	předepsaná
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
konzultace	konzultace	k1gFnSc1	konzultace
povinná	povinný	k2eAgFnSc1d1	povinná
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
investiční	investiční	k2eAgFnSc1d1	investiční
banka	banka	k1gFnSc1	banka
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
veřejným	veřejný	k2eAgInPc3d1	veřejný
i	i	k8xC	i
soukromým	soukromý	k2eAgInPc3d1	soukromý
subjektům	subjekt	k1gInPc3	subjekt
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
půjčky	půjčka	k1gFnSc2	půjčka
na	na	k7c4	na
kapitálové	kapitálový	k2eAgFnPc4d1	kapitálová
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
,	,	kIx,	,
pobočky	pobočka	k1gFnPc4	pobočka
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
,	,	kIx,	,
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
investiční	investiční	k2eAgInSc1d1	investiční
fond	fond	k1gInSc1	fond
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
s	s	k7c7	s
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
transevropských	transevropský	k2eAgFnPc2d1	transevropská
infrastruktur	infrastruktura	k1gFnPc2	infrastruktura
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
záruky	záruka	k1gFnPc4	záruka
na	na	k7c4	na
půjčky	půjčka	k1gFnSc2	půjčka
malým	malý	k2eAgInPc3d1	malý
a	a	k8xC	a
středním	střední	k2eAgInPc3d1	střední
podnikům	podnik	k1gInPc3	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgMnSc1d1	evropský
ombudsman	ombudsman	k1gMnSc1	ombudsman
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
stížnostmi	stížnost	k1gFnPc7	stížnost
na	na	k7c4	na
činnosti	činnost	k1gFnPc4	činnost
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Maastrichtské	maastrichtský	k2eAgFnSc2d1	Maastrichtská
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
oblasti	oblast	k1gFnSc3	oblast
činnosti	činnost	k1gFnSc2	činnost
EU	EU	kA	EU
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
politiky	politika	k1gFnSc2	politika
EU	EU	kA	EU
<g/>
,	,	kIx,	,
dělily	dělit	k5eAaImAgInP	dělit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
tří	tři	k4xCgFnPc2	tři
pilířů	pilíř	k1gInPc2	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
toto	tento	k3xDgNnSc4	tento
dělení	dělení	k1gNnSc4	dělení
formálně	formálně	k6eAd1	formálně
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
částečně	částečně	k6eAd1	částečně
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
v	v	k7c6	v
přechodných	přechodný	k2eAgNnPc6d1	přechodné
obdobích	období	k1gNnPc6	období
a	a	k8xC	a
specifických	specifický	k2eAgFnPc6d1	specifická
procedurách	procedura	k1gFnPc6	procedura
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
pilíř	pilíř	k1gInSc4	pilíř
<g/>
,	,	kIx,	,
nadstátní	nadstátní	k2eAgInSc4d1	nadstátní
<g/>
,	,	kIx,	,
zastřešoval	zastřešovat	k5eAaImAgMnS	zastřešovat
všechna	všechen	k3xTgNnPc4	všechen
tři	tři	k4xCgNnPc4	tři
dosavadní	dosavadní	k2eAgNnPc4d1	dosavadní
Společenství	společenství	k1gNnPc4	společenství
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
pilíř	pilíř	k1gInSc4	pilíř
zaváděly	zavádět	k5eAaImAgFnP	zavádět
nové	nový	k2eAgFnPc1d1	nová
oblasti	oblast	k1gFnPc1	oblast
mezivládní	mezivládní	k2eAgFnSc2d1	mezivládní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
pilíř	pilíř	k1gInSc1	pilíř
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
společnou	společný	k2eAgFnSc4d1	společná
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
pilíř	pilíř	k1gInSc4	pilíř
pak	pak	k6eAd1	pak
policejní	policejní	k2eAgFnSc3d1	policejní
a	a	k8xC	a
justiční	justiční	k2eAgFnSc3d1	justiční
spolupráci	spolupráce	k1gFnSc3	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Lisabonskou	lisabonský	k2eAgFnSc7d1	Lisabonská
smlouvou	smlouva	k1gFnSc7	smlouva
se	se	k3xPyFc4	se
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
pilíř	pilíř	k1gInSc4	pilíř
přeměnily	přeměnit	k5eAaPmAgFnP	přeměnit
v	v	k7c4	v
další	další	k2eAgFnPc4d1	další
společné	společný	k2eAgFnPc4d1	společná
politiky	politika	k1gFnPc4	politika
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Určitá	určitý	k2eAgFnSc1d1	určitá
výjimka	výjimka	k1gFnSc1	výjimka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
společné	společný	k2eAgFnSc2d1	společná
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zachována	zachován	k2eAgFnSc1d1	zachována
jednomyslnost	jednomyslnost	k1gFnSc1	jednomyslnost
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
světovou	světový	k2eAgFnSc7d1	světová
ekonomikou	ekonomika	k1gFnSc7	ekonomika
s	s	k7c7	s
HDP	HDP	kA	HDP
11,8	[number]	k4	11,8
biliónu	bilión	k4xCgInSc2	bilión
euro	euro	k1gNnSc4	euro
(	(	kIx(	(
<g/>
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
EU	EU	kA	EU
má	mít	k5eAaImIp3nS	mít
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
aktivní	aktivní	k2eAgNnSc1d1	aktivní
saldo	saldo	k1gNnSc1	saldo
běžného	běžný	k2eAgInSc2d1	běžný
účtu	účet	k1gInSc2	účet
platební	platební	k2eAgFnSc2d1	platební
bilance	bilance	k1gFnSc2	bilance
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc4d1	nízká
inflaci	inflace	k1gFnSc4	inflace
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalé	bývalý	k2eAgFnSc2d1	bývalá
"	"	kIx"	"
<g/>
patnáctky	patnáctka	k1gFnSc2	patnáctka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
finanční	finanční	k2eAgFnSc1d1	finanční
krize	krize	k1gFnSc1	krize
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
vážně	vážně	k6eAd1	vážně
postihla	postihnout	k5eAaPmAgFnS	postihnout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
starými	starý	k2eAgFnPc7d1	stará
a	a	k8xC	a
novými	nový	k2eAgFnPc7d1	nová
zeměmi	zem	k1gFnPc7	zem
existují	existovat	k5eAaImIp3nP	existovat
významné	významný	k2eAgInPc4d1	významný
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
vyspělosti	vyspělost	k1gFnSc6	vyspělost
i	i	k8xC	i
ostatních	ostatní	k2eAgInPc6d1	ostatní
ekonomických	ekonomický	k2eAgInPc6d1	ekonomický
ukazatelích	ukazatel	k1gInPc6	ukazatel
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
aktivit	aktivita	k1gFnPc2	aktivita
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
koordinována	koordinován	k2eAgFnSc1d1	koordinována
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sociální	sociální	k2eAgFnSc1d1	sociální
politika	politika	k1gFnSc1	politika
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
pravomoci	pravomoc	k1gFnSc6	pravomoc
institucí	instituce	k1gFnPc2	instituce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obchodní	obchodní	k2eAgFnSc1d1	obchodní
politika	politika	k1gFnSc1	politika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
jsou	být	k5eAaImIp3nP	být
zapojeny	zapojit	k5eAaPmNgFnP	zapojit
do	do	k7c2	do
jednotného	jednotný	k2eAgInSc2d1	jednotný
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
trhu	trh	k1gInSc2	trh
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
zajištěn	zajistit	k5eAaPmNgInS	zajistit
volný	volný	k2eAgInSc1d1	volný
pohyb	pohyb	k1gInSc1	pohyb
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
kapitálu	kapitál	k1gInSc2	kapitál
neexistují	existovat	k5eNaImIp3nP	existovat
technické	technický	k2eAgFnPc4d1	technická
překážky	překážka	k1gFnPc4	překážka
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
rozdíly	rozdíl	k1gInPc1	rozdíl
technických	technický	k2eAgInPc2d1	technický
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
stupně	stupeň	k1gInPc1	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
snížení	snížení	k1gNnSc4	snížení
administrativy	administrativa	k1gFnSc2	administrativa
v	v	k7c6	v
daňové	daňový	k2eAgFnSc6d1	daňová
oblasti	oblast	k1gFnSc6	oblast
Devatenáct	devatenáct	k4xCc4	devatenáct
<g />
.	.	kIx.	.
</s>
<s>
zemí	zem	k1gFnSc7	zem
EU	EU	kA	EU
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
členy	člen	k1gMnPc4	člen
eurozóny	eurozón	k1gMnPc4	eurozón
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
území	území	k1gNnSc6	území
platí	platit	k5eAaImIp3nS	platit
společná	společný	k2eAgFnSc1d1	společná
měna	měna	k1gFnSc1	měna
euro	euro	k1gNnSc4	euro
<g/>
;	;	kIx,	;
další	další	k2eAgFnSc2d1	další
země	zem	k1gFnSc2	zem
ho	on	k3xPp3gMnSc4	on
plánují	plánovat	k5eAaImIp3nP	plánovat
zavést	zavést	k5eAaPmF	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
eura	euro	k1gNnSc2	euro
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
konvergenční	konvergenční	k2eAgInPc1d1	konvergenční
nebo	nebo	k8xC	nebo
Maastrichtská	maastrichtský	k2eAgNnPc1d1	maastrichtské
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
průměrná	průměrný	k2eAgFnSc1d1	průměrná
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc2	inflace
země	zem	k1gFnSc2	zem
v	v	k7c6	v
období	období	k1gNnSc6	období
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
před	před	k7c7	před
prověřením	prověření	k1gNnSc7	prověření
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
závěrečného	závěrečný	k2eAgNnSc2d1	závěrečné
stadia	stadion	k1gNnSc2	stadion
nesmí	smět	k5eNaImIp3nS	smět
převýšit	převýšit	k5eAaPmF	převýšit
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,5	[number]	k4	1,5
procentního	procentní	k2eAgInSc2d1	procentní
bodu	bod	k1gInSc2	bod
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
míru	míra	k1gFnSc4	míra
inflace	inflace	k1gFnSc2	inflace
tří	tři	k4xCgFnPc2	tři
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
mírou	míra	k1gFnSc7	míra
inflace	inflace	k1gFnSc2	inflace
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
nominální	nominální	k2eAgFnSc1d1	nominální
úroková	úrokový	k2eAgFnSc1d1	úroková
míra	míra	k1gFnSc1	míra
nesmí	smět	k5eNaImIp3nS	smět
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
před	před	k7c7	před
prověřením	prověření	k1gNnSc7	prověření
země	zem	k1gFnSc2	zem
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
závěrečného	závěrečný	k2eAgNnSc2d1	závěrečné
stadia	stadion	k1gNnSc2	stadion
převýšit	převýšit	k5eAaPmF	převýšit
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
procentní	procentní	k2eAgInPc1d1	procentní
body	bod	k1gInPc1	bod
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
úrokovou	úrokový	k2eAgFnSc4d1	úroková
míru	míra	k1gFnSc4	míra
tří	tři	k4xCgFnPc2	tři
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
mírou	míra	k1gFnSc7	míra
inflace	inflace	k1gFnSc2	inflace
deficit	deficit	k1gInSc1	deficit
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
nesmí	smět	k5eNaImIp3nS	smět
převýšit	převýšit	k5eAaPmF	převýšit
3	[number]	k4	3
%	%	kIx~	%
HDP	HDP	kA	HDP
a	a	k8xC	a
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
nesmí	smět	k5eNaImIp3nS	smět
převýšit	převýšit	k5eAaPmF	převýšit
60	[number]	k4	60
%	%	kIx~	%
HDP	HDP	kA	HDP
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
EU	EU	kA	EU
prověřovat	prověřovat	k5eAaImF	prověřovat
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
EMU	emu	k1gMnPc2	emu
(	(	kIx(	(
<g/>
kritérium	kritérium	k1gNnSc1	kritérium
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
splněné	splněný	k2eAgNnSc4d1	splněné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
dluhu	dluh	k1gInSc2	dluh
na	na	k7c6	na
HDP	HDP	kA	HDP
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
měnový	měnový	k2eAgInSc1d1	měnový
kurz	kurz	k1gInSc1	kurz
členské	členský	k2eAgFnSc2d1	členská
země	zem	k1gFnSc2	zem
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
rozpětí	rozpětí	k1gNnSc1	rozpětí
dané	daný	k2eAgNnSc1d1	dané
Evropským	evropský	k2eAgInSc7d1	evropský
měnovým	měnový	k2eAgInSc7d1	měnový
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
ERM	ERM	kA	ERM
II	II	kA	II
<g/>
)	)	kIx)	)
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
před	před	k7c7	před
prověřením	prověření	k1gNnSc7	prověření
možnosti	možnost	k1gFnSc2	možnost
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
závěrečného	závěrečný	k2eAgNnSc2d1	závěrečné
stadia	stadion	k1gNnSc2	stadion
nesmí	smět	k5eNaImIp3nS	smět
členská	členský	k2eAgFnSc1d1	členská
země	země	k1gFnSc1	země
devalvovat	devalvovat	k5eAaBmF	devalvovat
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
oproti	oproti	k7c3	oproti
měnám	měna	k1gFnPc3	měna
ostatních	ostatní	k2eAgFnPc2d1	ostatní
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
financování	financování	k1gNnSc3	financování
politik	politika	k1gFnPc2	politika
EU	EU	kA	EU
<g/>
,	,	kIx,	,
administrativních	administrativní	k2eAgInPc2d1	administrativní
výdajů	výdaj	k1gInPc2	výdaj
evropských	evropský	k2eAgFnPc2d1	Evropská
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
národních	národní	k2eAgInPc2d1	národní
rozpočtů	rozpočet	k1gInPc2	rozpočet
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
,	,	kIx,	,
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozpočtové	rozpočtový	k2eAgInPc1d1	rozpočtový
deficity	deficit	k1gInPc1	deficit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozpočtového	rozpočtový	k2eAgInSc2d1	rozpočtový
rámce	rámec	k1gInSc2	rámec
na	na	k7c6	na
období	období	k1gNnSc6	období
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
bude	být	k5eAaImBp3nS	být
činit	činit	k5eAaImF	činit
1,05	[number]	k4	1,05
%	%	kIx~	%
HDP	HDP	kA	HDP
Unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
podíl	podíl	k1gInSc1	podíl
výdajů	výdaj	k1gInPc2	výdaj
národních	národní	k2eAgInPc2d1	národní
rozpočtů	rozpočet	k1gInPc2	rozpočet
na	na	k7c6	na
HDP	HDP	kA	HDP
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
rozpočtu	rozpočet	k1gInSc2	rozpočet
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
a	a	k8xC	a
předkládá	předkládat	k5eAaImIp3nS	předkládat
jej	on	k3xPp3gMnSc4	on
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
Radě	rada	k1gFnSc6	rada
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgInS	oprávnit
návrh	návrh	k1gInSc1	návrh
pozměnit	pozměnit	k5eAaPmF	pozměnit
<g/>
.	.	kIx.	.
</s>
<s>
Integrace	integrace	k1gFnSc1	integrace
do	do	k7c2	do
západoevropských	západoevropský	k2eAgFnPc2d1	západoevropská
struktur	struktura	k1gFnPc2	struktura
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
cílů	cíl	k1gInPc2	cíl
Československa	Československo	k1gNnSc2	Československo
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
významnou	významný	k2eAgFnSc7d1	významná
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
otevření	otevření	k1gNnSc1	otevření
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
odstranění	odstranění	k1gNnSc4	odstranění
řady	řada	k1gFnSc2	řada
dovozních	dovozní	k2eAgNnPc2d1	dovozní
omezení	omezení	k1gNnPc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Československem	Československo	k1gNnSc7	Československo
ale	ale	k8xC	ale
stál	stát	k5eAaImAgMnS	stát
obtížný	obtížný	k2eAgInSc4d1	obtížný
úkol	úkol	k1gInSc4	úkol
transformace	transformace	k1gFnSc2	transformace
centrálně	centrálně	k6eAd1	centrálně
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c4	na
tržní	tržní	k2eAgNnSc4d1	tržní
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
fond	fond	k1gInSc1	fond
PHARE	PHARE	kA	PHARE
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
pomáhat	pomáhat	k5eAaImF	pomáhat
Polsku	Polska	k1gFnSc4	Polska
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
tržní	tržní	k2eAgFnSc4d1	tržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
finančních	finanční	k2eAgInPc2d1	finanční
grantů	grant	k1gInPc2	grant
a	a	k8xC	a
poradenství	poradenství	k1gNnSc2	poradenství
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
působnost	působnost	k1gFnSc1	působnost
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
státy	stát	k1gInPc4	stát
bývalého	bývalý	k2eAgInSc2d1	bývalý
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
informací	informace	k1gFnPc2	informace
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
ČR	ČR	kA	ČR
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
celkově	celkově	k6eAd1	celkově
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
EU	EU	kA	EU
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
301,2	[number]	k4	301,2
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
získala	získat	k5eAaPmAgFnS	získat
551,2	[number]	k4	551,2
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
tak	tak	k9	tak
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
čisté	čistý	k2eAgMnPc4d1	čistý
příjemce	příjemce	k1gMnPc4	příjemce
s	s	k7c7	s
kladným	kladný	k2eAgNnSc7d1	kladné
saldem	saldo	k1gNnSc7	saldo
okolo	okolo	k7c2	okolo
250	[number]	k4	250
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
agentury	agentura	k1gFnSc2	agentura
STEM	sto	k4xCgNnSc7	sto
klesá	klesat	k5eAaImIp3nS	klesat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
podíl	podíl	k1gInSc4	podíl
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pozitivně	pozitivně	k6eAd1	pozitivně
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
za	za	k7c4	za
efektivně	efektivně	k6eAd1	efektivně
fungující	fungující	k2eAgFnSc4d1	fungující
organizaci	organizace	k1gFnSc4	organizace
ji	on	k3xPp3gFnSc4	on
pokládá	pokládat	k5eAaImIp3nS	pokládat
pouze	pouze	k6eAd1	pouze
27	[number]	k4	27
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
EU	EU	kA	EU
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
počet	počet	k1gInSc1	počet
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
postkomunistickými	postkomunistický	k2eAgInPc7d1	postkomunistický
státy	stát	k1gInPc7	stát
uzavírány	uzavírán	k2eAgFnPc4d1	uzavírána
asociační	asociační	k2eAgFnPc4d1	asociační
dohody	dohoda	k1gFnPc4	dohoda
(	(	kIx(	(
<g/>
též	též	k9	též
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
přidružení	přidružení	k1gNnSc6	přidružení
nebo	nebo	k8xC	nebo
Eurodohody	Eurodohod	k1gInPc4	Eurodohod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sice	sice	k8xC	sice
nedávaly	dávat	k5eNaImAgFnP	dávat
záruku	záruka	k1gFnSc4	záruka
přijetí	přijetí	k1gNnSc2	přijetí
do	do	k7c2	do
EU	EU	kA	EU
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
lepší	dobrý	k2eAgInSc4d2	lepší
politický	politický	k2eAgInSc4d1	politický
dialog	dialog	k1gInSc4	dialog
<g/>
,	,	kIx,	,
institucionální	institucionální	k2eAgFnSc4d1	institucionální
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc4	odstraňování
obchodních	obchodní	k2eAgFnPc2d1	obchodní
bariér	bariéra	k1gFnPc2	bariéra
(	(	kIx(	(
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
se	se	k3xPyFc4	se
zavázala	zavázat	k5eAaPmAgFnS	zavázat
snižovat	snižovat	k5eAaImF	snižovat
svoje	svůj	k3xOyFgNnPc4	svůj
cla	clo	k1gNnPc4	clo
a	a	k8xC	a
kvóty	kvóta	k1gFnPc4	kvóta
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
východní	východní	k2eAgInPc4d1	východní
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zmírnila	zmírnit	k5eAaPmAgFnS	zmírnit
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
ekonomiky	ekonomika	k1gFnPc4	ekonomika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Československa	Československo	k1gNnSc2	Československo
musela	muset	k5eAaImAgFnS	muset
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
podepsat	podepsat	k5eAaPmF	podepsat
novou	nový	k2eAgFnSc4d1	nová
asociační	asociační	k2eAgFnSc4d1	asociační
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
;	;	kIx,	;
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1995	[number]	k4	1995
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
premiér	premiér	k1gMnSc1	premiér
například	například	k6eAd1	například
mohl	moct	k5eAaImAgMnS	moct
účastnit	účastnit	k5eAaImF	účastnit
zasedání	zasedání	k1gNnSc4	zasedání
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
resortní	resortní	k2eAgMnPc1d1	resortní
ministři	ministr	k1gMnPc1	ministr
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
2	[number]	k4	2
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
účastnit	účastnit	k5eAaImF	účastnit
zasedání	zasedání	k1gNnSc4	zasedání
Rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
představitelé	představitel	k1gMnPc1	představitel
EU	EU	kA	EU
zdráhali	zdráhat	k5eAaImAgMnP	zdráhat
dát	dát	k5eAaPmF	dát
závazný	závazný	k2eAgInSc4d1	závazný
termín	termín	k1gInSc4	termín
přistoupení	přistoupení	k1gNnSc2	přistoupení
postkomunistických	postkomunistický	k2eAgFnPc2d1	postkomunistická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
schválena	schválit	k5eAaPmNgFnS	schválit
tzv.	tzv.	kA	tzv.
Kodaňská	kodaňský	k2eAgNnPc4d1	Kodaňské
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
,	,	kIx,	,
určující	určující	k2eAgFnPc4d1	určující
podmínky	podmínka	k1gFnPc4	podmínka
vstupu	vstup	k1gInSc2	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vstup	vstup	k1gInSc1	vstup
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
podala	podat	k5eAaPmAgFnS	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
EU	EU	kA	EU
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
<g/>
;	;	kIx,	;
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
EU	EU	kA	EU
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
priorit	priorita	k1gFnPc2	priorita
české	český	k2eAgFnSc2d1	Česká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
s	s	k7c7	s
významnými	významný	k2eAgInPc7d1	významný
dopady	dopad	k1gInPc7	dopad
také	také	k9	také
na	na	k7c4	na
domácí	domácí	k2eAgFnSc4d1	domácí
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
situaci	situace	k1gFnSc4	situace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nutnost	nutnost	k1gFnSc4	nutnost
postupné	postupný	k2eAgFnSc2d1	postupná
implementace	implementace	k1gFnSc2	implementace
acquis	acquis	k1gFnSc2	acquis
communautaire	communautair	k1gInSc5	communautair
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1997	[number]	k4	1997
vydala	vydat	k5eAaPmAgFnS	vydat
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
Agendu	agenda	k1gFnSc4	agenda
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základním	základní	k2eAgInSc7d1	základní
metodologickým	metodologický	k2eAgInSc7d1	metodologický
dokumentem	dokument	k1gInSc7	dokument
pro	pro	k7c4	pro
přijímání	přijímání	k1gNnSc4	přijímání
nových	nový	k2eAgFnPc2d1	nová
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
posudky	posudek	k1gInPc4	posudek
nově	nova	k1gFnSc3	nova
přistupujících	přistupující	k2eAgFnPc2d1	přistupující
zemí	zem	k1gFnPc2	zem
ohledně	ohledně	k7c2	ohledně
plnění	plnění	k1gNnSc2	plnění
nutných	nutný	k2eAgFnPc2d1	nutná
podmínek	podmínka	k1gFnPc2	podmínka
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
EK	EK	kA	EK
doporučila	doporučit	k5eAaPmAgFnS	doporučit
zahájit	zahájit	k5eAaPmF	zahájit
přístupová	přístupový	k2eAgNnPc4d1	přístupové
jednání	jednání	k1gNnPc4	jednání
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
Estonskem	Estonsko	k1gNnSc7	Estonsko
<g/>
,	,	kIx,	,
Kyprem	Kypr	k1gInSc7	Kypr
<g/>
,	,	kIx,	,
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
vydávala	vydávat	k5eAaImAgFnS	vydávat
EK	EK	kA	EK
každoroční	každoroční	k2eAgInPc1d1	každoroční
posudky	posudek	k1gInPc1	posudek
hodnotící	hodnotící	k2eAgInPc1d1	hodnotící
pokroky	pokrok	k1gInPc1	pokrok
kandidátských	kandidátský	k2eAgFnPc2d1	kandidátská
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
<g/>
,	,	kIx,	,
právní	právní	k2eAgFnSc6d1	právní
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnSc3d1	politická
i	i	k8xC	i
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
helsinském	helsinský	k2eAgInSc6d1	helsinský
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byly	být	k5eAaImAgInP	být
zahájeny	zahájen	k2eAgInPc1d1	zahájen
přístupové	přístupový	k2eAgInPc1d1	přístupový
rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
kandidátskými	kandidátský	k2eAgFnPc7d1	kandidátská
zeměmi	zem	k1gFnPc7	zem
<g/>
:	:	kIx,	:
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
,	,	kIx,	,
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
,	,	kIx,	,
Maltou	Malta	k1gFnSc7	Malta
<g/>
,	,	kIx,	,
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Helsinská	helsinský	k2eAgFnSc1d1	Helsinská
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
v	v	k7c4	v
niž	jenž	k3xRgFnSc4	jenž
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zprůhlednění	zprůhlednění	k1gNnSc3	zprůhlednění
a	a	k8xC	a
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
se	se	k3xPyFc4	se
celé	celý	k2eAgFnPc1d1	celá
acquis	acquis	k1gFnPc1	acquis
communautaire	communautair	k1gInSc5	communautair
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
do	do	k7c2	do
31	[number]	k4	31
kapitol	kapitola	k1gFnPc2	kapitola
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
screening	screening	k1gInSc1	screening
–	–	k?	–
tabulka	tabulka	k1gFnSc1	tabulka
vpravo	vpravo	k6eAd1	vpravo
udává	udávat	k5eAaImIp3nS	udávat
data	datum	k1gNnPc4	datum
uzavření	uzavření	k1gNnSc2	uzavření
kapitol	kapitola	k1gFnPc2	kapitola
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
probíhalo	probíhat	k5eAaImAgNnS	probíhat
formou	forma	k1gFnSc7	forma
mezivládních	mezivládní	k2eAgFnPc2d1	mezivládní
konferencí	konference	k1gFnPc2	konference
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
(	(	kIx(	(
<g/>
jednání	jednání	k1gNnSc4	jednání
vždy	vždy	k6eAd1	vždy
řídila	řídit	k5eAaImAgFnS	řídit
předsednická	předsednický	k2eAgFnSc1d1	předsednická
země	země	k1gFnSc1	země
EU	EU	kA	EU
<g/>
)	)	kIx)	)
s	s	k7c7	s
kandidátskými	kandidátský	k2eAgFnPc7d1	kandidátská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
nových	nový	k2eAgFnPc2d1	nová
zemí	zem	k1gFnPc2	zem
byla	být	k5eAaImAgFnS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
ratifikace	ratifikace	k1gFnSc1	ratifikace
Smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
připravila	připravit	k5eAaPmAgFnS	připravit
evropské	evropský	k2eAgFnPc4d1	Evropská
instituce	instituce	k1gFnPc4	instituce
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
s	s	k7c7	s
deseti	deset	k4xCc7	deset
kandidátskými	kandidátský	k2eAgInPc7d1	kandidátský
státy	stát	k1gInPc7	stát
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
jednání	jednání	k1gNnPc1	jednání
<g/>
;	;	kIx,	;
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
přistoupení	přistoupení	k1gNnSc6	přistoupení
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2003	[number]	k4	2003
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
;	;	kIx,	;
datem	datum	k1gNnSc7	datum
vstupu	vstup	k1gInSc2	vstup
byl	být	k5eAaImAgMnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvy	smlouva	k1gFnPc1	smlouva
o	o	k7c6	o
přistoupení	přistoupení	k1gNnSc6	přistoupení
ratifikovaly	ratifikovat	k5eAaBmAgInP	ratifikovat
národní	národní	k2eAgInPc1d1	národní
parlamenty	parlament	k1gInPc1	parlament
stávajících	stávající	k2eAgFnPc2d1	stávající
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
a	a	k8xC	a
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
přistupující	přistupující	k2eAgInPc1d1	přistupující
státy	stát	k1gInPc1	stát
ratifikovaly	ratifikovat	k5eAaBmAgInP	ratifikovat
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
EU	EU	kA	EU
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
zvolily	zvolit	k5eAaPmAgFnP	zvolit
referendum	referendum	k1gNnSc4	referendum
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
77,3	[number]	k4	77,3
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
volební	volební	k2eAgFnSc1d1	volební
účast	účast	k1gFnSc1	účast
byla	být	k5eAaImAgFnS	být
55,2	[number]	k4	55,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
plnoprávným	plnoprávný	k2eAgInSc7d1	plnoprávný
členem	člen	k1gInSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
s	s	k7c7	s
přechodnými	přechodný	k2eAgNnPc7d1	přechodné
obdobími	období	k1gNnPc7	období
vyjednanými	vyjednaný	k2eAgInPc7d1	vyjednaný
v	v	k7c6	v
přístupové	přístupový	k2eAgFnSc6d1	přístupová
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
:	:	kIx,	:
omezení	omezení	k1gNnSc1	omezení
volného	volný	k2eAgInSc2d1	volný
pohybu	pohyb	k1gInSc2	pohyb
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
netýká	týkat	k5eNaImIp3nS	týkat
se	se	k3xPyFc4	se
např.	např.	kA	např.
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
)	)	kIx)	)
postupné	postupný	k2eAgNnSc1d1	postupné
zavádění	zavádění	k1gNnSc1	zavádění
přímých	přímý	k2eAgFnPc2d1	přímá
plateb	platba	k1gFnPc2	platba
EU	EU	kA	EU
zemědělcům	zemědělec	k1gMnPc3	zemědělec
<g/>
:	:	kIx,	:
25	[number]	k4	25
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
v	v	k7c6	v
r.	r.	kA	r.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
30	[number]	k4	30
%	%	kIx~	%
v	v	k7c6	v
r.	r.	kA	r.
2005	[number]	k4	2005
a	a	k8xC	a
35	[number]	k4	35
%	%	kIx~	%
v	v	k7c6	v
r.	r.	kA	r.
2006	[number]	k4	2006
omezení	omezení	k1gNnPc2	omezení
nabývání	nabývání	k1gNnSc2	nabývání
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
lesů	les	k1gInPc2	les
cizozemci	cizozemec	k1gMnPc1	cizozemec
–	–	k?	–
občany	občan	k1gMnPc4	občan
EU	EU	kA	EU
(	(	kIx(	(
<g/>
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
přechodné	přechodný	k2eAgNnSc4d1	přechodné
období	období	k1gNnSc4	období
pro	pro	k7c4	pro
nabývání	nabývání	k1gNnSc4	nabývání
nemovitostí	nemovitost	k1gFnPc2	nemovitost
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
vedlejšímu	vedlejší	k2eAgNnSc3d1	vedlejší
bydlení	bydlení	k1gNnSc3	bydlení
pro	pro	k7c4	pro
cizozemce	cizozemec	k1gMnPc4	cizozemec
–	–	k?	–
občany	občan	k1gMnPc4	občan
EU	EU	kA	EU
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
let	let	k1gInSc4	let
<g/>
)	)	kIx)	)
přechodné	přechodný	k2eAgNnSc4d1	přechodné
období	období	k1gNnSc4	období
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
hygienické	hygienický	k2eAgFnPc4d1	hygienická
normy	norma	k1gFnPc4	norma
v	v	k7c6	v
zemědělských	zemědělský	k2eAgInPc6d1	zemědělský
podnicích	podnik	k1gInPc6	podnik
přechodné	přechodný	k2eAgNnSc4d1	přechodné
období	období	k1gNnSc4	období
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
parametrů	parametr	k1gInPc2	parametr
drůbežích	drůbeží	k2eAgInPc2d1	drůbeží
klecí	klec	k1gFnSc7	klec
přechodné	přechodný	k2eAgNnSc4d1	přechodné
období	období	k1gNnSc4	období
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
pro	pro	k7c4	pro
sníženou	snížený	k2eAgFnSc4d1	snížená
spotřební	spotřební	k2eAgFnSc4d1	spotřební
daň	daň	k1gFnSc4	daň
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
cigaret	cigareta	k1gFnPc2	cigareta
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
přechodná	přechodný	k2eAgNnPc1d1	přechodné
období	období	k1gNnPc1	období
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
sazbě	sazba	k1gFnSc6	sazba
DPH	DPH	kA	DPH
trvalá	trvalý	k2eAgFnSc1d1	trvalá
výjimka	výjimka	k1gFnSc1	výjimka
pro	pro	k7c4	pro
sníženou	snížený	k2eAgFnSc4d1	snížená
sazbu	sazba	k1gFnSc4	sazba
DPH	DPH	kA	DPH
z	z	k7c2	z
produktů	produkt	k1gInPc2	produkt
pěstitelského	pěstitelský	k2eAgNnSc2d1	pěstitelské
pálení	pálení	k1gNnSc2	pálení
množství	množství	k1gNnSc2	množství
přechodných	přechodný	k2eAgNnPc2d1	přechodné
období	období	k1gNnPc2	období
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
obalových	obalový	k2eAgInPc2d1	obalový
odpadů	odpad	k1gInPc2	odpad
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc3	čištění
městských	městský	k2eAgFnPc2d1	městská
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
emisí	emise	k1gFnPc2	emise
znečišťujících	znečišťující	k2eAgFnPc2d1	znečišťující
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
přijala	přijmout	k5eAaPmAgFnS	přijmout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
priority	priorita	k1gFnPc4	priorita
rozvoje	rozvoj	k1gInSc2	rozvoj
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
specifikované	specifikovaný	k2eAgFnSc2d1	specifikovaná
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
strategickém	strategický	k2eAgInSc6d1	strategický
referenčním	referenční	k2eAgInSc6d1	referenční
rámci	rámec	k1gInSc6	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
určuje	určovat	k5eAaImIp3nS	určovat
strategii	strategie	k1gFnSc4	strategie
čerpání	čerpání	k1gNnSc2	čerpání
finanční	finanční	k2eAgFnSc2d1	finanční
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Lisabonskou	lisabonský	k2eAgFnSc7d1	Lisabonská
strategií	strategie	k1gFnSc7	strategie
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
26,69	[number]	k4	26,69
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
v	v	k7c6	v
období	období	k1gNnSc6	období
2007	[number]	k4	2007
až	až	k9	až
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Demografie	demografie	k1gFnSc1	demografie
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
představuje	představovat	k5eAaImIp3nS	představovat
vysoce	vysoce	k6eAd1	vysoce
zalidněnou	zalidněný	k2eAgFnSc4d1	zalidněná
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
unii	unie	k1gFnSc4	unie
28	[number]	k4	28
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2009	[number]	k4	2009
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
celkem	celkem	k6eAd1	celkem
499,7	[number]	k4	499,7
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
očekává	očekávat	k5eAaImIp3nS	očekávat
pokles	pokles	k1gInSc1	pokles
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celku	celek	k1gInSc2	celek
vyrovnáno	vyrovnat	k5eAaPmNgNnS	vyrovnat
vstupem	vstup	k1gInSc7	vstup
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
do	do	k7c2	do
EU	EU	kA	EU
v	v	k7c6	v
horizontu	horizont	k1gInSc6	horizont
příštích	příští	k2eAgNnPc2d1	příští
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc1	Německo
s	s	k7c7	s
odhadovaným	odhadovaný	k2eAgInSc7d1	odhadovaný
počtem	počet	k1gInSc7	počet
82,1	[number]	k4	82,1
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nejméně	málo	k6eAd3	málo
lidnatým	lidnatý	k2eAgInSc7d1	lidnatý
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
je	být	k5eAaImIp3nS	být
Malta	Malta	k1gFnSc1	Malta
se	s	k7c7	s
400	[number]	k4	400
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Porodnost	porodnost	k1gFnSc1	porodnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
EU	EU	kA	EU
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
;	;	kIx,	;
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ženu	žena	k1gFnSc4	žena
1,5	[number]	k4	1,5
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
je	být	k5eAaImIp3nS	být
porodnost	porodnost	k1gFnSc1	porodnost
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
16,9	[number]	k4	16,9
‰	‰	k?	‰
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
porodnost	porodnost	k1gFnSc1	porodnost
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
EU	EU	kA	EU
Německo	Německo	k1gNnSc1	Německo
s	s	k7c7	s
8,2	[number]	k4	8,2
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
EU	EU	kA	EU
má	mít	k5eAaImIp3nS	mít
významnou	významný	k2eAgFnSc4d1	významná
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zrcadlí	zrcadlit	k5eAaImIp3nS	zrcadlit
v	v	k7c6	v
rozmanitosti	rozmanitost	k1gFnSc6	rozmanitost
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Nominální	nominální	k2eAgFnSc1d1	nominální
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
římský	římský	k2eAgInSc1d1	římský
katolicismus	katolicismus	k1gInSc1	katolicismus
<g/>
,	,	kIx,	,
protestantství	protestantství	k1gNnSc1	protestantství
a	a	k8xC	a
pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
ne	ne	k9	ne
všechny	všechen	k3xTgInPc4	všechen
státy	stát	k1gInPc4	stát
EU	EU	kA	EU
mají	mít	k5eAaImIp3nP	mít
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
většiny	většina	k1gFnPc1	většina
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
Estonsku	Estonsko	k1gNnSc6	Estonsko
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
většinu	většina	k1gFnSc4	většina
ateisté	ateista	k1gMnPc1	ateista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedávný	dávný	k2eNgInSc1d1	nedávný
příliv	příliv	k1gInSc1	příliv
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
do	do	k7c2	do
bohatých	bohatý	k2eAgInPc2d1	bohatý
států	stát	k1gInPc2	stát
EU	EU	kA	EU
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesl	přinést	k5eAaPmAgMnS	přinést
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgNnPc2d1	různé
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
dotyční	dotyčný	k2eAgMnPc1d1	dotyčný
vyznávali	vyznávat	k5eAaImAgMnP	vyznávat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rodných	rodný	k2eAgFnPc6d1	rodná
vlastech	vlast	k1gFnPc6	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
sikhismus	sikhismus	k1gInSc1	sikhismus
a	a	k8xC	a
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í.	í.	k?	í.
Judaismus	judaismus	k1gInSc1	judaismus
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
žili	žít	k5eAaImAgMnP	žít
pokojně	pokojně	k6eAd1	pokojně
s	s	k7c7	s
ostatním	ostatní	k2eAgNnSc7d1	ostatní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
století	století	k1gNnSc4	století
<g/>
,	,	kIx,	,
navzdory	navzdory	k6eAd1	navzdory
stoletím	století	k1gNnSc7	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
diskriminováni	diskriminován	k2eAgMnPc1d1	diskriminován
<g/>
,	,	kIx,	,
perzekvováni	perzekvován	k2eAgMnPc1d1	perzekvován
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
byla	být	k5eAaImAgFnS	být
spáchána	spáchán	k2eAgFnSc1d1	spáchána
genocida	genocida	k1gFnSc1	genocida
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
tradici	tradice	k1gFnSc4	tradice
sekularismus	sekularismus	k1gInSc4	sekularismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
ateismu	ateismus	k1gInSc2	ateismus
a	a	k8xC	a
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
úředních	úřední	k2eAgMnPc2d1	úřední
jazyků	jazyk	k1gMnPc2	jazyk
28	[number]	k4	28
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc1	status
oficiálního	oficiální	k2eAgMnSc2d1	oficiální
jazyku	jazyk	k1gInSc3	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
EU	EU	kA	EU
celkem	celkem	k6eAd1	celkem
24	[number]	k4	24
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
nejnovějšími	nový	k2eAgInPc7d3	nejnovější
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
irština	irština	k1gFnSc1	irština
<g/>
,	,	kIx,	,
bulharština	bulharština	k1gFnSc1	bulharština
<g/>
,	,	kIx,	,
rumunština	rumunština	k1gFnSc1	rumunština
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
poslední	poslední	k2eAgFnSc1d1	poslední
chorvatština	chorvatština	k1gFnSc1	chorvatština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
používaným	používaný	k2eAgInSc7d1	používaný
jazykem	jazyk	k1gInSc7	jazyk
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
na	na	k7c4	na
51	[number]	k4	51
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
vysoký	vysoký	k2eAgInSc4d1	vysoký
podíl	podíl	k1gInSc4	podíl
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgMnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
38	[number]	k4	38
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
EU	EU	kA	EU
používá	používat	k5eAaImIp3nS	používat
angličtinu	angličtina	k1gFnSc4	angličtina
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
buď	buď	k8xC	buď
druhý	druhý	k4xOgInSc4	druhý
jazyk	jazyk	k1gInSc4	jazyk
nebo	nebo	k8xC	nebo
cizí	cizí	k2eAgInSc4d1	cizí
jazyk	jazyk	k1gInSc4	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
používaným	používaný	k2eAgInSc7d1	používaný
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mluví	mluvit	k5eAaImIp3nS	mluvit
18	[number]	k4	18
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
Evropské	evropský	k2eAgNnSc1d1	Evropské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
kultury	kultura	k1gFnSc2	kultura
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
propůjčován	propůjčován	k2eAgInSc4d1	propůjčován
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unií	unie	k1gFnSc7	unie
jednomu	jeden	k4xCgNnSc3	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
evropským	evropský	k2eAgNnPc3d1	Evropské
městům	město	k1gNnPc3	město
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
možnost	možnost	k1gFnSc4	možnost
představit	představit	k5eAaPmF	představit
Evropě	Evropa	k1gFnSc6	Evropa
svůj	svůj	k3xOyFgInSc4	svůj
kulturní	kulturní	k2eAgInSc4d1	kulturní
život	život	k1gInSc4	život
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
města	město	k1gNnPc4	město
volena	volen	k2eAgNnPc4d1	voleno
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kulturní	kulturní	k2eAgFnSc2d1	kulturní
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
naplánovaných	naplánovaný	k2eAgFnPc6d1	naplánovaná
akcích	akce	k1gFnPc6	akce
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc6	schopnost
projekt	projekt	k1gInSc1	projekt
podpořit	podpořit	k5eAaPmF	podpořit
finančními	finanční	k2eAgInPc7d1	finanční
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
infrastrukturou	infrastruktura	k1gFnSc7	infrastruktura
<g/>
.	.	kIx.	.
</s>
