<p>
<s>
Growling	Growling	k1gInSc1	Growling
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
specifická	specifický	k2eAgFnSc1d1	specifická
technika	technika	k1gFnSc1	technika
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
užívaná	užívaný	k2eAgFnSc1d1	užívaná
především	především	k9	především
v	v	k7c4	v
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
podobných	podobný	k2eAgInPc6d1	podobný
žánrech	žánr	k1gInPc6	žánr
(	(	kIx(	(
<g/>
grindcore	grindcor	k1gMnSc5	grindcor
<g/>
,	,	kIx,	,
black	black	k6eAd1	black
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
viking	viking	k1gMnSc1	viking
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
gothic	gothic	k1gMnSc1	gothic
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
screamingu	screaming	k1gInSc2	screaming
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
tónů	tón	k1gInPc2	tón
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hluboká	hluboký	k2eAgFnSc1d1	hluboká
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
hlasu	hlas	k1gInSc2	hlas
je	být	k5eAaImIp3nS	být
hrubá	hrubý	k2eAgFnSc1d1	hrubá
a	a	k8xC	a
chraplavá	chraplavý	k2eAgFnSc1d1	chraplavá
<g/>
.	.	kIx.	.
</s>
<s>
Kupodivu	kupodivu	k6eAd1	kupodivu
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
technice	technika	k1gFnSc6	technika
není	být	k5eNaImIp3nS	být
intonace	intonace	k1gFnSc1	intonace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
artikulace	artikulace	k1gFnSc1	artikulace
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
rozumět	rozumět	k5eAaImF	rozumět
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
posluchače	posluchač	k1gMnSc4	posluchač
nezvyklého	zvyklý	k2eNgMnSc2d1	nezvyklý
na	na	k7c4	na
death	death	k1gInSc4	death
metal	metat	k5eAaImAgMnS	metat
problém	problém	k1gInSc4	problém
i	i	k9	i
u	u	k7c2	u
profesionálních	profesionální	k2eAgFnPc2d1	profesionální
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Growling	Growling	k1gInSc1	Growling
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
někteří	některý	k3yIgMnPc1	některý
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
když	když	k8xS	když
chroptí	chroptit	k5eAaImIp3nS	chroptit
nebo	nebo	k8xC	nebo
bručí	bručet	k5eAaImIp3nS	bručet
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
hlasivek	hlasivka	k1gFnPc2	hlasivka
do	do	k7c2	do
saxofonu	saxofon	k1gInSc2	saxofon
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
modulace	modulace	k1gFnSc1	modulace
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
drsnost	drsnost	k1gFnSc1	drsnost
a	a	k8xC	a
chraplavost	chraplavost	k1gFnSc1	chraplavost
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc4	blues
<g/>
,	,	kIx,	,
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
populárních	populární	k2eAgInPc6d1	populární
žánrech	žánr	k1gInPc6	žánr
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
i	i	k9	i
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpěváci	Zpěvák	k1gMnPc1	Zpěvák
používající	používající	k2eAgFnSc4d1	používající
tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
používají	používat	k5eAaImIp3nP	používat
všechny	všechen	k3xTgInPc4	všechen
death	death	k1gInSc4	death
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
jen	jen	k9	jen
skupiny	skupina	k1gFnPc1	skupina
jiných	jiný	k2eAgInPc2d1	jiný
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masha	Masha	k1gFnSc1	Masha
Scream	Scream	k1gInSc1	Scream
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Arkona	Arkon	k1gMnSc2	Arkon
</s>
</p>
<p>
<s>
Sylvain	Sylvain	k1gMnSc1	Sylvain
Houde	Houd	k1gInSc5	Houd
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Kataklysm	Kataklysm	k1gInSc1	Kataklysm
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lord	lord	k1gMnSc1	lord
Worm	Worm	k1gMnSc1	Worm
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Cryptopsy	Cryptopsa	k1gFnSc2	Cryptopsa
</s>
</p>
<p>
<s>
Lori	lori	k1gMnSc1	lori
Bravo	bravo	k1gMnSc1	bravo
–	–	k?	–
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
z	z	k7c2	z
Nuclear	Nucleara	k1gFnPc2	Nucleara
Death	Deatha	k1gFnPc2	Deatha
</s>
</p>
<p>
<s>
Angela	Angela	k1gFnSc1	Angela
Gossow	Gossow	k1gFnSc2	Gossow
–	–	k?	–
manažerka	manažerka	k1gFnSc1	manažerka
Arch	arch	k1gInSc1	arch
Enemy	Enema	k1gFnSc2	Enema
</s>
</p>
<p>
<s>
Burton	Burton	k1gInSc1	Burton
C.	C.	kA	C.
Bell	bell	k1gInSc1	bell
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Fear	Feara	k1gFnPc2	Feara
Factory	Factor	k1gMnPc4	Factor
</s>
</p>
<p>
<s>
Petri	Petri	k6eAd1	Petri
Lindroos	Lindroos	k1gMnSc1	Lindroos
–	–	k?	–
zpěvák	zpěvák	k1gMnSc1	zpěvák
z	z	k7c2	z
Ensiferum	Ensiferum	k1gInSc1	Ensiferum
a	a	k8xC	a
Norther	Northra	k1gFnPc2	Northra
</s>
</p>
<p>
<s>
Chris	Chris	k1gInSc1	Chris
Barnes	Barnes	k1gInSc1	Barnes
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
Cannibal	Cannibal	k1gInSc1	Cannibal
Corpse	corps	k1gInSc5	corps
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
–	–	k?	–
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Six	Six	k1gMnSc1	Six
Feet	Feet	k1gMnSc1	Feet
Under	Under	k1gMnSc1	Under
(	(	kIx(	(
<g/>
od	od	k7c2	od
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Fisher	Fishra	k1gFnPc2	Fishra
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Cannibal	Cannibal	k1gInSc1	Cannibal
Corpse	corps	k1gInSc5	corps
(	(	kIx(	(
<g/>
od	od	k7c2	od
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Jansen	Jansna	k1gFnPc2	Jansna
–	–	k?	–
ze	z	k7c2	z
symphonic-power	symphonicowra	k1gFnPc2	symphonic-powra
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Epica	Epica	k1gMnSc1	Epica
</s>
</p>
<p>
<s>
Chrigel	Chrigel	k1gMnSc1	Chrigel
Glanzmann	Glanzmann	k1gMnSc1	Glanzmann
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Eluveitie	Eluveitie	k1gFnSc2	Eluveitie
</s>
</p>
<p>
<s>
Johan	Johan	k1gMnSc1	Johan
Hegg	Hegg	k1gMnSc1	Hegg
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Amon	Amon	k1gMnSc1	Amon
Amarth	Amarth	k1gMnSc1	Amarth
</s>
</p>
<p>
<s>
Pekka	Pekka	k6eAd1	Pekka
Kokko	Kokko	k1gNnSc1	Kokko
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Kalmah	Kalmaha	k1gFnPc2	Kalmaha
</s>
</p>
<p>
<s>
Alissa	Alissa	k1gFnSc1	Alissa
White-Gluz	White-Gluza	k1gFnPc2	White-Gluza
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Arch	archa	k1gFnPc2	archa
Enemy	Enema	k1gFnSc2	Enema
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Tägtgren	Tägtgrna	k1gFnPc2	Tägtgrna
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Hypocrisy	Hypocris	k1gInPc1	Hypocris
</s>
</p>
<p>
<s>
Mikael	Mikaelit	k5eAaPmRp2nS	Mikaelit
Stanne	Stann	k1gMnSc5	Stann
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Dark	Darka	k1gFnPc2	Darka
Tranquillity	Tranquillita	k1gFnSc2	Tranquillita
</s>
</p>
<p>
<s>
Henke	Henke	k1gFnSc1	Henke
Forss	Forssa	k1gFnPc2	Forssa
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
In	In	k1gMnSc1	In
Flames	Flames	k1gMnSc1	Flames
</s>
</p>
<p>
<s>
Raven	Raven	k2eAgMnSc1d1	Raven
Filth	Filth	k1gMnSc1	Filth
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Bestial	Bestial	k1gInSc1	Bestial
Therapy	Therapa	k1gFnSc2	Therapa
</s>
</p>
<p>
<s>
Alexi	Alexe	k1gFnSc4	Alexe
Laiho	Lai	k1gMnSc2	Lai
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
</s>
</p>
<p>
<s>
Dani	daň	k1gFnSc3	daň
Filth	Filth	k1gMnSc1	Filth
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Cradle	Cradle	k1gFnSc2	Cradle
of	of	k?	of
Filth	Filth	k1gMnSc1	Filth
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Fras	Frasa	k1gFnPc2	Frasa
–	–	k?	–
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Laibach	Laibacha	k1gFnPc2	Laibacha
</s>
</p>
<p>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
Scott	Scott	k1gMnSc1	Scott
Sykes	Sykes	k1gMnSc1	Sykes
-	-	kIx~	-
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Bring	Bring	k1gMnSc1	Bring
Me	Me	k1gMnSc1	Me
The	The	k1gMnSc1	The
Horizon	Horizon	k1gMnSc1	Horizon
</s>
</p>
<p>
<s>
Mitch	Mitch	k1gMnSc1	Mitch
Lucker	Lucker	k1gMnSc1	Lucker
-	-	kIx~	-
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Suicide	Suicid	k1gMnSc5	Suicid
Silence	silenka	k1gFnSc3	silenka
</s>
</p>
<p>
<s>
Ronnie	Ronnie	k1gFnSc1	Ronnie
Radke	Radk	k1gFnSc2	Radk
-	-	kIx~	-
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Falling	Falling	k1gInSc1	Falling
in	in	k?	in
Reverse	reverse	k1gFnSc2	reverse
</s>
</p>
<p>
<s>
Caleb	Calba	k1gFnPc2	Calba
Shomo	Shoma	k1gFnSc5	Shoma
-	-	kIx~	-
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Beartooth	Beartootha	k1gFnPc2	Beartootha
</s>
</p>
<p>
<s>
Tatiana	Tatiana	k1gFnSc1	Tatiana
Shmailyuk	Shmailyuka	k1gFnPc2	Shmailyuka
-	-	kIx~	-
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Jinjer	Jinjra	k1gFnPc2	Jinjra
</s>
</p>
<p>
<s>
Danny	Danna	k1gFnPc1	Danna
Worsnop	Worsnop	k1gInSc1	Worsnop
-	-	kIx~	-
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Asking	Asking	k1gInSc1	Asking
Alexandria	Alexandrium	k1gNnPc4	Alexandrium
</s>
</p>
