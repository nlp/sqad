<s>
Growling	Growling	k1gInSc1	Growling
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
specifická	specifický	k2eAgFnSc1d1	specifická
technika	technika	k1gFnSc1	technika
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
užívaná	užívaný	k2eAgFnSc1d1	užívaná
především	především	k9	především
v	v	k7c4	v
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
podobných	podobný	k2eAgInPc6d1	podobný
žánrech	žánr	k1gInPc6	žánr
(	(	kIx(	(
<g/>
grindcore	grindcor	k1gMnSc5	grindcor
<g/>
,	,	kIx,	,
black	black	k6eAd1	black
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
viking	viking	k1gMnSc1	viking
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
gothic	gothic	k1gMnSc1	gothic
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
screamingu	screaming	k1gInSc2	screaming
<g/>
.	.	kIx.	.
</s>
