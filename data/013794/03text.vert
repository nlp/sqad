<s>
Howard	Howard	k1gMnSc1
Carter	Carter	k1gMnSc1
</s>
<s>
Howard	Howard	k1gMnSc1
Carter	Carter	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1874	#num#	k4
<g/>
Kensington	Kensington	k1gInSc4
nebo	nebo	k8xC
Brompton	Brompton	k1gInSc4
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Londýn	Londýn	k1gInSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
lymfom	lymfom	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Putney	Putnea	k1gFnPc1
Vale	val	k1gInSc5
Cemetery	Cemeter	k1gInPc7
Národnost	národnost	k1gFnSc1
</s>
<s>
Britové	Brit	k1gMnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
antropolog	antropolog	k1gMnSc1
<g/>
,	,	kIx,
archeolog	archeolog	k1gMnSc1
a	a	k8xC
egyptolog	egyptolog	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Howard	Howard	k1gMnSc1
Carter	Carter	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1874	#num#	k4
Londýn	Londýn	k1gInSc1
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
archeolog	archeolog	k1gMnSc1
a	a	k8xC
egyptolog	egyptolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
vytváření	vytváření	k1gNnSc6
kopií	kopie	k1gFnPc2
egyptských	egyptský	k2eAgInPc2d1
svitků	svitek	k1gInPc2
a	a	k8xC
maleb	malba	k1gFnPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
pracoval	pracovat	k5eAaImAgMnS
pod	pod	k7c7
EAS	EAS	kA
(	(	kIx(
<g/>
Egyptian	Egyptian	k1gMnSc1
Antiqueities	Antiqueities	k1gMnSc1
Service	Service	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
vystoupil	vystoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
archeologické	archeologický	k2eAgFnSc6d1
práci	práce	k1gFnSc6
byl	být	k5eAaImAgInS
sponzorován	sponzorovat	k5eAaImNgInS
lordem	lord	k1gMnSc7
Carnarvonem	Carnarvon	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1922	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
patnácti	patnáct	k4xCc6
letech	léto	k1gNnPc6
hledání	hledání	k1gNnSc2
<g/>
,	,	kIx,
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
Údolí	údolí	k1gNnSc6
Králů	Král	k1gMnPc2
nedaleko	nedaleko	k7c2
Luxoru	Luxor	k1gInSc2
Tutanchamonovu	Tutanchamonův	k2eAgFnSc4d1
hrobku	hrobka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
několikaletého	několikaletý	k2eAgNnSc2d1
hledání	hledání	k1gNnSc2
Tutanchamonovy	Tutanchamonův	k2eAgFnSc2d1
hrobky	hrobka	k1gFnSc2
musel	muset	k5eAaImAgMnS
čelit	čelit	k5eAaImF
složité	složitý	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
v	v	k7c6
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
tlaku	tlak	k1gInSc2
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
na	na	k7c6
zpřístupnění	zpřístupnění	k1gNnSc6
hrobky	hrobka	k1gFnSc2
a	a	k8xC
překonat	překonat	k5eAaPmF
období	období	k1gNnSc4
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
práce	práce	k1gFnPc1
na	na	k7c4
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
zastaveny	zastaven	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
neustále	neustále	k6eAd1
tlačen	tlačit	k5eAaImNgMnS
k	k	k7c3
rychlejšímu	rychlý	k2eAgNnSc3d2
tempu	tempo	k1gNnSc3
nekompetentními	kompetentní	k2eNgMnPc7d1
odborníky	odborník	k1gMnPc7
a	a	k8xC
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nenapravitelným	napravitelný	k2eNgFnPc3d1
škodám	škoda	k1gFnPc3
na	na	k7c6
nalezených	nalezený	k2eAgInPc6d1
artefaktech	artefakt	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
se	se	k3xPyFc4
pod	pod	k7c7
rukama	ruka	k1gFnPc7
rozpadaly	rozpadat	k5eAaImAgFnP,k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
archeolog	archeolog	k1gMnSc1
„	„	k?
<g/>
amatér	amatér	k1gMnSc1
<g/>
“	“	k?
byl	být	k5eAaImAgMnS
oficiálními	oficiální	k2eAgInPc7d1
vědeckými	vědecký	k2eAgInPc7d1
kruhy	kruh	k1gInPc7
považován	považován	k2eAgMnSc1d1
za	za	k7c4
šťastlivce	šťastlivec	k1gMnPc4
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
se	se	k3xPyFc4
nedočkal	dočkat	k5eNaPmAgMnS
uznání	uznání	k1gNnSc2
v	v	k7c6
Egyptě	Egypt	k1gInSc6
a	a	k8xC
ani	ani	k8xC
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lord	lord	k1gMnSc1
Carnarvon	Carnarvon	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
několik	několik	k4yIc4
let	léto	k1gNnPc2
vykopávky	vykopávka	k1gFnSc2
financoval	financovat	k5eAaBmAgInS
<g/>
,	,	kIx,
nedostal	dostat	k5eNaPmAgMnS
od	od	k7c2
tehdejší	tehdejší	k2eAgFnSc2d1
egyptské	egyptský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
ani	ani	k8xC
slíbenou	slíbený	k2eAgFnSc4d1
část	část	k1gFnSc4
nálezu	nález	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
zvykem	zvyk	k1gInSc7
jako	jako	k8xS,k8xC
kompenzace	kompenzace	k1gFnSc1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
vdova	vdova	k1gFnSc1
po	po	k7c6
lordu	lord	k1gMnSc3
Carnarvonovi	Carnarvon	k1gMnSc3
musela	muset	k5eAaImAgFnS
prodat	prodat	k5eAaPmF
z	z	k7c2
důvodu	důvod	k1gInSc2
finanční	finanční	k2eAgFnSc2d1
tísně	tíseň	k1gFnSc2
jeho	jeho	k3xOp3gFnSc4
sbírku	sbírka	k1gFnSc4
Metropolitnímu	metropolitní	k2eAgNnSc3d1
muzeu	muzeum	k1gNnSc3
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc4
nesla	nést	k5eAaImAgFnS
britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
dost	dost	k6eAd1
nelibě	libě	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Údolí	údolí	k1gNnSc1
Králů	Král	k1gMnPc2
v	v	k7c6
Egyptě	Egypt	k1gInSc6
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
na	na	k7c4
rakovinu	rakovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
komtur	komtur	k1gMnSc1
Řádu	řád	k1gInSc2
Nilu	Nil	k1gInSc2
–	–	k?
Egyptské	egyptský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
1926	#num#	k4
–	–	k?
udělil	udělit	k5eAaPmAgMnS
egyptský	egyptský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fuad	Fuad	k1gMnSc1
I.	I.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
doktor	doktor	k1gMnSc1
věd	věda	k1gFnPc2
na	na	k7c4
Yale	Yale	k1gNnSc4
University	universita	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
čestný	čestný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělsko	Španělsko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Scotsman	Scotsman	k1gMnSc1
<g/>
,	,	kIx,
Saturday	Saturdaa	k1gFnPc1
27	#num#	k4
March	Marcha	k1gFnPc2
1926	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
81	#num#	k4
2	#num#	k4
Howard	Howard	k1gMnSc1
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
64	#num#	k4
<g/>
,	,	kIx,
Egyptologist	Egyptologist	k1gInSc1
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
<g/>
.	.	kIx.
archive	archiv	k1gInSc5
<g/>
.	.	kIx.
<g/>
nytimes	nytimesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VANDENBERG	VANDENBERG	kA
<g/>
,	,	kIx,
Philipp	Philipp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
z	z	k7c2
Luxoru	Luxor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
Klub	klub	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
589	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
9788024209715	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JACQ	JACQ	kA
<g/>
,	,	kIx,
Christian	Christian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aféra	aféra	k1gFnSc1
Tutanchamon	Tutanchamona	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Alpress	Alpress	k1gInSc1
<g/>
,	,	kIx,
19990	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
9788072181834	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Howard	Howarda	k1gFnPc2
Carter	Cartrum	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Howard	Howard	k1gMnSc1
Carter	Carter	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700286	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118519328	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
6958	#num#	k4
9012	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50033940	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500009930	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
121597058	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50033940	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Starověký	starověký	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
</s>
