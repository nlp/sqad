<s>
Draslík	draslík	k1gInSc1	draslík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
K	K	kA	K
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Kalium	kalium	k1gNnSc1	kalium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
i	i	k8xC	i
živých	živý	k2eAgInPc6d1	živý
organizmech	organizmus	k1gInPc6	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
jeho	on	k3xPp3gInSc2	on
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
názvu	název	k1gInSc2	název
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc4	Presl
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
a	a	k8xC	a
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1	stříbrolesklý
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
krájet	krájet	k5eAaImF	krájet
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
měkčí	měkký	k2eAgInSc1d2	měkčí
než	než	k8xS	než
mastek	mastek	k1gInSc1	mastek
<g/>
,	,	kIx,	,
lithium	lithium	k1gNnSc1	lithium
i	i	k8xC	i
sodík	sodík	k1gInSc1	sodík
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
tvrdost	tvrdost	k1gFnSc1	tvrdost
se	se	k3xPyFc4	se
v	v	k7c6	v
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
stupně	stupeň	k1gInSc2	stupeň
0,5	[number]	k4	0,5
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
vede	vést	k5eAaImIp3nS	vést
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc4d1	nízký
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
parách	para	k1gFnPc6	para
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jednoatomových	jednoatomový	k2eAgFnPc2d1	jednoatomový
částic	částice	k1gFnPc2	částice
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
dvouatomové	dvouatomový	k2eAgFnPc1d1	dvouatomová
molekuly	molekula	k1gFnPc1	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
mají	mít	k5eAaImIp3nP	mít
modrou	modrý	k2eAgFnSc4d1	modrá
až	až	k9	až
modrozelenou	modrozelený	k2eAgFnSc4d1	modrozelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tmavěmodrého	tmavěmodrý	k2eAgInSc2d1	tmavěmodrý
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
kovový	kovový	k2eAgInSc1d1	kovový
draslík	draslík	k1gInSc1	draslík
lze	lze	k6eAd1	lze
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchovávat	uchovávat	k5eAaImF	uchovávat
pouze	pouze	k6eAd1	pouze
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabráníme	zabránit	k5eAaPmIp1nP	zabránit
jeho	on	k3xPp3gInSc2	on
styku	styk	k1gInSc2	styk
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
nebo	nebo	k8xC	nebo
vodními	vodní	k2eAgFnPc7d1	vodní
parami	para	k1gFnPc7	para
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
překrývá	překrývat	k5eAaImIp3nS	překrývat
vrstvou	vrstva	k1gFnSc7	vrstva
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
jako	jako	k8xC	jako
petrolej	petrolej	k1gInSc1	petrolej
nebo	nebo	k8xC	nebo
nafta	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yQgFnPc7	který
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
draselný	draselný	k2eAgInSc1d1	draselný
ion	ion	k1gInSc1	ion
K	K	kA	K
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
lze	lze	k6eAd1	lze
však	však	k9	však
také	také	k9	také
připravit	připravit	k5eAaPmF	připravit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
superbáze	superbáze	k1gFnSc2	superbáze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
draslík	draslík	k1gInSc4	draslík
draslidový	draslidový	k2eAgInSc4d1	draslidový
anion	anion	k1gInSc4	anion
K-	K-	k1gFnSc2	K-
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
draslík	draslík	k1gInSc1	draslík
tak	tak	k6eAd1	tak
zaplní	zaplnit	k5eAaPmIp3nS	zaplnit
s-orbital	srbital	k1gMnSc1	s-orbital
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
stabilní	stabilní	k2eAgFnSc4d1	stabilní
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
konfiguraci	konfigurace	k1gFnSc4	konfigurace
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
draslík	draslík	k1gInSc1	draslík
má	mít	k5eAaImIp3nS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
ionizační	ionizační	k2eAgFnSc4d1	ionizační
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
afinitu	afinita	k1gFnSc4	afinita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
dojde	dojít	k5eAaPmIp3nS	dojít
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejsilnější	silný	k2eAgNnPc4d3	nejsilnější
redukční	redukční	k2eAgNnPc4d1	redukční
činidla	činidlo	k1gNnPc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
až	až	k9	až
explozivně	explozivně	k6eAd1	explozivně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
draslík	draslík	k1gInSc1	draslík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
na	na	k7c4	na
superoxid	superoxid	k1gInSc4	superoxid
draselný	draselný	k2eAgInSc1d1	draselný
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
draselný	draselný	k2eAgInSc1d1	draselný
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
proto	proto	k8xC	proto
setkáváme	setkávat	k5eAaImIp1nP	setkávat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
draslíku	draslík	k1gInSc2	draslík
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
exotermní	exotermní	k2eAgNnSc1d1	exotermní
<g/>
,	,	kIx,	,
že	že	k8xS	že
unikající	unikající	k2eAgInSc1d1	unikající
vodík	vodík	k1gInSc1	vodík
reakčním	reakční	k2eAgNnSc7d1	reakční
teplem	teplo	k1gNnSc7	teplo
samovolně	samovolně	k6eAd1	samovolně
explozivně	explozivně	k6eAd1	explozivně
vzplane	vzplanout	k5eAaPmIp3nS	vzplanout
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
se	se	k3xPyFc4	se
také	také	k9	také
za	za	k7c2	za
mírného	mírný	k2eAgNnSc2d1	mírné
zahřátí	zahřátí	k1gNnSc2	zahřátí
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
na	na	k7c4	na
hydrid	hydrid	k1gInSc4	hydrid
draselný	draselný	k2eAgInSc1d1	draselný
KH	kh	k0	kh
<g/>
,	,	kIx,	,
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
na	na	k7c4	na
nitrid	nitrid	k1gInSc4	nitrid
draselný	draselný	k2eAgMnSc1d1	draselný
K3N	K3N	k1gMnSc1	K3N
nebo	nebo	k8xC	nebo
azid	azid	k1gInSc1	azid
draselný	draselný	k2eAgInSc1d1	draselný
KN	KN	kA	KN
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímo	přímo	k6eNd1	přímo
se	se	k3xPyFc4	se
také	také	k9	také
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnSc3	sůl
draslíku	draslík	k1gInSc2	draslík
barví	barvit	k5eAaImIp3nS	barvit
plamen	plamen	k1gInSc1	plamen
intenzivně	intenzivně	k6eAd1	intenzivně
fialově	fialově	k6eAd1	fialově
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
při	při	k7c6	při
stopách	stopa	k1gFnPc6	stopa
sodné	sodný	k2eAgFnSc2d1	sodná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
se	se	k3xPyFc4	se
plamen	plamen	k1gInSc1	plamen
barví	barvit	k5eAaImIp3nS	barvit
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
na	na	k7c4	na
takový	takový	k3xDgInSc4	takový
plamen	plamen	k1gInSc4	plamen
dívat	dívat	k5eAaImF	dívat
skrz	skrz	k7c4	skrz
modré	modrý	k2eAgNnSc4d1	modré
kobaltové	kobaltový	k2eAgNnSc4d1	kobaltové
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
draselných	draselný	k2eAgFnPc6d1	draselná
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
již	již	k6eAd1	již
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
látku	látka	k1gFnSc4	látka
neter	netra	k1gFnPc2	netra
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
praní	praní	k1gNnSc2	praní
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
samá	samý	k3xTgFnSc1	samý
látka	látka	k1gFnSc1	látka
byla	být	k5eAaImAgFnS	být
dobře	dobře	k6eAd1	dobře
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
Egypťanům	Egypťan	k1gMnPc3	Egypťan
<g/>
,	,	kIx,	,
Řekům	Řek	k1gMnPc3	Řek
a	a	k8xC	a
Římanům	Říman	k1gMnPc3	Říman
(	(	kIx(	(
<g/>
Římané	Říman	k1gMnPc1	Říman
ji	on	k3xPp3gFnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
nitrum	nitrum	k1gInSc4	nitrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
dnes	dnes	k6eAd1	dnes
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
soda	soda	k1gFnSc1	soda
-	-	kIx~	-
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
Na	na	k7c6	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
sodě	soda	k1gFnSc6	soda
byl	být	k5eAaImAgInS	být
přimíchán	přimíchat	k5eAaPmNgInS	přimíchat
i	i	k9	i
potaš	potaš	k1gInSc1	potaš
-	-	kIx~	-
uhličitan	uhličitan	k1gInSc1	uhličitan
draselný	draselný	k2eAgInSc1d1	draselný
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
odlišit	odlišit	k5eAaPmF	odlišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dal	dát	k5eAaPmAgMnS	dát
alchymista	alchymista	k1gMnSc1	alchymista
Geber	Geber	k1gMnSc1	Geber
této	tento	k3xDgFnSc3	tento
sloučenině	sloučenina	k1gFnSc3	sloučenina
název	název	k1gInSc4	název
alkali	alkat	k5eAaBmAgMnP	alkat
<g/>
.	.	kIx.	.
</s>
<s>
Oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
sebe	se	k3xPyFc2	se
sodu	soda	k1gFnSc4	soda
a	a	k8xC	a
potaš	potaš	k1gFnSc4	potaš
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
povedlo	povést	k5eAaPmAgNnS	povést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
Stahlovi	Stahlův	k2eAgMnPc1d1	Stahlův
a	a	k8xC	a
experimentálně	experimentálně	k6eAd1	experimentálně
to	ten	k3xDgNnSc4	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
roku	rok	k1gInSc2	rok
1736	[number]	k4	1736
Duhamel	Duhamel	k1gMnSc1	Duhamel
de	de	k?	de
Monceau	Monceaa	k1gFnSc4	Monceaa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
Mergraf	Mergraf	k1gMnSc1	Mergraf
odlišil	odlišit	k5eAaPmAgMnS	odlišit
oba	dva	k4xCgInPc4	dva
kovy	kov	k1gInPc4	kov
na	na	k7c6	na
základě	základ	k1gInSc6	základ
plamenových	plamenový	k2eAgFnPc2d1	plamenová
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
kov	kov	k1gInSc1	kov
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
siru	sir	k1gMnSc3	sir
Humphry	Humphra	k1gFnPc4	Humphra
Davymu	Davym	k1gInSc2	Davym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
elektrolyzoval	elektrolyzovat	k5eAaImAgInS	elektrolyzovat
kus	kus	k1gInSc4	kus
roztaveného	roztavený	k2eAgInSc2d1	roztavený
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
v	v	k7c6	v
platinové	platinový	k2eAgFnSc6d1	platinová
misce	miska	k1gFnSc6	miska
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2,0	[number]	k4	2,0
-	-	kIx~	-
2,4	[number]	k4	2,4
%	%	kIx~	%
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
380	[number]	k4	380
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
výskyt	výskyt	k1gInSc1	výskyt
1	[number]	k4	1
atomu	atom	k1gInSc2	atom
draslíku	draslík	k1gInSc2	draslík
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
významného	významný	k2eAgInSc2d1	významný
podílu	podíl	k1gInSc2	podíl
draslíku	draslík	k1gInSc2	draslík
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
soli	sůl	k1gFnSc6	sůl
jej	on	k3xPp3gInSc4	on
nalézáme	nalézat	k5eAaImIp1nP	nalézat
také	také	k9	také
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
podzemních	podzemní	k2eAgFnPc6d1	podzemní
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
sylvín	sylvín	k1gInSc1	sylvín
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
KCl	KCl	k1gFnSc1	KCl
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
také	také	k9	také
dusičnan	dusičnan	k1gInSc1	dusičnan
draselný	draselný	k2eAgInSc1d1	draselný
KNO	KNO	kA	KNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
ledek	ledek	k1gInSc1	ledek
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
draselným	draselný	k2eAgInPc3d1	draselný
minerálům	minerál	k1gInPc3	minerál
patří	patřit	k5eAaImIp3nS	patřit
arcanit	arcanit	k1gInSc1	arcanit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
kainit	kainit	k1gInSc4	kainit
KCl	KCl	k1gFnSc2	KCl
<g/>
.	.	kIx.	.
</s>
<s>
MgSO	MgSO	k?	MgSO
<g/>
4.3	[number]	k4	4.3
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
karnalit	karnalit	k1gInSc1	karnalit
KCl	KCl	k1gFnSc1	KCl
<g/>
.	.	kIx.	.
<g/>
MgCl	MgCl	k1gInSc1	MgCl
<g/>
2.6	[number]	k4	2.6
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
glaserit	glaserit	k1gInSc1	glaserit
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4.3	[number]	k4	4.3
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
polyhalit	polyhalit	k1gInSc1	polyhalit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
MgSO	MgSO	k1gFnSc1	MgSO
<g/>
4.2	[number]	k4	4.2
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4.2	[number]	k4	4.2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
schönit	schönit	k1gInSc1	schönit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
MgSO	MgSO	k1gFnSc1	MgSO
<g/>
4.6	[number]	k4	4.6
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
langbeinit	langbeinit	k5eAaPmF	langbeinit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4.2	[number]	k4	4.2
MgSO	MgSO	k1gFnSc1	MgSO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
kalinit	kalinit	k5eAaPmF	kalinit
KAl	kal	k1gInSc4	kal
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
živců	živec	k1gInPc2	živec
<g/>
,	,	kIx,	,
slíd	slída	k1gFnPc2	slída
<g/>
,	,	kIx,	,
alkalických	alkalický	k2eAgInPc2d1	alkalický
pyroxenů	pyroxen	k1gInPc2	pyroxen
<g/>
,	,	kIx,	,
alkalických	alkalický	k2eAgInPc2d1	alkalický
amfibolů	amfibol	k1gInPc2	amfibol
a	a	k8xC	a
zeolitů	zeolit	k1gInPc2	zeolit
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sodíkem	sodík	k1gInSc7	sodík
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
jejich	jejich	k3xOp3gFnPc2	jejich
koncentrací	koncentrace	k1gFnPc2	koncentrace
v	v	k7c6	v
buněčných	buněčný	k2eAgFnPc6d1	buněčná
tekutinách	tekutina	k1gFnPc6	tekutina
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
zdravý	zdravý	k2eAgInSc4d1	zdravý
vývoj	vývoj	k1gInSc4	vývoj
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
zdůrazňována	zdůrazňován	k2eAgFnSc1d1	zdůrazňována
významná	významný	k2eAgFnSc1d1	významná
role	role	k1gFnSc1	role
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
konzumace	konzumace	k1gFnSc1	konzumace
sodných	sodný	k2eAgFnPc2d1	sodná
solí	sůl	k1gFnPc2	sůl
je	být	k5eAaImIp3nS	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
zdraví	zdraví	k1gNnSc4	zdraví
ohrožující	ohrožující	k2eAgMnSc1d1	ohrožující
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
draslíku	draslík	k1gInSc2	draslík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
ven	ven	k6eAd1	ven
dochází	docházet	k5eAaImIp3nS	docházet
pomocí	pomocí	k7c2	pomocí
draslíkových	draslíkův	k2eAgInPc2d1	draslíkův
kanálů	kanál	k1gInPc2	kanál
při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
vzruchu	vzruch	k1gInSc2	vzruch
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
draslík	draslík	k1gInSc1	draslík
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
taveniny	tavenina	k1gFnSc2	tavenina
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
hydroxid	hydroxid	k1gInSc1	hydroxid
draselný	draselný	k2eAgInSc1d1	draselný
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgFnSc4d2	nižší
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
než	než	k8xS	než
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
volena	volit	k5eAaImNgFnS	volit
touto	tento	k3xDgFnSc7	tento
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
trochu	trochu	k6eAd1	trochu
komplikovanější	komplikovaný	k2eAgFnSc7d2	komplikovanější
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vhodnými	vhodný	k2eAgFnPc7d1	vhodná
přísadami	přísada	k1gFnPc7	přísada
podařilo	podařit	k5eAaPmAgNnS	podařit
výrazně	výrazně	k6eAd1	výrazně
snížit	snížit	k5eAaPmF	snížit
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kovový	kovový	k2eAgInSc1d1	kovový
draslík	draslík	k1gInSc1	draslík
nelze	lze	k6eNd1	lze
průmyslově	průmyslově	k6eAd1	průmyslově
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
)	)	kIx)	)
vyrábět	vyrábět	k5eAaImF	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztaveného	roztavený	k2eAgInSc2d1	roztavený
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
draslík	draslík	k1gInSc1	draslík
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
reakce	reakce	k1gFnSc1	reakce
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
lze	lze	k6eAd1	lze
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
tuto	tento	k3xDgFnSc4	tento
výrobu	výroba	k1gFnSc4	výroba
využít	využít	k5eAaPmF	využít
(	(	kIx(	(
<g/>
Pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
CaCl	CaCl	k1gInSc1	CaCl
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Materiálem	materiál	k1gInSc7	materiál
katody	katoda	k1gFnSc2	katoda
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
anoda	anoda	k1gFnSc1	anoda
je	být	k5eAaImIp3nS	být
grafitová	grafitový	k2eAgFnSc1d1	grafitová
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
produktem	produkt	k1gInSc7	produkt
této	tento	k3xDgFnSc2	tento
elektrolýzy	elektrolýza	k1gFnSc2	elektrolýza
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
ihned	ihned	k6eAd1	ihned
dále	daleko	k6eAd2	daleko
zužitkován	zužitkovat	k5eAaPmNgInS	zužitkovat
pro	pro	k7c4	pro
chemickou	chemický	k2eAgFnSc4d1	chemická
syntézu	syntéza	k1gFnSc4	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Železná	železný	k2eAgFnSc1d1	železná
katoda	katoda	k1gFnSc1	katoda
2	[number]	k4	2
K	K	kA	K
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
e-	e-	k?	e-
→	→	k?	→
2	[number]	k4	2
K	K	kA	K
Grafitová	grafitový	k2eAgFnSc1d1	grafitová
anoda	anoda	k1gFnSc1	anoda
2	[number]	k4	2
Cl-	Cl-	k1gMnPc2	Cl-
→	→	k?	→
Cl	Cl	k1gFnSc7	Cl
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
e-	e-	k?	e-
Draslík	draslík	k1gInSc1	draslík
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
redukcí	redukce	k1gFnSc7	redukce
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
sodíkem	sodík	k1gInSc7	sodík
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
destilací	destilace	k1gFnSc7	destilace
draslíku	draslík	k1gInSc2	draslík
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
vyrábět	vyrábět	k5eAaImF	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
kyslíkatých	kyslíkatý	k2eAgFnPc2d1	kyslíkatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
draslíku	draslík	k1gInSc2	draslík
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
v	v	k7c6	v
halogenidech	halogenid	k1gInPc6	halogenid
draselných	draselný	k2eAgInPc2d1	draselný
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc1	oxid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
peroxid	peroxid	k1gInSc1	peroxid
draselný	draselný	k2eAgInSc1d1	draselný
nebo	nebo	k8xC	nebo
superoxid	superoxid	k1gInSc1	superoxid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
grafitové	grafitový	k2eAgFnSc6d1	grafitová
anodě	anoda	k1gFnSc6	anoda
uniká	unikat	k5eAaImIp3nS	unikat
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
reakcí	reakce	k1gFnPc2	reakce
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
grafitovou	grafitový	k2eAgFnSc7d1	grafitová
anodou	anoda	k1gFnSc7	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
lze	lze	k6eAd1	lze
také	také	k9	také
připravit	připravit	k5eAaPmF	připravit
reakcí	reakce	k1gFnSc7	reakce
fluoridu	fluorid	k1gInSc2	fluorid
draselného	draselný	k2eAgInSc2d1	draselný
s	s	k7c7	s
karbidem	karbid	k1gInSc7	karbid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
mimořádné	mimořádný	k2eAgFnSc3d1	mimořádná
nestálosti	nestálost	k1gFnSc3	nestálost
a	a	k8xC	a
reaktivitě	reaktivita	k1gFnSc6	reaktivita
se	se	k3xPyFc4	se
čistý	čistý	k2eAgInSc1d1	čistý
kovový	kovový	k2eAgInSc1d1	kovový
draslík	draslík	k1gInSc1	draslík
prakticky	prakticky	k6eAd1	prakticky
využívá	využívat	k5eAaPmIp3nS	využívat
pouze	pouze	k6eAd1	pouze
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
redukčním	redukční	k2eAgFnPc3d1	redukční
reakcím	reakce	k1gFnPc3	reakce
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
nebo	nebo	k8xC	nebo
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
se	se	k3xPyFc4	se
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
fotoelektrických	fotoelektrický	k2eAgInPc2d1	fotoelektrický
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mýdel	mýdlo	k1gNnPc2	mýdlo
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
vyššími	vysoký	k2eAgNnPc7d2	vyšší
tzv.	tzv.	kA	tzv.
mastnými	mastný	k2eAgFnPc7d1	mastná
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Draselná	draselný	k2eAgNnPc1d1	draselné
mýdla	mýdlo	k1gNnPc1	mýdlo
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
tekutá	tekutý	k2eAgFnSc1d1	tekutá
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sodných	sodný	k2eAgInPc2d1	sodný
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgFnSc1	všechen
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
celulosy	celulosa	k1gFnSc2	celulosa
<g/>
,	,	kIx,	,
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
umělého	umělý	k2eAgNnSc2d1	umělé
hedvábí	hedvábí	k1gNnSc2	hedvábí
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
potaš	potaš	k1gFnSc1	potaš
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
a	a	k8xC	a
papírenském	papírenský	k2eAgInSc6d1	papírenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
pracích	prací	k2eAgInPc2d1	prací
prášků	prášek	k1gInPc2	prášek
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
v	v	k7c6	v
barvířství	barvířství	k1gNnSc6	barvířství
a	a	k8xC	a
běličství	běličství	k1gNnSc6	běličství
a	a	k8xC	a
při	při	k7c6	při
praní	praní	k1gNnSc6	praní
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
kyanidu	kyanid	k1gInSc2	kyanid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
draselné	draselný	k2eAgNnSc1d1	draselné
hnojivo	hnojivo	k1gNnSc1	hnojivo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nalézá	nalézat	k5eAaImIp3nS	nalézat
využití	využití	k1gNnSc1	využití
v	v	k7c6	v
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
jako	jako	k8xC	jako
silné	silný	k2eAgNnSc4d1	silné
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
kamence	kamenec	k1gInSc2	kamenec
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
příjem	příjem	k1gInSc4	příjem
draslíku	draslík	k1gInSc2	draslík
u	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
lidí	člověk	k1gMnPc2	člověk
problematický	problematický	k2eAgInSc1d1	problematický
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc1	draslík
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
mléčných	mléčný	k2eAgInPc6d1	mléčný
výrobcích	výrobek	k1gInPc6	výrobek
<g/>
,	,	kIx,	,
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
,	,	kIx,	,
zelenině	zelenina	k1gFnSc6	zelenina
<g/>
,	,	kIx,	,
obilovinách	obilovina	k1gFnPc6	obilovina
<g/>
,	,	kIx,	,
bramborách	brambora	k1gFnPc6	brambora
a	a	k8xC	a
kávě	káva	k1gFnSc3	káva
<g/>
.	.	kIx.	.
</s>
<s>
Organismus	organismus	k1gInSc1	organismus
se	se	k3xPyFc4	se
špatně	špatně	k6eAd1	špatně
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
jak	jak	k8xC	jak
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
s	s	k7c7	s
přebytkem	přebytek	k1gInSc7	přebytek
draslíku	draslík	k1gInSc2	draslík
(	(	kIx(	(
<g/>
problémy	problém	k1gInPc4	problém
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
diabetiků	diabetik	k1gMnPc2	diabetik
<g/>
,	,	kIx,	,
onkologických	onkologický	k2eAgMnPc2d1	onkologický
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
chorobami	choroba	k1gFnPc7	choroba
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
jater	játra	k1gNnPc2	játra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gFnSc1	jeho
denní	denní	k2eAgFnSc1d1	denní
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
dávka	dávka	k1gFnSc1	dávka
3700	[number]	k4	3700
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
draselný	draselný	k2eAgInSc1d1	draselný
KH	kh	k0	kh
je	on	k3xPp3gFnPc4	on
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
a	a	k8xC	a
i	i	k9	i
na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
velmi	velmi	k6eAd1	velmi
živě	živě	k6eAd1	živě
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
draselný	draselný	k2eAgInSc1d1	draselný
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
silným	silný	k2eAgNnSc7d1	silné
redukčním	redukční	k2eAgNnSc7d1	redukční
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
mírně	mírně	k6eAd1	mírně
zahřátého	zahřátý	k2eAgInSc2d1	zahřátý
draslíku	draslík	k1gInSc2	draslík
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
draselný	draselný	k2eAgInSc1d1	draselný
K2O	K2O	k1gFnSc4	K2O
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
nažloutlý	nažloutlý	k2eAgInSc1d1	nažloutlý
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
zahříváním	zahřívání	k1gNnSc7	zahřívání
peroxidu	peroxid	k1gInSc2	peroxid
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
superoxidu	superoxid	k1gInSc2	superoxid
draselného	draselný	k2eAgInSc2d1	draselný
s	s	k7c7	s
draslíkem	draslík	k1gInSc7	draslík
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
s	s	k7c7	s
draslíkem	draslík	k1gInSc7	draslík
<g/>
.	.	kIx.	.
</s>
<s>
Draslík	draslík	k1gInSc4	draslík
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
další	další	k2eAgFnPc1d1	další
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
složení	složení	k1gNnSc4	složení
K2O2	K2O2	k1gFnSc3	K2O2
a	a	k8xC	a
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Superoxid	superoxid	k1gInSc1	superoxid
draselný	draselný	k2eAgInSc1d1	draselný
neboli	neboli	k8xC	neboli
hyperoxid	hyperoxid	k1gInSc1	hyperoxid
draselný	draselný	k2eAgInSc1d1	draselný
KO2	KO2	k1gFnSc4	KO2
je	být	k5eAaImIp3nS	být
oranžový	oranžový	k2eAgInSc1d1	oranžový
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silným	silný	k2eAgNnSc7d1	silné
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žáru	žár	k1gInSc6	žár
je	být	k5eAaImIp3nS	být
docela	docela	k6eAd1	docela
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
oxidovatelné	oxidovatelný	k2eAgFnSc2d1	oxidovatelný
látky	látka	k1gFnSc2	látka
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
předává	předávat	k5eAaImIp3nS	předávat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
peroxid	peroxid	k1gInSc4	peroxid
draselný	draselný	k2eAgInSc4d1	draselný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
předat	předat	k5eAaPmF	předat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
přejít	přejít	k5eAaPmF	přejít
až	až	k9	až
v	v	k7c4	v
oxid	oxid	k1gInSc4	oxid
draselný	draselný	k2eAgInSc4d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Superoxid	superoxid	k1gInSc1	superoxid
draselný	draselný	k2eAgInSc1d1	draselný
vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
draslíku	draslík	k1gInSc2	draslík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
+	+	kIx~	+
O2	O2	k1gFnSc3	O2
→	→	k?	→
KO2	KO2	k1gFnSc3	KO2
Hydroxid	hydroxid	k1gInSc4	hydroxid
draselný	draselný	k2eAgInSc4d1	draselný
KOH	KOH	kA	KOH
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
,	,	kIx,	,
křehká	křehký	k2eAgFnSc1d1	křehká
<g/>
,	,	kIx,	,
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
leptavá	leptavý	k2eAgFnSc1d1	leptavá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
i	i	k9	i
sklo	sklo	k1gNnSc1	sklo
a	a	k8xC	a
porcelán	porcelán	k1gInSc1	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
roztok	roztok	k1gInSc1	roztok
se	se	k3xPyFc4	se
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
draselný	draselný	k2eAgInSc1d1	draselný
vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
peroxidu	peroxid	k1gInSc2	peroxid
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
superoxidu	superoxid	k1gInSc2	superoxid
draselného	draselný	k2eAgMnSc4d1	draselný
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Draselné	draselný	k2eAgFnPc1d1	draselná
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
a	a	k8xC	a
jen	jen	k9	jen
několik	několik	k4yIc1	několik
je	být	k5eAaImIp3nS	být
nerozpustných	rozpustný	k2eNgMnPc6d1	nerozpustný
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgMnPc4	všechen
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
anion	anion	k1gInSc1	anion
soli	sůl	k1gFnSc2	sůl
barevný	barevný	k2eAgMnSc1d1	barevný
(	(	kIx(	(
<g/>
manganistany	manganistan	k1gInPc1	manganistan
<g/>
,	,	kIx,	,
chromany	chroman	k1gInPc1	chroman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Draselné	draselný	k2eAgFnPc4d1	draselná
soli	sůl	k1gFnPc4	sůl
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
snadno	snadno	k6eAd1	snadno
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
nesnadno	snadno	k6eNd1	snadno
komplexy	komplex	k1gInPc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
50	[number]	k4	50
lety	léto	k1gNnPc7	léto
nebyly	být	k5eNaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
žádné	žádný	k3yNgInPc1	žádný
komplexy	komplex	k1gInPc1	komplex
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
vůbec	vůbec	k9	vůbec
schopny	schopen	k2eAgFnPc1d1	schopna
tvořit	tvořit	k5eAaImF	tvořit
komplexy	komplex	k1gInPc4	komplex
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
KCl	KCl	k1gFnSc4	KCl
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
hořké	hořký	k2eAgFnSc2d1	hořká
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
sylvín	sylvín	k1gInSc1	sylvín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
draselných	draselný	k2eAgInPc2d1	draselný
nerostů	nerost	k1gInPc2	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
vyrobit	vyrobit	k5eAaPmF	vyrobit
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
draselného	draselný	k2eAgInSc2d1	draselný
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
draselný	draselný	k2eAgInSc1d1	draselný
KF	KF	kA	KF
<g/>
,	,	kIx,	,
bromid	bromid	k1gInSc1	bromid
draselný	draselný	k2eAgInSc1d1	draselný
KBr	KBr	k1gMnSc7	KBr
a	a	k8xC	a
jodid	jodid	k1gInSc1	jodid
draselný	draselný	k2eAgInSc1d1	draselný
KI	KI	kA	KI
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
velmi	velmi	k6eAd1	velmi
podobají	podobat	k5eAaImIp3nP	podobat
chloridu	chlorid	k1gInSc3	chlorid
draselnému	draselný	k2eAgInSc3d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
draselný	draselný	k2eAgInSc1d1	draselný
neboli	neboli	k8xC	neboli
potaš	potaš	k1gInSc1	potaš
K2CO3	K2CO3	k1gFnSc2	K2CO3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
s	s	k7c7	s
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
draselných	draselný	k2eAgFnPc2d1	draselná
solí	sůl	k1gFnPc2	sůl
-	-	kIx~	-
např.	např.	kA	např.
stassfurtský	stassfurtský	k2eAgInSc1d1	stassfurtský
způsob	způsob	k1gInSc1	způsob
<g/>
.	.	kIx.	.
2	[number]	k4	2
KCl	KCl	k1gFnPc2	KCl
+	+	kIx~	+
3	[number]	k4	3
MgCO	MgCO	k1gFnPc2	MgCO
<g/>
3.3	[number]	k4	3.3
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
→	→	k?	→
2	[number]	k4	2
MgCO	MgCO	k1gFnSc1	MgCO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
KHCO	KHCO	kA	KHCO
<g/>
3.4	[number]	k4	3.4
H2O	H2O	k1gFnPc1	H2O
+	+	kIx~	+
MgCl	MgCl	k1gInSc1	MgCl
<g/>
2	[number]	k4	2
2	[number]	k4	2
MgCO	MgCO	k1gFnPc2	MgCO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
KHCO	KHCO	kA	KHCO
<g/>
3.4	[number]	k4	3.4
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
<g/>
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
<g/>
→	→	k?	→
K2CO3	K2CO3	k1gMnSc1	K2CO3
+	+	kIx~	+
2	[number]	k4	2
MgCO	MgCO	k1gFnPc2	MgCO
<g/>
3.3	[number]	k4	3.3
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
+	+	kIx~	+
3H2O	[number]	k4	3H2O
Dusičnan	dusičnan	k1gInSc1	dusičnan
draselný	draselný	k2eAgInSc1d1	draselný
KNO3	KNO3	k1gMnPc7	KNO3
neboli	neboli	k8xC	neboli
(	(	kIx(	(
<g/>
draselný	draselný	k2eAgInSc1d1	draselný
ledek	ledek	k1gInSc1	ledek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Zahříváním	zahřívání	k1gNnSc7	zahřívání
odštěpuje	odštěpovat	k5eAaImIp3nS	odštěpovat
dusičnan	dusičnan	k1gInSc1	dusičnan
draselný	draselný	k2eAgInSc1d1	draselný
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
dusitan	dusitan	k1gInSc4	dusitan
draselný	draselný	k2eAgInSc4d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vzniká	vznikat	k5eAaImIp3nS	vznikat
bakteriálním	bakteriální	k2eAgInSc7d1	bakteriální
rozkladem	rozklad	k1gInSc7	rozklad
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
uhličitanu	uhličitan	k1gInSc2	uhličitan
draselného	draselný	k2eAgInSc2d1	draselný
či	či	k8xC	či
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgMnSc2d1	draselný
na	na	k7c4	na
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyrobit	vyrobit	k5eAaPmF	vyrobit
reakcí	reakce	k1gFnSc7	reakce
uhličitanu	uhličitan	k1gInSc2	uhličitan
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgMnSc4d1	draselný
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
draselný	draselný	k2eAgInSc1d1	draselný
K2SO4	K2SO4	k1gFnSc4	K2SO4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
síranu	síran	k1gInSc3	síran
sodnému	sodný	k2eAgNnSc3d1	sodné
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
draselného	draselný	k2eAgMnSc4d1	draselný
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
rozkladem	rozklad	k1gInSc7	rozklad
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
dusičnanu	dusičnan	k1gInSc2	dusičnan
draselného	draselný	k2eAgInSc2d1	draselný
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
draslíku	draslík	k1gInSc2	draslík
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
draselné	draselný	k2eAgFnPc1d1	draselná
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
draselné	draselný	k2eAgInPc4d1	draselný
alkoholáty	alkoholát	k1gInPc4	alkoholát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
draselným	draselný	k2eAgFnPc3d1	draselná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
organické	organický	k2eAgInPc4d1	organický
komplexy	komplex	k1gInPc4	komplex
draselných	draselný	k2eAgFnPc2d1	draselná
sloučenin	sloučenina	k1gFnPc2	sloučenina
tzv.	tzv.	kA	tzv.
crowny	crowna	k1gFnSc2	crowna
a	a	k8xC	a
kryptáty	kryptát	k1gInPc4	kryptát
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
organických	organický	k2eAgFnPc2d1	organická
draselných	draselný	k2eAgFnPc2d1	draselná
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
