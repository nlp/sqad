<s>
Princ	princ	k1gMnSc1
William	William	k1gInSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Princ	princ	k1gMnSc1
William	William	k1gInSc4
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
William	William	k6eAd1
Arthur	Arthur	k1gMnSc1
Philip	Philip	k1gMnSc1
Louis	Louis	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1982	#num#	k4
(	(	kIx(
<g/>
38	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Eton	Eton	k1gInSc1
College	Colleg	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
St	St	kA
Andrews	Andrews	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Podvazkový	podvazkový	k2eAgMnSc1d1
řádMedaile	řádMedaile	k6eAd1
zlatého	zlatý	k2eAgNnSc2d1
výročí	výročí	k1gNnSc2
královny	královna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
Řád	řád	k1gInSc1
bodlákuMedaile	bodlákuMedaile	k6eAd1
diamantového	diamantový	k2eAgNnSc2d1
výročí	výročí	k1gNnSc2
královny	královna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Catherine	Catherin	k1gInSc5
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnPc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
Děti	dítě	k1gFnPc1
</s>
<s>
princ	princ	k1gMnSc1
George	Georg	k1gFnSc2
z	z	k7c2
Cambridgeprincezna	Cambridgeprincezna	k1gFnSc1
Charlotte	Charlott	k1gInSc5
z	z	k7c2
Cambridgeprinc	Cambridgeprinc	k1gFnSc4
Louis	louis	k1gInSc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
Rodiče	rodič	k1gMnPc1
</s>
<s>
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesuprincezna	Walesuprincezna	k1gFnSc1
Diana	Diana	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
bratr	bratr	k1gMnSc1
Princ	princ	k1gMnSc1
Harry	Harra	k1gFnSc2
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
ze	z	k7c2
Sussexu	Sussex	k1gInSc2
Funkce	funkce	k1gFnSc1
</s>
<s>
Vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
královská	královský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
</s>
<s>
JV	JV	kA
královna	královna	k1gFnSc1
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
z	z	k7c2
WalesuJkV	WalesuJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
CambridgeJkV	CambridgeJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
George	Georg	k1gMnSc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Charlotte	Charlott	k1gInSc5
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
Louis	Louis	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Vévoda	vévoda	k1gMnSc1
ze	z	k7c2
SussexuVévodkyně	SussexuVévodkyně	k1gFnSc2
ze	z	k7c2
Sussexu	Sussex	k1gInSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Beatrice	Beatrice	k1gFnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Eugenie	Eugenie	k1gFnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
JkV	JkV	k?
hrabě	hrabě	k1gMnSc1
z	z	k7c2
WessexuJkV	WessexuJkV	k1gFnSc2
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Wessexu	Wessex	k1gInSc2
</s>
<s>
JkV	JkV	k?
královská	královský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
Anne	Ann	k1gFnSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
GloucesteruJkV	GloucesteruJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Gloucesteru	Gloucester	k1gInSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
KentuJkV	KentuJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
Michael	Michael	k1gMnSc1
z	z	k7c2
KentuJkV	KentuJkV	k1gFnSc2
princezna	princezna	k1gFnSc1
Michaela	Michaela	k1gFnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
jméno	jméno	k1gNnSc4
se	s	k7c7
všemi	všecek	k3xTgInPc7
tituly	titul	k1gInPc7
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
William	William	k1gInSc1
Arthur	Arthur	k1gMnSc1
Philip	Philip	k1gMnSc1
Louis	Louis	k1gMnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Vilém	Vilém	k1gMnSc1
Artur	Artur	k1gMnSc1
Filip	Filip	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
ze	z	k7c2
Strathearnu	Strathearn	k1gInSc2
<g/>
,	,	kIx,
baron	baron	k1gMnSc1
z	z	k7c2
Carrickfergusu	Carrickfergus	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Charlese	Charles	k1gMnSc2
<g/>
,	,	kIx,
prince	princ	k1gMnSc2
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
první	první	k4xOgFnPc1
manželky	manželka	k1gFnPc1
princezny	princezna	k1gFnSc2
Diany	Diana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
druhého	druhý	k4xOgMnSc4
dědice	dědic	k1gMnSc4
v	v	k7c6
pořadí	pořadí	k1gNnSc6
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
a	a	k8xC
trůny	trůn	k1gInPc4
některých	některý	k3yIgInPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princ	princa	k1gFnPc2
William	William	k1gInSc4
je	být	k5eAaImIp3nS
rytířem	rytíř	k1gMnSc7
Podvazkového	podvazkový	k2eAgInSc2d1
řádu	řád	k1gInSc2
a	a	k8xC
vzděláním	vzdělání	k1gNnSc7
magistr	magistr	k1gMnSc1
umění	umění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
prezidentem	prezident	k1gMnSc7
Anglické	anglický	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
oznámilo	oznámit	k5eAaPmAgNnS
sídlo	sídlo	k1gNnSc1
rodiny	rodina	k1gFnSc2
následníka	následník	k1gMnSc2
trůnu	trůn	k1gInSc2
Clarence	Clarenec	k1gInSc2
House	house	k1gNnSc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
blízko	blízko	k7c2
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
<g/>
,	,	kIx,
zasnoubení	zasnoubení	k1gNnSc4
prince	princ	k1gMnSc2
Williama	William	k1gMnSc2
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
dlouholetou	dlouholetý	k2eAgFnSc7d1
přítelkyní	přítelkyně	k1gFnSc7
Catherine	Catherin	k1gInSc5
Middletonovou	Middletonový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc1
svatba	svatba	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Dětství	dětství	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1982	#num#	k4
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
Sv.	sv.	kA
Marie	Maria	k1gFnSc2
v	v	k7c6
Paddingtonu	Paddington	k1gInSc6
v	v	k7c6
Západním	západní	k2eAgInSc6d1
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
syn	syn	k1gMnSc1
prince	princ	k1gMnSc2
z	z	k7c2
Walesu	Wales	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
druhým	druhý	k4xOgMnSc7
následníkem	následník	k1gMnSc7
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
je	být	k5eAaImIp3nS
oslovován	oslovován	k2eAgInSc1d1
"	"	kIx"
<g/>
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
William	William	k1gInSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pojmenován	pojmenovat	k5eAaPmNgInS
po	po	k7c6
bratranci	bratranec	k1gMnPc7
prince	princ	k1gMnSc2
Charlese	Charles	k1gMnSc2
princi	princ	k1gMnSc3
Williamovi	William	k1gMnSc3
z	z	k7c2
Gloucesteru	Gloucester	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
tragicky	tragicky	k6eAd1
zahynul	zahynout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
byl	být	k5eAaImAgMnS
Charles	Charles	k1gMnSc1
o	o	k7c4
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
mladší	mladý	k2eAgMnSc1d2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
s	s	k7c7
Williamem	William	k1gInSc7
velmi	velmi	k6eAd1
blízký	blízký	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
dítě	dítě	k1gNnSc4
byl	být	k5eAaImAgInS
William	William	k1gInSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
rodiči	rodič	k1gMnSc3
oslovován	oslovován	k2eAgMnSc1d1
Wombat	Wombat	k1gMnSc1
<g/>
,	,	kIx,
Camel	Camel	k1gMnSc1
nebo	nebo	k8xC
Wills	Wills	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
William	William	k1gInSc1
byl	být	k5eAaImAgInS
pokřtěn	pokřtít	k5eAaPmNgInS
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1982	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
82	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc1
své	svůj	k3xOyFgFnSc2
prababičky	prababička	k1gFnSc2
královny	královna	k1gFnSc2
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
arcibiskupem	arcibiskup	k1gMnSc7
z	z	k7c2
Canterbury	Canterbura	k1gFnSc2
v	v	k7c6
Buckinghamském	buckinghamský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
má	mít	k5eAaImIp3nS
šest	šest	k4xCc1
kmotrů	kmotr	k1gMnPc2
a	a	k8xC
kmoter	kmotra	k1gFnPc2
<g/>
,	,	kIx,
mj.	mj.	kA
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
královnina	královnin	k2eAgFnSc1d1
sestřenice	sestřenice	k1gFnSc1
princezna	princezna	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
nebo	nebo	k8xC
bývalý	bývalý	k2eAgMnSc1d1
řecký	řecký	k2eAgMnSc1d1
král	král	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
svého	svůj	k3xOyFgMnSc4
dědečka	dědeček	k1gMnSc4
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
William	William	k1gInSc1
potomkem	potomek	k1gMnSc7
krále	král	k1gMnSc2
Karla	Karel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stuarta	Stuarta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
William	William	k1gInSc1
stane	stanout	k5eAaPmIp3nS
králem	král	k1gMnSc7
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
prvním	první	k4xOgInSc7
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
po	po	k7c6
královně	královna	k1gFnSc6
Anně	Anna	k1gFnSc3
Stuartovně	Stuartovna	k1gFnSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
předky	předek	k1gMnPc4
z	z	k7c2
rodu	rod	k1gInSc2
Stuartovců	Stuartovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
William	William	k1gInSc1
má	mít	k5eAaImIp3nS
mladšího	mladý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
prince	princ	k1gMnSc2
Harryho	Harry	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
15	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
oficiální	oficiální	k2eAgNnSc4d1
veřejné	veřejný	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
si	se	k3xPyFc3
odbyl	odbýt	k5eAaPmAgMnS
během	během	k7c2
návštěvy	návštěva	k1gFnSc2
Cardiffu	Cardiff	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přistání	přistání	k1gNnSc6
šel	jít	k5eAaImAgMnS
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
rodiči	rodič	k1gMnPc7
do	do	k7c2
cardiffské	cardiffský	k2eAgFnSc2d1
katedrály	katedrála	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
prohlídce	prohlídka	k1gFnSc6
podepsal	podepsat	k5eAaPmAgMnS
do	do	k7c2
návštěvní	návštěvní	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Aston	Astona	k1gFnPc2
Villa	Villo	k1gNnSc2
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
princezny	princezna	k1gFnSc2
Diany	Diana	k1gFnSc2
</s>
<s>
Princova	princův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
princezna	princezna	k1gFnSc1
Diana	Diana	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1997	#num#	k4
při	při	k7c6
automobilové	automobilový	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
smrt	smrt	k1gFnSc4
přišla	přijít	k5eAaPmAgFnS
krátce	krátce	k6eAd1
po	po	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
Diana	Diana	k1gFnSc1
strávila	strávit	k5eAaPmAgFnS
s	s	k7c7
oběma	dva	k4xCgInPc7
syny	syn	k1gMnPc7
prázdniny	prázdniny	k1gFnPc4
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okamžiku	okamžik	k1gInSc6
matčina	matčin	k2eAgInSc2d1
skonu	skon	k1gInSc2
se	se	k3xPyFc4
nacházeli	nacházet	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
princové	princ	k1gMnPc1
<g/>
,	,	kIx,
William	William	k1gInSc1
a	a	k8xC
Harry	Harr	k1gInPc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
královnou	královna	k1gFnSc7
Alžbětou	Alžběta	k1gFnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
princem	princ	k1gMnSc7
Charlesem	Charles	k1gMnSc7
<g/>
,	,	kIx,
na	na	k7c6
zámku	zámek	k1gInSc6
Balmoral	Balmoral	k1gFnSc2
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
otec	otec	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
noci	noc	k1gFnSc6
probudil	probudit	k5eAaPmAgMnS
a	a	k8xC
sdělil	sdělit	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
smutnou	smutný	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
matčině	matčin	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
matčině	matčin	k2eAgInSc6d1
pohřbu	pohřeb	k1gInSc6
šel	jít	k5eAaImAgInS
William	William	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
dědečkem	dědeček	k1gMnSc7
princem	princ	k1gMnSc7
Philipem	Philip	k1gMnSc7
<g/>
,	,	kIx,
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
bratrem	bratr	k1gMnSc7
a	a	k8xC
strýcem	strýc	k1gMnSc7
Charlesem	Charles	k1gMnSc7
hrabětem	hrabě	k1gMnSc7
Spencerem	Spencer	k1gMnSc7
za	za	k7c7
rakví	rakev	k1gFnSc7
své	svůj	k3xOyFgFnSc2
matky	matka	k1gFnSc2
od	od	k7c2
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
k	k	k7c3
Westminsterskému	Westminsterský	k2eAgNnSc3d1
opatství	opatství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
William	William	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgMnS
nezávislé	závislý	k2eNgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
chodil	chodit	k5eAaImAgMnS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
většina	většina	k1gFnSc1
členů	člen	k1gMnPc2
britské	britský	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
prestižní	prestižní	k2eAgNnSc4d1
Eton	Eton	k1gNnSc4
College	Colleg	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
studoval	studovat	k5eAaImAgMnS
zeměpis	zeměpis	k1gInSc4
<g/>
,	,	kIx,
biologii	biologie	k1gFnSc4
<g/>
,	,	kIx,
matematiku	matematika	k1gFnSc4
a	a	k8xC
dějiny	dějiny	k1gFnPc4
umění	umění	k1gNnPc2
na	na	k7c4
A-level	A-level	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
fotbalovým	fotbalový	k2eAgMnSc7d1
kapitánem	kapitán	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
většina	většina	k1gFnSc1
mladých	mladý	k2eAgMnPc2d1
Britů	Brit	k1gMnPc2
si	se	k3xPyFc3
po	po	k7c6
ukončení	ukončení	k1gNnSc6
studia	studio	k1gNnSc2
na	na	k7c6
Etonu	Eton	k1gInSc6
vzal	vzít	k5eAaPmAgMnS
rok	rok	k1gInSc4
přestávky	přestávka	k1gFnSc2
(	(	kIx(
<g/>
angl.	angl.	k?
tzv.	tzv.	kA
gap	gap	k?
year	year	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
členem	člen	k1gMnSc7
britské	britský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
cvičení	cvičení	k1gNnSc4
v	v	k7c6
Belize	Beliza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
strávil	strávit	k5eAaPmAgInS
také	také	k9
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Chile	Chile	k1gNnSc6
jako	jako	k8xS,k8xC
dobrovolník	dobrovolník	k1gMnSc1
organizace	organizace	k1gFnSc2
Releigh	Releigh	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pomáhá	pomáhat	k5eAaImIp3nS
mladým	mladý	k2eAgMnPc3d1
lidem	člověk	k1gMnPc3
najít	najít	k5eAaPmF
jejich	jejich	k3xOp3gInSc4
celý	celý	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
princ	princ	k1gMnSc1
William	William	k1gInSc4
čistí	čistit	k5eAaImIp3nS
záchody	záchod	k1gInPc4
<g/>
,	,	kIx,
obletěly	obletět	k5eAaPmAgFnP
celý	celý	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roční	roční	k2eAgFnSc6d1
pauze	pauza	k1gFnSc6
začal	začít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
navštěvovat	navštěvovat	k5eAaImF
Univerzitu	univerzita	k1gFnSc4
v	v	k7c6
St	St	kA
Andrews	Andrews	k1gInSc4
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
byla	být	k5eAaImAgNnP
jeho	jeho	k3xOp3gFnSc4
oborem	obor	k1gInSc7
historie	historie	k1gFnSc1
umění	umění	k1gNnPc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
ale	ale	k8xC
přešel	přejít	k5eAaPmAgMnS
na	na	k7c4
studium	studium	k1gNnSc4
zeměpisu	zeměpis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
dokončil	dokončit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
Master	master	k1gMnSc1
of	of	k?
Arts	Arts	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
nejvyšší	vysoký	k2eAgNnSc4d3
akademické	akademický	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
jakého	jaký	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
se	se	k3xPyFc4
kdy	kdy	k6eAd1
následníkovi	následník	k1gMnSc3
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
dostalo	dostat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
univerzitě	univerzita	k1gFnSc6
užíval	užívat	k5eAaImAgMnS
jméno	jméno	k1gNnSc4
William	William	k1gInSc1
Wales	Wales	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zaměstnání	zaměstnání	k1gNnSc1
pilota	pilot	k1gMnSc2
</s>
<s>
Princ	princ	k1gMnSc1
William	William	k1gInSc4
nejprve	nejprve	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
pilot	pilot	k1gMnSc1
záchranné	záchranný	k2eAgFnSc2d1
helikoptéry	helikoptéra	k1gFnSc2
Sea	Sea	k1gMnSc1
King	King	k1gMnSc1
u	u	k7c2
britského	britský	k2eAgNnSc2d1
Královského	královský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2015	#num#	k4
začal	začít	k5eAaPmAgMnS
létat	létat	k5eAaImF
jako	jako	k9
pilot	pilot	k1gMnSc1
vrtulníku	vrtulník	k1gInSc2
letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
East	East	k1gMnSc1
Anglian	Anglian	k1gMnSc1
Air	Air	k1gMnSc1
Ambulance	ambulance	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
povinnosti	povinnost	k1gFnPc1
</s>
<s>
V	v	k7c6
21	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
jako	jako	k9
druhý	druhý	k4xOgMnSc1
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
stal	stát	k5eAaPmAgMnS
státním	státní	k2eAgMnSc7d1
kancléřem	kancléř	k1gMnSc7
(	(	kIx(
<g/>
ten	ten	k3xDgMnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
zastupovat	zastupovat	k5eAaImF
monarchu	monarcha	k1gMnSc4
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
takto	takto	k6eAd1
sloužil	sloužit	k5eAaImAgMnS
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
královna	královna	k1gFnSc1
na	na	k7c6
státní	státní	k2eAgFnSc6d1
návštěvě	návštěva	k1gFnSc6
v	v	k7c6
Nigérii	Nigérie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2005	#num#	k4
poprvé	poprvé	k6eAd1
reprezentoval	reprezentovat	k5eAaImAgInS
královnu	královna	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jako	jako	k9
královnu	královna	k1gFnSc4
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
na	na	k7c6
oslavách	oslava	k1gFnPc6
konce	konec	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
členském	členský	k2eAgInSc6d1
státě	stát	k1gInSc6
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Princ	princ	k1gMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
května	květen	k1gInSc2
2006	#num#	k4
prezidentem	prezident	k1gMnSc7
Anglické	anglický	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
mnoho	mnoho	k4c1
jeho	jeho	k3xOp3gMnPc2
předchůdců	předchůdce	k1gMnPc2
se	se	k3xPyFc4
William	William	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
sloužit	sloužit	k5eAaImF
v	v	k7c6
britské	britský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2006	#num#	k4
graduoval	graduovat	k5eAaBmAgMnS
na	na	k7c6
Královské	královský	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
Sandhurst	Sandhurst	k1gInSc1
(	(	kIx(
<g/>
Royal	Royal	k1gInSc1
Military	Militara	k1gFnSc2
Academy	Academa	k1gFnSc2
Sandhurst	Sandhurst	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Princ	princ	k1gMnSc1
William	William	k1gInSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
britský	britský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
David	David	k1gMnSc1
Cameron	Cameron	k1gMnSc1
byli	být	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zapletení	zapletení	k1gNnPc2
do	do	k7c2
korupčního	korupční	k2eAgInSc2d1
skandálu	skandál	k1gInSc2
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
února	únor	k1gInSc2
2011	#num#	k4
je	být	k5eAaImIp3nS
plukovníkem	plukovník	k1gMnSc7
Irské	irský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
s	s	k7c7
Catherine	Catherin	k1gInSc5
Middletonovou	Middletonový	k2eAgFnSc7d1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
začala	začít	k5eAaPmAgFnS
média	médium	k1gNnSc2
probírat	probírat	k5eAaImF
princův	princův	k2eAgInSc4d1
dlouhodobý	dlouhodobý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
přítelkyní	přítelkyně	k1gFnSc7
Catherine	Catherin	k1gInSc5
Middletonovou	Middletonový	k2eAgFnSc4d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
spolužačkou	spolužačka	k1gFnSc7
z	z	k7c2
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
začal	začít	k5eAaPmAgMnS
stýkat	stýkat	k5eAaImF
v	v	k7c6
listopadu	listopad	k1gInSc6
nebo	nebo	k8xC
prosinci	prosinec	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Catherine	Catherin	k1gInSc5
nebo	nebo	k8xC
také	také	k9
Kate	kat	k1gInSc5
Middletonová	Middletonový	k2eAgFnSc1d1
navštívila	navštívit	k5eAaPmAgFnS
princovo	princův	k2eAgNnSc4d1
ukončení	ukončení	k1gNnSc4
studia	studio	k1gNnSc2
na	na	k7c6
Královské	královský	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
na	na	k7c6
nějaké	nějaký	k3yIgFnSc6
významné	významný	k2eAgFnSc6d1
akci	akce	k1gFnSc6
objevila	objevit	k5eAaPmAgFnS
jako	jako	k8xS,k8xC
host	host	k1gMnSc1
prince	princ	k1gMnSc2
Williama	William	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
média	médium	k1gNnSc2
rozšířila	rozšířit	k5eAaPmAgFnS
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
pár	pár	k4xCyI
rozešel	rozejít	k5eAaPmAgMnS
na	na	k7c6
prázdninách	prázdniny	k1gFnPc6
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
ale	ale	k9
opět	opět	k6eAd1
objevili	objevit	k5eAaPmAgMnP
spolu	spolu	k6eAd1
na	na	k7c6
večírku	večírek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
Catherine	Catherin	k1gInSc5
Middletonová	Middletonový	k2eAgFnSc1d1
zúčastnila	zúčastnit	k5eAaPmAgFnS
koncertu	koncert	k1gInSc2
pro	pro	k7c4
zemřelou	zemřelý	k2eAgFnSc4d1
princeznu	princezna	k1gFnSc4
Dianu	Diana	k1gFnSc4
na	na	k7c6
stadionu	stadion	k1gInSc6
ve	v	k7c4
Wembley	Wemblea	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
2007	#num#	k4
pak	pak	k6eAd1
média	médium	k1gNnSc2
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
princ	princ	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
přítelkyně	přítelkyně	k1gFnSc1
se	se	k3xPyFc4
usmířili	usmířit	k5eAaPmAgMnP
a	a	k8xC
obnovili	obnovit	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
vztah	vztah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
společně	společně	k6eAd1
navštívili	navštívit	k5eAaPmAgMnP
Williamova	Williamův	k2eAgMnSc4d1
otce	otec	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
ženu	žena	k1gFnSc4
Camillu	Camilla	k1gFnSc4
<g/>
,	,	kIx,
vévodkyni	vévodkyně	k1gFnSc4
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
rodina	rodina	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Svatba	svatba	k1gFnSc1
prince	princ	k1gMnSc4
Williama	William	k1gMnSc4
a	a	k8xC
Catheriny	Catherin	k1gInPc4
Middletonové	Middletonový	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
dovolené	dovolená	k1gFnSc6
v	v	k7c6
Keni	Keňa	k1gFnSc6
pár	pár	k4xCyI
zasnoubil	zasnoubit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
byl	být	k5eAaImAgInS
oznámen	oznámen	k2eAgInSc1d1
sňatek	sňatek	k1gInSc1
na	na	k7c4
pátek	pátek	k1gInSc4
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Svatba	svatba	k1gFnSc1
páru	pár	k1gInSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
podle	podle	k7c2
plánu	plán	k1gInSc2
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
a	a	k8xC
Anglikánské	anglikánský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
opatství	opatství	k1gNnSc6
Westminster	Westminstra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2012	#num#	k4
vévodský	vévodský	k2eAgInSc1d1
pár	pár	k1gInSc1
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
očekává	očekávat	k5eAaImIp3nS
narození	narození	k1gNnSc1
prvního	první	k4xOgMnSc4
potomka	potomek	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
v	v	k7c4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
se	se	k3xPyFc4
manželům	manžel	k1gMnPc3
narodil	narodit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
George	Georg	k1gFnSc2
Alexander	Alexandra	k1gFnPc2
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
obdržel	obdržet	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
rodilý	rodilý	k2eAgMnSc1d1
člen	člen	k1gMnSc1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
a	a	k8xC
budoucí	budoucí	k2eAgMnSc1d1
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
titul	titul	k1gInSc1
"	"	kIx"
<g/>
princ	princ	k1gMnSc1
<g/>
"	"	kIx"
a	a	k8xC
přídomek	přídomek	k1gInSc4
"	"	kIx"
<g/>
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
<g/>
"	"	kIx"
po	po	k7c6
svých	svůj	k3xOyFgMnPc6
rodičích	rodič	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pořadí	pořadí	k1gNnSc6
následnictví	následnictví	k1gNnSc4
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
a	a	k8xC
trůnu	trůn	k1gInSc2
některých	některý	k3yIgInPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
Commonwealthu	Commonwealth	k1gInSc2
je	být	k5eAaImIp3nS
princ	princ	k1gMnSc1
George	George	k1gNnSc1
třetí	třetí	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
návštěvě	návštěva	k1gFnSc6
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
v	v	k7c6
dubnu	duben	k1gInSc6
2014	#num#	k4
princ	princ	k1gMnSc1
William	William	k1gInSc4
naznačil	naznačit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
princ	princ	k1gMnSc1
George	Georg	k1gFnSc2
by	by	kYmCp3nS
brzy	brzy	k6eAd1
mohl	moct	k5eAaImAgMnS
mít	mít	k5eAaImF
sourozence	sourozenec	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2014	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
Clarence	Clarence	k1gFnSc2
House	house	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
vévodkyně	vévodkyně	k1gFnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
čeká	čekat	k5eAaImIp3nS
druhého	druhý	k4xOgMnSc4
potomka	potomek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
se	se	k3xPyFc4
2015	#num#	k4
v	v	k7c4
8	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
se	se	k3xPyFc4
se	se	k3xPyFc4
manželům	manžel	k1gMnPc3
narodila	narodit	k5eAaPmAgFnS
holčička	holčička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
se	se	k3xPyFc4
Charlotte	Charlott	k1gInSc5
Elizabeth	Elizabeth	k1gFnSc7
Diana	Diana	k1gFnSc1
a	a	k8xC
v	v	k7c6
pořadí	pořadí	k1gNnSc6
následnictví	následnictví	k1gNnSc4
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
je	být	k5eAaImIp3nS
čtvrtá	čtvrtá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2017	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
vévodkyně	vévodkyně	k1gFnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
čeká	čekat	k5eAaImIp3nS
třetího	třetí	k4xOgMnSc4
potomka	potomek	k1gMnSc4
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
vévodkyně	vévodkyně	k1gFnSc1
Catherine	Catherin	k1gInSc5
porodila	porodit	k5eAaPmAgFnS
v	v	k7c6
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
hodin	hodina	k1gFnPc2
chlapce	chlapec	k1gMnPc4
vážícího	vážící	k2eAgInSc2d1
3,82	3,82	k4
kilogramu	kilogram	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlapec	chlapec	k1gMnSc1
ponese	ponést	k5eAaPmIp3nS,k5eAaImIp3nS
titul	titul	k1gInSc4
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
výsost	výsost	k1gFnSc1
princ	princa	k1gFnPc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
tři	tři	k4xCgNnPc4
křestní	křestní	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
byla	být	k5eAaImAgFnS
oznámena	oznámen	k2eAgFnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
a	a	k8xC
zní	znět	k5eAaImIp3nS
Louis	Louis	k1gMnSc1
Arthur	Arthur	k1gMnSc1
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
jména	jméno	k1gNnPc1
</s>
<s>
Princ	princ	k1gMnSc1
William	William	k1gInSc4
hraje	hrát	k5eAaImIp3nS
pólo	pólo	k1gNnSc1
</s>
<s>
Tituly	titul	k1gInPc1
</s>
<s>
od	od	k7c2
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1982	#num#	k4
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
–	–	k?
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
William	William	k1gInSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
od	od	k7c2
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
–	–	k?
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příjmení	příjmení	k1gNnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
všichni	všechen	k3xTgMnPc1
potomci	potomek	k1gMnPc1
královny	královna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
prince	princ	k1gMnSc2
Phillipa	Phillip	k1gMnSc2
užívají	užívat	k5eAaImIp3nP
příjmení	příjmení	k1gNnSc4
Mountbatten-Windsor	Mountbatten-Windsora	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
kombinuje	kombinovat	k5eAaImIp3nS
příjmení	příjmení	k1gNnSc1
obou	dva	k4xCgMnPc2
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
královské	královský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
ale	ale	k8xC
William	William	k1gInSc1
užíval	užívat	k5eAaImAgInS
během	během	k7c2
studia	studio	k1gNnSc2
příjmení	příjmení	k1gNnSc2
"	"	kIx"
<g/>
Wales	Wales	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
"	"	kIx"
<g/>
Mountbatten-Windsor	Mountbatten-Windsor	k1gInSc1
<g/>
"	"	kIx"
používají	používat	k5eAaImIp3nP
jenom	jenom	k9
potomci	potomek	k1gMnPc1
bez	bez	k7c2
přímého	přímý	k2eAgInSc2d1
nároku	nárok	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
stanovila	stanovit	k5eAaPmAgFnS
v	v	k7c6
tzv.	tzv.	kA
"	"	kIx"
<g/>
Letters	Letters	k1gInSc1
Patent	patent	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Budoucí	budoucí	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
stane	stanout	k5eAaPmIp3nS
králem	král	k1gMnSc7
<g/>
,	,	kIx,
William	William	k1gInSc1
automaticky	automaticky	k6eAd1
zdědí	zdědit	k5eAaPmIp3nS
tituly	titul	k1gInPc4
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Rothesay	Rothesaa	k1gFnSc2
a	a	k8xC
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
v	v	k7c6
tom	ten	k3xDgInSc6
případě	případ	k1gInSc6
bude	být	k5eAaImBp3nS
udělen	udělit	k5eAaPmNgInS
i	i	k9
titul	titul	k1gInSc4
princ	princa	k1gFnPc2
z	z	k7c2
Walesu	Wales	k1gInSc2
jako	jako	k8xC,k8xS
prvnímu	první	k4xOgMnSc3
následníkovi	následník	k1gMnSc3
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
automatické	automatický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
se	se	k3xPyFc4
už	už	k6eAd1
spekuluje	spekulovat	k5eAaImIp3nS
o	o	k7c6
označení	označení	k1gNnSc6
"	"	kIx"
<g/>
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
podle	podle	k7c2
některých	některý	k3yIgInPc2
názorů	názor	k1gInPc2
patří	patřit	k5eAaImIp3nP
zároveň	zároveň	k6eAd1
otci	otec	k1gMnSc3
Charlesovi	Charles	k1gMnSc3
a	a	k8xC
synovi	syn	k1gMnSc3
Williamovi	William	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
se	se	k3xPyFc4
dosud	dosud	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
věci	věc	k1gFnSc6
údajně	údajně	k6eAd1
nevyjádřila	vyjádřit	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jestliže	jestliže	k8xS
se	se	k3xPyFc4
William	William	k1gInSc1
jako	jako	k8xC,k8xS
budoucí	budoucí	k2eAgMnSc1d1
král	král	k1gMnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
vzít	vzít	k5eAaPmF
si	se	k3xPyFc3
jako	jako	k8xC,k8xS
královské	královský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
své	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
vládnout	vládnout	k5eAaImF
jako	jako	k9
král	král	k1gMnSc1
Vilém	Vilém	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
William	William	k1gInSc1
V	V	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Prince	princ	k1gMnSc2
William	William	k1gInSc1
of	of	k?
Wales	Wales	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
má	mít	k5eAaImIp3nS
článek	článek	k1gInSc1
název	název	k1gInSc4
Prince	princa	k1gFnSc3
William	William	k1gInSc1
<g/>
,	,	kIx,
Duke	Duke	k1gInSc1
of	of	k?
Cambridge	Cambridge	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.officialroyalwedding2011.org	www.officialroyalwedding2011.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
30	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princ	princa	k1gFnPc2
William	William	k1gInSc4
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
jako	jako	k9
pilot	pilot	k1gMnSc1
letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
i	i	k9
noční	noční	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Princ	princ	k1gMnSc1
William	William	k1gInSc4
skončí	skončit	k5eAaPmIp3nS
jako	jako	k9
pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
čeká	čekat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
charita	charita	k1gFnSc1
a	a	k8xC
královské	královský	k2eAgFnPc1d1
povinnosti	povinnost	k1gFnPc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Skandál	skandál	k1gInSc1
FIFA	FIFA	kA
zasáhl	zasáhnout	k5eAaPmAgInS
i	i	k8xC
prince	princ	k1gMnSc2
Williama	William	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jsem	být	k5eAaImIp1nS
moc	moc	k6eAd1
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Kate	kat	k1gInSc5
řekla	říct	k5eAaPmAgFnS
ano	ano	k9
<g/>
,	,	kIx,
svěřil	svěřit	k5eAaPmAgMnS
se	se	k3xPyFc4
princ	princ	k1gMnSc1
William	William	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Prince	princ	k1gMnSc4
William	William	k1gInSc1
engaged	engaged	k1gInSc1
to	ten	k3xDgNnSc1
Kate	kat	k1gInSc5
Middleton	Middleton	k1gInSc1
<g/>
,	,	kIx,
Mirror	Mirror	k1gInSc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
www.mirror.co.uk	www.mirror.co.uk	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
20	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
William	William	k1gInSc1
a	a	k8xC
Kate	kat	k1gInSc5
si	se	k3xPyFc3
řekli	říct	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
"	"	kIx"
<g/>
ano	ano	k9
<g/>
"	"	kIx"
<g/>
,	,	kIx,
den	den	k1gInSc4
zakončili	zakončit	k5eAaPmAgMnP
slavnostní	slavnostní	k2eAgMnPc1d1
večeří	večeře	k1gFnSc7
a	a	k8xC
tancem	tanec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ON-LINE	ON-LINE	k1gMnPc1
<g/>
:	:	kIx,
Novomanželé	novomanžel	k1gMnPc1
William	William	k1gInSc4
a	a	k8xC
Kate	kat	k1gInSc5
oslavují	oslavovat	k5eAaImIp3nP
v	v	k7c6
královském	královský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
William	William	k1gInSc1
a	a	k8xC
Kate	kat	k1gInSc5
si	se	k3xPyFc3
řekli	říct	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
"	"	kIx"
<g/>
ano	ano	k9
<g/>
"	"	kIx"
<g/>
,	,	kIx,
den	den	k1gInSc4
zakončili	zakončit	k5eAaPmAgMnP
slavnostní	slavnostní	k2eAgMnPc1d1
večeří	večeře	k1gFnSc7
a	a	k8xC
tancem	tanec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Princ	princ	k1gMnSc1
William	William	k1gInSc1
a	a	k8xC
Kate	kat	k1gInSc5
očekávají	očekávat	k5eAaImIp3nP
potomka	potomek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
George	Georg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
sourozence	sourozenec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metro	metro	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
2014	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
71	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
7811	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://zahranicni.eurozpravy.cz/evropa/26820-kralovna-udelila-williamovi-a-kate-titul-vevoda-a-vevodkyne-z-cambridge/	http://zahranicni.eurozpravy.cz/evropa/26820-kralovna-udelila-williamovi-a-kate-titul-vevoda-a-vevodkyne-z-cambridge/	k4
<g/>
↑	↑	k?
Sledovali	sledovat	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
ONLINE	ONLINE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Londýně	Londýn	k1gInSc6
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
William	William	k1gInSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Britové	Brit	k1gMnPc1
připravují	připravovat	k5eAaImIp3nP
novelu	novela	k1gFnSc4
zákona	zákon	k1gInSc2
o	o	k7c4
nástupnictví	nástupnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
prospěch	prospěch	k1gInSc4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Princ	princ	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
umí	umět	k5eAaImIp3nS
vařit	vařit	k5eAaImF
a	a	k8xC
nejstarší	starý	k2eAgFnSc1d3
královská	královský	k2eAgFnSc1d1
nevěsta	nevěsta	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-29	2011-04-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.ct24.cz/svet/evropa/kralovska-svatba/kdo-je-kdo-v-kralovske-rodine/121736-princ-charles-je-rekordman-v-cekani-na-trun/	http://www.ct24.cz/svet/evropa/kralovska-svatba/kdo-je-kdo-v-kralovske-rodine/121736-princ-charles-je-rekordman-v-cekani-na-trun/	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000701970	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
120920883	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
6764	#num#	k4
8998	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83035580	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
232263808	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83035580	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
