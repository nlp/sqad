<p>
<s>
Džamal	Džamal	k1gMnSc1	Džamal
Zahalka	Zahalka	k1gFnSc1	Zahalka
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ג	ג	k?	ג
<g/>
'	'	kIx"	'
<g/>
מ	מ	k?	מ
ז	ז	k?	ז
<g/>
,	,	kIx,	,
Džamal	Džamal	k1gMnSc1	Džamal
Zachalka	Zachalka	k1gFnSc1	Zachalka
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
ج	ج	k?	ج
ز	ز	k?	ز
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1955	[number]	k4	1955
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
arabské	arabský	k2eAgFnSc2d1	arabská
národnosti	národnost	k1gFnSc2	národnost
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Knesetu	Kneset	k1gInSc2	Kneset
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Balad	balada	k1gFnPc2	balada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kafr	Kafr	k1gMnSc1	Kafr
Kara	kara	k1gFnSc1	kara
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
univerzitě	univerzita	k1gFnSc6	univerzita
obor	obora	k1gFnPc2	obora
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc1	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
arabsky	arabsky	k6eAd1	arabsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Knesetu	Kneset	k1gInSc2	Kneset
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Balad	balada	k1gFnPc2	balada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003-2006	[number]	k4	2003-2006
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
výboru	výbor	k1gInSc2	výbor
petičního	petiční	k2eAgInSc2d1	petiční
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
technologie	technologie	k1gFnSc2	technologie
a	a	k8xC	a
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc4d1	sociální
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
finančního	finanční	k2eAgInSc2d1	finanční
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
status	status	k1gInSc4	status
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
technologie	technologie	k1gFnSc2	technologie
a	a	k8xC	a
petičního	petiční	k2eAgInSc2d1	petiční
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Knesetu	Kneset	k1gInSc6	Kneset
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
drogy	droga	k1gFnPc4	droga
a	a	k8xC	a
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
trvale	trvale	k6eAd1	trvale
předsedá	předsedat	k5eAaImIp3nS	předsedat
poslaneckému	poslanecký	k2eAgInSc3d1	poslanecký
klubu	klub	k1gInSc3	klub
strany	strana	k1gFnSc2	strana
Balad	balada	k1gFnPc2	balada
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
za	za	k7c4	za
alianci	aliance	k1gFnSc4	aliance
arabských	arabský	k2eAgFnPc2d1	arabská
menšinových	menšinový	k2eAgFnPc2d1	menšinová
stran	strana	k1gFnPc2	strana
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
kandidátka	kandidátka	k1gFnSc1	kandidátka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Džamal	Džamal	k1gInSc4	Džamal
Zahalka	Zahalka	k1gFnSc1	Zahalka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kneset	Kneset	k1gInSc1	Kneset
–	–	k?	–
Džamal	Džamal	k1gInSc1	Džamal
Zahalka	Zahalka	k1gFnSc1	Zahalka
</s>
</p>
