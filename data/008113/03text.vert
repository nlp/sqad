<s>
KFC	KFC	kA	KFC
(	(	kIx(	(
<g/>
Kentucky	Kentucka	k1gFnSc2	Kentucka
Fried	Fried	k1gInSc1	Fried
Chicken	Chicken	k1gInSc1	Chicken
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americkou	americký	k2eAgFnSc7d1	americká
nadnárodní	nadnárodní	k2eAgFnSc7d1	nadnárodní
korporací	korporace	k1gFnSc7	korporace
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgNnPc4d1	zabývající
se	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
smaženým	smažený	k2eAgNnSc7d1	smažené
kuřecím	kuřecí	k2eAgNnSc7d1	kuřecí
masem	maso	k1gNnSc7	maso
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
American	Americana	k1gFnPc2	Americana
Restaurants	Restaurantsa	k1gFnPc2	Restaurantsa
<g/>
.	.	kIx.	.
</s>
<s>
Harland	Harland	k1gInSc1	Harland
Sanders	Sanders	k1gInSc1	Sanders
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
svá	svůj	k3xOyFgNnPc4	svůj
smažená	smažený	k2eAgNnPc4d1	smažené
kuřata	kuře	k1gNnPc4	kuře
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
v	v	k7c6	v
jídelně	jídelna	k1gFnSc6	jídelna
Sanders	Sandersa	k1gFnPc2	Sandersa
Court	Courta	k1gFnPc2	Courta
&	&	k?	&
Café	café	k1gNnSc6	café
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gFnSc2	jeho
čerpací	čerpací	k2eAgFnSc2d1	čerpací
stanice	stanice	k1gFnSc2	stanice
v	v	k7c4	v
Corbinu	Corbina	k1gFnSc4	Corbina
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Kentucky	Kentucka	k1gFnSc2	Kentucka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
a	a	k8xC	a
proto	proto	k8xC	proto
mu	on	k3xPp3gNnSc3	on
kentucký	kentucký	k1gMnSc1	kentucký
guvernér	guvernér	k1gMnSc1	guvernér
udělil	udělit	k5eAaPmAgMnS	udělit
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
Kentucky	Kentucka	k1gFnSc2	Kentucka
Colonel	Colonela	k1gFnPc2	Colonela
jako	jako	k8xS	jako
uznání	uznání	k1gNnSc2	uznání
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
kentucké	kentucké	k2eAgFnSc4d1	kentucké
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
restauraci	restaurace	k1gFnSc4	restaurace
na	na	k7c4	na
142	[number]	k4	142
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
a	a	k8xC	a
také	také	k9	také
koupil	koupit	k5eAaPmAgMnS	koupit
motel	motel	k1gInSc4	motel
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
trvala	trvat	k5eAaImAgFnS	trvat
příprava	příprava	k1gFnSc1	příprava
kuřete	kuře	k1gNnSc2	kuře
na	na	k7c6	na
ocelové	ocelový	k2eAgFnSc6d1	ocelová
pánvi	pánev	k1gFnSc6	pánev
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
restauraci	restaurace	k1gFnSc4	restaurace
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c6	na
smažení	smažení	k1gNnSc6	smažení
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
přípravu	příprava	k1gFnSc4	příprava
kuřete	kuře	k1gNnSc2	kuře
zkrátit	zkrátit	k5eAaPmF	zkrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
směs	směs	k1gFnSc4	směs
11	[number]	k4	11
druhů	druh	k1gInPc2	druh
bylinek	bylinka	k1gFnPc2	bylinka
a	a	k8xC	a
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Original	Original	k1gFnSc4	Original
Recipe	recipe	k1gNnSc2	recipe
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
Sanders	Sanders	k1gInSc1	Sanders
Court	Courta	k1gFnPc2	Courta
&	&	k?	&
Café	café	k1gNnSc1	café
sloužilo	sloužit	k5eAaImAgNnS	sloužit
především	především	k6eAd1	především
cestujícím	cestující	k1gMnPc3	cestující
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
projížděli	projíždět	k5eAaImAgMnP	projíždět
městem	město	k1gNnSc7	město
Corbin	Corbin	k2eAgInSc1d1	Corbin
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Floridu	Florida	k1gFnSc4	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
plánovat	plánovat	k5eAaImF	plánovat
obchvat	obchvat	k1gInSc4	obchvat
Corbinu	Corbina	k1gFnSc4	Corbina
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
Sanders	Sanders	k1gInSc1	Sanders
podnik	podnik	k1gInSc4	podnik
prodat	prodat	k5eAaPmF	prodat
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
svůj	svůj	k3xOyFgInSc4	svůj
recept	recept	k1gInSc4	recept
a	a	k8xC	a
výrobní	výrobní	k2eAgInSc4d1	výrobní
postup	postup	k1gInSc4	postup
nabízet	nabízet	k5eAaImF	nabízet
restauracím	restaurace	k1gFnPc3	restaurace
napříč	napříč	k7c7	napříč
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jeho	jeho	k3xOp3gFnSc3	jeho
postup	postup	k1gInSc1	postup
použil	použít	k5eAaPmAgInS	použít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Pete	Pete	k1gInSc1	Pete
Harman	Harman	k1gMnSc1	Harman
v	v	k7c4	v
South	South	k1gInSc4	South
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnSc7	Lake
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
<g/>
;	;	kIx,	;
společně	společně	k6eAd1	společně
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
první	první	k4xOgFnSc4	první
provozovnu	provozovna	k1gFnSc4	provozovna
Kentucky	Kentucka	k1gFnSc2	Kentucka
Fried	Fried	k1gInSc1	Fried
Chicken	Chicken	k2eAgInSc1d1	Chicken
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
už	už	k6eAd1	už
prodal	prodat	k5eAaPmAgInS	prodat
přes	přes	k7c4	přes
600	[number]	k4	600
licenci	licence	k1gFnSc4	licence
ve	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
už	už	k6eAd1	už
fungovalo	fungovat	k5eAaImAgNnS	fungovat
638	[number]	k4	638
restaurací	restaurace	k1gFnPc2	restaurace
KFC	KFC	kA	KFC
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
3	[number]	k4	3
500	[number]	k4	500
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
restaurace	restaurace	k1gFnSc2	restaurace
KFC	KFC	kA	KFC
nabízejí	nabízet	k5eAaImIp3nP	nabízet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
různých	různý	k2eAgInPc2d1	různý
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
od	od	k7c2	od
Chicken	Chickna	k1gFnPc2	Chickna
Pita	pit	k2eAgFnSc1d1	pita
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
přes	přes	k7c4	přes
Lososový	lososový	k2eAgInSc4d1	lososový
sendvič	sendvič	k1gInSc4	sendvič
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
až	až	k9	až
po	po	k7c4	po
Mean	Mean	k1gNnSc4	Mean
Griens	Griensa	k1gFnPc2	Griensa
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
nabídku	nabídka	k1gFnSc4	nabídka
tvoří	tvořit	k5eAaImIp3nP	tvořit
kousky	kousek	k1gInPc7	kousek
kuřete	kuře	k1gNnSc2	kuře
<g/>
,	,	kIx,	,
připravované	připravovaný	k2eAgNnSc1d1	připravované
podle	podle	k7c2	podle
tajného	tajný	k2eAgInSc2d1	tajný
receptu	recept	k1gInSc2	recept
Colonela	Colonela	k1gFnSc1	Colonela
Sanderse	Sanderse	k1gFnSc1	Sanderse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
81	[number]	k4	81
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
27	[number]	k4	27
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
5	[number]	k4	5
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
7	[number]	k4	7
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
2	[number]	k4	2
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
6	[number]	k4	6
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
(	(	kIx(	(
<g/>
1	[number]	k4	1
s	s	k7c7	s
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
4	[number]	k4	4
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
1	[number]	k4	1
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
3	[number]	k4	3
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
(	(	kIx(	(
<g/>
1	[number]	k4	1
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
3	[number]	k4	3
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
Šantovka	Šantovka	k1gFnSc1	Šantovka
<g/>
,	,	kIx,	,
OC	OC	kA	OC
Haná	Haná	k1gFnSc1	Haná
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
3	[number]	k4	3
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
1	[number]	k4	1
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
(	(	kIx(	(
<g/>
1	[number]	k4	1
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2	[number]	k4	2
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
(	(	kIx(	(
<g/>
1	[number]	k4	1
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2	[number]	k4	2
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
(	(	kIx(	(
<g/>
1	[number]	k4	1
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1	[number]	k4	1
v	v	k7c6	v
Chomutově	Chomutov	k1gInSc6	Chomutov
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
1	[number]	k4	1
v	v	k7c6	v
Mostě	most	k1gInSc6	most
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
1	[number]	k4	1
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
(	(	kIx(	(
<g/>
Galerie	galerie	k1gFnSc1	galerie
Přerov	Přerov	k1gInSc1	Přerov
<g/>
)	)	kIx)	)
Po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
,	,	kIx,	,
Králově	Králův	k2eAgInSc6d1	Králův
Dvoře	Dvůr	k1gInSc6	Dvůr
u	u	k7c2	u
Berouna	Beroun	k1gInSc2	Beroun
(	(	kIx(	(
<g/>
odpočívadlo	odpočívadlo	k1gNnSc1	odpočívadlo
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
na	na	k7c6	na
hraničním	hraniční	k2eAgInSc6d1	hraniční
přechodu	přechod	k1gInSc6	přechod
Hatě	hať	k1gFnSc2	hať
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
,	,	kIx,	,
Jablonci	Jablonec	k1gInSc6	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
restaurací	restaurace	k1gFnPc2	restaurace
u	u	k7c2	u
významných	významný	k2eAgInPc2d1	významný
silničních	silniční	k2eAgInPc2d1	silniční
tahů	tah	k1gInPc2	tah
nebo	nebo	k8xC	nebo
u	u	k7c2	u
frekventovaných	frekventovaný	k2eAgFnPc2d1	frekventovaná
ulic	ulice	k1gFnPc2	ulice
ve	v	k7c6	v
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
okénkem	okénko	k1gNnSc7	okénko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
pohodlně	pohodlně	k6eAd1	pohodlně
objednat	objednat	k5eAaPmF	objednat
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
opustit	opustit	k5eAaPmF	opustit
svůj	svůj	k3xOyFgInSc4	svůj
vůz	vůz	k1gInSc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
restaurací	restaurace	k1gFnPc2	restaurace
KFC	KFC	kA	KFC
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
pobočka	pobočka	k1gFnSc1	pobočka
polské	polský	k2eAgFnSc2d1	polská
společnosti	společnost	k1gFnSc2	společnost
Amrest	Amrest	k1gMnSc1	Amrest
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
tuzemsku	tuzemsko	k1gNnSc6	tuzemsko
provozuje	provozovat	k5eAaImIp3nS	provozovat
také	také	k9	také
restaurace	restaurace	k1gFnSc1	restaurace
řetězce	řetězec	k1gInSc2	řetězec
Burger	Burgra	k1gFnPc2	Burgra
King	Kinga	k1gFnPc2	Kinga
a	a	k8xC	a
kavárny	kavárna	k1gFnSc2	kavárna
Starbucks	Starbucksa	k1gFnPc2	Starbucksa
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
v	v	k7c6	v
KFC	KFC	kA	KFC
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
prodá	prodat	k5eAaPmIp3nS	prodat
914	[number]	k4	914
miliónů	milión	k4xCgInPc2	milión
kousků	kousek	k1gInPc2	kousek
kuřat	kuře	k1gNnPc2	kuře
<g/>
.	.	kIx.	.
</s>
<s>
KFC	KFC	kA	KFC
otevírá	otevírat	k5eAaImIp3nS	otevírat
na	na	k7c6	na
světě	svět	k1gInSc6	svět
průměrně	průměrně	k6eAd1	průměrně
jednu	jeden	k4xCgFnSc4	jeden
restauraci	restaurace	k1gFnSc4	restaurace
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
extra	extra	k2eAgNnSc1d1	extra
křupavé	křupavý	k2eAgNnSc1d1	křupavé
kuřecí	kuřecí	k2eAgNnSc1d1	kuřecí
křídlo	křídlo	k1gNnSc1	křídlo
má	mít	k5eAaImIp3nS	mít
510	[number]	k4	510
kalorií	kalorie	k1gFnPc2	kalorie
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
33	[number]	k4	33
gramů	gram	k1gInPc2	gram
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
americká	americký	k2eAgFnSc1d1	americká
Federal	Federal	k1gFnSc1	Federal
Trade	Trad	k1gInSc5	Trad
Commission	Commission	k1gInSc4	Commission
označila	označit	k5eAaPmAgFnS	označit
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
kampaň	kampaň	k1gFnSc1	kampaň
KFC	KFC	kA	KFC
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
tehdy	tehdy	k6eAd1	tehdy
vydávala	vydávat	k5eAaImAgFnS	vydávat
své	svůj	k3xOyFgInPc4	svůj
výrobky	výrobek	k1gInPc4	výrobek
za	za	k7c4	za
zdravou	zdravý	k2eAgFnSc4d1	zdravá
výživu	výživa	k1gFnSc4	výživa
s	s	k7c7	s
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
jezte	jíst	k5eAaImRp2nP	jíst
lépe	dobře	k6eAd2	dobře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
za	za	k7c4	za
lživou	lživý	k2eAgFnSc4d1	lživá
reklamu	reklama	k1gFnSc4	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
People	People	k1gFnPc2	People
for	forum	k1gNnPc2	forum
the	the	k?	the
Ethical	Ethical	k1gMnSc1	Ethical
Treatment	Treatment	k1gMnSc1	Treatment
of	of	k?	of
Animals	Animals	k1gInSc1	Animals
(	(	kIx(	(
<g/>
PETA	PETA	kA	PETA
<g/>
,	,	kIx,	,
Lidé	člověk	k1gMnPc1	člověk
za	za	k7c4	za
etické	etický	k2eAgNnSc4d1	etické
zacházení	zacházení	k1gNnSc4	zacházení
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
)	)	kIx)	)
KFC	KFC	kA	KFC
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
za	za	k7c4	za
odběr	odběr	k1gInSc4	odběr
kuřat	kuře	k1gNnPc2	kuře
od	od	k7c2	od
dodavatelů	dodavatel	k1gMnPc2	dodavatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gFnPc4	on
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
nepřijatelných	přijatelný	k2eNgFnPc6d1	nepřijatelná
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
dopouštějí	dopouštět	k5eAaImIp3nP	dopouštět
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc2	jejich
týrání	týrání	k1gNnSc2	týrání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
aktivistické	aktivistický	k2eAgFnSc2d1	aktivistická
skupiny	skupina	k1gFnSc2	skupina
Dogwood	Dogwooda	k1gFnPc2	Dogwooda
Alliance	Alliance	k1gFnSc2	Alliance
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc1	dřevo
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
produkty	produkt	k1gInPc4	produkt
KFC	KFC	kA	KFC
<g/>
,	,	kIx,	,
těženo	těžen	k2eAgNnSc1d1	těženo
z	z	k7c2	z
panenských	panenský	k2eAgInPc2d1	panenský
lesů	les	k1gInPc2	les
jižních	jižní	k2eAgFnPc2d1	jižní
oblastí	oblast	k1gFnPc2	oblast
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
stylem	styl	k1gInSc7	styl
velkoplošné	velkoplošný	k2eAgFnPc4d1	velkoplošná
deforestace	deforestace	k1gFnPc4	deforestace
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
prospektu	prospekt	k1gInSc2	prospekt
pozdější	pozdní	k2eAgFnSc2d2	pozdější
obnovy	obnova	k1gFnSc2	obnova
nebo	nebo	k8xC	nebo
konverze	konverze	k1gFnSc2	konverze
biodiverzních	biodiverzní	k2eAgInPc2d1	biodiverzní
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
v	v	k7c4	v
monokulturní	monokulturní	k2eAgFnPc4d1	monokulturní
"	"	kIx"	"
<g/>
plantáže	plantáž	k1gFnPc4	plantáž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
KFC	KFC	kA	KFC
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
KFC	KFC	kA	KFC
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Kentucky	Kentucka	k1gFnSc2	Kentucka
Fried	Fried	k1gInSc4	Fried
Cruelty	Cruelt	k1gInPc4	Cruelt
a	a	k8xC	a
Kentucky	Kentuck	k1gInPc4	Kentuck
Fried	Fried	k1gMnSc1	Fried
Forrests	Forrests	k1gInSc4	Forrests
<g/>
,	,	kIx,	,
stránky	stránka	k1gFnPc4	stránka
kritizující	kritizující	k2eAgFnSc2d1	kritizující
praktiky	praktika	k1gFnSc2	praktika
KFC	KFC	kA	KFC
</s>
