<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
automobilový	automobilový	k2eAgInSc4d1	automobilový
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
jel	jet	k5eAaImAgMnS	jet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
American	American	k1gMnSc1	American
Grand	grand	k1gMnSc1	grand
Prize	Prize	k1gFnSc1	Prize
<g/>
.	.	kIx.	.
</s>
