<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
automobilový	automobilový	k2eAgInSc4d1	automobilový
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
jel	jet	k5eAaImAgMnS	jet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
American	American	k1gMnSc1	American
Grand	grand	k1gMnSc1	grand
Prize	Prize	k1gFnSc1	Prize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
kalendáře	kalendář	k1gInSc2	kalendář
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
závod	závod	k1gInSc1	závod
koná	konat	k5eAaImIp3nS	konat
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Circuit	Circuita	k1gFnPc2	Circuita
of	of	k?	of
the	the	k?	the
Americas	Americas	k1gMnSc1	Americas
v	v	k7c6	v
Texaském	texaský	k2eAgInSc6d1	texaský
Austinu	Austin	k1gInSc6	Austin
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
šestém	šestý	k4xOgInSc6	šestý
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hostí	hostit	k5eAaImIp3nS	hostit
závody	závod	k1gInPc4	závod
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
a	a	k8xC	a
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
okruhu	okruh	k1gInSc6	okruh
celkem	celkem	k6eAd1	celkem
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Indianopolis	Indianopolis	k1gFnSc2	Indianopolis
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgNnSc1d1	Červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
představuje	představovat	k5eAaImIp3nS	představovat
závody	závod	k1gInPc7	závod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgFnP	být
součástí	součást	k1gFnSc7	součást
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Červeně	červeně	k6eAd1	červeně
podbarvení	podbarvení	k1gNnSc1	podbarvení
představuje	představovat	k5eAaImIp3nS	představovat
závody	závod	k1gInPc7	závod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgFnP	být
součástí	součást	k1gFnSc7	součást
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
