<s>
Zuby	zub	k1gInPc1	zub
moudrosti	moudrost	k1gFnSc2	moudrost
jsou	být	k5eAaImIp3nP	být
třetí	třetí	k4xOgFnPc1	třetí
stoličky	stolička	k1gFnPc1	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
prořezávají	prořezávat	k5eAaImIp3nP	prořezávat
až	až	k9	až
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
současného	současný	k2eAgMnSc2d1	současný
člověka	člověk	k1gMnSc2	člověk
pozbyly	pozbýt	k5eAaPmAgFnP	pozbýt
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měkká	měkký	k2eAgFnSc1d1	měkká
potrava	potrava	k1gFnSc1	potrava
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
takové	takový	k3xDgNnSc4	takový
mechanické	mechanický	k2eAgNnSc4d1	mechanické
zpracování	zpracování	k1gNnSc4	zpracování
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
daleké	daleký	k2eAgFnSc6d1	daleká
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zkrácení	zkrácení	k1gNnSc3	zkrácení
čelistí	čelist	k1gFnPc2	čelist
a	a	k8xC	a
třetí	třetí	k4xOgFnPc1	třetí
stoličky	stolička	k1gFnPc1	stolička
často	často	k6eAd1	často
nemají	mít	k5eNaImIp3nP	mít
dostatek	dostatek	k1gInSc1	dostatek
místa	místo	k1gNnSc2	místo
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
prořezaly	prořezat	k5eAaPmAgFnP	prořezat
a	a	k8xC	a
správně	správně	k6eAd1	správně
zařadily	zařadit	k5eAaPmAgInP	zařadit
do	do	k7c2	do
zubního	zubní	k2eAgInSc2d1	zubní
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
někoho	někdo	k3yInSc2	někdo
se	se	k3xPyFc4	se
některá	některý	k3yIgFnSc1	některý
třetí	třetí	k4xOgFnSc1	třetí
stolička	stolička	k1gFnSc1	stolička
neprořeže	prořezat	k5eNaPmIp3nS	prořezat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
třetí	třetí	k4xOgFnSc1	třetí
stolička	stolička	k1gFnSc1	stolička
prořezává	prořezávat	k5eAaImIp3nS	prořezávat
šikmo	šikmo	k6eAd1	šikmo
nebo	nebo	k8xC	nebo
vodorovně	vodorovně	k6eAd1	vodorovně
<g/>
;	;	kIx,	;
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vhodné	vhodný	k2eAgNnSc1d1	vhodné
její	její	k3xOp3gNnSc4	její
chirurgické	chirurgický	k2eAgNnSc4d1	chirurgické
odstranění	odstranění	k1gNnSc4	odstranění
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
prořezáváním	prořezávání	k1gNnSc7	prořezávání
zvláště	zvláště	k6eAd1	zvláště
dolních	dolní	k2eAgInPc2d1	dolní
zubů	zub	k1gInPc2	zub
moudrosti	moudrost	k1gFnSc2	moudrost
trpí	trpět	k5eAaImIp3nS	trpět
až	až	k6eAd1	až
každý	každý	k3xTgMnSc1	každý
pátý	pátý	k4xOgMnSc1	pátý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnPc1	třetí
stoličky	stolička	k1gFnPc1	stolička
jsou	být	k5eAaImIp3nP	být
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
monofyodontního	monofyodontní	k2eAgInSc2d1	monofyodontní
zubu	zub	k1gInSc2	zub
<g/>
:	:	kIx,	:
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc4	dva
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
zubu	zub	k1gInSc2	zub
moudrosti	moudrost	k1gFnSc2	moudrost
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
pojmenováním	pojmenování	k1gNnSc7	pojmenování
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
wisdom	wisdom	k1gInSc1	wisdom
teeth	teetha	k1gFnPc2	teetha
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Weisheitszahn	Weisheitszahn	k1gInSc1	Weisheitszahn
<g/>
,	,	kIx,	,
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
:	:	kIx,	:
verstandskies	verstandskies	k1gInSc1	verstandskies
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
dents	dents	k6eAd1	dents
de	de	k?	de
sagesse	sagesse	k1gFnSc1	sagesse
<g/>
,	,	kIx,	,
srbsky	srbsky	k6eAd1	srbsky
<g/>
/	/	kIx~	/
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
/	/	kIx~	/
<g/>
bosensky	bosensky	k6eAd1	bosensky
<g/>
:	:	kIx,	:
umnjaci	umnjace	k1gFnSc3	umnjace
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
moudrosti	moudrost	k1gFnSc2	moudrost
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
doprovázeny	doprovázen	k2eAgInPc4d1	doprovázen
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnSc2	jejich
retence	retence	k1gFnSc2	retence
<g/>
,	,	kIx,	,
zánětu	zánět	k1gInSc3	zánět
dásně	dáseň	k1gFnSc2	dáseň
a	a	k8xC	a
zubních	zubní	k2eAgFnPc2d1	zubní
cyst	cysta	k1gFnPc2	cysta
<g/>
.	.	kIx.	.
</s>
<s>
Retence	retence	k1gFnSc1	retence
zubů	zub	k1gInPc2	zub
–	–	k?	–
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zub	zub	k1gInSc1	zub
nemůže	moct	k5eNaImIp3nS	moct
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
prořezat	prořezat	k5eAaPmF	prořezat
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
zub	zub	k1gInSc4	zub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
zubů	zub	k1gInPc2	zub
moudrosti	moudrost	k1gFnSc2	moudrost
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgNnSc1d3	nejčastější
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zánětlivých	zánětlivý	k2eAgFnPc6d1	zánětlivá
komplikacích	komplikace	k1gFnPc6	komplikace
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
obtížném	obtížný	k2eAgNnSc6d1	obtížné
prořezávání	prořezávání	k1gNnSc6	prořezávání
zubů	zub	k1gInPc2	zub
(	(	kIx(	(
<g/>
Dentitio	Dentitio	k1gNnSc1	Dentitio
difficilis	difficilis	k1gFnPc2	difficilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
bolesti	bolest	k1gFnPc1	bolest
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
otevíráním	otevírání	k1gNnSc7	otevírání
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
otok	otok	k1gInSc1	otok
tváře	tvář	k1gFnSc2	tvář
nebo	nebo	k8xC	nebo
i	i	k9	i
vysoká	vysoký	k2eAgFnSc1d1	vysoká
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
schvácenost	schvácenost	k1gFnSc1	schvácenost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zánětu	zánět	k1gInSc3	zánět
dásně	dáseň	k1gFnSc2	dáseň
často	často	k6eAd1	často
vede	vést	k5eAaImIp3nS	vést
usazování	usazování	k1gNnSc4	usazování
plaku	plak	k1gInSc2	plak
okolo	okolo	k7c2	okolo
třetí	třetí	k4xOgFnSc2	třetí
stoličky	stolička	k1gFnSc2	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslabení	oslabení	k1gNnSc6	oslabení
imunity	imunita	k1gFnSc2	imunita
se	se	k3xPyFc4	se
zánět	zánět	k1gInSc1	zánět
může	moct	k5eAaImIp3nS	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
i	i	k9	i
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
zub	zub	k1gInSc4	zub
prořezán	prořezat	k5eAaPmNgInS	prořezat
jen	jen	k6eAd1	jen
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ještě	ještě	k9	ještě
navíc	navíc	k6eAd1	navíc
šikmo	šikmo	k6eAd1	šikmo
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
zubům	zub	k1gInPc3	zub
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
odstranit	odstranit	k5eAaPmF	odstranit
plak	plak	k1gInSc4	plak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
usazuje	usazovat	k5eAaImIp3nS	usazovat
na	na	k7c6	na
krčku	krček	k1gInSc6	krček
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Zubní	zubní	k2eAgFnSc1d1	zubní
cysta	cysta	k1gFnSc1	cysta
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
<g/>
,	,	kIx,	,
když	když	k8xS	když
buňky	buňka	k1gFnPc1	buňka
sedící	sedící	k2eAgFnPc1d1	sedící
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
skloviny	sklovina	k1gFnSc2	sklovina
neprořezaného	prořezaný	k2eNgInSc2d1	prořezaný
zubu	zub	k1gInSc2	zub
–	–	k?	–
ameloblasty	ameloblast	k1gInPc1	ameloblast
–	–	k?	–
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
sekreční	sekreční	k2eAgFnSc6d1	sekreční
činnosti	činnost	k1gFnSc6	činnost
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
sklovinu	sklovina	k1gFnSc4	sklovina
<g/>
.	.	kIx.	.
</s>
<s>
Cysta	cysta	k1gFnSc1	cysta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
infikována	infikovat	k5eAaBmNgFnS	infikovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
zánětu	zánět	k1gInSc2	zánět
čelisti	čelist	k1gFnSc2	čelist
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
tiše	tiš	k1gFnPc4	tiš
zvětšovat	zvětšovat	k5eAaImF	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
okolní	okolní	k2eAgFnSc4d1	okolní
kost	kost	k1gFnSc4	kost
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
až	až	k9	až
zlomeninu	zlomenina	k1gFnSc4	zlomenina
postižené	postižený	k2eAgFnSc2d1	postižená
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
neprořezaného	prořezaný	k2eNgInSc2d1	prořezaný
zubu	zub	k1gInSc2	zub
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
degeneraci	degenerace	k1gFnSc3	degenerace
zubního	zubní	k2eAgInSc2d1	zubní
váčku	váček	k1gInSc2	váček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
zub	zub	k1gInSc1	zub
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zub	zub	k1gInSc4	zub
moudrosti	moudrost	k1gFnSc2	moudrost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
