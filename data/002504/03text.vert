<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
je	být	k5eAaImIp3nS	být
nakažlivé	nakažlivý	k2eAgNnSc4d1	nakažlivé
onemocnění	onemocnění	k1gNnSc4	onemocnění
ptáků	pták	k1gMnPc2	pták
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
bakterií	bakterie	k1gFnPc2	bakterie
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
avium	avium	k1gInSc1	avium
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
avium	avium	k1gInSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
chronickým	chronický	k2eAgInSc7d1	chronický
průběhem	průběh	k1gInSc7	průběh
<g/>
,	,	kIx,	,
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
perzistencí	perzistence	k1gFnSc7	perzistence
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
<g/>
,	,	kIx,	,
snížením	snížení	k1gNnSc7	snížení
hmotnostních	hmotnostní	k2eAgMnPc2d1	hmotnostní
přírůstků	přírůstek	k1gInPc2	přírůstek
a	a	k8xC	a
snášky	snáška	k1gFnSc2	snáška
<g/>
,	,	kIx,	,
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
úhynem	úhyn	k1gInSc7	úhyn
a	a	k8xC	a
konfiskací	konfiskace	k1gFnSc7	konfiskace
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
držených	držený	k2eAgMnPc2d1	držený
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
i	i	k8xC	i
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
<g/>
.	.	kIx.	.
</s>
<s>
Infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
nákazy	nákaza	k1gFnSc2	nákaza
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
)	)	kIx)	)
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
v	v	k7c6	v
ojedinělých	ojedinělý	k2eAgInPc6d1	ojedinělý
případech	případ	k1gInPc6	případ
i	i	k9	i
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
zoonóza	zoonóza	k1gFnSc1	zoonóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
se	se	k3xPyFc4	se
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
epizootologických	epizootologický	k2eAgInPc2d1	epizootologický
důvodů	důvod	k1gInPc2	důvod
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
popis	popis	k1gInSc1	popis
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
již	již	k6eAd1	již
Aristotelovi	Aristoteles	k1gMnSc3	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
zvířat	zvíře	k1gNnPc2	zvíře
nedávala	dávat	k5eNaImAgFnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
za	za	k7c4	za
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
se	se	k3xPyFc4	se
nepovažovala	považovat	k5eNaImAgFnS	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
drůbeže	drůbež	k1gFnSc2	drůbež
asi	asi	k9	asi
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
Roloff	Roloff	k1gMnSc1	Roloff
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
mnohočetnou	mnohočetný	k2eAgFnSc4d1	mnohočetná
lymfosarkomatózu	lymfosarkomatóza	k1gFnSc4	lymfosarkomatóza
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
pak	pak	k8xC	pak
Zürn	Zürna	k1gFnPc2	Zürna
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Cornil	Cornil	k1gFnSc1	Cornil
a	a	k8xC	a
Mégnin	Mégnin	k1gInSc1	Mégnin
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paulike	Paulike	k1gInSc1	Paulike
a	a	k8xC	a
Gerlach	Gerlach	k1gInSc1	Gerlach
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
poukázali	poukázat	k5eAaPmAgMnP	poukázat
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
podobnost	podobnost	k1gFnSc4	podobnost
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
savců	savec	k1gMnPc2	savec
i	i	k9	i
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
přenosu	přenos	k1gInSc2	přenos
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
experimentálně	experimentálně	k6eAd1	experimentálně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Gerlach	Gerlach	k1gInSc4	Gerlach
u	u	k7c2	u
prasat	prase	k1gNnPc2	prase
po	po	k7c6	po
zkrmování	zkrmování	k1gNnSc6	zkrmování
tuberkulózní	tuberkulózní	k2eAgFnSc2d1	tuberkulózní
tkáně	tkáň	k1gFnSc2	tkáň
ze	z	k7c2	z
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
podstata	podstata	k1gFnSc1	podstata
a	a	k8xC	a
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
přenosnost	přenosnost	k1gFnSc1	přenosnost
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
mezi	mezi	k7c7	mezi
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
druhy	druh	k1gMnPc7	druh
zvířat	zvíře	k1gNnPc2	zvíře
byla	být	k5eAaImAgFnS	být
tak	tak	k9	tak
prokázána	prokázat	k5eAaPmNgFnS	prokázat
ještě	ještě	k9	ještě
před	před	k7c7	před
objevením	objevení	k1gNnSc7	objevení
původce	původce	k1gMnSc2	původce
onemocnění	onemocnění	k1gNnPc2	onemocnění
Robertem	Robert	k1gMnSc7	Robert
Kochem	Koch	k1gMnSc7	Koch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Koch	Koch	k1gMnSc1	Koch
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
považoval	považovat	k5eAaImAgInS	považovat
zpočátku	zpočátku	k6eAd1	zpočátku
jím	jíst	k5eAaImIp1nS	jíst
objeveného	objevený	k2eAgMnSc4d1	objevený
původce	původce	k1gMnSc4	původce
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
za	za	k7c2	za
vyvolavatele	vyvolavatel	k1gMnSc2	vyvolavatel
onemocnění	onemocnění	k1gNnSc2	onemocnění
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
nečinil	činit	k5eNaImAgInS	činit
rozdílu	rozdíl	k1gInSc2	rozdíl
mezi	mezi	k7c7	mezi
tuberkulózními	tuberkulózní	k2eAgInPc7d1	tuberkulózní
bacily	bacil	k1gInPc7	bacil
izolovanými	izolovaný	k2eAgInPc7d1	izolovaný
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
Maffucci	Maffucce	k1gFnSc4	Maffucce
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
a	a	k8xC	a
Strauss	Strauss	k1gInSc1	Strauss
a	a	k8xC	a
Gamaleja	Gamaleja	k1gFnSc1	Gamaleja
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
poukázali	poukázat	k5eAaPmAgMnP	poukázat
na	na	k7c4	na
odlišnost	odlišnost	k1gFnSc4	odlišnost
původce	původce	k1gMnSc2	původce
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
a	a	k8xC	a
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
zjistil	zjistit	k5eAaPmAgMnS	zjistit
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
původci	původce	k1gMnPc7	původce
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
Koch	Koch	k1gMnSc1	Koch
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
uznal	uznat	k5eAaPmAgInS	uznat
odlišnosti	odlišnost	k1gFnPc4	odlišnost
mezi	mezi	k7c7	mezi
ptačí	ptačí	k2eAgFnSc7d1	ptačí
<g/>
,	,	kIx,	,
bovinní	bovinní	k2eAgFnSc7d1	bovinní
a	a	k8xC	a
humánní	humánní	k2eAgFnSc7d1	humánní
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
prokázal	prokázat	k5eAaPmAgInS	prokázat
Ferran	Ferran	k1gInSc1	Ferran
aglutinační	aglutinační	k2eAgFnSc2d1	aglutinační
protilátky	protilátka	k1gFnSc2	protilátka
u	u	k7c2	u
případů	případ	k1gInPc2	případ
humánní	humánní	k2eAgFnSc2d1	humánní
i	i	k8xC	i
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
van	van	k1gInSc4	van
Es	es	k1gNnSc2	es
a	a	k8xC	a
Schalke	Schalke	k1gFnSc4	Schalke
techniku	technika	k1gFnSc4	technika
tuberkulinace	tuberkulinace	k1gFnSc2	tuberkulinace
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Rychlou	rychlý	k2eAgFnSc4d1	rychlá
sklíčkovou	sklíčkový	k2eAgFnSc4d1	sklíčkový
aglutinaci	aglutinace	k1gFnSc4	aglutinace
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
krví	krev	k1gFnSc7	krev
zavedli	zavést	k5eAaPmAgMnP	zavést
do	do	k7c2	do
diagnostiky	diagnostika	k1gFnSc2	diagnostika
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
drůbeže	drůbež	k1gFnSc2	drůbež
Moses	Mosesa	k1gFnPc2	Mosesa
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
se	se	k3xPyFc4	se
však	však	k9	však
různí	různit	k5eAaImIp3nP	různit
podle	podle	k7c2	podle
světadílů	světadíl	k1gInPc2	světadíl
<g/>
,	,	kIx,	,
úrovně	úroveň	k1gFnSc2	úroveň
chovu	chov	k1gInSc2	chov
drůbeže	drůbež	k1gFnSc2	drůbež
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
druhu	druh	k1gInSc6	druh
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
jejich	jejich	k3xOp3gInSc2	jejich
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Přetrvávajícím	přetrvávající	k2eAgInSc7d1	přetrvávající
problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
odlišného	odlišný	k2eAgInSc2d1	odlišný
způsobu	způsob	k1gInSc2	způsob
chovu	chov	k1gInSc2	chov
<g/>
,	,	kIx,	,
delšího	dlouhý	k2eAgInSc2d2	delší
věku	věk	k1gInSc2	věk
chovaných	chovaný	k2eAgNnPc2d1	chované
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
perzistence	perzistence	k1gFnSc1	perzistence
tuberkulózních	tuberkulózní	k2eAgInPc2d1	tuberkulózní
bacilů	bacil	k1gInPc2	bacil
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
ojediněle	ojediněle	k6eAd1	ojediněle
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
menších	malý	k2eAgInPc6d2	menší
extenzivních	extenzivní	k2eAgInPc6d1	extenzivní
chovech	chov	k1gInPc6	chov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
slepice	slepice	k1gFnPc1	slepice
drženy	držet	k5eAaImNgFnP	držet
do	do	k7c2	do
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
intenzivních	intenzivní	k2eAgFnPc6d1	intenzivní
formách	forma	k1gFnPc6	forma
chovu	chov	k1gInSc2	chov
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
Löwensteinem	Löwenstein	k1gInSc7	Löwenstein
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
u	u	k7c2	u
21	[number]	k4	21
<g/>
leté	letý	k2eAgFnSc2d1	letá
ženy	žena	k1gFnSc2	žena
s	s	k7c7	s
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
střev	střevo	k1gNnPc2	střevo
(	(	kIx(	(
<g/>
Pavlas	Pavlas	k1gMnSc1	Pavlas
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgNnPc4d1	osobní
sdělení	sdělení	k1gNnPc4	sdělení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
publikován	publikován	k2eAgMnSc1d1	publikován
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
Feldman	Feldman	k1gMnSc1	Feldman
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sérotypy	Sérotyp	k1gInPc1	Sérotyp
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
izolované	izolovaný	k2eAgInPc4d1	izolovaný
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
sérotypů	sérotyp	k1gInPc2	sérotyp
izolovaných	izolovaný	k2eAgInPc2d1	izolovaný
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
acidorezistentní	acidorezistentní	k2eAgFnSc2d1	acidorezistentní
bakterie	bakterie	k1gFnSc2	bakterie
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
avium	avium	k1gInSc4	avium
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInSc4d1	patřící
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
rodu	rod	k1gInSc2	rod
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
v	v	k7c4	v
čeledi	čeleď	k1gFnPc4	čeleď
Mycobacteriaceae	Mycobacteriaceae	k1gFnPc2	Mycobacteriaceae
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
60	[number]	k4	60
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgNnPc7	jenž
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
obligátně	obligátně	k6eAd1	obligátně
patogenních	patogenní	k2eAgMnPc2d1	patogenní
původců	původce	k1gMnPc2	původce
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
(	(	kIx(	(
<g/>
pravé	pravý	k2eAgFnSc2d1	pravá
tuberkulózní	tuberkulózní	k2eAgFnSc2d1	tuberkulózní
bakterie	bakterie	k1gFnSc2	bakterie
-	-	kIx~	-
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gInSc1	tuberculosis
a	a	k8xC	a
M.	M.	kA	M.
bovis	bovis	k1gFnSc7	bovis
<g/>
)	)	kIx)	)
též	též	k9	též
podmíněně	podmíněně	k6eAd1	podmíněně
patogenní	patogenní	k2eAgFnSc2d1	patogenní
(	(	kIx(	(
<g/>
atypické	atypický	k2eAgFnSc2d1	atypická
mykobakterie	mykobakterie	k1gFnSc2	mykobakterie
-	-	kIx~	-
např.	např.	kA	např.
M.	M.	kA	M.
avium	avium	k1gNnSc4	avium
<g/>
)	)	kIx)	)
a	a	k8xC	a
saprofyticky	saprofyticky	k6eAd1	saprofyticky
žijící	žijící	k2eAgInPc1d1	žijící
nepatogenní	patogenní	k2eNgInPc1d1	nepatogenní
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
polymorfismu	polymorfismus	k1gInSc2	polymorfismus
délky	délka	k1gFnSc2	délka
restrikčních	restrikční	k2eAgInPc2d1	restrikční
fragmentů	fragment	k1gInPc2	fragment
rozlišili	rozlišit	k5eAaPmAgMnP	rozlišit
Thorelová	Thorelová	k1gFnSc1	Thorelová
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
u	u	k7c2	u
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
3	[number]	k4	3
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
avium	avium	k1gInSc1	avium
<g/>
,	,	kIx,	,
paratuberculosis	paratuberculosis	k1gInSc1	paratuberculosis
a	a	k8xC	a
silvaticum	silvaticum	k1gInSc1	silvaticum
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
je	být	k5eAaImIp3nS	být
fenotypicky	fenotypicky	k6eAd1	fenotypicky
úzce	úzko	k6eAd1	úzko
příbuzné	příbuzný	k2eAgFnSc3d1	příbuzná
s	s	k7c7	s
M.	M.	kA	M.
intracellulare	intracellular	k1gMnSc5	intracellular
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
méně	málo	k6eAd2	málo
patogenní	patogenní	k2eAgFnSc1d1	patogenní
<g/>
.	.	kIx.	.
</s>
<s>
Morfologická	morfologický	k2eAgFnSc1d1	morfologická
<g/>
,	,	kIx,	,
biochemická	biochemický	k2eAgFnSc1d1	biochemická
i	i	k8xC	i
sérologická	sérologický	k2eAgFnSc1d1	sérologická
identifikace	identifikace	k1gFnSc1	identifikace
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
rutinně	rutinně	k6eAd1	rutinně
seskupovány	seskupovat	k5eAaImNgFnP	seskupovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
komplexu	komplex	k1gInSc2	komplex
M.	M.	kA	M.
avium-intracellulare	aviumntracellular	k1gMnSc5	avium-intracellular
(	(	kIx(	(
<g/>
komplex	komplex	k1gInSc4	komplex
MAI	MAI	kA	MAI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
podmíněně	podmíněně	k6eAd1	podmíněně
patogenní	patogenní	k2eAgInPc4d1	patogenní
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přenosné	přenosný	k2eAgNnSc1d1	přenosné
na	na	k7c4	na
některá	některý	k3yIgNnPc4	některý
domestikovaná	domestikovaný	k2eAgNnPc4d1	domestikované
i	i	k8xC	i
divoká	divoký	k2eAgNnPc4d1	divoké
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
tzv.	tzv.	kA	tzv.
mykobakteriózy	mykobakterióza	k1gFnSc2	mykobakterióza
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
MAI	MAI	kA	MAI
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
mykobakteriální	mykobakteriální	k2eAgFnSc7d1	mykobakteriální
infekcí	infekce	k1gFnSc7	infekce
komplikující	komplikující	k2eAgInSc4d1	komplikující
AIDS	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
i	i	k9	i
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
přírodních	přírodní	k2eAgInPc6d1	přírodní
zdrojích	zdroj	k1gInPc6	zdroj
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mykobakterie	Mykobakterie	k1gFnPc1	Mykobakterie
jsou	být	k5eAaImIp3nP	být
nepohyblivé	pohyblivý	k2eNgFnPc1d1	nepohyblivá
<g/>
,	,	kIx,	,
netvoří	tvořit	k5eNaImIp3nP	tvořit
pouzdro	pouzdro	k1gNnSc4	pouzdro
ani	ani	k8xC	ani
endospory	endospora	k1gFnPc4	endospora
<g/>
.	.	kIx.	.
</s>
<s>
Gramem	gram	k1gInSc7	gram
se	se	k3xPyFc4	se
barví	barvit	k5eAaImIp3nS	barvit
nesnadno	snadno	k6eNd1	snadno
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
grampozitivní	grampozitivní	k2eAgFnPc1d1	grampozitivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mikroskopickém	mikroskopický	k2eAgInSc6d1	mikroskopický
obraze	obraz	k1gInSc6	obraz
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
výrazně	výrazně	k6eAd1	výrazně
polymorfní	polymorfní	k2eAgFnPc4d1	polymorfní
(	(	kIx(	(
<g/>
od	od	k7c2	od
kokovitých	kokovitý	k2eAgNnPc2d1	kokovitý
až	až	k9	až
po	po	k7c4	po
vláknité	vláknitý	k2eAgFnPc4d1	vláknitá
formy	forma	k1gFnPc4	forma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
však	však	k9	však
rovné	rovný	k2eAgFnSc2d1	rovná
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
zahnuté	zahnutý	k2eAgFnPc1d1	zahnutá
krátké	krátká	k1gFnPc1	krátká
acidorezistentní	acidorezistentní	k2eAgFnSc2d1	acidorezistentní
tyčinky	tyčinka	k1gFnSc2	tyčinka
variabilní	variabilní	k2eAgFnSc2d1	variabilní
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
0,5	[number]	k4	0,5
x	x	k?	x
3	[number]	k4	3
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Netvoří	tvořit	k5eNaImIp3nS	tvořit
řetízky	řetízek	k1gInPc4	řetízek
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
neorganizovaných	organizovaný	k2eNgInPc6d1	neorganizovaný
shlucích	shluk	k1gInPc6	shluk
<g/>
.	.	kIx.	.
</s>
<s>
Množí	množit	k5eAaImIp3nS	množit
se	se	k3xPyFc4	se
dělením	dělení	k1gNnSc7	dělení
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
i	i	k9	i
větví	větvit	k5eAaImIp3nS	větvit
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mykobakterií	mykobakterie	k1gFnSc7	mykobakterie
je	být	k5eAaImIp3nS	být
acidorezistence	acidorezistence	k1gFnSc1	acidorezistence
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
obtížná	obtížný	k2eAgFnSc1d1	obtížná
barvitelnost	barvitelnost	k1gFnSc1	barvitelnost
organickými	organický	k2eAgNnPc7d1	organické
barvivy	barvivo	k1gNnPc7	barvivo
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
odolnost	odolnost	k1gFnSc1	odolnost
k	k	k7c3	k
odbarvení	odbarvení	k1gNnSc3	odbarvení
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
hydroxidy	hydroxid	k1gInPc7	hydroxid
a	a	k8xC	a
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
způsobovaná	způsobovaný	k2eAgNnPc4d1	způsobované
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
lipidů	lipid	k1gInPc2	lipid
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
toxickou	toxický	k2eAgFnSc7d1	toxická
bílkovinou	bílkovina	k1gFnSc7	bílkovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
autolýze	autolýza	k1gFnSc6	autolýza
bakterií	bakterie	k1gFnPc2	bakterie
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
do	do	k7c2	do
kultivačního	kultivační	k2eAgNnSc2d1	kultivační
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
tuberkulin	tuberkulin	k1gInSc1	tuberkulin
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
jej	on	k3xPp3gMnSc4	on
připravil	připravit	k5eAaPmAgMnS	připravit
Koch	Koch	k1gMnSc1	Koch
jako	jako	k8xS	jako
surový	surový	k2eAgInSc1d1	surový
zahuštěný	zahuštěný	k2eAgInSc1d1	zahuštěný
filtrát	filtrát	k1gInSc1	filtrát
částečně	částečně	k6eAd1	částečně
lyzované	lyzovaný	k2eAgFnSc2d1	lyzovaný
kultury	kultura	k1gFnSc2	kultura
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
v	v	k7c6	v
tekuté	tekutý	k2eAgFnSc6d1	tekutá
půdě	půda	k1gFnSc6	půda
(	(	kIx(	(
<g/>
tuberculinum	tuberculinum	k1gInSc1	tuberculinum
vetus	vetus	k1gMnSc1	vetus
Koch	Koch	k1gMnSc1	Koch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahoval	obsahovat	k5eAaImAgInS	obsahovat
vedle	vedle	k7c2	vedle
proteinové	proteinový	k2eAgFnSc2d1	proteinová
složky	složka	k1gFnSc2	složka
i	i	k8xC	i
jiné	jiný	k2eAgFnSc2d1	jiná
látky	látka	k1gFnSc2	látka
endoplasmatického	endoplasmatický	k2eAgInSc2d1	endoplasmatický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Purifikací	purifikace	k1gFnSc7	purifikace
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
"	"	kIx"	"
<g/>
purifikovaný	purifikovaný	k2eAgInSc1d1	purifikovaný
proteinový	proteinový	k2eAgInSc1d1	proteinový
derivát	derivát	k1gInSc1	derivát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
PPD	PPD	kA	PPD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
ještě	ještě	k9	ještě
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
určité	určitý	k2eAgNnSc1d1	určité
malé	malý	k2eAgNnSc1d1	malé
procento	procento	k1gNnSc1	procento
polysacharidů	polysacharid	k1gInPc2	polysacharid
a	a	k8xC	a
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
roste	růst	k5eAaImIp3nS	růst
aerobně	aerobně	k6eAd1	aerobně
na	na	k7c6	na
pevných	pevný	k2eAgFnPc6d1	pevná
půdách	půda	k1gFnPc6	půda
v	v	k7c6	v
optimu	optimum	k1gNnSc6	optimum
38-42	[number]	k4	38-42
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
10	[number]	k4	10
dní	den	k1gInPc2	den
až	až	k6eAd1	až
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
drobných	drobný	k2eAgInPc2d1	drobný
transparentních	transparentní	k2eAgInPc2d1	transparentní
granulovaných	granulovaný	k2eAgInPc2d1	granulovaný
a	a	k8xC	a
objemnějších	objemný	k2eAgInPc2d2	objemnější
hladkých	hladký	k2eAgInPc2d1	hladký
šedobílých	šedobílý	k2eAgInPc2d1	šedobílý
S	s	k7c7	s
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
;	;	kIx,	;
v	v	k7c6	v
tekutých	tekutý	k2eAgFnPc6d1	tekutá
půdách	půda	k1gFnPc6	půda
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
amorfní	amorfní	k2eAgInSc1d1	amorfní
sediment	sediment	k1gInSc1	sediment
a	a	k8xC	a
mléčný	mléčný	k2eAgInSc4d1	mléčný
zákal	zákal	k1gInSc4	zákal
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
kmeny	kmen	k1gInPc4	kmen
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
mycobactin	mycobactin	k2eAgInSc1d1	mycobactin
jako	jako	k8xC	jako
růstový	růstový	k2eAgInSc1d1	růstový
faktor	faktor	k1gInSc1	faktor
(	(	kIx(	(
<g/>
Matthews	Matthews	k1gInSc1	Matthews
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
nesyntetizuje	syntetizovat	k5eNaImIp3nS	syntetizovat
niacin	niacin	k1gInSc4	niacin
<g/>
,	,	kIx,	,
nehydrolyzuje	hydrolyzovat	k5eNaBmIp3nS	hydrolyzovat
Tween-	Tween-	k1gFnSc1	Tween-
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
peroxidáza	peroxidáza	k1gFnSc1	peroxidáza
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
ureázu	ureáza	k1gFnSc4	ureáza
ani	ani	k8xC	ani
arylsulfatázu	arylsulfatáza	k1gFnSc4	arylsulfatáza
a	a	k8xC	a
neredukuje	redukovat	k5eNaBmIp3nS	redukovat
nitráty	nitrát	k1gInPc4	nitrát
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
nikotinamid	nikotinamid	k1gInSc4	nikotinamid
a	a	k8xC	a
pyrazinamid	pyrazinamid	k1gInSc4	pyrazinamid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
obligátně	obligátně	k6eAd1	obligátně
patogenními	patogenní	k2eAgFnPc7d1	patogenní
mykobakteriemi	mykobakterie	k1gFnPc7	mykobakterie
je	být	k5eAaImIp3nS	být
M.	M.	kA	M.
avium	avium	k1gInSc1	avium
přirozeně	přirozeně	k6eAd1	přirozeně
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
na	na	k7c4	na
antituberkulotika	antituberkulotikum	k1gNnPc4	antituberkulotikum
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
isoniazid	isoniazid	k1gInSc1	isoniazid
<g/>
,	,	kIx,	,
rifampicin	rifampicin	k2eAgInSc1d1	rifampicin
<g/>
,	,	kIx,	,
pyrazinamid	pyrazinamid	k1gInSc1	pyrazinamid
<g/>
,	,	kIx,	,
streptomycin	streptomycin	k1gInSc1	streptomycin
nebo	nebo	k8xC	nebo
etambutol	etambutol	k1gInSc1	etambutol
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
MAI	MAI	kA	MAI
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
sérologického	sérologický	k2eAgInSc2d1	sérologický
průkazu	průkaz	k1gInSc2	průkaz
povrchových	povrchový	k2eAgInPc2d1	povrchový
antigenů	antigen	k1gInPc2	antigen
glykolipidové	glykolipidový	k2eAgFnSc2d1	glykolipidový
povahy	povaha	k1gFnSc2	povaha
na	na	k7c4	na
28	[number]	k4	28
sérotypů	sérotyp	k1gInPc2	sérotyp
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
jsou	být	k5eAaImIp3nP	být
patogenní	patogenní	k2eAgFnPc1d1	patogenní
zejména	zejména	k9	zejména
sérotypy	sérotyp	k1gInPc1	sérotyp
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
nalézány	nalézán	k2eAgInPc1d1	nalézán
sérotypy	sérotyp	k1gInPc1	sérotyp
4-20	[number]	k4	4-20
(	(	kIx(	(
<g/>
Thoen	Thona	k1gFnPc2	Thona
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Mykobakterie	Mykobakterie	k1gFnPc1	Mykobakterie
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
schopností	schopnost	k1gFnSc7	schopnost
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
přežívat	přežívat	k5eAaImF	přežívat
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
odolné	odolný	k2eAgFnPc1d1	odolná
proti	proti	k7c3	proti
vyschnutí	vyschnutí	k1gNnSc3	vyschnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
M.	M.	kA	M.
bovis	bovis	k1gFnSc7	bovis
větší	veliký	k2eAgFnSc4d2	veliký
schopnost	schopnost	k1gFnSc4	schopnost
přežívat	přežívat	k5eAaImF	přežívat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
pH	ph	kA	ph
<g/>
,	,	kIx,	,
druhu	druh	k1gInSc2	druh
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
světelných	světelný	k2eAgInPc2d1	světelný
poměrů	poměr	k1gInPc2	poměr
<g/>
)	)	kIx)	)
přežívá	přežívat	k5eAaImIp3nS	přežívat
113-417	[number]	k4	113-417
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
výběhu	výběh	k1gInSc2	výběh
180-792	[number]	k4	180-792
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
podestýlce	podestýlka	k1gFnSc6	podestýlka
až	až	k9	až
487	[number]	k4	487
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlhkém	vlhký	k2eAgInSc6d1	vlhký
trusu	trus	k1gInSc6	trus
až	až	k9	až
400	[number]	k4	400
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
kadáverech	kadáver	k1gInPc6	kadáver
drůbeže	drůbež	k1gFnSc2	drůbež
(	(	kIx(	(
<g/>
zahrabané	zahrabaný	k2eAgInPc1d1	zahrabaný
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
60	[number]	k4	60
cm	cm	kA	cm
<g/>
)	)	kIx)	)
až	až	k9	až
822	[number]	k4	822
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
zrnech	zrno	k1gNnPc6	zrno
obilnin	obilnina	k1gFnPc2	obilnina
až	až	k9	až
970	[number]	k4	970
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Schalk	Schalk	k6eAd1	Schalk
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
zjistili	zjistit	k5eAaPmAgMnP	zjistit
v	v	k7c6	v
kontaminované	kontaminovaný	k2eAgFnSc6d1	kontaminovaná
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
podestýlce	podestýlka	k1gFnSc6	podestýlka
životné	životný	k2eAgFnSc2d1	životná
mykobakterie	mykobakterie	k1gFnSc2	mykobakterie
po	po	k7c6	po
4-7	[number]	k4	4-7
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Virulentní	virulentní	k2eAgInPc1d1	virulentní
kmeny	kmen	k1gInPc1	kmen
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
přežívaly	přežívat	k5eAaImAgInP	přežívat
v	v	k7c6	v
pilinách	pilina	k1gFnPc6	pilina
168	[number]	k4	168
dní	den	k1gInPc2	den
při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
244	[number]	k4	244
dní	den	k1gInPc2	den
při	při	k7c6	při
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
Schliesser	Schliesser	k1gMnSc1	Schliesser
a	a	k8xC	a
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dezinfekčních	dezinfekční	k2eAgInPc2d1	dezinfekční
prostředků	prostředek	k1gInPc2	prostředek
mají	mít	k5eAaImIp3nP	mít
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
účinnost	účinnost	k1gFnSc4	účinnost
chlórové	chlórový	k2eAgInPc1d1	chlórový
preparáty	preparát	k1gInPc1	preparát
aktivované	aktivovaný	k2eAgInPc1d1	aktivovaný
amonnými	amonný	k2eAgFnPc7d1	amonná
solemi	sůl	k1gFnPc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
individuální	individuální	k2eAgFnSc1d1	individuální
druhová	druhový	k2eAgFnSc1d1	druhová
patogenita	patogenita	k1gFnSc1	patogenita
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
hostitele	hostitel	k1gMnPc4	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Mykobakterie	Mykobakterie	k1gFnPc1	Mykobakterie
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
intracelulárních	intracelulární	k2eAgMnPc2d1	intracelulární
parazitů	parazit	k1gMnPc2	parazit
chronickou	chronický	k2eAgFnSc4d1	chronická
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
typickým	typický	k2eAgInSc7d1	typický
projevem	projev	k1gInSc7	projev
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
infekčního	infekční	k2eAgInSc2d1	infekční
granulomu	granulom	k1gInSc2	granulom
-	-	kIx~	-
tuberkulózního	tuberkulózní	k2eAgInSc2d1	tuberkulózní
uzlíku	uzlík	k1gInSc2	uzlík
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
infikujících	infikující	k2eAgFnPc2d1	infikující
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
množení	množení	k1gNnSc4	množení
a	a	k8xC	a
perzistenci	perzistence	k1gFnSc4	perzistence
v	v	k7c6	v
hostiteli	hostitel	k1gMnSc6	hostitel
a	a	k8xC	a
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
nástupu	nástup	k1gInSc2	nástup
buněčné	buněčný	k2eAgFnSc2d1	buněčná
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Humorální	humorální	k2eAgFnSc1d1	humorální
imunita	imunita	k1gFnSc1	imunita
nemá	mít	k5eNaImIp3nS	mít
podstatný	podstatný	k2eAgInSc4d1	podstatný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
veterinární	veterinární	k2eAgFnSc4d1	veterinární
péči	péče	k1gFnSc4	péče
č.	č.	k?	č.
166	[number]	k4	166
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
nákazou	nákaza	k1gFnSc7	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přirozené	přirozený	k2eAgFnSc3d1	přirozená
infekci	infekce	k1gFnSc3	infekce
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
jsou	být	k5eAaImIp3nP	být
vnímavé	vnímavý	k2eAgInPc1d1	vnímavý
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
domestikovaní	domestikovaný	k2eAgMnPc1d1	domestikovaný
a	a	k8xC	a
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
chovaní	chovaný	k2eAgMnPc1d1	chovaný
ptáci	pták	k1gMnPc1	pták
bývají	bývat	k5eAaImIp3nP	bývat
postiženi	postihnout	k5eAaPmNgMnP	postihnout
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
než	než	k8xS	než
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgMnSc1d1	žijící
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
papoušků	papoušek	k1gMnPc2	papoušek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
také	také	k9	také
ojediněle	ojediněle	k6eAd1	ojediněle
vyvolána	vyvolán	k2eAgFnSc1d1	vyvolána
mykobakteriemi	mykobakterie	k1gFnPc7	mykobakterie
bovinního	bovinní	k2eAgInSc2d1	bovinní
i	i	k8xC	i
humánního	humánní	k2eAgInSc2d1	humánní
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
Ackerman	Ackerman	k1gMnSc1	Ackerman
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
extrémně	extrémně	k6eAd1	extrémně
vzácných	vzácný	k2eAgInPc6d1	vzácný
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc4	změna
obecně	obecně	k6eAd1	obecně
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
tvorbou	tvorba	k1gFnSc7	tvorba
benigních	benigní	k2eAgInPc2d1	benigní
lokalizovaných	lokalizovaný	k2eAgInPc2d1	lokalizovaný
granulomů	granulom	k1gInPc2	granulom
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
kolem	kolem	k7c2	kolem
zobáku	zobák	k1gInSc2	zobák
nebo	nebo	k8xC	nebo
nozder	nozdra	k1gFnPc2	nozdra
či	či	k8xC	či
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgFnPc1d1	ptačí
mykobakterie	mykobakterie	k1gFnPc1	mykobakterie
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
určitou	určitý	k2eAgFnSc4d1	určitá
patogenitu	patogenita	k1gFnSc4	patogenita
i	i	k9	i
pro	pro	k7c4	pro
důležité	důležitý	k2eAgInPc4d1	důležitý
druhy	druh	k1gInPc4	druh
domestikovaných	domestikovaný	k2eAgMnPc2d1	domestikovaný
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
slabou	slabý	k2eAgFnSc4d1	slabá
patogenitu	patogenita	k1gFnSc4	patogenita
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
(	(	kIx(	(
<g/>
Francis	Francis	k1gFnPc4	Francis
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
stájovém	stájový	k2eAgNnSc6d1	stájové
prostředí	prostředí	k1gNnSc6	prostředí
i	i	k8xC	i
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
povrchové	povrchový	k2eAgFnSc6d1	povrchová
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
měkkých	měkký	k2eAgInPc6d1	měkký
výbězích	výběh	k1gInPc6	výběh
<g/>
,	,	kIx,	,
pilinách	pilina	k1gFnPc6	pilina
<g/>
,	,	kIx,	,
hoblinách	hoblina	k1gFnPc6	hoblina
<g/>
,	,	kIx,	,
odpadových	odpadový	k2eAgFnPc6d1	odpadová
vodách	voda	k1gFnPc6	voda
dřevozpracujícího	dřevozpracující	k2eAgInSc2d1	dřevozpracující
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
výkalech	výkal	k1gInPc6	výkal
hlodavců	hlodavec	k1gMnPc2	hlodavec
i	i	k8xC	i
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Kontaminovat	kontaminovat	k5eAaBmF	kontaminovat
mohou	moct	k5eAaImIp3nP	moct
nevhodně	vhodně	k6eNd1	vhodně
uskladněné	uskladněný	k2eAgNnSc4d1	uskladněné
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
,	,	kIx,	,
slámu	sláma	k1gFnSc4	sláma
<g/>
,	,	kIx,	,
stájové	stájový	k2eAgNnSc4d1	stájové
prostředí	prostředí	k1gNnSc4	prostředí
včetně	včetně	k7c2	včetně
větracího	větrací	k2eAgNnSc2d1	větrací
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
nádrže	nádrž	k1gFnSc2	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
potrubí	potrubí	k1gNnSc4	potrubí
pro	pro	k7c4	pro
tekuté	tekutý	k2eAgNnSc4d1	tekuté
krmivo	krmivo	k1gNnSc4	krmivo
i	i	k8xC	i
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgInSc7d3	nejčastější
izolátem	izolát	k1gInSc7	izolát
z	z	k7c2	z
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
u	u	k7c2	u
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
zjišťováno	zjišťovat	k5eAaImNgNnS	zjišťovat
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
tuberkulózních	tuberkulózní	k2eAgInPc2d1	tuberkulózní
procesů	proces	k1gInPc2	proces
u	u	k7c2	u
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
vzniku	vznik	k1gInSc2	vznik
paraalergických	paraalergický	k2eAgFnPc2d1	paraalergický
tuberkulinových	tuberkulinový	k2eAgFnPc2d1	tuberkulinová
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Spontánní	spontánní	k2eAgFnPc1d1	spontánní
infekce	infekce	k1gFnPc1	infekce
savců	savec	k1gMnPc2	savec
nevyvolávají	vyvolávat	k5eNaImIp3nP	vyvolávat
tak	tak	k9	tak
vážné	vážný	k2eAgNnSc4d1	vážné
onemocnění	onemocnění	k1gNnSc4	onemocnění
jako	jako	k8xC	jako
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
experimentálně	experimentálně	k6eAd1	experimentálně
lze	lze	k6eAd1	lze
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
změny	změna	k1gFnPc4	změna
i	i	k9	i
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Mykobakteriózy	Mykobakterióza	k1gFnPc1	Mykobakterióza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
vyvolávány	vyvolávat	k5eAaImNgInP	vyvolávat
podmíněně	podmíněně	k6eAd1	podmíněně
patogenními	patogenní	k2eAgInPc7d1	patogenní
druhy	druh	k1gInPc7	druh
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
a	a	k8xC	a
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
stejný	stejný	k2eAgInSc4d1	stejný
typ	typ	k1gInSc4	typ
onemocnění	onemocnění	k1gNnSc2	onemocnění
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
časté	častý	k2eAgFnSc3d1	častá
rezistenci	rezistence	k1gFnSc3	rezistence
původců	původce	k1gMnPc2	původce
na	na	k7c4	na
antituberkulotika	antituberkulotikum	k1gNnPc4	antituberkulotikum
obtížně	obtížně	k6eAd1	obtížně
léčitelné	léčitelný	k2eAgFnPc1d1	léčitelná
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
endemicky	endemicky	k6eAd1	endemicky
i	i	k9	i
v	v	k7c6	v
ekonomicky	ekonomicky	k6eAd1	ekonomicky
vyspělých	vyspělý	k2eAgInPc6d1	vyspělý
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
dochází	docházet	k5eAaImIp3nS	docházet
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
rezervoárů	rezervoár	k1gInPc2	rezervoár
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
aerogenní	aerogenní	k2eAgFnSc7d1	aerogenní
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
faktorem	faktor	k1gInSc7	faktor
přenosu	přenos	k1gInSc2	přenos
je	být	k5eAaImIp3nS	být
infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
závodech	závod	k1gInPc6	závod
a	a	k8xC	a
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
interhumánnímu	interhumánní	k2eAgInSc3d1	interhumánní
přenosu	přenos	k1gInSc2	přenos
těchto	tento	k3xDgFnPc2	tento
infekcí	infekce	k1gFnPc2	infekce
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
M.	M.	kA	M.
avium-intracellulare	aviumntracellular	k1gMnSc5	avium-intracellular
prognosticky	prognosticky	k6eAd1	prognosticky
představuje	představovat	k5eAaImIp3nS	představovat
nejzávažnější	závažný	k2eAgFnSc4d3	nejzávažnější
formu	forma	k1gFnSc4	forma
mykobakteriózy	mykobakterióza	k1gFnSc2	mykobakterióza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vedlepneumocystózy	vedlepneumocystóza	k1gFnSc2	vedlepneumocystóza
a	a	k8xC	a
mykóz	mykóza	k1gFnPc2	mykóza
nejčastější	častý	k2eAgFnSc7d3	nejčastější
infekcí	infekce	k1gFnSc7	infekce
doprovázející	doprovázející	k2eAgInSc4d1	doprovázející
AIDS	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
<s>
Izolované	izolovaný	k2eAgInPc1d1	izolovaný
kmeny	kmen	k1gInPc1	kmen
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
rezistentní	rezistentní	k2eAgFnPc1d1	rezistentní
na	na	k7c4	na
všechna	všechen	k3xTgNnPc4	všechen
základní	základní	k2eAgNnPc4d1	základní
antituberkulostatika	antituberkulostatikum	k1gNnPc4	antituberkulostatikum
a	a	k8xC	a
i	i	k9	i
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
náhradních	náhradní	k2eAgInPc2d1	náhradní
léků	lék	k1gInPc2	lék
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
recidivám	recidiva	k1gFnPc3	recidiva
až	až	k9	až
v	v	k7c6	v
50	[number]	k4	50
%	%	kIx~	%
případů	případ	k1gInPc2	případ
a	a	k8xC	a
k	k	k7c3	k
letálnímu	letální	k2eAgInSc3d1	letální
průběhu	průběh	k1gInSc3	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Primárním	primární	k2eAgInSc7d1	primární
a	a	k8xC	a
nejčastějším	častý	k2eAgInSc7d3	nejčastější
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
jsou	být	k5eAaImIp3nP	být
postižení	postižený	k2eAgMnPc1d1	postižený
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
jejich	jejich	k3xOp3gInSc1	jejich
trus	trus	k1gInSc1	trus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
konstantním	konstantní	k2eAgInSc7d1	konstantní
zdrojem	zdroj	k1gInSc7	zdroj
virulentních	virulentní	k2eAgFnPc2d1	virulentní
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
<g/>
.	.	kIx.	.
</s>
<s>
Výkaly	výkal	k1gInPc1	výkal
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
bakterie	bakterie	k1gFnPc4	bakterie
ze	z	k7c2	z
zvředovatělých	zvředovatělý	k2eAgInPc2d1	zvředovatělý
(	(	kIx(	(
<g/>
ulcerujících	ulcerující	k2eAgInPc2d1	ulcerující
<g/>
)	)	kIx)	)
tuberkulů	tuberkul	k1gInPc2	tuberkul
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
změn	změna	k1gFnPc2	změna
jater	játra	k1gNnPc2	játra
i	i	k9	i
sliznice	sliznice	k1gFnSc1	sliznice
žlučového	žlučový	k2eAgInSc2d1	žlučový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
.	.	kIx.	.
-	-	kIx~	-
=	=	kIx~	=
nevnímavost	nevnímavost	k1gFnSc1	nevnímavost
<g/>
;	;	kIx,	;
+	+	kIx~	+
=	=	kIx~	=
nízká	nízký	k2eAgFnSc1d1	nízká
vnímavost	vnímavost	k1gFnSc1	vnímavost
<g/>
,	,	kIx,	,
převládají	převládat	k5eAaImIp3nP	převládat
dočasné	dočasný	k2eAgInPc1d1	dočasný
procesy	proces	k1gInPc1	proces
<g/>
;	;	kIx,	;
++	++	k?	++
=	=	kIx~	=
střední	střední	k2eAgFnSc1d1	střední
vnímavost	vnímavost	k1gFnSc1	vnímavost
<g/>
,	,	kIx,	,
převládají	převládat	k5eAaImIp3nP	převládat
lokální	lokální	k2eAgInPc1d1	lokální
procesy	proces	k1gInPc1	proces
<g/>
;	;	kIx,	;
+++	+++	k?	+++
=	=	kIx~	=
silná	silný	k2eAgFnSc1d1	silná
vnímavost	vnímavost	k1gFnSc1	vnímavost
<g/>
,	,	kIx,	,
lokální	lokální	k2eAgInPc4d1	lokální
a	a	k8xC	a
generalizované	generalizovaný	k2eAgInPc4d1	generalizovaný
procesy	proces	k1gInPc4	proces
<g/>
;	;	kIx,	;
++++	++++	k?	++++
=	=	kIx~	=
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vnímavost	vnímavost	k1gFnSc1	vnímavost
<g/>
,	,	kIx,	,
převládají	převládat	k5eAaImIp3nP	převládat
generalizované	generalizovaný	k2eAgInPc1d1	generalizovaný
procesy	proces	k1gInPc1	proces
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
kontaminovaném	kontaminovaný	k2eAgNnSc6d1	kontaminované
prostředí	prostředí	k1gNnSc6	prostředí
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
,	,	kIx,	,
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
půda	půda	k1gFnSc1	půda
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
na	na	k7c4	na
neinfikovaná	infikovaný	k2eNgNnPc4d1	neinfikované
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
déle	dlouho	k6eAd2	dlouho
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
prostředí	prostředí	k1gNnSc4	prostředí
kontaminováno	kontaminovat	k5eAaBmNgNnS	kontaminovat
a	a	k8xC	a
čím	co	k3yRnSc7	co
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
drůbeže	drůbež	k1gFnSc2	drůbež
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
vzniku	vznik	k1gInSc2	vznik
infekce	infekce	k1gFnSc2	infekce
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
savců	savec	k1gMnPc2	savec
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
nejčastěji	často	k6eAd3	často
k	k	k7c3	k
alimentárnímu	alimentární	k2eAgInSc3d1	alimentární
způsobu	způsob	k1gInSc3	způsob
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
aerogenní	aerogenní	k2eAgFnSc1d1	aerogenní
infekce	infekce	k1gFnSc1	infekce
není	být	k5eNaImIp3nS	být
vylučována	vylučován	k2eAgFnSc1d1	vylučována
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
epizootologického	epizootologický	k2eAgNnSc2d1	epizootologické
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc1d3	veliký
význam	význam	k1gInSc1	význam
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
ptáky	pták	k1gMnPc7	pták
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgMnPc7d1	jiný
druhy	druh	k1gMnPc7	druh
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
prasata	prase	k1gNnPc4	prase
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
hlavně	hlavně	k9	hlavně
synantropní	synantropní	k2eAgInPc4d1	synantropní
a	a	k8xC	a
hemisynantropní	hemisynantropní	k2eAgInPc4d1	hemisynantropní
druhy	druh	k1gInPc4	druh
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
pernatá	pernatý	k2eAgFnSc1d1	pernatá
lovná	lovný	k2eAgFnSc1d1	lovná
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
domestikovanými	domestikovaný	k2eAgMnPc7d1	domestikovaný
ptáky	pták	k1gMnPc7	pták
a	a	k8xC	a
savci	savec	k1gMnPc7	savec
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zalétávají	zalétávat	k5eAaImIp3nP	zalétávat
do	do	k7c2	do
zamořených	zamořený	k2eAgInPc2d1	zamořený
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
přelétávají	přelétávat	k5eAaImIp3nP	přelétávat
do	do	k7c2	do
nezamořených	zamořený	k2eNgInPc2d1	zamořený
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
šířit	šířit	k5eAaImF	šířit
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
pasivně	pasivně	k6eAd1	pasivně
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
kontaminováno	kontaminovat	k5eAaBmNgNnS	kontaminovat
trusem	trus	k1gInSc7	trus
infikované	infikovaný	k2eAgFnPc4d1	infikovaná
drůbeže	drůbež	k1gFnPc4	drůbež
nebo	nebo	k8xC	nebo
aktivně	aktivně	k6eAd1	aktivně
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
při	při	k7c6	při
opakovaném	opakovaný	k2eAgInSc6d1	opakovaný
styku	styk	k1gInSc6	styk
s	s	k7c7	s
tuberkulózní	tuberkulózní	k2eAgFnSc7d1	tuberkulózní
drůbeží	drůbež	k1gFnSc7	drůbež
sami	sám	k3xTgMnPc1	sám
infikují	infikovat	k5eAaBmIp3nP	infikovat
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
stálým	stálý	k2eAgInSc7d1	stálý
zdrojem	zdroj	k1gInSc7	zdroj
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
nejméně	málo	k6eAd3	málo
u	u	k7c2	u
45	[number]	k4	45
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
možnými	možný	k2eAgInPc7d1	možný
zdroji	zdroj	k1gInPc7	zdroj
jsou	být	k5eAaImIp3nP	být
maso	maso	k1gNnSc1	maso
(	(	kIx(	(
<g/>
bakteriémie	bakteriémie	k1gFnSc1	bakteriémie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odpady	odpad	k1gInPc4	odpad
a	a	k8xC	a
exkrementy	exkrement	k1gInPc4	exkrement
tuberkulózní	tuberkulózní	k2eAgFnSc2d1	tuberkulózní
drůbeže	drůbež	k1gFnSc2	drůbež
používané	používaný	k2eAgFnSc2d1	používaná
ke	k	k7c3	k
krmným	krmný	k2eAgInPc3d1	krmný
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
krmivo	krmivo	k1gNnSc4	krmivo
z	z	k7c2	z
mísíren	mísírna	k1gFnPc2	mísírna
nebo	nebo	k8xC	nebo
skladů	sklad	k1gInPc2	sklad
kontaminované	kontaminovaný	k2eAgFnPc4d1	kontaminovaná
trusem	trus	k1gInSc7	trus
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
obuv	obuv	k1gFnSc1	obuv
ošetřovatelů	ošetřovatel	k1gMnPc2	ošetřovatel
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgFnPc1d1	pracovní
pomůcky	pomůcka	k1gFnPc1	pomůcka
i	i	k8xC	i
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
kadávery	kadáver	k1gInPc1	kadáver
uhynulých	uhynulý	k2eAgNnPc2d1	uhynulé
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
kanibalismus	kanibalismus	k1gInSc1	kanibalismus
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Členovci	členovec	k1gMnPc1	členovec
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
mechanický	mechanický	k2eAgInSc4d1	mechanický
vektor	vektor	k1gInSc4	vektor
šíření	šíření	k1gNnSc2	šíření
M.	M.	kA	M.
avium	avium	k1gInSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
vertikální	vertikální	k2eAgInSc1d1	vertikální
přenos	přenos	k1gInSc1	přenos
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
bylo	být	k5eAaImAgNnS	být
izolováno	izolovat	k5eAaBmNgNnS	izolovat
z	z	k7c2	z
vajec	vejce	k1gNnPc2	vejce
od	od	k7c2	od
přirozeně	přirozeně	k6eAd1	přirozeně
i	i	k8xC	i
uměle	uměle	k6eAd1	uměle
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
nosnic	nosnice	k1gFnPc2	nosnice
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
přítomné	přítomný	k2eAgMnPc4d1	přítomný
ve	v	k7c6	v
vejcích	vejce	k1gNnPc6	vejce
je	být	k5eAaImIp3nS	být
inaktivováno	inaktivovat	k5eAaBmNgNnS	inaktivovat
varem	var	k1gInSc7	var
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
minimálně	minimálně	k6eAd1	minimálně
6	[number]	k4	6
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
střevní	střevní	k2eAgInSc4d1	střevní
trakt	trakt	k1gInSc4	trakt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počáteční	počáteční	k2eAgFnSc6d1	počáteční
kolonizaci	kolonizace	k1gFnSc6	kolonizace
stěny	stěna	k1gFnSc2	stěna
střevní	střevní	k2eAgFnSc2d1	střevní
dochází	docházet	k5eAaImIp3nS	docházet
rychle	rychle	k6eAd1	rychle
ke	k	k7c3	k
generalizaci	generalizace	k1gFnSc3	generalizace
onemocnění	onemocnění	k1gNnSc2	onemocnění
a	a	k8xC	a
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
bakterií	bakterie	k1gFnPc2	bakterie
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
cestou	cesta	k1gFnSc7	cesta
lymfatickou	lymfatický	k2eAgFnSc7d1	lymfatická
i	i	k8xC	i
krevní	krevní	k2eAgFnSc7d1	krevní
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
subklinická	subklinický	k2eAgFnSc1d1	subklinická
bakteriémie	bakteriémie	k1gFnSc1	bakteriémie
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
neomezené	omezený	k2eNgNnSc1d1	neomezené
šíření	šíření	k1gNnSc1	šíření
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnPc1	plíce
jsou	být	k5eAaImIp3nP	být
infikovány	infikován	k2eAgInPc1d1	infikován
spíše	spíše	k9	spíše
sekundárně	sekundárně	k6eAd1	sekundárně
během	během	k7c2	během
bakteriémie	bakteriémie	k1gFnSc2	bakteriémie
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
vzniká	vznikat	k5eAaImIp3nS	vznikat
primární	primární	k2eAgInSc1d1	primární
afekt	afekt	k1gInSc1	afekt
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkuly	tuberkul	k1gInPc1	tuberkul
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
infekce	infekce	k1gFnSc2	infekce
střevní	střevní	k2eAgFnSc2d1	střevní
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
otevřené	otevřený	k2eAgInPc4d1	otevřený
do	do	k7c2	do
střevního	střevní	k2eAgNnSc2d1	střevní
lumina	lumen	k1gNnSc2	lumen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
trvalé	trvalý	k2eAgNnSc4d1	trvalé
vylučování	vylučování	k1gNnSc4	vylučování
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
trusem	trus	k1gInSc7	trus
(	(	kIx(	(
<g/>
otevřená	otevřený	k2eAgFnSc1d1	otevřená
infekce	infekce	k1gFnSc1	infekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
typické	typický	k2eAgFnSc2d1	typická
infekce	infekce	k1gFnSc2	infekce
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
měkkozobí	měkkozobý	k2eAgMnPc1d1	měkkozobý
(	(	kIx(	(
<g/>
Columbiformes	Columbiformes	k1gMnSc1	Columbiformes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrubozobí	vrubozobí	k1gMnPc1	vrubozobí
(	(	kIx(	(
<g/>
Anseriformes	Anseriformes	k1gMnSc1	Anseriformes
<g/>
)	)	kIx)	)
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
pěvci	pěvec	k1gMnPc1	pěvec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Textor	Textor	k1gInSc1	Textor
změny	změna	k1gFnSc2	změna
často	často	k6eAd1	často
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgFnSc1d1	akutní
bakteriémie	bakteriémie	k1gFnSc1	bakteriémie
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
krátkokřídlých	krátkokřídlý	k2eAgFnPc2d1	krátkokřídlý
(	(	kIx(	(
<g/>
Gruiformes	Gruiformesa	k1gFnPc2	Gruiformesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ibise	ibis	k1gMnSc5	ibis
skalního	skalní	k2eAgMnSc2d1	skalní
(	(	kIx(	(
<g/>
Geronticus	Geronticus	k1gInSc1	Geronticus
eremita	eremit	k1gMnSc2	eremit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Columbiformes	Columbiformes	k1gMnSc1	Columbiformes
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
pěvců	pěvec	k1gMnPc2	pěvec
(	(	kIx(	(
<g/>
Passeriformes	Passeriformes	k1gMnSc1	Passeriformes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
3	[number]	k4	3
typy	typa	k1gFnSc2	typa
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
patogeneze	patogeneze	k1gFnPc1	patogeneze
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známa	znám	k2eAgFnSc1d1	známa
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
klasická	klasický	k2eAgFnSc1d1	klasická
forma	forma	k1gFnSc1	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
)	)	kIx)	)
paratuberkulózní	paratuberkulózní	k2eAgFnSc1d1	paratuberkulózní
forma	forma	k1gFnSc1	forma
s	s	k7c7	s
typickými	typický	k2eAgFnPc7d1	typická
změnami	změna	k1gFnPc7	změna
ve	v	k7c6	v
střevním	střevní	k2eAgInSc6d1	střevní
traktu	trakt	k1gInSc6	trakt
(	(	kIx(	(
<g/>
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
formě	forma	k1gFnSc3	forma
mykobakteriózy	mykobakterióza	k1gFnSc2	mykobakterióza
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc1	sklon
amazoňani	amazoňan	k1gMnPc1	amazoňan
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
amazónkové	amazónkové	k2eAgInSc1d1	amazónkové
(	(	kIx(	(
<g/>
Pionus	Pionus	k1gInSc1	Pionus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tiriky	tirik	k1gInPc1	tirik
(	(	kIx(	(
<g/>
Brotogeris	Brotogeris	k1gFnSc1	Brotogeris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alexandrové	alexandrové	k2eAgFnSc1d1	alexandrové
(	(	kIx(	(
<g/>
Psittacula	Psittacula	k1gFnSc1	Psittacula
<g/>
)	)	kIx)	)
a	a	k8xC	a
papoušek	papoušek	k1gMnSc1	papoušek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Eunymphicus	Eunymphicus	k1gMnSc1	Eunymphicus
conutus	conutus	k1gMnSc1	conutus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
3	[number]	k4	3
<g/>
)	)	kIx)	)
netuberkulózní	tuberkulózní	k2eNgFnSc1d1	netuberkulózní
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
zjistitelná	zjistitelný	k2eAgFnSc1d1	zjistitelná
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
)	)	kIx)	)
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
zpravidla	zpravidla	k6eAd1	zpravidla
probíhá	probíhat	k5eAaImIp3nS	probíhat
chronicky	chronicky	k6eAd1	chronicky
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
až	až	k8xS	až
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
četnosti	četnost	k1gFnSc6	četnost
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
infekční	infekční	k2eAgFnSc2d1	infekční
dávky	dávka	k1gFnSc2	dávka
a	a	k8xC	a
na	na	k7c4	na
kondici	kondice	k1gFnSc4	kondice
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
experimentální	experimentální	k2eAgFnSc4d1	experimentální
infekci	infekce	k1gFnSc4	infekce
asi	asi	k9	asi
20	[number]	k4	20
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
až	až	k9	až
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgFnSc4d1	variabilní
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
nespecifické	specifický	k2eNgNnSc1d1	nespecifické
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
nejčastěji	často	k6eAd3	často
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
starší	starší	k1gMnSc1	starší
1	[number]	k4	1
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nákaza	nákaza	k1gFnSc1	nákaza
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
pomalým	pomalý	k2eAgInSc7d1	pomalý
průběhem	průběh	k1gInSc7	průběh
<g/>
,	,	kIx,	,
ojedinělými	ojedinělý	k2eAgInPc7d1	ojedinělý
úhyny	úhyn	k1gInPc7	úhyn
<g/>
,	,	kIx,	,
vyhublostí	vyhublost	k1gFnSc7	vyhublost
a	a	k8xC	a
poklesem	pokles	k1gInSc7	pokles
snášky	snáška	k1gFnSc2	snáška
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
<g/>
.	.	kIx.	.
</s>
<s>
Postižená	postižený	k2eAgFnSc1d1	postižená
drůbež	drůbež	k1gFnSc1	drůbež
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
,	,	kIx,	,
posedává	posedávat	k5eAaImIp3nS	posedávat
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
unaví	unavit	k5eAaPmIp3nP	unavit
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
ke	k	k7c3	k
žrádlu	žrádlo	k1gNnSc3	žrádlo
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ptáci	pták	k1gMnPc1	pták
rychle	rychle	k6eAd1	rychle
hubnou	hubnout	k5eAaImIp3nP	hubnout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
atrofií	atrofie	k1gFnSc7	atrofie
prsních	prsní	k2eAgInPc2d1	prsní
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
prominujícím	prominující	k2eAgInSc7d1	prominující
hřebenem	hřeben	k1gInSc7	hřeben
kosti	kost	k1gFnSc2	kost
prsní	prsní	k2eAgFnSc2d1	prsní
<g/>
.	.	kIx.	.
</s>
<s>
Tělního	tělní	k2eAgInSc2d1	tělní
tuku	tuk	k1gInSc2	tuk
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
bývá	bývat	k5eAaImIp3nS	bývat
matné	matný	k2eAgFnPc4d1	matná
a	a	k8xC	a
načepýřené	načepýřený	k2eAgFnPc4d1	načepýřená
<g/>
.	.	kIx.	.
</s>
<s>
Hřebínek	hřebínek	k1gInSc1	hřebínek
<g/>
,	,	kIx,	,
lalůčky	lalůček	k1gInPc1	lalůček
a	a	k8xC	a
ušní	ušní	k2eAgInPc1d1	ušní
laloky	lalok	k1gInPc1	lalok
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
anemické	anemický	k2eAgInPc1d1	anemický
<g/>
,	,	kIx,	,
cyanotické	cyanotický	k2eAgInPc1d1	cyanotický
nebo	nebo	k8xC	nebo
ikterické	ikterický	k2eAgInPc1d1	ikterický
(	(	kIx(	(
<g/>
porucha	porucha	k1gFnSc1	porucha
jater	játra	k1gNnPc2	játra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
nezměněná	změněný	k2eNgFnSc1d1	nezměněná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
tuberkulů	tuberkul	k1gInPc2	tuberkul
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
drůbež	drůbež	k1gFnSc4	drůbež
kulhá	kulhat	k5eAaImIp3nS	kulhat
<g/>
.	.	kIx.	.
</s>
<s>
Průjem	průjem	k1gInSc1	průjem
vyvolaný	vyvolaný	k2eAgInSc1d1	vyvolaný
vředovitými	vředovitý	k2eAgFnPc7d1	vředovitá
změnami	změna	k1gFnPc7	změna
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nadměrnou	nadměrný	k2eAgFnSc4d1	nadměrná
slabost	slabost	k1gFnSc4	slabost
a	a	k8xC	a
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
silně	silně	k6eAd1	silně
vyhublé	vyhublý	k2eAgFnSc2d1	vyhublá
drůbeže	drůbež	k1gFnSc2	drůbež
lze	lze	k6eAd1	lze
detekovat	detekovat	k5eAaImF	detekovat
palpací	palpace	k1gFnSc7	palpace
přes	přes	k7c4	přes
stěnu	stěna	k1gFnSc4	stěna
břišní	břišní	k2eAgInPc4d1	břišní
tuberkuly	tuberkul	k1gInPc4	tuberkul
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
je	být	k5eAaImIp3nS	být
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
a	a	k8xC	a
rozsahu	rozsah	k1gInSc2	rozsah
postižení	postižení	k1gNnSc2	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
náhlým	náhlý	k2eAgInPc3d1	náhlý
úhynům	úhyn	k1gInPc3	úhyn
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ruptury	ruptura	k1gFnSc2	ruptura
jater	játra	k1gNnPc2	játra
anebo	anebo	k8xC	anebo
sleziny	slezina	k1gFnSc2	slezina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
atypické	atypický	k2eAgFnSc2d1	atypická
a	a	k8xC	a
acidorezistentní	acidorezistentní	k2eAgFnSc2d1	acidorezistentní
mykobakterie	mykobakterie	k1gFnSc2	mykobakterie
jsou	být	k5eAaImIp3nP	být
detekovány	detekován	k2eAgMnPc4d1	detekován
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
náhodně	náhodně	k6eAd1	náhodně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
malých	malý	k2eAgMnPc2d1	malý
pěvců	pěvec	k1gMnPc2	pěvec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čížek	čížek	k1gMnSc1	čížek
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
vyhublost	vyhublost	k1gFnSc1	vyhublost
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
polyurie	polyurie	k1gFnSc1	polyurie
<g/>
,	,	kIx,	,
anémie	anémie	k1gFnSc1	anémie
a	a	k8xC	a
matné	matný	k2eAgNnSc1d1	matné
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
tuberkulů	tuberkul	k1gInPc2	tuberkul
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
subklinický	subklinický	k2eAgInSc4d1	subklinický
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Přerušované	přerušovaný	k2eAgNnSc1d1	přerušované
kulhání	kulhání	k1gNnSc1	kulhání
<g/>
,	,	kIx,	,
přešlapování	přešlapování	k1gNnSc1	přešlapování
a	a	k8xC	a
artritidy	artritida	k1gFnPc1	artritida
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
karpometakarpálního	karpometakarpální	k2eAgInSc2d1	karpometakarpální
a	a	k8xC	a
loketního	loketní	k2eAgInSc2d1	loketní
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tvorba	tvorba	k1gFnSc1	tvorba
tuberkulů	tuberkul	k1gInPc2	tuberkul
ve	v	k7c6	v
svalovině	svalovina	k1gFnSc6	svalovina
stehna	stehno	k1gNnSc2	stehno
nebo	nebo	k8xC	nebo
běháku	běhák	k1gInSc2	běhák
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgInPc1d1	častý
u	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
postiženého	postižený	k2eAgInSc2d1	postižený
kloubu	kloub	k1gInSc2	kloub
bývá	bývat	k5eAaImIp3nS	bývat
zesílená	zesílený	k2eAgFnSc1d1	zesílená
a	a	k8xC	a
ulcerovaná	ulcerovaný	k2eAgFnSc1d1	ulcerovaný
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
tuberkulů	tuberkul	k1gInPc2	tuberkul
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Granulomy	granulom	k1gInPc1	granulom
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
konjunktiválním	konjunktivální	k2eAgInSc6d1	konjunktivální
vaku	vak	k1gInSc6	vak
<g/>
,	,	kIx,	,
v	v	k7c6	v
koutcích	koutek	k1gInPc6	koutek
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
vnějšího	vnější	k2eAgInSc2d1	vnější
zvukovodu	zvukovod	k1gInSc2	zvukovod
a	a	k8xC	a
na	na	k7c6	na
oropharyngu	oropharyng	k1gInSc6	oropharyng
(	(	kIx(	(
<g/>
dutina	dutina	k1gFnSc1	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
a	a	k8xC	a
hltan	hltan	k1gInSc1	hltan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nandu	nandu	k1gMnSc2	nandu
pampového	pampový	k2eAgMnSc2d1	pampový
(	(	kIx(	(
<g/>
Rhea	Rheus	k1gMnSc2	Rheus
americana	americana	k1gFnSc1	americana
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
granulomy	granulom	k1gInPc1	granulom
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
hltanu	hltan	k1gInSc6	hltan
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
pštrosů	pštros	k1gMnPc2	pštros
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
chronické	chronický	k2eAgNnSc1d1	chronické
chřadnutí	chřadnutí	k1gNnSc1	chřadnutí
<g/>
,	,	kIx,	,
tuberkuly	tuberkul	k1gInPc1	tuberkul
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
lze	lze	k6eAd1	lze
detekovat	detekovat	k5eAaImF	detekovat
laparotomií	laparotomie	k1gFnSc7	laparotomie
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
související	související	k2eAgInPc1d1	související
s	s	k7c7	s
kolonizací	kolonizace	k1gFnSc7	kolonizace
plic	plíce	k1gFnPc2	plíce
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
u	u	k7c2	u
pávů	páv	k1gMnPc2	páv
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
respiračním	respirační	k2eAgFnPc3d1	respirační
potížím	potíž	k1gFnPc3	potíž
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc6d1	způsobená
tvorbou	tvorba	k1gFnSc7	tvorba
granulomů	granulom	k1gInPc2	granulom
v	v	k7c6	v
průdušnici	průdušnice	k1gFnSc6	průdušnice
<g/>
.	.	kIx.	.
</s>
<s>
Pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
M.	M.	kA	M.
avium	avium	k1gInSc1	avium
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
druhem	druh	k1gInSc7	druh
hostitele	hostitel	k1gMnSc2	hostitel
a	a	k8xC	a
sérotypem	sérotyp	k1gInSc7	sérotyp
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Postiženy	postižen	k2eAgInPc1d1	postižen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kterékoliv	kterýkoliv	k3yIgInPc4	kterýkoliv
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
CNS	CNS	kA	CNS
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
je	být	k5eAaImIp3nS	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
výskytem	výskyt	k1gInSc7	výskyt
nepravidelných	pravidelný	k2eNgInPc2d1	nepravidelný
šedožlutých	šedožlutý	k2eAgInPc2d1	šedožlutý
nebo	nebo	k8xC	nebo
šedobílých	šedobílý	k2eAgInPc2d1	šedobílý
nodulů	nodul	k1gInPc2	nodul
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
i	i	k8xC	i
množství	množství	k1gNnSc2	množství
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
slezině	slezina	k1gFnSc3	slezina
<g/>
,	,	kIx,	,
střevu	střevo	k1gNnSc3	střevo
a	a	k8xC	a
kostní	kostní	k2eAgFnSc3d1	kostní
dřeni	dřeň	k1gFnSc3	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
tuberkulů	tuberkul	k1gInPc2	tuberkul
velmi	velmi	k6eAd1	velmi
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
,	,	kIx,	,
od	od	k7c2	od
několika	několik	k4yIc2	několik
milimetrů	milimetr	k1gInPc2	milimetr
až	až	k6eAd1	až
do	do	k7c2	do
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkuly	tuberkul	k1gInPc1	tuberkul
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
pohmat	pohmat	k1gInSc4	pohmat
tuhé	tuhý	k2eAgFnSc2d1	tuhá
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
vyjmutelné	vyjmutelný	k2eAgInPc1d1	vyjmutelný
z	z	k7c2	z
parenchymu	parenchym	k1gInSc2	parenchym
orgánů	orgán	k1gInPc2	orgán
<g/>
;	;	kIx,	;
na	na	k7c6	na
řezu	řez	k1gInSc6	řez
jsou	být	k5eAaImIp3nP	být
suché	suchý	k2eAgInPc1d1	suchý
s	s	k7c7	s
kaseózním	kaseózní	k2eAgInSc7d1	kaseózní
(	(	kIx(	(
<g/>
sýrovitým	sýrovitý	k2eAgInSc7d1	sýrovitý
<g/>
)	)	kIx)	)
středem	střed	k1gInSc7	střed
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
nodulů	nodul	k1gInPc2	nodul
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
forma	forma	k1gFnSc1	forma
miliární	miliární	k2eAgFnSc1d1	miliární
<g/>
,	,	kIx,	,
uzlíkovitá	uzlíkovitý	k2eAgFnSc1d1	uzlíkovitý
a	a	k8xC	a
uzlová	uzlový	k2eAgFnSc1d1	uzlová
<g/>
,	,	kIx,	,
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
forma	forma	k1gFnSc1	forma
vředovitá	vředovitý	k2eAgFnSc1d1	vředovitá
(	(	kIx(	(
<g/>
ulcerózní	ulcerózní	k2eAgFnSc1d1	ulcerózní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozsahu	rozsah	k1gInSc2	rozsah
pak	pak	k6eAd1	pak
forma	forma	k1gFnSc1	forma
lokální	lokální	k2eAgFnSc1d1	lokální
<g/>
,	,	kIx,	,
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
a	a	k8xC	a
generalizovaná	generalizovaný	k2eAgFnSc1d1	generalizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgFnPc1d1	obdobná
změny	změna	k1gFnPc1	změna
byly	být	k5eAaImAgFnP	být
také	také	k9	také
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
u	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
sov	sova	k1gFnPc2	sova
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
<g/>
,	,	kIx,	,
bahňáků	bahňák	k1gMnPc2	bahňák
<g/>
,	,	kIx,	,
brodivých	brodivý	k2eAgMnPc2d1	brodivý
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kukaček	kukačka	k1gFnPc2	kukačka
<g/>
,	,	kIx,	,
šplhavců	šplhavec	k1gMnPc2	šplhavec
(	(	kIx(	(
<g/>
Piciformes	Piciformes	k1gMnSc1	Piciformes
<g/>
)	)	kIx)	)
a	a	k8xC	a
chřastalovitých	chřastalovitý	k2eAgFnPc2d1	chřastalovitý
(	(	kIx(	(
<g/>
Rallidae	Rallidae	k1gFnPc2	Rallidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
toho	ten	k3xDgMnSc2	ten
u	u	k7c2	u
holubů	holub	k1gMnPc2	holub
<g/>
,	,	kIx,	,
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
pěvců	pěvec	k1gMnPc2	pěvec
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
papoušků	papoušek	k1gMnPc2	papoušek
se	se	k3xPyFc4	se
typické	typický	k2eAgInPc1d1	typický
granulomy	granulom	k1gInPc1	granulom
netvoří	tvořit	k5eNaImIp3nP	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Acidorezistentní	Acidorezistentní	k2eAgFnPc1d1	Acidorezistentní
tyčinky	tyčinka	k1gFnPc1	tyčinka
jsou	být	k5eAaImIp3nP	být
nalézány	nalézat	k5eAaImNgFnP	nalézat
roztroušené	roztroušený	k2eAgFnPc1d1	roztroušená
v	v	k7c6	v
parenchymu	parenchym	k1gInSc6	parenchym
infikovaných	infikovaný	k2eAgInPc2d1	infikovaný
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Infikovaná	infikovaný	k2eAgNnPc1d1	infikované
játra	játra	k1gNnPc1	játra
nebo	nebo	k8xC	nebo
slezina	slezina	k1gFnSc1	slezina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zduřelé	zduřelý	k2eAgInPc1d1	zduřelý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
nekrotické	nekrotický	k2eAgInPc4d1	nekrotický
okrsky	okrsek	k1gInPc4	okrsek
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
jen	jen	k6eAd1	jen
tuhé	tuhý	k2eAgInPc1d1	tuhý
na	na	k7c4	na
pohmat	pohmat	k1gInSc4	pohmat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
holubů	holub	k1gMnPc2	holub
jaterní	jaterní	k2eAgFnSc2d1	jaterní
změny	změna	k1gFnSc2	změna
připomínají	připomínat	k5eAaImIp3nP	připomínat
abscesy	absces	k1gInPc1	absces
při	při	k7c6	při
trichomonóze	trichomonóza	k1gFnSc6	trichomonóza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pelikánů	pelikán	k1gInPc2	pelikán
byly	být	k5eAaImAgFnP	být
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
tumorózní	tumorózní	k2eAgFnPc1d1	tumorózní
změny	změna	k1gFnPc1	změna
podobné	podobný	k2eAgFnPc1d1	podobná
změnám	změna	k1gFnPc3	změna
při	při	k7c6	při
leukóze	leukóza	k1gFnSc6	leukóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
hus	husa	k1gFnPc2	husa
<g/>
,	,	kIx,	,
snovačovitých	snovačovitý	k2eAgFnPc2d1	snovačovitý
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Queleopsis	Queleopsis	k1gFnSc2	Queleopsis
<g/>
,	,	kIx,	,
Quelea	Queleum	k1gNnSc2	Queleum
a	a	k8xC	a
Euplectes	Euplectesa	k1gFnPc2	Euplectesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
amazoňanů	amazoňan	k1gMnPc2	amazoňan
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zjistit	zjistit	k5eAaPmF	zjistit
nekrotické	nekrotický	k2eAgFnPc1d1	nekrotická
nebo	nebo	k8xC	nebo
vředovité	vředovitý	k2eAgFnPc1d1	vředovitá
změny	změna	k1gFnPc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
skladba	skladba	k1gFnSc1	skladba
tuberkulózních	tuberkulózní	k2eAgInPc2d1	tuberkulózní
uzlíků	uzlík	k1gInPc2	uzlík
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
-	-	kIx~	-
centrální	centrální	k2eAgFnSc1d1	centrální
nekróza	nekróza	k1gFnSc1	nekróza
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
mnohojadernými	mnohojaderný	k2eAgFnPc7d1	mnohojaderná
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
vrstvou	vrstva	k1gFnSc7	vrstva
epiteloidních	epiteloidní	k2eAgFnPc2d1	epiteloidní
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
vnějším	vnější	k2eAgNnSc7d1	vnější
fibrózním	fibrózní	k2eAgNnSc7d1	fibrózní
pouzdrem	pouzdro	k1gNnSc7	pouzdro
tvořeným	tvořený	k2eAgNnSc7d1	tvořené
heterofily	heterofil	k1gMnPc7	heterofil
a	a	k8xC	a
mononukleárními	mononukleární	k2eAgFnPc7d1	mononukleární
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
mykobakteriálních	mykobakteriální	k2eAgFnPc2d1	mykobakteriální
infekcí	infekce	k1gFnPc2	infekce
má	mít	k5eAaImIp3nS	mít
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
buňkami	buňka	k1gFnPc7	buňka
zprostředkovaná	zprostředkovaný	k2eAgFnSc1d1	zprostředkovaná
imunita	imunita	k1gFnSc1	imunita
(	(	kIx(	(
<g/>
CMI	CMI	kA	CMI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
patogenezi	patogeneze	k1gFnSc6	patogeneze
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
;	;	kIx,	;
humorální	humorální	k2eAgFnSc1d1	humorální
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
významná	významný	k2eAgFnSc1d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
imunitní	imunitní	k2eAgFnSc2d1	imunitní
reakce	reakce	k1gFnSc2	reakce
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizovat	k5eAaBmNgFnP	charakterizovat
nahloučením	nahloučení	k1gNnSc7	nahloučení
makrofágů	makrofág	k1gInPc2	makrofág
<g/>
,	,	kIx,	,
fagocytózou	fagocytóza	k1gFnSc7	fagocytóza
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
hypersenzitivity	hypersenzitivita	k1gFnSc2	hypersenzitivita
opožděného	opožděný	k2eAgInSc2d1	opožděný
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Aktivované	aktivovaný	k2eAgInPc1d1	aktivovaný
makrofágy	makrofág	k1gInPc1	makrofág
mají	mít	k5eAaImIp3nP	mít
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
schopnost	schopnost	k1gFnSc4	schopnost
zabíjet	zabíjet	k5eAaImF	zabíjet
intracelulárně	intracelulárně	k6eAd1	intracelulárně
uložené	uložený	k2eAgInPc4d1	uložený
M.	M.	kA	M.
avium	avium	k1gInSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
centrální	centrální	k2eAgFnSc2d1	centrální
nekrózy	nekróza	k1gFnSc2	nekróza
v	v	k7c6	v
granulomech	granulom	k1gInPc6	granulom
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
cytotoxické	cytotoxický	k2eAgFnPc1d1	cytotoxická
buňky	buňka	k1gFnPc1	buňka
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
produkty	produkt	k1gInPc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
tuberkulinaci	tuberkulinace	k1gFnSc6	tuberkulinace
nebo	nebo	k8xC	nebo
sérologickém	sérologický	k2eAgNnSc6d1	sérologické
vyšetření	vyšetření	k1gNnSc6	vyšetření
a	a	k8xC	a
na	na	k7c6	na
pitevním	pitevní	k2eAgInSc6d1	pitevní
nálezu	nález	k1gInSc6	nález
doplněném	doplněný	k2eAgInSc6d1	doplněný
nálezem	nález	k1gInSc7	nález
acidorezistentních	acidorezistentní	k2eAgFnPc2d1	acidorezistentní
tyček	tyčka	k1gFnPc2	tyčka
v	v	k7c6	v
otiskových	otiskův	k2eAgInPc6d1	otiskův
preparátech	preparát	k1gInPc6	preparát
z	z	k7c2	z
makroskopicky	makroskopicky	k6eAd1	makroskopicky
postižených	postižený	k2eAgFnPc2d1	postižená
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	Konečná	k1gFnSc1	Konečná
diagnózy	diagnóza	k1gFnSc2	diagnóza
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
stanovována	stanovovat	k5eAaImNgFnS	stanovovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
bakteriologického	bakteriologický	k2eAgNnSc2d1	bakteriologické
vyšetření	vyšetření	k1gNnSc2	vyšetření
a	a	k8xC	a
identifikace	identifikace	k1gFnSc2	identifikace
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
intravitální	intravitální	k2eAgFnSc4d1	intravitální
diagnostiku	diagnostika	k1gFnSc4	diagnostika
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejčastěji	často	k6eAd3	často
používaná	používaný	k2eAgFnSc1d1	používaná
alergenodiagnostika	alergenodiagnostika	k1gFnSc1	alergenodiagnostika
(	(	kIx(	(
<g/>
tuberkulinace	tuberkulinace	k1gFnSc1	tuberkulinace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
proveditelnost	proveditelnost	k1gFnSc1	proveditelnost
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
získané	získaný	k2eAgInPc4d1	získaný
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
plošného	plošný	k2eAgNnSc2d1	plošné
vyšetření	vyšetření	k1gNnSc2	vyšetření
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
k	k	k7c3	k
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
zamořených	zamořený	k2eAgInPc2d1	zamořený
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
k	k	k7c3	k
eliminaci	eliminace	k1gFnSc3	eliminace
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
většího	veliký	k2eAgInSc2d2	veliký
významu	význam	k1gInSc2	význam
také	také	k9	také
nabývá	nabývat	k5eAaImIp3nS	nabývat
sérologické	sérologický	k2eAgNnSc1d1	sérologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
krve	krev	k1gFnSc2	krev
rychlou	rychlý	k2eAgFnSc7d1	rychlá
sklíčkovou	sklíčkový	k2eAgFnSc7d1	sklíčkový
aglutinací	aglutinace	k1gFnSc7	aglutinace
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
dávána	dáván	k2eAgFnSc1d1	dávána
přednost	přednost	k1gFnSc1	přednost
před	před	k7c7	před
tuberkulinací	tuberkulinace	k1gFnSc7	tuberkulinace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
vyšetření	vyšetření	k1gNnSc6	vyšetření
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Druhová	druhový	k2eAgFnSc1d1	druhová
identifikace	identifikace	k1gFnSc1	identifikace
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
průkazu	průkaz	k1gInSc6	průkaz
jejich	jejich	k3xOp3gMnPc2	jejich
charakteristických	charakteristický	k2eAgMnPc2d1	charakteristický
růstových	růstový	k2eAgMnPc2d1	růstový
<g/>
,	,	kIx,	,
morfologických	morfologický	k2eAgInPc2d1	morfologický
a	a	k8xC	a
metabolických	metabolický	k2eAgInPc2d1	metabolický
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
in	in	k?	in
vitro	vitro	k1gNnSc4	vitro
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
M.	M.	kA	M.
avium	avium	k1gInSc4	avium
jsou	být	k5eAaImIp3nP	být
komerčně	komerčně	k6eAd1	komerčně
vyráběny	vyráběn	k2eAgFnPc4d1	vyráběna
oligonukleotidové	oligonukleotidový	k2eAgFnPc4d1	oligonukleotidový
genetické	genetický	k2eAgFnPc4d1	genetická
sondy	sonda	k1gFnPc4	sonda
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
hybridizují	hybridizovat	k5eAaBmIp3nP	hybridizovat
se	s	k7c7	s
specifickými	specifický	k2eAgFnPc7d1	specifická
cílovými	cílový	k2eAgFnPc7d1	cílová
sekvencemi	sekvence	k1gFnPc7	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vysoké	vysoký	k2eAgFnSc2d1	vysoká
citlivosti	citlivost	k1gFnSc2	citlivost
i	i	k8xC	i
specifičnosti	specifičnost	k1gFnSc2	specifičnost
(	(	kIx(	(
<g/>
95	[number]	k4	95
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
zkracují	zkracovat	k5eAaImIp3nP	zkracovat
identifikaci	identifikace	k1gFnSc4	identifikace
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
do	do	k7c2	do
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
podobnosti	podobnost	k1gFnSc3	podobnost
patologických	patologický	k2eAgFnPc2d1	patologická
změn	změna	k1gFnPc2	změna
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišit	odlišit	k5eAaPmF	odlišit
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
(	(	kIx(	(
<g/>
tyf	tyf	k1gInSc1	tyf
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
paratyfové	paratyfový	k2eAgFnSc2d1	paratyfový
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
arizonóza	arizonóza	k1gFnSc1	arizonóza
<g/>
,	,	kIx,	,
kolibacilóza	kolibacilóza	k1gFnSc1	kolibacilóza
<g/>
,	,	kIx,	,
ptačí	ptačí	k2eAgFnSc1d1	ptačí
cholera	cholera	k1gFnSc1	cholera
a	a	k8xC	a
chlamydióza	chlamydióza	k1gFnSc1	chlamydióza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plísňových	plísňový	k2eAgInPc2d1	plísňový
(	(	kIx(	(
<g/>
aspergilóza	aspergilóza	k1gFnSc1	aspergilóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
nádorových	nádorový	k2eAgFnPc2d1	nádorová
změn	změna	k1gFnPc2	změna
(	(	kIx(	(
<g/>
lymfoifní	lymfoifní	k2eAgFnSc1d1	lymfoifní
leukóza	leukóza	k1gFnSc1	leukóza
a	a	k8xC	a
Markova	Markův	k2eAgFnSc1d1	Markova
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
rozšíření	rozšíření	k1gNnSc3	rozšíření
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc3d1	vysoká
incidenci	incidence	k1gFnSc3	incidence
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
významu	význam	k1gInSc2	význam
pro	pro	k7c4	pro
drůbežnictví	drůbežnictví	k1gNnSc4	drůbežnictví
a	a	k8xC	a
chovy	chov	k1gInPc1	chov
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
také	také	k9	také
ke	k	k7c3	k
stoupajícímu	stoupající	k2eAgInSc3d1	stoupající
výskytu	výskyt	k1gInSc2	výskyt
mykobakterióz	mykobakterióza	k1gFnPc2	mykobakterióza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
nezbytnost	nezbytnost	k1gFnSc4	nezbytnost
kontroly	kontrola	k1gFnSc2	kontrola
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
eradikace	eradikace	k1gFnSc2	eradikace
<g/>
.	.	kIx.	.
</s>
<s>
Léčení	léčení	k1gNnSc1	léčení
tuberkulózní	tuberkulózní	k2eAgFnSc2d1	tuberkulózní
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nP	provádět
a	a	k8xC	a
postižení	postižený	k2eAgMnPc1d1	postižený
ptáci	pták	k1gMnPc1	pták
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
utraceni	utracen	k2eAgMnPc1d1	utracen
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
dodržování	dodržování	k1gNnSc4	dodržování
technologických	technologický	k2eAgInPc2d1	technologický
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
turnusovém	turnusový	k2eAgInSc6d1	turnusový
způsobu	způsob	k1gInSc6	způsob
chovu	chov	k1gInSc2	chov
<g/>
,	,	kIx,	,
zabránění	zabránění	k1gNnSc2	zabránění
volnému	volný	k2eAgInSc3d1	volný
přístupu	přístup	k1gInSc3	přístup
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
do	do	k7c2	do
výrobních	výrobní	k2eAgInPc2d1	výrobní
prostorů	prostor	k1gInPc2	prostor
a	a	k8xC	a
v	v	k7c6	v
kontrolním	kontrolní	k2eAgMnSc6d1	kontrolní
alergickém	alergický	k2eAgMnSc6d1	alergický
anebo	anebo	k8xC	anebo
sérologickém	sérologický	k2eAgNnSc6d1	sérologické
vyšetření	vyšetření	k1gNnSc6	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dezinfekčních	dezinfekční	k2eAgInPc2d1	dezinfekční
prostředků	prostředek	k1gInPc2	prostředek
jsou	být	k5eAaImIp3nP	být
nejvhodnější	vhodný	k2eAgInPc1d3	nejvhodnější
2,5	[number]	k4	2,5
%	%	kIx~	%
aktivovaný	aktivovaný	k2eAgInSc4d1	aktivovaný
chloramin	chloramin	k1gInSc4	chloramin
<g/>
,	,	kIx,	,
krezolové	krezolový	k2eAgInPc4d1	krezolový
přípravky	přípravek	k1gInPc4	přípravek
a	a	k8xC	a
2-4	[number]	k4	2-4
%	%	kIx~	%
formalin	formalin	k1gInSc1	formalin
<g/>
.	.	kIx.	.
</s>
