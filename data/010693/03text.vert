<p>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
physical	physicat	k5eAaPmAgInS	physicat
layer	layer	k1gInSc1	layer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
vrstva	vrstva	k1gFnSc1	vrstva
modelu	model	k1gInSc2	model
vrstvové	vrstvový	k2eAgFnSc2d1	vrstvová
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
OSI	OSI	kA	OSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
převod	převod	k1gInSc4	převod
proudu	proud	k1gInSc2	proud
bitů	bit	k1gInPc2	bit
na	na	k7c4	na
signál	signál	k1gInSc4	signál
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
elektrický	elektrický	k2eAgInSc1d1	elektrický
<g/>
)	)	kIx)	)
a	a	k8xC	a
opačný	opačný	k2eAgInSc1d1	opačný
převod	převod	k1gInSc1	převod
ze	z	k7c2	z
signálu	signál	k1gInSc2	signál
na	na	k7c4	na
proud	proud	k1gInSc4	proud
bitů	bit	k1gInPc2	bit
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
aktivaci	aktivace	k1gFnSc4	aktivace
<g/>
,	,	kIx,	,
udržování	udržování	k1gNnSc4	udržování
a	a	k8xC	a
rušení	rušení	k1gNnSc4	rušení
fyzického	fyzický	k2eAgInSc2d1	fyzický
spoje	spoj	k1gInSc2	spoj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
komutovaného	komutovaný	k2eAgNnSc2d1	komutované
spojení	spojení	k1gNnSc2	spojení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bitový	bitový	k2eAgInSc1d1	bitový
tok	tok	k1gInSc1	tok
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
před	před	k7c7	před
převodem	převod	k1gInSc7	převod
na	na	k7c4	na
fyzický	fyzický	k2eAgInSc4d1	fyzický
signál	signál	k1gInSc4	signál
seskupený	seskupený	k2eAgInSc4d1	seskupený
do	do	k7c2	do
kódových	kódový	k2eAgNnPc2d1	kódové
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protokoly	protokol	k1gInPc1	protokol
fyzické	fyzický	k2eAgFnSc2d1	fyzická
vrstvy	vrstva	k1gFnSc2	vrstva
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
elektrické	elektrický	k2eAgFnPc1d1	elektrická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
rozhraní	rozhraní	k1gNnSc2	rozhraní
(	(	kIx(	(
<g/>
napěťové	napěťový	k2eAgFnSc2d1	napěťová
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
průběhy	průběh	k1gInPc4	průběh
<g/>
,	,	kIx,	,
kmitočty	kmitočet	k1gInPc4	kmitočet
<g/>
,	,	kIx,	,
modulace	modulace	k1gFnPc4	modulace
<g/>
,	,	kIx,	,
rychlosti	rychlost	k1gFnPc4	rychlost
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnPc4d1	elektrická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kabelů	kabel	k1gInPc2	kabel
<g/>
)	)	kIx)	)
a	a	k8xC	a
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnPc4	velikost
a	a	k8xC	a
zapojení	zapojení	k1gNnSc6	zapojení
konektorů	konektor	k1gInPc2	konektor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
fyzické	fyzický	k2eAgFnSc2d1	fyzická
vrstvy	vrstva	k1gFnSc2	vrstva
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
signálem	signál	k1gInSc7	signál
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
její	její	k3xOp3gFnSc1	její
realizace	realizace	k1gFnSc1	realizace
hardwarová	hardwarový	k2eAgFnSc1d1	hardwarová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
fyzické	fyzický	k2eAgFnPc1d1	fyzická
vrstvy	vrstva	k1gFnPc1	vrstva
fungují	fungovat	k5eAaImIp3nP	fungovat
nejstarší	starý	k2eAgInPc4d3	nejstarší
modemy	modem	k1gInPc4	modem
<g/>
,	,	kIx,	,
huby	huba	k1gFnPc4	huba
<g/>
,	,	kIx,	,
opakovače	opakovač	k1gInPc4	opakovač
(	(	kIx(	(
<g/>
repeater	repeater	k1gMnSc1	repeater
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
síťové	síťový	k2eAgInPc4d1	síťový
adaptéry	adaptér	k1gInPc4	adaptér
a	a	k8xC	a
Hostitelské	hostitelský	k2eAgInPc4d1	hostitelský
adaptéry	adaptér	k1gInPc4	adaptér
(	(	kIx(	(
<g/>
Host	host	k1gMnSc1	host
Bus	bus	k1gInSc4	bus
Adapters	Adaptersa	k1gFnPc2	Adaptersa
používané	používaný	k2eAgFnPc4d1	používaná
v	v	k7c6	v
síťových	síťový	k2eAgNnPc6d1	síťové
úložištích	úložiště	k1gNnPc6	úložiště
NAS	NAS	kA	NAS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc4d2	novější
modemy	modem	k1gInPc4	modem
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
síťových	síťový	k2eAgFnPc2d1	síťová
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
realizují	realizovat	k5eAaBmIp3nP	realizovat
i	i	k8xC	i
úkoly	úkol	k1gInPc4	úkol
linkové	linkový	k2eAgFnSc2d1	Linková
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přísné	přísný	k2eAgFnSc6d1	přísná
interpretaci	interpretace	k1gFnSc6	interpretace
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
OSI	OSI	kA	OSI
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
fyzické	fyzický	k2eAgNnSc4d1	fyzické
médium	médium	k1gNnSc4	médium
do	do	k7c2	do
fyzické	fyzický	k2eAgFnSc2d1	fyzická
vrstvy	vrstva	k1gFnSc2	vrstva
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc4d1	významná
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
fyzické	fyzický	k2eAgFnSc2d1	fyzická
vrstvy	vrstva	k1gFnSc2	vrstva
==	==	k?	==
</s>
</p>
<p>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
standardizované	standardizovaný	k2eAgNnSc1d1	standardizované
rozhraní	rozhraní	k1gNnSc1	rozhraní
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
přenosovému	přenosový	k2eAgNnSc3d1	přenosové
mediu	medium	k1gNnSc3	medium
včetně	včetně	k7c2	včetně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mechanické	mechanický	k2eAgFnPc1d1	mechanická
specifikace	specifikace	k1gFnPc1	specifikace
elektrických	elektrický	k2eAgInPc2d1	elektrický
konektorů	konektor	k1gInPc2	konektor
a	a	k8xC	a
kabelů	kabel	k1gInPc2	kabel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
kabelu	kabel	k1gInSc2	kabel
</s>
</p>
<p>
<s>
Elektrické	elektrický	k2eAgFnSc2d1	elektrická
specifikace	specifikace	k1gFnSc2	specifikace
úrovně	úroveň	k1gFnSc2	úroveň
signálu	signál	k1gInSc2	signál
a	a	k8xC	a
impedance	impedance	k1gFnSc2	impedance
</s>
</p>
<p>
<s>
Radiové	radiový	k2eAgNnSc1d1	radiové
rozhraní	rozhraní	k1gNnSc1	rozhraní
včetně	včetně	k7c2	včetně
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
specifikace	specifikace	k1gFnSc2	specifikace
intenzity	intenzita	k1gFnSc2	intenzita
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
analogová	analogový	k2eAgFnSc1d1	analogová
šířka	šířka	k1gFnSc1	šířka
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Specifikace	specifikace	k1gFnSc1	specifikace
pro	pro	k7c4	pro
IR	Ir	k1gMnSc1	Ir
přes	přes	k7c4	přes
optické	optický	k2eAgNnSc4d1	optické
vlákno	vlákno	k1gNnSc4	vlákno
nebo	nebo	k8xC	nebo
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
IR	Ir	k1gMnSc1	Ir
komunikační	komunikační	k2eAgFnSc4d1	komunikační
linku	linka	k1gFnSc4	linka
</s>
</p>
<p>
<s>
Modulace	modulace	k1gFnSc1	modulace
</s>
</p>
<p>
<s>
Lineární	lineární	k2eAgNnSc1d1	lineární
kódování	kódování	k1gNnSc1	kódování
</s>
</p>
<p>
<s>
Bitová	bitový	k2eAgFnSc1d1	bitová
synchronizace	synchronizace	k1gFnSc1	synchronizace
v	v	k7c6	v
synchronní	synchronní	k2eAgFnSc6d1	synchronní
sériové	sériový	k2eAgFnSc6d1	sériová
komunikaci	komunikace	k1gFnSc6	komunikace
</s>
</p>
<p>
<s>
Regulace	regulace	k1gFnSc1	regulace
průtoku	průtok	k1gInSc2	průtok
v	v	k7c6	v
asynchronní	asynchronní	k2eAgFnSc6d1	asynchronní
sériové	sériový	k2eAgFnSc6d1	sériová
komunikaci	komunikace	k1gFnSc6	komunikace
</s>
</p>
<p>
<s>
Circuit	Circuit	k2eAgInSc1d1	Circuit
mód	mód	k1gInSc1	mód
multiplexování	multiplexování	k1gNnSc2	multiplexování
</s>
</p>
<p>
<s>
Carrier	Carrier	k1gInSc1	Carrier
Sense	Sense	k1gFnSc2	Sense
pro	pro	k7c4	pro
CSMA	CSMA	kA	CSMA
<g/>
/	/	kIx~	/
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
samotné	samotný	k2eAgNnSc1d1	samotné
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
vrstvy	vrstva	k1gFnSc2	vrstva
linkové	linkový	k2eAgFnSc2d1	Linková
</s>
</p>
<p>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
signálu	signál	k1gInSc2	signál
</s>
</p>
<p>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
sériová	sériový	k2eAgFnSc1d1	sériová
nebo	nebo	k8xC	nebo
paralelní	paralelní	k2eAgFnSc1d1	paralelní
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
fyzická	fyzický	k2eAgFnSc1d1	fyzická
vrstva	vrstva	k1gFnSc1	vrstva
realizovaná	realizovaný	k2eAgFnSc1d1	realizovaná
modemy	modem	k1gInPc4	modem
<g/>
:	:	kIx,	:
V	v	k7c4	v
<g/>
.21	.21	k4	.21
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
.22	.22	k4	.22
<g/>
,	,	kIx,	,
V	v	k7c6	v
<g/>
.22	.22	k4	.22
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
V	v	k7c6	v
<g/>
.26	.26	k4	.26
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
V	v	k7c6	v
<g/>
.27	.27	k4	.27
<g/>
ter	ter	k?	ter
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
.32	.32	k4	.32
<g/>
,	,	kIx,	,
V	v	k7c6	v
<g/>
.32	.32	k4	.32
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
.34	.34	k4	.34
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
.90	.90	k4	.90
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
.92	.92	k4	.92
<g/>
,	,	kIx,	,
V	v	k7c6	v
<g/>
.44	.44	k4	.44
</s>
</p>
<p>
<s>
EIA	EIA	kA	EIA
standardy	standard	k1gInPc1	standard
<g/>
:	:	kIx,	:
RS-	RS-	k1gFnSc1	RS-
<g/>
232	[number]	k4	232
<g/>
,	,	kIx,	,
RS-	RS-	k1gFnSc1	RS-
<g/>
422	[number]	k4	422
<g/>
,	,	kIx,	,
RS-	RS-	k1gFnSc1	RS-
<g/>
423	[number]	k4	423
<g/>
,	,	kIx,	,
RS-	RS-	k1gFnSc1	RS-
<g/>
449	[number]	k4	449
<g/>
,	,	kIx,	,
RS-485	RS-485	k1gFnSc1	RS-485
</s>
</p>
<p>
<s>
ISDN	ISDN	kA	ISDN
</s>
</p>
<p>
<s>
DSL	DSL	kA	DSL
</s>
</p>
<p>
<s>
T	T	kA	T
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
E1	E1	k1gFnSc1	E1
</s>
</p>
<p>
<s>
SDH	SDH	kA	SDH
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
BASE-T	BASE-T	k1gFnPc2	BASE-T
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
BASE	bas	k1gInSc6	bas
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
BASE	bas	k1gInSc6	bas
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
BASE-TX	BASE-TX	k1gFnPc2	BASE-TX
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
BASE-FX	BASE-FX	k1gFnPc2	BASE-FX
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
BASE-T	BASE-T	k1gFnPc2	BASE-T
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
<g/>
BASE-T	BASE-T	k1gFnPc2	BASE-T
<g/>
,	,	kIx,	,
1000BASE-SX	[number]	k4	1000BASE-SX
</s>
</p>
<p>
<s>
Bluetooth	Bluetooth	k1gMnSc1	Bluetooth
</s>
</p>
<p>
<s>
fyzické	fyzický	k2eAgFnPc1d1	fyzická
vrstvy	vrstva	k1gFnPc1	vrstva
IEEE	IEEE	kA	IEEE
802.11	[number]	k4	802.11
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
</s>
</p>
<p>
<s>
FireWire	FireWir	k1gMnSc5	FireWir
</s>
</p>
<p>
<s>
fyzická	fyzický	k2eAgFnSc1d1	fyzická
vrstva	vrstva	k1gFnSc1	vrstva
IRDA	IRDA	kA	IRDA
<g/>
#	#	kIx~	#
<g/>
IrPHY	IrPHY	k1gMnSc1	IrPHY
</s>
</p>
<p>
<s>
fyzická	fyzický	k2eAgFnSc1d1	fyzická
vrstva	vrstva	k1gFnSc1	vrstva
USB	USB	kA	USB
</s>
</p>
<p>
<s>
G.	G.	kA	G.
<g/>
hn	hn	k?	hn
</s>
</p>
<p>
<s>
CAN	CAN	kA	CAN
bus	bus	k1gInSc1	bus
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
hardwaru	hardware	k1gInSc2	hardware
==	==	k?	==
</s>
</p>
<p>
<s>
Repeater	Repeater	k1gMnSc1	Repeater
</s>
</p>
<p>
<s>
Hub	houba	k1gFnPc2	houba
</s>
</p>
<p>
<s>
Modem	modem	k1gInSc1	modem
</s>
</p>
<p>
<s>
Síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Referenční	referenční	k2eAgInSc1d1	referenční
model	model	k1gInSc1	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
</s>
</p>
