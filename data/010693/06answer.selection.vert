<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
physical	physicat	k5eAaPmAgInS	physicat
layer	layer	k1gInSc1	layer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
vrstva	vrstva	k1gFnSc1	vrstva
modelu	model	k1gInSc2	model
vrstvové	vrstvový	k2eAgFnSc2d1	vrstvová
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
OSI	OSI	kA	OSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
