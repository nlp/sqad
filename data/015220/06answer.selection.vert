<s desamb="1">
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
a	a	k8xC
svoje	svůj	k3xOyFgNnPc4
domácí	domácí	k2eAgNnPc4d1
utkání	utkání	k1gNnPc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c6
stadionu	stadion	k1gInSc6
Kâmil	Kâmila	k1gFnPc2
Ocak	Ocak	k1gInSc1
Stadyumu	Stadyum	k1gInSc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
16	#num#	k4
981	#num#	k4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
hraje	hrát	k5eAaImIp3nS
své	svůj	k3xOyFgMnPc4
domácí	domácí	k2eAgMnPc4d1
zápasy	zápas	k1gInPc4
i	i	k8xC
rival	rival	k1gMnSc1
Gaziantep	Gaziantep	k1gMnSc1
Büyükşehir	Büyükşehir	k1gMnSc1
Belediyespor	Belediyespor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Klubové	klubový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
byly	být	k5eAaImAgInP
červená	červený	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>