<s>
Gaziantepspor	Gaziantepspor	k1gMnSc1
</s>
<s>
Gaziantepspor	Gaziantepspor	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Şahinler	Şahinler	k1gInSc1
(	(	kIx(
<g/>
sokoli	sokol	k1gMnPc1
<g/>
)	)	kIx)
Země	země	k1gFnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Gaziantep	Gaziantep	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1969	#num#	k4
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zánik	zánik	k1gInSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_antep	_antep	k1gInSc1
<g/>
1213	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Süper	Süper	k1gInSc4
Lig	liga	k1gFnPc2
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Kâmil	Kâmil	k1gInSc1
Ocak	Ocaka	k1gFnPc2
Stadyumu	Stadyum	k1gInSc2
<g/>
,	,	kIx,
C.	C.	kA
Dogan	Dogan	k1gInSc1
Tes	tes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Şehir	Şehira	k1gFnPc2
Gösteren	Gösterna	k1gFnPc2
Villaları	Villaları	k1gFnSc2
a	a	k8xC
</s>
<s>
27060	#num#	k4
Şehitkamil	Şehitkamil	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Gaziantep	Gaziantep	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
3	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
37	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
16	#num#	k4
981	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gaziantepspor	Gaziantepspor	k1gInSc1
byl	být	k5eAaImAgInS
turecký	turecký	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
města	město	k1gNnSc2
Gaziantep	Gaziantep	k1gInSc1
nedaleko	nedaleko	k7c2
hranice	hranice	k1gFnSc2
se	s	k7c7
Sýrií	Sýrie	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
působil	působit	k5eAaImAgMnS
v	v	k7c4
Süper	Süper	k1gInSc4
Lig	liga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
a	a	k8xC
svoje	svůj	k3xOyFgNnPc4
domácí	domácí	k2eAgNnPc4d1
utkání	utkání	k1gNnPc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c6
stadionu	stadion	k1gInSc6
Kâmil	Kâmila	k1gFnPc2
Ocak	Ocak	k1gInSc1
Stadyumu	Stadyum	k1gInSc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
16	#num#	k4
981	#num#	k4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
hraje	hrát	k5eAaImIp3nS
své	svůj	k3xOyFgMnPc4
domácí	domácí	k2eAgMnPc4d1
zápasy	zápas	k1gInPc4
i	i	k8xC
rival	rival	k1gMnSc1
Gaziantep	Gaziantep	k1gMnSc1
Büyükşehir	Büyükşehir	k1gMnSc1
Belediyespor	Belediyespor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Klubové	klubový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
byly	být	k5eAaImAgInP
červená	červený	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Prezidenti	prezident	k1gMnPc1
klubu	klub	k1gInSc2
</s>
<s>
Beşir	Beşir	k1gInSc1
Bayram	Bayram	k1gInSc1
</s>
<s>
Ömer	Ömer	k1gInSc1
Köylüoğ	Köylüoğ	k1gInSc2
</s>
<s>
Hasan	Hasan	k1gMnSc1
Nehir	Nehir	k1gMnSc1
</s>
<s>
Selahattin	Selahattin	k1gMnSc1
Öztahtacı	Öztahtacı	k1gMnSc1
</s>
<s>
Halit	halit	k1gInSc1
Güleç	Güleç	k1gFnSc2
</s>
<s>
Muhittin	Muhittin	k2eAgInSc1d1
Göymen	Göymen	k1gInSc1
</s>
<s>
Hayri	Hayri	k6eAd1
Tütüncüler	Tütüncüler	k1gInSc1
</s>
<s>
M.	M.	kA
Saip	Saip	k1gMnSc1
Konukoğ	Konukoğ	k1gInSc2
</s>
<s>
Ata	Ata	k?
Aksu	Aksa	k1gFnSc4
</s>
<s>
Halil	halit	k5eAaImAgMnS
Kı	Kı	k1gMnSc1
</s>
<s>
Ali	Ali	k?
İ	İ	k1gMnSc1
Gögüş	Gögüş	k1gMnSc1
</s>
<s>
Mehmet	Mehmet	k1gMnSc1
Batallı	Batallı	k1gMnSc1
</s>
<s>
Bektaş	Bektaş	k?
Göçmen	Göçmen	k1gInSc1
</s>
<s>
Sadettin	Sadettin	k2eAgInSc1d1
Ergün	Ergün	k1gInSc1
</s>
<s>
Selahattin	Selahattin	k2eAgInSc1d1
Demirat	Demirat	k1gInSc1
</s>
<s>
Ömer	Ömer	k1gInSc1
Arpacı	Arpacı	k1gInSc2
</s>
<s>
Ali	Ali	k?
Şahindal	Şahindal	k1gFnSc1
</s>
<s>
Abdulkadir	Abdulkadir	k1gInSc1
Konukoğ	Konukoğ	k1gInSc2
</s>
<s>
Ahmet	Ahmet	k1gInSc1
Yı	Yı	k1gInSc1
</s>
<s>
Celal	Celal	k1gMnSc1
Doğ	Doğ	k1gMnSc1
</s>
<s>
İ	İ	k1gMnSc1
Kı	Kı	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Gaziantepspor	Gaziantepspora	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
(	(	kIx(
<g/>
data	datum	k1gNnSc2
and	and	k?
facts	factsa	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c4
transfermarkt	transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Kâmil	Kâmil	k1gMnSc1
Ocak	Ocak	k1gMnSc1
<g/>
,	,	kIx,
tff	tff	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
tr	tr	k?
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Stadien	Stadien	k1gInSc1
Türkei	Türke	k1gFnSc2
<g/>
,	,	kIx,
stadionwelt	stadionwelt	k1gMnSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gaziantepspor	Gaziantepspora	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
na	na	k7c4
transfermarkt	transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
156952629	#num#	k4
</s>
