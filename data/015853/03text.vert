<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
</s>
<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
COSPAR	COSPAR	kA
</s>
<s>
2010-013A	2010-013A	k4
Katalogové	katalogový	k2eAgFnPc4d1
číslo	číslo	k1gNnSc1
</s>
<s>
36508	#num#	k4
Start	start	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2010	#num#	k4
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Baikonur	Baikonur	k1gMnSc1
Cosmodrome	Cosmodrom	k1gInSc5
Site	Site	k1gFnPc2
109	#num#	k4
Nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
Dněpr	Dněpr	k1gInSc1
Typ	typ	k1gInSc1
oběžné	oběžný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
</s>
<s>
nízká	nízký	k2eAgFnSc1d1
oběžná	oběžný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Země	zem	k1gFnSc2
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
kosmická	kosmický	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Astrium	Astrium	k1gNnSc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
750	#num#	k4
kg	kg	kA
Parametry	parametr	k1gInPc1
dráhy	dráha	k1gFnSc2
Apogeum	apogeum	k1gNnSc1
</s>
<s>
732	#num#	k4
km	km	kA
Sklon	sklon	k1gInSc4
dráhy	dráha	k1gFnSc2
</s>
<s>
92	#num#	k4
°	°	k?
Doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
</s>
<s>
99,2	99,2	k4
min	mina	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
během	během	k7c2
testování	testování	k1gNnSc2
</s>
<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
sonda	sonda	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
kosmické	kosmický	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
určená	určený	k2eAgFnSc1d1
k	k	k7c3
výzkumu	výzkum	k1gInSc3
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstartovala	odstartovat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poskytuje	poskytovat	k5eAaImIp3nS
vědcům	vědec	k1gMnPc3
údaje	údaj	k1gInPc4
o	o	k7c6
polárních	polární	k2eAgInPc6d1
ledovcích	ledovec	k1gInPc6
a	a	k8xC
sleduje	sledovat	k5eAaImIp3nS
změnu	změna	k1gFnSc4
tloušťky	tloušťka	k1gFnSc2
ledu	led	k1gInSc2
s	s	k7c7
přesností	přesnost	k1gFnSc7
až	až	k9
1,3	1,3	k4
centimetru	centimetr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
náhrada	náhrada	k1gFnSc1
za	za	k7c4
CryoSat-	CryoSat-	k1gFnSc4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
Rokot	rokot	k1gInSc4
havarovala	havarovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
předchůdcem	předchůdce	k1gMnSc7
má	mít	k5eAaImIp3nS
CryoSat-	CryoSat-	k1gMnSc1
<g/>
2	#num#	k4
aktuálnější	aktuální	k2eAgInSc4d2
software	software	k1gInSc4
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc4d2
kapacitu	kapacita	k1gFnSc4
baterie	baterie	k1gFnPc1
a	a	k8xC
modernější	moderní	k2eAgInPc1d2
přístroje	přístroj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
přístrojem	přístroj	k1gInSc7
je	být	k5eAaImIp3nS
interferometrický	interferometrický	k2eAgInSc1d1
radarový	radarový	k2eAgInSc1d1
dálkoměr	dálkoměr	k1gInSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
anténami	anténa	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
měří	měřit	k5eAaImIp3nP
rozdíl	rozdíl	k1gInSc4
výšky	výška	k1gFnSc2
plovoucího	plovoucí	k2eAgInSc2d1
ledu	led	k1gInSc2
a	a	k8xC
otevřené	otevřený	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
programu	program	k1gInSc2
CryoSat	CryoSat	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
studovat	studovat	k5eAaImF
zemské	zemský	k2eAgFnPc4d1
polární	polární	k2eAgFnPc4d1
čepičky	čepička	k1gFnPc4
a	a	k8xC
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
součástí	součást	k1gFnSc7
projektu	projekt	k1gInSc2
Living	Living	k1gInSc1
Planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Satelit	satelit	k1gInSc4
postavila	postavit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Astrium	Astrium	k1gNnSc4
<g/>
,	,	kIx,
start	start	k1gInSc4
proběhl	proběhnout	k5eAaPmAgInS
na	na	k7c6
nosné	nosný	k2eAgFnSc6d1
raketě	raketa	k1gFnSc6
Dněpr	Dněpr	k1gInSc4
v	v	k7c6
dubnu	duben	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Původní	původní	k2eAgInSc1d1
návrh	návrh	k1gInSc1
na	na	k7c4
CryoSat	CryoSat	k1gInSc4
byl	být	k5eAaImAgInS
předložen	předložit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
Living	Living	k1gInSc1
Planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc1
byl	být	k5eAaImAgInS
vybrán	vybrat	k5eAaPmNgInS
pro	pro	k7c4
další	další	k2eAgNnSc4d1
studium	studium	k1gNnSc4
roku	rok	k1gInSc2
1999	#num#	k4
a	a	k8xC
po	po	k7c6
dokončení	dokončení	k1gNnSc6
studie	studie	k1gFnSc2
proveditelnosti	proveditelnost	k1gFnSc2
byla	být	k5eAaImAgFnS
mise	mise	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
povolena	povolit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
získala	získat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Astrium	Astrium	k1gNnSc1
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
stavbu	stavba	k1gFnSc4
satelitu	satelit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
byla	být	k5eAaImAgFnS
rovněž	rovněž	k9
podepsána	podepsán	k2eAgFnSc1d1
s	s	k7c7
provozovatelem	provozovatel	k1gMnSc7
nosných	nosný	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
Rokot	rokot	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
původní	původní	k2eAgFnSc2d1
družice	družice	k1gFnSc2
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
byla	být	k5eAaImAgFnS
sonda	sonda	k1gFnSc1
odeslána	odeslat	k5eAaPmNgFnS
na	na	k7c4
kosmodrom	kosmodrom	k1gInSc4
a	a	k8xC
v	v	k7c6
říjnu	říjen	k1gInSc6
2005	#num#	k4
proběhl	proběhnout	k5eAaPmAgInS
start	start	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
chybě	chyba	k1gFnSc3
v	v	k7c6
řízení	řízení	k1gNnSc6
rakety	raketa	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
z	z	k7c2
důvodu	důvod	k1gInSc2
špatného	špatný	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
stupňů	stupeň	k1gInPc2
nedosáhla	dosáhnout	k5eNaPmAgFnS
oběžné	oběžný	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
a	a	k8xC
sonda	sonda	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
ztracena	ztratit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
významu	význam	k1gInSc3
satelitu	satelit	k1gInSc2
pro	pro	k7c4
pochopení	pochopení	k1gNnSc4
globálních	globální	k2eAgFnPc2d1
změn	změna	k1gFnPc2
klimatu	klima	k1gNnSc2
a	a	k8xC
úbytku	úbytek	k1gInSc2
ledovců	ledovec	k1gInPc2
bylo	být	k5eAaImAgNnS
navrženo	navržen	k2eAgNnSc1d1
postavit	postavit	k5eAaPmF
náhradní	náhradní	k2eAgInSc4d1
satelit	satelit	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
schváleno	schválit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
CryoSat-	CryoSat-	k1gFnSc1
<g/>
1	#num#	k4
i	i	k8xC
CryoSat-	CryoSat-	k1gFnSc1
<g/>
2	#num#	k4
postavila	postavit	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Asterium	Asterium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Konstrukce	konstrukce	k1gFnSc1
a	a	k8xC
testování	testování	k1gNnSc1
sondy	sonda	k1gFnSc2
bylo	být	k5eAaImAgNnS
dokončeno	dokončit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
sonda	sonda	k1gFnSc1
připravena	připraven	k2eAgFnSc1d1
k	k	k7c3
letu	let	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Vzhledově	vzhledově	k6eAd1
je	být	k5eAaImIp3nS
CryoSat-	CryoSat-	k1gFnSc1
<g/>
2	#num#	k4
téměř	téměř	k6eAd1
přesnou	přesný	k2eAgFnSc7d1
kopií	kopie	k1gFnSc7
původní	původní	k2eAgFnSc2d1
sondy	sonda	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
ale	ale	k9
provedeny	provést	k5eAaPmNgFnP
některé	některý	k3yIgFnPc1
změny	změna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
byl	být	k5eAaImAgInS
přidán	přidán	k2eAgInSc1d1
záložní	záložní	k2eAgInSc1d1
výškoměr	výškoměr	k1gInSc1
a	a	k8xC
dále	daleko	k6eAd2
bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
85	#num#	k4
různých	různý	k2eAgNnPc2d1
zlepšení	zlepšení	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podpůrná	podpůrný	k2eAgNnPc1d1
měření	měření	k1gNnPc1
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
projektu	projekt	k1gInSc2
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
nutné	nutný	k2eAgNnSc1d1
provést	provést	k5eAaPmF
sérii	série	k1gFnSc4
měření	měření	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zřejmé	zřejmý	k2eAgNnSc1d1
jak	jak	k8xC,k8xS
probíhá	probíhat	k5eAaImIp3nS
interakce	interakce	k1gFnSc1
radarových	radarový	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
s	s	k7c7
ledovci	ledovec	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
prozkoumat	prozkoumat	k5eAaPmF
chování	chování	k1gNnSc4
plovoucích	plovoucí	k2eAgInPc2d1
ledovců	ledovec	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
zatíženy	zatížit	k5eAaPmNgInP
sněhem	sníh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
plovoucích	plovoucí	k2eAgInPc2d1
ledovců	ledovec	k1gInPc2
bylo	být	k5eAaImAgNnS
rovněž	rovněž	k9
potřeba	potřeba	k6eAd1
vyvinout	vyvinout	k5eAaPmF
techniky	technika	k1gFnSc2
umožňující	umožňující	k2eAgNnPc4d1
přesná	přesný	k2eAgNnPc4d1
měření	měření	k1gNnPc4
z	z	k7c2
ledovců	ledovec	k1gInPc2
pohybujících	pohybující	k2eAgInPc2d1
se	se	k3xPyFc4
různými	různý	k2eAgInPc7d1
směry	směr	k1gInPc7
a	a	k8xC
rychlostmi	rychlost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byly	být	k5eAaImAgFnP
na	na	k7c6
Antarktidě	Antarktida	k1gFnSc6
provedeny	proveden	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
následně	následně	k6eAd1
posloužily	posloužit	k5eAaPmAgInP
ke	k	k7c3
kalibraci	kalibrace	k1gFnSc3
satelitu	satelit	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
kalibrační	kalibrační	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
následovaly	následovat	k5eAaImAgInP
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
na	na	k7c6
Špicberkách	Špicberky	k1gInPc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
v	v	k7c6
Grónsku	Grónsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finální	finální	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
schválení	schválení	k1gNnSc6
mise	mise	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byl	být	k5eAaImAgInS
start	start	k1gInSc1
naplánován	naplánovat	k5eAaBmNgInS
na	na	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
a	a	k8xC
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
CryoSat-	CryoSat-	k1gFnSc2
<g/>
2	#num#	k4
pletí	pleť	k1gFnPc2
na	na	k7c6
raketě	raketa	k1gFnSc6
Rokot	rokot	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
nedostatku	nedostatek	k1gInSc2
těchto	tento	k3xDgFnPc2
raket	raketa	k1gFnPc2
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
vybrána	vybrat	k5eAaPmNgFnS
nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
Dněpr	Dněpr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
zpoždění	zpoždění	k1gNnSc3
předchozích	předchozí	k2eAgFnPc2d1
misí	mise	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
odkladu	odklad	k1gInSc3
startu	start	k1gInSc2
na	na	k7c4
rok	rok	k1gInSc4
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2009	#num#	k4
dorazila	dorazit	k5eAaPmAgFnS
na	na	k7c4
kosmodrom	kosmodrom	k1gInSc4
raketa	raketa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
ledna	leden	k1gInSc2
2010	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
špici	špice	k1gFnSc6
rakety	raketa	k1gFnSc2
umístěna	umístěn	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
a	a	k8xC
raketa	raketa	k1gFnSc1
byla	být	k5eAaImAgFnS
přesunuta	přesunout	k5eAaPmNgFnS
na	na	k7c4
místo	místo	k1gNnSc4
startu	start	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
závěrečného	závěrečný	k2eAgNnSc2d1
testování	testování	k1gNnSc2
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
hlavní	hlavní	k2eAgFnSc1d1
komunikační	komunikační	k2eAgFnSc1d1
anténa	anténa	k1gFnSc1
vysílá	vysílat	k5eAaImIp3nS
jen	jen	k9
nepatrný	nepatrný	k2eAgInSc4d1,k2eNgInSc4d1
zlomek	zlomek	k1gInSc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
by	by	kYmCp3nP
měla	mít	k5eAaImAgNnP
vysílat	vysílat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonda	sonda	k1gFnSc1
již	již	k6eAd1
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c6
raketě	raketa	k1gFnSc6
<g/>
,	,	kIx,
znamenalo	znamenat	k5eAaImAgNnS
by	by	kYmCp3nS
její	její	k3xOp3gNnSc4
rozebrání	rozebrání	k1gNnSc4
další	další	k2eAgInSc1d1
výrazný	výrazný	k2eAgInSc1d1
odklad	odklad	k1gInSc1
startu	start	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byli	být	k5eAaImAgMnP
místní	místní	k2eAgMnPc1d1
chirurgové	chirurg	k1gMnPc1
požádáni	požádat	k5eAaPmNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
závadu	závada	k1gFnSc4
pokusili	pokusit	k5eAaPmAgMnP
odstranit	odstranit	k5eAaPmF
endoskopicky	endoskopicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
povedlo	povést	k5eAaPmAgNnS
a	a	k8xC
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
vlnovodu	vlnovod	k1gInSc6
antény	anténa	k1gFnSc2
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
feritové	feritový	k2eAgFnPc1d1
nečistoty	nečistota	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
února	únor	k1gInSc2
byla	být	k5eAaImAgFnS
sonda	sonda	k1gFnSc1
natankována	natankovat	k5eAaPmNgFnS
a	a	k8xC
v	v	k7c6
polovině	polovina	k1gFnSc6
února	únor	k1gInSc2
proběhlo	proběhnout	k5eAaPmAgNnS
její	její	k3xOp3gNnSc4
poslední	poslední	k2eAgNnSc4d1
testování	testování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Start	start	k1gInSc1
</s>
<s>
Start	start	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
naplánován	naplánovat	k5eAaBmNgInS
na	na	k7c4
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
a	a	k8xC
odpočítávání	odpočítávání	k1gNnSc2
mělo	mít	k5eAaImAgNnS
začít	začít	k5eAaPmF
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
odpočítávání	odpočítávání	k1gNnSc2
ale	ale	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
odkladu	odklad	k1gInSc3
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
záložního	záložní	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
manévrovacích	manévrovací	k2eAgInPc2d1
motorů	motor	k1gInPc2
druhého	druhý	k4xOgMnSc2
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následně	následně	k6eAd1
bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
další	další	k2eAgNnSc1d1
testování	testování	k1gNnSc1
a	a	k8xC
start	start	k1gInSc1
byl	být	k5eAaImAgInS
naplánován	naplánovat	k5eAaBmNgInS
na	na	k7c6
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
začalo	začít	k5eAaPmAgNnS
odpočítávání	odpočítávání	k1gNnSc1
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
přišel	přijít	k5eAaPmAgMnS
start	start	k1gInSc4
rakety	raketa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Following	Following	k1gInSc1
a	a	k8xC
successful	successful	k1gInSc1
launch	launcha	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
tentokrát	tentokrát	k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
<g/>
,	,	kIx,
sonda	sonda	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
nízké	nízký	k2eAgFnPc4d1
oběžné	oběžný	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
signály	signál	k1gInPc1
ze	z	k7c2
satelitu	satelit	k1gInSc2
registrovala	registrovat	k5eAaBmAgFnS
17	#num#	k4
minut	minuta	k1gFnPc2
po	po	k7c6
startu	start	k1gInSc6
pozemní	pozemní	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
v	v	k7c6
Keni	Keňa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mise	mise	k1gFnSc1
</s>
<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
měření	měření	k1gNnSc2
polárních	polární	k2eAgFnPc2d1
čepiček	čepička	k1gFnPc2
a	a	k8xC
tloušťky	tloušťka	k1gFnSc2
ledu	led	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Primárním	primární	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
satelitu	satelit	k1gInSc2
je	být	k5eAaImIp3nS
interferometrický	interferometrický	k2eAgInSc1d1
radarový	radarový	k2eAgInSc1d1
výškoměr	výškoměr	k1gInSc1
SIRAL-	SIRAL-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
měří	měřit	k5eAaImIp3nS
pomocí	pomocí	k7c2
radaru	radar	k1gInSc2
výšku	výška	k1gFnSc4
ledu	led	k1gInSc2
a	a	k8xC
výšku	výška	k1gFnSc4
kosmické	kosmický	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
s	s	k7c7
přesností	přesnost	k1gFnSc7
až	až	k9
1,3	1,3	k4
centimetru	centimetr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Satelit	satelit	k1gInSc1
má	mít	k5eAaImIp3nS
dva	dva	k4xCgInPc4
tyto	tento	k3xDgInPc4
výškoměry	výškoměr	k1gInPc4
pro	pro	k7c4
případ	případ	k1gInSc4
selhání	selhání	k1gNnSc2
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
nástroj	nástroj	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
DORIS	DORIS	kA
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
přesnému	přesný	k2eAgNnSc3d1
určení	určení	k1gNnSc3
oběžné	oběžný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
kosmické	kosmický	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Satelit	satelit	k1gInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
palubě	paluba	k1gFnSc6
také	také	k9
retroreflektory	retroreflektor	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
umožňují	umožňovat	k5eAaImIp3nP
provádět	provádět	k5eAaImF
zpětná	zpětný	k2eAgNnPc4d1
měření	měření	k1gNnPc4
k	k	k7c3
ověření	ověření	k1gNnSc3
výsledků	výsledek	k1gInPc2
DORIS	DORIS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
CryoSat-	CryoSat-	k?
<g/>
2	#num#	k4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
nízké	nízký	k2eAgFnSc6d1
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
s	s	k7c7
perigeem	perigeum	k1gNnSc7
ve	v	k7c6
výšce	výška	k1gFnSc6
720	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
apogeem	apogeum	k1gNnSc7
ve	v	k7c6
výšce	výška	k1gFnSc6
732	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
oběžná	oběžný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
má	mít	k5eAaImIp3nS
sklon	sklon	k1gInSc4
92	#num#	k4
stupňů	stupeň	k1gInPc2
k	k	k7c3
rovníku	rovník	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
satelitu	satelit	k1gInSc2
je	být	k5eAaImIp3nS
99	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonda	sonda	k1gFnSc1
má	mít	k5eAaImIp3nS
hmotnost	hmotnost	k1gFnSc4
720	#num#	k4
kilogramů	kilogram	k1gInPc2
a	a	k8xC
mise	mise	k1gFnSc1
měla	mít	k5eAaImAgFnS
trvat	trvat	k5eAaImF
nejméně	málo	k6eAd3
3	#num#	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Startovní	startovní	k2eAgFnSc1d1
a	a	k8xC
první	první	k4xOgFnSc1
fáze	fáze	k1gFnSc1
na	na	k7c6
orbitě	orbita	k1gFnSc6
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
následovala	následovat	k5eAaImAgFnS
aktivace	aktivace	k1gFnSc1
přístrojů	přístroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
sonda	sonda	k1gFnSc1
odeslal	odeslat	k5eAaPmAgMnS
první	první	k4xOgNnPc4
vědecká	vědecký	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orbitální	orbitální	k2eAgInSc4d1
testování	testování	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
sonda	sonda	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
pořádku	pořádek	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
připravena	připravit	k5eAaPmNgFnS
na	na	k7c4
plný	plný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
CryoSat-	CryoSat-	k1gFnSc2
<g/>
2	#num#	k4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CryoSat-	CryoSat-	k1gFnSc7
<g/>
2	#num#	k4
Earth	Eartha	k1gFnPc2
Explorer	Explorra	k1gFnPc2
Opportunity	Opportunita	k1gFnSc2
Mission-	Mission-	k1gFnSc7
<g/>
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESA	eso	k1gNnSc2
eoPortal	eoPortat	k5eAaPmAgMnS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WADE	WADE	k?
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cryosat	Cryosat	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
Astronautica	Astronauticum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
COPPINGER	COPPINGER	kA
<g/>
,	,	kIx,
Rob	roba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cryosat	Cryosat	k1gFnSc1
<g/>
:	:	kIx,
a	a	k8xC
decade	decást	k5eAaPmIp3nS
long	long	k1gMnSc1
journey	journea	k1gFnSc2
for	forum	k1gNnPc2
industry	industra	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flight	Flight	k1gMnSc1
Global	globat	k5eAaImAgMnS
<g/>
,	,	kIx,
22	#num#	k4
April	April	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
22	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
L-37	L-37	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurockot	Eurockot	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
September	September	k1gInSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CryoSat	CryoSat	k2eAgInSc4d1
Mission	Mission	k1gInSc4
lost	lost	k1gMnSc1
due	due	k?
to	ten	k3xDgNnSc1
launch	launch	k1gMnSc1
failure	failur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
8	#num#	k4
October	October	k1gInSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CryoSat	CryoSat	k2eAgInSc4d1
Mission	Mission	k1gInSc4
has	hasit	k5eAaImRp2nS
been	been	k1gNnSc4
lost	lost	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurockot	Eurockot	k1gInSc1
<g/>
,	,	kIx,
8	#num#	k4
October	October	k1gInSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
27	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cryosat	Cryosat	k1gFnSc1
team	team	k1gInSc1
desperate	desperat	k1gInSc5
to	ten	k3xDgNnSc4
rebuild	rebuild	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
October	October	k1gInSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CLARK	CLARK	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	European	k1gInSc1
ice-watching	ice-watching	k1gInSc4
satellite	satellit	k1gInSc5
will	wilnout	k5eAaPmAgMnS
launch	launcha	k1gFnPc2
Thursday	Thursdaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spaceflight	Spaceflight	k1gMnSc1
Now	Now	k1gMnSc1
<g/>
,	,	kIx,
8	#num#	k4
April	April	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ESA	eso	k1gNnSc2
confirms	confirms	k6eAd1
CryoSat	CryoSat	k1gInSc4
recovery	recovera	k1gFnSc2
mission	mission	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
24	#num#	k4
February	Februara	k1gFnSc2
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CryoSat	CryoSat	k1gInSc1
ready	ready	k0
for	forum	k1gNnPc2
launch	launch	k1gInSc1
<g/>
:	:	kIx,
Media	medium	k1gNnSc2
Day	Day	k1gFnSc2
at	at	k?
IABG	IABG	kA
<g/>
/	/	kIx~
<g/>
Munich	Munich	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
4	#num#	k4
September	September	k1gInSc1
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
CryoSat-	CryoSat-	k1gFnPc2
<g/>
2	#num#	k4
on	on	k3xPp3gMnSc1
the	the	k?
road	road	k1gInSc1
to	ten	k3xDgNnSc1
recovery	recovera	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
12	#num#	k4
March	March	k1gInSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Workshop	workshop	k1gInSc1
on	on	k3xPp3gMnSc1
Antarctic	Antarctice	k1gFnPc2
sea-ice	sea-ice	k1gInPc1
highlights	highlights	k6eAd1
need	ed	k6eNd1
for	forum	k1gNnPc2
CryoSat-	CryoSat-	k1gFnSc1
<g/>
2	#num#	k4
mission	mission	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
9	#num#	k4
August	August	k1gMnSc1
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CryoSat-	CryoSat-	k1gFnSc2
<g/>
2	#num#	k4
Announcement	Announcement	k1gMnSc1
of	of	k?
Opportunity	Opportunita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
11	#num#	k4
January	Januara	k1gFnSc2
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Scientists	Scientists	k1gInSc1
endure	endur	k1gMnSc5
Arctic	Arctice	k1gFnPc2
for	forum	k1gNnPc2
last	last	k1gMnSc1
campaign	campaign	k1gMnSc1
prior	prior	k1gMnSc1
to	ten	k3xDgNnSc4
CryoSat-	CryoSat-	k1gFnPc2
<g/>
2	#num#	k4
launch	launcha	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
9	#num#	k4
May	May	k1gMnSc1
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Scientists	Scientists	k1gInSc1
and	and	k?
polar	polar	k1gInSc1
explorers	explorers	k1gInSc1
brave	brav	k1gInSc5
the	the	k?
elements	elements	k1gInSc1
in	in	k?
support	support	k1gInSc1
of	of	k?
CryoSat-	CryoSat-	k1gFnSc2
<g/>
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
19	#num#	k4
April	April	k1gInSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Last	Last	k1gMnSc1
look	look	k1gMnSc1
at	at	k?
CryoSat-	CryoSat-	k1gMnSc1
<g/>
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
11	#num#	k4
February	Februara	k1gFnSc2
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MCDOWELL	MCDOWELL	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Launch	Launch	k1gInSc1
Log	log	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jonathan	Jonathan	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Space	Spaec	k1gInSc2
Page	Pag	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JONES	JONES	kA
<g/>
,	,	kIx,
Tamera	Tamero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Successful	Successful	k1gInSc1
launch	launch	k1gInSc4
for	forum	k1gNnPc2
ESA	eso	k1gNnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
CryoSat-	CryoSat-	k1gFnSc7
<g/>
2	#num#	k4
ice	ice	k?
mission	mission	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natural	Natural	k?
Environment	Environment	k1gMnSc1
Research	Research	k1gMnSc1
Council	Council	k1gMnSc1
<g/>
,	,	kIx,
8	#num#	k4
April	April	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Successful	Successful	k1gInSc1
launch	launcha	k1gFnPc2
for	forum	k1gNnPc2
ESA	eso	k1gNnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
CryoSat-	CryoSat-	k1gFnSc7
<g/>
2	#num#	k4
ice	ice	k?
satellite	satellit	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
,	,	kIx,
8	#num#	k4
April	April	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
CryoSat	CryoSat	k1gInSc1
<g/>
:	:	kIx,
an	an	k?
icy	icy	k?
mission	mission	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ILRS	ILRS	kA
Mission	Mission	k1gInSc1
Support	support	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
International	International	k1gFnSc1
Laser	laser	k1gInSc1
Ranging	Ranging	k1gInSc1
Service	Service	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KREBS	KREBS	kA
<g/>
,	,	kIx,
Gunter	Gunter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cryosat	Cryosat	k1gFnSc1
1,2	1,2	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gunter	Gunter	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Space	Spaec	k1gInSc2
Page	Pag	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CRYOSAT	CRYOSAT	kA
2	#num#	k4
Satellite	Satellit	k1gInSc5
details	detailsit	k5eAaPmRp2nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
N	N	kA
<g/>
2	#num#	k4
<g/>
YO	YO	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cryosat	Cryosat	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
International	International	k1gFnSc1
Laser	laser	k1gInSc1
Ranging	Ranging	k1gInSc1
Service	Service	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CLARK	CLARK	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CryoSat	CryoSat	k1gInSc1
2	#num#	k4
passes	passesa	k1gFnPc2
in-orbit	in-orbit	k5eAaPmF,k5eAaImF
tests	tests	k1gInSc4
with	with	k1gInSc1
flying	flying	k1gInSc1
colors	colors	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spaceflight	Spaceflight	k1gMnSc1
Now	Now	k1gMnSc1
<g/>
,	,	kIx,
27	#num#	k4
October	October	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
