<s>
Marie	Marie	k1gFnSc1
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Maria	Maria	k1gFnSc1
Salomea	Salomea	k1gFnSc1
Skłodowska	Skłodowska	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
polštině	polština	k1gFnSc6
Maria	Maria	k1gFnSc1
Skłodowska-Curie	Skłodowska-Curie	k1gFnSc1
(	(	kIx(
<g/>
7	[number]	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1867	[number]	k4
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
-	-	kIx~
4	[number]	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1934	[number]	k4
Passy	Passa	k1gFnSc2
<g/>
,	,	kIx,
Haute-Savoie	Haute-Savoie	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
významná	významný	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
vědkyně	vědkyně	k1gFnSc1
polského	polský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>