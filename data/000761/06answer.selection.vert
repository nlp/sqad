<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
29	[number]	k4	29
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
1336	[number]	k4	1336
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zdržoval	zdržovat	k5eAaImAgInS	zdržovat
i	i	k9	i
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1341	[number]	k4	1341
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
vratislavská	vratislavský	k2eAgFnSc1d1	Vratislavská
kapitula	kapitula	k1gFnSc1	kapitula
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
biskupa	biskup	k1gMnSc2	biskup
Nankera	Nanker	k1gMnSc2	Nanker
zvolila	zvolit	k5eAaPmAgFnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
