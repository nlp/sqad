<s>
Trnová	trnový	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
</s>
<s>
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
s	s	k7c7
trnovou	trnový	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
na	na	k7c6
hlavě	hlava	k1gFnSc6
(	(	kIx(
<g/>
El	Ela	k1gFnPc2
Greco	Greco	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Trnová	trnový	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Trnová	trnový	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
věnec	věnec	k1gInSc4
z	z	k7c2
trní	trní	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Ježíši	Ježíš	k1gMnSc3
Kristu	Krista	k1gFnSc4
vsadili	vsadit	k5eAaPmAgMnP
vojáci	voják	k1gMnPc1
na	na	k7c4
hlavu	hlava	k1gFnSc4
před	před	k7c7
jeho	jeho	k3xOp3gNnSc7
ukřižováním	ukřižování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
křesťanství	křesťanství	k1gNnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nástrojů	nástroj	k1gInPc2
Ježíšova	Ježíšův	k2eAgNnSc2d1
umučení	umučení	k1gNnSc2
vztahujícím	vztahující	k2eAgInSc7d1
se	se	k3xPyFc4
k	k	k7c3
pašijím	pašije	k1gFnPc3
<g/>
,	,	kIx,
biblickým	biblický	k2eAgNnSc7d1
utrpením	utrpení	k1gNnSc7
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
předcházejícím	předcházející	k2eAgNnSc6d1
samotnému	samotný	k2eAgNnSc3d1
ukřižování	ukřižování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Trnová	trnový	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
rákosem	rákos	k1gInSc7
(	(	kIx(
<g/>
„	„	k?
<g/>
žezlem	žezlo	k1gNnSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
purpurovým	purpurový	k2eAgInSc7d1
pláštěm	plášť	k1gInSc7
<g/>
,	,	kIx,
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
bolestná	bolestný	k2eAgFnSc1d1
parodie	parodie	k1gFnSc1
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
titul	titul	k1gInSc4
židovského	židovský	k2eAgMnSc2d1
krále	král	k1gMnSc2
(	(	kIx(
<g/>
obvinění	obvinění	k1gNnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
Pilátem	pilát	k1gInSc7
odsouzen	odsouzen	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluví	mluvit	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gMnSc6
v	v	k7c6
evangeliích	evangelium	k1gNnPc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
často	často	k6eAd1
jej	on	k3xPp3gMnSc4
zmiňují	zmiňovat	k5eAaImIp3nP
také	také	k9
další	další	k2eAgInPc1d1
rané	raný	k2eAgInPc1d1
křesťanské	křesťanský	k2eAgInPc1d1
texty	text	k1gInPc1
jako	jako	k8xS,k8xC
Klement	Klement	k1gMnSc1
Alexandrijský	alexandrijský	k2eAgMnSc1d1
nebo	nebo	k8xC
Órigenés	Órigenés	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Francouzskému	francouzský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Ludvíku	Ludvík	k1gMnSc3
IX	IX	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
sv.	sv.	kA
Ludvík	Ludvík	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
údajně	údajně	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
tuto	tento	k3xDgFnSc4
relikvii	relikvie	k1gFnSc4
získat	získat	k5eAaPmF
a	a	k8xC
pro	pro	k7c4
její	její	k3xOp3gNnSc4
uschování	uschování	k1gNnSc4
byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
Sainte-Chapelle	Sainte-Chapelle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
trn	trn	k1gInSc1
byl	být	k5eAaImAgInS
podle	podle	k7c2
pověsti	pověst	k1gFnSc2
vložen	vložit	k5eAaPmNgInS
do	do	k7c2
Svatováclavské	svatováclavský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
si	se	k3xPyFc3
náš	náš	k3xOp1gInSc4
Pán	pán	k1gMnSc1
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
vyvolil	vyvolit	k5eAaPmAgMnS
zemi	zem	k1gFnSc4
zaslíbenou	zaslíbená	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
ukázal	ukázat	k5eAaPmAgMnS
tajemství	tajemství	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
vykoupení	vykoupení	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
a	a	k8xC
věříme	věřit	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
ke	k	k7c3
zbožnějšímu	zbožní	k2eAgNnSc3d2
uctívání	uctívání	k1gNnSc3
svého	svůj	k3xOyFgNnSc2
vítězství	vítězství	k1gNnSc2
nad	nad	k7c7
temnotou	temnota	k1gFnSc7
a	a	k8xC
smrtí	smrtit	k5eAaImIp3nS
si	se	k3xPyFc3
vyvolil	vyvolit	k5eAaPmAgMnS
obzvláště	obzvláště	k6eAd1
naši	náš	k3xOp1gFnSc4
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
od	od	k7c2
Východu	východ	k1gInSc2
na	na	k7c4
Západ	západ	k1gInSc4
uctívalo	uctívat	k5eAaImAgNnS
jméno	jméno	k1gNnSc1
Páně	páně	k2eAgNnSc2d1
skrze	skrze	k?
přesun	přesun	k1gInSc1
nástrojů	nástroj	k1gInPc2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
přesvatého	přesvatý	k2eAgNnSc2d1
utrpení	utrpení	k1gNnSc2
<g/>
,	,	kIx,
kterýžto	kterýžto	k?
přesun	přesun	k1gInSc4
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
náš	náš	k3xOp1gMnSc1
Pán	pán	k1gMnSc1
a	a	k8xC
vykupitel	vykupitel	k1gMnSc1
z	z	k7c2
řeckých	řecký	k2eAgFnPc2d1
končin	končina	k1gFnPc2
<g/>
,	,	kIx,
o	o	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ze	z	k7c2
všech	všecek	k3xTgFnPc2
končin	končina	k1gFnPc2
východních	východní	k2eAgFnPc2d1
jsou	být	k5eAaImIp3nP
nejblíže	blízce	k6eAd3
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
dotýká	dotýkat	k5eAaImIp3nS
hranic	hranice	k1gFnPc2
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Gautier	Gautier	k1gMnSc1
Cornut	Cornut	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Symbol	symbol	k1gInSc1
</s>
<s>
Trnová	trnový	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
symbol	symbol	k1gInSc1
utrpení	utrpení	k1gNnSc2
a	a	k8xC
mučednictví	mučednictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zažité	zažitý	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
trnovou	trnový	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
položila	položit	k5eAaPmAgFnS
Božena	Božena	k1gFnSc1
Němcová	Němcová	k1gFnSc1
na	na	k7c4
rakev	rakev	k1gFnSc4
Karla	Karel	k1gMnSc4
Havlíčka	Havlíček	k1gMnSc4
Borovského	Borovský	k1gMnSc4
při	při	k7c6
jeho	jeho	k3xOp3gInSc6
pohřbu	pohřeb	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
vavřínový	vavřínový	k2eAgInSc4d1
věnec	věnec	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
objednal	objednat	k5eAaPmAgMnS
Ferdinand	Ferdinand	k1gMnSc1
Fingerhut	Fingerhut	k1gMnSc1
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
Vojty	Vojta	k1gMnSc2
Náprstka	Náprstka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Mk	Mk	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mt	Mt	k1gFnSc1
27	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
19	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Svatý	svatý	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
106	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pohřeb	pohřeb	k1gInSc1
Havlíčkův	Havlíčkův	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavitelé	stavitel	k1gMnPc1
katedrál	katedrála	k1gFnPc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristova	Kristův	k2eAgFnSc1d1
trnová	trnový	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ars	Ars	k1gFnSc1
Auro	aura	k1gFnSc5
prior	prior	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
384	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LE	LE	kA
GOFF	GOFF	kA
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatý	svatý	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
724	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
685	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
