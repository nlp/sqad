<s desamb="1">
První	první	k4xOgFnSc1
část	část	k1gFnSc1
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc4d1
kód	kód	k1gInSc4
pro	pro	k7c4
Španělsko	Španělsko	k1gNnSc4
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
jednoho	jeden	k4xCgInSc2
nebo	nebo	k8xC
dvou	dva	k4xCgNnPc2
písmen	písmeno	k1gNnPc2
identifikujících	identifikující	k2eAgNnPc2d1
region	region	k1gInSc4
<g/>
,	,	kIx,
provincii	provincie	k1gFnSc4
nebo	nebo	k8xC
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>