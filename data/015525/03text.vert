<s>
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
ES	ES	kA
</s>
<s>
Kódy	kód	k1gInPc1
ISO	ISO	kA
3166-2	3166-2	k4
pro	pro	k7c4
Španělsko	Španělsko	k1gNnSc4
identifikují	identifikovat	k5eAaBmIp3nP
17	#num#	k4
autonomních	autonomní	k2eAgMnPc2d1
regionů	region	k1gInPc2
<g/>
,	,	kIx,
50	#num#	k4
provincií	provincie	k1gFnPc2
a	a	k8xC
2	#num#	k4
autonomní	autonomní	k2eAgFnSc4d1
města	město	k1gNnPc1
(	(	kIx(
<g/>
stav	stav	k1gInSc1
v	v	k7c6
dubnu	duben	k1gInSc6
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
část	část	k1gFnSc1
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc4d1
kód	kód	k1gInSc4
pro	pro	k7c4
Španělsko	Španělsko	k1gNnSc4
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
jednoho	jeden	k4xCgInSc2
nebo	nebo	k8xC
dvou	dva	k4xCgNnPc2
písmen	písmeno	k1gNnPc2
identifikujících	identifikující	k2eAgNnPc2d1
region	region	k1gInSc4
<g/>
,	,	kIx,
provincii	provincie	k1gFnSc4
nebo	nebo	k8xC
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Zpravodaje	zpravodaj	k1gInPc1
</s>
<s>
en	en	k?
<g/>
:	:	kIx,
<g/>
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
</s>
<s>
en	en	k?
<g/>
:	:	kIx,
<g/>
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
</s>
<s>
en	en	k?
<g/>
:	:	kIx,
<g/>
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
kódů	kód	k1gInPc2
</s>
<s>
ES-AN	ES-AN	k?
Andalusie	Andalusie	k1gFnSc1
</s>
<s>
ES-AR	ES-AR	k?
Aragonie	Aragonie	k1gFnSc1
</s>
<s>
ES-AS	ES-AS	k?
Asturie	Asturie	k1gFnSc1
</s>
<s>
ES-IB	ES-IB	k?
Baleáry	Baleáry	k1gFnPc1
</s>
<s>
ES-PV	ES-PV	k?
Baskicko	Baskicko	k1gNnSc1
</s>
<s>
ES-EX	ES-EX	k?
Extremadura	Extremadura	k1gFnSc1
</s>
<s>
ES-GA	ES-GA	k?
Galicie	Galicie	k1gFnSc1
</s>
<s>
ES-CN	ES-CN	k?
Kanárské	kanárský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
</s>
<s>
ES-CB	ES-CB	k?
Kantábrie	Kantábrie	k1gFnSc1
</s>
<s>
ES-CM	ES-CM	k?
Kastilie	Kastilie	k1gFnSc1
–	–	k?
La	la	k1gNnSc1
Mancha	Mancha	k1gMnSc1
</s>
<s>
ES-CL	ES-CL	k?
Kastilie	Kastilie	k1gFnSc1
a	a	k8xC
León	León	k1gInSc1
</s>
<s>
ES-CT	ES-CT	k?
Katalánsko	Katalánsko	k1gNnSc1
</s>
<s>
ES-RI	ES-RI	k?
La	la	k1gNnSc1
Rioja	Rioj	k1gInSc2
</s>
<s>
ES-MD	ES-MD	k?
Madridské	madridský	k2eAgNnSc1d1
autonomní	autonomní	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
</s>
<s>
ES-MC	ES-MC	k?
Murcijský	Murcijský	k2eAgInSc4d1
region	region	k1gInSc4
</s>
<s>
ES-NC	ES-NC	k?
Navarra	Navarra	k1gFnSc1
</s>
<s>
ES-VC	ES-VC	k?
Valencijské	valencijský	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
</s>
<s>
ES-A	ES-A	k?
Alicante	Alicant	k1gMnSc5
<g/>
/	/	kIx~
<g/>
Alacant	Alacant	k1gInSc4
(	(	kIx(
<g/>
Alicante	Alicant	k1gMnSc5
<g/>
/	/	kIx~
<g/>
Alacant	Alacant	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ES-AB	ES-AB	k?
Albacete	Albace	k1gNnSc2
(	(	kIx(
<g/>
Albacete	Albace	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
ES-AL	ES-AL	k?
Almería	Almería	k1gFnSc1
(	(	kIx(
<g/>
Almería	Almería	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-AV	ES-AV	k?
Ávila	Ávila	k1gFnSc1
(	(	kIx(
<g/>
Ávila	Ávila	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-B	ES-B	k?
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-BA	ES-BA	k?
Badajoz	Badajoz	k1gMnSc1
(	(	kIx(
<g/>
Badajoz	Badajoz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-BI	ES-BI	k?
Vizcaya	Vizcaya	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Bizkaia	Bizkaia	k1gFnSc1
(	(	kIx(
<g/>
Bilbao	Bilbao	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Bilbo	Bilba	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
ES-BU	ES-BU	k?
Burgos	Burgos	k1gMnSc1
(	(	kIx(
<g/>
Burgos	Burgos	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-C	ES-C	k?
La	la	k1gNnSc1
Coruñ	Coruñ	k1gInSc2
<g/>
/	/	kIx~
<g/>
A	a	k9
Coruñ	Coruñ	k1gNnSc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
Coruñ	Coruñ	k1gInSc2
<g/>
/	/	kIx~
<g/>
A	a	k9
Coruñ	Coruñ	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
ES-CA	ES-CA	k?
Cádiz	Cádiz	k1gInSc1
(	(	kIx(
<g/>
Cádiz	Cádiz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ES-CC	ES-CC	k?
Cáceres	Cáceres	k1gMnSc1
(	(	kIx(
<g/>
Cáceres	Cáceres	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-CO	ES-CO	k?
Córdoba	Córdoba	k1gFnSc1
(	(	kIx(
<g/>
Córdoba	Córdoba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-CR	ES-CR	k?
Ciudad	Ciudad	k1gInSc1
Real	Real	k1gInSc1
(	(	kIx(
<g/>
Ciudad	Ciudad	k1gInSc1
Real	Real	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ES-CS	ES-CS	k?
Castellón	Castellón	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Castelló	Castelló	k1gMnSc1
(	(	kIx(
<g/>
Castellón	Castellón	k1gMnSc1
de	de	k?
la	la	k1gNnPc2
Plana	Plana	k?
<g/>
/	/	kIx~
<g/>
Castelló	Castelló	k1gMnPc2
de	de	k?
la	la	k1gNnPc2
Plana	Plana	k?
<g/>
)	)	kIx)
</s>
<s>
ES-CU	ES-CU	k?
Cuenca	Cuenca	k1gFnSc1
(	(	kIx(
<g/>
Cuenca	Cuenca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-GC	ES-GC	k?
Las	laso	k1gNnPc2
Palmas	Palmas	k1gInSc1
(	(	kIx(
<g/>
Las	laso	k1gNnPc2
Palmas	Palmasa	k1gFnPc2
de	de	k?
Gran	Gran	k1gNnSc1
Canaria	Canarium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
ES-GI	ES-GI	k?
Gerona	Gero	k1gMnSc4
<g/>
/	/	kIx~
<g/>
Girona	Giron	k1gMnSc4
(	(	kIx(
<g/>
Gerona	Gero	k1gMnSc4
<g/>
/	/	kIx~
<g/>
Girona	Giron	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
ES-GR	ES-GR	k?
Granada	Granada	k1gFnSc1
(	(	kIx(
<g/>
Granada	Granada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-GU	ES-GU	k?
Guadalajara	Guadalajara	k1gFnSc1
(	(	kIx(
<g/>
Guadalajara	Guadalajara	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-H	ES-H	k?
Huelva	Huelva	k1gFnSc1
(	(	kIx(
<g/>
Huelva	Huelva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-HU	ES-HU	k?
Huesca	Huesca	k1gFnSc1
(	(	kIx(
<g/>
Huesca	Huesca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-J	ES-J	k?
Jaén	Jaén	k1gMnSc1
(	(	kIx(
<g/>
Jaén	Jaén	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-L	ES-L	k?
Lérida	Lérida	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Lleida	Lleida	k1gFnSc1
(	(	kIx(
<g/>
Lérida	Lérida	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Lleida	Lleida	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-LE	ES-LE	k?
León	León	k1gMnSc1
(	(	kIx(
<g/>
León	León	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-LO	ES-LO	k?
La	la	k1gNnSc1
Rioja	Rioja	k1gMnSc1
(	(	kIx(
<g/>
Logroñ	Logroñ	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-LU	ES-LU	k?
Lugo	Lugo	k1gMnSc1
(	(	kIx(
<g/>
Lugo	Lugo	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-M	ES-M	k?
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ES-MA	ES-MA	k?
Málaga	Málaga	k1gFnSc1
(	(	kIx(
<g/>
Málaga	Málaga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-MU	ES-MU	k?
Murcia	Murcia	k1gFnSc1
(	(	kIx(
<g/>
Murcia	Murcia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-NA	ES-NA	k?
Navarra	Navarra	k1gFnSc1
(	(	kIx(
<g/>
Pamplona	Pamplona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-O	ES-O	k?
Asturias	Asturias	k1gInSc1
(	(	kIx(
<g/>
Oviedo	Oviedo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-OR	ES-OR	k?
Orense	Orense	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Ourense	Ourense	k1gFnSc1
(	(	kIx(
<g/>
Orense	Orense	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Ourense	Ourense	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-P	ES-P	k?
Palencia	Palencia	k1gFnSc1
(	(	kIx(
<g/>
Palencia	Palencia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-PM	ES-PM	k?
Baleáry	Baleáry	k1gFnPc1
(	(	kIx(
<g/>
Islas	Islas	k1gInSc1
Baleares	Baleares	k1gInSc1
<g/>
/	/	kIx~
<g/>
Illes	Illes	k1gInSc1
Balears	Balears	k1gInSc1
<g/>
,	,	kIx,
Palma	palma	k1gFnSc1
de	de	k?
Mallorca	Mallorca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-PO	ES-PO	k?
Pontevedra	Pontevedra	k1gFnSc1
(	(	kIx(
<g/>
Pontevedra	Pontevedra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-S	ES-S	k?
Cantabria	Cantabrium	k1gNnPc1
(	(	kIx(
<g/>
Santander	Santander	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ES-SA	ES-SA	k?
Salamanca	Salamanca	k1gFnSc1
(	(	kIx(
<g/>
Salamanca	Salamanca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-SE	ES-SE	k?
Sevilla	Sevilla	k1gFnSc1
(	(	kIx(
<g/>
Sevilla	Sevilla	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-SG	ES-SG	k?
Segovia	Segovia	k1gFnSc1
(	(	kIx(
<g/>
Segovia	Segovia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-SO	ES-SO	k?
Soria	Soria	k1gFnSc1
(	(	kIx(
<g/>
Soria	Soria	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-SS	ES-SS	k?
Guipúzcoa	Guipúzcoa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Gipuzkoa	Gipuzkoa	k1gMnSc1
(	(	kIx(
<g/>
San	San	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Donostia	Donostia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-T	ES-T	k?
Tarragona	Tarragona	k1gFnSc1
(	(	kIx(
<g/>
Tarragona	Tarragona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-TE	ES-TE	k?
Teruel	Teruel	k1gMnSc1
(	(	kIx(
<g/>
Teruel	Teruel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-TF	ES-TF	k?
Tenerife	Tenerif	k1gMnSc5
(	(	kIx(
<g/>
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
De	De	k?
Tenerife	Tenerif	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
ES-TO	ES-TO	k?
Toledo	Toledo	k1gNnSc1
(	(	kIx(
<g/>
Toledo	Toledo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-V	ES-V	k?
Valencia	Valencia	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Valè	Valè	k1gFnSc1
(	(	kIx(
<g/>
Valencia	Valencia	k1gFnSc1
<g/>
\	\	kIx~
<g/>
Valè	Valè	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-VA	ES-VA	k?
Valladolid	Valladolid	k1gInSc1
(	(	kIx(
<g/>
Valladolid	Valladolid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ES-VI	ES-VI	k?
Álava	Álav	k1gMnSc4
<g/>
/	/	kIx~
<g/>
Araba	Arab	k1gMnSc4
(	(	kIx(
<g/>
Vitoria	Vitorium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Gasteiz	Gasteiz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ES-Z	ES-Z	k?
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
Zaragoza	Zaragoza	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-ZA	ES-ZA	k?
Zamora	Zamora	k1gFnSc1
(	(	kIx(
<g/>
Zamora	Zamora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-ML	ES-ML	k?
Melilla	Melilla	k1gFnSc1
(	(	kIx(
<g/>
Melilla	Melilla	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ES-CE	ES-CE	k?
Ceuta	Ceuta	k1gFnSc1
(	(	kIx(
<g/>
Ceuta	Ceuta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
seznam	seznam	k1gInSc1
kódů	kód	k1gInPc2
územně	územně	k6eAd1
správních	správní	k2eAgInPc2d1
celků	celek	k1gInPc2
všech	všecek	k3xTgFnPc2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
seznam	seznam	k1gInSc1
kódů	kód	k1gInPc2
všech	všecek	k3xTgFnPc2
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
používaných	používaný	k2eAgInPc2d1
současně	současně	k6eAd1
jako	jako	k8xC,k8xS
jejich	jejich	k3xOp3gNnPc1
doménová	doménový	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
