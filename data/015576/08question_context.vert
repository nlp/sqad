<s>
První	první	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
–	–	k?
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
čínskou	čínský	k2eAgFnSc7d1
říší	říše	k1gFnSc7
Čching	Čching	k1gInSc1
a	a	k8xC
Japonskem	Japonsko	k1gNnSc7
o	o	k7c4
nadvládu	nadvláda	k1gFnSc4
nad	nad	k7c7
územím	území	k1gNnSc7
Korejského	korejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
přilehlých	přilehlý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>