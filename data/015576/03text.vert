<s>
První	první	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
období	období	k1gNnSc6
1894	#num#	k4
<g/>
-	-	kIx~
<g/>
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
válce	válka	k1gFnSc6
z	z	k7c2
let	léto	k1gNnPc2
1937-1945	1937-1945	k4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
ilustrační	ilustrační	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1894	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1895	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
2	#num#	k4
týdny	týden	k1gInPc4
a	a	k8xC
2	#num#	k4
dny	den	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
,	,	kIx,
Žluté	žlutý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
vítězství	vítězství	k1gNnSc1
Japonského	japonský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Říše	říše	k1gFnSc1
Čching	Čching	k1gInSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Aritomo	Aritomo	k6eAd1
Jamagata	Jamagata	k1gFnSc1
Sukejuki	Sukejuk	k1gFnSc2
Itó	Itó	k1gFnSc2
</s>
<s>
Li	li	k8xS
Chung-čang	Chung-čang	k1gMnSc1
Ting	Ting	k1gMnSc1
Žu-čchang	Žu-čchang	k1gMnSc1
†	†	k?
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
13	#num#	k4
820	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
<g/>
3	#num#	k4
970	#num#	k4
raněných	raněný	k1gMnPc2
</s>
<s>
35	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
raněných	raněný	k1gMnPc2
</s>
<s>
První	první	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
–	–	k?
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
čínskou	čínský	k2eAgFnSc7d1
říší	říše	k1gFnSc7
Čching	Čching	k1gInSc1
a	a	k8xC
Japonskem	Japonsko	k1gNnSc7
o	o	k7c4
nadvládu	nadvláda	k1gFnSc4
nad	nad	k7c7
územím	území	k1gNnSc7
Korejského	korejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
přilehlých	přilehlý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
drtivou	drtivý	k2eAgFnSc7d1
porážkou	porážka	k1gFnSc7
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ztratila	ztratit	k5eAaPmAgFnS
nejen	nejen	k6eAd1
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
v	v	k7c6
Koreji	Korea	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
ostrov	ostrov	k1gInSc4
Tchaj-wan	Tchaj-wan	k1gInSc1
a	a	k8xC
postavení	postavení	k1gNnSc4
první	první	k4xOgFnSc2
asijské	asijský	k2eAgFnSc2d1
velmoci	velmoc	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
převzalo	převzít	k5eAaPmAgNnS
Japonsko	Japonsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
nepřímo	přímo	k6eNd1
položila	položit	k5eAaPmAgFnS
základ	základ	k1gInSc4
pozdější	pozdní	k2eAgFnSc2d2
rusko-japonské	rusko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgInPc1d1
impérium	impérium	k1gNnSc4
totiž	totiž	k9
nejenže	nejenže	k6eAd1
využilo	využít	k5eAaPmAgNnS
oslabení	oslabení	k1gNnSc1
Číny	Čína	k1gFnSc2
a	a	k8xC
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
k	k	k7c3
ovládnutí	ovládnutí	k1gNnSc3
části	část	k1gFnSc2
Mandžuska	Mandžusko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
dokonce	dokonce	k9
i	i	k9
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
s	s	k7c7
podporou	podpora	k1gFnSc7
Francie	Francie	k1gFnSc2
a	a	k8xC
Německa	Německo	k1gNnSc2
přinutilo	přinutit	k5eAaPmAgNnS
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
předalo	předat	k5eAaPmAgNnS
Port	port	k1gInSc4
Arthur	Arthur	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
mu	on	k3xPp3gMnSc3
Čína	Čína	k1gFnSc1
v	v	k7c4
Šimonosecké	Šimonosecká	k1gFnPc4
mírové	mírový	k2eAgFnSc3d1
smlouvě	smlouva	k1gFnSc3
také	také	k9
postoupila	postoupit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
Japonskem	Japonsko	k1gNnSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
ohledně	ohledně	k7c2
Koreje	Korea	k1gFnSc2
stoupalo	stoupat	k5eAaImAgNnS
od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
se	se	k3xPyFc4
klika	klika	k1gFnSc1
korejských	korejský	k2eAgMnPc2d1
reformních	reformní	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
podporovaných	podporovaný	k2eAgMnPc2d1
Japonskem	Japonsko	k1gNnSc7
pokusila	pokusit	k5eAaPmAgFnS
provést	provést	k5eAaPmF
převrat	převrat	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
na	na	k7c6
straně	strana	k1gFnSc6
konzervativců	konzervativec	k1gMnPc2
zasáhla	zasáhnout	k5eAaPmAgFnS
čínská	čínský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
a	a	k8xC
puč	puč	k1gInSc1
tak	tak	k6eAd1
skončil	skončit	k5eAaPmAgInS
neúspěchem	neúspěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
případným	případný	k2eAgInPc3d1
budoucím	budoucí	k2eAgInPc3d1
střetům	střet	k1gInPc3
<g/>
,	,	kIx,
uzavřely	uzavřít	k5eAaPmAgFnP
obě	dva	k4xCgFnPc1
dvě	dva	k4xCgFnPc1
mocnosti	mocnost	k1gFnPc1
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1885	#num#	k4
dohodu	dohoda	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
přislíbily	přislíbit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
vzájemně	vzájemně	k6eAd1
předem	předem	k6eAd1
oznámí	oznámit	k5eAaPmIp3nS
vyslání	vyslání	k1gNnSc4
expedičních	expediční	k2eAgFnPc2d1
sil	síla	k1gFnPc2
do	do	k7c2
Koreje	Korea	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Válka	válka	k1gFnSc1
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1894	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Koreji	Korea	k1gFnSc6
rozpoutalo	rozpoutat	k5eAaPmAgNnS
rolnické	rolnický	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
Tonghak	Tonghak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korejští	korejský	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
požádali	požádat	k5eAaPmAgMnP
Čínu	Čína	k1gFnSc4
o	o	k7c6
vyslání	vyslání	k1gNnSc6
vojenské	vojenský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
souhlasila	souhlasit	k5eAaImAgFnS
a	a	k8xC
informovala	informovat	k5eAaBmAgFnS
o	o	k7c6
nadcházejícím	nadcházející	k2eAgInSc6d1
zásahu	zásah	k1gInSc6
Japonsko	Japonsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
se	se	k3xPyFc4
taktéž	taktéž	k?
připravovalo	připravovat	k5eAaImAgNnS
v	v	k7c6
Koreji	Korea	k1gFnSc6
vojensky	vojensky	k6eAd1
intervenovat	intervenovat	k5eAaImF
a	a	k8xC
vyvolat	vyvolat	k5eAaPmF
válku	válka	k1gFnSc4
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
západním	západní	k2eAgMnSc7d1
sousedem	soused	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prvním	první	k4xOgNnPc3
střetům	střet	k1gInPc3
došlo	dojít	k5eAaPmAgNnS
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1894	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
japonská	japonský	k2eAgFnSc1d1
křižníková	křižníkový	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
(	(	kIx(
<g/>
chráněné	chráněný	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Akicušima	Akicušima	k1gNnSc1
<g/>
,	,	kIx,
Jošino	Jošino	k1gNnSc1
a	a	k8xC
Naniwa	Naniwa	k1gFnSc1
<g/>
)	)	kIx)
nejprve	nejprve	k6eAd1
porazila	porazit	k5eAaPmAgFnS
čínský	čínský	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Čchi-jüan	Čchi-jüana	k1gFnPc2
a	a	k8xC
dělový	dělový	k2eAgInSc1d1
člun	člun	k1gInSc1
Kchuang-i	Kchuang-	k1gFnSc2
u	u	k7c2
korejského	korejský	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
Pchungdo	Pchungdo	k1gNnSc1
a	a	k8xC
o	o	k7c4
chvíli	chvíle	k1gFnSc4
později	pozdě	k6eAd2
přepadla	přepadnout	k5eAaPmAgFnS
čínské	čínský	k2eAgMnPc4d1
lodě	loď	k1gFnPc1
převážející	převážející	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
a	a	k8xC
potopila	potopit	k5eAaPmAgFnS
parník	parník	k1gInSc4
Kao-šeng	Kao-šenga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
si	se	k3xPyFc3
vyhlásily	vyhlásit	k5eAaPmAgFnP
válku	válka	k1gFnSc4
až	až	k9
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1894	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc1
polovina	polovina	k1gFnSc1
střetu	střet	k1gInSc2
se	se	k3xPyFc4
vedla	vést	k5eAaImAgFnS
především	především	k9
na	na	k7c6
území	území	k1gNnSc6
Koreje	Korea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
postupovali	postupovat	k5eAaImAgMnP
korejským	korejský	k2eAgInSc7d1
poloostrovem	poloostrov	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
již	již	k6eAd1
pod	pod	k7c7
značným	značný	k2eAgInSc7d1
japonským	japonský	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
území	území	k1gNnPc1
však	však	k9
byla	být	k5eAaImAgNnP
spíše	spíše	k9
čínská	čínský	k2eAgNnPc1d1
<g/>
:	:	kIx,
hlavně	hlavně	k9
kolem	kolem	k7c2
města	město	k1gNnSc2
Pchjongjang	Pchjongjang	k1gInSc1
<g/>
,	,	kIx,
Soul	Soul	k1gInSc1
a	a	k8xC
dál	daleko	k6eAd2
severně	severně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
problému	problém	k1gInSc2
byl	být	k5eAaImAgInS
obsazen	obsadit	k5eAaPmNgInS
Pusan	Pusan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
bitvách	bitva	k1gFnPc6
pozemních	pozemní	k2eAgFnPc2d1
a	a	k8xC
námořních	námořní	k2eAgFnPc2d1
zejména	zejména	k9
o	o	k7c4
Asan	Asan	k1gNnSc4
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Pchjongjang	Pchjongjang	k1gInSc1
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c4
Pchjongjang	Pchjongjang	k1gInSc4
musela	muset	k5eAaImAgFnS
čínská	čínský	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
opustit	opustit	k5eAaPmF
Koreu	Korea	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
námořní	námořní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
řeky	řeka	k1gFnSc2
Jalu	Jalus	k1gInSc2
postoupila	postoupit	k5eAaPmAgFnS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
až	až	k9
do	do	k7c2
Mandžuska	Mandžusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
tím	ten	k3xDgNnSc7
Japonci	Japonec	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
Tchaj-wan	Tchaj-wan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Výhra	výhra	k1gFnSc1
Japonska	Japonsko	k1gNnSc2
byla	být	k5eAaImAgFnS
důsledkem	důsledek	k1gInSc7
modernizace	modernizace	k1gFnSc2
a	a	k8xC
industrializace	industrializace	k1gFnSc2
Japonska	Japonsko	k1gNnSc2
započaté	započatý	k2eAgNnSc4d1
o	o	k7c4
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
dříve	dříve	k6eAd2
na	na	k7c6
počátku	počátek	k1gInSc6
období	období	k1gNnSc2
Meidži	Meidž	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
zajistilo	zajistit	k5eAaPmAgNnS
Japonsku	Japonsko	k1gNnSc3
pozici	pozice	k1gFnSc6
asijské	asijský	k2eAgFnPc1d1
velmoci	velmoc	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Porážka	porážka	k1gFnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
tradičního	tradiční	k2eAgMnSc2d1
vazala	vazal	k1gMnSc2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
podkopala	podkopat	k5eAaPmAgFnS
čínské	čínský	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
sebevědomí	sebevědomí	k1gNnSc4
<g/>
,	,	kIx,
už	už	k6eAd1
tak	tak	k9
dost	dost	k6eAd1
pošramocené	pošramocený	k2eAgInPc1d1
snadnými	snadný	k2eAgFnPc7d1
porážkami	porážka	k1gFnPc7
ze	z	k7c2
strany	strana	k1gFnSc2
západních	západní	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
<g/>
,	,	kIx,
především	především	k6eAd1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
v	v	k7c6
obou	dva	k4xCgFnPc6
opiových	opiový	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespokojenost	nespokojenost	k1gFnSc1
se	se	k3xPyFc4
o	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
projevila	projevit	k5eAaPmAgFnS
v	v	k7c6
proticizineckém	proticizinecký	k2eAgNnSc6d1
Boxerském	boxerský	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
LAI	LAI	kA
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chinese	Chinese	k1gFnSc1
Ironclad	Ironclad	k1gInSc4
Battleship	Battleship	k1gInSc1
vs	vs	k?
Japanese	Japanese	k1gFnSc2
Protected	Protected	k1gMnSc1
Cruiser	Cruiser	k1gMnSc1
<g/>
:	:	kIx,
Yalu	Yala	k1gMnSc4
River	River	k1gInSc4
1894	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Great	Great	k2eAgInSc1d1
Britain	Britain	k1gInSc1
<g/>
:	:	kIx,
Osprey	Osprea	k1gFnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
79	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
9781472828408	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chinese	Chinese	k1gFnSc1
Ironclad	Ironclad	k1gInSc1
Battleship	Battleship	k1gMnSc1
vs	vs	k?
Japanese	Japanese	k1gFnSc2
Protected	Protected	k1gMnSc1
Cruiser	Cruiser	k1gMnSc1
<g/>
:	:	kIx,
Yalu	Yala	k1gFnSc4
River	Rivra	k1gFnPc2
1894	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Port	porta	k1gFnPc2
Artur	Artur	k1gMnSc1
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
začala	začít	k5eAaPmAgFnS
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
471	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
745	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DUPUY	DUPUY	kA
<g/>
,	,	kIx,
R.	R.	kA
Ernest	Ernest	k1gMnSc1
<g/>
;	;	kIx,
DUPUY	DUPUY	kA
<g/>
,	,	kIx,
Trevor	Trevor	k1gMnSc1
N.	N.	kA
Vojenské	vojenský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Forma	forma	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
962	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ALLAN	Allan	k1gMnSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Under	Under	k1gMnSc1
the	the	k?
Dragon	Dragon	k1gMnSc1
Flag	flag	k1gInSc1
<g/>
:	:	kIx,
My	my	k3xPp1nPc1
Experiences	Experiencesa	k1gFnPc2
in	in	k?
the	the	k?
Chino-Japanese	Chino-Japanese	k1gFnSc1
War	War	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
FREDERICK	FREDERICK	kA
A.	A.	kA
STOKES	STOKES	kA
COMPANY	COMPANY	kA
<g/>
,	,	kIx,
1898	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CLEMENTS	CLEMENTS	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Admiral	Admiral	k1gMnSc1
Tō	Tō	k1gMnSc1
<g/>
:	:	kIx,
Nelson	Nelson	k1gMnSc1
of	of	k?
the	the	k?
East	East	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Haus	Haus	k1gInSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
906598	#num#	k4
<g/>
-	-	kIx~
<g/>
62	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
INOUYE	INOUYE	kA
<g/>
,	,	kIx,
Jukichi	Jukichi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Concise	Concise	k1gFnSc1
history	histor	k1gInPc1
of	of	k?
the	the	k?
war	war	k?
between	between	k2eAgInSc1d1
Japan	japan	k1gInSc1
and	and	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osaka	Osaka	k1gMnSc1
&	&	k?
Tokyo	Tokyo	k1gMnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KOČVAR	KOČVAR	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
o	o	k7c4
Koreu	Korea	k1gFnSc4
<g/>
:	:	kIx,
čínsko-japonské	čínsko-japonský	k2eAgNnSc1d1
soupeření	soupeření	k1gNnSc1
1868	#num#	k4
<g/>
-	-	kIx~
<g/>
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
61	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
INOUYE	INOUYE	kA
<g/>
,	,	kIx,
Jukichi	Jukichi	k1gNnSc1
<g/>
;	;	kIx,
OGAWA	OGAWA	kA
<g/>
,	,	kIx,
Kazumasa	Kazumasa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Japan-China	Japan-China	k1gMnSc1
War	War	k1gMnSc1
:	:	kIx,
on	on	k3xPp3gMnSc1
the	the	k?
regent	regent	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
sword	sword	k6eAd1
:	:	kIx,
Kinchow	Kinchow	k1gFnSc1
<g/>
,	,	kIx,
Port	port	k1gInSc1
Arthur	Arthura	k1gFnPc2
<g/>
,	,	kIx,
and	and	k?
Talienwan	Talienwan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yokohama	Yokohama	k1gNnSc1
<g/>
:	:	kIx,
Kelly	Kella	k1gFnPc1
&	&	k?
Walsh	Walsh	k1gMnSc1
<g/>
,	,	kIx,
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LENGERER	LENGERER	kA
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval	navalit	k5eAaPmRp2nS
Operations	Operations	k1gInSc1
in	in	k?
the	the	k?
Sino-Japanese	Sino-Japanese	k1gFnSc1
War	War	k1gFnSc1
–	–	k?
Part	parta	k1gFnPc2
I.	I.	kA
In	In	k1gFnSc2
<g/>
:	:	kIx,
LENGERER	LENGERER	kA
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
<g/>
;	;	kIx,
AHLBERG	AHLBERG	kA
<g/>
,	,	kIx,
Lars	Lars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Contributions	Contributions	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc2
Warships	Warshipsa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ebook	Ebook	k1gInSc1
Paper	Paper	k1gInSc4
XII	XII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
47	#num#	k4
až	až	k9
68	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LENGERER	LENGERER	kA
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval	navalit	k5eAaPmRp2nS
Operations	Operations	k1gInSc1
in	in	k?
the	the	k?
Sino-Japanese	Sino-Japanese	k1gFnSc1
War	War	k1gFnSc1
–	–	k?
Part	parta	k1gFnPc2
II	II	kA
<g/>
:	:	kIx,
After	After	k1gMnSc1
the	the	k?
Declaration	Declaration	k1gInSc1
of	of	k?
War	War	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
LENGERER	LENGERER	kA
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
<g/>
;	;	kIx,
AHLBERG	AHLBERG	kA
<g/>
,	,	kIx,
Lars	Lars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Contributions	Contributions	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc2
Warships	Warshipsa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ebook	Ebook	k1gInSc1
Paper	Paper	k1gInSc4
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
29	#num#	k4
až	až	k9
59	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
OLENDER	OLENDER	kA
<g/>
,	,	kIx,
Piotr	Piotr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sino-Japanese	Sino-Japanese	k1gFnSc1
Naval	navalit	k5eAaPmRp2nS
War	War	k1gMnSc7
1894	#num#	k4
<g/>
-	-	kIx~
<g/>
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sandomierz	Sandomierz	k1gInSc1
<g/>
:	:	kIx,
Stratus	stratus	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Maritime	Maritim	k1gMnSc5
Series	Series	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
3105	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
63678	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PAINE	PAINE	kA
<g/>
,	,	kIx,
S.	S.	kA
<g/>
C.	C.	kA
<g/>
M.	M.	kA
The	The	k1gFnSc1
Sino-Japanese	Sino-Japanese	k1gFnSc1
War	War	k1gFnSc1
of	of	k?
1894	#num#	k4
<g/>
-	-	kIx~
<g/>
1895	#num#	k4
<g/>
:	:	kIx,
Perceptions	Perceptions	k1gInSc1
<g/>
,	,	kIx,
Power	Power	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Primacy	Primaca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
61745	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
RAWLINSON	RAWLINSON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
L.	L.	kA
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Struggle	Struggle	k1gNnSc7
for	forum	k1gNnPc2
Naval	navalit	k5eAaPmRp2nS
Development	Development	k1gInSc1
1839	#num#	k4
<g/>
-	-	kIx~
<g/>
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
:	:	kIx,
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Vladimir	Vladimir	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Zenone	Zenon	k1gMnSc5
Volpicelli	Volpicell	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
China-Japan	China-Japan	k1gMnSc1
war	war	k?
compiled	compiled	k1gMnSc1
from	from	k1gMnSc1
Japanese	Japanese	k1gFnSc1
<g/>
,	,	kIx,
Chinese	Chinese	k1gFnSc1
and	and	k?
foreign	foreign	k1gMnSc1
sources	sources	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
C.	C.	kA
Scribner	Scribner	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
1896	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WARRINGTON	WARRINGTON	kA
<g/>
,	,	kIx,
F.	F.	kA
Eastlake	Eastlake	k1gFnSc1
<g/>
;	;	kIx,
YAMADA	YAMADA	kA
<g/>
,	,	kIx,
Yoshiaki	Yoshiaki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heroic	Heroic	k1gMnSc1
Japan	japan	k1gInSc1
<g/>
:	:	kIx,
a	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
war	war	k?
between	between	k1gInSc1
China	China	k1gFnSc1
&	&	k?
Japan	japan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
SAMPSON	SAMPSON	kA
LOW	LOW	kA
<g/>
,	,	kIx,
MAESTON	MAESTON	kA
&	&	k?
COMPANY	COMPANY	kA
<g/>
,	,	kIx,
1897	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WHITE	WHITE	kA
<g/>
,	,	kIx,
Trumbull	Trumbull	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
war	war	k?
in	in	k?
the	the	k?
East	East	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japan	japan	k1gInSc1
<g/>
,	,	kIx,
China	China	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Corea	Corea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
complete	complést	k5eAaPmIp3nS
history	histor	k1gInPc4
of	of	k?
the	the	k?
war	war	k?
<g/>
:	:	kIx,
Its	Its	k1gMnSc1
causes	causes	k1gMnSc1
and	and	k?
results	results	k1gInSc1
<g/>
;	;	kIx,
its	its	k?
campaigns	campaigns	k1gInSc1
on	on	k3xPp3gMnSc1
sea	sea	k?
and	and	k?
land	land	k1gInSc1
<g/>
;	;	kIx,
its	its	k?
terrific	terrific	k1gMnSc1
fights	fights	k6eAd1
<g/>
,	,	kIx,
grand	grand	k1gMnSc1
victories	victories	k1gMnSc1
and	and	k?
overwhelming	overwhelming	k1gInSc1
defeats	defeats	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
With	Witha	k1gFnPc2
a	a	k8xC
preliminary	preliminara	k1gFnSc2
account	account	k1gMnSc1
of	of	k?
the	the	k?
customs	customs	k1gInSc1
<g/>
,	,	kIx,
habits	habits	k6eAd1
and	and	k?
history	histor	k1gInPc1
of	of	k?
the	the	k?
three	thre	k1gFnSc2
peoples	peoples	k1gMnSc1
involved	involved	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Their	Their	k1gMnSc1
cities	cities	k1gMnSc1
<g/>
,	,	kIx,
arts	arts	k1gInSc1
<g/>
,	,	kIx,
sciences	sciences	k1gInSc1
<g/>
,	,	kIx,
amusements	amusements	k1gInSc1
and	and	k?
literature	literatur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
Philadelphia	Philadelphia	k1gFnSc1
<g/>
:	:	kIx,
P.	P.	kA
<g/>
W.	W.	kA
Ziegler	Ziegler	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
,	,	kIx,
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
první	první	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
The	The	k?
Sino-Japanese	Sino-Japanese	k1gFnSc1
War	War	k1gFnSc1
of	of	k?
1894-1895	1894-1895	k4
:	:	kIx,
as	as	k1gInSc1
seen	seen	k1gInSc1
in	in	k?
prints	prints	k1gInSc1
and	and	k?
archives	archives	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japan	japan	k1gInSc1
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Asian	Asian	k1gMnSc1
Historical	Historical	k1gFnSc2
Records	Records	k1gInSc1
&	&	k?
British	British	k1gInSc1
Library	Librara	k1gFnSc2
<g/>
,	,	kIx,
2014-05-27	2014-05-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
První	první	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
Bitvy	bitva	k1gFnSc2
</s>
<s>
Asan	Asan	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Pungdo	Pungdo	k1gNnSc1
•	•	k?
Sŏ	Sŏ	k1gMnSc1
•	•	k?
Pchjongjang	Pchjongjang	k1gInSc1
•	•	k?
Jalu	Jalus	k1gInSc2
•	•	k?
Ťiou-lien-čcheng	Ťiou-lien-čcheng	k1gMnSc1
•	•	k?
Lü-šun-kchou	Lü-šun-kcha	k1gMnSc7
•	•	k?
Wej-chaj-wej	Wej-chaj-wej	k1gInSc1
•	•	k?
Jing-kchou	Jing-kcha	k1gMnSc7
•	•	k?
Pescadory	Pescador	k1gInPc4
Důsledky	důsledek	k1gInPc1
</s>
<s>
Šimonosecká	Šimonosecký	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
•	•	k?
Intervence	intervence	k1gFnSc1
Tří	tři	k4xCgNnPc2
•	•	k?
Republika	republika	k1gFnSc1
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Japonská	japonský	k2eAgFnSc1d1
invaze	invaze	k1gFnSc1
na	na	k7c4
Tchaj-wan	Tchaj-wan	k1gInSc4
•	•	k?
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
|	|	kIx~
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Korea	Korea	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
