<s>
Armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
je	být	k5eAaImIp3nS	být
generálská	generálský	k2eAgFnSc1d1	generálská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
používaná	používaný	k2eAgFnSc1d1	používaná
armádami	armáda	k1gFnPc7	armáda
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádách	armáda	k1gFnPc6	armáda
užívajících	užívající	k2eAgFnPc6d1	užívající
hodnost	hodnost	k1gFnSc4	hodnost
maršála	maršál	k1gMnSc2	maršál
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
generálská	generálský	k2eAgFnSc1d1	generálská
hodnost	hodnost	k1gFnSc1	hodnost
v	v	k7c6	v
Armádě	armáda	k1gFnSc6	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc4d3	nejbližší
nižší	nízký	k2eAgFnSc4d2	nižší
hodnost	hodnost	k1gFnSc4	hodnost
je	být	k5eAaImIp3nS	být
generálporučík	generálporučík	k1gMnSc1	generálporučík
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
náčelníka	náčelník	k1gMnSc2	náčelník
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
Armády	armáda	k1gFnSc2	armáda
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jedinou	jediný	k2eAgFnSc7d1	jediná
výše	vysoce	k6eAd2	vysoce
postavenou	postavený	k2eAgFnSc7d1	postavená
osobou	osoba	k1gFnSc7	osoba
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
českých	český	k2eAgFnPc2d1	Česká
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
<g/>
:	:	kIx,	:
čtyři	čtyři	k4xCgFnPc1	čtyři
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
zlaté	zlatý	k2eAgFnPc1d1	zlatá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
zpravidla	zpravidla	k6eAd1	zpravidla
velí	velet	k5eAaImIp3nS	velet
jedné	jeden	k4xCgFnSc3	jeden
armádě	armáda	k1gFnSc3	armáda
nebo	nebo	k8xC	nebo
sboru	sbor	k1gInSc3	sbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
jmenováno	jmenovat	k5eAaBmNgNnS	jmenovat
sedm	sedm	k4xCc1	sedm
armádních	armádní	k2eAgMnPc2d1	armádní
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
generála	generál	k1gMnSc4	generál
Sedláčka	Sedláček	k1gMnSc4	Sedláček
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
vždy	vždy	k6eAd1	vždy
zastávali	zastávat	k5eAaImAgMnP	zastávat
funkci	funkce	k1gFnSc4	funkce
náčelníka	náčelník	k1gMnSc2	náčelník
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
AČR	AČR	kA	AČR
<g/>
:	:	kIx,	:
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1993	[number]	k4	1993
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
Pezl	Pezl	k1gMnSc1	Pezl
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
-	-	kIx~	-
Jiří	Jiří	k1gMnSc1	Jiří
Šedivý	Šedivý	k1gMnSc1	Šedivý
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
-	-	kIx~	-
Pavel	Pavel	k1gMnSc1	Pavel
Štefka	Štefka	k1gMnSc1	Štefka
<g />
.	.	kIx.	.
</s>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
-	-	kIx~	-
Tomáš	Tomáš	k1gMnSc1	Tomáš
Sedláček	Sedláček	k1gMnSc1	Sedláček
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
-	-	kIx~	-
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Picek	Picek	k1gMnSc1	Picek
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
-	-	kIx~	-
Petr	Petr	k1gMnSc1	Petr
Pavel	Pavel	k1gMnSc1	Pavel
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Bečvář	Bečvář	k1gMnSc1	Bečvář
Armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
général	générat	k5eAaImAgMnS	générat
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
armée	armée	k1gInSc1	armée
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
generálská	generálský	k2eAgFnSc1d1	generálská
hodnost	hodnost	k1gFnSc1	hodnost
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
označení	označení	k1gNnSc1	označení
hodnosti	hodnost	k1gFnSc2	hodnost
používané	používaný	k2eAgFnSc2d1	používaná
divizními	divizní	k2eAgMnPc7d1	divizní
generály	generál	k1gMnPc7	generál
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
vojenských	vojenský	k2eAgFnPc6d1	vojenská
funkcích	funkce	k1gFnPc6	funkce
-	-	kIx~	-
náčelník	náčelník	k1gMnSc1	náčelník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
štábu	štáb	k1gInSc2	štáb
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
pět	pět	k4xCc1	pět
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
náramennících	náramenník	k1gInPc6	náramenník
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
hodnost	hodnost	k1gFnSc4	hodnost
armádního	armádní	k2eAgMnSc2d1	armádní
generála	generál	k1gMnSc2	generál
(	(	kIx(	(
<g/>
г	г	k?	г
а	а	k?	а
<g/>
)	)	kIx)	)
zavedena	zaveden	k2eAgFnSc1d1	zavedena
v	v	k7c6	v
Rudé	rudý	k2eAgFnSc6d1	rudá
armádě	armáda	k1gFnSc6	armáda
při	při	k7c6	při
přejmenování	přejmenování	k1gNnSc6	přejmenování
generálských	generálský	k2eAgFnPc2d1	generálská
hodností	hodnost	k1gFnPc2	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
maršála	maršál	k1gMnSc4	maršál
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
)	)	kIx)	)
a	a	k8xC	a
generálplukovníka	generálplukovník	k1gMnSc2	generálplukovník
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgMnSc1d2	nižší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
hodnosti	hodnost	k1gFnPc4	hodnost
admirála	admirál	k1gMnSc2	admirál
loďstva	loďstvo	k1gNnSc2	loďstvo
v	v	k7c6	v
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1940	[number]	k4	1940
byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
první	první	k4xOgMnPc1	první
tři	tři	k4xCgFnPc4	tři
armádní	armádní	k2eAgFnPc4d1	armádní
generálové	generálová	k1gFnPc4	generálová
<g/>
,	,	kIx,	,
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
,	,	kIx,	,
hodnost	hodnost	k1gFnSc4	hodnost
získávali	získávat	k5eAaImAgMnP	získávat
zpravidla	zpravidla	k6eAd1	zpravidla
velitelé	velitel	k1gMnPc1	velitel
frontů	front	k1gInPc2	front
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
jmenováno	jmenovat	k5eAaBmNgNnS	jmenovat
armádními	armádní	k2eAgNnPc7d1	armádní
generály	generál	k1gMnPc7	generál
133	[number]	k4	133
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
hodnost	hodnost	k1gFnSc1	hodnost
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
též	též	k9	též
pro	pro	k7c4	pro
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
představitele	představitel	k1gMnPc4	představitel
rezortů	rezort	k1gInPc2	rezort
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1940	[number]	k4	1940
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
této	tento	k3xDgFnSc2	tento
hodnosti	hodnost	k1gFnSc2	hodnost
komandarm	komandarm	k1gInSc1	komandarm
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
I.	I.	kA	I.
stupně	stupeň	k1gInPc1	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
Army	Arma	k1gMnSc2	Arma
general	generat	k5eAaPmAgMnS	generat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzácně	vzácně	k6eAd1	vzácně
používaná	používaný	k2eAgFnSc1d1	používaná
hodnost	hodnost	k1gFnSc1	hodnost
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k9	jako
(	(	kIx(	(
<g/>
čtyřhvězdičková	čtyřhvězdičkový	k2eAgFnSc1d1	čtyřhvězdičková
<g/>
)	)	kIx)	)
hodnost	hodnost	k1gFnSc1	hodnost
pro	pro	k7c4	pro
generála	generál	k1gMnSc4	generál
Ulyssese	Ulyssese	k1gFnSc2	Ulyssese
S.	S.	kA	S.
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
Grantovy	Grantovy	k?	Grantovy
nástupce	nástupce	k1gMnSc1	nástupce
W.	W.	kA	W.
T.	T.	kA	T.
Shermana	Sherman	k1gMnSc4	Sherman
a	a	k8xC	a
P.	P.	kA	P.
H.	H.	kA	H.
Sheridana	Sheridan	k1gMnSc2	Sheridan
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
armádní	armádní	k2eAgFnPc1d1	armádní
generálové	generálová	k1gFnPc1	generálová
(	(	kIx(	(
<g/>
s	s	k7c7	s
pěti	pět	k4xCc7	pět
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
až	až	k9	až
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
G.	G.	kA	G.
<g/>
Marshall	Marshall	k1gInSc1	Marshall
<g/>
,	,	kIx,	,
D.	D.	kA	D.
MacArthur	MacArthur	k1gMnSc1	MacArthur
<g/>
,	,	kIx,	,
D.	D.	kA	D.
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gInSc4	Eisenhower
a	a	k8xC	a
H.	H.	kA	H.
H.	H.	kA	H.
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O.	O.	kA	O.
Bradley	Bradlea	k1gFnSc2	Bradlea
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
v	v	k7c6	v
září	září	k1gNnSc6	září
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
dosažitelnou	dosažitelný	k2eAgFnSc7d1	dosažitelná
hodností	hodnost	k1gFnSc7	hodnost
pro	pro	k7c4	pro
generály	generál	k1gMnPc4	generál
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
hodnost	hodnost	k1gFnSc1	hodnost
generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
general	generat	k5eAaPmAgInS	generat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
s	s	k7c7	s
převzetím	převzetí	k1gNnSc7	převzetí
sovětského	sovětský	k2eAgInSc2d1	sovětský
systému	systém	k1gInSc2	systém
generálských	generálský	k2eAgFnPc2d1	generálská
hodností	hodnost	k1gFnPc2	hodnost
převzaly	převzít	k5eAaPmAgInP	převzít
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
i	i	k9	i
tuto	tento	k3xDgFnSc4	tento
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
pro	pro	k7c4	pro
ministry	ministr	k1gMnPc4	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
je	být	k5eAaImIp3nS	být
hodnost	hodnost	k1gFnSc1	hodnost
používána	používán	k2eAgFnSc1d1	používána
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
státech	stát	k1gInPc6	stát
-	-	kIx~	-
Indonésie	Indonésie	k1gFnPc1	Indonésie
<g/>
,	,	kIx,	,
Libérie	Libérie	k1gFnSc1	Libérie
<g/>
,	,	kIx,	,
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Hodnosti	hodnost	k1gFnPc1	hodnost
-	-	kIx~	-
české	český	k2eAgFnPc1d1	Česká
hodnosti	hodnost	k1gFnPc1	hodnost
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
army	arma	k1gFnSc2	arma
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Г	Г	k?	Г
а	а	k?	а
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
General	General	k1gFnSc6	General
of	of	k?	of
the	the	k?	the
Army	Arma	k1gFnSc2	Arma
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
