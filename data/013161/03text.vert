<p>
<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
CHKO	CHKO	kA	CHKO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
velkoplošné	velkoplošný	k2eAgNnSc4d1	velkoplošné
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaký	jaký	k3yIgInSc4	jaký
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
CHKO	CHKO	kA	CHKO
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
==	==	k?	==
</s>
</p>
<p>
<s>
Chráněné	chráněný	k2eAgFnPc1d1	chráněná
krajinné	krajinný	k2eAgFnPc1d1	krajinná
oblasti	oblast	k1gFnPc1	oblast
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
==	==	k?	==
CHKO	CHKO	kA	CHKO
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
překládá	překládat	k5eAaImIp3nS	překládat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Protected	Protected	k1gInSc1	Protected
Landscape	Landscap	k1gInSc5	Landscap
Area	area	k1gFnSc1	area
<g/>
,	,	kIx,	,
PLA	PLA	kA	PLA
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
a	a	k8xC	a
slovenské	slovenský	k2eAgNnSc1d1	slovenské
CHKO	CHKO	kA	CHKO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Landscape	Landscap	k1gInSc5	Landscap
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
polské	polský	k2eAgInPc4d1	polský
parky	park	k1gInPc4	park
krajobrazowe	krajobrazowat	k5eAaPmIp3nS	krajobrazowat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Landschaftsschutzgebiet	Landschaftsschutzgebiet	k1gInSc1	Landschaftsschutzgebiet
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
a	a	k8xC	a
slovenské	slovenský	k2eAgNnSc1d1	slovenské
CHKO	CHKO	kA	CHKO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Landschaftsschutzpark	Landschaftsschutzpark	k1gInSc1	Landschaftsschutzpark
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
polské	polský	k2eAgInPc4d1	polský
parky	park	k1gInPc4	park
krajobrazowe	krajobrazowat	k5eAaPmIp3nS	krajobrazowat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
park	park	k1gInSc1	park
krajobrazowy	krajobrazowa	k1gFnSc2	krajobrazowa
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
polské	polský	k2eAgNnSc4d1	polské
PK	PK	kA	PK
<g/>
)	)	kIx)	)
popřípadě	popřípadě	k6eAd1	popřípadě
obszar	obszar	k1gInSc1	obszar
przyrody	przyroda	k1gFnSc2	przyroda
chronionej	chronionat	k5eAaPmRp2nS	chronionat
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
a	a	k8xC	a
slovenské	slovenský	k2eAgNnSc1d1	slovenské
CHKO	CHKO	kA	CHKO
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
chránená	chránený	k2eAgFnSc1d1	chránená
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblasť	oblastit	k5eAaPmRp2nS	oblastit
</s>
</p>
<p>
<s>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
р	р	k?	р
л	л	k?	л
п	п	k?	п
</s>
</p>
<p>
<s>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
о	о	k?	о
п	п	k?	п
о	о	k?	о
</s>
</p>
<p>
<s>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
tájvedelmi	tájvedel	k1gFnPc7	tájvedel
körzet	körzet	k5eAaPmF	körzet
</s>
</p>
<p>
<s>
litevsky	litevsky	k6eAd1	litevsky
<g/>
:	:	kIx,	:
regioninis	regioninis	k1gFnSc1	regioninis
parkas	parkasa	k1gFnPc2	parkasa
</s>
</p>
<p>
<s>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Area	area	k1gFnSc1	area
nature	natur	k1gInSc5	natur
protecta	protecto	k1gNnPc4	protecto
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
CHKO	CHKO	kA	CHKO
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
CHKO	CHKO	kA	CHKO
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
