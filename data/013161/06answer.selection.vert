<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
CHKO	CHKO	kA	CHKO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
velkoplošné	velkoplošný	k2eAgNnSc4d1	velkoplošné
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaký	jaký	k3yQgInSc4	jaký
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
