<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
rychlosti	rychlost	k1gFnSc6	rychlost
vozidla	vozidlo	k1gNnSc2	vozidlo
po	po	k7c6	po
mokré	mokrý	k2eAgFnSc6d1	mokrá
vozovce	vozovka	k1gFnSc6	vozovka
<g/>
?	?	kIx.	?
</s>
