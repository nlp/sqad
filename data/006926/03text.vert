<s>
Aquaplaning	Aquaplaning	k1gInSc1	Aquaplaning
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
též	též	k9	též
akvaplanink	akvaplanink	k1gInSc1	akvaplanink
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
rychlosti	rychlost	k1gFnSc6	rychlost
vozidla	vozidlo	k1gNnSc2	vozidlo
na	na	k7c6	na
mokré	mokrý	k2eAgFnSc6d1	mokrá
vozovce	vozovka	k1gFnSc6	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
ztráta	ztráta	k1gFnSc1	ztráta
přilnavosti	přilnavost	k1gFnSc2	přilnavost
pneumatiky	pneumatika	k1gFnSc2	pneumatika
k	k	k7c3	k
vozovce	vozovka	k1gFnSc3	vozovka
vlivem	vlivem	k7c2	vlivem
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
mezi	mezi	k7c4	mezi
pneumatiku	pneumatika	k1gFnSc4	pneumatika
a	a	k8xC	a
vozovku	vozovka	k1gFnSc4	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Pneumatika	pneumatika	k1gFnSc1	pneumatika
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
vozovky	vozovka	k1gFnPc4	vozovka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vodního	vodní	k2eAgInSc2d1	vodní
klínu	klín	k1gInSc2	klín
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
špatného	špatný	k2eAgNnSc2d1	špatné
odvádění	odvádění	k1gNnSc2	odvádění
vody	voda	k1gFnSc2	voda
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
ovladatelnosti	ovladatelnost	k1gFnSc2	ovladatelnost
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
ve	v	k7c4	v
vážnou	vážný	k2eAgFnSc4d1	vážná
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc4	vznik
aquaplaningu	aquaplaning	k1gInSc2	aquaplaning
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
tři	tři	k4xCgInPc4	tři
hlavní	hlavní	k2eAgInPc4d1	hlavní
faktory	faktor	k1gInPc4	faktor
<g/>
:	:	kIx,	:
rychlost	rychlost	k1gFnSc1	rychlost
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
vozovce	vozovka	k1gFnSc6	vozovka
a	a	k8xC	a
typ	typ	k1gInSc1	typ
a	a	k8xC	a
hloubka	hloubka	k1gFnSc1	hloubka
dezénu	dezén	k1gInSc2	dezén
pneumatiky	pneumatika	k1gFnSc2	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
aquaplaningu	aquaplaning	k1gInSc3	aquaplaning
dochází	docházet	k5eAaImIp3nS	docházet
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
drážky	drážka	k1gFnPc1	drážka
dezénu	dezén	k1gInSc2	dezén
pneumatiky	pneumatika	k1gFnPc1	pneumatika
nestačí	stačit	k5eNaBmIp3nP	stačit
odvádět	odvádět	k5eAaImF	odvádět
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
styku	styk	k1gInSc2	styk
pneumatiky	pneumatika	k1gFnSc2	pneumatika
s	s	k7c7	s
vozovkou	vozovka	k1gFnSc7	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
hloubce	hloubka	k1gFnSc6	hloubka
dezénu	dezén	k1gInSc2	dezén
pneumatiky	pneumatika	k1gFnSc2	pneumatika
<g/>
,	,	kIx,	,
zvyšování	zvyšování	k1gNnSc1	zvyšování
rychlosti	rychlost	k1gFnSc2	rychlost
vozidla	vozidlo	k1gNnSc2	vozidlo
nebo	nebo	k8xC	nebo
zvyšování	zvyšování	k1gNnSc2	zvyšování
hladiny	hladina	k1gFnSc2	hladina
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
nestačí	stačit	k5eNaBmIp3nS	stačit
odvádět	odvádět	k5eAaImF	odvádět
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
mezi	mezi	k7c7	mezi
pneumatikou	pneumatika	k1gFnSc7	pneumatika
a	a	k8xC	a
vozovkou	vozovka	k1gFnSc7	vozovka
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
souvislá	souvislý	k2eAgFnSc1d1	souvislá
vrstva	vrstva	k1gFnSc1	vrstva
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
za	za	k7c2	za
mokra	mokro	k1gNnSc2	mokro
mají	mít	k5eAaImIp3nP	mít
pneumatiky	pneumatika	k1gFnPc4	pneumatika
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgFnSc1d2	menší
přilnavost	přilnavost	k1gFnSc1	přilnavost
než	než	k8xS	než
na	na	k7c6	na
suché	suchý	k2eAgFnSc6d1	suchá
vozovce	vozovka	k1gFnSc6	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Smyk	smyk	k1gInSc1	smyk
nebo	nebo	k8xC	nebo
problémy	problém	k1gInPc1	problém
při	při	k7c6	při
brždění	bržděný	k2eAgMnPc1d1	bržděný
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
když	když	k8xS	když
nenastal	nastat	k5eNaPmAgInS	nastat
aquaplaning	aquaplaning	k1gInSc1	aquaplaning
<g/>
.	.	kIx.	.
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
pozornost	pozornost	k1gFnSc1	pozornost
na	na	k7c4	na
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
kaluže	kaluž	k1gFnPc1	kaluž
<g/>
,	,	kIx,	,
koleje	kolej	k1gFnPc1	kolej
vyjeté	vyjetý	k2eAgFnPc1d1	vyjetá
kamiony	kamion	k1gInPc4	kamion
<g/>
,	,	kIx,	,
na	na	k7c4	na
strouhy	strouha	k1gFnPc4	strouha
napříč	napříč	k7c7	napříč
vozovkou	vozovka	k1gFnSc7	vozovka
po	po	k7c6	po
prudkém	prudký	k2eAgInSc6d1	prudký
dešti	dešť	k1gInSc6	dešť
pravidelně	pravidelně	k6eAd1	pravidelně
sledovat	sledovat	k5eAaImF	sledovat
stopy	stopa	k1gFnPc4	stopa
vašeho	váš	k3xOp2gInSc2	váš
automobilu	automobil	k1gInSc2	automobil
ve	v	k7c6	v
zpětném	zpětný	k2eAgNnSc6d1	zpětné
zrcátku	zrcátko	k1gNnSc6	zrcátko
zřetelnější	zřetelný	k2eAgFnSc2d2	zřetelnější
a	a	k8xC	a
trvanlivější	trvanlivý	k2eAgFnSc2d2	trvanlivější
stopy	stopa	k1gFnSc2	stopa
<g />
.	.	kIx.	.
</s>
<s>
pneumatik	pneumatika	k1gFnPc2	pneumatika
značí	značit	k5eAaImIp3nS	značit
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
povrch	povrch	k1gInSc4	povrch
stopy	stopa	k1gFnSc2	stopa
rychle	rychle	k6eAd1	rychle
se	s	k7c7	s
plnící	plnící	k2eAgFnSc7d1	plnící
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
ztrácející	ztrácející	k2eAgMnSc1d1	ztrácející
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
snížení	snížení	k1gNnSc1	snížení
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
plochy	plocha	k1gFnSc2	plocha
pneumatiky	pneumatika	k1gFnSc2	pneumatika
a	a	k8xC	a
vozovky	vozovka	k1gFnSc2	vozovka
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
silnější	silný	k2eAgInPc1d2	silnější
rázy	ráz	k1gInPc1	ráz
do	do	k7c2	do
volantu	volant	k1gInSc2	volant
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
vodní	vodní	k2eAgInSc4d1	vodní
klín	klín	k1gInSc4	klín
mezi	mezi	k7c4	mezi
pneumatiku	pneumatika	k1gFnSc4	pneumatika
a	a	k8xC	a
vozovku	vozovka	k1gFnSc4	vozovka
při	při	k7c6	při
vznikajícím	vznikající	k2eAgInSc6d1	vznikající
aquaplaningu	aquaplaning	k1gInSc6	aquaplaning
je	být	k5eAaImIp3nS	být
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
brzdit	brzdit	k5eAaImF	brzdit
a	a	k8xC	a
točit	točit	k5eAaImF	točit
volantem	volant	k1gInSc7	volant
-	-	kIx~	-
kola	kola	k1gFnSc1	kola
musí	muset	k5eAaImIp3nS	muset
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
směru	směr	k1gInSc6	směr
a	a	k8xC	a
být	být	k5eAaImF	být
nebrzděná	brzděný	k2eNgFnSc1d1	nebrzděná
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
hrozí	hrozit	k5eAaImIp3nS	hrozit
smyk	smyk	k1gInSc4	smyk
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
roztočení	roztočení	k1gNnSc4	roztočení
vozidla	vozidlo	k1gNnSc2	vozidlo
ideální	ideální	k2eAgFnSc2d1	ideální
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vznikajícím	vznikající	k2eAgInSc6d1	vznikající
aquaplaningu	aquaplaning	k1gInSc6	aquaplaning
povolit	povolit	k5eAaPmF	povolit
pedál	pedál	k1gInSc4	pedál
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
sešlápnout	sešlápnout	k5eAaPmF	sešlápnout
spojku	spojka	k1gFnSc4	spojka
pro	pro	k7c4	pro
uvolnění	uvolnění	k1gNnSc4	uvolnění
hnacích	hnací	k2eAgNnPc2d1	hnací
kol	kolo	k1gNnPc2	kolo
-	-	kIx~	-
vznikající	vznikající	k2eAgInSc1d1	vznikající
vodní	vodní	k2eAgInSc1d1	vodní
klín	klín	k1gInSc1	klín
začne	začít	k5eAaPmIp3nS	začít
vozidlo	vozidlo	k1gNnSc4	vozidlo
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
a	a	k8xC	a
obnoví	obnovit	k5eAaPmIp3nS	obnovit
se	se	k3xPyFc4	se
původní	původní	k2eAgInSc1d1	původní
kontakt	kontakt	k1gInSc1	kontakt
pneumatik	pneumatika	k1gFnPc2	pneumatika
s	s	k7c7	s
vozovkou	vozovka	k1gFnSc7	vozovka
</s>
