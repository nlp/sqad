<p>
<s>
Celluloid	Celluloid	k1gInSc1	Celluloid
Records	Recordsa	k1gFnPc2	Recordsa
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc4d1	americké
hudební	hudební	k2eAgNnSc4d1	hudební
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
založil	založit	k5eAaPmAgMnS	založit
Jean	Jean	k1gMnSc1	Jean
Georgakarakos	Georgakarakos	k1gMnSc1	Georgakarakos
<g/>
.	.	kIx.	.
</s>
<s>
Věnovalo	věnovat	k5eAaPmAgNnS	věnovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
experimentální	experimentální	k2eAgInSc1d1	experimentální
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Georgakarakos	Georgakarakos	k1gInSc1	Georgakarakos
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
hudebníkem	hudebník	k1gMnSc7	hudebník
Billem	Bill	k1gMnSc7	Bill
Laswellem	Laswell	k1gMnSc7	Laswell
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
producentem	producent	k1gMnSc7	producent
většiny	většina	k1gFnSc2	většina
nahrávek	nahrávka	k1gFnPc2	nahrávka
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
nahrávky	nahrávka	k1gFnPc4	nahrávka
zde	zde	k6eAd1	zde
vydávali	vydávat	k5eAaPmAgMnP	vydávat
například	například	k6eAd1	například
James	James	k1gMnSc1	James
Chance	Chanec	k1gInSc2	Chanec
<g/>
,	,	kIx,	,
Mory	mora	k1gFnSc2	mora
Kanté	Kantý	k2eAgFnSc2d1	Kantý
<g/>
,	,	kIx,	,
Modern	Modern	k1gMnSc1	Modern
Guy	Guy	k1gFnSc2	Guy
nebo	nebo	k8xC	nebo
Youssou	Youssa	k1gFnSc7	Youssa
N	N	kA	N
<g/>
'	'	kIx"	'
<g/>
Dour	Dour	k1gMnSc1	Dour
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
