<p>
<s>
Víra	víra	k1gFnSc1	víra
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k6eAd1	též
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
bahaismus	bahaismus	k1gInSc1	bahaismus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
abrahámovské	abrahámovský	k2eAgNnSc1d1	abrahámovské
monoteistické	monoteistický	k2eAgNnSc1d1	monoteistické
náboženství	náboženství	k1gNnSc1	náboženství
vycházející	vycházející	k2eAgFnSc1d1	vycházející
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
starších	starý	k2eAgNnPc2d2	starší
abrahámovských	abrahámovský	k2eAgNnPc2d1	abrahámovské
náboženství	náboženství	k1gNnPc2	náboženství
a	a	k8xC	a
hlásající	hlásající	k2eAgFnSc4d1	hlásající
jednotu	jednota	k1gFnSc4	jednota
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
je	být	k5eAaImIp3nS	být
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věřícími	věřící	k2eAgMnPc7d1	věřící
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
posledního	poslední	k2eAgNnSc2d1	poslední
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
Božích	boží	k2eAgMnPc2d1	boží
Poslů	posel	k1gMnPc2	posel
<g/>
,	,	kIx,	,
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Abrahám	Abrahám	k1gMnSc1	Abrahám
<g/>
,	,	kIx,	,
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
,	,	kIx,	,
Kršna	Kršna	k1gMnSc1	Kršna
<g/>
,	,	kIx,	,
Zarathuštra	Zarathuštra	k1gFnSc1	Zarathuštra
<g/>
,	,	kIx,	,
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
víry	víra	k1gFnSc2	víra
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Báb	Báb	k1gMnPc5	Báb
===	===	k?	===
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
víry	víra	k1gFnSc2	víra
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mladý	mladý	k2eAgMnSc1d1	mladý
kupec	kupec	k1gMnSc1	kupec
z	z	k7c2	z
perského	perský	k2eAgNnSc2d1	perské
města	město	k1gNnSc2	město
Šírázu	Šíráz	k1gInSc2	Šíráz
začal	začít	k5eAaPmAgMnS	začít
lidstvo	lidstvo	k1gNnSc4	lidstvo
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
příchod	příchod	k1gInSc4	příchod
Božího	boží	k2eAgInSc2d1	boží
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
měla	mít	k5eAaImAgFnS	mít
údajně	údajně	k6eAd1	údajně
očekávala	očekávat	k5eAaImAgNnP	očekávat
všechna	všechen	k3xTgNnPc1	všechen
světová	světový	k2eAgNnPc1d1	světové
náboženství	náboženství	k1gNnPc1	náboženství
a	a	k8xC	a
jehož	jehož	k3xOyRp3gNnPc2	jehož
zjevení	zjevení	k1gNnPc2	zjevení
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
přislíbeno	přislíbit	k5eAaPmNgNnS	přislíbit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
svatých	svatý	k2eAgInPc6d1	svatý
textech	text	k1gInPc6	text
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mladý	mladý	k2eAgInSc1d1	mladý
Peršan	peršan	k1gInSc1	peršan
se	se	k3xPyFc4	se
symbolicky	symbolicky	k6eAd1	symbolicky
nazval	nazvat	k5eAaPmAgInS	nazvat
titulem	titul	k1gInSc7	titul
Báb	Báb	k1gFnSc2	Báb
<g/>
,	,	kIx,	,
značícím	značící	k2eAgInSc7d1	značící
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pomyslně	pomyslně	k6eAd1	pomyslně
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příchod	příchod	k1gInSc1	příchod
ohlašoval	ohlašovat	k5eAaImAgInS	ohlašovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
zbrusu	zbrusu	k6eAd1	zbrusu
nového	nový	k2eAgNnSc2d1	nové
náboženství	náboženství	k1gNnSc2	náboženství
bábismu	bábismus	k1gInSc2	bábismus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
byl	být	k5eAaImAgMnS	být
Báb	Báb	k1gMnSc1	Báb
nejprve	nejprve	k6eAd1	nejprve
vězněn	věznit	k5eAaImNgMnS	věznit
a	a	k8xC	a
pak	pak	k6eAd1	pak
brutálně	brutálně	k6eAd1	brutálně
popraven	popravit	k5eAaPmNgInS	popravit
perskými	perský	k2eAgInPc7d1	perský
úřady	úřad	k1gInPc7	úřad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
v	v	k7c6	v
městě	město	k1gNnSc6	město
Tabrízu	tabríz	k1gInSc2	tabríz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bahá	Bahé	k1gNnPc4	Bahé
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
===	===	k?	===
</s>
</p>
<p>
<s>
Tímto	tento	k3xDgInSc7	tento
přislíbeným	přislíbený	k2eAgMnSc7d1	přislíbený
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
Mírzá	Mírzá	k1gMnSc1	Mírzá
Husajn	Husajn	k1gMnSc1	Husajn
'	'	kIx"	'
<g/>
Alí	Alí	k1gMnSc1	Alí
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
vlastní	vlastní	k2eAgFnSc2d1	vlastní
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
)	)	kIx)	)
zvaný	zvaný	k2eAgInSc1d1	zvaný
Bahá	Bahé	k1gNnPc4	Bahé
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gInSc1	lláh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
zakladatelem	zakladatel	k1gMnSc7	zakladatel
víry	víra	k1gFnSc2	víra
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í.	í.	k?	í.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
v	v	k7c6	v
iráckém	irácký	k2eAgInSc6d1	irácký
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
příchod	příchod	k1gInSc1	příchod
představuje	představovat	k5eAaImIp3nS	představovat
naplnění	naplnění	k1gNnSc4	naplnění
proroctví	proroctví	k1gNnSc2	proroctví
všech	všecek	k3xTgNnPc2	všecek
předchozích	předchozí	k2eAgNnPc2d1	předchozí
náboženství	náboženství	k1gNnPc2	náboženství
a	a	k8xC	a
že	že	k8xS	že
jeho	on	k3xPp3gNnSc2	on
poslání	poslání	k1gNnSc2	poslání
jako	jako	k9	jako
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
Božího	boží	k2eAgInSc2d1	boží
projevu	projev	k1gInSc2	projev
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidstvo	lidstvo	k1gNnSc1	lidstvo
přivede	přivést	k5eAaPmIp3nS	přivést
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
úroveň	úroveň	k1gFnSc4	úroveň
jeho	jeho	k3xOp3gInSc2	jeho
postupného	postupný	k2eAgInSc2d1	postupný
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
bude	být	k5eAaImBp3nS	být
jednota	jednota	k1gFnSc1	jednota
všech	všecek	k3xTgFnPc2	všecek
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
národů	národ	k1gInPc2	národ
a	a	k8xC	a
etnik	etnikum	k1gNnPc2	etnikum
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
však	však	k9	však
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
všemi	všecek	k3xTgInPc7	všecek
bábisty	bábist	k1gInPc7	bábist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gInSc1	lláh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
své	své	k1gNnSc4	své
učení	učení	k1gNnSc2	učení
pronásledován	pronásledovat	k5eAaImNgInS	pronásledovat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
téměř	téměř	k6eAd1	téměř
čtyřicetiletého	čtyřicetiletý	k2eAgNnSc2d1	čtyřicetileté
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
Svatyně	svatyně	k1gFnSc1	svatyně
v	v	k7c6	v
izraelské	izraelský	k2eAgFnSc6d1	izraelská
usedlosti	usedlost	k1gFnSc6	usedlost
Bahdží	Bahdží	k1gNnSc2	Bahdží
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
pouti	pouť	k1gFnSc2	pouť
všech	všecek	k3xTgFnPc2	všecek
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
istů	ist	k1gInPc2	ist
světa	svět	k1gInSc2	svět
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
obracejí	obracet	k5eAaImIp3nP	obracet
při	při	k7c6	při
vykonávání	vykonávání	k1gNnSc6	vykonávání
svých	svůj	k3xOyFgFnPc2	svůj
denních	denní	k2eAgFnPc2d1	denní
modliteb	modlitba	k1gFnPc2	modlitba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
'	'	kIx"	'
<g/>
Abdu	Abda	k1gFnSc4	Abda
<g/>
'	'	kIx"	'
<g/>
l-Bahá	l-Bahat	k5eAaPmIp3nS	l-Bahat
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhův	lláhův	k2eAgMnSc1d1	lláhův
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Abdu	Abda	k1gMnSc4	Abda
<g/>
'	'	kIx"	'
<g/>
l	l	kA	l
<g/>
–	–	k?	–
<g/>
Bahá	Bahá	k1gFnSc1	Bahá
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
svým	svůj	k3xOyFgMnPc3	svůj
otcem	otec	k1gMnSc7	otec
hlavou	hlava	k1gFnSc7	hlava
společenství	společenství	k1gNnPc2	společenství
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g />
.	.	kIx.	.
</s>
<s>
učení	učení	k1gNnSc1	učení
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
do	do	k7c2	do
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
během	během	k7c2	během
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
'	'	kIx"	'
<g/>
Abdu	Abda	k1gFnSc4	Abda
<g/>
'	'	kIx"	'
<g/>
l-Bahá	l-Bahat	k5eAaPmIp3nS	l-Bahat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
proslul	proslout	k5eAaPmAgMnS	proslout
svou	svůj	k3xOyFgFnSc7	svůj
dobročinností	dobročinnost	k1gFnSc7	dobročinnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
strádajícím	strádající	k2eAgMnSc7d1	strádající
a	a	k8xC	a
potřebným	potřebný	k2eAgMnPc3d1	potřebný
<g/>
,	,	kIx,	,
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
věřící	věřící	k2eAgInSc4d1	věřící
vzor	vzor	k1gInSc4	vzor
ctností	ctnost	k1gFnPc2	ctnost
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í.	í.	k?	í.
</s>
</p>
<p>
<s>
===	===	k?	===
Shoghi	Shoghi	k1gNnSc3	Shoghi
Effendi	Effend	k1gMnPc1	Effend
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
'	'	kIx"	'
<g/>
Abdu	Abdus	k1gInSc6	Abdus
<g/>
'	'	kIx"	'
<g/>
l-Baháově	l-Baháův	k2eAgInSc6d1	l-Baháův
díle	díl	k1gInSc6	díl
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
Shoghi	Shoghi	k1gNnSc4	Shoghi
Effendi	Effend	k1gMnPc1	Effend
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
vnuk	vnuk	k1gMnSc1	vnuk
a	a	k8xC	a
pravnuk	pravnuk	k1gMnSc1	pravnuk
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláha	lláha	k1gMnSc1	lláha
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
jedinečného	jedinečný	k2eAgInSc2d1	jedinečný
systému	systém	k1gInSc2	systém
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
demokraticky	demokraticky	k6eAd1	demokraticky
volených	volený	k2eAgFnPc6d1	volená
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
obnovovaných	obnovovaný	k2eAgFnPc2d1	obnovovaná
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
z	z	k7c2	z
náboženského	náboženský	k2eAgInSc2d1	náboženský
života	život	k1gInSc2	život
minulosti	minulost	k1gFnSc2	minulost
odstranily	odstranit	k5eAaPmAgFnP	odstranit
dvě	dva	k4xCgFnPc4	dva
největší	veliký	k2eAgFnPc4d3	veliký
překážky	překážka	k1gFnPc4	překážka
bránící	bránící	k2eAgFnPc4d1	bránící
zdravému	zdravý	k2eAgInSc3d1	zdravý
duchovnímu	duchovní	k2eAgInSc3d1	duchovní
vývoji	vývoj	k1gInSc3	vývoj
jedince	jedinec	k1gMnSc2	jedinec
i	i	k8xC	i
společenství	společenství	k1gNnSc1	společenství
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svévolnou	svévolný	k2eAgFnSc4d1	svévolná
interpretaci	interpretace	k1gFnSc4	interpretace
Písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
roli	role	k1gFnSc4	role
jakéhokoli	jakýkoli	k3yIgMnSc2	jakýkoli
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
kazatelů	kazatel	k1gMnPc2	kazatel
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgMnPc2d1	náboženský
vykladačů	vykladač	k1gMnPc2	vykladač
práva	právo	k1gNnSc2	právo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláha	lláha	k1gFnSc1	lláha
==	==	k?	==
</s>
</p>
<p>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Mírzá	Mírzá	k1gMnSc2	Mírzá
Hosejn-	Hosejn-	k1gMnSc2	Hosejn-
<g/>
'	'	kIx"	'
<g/>
Alí	Alí	k1gMnSc2	Alí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
v	v	k7c6	v
perském	perský	k2eAgNnSc6d1	perské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Teheránu	Teherán	k1gInSc2	Teherán
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
ministra	ministr	k1gMnSc2	ministr
(	(	kIx(	(
<g/>
vazíra	vazír	k1gMnSc2	vazír
<g/>
)	)	kIx)	)
na	na	k7c6	na
šáhově	šáhův	k2eAgInSc6d1	šáhův
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
byl	být	k5eAaImAgInS	být
přes	přes	k7c4	přes
vysoké	vysoký	k2eAgNnSc4d1	vysoké
postavení	postavení	k1gNnSc4	postavení
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
znám	znát	k5eAaImIp1nS	znát
svou	svůj	k3xOyFgFnSc7	svůj
filantropickou	filantropický	k2eAgFnSc7d1	filantropická
a	a	k8xC	a
charitativní	charitativní	k2eAgFnSc7d1	charitativní
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
duchovními	duchovní	k2eAgFnPc7d1	duchovní
vědomostmi	vědomost	k1gFnPc7	vědomost
a	a	k8xC	a
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
kdy	kdy	k6eAd1	kdy
dostalo	dostat	k5eAaPmAgNnS	dostat
formálního	formální	k2eAgNnSc2d1	formální
teologického	teologický	k2eAgNnSc2d1	teologické
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
v	v	k7c6	v
iráckém	irácký	k2eAgInSc6d1	irácký
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
jako	jako	k8xC	jako
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
hnutí	hnutí	k1gNnSc2	hnutí
Bábí	Bábí	k1gFnSc2	Bábí
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaPmAgMnS	učinit
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
Rezvánu	Rezván	k2eAgFnSc4d1	Rezván
veřejné	veřejný	k2eAgNnSc4d1	veřejné
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oním	onen	k3xDgInSc7	onen
"	"	kIx"	"
<g/>
Přislíbeným	přislíbený	k2eAgMnSc7d1	přislíbený
<g/>
"	"	kIx"	"
minulých	minulý	k2eAgInPc2d1	minulý
věků	věk	k1gInPc2	věk
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
příchod	příchod	k1gInSc4	příchod
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
Báb	Báb	k1gMnSc1	Báb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgNnSc2	svůj
čtyřicetiletého	čtyřicetiletý	k2eAgNnSc2d1	čtyřicetileté
působení	působení	k1gNnSc2	působení
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gMnSc1	lláh
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
,	,	kIx,	,
žaláři	žalář	k1gInSc6	žalář
a	a	k8xC	a
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Bagdádu	Bagdád	k1gInSc2	Bagdád
byl	být	k5eAaImAgInS	být
vypovězen	vypovědět	k5eAaPmNgMnS	vypovědět
za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
světskou	světský	k2eAgFnSc7d1	světská
i	i	k8xC	i
duchovní	duchovní	k2eAgFnSc7d1	duchovní
mocí	moc	k1gFnSc7	moc
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
Edirne	Edirn	k1gInSc5	Edirn
(	(	kIx(	(
<g/>
Drinopol	Drinopol	k1gInSc4	Drinopol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgInS	strávit
celých	celý	k2eAgInPc2d1	celý
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
byl	být	k5eAaImAgInS	být
osmanským	osmanský	k2eAgMnSc7d1	osmanský
sultánem	sultán	k1gMnSc7	sultán
'	'	kIx"	'
<g/>
Abdu	Abdum	k1gNnSc3	Abdum
<g/>
'	'	kIx"	'
<g/>
l-	l-	k?	l-
<g/>
'	'	kIx"	'
<g/>
Azízem	Azíz	k1gMnSc7	Azíz
poslán	poslán	k2eAgInSc4d1	poslán
do	do	k7c2	do
trestanecké	trestanecký	k2eAgFnSc2d1	trestanecká
kolonie	kolonie	k1gFnSc2	kolonie
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
vlivem	vlivem	k7c2	vlivem
podmínek	podmínka	k1gFnPc2	podmínka
žaláře	žalář	k1gInSc2	žalář
původního	původní	k2eAgNnSc2d1	původní
křižáckého	křižácký	k2eAgNnSc2d1	křižácké
pevnostního	pevnostní	k2eAgNnSc2d1	pevnostní
města	město	k1gNnSc2	město
'	'	kIx"	'
<g/>
Akká	Akká	k1gFnSc1	Akká
(	(	kIx(	(
<g/>
Akra	Akra	k1gFnSc1	Akra
<g/>
)	)	kIx)	)
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gMnSc1	lláh
však	však	k9	však
téměř	téměř	k6eAd1	téměř
dvouleté	dvouletý	k2eAgNnSc4d1	dvouleté
žalářování	žalářování	k1gNnSc4	žalářování
přestál	přestát	k5eAaPmAgMnS	přestát
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
dožít	dožít	k5eAaPmF	dožít
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
usedlosti	usedlost	k1gFnSc6	usedlost
Bahdží	Bahdž	k1gFnPc2	Bahdž
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
'	'	kIx"	'
<g/>
Akká	Akká	k1gFnSc1	Akká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gMnSc1	lláh
posílal	posílat	k5eAaImAgMnS	posílat
dopisy	dopis	k1gInPc4	dopis
všem	všecek	k3xTgNnPc3	všecek
hlavám	hlava	k1gFnPc3	hlava
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
nového	nový	k2eAgNnSc2d1	nové
náboženství	náboženství	k1gNnSc2	náboženství
přehodnotili	přehodnotit	k5eAaPmAgMnP	přehodnotit
způsob	způsob	k1gInSc4	způsob
svého	svůj	k3xOyFgNnSc2	svůj
vládnutí	vládnutí	k1gNnSc2	vládnutí
a	a	k8xC	a
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
blaha	blaho	k1gNnSc2	blaho
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
míru	mír	k1gInSc6	mír
<g/>
.	.	kIx.	.
</s>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
přes	přes	k7c4	přes
příkoří	příkoří	k1gNnSc4	příkoří
a	a	k8xC	a
protivenství	protivenství	k1gNnSc4	protivenství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gInSc4	on
doslova	doslova	k6eAd1	doslova
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
stíhaly	stíhat	k5eAaImAgFnP	stíhat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
světské	světský	k2eAgFnSc2d1	světská
i	i	k8xC	i
církevní	církevní	k2eAgFnSc2d1	církevní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
rodinné	rodinný	k2eAgFnPc4d1	rodinná
tragédie	tragédie	k1gFnPc4	tragédie
a	a	k8xC	a
přes	přes	k7c4	přes
útok	útok	k1gInSc4	útok
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
zjevoval	zjevovat	k5eAaImAgMnS	zjevovat
své	svůj	k3xOyFgInPc4	svůj
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
v	v	k7c4	v
Bahá	Bahé	k1gNnPc4	Bahé
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
terminologii	terminologie	k1gFnSc6	terminologie
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
Starý	starý	k2eAgInSc4d1	starý
zákon	zákon	k1gInSc4	zákon
Deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
dny	dna	k1gFnPc1	dna
života	život	k1gInSc2	život
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gMnSc1	lláh
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
na	na	k7c6	na
usedlosti	usedlost	k1gFnSc6	usedlost
Bahdží	Bahdž	k1gFnPc2	Bahdž
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
'	'	kIx"	'
<g/>
Akká	Akká	k1gFnSc1	Akká
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
uloženy	uložit	k5eAaPmNgInP	uložit
jeho	jeho	k3xOp3gInPc1	jeho
tělesné	tělesný	k2eAgInPc1d1	tělesný
ostatky	ostatek	k1gInPc1	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhova	lláhova	k1gFnSc1	lláhova
Svatyně	svatyně	k1gFnSc2	svatyně
v	v	k7c6	v
Bahdží	Bahdží	k1gNnSc6	Bahdží
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xS	jako
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
směr	směr	k1gInSc4	směr
uctívání	uctívání	k1gNnSc2	uctívání
při	při	k7c6	při
denní	denní	k2eAgFnSc6d1	denní
modlitbě	modlitba	k1gFnSc6	modlitba
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věřící	věřící	k1gFnSc3	věřící
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
učení	učení	k1gNnSc2	učení
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Bůh	bůh	k1gMnSc1	bůh
===	===	k?	===
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaPmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
svět	svět	k1gInSc1	svět
byl	být	k5eAaImAgInS	být
stvořen	stvořit	k5eAaPmNgInS	stvořit
vědomou	vědomý	k2eAgFnSc7d1	vědomá
a	a	k8xC	a
myslící	myslící	k2eAgFnSc7d1	myslící
Příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
náboženské	náboženský	k2eAgFnSc6d1	náboženská
tradici	tradice	k1gFnSc6	tradice
zpravidla	zpravidla	k6eAd1	zpravidla
nazývá	nazývat	k5eAaImIp3nS	nazývat
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Božská	božský	k2eAgFnSc1d1	božská
podstata	podstata	k1gFnSc1	podstata
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
mysl	mysl	k1gFnSc4	mysl
zcela	zcela	k6eAd1	zcela
nepoznatelná	poznatelný	k2eNgFnSc1d1	nepoznatelná
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
nepopsatelná	popsatelný	k2eNgFnSc1d1	nepopsatelná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
nazývat	nazývat	k5eAaImF	nazývat
nejrůznějšími	různý	k2eAgNnPc7d3	nejrůznější
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
jménech	jméno	k1gNnPc6	jméno
a	a	k8xC	a
přídomcích	přídomek	k1gInPc6	přídomek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
naprosto	naprosto	k6eAd1	naprosto
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnSc2	jméno
jako	jako	k8xC	jako
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
Hospodin	Hospodin	k1gMnSc1	Hospodin
apod.	apod.	kA	apod.
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
pomocnými	pomocný	k2eAgInPc7d1	pomocný
nástroji	nástroj	k1gInPc7	nástroj
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
odkazujeme	odkazovat	k5eAaImIp1nP	odkazovat
k	k	k7c3	k
oné	onen	k3xDgFnSc3	onen
věčné	věčný	k2eAgFnSc3d1	věčná
Příčině	příčina	k1gFnSc3	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tato	tento	k3xDgFnSc1	tento
Příčina	příčina	k1gFnSc1	příčina
stvořila	stvořit	k5eAaPmAgFnS	stvořit
svět	svět	k1gInSc4	svět
vědomě	vědomě	k6eAd1	vědomě
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
představuje	představovat	k5eAaImIp3nS	představovat
inteligentní	inteligentní	k2eAgFnSc4d1	inteligentní
entitu	entita	k1gFnSc4	entita
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
stvoření	stvoření	k1gNnSc1	stvoření
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
se	s	k7c7	s
specifickým	specifický	k2eAgInSc7d1	specifický
účelem	účel	k1gInSc7	účel
a	a	k8xC	a
záměrem	záměr	k1gInSc7	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
lidstvo	lidstvo	k1gNnSc1	lidstvo
mohlo	moct	k5eAaImAgNnS	moct
tento	tento	k3xDgInSc4	tento
smysl	smysl	k1gInSc4	smysl
stvoření	stvoření	k1gNnSc2	stvoření
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
věků	věk	k1gInPc2	věk
sesíláni	sesílán	k2eAgMnPc1d1	sesílán
božští	božský	k2eAgMnPc1d1	božský
učitelé	učitel	k1gMnPc1	učitel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
terminologie	terminologie	k1gFnSc1	terminologie
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
nazývá	nazývat	k5eAaImIp3nS	nazývat
Božími	boží	k2eAgInPc7d1	boží
projevy	projev	k1gInPc7	projev
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
dokonalým	dokonalý	k2eAgInPc3d1	dokonalý
projevům	projev	k1gInPc3	projev
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
lze	lze	k6eAd1	lze
připodobnit	připodobnit	k5eAaPmF	připodobnit
k	k	k7c3	k
čistému	čistý	k2eAgNnSc3d1	čisté
zrcadlu	zrcadlo	k1gNnSc3	zrcadlo
<g/>
,	,	kIx,	,
věrně	věrně	k6eAd1	věrně
odrážejícímu	odrážející	k2eAgMnSc3d1	odrážející
přesnou	přesný	k2eAgFnSc4d1	přesná
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
záměry	záměr	k1gInPc4	záměr
Stvořitele	Stvořitel	k1gMnSc2	Stvořitel
<g/>
,	,	kIx,	,
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jejich	jejich	k3xOp3gNnSc2	jejich
učení	učení	k1gNnSc2	učení
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
schopni	schopen	k2eAgMnPc1d1	schopen
pochopit	pochopit	k5eAaPmF	pochopit
Boží	boží	k2eAgInSc4d1	boží
plán	plán	k1gInSc4	plán
a	a	k8xC	a
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
jeho	jeho	k3xOp3gFnSc4	jeho
vůli	vůle	k1gFnSc4	vůle
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Náboženskou	náboženský	k2eAgFnSc4d1	náboženská
zvěst	zvěst	k1gFnSc4	zvěst
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tyto	tento	k3xDgInPc1	tento
Boží	boží	k2eAgInPc1d1	boží
projevy	projev	k1gInPc1	projev
pravidelně	pravidelně	k6eAd1	pravidelně
přinášejí	přinášet	k5eAaImIp3nP	přinášet
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
přímé	přímý	k2eAgNnSc4d1	přímé
Boží	boží	k2eAgNnSc4d1	boží
poselství	poselství	k1gNnSc4	poselství
vyjádřené	vyjádřený	k2eAgFnSc2d1	vyjádřená
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
stylem	styl	k1gInSc7	styl
srozumitelným	srozumitelný	k2eAgInSc7d1	srozumitelný
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
fázi	fáze	k1gFnSc6	fáze
svého	svůj	k3xOyFgInSc2	svůj
pozemského	pozemský	k2eAgInSc2d1	pozemský
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boží	boží	k2eAgInPc1d1	boží
projevy	projev	k1gInPc1	projev
===	===	k?	===
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaBmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boží	boží	k2eAgInPc1d1	boží
projevy	projev	k1gInPc1	projev
představují	představovat	k5eAaImIp3nP	představovat
jediné	jediný	k2eAgNnSc4d1	jediné
pojítko	pojítko	k1gNnSc4	pojítko
mezi	mezi	k7c7	mezi
nepoznatelnou	poznatelný	k2eNgFnSc7d1	nepoznatelná
podstatou	podstata	k1gFnSc7	podstata
a	a	k8xC	a
lidstvem	lidstvo	k1gNnSc7	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Představují	představovat	k5eAaImIp3nP	představovat
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
projev	projev	k1gInSc4	projev
či	či	k8xC	či
odraz	odraz	k1gInSc4	odraz
pomyslného	pomyslný	k2eAgNnSc2d1	pomyslné
slunce	slunce	k1gNnSc2	slunce
Boží	boží	k2eAgFnSc2d1	boží
podstaty	podstata	k1gFnSc2	podstata
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
lidstvo	lidstvo	k1gNnSc1	lidstvo
čerpá	čerpat	k5eAaImIp3nS	čerpat
svůj	svůj	k3xOyFgInSc4	svůj
duchovní	duchovní	k2eAgInSc4d1	duchovní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Boží	boží	k2eAgInPc1d1	boží
projevy	projev	k1gInPc1	projev
lidstvu	lidstvo	k1gNnSc6	lidstvo
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
Boží	boží	k2eAgFnSc4d1	boží
vůli	vůle	k1gFnSc4	vůle
pomocí	pomocí	k7c2	pomocí
svého	svůj	k3xOyFgNnSc2	svůj
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zjevují	zjevovat	k5eAaImIp3nP	zjevovat
od	od	k7c2	od
samotného	samotný	k2eAgInSc2d1	samotný
počátku	počátek	k1gInSc2	počátek
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
dle	dle	k7c2	dle
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
učení	učení	k1gNnSc2	učení
bude	být	k5eAaImBp3nS	být
navždy	navždy	k6eAd1	navždy
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
lidstvo	lidstvo	k1gNnSc1	lidstvo
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
době	doba	k1gFnSc6	doba
a	a	k8xC	a
fázi	fáze	k1gFnSc6	fáze
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
potřebovat	potřebovat	k5eAaImF	potřebovat
duchovní	duchovní	k2eAgNnSc4d1	duchovní
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
inspiraci	inspirace	k1gFnSc4	inspirace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
zdrojů	zdroj	k1gInPc2	zdroj
jsou	být	k5eAaImIp3nP	být
nám	my	k3xPp1nPc3	my
známa	znám	k2eAgNnPc1d1	známo
jména	jméno	k1gNnPc1	jméno
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Adamova	Adamův	k2eAgInSc2d1	Adamův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
začíná	začínat	k5eAaImIp3nS	začínat
Adamem	Adam	k1gMnSc7	Adam
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
Abraháma	Abrahám	k1gMnSc4	Abrahám
<g/>
,	,	kIx,	,
Mojžíše	Mojžíš	k1gMnSc4	Mojžíš
<g/>
,	,	kIx,	,
Ježíše	Ježíš	k1gMnSc4	Ježíš
a	a	k8xC	a
Mohameda	Mohamed	k1gMnSc4	Mohamed
až	až	k9	až
po	po	k7c6	po
Bába	bába	k1gFnSc1	bába
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
cyklus	cyklus	k1gInSc1	cyklus
završil	završit	k5eAaPmAgInS	završit
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
ohlášením	ohlášení	k1gNnSc7	ohlášení
příchodu	příchod	k1gInSc2	příchod
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláha	lláha	k1gMnSc1	lláha
zároveň	zároveň	k6eAd1	zároveň
zahájil	zahájit	k5eAaPmAgMnS	zahájit
nový	nový	k2eAgInSc4d1	nový
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
výše	výše	k1gFnSc2	výše
jmenované	jmenovaný	k2eAgFnSc2d1	jmenovaná
"	"	kIx"	"
<g/>
semitské	semitský	k2eAgFnSc2d1	semitská
<g/>
"	"	kIx"	"
rodiny	rodina	k1gFnPc1	rodina
projevů	projev	k1gInPc2	projev
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
projevy	projev	k1gInPc1	projev
"	"	kIx"	"
<g/>
árijské	árijský	k2eAgFnPc1d1	árijská
<g/>
"	"	kIx"	"
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Kršnu	Kršna	k1gMnSc4	Kršna
<g/>
,	,	kIx,	,
Zoroastra	Zoroastra	k1gFnSc1	Zoroastra
(	(	kIx(	(
<g/>
Zarathuštra	Zarathuštra	k1gFnSc1	Zarathuštra
<g/>
)	)	kIx)	)
a	a	k8xC	a
Buddhu	Buddha	k1gMnSc4	Buddha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaImIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
nečiní	činit	k5eNaImIp3nS	činit
jediný	jediný	k2eAgInSc4d1	jediný
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
těchto	tento	k3xDgInPc2	tento
projevů	projev	k1gInPc2	projev
–	–	k?	–
jediné	jediné	k1gNnSc4	jediné
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
se	se	k3xPyFc4	se
Boží	božit	k5eAaImIp3nP	božit
poslové	posel	k1gMnPc1	posel
dle	dle	k7c2	dle
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
učení	učení	k1gNnSc2	učení
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
míra	míra	k1gFnSc1	míra
zjevení	zjevení	k1gNnSc2	zjevení
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
okamžiku	okamžik	k1gInSc6	okamžik
historie	historie	k1gFnSc2	historie
lidstvu	lidstvo	k1gNnSc6	lidstvo
zprostředkovali	zprostředkovat	k5eAaPmAgMnP	zprostředkovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednota	jednota	k1gFnSc1	jednota
lidstva	lidstvo	k1gNnSc2	lidstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaPmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
poslání	poslání	k1gNnPc2	poslání
Božích	boží	k2eAgInPc2d1	boží
projevů	projev	k1gInPc2	projev
bylo	být	k5eAaImAgNnS	být
ustanovení	ustanovení	k1gNnSc1	ustanovení
vždy	vždy	k6eAd1	vždy
vyšší	vysoký	k2eAgFnPc4d2	vyšší
formy	forma	k1gFnPc4	forma
integrace	integrace	k1gFnSc2	integrace
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
kmenových	kmenový	k2eAgInPc2d1	kmenový
svazků	svazek	k1gInPc2	svazek
a	a	k8xC	a
společenství	společenství	k1gNnSc2	společenství
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
lidstvo	lidstvo	k1gNnSc1	lidstvo
přes	přes	k7c4	přes
úroveň	úroveň	k1gFnSc4	úroveň
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
dospělo	dochvít	k5eAaPmAgNnS	dochvít
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
stupni	stupeň	k1gInPc7	stupeň
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
principy	princip	k1gInPc7	princip
zastřešil	zastřešit	k5eAaPmAgInS	zastřešit
Bahá	Bahé	k1gNnPc4	Bahé
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
etapa	etapa	k1gFnSc1	etapa
vývoje	vývoj	k1gInSc2	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
spojení	spojení	k1gNnSc1	spojení
národů	národ	k1gInPc2	národ
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
společných	společný	k2eAgFnPc2d1	společná
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
institucí	instituce	k1gFnPc2	instituce
(	(	kIx(	(
<g/>
legislativních	legislativní	k2eAgFnPc2d1	legislativní
<g/>
,	,	kIx,	,
soudních	soudní	k2eAgFnPc2d1	soudní
i	i	k8xC	i
exekutivních	exekutivní	k2eAgFnPc2d1	exekutivní
<g/>
)	)	kIx)	)
při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
základního	základní	k2eAgInSc2d1	základní
principu	princip	k1gInSc2	princip
jednoty	jednota	k1gFnSc2	jednota
v	v	k7c6	v
různosti	různost	k1gFnSc6	různost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zachování	zachování	k1gNnSc4	zachování
svébytnosti	svébytnost	k1gFnSc2	svébytnost
kultur	kultura	k1gFnPc2	kultura
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
národů	národ	k1gInPc2	národ
a	a	k8xC	a
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Principy	princip	k1gInPc1	princip
učení	učení	k1gNnSc2	učení
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
===	===	k?	===
</s>
</p>
<p>
<s>
Jednota	jednota	k1gFnSc1	jednota
Boha	bůh	k1gMnSc2	bůh
</s>
</p>
<p>
<s>
Jednota	jednota	k1gFnSc1	jednota
náboženství	náboženství	k1gNnSc2	náboženství
</s>
</p>
<p>
<s>
Jednota	jednota	k1gFnSc1	jednota
lidstva	lidstvo	k1gNnSc2	lidstvo
</s>
</p>
<p>
<s>
Odstranění	odstranění	k1gNnSc1	odstranění
všech	všecek	k3xTgFnPc2	všecek
forem	forma	k1gFnPc2	forma
předsudků	předsudek	k1gInPc2	předsudek
</s>
</p>
<p>
<s>
Odstranění	odstranění	k1gNnSc1	odstranění
extrémů	extrém	k1gInPc2	extrém
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
chudoby	chudoba	k1gFnSc2	chudoba
</s>
</p>
<p>
<s>
Rovnost	rovnost	k1gFnSc1	rovnost
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
Harmonie	harmonie	k1gFnSc1	harmonie
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
</s>
</p>
<p>
<s>
Nezávislé	závislý	k2eNgNnSc1d1	nezávislé
hledání	hledání	k1gNnSc1	hledání
pravdy	pravda	k1gFnSc2	pravda
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
mír	mír	k1gInSc1	mír
</s>
</p>
<p>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
světového	světový	k2eAgInSc2d1	světový
pomocného	pomocný	k2eAgInSc2d1	pomocný
jazyka	jazyk	k1gInSc2	jazyk
</s>
</p>
<p>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
povinného	povinný	k2eAgNnSc2d1	povinné
vzdělání	vzdělání	k1gNnSc2	vzdělání
</s>
</p>
<p>
<s>
==	==	k?	==
Svaté	svatý	k2eAgNnSc1d1	svaté
Písmo	písmo	k1gNnSc1	písmo
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
==	==	k?	==
</s>
</p>
<p>
<s>
Citát	citát	k1gInSc1	citát
z	z	k7c2	z
Písma	písmo	k1gNnSc2	písmo
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláha	lláha	k1gFnSc1	lláha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Baháí	Baháí	k6eAd1	Baháí
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
tzv.	tzv.	kA	tzv.
postupné	postupný	k2eAgNnSc1d1	postupné
zjevování	zjevování	k1gNnSc1	zjevování
Písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
Písma	písmo	k1gNnSc2	písmo
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
jsou	být	k5eAaImIp3nP	být
spisy	spis	k1gInPc4	spis
zjevené	zjevený	k2eAgFnSc2d1	zjevená
Bábem	Báb	k1gInSc7	Báb
a	a	k8xC	a
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhem	lláh	k1gInSc7	lláh
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
všemi	všecek	k3xTgMnPc7	všecek
věřícími	věřící	k2eAgMnPc7d1	věřící
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
Boží	boží	k2eAgNnSc4d1	boží
slovo	slovo	k1gNnSc4	slovo
zjevené	zjevený	k2eAgFnSc2d1	zjevená
lidstvu	lidstvo	k1gNnSc3	lidstvo
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
samotných	samotný	k2eAgFnPc2d1	samotná
Baháí	Bahá	k1gFnPc2	Bahá
spisů	spis	k1gInPc2	spis
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
sedmnáct	sedmnáct	k4xCc1	sedmnáct
tzv.	tzv.	kA	tzv.
Písmen	písmeno	k1gNnPc2	písmeno
živoucího	živoucí	k2eAgInSc2d1	živoucí
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
přislíbených	přislíbený	k2eAgMnPc2d1	přislíbený
proroků	prorok	k1gMnPc2	prorok
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přijmou	přijmout	k5eAaPmIp3nP	přijmout
zjevení	zjevený	k2eAgMnPc1d1	zjevený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Báb	Báb	k?	Báb
zjevil	zjevit	k5eAaPmAgMnS	zjevit
svatou	svatý	k2eAgFnSc4d1	svatá
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Baján	Bajána	k1gFnPc2	Bajána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřicetiletou	čtyřicetiletý	k2eAgFnSc4d1	čtyřicetiletá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
zjevil	zjevit	k5eAaPmAgMnS	zjevit
kolem	kolem	k7c2	kolem
stovky	stovka	k1gFnSc2	stovka
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
hraje	hrát	k5eAaImIp3nS	hrát
ústřední	ústřední	k2eAgFnSc4d1	ústřední
roli	role	k1gFnSc4	role
kniha	kniha	k1gFnSc1	kniha
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Kitáb-i-Aqdas	Kitáb-Aqdas	k1gInSc1	Kitáb-i-Aqdas
(	(	kIx(	(
<g/>
Nejsvětější	nejsvětější	k2eAgFnSc1d1	nejsvětější
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
formulovány	formulován	k2eAgInPc4d1	formulován
základy	základ	k1gInPc4	základ
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í.	í.	k?	í.
Druhou	druhý	k4xOgFnSc7	druhý
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
knihou	kniha	k1gFnSc7	kniha
víry	víra	k1gFnSc2	víra
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
je	on	k3xPp3gMnPc4	on
Kitáb-i-Íqán	Kitáb-Íqán	k1gInSc4	Kitáb-i-Íqán
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gInSc1	lláh
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
význam	význam	k1gInSc4	význam
a	a	k8xC	a
roli	role	k1gFnSc4	role
Božích	boží	k2eAgInPc2d1	boží
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
seslání	seslání	k1gNnSc4	seslání
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neodmyslitelnou	odmyslitelný	k2eNgFnSc4d1	neodmyslitelná
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
také	také	k9	také
spisy	spis	k1gInPc1	spis
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhova	lláhův	k2eAgMnSc4d1	lláhův
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
'	'	kIx"	'
<g/>
Adbu	Adba	k1gMnSc4	Adba
<g/>
'	'	kIx"	'
<g/>
l-Baháa	l-Baháus	k1gMnSc4	l-Baháus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mají	mít	k5eAaImIp3nP	mít
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
roli	role	k1gFnSc4	role
interpretační	interpretační	k2eAgFnSc4d1	interpretační
a	a	k8xC	a
didaktickou	didaktický	k2eAgFnSc4d1	didaktická
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
součástí	součást	k1gFnSc7	součást
Písma	písmo	k1gNnSc2	písmo
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
zjeveného	zjevený	k2eAgNnSc2d1	zjevené
Božího	boží	k2eAgNnSc2d1	boží
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písmo	písmo	k1gNnSc1	písmo
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
bylo	být	k5eAaImAgNnS	být
zjeveno	zjevit	k5eAaPmNgNnS	zjevit
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
a	a	k8xC	a
perském	perský	k2eAgInSc6d1	perský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
do	do	k7c2	do
všech	všecek	k3xTgMnPc2	všecek
hlavních	hlavní	k2eAgMnPc2d1	hlavní
světových	světový	k2eAgMnPc2d1	světový
jazyků	jazyk	k1gMnPc2	jazyk
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
národních	národní	k2eAgInPc2d1	národní
a	a	k8xC	a
etnických	etnický	k2eAgInPc2d1	etnický
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhovo	lláhův	k2eAgNnSc4d1	lláhův
specifické	specifický	k2eAgNnSc4d1	specifické
ustanovení	ustanovení	k1gNnSc4	ustanovení
v	v	k7c4	v
Kitáb-i-Aqdas	Kitáb-Aqdas	k1gInSc4	Kitáb-i-Aqdas
zapovídající	zapovídající	k2eAgFnSc2d1	zapovídající
komukoli	kdokoli	k3yInSc3	kdokoli
interpretovat	interpretovat	k5eAaBmF	interpretovat
explicitní	explicitní	k2eAgInSc4d1	explicitní
význam	význam	k1gInSc4	význam
jeho	on	k3xPp3gNnSc2	on
Písma	písmo	k1gNnSc2	písmo
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
společenství	společenství	k1gNnSc4	společenství
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
letech	let	k1gInPc6	let
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
jednotné	jednotný	k2eAgFnSc2d1	jednotná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Denní	denní	k2eAgInSc1d1	denní
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaPmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
dvojí	dvojí	k4xRgFnSc1	dvojí
přirozenost	přirozenost	k1gFnSc1	přirozenost
–	–	k?	–
hmotnou	hmotný	k2eAgFnSc7d1	hmotná
a	a	k8xC	a
duchovní	duchovní	k2eAgFnSc7d1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
duchovní	duchovní	k2eAgFnSc1d1	duchovní
stránka	stránka	k1gFnSc1	stránka
představuje	představovat	k5eAaImIp3nS	představovat
jeho	jeho	k3xOp3gFnSc4	jeho
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
podstatu	podstata	k1gFnSc4	podstata
<g/>
,	,	kIx,	,
hmotná	hmotný	k2eAgFnSc1d1	hmotná
přirozenost	přirozenost	k1gFnSc1	přirozenost
jeho	jeho	k3xOp3gNnSc2	jeho
bytí	bytí	k1gNnSc2	bytí
představuje	představovat	k5eAaImIp3nS	představovat
nástroj	nástroj	k1gInSc1	nástroj
k	k	k7c3	k
dosahování	dosahování	k1gNnSc3	dosahování
duchovního	duchovní	k2eAgInSc2d1	duchovní
pokroku	pokrok	k1gInSc2	pokrok
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
materiálním	materiální	k2eAgInSc6d1	materiální
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
harmonii	harmonie	k1gFnSc4	harmonie
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
stránkami	stránka	k1gFnPc7	stránka
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
leze	lézt	k5eAaImIp3nS	lézt
docílit	docílit	k5eAaPmF	docílit
zaváděním	zavádění	k1gNnSc7	zavádění
Božího	boží	k2eAgNnSc2d1	boží
učení	učení	k1gNnSc2	učení
do	do	k7c2	do
našeho	náš	k3xOp1gInSc2	náš
denního	denní	k2eAgInSc2d1	denní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
naše	náš	k3xOp1gNnSc1	náš
tělo	tělo	k1gNnSc1	tělo
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
správnou	správný	k2eAgFnSc4d1	správná
a	a	k8xC	a
zdravou	zdravý	k2eAgFnSc4d1	zdravá
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
i	i	k8xC	i
naše	náš	k3xOp1gFnSc1	náš
duchovní	duchovní	k2eAgFnSc1d1	duchovní
přirozenost	přirozenost	k1gFnSc1	přirozenost
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pravidelně	pravidelně	k6eAd1	pravidelně
vyživována	vyživovat	k5eAaImNgFnS	vyživovat
duchovní	duchovní	k2eAgFnSc7d1	duchovní
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
různé	různý	k2eAgInPc4d1	různý
duchovní	duchovní	k2eAgInPc4d1	duchovní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
ve	v	k7c6	v
Svém	svůj	k3xOyFgNnSc6	svůj
Písmu	písmo	k1gNnSc6	písmo
stanovil	stanovit	k5eAaPmAgMnS	stanovit
a	a	k8xC	a
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
modlitba	modlitba	k1gFnSc1	modlitba
a	a	k8xC	a
půst	půst	k1gInSc1	půst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Modlitba	modlitba	k1gFnSc1	modlitba
===	===	k?	===
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaImIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
modliteb	modlitba	k1gFnPc2	modlitba
–	–	k?	–
běžnou	běžný	k2eAgFnSc4d1	běžná
a	a	k8xC	a
denní	denní	k2eAgFnSc4d1	denní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžné	běžný	k2eAgFnPc4d1	běžná
modlitby	modlitba	k1gFnPc4	modlitba
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
pronášejí	pronášet	k5eAaImIp3nP	pronášet
při	při	k7c6	při
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
individuálně	individuálně	k6eAd1	individuálně
i	i	k9	i
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
spoluvěřících	spoluvěřící	k2eAgInPc2d1	spoluvěřící
<g/>
,	,	kIx,	,
za	za	k7c7	za
různým	různý	k2eAgInSc7d1	různý
účelem	účel	k1gInSc7	účel
(	(	kIx(	(
<g/>
duchovní	duchovní	k2eAgFnSc1d1	duchovní
posila	posila	k1gFnSc1	posila
<g/>
,	,	kIx,	,
odpuštění	odpuštění	k1gNnSc1	odpuštění
<g/>
,	,	kIx,	,
Boží	boží	k2eAgFnSc1d1	boží
chvála	chvála	k1gFnSc1	chvála
<g/>
,	,	kIx,	,
uzdravení	uzdravení	k1gNnSc1	uzdravení
z	z	k7c2	z
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
protivenství	protivenství	k1gNnSc6	protivenství
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Denní	denní	k2eAgFnSc1d1	denní
modlitba	modlitba	k1gFnSc1	modlitba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
trojího	trojí	k4xRgMnSc2	trojí
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
modlitby	modlitba	k1gFnSc2	modlitba
svůj	svůj	k3xOyFgInSc4	svůj
pevný	pevný	k2eAgInSc4d1	pevný
řád	řád	k1gInSc4	řád
–	–	k?	–
specifický	specifický	k2eAgInSc1d1	specifický
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
frekvenci	frekvence	k1gFnSc4	frekvence
i	i	k8xC	i
přesný	přesný	k2eAgInSc4d1	přesný
čas	čas	k1gInSc4	čas
provádění	provádění	k1gNnSc2	provádění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
analogii	analogie	k1gFnSc4	analogie
každodenní	každodenní	k2eAgFnSc2d1	každodenní
audience	audience	k1gFnSc2	audience
u	u	k7c2	u
Stvořitele	Stvořitel	k1gMnSc2	Stvořitel
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
věřící	věřící	k1gMnSc1	věřící
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
Boží	boží	k2eAgFnSc6d1	boží
přítomnosti	přítomnost	k1gFnSc6	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
dle	dle	k7c2	dle
určitých	určitý	k2eAgNnPc2d1	určité
stanovených	stanovený	k2eAgNnPc2d1	stanovené
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
například	například	k6eAd1	například
chovali	chovat	k5eAaImAgMnP	chovat
na	na	k7c6	na
pomyslné	pomyslný	k2eAgFnSc6d1	pomyslná
audienci	audience	k1gFnSc6	audience
u	u	k7c2	u
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
denní	denní	k2eAgFnSc1d1	denní
modlitba	modlitba	k1gFnSc1	modlitba
se	se	k3xPyFc4	se
pronáší	pronášet	k5eAaImIp3nS	pronášet
jednou	jeden	k4xCgFnSc7	jeden
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
od	od	k7c2	od
poledne	poledne	k1gNnSc2	poledne
do	do	k7c2	do
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
západu	západ	k1gInSc6	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
modlitba	modlitba	k1gFnSc1	modlitba
se	se	k3xPyFc4	se
pronáší	pronášet	k5eAaImIp3nS	pronášet
třikrát	třikrát	k6eAd1	třikrát
denně	denně	k6eAd1	denně
–	–	k?	–
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
a	a	k8xC	a
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
denní	denní	k2eAgFnSc1d1	denní
modlitba	modlitba	k1gFnSc1	modlitba
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
od	od	k7c2	od
poledne	poledne	k1gNnSc2	poledne
jednoho	jeden	k4xCgMnSc2	jeden
dne	den	k1gInSc2	den
do	do	k7c2	do
poledne	poledne	k1gNnSc2	poledne
dne	den	k1gInSc2	den
následujícího	následující	k2eAgInSc2d1	následující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věřící	věřící	k1gMnPc1	věřící
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
modliteb	modlitba	k1gFnPc2	modlitba
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
uvážení	uvážení	k1gNnSc2	uvážení
<g/>
,	,	kIx,	,
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
vykonáním	vykonání	k1gNnSc7	vykonání
modlitby	modlitba	k1gFnSc2	modlitba
si	se	k3xPyFc3	se
věřící	věřící	k1gMnSc1	věřící
omyje	omýt	k5eAaPmIp3nS	omýt
tvář	tvář	k1gFnSc4	tvář
a	a	k8xC	a
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
modlitební	modlitební	k2eAgInSc4d1	modlitební
postoj	postoj	k1gInSc4	postoj
tváří	tvář	k1gFnPc2	tvář
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
směr	směr	k1gInSc4	směr
uctívání	uctívání	k1gNnSc2	uctívání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
nachází	nacházet	k5eAaImIp3nS	nacházet
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhova	lláhova	k1gFnSc1	lláhova
svatyně	svatyně	k1gFnSc1	svatyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Půst	půst	k1gInSc4	půst
===	===	k?	===
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaPmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
se	se	k3xPyFc4	se
postí	postit	k5eAaImIp3nS	postit
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
během	během	k7c2	během
postního	postní	k2eAgInSc2d1	postní
měsíce	měsíc	k1gInSc2	měsíc
'	'	kIx"	'
<g/>
Alá	Alá	k1gFnSc1	Alá
<g/>
'	'	kIx"	'
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
kalendáře	kalendář	k1gInPc1	kalendář
(	(	kIx(	(
<g/>
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bahá	Bahat	k5eAaPmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
se	se	k3xPyFc4	se
po	po	k7c4	po
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
jídla	jídlo	k1gNnPc1	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc1	pití
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
východu	východ	k1gInSc2	východ
do	do	k7c2	do
Slunce	slunce	k1gNnSc2	slunce
západu	západ	k1gInSc2	západ
každého	každý	k3xTgInSc2	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
19	[number]	k4	19
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
a	a	k8xC	a
záměrem	záměr	k1gInSc7	záměr
půstu	půst	k1gInSc2	půst
je	být	k5eAaImIp3nS	být
fyzická	fyzický	k2eAgFnSc1d1	fyzická
očista	očista	k1gFnSc1	očista
naší	náš	k3xOp1gFnSc2	náš
hmotné	hmotný	k2eAgFnSc2d1	hmotná
schránky	schránka	k1gFnSc2	schránka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
podstatnější	podstatný	k2eAgFnSc4d2	podstatnější
očistu	očista	k1gFnSc4	očista
duchovní	duchovní	k2eAgFnSc4d1	duchovní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
také	také	k9	také
představuje	představovat	k5eAaImIp3nS	představovat
praktickou	praktický	k2eAgFnSc4d1	praktická
zkoušku	zkouška	k1gFnSc4	zkouška
ochoty	ochota	k1gFnSc2	ochota
věřících	věřící	k1gMnPc2	věřící
obětovat	obětovat	k5eAaBmF	obětovat
Bohu	bůh	k1gMnSc3	bůh
něco	něco	k3yInSc1	něco
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
světských	světský	k2eAgFnPc2d1	světská
příchylností	příchylnost	k1gFnPc2	příchylnost
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jen	jen	k9	jen
během	během	k7c2	během
velmi	velmi	k6eAd1	velmi
krátkého	krátký	k2eAgNnSc2d1	krátké
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepožívání	nepožívání	k1gNnSc4	nepožívání
omamných	omamný	k2eAgFnPc2d1	omamná
látek	látka	k1gFnPc2	látka
===	===	k?	===
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaBmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
požívání	požívání	k1gNnSc3	požívání
alkoholu	alkohol	k1gInSc2	alkohol
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
omamných	omamný	k2eAgFnPc2d1	omamná
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odborný	odborný	k2eAgMnSc1d1	odborný
lékař	lékař	k1gMnSc1	lékař
výslovně	výslovně	k6eAd1	výslovně
předepíše	předepsat	k5eAaPmIp3nS	předepsat
danou	daný	k2eAgFnSc4d1	daná
látku	látka	k1gFnSc4	látka
jako	jako	k8xC	jako
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
lék	lék	k1gInSc1	lék
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc1	jeho
součást	součást	k1gFnSc1	součást
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
daného	daný	k2eAgNnSc2d1	dané
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kalendář	kalendář	k1gInSc1	kalendář
===	===	k?	===
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaImIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgNnSc4d1	předchozí
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
kalendář	kalendář	k1gInSc4	kalendář
sestávající	sestávající	k2eAgInSc4d1	sestávající
z	z	k7c2	z
19	[number]	k4	19
měsíců	měsíc	k1gInPc2	měsíc
o	o	k7c6	o
19	[number]	k4	19
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgInP	nazývat
Ajjám-i-Há	Ajjám-Hé	k1gNnPc1	Ajjám-i-Hé
(	(	kIx(	(
<g/>
Dny	den	k1gInPc1	den
Boží	boží	k2eAgFnSc2d1	boží
podstaty	podstata	k1gFnSc2	podstata
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
zasvěceny	zasvěcen	k2eAgFnPc1d1	zasvěcena
oslavám	oslava	k1gFnPc3	oslava
a	a	k8xC	a
charitativní	charitativní	k2eAgFnSc3d1	charitativní
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
kalendáře	kalendář	k1gInPc1	kalendář
jsou	být	k5eAaImIp3nP	být
nazvány	nazvat	k5eAaBmNgInP	nazvat
dle	dle	k7c2	dle
různých	různý	k2eAgNnPc2d1	různé
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
přídomků	přídomek	k1gInPc2	přídomek
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
21	[number]	k4	21
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
jarní	jarní	k2eAgInSc1d1	jarní
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
světí	světit	k5eAaImIp3nS	světit
9	[number]	k4	9
Svátků	svátek	k1gInPc2	svátek
svázaných	svázaný	k2eAgInPc2d1	svázaný
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
událostmi	událost	k1gFnPc7	událost
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
každého	každý	k3xTgInSc2	každý
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Slavnostech	slavnost	k1gFnPc6	slavnost
19	[number]	k4	19
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
společně	společně	k6eAd1	společně
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
čtením	čtení	k1gNnSc7	čtení
modliteb	modlitba	k1gFnPc2	modlitba
a	a	k8xC	a
úryvků	úryvek	k1gInPc2	úryvek
z	z	k7c2	z
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
Písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
probírají	probírat	k5eAaImIp3nP	probírat
formou	forma	k1gFnSc7	forma
konzultace	konzultace	k1gFnSc2	konzultace
všechny	všechen	k3xTgFnPc4	všechen
praktické	praktický	k2eAgFnPc4d1	praktická
záležitosti	záležitost	k1gFnPc4	záležitost
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
chodem	chod	k1gInSc7	chod
místního	místní	k2eAgNnSc2d1	místní
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Praktický	praktický	k2eAgInSc1d1	praktický
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
snaží	snažit	k5eAaImIp3nP	snažit
důsledně	důsledně	k6eAd1	důsledně
žít	žít	k5eAaImF	žít
podle	podle	k7c2	podle
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhem	lláh	k1gInSc7	lláh
zjevených	zjevený	k2eAgFnPc2d1	zjevená
zásad	zásada	k1gFnPc2	zásada
a	a	k8xC	a
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
chtějí	chtít	k5eAaImIp3nP	chtít
být	být	k5eAaImF	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gInSc1	lláh
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
veškeré	veškerý	k3xTgFnPc4	veškerý
formy	forma	k1gFnPc4	forma
fanatického	fanatický	k2eAgMnSc2d1	fanatický
a	a	k8xC	a
extrémního	extrémní	k2eAgNnSc2d1	extrémní
praktikování	praktikování	k1gNnSc2	praktikování
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgInPc4	jenž
považuje	považovat	k5eAaImIp3nS	považovat
např.	např.	kA	např.
asketismus	asketismus	k1gInSc4	asketismus
<g/>
,	,	kIx,	,
mnišské	mnišský	k2eAgInPc4d1	mnišský
řády	řád	k1gInPc4	řád
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
formy	forma	k1gFnPc4	forma
vydělování	vydělování	k1gNnSc2	vydělování
se	se	k3xPyFc4	se
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bahá	Bahat	k5eAaPmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
cítí	cítit	k5eAaImIp3nS	cítit
povinnost	povinnost	k1gFnSc4	povinnost
přispívat	přispívat	k5eAaImF	přispívat
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
ke	k	k7c3	k
společenskému	společenský	k2eAgInSc3d1	společenský
životu	život	k1gInSc3	život
své	své	k1gNnSc1	své
země	zem	k1gFnSc2	zem
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
službu	služba	k1gFnSc4	služba
spatřují	spatřovat	k5eAaImIp3nP	spatřovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
příležitosti	příležitost	k1gFnSc6	příležitost
obohatit	obohatit	k5eAaPmF	obohatit
myšlenkovou	myšlenkový	k2eAgFnSc4d1	myšlenková
kulturu	kultura	k1gFnSc4	kultura
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
o	o	k7c6	o
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
praktických	praktický	k2eAgInPc6d1	praktický
projektech	projekt	k1gInPc6	projekt
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
třetího	třetí	k4xOgMnSc2	třetí
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dětí	dítě	k1gFnPc2	dítě
i	i	k9	i
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
aktivit	aktivita	k1gFnPc2	aktivita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
společenství	společenství	k1gNnSc2	společenství
se	se	k3xPyFc4	se
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
zasazují	zasazovat	k5eAaImIp3nP	zasazovat
o	o	k7c6	o
vnímání	vnímání	k1gNnSc6	vnímání
světa	svět	k1gInSc2	svět
jako	jako	k8xS	jako
společného	společný	k2eAgInSc2d1	společný
domova	domov	k1gInSc2	domov
všech	všecek	k3xTgFnPc2	všecek
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
národů	národ	k1gInPc2	národ
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláhova	lláhův	k2eAgInSc2d1	lláhův
známého	známý	k2eAgInSc2d1	známý
citátu	citát	k1gInSc2	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
domovem	domov	k1gInSc7	domov
a	a	k8xC	a
lidstvo	lidstvo	k1gNnSc1	lidstvo
jeho	jeho	k3xOp3gFnSc2	jeho
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
společenství	společenství	k1gNnSc1	společenství
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
a	a	k8xC	a
OSN	OSN	kA	OSN
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
společenství	společenství	k1gNnSc1	společenství
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
je	být	k5eAaImIp3nS	být
nevládní	vládní	k2eNgFnSc1d1	nevládní
organizace	organizace	k1gFnSc1	organizace
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
u	u	k7c2	u
OSN	OSN	kA	OSN
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
stoupenců	stoupenec	k1gMnPc2	stoupenec
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
víry	víra	k1gFnSc2	víra
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
se	se	k3xPyFc4	se
kolektivně	kolektivně	k6eAd1	kolektivně
účastní	účastnit	k5eAaImIp3nS	účastnit
širokého	široký	k2eAgNnSc2d1	široké
spektra	spektrum	k1gNnSc2	spektrum
aktivit	aktivita	k1gFnPc2	aktivita
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
společenství	společenství	k1gNnSc2	společenství
–	–	k?	–
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mírového	mírový	k2eAgNnSc2d1	Mírové
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
,	,	kIx,	,
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
postavení	postavení	k1gNnSc1	postavení
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
potravinových	potravinový	k2eAgInPc2d1	potravinový
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Organizaci	organizace	k1gFnSc4	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
je	být	k5eAaImIp3nS	být
mezinárodnímu	mezinárodní	k2eAgNnSc3d1	mezinárodní
společenství	společenství	k1gNnSc3	společenství
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
přiznán	přiznán	k2eAgInSc4d1	přiznán
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
konzultativní	konzultativní	k2eAgInSc4d1	konzultativní
statut	statut	k1gInSc4	statut
u	u	k7c2	u
Ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc3d1	sociální
radě	rada	k1gFnSc3	rada
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Ecomomic	Ecomomic	k1gMnSc1	Ecomomic
and	and	k?	and
Social	Social	k1gMnSc1	Social
Council	Council	k1gMnSc1	Council
ECOSOC	ECOSOC	kA	ECOSOC
<g/>
)	)	kIx)	)
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
u	u	k7c2	u
dětského	dětský	k2eAgInSc2d1	dětský
fondu	fond	k1gInSc2	fond
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UNICEF	UNICEF	kA	UNICEF
<g/>
)	)	kIx)	)
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Bahá	Bahat	k5eAaPmIp3nS	Bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
společenství	společenství	k1gNnSc3	společenství
též	též	k9	též
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
Světovou	světový	k2eAgFnSc7d1	světová
zdravotnickou	zdravotnický	k2eAgFnSc7d1	zdravotnická
organizací	organizace	k1gFnSc7	organizace
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Health	Health	k1gInSc1	Health
Organization	Organization	k1gInSc4	Organization
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Úřadem	úřad	k1gInSc7	úřad
vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
programů	program	k1gInPc2	program
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Environment	Environment	k1gMnSc1	Environment
Programme	Programme	k1gFnSc2	Programme
–	–	k?	–
UNEP	UNEP	kA	UNEP
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Development	Development	k1gMnSc1	Development
Programme	Programme	k1gFnSc2	Programme
–	–	k?	–
UNDP	UNDP	kA	UNDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
==	==	k?	==
</s>
</p>
<p>
<s>
Správa	správa	k1gFnSc1	správa
společenství	společenství	k1gNnSc2	společenství
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc3d1	národní
a	a	k8xC	a
celosvětové	celosvětový	k2eAgFnSc3d1	celosvětová
úrovni	úroveň	k1gFnSc3	úroveň
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
demokratických	demokratický	k2eAgFnPc2d1	demokratická
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
volených	volený	k2eAgFnPc2d1	volená
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
základní	základní	k2eAgInPc4d1	základní
principy	princip	k1gInPc4	princip
fungování	fungování	k1gNnSc2	fungování
stanovil	stanovit	k5eAaPmAgMnS	stanovit
Bahá	Bahé	k1gNnPc4	Bahé
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláha	k1gFnPc2	lláha
<g/>
,	,	kIx,	,
podrobněji	podrobně	k6eAd2	podrobně
rozpracoval	rozpracovat	k5eAaPmAgInS	rozpracovat
'	'	kIx"	'
<g/>
Abdu	Abda	k1gFnSc4	Abda
<g/>
'	'	kIx"	'
<g/>
l-Bahá	l-Bahý	k2eAgNnPc4d1	l-Bahý
a	a	k8xC	a
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
a	a	k8xC	a
dovedl	dovést	k5eAaPmAgInS	dovést
Shoghi	Shogh	k1gFnSc3	Shogh
Effendi	Effend	k1gMnPc1	Effend
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
a	a	k8xC	a
národní	národní	k2eAgFnSc6d1	národní
úrovni	úroveň	k1gFnSc6	úroveň
jsou	být	k5eAaImIp3nP	být
každoročně	každoročně	k6eAd1	každoročně
voleni	volit	k5eAaImNgMnP	volit
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
všech	všecek	k3xTgMnPc2	všecek
věřících	věřící	k1gMnPc2	věřící
zástupci	zástupce	k1gMnPc1	zástupce
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
duchovních	duchovní	k2eAgFnPc2d1	duchovní
rad	rada	k1gFnPc2	rada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spravují	spravovat	k5eAaImIp3nP	spravovat
administrativní	administrativní	k2eAgFnPc1d1	administrativní
záležitosti	záležitost	k1gFnPc1	záležitost
daného	daný	k2eAgNnSc2d1	dané
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
probíhá	probíhat	k5eAaImIp3nS	probíhat
volba	volba	k1gFnSc1	volba
každých	každý	k3xTgNnPc2	každý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
–	–	k?	–
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věřících	věřící	k1gMnPc2	věřící
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
Světový	světový	k2eAgInSc1d1	světový
dům	dům	k1gInSc1	dům
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Haifě	Haifa	k1gFnSc6	Haifa
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gInSc1	lláh
zavedením	zavedení	k1gNnSc7	zavedení
tohoto	tento	k3xDgInSc2	tento
jedinečného	jedinečný	k2eAgInSc2d1	jedinečný
systému	systém	k1gInSc2	systém
náboženské	náboženský	k2eAgFnSc2d1	náboženská
správy	správa	k1gFnSc2	správa
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
zrušil	zrušit	k5eAaPmAgInS	zrušit
instituci	instituce	k1gFnSc3	instituce
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
víře	víra	k1gFnSc6	víra
byl	být	k5eAaImAgInS	být
případný	případný	k2eAgInSc1d1	případný
vliv	vliv	k1gInSc1	vliv
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
na	na	k7c4	na
chod	chod	k1gInSc4	chod
společenství	společenství	k1gNnSc2	společenství
prakticky	prakticky	k6eAd1	prakticky
usměrněn	usměrnit	k5eAaPmNgMnS	usměrnit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
demokratické	demokratický	k2eAgFnSc2d1	demokratická
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
svobodného	svobodný	k2eAgNnSc2d1	svobodné
rozhodování	rozhodování	k1gNnSc2	rozhodování
o	o	k7c6	o
záležitostech	záležitost	k1gFnPc6	záležitost
společenství	společenství	k1gNnSc2	společenství
formou	forma	k1gFnSc7	forma
konzultace	konzultace	k1gFnSc2	konzultace
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
znemožnilo	znemožnit	k5eAaPmAgNnS	znemožnit
komukoli	kdokoli	k3yInSc3	kdokoli
požívat	požívat	k5eAaImF	požívat
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
individuální	individuální	k2eAgFnPc4d1	individuální
autority	autorita	k1gFnPc4	autorita
jak	jak	k8xC	jak
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
např.	např.	kA	např.
při	při	k7c6	při
výkladu	výklad	k1gInSc6	výklad
Písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Instituce	instituce	k1gFnSc1	instituce
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
a	a	k8xC	a
dbají	dbát	k5eAaImIp3nP	dbát
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
praktické	praktický	k2eAgFnPc4d1	praktická
a	a	k8xC	a
administrativní	administrativní	k2eAgFnPc4d1	administrativní
záležitosti	záležitost	k1gFnPc4	záležitost
místních	místní	k2eAgNnPc2d1	místní
a	a	k8xC	a
národních	národní	k2eAgNnPc2d1	národní
společenství	společenství	k1gNnPc2	společenství
(	(	kIx(	(
<g/>
svatby	svatba	k1gFnPc1	svatba
<g/>
,	,	kIx,	,
pohřby	pohřeb	k1gInPc1	pohřeb
<g/>
,	,	kIx,	,
oslavy	oslava	k1gFnPc1	oslava
Svátků	Svátek	k1gMnPc2	Svátek
<g/>
,	,	kIx,	,
representace	representace	k1gFnPc1	representace
ve	v	k7c6	v
vnějších	vnější	k2eAgInPc6d1	vnější
vztazích	vztah	k1gInPc6	vztah
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nemají	mít	k5eNaImIp3nP	mít
právo	právo	k1gNnSc4	právo
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
duchovního	duchovní	k2eAgInSc2d1	duchovní
života	život	k1gInSc2	život
nikoho	nikdo	k3yNnSc4	nikdo
z	z	k7c2	z
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gNnSc4	jeho
případné	případný	k2eAgNnSc4d1	případné
chování	chování	k1gNnSc4	chování
nepoškozuje	poškozovat	k5eNaImIp3nS	poškozovat
dobré	dobrý	k2eAgNnSc1d1	dobré
jméno	jméno	k1gNnSc1	jméno
víry	víra	k1gFnSc2	víra
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světové	světový	k2eAgNnSc1d1	světové
centrum	centrum	k1gNnSc1	centrum
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
je	být	k5eAaImIp3nS	být
administrativní	administrativní	k2eAgNnSc1d1	administrativní
centrum	centrum	k1gNnSc1	centrum
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í.	í.	k?	í.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
víra	víra	k1gFnSc1	víra
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
do	do	k7c2	do
247	[number]	k4	247
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
teritorií	teritorium	k1gNnPc2	teritorium
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
společenství	společenství	k1gNnSc4	společenství
existují	existovat	k5eAaImIp3nP	existovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
000	[number]	k4	000
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
́	́	k?	́
<g/>
í	í	k0	í
věřících	věřící	k1gMnPc2	věřící
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
100	[number]	k4	100
různých	různý	k2eAgInPc2d1	různý
národů	národ	k1gInPc2	národ
a	a	k8xC	a
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KROPÁČEK	Kropáček	k1gMnSc1	Kropáček
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgFnPc1d1	duchovní
cesty	cesta	k1gFnPc1	cesta
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
292	[number]	k4	292
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
925	[number]	k4	925
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Šíca	Šíca	k1gFnSc1	Šíca
<g/>
,	,	kIx,	,
s.	s.	k?	s.
201	[number]	k4	201
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
víra	víra	k1gFnSc1	víra
<g/>
?	?	kIx.	?
</s>
<s>
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
́	́	k?	́
<g/>
í	í	k0	í
společenství	společenství	k1gNnPc2	společenství
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
́	́	k?	́
<g/>
í	í	k0	í
společenství	společenství	k1gNnPc2	společenství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
e-Knihovna	e-Knihovna	k1gFnSc1	e-Knihovna
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
Písem	písmo	k1gNnPc2	písmo
s	s	k7c7	s
vyhledáváním	vyhledávání	k1gNnSc7	vyhledávání
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
Baháí	Baháí	k2eAgFnSc2d1	Baháí
víry	víra	k1gFnSc2	víra
na	na	k7c6	na
Českém	český	k2eAgInSc6d1	český
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
pořad	pořad	k1gInSc1	pořad
Hovory	hovora	k1gMnSc2	hovora
o	o	k7c6	o
víře	víra	k1gFnSc6	víra
</s>
</p>
<p>
<s>
Persekuce	persekuce	k1gFnSc1	persekuce
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
věřících	věřící	k2eAgMnPc2d1	věřící
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
BBC	BBC	kA	BBC
–	–	k?	–
představení	představení	k1gNnSc1	představení
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
víry	víra	k1gFnSc2	víra
</s>
</p>
