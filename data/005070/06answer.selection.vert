<s>
Wi-Fi	Wi-Fi	k6eAd1	Wi-Fi
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c6	na
spojové	spojový	k2eAgFnSc6d1	spojová
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
záležitost	záležitost	k1gFnSc4	záležitost
vyšších	vysoký	k2eAgInPc2d2	vyšší
protokolů	protokol	k1gInPc2	protokol
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Bluetooth	Bluetootha	k1gFnPc2	Bluetootha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
služby	služba	k1gFnPc4	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
