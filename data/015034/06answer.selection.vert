<s desamb="1">
Teprve	teprve	k6eAd1
druhý	druhý	k4xOgInSc4
tereziánský	tereziánský	k2eAgInSc4d1
katastr	katastr	k1gInSc4
z	z	k7c2
r.	r.	kA
1757	#num#	k4
zahrnul	zahrnout	k5eAaPmAgInS
jak	jak	k6eAd1
rustikální	rustikální	k2eAgInSc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
dominikální	dominikální	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
a	a	k8xC
kvalitně	kvalitně	k6eAd1
zachytil	zachytit	k5eAaPmAgMnS
bonitu	bonita	k1gFnSc4
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
rozdělil	rozdělit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
do	do	k7c2
osmi	osm	k4xCc2
kategorií	kategorie	k1gFnPc2
podle	podle	k7c2
úrodnosti	úrodnost	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
dokonce	dokonce	k9
více	hodně	k6eAd2
než	než	k8xS
dnešní	dnešní	k2eAgInPc1d1
katastry	katastr	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>