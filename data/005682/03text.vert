<s>
Oštěp	oštěp	k1gInSc1	oštěp
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
vrhací	vrhací	k2eAgFnSc1d1	vrhací
a	a	k8xC	a
bodná	bodný	k2eAgFnSc1d1	bodná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
také	také	k9	také
sportovní	sportovní	k2eAgNnSc4d1	sportovní
náčiní	náčiní	k1gNnSc4	náčiní
<g/>
.	.	kIx.	.
</s>
<s>
Oštěp	oštěp	k1gInSc1	oštěp
je	být	k5eAaImIp3nS	být
vrhací	vrhací	k2eAgFnSc1d1	vrhací
a	a	k8xC	a
bodná	bodný	k2eAgFnSc1d1	bodná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
historickou	historický	k2eAgFnSc4d1	historická
bodnou	bodný	k2eAgFnSc4d1	bodná
zbraň	zbraň	k1gFnSc4	zbraň
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgFnSc7d2	lehčí
a	a	k8xC	a
kratší	krátký	k2eAgFnSc7d2	kratší
variantou	varianta	k1gFnSc7	varianta
kopí	kopit	k5eAaImIp3nS	kopit
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
užívanou	užívaný	k2eAgFnSc4d1	užívaná
především	především	k6eAd1	především
jako	jako	k8xC	jako
sportovní	sportovní	k2eAgNnSc4d1	sportovní
náčiní	náčiní	k1gNnSc4	náčiní
<g/>
.	.	kIx.	.
</s>
<s>
Podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
(	(	kIx(	(
<g/>
ratištěm	ratiště	k1gNnSc7	ratiště
<g/>
)	)	kIx)	)
určité	určitý	k2eAgFnSc2d1	určitá
délky	délka	k1gFnSc2	délka
opatřeným	opatřený	k2eAgInSc7d1	opatřený
hrotem	hrot	k1gInSc7	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Ratiště	ratiště	k1gNnSc1	ratiště
historických	historický	k2eAgInPc2d1	historický
oštěpů	oštěp	k1gInPc2	oštěp
bylo	být	k5eAaImAgNnS	být
zpravidla	zpravidla	k6eAd1	zpravidla
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
nebo	nebo	k8xC	nebo
bambusové	bambusový	k2eAgInPc4d1	bambusový
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
oštěpy	oštěp	k1gInPc1	oštěp
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
z	z	k7c2	z
duralu	dural	k1gInSc2	dural
nebo	nebo	k8xC	nebo
kompozitních	kompozitní	k2eAgInPc2d1	kompozitní
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Hrot	hrot	k1gInSc1	hrot
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
kamenný	kamenný	k2eAgInSc4d1	kamenný
<g/>
,	,	kIx,	,
kostěný	kostěný	k2eAgInSc4d1	kostěný
nebo	nebo	k8xC	nebo
u	u	k7c2	u
modernějších	moderní	k2eAgInPc2d2	modernější
oštěpů	oštěp	k1gInPc2	oštěp
kovový	kovový	k2eAgInSc1d1	kovový
<g/>
.	.	kIx.	.
</s>
<s>
Nejprimitivnější	primitivní	k2eAgInPc1d3	nejprimitivnější
oštěpy	oštěp	k1gInPc1	oštěp
byly	být	k5eAaImAgInP	být
zakončeny	zakončit	k5eAaPmNgInP	zakončit
pouze	pouze	k6eAd1	pouze
zaostřeným	zaostřený	k2eAgInSc7d1	zaostřený
koncem	konec	k1gInSc7	konec
ratiště	ratiště	k1gNnSc2	ratiště
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
oštěpu	oštěp	k1gInSc2	oštěp
je	být	k5eAaImIp3nS	být
nutností	nutnost	k1gFnSc7	nutnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zachování	zachování	k1gNnSc3	zachování
správné	správný	k2eAgFnSc2d1	správná
dráhy	dráha	k1gFnSc2	dráha
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
oštěpem	oštěp	k1gInSc7	oštěp
je	být	k5eAaImIp3nS	být
zaostřený	zaostřený	k2eAgInSc1d1	zaostřený
bambus	bambus	k1gInSc1	bambus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Oštěp	oštěp	k1gInSc1	oštěp
římských	římský	k2eAgMnPc2d1	římský
legionářů	legionář	k1gMnPc2	legionář
zvaný	zvaný	k2eAgInSc4d1	zvaný
"	"	kIx"	"
<g/>
pilum	pilum	k1gInSc4	pilum
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgInPc7d1	mnohý
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejdokonalejší	dokonalý	k2eAgNnSc4d3	nejdokonalejší
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
úzké	úzký	k2eAgFnSc6d1	úzká
kovové	kovový	k2eAgFnSc6d1	kovová
části	část	k1gFnSc6	část
zakončené	zakončený	k2eAgFnSc6d1	zakončená
hrotem	hrot	k1gInSc7	hrot
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
ratišti	ratiště	k1gNnSc3	ratiště
připojena	připojit	k5eAaPmNgFnS	připojit
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
nýty	nýt	k1gInPc7	nýt
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
oštěp	oštěp	k1gInSc1	oštěp
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
projít	projít	k5eAaPmF	projít
štítem	štít	k1gInSc7	štít
prvního	první	k4xOgMnSc2	první
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
mužem	muž	k1gMnSc7	muž
samým	samý	k3xTgMnPc3	samý
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
zabodnout	zabodnout	k5eAaPmF	zabodnout
do	do	k7c2	do
štítu	štít	k1gInSc2	štít
muže	muž	k1gMnSc2	muž
stojícího	stojící	k2eAgMnSc2d1	stojící
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
náhodou	náhodou	k6eAd1	náhodou
prošel	projít	k5eAaPmAgInS	projít
jen	jen	k9	jen
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
se	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vahou	váha	k1gFnSc7	váha
ohnul	ohnout	k5eAaPmAgMnS	ohnout
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
ho	on	k3xPp3gNnSc4	on
možno	možno	k6eAd1	možno
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
spoj	spoj	k1gInSc1	spoj
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
oštěpy	oštěp	k1gInPc1	oštěp
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
rozpadly	rozpadnout	k5eAaPmAgInP	rozpadnout
ve	v	k7c6	v
dví	dví	k1xP	dví
a	a	k8xC	a
nemohly	moct	k5eNaImAgFnP	moct
být	být	k5eAaImF	být
nepřítelem	nepřítel	k1gMnSc7	nepřítel
vrhnuty	vrhnout	k5eAaPmNgInP	vrhnout
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
sportovní	sportovní	k2eAgNnSc1d1	sportovní
náčiní	náčiní	k1gNnSc1	náčiní
je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
duralu	dural	k1gInSc2	dural
<g/>
.	.	kIx.	.
</s>
<s>
Hod	hod	k1gInSc4	hod
oštěpem	oštěp	k1gInSc7	oštěp
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
součástí	součást	k1gFnSc7	součást
olympijské	olympijský	k2eAgFnSc2d1	olympijská
lehkoatletické	lehkoatletický	k2eAgFnSc2d1	lehkoatletická
disciplíny	disciplína	k1gFnSc2	disciplína
desetiboje	desetiboj	k1gInSc2	desetiboj
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
atletického	atletický	k2eAgInSc2d1	atletický
víceboje	víceboj	k1gInSc2	víceboj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lovecký	lovecký	k2eAgInSc4d1	lovecký
oštěp	oštěp	k1gInSc4	oštěp
Kopí	kopit	k5eAaImIp3nS	kopit
Desetiboj	desetiboj	k1gInSc1	desetiboj
Hod	hod	k1gInSc4	hod
oštěpem	oštěp	k1gInSc7	oštěp
Vrhač	vrhač	k1gInSc1	vrhač
oštěpů	oštěp	k1gInPc2	oštěp
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oštěp	oštěp	k1gInSc4	oštěp
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
