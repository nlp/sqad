<s>
Oštěp	oštěp	k1gInSc1
je	být	k5eAaImIp3nS
vrhací	vrhací	k2eAgFnSc1d1
a	a	k8xC
bodná	bodný	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
historickou	historický	k2eAgFnSc4d1
bodnou	bodný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
která	který	k3yQgFnSc1
je	být	k5eAaImIp3nS
lehčí	lehký	k2eAgFnSc7d2
a	a	k8xC
kratší	krátký	k2eAgFnSc7d2
variantou	varianta	k1gFnSc7
kopí	kopit	k5eAaImIp3nS
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
běžně	běžně	k6eAd1
užívanou	užívaný	k2eAgFnSc4d1
především	především	k6eAd1
jako	jako	k8xC
sportovní	sportovní	k2eAgNnSc4d1
náčiní	náčiní	k1gNnSc4
<g/>
.	.	kIx.
</s>