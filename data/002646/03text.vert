<s>
Glum	Glum	k1gInSc1	Glum
-	-	kIx~	-
Sméagol	Sméagol	k1gInSc1	Sméagol
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Gollum	Gollum	k1gNnSc1	Gollum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
spisovatelem	spisovatel	k1gMnSc7	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkienem	Tolkien	k1gInSc7	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Tolkienově	Tolkienův	k2eAgFnSc6d1	Tolkienova
fantasy	fantas	k1gInPc1	fantas
novele	novela	k1gFnSc6	novela
Hobit	hobit	k1gMnSc1	hobit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
autorově	autorův	k2eAgFnSc6d1	autorova
epické	epický	k2eAgFnSc6d1	epická
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Sméagol	Sméagol	k1gInSc4	Sméagol
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Glum	Gluma	k1gFnPc2	Gluma
podle	podle	k7c2	podle
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vydával	vydávat	k5eAaImAgMnS	vydávat
svým	svůj	k3xOyFgNnSc7	svůj
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Sméagol	Sméagol	k1gInSc1	Sméagol
byl	být	k5eAaImAgInS	být
nejzvídavějším	zvídavý	k2eAgInSc7d3	zvídavý
členem	člen	k1gInSc7	člen
bohaté	bohatý	k2eAgNnSc1d1	bohaté
hobití	hobití	k1gNnSc1	hobití
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
se	se	k3xPyFc4	se
Staty	status	k1gInPc7	status
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Velké	velký	k2eAgFnSc2d1	velká
řeky	řeka	k1gFnSc2	řeka
poblíž	poblíž	k6eAd1	poblíž
Kosatcových	kosatcový	k2eAgFnPc2d1	Kosatcová
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2463	[number]	k4	2463
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
si	se	k3xPyFc3	se
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
jménem	jméno	k1gNnSc7	jméno
Déagol	Déagol	k1gInSc4	Déagol
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
na	na	k7c6	na
člunu	člun	k1gInSc6	člun
ke	k	k7c3	k
Kosatcovým	kosatcový	k2eAgFnPc3d1	Kosatcová
polím	pole	k1gFnPc3	pole
a	a	k8xC	a
lovili	lovit	k5eAaImAgMnP	lovit
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Zabrala	zabrat	k5eAaPmAgFnS	zabrat
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
strhla	strhnout	k5eAaPmAgFnS	strhnout
Déagola	Déagola	k1gFnSc1	Déagola
pod	pod	k7c4	pod
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
uviděl	uvidět	k5eAaPmAgInS	uvidět
něco	něco	k3yInSc4	něco
třpytivého	třpytivý	k2eAgNnSc2d1	třpytivé
<g/>
,	,	kIx,	,
hrábl	hrábnout	k5eAaPmAgInS	hrábnout
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Sméagol	Sméagol	k1gInSc1	Sméagol
jej	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
,	,	kIx,	,
zatoužil	zatoužit	k5eAaPmAgMnS	zatoužit
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
nálezu	nález	k1gInSc6	nález
a	a	k8xC	a
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
přítel	přítel	k1gMnSc1	přítel
nechtěl	chtít	k5eNaImAgMnS	chtít
dát	dát	k5eAaPmF	dát
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Neviditelnost	neviditelnost	k1gFnSc1	neviditelnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prsten	prsten	k1gInSc1	prsten
propůjčoval	propůjčovat	k5eAaImAgInS	propůjčovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
užíval	užívat	k5eAaImAgMnS	užívat
ke	k	k7c3	k
zlomyslnostem	zlomyslnost	k1gFnPc3	zlomyslnost
a	a	k8xC	a
špehování	špehování	k1gNnPc2	špehování
druhých	druhý	k4xOgInPc2	druhý
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
neoblíbeným	oblíbený	k2eNgMnSc7d1	neoblíbený
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nazván	nazvat	k5eAaPmNgMnS	nazvat
Glumem	Glumo	k1gNnSc7	Glumo
a	a	k8xC	a
bába	bába	k1gFnSc1	bába
rodu	rod	k1gInSc2	rod
jej	on	k3xPp3gInSc4	on
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Schoval	schovat	k5eAaPmAgInS	schovat
se	se	k3xPyFc4	se
do	do	k7c2	do
jeskyní	jeskyně	k1gFnPc2	jeskyně
pod	pod	k7c7	pod
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
tam	tam	k6eAd1	tam
mocí	moc	k1gFnSc7	moc
Prstenu	prsten	k1gInSc2	prsten
žil	žít	k5eAaImAgMnS	žít
nepřirozeně	přirozeně	k6eNd1	přirozeně
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
let	léto	k1gNnPc2	léto
a	a	k8xC	a
změnil	změnit	k5eAaPmAgInS	změnit
se	se	k3xPyFc4	se
k	k	k7c3	k
nepoznání	nepoznání	k1gNnSc3	nepoznání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2941	[number]	k4	2941
však	však	k9	však
Prsten	prsten	k1gInSc1	prsten
ztratil	ztratit	k5eAaPmAgInS	ztratit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
nalezl	naleznout	k5eAaPmAgInS	naleznout
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
,	,	kIx,	,
Glum	Glum	k1gMnSc1	Glum
si	se	k3xPyFc3	se
však	však	k9	však
domyslel	domyslet	k5eAaPmAgMnS	domyslet
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
jej	on	k3xPp3gMnSc4	on
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
připravil	připravit	k5eAaPmAgMnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2944	[number]	k4	2944
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
přemohla	přemoct	k5eAaPmAgFnS	přemoct
jeho	on	k3xPp3gInSc4	on
strach	strach	k1gInSc4	strach
a	a	k8xC	a
po	po	k7c6	po
Bilbových	Bilbův	k2eAgFnPc6d1	Bilbova
stopách	stopa	k1gFnPc6	stopa
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
do	do	k7c2	do
Esgarothu	Esgaroth	k1gInSc2	Esgaroth
a	a	k8xC	a
ulic	ulice	k1gFnPc2	ulice
Dolu	dol	k1gInSc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bilba	Bilba	k1gFnSc1	Bilba
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
2951	[number]	k4	2951
jej	on	k3xPp3gInSc4	on
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
Stínu	stín	k1gInSc6	stín
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
přiměla	přimět	k5eAaPmAgFnS	přimět
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgInS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
najde	najít	k5eAaPmIp3nS	najít
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
pomohou	pomoct	k5eAaPmIp3nP	pomoct
k	k	k7c3	k
pomstě	pomsta	k1gFnSc3	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2980	[number]	k4	2980
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Mordoru	Mordor	k1gInSc2	Mordor
s	s	k7c7	s
Odulou	odulý	k2eAgFnSc7d1	odulá
<g/>
.	.	kIx.	.
</s>
<s>
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
služebníci	služebník	k1gMnPc1	služebník
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
chytili	chytit	k5eAaPmAgMnP	chytit
a	a	k8xC	a
odvedli	odvést	k5eAaPmAgMnP	odvést
k	k	k7c3	k
výslechu	výslech	k1gInSc3	výslech
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
3017	[number]	k4	3017
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
Mordoru	Mordor	k1gInSc2	Mordor
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
a	a	k8xC	a
zajal	zajmout	k5eAaPmAgMnS	zajmout
ho	on	k3xPp3gInSc4	on
Aragorn	Aragorn	k1gInSc4	Aragorn
v	v	k7c6	v
Mrtvých	mrtvý	k2eAgInPc6d1	mrtvý
močálech	močál	k1gInPc6	močál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
zavedl	zavést	k5eAaPmAgInS	zavést
k	k	k7c3	k
Thranduilovi	Thranduilův	k2eAgMnPc1d1	Thranduilův
v	v	k7c6	v
Temném	temný	k2eAgInSc6d1	temný
hvozdu	hvozd	k1gInSc6	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
byli	být	k5eAaImAgMnP	být
Thranduilovi	Thranduilův	k2eAgMnPc1d1	Thranduilův
elfové	elf	k1gMnPc1	elf
napadeni	napadnout	k5eAaPmNgMnP	napadnout
skřety	skřet	k1gMnPc7	skřet
a	a	k8xC	a
Glum	Glum	k1gMnSc1	Glum
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
srpna	srpen	k1gInSc2	srpen
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
uniknout	uniknout	k5eAaPmF	uniknout
pronásledování	pronásledování	k1gNnSc4	pronásledování
našel	najít	k5eAaPmAgInS	najít
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
Morii	Morie	k1gFnSc6	Morie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
čekal	čekat	k5eAaImAgInS	čekat
poblíž	poblíž	k6eAd1	poblíž
Západní	západní	k2eAgFnPc1d1	západní
brány	brána	k1gFnPc1	brána
Morie	Morie	k1gFnSc2	Morie
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
3019	[number]	k4	3019
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
Společenstvo	společenstvo	k1gNnSc4	společenstvo
prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
jej	on	k3xPp3gMnSc4	on
sledovat	sledovat	k5eAaImF	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Lórienu	Lórien	k1gInSc2	Lórien
unikl	uniknout	k5eAaPmAgMnS	uniknout
před	před	k7c7	před
elfy	elf	k1gMnPc7	elf
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
Stříberky	Stříberka	k1gFnSc2	Stříberka
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
ze	z	k7c2	z
západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
odjezd	odjezd	k1gInSc1	odjezd
družiny	družina	k1gFnSc2	družina
a	a	k8xC	a
pak	pak	k6eAd1	pak
plaval	plavat	k5eAaImAgMnS	plavat
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
dostihl	dostihnout	k5eAaPmAgMnS	dostihnout
Froda	Froda	k1gMnSc1	Froda
a	a	k8xC	a
Sama	sám	k3xTgFnSc1	sám
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Emyn	Emyno	k1gNnPc2	Emyno
Muilu	Muila	k1gFnSc4	Muila
<g/>
,	,	kIx,	,
napadl	napadnout	k5eAaPmAgMnS	napadnout
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
přemožen	přemoct	k5eAaPmNgInS	přemoct
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc4	život
si	se	k3xPyFc3	se
zachránil	zachránit	k5eAaPmAgMnS	zachránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přísahal	přísahat	k5eAaImAgMnS	přísahat
při	při	k7c6	při
Prstenu	prsten	k1gInSc6	prsten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gFnPc3	on
bude	být	k5eAaImBp3nS	být
pomáhat	pomáhat	k5eAaImF	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Provedl	provést	k5eAaPmAgMnS	provést
hobity	hobit	k1gMnPc4	hobit
přes	přes	k7c4	přes
Mrtvé	mrtvý	k1gMnPc4	mrtvý
močály	močál	k1gInPc4	močál
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
Černé	Černé	k2eAgFnSc2d1	Černé
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Frodovi	Froda	k1gMnSc3	Froda
zabránil	zabránit	k5eAaPmAgInS	zabránit
vstoupit	vstoupit	k5eAaPmF	vstoupit
a	a	k8xC	a
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
nechal	nechat	k5eAaPmAgInS	nechat
vést	vést	k5eAaImF	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
tajné	tajný	k2eAgFnSc3d1	tajná
cestě	cesta	k1gFnSc3	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Froda	Froda	k1gMnSc1	Froda
zajal	zajmout	k5eAaPmAgMnS	zajmout
v	v	k7c6	v
Ithilienu	Ithilieno	k1gNnSc6	Ithilieno
Faramir	Faramir	k1gMnSc1	Faramir
<g/>
,	,	kIx,	,
Glum	Glum	k1gMnSc1	Glum
vyklouzl	vyklouznout	k5eAaPmAgMnS	vyklouznout
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ránem	ráno	k1gNnSc7	ráno
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
ale	ale	k9	ale
sám	sám	k3xTgMnSc1	sám
nevědomky	nevědomky	k6eAd1	nevědomky
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
tajného	tajný	k2eAgInSc2d1	tajný
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
,	,	kIx,	,
zapovězené	zapovězený	k2eAgFnSc2d1	zapovězená
tůně	tůně	k1gFnSc2	tůně
v	v	k7c4	v
Henneth	Henneth	k1gInSc4	Henneth
Annû	Annû	k1gFnSc2	Annû
a	a	k8xC	a
Frodo	Frodo	k1gNnSc1	Frodo
jej	on	k3xPp3gInSc2	on
musel	muset	k5eAaImAgMnS	muset
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
nalákat	nalákat	k5eAaPmF	nalákat
do	do	k7c2	do
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
gondorští	gondorský	k2eAgMnPc1d1	gondorský
vojáci	voják	k1gMnPc1	voják
mohli	moct	k5eAaImAgMnP	moct
snadno	snadno	k6eAd1	snadno
zajmout	zajmout	k5eAaPmF	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Glum	Glum	k1gInSc1	Glum
vnímal	vnímat	k5eAaImAgInS	vnímat
jako	jako	k9	jako
zradu	zrada	k1gFnSc4	zrada
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mohl	moct	k5eAaImAgInS	moct
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
temné	temný	k2eAgInPc4d1	temný
záměry	záměr	k1gInPc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tajné	tajný	k2eAgFnSc6d1	tajná
návštěvě	návštěva	k1gFnSc6	návštěva
Oduly	odout	k5eAaPmAgFnP	odout
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
sice	sice	k8xC	sice
Glum	Glum	k1gMnSc1	Glum
dostal	dostat	k5eAaPmAgMnS	dostat
poslední	poslední	k2eAgFnSc4d1	poslední
příležitost	příležitost	k1gFnSc4	příležitost
se	se	k3xPyFc4	se
napravit	napravit	k5eAaPmF	napravit
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
spícího	spící	k2eAgMnSc4d1	spící
Froda	Frod	k1gMnSc4	Frod
pocítil	pocítit	k5eAaPmAgMnS	pocítit
lítost	lítost	k1gFnSc4	lítost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
okamžik	okamžik	k1gInSc1	okamžik
až	až	k9	až
příliš	příliš	k6eAd1	příliš
snadno	snadno	k6eAd1	snadno
zkazil	zkazit	k5eAaPmAgMnS	zkazit
Sam	Sam	k1gMnSc1	Sam
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgInSc1d1	příští
den	den	k1gInSc1	den
večer	večer	k1gInSc1	večer
zavedl	zavést	k5eAaPmAgMnS	zavést
oba	dva	k4xCgMnPc4	dva
hobity	hobit	k1gMnPc4	hobit
do	do	k7c2	do
doupěte	doupě	k1gNnSc2	doupě
Oduly	odout	k5eAaPmAgFnP	odout
a	a	k8xC	a
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
jeho	jeho	k3xOp3gFnSc3	jeho
zradě	zrada	k1gFnSc3	zrada
dostali	dostat	k5eAaPmAgMnP	dostat
z	z	k7c2	z
tunelu	tunel	k1gInSc2	tunel
<g/>
,	,	kIx,	,
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
Sama	Sam	k1gMnSc4	Sam
právě	právě	k6eAd1	právě
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
Froda	Frod	k1gMnSc4	Frod
útočila	útočit	k5eAaImAgFnS	útočit
Odula	odout	k5eAaPmAgFnS	odout
a	a	k8xC	a
zabránil	zabránit	k5eAaPmAgMnS	zabránit
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Sam	Sam	k1gMnSc1	Sam
se	se	k3xPyFc4	se
Glumovi	Glum	k1gMnSc3	Glum
ubránil	ubránit	k5eAaPmAgInS	ubránit
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
proto	proto	k8xC	proto
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k1gInSc1	Glum
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
oba	dva	k4xCgMnPc4	dva
hobity	hobit	k1gMnPc4	hobit
až	až	k8xS	až
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
dostihl	dostihnout	k5eAaPmAgMnS	dostihnout
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Pochopil	pochopit	k5eAaPmAgMnS	pochopit
jejich	jejich	k3xOp3gInSc4	jejich
úmysl	úmysl	k1gInSc4	úmysl
a	a	k8xC	a
v	v	k7c6	v
zoufalství	zoufalství	k1gNnSc6	zoufalství
je	být	k5eAaImIp3nS	být
napadl	napadnout	k5eAaPmAgMnS	napadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
přemožen	přemoct	k5eAaPmNgInS	přemoct
<g/>
.	.	kIx.	.
</s>
<s>
Samovi	Sam	k1gMnSc3	Sam
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
zželelo	zželet	k5eAaPmAgNnS	zželet
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
nejprve	nejprve	k6eAd1	nejprve
chtěl	chtít	k5eAaImAgMnS	chtít
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
to	ten	k3xDgNnSc4	ten
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ho	on	k3xPp3gMnSc4	on
běžet	běžet	k5eAaImF	běžet
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k1gMnSc1	Glum
ale	ale	k9	ale
odešel	odejít	k5eAaPmAgMnS	odejít
jen	jen	k9	jen
naoko	naoko	k6eAd1	naoko
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
hobity	hobit	k1gMnPc7	hobit
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
okraji	okraj	k1gInSc6	okraj
propasti	propast	k1gFnSc2	propast
<g/>
.	.	kIx.	.
</s>
<s>
Srazil	srazit	k5eAaPmAgInS	srazit
Sama	sám	k3xTgMnSc4	sám
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
skočil	skočit	k5eAaPmAgMnS	skočit
na	na	k7c4	na
neviditelného	viditelný	k2eNgMnSc4d1	Neviditelný
Froda	Frod	k1gMnSc4	Frod
<g/>
,	,	kIx,	,
zápasil	zápasit	k5eAaImAgMnS	zápasit
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
,	,	kIx,	,
ukousl	ukousnout	k5eAaPmAgMnS	ukousnout
Frodovi	Froda	k1gMnSc3	Froda
prst	prst	k1gInSc4	prst
a	a	k8xC	a
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
se	se	k3xPyFc4	se
Prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
záchvěvu	záchvěv	k1gInSc6	záchvěv
štěstí	štěstí	k1gNnSc2	štěstí
udělal	udělat	k5eAaPmAgInS	udělat
krok	krok	k1gInSc1	krok
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
padl	padnout	k5eAaPmAgMnS	padnout
do	do	k7c2	do
Pukliny	puklina	k1gFnSc2	puklina
Osudu	osud	k1gInSc2	osud
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
Prsten	prsten	k1gInSc4	prsten
zničen	zničen	k2eAgInSc4d1	zničen
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hunt	hunt	k1gInSc1	hunt
for	forum	k1gNnPc2	forum
Gollum	Gollum	k1gInSc1	Gollum
</s>
