<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Sméagol	Sméagol	k1gInSc4	Sméagol
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Glum	Gluma	k1gFnPc2	Gluma
podle	podle	k7c2	podle
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vydával	vydávat	k5eAaImAgMnS	vydávat
svým	svůj	k3xOyFgNnSc7	svůj
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
.	.	kIx.	.
</s>
