<s>
Kladná	kladný	k2eAgFnSc1d1	kladná
elektroda	elektroda	k1gFnSc1	elektroda
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
oxid-hydroxidu	oxidydroxid	k1gInSc2	oxid-hydroxid
niklitého	niklitý	k2eAgInSc2d1	niklitý
–	–	k?	–
NiO	NiO	k1gFnSc1	NiO
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektrolytem	elektrolyt	k1gInSc7	elektrolyt
je	být	k5eAaImIp3nS	být
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
