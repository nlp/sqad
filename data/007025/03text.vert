<s>
Nikon	Nikon	k1gInSc1	Nikon
D300	D300	k1gFnSc2	D300
je	být	k5eAaImIp3nS	být
jednooká	jednooký	k2eAgFnSc1d1	jednooká
digitální	digitální	k2eAgFnSc1d1	digitální
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
firmy	firma	k1gFnSc2	firma
Nikon	Nikon	k1gNnSc1	Nikon
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
na	na	k7c4	na
trh	trh	k1gInSc4	trh
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
modelu	model	k1gInSc2	model
D200	D200	k1gFnSc1	D200
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
vyspělé	vyspělý	k2eAgInPc4d1	vyspělý
amatérské	amatérský	k2eAgInPc4d1	amatérský
a	a	k8xC	a
profesionální	profesionální	k2eAgMnPc4d1	profesionální
fotografy	fotograf	k1gMnPc4	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Úrovní	úroveň	k1gFnSc7	úroveň
je	být	k5eAaImIp3nS	být
posazen	posadit	k5eAaPmNgMnS	posadit
mezi	mezi	k7c4	mezi
amatérský	amatérský	k2eAgInSc4d1	amatérský
model	model	k1gInSc4	model
Nikon	Nikon	k1gMnSc1	Nikon
D80	D80	k1gMnSc1	D80
a	a	k8xC	a
profesionální	profesionální	k2eAgMnSc1d1	profesionální
Nikon	Nikon	k1gMnSc1	Nikon
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
modelům	model	k1gInPc3	model
Canon	Canon	kA	Canon
EOS	EOS	kA	EOS
40	[number]	k4	40
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
Olympus	Olympus	k1gMnSc1	Olympus
E-3	E-3	k1gMnSc1	E-3
a	a	k8xC	a
Sony	Sony	kA	Sony
α	α	k?	α
<g/>
700	[number]	k4	700
<g/>
.	.	kIx.	.
</s>
<s>
Čip	čip	k1gInSc1	čip
<g/>
:	:	kIx,	:
CMOS	CMOS	kA	CMOS
formátu	formát	k1gInSc2	formát
Nikon	Nikon	k1gInSc1	Nikon
DX	DX	kA	DX
23.6	[number]	k4	23.6
mm	mm	kA	mm
×	×	k?	×
15.8	[number]	k4	15.8
mm	mm	kA	mm
s	s	k7c7	s
4,288	[number]	k4	4,288
×	×	k?	×
2,848	[number]	k4	2,848
(	(	kIx(	(
<g/>
13.1	[number]	k4	13.1
M	M	kA	M
<g/>
/	/	kIx~	/
<g/>
12.3	[number]	k4	12.3
M	M	kA	M
sensor	sensora	k1gFnPc2	sensora
<g/>
/	/	kIx~	/
<g/>
efektivních	efektivní	k2eAgInPc2d1	efektivní
pixelů	pixel	k1gInPc2	pixel
<g/>
)	)	kIx)	)
Závěrka	závěrka	k1gFnSc1	závěrka
<g/>
:	:	kIx,	:
vertikální	vertikální	k2eAgFnSc1d1	vertikální
<g/>
,	,	kIx,	,
elektronicky	elektronicky	k6eAd1	elektronicky
řízená	řízený	k2eAgFnSc1d1	řízená
<g/>
,	,	kIx,	,
30	[number]	k4	30
-	-	kIx~	-
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
8000	[number]	k4	8000
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
B	B	kA	B
51	[number]	k4	51
ostřících	ostřící	k2eAgInPc2d1	ostřící
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
15	[number]	k4	15
křížových	křížový	k2eAgFnPc2d1	křížová
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
režimů	režim	k1gInPc2	režim
ostření	ostření	k1gNnSc2	ostření
optický	optický	k2eAgInSc4d1	optický
hledáček	hledáček	k1gInSc4	hledáček
se	s	k7c7	s
100	[number]	k4	100
<g/>
%	%	kIx~	%
krytím	krytí	k1gNnSc7	krytí
<g/>
,	,	kIx,	,
display	displa	k1gMnPc7	displa
(	(	kIx(	(
<g/>
VGA	VGA	kA	VGA
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
)	)	kIx)	)
s	s	k7c7	s
živým	živý	k2eAgInSc7d1	živý
náhledem	náhled	k1gInSc7	náhled
rychlost	rychlost	k1gFnSc1	rychlost
snímání	snímání	k1gNnSc1	snímání
6	[number]	k4	6
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
s	s	k7c7	s
přídavným	přídavný	k2eAgNnSc7d1	přídavné
bateriovým	bateriový	k2eAgNnSc7d1	bateriové
pouzdrem	pouzdro	k1gNnSc7	pouzdro
a	a	k8xC	a
speciálními	speciální	k2eAgFnPc7d1	speciální
bateriemi	baterie	k1gFnPc7	baterie
8	[number]	k4	8
(	(	kIx(	(
<g/>
přístroj	přístroj	k1gInSc1	přístroj
sám	sám	k3xTgInSc1	sám
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
7	[number]	k4	7
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
bez	bez	k7c2	bez
přídavného	přídavný	k2eAgNnSc2d1	přídavné
příslušenství	příslušenství	k1gNnSc2	příslušenství
uměle	uměle	k6eAd1	uměle
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
)	)	kIx)	)
</s>
