<s>
Paršovice	Paršovice	k1gFnPc1	Paršovice
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zvané	zvaný	k2eAgNnSc1d1	zvané
Záhoří	Záhoří	k1gNnSc1	Záhoří
<g/>
,	,	kIx,	,
cca	cca	kA	cca
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
Hranic	Hranice	k1gFnPc2	Hranice
<g/>
,	,	kIx,	,
12	[number]	k4	12
km	km	kA	km
od	od	k7c2	od
Bystřice	Bystřice	k1gFnSc2	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
mikroregionu	mikroregion	k1gInSc2	mikroregion
Záhoran	Záhoran	k1gInSc1	Záhoran
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
322	[number]	k4	322
m	m	kA	m
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
1356	[number]	k4	1356
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
doloženou	doložený	k2eAgFnSc7d1	doložená
památkou	památka	k1gFnSc7	památka
byl	být	k5eAaImAgInS	být
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
kalich	kalich	k1gInSc1	kalich
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
1059	[number]	k4	1059
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
v	v	k7c6	v
inventáři	inventář	k1gInSc6	inventář
paršovického	paršovický	k2eAgInSc2d1	paršovický
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
potom	potom	k6eAd1	potom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1371	[number]	k4	1371
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obec	obec	k1gFnSc1	obec
koupil	koupit	k5eAaPmAgMnS	koupit
Vok	Vok	k1gMnPc4	Vok
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
let	léto	k1gNnPc2	léto
součástí	součást	k1gFnPc2	součást
majetku	majetek	k1gInSc2	majetek
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Helfštýna	Helfštýn	k1gMnSc2	Helfštýn
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
kladou	klást	k5eAaImIp3nP	klást
její	její	k3xOp3gInSc4	její
vznik	vznik	k1gInSc4	vznik
ještě	ještě	k6eAd1	ještě
dál	daleko	k6eAd2	daleko
–	–	k?	–
až	až	k6eAd1	až
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
věrohodně	věrohodně	k6eAd1	věrohodně
doloženy	doložit	k5eAaPmNgInP	doložit
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
empírový	empírový	k2eAgInSc1d1	empírový
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
posvěcený	posvěcený	k2eAgInSc1d1	posvěcený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
–	–	k?	–
jediný	jediný	k2eAgInSc1d1	jediný
empírový	empírový	k2eAgInSc1d1	empírový
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
stál	stát	k5eAaImAgInS	stát
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kostel	kostel	k1gInSc4	kostel
–	–	k?	–
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
reformace	reformace	k1gFnSc2	reformace
přeměněný	přeměněný	k2eAgInSc1d1	přeměněný
na	na	k7c4	na
českobratrský	českobratrský	k2eAgInSc4d1	českobratrský
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
na	na	k7c4	na
katolický	katolický	k2eAgInSc4d1	katolický
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
a	a	k8xC	a
po	po	k7c4	po
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
budován	budovat	k5eAaImNgInS	budovat
kostel	kostel	k1gInSc1	kostel
nynější	nynější	k2eAgInSc1d1	nynější
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
výzdoby	výzdoba	k1gFnSc2	výzdoba
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
mramorový	mramorový	k2eAgInSc1d1	mramorový
oltář	oltář	k1gInSc1	oltář
a	a	k8xC	a
cenný	cenný	k2eAgInSc1d1	cenný
obraz	obraz	k1gInSc1	obraz
Vidění	vidění	k1gNnSc2	vidění
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
mistr	mistr	k1gMnSc1	mistr
Urban	Urban	k1gMnSc1	Urban
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
tzv.	tzv.	kA	tzv.
misijní	misijní	k2eAgInSc4d1	misijní
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
kamenný	kamenný	k2eAgInSc4d1	kamenný
kříž	kříž	k1gInSc4	kříž
v	v	k7c6	v
lidovém	lidový	k2eAgInSc6d1	lidový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k6eAd1	naproti
kostela	kostel	k1gInSc2	kostel
stojí	stát	k5eAaImIp3nS	stát
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
u	u	k7c2	u
odbočky	odbočka	k1gFnSc2	odbočka
na	na	k7c4	na
Valšovice	Valšovice	k1gFnPc4	Valšovice
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
sokolovny	sokolovna	k1gFnSc2	sokolovna
socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
Ježíškem	ježíšek	k1gInSc7	ježíšek
<g/>
,	,	kIx,	,
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
konci	konec	k1gInSc6	konec
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
malých	malý	k2eAgFnPc2d1	malá
kapliček	kaplička	k1gFnPc2	kaplička
a	a	k8xC	a
božích	boží	k2eAgFnPc2d1	boží
muk	muka	k1gFnPc2	muka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
pamětihodností	pamětihodnost	k1gFnSc7	pamětihodnost
v	v	k7c6	v
Paršovicích	Paršovice	k1gFnPc6	Paršovice
je	být	k5eAaImIp3nS	být
školní	školní	k2eAgFnSc1d1	školní
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
škola	škola	k1gFnSc1	škola
císaře	císař	k1gMnSc2	císař
pána	pán	k1gMnSc2	pán
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
<g/>
"	"	kIx"	"
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
mužského	mužský	k2eAgNnSc2d1	mužské
jména	jméno	k1gNnSc2	jméno
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
–	–	k?	–
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
staročesky	staročesky	k6eAd1	staročesky
Pareš	Pareš	k1gFnSc1	Pareš
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
původu	původ	k1gInSc2	původ
názvu	název	k1gInSc2	název
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
obec	obec	k1gFnSc1	obec
lidu	lid	k1gInSc2	lid
Bartošova	Bartošův	k2eAgInSc2d1	Bartošův
(	(	kIx(	(
<g/>
Parešova	Parešův	k2eAgInSc2d1	Parešův
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
–	–	k?	–
tedy	tedy	k8xC	tedy
původně	původně	k6eAd1	původně
Parešovice	Parešovice	k1gFnSc1	Parešovice
<g/>
,	,	kIx,	,
zkráceno	zkrácen	k2eAgNnSc1d1	zkráceno
na	na	k7c4	na
Paršovice	Paršovice	k1gFnPc4	Paršovice
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Paršovice	Paršovice	k1gFnSc2	Paršovice
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Paršovice	Paršovice	k1gFnSc2	Paršovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
