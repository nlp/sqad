<p>
<s>
John	John	k1gMnSc1	John
Logie	Logie	k1gFnSc2	Logie
Baird	Baird	k1gMnSc1	Baird
FRSE	FRSE	kA	FRSE
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1888	[number]	k4	1888
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
skotský	skotský	k2eAgMnSc1d1	skotský
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
prvního	první	k4xOgInSc2	první
televizoru	televizor	k1gInSc2	televizor
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
předveden	předvést	k5eAaPmNgInS	předvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
přenos	přenos	k1gInSc1	přenos
živého	živý	k2eAgInSc2d1	živý
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
dálkový	dálkový	k2eAgInSc1d1	dálkový
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
první	první	k4xOgNnSc4	první
plně	plně	k6eAd1	plně
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
trubici	trubice	k1gFnSc4	trubice
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
obrazovek	obrazovka	k1gFnPc2	obrazovka
barevných	barevný	k2eAgInPc2d1	barevný
televizních	televizní	k2eAgInPc2d1	televizní
přijímačů	přijímač	k1gInPc2	přijímač
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1944	[number]	k4	1944
představil	představit	k5eAaPmAgMnS	představit
první	první	k4xOgFnSc4	první
barevnou	barevný	k2eAgFnSc4d1	barevná
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
elektromechanický	elektromechanický	k2eAgInSc1d1	elektromechanický
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
nahrazen	nahradit	k5eAaPmNgInS	nahradit
čistě	čistě	k6eAd1	čistě
elektronickým	elektronický	k2eAgInSc7d1	elektronický
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
oznámil	oznámit	k5eAaPmAgMnS	oznámit
vyvinutí	vyvinutí	k1gNnSc4	vyvinutí
stereoskopické	stereoskopický	k2eAgFnSc2d1	stereoskopická
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
44	[number]	k4	44
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
100	[number]	k4	100
největších	veliký	k2eAgMnPc2d3	veliký
Britů	Brit	k1gMnPc2	Brit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
mezi	mezi	k7c7	mezi
10	[number]	k4	10
největších	veliký	k2eAgMnPc2d3	veliký
skotských	skotský	k2eAgMnPc2d1	skotský
vědců	vědec	k1gMnPc2	vědec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Skotskou	skotský	k2eAgFnSc7d1	skotská
národní	národní	k2eAgFnSc7d1	národní
knihovnou	knihovna	k1gFnSc7	knihovna
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
Skotské	skotský	k2eAgFnSc2d1	skotská
vědecké	vědecký	k2eAgFnSc2d1	vědecká
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
John	John	k1gMnSc1	John
Logie	Logie	k1gFnSc2	Logie
Baird	Bairdo	k1gNnPc2	Bairdo
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
Logie	Logie	k1gFnSc2	Logie
Baird	Bairdo	k1gNnPc2	Bairdo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Baird	Baird	k1gMnSc1	Baird
Television	Television	k1gInSc1	Television
Website	Websit	k1gInSc5	Websit
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bairdova	Bairdův	k2eAgFnSc1d1	Bairdova
biografie	biografie	k1gFnSc1	biografie
na	na	k7c4	na
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bairdova	Bairdův	k2eAgFnSc1d1	Bairdova
biografie	biografie	k1gFnSc1	biografie
na	na	k7c4	na
BFI	BFI	kA	BFI
Screenonline	Screenonlin	k1gInSc5	Screenonlin
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Logie	Logie	k1gFnSc2	Logie
Baird	Baird	k1gMnSc1	Baird
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Helensburgh	Helensburgh	k1gMnSc1	Helensburgh
Heroes	Heroes	k1gMnSc1	Heroes
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bairdova	Bairdův	k2eAgFnSc1d1	Bairdova
biografie	biografie	k1gFnSc1	biografie
na	na	k7c4	na
Virtual	Virtual	k1gInSc4	Virtual
Scotland	Scotlanda	k1gFnPc2	Scotlanda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
Who	Who	k1gMnSc1	Who
Invented	Invented	k1gMnSc1	Invented
Television	Television	k1gInSc1	Television
(	(	kIx(	(
<g/>
knol	knol	k1gInSc1	knol
<g/>
)	)	kIx)	)
</s>
</p>
