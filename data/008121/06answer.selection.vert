<s>
Kofola	Kofola	k1gFnSc1	Kofola
je	být	k5eAaImIp3nS	být
sycený	sycený	k2eAgInSc4d1	sycený
nealkoholický	alkoholický	k2eNgInSc4d1	nealkoholický
nápoj	nápoj	k1gInSc4	nápoj
kolového	kolový	k2eAgInSc2d1	kolový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
tehdy	tehdy	k6eAd1	tehdy
nedostupným	dostupný	k2eNgInPc3d1	nedostupný
konkurenčním	konkurenční	k2eAgInPc3d1	konkurenční
nápojům	nápoj	k1gInPc3	nápoj
Coca-Cola	cocaola	k1gFnSc1	coca-cola
nebo	nebo	k8xC	nebo
Pepsi	Pepsi	k1gFnSc1	Pepsi
<g/>
.	.	kIx.	.
</s>
