<s>
Tennessin	Tennessin	k1gMnSc1
</s>
<s>
Tennessin	Tennessin	k1gMnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
astatu	astat	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
292	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ts	ts	kA
</s>
<s>
117	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Tennessin	Tennessin	k1gMnSc1
<g/>
,	,	kIx,
Ts	ts	k0
<g/>
,	,	kIx,
117	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
angl.	angl.	k?
Tennessine	Tennessin	k1gMnSc5
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
p	p	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
54101-14-3	54101-14-3	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
vypočítaná	vypočítaný	k2eAgFnSc1d1
292,207	292,207	k4
<g/>
73	#num#	k4
<g/>
(	(	kIx(
<g/>
89	#num#	k4
<g/>
)	)	kIx)
pro	pro	k7c4
292	#num#	k4
<g/>
Ts	ts	k0
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
astatu	astat	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgFnSc3d1
pevné	pevný	k2eAgFnSc3d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
293	#num#	k4
<g/>
Ts	ts	k0
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
14	#num#	k4
ms	ms	k?
</s>
<s>
α	α	k?
</s>
<s>
289	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
294	#num#	k4
<g/>
Ts	ts	k0
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
α	α	k?
</s>
<s>
290	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
At	At	k?
<g/>
⋏	⋏	k?
</s>
<s>
Livermorium	Livermorium	k1gNnSc1
≺	≺	k?
<g/>
Ts	ts	k0
<g/>
≻	≻	k?
Oganesson	Oganesson	k1gInSc4
</s>
<s>
Tennessin	Tennessin	k1gInSc1
(	(	kIx(
<g/>
zatím	zatím	k6eAd1
nekodifikovaný	kodifikovaný	k2eNgInSc1d1
český	český	k2eAgInSc1d1
název	název	k1gInSc1
odvozený	odvozený	k2eAgInSc1d1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
názvu	název	k1gInSc2
tennessine	tennessinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
resp.	resp.	kA
ze	z	k7c2
zprávy	zpráva	k1gFnSc2
ČTK	ČTK	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ts	ts	k0
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
transuran	transuran	k1gInSc1
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
117	#num#	k4
<g/>
,	,	kIx,
řazený	řazený	k2eAgInSc1d1
(	(	kIx(
<g/>
zatím	zatím	k6eAd1
pouze	pouze	k6eAd1
formálně	formálně	k6eAd1
<g/>
)	)	kIx)
mezi	mezi	k7c7
halogeny	halogen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
se	se	k3xPyFc4
provizorně	provizorně	k6eAd1
označoval	označovat	k5eAaImAgInS
jako	jako	k9
ununseptium	ununseptium	k1gNnSc4
(	(	kIx(
<g/>
Uus	Uus	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
připraven	připravit	k5eAaPmNgInS
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
v	v	k7c6
ruském	ruský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Dubna	duben	k1gInSc2
sloučením	sloučení	k1gNnSc7
atomových	atomový	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
izotopů	izotop	k1gInPc2
vápníku	vápník	k1gInSc2
4820	#num#	k4
Ca	ca	kA
a	a	k8xC
berkelia	berkelia	k1gFnSc1
24997	#num#	k4
Bk	Bk	k1gFnPc2
<g/>
:	:	kIx,
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4820	#num#	k4
Ca	ca	kA
+	+	kIx~
24997	#num#	k4
Bk	Bk	k1gFnSc2
→	→	k?
297117	#num#	k4
Ts	ts	k0
→	→	k?
294117	#num#	k4
Ts	ts	k0
+	+	kIx~
3	#num#	k4
10	#num#	k4
n	n	k0
</s>
<s>
Předpokládané	předpokládaný	k2eAgFnPc1d1
rozpadové	rozpadový	k2eAgFnPc1d1
řady	řada	k1gFnPc1
jader	jádro	k1gNnPc2
tennessinu	tennessina	k1gFnSc4
končí	končit	k5eAaImIp3nP
izotopy	izotop	k1gInPc1
lawrencia	lawrencium	k1gNnSc2
a	a	k8xC
vypadají	vypadat	k5eAaImIp3nP,k5eAaPmIp3nP
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Vypočtená	vypočtený	k2eAgFnSc1d1
rozpadová	rozpadový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
pro	pro	k7c4
izotopy	izotop	k1gInPc4
jader	jádro	k1gNnPc2
293	#num#	k4
<g/>
Uus	Uus	k1gMnPc2
a	a	k8xC
294	#num#	k4
Uus	Uus	k1gFnPc2
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
pro	pro	k7c4
čistou	čistý	k2eAgFnSc4d1
a	a	k8xC
užitou	užitý	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
potvrdila	potvrdit	k5eAaPmAgFnS
splnění	splnění	k1gNnSc4
kritérií	kritérion	k1gNnPc2
pro	pro	k7c4
prokázání	prokázání	k1gNnSc4
objevu	objev	k1gInSc2
nového	nový	k2eAgInSc2d1
prvku	prvek	k1gInSc2
<g/>
,	,	kIx,
uznala	uznat	k5eAaPmAgFnS
Uus	Uus	k1gFnSc1
za	za	k7c4
objevené	objevený	k2eAgInPc4d1
spolupracujícími	spolupracující	k2eAgInPc7d1
týmy	tým	k1gInPc7
vědců	vědec	k1gMnPc2
ze	z	k7c2
Spojeného	spojený	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
jaderných	jaderný	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
v	v	k7c6
Dubně	Dubna	k1gFnSc6
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lawrence	Lawrence	k1gFnSc1
Livermore	Livermor	k1gInSc5
National	National	k1gFnSc1
Laboratory	Laborator	k1gInPc1
(	(	kIx(
<g/>
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
a	a	k8xC
Národní	národní	k2eAgFnSc2d1
laboratoře	laboratoř	k1gFnSc2
Oak	Oak	k1gMnSc2
Ridge	Ridg	k1gMnSc2
(	(	kIx(
<g/>
Tennessee	Tennesse	k1gMnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
a	a	k8xC
vyzvala	vyzvat	k5eAaPmAgFnS
objevitele	objevitel	k1gMnSc4
k	k	k7c3
navržení	navržení	k1gNnSc3
konečného	konečný	k2eAgInSc2d1
názvu	název	k1gInSc2
a	a	k8xC
značky	značka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Konečným	konečný	k2eAgInSc7d1
návrhem	návrh	k1gInSc7
objevitelů	objevitel	k1gMnPc2
byl	být	k5eAaImAgInS
název	název	k1gInSc4
tennessine	tennessinout	k5eAaPmIp3nS
a	a	k8xC
značka	značka	k1gFnSc1
Ts	ts	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvek	prvek	k1gInSc1
je	být	k5eAaImIp3nS
takto	takto	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgMnS
na	na	k7c4
počest	počest	k1gFnSc4
amerického	americký	k2eAgInSc2d1
státu	stát	k1gInSc2
Tennessee	Tennesse	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
sídlí	sídlet	k5eAaImIp3nS
Národní	národní	k2eAgFnPc4d1
laboratoře	laboratoř	k1gFnPc4
Oak	Oak	k1gMnSc2
Ridge	Ridg	k1gMnSc2
<g/>
,	,	kIx,
Vanderbilt	Vanderbilt	k1gInSc4
University	universita	k1gFnSc2
a	a	k8xC
University	universita	k1gFnSc2
of	of	k?
Tennessee	Tennesse	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
připomíná	připomínat	k5eAaImIp3nS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
přínos	přínos	k1gInSc4
k	k	k7c3
výzkumu	výzkum	k1gInSc3
supertěžkých	supertěžký	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Tento	tento	k3xDgInSc1
anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
názvoslovným	názvoslovný	k2eAgNnSc7d1
doporučením	doporučení	k1gNnSc7
IUPAC	IUPAC	kA
a	a	k8xC
ctí	ctít	k5eAaImIp3nS
tradiční	tradiční	k2eAgFnSc4d1
(	(	kIx(
<g/>
anglojazyčnou	anglojazyčný	k2eAgFnSc4d1
<g/>
)	)	kIx)
příponu	přípona	k1gFnSc4
halogenů	halogen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
konečného	konečný	k2eAgNnSc2d1
pojmenování	pojmenování	k1gNnSc2
předložila	předložit	k5eAaPmAgFnS
IUPAC	IUPAC	kA
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
k	k	k7c3
veřejné	veřejný	k2eAgFnSc3d1
diskusi	diskuse	k1gFnSc3
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
schválila	schválit	k5eAaPmAgFnS
jako	jako	k9
konečné	konečný	k2eAgNnSc4d1
pojmenování	pojmenování	k1gNnSc4
a	a	k8xC
značku	značka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
V	v	k7c6
r.	r.	kA
2012	#num#	k4
objevitelé	objevitel	k1gMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
průkaznou	průkazný	k2eAgFnSc4d1
syntézu	syntéza	k1gFnSc4
2	#num#	k4
izotopů	izotop	k1gInPc2
<g/>
:	:	kIx,
293117	#num#	k4
Ts	ts	k0
a	a	k8xC
294117	#num#	k4
Ts	ts	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
293117	#num#	k4
Ts	ts	k0
bylo	být	k5eAaImAgNnS
registrováno	registrovat	k5eAaBmNgNnS
10	#num#	k4
rozpadů	rozpad	k1gInPc2
alfa	alfa	k1gNnPc2
<g/>
,	,	kIx,
u	u	k7c2
294117	#num#	k4
Ts	ts	k0
pak	pak	k6eAd1
3	#num#	k4
rozpady	rozpad	k1gInPc4
alfa	alfa	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
hlavním	hlavní	k2eAgInSc7d1
rozpadovým	rozpadový	k2eAgInSc7d1
kanálem	kanál	k1gInSc7
(	(	kIx(
<g/>
podíl	podíl	k1gInSc1
~	~	kIx~
100	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
další	další	k2eAgInPc1d1
dva	dva	k4xCgInPc1
izotopy	izotop	k1gInPc1
<g/>
;	;	kIx,
291117	#num#	k4
Ts	ts	k0
a	a	k8xC
292117	#num#	k4
Ts	ts	k0
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc4
shrnuje	shrnovat	k5eAaImIp3nS
tabulka	tabulka	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IzotopZpůsob	IzotopZpůsoba	k1gFnPc2
přeměnyPodílPoločas	přeměnyPodílPoločasa	k1gFnPc2
přeměny	přeměna	k1gFnSc2
(	(	kIx(
<g/>
ms	ms	k?
<g/>
)	)	kIx)
<g/>
Energie	energie	k1gFnSc1
přeměny	přeměna	k1gFnSc2
Eα	Eα	k1gFnSc1
(	(	kIx(
<g/>
MeV	MeV	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
291	#num#	k4
<g/>
Tsα	Tsα	k1gFnSc1
/	/	kIx~
SF	SF	kA
?	?	kIx.
</s>
<s desamb="1">
?	?	kIx.
</s>
<s desamb="1">
?	?	kIx.
</s>
<s>
292	#num#	k4
<g/>
Tsα	Tsα	k1gFnSc1
/	/	kIx~
SF	SF	kA
?	?	kIx.
</s>
<s desamb="1">
?	?	kIx.
</s>
<s desamb="1">
?	?	kIx.
</s>
<s>
293	#num#	k4
<g/>
Tsα	Tsα	k1gFnSc1
<g/>
~	~	kIx~
<g/>
100	#num#	k4
%	%	kIx~
<g/>
27	#num#	k4
+12	+12	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
10,6	10,6	k4
–	–	k?
11,14	11,14	k4
</s>
<s>
294	#num#	k4
<g/>
Tsα	Tsα	k1gFnSc1
<g/>
~	~	kIx~
<g/>
100	#num#	k4
%	%	kIx~
<g/>
50	#num#	k4
+60	+60	k4
<g/>
−	−	k?
<g/>
18	#num#	k4
10,81	10,81	k4
–	–	k?
10,97	10,97	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
dodržuje	dodržovat	k5eAaImIp3nS
tamní	tamní	k2eAgFnSc4d1
zvyklost	zvyklost	k1gFnSc4
označovat	označovat	k5eAaImF
halogeny	halogen	k1gInPc4
s	s	k7c7
příponou	přípona	k1gFnSc7
-ine	-inat	k5eAaPmIp3nS
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
chlorine	chlorin	k1gInSc5
<g/>
,	,	kIx,
iodine	iodinout	k5eAaPmIp3nS
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
čeština	čeština	k1gFnSc1
(	(	kIx(
<g/>
ani	ani	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
)	)	kIx)
nezná	neznat	k5eAaImIp3nS,k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUPAC	IUPAC	kA
výslovně	výslovně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
přípona	přípona	k1gFnSc1
-ine	-in	k1gFnSc2
není	být	k5eNaImIp3nS
pravidlem	pravidlem	k6eAd1
a	a	k8xC
mnoho	mnoho	k4c4
jazyků	jazyk	k1gInPc2
používá	používat	k5eAaImIp3nS
kratší	krátký	k2eAgInPc4d2
názvy	název	k1gInPc4
bez	bez	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Český	český	k2eAgInSc1d1
název	název	k1gInSc1
tennessin	tennessin	k2eAgInSc1d1
<g/>
,	,	kIx,
tedy	tedy	k9
s	s	k7c7
„	„	k?
<g/>
počeštěnou	počeštěný	k2eAgFnSc7d1
<g/>
“	“	k?
příponou	přípona	k1gFnSc7
-in	-in	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
přesto	přesto	k8xC
začíná	začínat	k5eAaImIp3nS
prosazovat	prosazovat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
v	v	k7c6
odborných	odborný	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
Glosář	glosář	k1gInSc1
Aldebaran	Aldebaran	k1gInSc1
<g/>
,	,	kIx,
experimentální	experimentální	k2eAgFnSc1d1
školní	školní	k2eAgFnSc1d1
výuka	výuka	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Ústavem	ústav	k1gInSc7
fyzikální	fyzikální	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
J.	J.	kA
Heyrovského	Heyrovský	k2eAgMnSc2d1
AV	AV	kA
ČR	ČR	kA
Školní	školní	k2eAgInSc4d1
rok	rok	k1gInSc4
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Naopak	naopak	k6eAd1
možná	možný	k2eAgFnSc1d1
kratší	krátký	k2eAgFnSc1d2
varianta	varianta	k1gFnSc1
tenness	tenness	k6eAd1
bez	bez	k7c2
přípony	přípona	k1gFnSc2
používaná	používaný	k2eAgFnSc1d1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
se	se	k3xPyFc4
rozrostla	rozrůst	k5eAaPmAgFnS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
nové	nový	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ÖHRSTRÖM	ÖHRSTRÖM	kA
<g/>
,	,	kIx,
Lars	Lars	k1gInSc1
<g/>
;	;	kIx,
REEDIJK	REEDIJK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Names	Names	k1gInSc1
and	and	k?
symbols	symbols	k1gInSc1
of	of	k?
the	the	k?
elements	elements	k1gInSc1
with	with	k1gMnSc1
atomic	atomic	k1gMnSc1
numbers	numbers	k1gInSc4
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
and	and	k?
118	#num#	k4
(	(	kIx(
<g/>
IUPAC	IUPAC	kA
Recommendations	Recommendations	k1gInSc4
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
2.3	2.3	k4
Note	Not	k1gInSc2
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1227	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pure	Pur	k1gInSc2
and	and	k?
Applied	Applied	k1gMnSc1
Chemistry	Chemistr	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
de	de	k?
Gruyter	Gruyter	k1gMnSc1
GmbH	GmbH	k1gMnSc1
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
88	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1227	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1365	#num#	k4
<g/>
-	-	kIx~
<g/>
3075	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.151	10.151	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
pac-	pac-	k?
<g/>
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
501	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Flerov	Flerov	k1gInSc1
Lab	Lab	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.nanowerk.com/news/newsid=11478.php	http://www.nanowerk.com/news/newsid=11478.php	k1gInSc1
<g/>
↑	↑	k?
Discovery	Discovera	k1gFnSc2
and	and	k?
Assignment	Assignment	k1gInSc1
of	of	k?
Elements	Elements	k1gInSc1
with	with	k1gMnSc1
Atomic	Atomic	k1gMnSc1
Numbers	Numbers	k1gInSc4
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUPAC	IUPAC	kA
Latest	Latest	k1gInSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
IUPAC	IUPAC	kA
News	Newsa	k1gFnPc2
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
is	is	k?
naming	naming	k1gInSc1
the	the	k?
four	four	k1gInSc1
new	new	k?
elements	elements	k6eAd1
nihonium	nihonium	k1gNnSc1
<g/>
,	,	kIx,
moscovium	moscovium	k1gNnSc1
<g/>
,	,	kIx,
tennessine	tennessinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
and	and	k?
oganesson	oganesson	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
chemické	chemický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
obohatí	obohatit	k5eAaPmIp3nP
tabulku	tabulka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
IUPAC	IUPAC	kA
Recommendations	Recommendations	k1gInSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
to	ten	k3xDgNnSc4
name	name	k6eAd1
new	new	k?
chemical	chemicat	k5eAaPmAgInS
elements	elements	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IUPAC	IUPAC	kA
News	News	k1gInSc1
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
Announces	Announces	k1gMnSc1
the	the	k?
Names	Names	k1gMnSc1
of	of	k?
the	the	k?
Elements	Elements	k1gInSc1
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
<g/>
,	,	kIx,
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
GREINER	GREINER	kA
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viewpoint	Viewpoint	k1gInSc1
<g/>
:	:	kIx,
Heavy	Heav	k1gInPc1
into	into	k6eAd1
Stability	stabilita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physics	Physics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
115	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1943	#num#	k4
<g/>
-	-	kIx~
<g/>
2879	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
Physics	Physics	k1gInSc1
<g/>
.5	.5	k4
<g/>
.115	.115	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OGANESSJAN	OGANESSJAN	kA
<g/>
,	,	kIx,
Ju	ju	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
C.	C.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Production	Production	k1gInSc1
and	and	k?
Decay	Decaa	k1gFnSc2
of	of	k?
the	the	k?
Heaviest	Heaviest	k1gFnSc1
Nuclei	Nucle	k1gFnSc2
293,294	293,294	k4
<g/>
117	#num#	k4
and	and	k?
294118	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
Letters	Lettersa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
109	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
16	#num#	k4
<g/>
,	,	kIx,
e	e	k0
<g/>
162501	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1079	#num#	k4
<g/>
-	-	kIx~
<g/>
7114	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevLett	PhysRevLett	k1gInSc1
<g/>
.109	.109	k4
<g/>
.162501	.162501	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
tennessin	tennessina	k1gFnPc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
tennessin	tennessina	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2012003556	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2012003556	#num#	k4
</s>
