<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
zbraň	zbraň	k1gFnSc1	zbraň
nebo	nebo	k8xC	nebo
též	též	k9	též
atomová	atomový	k2eAgFnSc1d1	atomová
zbraň	zbraň	k1gFnSc1	zbraň
je	být	k5eAaImIp3nS	být
zbraň	zbraň	k1gFnSc4	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
principu	princip	k1gInSc6	princip
neřízené	řízený	k2eNgFnSc2d1	neřízená
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
jader	jádro	k1gNnPc2	jádro
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
