<p>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
(	(	kIx(	(
<g/>
ET	ET	kA	ET
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
výpar	výpar	k1gInSc1	výpar
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
území	území	k1gNnSc3	území
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
celkový	celkový	k2eAgInSc1d1	celkový
výpar	výpar	k1gInSc1	výpar
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
výparu	výpar	k1gInSc2	výpar
(	(	kIx(	(
<g/>
evaporace	evaporace	k1gFnSc1	evaporace
<g/>
)	)	kIx)	)
a	a	k8xC	a
fyziologického	fyziologický	k2eAgMnSc2d1	fyziologický
(	(	kIx(	(
<g/>
transpirace	transpirace	k1gFnSc1	transpirace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evaporace	evaporace	k1gFnSc1	evaporace
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pohyb	pohyb	k1gInSc4	pohyb
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
jako	jako	k9	jako
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
a	a	k8xC	a
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
zachycená	zachycený	k2eAgFnSc1d1	zachycená
na	na	k7c4	na
vegetaci	vegetace	k1gFnSc4	vegetace
(	(	kIx(	(
<g/>
intercepce	intercepce	k1gFnSc1	intercepce
srážek	srážka	k1gFnPc2	srážka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Transpirace	transpirace	k1gFnSc1	transpirace
je	být	k5eAaImIp3nS	být
výdej	výdej	k1gInSc4	výdej
vody	voda	k1gFnSc2	voda
vegetací	vegetace	k1gFnPc2	vegetace
zejména	zejména	k9	zejména
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
související	související	k2eAgFnSc3d1	související
ztrátě	ztráta	k1gFnSc3	ztráta
vody	voda	k1gFnSc2	voda
jako	jako	k8xC	jako
výparu	výpar	k1gInSc2	výpar
z	z	k7c2	z
průduchů	průduch	k1gInPc2	průduch
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
vodního	vodní	k2eAgInSc2d1	vodní
cyklu	cyklus	k1gInSc2	cyklus
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Element	element	k1gInSc1	element
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
strom	strom	k1gInSc1	strom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
evapotranspiraci	evapotranspirace	k1gFnSc3	evapotranspirace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
evapotranspirátor	evapotranspirátor	k1gInSc1	evapotranspirátor
<g/>
.	.	kIx.	.
</s>
<s>
Referenční	referenční	k2eAgFnSc1d1	referenční
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
(	(	kIx(	(
<g/>
ET	ET	kA	ET
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nepřesně	přesně	k6eNd1	přesně
předkládána	předkládat	k5eAaImNgFnS	předkládat
jako	jako	k8xS	jako
potenciální	potenciální	k2eAgInPc1d1	potenciální
ET	ET	kA	ET
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
reprezentací	reprezentace	k1gFnSc7	reprezentace
potřeby	potřeba	k1gFnSc2	potřeba
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
míru	míra	k1gFnSc4	míra
ET	ET	kA	ET
krátkých	krátký	k2eAgFnPc2d1	krátká
zelených	zelená	k1gFnPc2	zelená
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
tráva	tráva	k1gFnSc1	tráva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
kompletně	kompletně	k6eAd1	kompletně
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ji	on	k3xPp3gFnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
udržují	udržovat	k5eAaImIp3nP	udržovat
adekvátní	adekvátní	k2eAgInSc4d1	adekvátní
vodní	vodní	k2eAgInSc4d1	vodní
status	status	k1gInSc4	status
v	v	k7c6	v
půdním	půdní	k2eAgInSc6d1	půdní
profilu	profil	k1gInSc6	profil
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
od	od	k7c2	od
energie	energie	k1gFnSc2	energie
dostupné	dostupný	k2eAgFnSc2d1	dostupná
pro	pro	k7c4	pro
evaporaci	evaporace	k1gFnSc4	evaporace
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
dostupného	dostupný	k2eAgInSc2d1	dostupný
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
vodní	vodní	k2eAgInSc1d1	vodní
výpar	výpar	k1gInSc1	výpar
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
části	část	k1gFnSc2	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
dostatku	dostatek	k1gInSc2	dostatek
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
aktuální	aktuální	k2eAgFnSc1d1	aktuální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
rovná	rovnat	k5eAaImIp3nS	rovnat
referenční	referenční	k2eAgFnSc4d1	referenční
evapotranspiraci	evapotranspirace	k1gFnSc4	evapotranspirace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
cyklus	cyklus	k1gInSc1	cyklus
==	==	k?	==
</s>
</p>
<p>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
ztráta	ztráta	k1gFnSc1	ztráta
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
podstatně	podstatně	k6eAd1	podstatně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
typy	typ	k1gInPc1	typ
vegetace	vegetace	k1gFnSc2	vegetace
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
s	s	k7c7	s
hlubokými	hluboký	k2eAgInPc7d1	hluboký
kořeny	kořen	k1gInPc7	kořen
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vyšší	vysoký	k2eAgFnSc3d2	vyšší
a	a	k8xC	a
nepřetržité	přetržitý	k2eNgFnSc3d1	nepřetržitá
transpiraci	transpirace	k1gFnSc3	transpirace
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
listů	list	k1gInPc2	list
transpiruje	transpirovat	k5eAaImIp3nS	transpirovat
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Byliny	bylina	k1gFnPc1	bylina
obecně	obecně	k6eAd1	obecně
transpirují	transpirovat	k5eAaImIp3nP	transpirovat
méně	málo	k6eAd2	málo
vody	voda	k1gFnSc2	voda
než	než	k8xS	než
dřeviny	dřevina	k1gFnSc2	dřevina
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInSc3	jejich
menšímu	malý	k2eAgInSc3d2	menší
olistění	olistěný	k2eAgMnPc1d1	olistěný
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
tíhnou	tíhnout	k5eAaImIp3nP	tíhnout
k	k	k7c3	k
vyšší	vysoký	k2eAgFnSc3d2	vyšší
míře	míra	k1gFnSc3	míra
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
než	než	k8xS	než
listnaté	listnatý	k2eAgInPc1d1	listnatý
opadavé	opadavý	k2eAgInPc1d1	opadavý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jejich	jejich	k3xOp3gInSc2	jejich
vegetačního	vegetační	k2eAgInSc2d1	vegetační
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
zdokonalenému	zdokonalený	k2eAgNnSc3d1	zdokonalené
zachytávání	zachytávání	k1gNnSc3	zachytávání
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
vypařování	vypařování	k1gNnSc1	vypařování
jehličím	jehličí	k1gNnSc7	jehličí
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
evapotranspiraci	evapotranspirace	k1gFnSc4	evapotranspirace
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
fázi	fáze	k1gFnSc4	fáze
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
růstu	růst	k1gInSc2	růst
nebo	nebo	k8xC	nebo
míru	mír	k1gInSc2	mír
vyspělosti	vyspělost	k1gFnSc2	vyspělost
<g/>
,	,	kIx,	,
procentuální	procentuální	k2eAgNnSc1d1	procentuální
zastoupení	zastoupení	k1gNnSc1	zastoupení
půdního	půdní	k2eAgInSc2d1	půdní
pokryvu	pokryv	k1gInSc2	pokryv
<g/>
,	,	kIx,	,
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
humusu	humus	k1gInSc2	humus
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Izotopní	Izotopní	k2eAgNnSc1d1	Izotopní
měření	měření	k1gNnSc1	měření
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
transpirace	transpirace	k1gFnSc1	transpirace
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
redukují	redukovat	k5eAaBmIp3nP	redukovat
vodní	vodní	k2eAgFnPc1d1	vodní
zásoby	zásoba	k1gFnPc1	zásoba
až	až	k9	až
na	na	k7c4	na
jedinečné	jedinečný	k2eAgInPc4d1	jedinečný
ekosystémy	ekosystém	k1gInPc4	ekosystém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
mlžné	mlžný	k2eAgInPc1d1	mlžný
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
v	v	k7c6	v
mlžných	mlžný	k2eAgInPc6d1	mlžný
lesech	les	k1gInPc6	les
podporují	podporovat	k5eAaImIp3nP	podporovat
shromažďování	shromažďování	k1gNnSc4	shromažďování
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mlhy	mlha	k1gFnSc2	mlha
nebo	nebo	k8xC	nebo
nízkých	nízký	k2eAgFnPc2d1	nízká
mračen	mračna	k1gFnPc2	mračna
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
následně	následně	k6eAd1	následně
padá	padat	k5eAaImIp3nS	padat
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stromy	strom	k1gInPc1	strom
stále	stále	k6eAd1	stále
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
evapotranspiraci	evapotranspirace	k1gFnSc3	evapotranspirace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
sbírají	sbírat	k5eAaImIp3nP	sbírat
více	hodně	k6eAd2	hodně
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vypaří	vypařit	k5eAaPmIp3nP	vypařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nezavlažovaných	zavlažovaný	k2eNgFnPc6d1	zavlažovaný
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
aktuální	aktuální	k2eAgFnSc1d1	aktuální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
obvykle	obvykle	k6eAd1	obvykle
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
půda	půda	k1gFnSc1	půda
má	mít	k5eAaImIp3nS	mít
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
schopnosti	schopnost	k1gFnSc3	schopnost
udržet	udržet	k5eAaPmF	udržet
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zadržené	zadržený	k2eAgFnSc2d1	zadržená
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
ztratí	ztratit	k5eAaPmIp3nS	ztratit
při	při	k7c6	při
filtrování	filtrování	k1gNnSc6	filtrování
nebo	nebo	k8xC	nebo
povrchovým	povrchový	k2eAgInSc7d1	povrchový
odtokem	odtok	k1gInSc7	odtok
dešťové	dešťový	k2eAgFnSc2d1	dešťová
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hladinou	hladina	k1gFnSc7	hladina
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proces	proces	k1gInSc1	proces
kapilárního	kapilární	k2eAgNnSc2d1	kapilární
vzlínání	vzlínání	k1gNnSc2	vzlínání
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
vystoupání	vystoupání	k1gNnSc4	vystoupání
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
přes	přes	k7c4	přes
půdu	půda	k1gFnSc4	půda
až	až	k9	až
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgInSc4d2	vyšší
než	než	k8xS	než
skutečný	skutečný	k2eAgInSc4d1	skutečný
úhrn	úhrn	k1gInSc4	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
půda	půda	k1gFnSc1	půda
vyschne	vyschnout	k5eAaPmIp3nS	vyschnout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nebude	být	k5eNaImBp3nS	být
zavlažována	zavlažován	k2eAgFnSc1d1	zavlažována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vypařit	vypařit	k5eAaPmF	vypařit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
schopny	schopen	k2eAgFnPc1d1	schopna
transpirovat	transpirovat	k5eAaImF	transpirovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odhad	odhad	k1gInSc1	odhad
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
==	==	k?	==
</s>
</p>
<p>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
nebo	nebo	k8xC	nebo
vypočítána	vypočítat	k5eAaPmNgFnS	vypočítat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
několika	několik	k4yIc2	několik
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepřímé	přímý	k2eNgFnPc1d1	nepřímá
metody	metoda	k1gFnPc1	metoda
===	===	k?	===
</s>
</p>
<p>
<s>
Údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
výparoměrů	výparoměr	k1gInPc2	výparoměr
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
výparu	výpar	k1gInSc2	výpar
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
transpirace	transpirace	k1gFnSc1	transpirace
a	a	k8xC	a
výpar	výpar	k1gInSc1	výpar
dešťové	dešťový	k2eAgFnSc2d1	dešťová
vody	voda	k1gFnSc2	voda
zachycené	zachycený	k2eAgFnSc2d1	zachycená
vegetací	vegetace	k1gFnSc7	vegetace
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
nevypočítá	vypočítat	k5eNaPmIp3nS	vypočítat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
obecné	obecný	k2eAgInPc1d1	obecný
přístupy	přístup	k1gInPc1	přístup
v	v	k7c6	v
nepřímém	přímý	k2eNgNnSc6d1	nepřímé
měření	měření	k1gNnSc6	měření
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vodní	vodní	k2eAgFnPc1d1	vodní
bilance	bilance	k1gFnPc1	bilance
povodí	povodí	k1gNnSc2	povodí
====	====	k?	====
</s>
</p>
<p>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vypočítána	vypočítat	k5eAaPmNgFnS	vypočítat
rovnicí	rovnice	k1gFnSc7	rovnice
vodní	vodní	k2eAgFnSc2d1	vodní
bilance	bilance	k1gFnSc2	bilance
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rovnice	rovnice	k1gFnSc1	rovnice
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
změnu	změna	k1gFnSc4	změna
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
vstupy	vstup	k1gInPc7	vstup
a	a	k8xC	a
výstupy	výstup	k1gInPc7	výstup
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
P-ET-Q-D	P-ET-Q-D	k1gMnSc1	P-ET-Q-D
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Vstupy	vstup	k1gInPc1	vstup
znamenají	znamenat	k5eAaImIp3nP	znamenat
srážky	srážka	k1gFnPc1	srážka
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
a	a	k8xC	a
výstupy	výstup	k1gInPc1	výstup
evapotranspiraci	evapotranspirace	k1gFnSc4	evapotranspirace
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vypočítávána	vypočítáván	k2eAgFnSc1d1	vypočítávána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průtok	průtok	k1gInSc1	průtok
povodí	povodí	k1gNnSc2	povodí
(	(	kIx(	(
<g/>
Q	Q	kA	Q
<g/>
)	)	kIx)	)
a	a	k8xC	a
doplňování	doplňování	k1gNnSc1	doplňování
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
údaje	údaj	k1gInPc1	údaj
změna	změna	k1gFnSc1	změna
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
uložené	uložený	k2eAgFnSc2d1	uložená
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
<g/>
,	,	kIx,	,
průtok	průtok	k1gInSc4	průtok
povodí	povodí	k1gNnSc2	povodí
a	a	k8xC	a
doplňování	doplňování	k1gNnSc2	doplňování
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
chybějící	chybějící	k2eAgInSc1d1	chybějící
údaj	údaj	k1gInSc1	údaj
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vypočítán	vypočítat	k5eAaPmNgMnS	vypočítat
přeskupením	přeskupení	k1gNnSc7	přeskupení
rovnice	rovnice	k1gFnSc2	rovnice
na	na	k7c6	na
následující	následující	k2eAgFnSc6d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ET	ET	kA	ET
<g/>
=	=	kIx~	=
<g/>
P-	P-	k1gMnSc1	P-
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
S-Q-D	S-Q-D	k1gMnSc1	S-Q-D
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Evapotranspirace	Evapotranspirace	k1gFnPc4	Evapotranspirace
městské	městský	k2eAgFnSc2d1	městská
vegetace	vegetace	k1gFnSc2	vegetace
====	====	k?	====
</s>
</p>
<p>
<s>
Obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
náročnosti	náročnost	k1gFnSc2	náročnost
městské	městský	k2eAgFnSc2d1	městská
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
městských	městský	k2eAgFnPc2d1	městská
zelených	zelený	k2eAgFnPc2d1	zelená
ploch	plocha	k1gFnPc2	plocha
na	na	k7c4	na
dostatek	dostatek	k1gInSc4	dostatek
vody	voda	k1gFnSc2	voda
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
odhadu	odhad	k1gInSc2	odhad
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
(	(	kIx(	(
<g/>
ET	ET	kA	ET
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
náročnosti	náročnost	k1gFnSc3	náročnost
městské	městský	k2eAgFnSc2d1	městská
vegetace	vegetace	k1gFnSc2	vegetace
na	na	k7c4	na
dostatek	dostatek	k1gInSc4	dostatek
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zvážit	zvážit	k5eAaPmF	zvážit
heterogenitu	heterogenita	k1gFnSc4	heterogenita
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
provedený	provedený	k2eAgInSc1d1	provedený
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
využil	využít	k5eAaPmAgMnS	využít
dvou	dva	k4xCgInPc2	dva
praktických	praktický	k2eAgInPc2d1	praktický
přístupů	přístup	k1gInPc2	přístup
v	v	k7c6	v
odhadování	odhadování	k1gNnSc6	odhadování
ET	ET	kA	ET
a	a	k8xC	a
porovnával	porovnávat	k5eAaImAgMnS	porovnávat
je	být	k5eAaImIp3nS	být
pod	pod	k7c4	pod
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
s	s	k7c7	s
detailní	detailní	k2eAgFnSc7d1	detailní
analýzou	analýza	k1gFnSc7	analýza
vodní	vodní	k2eAgFnSc2d1	vodní
bilance	bilance	k1gFnSc2	bilance
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
přístupů	přístup	k1gInPc2	přístup
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
klasifikace	klasifikace	k1gFnSc2	klasifikace
krajinotvorných	krajinotvorný	k2eAgFnPc2d1	krajinotvorná
rostlin	rostlina	k1gFnPc2	rostlina
podle	podle	k7c2	podle
využití	využití	k1gNnSc2	využití
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
WUCOLS	WUCOLS	kA	WUCOLS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
názoru	názor	k1gInSc6	názor
expertů	expert	k1gMnPc2	expert
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgFnPc1d1	různá
skupiny	skupina	k1gFnPc1	skupina
krajinotvorných	krajinotvorný	k2eAgFnPc2d1	krajinotvorná
rostlin	rostlina	k1gFnPc2	rostlina
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
různé	různý	k2eAgFnPc1d1	různá
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
dálkové	dálkový	k2eAgNnSc4d1	dálkové
snímání	snímání	k1gNnSc4	snímání
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
rozšířeném	rozšířený	k2eAgInSc6d1	rozšířený
vegetačním	vegetační	k2eAgInSc6d1	vegetační
indexu	index	k1gInSc6	index
(	(	kIx(	(
<g/>
EVI	EVI	kA	EVI
<g/>
)	)	kIx)	)
získaného	získaný	k2eAgInSc2d1	získaný
ze	z	k7c2	z
snímačů	snímač	k1gInPc2	snímač
satelitu	satelit	k1gInSc2	satelit
Terra	Terr	k1gMnSc2	Terr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
situaci	situace	k1gFnSc4	situace
snímanou	snímaný	k2eAgFnSc4d1	snímaná
spektroradiometrem	spektroradiometr	k1gInSc7	spektroradiometr
MODIS	MODIS	kA	MODIS
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
metody	metoda	k1gFnPc1	metoda
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c4	o
evapotranspiraci	evapotranspirace	k1gFnSc4	evapotranspirace
počítané	počítaný	k2eAgNnSc1d1	počítané
z	z	k7c2	z
meteorologických	meteorologický	k2eAgNnPc2d1	meteorologické
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hydrometeorologické	hydrometeorologický	k2eAgFnPc1d1	hydrometeorologická
rovnice	rovnice	k1gFnPc1	rovnice
====	====	k?	====
</s>
</p>
<p>
<s>
Nejobecnější	obecní	k2eAgFnSc7d3	obecní
a	a	k8xC	a
nejrozšířeněji	rozšířeně	k6eAd3	rozšířeně
používaná	používaný	k2eAgFnSc1d1	používaná
rovnice	rovnice	k1gFnSc1	rovnice
pro	pro	k7c4	pro
vypočítání	vypočítání	k1gNnPc4	vypočítání
referenční	referenční	k2eAgNnPc4d1	referenční
ET	ET	kA	ET
je	být	k5eAaImIp3nS	být
Penmanova	Penmanův	k2eAgFnSc1d1	Penmanův
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Penman-Monteithovu	Penman-Monteithův	k2eAgFnSc4d1	Penman-Monteithův
variaci	variace	k1gFnSc4	variace
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
FAO	FAO	kA	FAO
a	a	k8xC	a
Americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
civilního	civilní	k2eAgNnSc2d1	civilní
inženýrství	inženýrství	k1gNnSc2	inženýrství
.	.	kIx.	.
</s>
<s>
Jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
Blaney-Criddlova	Blaney-Criddlův	k2eAgFnSc1d1	Blaney-Criddlův
rovnice	rovnice	k1gFnSc1	rovnice
byla	být	k5eAaImAgFnS	být
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
populární	populární	k2eAgFnSc2d1	populární
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
v	v	k7c6	v
regionech	region	k1gInPc6	region
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
možnosti	možnost	k1gFnPc1	možnost
tvoří	tvořit	k5eAaImIp3nP	tvořit
Hargreavesova	Hargreavesův	k2eAgFnSc1d1	Hargreavesův
rovnice	rovnice	k1gFnSc1	rovnice
nebo	nebo	k8xC	nebo
Makkinkova	Makkinkův	k2eAgFnSc1d1	Makkinkův
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
přesně	přesně	k6eAd1	přesně
nastavená	nastavený	k2eAgFnSc1d1	nastavená
na	na	k7c4	na
specifickou	specifický	k2eAgFnSc4d1	specifická
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
převedení	převedení	k1gNnSc4	převedení
referenční	referenční	k2eAgFnSc2d1	referenční
ET	ET	kA	ET
na	na	k7c6	na
aktuální	aktuální	k2eAgFnSc6d1	aktuální
evapotranspiraci	evapotranspirace	k1gFnSc6	evapotranspirace
plodin	plodina	k1gFnPc2	plodina
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
použít	použít	k5eAaPmF	použít
plodinový	plodinový	k2eAgInSc1d1	plodinový
koeficient	koeficient	k1gInSc1	koeficient
a	a	k8xC	a
koeficient	koeficient	k1gInSc1	koeficient
stresu	stres	k1gInSc2	stres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Energetická	energetický	k2eAgFnSc1d1	energetická
bilance	bilance	k1gFnSc1	bilance
====	====	k?	====
</s>
</p>
<p>
<s>
Třetím	třetí	k4xOgInSc7	třetí
postupem	postup	k1gInSc7	postup
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vypočítat	vypočítat	k5eAaPmF	vypočítat
aktuální	aktuální	k2eAgFnSc4d1	aktuální
evapotranspiraci	evapotranspirace	k1gFnSc4	evapotranspirace
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
energetické	energetický	k2eAgFnSc2d1	energetická
bilance	bilance	k1gFnSc2	bilance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
λ	λ	k?	λ
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
pro	pro	k7c4	pro
přeměnu	přeměna	k1gFnSc4	přeměna
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
kapalného	kapalný	k2eAgMnSc2d1	kapalný
do	do	k7c2	do
plynného	plynný	k2eAgNnSc2d1	plynné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
,	,	kIx,	,
Rn	Rn	k1gFnSc1	Rn
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
radiační	radiační	k2eAgFnSc4d1	radiační
bilanci	bilance	k1gFnSc4	bilance
<g/>
,	,	kIx,	,
G	G	kA	G
je	být	k5eAaImIp3nS	být
půdní	půdní	k2eAgInSc1d1	půdní
tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
a	a	k8xC	a
H	H	kA	H
značí	značit	k5eAaImIp3nS	značit
turbulentní	turbulentní	k2eAgInSc4d1	turbulentní
tok	tok	k1gInSc4	tok
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Použitím	použití	k1gNnSc7	použití
přístrojů	přístroj	k1gInPc2	přístroj
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
scintilometr	scintilometr	k1gInSc4	scintilometr
<g/>
,	,	kIx,	,
měřením	měření	k1gNnSc7	měření
půdního	půdní	k2eAgInSc2d1	půdní
tepelného	tepelný	k2eAgInSc2d1	tepelný
toku	tok	k1gInSc2	tok
nebo	nebo	k8xC	nebo
měřením	měření	k1gNnSc7	měření
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
radiometry	radiometr	k1gInPc7	radiometr
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
energetické	energetický	k2eAgFnSc2d1	energetická
bilance	bilance	k1gFnSc2	bilance
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gNnPc2	on
určit	určit	k5eAaPmF	určit
energii	energie	k1gFnSc4	energie
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
pro	pro	k7c4	pro
aktuální	aktuální	k2eAgFnSc4d1	aktuální
evapotranspiraci	evapotranspirace	k1gFnSc4	evapotranspirace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEBAL	SEBAL	kA	SEBAL
a	a	k8xC	a
METRIC	METRIC	kA	METRIC
algoritmy	algoritmus	k1gInPc1	algoritmus
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
energetickou	energetický	k2eAgFnSc4d1	energetická
bilanci	bilance	k1gFnSc4	bilance
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
použitím	použití	k1gNnSc7	použití
satelitního	satelitní	k2eAgNnSc2d1	satelitní
snímkování	snímkování	k1gNnSc2	snímkování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vypočítání	vypočítání	k1gNnSc1	vypočítání
aktuální	aktuální	k2eAgFnSc2d1	aktuální
i	i	k8xC	i
potenciální	potenciální	k2eAgFnSc2d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
pixel	pixel	k1gInSc4	pixel
za	za	k7c7	za
pixelem	pixel	k1gInSc7	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
indikátorem	indikátor	k1gInSc7	indikátor
pro	pro	k7c4	pro
vodohospodářský	vodohospodářský	k2eAgInSc4d1	vodohospodářský
management	management	k1gInSc4	management
a	a	k8xC	a
efektivitu	efektivita	k1gFnSc4	efektivita
zavlažování	zavlažování	k1gNnSc2	zavlažování
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmy	algoritmus	k1gInPc1	algoritmus
SEBAL	SEBAL	kA	SEBAL
a	a	k8xC	a
METRIC	METRIC	kA	METRIC
mohou	moct	k5eAaImIp3nP	moct
mapovat	mapovat	k5eAaImF	mapovat
tyto	tento	k3xDgInPc4	tento
klíčové	klíčový	k2eAgInPc4d1	klíčový
indikátory	indikátor	k1gInPc4	indikátor
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
po	po	k7c4	po
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
týdny	týden	k1gInPc4	týden
nebo	nebo	k8xC	nebo
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Experimentální	experimentální	k2eAgFnPc4d1	experimentální
metody	metoda	k1gFnPc4	metoda
měření	měření	k1gNnSc2	měření
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
===	===	k?	===
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
metoda	metoda	k1gFnSc1	metoda
měření	měření	k1gNnSc2	měření
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
vážícím	vážící	k2eAgInSc7d1	vážící
lysimetrem	lysimetr	k1gInSc7	lysimetr
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
půdního	půdní	k2eAgInSc2d1	půdní
sloupce	sloupec	k1gInSc2	sloupec
je	být	k5eAaImIp3nS	být
měřena	měřit	k5eAaImNgFnS	měřit
průběžně	průběžně	k6eAd1	průběžně
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
půdní	půdní	k2eAgFnSc6d1	půdní
zásobě	zásoba	k1gFnSc6	zásoba
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
ze	z	k7c2	z
změny	změna	k1gFnSc2	změna
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
váhy	váha	k1gFnSc2	váha
je	být	k5eAaImIp3nS	být
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
jednotky	jednotka	k1gFnPc4	jednotka
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
plochu	plocha	k1gFnSc4	plocha
vážícího	vážící	k2eAgInSc2d1	vážící
lysimetru	lysimetr	k1gInSc2	lysimetr
a	a	k8xC	a
jednotku	jednotka	k1gFnSc4	jednotka
váhy	váha	k1gFnSc2	váha
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Evapotranspirace	Evapotranspirace	k1gFnSc1	Evapotranspirace
je	být	k5eAaImIp3nS	být
vypočítána	vypočítat	k5eAaPmNgFnS	vypočítat
jako	jako	k9	jako
změna	změna	k1gFnSc1	změna
váhy	váha	k1gFnSc2	váha
plus	plus	k6eAd1	plus
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
mínus	mínus	k1gInSc1	mínus
perkolace	perkolace	k1gFnSc2	perkolace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dálkové	dálkový	k2eAgNnSc4d1	dálkové
snímání	snímání	k1gNnSc4	snímání
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
odhadování	odhadování	k1gNnSc1	odhadování
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
díky	díky	k7c3	díky
dálkovému	dálkový	k2eAgNnSc3d1	dálkové
snímání	snímání	k1gNnSc3	snímání
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
agronomie	agronomie	k1gFnSc2	agronomie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vyčíslení	vyčíslení	k1gNnSc4	vyčíslení
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
ze	z	k7c2	z
smíšeného	smíšený	k2eAgNnSc2d1	smíšené
rostlinného	rostlinný	k2eAgNnSc2d1	rostlinné
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
městských	městský	k2eAgFnPc2d1	městská
zelených	zelený	k2eAgFnPc2d1	zelená
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
výzvou	výzva	k1gFnSc7	výzva
kvůli	kvůli	k7c3	kvůli
heterogenitě	heterogenita	k1gFnSc3	heterogenita
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
zápoje	zápoj	k1gInSc2	zápoj
vegetace	vegetace	k1gFnSc2	vegetace
a	a	k8xC	a
mikroklimatům	mikroklima	k1gNnPc3	mikroklima
a	a	k8xC	a
protože	protože	k8xS	protože
postupy	postup	k1gInPc1	postup
jsou	být	k5eAaImIp3nP	být
nákladné	nákladný	k2eAgInPc1d1	nákladný
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
přístupy	přístup	k1gInPc1	přístup
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
dálkovém	dálkový	k2eAgNnSc6d1	dálkové
snímání	snímání	k1gNnSc6	snímání
pro	pro	k7c4	pro
odhad	odhad	k1gInSc4	odhad
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc4d1	různá
výhody	výhoda	k1gFnPc4	výhoda
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vířivá	vířivý	k2eAgFnSc1d1	vířivá
kovariance	kovariance	k1gFnSc1	kovariance
===	===	k?	===
</s>
</p>
<p>
<s>
Nejpřímější	přímý	k2eAgFnSc7d3	nejpřímější
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
je	být	k5eAaImIp3nS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
techniky	technika	k1gFnSc2	technika
vířivé	vířivý	k2eAgFnSc2d1	vířivá
kovariance	kovariance	k1gFnSc2	kovariance
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
výkyvy	výkyv	k1gInPc4	výkyv
hodnot	hodnota	k1gFnPc2	hodnota
vertikální	vertikální	k2eAgFnSc2d1	vertikální
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
korelují	korelovat	k5eAaImIp3nP	korelovat
s	s	k7c7	s
výkyvy	výkyv	k1gInPc7	výkyv
měrné	měrný	k2eAgFnSc2d1	měrná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
přímo	přímo	k6eAd1	přímo
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
přenos	přenos	k1gInSc1	přenos
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
(	(	kIx(	(
<g/>
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
<g/>
)	)	kIx)	)
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
zápoje	zápoj	k1gInPc1	zápoj
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
)	)	kIx)	)
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vegetace	vegetace	k1gFnPc4	vegetace
městské	městský	k2eAgFnSc2d1	městská
krajiny	krajina	k1gFnSc2	krajina
===	===	k?	===
</s>
</p>
<p>
<s>
Metody	metoda	k1gFnPc4	metoda
měření	měření	k1gNnSc2	měření
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
městskému	městský	k2eAgNnSc3d1	Městské
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
nároky	nárok	k1gInPc4	nárok
vegetace	vegetace	k1gFnSc2	vegetace
městské	městský	k2eAgFnSc2d1	městská
krajiny	krajina	k1gFnSc2	krajina
na	na	k7c4	na
dostupnost	dostupnost	k1gFnSc4	dostupnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potenciální	potenciální	k2eAgFnSc2d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
==	==	k?	==
</s>
</p>
<p>
<s>
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
(	(	kIx(	(
<g/>
PET	PET	kA	PET
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
vypařit	vypařit	k5eAaPmF	vypařit
a	a	k8xC	a
transpirovat	transpirovat	k5eAaImF	transpirovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
dostupné	dostupný	k2eAgNnSc4d1	dostupné
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
požadavek	požadavek	k1gInSc1	požadavek
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
energii	energie	k1gFnSc4	energie
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
pro	pro	k7c4	pro
výpar	výpar	k1gInSc4	výpar
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
nižší	nízký	k2eAgFnSc2d2	nižší
atmosféry	atmosféra	k1gFnSc2	atmosféra
přenášet	přenášet	k5eAaImF	přenášet
vypařenou	vypařený	k2eAgFnSc4d1	vypařená
vlhkost	vlhkost	k1gFnSc4	vlhkost
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vyšších	vysoký	k2eAgFnPc2d2	vyšší
hodnot	hodnota	k1gFnPc2	hodnota
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
při	při	k7c6	při
méně	málo	k6eAd2	málo
oblačných	oblačný	k2eAgInPc6d1	oblačný
dnech	den	k1gInPc6	den
a	a	k8xC	a
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyšší	vysoký	k2eAgNnSc4d2	vyšší
množství	množství	k1gNnSc4	množství
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc1	energie
pro	pro	k7c4	pro
výpar	výpar	k1gInSc4	výpar
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
je	být	k5eAaImIp3nS	být
také	také	k9	také
vyšší	vysoký	k2eAgMnSc1d2	vyšší
během	během	k7c2	během
větrných	větrný	k2eAgInPc2d1	větrný
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vypařená	vypařený	k2eAgFnSc1d1	vypařená
vlhkost	vlhkost	k1gFnSc1	vlhkost
je	být	k5eAaImIp3nS	být
rychleji	rychle	k6eAd2	rychle
odnášena	odnášet	k5eAaImNgFnS	odnášet
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
výšky	výška	k1gFnSc2	výška
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
roční	roční	k2eAgInSc4d1	roční
průběh	průběh	k1gInSc4	průběh
lze	lze	k6eAd1	lze
znázornit	znázornit	k5eAaPmF	znázornit
graficky	graficky	k6eAd1	graficky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
měřena	měřit	k5eAaImNgFnS	měřit
nepřímo	přímo	k6eNd1	přímo
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
klimatických	klimatický	k2eAgInPc2d1	klimatický
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
typu	typ	k1gInSc6	typ
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
typu	typ	k1gInSc2	typ
vegetace	vegetace	k1gFnSc2	vegetace
a	a	k8xC	a
při	při	k7c6	při
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
vegetačního	vegetační	k2eAgInSc2d1	vegetační
krytu	kryt	k1gInSc2	kryt
i	i	k9	i
na	na	k7c6	na
typu	typ	k1gInSc6	typ
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
potenciální	potenciální	k2eAgFnSc2d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc2	evapotranspirace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
počítána	počítat	k5eAaImNgFnS	počítat
na	na	k7c6	na
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
klimatické	klimatický	k2eAgFnSc6d1	klimatická
stanici	stanice	k1gFnSc6	stanice
na	na	k7c6	na
referenčním	referenční	k2eAgInSc6d1	referenční
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
konvenčně	konvenčně	k6eAd1	konvenčně
na	na	k7c6	na
krátce	krátce	k6eAd1	krátce
střiženém	střižený	k2eAgInSc6d1	střižený
trávníku	trávník	k1gInSc6	trávník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
referenční	referenční	k2eAgFnSc1d1	referenční
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
potenciální	potenciální	k2eAgFnSc4d1	potenciální
evapotranspiraci	evapotranspirace	k1gFnSc4	evapotranspirace
vynásobením	vynásobení	k1gNnSc7	vynásobení
koeficientem	koeficient	k1gInSc7	koeficient
příslušného	příslušný	k2eAgInSc2d1	příslušný
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
koeficient	koeficient	k1gInSc1	koeficient
nazývá	nazývat	k5eAaImIp3nS	nazývat
plodinový	plodinový	k2eAgInSc1d1	plodinový
koeficient	koeficient	k1gInSc1	koeficient
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
potenciální	potenciální	k2eAgFnSc7d1	potenciální
evapotranspirací	evapotranspirace	k1gFnSc7	evapotranspirace
a	a	k8xC	a
úhrnem	úhrn	k1gInSc7	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
plánování	plánování	k1gNnSc6	plánování
zavlažování	zavlažování	k1gNnSc2	zavlažování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
potenciální	potenciální	k2eAgFnSc1d1	potenciální
evapotranspirace	evapotranspirace	k1gFnSc1	evapotranspirace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
srovnávána	srovnávat	k5eAaImNgFnS	srovnávat
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
ročním	roční	k2eAgInSc7d1	roční
úhrnem	úhrn	k1gInSc7	úhrn
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
PET	PET	kA	PET
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
index	index	k1gInSc1	index
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Evapotranspiration	Evapotranspiration	k1gInSc1	Evapotranspiration
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
