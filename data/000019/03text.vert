<s>
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
hudební	hudební	k2eAgFnSc7d1	hudební
kariérou	kariéra	k1gFnSc7	kariéra
už	už	k6eAd1	už
v	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
na	na	k7c6	na
folkovém	folkový	k2eAgInSc6d1	folkový
festivalu	festival	k1gInSc6	festival
s	s	k7c7	s
básní	báseň	k1gFnSc7	báseň
Adolfa	Adolf	k1gMnSc2	Adolf
Heyduka	Heyduk	k1gMnSc2	Heyduk
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sama	sám	k3xTgFnSc1	sám
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
legendárního	legendární	k2eAgInSc2d1	legendární
pěveckého	pěvecký	k2eAgInSc2d1	pěvecký
souboru	soubor	k1gInSc2	soubor
Bambini	Bambin	k2eAgMnPc1d1	Bambin
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
byla	být	k5eAaImAgFnS	být
frontmankou	frontmanka	k1gFnSc7	frontmanka
pražské	pražský	k2eAgFnSc2d1	Pražská
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Sluníčko	sluníčko	k1gNnSc1	sluníčko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
vydala	vydat	k5eAaPmAgFnS	vydat
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
hudební	hudební	k2eAgFnSc4d1	hudební
soutěž	soutěž	k1gFnSc4	soutěž
Marlboro	Marlboro	k1gNnSc2	Marlboro
Rock	rock	k1gInSc1	rock
In	In	k1gFnSc4	In
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
také	také	k9	také
zahájila	zahájit	k5eAaPmAgFnS	zahájit
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Open	Open	k1gMnSc1	Open
Air	Air	k1gMnSc1	Air
Gampell	Gampell	k1gMnSc1	Gampell
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nominovala	nominovat	k5eAaBmAgFnS	nominovat
Lenku	Lenka	k1gFnSc4	Lenka
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
"	"	kIx"	"
<g/>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Lucie	Lucie	k1gFnPc1	Lucie
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gMnSc7	jehož
frontmanem	frontman	k1gMnSc7	frontman
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
)	)	kIx)	)
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ze	z	k7c2	z
Sluníčka	sluníčko	k1gNnSc2	sluníčko
založila	založit	k5eAaPmAgFnS	založit
skupinu	skupina	k1gFnSc4	skupina
Pusa	pusa	k1gFnSc1	pusa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
skladba	skladba	k1gFnSc1	skladba
Muka	muka	k1gFnSc1	muka
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
čtenáři	čtenář	k1gMnSc3	čtenář
hudebního	hudební	k2eAgInSc2d1	hudební
časopisu	časopis	k1gInSc2	časopis
Report	report	k1gInSc4	report
opakovaně	opakovaně	k6eAd1	opakovaně
volena	volen	k2eAgFnSc1d1	volena
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
se	se	k3xPyFc4	se
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
začala	začít	k5eAaPmAgFnS	začít
také	také	k9	také
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
folk-rockovou	folkockův	k2eAgFnSc7d1	folk-rockův
skupinou	skupina	k1gFnSc7	skupina
Čechomor	Čechomora	k1gFnPc2	Čechomora
<g/>
.	.	kIx.	.
</s>
<s>
Hostuje	hostovat	k5eAaImIp3nS	hostovat
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
přelomovém	přelomový	k2eAgNnSc6d1	přelomové
albu	album	k1gNnSc6	album
Proměny	proměna	k1gFnSc2	proměna
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
produkoval	produkovat	k5eAaImAgMnS	produkovat
skladatel	skladatel	k1gMnSc1	skladatel
Jazz	jazz	k1gInSc4	jazz
Coleman	Coleman	k1gMnSc1	Coleman
a	a	k8xC	a
který	který	k3yRgMnSc1	který
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mluvil	mluvit	k5eAaImAgMnS	mluvit
jako	jako	k8xS	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
velkém	velký	k2eAgInSc6d1	velký
talentu	talent	k1gInSc6	talent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Duet	duet	k1gInSc1	duet
Dusilové	Dusilová	k1gFnSc2	Dusilová
a	a	k8xC	a
Franty	Franta	k1gMnSc2	Franta
Černého	Černý	k1gMnSc2	Černý
z	z	k7c2	z
Čechomoru	Čechomor	k1gInSc2	Čechomor
na	na	k7c6	na
úvodní	úvodní	k2eAgFnSc6d1	úvodní
skladbě	skladba	k1gFnSc6	skladba
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
"	"	kIx"	"
<g/>
Skladbou	skladba	k1gFnSc7	skladba
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
na	na	k7c4	na
udělování	udělování	k1gNnSc4	udělování
cen	cena	k1gFnPc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Proměny	proměna	k1gFnSc2	proměna
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
platinové	platinový	k2eAgNnSc1d1	platinové
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
za	za	k7c7	za
"	"	kIx"	"
<g/>
Album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
kromě	kromě	k7c2	kromě
sólových	sólový	k2eAgInPc2d1	sólový
koncertů	koncert	k1gInPc2	koncert
i	i	k8xC	i
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
doprovodnou	doprovodný	k2eAgFnSc7d1	doprovodná
kapelou	kapela	k1gFnSc7	kapela
Baromantika	Baromantikum	k1gNnSc2	Baromantikum
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
Klávesové	klávesový	k2eAgInPc1d1	klávesový
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
-	-	kIx~	-
Beata	Beata	k1gFnSc1	Beata
Hlavenková	Hlavenkový	k2eAgFnSc1d1	Hlavenková
Klávesové	klávesový	k2eAgFnPc1d1	klávesová
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
samply	sampnout	k5eAaPmAgFnP	sampnout
<g/>
,	,	kIx,	,
kotle	kotel	k1gInPc1	kotel
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
-	-	kIx~	-
Viliam	Viliam	k1gMnSc1	Viliam
Béreš	Béreš	k1gFnSc2	Béreš
elektrická	elektrický	k2eAgNnPc1d1	elektrické
a	a	k8xC	a
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
-	-	kIx~	-
Patrick	Patrick	k1gMnSc1	Patrick
Karpentski	Karpentsk	k1gFnSc2	Karpentsk
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
samply	sampnout	k5eAaPmAgFnP	sampnout
-	-	kIx~	-
Martin	Martin	k2eAgMnSc1d1	Martin
Novák	Novák	k1gMnSc1	Novák
Album	album	k1gNnSc4	album
Mezi	mezi	k7c4	mezi
světy	svět	k1gInPc4	svět
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
v	v	k7c4	v
r.	r.	kA	r.
2005	[number]	k4	2005
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Universal	Universal	k1gMnSc1	Universal
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
pár	pár	k4xCyI	pár
týdnů	týden	k1gInPc2	týden
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
získalo	získat	k5eAaPmAgNnS	získat
status	status	k1gInSc4	status
zlaté	zlatý	k2eAgFnSc2d1	zlatá
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
světy	svět	k1gInPc4	svět
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
rocková	rockový	k2eAgFnSc1d1	rocková
deska	deska	k1gFnSc1	deska
<g/>
"	"	kIx"	"
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Desku	deska	k1gFnSc4	deska
produkoval	produkovat	k5eAaImAgMnS	produkovat
americký	americký	k2eAgMnSc1d1	americký
producent	producent	k1gMnSc1	producent
Ben	Ben	k1gInSc4	Ben
Yonas	Yonas	k1gMnSc1	Yonas
z	z	k7c2	z
Coast	Coast	k1gMnSc1	Coast
Recorders	Recorders	k1gInSc1	Recorders
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
a	a	k8xC	a
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
společně	společně	k6eAd1	společně
s	s	k7c7	s
Dusilovou	Dusilová	k1gFnSc7	Dusilová
a	a	k8xC	a
kytaristou	kytarista	k1gMnSc7	kytarista
Martinem	Martin	k1gMnSc7	Martin
Ledvinou	Ledvina	k1gMnSc7	Ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
hostovali	hostovat	k5eAaImAgMnP	hostovat
renomovaní	renomovaný	k2eAgMnPc1d1	renomovaný
hudebníci	hudebník	k1gMnPc1	hudebník
ze	z	k7c2	z
San	San	k1gFnSc2	San
Franciska	Francisko	k1gNnSc2	Francisko
-	-	kIx~	-
Jon	Jon	k1gFnSc1	Jon
Evans	Evansa	k1gFnPc2	Evansa
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Scott	Scott	k1gMnSc1	Scott
Amendola	Amendola	k1gFnSc1	Amendola
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
společně	společně	k6eAd1	společně
s	s	k7c7	s
Beatou	Beata	k1gFnSc7	Beata
Hlavenkovou	Hlavenkový	k2eAgFnSc7d1	Hlavenková
a	a	k8xC	a
klarinetovým	klarinetový	k2eAgInSc7d1	klarinetový
kvartetem	kvartet	k1gInSc7	kvartet
Clarinet	Clarinet	k1gMnSc1	Clarinet
Factory	Factor	k1gInPc4	Factor
zakládá	zakládat	k5eAaImIp3nS	zakládat
novou	nový	k2eAgFnSc4d1	nová
formaci	formace	k1gFnSc4	formace
Eternal	Eternal	k1gMnPc2	Eternal
Seekers	Seekers	k1gInSc4	Seekers
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yIgFnSc7	který
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
natočili	natočit	k5eAaBmAgMnP	natočit
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
eponymní	eponymní	k2eAgNnSc4d1	eponymní
album	album	k1gNnSc4	album
Eternal	Eternal	k1gFnSc2	Eternal
Seekers	Seekersa	k1gFnPc2	Seekersa
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgMnPc4	který
Lenka	Lenka	k1gFnSc1	Lenka
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sluníčko	sluníčko	k1gNnSc1	sluníčko
Sluníčko	sluníčko	k1gNnSc1	sluníčko
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Pusa	pusa	k1gFnSc1	pusa
Pusa	pusa	k1gFnSc1	pusa
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Sólová	sólový	k2eAgFnSc1d1	sólová
alba	alba	k1gFnSc1	alba
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Spatřit	spatřit	k5eAaPmF	spatřit
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
UnEarthEd	UnEarthEda	k1gFnPc2	UnEarthEda
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Mezi	mezi	k7c7	mezi
světy	svět	k1gInPc7	svět
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Mezi	mezi	k7c7	mezi
světy	svět	k1gInPc7	svět
(	(	kIx(	(
<g/>
US	US	kA	US
verze	verze	k1gFnSc1	verze
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Eternal	Eternal	k1gFnSc1	Eternal
Seekers	Seekersa	k1gFnPc2	Seekersa
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
s	s	k7c7	s
Clarinet	Clarinet	k1gInSc4	Clarinet
Factory	Factor	k1gInPc7	Factor
a	a	k8xC	a
Beatou	Beata	k1gFnSc7	Beata
Hlavenkovou	Hlavenkový	k2eAgFnSc7d1	Hlavenková
<g/>
)	)	kIx)	)
Baromantika	Baromantikum	k1gNnPc1	Baromantikum
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
a	a	k8xC	a
Baromantika	Baromantika	k1gFnSc1	Baromantika
<g/>
:	:	kIx,	:
Live	Live	k1gFnSc1	Live
at	at	k?	at
Café	café	k1gNnSc1	café
v	v	k7c6	v
lese	les	k1gInSc6	les
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
a	a	k8xC	a
Baromantika	Baromantika	k1gFnSc1	Baromantika
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Hostování	hostování	k1gNnSc1	hostování
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc1	spolupráce
Lucie	Lucie	k1gFnSc1	Lucie
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
-	-	kIx~	-
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc4	píseň
Holka	holka	k1gFnSc1	holka
Magor	magor	k1gMnSc1	magor
<g/>
,	,	kIx,	,
Fet	Fet	k1gMnSc1	Fet
<g/>
)	)	kIx)	)
Čechomor	Čechomor	k1gMnSc1	Čechomor
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
-	-	kIx~	-
Proměny	proměna	k1gFnPc1	proměna
Wohnout	Wohnout	k1gFnSc2	Wohnout
-	-	kIx~	-
Polib	políbit	k5eAaPmRp2nS	políbit
si	se	k3xPyFc3	se
dědu	děda	k1gMnSc4	děda
<g/>
,	,	kIx,	,
Rande	rande	k1gNnSc4	rande
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Bendou	Benda	k1gMnSc7	Benda
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
Rayda	Rayda	k1gMnSc1	Rayda
<g/>
)	)	kIx)	)
Vertigo	Vertigo	k1gMnSc1	Vertigo
quintet	quintet	k1gMnSc1	quintet
Jan	Jan	k1gMnSc1	Jan
Burian	Burian	k1gMnSc1	Burian
-	-	kIx~	-
Dívčí	dívčí	k2eAgFnSc1d1	dívčí
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
věšáci	věšák	k1gMnPc1	věšák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Hrůza	Hrůza	k1gMnSc1	Hrůza
-	-	kIx~	-
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Píseň	píseň	k1gFnSc1	píseň
kovbojská	kovbojský	k2eAgFnSc1d1	kovbojská
Květy	Květa	k1gFnPc1	Květa
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
Myjau	Myjaus	k1gInSc2	Myjaus
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
V	v	k7c6	v
čajové	čajový	k2eAgFnSc6d1	čajová
konvici	konvice	k1gFnSc6	konvice
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Neckář	Neckář	k1gMnSc1	Neckář
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgInSc4d1	dobrý
časy	čas	k1gInPc4	čas
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
Na	na	k7c6	na
Rafandě	Rafanda	k1gFnSc6	Rafanda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Lucie	Lucie	k1gFnSc1	Lucie
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
za	za	k7c4	za
"	"	kIx"	"
<g/>
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
"	"	kIx"	"
<g/>
Skladba	skladba	k1gFnSc1	skladba
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
za	za	k7c4	za
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Muka	muka	k1gFnSc1	muka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
"	"	kIx"	"
<g/>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
"	"	kIx"	"
<g/>
Album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
za	za	k7c4	za
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Mezi	mezi	k7c4	mezi
světy	svět	k1gInPc4	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
"	"	kIx"	"
<g/>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
televize	televize	k1gFnSc2	televize
Óčko	Óčko	k6eAd1	Óčko
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
domácí	domácí	k2eAgFnSc1d1	domácí
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
"	"	kIx"	"
<g/>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
"	"	kIx"	"
<g/>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
za	za	k7c4	za
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Eternal	Eternal	k1gFnSc1	Eternal
Seekers	Seekersa	k1gFnPc2	Seekersa
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
Cena	cena	k1gFnSc1	cena
<g />
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
"	"	kIx"	"
<g/>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
za	za	k7c4	za
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Baromantika	Baromantika	k1gFnSc1	Baromantika
<g/>
"	"	kIx"	"
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilový	k2eAgFnSc1d1	Dusilová
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Lenčin	Lenčin	k2eAgInSc1d1	Lenčin
virtuální	virtuální	k2eAgInSc1d1	virtuální
svět	svět	k1gInSc1	svět
na	na	k7c4	na
MySpace	MySpace	k1gFnPc4	MySpace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Video	video	k1gNnSc1	video
Čechomor	Čechomora	k1gFnPc2	Čechomora
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
-	-	kIx~	-
Proměny	proměna	k1gFnSc2	proměna
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
:	:	kIx,	:
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
Osoba	osoba	k1gFnSc1	osoba
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilový	k2eAgFnSc1d1	Dusilová
</s>
