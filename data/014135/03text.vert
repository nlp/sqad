<s>
Požární	požární	k2eAgInSc1d1
úsek	úsek	k1gInSc1
</s>
<s>
Požární	požární	k2eAgInSc1d1
úsek	úsek	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
PÚ	PÚ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prostor	prostor	k1gInSc4
ve	v	k7c6
stavebním	stavební	k2eAgInSc6d1
objektu	objekt	k1gInSc6
<g/>
,	,	kIx,
oddělený	oddělený	k2eAgMnSc1d1
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
požárních	požární	k2eAgInPc2d1
úseků	úsek	k1gInPc2
požárně	požárně	k6eAd1
dělicími	dělicí	k2eAgFnPc7d1
konstrukcemi	konstrukce	k1gFnPc7
nebo	nebo	k8xC
požárně	požárně	k6eAd1
bezpečnostním	bezpečnostní	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnPc4d2
stavby	stavba	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
rodinné	rodinný	k2eAgInPc1d1
domy	dům	k1gInPc1
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
ve	v	k7c6
stanovených	stanovený	k2eAgInPc6d1
případech	případ	k1gInPc6
tvořit	tvořit	k5eAaImF
jen	jen	k6eAd1
jediný	jediný	k2eAgInSc1d1
požární	požární	k2eAgInSc1d1
úsek	úsek	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Účelem	účel	k1gInSc7
dělení	dělení	k1gNnSc1
stavby	stavba	k1gFnSc2
do	do	k7c2
požárních	požární	k2eAgInPc2d1
úseků	úsek	k1gInPc2
je	být	k5eAaImIp3nS
zabránění	zabránění	k1gNnSc1
šíření	šíření	k1gNnSc2
požáru	požár	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dělení	dělení	k1gNnSc1
stavby	stavba	k1gFnSc2
do	do	k7c2
požárních	požární	k2eAgInPc2d1
úseků	úsek	k1gInPc2
</s>
<s>
Požární	požární	k2eAgInPc1d1
úseky	úsek	k1gInPc1
jsou	být	k5eAaImIp3nP
omezeny	omezen	k2eAgFnPc1d1
zejména	zejména	k9
mezní	mezní	k2eAgFnSc7d1
velikostí	velikost	k1gFnSc7
(	(	kIx(
<g/>
šířka	šířka	k1gFnSc1
a	a	k8xC
délka	délka	k1gFnSc1
požárního	požární	k2eAgInSc2d1
úseku	úsek	k1gInSc2
nebo	nebo	k8xC
půdorysná	půdorysný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
mezním	mezní	k2eAgInSc7d1
počtem	počet	k1gInSc7
užitných	užitný	k2eAgNnPc2d1
podlaží	podlaží	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
některých	některý	k3yIgNnPc2
zařízení	zařízení	k1gNnPc2
a	a	k8xC
provozů	provoz	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vyžadováno	vyžadován	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
samostaných	samostaný	k2eAgInPc2d1
požárních	požární	k2eAgInPc2d1
úseků	úsek	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
chráněné	chráněný	k2eAgFnPc4d1
únikové	únikový	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
výtahové	výtahový	k2eAgFnPc4d1
a	a	k8xC
instalační	instalační	k2eAgFnPc4d1
šachty	šachta	k1gFnPc4
<g/>
,	,	kIx,
kotelny	kotelna	k1gFnPc4
<g/>
,	,	kIx,
strojovny	strojovna	k1gFnPc4
výtahů	výtah	k1gInPc2
a	a	k8xC
vzduchotechniky	vzduchotechnika	k1gFnSc2
<g/>
,	,	kIx,
strojovny	strojovna	k1gFnSc2
požárně	požárně	k6eAd1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1
požadavk	požadavk	k1gInSc1
na	na	k7c4
dělení	dělení	k1gNnSc4
stavby	stavba	k1gFnSc2
do	do	k7c2
požárních	požární	k2eAgInPc2d1
úseků	úsek	k1gInPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
stanoveny	stanovit	k5eAaPmNgInP
také	také	k9
věcně	věcně	k6eAd1
příslušnými	příslušný	k2eAgFnPc7d1
normami	norma	k1gFnPc7
požární	požární	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
staveb	stavba	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
stavby	stavba	k1gFnSc2
pro	pro	k7c4
bydlení	bydlení	k1gNnSc4
a	a	k8xC
ubytování	ubytování	k1gNnSc4
<g/>
,	,	kIx,
zdravotnická	zdravotnický	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
<g/>
,	,	kIx,
shromažďovací	shromažďovací	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Požární	požární	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
</s>
<s>
Požární	požární	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
charakteristik	charakteristika	k1gFnPc2
požárních	požární	k2eAgInPc2d1
úseků	úsek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vyjadřováno	vyjadřovat	k5eAaImNgNnS
jako	jako	k9
množství	množství	k1gNnSc1
dřeva	dřevo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc7
normovou	normový	k2eAgFnSc7d1
výhřevností	výhřevnost	k1gFnSc7
ekvivalentní	ekvivalentní	k2eAgFnSc2d1
k	k	k7c3
normové	normový	k2eAgFnSc3d1
výhřevnosti	výhřevnost	k1gFnSc3
všech	všecek	k3xTgFnPc2
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
PÚ	PÚ	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS
se	se	k3xPyFc4
požární	požární	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
stálé	stálý	k2eAgNnSc1d1
a	a	k8xC
nahodilé	nahodilý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stálé	stálý	k2eAgNnSc1d1
požární	požární	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
je	být	k5eAaImIp3nS
pevně	pevně	k6eAd1
zabudováno	zabudovat	k5eAaPmNgNnS
ve	v	k7c6
stavbě	stavba	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
zejména	zejména	k9
okny	okno	k1gNnPc7
<g/>
,	,	kIx,
dveřmi	dveře	k1gFnPc7
a	a	k8xC
podlahou	podlaha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Výpočtové	výpočtový	k2eAgNnSc1d1
požární	požární	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
charakterizováno	charakterizován	k2eAgNnSc1d1
součinitelem	součinitel	k1gInSc7
rychlosti	rychlost	k1gFnSc2
odhořívání	odhořívání	k1gNnSc2
a	a	k8xC
<g/>
,	,	kIx,
součinitelem	součinitel	k1gMnSc7
odvětrávání	odvětrávání	k1gNnSc2
b	b	k?
a	a	k8xC
součinitelem	součinitel	k1gInSc7
vyjadřujícím	vyjadřující	k2eAgInSc7d1
vliv	vliv	k1gInSc4
požárně	požárně	k6eAd1
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
c.	c.	k?
</s>
<s>
Prostory	prostor	k1gInPc1
s	s	k7c7
požárním	požární	k2eAgNnSc7d1
zatížením	zatížení	k1gNnSc7
nejvýše	nejvýše	k6eAd1,k6eAd3
7,5	7,5	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
a	a	k8xC
zároveň	zároveň	k6eAd1
součinitelem	součinitel	k1gInSc7
a	a	k8xC
nejvýše	nejvýše	k6eAd1,k6eAd3
1,1	1,1	k4
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
prostory	prostora	k1gFnPc4
bez	bez	k7c2
požárního	požární	k2eAgNnSc2d1
rizika	riziko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Stupeň	stupeň	k1gInSc1
požární	požární	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
</s>
<s>
Stupeň	stupeň	k1gInSc1
požární	požární	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
SPB	SPB	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dalším	další	k2eAgInSc7d1
údajem	údaj	k1gInSc7
charakterizujícím	charakterizující	k2eAgInSc7d1
daný	daný	k2eAgInSc4d1
požární	požární	k2eAgInSc4d1
úsek	úsek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
(	(	kIx(
<g/>
I	i	k9
–	–	k?
VII	VII	kA
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
stanovována	stanovovat	k5eAaImNgFnS
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
požární	požární	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
objektu	objekt	k1gInSc2
<g/>
,	,	kIx,
nejvyšším	vysoký	k2eAgInSc7d3
požárním	požární	k2eAgInSc7d1
zatížení	zatížení	k1gNnSc4
v	v	k7c6
PÚ	PÚ	kA
a	a	k8xC
hořlavostí	hořlavost	k1gFnSc7
konstrukčního	konstrukční	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c4
SPB	SPB	kA
jsou	být	k5eAaImIp3nP
stanovovány	stanovován	k2eAgInPc1d1
další	další	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
<g/>
,	,	kIx,
zejména	zejména	k9
požární	požární	k2eAgFnSc4d1
odolnost	odolnost	k1gFnSc4
konstrukcí	konstrukce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Požární	požární	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
staveb	stavba	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ČSN	ČSN	kA
73	#num#	k4
0833	#num#	k4
-	-	kIx~
Požární	požární	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
staveb	stavba	k1gFnPc2
-	-	kIx~
Budovy	budova	k1gFnPc1
pro	pro	k7c4
bydlení	bydlení	k1gNnSc4
a	a	k8xC
ubytování	ubytování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
REICHEL	Reichel	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabraňujeme	zabraňovat	k5eAaImIp1nP
škodám	škoda	k1gFnPc3
<g/>
:	:	kIx,
svazek	svazek	k1gInSc4
2	#num#	k4
-	-	kIx~
Požární	požární	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
pro	pro	k7c4
stavební	stavební	k2eAgInPc4d1
objekty	objekt	k1gInPc4
v	v	k7c6
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
pojišťovna	pojišťovna	k1gFnSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČSN	ČSN	kA
73	#num#	k4
0802	#num#	k4
-	-	kIx~
Požární	požární	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
staveb	stavba	k1gFnPc2
-	-	kIx~
Nevýrobní	výrobní	k2eNgInPc1d1
objekty	objekt	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Požární	požární	k2eAgInSc1d1
úsek	úsek	k1gInSc1
</s>
<s>
Požární	požární	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
staveb	stavba	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
