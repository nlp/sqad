<s>
Google	Google	k6eAd1	Google
Talk	Talk	k1gInSc1	Talk
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
instant	instant	k?	instant
messenger	messenger	k1gInSc1	messenger
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc1d1	komunikační
služba	služba	k1gFnSc1	služba
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gFnSc1	Google
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
protokolu	protokol	k1gInSc6	protokol
XMPP	XMPP	kA	XMPP
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
textové	textový	k2eAgFnSc2d1	textová
komunikace	komunikace	k1gFnSc2	komunikace
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
službu	služba	k1gFnSc4	služba
VoIP	VoIP	k1gFnSc4	VoIP
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
protokolu	protokol	k1gInSc6	protokol
Jingle	Jingle	k1gFnSc2	Jingle
<g/>
.	.	kIx.	.
</s>
