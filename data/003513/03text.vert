<s>
Google	Google	k6eAd1	Google
Talk	Talk	k1gInSc1	Talk
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
instant	instant	k?	instant
messenger	messenger	k1gInSc1	messenger
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc1d1	komunikační
služba	služba	k1gFnSc1	služba
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gFnSc1	Google
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
protokolu	protokol	k1gInSc6	protokol
XMPP	XMPP	kA	XMPP
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
textové	textový	k2eAgFnSc2d1	textová
komunikace	komunikace	k1gFnSc2	komunikace
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
službu	služba	k1gFnSc4	služba
VoIP	VoIP	k1gFnSc4	VoIP
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
protokolu	protokol	k1gInSc6	protokol
Jingle	Jingle	k1gFnSc2	Jingle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
službami	služba	k1gFnPc7	služba
pak	pak	k6eAd1	pak
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
videochatu	videochat	k2eAgFnSc4d1	videochat
<g/>
,	,	kIx,	,
sdílení	sdílení	k1gNnSc4	sdílení
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
hlasové	hlasový	k2eAgFnSc2d1	hlasová
schránky	schránka	k1gFnSc2	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
registrací	registrace	k1gFnSc7	registrace
účtu	účet	k1gInSc2	účet
Google	Google	k1gNnSc2	Google
nebo	nebo	k8xC	nebo
na	na	k7c6	na
poštovním	poštovní	k2eAgInSc6d1	poštovní
serveru	server	k1gInSc6	server
Gmail	Gmaila	k1gFnPc2	Gmaila
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
propojen	propojen	k2eAgMnSc1d1	propojen
(	(	kIx(	(
<g/>
sdílení	sdílení	k1gNnSc1	sdílení
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
oznamování	oznamování	k1gNnSc1	oznamování
nové	nový	k2eAgFnSc2d1	nová
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
integrovaný	integrovaný	k2eAgMnSc1d1	integrovaný
klient	klient	k1gMnSc1	klient
ve	v	k7c6	v
webovém	webový	k2eAgNnSc6d1	webové
rozhraní	rozhraní	k1gNnSc6	rozhraní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aplikaci	aplikace	k1gFnSc4	aplikace
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
v	v	k7c6	v
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
Windows	Windows	kA	Windows
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
XP	XP	kA	XP
<g/>
,	,	kIx,	,
Vista	vista	k2eAgFnSc1d1	vista
a	a	k8xC	a
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Službu	služba	k1gFnSc4	služba
Google	Google	k1gNnSc2	Google
Talk	Talka	k1gFnPc2	Talka
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
klientech	klient	k1gMnPc6	klient
pro	pro	k7c4	pro
instant	instant	k?	instant
messaging	messaging	k1gInSc4	messaging
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
Miranda	Miranda	k1gFnSc1	Miranda
<g/>
,	,	kIx,	,
QIP	QIP	kA	QIP
<g/>
,	,	kIx,	,
Pidgin	Pidgin	k1gMnSc1	Pidgin
<g/>
,	,	kIx,	,
Psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
Trillian	Trillian	k1gMnSc1	Trillian
<g/>
,	,	kIx,	,
Kopete	kopat	k5eAaImIp2nP	kopat
nebo	nebo	k8xC	nebo
Mozilla	Mozilla	k1gFnSc1	Mozilla
Thunderbird	Thunderbirda	k1gFnPc2	Thunderbirda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
Google	Google	k1gInSc1	Google
přestal	přestat	k5eAaPmAgInS	přestat
podporovat	podporovat	k5eAaImF	podporovat
klientskou	klientský	k2eAgFnSc4d1	klientská
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
budou	být	k5eAaImBp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
uživatelé	uživatel	k1gMnPc1	uživatel
Google	Google	k1gNnPc2	Google
Talk	Talka	k1gFnPc2	Talka
přepnuti	přepnut	k2eAgMnPc1d1	přepnut
na	na	k7c4	na
Google	Google	k1gNnSc4	Google
Hangouts	Hangoutsa	k1gFnPc2	Hangoutsa
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchý	jednoduchý	k2eAgMnSc1d1	jednoduchý
a	a	k8xC	a
hezký	hezký	k2eAgMnSc1d1	hezký
klient	klient	k1gMnSc1	klient
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Propojení	propojení	k1gNnSc1	propojení
s	s	k7c7	s
emailovou	emailový	k2eAgFnSc7d1	emailová
schránkou	schránka	k1gFnSc7	schránka
Gmail	Gmaila	k1gFnPc2	Gmaila
<g/>
.	.	kIx.	.
</s>
<s>
Webové	webový	k2eAgNnSc1d1	webové
rozhraní	rozhraní	k1gNnSc1	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Součást	součást	k1gFnSc1	součást
XMPP	XMPP	kA	XMPP
sítě	síť	k1gFnSc2	síť
-	-	kIx~	-
bezproblémová	bezproblémový	k2eAgFnSc1d1	bezproblémová
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
použít	použít	k5eAaPmF	použít
jiného	jiný	k2eAgMnSc4d1	jiný
klienta	klient	k1gMnSc4	klient
<g/>
.	.	kIx.	.
</s>
<s>
Transporty	transporta	k1gFnPc1	transporta
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
ICQ	ICQ	kA	ICQ
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gNnSc1	Yahoo
<g/>
,	,	kIx,	,
MSN	MSN	kA	MSN
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
)	)	kIx)	)
Možnost	možnost	k1gFnSc4	možnost
integrace	integrace	k1gFnSc2	integrace
různých	různý	k2eAgFnPc2d1	různá
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
předpověď	předpověď	k1gFnSc1	předpověď
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
posílání	posílání	k1gNnSc1	posílání
SMS	SMS	kA	SMS
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
Oproti	oproti	k7c3	oproti
např.	např.	kA	např.
ICQ	ICQ	kA	ICQ
u	u	k7c2	u
Jabberu	Jabber	k1gInSc2	Jabber
není	být	k5eNaImIp3nS	být
omezení	omezení	k1gNnSc1	omezení
délky	délka	k1gFnSc2	délka
zprávy	zpráva	k1gFnSc2	zpráva
Komunikace	komunikace	k1gFnSc1	komunikace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zašifrovaná	zašifrovaný	k2eAgFnSc1d1	zašifrovaná
Adresa	adresa	k1gFnSc1	adresa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lidsky	lidsky	k6eAd1	lidsky
čitelném	čitelný	k2eAgInSc6d1	čitelný
formátu	formát	k1gInSc6	formát
Jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
přenos	přenos	k1gInSc4	přenos
souborů	soubor	k1gInPc2	soubor
(	(	kIx(	(
<g/>
pouhým	pouhý	k2eAgNnSc7d1	pouhé
přetažením	přetažení	k1gNnSc7	přetažení
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
kontaktech	kontakt	k1gInPc6	kontakt
nebo	nebo	k8xC	nebo
do	do	k7c2	do
komunikačního	komunikační	k2eAgNnSc2d1	komunikační
okna	okno	k1gNnSc2	okno
<g/>
)	)	kIx)	)
Nestandardní	standardní	k2eNgInSc1d1	nestandardní
protokol	protokol	k1gInSc1	protokol
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Nestandardní	standardní	k2eNgNnSc1d1	nestandardní
chování	chování	k1gNnSc1	chování
offline	offlin	k1gInSc5	offlin
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
zaslány	zaslán	k2eAgInPc1d1	zaslán
jako	jako	k8xS	jako
e-mail	eail	k1gInSc1	e-mail
a	a	k8xC	a
ne	ne	k9	ne
jako	jako	k8xC	jako
jabber	jabber	k1gMnSc1	jabber
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
)	)	kIx)	)
Nestandardní	standardní	k2eNgNnSc1d1	nestandardní
blokování	blokování	k1gNnSc1	blokování
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Nestandardní	standardní	k2eNgFnSc1d1	nestandardní
implementace	implementace	k1gFnSc1	implementace
konferencí	konference	k1gFnPc2	konference
(	(	kIx(	(
<g/>
Multi-User	Multi-User	k1gInSc1	Multi-User
Chat	chata	k1gFnPc2	chata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
chování	chování	k1gNnSc4	chování
při	při	k7c6	při
připojení	připojení	k1gNnSc6	připojení
z	z	k7c2	z
Apple	Apple	kA	Apple
iChat	iChat	k5eAaBmF	iChat
<g/>
.	.	kIx.	.
</s>
<s>
Shrnutí	shrnutí	k1gNnSc1	shrnutí
anglicky	anglicky	k6eAd1	anglicky
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Google	Google	k1gNnSc2	Google
Talk	Talka	k1gFnPc2	Talka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
<g/>
:	:	kIx,	:
Google	Google	k1gNnSc1	Google
Chat	chata	k1gFnPc2	chata
-	-	kIx~	-
Chat	chata	k1gFnPc2	chata
with	witha	k1gFnPc2	witha
family	famila	k1gFnSc2	famila
and	and	k?	and
friends	friends	k1gInSc1	friends
Google	Google	k1gFnSc1	Google
Talk	Talk	k1gMnSc1	Talk
(	(	kIx(	(
<g/>
služba	služba	k1gFnSc1	služba
<g/>
)	)	kIx)	)
na	na	k7c4	na
Jabber	Jabber	k1gInSc4	Jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Wiki	Wiki	k1gNnSc1	Wiki
<g/>
:	:	kIx,	:
Google	Google	k1gFnSc1	Google
Talk	Talk	k1gMnSc1	Talk
(	(	kIx(	(
<g/>
server	server	k1gInSc1	server
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jabber	Jabber	k1gInSc1	Jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Wiki	Wik	k1gFnSc2	Wik
Google	Googl	k1gMnSc4	Googl
Talk	Talk	k1gMnSc1	Talk
(	(	kIx(	(
<g/>
klient	klient	k1gMnSc1	klient
<g/>
)	)	kIx)	)
na	na	k7c4	na
Jabber	Jabber	k1gInSc4	Jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Wiki	Wiki	k1gNnSc1	Wiki
<g/>
:	:	kIx,	:
Google	Google	k1gFnSc1	Google
Talk	Talk	k1gMnSc1	Talk
-	-	kIx~	-
Jabber	Jabber	k1gMnSc1	Jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Wiki	Wik	k1gFnSc2	Wik
</s>
