<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
je	být	k5eAaImIp3nS	být
kriminální	kriminální	k2eAgNnSc1d1	kriminální
filmové	filmový	k2eAgNnSc1d1	filmové
drama	drama	k1gNnSc1	drama
Davida	David	k1gMnSc2	David
Ondříčka	Ondříček	k1gMnSc2	Ondříček
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
měnovou	měnový	k2eAgFnSc7d1	měnová
reformou	reforma	k1gFnSc7	reforma
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
udílení	udílení	k1gNnSc2	udílení
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgInS	odnést
jedenáct	jedenáct	k4xCc4	jedenáct
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
scéna	scéna	k1gFnSc1	scéna
se	s	k7c7	s
zastřeleným	zastřelený	k2eAgMnSc7d1	zastřelený
koněm	kůň	k1gMnSc7	kůň
(	(	kIx(	(
<g/>
nejdražší	drahý	k2eAgInSc1d3	nejdražší
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
filmu	film	k1gInSc6	film
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
by	by	kYmCp3nS	by
tak	tak	k9	tak
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
podobný	podobný	k2eAgInSc1d1	podobný
seriálu	seriál	k1gInSc2	seriál
Znamení	znamení	k1gNnSc2	znamení
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Ondříček	Ondříček	k1gMnSc1	Ondříček
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
kapitán	kapitán	k1gMnSc1	kapitán
Hakl	Hakl	k1gMnSc1	Hakl
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
zaníceným	zanícený	k2eAgMnSc7d1	zanícený
komunistou	komunista	k1gMnSc7	komunista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
děje	děj	k1gInSc2	děj
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
natáčení	natáčení	k1gNnSc3	natáčení
byl	být	k5eAaImAgInS	být
naplánován	naplánovat	k5eAaBmNgInS	naplánovat
na	na	k7c6	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
plánováno	plánovat	k5eAaImNgNnS	plánovat
bylo	být	k5eAaImAgNnS	být
45	[number]	k4	45
natáčecích	natáčecí	k2eAgInPc2d1	natáčecí
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Natáčelo	natáčet	k5eAaImAgNnS	natáčet
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
i	i	k8xC	i
v	v	k7c6	v
barrandovských	barrandovský	k2eAgInPc6d1	barrandovský
ateliérech	ateliér	k1gInPc6	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
zabrala	zabrat	k5eAaPmAgFnS	zabrat
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
17	[number]	k4	17
verzí	verze	k1gFnPc2	verze
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Výrobu	výroba	k1gFnSc4	výroba
filmu	film	k1gInSc2	film
podpořila	podpořit	k5eAaPmAgFnS	podpořit
společnost	společnost	k1gFnSc1	společnost
RWE	RWE	kA	RWE
<g/>
,	,	kIx,	,
Státní	státní	k2eAgInSc1d1	státní
fond	fond	k1gInSc1	fond
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
evropský	evropský	k2eAgInSc1d1	evropský
fond	fond	k1gInSc1	fond
Eurimages	Eurimagesa	k1gFnPc2	Eurimagesa
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Polský	polský	k2eAgInSc1d1	polský
filmový	filmový	k2eAgInSc1d1	filmový
institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
amerického	americký	k2eAgMnSc2d1	americký
distributora	distributor	k1gMnSc2	distributor
a	a	k8xC	a
koproducenta	koproducent	k1gMnSc2	koproducent
Ehuda	Ehud	k1gMnSc2	Ehud
Bleiberga	Bleiberg	k1gMnSc2	Bleiberg
byl	být	k5eAaImAgInS	být
film	film	k1gInSc4	film
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
na	na	k7c6	na
Berlinale	Berlinale	k1gFnSc6	Berlinale
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
přestříhán	přestříhán	k2eAgInSc1d1	přestříhán
(	(	kIx(	(
<g/>
na	na	k7c6	na
konečném	konečný	k2eAgInSc6d1	konečný
střihu	střih	k1gInSc6	střih
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
také	také	k9	také
Ivan	Ivan	k1gMnSc1	Ivan
Passer	Passer	k1gMnSc1	Passer
<g/>
)	)	kIx)	)
a	a	k8xC	a
premiéra	premiéra	k1gFnSc1	premiéra
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
odsunuta	odsunout	k5eAaPmNgFnS	odsunout
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
únorového	únorový	k2eAgInSc2d1	únorový
termínu	termín	k1gInSc2	termín
na	na	k7c6	na
září	září	k1gNnSc6	září
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k9	také
upravena	upravit	k5eAaPmNgFnS	upravit
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
složili	složit	k5eAaPmAgMnP	složit
Jan	Jan	k1gMnSc1	Jan
P.	P.	kA	P.
Muchow	Muchow	k1gMnSc1	Muchow
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Novinski	Novinsk	k1gFnSc2	Novinsk
.	.	kIx.	.
</s>
<s>
Muchow	Muchow	k?	Muchow
a	a	k8xC	a
Novinski	Novinsk	k1gFnSc2	Novinsk
na	na	k7c6	na
hudbě	hudba	k1gFnSc6	hudba
intenzivněji	intenzivně	k6eAd2	intenzivně
pracovali	pracovat	k5eAaImAgMnP	pracovat
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
Cenou	cena	k1gFnSc7	cena
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
i	i	k8xC	i
Českým	český	k2eAgInSc7d1	český
lvem	lev	k1gInSc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Sarou	Sara	k1gFnSc7	Sara
Vondráškovou	Vondrášková	k1gFnSc7	Vondrášková
(	(	kIx(	(
<g/>
Never	Never	k1gInSc1	Never
Sol	sol	k1gNnSc1	sol
<g/>
)	)	kIx)	)
Muchow	Muchow	k1gMnPc1	Muchow
a	a	k8xC	a
Novinski	Novinsk	k1gMnPc1	Novinsk
složili	složit	k5eAaPmAgMnP	složit
a	a	k8xC	a
nahráli	nahrát	k5eAaBmAgMnP	nahrát
ústřední	ústřední	k2eAgFnSc4d1	ústřední
píseň	píseň	k1gFnSc4	píseň
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
Lay	Lay	k1gMnSc1	Lay
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
klip	klip	k1gInSc1	klip
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
herci	herec	k1gMnSc3	herec
Radoslavu	Radoslav	k1gMnSc3	Radoslav
Brzobohatému	Brzobohatý	k1gMnSc3	Brzobohatý
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
ve	v	k7c6	v
filmu	film	k1gInSc6	film
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
den	den	k1gInSc4	den
před	před	k7c7	před
premiérou	premiéra	k1gFnSc7	premiéra
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
v	v	k7c4	v
den	den	k1gInSc4	den
premiéry	premiéra	k1gFnSc2	premiéra
by	by	kYmCp3nS	by
oslavil	oslavit	k5eAaPmAgInS	oslavit
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kin	kino	k1gNnPc2	kino
byl	být	k5eAaImAgMnS	být
film	film	k1gInSc4	film
nasazen	nasazen	k2eAgInSc4d1	nasazen
jak	jak	k6eAd1	jak
na	na	k7c6	na
digitálních	digitální	k2eAgInPc6d1	digitální
nosičích	nosič	k1gInPc6	nosič
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
klasických	klasický	k2eAgInPc2d1	klasický
35	[number]	k4	35
<g/>
mm	mm	kA	mm
filmových	filmový	k2eAgFnPc6d1	filmová
kopiích	kopie	k1gFnPc6	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
ruštiny	ruština	k1gFnSc2	ruština
(	(	kIx(	(
<g/>
překladatelé	překladatel	k1gMnPc1	překladatel
Diana	Diana	k1gFnSc1	Diana
Shvedova	Shvedův	k2eAgFnSc1d1	Shvedova
a	a	k8xC	a
Andrey	Andrea	k1gFnPc1	Andrea
Efremov	Efremov	k1gInSc1	Efremov
<g/>
)	)	kIx)	)
a	a	k8xC	a
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Dnů	den	k1gInPc2	den
slovenské	slovenský	k2eAgFnSc2d1	slovenská
kinematografie	kinematografie	k1gFnSc2	kinematografie
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Kamil	Kamil	k1gMnSc1	Kamil
Fila	Fila	k1gMnSc1	Fila
<g/>
,	,	kIx,	,
Respekt	respekt	k1gInSc1	respekt
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Mirka	Mirka	k1gFnSc1	Mirka
Spáčilová	Spáčilová	k1gFnSc1	Spáčilová
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Petr	Petr	k1gMnSc1	Petr
Čihula	Čihula	k1gFnSc1	Čihula
<g/>
,	,	kIx,	,
MovieZone	MovieZon	k1gMnSc5	MovieZon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
František	František	k1gMnSc1	František
Fuka	Fuka	k1gMnSc1	Fuka
<g/>
,	,	kIx,	,
FFFilm	FFFilm	k1gMnSc1	FFFilm
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
67	[number]	k4	67
%	%	kIx~	%
na	na	k7c4	na
Film	film	k1gInSc4	film
CZ	CZ	kA	CZ
-	-	kIx~	-
Vynikající	vynikající	k2eAgFnSc1d1	vynikající
vizuální	vizuální	k2eAgFnSc1d1	vizuální
podívaná	podívaná	k1gFnSc1	podívaná
–	–	k?	–
Příběh	příběh	k1gInSc1	příběh
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
poněkud	poněkud	k6eAd1	poněkud
"	"	kIx"	"
<g/>
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ToSiPiš	ToSiPiš	k1gMnSc1	ToSiPiš
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
17.12	[number]	k4	17.12
<g/>
.2012	.2012	k4	.2012
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
Českou	český	k2eAgFnSc7d1	Česká
filmovou	filmový	k2eAgFnSc7d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc7d1	televizní
akademií	akademie	k1gFnSc7	akademie
navržen	navrhnout	k5eAaPmNgMnS	navrhnout
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
Cenách	cena	k1gFnPc6	cena
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Ivana	Ivan	k1gMnSc4	Ivan
Trojana	Trojan	k1gMnSc4	Trojan
<g/>
.	.	kIx.	.
</s>
<s>
Nominován	nominovat	k5eAaBmNgInS	nominovat
byl	být	k5eAaImAgInS	být
také	také	k9	také
za	za	k7c4	za
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Kocha	Koch	k1gMnSc4	Koch
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Soňu	Soňa	k1gFnSc4	Soňa
Norisovou	Norisový	k2eAgFnSc4d1	Norisová
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
cenu	cena	k1gFnSc4	cena
Trilobit	trilobit	k1gMnSc1	trilobit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Českých	český	k2eAgInPc6d1	český
lvech	lev	k1gInPc6	lev
získal	získat	k5eAaPmAgInS	získat
devět	devět	k4xCc4	devět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
kamera	kamera	k1gFnSc1	kamera
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
řešení	řešení	k1gNnSc4	řešení
a	a	k8xC	a
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Ivana	Ivan	k1gMnSc4	Ivan
Trojana	Trojan	k1gMnSc4	Trojan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Soňu	Soňa	k1gFnSc4	Soňa
Norisovou	Norisový	k2eAgFnSc4d1	Norisová
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Kocha	Koch	k1gMnSc4	Koch
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k6eAd1	také
cenu	cena	k1gFnSc4	cena
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
teoretiků	teoretik	k1gMnPc2	teoretik
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
plakát	plakát	k1gInSc4	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
započítají	započítat	k5eAaPmIp3nP	započítat
i	i	k9	i
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
film	film	k1gInSc4	film
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
11	[number]	k4	11
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vyrovnal	vyrovnat	k5eAaPmAgMnS	vyrovnat
historický	historický	k2eAgInSc4d1	historický
rekord	rekord	k1gInSc4	rekord
filmu	film	k1gInSc2	film
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zabít	zabít	k5eAaPmF	zabít
Sekala	Sekal	k1gMnSc4	Sekal
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
filmu	film	k1gInSc2	film
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gMnSc5	Databas
</s>
