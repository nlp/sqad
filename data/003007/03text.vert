<s>
Gamut	Gamut	k1gMnSc1	Gamut
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
barevný	barevný	k2eAgInSc1d1	barevný
gamut	gamut	k1gInSc1	gamut
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
oblast	oblast	k1gFnSc1	oblast
barev	barva	k1gFnPc2	barva
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
barevném	barevný	k2eAgInSc6d1	barevný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
mimo	mimo	k7c4	mimo
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
lze	lze	k6eAd1	lze
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
barevném	barevný	k2eAgInSc6d1	barevný
prostoru	prostor	k1gInSc6	prostor
zobrazit	zobrazit	k5eAaPmF	zobrazit
jen	jen	k9	jen
přibližně	přibližně	k6eAd1	přibližně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
přibližnému	přibližný	k2eAgNnSc3d1	přibližné
zobrazení	zobrazení	k1gNnSc3	zobrazení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgFnPc1d1	různá
zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
záměny	záměna	k1gFnPc1	záměna
<g/>
.	.	kIx.	.
</s>
<s>
Barevný	barevný	k2eAgInSc4d1	barevný
gamut	gamut	k1gInSc4	gamut
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
množinu	množina	k1gFnSc4	množina
zobrazitelných	zobrazitelný	k2eAgFnPc2d1	zobrazitelná
hodnot	hodnota	k1gFnPc2	hodnota
z	z	k7c2	z
určitého	určitý	k2eAgInSc2d1	určitý
barevného	barevný	k2eAgInSc2d1	barevný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
počítačového	počítačový	k2eAgNnSc2d1	počítačové
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
zobrazování	zobrazování	k1gNnSc2	zobrazování
či	či	k8xC	či
třeba	třeba	k6eAd1	třeba
tisku	tisk	k1gInSc2	tisk
obrazu	obraz	k1gInSc2	obraz
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
definovány	definován	k2eAgFnPc4d1	definována
absolutní	absolutní	k2eAgFnPc4d1	absolutní
barevné	barevný	k2eAgFnPc4d1	barevná
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
standardizované	standardizovaný	k2eAgFnPc4d1	standardizovaná
prostory	prostora	k1gFnPc4	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bude	být	k5eAaImBp3nS	být
určitě	určitě	k6eAd1	určitě
znát	znát	k5eAaImF	znát
sRGB	sRGB	k?	sRGB
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
také	také	k9	také
Rec	Rec	k1gFnSc1	Rec
<g/>
.	.	kIx.	.
709	[number]	k4	709
nebo	nebo	k8xC	nebo
HDTV	HDTV	kA	HDTV
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vycházející	vycházející	k2eAgFnSc4d1	vycházející
a	a	k8xC	a
širší	široký	k2eAgFnSc4d2	širší
AdobeRGB	AdobeRGB	k1gFnSc4	AdobeRGB
<g/>
.	.	kIx.	.
</s>
<s>
Barevný	barevný	k2eAgInSc1d1	barevný
gamut	gamut	k1gInSc1	gamut
bývá	bývat	k5eAaImIp3nS	bývat
typicky	typicky	k6eAd1	typicky
porovnáván	porovnáván	k2eAgInSc1d1	porovnáván
s	s	k7c7	s
rozsahem	rozsah	k1gInSc7	rozsah
těchto	tento	k3xDgInPc2	tento
standardů	standard	k1gInPc2	standard
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
zobrazených	zobrazený	k2eAgFnPc2d1	zobrazená
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
populární	populární	k2eAgInPc1d1	populární
bity	bit	k1gInPc1	bit
-	-	kIx~	-
"	"	kIx"	"
<g/>
8	[number]	k4	8
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
monitor	monitor	k1gInSc4	monitor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc1	kolik
bitů	bit	k1gInPc2	bit
máte	mít	k5eAaImIp2nP	mít
pro	pro	k7c4	pro
barevnou	barevný	k2eAgFnSc4d1	barevná
složku	složka	k1gFnSc4	složka
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
)	)	kIx)	)
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
množství	množství	k1gNnSc1	množství
zobrazitelných	zobrazitelný	k2eAgInPc2d1	zobrazitelný
odstínů	odstín	k1gInPc2	odstín
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
8	[number]	k4	8
bitů	bit	k1gInPc2	bit
tedy	tedy	k9	tedy
16,7	[number]	k4	16,7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
10	[number]	k4	10
bitů	bit	k1gInPc2	bit
pak	pak	k6eAd1	pak
1,07	[number]	k4	1,07
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
a	a	k8xC	a
plynulost	plynulost	k1gFnSc4	plynulost
přechodů	přechod	k1gInPc2	přechod
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
8	[number]	k4	8
bitů	bit	k1gInPc2	bit
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
256	[number]	k4	256
odstínů	odstín	k1gInPc2	odstín
jedné	jeden	k4xCgFnSc2	jeden
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
10	[number]	k4	10
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
1024	[number]	k4	1024
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gamut	gamut	k1gInSc1	gamut
udává	udávat	k5eAaImIp3nS	udávat
sytost	sytost	k1gFnSc4	sytost
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
mít	mít	k5eAaImF	mít
tedy	tedy	k9	tedy
monitor	monitor	k1gInSc4	monitor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
jen	jen	k6eAd1	jen
tříbitové	tříbitový	k2eAgFnPc4d1	tříbitový
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
široký	široký	k2eAgInSc4d1	široký
gamut	gamut	k1gInSc4	gamut
-	-	kIx~	-
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc4d1	modrá
a	a	k8xC	a
zelenou	zelený	k2eAgFnSc4d1	zelená
klidně	klidně	k6eAd1	klidně
třeba	třeba	k6eAd1	třeba
až	až	k9	až
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
AdobeRGB	AdobeRGB	k1gFnPc2	AdobeRGB
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barevné	barevný	k2eAgFnSc6d1	barevná
reprodukci	reprodukce	k1gFnSc6	reprodukce
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
grafiku	grafika	k1gFnSc4	grafika
a	a	k8xC	a
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
gamut	gamut	k2eAgMnSc1d1	gamut
zajisté	zajisté	k9	zajisté
kompletní	kompletní	k2eAgFnSc7d1	kompletní
podskupinou	podskupina	k1gFnSc7	podskupina
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgNnSc1d3	nejběžnější
využití	využití	k1gNnSc1	využití
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
podskupinu	podskupina	k1gFnSc4	podskupina
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přesně	přesně	k6eAd1	přesně
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
v	v	k7c6	v
daných	daný	k2eAgFnPc6d1	daná
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
barevném	barevný	k2eAgInSc6d1	barevný
modelu	model	k1gInSc6	model
nebo	nebo	k8xC	nebo
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
výstupních	výstupní	k2eAgNnPc6d1	výstupní
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
používaný	používaný	k2eAgInSc1d1	používaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nijak	nijak	k6eAd1	nijak
nesprávný	správný	k2eNgInSc1d1	nesprávný
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
kompletní	kompletní	k2eAgFnSc4d1	kompletní
sadu	sada	k1gFnSc4	sada
barev	barva	k1gFnPc2	barva
nalezenou	nalezený	k2eAgFnSc4d1	nalezená
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
<g/>
,	,	kIx,	,
digitalizace	digitalizace	k1gFnSc1	digitalizace
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
převádění	převádění	k1gNnSc1	převádění
digitalizovaného	digitalizovaný	k2eAgInSc2d1	digitalizovaný
obrazu	obraz	k1gInSc2	obraz
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
barevných	barevný	k2eAgInPc2d1	barevný
modelů	model	k1gInPc2	model
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gInSc4	jejich
výstup	výstup	k1gInSc4	výstup
do	do	k7c2	do
dané	daný	k2eAgFnSc2d1	daná
jakosti	jakost	k1gFnSc2	jakost
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgFnSc4d1	využívající
určité	určitý	k2eAgNnSc4d1	určité
výstupní	výstupní	k2eAgNnSc4d1	výstupní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
změní	změnit	k5eAaPmIp3nS	změnit
jejich	jejich	k3xOp3gInSc1	jejich
gamut	gamut	k1gInSc1	gamut
<g/>
,	,	kIx,	,
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
barvy	barva	k1gFnPc4	barva
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
obrazu	obraz	k1gInSc2	obraz
se	se	k3xPyFc4	se
ztratí	ztratit	k5eAaPmIp3nS	ztratit
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Gamut	Gamut	k1gMnSc1	Gamut
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
barvy	barva	k1gFnPc4	barva
je	být	k5eAaImIp3nS	být
dané	daný	k2eAgNnSc1d1	dané
zařízení	zařízení	k1gNnSc1	zařízení
schopné	schopný	k2eAgNnSc1d1	schopné
zobrazit	zobrazit	k5eAaPmF	zobrazit
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
o	o	k7c6	o
barvě	barva	k1gFnSc6	barva
je	být	k5eAaImIp3nS	být
gamut	gamut	k1gInSc4	gamut
nějakého	nějaký	k3yIgNnSc2	nějaký
zařízení	zařízení	k1gNnSc2	zařízení
nebo	nebo	k8xC	nebo
postupu	postup	k1gInSc6	postup
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
nebo	nebo	k8xC	nebo
reprodukována	reprodukovat	k5eAaBmNgFnS	reprodukovat
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
barevný	barevný	k2eAgInSc1d1	barevný
gamut	gamut	k1gInSc1	gamut
specifikován	specifikovat	k5eAaBmNgInS	specifikovat
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
odstín	odstín	k1gInSc1	odstín
<g/>
-	-	kIx~	-
<g/>
sytost	sytost	k1gFnSc1	sytost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgNnPc1d1	mnohé
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
vytvořit	vytvořit	k5eAaPmF	vytvořit
barvy	barva	k1gFnPc4	barva
s	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
rozsahem	rozsah	k1gInSc7	rozsah
intenzity	intenzita	k1gFnSc2	intenzita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jejich	jejich	k3xOp3gInSc2	jejich
barevného	barevný	k2eAgInSc2d1	barevný
gamutu	gamut	k1gInSc2	gamut
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
pro	pro	k7c4	pro
odčítací	odčítací	k2eAgNnPc4d1	odčítací
barevná	barevný	k2eAgNnPc4d1	barevné
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
polygrafická	polygrafický	k2eAgFnSc1d1	polygrafická
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
intenzity	intenzita	k1gFnSc2	intenzita
dostupné	dostupný	k2eAgFnSc2d1	dostupná
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
zařízení	zařízení	k1gNnSc6	zařízení
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
částí	část	k1gFnPc2	část
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
určité	určitý	k2eAgFnPc1d1	určitá
barvy	barva	k1gFnPc1	barva
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
zobrazeny	zobrazit	k5eAaPmNgInP	zobrazit
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
určitého	určitý	k2eAgInSc2d1	určitý
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
barvy	barva	k1gFnPc1	barva
mimo	mimo	k7c4	mimo
gamut	gamut	k1gInSc4	gamut
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
čistá	čistý	k2eAgFnSc1d1	čistá
červená	červená	k1gFnSc1	červená
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
barevném	barevný	k2eAgInSc6d1	barevný
modelu	model	k1gInSc6	model
RGB	RGB	kA	RGB
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
mimo	mimo	k7c4	mimo
gamut	gamut	k1gInSc4	gamut
<g/>
"	"	kIx"	"
v	v	k7c6	v
barevném	barevný	k2eAgInSc6d1	barevný
modelu	model	k1gInSc6	model
CMYK	CMYK	kA	CMYK
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
přenést	přenést	k5eAaPmF	přenést
(	(	kIx(	(
<g/>
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
<g/>
)	)	kIx)	)
veškeré	veškerý	k3xTgNnSc4	veškerý
viditelné	viditelný	k2eAgNnSc4d1	viditelné
barevné	barevný	k2eAgNnSc4d1	barevné
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
něco	něco	k6eAd1	něco
jako	jako	k8xC	jako
Svatý	svatý	k2eAgInSc1d1	svatý
grál	grál	k1gInSc1	grál
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
barevných	barevný	k2eAgInPc2d1	barevný
displejů	displej	k1gInPc2	displej
a	a	k8xC	a
v	v	k7c6	v
polygrafických	polygrafický	k2eAgInPc6d1	polygrafický
procesech	proces	k1gInPc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
moderní	moderní	k2eAgFnPc1d1	moderní
techniky	technika	k1gFnPc1	technika
umožní	umožnit	k5eAaPmIp3nP	umožnit
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
a	a	k8xC	a
lepší	dobrý	k2eAgNnSc4d2	lepší
přiblížení	přiblížení	k1gNnSc4	přiblížení
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
složitost	složitost	k1gFnSc1	složitost
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
dělá	dělat	k5eAaImIp3nS	dělat
neproveditelnými	proveditelný	k2eNgInPc7d1	neproveditelný
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dost	dost	k6eAd1	dost
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určováno	určovat	k5eAaImNgNnS	určovat
hranicemi	hranice	k1gFnPc7	hranice
lidského	lidský	k2eAgNnSc2d1	lidské
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
gamut	gamut	k1gInSc1	gamut
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znamenal	znamenat	k5eAaImAgInS	znamenat
sadu	sada	k1gFnSc4	sada
výšek	výška	k1gFnPc2	výška
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
set	set	k1gInSc1	set
of	of	k?	of
pitches	pitches	k1gInSc1	pitches
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yRgFnPc2	který
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgFnSc1d1	hudební
melodie	melodie	k1gFnSc1	melodie
složena	složit	k5eAaPmNgFnS	složit
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeareovo	Shakespeareův	k2eAgNnSc1d1	Shakespeareovo
použití	použití	k1gNnSc1	použití
termínu	termín	k1gInSc2	termín
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Zkrocení	zkrocení	k1gNnSc2	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
1594	[number]	k4	1594
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
přisuzováno	přisuzován	k2eAgNnSc1d1	přisuzováno
jinému	jiný	k2eAgMnSc3d1	jiný
autorovi-muzikantovi	autoroviuzikant	k1gMnSc3	autorovi-muzikant
<g/>
,	,	kIx,	,
Thomasu	Thomasu	k?	Thomasu
Morleymu	Morleym	k1gInSc6	Morleym
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
škály	škála	k1gFnSc2	škála
barev	barva	k1gFnPc2	barva
nebo	nebo	k8xC	nebo
odstínů	odstín	k1gInPc2	odstín
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Thomasem	Thomas	k1gMnSc7	Thomas
De	De	k?	De
Quincey	Quincea	k1gMnSc2	Quincea
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Porphyry	Porphyr	k1gInPc1	Porphyr
<g/>
,	,	kIx,	,
I	i	k9	i
have	have	k6eAd1	have
heard	heard	k6eAd1	heard
<g/>
,	,	kIx,	,
runs	runs	k6eAd1	runs
through	through	k1gInSc1	through
as	as	k1gNnSc2	as
large	large	k1gInSc1	large
a	a	k8xC	a
gamut	gamut	k1gInSc1	gamut
of	of	k?	of
hues	hues	k6eAd1	hues
as	as	k9	as
marble	marble	k6eAd1	marble
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
digitálního	digitální	k2eAgInSc2d1	digitální
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
nejvýhodnějším	výhodný	k2eAgInSc7d3	nejvýhodnější
barevným	barevný	k2eAgInSc7d1	barevný
modelem	model	k1gInSc7	model
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
model	modla	k1gFnPc2	modla
RGB	RGB	kA	RGB
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vytisknutím	vytisknutí	k1gNnSc7	vytisknutí
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
převést	převést	k5eAaPmF	převést
obraz	obraz	k1gInSc4	obraz
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
RGB	RGB	kA	RGB
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
na	na	k7c4	na
tiskařský	tiskařský	k2eAgInSc4d1	tiskařský
model	model	k1gInSc4	model
CMYK	CMYK	kA	CMYK
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
barvy	barva	k1gFnPc4	barva
z	z	k7c2	z
RGB	RGB	kA	RGB
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
mimo	mimo	k7c4	mimo
gamut	gamut	k1gInSc4	gamut
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
převedeny	převeden	k2eAgInPc1d1	převeden
do	do	k7c2	do
přibližných	přibližný	k2eAgFnPc2d1	přibližná
hodnot	hodnota	k1gFnPc2	hodnota
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
gamutu	gamut	k1gInSc2	gamut
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
CMYK	CMYK	kA	CMYK
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchým	jednoduchý	k2eAgNnSc7d1	jednoduché
seříznutím	seříznutí	k1gNnSc7	seříznutí
těch	ten	k3xDgFnPc2	ten
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
gamut	gamut	k1gInSc4	gamut
<g/>
,	,	kIx,	,
k	k	k7c3	k
těm	ten	k3xDgFnPc3	ten
nejbližším	blízký	k2eAgFnPc3d3	nejbližší
barvám	barva	k1gFnPc3	barva
v	v	k7c6	v
určeném	určený	k2eAgNnSc6d1	určené
místě	místo	k1gNnSc6	místo
lze	lze	k6eAd1	lze
vylepšit	vylepšit	k5eAaPmF	vylepšit
obraz	obraz	k1gInSc4	obraz
(	(	kIx(	(
<g/>
doostření	doostření	k1gNnSc4	doostření
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgInSc4d2	lepší
kontrast	kontrast	k1gInSc4	kontrast
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
algoritmů	algoritmus	k1gInPc2	algoritmus
přibližujících	přibližující	k2eAgInPc2d1	přibližující
tuto	tento	k3xDgFnSc4	tento
přeměnu	přeměna	k1gFnSc4	přeměna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
úplně	úplně	k6eAd1	úplně
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
jednoduše	jednoduše	k6eAd1	jednoduše
mimo	mimo	k7c4	mimo
schopnosti	schopnost	k1gFnPc4	schopnost
cílového	cílový	k2eAgNnSc2d1	cílové
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
proto	proto	k8xC	proto
správné	správný	k2eAgNnSc4d1	správné
určení	určení	k1gNnSc4	určení
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
gamut	gamut	k1gInSc4	gamut
cílového	cílový	k2eAgInSc2d1	cílový
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
možné	možný	k2eAgNnSc1d1	možné
během	během	k7c2	během
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
pro	pro	k7c4	pro
kvalitu	kvalita	k1gFnSc4	kvalita
konečného	konečný	k2eAgInSc2d1	konečný
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Gamuty	Gamut	k1gInPc1	Gamut
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
představovány	představován	k2eAgFnPc4d1	představována
jako	jako	k8xC	jako
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
CIE	CIE	kA	CIE
1931	[number]	k4	1931
kolorimetrickém	kolorimetrický	k2eAgInSc6d1	kolorimetrický
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ukázán	ukázat	k5eAaPmNgInS	ukázat
napravo	napravo	k6eAd1	napravo
<g/>
,	,	kIx,	,
se	s	k7c7	s
zakřivenými	zakřivený	k2eAgInPc7d1	zakřivený
okraji	okraj	k1gInPc7	okraj
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgFnPc4d1	vyjadřující
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Gamutové	Gamutový	k2eAgFnPc1d1	Gamutový
oblasti	oblast	k1gFnPc1	oblast
obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejbarevnější	barevný	k2eAgFnSc1d3	nejbarevnější
reprodukce	reprodukce	k1gFnSc1	reprodukce
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
základními	základní	k2eAgFnPc7d1	základní
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
dosažitelný	dosažitelný	k2eAgMnSc1d1	dosažitelný
gamut	gamut	k1gMnSc1	gamut
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jasu	jas	k1gInSc6	jas
<g/>
;	;	kIx,	;
plný	plný	k2eAgInSc4d1	plný
gamut	gamut	k1gInSc4	gamut
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
předváděn	předváděn	k2eAgMnSc1d1	předváděn
v	v	k7c6	v
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
dole	dole	k6eAd1	dole
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc1	obraz
nalevo	nalevo	k6eAd1	nalevo
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
gamuty	gamut	k1gInPc1	gamut
RGB	RGB	kA	RGB
barevného	barevný	k2eAgInSc2d1	barevný
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
na	na	k7c6	na
monitorech	monitor	k1gInPc6	monitor
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
a	a	k8xC	a
reflexních	reflexní	k2eAgFnPc2d1	reflexní
barev	barva	k1gFnPc2	barva
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
dole	dole	k6eAd1	dole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kužel	kužel	k1gInSc4	kužel
nakreslený	nakreslený	k2eAgInSc4d1	nakreslený
v	v	k7c6	v
šedivém	šedivý	k2eAgMnSc6d1	šedivý
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
CIE	CIE	kA	CIE
diagramem	diagram	k1gInSc7	diagram
napravo	napravo	k6eAd1	napravo
<g/>
,	,	kIx,	,
jas	jas	k1gInSc1	jas
je	být	k5eAaImIp3nS	být
zvětšený	zvětšený	k2eAgInSc1d1	zvětšený
<g/>
.	.	kIx.	.
</s>
<s>
Osy	osa	k1gFnPc1	osa
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
diagramech	diagram	k1gInPc6	diagram
jsou	být	k5eAaImIp3nP	být
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
za	za	k7c4	za
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
čípky	čípek	k1gInPc4	čípek
sítnice	sítnice	k1gFnSc2	sítnice
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
oku	oko	k1gNnSc6	oko
při	při	k7c6	při
short-wavelength	shortavelength	k1gMnSc1	short-wavelength
(	(	kIx(	(
<g/>
krátké	krátký	k2eAgFnSc6d1	krátká
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
middle-wavelength	middleavelength	k1gInSc1	middle-wavelength
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc6d1	střední
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
)	)	kIx)	)
a	a	k8xC	a
long-wavelength	longavelength	k1gInSc1	long-wavelength
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
písmena	písmeno	k1gNnPc1	písmeno
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
black	black	k1gInSc4	black
(	(	kIx(	(
<g/>
černou	černý	k2eAgFnSc4d1	černá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
red	red	k?	red
(	(	kIx(	(
<g/>
červenou	červený	k2eAgFnSc4d1	červená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
green	green	k1gInSc4	green
(	(	kIx(	(
<g/>
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
blue	bluat	k5eAaPmIp3nS	bluat
(	(	kIx(	(
<g/>
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyan	cyan	k1gInSc4	cyan
(	(	kIx(	(
<g/>
modrozelenou	modrozelený	k2eAgFnSc4d1	modrozelená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
magenta	magento	k1gNnSc2	magento
(	(	kIx(	(
<g/>
fuchsiovou	fuchsiový	k2eAgFnSc7d1	fuchsiová
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
yellow	yellow	k?	yellow
(	(	kIx(	(
<g/>
žlutou	žlutý	k2eAgFnSc7d1	žlutá
<g/>
)	)	kIx)	)
a	a	k8xC	a
white	white	k5eAaPmIp2nP	white
(	(	kIx(	(
<g/>
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
)	)	kIx)	)
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Tyto	tento	k3xDgInPc1	tento
obrázky	obrázek	k1gInPc1	obrázek
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Gamut	Gamut	k1gInSc1	Gamut
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
CMYK	CMYK	kA	CMYK
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
jako	jako	k8xS	jako
gamut	gamut	k2eAgInSc4d1	gamut
pro	pro	k7c4	pro
RGB	RGB	kA	RGB
<g/>
,	,	kIx,	,
s	s	k7c7	s
mírně	mírně	k6eAd1	mírně
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
,	,	kIx,	,
závisejícími	závisející	k2eAgInPc7d1	závisející
jak	jak	k8xC	jak
na	na	k7c6	na
přesných	přesný	k2eAgFnPc6d1	přesná
vlastnostech	vlastnost	k1gFnPc6	vlastnost
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
světelné	světelný	k2eAgFnSc6d1	světelná
složce	složka	k1gFnSc6	složka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
způsobu	způsob	k1gInSc3	způsob
rastrového	rastrový	k2eAgInSc2d1	rastrový
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc1	barva
reagují	reagovat	k5eAaBmIp3nP	reagovat
vzájemně	vzájemně	k6eAd1	vzájemně
a	a	k8xC	a
s	s	k7c7	s
papírem	papír	k1gInSc7	papír
a	a	k8xC	a
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
neideální	ideální	k2eNgFnSc3d1	neideální
spektrální	spektrální	k2eAgFnSc3d1	spektrální
absorpci	absorpce	k1gFnSc3	absorpce
je	být	k5eAaImIp3nS	být
gamut	gamut	k2eAgInSc1d1	gamut
menší	malý	k2eAgInSc1d2	menší
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zakulacené	zakulacený	k2eAgInPc4d1	zakulacený
rohy	roh	k1gInPc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Gamut	Gamut	k1gInSc1	Gamut
reflexních	reflexní	k2eAgFnPc2d1	reflexní
barev	barva	k1gFnPc2	barva
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
má	mít	k5eAaImIp3nS	mít
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
,	,	kIx,	,
jen	jen	k9	jen
více	hodně	k6eAd2	hodně
zakulacený	zakulacený	k2eAgInSc4d1	zakulacený
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
jenom	jenom	k9	jenom
úzký	úzký	k2eAgInSc1d1	úzký
pás	pás	k1gInSc1	pás
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
barvu	barva	k1gFnSc4	barva
blízkou	blízký	k2eAgFnSc4d1	blízká
hraně	hraně	k6eAd1	hraně
z	z	k7c2	z
CIE	CIE	kA	CIE
diagramu	diagram	k1gInSc2	diagram
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
současně	současně	k6eAd1	současně
velice	velice	k6eAd1	velice
nízkou	nízký	k2eAgFnSc4d1	nízká
jasovou	jasový	k2eAgFnSc4d1	jasová
složku	složka	k1gFnSc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
jasnosti	jasnost	k1gFnSc6	jasnost
je	být	k5eAaImIp3nS	být
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
CIE	CIE	kA	CIE
diagramu	diagram	k1gInSc6	diagram
menší	malý	k2eAgFnPc4d2	menší
a	a	k8xC	a
menší	malý	k2eAgFnPc4d2	menší
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
samotnému	samotný	k2eAgInSc3d1	samotný
bodu	bod	k1gInSc3	bod
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
všechny	všechen	k3xTgFnPc4	všechen
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
jsou	být	k5eAaImIp3nP	být
odráženy	odrážet	k5eAaImNgFnP	odrážet
na	na	k7c4	na
100	[number]	k4	100
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc4d1	přesná
souřadnice	souřadnice	k1gFnPc4	souřadnice
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
jsou	být	k5eAaImIp3nP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
určeny	určit	k5eAaPmNgInP	určit
barvou	barva	k1gFnSc7	barva
světelného	světelný	k2eAgInSc2d1	světelný
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Barevný	barevný	k2eAgInSc1d1	barevný
gamut	gamut	k1gInSc1	gamut
většiny	většina	k1gFnSc2	většina
řádů	řád	k1gInPc2	řád
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozuměn	rozuměn	k2eAgMnSc1d1	rozuměn
jako	jako	k8xC	jako
výsledek	výsledek	k1gInSc1	výsledek
potíží	potíž	k1gFnPc2	potíž
vytvářejících	vytvářející	k2eAgInPc2d1	vytvářející
čisté	čistý	k2eAgNnSc4d1	čisté
světlo	světlo	k1gNnSc4	světlo
jedné	jeden	k4xCgFnSc2	jeden
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
jednobarevného	jednobarevný	k2eAgMnSc2d1	jednobarevný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
technologickým	technologický	k2eAgInSc7d1	technologický
zdrojem	zdroj	k1gInSc7	zdroj
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
<g/>
)	)	kIx)	)
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
drahý	drahý	k2eAgInSc1d1	drahý
a	a	k8xC	a
nepraktický	praktický	k2eNgInSc1d1	nepraktický
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
laserová	laserový	k2eAgFnSc1d1	laserová
technologie	technologie	k1gFnSc1	technologie
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
přístupnější	přístupný	k2eAgMnSc1d2	přístupnější
a	a	k8xC	a
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
cena	cena	k1gFnSc1	cena
již	již	k6eAd1	již
nebude	být	k5eNaImBp3nS	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
nevyužití	nevyužití	k1gNnSc4	nevyužití
laseru	laser	k1gInSc2	laser
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
jiných	jiný	k2eAgInPc6d1	jiný
než	než	k8xS	než
laserových	laserový	k2eAgInPc6d1	laserový
<g/>
,	,	kIx,	,
zpodobňuje	zpodobňovat	k5eAaImIp3nS	zpodobňovat
vysoce	vysoce	k6eAd1	vysoce
syté	sytý	k2eAgFnPc4d1	sytá
barvy	barva	k1gFnPc4	barva
s	s	k7c7	s
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
hrubým	hrubý	k2eAgNnSc7d1	hrubé
přiblížením	přiblížení	k1gNnSc7	přiblížení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
světlo	světlo	k1gNnSc4	světlo
s	s	k7c7	s
rozsahem	rozsah	k1gInSc7	rozsah
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
odstínech	odstín	k1gInPc6	odstín
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
druhých	druhý	k4xOgInPc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
používají	používat	k5eAaImIp3nP	používat
aditivní	aditivní	k2eAgNnPc1d1	aditivní
míchání	míchání	k1gNnPc1	míchání
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
doplňkové	doplňkový	k2eAgInPc1d1	doplňkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
barevný	barevný	k2eAgInSc4d1	barevný
gamut	gamut	k1gInSc4	gamut
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
konvexního	konvexní	k2eAgInSc2d1	konvexní
mnohoúhelníku	mnohoúhelník	k1gInSc2	mnohoúhelník
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
odstín	odstín	k1gInSc1	odstín
<g/>
,	,	kIx,	,
jas	jas	k1gInSc1	jas
a	a	k8xC	a
sytost	sytost	k1gFnSc1	sytost
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholy	vrchol	k1gInPc1	vrchol
mnohoúhelníku	mnohoúhelník	k1gInSc2	mnohoúhelník
mají	mít	k5eAaImIp3nP	mít
tu	ten	k3xDgFnSc4	ten
nejsytější	sytý	k2eAgFnSc4d3	nejsytější
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zařízení	zařízení	k1gNnSc1	zařízení
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zařízení	zařízení	k1gNnSc2	zařízení
s	s	k7c7	s
subtraktivním	subtraktivní	k2eAgInSc7d1	subtraktivní
míchání	míchání	k1gNnSc4	míchání
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
barevný	barevný	k2eAgInSc1d1	barevný
gamut	gamut	k1gInSc1	gamut
častěji	často	k6eAd2	často
v	v	k7c6	v
nepravidelném	pravidelný	k2eNgInSc6d1	nepravidelný
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
seznam	seznam	k1gInSc4	seznam
představující	představující	k2eAgNnPc1d1	představující
barevná	barevný	k2eAgNnPc1d1	barevné
zařízení	zařízení	k1gNnPc1	zařízení
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
od	od	k7c2	od
velkého	velký	k2eAgMnSc2d1	velký
k	k	k7c3	k
malému	malý	k2eAgInSc3d1	malý
barevnému	barevný	k2eAgInSc3d1	barevný
gamutu	gamut	k1gInSc3	gamut
<g/>
:	:	kIx,	:
Laserový	laserový	k2eAgInSc1d1	laserový
video	video	k1gNnSc1	video
projektor	projektor	k1gInSc1	projektor
využívá	využívat	k5eAaImIp3nS	využívat
třech	tři	k4xCgFnPc6	tři
laserů	laser	k1gInPc2	laser
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nejširšího	široký	k2eAgInSc2d3	nejširší
gamutu	gamut	k1gInSc2	gamut
<g/>
,	,	kIx,	,
možného	možný	k2eAgNnSc2d1	možné
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
zobrazovacích	zobrazovací	k2eAgFnPc6d1	zobrazovací
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
,	,	kIx,	,
odvozený	odvozený	k2eAgMnSc1d1	odvozený
od	od	k7c2	od
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
lasery	laser	k1gInPc1	laser
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
věrně	věrně	k6eAd1	věrně
monochromatické	monochromatický	k2eAgFnPc1d1	monochromatická
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
pracuje	pracovat	k5eAaImIp3nS	pracovat
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
skenování	skenování	k1gNnSc2	skenování
celého	celý	k2eAgInSc2d1	celý
obrázku	obrázek	k1gInSc2	obrázek
<g/>
,	,	kIx,	,
všech	všecek	k3xTgInPc2	všecek
bodů	bod	k1gInPc2	bod
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
působení	působení	k1gNnSc2	působení
laseru	laser	k1gInSc2	laser
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
frekvenci	frekvence	k1gFnSc6	frekvence
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
elektronové	elektronový	k2eAgInPc1d1	elektronový
paprsky	paprsek	k1gInPc1	paprsek
v	v	k7c6	v
CRT	CRT	kA	CRT
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
pomocí	pomocí	k7c2	pomocí
optického	optický	k2eAgNnSc2d1	optické
rozprostírání	rozprostírání	k1gNnSc2	rozprostírání
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
působení	působení	k1gNnSc2	působení
laseru	laser	k1gInSc2	laser
a	a	k8xC	a
skenování	skenování	k1gNnSc2	skenování
linek	linka	k1gFnPc2	linka
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
linky	linka	k1gFnPc1	linka
samy	sám	k3xTgFnPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
budou	být	k5eAaImBp3nP	být
působit	působit	k5eAaImF	působit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
Digital	Digital	kA	Digital
Light	Light	k2eAgInSc1d1	Light
Processing	Processing	k1gInSc1	Processing
<g/>
.	.	kIx.	.
</s>
<s>
Fotografický	fotografický	k2eAgInSc1d1	fotografický
film	film	k1gInSc1	film
může	moct	k5eAaImIp3nS	moct
zobrazit	zobrazit	k5eAaPmF	zobrazit
větší	veliký	k2eAgInSc1d2	veliký
barevný	barevný	k2eAgInSc1d1	barevný
gamut	gamut	k1gInSc1	gamut
než	než	k8xS	než
typický	typický	k2eAgInSc1d1	typický
televizor	televizor	k1gInSc1	televizor
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
nebo	nebo	k8xC	nebo
systém	systém	k1gInSc1	systém
domácího	domácí	k2eAgNnSc2d1	domácí
kina	kino	k1gNnSc2	kino
Laserová	laserový	k2eAgFnSc1d1	laserová
světelná	světelný	k2eAgFnSc1d1	světelná
show	show	k1gFnSc1	show
využívá	využívat	k5eAaImIp3nS	využívat
lasery	laser	k1gInPc4	laser
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
téměř	téměř	k6eAd1	téměř
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
barev	barva	k1gFnPc2	barva
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
nasycených	nasycený	k2eAgFnPc2d1	nasycená
než	než	k8xS	než
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
míchání	míchání	k1gNnSc2	míchání
odstínů	odstín	k1gInPc2	odstín
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
méně	málo	k6eAd2	málo
sytých	sytý	k2eAgInPc2d1	sytý
odstínů	odstín	k1gInPc2	odstín
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
taková	takový	k3xDgNnPc1	takový
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
komplikovaná	komplikovaný	k2eAgNnPc1d1	komplikované
<g/>
,	,	kIx,	,
drahá	drahý	k2eAgNnPc1d1	drahé
a	a	k8xC	a
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Obrazovka	obrazovka	k1gFnSc1	obrazovka
(	(	kIx(	(
<g/>
CRT	CRT	kA	CRT
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobné	podobný	k2eAgNnSc4d1	podobné
video	video	k1gNnSc4	video
displeje	displej	k1gInSc2	displej
mají	mít	k5eAaImIp3nP	mít
zhruba	zhruba	k6eAd1	zhruba
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
barevný	barevný	k2eAgInSc4d1	barevný
gamut	gamut	k1gInSc4	gamut
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
část	část	k1gFnSc4	část
viditelného	viditelný	k2eAgNnSc2d1	viditelné
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obrazovek	obrazovka	k1gFnPc2	obrazovka
je	být	k5eAaImIp3nS	být
omezení	omezení	k1gNnSc1	omezení
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
luminoforu	luminofor	k1gInSc3	luminofor
uvnitř	uvnitř	k7c2	uvnitř
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
červené	červený	k2eAgFnPc4d1	červená
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc4d1	zelená
a	a	k8xC	a
modré	modrý	k2eAgNnSc1d1	modré
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
