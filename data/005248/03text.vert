<s>
ZFS	ZFS	kA	ZFS
(	(	kIx(	(
<g/>
Zettabyte	Zettabyt	k1gInSc5	Zettabyt
File	File	k1gFnSc4	File
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
a	a	k8xC	a
správce	správce	k1gMnSc1	správce
logických	logický	k2eAgInPc2d1	logický
svazků	svazek	k1gInPc2	svazek
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
společností	společnost	k1gFnSc7	společnost
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Solaris	Solaris	k1gFnSc2	Solaris
<g/>
.	.	kIx.	.
</s>
<s>
ZFS	ZFS	kA	ZFS
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
funkce	funkce	k1gFnPc4	funkce
pro	pro	k7c4	pro
ověřování	ověřování	k1gNnSc4	ověřování
integrity	integrita	k1gFnSc2	integrita
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
velkých	velký	k2eAgInPc2d1	velký
objemů	objem	k1gInPc2	objem
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
integraci	integrace	k1gFnSc3	integrace
konceptů	koncept	k1gInPc2	koncept
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
zaznamenávání	zaznamenávání	k1gNnSc4	zaznamenávání
a	a	k8xC	a
ukládání	ukládání	k1gNnSc4	ukládání
aktuálního	aktuální	k2eAgInSc2d1	aktuální
stavu	stav	k1gInSc2	stav
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
bod	bod	k1gInSc1	bod
obnovy	obnova	k1gFnSc2	obnova
u	u	k7c2	u
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ověřování	ověřování	k1gNnSc1	ověřování
integrity	integrita	k1gFnSc2	integrita
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
opravy	oprava	k1gFnPc4	oprava
za	za	k7c2	za
chodu	chod	k1gInSc2	chod
<g/>
,	,	kIx,	,
RAID-Z	RAID-Z	k1gFnSc1	RAID-Z
a	a	k8xC	a
přirozená	přirozený	k2eAgFnSc1d1	přirozená
podpora	podpora	k1gFnSc1	podpora
NFSv	NFSva	k1gFnPc2	NFSva
<g/>
4	[number]	k4	4
ACLs	ACLsa	k1gFnPc2	ACLsa
<g/>
.	.	kIx.	.
</s>
<s>
ZFS	ZFS	kA	ZFS
je	být	k5eAaImIp3nS	být
implementováno	implementován	k2eAgNnSc1d1	implementováno
jako	jako	k8xC	jako
open-source	openourka	k1gFnSc6	open-sourka
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
licencováno	licencován	k2eAgNnSc1d1	licencováno
pod	pod	k7c4	pod
Common	Common	k1gInSc4	Common
Development	Development	k1gMnSc1	Development
and	and	k?	and
Distribution	Distribution	k1gInSc1	Distribution
License	License	k1gFnSc1	License
(	(	kIx(	(
<g/>
CDDL	CDDL	kA	CDDL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ZFS	ZFS	kA	ZFS
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
registrovanou	registrovaný	k2eAgFnSc7d1	registrovaná
obchodní	obchodní	k2eAgFnSc7d1	obchodní
značkou	značka	k1gFnSc7	značka
firmy	firma	k1gFnSc2	firma
Oracle	Oracle	k1gFnSc2	Oracle
<g/>
.	.	kIx.	.
</s>
<s>
ZFS	ZFS	kA	ZFS
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
a	a	k8xC	a
implementován	implementovat	k5eAaImNgInS	implementovat
týmem	tým	k1gInSc7	tým
společnosti	společnost	k1gFnSc2	společnost
Sun	Sun	kA	Sun
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
Jeff	Jeff	k1gInSc4	Jeff
Bonwick	Bonwicka	k1gFnPc2	Bonwicka
<g/>
.	.	kIx.	.
</s>
<s>
ZFS	ZFS	kA	ZFS
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
ZFS	ZFS	kA	ZFS
byl	být	k5eAaImAgInS	být
integrován	integrovat	k5eAaBmNgInS	integrovat
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
vývojového	vývojový	k2eAgInSc2d1	vývojový
kmene	kmen	k1gInSc2	kmen
Solaris	Solaris	k1gFnSc2	Solaris
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
a	a	k8xC	a
uvolněn	uvolněn	k2eAgInSc1d1	uvolněn
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
OpenSolarisu	OpenSolaris	k1gInSc2	OpenSolaris
(	(	kIx(	(
<g/>
build	build	k1gInSc1	build
27	[number]	k4	27
<g/>
)	)	kIx)	)
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Sun	sun	k1gInSc1	sun
oznámil	oznámit	k5eAaPmAgInS	oznámit
ZFS	ZFS	kA	ZFS
do	do	k7c2	do
aktualizace	aktualizace	k1gFnSc2	aktualizace
na	na	k7c6	na
verzi	verze	k1gFnSc6	verze
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
pro	pro	k7c4	pro
Solaris	Solaris	k1gInSc4	Solaris
10	[number]	k4	10
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
komunity	komunita	k1gFnSc2	komunita
Open	Opena	k1gFnPc2	Opena
Solaris	Solaris	k1gFnSc2	Solaris
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
zní	znět	k5eAaImIp3nS	znět
Zettabyte	Zettabyt	k1gInSc5	Zettabyt
File	Fil	k1gInPc4	Fil
System	Systo	k1gNnSc7	Systo
<g/>
.	.	kIx.	.
</s>
<s>
ZFS	ZFS	kA	ZFS
umí	umět	k5eAaImIp3nS	umět
uložit	uložit	k5eAaPmF	uložit
až	až	k9	až
256	[number]	k4	256
kvadrilionů	kvadrilion	k4xCgInPc2	kvadrilion
Zettabytů	Zettabyt	k1gInPc2	Zettabyt
(	(	kIx(	(
<g/>
1	[number]	k4	1
ZB	ZB	kA	ZB
=	=	kIx~	=
270	[number]	k4	270
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
verze	verze	k1gFnPc1	verze
ZFS	ZFS	kA	ZFS
<g/>
:	:	kIx,	:
10	[number]	k4	10
–	–	k?	–
podporováno	podporovat	k5eAaImNgNnS	podporovat
Solaris	Solaris	k1gFnSc7	Solaris
10	[number]	k4	10
U7	U7	k1gFnPc2	U7
14	[number]	k4	14
–	–	k?	–
podporováno	podporovat	k5eAaImNgNnS	podporovat
OpenSolaris	OpenSolaris	k1gFnSc7	OpenSolaris
2009.06	[number]	k4	2009.06
<g/>
,	,	kIx,	,
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
8.1	[number]	k4	8.1
15	[number]	k4	15
–	–	k?	–
podporováno	podporovat	k5eAaImNgNnS	podporovat
Solaris	Solaris	k1gFnSc7	Solaris
10	[number]	k4	10
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
8.2	[number]	k4	8.2
17	[number]	k4	17
–	–	k?	–
Triple	tripl	k1gInSc5	tripl
Parity	parita	k1gFnPc4	parita
RAID-Z	RAID-Z	k1gFnSc4	RAID-Z
19	[number]	k4	19
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
podporováno	podporovat	k5eAaImNgNnS	podporovat
Solaris	Solaris	k1gFnSc7	Solaris
10	[number]	k4	10
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
21	[number]	k4	21
–	–	k?	–
Deduplikace	Deduplikace	k1gFnSc1	Deduplikace
22	[number]	k4	22
–	–	k?	–
Solaris	Solaris	k1gFnSc6	Solaris
10	[number]	k4	10
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
28	[number]	k4	28
–	–	k?	–
FreeBSD	FreeBSD	k1gFnPc2	FreeBSD
9.0	[number]	k4	9.0
<g/>
,	,	kIx,	,
OpenIndiana	OpenIndiana	k1gFnSc1	OpenIndiana
<g/>
/	/	kIx~	/
<g/>
Illumos	Illumos	k1gInSc1	Illumos
<g/>
,	,	kIx,	,
ZFSOnLinux	ZFSOnLinux	k1gInSc1	ZFSOnLinux
<g/>
,	,	kIx,	,
ZFS-FUSE	ZFS-FUSE	k1gFnSc1	ZFS-FUSE
29	[number]	k4	29
–	–	k?	–
Solaris	Solaris	k1gFnSc6	Solaris
10	[number]	k4	10
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
30	[number]	k4	30
–	–	k?	–
podpora	podpora	k1gFnSc1	podpora
šifrování	šifrování	k1gNnSc2	šifrování
–	–	k?	–
není	být	k5eNaImIp3nS	být
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
s	s	k7c7	s
otevřenými	otevřený	k2eAgFnPc7d1	otevřená
implementacemi	implementace	k1gFnPc7	implementace
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
zdroji	zdroj	k1gInSc6	zdroj
pro	pro	k7c4	pro
licencovaný	licencovaný	k2eAgInSc4d1	licencovaný
Solaris	Solaris	k1gInSc4	Solaris
11	[number]	k4	11
Express	express	k1gInSc1	express
release	release	k6eAd1	release
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
ZFS	ZFS	kA	ZFS
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc3	jeho
vytvoření	vytvoření	k1gNnSc3	vytvoření
od	od	k7c2	od
základu	základ	k1gInSc2	základ
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
integritu	integrita	k1gFnSc4	integrita
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
uživatelská	uživatelský	k2eAgNnPc4d1	Uživatelské
data	datum	k1gNnPc4	datum
na	na	k7c6	na
disku	disk	k1gInSc6	disk
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
nenápadným	nápadný	k2eNgNnSc7d1	nenápadné
poškozením	poškození	k1gNnSc7	poškození
<g/>
,	,	kIx,	,
způsobeným	způsobený	k2eAgMnSc7d1	způsobený
například	například	k6eAd1	například
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
radiací	radiace	k1gFnSc7	radiace
<g/>
,	,	kIx,	,
chybami	chyba	k1gFnPc7	chyba
v	v	k7c6	v
ovladači	ovladač	k1gInSc6	ovladač
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
falešnými	falešný	k2eAgInPc7d1	falešný
zápisy	zápis	k1gInPc7	zápis
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Integrita	integrita	k1gFnSc1	integrita
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
vysokou	vysoký	k2eAgFnSc7d1	vysoká
prioritou	priorita	k1gFnSc7	priorita
v	v	k7c6	v
ZFS	ZFS	kA	ZFS
protože	protože	k8xS	protože
nedávné	dávný	k2eNgInPc1d1	nedávný
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
ze	z	k7c2	z
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
rozšířených	rozšířený	k2eAgInPc2d1	rozšířený
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Ext	Ext	k1gFnSc4	Ext
<g/>
,	,	kIx,	,
XFS	XFS	kA	XFS
<g/>
,	,	kIx,	,
JFS	JFS	kA	JFS
<g/>
,	,	kIx,	,
ReiserFS	ReiserFS	k1gFnSc1	ReiserFS
nebo	nebo	k8xC	nebo
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
hardwarový	hardwarový	k2eAgInSc4d1	hardwarový
RAID	raid	k1gInSc4	raid
<g/>
,	,	kIx,	,
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
takovýmto	takovýto	k3xDgInPc3	takovýto
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ZFS	ZFS	kA	ZFS
chrání	chránit	k5eAaImIp3nS	chránit
data	datum	k1gNnPc4	datum
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
než	než	k8xS	než
dřívější	dřívější	k2eAgNnSc1d1	dřívější
řešení	řešení	k1gNnSc1	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Integrity	integrita	k1gFnPc4	integrita
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
používáním	používání	k1gNnSc7	používání
Fletcherova	Fletcherův	k2eAgInSc2d1	Fletcherův
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
součtu	součet	k1gInSc2	součet
nebo	nebo	k8xC	nebo
používáním	používání	k1gNnSc7	používání
hashovacího	hashovací	k2eAgInSc2d1	hashovací
algoritmu	algoritmus	k1gInSc2	algoritmus
SHA-2	SHA-2	k1gFnSc2	SHA-2
přes	přes	k7c4	přes
stromovou	stromový	k2eAgFnSc4d1	stromová
strukturu	struktura	k1gFnSc4	struktura
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vypočten	vypočten	k2eAgInSc4d1	vypočten
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
součet	součet	k1gInSc4	součet
každého	každý	k3xTgInSc2	každý
bloku	blok	k1gInSc2	blok
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
hodnota	hodnota	k1gFnSc1	hodnota
každého	každý	k3xTgInSc2	každý
součtu	součet	k1gInSc2	součet
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
ukazateli	ukazatel	k1gInSc6	ukazatel
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
blok	blok	k1gInSc4	blok
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
bloku	blok	k1gInSc6	blok
samotném	samotný	k2eAgInSc6d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
blok	blok	k1gInSc1	blok
obsahující	obsahující	k2eAgInSc1d1	obsahující
tento	tento	k3xDgInSc1	tento
ukazatel	ukazatel	k1gInSc1	ukazatel
je	být	k5eAaImIp3nS	být
také	také	k9	také
chráněn	chránit	k5eAaImNgInS	chránit
kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
součtem	součet	k1gInSc7	součet
a	a	k8xC	a
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
do	do	k7c2	do
ukazatele	ukazatel	k1gInSc2	ukazatel
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
kontrola	kontrola	k1gFnSc1	kontrola
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
do	do	k7c2	do
kořene	kořen	k1gInSc2	kořen
stromové	stromový	k2eAgFnSc2d1	stromová
struktury	struktura	k1gFnSc2	struktura
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
součet	součet	k1gInSc1	součet
je	být	k5eAaImIp3nS	být
také	také	k9	také
proveden	provést	k5eAaPmNgInS	provést
a	a	k8xC	a
tímto	tento	k3xDgNnSc7	tento
je	být	k5eAaImIp3nS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
hash	hash	k1gMnSc1	hash
tree	treat	k5eAaPmIp3nS	treat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
přečten	přečíst	k5eAaPmNgInS	přečíst
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
nezáleží	záležet	k5eNaImIp3nS	záležet
jestli	jestli	k8xS	jestli
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
data	datum	k1gNnPc1	datum
nebo	nebo	k8xC	nebo
metadata	metadat	k2eAgFnSc1d1	metadata
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vypočten	vypočten	k2eAgInSc4d1	vypočten
jeho	jeho	k3xOp3gInSc4	jeho
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
součet	součet	k1gInSc4	součet
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
porovnán	porovnán	k2eAgInSc1d1	porovnán
s	s	k7c7	s
uloženou	uložený	k2eAgFnSc7d1	uložená
hodnotou	hodnota	k1gFnSc7	hodnota
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
součtu	součet	k1gInSc2	součet
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
součty	součet	k1gInPc1	součet
shodné	shodný	k2eAgInPc1d1	shodný
<g/>
,	,	kIx,	,
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
poslána	poslat	k5eAaPmNgNnP	poslat
dále	daleko	k6eAd2	daleko
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
shodné	shodný	k2eAgFnPc1d1	shodná
<g/>
,	,	kIx,	,
ZFS	ZFS	kA	ZFS
dokáže	dokázat	k5eAaPmIp3nS	dokázat
data	datum	k1gNnPc4	datum
opravit	opravit	k5eAaPmF	opravit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
datové	datový	k2eAgNnSc1d1	datové
úložiště	úložiště	k1gNnSc1	úložiště
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
redundanci	redundance	k1gFnSc4	redundance
dat	datum	k1gNnPc2	datum
pomocí	pomocí	k7c2	pomocí
zrcadlení	zrcadlení	k1gNnSc2	zrcadlení
disků	disk	k1gInPc2	disk
typu	typ	k1gInSc2	typ
ZFS	ZFS	kA	ZFS
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
typu	typ	k1gInSc2	typ
RAID	raid	k1gInSc1	raid
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
datové	datový	k2eAgNnSc1d1	datové
úložiště	úložiště	k1gNnSc1	úložiště
skládá	skládat	k5eAaImIp3nS	skládat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
ZFS	ZFS	kA	ZFS
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
možnost	možnost	k1gFnSc4	možnost
redundance	redundance	k1gFnSc2	redundance
pomocí	pomocí	k7c2	pomocí
parametru	parametr	k1gInSc2	parametr
copies	copiesa	k1gFnPc2	copiesa
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
copies	copies	k1gMnSc1	copies
<g/>
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
data	datum	k1gNnPc1	datum
budou	být	k5eAaImBp3nP	být
uložena	uložit	k5eAaPmNgNnP	uložit
na	na	k7c6	na
disku	disk	k1gInSc6	disk
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
třikrát	třikrát	k6eAd1	třikrát
<g/>
)	)	kIx)	)
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
disku	disk	k1gInSc2	disk
bude	být	k5eAaImBp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
(	(	kIx(	(
<g/>
na	na	k7c4	na
třetiny	třetina	k1gFnPc4	třetina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
redundance	redundance	k1gFnSc1	redundance
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
ZFS	ZFS	kA	ZFS
načte	načíst	k5eAaPmIp3nS	načíst
druhou	druhý	k4xOgFnSc4	druhý
kopii	kopie	k1gFnSc4	kopie
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
data	datum	k1gNnPc1	datum
opraví	opravit	k5eAaPmIp3nP	opravit
pomocí	pomocí	k7c2	pomocí
obnovovacích	obnovovací	k2eAgInPc2d1	obnovovací
mechanismů	mechanismus	k1gInPc2	mechanismus
RAID	raid	k1gInSc1	raid
<g/>
)	)	kIx)	)
a	a	k8xC	a
přepočítá	přepočítat	k5eAaPmIp3nS	přepočítat
kontrolní	kontrolní	k2eAgInPc4d1	kontrolní
součty	součet	k1gInPc4	součet
<g/>
,	,	kIx,	,
doufaje	doufat	k5eAaImSgMnS	doufat
že	že	k9	že
tentokrát	tentokrát	k6eAd1	tentokrát
budou	být	k5eAaImBp3nP	být
data	datum	k1gNnPc4	datum
správná	správný	k2eAgFnSc1d1	správná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
kontrolní	kontrolní	k2eAgInPc1d1	kontrolní
součty	součet	k1gInPc1	součet
shodné	shodný	k2eAgInPc1d1	shodný
<g/>
,	,	kIx,	,
ZFS	ZFS	kA	ZFS
nahradí	nahradit	k5eAaPmIp3nP	nahradit
poškozená	poškozený	k2eAgNnPc1d1	poškozené
data	datum	k1gNnPc1	datum
daty	datum	k1gNnPc7	datum
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
kopie	kopie	k1gFnSc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
zapojení	zapojení	k1gNnSc6	zapojení
podpory	podpora	k1gFnSc2	podpora
ZFS	ZFS	kA	ZFS
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
mnoha	mnoho	k4c3	mnoho
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
<g/>
,	,	kIx,	,
BSD	BSD	kA	BSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
serverových	serverový	k2eAgFnPc6d1	serverová
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
ZFS	ZFS	kA	ZFS
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
objevit	objevit	k5eAaPmF	objevit
v	v	k7c6	v
chystaném	chystaný	k2eAgNnSc6d1	chystané
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
10.6	[number]	k4	10.6
Snow	Snow	k1gMnSc1	Snow
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
v	v	k7c6	v
10.5	[number]	k4	10.5
Leopard	leopard	k1gMnSc1	leopard
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
experimentální	experimentální	k2eAgFnSc1d1	experimentální
podpora	podpora	k1gFnSc1	podpora
"	"	kIx"	"
<g/>
ZFS	ZFS	kA	ZFS
Beta	beta	k1gNnSc4	beta
Seed	Seed	k1gInSc1	Seed
v	v	k7c6	v
<g/>
1.1	[number]	k4	1.1
<g/>
"	"	kIx"	"
umožňující	umožňující	k2eAgFnSc1d1	umožňující
jen	jen	k6eAd1	jen
čtení	čtení	k1gNnSc3	čtení
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
přes	přes	k7c4	přes
terminál	terminál	k1gInSc4	terminál
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nedočkala	dočkat	k5eNaPmAgFnS	dočkat
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
10.5	[number]	k4	10.5
<g/>
.1	.1	k4	.1
žádné	žádný	k3yNgFnSc2	žádný
aktualizace	aktualizace	k1gFnSc2	aktualizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dořešení	dořešení	k1gNnSc6	dořešení
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
kolizí	kolize	k1gFnSc7	kolize
licencí	licence	k1gFnPc2	licence
CDDL	CDDL	kA	CDDL
a	a	k8xC	a
GPL	GPL	kA	GPL
bude	být	k5eAaImBp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
jako	jako	k8xS	jako
volitelná	volitelný	k2eAgFnSc1d1	volitelná
součást	součást	k1gFnSc1	součást
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
automatická	automatický	k2eAgFnSc1d1	automatická
kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
oprava	oprava	k1gFnSc1	oprava
konsistence	konsistence	k1gFnSc2	konsistence
zapsaných	zapsaný	k2eAgNnPc2d1	zapsané
dat	datum	k1gNnPc2	datum
téměř	téměř	k6eAd1	téměř
nepřekonatelná	překonatelný	k2eNgFnSc1d1	nepřekonatelná
horní	horní	k2eAgFnPc4d1	horní
kapacitní	kapacitní	k2eAgFnPc4d1	kapacitní
hranice	hranice	k1gFnPc4	hranice
(	(	kIx(	(
<g/>
až	až	k9	až
16	[number]	k4	16
EB	EB	kA	EB
=	=	kIx~	=
16	[number]	k4	16
mld.	mld.	k?	mld.
GB	GB	kA	GB
<g/>
)	)	kIx)	)
zvýšení	zvýšení	k1gNnSc4	zvýšení
maximálního	maximální	k2eAgInSc2d1	maximální
počtu	počet	k1gInSc2	počet
souborů	soubor	k1gInPc2	soubor
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
složce	složka	k1gFnSc6	složka
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
<	<	kIx(	<
7,2	[number]	k4	7,2
<g/>
×	×	k?	×
<g/>
1016	[number]	k4	1016
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
velikost	velikost	k1gFnSc1	velikost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
bloků	blok	k1gInPc2	blok
implementace	implementace	k1gFnSc2	implementace
transparentní	transparentní	k2eAgFnSc1d1	transparentní
komprese	komprese	k1gFnSc1	komprese
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
zápis	zápis	k1gInSc1	zápis
Btrfs	Btrfs	k1gInSc1	Btrfs
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
projektu	projekt	k1gInSc2	projekt
Co	co	k3yInSc1	co
umí	umět	k5eAaImIp3nS	umět
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
ZFS	ZFS	kA	ZFS
(	(	kIx(	(
<g/>
Root	Root	k1gMnSc1	Root
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
