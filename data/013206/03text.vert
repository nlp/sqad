<p>
<s>
Rabi	rabi	k1gMnSc1	rabi
(	(	kIx(	(
<g/>
rav	rav	k?	rav
<g/>
)	)	kIx)	)
Uri	Uri	k1gMnSc1	Uri
Šerki	Šerk	k1gFnSc2	Šerk
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
א	א	k?	א
ע	ע	k?	ע
ש	ש	k?	ש
<g/>
,	,	kIx,	,
Uri	Uri	k1gMnSc1	Uri
Amos	Amos	k1gMnSc1	Amos
Šerki	Šerke	k1gFnSc4	Šerke
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
současných	současný	k2eAgMnPc2d1	současný
izraelských	izraelský	k2eAgMnPc2d1	izraelský
rabínů	rabín	k1gMnPc2	rabín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vede	vést	k5eAaImIp3nS	vést
hebrejskojazyčné	hebrejskojazyčný	k2eAgNnSc1d1	hebrejskojazyčný
oddělení	oddělení	k1gNnSc1	oddělení
ješivy	ješiva	k1gFnPc1	ješiva
Machon	Machon	k1gMnSc1	Machon
Me	Me	k1gMnSc1	Me
<g/>
'	'	kIx"	'
<g/>
ir	ir	k?	ir
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
výuky	výuka	k1gFnSc2	výuka
judaismu	judaismus	k1gInSc2	judaismus
francouzského	francouzský	k2eAgNnSc2d1	francouzské
oddělení	oddělení	k1gNnSc2	oddělení
téže	tenže	k3xDgFnSc2	tenže
ješivy	ješiva	k1gFnSc2	ješiva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rabínem	rabín	k1gMnSc7	rabín
komunity	komunita	k1gFnSc2	komunita
při	při	k7c6	při
synagoze	synagoga	k1gFnSc6	synagoga
Bejt	Bejt	k?	Bejt
Jehuda	Jehudo	k1gNnSc2	Jehudo
v	v	k7c6	v
jeruzalémské	jeruzalémský	k2eAgFnSc6d1	Jeruzalémská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Kirjat	Kirjat	k2eAgMnSc1d1	Kirjat
Moše	mocha	k1gFnSc3	mocha
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
judaismus	judaismus	k1gInSc1	judaismus
širokou	široký	k2eAgFnSc4d1	široká
(	(	kIx(	(
<g/>
i	i	k8xC	i
sekulární	sekulární	k2eAgFnSc1d1	sekulární
<g/>
)	)	kIx)	)
veřejnost	veřejnost	k1gFnSc1	veřejnost
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Roš	Roš	k1gMnSc1	Roš
Jehudi	Jehud	k1gMnPc1	Jehud
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruskojazyčném	ruskojazyčný	k2eAgInSc6d1	ruskojazyčný
vzdělávajím	vzdělávajit	k5eAaPmIp1nS	vzdělávajit
centru	centr	k1gInSc6	centr
Machanajim	Machanajim	k1gInSc1	Machanajim
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
osobnosti	osobnost	k1gFnPc4	osobnost
nábožensko-sionistické	náboženskoionistický	k2eAgFnSc2d1	nábožensko-sionistický
frakce	frakce	k1gFnSc2	frakce
Likudu	Likud	k1gInSc2	Likud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
vystěhovala	vystěhovat	k5eAaPmAgFnS	vystěhovat
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Rabín	rabín	k1gMnSc1	rabín
Uri	Uri	k1gMnSc1	Uri
Šerki	Šerki	k1gNnPc2	Šerki
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
ješivě	ješiva	k1gFnSc6	ješiva
Nativ	Nativa	k1gFnPc2	Nativa
Me	Me	k1gFnSc2	Me
<g/>
'	'	kIx"	'
<g/>
ir	ir	k?	ir
<g/>
,	,	kIx,	,
u	u	k7c2	u
r.	r.	kA	r.
C.	C.	kA	C.
J.	J.	kA	J.
Kooka	Kooka	k1gMnSc1	Kooka
(	(	kIx(	(
<g/>
Kuka	Kuka	k1gMnSc1	Kuka
<g/>
)	)	kIx)	)
v	v	k7c6	v
ješivě	ješiva	k1gFnSc6	ješiva
Merkaz	Merkaz	k1gInSc4	Merkaz
ha-rav	haat	k5eAaPmDgInS	ha-rat
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgMnPc2d1	další
významných	významný	k2eAgMnPc2d1	významný
rabínů	rabín	k1gMnPc2	rabín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Ш	Ш	k?	Ш
<g/>
,	,	kIx,	,
У	У	k?	У
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
א	א	k?	א
ש	ש	k?	ש
na	na	k7c6	na
hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Náboženský	náboženský	k2eAgInSc4d1	náboženský
sionismus	sionismus	k1gInSc4	sionismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Uri	Uri	k1gFnSc2	Uri
Šerki	Šerki	k1gNnSc2	Šerki
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
r.	r.	kA	r.
Šerkiho	Šerki	k1gMnSc2	Šerki
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Články	článek	k1gInPc1	článek
r.	r.	kA	r.
Šerkiho	Šerki	k1gMnSc2	Šerki
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Parašat	Parašat	k2eAgMnSc1d1	Parašat
šavua	šavua	k1gMnSc1	šavua
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Roš	Roš	k1gFnPc2	Roš
Jehudi	Jehud	k1gMnPc1	Jehud
-	-	kIx~	-
články	článek	k1gInPc1	článek
r.	r.	kA	r.
Šerkiho	Šerki	k1gMnSc4	Šerki
</s>
</p>
