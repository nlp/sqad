<s>
Rabi	rabi	k1gMnSc1	rabi
(	(	kIx(	(
<g/>
rav	rav	k?	rav
<g/>
)	)	kIx)	)
Uri	Uri	k1gMnSc1	Uri
Šerki	Šerk	k1gFnSc2	Šerk
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
א	א	k?	א
ע	ע	k?	ע
ש	ש	k?	ש
<g/>
,	,	kIx,	,
Uri	Uri	k1gMnSc1	Uri
Amos	Amos	k1gMnSc1	Amos
Šerki	Šerke	k1gFnSc4	Šerke
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
současných	současný	k2eAgMnPc2d1	současný
izraelských	izraelský	k2eAgMnPc2d1	izraelský
rabínů	rabín	k1gMnPc2	rabín
<g/>
.	.	kIx.	.
</s>
