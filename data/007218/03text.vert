<s>
Dignitas	Dignitas	k1gInSc1	Dignitas
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
důstojnost	důstojnost	k1gFnSc1	důstojnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
organizace	organizace	k1gFnSc1	organizace
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
asistovanou	asistovaný	k2eAgFnSc4d1	asistovaná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Organizaci	organizace	k1gFnSc4	organizace
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
pro	pro	k7c4	pro
eutanázii	eutanázie	k1gFnSc4	eutanázie
Ludwig	Ludwig	k1gMnSc1	Ludwig
A.	A.	kA	A.
Minelli	Minell	k1gMnPc1	Minell
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
vykonáno	vykonat	k5eAaPmNgNnS	vykonat
kolem	kolem	k7c2	kolem
840	[number]	k4	840
asistovaných	asistovaný	k2eAgFnPc2d1	asistovaná
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
klientů	klient	k1gMnPc2	klient
tvořili	tvořit	k5eAaImAgMnP	tvořit
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Asistovanou	asistovaný	k2eAgFnSc4d1	asistovaná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
zde	zde	k6eAd1	zde
spáchali	spáchat	k5eAaPmAgMnP	spáchat
i	i	k9	i
Češi	Čech	k1gMnPc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
21	[number]	k4	21
<g/>
%	%	kIx~	%
sebevrahů	sebevrah	k1gMnPc2	sebevrah
netrpí	trpět	k5eNaImIp3nS	trpět
nevyléčitelnými	vyléčitelný	k2eNgFnPc7d1	nevyléčitelná
chorobami	choroba	k1gFnPc7	choroba
a	a	k8xC	a
k	k	k7c3	k
asistované	asistovaný	k2eAgFnSc3d1	asistovaná
sebevraždě	sebevražda	k1gFnSc3	sebevražda
je	on	k3xPp3gNnSc4	on
vedou	vést	k5eAaImIp3nP	vést
jiné	jiný	k2eAgInPc1d1	jiný
důvody	důvod	k1gInPc1	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
klienty	klient	k1gMnPc7	klient
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
trpící	trpící	k2eAgNnSc1d1	trpící
pouhou	pouhý	k2eAgFnSc7d1	pouhá
depresí	deprese	k1gFnSc7	deprese
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Minelli	Minelle	k1gFnSc4	Minelle
údajně	údajně	k6eAd1	údajně
také	také	k9	také
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
nevyléčitelnou	vyléčitelný	k2eNgFnSc4d1	nevyléčitelná
chorobu	choroba	k1gFnSc4	choroba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
klientům	klient	k1gMnPc3	klient
s	s	k7c7	s
duševními	duševní	k2eAgFnPc7d1	duševní
chorobami	choroba	k1gFnPc7	choroba
asistenci	asistence	k1gFnSc6	asistence
při	při	k7c6	při
sebevraždě	sebevražda	k1gFnSc6	sebevražda
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
právo	právo	k1gNnSc1	právo
a	a	k8xC	a
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
psychiatři	psychiatr	k1gMnPc1	psychiatr
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
dosud	dosud	k6eAd1	dosud
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
klienty	klient	k1gMnPc7	klient
Dignitas	Dignitasa	k1gFnPc2	Dignitasa
patřily	patřit	k5eAaImAgFnP	patřit
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
známé	známý	k2eAgFnPc1d1	známá
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
autor	autor	k1gMnSc1	autor
komiksů	komiks	k1gInPc2	komiks
John	John	k1gMnSc1	John
Hicklenton	Hicklenton	k1gInSc1	Hicklenton
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
a	a	k8xC	a
lesbická	lesbický	k2eAgFnSc1d1	lesbická
aktivistka	aktivistka	k1gFnSc1	aktivistka
Michè	Michè	k1gFnSc1	Michè
Causseová	Causseová	k1gFnSc1	Causseová
či	či	k8xC	či
britský	britský	k2eAgMnSc1d1	britský
dirigent	dirigent	k1gMnSc1	dirigent
Edward	Edward	k1gMnSc1	Edward
Downes	Downes	k1gMnSc1	Downes
<g/>
.	.	kIx.	.
</s>
<s>
Sebevrah	sebevrah	k1gMnSc1	sebevrah
nejprve	nejprve	k6eAd1	nejprve
projde	projít	k5eAaPmIp3nS	projít
několikaměsíčním	několikaměsíční	k2eAgNnSc7d1	několikaměsíční
obdobím	období	k1gNnSc7	období
konzultací	konzultace	k1gFnPc2	konzultace
s	s	k7c7	s
odborníky	odborník	k1gMnPc7	odborník
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
s	s	k7c7	s
časovým	časový	k2eAgInSc7d1	časový
odstupem	odstup	k1gInSc7	odstup
musí	muset	k5eAaImIp3nS	muset
písemně	písemně	k6eAd1	písemně
i	i	k9	i
ústně	ústně	k6eAd1	ústně
projevit	projevit	k5eAaPmF	projevit
svou	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
asistovanou	asistovaný	k2eAgFnSc4d1	asistovaná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
podstoupit	podstoupit	k5eAaPmF	podstoupit
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sebevraždou	sebevražda	k1gFnSc7	sebevražda
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gMnSc3	on
podány	podán	k2eAgInPc1d1	podán
léky	lék	k1gInPc1	lék
proti	proti	k7c3	proti
zvracení	zvracení	k1gNnSc3	zvracení
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
sama	sám	k3xTgFnSc1	sám
je	být	k5eAaImIp3nS	být
provedena	provést	k5eAaPmNgFnS	provést
pomocí	pomocí	k7c2	pomocí
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
dávky	dávka	k1gFnSc2	dávka
pentobarbitalu	pentobarbital	k1gMnSc3	pentobarbital
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
ve	v	k7c6	v
sklenici	sklenice	k1gFnSc6	sklenice
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
džusu	džus	k1gInSc2	džus
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
krmící	krmící	k2eAgFnSc2d1	krmící
trubice	trubice	k1gFnSc2	trubice
či	či	k8xC	či
nitrožilně	nitrožilně	k6eAd1	nitrožilně
(	(	kIx(	(
<g/>
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
přísun	přísun	k1gInSc1	přísun
jedu	jet	k5eAaImIp1nS	jet
zahájit	zahájit	k5eAaPmF	zahájit
sám	sám	k3xTgMnSc1	sám
sebevrah	sebevrah	k1gMnSc1	sebevrah
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c6	o
eutanázii	eutanázie	k1gFnSc6	eutanázie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
bylo	být	k5eAaImAgNnS	být
experimentálně	experimentálně	k6eAd1	experimentálně
použito	použít	k5eAaPmNgNnS	použít
také	také	k9	také
zadušení	zadušení	k1gNnSc1	zadušení
pomocí	pomoc	k1gFnPc2	pomoc
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
organizace	organizace	k1gFnSc1	organizace
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
nutnosti	nutnost	k1gFnSc2	nutnost
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
jed	jed	k1gInSc4	jed
předepsat	předepsat	k5eAaPmF	předepsat
od	od	k7c2	od
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarům	Švýcar	k1gMnPc3	Švýcar
se	se	k3xPyFc4	se
asistence	asistence	k1gFnSc2	asistence
při	při	k7c6	při
sebevraždě	sebevražda	k1gFnSc6	sebevražda
většinou	většina	k1gFnSc7	většina
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
klientům	klient	k1gMnPc3	klient
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Sebevrahům	sebevrah	k1gMnPc3	sebevrah
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
přejí	přát	k5eAaImIp3nP	přát
<g/>
,	,	kIx,	,
dělají	dělat	k5eAaImIp3nP	dělat
společnost	společnost	k1gFnSc4	společnost
rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
nebo	nebo	k8xC	nebo
profesionální	profesionální	k2eAgMnPc1d1	profesionální
společníci	společník	k1gMnPc1	společník
Dignitas	Dignitasa	k1gFnPc2	Dignitasa
se	se	k3xPyFc4	se
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
jako	jako	k9	jako
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
švýcarské	švýcarský	k2eAgInPc1d1	švýcarský
zákony	zákon	k1gInPc1	zákon
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
provádět	provádět	k5eAaImF	provádět
asistovanou	asistovaný	k2eAgFnSc4d1	asistovaná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
"	"	kIx"	"
<g/>
ze	z	k7c2	z
zištných	zištný	k2eAgInPc2d1	zištný
důvodů	důvod	k1gInPc2	důvod
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
sebevrazích	sebevrah	k1gMnPc6	sebevrah
nicméně	nicméně	k8xC	nicméně
požaduje	požadovat	k5eAaImIp3nS	požadovat
poplatek	poplatek	k1gInSc4	poplatek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
čtyř	čtyři	k4xCgNnPc2	čtyři
až	až	k9	až
sedmi	sedm	k4xCc2	sedm
tisíc	tisíc	k4xCgInSc4	tisíc
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Dignitas	Dignitas	k1gInSc1	Dignitas
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
i	i	k9	i
pohřeb	pohřeb	k1gInSc4	pohřeb
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
na	na	k7c4	na
pokrytí	pokrytí	k1gNnSc4	pokrytí
nákladů	náklad	k1gInPc2	náklad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Minelli	Minell	k1gMnPc1	Minell
na	na	k7c6	na
chodu	chod	k1gInSc6	chod
instituce	instituce	k1gFnSc2	instituce
značně	značně	k6eAd1	značně
zbohatl	zbohatnout	k5eAaPmAgMnS	zbohatnout
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
bývalé	bývalý	k2eAgFnSc2d1	bývalá
zaměstnankyně	zaměstnankyně	k1gFnSc2	zaměstnankyně
Sorayi	Soray	k1gFnSc2	Soray
Wernliové	Wernliový	k2eAgFnSc2d1	Wernliový
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
Dignitas	Dignitasa	k1gFnPc2	Dignitasa
napsala	napsat	k5eAaPmAgFnS	napsat
značně	značně	k6eAd1	značně
kritickou	kritický	k2eAgFnSc4d1	kritická
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
mu	on	k3xPp3gNnSc3	on
klienti	klient	k1gMnPc1	klient
často	často	k6eAd1	často
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
natočila	natočit	k5eAaBmAgFnS	natočit
také	také	k9	také
několik	několik	k4yIc4	několik
propagačních	propagační	k2eAgInPc2d1	propagační
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
i	i	k9	i
videozáznamy	videozáznam	k1gInPc4	videozáznam
sebevražd	sebevražda	k1gFnPc2	sebevražda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Right	Right	k1gMnSc1	Right
to	ten	k3xDgNnSc1	ten
Die	Die	k1gMnSc1	Die
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Klinika	klinika	k1gFnSc1	klinika
Dignitas	Dignitas	k1gInSc1	Dignitas
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
právními	právní	k2eAgInPc7d1	právní
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mediálně	mediálně	k6eAd1	mediálně
známých	známý	k2eAgFnPc2d1	známá
kauz	kauza	k1gFnPc2	kauza
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
vhazování	vhazování	k1gNnSc1	vhazování
uren	urna	k1gFnPc2	urna
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
sebevrahů	sebevrah	k1gMnPc2	sebevrah
do	do	k7c2	do
Curyšského	curyšský	k2eAgNnSc2d1	Curyšské
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
