<p>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Balcarka	Balcarka	k1gFnSc1	Balcarka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Ostrova	ostrov	k1gInSc2	ostrov
u	u	k7c2	u
Macochy	Macocha	k1gFnSc2	Macocha
<g/>
,	,	kIx,	,
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc4d1	moravský
kras	kras	k1gInSc4	kras
<g/>
,	,	kIx,	,
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
skalnatého	skalnatý	k2eAgInSc2d1	skalnatý
meandru	meandr	k1gInSc2	meandr
Suchého	Suchého	k2eAgInSc2d1	Suchého
žlebu	žleb	k1gInSc2	žleb
zvaného	zvaný	k2eAgInSc2d1	zvaný
Balcarova	Balcarův	k2eAgFnSc1d1	Balcarova
skála	skála	k1gFnSc1	skála
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
Balcarky	Balcarka	k1gFnSc2	Balcarka
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
odnepaměti	odnepaměti	k6eAd1	odnepaměti
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
zde	zde	k6eAd1	zde
nalezena	nalezen	k2eAgNnPc1d1	Nalezeno
ohniště	ohniště	k1gNnPc1	ohniště
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
nástroje	nástroj	k1gInPc1	nástroj
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
a	a	k8xC	a
kosti	kost	k1gFnSc2	kost
čtvrtohorních	čtvrtohorní	k2eAgNnPc2d1	čtvrtohorní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
jeskyně	jeskyně	k1gFnSc1	jeskyně
postupně	postupně	k6eAd1	postupně
objevována	objevován	k2eAgFnSc1d1	objevována
Josefem	Josef	k1gMnSc7	Josef
Šamalíkem	Šamalík	k1gMnSc7	Šamalík
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
byly	být	k5eAaImAgFnP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
jeskyně	jeskyně	k1gFnSc2	jeskyně
spojeny	spojen	k2eAgFnPc1d1	spojena
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
tvoří	tvořit	k5eAaImIp3nS	tvořit
složitý	složitý	k2eAgInSc4d1	složitý
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
vystupující	vystupující	k2eAgFnSc4d1	vystupující
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
výškových	výškový	k2eAgFnPc6d1	výšková
úrovních	úroveň	k1gFnPc6	úroveň
spojených	spojený	k2eAgFnPc2d1	spojená
vysokými	vysoký	k2eAgInPc7d1	vysoký
dómy	dóm	k1gInPc7	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
jeskyně	jeskyně	k1gFnSc2	jeskyně
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
vody	voda	k1gFnPc1	voda
Lopače	Lopač	k1gInSc2	Lopač
a	a	k8xC	a
Krasovského	Krasovský	k2eAgInSc2d1	Krasovský
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
ponory	ponor	k1gInPc1	ponor
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
byla	být	k5eAaImAgFnS	být
veřejnosti	veřejnost	k1gFnSc2	veřejnost
zpřístupněna	zpřístupnit	k5eAaPmNgFnS	zpřístupnit
Správou	správa	k1gFnSc7	správa
jeskyní	jeskyně	k1gFnPc2	jeskyně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
otevírací	otevírací	k2eAgFnSc7d1	otevírací
dobou	doba	k1gFnSc7	doba
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
probíhala	probíhat	k5eAaImAgFnS	probíhat
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yRgFnSc3	který
byla	být	k5eAaImAgFnS	být
jeskyně	jeskyně	k1gFnSc2	jeskyně
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
,	,	kIx,	,
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
Balcarka	Balcarka	k1gFnSc1	Balcarka
znovu	znovu	k6eAd1	znovu
otevřena	otevřít	k5eAaPmNgFnS	otevřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dóm	dóm	k1gInSc1	dóm
zkázy	zkáza	k1gFnSc2	zkáza
a	a	k8xC	a
Fochův	Fochův	k2eAgInSc4d1	Fochův
dóm	dóm	k1gInSc4	dóm
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
prostor	prostora	k1gFnPc2	prostora
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
<g/>
.	.	kIx.	.
</s>
<s>
Krápníková	krápníkový	k2eAgFnSc1d1	krápníková
výzdoba	výzdoba	k1gFnSc1	výzdoba
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
z	z	k7c2	z
veřejnosti	veřejnost	k1gFnSc2	veřejnost
přístupných	přístupný	k2eAgFnPc2d1	přístupná
českých	český	k2eAgFnPc2d1	Česká
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
části	část	k1gFnPc1	část
jeskyně	jeskyně	k1gFnSc2	jeskyně
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
dómů	dóm	k1gInPc2	dóm
Rotundy	rotunda	k1gFnSc2	rotunda
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Popeluška	Popeluška	k1gFnSc1	Popeluška
a	a	k8xC	a
Jubilejní	jubilejní	k2eAgInPc1d1	jubilejní
dómy	dóm	k1gInPc1	dóm
Masarykovy	Masarykův	k2eAgInPc1d1	Masarykův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Balcarka	Balcarka	k1gFnSc1	Balcarka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Balcarka	Balcarka	k1gFnSc1	Balcarka
na	na	k7c4	na
www.mojebrno.jecool.net	www.mojebrno.jecool.net	k1gInSc4	www.mojebrno.jecool.net
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
návštěvě	návštěva	k1gFnSc6	návštěva
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
mapa	mapa	k1gFnSc1	mapa
</s>
</p>
<p>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Balcarka	Balcarka	k1gFnSc1	Balcarka
</s>
</p>
<p>
<s>
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
Balcarka	Balcarka	k1gFnSc1	Balcarka
</s>
</p>
<p>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Balcarka	Balcarka	k1gFnSc1	Balcarka
–	–	k?	–
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
2005-03-20	[number]	k4	2005-03-20
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
