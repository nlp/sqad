<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Balcarka	Balcarka	k1gFnSc1	Balcarka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Ostrova	ostrov	k1gInSc2	ostrov
u	u	k7c2	u
Macochy	Macocha	k1gFnSc2	Macocha
<g/>
,	,	kIx,	,
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc4d1	moravský
kras	kras	k1gInSc4	kras
<g/>
,	,	kIx,	,
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
skalnatého	skalnatý	k2eAgInSc2d1	skalnatý
meandru	meandr	k1gInSc2	meandr
Suchého	Suchého	k2eAgInSc2d1	Suchého
žlebu	žleb	k1gInSc2	žleb
zvaného	zvaný	k2eAgInSc2d1	zvaný
Balcarova	Balcarův	k2eAgFnSc1d1	Balcarova
skála	skála	k1gFnSc1	skála
<g/>
.	.	kIx.	.
</s>
