<p>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
je	být	k5eAaImIp3nS	být
probíhající	probíhající	k2eAgFnSc1d1	probíhající
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
hútíjskému	hútíjský	k2eAgNnSc3d1	hútíjský
povstání	povstání	k1gNnSc3	povstání
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
.	.	kIx.	.
</s>
<s>
Saúdskoarabské	saúdskoarabský	k2eAgNnSc1d1	Saúdskoarabské
kódové	kódový	k2eAgNnSc1d1	kódové
označení	označení	k1gNnSc1	označení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
zní	znět	k5eAaImIp3nS	znět
operace	operace	k1gFnSc1	operace
Decisive	Decisiev	k1gFnSc2	Decisiev
Storm	Storm	k1gInSc1	Storm
(	(	kIx(	(
<g/>
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
–	–	k?	–
tj.	tj.	kA	tj.
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
6	[number]	k4	6
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
Restoring	Restoring	k1gInSc1	Restoring
Hope	Hop	k1gInSc2	Hop
(	(	kIx(	(
<g/>
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Intervence	intervence	k1gFnSc1	intervence
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
pokračování	pokračování	k1gNnSc1	pokračování
mocenského	mocenský	k2eAgInSc2d1	mocenský
souboje	souboj	k1gInSc2	souboj
mezi	mezi	k7c7	mezi
sunnitskou	sunnitský	k2eAgFnSc7d1	sunnitská
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
a	a	k8xC	a
šíitským	šíitský	k2eAgInSc7d1	šíitský
Íránem	Írán	k1gInSc7	Írán
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
donutili	donutit	k5eAaPmAgMnP	donutit
šíitští	šíitský	k2eAgMnPc1d1	šíitský
Hútíové	Hútíus	k1gMnPc1	Hútíus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
obsadili	obsadit	k5eAaPmAgMnP	obsadit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
San	San	k1gFnSc2	San
<g/>
'	'	kIx"	'
<g/>
á	á	k0	á
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
jemenského	jemenský	k2eAgMnSc2d1	jemenský
prezidenta	prezident	k1gMnSc2	prezident
Abd	Abd	k1gMnSc4	Abd
Rabúa	Rabúus	k1gMnSc4	Rabúus
Hádího	Hádí	k1gMnSc4	Hádí
a	a	k8xC	a
jemenskou	jemenský	k2eAgFnSc4d1	Jemenská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
bylo	být	k5eAaImAgNnS	být
uvaleno	uvalen	k2eAgNnSc1d1	uvaleno
domácí	domácí	k2eAgNnSc1d1	domácí
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
pětičlennou	pětičlenný	k2eAgFnSc4d1	pětičlenná
tzv.	tzv.	kA	tzv.
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Hádímu	Hádí	k1gMnSc3	Hádí
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
Adenu	Aden	k1gInSc2	Aden
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
odpor	odpor	k1gInSc4	odpor
z	z	k7c2	z
části	část	k1gFnSc2	část
jemu	on	k3xPp3gNnSc3	on
věrné	věrný	k2eAgFnPc1d1	věrná
armády	armáda	k1gFnPc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Hútíové	Hútíová	k1gFnPc1	Hútíová
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prezidenta	prezident	k1gMnSc2	prezident
Alího	Alí	k1gMnSc2	Alí
Sáliha	Sálih	k1gMnSc2	Sálih
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
stoupenců	stoupenec	k1gMnPc2	stoupenec
postupovali	postupovat	k5eAaImAgMnP	postupovat
na	na	k7c4	na
Aden	Aden	k1gInSc4	Aden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
požádal	požádat	k5eAaPmAgMnS	požádat
Hádí	Hádí	k2eAgNnSc4d1	Hádí
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
společenství	společenství	k1gNnSc4	společenství
o	o	k7c4	o
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
bojuje	bojovat	k5eAaImIp3nS	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
oficiální	oficiální	k2eAgFnSc2d1	oficiální
vlády	vláda	k1gFnSc2	vláda
Mansúra	Mansúr	k1gMnSc2	Mansúr
Hádího	Hádí	k1gMnSc2	Hádí
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
údajně	údajně	k6eAd1	údajně
podporuje	podporovat	k5eAaImIp3nS	podporovat
hutíjské	hutíjský	k2eAgMnPc4d1	hutíjský
rebely	rebel	k1gMnPc4	rebel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
vyjádření	vyjádření	k1gNnSc2	vyjádření
zástupce	zástupce	k1gMnSc2	zástupce
koalice	koalice	k1gFnSc2	koalice
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
provést	provést	k5eAaPmF	provést
intervenci	intervence	k1gFnSc4	intervence
také	také	k9	také
údajné	údajný	k2eAgNnSc4d1	údajné
přemístění	přemístění	k1gNnSc4	přemístění
části	část	k1gFnSc2	část
odpalovačů	odpalovač	k1gMnPc2	odpalovač
raket	raketa	k1gFnPc2	raketa
Scud	Scuda	k1gFnPc2	Scuda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
má	mít	k5eAaImIp3nS	mít
jemenská	jemenský	k2eAgFnSc1d1	Jemenská
armáda	armáda	k1gFnSc1	armáda
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
a	a	k8xC	a
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
chopili	chopit	k5eAaPmAgMnP	chopit
Hútíové	Hútíus	k1gMnPc1	Hútíus
<g/>
,	,	kIx,	,
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
severního	severní	k2eAgInSc2d1	severní
Jemenu	Jemen	k1gInSc2	Jemen
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
měly	mít	k5eAaImAgFnP	mít
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
jih	jih	k1gInSc4	jih
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Intervenční	intervenční	k2eAgFnPc1d1	intervenční
síly	síla	k1gFnPc1	síla
==	==	k?	==
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
operace	operace	k1gFnSc2	operace
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
150	[number]	k4	150
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
100	[number]	k4	100
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
leteckých	letecký	k2eAgInPc2d1	letecký
útoků	útok	k1gInPc2	útok
se	se	k3xPyFc4	se
zapojily	zapojit	k5eAaPmAgInP	zapojit
i	i	k9	i
stroje	stroj	k1gInPc1	stroj
z	z	k7c2	z
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
Podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
ochotu	ochota	k1gFnSc4	ochota
se	se	k3xPyFc4	se
zapojit	zapojit	k5eAaPmF	zapojit
projevily	projevit	k5eAaPmAgFnP	projevit
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc4	Maroko
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc4	Súdán
a	a	k8xC	a
Pákistán	Pákistán	k1gInSc4	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
se	se	k3xPyFc4	se
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
poskytnout	poskytnout	k5eAaPmF	poskytnout
logistickou	logistický	k2eAgFnSc4d1	logistická
a	a	k8xC	a
výzvědnou	výzvědný	k2eAgFnSc4d1	výzvědná
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nehodlají	hodlat	k5eNaImIp3nP	hodlat
se	se	k3xPyFc4	se
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
přímých	přímý	k2eAgInPc2d1	přímý
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
intervence	intervence	k1gFnSc1	intervence
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
leteckými	letecký	k2eAgInPc7d1	letecký
útoky	útok	k1gInPc7	útok
vůči	vůči	k7c3	vůči
pozicím	pozice	k1gFnPc3	pozice
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
prohlášení	prohlášení	k1gNnSc2	prohlášení
saúdskoarabské	saúdskoarabský	k2eAgFnSc2d1	saúdskoarabská
tiskové	tiskový	k2eAgFnSc2d1	tisková
agentury	agentura	k1gFnSc2	agentura
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
saúdskoarabské	saúdskoarabský	k2eAgInPc1d1	saúdskoarabský
stroje	stroj	k1gInPc1	stroj
na	na	k7c4	na
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
al-Dailami	al-Daila	k1gFnPc7	al-Daila
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zničily	zničit	k5eAaPmAgFnP	zničit
protileteckou	protiletecký	k2eAgFnSc4d1	protiletecká
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
4	[number]	k4	4
povstalecké	povstalecký	k2eAgInPc4d1	povstalecký
letouny	letoun	k1gInPc4	letoun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
operace	operace	k1gFnSc1	operace
si	se	k3xPyFc3	se
měla	mít	k5eAaImAgFnS	mít
koalice	koalice	k1gFnSc1	koalice
vydobýt	vydobýt	k5eAaPmF	vydobýt
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
Jemenem	Jemen	k1gInSc7	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
do	do	k7c2	do
akce	akce	k1gFnSc2	akce
měly	mít	k5eAaImAgInP	mít
zapojit	zapojit	k5eAaPmF	zapojit
i	i	k9	i
letouny	letoun	k1gInPc4	letoun
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
bombardování	bombardování	k1gNnSc1	bombardování
tří	tři	k4xCgFnPc2	tři
základen	základna	k1gFnPc2	základna
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Marib	Mariba	k1gFnPc2	Mariba
a	a	k8xC	a
také	také	k9	také
kmenové	kmenový	k2eAgNnSc1d1	kmenové
území	území	k1gNnSc1	území
Hútíů	Hútí	k1gInPc2	Hútí
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nálety	nálet	k1gInPc4	nálet
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Hútíové	Hútíus	k1gMnPc1	Hútíus
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
přístav	přístav	k1gInSc1	přístav
Šukra	Šukr	k1gInSc2	Šukr
a	a	k8xC	a
pronikli	proniknout	k5eAaPmAgMnP	proniknout
tak	tak	k9	tak
k	k	k7c3	k
Arabskému	arabský	k2eAgNnSc3d1	arabské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
také	také	k9	také
boje	boj	k1gInPc1	boj
u	u	k7c2	u
Adenu	Aden	k1gInSc2	Aden
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
povstalci	povstalec	k1gMnPc1	povstalec
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
místní	místní	k2eAgNnSc4d1	místní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
také	také	k9	také
zachránily	zachránit	k5eAaPmAgFnP	zachránit
americké	americký	k2eAgFnPc1d1	americká
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
dva	dva	k4xCgInPc4	dva
saúdskoarabské	saúdskoarabský	k2eAgInPc4d1	saúdskoarabský
piloty	pilot	k1gInPc4	pilot
z	z	k7c2	z
Adenského	adenský	k2eAgInSc2d1	adenský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Saúdskoarabské	saúdskoarabský	k2eAgNnSc1d1	Saúdskoarabské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
také	také	k9	také
provedlo	provést	k5eAaPmAgNnS	provést
evakuaci	evakuace	k1gFnSc4	evakuace
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
diplomatů	diplomat	k1gMnPc2	diplomat
z	z	k7c2	z
Adenu	Aden	k1gInSc2	Aden
do	do	k7c2	do
rudomořského	rudomořský	k2eAgInSc2d1	rudomořský
přístavu	přístav	k1gInSc2	přístav
Džidda	Džiddo	k1gNnSc2	Džiddo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
náletu	nálet	k1gInSc3	nálet
na	na	k7c4	na
hútíjský	hútíjský	k2eAgInSc4d1	hútíjský
konvoj	konvoj	k1gInSc4	konvoj
s	s	k7c7	s
obrněnou	obrněný	k2eAgFnSc7d1	obrněná
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
nákladními	nákladní	k2eAgNnPc7d1	nákladní
vozidly	vozidlo	k1gNnPc7	vozidlo
mířící	mířící	k2eAgFnSc1d1	mířící
z	z	k7c2	z
Šukry	Šukra	k1gFnSc2	Šukra
na	na	k7c4	na
Aden	Aden	k1gInSc4	Aden
<g/>
.	.	kIx.	.
</s>
<s>
Saúdové	Saúda	k1gMnPc1	Saúda
zahájili	zahájit	k5eAaPmAgMnP	zahájit
svoji	svůj	k3xOyFgFnSc4	svůj
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2015	[number]	k4	2015
společně	společně	k6eAd1	společně
s	s	k7c7	s
koalicí	koalice	k1gFnSc7	koalice
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
včetně	včetně	k7c2	včetně
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
<g/>
,	,	kIx,	,
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
,	,	kIx,	,
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
také	také	k9	také
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
pátek	pátek	k1gInSc4	pátek
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
náletu	nálet	k1gInSc3	nálet
na	na	k7c4	na
jemenskou	jemenský	k2eAgFnSc4d1	Jemenská
metropoli	metropole	k1gFnSc4	metropole
Saná	Saná	k1gFnSc1	Saná
<g/>
.	.	kIx.	.
</s>
<s>
Nálet	nálet	k1gInSc1	nálet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
dva	dva	k4xCgInPc4	dva
domy	dům	k1gInPc4	dům
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
předměstí	předměstí	k1gNnSc6	předměstí
Saná	Saná	k1gFnSc1	Saná
<g/>
,	,	kIx,	,
podnikla	podniknout	k5eAaPmAgFnS	podniknout
letadla	letadlo	k1gNnSc2	letadlo
koalice	koalice	k1gFnSc2	koalice
vedené	vedený	k2eAgNnSc1d1	vedené
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jemenské	jemenský	k2eAgFnSc3d1	Jemenská
vládě	vláda	k1gFnSc3	vláda
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
šíitským	šíitský	k2eAgMnPc3d1	šíitský
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Arabská	arabský	k2eAgFnSc1d1	arabská
koalice	koalice	k1gFnSc1	koalice
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
:	:	kIx,	:
agentura	agentura	k1gFnSc1	agentura
Reuters	Reutersa	k1gFnPc2	Reutersa
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
psala	psát	k5eAaImAgFnS	psát
o	o	k7c4	o
nejméně	málo	k6eAd3	málo
12	[number]	k4	12
mrtvých	mrtvý	k1gMnPc2	mrtvý
civilistech	civilista	k1gMnPc6	civilista
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
6	[number]	k4	6
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
agentura	agentura	k1gFnSc1	agentura
AFP	AFP	kA	AFP
o	o	k7c4	o
14	[number]	k4	14
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
sourozenců	sourozenec	k1gMnPc2	sourozenec
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
černu	černo	k1gNnSc6	černo
2018	[number]	k4	2018
zahájila	zahájit	k5eAaPmAgFnS	zahájit
koalice	koalice	k1gFnSc1	koalice
vedená	vedený	k2eAgFnSc1d1	vedená
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
útok	útok	k1gInSc1	útok
na	na	k7c4	na
jemenský	jemenský	k2eAgInSc4d1	jemenský
přístav	přístav	k1gInSc4	přístav
Hudajda	Hudajda	k1gFnSc1	Hudajda
<g/>
.20	.20	k4	.20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
generální	generální	k2eAgInSc4d1	generální
shromáždění	shromáždění	k1gNnSc1	shromáždění
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
sněmu	sněm	k1gInSc2	sněm
OSN	OSN	kA	OSN
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
zahrnující	zahrnující	k2eAgNnSc4d1	zahrnující
období	období	k1gNnSc4	období
ledna	leden	k1gInSc2	leden
až	až	k8xS	až
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
šestinásobný	šestinásobný	k2eAgInSc4d1	šestinásobný
vzrůst	vzrůst	k1gInSc4	vzrůst
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
zabitých	zabitý	k2eAgFnPc2d1	zabitá
a	a	k8xC	a
zmrzačených	zmrzačený	k2eAgFnPc2d1	zmrzačená
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
s	s	k7c7	s
1953	[number]	k4	1953
dětskými	dětský	k2eAgFnPc7d1	dětská
obětmi	oběť	k1gFnPc7	oběť
(	(	kIx(	(
<g/>
785	[number]	k4	785
zabitých	zabitý	k2eAgMnPc2d1	zabitý
<g/>
,	,	kIx,	,
1168	[number]	k4	1168
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
)	)	kIx)	)
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
%	%	kIx~	%
chlapců	chlapec	k1gMnPc2	chlapec
a	a	k8xC	a
60	[number]	k4	60
%	%	kIx~	%
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
510	[number]	k4	510
zabití	zabití	k1gNnPc2	zabití
a	a	k8xC	a
667	[number]	k4	667
zranění	zranění	k1gNnPc2	zranění
<g/>
)	)	kIx)	)
připadalo	připadat	k5eAaPmAgNnS	připadat
na	na	k7c4	na
koalici	koalice	k1gFnSc4	koalice
vedenou	vedený	k2eAgFnSc7d1	vedená
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
letecké	letecký	k2eAgInPc4d1	letecký
nálety	nálet	k1gInPc4	nálet
Saúdy	Saúda	k1gFnSc2	Saúda
vedenou	vedený	k2eAgFnSc7d1	vedená
koalicí	koalice	k1gFnSc7	koalice
zabily	zabít	k5eAaPmAgFnP	zabít
140	[number]	k4	140
zranily	zranit	k5eAaPmAgInP	zranit
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejkrvavějších	krvavý	k2eAgInPc2d3	nejkrvavější
dnů	den	k1gInPc2	den
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
snažilo	snažit	k5eAaImAgNnS	snažit
vyjednat	vyjednat	k5eAaPmF	vyjednat
se	s	k7c7	s
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
lukrativní	lukrativní	k2eAgFnSc4d1	lukrativní
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2018	[number]	k4	2018
přinesly	přinést	k5eAaPmAgFnP	přinést
The	The	k1gFnPc1	The
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejméně	málo	k6eAd3	málo
30	[number]	k4	30
civilistů	civilista	k1gMnPc2	civilista
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
Saúdy	Saúd	k1gInPc4	Saúd
vedenou	vedený	k2eAgFnSc7d1	vedená
aliancí	aliance	k1gFnSc7	aliance
při	při	k7c6	při
leteckém	letecký	k2eAgInSc6d1	letecký
náletu	nálet	k1gInSc6	nálet
v	v	k7c6	v
jemenském	jemenský	k2eAgInSc6d1	jemenský
Hudaydahu	Hudaydah	k1gInSc6	Hudaydah
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2018	[number]	k4	2018
další	další	k2eAgInSc1d1	další
letecký	letecký	k2eAgInSc1d1	letecký
útok	útok	k1gInSc1	útok
v	v	k7c6	v
Dahyanu	Dahyan	k1gInSc6	Dahyan
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
školní	školní	k2eAgInSc4d1	školní
autobus	autobus	k1gInSc4	autobus
a	a	k8xC	a
zavinil	zavinit	k5eAaPmAgMnS	zavinit
smrt	smrt	k1gFnSc4	smrt
51	[number]	k4	51
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
školou	škola	k1gFnSc7	škola
povinných	povinný	k2eAgFnPc2d1	povinná
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
79	[number]	k4	79
lidí	člověk	k1gMnPc2	člověk
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
zranění	zranění	k1gNnSc1	zranění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Západní	západní	k2eAgFnSc1d1	západní
podpora	podpora	k1gFnSc1	podpora
==	==	k?	==
</s>
</p>
<p>
<s>
Administrativa	administrativa	k1gFnSc1	administrativa
Baracka	Baracka	k1gFnSc1	Baracka
Obamy	Obama	k1gFnSc2	Obama
si	se	k3xPyFc3	se
vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
kritiku	kritika	k1gFnSc4	kritika
některých	některý	k3yIgNnPc2	některý
médií	médium	k1gNnPc2	médium
za	za	k7c4	za
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
logistickou	logistický	k2eAgFnSc4d1	logistická
podporu	podpora	k1gFnSc4	podpora
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
a	a	k8xC	a
k	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
válečným	válečný	k2eAgMnPc3d1	válečný
zločinům	zločin	k1gMnPc3	zločin
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
arabské	arabský	k2eAgFnSc2d1	arabská
koalice	koalice	k1gFnSc2	koalice
vedené	vedený	k2eAgFnSc2d1	vedená
Saúdy	Saúda	k1gFnSc2	Saúda
<g/>
.	.	kIx.	.
</s>
<s>
Obamova	Obamův	k2eAgFnSc1d1	Obamova
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
také	také	k9	také
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
schválila	schválit	k5eAaPmAgFnS	schválit
prodej	prodej	k1gInSc4	prodej
amerických	americký	k2eAgFnPc2d1	americká
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
110	[number]	k4	110
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podpoře	podpora	k1gFnSc6	podpora
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
administrativa	administrativa	k1gFnSc1	administrativa
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
Texetron	Texetron	k1gInSc4	Texetron
Corporation	Corporation	k1gInSc4	Corporation
prodala	prodat	k5eAaPmAgFnS	prodat
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
1300	[number]	k4	1300
kazetových	kazetový	k2eAgFnPc2d1	kazetová
pum	puma	k1gFnPc2	puma
za	za	k7c4	za
641	[number]	k4	641
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
podpořila	podpořit	k5eAaPmAgFnS	podpořit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
vývozce	vývozce	k1gMnPc4	vývozce
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
premiérky	premiérka	k1gFnSc2	premiérka
Mayové	Mayová	k1gFnSc2	Mayová
proto	proto	k8xC	proto
čelila	čelit	k5eAaImAgFnS	čelit
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nepřímo	přímo	k6eNd1	přímo
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
páchání	páchání	k1gNnSc6	páchání
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
a	a	k8xC	a
na	na	k7c6	na
smrti	smrt	k1gFnSc6	smrt
jemenských	jemenský	k2eAgMnPc2d1	jemenský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
ministr	ministr	k1gMnSc1	ministr
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
Boris	Boris	k1gMnSc1	Boris
Johnson	Johnson	k1gMnSc1	Johnson
v	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
OSN	OSN	kA	OSN
zablokoval	zablokovat	k5eAaPmAgMnS	zablokovat
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
<g/>
Vývoz	vývoz	k1gInSc1	vývoz
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
v	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
dojednala	dojednat	k5eAaPmAgFnS	dojednat
také	také	k9	také
německá	německý	k2eAgFnSc1d1	německá
kancléřka	kancléřka	k1gFnSc1	kancléřka
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
se	s	k7c7	s
saúdským	saúdský	k2eAgMnSc7d1	saúdský
korunním	korunní	k2eAgMnSc7d1	korunní
princem	princ	k1gMnSc7	princ
Muhammadem	Muhammad	k1gInSc7	Muhammad
bin	bin	k?	bin
Salmánem	Salmán	k1gMnSc7	Salmán
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
podporu	podpora	k1gFnSc4	podpora
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
prodej	prodej	k1gInSc4	prodej
francouzských	francouzský	k2eAgFnPc2d1	francouzská
zbraní	zbraň	k1gFnPc2	zbraň
prezident	prezident	k1gMnSc1	prezident
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Macron	Macron	k1gMnSc1	Macron
<g/>
.	.	kIx.	.
<g/>
Zbraně	zbraň	k1gFnPc1	zbraň
dodává	dodávat	k5eAaImIp3nS	dodávat
válčícím	válčící	k2eAgFnPc3d1	válčící
stranám	strana	k1gFnPc3	strana
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
také	také	k9	také
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnSc1	International
armáda	armáda	k1gFnSc1	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
"	"	kIx"	"
<g/>
dostává	dostávat	k5eAaImIp3nS	dostávat
od	od	k7c2	od
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
dodávky	dodávka	k1gFnSc2	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
miliardové	miliardový	k2eAgFnSc6d1	miliardová
výši	výše	k1gFnSc6	výše
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obratem	obratem	k6eAd1	obratem
posílá	posílat	k5eAaImIp3nS	posílat
milicím	milice	k1gFnPc3	milice
do	do	k7c2	do
Jemenu	Jemen	k1gInSc2	Jemen
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
prokazatelně	prokazatelně	k6eAd1	prokazatelně
páchají	páchat	k5eAaImIp3nP	páchat
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Některé	některý	k3yIgFnPc4	některý
západní	západní	k2eAgFnPc4d1	západní
zbraně	zbraň	k1gFnPc4	zbraň
používá	používat	k5eAaImIp3nS	používat
teroristická	teroristický	k2eAgFnSc1d1	teroristická
organizace	organizace	k1gFnSc1	organizace
al-Káida	al-Káida	k1gFnSc1	al-Káida
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
jemenským	jemenský	k2eAgMnPc3d1	jemenský
šíitům	šíita	k1gMnPc3	šíita
v	v	k7c6	v
saúdskoarabské	saúdskoarabský	k2eAgFnSc6d1	saúdskoarabská
koalici	koalice	k1gFnSc6	koalice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Humanitární	humanitární	k2eAgFnSc1d1	humanitární
situace	situace	k1gFnSc1	situace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vojenského	vojenský	k2eAgInSc2d1	vojenský
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
blokády	blokáda	k1gFnSc2	blokáda
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
postihl	postihnout	k5eAaPmAgInS	postihnout
Jemen	Jemen	k1gInSc1	Jemen
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
životy	život	k1gInPc4	život
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
000	[number]	k4	000
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2015	[number]	k4	2015
a	a	k8xC	a
2018	[number]	k4	2018
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
OSN	OSN	kA	OSN
okolo	okolo	k6eAd1	okolo
85	[number]	k4	85
000	[number]	k4	000
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zemřely	zemřít	k5eAaPmAgFnP	zemřít
hlady	hlady	k6eAd1	hlady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
3,3	[number]	k4	3,3
milionu	milion	k4xCgInSc2	milion
podvyživených	podvyživený	k2eAgFnPc2d1	podvyživená
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jemen	Jemen	k1gInSc4	Jemen
také	také	k9	také
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
epidemie	epidemie	k1gFnSc1	epidemie
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
Yemen	Yemna	k1gFnPc2	Yemna
Data	datum	k1gNnSc2	datum
Project	Projecta	k1gFnPc2	Projecta
od	od	k7c2	od
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
do	do	k7c2	do
září	září	k1gNnSc2	září
2017	[number]	k4	2017
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
Saúdy	Saúd	k1gInPc4	Saúd
vedená	vedený	k2eAgFnSc1d1	vedená
koalice	koalice	k1gFnSc1	koalice
356	[number]	k4	356
<g/>
krát	krát	k6eAd1	krát
farmy	farma	k1gFnSc2	farma
<g/>
,	,	kIx,	,
174	[number]	k4	174
<g/>
krát	krát	k6eAd1	krát
tržiště	tržiště	k1gNnSc2	tržiště
a	a	k8xC	a
61	[number]	k4	61
<g/>
krát	krát	k6eAd1	krát
skladiště	skladiště	k1gNnPc4	skladiště
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	cíl	k1gInPc1	cíl
sloužící	sloužící	k2eAgInPc1d1	sloužící
k	k	k7c3	k
distribuci	distribuce	k1gFnSc3	distribuce
jídla	jídlo	k1gNnSc2	jídlo
byly	být	k5eAaImAgFnP	být
zasaženy	zasáhnout	k5eAaPmNgFnP	zasáhnout
téměř	téměř	k6eAd1	téměř
600	[number]	k4	600
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
<g/>
Boje	boj	k1gInPc1	boj
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
OSN	OSN	kA	OSN
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
civiliního	civiliní	k2eAgNnSc2d1	civiliní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
při	při	k7c6	při
leteckých	letecký	k2eAgInPc6d1	letecký
náletech	nálet	k1gInPc6	nálet
Saudy	Sauda	k1gFnSc2	Sauda
vedené	vedený	k2eAgFnSc2d1	vedená
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
při	při	k7c6	při
náletu	nálet	k1gInSc6	nálet
přes	přes	k7c4	přes
130	[number]	k4	130
svatebčanů	svatebčan	k1gMnPc2	svatebčan
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
při	při	k7c6	při
leteckém	letecký	k2eAgNnSc6d1	letecké
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
v	v	k7c4	v
Saná	Sané	k1gNnPc4	Sané
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
140	[number]	k4	140
smutečních	smuteční	k2eAgMnPc2d1	smuteční
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
při	při	k7c6	při
leteckém	letecký	k2eAgNnSc6d1	letecké
náletu	nálet	k1gInSc6	nálet
na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
40	[number]	k4	40
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Al-Kajda	Al-Kajdo	k1gNnSc2	Al-Kajdo
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
bojovníky	bojovník	k1gMnPc7	bojovník
al-Kajdy	al-Kajda	k1gFnSc2	al-Kajda
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vládního	vládní	k2eAgNnSc2d1	vládní
vojska	vojsko	k1gNnSc2	vojsko
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
hútíjským	hútíjský	k2eAgMnPc3d1	hútíjský
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
dohody	dohoda	k1gFnPc1	dohoda
umožnily	umožnit	k5eAaPmAgFnP	umožnit
bojovníkům	bojovník	k1gMnPc3	bojovník
al-Kajdy	al-Kajdy	k6eAd1	al-Kajdy
opustit	opustit	k5eAaPmF	opustit
území	území	k1gNnSc4	území
i	i	k9	i
s	s	k7c7	s
výzbrojí	výzbroj	k1gFnSc7	výzbroj
a	a	k8xC	a
bojovou	bojový	k2eAgFnSc7d1	bojová
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
AP	ap	kA	ap
analytika	analytika	k1gFnSc1	analytika
Michaela	Michaela	k1gFnSc1	Michaela
Hortona	Hortona	k1gFnSc1	Hortona
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
dobře	dobře	k6eAd1	dobře
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporou	podpora	k1gFnSc7	podpora
koalice	koalice	k1gFnSc2	koalice
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
i	i	k9	i
tamní	tamní	k2eAgFnSc3d1	tamní
odnoži	odnož	k1gFnSc3	odnož
al-Kajdy	al-Kajda	k1gFnSc2	al-Kajda
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
podpora	podpora	k1gFnSc1	podpora
SAE	SAE	kA	SAE
a	a	k8xC	a
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
USA	USA	kA	USA
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
íránský	íránský	k2eAgInSc1d1	íránský
expanzionismus	expanzionismus	k1gInSc1	expanzionismus
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
al-Kajdě	al-Kajda	k1gFnSc3	al-Kajda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Mezinárodní	mezinárodní	k2eAgInPc1d1	mezinárodní
ohlasy	ohlas	k1gInPc1	ohlas
==	==	k?	==
</s>
</p>
<p>
<s>
Íránští	íránský	k2eAgMnPc1d1	íránský
představitelé	představitel	k1gMnPc1	představitel
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
intervenci	intervence	k1gFnSc4	intervence
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
agresi	agrese	k1gFnSc4	agrese
<g/>
"	"	kIx"	"
a	a	k8xC	a
porušení	porušení	k1gNnPc4	porušení
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
požadovali	požadovat	k5eAaImAgMnP	požadovat
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
zastavení	zastavení	k1gNnSc4	zastavení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
představiteli	představitel	k1gMnPc7	představitel
OSN	OSN	kA	OSN
a	a	k8xC	a
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VISINGR	VISINGR	kA	VISINGR
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Decisive	Decisiev	k1gFnSc2	Decisiev
Storm	Storm	k1gInSc1	Storm
<g/>
:	:	kIx,	:
Intervence	intervence	k1gFnSc1	intervence
arabské	arabský	k2eAgFnSc2d1	arabská
koalice	koalice	k1gFnSc2	koalice
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
47	[number]	k4	47
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
14	[number]	k4	14
až	až	k9	až
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
