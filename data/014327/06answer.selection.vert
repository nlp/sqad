<s>
Barvou	barva	k1gFnSc7	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
fialová	fialový	k2eAgFnSc1d1	fialová
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
2593	[number]	k4	2593
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
barvě	barva	k1gFnSc6	barva
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
provedeno	proveden	k2eAgNnSc1d1	provedeno
logo	logo	k1gNnSc1	logo
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
postava	postava	k1gFnSc1	postava
Spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
a	a	k8xC	a
paprsky	paprsek	k1gInPc7	paprsek
a	a	k8xC	a
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Universitas	Universitas	k1gInSc1	Universitas
Masarykiana	Masarykian	k1gMnSc2	Masarykian
Brunensis	Brunensis	k1gFnSc2	Brunensis
<g/>
.	.	kIx.	.
</s>
