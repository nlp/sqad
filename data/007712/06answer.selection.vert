<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
hrubé	hrubý	k2eAgFnSc2d1	hrubá
skladby	skladba	k1gFnSc2	skladba
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
Zemi	zem	k1gFnSc3	zem
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
planetou	planeta	k1gFnSc7	planeta
<g/>
"	"	kIx"	"
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
