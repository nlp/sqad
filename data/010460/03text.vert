<p>
<s>
Bydhošť	Bydhoštit	k5eAaPmRp2nS	Bydhoštit
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Bydgoszcz	Bydgoszcz	k1gMnSc1	Bydgoszcz
[	[	kIx(	[
<g/>
bydgošč	bydgošč	k1gMnSc1	bydgošč
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Bromberg	Bromberg	k1gInSc1	Bromberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Kujavsko-pomořského	Kujavskoomořský	k2eAgNnSc2d1	Kujavsko-pomořské
vojvodství	vojvodství	k1gNnSc2	vojvodství
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Polsku	Polsko	k1gNnSc6	Polsko
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řek	k1gMnSc1	řek
Brdy	Brdy	k1gInPc4	Brdy
a	a	k8xC	a
Visly	Visla	k1gFnPc4	Visla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
osmým	osmý	k4xOgNnSc7	osmý
největším	veliký	k2eAgNnSc7d3	veliký
polským	polský	k2eAgNnSc7d1	polské
městem	město	k1gNnSc7	město
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
358	[number]	k4	358
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
aglomerací	aglomerace	k1gFnSc7	aglomerace
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
470	[number]	k4	470
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
sídlí	sídlet	k5eAaImIp3nS	sídlet
Univerzita	univerzita	k1gFnSc1	univerzita
Kazimíra	Kazimír	k1gMnSc2	Kazimír
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
rybářská	rybářský	k2eAgFnSc1d1	rybářská
osada	osada	k1gFnSc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1238	[number]	k4	1238
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1331	[number]	k4	1331
ji	on	k3xPp3gFnSc4	on
obsadil	obsadit	k5eAaPmAgInS	obsadit
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1343	[number]	k4	1343
připadla	připadnout	k5eAaPmAgFnS	připadnout
opět	opět	k6eAd1	opět
polskému	polský	k2eAgMnSc3d1	polský
králi	král	k1gMnSc3	král
Kazimíru	Kazimír	k1gMnSc3	Kazimír
Velikému	veliký	k2eAgMnSc3d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
Bydhošti	Bydhošti	k1gFnSc7	Bydhošti
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1346	[number]	k4	1346
udělil	udělit	k5eAaPmAgInS	udělit
městská	městský	k2eAgNnPc4d1	Městské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
začali	začít	k5eAaPmAgMnP	začít
usazovat	usazovat	k5eAaImF	usazovat
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
významným	významný	k2eAgMnSc7d1	významný
centrem	centr	k1gMnSc7	centr
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
pšenicí	pšenice	k1gFnSc7	pšenice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
polsko-švédské	polsko-švédský	k2eAgFnSc2d1	polsko-švédská
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
dobyto	dobýt	k5eAaPmNgNnS	dobýt
a	a	k8xC	a
značně	značně	k6eAd1	značně
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
vojskem	vojsko	k1gNnSc7	vojsko
švédského	švédský	k2eAgMnSc2d1	švédský
krále	král	k1gMnSc2	král
Gustava	Gustav	k1gMnSc2	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc2	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Švédy	švéda	k1gFnSc2	švéda
bylo	být	k5eAaImAgNnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
dobyto	dobýt	k5eAaPmNgNnS	dobýt
ještě	ještě	k9	ještě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1656	[number]	k4	1656
a	a	k8xC	a
1657	[number]	k4	1657
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdejšího	zdejší	k2eAgInSc2d1	zdejší
hradu	hrad	k1gInSc2	hrad
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
zřícenina	zřícenina	k1gFnSc1	zřícenina
a	a	k8xC	a
obydleno	obydlen	k2eAgNnSc1d1	obydleno
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
94	[number]	k4	94
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1657	[number]	k4	1657
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
Janem	Jan	k1gMnSc7	Jan
Kazimírem	Kazimír	k1gMnSc7	Kazimír
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vasou	Vasa	k1gMnSc7	Vasa
a	a	k8xC	a
braniborským	braniborský	k2eAgMnSc7d1	braniborský
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Vilémem	Vilém	k1gMnSc7	Vilém
I.	I.	kA	I.
o	o	k7c6	o
postupu	postup	k1gInSc6	postup
proti	proti	k7c3	proti
Švédům	Švéd	k1gMnPc3	Švéd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
dělení	dělení	k1gNnSc6	dělení
Polska	Polska	k1gFnSc1	Polska
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1772	[number]	k4	1772
město	město	k1gNnSc1	město
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Prusku	Prusko	k1gNnSc3	Prusko
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Bromberg	Bromberg	k1gInSc4	Bromberg
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc1d1	veliký
provedl	provést	k5eAaPmAgInS	provést
velkou	velký	k2eAgFnSc4d1	velká
přestavbu	přestavba	k1gFnSc4	přestavba
Bydhoště	Bydhošť	k1gFnSc2	Bydhošť
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
25	[number]	k4	25
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
Bydhošťský	Bydhošťský	k2eAgInSc1d1	Bydhošťský
kanál	kanál	k1gInSc1	kanál
spojující	spojující	k2eAgFnSc4d1	spojující
Vislu	Visla	k1gFnSc4	Visla
a	a	k8xC	a
Odru	Odra	k1gFnSc4	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Bydhošť	Bydhoštit	k5eAaPmRp2nS	Bydhoštit
se	se	k3xPyFc4	se
tak	tak	k9	tak
stala	stát	k5eAaPmAgFnS	stát
významným	významný	k2eAgNnSc7d1	významné
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Napoleona	Napoleon	k1gMnSc2	Napoleon
se	se	k3xPyFc4	se
Bydhošť	Bydhošť	k1gMnSc1	Bydhošť
Tylžským	Tylžský	k2eAgInSc7d1	Tylžský
mírem	mír	k1gInSc7	mír
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
Poznaňského	poznaňský	k2eAgNnSc2d1	poznaňské
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
a	a	k8xC	a
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
57	[number]	k4	57
700	[number]	k4	700
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
84	[number]	k4	84
%	%	kIx~	%
bylo	být	k5eAaImAgNnS	být
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
16	[number]	k4	16
%	%	kIx~	%
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
Bydhošť	Bydhošť	k1gFnSc1	Bydhošť
stala	stát	k5eAaPmAgFnS	stát
znovu	znovu	k6eAd1	znovu
součástí	součást	k1gFnSc7	součást
Poznaňského	poznaňský	k2eAgNnSc2d1	poznaňské
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
a	a	k8xC	a
zdejší	zdejší	k2eAgNnSc1d1	zdejší
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
muselo	muset	k5eAaImAgNnS	muset
získat	získat	k5eAaPmF	získat
polské	polský	k2eAgNnSc4d1	polské
občanství	občanství	k1gNnSc4	občanství
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
tak	tak	k6eAd1	tak
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
1910	[number]	k4	1910
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
63	[number]	k4	63
000	[number]	k4	000
Němců	Němec	k1gMnPc2	Němec
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
zde	zde	k6eAd1	zde
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
Krvavé	krvavý	k2eAgFnSc6d1	krvavá
neděli	neděle	k1gFnSc6	neděle
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
kolem	kolem	k7c2	kolem
300	[number]	k4	300
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
města	město	k1gNnSc2	město
Němci	Němec	k1gMnPc1	Němec
popravili	popravit	k5eAaPmAgMnP	popravit
stovky	stovka	k1gFnPc4	stovka
polských	polský	k2eAgMnPc2d1	polský
a	a	k8xC	a
židovských	židovský	k2eAgMnPc2d1	židovský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
během	během	k7c2	během
války	válka	k1gFnSc2	válka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
kolem	kolem	k7c2	kolem
37	[number]	k4	37
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
a	a	k8xC	a
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1466	[number]	k4	1466
<g/>
–	–	k?	–
<g/>
1502	[number]	k4	1502
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
stavbou	stavba	k1gFnSc7	stavba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pozdněgotický	pozdněgotický	k2eAgInSc1d1	pozdněgotický
obraz	obraz	k1gInSc1	obraz
Madony	Madona	k1gFnSc2	Madona
s	s	k7c7	s
růží	růž	k1gFnSc7	růž
a	a	k8xC	a
Panna	Panna	k1gFnSc1	Panna
Marie	Maria	k1gFnSc2	Maria
Milosrdné	milosrdný	k2eAgFnSc2d1	milosrdná
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k9	jako
kostel	kostel	k1gInSc1	kostel
klarisek	klariska	k1gFnPc2	klariska
je	být	k5eAaImIp3nS	být
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
gotickorenesanční	gotickorenesanční	k2eAgInSc1d1	gotickorenesanční
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1582	[number]	k4	1582
<g/>
-	-	kIx~	-
<g/>
1602	[number]	k4	1602
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
strop	strop	k1gInSc4	strop
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wyspa	Wyspa	k1gFnSc1	Wyspa
Młyńska	Młyńska	k1gFnSc1	Młyńska
<g/>
,	,	kIx,	,
Mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc1	místo
historickými	historický	k2eAgFnPc7d1	historická
budovami	budova	k1gFnPc7	budova
nedaleko	nedaleko	k7c2	nedaleko
hlavního	hlavní	k2eAgNnSc2d1	hlavní
náměstí	náměstí	k1gNnSc2	náměstí
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
bývala	bývat	k5eAaImAgFnS	bývat
královská	královský	k2eAgFnSc1d1	královská
mincovna	mincovna	k1gFnSc1	mincovna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pochází	pocházet	k5eAaImIp3nS	pocházet
tzv.	tzv.	kA	tzv.
Biały	Biała	k1gFnSc2	Biała
Spichlerz	Spichlerz	k1gInSc1	Spichlerz
(	(	kIx(	(
<g/>
Bíla	bílo	k1gNnSc2	bílo
Sýpka	sýpka	k1gFnSc1	sýpka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hotel	hotel	k1gInSc1	hotel
Pod	pod	k7c4	pod
Orłem	Orłem	k1gInSc4	Orłem
<g/>
,	,	kIx,	,
Hotel	hotel	k1gInSc4	hotel
Adler	Adler	k1gMnSc1	Adler
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
neobarokním	obarokní	k2eNgInSc6d1	neobarokní
slohu	sloh	k1gInSc6	sloh
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
místním	místní	k2eAgMnSc7d1	místní
architektem	architekt	k1gMnSc7	architekt
Józefem	Józef	k1gMnSc7	Józef
Swiecickim	Swiecickim	k1gInSc4	Swiecickim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
asi	asi	k9	asi
60	[number]	k4	60
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Bydhošti	Bydhošti	k1gFnSc6	Bydhošti
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Vincence	Vincenc	k1gMnSc4	Vincenc
z	z	k7c2	z
Pauly	Paula	k1gFnSc2	Paula
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
Bydhošti	Bydhošti	k1gFnSc6	Bydhošti
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
kolem	kolem	k7c2	kolem
12	[number]	k4	12
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
postaven	postavit	k5eAaPmNgMnS	postavit
polským	polský	k2eAgMnSc7d1	polský
architektem	architekt	k1gMnSc7	architekt
Adamem	Adam	k1gMnSc7	Adam
Ballenstedtem	Ballenstedt	k1gMnSc7	Ballenstedt
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
římského	římský	k2eAgInSc2d1	římský
Pantheonu	Pantheon	k1gInSc2	Pantheon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
sýpky	sýpka	k1gFnPc1	sýpka
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Brda	brdo	k1gNnSc2	brdo
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Grodzka	Grodzka	k1gFnSc1	Grodzka
jsou	být	k5eAaImIp3nP	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
výstavní	výstavní	k2eAgInPc4d1	výstavní
prostory	prostor	k1gInPc4	prostor
místního	místní	k2eAgNnSc2d1	místní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
3,5	[number]	k4	3,5
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
letiště	letiště	k1gNnSc1	letiště
Port	porta	k1gFnPc2	porta
lotniczy	lotnicza	k1gFnSc2	lotnicza
im	im	k?	im
<g/>
.	.	kIx.	.
</s>
<s>
Ignacego	Ignacego	k1gMnSc1	Ignacego
Jana	Jan	k1gMnSc2	Jan
Paderewskiego	Paderewskiego	k1gMnSc1	Paderewskiego
Bydgoszcz-Szwederowo	Bydgoszcz-Szwederowo	k1gMnSc1	Bydgoszcz-Szwederowo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
přes	přes	k7c4	přes
343	[number]	k4	343
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
devátým	devátý	k4xOgInSc7	devátý
největším	veliký	k2eAgInSc7d3	veliký
letištěm	letiště	k1gNnSc7	letiště
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
opolských	opolský	k2eAgInPc2d1	opolský
železničních	železniční	k2eAgInPc2d1	železniční
uzlů	uzel	k1gInPc2	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
9	[number]	k4	9
linek	linka	k1gFnPc2	linka
s	s	k7c7	s
29,1	[number]	k4	29,1
km	km	kA	km
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
železniční	železniční	k2eAgFnSc1d1	železniční
továrna	továrna	k1gFnSc1	továrna
PESA	peso	k1gNnSc2	peso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Zawisza	Zawisza	k1gFnSc1	Zawisza
Bydgoszcz	Bydgoszcz	k1gInSc1	Bydgoszcz
–	–	k?	–
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
</s>
</p>
<p>
<s>
SC	SC	kA	SC
Bromberg	Bromberg	k1gInSc1	Bromberg
–	–	k?	–
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
</s>
</p>
<p>
<s>
Reichsbahn	Reichsbahn	k1gInSc1	Reichsbahn
SG	SG	kA	SG
Bromberg	Bromberg	k1gInSc1	Bromberg
–	–	k?	–
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
době	doba	k1gFnSc6	doba
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
SG	SG	kA	SG
Bromberg	Bromberg	k1gInSc1	Bromberg
–	–	k?	–
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
době	doba	k1gFnSc6	doba
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Astoria	Astorium	k1gNnPc4	Astorium
Bydgoszcz	Bydgoszcz	k1gInSc1	Bydgoszcz
–	–	k?	–
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
</s>
</p>
<p>
<s>
RTW	RTW	kA	RTW
Bydgostia	Bydgostia	k1gFnSc1	Bydgostia
Bydgoszcz	Bydgoszcz	k1gInSc1	Bydgoszcz
–	–	k?	–
veslařský	veslařský	k2eAgInSc1d1	veslařský
klub	klub	k1gInSc1	klub
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Eberhard	Eberhard	k1gInSc1	Eberhard
von	von	k1gInSc1	von
Mackensen	Mackensen	k1gInSc1	Mackensen
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
</s>
</p>
<p>
<s>
Marian	Marian	k1gMnSc1	Marian
Rejewski	Rejewsk	k1gFnSc2	Rejewsk
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
</s>
</p>
<p>
<s>
Radosław	Radosław	k?	Radosław
Sikorski	Sikorski	k1gNnSc1	Sikorski
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maršálek	maršálek	k1gMnSc1	maršálek
Sejmu	sejmout	k5eAaPmIp1nS	sejmout
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Reggio	Reggio	k6eAd1	Reggio
Emilia	Emilia	k1gFnSc1	Emilia
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kragujevac	Kragujevac	k1gInSc1	Kragujevac
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mannheim	Mannheim	k1gInSc1	Mannheim
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hartford	Hartford	k1gInSc1	Hartford
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavlodar	Pavlodar	k1gInSc1	Pavlodar
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Perth	Perth	k1gInSc1	Perth
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čerkasy	Čerkasa	k1gFnPc1	Čerkasa
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kremenčuk	Kremenčuk	k1gInSc1	Kremenčuk
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Patra	patro	k1gNnPc1	patro
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ning-po	Ninga	k1gFnSc5	Ning-pa
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wilhelmshaven	Wilhelmshaven	k2eAgMnSc1d1	Wilhelmshaven
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Piteș	Piteș	k?	Piteș
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Bydhošti	Bydhošti	k1gFnSc6	Bydhošti
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bydhošť	Bydhošť	k1gFnSc2	Bydhošť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Foto	foto	k1gNnSc1	foto
Galeria	Galerium	k1gNnSc2	Galerium
Bydgoska	Bydgoska	k1gFnSc1	Bydgoska
–	–	k?	–
foto	foto	k1gNnSc1	foto
<g/>
.	.	kIx.	.
<g/>
bydgoszcz	bydgoszcz	k1gInSc1	bydgoszcz
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
Galerie	galerie	k1gFnSc1	galerie
Bydhošť	Bydhošť	k1gFnSc2	Bydhošť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
