<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1685	[number]	k4	1685
Eisenach	Eisenach	k1gInSc1	Eisenach
–	–	k?	–
28.	[number]	k4	28.
července	červenec	k1gInSc2	červenec
1750	[number]	k4	1750
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
virtuos	virtuos	k1gMnSc1	virtuos
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
hudebních	hudební	k2eAgMnPc2d1	hudební
géniů	génius	k1gMnPc2	génius
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
završitele	završitel	k1gMnSc2	završitel
barokního	barokní	k2eAgInSc2d1	barokní
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Bachovo	Bachův	k2eAgNnSc1d1	Bachovo
dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
hudby	hudba	k1gFnSc2	hudba
počínaje	počínaje	k7c7	počínaje
W.	W.	kA	W.
A.	A.	kA	A.
Mozartem	Mozart	k1gMnSc7	Mozart
a	a	k8xC	a
Ludwigem	Ludwig	k1gMnSc7	Ludwig
van	vana	k1gFnPc2	vana
Beethovenem	Beethoven	k1gMnSc7	Beethoven
až	až	k9	až
po	po	k7c4	po
Arnolda	Arnold	k1gMnSc4	Arnold
Schoenberga	Schoenberg	k1gMnSc4	Schoenberg
nebo	nebo	k8xC	nebo
Henryka	Henryek	k1gMnSc4	Henryek
Góreckého	Górecký	k2eAgMnSc4d1	Górecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k9	především
jako	jako	k9	jako
interpret	interpret	k1gMnSc1	interpret
a	a	k8xC	a
improvizátor	improvizátor	k1gMnSc1	improvizátor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
skladatel	skladatel	k1gMnSc1	skladatel
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
uznáván	uznávat	k5eAaImNgMnS	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Bachova	Bachův	k2eAgFnSc1d1	Bachova
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
upadla	upadnout	k5eAaPmAgFnS	upadnout
na	na	k7c4	na
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
zapomenutí	zapomenutí	k1gNnSc6	zapomenutí
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
počínaje	počínaje	k7c7	počínaje
Felixem	Felix	k1gMnSc7	Felix
Mendelssohnem	Mendelssohn	k1gMnSc7	Mendelssohn
Bartholdym	Bartholdym	k1gInSc4	Bartholdym
se	se	k3xPyFc4	se
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19.	[number]	k4	19.
století	století	k1gNnPc2	století
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
opět	opět	k6eAd1	opět
více	hodně	k6eAd2	hodně
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
světských	světský	k2eAgFnPc6d1	světská
i	i	k8xC	i
církevních	církevní	k2eAgFnPc6d1	církevní
službách	služba	k1gFnPc6	služba
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
jeho	jeho	k3xOp3gNnSc2	jeho
působiště	působiště	k1gNnSc2	působiště
byla	být	k5eAaImAgFnS	být
Výmar	Výmar	k1gInSc4	Výmar
<g/>
,	,	kIx,	,
Köthen	Köthen	k1gInSc4	Köthen
a	a	k8xC	a
Lipsko	Lipsko	k1gNnSc4	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnSc7	jeho
nejznámější	známý	k2eAgFnSc1d3	nejznámější
kompozice	kompozice	k1gFnSc1	kompozice
patří	patřit	k5eAaImIp3nS	patřit
Braniborské	braniborský	k2eAgInPc4d1	braniborský
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc4d1	temperovaný
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
Mše	mše	k1gFnSc1	mše
h	h	k?	h
moll	moll	k1gNnSc1	moll
<g/>
,	,	kIx,	,
Matoušovy	Matoušův	k2eAgFnPc1d1	Matoušova
pašije	pašije	k1gFnPc1	pašije
<g/>
,	,	kIx,	,
Vánoční	vánoční	k2eAgNnSc1d1	vánoční
oratorium	oratorium	k1gNnSc1	oratorium
<g/>
,	,	kIx,	,
Hudební	hudební	k2eAgFnSc1d1	hudební
obětina	obětina	k1gFnSc1	obětina
<g/>
,	,	kIx,	,
Goldbergovy	Goldbergův	k2eAgFnPc1d1	Goldbergova
variace	variace	k1gFnPc1	variace
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
opus	opus	k1gInSc1	opus
Umění	umění	k1gNnSc2	umění
fugy	fuga	k1gFnSc2	fuga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
20.	[number]	k4	20.
do	do	k7c2	do
22.	[number]	k4	22.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
spustil	spustit	k5eAaPmAgInS	spustit
Google	Google	k1gInSc1	Google
Doodle	Doodle	k1gFnSc2	Doodle
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
připomínat	připomínat	k5eAaImF	připomínat
Johanna	Johann	k1gMnSc4	Johann
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Bacha	Bacha	k?	Bacha
a	a	k8xC	a
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vyhledávače	vyhledávač	k1gMnSc2	vyhledávač
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
o	o	k7c4	o
první	první	k4xOgFnSc4	první
hru	hra	k1gFnSc4	hra
od	od	k7c2	od
Google	Google	k1gFnSc2	Google
Doodle	Doodle	k1gFnSc2	Doodle
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
naprogramována	naprogramovat	k5eAaPmNgFnS	naprogramovat
s	s	k7c7	s
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
zadal	zadat	k5eAaPmAgMnS	zadat
na	na	k7c4	na
notovou	notový	k2eAgFnSc4d1	notová
osnovu	osnova	k1gFnSc4	osnova
několik	několik	k4yIc4	několik
not	nota	k1gFnPc2	nota
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
čtyři	čtyři	k4xCgFnPc4	čtyři
<g/>
)	)	kIx)	)
a	a	k8xC	a
systém	systém	k1gInSc4	systém
automaticky	automaticky	k6eAd1	automaticky
vygeneroval	vygenerovat	k5eAaPmAgMnS	vygenerovat
čtyři	čtyři	k4xCgFnPc4	čtyři
tónové	tónový	k2eAgFnPc4d1	tónová
linky	linka	k1gFnPc4	linka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výsledná	výsledný	k2eAgFnSc1d1	výsledná
hudba	hudba	k1gFnSc1	hudba
zněla	znět	k5eAaImAgFnS	znět
v	v	k7c6	v
Bachově	Bachův	k2eAgInSc6d1	Bachův
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgInS	učit
generovat	generovat	k5eAaImF	generovat
skladby	skladba	k1gFnPc4	skladba
díky	díky	k7c3	díky
znalosti	znalost	k1gFnSc3	znalost
306	[number]	k4	306
Bachových	Bachová	k1gFnPc2	Bachová
kompozic	kompozice	k1gFnPc2	kompozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
duryňském	duryňský	k2eAgInSc6d1	duryňský
Eisenachu	Eisenach	k1gInSc6	Eisenach
v	v	k7c6	v
Sasko-eisenašském	Saskoisenašský	k2eAgNnSc6d1	Sasko-eisenašský
vévodství	vévodství	k1gNnSc6	vévodství
ve	v	k7c6	v
středovýchodním	středovýchodní	k2eAgNnSc6d1	středovýchodní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
syn	syn	k1gMnSc1	syn
dvorního	dvorní	k2eAgMnSc2d1	dvorní
městského	městský	k2eAgMnSc2d1	městský
hudebníka	hudebník	k1gMnSc2	hudebník
Johanna	Johann	k1gMnSc4	Johann
Ambrozia	Ambrozius	k1gMnSc4	Ambrozius
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
tradice	tradice	k1gFnSc1	tradice
rodu	rod	k1gInSc2	rod
byla	být	k5eAaImAgFnS	být
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
předkové	předek	k1gMnPc1	předek
byli	být	k5eAaImAgMnP	být
hudebníky	hudebník	k1gMnPc4	hudebník
téměř	téměř	k6eAd1	téměř
po	po	k7c4	po
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
projevoval	projevovat	k5eAaImAgMnS	projevovat
výrazné	výrazný	k2eAgNnSc4d1	výrazné
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
již	již	k9	již
od	od	k7c2	od
útlých	útlý	k2eAgNnPc2d1	útlé
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1694	[number]	k4	1694
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
1695	[number]	k4	1695
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
oporou	opora	k1gFnSc7	opora
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Johann	Johann	k1gMnSc1	Johann
Christoph	Christoph	k1gMnSc1	Christoph
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
do	do	k7c2	do
základů	základ	k1gInPc2	základ
varhanní	varhanní	k2eAgFnSc2d1	varhanní
a	a	k8xC	a
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
i	i	k8xC	i
do	do	k7c2	do
hudební	hudební	k2eAgFnSc2d1	hudební
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
ostatního	ostatní	k2eAgNnSc2d1	ostatní
vzdělání	vzdělání	k1gNnSc2	vzdělání
získal	získat	k5eAaPmAgMnS	získat
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
na	na	k7c6	na
protestantské	protestantský	k2eAgFnSc6d1	protestantská
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Eisenachu	Eisenach	k1gInSc6	Eisenach
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
však	však	k9	však
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
odešel	odejít	k5eAaPmAgMnS	odejít
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
Johannem	Johann	k1gMnSc7	Johann
Christophem	Christoph	k1gInSc7	Christoph
do	do	k7c2	do
Ohrdrufu	Ohrdruf	k1gInSc2	Ohrdruf
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tam	tam	k6eAd1	tam
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
předal	předat	k5eAaPmAgInS	předat
další	další	k2eAgInSc4d1	další
hudební	hudební	k2eAgFnSc4d1	hudební
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
špatné	špatný	k2eAgFnSc3d1	špatná
finanční	finanční	k2eAgFnSc3d1	finanční
situaci	situace	k1gFnSc3	situace
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
byl	být	k5eAaImAgMnS	být
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
nucen	nutit	k5eAaImNgMnS	nutit
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
15	[number]	k4	15
letech	let	k1gInPc6	let
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Lüneburgu	Lüneburg	k1gInSc2	Lüneburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sborovým	sborový	k2eAgMnSc7d1	sborový
sopranistou	sopranista	k1gMnSc7	sopranista
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
strávil	strávit	k5eAaPmAgMnS	strávit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
dokončil	dokončit	k5eAaPmAgMnS	dokončit
humanisticko-teologické	humanistickoeologický	k2eAgNnSc4d1	humanisticko-teologický
středoškolské	středoškolský	k2eAgNnSc4d1	středoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
si	se	k3xPyFc3	se
své	své	k1gNnSc4	své
hudební	hudební	k2eAgFnSc2d1	hudební
znalosti	znalost	k1gFnSc2	znalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Arnstadt	Arnstadtum	k1gNnPc2	Arnstadtum
===	===	k?	===
</s>
</p>
<p>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
března	březen	k1gInSc2	březen
1703	[number]	k4	1703
Bach	Bach	k1gMnSc1	Bach
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
houslista	houslista	k1gMnSc1	houslista
soukromé	soukromý	k2eAgFnSc2d1	soukromá
kapely	kapela	k1gFnSc2	kapela
výmarského	výmarský	k2eAgMnSc2d1	výmarský
spoluregenta	spoluregent	k1gMnSc2	spoluregent
Johanna	Johann	k1gMnSc2	Johann
Ernsta	Ernst	k1gMnSc2	Ernst
von	von	k1gInSc1	von
Sachsen-Weimar	Sachsen-Weimar	k1gInSc1	Sachsen-Weimar
<g/>
.	.	kIx.	.
9.	[number]	k4	9.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
varhaníka	varhaník	k1gMnSc4	varhaník
Nového	Nového	k2eAgInSc2d1	Nového
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
Neue	Neue	k1gInSc1	Neue
Kirche	Kirch	k1gInSc2	Kirch
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Bachkirche	Bachkirche	k1gNnSc1	Bachkirche
<g/>
)	)	kIx)	)
v	v	k7c6	v
Arnstadtu	Arnstadt	k1gInSc6	Arnstadt
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nový	nový	k2eAgInSc4d1	nový
dvojmanuálový	dvojmanuálový	k2eAgInSc4d1	dvojmanuálový
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
sestrojený	sestrojený	k2eAgInSc4d1	sestrojený
významným	významný	k2eAgMnSc7d1	významný
stavitelem	stavitel	k1gMnSc7	stavitel
varhan	varhany	k1gFnPc2	varhany
Johannem	Johann	k1gMnSc7	Johann
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Wenderem	Wender	k1gMnSc7	Wender
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1705	[number]	k4	1705
dostal	dostat	k5eAaPmAgMnS	dostat
Bach	Bach	k1gMnSc1	Bach
třítýdenní	třítýdenní	k2eAgFnSc4d1	třítýdenní
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navštívil	navštívit	k5eAaPmAgMnS	navštívit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
skladatelů	skladatel	k1gMnPc2	skladatel
a	a	k8xC	a
varhaníků	varhaník	k1gMnPc2	varhaník
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Dietricha	Dietrich	k1gMnSc2	Dietrich
Buxtehudeho	Buxtehude	k1gMnSc2	Buxtehude
<g/>
.	.	kIx.	.
</s>
<s>
Bach	Bach	k1gInSc4	Bach
si	se	k3xPyFc3	se
pobyt	pobyt	k1gInSc4	pobyt
u	u	k7c2	u
Buxtehuda	Buxtehudo	k1gNnSc2	Buxtehudo
bez	bez	k7c2	bez
dovolení	dovolení	k1gNnSc2	dovolení
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Arnstadtu	Arnstadt	k1gInSc2	Arnstadt
napomínán	napomínat	k5eAaImNgMnS	napomínat
tamní	tamní	k2eAgFnSc7d1	tamní
konzistoří	konzistoř	k1gFnSc7	konzistoř
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
nedbalost	nedbalost	k1gFnSc4	nedbalost
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Buxtehude	Buxtehude	k6eAd1	Buxtehude
se	se	k3xPyFc4	se
však	však	k9	však
pro	pro	k7c4	pro
Bacha	Bacha	k?	Bacha
stal	stát	k5eAaPmAgInS	stát
cenným	cenný	k2eAgMnSc7d1	cenný
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
hudebním	hudební	k2eAgInSc7d1	hudební
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vysledovat	vysledovat	k5eAaImF	vysledovat
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
Bachových	Bachův	k2eAgFnPc2d1	Bachova
kompozic	kompozice	k1gFnPc2	kompozice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
chorálních	chorální	k2eAgFnPc6d1	chorální
předehrách	předehra	k1gFnPc6	předehra
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Wie	Wie	k1gMnSc1	Wie
schön	schön	k1gMnSc1	schön
leuchtet	leuchtet	k1gMnSc1	leuchtet
der	drát	k5eAaImRp2nS	drát
Morgenstern	Morgenstern	k1gMnSc1	Morgenstern
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
739	[number]	k4	739
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
preludiích	preludie	k1gFnPc6	preludie
<g/>
,	,	kIx,	,
tokátách	tokáta	k1gFnPc6	tokáta
a	a	k8xC	a
fantaziích	fantazie	k1gFnPc6	fantazie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
finanční	finanční	k2eAgFnPc1d1	finanční
podmínky	podmínka	k1gFnPc1	podmínka
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
Arnstadtu	Arnstadto	k1gNnSc6	Arnstadto
byly	být	k5eAaImAgInP	být
dobré	dobrý	k2eAgInPc1d1	dobrý
<g/>
,	,	kIx,	,
Bach	Bach	k1gInSc1	Bach
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
tamní	tamní	k2eAgFnSc7d1	tamní
konzistoří	konzistoř	k1gFnSc7	konzistoř
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
svévolného	svévolný	k2eAgNnSc2d1	svévolné
prodloužení	prodloužení	k1gNnSc2	prodloužení
studijního	studijní	k2eAgInSc2d1	studijní
pobytu	pobyt	k1gInSc2	pobyt
u	u	k7c2	u
Buxtehuda	Buxtehudo	k1gNnSc2	Buxtehudo
nebo	nebo	k8xC	nebo
stížnosti	stížnost	k1gFnSc2	stížnost
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bach	Bach	k1gMnSc1	Bach
dovolil	dovolit	k5eAaPmAgMnS	dovolit
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
kůr	kůr	k1gInSc4	kůr
"	"	kIx"	"
<g/>
cizí	cizí	k2eAgFnSc3d1	cizí
dívce	dívka	k1gFnSc3	dívka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
snad	snad	k9	snad
své	svůj	k3xOyFgFnSc3	svůj
budoucí	budoucí	k2eAgFnSc3d1	budoucí
ženě	žena	k1gFnSc3	žena
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
především	především	k9	především
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bachova	Bachova	k1gFnSc1	Bachova
novátorská	novátorský	k2eAgFnSc1d1	novátorská
a	a	k8xC	a
emotivní	emotivní	k2eAgFnSc1d1	emotivní
hudba	hudba	k1gFnSc1	hudba
působila	působit	k5eAaImAgFnS	působit
na	na	k7c4	na
místní	místní	k2eAgMnPc4d1	místní
posluchače	posluchač	k1gMnPc4	posluchač
rušivě	rušivě	k6eAd1	rušivě
a	a	k8xC	a
rozptylovala	rozptylovat	k5eAaImAgFnS	rozptylovat
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
komponista	komponista	k1gMnSc1	komponista
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
utéct	utéct	k5eAaPmF	utéct
ze	z	k7c2	z
stísněných	stísněný	k2eAgInPc2d1	stísněný
arnstadtských	arnstadtský	k2eAgInPc2d1	arnstadtský
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
vyhledat	vyhledat	k5eAaPmF	vyhledat
si	se	k3xPyFc3	se
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mühlhausen	Mühlhausen	k1gInSc1	Mühlhausen
a	a	k8xC	a
Výmar	Výmar	k1gInSc1	Výmar
===	===	k?	===
</s>
</p>
<p>
<s>
24.	[number]	k4	24.
dubna	duben	k1gInSc2	duben
1707	[number]	k4	1707
hrál	hrát	k5eAaImAgInS	hrát
Bach	Bach	k1gInSc1	Bach
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
v	v	k7c4	v
Mühlhausenu	Mühlhausen	k2eAgFnSc4d1	Mühlhausen
a	a	k8xC	a
od	od	k7c2	od
1.	[number]	k4	1.
července	červenec	k1gInSc2	červenec
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
místo	místo	k1gNnSc4	místo
varhaníka	varhaník	k1gMnSc2	varhaník
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
kostele	kostel	k1gInSc6	kostel
Divi	Dive	k1gFnSc4	Dive
Blasii	Blasie	k1gFnSc4	Blasie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Mühlhausenu	Mühlhausen	k2eAgFnSc4d1	Mühlhausen
setrval	setrvat	k5eAaPmAgInS	setrvat
zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc1	rok
–	–	k?	–
o	o	k7c4	o
propuštění	propuštění	k1gNnSc4	propuštění
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
požádal	požádat	k5eAaPmAgMnS	požádat
25.	[number]	k4	25.
června	červen	k1gInSc2	červen
1708	[number]	k4	1708
<g/>
.	.	kIx.	.
</s>
<s>
Výmarský	výmarský	k2eAgMnSc1d1	výmarský
vévoda	vévoda	k1gMnSc1	vévoda
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
skladatele	skladatel	k1gMnSc2	skladatel
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
návštěvě	návštěva	k1gFnSc6	návštěva
Výmaru	Výmar	k1gInSc2	Výmar
slyšel	slyšet	k5eAaImAgInS	slyšet
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
lépe	dobře	k6eAd2	dobře
placené	placený	k2eAgNnSc4d1	placené
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Bach	Bach	k1gMnSc1	Bach
17.	[number]	k4	17.
října	říjen	k1gInSc2	říjen
1707	[number]	k4	1707
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Barbarou	Barbara	k1gFnSc7	Barbara
Bachovou	Bachová	k1gFnSc7	Bachová
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
vzdálenou	vzdálený	k2eAgFnSc7d1	vzdálená
sestřenicí	sestřenice	k1gFnSc7	sestřenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
těhotnou	těhotný	k2eAgFnSc7d1	těhotná
ženou	žena	k1gFnSc7	žena
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
do	do	k7c2	do
Výmaru	Výmar	k1gInSc2	Výmar
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
1708	[number]	k4	1708
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
zde	zde	k6eAd1	zde
jako	jako	k8xS	jako
dvorní	dvorní	k2eAgMnSc1d1	dvorní
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
komorní	komorní	k1gMnSc1	komorní
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Výmarský	výmarský	k2eAgInSc1d1	výmarský
kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
inspirativnější	inspirativní	k2eAgMnSc1d2	inspirativnější
a	a	k8xC	a
bohatší	bohatý	k2eAgMnSc1d2	bohatší
než	než	k8xS	než
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
působišti	působiště	k1gNnSc6	působiště
a	a	k8xC	a
Bachův	Bachův	k2eAgMnSc1d1	Bachův
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velkorysým	velkorysý	k2eAgMnSc7d1	velkorysý
a	a	k8xC	a
uměnímilovným	uměnímilovný	k2eAgMnSc7d1	uměnímilovný
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
výmarského	výmarský	k2eAgNnSc2d1	výmarské
působení	působení	k1gNnSc2	působení
pochází	pocházet	k5eAaImIp3nS	pocházet
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Bachova	Bachův	k2eAgNnSc2d1	Bachovo
varhanního	varhanní	k2eAgNnSc2d1	varhanní
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Lovecká	lovecký	k2eAgFnSc1d1	lovecká
kantáta	kantáta	k1gFnSc1	kantáta
BWV	BWV	kA	BWV
208	[number]	k4	208
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
Bachova	Bachův	k2eAgFnSc1d1	Bachova
světská	světský	k2eAgFnSc1d1	světská
kantáta	kantáta	k1gFnSc1	kantáta
<g/>
.	.	kIx.	.
</s>
<s>
Bachům	Bach	k1gInPc3	Bach
se	se	k3xPyFc4	se
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
také	také	k9	také
narodilo	narodit	k5eAaPmAgNnS	narodit
jejich	jejich	k3xOp3gNnSc1	jejich
prvních	první	k4xOgNnPc6	první
šest	šest	k4xCc1	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dvě	dva	k4xCgFnPc1	dva
ovšem	ovšem	k9	ovšem
brzy	brzy	k6eAd1	brzy
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1713	[number]	k4	1713
se	se	k3xPyFc4	se
Bach	Bach	k1gMnSc1	Bach
z	z	k7c2	z
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
varhaníka	varhaník	k1gMnSc2	varhaník
v	v	k7c6	v
Halle	Halla	k1gFnSc6	Halla
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
podepsat	podepsat	k5eAaPmF	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabízená	nabízený	k2eAgFnSc1d1	nabízená
mzda	mzda	k1gFnSc1	mzda
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
jeho	jeho	k3xOp3gNnSc4	jeho
očekávání	očekávání	k1gNnSc4	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ernst	Ernst	k1gMnSc1	Ernst
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Bacha	Bacha	k?	Bacha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1714	[number]	k4	1714
koncertním	koncertní	k2eAgMnSc7d1	koncertní
mistrem	mistr	k1gMnSc7	mistr
<g/>
,	,	kIx,	,
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
dvorních	dvorní	k2eAgMnPc2d1	dvorní
hudebníků	hudebník	k1gMnPc2	hudebník
až	až	k8xS	až
třetí	třetí	k4xOgMnSc1	třetí
po	po	k7c6	po
kapelníkovi	kapelník	k1gMnSc6	kapelník
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
zástupci	zástupce	k1gMnPc1	zástupce
<g/>
,	,	kIx,	,
Bach	Bach	k1gMnSc1	Bach
dostával	dostávat	k5eAaImAgMnS	dostávat
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
plat	plat	k1gInSc4	plat
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
pozice	pozice	k1gFnSc1	pozice
znamenala	znamenat	k5eAaImAgFnS	znamenat
i	i	k9	i
nové	nový	k2eAgFnPc4d1	nová
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jiným	jiné	k1gNnSc7	jiné
musel	muset	k5eAaImAgMnS	muset
Bach	Bach	k1gMnSc1	Bach
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
komponovat	komponovat	k5eAaImF	komponovat
jednu	jeden	k4xCgFnSc4	jeden
církevní	církevní	k2eAgFnSc4d1	církevní
kantátu	kantáta	k1gFnSc4	kantáta
pro	pro	k7c4	pro
nedělní	nedělní	k2eAgFnPc4d1	nedělní
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
–	–	k?	–
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kantáta	kantáta	k1gFnSc1	kantáta
Himmelskönig	Himmelskönig	k1gInSc1	Himmelskönig
<g/>
,	,	kIx,	,
sei	sei	k?	sei
willkommen	willkommen	k1gInSc1	willkommen
BWV	BWV	kA	BWV
182	[number]	k4	182
<g/>
.	.	kIx.	.
</s>
<s>
Bachovým	Bachův	k2eAgMnSc7d1	Bachův
hudebním	hudební	k2eAgMnSc7d1	hudební
přítelem	přítel	k1gMnSc7	přítel
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
drážďanský	drážďanský	k2eAgMnSc1d1	drážďanský
hudební	hudební	k2eAgMnSc1d1	hudební
ředitel	ředitel	k1gMnSc1	ředitel
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Pisendel	Pisendlo	k1gNnPc2	Pisendlo
(	(	kIx(	(
<g/>
1687	[number]	k4	1687
<g/>
–	–	k?	–
<g/>
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Bachovi	Bach	k1gMnSc3	Bach
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
italskou	italský	k2eAgFnSc4d1	italská
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
vliv	vliv	k1gInSc4	vliv
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
Antonia	Antonio	k1gMnSc2	Antonio
Vivaldiho	Vivaldi	k1gMnSc2	Vivaldi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
setrval	setrvat	k5eAaPmAgInS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1717	[number]	k4	1717
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
post	post	k1gInSc4	post
kapelníka	kapelník	k1gMnSc4	kapelník
v	v	k7c6	v
Köthenu	Köthen	k1gInSc6	Köthen
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
ovšem	ovšem	k9	ovšem
předem	předem	k6eAd1	předem
požádal	požádat	k5eAaPmAgMnS	požádat
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
o	o	k7c4	o
uvolnění	uvolnění	k1gNnSc4	uvolnění
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
proto	proto	k8xC	proto
k	k	k7c3	k
otevřenému	otevřený	k2eAgInSc3d1	otevřený
konfliktu	konflikt	k1gInSc3	konflikt
s	s	k7c7	s
panovníkem	panovník	k1gMnSc7	panovník
a	a	k8xC	a
Bach	Bach	k1gInSc4	Bach
byl	být	k5eAaImAgMnS	být
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
a	a	k8xC	a
poté	poté	k6eAd1	poté
2.	[number]	k4	2.
prosince	prosinec	k1gInSc2	prosinec
1717	[number]	k4	1717
v	v	k7c6	v
nemilosti	nemilost	k1gFnSc6	nemilost
propuštěn	propuštěn	k2eAgInSc4d1	propuštěn
z	z	k7c2	z
vězení	vězení	k1gNnPc2	vězení
i	i	k9	i
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Köthen	Köthno	k1gNnPc2	Köthno
===	===	k?	===
</s>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
v	v	k7c6	v
Köthenu	Köthen	k1gInSc6	Köthen
získal	získat	k5eAaPmAgMnS	získat
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
osobnímu	osobní	k2eAgNnSc3d1	osobní
přátelství	přátelství	k1gNnSc3	přátelství
s	s	k7c7	s
hudbymilovným	hudbymilovný	k2eAgMnSc7d1	hudbymilovný
knížetem	kníže	k1gMnSc7	kníže
Leopoldem	Leopold	k1gMnSc7	Leopold
von	von	k1gInSc4	von
Anhalt-Köthen	Anhalt-Köthna	k1gFnPc2	Anhalt-Köthna
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
zde	zde	k6eAd1	zde
titul	titul	k1gInSc1	titul
kapelníka	kapelník	k1gMnSc2	kapelník
a	a	k8xC	a
ředitele	ředitel	k1gMnSc2	ředitel
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostal	dostat	k5eAaPmAgMnS	dostat
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
sedmnáctičlennou	sedmnáctičlenný	k2eAgFnSc4d1	sedmnáctičlenná
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
nešetřil	šetřit	k5eNaImAgMnS	šetřit
ani	ani	k8xC	ani
na	na	k7c6	na
nástrojovém	nástrojový	k2eAgNnSc6d1	nástrojové
vybavení	vybavení	k1gNnSc6	vybavení
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bach	Bach	k1gInSc4	Bach
tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
1719	[number]	k4	1719
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
zakoupit	zakoupit	k5eAaPmF	zakoupit
nové	nový	k2eAgNnSc4d1	nové
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
poznal	poznat	k5eAaPmAgMnS	poznat
hudbymilovného	hudbymilovný	k2eAgMnSc4d1	hudbymilovný
braniborského	braniborský	k2eAgMnSc4d1	braniborský
markraběte	markrabě	k1gMnSc4	markrabě
Christiana	Christian	k1gMnSc4	Christian
Ludwiga	Ludwig	k1gMnSc4	Ludwig
(	(	kIx(	(
<g/>
1677-1734	[number]	k4	1677-1734
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgMnSc4	jenž
pak	pak	k6eAd1	pak
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
slavné	slavný	k2eAgFnPc4d1	slavná
instrumentální	instrumentální	k2eAgFnPc4d1	instrumentální
skladby	skladba	k1gFnPc4	skladba
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
Braniborské	braniborský	k2eAgInPc1d1	braniborský
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1720	[number]	k4	1720
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Bach	Bach	k1gInSc1	Bach
s	s	k7c7	s
knížecím	knížecí	k2eAgInSc7d1	knížecí
dvorem	dvůr	k1gInSc7	dvůr
na	na	k7c6	na
dvouměsíčním	dvouměsíční	k2eAgInSc6d1	dvouměsíční
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
v	v	k7c6	v
Köthenu	Kötheno	k1gNnSc6	Kötheno
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
a	a	k8xC	a
náhle	náhle	k6eAd1	náhle
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
Marie	Maria	k1gFnSc2	Maria
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
.	.	kIx.	.
3.	[number]	k4	3.
prosince	prosinec	k1gInSc2	prosinec
1721	[number]	k4	1721
si	se	k3xPyFc3	se
Bach	Bach	k1gMnSc1	Bach
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Annu	Anna	k1gFnSc4	Anna
Magdalenu	Magdalena	k1gFnSc4	Magdalena
rozenou	rozený	k2eAgFnSc4d1	rozená
Wilcke	Wilcke	k1gFnSc4	Wilcke
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
dvorního	dvorní	k2eAgMnSc2d1	dvorní
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stala	stát	k5eAaPmAgFnS	stát
vzornou	vzorný	k2eAgFnSc7d1	vzorná
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
dala	dát	k5eAaPmAgFnS	dát
dalších	další	k2eAgNnPc6d1	další
třináct	třináct	k4xCc4	třináct
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
celkem	celkem	k6eAd1	celkem
dvaceti	dvacet	k4xCc2	dvacet
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většina	většina	k1gFnSc1	většina
ovšem	ovšem	k9	ovšem
zemřela	zemřít	k5eAaPmAgFnS	zemřít
již	již	k6eAd1	již
v	v	k7c6	v
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Magdalena	Magdalena	k1gFnSc1	Magdalena
Bachová	Bachová	k1gFnSc1	Bachová
byla	být	k5eAaImAgFnS	být
i	i	k9	i
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
spolupracovnicí	spolupracovnice	k1gFnSc7	spolupracovnice
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
opisovala	opisovat	k5eAaImAgFnS	opisovat
jeho	jeho	k3xOp3gFnPc4	jeho
skladby	skladba	k1gFnPc4	skladba
a	a	k8xC	a
podporovala	podporovat	k5eAaImAgFnS	podporovat
jeho	jeho	k3xOp3gFnPc4	jeho
tvůrčí	tvůrčí	k2eAgFnPc4d1	tvůrčí
aktivity	aktivita	k1gFnPc4	aktivita
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Köthenu	Köthen	k1gInSc6	Köthen
Bach	Bach	k1gMnSc1	Bach
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
(	(	kIx(	(
<g/>
1	[number]	k4	1
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
846	[number]	k4	846
<g/>
–	–	k?	–
<g/>
869	[number]	k4	869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
houslových	houslový	k2eAgFnPc2d1	houslová
partit	partita	k1gFnPc2	partita
a	a	k8xC	a
sonát	sonáta	k1gFnPc2	sonáta
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
závažných	závažný	k2eAgFnPc2d1	závažná
kompozic	kompozice	k1gFnPc2	kompozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
díla	dílo	k1gNnSc2	dílo
spíše	spíše	k9	spíše
pedagogického	pedagogický	k2eAgNnSc2d1	pedagogické
určení	určení	k1gNnSc2	určení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
knížka	knížka	k1gFnSc1	knížka
Anny	Anna	k1gFnSc2	Anna
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Bachové	Bachová	k1gFnSc2	Bachová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c4	v
Köthenu	Köthena	k1gFnSc4	Köthena
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
Bach	Bach	k1gMnSc1	Bach
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1723	[number]	k4	1723
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
důvodech	důvod	k1gInPc6	důvod
lze	lze	k6eAd1	lze
jen	jen	k9	jen
spekulovat	spekulovat	k5eAaImF	spekulovat
<g/>
,	,	kIx,	,
snad	snad	k9	snad
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
hudebního	hudební	k2eAgInSc2d1	hudební
vkusu	vkus	k1gInSc2	vkus
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
postojů	postoj	k1gInPc2	postoj
knížete	kníže	k1gMnSc2	kníže
k	k	k7c3	k
Bachovi	Bach	k1gMnSc3	Bach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Lipsko	Lipsko	k1gNnSc1	Lipsko
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Smrtí	smrt	k1gFnSc7	smrt
předchozího	předchozí	k2eAgMnSc2d1	předchozí
varhaníka	varhaník	k1gMnSc2	varhaník
Johanna	Johann	k1gMnSc2	Johann
Kuhnaua	Kuhnau	k1gInSc2	Kuhnau
se	se	k3xPyFc4	se
5.	[number]	k4	5.
června	červen	k1gInSc2	červen
1722	[number]	k4	1722
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
místo	místo	k7c2	místo
kantora	kantor	k1gMnSc2	kantor
Tomášského	tomášský	k2eAgInSc2d1	tomášský
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Bach	Bach	k1gMnSc1	Bach
se	se	k3xPyFc4	se
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
ucházel	ucházet	k5eAaImAgMnS	ucházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jiný	jiný	k2eAgMnSc1d1	jiný
slavný	slavný	k2eAgMnSc1d1	slavný
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Philipp	Philipp	k1gMnSc1	Philipp
Telemann	Telemann	k1gMnSc1	Telemann
<g/>
.	.	kIx.	.
</s>
<s>
Telemann	Telemann	k1gMnSc1	Telemann
ovšem	ovšem	k9	ovšem
místo	místo	k1gNnSc4	místo
vzápětí	vzápětí	k6eAd1	vzápětí
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
-	-	kIx~	-
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
zvýšen	zvýšen	k2eAgInSc4d1	zvýšen
plat	plat	k1gInSc4	plat
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
vypsáno	vypsán	k2eAgNnSc1d1	vypsáno
druhé	druhý	k4xOgNnSc1	druhý
výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
byl	být	k5eAaImAgMnS	být
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
opět	opět	k6eAd1	opět
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
kapelníkem	kapelník	k1gMnSc7	kapelník
z	z	k7c2	z
Darmstadtu	Darmstadt	k1gInSc2	Darmstadt
Johannem	Johanno	k1gNnSc7	Johanno
Christophem	Christoph	k1gInSc7	Christoph
Graupnerem	Graupner	k1gMnSc7	Graupner
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
volba	volba	k1gFnSc1	volba
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Graupnerovi	Graupner	k1gMnSc3	Graupner
nebylo	být	k5eNaImAgNnS	být
uděleno	udělen	k2eAgNnSc1d1	uděleno
povolení	povolení	k1gNnSc1	povolení
opustit	opustit	k5eAaPmF	opustit
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
místo	místo	k6eAd1	místo
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
třetí	třetí	k4xOgFnSc3	třetí
volbě	volba	k1gFnSc3	volba
<g/>
"	"	kIx"	"
Bachovi	Bach	k1gMnSc3	Bach
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
zastával	zastávat	k5eAaImAgMnS	zastávat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
nadále	nadále	k6eAd1	nadále
podržel	podržet	k5eAaPmAgMnS	podržet
titul	titul	k1gInSc4	titul
knížecího	knížecí	k2eAgMnSc2d1	knížecí
dvorního	dvorní	k2eAgMnSc2d1	dvorní
kapelníka	kapelník	k1gMnSc2	kapelník
v	v	k7c6	v
Köthenu	Köthen	k1gInSc6	Köthen
a	a	k8xC	a
pro	pro	k7c4	pro
tamní	tamní	k2eAgInSc4d1	tamní
dvůr	dvůr	k1gInSc4	dvůr
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
dodával	dodávat	k5eAaImAgMnS	dodávat
skladby	skladba	k1gFnSc2	skladba
k	k	k7c3	k
slavnostním	slavnostní	k2eAgFnPc3d1	slavnostní
příležitostem	příležitost	k1gFnPc3	příležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
Bach	Bach	k1gMnSc1	Bach
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
1723	[number]	k4	1723
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc6	funkce
byl	být	k5eAaImAgMnS	být
Bach	Bach	k1gMnSc1	Bach
jako	jako	k8xC	jako
městský	městský	k2eAgMnSc1d1	městský
hudební	hudební	k2eAgMnSc1d1	hudební
ředitel	ředitel	k1gMnSc1	ředitel
zodpovědný	zodpovědný	k2eAgInSc4d1	zodpovědný
za	za	k7c4	za
hudební	hudební	k2eAgInSc4d1	hudební
život	život	k1gInSc4	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
čtyřech	čtyři	k4xCgInPc6	čtyři
kostelích	kostel	k1gInPc6	kostel
<g/>
,	,	kIx,	,
připravoval	připravovat	k5eAaImAgMnS	připravovat
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
,	,	kIx,	,
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
církevní	církevní	k2eAgFnPc4d1	církevní
a	a	k8xC	a
společenské	společenský	k2eAgFnPc4d1	společenská
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
,	,	kIx,	,
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
též	též	k9	též
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
souboru	soubor	k1gInSc2	soubor
Collegium	Collegium	k1gNnSc1	Collegium
musicum	musicum	k1gInSc1	musicum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
žáků	žák	k1gMnPc2	žák
a	a	k8xC	a
městských	městský	k2eAgMnPc2d1	městský
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
díla	dílo	k1gNnPc4	dílo
soudobých	soudobý	k2eAgMnPc2d1	soudobý
skladatelů	skladatel	k1gMnPc2	skladatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
významného	významný	k2eAgMnSc2d1	významný
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Jana	Jan	k1gMnSc2	Jan
Dismase	Dismas	k1gInSc6	Dismas
Zelenky	Zelenka	k1gMnSc2	Zelenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
Bach	Bach	k1gMnSc1	Bach
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
další	další	k2eAgFnPc4d1	další
závažné	závažný	k2eAgFnPc4d1	závažná
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Janovy	Janův	k2eAgFnSc2d1	Janova
pašije	pašije	k1gFnSc2	pašije
<g/>
,	,	kIx,	,
Matoušovy	Matoušův	k2eAgFnSc2d1	Matoušova
pašije	pašije	k1gFnSc2	pašije
<g/>
,	,	kIx,	,
Mši	mše	k1gFnSc3	mše
h	h	k?	h
moll	moll	k1gNnSc1	moll
nebo	nebo	k8xC	nebo
Umění	umění	k1gNnSc1	umění
fugy	fuga	k1gFnSc2	fuga
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
církevní	církevní	k2eAgInSc4d1	církevní
provoz	provoz	k1gInSc4	provoz
komponoval	komponovat	k5eAaImAgMnS	komponovat
pravidelně	pravidelně	k6eAd1	pravidelně
kantáty	kantáta	k1gFnSc2	kantáta
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
jednu	jeden	k4xCgFnSc4	jeden
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jich	on	k3xPp3gNnPc2	on
celkem	celkem	k6eAd1	celkem
napsal	napsat	k5eAaBmAgMnS	napsat
kolem	kolem	k7c2	kolem
tří	tři	k4xCgFnPc2	tři
set	sto	k4xCgNnPc2	sto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
aktivitu	aktivita	k1gFnSc4	aktivita
Bacha	Bacha	k?	Bacha
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
unavovat	unavovat	k5eAaImF	unavovat
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
veřejné	veřejný	k2eAgNnSc4d1	veřejné
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
uzavíral	uzavírat	k5eAaPmAgInS	uzavírat
více	hodně	k6eAd2	hodně
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
nejšťastnější	šťastný	k2eAgMnSc1d3	nejšťastnější
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1	poslední
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
Bachova	Bachův	k2eAgInSc2d1	Bachův
života	život	k1gInSc2	život
bylo	být	k5eAaImAgNnS	být
naplněno	naplněn	k2eAgNnSc1d1	naplněno
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
relativním	relativní	k2eAgInSc7d1	relativní
klidem	klid	k1gInSc7	klid
<g/>
.	.	kIx.	.
</s>
<s>
Bachův	Bachův	k2eAgMnSc1d1	Bachův
syn	syn	k1gMnSc1	syn
Carl	Carl	k1gMnSc1	Carl
Philipp	Philipp	k1gMnSc1	Philipp
Emanuel	Emanuel	k1gMnSc1	Emanuel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hudebníkem	hudebník	k1gMnSc7	hudebník
pruského	pruský	k2eAgMnSc2d1	pruský
krále	král	k1gMnSc2	král
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Velikého	veliký	k2eAgMnSc2d1	veliký
(	(	kIx(	(
<g/>
též	též	k9	též
aktivního	aktivní	k2eAgMnSc2d1	aktivní
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
otce	otec	k1gMnSc2	otec
Bacha	Bacha	k?	Bacha
pozval	pozvat	k5eAaPmAgMnS	pozvat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1747	[number]	k4	1747
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
ho	on	k3xPp3gInSc4	on
s	s	k7c7	s
poctami	pocta	k1gFnPc7	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
odvděčil	odvděčit	k5eAaPmAgMnS	odvděčit
improvizacemi	improvizace	k1gFnPc7	improvizace
na	na	k7c6	na
králem	král	k1gMnSc7	král
zadané	zadaný	k2eAgNnSc4d1	zadané
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Hudební	hudební	k2eAgFnSc6d1	hudební
obětině	obětina	k1gFnSc6	obětina
<g/>
,	,	kIx,	,
dílu	díl	k1gInSc6	díl
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
kontrapunktické	kontrapunktický	k2eAgFnSc2d1	kontrapunktická
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
Bach	Bach	k1gMnSc1	Bach
ztratil	ztratit	k5eAaPmAgMnS	ztratit
zrak	zrak	k1gInSc4	zrak
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
zhoršoval	zhoršovat	k5eAaImAgInS	zhoršovat
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
celkový	celkový	k2eAgInSc1d1	celkový
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zhoršení	zhoršení	k1gNnSc1	zhoršení
kulminovalo	kulminovat	k5eAaImAgNnS	kulminovat
nezdařenou	zdařený	k2eNgFnSc7d1	nezdařená
oční	oční	k2eAgFnSc7d1	oční
operací	operace	k1gFnSc7	operace
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Bach	Bach	k1gMnSc1	Bach
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
tichosti	tichost	k1gFnSc6	tichost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
obklopen	obklopen	k2eAgInSc1d1	obklopen
milující	milující	k2eAgFnSc7d1	milující
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
28.	[number]	k4	28.
července	červenec	k1gInSc2	červenec
1750	[number]	k4	1750
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
prokazovali	prokazovat	k5eAaImAgMnP	prokazovat
poslední	poslední	k2eAgFnSc4d1	poslední
úctu	úcta	k1gFnSc4	úcta
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
skupiny	skupina	k1gFnSc2	skupina
studentů	student	k1gMnPc2	student
Tomášské	tomášský	k2eAgFnSc2d1	Tomášská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
22.	[number]	k4	22.
října	říjen	k1gInSc2	říjen
1894	[number]	k4	1894
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
přestavby	přestavba	k1gFnSc2	přestavba
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
sousedícího	sousedící	k2eAgInSc2d1	sousedící
hřbitova	hřbitov	k1gInSc2	hřbitov
exhumována	exhumován	k2eAgFnSc1d1	exhumována
dubová	dubový	k2eAgFnSc1d1	dubová
rakev	rakev	k1gFnSc1	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Náhrobní	náhrobní	k2eAgInSc1d1	náhrobní
kámen	kámen	k1gInSc1	kámen
se	se	k3xPyFc4	se
nezachoval	zachovat	k5eNaPmAgInS	zachovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
ústního	ústní	k2eAgNnSc2d1	ústní
podání	podání	k1gNnSc2	podání
<g/>
,	,	kIx,	,
i	i	k9	i
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
12	[number]	k4	12
ze	z	k7c2	z
1400	[number]	k4	1400
zemřelých	zemřelý	k1gMnPc2	zemřelý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1750	[number]	k4	1750
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
v	v	k7c6	v
dubové	dubový	k2eAgFnSc6d1	dubová
rakvi	rakev	k1gFnSc6	rakev
a	a	k8xC	a
z	z	k7c2	z
dobrozdání	dobrozdání	k1gNnSc2	dobrozdání
lipského	lipský	k2eAgMnSc2d1	lipský
anatoma	anatom	k1gMnSc2	anatom
Wilhelma	Wilhelma	k1gFnSc1	Wilhelma
Hise	Hise	k1gFnSc1	Hise
byla	být	k5eAaImAgFnS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
identita	identita	k1gFnSc1	identita
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
jednoduchém	jednoduchý	k2eAgInSc6d1	jednoduchý
kamenném	kamenný	k2eAgInSc6d1	kamenný
sarkofágu	sarkofág	k1gInSc6	sarkofág
pohřbeny	pohřben	k2eAgFnPc1d1	pohřbena
pod	pod	k7c4	pod
oltář	oltář	k1gInSc4	oltář
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
hřbitova	hřbitov	k1gInSc2	hřbitov
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
200.	[number]	k4	200.
výročí	výročí	k1gNnSc2	výročí
Bachovy	Bachův	k2eAgFnSc2d1	Bachova
smrti	smrt	k1gFnSc2	smrt
sarkofág	sarkofág	k1gInSc1	sarkofág
přemístěn	přemístěn	k2eAgInSc1d1	přemístěn
do	do	k7c2	do
chóru	chór	k1gInSc2	chór
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Identita	identita	k1gFnSc1	identita
ostatků	ostatek	k1gInPc2	ostatek
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgFnPc7	některý
moderními	moderní	k2eAgFnPc7d1	moderní
muzikology	muzikolog	k1gMnPc4	muzikolog
zpochybňována	zpochybňován	k2eAgFnSc1d1	zpochybňována
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
kosti	kost	k1gFnPc1	kost
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Carla	Carl	k1gMnSc2	Carl
Philippa	Philipp	k1gMnSc2	Philipp
Emanuela	Emanuel	k1gMnSc2	Emanuel
<g/>
,	,	kIx,	,
test	test	k1gInSc1	test
DNA	dno	k1gNnSc2	dno
nebyl	být	k5eNaImAgInS	být
dosud	dosud	k6eAd1	dosud
proveden	provést	k5eAaPmNgInS	provést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
Bach	Bach	k1gInSc4	Bach
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
pracoval	pracovat	k5eAaImAgInS	pracovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
slavný	slavný	k2eAgInSc1d1	slavný
cyklus	cyklus	k1gInSc1	cyklus
Umění	umění	k1gNnSc1	umění
fugy	fuga	k1gFnSc2	fuga
(	(	kIx(	(
<g/>
nedokončen	dokončen	k2eNgInSc1d1	nedokončen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
Bachových	Bachův	k2eAgFnPc2d1	Bachova
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
čtyři	čtyři	k4xCgFnPc1	čtyři
staly	stát	k5eAaPmAgFnP	stát
skladateli	skladatel	k1gMnSc3	skladatel
<g/>
:	:	kIx,	:
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Friedemann	Friedemann	k1gMnSc1	Friedemann
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Philipp	Philipp	k1gMnSc1	Philipp
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Friedrich	Friedrich	k1gMnSc1	Friedrich
Christoph	Christoph	k1gMnSc1	Christoph
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Christian	Christian	k1gMnSc1	Christian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
hudební	hudební	k2eAgFnSc4d1	hudební
soutěž	soutěž	k1gFnSc4	soutěž
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
byl	být	k5eAaImAgMnS	být
mistrem	mistr	k1gMnSc7	mistr
polyfonie	polyfonie	k1gFnSc2	polyfonie
a	a	k8xC	a
přivedl	přivést	k5eAaPmAgInS	přivést
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
hudební	hudební	k2eAgFnSc4d1	hudební
formu	forma	k1gFnSc4	forma
fugy	fuga	k1gFnSc2	fuga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
bohaté	bohatý	k2eAgFnSc6d1	bohatá
tvorbě	tvorba	k1gFnSc6	tvorba
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
hluboké	hluboký	k2eAgFnSc2d1	hluboká
myšlenky	myšlenka	k1gFnSc2	myšlenka
umělecky	umělecky	k6eAd1	umělecky
přesvědčivým	přesvědčivý	k2eAgInSc7d1	přesvědčivý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
hudební	hudební	k2eAgNnSc4d1	hudební
myšlení	myšlení	k1gNnSc4	myšlení
je	být	k5eAaImIp3nS	být
pronikavé	pronikavý	k2eAgNnSc1d1	pronikavé
a	a	k8xC	a
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
však	však	k9	však
nepostrádá	postrádat	k5eNaImIp3nS	postrádat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
citovost	citovost	k1gFnSc4	citovost
<g/>
.	.	kIx.	.
</s>
<s>
Bach	Bach	k1gMnSc1	Bach
nebyl	být	k5eNaImAgMnS	být
extravagantním	extravagantní	k2eAgMnSc7d1	extravagantní
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spořádaným	spořádaný	k2eAgMnSc7d1	spořádaný
otcem	otec	k1gMnSc7	otec
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
praktickým	praktický	k2eAgNnSc7d1	praktické
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
hluboce	hluboko	k6eAd1	hluboko
věřícím	věřící	k2eAgMnSc7d1	věřící
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
zároveň	zároveň	k6eAd1	zároveň
smysl	smysl	k1gInSc4	smysl
i	i	k9	i
pro	pro	k7c4	pro
běžné	běžný	k2eAgFnPc4d1	běžná
pozemské	pozemský	k2eAgFnPc4d1	pozemská
radosti	radost	k1gFnPc4	radost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
odráží	odrážet	k5eAaImIp3nS	odrážet
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
povznáší	povznášet	k5eAaImIp3nS	povznášet
do	do	k7c2	do
dimenzí	dimenze	k1gFnPc2	dimenze
téměř	téměř	k6eAd1	téměř
nadpozemských	nadpozemský	k2eAgFnPc2d1	nadpozemská
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
stojí	stát	k5eAaImIp3nP	stát
pevnýma	pevný	k2eAgFnPc7d1	pevná
nohama	noha	k1gFnPc7	noha
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pokoru	pokora	k1gFnSc4	pokora
a	a	k8xC	a
soucit	soucit	k1gInSc4	soucit
s	s	k7c7	s
lidským	lidský	k2eAgNnSc7d1	lidské
utrpením	utrpení	k1gNnSc7	utrpení
i	i	k9	i
vřelou	vřelý	k2eAgFnSc4d1	vřelá
oslavu	oslava	k1gFnSc4	oslava
světského	světský	k2eAgInSc2d1	světský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
byla	být	k5eAaImAgFnS	být
psána	psát	k5eAaImNgFnS	psát
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
<g/>
,	,	kIx,	,
Bach	Bach	k1gInSc4	Bach
je	být	k5eAaImIp3nS	být
dokázal	dokázat	k5eAaPmAgInS	dokázat
naplňovat	naplňovat	k5eAaImF	naplňovat
hlubokým	hluboký	k2eAgInSc7d1	hluboký
obsahem	obsah	k1gInSc7	obsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
Bachova	Bachův	k2eAgNnSc2d1	Bachovo
díla	dílo	k1gNnSc2	dílo
víme	vědět	k5eAaImIp1nP	vědět
o	o	k7c4	o
celkem	celkem	k6eAd1	celkem
1126	[number]	k4	1126
hudebních	hudební	k2eAgFnPc6d1	hudební
kompozicích	kompozice	k1gFnPc6	kompozice
(	(	kIx(	(
<g/>
označují	označovat	k5eAaImIp3nP	označovat
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
BWV	BWV	kA	BWV
a	a	k8xC	a
pořadovým	pořadový	k2eAgNnSc7d1	pořadové
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
skladeb	skladba	k1gFnPc2	skladba
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
a	a	k8xC	a
do	do	k7c2	do
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
nejsou	být	k5eNaImIp3nP	být
započítány	započítat	k5eAaPmNgFnP	započítat
ani	ani	k8xC	ani
autorem	autor	k1gMnSc7	autor
revidované	revidovaný	k2eAgFnSc2d1	revidovaná
verze	verze	k1gFnSc2	verze
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
hudební	hudební	k2eAgInPc4d1	hudební
kusy	kus	k1gInPc4	kus
sloužící	sloužící	k2eAgInPc4d1	sloužící
k	k	k7c3	k
výuce	výuka	k1gFnSc3	výuka
-	-	kIx~	-
celkově	celkově	k6eAd1	celkově
tak	tak	k9	tak
Bach	Bach	k1gMnSc1	Bach
složil	složit	k5eAaPmAgMnS	složit
nejméně	málo	k6eAd3	málo
kolem	kolem	k7c2	kolem
1400	[number]	k4	1400
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bachově	Bachův	k2eAgNnSc6d1	Bachovo
díle	dílo	k1gNnSc6	dílo
jsou	být	k5eAaImIp3nP	být
čitelné	čitelný	k2eAgInPc1d1	čitelný
vlivy	vliv	k1gInPc1	vliv
italské	italský	k2eAgFnSc2d1	italská
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
forma	forma	k1gFnSc1	forma
kantáty	kantáta	k1gFnSc2	kantáta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přímým	přímý	k2eAgInSc7d1	přímý
vzorem	vzor	k1gInSc7	vzor
při	při	k7c6	při
komponování	komponování	k1gNnSc6	komponování
instrumentálních	instrumentální	k2eAgInPc2d1	instrumentální
koncertů	koncert	k1gInPc2	koncert
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
skladatel	skladatel	k1gMnSc1	skladatel
Vivaldi	Vivald	k1gMnPc1	Vivald
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
a	a	k8xC	a
propracoval	propracovat	k5eAaPmAgMnS	propracovat
formu	forma	k1gFnSc4	forma
taneční	taneční	k2eAgFnSc2d1	taneční
suity	suita	k1gFnSc2	suita
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gFnPc6	jejichž
pomalých	pomalý	k2eAgFnPc6d1	pomalá
větách	věta	k1gFnPc6	věta
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
vlivy	vliv	k1gInPc1	vliv
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
si	se	k3xPyFc3	se
vážil	vážit	k5eAaImAgMnS	vážit
umění	umění	k1gNnSc4	umění
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
žákem	žák	k1gMnSc7	žák
Buxtehudeho	Buxtehude	k1gMnSc2	Buxtehude
a	a	k8xC	a
nelitoval	litovat	k5eNaImAgInS	litovat
ujít	ujít	k5eAaPmF	ujít
pěšky	pěšky	k6eAd1	pěšky
(	(	kIx(	(
<g/>
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1705	[number]	k4	1705
<g/>
)	)	kIx)	)
320	[number]	k4	320
kilometrů	kilometr	k1gInPc2	kilometr
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vyposlechl	vyposlechnout	k5eAaPmAgMnS	vyposlechnout
<g/>
.	.	kIx.	.
</s>
<s>
Bachovo	Bachův	k2eAgNnSc1d1	Bachovo
nepopiratelně	popiratelně	k6eNd1	popiratelně
originální	originální	k2eAgNnSc1d1	originální
a	a	k8xC	a
hluboké	hluboký	k2eAgNnSc1d1	hluboké
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
vlastní	vlastní	k2eAgFnSc7d1	vlastní
svébytnou	svébytný	k2eAgFnSc7d1	svébytná
a	a	k8xC	a
vědomou	vědomý	k2eAgFnSc7d1	vědomá
syntézou	syntéza	k1gFnSc7	syntéza
a	a	k8xC	a
rekapitulací	rekapitulace	k1gFnSc7	rekapitulace
předchozího	předchozí	k2eAgInSc2d1	předchozí
vývoje	vývoj	k1gInSc2	vývoj
německé	německý	k2eAgFnSc2d1	německá
<g/>
,	,	kIx,	,
italské	italský	k2eAgFnSc2d1	italská
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Klávesové	klávesový	k2eAgFnPc1d1	klávesová
skladby	skladba	k1gFnPc1	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
ve	v	k7c6	v
skladatelově	skladatelův	k2eAgInSc6d1	skladatelův
odkazu	odkaz	k1gInSc6	odkaz
mají	mít	k5eAaImIp3nP	mít
skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
</s>
</p>
<p>
</p>
<p>
<s>
Fugy	fuga	k1gFnPc1	fuga
</s>
</p>
<p>
<s>
Fuga	fuga	k1gFnSc1	fuga
h	h	k?	h
moll	moll	k1gNnSc1	moll
(	(	kIx(	(
<g/>
Fuge	Fuge	k1gFnSc1	Fuge
h-moll	holl	k1gMnSc1	h-moll
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
951	[number]	k4	951
a	a	k8xC	a
951a	951a	k6eAd1	951a
</s>
</p>
<p>
<s>
Fantazie	fantazie	k1gFnSc1	fantazie
</s>
</p>
<p>
<s>
Fantazie	fantazie	k1gFnSc1	fantazie
c	c	k0	c
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
Fantasie	fantasie	k1gFnSc1	fantasie
c-moll	coll	k1gMnSc1	c-moll
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1121	[number]	k4	1121
</s>
</p>
<p>
<s>
Chromatická	chromatický	k2eAgFnSc1d1	chromatická
fantazie	fantazie	k1gFnSc1	fantazie
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
(	(	kIx(	(
<g/>
Chromatische	Chromatische	k1gInSc1	Chromatische
Fantasie	fantasie	k1gFnSc2	fantasie
und	und	k?	und
Fuge	Fug	k1gFnSc2	Fug
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
903	[number]	k4	903
</s>
</p>
<p>
<s>
Preludia	preludium	k1gNnPc1	preludium
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
c	c	k0	c
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
Präludium	Präludium	k1gNnSc1	Präludium
c-moll	colla	k1gFnPc2	c-molla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
999	[number]	k4	999
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
(	(	kIx(	(
<g/>
Präludium	Präludium	k1gNnSc1	Präludium
und	und	k?	und
Fuge	Fug	k1gInSc2	Fug
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
552	[number]	k4	552
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
<g/>
,	,	kIx,	,
fuga	fuga	k1gFnSc1	fuga
a	a	k8xC	a
Allegro	allegro	k1gNnSc1	allegro
(	(	kIx(	(
<g/>
Präludium	Präludium	k1gNnSc1	Präludium
<g/>
,	,	kIx,	,
Fuge	Fuge	k1gNnSc1	Fuge
und	und	k?	und
Allegro	allegro	k1gNnSc1	allegro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
998	[number]	k4	998
</s>
</p>
<p>
<s>
Tokáty	tokáta	k1gFnPc1	tokáta
</s>
</p>
<p>
<s>
Tokáta	tokáta	k1gFnSc1	tokáta
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
d	d	k?	d
moll	moll	k1gNnSc1	moll
(	(	kIx(	(
<g/>
Toccata	toccata	k1gFnSc1	toccata
und	und	k?	und
fuge	fugat	k5eAaPmIp3nS	fugat
d-moll	doll	k1gInSc1	d-moll
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
565	[number]	k4	565
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc1d3	nejznámější
a	a	k8xC	a
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
varhanní	varhanní	k2eAgFnSc1d1	varhanní
skladba	skladba	k1gFnSc1	skladba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tokáta	tokáta	k1gFnSc1	tokáta
g	g	kA	g
moll	moll	k1gNnSc2	moll
(	(	kIx(	(
<g/>
Toccata	toccata	k1gFnSc1	toccata
g-moll	goll	k1gMnSc1	g-moll
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
915	[number]	k4	915
</s>
</p>
<p>
<s>
Chorální	chorální	k2eAgFnPc1d1	chorální
skladby	skladba	k1gFnPc1	skladba
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
kolem	kolem	k6eAd1	kolem
260	[number]	k4	260
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Při	při	k7c6	při
řekách	řeka	k1gFnPc6	řeka
babylónských	babylónský	k2eAgMnPc2d1	babylónský
(	(	kIx(	(
<g/>
An	An	k1gMnSc2	An
Wasserflüssen	Wasserflüssen	k2eAgInSc1d1	Wasserflüssen
Babylon	Babylon	k1gInSc1	Babylon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
653	[number]	k4	653
</s>
</p>
<p>
<s>
Otče	otec	k1gMnSc5	otec
náš	náš	k3xOp1gInSc4	náš
v	v	k7c6	v
nebeské	nebeský	k2eAgFnSc6d1	nebeská
říši	říš	k1gFnSc6	říš
(	(	kIx(	(
<g/>
Vater	vatra	k1gFnPc2	vatra
unser	unser	k1gInSc1	unser
im	im	k?	im
Himmelreich	Himmelreich	k1gInSc1	Himmelreich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
682	[number]	k4	682
a	a	k8xC	a
683	[number]	k4	683
</s>
</p>
<p>
<s>
Kristus	Kristus	k1gMnSc1	Kristus
vstal	vstát	k5eAaPmAgMnS	vstát
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
Christ	Christ	k1gMnSc1	Christ
ist	ist	k?	ist
erstanden	erstandno	k1gNnPc2	erstandno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
627	[number]	k4	627
</s>
</p>
<p>
<s>
Chorální	chorální	k2eAgFnPc1d1	chorální
předehry	předehra	k1gFnPc1	předehra
</s>
</p>
<p>
<s>
Chorální	chorální	k2eAgFnSc1d1	chorální
předehra	předehra	k1gFnSc1	předehra
G	G	kA	G
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
Choral	Choral	k1gMnSc1	Choral
Vorspiel	Vorspiel	k1gMnSc1	Vorspiel
G-dur	Gur	k1gMnSc1	G-dur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
95	[number]	k4	95
</s>
</p>
<p>
<s>
Pasacaglie	Pasacaglie	k1gFnSc1	Pasacaglie
</s>
</p>
<p>
<s>
Passacaglia	Passacaglia	k1gFnSc1	Passacaglia
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
v	v	k7c6	v
c	c	k0	c
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
582	[number]	k4	582
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jiná	jiný	k2eAgFnSc1d1	jiná
duchovní	duchovní	k2eAgFnSc1d1	duchovní
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
částí	část	k1gFnSc7	část
skladatelovy	skladatelův	k2eAgFnSc2d1	skladatelova
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
další	další	k2eAgFnPc1d1	další
duchovní	duchovní	k2eAgFnPc1d1	duchovní
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
</p>
<p>
<s>
Mše	mše	k1gFnSc1	mše
</s>
</p>
<p>
<s>
Mše	mše	k1gFnSc1	mše
G	G	kA	G
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
Messe	Messe	k1gFnSc1	Messe
G-dur	Gura	k1gFnPc2	G-dura
/	/	kIx~	/
Kyrie-Gloria	Kyrie-Glorium	k1gNnSc2	Kyrie-Glorium
Messe	Messe	k1gFnSc2	Messe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
24	[number]	k4	24
</s>
</p>
<p>
<s>
Mše	mše	k1gFnSc1	mše
h	h	k?	h
moll	moll	k1gNnSc1	moll
(	(	kIx(	(
<g/>
Messe	Messe	k1gFnSc1	Messe
h-moll	holl	k1gMnSc1	h-moll
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
232	[number]	k4	232
</s>
</p>
<p>
<s>
Mše	mše	k1gFnSc1	mše
F	F	kA	F
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
Messe	Messe	k1gFnSc1	Messe
F-dur	Fur	k1gMnSc1	F-dur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
233	[number]	k4	233
</s>
</p>
<p>
<s>
Oratoria	oratorium	k1gNnPc1	oratorium
<g/>
,	,	kIx,	,
moteta	moteto	k1gNnPc1	moteto
a	a	k8xC	a
kantáty	kantáta	k1gFnPc1	kantáta
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
napsal	napsat	k5eAaBmAgInS	napsat
přes	přes	k7c4	přes
200	[number]	k4	200
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
světských	světský	k2eAgFnPc2d1	světská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oratorium	oratorium	k1gNnSc1	oratorium
Nanebevstoupení	nanebevstoupení	k1gNnSc2	nanebevstoupení
"	"	kIx"	"
<g/>
Lobet	Lobet	k1gInSc1	Lobet
Gott	Gott	k2eAgInSc1d1	Gott
in	in	k?	in
seinen	seinen	k2eAgInSc4d1	seinen
Reichen	Reichen	k1gInSc4	Reichen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Himmelfahrts-Oratorium	Himmelfahrts-Oratorium	k1gNnSc1	Himmelfahrts-Oratorium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
11	[number]	k4	11
</s>
</p>
<p>
<s>
Matoušovy	Matoušův	k2eAgFnPc1d1	Matoušova
pašije	pašije	k1gFnPc1	pašije
(	(	kIx(	(
<g/>
Matthäus-Passion	Matthäus-Passion	k1gInSc1	Matthäus-Passion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
244	[number]	k4	244
</s>
</p>
<p>
<s>
Janovy	Janův	k2eAgFnPc1d1	Janova
pašije	pašije	k1gFnPc1	pašije
(	(	kIx(	(
<g/>
Johannes-Passion	Johannes-Passion	k1gInSc1	Johannes-Passion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
245	[number]	k4	245
</s>
</p>
<p>
<s>
Markovy	Markův	k2eAgFnPc1d1	Markova
pašije	pašije	k1gFnPc1	pašije
(	(	kIx(	(
<g/>
Markus-Passion	Markus-Passion	k1gInSc1	Markus-Passion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
247	[number]	k4	247
</s>
</p>
<p>
<s>
Vánoční	vánoční	k2eAgNnSc1d1	vánoční
oratorium	oratorium	k1gNnSc1	oratorium
(	(	kIx(	(
<g/>
Weihnachts-Oratorium	Weihnachts-Oratorium	k1gNnSc1	Weihnachts-Oratorium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
248	[number]	k4	248
</s>
</p>
<p>
<s>
Velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
oratorium	oratorium	k1gNnSc1	oratorium
(	(	kIx(	(
<g/>
Oster-Oratorium	Oster-Oratorium	k1gNnSc1	Oster-Oratorium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
249	[number]	k4	249
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Magnificat	Magnificat	k1gNnSc1	Magnificat
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
243	[number]	k4	243
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Koncertní	koncertní	k2eAgFnPc4d1	koncertní
a	a	k8xC	a
orchestrální	orchestrální	k2eAgFnPc4d1	orchestrální
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
hudebními	hudební	k2eAgInPc7d1	hudební
útvary	útvar	k1gInPc7	útvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
své	svůj	k3xOyFgNnSc4	svůj
důležité	důležitý	k2eAgNnSc4d1	důležité
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Bachově	Bachův	k2eAgFnSc6d1	Bachova
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
koncertní	koncertní	k2eAgFnPc1d1	koncertní
a	a	k8xC	a
koncertantní	koncertantní	k2eAgFnPc1d1	koncertantní
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Braniborské	braniborský	k2eAgInPc1d1	braniborský
koncerty	koncert	k1gInPc1	koncert
č	č	k0	č
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
Brandenburgische	Brandenburgischus	k1gMnSc5	Brandenburgischus
Konzerte	Konzert	k1gMnSc5	Konzert
Nr	Nr	k1gMnSc5	Nr
<g/>
.	.	kIx.	.
1-6	[number]	k4	1-6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1046-1051	[number]	k4	1046-1051
</s>
</p>
<p>
<s>
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
a	a	k8xC	a
moll	moll	k1gNnSc4	moll
(	(	kIx(	(
<g/>
Cembalokonzert	Cembalokonzert	k1gMnSc1	Cembalokonzert
a-moll	aoll	k1gMnSc1	a-moll
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1065	[number]	k4	1065
</s>
</p>
<p>
<s>
Ouvertury	ouvertura	k1gFnPc1	ouvertura
(	(	kIx(	(
<g/>
Suity	suita	k1gFnPc1	suita
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
<g/>
)	)	kIx)	)
<g/>
Varhanní	varhanní	k2eAgInPc4d1	varhanní
koncerty	koncert	k1gInPc4	koncert
</s>
</p>
<p>
<s>
Varhanní	varhanní	k2eAgInSc4d1	varhanní
koncert	koncert	k1gInSc4	koncert
G	G	kA	G
dur	dur	k1gNnSc1	dur
(	(	kIx(	(
<g/>
Orgelkonzerte	Orgelkonzert	k1gMnSc5	Orgelkonzert
G-dur	Gura	k1gFnPc2	G-dura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
592	[number]	k4	592
</s>
</p>
<p>
<s>
Varhanní	varhanní	k2eAgInSc1d1	varhanní
koncert	koncert	k1gInSc1	koncert
C	C	kA	C
dur	dur	k1gNnSc2	dur
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Antonia	Antonio	k1gMnSc2	Antonio
Vivaldiho	Vivaldi	k1gMnSc2	Vivaldi
"	"	kIx"	"
<g/>
Velký	velký	k2eAgMnSc1d1	velký
Mogul	mogul	k1gMnSc1	mogul
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
594Cembalové	594Cembal	k1gMnPc1	594Cembal
(	(	kIx(	(
<g/>
klavírní	klavírní	k2eAgMnSc1d1	klavírní
<g/>
)	)	kIx)	)
koncerty	koncert	k1gInPc1	koncert
</s>
</p>
<p>
<s>
Italský	italský	k2eAgInSc1d1	italský
koncert	koncert	k1gInSc1	koncert
(	(	kIx(	(
<g/>
Italienisches	Italienisches	k1gMnSc1	Italienisches
Konzert	Konzert	k1gMnSc1	Konzert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
971	[number]	k4	971
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Suity	suita	k1gFnPc4	suita
a	a	k8xC	a
sonáty	sonáta	k1gFnPc4	sonáta
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
suity	suita	k1gFnPc1	suita
1-6	[number]	k4	1-6
(	(	kIx(	(
<g/>
Englische	Englischus	k1gMnSc5	Englischus
Suite	Suit	k1gMnSc5	Suit
Nr	Nr	k1gMnSc5	Nr
<g/>
.	.	kIx.	.
1-6	[number]	k4	1-6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
806-811	[number]	k4	806-811
</s>
</p>
<p>
<s>
Francouzské	francouzský	k2eAgFnPc1d1	francouzská
suity	suita	k1gFnPc1	suita
(	(	kIx(	(
<g/>
Französische	Französischus	k1gMnSc5	Französischus
Suite	Suit	k1gMnSc5	Suit
Nr	Nr	k1gMnSc5	Nr
<g/>
.	.	kIx.	.
1-6	[number]	k4	1-6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
812-817	[number]	k4	812-817
</s>
</p>
<p>
<s>
Sinfonia	sinfonia	k1gFnSc1	sinfonia
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1046	[number]	k4	1046
a	a	k8xC	a
1071	[number]	k4	1071
</s>
</p>
<p>
<s>
Suity	suita	k1gFnPc1	suita
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
(	(	kIx(	(
<g/>
Cellosuiten	Cellosuiten	k2eAgMnSc1d1	Cellosuiten
Nr	Nr	k1gMnSc1	Nr
<g/>
.	.	kIx.	.
1-6	[number]	k4	1-6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1007-1012	[number]	k4	1007-1012
</s>
</p>
<p>
<s>
Triové	triový	k2eAgFnPc1d1	triová
sonáty	sonáta	k1gFnPc1	sonáta
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
flétny	flétna	k1gFnPc4	flétna
a	a	k8xC	a
continuo	continuo	k1gNnSc1	continuo
(	(	kIx(	(
<g/>
Sonate	Sonat	k1gMnSc5	Sonat
für	für	k?	für
Zwei	Zwei	k1gNnSc3	Zwei
Flauti	Flauť	k1gFnSc2	Flauť
traversi	traverse	k1gFnSc3	traverse
und	und	k?	und
Basso	Bassa	k1gFnSc5	Bassa
continuo	continua	k1gMnSc5	continua
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1039	[number]	k4	1039
</s>
</p>
<p>
<s>
Varhanní	varhanní	k2eAgFnPc1d1	varhanní
sonáty	sonáta	k1gFnPc1	sonáta
č	č	k0	č
<g/>
.	.	kIx.	.
<g/>
1-6	[number]	k4	1-6
(	(	kIx(	(
<g/>
Orgelsonaten	Orgelsonaten	k2eAgMnSc1d1	Orgelsonaten
Nr	Nr	k1gMnSc1	Nr
<g/>
.	.	kIx.	.
1-6	[number]	k4	1-6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
525-530	[number]	k4	525-530
</s>
</p>
<p>
<s>
Sonáty	sonáta	k1gFnPc1	sonáta
a	a	k8xC	a
partity	partita	k1gFnPc1	partita
pro	pro	k7c4	pro
sólové	sólový	k2eAgFnPc4d1	sólová
housle	housle	k1gFnPc4	housle
(	(	kIx(	(
<g/>
Sonaten	Sonaten	k2eAgInSc1d1	Sonaten
und	und	k?	und
Partiten	Partiten	k2eAgInSc1d1	Partiten
für	für	k?	für
Violin	violina	k1gFnPc2	violina
Solo	Solo	k1gMnSc1	Solo
<g/>
)	)	kIx)	)
BWV	BWV	kA	BWV
1001-1006	[number]	k4	1001-1006
</s>
</p>
<p>
<s>
Sonáty	sonáta	k1gFnPc1	sonáta
pro	pro	k7c4	pro
flétnu	flétna	k1gFnSc4	flétna
</s>
</p>
<p>
<s>
Sonáty	sonáta	k1gFnPc1	sonáta
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
da	da	k?	da
gamba	gamba	k1gFnSc1	gamba
a	a	k8xC	a
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1027-1029	[number]	k4	1027-1029
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Skladby	skladba	k1gFnSc2	skladba
pro	pro	k7c4	pro
sólové	sólový	k2eAgInPc4d1	sólový
nástroje	nástroj	k1gInPc4	nástroj
===	===	k?	===
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
sólové	sólový	k2eAgInPc4d1	sólový
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
varhan	varhany	k1gFnPc2	varhany
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgFnPc1d3	nejznámější
klavírní	klavírní	k2eAgFnPc1d1	klavírní
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznačnějšími	význačný	k2eAgFnPc7d3	nejvýznačnější
jsou	být	k5eAaImIp3nP	být
komplety	komplet	k1gInPc4	komplet
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
(	(	kIx(	(
<g/>
Wohltemperiertes	Wohltemperiertes	k1gMnSc1	Wohltemperiertes
Klavier	Klavier	k1gMnSc1	Klavier
Nr	Nr	k1gMnSc1	Nr
<g/>
.	.	kIx.	.
1	[number]	k4	1
und	und	k?	und
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
lze	lze	k6eAd1	lze
také	také	k9	také
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
varhanní	varhanní	k2eAgFnPc4d1	varhanní
skladby	skladba	k1gFnPc4	skladba
nebo	nebo	k8xC	nebo
cellové	cellový	k2eAgFnPc4d1	cellový
a	a	k8xC	a
houslové	houslový	k2eAgFnPc4d1	houslová
suity	suita	k1gFnPc4	suita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Polyfonní	polyfonní	k2eAgInPc1d1	polyfonní
cykly	cyklus	k1gInPc1	cyklus
===	===	k?	===
</s>
</p>
<p>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
kontrapunktického	kontrapunktický	k2eAgNnSc2d1	kontrapunktické
umění	umění	k1gNnSc2	umění
jsou	být	k5eAaImIp3nP	být
polyfonní	polyfonní	k2eAgInPc1d1	polyfonní
cykly	cyklus	k1gInPc1	cyklus
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
nástrojové	nástrojový	k2eAgFnSc2d1	nástrojová
specifikace	specifikace	k1gFnSc2	specifikace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Umění	umění	k1gNnSc1	umění
fugy	fuga	k1gFnSc2	fuga
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Kunst	Kunst	k1gMnSc1	Kunst
der	drát	k5eAaImRp2nS	drát
Fuge	Fugus	k1gMnSc5	Fugus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1080	[number]	k4	1080
<g/>
;	;	kIx,	;
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
děl	dělo	k1gNnPc2	dělo
hudební	hudební	k2eAgFnSc2d1	hudební
historie	historie	k1gFnSc2	historie
jak	jak	k8xS	jak
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
obsahové	obsahový	k2eAgFnSc6d1	obsahová
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
formální	formální	k2eAgInPc1d1	formální
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
obětina	obětina	k1gFnSc1	obětina
(	(	kIx(	(
<g/>
Musikalisches	Musikalisches	k1gMnSc1	Musikalisches
Opfer	Opfer	k1gMnSc1	Opfer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1079	[number]	k4	1079
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnSc1d1	ostatní
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
zanedbatelném	zanedbatelný	k2eAgInSc6d1	zanedbatelný
počtu	počet	k1gInSc6	počet
najdeme	najít	k5eAaPmIp1nP	najít
ve	v	k7c6	v
skladatelově	skladatelův	k2eAgFnSc6d1	skladatelova
tvorbě	tvorba	k1gFnSc6	tvorba
také	také	k9	také
</s>
</p>
<p>
</p>
<p>
<s>
Kánony	kánon	k1gInPc1	kánon
</s>
</p>
<p>
<s>
Variace	variace	k1gFnPc1	variace
</s>
</p>
<p>
<s>
Goldbergovy	Goldbergův	k2eAgFnPc1d1	Goldbergova
variace	variace	k1gFnPc1	variace
(	(	kIx(	(
<g/>
Goldberg-Variationen	Goldberg-Variationen	k1gInSc1	Goldberg-Variationen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
988	[number]	k4	988
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
</s>
</p>
<p>
<s>
Pochody	pochod	k1gInPc1	pochod
</s>
</p>
<p>
<s>
Scherza	scherzo	k1gNnPc1	scherzo
</s>
</p>
<p>
<s>
Capriccia	capriccio	k1gNnPc1	capriccio
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ukázky	ukázka	k1gFnPc1	ukázka
hudby	hudba	k1gFnSc2	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č	č	k0	č
<g/>
.	.	kIx.	.
1	[number]	k4	1
d	d	k?	d
moll	moll	k1gNnSc1	moll
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
1052	[number]	k4	1052
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Allegro	allegro	k1gNnSc1	allegro
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Adagio	adagio	k1gNnSc1	adagio
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Allegro	allegro	k6eAd1	allegro
</s>
</p>
<p>
<s>
Kantáta	kantáta	k1gFnSc1	kantáta
BWV	BWV	kA	BWV
140	[number]	k4	140
"	"	kIx"	"
<g/>
Wachet	Wachet	k1gInSc1	Wachet
auf	auf	k?	auf
<g/>
"	"	kIx"	"
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Recitativ	recitativ	k1gInSc1	recitativ
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Duet	duet	k1gInSc1	duet
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Chorál	chorál	k1gInSc1	chorál
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Recitativ	recitativ	k1gInSc1	recitativ
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Duet	duet	k1gInSc1	duet
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Chorál	chorál	k1gInSc1	chorál
</s>
</p>
<p>
<s>
Partita	partita	k1gFnSc1	partita
a	a	k8xC	a
moll	moll	k1gNnSc1	moll
pro	pro	k7c4	pro
sólovou	sólový	k2eAgFnSc4d1	sólová
flétnu	flétna	k1gFnSc4	flétna
BWV	BWV	kA	BWV
1013	[number]	k4	1013
Partita	partita	k1gFnSc1	partita
</s>
</p>
<p>
<s>
Loutnová	loutnový	k2eAgFnSc1d1	loutnová
suita	suita	k1gFnSc1	suita
e	e	k0	e
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
996	[number]	k4	996
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Preludium	preludium	k1gNnSc1	preludium
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Allemande	Allemand	k1gInSc5	Allemand
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Courante	Courant	k1gMnSc5	Courant
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sarabanda	sarabanda	k1gFnSc1	sarabanda
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Bourrée	bourrée	k1gNnSc1	bourrée
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Giga	giga	k1gFnSc1	giga
</s>
</p>
<p>
<s>
Partita	partita	k1gFnSc1	partita
d	d	k?	d
moll	moll	k1gNnSc7	moll
pro	pro	k7c4	pro
sólové	sólový	k2eAgFnPc4d1	sólová
housle	housle	k1gFnPc4	housle
BWV	BWV	kA	BWV
1004	[number]	k4	1004
Chaconna	Chaconn	k1gInSc2	Chaconn
</s>
</p>
<p>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
suita	suita	k1gFnSc1	suita
č	č	k0	č
<g/>
.	.	kIx.	.
1	[number]	k4	1
A	a	k8xC	a
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
806	[number]	k4	806
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Preludium	preludium	k1gNnSc1	preludium
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Allemande	Allemand	k1gInSc5	Allemand
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Courante	Courant	k1gMnSc5	Courant
I.	I.	kA	I.
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Courante	Courant	k1gMnSc5	Courant
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Double	double	k1gInSc1	double
I.	I.	kA	I.
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Double	double	k1gInSc1	double
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Sarabanda	sarabanda	k1gFnSc1	sarabanda
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Bourrée	bourrée	k1gNnSc1	bourrée
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Giga	giga	k1gFnSc1	giga
</s>
</p>
<p>
<s>
Italský	italský	k2eAgInSc4d1	italský
koncert	koncert	k1gInSc4	koncert
F	F	kA	F
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
971	[number]	k4	971
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Allegro	allegro	k1gNnSc1	allegro
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Andante	andante	k1gNnSc1	andante
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Presto	presto	k6eAd1	presto
</s>
</p>
<p>
<s>
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
1.	[number]	k4	1.
díl	díl	k1gInSc1	díl
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
č	č	k0	č
<g/>
.	.	kIx.	.
20	[number]	k4	20
a	a	k8xC	a
moll	moll	k1gNnSc1	moll
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
865	[number]	k4	865
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
BWV	BWV	kA	BWV
–	–	k?	–
katalog	katalog	k1gInSc1	katalog
děl	dít	k5eAaBmAgMnS	dít
Johanna	Johann	k1gMnSc4	Johann
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Bacha	Bacha	k?	Bacha
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bach	Bach	k1gMnSc1	Bach
Bibliography	Bibliographa	k1gFnSc2	Bibliographa
<g/>
"	"	kIx"	"
–	–	k?	–
Bachovská	bachovský	k2eAgFnSc1d1	bachovská
bibliografie	bibliografie	k1gFnSc1	bibliografie
</s>
</p>
<p>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Zavarský	Zavarský	k2eAgMnSc1d1	Zavarský
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Editio	Editio	k6eAd1	Editio
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Holzknecht	Holzknecht	k1gMnSc1	Holzknecht
<g/>
:	:	kIx,	:
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Panton	Panton	k1gInSc1	Panton
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1968Zahraniční	1968Zahraniční	k2eAgFnSc2d1	1968Zahraniční
</s>
</p>
<p>
</p>
<p>
<s>
Christoph	Christoph	k1gMnSc1	Christoph
Wolff	Wolff	k1gMnSc1	Wolff
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Heinrich	Heinrich	k1gMnSc1	Heinrich
Eggebrecht	Eggebrecht	k1gMnSc1	Eggebrecht
<g/>
:	:	kIx,	:
Geheimnis	Geheimnis	k1gFnSc1	Geheimnis
Bach	Bacha	k1gFnPc2	Bacha
<g/>
.	.	kIx.	.
</s>
<s>
Nötzel	Nötzet	k5eAaPmAgMnS	Nötzet
<g/>
,	,	kIx,	,
Wilhelmshaven	Wilhelmshaven	k2eAgMnSc1d1	Wilhelmshaven
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-7959-0790-X	[number]	k4	3-7959-0790-X
</s>
</p>
<p>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
Eidam	eidam	k1gInSc1	eidam
<g/>
:	:	kIx,	:
Das	Das	k1gMnSc5	Das
wahre	wahr	k1gMnSc5	wahr
Leben	Lebna	k1gFnPc2	Lebna
des	des	k1gNnSc1	des
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Piper	Piper	k1gMnSc1	Piper
Verlag	Verlag	k1gMnSc1	Verlag
GmbH	GmbH	k1gMnSc1	GmbH
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-492-24435-1	[number]	k4	3-492-24435-1
</s>
</p>
<p>
<s>
Arno	Arno	k1gMnSc1	Arno
Forchert	Forchert	k1gMnSc1	Forchert
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
und	und	k?	und
seine	seinout	k5eAaPmIp3nS	seinout
Zeit	Zeit	k1gInSc1	Zeit
<g/>
.	.	kIx.	.
</s>
<s>
Laaber	Laaber	k1gMnSc1	Laaber
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
Neuaufl	Neuaufl	k1gMnSc1	Neuaufl
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-89007-531-2	[number]	k4	3-89007-531-2
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
Forkel	Forkel	k1gMnSc1	Forkel
<g/>
:	:	kIx,	:
Über	Über	k1gMnSc1	Über
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bachs	Bachsa	k1gFnPc2	Bachsa
Leben	Leben	k1gInSc1	Leben
<g/>
,	,	kIx,	,
Kunst	Kunst	k1gInSc1	Kunst
und	und	k?	und
Kunstwerke	Kunstwerke	k1gInSc1	Kunstwerke
<g/>
.	.	kIx.	.
</s>
<s>
Bärenreiter-Verlag	Bärenreiter-Verlag	k1gInSc1	Bärenreiter-Verlag
<g/>
,	,	kIx,	,
Kassel	Kassel	k1gInSc1	Kassel
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-7618-1472-0	[number]	k4	3-7618-1472-0
(	(	kIx(	(
<g/>
Repr	Repra	k1gFnPc2	Repra
<g/>
.	.	kIx.	.
d	d	k?	d
<g/>
.	.	kIx.	.
</s>
<s>
Ausg	Ausg	k1gMnSc1	Ausg
<g/>
.	.	kIx.	.
</s>
<s>
Leipzig	Leipzig	k1gInSc1	Leipzig
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Geck	Geck	k1gMnSc1	Geck
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Rowohlt	Rowohlt	k1gInSc1	Rowohlt
<g/>
,	,	kIx,	,
Reinbek	Reinbek	k1gInSc1	Reinbek
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-499-50637-8	[number]	k4	3-499-50637-8
</s>
</p>
<p>
<s>
Maarten	Maarten	k2eAgInSc1d1	Maarten
'	'	kIx"	'
<g/>
t	t	k?	t
Hart	Hart	k1gMnSc1	Hart
<g/>
:	:	kIx,	:
Bach	Bach	k1gMnSc1	Bach
und	und	k?	und
ich	ich	k?	ich
<g/>
.	.	kIx.	.
</s>
<s>
Piper	Piper	k1gInSc1	Piper
Verlag	Verlag	k1gInSc1	Verlag
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-492-23296-5	[number]	k4	3-492-23296-5
(	(	kIx(	(
<g/>
s	s	k7c7	s
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Hausegger	Hausegger	k1gMnSc1	Hausegger
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
-	-	kIx~	-
Im	Im	k1gFnSc1	Im
Kontext	kontext	k1gInSc1	kontext
der	drát	k5eAaImRp2nS	drát
Musikgeschichte	Musikgeschicht	k1gInSc5	Musikgeschicht
<g/>
.	.	kIx.	.
</s>
<s>
ABOD	ABOD	kA	ABOD
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
CD	CD	kA	CD
ISBN	ISBN	kA	ISBN
3-8341-0171-0	[number]	k4	3-8341-0171-0
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Heinemann	Heinemann	k1gMnSc1	Heinemann
(	(	kIx(	(
<g/>
Hg	Hg	k1gMnSc1	Hg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Das	Das	k1gMnSc1	Das
Bach-Lexikon	Bach-Lexikon	k1gMnSc1	Bach-Lexikon
<g/>
.	.	kIx.	.
</s>
<s>
Laaber-Verlag	Laaber-Verlag	k1gInSc1	Laaber-Verlag
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Band	band	k1gInSc1	band
6	[number]	k4	6
des	des	k1gNnPc2	des
Bach-Handbuch	Bach-Handbucha	k1gFnPc2	Bach-Handbucha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-89007-456-1	[number]	k4	3-89007-456-1
</s>
</p>
<p>
<s>
Konrad	Konrad	k1gInSc1	Konrad
Küster	Küster	k1gInSc1	Küster
(	(	kIx(	(
<g/>
Hg	Hg	k1gFnSc1	Hg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Bach	Bach	k1gMnSc1	Bach
Handbuch	Handbuch	k1gMnSc1	Handbuch
<g/>
.	.	kIx.	.
</s>
<s>
Bärenreiter-Verlag	Bärenreiter-Verlag	k1gInSc1	Bärenreiter-Verlag
<g/>
,	,	kIx,	,
Kassel	Kassel	k1gInSc1	Kassel
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-7618-2000-3	[number]	k4	3-7618-2000-3
</s>
</p>
<p>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
Prautzsch	Prautzsch	k1gMnSc1	Prautzsch
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
verborgene	verborgen	k1gInSc5	verborgen
Symbolsprache	Symbolsprache	k1gNnSc1	Symbolsprache
Johann	Johann	k1gInSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bachs	Bachsa	k1gFnPc2	Bachsa
<g/>
,	,	kIx,	,
1.	[number]	k4	1.
-	-	kIx~	-
Zeichen	Zeichna	k1gFnPc2	Zeichna
<g/>
-	-	kIx~	-
und	und	k?	und
Zahlenalphabet	Zahlenalphabet	k1gInSc1	Zahlenalphabet
der	drát	k5eAaImRp2nS	drát
kirchenmusikalischen	kirchenmusikalischen	k1gInSc4	kirchenmusikalischen
Werke	Werk	k1gMnSc2	Werk
<g/>
.	.	kIx.	.
</s>
<s>
Merseburger	Merseburger	k1gMnSc1	Merseburger
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
Kassel	Kassel	k1gInSc1	Kassel
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3875372980	[number]	k4	3875372980
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Schmieder	Schmieder	k1gMnSc1	Schmieder
<g/>
:	:	kIx,	:
Bach-Werke-Verzeichnis	Bach-Werke-Verzeichnis	k1gFnSc1	Bach-Werke-Verzeichnis
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Thematisch-systematisches	Thematischystematisches	k1gInSc1	Thematisch-systematisches
Verzeichnis	Verzeichnis	k1gFnSc2	Verzeichnis
der	drát	k5eAaImRp2nS	drát
musikalischen	musikalischen	k1gInSc1	musikalischen
Werke	Werke	k1gNnSc1	Werke
von	von	k1gInSc1	von
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Breitkopf	Breitkopf	k1gInSc1	Breitkopf
&	&	k?	&
Härtel	Härtel	k1gInSc1	Härtel
<g/>
,	,	kIx,	,
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-7651-0255-5	[number]	k4	3-7651-0255-5
</s>
</p>
<p>
<s>
Gottfried	Gottfried	k1gMnSc1	Gottfried
Scholz	Scholz	k1gMnSc1	Scholz
<g/>
:	:	kIx,	:
Bachs	Bachs	k1gInSc1	Bachs
Passionen	Passionen	k2eAgInSc1d1	Passionen
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
musikalischer	musikalischra	k1gFnPc2	musikalischra
Werkführer	Werkführer	k1gMnSc1	Werkführer
<g/>
.	.	kIx.	.
</s>
<s>
Beck	Beck	k1gInSc1	Beck
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-406-43305-7	[number]	k4	3-406-43305-7
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Schweitzer	Schweitzer	k1gMnSc1	Schweitzer
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Breitkopf	Breitkopf	k1gInSc1	Breitkopf
&	&	k?	&
Härtel	Härtel	k1gInSc1	Härtel
<g/>
,	,	kIx,	,
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-7651-0034-X	[number]	k4	3-7651-0034-X
</s>
</p>
<p>
<s>
Philipp	Philipp	k1gMnSc1	Philipp
Spitta	Spitta	k1gMnSc1	Spitta
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
DOVER	DOVER	kA	DOVER
PUBN	PUBN	kA	PUBN
INC	INC	kA	INC
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1 1992	[number]	k4	1 1992
ISBN	ISBN	kA	ISBN
0-486-27412-8	[number]	k4	0-486-27412-8
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2 2000	[number]	k4	2 2000
ISBN	ISBN	kA	ISBN
0-486-27413-6	[number]	k4	0-486-27413-6
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3 1992	[number]	k4	3 1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-486-27414-4	[number]	k4	0-486-27414-4
</s>
</p>
<p>
<s>
Philipp	Philipp	k1gMnSc1	Philipp
Spitta	Spitta	k1gMnSc1	Spitta
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
2	[number]	k4	2
Bde	Bde	k1gMnSc1	Bde
<g/>
,	,	kIx,	,
Breitkopf	Breitkopf	k1gMnSc1	Breitkopf
&	&	k?	&
Härtel	Härtel	k1gInSc1	Härtel
<g/>
,	,	kIx,	,
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
1964	[number]	k4	1964
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Sanford	Sanford	k1gMnSc1	Sanford
Terry	Terra	k1gFnSc2	Terra
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Insel	Insel	k1gInSc1	Insel
Verlag	Verlag	k1gInSc1	Verlag
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-458-34288-5	[number]	k4	3-458-34288-5
</s>
</p>
<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Werner-Jensen	Werner-Jensen	k2eAgMnSc1d1	Werner-Jensen
<g/>
:	:	kIx,	:
Reclams	Reclams	k1gInSc1	Reclams
Musikführer	Musikführer	k1gMnSc1	Musikführer
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Bd	Bd	k?	Bd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Instrumentalmusik	Instrumentalmusik	k1gMnSc1	Instrumentalmusik
<g/>
,	,	kIx,	,
Bd	Bd	k1gMnSc1	Bd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
Vokalmusik	Vokalmusik	k1gInSc1	Vokalmusik
<g/>
,	,	kIx,	,
Philipp	Philipp	k1gInSc1	Philipp
Reclam	Reclam	k1gInSc1	Reclam
jun	jun	k1gMnSc1	jun
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
Christoph	Christoph	k1gMnSc1	Christoph
Wolff	Wolff	k1gMnSc1	Wolff
<g/>
:	:	kIx,	:
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
/	/	kIx~	/
<g/>
M.	M.	kA	M.
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-10-092584-X	[number]	k4	3-10-092584-X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Johann	Johanna	k1gFnPc2	Johanna
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Johann	Johann	k1gInSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bacha	k1gFnPc2	Bacha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Bach	Bacha	k1gFnPc2	Bacha
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaImAgMnS	dít
od	od	k7c2	od
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
Cantatas	Cantatas	k1gMnSc1	Cantatas
Website	Websit	k1gInSc5	Websit
a	a	k8xC	a
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Česky	Česko	k1gNnPc7	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
životopis	životopis	k1gInSc1	životopis
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
životopis	životopis	k1gInSc1	životopis
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
životopis	životopis	k1gInSc1	životopis
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Bachovi	Bach	k1gMnSc3	Bach
</s>
</p>
<p>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
Braniborských	braniborský	k2eAgInPc2d1	braniborský
koncertů	koncert	k1gInPc2	koncert
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
ze	z	k7c2	z
stránek	stránka	k1gFnPc2	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Noty	nota	k1gFnPc4	nota
a	a	k8xC	a
texty	text	k1gInPc4	text
===	===	k?	===
</s>
</p>
<p>
<s>
Mutopiaproject.org	Mutopiaproject.org	k1gMnSc1	Mutopiaproject.org
</s>
</p>
<p>
<s>
Klávesové	klávesový	k2eAgFnPc1d1	klávesová
kompozice	kompozice	k1gFnPc1	kompozice
jako	jako	k8xC	jako
pdf	pdf	k?	pdf
a	a	k8xC	a
midi	midi	k6eAd1	midi
</s>
</p>
<p>
<s>
Loutnové	loutnový	k2eAgFnPc1d1	loutnová
tabulatury	tabulatura	k1gFnPc1	tabulatura
</s>
</p>
<p>
<s>
Bach	Bach	k1gInSc1	Bach
Cantata	Cantat	k1gMnSc2	Cantat
Page	Pag	k1gMnSc2	Pag
-	-	kIx~	-
texty	text	k1gInPc1	text
vokálních	vokální	k2eAgNnPc2d1	vokální
děl	dělo	k1gNnPc2	dělo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
cizích	cizí	k2eAgInPc6d1	cizí
jazycích	jazyk	k1gInPc6	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaImAgMnS	dít
od	od	k7c2	od
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
/	/	kIx~	/
<g/>
cs	cs	k?	cs
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bachův	Bachův	k2eAgInSc1d1	Bachův
archiv	archiv	k1gInSc1	archiv
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Bachova	Bachův	k2eAgFnSc1d1	Bachova
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
,	,	kIx,	,
en	en	k?	en
<g/>
,	,	kIx,	,
fr	fr	k0	fr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bachův	Bachův	k2eAgInSc1d1	Bachův
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Eisenachu	Eisenach	k1gInSc6	Eisenach
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
,	,	kIx,	,
en	en	k?	en
<g/>
,	,	kIx,	,
fr	fr	k0	fr
<g/>
,	,	kIx,	,
es	es	k1gNnPc2	es
<g/>
,	,	kIx,	,
jp	jp	k?	jp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
na	na	k7c6	na
webu	web	k1gInSc6	web
Klassika	Klassika	k1gFnSc1	Klassika
<g/>
.	.	kIx.	.
<g/>
info	info	k1gNnSc1	info
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poslech	poslech	k1gInSc1	poslech
Bachových	Bachův	k2eAgNnPc2d1	Bachovo
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Bachovi	Bachovi	k1gRnPc1	Bachovi
(	(	kIx(	(
<g/>
sk	sk	k?	sk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JSBach.org	JSBach.org	k1gInSc1	JSBach.org
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Goldbergovy	Goldbergův	k2eAgFnPc1d1	Goldbergova
variace	variace	k1gFnPc1	variace
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Digitalbach	Digitalbacha	k1gFnPc2	Digitalbacha
oregonské	oregonský	k2eAgFnSc2d1	oregonská
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Mše	mše	k1gFnSc1	mše
h	h	k?	h
moll	moll	k1gNnSc1	moll
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Digitalbach	Digitalbacha	k1gFnPc2	Digitalbacha
oregonské	oregonský	k2eAgFnSc2d1	oregonská
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Matoušovy	Matoušův	k2eAgFnPc1d1	Matoušova
pašije	pašije	k1gFnPc1	pašije
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
,	,	kIx,	,
en	en	k?	en
<g/>
,	,	kIx,	,
fr	fr	k0	fr
<g/>
,	,	kIx,	,
es	es	k1gNnPc2	es
<g/>
,	,	kIx,	,
he	he	k0	he
<g/>
,	,	kIx,	,
it	it	k?	it
<g/>
,	,	kIx,	,
ja	ja	k?	ja
<g/>
,	,	kIx,	,
sw	sw	k?	sw
<g/>
,	,	kIx,	,
pt	pt	k?	pt
<g/>
,	,	kIx,	,
hu	hu	k0	hu
<g/>
,	,	kIx,	,
ru	ru	k?	ru
<g/>
,	,	kIx,	,
sv	sv	kA	sv
<g/>
,	,	kIx,	,
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
ca	ca	kA	ca
<g/>
,	,	kIx,	,
zh	zh	k?	zh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Flash	Flash	k1gInSc4	Flash
<g/>
)	)	kIx)	)
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc4d1	temperovaný
klavír	klavír	k1gInSc4	klavír
<g/>
:	:	kIx,	:
Korevaar	Korevaar	k1gInSc4	Korevaar
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc4	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Goeth	Goeth	k1gMnSc1	Goeth
(	(	kIx(	(
<g/>
órgano	órgana	k1gFnSc5	órgana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Parmentier	Parmentier	k1gInSc1	Parmentier
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
,	,	kIx,	,
<g/>
es	es	k1gNnSc1	es
<g/>
,	,	kIx,	,
pt	pt	k?	pt
<g/>
)	)	kIx)	)
</s>
</p>
