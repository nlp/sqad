<p>
<s>
Metodologie	metodologie	k1gFnSc1	metodologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
methodos	methodosa	k1gFnPc2	methodosa
<g/>
,	,	kIx,	,
sledování	sledování	k1gNnSc2	sledování
<g/>
,	,	kIx,	,
stopování	stopování	k1gNnSc2	stopování
<g/>
,	,	kIx,	,
od	od	k7c2	od
hodos	hodosa	k1gFnPc2	hodosa
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
aplikací	aplikace	k1gFnSc7	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
teorie	teorie	k1gFnSc2	teorie
či	či	k8xC	či
filosofie	filosofie	k1gFnSc2	filosofie
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
obecněji	obecně	k6eAd2	obecně
do	do	k7c2	do
epistemologie	epistemologie	k1gFnSc2	epistemologie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
angličtiny	angličtina	k1gFnSc2	angličtina
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
metodiku	metodika	k1gFnSc4	metodika
(	(	kIx(	(
<g/>
použitý	použitý	k2eAgInSc4d1	použitý
postup	postup	k1gInSc4	postup
či	či	k8xC	či
metodu	metoda	k1gFnSc4	metoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
však	však	k9	však
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
důležitý	důležitý	k2eAgInSc4d1	důležitý
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
metodami	metoda	k1gFnPc7	metoda
jako	jako	k8xS	jako
nástroji	nástroj	k1gInPc7	nástroj
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
a	a	k8xC	a
metodologií	metodologie	k1gFnSc7	metodologie
jako	jako	k8xS	jako
reflexí	reflexe	k1gFnSc7	reflexe
o	o	k7c4	o
vhodnosti	vhodnost	k1gFnPc4	vhodnost
či	či	k8xC	či
použitelnosti	použitelnost	k1gFnPc4	použitelnost
těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vědní	vědní	k2eAgFnPc1d1	vědní
discipliny	disciplina	k1gFnPc1	disciplina
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
svým	svůj	k3xOyFgInSc7	svůj
předmětem	předmět	k1gInSc7	předmět
a	a	k8xC	a
metodou	metoda	k1gFnSc7	metoda
(	(	kIx(	(
<g/>
metodami	metoda	k1gFnPc7	metoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
věd	věda	k1gFnPc2	věda
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
metodologie	metodologie	k1gFnSc1	metodologie
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
přítomností	přítomnost	k1gFnSc7	přítomnost
axiomů	axiom	k1gInPc2	axiom
a	a	k8xC	a
deduktivními	deduktivní	k2eAgInPc7d1	deduktivní
postupy	postup	k1gInPc7	postup
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
metodologie	metodologie	k1gFnSc1	metodologie
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
induktivními	induktivní	k2eAgInPc7d1	induktivní
postupy	postup	k1gInPc7	postup
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hypotézy	hypotéza	k1gFnSc2	hypotéza
a	a	k8xC	a
experimentálních	experimentální	k2eAgInPc2d1	experimentální
výsledků	výsledek	k1gInPc2	výsledek
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc2	měření
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
metodologie	metodologie	k1gFnSc1	metodologie
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
často	často	k6eAd1	často
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
metody	metoda	k1gFnPc4	metoda
kvantitativní	kvantitativní	k2eAgFnPc4d1	kvantitativní
(	(	kIx(	(
<g/>
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
měřeních	měření	k1gNnPc6	měření
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvalitativní	kvalitativní	k2eAgNnSc4d1	kvalitativní
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
metodologie	metodologie	k1gFnSc1	metodologie
humanitních	humanitní	k2eAgFnPc2d1	humanitní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
kritika	kritika	k1gFnSc1	kritika
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
<g/>
Úlohou	úloha	k1gFnSc7	úloha
metodologie	metodologie	k1gFnSc2	metodologie
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
</s>
</p>
<p>
<s>
systematické	systematický	k2eAgNnSc1d1	systematické
vytváření	vytváření	k1gNnSc1	vytváření
<g/>
,	,	kIx,	,
formulace	formulace	k1gFnSc1	formulace
a	a	k8xC	a
řazení	řazení	k1gNnSc1	řazení
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kritické	kritický	k2eAgNnSc4d1	kritické
posouzení	posouzení	k1gNnSc4	posouzení
předností	přednost	k1gFnPc2	přednost
a	a	k8xC	a
nedostatků	nedostatek	k1gInPc2	nedostatek
různých	různý	k2eAgFnPc2d1	různá
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
omezení	omezení	k1gNnPc2	omezení
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
diskuse	diskuse	k1gFnSc1	diskuse
vhodnosti	vhodnost	k1gFnSc2	vhodnost
metod	metoda	k1gFnPc2	metoda
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
účel	účel	k1gInSc4	účel
nebo	nebo	k8xC	nebo
zkoumání	zkoumání	k1gNnSc4	zkoumání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
metoda	metoda	k1gFnSc1	metoda
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
synonymum	synonymum	k1gNnSc1	synonymum
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
už	už	k6eAd1	už
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
představu	představa	k1gFnSc4	představa
spolehlivé	spolehlivý	k2eAgFnPc1d1	spolehlivá
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jednotné	jednotný	k2eAgFnSc2d1	jednotná
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
srovnávání	srovnávání	k1gNnSc2	srovnávání
výsledků	výsledek	k1gInPc2	výsledek
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vědu	věda	k1gFnSc4	věda
jako	jako	k8xS	jako
kolektivní	kolektivní	k2eAgFnSc4d1	kolektivní
činnost	činnost	k1gFnSc4	činnost
poprvé	poprvé	k6eAd1	poprvé
podal	podat	k5eAaPmAgMnS	podat
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Kusánský	Kusánský	k2eAgMnSc1d1	Kusánský
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
"	"	kIx"	"
<g/>
Soukromník	soukromník	k1gMnSc1	soukromník
o	o	k7c6	o
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
váhami	váha	k1gFnPc7	váha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1440	[number]	k4	1440
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
úvahami	úvaha	k1gFnPc7	úvaha
o	o	k7c6	o
obecné	obecný	k2eAgFnSc6d1	obecná
metodě	metoda	k1gFnSc6	metoda
vědy	věda	k1gFnSc2	věda
zabýval	zabývat	k5eAaImAgMnS	zabývat
anglický	anglický	k2eAgMnSc1d1	anglický
filosof	filosof	k1gMnSc1	filosof
Francis	Francis	k1gFnSc2	Francis
Bacon	Bacon	k1gMnSc1	Bacon
a	a	k8xC	a
zejména	zejména	k9	zejména
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filosof	filosof	k1gMnSc1	filosof
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
v	v	k7c6	v
"	"	kIx"	"
<g/>
Rozpravě	rozprava	k1gFnSc6	rozprava
o	o	k7c6	o
metodě	metoda	k1gFnSc6	metoda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
jako	jako	k8xS	jako
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
návod	návod	k1gInSc1	návod
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
nepochybných	pochybný	k2eNgInPc2d1	nepochybný
a	a	k8xC	a
porovnatelných	porovnatelný	k2eAgInPc2d1	porovnatelný
výsledků	výsledek	k1gInPc2	výsledek
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
rozvoje	rozvoj	k1gInSc2	rozvoj
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
metodám	metoda	k1gFnPc3	metoda
a	a	k8xC	a
metodologii	metodologie	k1gFnSc3	metodologie
vědy	věda	k1gFnSc2	věda
věnovali	věnovat	k5eAaPmAgMnP	věnovat
zejména	zejména	k9	zejména
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
Henri	Henr	k1gMnPc1	Henr
Poincaré	Poincarý	k2eAgFnSc2d1	Poincarý
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Popper	Popper	k1gMnSc1	Popper
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Metodologie	metodologie	k1gFnSc1	metodologie
a	a	k8xC	a
dějiny	dějiny	k1gFnPc1	dějiny
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
:	:	kIx,	:
základní	základní	k2eAgInSc4d1	základní
přehled	přehled	k1gInSc4	přehled
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
TF	tf	k0wR	tf
JU	ju	k0	ju
2005	[number]	k4	2005
-	-	kIx~	-
ISBN	ISBN	kA	ISBN
80-7040-778-6	[number]	k4	80-7040-778-6
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
,	,	kIx,	,
Rozprava	rozprava	k1gFnSc1	rozprava
o	o	k7c6	o
metodě	metoda	k1gFnSc6	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	svoboda	k1gFnSc1	svoboda
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
B.	B.	kA	B.
Fajkus	Fajkus	k1gMnSc1	Fajkus
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
a	a	k8xC	a
metodologie	metodologie	k1gFnSc1	metodologie
vědy	věda	k1gFnSc2	věda
<g/>
:	:	kIx,	:
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
současnost	současnost	k1gFnSc4	současnost
a	a	k8xC	a
perspektivy	perspektiva	k1gFnPc4	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
-	-	kIx~	-
339	[number]	k4	339
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-200-1304-0	[number]	k4	80-200-1304-0
</s>
</p>
<p>
<s>
Maňák	Maňák	k1gMnSc1	Maňák
–	–	k?	–
Švec	Švec	k1gMnSc1	Švec
<g/>
,	,	kIx,	,
Slovník	slovník	k1gInSc1	slovník
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
metodologie	metodologie	k1gFnSc2	metodologie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
-	-	kIx~	-
134	[number]	k4	134
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7315-102-2	[number]	k4	80-7315-102-2
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
nové	nový	k2eAgFnSc2d1	nová
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
hesla	heslo	k1gNnSc2	heslo
Metoda	metoda	k1gFnSc1	metoda
–	–	k?	–
metodologie	metodologie	k1gFnSc2	metodologie
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
fysiky	fysika	k1gFnSc2	fysika
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
7	[number]	k4	7
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
207	[number]	k4	207
nn	nn	k?	nn
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
Metodologie	metodologie	k1gFnSc1	metodologie
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
-	-	kIx~	-
354	[number]	k4	354
s.	s.	k?	s.
;	;	kIx,	;
20	[number]	k4	20
cm	cm	kA	cm
ISBN	ISBN	kA	ISBN
80-86005-48-8	[number]	k4	80-86005-48-8
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dedukce	dedukce	k1gFnSc1	dedukce
</s>
</p>
<p>
<s>
Logická	logický	k2eAgFnSc1d1	logická
indukce	indukce	k1gFnSc1	indukce
</s>
</p>
<p>
<s>
Kvalitativní	kvalitativní	k2eAgInSc1d1	kvalitativní
výzkum	výzkum	k1gInSc1	výzkum
</s>
</p>
<p>
<s>
Kvantitativní	kvantitativní	k2eAgInSc1d1	kvantitativní
výzkum	výzkum	k1gInSc1	výzkum
</s>
</p>
<p>
<s>
Metoda	metoda	k1gFnSc1	metoda
</s>
</p>
<p>
<s>
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
metoda	metoda	k1gFnSc1	metoda
</s>
</p>
<p>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
podvod	podvod	k1gInSc1	podvod
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
metodologie	metodologie	k1gFnSc2	metodologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stanford	Stanford	k1gInSc1	Stanford
encyclopedia	encyclopedium	k1gNnSc2	encyclopedium
of	of	k?	of
philosophy	philosoph	k1gInPc1	philosoph
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Science	Science	k1gFnSc2	Science
and	and	k?	and
Pseudo-Science	Pseudo-Science	k1gFnSc2	Pseudo-Science
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Dictionary	Dictionar	k1gInPc1	Dictionar
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Methodology	Methodolog	k1gMnPc7	Methodolog
</s>
</p>
