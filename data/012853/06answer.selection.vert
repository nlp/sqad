<s>
Metodologie	metodologie	k1gFnSc1	metodologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
methodos	methodosa	k1gFnPc2	methodosa
<g/>
,	,	kIx,	,
sledování	sledování	k1gNnSc2	sledování
<g/>
,	,	kIx,	,
stopování	stopování	k1gNnSc2	stopování
<g/>
,	,	kIx,	,
od	od	k7c2	od
hodos	hodosa	k1gFnPc2	hodosa
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
aplikací	aplikace	k1gFnSc7	aplikace
<g/>
.	.	kIx.	.
</s>
