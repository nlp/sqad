<s>
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
NaCl	NaCl	k1gInSc1	NaCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
prostě	prostě	k9	prostě
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc7d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nerostu	nerost	k1gInSc2	nerost
halitu	halit	k1gInSc2	halit
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
též	též	k9	též
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
.	.	kIx.	.
</s>
