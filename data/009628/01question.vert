<s>
Je	být	k5eAaImIp3nS	
studiový	studiový	k2eAgMnSc1d1	studiový
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	
hudebník	hudebník	k1gMnSc1	hudebník
najatý	najatý	k2eAgMnSc1d1	najatý
za	za	k7c7	
účelem	účel	k1gInSc7	účel
nahrání	nahrání	k1gNnSc2	nahrání
nástrojových	nástrojový	k2eAgInPc2d1	nástrojový
partů	part	k1gInPc2	part
na	na	k7c4	
album	album	k1gNnSc4	album
(	(	kIx(	
<g/>
případně	případně	k6eAd1	
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	
<g/>
,	,	kIx,	
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1	
není	být	k5eNaImIp3nS	
členem	člen	k1gMnSc7	člen
dané	daný	k2eAgFnSc2d1	daná
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
<g/>
?	?	kIx.	
</s>
