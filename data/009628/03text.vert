<p>
<s>
Studiový	studiový	k2eAgMnSc1d1	studiový
hudebník	hudebník	k1gMnSc1	hudebník
je	být	k5eAaImIp3nS	být
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
najatý	najatý	k2eAgMnSc1d1	najatý
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
nahrání	nahrání	k1gNnSc2	nahrání
nástrojových	nástrojový	k2eAgInPc2d1	nástrojový
partů	part	k1gInPc2	part
na	na	k7c4	na
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
dané	daný	k2eAgFnSc2d1	daná
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
hudebníky	hudebník	k1gMnPc4	hudebník
na	na	k7c6	na
sólové	sólový	k2eAgFnSc6d1	sólová
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vznikají	vznikat	k5eAaImIp3nP	vznikat
celé	celý	k2eAgFnPc1d1	celá
skupiny	skupina	k1gFnPc1	skupina
studiových	studiový	k2eAgMnPc2d1	studiový
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
The	The	k1gMnSc3	The
Wrecking	Wrecking	k1gInSc4	Wrecking
Crew	Crew	k1gMnPc2	Crew
a	a	k8xC	a
The	The	k1gMnPc2	The
Funk	funk	k1gInSc1	funk
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
.	.	kIx.	.
</s>
<s>
Studioví	studiový	k2eAgMnPc1d1	studiový
hudebníci	hudebník	k1gMnPc1	hudebník
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
schopni	schopen	k2eAgMnPc1d1	schopen
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
hudebních	hudební	k2eAgInPc6d1	hudební
stylech	styl	k1gInPc6	styl
a	a	k8xC	a
žánrech	žánr	k1gInPc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hudebních	hudební	k2eAgNnPc2d1	hudební
alb	album	k1gNnPc2	album
a	a	k8xC	a
singlů	singl	k1gInPc2	singl
mohou	moct	k5eAaImIp3nP	moct
studioví	studiový	k2eAgMnPc1d1	studiový
hudebníci	hudebník	k1gMnPc1	hudebník
hrát	hrát	k5eAaImF	hrát
také	také	k9	také
na	na	k7c6	na
reklamních	reklamní	k2eAgFnPc6d1	reklamní
znělkách	znělka	k1gFnPc6	znělka
či	či	k8xC	či
filmové	filmový	k2eAgFnSc3d1	filmová
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
studiové	studiový	k2eAgMnPc4d1	studiový
hudebníky	hudebník	k1gMnPc4	hudebník
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Frank	Frank	k1gMnSc1	Frank
Ricotti	Ricotť	k1gFnSc2	Ricotť
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Spedding	Spedding	k1gInSc1	Spedding
a	a	k8xC	a
Hugh	Hugh	k1gInSc1	Hugh
McCracken	McCracken	k2eAgInSc1d1	McCracken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
studiový	studiový	k2eAgMnSc1d1	studiový
hudebník	hudebník	k1gMnSc1	hudebník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
MCLANE	MCLANE	kA	MCLANE
<g/>
,	,	kIx,	,
Daisann	Daisann	k1gInSc1	Daisann
<g/>
.	.	kIx.	.
</s>
<s>
Session	Session	k1gInSc1	Session
Musicians	Musicians	k1gInSc1	Musicians
<g/>
:	:	kIx,	:
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Invisible	Invisible	k1gFnSc7	Invisible
Elite	Elit	k1gMnSc5	Elit
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
1979-09-06	[number]	k4	1979-09-06
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MCDONALD	MCDONALD	kA	MCDONALD
<g/>
,	,	kIx,	,
Heather	Heathra	k1gFnPc2	Heathra
<g/>
.	.	kIx.	.
</s>
<s>
What	What	k1gMnSc1	What
Is	Is	k1gMnSc1	Is
a	a	k8xC	a
Session	Session	k1gInSc1	Session
Musician	Musiciana	k1gFnPc2	Musiciana
<g/>
?	?	kIx.	?
</s>
<s>
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Balance	balance	k1gFnSc1	balance
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2017-02-18	[number]	k4	2017-02-18
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
