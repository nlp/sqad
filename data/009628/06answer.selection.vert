<s>
Studiový	studiový	k2eAgMnSc1d1	studiový
hudebník	hudebník	k1gMnSc1	hudebník
je	být	k5eAaImIp3nS	být
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
najatý	najatý	k2eAgMnSc1d1	najatý
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
nahrání	nahrání	k1gNnSc2	nahrání
nástrojových	nástrojový	k2eAgInPc2d1	nástrojový
partů	part	k1gInPc2	part
na	na	k7c4	na
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
dané	daný	k2eAgFnSc2d1	daná
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
