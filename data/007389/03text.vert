<s>
Cuba	Cuba	k6eAd1	Cuba
Libre	Libr	k1gInSc5	Libr
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
koktejl	koktejl	k1gInSc4	koktejl
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Cuba	Cub	k1gInSc2	Cub
Libre	Libr	k1gInSc5	Libr
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
připíjeli	připíjet	k5eAaImAgMnP	připíjet
na	na	k7c4	na
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
Kubu	Kuba	k1gFnSc4	Kuba
právě	právě	k6eAd1	právě
nápojem	nápoj	k1gInSc7	nápoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
označován	označovat	k5eAaImNgMnS	označovat
jako	jako	k8xC	jako
Bacardi	Bacard	k1gMnPc1	Bacard
<g/>
&	&	k?	&
<g/>
Coke	Coke	k1gFnSc1	Coke
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k6eAd1	navzdory
své	svůj	k3xOyFgNnSc4	svůj
(	(	kIx(	(
<g/>
relativně	relativně	k6eAd1	relativně
<g/>
)	)	kIx)	)
vyšší	vysoký	k2eAgFnSc3d2	vyšší
ceně	cena	k1gFnSc3	cena
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
oblíben	oblíben	k2eAgMnSc1d1	oblíben
hlavně	hlavně	k6eAd1	hlavně
mezi	mezi	k7c7	mezi
mládeží	mládež	k1gFnSc7	mládež
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
cl	cl	k?	cl
BACARDI	BACARDI	kA	BACARDI
GOLD	GOLD	kA	GOLD
rumu	rum	k1gInSc2	rum
10-12	[number]	k4	10-12
cl	cl	k?	cl
coly	cola	k1gFnSc2	cola
limetka	limetka	k1gFnSc1	limetka
kostky	kostka	k1gFnSc2	kostka
ledu	led	k1gInSc2	led
Do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
se	se	k3xPyFc4	se
nalije	nalít	k5eAaBmIp3nS	nalít
tmavý	tmavý	k2eAgInSc1d1	tmavý
rum	rum	k1gInSc1	rum
<g/>
,	,	kIx,	,
vymačká	vymačkat	k5eAaPmIp3nS	vymačkat
limetka	limetka	k1gFnSc1	limetka
<g/>
,	,	kIx,	,
přidá	přidat	k5eAaPmIp3nS	přidat
led	led	k1gInSc1	led
a	a	k8xC	a
dolije	dolít	k5eAaPmIp3nS	dolít
se	se	k3xPyFc4	se
colou	cola	k1gFnSc7	cola
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
sami	sám	k3xTgMnPc1	sám
Kubánci	Kubánec	k1gMnPc1	Kubánec
rum	rum	k1gInSc4	rum
neodměřují	odměřovat	k5eNaImIp3nP	odměřovat
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
luxusních	luxusní	k2eAgInPc6d1	luxusní
hotelech	hotel	k1gInPc6	hotel
se	se	k3xPyFc4	se
nápoj	nápoj	k1gInSc1	nápoj
připravuje	připravovat	k5eAaImIp3nS	připravovat
"	"	kIx"	"
<g/>
od	od	k7c2	od
oka	oko	k1gNnSc2	oko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sex	sex	k1gInSc1	sex
on	on	k3xPp3gMnSc1	on
the	the	k?	the
beach	beach	k1gMnSc1	beach
Mojito	Mojita	k1gMnSc5	Mojita
Piñ	Piñ	k1gFnSc6	Piñ
Colada	Colada	k1gFnSc1	Colada
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cuba	Cub	k1gInSc2	Cub
Libre	Libr	k1gInSc5	Libr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Recept	recept	k1gInSc1	recept
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
připravit	připravit	k5eAaPmF	připravit
pravé	pravá	k1gFnPc4	pravá
Cuba	Cubum	k1gNnSc2	Cubum
Libre	Libr	k1gInSc5	Libr
</s>
