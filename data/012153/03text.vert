<p>
<s>
Úvoz	úvoz	k1gInSc1	úvoz
nebo	nebo	k8xC	nebo
úvozová	úvozový	k2eAgFnSc1d1	úvozová
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zařezává	zařezávat	k5eAaImIp3nS	zařezávat
do	do	k7c2	do
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
Úvozové	úvozový	k2eAgFnPc1d1	úvozová
cesty	cesta	k1gFnPc1	cesta
obvykle	obvykle	k6eAd1	obvykle
bývají	bývat	k5eAaImIp3nP	bývat
staré	starý	k2eAgFnPc4d1	stará
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
už	už	k6eAd1	už
jen	jen	k9	jen
nesouvisle	souvisle	k6eNd1	souvisle
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kolové	kolový	k2eAgFnSc3d1	kolová
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dost	dost	k6eAd1	dost
široké	široký	k2eAgNnSc1d1	široké
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jimi	on	k3xPp3gMnPc7	on
projel	projet	k5eAaPmAgMnS	projet
vůz	vůz	k1gInSc4	vůz
s	s	k7c7	s
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
tak	tak	k6eAd1	tak
úzké	úzký	k2eAgNnSc1d1	úzké
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
projel	projet	k5eAaPmAgMnS	projet
pouze	pouze	k6eAd1	pouze
kůň	kůň	k1gMnSc1	kůň
či	či	k8xC	či
jiné	jiný	k2eAgNnSc4d1	jiné
tažné	tažný	k2eAgNnSc4d1	tažné
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Úvozy	úvoz	k1gInPc1	úvoz
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
totožné	totožný	k2eAgInPc1d1	totožný
s	s	k7c7	s
trvalými	trvalý	k2eAgFnPc7d1	trvalá
vodotečemi	vodoteč	k1gFnPc7	vodoteč
či	či	k8xC	či
místy	místy	k6eAd1	místy
nárazového	nárazový	k2eAgInSc2d1	nárazový
či	či	k8xC	či
sezonního	sezonní	k2eAgInSc2d1	sezonní
stoku	stok	k1gInSc2	stok
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
přispět	přispět	k5eAaPmF	přispět
prohloubení	prohloubení	k1gNnSc1	prohloubení
úvozu	úvoz	k1gInSc2	úvoz
vodní	vodní	k2eAgFnSc7d1	vodní
erozí	eroze	k1gFnSc7	eroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahloubení	zahloubení	k1gNnSc1	zahloubení
úvozových	úvozový	k2eAgFnPc2d1	úvozová
cest	cesta	k1gFnPc2	cesta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
od	od	k7c2	od
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
až	až	k6eAd1	až
po	po	k7c4	po
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zanoření	zanoření	k1gNnSc1	zanoření
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
terénu	terén	k1gInSc2	terén
mohlo	moct	k5eAaImAgNnS	moct
poskytovat	poskytovat	k5eAaImF	poskytovat
cestujícím	cestující	k1gMnPc3	cestující
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
nepřízní	nepřízeň	k1gFnSc7	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ukrytí	ukrytý	k2eAgMnPc1d1	ukrytý
před	před	k7c7	před
lupiči	lupič	k1gMnPc7	lupič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
úvozové	úvozový	k2eAgFnPc1d1	úvozová
cesty	cesta	k1gFnPc1	cesta
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
zpevněny	zpevnit	k5eAaPmNgFnP	zpevnit
štěrkem	štěrk	k1gInSc7	štěrk
<g/>
,	,	kIx,	,
vydlážděny	vydlážděn	k2eAgMnPc4d1	vydlážděn
nebo	nebo	k8xC	nebo
vyasfaltovány	vyasfaltován	k2eAgMnPc4d1	vyasfaltován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
městech	město	k1gNnPc6	město
a	a	k8xC	a
obcích	obec	k1gFnPc6	obec
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc2	jejich
někdejší	někdejší	k2eAgFnSc2d1	někdejší
existence	existence	k1gFnSc2	existence
dochovala	dochovat	k5eAaPmAgFnS	dochovat
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
názvech	název	k1gInPc6	název
ulic	ulice	k1gFnPc2	ulice
(	(	kIx(	(
<g/>
Úvoz	úvoz	k1gInSc1	úvoz
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Úvoze	úvoz	k1gInSc6	úvoz
<g/>
,	,	kIx,	,
Nad	nad	k7c7	nad
Úvozem	úvoz	k1gInSc7	úvoz
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgInSc1d1	dolní
úvoz	úvoz	k1gInSc1	úvoz
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ukryta	ukrýt	k5eAaPmNgFnS	ukrýt
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
pro	pro	k7c4	pro
silniční	silniční	k2eAgInSc4d1	silniční
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Průhon	průhon	k1gInSc1	průhon
</s>
</p>
<p>
<s>
Úval	úval	k1gInSc1	úval
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
úvozová	úvozový	k2eAgFnSc1d1	úvozová
cesta	cesta	k1gFnSc1	cesta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
