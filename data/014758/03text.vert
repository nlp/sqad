<s>
Garota	Garota	k1gFnSc1
</s>
<s>
Garota	Garota	k1gFnSc1
<g/>
,	,	kIx,
též	též	k9
garotta	garotta	k1gMnSc1
nebo	nebo	k8xC
garote	garot	k1gInSc5
<g/>
,	,	kIx,
ze	z	k7c2
šp	šp	k?
<g/>
.	.	kIx.
garrote	garrot	k1gMnSc5
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
popravčí	popravčí	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
používal	používat	k5eAaImAgInS
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
<g/>
,	,	kIx,
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
jejich	jejich	k3xOp3gFnPc6
koloniích	kolonie	k1gFnPc6
a	a	k8xC
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
kdysi	kdysi	k6eAd1
pod	pod	k7c7
jejich	jejich	k3xOp3gInSc7
vlivem	vliv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedící	sedící	k2eAgFnSc1d1
odsouzenec	odsouzenec	k1gMnSc1
byl	být	k5eAaImAgInS
pomocí	pomocí	k7c2
zvláštního	zvláštní	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
udušen	udušen	k2eAgMnSc1d1
nebo	nebo	k8xC
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
zlomen	zlomen	k2eAgInSc4d1
hrtan	hrtan	k1gInSc4
a	a	k8xC
trachea	trachea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Garota	Garota	k1gFnSc1
byla	být	k5eAaImAgFnS
používána	používat	k5eAaImNgFnS
od	od	k7c2
středověku	středověk	k1gInSc2
do	do	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
už	už	k9
pravděpodobně	pravděpodobně	k6eAd1
nikde	nikde	k6eAd1
pomocí	pomocí	k7c2
garoty	garota	k1gFnSc2
nepopravuje	popravovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Výkon	výkon	k1gInSc1
popravy	poprava	k1gFnSc2
</s>
<s>
Postup	postup	k1gInSc1
popravy	poprava	k1gFnSc2
</s>
<s>
Garota	Garota	k1gFnSc1
je	být	k5eAaImIp3nS
nástroj	nástroj	k1gInSc1
používaný	používaný	k2eAgInSc1d1
k	k	k7c3
popravě	poprava	k1gFnSc3
zardoušením	zardoušení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odsouzenec	odsouzenec	k1gMnSc1
sedí	sedit	k5eAaImIp3nS
na	na	k7c6
lavici	lavice	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
opřen	opřít	k5eAaPmNgInS
o	o	k7c4
kůl	kůl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kůlu	kůl	k1gInSc6
je	být	k5eAaImIp3nS
připevněn	připevněn	k2eAgInSc1d1
železný	železný	k2eAgInSc1d1
kruh	kruh	k1gInSc1
(	(	kIx(
<g/>
obojek	obojek	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
odsouzenci	odsouzenec	k1gMnPc1
svírá	svírat	k5eAaImIp3nS
krk	krk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obojek	obojek	k1gInSc1
je	být	k5eAaImIp3nS
pomalu	pomalu	k6eAd1
utahován	utahovat	k5eAaImNgInS
šroubem	šroub	k1gInSc7
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
odsouzenec	odsouzenec	k1gMnSc1
neudusí	udusit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jiné	jiný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
je	být	k5eAaImIp3nS
garota	garota	k1gFnSc1
drát	drát	k5eAaImF
s	s	k7c7
dřevěnými	dřevěný	k2eAgFnPc7d1
rukojeťmi	rukojeť	k1gFnPc7
na	na	k7c6
koncích	konec	k1gInPc6
a	a	k8xC
je	být	k5eAaImIp3nS
držena	držet	k5eAaImNgFnS
popravčím	popravčí	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
další	další	k2eAgInSc1d1
typ	typ	k1gInSc1
garoty	garota	k1gFnSc2
<g/>
,	,	kIx,
u	u	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
byla	být	k5eAaImAgFnS
ke	k	k7c3
šroubu	šroub	k1gInSc3
připevněna	připevněn	k2eAgNnPc4d1
dvě	dva	k4xCgNnPc4
ramena	rameno	k1gNnPc4
se	s	k7c7
zátěží	zátěž	k1gFnSc7
na	na	k7c6
konci	konec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačilo	stačit	k5eAaBmAgNnS
za	za	k7c4
ně	on	k3xPp3gMnPc4
jen	jen	k9
prudce	prudko	k6eAd1
škubnout	škubnout	k5eAaPmF
a	a	k8xC
rychle	rychle	k6eAd1
se	se	k3xPyFc4
otáčející	otáčející	k2eAgInSc4d1
šroub	šroub	k1gInSc4
zlomil	zlomit	k5eAaPmAgMnS
tracheu	trachea	k1gFnSc4
a	a	k8xC
hrtan	hrtan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kůlu	kůl	k1gInSc6
byl	být	k5eAaImAgInS
navíc	navíc	k6eAd1
ještě	ještě	k9
výstupek	výstupek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zároveň	zároveň	k6eAd1
rozdrtil	rozdrtit	k5eAaPmAgInS
mozeček	mozeček	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytla	vyskytnout	k5eAaPmAgFnS
se	se	k3xPyFc4
i	i	k9
garota	garota	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgInPc7
prstenci	prstenec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
prstenec	prstenec	k1gInSc1
se	se	k3xPyFc4
pohyboval	pohybovat	k5eAaImAgInS
dopředu	dopředu	k6eAd1
a	a	k8xC
druhý	druhý	k4xOgInSc4
dozadu	dozadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
prstence	prstenec	k1gInPc1
vyvíjely	vyvíjet	k5eAaImAgInP
tlak	tlak	k1gInSc4
na	na	k7c4
dva	dva	k4xCgInPc4
sousední	sousední	k2eAgInPc4d1
obratle	obratel	k1gInPc4
<g/>
,	,	kIx,
vedlo	vést	k5eAaImAgNnS
to	ten	k3xDgNnSc1
k	k	k7c3
přerušení	přerušení	k1gNnSc3
míchy	mícha	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
téměř	téměř	k6eAd1
okamžitou	okamžitý	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
posledních	poslední	k2eAgInPc2d1
typů	typ	k1gInPc2
obsahoval	obsahovat	k5eAaImAgInS
tenké	tenký	k2eAgNnSc4d1
ostří	ostří	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
procházelo	procházet	k5eAaImAgNnS
skrze	skrze	k?
kůl	kůl	k1gInSc4
a	a	k8xC
bleskurychle	bleskurychle	k6eAd1
přeťalo	přetít	k5eAaPmAgNnS
míchu	mícha	k1gFnSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
obratli	obratel	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
garoty	garota	k1gFnSc2
</s>
<s>
Poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgFnS
garota	garota	k1gFnSc1
užívána	užívat	k5eAaImNgFnS
ve	v	k7c6
středověku	středověk	k1gInSc6
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
a	a	k8xC
jejich	jejich	k3xOp3gFnPc6
koloniích	kolonie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
i	i	k9
při	při	k7c6
popravě	poprava	k1gFnSc6
inckého	incký	k2eAgMnSc2d1
krále	král	k1gMnSc2
Atahualpy	Atahualpa	k1gFnSc2
roku	rok	k1gInSc2
1533	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1810	#num#	k4
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
garota	garota	k1gFnSc1
užívána	užíván	k2eAgFnSc1d1
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
oficiálně	oficiálně	k6eAd1
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1828	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jediným	jediný	k2eAgInSc7d1
povoleným	povolený	k2eAgInSc7d1
civilním	civilní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
popravy	poprava	k1gFnSc2
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1851	#num#	k4
se	se	k3xPyFc4
totéž	týž	k3xTgNnSc1
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
<g/>
,	,	kIx,
tam	tam	k6eAd1
ale	ale	k9
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
zrušen	zrušit	k5eAaPmNgInS
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
a	a	k8xC
garota	garota	k1gFnSc1
tam	tam	k6eAd1
nikdy	nikdy	k6eAd1
nebyla	být	k5eNaImAgFnS
použita	použít	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1897	#num#	k4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
k	k	k7c3
poslední	poslední	k2eAgFnSc3d1
veřejné	veřejný	k2eAgFnSc3d1
popravě	poprava	k1gFnSc3
garotou	garota	k1gFnSc7
<g/>
,	,	kIx,
následující	následující	k2eAgFnPc4d1
popravy	poprava	k1gFnPc4
už	už	k9
byly	být	k5eAaImAgFnP
bez	bez	k7c2
přístupu	přístup	k1gInSc2
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
civilní	civilní	k2eAgFnSc1d1
poprava	poprava	k1gFnSc1
garotou	garota	k1gFnSc7
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
v	v	k7c6
červnu	červen	k1gInSc6
1959	#num#	k4
<g/>
,	,	kIx,
posledním	poslední	k2eAgMnPc3d1
popraveným	popravený	k2eAgMnPc3d1
byl	být	k5eAaImAgMnS
José	Josá	k1gFnPc4
María	Maríum	k1gNnSc2
Jarabo	Jaraba	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1973	#num#	k4
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
civilní	civilní	k2eAgInSc1d1
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledními	poslední	k2eAgInPc7d1
popravenými	popravený	k2eAgInPc7d1
garotou	garota	k1gFnSc7
byli	být	k5eAaImAgMnP
Heinz	Heinz	k1gMnSc1
Chez	Chez	k1gMnSc1
a	a	k8xC
Salvador	Salvador	k1gMnSc1
Puig	Puig	k1gMnSc1
Antich	Antich	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
byl	být	k5eAaImAgInS
obnoven	obnovit	k5eAaPmNgInS
civilní	civilní	k2eAgInSc1d1
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
garotou	garota	k1gFnSc7
za	za	k7c4
loupežnou	loupežný	k2eAgFnSc4d1
vraždu	vražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgMnPc1d1
člověk	člověk	k1gMnSc1
odsouzený	odsouzený	k1gMnSc1
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
garotou	garota	k1gFnSc7
byl	být	k5eAaImAgMnS
pedofilní	pedofilní	k2eAgMnSc1d1
loupežný	loupežný	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
José	Josá	k1gFnSc2
Luis	Luisa	k1gFnPc2
Cerveto	Cerveto	k1gNnSc1
roku	rok	k1gInSc2
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
prováděn	provádět	k5eAaImNgMnS
garotou	garota	k1gFnSc7
<g/>
,	,	kIx,
i	i	k8xC
v	v	k7c6
Andoře	Andorra	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
posledním	poslední	k2eAgNnSc7d1
popraveným	popravený	k2eAgNnSc7d1
Antone	Anton	k1gMnSc5
Arenis	Arenis	k1gFnSc6
roku	rok	k1gInSc2
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Garota	Garota	k1gFnSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Garota	Garota	k1gFnSc1
ve	v	k7c6
filmu	film	k1gInSc6
</s>
<s>
Garota	Garota	k1gFnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
ve	v	k7c6
filmu	film	k1gInSc6
např.	např.	kA
Jeden	jeden	k4xCgInSc4
svět	svět	k1gInSc4
nestačí	stačit	k5eNaBmIp3nS
filmového	filmový	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
o	o	k7c4
Jamesi	Jamese	k1gFnSc4
Bondovi	Bonda	k1gMnSc3
<g/>
,	,	kIx,
ve	v	k7c6
1492	#num#	k4
<g/>
:	:	kIx,
Dobytí	dobytí	k1gNnSc4
ráje	ráj	k1gInSc2
režiséra	režisér	k1gMnSc2
Ridleyho	Ridley	k1gMnSc2
Scotta	Scott	k1gMnSc2
<g/>
,	,	kIx,
ve	v	k7c6
filmu	film	k1gInSc6
Miloše	Miloš	k1gMnSc2
Formana	Forman	k1gMnSc2
Goyovy	Goyův	k2eAgInPc1d1
přízraky	přízrak	k1gInPc1
nebo	nebo	k8xC
ve	v	k7c6
filmu	film	k1gInSc6
Oro	Oro	k1gMnSc2
režiséra	režisér	k1gMnSc2
Agustína	Agustín	k1gMnSc2
Díaze	Díaze	k1gFnSc1
Yanese	Yanese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Garota	Garot	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
</s>
<s>
Oběšení	oběšení	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1075496691	#num#	k4
</s>
