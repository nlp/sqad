<p>
<s>
Hébé	Hébé	k1gFnSc1	Hébé
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ἥ	Ἥ	k?	Ἥ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Hebe	Hebe	k1gFnSc1	Hebe
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
bohyně	bohyně	k1gFnSc2	bohyně
věčné	věčný	k2eAgFnSc2d1	věčná
mladosti	mladost	k1gFnSc2	mladost
a	a	k8xC	a
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
číšnice	číšnice	k1gFnSc1	číšnice
olympských	olympský	k2eAgMnPc2d1	olympský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
boha	bůh	k1gMnSc4	bůh
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Héry	Héra	k1gFnSc2	Héra
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
zrodila	zrodit	k5eAaPmAgFnS	zrodit
Héra	Héra	k1gFnSc1	Héra
sama	sám	k3xTgFnSc1	sám
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Zeus	Zeus	k1gInSc1	Zeus
Athénu	Athéna	k1gFnSc4	Athéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Olympu	Olymp	k1gInSc6	Olymp
má	mít	k5eAaImIp3nS	mít
významnou	významný	k2eAgFnSc4d1	významná
funkci	funkce	k1gFnSc4	funkce
<g/>
:	:	kIx,	:
nalévá	nalévat	k5eAaImIp3nS	nalévat
bohům	bůh	k1gMnPc3	bůh
nektar	nektar	k1gInSc4	nektar
<g/>
,	,	kIx,	,
nápoj	nápoj	k1gInSc4	nápoj
věčné	věčný	k2eAgFnSc2d1	věčná
mladosti	mladost	k1gFnSc2	mladost
<g/>
,	,	kIx,	,
devětkrát	devětkrát	k6eAd1	devětkrát
sladší	sladký	k2eAgFnSc1d2	sladší
než	než	k8xS	než
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
bohové	bůh	k1gMnPc1	bůh
pili	pít	k5eAaImAgMnP	pít
místo	místo	k7c2	místo
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
ambrosii	ambrosia	k1gFnSc4	ambrosia
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc4	jídlo
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
dává	dávat	k5eAaImIp3nS	dávat
mládí	mládí	k1gNnSc4	mládí
<g/>
,	,	kIx,	,
krásu	krása	k1gFnSc4	krása
i	i	k8xC	i
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
balzám	balzám	k1gInSc1	balzám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Zeus	Zeus	k1gInSc1	Zeus
unesl	unést	k5eAaPmAgInS	unést
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
nechal	nechat	k5eAaPmAgMnS	nechat
unést	unést	k5eAaPmF	unést
<g/>
)	)	kIx)	)
později	pozdě	k6eAd2	pozdě
Ganyméda	Ganyméd	k1gMnSc4	Ganyméd
a	a	k8xC	a
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
ho	on	k3xPp3gMnSc4	on
novým	nový	k2eAgMnSc7d1	nový
číšníkem	číšník	k1gMnSc7	číšník
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
Hébé	Hébé	k1gFnSc3	Hébé
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
provázeno	provázet	k5eAaImNgNnS	provázet
značnou	značný	k2eAgFnSc7d1	značná
nelibostí	nelibost	k1gFnSc7	nelibost
bohyně	bohyně	k1gFnSc2	bohyně
Héry	Héra	k1gFnSc2	Héra
<g/>
.	.	kIx.	.
</s>
<s>
Hébé	Hébé	k1gFnSc1	Hébé
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
hrdinu	hrdina	k1gMnSc4	hrdina
Hérakla	Hérakles	k1gMnSc4	Hérakles
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
stal	stát	k5eAaPmAgMnS	stát
bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
jménem	jméno	k1gNnSc7	jméno
Alexiares	Alexiaresa	k1gFnPc2	Alexiaresa
a	a	k8xC	a
Aníkétos	Aníkétosa	k1gFnPc2	Aníkétosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Podoba	podoba	k1gFnSc1	podoba
Hébé	Hébé	k1gFnSc1	Hébé
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
reliéfech	reliéf	k1gInPc6	reliéf
a	a	k8xC	a
vázových	vázový	k2eAgFnPc6d1	vázový
malbách	malba	k1gFnPc6	malba
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Hérakla	Hérakles	k1gMnSc2	Hérakles
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
z	z	k7c2	z
novodobých	novodobý	k2eAgNnPc2d1	novodobé
děl	dělo	k1gNnPc2	dělo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
A.	A.	kA	A.
Pajou	Pajá	k1gFnSc4	Pajá
<g/>
,	,	kIx,	,
dvorní	dvorní	k2eAgMnSc1d1	dvorní
sochař	sochař	k1gMnSc1	sochař
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
hlavu	hlava	k1gFnSc4	hlava
Mme	Mme	k1gFnSc2	Mme
du	du	k?	du
Barry	Barra	k1gFnSc2	Barra
</s>
</p>
<p>
<s>
socha	socha	k1gFnSc1	socha
A.	A.	kA	A.
Canova	Canův	k2eAgFnSc1d1	Canova
(	(	kIx(	(
<g/>
asi	asi	k9	asi
r.	r.	kA	r.
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnPc1	antika
</s>
</p>
<p>
<s>
Publius	Publius	k1gMnSc1	Publius
Ovidius	Ovidius	k1gMnSc1	Ovidius
Naso	Naso	k1gMnSc1	Naso
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
,	,	kIx,	,
Starověké	starověký	k2eAgFnPc1d1	starověká
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hébé	Hébé	k1gFnSc2	Hébé
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Hébé	Hébé	k1gFnSc2	Hébé
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
