<p>
<s>
Kvartet	kvartet	k1gInSc1	kvartet
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
hudební	hudební	k2eAgNnSc4d1	hudební
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgMnPc4	čtyři
muzikanty	muzikant	k1gMnPc4	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
formou	forma	k1gFnSc7	forma
kvartetu	kvartet	k1gInSc2	kvartet
je	být	k5eAaImIp3nS	být
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
skladba	skladba	k1gFnSc1	skladba
napsaná	napsaný	k2eAgFnSc1d1	napsaná
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Smyčcové	smyčcový	k2eAgInPc1d1	smyčcový
kvartety	kvartet	k1gInPc1	kvartet
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
komorních	komorní	k2eAgFnPc2d1	komorní
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
jako	jako	k9	jako
takovým	takový	k3xDgNnSc7	takový
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
věnuje	věnovat	k5eAaImIp3nS	věnovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
klavírní	klavírní	k2eAgNnSc1d1	klavírní
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
a	a	k8xC	a
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Kvarteto	kvarteto	k1gNnSc1	kvarteto
fléten	flétna	k1gFnPc2	flétna
nebo	nebo	k8xC	nebo
lesních	lesní	k2eAgInPc2d1	lesní
rohů	roh	k1gInPc2	roh
<g/>
;	;	kIx,	;
v	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
například	například	k6eAd1	například
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
vibrafon	vibrafon	k1gInSc4	vibrafon
<g/>
,	,	kIx,	,
kontrabas	kontrabas	k1gInSc4	kontrabas
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejné	stejný	k2eAgNnSc4d1	stejné
označení	označení	k1gNnSc4	označení
kvartet	kvartet	k1gInSc4	kvartet
pak	pak	k6eAd1	pak
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
hudební	hudební	k2eAgFnSc1d1	hudební
interpretace	interpretace	k1gFnSc1	interpretace
čtyř	čtyři	k4xCgMnPc2	čtyři
hudebníků	hudebník	k1gMnPc2	hudebník
nebo	nebo	k8xC	nebo
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k8xC	i
názvu	název	k1gInSc2	název
takovéto	takovýto	k3xDgFnSc2	takovýto
skupiny	skupina	k1gFnSc2	skupina
-	-	kIx~	-
např.	např.	kA	např.
Inkognito	inkognito	k1gNnSc1	inkognito
kvartet	kvartet	k1gInSc1	kvartet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známá	známý	k2eAgNnPc4d1	známé
pěvecká	pěvecký	k2eAgNnPc4d1	pěvecké
kvarteta	kvarteto	k1gNnPc4	kvarteto
==	==	k?	==
</s>
</p>
<p>
<s>
4TET	[number]	k4	4TET
</s>
</p>
<p>
<s>
Settleři	Settler	k1gMnPc1	Settler
</s>
</p>
<p>
<s>
Yellow	Yellow	k?	Yellow
Sisters	Sisters	k1gInSc1	Sisters
</s>
</p>
<p>
<s>
Inkognito	inkognito	k6eAd1	inkognito
kvartet	kvartet	k1gInSc1	kvartet
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
interpretace	interpretace	k1gFnSc1	interpretace
</s>
</p>
<p>
<s>
Smyčcové	smyčcový	k2eAgNnSc1d1	smyčcové
kvarteto	kvarteto	k1gNnSc1	kvarteto
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
smyčcových	smyčcový	k2eAgNnPc2d1	smyčcové
kvartet	kvarteto	k1gNnPc2	kvarteto
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kvartet	kvarteto	k1gNnPc2	kvarteto
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
