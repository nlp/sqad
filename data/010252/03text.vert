<p>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
Im	Im	k1gFnSc7	Im
Westen	Westen	k2eAgInSc4d1	Westen
nichts	nichts	k1gInSc4	nichts
Neues	Neues	k1gInSc1	Neues
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
Ericha	Erich	k1gMnSc4	Erich
Marii	Maria	k1gFnSc4	Maria
Remarqua	Remarqua	k1gFnSc1	Remarqua
<g/>
,	,	kIx,	,
německého	německý	k2eAgMnSc2d1	německý
veterána	veterán	k1gMnSc2	veterán
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
do	do	k7c2	do
realistického	realistický	k2eAgInSc2d1	realistický
proudu	proud	k1gInSc2	proud
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
krutou	krutý	k2eAgFnSc4d1	krutá
realitu	realita	k1gFnSc4	realita
války	válka	k1gFnSc2	válka
a	a	k8xC	a
hluboké	hluboký	k2eAgNnSc1d1	hluboké
odtržení	odtržení	k1gNnSc1	odtržení
od	od	k7c2	od
civilního	civilní	k2eAgInSc2d1	civilní
života	život	k1gInSc2	život
mladých	mladý	k2eAgMnPc2d1	mladý
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
vracejících	vracející	k2eAgMnPc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc4	jejich
mládí	mládí	k1gNnSc4	mládí
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
chuť	chuť	k1gFnSc4	chuť
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
podrobně	podrobně	k6eAd1	podrobně
a	a	k8xC	a
výstižně	výstižně	k6eAd1	výstižně
představuje	představovat	k5eAaImIp3nS	představovat
válečnou	válečný	k2eAgFnSc4d1	válečná
realitu	realita	k1gFnSc4	realita
takřka	takřka	k6eAd1	takřka
s	s	k7c7	s
novinářskou	novinářský	k2eAgFnSc7d1	novinářská
preciozitou	preciozita	k1gFnSc7	preciozita
<g/>
.	.	kIx.	.
</s>
<s>
Neukazuje	ukazovat	k5eNaImIp3nS	ukazovat
heroický	heroický	k2eAgInSc4d1	heroický
boj	boj	k1gInSc4	boj
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
se	se	k3xPyFc4	se
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
nesmyslnost	nesmyslnost	k1gFnSc4	nesmyslnost
celého	celý	k2eAgInSc2d1	celý
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
tragédii	tragédie	k1gFnSc4	tragédie
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
prvním	první	k4xOgMnSc6	první
povoláním	povolání	k1gNnSc7	povolání
bylo	být	k5eAaImAgNnS	být
vyrábění	vyrábění	k1gNnSc1	vyrábění
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
hromady	hromada	k1gFnSc2	hromada
vojáků	voják	k1gMnPc2	voják
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
velikosti	velikost	k1gFnSc2	velikost
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
stadionu	stadion	k1gInSc2	stadion
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
dobyto	dobyt	k2eAgNnSc1d1	dobyto
<g/>
.	.	kIx.	.
</s>
<s>
Remarque	Remarque	k6eAd1	Remarque
též	též	k9	též
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
životu	život	k1gInSc2	život
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
vojáků	voják	k1gMnPc2	voják
rozhodovali	rozhodovat	k5eAaImAgMnP	rozhodovat
velitelé	velitel	k1gMnPc1	velitel
pohodlně	pohodlně	k6eAd1	pohodlně
usazení	usazený	k2eAgMnPc1d1	usazený
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
přední	přední	k2eAgFnSc2d1	přední
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgNnPc1	jaký
zvěrstva	zvěrstvo	k1gNnPc1	zvěrstvo
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
dějí	dít	k5eAaBmIp3nP	dít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
s	s	k7c7	s
občasnými	občasný	k2eAgInPc7d1	občasný
lyrickými	lyrický	k2eAgInPc7d1	lyrický
prvky	prvek	k1gInPc7	prvek
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
převážně	převážně	k6eAd1	převážně
spisovným	spisovný	k2eAgInSc7d1	spisovný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
autor	autor	k1gMnSc1	autor
přidal	přidat	k5eAaPmAgMnS	přidat
autenticitě	autenticita	k1gFnSc3	autenticita
Bäumerovu	Bäumerův	k2eAgFnSc4d1	Bäumerův
vyprávění	vyprávění	k1gNnSc3	vyprávění
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
i	i	k9	i
nespisovná	spisovný	k2eNgFnSc1d1	nespisovná
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
německé	německý	k2eAgNnSc1d1	německé
vydání	vydání	k1gNnSc1	vydání
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
Im	Im	k1gFnSc7	Im
Westen	Westen	k2eAgMnSc1d1	Westen
nichts	nichts	k6eAd1	nichts
Neues	Neues	k1gInSc1	Neues
a	a	k8xC	a
objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
milion	milion	k4xCgInSc4	milion
exemplářů	exemplář	k1gInPc2	exemplář
jenom	jenom	k6eAd1	jenom
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
milion	milion	k4xCgInSc1	milion
v	v	k7c4	v
zahraničí	zahraničí	k1gNnSc4	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Melantrich	Melantricha	k1gFnPc2	Melantricha
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Bohumila	Bohumil	k1gMnSc2	Bohumil
Mathesia	Mathesius	k1gMnSc2	Mathesius
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
překlad	překlad	k1gInSc1	překlad
učinil	učinit	k5eAaImAgInS	učinit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
František	František	k1gMnSc1	František
Gel	gel	k1gInSc1	gel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
knihy	kniha	k1gFnSc2	kniha
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
natočen	natočen	k2eAgInSc4d1	natočen
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Režíroval	režírovat	k5eAaImAgMnS	režírovat
jej	on	k3xPp3gInSc4	on
Lewis	Lewis	k1gInSc4	Lewis
Milestone	Mileston	k1gInSc5	Mileston
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
byl	být	k5eAaImAgInS	být
román	román	k1gInSc1	román
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Delbert	Delbert	k1gMnSc1	Delbert
Mann	Mann	k1gMnSc1	Mann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběhy	příběh	k1gInPc4	příběh
německého	německý	k2eAgMnSc2d1	německý
vojáka	voják	k1gMnSc2	voják
Paula	Paul	k1gMnSc2	Paul
Bäumera	Bäumer	k1gMnSc2	Bäumer
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kamarády	kamarád	k1gMnPc7	kamarád
Müllerem	Müller	k1gMnSc7	Müller
<g/>
,	,	kIx,	,
Kroppem	Kropp	k1gMnSc7	Kropp
<g/>
,	,	kIx,	,
Kemmerichem	Kemmerich	k1gMnSc7	Kemmerich
a	a	k8xC	a
Leerem	Leer	k1gMnSc7	Leer
je	být	k5eAaImIp3nS	být
Paul	Paul	k1gMnSc1	Paul
silným	silný	k2eAgInSc7d1	silný
psychickým	psychický	k2eAgInSc7d1	psychický
nátlakem	nátlak	k1gInSc7	nátlak
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
třídního	třídní	k2eAgMnSc2d1	třídní
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
donucen	donucen	k2eAgInSc4d1	donucen
ukončit	ukončit	k5eAaPmF	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
a	a	k8xC	a
dobrovolně	dobrovolně	k6eAd1	dobrovolně
se	se	k3xPyFc4	se
přihlásit	přihlásit	k5eAaPmF	přihlásit
k	k	k7c3	k
výcviku	výcvik	k1gInSc3	výcvik
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
brzy	brzy	k6eAd1	brzy
odvelen	odvelet	k5eAaPmNgInS	odvelet
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
popisuje	popisovat	k5eAaImIp3nS	popisovat
vše	všechen	k3xTgNnSc4	všechen
od	od	k7c2	od
výcviku	výcvik	k1gInSc2	výcvik
plného	plný	k2eAgInSc2d1	plný
šikany	šikana	k1gFnSc2	šikana
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
život	život	k1gInSc4	život
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
jeho	jeho	k3xOp3gNnSc2	jeho
nadšení	nadšení	k1gNnSc2	nadšení
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
opadá	opadat	k5eAaBmIp3nS	opadat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
kamarádi	kamarád	k1gMnPc1	kamarád
jeden	jeden	k4xCgInSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
umírají	umírat	k5eAaImIp3nP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
scény	scéna	k1gFnPc1	scéna
jsou	být	k5eAaImIp3nP	být
líčeny	líčit	k5eAaImNgFnP	líčit
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
obyčejného	obyčejný	k2eAgMnSc2d1	obyčejný
bezvýznamného	bezvýznamný	k2eAgMnSc2d1	bezvýznamný
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
nejmenších	malý	k2eAgInPc2d3	nejmenší
detailů	detail	k1gInPc2	detail
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
naturalisticky	naturalisticky	k6eAd1	naturalisticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
příběhu	příběh	k1gInSc2	příběh
mu	on	k3xPp3gMnSc3	on
umírá	umírat	k5eAaImIp3nS	umírat
jeho	jeho	k3xOp3gMnSc7	jeho
přítel	přítel	k1gMnSc1	přítel
Kemmerich	Kemmerich	k1gMnSc1	Kemmerich
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
zná	znát	k5eAaImIp3nS	znát
už	už	k6eAd1	už
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
takových	takový	k3xDgFnPc6	takový
situacích	situace	k1gFnPc6	situace
se	se	k3xPyFc4	se
Paul	Paul	k1gMnSc1	Paul
zabírá	zabírat	k5eAaImIp3nS	zabírat
do	do	k7c2	do
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
staré	starý	k2eAgInPc4d1	starý
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
zas	zas	k6eAd1	zas
povolán	povolat	k5eAaPmNgInS	povolat
do	do	k7c2	do
zákopů	zákop	k1gInPc2	zákop
a	a	k8xC	a
líčí	líčit	k5eAaImIp3nS	líčit
průběh	průběh	k1gInSc4	průběh
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgInPc4	svůj
pocity	pocit	k1gInPc4	pocit
a	a	k8xC	a
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
tam	tam	k6eAd1	tam
zažil	zažít	k5eAaPmAgInS	zažít
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
ale	ale	k8xC	ale
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
propustku	propustek	k1gInSc2	propustek
a	a	k8xC	a
jede	jet	k5eAaImIp3nS	jet
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
maminka	maminka	k1gFnSc1	maminka
má	mít	k5eAaImIp3nS	mít
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
registruje	registrovat	k5eAaBmIp3nS	registrovat
úplné	úplný	k2eAgNnSc4d1	úplné
odtržení	odtržení	k1gNnSc4	odtržení
od	od	k7c2	od
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
a	a	k8xC	a
psychické	psychický	k2eAgInPc4d1	psychický
důsledky	důsledek	k1gInPc4	důsledek
války	válka	k1gFnSc2	válka
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
zcela	zcela	k6eAd1	zcela
deformován	deformovat	k5eAaImNgInS	deformovat
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgMnS	změnit
i	i	k9	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
už	už	k6eAd1	už
bere	brát	k5eAaImIp3nS	brát
jako	jako	k9	jako
samozřejmost	samozřejmost	k1gFnSc1	samozřejmost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
už	už	k6eAd1	už
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
nebaví	bavit	k5eNaImIp3nS	bavit
ho	on	k3xPp3gInSc4	on
jeho	on	k3xPp3gInSc4	on
dřívější	dřívější	k2eAgInSc4d1	dřívější
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
psychicky	psychicky	k6eAd1	psychicky
hroutit	hroutit	k5eAaImF	hroutit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
rota	rota	k1gFnSc1	rota
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyklidit	vyklidit	k5eAaPmF	vyklidit
jednu	jeden	k4xCgFnSc4	jeden
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
postřelili	postřelit	k5eAaPmAgMnP	postřelit
jeho	jeho	k3xOp3gNnSc1	jeho
i	i	k9	i
Kroppa	Kroppa	k1gFnSc1	Kroppa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zase	zase	k9	zase
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Kroppovi	Kroppův	k2eAgMnPc1d1	Kroppův
amputují	amputovat	k5eAaBmIp3nP	amputovat
nohu	noha	k1gFnSc4	noha
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
zase	zase	k9	zase
povolán	povolán	k2eAgInSc4d1	povolán
do	do	k7c2	do
zákopů	zákop	k1gInPc2	zákop
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
posledním	poslední	k2eAgMnSc7d1	poslední
žijícím	žijící	k2eAgMnSc7d1	žijící
kamarádem	kamarád	k1gMnSc7	kamarád
Katczinskym	Katczinskymum	k1gNnPc2	Katczinskymum
(	(	kIx(	(
<g/>
čtyřicetiletý	čtyřicetiletý	k2eAgInSc1d1	čtyřicetiletý
Polák	polák	k1gInSc1	polák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
knihu	kniha	k1gFnSc4	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
ale	ale	k9	ale
postřelí	postřelit	k5eAaPmIp3nS	postřelit
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
ho	on	k3xPp3gNnSc4	on
nese	nést	k5eAaImIp3nS	nést
přes	přes	k7c4	přes
celé	celý	k2eAgNnSc4d1	celé
bojiště	bojiště	k1gNnSc4	bojiště
k	k	k7c3	k
polní	polní	k2eAgFnSc3d1	polní
nemocnici	nemocnice	k1gFnSc3	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Katczinsky	Katczinsky	k6eAd1	Katczinsky
nakonec	nakonec	k6eAd1	nakonec
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dostal	dostat	k5eAaPmAgMnS	dostat
střepinou	střepina	k1gFnSc7	střepina
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Paula	Paula	k1gFnSc1	Paula
smrt	smrt	k1gFnSc4	smrt
Katczinského	Katczinský	k2eAgInSc2d1	Katczinský
velice	velice	k6eAd1	velice
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
dostává	dostávat	k5eAaImIp3nS	dostávat
14	[number]	k4	14
dní	den	k1gInPc2	den
volna	volno	k1gNnSc2	volno
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
normálního	normální	k2eAgInSc2d1	normální
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
končí	končit	k5eAaImIp3nS	končit
Paulovou	Paulův	k2eAgFnSc7d1	Paulova
smrtí	smrt	k1gFnSc7	smrt
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Padl	padnout	k5eAaImAgInS	padnout
v	v	k7c6	v
září	září	k1gNnSc6	září
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
bojišti	bojiště	k1gNnSc6	bojiště
tak	tak	k6eAd1	tak
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zpráva	zpráva	k1gFnSc1	zpráva
vrchního	vrchní	k2eAgNnSc2d1	vrchní
velitelství	velitelství	k1gNnSc2	velitelství
omezila	omezit	k5eAaPmAgFnS	omezit
na	na	k7c4	na
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
byl	být	k5eAaImAgInS	být
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
Citováno	citován	k2eAgNnSc4d1	citováno
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
překladu	překlad	k1gInSc2	překlad
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Bäumer	Bäumer	k1gMnSc1	Bäumer
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
19	[number]	k4	19
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
donucen	donucen	k2eAgInSc1d1	donucen
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zažívá	zažívat	k5eAaImIp3nS	zažívat
děsivé	děsivý	k2eAgInPc4d1	děsivý
důsledky	důsledek	k1gInPc4	důsledek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgMnS	být
Paul	Paul	k1gMnSc1	Paul
kreativní	kreativní	k2eAgMnSc1d1	kreativní
<g/>
,	,	kIx,	,
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
a	a	k8xC	a
citlivý	citlivý	k2eAgMnSc1d1	citlivý
mladík	mladík	k1gMnSc1	mladík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
psával	psávat	k5eAaImAgMnS	psávat
poemy	poema	k1gFnSc2	poema
a	a	k8xC	a
upřímně	upřímně	k6eAd1	upřímně
miloval	milovat	k5eAaImAgMnS	milovat
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
válka	válka	k1gFnSc1	válka
mění	měnit	k5eAaImIp3nS	měnit
jeho	jeho	k3xOp3gFnSc1	jeho
osobnost	osobnost	k1gFnSc1	osobnost
a	a	k8xC	a
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hrůza	hrůza	k6eAd1	hrůza
a	a	k8xC	a
děs	děs	k1gInSc4	děs
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dennodenně	dennodenně	k6eAd1	dennodenně
doléhají	doléhat	k5eAaImIp3nP	doléhat
<g/>
,	,	kIx,	,
ho	on	k3xPp3gNnSc4	on
postupně	postupně	k6eAd1	postupně
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
lidskosti	lidskost	k1gFnSc2	lidskost
a	a	k8xC	a
tradičních	tradiční	k2eAgFnPc2d1	tradiční
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
cynik	cynik	k1gMnSc1	cynik
<g/>
.	.	kIx.	.
</s>
<s>
Cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
nikomu	nikdo	k3yNnSc3	nikdo
říct	říct	k5eAaPmF	říct
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
citech	cit	k1gInPc6	cit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přichází	přicházet	k5eAaImIp3nS	přicházet
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
si	se	k3xPyFc3	se
připadá	připadat	k5eAaImIp3nS	připadat
jako	jako	k9	jako
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělal	udělat	k5eAaPmAgMnS	udělat
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
umíráním	umírání	k1gNnSc7	umírání
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k9	už
po	po	k7c6	po
ničem	nic	k3yNnSc6	nic
netouží	toužit	k5eNaImIp3nS	toužit
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
co	co	k3yInSc4	co
ztratit	ztratit	k5eAaPmF	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
ho	on	k3xPp3gMnSc4	on
zbavila	zbavit	k5eAaPmAgFnS	zbavit
veškerých	veškerý	k3xTgInPc2	veškerý
snů	sen	k1gInPc2	sen
a	a	k8xC	a
nadějí	naděje	k1gFnPc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
Paul	Paul	k1gMnSc1	Paul
Bäumer	Bäumer	k1gMnSc1	Bäumer
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
zabit	zabit	k2eAgInSc1d1	zabit
během	během	k7c2	během
výjimečně	výjimečně	k6eAd1	výjimečně
klidného	klidný	k2eAgInSc2d1	klidný
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Velitelova	velitelův	k2eAgFnSc1d1	velitelova
zpráva	zpráva	k1gFnSc1	zpráva
toho	ten	k3xDgNnSc2	ten
data	datum	k1gNnSc2	datum
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
větu	věta	k1gFnSc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Paul	Paul	k1gMnSc1	Paul
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
obličej	obličej	k1gInSc4	obličej
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
klidný	klidný	k2eAgMnSc1d1	klidný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
dlouho	dlouho	k6eAd1	dlouho
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
konec	konec	k1gInSc4	konec
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
už	už	k6eAd1	už
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Kropp	Kropp	k1gMnSc1	Kropp
–	–	k?	–
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spolužák	spolužák	k1gMnSc1	spolužák
Paula	Paula	k1gFnSc1	Paula
B.	B.	kA	B.
Je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
jako	jako	k9	jako
nejchytřejší	chytrý	k2eAgMnSc1d3	nejchytřejší
a	a	k8xC	a
nejmenší	malý	k2eAgMnSc1d3	nejmenší
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Paulových	Paulových	k2eAgMnPc2d1	Paulových
spolužáků	spolužák	k1gMnPc2	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
Kropp	Kropp	k1gMnSc1	Kropp
raněn	ranit	k5eAaPmNgMnS	ranit
a	a	k8xC	a
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
amputaci	amputace	k1gFnSc4	amputace
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
se	s	k7c7	s
zraněnou	zraněný	k2eAgFnSc7d1	zraněná
rukou	ruka	k1gFnSc7	ruka
a	a	k8xC	a
nohou	noha	k1gFnSc7	noha
nakonec	nakonec	k6eAd1	nakonec
spolu	spolu	k6eAd1	spolu
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
chtěl	chtít	k5eAaImAgMnS	chtít
Kropp	Kropp	k1gInSc4	Kropp
původně	původně	k6eAd1	původně
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
odejmuta	odejmut	k2eAgFnSc1d1	odejmuta
noha	noha	k1gFnSc1	noha
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
silnému	silný	k2eAgInSc3d1	silný
přátelství	přátelství	k1gNnSc2	přátelství
ji	on	k3xPp3gFnSc4	on
nakonec	nakonec	k6eAd1	nakonec
odkládá	odkládat	k5eAaImIp3nS	odkládat
<g/>
.	.	kIx.	.
</s>
<s>
Bäumer	Bäumer	k1gInSc1	Bäumer
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
čase	čas	k1gInSc6	čas
opět	opět	k6eAd1	opět
odvolán	odvolat	k5eAaPmNgMnS	odvolat
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
těžké	těžký	k2eAgNnSc1d1	těžké
loučení	loučení	k1gNnSc1	loučení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Heie	Heie	k6eAd1	Heie
Westhus	Westhus	k1gInSc1	Westhus
–	–	k?	–
vysoký	vysoký	k2eAgInSc1d1	vysoký
a	a	k8xC	a
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
povoláním	povolání	k1gNnSc7	povolání
kopáč	kopáč	k1gMnSc1	kopáč
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
vypadá	vypadat	k5eAaImIp3nS	vypadat
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
stejně	stejně	k6eAd1	stejně
starý	starý	k2eAgMnSc1d1	starý
kamarád	kamarád	k1gMnSc1	kamarád
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
Paul	Paul	k1gMnSc1	Paul
Bäumer	Bäumer	k1gMnSc1	Bäumer
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
smrtelně	smrtelně	k6eAd1	smrtelně
zasažen	zasažen	k2eAgMnSc1d1	zasažen
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
Himmelstoss	Himmelstoss	k1gInSc1	Himmelstoss
odtahuje	odtahovat	k5eAaImIp3nS	odtahovat
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
,	,	kIx,	,
Bäumer	Bäumer	k1gMnSc1	Bäumer
vidí	vidět	k5eAaImIp3nS	vidět
jeho	jeho	k3xOp3gFnPc4	jeho
roztahující	roztahující	k2eAgFnPc4d1	roztahující
a	a	k8xC	a
smršťující	smršťující	k2eAgFnPc4d1	smršťující
se	se	k3xPyFc4	se
plíce	plíce	k1gFnPc4	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fredrich	Fredrich	k1gMnSc1	Fredrich
Müller	Müller	k1gMnSc1	Müller
–	–	k?	–
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spolužák	spolužák	k1gMnSc1	spolužák
Paula	Paula	k1gFnSc1	Paula
B.	B.	kA	B.
18,5	[number]	k4	18,5
<g/>
letý	letý	k2eAgInSc1d1	letý
mladík	mladík	k1gInSc1	mladík
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
jako	jako	k9	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nosí	nosit	k5eAaImIp3nS	nosit
své	svůj	k3xOyFgFnPc4	svůj
staré	starý	k2eAgFnPc4d1	stará
školní	školní	k2eAgFnPc4d1	školní
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
stále	stále	k6eAd1	stále
připomíná	připomínat	k5eAaImIp3nS	připomínat
důležitost	důležitost	k1gFnSc4	důležitost
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
i	i	k9	i
pod	pod	k7c7	pod
palbou	palba	k1gFnSc7	palba
nepřítele	nepřítel	k1gMnSc2	nepřítel
opakuje	opakovat	k5eAaImIp3nS	opakovat
fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
dobré	dobrý	k2eAgFnPc4d1	dobrá
boty	bota	k1gFnPc4	bota
Kemmrichovy	Kemmrichův	k2eAgFnPc4d1	Kemmrichův
a	a	k8xC	a
dědí	dědit	k5eAaImIp3nP	dědit
je	on	k3xPp3gFnPc4	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
románu	román	k1gInSc2	román
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
fatálně	fatálně	k6eAd1	fatálně
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
světlicí	světlice	k1gFnSc7	světlice
do	do	k7c2	do
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnSc2	svůj
boty	bota	k1gFnSc2	bota
předává	předávat	k5eAaImIp3nS	předávat
Paulu	Paul	k1gMnSc3	Paul
Bäumerovi	Bäumer	k1gMnSc3	Bäumer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanisław	Stanisław	k?	Stanisław
"	"	kIx"	"
<g/>
Katcza	Katcza	k1gFnSc1	Katcza
<g/>
"	"	kIx"	"
Katczyński	Katczyński	k1gNnSc1	Katczyński
–	–	k?	–
hlava	hlava	k1gFnSc1	hlava
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
kamarád	kamarád	k1gMnSc1	kamarád
Paula	Paul	k1gMnSc2	Paul
B.	B.	kA	B.
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
o	o	k7c4	o
21	[number]	k4	21
let	léto	k1gNnPc2	léto
mladšího	mladý	k2eAgMnSc2d2	mladší
Paula	Paul	k1gMnSc2	Paul
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
kolegy	kolega	k1gMnSc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
životě	život	k1gInSc6	život
byl	být	k5eAaImAgMnS	být
švec	švec	k1gMnSc1	švec
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kontrast	kontrast	k1gInSc4	kontrast
mezi	mezi	k7c7	mezi
světem	svět	k1gInSc7	svět
mladých	mladý	k2eAgMnPc2d1	mladý
a	a	k8xC	a
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
měli	mít	k5eAaImAgMnP	mít
více	hodně	k6eAd2	hodně
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
již	již	k6eAd1	již
nějaké	nějaký	k3yIgFnPc4	nějaký
profesní	profesní	k2eAgFnPc4d1	profesní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Paula	Paul	k1gMnSc4	Paul
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
kamarády	kamarád	k1gMnPc4	kamarád
je	být	k5eAaImIp3nS	být
,,	,,	k?	,,
<g/>
tvorba	tvorba	k1gFnSc1	tvorba
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
"	"	kIx"	"
první	první	k4xOgNnPc4	první
povolání	povolání	k1gNnPc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Katcza	Katcza	k1gFnSc1	Katcza
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
schopnost	schopnost	k1gFnSc4	schopnost
získat	získat	k5eAaPmF	získat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
potřebné	potřebný	k2eAgFnPc4d1	potřebná
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
šrapnelem	šrapnel	k1gInSc7	šrapnel
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
ho	on	k3xPp3gNnSc4	on
táhne	táhnout	k5eAaImIp3nS	táhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
střepinou	střepina	k1gFnSc7	střepina
do	do	k7c2	do
týlu	týl	k1gInSc2	týl
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
toho	ten	k3xDgMnSc4	ten
Bäumer	Bäumer	k1gMnSc1	Bäumer
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
ho	on	k3xPp3gMnSc4	on
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
až	až	k9	až
vojenský	vojenský	k2eAgMnSc1d1	vojenský
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Katczova	Katczův	k2eAgFnSc1d1	Katczův
smrt	smrt	k1gFnSc1	smrt
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Paulovi	Paul	k1gMnSc3	Paul
přestalo	přestat	k5eAaPmAgNnS	přestat
záležet	záležet	k5eAaImF	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
či	či	k8xC	či
bude	být	k5eAaImBp3nS	být
žít	žít	k5eAaImF	žít
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dokázal	dokázat	k5eAaPmAgMnS	dokázat
žít	žít	k5eAaImF	žít
beze	beze	k7c2	beze
strachu	strach	k1gInSc2	strach
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tjaden	Tjadno	k1gNnPc2	Tjadno
–	–	k?	–
největší	veliký	k2eAgMnSc1d3	veliký
jedlík	jedlík	k1gMnSc1	jedlík
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Paulův	Paulův	k2eAgMnSc1d1	Paulův
mimoškolní	mimoškolní	k2eAgMnSc1d1	mimoškolní
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
byl	být	k5eAaImAgInS	být
Tjaden	Tjadno	k1gNnPc2	Tjadno
zámečníkem	zámečník	k1gMnSc7	zámečník
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
krutému	krutý	k2eAgInSc3d1	krutý
výcviku	výcvik	k1gInSc3	výcvik
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
duše	duše	k1gFnSc2	duše
nesnáší	snášet	k5eNaImIp3nS	snášet
Himmelstosse	Himmelstosse	k1gFnSc1	Himmelstosse
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
mu	on	k3xPp3gMnSc3	on
odpouští	odpouštět	k5eAaImIp3nS	odpouštět
<g/>
.	.	kIx.	.
</s>
<s>
Bäumer	Bäumer	k1gInSc1	Bäumer
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
sní	sníst	k5eAaPmIp3nS	sníst
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yInSc4	co
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stále	stále	k6eAd1	stále
udržuje	udržovat	k5eAaImIp3nS	udržovat
jeho	jeho	k3xOp3gFnSc1	jeho
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
osudy	osud	k1gInPc1	osud
Tjadena	Tjadena	k1gFnSc1	Tjadena
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kantorek	kantorka	k1gFnPc2	kantorka
–	–	k?	–
tělocvikář	tělocvikář	k1gMnSc1	tělocvikář
Paula	Paul	k1gMnSc2	Paul
<g/>
,	,	kIx,	,
Kroppa	Kropp	k1gMnSc2	Kropp
<g/>
,	,	kIx,	,
Leera	Leer	k1gMnSc2	Leer
<g/>
,	,	kIx,	,
Müllera	Müller	k1gMnSc2	Müller
a	a	k8xC	a
Böhma	Böhm	k1gMnSc2	Böhm
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
nad	nad	k7c4	nad
studenty	student	k1gMnPc4	student
si	se	k3xPyFc3	se
vyloženě	vyloženě	k6eAd1	vyloženě
užívá	užívat	k5eAaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
silného	silný	k2eAgMnSc4d1	silný
zastánce	zastánce	k1gMnSc4	zastánce
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
celou	celý	k2eAgFnSc4d1	celá
třídu	třída	k1gFnSc4	třída
přiměl	přimět	k5eAaPmAgMnS	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
narukovala	narukovat	k5eAaPmAgFnS	narukovat
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
mezi	mezi	k7c7	mezi
20	[number]	k4	20
studenty	student	k1gMnPc7	student
jediný	jediný	k2eAgInSc4d1	jediný
Joseph	Joseph	k1gInSc4	Joseph
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
vzdoruje	vzdorovat	k5eAaImIp3nS	vzdorovat
<g/>
,	,	kIx,	,
umírá	umírat	k5eAaImIp3nS	umírat
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Kantorek	kantorka	k1gFnPc2	kantorka
sám	sám	k3xTgInSc4	sám
je	být	k5eAaImIp3nS	být
pokrytec	pokrytec	k1gMnSc1	pokrytec
<g/>
.	.	kIx.	.
</s>
<s>
Oháněje	ohánět	k5eAaImSgMnS	ohánět
se	se	k3xPyFc4	se
patriotismem	patriotismus	k1gInSc7	patriotismus
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
nevstupuje	vstupovat	k5eNaImIp3nS	vstupovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgInSc1	sám
povolán	povolán	k2eAgInSc1d1	povolán
a	a	k8xC	a
zažívá	zažívat	k5eAaImIp3nS	zažívat
během	během	k7c2	během
výcviku	výcvik	k1gInSc2	výcvik
dril	dril	k1gInSc4	dril
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
bývalých	bývalý	k2eAgMnPc2d1	bývalý
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
dřívějšími	dřívější	k2eAgFnPc7d1	dřívější
hodinami	hodina	k1gFnPc7	hodina
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Leer	Leer	k1gMnSc1	Leer
–	–	k?	–
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spolužák	spolužák	k1gMnSc1	spolužák
Paula	Paula	k1gFnSc1	Paula
B.	B.	kA	B.
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
inteligentního	inteligentní	k2eAgMnSc4d1	inteligentní
vojáka	voják	k1gMnSc4	voják
populárního	populární	k2eAgInSc2d1	populární
především	především	k9	především
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
spolužáci	spolužák	k1gMnPc1	spolužák
potkávají	potkávat	k5eAaImIp3nP	potkávat
tři	tři	k4xCgFnPc1	tři
francouzské	francouzský	k2eAgFnPc1d1	francouzská
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
on	on	k3xPp3gMnSc1	on
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
jednu	jeden	k4xCgFnSc4	jeden
svést	svést	k5eAaPmF	svést
<g/>
.	.	kIx.	.
</s>
<s>
Leer	Leer	k1gInSc1	Leer
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
stejným	stejný	k2eAgInSc7d1	stejný
šrapnelem	šrapnel	k1gInSc7	šrapnel
jako	jako	k8xC	jako
Bertinck	Bertinck	k1gInSc4	Bertinck
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
poté	poté	k6eAd1	poté
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
vykrvácení	vykrvácení	k1gNnSc4	vykrvácení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bertinck	Bertinck	k1gMnSc1	Bertinck
–	–	k?	–
velitel	velitel	k1gMnSc1	velitel
Baumerovy	Baumerův	k2eAgFnSc2d1	Baumerův
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
muži	muž	k1gMnSc3	muž
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
respekt	respekt	k1gInSc4	respekt
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
Umírá	umírat	k5eAaImIp3nS	umírat
stejným	stejný	k2eAgInSc7d1	stejný
šrapnelem	šrapnel	k1gInSc7	šrapnel
jako	jako	k8xC	jako
Leer	Leer	k1gInSc4	Leer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Kemmerich	Kemmerich	k1gMnSc1	Kemmerich
–	–	k?	–
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spolužák	spolužák	k1gMnSc1	spolužák
Paula	Paul	k1gMnSc2	Paul
B.	B.	kA	B.
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
-ti	i	k?	-ti
letý	letý	k2eAgMnSc1d1	letý
mladík	mladík	k1gMnSc1	mladík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
střelen	střelit	k5eAaPmNgMnS	střelit
do	do	k7c2	do
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
následně	následně	k6eAd1	následně
musejí	muset	k5eAaImIp3nP	muset
amputovat	amputovat	k5eAaBmF	amputovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
boty	bota	k1gFnPc1	bota
dědí	dědit	k5eAaImIp3nP	dědit
Müller	Müller	k1gInSc4	Müller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Hamacher	Hamachra	k1gFnPc2	Hamachra
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
nemocnici	nemocnice	k1gFnSc6	nemocnice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Kroppem	Kropp	k1gMnSc7	Kropp
a	a	k8xC	a
Paulem	Paul	k1gMnSc7	Paul
Baumerem	Baumer	k1gMnSc7	Baumer
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
potvrzení	potvrzení	k1gNnSc4	potvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
zranění	zranění	k1gNnSc4	zranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
zamane	zamanout	k5eAaPmIp3nS	zamanout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Detering	Detering	k1gInSc1	Detering
–	–	k?	–
sedlák	sedlák	k1gInSc1	sedlák
<g/>
,	,	kIx,	,
škrt	škrt	k1gInSc1	škrt
<g/>
,	,	kIx,	,
myslí	myslet	k5eAaImIp3nS	myslet
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
usedlost	usedlost	k1gFnSc4	usedlost
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nechápe	chápat	k5eNaImIp3nS	chápat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
používají	používat	k5eAaImIp3nP	používat
koně	kůň	k1gMnPc1	kůň
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
raněné	raněný	k2eAgFnPc1d1	raněná
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
ze	z	k7c2	z
zákopů	zákop	k1gInPc2	zákop
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
utíká	utíkat	k5eAaImIp3nS	utíkat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chycen	chytit	k5eAaPmNgInS	chytit
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
souzen	souzen	k2eAgInSc1d1	souzen
dle	dle	k7c2	dle
stanného	stanný	k2eAgNnSc2d1	stanné
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
poté	poté	k6eAd1	poté
už	už	k9	už
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
nikdo	nikdo	k3yNnSc1	nikdo
nikdy	nikdy	k6eAd1	nikdy
neslyšel	slyšet	k5eNaImAgMnS	slyšet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Himmelstoss	Himmelstoss	k1gInSc1	Himmelstoss
–	–	k?	–
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
,	,	kIx,	,
sadistický	sadistický	k2eAgInSc1d1	sadistický
a	a	k8xC	a
bezohledný	bezohledný	k2eAgMnSc1d1	bezohledný
velitel	velitel	k1gMnSc1	velitel
výcviku	výcvik	k1gInSc2	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
dává	dávat	k5eAaImIp3nS	dávat
Bäumerovi	Bäumer	k1gMnSc3	Bäumer
a	a	k8xC	a
ostatním	ostatní	k2eAgMnPc3d1	ostatní
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
pomstě	pomsta	k1gFnSc3	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
zbabělec	zbabělec	k1gMnSc1	zbabělec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
usmiřují	usmiřovat	k5eAaImIp3nP	usmiřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
All	All	k?	All
Quiet	Quiet	k1gInSc1	Quiet
on	on	k3xPp3gInSc1	on
the	the	k?	the
Western	western	k1gInSc1	western
Front	front	k1gInSc1	front
<g/>
)	)	kIx)	)
–	–	k?	–
americko-britský	americkoritský	k2eAgInSc1d1	americko-britský
válečný	válečný	k2eAgInSc1d1	válečný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Delberta	Delbert	k1gMnSc2	Delbert
Manna	Mann	k1gMnSc2	Mann
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
All	All	k?	All
Quiet	Quiet	k1gInSc1	Quiet
on	on	k3xPp3gInSc1	on
the	the	k?	the
Western	western	k1gInSc1	western
Front	front	k1gInSc1	front
<g/>
)	)	kIx)	)
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
válečný	válečný	k2eAgInSc4d1	válečný
film	film	k1gInSc4	film
Lewise	Lewise	k1gFnSc1	Lewise
Milestona	Milestona	k1gFnSc1	Milestona
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Erich	Erich	k1gMnSc1	Erich
Maria	Mario	k1gMnSc2	Mario
Remarque	Remarque	k1gNnSc2	Remarque
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rozbor	rozbor	k1gInSc1	rozbor
románu	román	k1gInSc2	román
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
</s>
</p>
