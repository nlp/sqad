<p>
<s>
Cuketa	cuketa	k1gFnSc1	cuketa
je	být	k5eAaImIp3nS	být
kultivar	kultivar	k1gInSc4	kultivar
jednoleté	jednoletý	k2eAgFnSc2d1	jednoletá
plodové	plodový	k2eAgFnSc2d1	plodová
zeleniny	zelenina	k1gFnSc2	zelenina
řadící	řadící	k2eAgFnSc1d1	řadící
se	se	k3xPyFc4	se
k	k	k7c3	k
druhu	druh	k1gInSc2	druh
tykev	tykev	k1gFnSc1	tykev
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Cucurbita	Cucurbita	k1gMnSc1	Cucurbita
pepo	pepo	k1gMnSc1	pepo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Cuketa	cuketa	k1gFnSc1	cuketa
je	být	k5eAaImIp3nS	být
však	však	k9	však
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
šlechtěním	šlechtění	k1gNnPc3	šlechtění
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Běžnou	běžný	k2eAgFnSc7d1	běžná
odrůdou	odrůda	k1gFnSc7	odrůda
cukety	cuketa	k1gFnSc2	cuketa
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Cucurbita	Cucurbita	k1gMnSc1	Cucurbita
pepo	pepo	k1gMnSc1	pepo
́	́	k?	́
<g/>
Nefertiti	Nefertiti	k1gMnSc1	Nefertiti
<g/>
́	́	k?	́
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnPc4	synonymum
a	a	k8xC	a
etymologie	etymologie	k1gFnPc4	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Cuketám	cuketa	k1gFnPc3	cuketa
se	se	k3xPyFc4	se
také	také	k6eAd1	také
říká	říkat	k5eAaImIp3nS	říkat
cukíny	cukína	k1gFnPc4	cukína
<g/>
,	,	kIx,	,
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
názvy	název	k1gInPc1	název
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
výrazu	výraz	k1gInSc6	výraz
pro	pro	k7c4	pro
tykev	tykev	k1gFnSc4	tykev
<g/>
:	:	kIx,	:
zucca	zucca	k6eAd1	zucca
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
používá	používat	k5eAaImIp3nS	používat
ženská	ženský	k2eAgFnSc1d1	ženská
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
,	,	kIx,	,
zucchine	zucchinout	k5eAaPmIp3nS	zucchinout
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
používají	používat	k5eAaImIp3nP	používat
jeho	jeho	k3xOp3gFnSc4	jeho
mužskou	mužský	k2eAgFnSc4d1	mužská
zdrobnělinu	zdrobnělina	k1gFnSc4	zdrobnělina
<g/>
,	,	kIx,	,
zucchini	zucchin	k1gMnPc1	zucchin
či	či	k8xC	či
zucchetti	zucchett	k1gMnPc1	zucchett
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Cuketa	cuketa	k1gFnSc1	cuketa
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
keřovitého	keřovitý	k2eAgInSc2d1	keřovitý
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
dřípené	dřípený	k2eAgInPc1d1	dřípený
listy	list	k1gInPc1	list
na	na	k7c6	na
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
řapících	řapík	k1gInPc6	řapík
jsou	být	k5eAaImIp3nP	být
tmavozelené	tmavozelený	k2eAgFnPc1d1	tmavozelená
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
bělavé	bělavý	k2eAgFnPc1d1	bělavá
skvrny	skvrna	k1gFnPc1	skvrna
ze	z	k7c2	z
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
pletiva	pletivo	k1gNnSc2	pletivo
<g/>
.	.	kIx.	.
<g/>
Květy	Květa	k1gFnPc1	Květa
cukety	cuketa	k1gFnSc2	cuketa
bývají	bývat	k5eAaImIp3nP	bývat
tmavě	tmavě	k6eAd1	tmavě
žluté	žlutý	k2eAgNnSc1d1	žluté
s	s	k7c7	s
pěti	pět	k4xCc7	pět
okvětními	okvětní	k2eAgInPc7d1	okvětní
lístky	lístek	k1gInPc7	lístek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plod	plod	k1gInSc1	plod
cukety	cuketa	k1gFnSc2	cuketa
mívá	mívat	k5eAaImIp3nS	mívat
většinou	většinou	k6eAd1	většinou
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
žlutozelenou	žlutozelený	k2eAgFnSc4d1	žlutozelená
či	či	k8xC	či
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
silnou	silný	k2eAgFnSc4d1	silná
okurku	okurka	k1gFnSc4	okurka
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
má	mít	k5eAaImIp3nS	mít
plně	plně	k6eAd1	plně
zralá	zralý	k2eAgFnSc1d1	zralá
cuketa	cuketa	k1gFnSc1	cuketa
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
lesklý	lesklý	k2eAgInSc4d1	lesklý
a	a	k8xC	a
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
<g/>
,	,	kIx,	,
dužina	dužina	k1gFnSc1	dužina
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
zelená	zelený	k2eAgFnSc1d1	zelená
či	či	k8xC	či
žlutá	žlutý	k2eAgFnSc1d1	žlutá
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
odrůdě	odrůda	k1gFnSc6	odrůda
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
vodnatá	vodnatý	k2eAgFnSc1d1	vodnatá
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
a	a	k8xC	a
sklizeň	sklizeň	k1gFnSc1	sklizeň
==	==	k?	==
</s>
</p>
<p>
<s>
Semenáčky	semenáček	k1gInPc1	semenáček
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
předpěstovat	předpěstovat	k5eAaPmF	předpěstovat
v	v	k7c6	v
kelímcích	kelímek	k1gInPc6	kelímek
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
semena	semeno	k1gNnPc1	semeno
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
rovnou	rovnou	k6eAd1	rovnou
do	do	k7c2	do
venkovní	venkovní	k2eAgFnSc2d1	venkovní
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hojnosti	hojnost	k1gFnSc6	hojnost
zálivky	zálivka	k1gFnSc2	zálivka
a	a	k8xC	a
hnojení	hnojení	k1gNnSc1	hnojení
rostliny	rostlina	k1gFnSc2	rostlina
rychle	rychle	k6eAd1	rychle
mohutní	mohutnět	k5eAaImIp3nS	mohutnět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
5-6	[number]	k4	5-6
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
samčí	samčí	k2eAgInPc1d1	samčí
květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
týdnu	týden	k1gInSc6	týden
samičí	samičí	k2eAgFnSc2d1	samičí
a	a	k8xC	a
první	první	k4xOgInPc4	první
plody	plod	k1gInPc4	plod
se	se	k3xPyFc4	se
tak	tak	k9	tak
dají	dát	k5eAaPmIp3nP	dát
sklízet	sklízet	k5eAaImF	sklízet
už	už	k9	už
za	za	k7c4	za
60-65	[number]	k4	60-65
dnů	den	k1gInPc2	den
od	od	k7c2	od
výsevu	výsev	k1gInSc2	výsev
<g/>
.	.	kIx.	.
<g/>
Rostliny	rostlina	k1gFnPc1	rostlina
netrpí	trpět	k5eNaImIp3nP	trpět
mnoha	mnoho	k4c7	mnoho
škůdci	škůdce	k1gMnPc7	škůdce
ani	ani	k8xC	ani
chorobami	choroba	k1gFnPc7	choroba
<g/>
,	,	kIx,	,
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
padlím	padlí	k1gNnSc7	padlí
okurkovým	okurkový	k2eAgNnSc7d1	okurkové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Cuketa	cuketa	k1gFnSc1	cuketa
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
spoustu	spousta	k1gFnSc4	spousta
způsobů	způsob	k1gInPc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
kuchyních	kuchyně	k1gFnPc6	kuchyně
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
rozšíření	rozšíření	k1gNnSc3	rozšíření
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
snadné	snadný	k2eAgNnSc1d1	snadné
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc1d1	rychlé
pěstování	pěstování	k1gNnSc1	pěstování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
i	i	k9	i
nepříliš	příliš	k6eNd1	příliš
zdatný	zdatný	k2eAgMnSc1d1	zdatný
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
levná	levný	k2eAgFnSc1d1	levná
úroda	úroda	k1gFnSc1	úroda
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
výběru	výběr	k1gInSc2	výběr
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
dobrou	dobrý	k2eAgFnSc4d1	dobrá
celoroční	celoroční	k2eAgFnSc4d1	celoroční
dostupnost	dostupnost	k1gFnSc4	dostupnost
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
stále	stále	k6eAd1	stále
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
okurek	okurka	k1gFnPc2	okurka
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
podává	podávat	k5eAaImIp3nS	podávat
tepelně	tepelně	k6eAd1	tepelně
upravená	upravený	k2eAgFnSc1d1	upravená
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
jen	jen	k9	jen
zažitý	zažitý	k2eAgInSc1d1	zažitý
stereotyp	stereotyp	k1gInSc1	stereotyp
<g/>
.	.	kIx.	.
protože	protože	k8xS	protože
zejména	zejména	k9	zejména
mladá	mladý	k2eAgFnSc1d1	mladá
cuketa	cuketa	k1gFnSc1	cuketa
je	být	k5eAaImIp3nS	být
chutná	chutný	k2eAgFnSc1d1	chutná
i	i	k8xC	i
syrová	syrový	k2eAgFnSc1d1	syrová
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
zeleninových	zeleninový	k2eAgInPc2d1	zeleninový
salátů	salát	k1gInPc2	salát
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
jemnou	jemný	k2eAgFnSc4d1	jemná
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
snadno	snadno	k6eAd1	snadno
přejímá	přejímat	k5eAaImIp3nS	přejímat
chuť	chuť	k1gFnSc4	chuť
přísad	přísada	k1gFnPc2	přísada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgNnSc1d1	vhodné
ji	on	k3xPp3gFnSc4	on
silně	silně	k6eAd1	silně
kořenit	kořenit	k5eAaImF	kořenit
a	a	k8xC	a
používat	používat	k5eAaImF	používat
výrazné	výrazný	k2eAgFnPc4d1	výrazná
ingredience	ingredience	k1gFnPc4	ingredience
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
česnek	česnek	k1gInSc4	česnek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dobře	dobře	k6eAd1	dobře
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
pokrmy	pokrm	k1gInPc4	pokrm
z	z	k7c2	z
dušené	dušený	k2eAgFnSc2d1	dušená
či	či	k8xC	či
zapečené	zapečený	k2eAgFnSc2d1	zapečená
cukety	cuketa	k1gFnSc2	cuketa
(	(	kIx(	(
<g/>
populární	populární	k2eAgFnSc1d1	populární
je	být	k5eAaImIp3nS	být
obdoba	obdoba	k1gFnSc1	obdoba
bramboráku	bramborák	k1gInSc2	bramborák
zvaná	zvaný	k2eAgFnSc1d1	zvaná
cukeťák	cukeťák	k1gInSc1	cukeťák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
však	však	k9	však
dělá	dělat	k5eAaImIp3nS	dělat
plněná	plněný	k2eAgFnSc1d1	plněná
<g/>
,	,	kIx,	,
smaží	smažit	k5eAaImIp3nS	smažit
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
řízek	řízek	k1gInSc4	řízek
<g/>
,	,	kIx,	,
griluje	grilovat	k5eAaImIp3nS	grilovat
se	se	k3xPyFc4	se
a	a	k8xC	a
vaří	vařit	k5eAaImIp3nS	vařit
se	se	k3xPyFc4	se
též	též	k9	též
cuketová	cuketový	k2eAgFnSc1d1	cuketová
polévka	polévka	k1gFnSc1	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
jsou	být	k5eAaImIp3nP	být
recepty	recept	k1gInPc1	recept
na	na	k7c4	na
sladké	sladký	k2eAgInPc4d1	sladký
moučníky	moučník	k1gInPc4	moučník
<g/>
,	,	kIx,	,
saláty	salát	k1gInPc4	salát
či	či	k8xC	či
knedlíky	knedlík	k1gInPc4	knedlík
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
pokrmů	pokrm	k1gInPc2	pokrm
se	se	k3xPyFc4	se
také	také	k9	také
dají	dát	k5eAaPmIp3nP	dát
spotřebovat	spotřebovat	k5eAaPmF	spotřebovat
cuketové	cuketový	k2eAgInPc4d1	cuketový
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
plněné	plněný	k2eAgNnSc1d1	plněné
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
cuketa	cuketa	k1gFnSc1	cuketa
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
zásadní	zásadní	k2eAgFnSc1d1	zásadní
přísada	přísada	k1gFnSc1	přísada
do	do	k7c2	do
ratatouille	ratatouille	k1gFnSc2	ratatouille
<g/>
.	.	kIx.	.
<g/>
Nejchutnější	chutný	k2eAgInPc1d3	nejchutnější
a	a	k8xC	a
nejkvalitnější	kvalitní	k2eAgInPc1d3	nejkvalitnější
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
až	až	k9	až
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc4d1	velký
plody	plod	k1gInPc4	plod
do	do	k7c2	do
25	[number]	k4	25
cm	cm	kA	cm
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nP	muset
loupat	loupat	k5eAaImF	loupat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
minimum	minimum	k1gNnSc4	minimum
masy	masa	k1gFnSc2	masa
měkkých	měkký	k2eAgNnPc2d1	měkké
nedovyvinutých	dovyvinutý	k2eNgNnPc2d1	nedovyvinuté
semínek	semínko	k1gNnPc2	semínko
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
křehké	křehký	k2eAgFnPc1d1	křehká
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
sklizeň	sklizeň	k1gFnSc1	sklizeň
menších	malý	k2eAgInPc2d2	menší
plodů	plod	k1gInPc2	plod
navíc	navíc	k6eAd1	navíc
rostliny	rostlina	k1gFnPc1	rostlina
nutí	nutit	k5eAaImIp3nP	nutit
do	do	k7c2	do
remontantnosti	remontantnost	k1gFnSc2	remontantnost
(	(	kIx(	(
<g/>
opakované	opakovaný	k2eAgFnSc2d1	opakovaná
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cuketa	cuketa	k1gFnSc1	cuketa
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
energetickou	energetický	k2eAgFnSc4d1	energetická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kolem	kolem	k7c2	kolem
91	[number]	k4	91
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
jen	jen	k9	jen
trochu	trochu	k6eAd1	trochu
vlákniny	vláknina	k1gFnPc4	vláknina
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
kolem	kolem	k7c2	kolem
4	[number]	k4	4
%	%	kIx~	%
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgFnSc7d3	nejcennější
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
karotenu	karoten	k1gInSc2	karoten
(	(	kIx(	(
<g/>
až	až	k9	až
5	[number]	k4	5
mg	mg	kA	mg
%	%	kIx~	%
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
u	u	k7c2	u
papriky	paprika	k1gFnSc2	paprika
<g/>
)	)	kIx)	)
a	a	k8xC	a
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyplavuje	vyplavovat	k5eAaImIp3nS	vyplavovat
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
nadbytečnou	nadbytečný	k2eAgFnSc4d1	nadbytečná
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
==	==	k?	==
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
cuketách	cuketa	k1gFnPc6	cuketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
vypěstovaná	vypěstovaný	k2eAgFnSc1d1	vypěstovaná
cuketa	cuketa	k1gFnSc1	cuketa
měřila	měřit	k5eAaImAgFnS	měřit
2,52	[number]	k4	2,52
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vypěstoval	vypěstovat	k5eAaPmAgMnS	vypěstovat
ji	on	k3xPp3gFnSc4	on
Ital	Ital	k1gMnSc1	Ital
Giovanni	Giovann	k1gMnPc1	Giovann
Batista	Batista	k1gMnSc1	Batista
Scozzafava	Scozzafava	k1gFnSc1	Scozzafava
v	v	k7c6	v
Niagara	Niagara	k1gFnSc1	Niagara
Falls	Falls	k1gInSc1	Falls
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Ontariu	Ontario	k1gNnSc6	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Změřena	změřen	k2eAgFnSc1d1	změřena
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
a	a	k8xC	a
rekord	rekord	k1gInSc1	rekord
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
v	v	k7c6	v
Guinessově	Guinessův	k2eAgFnSc6d1	Guinessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Zucchini	Zucchin	k1gMnPc1	Zucchin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Známou	známý	k2eAgFnSc7d1	známá
českou	český	k2eAgFnSc7d1	Česká
propagátorkou	propagátorka	k1gFnSc7	propagátorka
vaření	vaření	k1gNnSc2	vaření
z	z	k7c2	z
cuket	cuketa	k1gFnPc2	cuketa
je	být	k5eAaImIp3nS	být
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Štěpánková	Štěpánková	k1gFnSc1	Štěpánková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sepsala	sepsat	k5eAaPmAgFnS	sepsat
cuketovou	cuketový	k2eAgFnSc4d1	cuketová
kuchařku	kuchařka	k1gFnSc4	kuchařka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Tak	tak	k9	tak
tady	tady	k6eAd1	tady
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgInPc4	ten
recepty	recept	k1gInPc4	recept
na	na	k7c4	na
cukety	cuketa	k1gFnPc4	cuketa
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc1	paní
Ehmová	Ehmová	k1gFnSc1	Ehmová
(	(	kIx(	(
<g/>
Dona	dona	k1gFnSc1	dona
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cuketa	cuketa	k1gFnSc1	cuketa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cuketa	cuketa	k1gFnSc1	cuketa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Cuketa	cuketa	k1gFnSc1	cuketa
na	na	k7c4	na
biolibu	bioliba	k1gFnSc4	bioliba
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
cuketa	cuketa	k1gFnSc1	cuketa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
