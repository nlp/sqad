<s>
První	první	k4xOgNnSc1	první
prokazatelné	prokazatelný	k2eAgNnSc1d1	prokazatelné
dosažení	dosažení	k1gNnSc1	dosažení
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
až	až	k9	až
přelet	přelet	k1gInSc1	přelet
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
Norge	Norg	k1gFnSc2	Norg
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
