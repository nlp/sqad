<s>
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
<g/>
,	,	kIx,	,
též	též	k9	též
severní	severní	k2eAgFnSc1d1	severní
točna	točna	k1gFnSc1	točna
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
protíná	protínat	k5eAaImIp3nS	protínat
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Země	zem	k1gFnSc2	zem
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
dolů	dolů	k6eAd1	dolů
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
otáčela	otáčet	k5eAaImAgFnS	otáčet
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
uprostřed	uprostřed	k7c2	uprostřed
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
trvale	trvale	k6eAd1	trvale
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
vrstvou	vrstva	k1gFnSc7	vrstva
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Geografický	geografický	k2eAgInSc1d1	geografický
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c4	na
90	[number]	k4	90
<g/>
°	°	k?	°
zemské	zemský	k2eAgFnSc2d1	zemská
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
se	s	k7c7	s
severním	severní	k2eAgNnSc7d1	severní
pólem	pólo	k1gNnSc7	pólo
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
výprava	výprava	k1gFnSc1	výprava
na	na	k7c6	na
dobytí	dobytí	k1gNnSc6	dobytí
a	a	k8xC	a
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
podniknuta	podniknut	k2eAgFnSc1d1	podniknuta
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
Charlesem	Charles	k1gMnSc7	Charles
Francisem	Francis	k1gInSc7	Francis
Hallem	Hall	k1gMnSc7	Hall
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
nesla	nést	k5eAaImAgFnS	nést
označení	označení	k1gNnSc4	označení
Polaris	Polaris	k1gFnSc2	Polaris
expedition	expedition	k1gInSc1	expedition
(	(	kIx(	(
<g/>
Polární	polární	k2eAgFnSc1d1	polární
expedice	expedice	k1gFnSc1	expedice
<g/>
)	)	kIx)	)
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
expedice	expedice	k1gFnSc1	expedice
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
Fridtjofem	Fridtjof	k1gMnSc7	Fridtjof
Nansenem	Nansen	k1gMnSc7	Nansen
a	a	k8xC	a
pokusila	pokusit	k5eAaPmAgFnS	pokusit
se	se	k3xPyFc4	se
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
dostat	dostat	k5eAaPmF	dostat
driftem	drift	k1gInSc7	drift
na	na	k7c4	na
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zkonstruované	zkonstruovaný	k2eAgInPc1d1	zkonstruovaný
lodi	loď	k1gFnPc4	loď
Fram	Framo	k1gNnPc2	Framo
<g/>
.	.	kIx.	.
</s>
<s>
Nansen	Nansen	k1gInSc1	Nansen
se	s	k7c7	s
společníkem	společník	k1gMnSc7	společník
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1895	[number]	k4	1895
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
86	[number]	k4	86
<g/>
°	°	k?	°
14	[number]	k4	14
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
severnímu	severní	k2eAgInSc3d1	severní
pólu	pól	k1gInSc3	pól
se	se	k3xPyFc4	se
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
po	po	k7c6	po
ledu	led	k1gInSc6	led
vypravili	vypravit	k5eAaPmAgMnP	vypravit
Frederick	Frederick	k1gMnSc1	Frederick
Cook	Cook	k1gMnSc1	Cook
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Peary	Peara	k1gFnSc2	Peara
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Renomované	renomovaný	k2eAgFnPc1d1	renomovaná
instituce	instituce	k1gFnPc1	instituce
Cookovy	Cookův	k2eAgFnPc1d1	Cookova
doklady	doklad	k1gInPc4	doklad
dosažení	dosažení	k1gNnSc3	dosažení
pólu	pól	k1gInSc2	pól
neshledaly	shledat	k5eNaPmAgFnP	shledat
za	za	k7c4	za
průkazné	průkazný	k2eAgFnPc4d1	průkazná
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
pokořitele	pokořitel	k1gMnSc4	pokořitel
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
Peary	Peara	k1gFnSc2	Peara
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
ovšem	ovšem	k9	ovšem
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
úspěch	úspěch	k1gInSc4	úspěch
zpochybnily	zpochybnit	k5eAaPmAgFnP	zpochybnit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
cesty	cesta	k1gFnPc1	cesta
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
přelet	přelet	k1gInSc1	přelet
přes	přes	k7c4	přes
pól	pól	k1gInSc4	pól
v	v	k7c6	v
letounu	letoun	k1gInSc6	letoun
Fokker	Fokkra	k1gFnPc2	Fokkra
F.	F.	kA	F.
<g/>
VII	VII	kA	VII
Američan	Američan	k1gMnSc1	Američan
Richard	Richard	k1gMnSc1	Richard
Byrd	Byrd	k1gMnSc1	Byrd
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc3	jeho
dosažení	dosažení	k1gNnSc3	dosažení
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
zpochybňováno	zpochybňován	k2eAgNnSc1d1	zpochybňováno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
prokazatelné	prokazatelný	k2eAgNnSc1d1	prokazatelné
dosažení	dosažení	k1gNnSc1	dosažení
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
až	až	k9	až
přelet	přelet	k1gInSc1	přelet
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
Norge	Norg	k1gFnSc2	Norg
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Expedici	expedice	k1gFnSc4	expedice
vedl	vést	k5eAaImAgMnS	vést
norský	norský	k2eAgMnSc1d1	norský
polárník	polárník	k1gMnSc1	polárník
Roald	Roald	k1gMnSc1	Roald
Amundsen	Amundsen	k2eAgMnSc1d1	Amundsen
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
konstruktér	konstruktér	k1gMnSc1	konstruktér
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
Umberto	Umberta	k1gFnSc5	Umberta
Nobile	nobile	k1gMnSc1	nobile
a	a	k8xC	a
americký	americký	k2eAgMnSc1d1	americký
průzkumník	průzkumník	k1gMnSc1	průzkumník
a	a	k8xC	a
sponzor	sponzor	k1gMnSc1	sponzor
výpravy	výprava	k1gFnSc2	výprava
Lincoln	Lincoln	k1gMnSc1	Lincoln
Ellsworth	Ellsworth	k1gMnSc1	Ellsworth
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
pól	pól	k1gInSc4	pól
přelétla	přelétnout	k5eAaPmAgFnS	přelétnout
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
i	i	k8xC	i
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
Italia	Italia	k1gFnSc1	Italia
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
palubě	paluba	k1gFnSc6	paluba
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
František	František	k1gMnSc1	František
Běhounek	běhounek	k1gMnSc1	běhounek
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
silnému	silný	k2eAgInSc3d1	silný
větru	vítr	k1gInSc3	vítr
se	se	k3xPyFc4	se
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zamýšlené	zamýšlený	k2eAgNnSc1d1	zamýšlené
vysazení	vysazení	k1gNnSc1	vysazení
pasažérů	pasažér	k1gMnPc2	pasažér
na	na	k7c4	na
led	led	k1gInSc4	led
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
provedení	provedení	k1gNnSc2	provedení
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
měření	měření	k1gNnPc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
pod	pod	k7c7	pod
ledem	led	k1gInSc7	led
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
proplula	proplout	k5eAaPmAgFnS	proplout
první	první	k4xOgFnSc1	první
jaderná	jaderný	k2eAgFnSc1d1	jaderná
ponorka	ponorka	k1gFnSc1	ponorka
USS	USS	kA	USS
Nautilus	nautilus	k1gInSc1	nautilus
amerického	americký	k2eAgNnSc2d1	americké
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
doplul	doplout	k5eAaPmAgMnS	doplout
sovětský	sovětský	k2eAgMnSc1d1	sovětský
ledoborec	ledoborec	k1gMnSc1	ledoborec
Arktika	Arktikum	k1gNnSc2	Arktikum
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
teplejší	teplý	k2eAgInSc1d2	teplejší
než	než	k8xS	než
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
uprostřed	uprostřed	k7c2	uprostřed
severního	severní	k2eAgInSc2d1	severní
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
velkou	velký	k2eAgFnSc4d1	velká
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
setrvačnost	setrvačnost	k1gFnSc4	setrvačnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
povrch	povrch	k6eAd1wR	povrch
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
(	(	kIx(	(
<g/>
lednové	lednový	k2eAgFnPc1d1	lednová
<g/>
)	)	kIx)	)
teploty	teplota	k1gFnPc1	teplota
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
asi	asi	k9	asi
-43	-43	k4	-43
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
do	do	k7c2	do
-26	-26	k4	-26
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
-34	-34	k4	-34
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
(	(	kIx(	(
<g/>
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
bodu	bod	k1gInSc2	bod
mrazu	mráz	k1gInSc2	mráz
(	(	kIx(	(
<g/>
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
32	[number]	k4	32
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Mocnost	mocnost	k1gFnSc1	mocnost
(	(	kIx(	(
<g/>
tloušťka	tloušťka	k1gFnSc1	tloušťka
<g/>
)	)	kIx)	)
ledu	led	k1gInSc2	led
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgInPc2	dva
až	až	k9	až
tří	tři	k4xCgInPc2	tři
metrů	metr	k1gInPc2	metr
tloušťky	tloušťka	k1gFnSc2	tloušťka
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
vlivem	vlivem	k7c2	vlivem
posuvu	posuv	k1gInSc2	posuv
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc3	jejich
zdvihu	zdvih	k1gInSc3	zdvih
a	a	k8xC	a
poklesu	pokles	k1gInSc3	pokles
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rozlomení	rozlomení	k1gNnSc1	rozlomení
a	a	k8xC	a
odkrytí	odkrytí	k1gNnSc1	odkrytí
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrná	průměrný	k2eAgFnSc1d1	průměrná
tloušťka	tloušťka	k1gFnSc1	tloušťka
ledu	led	k1gInSc2	led
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
to	ten	k3xDgNnSc4	ten
připisují	připisovat	k5eAaImIp3nP	připisovat
vlivu	vliv	k1gInSc3	vliv
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
sporný	sporný	k2eAgInSc1d1	sporný
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
bude	být	k5eAaImBp3nS	být
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
úplně	úplně	k6eAd1	úplně
bez	bez	k7c2	bez
ledu	led	k1gInSc2	led
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
trvale	trvale	k6eAd1	trvale
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
během	během	k7c2	během
zimních	zimní	k2eAgInPc2d1	zimní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Východ	východ	k1gInSc1	východ
Slunce	slunce	k1gNnSc2	slunce
nastává	nastávat	k5eAaImIp3nS	nastávat
těsně	těsně	k6eAd1	těsně
před	před	k7c4	před
jarní	jarní	k2eAgFnPc4d1	jarní
rovnodennosti	rovnodennost	k1gFnPc4	rovnodennost
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slunce	slunce	k1gNnSc1	slunce
pak	pak	k6eAd1	pak
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
vystoupá	vystoupat	k5eAaPmIp3nS	vystoupat
do	do	k7c2	do
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
bodu	bod	k1gInSc2	bod
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
(	(	kIx(	(
<g/>
23,5	[number]	k4	23,5
<g/>
°	°	k?	°
elevace	elevace	k1gFnSc2	elevace
<g/>
)	)	kIx)	)
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
letnímu	letní	k2eAgInSc3d1	letní
slunovratu	slunovrat	k1gInSc3	slunovrat
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
Slunce	slunce	k1gNnSc1	slunce
klesá	klesat	k5eAaImIp3nS	klesat
k	k	k7c3	k
obzoru	obzor	k1gInSc3	obzor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
západu	západ	k1gInSc3	západ
při	při	k7c6	při
podzimní	podzimní	k2eAgFnSc6d1	podzimní
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
trvale	trvale	k6eAd1	trvale
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
polární	polární	k2eAgInSc1d1	polární
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc1	slunce
pod	pod	k7c7	pod
obzorem	obzor	k1gInSc7	obzor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
polární	polární	k2eAgFnSc1d1	polární
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
výšce	výška	k1gFnSc3	výška
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
trvání	trvání	k1gNnSc2	trvání
soumraku	soumrak	k1gInSc2	soumrak
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
ze	z	k7c2	z
středních	střední	k2eAgFnPc2d1	střední
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
výšek	výška	k1gFnPc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Občanský	občanský	k2eAgInSc1d1	občanský
soumrak	soumrak	k1gInSc1	soumrak
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
před	před	k7c7	před
východem	východ	k1gInSc7	východ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgInSc1d1	námořní
soumrak	soumrak	k1gInSc1	soumrak
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
pět	pět	k4xCc1	pět
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
východem	východ	k1gInSc7	východ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
astronomický	astronomický	k2eAgInSc1d1	astronomický
soumrak	soumrak	k1gInSc1	soumrak
trvá	trvat	k5eAaImIp3nS	trvat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
sedmi	sedm	k4xCc2	sedm
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
východem	východ	k1gInSc7	východ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
vlivem	vlivem	k7c2	vlivem
axiálního	axiální	k2eAgNnSc2d1	axiální
naklonění	naklonění	k1gNnSc2	naklonění
(	(	kIx(	(
<g/>
23,5	[number]	k4	23,5
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
Země	zem	k1gFnPc1	zem
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
rotace	rotace	k1gFnPc1	rotace
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
axiálního	axiální	k2eAgNnSc2d1	axiální
naklonění	naklonění	k1gNnSc2	naklonění
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gInSc1	její
úhel	úhel	k1gInSc1	úhel
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
téměř	téměř	k6eAd1	téměř
konstantní	konstantní	k2eAgFnSc1d1	konstantní
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
směřuje	směřovat	k5eAaImIp3nS	směřovat
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
postupně	postupně	k6eAd1	postupně
odvrací	odvracet	k5eAaImIp3nS	odvracet
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
až	až	k9	až
do	do	k7c2	do
zimních	zimní	k2eAgInPc2d1	zimní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
trvale	trvale	k6eAd1	trvale
odvrácen	odvrátit	k5eAaPmNgInS	odvrátit
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
šestiměsíčním	šestiměsíční	k2eAgInSc7d1	šestiměsíční
časovým	časový	k2eAgInSc7d1	časový
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgInSc1d1	místní
čas	čas	k1gInSc1	čas
určen	určit	k5eAaPmNgInS	určit
podle	podle	k7c2	podle
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
denní	denní	k2eAgFnSc1d1	denní
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
shodná	shodný	k2eAgFnSc1d1	shodná
a	a	k8xC	a
postavením	postavení	k1gNnSc7	postavení
slunce	slunce	k1gNnSc2	slunce
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
(	(	kIx(	(
<g/>
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc4	slunce
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
v	v	k7c6	v
nadhlavníku	nadhlavník	k1gInSc6	nadhlavník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
limitně	limitně	k6eAd1	limitně
blíží	blížit	k5eAaImIp3nS	blížit
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
zde	zde	k6eAd1	zde
vychází	vycházet	k5eAaImIp3nS	vycházet
a	a	k8xC	a
zapadá	zapadat	k5eAaPmIp3nS	zapadat
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
délkách	délka	k1gFnPc6	délka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
časových	časový	k2eAgNnPc6d1	časové
pásmech	pásmo	k1gNnPc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
není	být	k5eNaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pevné	pevný	k2eAgNnSc1d1	pevné
časové	časový	k2eAgNnSc1d1	časové
pásmo	pásmo	k1gNnSc1	pásmo
a	a	k8xC	a
expedice	expedice	k1gFnSc1	expedice
může	moct	k5eAaImIp3nS	moct
použít	použít	k5eAaPmF	použít
čas	čas	k1gInSc4	čas
GMT	GMT	kA	GMT
nebo	nebo	k8xC	nebo
národní	národní	k2eAgInSc4d1	národní
čas	čas	k1gInSc4	čas
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
expedice	expedice	k1gFnSc1	expedice
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
časová	časový	k2eAgNnPc1d1	časové
pásma	pásmo	k1gNnPc1	pásmo
na	na	k7c6	na
pólu	pól	k1gInSc6	pól
limitně	limitně	k6eAd1	limitně
blíží	blížit	k5eAaImIp3nS	blížit
nule	nula	k1gFnSc3	nula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
kratochvílí	kratochvíle	k1gFnSc7	kratochvíle
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
projití	projití	k1gNnSc2	projití
všech	všecek	k3xTgNnPc2	všecek
časových	časový	k2eAgNnPc2d1	časové
pásem	pásmo	k1gNnPc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
kruté	krutý	k2eAgFnPc4d1	krutá
podmínky	podmínka	k1gFnPc4	podmínka
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
mořští	mořský	k2eAgMnPc1d1	mořský
a	a	k8xC	a
suchozemští	suchozemský	k2eAgMnPc1d1	suchozemský
savci	savec	k1gMnPc1	savec
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
polární	polární	k2eAgFnSc1d1	polární
liška	liška	k1gFnSc1	liška
<g/>
,	,	kIx,	,
lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
polární	polární	k2eAgMnSc1d1	polární
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
tuleni	tuleň	k1gMnPc1	tuleň
a	a	k8xC	a
lachtani	lachtan	k1gMnPc1	lachtan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vodních	vodní	k2eAgMnPc2d1	vodní
živočichů	živočich	k1gMnPc2	živočich
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
výskyt	výskyt	k1gInSc1	výskyt
sasanek	sasanka	k1gFnPc2	sasanka
a	a	k8xC	a
neznámých	známý	k2eNgInPc2d1	neznámý
druhů	druh	k1gInPc2	druh
krevet	kreveta	k1gFnPc2	kreveta
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flóry	flóra	k1gFnSc2	flóra
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
vývoji	vývoj	k1gInSc6	vývoj
Země	zem	k1gFnSc2	zem
podřízeno	podřízen	k2eAgNnSc1d1	podřízeno
cirkumpolárnímu	cirkumpolární	k2eAgNnSc3d1	cirkumpolární
rozšíření	rozšíření	k1gNnSc3	rozšíření
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
žádná	žádný	k3yNgFnSc1	žádný
země	země	k1gFnSc1	země
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nemá	mít	k5eNaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c6	na
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
nebo	nebo	k8xC	nebo
oblasti	oblast	k1gFnSc2	oblast
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Arktidy	Arktida	k1gFnSc2	Arktida
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Grónsko	Grónsko	k1gNnSc4	Grónsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Aljašku	Aljaška	k1gFnSc4	Aljaška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgInP	omezit
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
200	[number]	k4	200
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
370	[number]	k4	370
km	km	kA	km
<g/>
)	)	kIx)	)
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
nárokovat	nárokovat	k5eAaImF	nárokovat
jako	jako	k8xC	jako
oblast	oblast	k1gFnSc4	oblast
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
nároků	nárok	k1gInPc2	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
touto	tento	k3xDgFnSc7	tento
hranicí	hranice	k1gFnSc7	hranice
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
spravován	spravovat	k5eAaImNgInS	spravovat
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ratifikaci	ratifikace	k1gFnSc6	ratifikace
Úmluvy	úmluva	k1gFnSc2	úmluva
OSN	OSN	kA	OSN
o	o	k7c6	o
mořském	mořský	k2eAgNnSc6d1	mořské
právu	právo	k1gNnSc6	právo
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
země	zem	k1gFnPc1	zem
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
dodržovat	dodržovat	k5eAaImF	dodržovat
toto	tento	k3xDgNnSc4	tento
ujednání	ujednání	k1gNnSc4	ujednání
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
(	(	kIx(	(
<g/>
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
Úmluvu	úmluva	k1gFnSc4	úmluva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
))	))	k?	))
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
ratifikovala	ratifikovat	k5eAaBmAgFnS	ratifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
)	)	kIx)	)
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
státy	stát	k1gInPc1	stát
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
aktivit	aktivita	k1gFnPc2	aktivita
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
musí	muset	k5eAaImIp3nS	muset
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
zájmu	zájem	k1gInSc2	zájem
spadá	spadat	k5eAaImIp3nS	spadat
výlučně	výlučně	k6eAd1	výlučně
pod	pod	k7c4	pod
jejich	jejich	k3xOp3gNnSc4	jejich
svrchované	svrchovaný	k2eAgNnSc4d1	svrchované
území	území	k1gNnSc4	území
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
předpokládaným	předpokládaný	k2eAgInSc7d1	předpokládaný
zdrojem	zdroj	k1gInSc7	zdroj
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pomocí	pomocí	k7c2	pomocí
sedimentů	sediment	k1gInPc2	sediment
z	z	k7c2	z
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
geograficky	geograficky	k6eAd1	geograficky
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
přehled	přehled	k1gInSc4	přehled
výprav	výprava	k1gFnPc2	výprava
a	a	k8xC	a
expedic	expedice	k1gFnPc2	expedice
k	k	k7c3	k
severnímu	severní	k2eAgNnSc3d1	severní
pólu	pólo	k1gNnSc3	pólo
<g/>
,	,	kIx,	,
dosažení	dosažení	k1gNnSc3	dosažení
pólu	pól	k1gInSc3	pól
plavidly	plavidlo	k1gNnPc7	plavidlo
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
přepis	přepis	k1gInSc1	přepis
názvu	název	k1gInSc2	název
lodě	loď	k1gFnSc2	loď
a	a	k8xC	a
jména	jméno	k1gNnSc2	jméno
kapitána	kapitán	k1gMnSc2	kapitán
podle	podle	k7c2	podle
anglického	anglický	k2eAgInSc2d1	anglický
zápisu	zápis	k1gInSc2	zápis
<g/>
)	)	kIx)	)
Vysvětlivky	vysvětlivka	k1gFnPc1	vysvětlivka
k	k	k7c3	k
poznámkám	poznámka	k1gFnPc3	poznámka
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
číslice	číslice	k1gFnPc4	číslice
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc1	počet
dosažení	dosažení	k1gNnSc2	dosažení
pólu	pól	k1gInSc2	pól
A	A	kA	A
<g/>
:	:	kIx,	:
vědecká	vědecký	k2eAgFnSc1d1	vědecká
expedice	expedice	k1gFnSc1	expedice
s	s	k7c7	s
pasažéry	pasažér	k1gMnPc7	pasažér
B	B	kA	B
<g/>
:	:	kIx,	:
vědecká	vědecký	k2eAgFnSc1d1	vědecká
expedice	expedice	k1gFnSc1	expedice
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
do	do	k7c2	do
Pacifického	pacifický	k2eAgInSc2d1	pacifický
oceánu	oceán	k1gInSc2	oceán
nebo	nebo	k8xC	nebo
zpět	zpět	k6eAd1	zpět
přes	přes	k7c4	přes
Severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
C	C	kA	C
<g/>
:	:	kIx,	:
není	být	k5eNaImIp3nS	být
ledoborec	ledoborec	k1gMnSc1	ledoborec
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
zesílená	zesílený	k2eAgFnSc1d1	zesílená
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
ledu	led	k1gInSc3	led
a	a	k8xC	a
krám	krám	k1gInSc1	krám
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
západních	západní	k2eAgFnPc6d1	západní
kulturách	kultura	k1gFnPc6	kultura
se	se	k3xPyFc4	se
k	k	k7c3	k
zeměpisnému	zeměpisný	k2eAgNnSc3d1	zeměpisné
severnímu	severní	k2eAgNnSc3d1	severní
pólu	pólo	k1gNnSc3	pólo
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
bydliště	bydliště	k1gNnSc4	bydliště
Santa	Sant	k1gInSc2	Sant
Clause	Clause	k1gFnSc2	Clause
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
pošta	pošta	k1gFnSc1	pošta
má	mít	k5eAaImIp3nS	mít
přiděleno	přidělen	k2eAgNnSc4d1	přiděleno
PSČ	PSČ	kA	PSČ
H0H	H0H	k1gFnSc2	H0H
0H0	[number]	k4	0H0
pro	pro	k7c4	pro
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
tradičního	tradiční	k2eAgMnSc2d1	tradiční
santovského	santovský	k2eAgMnSc2d1	santovský
"	"	kIx"	"
<g/>
Ho-ho-ho	Hoo-	k1gMnSc2	Ho-ho-
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Mytologie	mytologie	k1gFnSc1	mytologie
odráží	odrážet	k5eAaImIp3nS	odrážet
prastarou	prastarý	k2eAgFnSc4d1	prastará
esoterickou	esoterický	k2eAgFnSc4d1	esoterická
mytologii	mytologie	k1gFnSc4	mytologie
Hyperborea	Hyperbore	k1gInSc2	Hyperbore
<g/>
,	,	kIx,	,
že	že	k8xS	že
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
je	být	k5eAaImIp3nS	být
nadpozemský	nadpozemský	k2eAgInSc1d1	nadpozemský
příbytek	příbytek	k1gInSc1	příbytek
Boží	boží	k2eAgFnSc2d1	boží
a	a	k8xC	a
nadlidských	nadlidský	k2eAgFnPc2d1	nadlidská
bytosti	bytost	k1gFnPc1	bytost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Joscelyn	Joscelyn	k1gMnSc1	Joscelyn
Godwin	Godwin	k1gMnSc1	Godwin
<g/>
,	,	kIx,	,
Arktos	Arktos	k1gMnSc1	Arktos
<g/>
:	:	kIx,	:
Polární	polární	k2eAgInSc1d1	polární
mýtus	mýtus	k1gInSc1	mýtus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Santa	Santa	k1gFnSc1	Santa
Clause	Clause	k1gFnSc1	Clause
funguje	fungovat	k5eAaImIp3nS	fungovat
tedy	tedy	k9	tedy
jako	jako	k9	jako
esoterický	esoterický	k2eAgInSc1d1	esoterický
archetyp	archetyp	k1gInSc1	archetyp
duchovní	duchovní	k2eAgFnSc2d1	duchovní
čistoty	čistota	k1gFnSc2	čistota
a	a	k8xC	a
transcendence	transcendence	k1gFnSc2	transcendence
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zdokumentoval	zdokumentovat	k5eAaPmAgInS	zdokumentovat
Henry	henry	k1gInSc1	henry
Corbin	Corbin	k2eAgInSc1d1	Corbin
<g/>
,	,	kIx,	,
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
světonázoru	světonázor	k1gInSc6	světonázor
esoterického	esoterický	k2eAgInSc2d1	esoterický
Súfismu	súfismus	k1gInSc2	súfismus
a	a	k8xC	a
íránské	íránský	k2eAgFnSc2d1	íránská
mysticismu	mysticismus	k1gInSc3	mysticismus
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
nedostupnosti	nedostupnost	k1gFnSc2	nedostupnost
Severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
North	Northa	k1gFnPc2	Northa
Pole	pole	k1gFnSc2	pole
Marathon	Marathon	k1gInSc1	Marathon
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
Arktida	Arktida	k1gFnSc1	Arktida
</s>
