<s>
Protože	protože	k8xS	protože
obvod	obvod	k1gInSc1	obvod
země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
360	[number]	k4	360
úhlových	úhlový	k2eAgInPc2d1	úhlový
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
oběh	oběh	k1gInSc4	oběh
země	zem	k1gFnSc2	zem
1440	[number]	k4	1440
<g/>
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
západu	západ	k1gInSc2	západ
slunce	slunce	k1gNnSc2	slunce
15	[number]	k4	15
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
za	za	k7c4	za
1	[number]	k4	1
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
