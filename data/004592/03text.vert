<s>
Housle	housle	k1gFnPc1	housle
jsou	být	k5eAaImIp3nP	být
strunný	strunný	k2eAgInSc4d1	strunný
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
nástroj	nástroj	k1gInSc4	nástroj
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
strunami	struna	k1gFnPc7	struna
laděnými	laděný	k2eAgFnPc7d1	laděná
v	v	k7c6	v
čistých	čistý	k2eAgFnPc6d1	čistá
kvintách	kvinta	k1gFnPc6	kvinta
<g/>
:	:	kIx,	:
g	g	kA	g
<g/>
,	,	kIx,	,
d1	d1	k4	d1
<g/>
,	,	kIx,	,
a1	a1	k4	a1
<g/>
,	,	kIx,	,
e2	e2	k4	e2
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
skladatelů	skladatel	k1gMnPc2	skladatel
jim	on	k3xPp3gMnPc3	on
věnovala	věnovat	k5eAaPmAgFnS	věnovat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nejmenší	malý	k2eAgInPc1d3	nejmenší
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
houslových	houslový	k2eAgInPc2d1	houslový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
ještě	ještě	k9	ještě
viola	viola	k1gFnSc1	viola
a	a	k8xC	a
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
nástroj	nástroj	k1gInSc1	nástroj
–	–	k?	–
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
,	,	kIx,	,
však	však	k9	však
svou	svůj	k3xOyFgFnSc7	svůj
stavbou	stavba	k1gFnSc7	stavba
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
violové	violový	k2eAgFnSc2d1	violová
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
např.	např.	kA	např.
renesanční	renesanční	k2eAgMnSc1d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgInSc1d1	barokní
nástroj	nástroj	k1gInSc1	nástroj
viola	viola	k1gFnSc1	viola
da	da	k?	da
gamba	gamba	k1gFnSc1	gamba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
houslí	housle	k1gFnPc2	housle
tvoří	tvořit	k5eAaImIp3nS	tvořit
ozvučná	ozvučný	k2eAgFnSc1d1	ozvučná
skříň	skříň	k1gFnSc1	skříň
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
připevněn	připevněn	k2eAgInSc1d1	připevněn
krk	krk	k1gInSc1	krk
zakončený	zakončený	k2eAgInSc1d1	zakončený
šnekem	šnek	k1gMnSc7	šnek
a	a	k8xC	a
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
černý	černý	k2eAgInSc1d1	černý
hmatník	hmatník	k1gInSc1	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
houslí	houslit	k5eAaImIp3nP	houslit
upevněny	upevnit	k5eAaPmNgFnP	upevnit
pomocí	pomocí	k7c2	pomocí
struníku	struník	k1gInSc2	struník
<g/>
,	,	kIx,	,
vedeny	veden	k2eAgInPc4d1	veden
přes	přes	k7c4	přes
kobylku	kobylka	k1gFnSc4	kobylka
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
hmatníkem	hmatník	k1gInSc7	hmatník
a	a	k8xC	a
upevněny	upevněn	k2eAgFnPc4d1	upevněna
v	v	k7c6	v
količníku	količník	k1gInSc6	količník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napínají	napínat	k5eAaImIp3nP	napínat
kolíčky	kolíček	k1gInPc1	kolíček
<g/>
.	.	kIx.	.
</s>
<s>
Ozvučnou	ozvučný	k2eAgFnSc4d1	ozvučná
skříň	skříň	k1gFnSc4	skříň
neboli	neboli	k8xC	neboli
ozvučnici	ozvučnice	k1gFnSc4	ozvučnice
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvě	dva	k4xCgFnPc4	dva
mírně	mírně	k6eAd1	mírně
prohnuté	prohnutý	k2eAgFnPc4d1	prohnutá
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
u	u	k7c2	u
krajů	kraj	k1gInPc2	kraj
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
luby	lub	k1gInPc7	lub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
desce	deska	k1gFnSc6	deska
jsou	být	k5eAaImIp3nP	být
vyřezané	vyřezaný	k2eAgInPc1d1	vyřezaný
dva	dva	k4xCgInPc1	dva
otvory	otvor	k1gInPc1	otvor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
f	f	k?	f
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
efa	efa	k?	efa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
lepší	dobrý	k2eAgInSc4d2	lepší
výstup	výstup	k1gInSc4	výstup
zvuku	zvuk	k1gInSc2	zvuk
z	z	k7c2	z
vnitřku	vnitřek	k1gInSc2	vnitřek
houslí	houslit	k5eAaImIp3nP	houslit
a	a	k8xC	a
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
tvar	tvar	k1gInSc4	tvar
ohybu	ohyb	k1gInSc2	ohyb
víka	víko	k1gNnSc2	víko
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
smrkového	smrkový	k2eAgNnSc2d1	smrkové
dřeva	dřevo	k1gNnSc2	dřevo
s	s	k7c7	s
hustými	hustý	k2eAgInPc7d1	hustý
letokruhy	letokruh	k1gInPc7	letokruh
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
deska	deska	k1gFnSc1	deska
a	a	k8xC	a
luby	lub	k1gInPc1	lub
bývají	bývat	k5eAaImIp3nP	bývat
javorové	javorový	k2eAgInPc1d1	javorový
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc1	okraj
desek	deska	k1gFnPc2	deska
jsou	být	k5eAaImIp3nP	být
lemovány	lemovat	k5eAaImNgInP	lemovat
ozdobným	ozdobný	k2eAgNnSc7d1	ozdobné
vykládáním	vykládání	k1gNnSc7	vykládání
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
výložkami	výložka	k1gFnPc7	výložka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
kromě	kromě	k7c2	kromě
estetické	estetický	k2eAgFnSc2d1	estetická
funkce	funkce	k1gFnSc2	funkce
částečně	částečně	k6eAd1	částečně
chrání	chránit	k5eAaImIp3nS	chránit
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
desky	deska	k1gFnPc4	deska
před	před	k7c7	před
naštípnutím	naštípnutí	k1gNnSc7	naštípnutí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
laciných	laciný	k2eAgInPc2d1	laciný
modelů	model	k1gInPc2	model
jsou	být	k5eAaImIp3nP	být
výložky	výložka	k1gFnPc1	výložka
jen	jen	k6eAd1	jen
naznačené	naznačený	k2eAgFnPc1d1	naznačená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
strany	strana	k1gFnSc2	strana
víka	víko	k1gNnSc2	víko
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
strunou	struna	k1gFnSc7	struna
G	G	kA	G
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
violy	viola	k1gFnSc2	viola
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
<g/>
)	)	kIx)	)
připevněna	připevněn	k2eAgFnSc1d1	připevněna
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
lišta	lišta	k1gFnSc1	lišta
jmenující	jmenující	k2eAgFnSc1d1	jmenující
se	se	k3xPyFc4	se
basový	basový	k2eAgInSc1d1	basový
trámec	trámec	k1gInSc1	trámec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
přenášet	přenášet	k5eAaImF	přenášet
nízké	nízký	k2eAgFnPc4d1	nízká
frekvence	frekvence	k1gFnPc4	frekvence
na	na	k7c4	na
svrchní	svrchní	k2eAgFnSc4d1	svrchní
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
naopak	naopak	k6eAd1	naopak
přenáší	přenášet	k5eAaImIp3nS	přenášet
vysoké	vysoký	k2eAgFnPc4d1	vysoká
frekvence	frekvence	k1gFnPc4	frekvence
ze	z	k7c2	z
svrchní	svrchní	k2eAgFnSc2d1	svrchní
desky	deska	k1gFnSc2	deska
na	na	k7c4	na
spodní	spodní	k2eAgNnSc4d1	spodní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vložena	vložit	k5eAaPmNgFnS	vložit
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
desky	deska	k1gFnPc4	deska
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
pod	pod	k7c7	pod
strunou	struna	k1gFnSc7	struna
E.	E.	kA	E.
I	i	k8xC	i
nepatrná	patrný	k2eNgFnSc1d1	patrný
odchylka	odchylka	k1gFnSc1	odchylka
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
umístění	umístění	k1gNnSc6	umístění
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
výraznou	výrazný	k2eAgFnSc4d1	výrazná
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
intenzity	intenzita	k1gFnSc2	intenzita
tónu	tón	k1gInSc2	tón
houslí	houslit	k5eAaImIp3nS	houslit
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
také	také	k9	také
brání	bránit	k5eAaImIp3nS	bránit
prohnutí	prohnutí	k1gNnSc4	prohnutí
obou	dva	k4xCgFnPc2	dva
desek	deska	k1gFnPc2	deska
do	do	k7c2	do
vnitřku	vnitřek	k1gInSc2	vnitřek
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
víka	víko	k1gNnSc2	víko
je	být	k5eAaImIp3nS	být
připevněn	připevněn	k2eAgInSc1d1	připevněn
podbradek	podbradek	k1gInSc1	podbradek
pro	pro	k7c4	pro
větší	veliký	k2eAgInSc4d2	veliký
komfort	komfort	k1gInSc4	komfort
při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
houslistů	houslista	k1gMnPc2	houslista
ještě	ještě	k9	ještě
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
pavouk	pavouk	k1gMnSc1	pavouk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
opěrka	opěrka	k1gFnSc1	opěrka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
na	na	k7c4	na
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
houslí	houslit	k5eAaImIp3nS	houslit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
netlačily	tlačit	k5eNaImAgFnP	tlačit
na	na	k7c4	na
klíční	klíční	k2eAgFnSc4d1	klíční
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Krk	krk	k1gInSc1	krk
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
stejného	stejný	k2eAgNnSc2d1	stejné
dřeva	dřevo	k1gNnSc2	dřevo
jako	jako	k8xS	jako
spodní	spodní	k2eAgFnSc1d1	spodní
deska	deska	k1gFnSc1	deska
a	a	k8xC	a
luby	lub	k1gInPc1	lub
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
javoru	javor	k1gInSc2	javor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
svrchní	svrchní	k2eAgFnSc6d1	svrchní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
připojen	připojen	k2eAgInSc1d1	připojen
hmatník	hmatník	k1gInSc1	hmatník
z	z	k7c2	z
ebenového	ebenový	k2eAgNnSc2d1	ebenové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
u	u	k7c2	u
levnějších	levný	k2eAgInPc2d2	levnější
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
lacinější	laciný	k2eAgInPc1d2	lacinější
druhy	druh	k1gInPc1	druh
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
obarvené	obarvený	k2eAgFnSc2d1	obarvená
na	na	k7c4	na
černo	černo	k1gNnSc4	černo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
opět	opět	k6eAd1	opět
javorové	javorový	k2eAgNnSc1d1	javorové
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
krku	krk	k1gInSc2	krk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
količník	količník	k1gInSc1	količník
zakončený	zakončený	k2eAgInSc1d1	zakončený
šnekem	šnek	k1gInSc7	šnek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
estetická	estetický	k2eAgFnSc1d1	estetická
krása	krása	k1gFnSc1	krása
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
količníku	količník	k1gInSc3	količník
upevněny	upevnit	k5eAaPmNgFnP	upevnit
čtyřmi	čtyři	k4xCgInPc7	čtyři
kolíčky	kolíček	k1gInPc7	kolíček
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgFnP	vést
přes	přes	k7c4	přes
malý	malý	k2eAgInSc4d1	malý
pražec	pražec	k1gInSc4	pražec
(	(	kIx(	(
<g/>
začátek	začátek	k1gInSc1	začátek
hmatníku	hmatník	k1gInSc2	hmatník
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
nad	nad	k7c4	nad
hmatník	hmatník	k1gInSc4	hmatník
a	a	k8xC	a
přes	přes	k7c4	přes
kobylku	kobylka	k1gFnSc4	kobylka
do	do	k7c2	do
struníku	struník	k1gInSc2	struník
<g/>
.	.	kIx.	.
</s>
<s>
Kobylka	kobylka	k1gFnSc1	kobylka
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
javorového	javorový	k2eAgNnSc2d1	javorové
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
důležitých	důležitý	k2eAgFnPc2d1	důležitá
funkcí	funkce	k1gFnPc2	funkce
<g/>
:	:	kIx,	:
Drží	držet	k5eAaImIp3nP	držet
struny	struna	k1gFnPc1	struna
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
hmatníku	hmatník	k1gInSc2	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
zaoblený	zaoblený	k2eAgInSc1d1	zaoblený
tvar	tvar	k1gInSc1	tvar
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
smyčec	smyčec	k1gInSc1	smyčec
dotýkal	dotýkat	k5eAaImAgMnS	dotýkat
strun	struna	k1gFnPc2	struna
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Přenáší	přenášet	k5eAaImIp3nP	přenášet
vibrace	vibrace	k1gFnPc1	vibrace
strun	struna	k1gFnPc2	struna
na	na	k7c4	na
trup	trup	k1gInSc4	trup
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
akustický	akustický	k2eAgInSc4d1	akustický
filtr	filtr	k1gInSc4	filtr
–	–	k?	–
omezuje	omezovat	k5eAaImIp3nS	omezovat
některé	některý	k3yIgFnPc4	některý
frekvence	frekvence	k1gFnPc4	frekvence
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
zhoršovaly	zhoršovat	k5eAaImAgFnP	zhoršovat
tónové	tónový	k2eAgFnPc4d1	tónová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
malá	malý	k2eAgFnSc1d1	malá
odchylka	odchylka	k1gFnSc1	odchylka
v	v	k7c6	v
umístění	umístění	k1gNnSc6	umístění
kobylky	kobylka	k1gFnSc2	kobylka
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
výraznou	výrazný	k2eAgFnSc4d1	výrazná
změnu	změna	k1gFnSc4	změna
charakteru	charakter	k1gInSc2	charakter
tónu	tón	k1gInSc2	tón
houslí	housle	k1gFnPc2	housle
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
duše	duše	k1gFnSc2	duše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kovových	kovový	k2eAgFnPc2d1	kovová
strun	struna	k1gFnPc2	struna
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
struny	struna	k1gFnSc2	struna
nad	nad	k7c7	nad
koncem	konec	k1gInSc7	konec
hmatníku	hmatník	k1gInSc2	hmatník
okolo	okolo	k7c2	okolo
2,5	[number]	k4	2,5
mm	mm	kA	mm
pro	pro	k7c4	pro
strunu	struna	k1gFnSc4	struna
e	e	k0	e
<g/>
2	[number]	k4	2
a	a	k8xC	a
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
až	až	k9	až
po	po	k7c6	po
cca	cca	kA	cca
4	[number]	k4	4
mm	mm	kA	mm
u	u	k7c2	u
struny	struna	k1gFnSc2	struna
g.	g.	k?	g.
U	u	k7c2	u
střevových	střevový	k2eAgFnPc2d1	střevový
strun	struna	k1gFnPc2	struna
jsou	být	k5eAaImIp3nP	být
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
nad	nad	k7c7	nad
hmatníkem	hmatník	k1gInSc7	hmatník
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
struník	struník	k1gInSc4	struník
jsou	být	k5eAaImIp3nP	být
připevněny	připevněn	k2eAgInPc1d1	připevněn
konce	konec	k1gInPc1	konec
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyráběn	vyrábět	k5eAaImNgMnS	vyrábět
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
kovu	kov	k1gInSc2	kov
nebo	nebo	k8xC	nebo
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
struníku	struník	k1gInSc2	struník
vede	vést	k5eAaImIp3nS	vést
vlákno	vlákno	k1gNnSc1	vlákno
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgNnSc1d1	zvané
poutko	poutko	k1gNnSc1	poutko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
připevněno	připevnit	k5eAaPmNgNnS	připevnit
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
žaludu	žalud	k1gInSc6	žalud
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zabezpečeno	zabezpečen	k2eAgNnSc1d1	zabezpečeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
struník	struník	k1gInSc1	struník
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
horní	horní	k2eAgFnPc4d1	horní
desky	deska	k1gFnPc4	deska
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
struníku	struník	k1gInSc6	struník
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
připojené	připojený	k2eAgInPc4d1	připojený
dolaďovače	dolaďovač	k1gInPc4	dolaďovač
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
malým	malý	k2eAgFnPc3d1	malá
změnám	změna	k1gFnPc3	změna
výšky	výška	k1gFnSc2	výška
tónu	tón	k1gInSc2	tón
strun	struna	k1gFnPc2	struna
při	při	k7c6	při
ladění	ladění	k1gNnSc6	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
struníky	struník	k1gInPc1	struník
s	s	k7c7	s
vestavěnými	vestavěný	k2eAgInPc7d1	vestavěný
dolaďovači	dolaďovač	k1gInPc7	dolaďovač
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
smyčec	smyčec	k1gInSc1	smyčec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
smyčce	smyčec	k1gInSc2	smyčec
jsou	být	k5eAaImIp3nP	být
žíně	žíně	k1gFnPc1	žíně
napnuté	napnutý	k2eAgFnPc1d1	napnutá
mezi	mezi	k7c7	mezi
špičkou	špička	k1gFnSc7	špička
smyčce	smyčec	k1gInSc2	smyčec
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
žabkou	žabka	k1gFnSc7	žabka
<g/>
.	.	kIx.	.
</s>
<s>
Žíně	žíně	k1gFnSc1	žíně
lze	lze	k6eAd1	lze
povolovat	povolovat	k5eAaImF	povolovat
a	a	k8xC	a
napínat	napínat	k5eAaImF	napínat
pomocí	pomocí	k7c2	pomocí
šroubu	šroub	k1gInSc2	šroub
u	u	k7c2	u
žabky	žabka	k1gFnSc2	žabka
a	a	k8xC	a
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
namazat	namazat	k5eAaPmF	namazat
kalafunou	kalafuna	k1gFnSc7	kalafuna
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
tření	tření	k1gNnSc4	tření
se	s	k7c7	s
strunou	struna	k1gFnSc7	struna
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
kalafuny	kalafuna	k1gFnSc2	kalafuna
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
množství	množství	k1gNnSc4	množství
nanesené	nanesený	k2eAgNnSc4d1	nanesené
na	na	k7c4	na
smyčec	smyčec	k1gInSc4	smyčec
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
podle	podle	k7c2	podle
použitých	použitý	k2eAgFnPc2d1	použitá
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
podle	podle	k7c2	podle
stylu	styl	k1gInSc2	styl
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
měkčí	měkký	k2eAgFnPc1d2	měkčí
kalafuny	kalafuna	k1gFnPc1	kalafuna
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
tmavší	tmavý	k2eAgFnPc1d2	tmavší
<g/>
,	,	kIx,	,
světlejší	světlý	k2eAgFnPc1d2	světlejší
kalafuny	kalafuna	k1gFnPc1	kalafuna
bývají	bývat	k5eAaImIp3nP	bývat
tvrdší	tvrdý	k2eAgFnPc1d2	tvrdší
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejlevnějších	levný	k2eAgInPc2d3	nejlevnější
typů	typ	k1gInPc2	typ
smyčců	smyčec	k1gInPc2	smyčec
se	se	k3xPyFc4	se
koňské	koňský	k2eAgFnPc1d1	koňská
žíně	žíně	k1gFnPc1	žíně
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
syntetickými	syntetický	k2eAgNnPc7d1	syntetické
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
horší	zlý	k2eAgFnPc4d2	horší
užitné	užitný	k2eAgFnPc4d1	užitná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
hůlka	hůlka	k1gFnSc1	hůlka
smyčce	smyčec	k1gInSc2	smyčec
bývá	bývat	k5eAaImIp3nS	bývat
obyčejně	obyčejně	k6eAd1	obyčejně
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
brazilového	brazilový	k2eAgNnSc2d1	brazilový
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kvalitnější	kvalitní	k2eAgNnSc1d2	kvalitnější
dřevo	dřevo	k1gNnSc1	dřevo
na	na	k7c6	na
smyčce	smyčka	k1gFnSc6	smyčka
je	být	k5eAaImIp3nS	být
fernambukové	fernambukový	k2eAgNnSc1d1	fernambukový
<g/>
.	.	kIx.	.
</s>
<s>
Dražší	drahý	k2eAgInPc1d2	dražší
smyčce	smyčec	k1gInPc1	smyčec
bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
technologií	technologie	k1gFnPc2	technologie
štípání	štípání	k1gNnSc4	štípání
dřeva	dřevo	k1gNnSc2	dřevo
přesně	přesně	k6eAd1	přesně
po	po	k7c6	po
letech	let	k1gInPc6	let
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezačnou	začít	k5eNaPmIp3nP	začít
prohýbat	prohýbat	k5eAaImF	prohýbat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovské	mistrovský	k2eAgFnSc6d1	mistrovská
smyčce	smyčka	k1gFnSc6	smyčka
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
ozdobném	ozdobný	k2eAgNnSc6d1	ozdobné
vykládání	vykládání	k1gNnSc6	vykládání
žabky	žabka	k1gFnSc2	žabka
používají	používat	k5eAaImIp3nP	používat
ušlechtilé	ušlechtilý	k2eAgInPc1d1	ušlechtilý
a	a	k8xC	a
vzácné	vzácný	k2eAgInPc1d1	vzácný
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
perleť	perleť	k1gFnSc1	perleť
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
slonovina	slonovina	k1gFnSc1	slonovina
<g/>
,	,	kIx,	,
želvovina	želvovina	k1gFnSc1	želvovina
atd.	atd.	kA	atd.
Houslové	houslový	k2eAgFnSc2d1	houslová
struny	struna	k1gFnSc2	struna
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
z	z	k7c2	z
ovčích	ovčí	k2eAgNnPc2d1	ovčí
střev	střevo	k1gNnPc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tyto	tento	k3xDgFnPc4	tento
struny	struna	k1gFnPc4	struna
používají	používat	k5eAaImIp3nP	používat
interpreti	interpret	k1gMnPc1	interpret
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
docílit	docílit	k5eAaPmF	docílit
autentického	autentický	k2eAgInSc2d1	autentický
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
měly	mít	k5eAaImAgFnP	mít
housle	housle	k1gFnPc4	housle
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c4	na
struny	struna	k1gFnPc4	struna
ze	z	k7c2	z
střev	střevo	k1gNnPc2	střevo
přidávat	přidávat	k5eAaImF	přidávat
kovové	kovový	k2eAgNnSc4d1	kovové
vinutí	vinutí	k1gNnSc4	vinutí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dovolovalo	dovolovat	k5eAaImAgNnS	dovolovat
silnější	silný	k2eAgInPc4d2	silnější
záběry	záběr	k1gInPc4	záběr
smyčcem	smyčec	k1gInSc7	smyčec
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
silnější	silný	k2eAgFnSc1d2	silnější
zvuk	zvuk	k1gInSc1	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
struny	struna	k1gFnPc1	struna
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
tvořeného	tvořený	k2eAgInSc2d1	tvořený
syntetickým	syntetický	k2eAgNnSc7d1	syntetické
nebo	nebo	k8xC	nebo
kovovým	kovový	k2eAgNnSc7d1	kovové
vláknem	vlákno	k1gNnSc7	vlákno
a	a	k8xC	a
z	z	k7c2	z
kovového	kovový	k2eAgNnSc2d1	kovové
vinutí	vinutí	k1gNnSc2	vinutí
(	(	kIx(	(
<g/>
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
<g/>
,	,	kIx,	,
ocelové	ocelový	k2eAgFnSc2d1	ocelová
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
<g/>
,	,	kIx,	,
chromové	chromový	k2eAgFnSc2d1	chromová
<g/>
,	,	kIx,	,
titanové	titanový	k2eAgFnSc2d1	titanová
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
nejkvalitnější	kvalitní	k2eAgFnPc1d3	nejkvalitnější
struny	struna	k1gFnPc1	struna
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
teplý	teplý	k2eAgInSc4d1	teplý
zvuk	zvuk	k1gInSc4	zvuk
jako	jako	k8xC	jako
střevové	střevový	k2eAgFnPc4d1	střevový
struny	struna	k1gFnPc4	struna
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
nerozlaďují	rozlaďovat	k5eNaImIp3nP	rozlaďovat
ani	ani	k8xC	ani
při	při	k7c6	při
prudkých	prudký	k2eAgInPc6d1	prudký
výkyvech	výkyv	k1gInPc6	výkyv
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
hraní	hraní	k1gNnSc6	hraní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
střevových	střevový	k2eAgFnPc2d1	střevový
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Lak	lak	k1gInSc1	lak
chrání	chránit	k5eAaImIp3nS	chránit
housle	housle	k1gFnPc4	housle
před	před	k7c7	před
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
a	a	k8xC	a
chemickým	chemický	k2eAgNnSc7d1	chemické
poškozením	poškození	k1gNnSc7	poškození
a	a	k8xC	a
plní	plnit	k5eAaImIp3nP	plnit
i	i	k9	i
estetickou	estetický	k2eAgFnSc4d1	estetická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
receptur	receptura	k1gFnPc2	receptura
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
přírodní	přírodní	k2eAgFnPc1d1	přírodní
pryskyřice	pryskyřice	k1gFnPc1	pryskyřice
naložené	naložený	k2eAgFnPc1d1	naložená
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
nebo	nebo	k8xC	nebo
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
přísady	přísada	k1gFnPc1	přísada
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
mastix	mastix	k1gInSc1	mastix
–	–	k?	–
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
stromu	strom	k1gInSc2	strom
řečík	řečík	k1gInSc4	řečík
lentišek	lentišek	k1gInSc1	lentišek
(	(	kIx(	(
<g/>
Pistacia	Pistacia	k1gFnSc1	Pistacia
lentiscus	lentiscus	k1gMnSc1	lentiscus
<g/>
)	)	kIx)	)
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
benzoová	benzoový	k2eAgFnSc1d1	benzoová
šelak	šelak	k1gInSc4	šelak
–	–	k?	–
přírodní	přírodní	k2eAgFnSc2d1	přírodní
živice	živice	k1gFnSc2	živice
získávaná	získávaný	k2eAgFnSc1d1	získávaná
z	z	k7c2	z
výměšků	výměšek	k1gInPc2	výměšek
červa	červ	k1gMnSc2	červ
Tachardia	Tachardium	k1gNnSc2	Tachardium
lacca	lacca	k1gMnSc1	lacca
sandarak	sandarak	k1gMnSc1	sandarak
–	–	k?	–
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
stromu	strom	k1gInSc2	strom
sandarakovník	sandarakovník	k1gInSc4	sandarakovník
článkovaný	článkovaný	k2eAgInSc4d1	článkovaný
(	(	kIx(	(
<g/>
Tetraclinis	Tetraclinis	k1gInSc4	Tetraclinis
articulata	articule	k1gNnPc1	articule
<g/>
)	)	kIx)	)
kopál	kopál	k1gInSc1	kopál
–	–	k?	–
fosilizovaná	fosilizovaný	k2eAgFnSc1d1	fosilizovaná
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
jantar	jantar	k1gInSc1	jantar
dračí	dračí	k2eAgFnSc1d1	dračí
krev	krev	k1gFnSc1	krev
–	–	k?	–
přírodní	přírodní	k2eAgNnSc4d1	přírodní
barvivo	barvivo	k1gNnSc4	barvivo
získávané	získávaný	k2eAgInPc1d1	získávaný
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
dračince	dračinec	k1gInSc2	dračinec
rumělkového	rumělkový	k2eAgInSc2d1	rumělkový
šafrán	šafrán	k1gInSc1	šafrán
terpentýn	terpentýn	k1gInSc1	terpentýn
propolis	propolis	k1gInSc1	propolis
Lak	lak	k1gInSc4	lak
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
vylepšit	vylepšit	k5eAaPmF	vylepšit
akustické	akustický	k2eAgFnPc4d1	akustická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
nenalakovaných	nalakovaný	k2eNgFnPc2d1	nenalakovaná
houslí	housle	k1gFnPc2	housle
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
doby	doba	k1gFnSc2	doba
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
degradací	degradace	k1gFnSc7	degradace
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
lak	lak	k1gInSc1	lak
pouze	pouze	k6eAd1	pouze
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
uchovat	uchovat	k5eAaPmF	uchovat
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlastnosti	vlastnost	k1gFnPc4	vlastnost
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
levné	levný	k2eAgInPc4d1	levný
tovární	tovární	k2eAgInPc4d1	tovární
a	a	k8xC	a
školní	školní	k2eAgFnPc4d1	školní
housle	housle	k1gFnPc4	housle
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
laky	lak	k1gInPc1	lak
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
umělé	umělý	k2eAgFnSc2d1	umělá
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
(	(	kIx(	(
<g/>
polyuretanové	polyuretanový	k2eAgInPc1d1	polyuretanový
laky	lak	k1gInPc1	lak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
nalakování	nalakování	k1gNnSc1	nalakování
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
odolnější	odolný	k2eAgInPc4d2	odolnější
než	než	k8xS	než
tradiční	tradiční	k2eAgInPc4d1	tradiční
olejové	olejový	k2eAgInPc4d1	olejový
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
lihové	lihový	k2eAgInPc1d1	lihový
laky	lak	k1gInPc1	lak
<g/>
,	,	kIx,	,
lak	lak	k1gInSc1	lak
schne	schnout	k5eAaImIp3nS	schnout
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
tradičními	tradiční	k2eAgInPc7d1	tradiční
materiály	materiál	k1gInPc7	materiál
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tónová	tónový	k2eAgFnSc1d1	tónová
kvalita	kvalita	k1gFnSc1	kvalita
takto	takto	k6eAd1	takto
lakovaných	lakovaný	k2eAgFnPc2d1	lakovaná
houslí	housle	k1gFnPc2	housle
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgMnSc1d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
standardních	standardní	k2eAgFnPc2d1	standardní
"	"	kIx"	"
<g/>
čtyřčtvrťových	čtyřčtvrťový	k2eAgFnPc2d1	čtyřčtvrťový
<g/>
"	"	kIx"	"
houslí	housle	k1gFnPc2	housle
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
36	[number]	k4	36
až	až	k9	až
38	[number]	k4	38
cm	cm	kA	cm
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
menší	malý	k2eAgFnPc4d2	menší
housle	housle	k1gFnPc4	housle
<g/>
:	:	kIx,	:
tříčtvrťové	tříčtvrťový	k2eAgFnPc4d1	tříčtvrťový
<g/>
,	,	kIx,	,
půlové	půlový	k2eAgFnPc4d1	půlová
<g/>
,	,	kIx,	,
čtvrťové	čtvrťový	k2eAgFnPc4d1	čtvrťová
atd.	atd.	kA	atd.
Toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
jen	jen	k9	jen
orientační	orientační	k2eAgFnPc1d1	orientační
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
skutečná	skutečný	k2eAgFnSc1d1	skutečná
délka	délka	k1gFnSc1	délka
půlových	půlový	k2eAgFnPc2d1	půlová
houslí	housle	k1gFnPc2	housle
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
31	[number]	k4	31
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
vhodné	vhodný	k2eAgFnSc2d1	vhodná
délky	délka	k1gFnSc2	délka
houslí	housle	k1gFnPc2	housle
platí	platit	k5eAaImIp3nS	platit
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
:	:	kIx,	:
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
dítě	dítě	k1gNnSc1	dítě
housle	housle	k1gFnPc4	housle
pod	pod	k7c7	pod
bradou	brada	k1gFnSc7	brada
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
dlaní	dlaň	k1gFnSc7	dlaň
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
na	na	k7c4	na
konec	konec	k1gInSc4	konec
šneku	šnek	k1gInSc2	šnek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
vidělo	vidět	k5eAaImAgNnS	vidět
na	na	k7c4	na
špičky	špička	k1gFnPc4	špička
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nP	držet
pod	pod	k7c7	pod
bradou	brada	k1gFnSc7	brada
a	a	k8xC	a
levou	levý	k2eAgFnSc7d1	levá
rukou	ruka	k1gFnSc7	ruka
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
na	na	k7c6	na
hmatníku	hmatník	k1gInSc6	hmatník
výšky	výška	k1gFnSc2	výška
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
buď	buď	k8xC	buď
rozeznívá	rozeznívat	k5eAaImIp3nS	rozeznívat
struny	struna	k1gFnSc2	struna
smyčcem	smyčec	k1gInSc7	smyčec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
brnkáním	brnkání	k1gNnSc7	brnkání
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pizzicato	pizzicato	k6eAd1	pizzicato
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc4	housle
nemají	mít	k5eNaImIp3nP	mít
pražce	pražec	k1gInPc1	pražec
jako	jako	k8xC	jako
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
znát	znát	k5eAaImF	znát
přesná	přesný	k2eAgNnPc4d1	přesné
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
položit	položit	k5eAaPmF	položit
prst	prst	k1gInSc4	prst
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zazněl	zaznít	k5eAaPmAgInS	zaznít
určitý	určitý	k2eAgInSc4d1	určitý
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Prsty	prst	k1gInPc1	prst
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nP	značit
od	od	k7c2	od
1	[number]	k4	1
(	(	kIx(	(
<g/>
ukazováček	ukazováček	k1gInSc1	ukazováček
<g/>
)	)	kIx)	)
do	do	k7c2	do
(	(	kIx(	(
<g/>
4	[number]	k4	4
malíček	malíček	k1gInSc1	malíček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
0	[number]	k4	0
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
hrát	hrát	k5eAaImF	hrát
prázdná	prázdný	k2eAgFnSc1d1	prázdná
struna	struna	k1gFnSc1	struna
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
vlevo	vlevo	k6eAd1	vlevo
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgInPc7	jaký
prsty	prst	k1gInPc7	prst
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
tóny	tón	k1gInPc1	tón
v	v	k7c6	v
první	první	k4xOgFnSc6	první
poloze	poloha	k1gFnSc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
prstem	prst	k1gInSc7	prst
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
celé	celý	k2eAgInPc1d1	celý
tóny	tón	k1gInPc1	tón
a	a	k8xC	a
<g/>
,	,	kIx,	,
e1	e1	k4	e1
<g/>
,	,	kIx,	,
h1	h1	k4	h1
<g/>
,	,	kIx,	,
f2	f2	k4	f2
(	(	kIx(	(
<g/>
sekce	sekce	k1gFnSc2	sekce
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
modrou	modrý	k2eAgFnSc7d1	modrá
značkou	značka	k1gFnSc7	značka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
prstem	prst	k1gInSc7	prst
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
také	také	k9	také
všechny	všechen	k3xTgInPc1	všechen
zmenšené	zmenšený	k2eAgInPc1d1	zmenšený
a	a	k8xC	a
zvětšené	zvětšený	k2eAgInPc1d1	zvětšený
tóny	tón	k1gInPc1	tón
od	od	k7c2	od
těchto	tento	k3xDgInPc2	tento
tónů	tón	k1gInPc2	tón
<g/>
:	:	kIx,	:
as	as	k1gInSc1	as
<g/>
,	,	kIx,	,
ais	ais	k?	ais
<g/>
,	,	kIx,	,
es1	es1	k4	es1
<g/>
,	,	kIx,	,
eis1	eis1	k4	eis1
<g/>
,	,	kIx,	,
b1	b1	k4	b1
<g/>
,	,	kIx,	,
his1	his1	k4	his1
<g/>
,	,	kIx,	,
fis2	fis2	k4	fis2
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
prstů	prst	k1gInPc2	prst
<g/>
;	;	kIx,	;
např.	např.	kA	např.
druhý	druhý	k4xOgInSc4	druhý
prst	prst	k1gInSc4	prst
<g/>
:	:	kIx,	:
hrají	hrát	k5eAaImIp3nP	hrát
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
tóny	tón	k1gInPc1	tón
h	h	k?	h
<g/>
,	,	kIx,	,
f1	f1	k4	f1
<g/>
,	,	kIx,	,
c1	c1	k4	c1
<g/>
,	,	kIx,	,
g2	g2	k4	g2
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
zvětšené	zvětšený	k2eAgFnPc4d1	zvětšená
a	a	k8xC	a
zmenšené	zmenšený	k2eAgFnPc4d1	zmenšená
varianty	varianta	k1gFnPc4	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hraní	k1gNnSc1	hraní
zvětšeného	zvětšený	k2eAgInSc2d1	zvětšený
tónu	tón	k1gInSc2	tón
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
prstem	prst	k1gInSc7	prst
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
přesah	přesah	k1gInSc1	přesah
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
přehmat	přehmat	k1gInSc1	přehmat
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
označuje	označovat	k5eAaImIp3nS	označovat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
hmatníku	hmatník	k1gInSc2	hmatník
<g/>
;	;	kIx,	;
první	první	k4xOgFnSc1	první
poloha	poloha	k1gFnSc1	poloha
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
poloha	poloha	k1gFnSc1	poloha
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
houslista	houslista	k1gMnSc1	houslista
hraje	hrát	k5eAaImIp3nS	hrát
všemi	všecek	k3xTgInPc7	všecek
prsty	prst	k1gInPc7	prst
o	o	k7c4	o
tón	tón	k1gInSc4	tón
výše	výše	k1gFnSc2	výše
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
ruku	ruka	k1gFnSc4	ruka
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
hmatníku	hmatník	k1gInSc2	hmatník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
hraje	hrát	k5eAaImIp3nS	hrát
prvním	první	k4xOgInSc7	první
prstem	prst	k1gInSc7	prst
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
by	by	kYmCp3nP	by
v	v	k7c6	v
první	první	k4xOgFnSc6	první
poloze	poloha	k1gFnSc6	poloha
hrál	hrát	k5eAaImAgMnS	hrát
prstem	prst	k1gInSc7	prst
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
prstem	prst	k1gInSc7	prst
hraje	hrát	k5eAaImIp3nS	hrát
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
první	první	k4xOgFnSc6	první
poloze	poloha	k1gFnSc6	poloha
hrál	hrát	k5eAaImAgMnS	hrát
prstem	prst	k1gInSc7	prst
třetím	třetí	k4xOgMnSc6	třetí
atd.	atd.	kA	atd.
Čím	co	k3yInSc7	co
vyšší	vysoký	k2eAgFnSc1d2	vyšší
poloha	poloha	k1gFnSc1	poloha
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
stává	stávat	k5eAaImIp3nS	stávat
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
půltóny	půltón	k1gInPc7	půltón
zkracují	zkracovat	k5eAaImIp3nP	zkracovat
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c6	na
kytaře	kytara	k1gFnSc6	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
tón	tón	k1gInSc1	tón
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc2d2	veliký
přesnosti	přesnost	k1gFnSc2	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
houslové	houslový	k2eAgFnSc6d1	houslová
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
a	a	k8xC	a
pátá	pátý	k4xOgFnSc1	pátý
poloha	poloha	k1gFnSc1	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Houslisté	houslista	k1gMnPc1	houslista
používají	používat	k5eAaImIp3nP	používat
často	často	k6eAd1	často
různé	různý	k2eAgFnPc1d1	různá
polohy	poloha	k1gFnPc1	poloha
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
změně	změna	k1gFnSc3	změna
barvy	barva	k1gFnSc2	barva
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tón	tón	k1gInSc4	tón
h1	h1	k4	h1
zahraný	zahraný	k2eAgInSc4d1	zahraný
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
d	d	k?	d
má	mít	k5eAaImIp3nS	mít
temnější	temný	k2eAgInSc1d2	temnější
"	"	kIx"	"
<g/>
violový	violový	k2eAgInSc1d1	violový
<g/>
"	"	kIx"	"
zvuk	zvuk	k1gInSc1	zvuk
než	než	k8xS	než
tentýž	týž	k3xTgInSc1	týž
tón	tón	k1gInSc1	tón
zahraný	zahraný	k2eAgInSc1d1	zahraný
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
a.	a.	k?	a.
Hudební	hudební	k2eAgMnPc1d1	hudební
skladatelé	skladatel	k1gMnPc1	skladatel
těchto	tento	k3xDgInPc2	tento
zvukových	zvukový	k2eAgInPc2d1	zvukový
efektů	efekt	k1gInPc2	efekt
často	často	k6eAd1	často
využívají	využívat	k5eAaImIp3nP	využívat
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
označení	označení	k1gNnSc4	označení
sul	sout	k5eAaImAgMnS	sout
G	G	kA	G
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
daná	daný	k2eAgFnSc1d1	daná
pasáž	pasáž	k1gFnSc1	pasáž
zahrát	zahrát	k5eAaPmF	zahrát
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
g.	g.	k?	g.
Dvojhmat	dvojhmat	k1gInSc4	dvojhmat
znamená	znamenat	k5eAaImIp3nS	znamenat
rozezvučení	rozezvučení	k1gNnSc1	rozezvučení
dvou	dva	k4xCgFnPc2	dva
strun	struna	k1gFnPc2	struna
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
náročnější	náročný	k2eAgNnSc1d2	náročnější
než	než	k8xS	než
hraní	hraň	k1gFnSc7	hraň
jednoho	jeden	k4xCgInSc2	jeden
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
houslista	houslista	k1gMnSc1	houslista
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
přesné	přesný	k2eAgNnSc4d1	přesné
umístění	umístění	k1gNnSc4	umístění
dvou	dva	k4xCgInPc2	dva
prstů	prst	k1gInPc2	prst
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jednoho	jeden	k4xCgInSc2	jeden
prstu	prst	k1gInSc2	prst
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
strunách	struna	k1gFnPc6	struna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trojhmaty	Trojhmat	k1gInPc1	Trojhmat
a	a	k8xC	a
čtyřhmaty	čtyřhmat	k1gInPc1	čtyřhmat
znamenají	znamenat	k5eAaImIp3nP	znamenat
rozezvučení	rozezvučení	k1gNnSc4	rozezvučení
tří	tři	k4xCgFnPc2	tři
resp.	resp.	kA	resp.
čtyř	čtyři	k4xCgNnPc2	čtyři
strun	struna	k1gFnPc2	struna
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Kobylka	kobylka	k1gFnSc1	kobylka
je	být	k5eAaImIp3nS	být
nahoře	nahoře	k6eAd1	nahoře
zaoblená	zaoblený	k2eAgFnSc1d1	zaoblená
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
struny	struna	k1gFnPc1	struna
neleží	ležet	k5eNaImIp3nP	ležet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
a	a	k8xC	a
smyčec	smyčec	k1gInSc1	smyčec
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dotýkat	dotýkat	k5eAaImF	dotýkat
jen	jen	k9	jen
dvou	dva	k4xCgFnPc2	dva
strun	struna	k1gFnPc2	struna
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
rozeznívat	rozeznívat	k5eAaImF	rozeznívat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
houslista	houslista	k1gMnSc1	houslista
musí	muset	k5eAaImIp3nS	muset
držet	držet	k5eAaImF	držet
při	při	k7c6	při
vícehmatech	vícehmat	k1gInPc6	vícehmat
všechny	všechen	k3xTgInPc4	všechen
prsty	prst	k1gInPc4	prst
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
struna	struna	k1gFnSc1	struna
mohla	moct	k5eAaImAgFnS	moct
správně	správně	k6eAd1	správně
chvět	chvět	k5eAaImF	chvět
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přestane	přestat	k5eAaPmIp3nS	přestat
dotýkat	dotýkat	k5eAaImF	dotýkat
smyčcem	smyčec	k1gInSc7	smyčec
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
dvoj-	dvoj-	k?	dvoj-
a	a	k8xC	a
vícehmaty	vícehmat	k1gInPc1	vícehmat
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
s	s	k7c7	s
prázdnými	prázdný	k2eAgFnPc7d1	prázdná
strunami	struna	k1gFnPc7	struna
<g/>
.	.	kIx.	.
</s>
<s>
Vibrato	vibrato	k6eAd1	vibrato
je	být	k5eAaImIp3nS	být
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
houslovou	houslový	k2eAgFnSc7d1	houslová
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
naprosté	naprostý	k2eAgFnSc2d1	naprostá
většiny	většina	k1gFnSc2	většina
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
houslista	houslista	k1gMnSc1	houslista
hraje	hrát	k5eAaImIp3nS	hrát
určitý	určitý	k2eAgInSc4d1	určitý
tón	tón	k1gInSc4	tón
<g/>
,	,	kIx,	,
vibrata	vibrato	k1gNnPc4	vibrato
docílí	docílit	k5eAaPmIp3nP	docílit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jemně	jemně	k6eAd1	jemně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
prstem	prst	k1gInSc7	prst
tam	tam	k6eAd1	tam
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
drobné	drobný	k2eAgFnSc2d1	drobná
změny	změna	k1gFnSc2	změna
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Vibrato	vibrato	k6eAd1	vibrato
dodá	dodat	k5eAaPmIp3nS	dodat
hře	hra	k1gFnSc3	hra
větší	veliký	k2eAgFnSc4d2	veliký
emotivnost	emotivnost	k1gFnSc4	emotivnost
a	a	k8xC	a
barvě	barva	k1gFnSc6	barva
tónu	tón	k1gInSc2	tón
větší	veliký	k2eAgInSc4d2	veliký
"	"	kIx"	"
<g/>
sytost	sytost	k1gFnSc4	sytost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jemné	jemný	k2eAgNnSc1d1	jemné
vibrato	vibrato	k1gNnSc1	vibrato
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
při	při	k7c6	při
přednesu	přednes	k1gInSc6	přednes
barokních	barokní	k2eAgFnPc2d1	barokní
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
používalo	používat	k5eAaImAgNnS	používat
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hraní	k1gNnSc1	hraní
flažoletů	flažolet	k1gInPc2	flažolet
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
náročnějších	náročný	k2eAgFnPc2d2	náročnější
houslových	houslový	k2eAgFnPc2d1	houslová
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgInPc1d1	přirozený
flažolety	flažolet	k1gInPc1	flažolet
využívají	využívat	k5eAaImIp3nP	využívat
vyšších	vysoký	k2eAgFnPc2d2	vyšší
harmonických	harmonický	k2eAgFnPc2d1	harmonická
frekvencí	frekvence	k1gFnPc2	frekvence
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
houslista	houslista	k1gMnSc1	houslista
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
jemně	jemně	k6eAd1	jemně
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
struny	struna	k1gFnPc4	struna
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
polovině	polovina	k1gFnSc6	polovina
<g/>
,	,	kIx,	,
třetině	třetina	k1gFnSc6	třetina
<g/>
,	,	kIx,	,
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
strunou	struna	k1gFnSc7	struna
se	se	k3xPyFc4	se
ale	ale	k9	ale
nesmí	smět	k5eNaImIp3nS	smět
dotknout	dotknout	k5eAaPmF	dotknout
hmatníku	hmatník	k1gInSc3	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
takto	takto	k6eAd1	takto
zahrané	zahraný	k2eAgInPc4d1	zahraný
h2	h2	k4	h2
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
e	e	k0	e
(	(	kIx(	(
<g/>
h2	h2	k4	h2
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
třetině	třetina	k1gFnSc6	třetina
struny	struna	k1gFnSc2	struna
e	e	k0	e
<g/>
)	)	kIx)	)
zní	znět	k5eAaImIp3nS	znět
jako	jako	k9	jako
h3	h3	k4	h3
<g/>
;	;	kIx,	;
takto	takto	k6eAd1	takto
zahrané	zahraný	k2eAgInPc4d1	zahraný
a2	a2	k4	a2
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
e	e	k0	e
(	(	kIx(	(
<g/>
a2	a2	k4	a2
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
struny	struna	k1gFnSc2	struna
e	e	k0	e
<g/>
)	)	kIx)	)
zní	znět	k5eAaImIp3nS	znět
jako	jako	k9	jako
e	e	k0	e
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgInPc1d1	umělý
flažolety	flažolet	k1gInPc1	flažolet
fungují	fungovat	k5eAaImIp3nP	fungovat
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
jako	jako	k9	jako
přirozené	přirozený	k2eAgNnSc1d1	přirozené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
houslista	houslista	k1gMnSc1	houslista
"	"	kIx"	"
<g/>
uměle	uměle	k6eAd1	uměle
<g/>
"	"	kIx"	"
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
délku	délka	k1gFnSc4	délka
struny	struna	k1gFnSc2	struna
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedním	jeden	k4xCgInSc7	jeden
prstem	prst	k1gInSc7	prst
mačká	mačkat	k5eAaImIp3nS	mačkat
určitý	určitý	k2eAgInSc1d1	určitý
tón	tón	k1gInSc1	tón
(	(	kIx(	(
<g/>
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
strunu	struna	k1gFnSc4	struna
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhým	druhý	k4xOgMnSc7	druhý
hraje	hrát	k5eAaImIp3nS	hrát
flažolet	flažolet	k1gInSc4	flažolet
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
hraním	hranit	k5eAaImIp1nS	hranit
fis2	fis2	k4	fis2
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
e	e	k0	e
a	a	k8xC	a
lehkým	lehký	k2eAgInSc7d1	lehký
dotykem	dotyk	k1gInSc7	dotyk
h2	h2	k4	h2
(	(	kIx(	(
<g/>
ve	v	k7c6	v
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
takto	takto	k6eAd1	takto
"	"	kIx"	"
<g/>
zkrácené	zkrácený	k2eAgFnPc4d1	zkrácená
<g/>
"	"	kIx"	"
struny	struna	k1gFnPc4	struna
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
houslí	housle	k1gFnPc2	housle
vychází	vycházet	k5eAaImIp3nS	vycházet
tón	tón	k1gInSc1	tón
fis	fis	k1gNnSc1	fis
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
vydávají	vydávat	k5eAaPmIp3nP	vydávat
silnější	silný	k2eAgInSc4d2	silnější
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
smyčcem	smyčec	k1gInSc7	smyčec
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
přítlakem	přítlak	k1gInSc7	přítlak
na	na	k7c4	na
struny	struna	k1gFnPc4	struna
nebo	nebo	k8xC	nebo
rychlejšími	rychlý	k2eAgInPc7d2	rychlejší
tahy	tah	k1gInPc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
barvu	barva	k1gFnSc4	barva
tónu	tón	k1gInSc2	tón
a	a	k8xC	a
hlasitost	hlasitost	k1gFnSc1	hlasitost
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitá	důležitý	k2eAgFnSc1d1	důležitá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
smyčce	smyčec	k1gInSc2	smyčec
od	od	k7c2	od
kobylky	kobylka	k1gFnSc2	kobylka
<g/>
:	:	kIx,	:
při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
blízko	blízko	k7c2	blízko
kobylky	kobylka	k1gFnSc2	kobylka
se	se	k3xPyFc4	se
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
vyšší	vysoký	k2eAgInPc1d2	vyšší
harmonické	harmonický	k2eAgInPc1d1	harmonický
tóny	tón	k1gInPc1	tón
a	a	k8xC	a
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
ostřejší	ostrý	k2eAgInSc1d2	ostřejší
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
smyčcem	smyčec	k1gInSc7	smyčec
od	od	k7c2	od
kobylky	kobylka	k1gFnSc2	kobylka
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
zvuk	zvuk	k1gInSc1	zvuk
jemnější	jemný	k2eAgInSc1d2	jemnější
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
smyčcových	smyčcový	k2eAgFnPc2d1	smyčcová
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
není	být	k5eNaImIp3nS	být
vyčerpávající	vyčerpávající	k2eAgInSc1d1	vyčerpávající
<g/>
:	:	kIx,	:
détaché	détaché	k1gNnSc1	détaché
–	–	k?	–
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
noty	nota	k1gFnPc1	nota
mají	mít	k5eAaImIp3nP	mít
hrát	hrát	k5eAaImF	hrát
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Zastavení	zastavení	k1gNnSc1	zastavení
smyčce	smyčec	k1gInSc2	smyčec
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
utlumí	utlumit	k5eAaPmIp3nS	utlumit
vibrace	vibrace	k1gFnPc4	vibrace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
malá	malý	k2eAgFnSc1d1	malá
pomlka	pomlka	k1gFnSc1	pomlka
mezi	mezi	k7c7	mezi
notami	nota	k1gFnPc7	nota
<g/>
.	.	kIx.	.
</s>
<s>
Détaché	détaché	k1gNnSc1	détaché
traîné	traîná	k1gFnSc2	traîná
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
hrát	hrát	k5eAaImF	hrát
détaché	détaché	k1gNnSc4	détaché
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nevnikaly	vnikat	k5eNaImAgInP	vnikat
žádné	žádný	k3yNgFnPc4	žádný
pomlky	pomlka	k1gFnPc4	pomlka
mezi	mezi	k7c7	mezi
notami	nota	k1gFnPc7	nota
<g/>
.	.	kIx.	.
martelé	martelý	k2eAgNnSc1d1	martelý
–	–	k?	–
détaché	détaché	k1gNnSc1	détaché
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
silnými	silný	k2eAgInPc7d1	silný
tahy	tah	k1gInPc7	tah
smyčcem	smyčec	k1gInSc7	smyčec
collé	collý	k2eAgNnSc4d1	collý
–	–	k?	–
smyk	smyk	k1gInSc4	smyk
začíná	začínat	k5eAaImIp3nS	začínat
při	při	k7c6	při
silném	silný	k2eAgInSc6d1	silný
tlaku	tlak	k1gInSc6	tlak
na	na	k7c4	na
smyčec	smyčec	k1gInSc4	smyčec
legato	legato	k6eAd1	legato
–	–	k?	–
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
smyk	smyk	k1gInSc4	smyk
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
více	hodně	k6eAd2	hodně
tónů	tón	k1gInPc2	tón
po	po	k7c6	po
sobě	se	k3xPyFc3	se
staccato	staccato	k1gNnSc1	staccato
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
velmi	velmi	k6eAd1	velmi
krátký	krátký	k2eAgInSc4d1	krátký
smyk	smyk	k1gInSc4	smyk
spiccato	spiccato	k6eAd1	spiccato
–	–	k?	–
skákavě	skákavě	k6eAd1	skákavě
-	-	kIx~	-
smyčcem	smyčec	k1gInSc7	smyčec
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
každá	každý	k3xTgFnSc1	každý
nota	nota	k1gFnSc1	nota
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
smyk	smyk	k1gInSc1	smyk
začíná	začínat	k5eAaImIp3nS	začínat
dopadem	dopad	k1gInSc7	dopad
smyčce	smyčec	k1gInSc2	smyčec
na	na	k7c4	na
strunu	struna	k1gFnSc4	struna
<g/>
,	,	kIx,	,
následný	následný	k2eAgInSc4d1	následný
odraz	odraz	k1gInSc4	odraz
(	(	kIx(	(
<g/>
odskok	odskok	k1gInSc1	odskok
<g/>
)	)	kIx)	)
od	od	k7c2	od
struny	struna	k1gFnSc2	struna
ukončí	ukončit	k5eAaPmIp3nS	ukončit
tón	tón	k1gInSc4	tón
sautillé	sautillý	k2eAgFnSc2d1	sautillý
–	–	k?	–
podobné	podobný	k2eAgFnPc4d1	podobná
spiccatu	spiccata	k1gFnSc4	spiccata
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
smyčec	smyčec	k1gInSc1	smyčec
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
struně	struna	k1gFnSc6	struna
poskočí	poskočit	k5eAaPmIp3nS	poskočit
několikrát	několikrát	k6eAd1	několikrát
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vahou	váha	k1gFnSc7	váha
<g/>
,	,	kIx,	,
zazní	zaznět	k5eAaImIp3nS	zaznět
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
za	za	k7c7	za
sebou	se	k3xPyFc7	se
několikrát	několikrát	k6eAd1	několikrát
týž	týž	k3xTgInSc4	týž
tón	tón	k1gInSc4	tón
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
rychlý	rychlý	k2eAgInSc1d1	rychlý
sled	sled	k1gInSc1	sled
tónů	tón	k1gInPc2	tón
col	cola	k1gFnPc2	cola
legno	legno	k6eAd1	legno
–	–	k?	–
smyčcem	smyčec	k1gInSc7	smyčec
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
struny	struna	k1gFnSc2	struna
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
prutem	prut	k1gInSc7	prut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ne	ne	k9	ne
žíněmi	žíně	k1gFnPc7	žíně
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
pizzicato	pizzicato	k1gNnSc1	pizzicato
<g/>
.	.	kIx.	.
</s>
<s>
Pizzicato	pizzicato	k6eAd1	pizzicato
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
struny	struna	k1gFnSc2	struna
rozezvučí	rozezvučet	k5eAaPmIp3nS	rozezvučet
brnkáním	brnkání	k1gNnSc7	brnkání
prstu	prst	k1gInSc2	prst
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
skladbách	skladba	k1gFnPc6	skladba
se	se	k3xPyFc4	se
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
pizzicato	pizzicato	k6eAd1	pizzicato
levou	levý	k2eAgFnSc7d1	levá
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
malého	malý	k2eAgInSc2d1	malý
kovového	kovový	k2eAgInSc2d1	kovový
<g/>
,	,	kIx,	,
gumového	gumový	k2eAgMnSc2d1	gumový
nebo	nebo	k8xC	nebo
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
dusítka	dusítko	k1gNnSc2	dusítko
na	na	k7c4	na
kobylku	kobylka	k1gFnSc4	kobylka
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
potlačí	potlačit	k5eAaPmIp3nS	potlačit
některé	některý	k3yIgFnPc4	některý
vyšší	vysoký	k2eAgFnPc4d2	vyšší
frekvence	frekvence	k1gFnPc4	frekvence
a	a	k8xC	a
housle	housle	k1gFnPc4	housle
mají	mít	k5eAaImIp3nP	mít
tišší	tichý	k2eAgInPc1d2	tišší
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
jemnější	jemný	k2eAgInSc4d2	jemnější
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
než	než	k8xS	než
v	v	k7c6	v
sólové	sólový	k2eAgFnSc6d1	sólová
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
tohoto	tento	k3xDgInSc2	tento
efektu	efekt	k1gInSc2	efekt
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
orchestrálních	orchestrální	k2eAgFnPc6d1	orchestrální
skladbách	skladba	k1gFnPc6	skladba
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc6	označení
con	con	k?	con
sord	sord	k1gInSc1	sord
<g/>
.	.	kIx.	.
–	–	k?	–
z	z	k7c2	z
italského	italský	k2eAgNnSc2d1	italské
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
tlumítko	tlumítko	k1gNnSc4	tlumítko
<g/>
:	:	kIx,	:
sordino	sordina	k1gFnSc5	sordina
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
masivnější	masivní	k2eAgFnSc1d2	masivnější
"	"	kIx"	"
<g/>
hotelová	hotelový	k2eAgFnSc1d1	hotelová
<g/>
"	"	kIx"	"
dusítka	dusítko	k1gNnPc1	dusítko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
co	co	k9	co
největšímu	veliký	k2eAgNnSc3d3	veliký
zeslabení	zeslabení	k1gNnSc3	zeslabení
zvuku	zvuk	k1gInSc2	zvuk
houslí	housle	k1gFnPc2	housle
při	při	k7c6	při
cvičení	cvičení	k1gNnSc6	cvičení
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
abychom	aby	kYmCp1nP	aby
svou	svůj	k3xOyFgFnSc7	svůj
hrou	hra	k1gFnSc7	hra
příliš	příliš	k6eAd1	příliš
nerušili	rušit	k5eNaImAgMnP	rušit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
hotelové	hotelový	k2eAgMnPc4d1	hotelový
hosty	host	k1gMnPc4	host
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
ladit	ladit	k5eAaImF	ladit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
stupních	stupeň	k1gInPc6	stupeň
<g/>
:	:	kIx,	:
kolíčky	kolíček	k1gInPc1	kolíček
nebo	nebo	k8xC	nebo
i	i	k9	i
dolaďovači	dolaďovač	k1gInSc3	dolaďovač
na	na	k7c6	na
struníku	struník	k1gInSc3	struník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pohodlné	pohodlný	k2eAgNnSc4d1	pohodlné
a	a	k8xC	a
přesné	přesný	k2eAgNnSc4d1	přesné
ladění	ladění	k1gNnSc4	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
housle	housle	k1gFnPc1	housle
ladily	ladit	k5eAaImAgFnP	ladit
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
kolíčků	kolíček	k1gInPc2	kolíček
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
houslistů	houslista	k1gMnPc2	houslista
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
dolaďovač	dolaďovač	k1gInSc4	dolaďovač
alespoň	alespoň	k9	alespoň
na	na	k7c6	na
struně	struna	k1gFnSc6	struna
e	e	k0	e
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
napnutá	napnutý	k2eAgFnSc1d1	napnutá
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
i	i	k9	i
nejhůře	zle	k6eAd3	zle
ladí	ladit	k5eAaImIp3nP	ladit
<g/>
.	.	kIx.	.
</s>
<s>
Struna	struna	k1gFnSc1	struna
a	a	k8xC	a
se	se	k3xPyFc4	se
ladí	ladit	k5eAaImIp3nP	ladit
nejdříve	dříve	k6eAd3	dříve
(	(	kIx(	(
<g/>
komorní	komorní	k2eAgFnSc1d1	komorní
a1	a1	k4	a1
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
440	[number]	k4	440
až	až	k9	až
442	[number]	k4	442
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
struny	struna	k1gFnPc1	struna
se	se	k3xPyFc4	se
ladí	ladit	k5eAaImIp3nP	ladit
podle	podle	k7c2	podle
sluchu	sluch	k1gInSc2	sluch
v	v	k7c6	v
čistých	čistý	k2eAgFnPc6d1	čistá
kvintách	kvinta	k1gFnPc6	kvinta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
některé	některý	k3yIgFnPc1	některý
struny	struna	k1gFnPc1	struna
o	o	k7c4	o
tón	tón	k1gInSc4	tón
podladit	podladit	k5eAaPmF	podladit
nebo	nebo	k8xC	nebo
přeladit	přeladit	k5eAaPmF	přeladit
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
scordatura	scordatura	k1gFnSc1	scordatura
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
jí	on	k3xPp3gFnSc3	on
některé	některý	k3yIgFnPc4	některý
skladby	skladba	k1gFnPc4	skladba
klasické	klasický	k2eAgFnSc2d1	klasická
i	i	k8xC	i
folkové	folkový	k2eAgFnSc2d1	folková
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgInPc1	všechen
hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc1	housle
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
pečlivé	pečlivý	k2eAgNnSc4d1	pečlivé
zacházení	zacházení	k1gNnSc4	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
jejich	jejich	k3xOp3gFnSc1	jejich
tónová	tónový	k2eAgFnSc1d1	tónová
kvalita	kvalita	k1gFnSc1	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
stírání	stírání	k1gNnSc1	stírání
zbytků	zbytek	k1gInPc2	zbytek
kalafuny	kalafuna	k1gFnSc2	kalafuna
z	z	k7c2	z
laku	lak	k1gInSc2	lak
mezi	mezi	k7c7	mezi
hmatníkem	hmatník	k1gInSc7	hmatník
a	a	k8xC	a
kobylkou	kobylka	k1gFnSc7	kobylka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
době	doba	k1gFnSc6	doba
kalafuna	kalafuna	k1gFnSc1	kalafuna
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
lakem	lak	k1gInSc7	lak
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
jeho	jeho	k3xOp3gFnSc4	jeho
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Kalafunu	kalafuna	k1gFnSc4	kalafuna
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
také	také	k9	také
často	často	k6eAd1	často
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
ze	z	k7c2	z
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
zvukové	zvukový	k2eAgFnPc4d1	zvuková
charakteristiky	charakteristika	k1gFnPc4	charakteristika
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Líh	líh	k1gInSc1	líh
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc1d1	dobré
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
kalafuny	kalafuna	k1gFnSc2	kalafuna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesmí	smět	k5eNaImIp3nS	smět
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
lakem	lak	k1gInSc7	lak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ladění	ladění	k1gNnSc6	ladění
nebo	nebo	k8xC	nebo
výměně	výměna	k1gFnSc6	výměna
strun	struna	k1gFnPc2	struna
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
může	moct	k5eAaImIp3nS	moct
vychýlit	vychýlit	k5eAaPmF	vychýlit
nebo	nebo	k8xC	nebo
posunout	posunout	k5eAaPmF	posunout
kobylka	kobylka	k1gFnSc1	kobylka
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
postupnému	postupný	k2eAgNnSc3d1	postupné
vychylování	vychylování	k1gNnSc3	vychylování
ke	k	k7c3	k
hmatníku	hmatník	k1gInSc3	hmatník
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
dopředu	dopředu	k6eAd1	dopředu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
kobylka	kobylka	k1gFnSc1	kobylka
začne	začít	k5eAaPmIp3nS	začít
prohýbat	prohýbat	k5eAaImF	prohýbat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
její	její	k3xOp3gFnSc4	její
životnost	životnost	k1gFnSc4	životnost
a	a	k8xC	a
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
zvuk	zvuk	k1gInSc4	zvuk
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zadní	zadní	k2eAgFnSc1d1	zadní
rovina	rovina	k1gFnSc1	rovina
kobylky	kobylka	k1gFnSc2	kobylka
svírá	svírat	k5eAaImIp3nS	svírat
pravý	pravý	k2eAgInSc4d1	pravý
úhel	úhel	k1gInSc4	úhel
s	s	k7c7	s
vrchní	vrchní	k2eAgFnSc7d1	vrchní
deskou	deska	k1gFnSc7	deska
houslí	housle	k1gFnPc2	housle
(	(	kIx(	(
<g/>
vlivem	vliv	k1gInSc7	vliv
opracování	opracování	k1gNnSc1	opracování
kobylky	kobylka	k1gFnSc2	kobylka
do	do	k7c2	do
zužujícího	zužující	k2eAgMnSc2d1	zužující
se	se	k3xPyFc4	se
tvaru	tvar	k1gInSc2	tvar
to	ten	k3xDgNnSc4	ten
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
boku	bok	k1gInSc2	bok
potom	potom	k6eAd1	potom
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kobylka	kobylka	k1gFnSc1	kobylka
mírně	mírně	k6eAd1	mírně
zakloněná	zakloněný	k2eAgFnSc1d1	zakloněná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kobylka	kobylka	k1gFnSc1	kobylka
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
vrchní	vrchní	k2eAgFnSc4d1	vrchní
desku	deska	k1gFnSc4	deska
houslí	houslit	k5eAaImIp3nS	houslit
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
mezi	mezi	k7c7	mezi
zářezy	zářez	k1gInPc7	zářez
na	na	k7c6	na
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
stranách	strana	k1gFnPc6	strana
otvorů	otvor	k1gInPc2	otvor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
F	F	kA	F
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zamýšlená	zamýšlený	k2eAgNnPc4d1	zamýšlené
místa	místo	k1gNnPc4	místo
dotyku	dotyk	k1gInSc6	dotyk
strun	struna	k1gFnPc2	struna
s	s	k7c7	s
kobylkou	kobylka	k1gFnSc7	kobylka
ještě	ještě	k9	ještě
před	před	k7c7	před
natažením	natažení	k1gNnSc7	natažení
strun	struna	k1gFnPc2	struna
naneseme	nanést	k5eAaPmIp1nP	nanést
vrstvu	vrstva	k1gFnSc4	vrstva
grafitu	grafit	k1gInSc2	grafit
(	(	kIx(	(
<g/>
stačí	stačit	k5eAaBmIp3nS	stačit
použít	použít	k5eAaPmF	použít
obyčejnou	obyčejný	k2eAgFnSc4d1	obyčejná
měkkou	měkký	k2eAgFnSc4d1	měkká
tužku	tužka	k1gFnSc4	tužka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
potom	potom	k6eAd1	potom
déle	dlouho	k6eAd2	dlouho
vydrží	vydržet	k5eAaPmIp3nS	vydržet
(	(	kIx(	(
<g/>
lépe	dobře	k6eAd2	dobře
kloužou	klouzat	k5eAaImIp3nP	klouzat
a	a	k8xC	a
nedochází	docházet	k5eNaImIp3nS	docházet
tak	tak	k9	tak
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
opotřebování	opotřebování	k1gNnSc3	opotřebování
<g/>
)	)	kIx)	)
a	a	k8xC	a
rovnání	rovnání	k1gNnSc1	rovnání
kobylky	kobylka	k1gFnSc2	kobylka
je	být	k5eAaImIp3nS	být
snazší	snadný	k2eAgMnSc1d2	snazší
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
kolíčky	kolíček	k1gInPc1	kolíček
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
jejich	jejich	k3xOp3gNnSc4	jejich
namazání	namazání	k1gNnSc4	namazání
bílou	bílý	k2eAgFnSc7d1	bílá
křídou	křída	k1gFnSc7	křída
nebo	nebo	k8xC	nebo
lépe	dobře	k6eAd2	dobře
speciální	speciální	k2eAgFnSc7d1	speciální
tyčinkou	tyčinka	k1gFnSc7	tyčinka
k	k	k7c3	k
mazání	mazání	k1gNnSc3	mazání
kolíčků	kolíček	k1gInPc2	kolíček
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
častém	častý	k2eAgNnSc6d1	časté
hraní	hraní	k1gNnSc6	hraní
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
měnit	měnit	k5eAaImF	měnit
struny	struna	k1gFnPc4	struna
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
a	a	k8xC	a
opotřebované	opotřebovaný	k2eAgFnPc1d1	opotřebovaná
struny	struna	k1gFnPc1	struna
(	(	kIx(	(
<g/>
nemusí	muset	k5eNaImIp3nS	muset
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
být	být	k5eAaImF	být
přímo	přímo	k6eAd1	přímo
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozladění	rozladění	k1gNnSc3	rozladění
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
si	se	k3xPyFc3	se
povšimneme	povšimnout	k5eAaPmIp1nP	povšimnout
když	když	k8xS	když
struny	struna	k1gFnPc1	struna
vyměníme	vyměnit	k5eAaPmIp1nP	vyměnit
–	–	k?	–
po	po	k7c6	po
výměně	výměna	k1gFnSc6	výměna
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
položíme	položit	k5eAaPmIp1nP	položit
<g/>
-li	i	k?	-li
prst	prst	k1gInSc4	prst
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
které	který	k3yRgInPc4	který
jsme	být	k5eAaImIp1nP	být
byli	být	k5eAaImAgMnP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
ho	on	k3xPp3gNnSc4	on
pokládat	pokládat	k5eAaImF	pokládat
ozve	ozvat	k5eAaPmIp3nS	ozvat
nepatrně	nepatrně	k6eAd1	nepatrně
jiný	jiný	k2eAgMnSc1d1	jiný
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
nižší	nízký	k2eAgInSc1d2	nižší
<g/>
)	)	kIx)	)
tón	tón	k1gInSc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
Praskne	prasknout	k5eAaPmIp3nS	prasknout
<g/>
-li	i	k?	-li
nám	my	k3xPp1nPc3	my
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
žíně	žíně	k1gFnSc2	žíně
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
ji	on	k3xPp3gFnSc4	on
odstřihnout	odstřihnout	k5eAaPmF	odstřihnout
–	–	k?	–
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	k9	aby
konce	konec	k1gInSc2	konec
stále	stále	k6eAd1	stále
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
upevněny	upevnit	k5eAaPmNgFnP	upevnit
ve	v	k7c6	v
smyčci	smyčec	k1gInSc6	smyčec
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
je	on	k3xPp3gMnPc4	on
nevytrháváme	vytrhávat	k5eNaImIp1nP	vytrhávat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nadměrnému	nadměrný	k2eAgNnSc3d1	nadměrné
uvolňování	uvolňování	k1gNnSc3	uvolňování
žíní	žíně	k1gFnPc2	žíně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
pro	pro	k7c4	pro
návštěvu	návštěva	k1gFnSc4	návštěva
houslaře	houslař	k1gMnSc2	houslař
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
potah	potah	k1gInSc4	potah
smyčce	smyčec	k1gInSc2	smyčec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žíní	žíně	k1gFnPc2	žíně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
opotřebení	opotřebení	k1gNnSc3	opotřebení
–	–	k?	–
povrch	povrch	k1gInSc1	povrch
žíní	žíně	k1gFnPc2	žíně
se	se	k3xPyFc4	se
zahladí	zahladit	k5eAaPmIp3nS	zahladit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
též	též	k9	též
důvod	důvod	k1gInSc4	důvod
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
potah	potah	k1gInSc4	potah
<g/>
.	.	kIx.	.
</s>
<s>
Žíně	žíně	k1gFnSc1	žíně
se	se	k3xPyFc4	se
opotřebí	opotřebit	k5eAaPmIp3nS	opotřebit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
hraní	hranit	k5eAaImIp3nS	hranit
během	běh	k1gInSc7	běh
i	i	k9	i
třeba	třeba	k6eAd1	třeba
jen	jen	k9	jen
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
výměny	výměna	k1gFnSc2	výměna
poznáme	poznat	k5eAaPmIp1nP	poznat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
položíme	položit	k5eAaPmIp1nP	položit
smyčec	smyčec	k1gInSc4	smyčec
před	před	k7c7	před
nakalafunováním	nakalafunování	k1gNnSc7	nakalafunování
na	na	k7c4	na
struny	struna	k1gFnPc4	struna
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
držíme	držet	k5eAaImIp1nP	držet
pod	pod	k7c7	pod
bradou	brada	k1gFnSc7	brada
mírně	mírně	k6eAd1	mírně
skloněny	skloněn	k2eAgInPc1d1	skloněn
(	(	kIx(	(
<g/>
ruka	ruka	k1gFnSc1	ruka
na	na	k7c6	na
hmatníku	hmatník	k1gInSc6	hmatník
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloze	poloha	k1gFnSc6	poloha
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
o	o	k7c4	o
15	[number]	k4	15
cm	cm	kA	cm
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nám	my	k3xPp1nPc3	my
smyčec	smyčec	k1gInSc1	smyčec
sám	sám	k3xTgInSc1	sám
sklouzává	sklouzávat	k5eAaImIp3nS	sklouzávat
dolů	dolů	k6eAd1	dolů
po	po	k7c6	po
strunách	struna	k1gFnPc6	struna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ho	on	k3xPp3gMnSc4	on
nechat	nechat	k5eAaPmF	nechat
potáhnout	potáhnout	k5eAaPmF	potáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Smyčec	smyčec	k1gInSc1	smyčec
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
hry	hra	k1gFnSc2	hra
povolujeme	povolovat	k5eAaImIp1nP	povolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neochabovala	ochabovat	k5eNaImAgFnS	ochabovat
pružnost	pružnost	k1gFnSc1	pružnost
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
hůlky	hůlka	k1gFnSc2	hůlka
(	(	kIx(	(
<g/>
prutu	prut	k1gInSc2	prut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Houslím	housle	k1gFnPc3	housle
neprospívají	prospívat	k5eNaImIp3nP	prospívat
prudké	prudký	k2eAgInPc4d1	prudký
výkyvy	výkyv	k1gInPc4	výkyv
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc2	vlhkost
(	(	kIx(	(
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
prasknutí	prasknutí	k1gNnSc1	prasknutí
desek	deska	k1gFnPc2	deska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
přechovávají	přechovávat	k5eAaImIp3nP	přechovávat
v	v	k7c6	v
termoizolačních	termoizolační	k2eAgNnPc6d1	termoizolační
pouzdrech	pouzdro	k1gNnPc6	pouzdro
s	s	k7c7	s
měřiči	měřič	k1gMnPc7	měřič
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
se	se	k3xPyFc4	se
použijí	použít	k5eAaPmIp3nP	použít
zvlhčovače	zvlhčovač	k1gInPc1	zvlhčovač
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
houslí	housle	k1gFnPc2	housle
je	být	k5eAaImIp3nS	být
středověká	středověký	k2eAgFnSc1d1	středověká
rubeba	rubeba	k1gFnSc1	rubeba
(	(	kIx(	(
<g/>
rebec	rebec	k1gInSc1	rebec
<g/>
,	,	kIx,	,
rebeka	rebeka	k1gFnSc1	rebeka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nástupce	nástupce	k1gMnSc1	nástupce
arabského	arabský	k2eAgInSc2d1	arabský
rabábu	rabáb	k1gInSc2	rabáb
<g/>
,	,	kIx,	,
fidéla	fidélo	k1gNnSc2	fidélo
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
lira	lira	k1gFnSc1	lira
da	da	k?	da
braccio	braccio	k6eAd1	braccio
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
housle	housle	k1gFnPc1	housle
začaly	začít	k5eAaPmAgFnP	začít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikat	vznikat	k5eAaImF	vznikat
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pak	pak	k6eAd1	pak
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikly	proniknout	k5eAaPmAgFnP	proniknout
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
se	se	k3xPyFc4	se
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc4	jejich
tvar	tvar	k1gInSc4	tvar
můžeme	moct	k5eAaImIp1nP	moct
vysledovat	vysledovat	k5eAaPmF	vysledovat
z	z	k7c2	z
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
kreseb	kresba	k1gFnPc2	kresba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
moderní	moderní	k2eAgMnSc1d1	moderní
housle	housle	k1gFnPc4	housle
postavil	postavit	k5eAaPmAgMnS	postavit
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
Andrea	Andrea	k1gFnSc1	Andrea
Amati	Amať	k1gFnSc2	Amať
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
od	od	k7c2	od
rodu	rod	k1gInSc2	rod
Medicejských	Medicejský	k2eAgFnPc2d1	Medicejská
–	–	k?	–
byl	být	k5eAaImAgMnS	být
požádán	požádat	k5eAaPmNgMnS	požádat
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vhodný	vhodný	k2eAgInSc4d1	vhodný
pro	pro	k7c4	pro
pouliční	pouliční	k2eAgMnPc4d1	pouliční
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
housle	housle	k1gFnPc1	housle
rychle	rychle	k6eAd1	rychle
získaly	získat	k5eAaPmAgFnP	získat
oblibu	obliba	k1gFnSc4	obliba
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
společenskými	společenský	k2eAgFnPc7d1	společenská
vrstvami	vrstva	k1gFnPc7	vrstva
a	a	k8xC	a
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
celé	celá	k1gFnPc4	celá
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc4d3	nejstarší
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
housle	housle	k1gFnPc4	housle
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
taktéž	taktéž	k?	taktéž
Andrea	Andrea	k1gFnSc1	Andrea
Amati	Amať	k1gFnSc2	Amať
<g/>
,	,	kIx,	,
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejznámější	známý	k2eAgFnPc4d3	nejznámější
housle	housle	k1gFnPc4	housle
"	"	kIx"	"
<g/>
Le	Le	k1gFnSc1	Le
Messie	Messie	k1gFnSc1	Messie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Salabue	Salabue	k1gFnSc1	Salabue
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1716	[number]	k4	1716
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivare	k1gFnSc4	Stradivare
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
nehrálo	hrát	k5eNaImAgNnS	hrát
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
v	v	k7c4	v
Ashmolean	Ashmolean	k1gInSc4	Ashmolean
Museum	museum	k1gNnSc4	museum
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
houslaři	houslař	k1gMnPc1	houslař
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
italská	italský	k2eAgFnSc1d1	italská
houslařská	houslařský	k2eAgFnSc1d1	houslařská
rodina	rodina	k1gFnSc1	rodina
Amati	Amať	k1gFnSc2	Amať
<g/>
:	:	kIx,	:
Andrea	Andrea	k1gFnSc1	Andrea
Amati	Amať	k1gFnSc2	Amať
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1577	[number]	k4	1577
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonio	Antonio	k1gMnSc1	Antonio
Amati	Amať	k1gFnSc2	Amať
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1607	[number]	k4	1607
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hieronymous	Hieronymous	k1gMnSc1	Hieronymous
Amati	Amať	k1gFnSc2	Amať
I	I	kA	I
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1561	[number]	k4	1561
<g/>
–	–	k?	–
<g/>
1630	[number]	k4	1630
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nicolo	Nicola	k1gFnSc5	Nicola
Amati	Amati	k1gNnPc1	Amati
(	(	kIx(	(
<g/>
1596	[number]	k4	1596
<g/>
–	–	k?	–
<g/>
1684	[number]	k4	1684
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hieronymous	Hieronymous	k1gMnSc1	Hieronymous
Amati	Amať	k1gFnSc2	Amať
II	II	kA	II
(	(	kIx(	(
<g/>
1649	[number]	k4	1649
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
italská	italský	k2eAgFnSc1d1	italská
houslařská	houslařský	k2eAgFnSc1d1	houslařská
rodina	rodina	k1gFnSc1	rodina
Guarneri	Guarner	k1gFnSc2	Guarner
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
Guarneri	Guarner	k1gFnSc2	Guarner
(	(	kIx(	(
<g/>
1626	[number]	k4	1626
<g/>
–	–	k?	–
<g/>
1698	[number]	k4	1698
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pietro	Pietro	k1gNnSc1	Pietro
Giovanni	Giovann	k1gMnPc1	Giovann
Guarneri	Guarner	k1gFnSc2	Guarner
(	(	kIx(	(
<g/>
Pietro	Pietro	k1gNnSc1	Pietro
da	da	k?	da
Mantova	Mantova	k1gFnSc1	Mantova
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1655	[number]	k4	1655
<g/>
–	–	k?	–
<g/>
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Giovanni	Giovanň	k1gMnSc5	Giovanň
Battista	Battista	k1gMnSc1	Battista
Guarneri	Guarner	k1gFnSc2	Guarner
(	(	kIx(	(
<g/>
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
filius	filius	k1gMnSc1	filius
Andreae	Andrea	k1gFnSc2	Andrea
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1666	[number]	k4	1666
<g/>
–	–	k?	–
<g/>
1739	[number]	k4	1739
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pietro	Pietro	k1gNnSc1	Pietro
Guarneri	Guarner	k1gFnSc2	Guarner
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Pietro	Pietro	k1gNnSc4	Pietro
da	da	k?	da
Venezia	Venezia	k1gFnSc1	Venezia
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1695	[number]	k4	1695
<g/>
–	–	k?	–
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bartolomeo	Bartolomea	k1gMnSc5	Bartolomea
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Guarneri	Guarner	k1gMnSc5	Guarner
(	(	kIx(	(
<g/>
Guarnerius	Guarnerius	k1gInSc1	Guarnerius
del	del	k?	del
Gesu	Gesus	k1gInSc2	Gesus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1698	[number]	k4	1698
<g/>
–	–	k?	–
<g/>
1744	[number]	k4	1744
<g/>
)	)	kIx)	)
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
<g/>
–	–	k?	–
<g/>
1737	[number]	k4	1737
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Cremoně	Cremona	k1gFnSc6	Cremona
Jakob	Jakob	k1gMnSc1	Jakob
Stainer	Stainer	k1gMnSc1	Stainer
(	(	kIx(	(
<g/>
1617	[number]	k4	1617
<g/>
–	–	k?	–
<g/>
1683	[number]	k4	1683
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
tyrolském	tyrolský	k2eAgInSc6d1	tyrolský
Absamu	Absam	k1gInSc6	Absam
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
Battista	Battista	k1gMnSc1	Battista
Guadagnini	Guadagnin	k2eAgMnPc1d1	Guadagnin
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
-	-	kIx~	-
<g/>
1786	[number]	k4	1786
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
benátský	benátský	k2eAgMnSc1d1	benátský
houslař	houslař	k1gMnSc1	houslař
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Piacenze	Piacenz	k1gInSc6	Piacenz
<g/>
,	,	kIx,	,
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
Parmě	Parma	k1gFnSc6	Parma
<g/>
,	,	kIx,	,
Turíně	Turín	k1gInSc6	Turín
Tvar	tvar	k1gInSc4	tvar
houslí	houslit	k5eAaImIp3nP	houslit
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
prakticky	prakticky	k6eAd1	prakticky
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
,	,	kIx,	,
udály	udát	k5eAaPmAgFnP	udát
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnPc1d1	malá
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
se	se	k3xPyFc4	se
o	o	k7c4	o
něco	něco	k3yInSc4	něco
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
krk	krk	k1gInSc1	krk
a	a	k8xC	a
hmatník	hmatník	k1gInSc1	hmatník
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hraní	hraní	k1gNnSc4	hraní
vyšších	vysoký	k2eAgInPc2d2	vyšší
tónů	tón	k1gInPc2	tón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc4	housle
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
význačnými	význačný	k2eAgMnPc7d1	význačný
houslaři	houslař	k1gMnPc7	houslař
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nepřekonané	překonaný	k2eNgFnSc6d1	nepřekonaná
v	v	k7c6	v
kráse	krása	k1gFnSc6	krása
tónu	tón	k1gInSc2	tón
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
dřeva	dřevo	k1gNnSc2	dřevo
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
také	také	k9	také
o	o	k7c4	o
haló	haló	k0	haló
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
houslí	housle	k1gFnPc2	housle
(	(	kIx(	(
<g/>
nejprve	nejprve	k6eAd1	nejprve
manufakturní	manufakturní	k2eAgInSc1d1	manufakturní
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
tovární	tovární	k2eAgInPc4d1	tovární
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
pronikavě	pronikavě	k6eAd1	pronikavě
zrychlila	zrychlit	k5eAaPmAgFnS	zrychlit
a	a	k8xC	a
zlevnila	zlevnit	k5eAaPmAgFnS	zlevnit
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
takovéto	takovýto	k3xDgFnPc4	takovýto
housle	housle	k1gFnPc4	housle
zdaleka	zdaleka	k6eAd1	zdaleka
nemohou	moct	k5eNaImIp3nP	moct
měřit	měřit	k5eAaImF	měřit
s	s	k7c7	s
mistrovskými	mistrovský	k2eAgInPc7d1	mistrovský
nástroji	nástroj	k1gInPc7	nástroj
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
víceméně	víceméně	k9	víceméně
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
školní	školní	k2eAgInPc4d1	školní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Manufakturní	manufakturní	k2eAgInPc1d1	manufakturní
a	a	k8xC	a
tovární	tovární	k2eAgInPc1d1	tovární
nástroje	nástroj	k1gInPc1	nástroj
vznikaly	vznikat	k5eAaImAgInP	vznikat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
producentem	producent	k1gMnSc7	producent
nástrojů	nástroj	k1gInPc2	nástroj
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
například	například	k6eAd1	například
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
jsou	být	k5eAaImIp3nP	být
úzce	úzko	k6eAd1	úzko
spjaty	spjat	k2eAgInPc1d1	spjat
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
evropské	evropský	k2eAgFnSc2d1	Evropská
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
jim	on	k3xPp3gMnPc3	on
věnovala	věnovat	k5eAaImAgFnS	věnovat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
aktivními	aktivní	k2eAgMnPc7d1	aktivní
houslisty	houslista	k1gMnPc7	houslista
(	(	kIx(	(
<g/>
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
Haydn	Haydn	k1gMnSc1	Haydn
nebo	nebo	k8xC	nebo
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgFnPc1d3	nejslavnější
skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
sólové	sólový	k2eAgFnPc4d1	sólová
housle	housle	k1gFnPc4	housle
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
)	)	kIx)	)
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c6	v
období	období	k1gNnSc6	období
baroka	baroko	k1gNnSc2	baroko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bachovy	Bachov	k1gInPc1	Bachov
Sonáty	sonáta	k1gFnSc2	sonáta
a	a	k8xC	a
partity	partita	k1gFnSc2	partita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
skladbách	skladba	k1gFnPc6	skladba
využívají	využívat	k5eAaPmIp3nP	využívat
dvojhmaty	dvojhmat	k1gInPc1	dvojhmat
<g/>
.	.	kIx.	.
</s>
<s>
Houslová	houslový	k2eAgFnSc1d1	houslová
sólová	sólový	k2eAgFnSc1d1	sólová
hra	hra	k1gFnSc1	hra
prodělala	prodělat	k5eAaPmAgFnS	prodělat
renesanci	renesance	k1gFnSc4	renesance
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
díky	díky	k7c3	díky
skladatelům	skladatel	k1gMnPc3	skladatel
jako	jako	k8xS	jako
např.	např.	kA	např.
Bartók	Bartók	k1gMnSc1	Bartók
<g/>
,	,	kIx,	,
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
nebo	nebo	k8xC	nebo
Hindemith	Hindemith	k1gMnSc1	Hindemith
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
houslové	houslový	k2eAgInPc1d1	houslový
koncerty	koncert	k1gInPc1	koncert
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
sólové	sólový	k2eAgFnPc4d1	sólová
housle	housle	k1gFnPc4	housle
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
orchestru	orchestr	k1gInSc2	orchestr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
formy	forma	k1gFnSc2	forma
concerto	concerta	k1gFnSc5	concerta
grosso	grossa	k1gFnSc5	grossa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
koncertní	koncertní	k2eAgMnSc1d1	koncertní
mistr	mistr	k1gMnSc1	mistr
(	(	kIx(	(
<g/>
sedící	sedící	k2eAgMnSc1d1	sedící
napravo	napravo	k6eAd1	napravo
u	u	k7c2	u
prvního	první	k4xOgInSc2	první
pultu	pult	k1gInSc2	pult
prvních	první	k4xOgFnPc2	první
houslí	housle	k1gFnPc2	housle
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
hrál	hrát	k5eAaImAgMnS	hrát
vlastní	vlastní	k2eAgInSc4d1	vlastní
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
skutečné	skutečný	k2eAgInPc4d1	skutečný
houslové	houslový	k2eAgInPc4d1	houslový
koncerty	koncert	k1gInPc4	koncert
složil	složit	k5eAaPmAgMnS	složit
Vivaldi	Vivald	k1gMnPc1	Vivald
a	a	k8xC	a
Bach	Bach	k1gInSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
klasicismu	klasicismus	k1gInSc2	klasicismus
jsou	být	k5eAaImIp3nP	být
významnými	významný	k2eAgMnPc7d1	významný
autory	autor	k1gMnPc7	autor
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
a	a	k8xC	a
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pokládá	pokládat	k5eAaImIp3nS	pokládat
základy	základ	k1gInPc4	základ
romantického	romantický	k2eAgInSc2d1	romantický
houslového	houslový	k2eAgInSc2d1	houslový
koncertu	koncert	k1gInSc2	koncert
–	–	k?	–
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
ho	on	k3xPp3gInSc4	on
pojetím	pojetí	k1gNnSc7	pojetí
k	k	k7c3	k
symfonii	symfonie	k1gFnSc3	symfonie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
–	–	k?	–
cca	cca	kA	cca
40	[number]	k4	40
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgInPc1d3	nejslavnější
a	a	k8xC	a
nejhranější	hraný	k2eAgInPc1d3	nejhranější
houslové	houslový	k2eAgInPc1d1	houslový
koncerty	koncert	k1gInPc1	koncert
z	z	k7c2	z
období	období	k1gNnSc2	období
romantismu	romantismus	k1gInSc2	romantismus
zkomponovali	zkomponovat	k5eAaPmAgMnP	zkomponovat
Bruch	bruch	k1gInSc4	bruch
<g/>
,	,	kIx,	,
Mendelssohn	Mendelssohn	k1gMnSc1	Mendelssohn
<g/>
,	,	kIx,	,
Brahms	Brahms	k1gMnSc1	Brahms
<g/>
,	,	kIx,	,
Sibelius	Sibelius	k1gMnSc1	Sibelius
<g/>
,	,	kIx,	,
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
atd.	atd.	kA	atd.
Snad	snad	k9	snad
nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
houslistou	houslista	k1gMnSc7	houslista
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
skladatelem	skladatel	k1gMnSc7	skladatel
byl	být	k5eAaImAgMnS	být
Niccolò	Niccolò	k1gMnSc1	Niccolò
Paganini	Paganin	k2eAgMnPc1d1	Paganin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
houslím	houslit	k5eAaImIp1nS	houslit
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
houslistů	houslista	k1gMnPc2	houslista
<g/>
/	/	kIx~	/
<g/>
tek	teka	k1gFnPc2	teka
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaBmF	jmenovat
Fritze	Fritze	k1gFnPc4	Fritze
Kreislera	Kreisler	k1gMnSc2	Kreisler
<g/>
,	,	kIx,	,
Jaschu	Jascha	k1gFnSc4	Jascha
Heifetze	Heifetze	k1gFnSc2	Heifetze
<g/>
,	,	kIx,	,
Yehudiho	Yehudi	k1gMnSc2	Yehudi
Menuhina	Menuhin	k1gMnSc2	Menuhin
<g/>
,	,	kIx,	,
Davida	David	k1gMnSc2	David
Fjodoroviče	Fjodorovič	k1gMnSc2	Fjodorovič
Oistracha	Oistrach	k1gMnSc2	Oistrach
<g/>
,	,	kIx,	,
Leonida	Leonid	k1gMnSc2	Leonid
Kogana	Kogan	k1gMnSc2	Kogan
<g/>
,	,	kIx,	,
Henryka	Henryek	k1gMnSc2	Henryek
Szerynga	Szeryng	k1gMnSc2	Szeryng
<g/>
,	,	kIx,	,
Itzhaka	Itzhak	k1gMnSc2	Itzhak
Perlmana	Perlman	k1gMnSc2	Perlman
<g/>
,	,	kIx,	,
Idu	Ida	k1gFnSc4	Ida
Haendlovou	Haendlový	k2eAgFnSc4d1	Haendlový
<g/>
,	,	kIx,	,
Pinchase	Pinchasa	k1gFnSc3	Pinchasa
Zuckermanna	Zuckermanno	k1gNnSc2	Zuckermanno
<g/>
,	,	kIx,	,
Hilary	Hilar	k1gMnPc4	Hilar
Hahnovou	Hahnová	k1gFnSc4	Hahnová
<g/>
,	,	kIx,	,
Maxima	maximum	k1gNnPc4	maximum
Vengerova	Vengerův	k2eAgNnPc4d1	Vengerův
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
každého	každý	k3xTgInSc2	každý
symfonického	symfonický	k2eAgInSc2d1	symfonický
i	i	k8xC	i
komorního	komorní	k2eAgInSc2d1	komorní
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
smyčcové	smyčcový	k2eAgInPc1d1	smyčcový
orchestry	orchestr	k1gInPc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
baroka	baroko	k1gNnSc2	baroko
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
orchestru	orchestr	k1gInSc2	orchestr
dva	dva	k4xCgInPc1	dva
samostatné	samostatný	k2eAgInPc1d1	samostatný
houslové	houslový	k2eAgInPc1d1	houslový
hlasy	hlas	k1gInPc1	hlas
<g/>
:	:	kIx,	:
první	první	k4xOgFnPc1	první
housle	housle	k1gFnPc1	housle
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
16	[number]	k4	16
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhé	druhý	k4xOgFnPc4	druhý
housle	housle	k1gFnPc4	housle
(	(	kIx(	(
<g/>
14	[number]	k4	14
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
tvoří	tvořit	k5eAaImIp3nP	tvořit
také	také	k9	také
nedílnou	dílný	k2eNgFnSc4d1	nedílná
součást	součást	k1gFnSc4	součást
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
kvartetů	kvartet	k1gInPc2	kvartet
a	a	k8xC	a
kvintetů	kvintet	k1gInPc2	kvintet
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
smyčcové	smyčcový	k2eAgInPc4d1	smyčcový
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klavírní	klavírní	k2eAgNnSc1d1	klavírní
trio	trio	k1gNnSc1	trio
popř.	popř.	kA	popř.
klavírní	klavírní	k2eAgInSc4d1	klavírní
kvartet	kvartet	k1gInSc4	kvartet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
kombinací	kombinace	k1gFnSc7	kombinace
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
violy	viola	k1gFnSc2	viola
a	a	k8xC	a
violoncella	violoncello	k1gNnSc2	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
obsazení	obsazení	k1gNnSc4	obsazení
existuje	existovat	k5eAaImIp3nS	existovat
literatura	literatura	k1gFnSc1	literatura
již	již	k6eAd1	již
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
jako	jako	k8xS	jako
folková	folkový	k2eAgFnSc1d1	folková
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
tango	tango	k1gNnSc1	tango
<g/>
,	,	kIx,	,
romská	romský	k2eAgFnSc1d1	romská
hudba	hudba	k1gFnSc1	hudba
či	či	k8xC	či
klezmer	klezmer	k1gInSc1	klezmer
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k9	i
rock	rock	k1gInSc4	rock
a	a	k8xC	a
jazz	jazz	k1gInSc4	jazz
<g/>
;	;	kIx,	;
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dokonce	dokonce	k9	dokonce
i	i	k9	i
pro	pro	k7c4	pro
extrémnější	extrémní	k2eAgInPc4d2	extrémnější
styly	styl	k1gInPc4	styl
jako	jako	k8xS	jako
např.	např.	kA	např.
black	black	k6eAd1	black
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
moderních	moderní	k2eAgInPc2d1	moderní
stylů	styl	k1gInPc2	styl
se	se	k3xPyFc4	se
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
elektrické	elektrický	k2eAgFnPc4d1	elektrická
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
