<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
<g/>
?	?	kIx.	?
</s>
