<p>
<s>
Termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
reakce	reakce	k1gFnSc1	reakce
či	či	k8xC	či
termojaderná	termojaderný	k2eAgFnSc1d1	termojaderná
fúze	fúze	k1gFnSc1	fúze
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgFnSc3d1	jaderná
fúzi	fúze	k1gFnSc3	fúze
<g/>
)	)	kIx)	)
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
termojaderné	termojaderný	k2eAgFnSc2d1	termojaderná
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
hmotnostního	hmotnostní	k2eAgInSc2d1	hmotnostní
úbytku	úbytek	k1gInSc2	úbytek
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
slučování	slučování	k1gNnSc3	slučování
jader	jádro	k1gNnPc2	jádro
působí	působit	k5eAaImIp3nS	působit
odpudivá	odpudivý	k2eAgFnSc1d1	odpudivá
Coulombova	Coulombův	k2eAgFnSc1d1	Coulombova
interakce	interakce	k1gFnSc1	interakce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
například	například	k6eAd1	například
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
dvěma	dva	k4xCgFnPc7	dva
jádrům	jádro	k1gNnPc3	jádro
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
náboji	náboj	k1gInPc7	náboj
přiblížit	přiblížit	k5eAaPmF	přiblížit
se	se	k3xPyFc4	se
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
krátkodosahová	krátkodosahový	k2eAgFnSc1d1	krátkodosahový
jaderná	jaderný	k2eAgFnSc1d1	jaderná
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
Coulombovy	Coulombův	k2eAgFnSc2d1	Coulombova
potenciálové	potenciálový	k2eAgFnSc2d1	potenciálová
bariéry	bariéra	k1gFnSc2	bariéra
například	například	k6eAd1	například
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
protony	proton	k1gInPc4	proton
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
400	[number]	k4	400
keV	keV	k?	keV
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
jejího	její	k3xOp3gNnSc2	její
překonání	překonání	k1gNnSc2	překonání
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
tepelného	tepelný	k2eAgInSc2d1	tepelný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
Slunce	slunce	k1gNnSc2	slunce
==	==	k?	==
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Slunce	slunce	k1gNnSc2	slunce
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,5	[number]	k4	1,5
<g/>
×	×	k?	×
<g/>
107	[number]	k4	107
K	k	k7c3	k
neboli	neboli	k8xC	neboli
1,30	[number]	k4	1,30
keV	keV	k?	keV
<g/>
/	/	kIx~	/
<g/>
k	k	k7c3	k
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
eV	eV	k?	eV
<g/>
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
nesprávně	správně	k6eNd1	správně
uváděná	uváděný	k2eAgFnSc1d1	uváděná
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
eV	eV	k?	eV
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
plazmatu	plazma	k1gNnSc2	plazma
pro	pro	k7c4	pro
udání	udání	k1gNnSc4	udání
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
teplotě	teplota	k1gFnSc6	teplota
již	již	k9	již
část	část	k1gFnSc1	část
protonů	proton	k1gInPc2	proton
může	moct	k5eAaImIp3nS	moct
nahodilými	nahodilý	k2eAgFnPc7d1	nahodilá
srážkami	srážka	k1gFnPc7	srážka
získat	získat	k5eAaPmF	získat
dostatečnou	dostatečná	k1gFnSc4	dostatečná
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
elektrické	elektrický	k2eAgFnSc2d1	elektrická
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
sloučení	sloučení	k1gNnSc2	sloučení
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
mohla	moct	k5eAaImAgFnS	moct
fúze	fúze	k1gFnSc1	fúze
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
probíhat	probíhat	k5eAaImF	probíhat
všude	všude	k6eAd1	všude
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
(	(	kIx(	(
<g/>
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
teplota	teplota	k1gFnSc1	teplota
asi	asi	k9	asi
400	[number]	k4	400
keV	keV	k?	keV
<g/>
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
překonání	překonání	k1gNnSc4	překonání
odpudivé	odpudivý	k2eAgFnSc2d1	odpudivá
elektrické	elektrický	k2eAgFnSc2d1	elektrická
síly	síla	k1gFnSc2	síla
dvou	dva	k4xCgInPc2	dva
protonů	proton	k1gInPc2	proton
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
takto	takto	k6eAd1	takto
probíhá	probíhat	k5eAaImIp3nS	probíhat
až	až	k9	až
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fúzi	fúze	k1gFnSc6	fúze
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
pravděpodobnostní	pravděpodobnostní	k2eAgInSc1d1	pravděpodobnostní
charakter	charakter	k1gInSc1	charakter
částic	částice	k1gFnPc2	částice
na	na	k7c6	na
fundamentální	fundamentální	k2eAgFnSc6d1	fundamentální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jev	jev	k1gInSc4	jev
zvaný	zvaný	k2eAgInSc4d1	zvaný
kvantové	kvantový	k2eAgFnPc4d1	kvantová
tunelování	tunelování	k1gNnPc4	tunelování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
jistá	jistý	k2eAgFnSc1d1	jistá
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgNnSc6	jaký
místě	místo	k1gNnSc6	místo
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
daná	daný	k2eAgFnSc1d1	daná
částice	částice	k1gFnSc1	částice
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
popisuje	popisovat	k5eAaImIp3nS	popisovat
tzv.	tzv.	kA	tzv.
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Šance	šance	k1gFnSc1	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
proton	proton	k1gInSc1	proton
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jiného	jiný	k2eAgNnSc2d1	jiné
jádra	jádro	k1gNnSc2	jádro
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k6eAd1	blízko
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
silná	silný	k2eAgFnSc1d1	silná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
tak	tak	k9	tak
následně	následně	k6eAd1	následně
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
opravdu	opravdu	k6eAd1	opravdu
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
obrovskému	obrovský	k2eAgNnSc3d1	obrovské
množství	množství	k1gNnSc3	množství
částic	částice	k1gFnPc2	částice
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
může	moct	k5eAaImIp3nS	moct
slučování	slučování	k1gNnSc4	slučování
probíhat	probíhat	k5eAaImF	probíhat
prakticky	prakticky	k6eAd1	prakticky
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
sloučení	sloučení	k1gNnSc6	sloučení
dvou	dva	k4xCgInPc2	dva
protonů	proton	k1gInPc2	proton
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
neutron	neutron	k1gInSc4	neutron
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
deutéria	deutérium	k1gNnSc2	deutérium
<g/>
,	,	kIx,	,
neutrína	neutrín	k1gInSc2	neutrín
<g/>
,	,	kIx,	,
pozitronu	pozitron	k1gInSc2	pozitron
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
gamma	gammum	k1gNnSc2	gammum
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
deutérium	deutérium	k1gNnSc1	deutérium
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
znovu	znovu	k6eAd1	znovu
dál	daleko	k6eAd2	daleko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kvantového	kvantový	k2eAgNnSc2d1	kvantové
tunelování	tunelování	k1gNnSc2	tunelování
slučovat	slučovat	k5eAaImF	slučovat
s	s	k7c7	s
protonem	proton	k1gInSc7	proton
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
jádrem	jádro	k1gNnSc7	jádro
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
jader	jádro	k1gNnPc2	jádro
těžších	těžký	k2eAgNnPc2d2	těžší
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc4	uvolnění
dalšího	další	k2eAgInSc2d1	další
vysokoenergetického	vysokoenergetický	k2eAgInSc2d1	vysokoenergetický
fotonu	foton	k1gInSc2	foton
gamma	gammum	k1gNnSc2	gammum
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
sloučí	sloučit	k5eAaPmIp3nS	sloučit
Slunce	slunce	k1gNnSc1	slunce
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jádru	jádro	k1gNnSc6	jádro
620	[number]	k4	620
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
životnost	životnost	k1gFnSc1	životnost
nejméně	málo	k6eAd3	málo
dalších	další	k2eAgFnPc2d1	další
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
dojde	dojít	k5eAaPmIp3nS	dojít
vodík	vodík	k1gInSc4	vodík
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
naruší	narušit	k5eAaPmIp3nS	narušit
se	se	k3xPyFc4	se
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
hydrostatická	hydrostatický	k2eAgFnSc1d1	hydrostatická
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
červeným	červený	k2eAgMnSc7d1	červený
obrem	obr	k1gMnSc7	obr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
hvězdách	hvězda	k1gFnPc6	hvězda
==	==	k?	==
</s>
</p>
<p>
<s>
Slučováním	slučování	k1gNnSc7	slučování
dalších	další	k2eAgNnPc2d1	další
jader	jádro	k1gNnPc2	jádro
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dostaneme	dostat	k5eAaPmIp1nP	dostat
ke	k	k7c3	k
všem	všecek	k3xTgInPc3	všecek
možným	možný	k2eAgInPc3d1	možný
prvkům	prvek	k1gInPc3	prvek
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
nukleonovým	nukleonův	k2eAgNnSc7d1	nukleonův
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
esenciálním	esenciální	k2eAgInSc7d1	esenciální
základem	základ	k1gInSc7	základ
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
kamenných	kamenný	k2eAgFnPc2d1	kamenná
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
těžší	těžký	k2eAgInPc4d2	těžší
než	než	k8xS	než
helium	helium	k1gNnSc4	helium
tedy	tedy	k8xC	tedy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jadernou	jaderný	k2eAgFnSc7d1	jaderná
fúzí	fúze	k1gFnSc7	fúze
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
(	(	kIx(	(
<g/>
prvotní	prvotní	k2eAgInSc4d1	prvotní
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
helium	helium	k1gNnSc1	helium
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
během	během	k7c2	během
Velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
<g/>
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
<g/>
%	%	kIx~	%
<g/>
helia	helium	k1gNnSc2	helium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termonukleární	termonukleární	k2eAgFnPc1d1	termonukleární
reakce	reakce	k1gFnPc1	reakce
mohou	moct	k5eAaImIp3nP	moct
u	u	k7c2	u
hvězd	hvězda	k1gFnPc2	hvězda
menších	malý	k2eAgFnPc2d2	menší
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
tříd	třída	k1gFnPc2	třída
trvat	trvat	k5eAaImF	trvat
i	i	k8xC	i
stovky	stovka	k1gFnPc1	stovka
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
u	u	k7c2	u
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělé	umělý	k2eAgNnSc1d1	umělé
vyvolání	vyvolání	k1gNnSc1	vyvolání
fúze	fúze	k1gFnSc1	fúze
==	==	k?	==
</s>
</p>
<p>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
probíhá	probíhat	k5eAaImIp3nS	probíhat
s	s	k7c7	s
obrovským	obrovský	k2eAgInSc7d1	obrovský
využitelným	využitelný	k2eAgInSc7d1	využitelný
ziskem	zisk	k1gInSc7	zisk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vědci	vědec	k1gMnPc1	vědec
a	a	k8xC	a
inženýři	inženýr	k1gMnPc1	inženýr
snaží	snažit	k5eAaImIp3nP	snažit
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
optimální	optimální	k2eAgNnSc4d1	optimální
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
využít	využít	k5eAaPmF	využít
tento	tento	k3xDgInSc1	tento
potenciál	potenciál	k1gInSc1	potenciál
i	i	k9	i
zde	zde	k6eAd1	zde
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Účinnější	účinný	k2eAgNnSc1d2	účinnější
využití	využití	k1gNnSc1	využití
hmoty	hmota	k1gFnSc2	hmota
už	už	k9	už
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pouze	pouze	k6eAd1	pouze
její	její	k3xOp3gFnSc7	její
kompletní	kompletní	k2eAgFnSc7d1	kompletní
přeměnou	přeměna	k1gFnSc7	přeměna
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
formu	forma	k1gFnSc4	forma
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
hmota	hmota	k1gFnSc1	hmota
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
její	její	k3xOp3gFnSc7	její
anihilací	anihilace	k1gFnSc7	anihilace
s	s	k7c7	s
antihmotou	antihmota	k1gFnSc7	antihmota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
budoucnosti	budoucnost	k1gFnSc6	budoucnost
proměnou	proměna	k1gFnSc7	proměna
v	v	k7c4	v
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
a	a	k8xC	a
zachycení	zachycení	k1gNnSc4	zachycení
její	její	k3xOp3gFnSc2	její
energie	energie	k1gFnSc2	energie
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
vypařování	vypařování	k1gNnSc6	vypařování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dvou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
energetický	energetický	k2eAgInSc4d1	energetický
zisk	zisk	k1gInSc4	zisk
z	z	k7c2	z
hmoty	hmota	k1gFnSc2	hmota
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potenciální	potenciální	k2eAgNnSc1d1	potenciální
využití	využití	k1gNnSc1	využití
fúze	fúze	k1gFnSc2	fúze
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
vojenské	vojenský	k2eAgNnSc4d1	vojenské
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
vývoj	vývoj	k1gInSc1	vývoj
jaderných	jaderný	k2eAgFnPc2d1	jaderná
fúzních	fúzní	k2eAgFnPc2d1	fúzní
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
a	a	k8xC	a
civilní	civilní	k2eAgInSc1d1	civilní
vývoj	vývoj	k1gInSc1	vývoj
fúzní	fúzní	k2eAgFnSc2d1	fúzní
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
řízené	řízený	k2eAgFnSc2d1	řízená
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fúze	fúze	k1gFnPc4	fúze
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zvládnout	zvládnout	k5eAaPmF	zvládnout
jadernou	jaderný	k2eAgFnSc4d1	jaderná
fúzi	fúze	k1gFnSc4	fúze
řízeně	řízeně	k6eAd1	řízeně
a	a	k8xC	a
následně	následně	k6eAd1	následně
ji	on	k3xPp3gFnSc4	on
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
fúze	fúze	k1gFnSc2	fúze
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ohromné	ohromný	k2eAgFnPc4d1	ohromná
teploty	teplota	k1gFnPc4	teplota
(	(	kIx(	(
<g/>
desítky	desítka	k1gFnPc4	desítka
milionů	milion	k4xCgInPc2	milion
kelvinů	kelvin	k1gInPc2	kelvin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
plazma	plazma	k1gNnSc1	plazma
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
teplotě	teplota	k1gFnSc6	teplota
udržet	udržet	k5eAaPmF	udržet
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
hustotou	hustota	k1gFnSc7	hustota
po	po	k7c4	po
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
plazmatu	plazma	k1gNnSc6	plazma
mluvíme	mluvit	k5eAaImIp1nP	mluvit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
při	při	k7c6	při
dané	daný	k2eAgFnSc6d1	daná
teplotě	teplota	k1gFnSc6	teplota
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
v	v	k7c4	v
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
i	i	k9	i
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
palivo	palivo	k1gNnSc1	palivo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
tritium	tritium	k1gNnSc1	tritium
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
reaktoru	reaktor	k1gInSc2	reaktor
se	se	k3xPyFc4	se
také	také	k9	také
stávají	stávat	k5eAaImIp3nP	stávat
radioaktivními	radioaktivní	k2eAgMnPc7d1	radioaktivní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
energie	energie	k1gFnSc2	energie
produkuje	produkovat	k5eAaImIp3nS	produkovat
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
odpad	odpad	k1gInSc4	odpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
jako	jako	k9	jako
potenciálně	potenciálně	k6eAd1	potenciálně
použitelné	použitelný	k2eAgFnPc1d1	použitelná
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
dvě	dva	k4xCgFnPc1	dva
metody	metoda	k1gFnPc1	metoda
<g/>
:	:	kIx,	:
magnetické	magnetický	k2eAgNnSc1d1	magnetické
udržení	udržení	k1gNnSc1	udržení
pomocí	pomocí	k7c2	pomocí
tokamaků	tokamak	k1gInPc2	tokamak
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
stellarátorů	stellarátor	k1gInPc2	stellarátor
<g/>
)	)	kIx)	)
a	a	k8xC	a
inerciální	inerciální	k2eAgNnSc1d1	inerciální
udržení	udržení	k1gNnSc1	udržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
udržení	udržení	k1gNnSc1	udržení
využívá	využívat	k5eAaPmIp3nS	využívat
komplikovaných	komplikovaný	k2eAgFnPc2d1	komplikovaná
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
v	v	k7c6	v
magnetických	magnetický	k2eAgFnPc6d1	magnetická
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
plazma	plazma	k1gNnSc1	plazma
nedotýkalo	dotýkat	k5eNaImAgNnS	dotýkat
stěn	stěn	k1gInSc4	stěn
a	a	k8xC	a
defakto	defakto	k1gNnSc1	defakto
levitovalo	levitovat	k5eAaImAgNnS	levitovat
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
<g/>
Inerciální	inerciální	k2eAgNnSc1d1	inerciální
udržení	udržení	k1gNnSc1	udržení
naopak	naopak	k6eAd1	naopak
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
na	na	k7c4	na
fyzické	fyzický	k2eAgNnSc4d1	fyzické
držení	držení	k1gNnSc4	držení
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
takových	takový	k3xDgFnPc2	takový
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
hustoty	hustota	k1gFnSc2	hustota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
materiál	materiál	k1gInSc1	materiál
nestihl	stihnout	k5eNaPmAgInS	stihnout
expandovat	expandovat	k5eAaImF	expandovat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
fúzní	fúzní	k2eAgFnSc3d1	fúzní
reakci	reakce	k1gFnSc3	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
udržován	udržovat	k5eAaImNgInS	udržovat
jen	jen	k9	jen
svou	svůj	k3xOyFgFnSc7	svůj
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
inertia	inertia	k1gFnSc1	inertia
<g/>
)	)	kIx)	)
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
brání	bránit	k5eAaImIp3nS	bránit
v	v	k7c6	v
rychlé	rychlý	k2eAgFnSc6d1	rychlá
expanzi	expanze	k1gFnSc6	expanze
<g/>
.	.	kIx.	.
<g/>
Termojaderná	termojaderný	k2eAgFnSc1d1	termojaderná
fúzní	fúzní	k2eAgFnSc1d1	fúzní
reakce	reakce	k1gFnSc1	reakce
v	v	k7c6	v
tokamacích	tokamace	k1gFnPc6	tokamace
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
demonstrována	demonstrován	k2eAgFnSc1d1	demonstrována
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
tokamaku	tokamak	k1gInSc6	tokamak
JET	jet	k5eAaImF	jet
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
využití	využití	k1gNnSc6	využití
pro	pro	k7c4	pro
energetiku	energetika	k1gFnSc4	energetika
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současných	současný	k2eAgFnPc6d1	současná
tokamacích	tokamace	k1gFnPc6	tokamace
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
třeba	třeba	k6eAd1	třeba
dodat	dodat	k5eAaPmF	dodat
do	do	k7c2	do
zařízení	zařízení	k1gNnSc2	zařízení
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolik	kolik	k9	kolik
je	být	k5eAaImIp3nS	být
vyprodukováno	vyprodukovat	k5eAaPmNgNnS	vyprodukovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
s	s	k7c7	s
rostoucími	rostoucí	k2eAgInPc7d1	rostoucí
rozměry	rozměr	k1gInPc7	rozměr
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ovšem	ovšem	k9	ovšem
narůstají	narůstat	k5eAaImIp3nP	narůstat
mnohé	mnohý	k2eAgInPc4d1	mnohý
technologické	technologický	k2eAgInPc4d1	technologický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
využití	využití	k1gNnSc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Zažehnutí	zažehnutí	k1gNnSc1	zažehnutí
neřízených	řízený	k2eNgFnPc2d1	neřízená
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
ve	v	k7c6	v
zbrojním	zbrojní	k2eAgInSc6d1	zbrojní
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
termonukleární	termonukleární	k2eAgFnPc4d1	termonukleární
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
díky	díky	k7c3	díky
obrovskému	obrovský	k2eAgNnSc3d1	obrovské
množství	množství	k1gNnSc3	množství
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
velmi	velmi	k6eAd1	velmi
devastující	devastující	k2eAgInPc1d1	devastující
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
řádově	řádově	k6eAd1	řádově
tisíckrát	tisíckrát	k6eAd1	tisíckrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
štěpná	štěpný	k2eAgFnSc1d1	štěpná
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
dosud	dosud	k6eAd1	dosud
vyrobenou	vyrobený	k2eAgFnSc7d1	vyrobená
a	a	k8xC	a
otestovanou	otestovaný	k2eAgFnSc7d1	otestovaná
fúzní	fúzní	k2eAgFnSc7d1	fúzní
bombou	bomba	k1gFnSc7	bomba
byl	být	k5eAaImAgInS	být
ruský	ruský	k2eAgMnSc1d1	ruský
Car	car	k1gMnSc1	car
se	s	k7c7	s
silou	síla	k1gFnSc7	síla
50	[number]	k4	50
Megatun	megatuna	k1gFnPc2	megatuna
TNT	TNT	kA	TNT
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyzkoušen	vyzkoušet	k5eAaPmNgInS	vyzkoušet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
využití	využití	k1gNnPc4	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
fúze	fúze	k1gFnSc1	fúze
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
použít	použít	k5eAaPmF	použít
při	při	k7c6	při
mezihvězdných	mezihvězdný	k2eAgNnPc6d1	mezihvězdné
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
množství	množství	k1gNnSc4	množství
směsi	směs	k1gFnSc2	směs
deuteria	deuterium	k1gNnSc2	deuterium
a	a	k8xC	a
tritia	tritium	k1gNnSc2	tritium
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zapálilo	zapálit	k5eAaPmAgNnS	zapálit
elektronovým	elektronový	k2eAgInSc7d1	elektronový
paprskem	paprsek	k1gInSc7	paprsek
(	(	kIx(	(
<g/>
laserem	laser	k1gInSc7	laser
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
pohonných	pohonný	k2eAgFnPc2d1	pohonná
látek	látka	k1gFnPc2	látka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
výsledná	výsledný	k2eAgFnSc1d1	výsledná
směs	směs	k1gFnSc1	směs
v	v	k7c6	v
spalovací	spalovací	k2eAgFnSc6d1	spalovací
komoře	komora	k1gFnSc6	komora
rozpínala	rozpínat	k5eAaImAgFnS	rozpínat
a	a	k8xC	a
tryskou	tryska	k1gFnSc7	tryska
by	by	kYmCp3nS	by
odtekla	odtéct	k5eAaPmAgFnS	odtéct
do	do	k7c2	do
volného	volný	k2eAgInSc2d1	volný
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nS	by
raketě	raketa	k1gFnSc3	raketa
udělila	udělit	k5eAaPmAgFnS	udělit
dopředný	dopředný	k2eAgInSc4d1	dopředný
impuls	impuls	k1gInSc4	impuls
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
opakovalo	opakovat	k5eAaImAgNnS	opakovat
v	v	k7c6	v
dávkách	dávka	k1gFnPc6	dávka
a	a	k8xC	a
raketa	raketa	k1gFnSc1	raketa
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
ne	ne	k9	ne
kontinuálním	kontinuální	k2eAgInSc7d1	kontinuální
tahem	tah	k1gInSc7	tah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
pulsů	puls	k1gInPc2	puls
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fúze	fúze	k1gFnSc1	fúze
</s>
</p>
<p>
<s>
Studená	studený	k2eAgFnSc1d1	studená
fúze	fúze	k1gFnSc1	fúze
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
</s>
</p>
<p>
<s>
ITER	ITER	kA	ITER
</s>
</p>
<p>
<s>
Tokamak	Tokamak	k6eAd1	Tokamak
</s>
</p>
<p>
<s>
Stellarátor	Stellarátor	k1gMnSc1	Stellarátor
</s>
</p>
<p>
<s>
Termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
zbraň	zbraň	k1gFnSc1	zbraň
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
fúze	fúze	k1gFnSc2	fúze
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
fúze	fúze	k1gFnSc1	fúze
na	na	k7c4	na
ipp	ipp	k?	ipp
<g/>
.	.	kIx.	.
<g/>
cas	cas	k?	cas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
