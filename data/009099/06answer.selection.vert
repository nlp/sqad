<s>
Termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
reakce	reakce	k1gFnSc1	reakce
či	či	k8xC	či
termojaderná	termojaderný	k2eAgFnSc1d1	termojaderná
fúze	fúze	k1gFnSc1	fúze
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgFnSc3d1	jaderná
fúzi	fúze	k1gFnSc3	fúze
<g/>
)	)	kIx)	)
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
