<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
kapela	kapela	k1gFnSc1	kapela
Overkill	Overkilla	k1gFnPc2	Overkilla
(	(	kIx(	(
<g/>
název	název	k1gInSc4	název
si	se	k3xPyFc3	se
zvolili	zvolit	k5eAaPmAgMnP	zvolit
podle	podle	k7c2	podle
alba	album	k1gNnSc2	album
Motörhead	Motörhead	k1gInSc1	Motörhead
Overkill	Overkillum	k1gNnPc2	Overkillum
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
napsala	napsat	k5eAaBmAgFnS	napsat
song	song	k1gInSc1	song
"	"	kIx"	"
<g/>
Unleash	Unleash	k1gMnSc1	Unleash
the	the	k?	the
Beast	Beast	k1gMnSc1	Beast
Within	Within	k1gMnSc1	Within
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgFnSc4	první
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
thrashovou	thrashový	k2eAgFnSc4d1	thrashová
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.	.
</s>
