<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
Határ	Határ	k1gInSc1	Határ
út	út	k?	út
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Budapešti	Budapešť	k1gFnSc2	Budapešť
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
M3	M3	k1gFnSc2	M3
budapešťského	budapešťský	k2eAgNnSc2d1	budapešťské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
hloubená	hloubený	k2eAgFnSc1d1	hloubená
<g/>
,	,	kIx,	,
uložená	uložený	k2eAgFnSc1d1	uložená
4,86	[number]	k4	4,86
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
třemi	tři	k4xCgFnPc7	tři
kolejemi	kolej	k1gFnPc7	kolej
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
bočním	boční	k2eAgNnSc7d1	boční
nástupištěm	nástupiště	k1gNnSc7	nástupiště
pro	pro	k7c4	pro
kolej	kolej	k1gFnSc4	kolej
směr	směr	k1gInSc1	směr
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
ostrovním	ostrovní	k2eAgNnSc7d1	ostrovní
nástupištěm	nástupiště	k1gNnSc7	nástupiště
mezi	mezi	k7c7	mezi
zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
dvěma	dva	k4xCgFnPc7	dva
kolejemi	kolej	k1gFnPc7	kolej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
provozu	provoz	k1gInSc6	provoz
jsou	být	k5eAaImIp3nP	být
užívány	užíván	k2eAgFnPc4d1	užívána
krajní	krajní	k2eAgFnPc4d1	krajní
koleje	kolej	k1gFnPc4	kolej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
přestup	přestup	k1gInSc1	přestup
na	na	k7c4	na
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
linky	linka	k1gFnPc4	linka
42	[number]	k4	42
<g/>
,	,	kIx,	,
50	[number]	k4	50
a	a	k8xC	a
52	[number]	k4	52
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
3,5	[number]	k4	3,5
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
ulice	ulice	k1gFnSc2	ulice
Hátar	Hátar	k1gMnSc1	Hátar
út	út	k?	út
(	(	kIx(	(
<g/>
Hraniční	hraniční	k2eAgFnPc1d1	hraniční
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zde	zde	k6eAd1	zde
počíná	počínat	k5eAaImIp3nS	počínat
a	a	k8xC	a
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1950	[number]	k4	1950
tvořila	tvořit	k5eAaImAgFnS	tvořit
hranici	hranice	k1gFnSc4	hranice
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Határ	Határ	k1gInSc1	Határ
út	út	k?	út
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
</s>
</p>
