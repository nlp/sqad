<p>
<s>
Bělořit	bělořit	k1gMnSc1	bělořit
šedý	šedý	k2eAgMnSc1d1	šedý
(	(	kIx(	(
<g/>
Oenanthe	Oenanthe	k1gInSc1	Oenanthe
oenanthe	oenanth	k1gInSc2	oenanth
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
pěvců	pěvec	k1gMnPc2	pěvec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
jako	jako	k8xS	jako
vrabec	vrabec	k1gMnSc1	vrabec
<g/>
.	.	kIx.	.
</s>
<s>
Tažný	tažný	k2eAgInSc1d1	tažný
<g/>
;	;	kIx,	;
ze	z	k7c2	z
zimovišť	zimoviště	k1gNnPc2	zimoviště
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
odlétá	odlétat	k5eAaImIp3nS	odlétat
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
mezi	mezi	k7c7	mezi
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
v	v	k7c6	v
pískových	pískový	k2eAgFnPc6d1	písková
nebo	nebo	k8xC	nebo
hlinitých	hlinitý	k2eAgFnPc6d1	hlinitá
stěnách	stěna	k1gFnPc6	stěna
apod.	apod.	kA	apod.
Ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Bělořit	bělořit	k1gMnSc1	bělořit
šedý	šedý	k2eAgMnSc1d1	šedý
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
veliký	veliký	k2eAgMnSc1d1	veliký
asi	asi	k9	asi
jako	jako	k9	jako
vrabec	vrabec	k1gMnSc1	vrabec
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
14,5	[number]	k4	14,5
až	až	k8xS	až
15,5	[number]	k4	15,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
má	mít	k5eAaImIp3nS	mít
26	[number]	k4	26
až	až	k9	až
32	[number]	k4	32
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
17	[number]	k4	17
až	až	k9	až
30	[number]	k4	30
g.	g.	k?	g.
Žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
ani	ani	k8xC	ani
samec	samec	k1gMnSc1	samec
v	v	k7c6	v
prostém	prostý	k2eAgInSc6d1	prostý
šatě	šat	k1gInSc6	šat
nejsou	být	k5eNaImIp3nP	být
moc	moc	k6eAd1	moc
kontrastně	kontrastně	k6eAd1	kontrastně
zbarveni	zbarvit	k5eAaPmNgMnP	zbarvit
<g/>
.	.	kIx.	.
</s>
<s>
Sameček	sameček	k1gMnSc1	sameček
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
šedou	šedý	k2eAgFnSc4d1	šedá
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
proužek	proužek	k1gInSc4	proužek
přes	přes	k7c4	přes
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc4d1	černá
skvrnu	skvrna	k1gFnSc4	skvrna
za	za	k7c7	za
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc4d1	bílá
pásku	páska	k1gFnSc4	páska
nad	nad	k7c7	nad
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
černá	černý	k2eAgNnPc4d1	černé
křídla	křídlo	k1gNnPc4	křídlo
i	i	k9	i
konec	konec	k1gInSc1	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Severoafričtí	severoafrický	k2eAgMnPc1d1	severoafrický
bělořiti	bělořit	k5eAaImF	bělořit
mají	mít	k5eAaImIp3nP	mít
hrdlo	hrdlo	k1gNnSc4	hrdlo
černé	černá	k1gFnSc2	černá
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
kostřec	kostřec	k1gInSc1	kostřec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
dostal	dostat	k5eAaPmAgMnS	dostat
pták	pták	k1gMnSc1	pták
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
skvrnitá	skvrnitý	k2eAgNnPc1d1	skvrnité
<g/>
.	.	kIx.	.
</s>
<s>
Zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
sedává	sedávat	k5eAaImIp3nS	sedávat
na	na	k7c6	na
velkých	velký	k2eAgInPc6d1	velký
balvanech	balvan	k1gInPc6	balvan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prostředí	prostředí	k1gNnSc2	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Bezlesé	bezlesý	k2eAgFnPc1d1	bezlesá
<g/>
,	,	kIx,	,
suché	suchý	k2eAgFnPc1d1	suchá
krajiny	krajina	k1gFnPc1	krajina
se	s	k7c7	s
suťovými	suťový	k2eAgInPc7d1	suťový
a	a	k8xC	a
skalnatými	skalnatý	k2eAgInPc7d1	skalnatý
terény	terén	k1gInPc7	terén
<g/>
,	,	kIx,	,
suchými	suchý	k2eAgFnPc7d1	suchá
zídkami	zídka	k1gFnPc7	zídka
anebo	anebo	k8xC	anebo
dunami	duna	k1gFnPc7	duna
s	s	k7c7	s
vřesem	vřes	k1gInSc7	vřes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rozšíření	rozšíření	k1gNnSc1	rozšíření
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
palearktické	palearktický	k2eAgFnSc2d1	palearktická
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc2d1	západní
Eurasie	Eurasie	k1gFnSc2	Eurasie
včetně	včetně	k7c2	včetně
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Přední	přední	k2eAgFnSc2d1	přední
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
palearktické	palearktický	k2eAgFnSc2d1	palearktická
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
Eurasie	Eurasie	k1gFnSc2	Eurasie
včetně	včetně	k7c2	včetně
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Číny	Čína	k1gFnSc2	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etiopská	etiopský	k2eAgFnSc1d1	etiopská
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nearktická	arktický	k2eNgFnSc1d1	nearktická
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
je	být	k5eAaImIp3nS	být
nepravidelně	pravidelně	k6eNd1	pravidelně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
hojnější	hojný	k2eAgMnSc1d2	hojnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
pravidelně	pravidelně	k6eAd1	pravidelně
ale	ale	k8xC	ale
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
na	na	k7c6	na
bezlesých	bezlesý	k2eAgInPc6d1	bezlesý
hřebenech	hřeben	k1gInPc6	hřeben
hor.	hor.	k?	hor.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
početnost	početnost	k1gFnSc1	početnost
stále	stále	k6eAd1	stále
silně	silně	k6eAd1	silně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
téměř	téměř	k6eAd1	téměř
vymizel	vymizet	k5eAaPmAgMnS	vymizet
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
to	ten	k3xDgNnSc1	ten
býval	bývat	k5eAaImAgInS	bývat
hojný	hojný	k2eAgInSc1d1	hojný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
10	[number]	k4	10
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001-03	[number]	k4	2001-03
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
200-400	[number]	k4	200-400
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ubývající	ubývající	k2eAgFnSc1d1	ubývající
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
počty	počet	k1gInPc1	počet
ubývají	ubývat	k5eAaImIp3nP	ubývat
i	i	k9	i
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
na	na	k7c4	na
4,6	[number]	k4	4,6
milionu	milion	k4xCgInSc2	milion
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
EN	EN	kA	EN
-	-	kIx~	-
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
drobnými	drobný	k2eAgMnPc7d1	drobný
bezobratlými	bezobratlý	k2eAgMnPc7d1	bezobratlý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
malími	malí	k2eAgMnPc7d1	malí
plži	plž	k1gMnPc7	plž
<g/>
,	,	kIx,	,
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jiní	jiný	k2eAgMnPc1d1	jiný
drozdovití	drozdovitý	k2eAgMnPc1d1	drozdovitý
ptáci	pták	k1gMnPc1	pták
sbírá	sbírat	k5eAaImIp3nS	sbírat
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
bobule	bobule	k1gFnPc4	bobule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
skalnatých	skalnatý	k2eAgNnPc6d1	skalnaté
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
v	v	k7c6	v
lomech	lom	k1gInPc6	lom
<g/>
,	,	kIx,	,
na	na	k7c6	na
pastvinách	pastvina	k1gFnPc6	pastvina
s	s	k7c7	s
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
i	i	k8xC	i
nad	nad	k7c7	nad
pásmem	pásmo	k1gNnSc7	pásmo
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
červnu	červen	k1gInSc6	červen
jednou	jednou	k6eAd1	jednou
až	až	k9	až
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
spleteno	spleten	k2eAgNnSc1d1	spleteno
ze	z	k7c2	z
stébel	stéblo	k1gNnPc2	stéblo
a	a	k8xC	a
kořínků	kořínek	k1gInPc2	kořínek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
křovinách	křovina	k1gFnPc6	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
hnízdit	hnízdit	k5eAaImF	hnízdit
i	i	k9	i
v	v	k7c6	v
králičí	králičí	k2eAgFnSc6d1	králičí
noře	nora	k1gFnSc6	nora
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kamenem	kámen	k1gInSc7	kámen
či	či	k8xC	či
v	v	k7c6	v
kamenné	kamenný	k2eAgFnSc6d1	kamenná
zídce	zídka	k1gFnSc6	zídka
<g/>
.	.	kIx.	.
</s>
<s>
Bělořit	bělořit	k1gMnSc1	bělořit
šedý	šedý	k2eAgMnSc1d1	šedý
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zimovišť	zimoviště	k1gNnPc2	zimoviště
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
odlétá	odlétat	k5eAaImIp3nS	odlétat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
bývá	bývat	k5eAaImIp3nS	bývat
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
světle	světle	k6eAd1	světle
modrých	modrý	k2eAgNnPc2d1	modré
neskvrnitých	skvrnitý	k2eNgNnPc2d1	skvrnitý
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
asi	asi	k9	asi
13	[number]	k4	13
až	až	k9	až
14	[number]	k4	14
dní	den	k1gInPc2	den
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
poté	poté	k6eAd1	poté
krmí	krmit	k5eAaImIp3nS	krmit
též	též	k9	též
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
asi	asi	k9	asi
13	[number]	k4	13
-	-	kIx~	-
17	[number]	k4	17
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlas	hlas	k1gInSc1	hlas
==	==	k?	==
</s>
</p>
<p>
<s>
Vábení	vábení	k1gNnSc1	vábení
je	být	k5eAaImIp3nS	být
opakované	opakovaný	k2eAgNnSc1d1	opakované
"	"	kIx"	"
<g/>
jiv	jiv	k?	jiv
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
jiv-tek	jivek	k1gInSc1	jiv-tek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
<g/>
,	,	kIx,	,
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
variacemi	variace	k1gFnPc7	variace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přednášený	přednášený	k2eAgInSc1d1	přednášený
i	i	k9	i
v	v	k7c6	v
letu	let	k1gInSc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
i	i	k9	i
zpěv	zpěv	k1gInSc1	zpěv
jiných	jiný	k2eAgMnPc2d1	jiný
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Let	léto	k1gNnPc2	léto
==	==	k?	==
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
třepotavý	třepotavý	k2eAgInSc1d1	třepotavý
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlnovkách	vlnovka	k1gFnPc6	vlnovka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
startuje	startovat	k5eAaBmIp3nS	startovat
z	z	k7c2	z
větve	větev	k1gFnSc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
poklonkuje	poklonkovat	k5eAaImIp3nS	poklonkovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Oenanthe	Oenanthe	k6eAd1	Oenanthe
oenanthe	oenanthe	k6eAd1	oenanthe
oenanthe	oenanthat	k5eAaPmIp3nS	oenanthat
–	–	k?	–
většina	většina	k1gFnSc1	většina
areálu	areál	k1gInSc2	areál
výskytu	výskyt	k1gInSc2	výskyt
</s>
</p>
<p>
<s>
Oenanthe	Oenanthe	k1gFnSc1	Oenanthe
oenanthe	oenanthat	k5eAaPmIp3nS	oenanthat
leucorrhoa	leucorrhoa	k6eAd1	leucorrhoa
–	–	k?	–
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
bohatěji	bohatě	k6eAd2	bohatě
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
<g/>
,	,	kIx,	,
než	než	k8xS	než
bělořit	bělořit	k1gMnSc1	bělořit
šedý	šedý	k2eAgMnSc1d1	šedý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oenanthe	Oenanthat	k5eAaPmIp3nS	Oenanthat
oenanthe	oenanthe	k6eAd1	oenanthe
seebohmi	seeboh	k1gFnPc7	seeboh
–	–	k?	–
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
poddruhu	poddruh	k1gInSc2	poddruh
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgInPc1d1	podobný
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Bělořit	bělořit	k1gMnSc1	bělořit
okrový	okrový	k2eAgMnSc1d1	okrový
</s>
</p>
<p>
<s>
Bramborníček	bramborníček	k1gMnSc1	bramborníček
hnědý	hnědý	k2eAgMnSc1d1	hnědý
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Steinbachův	Steinbachův	k2eAgMnSc1d1	Steinbachův
velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
přírodou	příroda	k1gFnSc7	příroda
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
</s>
</p>
<p>
<s>
Ptáci	pták	k1gMnPc1	pták
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
Rob	robit	k5eAaImRp2nS	robit
Hume	Hume	k1gInSc1	Hume
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bělořit	bělořit	k1gMnSc1	bělořit
šedý	šedý	k2eAgMnSc1d1	šedý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
bělořit	bělořit	k1gMnSc1	bělořit
šedý	šedý	k2eAgMnSc1d1	šedý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.rozhlas.cz/hlas/pevci-a/_zprava/belorit-sedy--29824	[url]	k4	http://www.rozhlas.cz/hlas/pevci-a/_zprava/belorit-sedy--29824
</s>
</p>
<p>
<s>
http://www.prirodainfo.cz/karta.php?cislo=284.00	[url]	k4	http://www.prirodainfo.cz/karta.php?cislo=284.00
</s>
</p>
<p>
<s>
http://www.biolib.cz/cz/taxon/id8876/	[url]	k4	http://www.biolib.cz/cz/taxon/id8876/
</s>
</p>
