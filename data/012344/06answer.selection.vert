<s>
Rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
je	být	k5eAaImIp3nS	být
nepravidelně	pravidelně	k6eNd1	pravidelně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
hojnější	hojný	k2eAgMnSc1d2	hojnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
pravidelně	pravidelně	k6eAd1	pravidelně
ale	ale	k8xC	ale
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
na	na	k7c6	na
bezlesých	bezlesý	k2eAgInPc6d1	bezlesý
hřebenech	hřeben	k1gInPc6	hřeben
hor.	hor.	k?	hor.
</s>
