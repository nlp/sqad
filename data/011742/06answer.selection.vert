<s>
Valles	Valles	k1gInSc1	Valles
Marineris	Marineris	k1gFnSc2	Marineris
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
údolí	údolí	k1gNnSc4	údolí
Marineru	Mariner	k1gMnSc3	Mariner
–	–	k?	–
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
sondě	sonda	k1gFnSc6	sonda
Mariner	Marinra	k1gFnPc2	Marinra
9	[number]	k4	9
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc1d1	známý
systém	systém	k1gInSc1	systém
kaňonů	kaňon	k1gInPc2	kaňon
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
nacházející	nacházející	k2eAgFnSc6d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
