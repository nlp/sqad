<p>
<s>
Valles	Valles	k1gInSc1	Valles
Marineris	Marineris	k1gFnSc2	Marineris
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
údolí	údolí	k1gNnSc4	údolí
Marineru	Mariner	k1gMnSc3	Mariner
–	–	k?	–
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
sondě	sonda	k1gFnSc6	sonda
Mariner	Marinra	k1gFnPc2	Marinra
9	[number]	k4	9
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc1d1	známý
systém	systém	k1gInSc1	systém
kaňonů	kaňon	k1gInPc2	kaňon
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
nacházející	nacházející	k2eAgFnSc6d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šestinu	šestina	k1gFnSc4	šestina
obvodu	obvod	k1gInSc2	obvod
rovníku	rovník	k1gInSc2	rovník
planety	planeta	k1gFnSc2	planeta
východně	východně	k6eAd1	východně
od	od	k7c2	od
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
sopečně-tektonické	sopečněektonický	k2eAgFnSc2d1	sopečně-tektonický
oblasti	oblast	k1gFnSc2	oblast
Tharsis	Tharsis	k1gFnSc2	Tharsis
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
200	[number]	k4	200
km	km	kA	km
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místy	místy	k6eAd1	místy
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Svahy	svah	k1gInPc1	svah
se	se	k3xPyFc4	se
zařezávají	zařezávat	k5eAaImIp3nP	zařezávat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Valles	Valles	k1gInSc1	Valles
Marineris	Marineris	k1gFnSc2	Marineris
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
množstvím	množství	k1gNnSc7	množství
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgInPc2d1	propojený
kaňonů	kaňon	k1gInPc2	kaňon
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
Noctis	Noctis	k1gFnSc2	Noctis
Labyrinthus	Labyrinthus	k1gInSc1	Labyrinthus
<g/>
,	,	kIx,	,
regionu	region	k1gInSc3	region
Chryse	Chryse	k1gFnSc2	Chryse
<g/>
,	,	kIx,	,
Ius	Ius	k1gFnSc2	Ius
<g/>
,	,	kIx,	,
Tithonium	Tithonium	k1gNnSc1	Tithonium
<g/>
,	,	kIx,	,
Melas	melasa	k1gFnPc2	melasa
<g/>
,	,	kIx,	,
Candor	Candora	k1gFnPc2	Candora
<g/>
,	,	kIx,	,
Ophir	Ophira	k1gFnPc2	Ophira
<g/>
,	,	kIx,	,
Coprates	Copratesa	k1gFnPc2	Copratesa
<g/>
,	,	kIx,	,
Eos	Eos	k1gFnPc2	Eos
a	a	k8xC	a
Ganges	Ganges	k1gInSc1	Ganges
Chasmata	Chasma	k1gNnPc1	Chasma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
dodatečně	dodatečně	k6eAd1	dodatečně
např.	např.	kA	např.
vodní	vodní	k2eAgFnSc7d1	vodní
erozí	eroze	k1gFnSc7	eroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
vědců	vědec	k1gMnPc2	vědec
mohl	moct	k5eAaImAgInS	moct
vznik	vznik	k1gInSc1	vznik
Tharsis	Tharsis	k1gFnSc4	Tharsis
přispět	přispět	k5eAaPmF	přispět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Valles	Vallesa	k1gFnPc2	Vallesa
Marineris	Marineris	k1gFnSc4	Marineris
extrémním	extrémní	k2eAgNnSc7d1	extrémní
namáháním	namáhání	k1gNnSc7	namáhání
litosféry	litosféra	k1gFnSc2	litosféra
během	během	k7c2	během
jejího	její	k3xOp3gNnSc2	její
formování	formování	k1gNnSc2	formování
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
část	část	k1gFnSc1	část
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Valles	Valles	k1gMnSc1	Valles
Marineris	Marineris	k1gInSc4	Marineris
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
výsledkem	výsledek	k1gInSc7	výsledek
tektonických	tektonický	k2eAgInPc2d1	tektonický
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
sérií	série	k1gFnPc2	série
kolapsů	kolaps	k1gInPc2	kolaps
či	či	k8xC	či
souběhem	souběh	k1gInSc7	souběh
obou	dva	k4xCgFnPc6	dva
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
vědecká	vědecký	k2eAgFnSc1d1	vědecká
studie	studie	k1gFnSc1	studie
dokládající	dokládající	k2eAgFnSc1d1	dokládající
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
nejhlubšího	hluboký	k2eAgInSc2d3	nejhlubší
kaňonu	kaňon	k1gInSc2	kaňon
Coprates	Coprates	k1gInSc1	Coprates
Chasma	Chasma	k1gFnSc1	Chasma
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
sopečné	sopečný	k2eAgFnPc4d1	sopečná
pole	pole	k1gFnPc4	pole
tvořené	tvořený	k2eAgFnPc4d1	tvořená
sypanými	sypaný	k2eAgInPc7d1	sypaný
kužely	kužel	k1gInPc7	kužel
a	a	k8xC	a
lávovými	lávový	k2eAgInPc7d1	lávový
proudy	proud	k1gInPc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
části	část	k1gFnSc6	část
stěn	stěna	k1gFnPc2	stěna
roklí	rokle	k1gFnPc2	rokle
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
až	až	k9	až
na	na	k7c4	na
desítky	desítka	k1gFnPc4	desítka
různých	různý	k2eAgFnPc2d1	různá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
lávové	lávový	k2eAgInPc1d1	lávový
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikaly	vznikat	k5eAaImAgFnP	vznikat
asi	asi	k9	asi
před	před	k7c7	před
3,8	[number]	k4	3,8
až	až	k9	až
3,5	[number]	k4	3,5
mld.	mld.	k?	mld.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
Valles	Valles	k1gInSc1	Valles
Marineris	Marineris	k1gInSc1	Marineris
způsobený	způsobený	k2eAgInSc1d1	způsobený
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
sousední	sousední	k2eAgFnSc1d1	sousední
oblast	oblast	k1gFnSc1	oblast
Tharsis	Tharsis	k1gFnSc2	Tharsis
<g/>
,	,	kIx,	,
je	on	k3xPp3gNnSc4	on
nejjendoduším	jjendodušet	k5eNaImIp1nS	jjendodušet
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
Marinerů	Mariner	k1gMnPc2	Mariner
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přirovnat	přirovnat	k5eAaPmF	přirovnat
propadlině	propadlina	k1gFnSc3	propadlina
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
především	především	k6eAd1	především
hlubší	hluboký	k2eAgFnSc1d2	hlubší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
utváření	utváření	k1gNnSc6	utváření
profilu	profil	k1gInSc2	profil
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
podílet	podílet	k5eAaImF	podílet
i	i	k9	i
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
to	ten	k3xDgNnSc1	ten
voda	voda	k1gFnSc1	voda
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
jako	jako	k8xC	jako
řeka	řeka	k1gFnSc1	řeka
měla	mít	k5eAaImAgFnS	mít
modelovat	modelovat	k5eAaImF	modelovat
hlavní	hlavní	k2eAgInSc4d1	hlavní
útvar	útvar	k1gInSc4	útvar
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
svažující	svažující	k2eAgFnSc2d1	svažující
se	se	k3xPyFc4	se
od	od	k7c2	od
výšin	výšina	k1gFnPc2	výšina
oblasti	oblast	k1gFnSc2	oblast
Tharsis	Tharsis	k1gFnSc2	Tharsis
na	na	k7c6	na
západě	západ	k1gInSc6	západ
k	k	k7c3	k
nízko	nízko	k6eAd1	nízko
položenému	položený	k2eAgInSc3d1	položený
konci	konec	k1gInSc3	konec
údolí	údolí	k1gNnSc2	údolí
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
