<s>
Mozkovna	mozkovna	k1gFnSc1	mozkovna
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
neurocranium	neurocranium	k1gNnSc1	neurocranium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
lebky	lebka	k1gFnSc2	lebka
chránící	chránící	k2eAgInSc4d1	chránící
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
mozkovna	mozkovna	k1gFnSc1	mozkovna
větší	veliký	k2eAgFnSc1d2	veliký
něž	jenž	k3xRgFnPc4	jenž
obličejová	obličejový	k2eAgFnSc1d1	obličejová
část	část	k1gFnSc1	část
lebky	lebka	k1gFnSc2	lebka
(	(	kIx(	(
<g/>
viscerocranium	viscerocranium	k1gNnSc1	viscerocranium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
výraznější	výrazný	k2eAgNnSc1d2	výraznější
u	u	k7c2	u
lebky	lebka	k1gFnSc2	lebka
novorozence	novorozenec	k1gMnSc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Mozkovna	mozkovna	k1gFnSc1	mozkovna
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
lebeční	lebeční	k2eAgFnSc2d1	lebeční
klenby	klenba	k1gFnSc2	klenba
(	(	kIx(	(
<g/>
calvaria	calvarium	k1gNnSc2	calvarium
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
lebeční	lebeční	k2eAgFnSc2d1	lebeční
spodiny	spodina	k1gFnSc2	spodina
(	(	kIx(	(
<g/>
basis	basis	k?	basis
cranii	cranie	k1gFnSc4	cranie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lebeční	lebeční	k2eAgFnSc4d1	lebeční
klenbu	klenba	k1gFnSc4	klenba
tvoří	tvořit	k5eAaImIp3nS	tvořit
kost	kost	k1gFnSc1	kost
čelní	čelní	k2eAgFnSc1d1	čelní
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
kostí	kost	k1gFnPc2	kost
temenní	temenní	k2eAgNnSc1d1	temenní
<g/>
,	,	kIx,	,
kost	kost	k1gFnSc1	kost
týlní	týlní	k2eAgFnSc1d1	týlní
a	a	k8xC	a
kost	kost	k1gFnSc1	kost
spánková	spánkový	k2eAgFnSc1d1	spánková
<g/>
.	.	kIx.	.
</s>
<s>
Lebeční	lebeční	k2eAgFnSc4d1	lebeční
spodinu	spodina	k1gFnSc4	spodina
pak	pak	k6eAd1	pak
kost	kost	k1gFnSc1	kost
spánková	spánkový	k2eAgFnSc1d1	spánková
<g/>
,	,	kIx,	,
klínová	klínový	k2eAgFnSc1d1	klínová
a	a	k8xC	a
čichová	čichový	k2eAgFnSc1d1	čichová
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
spojeny	spojen	k2eAgInPc1d1	spojen
švy	šev	k1gInPc1	šev
<g/>
.	.	kIx.	.
</s>
