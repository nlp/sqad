<p>
<s>
Líza	Líza	k1gFnSc1	Líza
Marie	Marie	k1gFnSc1	Marie
Simpsonová	Simpsonová	k1gFnSc1	Simpsonová
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lisa	Lisa	k1gFnSc1	Lisa
Marie	Maria	k1gFnSc2	Maria
Simpson	Simpsona	k1gFnPc2	Simpsona
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
ji	on	k3xPp3gFnSc4	on
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
inteligenci	inteligence	k1gFnSc4	inteligence
a	a	k8xC	a
samé	samý	k3xTgFnPc4	samý
jedničky	jednička	k1gFnPc4	jednička
vnímají	vnímat	k5eAaImIp3nP	vnímat
jako	jako	k9	jako
šprtku	šprtku	k?	šprtku
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
postupnému	postupný	k2eAgNnSc3d1	postupné
vyčleňování	vyčleňování	k1gNnSc3	vyčleňování
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Miluje	milovat	k5eAaImIp3nS	milovat
poníky	poník	k1gMnPc7	poník
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Bartem	Bart	k1gMnSc7	Bart
se	se	k3xPyFc4	se
rádi	rád	k2eAgMnPc1d1	rád
koukají	koukat	k5eAaImIp3nP	koukat
na	na	k7c4	na
animovaný	animovaný	k2eAgInSc4d1	animovaný
seriál	seriál	k1gInSc4	seriál
Itchy	Itcha	k1gFnSc2	Itcha
&	&	k?	&
Scratchy	Scratcha	k1gFnSc2	Scratcha
Show	show	k1gFnSc2	show
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
vzorná	vzorný	k2eAgFnSc1d1	vzorná
žačka	žačka	k1gFnSc1	žačka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
samé	samý	k3xTgFnPc4	samý
jedničky	jednička	k1gFnPc4	jednička
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
její	její	k3xOp3gMnSc1	její
bratr	bratr	k1gMnSc1	bratr
Bart	Bart	k1gMnSc1	Bart
má	mít	k5eAaImIp3nS	mít
samé	samý	k3xTgFnSc2	samý
pětky	pětka	k1gFnSc2	pětka
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
se	se	k3xPyFc4	se
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
provokují	provokovat	k5eAaImIp3nP	provokovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
společné	společný	k2eAgInPc1d1	společný
<g/>
,	,	kIx,	,
milují	milovat	k5eAaImIp3nP	milovat
Šášu	Šáša	k1gFnSc4	Šáša
Krustyho	Krusty	k1gMnSc2	Krusty
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
ji	on	k3xPp3gFnSc4	on
namluvila	namluvit	k5eAaPmAgFnS	namluvit
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
Yeardley	Yeardlea	k1gFnSc2	Yeardlea
Smithová	Smithová	k1gFnSc1	Smithová
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
znění	znění	k1gNnSc6	znění
pak	pak	k6eAd1	pak
až	až	k9	až
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
Helena	Helena	k1gFnSc1	Helena
Štáchová	Štáchová	k1gFnSc1	Štáchová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
Štáchové	Štáchová	k1gFnSc2	Štáchová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
dabuje	dabovat	k5eAaBmIp3nS	dabovat
Lízin	Lízin	k2eAgInSc4d1	Lízin
hlas	hlas	k1gInSc4	hlas
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivana	k1gFnSc1	Ivana
Korolová	Korolový	k2eAgFnSc1d1	Korolová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
postavy	postava	k1gFnSc2	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
sestrou	sestra	k1gFnSc7	sestra
Barta	Barta	k1gFnSc1	Barta
Simpsona	Simpsona	k1gFnSc1	Simpsona
a	a	k8xC	a
Maggie	Maggie	k1gFnSc1	Maggie
Simpsonové	Simpsonová	k1gFnSc2	Simpsonová
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
Homera	Homero	k1gNnSc2	Homero
a	a	k8xC	a
Marge	Marge	k1gNnSc2	Marge
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
společně	společně	k6eAd1	společně
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
ve	v	k7c6	v
Springfieldu	Springfieldo	k1gNnSc6	Springfieldo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Líza	Líza	k1gFnSc1	Líza
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
dívka	dívka	k1gFnSc1	dívka
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
IQ	iq	kA	iq
je	být	k5eAaImIp3nS	být
156	[number]	k4	156
–	–	k?	–
epizoda	epizoda	k1gFnSc1	epizoda
Chytrý	chytrý	k2eAgMnSc1d1	chytrý
a	a	k8xC	a
chytřejší	chytrý	k2eAgMnSc1d2	chytřejší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vždy	vždy	k6eAd1	vždy
zdá	zdát	k5eAaImIp3nS	zdát
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
největším	veliký	k2eAgInSc7d3	veliký
koníčkem	koníček	k1gInSc7	koníček
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
saxofon	saxofon	k1gInSc4	saxofon
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
především	především	k9	především
jazz	jazz	k1gInSc4	jazz
a	a	k8xC	a
blues	blues	k1gNnSc4	blues
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
LP	LP	kA	LP
je	být	k5eAaImIp3nS	být
Birth	Birth	k1gInSc1	Birth
of	of	k?	of
the	the	k?	the
Cool	Cool	k1gMnSc1	Cool
od	od	k7c2	od
Milese	Milese	k1gFnSc2	Milese
Davise	Davise	k1gFnSc2	Davise
<g/>
.	.	kIx.	.
</s>
<s>
Líza	Líza	k1gFnSc1	Líza
je	být	k5eAaImIp3nS	být
buddhistka	buddhistka	k1gFnSc1	buddhistka
<g/>
,	,	kIx,	,
ráda	rád	k2eAgFnSc1d1	ráda
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
historickými	historický	k2eAgInPc7d1	historický
<g/>
,	,	kIx,	,
filozofickými	filozofický	k2eAgInPc7d1	filozofický
nebo	nebo	k8xC	nebo
vědeckými	vědecký	k2eAgInPc7d1	vědecký
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastává	zastávat	k5eAaImIp3nS	zastávat
levicové	levicový	k2eAgInPc4d1	levicový
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Ráda	rád	k2eAgFnSc1d1	ráda
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
recyklovat	recyklovat	k5eAaBmF	recyklovat
odpad	odpad	k1gInSc4	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
stromečkem	stromeček	k1gInSc7	stromeček
by	by	kYmCp3nS	by
však	však	k9	však
nejraději	rád	k6eAd3	rád
viděla	vidět	k5eAaImAgFnS	vidět
poníka	poník	k1gMnSc4	poník
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kočku	kočka	k1gFnSc4	kočka
Sněhulku	Sněhulka	k1gFnSc4	Sněhulka
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapřisáhlou	zapřisáhlý	k2eAgFnSc7d1	zapřisáhlá
vegetariánkou	vegetariánka	k1gFnSc7	vegetariánka
a	a	k8xC	a
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
aktivistkou	aktivistka	k1gFnSc7	aktivistka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matt	Matt	k2eAgInSc4d1	Matt
Groening	Groening	k1gInSc4	Groening
jí	on	k3xPp3gFnSc7	on
dal	dát	k5eAaPmAgMnS	dát
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
sestře	sestra	k1gFnSc6	sestra
Lise	Lisa	k1gFnSc6	Lisa
Groeningové	Groeningový	k2eAgNnSc1d1	Groeningový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jejího	její	k3xOp3gMnSc4	její
bratra	bratr	k1gMnSc4	bratr
Barta	Bart	k1gMnSc4	Bart
<g/>
,	,	kIx,	,
pojmenují	pojmenovat	k5eAaPmIp3nP	pojmenovat
rodiče	rodič	k1gMnPc1	rodič
Lízu	líz	k1gInSc2	líz
po	po	k7c6	po
vlaku	vlak	k1gInSc6	vlak
(	(	kIx(	(
<g/>
LI	li	k9	li
<g/>
'	'	kIx"	'
<g/>
L	L	kA	L
LISA	Lisa	k1gFnSc1	Lisa
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
vláčku	vláček	k1gInSc2	vláček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
projížděli	projíždět	k5eAaImAgMnP	projíždět
Marge	Marge	k1gFnPc2	Marge
a	a	k8xC	a
Homer	Homra	k1gFnPc2	Homra
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
výročí	výročí	k1gNnSc4	výročí
svatby	svatba	k1gFnSc2	svatba
–	–	k?	–
epizoda	epizoda	k1gFnSc1	epizoda
Výročí	výročí	k1gNnPc2	výročí
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živila	živit	k5eAaImAgFnS	živit
se	se	k3xPyFc4	se
hlídáním	hlídání	k1gNnSc7	hlídání
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
později	pozdě	k6eAd2	pozdě
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
(	(	kIx(	(
<g/>
učí	učit	k5eAaImIp3nP	učit
děti	dítě	k1gFnPc1	dítě
Brandine	Brandin	k1gInSc5	Brandin
a	a	k8xC	a
Cletuse	Cletus	k1gInSc6	Cletus
Spucklerových	Spucklerová	k1gFnPc2	Spucklerová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měla	mít	k5eAaImAgFnS	mít
poníka	poník	k1gMnSc4	poník
Princeznu	princezna	k1gFnSc4	princezna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
epizoda	epizoda	k1gFnSc1	epizoda
Cena	cena	k1gFnSc1	cena
lásky	láska	k1gFnSc2	láska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Homer	Homer	k1gInSc1	Homer
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
funkční	funkční	k2eAgInSc1d1	funkční
jaderný	jaderný	k2eAgInSc1d1	jaderný
reaktor	reaktor	k1gInSc1	reaktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tiskla	tisknout	k5eAaImAgFnS	tisknout
svoje	svůj	k3xOyFgFnPc4	svůj
noviny	novina	k1gFnPc4	novina
Líziny	Lízin	k2eAgFnSc2d1	Lízina
listy	lista	k1gFnSc2	lista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
slovo	slovo	k1gNnSc1	slovo
bylo	být	k5eAaImAgNnS	být
Bart	Bart	k1gInSc4	Bart
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Epizoda	epizoda	k1gFnSc1	epizoda
Lízino	Lízin	k2eAgNnSc4d1	Lízino
první	první	k4xOgNnSc4	první
slovo	slovo	k1gNnSc4	slovo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
hokejovým	hokejový	k2eAgMnSc7d1	hokejový
brankářem	brankář	k1gMnSc7	brankář
(	(	kIx(	(
<g/>
zpočátku	zpočátku	k6eAd1	zpočátku
z	z	k7c2	z
donucení	donucení	k1gNnSc2	donucení
–	–	k?	–
hrozba	hrozba	k1gFnSc1	hrozba
pětky	pětka	k1gFnSc2	pětka
z	z	k7c2	z
tělocviku	tělocvik	k1gInSc2	tělocvik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trénovala	trénovat	k5eAaImAgFnS	trénovat
také	také	k9	také
baseballový	baseballový	k2eAgInSc4d1	baseballový
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
přísně	přísně	k6eAd1	přísně
vědecky	vědecky	k6eAd1	vědecky
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
Barta	Bart	k1gInSc2	Bart
(	(	kIx(	(
<g/>
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
zvýšit	zvýšit	k5eAaPmF	zvýšit
počet	počet	k1gInSc4	počet
mimoškolních	mimoškolní	k2eAgFnPc2d1	mimoškolní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
panenku	panenka	k1gFnSc4	panenka
Líza	Líza	k1gFnSc1	Líza
Lví	lví	k2eAgNnSc4d1	lví
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
Stacey	Stace	k1gMnPc7	Stace
Lovell	Lovella	k1gFnPc2	Lovella
<g/>
,	,	kIx,	,
tvůrkyní	tvůrkyně	k1gFnPc2	tvůrkyně
panenky	panenka	k1gFnSc2	panenka
Malibu	Maliba	k1gMnSc4	Maliba
Stacy	Staca	k1gFnSc2	Staca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Natočila	natočit	k5eAaBmAgFnS	natočit
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
rodině	rodina	k1gFnSc6	rodina
(	(	kIx(	(
<g/>
úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c6	na
Sundance	Sundanka	k1gFnSc6	Sundanka
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
prorazila	prorazit	k5eAaPmAgFnS	prorazit
i	i	k9	i
v	v	k7c6	v
zábavním	zábavní	k2eAgInSc6d1	zábavní
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Krustyho	Krusty	k1gMnSc4	Krusty
v	v	k7c4	v
televizi	televize	k1gFnSc4	televize
(	(	kIx(	(
<g/>
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
nepokračovat	pokračovat	k5eNaImF	pokračovat
v	v	k7c6	v
klaunské	klaunský	k2eAgFnSc6d1	klaunská
kariéře	kariéra	k1gFnSc6	kariéra
po	po	k7c6	po
rozhovoru	rozhovor	k1gInSc6	rozhovor
Melvinem	Melvin	k1gMnSc7	Melvin
Van	vana	k1gFnPc2	vana
Hornem	Horn	k1gMnSc7	Horn
–	–	k?	–
podpulťák	podpulťák	k1gMnSc1	podpulťák
Mel	mlít	k5eAaImRp2nS	mlít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bártem	Bártem	k?	Bártem
napsala	napsat	k5eAaBmAgFnS	napsat
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
epizodu	epizoda	k1gFnSc4	epizoda
The	The	k1gFnSc2	The
Itchy	Itcha	k1gFnSc2	Itcha
&	&	k?	&
Scratchy	Scratcha	k1gFnSc2	Scratcha
Show	show	k1gFnSc2	show
s	s	k7c7	s
názvem	název	k1gInSc7	název
Malé	Malé	k2eAgNnSc4d1	Malé
holičství	holičství	k1gNnSc4	holičství
hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
autorství	autorství	k1gNnSc1	autorství
je	být	k5eAaImIp3nS	být
všemi	všecek	k3xTgFnPc7	všecek
připisováno	připisovat	k5eAaImNgNnS	připisovat
Abrahamu	Abraham	k1gMnSc6	Abraham
Simpsonovi	Simpson	k1gMnSc6	Simpson
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
epizoda	epizoda	k1gFnSc1	epizoda
plná	plný	k2eAgFnSc1d1	plná
brutálních	brutální	k2eAgMnPc2d1	brutální
animáků	animák	k1gMnPc2	animák
znovu	znovu	k6eAd1	znovu
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
Lísiny	Lísin	k1gInPc4	Lísin
umělecké	umělecký	k2eAgFnSc2d1	umělecká
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
za	za	k7c4	za
než	než	k8xS	než
získá	získat	k5eAaPmIp3nS	získat
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
IQ	iq	kA	iq
156	[number]	k4	156
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
vede	vést	k5eAaImIp3nS	vést
účetnictví	účetnictví	k1gNnSc1	účetnictví
celé	celý	k2eAgFnSc2d1	celá
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gFnPc4	její
veškeré	veškerý	k3xTgFnPc4	veškerý
finance	finance	k1gFnPc4	finance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
italštinu	italština	k1gFnSc4	italština
(	(	kIx(	(
<g/>
naučila	naučit	k5eAaPmAgFnS	naučit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
od	od	k7c2	od
Milhouse	Milhouse	k1gFnSc1	Milhouse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
také	také	k9	také
hebrejsky	hebrejsky	k6eAd1	hebrejsky
(	(	kIx(	(
<g/>
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c7	za
příliš	příliš	k6eAd1	příliš
dobrou	dobrý	k2eAgFnSc7d1	dobrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
Milhousem	Milhous	k1gInSc7	Milhous
Van	van	k1gInSc1	van
Houtenem	Houteno	k1gNnSc7	Houteno
<g/>
,	,	kIx,	,
Ralphem	Ralph	k1gInSc7	Ralph
Wiggumem	Wiggum	k1gInSc7	Wiggum
a	a	k8xC	a
Nelsonem	Nelson	k1gMnSc7	Nelson
Muntzem	Muntz	k1gMnSc7	Muntz
<g/>
,	,	kIx,	,
Michaelem	Michael	k1gMnSc7	Michael
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Amico	Amico	k1gMnSc1	Amico
<g/>
,	,	kIx,	,
Nickem	Nicko	k1gNnSc7	Nicko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gInSc1	její
nejoblíbenější	oblíbený	k2eAgInSc1d3	nejoblíbenější
předmět	předmět	k1gInSc1	předmět
je	být	k5eAaImIp3nS	být
dějepis	dějepis	k1gInSc1	dějepis
</s>
</p>
<p>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jebediah	Jebediah	k1gMnSc1	Jebediah
Springfield	Springfield	k1gMnSc1	Springfield
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pirát	pirát	k1gMnSc1	pirát
Hans	Hans	k1gMnSc1	Hans
Sprungfeld	Sprungfeld	k1gMnSc1	Sprungfeld
(	(	kIx(	(
<g/>
epizoda	epizoda	k1gFnSc1	epizoda
Líza	Líza	k1gFnSc1	Líza
bortí	bortit	k5eAaImIp3nS	bortit
mýty	mýtus	k1gInPc4	mýtus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
spelympiádu	spelympiáda	k1gFnSc4	spelympiáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Epizoda	epizoda	k1gFnSc1	epizoda
Speluji	Speluji	k1gFnSc1	Speluji
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nejrychleji	rychle	k6eAd3	rychle
dovedu	dovést	k5eAaPmIp1nS	dovést
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zapálenou	zapálený	k2eAgFnSc7d1	zapálená
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
aktivistkou	aktivistka	k1gFnSc7	aktivistka
a	a	k8xC	a
bojovnicí	bojovnice	k1gFnSc7	bojovnice
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
Líza	Líza	k1gFnSc1	Líza
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
příčinou	příčina	k1gFnSc7	příčina
dopadení	dopadení	k1gNnSc2	dopadení
a	a	k8xC	a
uvěznění	uvěznění	k1gNnSc2	uvěznění
Leváka	levák	k1gMnSc2	levák
Boba	Bob	k1gMnSc2	Bob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
vášní	vášeň	k1gFnSc7	vášeň
je	být	k5eAaImIp3nS	být
hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
saxofon	saxofon	k1gInSc4	saxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejími	její	k3xOp3gInPc7	její
vzory	vzor	k1gInPc7	vzor
jsou	být	k5eAaImIp3nP	být
jazzman	jazzman	k1gMnSc1	jazzman
Murphy	Murpha	k1gFnSc2	Murpha
"	"	kIx"	"
<g/>
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
dáseň	dáseň	k1gFnSc1	dáseň
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
naučil	naučit	k5eAaPmAgMnS	naučit
Lízu	Líza	k1gFnSc4	Líza
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudba	hudba	k1gFnSc1	hudba
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
<g/>
)	)	kIx)	)
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
Dewey	Dewea	k1gFnSc2	Dewea
Largo	largo	k6eAd1	largo
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
skladba	skladba	k1gFnSc1	skladba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
neposlouchatelná	poslouchatelný	k2eNgFnSc1d1	neposlouchatelná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
i	i	k9	i
velice	velice	k6eAd1	velice
nadanou	nadaný	k2eAgFnSc7d1	nadaná
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
soutěž	soutěž	k1gFnSc1	soutěž
Springfield	Springfielda	k1gFnPc2	Springfielda
hledá	hledat	k5eAaImIp3nS	hledat
ministar	ministar	k1gInSc1	ministar
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Epizoda	epizoda	k1gFnSc1	epizoda
Springfield	Springfielda	k1gFnPc2	Springfielda
hledá	hledat	k5eAaImIp3nS	hledat
ministar	ministar	k1gInSc1	ministar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
opravdu	opravdu	k6eAd1	opravdu
prožít	prožít	k5eAaPmF	prožít
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
poukáže	poukázat	k5eAaPmIp3nS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vychována	vychovat	k5eAaPmNgFnS	vychovat
v	v	k7c6	v
útlém	útlý	k2eAgNnSc6d1	útlé
dětství	dětství	k1gNnSc6	dětství
pouze	pouze	k6eAd1	pouze
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dělat	dělat	k5eAaImF	dělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
životě	život	k1gInSc6	život
neskončila	skončit	k5eNaPmAgFnS	skončit
jako	jako	k9	jako
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
Líza	Líza	k1gFnSc1	Líza
dokáže	dokázat	k5eAaPmIp3nS	dokázat
propadnout	propadnout	k5eAaPmF	propadnout
různým	různý	k2eAgFnPc3d1	různá
závislostem	závislost	k1gFnPc3	závislost
(	(	kIx(	(
<g/>
antidepresiva	antidepresivum	k1gNnSc2	antidepresivum
–	–	k?	–
epizoda	epizoda	k1gFnSc1	epizoda
Poslední	poslední	k2eAgFnPc1d1	poslední
recept	recept	k1gInSc4	recept
a	a	k8xC	a
Hodný	hodný	k2eAgInSc1d1	hodný
<g/>
,	,	kIx,	,
zlý	zlý	k2eAgInSc1d1	zlý
a	a	k8xC	a
zfetlá	zfetlý	k2eAgFnSc1d1	zfetlá
<g/>
;	;	kIx,	;
pokeru	poker	k1gInSc2	poker
–	–	k?	–
epizoda	epizoda	k1gFnSc1	epizoda
Děda	děd	k1gMnSc2	děd
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
<g/>
;	;	kIx,	;
nekontrolované	kontrolovaný	k2eNgNnSc1d1	nekontrolované
stahování	stahování	k1gNnSc1	stahování
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
–	–	k?	–
epizoda	epizoda	k1gFnSc1	epizoda
MyPody	MyPoda	k1gFnSc2	MyPoda
a	a	k8xC	a
sousedi	soused	k1gMnPc1	soused
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
ošklivku	ošklivka	k1gFnSc4	ošklivka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vice	vika	k1gFnSc3	vika
miss	miss	k1gFnSc2	miss
Springfieldu	Springfield	k1gInSc2	Springfield
(	(	kIx(	(
<g/>
Epizoda	epizoda	k1gFnSc1	epizoda
Líza	Líza	k1gFnSc1	Líza
královnou	královna	k1gFnSc7	královna
krásy	krása	k1gFnSc2	krása
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Její	její	k3xOp3gMnSc1	její
nepřítel	nepřítel	k1gMnSc1	nepřítel
je	být	k5eAaImIp3nS	být
Levák	levák	k1gMnSc1	levák
Bob	Bob	k1gMnSc1	Bob
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
