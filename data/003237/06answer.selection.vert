<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
rysů	rys	k1gInPc2	rys
blackmetalového	blackmetalový	k2eAgInSc2d1	blackmetalový
image	image	k1gInSc2	image
je	být	k5eAaImIp3nS	být
líčení	líčení	k1gNnSc1	líčení
corpsepaint	corpsepainta	k1gFnPc2	corpsepainta
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
napodobující	napodobující	k2eAgFnSc4d1	napodobující
mrtvolu	mrtvola	k1gFnSc4	mrtvola
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
warpaint	warpaint	k1gInSc1	warpaint
černý	černý	k2eAgInSc1d1	černý
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
makeup	makeup	k1gInSc1	makeup
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
napodobující	napodobující	k2eAgFnPc1d1	napodobující
obličejové	obličejový	k2eAgFnPc1d1	obličejová
válečné	válečný	k2eAgFnPc1d1	válečná
barvy	barva	k1gFnPc1	barva
pohanských	pohanský	k2eAgMnPc2d1	pohanský
válečníků	válečník	k1gMnPc2	válečník
<g/>
,	,	kIx,	,
majících	mající	k2eAgMnPc2d1	mající
zastrašit	zastrašit	k5eAaPmF	zastrašit
nepřítele	nepřítel	k1gMnSc4	nepřítel
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bitvy	bitva	k1gFnSc2	bitva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
falešnou	falešný	k2eAgFnSc7d1	falešná
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
