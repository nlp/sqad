<s>
Optický	optický	k2eAgInSc1d1	optický
dalekohled	dalekohled	k1gInSc1	dalekohled
či	či	k8xC	či
teleskop	teleskop	k1gInSc1	teleskop
je	být	k5eAaImIp3nS	být
přístroj	přístroj	k1gInSc4	přístroj
k	k	k7c3	k
optickému	optický	k2eAgNnSc3d1	optické
přiblížení	přiblížení	k1gNnSc3	přiblížení
pomocí	pomoc	k1gFnPc2	pomoc
dvou	dva	k4xCgFnPc2	dva
soustav	soustava	k1gFnPc2	soustava
čoček	čočka	k1gFnPc2	čočka
nebo	nebo	k8xC	nebo
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
:	:	kIx,	:
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
okuláru	okulár	k1gInSc2	okulár
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
parametry	parametr	k1gInPc7	parametr
optických	optický	k2eAgInPc2d1	optický
dalekohledů	dalekohled	k1gInPc2	dalekohled
jsou	být	k5eAaImIp3nP	být
světelnost	světelnost	k1gFnSc4	světelnost
a	a	k8xC	a
zvětšení	zvětšení	k1gNnSc4	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Opticky	opticky	k6eAd1	opticky
účinná	účinný	k2eAgFnSc1d1	účinná
plocha	plocha	k1gFnSc1	plocha
objektivu	objektiv	k1gInSc2	objektiv
(	(	kIx(	(
<g/>
apertura	apertura	k1gFnSc1	apertura
<g/>
)	)	kIx)	)
určuje	určovat	k5eAaImIp3nS	určovat
světelnost	světelnost	k1gFnSc1	světelnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
ohniskových	ohniskový	k2eAgFnPc2d1	ohnisková
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
okuláru	okulár	k1gInSc2	okulár
jeho	on	k3xPp3gNnSc2	on
zvětšení	zvětšení	k1gNnSc2	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
konstrukce	konstrukce	k1gFnSc2	konstrukce
objektivu	objektiv	k1gInSc2	objektiv
se	se	k3xPyFc4	se
optické	optický	k2eAgInPc1d1	optický
dalekohledy	dalekohled	k1gInPc1	dalekohled
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
refraktory	refraktor	k1gInPc4	refraktor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
objektiv	objektiv	k1gInSc1	objektiv
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
čočkou	čočka	k1gFnSc7	čočka
nebo	nebo	k8xC	nebo
soustavou	soustava	k1gFnSc7	soustava
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
reflektory	reflektor	k1gInPc7	reflektor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
objektiv	objektiv	k1gInSc1	objektiv
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hlavního	hlavní	k2eAgNnSc2d1	hlavní
určení	určení	k1gNnSc2	určení
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dalekohledy	dalekohled	k1gInPc1	dalekohled
astronomické	astronomický	k2eAgInPc1d1	astronomický
dalekohledy	dalekohled	k1gInPc1	dalekohled
pozemní	pozemní	k2eAgFnSc2d1	pozemní
(	(	kIx(	(
<g/>
terestrické	terestrický	k2eAgFnSc2d1	terestrická
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
zaměřovacích	zaměřovací	k2eAgFnPc2d1	zaměřovací
a	a	k8xC	a
geodetických	geodetický	k2eAgFnPc2d1	geodetická
divadelní	divadelní	k2eAgNnPc1d1	divadelní
kukátka	kukátko	k1gNnPc1	kukátko
triedry	triedr	k1gInPc4	triedr
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
teleskop	teleskop	k1gInSc1	teleskop
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
τ	τ	k?	τ
–	–	k?	–
téle	téle	k6eAd1	téle
znamená	znamenat	k5eAaImIp3nS	znamenat
daleko	daleko	k6eAd1	daleko
a	a	k8xC	a
σ	σ	k?	σ
–	–	k?	–
skopein	skopein	k1gInSc1	skopein
znamená	znamenat	k5eAaImIp3nS	znamenat
hledět	hledět	k5eAaImF	hledět
<g/>
.	.	kIx.	.
τ	τ	k?	τ
–	–	k?	–
teleskopos	teleskopos	k1gInSc1	teleskopos
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
daleko-hled	dalekoled	k1gInSc1	daleko-hled
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
dalekohled	dalekohled	k1gInSc4	dalekohled
si	se	k3xPyFc3	se
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1608	[number]	k4	1608
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
holandský	holandský	k2eAgMnSc1d1	holandský
optik	optik	k1gMnSc1	optik
Hans	Hans	k1gMnSc1	Hans
Lippershey	Lippershea	k1gFnSc2	Lippershea
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc4	jeho
poznatky	poznatek	k1gInPc4	poznatek
použil	použít	k5eAaPmAgInS	použít
již	již	k6eAd1	již
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgMnSc1d1	známý
italský	italský	k2eAgMnSc1d1	italský
vědec	vědec	k1gMnSc1	vědec
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnSc7	Galilei
a	a	k8xC	a
pomocí	pomoc	k1gFnSc7	pomoc
zdokonaleného	zdokonalený	k2eAgInSc2d1	zdokonalený
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
složeného	složený	k2eAgInSc2d1	složený
ze	z	k7c2	z
spojky	spojka	k1gFnSc2	spojka
a	a	k8xC	a
rozptylky	rozptylka	k1gFnSc2	rozptylka
učinil	učinit	k5eAaImAgMnS	učinit
řadu	řada	k1gFnSc4	řada
převratných	převratný	k2eAgInPc2d1	převratný
objevů	objev	k1gInPc2	objev
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Jupiterovy	Jupiterův	k2eAgInPc4d1	Jupiterův
měsíce	měsíc	k1gInPc4	měsíc
nebo	nebo	k8xC	nebo
skvrny	skvrna	k1gFnPc4	skvrna
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
Slunce	slunce	k1gNnSc4	slunce
si	se	k3xPyFc3	se
nechránil	chránit	k5eNaImAgMnS	chránit
zrak	zrak	k1gInSc4	zrak
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
oslepl	oslepnout	k5eAaPmAgMnS	oslepnout
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc4	dalekohled
dále	daleko	k6eAd2	daleko
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
dvou	dva	k4xCgInPc6	dva
spojek	spojka	k1gFnPc2	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
tak	tak	k6eAd1	tak
sice	sice	k8xC	sice
převrácený	převrácený	k2eAgInSc1d1	převrácený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ostřejší	ostrý	k2eAgInSc1d2	ostřejší
obraz	obraz	k1gInSc1	obraz
a	a	k8xC	a
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
dalekohledu	dalekohled	k1gInSc2	dalekohled
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vložit	vložit	k5eAaPmF	vložit
například	například	k6eAd1	například
nitkový	nitkový	k2eAgInSc4d1	nitkový
kříž	kříž	k1gInSc4	kříž
pro	pro	k7c4	pro
přesnější	přesný	k2eAgNnSc4d2	přesnější
zaměření	zaměření	k1gNnSc4	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
astronomických	astronomický	k2eAgInPc2d1	astronomický
dalekohledů	dalekohled	k1gInPc2	dalekohled
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
dalekohledům	dalekohled	k1gInPc3	dalekohled
stále	stále	k6eAd1	stále
delším	dlouhý	k2eAgInPc3d2	delší
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
zvětšení	zvětšení	k1gNnSc3	zvětšení
<g/>
)	)	kIx)	)
i	i	k8xC	i
hmotnějším	hmotný	k2eAgMnSc6d2	hmotnější
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
světelnosti	světelnost	k1gFnSc3	světelnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
stále	stále	k6eAd1	stále
obtížněji	obtížně	k6eAd2	obtížně
použitelné	použitelný	k2eAgNnSc1d1	použitelné
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
refraktory	refraktor	k1gInPc1	refraktor
měřily	měřit	k5eAaImAgInP	měřit
kolem	kolem	k7c2	kolem
60	[number]	k4	60
m.	m.	k?	m.
První	první	k4xOgInSc1	první
reflektor	reflektor	k1gInSc1	reflektor
<g/>
,	,	kIx,	,
dalekohled	dalekohled	k1gInSc1	dalekohled
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
jako	jako	k8xS	jako
objektivem	objektiv	k1gInSc7	objektiv
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
a	a	k8xC	a
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
tak	tak	k6eAd1	tak
problém	problém	k1gInSc1	problém
chromatické	chromatický	k2eAgFnSc2d1	chromatická
čili	čili	k8xC	čili
barevné	barevný	k2eAgFnSc2d1	barevná
vady	vada	k1gFnSc2	vada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
pro	pro	k7c4	pro
světlo	světlo	k1gNnSc4	světlo
různé	různý	k2eAgFnSc2d1	různá
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
v	v	k7c6	v
čočce	čočka	k1gFnSc6	čočka
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	s	k7c7	s
"	"	kIx"	"
<g/>
duhovými	duhový	k2eAgInPc7d1	duhový
okraji	okraj	k1gInPc7	okraj
<g/>
"	"	kIx"	"
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začínají	začínat	k5eAaImIp3nP	začínat
pro	pro	k7c4	pro
astronomické	astronomický	k2eAgInPc4d1	astronomický
účely	účel	k1gInPc4	účel
převládat	převládat	k5eAaImF	převládat
reflektory	reflektor	k1gInPc4	reflektor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
velkých	velký	k2eAgInPc2d1	velký
průměrů	průměr	k1gInPc2	průměr
lze	lze	k6eAd1	lze
snáze	snadno	k6eAd2	snadno
vyrobit	vyrobit	k5eAaPmF	vyrobit
a	a	k8xC	a
také	také	k9	také
konstrukce	konstrukce	k1gFnSc1	konstrukce
dalekohledu	dalekohled	k1gInSc2	dalekohled
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
současné	současný	k2eAgInPc4d1	současný
reflektory	reflektor	k1gInPc4	reflektor
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
kolem	kolem	k7c2	kolem
10	[number]	k4	10
m	m	kA	m
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
dalekohled	dalekohled	k1gInSc1	dalekohled
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgFnSc6d1	Ondřejova
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
2	[number]	k4	2
m.	m.	k?	m.
Pro	pro	k7c4	pro
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInPc4d2	veliký
projekty	projekt	k1gInPc4	projekt
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
automaticky	automaticky	k6eAd1	automaticky
koordinovaných	koordinovaný	k2eAgFnPc2d1	koordinovaná
soustav	soustava	k1gFnPc2	soustava
segmentovaných	segmentovaný	k2eAgNnPc2d1	segmentované
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
je	být	k5eAaImIp3nS	být
reflektor	reflektor	k1gInSc4	reflektor
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
2,4	[number]	k4	2,4
m	m	kA	m
o	o	k7c6	o
ohniskové	ohniskový	k2eAgFnSc6d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
m.	m.	k?	m.
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
probíhá	probíhat	k5eAaImIp3nS	probíhat
rychlý	rychlý	k2eAgInSc4d1	rychlý
vývoj	vývoj	k1gInSc4	vývoj
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
využívajících	využívající	k2eAgInPc2d1	využívající
techniku	technika	k1gFnSc4	technika
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
získat	získat	k5eAaPmF	získat
velice	velice	k6eAd1	velice
dobré	dobrý	k2eAgInPc4d1	dobrý
výsledky	výsledek	k1gInPc4	výsledek
i	i	k9	i
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
vyslat	vyslat	k5eAaPmF	vyslat
dalekohled	dalekohled	k1gInSc4	dalekohled
mimo	mimo	k7c4	mimo
rušivý	rušivý	k2eAgInSc4d1	rušivý
vliv	vliv	k1gInSc4	vliv
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pozorování	pozorování	k1gNnPc4	pozorování
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
než	než	k8xS	než
optické	optický	k2eAgInPc4d1	optický
dalekohledy	dalekohled	k1gInPc4	dalekohled
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
radioteleskopy	radioteleskop	k1gInPc4	radioteleskop
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
elektromagnetickými	elektromagnetický	k2eAgFnPc7d1	elektromagnetická
vlnami	vlna	k1gFnPc7	vlna
větší	veliký	k2eAgFnSc2d2	veliký
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
s	s	k7c7	s
anténami	anténa	k1gFnPc7	anténa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přenosné	přenosný	k2eAgInPc4d1	přenosný
pozemní	pozemní	k2eAgInPc4d1	pozemní
dalekohledy	dalekohled	k1gInPc4	dalekohled
činila	činit	k5eAaImAgFnS	činit
potíže	potíž	k1gFnPc4	potíž
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
námořních	námořní	k2eAgInPc2d1	námořní
dalekohledů	dalekohled	k1gInPc2	dalekohled
se	se	k3xPyFc4	se
problém	problém	k1gInSc1	problém
řešil	řešit	k5eAaImAgInS	řešit
zasunovacím	zasunovací	k2eAgInSc7d1	zasunovací
tubusem	tubus	k1gInSc7	tubus
(	(	kIx(	(
<g/>
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
pak	pak	k8xC	pak
metaforicky	metaforicky	k6eAd1	metaforicky
říká	říkat	k5eAaImIp3nS	říkat
také	také	k9	také
"	"	kIx"	"
<g/>
teleskop	teleskop	k1gInSc1	teleskop
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Definitivním	definitivní	k2eAgNnSc7d1	definitivní
řešením	řešení	k1gNnSc7	řešení
je	být	k5eAaImIp3nS	být
triedr	triedr	k1gInSc1	triedr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
okulár	okulár	k1gInSc4	okulár
a	a	k8xC	a
objektiv	objektiv	k1gInSc4	objektiv
vkládá	vkládat	k5eAaImIp3nS	vkládat
dvojice	dvojice	k1gFnSc1	dvojice
hranolů	hranol	k1gInPc2	hranol
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dráha	dráha	k1gFnSc1	dráha
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
zalomí	zalomit	k5eAaPmIp3nS	zalomit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
vojenského	vojenský	k2eAgNnSc2d1	vojenské
i	i	k8xC	i
geodetického	geodetický	k2eAgNnSc2d1	geodetické
zaměřování	zaměřování	k1gNnSc2	zaměřování
se	se	k3xPyFc4	se
dalekohledy	dalekohled	k1gInPc1	dalekohled
opatřují	opatřovat	k5eAaImIp3nP	opatřovat
nitkovými	nitkový	k2eAgInPc7d1	nitkový
kříži	kříž	k1gInPc7	kříž
pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
zacílení	zacílení	k1gNnSc4	zacílení
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
větší	veliký	k2eAgMnSc1d2	veliký
je	být	k5eAaImIp3nS	být
zvětšení	zvětšení	k1gNnSc1	zvětšení
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgInPc1d2	veliký
nároky	nárok	k1gInPc1	nárok
se	se	k3xPyFc4	se
kladou	klást	k5eAaImIp3nP	klást
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
uložení	uložení	k1gNnSc4	uložení
a	a	k8xC	a
upevnění	upevnění	k1gNnSc4	upevnění
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
10	[number]	k4	10
<g/>
násobného	násobný	k2eAgNnSc2d1	násobné
zvětšení	zvětšení	k1gNnSc2	zvětšení
lze	lze	k6eAd1	lze
dalekohled	dalekohled	k1gInSc4	dalekohled
držet	držet	k5eAaImF	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
větší	veliký	k2eAgNnSc4d2	veliký
zvětšení	zvětšení	k1gNnSc4	zvětšení
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
stativ	stativ	k1gInSc4	stativ
a	a	k8xC	a
pro	pro	k7c4	pro
astronomické	astronomický	k2eAgInPc4d1	astronomický
dalekohledy	dalekohled	k1gInPc4	dalekohled
pevný	pevný	k2eAgInSc4d1	pevný
sloup	sloup	k1gInSc4	sloup
<g/>
,	,	kIx,	,
zakotvený	zakotvený	k2eAgInSc4d1	zakotvený
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
dalekohledy	dalekohled	k1gInPc1	dalekohled
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
a	a	k8xC	a
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
kopulí	kopule	k1gFnPc2	kopule
<g/>
.	.	kIx.	.
</s>
<s>
Objektiv	objektiv	k1gInSc1	objektiv
refraktoru	refraktor	k1gInSc2	refraktor
je	být	k5eAaImIp3nS	být
čočka	čočka	k1gFnSc1	čočka
nebo	nebo	k8xC	nebo
soustava	soustava	k1gFnSc1	soustava
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
korigovat	korigovat	k5eAaBmF	korigovat
barevnou	barevný	k2eAgFnSc4d1	barevná
vadu	vada	k1gFnSc4	vada
(	(	kIx(	(
<g/>
achromát	achromát	k1gInSc1	achromát
<g/>
,	,	kIx,	,
apochromát	apochromát	k1gInSc1	apochromát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Optická	optický	k2eAgFnSc1d1	optická
"	"	kIx"	"
<g/>
velikost	velikost	k1gFnSc1	velikost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
apertura	apertura	k1gFnSc1	apertura
<g/>
)	)	kIx)	)
objektivu	objektiv	k1gInSc2	objektiv
určuje	určovat	k5eAaImIp3nS	určovat
světelnost	světelnost	k1gFnSc1	světelnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
maximální	maximální	k2eAgFnSc1d1	maximální
možné	možný	k2eAgNnSc4d1	možné
zvětšení	zvětšení	k1gNnSc4	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úhlové	úhlový	k2eAgNnSc4d1	úhlové
zvětšení	zvětšení	k1gNnSc4	zvětšení
refraktoru	refraktor	k1gInSc2	refraktor
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Příkladem	příklad	k1gInSc7	příklad
konstrukce	konstrukce	k1gFnSc2	konstrukce
refraktoru	refraktor	k1gInSc2	refraktor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tzv.	tzv.	kA	tzv.
hvězdářský	hvězdářský	k2eAgMnSc1d1	hvězdářský
(	(	kIx(	(
<g/>
Keplerův	Keplerův	k2eAgInSc1d1	Keplerův
<g/>
)	)	kIx)	)
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dalekohled	dalekohled	k1gInSc1	dalekohled
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
soustavami	soustava	k1gFnPc7	soustava
spojných	spojný	k2eAgFnPc2d1	spojná
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
společnou	společný	k2eAgFnSc4d1	společná
optickou	optický	k2eAgFnSc4d1	optická
osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
Objektiv	objektiv	k1gInSc1	objektiv
tohoto	tento	k3xDgInSc2	tento
dalekohledu	dalekohled	k1gInSc2	dalekohled
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
,	,	kIx,	,
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
okuláru	okulár	k1gInSc2	okulár
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Obrazové	obrazový	k2eAgNnSc1d1	obrazové
ohnisko	ohnisko	k1gNnSc1	ohnisko
objektivu	objektiv	k1gInSc2	objektiv
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
předmětovým	předmětový	k2eAgNnSc7d1	předmětové
ohniskem	ohnisko	k1gNnSc7	ohnisko
okuláru	okulár	k1gInSc2	okulár
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc4	obraz
velmi	velmi	k6eAd1	velmi
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
předmětu	předmět	k1gInSc2	předmět
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
objektivem	objektiv	k1gInSc7	objektiv
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ohnisku	ohnisko	k1gNnSc6	ohnisko
okuláru	okulár	k1gInSc2	okulár
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obraz	obraz	k1gInSc4	obraz
skutečný	skutečný	k2eAgInSc4d1	skutečný
<g/>
,	,	kIx,	,
zmenšený	zmenšený	k2eAgInSc4d1	zmenšený
a	a	k8xC	a
převrácený	převrácený	k2eAgInSc4d1	převrácený
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
obraz	obraz	k1gInSc4	obraz
pak	pak	k6eAd1	pak
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
okulárem	okulár	k1gInSc7	okulár
jako	jako	k8xS	jako
lupou	lupa	k1gFnSc7	lupa
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
převrácený	převrácený	k2eAgInSc1d1	převrácený
i	i	k9	i
po	po	k7c4	po
zvětšení	zvětšení	k1gNnPc4	zvětšení
okulárem	okulár	k1gInSc7	okulár
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
pro	pro	k7c4	pro
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pozorování	pozorování	k1gNnPc4	pozorování
nepodstatná	podstatný	k2eNgFnSc1d1	nepodstatná
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
jiný	jiný	k2eAgInSc1d1	jiný
princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Galileiho	Galilei	k1gMnSc2	Galilei
(	(	kIx(	(
<g/>
holandském	holandský	k2eAgInSc6d1	holandský
<g/>
)	)	kIx)	)
dalekohledu	dalekohled	k1gInSc6	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dalekohled	dalekohled	k1gInSc1	dalekohled
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
spojným	spojný	k2eAgInSc7d1	spojný
objektivem	objektiv	k1gInSc7	objektiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
a	a	k8xC	a
rozptylným	rozptylný	k2eAgInSc7d1	rozptylný
okulárem	okulár	k1gInSc7	okulár
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Obrazové	obrazový	k2eAgNnSc4d1	obrazové
ohnisko	ohnisko	k1gNnSc4	ohnisko
objektivu	objektiv	k1gInSc2	objektiv
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
dalekohledu	dalekohled	k1gInSc2	dalekohled
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
obrazovým	obrazový	k2eAgNnSc7d1	obrazové
ohniskem	ohnisko	k1gNnSc7	ohnisko
okuláru	okulár	k1gInSc2	okulár
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
dalekohledu	dalekohled	k1gInSc2	dalekohled
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
např.	např.	kA	např.
jako	jako	k8xC	jako
divadelní	divadelní	k2eAgNnSc1d1	divadelní
kukátko	kukátko	k1gNnSc1	kukátko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
zhruba	zhruba	k6eAd1	zhruba
čtyřnásobné	čtyřnásobný	k2eAgNnSc1d1	čtyřnásobné
zvětšení	zvětšení	k1gNnSc1	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Objektivem	objektiv	k1gInSc7	objektiv
reflektoru	reflektor	k1gInSc2	reflektor
je	být	k5eAaImIp3nS	být
primární	primární	k2eAgNnSc1d1	primární
duté	dutý	k2eAgNnSc1d1	duté
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
kulové	kulový	k2eAgNnSc1d1	kulové
<g/>
,	,	kIx,	,
parabolické	parabolický	k2eAgNnSc1d1	parabolické
případně	případně	k6eAd1	případně
i	i	k9	i
hyperbolické	hyperbolický	k2eAgFnSc2d1	hyperbolická
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
plocha	plocha	k1gFnSc1	plocha
určuje	určovat	k5eAaImIp3nS	určovat
světelnost	světelnost	k1gFnSc4	světelnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
předmětu	předmět	k1gInSc2	předmět
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
ještě	ještě	k9	ještě
tzv.	tzv.	kA	tzv.
sekundárním	sekundární	k2eAgNnSc7d1	sekundární
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
a	a	k8xC	a
pak	pak	k6eAd1	pak
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
okulárem	okulár	k1gInSc7	okulár
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
výhody	výhoda	k1gFnPc1	výhoda
reflektorů	reflektor	k1gInPc2	reflektor
jsou	být	k5eAaImIp3nP	být
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
barevné	barevný	k2eAgFnSc2d1	barevná
vady	vada	k1gFnSc2	vada
<g/>
,	,	kIx,	,
snazší	snadný	k2eAgFnSc1d2	snazší
výroba	výroba	k1gFnSc1	výroba
velkých	velký	k2eAgNnPc2d1	velké
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
uspořádání	uspořádání	k1gNnSc1	uspořádání
tubusu	tubus	k1gInSc2	tubus
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
totiž	totiž	k9	totiž
odráží	odrážet	k5eAaImIp3nP	odrážet
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tubus	tubus	k1gInSc1	tubus
má	mít	k5eAaImIp3nS	mít
teoreticky	teoreticky	k6eAd1	teoreticky
jen	jen	k9	jen
poloviční	poloviční	k2eAgFnSc4d1	poloviční
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
těžké	těžký	k2eAgNnSc1d1	těžké
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
konci	konec	k1gInSc6	konec
tubusu	tubus	k1gInSc2	tubus
jako	jako	k8xS	jako
objektiv	objektiv	k1gInSc4	objektiv
refraktoru	refraktor	k1gInSc2	refraktor
<g/>
.	.	kIx.	.
</s>
<s>
Správně	správně	k6eAd1	správně
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
primární	primární	k2eAgNnSc4d1	primární
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
parabolický	parabolický	k2eAgInSc1d1	parabolický
povrch	povrch	k1gInSc1	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
malé	malý	k2eAgFnSc6d1	malá
ploše	plocha	k1gFnSc6	plocha
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
ohniskové	ohniskový	k2eAgFnSc3d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
je	být	k5eAaImIp3nS	být
kulová	kulový	k2eAgFnSc1d1	kulová
plocha	plocha	k1gFnSc1	plocha
dostatečnou	dostatečná	k1gFnSc7	dostatečná
aproximací	aproximace	k1gFnSc7	aproximace
pokud	pokud	k8xS	pokud
nelpíme	lpět	k5eNaImIp1nP	lpět
na	na	k7c6	na
špičkové	špičkový	k2eAgFnSc6d1	špičková
kvalitě	kvalita	k1gFnSc6	kvalita
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Kulová	kulový	k2eAgFnSc1d1	kulová
plocha	plocha	k1gFnSc1	plocha
má	mít	k5eAaImIp3nS	mít
výhodu	výhoda	k1gFnSc4	výhoda
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nižších	nízký	k2eAgInPc2d2	nižší
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
i	i	k8xC	i
nižších	nízký	k2eAgInPc2d2	nižší
výrobních	výrobní	k2eAgInPc2d1	výrobní
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Cassegrainově	Cassegrainův	k2eAgInSc6d1	Cassegrainův
dalekohledu	dalekohled	k1gInSc6	dalekohled
se	se	k3xPyFc4	se
paprsky	paprsek	k1gInPc7	paprsek
odražené	odražený	k2eAgNnSc1d1	odražené
dutým	dutý	k2eAgNnSc7d1	duté
primárním	primární	k2eAgNnSc7d1	primární
parabolickým	parabolický	k2eAgNnSc7d1	parabolické
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
soustředí	soustředit	k5eAaPmIp3nS	soustředit
do	do	k7c2	do
malého	malý	k2eAgNnSc2d1	malé
vypuklého	vypuklý	k2eAgNnSc2d1	vypuklé
hyperbolického	hyperbolický	k2eAgNnSc2d1	hyperbolické
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	být	k5eAaImIp3nS	být
odrazí	odrazit	k5eAaPmIp3nS	odrazit
do	do	k7c2	do
okuláru	okulár	k1gInSc2	okulár
<g/>
,	,	kIx,	,
umístěného	umístěný	k2eAgInSc2d1	umístěný
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
;	;	kIx,	;
primární	primární	k2eAgNnSc1d1	primární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
musí	muset	k5eAaImIp3nS	muset
tedy	tedy	k9	tedy
mít	mít	k5eAaImF	mít
uprostřed	uprostřed	k6eAd1	uprostřed
otvor	otvor	k1gInSc4	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jej	on	k3xPp3gMnSc4	on
sochař	sochař	k1gMnSc1	sochař
Guillaume	Guillaum	k1gInSc5	Guillaum
Cassegrain	Cassegrain	k1gMnSc1	Cassegrain
(	(	kIx(	(
<g/>
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
konstrukce	konstrukce	k1gFnSc2	konstrukce
vychází	vycházet	k5eAaImIp3nS	vycházet
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
modifikací	modifikace	k1gFnPc2	modifikace
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukčně	konstrukčně	k6eAd1	konstrukčně
podobný	podobný	k2eAgInSc1d1	podobný
Cassegrainu	Cassegraina	k1gFnSc4	Cassegraina
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
typ	typ	k1gInSc1	typ
Ritchey-Chrétien	Ritchey-Chrétina	k1gFnPc2	Ritchey-Chrétina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
jako	jako	k9	jako
primární	primární	k2eAgFnSc4d1	primární
plochu	plocha	k1gFnSc4	plocha
používá	používat	k5eAaImIp3nS	používat
plošší	plochý	k2eAgNnSc1d2	plošší
hyperbolické	hyperbolický	k2eAgNnSc1d1	hyperbolické
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
a	a	k8xC	a
jako	jako	k9	jako
sekundární	sekundární	k2eAgNnSc4d1	sekundární
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
hyperbolické	hyperbolický	k2eAgNnSc4d1	hyperbolické
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ohybem	ohyb	k1gInSc7	ohyb
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
ohnisku	ohnisko	k1gNnSc6	ohnisko
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
korekční	korekční	k2eAgInSc1d1	korekční
člen	člen	k1gInSc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
typ	typ	k1gInSc1	typ
dalekohledu	dalekohled	k1gInSc2	dalekohled
však	však	k9	však
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
vadu	vada	k1gFnSc4	vada
parabolických	parabolický	k2eAgInPc2d1	parabolický
reflektorů	reflektor	k1gInPc2	reflektor
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
koma	koma	k1gFnSc1	koma
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
Ritchey-Chrétien	Ritchey-Chrétina	k1gFnPc2	Ritchey-Chrétina
využívá	využívat	k5eAaPmIp3nS	využívat
většina	většina	k1gFnSc1	většina
velikých	veliký	k2eAgInPc2d1	veliký
dalekohledů	dalekohled	k1gInPc2	dalekohled
současnosti	současnost	k1gFnSc2	současnost
včetně	včetně	k7c2	včetně
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Newtonově	Newtonův	k2eAgInSc6d1	Newtonův
dalekohledu	dalekohled	k1gInSc6	dalekohled
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
Cassgrainově	Cassgrainův	k2eAgFnSc3d1	Cassgrainův
konstrukci	konstrukce	k1gFnSc3	konstrukce
používá	používat	k5eAaImIp3nS	používat
rovinné	rovinný	k2eAgNnSc1d1	rovinné
sekundární	sekundární	k2eAgNnSc1d1	sekundární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odráží	odrážet	k5eAaImIp3nP	odrážet
paprsky	paprsek	k1gInPc1	paprsek
do	do	k7c2	do
okuláru	okulár	k1gInSc2	okulár
na	na	k7c6	na
boku	bok	k1gInSc6	bok
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
tubusem	tubus	k1gInSc7	tubus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
primární	primární	k2eAgNnSc1d1	primární
a	a	k8xC	a
sekundární	sekundární	k2eAgNnSc1d1	sekundární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgNnSc1d1	primární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
má	mít	k5eAaImIp3nS	mít
parabolický	parabolický	k2eAgInSc4d1	parabolický
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
tubusu	tubus	k1gInSc2	tubus
<g/>
.	.	kIx.	.
</s>
<s>
Přijímá	přijímat	k5eAaImIp3nS	přijímat
přicházející	přicházející	k2eAgNnSc1d1	přicházející
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
ohniska	ohnisko	k1gNnSc2	ohnisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
malé	malý	k2eAgNnSc1d1	malé
sekundární	sekundární	k2eAgNnSc1d1	sekundární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odráží	odrážet	k5eAaImIp3nP	odrážet
paprsky	paprsek	k1gInPc1	paprsek
mimo	mimo	k7c4	mimo
tubus	tubus	k1gInSc4	tubus
do	do	k7c2	do
okuláru	okulár	k1gInSc2	okulár
<g/>
.	.	kIx.	.
</s>
<s>
Optická	optický	k2eAgFnSc1d1	optická
soustava	soustava	k1gFnSc1	soustava
dvou	dva	k4xCgNnPc2	dva
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
okulárů	okulár	k1gInPc2	okulár
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
převrácen	převrátit	k5eAaPmNgInS	převrátit
stranově	stranově	k6eAd1	stranově
a	a	k8xC	a
pólově	pólově	k6eAd1	pólově
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obrazová	obrazový	k2eAgFnSc1d1	obrazová
převrácenost	převrácenost	k1gFnSc1	převrácenost
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozemské	pozemský	k2eAgNnSc4d1	pozemské
použití	použití	k1gNnSc4	použití
lze	lze	k6eAd1	lze
okulár	okulár	k1gInSc1	okulár
doplnit	doplnit	k5eAaPmF	doplnit
hranoly	hranol	k1gInPc4	hranol
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
upraví	upravit	k5eAaPmIp3nP	upravit
obraz	obraz	k1gInSc4	obraz
do	do	k7c2	do
správné	správný	k2eAgFnSc2d1	správná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Schmidt-Cassegrainův	Schmidt-Cassegrainův	k2eAgInSc1d1	Schmidt-Cassegrainův
dalekohled	dalekohled	k1gInSc1	dalekohled
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
předřazenou	předřazený	k2eAgFnSc4d1	předřazená
korekční	korekční	k2eAgFnSc4d1	korekční
desku	deska	k1gFnSc4	deska
(	(	kIx(	(
<g/>
meniskus	meniskus	k1gInSc4	meniskus
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
složitého	složitý	k2eAgInSc2d1	složitý
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
toroidní	toroidní	k2eAgFnSc1d1	toroidní
rozptylka	rozptylka	k1gFnSc1	rozptylka
<g/>
,	,	kIx,	,
kruhová	kruhový	k2eAgFnSc1d1	kruhová
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
rovinná	rovinný	k2eAgFnSc1d1	rovinná
pro	pro	k7c4	pro
umístění	umístění	k1gNnSc4	umístění
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
koriguje	korigovat	k5eAaBmIp3nS	korigovat
různé	různý	k2eAgFnPc4d1	různá
vady	vada	k1gFnPc4	vada
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
opticky	opticky	k6eAd1	opticky
umístěna	umístěn	k2eAgFnSc1d1	umístěna
před	před	k7c7	před
primárním	primární	k2eAgNnSc7d1	primární
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
–	–	k?	–
paprsky	paprsek	k1gInPc7	paprsek
tedy	tedy	k8xC	tedy
nejdříve	dříve	k6eAd3	dříve
procházejí	procházet	k5eAaImIp3nP	procházet
jí	on	k3xPp3gFnSc3	on
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
dopadají	dopadat	k5eAaImIp3nP	dopadat
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
složitému	složitý	k2eAgInSc3d1	složitý
tvaru	tvar	k1gInSc3	tvar
je	být	k5eAaImIp3nS	být
meniskus	meniskus	k1gInSc1	meniskus
tenčí	tenký	k2eAgInSc1d2	tenčí
než	než	k8xS	než
u	u	k7c2	u
systému	systém	k1gInSc2	systém
Maksutov-Cassegrain	Maksutov-Cassegraina	k1gFnPc2	Maksutov-Cassegraina
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Maksutov-Cassegrain	Maksutov-Cassegrain	k1gInSc1	Maksutov-Cassegrain
–	–	k?	–
Je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
následníkem	následník	k1gMnSc7	následník
Schmidt-Cassegrainova	Schmidt-Cassegrainův	k2eAgInSc2d1	Schmidt-Cassegrainův
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
jsou	být	k5eAaImIp3nP	být
optické	optický	k2eAgFnPc1d1	optická
plochy	plocha	k1gFnPc1	plocha
korekční	korekční	k2eAgFnPc4d1	korekční
desky	deska	k1gFnPc4	deska
(	(	kIx(	(
<g/>
menisku	meniskus	k1gInSc2	meniskus
před	před	k7c7	před
primárním	primární	k2eAgNnSc7d1	primární
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
)	)	kIx)	)
konfigurované	konfigurovaný	k2eAgFnPc1d1	konfigurovaná
do	do	k7c2	do
kulového	kulový	k2eAgInSc2d1	kulový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
<g/>
.	.	kIx.	.
<g/>
Primární	primární	k2eAgNnSc1d1	primární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
je	být	k5eAaImIp3nS	být
také	také	k9	také
kulové	kulový	k2eAgNnSc1d1	kulové
<g/>
.	.	kIx.	.
<g/>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
levná	levný	k2eAgFnSc1d1	levná
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Nežádoucím	žádoucí	k2eNgInSc7d1	nežádoucí
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
korekční	korekční	k2eAgInSc1d1	korekční
člen	člen	k1gInSc1	člen
masivní	masivní	k2eAgInSc1d1	masivní
<g/>
.	.	kIx.	.
<g/>
Maksutov-Cassegrain	Maksutov-Cassegrain	k1gInSc1	Maksutov-Cassegrain
je	být	k5eAaImIp3nS	být
použitelný	použitelný	k2eAgInSc1d1	použitelný
pro	pro	k7c4	pro
fotografii	fotografia	k1gFnSc4	fotografia
velkých	velký	k2eAgFnPc2d1	velká
částí	část	k1gFnPc2	část
oblohy	obloha	k1gFnSc2	obloha
a	a	k8xC	a
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
nenáročnost	nenáročnost	k1gFnSc4	nenáročnost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
i	i	k9	i
mezi	mezi	k7c4	mezi
astronomy	astronom	k1gMnPc4	astronom
amatéry	amatér	k1gMnPc4	amatér
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
dobře	dobře	k6eAd1	dobře
použitelný	použitelný	k2eAgInSc1d1	použitelný
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgNnSc4d1	pozemní
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
.	.	kIx.	.
<g/>
Systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
omezený	omezený	k2eAgInSc1d1	omezený
právě	právě	k9	právě
masivností	masivnost	k1gFnSc7	masivnost
menisku	meniskus	k1gInSc2	meniskus
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývají	bývat	k5eAaImIp3nP	bývat
dalekohledy	dalekohled	k1gInPc1	dalekohled
relativně	relativně	k6eAd1	relativně
menších	malý	k2eAgInPc2d2	menší
průměrů	průměr	k1gInPc2	průměr
a	a	k8xC	a
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
menší	malý	k2eAgFnSc4d2	menší
světelnost	světelnost	k1gFnSc4	světelnost
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
Schmidt-Newton	Schmidt-Newton	k1gInSc1	Schmidt-Newton
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
předřazenou	předřazený	k2eAgFnSc4d1	předřazená
korekční	korekční	k2eAgFnSc4d1	korekční
desku	deska	k1gFnSc4	deska
(	(	kIx(	(
<g/>
meniskus	meniskus	k1gInSc4	meniskus
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
složitého	složitý	k2eAgInSc2d1	složitý
tvaru	tvar	k1gInSc2	tvar
stejnou	stejný	k2eAgFnSc4d1	stejná
jako	jako	k8xS	jako
Schmidt-Cassegrain	Schmidt-Cassegrain	k1gInSc4	Schmidt-Cassegrain
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
–	–	k?	–
omezuje	omezovat	k5eAaImIp3nS	omezovat
sklenutí	sklenutí	k1gNnSc4	sklenutí
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
komu	kdo	k3yQnSc3	kdo
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgNnSc1d1	sekundární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
konstrukčním	konstrukční	k2eAgInSc6d1	konstrukční
celku	celek	k1gInSc6	celek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
meniskem	meniskus	k1gInSc7	meniskus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odklání	odklánět	k5eAaImIp3nS	odklánět
paprsek	paprsek	k1gInSc1	paprsek
ven	ven	k6eAd1	ven
z	z	k7c2	z
tubusu	tubus	k1gInSc2	tubus
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c4	na
předmětnou	předmětný	k2eAgFnSc4d1	předmětná
osu	osa	k1gFnSc4	osa
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
klasický	klasický	k2eAgInSc4d1	klasický
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
<g/>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
ale	ale	k8xC	ale
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nemá	mít	k5eNaImIp3nS	mít
otvor	otvor	k1gInSc4	otvor
v	v	k7c6	v
primárním	primární	k2eAgNnSc6d1	primární
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
jeho	jeho	k3xOp3gNnSc3	jeho
provedení	provedení	k1gNnSc3	provedení
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
při	při	k7c6	při
srovnatelné	srovnatelný	k2eAgFnSc6d1	srovnatelná
optické	optický	k2eAgFnSc6d1	optická
délce	délka	k1gFnSc6	délka
(	(	kIx(	(
<g/>
ohnisku	ohnisko	k1gNnSc6	ohnisko
<g/>
)	)	kIx)	)
hlavní	hlavní	k2eAgInSc1d1	hlavní
tubus	tubus	k1gInSc1	tubus
téměř	téměř	k6eAd1	téměř
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Klevcovův	Klevcovův	k2eAgInSc1d1	Klevcovův
dalekohled	dalekohled	k1gInSc1	dalekohled
má	mít	k5eAaImIp3nS	mít
korekční	korekční	k2eAgInSc1d1	korekční
člen	člen	k1gInSc1	člen
umístěn	umístit	k5eAaPmNgInS	umístit
před	před	k7c7	před
sekundárním	sekundární	k2eAgNnSc7d1	sekundární
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgNnSc1d1	sekundární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
korekčním	korekční	k2eAgInSc7d1	korekční
meniskem	meniskus	k1gInSc7	meniskus
konstrukčně	konstrukčně	k6eAd1	konstrukčně
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Meniskus	meniskus	k1gInSc1	meniskus
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
mezikruží	mezikruží	k1gNnSc2	mezikruží
čočky	čočka	k1gFnSc2	čočka
se	s	k7c7	s
středovým	středový	k2eAgInSc7d1	středový
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
prochází	procházet	k5eAaImIp3nS	procházet
paprsek	paprsek	k1gInSc1	paprsek
od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
okuláru	okulár	k1gInSc3	okulár
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc7d1	aktivní
částí	část	k1gFnSc7	část
menisku	meniskus	k1gInSc2	meniskus
prochází	procházet	k5eAaImIp3nS	procházet
paprsek	paprsek	k1gInSc1	paprsek
před	před	k7c7	před
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
sekundární	sekundární	k2eAgNnSc4d1	sekundární
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Ritchey-Chretien	Ritchey-Chretina	k1gFnPc2	Ritchey-Chretina
používá	používat	k5eAaImIp3nS	používat
obě	dva	k4xCgNnPc4	dva
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
hyperbolického	hyperbolický	k2eAgInSc2d1	hyperbolický
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
koriguje	korigovat	k5eAaBmIp3nS	korigovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
vad	vada	k1gFnPc2	vada
a	a	k8xC	a
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
vložený	vložený	k2eAgInSc4d1	vložený
meniskus	meniskus	k1gInSc4	meniskus
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
však	však	k9	však
náročný	náročný	k2eAgInSc1d1	náročný
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
takto	takto	k6eAd1	takto
konfigurovaný	konfigurovaný	k2eAgMnSc1d1	konfigurovaný
je	být	k5eAaImIp3nS	být
HST	HST	kA	HST
–	–	k?	–
Hubbleův	Hubbleův	k2eAgInSc4d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
(	(	kIx(	(
<g/>
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
také	také	k9	také
projevil	projevit	k5eAaPmAgInS	projevit
problém	problém	k1gInSc1	problém
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
nepřesně	přesně	k6eNd1	přesně
vybroušeno	vybroušen	k2eAgNnSc1d1	vybroušeno
primární	primární	k2eAgNnSc1d1	primární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zejména	zejména	k9	zejména
zpočátku	zpočátku	k6eAd1	zpočátku
znemožňovalo	znemožňovat	k5eAaImAgNnS	znemožňovat
většinu	většina	k1gFnSc4	většina
měření	měření	k1gNnSc1	měření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chystané	chystaný	k2eAgInPc1d1	chystaný
největší	veliký	k2eAgInPc1d3	veliký
pozemní	pozemní	k2eAgInPc1d1	pozemní
optické	optický	k2eAgInPc1d1	optický
přístroje	přístroj	k1gInPc1	přístroj
budou	být	k5eAaImBp3nP	být
také	také	k9	také
používat	používat	k5eAaImF	používat
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Coudé	Coudý	k2eAgFnSc2d1	Coudý
není	být	k5eNaImIp3nS	být
přímo	přímo	k6eAd1	přímo
typem	typ	k1gInSc7	typ
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
systémem	systém	k1gInSc7	systém
nastavení	nastavení	k1gNnSc2	nastavení
dráhy	dráha	k1gFnSc2	dráha
paprsku	paprsek	k1gInSc2	paprsek
po	po	k7c6	po
průchodu	průchod	k1gInSc6	průchod
sekundárním	sekundární	k2eAgInSc6d1	sekundární
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
–	–	k?	–
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
coudé	coudý	k2eAgFnSc6d1	coudý
ohnisku	ohnisko	k1gNnSc6	ohnisko
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
dalekohledy	dalekohled	k1gInPc1	dalekohled
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
více	hodně	k6eAd2	hodně
ohnisek	ohnisko	k1gNnPc2	ohnisko
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc4	jaký
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
objekty	objekt	k1gInPc4	objekt
chceme	chtít	k5eAaImIp1nP	chtít
pozorovat	pozorovat	k5eAaImF	pozorovat
a	a	k8xC	a
co	co	k3yQnSc1	co
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
chceme	chtít	k5eAaImIp1nP	chtít
měřit	měřit	k5eAaImF	měřit
(	(	kIx(	(
<g/>
fotografovat	fotografovat	k5eAaImF	fotografovat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
získávat	získávat	k5eAaImF	získávat
spektrum	spektrum	k1gNnSc4	spektrum
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Coudé	Coudý	k2eAgFnSc2d1	Coudý
umístěním	umístění	k1gNnSc7	umístění
dalších	další	k2eAgNnPc2d1	další
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
svede	svést	k5eAaPmIp3nS	svést
paprsky	paprsek	k1gInPc4	paprsek
do	do	k7c2	do
pevně	pevně	k6eAd1	pevně
umístěného	umístěný	k2eAgNnSc2d1	umístěné
ohniska	ohnisko	k1gNnSc2	ohnisko
v	v	k7c6	v
polární	polární	k2eAgFnSc6d1	polární
ose	osa	k1gFnSc6	osa
montáže	montáž	k1gFnSc2	montáž
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc4	pět
vědeckých	vědecký	k2eAgInPc2d1	vědecký
dalekohledů	dalekohled	k1gInPc2	dalekohled
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
větším	veliký	k2eAgInSc7d2	veliký
než	než	k8xS	než
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Perkův	Perkův	k2eAgInSc1d1	Perkův
dalekohled	dalekohled	k1gInSc1	dalekohled
–	–	k?	–
průměr	průměr	k1gInSc1	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc1	metr
<g/>
,	,	kIx,	,
observatoř	observatoř	k1gFnSc1	observatoř
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgFnSc6d1	Ondřejova
Astronomického	astronomický	k2eAgInSc2d1	astronomický
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
Klenot	klenot	k1gInSc1	klenot
–	–	k?	–
průměr	průměr	k1gInSc1	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
106	[number]	k4	106
cm	cm	kA	cm
<g/>
,	,	kIx,	,
observatoř	observatoř	k1gFnSc1	observatoř
na	na	k7c6	na
Kleti	klet	k2eAgMnPc1d1	klet
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
2	[number]	k4	2
<g/>
.	.	kIx.	.
teleskop	teleskop	k1gInSc1	teleskop
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgInSc6d1	Ondřejův
–	–	k?	–
průměr	průměr	k1gInSc4	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
65	[number]	k4	65
cm	cm	kA	cm
Dalekohled	dalekohled	k1gInSc4	dalekohled
observatoře	observatoř	k1gFnSc2	observatoř
<g />
.	.	kIx.	.
</s>
<s>
MUNI	MUNI	k?	MUNI
–	–	k?	–
průměr	průměr	k1gInSc1	průměr
60	[number]	k4	60
cm	cm	kA	cm
<g/>
,	,	kIx,	,
observatoř	observatoř	k1gFnSc1	observatoř
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
2	[number]	k4	2
<g/>
.	.	kIx.	.
teleskop	teleskop	k1gInSc1	teleskop
na	na	k7c4	na
Kleti	klet	k2eAgMnPc1d1	klet
–	–	k?	–
průměr	průměr	k1gInSc4	průměr
57	[number]	k4	57
cm	cm	kA	cm
Tyto	tento	k3xDgInPc4	tento
největší	veliký	k2eAgInPc4d3	veliký
teleskopy	teleskop	k1gInPc4	teleskop
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
dalekohled	dalekohled	k1gInSc1	dalekohled
ve	v	k7c6	v
Rtyni	Rtyeň	k1gFnSc6	Rtyeň
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
<g/>
,	,	kIx,	,
asi	asi	k9	asi
82	[number]	k4	82
cm	cm	kA	cm
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
soukromý	soukromý	k2eAgMnSc1d1	soukromý
a	a	k8xC	a
neslouží	sloužit	k5eNaImIp3nS	sloužit
vědeckým	vědecký	k2eAgInPc3d1	vědecký
účelům	účel	k1gInPc3	účel
</s>
