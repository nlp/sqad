<p>
<s>
Páté	pátý	k4xOgFnPc1	pátý
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
na	na	k7c6	na
nezávislé	závislý	k2eNgFnSc6d1	nezávislá
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
volili	volit	k5eAaImAgMnP	volit
prezidenta	prezident	k1gMnSc4	prezident
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
pronásledovaly	pronásledovat	k5eAaImAgFnP	pronásledovat
obavy	obava	k1gFnPc4	obava
ze	z	k7c2	z
zfalšování	zfalšování	k1gNnSc2	zfalšování
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
prokázány	prokázat	k5eAaPmNgInP	prokázat
u	u	k7c2	u
minulých	minulý	k2eAgFnPc2d1	minulá
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgFnPc1	který
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
pozorování	pozorování	k1gNnSc2	pozorování
OBSE	OBSE	kA	OBSE
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
mezinárodních	mezinárodní	k2eAgMnPc2d1	mezinárodní
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
bylo	být	k5eAaImAgNnS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
však	však	k9	však
byly	být	k5eAaImAgFnP	být
kvalifikované	kvalifikovaný	k2eAgFnPc1d1	kvalifikovaná
a	a	k8xC	a
transparentní	transparentní	k2eAgFnPc1d1	transparentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těsným	těsný	k2eAgMnSc7d1	těsný
vítězem	vítěz	k1gMnSc7	vítěz
voleb	volba	k1gFnPc2	volba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
předák	předák	k1gInSc1	předák
Strany	strana	k1gFnSc2	strana
regionů	region	k1gInPc2	region
Viktor	Viktor	k1gMnSc1	Viktor
Janukovyč	Janukovyč	k1gMnSc1	Janukovyč
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnSc1	jeho
rivalka	rivalka	k1gFnSc1	rivalka
Julija	Julij	k2eAgFnSc1d1	Julija
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
výsledek	výsledek	k1gInSc4	výsledek
uznat	uznat	k5eAaPmF	uznat
a	a	k8xC	a
obrátila	obrátit	k5eAaPmAgFnS	obrátit
se	se	k3xPyFc4	se
i	i	k9	i
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
však	však	k9	však
Janukovyčovi	Janukovyč	k1gMnSc3	Janukovyč
blahopřála	blahopřát	k5eAaImAgFnS	blahopřát
nejedna	nejeden	k4xCyIgFnSc1	nejeden
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zpochybnění	zpochybnění	k1gNnSc6	zpochybnění
voleb	volba	k1gFnPc2	volba
Tymošenkovou	Tymošenkový	k2eAgFnSc4d1	Tymošenková
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
platnost	platnost	k1gFnSc4	platnost
výsledku	výsledek	k1gInSc2	výsledek
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
stížnost	stížnost	k1gFnSc1	stížnost
posoudila	posoudit	k5eAaPmAgFnS	posoudit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
však	však	k9	však
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
svou	svůj	k3xOyFgFnSc4	svůj
žalobu	žaloba	k1gFnSc4	žaloba
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
soud	soud	k1gInSc1	soud
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
využít	využít	k5eAaPmF	využít
písemné	písemný	k2eAgInPc4d1	písemný
důkazy	důkaz	k1gInPc4	důkaz
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
předvolat	předvolat	k5eAaPmF	předvolat
navržené	navržený	k2eAgMnPc4d1	navržený
svědky	svědek	k1gMnPc4	svědek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
inauguraci	inaugurace	k1gFnSc3	inaugurace
Viktora	Viktor	k1gMnSc2	Viktor
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
prezidenta	prezident	k1gMnSc2	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
prezident	prezident	k1gMnSc1	prezident
Viktor	Viktor	k1gMnSc1	Viktor
Juščenko	Juščenka	k1gFnSc5	Juščenka
svůj	svůj	k3xOyFgInSc4	svůj
post	post	k1gInSc4	post
neobhájí	obhájit	k5eNaPmIp3nS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgInS	vyjít
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vítězně	vítězně	k6eAd1	vítězně
Viktor	Viktor	k1gMnSc1	Viktor
Janukovyč	Janukovyč	k1gMnSc1	Janukovyč
s	s	k7c7	s
35	[number]	k4	35
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dle	dle	k7c2	dle
očekávání	očekávání	k1gNnSc2	očekávání
uspěl	uspět	k5eAaPmAgMnS	uspět
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Zakarpatí	Zakarpatí	k1gNnSc6	Zakarpatí
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
Julija	Julija	k1gFnSc1	Julija
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
získala	získat	k5eAaPmAgFnS	získat
25	[number]	k4	25
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
kyjevských	kyjevský	k2eAgInPc6d1	kyjevský
obvodech	obvod	k1gInPc6	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Juščenko	Juščenka	k1gFnSc5	Juščenka
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příklonu	příklon	k1gInSc6	příklon
k	k	k7c3	k
ukrajinskému	ukrajinský	k2eAgInSc3d1	ukrajinský
nacionalismu	nacionalismus	k1gInSc3	nacionalismus
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
několika	několik	k4yIc6	několik
obvodech	obvod	k1gInPc6	obvod
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
obdržel	obdržet	k5eAaPmAgInS	obdržet
5,45	[number]	k4	5,45
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Předstihli	předstihnout	k5eAaPmAgMnP	předstihnout
jej	on	k3xPp3gNnSc4	on
Arsenij	Arsenij	k1gMnPc1	Arsenij
Jaceňuk	Jaceňuk	k1gMnSc1	Jaceňuk
a	a	k8xC	a
zejména	zejména	k9	zejména
Serhij	Serhij	k1gFnSc1	Serhij
Tihipko	Tihipka	k1gFnSc5	Tihipka
(	(	kIx(	(
<g/>
13,05	[number]	k4	13,05
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bodoval	bodovat	k5eAaImAgMnS	bodovat
zejména	zejména	k9	zejména
ve	v	k7c6	v
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
Dněpropetrovska	Dněpropetrovsko	k1gNnSc2	Dněpropetrovsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volební	volební	k2eAgFnSc1d1	volební
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
67	[number]	k4	67
%	%	kIx~	%
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
byla	být	k5eAaImAgFnS	být
obecně	obecně	k6eAd1	obecně
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
mírně	mírně	k6eAd1	mírně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
volební	volební	k2eAgFnSc1d1	volební
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
69,15	[number]	k4	69,15
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Předběžné	předběžný	k2eAgInPc1d1	předběžný
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInSc4d1	počáteční
Janukovyčův	Janukovyčův	k2eAgInSc4d1	Janukovyčův
náskok	náskok	k1gInSc4	náskok
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
nejprve	nejprve	k6eAd1	nejprve
dotáhla	dotáhnout	k5eAaPmAgFnS	dotáhnout
na	na	k7c4	na
pouhá	pouhý	k2eAgNnPc4d1	pouhé
2	[number]	k4	2
%	%	kIx~	%
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
na	na	k7c4	na
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
;	;	kIx,	;
Janukovyč	Janukovyč	k1gFnSc1	Janukovyč
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
vítězem	vítěz	k1gMnSc7	vítěz
voleb	volba	k1gFnPc2	volba
se	s	k7c7	s
48,95	[number]	k4	48,95
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Janukovyč	Janukovyč	k1gInSc1	Janukovyč
měl	mít	k5eAaImAgInS	mít
dle	dle	k7c2	dle
očekávání	očekávání	k1gNnSc2	očekávání
nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
z	z	k7c2	z
Donbasu	Donbas	k1gInSc2	Donbas
(	(	kIx(	(
<g/>
Doněcká	doněcký	k2eAgFnSc1d1	Doněcká
oblast	oblast	k1gFnSc1	oblast
90,4	[number]	k4	90,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
uspěla	uspět	k5eAaPmAgFnS	uspět
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
Lvovská	lvovský	k2eAgFnSc1d1	Lvovská
oblast	oblast	k1gFnSc1	oblast
88,89	[number]	k4	88,89
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyrovnanější	vyrovnaný	k2eAgFnSc7d3	nejvyrovnanější
oblastí	oblast	k1gFnSc7	oblast
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
Zakarpatí	Zakarpatí	k1gNnSc1	Zakarpatí
(	(	kIx(	(
<g/>
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
51	[number]	k4	51
%	%	kIx~	%
<g/>
,	,	kIx,	,
Janukovyč	Janukovyč	k1gFnSc1	Janukovyč
41,5	[number]	k4	41,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
2010	[number]	k4	2010
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc4	stránka
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
volební	volební	k2eAgFnSc2d1	volební
komise	komise	k1gFnSc2	komise
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
konštanta	konštant	k1gMnSc2	konštant
Komentář	komentář	k1gInSc1	komentář
slovenského	slovenský	k2eAgMnSc2d1	slovenský
mezinárodního	mezinárodní	k2eAgMnSc2d1	mezinárodní
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
</p>
