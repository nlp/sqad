<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Fritz	Fritz	k1gMnSc1	Fritz
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1865	[number]	k4	1865
Prešpurk	Prešpurk	k?	Prešpurk
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
nebo	nebo	k8xC	nebo
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1915	[number]	k4	1915
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Českého	český	k2eAgInSc2d1	český
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
Říšského	říšský	k2eAgInSc2d1	říšský
svazu	svaz	k1gInSc2	svaz
hostinských	hostinský	k1gMnPc2	hostinský
a	a	k8xC	a
výčepních	výčepní	k1gMnPc2	výčepní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
v	v	k7c6	v
čp.	čp.	k?	čp.
450	[number]	k4	450
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
měšťan	měšťan	k1gMnSc1	měšťan
a	a	k8xC	a
hostinský	hostinský	k1gMnSc1	hostinský
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
v	v	k7c6	v
organizacích	organizace	k1gFnPc6	organizace
sdružujících	sdružující	k2eAgFnPc6d1	sdružující
hostinské	hostinský	k1gMnPc4	hostinský
a	a	k8xC	a
kavárníky	kavárník	k1gMnPc4	kavárník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
komunální	komunální	k2eAgFnSc6d1	komunální
<g/>
,	,	kIx,	,
zemské	zemský	k2eAgFnSc6d1	zemská
i	i	k8xC	i
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
kavárníkem	kavárník	k1gMnSc7	kavárník
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
Říšského	říšský	k2eAgInSc2d1	říšský
svazu	svaz	k1gInSc2	svaz
hostinských	hostinský	k2eAgFnPc2d1	hostinská
a	a	k8xC	a
výčepních	výčepní	k2eAgFnPc2d1	výčepní
(	(	kIx(	(
<g/>
Reichsverband	Reichsverbanda	k1gFnPc2	Reichsverbanda
der	drát	k5eAaImRp2nS	drát
Gast-	Gast-	k1gFnSc2	Gast-
und	und	k?	und
Schankwirte	Schankwirt	k1gMnSc5	Schankwirt
<g/>
,	,	kIx,	,
též	též	k9	též
Reichsverband	Reichsverband	k1gInSc1	Reichsverband
der	drát	k5eAaImRp2nS	drát
Gastwirte	Gastwirt	k1gMnSc5	Gastwirt
<g/>
)	)	kIx)	)
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
Západočeského	západočeský	k2eAgInSc2d1	západočeský
svazu	svaz	k1gInSc2	svaz
kavárníků	kavárník	k1gMnPc2	kavárník
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
list	list	k1gInSc1	list
Gastwirtezeitung	Gastwirtezeitung	k1gInSc1	Gastwirtezeitung
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vyjednáváních	vyjednávání	k1gNnPc6	vyjednávání
okolo	okolo	k7c2	okolo
zavedení	zavedení	k1gNnSc2	zavedení
zemské	zemský	k2eAgFnSc2d1	zemská
pivní	pivní	k2eAgFnSc2d1	pivní
přirážky	přirážka	k1gFnSc2	přirážka
<g/>
.	.	kIx.	.
<g/>
Zapojil	zapojit	k5eAaPmAgMnS	zapojit
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
Český	český	k2eAgInSc4d1	český
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
kurii	kurie	k1gFnSc4	kurie
venkovských	venkovský	k2eAgFnPc2d1	venkovská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Kraslice	kraslice	k1gFnSc2	kraslice
<g/>
.	.	kIx.	.
</s>
<s>
Uváděl	uvádět	k5eAaImAgMnS	uvádět
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
jako	jako	k9	jako
kandidát	kandidát	k1gMnSc1	kandidát
Německé	německý	k2eAgFnSc2d1	německá
radikální	radikální	k2eAgFnSc2d1	radikální
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
stoupenec	stoupenec	k1gMnSc1	stoupenec
Karla	Karel	k1gMnSc2	Karel
Hermanna	Hermann	k1gMnSc2	Hermann
Wolfa	Wolf	k1gMnSc2	Wolf
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1915	[number]	k4	1915
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
49	[number]	k4	49
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
