<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
článek	článek	k1gInSc1	článek
popisuje	popisovat	k5eAaImIp3nS	popisovat
některé	některý	k3yIgMnPc4	některý
smyšlené	smyšlený	k2eAgMnPc4d1	smyšlený
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Domácí	domácí	k2eAgMnPc1d1	domácí
skřítci	skřítek	k1gMnPc1	skřítek
==	==	k?	==
</s>
</p>
<p>
<s>
Domácí	domácí	k2eAgMnPc1d1	domácí
skřítci	skřítek	k1gMnPc1	skřítek
slouží	sloužit	k5eAaImIp3nP	sloužit
kouzelníkům	kouzelník	k1gMnPc3	kouzelník
<g/>
,	,	kIx,	,
nedostávají	dostávat	k5eNaImIp3nP	dostávat
za	za	k7c4	za
to	ten	k3xDgNnSc1	ten
žádnou	žádný	k3yNgFnSc4	žádný
mzdu	mzda	k1gFnSc4	mzda
(	(	kIx(	(
<g/>
pracují	pracovat	k5eAaImIp3nP	pracovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
otroci	otrok	k1gMnPc1	otrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
pánů	pan	k1gMnPc2	pan
dá	dát	k5eAaPmIp3nS	dát
skřítkovi	skřítek	k1gMnSc3	skřítek
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
skřítků	skřítek	k1gMnPc2	skřítek
slouží	sloužit	k5eAaImIp3nS	sloužit
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
spousty	spousta	k1gFnSc2	spousta
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velicí	veliký	k2eAgMnPc1d1	veliký
60	[number]	k4	60
-	-	kIx~	-
90	[number]	k4	90
cm	cm	kA	cm
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
hubené	hubený	k2eAgFnPc4d1	hubená
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
veliké	veliký	k2eAgNnSc4d1	veliké
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dobby	Dobb	k1gInPc4	Dobb
===	===	k?	===
</s>
</p>
<p>
<s>
Dobby	Dobba	k1gFnPc4	Dobba
původně	původně	k6eAd1	původně
sloužil	sloužit	k5eAaImAgMnS	sloužit
rodině	rodina	k1gFnSc3	rodina
Malfoyových	Malfoyových	k2eAgMnPc1d1	Malfoyových
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
Harryho	Harry	k1gMnSc4	Harry
varovat	varovat	k5eAaImF	varovat
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
ve	v	k7c6	v
Škole	škola	k1gFnSc6	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
otevřená	otevřený	k2eAgFnSc1d1	otevřená
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
chtěl	chtít	k5eAaImAgInS	chtít
i	i	k9	i
přes	přes	k7c4	přes
varování	varování	k1gNnPc4	varování
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
jet	jet	k5eAaImF	jet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
Dobby	Dobba	k1gFnSc2	Dobba
svrhnul	svrhnout	k5eAaPmAgInS	svrhnout
dort	dort	k1gInSc1	dort
na	na	k7c4	na
hosty	host	k1gMnPc4	host
jeho	jeho	k3xOp3gFnSc2	jeho
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
strýce	strýc	k1gMnSc4	strýc
<g/>
.	.	kIx.	.
</s>
<s>
Vznášecí	Vznášecí	k2eAgNnSc4d1	Vznášecí
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
Dobby	Dobb	k1gMnPc4	Dobb
provedl	provést	k5eAaPmAgMnS	provést
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
svedlo	svést	k5eAaPmAgNnS	svést
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
málem	málem	k6eAd1	málem
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
Harrymu	Harrym	k1gInSc2	Harrym
jet	jet	k5eAaImF	jet
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zablokoval	zablokovat	k5eAaPmAgInS	zablokovat
Dobby	Dobb	k1gInPc7	Dobb
přepážku	přepážka	k1gFnSc4	přepážka
na	na	k7c6	na
nástupišti	nástupiště	k1gNnSc6	nástupiště
93	[number]	k4	93
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
pak	pak	k6eAd1	pak
skřítek	skřítek	k1gMnSc1	skřítek
očaruje	očarovat	k5eAaPmIp3nS	očarovat
potlouk	potlouct	k5eAaPmDgInS	potlouct
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
Harryho	Harry	k1gMnSc4	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
vloží	vložit	k5eAaPmIp3nS	vložit
Harry	Harra	k1gFnPc4	Harra
do	do	k7c2	do
zničeného	zničený	k2eAgInSc2d1	zničený
deníku	deník	k1gInSc2	deník
Toma	Tom	k1gMnSc4	Tom
Raddlea	Raddleus	k1gMnSc4	Raddleus
ponožku	ponožka	k1gFnSc4	ponožka
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
předá	předat	k5eAaPmIp3nS	předat
Luciovi	Lucius	k1gMnSc3	Lucius
Malfoyovi	Malfoya	k1gMnSc3	Malfoya
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
ho	on	k3xPp3gInSc4	on
hodí	hodit	k5eAaPmIp3nS	hodit
Dobbymu	Dobbym	k1gInSc2	Dobbym
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
stane	stanout	k5eAaPmIp3nS	stanout
svobodný	svobodný	k2eAgMnSc1d1	svobodný
skřítek	skřítek	k1gMnSc1	skřítek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
jej	on	k3xPp3gMnSc4	on
nadaboval	nadabovat	k5eAaPmAgMnS	nadabovat
Toby	Toba	k1gMnSc2	Toba
Jones	Jones	k1gMnSc1	Jones
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
Dobby	Dobba	k1gFnPc1	Dobba
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
opět	opět	k6eAd1	opět
potká	potkat	k5eAaPmIp3nS	potkat
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
totiž	totiž	k9	totiž
společně	společně	k6eAd1	společně
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
skřítkou	skřítka	k1gFnSc7	skřítka
Bartyho	Barty	k1gMnSc2	Barty
Skrka	Skrek	k1gMnSc2	Skrek
Winky	Winka	k1gMnSc2	Winka
v	v	k7c6	v
bradavické	bradavický	k2eAgFnSc6d1	bradavická
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Pomůže	pomoct	k5eAaPmIp3nS	pomoct
Harrymu	Harrym	k1gInSc2	Harrym
zvládnout	zvládnout	k5eAaPmF	zvládnout
druhý	druhý	k4xOgInSc4	druhý
úkol	úkol	k1gInSc4	úkol
turnaje	turnaj	k1gInSc2	turnaj
tří	tři	k4xCgFnPc2	tři
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
když	když	k8xS	když
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
získá	získat	k5eAaPmIp3nS	získat
žaberník	žaberník	k1gInSc1	žaberník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
ho	on	k3xPp3gNnSc4	on
bratr	bratr	k1gMnSc1	bratr
Albuse	Albuse	k1gFnSc2	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
Aberforth	Aberfortha	k1gFnPc2	Aberfortha
pošle	poslat	k5eAaPmIp3nS	poslat
za	za	k7c4	za
Harrym	Harrym	k1gInSc4	Harrym
<g/>
,	,	kIx,	,
Ronem	ron	k1gInSc7	ron
a	a	k8xC	a
Hermionou	Hermiona	k1gFnSc7	Hermiona
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Malfoyových	Malfoyová	k1gFnPc2	Malfoyová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
pomůže	pomoct	k5eAaPmIp3nS	pomoct
také	také	k9	také
panu	pan	k1gMnSc3	pan
Ollivanderovi	Ollivander	k1gMnSc3	Ollivander
<g/>
,	,	kIx,	,
Lence	Lenka	k1gFnSc3	Lenka
Láskorádové	Láskorádový	k2eAgFnSc2d1	Láskorádová
<g/>
,	,	kIx,	,
Deanu	Dean	k1gMnSc6	Dean
Thomasovi	Thomas	k1gMnSc6	Thomas
a	a	k8xC	a
skřetovi	skřet	k1gMnSc6	skřet
Griphookovi	Griphook	k1gMnSc6	Griphook
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
samotný	samotný	k2eAgMnSc1d1	samotný
Dobby	Dobb	k1gMnPc7	Dobb
zabit	zabít	k5eAaPmNgMnS	zabít
nožem	nůž	k1gInSc7	nůž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
hodila	hodit	k5eAaImAgFnS	hodit
Belatrix	Belatrix	k1gInSc4	Belatrix
Lestrangeová	Lestrangeový	k2eAgFnSc1d1	Lestrangeová
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
ho	on	k3xPp3gNnSc2	on
pak	pak	k6eAd1	pak
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
kouzel	kouzlo	k1gNnPc2	kouzlo
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
lásky	láska	k1gFnSc2	láska
<g/>
)	)	kIx)	)
pohřbí	pohřbít	k5eAaPmIp3nS	pohřbít
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
Lasturové	lasturový	k2eAgFnSc2d1	lasturová
vily	vila	k1gFnSc2	vila
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
Bill	Bill	k1gMnSc1	Bill
a	a	k8xC	a
Fleur	Fleur	k1gMnSc1	Fleur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krátura	Krátura	k1gFnSc1	Krátura
===	===	k?	===
</s>
</p>
<p>
<s>
Krátura	Krátura	k1gFnSc1	Krátura
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Kreacher	Kreachra	k1gFnPc2	Kreachra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
a	a	k8xC	a
špinavý	špinavý	k2eAgMnSc1d1	špinavý
domácí	domácí	k2eAgMnSc1d1	domácí
skřítek	skřítek	k1gMnSc1	skřítek
sloužící	sloužící	k2eAgFnSc3d1	sloužící
rodině	rodina	k1gFnSc3	rodina
Blacků	Black	k1gInPc2	Black
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
vyskytl	vyskytnout	k5eAaPmAgMnS	vyskytnout
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
"	"	kIx"	"
<g/>
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
"	"	kIx"	"
věci	věc	k1gFnPc4	věc
Blackových	Blackových	k2eAgFnPc4d1	Blackových
před	před	k7c7	před
vyhozením	vyhození	k1gNnSc7	vyhození
při	při	k7c6	při
úklidu	úklid	k1gInSc6	úklid
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
zřídil	zřídit	k5eAaPmAgInS	zřídit
hlavní	hlavní	k2eAgInSc1d1	hlavní
štáb	štáb	k1gInSc1	štáb
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
ho	on	k3xPp3gMnSc4	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Kráturův	Kráturův	k2eAgMnSc1d1	Kráturův
pán	pán	k1gMnSc1	pán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Siriusově	Siriusův	k2eAgFnSc6d1	Siriusova
smrti	smrt	k1gFnSc6	smrt
dům	dům	k1gInSc1	dům
připadne	připadnout	k5eAaPmIp3nS	připadnout
Harrymu	Harrym	k1gInSc3	Harrym
<g/>
.	.	kIx.	.
</s>
<s>
Krátura	Krátura	k1gFnSc1	Krátura
zpočátku	zpočátku	k6eAd1	zpočátku
svého	svůj	k3xOyFgMnSc4	svůj
nového	nový	k2eAgMnSc4d1	nový
pána	pán	k1gMnSc4	pán
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
zlý	zlý	k2eAgInSc4d1	zlý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
paktuje	paktovat	k5eAaImIp3nS	paktovat
s	s	k7c7	s
mudly	mudl	k1gMnPc7	mudl
a	a	k8xC	a
krvezrádci	krvezrádce	k1gMnPc7	krvezrádce
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
ale	ale	k8xC	ale
odhalí	odhalit	k5eAaPmIp3nS	odhalit
tajemství	tajemství	k1gNnSc1	tajemství
Krátury	Krátura	k1gFnSc2	Krátura
a	a	k8xC	a
Reguluse	Reguluse	k1gFnSc2	Reguluse
Blacka	Blacko	k1gNnSc2	Blacko
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc2	bratr
Siriuse	Siriuse	k1gFnSc2	Siriuse
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
Krátura	Krátura	k1gFnSc1	Krátura
uctívá	uctívat	k5eAaImIp3nS	uctívat
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
mu	on	k3xPp3gMnSc3	on
památku	památka	k1gFnSc4	památka
na	na	k7c4	na
jeho	jeho	k3xOp3gMnSc4	jeho
pána	pán	k1gMnSc4	pán
<g/>
,	,	kIx,	,
falešný	falešný	k2eAgInSc4d1	falešný
medailon	medailon	k1gInSc4	medailon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
nahradit	nahradit	k5eAaPmF	nahradit
viteál	viteál	k1gInSc1	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Kráturovi	Krátur	k1gMnSc3	Krátur
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gInSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
si	se	k3xPyFc3	se
domácích	domácí	k2eAgMnPc2d1	domácí
skřítků	skřítek	k1gMnPc2	skřítek
váží	vážit	k5eAaImIp3nS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Krátura	Krátura	k1gFnSc1	Krátura
Harryho	Harry	k1gMnSc2	Harry
oddaným	oddaný	k2eAgMnSc7d1	oddaný
sluhou	sluha	k1gMnSc7	sluha
<g/>
.	.	kIx.	.
</s>
<s>
Krátura	Krátura	k1gFnSc1	Krátura
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
účastní	účastnit	k5eAaImIp3nS	účastnit
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
bitvy	bitva	k1gFnPc4	bitva
o	o	k7c4	o
Bradavice	bradavice	k1gFnPc4	bradavice
proti	proti	k7c3	proti
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
a	a	k8xC	a
smrtijedům	smrtijed	k1gMnPc3	smrtijed
po	po	k7c6	po
boku	bok	k1gInSc6	bok
ostatních	ostatní	k2eAgMnPc2d1	ostatní
domácích	domácí	k1gMnPc2	domácí
skřítků	skřítek	k1gMnPc2	skřítek
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
svého	své	k1gNnSc2	své
pána	pán	k1gMnSc2	pán
Reguluse	Reguluse	k1gFnSc2	Reguluse
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc1	ochránce
domácích	domácí	k2eAgMnPc2d1	domácí
skřítků	skřítek	k1gMnPc2	skřítek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Winky	Wink	k1gInPc4	Wink
===	===	k?	===
</s>
</p>
<p>
<s>
Winky	Winky	k6eAd1	Winky
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
rodině	rodina	k1gFnSc3	rodina
Skrkových	Skrkových	k2eAgFnSc3d1	Skrkových
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
famfrpálu	famfrpál	k1gInSc6	famfrpál
hlídá	hlídat	k5eAaImIp3nS	hlídat
svému	svůj	k3xOyFgMnSc3	svůj
pánovi	pán	k1gMnSc3	pán
<g/>
,	,	kIx,	,
zaměstnanci	zaměstnanec	k1gMnSc3	zaměstnanec
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
Bartymu	Bartym	k1gInSc2	Bartym
Skrkovi	Skrek	k1gMnSc3	Skrek
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
lóži	lóže	k1gFnSc6	lóže
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
vyčarováno	vyčarován	k2eAgNnSc4d1	vyčarován
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
znamení	znamení	k1gNnSc2	znamení
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
křoví	křoví	k1gNnSc6	křoví
je	být	k5eAaImIp3nS	být
objevena	objevit	k5eAaPmNgFnS	objevit
Winky	Winek	k1gMnPc7	Winek
s	s	k7c7	s
Harryho	Harry	k1gMnSc4	Harry
ukradenou	ukradený	k2eAgFnSc7d1	ukradená
hůlkou	hůlka	k1gFnSc7	hůlka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
bylo	být	k5eAaImAgNnS	být
znamení	znamení	k1gNnSc1	znamení
vykouzleno	vykouzlit	k5eAaPmNgNnS	vykouzlit
<g/>
.	.	kIx.	.
</s>
<s>
Barty	Bart	k1gInPc1	Bart
Skrk	Skrka	k1gFnPc2	Skrka
st.	st.	kA	st.
Winky	Winka	k1gFnSc2	Winka
propustí	propustit	k5eAaPmIp3nS	propustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gInSc4	on
neuposlechla	uposlechnout	k5eNaPmAgFnS	uposlechnout
a	a	k8xC	a
nezůstala	zůstat	k5eNaPmAgFnS	zůstat
ve	v	k7c6	v
stanu	stan	k1gInSc6	stan
<g/>
.	.	kIx.	.
</s>
<s>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
skřítka	skřítek	k1gMnSc4	skřítek
nemohla	moct	k5eNaImAgFnS	moct
najít	najít	k5eAaPmF	najít
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potkala	potkat	k5eAaPmAgFnS	potkat
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
Dobbyho	Dobby	k1gMnSc4	Dobby
<g/>
,	,	kIx,	,
také	také	k9	také
svobodného	svobodný	k2eAgMnSc4d1	svobodný
skřítka	skřítek	k1gMnSc4	skřítek
<g/>
,	,	kIx,	,
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
Školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Winky	Winky	k6eAd1	Winky
ale	ale	k8xC	ale
své	svůj	k3xOyFgMnPc4	svůj
pány	pan	k1gMnPc4	pan
měla	mít	k5eAaImAgFnS	mít
ráda	rád	k2eAgFnSc1d1	ráda
a	a	k8xC	a
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
je	být	k5eAaImIp3nS	být
hluboce	hluboko	k6eAd1	hluboko
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
svůj	svůj	k3xOyFgInSc4	svůj
stesk	stesk	k1gInSc4	stesk
zahání	zahánět	k5eAaImIp3nS	zahánět
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
knihy	kniha	k1gFnSc2	kniha
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
syn	syn	k1gMnSc1	syn
jejího	její	k3xOp3gMnSc2	její
pána	pán	k1gMnSc2	pán
Barty	Barta	k1gMnSc2	Barta
Skrk	Skrk	k1gInSc1	Skrk
jr	jr	k?	jr
<g/>
.	.	kIx.	.
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
v	v	k7c6	v
Azkabanu	Azkaban	k1gInSc6	Azkaban
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spojil	spojit	k5eAaPmAgMnS	spojit
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pánem	pán	k1gMnSc7	pán
a	a	k8xC	a
teď	teď	k6eAd1	teď
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
mnoholičného	mnoholičný	k2eAgInSc2d1	mnoholičný
lektvaru	lektvar	k1gInSc2	lektvar
vydával	vydávat	k5eAaPmAgInS	vydávat
za	za	k7c4	za
učitele	učitel	k1gMnPc4	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
Alastora	Alastor	k1gMnSc2	Alastor
Moodyho	Moody	k1gMnSc2	Moody
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
Harryho	Harryha	k1gFnSc5	Harryha
<g/>
.	.	kIx.	.
</s>
<s>
Unikl	uniknout	k5eAaPmAgMnS	uniknout
právě	právě	k9	právě
při	při	k7c6	při
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
Winky	Winek	k1gMnPc4	Winek
propuštěna	propuštěn	k2eAgFnSc1d1	propuštěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hokey	Hoke	k2eAgMnPc4d1	Hoke
===	===	k?	===
</s>
</p>
<p>
<s>
Hokey	Hokey	k1gInPc4	Hokey
byla	být	k5eAaImAgFnS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
nejhubenější	hubený	k2eAgMnSc1d3	nejhubenější
skřítka	skřítek	k1gMnSc4	skřítek
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
Harry	Harro	k1gNnPc7	Harro
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
u	u	k7c2	u
staré	starý	k2eAgFnSc2d1	stará
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
bohaté	bohatý	k2eAgFnPc1d1	bohatá
čarodějky	čarodějka	k1gFnPc1	čarodějka
Hepziby	Hepziba	k1gFnSc2	Hepziba
Smithové	Smithová	k1gFnSc2	Smithová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
díle	díl	k1gInSc6	díl
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
Harry	Harra	k1gFnPc4	Harra
a	a	k8xC	a
Brumbál	brumbál	k1gMnSc1	brumbál
do	do	k7c2	do
Hokeyiny	Hokeyin	k2eAgFnSc2d1	Hokeyin
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
a	a	k8xC	a
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tom	Tom	k1gMnSc1	Tom
Rojvol	Rojvola	k1gFnPc2	Rojvola
Raddle	Raddle	k1gMnSc1	Raddle
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
šálek	šálek	k1gInSc4	šálek
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
a	a	k8xC	a
medailonek	medailonek	k1gInSc1	medailonek
Salazara	Salazar	k1gMnSc2	Salazar
Zmijozela	Zmijozela	k1gMnSc2	Zmijozela
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
její	její	k3xOp3gFnSc2	její
paní	paní	k1gFnSc2	paní
<g/>
.	.	kIx.	.
</s>
<s>
Raddle	Raddle	k6eAd1	Raddle
vpravil	vpravit	k5eAaPmAgInS	vpravit
skřítce	skřítka	k1gFnSc3	skřítka
falešnou	falešný	k2eAgFnSc4d1	falešná
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
měla	mít	k5eAaImAgFnS	mít
skřítka	skřítek	k1gMnSc2	skřítek
svou	svůj	k3xOyFgFnSc4	svůj
paní	paní	k1gFnSc4	paní
otrávit	otrávit	k5eAaPmF	otrávit
kakaem	kakao	k1gNnSc7	kakao
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
nebylo	být	k5eNaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečným	skutečný	k2eAgMnSc7d1	skutečný
vrahem	vrah	k1gMnSc7	vrah
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
ukradených	ukradený	k2eAgFnPc2d1	ukradená
věcí	věc	k1gFnPc2	věc
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
staly	stát	k5eAaPmAgFnP	stát
viteály	viteála	k1gFnPc1	viteála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mozkomor	Mozkomor	k1gInSc4	Mozkomor
==	==	k?	==
</s>
</p>
<p>
<s>
Mozkomor	Mozkomor	k1gInSc1	Mozkomor
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Dementor	Dementor	k1gInSc1	Dementor
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
šťastnými	šťastný	k2eAgInPc7d1	šťastný
pocity	pocit	k1gInPc7	pocit
a	a	k8xC	a
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vysává	vysávat	k5eAaImIp3nS	vysávat
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mozkomorové	Mozkomorová	k1gFnPc1	Mozkomorová
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejobávanějších	obávaný	k2eAgFnPc2d3	nejobávanější
nestvůr	nestvůra	k1gFnPc2	nestvůra
kouzelnického	kouzelnický	k2eAgInSc2d1	kouzelnický
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byli	být	k5eAaImAgMnP	být
využíváni	využíván	k2eAgMnPc1d1	využíván
Lordem	lord	k1gMnSc7	lord
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
přidali	přidat	k5eAaPmAgMnP	přidat
k	k	k7c3	k
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
působili	působit	k5eAaImAgMnP	působit
jako	jako	k9	jako
strážní	strážní	k1gMnPc1	strážní
v	v	k7c6	v
azkabanské	azkabanský	k2eAgFnSc6d1	azkabanská
věznici	věznice	k1gFnSc6	věznice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Azkabanu	Azkaban	k1gInSc2	Azkaban
byli	být	k5eAaImAgMnP	být
zavírání	zavírání	k1gNnPc1	zavírání
kouzelníci	kouzelník	k1gMnPc1	kouzelník
a	a	k8xC	a
čarodějky	čarodějka	k1gFnPc1	čarodějka
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
neprominutelné	prominutelný	k2eNgFnSc2d1	neprominutelná
kletby	kletba	k1gFnSc2	kletba
(	(	kIx(	(
<g/>
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
příčinou	příčina	k1gFnSc7	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
člověk	člověk	k1gMnSc1	člověk
či	či	k8xC	či
jiný	jiný	k2eAgMnSc1d1	jiný
živočich	živočich	k1gMnSc1	živočich
mučen	mučit	k5eAaImNgMnS	mučit
<g/>
,	,	kIx,	,
ovládán	ovládat	k5eAaImNgMnS	ovládat
či	či	k8xC	či
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
prohřešku	prohřešek	k1gInSc2	prohřešek
hodného	hodný	k2eAgNnSc2d1	hodné
potrestání	potrestání	k1gNnSc2	potrestání
<g/>
.	.	kIx.	.
</s>
<s>
Mozkomorové	Mozkomor	k1gMnPc1	Mozkomor
vysávali	vysávat	k5eAaImAgMnP	vysávat
z	z	k7c2	z
vězňů	vězeň	k1gMnPc2	vězeň
každou	každý	k3xTgFnSc4	každý
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
naděje	naděje	k1gFnSc2	naděje
či	či	k8xC	či
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
nechávali	nechávat	k5eAaImAgMnP	nechávat
je	on	k3xPp3gMnPc4	on
pomalu	pomalu	k6eAd1	pomalu
uvadat	uvadat	k5eAaImF	uvadat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zoufalství	zoufalství	k1gNnSc2	zoufalství
a	a	k8xC	a
zármutku	zármutek	k1gInSc2	zármutek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
uvězněných	uvězněný	k2eAgMnPc2d1	uvězněný
zešílela	zešílet	k5eAaPmAgFnS	zešílet
nebo	nebo	k8xC	nebo
zahynula	zahynout	k5eAaPmAgFnS	zahynout
z	z	k7c2	z
čistého	čistý	k2eAgInSc2d1	čistý
smutku	smutek	k1gInSc2	smutek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejhorší	zlý	k2eAgFnSc7d3	nejhorší
zbraní	zbraň	k1gFnSc7	zbraň
mozkomora	mozkomor	k1gMnSc2	mozkomor
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
mozkomorův	mozkomorův	k2eAgInSc4d1	mozkomorův
polibek	polibek	k1gInSc4	polibek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
mozkomor	mozkomor	k1gMnSc1	mozkomor
své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
vysaje	vysát	k5eAaPmIp3nS	vysát
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
zbude	zbýt	k5eAaPmIp3nS	zbýt
jen	jen	k9	jen
živé	živý	k2eAgNnSc1d1	živé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prázdné	prázdný	k2eAgNnSc1d1	prázdné
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
zachránil	zachránit	k5eAaPmAgInS	zachránit
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
před	před	k7c7	před
polibkem	polibek	k1gInSc7	polibek
jeho	jeho	k3xOp3gFnSc4	jeho
nový	nový	k2eAgMnSc1d1	nový
učitel	učitel	k1gMnSc1	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Patronova	patronův	k2eAgNnSc2d1	Patronovo
zaklínadla	zaklínadlo	k1gNnSc2	zaklínadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
opětovném	opětovný	k2eAgNnSc6d1	opětovné
povstání	povstání	k1gNnSc6	povstání
Lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
mozkomoři	mozkomor	k1gMnPc1	mozkomor
opět	opět	k6eAd1	opět
přešli	přejít	k5eAaPmAgMnP	přejít
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
nekontrolovaně	kontrolovaně	k6eNd1	kontrolovaně
množit	množit	k5eAaImF	množit
a	a	k8xC	a
živit	živit	k5eAaImF	živit
na	na	k7c6	na
běžné	běžný	k2eAgFnSc6d1	běžná
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mozkomoři	Mozkomor	k1gMnPc1	Mozkomor
nosí	nosit	k5eAaImIp3nP	nosit
rozedraný	rozedraný	k2eAgMnSc1d1	rozedraný
<g/>
,	,	kIx,	,
černošedý	černošedý	k2eAgInSc1d1	černošedý
plášť	plášť	k1gInSc1	plášť
s	s	k7c7	s
kápí	kápě	k1gFnSc7	kápě
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pláště	plášť	k1gInSc2	plášť
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
slizké	slizký	k2eAgFnPc1d1	slizká
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
se	se	k3xPyFc4	se
rozkládající	rozkládající	k2eAgFnPc1d1	rozkládající
ruce	ruka	k1gFnPc1	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
věc	věc	k1gFnSc1	věc
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
jsou	být	k5eAaImIp3nP	být
velká	velký	k2eAgNnPc1d1	velké
ústa	ústa	k1gNnPc1	ústa
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
vysají	vysát	k5eAaPmIp3nP	vysát
své	svůj	k3xOyFgFnSc3	svůj
oběti	oběť	k1gFnSc3	oběť
duši	duše	k1gFnSc3	duše
a	a	k8xC	a
udělají	udělat	k5eAaPmIp3nP	udělat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
něco	něco	k6eAd1	něco
podobné	podobný	k2eAgFnSc2d1	podobná
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Kápi	kápě	k1gFnSc4	kápě
si	se	k3xPyFc3	se
ale	ale	k9	ale
sundávají	sundávat	k5eAaImIp3nP	sundávat
pouze	pouze	k6eAd1	pouze
provádějí	provádět	k5eAaImIp3nP	provádět
<g/>
-li	i	k?	-li
"	"	kIx"	"
<g/>
polibek	polibek	k1gInSc4	polibek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mozkomoři	Mozkomor	k1gMnPc1	Mozkomor
nechodí	chodit	k5eNaImIp3nP	chodit
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vznášejí	vznášet	k5eAaImIp3nP	vznášet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
dokonce	dokonce	k9	dokonce
létají	létat	k5eAaImIp3nP	létat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
více	hodně	k6eAd2	hodně
mozkomorů	mozkomor	k1gInPc2	mozkomor
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
se	se	k3xPyFc4	se
ochladí	ochladit	k5eAaPmIp3nS	ochladit
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
mlha	mlha	k1gFnSc1	mlha
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
vát	vát	k2eAgInSc1d1	vát
studený	studený	k2eAgInSc1d1	studený
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
3,05	[number]	k4	3,05
m.	m.	k?	m.
</s>
</p>
<p>
<s>
S	s	k7c7	s
mozkomory	mozkomor	k1gMnPc7	mozkomor
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
bojovat	bojovat	k5eAaImF	bojovat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
3	[number]	k4	3
naučí	naučit	k5eAaPmIp3nS	naučit
Harryho	Harryha	k1gFnSc5	Harryha
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
Patronovo	patronův	k2eAgNnSc4d1	Patronovo
zaklínadlo	zaklínadlo	k1gNnSc4	zaklínadlo
<g/>
.	.	kIx.	.
</s>
<s>
Patronovo	patronův	k2eAgNnSc1d1	Patronovo
zaklínadlo	zaklínadlo	k1gNnSc1	zaklínadlo
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
strážce	strážce	k1gMnSc2	strážce
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
mozkomor	mozkomor	k1gMnSc1	mozkomor
vysává	vysávat	k5eAaImIp3nS	vysávat
místo	místo	k1gNnSc4	místo
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
silný	silný	k2eAgInSc1d1	silný
může	moct	k5eAaImIp3nS	moct
mozkomora	mozkomor	k1gMnSc4	mozkomor
úplně	úplně	k6eAd1	úplně
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kouzelník	kouzelník	k1gMnSc1	kouzelník
či	či	k8xC	či
čarodějka	čarodějka	k1gFnSc1	čarodějka
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
fyzického	fyzický	k2eAgMnSc4d1	fyzický
patrona	patron	k1gMnSc4	patron
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
nějakého	nějaký	k3yIgNnSc2	nějaký
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
každého	každý	k3xTgMnSc4	každý
patrona	patron	k1gMnSc4	patron
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
jiný	jiný	k2eAgInSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Testrálové	Testrál	k1gMnPc5	Testrál
==	==	k?	==
</s>
</p>
<p>
<s>
Testrálové	Testrál	k1gMnPc1	Testrál
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
popisováni	popisovat	k5eAaImNgMnP	popisovat
jako	jako	k9	jako
neobvyklá	obvyklý	k2eNgNnPc4d1	neobvyklé
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
asi	asi	k9	asi
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
podobající	podobající	k2eAgFnSc1d1	podobající
koním	kůň	k1gMnPc3	kůň
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
potažení	potažení	k1gNnSc2	potažení
jen	jen	k9	jen
černou	černý	k2eAgFnSc7d1	černá
kůží	kůže	k1gFnSc7	kůže
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
jasně	jasně	k6eAd1	jasně
rýsuje	rýsovat	k5eAaImIp3nS	rýsovat
každá	každý	k3xTgFnSc1	každý
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetech	hřbet	k1gInPc6	hřbet
mají	mít	k5eAaImIp3nP	mít
obrovská	obrovský	k2eAgNnPc4d1	obrovské
černá	černé	k1gNnPc4	černé
kožnatá	kožnatý	k2eAgNnPc4d1	kožnaté
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgNnPc4d1	podobné
těm	ten	k3xDgInPc3	ten
netopýřím	netopýří	k2eAgInPc3d1	netopýří
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
hlavy	hlava	k1gFnPc4	hlava
podobné	podobný	k2eAgFnSc2d1	podobná
dračím	dračí	k2eAgNnSc7d1	dračí
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
oči	oko	k1gNnPc4	oko
bez	bez	k7c2	bez
zorniček	zornička	k1gFnPc2	zornička
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
masožravci	masožravec	k1gMnPc1	masožravec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
díle	dílo	k1gNnSc6	dílo
série	série	k1gFnSc2	série
pasou	pást	k5eAaImIp3nP	pást
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
75	[number]	k4	75
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mohou	moct	k5eAaImIp3nP	moct
je	on	k3xPp3gNnSc4	on
spatřit	spatřit	k5eAaPmF	spatřit
jen	jen	k9	jen
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
viděli	vidět	k5eAaImAgMnP	vidět
někoho	někdo	k3yInSc4	někdo
umírat	umírat	k5eAaImF	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
vidí	vidět	k5eAaImIp3nP	vidět
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
ročníku	ročník	k1gInSc2	ročník
svědkem	svědek	k1gMnSc7	svědek
smrti	smrt	k1gFnSc2	smrt
Cedrika	Cedrika	k1gFnSc1	Cedrika
Diggoryho	Diggory	k1gMnSc2	Diggory
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	on	k3xPp3gMnPc4	on
vidí	vidět	k5eAaImIp3nS	vidět
Lenka	Lenka	k1gFnSc1	Lenka
Láskorádová	Láskorádový	k2eAgFnSc1d1	Láskorádová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zase	zase	k9	zase
viděla	vidět	k5eAaImAgFnS	vidět
umírat	umírat	k5eAaImF	umírat
svoji	svůj	k3xOyFgFnSc4	svůj
maminku	maminka	k1gFnSc4	maminka
a	a	k8xC	a
Neville	Nevill	k1gMnPc4	Nevill
Longbottom	Longbottom	k1gInSc4	Longbottom
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
viděl	vidět	k5eAaImAgMnS	vidět
zemřít	zemřít	k5eAaPmF	zemřít
svého	svůj	k3xOyFgMnSc4	svůj
dědečka	dědeček	k1gMnSc4	dědeček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Testrálové	Testrál	k1gMnPc1	Testrál
jsou	být	k5eAaImIp3nP	být
zapřaženi	zapřáhnout	k5eAaPmNgMnP	zapřáhnout
do	do	k7c2	do
kočárů	kočár	k1gInPc2	kočár
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
studenty	student	k1gMnPc4	student
od	od	k7c2	od
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
Bradavického	Bradavický	k2eAgInSc2d1	Bradavický
expresu	expres	k1gInSc2	expres
dopravují	dopravovat	k5eAaImIp3nP	dopravovat
do	do	k7c2	do
Školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
díle	dílo	k1gNnSc6	dílo
posloužili	posloužit	k5eAaPmAgMnP	posloužit
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
Ronovi	Ron	k1gMnSc6	Ron
<g/>
,	,	kIx,	,
Hermioně	Hermiona	k1gFnSc6	Hermiona
<g/>
,	,	kIx,	,
Lence	Lenka	k1gFnSc6	Lenka
<g/>
,	,	kIx,	,
Nevillovi	Nevillův	k2eAgMnPc1d1	Nevillův
a	a	k8xC	a
Ginny	Ginna	k1gFnPc1	Ginna
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nasedli	nasednout	k5eAaPmAgMnP	nasednout
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
doletěli	doletět	k5eAaPmAgMnP	doletět
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
Odboru	odbor	k1gInSc2	odbor
záhad	záhada	k1gFnPc2	záhada
konala	konat	k5eAaImAgFnS	konat
bitva	bitva	k1gFnSc1	bitva
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
a	a	k8xC	a
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žíně	žíně	k1gFnSc1	žíně
testrála	testrála	k1gFnSc1	testrála
bývá	bývat	k5eAaImIp3nS	bývat
používána	používat	k5eAaImNgFnS	používat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
substancí	substance	k1gFnPc2	substance
do	do	k7c2	do
jader	jádro	k1gNnPc2	jádro
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
hůlek	hůlka	k1gFnPc2	hůlka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrba	vrba	k1gFnSc1	vrba
mlátička	mlátička	k1gFnSc1	mlátička
==	==	k?	==
</s>
</p>
<p>
<s>
Vrba	vrba	k1gFnSc1	vrba
mlátička	mlátička	k1gFnSc1	mlátička
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Whomping	Whomping	k1gInSc1	Whomping
Willow	Willow	k1gFnSc2	Willow
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
tajných	tajný	k2eAgFnPc2d1	tajná
chodeb	chodba	k1gFnPc2	chodba
vedoucích	vedoucí	k1gMnPc2	vedoucí
z	z	k7c2	z
Bradavic	bradavice	k1gFnPc2	bradavice
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Prasinky	Prasinka	k1gFnSc2	Prasinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
vysazena	vysadit	k5eAaPmNgFnS	vysadit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
studentům	student	k1gMnPc3	student
následovat	následovat	k5eAaImF	následovat
ho	on	k3xPp3gMnSc4	on
při	při	k7c6	při
proměně	proměna	k1gFnSc6	proměna
do	do	k7c2	do
Chroptící	chroptící	k2eAgFnSc2d1	chroptící
chýše	chýš	k1gFnSc2	chýš
<g/>
.	.	kIx.	.
</s>
<s>
Vrba	vrba	k1gFnSc1	vrba
mlátivá	mlátivý	k2eAgFnSc1d1	mlátivá
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
ožene	ohnat	k5eAaPmIp3nS	ohnat
po	po	k7c6	po
každém	každý	k3xTgNnSc6	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
příliš	příliš	k6eAd1	příliš
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
stisku	stisk	k1gInSc6	stisk
určitého	určitý	k2eAgInSc2d1	určitý
suku	suk	k1gInSc2	suk
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
stromu	strom	k1gInSc2	strom
se	se	k3xPyFc4	se
uklidní	uklidnit	k5eAaPmIp3nS	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Lupin	lupina	k1gFnPc2	lupina
tak	tak	k9	tak
mohl	moct	k5eAaImAgInS	moct
normálně	normálně	k6eAd1	normálně
studovat	studovat	k5eAaImF	studovat
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Albuse	Albuse	k1gFnSc1	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Ron	Ron	k1gMnSc1	Ron
Weasley	Weaslea	k1gFnSc2	Weaslea
se	se	k3xPyFc4	se
s	s	k7c7	s
Vrbou	vrba	k1gFnSc7	vrba
mlátičkou	mlátička	k1gFnSc7	mlátička
poprvé	poprvé	k6eAd1	poprvé
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
knihy	kniha	k1gFnSc2	kniha
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
létajícím	létající	k2eAgNnSc7d1	létající
autem	auto	k1gNnSc7	auto
(	(	kIx(	(
<g/>
Ford	ford	k1gInSc1	ford
Anglia	Anglius	k1gMnSc2	Anglius
<g/>
)	)	kIx)	)
narazili	narazit	k5eAaPmAgMnP	narazit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
tento	tento	k3xDgInSc4	tento
strom	strom	k1gInSc4	strom
náležitě	náležitě	k6eAd1	náležitě
oplatil	oplatit	k5eAaPmAgMnS	oplatit
<g/>
.	.	kIx.	.
</s>
<s>
Auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
patřilo	patřit	k5eAaImAgNnS	patřit
Arthurovi	Arthur	k1gMnSc3	Arthur
Weasleymu	Weasleymum	k1gNnSc6	Weasleymum
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Rona	Ron	k1gMnSc2	Ron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
zdemolováno	zdemolovat	k5eAaPmNgNnS	zdemolovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přineslo	přinést	k5eAaPmAgNnS	přinést
Arthurovi	Arthur	k1gMnSc3	Arthur
značné	značný	k2eAgFnSc2d1	značná
potíže	potíž	k1gFnSc2	potíž
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
očarovávání	očarovávání	k1gNnSc1	očarovávání
mudlovských	mudlovský	k2eAgInPc2d1	mudlovský
výtvorů	výtvor	k1gInPc2	výtvor
bylo	být	k5eAaImAgNnS	být
přísně	přísně	k6eAd1	přísně
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrba	vrba	k1gFnSc1	vrba
mlátička	mlátička	k1gFnSc1	mlátička
také	také	k9	také
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
díle	dílo	k1gNnSc6	dílo
zlikvidovala	zlikvidovat	k5eAaPmAgFnS	zlikvidovat
Nimbus	nimbus	k1gInSc4	nimbus
2000	[number]	k4	2000
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Harry	Harr	k1gInPc4	Harr
poté	poté	k6eAd1	poté
dostal	dostat	k5eAaPmAgInS	dostat
Kulový	kulový	k2eAgInSc1d1	kulový
blesk	blesk	k1gInSc1	blesk
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
kmotra	kmotr	k1gMnSc2	kmotr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kůrolez	Kůrolez	k1gInSc4	Kůrolez
==	==	k?	==
</s>
</p>
<p>
<s>
Kůrolez	Kůrolez	k1gInSc1	Kůrolez
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Bowtruckle	Bowtruckl	k1gInSc5	Bowtruckl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvor	tvor	k1gMnSc1	tvor
plnící	plnící	k2eAgFnSc4d1	plnící
funkci	funkce	k1gFnSc4	funkce
strážce	strážce	k1gMnSc1	strážce
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
najdeme	najít	k5eAaPmIp1nP	najít
především	především	k9	především
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
skandinávských	skandinávský	k2eAgInPc6d1	skandinávský
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
těžké	těžký	k2eAgNnSc1d1	těžké
ho	on	k3xPp3gInSc4	on
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
drobný	drobný	k2eAgMnSc1d1	drobný
(	(	kIx(	(
<g/>
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc1d1	maximální
výšky	výška	k1gFnPc1	výška
osmi	osm	k4xCc2	osm
palců	palec	k1gInPc2	palec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
kůrou	kůra	k1gFnSc7	kůra
a	a	k8xC	a
větvičkami	větvička	k1gFnPc7	větvička
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
svítí	svítit	k5eAaImIp3nS	svítit
dvě	dva	k4xCgNnPc4	dva
malá	malý	k2eAgNnPc4d1	malé
hnědá	hnědý	k2eAgNnPc4d1	hnědé
očka	očko	k1gNnPc4	očko
<g/>
.	.	kIx.	.
</s>
<s>
Kůrolez	Kůrolez	k1gInSc1	Kůrolez
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
kouzelnickým	kouzelnický	k2eAgInSc7d1	kouzelnický
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mírumilovný	mírumilovný	k2eAgInSc4d1	mírumilovný
a	a	k8xC	a
nesmírně	smírně	k6eNd1	smírně
plachý	plachý	k2eAgMnSc1d1	plachý
tvor	tvor	k1gMnSc1	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
však	však	k9	však
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
něčím	něco	k3yInSc7	něco
ohrožen	ohrožen	k2eAgInSc1d1	ohrožen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
skočit	skočit	k5eAaPmF	skočit
na	na	k7c4	na
dřevorubce	dřevorubec	k1gMnPc4	dřevorubec
nebo	nebo	k8xC	nebo
prořezávače	prořezávač	k1gMnPc4	prořezávač
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
uškodit	uškodit	k5eAaPmF	uškodit
jeho	jeho	k3xOp3gInSc3	jeho
domovu	domov	k1gInSc3	domov
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrazit	vrazit	k5eAaPmF	vrazit
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
ostré	ostrý	k2eAgInPc4d1	ostrý
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Ubližuje	ubližovat	k5eAaImIp3nS	ubližovat
i	i	k9	i
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
si	se	k3xPyFc3	se
chtějí	chtít	k5eAaImIp3nP	chtít
vzít	vzít	k5eAaPmF	vzít
větvičku	větvička	k1gFnSc4	větvička
na	na	k7c4	na
hůlku	hůlka	k1gFnSc4	hůlka
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nP	stačit
je	on	k3xPp3gInPc4	on
odlákat	odlákat	k5eAaPmF	odlákat
kouzelnickým	kouzelnický	k2eAgInSc7d1	kouzelnický
hmyzem	hmyz	k1gInSc7	hmyz
nebo	nebo	k8xC	nebo
nabídnout	nabídnout	k5eAaPmF	nabídnout
domluvu	domluva	k1gFnSc4	domluva
<g/>
.	.	kIx.	.
</s>
<s>
Nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
<g/>
-li	i	k?	-li
čarodějka	čarodějka	k1gFnSc1	čarodějka
nebo	nebo	k8xC	nebo
kouzelník	kouzelník	k1gMnSc1	kouzelník
kůrolezovi	kůrolez	k1gMnSc3	kůrolez
na	na	k7c4	na
usmířenou	usmířená	k1gFnSc4	usmířená
porci	porce	k1gFnSc4	porce
stínek	stínka	k1gFnPc2	stínka
<g/>
,	,	kIx,	,
udobří	udobřit	k5eAaPmIp3nS	udobřit
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
stromu	strom	k1gInSc2	strom
stačí	stačit	k5eAaBmIp3nS	stačit
odříznout	odříznout	k5eAaPmF	odříznout
kus	kus	k1gInSc1	kus
dřeva	dřevo	k1gNnSc2	dřevo
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
hůlky	hůlka	k1gFnSc2	hůlka
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc2	film
Fantastická	fantastický	k2eAgNnPc1d1	fantastické
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rarach	rarach	k1gMnSc1	rarach
==	==	k?	==
</s>
</p>
<p>
<s>
Rarach	rarach	k1gMnSc1	rarach
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Pixie	Pixie	k1gFnSc1	Pixie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kovově	kovově	k6eAd1	kovově
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
,	,	kIx,	,
až	až	k9	až
25	[number]	k4	25
cm	cm	kA	cm
vysoký	vysoký	k2eAgMnSc1d1	vysoký
a	a	k8xC	a
velice	velice	k6eAd1	velice
nezbedný	nezbedný	k2eAgInSc1d1	nezbedný
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nemá	mít	k5eNaImIp3nS	mít
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
umění	umění	k1gNnSc1	umění
levitace	levitace	k1gFnSc2	levitace
<g/>
.	.	kIx.	.
</s>
<s>
Raraši	Raraš	k1gMnPc1	Raraš
se	se	k3xPyFc4	se
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
pisklavým	pisklavý	k2eAgNnPc3d1	pisklavé
švitořením	švitoření	k1gNnPc3	švitoření
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
srozumitelné	srozumitelný	k2eAgNnSc1d1	srozumitelné
jen	jen	k9	jen
jiným	jiný	k2eAgMnPc3d1	jiný
rarachům	rarach	k1gMnPc3	rarach
<g/>
.	.	kIx.	.
</s>
<s>
Rodí	rodit	k5eAaImIp3nP	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgNnSc7d3	nejčastější
místem	místo	k1gNnSc7	místo
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgInSc1d1	anglický
Cornwall	Cornwall	k1gInSc1	Cornwall
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc4	název
Cornwallští	Cornwallský	k2eAgMnPc1d1	Cornwallský
rarachové	rarach	k1gMnPc1	rarach
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Cornish	Cornish	k1gInSc1	Cornish
blue	blue	k1gFnSc1	blue
pixies	pixies	k1gMnSc1	pixies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
studentům	student	k1gMnPc3	student
při	při	k7c6	při
hodině	hodina	k1gFnSc6	hodina
Obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
profesor	profesor	k1gMnSc1	profesor
Zlatoslav	Zlatoslav	k1gMnSc1	Zlatoslav
Lockhart	Lockhart	k1gInSc4	Lockhart
<g/>
.	.	kIx.	.
</s>
<s>
Vypustí	vypustit	k5eAaPmIp3nP	vypustit
je	on	k3xPp3gFnPc4	on
z	z	k7c2	z
klece	klec	k1gFnSc2	klec
a	a	k8xC	a
raraši	raraš	k1gMnPc1	raraš
začnou	začít	k5eAaPmIp3nP	začít
dělat	dělat	k5eAaImF	dělat
nepořádek	nepořádek	k1gInSc4	nepořádek
<g/>
.	.	kIx.	.
</s>
<s>
Neschopnému	schopný	k2eNgMnSc3d1	neschopný
Lockhartovi	Lockhart	k1gMnSc3	Lockhart
raraši	raraš	k1gMnSc3	raraš
seberou	sebrat	k5eAaPmIp3nP	sebrat
hůlku	hůlka	k1gFnSc4	hůlka
a	a	k8xC	a
kouzlí	kouzlit	k5eAaImIp3nS	kouzlit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
Lockhart	Lockharta	k1gFnPc2	Lockharta
proto	proto	k8xC	proto
uteče	utéct	k5eAaPmIp3nS	utéct
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
kabinetu	kabinet	k1gInSc2	kabinet
a	a	k8xC	a
zanechá	zanechat	k5eAaPmIp3nS	zanechat
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
rarachy	rarach	k1gMnPc7	rarach
vypořádali	vypořádat	k5eAaPmAgMnP	vypořádat
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
studentů	student	k1gMnPc2	student
se	se	k3xPyFc4	se
ze	z	k7c2	z
třídy	třída	k1gFnSc2	třída
vytratí	vytratit	k5eAaPmIp3nP	vytratit
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
rarachy	rarach	k1gMnPc4	rarach
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Harryho	Harry	k1gMnSc2	Harry
a	a	k8xC	a
Rona	Ron	k1gMnSc2	Ron
zneškodní	zneškodnit	k5eAaPmIp3nS	zneškodnit
zaklínadlem	zaklínadlo	k1gNnSc7	zaklínadlo
<g/>
.	.	kIx.	.
</s>
</p>
