<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
pravoslavné	pravoslavný	k2eAgFnSc6d1	pravoslavná
tradici	tradice	k1gFnSc6	tradice
zvaný	zvaný	k2eAgMnSc1d1	zvaný
též	též	k6eAd1	též
Předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prorok	prorok	k1gMnSc1	prorok
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
figuruje	figurovat	k5eAaImIp3nS	figurovat
ve	v	k7c6	v
vyprávěných	vyprávěný	k2eAgNnPc6d1	vyprávěné
evangeliích	evangelium	k1gNnPc6	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
také	také	k6eAd1	také
židovský	židovský	k2eAgMnSc1d1	židovský
historik	historik	k1gMnSc1	historik
Flavius	Flavius	k1gMnSc1	Flavius
Iosephus	Iosephus	k1gMnSc1	Iosephus
ve	v	k7c6	v
spise	spis	k1gInSc6	spis
Židovské	židovský	k2eAgFnSc2d1	židovská
starožitnosti	starožitnost	k1gFnSc2	starožitnost
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
postarším	postarší	k2eAgMnPc3d1	postarší
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
Zacharjáši	Zacharjáš	k1gMnSc3	Zacharjáš
a	a	k8xC	a
Alžbětě	Alžběta	k1gFnSc3	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
narození	narození	k1gNnSc4	narození
zvěstoval	zvěstovat	k5eAaImAgMnS	zvěstovat
archanděl	archanděl	k1gMnSc1	archanděl
Gabriel	Gabriel	k1gMnSc1	Gabriel
Zachariášovi	Zachariáš	k1gMnSc3	Zachariáš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
oněměl	oněmět	k5eAaPmAgMnS	oněmět
až	až	k6eAd1	až
do	do	k7c2	do
provedení	provedení	k1gNnSc2	provedení
obřízky	obřízka	k1gFnSc2	obřízka
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
mladý	mladý	k1gMnSc1	mladý
odešel	odejít	k5eAaPmAgMnS	odejít
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připravil	připravit	k5eAaPmAgInS	připravit
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
budoucí	budoucí	k2eAgNnSc4d1	budoucí
povolání	povolání	k1gNnSc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
medem	med	k1gInSc7	med
divokých	divoký	k2eAgFnPc2d1	divoká
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
kobylkami	kobylka	k1gFnPc7	kobylka
a	a	k8xC	a
kořínky	kořínek	k1gInPc7	kořínek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
30	[number]	k4	30
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kazatelem	kazatel	k1gMnSc7	kazatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravoval	připravovat	k5eAaImAgInS	připravovat
lidi	člověk	k1gMnPc4	člověk
na	na	k7c4	na
příchod	příchod	k1gInSc4	příchod
Vykupitele	vykupitel	k1gMnSc2	vykupitel
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
příkladným	příkladný	k2eAgInSc7d1	příkladný
a	a	k8xC	a
asketickým	asketický	k2eAgInSc7d1	asketický
životem	život	k1gInSc7	život
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
přicházelo	přicházet	k5eAaImAgNnS	přicházet
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
křtil	křtít	k5eAaImAgInS	křtít
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
;	;	kIx,	;
takto	takto	k6eAd1	takto
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
i	i	k9	i
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
kázání	kázání	k1gNnPc2	kázání
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
i	i	k9	i
Héróda	Héróda	k1gMnSc1	Héróda
Antipu	Antip	k1gInSc2	Antip
za	za	k7c4	za
zavržení	zavržení	k1gNnSc4	zavržení
manželky	manželka	k1gFnSc2	manželka
a	a	k8xC	a
sňatek	sňatek	k1gInSc1	sňatek
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
vlastního	vlastní	k2eAgMnSc2d1	vlastní
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Héródés	Héródés	k6eAd1	Héródés
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
kritiku	kritika	k1gFnSc4	kritika
nechal	nechat	k5eAaPmAgMnS	nechat
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
řekla	říct	k5eAaPmAgFnS	říct
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
Salomé	Salomý	k2eAgFnSc2d1	Salomý
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
jeho	jeho	k3xOp3gFnSc4	jeho
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
stalo	stát	k5eAaPmAgNnS	stát
-	-	kIx~	-
Heródés	Heródés	k1gInSc1	Heródés
Salomé	Salomý	k2eAgNnSc4d1	Salomé
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
nechal	nechat	k5eAaPmAgMnS	nechat
stít	stít	k5eAaPmF	stít
<g/>
.	.	kIx.	.
</s>
<s>
Janův	Janův	k2eAgInSc1d1	Janův
životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
Flavius	Flavius	k1gMnSc1	Flavius
Josephus	Josephus	k1gMnSc1	Josephus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
je	být	k5eAaImIp3nS	být
charakterizován	charakterizován	k2eAgMnSc1d1	charakterizován
jako	jako	k8xC	jako
nekompromisní	kompromisní	k2eNgMnSc1d1	nekompromisní
kazatel	kazatel	k1gMnSc1	kazatel
ctnosti	ctnost	k1gFnSc2	ctnost
a	a	k8xC	a
pokání	pokání	k1gNnSc2	pokání
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
vnějším	vnější	k2eAgNnSc7d1	vnější
znamením	znamení	k1gNnSc7	znamení
je	být	k5eAaImIp3nS	být
křest	křest	k1gInSc1	křest
<g/>
.	.	kIx.	.
</s>
<s>
Herodes	Herodes	k1gMnSc1	Herodes
ho	on	k3xPp3gNnSc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
zabít	zabít	k5eAaPmF	zabít
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
Machairús	Machairúsa	k1gFnPc2	Machairúsa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
podnítit	podnítit	k5eAaPmF	podnítit
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Narození	narození	k1gNnSc1	narození
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
církev	církev	k1gFnSc1	církev
slaví	slavit	k5eAaImIp3nS	slavit
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
stětí	stětí	k1gNnSc2	stětí
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
patronem	patron	k1gInSc7	patron
křižáckého	křižácký	k2eAgInSc2d1	křižácký
řádu	řád	k1gInSc2	řád
johanitů	johanita	k1gMnPc2	johanita
-	-	kIx~	-
později	pozdě	k6eAd2	pozdě
nazvaných	nazvaný	k2eAgMnPc2d1	nazvaný
maltézští	maltézský	k2eAgMnPc1d1	maltézský
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
i	i	k9	i
patronem	patron	k1gMnSc7	patron
Malty	Malta	k1gFnSc2	Malta
a	a	k8xC	a
tamní	tamní	k2eAgFnSc2d1	tamní
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c6	v
městě	město	k1gNnSc6	město
La	la	k1gNnSc2	la
Valetta	Valett	k1gInSc2	Valett
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Burgundska	Burgundsko	k1gNnSc2	Burgundsko
<g/>
,	,	kIx,	,
Provence	Provence	k1gFnSc2	Provence
a	a	k8xC	a
slezského	slezský	k2eAgNnSc2d1	Slezské
města	město	k1gNnSc2	město
Wroclaw	Wroclaw	k1gFnSc2	Wroclaw
<g/>
.	.	kIx.	.
</s>
<s>
Ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
profesí	profes	k1gFnPc2	profes
<g/>
:	:	kIx,	:
tkalce	tkadlec	k1gMnPc4	tkadlec
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnPc4	krejčí
<g/>
,	,	kIx,	,
kožešníky	kožešník	k1gMnPc4	kožešník
<g/>
,	,	kIx,	,
koželuhy	koželuh	k1gMnPc4	koželuh
<g/>
,	,	kIx,	,
sedláře	sedlář	k1gMnPc4	sedlář
<g/>
,	,	kIx,	,
vinaře	vinař	k1gMnPc4	vinař
<g/>
,	,	kIx,	,
hospodské	hospodský	k1gMnPc4	hospodský
<g/>
,	,	kIx,	,
bednáře	bednář	k1gMnPc4	bednář
<g/>
,	,	kIx,	,
kominíky	kominík	k1gMnPc4	kominík
<g/>
,	,	kIx,	,
kováře	kovář	k1gMnPc4	kovář
<g/>
,	,	kIx,	,
tesaře	tesař	k1gMnPc4	tesař
<g/>
,	,	kIx,	,
architekty	architekt	k1gMnPc4	architekt
<g/>
,	,	kIx,	,
zedníky	zedník	k1gMnPc4	zedník
<g/>
,	,	kIx,	,
kameníky	kameník	k1gMnPc4	kameník
<g/>
,	,	kIx,	,
majitele	majitel	k1gMnPc4	majitel
biografů	biograf	k1gMnPc2	biograf
<g/>
,	,	kIx,	,
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
,	,	kIx,	,
tanečníky	tanečník	k1gMnPc4	tanečník
<g/>
,	,	kIx,	,
zpěváky	zpěvák	k1gMnPc4	zpěvák
atd.	atd.	kA	atd.
Atributem	atribut	k1gInSc7	atribut
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
v	v	k7c6	v
ikonografii	ikonografie	k1gFnSc6	ikonografie
je	být	k5eAaImIp3nS	být
velbloudí	velbloudí	k2eAgNnSc4d1	velbloudí
rouno	rouno	k1gNnSc4	rouno
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kožešina	kožešina	k1gFnSc1	kožešina
oděná	oděný	k2eAgFnSc1d1	oděná
na	na	k7c6	na
nahém	nahý	k2eAgNnSc6d1	nahé
těle	tělo	k1gNnSc6	tělo
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
jako	jako	k9	jako
poustevník	poustevník	k1gMnSc1	poustevník
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mívá	mívat	k5eAaImIp3nS	mívat
poutnickou	poutnický	k2eAgFnSc4d1	poutnická
hůl	hůl	k1gFnSc4	hůl
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
neupravené	upravený	k2eNgInPc4d1	neupravený
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
plnovous	plnovous	k1gInSc4	plnovous
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
zvykem	zvyk	k1gInSc7	zvyk
o	o	k7c6	o
svátku	svátek	k1gInSc6	svátek
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
je	být	k5eAaImIp3nS	být
pálení	pálení	k1gNnSc4	pálení
tzv.	tzv.	kA	tzv.
svatojánských	svatojánský	k2eAgInPc2d1	svatojánský
ohňů	oheň	k1gInPc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
sťatá	sťatat	k5eAaImIp3nS	sťatat
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc2	král
Heroda	Herod	k1gMnSc2	Herod
Antipy	Antipa	k1gFnSc2	Antipa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
a	a	k8xC	a
uctívána	uctívat	k5eAaImNgFnS	uctívat
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
byzantských	byzantský	k2eAgMnPc2d1	byzantský
císařů	císař	k1gMnPc2	císař
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ji	on	k3xPp3gFnSc4	on
údajně	údajně	k6eAd1	údajně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1203	[number]	k4	1203
uloupili	uloupit	k5eAaPmAgMnP	uloupit
účastníci	účastník	k1gMnPc1	účastník
4	[number]	k4	4
<g/>
.	.	kIx.	.
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
darem	dar	k1gInSc7	dar
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
králi	král	k1gMnSc3	král
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Svatému	svatý	k1gMnSc3	svatý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vystavil	vystavit	k5eAaPmAgInS	vystavit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
soukromé	soukromý	k2eAgFnSc6d1	soukromá
kapli	kaple	k1gFnSc6	kaple
Sainte-Chapelle	Sainte-Chapelle	k1gFnSc2	Sainte-Chapelle
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
relikvie	relikvie	k1gFnSc2	relikvie
byly	být	k5eAaImAgFnP	být
oddělovány	oddělován	k2eAgFnPc1d1	oddělována
částečky	částečka	k1gFnPc1	částečka
jako	jako	k8xS	jako
dary	dar	k1gInPc1	dar
evropským	evropský	k2eAgMnPc3d1	evropský
panovníkům	panovník	k1gMnPc3	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
legendární	legendární	k2eAgFnSc2d1	legendární
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
Umajjádovské	Umajjádovský	k2eAgFnSc2d1	Umajjádovský
mešity	mešita	k1gFnSc2	mešita
v	v	k7c6	v
syrském	syrský	k2eAgInSc6d1	syrský
Damašku	Damašek	k1gInSc6	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
byl	být	k5eAaImAgMnS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
patronem	patron	k1gInSc7	patron
mnoha	mnoho	k4c2	mnoho
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
řádových	řádový	k2eAgInPc2d1	řádový
chrámů	chrám	k1gInPc2	chrám
benediktinů	benediktin	k1gMnPc2	benediktin
v	v	k7c6	v
Ostrově	ostrov	k1gInSc6	ostrov
u	u	k7c2	u
Davle	Davle	k1gFnSc2	Davle
a	a	k8xC	a
u	u	k7c2	u
sv.	sv.	kA	sv.
Jana	Jana	k1gFnSc1	Jana
pod	pod	k7c7	pod
Skalou	Skala	k1gMnSc7	Skala
<g/>
,	,	kIx,	,
benediktinek	benediktinka	k1gFnPc2	benediktinka
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
(	(	kIx(	(
<g/>
a	a	k8xC	a
města	město	k1gNnSc2	město
Teplice	teplice	k1gFnSc2	teplice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
kláštera	klášter	k1gInSc2	klášter
cisterciáků	cisterciák	k1gMnPc2	cisterciák
v	v	k7c6	v
Oseku	Osek	k1gInSc6	Osek
u	u	k7c2	u
Duchcova	Duchcův	k2eAgNnSc2d1	Duchcův
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
chován	chovat	k5eAaImNgInS	chovat
relikviář	relikviář	k1gInSc1	relikviář
s	s	k7c7	s
paží	paže	k1gFnSc7	paže
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nezvěstný	zvěstný	k2eNgInSc4d1	nezvěstný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
paže	paže	k1gNnPc1	paže
byla	být	k5eAaImAgNnP	být
uctívána	uctívat	k5eAaImNgNnP	uctívat
ve	v	k7c6	v
Svatovítském	svatovítský	k2eAgInSc6d1	svatovítský
pokladu	poklad	k1gInSc6	poklad
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
relikviáře	relikviář	k1gInPc1	relikviář
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
uťaté	uťatý	k2eAgFnSc2d1	uťatá
hlavy	hlava	k1gFnSc2	hlava
na	na	k7c6	na
talíři	talíř	k1gInSc6	talíř
byly	být	k5eAaImAgFnP	být
uctívány	uctívat	k5eAaImNgFnP	uctívat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
příklad	příklad	k1gInSc1	příklad
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrný	k2eAgNnSc4d1	stříbrné
poprsí	poprsí	k1gNnSc4	poprsí
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
zlatnické	zlatnický	k2eAgFnSc2d1	Zlatnická
dílny	dílna	k1gFnSc2	dílna
doby	doba	k1gFnSc2	doba
gotické	gotický	k2eAgNnSc1d1	gotické
je	být	k5eAaImIp3nS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
cisterciaček	cisterciačka	k1gFnPc2	cisterciačka
v	v	k7c6	v
lužickém	lužický	k2eAgInSc6d1	lužický
St.	st.	kA	st.
Mariensternu	Mariensterna	k1gFnSc4	Mariensterna
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ostatky	ostatek	k1gInPc1	ostatek
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgFnPc1d1	uložena
v	v	k7c6	v
relikviáři	relikviář	k1gInSc6	relikviář
svatého	svatý	k2eAgMnSc2d1	svatý
Maura	Maur	k1gMnSc2	Maur
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Bečově	Bečov	k1gInSc6	Bečov
nad	nad	k7c7	nad
Teplou	Teplá	k1gFnSc7	Teplá
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
der	drát	k5eAaImRp2nS	drát
christlichen	christlichen	k1gInSc1	christlichen
Ikonographie	Ikonographie	k1gFnPc1	Ikonographie
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Brauenfels	Brauenfelsa	k1gFnPc2	Brauenfelsa
<g/>
,	,	kIx,	,
Band	banda	k1gFnPc2	banda
A-J	A-J	k1gFnSc2	A-J
<g/>
,	,	kIx,	,
Basel-Freiburg-Rom-Wien	Basel-Freiburg-Rom-Wien	k1gInSc4	Basel-Freiburg-Rom-Wien
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
