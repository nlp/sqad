<s desamb="1">
Songhajské	Songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
jsou	být	k5eAaImIp3nP
také	také	k9
často	často	k6eAd1
řazeny	řadit	k5eAaImNgFnP
do	do	k7c2
velké	velký	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
nilosaharských	nilosaharský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
spojení	spojení	k1gNnSc1
je	být	k5eAaImIp3nS
ale	ale	k9
sporné	sporný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>