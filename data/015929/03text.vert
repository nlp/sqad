<s>
Songhajské	Songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Songhajské	Songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
Rozšíření	rozšíření	k1gNnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
songhajských	songhajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
na	na	k7c6
mapě	mapa	k1gFnSc6
Afriky	Afrika	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Korandje	Korandje	k1gFnSc1
<g/>
,	,	kIx,
Koyra	Koyra	k1gMnSc1
chiini	chiin	k1gMnPc1
<g/>
,	,	kIx,
<g/>
Tadaksahak	Tadaksahak	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Tasawaq	Tasawaq	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Tagdal	Tagdal	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Tondi	Tond	k1gMnPc1
songway	songwaa	k1gFnSc2
kiini	kiiň	k1gMnSc3
<g/>
,	,	kIx,
<g/>
Humburi	Humbur	k1gMnSc3
senniKoyraboro	senniKoyrabora	k1gFnSc5
senni	senň	k1gMnPc7
<g/>
,	,	kIx,
<g/>
Zarma	Zarma	k1gFnSc1
<g/>
,	,	kIx,
<g/>
DendiRozšíření	DendiRozšíření	k1gNnSc1
</s>
<s>
Západní	západní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
(	(	kIx(
<g/>
Mali	Mali	k1gNnSc1
Mali	Mali	k1gNnSc2
<g/>
,	,	kIx,
Niger	Niger	k1gInSc1
Niger	Niger	k1gInSc1
<g/>
,	,	kIx,
Benin	Benin	k1gMnSc1
Benin	Benin	k1gMnSc1
<g/>
,	,	kIx,
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
<g/>
,	,	kIx,
Nigérie	Nigérie	k1gFnSc1
NigérieAlžírsko	NigérieAlžírsko	k1gNnSc1
Alžírsko	Alžírsko	k1gNnSc1
Počet	počet	k1gInSc4
mluvčích	mluvčí	k1gMnPc2
</s>
<s>
přes	přes	k7c4
4	#num#	k4
miliony	milion	k4xCgInPc4
Počet	počet	k1gInSc1
jazyků	jazyk	k1gInPc2
</s>
<s>
10	#num#	k4
živých	živá	k1gFnPc2
Klasifikace	klasifikace	k1gFnSc2
</s>
<s>
nilosaharské	nilosaharský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
(	(	kIx(
<g/>
sporné	sporný	k2eAgFnSc3d1
<g/>
)	)	kIx)
Dělení	dělení	k1gNnSc1
Dělení	dělení	k1gNnSc1
</s>
<s>
severní	severní	k2eAgInPc1d1
songhajské	songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
jižní	jižní	k2eAgInPc1d1
songhajské	songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Songhajské	Songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
jsou	být	k5eAaImIp3nP
jazykovou	jazykový	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
patří	patřit	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
vzájemně	vzájemně	k6eAd1
blízkých	blízký	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
používaných	používaný	k2eAgInPc2d1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
především	především	k6eAd1
pak	pak	k6eAd1
v	v	k7c6
údolí	údolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Niger	Niger	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
mluvčích	mluvčí	k1gFnPc2
songhajských	songhajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
žije	žít	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Mali	Mali	k1gNnSc2
a	a	k8xC
Nigeru	Niger	k1gInSc2
<g/>
,	,	kIx,
oblasti	oblast	k1gFnSc2
kde	kde	k6eAd1
se	se	k3xPyFc4
mluví	mluvit	k5eAaImIp3nS
songhajskými	songhajský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
se	se	k3xPyFc4
ale	ale	k8xC
nachází	nacházet	k5eAaImIp3nS
také	také	k9
v	v	k7c6
Nigérii	Nigérie	k1gFnSc6
<g/>
,	,	kIx,
Beninu	Benino	k1gNnSc6
<g/>
,	,	kIx,
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
a	a	k8xC
Alžírsku	Alžírsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
dvě	dva	k4xCgFnPc4
třetiny	třetina	k1gFnPc1
mluvčích	mluvčí	k1gMnPc2
songhajských	songhajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
tvoří	tvořit	k5eAaImIp3nS
mluvčí	mluvčí	k1gMnSc1
jazyka	jazyk	k1gInSc2
zarma	zarmum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Songhajské	Songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
jsou	být	k5eAaImIp3nP
pojmenované	pojmenovaný	k2eAgInPc1d1
podle	podle	k7c2
Songhajské	Songhajský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
významné	významný	k2eAgFnPc4d1
říše	říš	k1gFnPc4
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ovládala	ovládat	k5eAaImAgFnS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
západní	západní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
songhajské	songhajský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
používali	používat	k5eAaImAgMnP
jako	jako	k9
lingua	lingua	k1gMnSc1
franca	franca	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Songhajské	Songhajský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
existovaly	existovat	k5eAaImAgInP
zápisy	zápis	k1gInPc1
některých	některý	k3yIgInPc2
songhajských	songhajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
v	v	k7c6
arabském	arabský	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
se	se	k3xPyFc4
všechny	všechen	k3xTgInPc1
songhajské	songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
píší	psát	k5eAaImIp3nP
latinkou	latinka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
songhajských	songhajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
jsou	být	k5eAaImIp3nP
jazyky	jazyk	k1gInPc1
tónové	tónový	k2eAgInPc1d1
<g/>
,	,	kIx,
se	s	k7c7
slovosledem	slovosled	k1gInSc7
SOV	sova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zařazení	zařazení	k1gNnSc1
a	a	k8xC
rozdělení	rozdělení	k1gNnSc1
</s>
<s>
Lingvista	lingvista	k1gMnSc1
Lionel	Lionel	k1gMnSc1
Bender	Bender	k1gMnSc1
a	a	k8xC
někteří	některý	k3yIgMnPc1
další	další	k2eAgMnPc1d1
spojují	spojovat	k5eAaImIp3nP
songhajské	songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
se	s	k7c7
saharskými	saharský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Songhajské	Songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
jsou	být	k5eAaImIp3nP
také	také	k9
často	často	k6eAd1
řazeny	řadit	k5eAaImNgFnP
do	do	k7c2
velké	velký	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
nilosaharských	nilosaharský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
spojení	spojení	k1gNnSc1
je	být	k5eAaImIp3nS
ale	ale	k9
sporné	sporný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Songhajské	Songhajský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
podskupiny	podskupina	k1gFnPc4
<g/>
:	:	kIx,
jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
severní	severní	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Srovnání	srovnání	k1gNnSc1
</s>
<s>
Číslovka	číslovka	k1gFnSc1
jedna	jeden	k4xCgFnSc1
v	v	k7c6
některých	některý	k3yIgInPc6
songhajských	songhajský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
Jazyk	jazyk	k1gInSc1
</s>
<s>
Slovo	slovo	k1gNnSc1
jedna	jeden	k4xCgFnSc1
v	v	k7c6
onom	onen	k3xDgInSc6
jazyce	jazyk	k1gInSc6
</s>
<s>
Korandje	Korandje	k1gFnSc1
</s>
<s>
affu	affu	k6eAd1
</s>
<s>
Tadaksahak	Tadaksahak	k6eAd1
</s>
<s>
a-ˈ	a-ˈ	k1gMnSc1
/	/	kIx~
a-ˈ	a-ˈ	k1gMnSc1
</s>
<s>
Tasawaq	Tasawaq	k?
</s>
<s>
fó	fó	k?
/	/	kIx~
a-fː	a-fː	k?
</s>
<s>
Dendi	Dend	k1gMnPc1
</s>
<s>
afɔ	afɔ	k?
</s>
<s>
Koyraboro	Koyrabora	k1gFnSc5
senni	seneň	k1gFnSc5
</s>
<s>
affoo	affoo	k6eAd1
</s>
<s>
Koyra	Koyra	k6eAd1
chiini	chiin	k1gMnPc1
</s>
<s>
foo	foo	k?
/	/	kIx~
a-foo	a-foo	k1gMnSc1
</s>
<s>
Zarma	Zarma	k1gFnSc1
</s>
<s>
à	à	k?
</s>
<s>
Přehled	přehled	k1gInSc1
</s>
<s>
Přehled	přehled	k1gInSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
songhajských	songhajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Jižní	jižní	k2eAgInPc1d1
songhajské	songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Jazyk	jazyk	k1gInSc1
</s>
<s>
Přibližný	přibližný	k2eAgInSc1d1
počet	počet	k1gInSc1
mluvčích	mluvčí	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Místo	místo	k7c2
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Zarma	Zarma	k1gFnSc1
</s>
<s>
3,6	3,6	k4
milionů	milion	k4xCgInPc2
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
Nigeru	Niger	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Niamey	Niamea	k1gFnSc2
<g/>
,	,	kIx,
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
</s>
<s>
Dialekt	dialekt	k1gInSc1
jazyka	jazyk	k1gInSc2
zvaný	zvaný	k2eAgInSc1d1
Songhoyboro	Songhoybora	k1gFnSc5
Ciinese	Ciinese	k1gFnSc1
někdy	někdy	k6eAd1
bere	brát	k5eAaImIp3nS
také	také	k9
jako	jako	k9
samostatný	samostatný	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koyraboro	Koyrabora	k1gFnSc5
senni	seneň	k1gFnSc5
</s>
<s>
430	#num#	k4
000	#num#	k4
</s>
<s>
Údolí	údolí	k1gNnSc1
Nigeru	Niger	k1gInSc2
v	v	k7c6
Mali	Mali	k1gNnSc6
<g/>
,	,	kIx,
východně	východně	k6eAd1
od	od	k7c2
Timbuktu	Timbukt	k1gInSc2
a	a	k8xC
v	v	k7c6
Gao	Gao	k1gFnSc6
</s>
<s>
Dendi	Dend	k1gMnPc1
</s>
<s>
250	#num#	k4
000	#num#	k4
</s>
<s>
sever	sever	k1gInSc1
Beninu	Benin	k1gInSc2
a	a	k8xC
přilehlá	přilehlý	k2eAgFnSc1d1
část	část	k1gFnSc1
Nigérie	Nigérie	k1gFnSc1
</s>
<s>
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
jako	jako	k9
obchodní	obchodní	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
na	na	k7c6
severu	sever	k1gInSc6
Beninu	Benin	k1gInSc2
</s>
<s>
Tondi	Tond	k1gMnPc1
songway	songwaa	k1gFnSc2
kiini	kiien	k2eAgMnPc1d1
</s>
<s>
3000	#num#	k4
</s>
<s>
Několik	několik	k4yIc1
vesnic	vesnice	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Kikary	Kikara	k1gFnSc2
ve	v	k7c6
středním	střední	k2eAgNnSc6d1
Mali	Mali	k1gNnSc6
</s>
<s>
Humburi	Humburi	k6eAd1
senni	senn	k1gMnPc1
</s>
<s>
25	#num#	k4
000	#num#	k4
</s>
<s>
Okolí	okolí	k1gNnSc1
Hombori	Hombor	k1gFnSc2
v	v	k7c6
Mali	Mali	k1gNnSc6
a	a	k8xC
v	v	k7c4
Burkina	Burkin	k1gMnSc4
Faso	Faso	k6eAd1
</s>
<s>
Severní	severní	k2eAgInPc1d1
songhajské	songhajský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Jazyk	jazyk	k1gInSc1
</s>
<s>
Přibližný	přibližný	k2eAgInSc1d1
počet	počet	k1gInSc1
mluvčích	mluvčí	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Místo	místo	k7c2
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Korandje	Korandje	k1gFnSc1
</s>
<s>
3000	#num#	k4
</s>
<s>
Město	město	k1gNnSc1
Tabelbala	Tabelbal	k1gMnSc2
v	v	k7c6
severozápadním	severozápadní	k2eAgNnSc6d1
Alžírsku	Alžírsko	k1gNnSc6
<g/>
,	,	kIx,
asi	asi	k9
160	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
hranic	hranice	k1gFnPc2
s	s	k7c7
Marokem	Maroko	k1gNnSc7
</s>
<s>
Nejseverněji	severně	k6eAd3
rozšířený	rozšířený	k2eAgInSc1d1
songhajský	songhajský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
do	do	k7c2
oblasti	oblast	k1gFnSc2
Alžírska	Alžírsko	k1gNnSc2
dostal	dostat	k5eAaPmAgInS
se	s	k7c7
songhajskými	songhajský	k2eAgMnPc7d1
obchodníky	obchodník	k1gMnPc7
</s>
<s>
Koyra	Koyra	k6eAd1
chiini	chiin	k1gMnPc1
</s>
<s>
200	#num#	k4
000	#num#	k4
</s>
<s>
Timbuktu	Timbukt	k1gInSc3
v	v	k7c6
Mali	Mali	k1gNnSc6
a	a	k8xC
přilehlá	přilehlý	k2eAgFnSc1d1
část	část	k1gFnSc1
údolí	údolí	k1gNnSc2
Nigeru	Niger	k1gInSc2
</s>
<s>
Koyra	Koyra	k6eAd1
chiini	chiin	k1gMnPc1
má	mít	k5eAaImIp3nS
dialekt	dialekt	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
Djenné	Djenný	k2eAgFnSc3d1
chiini	chiin	k1gMnPc1
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
v	v	k7c6
Djenné	Djenný	k2eAgFnSc6d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
sice	sice	k8xC
s	s	k7c7
koyra	koyra	k6eAd1
chiini	chiin	k1gMnPc1
vzájemně	vzájemně	k6eAd1
srozumitelný	srozumitelný	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
</s>
<s>
Tadaksahak	Tadaksahak	k6eAd1
</s>
<s>
100	#num#	k4
000	#num#	k4
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
západu	západ	k1gInSc2
Mali	Mali	k1gNnSc2
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
také	také	k9
Nigeru	Niger	k1gInSc2
</s>
<s>
Tasawaq	Tasawaq	k?
</s>
<s>
8000	#num#	k4
</s>
<s>
Město	město	k1gNnSc1
In-Gall	In-Galla	k1gFnPc2
ve	v	k7c6
středním	střední	k2eAgInSc6d1
Nigeru	Niger	k1gInSc6
</s>
<s>
Tagdal	Tagdat	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
27	#num#	k4
000	#num#	k4
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
ve	v	k7c6
středním	střední	k2eAgInSc6d1
Nigeru	Niger	k1gInSc6
</s>
<s>
Podle	podle	k7c2
Ethnologue	Ethnologu	k1gInSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
smíšený	smíšený	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vznikl	vzniknout	k5eAaPmAgInS
spojením	spojení	k1gNnSc7
songhajského	songhajský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
berberštiny	berberština	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Songhay	Songhaa	k1gFnSc2
languages	languagesa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Ethnologue	Ethnologue	k1gFnSc1
<g/>
↑	↑	k?
Glottolog	Glottolog	k1gMnSc1
4.2	4.2	k4
<g/>
.1	.1	k4
-	-	kIx~
Kaado	Kaado	k1gNnSc1
<g/>
.	.	kIx.
glottolog	glottolog	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ethnologue	Ethnologue	k1gInSc1
</s>
