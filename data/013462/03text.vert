<p>
<s>
Loď	loď	k1gFnSc1	loď
Lipsko	Lipsko	k1gNnSc4	Lipsko
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnPc1	první
z	z	k7c2	z
pěti	pět	k4xCc2	pět
sesterských	sesterský	k2eAgFnPc2d1	sesterská
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pro	pro	k7c4	pro
Dopravní	dopravní	k2eAgInSc4d1	dopravní
podnik	podnik	k1gInSc4	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
DPMB	DPMB	kA	DPMB
<g/>
)	)	kIx)	)
postavila	postavit	k5eAaPmAgFnS	postavit
loděnice	loděnice	k1gFnSc1	loděnice
firmy	firma	k1gFnSc2	firma
Jesko	Jesko	k1gNnSc1	Jesko
CZ	CZ	kA	CZ
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jiráň	Jiráň	k1gFnSc1	Jiráň
<g/>
)	)	kIx)	)
v	v	k7c6	v
Hlavečníku	Hlavečník	k1gInSc6	Hlavečník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
byla	být	k5eAaImAgFnS	být
zařazena	zařazen	k2eAgFnSc1d1	zařazena
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
pod	pod	k7c7	pod
evidenčním	evidenční	k2eAgNnSc7d1	evidenční
číslem	číslo	k1gNnSc7	číslo
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
potřeby	potřeba	k1gFnSc2	potřeba
postupně	postupně	k6eAd1	postupně
obnovit	obnovit	k5eAaPmF	obnovit
flotilu	flotila	k1gFnSc4	flotila
lodí	loď	k1gFnPc2	loď
vyrobených	vyrobený	k2eAgFnPc2d1	vyrobená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
objednal	objednat	k5eAaPmAgMnS	objednat
DPMB	DPMB	kA	DPMB
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
sérii	série	k1gFnSc4	série
pěti	pět	k4xCc2	pět
nových	nový	k2eAgNnPc2d1	nové
plavidel	plavidlo	k1gNnPc2	plavidlo
za	za	k7c4	za
celkovou	celkový	k2eAgFnSc4d1	celková
částku	částka	k1gFnSc4	částka
73,915	[number]	k4	73,915
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Jesko	Jesko	k1gNnSc1	Jesko
CZ	CZ	kA	CZ
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
první	první	k4xOgFnSc2	první
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
dožívajícího	dožívající	k2eAgMnSc4d1	dožívající
Pionýra	pionýr	k1gMnSc4	pionýr
a	a	k8xC	a
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pracovně	pracovně	k6eAd1	pracovně
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
jako	jako	k8xC	jako
Pionýr	pionýr	k1gInSc1	pionýr
II	II	kA	II
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
se	s	k7c7	s
stavitelem	stavitel	k1gMnSc7	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
ve	v	k7c6	v
Lhotce	Lhotka	k1gFnSc6	Lhotka
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
přetáhnut	přetáhnut	k2eAgInSc4d1	přetáhnut
do	do	k7c2	do
loděnice	loděnice	k1gFnSc2	loděnice
v	v	k7c6	v
Hlavečníku	Hlavečník	k1gInSc6	Hlavečník
u	u	k7c2	u
Přelouče	Přelouč	k1gFnSc2	Přelouč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
výzbroj	výzbroj	k1gFnSc4	výzbroj
se	se	k3xPyFc4	se
postarala	postarat	k5eAaPmAgFnS	postarat
firma	firma	k1gFnSc1	firma
Regul	Regul	k1gInSc1	Regul
Tech	Tech	k?	Tech
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
plavidlo	plavidlo	k1gNnSc1	plavidlo
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
po	po	k7c6	po
partnerském	partnerský	k2eAgNnSc6d1	partnerské
městě	město	k1gNnSc6	město
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
německém	německý	k2eAgNnSc6d1	německé
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
tramvaj	tramvaj	k1gFnSc4	tramvaj
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lipsko	Lipsko	k1gNnSc1	Lipsko
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
čtyř	čtyři	k4xCgInPc2	čtyři
dnů	den	k1gInPc2	den
přepraveno	přepravit	k5eAaPmNgNnS	přepravit
na	na	k7c6	na
silničním	silniční	k2eAgInSc6d1	silniční
trajleru	trajler	k1gInSc6	trajler
na	na	k7c4	na
Brněnskou	brněnský	k2eAgFnSc4d1	brněnská
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
,	,	kIx,	,
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
svého	svůj	k3xOyFgNnSc2	svůj
působiště	působiště	k1gNnSc2	působiště
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkompletování	zkompletování	k1gNnSc6	zkompletování
lodi	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
paluba	paluba	k1gFnSc1	paluba
<g/>
,	,	kIx,	,
kormidelna	kormidelna	k1gFnSc1	kormidelna
<g/>
,	,	kIx,	,
maketa	maketa	k1gFnSc1	maketa
komínu	komín	k1gInSc2	komín
<g/>
,	,	kIx,	,
elektrická	elektrický	k2eAgFnSc1d1	elektrická
výzbroj	výzbroj	k1gFnSc1	výzbroj
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
poprvé	poprvé	k6eAd1	poprvé
zkušebně	zkušebně	k6eAd1	zkušebně
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Křest	křest	k1gInSc1	křest
plavidla	plavidlo	k1gNnSc2	plavidlo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
spuštění	spuštění	k1gNnSc4	spuštění
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
přehrady	přehrada	k1gFnSc2	přehrada
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
zahájení	zahájení	k1gNnSc2	zahájení
plavební	plavební	k2eAgFnSc2d1	plavební
sezóny	sezóna	k1gFnSc2	sezóna
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
závady	závada	k1gFnSc2	závada
na	na	k7c6	na
elektroinstalaci	elektroinstalace	k1gFnSc6	elektroinstalace
(	(	kIx(	(
<g/>
vypálení	vypálení	k1gNnSc4	vypálení
frekvenčního	frekvenční	k2eAgInSc2d1	frekvenční
měniče	měnič	k1gInSc2	měnič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
bouřkou	bouřka	k1gFnSc7	bouřka
z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
Lipsko	Lipsko	k1gNnSc1	Lipsko
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
DPMB	DPMB	kA	DPMB
musel	muset	k5eAaImAgInS	muset
počkat	počkat	k5eAaPmF	počkat
na	na	k7c4	na
dodávku	dodávka	k1gFnSc4	dodávka
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Zkušební	zkušební	k2eAgFnPc1d1	zkušební
plavby	plavba	k1gFnPc1	plavba
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
a	a	k8xC	a
první	první	k4xOgFnSc1	první
plavba	plavba	k1gFnSc1	plavba
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
provozu	provoz	k1gInSc6	provoz
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
plavby	plavba	k1gFnPc4	plavba
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
malém	malý	k1gMnSc6	malý
okruhu	okruh	k1gInSc2	okruh
do	do	k7c2	do
Rokle	rokle	k1gFnSc2	rokle
a	a	k8xC	a
objednané	objednaný	k2eAgFnSc2d1	objednaná
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
okruh	okruh	k1gInSc4	okruh
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
mohlo	moct	k5eAaImAgNnS	moct
Lipsko	Lipsko	k1gNnSc1	Lipsko
vyplout	vyplout	k5eAaPmF	vyplout
až	až	k9	až
po	po	k7c6	po
úpravě	úprava	k1gFnSc6	úprava
přístavních	přístavní	k2eAgInPc2d1	přístavní
můstků	můstek	k1gInPc2	můstek
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
rokem	rok	k1gInSc7	rok
provozu	provoz	k1gInSc2	provoz
nové	nový	k2eAgFnSc2d1	nová
lodi	loď	k1gFnSc2	loď
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
DPMB	DPMB	kA	DPMB
výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c4	na
dodatečné	dodatečný	k2eAgFnPc4d1	dodatečná
úpravy	úprava	k1gFnPc4	úprava
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
1	[number]	k4	1
525	[number]	k4	525
053	[number]	k4	053
Kč	Kč	kA	Kč
firma	firma	k1gFnSc1	firma
Jesko	Jesko	k1gNnSc1	Jesko
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Lipsko	Lipsko	k1gNnSc4	Lipsko
vybavila	vybavit	k5eAaPmAgFnS	vybavit
příďovým	příďový	k2eAgNnSc7d1	příďové
dokormidlovacím	dokormidlovací	k2eAgNnSc7d1	dokormidlovací
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
fotovoltaickými	fotovoltaický	k2eAgFnPc7d1	fotovoltaická
články	článek	k1gInPc4	článek
střeše	střecha	k1gFnSc3	střecha
kormidelny	kormidelna	k1gFnSc2	kormidelna
pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
uživatelské	uživatelský	k2eAgFnSc2d1	Uživatelská
baterie	baterie	k1gFnSc2	baterie
<g/>
,	,	kIx,	,
bezpečnostními	bezpečnostní	k2eAgFnPc7d1	bezpečnostní
brankami	branka	k1gFnPc7	branka
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
vchodech	vchod	k1gInPc6	vchod
a	a	k8xC	a
náporovými	náporový	k2eAgInPc7d1	náporový
větracími	větrací	k2eAgInPc7d1	větrací
kanály	kanál	k1gInPc7	kanál
nad	nad	k7c7	nad
čelním	čelní	k2eAgNnSc7d1	čelní
oknem	okno	k1gNnSc7	okno
dolní	dolní	k2eAgFnSc2d1	dolní
paluby	paluba	k1gFnSc2	paluba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
Lipsko	Lipsko	k1gNnSc4	Lipsko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velké	velký	k2eAgFnPc4d1	velká
lodě	loď	k1gFnPc4	loď
provozované	provozovaný	k2eAgFnPc4d1	provozovaná
DPMB	DPMB	kA	DPMB
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dvoupalubovou	dvoupalubový	k2eAgFnSc4d1	dvoupalubová
loď	loď	k1gFnSc4	loď
s	s	k7c7	s
horní	horní	k2eAgFnSc7d1	horní
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sluneční	sluneční	k2eAgInPc1d1	sluneční
<g/>
)	)	kIx)	)
palubou	paluba	k1gFnSc7	paluba
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kormidelna	kormidelna	k1gFnSc1	kormidelna
a	a	k8xC	a
vzadu	vzadu	k6eAd1	vzadu
maketa	maketa	k1gFnSc1	maketa
komínu	komín	k1gInSc2	komín
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
sklad	sklad	k1gInSc4	sklad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
dolní	dolní	k2eAgFnSc1d1	dolní
paluba	paluba	k1gFnSc1	paluba
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
v	v	k7c6	v
bezbarierovém	bezbarierový	k2eAgNnSc6d1	bezbarierový
provedení	provedení	k1gNnSc6	provedení
<g/>
,	,	kIx,	,
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
plavidla	plavidlo	k1gNnSc2	plavidlo
se	s	k7c7	s
záchody	záchod	k1gInPc7	záchod
a	a	k8xC	a
schodištěm	schodiště	k1gNnSc7	schodiště
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
palubu	paluba	k1gFnSc4	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
poháněna	pohánět	k5eAaImNgFnS	pohánět
asynchronním	asynchronní	k2eAgInSc7d1	asynchronní
elektromotorem	elektromotor	k1gInSc7	elektromotor
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
45	[number]	k4	45
kW	kW	kA	kW
přes	přes	k7c4	přes
frekvenční	frekvenční	k2eAgInSc4d1	frekvenční
měnič	měnič	k1gInSc4	měnič
z	z	k7c2	z
trakční	trakční	k2eAgFnSc2d1	trakční
baterie	baterie	k1gFnSc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Elektromotor	elektromotor	k1gInSc1	elektromotor
uvádí	uvádět	k5eAaImIp3nS	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
čtyřlistou	čtyřlistý	k2eAgFnSc4d1	čtyřlistá
vrtuli	vrtule	k1gFnSc4	vrtule
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
686	[number]	k4	686
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
trupu	trup	k1gInSc2	trup
činí	činit	k5eAaImIp3nS	činit
25	[number]	k4	25
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
6,22	[number]	k4	6,22
m	m	kA	m
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
prázdné	prázdný	k2eAgFnSc2d1	prázdná
lodi	loď	k1gFnSc2	loď
58	[number]	k4	58
t	t	k?	t
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgInSc1d1	maximální
ponor	ponor	k1gInSc1	ponor
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,15	[number]	k4	1,15
m.	m.	k?	m.
Plavidlo	plavidlo	k1gNnSc1	plavidlo
může	moct	k5eAaImIp3nS	moct
plout	plout	k5eAaImF	plout
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
15	[number]	k4	15
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
pojme	pojmout	k5eAaPmIp3nS	pojmout
nejvýše	nejvýše	k6eAd1	nejvýše
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lipsko	Lipsko	k1gNnSc4	Lipsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
