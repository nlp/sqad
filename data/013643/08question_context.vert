<s>
Hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
aditivní	aditivní	k2eAgFnSc1d1
vlastnost	vlastnost	k1gFnSc1
hmoty	hmota	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
vlastnost	vlastnost	k1gFnSc1
jednotlivých	jednotlivý	k2eAgNnPc2d1
hmotných	hmotný	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
míru	míra	k1gFnSc4
setrvačných	setrvačný	k2eAgInPc2d1
účinků	účinek	k1gInPc2
či	či	k8xC
míru	míra	k1gFnSc4
gravitačních	gravitační	k2eAgInPc2d1
účinků	účinek	k1gInPc2
hmoty	hmota	k1gFnSc2
<g/>
.	.	kIx.
</s>