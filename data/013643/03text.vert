<s>
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
HmotnostNázev	HmotnostNázet	k5eAaPmDgInS
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
<g/>
,	,	kIx,
m	m	kA
Hlavní	hlavní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
SI	se	k3xPyFc3
</s>
<s>
kilogram	kilogram	k1gInSc1
Značka	značka	k1gFnSc1
hlavní	hlavní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
SI	se	k3xPyFc3
</s>
<s>
kg	kg	kA
Rozměrový	rozměrový	k2eAgInSc4d1
symbol	symbol	k1gInSc4
SI	se	k3xPyFc3
</s>
<s>
M	M	kA
Dle	dle	k7c2
transformace	transformace	k1gFnSc2
složek	složka	k1gFnPc2
</s>
<s>
skalární	skalární	k2eAgNnSc1d1
Zařazení	zařazení	k1gNnSc1
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	se	k3xPyFc3
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
aditivní	aditivní	k2eAgFnSc1d1
vlastnost	vlastnost	k1gFnSc1
hmoty	hmota	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
vlastnost	vlastnost	k1gFnSc1
jednotlivých	jednotlivý	k2eAgNnPc2d1
hmotných	hmotný	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
míru	míra	k1gFnSc4
setrvačných	setrvačný	k2eAgInPc2d1
účinků	účinek	k1gInPc2
či	či	k8xC
míru	míra	k1gFnSc4
gravitačních	gravitační	k2eAgInPc2d1
účinků	účinek	k1gInPc2
hmoty	hmota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ekvivalence	ekvivalence	k1gFnSc1
setrvačných	setrvačný	k2eAgFnPc2d1
a	a	k8xC
gravitačních	gravitační	k2eAgFnPc2d1
sil	síla	k1gFnPc2
je	být	k5eAaImIp3nS
postulována	postulovat	k5eAaImNgFnS
obecnou	obecný	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
relativity	relativita	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
experimentálně	experimentálně	k6eAd1
ověřena	ověřen	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
obdobná	obdobný	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
hmoty	hmota	k1gFnSc2
jako	jako	k8xC,k8xS
např.	např.	kA
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
elektrický	elektrický	k2eAgInSc4d1
náboj	náboj	k1gInSc4
apod.	apod.	kA
</s>
<s>
Značení	značení	k1gNnSc1
</s>
<s>
Symbol	symbol	k1gInSc1
veličiny	veličina	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
mass	mass	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
SI	SI	kA
<g/>
:	:	kIx,
kilogram	kilogram	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
kg	kg	kA
</s>
<s>
Další	další	k2eAgFnPc1d1
používané	používaný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
tuna	tuna	k1gFnSc1
t	t	k?
<g/>
,	,	kIx,
gram	gram	k1gInSc1
g	g	kA
<g/>
,	,	kIx,
karát	karát	k1gInSc1
Kt	Kt	k1gFnSc1
<g/>
,	,	kIx,
sluneční	sluneční	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
M	M	kA
</s>
<s>
⨀	⨀	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
M_	M_	k1gMnPc6
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
bigodot	bigodot	k1gInSc1
}}	}}	k?
</s>
<s>
,	,	kIx,
atomová	atomový	k2eAgFnSc1d1
hmotnostní	hmotnostní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
u	u	k7c2
<g/>
,	,	kIx,
elektronvolt	elektronvolt	k1gInSc1
eV	eV	k?
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
2	#num#	k4
<g/>
,	,	kIx,
...	...	k?
</s>
<s>
Planckova	Planckův	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
hmotnosti	hmotnost	k1gFnSc2
<g/>
:	:	kIx,
2,177	2,177	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
kg	kg	kA
</s>
<s>
Anglo-americké	Anglo-americký	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
libra	libra	k1gFnSc1
<g/>
,	,	kIx,
unce	unce	k1gFnSc1
<g/>
,	,	kIx,
kámen	kámen	k1gInSc1
(	(	kIx(
<g/>
stone	ston	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Starší	starý	k2eAgFnPc1d2
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
debet	debet	k1gInSc1
<g/>
,	,	kIx,
talent	talent	k1gInSc1
<g/>
,	,	kIx,
pud	pud	k1gInSc1
<g/>
,	,	kIx,
abbásí	abbásí	k1gNnSc1
<g/>
,	,	kIx,
…	…	k?
</s>
<s>
Měřidla	měřidlo	k1gNnPc1
hmotnosti	hmotnost	k1gFnSc2
<g/>
:	:	kIx,
váhy	váha	k1gFnSc2
(	(	kIx(
<g/>
rovnoramenné	rovnoramenný	k2eAgNnSc4d1
<g/>
,	,	kIx,
nerovnoramenné	rovnoramenný	k2eNgNnSc4d1
<g/>
,	,	kIx,
pružinové	pružinový	k2eAgNnSc4d1
<g/>
,	,	kIx,
elektronické	elektronický	k2eAgNnSc4d1
<g/>
,	,	kIx,
wattové	wattový	k2eAgNnSc4d1
<g/>
)	)	kIx)
</s>
<s>
Setrvačná	setrvačný	k2eAgFnSc1d1
a	a	k8xC
gravitační	gravitační	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
se	se	k3xPyFc4
fyzikálně	fyzikálně	k6eAd1
projevuje	projevovat	k5eAaImIp3nS
dvěma	dva	k4xCgInPc7
způsoby	způsob	k1gInPc7
<g/>
,	,	kIx,
podle	podle	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
setrvačná	setrvačný	k2eAgFnSc1d1
resp.	resp.	kA
gravitační	gravitační	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
setrvačná	setrvačný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
míra	míra	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
silovým	silový	k2eAgNnSc7d1
působením	působení	k1gNnSc7
měněn	měněn	k2eAgInSc4d1
pohybový	pohybový	k2eAgInSc4d1
stav	stav	k1gInSc4
hmotného	hmotný	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
pro	pro	k7c4
setrvačnou	setrvačný	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Newtonův	Newtonův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
lze	lze	k6eAd1
zjednodušeně	zjednodušeně	k6eAd1
zapsat	zapsat	k5eAaPmF
ve	v	k7c6
tvaru	tvar	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
m	m	kA
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
ma	ma	k?
<g/>
\	\	kIx~
<g/>
,,	,,	k?
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
F	F	kA
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
celková	celkový	k2eAgFnSc1d1
působící	působící	k2eAgFnSc1d1
<g/>
)	)	kIx)
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
m	m	kA
je	být	k5eAaImIp3nS
setrvačná	setrvačný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
tělesa	těleso	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
okamžité	okamžitý	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
tělesa	těleso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kolikrát	kolikrát	k6eAd1
větší	veliký	k2eAgFnSc4d2
setrvačnou	setrvačný	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
má	mít	k5eAaImIp3nS
těleso	těleso	k1gNnSc1
<g/>
,	,	kIx,
tolikrát	tolikrát	k6eAd1
menší	malý	k2eAgNnSc4d2
zrychlení	zrychlení	k1gNnSc4
mu	on	k3xPp3gMnSc3
udělí	udělit	k5eAaPmIp3nS
působící	působící	k2eAgFnSc1d1
celková	celkový	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
plyne	plynout	k5eAaImIp3nS
i	i	k9
stejný	stejný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
pro	pro	k7c4
setrvačné	setrvačný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
:	:	kIx,
Ve	v	k7c6
zrychleně	zrychleně	k6eAd1
se	se	k3xPyFc4
pohybujících	pohybující	k2eAgFnPc6d1
vztažných	vztažný	k2eAgFnPc6d1
soustavách	soustava	k1gFnPc6
je	být	k5eAaImIp3nS
působící	působící	k2eAgFnSc1d1
setrvačná	setrvačný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
přímo	přímo	k6eAd1
úměrná	úměrný	k2eAgFnSc1d1
setrvačné	setrvačný	k2eAgFnSc3d1
hmotnosti	hmotnost	k1gFnSc3
tělesa	těleso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
gravitační	gravitační	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
míra	míra	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
na	na	k7c4
sebe	sebe	k3xPyFc4
gravitačně	gravitačně	k6eAd1
působí	působit	k5eAaImIp3nP
hmotná	hmotný	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
pro	pro	k7c4
gravitační	gravitační	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
je	být	k5eAaImIp3nS
Newtonův	Newtonův	k2eAgInSc1d1
gravitační	gravitační	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
lze	lze	k6eAd1
zjednodušeně	zjednodušeně	k6eAd1
zapsat	zapsat	k5eAaPmF
(	(	kIx(
<g/>
pro	pro	k7c4
tělesa	těleso	k1gNnPc4
zanedbatelných	zanedbatelný	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
<g/>
)	)	kIx)
ve	v	k7c6
tvaru	tvar	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
G	G	kA
</s>
<s>
m	m	kA
</s>
<s>
1	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
2	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
G	G	kA
<g/>
{	{	kIx(
<g/>
m_	m_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
m_	m_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
\	\	kIx~
<g/>
over	over	k1gInSc1
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
,,	,,	k?
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
F	F	kA
je	být	k5eAaImIp3nS
gravitační	gravitační	k2eAgFnSc1d1
síla	síla	k1gFnSc1
působící	působící	k2eAgFnSc1d1
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
hmotnými	hmotný	k2eAgFnPc7d1
tělesy	těleso	k1gNnPc7
<g/>
,	,	kIx,
G	G	kA
je	být	k5eAaImIp3nS
gravitační	gravitační	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
<g/>
,	,	kIx,
m	m	kA
<g/>
1	#num#	k4
a	a	k8xC
m	m	kA
<g/>
2	#num#	k4
gravitační	gravitační	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
těles	těleso	k1gNnPc2
a	a	k8xC
r	r	kA
jejich	jejich	k3xOp3gFnSc4
vzdálenost	vzdálenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kolikrát	kolikrát	k6eAd1
větší	veliký	k2eAgFnSc1d2
gravitační	gravitační	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
má	mít	k5eAaImIp3nS
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
tolikrát	tolikrát	k6eAd1
větší	veliký	k2eAgFnSc7d2
silou	síla	k1gFnSc7
bude	být	k5eAaImBp3nS
gravitačně	gravitačně	k6eAd1
působit	působit	k5eAaImF
na	na	k7c4
jiná	jiný	k2eAgNnPc4d1
hmotná	hmotný	k2eAgNnPc4d1
tělesa	těleso	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
postuloval	postulovat	k5eAaImAgMnS
v	v	k7c6
obecné	obecná	k1gFnSc6
teorií	teorie	k1gFnPc2
relativity	relativita	k1gFnSc2
ekvivalenci	ekvivalence	k1gFnSc4
setrvačných	setrvačný	k2eAgFnPc2d1
a	a	k8xC
gravitačních	gravitační	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
tedy	tedy	k9
kvalitativní	kvalitativní	k2eAgFnSc1d1
i	i	k8xC
kvantitativní	kvantitativní	k2eAgFnSc1d1
shodnost	shodnost	k1gFnSc1
jejich	jejich	k3xOp3gInPc2
projevů	projev	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
rovnost	rovnost	k1gFnSc1
je	být	k5eAaImIp3nS
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
experimentálně	experimentálně	k6eAd1
ověřena	ověřen	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Lze	lze	k6eAd1
tedy	tedy	k9
hovořit	hovořit	k5eAaImF
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
rozlišovat	rozlišovat	k5eAaImF
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
míru	míra	k1gFnSc4
setrvačných	setrvačný	k2eAgInPc2d1
či	či	k8xC
gravitačních	gravitační	k2eAgInPc2d1
účinků	účinek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Klidová	klidový	k2eAgFnSc1d1
a	a	k8xC
relativistická	relativistický	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Ve	v	k7c6
speciální	speciální	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
relativity	relativita	k1gFnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
principiálně	principiálně	k6eAd1
odlišné	odlišný	k2eAgInPc1d1
koncepty	koncept	k1gInPc1
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
m	m	kA
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m_	m_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
(	(	kIx(
<g/>
též	též	k9
vlastní	vlastní	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
,	,	kIx,
invariantní	invariantní	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hmotnost	hmotnost	k1gFnSc1
tělesa	těleso	k1gNnSc2
měřená	měřený	k2eAgFnSc1d1
například	například	k6eAd1
na	na	k7c6
rovnoramenných	rovnoramenný	k2eAgFnPc6d1
vahách	váha	k1gFnPc6
ve	v	k7c6
vztažné	vztažný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
,	,	kIx,
vůči	vůči	k7c3
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
je	být	k5eAaImIp3nS
těleso	těleso	k1gNnSc1
v	v	k7c6
klidu	klid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částice	částice	k1gFnPc1
jako	jako	k8xC,k8xS
fotony	foton	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
nikdy	nikdy	k6eAd1
v	v	k7c6
klidu	klid	k1gInSc6
nejsou	být	k5eNaImIp3nP
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
klidovou	klidový	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
nulovou	nulový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vlastnost	vlastnost	k1gFnSc1
tělesa	těleso	k1gNnSc2
je	být	k5eAaImIp3nS
stejná	stejný	k2eAgFnSc1d1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
inerciálních	inerciální	k2eAgFnPc6d1
soustavách	soustava	k1gFnPc6
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
invariantní	invariantní	k2eAgMnSc1d1
vůči	vůči	k7c3
Lorentzově	Lorentzův	k2eAgFnSc3d1
transformaci	transformace	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjadřuje	vyjadřovat	k5eAaImIp3nS
množství	množství	k1gNnSc4
látky	látka	k1gFnSc2
v	v	k7c6
tělese	těleso	k1gNnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
shodná	shodný	k2eAgFnSc1d1
s	s	k7c7
koncepcí	koncepce	k1gFnSc7
hmotnosti	hmotnost	k1gFnSc2
v	v	k7c6
Newtonově	Newtonův	k2eAgFnSc6d1
klasické	klasický	k2eAgFnSc6d1
mechanice	mechanika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
klasické	klasický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
ale	ale	k8xC
při	při	k7c6
relativistických	relativistický	k2eAgInPc6d1
dějích	děj	k1gInPc6
neplatí	platit	k5eNaImIp3nS
zákon	zákon	k1gInSc1
zachování	zachování	k1gNnSc2
klidové	klidový	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
srážkou	srážka	k1gFnSc7
částic	částice	k1gFnPc2
na	na	k7c6
urychlovači	urychlovač	k1gInSc6
mohou	moct	k5eAaImIp3nP
vzniknout	vzniknout	k5eAaPmF
částice	částice	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
úhrnná	úhrnný	k2eAgFnSc1d1
klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
původních	původní	k2eAgFnPc2d1
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
částicové	částicový	k2eAgFnSc6d1
a	a	k8xC
teoretické	teoretický	k2eAgFnSc6d1
fyzice	fyzika	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
výhradně	výhradně	k6eAd1
klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
,	,	kIx,
nazývá	nazývat	k5eAaImIp3nS
se	se	k3xPyFc4
stručně	stručně	k6eAd1
slovem	slovem	k6eAd1
hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
značí	značit	k5eAaImIp3nS
se	se	k3xPyFc4
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
až	až	k9
na	na	k7c4
jednotky	jednotka	k1gFnPc4
ekvivalentní	ekvivalentní	k2eAgFnSc3d1
klidové	klidový	k2eAgFnSc3d1
energii	energie	k1gFnSc3
tělesa	těleso	k1gNnSc2
</s>
<s>
E	E	kA
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
E_	E_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3
fyzikálním	fyzikální	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
(	(	kIx(
<g/>
resp.	resp.	kA
klidová	klidový	k2eAgFnSc1d1
energie	energie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
relace	relace	k1gFnSc1
mezi	mezi	k7c7
energií	energie	k1gFnSc7
a	a	k8xC
hybností	hybnost	k1gFnSc7
tělesa	těleso	k1gNnSc2
zvaná	zvaný	k2eAgFnSc1d1
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
o	o	k7c4
energii	energie	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
E	E	kA
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
E	E	kA
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
p	p	k?
</s>
<s>
c	c	k0
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
E	E	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
E_	E_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
left	left	k1gInSc1
<g/>
(	(	kIx(
<g/>
pc	pc	k?
<g/>
\	\	kIx~
<g/>
right	right	k1gMnSc1
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
</s>
<s>
Relativistická	relativistický	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
veličina	veličina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
až	až	k9
na	na	k7c4
jednotky	jednotka	k1gFnPc4
ekvivalentní	ekvivalentní	k2eAgFnSc2d1
celkové	celkový	k2eAgFnSc2d1
energii	energie	k1gFnSc4
tělesa	těleso	k1gNnSc2
podle	podle	k7c2
vztahu	vztah	k1gInSc2
E	E	kA
<g/>
=	=	kIx~
<g/>
mc²	mc²	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
konstanta	konstanta	k1gFnSc1
<g/>
,	,	kIx,
rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Relativistická	relativistický	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
roste	růst	k5eAaImIp3nS
s	s	k7c7
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
zrychlování	zrychlování	k1gNnSc6
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
kinetická	kinetický	k2eAgFnSc1d1
energie	energie	k1gFnSc1
tělesa	těleso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dané	daný	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
má	mít	k5eAaImIp3nS
tedy	tedy	k9
různou	různý	k2eAgFnSc4d1
relativistickou	relativistický	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
pro	pro	k7c4
různé	různý	k2eAgMnPc4d1
pozorovatele	pozorovatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
veličina	veličina	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
ČR	ČR	kA
používá	používat	k5eAaImIp3nS
zejména	zejména	k9
ve	v	k7c6
středoškolské	středoškolský	k2eAgFnSc6d1
výuce	výuka	k1gFnSc6
a	a	k8xC
v	v	k7c6
učebnicích	učebnice	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
stručně	stručně	k6eAd1
hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
značí	značit	k5eAaImIp3nS
se	se	k3xPyFc4
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
veličina	veličina	k1gFnSc1
nevyjadřuje	vyjadřovat	k5eNaImIp3nS
množství	množství	k1gNnSc4
látky	látka	k1gFnSc2
v	v	k7c6
tělese	těleso	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
látka	látka	k1gFnSc1
zrychlováním	zrychlování	k1gNnPc3
nepřibývá	přibývat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
hmotnost	hmotnost	k1gFnSc4
platí	platit	k5eAaImIp3nS
zákon	zákon	k1gInSc1
zachování	zachování	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
ekvivalent	ekvivalent	k1gInSc4
zákona	zákon	k1gInSc2
zachování	zachování	k1gNnSc2
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používáme	používat	k5eAaImIp1nP
<g/>
-li	-li	k?
relativistickou	relativistický	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
<g/>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
psát	psát	k5eAaImF
beze	beze	k7c2
změny	změna	k1gFnSc2
klasický	klasický	k2eAgInSc4d1
vztah	vztah	k1gInSc4
pro	pro	k7c4
hybnost	hybnost	k1gFnSc4
tělesa	těleso	k1gNnSc2
</s>
<s>
p	p	k?
</s>
<s>
=	=	kIx~
</s>
<s>
m	m	kA
</s>
<s>
v	v	k7c6
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
p	p	k?
<g/>
=	=	kIx~
<g/>
mv	mv	k?
<g/>
}	}	kIx)
</s>
<s>
Tuto	tento	k3xDgFnSc4
relativistickou	relativistický	k2eAgFnSc4d1
hybnost	hybnost	k1gFnSc4
lze	lze	k6eAd1
použít	použít	k5eAaPmF
v	v	k7c6
pohybové	pohybový	k2eAgFnSc6d1
rovnici	rovnice	k1gFnSc6
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
p	p	k?
</s>
<s>
/	/	kIx~
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
p	p	k?
<g/>
/	/	kIx~
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}	}	kIx)
</s>
<s>
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
síly	síla	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
relativistická	relativistický	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
mírou	míra	k1gFnSc7
setrvačnosti	setrvačnost	k1gFnSc2
tělesa	těleso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Klidovou	klidový	k2eAgFnSc4d1
a	a	k8xC
relativistickou	relativistický	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
tělesa	těleso	k1gNnSc2
můžeme	moct	k5eAaImIp1nP
vzájemně	vzájemně	k6eAd1
přepočítávat	přepočítávat	k5eAaImF
<g/>
,	,	kIx,
pokud	pokud	k8xS
známe	znát	k5eAaImIp1nP
rychlost	rychlost	k1gFnSc4
tělesa	těleso	k1gNnSc2
ve	v	k7c6
zvolené	zvolený	k2eAgFnSc6d1
vztažné	vztažný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
m	m	kA
</s>
<s>
=	=	kIx~
</s>
<s>
m	m	kA
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
v	v	k7c6
</s>
<s>
2	#num#	k4
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
m_	m_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}}}}	}}}}}}}	k?
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
vztahu	vztah	k1gInSc6
značí	značit	k5eAaImIp3nS
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
relativistickou	relativistický	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nízkých	nízký	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
(	(	kIx(
<g/>
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
fyzice	fyzika	k1gFnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jmenovatel	jmenovatel	k1gInSc1
zlomku	zlomek	k1gInSc2
velmi	velmi	k6eAd1
přesně	přesně	k6eAd1
roven	roven	k2eAgMnSc1d1
1	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
relativistická	relativistický	k2eAgFnSc1d1
a	a	k8xC
klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
jsou	být	k5eAaImIp3nP
zaměnitelné	zaměnitelný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vysokých	vysoký	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
je	být	k5eAaImIp3nS
relativistická	relativistický	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
klidová	klidový	k2eAgFnSc1d1
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
rychlost	rychlost	k1gFnSc1
tělesa	těleso	k1gNnSc2
blíží	blížit	k5eAaImIp3nS
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
roste	růst	k5eAaImIp3nS
relativistická	relativistický	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
dokonce	dokonce	k9
nade	nad	k7c4
všechny	všechen	k3xTgFnPc4
meze	mez	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
se	se	k3xPyFc4
nemění	měnit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Již	již	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dosáhl	dosáhnout	k5eAaPmAgMnS
Loránd	Loránd	k1gMnSc1
Eötvös	Eötvös	k1gInSc4
při	při	k7c6
experimentu	experiment	k1gInSc6
s	s	k7c7
torzními	torzní	k2eAgFnPc7d1
vahami	váha	k1gFnPc7
přesnosti	přesnost	k1gFnSc2
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
např.	např.	kA
</s>
<s>
R.	R.	kA
v.	v.	k?
Eötvös	Eötvös	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
sborníku	sborník	k1gInSc6
Verhandlungen	Verhandlungen	k1gInSc1
der	drát	k5eAaImRp2nS
16	#num#	k4
Allgemeinen	Allgemeinen	k2eAgInSc4d1
Konferenz	Konferenz	k1gInSc4
der	drát	k5eAaImRp2nS
Internationalen	Internationalen	k2eAgMnSc1d1
Erdmessung	Erdmessung	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
Reiner	Reiner	k1gInSc1
<g/>
,	,	kIx,
Berlin	berlina	k1gFnPc2
<g/>
,	,	kIx,
319,191	319,191	k4
<g/>
0	#num#	k4
<g/>
↑	↑	k?
Profesor	profesor	k1gMnSc1
Matthew	Matthew	k1gMnSc1
Strassler	Strassler	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
26	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Neutron	neutron	k1gInSc1
Stability	stabilita	k1gFnSc2
in	in	k?
Atomic	Atomic	k1gMnSc1
Nuclei	Nucle	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
„	„	k?
<g/>
As	as	k1gNnSc1
is	is	k?
true	true	k1gNnPc2
for	forum	k1gNnPc2
all	all	k?
modern	modern	k1gMnSc1
particle	particle	k6eAd1
physicists	physicists	k6eAd1
<g/>
,	,	kIx,
by	by	kYmCp3nS
the	the	k?
word	word	k1gInSc1
"	"	kIx"
<g/>
mass	mass	k1gInSc1
<g/>
"	"	kIx"
I	i	k9
always	always	k6eAd1
mean	mean	k1gNnSc1
"	"	kIx"
<g/>
rest	rest	k6eAd1
mass	mass	k6eAd1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
all	all	k?
electrons	electrons	k1gInSc1
have	have	k1gFnSc1
the	the	k?
same	samat	k5eAaPmIp3nS
mass	mass	k1gInSc1
<g/>
,	,	kIx,
0.000511	0.000511	k4
GeV	GeV	k1gFnPc2
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
2	#num#	k4
<g/>
,	,	kIx,
no	no	k9
matter	matter	k1gInSc1
what	whata	k1gFnPc2
they	thea	k1gFnSc2
are	ar	k1gInSc5
doing	doinga	k1gFnPc2
or	or	k?
how	how	k?
fast	fast	k1gInSc4
they	thea	k1gFnSc2
<g/>
'	'	kIx"
<g/>
re	re	k9
moving	moving	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Řádová	řádový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
(	(	kIx(
<g/>
hmotnost	hmotnost	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Váha	váha	k1gFnSc1
</s>
<s>
Tíha	tíha	k1gFnSc1
</s>
<s>
Redukovaná	redukovaný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
molekulová	molekulový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
hmotnost	hmotnost	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
hmotnost	hmotnost	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Popis	popis	k1gInSc1
a	a	k8xC
převody	převod	k1gInPc1
jednotek	jednotka	k1gFnPc2
hmotnosti	hmotnost	k1gFnSc2
<g/>
:	:	kIx,
http://www.converter.cz/prevody/hmotnost.htm	http://www.converter.cz/prevody/hmotnost.htm	k6eAd1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4169025-4	4169025-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
2916	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85081853	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85081853	#num#	k4
</s>
