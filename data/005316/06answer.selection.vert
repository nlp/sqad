<s>
Evropský	evropský	k2eAgInSc1d1	evropský
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
(	(	kIx(	(
<g/>
ESLP	ESLP	kA	ESLP
<g/>
;	;	kIx,	;
anglicky	anglicky	k6eAd1	anglicky
European	European	k1gInSc1	European
Court	Court	k1gInSc1	Court
of	of	k?	of
Human	Human	k1gInSc1	Human
Rights	Rights	k1gInSc1	Rights
<g/>
)	)	kIx)	)
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
k	k	k7c3	k
projednávání	projednávání	k1gNnSc3	projednávání
porušení	porušení	k1gNnSc2	porušení
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
