<s>
Evropský	evropský	k2eAgInSc1d1	evropský
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
(	(	kIx(	(
<g/>
ESLP	ESLP	kA	ESLP
<g/>
;	;	kIx,	;
anglicky	anglicky	k6eAd1	anglicky
European	European	k1gInSc1	European
Court	Court	k1gInSc1	Court
of	of	k?	of
Human	Human	k1gInSc1	Human
Rights	Rights	k1gInSc1	Rights
<g/>
)	)	kIx)	)
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
k	k	k7c3	k
projednávání	projednávání	k1gNnSc3	projednávání
porušení	porušení	k1gNnSc2	porušení
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původního	původní	k2eAgNnSc2d1	původní
znění	znění	k1gNnSc2	znění
Úmluvy	úmluva	k1gFnSc2	úmluva
se	se	k3xPyFc4	se
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
mechanismus	mechanismus	k1gInSc1	mechanismus
kromě	kromě	k7c2	kromě
Soudu	soud	k1gInSc2	soud
skládal	skládat	k5eAaImAgMnS	skládat
ještě	ještě	k6eAd1	ještě
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
projednávala	projednávat	k5eAaImAgNnP	projednávat
příslušnost	příslušnost	k1gFnSc4	příslušnost
stížností	stížnost	k1gFnPc2	stížnost
zaslaných	zaslaný	k2eAgFnPc2d1	zaslaná
jednotlivci	jednotlivec	k1gMnPc7	jednotlivec
<g/>
,	,	kIx,	,
zjišťovala	zjišťovat	k5eAaImAgFnS	zjišťovat
skutečnosti	skutečnost	k1gFnSc2	skutečnost
a	a	k8xC	a
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
smírné	smírný	k2eAgNnSc4d1	smírné
řešení	řešení	k1gNnSc4	řešení
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
pak	pak	k6eAd1	pak
projednával	projednávat	k5eAaImAgInS	projednávat
ty	ten	k3xDgInPc4	ten
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gNnSc3	on
předložila	předložit	k5eAaPmAgFnS	předložit
Komise	komise	k1gFnSc1	komise
nebo	nebo	k8xC	nebo
některý	některý	k3yIgMnSc1	některý
ze	z	k7c2	z
smluvních	smluvní	k2eAgMnPc2d1	smluvní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nepružnost	Nepružnost	k1gFnSc1	Nepružnost
a	a	k8xC	a
pomalost	pomalost	k1gFnSc1	pomalost
systému	systém	k1gInSc2	systém
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
změnu	změna	k1gFnSc4	změna
uskutečněnou	uskutečněný	k2eAgFnSc4d1	uskutečněná
Protokolem	protokol	k1gInSc7	protokol
č.	č.	k?	č.
11	[number]	k4	11
k	k	k7c3	k
Úmluvě	úmluva	k1gFnSc3	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
funguje	fungovat	k5eAaImIp3nS	fungovat
místo	místo	k1gNnSc4	místo
Komise	komise	k1gFnSc2	komise
a	a	k8xC	a
Soudu	soud	k1gInSc2	soud
jen	jen	k9	jen
jediný	jediný	k2eAgInSc1d1	jediný
orgán	orgán	k1gInSc1	orgán
–	–	k?	–
stálý	stálý	k2eAgInSc1d1	stálý
Soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Výboru	výbor	k1gInSc3	výbor
ministrů	ministr	k1gMnPc2	ministr
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
byla	být	k5eAaImAgFnS	být
také	také	k9	také
omezena	omezit	k5eAaPmNgFnS	omezit
pravomoc	pravomoc	k1gFnSc1	pravomoc
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
dohlížení	dohlížení	k1gNnSc6	dohlížení
nad	nad	k7c7	nad
rozsudky	rozsudek	k1gInPc7	rozsudek
Soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
soudců	soudce	k1gMnPc2	soudce
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
soudních	soudní	k2eAgInPc2d1	soudní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
každý	každý	k3xTgInSc4	každý
smluvní	smluvní	k2eAgInSc4d1	smluvní
stát	stát	k1gInSc4	stát
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
jedním	jeden	k4xCgMnSc7	jeden
soudcem	soudce	k1gMnSc7	soudce
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
přes	přes	k7c4	přes
40	[number]	k4	40
soudců	soudce	k1gMnPc2	soudce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
však	však	k9	však
nutně	nutně	k6eAd1	nutně
u	u	k7c2	u
Soudu	soud	k1gInSc2	soud
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
svého	svůj	k3xOyFgMnSc4	svůj
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
omezen	omezen	k2eAgInSc1d1	omezen
počet	počet	k1gInSc1	počet
soudců	soudce	k1gMnPc2	soudce
stejné	stejný	k2eAgFnSc2d1	stejná
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
smluvní	smluvní	k2eAgFnSc1d1	smluvní
strana	strana	k1gFnSc1	strana
může	moct	k5eAaImIp3nS	moct
navrhnout	navrhnout	k5eAaPmF	navrhnout
až	až	k9	až
tři	tři	k4xCgMnPc4	tři
kandidáty	kandidát	k1gMnPc4	kandidát
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
cizince	cizinec	k1gMnPc4	cizinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgMnPc2	který
jsou	být	k5eAaImIp3nP	být
soudci	soudce	k1gMnPc1	soudce
voleni	volit	k5eAaImNgMnP	volit
Parlamentním	parlamentní	k2eAgNnSc7d1	parlamentní
shromážděním	shromáždění	k1gNnSc7	shromáždění
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
(	(	kIx(	(
<g/>
Článek	článek	k1gInSc4	článek
23	[number]	k4	23
odstavec	odstavec	k1gInSc1	odstavec
1	[number]	k4	1
Úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věkový	věkový	k2eAgInSc1d1	věkový
limit	limit	k1gInSc1	limit
pro	pro	k7c4	pro
soudce	soudce	k1gMnSc4	soudce
je	být	k5eAaImIp3nS	být
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
je	být	k5eAaImIp3nS	být
soudcem	soudce	k1gMnSc7	soudce
Aleš	Aleš	k1gMnSc1	Aleš
Pejchal	Pejchal	k1gMnSc1	Pejchal
<g/>
.	.	kIx.	.
</s>
<s>
Stížnost	stížnost	k1gFnSc4	stížnost
na	na	k7c4	na
porušení	porušení	k1gNnSc4	porušení
Úmluvy	úmluva	k1gFnSc2	úmluva
může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
Soudu	soud	k1gInSc3	soud
podat	podat	k5eAaPmF	podat
buď	buď	k8xC	buď
některý	některý	k3yIgInSc1	některý
smluvní	smluvní	k2eAgInSc1d1	smluvní
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
mezistátní	mezistátní	k2eAgFnSc1d1	mezistátní
stížnost	stížnost	k1gFnSc1	stížnost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnPc1	skupina
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
či	či	k8xC	či
nevládní	vládní	k2eNgFnSc2d1	nevládní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
individuální	individuální	k2eAgFnSc1d1	individuální
stížnost	stížnost	k1gFnSc1	stížnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1	individuální
stížnost	stížnost	k1gFnSc1	stížnost
je	být	k5eAaImIp3nS	být
přípustná	přípustný	k2eAgFnSc1d1	přípustná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
základního	základní	k2eAgNnSc2d1	základní
práva	právo	k1gNnSc2	právo
chráněného	chráněný	k2eAgNnSc2d1	chráněné
Úmluvou	úmluva	k1gFnSc7	úmluva
došlo	dojít	k5eAaPmAgNnS	dojít
státní	státní	k2eAgFnSc7d1	státní
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
přijatelnosti	přijatelnost	k1gFnSc6	přijatelnost
individuálních	individuální	k2eAgFnPc2d1	individuální
stížností	stížnost	k1gFnPc2	stížnost
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
tříčlenný	tříčlenný	k2eAgInSc1d1	tříčlenný
Výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
jednomyslně	jednomyslně	k6eAd1	jednomyslně
odmítnout	odmítnout	k5eAaPmF	odmítnout
nebo	nebo	k8xC	nebo
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Věc	věc	k1gFnSc1	věc
pak	pak	k6eAd1	pak
postupuje	postupovat	k5eAaImIp3nS	postupovat
sedmičlennému	sedmičlenný	k2eAgInSc3d1	sedmičlenný
Senátu	senát	k1gInSc3	senát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
meritu	meritum	k1gNnSc6	meritum
<g/>
.	.	kIx.	.
</s>
<s>
Sedmičlenný	sedmičlenný	k2eAgInSc1d1	sedmičlenný
Senát	senát	k1gInSc1	senát
také	také	k9	také
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
přijatelnosti	přijatelnost	k1gFnSc6	přijatelnost
a	a	k8xC	a
meritu	meritum	k1gNnSc6	meritum
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
stížností	stížnost	k1gFnPc2	stížnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mimořádně	mimořádně	k6eAd1	mimořádně
závažnou	závažný	k2eAgFnSc4d1	závažná
kauzu	kauza	k1gFnSc4	kauza
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
Senát	senát	k1gInSc1	senát
postoupit	postoupit	k5eAaPmF	postoupit
věc	věc	k1gFnSc4	věc
Velkému	velký	k2eAgInSc3d1	velký
senátu	senát	k1gInSc3	senát
složenému	složený	k2eAgInSc3d1	složený
ze	z	k7c2	z
sedmnácti	sedmnáct	k4xCc2	sedmnáct
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Rozsudek	rozsudek	k1gInSc1	rozsudek
Velkého	velký	k2eAgInSc2d1	velký
senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
konečný	konečný	k2eAgInSc1d1	konečný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
sedmičlenným	sedmičlenný	k2eAgInSc7d1	sedmičlenný
Senátem	senát	k1gInSc7	senát
můžou	můžou	k?	můžou
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
sporné	sporný	k2eAgFnSc2d1	sporná
strany	strana	k1gFnSc2	strana
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
ještě	ještě	k6eAd1	ještě
podat	podat	k5eAaPmF	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
projednání	projednání	k1gNnSc4	projednání
před	před	k7c7	před
Velkým	velký	k2eAgInSc7d1	velký
senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
přijatelnosti	přijatelnost	k1gFnSc2	přijatelnost
individuální	individuální	k2eAgFnSc7d1	individuální
stížností	stížnost	k1gFnSc7	stížnost
<g/>
:	:	kIx,	:
Musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vyčerpány	vyčerpat	k5eAaPmNgInP	vyčerpat
všechny	všechen	k3xTgInPc1	všechen
vnitrostátní	vnitrostátní	k2eAgInPc1d1	vnitrostátní
opravné	opravný	k2eAgInPc1d1	opravný
prostředky	prostředek	k1gInPc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
zamítnutí	zamítnutí	k1gNnSc3	zamítnutí
stížnosti	stížnost	k1gFnSc2	stížnost
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Stížnost	stížnost	k1gFnSc1	stížnost
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
podána	podat	k5eAaPmNgFnS	podat
do	do	k7c2	do
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
konečného	konečný	k2eAgNnSc2d1	konečné
vnitrostátního	vnitrostátní	k2eAgNnSc2d1	vnitrostátní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
jít	jít	k5eAaImF	jít
o	o	k7c4	o
anonymní	anonymní	k2eAgFnSc4d1	anonymní
stížnost	stížnost	k1gFnSc4	stížnost
<g/>
.	.	kIx.	.
</s>
<s>
Překážka	překážka	k1gFnSc1	překážka
rei	rei	k?	rei
iudicatae	iudicatae	k1gFnSc1	iudicatae
–	–	k?	–
musí	muset	k5eAaImIp3nS	muset
jít	jít	k5eAaImF	jít
o	o	k7c4	o
věc	věc	k1gFnSc4	věc
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
už	už	k6eAd1	už
Soudem	soud	k1gInSc7	soud
projednána	projednán	k2eAgFnSc1d1	projednána
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nebyla	být	k5eNaImAgFnS	být
předložena	předložit	k5eAaPmNgFnS	předložit
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
jinému	jiný	k2eAgInSc3d1	jiný
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
orgánu	orgán	k1gInSc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Nepřípustné	přípustný	k2eNgInPc1d1	nepřípustný
jsou	být	k5eAaImIp3nP	být
stížnosti	stížnost	k1gFnPc1	stížnost
zjevně	zjevně	k6eAd1	zjevně
nepodložené	podložený	k2eNgFnPc1d1	nepodložená
<g/>
,	,	kIx,	,
neslučitelné	slučitelný	k2eNgFnPc1d1	neslučitelná
s	s	k7c7	s
ustanovením	ustanovení	k1gNnSc7	ustanovení
Úmluvy	úmluva	k1gFnSc2	úmluva
nebo	nebo	k8xC	nebo
případně	případně	k6eAd1	případně
i	i	k9	i
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zneužívají	zneužívat	k5eAaImIp3nP	zneužívat
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
stížnost	stížnost	k1gFnSc4	stížnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
projednávání	projednávání	k1gNnSc2	projednávání
stížností	stížnost	k1gFnPc2	stížnost
podává	podávat	k5eAaImIp3nS	podávat
Soud	soud	k1gInSc1	soud
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnSc2	žádost
Výboru	výbor	k1gInSc2	výbor
ministrů	ministr	k1gMnPc2	ministr
také	také	k9	také
posudky	posudek	k1gInPc4	posudek
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
výkladu	výklad	k1gInSc3	výklad
Úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evropský	evropský	k2eAgInSc4d1	evropský
soud	soud	k1gInSc4	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Evropský	evropský	k2eAgInSc1d1	evropský
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
–	–	k?	–
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
obracet	obracet	k5eAaImF	obracet
na	na	k7c4	na
Evropský	evropský	k2eAgInSc4d1	evropský
soud	soud	k1gInSc4	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
–	–	k?	–
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
českého	český	k2eAgInSc2d1	český
veřejného	veřejný	k2eAgInSc2d1	veřejný
ochránce	ochránce	k1gMnSc1	ochránce
práv	práv	k2eAgMnSc1d1	práv
(	(	kIx(	(
<g/>
ombudsmana	ombudsman	k1gMnSc4	ombudsman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Evropské	evropský	k2eAgFnSc2d1	Evropská
úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
–	–	k?	–
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Evropského	evropský	k2eAgInSc2d1	evropský
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
Interview	interview	k1gNnSc2	interview
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgNnSc6	který
Luzius	Luzius	k1gMnSc1	Luzius
Wildhaber	Wildhaber	k1gMnSc1	Wildhaber
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
předseda	předseda	k1gMnSc1	předseda
Soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gFnSc4	jeho
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
úkoly	úkol	k1gInPc4	úkol
<g/>
:	:	kIx,	:
Wildhaber	Wildhaber	k1gMnSc1	Wildhaber
steps	stepsa	k1gFnPc2	stepsa
down	down	k1gMnSc1	down
from	from	k1gMnSc1	from
human	human	k1gMnSc1	human
rights	rightsa	k1gFnPc2	rightsa
court	court	k1gInSc4	court
–	–	k?	–
Swiss	Swiss	k1gInSc1	Swiss
judge	judgat	k5eAaPmIp3nS	judgat
Luzius	Luzius	k1gMnSc1	Luzius
Wildhaber	Wildhaber	k1gMnSc1	Wildhaber
has	hasit	k5eAaImRp2nS	hasit
a	a	k8xC	a
mountain	mountain	k2eAgInSc1d1	mountain
of	of	k?	of
paperwork	paperwork	k1gInSc1	paperwork
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
before	befor	k1gInSc5	befor
he	he	k0	he
retires	retires	k1gMnSc1	retires
as	as	k1gNnSc2	as
president	president	k1gMnSc1	president
of	of	k?	of
the	the	k?	the
European	European	k1gInSc1	European
Court	Court	k1gInSc1	Court
of	of	k?	of
Human	Human	k1gInSc1	Human
Rights	Rights	k1gInSc1	Rights
<g/>
,	,	kIx,	,
Swissinfo	Swissinfo	k1gMnSc1	Swissinfo
/	/	kIx~	/
NZZ	NZZ	kA	NZZ
<g/>
,	,	kIx,	,
26.12	[number]	k4	26.12
<g/>
.2006	.2006	k4	.2006
</s>
