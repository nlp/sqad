<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Frieden	Frieden	k2eAgMnSc1d1	Frieden
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1892	[number]	k4	1892
Mertert	Mertert	k1gInSc1	Mertert
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1959	[number]	k4	1959
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
byl	být	k5eAaImAgMnS	být
premiérem	premiér	k1gMnSc7	premiér
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
po	po	k7c6	po
osmi	osm	k4xCc6	osm
měsících	měsíc	k1gInPc6	měsíc
mandátu	mandát	k1gInSc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
též	též	k9	též
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Němci	Němec	k1gMnSc3	Němec
vězněn	věznit	k5eAaImNgMnS	věznit
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Hinzertu	Hinzert	k1gInSc6	Hinzert
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
esejů	esej	k1gInPc2	esej
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
i	i	k8xC	i
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
ústřední	ústřední	k2eAgFnSc4d1	ústřední
koncepci	koncepce	k1gFnSc4	koncepce
katolického	katolický	k2eAgInSc2d1	katolický
humanismu	humanismus	k1gInSc2	humanismus
(	(	kIx(	(
<g/>
De	De	k?	De
la	la	k1gNnSc4	la
primauté	primautý	k2eAgFnSc2d1	primautý
du	du	k?	du
spirituel	spirituel	k1gMnSc1	spirituel
<g/>
,	,	kIx,	,
Variations	Variations	k1gInSc1	Variations
sur	sur	k?	sur
le	le	k?	le
thè	thè	k?	thè
humaniste	humanist	k1gMnSc5	humanist
et	et	k?	et
européen	européen	k1gInSc1	européen
<g/>
,	,	kIx,	,
Vertus	Vertus	k1gInSc1	Vertus
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
humanisme	humanismus	k1gInSc5	humanismus
chrétien	chrétien	k2eAgInSc4d1	chrétien
<g/>
,	,	kIx,	,
Meditationnen	Meditationnen	k2eAgInSc4d1	Meditationnen
um	um	k1gInSc4	um
den	den	k1gInSc1	den
Menschen	Menschen	k1gInSc1	Menschen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
představitelem	představitel	k1gMnSc7	představitel
křesťansko-demokratické	křesťanskoemokratický	k2eAgFnSc2d1	křesťansko-demokratická
Chrëschtlech	Chrëschtl	k1gInPc6	Chrëschtl
Sozial	Sozial	k1gInSc1	Sozial
Vollekspartei	Volleksparte	k1gInSc6	Volleksparte
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pierre	Pierr	k1gInSc5	Pierr
Frieden	Frieden	k2eAgMnSc1d1	Frieden
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
