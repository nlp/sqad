<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Alexejevič	Alexejevič	k1gInSc4	Alexejevič
Gagarin	Gagarin	k1gInSc4	Gagarin
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ю	Ю	k?	Ю
А	А	k?	А
Г	Г	k?	Г
<g/>
;	;	kIx,	;
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1934	[number]	k4	1934
Klušino	Klušin	k2eAgNnSc4d1	Klušino
<g/>
,	,	kIx,	,
Západní	západní	k2eAgMnPc1d1	západní
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Smolenská	Smolenský	k2eAgFnSc1d1	Smolenská
<g/>
)	)	kIx)	)
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
RSFSR	RSFSR	kA	RSFSR
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Novoselovo	Novoselův	k2eAgNnSc1d1	Novoselovo
<g/>
,	,	kIx,	,
Vladimirská	Vladimirský	k2eAgFnSc1d1	Vladimirská
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
RSFSR	RSFSR	kA	RSFSR
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
