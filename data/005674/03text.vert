<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Alexejevič	Alexejevič	k1gInSc4	Alexejevič
Gagarin	Gagarin	k1gInSc4	Gagarin
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ю	Ю	k?	Ю
А	А	k?	А
Г	Г	k?	Г
<g/>
;	;	kIx,	;
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1934	[number]	k4	1934
Klušino	Klušin	k2eAgNnSc4d1	Klušino
<g/>
,	,	kIx,	,
Západní	západní	k2eAgMnPc1d1	západní
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Smolenská	Smolenský	k2eAgFnSc1d1	Smolenská
<g/>
)	)	kIx)	)
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
RSFSR	RSFSR	kA	RSFSR
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Novoselovo	Novoselův	k2eAgNnSc1d1	Novoselovo
<g/>
,	,	kIx,	,
Vladimirská	Vladimirský	k2eAgFnSc1d1	Vladimirská
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
RSFSR	RSFSR	kA	RSFSR
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
kosmickému	kosmický	k2eAgInSc3d1	kosmický
letu	let	k1gInSc3	let
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
Vostok	Vostok	k1gInSc1	Vostok
1	[number]	k4	1
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
<g/>
.	.	kIx.	.
</s>
<s>
Obletěl	obletět	k5eAaPmAgMnS	obletět
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
po	po	k7c6	po
108	[number]	k4	108
minutách	minuta	k1gFnPc6	minuta
přistál	přistát	k5eAaPmAgMnS	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
letu	let	k1gInSc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hrdinou	hrdina	k1gMnSc7	hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
oslavovanou	oslavovaný	k2eAgFnSc7d1	oslavovaná
světovou	světový	k2eAgFnSc7d1	světová
celebritou	celebrita	k1gFnSc7	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
i	i	k8xC	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zátěž	zátěž	k1gFnSc4	zátěž
svých	svůj	k3xOyFgFnPc2	svůj
veřejných	veřejný	k2eAgFnPc2d1	veřejná
povinností	povinnost	k1gFnPc2	povinnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Žukovského	Žukovského	k2eAgFnSc6d1	Žukovského
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
oddílu	oddíl	k1gInSc2	oddíl
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
kosmickému	kosmický	k2eAgInSc3d1	kosmický
letu	let	k1gInSc3	let
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
náhradníkem	náhradník	k1gMnSc7	náhradník
Vladimira	Vladimiro	k1gNnSc2	Vladimiro
Komarova	Komarův	k2eAgNnSc2d1	Komarův
pro	pro	k7c4	pro
let	let	k1gInSc4	let
Sojuzu	Sojuz	k1gInSc2	Sojuz
1	[number]	k4	1
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1968	[number]	k4	1968
dokončil	dokončit	k5eAaPmAgInS	dokončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
létání	létání	k1gNnSc3	létání
v	v	k7c6	v
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
při	při	k7c6	při
cvičném	cvičný	k2eAgInSc6d1	cvičný
letu	let	k1gInSc6	let
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Gagarin	Gagarin	k1gInSc1	Gagarin
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
prosté	prostý	k2eAgFnSc2d1	prostá
venkovské	venkovský	k2eAgFnSc2d1	venkovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Klušino	Klušin	k2eAgNnSc1d1	Klušino
ve	v	k7c6	v
Smolenské	Smolenský	k2eAgFnSc6d1	Smolenská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
kolchozu	kolchoz	k1gInSc6	kolchoz
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Alexej	Alexej	k1gMnSc1	Alexej
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Gagarin	Gagarin	k1gInSc4	Gagarin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
tesař	tesař	k1gMnSc1	tesař
a	a	k8xC	a
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Timofejevna	Timofejevna	k1gFnSc1	Timofejevna
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dojičkou	dojička	k1gFnSc7	dojička
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
povýšila	povýšit	k5eAaPmAgFnS	povýšit
na	na	k7c4	na
vedoucí	vedoucí	k1gMnPc4	vedoucí
mléčné	mléčný	k2eAgFnSc2d1	mléčná
farmy	farma	k1gFnSc2	farma
kolchozu	kolchoz	k1gInSc2	kolchoz
<g/>
.	.	kIx.	.
</s>
<s>
Gagarinovi	Gagarin	k1gMnSc3	Gagarin
měli	mít	k5eAaImAgMnP	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
Valentina	Valentin	k1gMnSc4	Valentin
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc4d1	jediná
dceru	dcera	k1gFnSc4	dcera
Zoju	Zoja	k1gFnSc4	Zoja
<g/>
,	,	kIx,	,
Jurije	Jurije	k1gFnSc1	Jurije
a	a	k8xC	a
nejmladšího	mladý	k2eAgMnSc2d3	nejmladší
Borise	Boris	k1gMnSc2	Boris
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
silně	silně	k6eAd1	silně
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
1941	[number]	k4	1941
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
za	za	k7c7	za
vesnicí	vesnice	k1gFnSc7	vesnice
poškozený	poškozený	k1gMnSc1	poškozený
Jak-	Jak-	k1gFnSc2	Jak-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vesnické	vesnický	k2eAgFnPc4d1	vesnická
děti	dítě	k1gFnPc4	dítě
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
viděly	vidět	k5eAaImAgInP	vidět
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Malému	Malý	k1gMnSc3	Malý
Jurijovi	Jurij	k1gMnSc3	Jurij
pilot	pilot	k1gInSc4	pilot
dovolil	dovolit	k5eAaPmAgInS	dovolit
i	i	k8xC	i
sednout	sednout	k5eAaPmF	sednout
si	se	k3xPyFc3	se
do	do	k7c2	do
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1941	[number]	k4	1941
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
začal	začít	k5eAaPmAgMnS	začít
sedmiletý	sedmiletý	k2eAgMnSc1d1	sedmiletý
Jurij	Jurij	k1gMnSc1	Jurij
Gagarin	Gagarin	k1gInSc4	Gagarin
chodit	chodit	k5eAaImF	chodit
teprve	teprve	k6eAd1	teprve
před	před	k7c7	před
několika	několik	k4yIc7	několik
týdny	týden	k1gInPc7	týden
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
nezahynul	zahynout	k5eNaPmAgMnS	zahynout
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
na	na	k7c6	na
okupovaném	okupovaný	k2eAgNnSc6d1	okupované
území	území	k1gNnSc6	území
byl	být	k5eAaImAgInS	být
nelehký	lehký	k2eNgInSc1d1	nelehký
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc4	jejich
dům	dům	k1gInSc4	dům
zabrali	zabrat	k5eAaPmAgMnP	zabrat
němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
Gagarinovi	Gagarinův	k2eAgMnPc1d1	Gagarinův
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
museli	muset	k5eAaImAgMnP	muset
vykopat	vykopat	k5eAaPmF	vykopat
zemljanku	zemljanka	k1gFnSc4	zemljanka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
měli	mít	k5eAaImAgMnP	mít
kde	kde	k6eAd1	kde
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
školních	školní	k2eAgFnPc2d1	školní
lavic	lavice	k1gFnPc2	lavice
se	se	k3xPyFc4	se
Jurij	Jurij	k1gMnSc1	Jurij
mohl	moct	k5eAaImAgMnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
až	až	k9	až
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
rodina	rodina	k1gFnSc1	rodina
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
,	,	kIx,	,
ustupující	ustupující	k2eAgMnPc1d1	ustupující
Němci	Němec	k1gMnPc1	Němec
s	s	k7c7	s
sebou	se	k3xPyFc7	se
odvlekli	odvléct	k5eAaPmAgMnP	odvléct
jeho	jeho	k3xOp3gNnSc4	jeho
dva	dva	k4xCgInPc4	dva
starší	starý	k2eAgMnSc1d2	starší
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oba	dva	k4xCgMnPc1	dva
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
utekli	utéct	k5eAaPmAgMnP	utéct
a	a	k8xC	a
poté	poté	k6eAd1	poté
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
Rudé	rudý	k2eAgFnSc6d1	rudá
armádě	armáda	k1gFnSc6	armáda
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Otce	Otka	k1gFnSc3	Otka
zase	zase	k9	zase
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
odvedli	odvést	k5eAaPmAgMnP	odvést
do	do	k7c2	do
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
Gžatsku	Gžatsko	k1gNnSc6	Gžatsko
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
shledala	shledat	k5eAaPmAgFnS	shledat
až	až	k9	až
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Gagarinovi	Gagarinův	k2eAgMnPc1d1	Gagarinův
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Gžatska	Gžatsko	k1gNnSc2	Gžatsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
Jurij	Jurij	k1gMnSc1	Jurij
Gagarin	Gagarin	k1gInSc4	Gagarin
dokončil	dokončit	k5eAaPmAgMnS	dokončit
6	[number]	k4	6
<g/>
.	.	kIx.	.
třídu	třída	k1gFnSc4	třída
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vyučit	vyučit	k5eAaPmF	vyučit
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Přijali	přijmout	k5eAaPmAgMnP	přijmout
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
učiliště	učiliště	k1gNnSc2	učiliště
při	při	k7c6	při
závodu	závod	k1gInSc6	závod
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
Ljubercích	Ljuberk	k1gInPc6	Ljuberk
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
slévač	slévač	k1gInSc1	slévač
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ve	v	k7c6	v
večerní	večerní	k2eAgFnSc6d1	večerní
škole	škola	k1gFnSc6	škola
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
mládeže	mládež	k1gFnSc2	mládež
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
úplné	úplný	k2eAgNnSc4d1	úplné
střední	střední	k2eAgNnSc4d1	střední
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
učiliště	učiliště	k1gNnSc2	učiliště
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
učňů	učeň	k1gMnPc2	učeň
vybrán	vybrán	k2eAgInSc1d1	vybrán
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
Saratovské	Saratovský	k2eAgFnSc6d1	Saratovská
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
prospěchem	prospěch	k1gInSc7	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ljubercích	Ljuberk	k1gInPc6	Ljuberk
i	i	k8xC	i
v	v	k7c6	v
Saratově	Saratův	k2eAgMnSc6d1	Saratův
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
věnoval	věnovat	k5eAaImAgMnS	věnovat
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
zejména	zejména	k9	zejména
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
výšce	výška	k1gFnSc3	výška
–	–	k?	–
dorostl	dorůst	k5eAaPmAgInS	dorůst
pouze	pouze	k6eAd1	pouze
165	[number]	k4	165
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Saratově	Saratův	k2eAgFnSc6d1	Saratova
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
kapitánem	kapitán	k1gMnSc7	kapitán
basketbalového	basketbalový	k2eAgNnSc2d1	basketbalové
mužstva	mužstvo	k1gNnSc2	mužstvo
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
trubku	trubka	k1gFnSc4	trubka
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
práce	práce	k1gFnSc2	práce
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
kroužku	kroužek	k1gInSc2	kroužek
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
připravit	připravit	k5eAaPmF	připravit
přednášku	přednáška	k1gFnSc4	přednáška
o	o	k7c6	o
Konstantinu	Konstantin	k1gMnSc6	Konstantin
Ciolkovském	Ciolkovský	k2eAgMnSc6d1	Ciolkovský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
pracích	práce	k1gFnPc6	práce
o	o	k7c6	o
raketových	raketový	k2eAgInPc6d1	raketový
motorech	motor	k1gInPc6	motor
a	a	k8xC	a
kosmických	kosmický	k2eAgInPc6d1	kosmický
letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Ciolkovského	Ciolkovský	k2eAgInSc2d1	Ciolkovský
myšlenky	myšlenka	k1gFnPc4	myšlenka
zanechaly	zanechat	k5eAaPmAgFnP	zanechat
v	v	k7c6	v
Gagarinovi	Gagarin	k1gMnSc6	Gagarin
hluboký	hluboký	k2eAgInSc4d1	hluboký
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
studií	studio	k1gNnPc2	studio
v	v	k7c6	v
Saratově	Saratův	k2eAgNnSc6d1	Saratův
se	s	k7c7	s
spolužáky	spolužák	k1gMnPc7	spolužák
snil	snít	k5eAaImAgMnS	snít
o	o	k7c4	o
létání	létání	k1gNnSc4	létání
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
do	do	k7c2	do
pilotních	pilotní	k2eAgInPc2d1	pilotní
kursů	kurs	k1gInPc2	kurs
DOSAAF	DOSAAF	kA	DOSAAF
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
létání	létání	k1gNnSc3	létání
se	se	k3xPyFc4	se
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nedaleko	daleko	k6eNd1	daleko
Saratova	Saratův	k2eAgFnSc1d1	Saratova
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
škola	škola	k1gFnSc1	škola
civilního	civilní	k2eAgNnSc2d1	civilní
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
zkusili	zkusit	k5eAaPmAgMnP	zkusit
štěstí	štěstit	k5eAaImIp3nP	štěstit
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesplňovali	splňovat	k5eNaImAgMnP	splňovat
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
<g/>
.	.	kIx.	.
</s>
<s>
Uspěli	uspět	k5eAaPmAgMnP	uspět
až	až	k9	až
napotřetí	napotřetí	k6eAd1	napotřetí
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1954	[number]	k4	1954
je	on	k3xPp3gInPc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
v	v	k7c6	v
saratovském	saratovský	k2eAgInSc6d1	saratovský
aeroklubu	aeroklub	k1gInSc6	aeroklub
<g/>
,	,	kIx,	,
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
1955	[number]	k4	1955
už	už	k9	už
Gagarin	Gagarin	k1gInSc1	Gagarin
létal	létat	k5eAaImAgInS	létat
na	na	k7c4	na
Jaku-	Jaku-	k1gFnSc4	Jaku-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
dokončil	dokončit	k5eAaPmAgInS	dokončit
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dráhu	dráha	k1gFnSc4	dráha
stíhacího	stíhací	k2eAgMnSc2d1	stíhací
letce	letec	k1gMnSc2	letec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1955	[number]	k4	1955
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
leteckou	letecký	k2eAgFnSc4d1	letecká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Orenburgu	Orenburg	k1gInSc6	Orenburg
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
neobešlo	obešnout	k5eNaPmAgNnS	obešnout
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
měl	mít	k5eAaImAgInS	mít
Gagarin	Gagarin	k1gInSc1	Gagarin
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
přistáváním	přistávání	k1gNnSc7	přistávání
s	s	k7c7	s
cvičným	cvičný	k2eAgMnSc7d1	cvičný
MiGu-	MiGu-	k1gMnSc7	MiGu-
<g/>
15	[number]	k4	15
<g/>
UTI	UTI	kA	UTI
<g/>
,	,	kIx,	,
hrozilo	hrozit	k5eAaImAgNnS	hrozit
mu	on	k3xPp3gMnSc3	on
dokonce	dokonce	k9	dokonce
vyloučení	vyloučení	k1gNnSc4	vyloučení
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
mezi	mezi	k7c7	mezi
spolustudenty	spolustudent	k1gMnPc7	spolustudent
mu	on	k3xPp3gMnSc3	on
nepřineslo	přinést	k5eNaPmAgNnS	přinést
jeho	jeho	k3xOp3gNnSc4	jeho
nekompromisní	kompromisní	k2eNgNnSc1d1	nekompromisní
vyžadování	vyžadování	k1gNnSc1	vyžadování
disciplíny	disciplína	k1gFnSc2	disciplína
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
pomocníka	pomocník	k1gMnSc2	pomocník
velitele	velitel	k1gMnSc2	velitel
čety	četa	k1gFnSc2	četa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
výuky	výuka	k1gFnSc2	výuka
leteckou	letecký	k2eAgFnSc4d1	letecká
školu	škola	k1gFnSc4	škola
ukončil	ukončit	k5eAaPmAgInS	ukončit
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
absolvent	absolvent	k1gMnSc1	absolvent
s	s	k7c7	s
vynikajícím	vynikající	k2eAgInSc7d1	vynikající
prospěchem	prospěch	k1gInSc7	prospěch
měl	mít	k5eAaImAgInS	mít
právo	právo	k1gNnSc4	právo
vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
místo	místo	k7c2	místo
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
život	život	k1gInSc4	život
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgInS	sloužit
u	u	k7c2	u
769	[number]	k4	769
<g/>
.	.	kIx.	.
stíhacího	stíhací	k2eAgInSc2d1	stíhací
leteckého	letecký	k2eAgInSc2d1	letecký
pluku	pluk	k1gInSc2	pluk
122	[number]	k4	122
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc2d1	stíhací
letecké	letecký	k2eAgFnSc2d1	letecká
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
769	[number]	k4	769
<g/>
-й	-й	k?	-й
и	и	k?	и
а	а	k?	а
п	п	k?	п
(	(	kIx(	(
<g/>
И	И	k?	И
<g/>
)	)	kIx)	)
122	[number]	k4	122
<g/>
-й	-й	k?	-й
и	и	k?	и
а	а	k?	а
д	д	k?	д
<g/>
)	)	kIx)	)
Severního	severní	k2eAgNnSc2d1	severní
loďstva	loďstvo	k1gNnSc2	loďstvo
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Luostari-Novoje	Luostari-Novoj	k1gInSc2	Luostari-Novoj
u	u	k7c2	u
Murmanska	Murmansk	k1gInSc2	Murmansk
za	za	k7c7	za
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pluku	pluk	k1gInSc3	pluk
nováčci	nováček	k1gMnPc1	nováček
přijeli	přijet	k5eAaPmAgMnP	přijet
koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
důkladné	důkladný	k2eAgFnSc6d1	důkladná
teoretické	teoretický	k2eAgFnSc6d1	teoretická
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
složení	složení	k1gNnSc6	složení
zkoušek	zkouška	k1gFnPc2	zkouška
ze	z	k7c2	z
specifik	specifikon	k1gNnPc2	specifikon
létání	létání	k1gNnSc2	létání
na	na	k7c6	na
severu	sever	k1gInSc6	sever
začali	začít	k5eAaPmAgMnP	začít
od	od	k7c2	od
března	březen	k1gInSc2	březen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
létat	létat	k5eAaImF	létat
na	na	k7c6	na
strojích	stroj	k1gInPc6	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
15	[number]	k4	15
<g/>
bis	bis	k?	bis
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
útvaru	útvar	k1gInSc2	útvar
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
náletu	nálet	k1gInSc2	nálet
265	[number]	k4	265
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
v	v	k7c6	v
letadlech	letadlo	k1gNnPc6	letadlo
nalétal	nalétat	k5eAaBmAgMnS	nalétat
265	[number]	k4	265
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
počítáno	počítán	k2eAgNnSc1d1	počítáno
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
letecké	letecký	k2eAgFnSc2d1	letecká
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
aeroklubu	aeroklub	k1gInSc6	aeroklub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
studií	studie	k1gFnPc2	studie
v	v	k7c6	v
Orenburgu	Orenburg	k1gInSc6	Orenburg
se	se	k3xPyFc4	se
po	po	k7c6	po
jedenapůlroční	jedenapůlroční	k2eAgFnSc6d1	jedenapůlroční
známosti	známost	k1gFnSc6	známost
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
oženil	oženit	k5eAaPmAgInS	oženit
se	s	k7c7	s
zdravotnicí	zdravotnice	k1gFnSc7	zdravotnice
Valentinou	Valentina	k1gFnSc7	Valentina
Ivanovnou	Ivanovna	k1gFnSc7	Ivanovna
Gorjačevovou	Gorjačevová	k1gFnSc7	Gorjačevová
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
na	na	k7c6	na
tanečním	taneční	k2eAgInSc6d1	taneční
večeru	večer	k1gInSc6	večer
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
škole	škola	k1gFnSc6	škola
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Jelena	Jelena	k1gFnSc1	Jelena
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
Galja	Galja	k1gFnSc1	Galja
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgInPc2d1	pouhý
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
před	před	k7c7	před
Gagarinovým	Gagarinův	k2eAgInSc7d1	Gagarinův
kosmickým	kosmický	k2eAgInSc7d1	kosmický
letem	let	k1gInSc7	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1959	[number]	k4	1959
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vláda	vláda	k1gFnSc1	vláda
o	o	k7c6	o
výběru	výběr	k1gInSc6	výběr
budoucích	budoucí	k2eAgMnPc2d1	budoucí
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pověřila	pověřit	k5eAaPmAgFnS	pověřit
velitelství	velitelství	k1gNnSc4	velitelství
letectva	letectvo	k1gNnSc2	letectvo
najít	najít	k5eAaPmF	najít
adepty	adept	k1gMnPc4	adept
mezi	mezi	k7c7	mezi
řadovými	řadový	k2eAgInPc7d1	řadový
piloty	pilot	k1gInPc7	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc4	výběr
prováděla	provádět	k5eAaImAgFnS	provádět
speciální	speciální	k2eAgFnSc1d1	speciální
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
Institutu	institut	k1gInSc6	institut
letecké	letecký	k2eAgFnSc2d1	letecká
medicíny	medicína	k1gFnSc2	medicína
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
plukovníkem	plukovník	k1gMnSc7	plukovník
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
п	п	k?	п
м	м	k?	м
с	с	k?	с
<g/>
)	)	kIx)	)
Jevgenijem	Jevgenij	k1gMnSc7	Jevgenij
Karpovem	Karpov	k1gInSc7	Karpov
<g/>
.	.	kIx.	.
</s>
<s>
Konečný	konečný	k2eAgInSc1d1	konečný
verdikt	verdikt	k1gInSc1	verdikt
v	v	k7c6	v
závěrečných	závěrečný	k2eAgFnPc6d1	závěrečná
fázích	fáze	k1gFnPc6	fáze
náboru	nábor	k1gInSc2	nábor
vynášela	vynášet	k5eAaImAgFnS	vynášet
lékařská	lékařský	k2eAgFnSc1d1	lékařská
komise	komise	k1gFnSc1	komise
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Babijčukem	Babijčuk	k1gMnSc7	Babijčuk
<g/>
.	.	kIx.	.
</s>
<s>
Kritéria	kritérion	k1gNnPc1	kritérion
byla	být	k5eAaImAgNnP	být
<g/>
:	:	kIx,	:
věk	věk	k1gInSc4	věk
do	do	k7c2	do
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
do	do	k7c2	do
175	[number]	k4	175
cm	cm	kA	cm
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
do	do	k7c2	do
75	[number]	k4	75
kg	kg	kA	kg
<g/>
,	,	kIx,	,
výborné	výborný	k2eAgNnSc1d1	výborné
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
výběr	výběr	k1gInSc1	výběr
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1959	[number]	k4	1959
prohlídkou	prohlídka	k1gFnSc7	prohlídka
lékařských	lékařský	k2eAgInPc2d1	lékařský
záznamů	záznam	k1gInPc2	záznam
3461	[number]	k4	3461
letců	letec	k1gMnPc2	letec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
206	[number]	k4	206
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
až	až	k8xS	až
prosinci	prosinec	k1gInSc6	prosinec
1959	[number]	k4	1959
procházeli	procházet	k5eAaImAgMnP	procházet
prohlídkami	prohlídka	k1gFnPc7	prohlídka
v	v	k7c6	v
Ústřední	ústřední	k2eAgFnSc6d1	ústřední
nemocnici	nemocnice	k1gFnSc6	nemocnice
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
povoláním	povolání	k1gNnSc7	povolání
k	k	k7c3	k
prohlídce	prohlídka	k1gFnSc3	prohlídka
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
úspěchů	úspěch	k1gInPc2	úspěch
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
Gagarin	Gagarin	k1gInSc1	Gagarin
podal	podat	k5eAaPmAgInS	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
oddílu	oddíl	k1gInSc2	oddíl
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
takový	takový	k3xDgInSc4	takový
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
první	první	k4xOgFnSc7	první
i	i	k8xC	i
druhou	druhý	k4xOgFnSc7	druhý
(	(	kIx(	(
<g/>
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
vlnou	vlna	k1gFnSc7	vlna
prohlídek	prohlídka	k1gFnPc2	prohlídka
a	a	k8xC	a
pohovorů	pohovor	k1gInPc2	pohovor
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
20	[number]	k4	20
budoucích	budoucí	k2eAgMnPc2d1	budoucí
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
1960	[number]	k4	1960
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
procházel	procházet	k5eAaImAgInS	procházet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
kolegy	kolega	k1gMnPc7	kolega
náročným	náročný	k2eAgInSc7d1	náročný
výcvikem	výcvik	k1gInSc7	výcvik
v	v	k7c6	v
nově	nově	k6eAd1	nově
zorganizovaném	zorganizovaný	k2eAgNnSc6d1	zorganizované
Středisku	středisko	k1gNnSc6	středisko
přípravy	příprava	k1gFnSc2	příprava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
(	(	kIx(	(
<g/>
CPK	CPK	kA	CPK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgInS	stanout
Karpov	Karpov	k1gInSc1	Karpov
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
generál	generál	k1gMnSc1	generál
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Kamanin	Kamanin	k2eAgMnSc1d1	Kamanin
<g/>
.	.	kIx.	.
</s>
<s>
Středisko	středisko	k1gNnSc1	středisko
bylo	být	k5eAaImAgNnS	být
zprvu	zprvu	k6eAd1	zprvu
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c6	v
spartánských	spartánský	k2eAgFnPc6d1	spartánská
podmínkách	podmínka	k1gFnPc6	podmínka
na	na	k7c6	na
moskevském	moskevský	k2eAgInSc6d1	moskevský
Frunzeho	Frunzeha	k1gFnSc5	Frunzeha
letišti	letiště	k1gNnSc6	letiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1960	[number]	k4	1960
přesídlilo	přesídlit	k5eAaPmAgNnS	přesídlit
do	do	k7c2	do
nově	nově	k6eAd1	nově
budovaného	budovaný	k2eAgNnSc2d1	budované
Hvězdného	hvězdný	k2eAgNnSc2d1	Hvězdné
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výcviku	výcvik	k1gInSc2	výcvik
Gagarin	Gagarin	k1gInSc1	Gagarin
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
středisko	středisko	k1gNnSc1	středisko
nedisponovalo	disponovat	k5eNaBmAgNnS	disponovat
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
výcvik	výcvik	k1gInSc4	výcvik
všech	všecek	k3xTgFnPc2	všecek
dvaceti	dvacet	k4xCc2	dvacet
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1960	[number]	k4	1960
z	z	k7c2	z
oddílu	oddíl	k1gInSc2	oddíl
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
vyčleněna	vyčlenit	k5eAaPmNgFnS	vyčlenit
šestičlenná	šestičlenný	k2eAgFnSc1d1	šestičlenná
skupina	skupina	k1gFnSc1	skupina
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
první	první	k4xOgInPc4	první
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgFnSc7d1	procházející
výukou	výuka	k1gFnSc7	výuka
přednostně	přednostně	k6eAd1	přednostně
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc4	program
přípravy	příprava	k1gFnSc2	příprava
sestavili	sestavit	k5eAaPmAgMnP	sestavit
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
Institutu	institut	k1gInSc2	institut
letecké	letecký	k2eAgFnSc2d1	letecká
medicíny	medicína	k1gFnSc2	medicína
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
střediska	středisko	k1gNnSc2	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Skládal	skládat	k5eAaImAgInS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
teoretické	teoretický	k2eAgFnSc2d1	teoretická
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
přednášky	přednáška	k1gFnPc4	přednáška
o	o	k7c6	o
raketách	raketa	k1gFnPc6	raketa
<g/>
,	,	kIx,	,
o	o	k7c6	o
konstrukci	konstrukce	k1gFnSc6	konstrukce
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Vostok	Vostok	k1gInSc1	Vostok
<g/>
,	,	kIx,	,
o	o	k7c6	o
letecké	letecký	k2eAgFnSc6d1	letecká
a	a	k8xC	a
kosmické	kosmický	k2eAgFnSc6d1	kosmická
medicíně	medicína	k1gFnSc6	medicína
a	a	k8xC	a
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
během	během	k7c2	během
kosmických	kosmický	k2eAgInPc2d1	kosmický
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
o	o	k7c6	o
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
,	,	kIx,	,
geofyzice	geofyzika	k1gFnSc6	geofyzika
a	a	k8xC	a
filmování	filmování	k1gNnSc6	filmování
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
lodí	loď	k1gFnSc7	loď
se	se	k3xPyFc4	se
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
seznámili	seznámit	k5eAaPmAgMnP	seznámit
ve	v	k7c6	v
výrobním	výrobní	k2eAgInSc6d1	výrobní
závodu	závod	k1gInSc6	závod
OKB-1	OKB-1	k1gFnSc2	OKB-1
i	i	k9	i
na	na	k7c6	na
kosmodromu	kosmodrom	k1gInSc6	kosmodrom
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
praktická	praktický	k2eAgFnSc1d1	praktická
část	část	k1gFnSc1	část
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
obecné	obecný	k2eAgFnSc2d1	obecná
fyzické	fyzický	k2eAgFnSc2d1	fyzická
přípravy	příprava	k1gFnSc2	příprava
<g/>
,	,	kIx,	,
náročné	náročný	k2eAgInPc1d1	náročný
testy	test	k1gInPc1	test
odolnosti	odolnost	k1gFnSc2	odolnost
–	–	k?	–
v	v	k7c6	v
barokomoře	barokomora	k1gFnSc6	barokomora
podstoupili	podstoupit	k5eAaPmAgMnP	podstoupit
simulaci	simulace	k1gFnSc4	simulace
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
centrifuze	centrifuga	k1gFnSc6	centrifuga
zažívali	zažívat	k5eAaImAgMnP	zažívat
přetížení	přetížený	k2eAgMnPc1d1	přetížený
do	do	k7c2	do
12	[number]	k4	12
G	G	kA	G
<g/>
,	,	kIx,	,
v	v	k7c6	v
termokomoře	termokomora	k1gFnSc6	termokomora
vydrželi	vydržet	k5eAaPmAgMnP	vydržet
teplotu	teplota	k1gFnSc4	teplota
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
třásli	třást	k5eAaImAgMnP	třást
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
na	na	k7c6	na
vibračním	vibrační	k2eAgInSc6d1	vibrační
stole	stol	k1gInSc6	stol
<g/>
,	,	kIx,	,
strávili	strávit	k5eAaPmAgMnP	strávit
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
v	v	k7c6	v
izolační	izolační	k2eAgFnSc6d1	izolační
komoře	komora	k1gFnSc6	komora
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc4	stav
beztíže	beztíže	k1gFnSc2	beztíže
si	se	k3xPyFc3	se
vyzkoušeli	vyzkoušet	k5eAaPmAgMnP	vyzkoušet
při	při	k7c6	při
parabolických	parabolický	k2eAgNnPc6d1	parabolické
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
cvičném	cvičný	k2eAgInSc6d1	cvičný
MiG-	MiG-	k1gMnSc2	MiG-
<g/>
15	[number]	k4	15
<g/>
UTI	UTI	kA	UTI
<g/>
,	,	kIx,	,
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
parašutistický	parašutistický	k2eAgInSc4d1	parašutistický
výcvik	výcvik	k1gInSc4	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
složila	složit	k5eAaPmAgFnS	složit
vybraná	vybraný	k2eAgFnSc1d1	vybraná
šestice	šestice	k1gFnSc1	šestice
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
získali	získat	k5eAaPmAgMnP	získat
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
"	"	kIx"	"
<g/>
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zkušební	zkušební	k2eAgFnSc1d1	zkušební
komise	komise	k1gFnSc1	komise
doporučila	doporučit	k5eAaPmAgFnS	doporučit
kandidáty	kandidát	k1gMnPc4	kandidát
ke	k	k7c3	k
kosmickým	kosmický	k2eAgInPc3d1	kosmický
letům	let	k1gInPc3	let
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
Jurij	Jurij	k1gFnSc1	Jurij
Gagarin	Gagarin	k1gInSc1	Gagarin
<g/>
,	,	kIx,	,
German	German	k1gMnSc1	German
Titov	Titov	k1gInSc1	Titov
<g/>
,	,	kIx,	,
Grigorij	Grigorij	k1gFnSc1	Grigorij
Něljubov	Něljubov	k1gInSc1	Něljubov
<g/>
,	,	kIx,	,
Andrijan	Andrijan	k1gMnSc1	Andrijan
Nikolajev	Nikolajev	k1gMnSc1	Nikolajev
<g/>
,	,	kIx,	,
Valerij	Valerij	k1gMnSc1	Valerij
Bykovskij	Bykovskij	k1gMnSc1	Bykovskij
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Popovič	Popovič	k1gMnSc1	Popovič
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
šestice	šestice	k1gFnSc2	šestice
kandidátů	kandidát	k1gMnPc2	kandidát
měli	mít	k5eAaImAgMnP	mít
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
první	první	k4xOgInSc4	první
let	let	k1gInSc4	let
Gagarin	Gagarin	k1gInSc4	Gagarin
<g/>
,	,	kIx,	,
Titov	Titov	k1gInSc4	Titov
a	a	k8xC	a
Něljubov	Něljubov	k1gInSc4	Něljubov
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rovnocenné	rovnocenný	k2eAgFnSc6d1	rovnocenná
připravenosti	připravenost	k1gFnSc6	připravenost
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
Gagarinova	Gagarinův	k2eAgFnSc1d1	Gagarinova
povaha	povaha	k1gFnSc1	povaha
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
srdečnost	srdečnost	k1gFnSc4	srdečnost
a	a	k8xC	a
skromnost	skromnost	k1gFnSc4	skromnost
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
vybrala	vybrat	k5eAaPmAgFnS	vybrat
prvního	první	k4xOgMnSc4	první
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
Státní	státní	k2eAgFnSc2d1	státní
komise	komise	k1gFnSc2	komise
pod	pod	k7c7	pod
předsednictvím	předsednictví	k1gNnSc7	předsednictví
Konstantina	Konstantin	k1gMnSc2	Konstantin
Rudněva	Rudněv	k1gMnSc2	Rudněv
až	až	k9	až
na	na	k7c4	na
Bajkonuru	Bajkonura	k1gFnSc4	Bajkonura
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Titov	Titov	k1gInSc4	Titov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
náhradníkem	náhradník	k1gMnSc7	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
Kamanin	Kamanin	k2eAgInSc4d1	Kamanin
s	s	k7c7	s
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
komise	komise	k1gFnSc1	komise
seznámil	seznámit	k5eAaPmAgMnS	seznámit
kosmonauty	kosmonaut	k1gMnPc4	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pouze	pouze	k6eAd1	pouze
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
pořadí	pořadí	k1gNnSc1	pořadí
známé	známý	k2eAgNnSc1d1	známé
už	už	k6eAd1	už
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
přesto	přesto	k8xC	přesto
Gagarin	Gagarin	k1gInSc1	Gagarin
reagoval	reagovat	k5eAaBmAgInS	reagovat
radostně	radostně	k6eAd1	radostně
a	a	k8xC	a
Titov	Titov	k1gInSc4	Titov
poněkud	poněkud	k6eAd1	poněkud
rozmrzele	rozmrzele	k6eAd1	rozmrzele
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zasedání	zasedání	k1gNnSc1	zasedání
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
kamery	kamera	k1gFnPc4	kamera
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
šestice	šestice	k1gFnSc2	šestice
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
činitelů	činitel	k1gMnPc2	činitel
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
přítomno	přítomno	k1gNnSc1	přítomno
na	na	k7c4	na
70	[number]	k4	70
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
komise	komise	k1gFnSc1	komise
formálně	formálně	k6eAd1	formálně
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
start	start	k1gInSc4	start
Vostoku	Vostok	k1gInSc2	Vostok
a	a	k8xC	a
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
výběr	výběr	k1gInSc4	výběr
Gagarina	Gagarino	k1gNnSc2	Gagarino
<g/>
,	,	kIx,	,
s	s	k7c7	s
Titovem	Titov	k1gInSc7	Titov
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
s	s	k7c7	s
úsvitem	úsvit	k1gInSc7	úsvit
technici	technik	k1gMnPc1	technik
vyvezli	vyvézt	k5eAaPmAgMnP	vyvézt
raketu	raketa	k1gFnSc4	raketa
na	na	k7c4	na
start	start	k1gInSc4	start
<g/>
.	.	kIx.	.
</s>
<s>
Dopoledne	dopoledne	k6eAd1	dopoledne
si	se	k3xPyFc3	se
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
poslechli	poslechnout	k5eAaPmAgMnP	poslechnout
poslední	poslední	k2eAgFnPc4d1	poslední
instrukce	instrukce	k1gFnPc4	instrukce
konstruktéra	konstruktér	k1gMnSc2	konstruktér
Konstantina	Konstantin	k1gMnSc2	Konstantin
Feoktistova	Feoktistův	k2eAgFnSc1d1	Feoktistova
<g/>
,	,	kIx,	,
odpoledne	odpoledne	k6eAd1	odpoledne
se	se	k3xPyFc4	se
Gagarin	Gagarin	k1gInSc1	Gagarin
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
důstojníky	důstojník	k1gMnPc7	důstojník
sloužícími	sloužící	k1gMnPc7	sloužící
na	na	k7c6	na
kosmodromu	kosmodrom	k1gInSc6	kosmodrom
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
Gagarin	Gagarin	k1gInSc1	Gagarin
s	s	k7c7	s
Titovem	Titov	k1gInSc7	Titov
strávili	strávit	k5eAaPmAgMnP	strávit
v	v	k7c6	v
dřevěném	dřevěný	k2eAgInSc6d1	dřevěný
domku	domek	k1gInSc6	domek
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
<g/>
,	,	kIx,	,
usnout	usnout	k5eAaPmF	usnout
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vostok	Vostok	k1gInSc1	Vostok
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vstávali	vstávat	k5eAaImAgMnP	vstávat
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
v	v	k7c6	v
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
moskevského	moskevský	k2eAgInSc2d1	moskevský
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Snídani	snídaně	k1gFnSc4	snídaně
z	z	k7c2	z
tub	tuba	k1gFnPc2	tuba
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
montážní	montážní	k2eAgFnSc6d1	montážní
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následovala	následovat	k5eAaImAgFnS	následovat
krátká	krátká	k1gFnSc1	krátká
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
prohlídka	prohlídka	k1gFnSc1	prohlídka
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
sedmou	sedma	k1gFnSc7	sedma
přijeli	přijet	k5eAaPmAgMnP	přijet
Gagarin	Gagarin	k1gInSc4	Gagarin
a	a	k8xC	a
Titov	Titov	k1gInSc1	Titov
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
ve	v	k7c6	v
skafandrech	skafandr	k1gInPc6	skafandr
<g/>
,	,	kIx,	,
na	na	k7c4	na
startovní	startovní	k2eAgFnSc4d1	startovní
plošinu	plošina	k1gFnSc4	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
hodiny	hodina	k1gFnPc1	hodina
před	před	k7c7	před
startem	start	k1gInSc7	start
<g/>
,	,	kIx,	,
v	v	k7c6	v
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
Gagarin	Gagarin	k1gInSc1	Gagarin
usazen	usazen	k2eAgInSc1d1	usazen
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
a	a	k8xC	a
navázal	navázat	k5eAaPmAgMnS	navázat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
řídícím	řídící	k2eAgInSc7d1	řídící
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgInS	používat
přidělený	přidělený	k2eAgInSc1d1	přidělený
volací	volací	k2eAgInSc1d1	volací
znak	znak	k1gInSc1	znak
Kedr	Kedra	k1gFnPc2	Kedra
(	(	kIx(	(
<g/>
К	К	k?	К
<g/>
,	,	kIx,	,
limba	limba	k1gFnSc1	limba
nebo	nebo	k8xC	nebo
cedr	cedr	k1gInSc1	cedr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgNnSc1d1	řídící
středisko	středisko	k1gNnSc1	středisko
bylo	být	k5eAaImAgNnS	být
Zarja	Zarja	k1gFnSc1	Zarja
(	(	kIx(	(
<g/>
З	З	k?	З
<g/>
,	,	kIx,	,
svítání	svítání	k1gNnSc2	svítání
či	či	k8xC	či
záře	zář	k1gFnSc2	zář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prověření	prověření	k1gNnSc6	prověření
spojení	spojení	k1gNnSc2	spojení
a	a	k8xC	a
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
Gagarin	Gagarin	k1gInSc1	Gagarin
už	už	k6eAd1	už
jen	jen	k9	jen
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
start	start	k1gInSc4	start
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
udržování	udržování	k1gNnSc6	udržování
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
se	se	k3xPyFc4	se
střídali	střídat	k5eAaImAgMnP	střídat
Kamanin	Kamanina	k1gFnPc2	Kamanina
<g/>
,	,	kIx,	,
Koroljov	Koroljov	k1gInSc1	Koroljov
a	a	k8xC	a
Popovič	Popovič	k1gInSc1	Popovič
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ukrácení	ukrácení	k1gNnSc4	ukrácení
čekání	čekání	k1gNnSc6	čekání
mu	on	k3xPp3gMnSc3	on
pouštěli	pouštět	k5eAaImAgMnP	pouštět
do	do	k7c2	do
sluchátek	sluchátko	k1gNnPc2	sluchátko
písničky	písnička	k1gFnSc2	písnička
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
v	v	k7c4	v
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
moskevského	moskevský	k2eAgInSc2d1	moskevský
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
světového	světový	k2eAgInSc2d1	světový
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
Jurij	Jurij	k1gMnSc1	Jurij
Gagarin	Gagarin	k1gInSc4	Gagarin
v	v	k7c6	v
kosmické	kosmický	k2eAgFnSc6d1	kosmická
lodi	loď	k1gFnSc6	loď
Vostok	Vostok	k1gInSc1	Vostok
1	[number]	k4	1
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
startu	start	k1gInSc6	start
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
minutu	minuta	k1gFnSc4	minuta
po	po	k7c6	po
startu	start	k1gInSc6	start
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
přetížení	přetížení	k1gNnSc1	přetížení
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
G	G	kA	G
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
snižovalo	snižovat	k5eAaImAgNnS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Gagarin	Gagarin	k1gInSc4	Gagarin
je	být	k5eAaImIp3nS	být
snášel	snášet	k5eAaImAgMnS	snášet
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
puls	puls	k1gInSc4	puls
mu	on	k3xPp3gMnSc3	on
vzrostl	vzrůst	k5eAaPmAgMnS	vzrůst
z	z	k7c2	z
klidných	klidný	k2eAgNnPc2d1	klidné
64	[number]	k4	64
na	na	k7c4	na
150	[number]	k4	150
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
rakety	raketa	k1gFnSc2	raketa
(	(	kIx(	(
<g/>
blok	blok	k1gInSc1	blok
A	A	kA	A
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vypnul	vypnout	k5eAaPmAgInS	vypnout
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
<g/>
;	;	kIx,	;
Vostok	Vostok	k1gInSc1	Vostok
1	[number]	k4	1
proto	proto	k8xC	proto
nabral	nabrat	k5eAaPmAgInS	nabrat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
letu	let	k1gInSc2	let
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
180	[number]	k4	180
<g/>
–	–	k?	–
<g/>
230	[number]	k4	230
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
parametry	parametr	k1gInPc7	parametr
181	[number]	k4	181
<g/>
–	–	k?	–
<g/>
327	[number]	k4	327
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
8	[number]	k4	8
minutách	minuta	k1gFnPc6	minuta
36	[number]	k4	36
sekundách	sekunda	k1gFnPc6	sekunda
letu	let	k1gInSc2	let
dohořel	dohořet	k5eAaPmAgInS	dohořet
motor	motor	k1gInSc1	motor
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgMnS	oddělit
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
rakety	raketa	k1gFnSc2	raketa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
Gagarin	Gagarin	k1gInSc1	Gagarin
pocítil	pocítit	k5eAaPmAgInS	pocítit
stav	stav	k1gInSc4	stav
beztíže	beztíže	k1gFnSc2	beztíže
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
letěla	letět	k5eAaImAgFnS	letět
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
letu	let	k1gInSc2	let
udržoval	udržovat	k5eAaImAgInS	udržovat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
řídícím	řídící	k2eAgNnSc7d1	řídící
střediskem	středisko	k1gNnSc7	středisko
<g/>
,	,	kIx,	,
zapisoval	zapisovat	k5eAaImAgMnS	zapisovat
svá	svůj	k3xOyFgNnPc4	svůj
pozorování	pozorování	k1gNnPc4	pozorování
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
(	(	kIx(	(
<g/>
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
letu	let	k1gInSc2	let
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
výborně	výborně	k6eAd1	výborně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gNnSc3	on
uplavala	uplavat	k5eAaPmAgFnS	uplavat
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
diktoval	diktovat	k5eAaImAgMnS	diktovat
na	na	k7c4	na
záznam	záznam	k1gInSc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
57	[number]	k4	57
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
loď	loď	k1gFnSc1	loď
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
maximální	maximální	k2eAgFnPc4d1	maximální
výšky	výška	k1gFnPc4	výška
327	[number]	k4	327
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Gagarin	Gagarin	k1gInSc1	Gagarin
trochu	trochu	k6eAd1	trochu
pojedl	pojíst	k5eAaPmAgInS	pojíst
<g/>
,	,	kIx,	,
napil	napít	k5eAaPmAgMnS	napít
se	se	k3xPyFc4	se
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
chystat	chystat	k5eAaImF	chystat
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Sestup	sestup	k1gInSc1	sestup
byl	být	k5eAaImAgInS	být
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
<g/>
,	,	kIx,	,
v	v	k7c6	v
10.25	[number]	k4	10.25
motor	motor	k1gInSc1	motor
Vostoku	Vostok	k1gInSc2	Vostok
zbrzdil	zbrzdit	k5eAaPmAgInS	zbrzdit
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
nepracoval	pracovat	k5eNaImAgMnS	pracovat
však	však	k9	však
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
řídící	řídící	k2eAgInSc4d1	řídící
systém	systém	k1gInSc4	systém
lodi	loď	k1gFnSc2	loď
proto	proto	k8xC	proto
neoddělil	oddělit	k5eNaPmAgMnS	oddělit
přístrojový	přístrojový	k2eAgInSc4d1	přístrojový
úsek	úsek	k1gInSc4	úsek
od	od	k7c2	od
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
navíc	navíc	k6eAd1	navíc
začala	začít	k5eAaPmAgFnS	začít
rotovat	rotovat	k5eAaImF	rotovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Gagarin	Gagarin	k1gInSc4	Gagarin
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nemohl	moct	k5eNaImAgInS	moct
pomoci	pomoct	k5eAaPmF	pomoct
ani	ani	k9	ani
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
10	[number]	k4	10
minutách	minuta	k1gFnPc6	minuta
automatika	automatika	k1gFnSc1	automatika
přístrojový	přístrojový	k2eAgInSc4d1	přístrojový
úsek	úsek	k1gInSc4	úsek
konečně	konečně	k6eAd1	konečně
odstřelila	odstřelit	k5eAaPmAgFnS	odstřelit
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
stočila	stočit	k5eAaPmAgFnS	stočit
potřebným	potřebný	k2eAgInSc7d1	potřebný
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
rotace	rotace	k1gFnSc1	rotace
ustala	ustat	k5eAaPmAgFnS	ustat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
sestup	sestup	k1gInSc1	sestup
probíhal	probíhat	k5eAaImAgInS	probíhat
normálně	normálně	k6eAd1	normálně
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
ochranného	ochranný	k2eAgInSc2d1	ochranný
štítu	štít	k1gInSc2	štít
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
2000	[number]	k4	2000
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
přetížení	přetížení	k1gNnSc2	přetížení
10	[number]	k4	10
G.	G.	kA	G.
Ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
7	[number]	k4	7
km	km	kA	km
se	se	k3xPyFc4	se
odstřelil	odstřelit	k5eAaPmAgInS	odstřelit
poklop	poklop	k1gInSc1	poklop
průlezu	průlez	k1gInSc2	průlez
<g/>
,	,	kIx,	,
pilot	pilot	k1gInSc1	pilot
byl	být	k5eAaImAgInS	být
katapultován	katapultovat	k5eAaBmNgInS	katapultovat
a	a	k8xC	a
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
padáku	padák	k1gInSc6	padák
vedle	vedle	k7c2	vedle
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
odchylek	odchylka	k1gFnPc2	odchylka
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
motorů	motor	k1gInPc2	motor
loď	loď	k1gFnSc1	loď
nedoletěla	doletět	k5eNaPmAgFnS	doletět
do	do	k7c2	do
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
přistávací	přistávací	k2eAgFnSc2d1	přistávací
oblasti	oblast	k1gFnSc2	oblast
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Kujbyševa	Kujbyšev	k1gInSc2	Kujbyšev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přistála	přistát	k5eAaPmAgFnS	přistát
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Smelovka	Smelovka	k1gFnSc1	Smelovka
ležící	ležící	k2eAgFnSc1d1	ležící
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Engels	Engels	k1gMnSc1	Engels
v	v	k7c6	v
Saratovské	Saratovský	k2eAgFnSc6d1	Saratovská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
místo	místo	k1gNnSc1	místo
přistání	přistání	k1gNnSc2	přistání
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Vostok	Vostok	k1gInSc1	Vostok
1	[number]	k4	1
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
Gagarin	Gagarin	k1gInSc1	Gagarin
kompletní	kompletní	k2eAgInSc4d1	kompletní
oblet	oblet	k1gInSc4	oblet
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
kosmický	kosmický	k2eAgInSc1d1	kosmický
let	let	k1gInSc1	let
trval	trvat	k5eAaImAgInS	trvat
108	[number]	k4	108
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
i	i	k8xC	i
nejkratší	krátký	k2eAgInSc1d3	nejkratší
orbitální	orbitální	k2eAgInSc1d1	orbitální
kosmický	kosmický	k2eAgInSc1d1	kosmický
let	let	k1gInSc1	let
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
kosmickém	kosmický	k2eAgInSc6d1	kosmický
letu	let	k1gInSc6	let
SSSR	SSSR	kA	SSSR
požádal	požádat	k5eAaPmAgInS	požádat
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
leteckou	letecký	k2eAgFnSc4d1	letecká
federaci	federace	k1gFnSc4	federace
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
leteckých	letecký	k2eAgInPc2d1	letecký
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
délky	délka	k1gFnSc2	délka
letu	let	k1gInSc2	let
40	[number]	k4	40
868,6	[number]	k4	868,6
km	km	kA	km
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc2d1	maximální
rychlosti	rychlost	k1gFnSc2	rychlost
letu	let	k1gInSc2	let
28	[number]	k4	28
260	[number]	k4	260
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
a	a	k8xC	a
výšky	výška	k1gFnSc2	výška
letu	let	k1gInSc2	let
327	[number]	k4	327
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
regule	regule	k1gFnSc1	regule
uznání	uznání	k1gNnSc2	uznání
rekordu	rekord	k1gInSc2	rekord
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
přistání	přistání	k1gNnSc4	přistání
pilota	pilot	k1gMnSc2	pilot
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
stroji	stroj	k1gInSc6	stroj
<g/>
,	,	kIx,	,
zatajili	zatajit	k5eAaPmAgMnP	zatajit
Sověti	Sovět	k1gMnPc1	Sovět
katapultáž	katapultáž	k1gFnSc1	katapultáž
pilota	pilota	k1gFnSc1	pilota
a	a	k8xC	a
přistání	přistání	k1gNnSc1	přistání
na	na	k7c6	na
padáku	padák	k1gInSc6	padák
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Gagarin	Gagarin	k1gInSc1	Gagarin
byl	být	k5eAaImAgInS	být
donucen	donutit	k5eAaPmNgInS	donutit
nadřízenými	nadřízený	k2eAgInPc7d1	nadřízený
orgány	orgán	k1gInPc7	orgán
lhát	lhát	k5eAaImF	lhát
na	na	k7c6	na
první	první	k4xOgFnSc6	první
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
sále	sál	k1gInSc6	sál
Domu	dům	k1gInSc2	dům
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
vynechat	vynechat	k5eAaPmF	vynechat
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
<g/>
.	.	kIx.	.
</s>
<s>
FAI	FAI	kA	FAI
rekordy	rekord	k1gInPc4	rekord
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1961	[number]	k4	1961
oficiálně	oficiálně	k6eAd1	oficiálně
zaregistrovala	zaregistrovat	k5eAaPmAgNnP	zaregistrovat
<g/>
.	.	kIx.	.
</s>
<s>
Uskutečnění	uskutečnění	k1gNnSc1	uskutečnění
prvního	první	k4xOgNnSc2	první
kosmického	kosmický	k2eAgNnSc2d1	kosmické
letu	let	k1gInSc3	let
člověka	člověk	k1gMnSc4	člověk
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
překvapení	překvapení	k1gNnSc1	překvapení
nemenší	malý	k2eNgNnSc1d2	nemenší
než	než	k8xS	než
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
kosmickém	kosmický	k2eAgInSc6d1	kosmický
letu	let	k1gInSc6	let
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
také	také	k9	také
sovětském	sovětský	k2eAgInSc6d1	sovětský
Sputniku	sputnik	k1gInSc6	sputnik
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
reagoval	reagovat	k5eAaBmAgInS	reagovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1961	[number]	k4	1961
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
nového	nový	k2eAgInSc2d1	nový
ambiciózního	ambiciózní	k2eAgInSc2d1	ambiciózní
cíle	cíl	k1gInSc2	cíl
amerického	americký	k2eAgInSc2d1	americký
kosmického	kosmický	k2eAgInSc2d1	kosmický
programu	program	k1gInSc2	program
–	–	k?	–
letu	let	k1gInSc2	let
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
do	do	k7c2	do
konce	konec	k1gInSc2	konec
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
neznámého	známý	k2eNgMnSc2d1	neznámý
vojenského	vojenský	k2eAgMnSc2d1	vojenský
letce	letec	k1gMnSc2	letec
se	se	k3xPyFc4	se
během	během	k7c2	během
hodiny	hodina	k1gFnSc2	hodina
stal	stát	k5eAaPmAgMnS	stát
nejpopulárnější	populární	k2eAgMnSc1d3	nejpopulárnější
člověk	člověk	k1gMnSc1	člověk
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
toho	ten	k3xDgMnSc4	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
náležitě	náležitě	k6eAd1	náležitě
využila	využít	k5eAaPmAgFnS	využít
k	k	k7c3	k
propagačním	propagační	k2eAgInPc3d1	propagační
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Gagarin	Gagarin	k1gInSc1	Gagarin
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
z	z	k7c2	z
nadporučíka	nadporučík	k1gMnSc2	nadporučík
(	(	kIx(	(
<g/>
с	с	k?	с
л	л	k?	л
<g/>
)	)	kIx)	)
na	na	k7c4	na
majora	major	k1gMnSc4	major
(	(	kIx(	(
<g/>
м	м	k?	м
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyznamenán	vyznamenán	k2eAgInSc1d1	vyznamenán
titulem	titul	k1gInSc7	titul
hrdina	hrdina	k1gMnSc1	hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
Leninovým	Leninův	k2eAgInSc7d1	Leninův
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
triumfálního	triumfální	k2eAgNnSc2d1	triumfální
přijetí	přijetí	k1gNnSc3	přijetí
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
a	a	k8xC	a
měsících	měsíc	k1gInPc6	měsíc
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
až	až	k9	až
po	po	k7c6	po
Havanu	havano	k1gNnSc6	havano
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gagarinova	Gagarinův	k2eAgFnSc1d1	Gagarinova
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc4	Československo
navštívil	navštívit	k5eAaPmAgMnS	navštívit
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Šarm	šarm	k1gInSc1	šarm
<g/>
,	,	kIx,	,
neustálý	neustálý	k2eAgInSc1d1	neustálý
úsměv	úsměv	k1gInSc1	úsměv
a	a	k8xC	a
přirozená	přirozený	k2eAgFnSc1d1	přirozená
inteligence	inteligence	k1gFnSc1	inteligence
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
získaly	získat	k5eAaPmAgFnP	získat
sympatie	sympatie	k1gFnPc1	sympatie
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
popularitě	popularita	k1gFnSc3	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
celebrity	celebrita	k1gFnSc2	celebrita
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
i	i	k9	i
Gagarinovo	Gagarinův	k2eAgNnSc1d1	Gagarinovo
chování	chování	k1gNnSc1	chování
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečná	konečný	k2eNgFnSc1d1	nekonečná
řada	řada	k1gFnSc1	řada
setkání	setkání	k1gNnSc2	setkání
a	a	k8xC	a
banketů	banket	k1gInPc2	banket
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
si	se	k3xPyFc3	se
kdekdo	kdekdo	k3yInSc1	kdekdo
chtěl	chtít	k5eAaImAgMnS	chtít
se	s	k7c7	s
slavným	slavný	k2eAgMnSc7d1	slavný
hostem	host	k1gMnSc7	host
připít	připít	k5eAaPmF	připít
"	"	kIx"	"
<g/>
až	až	k9	až
do	do	k7c2	do
dna	dno	k1gNnSc2	dno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nezůstaly	zůstat	k5eNaPmAgFnP	zůstat
bez	bez	k7c2	bez
následků	následek	k1gInPc2	následek
<g/>
.	.	kIx.	.
</s>
<s>
Gagarin	Gagarin	k1gInSc1	Gagarin
přestal	přestat	k5eAaPmAgInS	přestat
sportovat	sportovat	k5eAaImF	sportovat
<g/>
,	,	kIx,	,
přibral	přibrat	k5eAaPmAgMnS	přibrat
<g/>
,	,	kIx,	,
a	a	k8xC	a
namísto	namísto	k7c2	namísto
létání	létání	k1gNnSc2	létání
se	se	k3xPyFc4	se
proháněl	prohánět	k5eAaImAgInS	prohánět
autem	auto	k1gNnSc7	auto
po	po	k7c6	po
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Vážný	vážný	k2eAgInSc1d1	vážný
incident	incident	k1gInSc1	incident
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
září	září	k1gNnSc6	září
1961	[number]	k4	1961
při	při	k7c6	při
dovolené	dovolená	k1gFnSc6	dovolená
ve	v	k7c6	v
Forosu	Foros	k1gInSc6	Foros
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Flirt	flirt	k1gInSc1	flirt
se	s	k7c7	s
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
sestrou	sestra	k1gFnSc7	sestra
skončil	skončit	k5eAaPmAgMnS	skončit
skokem	skok	k1gInSc7	skok
z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
<g/>
,	,	kIx,	,
rozbitou	rozbitý	k2eAgFnSc7d1	rozbitá
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
měsíčním	měsíční	k2eAgInSc7d1	měsíční
pobytem	pobyt	k1gInSc7	pobyt
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Manželce	manželka	k1gFnSc3	manželka
poté	poté	k6eAd1	poté
zůstal	zůstat	k5eAaPmAgInS	zůstat
věrný	věrný	k2eAgInSc1d1	věrný
<g/>
,	,	kIx,	,
ostatně	ostatně	k6eAd1	ostatně
ona	onen	k3xDgFnSc1	onen
ho	on	k3xPp3gMnSc4	on
pozorně	pozorně	k6eAd1	pozorně
střežila	střežit	k5eAaImAgFnS	střežit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
rychlými	rychlý	k2eAgInPc7d1	rychlý
auty	aut	k1gInPc7	aut
a	a	k8xC	a
alkoholem	alkohol	k1gInSc7	alkohol
(	(	kIx(	(
<g/>
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
táhly	táhnout	k5eAaImAgInP	táhnout
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
populární	populární	k2eAgFnSc4d1	populární
osobnost	osobnost	k1gFnSc4	osobnost
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1962	[number]	k4	1962
za	za	k7c4	za
rodnou	rodný	k2eAgFnSc4d1	rodná
Smolenskou	Smolenský	k2eAgFnSc4d1	Smolenská
oblast	oblast	k1gFnSc4	oblast
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Sovětu	sovět	k1gInSc2	sovět
svazu	svaz	k1gInSc2	svaz
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
Sovětu	sovět	k1gInSc2	sovět
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
Komsomolu	Komsomol	k1gInSc2	Komsomol
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
a	a	k8xC	a
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
jeho	jeho	k3xOp3gInSc2	jeho
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
aktivním	aktivní	k2eAgInSc7d1	aktivní
členem	člen	k1gInSc7	člen
Komsomolu	Komsomol	k1gInSc2	Komsomol
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
v	v	k7c6	v
saratovské	saratovský	k2eAgFnSc6d1	saratovský
škole	škola	k1gFnSc6	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgInS	zaujmout
také	také	k9	také
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Společnosti	společnost	k1gFnSc2	společnost
sovětsko-kubánského	sovětskoubánský	k2eAgNnSc2d1	sovětsko-kubánský
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
značnou	značný	k2eAgFnSc4d1	značná
zátěž	zátěž	k1gFnSc4	zátěž
veřejných	veřejný	k2eAgFnPc2d1	veřejná
povinností	povinnost	k1gFnPc2	povinnost
zůstal	zůstat	k5eAaPmAgInS	zůstat
Gagarin	Gagarin	k1gInSc1	Gagarin
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vedoucí	vedoucí	k1gMnPc1	vedoucí
oddílu	oddíl	k1gInSc2	oddíl
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1963	[number]	k4	1963
a	a	k8xC	a
zástupce	zástupce	k1gMnSc1	zástupce
velitele	velitel	k1gMnSc2	velitel
CPK	CPK	kA	CPK
pro	pro	k7c4	pro
leteckou	letecký	k2eAgFnSc4d1	letecká
a	a	k8xC	a
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
přípravu	příprava	k1gFnSc4	příprava
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
výcviku	výcvik	k1gInSc6	výcvik
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
kosmických	kosmický	k2eAgNnPc6d1	kosmické
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
programu	program	k1gInSc2	program
letu	let	k1gInSc2	let
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1965	[number]	k4	1965
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
nácvik	nácvik	k1gInSc4	nácvik
spojení	spojení	k1gNnSc4	spojení
kosmických	kosmický	k2eAgFnPc2d1	kosmická
lodí	loď	k1gFnPc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
jako	jako	k8xC	jako
náhradník	náhradník	k1gMnSc1	náhradník
velitele	velitel	k1gMnSc2	velitel
aktivní	aktivní	k2eAgFnSc2d1	aktivní
lodě	loď	k1gFnSc2	loď
Vladimira	Vladimiro	k1gNnSc2	Vladimiro
Komarova	Komarův	k2eAgFnSc1d1	Komarova
<g/>
,	,	kIx,	,
s	s	k7c7	s
perspektivou	perspektiva	k1gFnSc7	perspektiva
opětného	opětný	k2eAgInSc2d1	opětný
letu	let	k1gInSc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opětném	opětný	k2eAgNnSc6d1	opětné
zařazení	zařazení	k1gNnSc6	zařazení
do	do	k7c2	do
přípravy	příprava	k1gFnSc2	příprava
k	k	k7c3	k
letu	let	k1gInSc3	let
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
důkladností	důkladnost	k1gFnSc7	důkladnost
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
výcviku	výcvik	k1gInSc2	výcvik
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
ke	k	k7c3	k
sportu	sport	k1gInSc3	sport
<g/>
,	,	kIx,	,
zhubl	zhubnout	k5eAaPmAgInS	zhubnout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozdělení	rozdělení	k1gNnSc6	rozdělení
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
programů	program	k1gInPc2	program
v	v	k7c6	v
září	září	k1gNnSc6	září
1966	[number]	k4	1966
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
připravující	připravující	k2eAgFnSc2d1	připravující
se	se	k3xPyFc4	se
na	na	k7c4	na
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Komarova	Komarův	k2eAgFnSc1d1	Komarova
při	při	k7c6	při
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
letu	let	k1gInSc6	let
Sojuzu	Sojuz	k1gInSc2	Sojuz
1	[number]	k4	1
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1967	[number]	k4	1967
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
opatrnosti	opatrnost	k1gFnSc2	opatrnost
odstraněn	odstranit	k5eAaPmNgInS	odstranit
z	z	k7c2	z
přípravy	příprava	k1gFnSc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1961	[number]	k4	1961
Gagarin	Gagarin	k1gInSc1	Gagarin
společně	společně	k6eAd1	společně
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
z	z	k7c2	z
oddílu	oddíl	k1gInSc2	oddíl
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Žukovského	Žukovského	k2eAgFnSc6d1	Žukovského
vojenské	vojenský	k2eAgFnSc6d1	vojenská
letecké	letecký	k2eAgFnSc6d1	letecká
inženýrské	inženýrský	k2eAgFnSc6d1	inženýrská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
novu	nova	k1gFnSc4	nova
specializaci	specializace	k1gFnSc4	specializace
pilot-inženýr-kosmonaut	pilotnženýrosmonaout	k5eAaPmNgInS	pilot-inženýr-kosmonaout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
komplexní	komplexní	k2eAgFnSc6d1	komplexní
diplomové	diplomový	k2eAgFnSc6d1	Diplomová
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
možnost	možnost	k1gFnSc4	možnost
postavení	postavení	k1gNnSc2	postavení
opakovaně	opakovaně	k6eAd1	opakovaně
použitelné	použitelný	k2eAgFnSc3d1	použitelná
okřídlené	okřídlený	k2eAgFnPc4d1	okřídlená
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
části	část	k1gFnPc4	část
problematiky	problematika	k1gFnSc2	problematika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Gagarin	Gagarin	k1gInSc1	Gagarin
hrál	hrát	k5eAaImAgInS	hrát
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
roli	role	k1gFnSc6	role
"	"	kIx"	"
<g/>
hlavního	hlavní	k2eAgMnSc2d1	hlavní
konstruktéra	konstruktér	k1gMnSc2	konstruktér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
určoval	určovat	k5eAaImAgInS	určovat
metodu	metoda	k1gFnSc4	metoda
využití	využití	k1gNnSc2	využití
a	a	k8xC	a
vybíral	vybírat	k5eAaImAgMnS	vybírat
celkový	celkový	k2eAgInSc4d1	celkový
tvar	tvar	k1gInSc4	tvar
letounu	letoun	k1gInSc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trenažéru	trenažér	k1gInSc6	trenažér
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
způsoby	způsob	k1gInPc4	způsob
přistání	přistání	k1gNnSc2	přistání
uvažovaného	uvažovaný	k2eAgInSc2d1	uvažovaný
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
obhájil	obhájit	k5eAaPmAgMnS	obhájit
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Smrt	smrt	k1gFnSc1	smrt
Jurije	Jurije	k1gFnSc1	Jurije
Gagarina	Gagarina	k1gFnSc1	Gagarina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
po	po	k7c6	po
několikaměsíční	několikaměsíční	k2eAgFnSc6d1	několikaměsíční
přestávce	přestávka	k1gFnSc6	přestávka
(	(	kIx(	(
<g/>
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
létání	létání	k1gNnSc3	létání
<g/>
.	.	kIx.	.
</s>
<s>
Cvičné	cvičný	k2eAgInPc4d1	cvičný
lety	let	k1gInPc4	let
opět	opět	k6eAd1	opět
zahájil	zahájit	k5eAaPmAgInS	zahájit
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
a	a	k8xC	a
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
provedl	provést	k5eAaPmAgMnS	provést
18	[number]	k4	18
letů	let	k1gInPc2	let
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Dopoledne	dopoledne	k1gNnSc1	dopoledne
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
měl	mít	k5eAaImAgMnS	mít
Jurij	Jurij	k1gMnSc1	Jurij
Gagarin	Gagarin	k1gInSc4	Gagarin
provést	provést	k5eAaPmF	provést
poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
s	s	k7c7	s
instruktorem	instruktor	k1gMnSc7	instruktor
na	na	k7c6	na
cvičném	cvičný	k2eAgInSc6d1	cvičný
MiG-	MiG-	k1gMnSc2	MiG-
<g/>
15	[number]	k4	15
<g/>
UTI	UTI	kA	UTI
<g/>
,	,	kIx,	,
vyrobeném	vyrobený	k2eAgNnSc6d1	vyrobené
v	v	k7c6	v
Aeru	aero	k1gNnSc6	aero
Vodochody	Vodochod	k1gInPc1	Vodochod
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
let	let	k1gInSc4	let
techniky	technika	k1gFnSc2	technika
pilotáže	pilotáž	k1gFnSc2	pilotáž
před	před	k7c7	před
samostatnými	samostatný	k2eAgNnPc7d1	samostatné
lety	léto	k1gNnPc7	léto
na	na	k7c6	na
už	už	k6eAd1	už
připraveném	připravený	k2eAgInSc6d1	připravený
MiGu-	MiGu-	k1gMnSc7	MiGu-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
měly	mít	k5eAaImAgFnP	mít
následovat	následovat	k5eAaImF	následovat
už	už	k6eAd1	už
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
instruktorem	instruktor	k1gMnSc7	instruktor
<g/>
,	,	kIx,	,
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Serjoginem	Serjogin	k1gMnSc7	Serjogin
<g/>
,	,	kIx,	,
hrdinou	hrdina	k1gMnSc7	hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
velitelem	velitel	k1gMnSc7	velitel
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
při	při	k7c6	při
letu	let	k1gInSc6	let
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Gagarin	Gagarin	k1gInSc1	Gagarin
se	s	k7c7	s
Serjoginem	Serjogin	k1gInSc7	Serjogin
vzlétli	vzlétnout	k5eAaPmAgMnP	vzlétnout
v	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
v	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
26	[number]	k4	26
Gagarin	Gagarin	k1gInSc1	Gagarin
zahájil	zahájit	k5eAaPmAgInS	zahájit
v	v	k7c6	v
určené	určený	k2eAgFnSc6d1	určená
zóně	zóna	k1gFnSc6	zóna
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
4200	[number]	k4	4200
m	m	kA	m
trénink	trénink	k1gInSc1	trénink
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
10.30	[number]	k4	10.30
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
splnění	splnění	k1gNnSc3	splnění
úkolu	úkol	k1gInSc2	úkol
a	a	k8xC	a
zažádal	zažádat	k5eAaPmAgMnS	zažádat
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Řídící	řídící	k2eAgNnSc4d1	řídící
létání	létání	k1gNnSc4	létání
návrat	návrat	k1gInSc1	návrat
bez	bez	k7c2	bez
otázek	otázka	k1gFnPc2	otázka
povolil	povolit	k5eAaPmAgMnS	povolit
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
cvičení	cvičení	k1gNnSc1	cvičení
mělo	mít	k5eAaImAgNnS	mít
trvat	trvat	k5eAaImF	trvat
nejméně	málo	k6eAd3	málo
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Gagarin	Gagarin	k1gInSc1	Gagarin
ani	ani	k8xC	ani
Serjogin	Serjogin	k1gMnSc1	Serjogin
už	už	k6eAd1	už
neozvali	ozvat	k5eNaPmAgMnP	ozvat
<g/>
.	.	kIx.	.
</s>
<s>
Trosky	troska	k1gFnPc1	troska
letounu	letoun	k1gInSc2	letoun
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
65	[number]	k4	65
km	km	kA	km
od	od	k7c2	od
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Novoselovo	Novoselův	k2eAgNnSc1d1	Novoselovo
<g/>
.	.	kIx.	.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1	okolnost
Gagarinova	Gagarinův	k2eAgInSc2d1	Gagarinův
posledního	poslední	k2eAgInSc2d1	poslední
letu	let	k1gInSc2	let
šetřila	šetřit	k5eAaImAgFnS	šetřit
vládní	vládní	k2eAgFnSc1d1	vládní
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
července	červenec	k1gInSc2	červenec
nakupila	nakupit	k5eAaPmAgFnS	nakupit
29	[number]	k4	29
svazků	svazek	k1gInPc2	svazek
výpovědí	výpověď	k1gFnPc2	výpověď
a	a	k8xC	a
rozborů	rozbor	k1gInPc2	rozbor
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určit	určit	k5eAaPmF	určit
příčinu	příčina	k1gFnSc4	příčina
pádu	pád	k1gInSc2	pád
letounu	letoun	k1gInSc2	letoun
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
komise	komise	k1gFnSc2	komise
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobnou	pravděpodobný	k2eAgFnSc7d1	pravděpodobná
příčinou	příčina	k1gFnSc7	příčina
neštěstí	neštěstí	k1gNnSc2	neštěstí
byl	být	k5eAaImAgInS	být
prudký	prudký	k2eAgInSc1d1	prudký
úhyb	úhyb	k1gInSc1	úhyb
pilota	pilot	k1gMnSc2	pilot
před	před	k7c7	před
mrakem	mrak	k1gInSc7	mrak
nebo	nebo	k8xC	nebo
meteorologickým	meteorologický	k2eAgInSc7d1	meteorologický
balónem	balón	k1gInSc7	balón
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
pádem	pád	k1gInSc7	pád
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
však	však	k9	však
protestovali	protestovat	k5eAaBmAgMnP	protestovat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
členové	člen	k1gMnPc1	člen
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
i	i	k9	i
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Závěry	závěr	k1gInPc1	závěr
komise	komise	k1gFnSc2	komise
byly	být	k5eAaImAgInP	být
odtajněny	odtajnit	k5eAaPmNgInP	odtajnit
až	až	k6eAd1	až
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
tiskem	tisk	k1gInSc7	tisk
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
několik	několik	k4yIc4	několik
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
pravděpodobných	pravděpodobný	k2eAgFnPc2d1	pravděpodobná
teorií	teorie	k1gFnPc2	teorie
snažících	snažící	k2eAgFnPc2d1	snažící
se	se	k3xPyFc4	se
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
neštěstí	neštěstí	k1gNnSc4	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
původní	původní	k2eAgFnSc2d1	původní
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Žukovského	Žukovského	k2eAgFnSc2d1	Žukovského
akademie	akademie	k1gFnSc2	akademie
Sergej	Sergej	k1gMnSc1	Sergej
Belocerkovskij	Belocerkovskij	k1gMnSc1	Belocerkovskij
a	a	k8xC	a
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Alexej	Alexej	k1gMnSc1	Alexej
Leonov	Leonov	k1gInSc4	Leonov
publikovali	publikovat	k5eAaBmAgMnP	publikovat
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
rozbor	rozbor	k1gInSc4	rozbor
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vyloučilo	vyloučit	k5eAaPmAgNnS	vyloučit
negativní	negativní	k2eAgNnSc1d1	negativní
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
posádky	posádka	k1gFnSc2	posádka
alkoholem	alkohol	k1gInSc7	alkohol
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgFnPc7d1	jiná
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
zavrhlo	zavrhnout	k5eAaPmAgNnS	zavrhnout
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
atentátu	atentát	k1gInSc2	atentát
nebo	nebo	k8xC	nebo
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
Dospělo	dochvít	k5eAaPmAgNnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
systémy	systém	k1gInPc1	systém
letadla	letadlo	k1gNnSc2	letadlo
fungovaly	fungovat	k5eAaImAgInP	fungovat
normálně	normálně	k6eAd1	normálně
a	a	k8xC	a
že	že	k8xS	že
nebyly	být	k5eNaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
žádné	žádný	k3yNgFnPc1	žádný
stopy	stopa	k1gFnPc1	stopa
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
cizím	cizí	k2eAgNnSc7d1	cizí
tělesem	těleso	k1gNnSc7	těleso
(	(	kIx(	(
<g/>
meteorologickým	meteorologický	k2eAgInSc7d1	meteorologický
balónem	balón	k1gInSc7	balón
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
<g/>
,	,	kIx,	,
jiným	jiný	k2eAgNnSc7d1	jiné
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
se	se	k3xPyFc4	se
během	během	k7c2	během
návratu	návrat	k1gInSc2	návrat
na	na	k7c4	na
letiště	letiště	k1gNnPc4	letiště
letoun	letoun	k1gMnSc1	letoun
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vývrtky	vývrtka	k1gFnSc2	vývrtka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylétnutí	vylétnutí	k1gNnSc6	vylétnutí
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
základny	základna	k1gFnSc2	základna
oblačnosti	oblačnost	k1gFnSc2	oblačnost
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnSc1d1	spodní
vrstva	vrstva	k1gFnSc1	vrstva
mraků	mrak	k1gInPc2	mrak
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
od	od	k7c2	od
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
do	do	k7c2	do
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
horní	horní	k2eAgMnSc1d1	horní
mezi	mezi	k7c7	mezi
4500	[number]	k4	4500
a	a	k8xC	a
5500	[number]	k4	5500
m	m	kA	m
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
piloti	pilot	k1gMnPc1	pilot
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
letadlo	letadlo	k1gNnSc4	letadlo
srovnat	srovnat	k5eAaPmF	srovnat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
malé	malý	k2eAgFnSc3d1	malá
výšce	výška	k1gFnSc3	výška
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
10.31	[number]	k4	10.31
narazili	narazit	k5eAaPmAgMnP	narazit
do	do	k7c2	do
země	zem	k1gFnSc2	zem
rychlostí	rychlost	k1gFnSc7	rychlost
190	[number]	k4	190
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
684	[number]	k4	684
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vývrtky	vývrtka	k1gFnSc2	vývrtka
podle	podle	k7c2	podle
Belocerkovského	Belocerkovský	k2eAgInSc2d1	Belocerkovský
a	a	k8xC	a
Leonova	Leonův	k2eAgInSc2d1	Leonův
letadlo	letadlo	k1gNnSc1	letadlo
upadlo	upadnout	k5eAaPmAgNnS	upadnout
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
vnější	vnější	k2eAgFnSc2d1	vnější
příčiny	příčina	k1gFnSc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
se	se	k3xPyFc4	se
piloti	pilot	k1gMnPc1	pilot
snažili	snažit	k5eAaImAgMnP	snažit
vyhnout	vyhnout	k5eAaPmF	vyhnout
hejnu	hejno	k1gNnSc3	hejno
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mraku	mrak	k1gInSc2	mrak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
hejno	hejno	k1gNnSc4	hejno
či	či	k8xC	či
za	za	k7c4	za
balón	balón	k1gInSc4	balón
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vlétli	vlétnout	k5eAaPmAgMnP	vlétnout
do	do	k7c2	do
úplavu	úplav	k1gInSc2	úplav
za	za	k7c7	za
letounem	letoun	k1gInSc7	letoun
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc2	Su-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
letadlo	letadlo	k1gNnSc4	letadlo
srazil	srazit	k5eAaPmAgInS	srazit
silný	silný	k2eAgInSc1d1	silný
poryv	poryv	k1gInSc1	poryv
větru	vítr	k1gInSc2	vítr
objevivší	objevivší	k2eAgFnSc2d1	objevivší
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přicházející	přicházející	k2eAgFnSc7d1	přicházející
studenou	studený	k2eAgFnSc7d1	studená
frontou	fronta	k1gFnSc7	fronta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Gagarinova	Gagarinův	k2eAgInSc2d1	Gagarinův
a	a	k8xC	a
Serjoginova	Serjoginův	k2eAgInSc2d1	Serjoginův
letu	let	k1gInSc2	let
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jiný	jiný	k2eAgInSc4d1	jiný
letoun	letoun	k1gInSc4	letoun
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgInS	podat
svědectví	svědectví	k1gNnSc4	svědectví
například	například	k6eAd1	například
další	další	k2eAgMnSc1d1	další
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Alexej	Alexej	k1gMnSc1	Alexej
Leonov	Leonovo	k1gNnPc2	Leonovo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
skupina	skupina	k1gFnSc1	skupina
hypotéz	hypotéza	k1gFnPc2	hypotéza
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
okolo	okolo	k7c2	okolo
selhání	selhání	k1gNnSc2	selhání
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Kuzněcov	Kuzněcov	k1gInSc4	Kuzněcov
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
velitel	velitel	k1gMnSc1	velitel
Střediska	středisko	k1gNnSc2	středisko
přípravy	příprava	k1gFnSc2	příprava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
,	,	kIx,	,
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
nehodu	nehoda	k1gFnSc4	nehoda
infarktem	infarkt	k1gInSc7	infarkt
Serjogina	Serjogino	k1gNnSc2	Serjogino
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
navalit	navalit	k5eAaPmF	navalit
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
znemožnit	znemožnit	k5eAaPmF	znemožnit
Gagarinovi	Gagarin	k1gMnSc3	Gagarin
zachránit	zachránit	k5eAaPmF	zachránit
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Vladimir	Vladimir	k1gMnSc1	Vladimir
Aksjonov	Aksjonov	k1gInSc4	Aksjonov
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
možnou	možný	k2eAgFnSc4d1	možná
ztrátu	ztráta	k1gFnSc4	ztráta
orientace	orientace	k1gFnSc2	orientace
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
se	se	k3xPyFc4	se
Serjogin	Serjogin	k1gMnSc1	Serjogin
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
rázně	rázně	k6eAd1	rázně
snížit	snížit	k5eAaPmF	snížit
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezorientoval	zorientovat	k5eNaPmAgMnS	zorientovat
se	se	k3xPyFc4	se
v	v	k7c6	v
mracích	mrak	k1gInPc6	mrak
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
spodní	spodní	k2eAgInSc4d1	spodní
okraj	okraj	k1gInSc4	okraj
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
nalézal	nalézat	k5eAaImAgMnS	nalézat
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
piloti	pilot	k1gMnPc1	pilot
před	před	k7c7	před
letem	let	k1gInSc7	let
informováni	informovat	k5eAaBmNgMnP	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
i	i	k8xC	i
Gagarinova	Gagarinův	k2eAgFnSc1d1	Gagarinova
pilotní	pilotní	k2eAgFnSc1d1	pilotní
chyba	chyba	k1gFnSc1	chyba
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
zatáčky	zatáčka	k1gFnSc2	zatáčka
<g/>
,	,	kIx,	,
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
podlehnutí	podlehnutí	k1gNnSc4	podlehnutí
iluzi	iluze	k1gFnSc4	iluze
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc4d1	spojená
se	se	k3xPyFc4	se
ztrátou	ztráta	k1gFnSc7	ztráta
prostorové	prostorový	k2eAgFnSc2d1	prostorová
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
význam	význam	k1gInSc1	význam
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
porušování	porušování	k1gNnSc1	porušování
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
předpisů	předpis	k1gInPc2	předpis
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
ztíženým	ztížený	k2eAgFnPc3d1	ztížená
povětrnostním	povětrnostní	k2eAgFnPc3d1	povětrnostní
podmínkám	podmínka	k1gFnPc3	podmínka
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
let	let	k1gInSc1	let
vůbec	vůbec	k9	vůbec
povolen	povolen	k2eAgMnSc1d1	povolen
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
tým	tým	k1gInSc4	tým
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
leteckých	letecký	k2eAgMnPc2d1	letecký
odborníků	odborník	k1gMnPc2	odborník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
leteckého	letecký	k2eAgMnSc2d1	letecký
inženýra	inženýr	k1gMnSc2	inženýr
Igora	Igor	k1gMnSc2	Igor
Kuzněcova	Kuzněcův	k2eAgMnSc2d1	Kuzněcův
neúspěšně	úspěšně	k6eNd1	úspěšně
požádal	požádat	k5eAaPmAgMnS	požádat
ruskou	ruský	k2eAgFnSc4d1	ruská
vládu	vláda	k1gFnSc4	vláda
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Kuzněcovův	Kuzněcovův	k2eAgInSc1d1	Kuzněcovův
tým	tým	k1gInSc1	tým
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgInS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
havárie	havárie	k1gFnSc2	havárie
stála	stát	k5eAaImAgFnS	stát
technická	technický	k2eAgFnSc1d1	technická
závada	závada	k1gFnSc1	závada
–	–	k?	–
nedovřený	dovřený	k2eNgInSc4d1	nedovřený
vyrovnávací	vyrovnávací	k2eAgInSc4d1	vyrovnávací
ventil	ventil	k1gInSc4	ventil
v	v	k7c6	v
kokpitu	kokpit	k1gInSc6	kokpit
letounu	letoun	k1gInSc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Kabina	kabina	k1gFnSc1	kabina
tak	tak	k9	tak
nebyla	být	k5eNaImAgFnS	být
hermeticky	hermeticky	k6eAd1	hermeticky
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
předpisy	předpis	k1gInPc7	předpis
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
předčasný	předčasný	k2eAgInSc4d1	předčasný
návrat	návrat	k1gInSc4	návrat
a	a	k8xC	a
klesání	klesání	k1gNnSc4	klesání
do	do	k7c2	do
2000	[number]	k4	2000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Serjogin	Serjogin	k1gMnSc1	Serjogin
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
oblibou	obliba	k1gFnSc7	obliba
razantních	razantní	k2eAgInPc2d1	razantní
manévrů	manévr	k1gInPc2	manévr
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgInS	zahájit
prudký	prudký	k2eAgInSc1d1	prudký
sestup	sestup	k1gInSc1	sestup
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
hypoxie	hypoxie	k1gFnSc1	hypoxie
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgNnPc4d1	následované
prudkým	prudký	k2eAgInSc7d1	prudký
nárůstem	nárůst	k1gInSc7	nárůst
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
a	a	k8xC	a
přetížením	přetížení	k1gNnSc7	přetížení
vedla	vést	k5eAaImAgFnS	vést
podle	podle	k7c2	podle
Kuzněcova	Kuzněcův	k2eAgNnSc2d1	Kuzněcův
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
schopnosti	schopnost	k1gFnSc2	schopnost
pilotů	pilot	k1gMnPc2	pilot
včas	včas	k6eAd1	včas
reagovat	reagovat	k5eAaBmF	reagovat
a	a	k8xC	a
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
letoun	letoun	k1gInSc4	letoun
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
teoriím	teorie	k1gFnPc3	teorie
bulvárního	bulvární	k2eAgInSc2d1	bulvární
charakteru	charakter	k1gInSc2	charakter
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
populární	populární	k2eAgNnSc4d1	populární
tvrzení	tvrzení	k1gNnSc4	tvrzení
o	o	k7c6	o
nehodě	nehoda	k1gFnSc6	nehoda
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
vypité	vypitý	k2eAgFnSc2d1	vypitá
vodky	vodka	k1gFnSc2	vodka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
konspirační	konspirační	k2eAgFnPc4d1	konspirační
teorie	teorie	k1gFnPc4	teorie
patří	patřit	k5eAaImIp3nP	patřit
tvrzení	tvrzení	k1gNnSc3	tvrzení
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
Gagarina	Gagarino	k1gNnSc2	Gagarino
a	a	k8xC	a
Serjogina	Serjogino	k1gNnSc2	Serjogino
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
při	při	k7c6	při
obletu	oblet	k1gInSc6	oblet
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1990	[number]	k4	1990
maďarský	maďarský	k2eAgMnSc1d1	maďarský
spisovatel	spisovatel	k1gMnSc1	spisovatel
István	István	k2eAgMnSc1d1	István
Nemere	Nemer	k1gInSc5	Nemer
prezentoval	prezentovat	k5eAaBmAgInS	prezentovat
sérii	série	k1gFnSc4	série
nedoložených	doložený	k2eNgFnPc2d1	nedoložená
domněnek	domněnka	k1gFnPc2	domněnka
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
o	o	k7c6	o
Gagarinovi	Gagarin	k1gMnSc6	Gagarin
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Zpochybnil	zpochybnit	k5eAaPmAgMnS	zpochybnit
jeho	jeho	k3xOp3gNnSc4	jeho
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
kosmu	kosmos	k1gInSc6	kosmos
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
hádce	hádka	k1gFnSc6	hádka
s	s	k7c7	s
Leonidem	Leonid	k1gMnSc7	Leonid
Břežněvem	Břežněv	k1gInSc7	Břežněv
zavřen	zavřen	k2eAgInSc1d1	zavřen
do	do	k7c2	do
blázince	blázinec	k1gInSc2	blázinec
a	a	k8xC	a
sprovozen	sprovodit	k5eAaPmNgMnS	sprovodit
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
č.	č.	k?	č.
1	[number]	k4	1
<g/>
"	"	kIx"	"
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
instruktorem	instruktor	k1gMnSc7	instruktor
na	na	k7c6	na
čestném	čestný	k2eAgNnSc6d1	čestné
pohřebišti	pohřebiště	k1gNnSc6	pohřebiště
u	u	k7c2	u
Kremelské	kremelský	k2eAgFnSc2d1	kremelská
zdi	zeď	k1gFnSc2	zeď
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
mezi	mezi	k7c7	mezi
nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
představiteli	představitel	k1gMnPc7	představitel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byla	být	k5eAaImAgFnS	být
urna	urna	k1gFnSc1	urna
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
Gagarina	Gagarina	k1gFnSc1	Gagarina
a	a	k8xC	a
Serjogina	Serjogina	k1gFnSc1	Serjogina
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
moskevském	moskevský	k2eAgInSc6d1	moskevský
Ústředním	ústřední	k2eAgInSc6d1	ústřední
domu	dům	k1gInSc6wR	dům
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
s	s	k7c7	s
mrtvými	mrtvý	k2eAgMnPc7d1	mrtvý
se	se	k3xPyFc4	se
přišlo	přijít	k5eAaPmAgNnS	přijít
rozloučit	rozloučit	k5eAaPmF	rozloučit
na	na	k7c4	na
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Gagarinově	Gagarinův	k2eAgFnSc6d1	Gagarinova
smrti	smrt	k1gFnSc6	smrt
vdova	vdova	k1gFnSc1	vdova
až	až	k8xS	až
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
pracovala	pracovat	k5eAaImAgFnS	pracovat
ve	v	k7c6	v
Hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
městečku	městečko	k1gNnSc6	městečko
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zde	zde	k6eAd1	zde
i	i	k9	i
na	na	k7c4	na
důchod	důchod	k1gInSc4	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
Jelena	Jelena	k1gFnSc1	Jelena
Jurjevna	Jurjevna	k1gFnSc1	Jurjevna
Gagarinová	Gagarinová	k1gFnSc1	Gagarinová
<g/>
,	,	kIx,	,
historička	historička	k1gFnSc1	historička
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
desátým	desátý	k4xOgNnSc7	desátý
rokem	rok	k1gInSc7	rok
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ředitelky	ředitelka	k1gFnSc2	ředitelka
muzea	muzeum	k1gNnSc2	muzeum
"	"	kIx"	"
<g/>
Moskevský	moskevský	k2eAgInSc1d1	moskevský
Kreml	Kreml	k1gInSc1	Kreml
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
Galina	Galina	k1gFnSc1	Galina
Jurjevna	Jurjevna	k1gFnSc1	Jurjevna
Gagarinová	Gagarinová	k1gFnSc1	Gagarinová
přednášela	přednášet	k5eAaImAgFnS	přednášet
na	na	k7c6	na
Plechanovově	Plechanovův	k2eAgFnSc6d1	Plechanovův
ruské	ruský	k2eAgFnSc6d1	ruská
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Ю	Ю	k?	Ю
А	А	k?	А
<g/>
.	.	kIx.	.
Д	Д	k?	Д
в	в	k?	в
к	к	k?	к
<g/>
.	.	kIx.	.
З	З	k?	З
л	л	k?	л
С	С	k?	С
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Н	Н	k?	Н
К	К	k?	К
<g/>
;	;	kIx,	;
literární	literární	k2eAgInSc1d1	literární
zápis	zápis	k1gInSc1	zápis
Н	Н	k?	Н
Д	Д	k?	Д
<g/>
,	,	kIx,	,
С	С	k?	С
Б	Б	k?	Б
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
М	М	k?	М
:	:	kIx,	:
П	П	k?	П
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
GAGARIN	GAGARIN	kA	GAGARIN
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
<g/>
.	.	kIx.	.
</s>
<s>
Moje	můj	k3xOp1gFnSc1	můj
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Věra	Věra	k1gFnSc1	Věra
Prokopová-Petříková	Prokopová-Petříková	k1gFnSc1	Prokopová-Petříková
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
133	[number]	k4	133
s.	s.	k?	s.
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Ю	Ю	k?	Ю
А	А	k?	А
<g/>
.	.	kIx.	.
В	В	k?	В
З	З	k?	З
<g/>
...	...	k?	...
Д	Д	k?	Д
р	р	k?	р
<g/>
..	..	k?	..
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Н	Н	k?	Н
К	К	k?	К
<g/>
;	;	kIx,	;
redakce	redakce	k1gFnSc1	redakce
Ю	Ю	k?	Ю
Д	Д	k?	Д
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
М	М	k?	М
:	:	kIx,	:
Д	Д	k?	Д
л	л	k?	л
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Ю	Ю	k?	Ю
А	А	k?	А
<g/>
;	;	kIx,	;
Л	Л	k?	Л
<g/>
,	,	kIx,	,
В	В	k?	В
И	И	k?	И
<g/>
.	.	kIx.	.
П	П	k?	П
и	и	k?	и
к	к	k?	к
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
М	М	k?	М
:	:	kIx,	:
М	М	k?	М
г	г	k?	г
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
djvu	djv	k1gMnSc3	djv
<g/>
.	.	kIx.	.
</s>
<s>
GAGARIN	GAGARIN	kA	GAGARIN
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
<g/>
;	;	kIx,	;
LEBEDĚV	LEBEDĚV	kA	LEBEDĚV
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
:	:	kIx,	:
Psychologie	psychologie	k1gFnSc1	psychologie
a	a	k8xC	a
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jemelka	Jemelka	k1gMnSc1	Jemelka
<g/>
;	;	kIx,	;
Předmluva	předmluva	k1gFnSc1	předmluva
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Codr	Codr	k1gMnSc1	Codr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Pragopress	Pragopress	k1gInSc1	Pragopress
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
220	[number]	k4	220
s.	s.	k?	s.
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Ю	Ю	k?	Ю
А	А	k?	А
<g/>
.	.	kIx.	.
Е	Е	k?	Е
п	п	k?	п
<g/>
!	!	kIx.	!
</s>
<s>
С	С	k?	С
<g/>
.	.	kIx.	.
Р	Р	k?	Р
<g/>
.	.	kIx.	.
П	П	k?	П
<g/>
.	.	kIx.	.
И	И	k?	И
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgFnPc4d1	doplněná
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
М	М	k?	М
:	:	kIx,	:
М	М	k?	М
Г	Г	k?	Г
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
191	[number]	k4	191
s.	s.	k?	s.
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1957	[number]	k4	1957
poručík	poručík	k1gMnSc1	poručík
(	(	kIx(	(
<g/>
л	л	k?	л
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
nadporučík	nadporučík	k1gMnSc1	nadporučík
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
с	с	k?	с
л	л	k?	л
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
major	major	k1gMnSc1	major
(	(	kIx(	(
<g/>
м	м	k?	м
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povýšen	povýšen	k2eAgMnSc1d1	povýšen
mimořádně	mimořádně	k6eAd1	mimořádně
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
hodnosti	hodnost	k1gFnPc4	hodnost
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1962	[number]	k4	1962
podplukovník	podplukovník	k1gMnSc1	podplukovník
(	(	kIx(	(
<g/>
п	п	k?	п
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
plukovník	plukovník	k1gMnSc1	plukovník
(	(	kIx(	(
<g/>
п	п	k?	п
<g/>
)	)	kIx)	)
Po	po	k7c6	po
letu	let	k1gInSc6	let
Gagarin	Gagarin	k1gInSc1	Gagarin
obdržel	obdržet	k5eAaPmAgInS	obdržet
řadu	řada	k1gFnSc4	řada
<g />
.	.	kIx.	.
</s>
<s>
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
titulů	titul	k1gInPc2	titul
<g/>
:	:	kIx,	:
Hrdina	Hrdina	k1gMnSc1	Hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
Leninův	Leninův	k2eAgInSc1d1	Leninův
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Hrdina	Hrdina	k1gMnSc1	Hrdina
socialistické	socialistický	k2eAgFnSc2d1	socialistická
práce	práce	k1gFnSc2	práce
BLR	BLR	kA	BLR
a	a	k8xC	a
řád	řád	k1gInSc1	řád
Georgije	Georgije	k1gFnSc2	Georgije
Dimitrova	Dimitrův	k2eAgInSc2d1	Dimitrův
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Hrdina	Hrdina	k1gMnSc1	Hrdina
socialistické	socialistický	k2eAgFnSc2d1	socialistická
práce	práce	k1gFnSc2	práce
ČSSR	ČSSR	kA	ČSSR
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Grunwaldského	Grunwaldský	k2eAgInSc2d1	Grunwaldský
kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
polský	polský	k2eAgInSc1d1	polský
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Playa	Playa	k1gMnSc1	Playa
Giron	Giron	k1gMnSc1	Giron
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kubánský	kubánský	k2eAgMnSc1d1	kubánský
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzduchoplavby	vzduchoplavba	k1gFnSc2	vzduchoplavba
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
brazilský	brazilský	k2eAgInSc1d1	brazilský
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgInSc1d1	maďarský
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Náhrdelník	náhrdelník	k1gInSc1	náhrdelník
Nilu	Nil	k1gInSc2	Nil
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
egyptský	egyptský	k2eAgInSc1d1	egyptský
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
stuha	stuha	k1gFnSc1	stuha
africké	africký	k2eAgFnSc2d1	africká
hvězdy	hvězda	k1gFnSc2	hvězda
k	k	k7c3	k
řádu	řád	k1gInSc3	řád
"	"	kIx"	"
<g/>
Světlo	světlo	k1gNnSc1	světlo
v	v	k7c6	v
temnotě	temnota	k1gFnSc6	temnota
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
liberijský	liberijský	k2eAgMnSc1d1	liberijský
<g/>
)	)	kIx)	)
Hrdina	Hrdina	k1gMnSc1	Hrdina
práce	práce	k1gFnSc2	práce
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
A	a	k9	a
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
medailí	medaile	k1gFnPc2	medaile
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Gagarin	Gagarin	k1gInSc1	Gagarin
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
řady	řada	k1gFnSc2	řada
sovětských	sovětský	k2eAgInPc2d1	sovětský
(	(	kIx(	(
<g/>
Kaluga	Kaluga	k1gFnSc1	Kaluga
<g/>
,	,	kIx,	,
Komsomolsk	Komsomolsk	k1gInSc1	Komsomolsk
na	na	k7c6	na
Amuru	Amur	k1gInSc6	Amur
<g/>
,	,	kIx,	,
Ljuberci	Ljuberec	k1gInSc6	Ljuberec
<g/>
,	,	kIx,	,
Novočerkassk	Novočerkassk	k1gInSc1	Novočerkassk
<g/>
,	,	kIx,	,
Saratov	Saratov	k1gInSc1	Saratov
<g/>
,	,	kIx,	,
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
,	,	kIx,	,
Smolensk	Smolensk	k1gInSc1	Smolensk
<g/>
,	,	kIx,	,
Sumgait	Sumgait	k1gInSc1	Sumgait
<g/>
,	,	kIx,	,
Ťumeň	Ťumeň	k1gFnSc1	Ťumeň
<g/>
,	,	kIx,	,
Vinnica	Vinnica	k1gFnSc1	Vinnica
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
(	(	kIx(	(
<g/>
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
Trenčianských	trenčianský	k2eAgFnPc2d1	Trenčianská
Teplic	Teplice	k1gFnPc2	Teplice
<g/>
)	)	kIx)	)
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Gagarin	Gagarin	k1gInSc1	Gagarin
<g/>
,	,	kIx,	,
za	za	k7c2	za
života	život	k1gInSc2	život
symbol	symbol	k1gInSc1	symbol
úspěchů	úspěch	k1gInPc2	úspěch
sovětské	sovětský	k2eAgFnSc2d1	sovětská
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
a	a	k8xC	a
zdroj	zdroj	k1gInSc1	zdroj
hrdosti	hrdost	k1gFnSc2	hrdost
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
jím	on	k3xPp3gNnSc7	on
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
Gagarinovy	Gagarinův	k2eAgFnSc2d1	Gagarinova
památky	památka	k1gFnSc2	památka
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
muzea	muzeum	k1gNnPc1	muzeum
a	a	k8xC	a
památníky	památník	k1gInPc1	památník
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
město	město	k1gNnSc1	město
Gžatsk	Gžatsk	k1gInSc1	Gžatsk
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnPc1	náměstí
<g/>
,	,	kIx,	,
bulváry	bulvár	k1gInPc1	bulvár
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
a	a	k8xC	a
parky	park	k1gInPc1	park
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
sovětských	sovětský	k2eAgNnPc2d1	sovětské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
představoval	představovat	k5eAaImAgMnS	představovat
ideálního	ideální	k2eAgMnSc4d1	ideální
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
udržovala	udržovat	k5eAaImAgFnS	udržovat
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c6	o
Gagarinově	Gagarinův	k2eAgNnSc6d1	Gagarinovo
prvenství	prvenství	k1gNnSc6	prvenství
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
vycházely	vycházet	k5eAaImAgFnP	vycházet
vzpomínkové	vzpomínkový	k2eAgFnPc1d1	vzpomínková
knihy	kniha	k1gFnPc1	kniha
a	a	k8xC	a
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
Gagarinův	Gagarinův	k2eAgInSc1d1	Gagarinův
portrét	portrét	k1gInSc1	portrét
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
na	na	k7c6	na
poštovních	poštovní	k2eAgFnPc6d1	poštovní
známkách	známka	k1gFnPc6	známka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jméno	jméno	k1gNnSc1	jméno
Jurij	Jurij	k1gFnPc2	Jurij
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
načas	načas	k6eAd1	načas
populární	populární	k2eAgMnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
prvního	první	k4xOgInSc2	první
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
získala	získat	k5eAaPmAgFnS	získat
trvalý	trvalý	k2eAgInSc4d1	trvalý
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
čtyřiatřiceti	čtyřiatřicet	k4xCc6	čtyřiatřicet
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
lidí	člověk	k1gMnPc2	člověk
zůstal	zůstat	k5eAaPmAgInS	zůstat
jako	jako	k9	jako
věčně	věčně	k6eAd1	věčně
mladý	mladý	k2eAgMnSc1d1	mladý
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
přátelský	přátelský	k2eAgInSc1d1	přátelský
<g/>
,	,	kIx,	,
s	s	k7c7	s
typickým	typický	k2eAgInSc7d1	typický
úsměvem	úsměv	k1gInSc7	úsměv
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
ze	z	k7c2	z
zapadlé	zapadlý	k2eAgFnSc2d1	zapadlá
vesnice	vesnice	k1gFnSc2	vesnice
aby	aby	kYmCp3nS	aby
doletěl	doletět	k5eAaPmAgInS	doletět
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lidovým	lidový	k2eAgMnSc7d1	lidový
hrdinou	hrdina	k1gMnSc7	hrdina
a	a	k8xC	a
<g/>
,	,	kIx,	,
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
k	k	k7c3	k
jiným	jiný	k2eAgMnPc3d1	jiný
sovětským	sovětský	k2eAgMnPc3d1	sovětský
hrdinům	hrdina	k1gMnPc3	hrdina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
popularita	popularita	k1gFnSc1	popularita
nezeslábla	zeslábnout	k5eNaPmAgFnS	zeslábnout
ani	ani	k8xC	ani
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nynějším	nynější	k2eAgNnSc6d1	nynější
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
populárnější	populární	k2eAgFnSc1d2	populárnější
než	než	k8xS	než
kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
jiný	jiný	k2eAgInSc1d1	jiný
idol	idol	k1gInSc1	idol
<g/>
,	,	kIx,	,
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
porazil	porazit	k5eAaPmAgMnS	porazit
i	i	k9	i
Vladimira	Vladimir	k1gMnSc4	Vladimir
Vysockého	vysocký	k2eAgMnSc4d1	vysocký
<g/>
.	.	kIx.	.
</s>
<s>
Respekt	respekt	k1gInSc1	respekt
ke	k	k7c3	k
Gagarinovi	Gagarin	k1gMnSc3	Gagarin
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k9	i
mimo	mimo	k7c4	mimo
jeho	jeho	k3xOp3gFnSc4	jeho
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
angolském	angolský	k2eAgNnSc6d1	angolské
městě	město	k1gNnSc6	město
Namibe	Namib	k1gInSc5	Namib
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prvního	první	k4xOgMnSc4	první
z	z	k7c2	z
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
nezapomínají	zapomínat	k5eNaImIp3nP	zapomínat
ani	ani	k9	ani
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
kolegové	kolega	k1gMnPc1	kolega
:	:	kIx,	:
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
Gagarinově	Gagarinův	k2eAgFnSc6d1	Gagarinova
smrti	smrt	k1gFnSc6	smrt
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
Středisko	středisko	k1gNnSc1	středisko
přípravy	příprava	k1gFnSc2	příprava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
v	v	k7c6	v
Hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
městečku	městečko	k1gNnSc6	městečko
a	a	k8xC	a
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
letecká	letecký	k2eAgFnSc1d1	letecká
akademie	akademie	k1gFnSc1	akademie
v	v	k7c6	v
Moninu	Monino	k1gNnSc6	Monino
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
město	město	k1gNnSc1	město
Gžatsk	Gžatsk	k1gInSc1	Gžatsk
poblíž	poblíž	k6eAd1	poblíž
Gagarinovy	Gagarinův	k2eAgFnSc2d1	Gagarinova
rodné	rodný	k2eAgFnSc2d1	rodná
vesnice	vesnice	k1gFnSc2	vesnice
nese	nést	k5eAaImIp3nS	nést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
jméno	jméno	k1gNnSc4	jméno
Gagarin	Gagarin	k1gInSc1	Gagarin
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
náměstí	náměstí	k1gNnSc1	náměstí
Kalužské	Kalužský	k2eAgFnSc2d1	Kalužská
stráže	stráž	k1gFnSc2	stráž
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Gagarinovo	Gagarinův	k2eAgNnSc4d1	Gagarinovo
a	a	k8xC	a
o	o	k7c4	o
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
vztyčen	vztyčit	k5eAaPmNgInS	vztyčit
46	[number]	k4	46
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
památník	památník	k1gInSc4	památník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
muzeum	muzeum	k1gNnSc4	muzeum
byl	být	k5eAaImAgInS	být
přeměněn	přeměněn	k2eAgInSc1d1	přeměněn
Gagarinův	Gagarinův	k2eAgInSc1d1	Gagarinův
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
v	v	k7c4	v
Klušinu	Klušina	k1gFnSc4	Klušina
<g/>
,	,	kIx,	,
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
žili	žít	k5eAaImAgMnP	žít
Gagarinovi	Gagarinův	k2eAgMnPc1d1	Gagarinův
v	v	k7c6	v
Gžatsku	Gžatsko	k1gNnSc6	Gžatsko
i	i	k8xC	i
domy	dům	k1gInPc4	dům
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
bydlel	bydlet	k5eAaImAgMnS	bydlet
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
služby	služba	k1gFnSc2	služba
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
Hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
městečku	městečko	k1gNnSc6	městečko
i	i	k8xC	i
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pádu	pád	k1gInSc2	pád
jeho	on	k3xPp3gNnSc2	on
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
památníky	památník	k1gInPc1	památník
a	a	k8xC	a
sochy	socha	k1gFnPc1	socha
připomínající	připomínající	k2eAgFnPc1d1	připomínající
Gagarina	Gagarino	k1gNnPc1	Gagarino
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
i	i	k9	i
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ljubercích	Ljuberk	k1gInPc6	Ljuberk
<g/>
,	,	kIx,	,
Kolomně	Kolomně	k1gFnSc4	Kolomně
<g/>
,	,	kIx,	,
Soči	Soči	k1gNnSc4	Soči
<g/>
,	,	kIx,	,
Erfurtu	Erfurta	k1gFnSc4	Erfurta
<g/>
...	...	k?	...
Jediná	jediný	k2eAgFnSc1d1	jediná
socha	socha	k1gFnSc1	socha
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
odhalen	odhalit	k5eAaPmNgInS	odhalit
první	první	k4xOgInSc1	první
památník	památník	k1gInSc1	památník
Jurije	Jurije	k1gMnSc2	Jurije
Gagarina	Gagarin	k1gMnSc2	Gagarin
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Gagarinovo	Gagarinův	k2eAgNnSc1d1	Gagarinovo
jméno	jméno	k1gNnSc1	jméno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
nosí	nosit	k5eAaImIp3nS	nosit
i	i	k9	i
saratovská	saratovský	k2eAgFnSc1d1	saratovský
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
letecký	letecký	k2eAgInSc1d1	letecký
závod	závod	k1gInSc1	závod
v	v	k7c6	v
Komsomolsku	Komsomolsek	k1gInSc6	Komsomolsek
na	na	k7c6	na
Amuru	Amur	k1gInSc6	Amur
–	–	k?	–
Komsomolskoamurské	komsomolskoamurský	k2eAgNnSc1d1	komsomolskoamurský
letecké	letecký	k2eAgNnSc1d1	letecké
výrobní	výrobní	k2eAgNnSc1d1	výrobní
sdružení	sdružení	k1gNnSc1	sdružení
J.	J.	kA	J.
A.	A.	kA	A.
Gagarina	Gagarina	k1gMnSc1	Gagarina
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
ročníkem	ročník	k1gInSc7	ročník
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
vítěz	vítěz	k1gMnSc1	vítěz
Kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
dostává	dostávat	k5eAaImIp3nS	dostávat
Gagarinův	Gagarinův	k2eAgInSc4d1	Gagarinův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Kosmonavt	Kosmonavt	k1gInSc1	Kosmonavt
Jurij	Jurij	k1gMnSc1	Jurij
Gagarin	Gagarin	k1gInSc1	Gagarin
<g/>
"	"	kIx"	"
nesla	nést	k5eAaImAgFnS	nést
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vědeckovýzkumná	vědeckovýzkumný	k2eAgFnSc1d1	vědeckovýzkumná
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
především	především	k9	především
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
kosmickými	kosmický	k2eAgFnPc7d1	kosmická
loděmi	loď	k1gFnPc7	loď
a	a	k8xC	a
stanicemi	stanice	k1gFnPc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
č.	č.	k?	č.
(	(	kIx(	(
<g/>
1772	[number]	k4	1772
<g/>
)	)	kIx)	)
Gagarin	Gagarin	k1gInSc1	Gagarin
a	a	k8xC	a
kráter	kráter	k1gInSc1	kráter
Gagarin	Gagarin	k1gInSc1	Gagarin
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
Gagarinova	Gagarinův	k2eAgInSc2d1	Gagarinův
letu	let	k1gInSc2	let
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
TMA-21	TMA-21	k1gFnSc2	TMA-21
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
"	"	kIx"	"
<g/>
Jurij	Jurij	k1gFnSc1	Jurij
Gagarin	Gagarin	k1gInSc1	Gagarin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jediným	jediný	k2eAgInSc7d1	jediný
Sojuzem	Sojuz	k1gInSc7	Sojuz
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
Gagarinův	Gagarinův	k2eAgInSc1d1	Gagarinův
portrét	portrét	k1gInSc1	portrét
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
známkách	známka	k1gFnPc6	známka
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
sovětská	sovětský	k2eAgFnSc1d1	sovětská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
československá	československý	k2eAgFnSc1d1	Československá
pošta	pošta	k1gFnSc1	pošta
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
už	už	k6eAd1	už
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
vydat	vydat	k5eAaPmF	vydat
známky	známka	k1gFnPc4	známka
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
letu	let	k1gInSc3	let
člověka	člověk	k1gMnSc2	člověk
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
pamětních	pamětní	k2eAgNnPc2d1	pamětní
razítek	razítko	k1gNnPc2	razítko
a	a	k8xC	a
obálek	obálka	k1gFnPc2	obálka
s	s	k7c7	s
gagarinovskými	gagarinovský	k2eAgInPc7d1	gagarinovský
náměty	námět	k1gInPc7	námět
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
tématem	téma	k1gNnSc7	téma
několika	několik	k4yIc3	několik
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Gagarin	Gagarin	k1gInSc4	Gagarin
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
připomínán	připomínat	k5eAaImNgInS	připomínat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
oslavná	oslavný	k2eAgFnSc1d1	oslavná
píseň	píseň	k1gFnSc1	píseň
už	už	k6eAd1	už
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	on	k3xPp3gInSc2	on
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Pozdrav	pozdrav	k1gInSc1	pozdrav
astronautovi	astronaut	k1gMnSc6	astronaut
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
známější	známý	k2eAgMnPc1d2	známější
pod	pod	k7c7	pod
nepůvodním	původní	k2eNgInSc7d1	nepůvodní
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
majore	major	k1gMnSc5	major
Gagarine	Gagarin	k1gInSc5	Gagarin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
brněnský	brněnský	k2eAgMnSc1d1	brněnský
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Gustav	Gustav	k1gMnSc1	Gustav
Brom	brom	k1gInSc4	brom
<g/>
,	,	kIx,	,
narychlo	narychlo	k6eAd1	narychlo
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
prvního	první	k4xOgInSc2	první
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
složili	složit	k5eAaPmAgMnP	složit
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hnilička	hnilička	k1gFnSc1	hnilička
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Pácl	Pácl	k1gMnSc1	Pácl
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
populární	populární	k2eAgFnSc2d1	populární
jak	jak	k8xC	jak
melodií	melodie	k1gFnPc2	melodie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
silná	silný	k2eAgFnSc1d1	silná
vlna	vlna	k1gFnSc1	vlna
připomínání	připomínání	k1gNnSc4	připomínání
Gagarina	Gagarin	k1gMnSc2	Gagarin
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
padesátým	padesátý	k4xOgNnSc7	padesátý
výročím	výročí	k1gNnSc7	výročí
jeho	on	k3xPp3gInSc2	on
letu	let	k1gInSc2	let
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vláda	vláda	k1gFnSc1	vláda
připravovala	připravovat	k5eAaImAgFnS	připravovat
oslavy	oslava	k1gFnPc4	oslava
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
množství	množství	k1gNnSc1	množství
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
shromáždění	shromáždění	k1gNnSc1	shromáždění
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
představitelů	představitel	k1gMnPc2	představitel
ruské	ruský	k2eAgFnSc2d1	ruská
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
a	a	k8xC	a
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Kremlu	Kreml	k1gInSc6	Kreml
<g/>
,	,	kIx,	,
zřízení	zřízení	k1gNnSc6	zřízení
Gagarinovy	Gagarinův	k2eAgFnSc2d1	Gagarinova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
renovace	renovace	k1gFnSc2	renovace
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
rozhovory	rozhovor	k1gInPc4	rozhovor
a	a	k8xC	a
soutěže	soutěž	k1gFnPc4	soutěž
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
setkání	setkání	k1gNnSc6	setkání
a	a	k8xC	a
konference	konference	k1gFnSc2	konference
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Gagarinovi	Gagarin	k1gMnSc3	Gagarin
a	a	k8xC	a
kosmonautice	kosmonautika	k1gFnSc3	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nové	nový	k2eAgNnSc1d1	nové
vydání	vydání	k1gNnSc1	vydání
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
biografie	biografie	k1gFnPc1	biografie
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Lva	Lev	k1gMnSc2	Lev
Danilkina	Danilkin	k1gMnSc2	Danilkin
<g/>
,	,	kIx,	,
podrobný	podrobný	k2eAgInSc4d1	podrobný
rozbor	rozbor	k1gInSc4	rozbor
letu	let	k1gInSc2	let
108	[number]	k4	108
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
změnily	změnit	k5eAaPmAgFnP	změnit
svět	svět	k1gInSc4	svět
Antona	Anton	k1gMnSc4	Anton
Pervušina	Pervušin	k2eAgFnSc1d1	Pervušina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
Gagarina	Gagarino	k1gNnPc4	Gagarino
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vlny	vlna	k1gFnSc2	vlna
článků	článek	k1gInPc2	článek
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
"	"	kIx"	"
<g/>
Noc	noc	k1gFnSc4	noc
Jurije	Jurije	k1gMnSc2	Jurije
Gagarina	Gagarin	k1gMnSc2	Gagarin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
se	se	k3xPyFc4	se
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
akce	akce	k1gFnSc1	akce
u	u	k7c2	u
Gagarinovy	Gagarinův	k2eAgFnSc2d1	Gagarinova
sochy	socha	k1gFnSc2	socha
na	na	k7c6	na
karlovarském	karlovarský	k2eAgNnSc6d1	Karlovarské
letišti	letiště	k1gNnSc6	letiště
a	a	k8xC	a
Štefánikova	Štefánikův	k2eAgFnSc1d1	Štefánikova
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
připravila	připravit	k5eAaPmAgFnS	připravit
výstavu	výstava	k1gFnSc4	výstava
s	s	k7c7	s
replikou	replika	k1gFnSc7	replika
Vostoku	Vostok	k1gInSc2	Vostok
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
