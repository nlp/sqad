<s>
Strach	strach	k1gInSc1	strach
je	být	k5eAaImIp3nS	být
emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc1d1	vznikající
jako	jako	k8xC	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
hrozící	hrozící	k2eAgNnSc4d1	hrozící
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
ho	on	k3xPp3gInSc4	on
neurovegetativní	urovegetativní	k2eNgInSc4d1	neurovegetativní
projevy	projev	k1gInPc4	projev
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
zblednutí	zblednutí	k1gNnSc1	zblednutí
<g/>
,	,	kIx,	,
chvění	chvění	k1gNnSc1	chvění
<g/>
,	,	kIx,	,
zrychlené	zrychlený	k2eAgNnSc1d1	zrychlené
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
,	,	kIx,	,
bušení	bušení	k1gNnSc1	bušení
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
husí	husí	k2eAgFnSc2d1	husí
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
normální	normální	k2eAgFnSc4d1	normální
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
skutečné	skutečný	k2eAgNnSc4d1	skutečné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
nebo	nebo	k8xC	nebo
ohrožení	ohrožení	k1gNnSc4	ohrožení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jedince	jedinec	k1gMnSc4	jedinec
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
,	,	kIx,	,
únik	únik	k1gInSc4	únik
nebo	nebo	k8xC	nebo
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
motivuje	motivovat	k5eAaBmIp3nS	motivovat
k	k	k7c3	k
vyhnutí	vyhnutí	k1gNnSc3	vyhnutí
se	se	k3xPyFc4	se
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
nebo	nebo	k8xC	nebo
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
často	často	k6eAd1	často
strach	strach	k1gInSc1	strach
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c6	na
agresi	agrese	k1gFnSc6	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc4	strach
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
na	na	k7c4	na
atavistický	atavistický	k2eAgInSc4d1	atavistický
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vrozený	vrozený	k2eAgInSc1d1	vrozený
(	(	kIx(	(
<g/>
strach	strach	k1gInSc1	strach
z	z	k7c2	z
bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
ze	z	k7c2	z
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
z	z	k7c2	z
některých	některý	k3yIgNnPc2	některý
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
a	a	k8xC	a
naučený	naučený	k2eAgInSc1d1	naučený
(	(	kIx(	(
<g/>
strach	strach	k1gInSc1	strach
ze	z	k7c2	z
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
osamělosti	osamělost	k1gFnSc2	osamělost
<g/>
,	,	kIx,	,
ztráty	ztráta	k1gFnPc1	ztráta
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc4d1	různý
stupně	stupeň	k1gInPc4	stupeň
podle	podle	k7c2	podle
intenzity	intenzita	k1gFnSc2	intenzita
<g/>
:	:	kIx,	:
obava	obava	k1gFnSc1	obava
<g/>
,	,	kIx,	,
bázeň	bázeň	k1gFnSc1	bázeň
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lze	lze	k6eAd1	lze
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
překonat	překonat	k5eAaPmF	překonat
hrůza	hrůza	k1gFnSc1	hrůza
<g/>
,	,	kIx,	,
děs	děs	k1gInSc1	děs
<g/>
,	,	kIx,	,
zděšení	zděšení	k1gNnSc1	zděšení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
strach	strach	k1gInSc1	strach
vystupňovaný	vystupňovaný	k2eAgInSc1d1	vystupňovaný
v	v	k7c4	v
afekt	afekt	k1gInSc4	afekt
Strach	strach	k1gInSc1	strach
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
odlišovat	odlišovat	k5eAaImF	odlišovat
od	od	k7c2	od
úzkosti	úzkost	k1gFnSc2	úzkost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
delší	dlouhý	k2eAgNnSc4d2	delší
trvání	trvání	k1gNnSc4	trvání
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
jasné	jasný	k2eAgNnSc1d1	jasné
zacílení	zacílení	k1gNnSc1	zacílení
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
trpící	trpící	k2eAgFnSc1d1	trpící
úzkostí	úzkost	k1gFnSc7	úzkost
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přesně	přesně	k6eAd1	přesně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
formou	forma	k1gFnSc7	forma
strachu	strach	k1gInSc2	strach
je	být	k5eAaImIp3nS	být
tréma	tréma	k1gFnSc1	tréma
<g/>
,	,	kIx,	,
strach	strach	k1gInSc1	strach
z	z	k7c2	z
neúspěchu	neúspěch	k1gInSc2	neúspěch
a	a	k8xC	a
veřejného	veřejný	k2eAgNnSc2d1	veřejné
ponížení	ponížení	k1gNnSc2	ponížení
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
jako	jako	k8xC	jako
emoce	emoce	k1gFnSc1	emoce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
přenosný	přenosný	k2eAgInSc4d1	přenosný
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
zalidněné	zalidněný	k2eAgFnSc6d1	zalidněná
prostoře	prostora	k1gFnSc6	prostora
(	(	kIx(	(
<g/>
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
strach	strach	k1gInSc1	strach
rychle	rychle	k6eAd1	rychle
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
panika	panika	k1gFnSc1	panika
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
pak	pak	k6eAd1	pak
často	často	k6eAd1	často
prchají	prchat	k5eAaImIp3nP	prchat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
před	před	k7c7	před
čím	co	k3yInSc7	co
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
také	také	k9	také
podporuje	podporovat	k5eAaImIp3nS	podporovat
sugestibilitu	sugestibilita	k1gFnSc4	sugestibilita
<g/>
,	,	kIx,	,
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
oklamou	oklamat	k5eAaPmIp3nP	oklamat
jeho	jeho	k3xOp3gInPc4	jeho
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidí	vidět	k5eAaImIp3nS	vidět
nebo	nebo	k8xC	nebo
slyší	slyšet	k5eAaImIp3nS	slyšet
něco	něco	k6eAd1	něco
hrůzného	hrůzný	k2eAgInSc2d1	hrůzný
–	–	k?	–
rčení	rčení	k1gNnSc1	rčení
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
strach	strach	k1gInSc1	strach
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgFnPc4d1	velká
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
sugestibilita	sugestibilita	k1gFnSc1	sugestibilita
strachu	strach	k1gInSc2	strach
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
kořenů	kořen	k1gInPc2	kořen
vzniku	vznik	k1gInSc2	vznik
různých	různý	k2eAgFnPc2d1	různá
pověstí	pověst	k1gFnPc2	pověst
<g/>
,	,	kIx,	,
strašidelných	strašidelný	k2eAgInPc2d1	strašidelný
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
historek	historka	k1gFnPc2	historka
o	o	k7c6	o
zjevení	zjevení	k1gNnSc6	zjevení
apod.	apod.	kA	apod.
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
chorobné	chorobný	k2eAgFnPc1d1	chorobná
formy	forma	k1gFnPc1	forma
strachu	strach	k1gInSc2	strach
–	–	k?	–
nepotlačitelný	potlačitelný	k2eNgInSc1d1	nepotlačitelný
strach	strach	k1gInSc1	strach
z	z	k7c2	z
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
situací	situace	k1gFnPc2	situace
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
fobie	fobie	k1gFnSc1	fobie
(	(	kIx(	(
<g/>
klaustrofobie	klaustrofobie	k1gFnSc1	klaustrofobie
–	–	k?	–
strach	strach	k1gInSc4	strach
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
arachnofobie	arachnofobie	k1gFnSc1	arachnofobie
–	–	k?	–
strach	strach	k1gInSc1	strach
z	z	k7c2	z
pavouků	pavouk	k1gMnPc2	pavouk
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
od	od	k7c2	od
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
úlekových	úlekův	k2eAgFnPc2d1	úlekův
reakcí	reakce	k1gFnPc2	reakce
kojence	kojenec	k1gMnSc2	kojenec
<g/>
,	,	kIx,	,
k	k	k7c3	k
reakcím	reakce	k1gFnPc3	reakce
na	na	k7c4	na
složitější	složitý	k2eAgFnPc4d2	složitější
situace	situace	k1gFnPc4	situace
(	(	kIx(	(
<g/>
neznámé	známý	k2eNgNnSc4d1	neznámé
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
smyslový	smyslový	k2eAgInSc4d1	smyslový
podnět	podnět	k1gInSc4	podnět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
prožitými	prožitý	k2eAgFnPc7d1	prožitá
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
a	a	k8xC	a
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
představivost	představivost	k1gFnSc4	představivost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
strach	strach	k1gInSc1	strach
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
jako	jako	k9	jako
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
,	,	kIx,	,
varující	varující	k2eAgFnPc1d1	varující
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
lidi	člověk	k1gMnPc4	člověk
jsou	být	k5eAaImIp3nP	být
určité	určitý	k2eAgFnPc1d1	určitá
formy	forma	k1gFnPc1	forma
strachu	strach	k1gInSc2	strach
příjemné	příjemný	k2eAgNnSc1d1	příjemné
a	a	k8xC	a
často	často	k6eAd1	často
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
úmyslně	úmyslně	k6eAd1	úmyslně
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
–	–	k?	–
sledováním	sledování	k1gNnSc7	sledování
<g/>
,	,	kIx,	,
čtením	čtení	k1gNnSc7	čtení
či	či	k8xC	či
vyprávěním	vyprávění	k1gNnSc7	vyprávění
hrůzostrašných	hrůzostrašný	k2eAgInPc2d1	hrůzostrašný
příběhů	příběh	k1gInPc2	příběh
(	(	kIx(	(
<g/>
hororů	horor	k1gInPc2	horor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
provozováním	provozování	k1gNnSc7	provozování
tzv.	tzv.	kA	tzv.
extrémních	extrémní	k2eAgInPc2d1	extrémní
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
pohádky	pohádka	k1gFnPc1	pohádka
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
o	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neznají	neznat	k5eAaImIp3nP	neznat
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Nebojsa	nebojsa	k1gMnSc1	nebojsa
a	a	k8xC	a
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
Franta	Franta	k1gMnSc1	Franta
naučil	naučit	k5eAaPmAgMnS	naučit
bát	bát	k5eAaImF	bát
</s>
