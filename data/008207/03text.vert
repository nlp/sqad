<p>
<s>
Kubáňská	kubáňský	k2eAgFnSc1d1	Kubáňská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
Н	Н	k?	Н
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Kubanskaja	Kubanskaj	k2eAgFnSc1d1	Kubanskaj
Narodnaja	Narodnaj	k2eAgFnSc1d1	Narodnaja
Respublika	Respublika	k1gFnSc1	Respublika
<g/>
;	;	kIx,	;
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
К	К	k?	К
Н	Н	k?	Н
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Kubanska	Kubansko	k1gNnSc2	Kubansko
Narodna	Narodno	k1gNnSc2	Narodno
Respublika	Respublika	k1gFnSc1	Respublika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
KLR	KLR	kA	KLR
(	(	kIx(	(
<g/>
К	К	k?	К
<g/>
,	,	kIx,	,
KNR	KNR	kA	KNR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
existující	existující	k2eAgInSc4d1	existující
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
území	území	k1gNnSc6	území
Kubáně	Kubáň	k1gFnSc2	Kubáň
(	(	kIx(	(
<g/>
jihozápadní	jihozápadní	k2eAgNnSc1d1	jihozápadní
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
tzv.	tzv.	kA	tzv.
Kubáňskou	kubáňský	k2eAgFnSc7d1	Kubáňská
radou	rada	k1gFnSc7	rada
místních	místní	k2eAgInPc2d1	místní
kozáků	kozák	k1gInPc2	kozák
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
antikomunistická	antikomunistický	k2eAgFnSc1d1	antikomunistická
<g/>
,	,	kIx,	,
kozáci	kozák	k1gMnPc1	kozák
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
Bílou	bílý	k2eAgFnSc7d1	bílá
armádou	armáda	k1gFnSc7	armáda
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Kubáně	Kubáň	k1gFnSc2	Kubáň
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předcházející	předcházející	k2eAgFnSc3d1	předcházející
události	událost	k1gFnSc3	událost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
byla	být	k5eAaImAgFnS	být
Kubáň	Kubáň	k1gFnSc1	Kubáň
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
gubernií	gubernium	k1gNnPc2	gubernium
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
spravována	spravovat	k5eAaImNgFnS	spravovat
černomořskými	černomořský	k2eAgInPc7d1	černomořský
kozáky	kozák	k1gInPc7	kozák
<g/>
,	,	kIx,	,
východ	východ	k1gInSc4	východ
spravovali	spravovat	k5eAaImAgMnP	spravovat
kozáci	kozák	k1gMnPc1	kozák
donští	donský	k2eAgMnPc1d1	donský
<g/>
.	.	kIx.	.
</s>
<s>
Kozáci	Kozák	k1gMnPc1	Kozák
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Kubáni	Kubáň	k1gFnSc6	Kubáň
usazeni	usazen	k2eAgMnPc1d1	usazen
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1793	[number]	k4	1793
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bránili	bránit	k5eAaImAgMnP	bránit
ruské	ruský	k2eAgNnSc4d1	ruské
území	území	k1gNnSc4	území
před	před	k7c7	před
čerkeskými	čerkeský	k2eAgMnPc7d1	čerkeský
nájezdníky	nájezdník	k1gMnPc7	nájezdník
z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
proráželi	prorážet	k5eAaImAgMnP	prorážet
cestu	cesta	k1gFnSc4	cesta
ruské	ruský	k2eAgFnSc3d1	ruská
expanzi	expanze	k1gFnSc3	expanze
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Kavkazské	kavkazský	k2eAgInPc1d1	kavkazský
národy	národ	k1gInPc1	národ
byly	být	k5eAaImAgInP	být
nakonec	nakonec	k6eAd1	nakonec
poraženy	porazit	k5eAaPmNgInP	porazit
v	v	k7c6	v
kavkazské	kavkazský	k2eAgFnSc6d1	kavkazská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
a	a	k8xC	a
kozáci	kozák	k1gMnPc1	kozák
začali	začít	k5eAaPmAgMnP	začít
tvořit	tvořit	k5eAaImF	tvořit
tvrdé	tvrdý	k2eAgNnSc4d1	tvrdé
jádro	jádro	k1gNnSc4	jádro
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vysloužili	vysloužit	k5eAaPmAgMnP	vysloužit
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
vděk	vděk	k1gInSc4	vděk
cara	car	k1gMnSc2	car
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
odpouštěl	odpouštět	k5eAaImAgMnS	odpouštět
daně	daň	k1gFnPc4	daň
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
spravovat	spravovat	k5eAaImF	spravovat
jejich	jejich	k3xOp3gNnSc4	jejich
území	území	k1gNnSc4	území
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
jim	on	k3xPp3gMnPc3	on
mnohá	mnohý	k2eAgNnPc4d1	mnohé
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
cara	car	k1gMnSc2	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
agrární	agrární	k2eAgFnPc1d1	agrární
reformy	reforma	k1gFnPc1	reforma
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
Kubáně	Kubáň	k1gFnSc2	Kubáň
byla	být	k5eAaImAgFnS	být
cílem	cíl	k1gInSc7	cíl
mnoha	mnoho	k4c2	mnoho
osadníků	osadník	k1gMnPc2	osadník
(	(	kIx(	(
<g/>
především	především	k9	především
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sem	sem	k6eAd1	sem
byli	být	k5eAaImAgMnP	být
posláni	poslat	k5eAaPmNgMnP	poslat
kultivovat	kultivovat	k5eAaImF	kultivovat
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
půdy	půda	k1gFnSc2	půda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
důvodem	důvod	k1gInSc7	důvod
velkého	velký	k2eAgNnSc2d1	velké
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
novými	nový	k2eAgMnPc7d1	nový
přistěhovalci	přistěhovalec	k1gMnPc7	přistěhovalec
a	a	k8xC	a
starousedlými	starousedlý	k2eAgMnPc7d1	starousedlý
kozáky	kozák	k1gMnPc7	kozák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Revoluce	revoluce	k1gFnSc1	revoluce
1917	[number]	k4	1917
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgMnS	být
svržen	svržen	k2eAgMnSc1d1	svržen
car	car	k1gMnSc1	car
a	a	k8xC	a
ruská	ruský	k2eAgFnSc1d1	ruská
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
však	však	k9	však
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
nezadržitelný	zadržitelný	k2eNgInSc1d1	nezadržitelný
kolaps	kolaps	k1gInSc1	kolaps
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kozáků	kozák	k1gMnPc2	kozák
dezertovalo	dezertovat	k5eAaBmAgNnS	dezertovat
a	a	k8xC	a
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
bránit	bránit	k5eAaImF	bránit
Kubáň	Kubáň	k1gFnSc4	Kubáň
před	před	k7c7	před
hrozbou	hrozba	k1gFnSc7	hrozba
turecké	turecký	k2eAgFnSc2d1	turecká
invaze	invaze	k1gFnSc2	invaze
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Kubáňská	kubáňský	k2eAgFnSc1d1	Kubáňská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1917	[number]	k4	1917
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
vládní	vládní	k2eAgInSc4d1	vládní
orgán	orgán	k1gInSc4	orgán
na	na	k7c6	na
Kubáni	Kubáň	k1gFnSc6	Kubáň
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Kubáňská	kubáňský	k2eAgFnSc1d1	Kubáňská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
nově	nova	k1gFnSc3	nova
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
demokratického	demokratický	k2eAgNnSc2d1	demokratické
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
Říjnovou	říjnový	k2eAgFnSc7d1	říjnová
revolucí	revoluce	k1gFnSc7	revoluce
padla	padnout	k5eAaPmAgFnS	padnout
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
chopili	chopit	k5eAaPmAgMnP	chopit
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
populace	populace	k1gFnSc1	populace
rolníků	rolník	k1gMnPc2	rolník
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jimi	on	k3xPp3gFnPc7	on
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Kubáňská	kubáňský	k2eAgFnSc1d1	Kubáňská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
plně	plně	k6eAd1	plně
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Kubáňské	kubáňský	k2eAgFnSc2d1	Kubáňská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
Kubáňskou	kubáňský	k2eAgFnSc7d1	Kubáňská
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
rada	rada	k1gFnSc1	rada
odhlasovala	odhlasovat	k5eAaPmAgFnS	odhlasovat
připojení	připojení	k1gNnSc4	připojení
Kubáně	Kubáň	k1gFnSc2	Kubáň
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
federace	federace	k1gFnSc2	federace
k	k	k7c3	k
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
Ukrajinské	ukrajinský	k2eAgFnSc3d1	ukrajinská
lidové	lidový	k2eAgFnSc3d1	lidová
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ruská	ruský	k2eAgFnSc1d1	ruská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Kozáci	Kozák	k1gMnPc1	Kozák
byli	být	k5eAaImAgMnP	být
věrní	věrný	k2eAgMnPc1d1	věrný
carovi	carův	k2eAgMnPc1d1	carův
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
logicky	logicky	k6eAd1	logicky
jejich	jejich	k3xOp3gNnSc2	jejich
vojska	vojsko	k1gNnSc2	vojsko
postavila	postavit	k5eAaPmAgFnS	postavit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Bílé	bílý	k2eAgFnSc2d1	bílá
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1918	[number]	k4	1918
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
bělogvardějci	bělogvardějec	k1gMnPc1	bělogvardějec
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
z	z	k7c2	z
území	území	k1gNnSc2	území
Kubáně	Kubáň	k1gFnSc2	Kubáň
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Děnikin	Děnikin	k1gMnSc1	Děnikin
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
postava	postava	k1gFnSc1	postava
bělogvardějců	bělogvardějec	k1gMnPc2	bělogvardějec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
velice	velice	k6eAd1	velice
nespokojen	spokojen	k2eNgMnSc1d1	nespokojen
s	s	k7c7	s
tamní	tamní	k2eAgFnSc7d1	tamní
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
Nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
Rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
najít	najít	k5eAaPmF	najít
spojence	spojenec	k1gMnPc4	spojenec
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
federace	federace	k1gFnSc2	federace
s	s	k7c7	s
Ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
zakrátko	zakrátko	k6eAd1	zakrátko
nato	nato	k6eAd1	nato
začlenit	začlenit	k5eAaPmF	začlenit
do	do	k7c2	do
federace	federace	k1gFnSc2	federace
s	s	k7c7	s
Gruzínskou	gruzínský	k2eAgFnSc7d1	gruzínská
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
kubáňští	kubáňský	k2eAgMnPc1d1	kubáňský
velvyslanci	velvyslanec	k1gMnPc1	velvyslanec
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vyhlašovali	vyhlašovat	k5eAaImAgMnP	vyhlašovat
úplnou	úplný	k2eAgFnSc4d1	úplná
nezávislost	nezávislost	k1gFnSc4	nezávislost
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kubáňská	kubáňský	k2eAgFnSc1d1	Kubáňská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozsahu	rozsah	k1gInSc2	rozsah
zabírala	zabírat	k5eAaImAgFnS	zabírat
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Stavropolského	stavropolský	k2eAgInSc2d1	stavropolský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Krasnodarského	krasnodarský	k2eAgInSc2d1	krasnodarský
kraje	kraj	k1gInSc2	kraj
až	až	k9	až
po	po	k7c4	po
dnešní	dnešní	k2eAgInSc4d1	dnešní
Dagestán	Dagestán	k1gInSc4	Dagestán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
Radou	rada	k1gFnSc7	rada
odhlasováno	odhlasovat	k5eAaPmNgNnS	odhlasovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kubáňský	kubáňský	k2eAgInSc1d1	kubáňský
státní	státní	k2eAgInSc1d1	státní
celek	celek	k1gInSc1	celek
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
jmenovat	jmenovat	k5eAaImF	jmenovat
Kubáňský	kubáňský	k2eAgInSc4d1	kubáňský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
byli	být	k5eAaImAgMnP	být
posláni	poslán	k2eAgMnPc1d1	poslán
velvyslanci	velvyslanec	k1gMnPc1	velvyslanec
na	na	k7c4	na
Pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
mírovou	mírový	k2eAgFnSc4d1	mírová
konferenci	konference	k1gFnSc4	konference
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1919	[number]	k4	1919
–	–	k?	–
leden	leden	k1gInSc1	leden
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
prosadit	prosadit	k5eAaPmF	prosadit
nezávislost	nezávislost	k1gFnSc4	nezávislost
Kubáně	Kubáň	k1gFnSc2	Kubáň
a	a	k8xC	a
vyjednat	vyjednat	k5eAaPmF	vyjednat
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
rudým	rudý	k1gMnPc3	rudý
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
následně	následně	k6eAd1	následně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
veškerou	veškerý	k3xTgFnSc4	veškerý
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
Bílé	bílý	k2eAgFnSc3d1	bílá
armádě	armáda	k1gFnSc3	armáda
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
generál	generál	k1gMnSc1	generál
Děnikin	Děnikin	k1gMnSc1	Děnikin
nebyl	být	k5eNaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
uznat	uznat	k5eAaPmF	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Kubáně	Kubáň	k1gFnSc2	Kubáň
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
však	však	k9	však
nezískala	získat	k5eNaPmAgFnS	získat
podporu	podpora	k1gFnSc4	podpora
ani	ani	k8xC	ani
jednoho	jeden	k4xCgInSc2	jeden
státu	stát	k1gInSc2	stát
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
jen	jen	k9	jen
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
státy	stát	k1gInPc7	stát
vzniklými	vzniklý	k2eAgInPc7d1	vzniklý
z	z	k7c2	z
rozpadu	rozpad	k1gInSc2	rozpad
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Gruzínskou	gruzínský	k2eAgFnSc7d1	gruzínská
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
Ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Horskou	horský	k2eAgFnSc7d1	horská
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1919	[number]	k4	1919
generál	generál	k1gMnSc1	generál
Děnikin	Děnikin	k1gMnSc1	Děnikin
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
sídlo	sídlo	k1gNnSc4	sídlo
rady	rada	k1gFnSc2	rada
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
místních	místní	k2eAgMnPc2d1	místní
kozáků	kozák	k1gMnPc2	kozák
a	a	k8xC	a
zajmul	zajmout	k5eAaPmAgMnS	zajmout
deset	deset	k4xCc4	deset
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
včetně	včetně	k7c2	včetně
premiéra	premiér	k1gMnSc2	premiér
Kurganského	Kurganský	k2eAgMnSc2d1	Kurganský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
okamžitě	okamžitě	k6eAd1	okamžitě
popraven	popravit	k5eAaPmNgInS	popravit
za	za	k7c4	za
vlastizradu	vlastizrada	k1gFnSc4	vlastizrada
<g/>
.	.	kIx.	.
</s>
<s>
Kozáci	Kozák	k1gMnPc1	Kozák
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
za	za	k7c4	za
Děnikina	Děnikin	k1gMnSc4	Děnikin
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
vláda	vláda	k1gFnSc1	vláda
poslušná	poslušný	k2eAgFnSc1d1	poslušná
bělogvardějcům	bělogvardějec	k1gMnPc3	bělogvardějec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1919	[number]	k4	1919
ale	ale	k8xC	ale
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
bělogvardějci	bělogvardějec	k1gMnPc1	bělogvardějec
řadu	řada	k1gFnSc4	řada
těžkých	těžký	k2eAgFnPc2d1	těžká
porážek	porážka	k1gFnPc2	porážka
a	a	k8xC	a
představitelé	představitel	k1gMnPc1	představitel
Kubáně	Kubáň	k1gFnSc2	Kubáň
začali	začít	k5eAaPmAgMnP	začít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
opuštění	opuštění	k1gNnSc4	opuštění
bělogvardějských	bělogvardějský	k2eAgFnPc2d1	bělogvardějská
řad	řada	k1gFnPc2	řada
a	a	k8xC	a
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
koalice	koalice	k1gFnSc2	koalice
s	s	k7c7	s
Gruzií	Gruzie	k1gFnSc7	Gruzie
a	a	k8xC	a
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
celou	celý	k2eAgFnSc4d1	celá
Kubáň	Kubáň	k1gFnSc4	Kubáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
KLR	KLR	kA	KLR
==	==	k?	==
</s>
</p>
<p>
<s>
Děnikinova	Děnikinův	k2eAgFnSc1d1	Děnikinův
zrada	zrada	k1gFnSc1	zrada
vůči	vůči	k7c3	vůči
KLR	KLR	kA	KLR
a	a	k8xC	a
násilné	násilný	k2eAgNnSc1d1	násilné
sesazení	sesazení	k1gNnSc1	sesazení
Rady	rada	k1gFnSc2	rada
bylo	být	k5eAaImAgNnS	být
podrobeno	podroben	k2eAgNnSc1d1	podrobeno
silné	silný	k2eAgNnSc1d1	silné
kritice	kritika	k1gFnSc3	kritika
bělogvardějců	bělogvardějec	k1gMnPc2	bělogvardějec
jako	jako	k8xS	jako
zcela	zcela	k6eAd1	zcela
chybný	chybný	k2eAgInSc1d1	chybný
a	a	k8xC	a
zrádný	zrádný	k2eAgInSc1d1	zrádný
čin	čin	k1gInSc1	čin
v	v	k7c6	v
kritické	kritický	k2eAgFnSc6d1	kritická
době	doba	k1gFnSc6	doba
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
bolševikům	bolševik	k1gMnPc3	bolševik
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
a	a	k8xC	a
snadnému	snadný	k2eAgNnSc3d1	snadné
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
historici	historik	k1gMnPc1	historik
vypracovali	vypracovat	k5eAaPmAgMnP	vypracovat
alternativní	alternativní	k2eAgFnSc4d1	alternativní
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
KLR	KLR	kA	KLR
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
nově	nově	k6eAd1	nově
vzniklým	vzniklý	k2eAgInSc7d1	vzniklý
ukrajinským	ukrajinský	k2eAgInSc7d1	ukrajinský
státem	stát	k1gInSc7	stát
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
aktivněji	aktivně	k6eAd2	aktivně
žádala	žádat	k5eAaImAgFnS	žádat
ukrajinského	ukrajinský	k2eAgMnSc4d1	ukrajinský
spojence	spojenec	k1gMnSc4	spojenec
o	o	k7c4	o
vojenské	vojenský	k2eAgFnPc4d1	vojenská
posily	posila	k1gFnPc4	posila
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
další	další	k2eAgNnSc1d1	další
silné	silný	k2eAgNnSc1d1	silné
ohnisko	ohnisko	k1gNnSc1	ohnisko
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Centrálních	centrální	k2eAgFnPc2d1	centrální
mocností	mocnost	k1gFnPc2	mocnost
a	a	k8xC	a
koordinovaného	koordinovaný	k2eAgInSc2d1	koordinovaný
útoku	útok	k1gInSc2	útok
s	s	k7c7	s
Kolčakovou	Kolčakový	k2eAgFnSc7d1	Kolčakový
armádou	armáda	k1gFnSc7	armáda
na	na	k7c6	na
východě	východ	k1gInSc6	východ
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
svrhnout	svrhnout	k5eAaPmF	svrhnout
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
tak	tak	k9	tak
ale	ale	k9	ale
nestalo	stát	k5eNaPmAgNnS	stát
a	a	k8xC	a
z	z	k7c2	z
Kubáně	Kubáň	k1gFnSc2	Kubáň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součást	součást	k1gFnSc1	součást
sovětského	sovětský	k2eAgNnSc2d1	sovětské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kuban	Kuban	k1gMnSc1	Kuban
People	People	k1gMnSc1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Republic	Republice	k1gFnPc2	Republice
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
