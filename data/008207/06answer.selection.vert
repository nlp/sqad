<s>
Kubáňská	kubáňský	k2eAgFnSc1d1	Kubáňská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
Н	Н	k?	Н
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Kubanskaja	Kubanskaj	k2eAgFnSc1d1	Kubanskaj
Narodnaja	Narodnaj	k2eAgFnSc1d1	Narodnaja
Respublika	Respublika	k1gFnSc1	Respublika
<g/>
;	;	kIx,	;
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
К	К	k?	К
Н	Н	k?	Н
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Kubanska	Kubansko	k1gNnSc2	Kubansko
Narodna	Narodno	k1gNnSc2	Narodno
Respublika	Respublika	k1gFnSc1	Respublika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
KLR	KLR	kA	KLR
(	(	kIx(	(
<g/>
К	К	k?	К
<g/>
,	,	kIx,	,
KNR	KNR	kA	KNR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
existující	existující	k2eAgInSc4d1	existující
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
území	území	k1gNnSc6	území
Kubáně	Kubáň	k1gFnSc2	Kubáň
(	(	kIx(	(
<g/>
jihozápadní	jihozápadní	k2eAgNnSc1d1	jihozápadní
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
