<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
též	též	k9	též
AC	AC	kA	AC
(	(	kIx(	(
<g/>
alternating	alternating	k1gInSc1	alternating
current	current	k1gInSc1	current
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
označující	označující	k2eAgInSc1d1	označující
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
DC	DC	kA	DC
(	(	kIx(	(
<g/>
direct	direct	k1gMnSc1	direct
current	current	k1gMnSc1	current
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
obvodem	obvod	k1gInSc7	obvod
stále	stále	k6eAd1	stále
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
.	.	kIx.	.
</s>
