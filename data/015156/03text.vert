<s>
Speed	Speed	k1gInSc1
painting	painting	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navržen	k2eAgNnSc4d1
začlenění	začlenění	k1gNnSc4
celého	celý	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
do	do	k7c2
článku	článek	k1gInSc2
Rychlokresba	Rychlokresba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odsud	odsud	k6eAd1
tam	tam	k6eAd1
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
vést	vést	k5eAaImF
přesměrování	přesměrování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Speed	Speed	k1gInSc1
Painting	Painting	k1gInSc1
je	být	k5eAaImIp3nS
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
vypracovaný	vypracovaný	k2eAgInSc4d1
převážně	převážně	k6eAd1
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
umělec	umělec	k1gMnSc1
snaží	snažit	k5eAaImIp3nS
dle	dle	k7c2
předem	předem	k6eAd1
určených	určený	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
vytvořit	vytvořit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
dílo	dílo	k1gNnSc4
v	v	k7c6
co	co	k9
nejkratším	krátký	k2eAgInSc6d3
možném	možný	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speed	Speed	k1gInSc1
Painting	Painting	k1gInSc4
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
to	ten	k3xDgNnSc4
samé	samý	k3xTgNnSc4
jako	jako	k9
takzvané	takzvaný	k2eAgFnPc1d1
"	"	kIx"
<g/>
time-lapse	time-lapse	k6eAd1
video	video	k1gNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speed	Speed	k1gInSc1
Painting	Painting	k1gInSc1
se	se	k3xPyFc4
často	často	k6eAd1
uplatňuje	uplatňovat	k5eAaImIp3nS
v	v	k7c6
zábavním	zábavní	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
hlavně	hlavně	k9
mezi	mezi	k7c7
umělci	umělec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
si	se	k3xPyFc3
říkají	říkat	k5eAaImIp3nP
tzv.	tzv.	kA
"	"	kIx"
<g/>
Concept	Concept	k2eAgInSc1d1
Artists	Artists	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popularita	popularita	k1gFnSc1
a	a	k8xC
zájem	zájem	k1gInSc1
o	o	k7c4
Speed	Speed	k1gInSc4
Painting	Painting	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
výrazně	výrazně	k6eAd1
zvýšil	zvýšit	k5eAaPmAgInS
hlavně	hlavně	k9
díky	díky	k7c3
internetové	internetový	k2eAgFnSc3d1
prezentaci	prezentace	k1gFnSc3
Johna	John	k1gMnSc2
Locka	Locek	k1gMnSc2
(	(	kIx(
<g/>
seriál	seriál	k1gInSc1
Lost	Losta	k1gFnPc2
<g/>
)	)	kIx)
od	od	k7c2
umělce	umělec	k1gMnSc2
jménem	jméno	k1gNnSc7
Nico	Nico	k1gNnSc4
Di	Di	k1gFnSc1
Mattia	Mattium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Převzato	převzít	k5eAaPmNgNnS
ze	z	k7c2
Sphere	Spher	k1gMnSc5
Design	design	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Časosběr	Časosběr	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Speed	Speed	k1gInSc1
painting	painting	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Speed-painting	Speed-painting	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
-	-	kIx~
Galerie	galerie	k1gFnSc1
speed	speed	k1gMnSc1
painting	painting	k1gInSc1
videí	video	k1gNnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
