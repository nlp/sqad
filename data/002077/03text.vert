<s>
Londýn	Londýn	k1gInSc1	Londýn
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
London	London	k1gMnSc1	London
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Produkuje	produkovat	k5eAaImIp3nS	produkovat
20	[number]	k4	20
%	%	kIx~	%
HDP	HDP	kA	HDP
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
londýnská	londýnský	k2eAgFnSc1d1	londýnská
City	City	k1gFnSc1	City
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
světových	světový	k2eAgNnPc2d1	světové
obchodních	obchodní	k2eAgNnPc2d1	obchodní
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
městy	město	k1gNnPc7	město
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
Tokio	Tokio	k1gNnSc1	Tokio
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
vymezené	vymezený	k2eAgNnSc4d1	vymezené
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
anglických	anglický	k2eAgInPc2d1	anglický
regionů	region	k1gInPc2	region
-	-	kIx~	-
Velký	velký	k2eAgInSc1d1	velký
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
existují	existovat	k5eAaImIp3nP	existovat
jisté	jistý	k2eAgFnPc4d1	jistá
známky	známka	k1gFnPc4	známka
roztroušených	roztroušený	k2eAgFnPc2d1	roztroušená
britských	britský	k2eAgFnPc2d1	britská
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
obec	obec	k1gFnSc4	obec
Londinium	Londinium	k1gNnSc4	Londinium
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
roku	rok	k1gInSc2	rok
43	[number]	k4	43
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
osada	osada	k1gFnSc1	osada
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
vypálena	vypálit	k5eAaPmNgFnS	vypálit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
61	[number]	k4	61
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
keltského	keltský	k2eAgNnSc2d1	keltské
vojska	vojsko	k1gNnSc2	vojsko
královny	královna	k1gFnSc2	královna
Boudiccy	Boudicca	k1gFnSc2	Boudicca
<g/>
.	.	kIx.	.
</s>
<s>
Obnovená	obnovený	k2eAgFnSc1d1	obnovená
osada	osada	k1gFnSc1	osada
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
a	a	k8xC	a
překonala	překonat	k5eAaPmAgFnS	překonat
svým	svůj	k3xOyFgInSc7	svůj
významem	význam	k1gInSc7	význam
Colchester	Colchestra	k1gFnPc2	Colchestra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc2d3	veliký
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
Anglosasové	Anglosas	k1gMnPc1	Anglosas
novou	nový	k2eAgFnSc4d1	nová
osadu	osada	k1gFnSc4	osada
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Lundenwic	Lundenwice	k1gInPc2	Lundenwice
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
od	od	k7c2	od
původního	původní	k2eAgNnSc2d1	původní
římského	římský	k2eAgNnSc2d1	římské
města	město	k1gNnSc2	město
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
současné	současný	k2eAgFnPc1d1	současná
Covent	Covent	k1gInSc4	Covent
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Fleet	Fleet	k1gInSc1	Fleet
zřejmě	zřejmě	k6eAd1	zřejmě
existoval	existovat	k5eAaImAgInS	existovat
rybářský	rybářský	k2eAgInSc1d1	rybářský
a	a	k8xC	a
obchodní	obchodní	k2eAgInSc1d1	obchodní
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
851	[number]	k4	851
na	na	k7c4	na
město	město	k1gNnSc4	město
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
Vikingové	Viking	k1gMnPc1	Viking
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
vypálili	vypálit	k5eAaPmAgMnP	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Alfréd	Alfréd	k1gMnSc1	Alfréd
Veliký	veliký	k2eAgMnSc1d1	veliký
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Vikingy	Viking	k1gMnPc7	Viking
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
mír	mír	k1gInSc4	mír
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
město	město	k1gNnSc4	město
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
původních	původní	k2eAgFnPc2d1	původní
římských	římský	k2eAgFnPc2d1	římská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
dalších	další	k2eAgMnPc2d1	další
anglosaských	anglosaský	k2eAgMnPc2d1	anglosaský
králů	král	k1gMnPc2	král
město	město	k1gNnSc4	město
vzkvétalo	vzkvétat	k5eAaImAgNnS	vzkvétat
jako	jako	k9	jako
středisko	středisko	k1gNnSc1	středisko
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
důležité	důležitý	k2eAgNnSc1d1	důležité
politické	politický	k2eAgNnSc1d1	politické
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
občasné	občasný	k2eAgInPc1d1	občasný
útoky	útok	k1gInPc1	útok
Vikingů	Viking	k1gMnPc2	Viking
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
a	a	k8xC	a
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1013	[number]	k4	1013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
Knut	knuta	k1gFnPc2	knuta
město	město	k1gNnSc4	město
dobyl	dobýt	k5eAaPmAgInS	dobýt
a	a	k8xC	a
donutil	donutit	k5eAaPmAgInS	donutit
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Ethelreda	Ethelred	k1gMnSc2	Ethelred
II	II	kA	II
<g/>
.	.	kIx.	.
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nechal	nechat	k5eAaPmAgInS	nechat
obnovit	obnovit	k5eAaPmF	obnovit
Westminsterské	Westminsterský	k2eAgNnSc4d1	Westminsterské
opatství	opatství	k1gNnSc4	opatství
a	a	k8xC	a
Westminsterský	Westminsterský	k2eAgInSc4d1	Westminsterský
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc2	Hastings
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
korunován	korunován	k2eAgMnSc1d1	korunován
na	na	k7c4	na
krále	král	k1gMnSc4	král
roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
v	v	k7c6	v
nově	nově	k6eAd1	nově
dokončeném	dokončený	k2eAgNnSc6d1	dokončené
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
přiznal	přiznat	k5eAaPmAgMnS	přiznat
obyvatelům	obyvatel	k1gMnPc3	obyvatel
města	město	k1gNnSc2	město
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
postavit	postavit	k5eAaPmF	postavit
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
město	město	k1gNnSc4	město
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1097	[number]	k4	1097
začal	začít	k5eAaPmAgMnS	začít
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
stavět	stavět	k5eAaImF	stavět
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
opatství	opatství	k1gNnPc2	opatství
Westminster	Westminster	k1gMnSc1	Westminster
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
královskou	královský	k2eAgFnSc7d1	královská
rezidencí	rezidence	k1gFnSc7	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
Westminster	Westminster	k1gMnSc1	Westminster
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
City	City	k1gFnSc1	City
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
spravována	spravován	k2eAgFnSc1d1	spravována
vlastní	vlastní	k2eAgFnSc1d1	vlastní
institucí	instituce	k1gFnPc2	instituce
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
sousedící	sousedící	k2eAgFnPc1d1	sousedící
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozrůstaly	rozrůstat	k5eAaImAgInP	rozrůstat
a	a	k8xC	a
staly	stát	k5eAaPmAgInP	stát
se	s	k7c7	s
základem	základ	k1gInSc7	základ
moderního	moderní	k2eAgNnSc2d1	moderní
centra	centrum	k1gNnSc2	centrum
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
převzaly	převzít	k5eAaPmAgInP	převzít
po	po	k7c6	po
Winchesteru	Winchester	k1gInSc6	Winchester
roli	role	k1gFnSc4	role
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
18	[number]	k4	18
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1100	[number]	k4	1100
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Londýn	Londýn	k1gInSc4	Londýn
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
(	(	kIx(	(
<g/>
Černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
smrt	smrt	k1gFnSc4	smrt
asi	asi	k9	asi
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
město	město	k1gNnSc4	město
i	i	k8xC	i
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1665	[number]	k4	1665
až	až	k9	až
1666	[number]	k4	1666
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
mor	mor	k1gInSc1	mor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
velkou	velký	k2eAgFnSc7d1	velká
katastrofou	katastrofa	k1gFnSc7	katastrofa
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
zničil	zničit	k5eAaPmAgInS	zničit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezpůsobil	způsobit	k5eNaPmAgInS	způsobit
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc4	žádný
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
důsledku	důsledek	k1gInSc6	důsledek
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
Londýn	Londýn	k1gInSc4	Londýn
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
žádná	žádný	k3yNgFnSc1	žádný
morová	morový	k2eAgFnSc1d1	morová
rána	rána	k1gFnSc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
města	město	k1gNnSc2	město
trvala	trvat	k5eAaImAgFnS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
expanzi	expanze	k1gFnSc6	expanze
města	město	k1gNnSc2	město
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
světa	svět	k1gInSc2	svět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1831	[number]	k4	1831
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
růst	růst	k1gInSc1	růst
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
i	i	k9	i
první	první	k4xOgFnSc4	první
železnici	železnice	k1gFnSc4	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
dopravy	doprava	k1gFnSc2	doprava
byl	být	k5eAaImAgInS	být
doplněn	doplněn	k2eAgInSc1d1	doplněn
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
vznikem	vznik	k1gInSc7	vznik
prvního	první	k4xOgNnSc2	první
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
samospráva	samospráva	k1gFnSc1	samospráva
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
města	město	k1gNnSc2	město
a	a	k8xC	a
vybudováním	vybudování	k1gNnSc7	vybudování
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885	[number]	k4	1885
až	až	k9	až
1889	[number]	k4	1889
nastartoval	nastartovat	k5eAaPmAgInS	nastartovat
řešení	řešení	k1gNnSc4	řešení
těchto	tento	k3xDgInPc2	tento
problémů	problém	k1gInPc2	problém
instituce	instituce	k1gFnSc1	instituce
Metropolitan	metropolitan	k1gInSc1	metropolitan
Board	Board	k1gInSc1	Board
of	of	k?	of
Works	Works	kA	Works
později	pozdě	k6eAd2	pozdě
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
prvním	první	k4xOgInSc7	první
voleným	volený	k2eAgInSc7d1	volený
správním	správní	k2eAgInSc7d1	správní
orgánem	orgán	k1gInSc7	orgán
Radou	rada	k1gFnSc7	rada
hrabství	hrabství	k1gNnSc3	hrabství
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bombardování	bombardování	k1gNnSc2	bombardování
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
a	a	k8xC	a
zničeno	zničen	k2eAgNnSc4d1	zničeno
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
města	město	k1gNnSc2	město
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
použitím	použití	k1gNnSc7	použití
různých	různý	k2eAgInPc2d1	různý
architektonických	architektonický	k2eAgInPc2d1	architektonický
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
změnila	změnit	k5eAaPmAgFnS	změnit
tak	tak	k9	tak
charakter	charakter	k1gInSc4	charakter
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
hranic	hranice	k1gFnPc2	hranice
města	město	k1gNnSc2	město
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
jako	jako	k8xS	jako
Velký	velký	k2eAgInSc1d1	velký
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
imigrace	imigrace	k1gFnSc1	imigrace
změnila	změnit	k5eAaPmAgFnS	změnit
složení	složení	k1gNnSc4	složení
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
multikulturním	multikulturní	k2eAgNnSc7d1	multikulturní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vrátil	vrátit	k5eAaPmAgMnS	vrátit
městu	město	k1gNnSc3	město
jeho	jeho	k3xOp3gFnSc6	jeho
pozici	pozice	k1gFnSc6	pozice
jako	jako	k8xC	jako
města	město	k1gNnSc2	město
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
břehu	břeh	k1gInSc6	břeh
Temže	Temže	k1gFnSc2	Temže
a	a	k8xC	a
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
století	století	k1gNnPc2	století
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
vedl	vést	k5eAaImAgMnS	vést
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc4d1	jediný
most	most	k1gInSc4	most
London	London	k1gMnSc1	London
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Londýn	Londýn	k1gInSc1	Londýn
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vystavěny	vystavěn	k2eAgInPc4d1	vystavěn
další	další	k2eAgInPc4d1	další
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
město	město	k1gNnSc4	město
expandovat	expandovat	k5eAaImF	expandovat
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
směrů	směr	k1gInPc2	směr
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rovná	rovnat	k5eAaImIp3nS	rovnat
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
krajina	krajina	k1gFnSc1	krajina
okolo	okolo	k7c2	okolo
řeky	řeka	k1gFnSc2	řeka
nepředstavovala	představovat	k5eNaImAgFnS	představovat
žádnou	žádný	k3yNgFnSc4	žádný
přírodní	přírodní	k2eAgFnSc4d1	přírodní
překážku	překážka	k1gFnSc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc4	několik
návrší	návrš	k1gFnPc2	návrš
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Parliament	Parliament	k1gMnSc1	Parliament
Hill	Hill	k1gMnSc1	Hill
anebo	anebo	k8xC	anebo
Primrose	Primrosa	k1gFnSc3	Primrosa
Hill	Hillum	k1gNnPc2	Hillum
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nepříliš	příliš	k6eNd1	příliš
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
a	a	k8xC	a
nepředstavovaly	představovat	k5eNaImAgFnP	představovat
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
žádnou	žádný	k3yNgFnSc4	žádný
překážku	překážka	k1gFnSc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
skutečnostem	skutečnost	k1gFnPc3	skutečnost
má	mít	k5eAaImIp3nS	mít
Londýn	Londýn	k1gInSc1	Londýn
téměř	téměř	k6eAd1	téměř
kruhový	kruhový	k2eAgInSc4d1	kruhový
půdorys	půdorys	k1gInSc4	půdorys
a	a	k8xC	a
území	území	k1gNnSc4	území
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
1	[number]	k4	1
577	[number]	k4	577
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Temže	Temže	k1gFnSc2	Temže
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
daleko	daleko	k6eAd1	daleko
širší	široký	k2eAgInSc1d2	širší
a	a	k8xC	a
mělčí	mělký	k2eAgInSc1d2	mělčí
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
sevřena	sevřít	k5eAaPmNgFnS	sevřít
do	do	k7c2	do
břehů	břeh	k1gInPc2	břeh
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
přítoků	přítok	k1gInPc2	přítok
do	do	k7c2	do
Temže	Temže	k1gFnSc2	Temže
protéká	protékat	k5eAaImIp3nS	protékat
londýnským	londýnský	k2eAgNnSc7d1	Londýnské
podzemím	podzemí	k1gNnSc7	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Temže	Temže	k1gFnSc1	Temže
je	být	k5eAaImIp3nS	být
přílivová	přílivový	k2eAgFnSc1d1	přílivová
řeka	řeka	k1gFnSc1	řeka
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
ohrožen	ohrozit	k5eAaPmNgMnS	ohrozit
záplavami	záplava	k1gFnPc7	záplava
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
možnými	možný	k2eAgFnPc7d1	možná
záplavami	záplava	k1gFnPc7	záplava
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgFnP	vybudovat
bariéry	bariéra	k1gFnPc1	bariéra
na	na	k7c4	na
Temži	Temže	k1gFnSc4	Temže
ve	v	k7c6	v
Woolwichi	Woolwich	k1gFnSc6	Woolwich
<g/>
.	.	kIx.	.
</s>
<s>
Mírné	mírný	k2eAgNnSc4d1	mírné
oceánské	oceánský	k2eAgNnSc4d1	oceánské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
i	i	k9	i
velmi	velmi	k6eAd1	velmi
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
37	[number]	k4	37
<g/>
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
teplejší	teplý	k2eAgNnPc4d2	teplejší
léta	léto	k1gNnPc4	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
chladná	chladný	k2eAgFnSc1d1	chladná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
moc	moc	k6eAd1	moc
mrazivá	mrazivý	k2eAgFnSc1d1	mrazivá
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgFnPc1d1	noční
teploty	teplota	k1gFnPc1	teplota
klesají	klesat	k5eAaImIp3nP	klesat
nejníže	nízce	k6eAd3	nízce
na	na	k7c4	na
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Největší	veliký	k2eAgInPc1d3	veliký
mrazy	mráz	k1gInPc1	mráz
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
a	a	k8xC	a
lednu	leden	k1gInSc6	leden
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Mrazivých	mrazivý	k2eAgInPc2d1	mrazivý
dnů	den	k1gInPc2	den
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ochlazeních	ochlazení	k1gNnPc6	ochlazení
od	od	k7c2	od
konce	konec	k1gInSc2	konec
podzimu	podzim	k1gInSc2	podzim
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
jara	jaro	k1gNnSc2	jaro
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
rozehřátým	rozehřátý	k2eAgInSc7d1	rozehřátý
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
a	a	k8xC	a
okrajem	okraj	k1gInSc7	okraj
5	[number]	k4	5
°	°	k?	°
<g/>
C.	C.	kA	C.
Bouřky	bouřka	k1gFnPc1	bouřka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
5	[number]	k4	5
dní	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Větry	vítr	k1gInPc1	vítr
vanoucí	vanoucí	k2eAgInPc1d1	vanoucí
od	od	k7c2	od
oceánu	oceán	k1gInSc2	oceán
přinášejí	přinášet	k5eAaImIp3nP	přinášet
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
chlad	chlad	k1gInSc4	chlad
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
západoevropských	západoevropský	k2eAgFnPc6d1	západoevropská
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Větry	vítr	k1gInPc1	vítr
jsou	být	k5eAaImIp3nP	být
nejsilnější	silný	k2eAgMnPc4d3	nejsilnější
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Bouře	bouř	k1gFnPc1	bouř
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
krát	krát	k6eAd1	krát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tornáda	tornádo	k1gNnPc1	tornádo
jsou	být	k5eAaImIp3nP	být
vzácná	vzácný	k2eAgNnPc1d1	vzácné
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
etnické	etnický	k2eAgNnSc1d1	etnické
složení	složení	k1gNnSc1	složení
Londýna	Londýn	k1gInSc2	Londýn
následující	následující	k2eAgFnSc7d1	následující
<g/>
:	:	kIx,	:
71,1	[number]	k4	71,1
%	%	kIx~	%
-	-	kIx~	-
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
59,8	[number]	k4	59,8
%	%	kIx~	%
bílí	bílý	k2eAgMnPc1d1	bílý
Britové	Brit	k1gMnPc1	Brit
<g/>
;	;	kIx,	;
3,1	[number]	k4	3,1
%	%	kIx~	%
bílí	bílý	k2eAgMnPc1d1	bílý
Irové	Ir	k1gMnPc1	Ir
<g/>
;	;	kIx,	;
8,3	[number]	k4	8,3
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
běloši	běloch	k1gMnPc1	běloch
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
12,1	[number]	k4	12,1
%	%	kIx~	%
Jižní	jižní	k2eAgMnPc1d1	jižní
Asiaté	Asiat	k1gMnPc1	Asiat
(	(	kIx(	(
<g/>
6,1	[number]	k4	6,1
%	%	kIx~	%
Indové	Ind	k1gMnPc1	Ind
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
2,2	[number]	k4	2,2
%	%	kIx~	%
Bangladéšané	Bangladéšaná	k1gFnSc2	Bangladéšaná
<g/>
;	;	kIx,	;
2,0	[number]	k4	2,0
%	%	kIx~	%
Pákistánci	Pákistánec	k1gMnPc1	Pákistánec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
10,9	[number]	k4	10,9
%	%	kIx~	%
černoši	černoch	k1gMnPc1	černoch
(	(	kIx(	(
<g/>
5,3	[number]	k4	5,3
%	%	kIx~	%
-	-	kIx~	-
Afričané	Afričan	k1gMnPc1	Afričan
<g/>
;	;	kIx,	;
4,8	[number]	k4	4,8
%	%	kIx~	%
Karibští	karibský	k2eAgMnPc1d1	karibský
černoši	černoch	k1gMnPc1	černoch
<g/>
;	;	kIx,	;
0,8	[number]	k4	0,8
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
černoši	černoch	k1gMnPc1	černoch
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
3,2	[number]	k4	3,2
%	%	kIx~	%
-	-	kIx~	-
míšenci	míšenec	k1gMnSc3	míšenec
<g/>
;	;	kIx,	;
1,1	[number]	k4	1,1
%	%	kIx~	%
-	-	kIx~	-
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
;	;	kIx,	;
1,6	[number]	k4	1,6
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
21,8	[number]	k4	21,8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
uvádělo	uvádět	k5eAaImAgNnS	uvádět
místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
mimo	mimo	k7c4	mimo
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
59,8	[number]	k4	59,8
%	%	kIx~	%
-	-	kIx~	-
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
44,8	[number]	k4	44,8
%	%	kIx~	%
bílí	bílý	k2eAgMnPc1d1	bílý
Britové	Brit	k1gMnPc1	Brit
<g/>
;	;	kIx,	;
2,2	[number]	k4	2,2
%	%	kIx~	%
bílí	bílý	k2eAgMnPc1d1	bílý
Irové	Ir	k1gMnPc1	Ir
<g/>
;	;	kIx,	;
0,1	[number]	k4	0,1
%	%	kIx~	%
cikáni	cikán	k2eAgMnPc1d1	cikán
a	a	k8xC	a
Irští	irský	k2eAgMnPc1d1	irský
Travellers	Travellersa	k1gFnPc2	Travellersa
<g/>
;	;	kIx,	;
12,1	[number]	k4	12,1
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
běloši	běloch	k1gMnPc1	běloch
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
6,6	[number]	k4	6,6
%	%	kIx~	%
Indové	Ind	k1gMnPc1	Ind
<g/>
;	;	kIx,	;
2,7	[number]	k4	2,7
%	%	kIx~	%
Bangladéšané	Bangladéšaná	k1gFnSc2	Bangladéšaná
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
2,7	[number]	k4	2,7
%	%	kIx~	%
Pákistánci	Pákistánec	k1gMnPc1	Pákistánec
<g/>
;	;	kIx,	;
13,3	[number]	k4	13,3
%	%	kIx~	%
černoši	černoch	k1gMnPc1	černoch
(	(	kIx(	(
<g/>
7,0	[number]	k4	7,0
%	%	kIx~	%
-	-	kIx~	-
Afričané	Afričan	k1gMnPc1	Afričan
<g/>
;	;	kIx,	;
4,2	[number]	k4	4,2
%	%	kIx~	%
Karibští	karibský	k2eAgMnPc1d1	karibský
černoši	černoch	k1gMnPc1	černoch
<g/>
;	;	kIx,	;
2,1	[number]	k4	2,1
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
černoši	černoch	k1gMnPc1	černoch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5,0	[number]	k4	5,0
%	%	kIx~	%
-	-	kIx~	-
míšenci	míšenec	k1gMnSc3	míšenec
<g/>
;	;	kIx,	;
1,5	[number]	k4	1,5
%	%	kIx~	%
-	-	kIx~	-
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
;	;	kIx,	;
1,3	[number]	k4	1,3
%	%	kIx~	%
Arabové	Arab	k1gMnPc1	Arab
<g/>
;	;	kIx,	;
2,1	[number]	k4	2,1
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
37	[number]	k4	37
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
uvádělo	uvádět	k5eAaImAgNnS	uvádět
místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
žilo	žít	k5eAaImAgNnS	žít
k	k	k7c3	k
rozhodnému	rozhodný	k2eAgNnSc3d1	rozhodné
datu	datum	k1gNnSc3	datum
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
asi	asi	k9	asi
8	[number]	k4	8
841	[number]	k4	841
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
12	[number]	k4	12
800	[number]	k4	800
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
<g/>
:	:	kIx,	:
58,25	[number]	k4	58,25
%	%	kIx~	%
-	-	kIx~	-
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
15,8	[number]	k4	15,8
%	%	kIx~	%
-	-	kIx~	-
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
860	[number]	k4	860
000	[number]	k4	000
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
Paříž	Paříž	k1gFnSc4	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
měla	mít	k5eAaImAgFnS	mít
asi	asi	k9	asi
670	[number]	k4	670
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
do	do	k7c2	do
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
překonán	překonat	k5eAaPmNgInS	překonat
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
činí	činit	k5eAaImIp3nS	činit
7	[number]	k4	7
750	[number]	k4	750
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
včetně	včetně	k7c2	včetně
příměstských	příměstský	k2eAgFnPc2d1	příměstská
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
urban	urban	k1gInSc1	urban
area	area	k1gFnSc1	area
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
8	[number]	k4	8
278	[number]	k4	278
251	[number]	k4	251
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Londýn	Londýn	k1gInSc1	Londýn
včetně	včetně	k7c2	včetně
příměstských	příměstský	k2eAgFnPc2d1	příměstská
částí	část	k1gFnPc2	část
třetí	třetí	k4xOgFnSc2	třetí
největší	veliký	k2eAgFnSc2d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
po	po	k7c6	po
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
11,7	[number]	k4	11,7
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
9,6	[number]	k4	9,6
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
národně	národně	k6eAd1	národně
koordinované	koordinovaný	k2eAgInPc4d1	koordinovaný
počty	počet	k1gInPc4	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
oblasti	oblast	k1gFnSc2	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
London	London	k1gMnSc1	London
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
metropolitan	metropolitan	k1gInSc1	metropolitan
area	area	k1gFnSc1	area
<g/>
)	)	kIx)	)
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
sledování	sledování	k1gNnSc4	sledování
osob	osoba	k1gFnPc2	osoba
dojíždějících	dojíždějící	k2eAgFnPc2d1	dojíždějící
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
oblast	oblast	k1gFnSc1	oblast
Londýna	Londýn	k1gInSc2	Londýn
plochu	plocha	k1gFnSc4	plocha
16	[number]	k4	16
043	[number]	k4	043
km2	km2	k4	km2
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
13	[number]	k4	13
945	[number]	k4	945
000	[number]	k4	000
-	-	kIx~	-
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
Londýna	Londýn	k1gInSc2	Londýn
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
Moskvy	Moskva	k1gFnSc2	Moskva
(	(	kIx(	(
<g/>
asi	asi	k9	asi
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
největší	veliký	k2eAgFnSc7d3	veliký
metropolitní	metropolitní	k2eAgFnSc7d1	metropolitní
oblastí	oblast	k1gFnSc7	oblast
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
oblast	oblast	k1gFnSc1	oblast
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
11,5	[number]	k4	11,5
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Definici	definice	k1gFnSc4	definice
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pramenu	pramen	k1gInSc6	pramen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
opatrností	opatrnost	k1gFnSc7	opatrnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
místa	místo	k1gNnPc1	místo
dosti	dosti	k6eAd1	dosti
vzdálená	vzdálený	k2eAgNnPc1d1	vzdálené
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Dover	Dover	k1gMnSc1	Dover
ležící	ležící	k2eAgMnSc1d1	ležící
u	u	k7c2	u
Lamanšského	lamanšský	k2eAgInSc2d1	lamanšský
průlivu	průliv	k1gInSc2	průliv
nebo	nebo	k8xC	nebo
Colchester	Colchestra	k1gFnPc2	Colchestra
město	město	k1gNnSc4	město
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Essexu	Essex	k1gInSc2	Essex
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odečtení	odečtení	k1gNnSc6	odečtení
východního	východní	k2eAgInSc2d1	východní
Kentu	Kent	k1gInSc2	Kent
<g/>
,	,	kIx,	,
severního	severní	k2eAgInSc2d1	severní
Essexu	Essex	k1gInSc2	Essex
a	a	k8xC	a
West	Westa	k1gFnPc2	Westa
Berkshire	Berkshir	k1gInSc5	Berkshir
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
mezi	mezi	k7c7	mezi
12	[number]	k4	12
až	až	k8xS	až
12,5	[number]	k4	12,5
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vláda	vláda	k1gFnSc1	vláda
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Londýnský	londýnský	k2eAgInSc4d1	londýnský
region	region	k1gInSc4	region
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
metropolitan	metropolitan	k1gInSc1	metropolitan
region	region	k1gInSc1	region
<g/>
)	)	kIx)	)
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
27	[number]	k4	27
224	[number]	k4	224
km2	km2	k4	km2
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přibližně	přibližně	k6eAd1	přibližně
18	[number]	k4	18
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Velký	velký	k2eAgInSc4d1	velký
Londýn	Londýn	k1gInSc4	Londýn
<g/>
,	,	kIx,	,
celou	celý	k2eAgFnSc4d1	celá
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
východní	východní	k2eAgFnSc2d1	východní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgInSc1d1	metropolitní
region	region	k1gInSc1	region
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
pojem	pojem	k1gInSc1	pojem
od	od	k7c2	od
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
území	území	k1gNnSc4	území
s	s	k7c7	s
obrovským	obrovský	k2eAgNnSc7d1	obrovské
množstvím	množství	k1gNnSc7	množství
spojení	spojení	k1gNnSc2	spojení
a	a	k8xC	a
sítí	síť	k1gFnPc2	síť
mezi	mezi	k7c7	mezi
obydlenými	obydlený	k2eAgFnPc7d1	obydlená
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
metropolitní	metropolitní	k2eAgInSc1d1	metropolitní
region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
definován	definovat	k5eAaBmNgInS	definovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
málo	málo	k4c4	málo
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
pojmem	pojem	k1gInSc7	pojem
Londýn	Londýn	k1gInSc1	Londýn
označováno	označovat	k5eAaImNgNnS	označovat
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
Londýn	Londýn	k1gInSc1	Londýn
a	a	k8xC	a
Vnější	vnější	k2eAgInSc1d1	vnější
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
společenského	společenský	k2eAgNnSc2d1	společenské
dění	dění	k1gNnSc2	dění
Londýna	Londýn	k1gInSc2	Londýn
je	být	k5eAaImIp3nS	být
Westminster	Westminster	k1gInSc1	Westminster
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
většina	většina	k1gFnSc1	většina
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
zábavních	zábavní	k2eAgNnPc2d1	zábavní
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
vedení	vedení	k1gNnSc1	vedení
největších	veliký	k2eAgInPc2d3	veliký
britských	britský	k2eAgInPc2d1	britský
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
finančního	finanční	k2eAgInSc2d1	finanční
sektoru	sektor	k1gInSc2	sektor
<g/>
)	)	kIx)	)
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
City	City	k1gFnSc1	City
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
finančním	finanční	k2eAgMnSc7d1	finanční
centrem	centr	k1gMnSc7	centr
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
řízená	řízený	k2eAgFnSc1d1	řízená
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
institucí	instituce	k1gFnPc2	instituce
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
středověké	středověký	k2eAgInPc4d1	středověký
kořeny	kořen	k1gInPc4	kořen
a	a	k8xC	a
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
je	být	k5eAaImIp3nS	být
Lord	lord	k1gMnSc1	lord
Mayor	Mayor	k1gMnSc1	Mayor
of	of	k?	of
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
starosta	starosta	k1gMnSc1	starosta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
dominantu	dominanta	k1gFnSc4	dominanta
katedrálu	katedrála	k1gFnSc4	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
doplnily	doplnit	k5eAaPmAgFnP	doplnit
vysoké	vysoký	k2eAgFnPc1d1	vysoká
budovy	budova	k1gFnPc1	budova
kanceláří	kancelář	k1gFnPc2	kancelář
včetně	včetně	k7c2	včetně
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
Tower	Towra	k1gFnPc2	Towra
42	[number]	k4	42
(	(	kIx(	(
<g/>
známa	známo	k1gNnSc2	známo
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
NatWest	NatWest	k1gMnSc1	NatWest
Tower	Tower	k1gMnSc1	Tower
<g/>
)	)	kIx)	)
a	a	k8xC	a
30	[number]	k4	30
St	St	kA	St
Mary	Mary	k1gFnSc1	Mary
Axe	Axe	k1gFnSc1	Axe
(	(	kIx(	(
<g/>
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Gherkin	Gherkin	k1gInSc1	Gherkin
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
asi	asi	k9	asi
7	[number]	k4	7
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
dne	den	k1gInSc2	den
zde	zde	k6eAd1	zde
pracuje	pracovat	k5eAaImIp3nS	pracovat
až	až	k9	až
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
finanční	finanční	k2eAgNnSc1d1	finanční
centrum	centrum	k1gNnSc1	centrum
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
z	z	k7c2	z
City	City	k1gFnSc2	City
do	do	k7c2	do
Canary	Canara	k1gFnSc2	Canara
Wharf	Wharf	k1gInSc1	Wharf
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k1gInSc1	West
End	End	k1gFnSc2	End
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejpopulárnější	populární	k2eAgInSc4d3	nejpopulárnější
nákupní	nákupní	k2eAgInSc4d1	nákupní
a	a	k8xC	a
zábavní	zábavní	k2eAgInSc4d1	zábavní
obvod	obvod	k1gInSc4	obvod
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
Trafalgarské	Trafalgarský	k2eAgNnSc4d1	Trafalgarské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgFnPc2d3	nejnavštěvovanější
památek	památka	k1gFnPc2	památka
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
Street	Street	k1gMnSc1	Street
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
ulicí	ulice	k1gFnSc7	ulice
světa	svět	k1gInSc2	svět
pro	pro	k7c4	pro
nakupování	nakupování	k1gNnSc4	nakupování
módního	módní	k2eAgNnSc2d1	módní
značkového	značkový	k2eAgNnSc2d1	značkové
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
od	od	k7c2	od
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Oxford	Oxford	k1gInSc1	Oxford
Street	Street	k1gInSc1	Street
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Soho	Soho	k1gNnSc1	Soho
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc1	síť
malých	malý	k2eAgFnPc2d1	malá
uliček	ulička	k1gFnPc2	ulička
plných	plný	k2eAgFnPc2d1	plná
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
hospůdek	hospůdka	k1gFnPc2	hospůdka
<g/>
,	,	kIx,	,
menších	malý	k2eAgInPc2d2	menší
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
butiků	butik	k1gInPc2	butik
<g/>
,	,	kIx,	,
divadel	divadlo	k1gNnPc2	divadlo
a	a	k8xC	a
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
mediálních	mediální	k2eAgInPc2d1	mediální
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgInPc2d1	filmový
<g/>
,	,	kIx,	,
inzertních	inzertní	k2eAgInPc2d1	inzertní
a	a	k8xC	a
postprodukčních	postprodukční	k2eAgFnPc2d1	postprodukční
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Soho	Soho	k6eAd1	Soho
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
známé	známý	k2eAgNnSc1d1	známé
svými	svůj	k3xOyFgInPc7	svůj
rušnými	rušný	k2eAgInPc7d1	rušný
kluby	klub	k1gInPc7	klub
<g/>
,	,	kIx,	,
bary	bar	k1gInPc1	bar
a	a	k8xC	a
gay	gay	k1gMnSc1	gay
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
je	být	k5eAaImIp3nS	být
známou	známý	k2eAgFnSc7d1	známá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
tepnou	tepna	k1gFnSc7	tepna
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
z	z	k7c2	z
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
Circus	Circus	k1gInSc1	Circus
na	na	k7c6	na
východě	východ	k1gInSc6	východ
k	k	k7c3	k
Hyde	Hyde	k1gNnSc3	Hyde
Park	park	k1gInSc1	park
Corner	Corner	k1gInSc1	Corner
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
Mayfair	Mayfair	k1gInSc4	Mayfair
a	a	k8xC	a
Green	Green	k2eAgInSc4d1	Green
Park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
dopravními	dopravní	k2eAgFnPc7d1	dopravní
tepnami	tepna	k1gFnPc7	tepna
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
Regent	regent	k1gMnSc1	regent
Street	Street	k1gMnSc1	Street
a	a	k8xC	a
Bond	bond	k1gInSc1	bond
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc4d1	východní
Londýn	Londýn	k1gInSc4	Londýn
byl	být	k5eAaImAgMnS	být
centrem	centrum	k1gNnSc7	centrum
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
výrazně	výrazně	k6eAd1	výrazně
přestavována	přestavován	k2eAgFnSc1d1	přestavována
jako	jako	k8xS	jako
část	část	k1gFnSc1	část
Thames	Thamesa	k1gFnPc2	Thamesa
Gateway	Gatewaa	k1gFnSc2	Gatewaa
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
rovněž	rovněž	k9	rovněž
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c6	na
pořádání	pořádání	k1gNnSc6	pořádání
olympijský	olympijský	k2eAgMnSc1d1	olympijský
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
probíhá	probíhat	k5eAaImIp3nS	probíhat
výrazná	výrazný	k2eAgFnSc1d1	výrazná
obnova	obnova	k1gFnSc1	obnova
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
pro	pro	k7c4	pro
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
konání	konání	k1gNnSc2	konání
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
výrazně	výrazně	k6eAd1	výrazně
přestavována	přestavován	k2eAgFnSc1d1	přestavována
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
náletech	nálet	k1gInPc6	nálet
německého	německý	k2eAgNnSc2d1	německé
letectva	letectvo	k1gNnSc2	letectvo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
East	East	k2eAgMnSc1d1	East
End	End	k1gMnSc1	End
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
původnímu	původní	k2eAgInSc3d1	původní
londýnskému	londýnský	k2eAgInSc3d1	londýnský
přístavu	přístav	k1gInSc3	přístav
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oblastí	oblast	k1gFnSc7	oblast
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
zpočátku	zpočátku	k6eAd1	zpočátku
usidlovali	usidlovat	k5eAaImAgMnP	usidlovat
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
přijíždějící	přijíždějící	k2eAgMnPc1d1	přijíždějící
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
postupně	postupně	k6eAd1	postupně
přistěhovalecké	přistěhovalecký	k2eAgFnPc1d1	přistěhovalecká
vlny	vlna	k1gFnPc1	vlna
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
,	,	kIx,	,
Hugenotů	hugenot	k1gMnPc2	hugenot
<g/>
,	,	kIx,	,
Belgičanů	Belgičan	k1gMnPc2	Belgičan
<g/>
,	,	kIx,	,
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
Pákistánců	Pákistánec	k1gMnPc2	Pákistánec
<g/>
.	.	kIx.	.
</s>
<s>
East	East	k1gMnSc1	East
End	End	k1gMnSc1	End
se	se	k3xPyFc4	se
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
City	City	k1gFnSc2	City
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tyto	tento	k3xDgFnPc4	tento
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
-	-	kIx~	-
Whitechapel	Whitechapel	k1gMnSc1	Whitechapel
<g/>
,	,	kIx,	,
Mile	mile	k6eAd1	mile
End	End	k1gMnPc1	End
<g/>
,	,	kIx,	,
Bethnal	Bethnal	k1gMnPc1	Bethnal
Green	Grena	k1gFnPc2	Grena
<g/>
,	,	kIx,	,
Hackney	Hacknea	k1gFnSc2	Hacknea
<g/>
,	,	kIx,	,
Bow	Bow	k1gFnSc2	Bow
a	a	k8xC	a
Poplar	Poplara	k1gFnPc2	Poplara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mnoha	mnoho	k4c2	mnoho
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
místa	místo	k1gNnPc4	místo
včetně	včetně	k7c2	včetně
londýnských	londýnský	k2eAgNnPc2d1	Londýnské
tržišť	tržiště	k1gNnPc2	tržiště
(	(	kIx(	(
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
Road	Roada	k1gFnPc2	Roada
Flower	Flowra	k1gFnPc2	Flowra
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Spitalfields	Spitalfields	k1gInSc1	Spitalfields
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Brick	Brick	k1gMnSc1	Brick
Lane	Lan	k1gFnSc2	Lan
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Petticoat	Petticoat	k2eAgInSc1d1	Petticoat
Lane	Lane	k1gInSc1	Lane
Market	market	k1gInSc1	market
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc4	několik
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Geffrye	Geffrye	k1gNnSc1	Geffrye
Museum	museum	k1gNnSc1	museum
a	a	k8xC	a
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Childhood	Childhooda	k1gFnPc2	Childhooda
v	v	k7c6	v
Bethnal	Bethnal	k1gFnSc6	Bethnal
Green	Grena	k1gFnPc2	Grena
<g/>
.	.	kIx.	.
</s>
<s>
Docklands	Docklands	k1gInSc1	Docklands
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
na	na	k7c6	na
Ostrově	ostrov	k1gInSc6	ostrov
psů	pes	k1gMnPc2	pes
(	(	kIx(	(
<g/>
Isle	Isle	k1gInSc1	Isle
of	of	k?	of
Dogs	Dogs	k1gInSc1	Dogs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
bouřlivým	bouřlivý	k2eAgInSc7d1	bouřlivý
rozvojem	rozvoj	k1gInSc7	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c4	mnoho
skladů	sklad	k1gInPc2	sklad
ve	v	k7c4	v
Wapping	Wapping	k1gInSc4	Wapping
obsazováno	obsazovat	k5eAaImNgNnS	obsazovat
a	a	k8xC	a
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k8xC	jako
umělecké	umělecký	k2eAgFnPc1d1	umělecká
dílny	dílna	k1gFnPc1	dílna
a	a	k8xC	a
levné	levný	k2eAgFnPc1d1	levná
ubytovny	ubytovna	k1gFnPc1	ubytovna
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
Docklands	Docklandsa	k1gFnPc2	Docklandsa
Development	Development	k1gMnSc1	Development
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
LDDC	LDDC	kA	LDDC
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
proces	proces	k1gInSc4	proces
využití	využití	k1gNnSc2	využití
a	a	k8xC	a
změny	změna	k1gFnSc2	změna
tváře	tvář	k1gFnSc2	tvář
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
Canary	Canara	k1gFnSc2	Canara
Wharf	Wharf	k1gMnSc1	Wharf
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
dominantou	dominanta	k1gFnSc7	dominanta
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
kancelářského	kancelářský	k2eAgInSc2d1	kancelářský
komplexu	komplex	k1gInSc2	komplex
1	[number]	k4	1
Canada	Canada	k1gFnSc1	Canada
Square	square	k1gInSc4	square
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
britský	britský	k2eAgInSc1d1	britský
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
metrem	metro	k1gNnSc7	metro
a	a	k8xC	a
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
částmi	část	k1gFnPc7	část
města	město	k1gNnSc2	město
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Docklands	Docklands	k1gInSc1	Docklands
Light	Lighta	k1gFnPc2	Lighta
Railway	Railwaa	k1gFnSc2	Railwaa
(	(	kIx(	(
<g/>
DLR	DLR	kA	DLR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInSc1d1	západní
Londýn	Londýn	k1gInSc1	Londýn
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
elegantní	elegantní	k2eAgFnSc4d1	elegantní
a	a	k8xC	a
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
rezidenční	rezidenční	k2eAgFnSc4d1	rezidenční
oblast	oblast	k1gFnSc4	oblast
Notting	Notting	k1gInSc1	Notting
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Kensington	Kensington	k1gInSc1	Kensington
a	a	k8xC	a
Chelsea	Chelseum	k1gNnPc1	Chelseum
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
nejluxusnějším	luxusní	k2eAgNnSc7d3	nejluxusnější
bydlením	bydlení	k1gNnSc7	bydlení
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
u	u	k7c2	u
White	Whit	k1gInSc5	Whit
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
Shepherd	Shepherd	k1gInSc1	Shepherd
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavní	hlavní	k2eAgNnSc1d1	hlavní
centrum	centrum	k1gNnSc1	centrum
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
v	v	k7c6	v
předměstí	předměstí	k1gNnSc6	předměstí
Hillingdon	Hillingdona	k1gFnPc2	Hillingdona
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
letiště	letiště	k1gNnSc4	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kout	kout	k1gInSc1	kout
Londýna	Londýn	k1gInSc2	Londýn
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
Richmond	Richmond	k1gInSc4	Richmond
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
stadion	stadion	k1gInSc4	stadion
Chelsea	Chelse	k1gInSc2	Chelse
FC	FC	kA	FC
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
Stamford	Stamford	k1gInSc4	Stamford
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Londýně	Londýn	k1gInSc6	Londýn
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
(	(	kIx(	(
<g/>
známý	známý	k2eAgInSc4d1	známý
pořádáním	pořádání	k1gNnSc7	pořádání
velkého	velký	k2eAgInSc2d1	velký
tenisového	tenisový	k2eAgInSc2d1	tenisový
turnaje	turnaj	k1gInSc2	turnaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Greenwich	Greenwich	k1gInSc4	Greenwich
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Temže	Temže	k1gFnSc2	Temže
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tato	tento	k3xDgFnSc1	tento
řeka	řeka	k1gFnSc1	řeka
meandruje	meandrovat	k5eAaImIp3nS	meandrovat
je	on	k3xPp3gNnPc4	on
známá	známý	k2eAgNnPc4d1	známé
parkem	park	k1gInSc7	park
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
sídlem	sídlo	k1gNnSc7	sídlo
Královské	královský	k2eAgFnSc2d1	královská
greenwichské	greenwichský	k2eAgFnSc2d1	greenwichská
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Brixton	Brixton	k1gInSc1	Brixton
<g/>
,	,	kIx,	,
Camberwell	Camberwell	k1gInSc1	Camberwell
a	a	k8xC	a
Peckham	Peckham	k1gInSc1	Peckham
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
rodin	rodina	k1gFnPc2	rodina
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
v	v	k7c6	v
létech	léto	k1gNnPc6	léto
padesátých	padesátý	k4xOgNnPc2	padesátý
<g/>
,	,	kIx,	,
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
Londýn	Londýn	k1gInSc1	Londýn
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
například	například	k6eAd1	například
předměstí	předměstí	k1gNnSc6	předměstí
Hampstead	Hampstead	k1gInSc4	Hampstead
a	a	k8xC	a
Highgate	Highgat	k1gMnSc5	Highgat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
podržely	podržet	k5eAaPmAgFnP	podržet
venkovský	venkovský	k2eAgInSc4d1	venkovský
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
velké	velký	k2eAgInPc1d1	velký
parky	park	k1gInPc1	park
včetně	včetně	k7c2	včetně
Hampstead	Hampstead	k1gInSc4	Hampstead
Heath	Heath	k1gInSc4	Heath
s	s	k7c7	s
Parliament	Parliament	k1gMnSc1	Parliament
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
oblastí	oblast	k1gFnPc2	oblast
má	mít	k5eAaImIp3nS	mít
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zastoupení	zastoupení	k1gNnSc4	zastoupení
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
napřiklad	napřiklad	k6eAd1	napřiklad
Stamford	Stamford	k1gMnSc1	Stamford
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
obydlen	obydlen	k2eAgInSc1d1	obydlen
výraznou	výrazný	k2eAgFnSc7d1	výrazná
komunitou	komunita	k1gFnSc7	komunita
ortodoxních	ortodoxní	k2eAgMnPc2d1	ortodoxní
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
a	a	k8xC	a
Green	Green	k2eAgInSc1d1	Green
Lanes	Lanes	k1gInSc1	Lanes
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Harringey	Harringea	k1gFnSc2	Harringea
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
zastoupením	zastoupení	k1gNnSc7	zastoupení
turecké	turecký	k2eAgFnSc2d1	turecká
a	a	k8xC	a
řecké	řecký	k2eAgFnSc2d1	řecká
menšiny	menšina	k1gFnSc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Islington	Islington	k1gInSc1	Islington
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
oblastí	oblast	k1gFnPc2	oblast
severního	severní	k2eAgInSc2d1	severní
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
Arsenal	Arsenal	k1gFnSc2	Arsenal
nebo	nebo	k8xC	nebo
Tottenham	Tottenham	k1gInSc1	Tottenham
Hotspur	Hotspura	k1gFnPc2	Hotspura
FC	FC	kA	FC
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
stadión	stadión	k1gInSc1	stadión
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
Tottenhamu	Tottenham	k1gInSc6	Tottenham
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
význam	význam	k1gInSc1	význam
Londýna	Londýn	k1gInSc2	Londýn
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
jeho	jeho	k3xOp3gMnPc4	jeho
včasnou	včasný	k2eAgFnSc7d1	včasná
poválečnou	poválečný	k2eAgFnSc7d1	poválečná
orientací	orientace	k1gFnSc7	orientace
na	na	k7c4	na
odvětví	odvětví	k1gNnSc4	odvětví
poskytování	poskytování	k1gNnSc2	poskytování
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
centrálou	centrála	k1gFnSc7	centrála
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
New	New	k1gMnSc7	New
Yorkem	York	k1gInSc7	York
a	a	k8xC	a
Tokiem	Tokio	k1gNnSc7	Tokio
mezi	mezi	k7c4	mezi
tři	tři	k4xCgNnPc4	tři
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
ekonomická	ekonomický	k2eAgNnPc4d1	ekonomické
centra	centrum	k1gNnPc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
světovým	světový	k2eAgNnSc7d1	světové
finančním	finanční	k2eAgNnSc7d1	finanční
střediskem	středisko	k1gNnSc7	středisko
a	a	k8xC	a
po	po	k7c6	po
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
Paříží	Paříž	k1gFnPc2	Paříž
šestou	šestý	k4xOgFnSc7	šestý
největší	veliký	k2eAgFnSc7d3	veliký
městskou	městský	k2eAgFnSc7d1	městská
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
produkuje	produkovat	k5eAaImIp3nS	produkovat
asi	asi	k9	asi
20	[number]	k4	20
%	%	kIx~	%
HDP	HDP	kA	HDP
a	a	k8xC	a
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
HDP	HDP	kA	HDP
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
85	[number]	k4	85
%	%	kIx~	%
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
lidí	člověk	k1gMnPc2	člověk
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
odvětví	odvětví	k1gNnSc6	odvětví
poskytování	poskytování	k1gNnSc4	poskytování
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
asi	asi	k9	asi
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
podnicích	podnik	k1gInPc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
jich	on	k3xPp3gMnPc2	on
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
asi	asi	k9	asi
15	[number]	k4	15
000	[number]	k4	000
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
tiskařství	tiskařství	k1gNnSc4	tiskařství
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
nábytku	nábytek	k1gInSc2	nábytek
a	a	k8xC	a
produkci	produkce	k1gFnSc4	produkce
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
finanční	finanční	k2eAgInSc1d1	finanční
sektor	sektor	k1gInSc1	sektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poskytování	poskytování	k1gNnSc6	poskytování
finančních	finanční	k2eAgFnPc2d1	finanční
služeb	služba	k1gFnPc2	služba
pracuje	pracovat	k5eAaImIp3nS	pracovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
než	než	k8xS	než
480	[number]	k4	480
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
City	city	k1gNnSc6	city
je	být	k5eAaImIp3nS	být
proinvestováno	proinvestovat	k5eAaPmNgNnS	proinvestovat
více	hodně	k6eAd2	hodně
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
deseti	deset	k4xCc6	deset
největších	veliký	k2eAgNnPc6d3	veliký
evropských	evropský	k2eAgNnPc6d1	Evropské
městech	město	k1gNnPc6	město
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
více	hodně	k6eAd2	hodně
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
telefonních	telefonní	k2eAgInPc2d1	telefonní
hovorů	hovor	k1gInPc2	hovor
než	než	k8xS	než
kdekoli	kdekoli	k6eAd1	kdekoli
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
finančním	finanční	k2eAgInSc7d1	finanční
a	a	k8xC	a
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
dostihuje	dostihovat	k5eAaImIp3nS	dostihovat
i	i	k9	i
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
sídlí	sídlet	k5eAaImIp3nP	sídlet
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
brokerské	brokerský	k2eAgFnSc2d1	brokerská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
<g/>
,	,	kIx,	,
právní	právní	k2eAgFnSc2d1	právní
a	a	k8xC	a
účetní	účetní	k2eAgFnSc2d1	účetní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
kancelářské	kancelářský	k2eAgNnSc1d1	kancelářské
středisko	středisko	k1gNnSc1	středisko
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
City	City	k1gFnSc2	City
v	v	k7c4	v
Canary	Canara	k1gFnPc4	Canara
Wharf	Wharf	k1gInSc4	Wharf
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
světové	světový	k2eAgFnSc2d1	světová
centrály	centrála	k1gFnSc2	centrála
společností	společnost	k1gFnPc2	společnost
HSBC	HSBC	kA	HSBC
<g/>
,	,	kIx,	,
Reuters	Reuters	k1gInSc1	Reuters
<g/>
,	,	kIx,	,
Barclays	Barclays	k1gInSc1	Barclays
a	a	k8xC	a
Clifford	Clifford	k1gInSc1	Clifford
Chance	Chanec	k1gInSc2	Chanec
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
právní	právní	k2eAgFnSc1d1	právní
společnost	společnost	k1gFnSc1	společnost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
důležitým	důležitý	k2eAgNnSc7d1	důležité
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
turistika	turistika	k1gFnSc1	turistika
<g/>
,	,	kIx,	,
zaměstnávající	zaměstnávající	k2eAgFnSc1d1	zaměstnávající
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
asi	asi	k9	asi
350	[number]	k4	350
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
roční	roční	k2eAgInSc4d1	roční
příjem	příjem	k1gInSc4	příjem
z	z	k7c2	z
turistiky	turistika	k1gFnSc2	turistika
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
město	město	k1gNnSc1	město
navštíví	navštívit	k5eAaPmIp3nS	navštívit
27	[number]	k4	27
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
stráví	strávit	k5eAaPmIp3nP	strávit
alespoň	alespoň	k9	alespoň
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Londýnský	londýnský	k2eAgInSc1d1	londýnský
přístav	přístav	k1gInSc1	přístav
v	v	k7c4	v
Tilbury	Tilbur	k1gInPc4	Tilbur
se	se	k3xPyFc4	se
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
největšího	veliký	k2eAgInSc2d3	veliký
světového	světový	k2eAgInSc2d1	světový
přístavu	přístav	k1gInSc2	přístav
dostal	dostat	k5eAaPmAgInS	dostat
až	až	k9	až
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
jím	on	k3xPp3gInSc7	on
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velký	velký	k2eAgInSc1d1	velký
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
32	[number]	k4	32
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
londýnské	londýnský	k2eAgFnSc2d1	londýnská
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
samosprávy	samospráva	k1gFnPc1	samospráva
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
jednotkou	jednotka	k1gFnSc7	jednotka
místní	místní	k2eAgFnSc2d1	místní
správy	správa	k1gFnSc2	správa
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgMnPc4d1	zodpovědný
za	za	k7c4	za
zajištění	zajištění	k1gNnSc4	zajištění
většiny	většina	k1gFnSc2	většina
lokálních	lokální	k2eAgFnPc2d1	lokální
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
spravují	spravovat	k5eAaImIp3nP	spravovat
<g/>
.	.	kIx.	.
</s>
<s>
City	City	k1gFnSc1	City
není	být	k5eNaImIp3nS	být
spravována	spravovat	k5eAaImNgFnS	spravovat
běžnou	běžný	k2eAgFnSc7d1	běžná
místní	místní	k2eAgFnSc7d1	místní
radou	rada	k1gFnSc7	rada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
historickou	historický	k2eAgFnSc4d1	historická
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Greater	Greater	k1gMnSc1	Greater
London	London	k1gMnSc1	London
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
GLA	GLA	kA	GLA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
orgán	orgán	k1gInSc1	orgán
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
dění	dění	k1gNnSc4	dění
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
koordinaci	koordinace	k1gFnSc4	koordinace
činností	činnost	k1gFnPc2	činnost
samospráv	samospráva	k1gFnPc2	samospráva
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
strategické	strategický	k2eAgNnSc4d1	strategické
plánování	plánování	k1gNnSc4	plánování
a	a	k8xC	a
chod	chod	k1gInSc4	chod
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
dosah	dosah	k1gInSc4	dosah
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
požární	požární	k2eAgFnSc1d1	požární
ochrana	ochrana	k1gFnSc1	ochrana
a	a	k8xC	a
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
GLA	GLA	kA	GLA
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Assembly	Assembly	k1gMnSc1	Assembly
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
řídí	řídit	k5eAaImIp3nS	řídit
starosta	starosta	k1gMnSc1	starosta
Londýna	Londýn	k1gInSc2	Londýn
(	(	kIx(	(
<g/>
Mayor	Mayor	k1gMnSc1	Mayor
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
starosta	starosta	k1gMnSc1	starosta
Boris	Boris	k1gMnSc1	Boris
Johnson	Johnson	k1gMnSc1	Johnson
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
GLA	GLA	kA	GLA
byla	být	k5eAaImAgFnS	být
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
Radu	rada	k1gFnSc4	rada
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
(	(	kIx(	(
<g/>
Greater	Greater	k1gMnSc1	Greater
London	London	k1gMnSc1	London
Council	Council	k1gMnSc1	Council
<g/>
,	,	kIx,	,
GLC	GLC	kA	GLC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zrušen	zrušen	k2eAgInSc1d1	zrušen
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
po	po	k7c6	po
politických	politický	k2eAgFnPc6d1	politická
šarvátkách	šarvátka	k1gFnPc6	šarvátka
mezi	mezi	k7c7	mezi
GLC	GLC	kA	GLC
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
vedeným	vedený	k2eAgMnSc7d1	vedený
Kenem	Ken	k1gMnSc7	Ken
Livingstonem	Livingston	k1gInSc7	Livingston
<g/>
)	)	kIx)	)
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
vedenou	vedený	k2eAgFnSc4d1	vedená
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherovou	Thatcherová	k1gFnSc4	Thatcherová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
GLC	GLC	kA	GLC
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
přešla	přejít	k5eAaPmAgFnS	přejít
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
pravomocí	pravomoc	k1gFnPc2	pravomoc
na	na	k7c6	na
samosprávy	samospráva	k1gFnSc2	samospráva
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
byl	být	k5eAaImAgInS	být
zajišťován	zajišťovat	k5eAaImNgInS	zajišťovat
jejich	jejich	k3xOp3gFnSc3	jejich
spolupráci	spolupráce	k1gFnSc3	spolupráce
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgInPc7d1	jiný
nevolenými	volený	k2eNgInPc7d1	nevolený
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Samosprávy	samospráva	k1gFnPc1	samospráva
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
tak	tak	k6eAd1	tak
získaly	získat	k5eAaPmAgFnP	získat
značnou	značný	k2eAgFnSc4d1	značná
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
jednotný	jednotný	k2eAgInSc4d1	jednotný
statut	statut	k1gInSc4	statut
<g/>
,	,	kIx,	,
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
GLC	GLC	kA	GLC
ztratily	ztratit	k5eAaPmAgFnP	ztratit
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
GLC	GLC	kA	GLC
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
londýnské	londýnský	k2eAgInPc1d1	londýnský
administrativní	administrativní	k2eAgInPc1d1	administrativní
úřady	úřad	k1gInPc1	úřad
<g/>
:	:	kIx,	:
Metropolitan	metropolitan	k1gInSc1	metropolitan
Board	Board	k1gInSc1	Board
of	of	k?	of
Works	Works	kA	Works
(	(	kIx(	(
<g/>
MBW	MBW	kA	MBW
<g/>
)	)	kIx)	)
-	-	kIx~	-
1855	[number]	k4	1855
až	až	k9	až
1889	[number]	k4	1889
London	London	k1gMnSc1	London
County	Counta	k1gFnSc2	Counta
Council	Council	k1gMnSc1	Council
(	(	kIx(	(
<g/>
LCC	LCC	kA	LCC
<g/>
)	)	kIx)	)
-	-	kIx~	-
1889	[number]	k4	1889
až	až	k9	až
1965	[number]	k4	1965
Greater	Greater	k1gMnSc1	Greater
London	London	k1gMnSc1	London
Council	Council	k1gMnSc1	Council
(	(	kIx(	(
<g/>
GLC	GLC	kA	GLC
<g/>
)	)	kIx)	)
-	-	kIx~	-
1965	[number]	k4	1965
do	do	k7c2	do
1986	[number]	k4	1986
Greater	Greatrum	k1gNnPc2	Greatrum
London	London	k1gMnSc1	London
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
GLA	GLA	kA	GLA
<g/>
)	)	kIx)	)
-	-	kIx~	-
2000	[number]	k4	2000
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
reprezentován	reprezentovat	k5eAaImNgMnS	reprezentovat
74	[number]	k4	74
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgFnSc4d1	policejní
službu	služba	k1gFnSc4	služba
pro	pro	k7c4	pro
32	[number]	k4	32
londýnských	londýnský	k2eAgFnPc2d1	londýnská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Metropolitan	metropolitan	k1gInSc1	metropolitan
Police	police	k1gFnSc2	police
Service	Service	k1gFnSc2	Service
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
označována	označován	k2eAgFnSc1d1	označována
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
Metropolitan	metropolitan	k1gInSc4	metropolitan
Police	police	k1gFnSc2	police
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
Met	Mety	k1gFnPc2	Mety
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
policejní	policejní	k2eAgFnSc4d1	policejní
správu	správa	k1gFnSc4	správa
City	City	k1gFnSc2	City
of	of	k?	of
London	London	k1gMnSc1	London
Police	police	k1gFnSc2	police
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
služba	služba	k1gFnSc1	služba
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
vládou	vláda	k1gFnSc7	vláda
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Národní	národní	k2eAgFnSc2d1	národní
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
NHS	NHS	kA	NHS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c4	mezi
5	[number]	k4	5
Strategic	Strategice	k1gFnPc2	Strategice
Health	Health	k1gMnSc1	Health
Authorities	Authorities	k1gMnSc1	Authorities
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
dopravy	doprava	k1gFnSc2	doprava
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejrušnějších	rušný	k2eAgNnPc2d3	nejrušnější
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
spravována	spravovat	k5eAaImNgFnS	spravovat
úřadem	úřad	k1gInSc7	úřad
starosty	starosta	k1gMnSc2	starosta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
finanční	finanční	k2eAgInSc1d1	finanční
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Výkonným	výkonný	k2eAgInSc7d1	výkonný
orgánem	orgán	k1gInSc7	orgán
řízení	řízení	k1gNnSc2	řízení
londýnského	londýnský	k2eAgInSc2d1	londýnský
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
Transport	transport	k1gInSc1	transport
for	forum	k1gNnPc2	forum
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
TfL	TfL	k1gMnSc1	TfL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejrozsáhlejších	rozsáhlý	k2eAgFnPc2d3	nejrozsáhlejší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
přehuštěním	přehuštění	k1gNnSc7	přehuštění
a	a	k8xC	a
výpadky	výpadek	k1gInPc7	výpadek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejkomplexnějších	komplexní	k2eAgInPc2d3	nejkomplexnější
dopravních	dopravní	k2eAgInPc2d1	dopravní
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
nedávno	nedávno	k6eAd1	nedávno
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
systém	systém	k1gInSc1	systém
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
dopravním	dopravní	k2eAgInSc7d1	dopravní
systémem	systém	k1gInSc7	systém
Londýna	Londýn	k1gInSc2	Londýn
je	být	k5eAaImIp3nS	být
metro	metro	k1gNnSc1	metro
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dvanácti	dvanáct	k4xCc7	dvanáct
linkami	linka	k1gFnPc7	linka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgNnSc4	první
metro	metro	k1gNnSc4	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
otevřené	otevřený	k2eAgFnSc2d1	otevřená
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
i	i	k9	i
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
první	první	k4xOgFnSc1	první
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
linka	linka	k1gFnSc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
je	být	k5eAaImIp3nS	být
využito	využít	k5eAaPmNgNnS	využít
pro	pro	k7c4	pro
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
pro	pro	k7c4	pro
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Trasy	trasa	k1gFnPc1	trasa
metra	metro	k1gNnSc2	metro
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
centrum	centrum	k1gNnSc4	centrum
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
bylo	být	k5eAaImAgNnS	být
metro	metro	k1gNnSc1	metro
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c4	o
systém	systém	k1gInSc4	systém
lehké	lehký	k2eAgFnSc2d1	lehká
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
DLR	DLR	kA	DLR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
používá	používat	k5eAaImIp3nS	používat
menší	malý	k2eAgFnPc4d2	menší
a	a	k8xC	a
lehčí	lehký	k2eAgFnPc4d2	lehčí
soupravy	souprava	k1gFnPc4	souprava
bez	bez	k7c2	bez
řidiče	řidič	k1gInSc2	řidič
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
síť	síť	k1gFnSc1	síť
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
oblast	oblast	k1gFnSc4	oblast
východního	východní	k2eAgInSc2d1	východní
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
Greenwiche	Greenwich	k1gInSc2	Greenwich
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnPc1d1	místní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
železniční	železniční	k2eAgFnPc1d1	železniční
spoje	spoj	k1gFnPc1	spoj
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
některých	některý	k3yIgFnPc2	některý
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
neprojíždějí	projíždět	k5eNaImIp3nP	projíždět
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
na	na	k7c6	na
železničních	železniční	k2eAgFnPc6d1	železniční
stanicích	stanice	k1gFnPc6	stanice
rozptýlených	rozptýlený	k2eAgInPc2d1	rozptýlený
po	po	k7c6	po
okraji	okraj	k1gInSc6	okraj
jeho	on	k3xPp3gNnSc2	on
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstající	vzrůstající	k2eAgInPc4d1	vzrůstající
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
dopravu	doprava	k1gFnSc4	doprava
vedly	vést	k5eAaImAgFnP	vést
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
podzemní	podzemní	k2eAgFnSc2d1	podzemní
východo-západní	východoápadní	k2eAgFnSc2d1	východo-západní
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
pod	pod	k7c7	pod
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
Crossrail	Crossrail	k1gInSc1	Crossrail
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
momentálně	momentálně	k6eAd1	momentálně
buduje	budovat	k5eAaImIp3nS	budovat
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
zprovoznění	zprovoznění	k1gNnSc1	zprovoznění
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Rychlovlaky	rychlovlak	k1gInPc1	rychlovlak
Eurostar	Eurostara	k1gFnPc2	Eurostara
spojují	spojovat	k5eAaImIp3nP	spojovat
nádraží	nádraží	k1gNnSc2	nádraží
St	St	kA	St
Pancras	Pancras	k1gMnSc1	Pancras
International	International	k1gMnSc1	International
s	s	k7c7	s
Paříží	Paříž	k1gFnSc7	Paříž
a	a	k8xC	a
Bruselem	Brusel	k1gInSc7	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
do	do	k7c2	do
Bruselu	Brusel	k1gInSc2	Brusel
asi	asi	k9	asi
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
51	[number]	k4	51
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
londýnské	londýnský	k2eAgFnSc2d1	londýnská
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejrozsáhlejších	rozsáhlý	k2eAgFnPc2d3	nejrozsáhlejší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
asi	asi	k9	asi
8	[number]	k4	8
000	[number]	k4	000
autobusů	autobus	k1gInPc2	autobus
na	na	k7c4	na
700	[number]	k4	700
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
týdně	týdně	k6eAd1	týdně
přepraví	přepravit	k5eAaPmIp3nS	přepravit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
dopravní	dopravní	k2eAgFnSc2d1	dopravní
sítě	síť	k1gFnSc2	síť
investováno	investován	k2eAgNnSc1d1	investováno
850	[number]	k4	850
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
Londýn	Londýn	k1gInSc1	Londýn
má	mít	k5eAaImIp3nS	mít
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3	nejrozsáhlejší
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
použitelnou	použitelný	k2eAgFnSc4d1	použitelná
vozíčkáři	vozíčkář	k1gMnSc6	vozíčkář
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
přístupná	přístupný	k2eAgFnSc1d1	přístupná
i	i	k9	i
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
se	s	k7c7	s
sluchovým	sluchový	k2eAgNnSc7d1	sluchové
nebo	nebo	k8xC	nebo
zrakovým	zrakový	k2eAgNnSc7d1	zrakové
postižením	postižení	k1gNnSc7	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
je	být	k5eAaImIp3nS	být
nejrušnějším	rušný	k2eAgNnSc7d3	nejrušnější
letištěm	letiště	k1gNnSc7	letiště
světa	svět	k1gInSc2	svět
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
již	již	k9	již
pátý	pátý	k4xOgInSc1	pátý
terminál	terminál	k1gInSc1	terminál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
ročně	ročně	k6eAd1	ročně
odbavit	odbavit	k5eAaPmF	odbavit
až	až	k9	až
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
důležitá	důležitý	k2eAgNnPc1d1	důležité
letiště	letiště	k1gNnPc1	letiště
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
Stansted	Stansted	k1gInSc1	Stansted
<g/>
,	,	kIx,	,
Gatwick	Gatwick	k1gInSc1	Gatwick
<g/>
,	,	kIx,	,
Luton	Luton	k1gNnSc1	Luton
<g/>
,	,	kIx,	,
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Southend	Southend	k1gMnSc1	Southend
a	a	k8xC	a
Biggin	Biggin	k1gMnSc1	Biggin
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
především	především	k9	především
obchodní	obchodní	k2eAgMnPc4d1	obchodní
cestující	cestující	k1gMnPc4	cestující
a	a	k8xC	a
také	také	k9	také
nízkonákladové	nízkonákladový	k2eAgFnSc2d1	nízkonákladová
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
vyslal	vyslat	k5eAaPmAgMnS	vyslat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
597	[number]	k4	597
svatého	svatý	k1gMnSc2	svatý
Augustina	Augustin	k1gMnSc2	Augustin
z	z	k7c2	z
Canterbury	Canterbura	k1gFnSc2	Canterbura
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
vyslanec	vyslanec	k1gMnSc1	vyslanec
stane	stanout	k5eAaPmIp3nS	stanout
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
londýnským	londýnský	k2eAgMnSc7d1	londýnský
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
římské	římský	k2eAgFnSc2d1	římská
kolonie	kolonie	k1gFnSc2	kolonie
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Augustin	Augustin	k1gMnSc1	Augustin
byl	být	k5eAaImAgMnS	být
pohostinně	pohostinně	k6eAd1	pohostinně
přijat	přijmout	k5eAaPmNgMnS	přijmout
v	v	k7c6	v
Kentském	kentský	k2eAgInSc6d1	kentský
království	království	k1gNnSc2	království
králem	král	k1gMnSc7	král
Ethelbertem	Ethelbert	k1gMnSc7	Ethelbert
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
601	[number]	k4	601
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
v	v	k7c4	v
Canterbury	Canterbur	k1gMnPc4	Canterbur
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
vždy	vždy	k6eAd1	vždy
důležitým	důležitý	k2eAgInSc7d1	důležitý
náboženským	náboženský	k2eAgInSc7d1	náboženský
centrem	centr	k1gInSc7	centr
a	a	k8xC	a
canterburští	canterburský	k2eAgMnPc1d1	canterburský
arcibiskupové	arcibiskup	k1gMnPc1	arcibiskup
zde	zde	k6eAd1	zde
trávili	trávit	k5eAaImAgMnP	trávit
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
oficiální	oficiální	k2eAgFnSc7d1	oficiální
londýnskou	londýnský	k2eAgFnSc7d1	londýnská
rezidencí	rezidence	k1gFnSc7	rezidence
je	být	k5eAaImIp3nS	být
Lambethský	Lambethský	k2eAgInSc1d1	Lambethský
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
dva	dva	k4xCgInPc4	dva
biskupy	biskup	k1gInPc4	biskup
-	-	kIx~	-
biskup	biskup	k1gInSc4	biskup
londýnský	londýnský	k2eAgInSc4d1	londýnský
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
část	část	k1gFnSc4	část
Londýna	Londýn	k1gInSc2	Londýn
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Temže	Temže	k1gFnSc2	Temže
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
něhož	jenž	k3xRgMnSc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
největší	veliký	k2eAgInSc1d3	veliký
londýnský	londýnský	k2eAgInSc1d1	londýnský
barokní	barokní	k2eAgInSc1d1	barokní
chrám	chrám	k1gInSc1	chrám
katedrála	katedrál	k1gMnSc2	katedrál
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
southwarský	southwarský	k2eAgMnSc1d1	southwarský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
anglikánskou	anglikánský	k2eAgFnSc4d1	anglikánská
církev	církev	k1gFnSc4	církev
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1	důležitá
národní	národní	k2eAgFnPc1d1	národní
a	a	k8xC	a
královské	královský	k2eAgFnPc1d1	královská
ceremonie	ceremonie	k1gFnPc1	ceremonie
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
mezi	mezi	k7c4	mezi
katedrálu	katedrála	k1gFnSc4	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
a	a	k8xC	a
Westminsterské	Westminsterský	k2eAgNnSc4d1	Westminsterské
opatství	opatství	k1gNnSc4	opatství
<g/>
,	,	kIx,	,
gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
připomínající	připomínající	k2eAgFnSc4d1	připomínající
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
westminsterský	westminsterský	k2eAgMnSc1d1	westminsterský
je	být	k5eAaImIp3nS	být
uznáván	uznáván	k2eAgInSc1d1	uznáván
jako	jako	k8xS	jako
hlava	hlava	k1gFnSc1	hlava
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
něj	on	k3xPp3gMnSc4	on
patří	patřit	k5eAaImIp3nS	patřit
katolická	katolický	k2eAgFnSc1d1	katolická
Westminsterská	Westminsterský	k2eAgFnSc1d1	Westminsterská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odlišovat	odlišovat	k5eAaImF	odlišovat
od	od	k7c2	od
anglikánského	anglikánský	k2eAgNnSc2d1	anglikánské
Westminsterského	Westminsterský	k2eAgNnSc2d1	Westminsterské
opatství	opatství	k1gNnSc2	opatství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
tradiční	tradiční	k2eAgFnPc1d1	tradiční
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
rezidenci	rezidence	k1gFnSc4	rezidence
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
reformovaná	reformovaný	k2eAgFnSc1d1	reformovaná
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
Reformed	Reformed	k1gMnSc1	Reformed
Church	Church	k1gMnSc1	Church
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvakeři	kvaker	k1gMnPc1	kvaker
(	(	kIx(	(
<g/>
Quakers	Quakers	k1gInSc1	Quakers
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
imigrantů	imigrant	k1gMnPc2	imigrant
ustavilo	ustavit	k5eAaPmAgNnS	ustavit
své	své	k1gNnSc4	své
vlastní	vlastní	k2eAgFnSc2d1	vlastní
církve	církev	k1gFnSc2	církev
-	-	kIx~	-
například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
také	také	k9	také
další	další	k2eAgInPc1d1	další
protestantské	protestantský	k2eAgInPc1d1	protestantský
sbory	sbor	k1gInPc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
centrem	centrum	k1gNnSc7	centrum
islámu	islám	k1gInSc2	islám
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
londýnských	londýnský	k2eAgNnPc6d1	Londýnské
předměstích	předměstí	k1gNnPc6	předměstí
jsou	být	k5eAaImIp3nP	být
enklávy	enkláva	k1gFnPc1	enkláva
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
muslimů	muslim	k1gMnPc2	muslim
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
-	-	kIx~	-
Tower	Tower	k1gInSc1	Tower
Hamlets	Hamlets	k1gInSc1	Hamlets
a	a	k8xC	a
Newham	Newham	k1gInSc1	Newham
<g/>
.	.	kIx.	.
</s>
<s>
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
centrální	centrální	k2eAgFnSc1d1	centrální
mešita	mešita	k1gFnSc1	mešita
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Central	Central	k1gMnSc2	Central
Mosque	Mosque	k1gNnSc2	Mosque
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
známá	známý	k2eAgFnSc1d1	známá
londýnská	londýnský	k2eAgFnSc1d1	londýnská
silueta	silueta	k1gFnSc1	silueta
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Regent	regent	k1gMnSc1	regent
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
mešity	mešita	k1gFnPc1	mešita
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Londýnu	Londýn	k1gInSc3	Londýn
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
islámskou	islámský	k2eAgFnSc4d1	islámská
menšinu	menšina	k1gFnSc4	menšina
také	také	k9	také
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Londonistán	Londonistán	k1gInSc1	Londonistán
<g/>
.	.	kIx.	.
</s>
<s>
Hinduistický	hinduistický	k2eAgInSc1d1	hinduistický
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Neasden	Neasdna	k1gFnPc2	Neasdna
(	(	kIx(	(
<g/>
Neasdenský	Neasdenský	k2eAgInSc1d1	Neasdenský
chrám	chrám	k1gInSc1	chrám
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
hinduistickým	hinduistický	k2eAgInSc7d1	hinduistický
chrámem	chrám	k1gInSc7	chrám
mimo	mimo	k7c4	mimo
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pozoruhodným	pozoruhodný	k2eAgInSc7d1	pozoruhodný
příkladem	příklad	k1gInSc7	příklad
moderní	moderní	k2eAgFnSc2d1	moderní
budovy	budova	k1gFnSc2	budova
postavené	postavený	k2eAgFnSc2d1	postavená
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dokonale	dokonale	k6eAd1	dokonale
propracovaných	propracovaný	k2eAgFnPc2d1	propracovaná
a	a	k8xC	a
spletitých	spletitý	k2eAgFnPc2d1	spletitá
mramorových	mramorový	k2eAgFnPc2d1	mramorová
plastik	plastika	k1gFnPc2	plastika
použitých	použitý	k2eAgFnPc2d1	použitá
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
byla	být	k5eAaImAgFnS	být
vytesáno	vytesán	k2eAgNnSc4d1	vytesáno
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc1	třetina
britských	britský	k2eAgMnPc2d1	britský
židů	žid	k1gMnPc2	žid
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
řadí	řadit	k5eAaImIp3nS	řadit
Londýn	Londýn	k1gInSc1	Londýn
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
židů	žid	k1gMnPc2	žid
žijících	žijící	k2eAgMnPc2d1	žijící
mimo	mimo	k7c4	mimo
Izrael	Izrael	k1gInSc4	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
asi	asi	k9	asi
378	[number]	k4	378
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
mnohé	mnohý	k2eAgInPc4d1	mnohý
a	a	k8xC	a
vědecké	vědecký	k2eAgInPc4d1	vědecký
a	a	k8xC	a
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
ústavy	ústav	k1gInPc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
125	[number]	k4	125
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
20	[number]	k4	20
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
několika	několik	k4yIc2	několik
dalších	další	k2eAgMnPc2d1	další
menších	malý	k2eAgMnPc2d2	menší
ústavů	ústav	k1gInPc2	ústav
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
mírou	míra	k1gFnSc7	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
fakulty	fakulta	k1gFnPc1	fakulta
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
všeobecně	všeobecně	k6eAd1	všeobecně
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
specializované	specializovaný	k2eAgInPc1d1	specializovaný
ústavy	ústav	k1gInPc1	ústav
například	například	k6eAd1	například
London	London	k1gMnSc1	London
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Economics	Economicsa	k1gFnPc2	Economicsa
<g/>
,	,	kIx,	,
SOAS	SOAS	kA	SOAS
<g/>
,	,	kIx,	,
Royal	Royal	k1gMnSc1	Royal
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Music	Music	k1gMnSc1	Music
a	a	k8xC	a
Institute	institut	k1gInSc5	institut
of	of	k?	of
Education	Education	k1gInSc1	Education
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
University	universita	k1gFnPc1	universita
College	Colleg	k1gFnSc2	Colleg
London	London	k1gMnSc1	London
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
The	The	k1gFnSc2	The
Times	Timesa	k1gFnPc2	Timesa
Higher	Highra	k1gFnPc2	Highra
Education	Education	k1gInSc1	Education
Supplement	Supplement	k1gMnSc1	Supplement
zařazeny	zařadit	k5eAaPmNgInP	zařadit
mezi	mezi	k7c4	mezi
desítku	desítka	k1gFnSc4	desítka
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
univerzit	univerzita	k1gFnPc2	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
ICL	ICL	kA	ICL
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
UCL	UCL	kA	UCL
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
univerzitami	univerzita	k1gFnPc7	univerzita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
Brunel	Brunel	k1gInSc4	Brunel
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
City	city	k1gNnSc1	city
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Metropolitan	metropolitan	k1gInSc4	metropolitan
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Královská	královský	k2eAgFnSc1d1	královská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Middlesex	Middlesex	k1gInSc1	Middlesex
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
University	universita	k1gFnSc2	universita
of	of	k?	of
East	East	k1gMnSc1	East
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Westminster	Westminster	k1gMnSc1	Westminster
a	a	k8xC	a
London	London	k1gMnSc1	London
South	Southa	k1gFnPc2	Southa
Bank	banka	k1gFnPc2	banka
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
svými	svůj	k3xOyFgFnPc7	svůj
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
školami	škola	k1gFnPc7	škola
-	-	kIx~	-
London	London	k1gMnSc1	London
Business	business	k1gInSc4	business
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
škola	škola	k1gFnSc1	škola
podle	podle	k7c2	podle
Business	business	k1gInSc4	business
Week	Week	k1gInSc1	Week
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cass	Cass	k1gInSc4	Cass
Business	business	k1gInSc1	business
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
škola	škola	k1gFnSc1	škola
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
galerií	galerie	k1gFnPc2	galerie
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jednak	jednak	k8xC	jednak
turistickými	turistický	k2eAgFnPc7d1	turistická
atrakcemi	atrakce	k1gFnPc7	atrakce
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
Přírodopisné	přírodopisný	k2eAgNnSc1d1	přírodopisné
muzeum	muzeum	k1gNnSc1	muzeum
-	-	kIx~	-
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc1	geologie
<g/>
;	;	kIx,	;
Science	Science	k1gFnSc1	Science
Museum	museum	k1gNnSc1	museum
a	a	k8xC	a
Victoria	Victorium	k1gNnSc2	Victorium
and	and	k?	and
Albert	Albert	k1gMnSc1	Albert
Museum	museum	k1gNnSc1	museum
-	-	kIx~	-
móda	móda	k1gFnSc1	móda
a	a	k8xC	a
design	design	k1gInSc1	design
<g/>
)	)	kIx)	)
plní	plnit	k5eAaImIp3nS	plnit
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
centra	centrum	k1gNnSc2	centrum
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
u	u	k7c2	u
St	St	kA	St
Pancras	Pancrasa	k1gFnPc2	Pancrasa
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
knihovnou	knihovna	k1gFnSc7	knihovna
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
maratónů	maratón	k1gInPc2	maratón
s	s	k7c7	s
masovou	masový	k2eAgFnSc7d1	masová
účastí	účast	k1gFnSc7	účast
<g/>
,	,	kIx,	,
Londýnského	londýnský	k2eAgInSc2d1	londýnský
maratónu	maratón	k1gInSc2	maratón
<g/>
,	,	kIx,	,
konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
a	a	k8xC	a
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
vybrán	vybrán	k2eAgInSc1d1	vybrán
jako	jako	k9	jako
místo	místo	k7c2	místo
pořádání	pořádání	k1gNnSc2	pořádání
Letních	letní	k2eAgFnPc2d1	letní
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
olympijským	olympijský	k2eAgMnSc7d1	olympijský
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
konaly	konat	k5eAaImAgFnP	konat
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanějším	navštěvovaný	k2eAgInSc7d3	nejnavštěvovanější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
sídlí	sídlet	k5eAaImIp3nS	sídlet
několik	několik	k4yIc1	několik
předních	přední	k2eAgInPc2d1	přední
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
nebyly	být	k5eNaImAgInP	být
londýnské	londýnský	k2eAgInPc1d1	londýnský
kluby	klub	k1gInPc1	klub
příliš	příliš	k6eAd1	příliš
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
v	v	k7c6	v
získávání	získávání	k1gNnSc6	získávání
nejcennějších	cenný	k2eAgFnPc2d3	nejcennější
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
trofejí	trofej	k1gFnPc2	trofej
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
například	například	k6eAd1	například
Liverpool	Liverpool	k1gInSc1	Liverpool
nebo	nebo	k8xC	nebo
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současný	současný	k2eAgMnSc1d1	současný
Arsenal	Arsenal	k1gMnSc1	Arsenal
(	(	kIx(	(
<g/>
založený	založený	k2eAgInSc1d1	založený
ve	v	k7c6	v
Woolwich	Woolwi	k1gFnPc6	Woolwi
Arsenal	Arsenal	k1gFnSc2	Arsenal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Highbury	Highbura	k1gFnSc2	Highbura
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
sídlí	sídlet	k5eAaImIp3nS	sídlet
Emirates	Emirates	k1gInSc1	Emirates
Stadium	stadium	k1gNnSc1	stadium
<g/>
)	)	kIx)	)
a	a	k8xC	a
Chelsea	Chelsea	k1gFnSc1	Chelsea
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
Fullhamu	Fullham	k1gInSc6	Fullham
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
a	a	k8xC	a
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
členy	člen	k1gMnPc4	člen
velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
-	-	kIx~	-
Big	Big	k1gFnSc2	Big
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
byly	být	k5eAaImAgFnP	být
první	první	k4xOgFnSc6	první
dvojicí	dvojice	k1gFnSc7	dvojice
londýnských	londýnský	k2eAgInPc2d1	londýnský
klubů	klub	k1gInPc2	klub
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
anglické	anglický	k2eAgFnSc6d1	anglická
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgInP	umístit
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
vítězným	vítězný	k2eAgInSc7d1	vítězný
Arsenalem	Arsenal	k1gInSc7	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
opakovala	opakovat	k5eAaImAgFnS	opakovat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Chlesea	Chlesea	k1gFnSc1	Chlesea
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
konání	konání	k1gNnSc2	konání
Mistrovství	mistrovství	k1gNnPc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
a	a	k8xC	a
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
finálových	finálový	k2eAgNnPc2d1	finálové
utkání	utkání	k1gNnPc2	utkání
obou	dva	k4xCgInPc2	dva
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
ve	v	k7c6	v
starém	staré	k1gNnSc6	staré
Wembley	Wemblea	k1gFnSc2	Wemblea
pětkrát	pětkrát	k6eAd1	pětkrát
hostil	hostit	k5eAaImAgMnS	hostit
finále	finále	k1gNnSc7	finále
PMEZ	PMEZ	kA	PMEZ
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
již	již	k6eAd1	již
zrekonstruovaném	zrekonstruovaný	k2eAgInSc6d1	zrekonstruovaný
stadionu	stadion	k1gInSc6	stadion
dvakrát	dvakrát	k6eAd1	dvakrát
finále	finále	k1gNnSc1	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Wembley	Wembley	k1gInPc1	Wembley
Stadium	stadium	k1gNnSc1	stadium
-	-	kIx~	-
národní	národní	k2eAgInSc1d1	národní
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadión	stadión	k1gInSc1	stadión
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
svědkem	svědek	k1gMnSc7	svědek
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgInPc2d1	významný
mezistátních	mezistátní	k2eAgInPc2d1	mezistátní
a	a	k8xC	a
klubových	klubový	k2eAgInPc2d1	klubový
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
a	a	k8xC	a
ragby	ragby	k1gNnSc3	ragby
utkání	utkání	k1gNnSc2	utkání
<g/>
,	,	kIx,	,
prošel	projít	k5eAaPmAgMnS	projít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
byl	být	k5eAaImAgInS	být
místem	místem	k6eAd1	místem
důležitých	důležitý	k2eAgInPc2d1	důležitý
zápasů	zápas	k1gInPc2	zápas
cardiffský	cardiffský	k2eAgInSc1d1	cardiffský
Millennium	millennium	k1gNnSc1	millennium
Stadium	stadium	k1gNnSc1	stadium
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgInSc7d1	populární
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
také	také	k9	také
ragby	ragby	k1gNnSc4	ragby
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
příznivci	příznivec	k1gMnPc7	příznivec
pocházejí	pocházet	k5eAaImIp3nP	pocházet
především	především	k9	především
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
společnosti	společnost	k1gFnSc2	společnost
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
západu	západ	k1gInSc2	západ
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
národní	národní	k2eAgInSc1d1	národní
ragbyový	ragbyový	k2eAgInSc1d1	ragbyový
stadión	stadión	k1gInSc1	stadión
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Tvickenhamu	Tvickenham	k1gInSc6	Tvickenham
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
klubů	klub	k1gInPc2	klub
elitní	elitní	k2eAgFnSc2d1	elitní
ligy	liga	k1gFnSc2	liga
-	-	kIx~	-
Guinness	Guinness	k1gInSc1	Guinness
Premiership	Premiership	k1gMnSc1	Premiership
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Kluby	klub	k1gInPc4	klub
London	London	k1gMnSc1	London
Irish	Irish	k1gMnSc1	Irish
<g/>
,	,	kIx,	,
Saracens	Saracens	k1gInSc1	Saracens
a	a	k8xC	a
Wasps	Wasps	k1gInSc1	Wasps
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
mimo	mimo	k6eAd1	mimo
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Harlequins	Harlequins	k1gInSc1	Harlequins
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
soutěže	soutěž	k1gFnSc2	soutěž
National	National	k1gMnPc2	National
Division	Division	k1gInSc1	Division
One	One	k1gFnPc2	One
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc1	dva
kriketová	kriketový	k2eAgNnPc1d1	kriketové
hřiště	hřiště	k1gNnPc1	hřiště
-	-	kIx~	-
Lord	lord	k1gMnSc1	lord
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
-	-	kIx~	-
domov	domov	k1gInSc1	domov
Middlesex	Middlesex	k1gInSc1	Middlesex
a	a	k8xC	a
Marylebone	Marylebon	k1gInSc5	Marylebon
Cricket	Cricket	k1gInSc1	Cricket
Club	club	k1gInSc1	club
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
St	St	kA	St
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wood	Wood	k1gInSc1	Wood
kousek	kousek	k1gInSc4	kousek
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Regent	regent	k1gMnSc1	regent
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parku	park	k1gInSc6	park
a	a	k8xC	a
Oval	ovalit	k5eAaPmRp2nS	ovalit
-	-	kIx~	-
sídlo	sídlo	k1gNnSc4	sídlo
Surrey	Surrea	k1gFnSc2	Surrea
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
All	All	k?	All
England	England	k1gInSc1	England
Lawn	Lawn	k1gMnSc1	Lawn
Tennis	Tennis	k1gInSc1	Tennis
and	and	k?	and
Croquet	Croquet	k1gInSc1	Croquet
Club	club	k1gInSc1	club
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
světově	světově	k6eAd1	světově
proslulého	proslulý	k2eAgInSc2d1	proslulý
tenisového	tenisový	k2eAgInSc2d1	tenisový
turnaje	turnaj	k1gInSc2	turnaj
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvíce	nejvíce	k6eAd1	nejvíce
navštěvovaným	navštěvovaný	k2eAgNnPc3d1	navštěvované
městům	město	k1gNnPc3	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Turistické	turistický	k2eAgFnPc1d1	turistická
atrakce	atrakce	k1gFnPc1	atrakce
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
historická	historický	k2eAgFnSc1d1	historická
City	City	k1gFnSc1	City
<g/>
;	;	kIx,	;
West	West	k1gInSc1	West
End	End	k1gFnSc2	End
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
proslulá	proslulý	k2eAgNnPc1d1	proslulé
kina	kino	k1gNnPc1	kino
<g/>
,	,	kIx,	,
bary	bar	k1gInPc1	bar
<g/>
,	,	kIx,	,
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc1	obchod
a	a	k8xC	a
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
;	;	kIx,	;
Westminster	Westminster	k1gInSc1	Westminster
s	s	k7c7	s
Westminsterským	Westminsterský	k2eAgNnSc7d1	Westminsterské
opatstvím	opatství	k1gNnSc7	opatství
<g/>
,	,	kIx,	,
Buckinghamským	buckinghamský	k2eAgInSc7d1	buckinghamský
palácem	palác	k1gInSc7	palác
<g/>
,	,	kIx,	,
Clarence	Clarence	k1gFnSc1	Clarence
House	house	k1gNnSc1	house
atd.	atd.	kA	atd.
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Science	Science	k1gFnSc1	Science
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Přírodopisné	přírodopisný	k2eAgNnSc1d1	přírodopisné
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Britské	britský	k2eAgNnSc1d1	Britské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Tate	Tate	k1gFnSc1	Tate
Gallery	Galler	k1gInPc4	Galler
<g/>
,	,	kIx,	,
mosty	most	k1gInPc4	most
London	London	k1gMnSc1	London
Bridge	Bridge	k1gInSc4	Bridge
<g/>
,	,	kIx,	,
Tower	Tower	k1gInSc4	Tower
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
Královská	královský	k2eAgFnSc1d1	královská
observatoř	observatoř	k1gFnSc1	observatoř
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
prochází	procházet	k5eAaImIp3nS	procházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
nultý	nultý	k4xOgMnSc1	nultý
poledník	poledník	k1gMnSc1	poledník
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
zajímavosti	zajímavost	k1gFnPc4	zajímavost
<g/>
.	.	kIx.	.
</s>
<s>
Camden	Camdna	k1gFnPc2	Camdna
Town	Town	k1gMnSc1	Town
Covent	Covent	k1gMnSc1	Covent
Garden	Gardno	k1gNnPc2	Gardno
Čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
Downing	Downing	k1gInSc4	Downing
Street	Street	k1gInSc1	Street
Horse	Horse	k1gFnSc2	Horse
Guards	Guardsa	k1gFnPc2	Guardsa
Parade	Parad	k1gInSc5	Parad
Jižní	jižní	k2eAgNnSc1d1	jižní
nábřeží	nábřeží	k1gNnSc4	nábřeží
Leicester	Leicestra	k1gFnPc2	Leicestra
Square	square	k1gInSc1	square
London	London	k1gMnSc1	London
Dungeon	Dungeon	k1gInSc1	Dungeon
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
planetárium	planetárium	k1gNnSc1	planetárium
Muzeum	muzeum	k1gNnSc1	muzeum
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
Madame	madame	k1gFnSc2	madame
Tussaud	Tussaud	k1gMnSc1	Tussaud
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
Circus	Circus	k1gMnSc1	Circus
Tower	Tower	k1gMnSc1	Tower
Bridge	Bridge	k1gFnPc2	Bridge
Tower	Tower	k1gMnSc1	Tower
Trafalgarské	Trafalgarský	k2eAgNnSc4d1	Trafalgarské
náměstí	náměstí	k1gNnSc4	náměstí
1	[number]	k4	1
Canada	Canada	k1gFnSc1	Canada
Square	square	k1gInSc1	square
30	[number]	k4	30
St	St	kA	St
Mary	Mary	k1gFnPc2	Mary
Axe	Axe	k1gFnPc2	Axe
Albertův	Albertův	k2eAgInSc1d1	Albertův
památník	památník	k1gInSc1	památník
Bank	bank	k1gInSc1	bank
of	of	k?	of
England	England	k1gInSc1	England
Big	Big	k1gMnSc1	Big
Ben	Ben	k1gInSc1	Ben
<g />
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
knihovna	knihovna	k1gFnSc1	knihovna
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
House	house	k1gNnSc4	house
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
Budova	budova	k1gFnSc1	budova
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
Lloyds	Lloydsa	k1gFnPc2	Lloydsa
Clarence	Clarence	k1gFnSc2	Clarence
House	house	k1gNnSc1	house
Elektrárna	elektrárna	k1gFnSc1	elektrárna
Battersea	Battersea	k1gFnSc1	Battersea
Hampton	Hampton	k1gInSc4	Hampton
Court	Court	k1gInSc1	Court
Palace	Palace	k1gFnSc1	Palace
Katedrála	katedrál	k1gMnSc2	katedrál
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
Kensingtonský	Kensingtonský	k2eAgInSc1d1	Kensingtonský
palác	palác	k1gInSc1	palác
Kleopatřin	Kleopatřin	k2eAgInSc4d1	Kleopatřin
obelisk	obelisk	k1gInSc4	obelisk
Královská	královský	k2eAgFnSc1d1	královská
burza	burza	k1gFnSc1	burza
Královská	královský	k2eAgFnSc1d1	královská
greenwichská	greenwichský	k2eAgFnSc1d1	greenwichská
observatoř	observatoř	k1gFnSc1	observatoř
Královský	královský	k2eAgInSc1d1	královský
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
Lambethský	Lambethský	k2eAgInSc4d1	Lambethský
palác	palác	k1gInSc4	palác
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
radnice	radnice	k1gFnSc1	radnice
Millennium	millennium	k1gNnSc1	millennium
Dome	dům	k1gInSc5	dům
Mramorový	mramorový	k2eAgInSc1d1	mramorový
oblouk	oblouk	k1gInSc4	oblouk
Neasdenský	Neasdenský	k2eAgInSc4d1	Neasdenský
chrám	chrám	k1gInSc4	chrám
Nelsonův	Nelsonův	k2eAgInSc1d1	Nelsonův
sloup	sloup	k1gInSc1	sloup
Památník	památník	k1gInSc4	památník
Diany	Diana	k1gFnSc2	Diana
<g/>
,	,	kIx,	,
princezny	princezna	k1gFnSc2	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g />
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
Velkého	velký	k2eAgInSc2d1	velký
požáru	požár	k1gInSc2	požár
Londýna	Londýn	k1gInSc2	Londýn
Royal	Royal	k1gMnSc1	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
Royal	Royal	k1gMnSc1	Royal
Festival	festival	k1gInSc1	festival
Hall	Hall	k1gMnSc1	Hall
St	St	kA	St
James	James	k1gMnSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Palace	Palace	k1gFnSc1	Palace
Tower	Tower	k1gInSc4	Tower
Tower	Tower	k1gInSc1	Tower
42	[number]	k4	42
Věž	věž	k1gFnSc4	věž
British	Britisha	k1gFnPc2	Britisha
Telecom	Telecom	k1gInSc4	Telecom
Westminsterské	Westminsterský	k2eAgNnSc4d1	Westminsterské
opatství	opatství	k1gNnSc4	opatství
Westminsterský	Westminsterský	k2eAgInSc1d1	Westminsterský
palác	palác	k1gInSc1	palác
Viz	vidět	k5eAaImRp2nS	vidět
Londýnská	londýnský	k2eAgNnPc1d1	Londýnské
muzea	muzeum	k1gNnPc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
Borough	Borough	k1gInSc4	Borough
Market	market	k1gInSc1	market
Brick	Brick	k1gInSc1	Brick
Lane	Lane	k1gInSc1	Lane
Market	market	k1gInSc1	market
Camden	Camdna	k1gFnPc2	Camdna
Town	Town	k1gInSc1	Town
Covent	Covent	k1gMnSc1	Covent
Garden	Gardna	k1gFnPc2	Gardna
Knightsbridge	Knightsbridge	k1gFnPc2	Knightsbridge
Petticoat	Petticoat	k2eAgInSc4d1	Petticoat
Lane	Lane	k1gInSc4	Lane
Market	market	k1gInSc1	market
Portobello	Portobello	k1gNnSc1	Portobello
Road	Roada	k1gFnPc2	Roada
Market	market	k1gInSc1	market
West	West	k1gMnSc1	West
End	End	k1gMnSc1	End
</s>
<s>
11	[number]	k4	11
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
zabírají	zabírat	k5eAaImIp3nP	zabírat
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
královských	královský	k2eAgInPc2d1	královský
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
honebních	honební	k2eAgInPc2d1	honební
revírů	revír	k1gInPc2	revír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
přístupno	přístupen	k2eAgNnSc1d1	přístupno
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Green	Green	k2eAgInSc1d1	Green
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
St.	st.	kA	st.
James	James	k1gMnSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Hyde	Hyd	k1gInPc1	Hyd
Park	park	k1gInSc1	park
a	a	k8xC	a
Kensingtonské	Kensingtonský	k2eAgFnPc1d1	Kensingtonský
zahrady	zahrada	k1gFnPc1	zahrada
formují	formovat	k5eAaImIp3nP	formovat
pásmo	pásmo	k1gNnSc4	pásmo
zeleně	zeleň	k1gFnSc2	zeleň
ve	v	k7c4	v
West	West	k1gInSc4	West
Endu	Endus	k1gInSc2	Endus
<g/>
.	.	kIx.	.
</s>
<s>
Regent	regent	k1gMnSc1	regent
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Park	park	k1gInSc1	park
tvoří	tvořit	k5eAaImIp3nS	tvořit
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
centra	centrum	k1gNnSc2	centrum
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Greenwich	Greenwich	k1gInSc4	Greenwich
Park-národní	Parkárodní	k2eAgInSc1d1	Park-národní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Bushy	Bush	k1gInPc1	Bush
Park	park	k1gInSc1	park
a	a	k8xC	a
Richmond	Richmond	k1gInSc1	Richmond
Park	park	k1gInSc1	park
jsou	být	k5eAaImIp3nP	být
oázou	oáza	k1gFnSc7	oáza
klidu	klid	k1gInSc2	klid
v	v	k7c6	v
londýnských	londýnský	k2eAgNnPc6d1	Londýnské
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
menších	malý	k2eAgInPc2d2	menší
parků	park	k1gInPc2	park
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
u	u	k7c2	u
bohatých	bohatý	k2eAgFnPc2d1	bohatá
soukromých	soukromý	k2eAgFnPc2d1	soukromá
rezidencí	rezidence	k1gFnPc2	rezidence
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgNnSc4d1	soukromé
používání	používání	k1gNnSc4	používání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
parků	park	k1gInPc2	park
spravovaných	spravovaný	k2eAgInPc2d1	spravovaný
londýnskou	londýnský	k2eAgFnSc7d1	londýnská
samosprávou	samospráva	k1gFnSc7	samospráva
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
mezi	mezi	k7c7	mezi
polovinou	polovina	k1gFnSc7	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
Victoria	Victorium	k1gNnSc2	Victorium
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Alexandra	Alexandra	k1gFnSc1	Alexandra
Park	park	k1gInSc1	park
a	a	k8xC	a
Battersea	Battersea	k1gFnSc1	Battersea
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
parky	park	k1gInPc1	park
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
například	například	k6eAd1	například
Hampstead	Hampstead	k1gInSc4	Hampstead
Heath	Heatha	k1gFnPc2	Heatha
<g/>
,	,	kIx,	,
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
Common	Commona	k1gFnPc2	Commona
a	a	k8xC	a
Epping	Epping	k1gInSc4	Epping
Forest	Forest	k1gFnSc4	Forest
mají	mít	k5eAaImIp3nP	mít
neformální	formální	k2eNgInSc4d1	neformální
přírodní	přírodní	k2eAgInSc4d1	přírodní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
zahrada	zahrada	k1gFnSc1	zahrada
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
vstupné	vstupné	k1gNnSc1	vstupné
je	být	k5eAaImIp3nS	být
Královské	královský	k2eAgFnPc4d1	královská
botanické	botanický	k2eAgFnPc4d1	botanická
zahrady	zahrada	k1gFnPc4	zahrada
v	v	k7c6	v
Kew	Kew	k1gFnSc6	Kew
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Sofie	Sofia	k1gFnSc2	Sofia
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
Alžír	Alžír	k1gInSc1	Alžír
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
Bogotá	Bogotá	k1gFnPc2	Bogotá
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Johannesburg	Johannesburg	k1gInSc1	Johannesburg
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
Podgorica	Podgoric	k2eAgFnSc1d1	Podgorica
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
Bagdád	Bagdád	k1gInSc1	Bagdád
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
průvodce	průvodce	k1gMnSc1	průvodce
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
mapou	mapa	k1gFnSc7	mapa
<g/>
,	,	kIx,	,
COMPUTER	computer	k1gInSc1	computer
PRESS	PRESS	kA	PRESS
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-251-0768-X	[number]	k4	80-251-0768-X
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
společník	společník	k1gMnSc1	společník
cestovatele	cestovatel	k1gMnSc2	cestovatel
<g/>
,	,	kIx,	,
Leapman	Leapman	k1gMnSc1	Leapman
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Carstensen	Carstensen	k1gInSc1	Carstensen
Heidede	Heided	k1gMnSc5	Heided
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7236-512-6	[number]	k4	80-7236-512-6
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
Velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
National	National	k1gMnSc1	National
Geographic	Geographic	k1gMnSc1	Geographic
<g/>
,	,	kIx,	,
Nicholson	Nicholson	k1gMnSc1	Nicholson
<g/>
,	,	kIx,	,
Louise	Louis	k1gMnPc4	Louis
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
1274	[number]	k4	1274
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Computer	computer	k1gInSc1	computer
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
kulturněhistorický	kulturněhistorický	k2eAgMnSc1d1	kulturněhistorický
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
,	,	kIx,	,
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Velký	velký	k2eAgInSc1d1	velký
smog	smog	k1gInSc1	smog
1952	[number]	k4	1952
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Londýn	Londýn	k1gInSc1	Londýn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Londýn	Londýn	k1gInSc1	Londýn
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc1	kategorie
Londýn	Londýn	k1gInSc1	Londýn
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Téma	téma	k1gNnSc1	téma
Londýn	Londýn	k1gInSc1	Londýn
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Londýn	Londýn	k1gInSc1	Londýn
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Londýna	Londýn	k1gInSc2	Londýn
-	-	kIx~	-
neplatný	platný	k2eNgInSc1d1	neplatný
odkaz	odkaz	k1gInSc1	odkaz
!	!	kIx.	!
</s>
<s>
Správní	správní	k2eAgInSc1d1	správní
orgán	orgán	k1gInSc1	orgán
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
Památky	památka	k1gFnSc2	památka
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
k	k	k7c3	k
vytištění	vytištění	k1gNnSc3	vytištění
a	a	k8xC	a
popiskem	popisek	k1gInSc7	popisek
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
pohyby	pohyb	k1gInPc1	pohyb
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
-	-	kIx~	-
Největší	veliký	k2eAgInSc1d3	veliký
nezávislý	závislý	k2eNgInSc1d1	nezávislý
portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
</s>
