<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Hasištejn	Hasištejn	k1gInSc1	Hasištejn
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hassenstein	Hassenstein	k2eAgInSc1d1	Hassenstein
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Místo	místo	k1gNnSc4	místo
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
627	[number]	k4	627
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
hradů	hrad	k1gInPc2	hrad
severozápadních	severozápadní	k2eAgFnPc2d1	severozápadní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
též	též	k9	též
rodištěm	rodiště	k1gNnSc7	rodiště
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
významného	významný	k2eAgMnSc2d1	významný
aristokrata	aristokrat	k1gMnSc2	aristokrat
a	a	k8xC	a
humanisty	humanista	k1gMnSc2	humanista
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Hasištejnského	Hasištejnský	k1gMnSc4	Hasištejnský
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
Hasištejna	Hasištejn	k1gInSc2	Hasištejn
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hradišti	hradiště	k1gNnSc6	hradiště
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
stopy	stopa	k1gFnPc1	stopa
pohanského	pohanský	k2eAgNnSc2d1	pohanské
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
dávném	dávný	k2eAgNnSc6d1	dávné
osídlení	osídlení	k1gNnSc6	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
královském	královský	k2eAgInSc6d1	královský
hradu	hrad	k1gInSc6	hrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
zákoníku	zákoník	k1gInSc6	zákoník
Majestas	Majestasa	k1gFnPc2	Majestasa
Carolina	Carolin	k2eAgFnSc1d1	Carolina
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
zmiňováno	zmiňován	k2eAgNnSc4d1	zmiňováno
opevněné	opevněný	k2eAgNnSc4d1	opevněné
místo	místo	k1gNnSc4	místo
už	už	k6eAd1	už
ve	v	k7c6	v
století	století	k1gNnSc6	století
dvanáctém	dvanáctý	k4xOgNnSc6	dvanáctý
<g/>
.	.	kIx.	.
</s>
<s>
Poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
obchodní	obchodní	k2eAgFnSc3d1	obchodní
cestě	cesta	k1gFnSc3	cesta
mezi	mezi	k7c7	mezi
Kadaní	Kadaň	k1gFnSc7	Kadaň
a	a	k8xC	a
německým	německý	k2eAgNnSc7d1	německé
městem	město	k1gNnSc7	město
Zwickau	Zwickaus	k1gInSc2	Zwickaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
gotický	gotický	k2eAgInSc1d1	gotický
hrad	hrad	k1gInSc1	hrad
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
ze	z	k7c2	z
Šumburka	Šumburek	k1gMnSc2	Šumburek
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
jim	on	k3xPp3gMnPc3	on
jeho	jeho	k3xOp3gInSc6	jeho
držbu	držba	k1gFnSc4	držba
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
i	i	k9	i
s	s	k7c7	s
panstvími	panství	k1gNnPc7	panství
Přísečnice	Přísečnice	k1gFnSc2	Přísečnice
a	a	k8xC	a
Schlettau	Schlettaus	k1gInSc2	Schlettaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jím	on	k3xPp3gMnSc7	on
vydaném	vydaný	k2eAgNnSc6d1	vydané
zákoníku	zákoník	k1gInSc2	zákoník
Majestas	Majestas	k1gInSc1	Majestas
Carolina	Carolina	k1gFnSc1	Carolina
jej	on	k3xPp3gMnSc4	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
mezi	mezi	k7c7	mezi
královskými	královský	k2eAgInPc7d1	královský
hrady	hrad	k1gInPc7	hrad
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
panovníci	panovník	k1gMnPc1	panovník
nesmí	smět	k5eNaImIp3nP	smět
prodat	prodat	k5eAaPmF	prodat
a	a	k8xC	a
zastavit	zastavit	k5eAaPmF	zastavit
jej	on	k3xPp3gMnSc4	on
směli	smět	k5eAaImAgMnP	smět
nanejvýš	nanejvýš	k6eAd1	nanejvýš
na	na	k7c4	na
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Šumburkům	Šumburek	k1gMnPc3	Šumburek
hrad	hrad	k1gInSc1	hrad
patřil	patřit	k5eAaImAgInS	patřit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1412	[number]	k4	1412
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
přenechali	přenechat	k5eAaPmAgMnP	přenechat
Jindřichu	Jindřich	k1gMnSc3	Jindřich
mladšímu	mladý	k2eAgMnSc3d2	mladší
Reussovi	Reuss	k1gMnSc3	Reuss
z	z	k7c2	z
Plavna	Plavno	k1gNnSc2	Plavno
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
spolčoval	spolčovat	k5eAaImAgMnS	spolčovat
se	se	k3xPyFc4	se
s	s	k7c7	s
loupeživými	loupeživý	k2eAgMnPc7d1	loupeživý
rytíři	rytíř	k1gMnPc7	rytíř
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
přepadávali	přepadávat	k5eAaImAgMnP	přepadávat
královské	královský	k2eAgInPc4d1	královský
statky	statek	k1gInPc4	statek
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgNnSc1d1	královské
vojsko	vojsko	k1gNnSc1	vojsko
proto	proto	k8xC	proto
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Chudého	Chudý	k1gMnSc2	Chudý
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
hrad	hrad	k1gInSc4	hrad
po	po	k7c6	po
dvouměsíčním	dvouměsíční	k2eAgNnSc6d1	dvouměsíční
obléhání	obléhání	k1gNnSc6	obléhání
dobylo	dobýt	k5eAaPmAgNnS	dobýt
a	a	k8xC	a
král	král	k1gMnSc1	král
Hasištejn	Hasištejn	k1gMnSc1	Hasištejn
ihned	ihned	k6eAd1	ihned
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1421	[number]	k4	1421
ho	on	k3xPp3gInSc4	on
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k9	jako
dědičné	dědičný	k2eAgNnSc4d1	dědičné
korunní	korunní	k2eAgNnSc4d1	korunní
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
<g/>
,	,	kIx,	,
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
humanista	humanista	k1gMnSc1	humanista
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Hasištejnský	Hasištejnský	k1gMnSc1	Hasištejnský
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
trvale	trvale	k6eAd1	trvale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
hrad	hrad	k1gInSc4	hrad
výrazně	výrazně	k6eAd1	výrazně
rozšířit	rozšířit	k5eAaPmF	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
založil	založit	k5eAaPmAgMnS	založit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
knihoven	knihovna	k1gFnPc2	knihovna
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
do	do	k7c2	do
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
shořela	shořet	k5eAaPmAgFnS	shořet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc4	hrad
dále	daleko	k6eAd2	daleko
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
různí	různý	k2eAgMnPc1d1	různý
příslušníci	příslušník	k1gMnPc1	příslušník
Lobkoviců	Lobkovice	k1gMnPc2	Lobkovice
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
čas	čas	k1gInSc4	čas
i	i	k9	i
Šliků	Šlik	k1gInPc2	Šlik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
konfiskaci	konfiskace	k1gFnSc4	konfiskace
majetku	majetek	k1gInSc2	majetek
Jiřího	Jiří	k1gMnSc2	Jiří
Popela	popel	k1gInSc2	popel
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
sedm	sedm	k4xCc4	sedm
osmin	osmina	k1gFnPc2	osmina
hradu	hrad	k1gInSc2	hrad
císař	císař	k1gMnSc1	císař
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1606	[number]	k4	1606
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
koupil	koupit	k5eAaPmAgMnS	koupit
Linhart	Linhart	k1gMnSc1	Linhart
starší	starší	k1gMnSc1	starší
Štampach	Štampach	k1gMnSc1	Štampach
ze	z	k7c2	z
Štampachu	Štampach	k1gInSc2	Štampach
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
majitelé	majitel	k1gMnPc1	majitel
měnili	měnit	k5eAaImAgMnP	měnit
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
chátrat	chátrat	k5eAaImF	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
záznamech	záznam	k1gInPc6	záznam
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xS	jako
opuštěný	opuštěný	k2eAgInSc1d1	opuštěný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zříceninu	zřícenina	k1gFnSc4	zřícenina
hradu	hrad	k1gInSc2	hrad
nechal	nechat	k5eAaPmAgMnS	nechat
upravit	upravit	k5eAaPmF	upravit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Karsch	Karsch	k1gMnSc1	Karsch
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgInS	postavit
zde	zde	k6eAd1	zde
hostinec	hostinec	k1gInSc1	hostinec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
vyhořel	vyhořet	k5eAaPmAgMnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
budovy	budova	k1gFnPc1	budova
hostince	hostinec	k1gInSc2	hostinec
před	před	k7c7	před
požárem	požár	k1gInSc7	požár
jsou	být	k5eAaImIp3nP	být
zachyceny	zachycen	k2eAgInPc1d1	zachycen
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
dílu	díl	k1gInSc6	díl
seriálu	seriál	k1gInSc2	seriál
30	[number]	k4	30
případů	případ	k1gInPc2	případ
majora	major	k1gMnSc2	major
Zemana	Zeman	k1gMnSc2	Zeman
Hon	hon	k1gInSc1	hon
na	na	k7c4	na
lišku	liška	k1gFnSc4	liška
jako	jako	k8xC	jako
Čadkova	Čadkův	k2eAgFnSc1d1	Čadkův
chata	chata	k1gFnSc1	chata
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Liščí	liščí	k2eAgNnSc4d1	liščí
doupě	doupě	k1gNnSc4	doupě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
hostinec	hostinec	k1gInSc1	hostinec
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
připomínající	připomínající	k2eAgFnSc4d1	připomínající
návštěvu	návštěva	k1gFnSc4	návštěva
J.	J.	kA	J.
W.	W.	kA	W.
Goetha	Goetha	k1gMnSc1	Goetha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavební	stavební	k2eAgFnSc1d1	stavební
podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
oválná	oválný	k2eAgFnSc1d1	oválná
plocha	plocha	k1gFnSc1	plocha
nad	nad	k7c7	nad
prudkým	prudký	k2eAgInSc7d1	prudký
svahem	svah	k1gInSc7	svah
k	k	k7c3	k
údolí	údolí	k1gNnSc3	údolí
Prunéřovského	Prunéřovský	k2eAgInSc2d1	Prunéřovský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
stavební	stavební	k2eAgFnSc1d1	stavební
fáze	fáze	k1gFnSc1	fáze
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
hradům	hrad	k1gInPc3	hrad
bergfritového	bergfritový	k2eAgInSc2d1	bergfritový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Bergfrit	Bergfrit	k1gInSc1	Bergfrit
<g/>
,	,	kIx,	,
obehnaný	obehnaný	k2eAgInSc1d1	obehnaný
plášťovou	plášťový	k2eAgFnSc7d1	plášťová
hradbou	hradba	k1gFnSc7	hradba
<g/>
,	,	kIx,	,
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
místo	místo	k1gNnSc4	místo
jádra	jádro	k1gNnSc2	jádro
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
věže	věž	k1gFnSc2	věž
byla	být	k5eAaImAgFnS	být
vytesána	vytesán	k2eAgFnSc1d1	vytesána
cisterna	cisterna	k1gFnSc1	cisterna
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
protilehlém	protilehlý	k2eAgInSc6d1	protilehlý
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
chráněném	chráněný	k2eAgNnSc6d1	chráněné
místě	místo	k1gNnSc6	místo
stojí	stát	k5eAaImIp3nS	stát
věžový	věžový	k2eAgInSc1d1	věžový
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
naproti	naproti	k6eAd1	naproti
vstupní	vstupní	k2eAgFnSc3d1	vstupní
bráně	brána	k1gFnSc3	brána
přiléhala	přiléhat	k5eAaImAgFnS	přiléhat
kaple	kaple	k1gFnSc1	kaple
zaklenutá	zaklenutý	k2eAgFnSc1d1	zaklenutá
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
podporuje	podporovat	k5eAaImIp3nS	podporovat
královské	královský	k2eAgNnSc4d1	královské
založení	založení	k1gNnSc4	založení
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
obléhání	obléhání	k1gNnSc6	obléhání
roku	rok	k1gInSc2	rok
1418	[number]	k4	1418
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
výrazně	výrazně	k6eAd1	výrazně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
jádra	jádro	k1gNnSc2	jádro
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
rozlehlé	rozlehlý	k2eAgNnSc1d1	rozlehlé
nádvoří	nádvoří	k1gNnSc1	nádvoří
postavené	postavený	k2eAgNnSc1d1	postavené
nad	nad	k7c7	nad
klenbou	klenba	k1gFnSc7	klenba
velkého	velký	k2eAgInSc2d1	velký
sklepa	sklep	k1gInSc2	sklep
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
využil	využít	k5eAaPmAgInS	využít
prostor	prostor	k1gInSc4	prostor
původně	původně	k6eAd1	původně
velmi	velmi	k6eAd1	velmi
strmého	strmý	k2eAgInSc2d1	strmý
svahu	svah	k1gInSc2	svah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
se	se	k3xPyFc4	se
vstupovalo	vstupovat	k5eAaImAgNnS	vstupovat
přes	přes	k7c4	přes
šíjový	šíjový	k2eAgInSc4d1	šíjový
příkop	příkop	k1gInSc4	příkop
čtverhrannou	čtverhranný	k2eAgFnSc7d1	čtverhranná
<g/>
,	,	kIx,	,
dovnitř	dovnitř	k6eAd1	dovnitř
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
obehnán	obehnat	k5eAaPmNgInS	obehnat
parkánovou	parkánový	k2eAgFnSc7d1	Parkánová
hradbou	hradba	k1gFnSc7	hradba
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
nevýraznými	výrazný	k2eNgFnPc7d1	nevýrazná
baštami	bašta	k1gFnPc7	bašta
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgFnSc2d1	gotická
přestavby	přestavba	k1gFnSc2	přestavba
za	za	k7c2	za
Lobkoviců	Lobkoviec	k1gInPc2	Lobkoviec
bylo	být	k5eAaImAgNnS	být
opevnění	opevnění	k1gNnSc1	opevnění
hradu	hrad	k1gInSc2	hrad
znovu	znovu	k6eAd1	znovu
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
šíjovým	šíjový	k2eAgInSc7d1	šíjový
příkopem	příkop	k1gInSc7	příkop
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
hradba	hradba	k1gFnSc1	hradba
s	s	k7c7	s
věžovou	věžový	k2eAgFnSc7d1	věžová
branou	brána	k1gFnSc7	brána
a	a	k8xC	a
čtverhrannou	čtverhranný	k2eAgFnSc7d1	čtverhranná
bateriovou	bateriový	k2eAgFnSc7d1	bateriová
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
byly	být	k5eAaImAgFnP	být
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hradu	hrad	k1gInSc2	hrad
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Hradbu	hradba	k1gFnSc4	hradba
chránil	chránit	k5eAaImAgMnS	chránit
další	další	k2eAgInSc4d1	další
příkop	příkop	k1gInSc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
hrad	hrad	k1gInSc1	hrad
účinně	účinně	k6eAd1	účinně
ostřelován	ostřelovat	k5eAaImNgInS	ostřelovat
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
valy	val	k1gInPc4	val
opevněná	opevněný	k2eAgFnSc1d1	opevněná
předsunutá	předsunutý	k2eAgFnSc1d1	předsunutá
bašta	bašta	k1gFnSc1	bašta
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
opatřenou	opatřený	k2eAgFnSc4d1	opatřená
břitem	břit	k1gInSc7	břit
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
nových	nový	k2eAgFnPc2d1	nová
bašt	bašta	k1gFnPc2	bašta
zpevnila	zpevnit	k5eAaPmAgFnS	zpevnit
parkánovou	parkánový	k2eAgFnSc4d1	Parkánová
hradbu	hradba	k1gFnSc4	hradba
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
pod	pod	k7c7	pod
bergfritem	bergfrit	k1gInSc7	bergfrit
postaven	postaven	k2eAgInSc4d1	postaven
nový	nový	k2eAgInSc4d1	nový
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
především	především	k9	především
východní	východní	k2eAgFnSc1d1	východní
zeď	zeď	k1gFnSc1	zeď
s	s	k7c7	s
arkýři	arkýř	k1gInPc7	arkýř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnešní	dnešní	k2eAgInSc1d1	dnešní
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
Zachovala	zachovat	k5eAaPmAgFnS	zachovat
se	se	k3xPyFc4	se
velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zbytky	zbytek	k1gInPc4	zbytek
dvou	dva	k4xCgInPc2	dva
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
část	část	k1gFnSc4	část
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Vrch	vrch	k1gInSc1	vrch
je	být	k5eAaImIp3nS	být
zčásti	zčásti	k6eAd1	zčásti
zarostlý	zarostlý	k2eAgInSc1d1	zarostlý
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Místo	místo	k7c2	místo
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
km	km	kA	km
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
motorestu	motorest	k1gInSc2	motorest
Ušák	ušák	k1gInSc4	ušák
na	na	k7c4	na
silnici	silnice	k1gFnSc4	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
sem	sem	k6eAd1	sem
vede	vést	k5eAaImIp3nS	vést
modrá	modrý	k2eAgFnSc1d1	modrá
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Místo	místo	k6eAd1	místo
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
<g/>
Obec	obec	k1gFnSc1	obec
Místo	místo	k7c2	místo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hrad	hrad	k1gInSc1	hrad
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
pořádá	pořádat	k5eAaImIp3nS	pořádat
každoročně	každoročně	k6eAd1	každoročně
řadu	řada	k1gFnSc4	řada
společenských	společenský	k2eAgFnPc2d1	společenská
akcí	akce	k1gFnPc2	akce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Fesťáček	Fesťáček	k1gInSc1	Fesťáček
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
bývalou	bývalý	k2eAgFnSc4d1	bývalá
konírnu	konírna	k1gFnSc4	konírna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chce	chtít	k5eAaImIp3nS	chtít
vybudovat	vybudovat	k5eAaPmF	vybudovat
stálou	stálý	k2eAgFnSc4d1	stálá
výstavku	výstavka	k1gFnSc4	výstavka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnPc1d1	severní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Rudolf	Rudolf	k1gMnSc1	Rudolf
Anděl	Anděl	k1gMnSc1	Anděl
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
664	[number]	k4	664
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Hasištejn	Hasištejn	k1gInSc1	Hasištejn
–	–	k?	–
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
s.	s.	k?	s.
129	[number]	k4	129
<g/>
–	–	k?	–
<g/>
131	[number]	k4	131
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HEFNER	HEFNER	kA	HEFNER
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Bergfrit	Bergfrit	k1gInSc1	Bergfrit
hradu	hrad	k1gInSc2	hrad
Hasištejna	Hasištejno	k1gNnSc2	Hasištejno
<g/>
.	.	kIx.	.
</s>
<s>
Hláska	hláska	k1gFnSc1	hláska
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
XXII	XXII	kA	XXII
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
4974	[number]	k4	4974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PACHNER	PACHNER	kA	PACHNER
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Hasištejn	Hasištejn	k1gNnSc1	Hasištejn
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
konzervace	konzervace	k1gFnSc2	konzervace
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgInPc1d1	experimentální
vzorky	vzorek	k1gInPc1	vzorek
oprav	oprava	k1gFnPc2	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
111	[number]	k4	111
<g/>
–	–	k?	–
<g/>
117	[number]	k4	117
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
5076	[number]	k4	5076
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
Království	království	k1gNnSc2	království
českého	český	k2eAgInSc2d1	český
–	–	k?	–
Litoměřicko	Litoměřicko	k1gNnSc1	Litoměřicko
a	a	k8xC	a
Žatecko	Žatecko	k1gNnSc1	Žatecko
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Čížek	Čížek	k1gMnSc1	Čížek
–	–	k?	–
ViGo	ViGo	k1gMnSc1	ViGo
agency	agenca	k1gFnSc2	agenca
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
445	[number]	k4	445
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Hasištein	Hasištein	k1gMnSc1	Hasištein
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
s.	s.	k?	s.
128	[number]	k4	128
<g/>
–	–	k?	–
<g/>
138	[number]	k4	138
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZÁRUBA	Záruba	k1gMnSc1	Záruba
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Hradní	hradní	k2eAgFnSc1d1	hradní
kaple	kaple	k1gFnSc1	kaple
na	na	k7c6	na
Hasištejně	Hasištejně	k1gFnSc6	Hasištejně
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
KULJAVCEVA	KULJAVCEVA	kA	KULJAVCEVA
HLAVOVÁ	Hlavová	k1gFnSc1	Hlavová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
;	;	kIx,	;
KOTYZA	KOTYZA	kA	KOTYZA
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
;	;	kIx,	;
SÝKORA	Sýkora	k1gMnSc1	Sýkora
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
českého	český	k2eAgInSc2d1	český
severozápadu	severozápad	k1gInSc2	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
archeologické	archeologický	k2eAgFnSc2d1	archeologická
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
severozápadních	severozápadní	k2eAgFnPc2d1	severozápadní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86531	[number]	k4	86531
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
287	[number]	k4	287
<g/>
–	–	k?	–
<g/>
304	[number]	k4	304
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hradů	hrad	k1gInPc2	hrad
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hasištejn	Hasištejna	k1gFnPc2	Hasištejna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Hrady	hrad	k1gInPc1	hrad
a	a	k8xC	a
zříceniny	zřícenina	k1gFnPc1	zřícenina
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
5	[number]	k4	5
<g/>
.	.	kIx.	.
–	–	k?	–
Hrad	hrad	k1gInSc1	hrad
Hasištejn	Hasištejn	k1gInSc1	Hasištejn
–	–	k?	–
před	před	k7c7	před
hradbami	hradba	k1gFnPc7	hradba
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
–	–	k?	–
Litvínovsko	Litvínovsko	k1gNnSc1	Litvínovsko
<g/>
,	,	kIx,	,
2017-02-09	[number]	k4	2017-02-09
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
s	s	k7c7	s
komentářem	komentář	k1gInSc7	komentář
Milana	Milan	k1gMnSc2	Milan
Sýkory	Sýkora	k1gMnSc2	Sýkora
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
