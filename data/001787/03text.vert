<s>
Sloughi	Sloughi	k6eAd1	Sloughi
<g/>
,	,	kIx,	,
též	též	k9	též
arabský	arabský	k2eAgMnSc1d1	arabský
chrt	chrt	k1gMnSc1	chrt
nebo	nebo	k8xC	nebo
sluga	sluga	k1gFnSc1	sluga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chrt	chrt	k1gMnSc1	chrt
orientálního	orientální	k2eAgInSc2d1	orientální
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Sloughi	Sloughi	k6eAd1	Sloughi
vznikala	vznikat	k5eAaImAgFnS	vznikat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
,	,	kIx,	,
Tunisu	Tunis	k1gInSc2	Tunis
nebo	nebo	k8xC	nebo
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Orientu	Orient	k1gInSc2	Orient
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
dostala	dostat	k5eAaPmAgFnS	dostat
jako	jako	k9	jako
dar	dar	k1gInSc1	dar
núbijským	núbijský	k2eAgMnPc3d1	núbijský
faraonům	faraon	k1gMnPc3	faraon
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Asie	Asie	k1gFnSc2	Asie
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
však	však	k9	však
byla	být	k5eAaImAgNnP	být
sloughi	sloughi	k6eAd1	sloughi
jiná	jiný	k2eAgNnPc1d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
ji	on	k3xPp3gFnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
dospěla	dochvít	k5eAaPmAgFnS	dochvít
7	[number]	k4	7
-	-	kIx~	-
8	[number]	k4	8
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Sloughi	Sloughi	k6eAd1	Sloughi
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
společnicí	společnice	k1gFnSc7	společnice
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
kočovných	kočovný	k2eAgInPc2d1	kočovný
pouštních	pouštní	k2eAgInPc2d1	pouštní
kmenů	kmen	k1gInPc2	kmen
Berberů	Berber	k1gMnPc2	Berber
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
obývali	obývat	k5eAaImAgMnP	obývat
severní	severní	k2eAgFnPc4d1	severní
části	část	k1gFnPc4	část
Afriky	Afrika	k1gFnSc2	Afrika
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
chrtí	chrtí	k2eAgFnPc1d1	chrtí
rasy	rasa	k1gFnPc1	rasa
původně	původně	k6eAd1	původně
vyšly	vyjít	k5eAaPmAgFnP	vyjít
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
hladkosrstého	hladkosrstý	k2eAgMnSc2d1	hladkosrstý
předka	předek	k1gMnSc2	předek
s	s	k7c7	s
dopředu	dopředu	k6eAd1	dopředu
spuštěným	spuštěný	k2eAgNnSc7d1	spuštěné
uchem	ucho	k1gNnSc7	ucho
<g/>
,	,	kIx,	,
k	k	k7c3	k
typovým	typový	k2eAgFnPc3d1	typová
odlišnostem	odlišnost	k1gFnPc3	odlišnost
došlo	dojít	k5eAaPmAgNnS	dojít
tedy	tedy	k9	tedy
asi	asi	k9	asi
šlechtěním	šlechtění	k1gNnSc7	šlechtění
různými	různý	k2eAgNnPc7d1	různé
kmeny	kmen	k1gInPc1	kmen
–	–	k?	–
např.	např.	kA	např.
azavak	azavak	k6eAd1	azavak
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
psa	pes	k1gMnSc4	pes
kmene	kmen	k1gInSc2	kmen
Tuarégů	Tuarég	k1gInPc2	Tuarég
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgInSc3	ten
saluki	saluk	k1gInSc3	saluk
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
psa	pes	k1gMnSc4	pes
Beduínů	Beduín	k1gMnPc2	Beduín
<g/>
.	.	kIx.	.
</s>
<s>
Sloughi	Sloughi	k6eAd1	Sloughi
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
vysoce	vysoce	k6eAd1	vysoce
ceněna	cenit	k5eAaImNgFnS	cenit
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
práci	práce	k1gFnSc4	práce
při	při	k7c6	při
lovech	lov	k1gInPc6	lov
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
svého	svůj	k3xOyFgMnSc4	svůj
majitele	majitel	k1gMnSc4	majitel
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
sokola	sokol	k1gMnSc2	sokol
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
za	za	k7c7	za
zajíci	zajíc	k1gMnPc7	zajíc
<g/>
,	,	kIx,	,
fenky	fenek	k1gMnPc7	fenek
<g/>
,	,	kIx,	,
hyenami	hyena	k1gFnPc7	hyena
<g/>
,	,	kIx,	,
pštrosy	pštros	k1gMnPc7	pštros
a	a	k8xC	a
gazelami	gazela	k1gFnPc7	gazela
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
chrti	chrt	k1gMnPc1	chrt
jsou	být	k5eAaImIp3nP	být
známí	známý	k1gMnPc1	známý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
loví	lovit	k5eAaImIp3nS	lovit
očima	oko	k1gNnPc7	oko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
nepoužívají	používat	k5eNaImIp3nP	používat
čich	čich	k1gInSc4	čich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zrak	zrak	k1gInSc1	zrak
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc7	jejich
největší	veliký	k2eAgFnSc7d3	veliký
výhodou	výhoda	k1gFnSc7	výhoda
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgNnPc3d1	ostatní
psím	psí	k2eAgNnPc3d1	psí
plemenům	plemeno	k1gNnPc3	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
slougi	sloug	k1gFnSc2	sloug
není	být	k5eNaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
spojitost	spojitost	k1gFnSc1	spojitost
<g/>
,	,	kIx,	,
s	s	k7c7	s
jemenským	jemenský	k2eAgNnSc7d1	jemenské
městem	město	k1gNnSc7	město
Saloug	Salouga	k1gFnPc2	Salouga
.	.	kIx.	.
</s>
<s>
Sloughi	Sloughi	k6eAd1	Sloughi
je	být	k5eAaImIp3nS	být
elegantní	elegantní	k2eAgMnSc1d1	elegantní
<g/>
,	,	kIx,	,
hrdý	hrdý	k2eAgMnSc1d1	hrdý
pes	pes	k1gMnSc1	pes
se	s	k7c7	s
suchým	suchý	k2eAgNnSc7d1	suché
osvalením	osvalení	k1gNnSc7	osvalení
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
zanechávající	zanechávající	k2eAgInSc1d1	zanechávající
dojem	dojem	k1gInSc1	dojem
velmi	velmi	k6eAd1	velmi
ušlechtilého	ušlechtilý	k2eAgNnSc2d1	ušlechtilé
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
protáhlá	protáhlý	k2eAgFnSc1d1	protáhlá
a	a	k8xC	a
elegantní	elegantní	k2eAgFnSc1d1	elegantní
<g/>
.	.	kIx.	.
</s>
<s>
Stop	stop	k1gInSc1	stop
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
černou	černý	k2eAgFnSc4d1	černá
nosní	nosní	k2eAgFnSc4d1	nosní
houbu	houba	k1gFnSc4	houba
<g/>
,	,	kIx,	,
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
jiné	jiný	k2eAgNnSc1d1	jiné
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
nepřípustné	přípustný	k2eNgNnSc1d1	nepřípustné
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
mají	mít	k5eAaImIp3nP	mít
nůžkovitý	nůžkovitý	k2eAgInSc4d1	nůžkovitý
skus	skus	k1gInSc4	skus
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc4	ucho
vysoko	vysoko	k6eAd1	vysoko
nasazené	nasazený	k2eAgFnPc1d1	nasazená
a	a	k8xC	a
přiléhající	přiléhající	k2eAgFnPc1d1	přiléhající
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
bez	bez	k7c2	bez
laloku	lalok	k1gInSc2	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
klenutý	klenutý	k2eAgInSc1d1	klenutý
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
praporců	praporec	k1gInPc2	praporec
a	a	k8xC	a
do	do	k7c2	do
špičky	špička	k1gFnSc2	špička
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
osvalené	osvalený	k2eAgFnPc1d1	osvalená
<g/>
.	.	kIx.	.
</s>
<s>
Tlapky	tlapka	k1gFnPc4	tlapka
kulaté	kulatý	k2eAgFnPc4d1	kulatá
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
kočičí	kočičí	k2eAgNnSc1d1	kočičí
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgInPc1d1	tmavý
drápky	drápek	k1gInPc1	drápek
<g/>
.	.	kIx.	.
</s>
<s>
Sloughi	Sloughi	k6eAd1	Sloughi
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgInPc1d1	aktivní
<g/>
,	,	kIx,	,
mrštné	mrštný	k2eAgInPc1d1	mrštný
a	a	k8xC	a
hbité	hbitý	k2eAgInPc1d1	hbitý
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
sebevědomé	sebevědomý	k2eAgNnSc1d1	sebevědomé
a	a	k8xC	a
hrdé	hrdý	k2eAgNnSc1d1	hrdé
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
lovecký	lovecký	k2eAgInSc4d1	lovecký
pud	pud	k1gInSc4	pud
a	a	k8xC	a
při	při	k7c6	při
procházkách	procházka	k1gFnPc6	procházka
na	na	k7c6	na
neoploceném	oplocený	k2eNgNnSc6d1	neoplocené
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
mít	mít	k5eAaImF	mít
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
na	na	k7c6	na
vodítku	vodítko	k1gNnSc6	vodítko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
vidí	vidět	k5eAaImIp3nP	vidět
něco	něco	k3yInSc4	něco
zajímavého	zajímavý	k2eAgMnSc4d1	zajímavý
<g/>
,	,	kIx,	,
běží	běžet	k5eAaImIp3nS	běžet
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
podívat	podívat	k5eAaPmF	podívat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
55	[number]	k4	55
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
uznávají	uznávat	k5eAaImIp3nP	uznávat
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
poslouchají	poslouchat	k5eAaImIp3nP	poslouchat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nebudou	být	k5eNaImBp3nP	být
otrocky	otrocky	k6eAd1	otrocky
poslušné	poslušný	k2eAgInPc1d1	poslušný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
labradorský	labradorský	k2eAgMnSc1d1	labradorský
retrívr	retrívr	k1gMnSc1	retrívr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dětem	dítě	k1gFnPc3	dítě
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
k	k	k7c3	k
jiným	jiný	k2eAgNnPc3d1	jiné
domácím	domácí	k2eAgNnPc3d1	domácí
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
sklony	sklon	k1gInPc4	sklon
honit	honit	k5eAaImF	honit
a	a	k8xC	a
následně	následně	k6eAd1	následně
dávit	dávit	k5eAaImF	dávit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
chovat	chovat	k5eAaImF	chovat
je	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dobří	dobrý	k2eAgMnPc1d1	dobrý
hlídači	hlídač	k1gMnPc1	hlídač
a	a	k8xC	a
štěkají	štěkat	k5eAaImIp3nP	štěkat
jen	jen	k9	jen
výjmečně	výjmečně	k6eAd1	výjmečně
<g/>
.	.	kIx.	.
</s>
