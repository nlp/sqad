<s>
Prof.	prof.	kA	prof.
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1911	[number]	k4	1911
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
od	od	k7c2	od
třicátých	třicátý	k4xOgNnPc2	třicátý
do	do	k7c2	do
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
natočil	natočit	k5eAaBmAgMnS	natočit
mnoho	mnoho	k4c4	mnoho
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
husitská	husitský	k2eAgFnSc1d1	husitská
trilogie	trilogie	k1gFnSc1	trilogie
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Romance	romance	k1gFnSc1	romance
pro	pro	k7c4	pro
křídlovku	křídlovka	k1gFnSc4	křídlovka
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozporuplné	rozporuplný	k2eAgNnSc1d1	rozporuplné
<g/>
:	:	kIx,	:
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
nejkontroverznějšího	kontroverzní	k2eAgMnSc4d3	nejkontroverznější
představitele	představitel	k1gMnSc4	představitel
českého	český	k2eAgInSc2d1	český
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
každému	každý	k3xTgInSc3	každý
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
nejpřínosnějších	přínosný	k2eAgFnPc2d3	nejpřínosnější
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mj.	mj.	kA	mj.
vychoval	vychovat	k5eAaPmAgMnS	vychovat
řadu	řada	k1gFnSc4	řada
tvůrců	tvůrce	k1gMnPc2	tvůrce
tzv.	tzv.	kA	tzv.
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stal	stát	k5eAaPmAgInS	stát
inspirací	inspirace	k1gFnSc7	inspirace
k	k	k7c3	k
autobiografii	autobiografie	k1gFnSc3	autobiografie
Podivný	podivný	k2eAgInSc4d1	podivný
život	život	k1gInSc4	život
režiséra	režisér	k1gMnSc2	režisér
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
chybami	chyba	k1gFnPc7	chyba
<g/>
,	,	kIx,	,
nehrdinský	hrdinský	k2eNgInSc1d1	nehrdinský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
dost	dost	k6eAd1	dost
odvážný	odvážný	k2eAgInSc1d1	odvážný
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
přece	přece	k9	přece
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
vždycky	vždycky	k6eAd1	vždycky
zvedá	zvedat	k5eAaImIp3nS	zvedat
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
neztrácí	ztrácet	k5eNaImIp3nS	ztrácet
naději	naděje	k1gFnSc4	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Umělec	umělec	k1gMnSc1	umělec
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
nezlomnou	zlomný	k2eNgFnSc4d1	nezlomná
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Českým	český	k2eAgInSc7d1	český
lvem	lev	k1gInSc7	lev
za	za	k7c4	za
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
přínos	přínos	k1gInSc4	přínos
české	český	k2eAgFnSc2d1	Česká
kinematografii	kinematografie	k1gFnSc3	kinematografie
i	i	k8xC	i
Cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
umělecký	umělecký	k2eAgInSc4d1	umělecký
přínos	přínos	k1gInSc4	přínos
světovému	světový	k2eAgInSc3d1	světový
filmu	film	k1gInSc3	film
na	na	k7c6	na
karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Medailí	medaile	k1gFnPc2	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
.	.	kIx.	.
</s>
<s>
Oceněn	oceněn	k2eAgMnSc1d1	oceněn
byl	být	k5eAaImAgMnS	být
prakticky	prakticky	k6eAd1	prakticky
každým	každý	k3xTgInSc7	každý
politickým	politický	k2eAgInSc7d1	politický
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
prošel	projít	k5eAaPmAgMnS	projít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
40	[number]	k4	40
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
režisérka	režisérka	k1gFnSc1	režisérka
Jitka	Jitka	k1gFnSc1	Jitka
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
měl	mít	k5eAaImAgInS	mít
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Raimund	Raimunda	k1gFnPc2	Raimunda
Vávra	Vávra	k1gMnSc1	Vávra
byl	být	k5eAaImAgMnS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
scénáře	scénář	k1gInSc2	scénář
filmu	film	k1gInSc2	film
Otakara	Otakar	k1gMnSc2	Otakar
Vávry	Vávra	k1gMnSc2	Vávra
Krakatit	Krakatit	k1gInSc4	Krakatit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2011	[number]	k4	2011
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc1	hodina
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
na	na	k7c4	na
pooperační	pooperační	k2eAgFnPc4d1	pooperační
komplikace	komplikace	k1gFnPc4	komplikace
zlomeniny	zlomenina	k1gFnSc2	zlomenina
krčku	krček	k1gInSc2	krček
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Praze	Praha	k1gFnSc6	Praha
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
dokumentech	dokument	k1gInPc6	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
produkoval	produkovat	k5eAaImAgInS	produkovat
experimentální	experimentální	k2eAgInSc1d1	experimentální
film	film	k1gInSc1	film
Světlo	světlo	k1gNnSc1	světlo
proniká	pronikat	k5eAaImIp3nS	pronikat
tmou	tma	k1gFnSc7	tma
(	(	kIx(	(
<g/>
i	i	k9	i
následující	následující	k2eAgInPc4d1	následující
krátké	krátký	k2eAgInPc4d1	krátký
filmy	film	k1gInPc4	film
Žijeme	žít	k5eAaImIp1nP	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Listopad	listopad	k1gInSc1	listopad
byly	být	k5eAaImAgInP	být
experimentálními	experimentální	k2eAgInPc7d1	experimentální
snímky	snímek	k1gInPc7	snímek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Filosofská	Filosofská	k?	Filosofská
historie	historie	k1gFnSc2	historie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgInS	být
Cech	cech	k1gInSc1	cech
panen	panna	k1gFnPc2	panna
kutnohorských	kutnohorský	k2eAgFnPc2d1	Kutnohorská
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
Zorka	Zorka	k1gFnSc1	Zorka
Janů	Janů	k1gFnSc1	Janů
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
známé	známý	k2eAgFnSc2d1	známá
Lídy	Lída	k1gFnSc2	Lída
Baarové	Baarová	k1gFnSc2	Baarová
<g/>
.	.	kIx.	.
</s>
<s>
Zorka	Zorka	k1gFnSc1	Zorka
Janů	Jan	k1gMnPc2	Jan
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
ve	v	k7c6	v
Vávrových	Vávrových	k2eAgInPc6d1	Vávrových
filmech	film	k1gInPc6	film
ze	z	k7c2	z
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Podvod	podvod	k1gInSc1	podvod
s	s	k7c7	s
Rubensem	Rubenso	k1gNnSc7	Rubenso
a	a	k8xC	a
Pacientka	pacientka	k1gFnSc1	pacientka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hegela	Hegel	k1gMnSc2	Hegel
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Lída	Lída	k1gFnSc1	Lída
Baarová	Baarová	k1gFnSc1	Baarová
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
Vávrových	Vávrových	k2eAgInPc6d1	Vávrových
filmech	film	k1gInPc6	film
Panenství	panenství	k1gNnSc2	panenství
<g/>
,	,	kIx,	,
Maskovaná	maskovaný	k2eAgFnSc1d1	maskovaná
milenka	milenka	k1gFnSc1	milenka
<g/>
,	,	kIx,	,
Dívka	dívka	k1gFnSc1	dívka
v	v	k7c6	v
modrém	modré	k1gNnSc6	modré
a	a	k8xC	a
Turbina	turbina	k1gFnSc1	turbina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
natočil	natočit	k5eAaBmAgInS	natočit
Krakatit	Krakatit	k1gInSc1	Krakatit
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
se	s	k7c7	s
silným	silný	k2eAgNnSc7d1	silné
protiválečným	protiválečný	k2eAgNnSc7d1	protiválečné
poselstvím	poselství	k1gNnSc7	poselství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
komunistickém	komunistický	k2eAgInSc6d1	komunistický
puči	puč	k1gInSc6	puč
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
nové	nový	k2eAgFnSc3d1	nová
situaci	situace	k1gFnSc3	situace
a	a	k8xC	a
točil	točit	k5eAaImAgMnS	točit
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
filmy	film	k1gInPc4	film
oslavující	oslavující	k2eAgInSc4d1	oslavující
nový	nový	k2eAgInSc4d1	nový
režim	režim	k1gInSc4	režim
nebo	nebo	k8xC	nebo
epické	epický	k2eAgFnPc4d1	epická
historické	historický	k2eAgFnPc4d1	historická
fresky	freska	k1gFnPc4	freska
podporující	podporující	k2eAgFnSc4d1	podporující
oficiální	oficiální	k2eAgFnSc4d1	oficiální
interpretaci	interpretace	k1gFnSc4	interpretace
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
desetiletí	desetiletí	k1gNnSc6	desetiletí
se	se	k3xPyFc4	se
režim	režim	k1gInSc1	režim
stal	stát	k5eAaPmAgInS	stát
liberálnějším	liberální	k2eAgMnSc7d2	liberálnější
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
přineslo	přinést	k5eAaPmAgNnS	přinést
jeho	jeho	k3xOp3gInPc4	jeho
nejhodnotnější	hodnotný	k2eAgInPc4d3	nejhodnotnější
filmy	film	k1gInPc4	film
<g/>
:	:	kIx,	:
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
reneta	reneta	k1gFnSc1	reneta
<g/>
,	,	kIx,	,
Romance	romance	k1gFnSc1	romance
pro	pro	k7c4	pro
křídlovku	křídlovka	k1gFnSc4	křídlovka
a	a	k8xC	a
především	především	k9	především
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
celý	celý	k2eAgInSc4d1	celý
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prožíval	prožívat	k5eAaImAgMnS	prožívat
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
tvůrčí	tvůrčí	k2eAgNnSc4d1	tvůrčí
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
socialismu	socialismus	k1gInSc2	socialismus
stát	stát	k5eAaPmF	stát
víceméně	víceméně	k9	víceméně
přestal	přestat	k5eAaPmAgInS	přestat
dotovat	dotovat	k5eAaBmF	dotovat
kinematografii	kinematografie	k1gFnSc4	kinematografie
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
dlouhometrážním	dlouhometrážní	k2eAgInSc7d1	dlouhometrážní
filmem	film	k1gInSc7	film
tak	tak	k6eAd1	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Evropa	Evropa	k1gFnSc1	Evropa
tančila	tančit	k5eAaImAgFnS	tančit
valčík	valčík	k1gInSc4	valčík
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
natočil	natočit	k5eAaBmAgMnS	natočit
celkem	celkem	k6eAd1	celkem
52	[number]	k4	52
hraných	hraný	k2eAgInPc2d1	hraný
filmů	film	k1gInPc2	film
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
k	k	k7c3	k
80	[number]	k4	80
filmům	film	k1gInPc3	film
napsal	napsat	k5eAaBmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
první	první	k4xOgMnPc1	první
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
dílo	dílo	k1gNnSc4	dílo
dělí	dělit	k5eAaImIp3nS	dělit
neuvěřitelné	uvěřitelný	k2eNgNnSc4d1	neuvěřitelné
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
magnum	magnum	k1gInSc1	magnum
opus	opus	k1gInSc4	opus
je	být	k5eAaImIp3nS	být
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
stejnojmenné	stejnojmenný	k2eAgInPc4d1	stejnojmenný
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
knize	kniha	k1gFnSc3	kniha
Václava	Václav	k1gMnSc2	Václav
Kaplického	Kaplický	k2eAgMnSc2d1	Kaplický
o	o	k7c6	o
čarodějnických	čarodějnický	k2eAgInPc6d1	čarodějnický
procesech	proces	k1gInPc6	proces
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kaplický	Kaplický	k2eAgMnSc1d1	Kaplický
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
knihou	kniha	k1gFnSc7	kniha
Malleus	Malleus	k1gMnSc1	Malleus
maleficarum	maleficarum	k1gInSc1	maleficarum
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
osudy	osud	k1gInPc4	osud
skutečných	skutečný	k2eAgMnPc2d1	skutečný
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
v	v	k7c6	v
čarodějnických	čarodějnický	k2eAgInPc6d1	čarodějnický
procesech	proces	k1gInPc6	proces
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
;	;	kIx,	;
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1678	[number]	k4	1678
<g/>
–	–	k?	–
<g/>
1695	[number]	k4	1695
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
humanismu	humanismus	k1gInSc2	humanismus
znovu	znovu	k6eAd1	znovu
propukla	propuknout	k5eAaPmAgFnS	propuknout
mezilidská	mezilidský	k2eAgFnSc1d1	mezilidská
nenávist	nenávist	k1gFnSc1	nenávist
zapříčiněná	zapříčiněný	k2eAgFnSc1d1	zapříčiněná
mocichtivými	mocichtivý	k2eAgMnPc7d1	mocichtivý
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
Jindřich	Jindřich	k1gMnSc1	Jindřich
František	František	k1gMnSc1	František
Boblig	Boblig	k1gMnSc1	Boblig
z	z	k7c2	z
Edelstadtu	Edelstadt	k1gInSc2	Edelstadt
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
sérii	série	k1gFnSc4	série
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
život	život	k1gInSc4	život
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
stovku	stovka	k1gFnSc4	stovka
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Popisem	popis	k1gInSc7	popis
brutality	brutalita	k1gFnSc2	brutalita
moci	moc	k1gFnSc2	moc
film	film	k1gInSc1	film
sváděl	svádět	k5eAaImAgInS	svádět
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
politickými	politický	k2eAgInPc7d1	politický
procesy	proces	k1gInPc7	proces
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
osudem	osud	k1gInSc7	osud
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
nebo	nebo	k8xC	nebo
s	s	k7c7	s
procesem	proces	k1gInSc7	proces
se	s	k7c7	s
Slánským	Slánský	k1gMnSc7	Slánský
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Vávra	Vávra	k1gMnSc1	Vávra
tento	tento	k3xDgInSc4	tento
postřeh	postřeh	k1gInSc4	postřeh
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
jako	jako	k9	jako
správný	správný	k2eAgMnSc1d1	správný
<g/>
.	.	kIx.	.
</s>
<s>
Černobílá	černobílý	k2eAgFnSc1d1	černobílá
alegorie	alegorie	k1gFnSc1	alegorie
plná	plný	k2eAgFnSc1d1	plná
symbolů	symbol	k1gInPc2	symbol
sleduje	sledovat	k5eAaImIp3nS	sledovat
příběh	příběh	k1gInSc4	příběh
až	až	k9	až
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
odsouzení	odsouzení	k1gNnSc2	odsouzení
kněze	kněz	k1gMnSc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Neschopnost	neschopnost	k1gFnSc1	neschopnost
zastavit	zastavit	k5eAaPmF	zastavit
zlo	zlo	k1gNnSc4	zlo
hned	hned	k6eAd1	hned
zpočátku	zpočátku	k6eAd1	zpočátku
jen	jen	k9	jen
inkvizitora	inkvizitor	k1gMnSc4	inkvizitor
podporuje	podporovat	k5eAaImIp3nS	podporovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
činech	čin	k1gInPc6	čin
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
končí	končit	k5eAaImIp3nS	končit
jeho	jeho	k3xOp3gFnSc4	jeho
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
už	už	k9	už
nejsem	být	k5eNaImIp1nS	být
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
<g/>
...	...	k?	...
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
promítán	promítat	k5eAaImNgMnS	promítat
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
kinech	kino	k1gNnPc6	kino
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
stažen	stáhnout	k5eAaPmNgInS	stáhnout
z	z	k7c2	z
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
promítán	promítat	k5eAaImNgInS	promítat
pouze	pouze	k6eAd1	pouze
příležitostně	příležitostně	k6eAd1	příležitostně
ve	v	k7c6	v
filmových	filmový	k2eAgInPc6d1	filmový
klubech	klub	k1gInPc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
tvůrčích	tvůrčí	k2eAgInPc2d1	tvůrčí
vrcholů	vrchol	k1gInPc2	vrchol
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Romance	romance	k1gFnSc1	romance
pro	pro	k7c4	pro
křídlovku	křídlovka	k1gFnSc4	křídlovka
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Černobílý	černobílý	k2eAgInSc1d1	černobílý
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
básni	báseň	k1gFnSc6	báseň
Františka	František	k1gMnSc2	František
Hrubína	Hrubín	k1gMnSc2	Hrubín
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
mezi	mezi	k7c7	mezi
studentem	student	k1gMnSc7	student
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
užívá	užívat	k5eAaImIp3nS	užívat
letní	letní	k2eAgFnPc4d1	letní
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
,	,	kIx,	,
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
principála	principál	k1gMnSc2	principál
<g/>
.	.	kIx.	.
</s>
<s>
Milenci	milenec	k1gMnPc1	milenec
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
utéct	utéct	k5eAaPmF	utéct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otec	otec	k1gMnSc1	otec
vybere	vybrat	k5eAaPmIp3nS	vybrat
pro	pro	k7c4	pro
dívku	dívka	k1gFnSc4	dívka
jiného	jiný	k2eAgMnSc2d1	jiný
nápadníka	nápadník	k1gMnSc2	nápadník
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
tento	tento	k3xDgInSc4	tento
svůj	svůj	k3xOyFgInSc4	svůj
film	film	k1gInSc4	film
označoval	označovat	k5eAaImAgMnS	označovat
za	za	k7c7	za
vůbec	vůbec	k9	vůbec
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Krakatit	Krakatit	k1gInSc1	Krakatit
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezce	vynálezce	k1gMnSc1	vynálezce
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
výbušniny	výbušnina	k1gFnSc2	výbušnina
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
uchovat	uchovat	k5eAaPmF	uchovat
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
zneužit	zneužít	k5eAaPmNgInS	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
Černobílá	černobílý	k2eAgFnSc1d1	černobílá
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
následována	následovat	k5eAaImNgFnS	následovat
barevnou	barevný	k2eAgFnSc7d1	barevná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Temné	temný	k2eAgNnSc1d1	temné
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Remake	Remake	k1gInSc1	Remake
snímku	snímek	k1gInSc2	snímek
posunul	posunout	k5eAaPmAgInS	posunout
příběh	příběh	k1gInSc1	příběh
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
nevyhnul	vyhnout	k5eNaPmAgMnS	vyhnout
se	se	k3xPyFc4	se
propagandě	propaganda	k1gFnSc3	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
nejslabší	slabý	k2eAgFnSc4d3	nejslabší
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
společně	společně	k6eAd1	společně
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
českými	český	k2eAgMnPc7d1	český
režiséry	režisér	k1gMnPc7	režisér
pomohl	pomoct	k5eAaPmAgMnS	pomoct
založit	založit	k5eAaPmF	založit
FAMU	FAMU	kA	FAMU
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
po	po	k7c4	po
pět	pět	k4xCc4	pět
dekád	dekáda	k1gFnPc2	dekáda
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
studenty	student	k1gMnPc7	student
byli	být	k5eAaImAgMnP	být
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
představitelé	představitel	k1gMnPc1	představitel
české	český	k2eAgFnSc2d1	Česká
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
FAMU	FAMU	kA	FAMU
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
žáky	žák	k1gMnPc7	žák
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Chytilová	Chytilová	k1gFnSc1	Chytilová
<g/>
,	,	kIx,	,
Evald	Evald	k1gInSc1	Evald
Schorm	Schorm	k1gInSc1	Schorm
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
,	,	kIx,	,
Emir	Emir	k1gMnSc1	Emir
Kusturica	Kusturica	k1gMnSc1	Kusturica
nebo	nebo	k8xC	nebo
Lordan	Lordan	k1gMnSc1	Lordan
Zafranović	Zafranović	k1gMnSc1	Zafranović
<g/>
.	.	kIx.	.
</s>
