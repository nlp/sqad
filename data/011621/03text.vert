<p>
<s>
Látkové	látkový	k2eAgNnSc1d1	látkové
množství	množství	k1gNnSc1	množství
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
vyjadřující	vyjadřující	k2eAgFnSc1d1	vyjadřující
počet	počet	k1gInSc4	počet
entit	entita	k1gFnPc2	entita
(	(	kIx(	(
<g/>
elementárních	elementární	k2eAgMnPc2d1	elementární
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
částice	částice	k1gFnPc1	částice
nějaké	nějaký	k3yIgFnSc2	nějaký
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
atomy	atom	k1gInPc1	atom
<g/>
,	,	kIx,	,
ionty	ion	k1gInPc1	ion
<g/>
,	,	kIx,	,
molekuly	molekula	k1gFnPc1	molekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotony	foton	k1gInPc1	foton
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
jimi	on	k3xPp3gNnPc7	on
být	být	k5eAaImF	být
např.	např.	kA	např.
i	i	k8xC	i
chemické	chemický	k2eAgFnPc1d1	chemická
vazby	vazba	k1gFnPc1	vazba
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
proběhlé	proběhlý	k2eAgFnPc1d1	proběhlá
chemické	chemický	k2eAgFnPc1d1	chemická
reakce	reakce	k1gFnPc1	reakce
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
reakční	reakční	k2eAgInPc1d1	reakční
obraty	obrat	k1gInPc1	obrat
<g/>
)	)	kIx)	)
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
obecnější	obecní	k2eAgFnSc2d2	obecní
entity	entita	k1gFnSc2	entita
<g/>
.	.	kIx.	.
</s>
<s>
Entity	entita	k1gFnPc1	entita
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
počet	počet	k1gInSc4	počet
látkové	látkový	k2eAgNnSc1d1	látkové
množství	množství	k1gNnSc1	množství
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
specifikovány	specifikován	k2eAgFnPc1d1	specifikována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
látkové	látkový	k2eAgNnSc4d1	látkové
množství	množství	k1gNnSc4	množství
číselně	číselně	k6eAd1	číselně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
entit	entita	k1gFnPc2	entita
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
12	[number]	k4	12
g	g	kA	g
uhlíku	uhlík	k1gInSc2	uhlík
12	[number]	k4	12
(	(	kIx(	(
<g/>
asi	asi	k9	asi
6,022	[number]	k4	6,022
<g/>
141	[number]	k4	141
<g/>
×	×	k?	×
<g/>
1023	[number]	k4	1023
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Avogadrova	Avogadrův	k2eAgFnSc1d1	Avogadrova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnPc1	značení
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
Značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
molJeden	molJeden	k2eAgInSc1d1	molJeden
mol	mol	k1gInSc1	mol
je	být	k5eAaImIp3nS	být
látkové	látkový	k2eAgNnSc4d1	látkové
množství	množství	k1gNnSc4	množství
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
počet	počet	k1gInSc1	počet
entit	entita	k1gFnPc2	entita
(	(	kIx(	(
<g/>
elementárních	elementární	k2eAgMnPc2d1	elementární
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
počtu	počet	k1gInSc2	počet
atomů	atom	k1gInPc2	atom
v	v	k7c4	v
0,012	[number]	k4	0,012
kg	kg	kA	kg
uhlíku	uhlík	k1gInSc2	uhlík
12	[number]	k4	12
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnPc1d1	další
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
kilomol	kilomol	k1gInSc1	kilomol
<g/>
,	,	kIx,	,
1	[number]	k4	1
kmol	kmola	k1gFnPc2	kmola
=	=	kIx~	=
1	[number]	k4	1
000	[number]	k4	000
mol	molo	k1gNnPc2	molo
</s>
</p>
<p>
<s>
==	==	k?	==
Výpočet	výpočet	k1gInSc1	výpočet
==	==	k?	==
</s>
</p>
<p>
<s>
Látkové	látkový	k2eAgNnSc1d1	látkové
množství	množství	k1gNnSc1	množství
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
určeno	určit	k5eAaPmNgNnS	určit
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
látkové	látkový	k2eAgNnSc1d1	látkové
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
Avogadrova	Avogadrův	k2eAgFnSc1d1	Avogadrova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
též	též	k9	též
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vzorcem	vzorec	k1gInSc7	vzorec
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
molární	molární	k2eAgFnSc1d1	molární
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tedy	tedy	k9	tedy
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
právě	právě	k6eAd1	právě
tolik	tolik	k4yIc4	tolik
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
iontů	ion	k1gInPc2	ion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
je	být	k5eAaImIp3nS	být
atomů	atom	k1gInPc2	atom
nuklidu	nuklid	k1gInSc2	nuklid
uhlíku	uhlík	k1gInSc2	uhlík
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
hmotnosti	hmotnost	k1gFnSc6	hmotnost
12	[number]	k4	12
g	g	kA	g
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
látkové	látkový	k2eAgNnSc1d1	látkové
množství	množství	k1gNnSc1	množství
1	[number]	k4	1
mol	molo	k1gNnPc2	molo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
částic	částice	k1gFnPc2	částice
</s>
</p>
<p>
<s>
Molekulová	molekulový	k2eAgFnSc1d1	molekulová
fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
Molární	molární	k2eAgInSc1d1	molární
objem	objem	k1gInSc1	objem
</s>
</p>
<p>
<s>
Molární	molární	k2eAgFnSc1d1	molární
hmotnost	hmotnost	k1gFnSc1	hmotnost
</s>
</p>
<p>
<s>
Látková	látkový	k2eAgFnSc1d1	látková
koncentrace	koncentrace	k1gFnSc1	koncentrace
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Řešené	řešený	k2eAgInPc1d1	řešený
příklady	příklad	k1gInPc1	příklad
z	z	k7c2	z
látkového	látkový	k2eAgNnSc2d1	látkové
množství	množství	k1gNnSc2	množství
</s>
</p>
