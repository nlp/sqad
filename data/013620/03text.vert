<s>
Val	val	k1gInSc4
Valentino	Valentina	k1gFnSc5
</s>
<s>
Val	val	k1gInSc4
Valentino	Valentina	k1gFnSc5
Val	val	k1gInSc4
Valentino	Valentina	k1gFnSc5
Narození	narození	k1gNnPc5
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1956	#num#	k4
<g/>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Povolání	povolání	k1gNnSc3
</s>
<s>
iluzionista	iluzionista	k1gMnSc1
Znám	znát	k5eAaImIp1nS
jako	jako	k9
</s>
<s>
Masked	Masked	k1gMnSc1
Magician	Magician	k1gMnSc1
Web	web	k1gInSc4
</s>
<s>
http://www.themaskedmagician.com/	http://www.themaskedmagician.com/	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Val	val	k1gMnSc1
Valentino	Valentino	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1956	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
kouzelník	kouzelník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgMnPc2d1
je	být	k5eAaImIp3nS
známý	známý	k1gMnSc1
jako	jako	k8xC,k8xS
Masked	Masked	k1gMnSc1
Magician	Magician	k1gMnSc1
(	(	kIx(
<g/>
Maskovaný	maskovaný	k2eAgMnSc1d1
mág	mág	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Kouzlení	kouzlení	k1gNnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
od	od	k7c2
pěti	pět	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pubertě	puberta	k1gFnSc6
napodoboval	napodobovat	k5eAaImAgMnS
a	a	k8xC
pak	pak	k6eAd1
prozrazoval	prozrazovat	k5eAaImAgInS
kouzla	kouzlo	k1gNnPc4
slavných	slavný	k2eAgMnPc2d1
kouzelníků	kouzelník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
kasínu	kasín	k1gInSc6
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystoupil	vystoupit	k5eAaPmAgMnS
i	i	k9
v	v	k7c6
show	show	k1gFnSc6
Merva	Merv	k1gMnSc2
Griffina	Griffin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
v	v	k7c6
masce	maska	k1gFnSc6
účinkoval	účinkovat	k5eAaImAgMnS
v	v	k7c6
pořadu	pořad	k1gInSc6
televize	televize	k1gFnSc1
Fox	fox	k1gInSc1
Magie	magie	k1gFnSc2
kouzla	kouzlo	k1gNnSc2
zbavená	zbavený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
99023257	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
53781671	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
99023257	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Umění	umění	k1gNnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
