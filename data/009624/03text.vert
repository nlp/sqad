<p>
<s>
Porto	porto	k1gNnSc1	porto
Katsiki	Katsik	k1gFnSc2	Katsik
je	být	k5eAaImIp3nS	být
pláž	pláž	k1gFnSc1	pláž
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
řeckého	řecký	k2eAgInSc2d1	řecký
ostrova	ostrov	k1gInSc2	ostrov
Lefkada	Lefkada	k1gFnSc1	Lefkada
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
hodnocení	hodnocení	k1gNnSc2	hodnocení
o	o	k7c4	o
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
pláže	pláž	k1gFnPc4	pláž
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
menší	malý	k2eAgFnSc6d2	menší
<g/>
,	,	kIx,	,
cca	cca	kA	cca
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
pláž	pláž	k1gFnSc4	pláž
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
průzračně	průzračně	k6eAd1	průzračně
modrá	modrý	k2eAgFnSc1d1	modrá
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
místním	místní	k2eAgNnSc7d1	místní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
pískovcovými	pískovcový	k2eAgFnPc7d1	pískovcová
skalami	skála	k1gFnPc7	skála
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouštějící	rozpouštějící	k2eAgMnSc1d1	rozpouštějící
se	se	k3xPyFc4	se
pískovec	pískovec	k1gInSc1	pískovec
pak	pak	k6eAd1	pak
dodává	dodávat	k5eAaImIp3nS	dodávat
moři	moře	k1gNnSc3	moře
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
Porto	porto	k1gNnSc1	porto
Katsiki	Katsik	k1gFnSc2	Katsik
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
Porto	porto	k1gNnSc1	porto
Katsiki	Katsik	k1gFnSc2	Katsik
fan	fana	k1gFnPc2	fana
club	club	k1gInSc1	club
</s>
</p>
