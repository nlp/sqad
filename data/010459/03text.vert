<p>
<s>
Filip	Filip	k1gMnSc1	Filip
Prosper	Prosper	k1gMnSc1	Prosper
Španělský	španělský	k2eAgMnSc1d1	španělský
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Felipe	Felip	k1gInSc5	Felip
Próspero	Próspero	k1gNnSc1	Próspero
José	Josá	k1gFnPc1	Josá
Francisco	Francisco	k1gMnSc1	Francisco
Domingo	Domingo	k1gMnSc1	Domingo
Ignacio	Ignacio	k1gMnSc1	Ignacio
Antonio	Antonio	k1gMnSc1	Antonio
Buenaventura	Buenaventura	k1gFnSc1	Buenaventura
Diego	Diego	k6eAd1	Diego
Miguel	Miguel	k1gMnSc1	Miguel
Luis	Luisa	k1gFnPc2	Luisa
Alfonso	Alfonso	k1gMnSc1	Alfonso
Isidro	Isidra	k1gFnSc5	Isidra
Ramón	Ramón	k1gMnSc1	Ramón
Víctor	Víctor	k1gInSc1	Víctor
<g/>
;	;	kIx,	;
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1657	[number]	k4	1657
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
infant	infant	k1gMnSc1	infant
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
asturijský	asturijský	k2eAgMnSc1d1	asturijský
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
linie	linie	k1gFnSc2	linie
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
ženy	žena	k1gFnPc1	žena
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
neteří	neteř	k1gFnSc7	neteř
svého	svůj	k3xOyFgMnSc2	svůj
chotě	choť	k1gMnSc2	choť
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
Marie	Marie	k1gFnSc1	Marie
Tereza	Tereza	k1gFnSc1	Tereza
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
královnou	královna	k1gFnSc7	královna
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Markéta	Markéta	k1gFnSc1	Markéta
Tereza	Tereza	k1gFnSc1	Tereza
císařovnou	císařovna	k1gFnSc7	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Filipa	Filip	k1gMnSc2	Filip
Prospera	Prosper	k1gMnSc2	Prosper
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
poledním	polední	k2eAgMnSc7d1	polední
Habsburkem	Habsburk	k1gMnSc7	Habsburk
na	na	k7c6	na
španělském	španělský	k2eAgInSc6d1	španělský
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgInS	dožít
pouhých	pouhý	k2eAgNnPc2d1	pouhé
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
pozice	pozice	k1gFnSc2	pozice
Španělska	Španělsko	k1gNnSc2	Španělsko
vůči	vůči	k7c3	vůči
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1659	[number]	k4	1659
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
narození	narození	k1gNnSc2	narození
měl	mít	k5eAaImAgMnS	mít
sklony	sklona	k1gFnSc2	sklona
k	k	k7c3	k
epileptickým	epileptický	k2eAgInPc3d1	epileptický
záchvatům	záchvat	k1gInPc3	záchvat
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gFnSc2	jeho
předčasné	předčasný	k2eAgFnSc2d1	předčasná
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1	pohřben
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
hrobky	hrobka	k1gFnSc2	hrobka
v	v	k7c6	v
El	Ela	k1gFnPc2	Ela
Escorialu	Escorial	k1gInSc2	Escorial
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
122	[number]	k4	122
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Filip	Filip	k1gMnSc1	Filip
Prosper	Prosper	k1gMnSc1	Prosper
Španělský	španělský	k2eAgMnSc1d1	španělský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
