<s>
Byl	být	k5eAaImAgMnS	být
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
ženy	žena	k1gFnPc1	žena
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
neteří	neteř	k1gFnSc7	neteř
svého	svůj	k3xOyFgMnSc2	svůj
chotě	choť	k1gMnSc2	choť
<g/>
.	.	kIx.	.
</s>
