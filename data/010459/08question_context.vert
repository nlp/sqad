<s>
Filip	Filip	k1gMnSc1	Filip
Prosper	Prosper	k1gMnSc1	Prosper
Španělský	španělský	k2eAgMnSc1d1	španělský
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Felipe	Felip	k1gInSc5	Felip
Próspero	Próspero	k1gNnSc1	Próspero
José	Josá	k1gFnPc1	Josá
Francisco	Francisco	k1gMnSc1	Francisco
Domingo	Domingo	k1gMnSc1	Domingo
Ignacio	Ignacio	k1gMnSc1	Ignacio
Antonio	Antonio	k1gMnSc1	Antonio
Buenaventura	Buenaventura	k1gFnSc1	Buenaventura
Diego	Diego	k6eAd1	Diego
Miguel	Miguel	k1gMnSc1	Miguel
Luis	Luisa	k1gFnPc2	Luisa
Alfonso	Alfonso	k1gMnSc1	Alfonso
Isidro	Isidra	k1gFnSc5	Isidra
Ramón	Ramón	k1gMnSc1	Ramón
Víctor	Víctor	k1gInSc1	Víctor
<g/>
;	;	kIx,	;
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1657	[number]	k4	1657
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
infant	infant	k1gMnSc1	infant
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
asturijský	asturijský	k2eAgMnSc1d1	asturijský
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
linie	linie	k1gFnSc2	linie
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
ženy	žena	k1gFnPc1	žena
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
neteří	neteř	k1gFnSc7	neteř
svého	svůj	k3xOyFgMnSc2	svůj
chotě	choť	k1gMnSc2	choť
<g/>
.	.	kIx.	.
</s>
