<s>
Golfský	golfský	k2eAgInSc4d1	golfský
proud	proud	k1gInSc4	proud
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
severní	severní	k2eAgFnPc1d1	severní
větve	větev	k1gFnPc1	větev
-	-	kIx~	-
Irmingerův	Irmingerův	k2eAgInSc4d1	Irmingerův
<g/>
,	,	kIx,	,
Norský	norský	k2eAgInSc4d1	norský
a	a	k8xC	a
Severoatlantický	severoatlantický	k2eAgInSc4d1	severoatlantický
proud	proud	k1gInSc4	proud
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
teplý	teplý	k2eAgInSc1d1	teplý
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
rychlý	rychlý	k2eAgInSc1d1	rychlý
mořský	mořský	k2eAgInSc1d1	mořský
proud	proud	k1gInSc1	proud
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
