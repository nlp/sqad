<p>
<s>
Kuře	kura	k1gFnSc3	kura
huli-huli	huliule	k1gFnSc3	huli-hule
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
pokrmů	pokrm	k1gInPc2	pokrm
havajské	havajský	k2eAgFnSc2d1	Havajská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Kuřecí	kuřecí	k2eAgNnSc1d1	kuřecí
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
marinuje	marinovat	k5eAaBmIp3nS	marinovat
v	v	k7c6	v
huli-huli	huliule	k1gFnSc6	huli-hule
omáčce	omáčka	k1gFnSc6	omáčka
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
griluje	grilovat	k5eAaImIp3nS	grilovat
<g/>
.	.	kIx.	.
</s>
<s>
Marináda	marináda	k1gFnSc1	marináda
huli-huli	huliule	k1gFnSc4	huli-hule
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
složek	složka	k1gFnPc2	složka
–	–	k?	–
sojové	sojový	k2eAgFnSc2d1	sojová
omáčky	omáčka	k1gFnSc2	omáčka
a	a	k8xC	a
zázvoru	zázvor	k1gInSc2	zázvor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
často	často	k6eAd1	často
také	také	k9	také
přidává	přidávat	k5eAaImIp3nS	přidávat
česnek	česnek	k1gInSc4	česnek
<g/>
,	,	kIx,	,
kečup	kečup	k1gInSc4	kečup
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
sherry	sherry	k1gNnSc4	sherry
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
marináda	marináda	k1gFnSc1	marináda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vepřového	vepřový	k2eAgNnSc2d1	vepřové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kuře	kuře	k1gNnSc1	kuře
huli-huli	huliule	k1gFnSc4	huli-hule
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Hawaiian	Hawaiian	k1gInSc1	Hawaiian
Huli	Hul	k1gFnSc2	Hul
Huli	Hul	k1gFnSc2	Hul
Chicken	Chickna	k1gFnPc2	Chickna
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
