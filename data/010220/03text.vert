<p>
<s>
Informované	informovaný	k2eAgNnSc1d1	informované
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
je	být	k5eAaImIp3nS	být
právně	právně	k6eAd1	právně
etický	etický	k2eAgInSc1d1	etický
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
(	(	kIx(	(
<g/>
právnickými	právnický	k2eAgFnPc7d1	právnická
<g/>
,	,	kIx,	,
fyzickými	fyzický	k2eAgFnPc7d1	fyzická
<g/>
)	)	kIx)	)
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
nějaké	nějaký	k3yIgFnSc2	nějaký
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
zubař	zubař	k1gMnSc1	zubař
<g/>
,	,	kIx,	,
plastický	plastický	k2eAgMnSc1d1	plastický
chirurg	chirurg	k1gMnSc1	chirurg
<g/>
...	...	k?	...
před	před	k7c7	před
každým	každý	k3xTgInSc7	každý
zásadním	zásadní	k2eAgInSc7d1	zásadní
zákrokem	zákrok	k1gInSc7	zákrok
by	by	kYmCp3nP	by
měl	mít	k5eAaImAgInS	mít
pacientovi	pacient	k1gMnSc3	pacient
objasnit	objasnit	k5eAaPmF	objasnit
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
má	mít	k5eAaImIp3nS	mít
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc1	jaký
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnPc4	jejich
výhody	výhoda	k1gFnPc4	výhoda
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
hrazené	hrazený	k2eAgInPc1d1	hrazený
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
medicíny	medicína	k1gFnSc2	medicína
se	se	k3xPyFc4	se
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
pacienta	pacient	k1gMnSc4	pacient
častěji	často	k6eAd2	často
říká	říkat	k5eAaImIp3nS	říkat
informovaný	informovaný	k2eAgInSc1d1	informovaný
souhlas	souhlas	k1gInSc1	souhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákazník	zákazník	k1gMnSc1	zákazník
kupující	kupující	k1gMnSc1	kupující
zboží	zboží	k1gNnSc4	zboží
nebo	nebo	k8xC	nebo
služby	služba	k1gFnPc1	služba
e-shop	ehop	k1gInSc1	e-shop
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
od	od	k7c2	od
provozovatele	provozovatel	k1gMnSc2	provozovatel
e-shopu	ehopa	k1gFnSc4	e-shopa
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
obchodním	obchodní	k2eAgFnPc3d1	obchodní
podmínkám	podmínka	k1gFnPc3	podmínka
a	a	k8xC	a
při	pře	k1gFnSc3	pře
učinění	učinění	k1gNnSc2	učinění
závazné	závazný	k2eAgFnSc2d1	závazná
objednávky	objednávka	k1gFnSc2	objednávka
srozuměn	srozumět	k5eAaPmNgMnS	srozumět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
podmínkami	podmínka	k1gFnPc7	podmínka
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bankovní	bankovní	k2eAgMnSc1d1	bankovní
poradce	poradce	k1gMnSc1	poradce
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
před	před	k7c7	před
využitím	využití	k1gNnSc7	využití
leasingu	leasing	k1gInSc2	leasing
<g/>
,	,	kIx,	,
zápůjčky	zápůjčka	k1gFnPc1	zápůjčka
na	na	k7c4	na
splátky	splátka	k1gFnPc4	splátka
<g/>
,	,	kIx,	,
hypotéky	hypotéka	k1gFnPc4	hypotéka
či	či	k8xC	či
obdobného	obdobný	k2eAgInSc2d1	obdobný
finančního	finanční	k2eAgInSc2d1	finanční
produktu	produkt	k1gInSc2	produkt
klientovi	klientův	k2eAgMnPc1d1	klientův
objasnit	objasnit	k5eAaPmF	objasnit
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
itinerář	itinerář	k1gInSc4	itinerář
splátek	splátka	k1gFnPc2	splátka
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
může	moct	k5eAaImIp3nS	moct
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
brzké	brzký	k2eAgNnSc4d1	brzké
splacení	splacení	k1gNnSc4	splacení
nebo	nebo	k8xC	nebo
opozdit	opozdit	k5eAaPmF	opozdit
platbu	platba	k1gFnSc4	platba
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
jsou	být	k5eAaImIp3nP	být
penále	penále	k1gNnSc4	penále
za	za	k7c4	za
opoždění	opoždění	k1gNnSc4	opoždění
platby	platba	k1gFnSc2	platba
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
informované	informovaný	k2eAgNnSc4d1	informované
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
vstup	vstup	k1gInSc4	vstup
osob	osoba	k1gFnPc2	osoba
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
určitými	určitý	k2eAgFnPc7d1	určitá
interními	interní	k2eAgFnPc7d1	interní
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
řády	řád	k1gInPc7	řád
či	či	k8xC	či
předpisy	předpis	k1gInPc7	předpis
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přepravní	přepravní	k2eAgInSc4d1	přepravní
prostor	prostor	k1gInSc4	prostor
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
viditelně	viditelně	k6eAd1	viditelně
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
a	a	k8xC	a
instrukce	instrukce	k1gFnSc1	instrukce
jemu	on	k3xPp3gInSc3	on
podléhající	podléhající	k2eAgFnPc4d1	podléhající
viditelně	viditelně	k6eAd1	viditelně
umístěny	umístit	k5eAaPmNgInP	umístit
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Informované	informovaný	k2eAgNnSc1d1	informované
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
předchází	předcházet	k5eAaImIp3nS	předcházet
(	(	kIx(	(
<g/>
explicitní	explicitní	k2eAgNnSc4d1	explicitní
nebo	nebo	k8xC	nebo
formální	formální	k2eAgNnSc4d1	formální
<g/>
)	)	kIx)	)
uzavření	uzavření	k1gNnSc4	uzavření
nějaké	nějaký	k3yIgFnSc2	nějaký
smlouvy	smlouva	k1gFnSc2	smlouva
nebo	nebo	k8xC	nebo
dohody	dohoda	k1gFnSc2	dohoda
pro	pro	k7c4	pro
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
určité	určitý	k2eAgFnSc2d1	určitá
služby	služba	k1gFnSc2	služba
nebo	nebo	k8xC	nebo
zboží	zboží	k1gNnSc2	zboží
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
subjekty	subjekt	k1gInPc7	subjekt
(	(	kIx(	(
<g/>
nazvěme	nazvat	k5eAaBmRp1nP	nazvat
je	on	k3xPp3gNnSc4	on
třeba	třeba	k6eAd1	třeba
poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
a	a	k8xC	a
klient	klient	k1gMnSc1	klient
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
jistá	jistý	k2eAgNnPc4d1	jisté
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
by	by	kYmCp3nP	by
nějakou	nějaký	k3yIgFnSc4	nějaký
formou	forma	k1gFnSc7	forma
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
poskytovatelem	poskytovatel	k1gMnSc7	poskytovatel
postaven	postavit	k5eAaPmNgInS	postavit
před	před	k7c4	před
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
nabízenou	nabízený	k2eAgFnSc4d1	nabízená
službu	služba	k1gFnSc4	služba
chce	chtít	k5eAaImIp3nS	chtít
využít	využít	k5eAaPmF	využít
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
jaké	jaký	k3yRgNnSc4	jaký
zboží	zboží	k1gNnSc4	zboží
koupit	koupit	k5eAaPmF	koupit
<g/>
)	)	kIx)	)
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
být	být	k5eAaImF	být
dána	dát	k5eAaPmNgFnS	dát
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
odstoupení	odstoupení	k1gNnSc2	odstoupení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nijak	nijak	k6eAd1	nijak
nepodmíněná	podmíněný	k2eNgFnSc1d1	nepodmíněná
a	a	k8xC	a
nepenalizovaná	penalizovaný	k2eNgFnSc1d1	penalizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
informovaného	informovaný	k2eAgInSc2d1	informovaný
souhlasu	souhlas	k1gInSc2	souhlas
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různá	různý	k2eAgNnPc4d1	různé
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
ceny	cena	k1gFnSc2	cena
služby	služba	k1gFnSc2	služba
či	či	k8xC	či
zboží	zboží	k1gNnSc2	zboží
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
aplikaci	aplikace	k1gFnSc4	aplikace
zubní	zubní	k2eAgFnSc2d1	zubní
plomby	plomba	k1gFnSc2	plomba
většinou	většina	k1gFnSc7	většina
stačí	stačit	k5eAaBmIp3nS	stačit
slovní	slovní	k2eAgInSc4d1	slovní
souhlas	souhlas	k1gInSc4	souhlas
nebo	nebo	k8xC	nebo
i	i	k8xC	i
kývnutí	kývnutí	k1gNnSc1	kývnutí
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
u	u	k7c2	u
statisícové	statisícový	k2eAgFnSc2d1	statisícová
hypotéky	hypotéka	k1gFnSc2	hypotéka
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
písemně	písemně	k6eAd1	písemně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Informovaným	informovaný	k2eAgInSc7d1	informovaný
souhlasem	souhlas	k1gInSc7	souhlas
se	se	k3xPyFc4	se
klient	klient	k1gMnSc1	klient
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
dané	daný	k2eAgFnSc2d1	daná
transakce	transakce	k1gFnSc2	transakce
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
předmětné	předmětný	k2eAgFnSc3d1	předmětná
smlouvě	smlouva	k1gFnSc3	smlouva
nebo	nebo	k8xC	nebo
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc2	on
vyplývajících	vyplývající	k2eAgFnPc2d1	vyplývající
povinností	povinnost	k1gFnPc2	povinnost
a	a	k8xC	a
penále	penále	k1gNnSc1	penále
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
stanovených	stanovený	k2eAgFnPc2d1	stanovená
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
klienta	klient	k1gMnSc4	klient
odvedení	odvedení	k1gNnSc2	odvedení
služby	služba	k1gFnSc2	služba
nebo	nebo	k8xC	nebo
dodání	dodání	k1gNnSc2	dodání
zboží	zboží	k1gNnSc2	zboží
znamená	znamenat	k5eAaImIp3nS	znamenat
určité	určitý	k2eAgInPc4d1	určitý
výdaje	výdaj	k1gInPc4	výdaj
(	(	kIx(	(
<g/>
nezřídka	nezřídka	k6eAd1	nezřídka
už	už	k9	už
jen	jen	k9	jen
za	za	k7c4	za
započetí	započetí	k1gNnPc4	započetí
plnění	plnění	k1gNnSc2	plnění
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
služba	služba	k1gFnSc1	služba
či	či	k8xC	či
zboží	zboží	k1gNnSc1	zboží
dostane	dostat	k5eAaPmIp3nS	dostat
ke	k	k7c3	k
klientovi	klient	k1gMnSc3	klient
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
smlouvou	smlouva	k1gFnSc7	smlouva
a	a	k8xC	a
informovaným	informovaný	k2eAgInSc7d1	informovaný
souhlasem	souhlas	k1gInSc7	souhlas
získává	získávat	k5eAaImIp3nS	získávat
možnost	možnost	k1gFnSc1	možnost
vydobýt	vydobýt	k5eAaPmF	vydobýt
si	se	k3xPyFc3	se
uhrazení	uhrazení	k1gNnSc4	uhrazení
těchto	tento	k3xDgInPc2	tento
výdajů	výdaj	k1gInPc2	výdaj
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
chtít	chtít	k5eAaImF	chtít
klient	klient	k1gMnSc1	klient
odstoupit	odstoupit	k5eAaPmF	odstoupit
někde	někde	k6eAd1	někde
uprostřed	uprostřed	k7c2	uprostřed
procesu	proces	k1gInSc2	proces
plnění	plnění	k1gNnSc2	plnění
<g/>
.	.	kIx.	.
</s>
</p>
