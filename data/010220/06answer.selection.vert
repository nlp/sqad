<s>
Informované	informovaný	k2eAgNnSc1d1	informované
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
je	být	k5eAaImIp3nS	být
právně	právně	k6eAd1	právně
etický	etický	k2eAgInSc1d1	etický
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
(	(	kIx(	(
<g/>
právnickými	právnický	k2eAgFnPc7d1	právnická
<g/>
,	,	kIx,	,
fyzickými	fyzický	k2eAgFnPc7d1	fyzická
<g/>
)	)	kIx)	)
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
nějaké	nějaký	k3yIgFnSc2	nějaký
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
