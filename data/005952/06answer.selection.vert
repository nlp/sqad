<s>
Fénické	fénický	k2eAgInPc1d1	fénický
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
fénicko-kanaánské	fénickoanaánský	k2eAgInPc1d1	fénicko-kanaánský
<g/>
)	)	kIx)	)
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
protosinajského	protosinajský	k2eAgNnSc2d1	protosinajský
patrně	patrně	k6eAd1	patrně
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
(	(	kIx(	(
<g/>
Fénicie	Fénicie	k1gFnSc1	Fénicie
nebo	nebo	k8xC	nebo
Kanaán	Kanaán	k1gInSc1	Kanaán
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
