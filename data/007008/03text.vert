<s>
III	III	kA	III
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
v	v	k7c6	v
Saint	Sainta	k1gFnPc2	Sainta
Louis	louis	k1gInSc2	louis
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Missouri	Missouri	k1gFnSc2	Missouri
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
hry	hra	k1gFnPc1	hra
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
historickým	historický	k2eAgNnSc7d1	historické
výročím	výročí	k1gNnSc7	výročí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
především	především	k9	především
domácí	domácí	k2eAgMnPc1d1	domácí
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
,	,	kIx,	,
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
a	a	k8xC	a
Kubánci	Kubánec	k1gMnPc1	Kubánec
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
sportovců	sportovec	k1gMnPc2	sportovec
se	se	k3xPyFc4	se
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
ani	ani	k8xC	ani
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
kandidátským	kandidátský	k2eAgNnSc7d1	kandidátské
městem	město	k1gNnSc7	město
na	na	k7c6	na
uspořádání	uspořádání	k1gNnSc6	uspořádání
3	[number]	k4	3
<g/>
.	.	kIx.	.
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
bylo	být	k5eAaImAgNnS	být
Chicago	Chicago	k1gNnSc4	Chicago
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pořadatelská	pořadatelský	k2eAgFnSc1d1	pořadatelská
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
tučně	tučně	k6eAd1	tučně
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Výsledky	výsledek	k1gInPc1	výsledek
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1904	[number]	k4	1904
</s>
