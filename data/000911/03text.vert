<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
podle	podle	k7c2	podle
křestního	křestní	k2eAgInSc2d1	křestní
záznamu	záznam	k1gInSc2	záznam
Joannes	Joannes	k1gMnSc1	Joannes
Chrysostomus	Chrysostomus	k1gMnSc1	Chrysostomus
Wolfgangus	Wolfgangus	k1gMnSc1	Wolfgangus
Theophilus	Theophilus	k1gMnSc1	Theophilus
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1756	[number]	k4	1756
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
klasicistní	klasicistní	k2eAgMnSc1d1	klasicistní
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
<g/>
;	;	kIx,	;
geniální	geniální	k2eAgMnSc1d1	geniální
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
složil	složit	k5eAaPmAgInS	složit
626	[number]	k4	626
děl	dělo	k1gNnPc2	dělo
světského	světský	k2eAgInSc2d1	světský
i	i	k8xC	i
duchovního	duchovní	k2eAgInSc2d1	duchovní
charakteru	charakter	k1gInSc2	charakter
-	-	kIx~	-
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
symfonie	symfonie	k1gFnSc2	symfonie
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnSc4d1	komorní
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
mše	mše	k1gFnPc4	mše
a	a	k8xC	a
chorály	chorál	k1gInPc4	chorál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
dětství	dětství	k1gNnSc6	dětství
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
opustil	opustit	k5eAaPmAgInS	opustit
pozici	pozice	k1gFnSc4	pozice
dvorního	dvorní	k2eAgMnSc2d1	dvorní
skladatele	skladatel	k1gMnSc2	skladatel
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
,	,	kIx,	,
nenalezl	naleznout	k5eNaPmAgMnS	naleznout
již	již	k6eAd1	již
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
proslulost	proslulost	k1gFnSc4	proslulost
stálé	stálý	k2eAgFnSc2d1	stálá
placené	placený	k2eAgFnSc2d1	placená
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
existence	existence	k1gFnSc2	existence
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěl	chtít	k5eNaImAgMnS	chtít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
lokaj	lokaj	k1gMnSc1	lokaj
aristokratické	aristokratický	k2eAgFnSc2d1	aristokratická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
zůstat	zůstat	k5eAaPmF	zůstat
svobodným	svobodný	k2eAgMnSc7d1	svobodný
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnPc4	jeho
nejproslulejší	proslulý	k2eAgNnPc4d3	nejproslulejší
díla	dílo	k1gNnPc4	dílo
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
opery	opera	k1gFnSc2	opera
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
a	a	k8xC	a
Don	Don	k1gMnSc1	Don
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
monumentální	monumentální	k2eAgInSc1d1	monumentální
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
opus	opus	k1gInSc1	opus
<g/>
,	,	kIx,	,
zádušní	zádušní	k2eAgFnSc2d1	zádušní
mše	mše	k1gFnSc2	mše
Requiem	Requius	k1gMnSc7	Requius
d	d	k?	d
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
KV	KV	kA	KV
626	[number]	k4	626
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1756	[number]	k4	1756
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
jako	jako	k9	jako
nejmladší	mladý	k2eAgInSc4d3	nejmladší
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
dětí	dítě	k1gFnPc2	dítě
Leopolda	Leopolda	k1gFnSc1	Leopolda
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
Marie	Maria	k1gFnSc2	Maria
Mozartových	Mozartová	k1gFnPc2	Mozartová
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
sedmi	sedm	k4xCc2	sedm
sourozenců	sourozenec	k1gMnPc2	sourozenec
se	se	k3xPyFc4	se
však	však	k9	však
dospělosti	dospělost	k1gFnSc2	dospělost
dožili	dožít	k5eAaPmAgMnP	dožít
jen	jen	k9	jen
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc4	jeho
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
Maria	Maria	k1gFnSc1	Maria
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Nannerl	Nannerl	k1gInSc1	Nannerl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
předčasná	předčasný	k2eAgNnPc4d1	předčasné
úmrtí	úmrtí	k1gNnSc4	úmrtí
Wolfgangových	Wolfgangův	k2eAgMnPc2d1	Wolfgangův
sourozenců	sourozenec	k1gMnPc2	sourozenec
mohly	moct	k5eAaImAgFnP	moct
především	především	k6eAd1	především
dětské	dětský	k2eAgFnPc4d1	dětská
nemoci	nemoc	k1gFnPc4	nemoc
a	a	k8xC	a
špatná	špatný	k2eAgFnSc1d1	špatná
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
následující	následující	k2eAgNnPc4d1	následující
křestní	křestní	k2eAgNnPc4d1	křestní
jména	jméno	k1gNnPc4	jméno
<g/>
:	:	kIx,	:
Joannes	Joannes	k1gMnSc1	Joannes
Chrysostomus	Chrysostomus	k1gMnSc1	Chrysostomus
Wolfgangus	Wolfgangus	k1gMnSc1	Wolfgangus
Theophilus	Theophilus	k1gMnSc1	Theophilus
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Joannes	Joannes	k1gMnSc1	Joannes
Chrysostomus	Chrysostomus	k1gMnSc1	Chrysostomus
(	(	kIx(	(
<g/>
=	=	kIx~	=
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Zlatoústý	zlatoústý	k2eAgMnSc1d1	zlatoústý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
chrysos	chrysos	k1gInSc1	chrysos
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
řecky	řecky	k6eAd1	řecky
"	"	kIx"	"
<g/>
zlato	zlato	k1gNnSc4	zlato
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
stoma	stoma	k1gFnSc1	stoma
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
ústa	ústa	k1gNnPc4	ústa
<g/>
;	;	kIx,	;
jméno	jméno	k1gNnSc4	jméno
Wolfgangus	Wolfgangus	k1gInSc4	Wolfgangus
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
dědovi	děd	k1gMnSc6	děd
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
Pertl	Pertl	k1gMnSc1	Pertl
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jméno	jméno	k1gNnSc1	jméno
Theophilus	Theophilus	k1gInSc1	Theophilus
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Gottlieb	Gottliba	k1gFnPc2	Gottliba
<g/>
)	)	kIx)	)
dostal	dostat	k5eAaPmAgMnS	dostat
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
po	po	k7c6	po
kmotrovi	kmotr	k1gMnSc6	kmotr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Johann	Johann	k1gMnSc1	Johann
Theophilius	Theophilius	k1gMnSc1	Theophilius
Pergmayr	Pergmayr	k1gMnSc1	Pergmayr
<g/>
.	.	kIx.	.
</s>
<s>
Theophilius	Theophilius	k1gMnSc1	Theophilius
<g/>
/	/	kIx~	/
<g/>
Theofilus	Theofilus	k1gMnSc1	Theofilus
<g/>
,	,	kIx,	,
také	také	k9	také
Gottlieb	Gottliba	k1gFnPc2	Gottliba
či	či	k8xC	či
Amadeus	Amadeus	k1gMnSc1	Amadeus
jsou	být	k5eAaImIp3nP	být
překlady	překlad	k1gInPc1	překlad
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
známe	znát	k5eAaImIp1nP	znát
jako	jako	k9	jako
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Mozart	Mozart	k1gMnSc1	Mozart
sám	sám	k3xTgMnSc1	sám
kdy	kdy	k6eAd1	kdy
používal	používat	k5eAaImAgMnS	používat
své	svůj	k3xOyFgNnSc4	svůj
nyní	nyní	k6eAd1	nyní
běžné	běžný	k2eAgFnPc1d1	běžná
druhé	druhý	k4xOgFnPc1	druhý
(	(	kIx(	(
<g/>
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
pořadí	pořadí	k1gNnSc6	pořadí
ovšem	ovšem	k9	ovšem
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
<g/>
)	)	kIx)	)
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
jména	jméno	k1gNnSc2	jméno
Wolfgangus	Wolfgangus	k1gMnSc1	Wolfgangus
a	a	k8xC	a
Theophilus	Theophilus	k1gMnSc1	Theophilus
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
upravil	upravit	k5eAaPmAgMnS	upravit
na	na	k7c4	na
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadé	Amadý	k2eAgInPc4d1	Amadý
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
i	i	k9	i
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
Wolfgango	Wolfgango	k1gMnSc1	Wolfgango
Amadeo	Amadeo	k1gMnSc1	Amadeo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Amadeus	Amadeus	k1gMnSc1	Amadeus
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
označil	označit	k5eAaPmAgMnS	označit
jen	jen	k6eAd1	jen
v	v	k7c6	v
žertu	žert	k1gInSc6	žert
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
dopisech	dopis	k1gInPc6	dopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
cestoval	cestovat	k5eAaImAgMnS	cestovat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
na	na	k7c6	na
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
hudebních	hudební	k2eAgNnPc6d1	hudební
turné	turné	k1gNnPc6	turné
po	po	k7c6	po
metropolích	metropol	k1gFnPc6	metropol
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tehdy	tehdy	k6eAd1	tehdy
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
trvajícím	trvající	k2eAgInSc7d1	trvající
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
navštívili	navštívit	k5eAaPmAgMnP	navštívit
severní	severní	k2eAgNnSc4d1	severní
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Haag	Haag	k1gInSc1	Haag
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
koncert	koncert	k1gInSc1	koncert
odehrál	odehrát	k5eAaPmAgInS	odehrát
i	i	k9	i
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Johannem	Johann	k1gMnSc7	Johann
Christianem	Christian	k1gMnSc7	Christian
Bachem	Bach	k1gInSc7	Bach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zdokonalovat	zdokonalovat	k5eAaImF	zdokonalovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
a	a	k8xC	a
poznal	poznat	k5eAaPmAgMnS	poznat
z	z	k7c2	z
blízka	blízko	k1gNnSc2	blízko
prostředí	prostředí	k1gNnSc2	prostředí
italské	italský	k2eAgFnSc2d1	italská
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
(	(	kIx(	(
<g/>
rodiče	rodič	k1gMnPc1	rodič
jej	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Wolferl	Wolferl	k1gInSc4	Wolferl
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
vlček	vlček	k1gMnSc1	vlček
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Nannerl	Nannerlum	k1gNnPc2	Nannerlum
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
představováni	představován	k2eAgMnPc1d1	představován
jako	jako	k8xS	jako
zázračné	zázračný	k2eAgFnPc1d1	zázračná
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
podnikl	podniknout	k5eAaPmAgMnS	podniknout
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
studijní	studijní	k2eAgFnPc4d1	studijní
cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
otcových	otcův	k2eAgFnPc2d1	otcova
známostí	známost	k1gFnPc2	známost
získával	získávat	k5eAaImAgInS	získávat
od	od	k7c2	od
šlechtických	šlechtický	k2eAgMnPc2d1	šlechtický
příznivců	příznivec	k1gMnPc2	příznivec
objednávky	objednávka	k1gFnSc2	objednávka
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
opery	opera	k1gFnPc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
poznal	poznat	k5eAaPmAgInS	poznat
italský	italský	k2eAgInSc1d1	italský
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
uznáván	uznávat	k5eAaImNgInS	uznávat
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
a	a	k8xC	a
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
<g/>
,	,	kIx,	,
a	a	k8xC	a
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
dvě	dva	k4xCgFnPc4	dva
typicky	typicky	k6eAd1	typicky
italské	italský	k2eAgFnSc2d1	italská
opery	opera	k1gFnSc2	opera
<g/>
:	:	kIx,	:
Mithridates	Mithridates	k1gMnSc1	Mithridates
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
pontský	pontský	k2eAgMnSc1d1	pontský
(	(	kIx(	(
<g/>
Mitridate	Mitridat	k1gMnSc5	Mitridat
<g/>
,	,	kIx,	,
re	re	k9	re
di	di	k?	di
Ponto	Ponto	k1gNnSc1	Ponto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lucio	Lucio	k1gMnSc1	Lucio
Silla	Silla	k1gMnSc1	Silla
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
serenádu	serenáda	k1gFnSc4	serenáda
Ascanius	Ascanius	k1gInSc4	Ascanius
v	v	k7c6	v
Albě	alba	k1gFnSc6	alba
(	(	kIx(	(
<g/>
Ascanio	Ascanio	k6eAd1	Ascanio
in	in	k?	in
Alba	alba	k1gFnSc1	alba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
italské	italský	k2eAgFnSc2d1	italská
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Vídeň	Vídeň	k1gFnSc4	Vídeň
a	a	k8xC	a
Mnichov	Mnichov	k1gInSc4	Mnichov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
premiéra	premiéra	k1gFnSc1	premiéra
jeho	jeho	k3xOp3gFnSc2	jeho
opery	opera	k1gFnSc2	opera
Zahradnice	zahradnice	k1gFnSc2	zahradnice
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
finta	finta	k1gFnSc1	finta
giardiniera	giardiniera	k1gFnSc1	giardiniera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
fenomenálními	fenomenální	k2eAgFnPc7d1	fenomenální
schopnostmi	schopnost	k1gFnPc7	schopnost
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
útlého	útlý	k2eAgNnSc2d1	útlé
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Skládal	skládat	k5eAaImAgMnS	skládat
již	již	k6eAd1	již
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
a	a	k8xC	a
předváděl	předvádět	k5eAaImAgMnS	předvádět
svá	svůj	k3xOyFgNnPc4	svůj
raná	raný	k2eAgNnPc4d1	rané
díla	dílo	k1gNnPc4	dílo
před	před	k7c7	před
evropskou	evropský	k2eAgFnSc7d1	Evropská
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnán	k2eAgMnSc1d1	zaměstnán
jako	jako	k8xC	jako
dvorní	dvorní	k2eAgMnSc1d1	dvorní
skladatel	skladatel	k1gMnSc1	skladatel
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
,	,	kIx,	,
sídelním	sídelní	k2eAgNnSc6d1	sídelní
městě	město	k1gNnSc6	město
Salzburského	salzburský	k2eAgNnSc2d1	Salzburské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
nesoustředěným	soustředěný	k2eNgMnSc7d1	nesoustředěný
a	a	k8xC	a
hledal	hledat	k5eAaImAgInS	hledat
lepší	dobrý	k2eAgNnSc4d2	lepší
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
epidemie	epidemie	k1gFnSc1	epidemie
černých	černý	k2eAgFnPc2d1	černá
neštovic	neštovice	k1gFnPc2	neštovice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
ani	ani	k8xC	ani
císařské	císařský	k2eAgFnSc3d1	císařská
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Mozartovi	Mozart	k1gMnSc3	Mozart
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
nacházeli	nacházet	k5eAaImAgMnP	nacházet
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Leopold	Leopold	k1gMnSc1	Leopold
Mozart	Mozart	k1gMnSc1	Mozart
chtěl	chtít	k5eAaImAgMnS	chtít
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
svatby	svatba	k1gFnSc2	svatba
v	v	k7c6	v
císařské	císařský	k2eAgFnSc6d1	císařská
rodině	rodina	k1gFnSc6	rodina
uplatnit	uplatnit	k5eAaPmF	uplatnit
nadání	nadání	k1gNnSc4	nadání
své	své	k1gNnSc4	své
i	i	k9	i
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
vydělat	vydělat	k5eAaPmF	vydělat
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
svatby	svatba	k1gFnSc2	svatba
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
císařské	císařský	k2eAgFnSc6d1	císařská
rodině	rodina	k1gFnSc6	rodina
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Leopold	Leopold	k1gMnSc1	Leopold
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Nannerl	Nannerla	k1gFnPc2	Nannerla
a	a	k8xC	a
jedenáctiletým	jedenáctiletý	k2eAgMnSc7d1	jedenáctiletý
synem	syn	k1gMnSc7	syn
Wolfgangem	Wolfgang	k1gMnSc7	Wolfgang
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odjet	odjet	k5eAaPmF	odjet
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijeli	přijet	k5eAaPmAgMnP	přijet
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1767	[number]	k4	1767
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
moravská	moravský	k2eAgFnSc1d1	Moravská
šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
venkovských	venkovský	k2eAgInPc6d1	venkovský
statcích	statek	k1gInPc6	statek
<g/>
,	,	kIx,	,
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	s	k7c7	s
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
hostinci	hostinec	k1gInSc6	hostinec
"	"	kIx"	"
<g/>
U	u	k7c2	u
Černého	Černý	k1gMnSc2	Černý
orla	orel	k1gMnSc2	orel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Hauenschildův	Hauenschildův	k2eAgInSc1d1	Hauenschildův
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
u	u	k7c2	u
malého	malý	k2eAgMnSc2d1	malý
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
propukly	propuknout	k5eAaPmAgInP	propuknout
příznaky	příznak	k1gInPc4	příznak
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
kapitulní	kapitulní	k2eAgMnSc1d1	kapitulní
děkan	děkan	k1gMnSc1	děkan
hrabě	hrabě	k1gMnSc1	hrabě
Leopold	Leopold	k1gMnSc1	Leopold
Antonín	Antonín	k1gMnSc1	Antonín
Podstatský	Podstatský	k2eAgMnSc1d1	Podstatský
Leopoldu	Leopold	k1gMnSc3	Leopold
Mozartovi	Mozartův	k2eAgMnPc1d1	Mozartův
svého	svůj	k3xOyFgMnSc4	svůj
lékaře	lékař	k1gMnSc4	lékař
a	a	k8xC	a
ubytování	ubytování	k1gNnSc4	ubytování
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jim	on	k3xPp3gMnPc3	on
vyčlenil	vyčlenit	k5eAaPmAgInS	vyčlenit
dva	dva	k4xCgInPc4	dva
pokoje	pokoj	k1gInPc4	pokoj
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rezidenci	rezidence	k1gFnSc6	rezidence
v	v	k7c6	v
kapitulním	kapitulní	k2eAgNnSc6d1	kapitulní
děkanství	děkanství	k1gNnSc6	děkanství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kapitulním	kapitulní	k2eAgNnSc6d1	kapitulní
děkanství	děkanství	k1gNnSc6	děkanství
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
Mozartových	Mozartová	k1gFnPc2	Mozartová
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
přátelský	přátelský	k2eAgInSc1d1	přátelský
okruh	okruh	k1gInSc1	okruh
zejména	zejména	k9	zejména
církevních	církevní	k2eAgMnPc2d1	církevní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
<g/>
:	:	kIx,	:
rodinu	rodina	k1gFnSc4	rodina
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
například	například	k6eAd1	například
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
biskupský	biskupský	k2eAgMnSc1d1	biskupský
ceremoniář	ceremoniář	k1gMnSc1	ceremoniář
Jan	Jan	k1gMnSc1	Jan
Leopold	Leopold	k1gMnSc1	Leopold
Hay	Hay	k1gMnSc1	Hay
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
kanovník	kanovník	k1gMnSc1	kanovník
Matyáš	Matyáš	k1gMnSc1	Matyáš
František	František	k1gMnSc1	František
Chorinský	Chorinský	k2eAgMnSc1d1	Chorinský
a	a	k8xC	a
kroměřížský	kroměřížský	k2eAgMnSc1d1	kroměřížský
probošt	probošt	k1gMnSc1	probošt
Antonín	Antonín	k1gMnSc1	Antonín
Theodor	Theodor	k1gMnSc1	Theodor
Colloredo-Waldsee	Colloredo-Waldsee	k1gFnSc1	Colloredo-Waldsee
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
pozdější	pozdní	k2eAgMnPc1d2	pozdější
biskupové	biskup	k1gMnPc1	biskup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kanovníci	kanovník	k1gMnPc1	kanovník
Jan	Jan	k1gMnSc1	Jan
Leopold	Leopold	k1gMnSc1	Leopold
Post	posta	k1gFnPc2	posta
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Baptist	Baptist	k1gMnSc1	Baptist
Grimm	Grimm	k1gMnSc1	Grimm
nebo	nebo	k8xC	nebo
student	student	k1gMnSc1	student
teologie	teologie	k1gFnSc2	teologie
Josef	Josef	k1gMnSc1	Josef
Tonsern	Tonsern	k1gMnSc1	Tonsern
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
se	se	k3xPyFc4	se
v	v	k7c6	v
příjemném	příjemné	k1gNnSc6	příjemné
prostředí	prostředí	k1gNnSc2	prostředí
paláce	palác	k1gInSc2	palác
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
natolik	natolik	k6eAd1	natolik
zotavil	zotavit	k5eAaPmAgMnS	zotavit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
opět	opět	k6eAd1	opět
komponovat	komponovat	k5eAaImF	komponovat
(	(	kIx(	(
<g/>
složil	složit	k5eAaPmAgMnS	složit
zde	zde	k6eAd1	zde
mj.	mj.	kA	mj.
6	[number]	k4	6
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc6	symfonie
F	F	kA	F
dur	dur	k1gNnSc2	dur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odjeli	odjet	k5eAaPmAgMnP	odjet
Mozartovi	Mozart	k1gMnSc3	Mozart
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1767	[number]	k4	1767
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
ubytováni	ubytován	k2eAgMnPc1d1	ubytován
ve	v	k7c6	v
Schrattenbachově	Schrattenbachův	k2eAgInSc6d1	Schrattenbachův
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Taverně	taverna	k1gFnSc6	taverna
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Redutě	reduta	k1gFnSc6	reduta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
odjela	odjet	k5eAaPmAgFnS	odjet
rodina	rodina	k1gFnSc1	rodina
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1768	[number]	k4	1768
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
skladbu	skladba	k1gFnSc4	skladba
složil	složit	k5eAaPmAgMnS	složit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
5	[number]	k4	5
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
strávil	strávit	k5eAaPmAgMnS	strávit
Mozart	Mozart	k1gMnSc1	Mozart
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
arcibiskupské	arcibiskupský	k2eAgFnSc2d1	arcibiskupská
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Projel	projet	k5eAaPmAgMnS	projet
několik	několik	k4yIc4	několik
německých	německý	k2eAgFnPc2d1	německá
metropolí	metropol	k1gFnPc2	metropol
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Mannheimu	Mannheim	k1gInSc6	Mannheim
<g/>
,	,	kIx,	,
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
hudebním	hudební	k2eAgNnSc6d1	hudební
centru	centrum	k1gNnSc6	centrum
<g/>
,	,	kIx,	,
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
Maria	Maria	k1gFnSc1	Maria
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
nakazila	nakazit	k5eAaPmAgFnS	nakazit
tyfoidní	tyfoidní	k2eAgFnSc7d1	tyfoidní
horečkou	horečka	k1gFnSc7	horečka
a	a	k8xC	a
za	za	k7c4	za
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Salzburgu	Salzburg	k1gInSc2	Salzburg
se	se	k3xPyFc4	se
Mozart	Mozart	k1gMnSc1	Mozart
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1779	[number]	k4	1779
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
varhaníka	varhaník	k1gMnSc2	varhaník
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
kromě	kromě	k7c2	kromě
obvyklých	obvyklý	k2eAgFnPc2d1	obvyklá
skladeb	skladba	k1gFnPc2	skladba
souvisejících	související	k2eAgFnPc2d1	související
se	s	k7c7	s
službou	služba	k1gFnSc7	služba
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
také	také	k9	také
několik	několik	k4yIc1	několik
oper	opera	k1gFnPc2	opera
pro	pro	k7c4	pro
kočovné	kočovný	k2eAgFnPc4d1	kočovná
divadelní	divadelní	k2eAgFnPc4d1	divadelní
společnosti	společnost	k1gFnPc4	společnost
a	a	k8xC	a
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
život	život	k1gInSc4	život
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
špatně	špatně	k6eAd1	špatně
snášel	snášet	k5eAaImAgMnS	snášet
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poddaný	poddaný	k1gMnSc1	poddaný
suverénního	suverénní	k2eAgMnSc2d1	suverénní
vládce	vládce	k1gMnSc2	vládce
Salzburského	salzburský	k2eAgNnSc2d1	Salzburské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
(	(	kIx(	(
<g/>
Fürsterzbistum	Fürsterzbistum	k1gNnSc1	Fürsterzbistum
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
propuštění	propuštění	k1gNnSc4	propuštění
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
žádosti	žádost	k1gFnSc3	žádost
však	však	k9	však
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
von	von	k1gInSc4	von
Colloredo	Colloredo	k1gNnSc4	Colloredo
nevyhověl	vyhovět	k5eNaPmAgInS	vyhovět
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
Mozart	Mozart	k1gMnSc1	Mozart
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
ilegálně	ilegálně	k6eAd1	ilegálně
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
Vídně	Vídeň	k1gFnSc2	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
opustil	opustit	k5eAaPmAgMnS	opustit
Mozart	Mozart	k1gMnSc1	Mozart
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
vůli	vůle	k1gFnSc6	vůle
pozici	pozice	k1gFnSc4	pozice
dvorního	dvorní	k2eAgMnSc2d1	dvorní
skladatele	skladatel	k1gMnSc2	skladatel
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
téměř	téměř	k6eAd1	téměř
definitivní	definitivní	k2eAgFnSc3d1	definitivní
roztržce	roztržka	k1gFnSc3	roztržka
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
Leopoldem	Leopold	k1gMnSc7	Leopold
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
obvykle	obvykle	k6eAd1	obvykle
pracoval	pracovat	k5eAaImAgMnS	pracovat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
skladby	skladba	k1gFnPc4	skladba
dokončit	dokončit	k5eAaPmF	dokončit
závratnou	závratný	k2eAgFnSc7d1	závratná
rychlostí	rychlost	k1gFnSc7	rychlost
s	s	k7c7	s
blížící	blížící	k2eAgFnSc7d1	blížící
se	se	k3xPyFc4	se
dobou	doba	k1gFnSc7	doba
termínu	termín	k1gInSc2	termín
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
roků	rok	k1gInPc2	rok
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
složil	složit	k5eAaPmAgMnS	složit
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
symfonií	symfonie	k1gFnPc2	symfonie
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc1	koncert
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
sólové	sólový	k2eAgInPc4d1	sólový
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
především	především	k9	především
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
housle	housle	k1gFnPc4	housle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mše	mše	k1gFnSc2	mše
a	a	k8xC	a
několik	několik	k4yIc4	několik
oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
Mozart	Mozart	k1gMnSc1	Mozart
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
pokrokářských	pokrokářský	k2eAgInPc2d1	pokrokářský
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
původní	původní	k2eAgFnSc2d1	původní
německé	německý	k2eAgFnSc2d1	německá
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zpěvu	zpěv	k1gInSc2	zpěv
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
opera	opera	k1gFnSc1	opera
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
italské	italský	k2eAgNnSc4d1	italské
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
považované	považovaný	k2eAgNnSc1d1	považované
za	za	k7c4	za
elitářské	elitářský	k2eAgNnSc4d1	elitářské
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
Zaide	Zaid	k1gInSc5	Zaid
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
oceňovaná	oceňovaný	k2eAgFnSc1d1	oceňovaná
opera	opera	k1gFnSc1	opera
Únos	únos	k1gInSc4	únos
ze	z	k7c2	z
serailu	serail	k1gInSc2	serail
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Entführung	Entführung	k1gMnSc1	Entführung
aus	aus	k?	aus
dem	dem	k?	dem
Serail	serail	k1gInSc1	serail
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1782	[number]	k4	1782
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
u	u	k7c2	u
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
Mozart	Mozart	k1gMnSc1	Mozart
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
své	svůj	k3xOyFgFnSc2	svůj
bývalé	bývalý	k2eAgFnSc2d1	bývalá
bytné	bytný	k2eAgFnSc2d1	bytná
Constanze	Constanze	k1gFnSc2	Constanze
(	(	kIx(	(
<g/>
Konstancií	Konstancie	k1gFnPc2	Konstancie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc4d1	rozená
Weberovou	Weberová	k1gFnSc4	Weberová
<g/>
.	.	kIx.	.
</s>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
otce	otec	k1gMnSc2	otec
Leopolda	Leopold	k1gMnSc2	Leopold
a	a	k8xC	a
sestry	sestra	k1gFnPc1	sestra
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Dovršila	dovršit	k5eAaPmAgFnS	dovršit
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
roztržka	roztržka	k1gFnSc1	roztržka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
při	při	k7c6	při
Mozartově	Mozartův	k2eAgInSc6d1	Mozartův
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Colloredem	Collored	k1gMnSc7	Collored
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Leopoldu	Leopold	k1gMnSc3	Leopold
Mozartovi	Mozart	k1gMnSc3	Mozart
se	se	k3xPyFc4	se
také	také	k9	také
naprosto	naprosto	k6eAd1	naprosto
nezamlouval	zamlouvat	k5eNaImAgInS	zamlouvat
synův	synův	k2eAgInSc1d1	synův
rozmařilý	rozmařilý	k2eAgInSc1d1	rozmařilý
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc4	neschopnost
dobře	dobře	k6eAd1	dobře
hospodařit	hospodařit	k5eAaImF	hospodařit
s	s	k7c7	s
často	často	k6eAd1	často
značnými	značný	k2eAgInPc7d1	značný
příjmy	příjem	k1gInPc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
let	léto	k1gNnPc2	léto
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Mozart	Mozart	k1gMnSc1	Mozart
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vrcholu	vrchol	k1gInSc2	vrchol
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
příjmech	příjem	k1gInPc6	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porodu	porod	k1gInSc6	porod
prvního	první	k4xOgNnSc2	první
dítěte	dítě	k1gNnSc2	dítě
Raimunda	Raimunda	k1gFnSc1	Raimunda
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
,	,	kIx,	,
odjeli	odjet	k5eAaPmAgMnP	odjet
Mozart	Mozart	k1gMnSc1	Mozart
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
do	do	k7c2	do
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
představit	představit	k5eAaPmF	představit
Constanzi	Constanze	k1gFnSc4	Constanze
otci	otec	k1gMnSc3	otec
a	a	k8xC	a
sestře	sestra	k1gFnSc3	sestra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
druhého	druhý	k4xOgMnSc2	druhý
syna	syn	k1gMnSc2	syn
Carla	Carlo	k1gNnSc2	Carlo
Thomase	Thomas	k1gMnSc2	Thomas
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1784	[number]	k4	1784
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
poměrně	poměrně	k6eAd1	poměrně
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
porodila	porodit	k5eAaPmAgFnS	porodit
Konstance	Konstance	k1gFnSc1	Konstance
dalšího	další	k2eAgMnSc2d1	další
syna	syn	k1gMnSc2	syn
Johanna	Johann	k1gMnSc4	Johann
Thomase	Thomas	k1gMnSc4	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Theresia	Theresia	k1gFnSc1	Theresia
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jako	jako	k9	jako
půlletá	půlletý	k2eAgFnSc1d1	půlletá
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Maria	Maria	k1gFnSc1	Maria
ještě	ještě	k6eAd1	ještě
v	v	k7c4	v
den	den	k1gInSc4	den
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
šesté	šestý	k4xOgNnSc1	šestý
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Franz	Franz	k1gMnSc1	Franz
Xaver	Xaver	k1gMnSc1	Xaver
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
Mozartovou	Mozartův	k2eAgFnSc7d1	Mozartova
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
dospělosti	dospělost	k1gFnSc2	dospělost
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
však	však	k9	však
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Carl	Carl	k1gMnSc1	Carl
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
Mozart	Mozart	k1gMnSc1	Mozart
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
zednářské	zednářský	k2eAgFnSc2d1	zednářská
lóže	lóže	k1gFnSc2	lóže
"	"	kIx"	"
<g/>
Dobročinnost	dobročinnost	k1gFnSc1	dobročinnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Zur	Zur	k1gMnSc1	Zur
Wohltätigkeit	Wohltätigkeit	k1gMnSc1	Wohltätigkeit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1786	[number]	k4	1786
a	a	k8xC	a
1791	[number]	k4	1791
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
Mozartovy	Mozartův	k2eAgInPc1d1	Mozartův
čtyři	čtyři	k4xCgFnPc1	čtyři
nejslavnější	slavný	k2eAgFnPc1d3	nejslavnější
opery	opera	k1gFnPc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
)	)	kIx)	)
a	a	k8xC	a
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
Don	Don	k1gMnSc1	Don
Giovanni	Giovann	k1gMnPc1	Giovann
<g/>
.	.	kIx.	.
</s>
<s>
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
je	být	k5eAaImIp3nS	být
zhudebněním	zhudebnění	k1gNnSc7	zhudebnění
politicky	politicky	k6eAd1	politicky
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
komedie	komedie	k1gFnSc2	komedie
francouzského	francouzský	k2eAgMnSc2d1	francouzský
dramatika	dramatik	k1gMnSc2	dramatik
Beaumarchaise	Beaumarchaise	k1gFnSc2	Beaumarchaise
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímuž	jejíž	k3xOyRp3gInSc3	jejíž
prvnímu	první	k4xOgInSc3	první
uvedení	uvedení	k1gNnSc6	uvedení
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
dal	dát	k5eAaPmAgMnS	dát
svolení	svolení	k1gNnSc6	svolení
sám	sám	k3xTgMnSc1	sám
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
libret	libreto	k1gNnPc2	libreto
obou	dva	k4xCgFnPc6	dva
slavných	slavný	k2eAgFnPc2d1	slavná
oper	opera	k1gFnPc2	opera
byl	být	k5eAaImAgMnS	být
Ital	Ital	k1gMnSc1	Ital
Lorenzo	Lorenza	k1gFnSc5	Lorenza
da	da	k?	da
Ponte	Pont	k1gInSc5	Pont
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Operu	oprat	k5eAaPmIp1nS	oprat
Don	Don	k1gMnSc1	Don
Giovanni	Giovann	k1gMnPc1	Giovann
Mozart	Mozart	k1gMnSc1	Mozart
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dokončil	dokončit	k5eAaPmAgMnS	dokončit
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zde	zde	k6eAd1	zde
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1787	[number]	k4	1787
prvního	první	k4xOgNnSc2	první
provedení	provedení	k1gNnSc2	provedení
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
byl	být	k5eAaImAgMnS	být
Mozart	Mozart	k1gMnSc1	Mozart
osobně	osobně	k6eAd1	osobně
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
<s>
Mozartův	Mozartův	k2eAgInSc1d1	Mozartův
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
triumf	triumf	k1gInSc1	triumf
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pomohl	pomoct	k5eAaPmAgInS	pomoct
ke	k	k7c3	k
zmírnění	zmírnění	k1gNnSc3	zmírnění
jeho	jeho	k3xOp3gInPc2	jeho
finančních	finanční	k2eAgInPc2d1	finanční
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Mozart	Mozart	k1gMnSc1	Mozart
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
manžele	manžel	k1gMnPc4	manžel
Duškovy	Duškův	k2eAgMnPc4d1	Duškův
<g/>
,	,	kIx,	,
skladatele	skladatel	k1gMnSc2	skladatel
Františka	František	k1gMnSc2	František
Xavera	Xaver	k1gMnSc2	Xaver
Duška	Dušek	k1gMnSc2	Dušek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
pěvkyně	pěvkyně	k1gFnSc2	pěvkyně
Josefíny	Josefína	k1gFnSc2	Josefína
Duškové	Dušková	k1gFnSc2	Dušková
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
bydlištěm	bydliště	k1gNnSc7	bydliště
byla	být	k5eAaImAgFnS	být
Bertramka	Bertramka	k1gFnSc1	Bertramka
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nepatřil	patřit	k5eNaImAgMnS	patřit
mezi	mezi	k7c4	mezi
pražská	pražský	k2eAgNnPc4d1	Pražské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Duškovi	Duškův	k2eAgMnPc1d1	Duškův
zůstali	zůstat	k5eAaPmAgMnP	zůstat
spřáteleni	spřátelen	k2eAgMnPc1d1	spřátelen
s	s	k7c7	s
Constanzí	Constanze	k1gFnSc7	Constanze
Mozartovou	Mozartová	k1gFnSc7	Mozartová
i	i	k8xC	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jejího	její	k3xOp3gMnSc2	její
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Mozart	Mozart	k1gMnSc1	Mozart
přijat	přijmout	k5eAaPmNgMnS	přijmout
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
příjmy	příjem	k1gInPc4	příjem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
nákladný	nákladný	k2eAgInSc4d1	nákladný
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
nadále	nadále	k6eAd1	nadále
potýkal	potýkat	k5eAaImAgInS	potýkat
s	s	k7c7	s
finančními	finanční	k2eAgFnPc7d1	finanční
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
musel	muset	k5eAaImAgMnS	muset
překlenovat	překlenovat	k5eAaImF	překlenovat
půjčkami	půjčka	k1gFnPc7	půjčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
podnikl	podniknout	k5eAaPmAgMnS	podniknout
tříměsíční	tříměsíční	k2eAgNnSc4d1	tříměsíční
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
příliš	příliš	k6eAd1	příliš
výnosné	výnosný	k2eAgNnSc1d1	výnosné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
zreorganizoval	zreorganizovat	k5eAaPmAgMnS	zreorganizovat
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
nástupce	nástupce	k1gMnSc1	nástupce
Leopold	Leopold	k1gMnSc1	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
dvorní	dvorní	k2eAgInSc1d1	dvorní
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
Mozarta	Mozart	k1gMnSc4	Mozart
finančně	finančně	k6eAd1	finančně
příliš	příliš	k6eAd1	příliš
nedotklo	dotknout	k5eNaPmAgNnS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgNnSc1d1	náročné
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
uhrazení	uhrazení	k1gNnSc1	uhrazení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
manželky	manželka	k1gFnSc2	manželka
Constanze	Constanze	k1gFnSc2	Constanze
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
Baden	Baden	k1gInSc1	Baden
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
prodělané	prodělaný	k2eAgInPc4d1	prodělaný
porody	porod	k1gInPc4	porod
a	a	k8xC	a
nervové	nervový	k2eAgInPc4d1	nervový
otřesy	otřes	k1gInPc4	otřes
způsobené	způsobený	k2eAgFnSc2d1	způsobená
ztrátou	ztráta	k1gFnSc7	ztráta
čtyř	čtyři	k4xCgInPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
jí	on	k3xPp3gFnSc2	on
značně	značně	k6eAd1	značně
podlomily	podlomit	k5eAaPmAgFnP	podlomit
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1791	[number]	k4	1791
měla	mít	k5eAaImAgFnS	mít
opět	opět	k6eAd1	opět
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
premiéru	premiéra	k1gFnSc4	premiéra
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
další	další	k2eAgFnSc1d1	další
opera	opera	k1gFnSc1	opera
La	la	k1gNnSc2	la
clemenza	clemenza	k1gFnSc1	clemenza
di	di	k?	di
Tito	tento	k3xDgMnPc1	tento
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
pro	pro	k7c4	pro
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
korunovaci	korunovace	k1gFnSc4	korunovace
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
Mozart	Mozart	k1gMnSc1	Mozart
získal	získat	k5eAaPmAgMnS	získat
objednávku	objednávka	k1gFnSc4	objednávka
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
italskou	italský	k2eAgFnSc4d1	italská
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
libreto	libreto	k1gNnSc4	libreto
původně	původně	k6eAd1	původně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Antonio	Antonio	k1gMnSc1	Antonio
Salieri	Salier	k1gFnSc2	Salier
<g/>
,	,	kIx,	,
Mozartův	Mozartův	k2eAgMnSc1d1	Mozartův
konkurent	konkurent	k1gMnSc1	konkurent
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
Così	Così	k1gFnPc2	Così
fan	fana	k1gFnPc2	fana
tutte	tutte	k5eAaPmIp2nP	tutte
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Takové	takový	k3xDgFnPc1	takový
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
romantické	romantický	k2eAgFnSc2d1	romantická
fabulace	fabulace	k1gFnSc2	fabulace
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgInS	být
Salieri	Salieri	k1gNnSc4	Salieri
Mozartovým	Mozartův	k2eAgMnSc7d1	Mozartův
protivníkem	protivník	k1gMnSc7	protivník
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
žárlil	žárlit	k5eAaImAgMnS	žárlit
<g/>
,	,	kIx,	,
záviděl	závidět	k5eAaImAgMnS	závidět
mu	on	k3xPp3gMnSc3	on
jeho	on	k3xPp3gInSc4	on
genialitu	genialita	k1gFnSc4	genialita
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
dokonce	dokonce	k9	dokonce
zavinit	zavinit	k5eAaPmF	zavinit
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
pravda	pravda	k1gFnSc1	pravda
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
Mozart	Mozart	k1gMnSc1	Mozart
bezesporu	bezesporu	k9	bezesporu
mnohem	mnohem	k6eAd1	mnohem
nadanější	nadaný	k2eAgMnSc1d2	nadanější
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Salieri	Saliere	k1gFnSc4	Saliere
větší	veliký	k2eAgInSc4d2	veliký
úspěch	úspěch	k1gInSc4	úspěch
u	u	k7c2	u
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
také	také	k9	také
velmi	velmi	k6eAd1	velmi
prestižní	prestižní	k2eAgFnSc4d1	prestižní
a	a	k8xC	a
výnosnou	výnosný	k2eAgFnSc4d1	výnosná
funkci	funkce	k1gFnSc4	funkce
ředitele	ředitel	k1gMnSc2	ředitel
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
dvorního	dvorní	k2eAgMnSc2d1	dvorní
kapelníka	kapelník	k1gMnSc2	kapelník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jako	jako	k8xC	jako
Ital	Ital	k1gMnSc1	Ital
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgInS	těšit
přízni	přízeň	k1gFnSc3	přízeň
dobové	dobový	k2eAgFnSc2d1	dobová
módy	móda	k1gFnSc2	móda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
upřednostňovala	upřednostňovat	k5eAaImAgFnS	upřednostňovat
Italy	Ital	k1gMnPc4	Ital
a	a	k8xC	a
italský	italský	k2eAgInSc1d1	italský
styl	styl	k1gInSc1	styl
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
takové	takový	k3xDgFnPc4	takový
situace	situace	k1gFnPc4	situace
mohl	moct	k5eAaImAgMnS	moct
spíš	spíš	k9	spíš
Mozart	Mozart	k1gMnSc1	Mozart
žárlit	žárlit	k5eAaImF	žárlit
na	na	k7c6	na
svého	svůj	k3xOyFgMnSc2	svůj
protežovaného	protežovaný	k2eAgMnSc2d1	protežovaný
italského	italský	k2eAgMnSc2d1	italský
soka	sok	k1gMnSc2	sok
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
když	když	k8xS	když
Antonio	Antonio	k1gMnSc1	Antonio
Salieri	Saliere	k1gFnSc4	Saliere
často	často	k6eAd1	často
dostával	dostávat	k5eAaImAgMnS	dostávat
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
Mozartem	Mozart	k1gMnSc7	Mozart
při	při	k7c6	při
objednávkách	objednávka	k1gFnPc6	objednávka
nové	nový	k2eAgFnSc2d1	nová
opery	opera	k1gFnSc2	opera
pro	pro	k7c4	pro
císařský	císařský	k2eAgInSc4d1	císařský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
nebylo	být	k5eNaImAgNnS	být
nijak	nijak	k6eAd1	nijak
silné	silný	k2eAgNnSc1d1	silné
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
uvedl	uvést	k5eAaPmAgMnS	uvést
Mozart	Mozart	k1gMnSc1	Mozart
operu	opera	k1gFnSc4	opera
Così	Così	k1gFnSc4	Così
fan	fana	k1gFnPc2	fana
tutte	tutte	k5eAaPmIp2nP	tutte
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
další	další	k2eAgNnSc4d1	další
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
složil	složit	k5eAaPmAgMnS	složit
několik	několik	k4yIc4	několik
menších	malý	k2eAgNnPc2d2	menší
komorních	komorní	k2eAgNnPc2d1	komorní
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
také	také	k9	také
zpěvohru	zpěvohra	k1gFnSc4	zpěvohra
(	(	kIx(	(
<g/>
či	či	k8xC	či
operu	opera	k1gFnSc4	opera
<g/>
)	)	kIx)	)
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
)	)	kIx)	)
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Emanuela	Emanuel	k1gMnSc2	Emanuel
Schikanedera	Schikaneder	k1gMnSc2	Schikaneder
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
okamžitě	okamžitě	k6eAd1	okamžitě
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgNnSc1d1	populární
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
provedeno	provést	k5eAaPmNgNnS	provést
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
předměstském	předměstský	k2eAgNnSc6d1	předměstské
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vídeňce	Vídeňka	k1gFnSc6	Vídeňka
(	(	kIx(	(
<g/>
Theater	Theater	k1gInSc1	Theater
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Wien	Wien	k1gNnSc4	Wien
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
ředitelem	ředitel	k1gMnSc7	ředitel
byl	být	k5eAaImAgMnS	být
Schikaneder	Schikaneder	k1gMnSc1	Schikaneder
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
pouhá	pouhý	k2eAgFnSc1d1	pouhá
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
zednářských	zednářský	k2eAgFnPc2d1	zednářská
a	a	k8xC	a
společensky	společensky	k6eAd1	společensky
radikálních	radikální	k2eAgInPc2d1	radikální
motivů	motiv	k1gInPc2	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
započal	započnout	k5eAaPmAgMnS	započnout
Mozart	Mozart	k1gMnSc1	Mozart
kompozici	kompozice	k1gFnSc4	kompozice
slavného	slavný	k2eAgMnSc2d1	slavný
Requiem	Requium	k1gNnSc7	Requium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
však	však	k9	však
napsal	napsat	k5eAaPmAgMnS	napsat
sám	sám	k3xTgMnSc1	sám
jen	jen	k6eAd1	jen
asi	asi	k9	asi
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
oslovila	oslovit	k5eAaPmAgFnS	oslovit
vdova	vdova	k1gFnSc1	vdova
Constanze	Constanze	k1gFnSc1	Constanze
Mozartová	Mozartová	k1gFnSc1	Mozartová
postupně	postupně	k6eAd1	postupně
několik	několik	k4yIc1	několik
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
však	však	k9	však
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
díla	dílo	k1gNnSc2	dílo
minimálně	minimálně	k6eAd1	minimálně
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
Requiem	Requium	k1gNnSc7	Requium
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
skladatele	skladatel	k1gMnSc2	skladatel
Franze	Franze	k1gFnSc2	Franze
Xavera	Xaver	k1gMnSc2	Xaver
Süssmayra	Süssmayr	k1gMnSc2	Süssmayr
<g/>
.	.	kIx.	.
</s>
<s>
Süssmayr	Süssmayr	k1gMnSc1	Süssmayr
použil	použít	k5eAaPmAgMnS	použít
všechny	všechen	k3xTgFnPc4	všechen
Mozartovy	Mozartův	k2eAgFnPc4d1	Mozartova
originální	originální	k2eAgFnPc4d1	originální
partitury	partitura	k1gFnPc4	partitura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
nový	nový	k2eAgInSc4d1	nový
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
části	část	k1gFnPc4	část
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
zbytek	zbytek	k1gInSc1	zbytek
"	"	kIx"	"
<g/>
Lacrimosa	Lacrimosa	k1gFnSc1	Lacrimosa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
znovu	znovu	k6eAd1	znovu
použil	použít	k5eAaPmAgMnS	použít
Mozartovy	Mozartův	k2eAgInPc4d1	Mozartův
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
"	"	kIx"	"
<g/>
Introitus	introitus	k1gInSc1	introitus
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Kyrie	Kyrie	k1gNnSc1	Kyrie
<g/>
"	"	kIx"	"
a	a	k8xC	a
Requiem	Requium	k1gNnSc7	Requium
dovedl	dovést	k5eAaPmAgMnS	dovést
do	do	k7c2	do
finálního	finální	k2eAgNnSc2d1	finální
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známého	známý	k2eAgNnSc2d1	známé
pojetí	pojetí	k1gNnSc2	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
práce	práce	k1gFnSc1	práce
mnoha	mnoho	k4c2	mnoho
odborníky	odborník	k1gMnPc7	odborník
kritizována	kritizován	k2eAgFnSc1d1	kritizována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
předložil	předložit	k5eAaPmAgMnS	předložit
hudebník	hudebník	k1gMnSc1	hudebník
Franz	Franz	k1gMnSc1	Franz
Beyer	Beyer	k1gMnSc1	Beyer
kompletně	kompletně	k6eAd1	kompletně
přepracovanou	přepracovaný	k2eAgFnSc4d1	přepracovaná
verzi	verze	k1gFnSc4	verze
Rekviem	rekviem	k1gNnSc2	rekviem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
upravil	upravit	k5eAaPmAgMnS	upravit
Süssmayrovy	Süssmayrův	k2eAgInPc4d1	Süssmayrův
nástrojové	nástrojový	k2eAgInPc4d1	nástrojový
hlasy	hlas	k1gInPc4	hlas
(	(	kIx(	(
<g/>
instrumentaci	instrumentace	k1gFnSc4	instrumentace
<g/>
)	)	kIx)	)
a	a	k8xC	a
opravil	opravit	k5eAaPmAgMnS	opravit
některá	některý	k3yIgNnPc4	některý
problematická	problematický	k2eAgNnPc4d1	problematické
místa	místo	k1gNnPc4	místo
Süssmayrova	Süssmayrův	k2eAgNnPc4d1	Süssmayrův
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
části	část	k1gFnSc6	část
"	"	kIx"	"
<g/>
Lacrimosa	Lacrimosa	k1gFnSc1	Lacrimosa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
verze	verze	k1gFnPc1	verze
celého	celý	k2eAgNnSc2d1	celé
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
mnoha	mnoho	k4c7	mnoho
znalci	znalec	k1gMnPc7	znalec
jako	jako	k8xC	jako
kongeniální	kongeniální	k2eAgInPc4d1	kongeniální
a	a	k8xC	a
uvedena	uvést	k5eAaPmNgFnS	uvést
a	a	k8xC	a
nahrána	nahrát	k5eAaPmNgFnS	nahrát
význačnými	význačný	k2eAgMnPc7d1	význačný
dirigenty	dirigent	k1gMnPc7	dirigent
Leonardem	Leonardo	k1gMnSc7	Leonardo
Bernsteinem	Bernstein	k1gMnSc7	Bernstein
<g/>
,	,	kIx,	,
Nikolausem	Nikolaus	k1gMnSc7	Nikolaus
Harnoncourtem	Harnoncourt	k1gMnSc7	Harnoncourt
a	a	k8xC	a
Nevillem	Nevill	k1gMnSc7	Nevill
Marrinerem	Marriner	k1gMnSc7	Marriner
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
složil	složit	k5eAaPmAgMnS	složit
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
krátkého	krátký	k2eAgInSc2d1	krátký
života	život	k1gInSc2	život
neuvěřitelných	uvěřitelný	k2eNgNnPc2d1	neuvěřitelné
626	[number]	k4	626
děl	dělo	k1gNnPc2	dělo
<g/>
:	:	kIx,	:
symfonie	symfonie	k1gFnPc1	symfonie
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnPc1d1	komorní
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
opery	opera	k1gFnPc1	opera
a	a	k8xC	a
sborové	sborový	k2eAgFnPc1d1	sborová
skladby	skladba	k1gFnPc1	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
klasických	klasický	k2eAgMnPc2d1	klasický
skladatelů	skladatel	k1gMnPc2	skladatel
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
hudebního	hudební	k2eAgNnSc2d1	hudební
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
.	.	kIx.	.
</s>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
skládal	skládat	k5eAaImAgMnS	skládat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
rané	raný	k2eAgFnPc4d1	raná
práce	práce	k1gFnPc4	práce
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
Mozarta	Mozart	k1gMnSc2	Mozart
a	a	k8xC	a
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
potomci	potomek	k1gMnPc1	potomek
neuvidí	uvidět	k5eNaPmIp3nP	uvidět
takový	takový	k3xDgInSc4	takový
talent	talent	k1gInSc4	talent
znovu	znovu	k6eAd1	znovu
za	za	k7c4	za
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
se	se	k3xPyFc4	se
Mozart	Mozart	k1gMnSc1	Mozart
nakazil	nakazit	k5eAaPmAgMnS	nakazit
infekční	infekční	k2eAgFnSc7d1	infekční
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1791	[number]	k4	1791
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
poslední	poslední	k2eAgNnSc1d1	poslední
dílo	dílo	k1gNnSc1	dílo
Requiem	Requium	k1gNnSc7	Requium
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgMnS	moct
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
učinil	učinit	k5eAaPmAgMnS	učinit
až	až	k9	až
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Xaver	Xaver	k1gMnSc1	Xaver
Süssmayr	Süssmayr	k1gMnSc1	Süssmayr
<g/>
,	,	kIx,	,
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
ženy	žena	k1gFnSc2	žena
Constanze	Constanze	k1gFnSc2	Constanze
<g/>
.	.	kIx.	.
</s>
<s>
Brzo	brzo	k6eAd1	brzo
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
byl	být	k5eAaImAgMnS	být
Mozart	Mozart	k1gMnSc1	Mozart
otráven	otrávit	k5eAaPmNgMnS	otrávit
a	a	k8xC	a
jako	jako	k9	jako
nepohodlný	pohodlný	k2eNgInSc1d1	nepohodlný
odstraněn	odstraněn	k2eAgInSc1d1	odstraněn
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
domnělého	domnělý	k2eAgInSc2d1	domnělý
zločinu	zločin	k1gInSc2	zločin
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
obviňován	obviňován	k2eAgMnSc1d1	obviňován
Antonio	Antonio	k1gMnSc1	Antonio
Salieri	Salier	k1gFnSc2	Salier
<g/>
,	,	kIx,	,
domněnky	domněnka	k1gFnPc1	domněnka
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
a	a	k8xC	a
neexistují	existovat	k5eNaImIp3nP	existovat
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
absolutně	absolutně	k6eAd1	absolutně
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
ani	ani	k8xC	ani
indicie	indicie	k1gFnPc4	indicie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
peněz	peníze	k1gInPc2	peníze
bylo	být	k5eAaImAgNnS	být
Mozartovo	Mozartův	k2eAgNnSc1d1	Mozartovo
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
tělo	tělo	k1gNnSc1	tělo
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
do	do	k7c2	do
"	"	kIx"	"
<g/>
všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
hrobu	hrob	k1gInSc2	hrob
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
do	do	k7c2	do
hromadného	hromadný	k2eAgInSc2d1	hromadný
hrobu	hrob	k1gInSc2	hrob
pro	pro	k7c4	pro
chudé	chudý	k1gMnPc4	chudý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
nedochoval	dochovat	k5eNaPmAgMnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
hřbitově	hřbitov	k1gInSc6	hřbitov
Sankt	Sankt	k1gInSc1	Sankt
Marx	Marx	k1gMnSc1	Marx
(	(	kIx(	(
<g/>
Sankt	Sankt	k1gInSc1	Sankt
Marxer	Marxer	k1gMnSc1	Marxer
Friedhof	Friedhof	k1gMnSc1	Friedhof
<g/>
)	)	kIx)	)
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
3	[number]	k4	3
<g/>
.	.	kIx.	.
městském	městský	k2eAgInSc6d1	městský
okrese	okres	k1gInSc6	okres
Landstrasse	Landstrasse	k1gFnSc2	Landstrasse
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Vídně	Vídeň	k1gFnSc2	Vídeň
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Mozart	Mozart	k1gMnSc1	Mozart
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
však	však	k9	však
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
postaven	postaven	k2eAgInSc1d1	postaven
památník	památník	k1gInSc1	památník
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
okolnosti	okolnost	k1gFnPc1	okolnost
Mozartova	Mozartův	k2eAgInSc2d1	Mozartův
pohřbu	pohřeb	k1gInSc2	pohřeb
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
a	a	k8xC	a
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
mrtvola	mrtvola	k1gFnSc1	mrtvola
zůstala	zůstat	k5eAaPmAgFnS	zůstat
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rakvi	rakev	k1gFnSc6	rakev
přepravena	přepravit	k5eAaPmNgFnS	přepravit
do	do	k7c2	do
postranní	postranní	k2eAgFnSc2d1	postranní
kaple	kaple	k1gFnSc2	kaple
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
katedrály	katedrála	k1gFnSc2	katedrála
Svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1791	[number]	k4	1791
konal	konat	k5eAaImAgInS	konat
smuteční	smuteční	k2eAgInSc1d1	smuteční
obřad	obřad	k1gInSc1	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
pohřební	pohřební	k2eAgInSc1d1	pohřební
průvod	průvod	k1gInSc1	průvod
vydal	vydat	k5eAaPmAgInS	vydat
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
hřbitovu	hřbitov	k1gInSc3	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
hranici	hranice	k1gFnSc4	hranice
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
.	.	kIx.	.
okres	okres	k1gInSc4	okres
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
průvod	průvod	k1gInSc1	průvod
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
zpráv	zpráva	k1gFnPc2	zpráva
jen	jen	k9	jen
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Albrechtsberger	Albrechtsberger	k1gMnSc1	Albrechtsberger
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
rakev	rakev	k1gFnSc4	rakev
doprovodili	doprovodit	k5eAaPmAgMnP	doprovodit
až	až	k9	až
přímo	přímo	k6eAd1	přímo
ke	k	k7c3	k
hřbitovu	hřbitov	k1gInSc3	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
rakev	rakev	k1gFnSc1	rakev
s	s	k7c7	s
mrtvým	mrtvý	k1gMnSc7	mrtvý
Mozartem	Mozart	k1gMnSc7	Mozart
ponechána	ponechán	k2eAgFnSc1d1	ponechána
v	v	k7c6	v
márnici	márnice	k1gFnSc6	márnice
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
beze	beze	k7c2	beze
svědků	svědek	k1gMnPc2	svědek
asi	asi	k9	asi
až	až	k9	až
po	po	k7c6	po
48	[number]	k4	48
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
snad	snad	k9	snad
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
předepsaná	předepsaný	k2eAgFnSc1d1	předepsaná
lhůta	lhůta	k1gFnSc1	lhůta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zamezilo	zamezit	k5eAaPmAgNnS	zamezit
pochování	pochování	k1gNnSc1	pochování
zdánlivě	zdánlivě	k6eAd1	zdánlivě
mrtvých	mrtvý	k1gMnPc2	mrtvý
zaživa	zaživa	k6eAd1	zaživa
<g/>
.	.	kIx.	.
</s>
<s>
Mozartovým	Mozartův	k2eAgMnSc7d1	Mozartův
hrobníkem	hrobník	k1gMnSc7	hrobník
byl	být	k5eAaImAgMnS	být
jistý	jistý	k2eAgMnSc1d1	jistý
Simon	Simon	k1gMnSc1	Simon
Preuschl	Preuschl	k1gMnSc1	Preuschl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rakev	rakev	k1gFnSc4	rakev
uložil	uložit	k5eAaPmAgMnS	uložit
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
nijak	nijak	k6eAd1	nijak
neoznačeného	označený	k2eNgInSc2d1	neoznačený
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
křížem	křížem	k6eAd1	křížem
ani	ani	k8xC	ani
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
vdova	vdova	k1gFnSc1	vdova
Constanze	Constanze	k1gFnSc2	Constanze
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
krátce	krátce	k6eAd1	krátce
bez	bez	k7c2	bez
finanční	finanční	k2eAgFnSc2d1	finanční
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Dluhy	dluh	k1gInPc1	dluh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
po	po	k7c6	po
Mozartovi	Mozart	k1gMnSc6	Mozart
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
,	,	kIx,	,
a	a	k8xC	a
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
své	svůj	k3xOyFgFnSc2	svůj
a	a	k8xC	a
dvou	dva	k4xCgNnPc2	dva
synů	syn	k1gMnPc2	syn
mohla	moct	k5eAaImAgFnS	moct
pak	pak	k6eAd1	pak
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
krýt	krýt	k5eAaImF	krýt
díky	díky	k7c3	díky
penzi	penze	k1gFnSc3	penze
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
jí	on	k3xPp3gFnSc7	on
udělil	udělit	k5eAaPmAgInS	udělit
císař	císař	k1gMnSc1	císař
Leopold	Leopolda	k1gFnPc2	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
velkoryse	velkoryse	k6eAd1	velkoryse
přispěl	přispět	k5eAaPmAgMnS	přispět
na	na	k7c4	na
výtěžek	výtěžek	k1gInSc4	výtěžek
benefičního	benefiční	k2eAgInSc2d1	benefiční
koncertu	koncert	k1gInSc2	koncert
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Mozartových	Mozartových	k2eAgMnPc2d1	Mozartových
pozůstalých	pozůstalý	k1gMnPc2	pozůstalý
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Constanze	Constanze	k1gFnSc1	Constanze
Mozart	Mozart	k1gMnSc1	Mozart
podruhé	podruhé	k6eAd1	podruhé
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
dánského	dánský	k2eAgMnSc2d1	dánský
diplomata	diplomat	k1gMnSc2	diplomat
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Georga	Georg	k1gMnSc2	Georg
Nissena	Nissen	k2eAgFnSc1d1	Nissena
<g/>
.	.	kIx.	.
</s>
<s>
Mozartův	Mozartův	k2eAgInSc4d1	Mozartův
fyzický	fyzický	k2eAgInSc4d1	fyzický
vzhled	vzhled	k1gInSc4	vzhled
popsal	popsat	k5eAaPmAgMnS	popsat
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
tenorista	tenorista	k1gMnSc1	tenorista
Michael	Michael	k1gMnSc1	Michael
Kelly	Kella	k1gFnSc2	Kella
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
byl	být	k5eAaImAgMnS	být
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
malý	malý	k2eAgMnSc1d1	malý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
hubený	hubený	k2eAgMnSc1d1	hubený
a	a	k8xC	a
bledý	bledý	k2eAgMnSc1d1	bledý
<g/>
,	,	kIx,	,
s	s	k7c7	s
až	až	k6eAd1	až
přespříliš	přespříliš	k6eAd1	přespříliš
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
pěkných	pěkný	k2eAgInPc2d1	pěkný
světlých	světlý	k2eAgInPc2d1	světlý
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
Mozartových	Mozartových	k2eAgMnPc2d1	Mozartových
životopisců	životopisec	k1gMnPc2	životopisec
Niemetschek	Niemetschek	k6eAd1	Niemetschek
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebylo	být	k5eNaImAgNnS	být
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
fyzickém	fyzický	k2eAgInSc6d1	fyzický
vzhledu	vzhled	k1gInSc6	vzhled
nic	nic	k3yNnSc1	nic
zajímavého	zajímavý	k2eAgMnSc4d1	zajímavý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
tvář	tvář	k1gFnSc1	tvář
kromě	kromě	k7c2	kromě
jeho	jeho	k3xOp3gInPc2	jeho
obrovských	obrovský	k2eAgInPc2d1	obrovský
<g/>
,	,	kIx,	,
pronikavých	pronikavý	k2eAgNnPc2d1	pronikavé
očí	oko	k1gNnPc2	oko
neznačila	značit	k5eNaImAgFnS	značit
žádné	žádný	k3yNgFnPc4	žádný
známky	známka	k1gFnPc4	známka
geniality	genialita	k1gFnSc2	genialita
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gFnSc1	jeho
pleť	pleť	k1gFnSc1	pleť
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
byla	být	k5eAaImAgFnS	být
zjizvená	zjizvený	k2eAgFnSc1d1	zjizvená
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
od	od	k7c2	od
neštovic	neštovice	k1gFnPc2	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Miloval	milovat	k5eAaImAgMnS	milovat
elegantní	elegantní	k2eAgNnSc4d1	elegantní
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Kelly	Kell	k1gInPc4	Kell
si	se	k3xPyFc3	se
vybavoval	vybavovat	k5eAaImAgMnS	vybavovat
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
při	při	k7c6	při
zkoušce	zkouška	k1gFnSc6	zkouška
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgInS	být
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
sytě	sytě	k6eAd1	sytě
červeným	červený	k2eAgInSc7d1	červený
frakem	frak	k1gInSc7	frak
a	a	k8xC	a
zlatým	zlatý	k2eAgInSc7d1	zlatý
krajkovaným	krajkovaný	k2eAgInSc7d1	krajkovaný
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
,	,	kIx,	,
udával	udávat	k5eAaImAgMnS	udávat
tón	tón	k1gInSc4	tón
hudby	hudba	k1gFnSc2	hudba
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
hlasu	hlas	k1gInSc6	hlas
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Constanze	Constanze	k1gFnSc1	Constanze
napsala	napsat	k5eAaBmAgFnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgMnS	mít
tenor	tenor	k1gMnSc1	tenor
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
spíše	spíše	k9	spíše
tlumený	tlumený	k2eAgInSc1d1	tlumený
když	když	k8xS	když
hovořil	hovořit	k5eAaImAgInS	hovořit
a	a	k8xC	a
jemný	jemný	k2eAgInSc1d1	jemný
ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
něco	něco	k6eAd1	něco
rozčílilo	rozčílit	k5eAaPmAgNnS	rozčílit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
být	být	k5eAaImF	být
přísný	přísný	k2eAgMnSc1d1	přísný
<g/>
,	,	kIx,	,
zněl	znět	k5eAaImAgInS	znět
energicky	energicky	k6eAd1	energicky
a	a	k8xC	a
silně	silně	k6eAd1	silně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Raimund	Raimunda	k1gFnPc2	Raimunda
Leopold	Leopold	k1gMnSc1	Leopold
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1783	[number]	k4	1783
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
srpna	srpen	k1gInSc2	srpen
1783	[number]	k4	1783
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
Carl	Carl	k1gMnSc1	Carl
Thomas	Thomas	k1gMnSc1	Thomas
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1784	[number]	k4	1784
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
,	,	kIx,	,
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1858	[number]	k4	1858
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
Johann	Johann	k1gMnSc1	Johann
Thomas	Thomas	k1gMnSc1	Thomas
Leopold	Leopold	k1gMnSc1	Leopold
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1786	[number]	k4	1786
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1786	[number]	k4	1786
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Theresia	Theresius	k1gMnSc2	Theresius
Maria	Mario	k1gMnSc2	Mario
Anna	Anna	k1gFnSc1	Anna
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1787	[number]	k4	1787
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1788	[number]	k4	1788
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Maria	Maria	k1gFnSc1	Maria
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1789	[number]	k4	1789
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
†	†	k?	†
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1789	[number]	k4	1789
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
Franz	Franz	k1gMnSc1	Franz
Xaver	Xaver	k1gMnSc1	Xaver
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1791	[number]	k4	1791
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
;	;	kIx,	;
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1844	[number]	k4	1844
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
)	)	kIx)	)
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
skladatelů	skladatel	k1gMnPc2	skladatel
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
je	být	k5eAaImIp3nS	být
katalogizováno	katalogizován	k2eAgNnSc1d1	katalogizováno
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
skladba	skladba	k1gFnSc1	skladba
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc1	označení
katalogu	katalog	k1gInSc2	katalog
KV	KV	kA	KV
a	a	k8xC	a
pořadové	pořadový	k2eAgNnSc1d1	pořadové
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
KV	KV	kA	KV
-	-	kIx~	-
Köchelův	Köchelův	k2eAgInSc1d1	Köchelův
seznam	seznam	k1gInSc1	seznam
(	(	kIx(	(
<g/>
Köchel-Verzeichnis	Köchel-Verzeichnis	k1gInSc1	Köchel-Verzeichnis
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
árií	árie	k1gFnPc2	árie
<g/>
,	,	kIx,	,
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
sborů	sbor	k1gInPc2	sbor
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
církevních	církevní	k2eAgFnPc2d1	církevní
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
mší	mše	k1gFnPc2	mše
a	a	k8xC	a
motet	moteto	k1gNnPc2	moteto
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
duet	duet	k1gInSc1	duet
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
kantát	kantáta	k1gFnPc2	kantáta
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
komorních	komorní	k2eAgFnPc2d1	komorní
skladeb	skladba	k1gFnPc2	skladba
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc1	seznam
koncertů	koncert	k1gInPc2	koncert
W.	W.	kA	W.
A.	A.	kA	A.
<g />
.	.	kIx.	.
</s>
<s>
Mozarta	Mozart	k1gMnSc4	Mozart
Seznam	seznam	k1gInSc1	seznam
kvartet	kvartet	k1gInSc4	kvartet
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
kvintet	kvintet	k1gInSc1	kvintet
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
oper	opera	k1gFnPc2	opera
a	a	k8xC	a
oratorií	oratorium	k1gNnPc2	oratorium
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
orchestrálních	orchestrální	k2eAgNnPc2d1	orchestrální
děl	dělo	k1gNnPc2	dělo
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
sólové	sólový	k2eAgInPc4d1	sólový
nástroje	nástroj	k1gInPc4	nástroj
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Seznam	seznam	k1gInSc4	seznam
trií	trio	k1gNnPc2	trio
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Die	Die	k1gMnSc2	Die
Schuldigkeit	Schuldigkeit	k1gInSc4	Schuldigkeit
des	des	k1gNnSc2	des
Ersten	Ersten	k2eAgInSc4d1	Ersten
Gebots	Gebots	k1gInSc4	Gebots
Apollo	Apollo	k1gMnSc1	Apollo
et	et	k?	et
Hyacinthus	Hyacinthus	k1gMnSc1	Hyacinthus
Bastien	Bastien	k2eAgMnSc1d1	Bastien
und	und	k?	und
Bastienne	Bastienn	k1gInSc5	Bastienn
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Bastien	Bastien	k1gInSc1	Bastien
a	a	k8xC	a
Bastienka	Bastienka	k1gFnSc1	Bastienka
<g/>
)	)	kIx)	)
La	la	k1gNnSc1	la
finta	finta	k1gFnSc1	finta
semplice	semplice	k1gFnSc1	semplice
Mitridate	Mitridat	k1gMnSc5	Mitridat
<g/>
,	,	kIx,	,
re	re	k9	re
di	di	k?	di
Ponto	Ponto	k1gNnSc1	Ponto
Betula	Betul	k1gMnSc2	Betul
liberata	liberata	k1gFnSc1	liberata
Ascanio	Ascanio	k1gMnSc1	Ascanio
in	in	k?	in
Alba	alba	k1gFnSc1	alba
Il	Il	k1gFnSc4	Il
sogno	sogno	k6eAd1	sogno
di	di	k?	di
Scipione	Scipion	k1gInSc5	Scipion
Lucio	Lucio	k1gNnSc1	Lucio
Silla	Silla	k1gMnSc1	Silla
Thamos	Thamos	k1gMnSc1	Thamos
<g/>
,	,	kIx,	,
König	König	k1gMnSc1	König
in	in	k?	in
Ägypten	Ägyptno	k1gNnPc2	Ägyptno
(	(	kIx(	(
<g/>
Thamos	Thamos	k1gMnSc1	Thamos
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
egyptský	egyptský	k2eAgMnSc1d1	egyptský
<g/>
)	)	kIx)	)
La	la	k1gNnSc1	la
finta	finta	k1gFnSc1	finta
giardiniera	giardinier	k1gMnSc2	giardinier
Il	Il	k1gMnSc2	Il
re	re	k9	re
pastore	pastor	k1gMnSc5	pastor
(	(	kIx(	(
<g/>
Král	Král	k1gMnSc1	Král
pastýř	pastýř	k1gMnSc1	pastýř
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Zaide	Zaid	k1gInSc5	Zaid
(	(	kIx(	(
<g/>
Zaida	Zaido	k1gNnSc2	Zaido
<g/>
)	)	kIx)	)
Idomeneo	Idomeneo	k6eAd1	Idomeneo
Únos	únos	k1gInSc1	únos
ze	z	k7c2	z
serailu	serail	k1gInSc2	serail
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Entführung	Entführung	k1gMnSc1	Entführung
aus	aus	k?	aus
dem	dem	k?	dem
Serail	serail	k1gInSc1	serail
<g/>
)	)	kIx)	)
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
oca	oca	k?	oca
del	del	k?	del
Cairo	Cairo	k1gNnSc4	Cairo
Lo	Lo	k1gFnSc2	Lo
sposo	sposa	k1gFnSc5	sposa
deluso	delusa	k1gFnSc5	delusa
Divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Schauspieldirektor	Schauspieldirektor	k1gInSc1	Schauspieldirektor
<g/>
)	)	kIx)	)
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
nozze	nozze	k1gFnSc2	nozze
di	di	k?	di
Figaro	Figara	k1gFnSc5	Figara
<g/>
)	)	kIx)	)
Don	Don	k1gMnSc1	Don
<g />
.	.	kIx.	.
</s>
<s>
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
Così	Così	k1gMnPc1	Così
fan	fana	k1gFnPc2	fana
tutte	tutte	k5eAaPmIp2nP	tutte
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
)	)	kIx)	)
La	la	k1gNnSc1	la
Clemenza	Clemenz	k1gMnSc2	Clemenz
di	di	k?	di
Tito	tento	k3xDgMnPc1	tento
Or	Or	k1gMnSc1	Or
che	che	k0	che
il	il	k?	il
dover	dover	k1gInSc1	dover
<g/>
...	...	k?	...
Tali	Tali	k1gNnSc1	Tali
e	e	k0	e
cotanti	cotant	k1gMnPc1	cotant
sono	sono	k6eAd1	sono
KV	KV	kA	KV
36	[number]	k4	36
<g/>
;	;	kIx,	;
Se	s	k7c7	s
al	ala	k1gFnPc2	ala
labbro	labbro	k1gNnSc1	labbro
mio	mio	k?	mio
non	non	k?	non
credi	cred	k1gMnPc1	cred
<g/>
...	...	k?	...
Il	Il	k1gMnPc1	Il
cor	cor	k?	cor
dolente	dolent	k1gMnSc5	dolent
<g/>
,	,	kIx,	,
KV	KV	kA	KV
295	[number]	k4	295
<g/>
;	;	kIx,	;
Per	pero	k1gNnPc2	pero
pietà	pietà	k?	pietà
<g/>
,	,	kIx,	,
non	non	k?	non
ricercate	ricercat	k1gMnSc5	ricercat
KV	KV	kA	KV
420	[number]	k4	420
<g/>
;	;	kIx,	;
Misero	Misero	k1gNnSc1	Misero
<g/>
!	!	kIx.	!
</s>
<s>
o	o	k7c4	o
sogno	sogno	k1gNnSc4	sogno
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
...	...	k?	...
Aura	aura	k1gFnSc1	aura
<g/>
,	,	kIx,	,
che	che	k0	che
intorno	intorno	k1gNnSc1	intorno
KV	KV	kA	KV
431	[number]	k4	431
<g/>
;	;	kIx,	;
Non	Non	k1gFnSc1	Non
più	più	k?	più
<g/>
,	,	kIx,	,
tutto	tutt	k2eAgNnSc1d1	tutto
ascoltai	ascoltai	k1gNnSc1	ascoltai
<g/>
...	...	k?	...
Non	Non	k1gFnSc1	Non
temer	temer	k1gInSc1	temer
<g/>
,	,	kIx,	,
amato	amat	k2eAgNnSc1d1	amat
bene	bene	k6eAd1	bene
KV	KV	kA	KV
490	[number]	k4	490
Misero	Misero	k1gNnSc4	Misero
me	me	k?	me
<g/>
...	...	k?	...
Misero	Misero	k1gNnSc1	Misero
pargoletto	pargoletto	k1gNnSc1	pargoletto
<g/>
,	,	kIx,	,
KV	KV	kA	KV
77	[number]	k4	77
<g/>
;	;	kIx,	;
Se	s	k7c7	s
ardire	ardir	k1gInSc5	ardir
<g/>
,	,	kIx,	,
e	e	k0	e
speranza	speranza	k1gFnSc1	speranza
KV	KV	kA	KV
82	[number]	k4	82
<g/>
;	;	kIx,	;
Se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
tutti	tutti	k2eAgNnSc1d1	tutti
mali	mali	k1gNnSc1	mali
miei	mie	k1gFnSc2	mie
<g/>
,	,	kIx,	,
KV	KV	kA	KV
83	[number]	k4	83
<g/>
;	;	kIx,	;
Fra	Fra	k1gFnSc6	Fra
cento	cento	k1gNnSc4	cento
affanni	affannit	k5eAaPmRp2nS	affannit
KV	KV	kA	KV
88	[number]	k4	88
<g/>
;	;	kIx,	;
Der	drát	k5eAaImRp2nS	drát
Liebe	Lieb	k1gMnSc5	Lieb
himmlisches	himmlisches	k1gMnSc1	himmlisches
Gefühl	Gefühl	k1gFnSc3	Gefühl
KV	KV	kA	KV
119	[number]	k4	119
<g/>
;	;	kIx,	;
Ah	ah	k0	ah
<g/>
,	,	kIx,	,
lo	lo	k?	lo
previdi	previd	k1gMnPc1	previd
<g/>
...	...	k?	...
Ah	ah	k0	ah
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
invola	invola	k1gFnSc1	invola
agl	agl	k?	agl
<g/>
'	'	kIx"	'
<g/>
occhi	occhit	k5eAaImRp2nS	occhit
miei	mie	k1gFnSc2	mie
mV	mV	k?	mV
272	[number]	k4	272
<g/>
;	;	kIx,	;
Alcandro	Alcandra	k1gFnSc5	Alcandra
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
lo	lo	k?	lo
confesso	confessa	k1gFnSc5	confessa
<g/>
...	...	k?	...
Non	Non	k1gFnPc3	Non
sò	sò	k?	sò
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
onde	onde	k6eAd1	onde
viene	vienout	k5eAaPmIp3nS	vienout
KV	KV	kA	KV
294	[number]	k4	294
<g/>
;	;	kIx,	;
Popoli	Popole	k1gFnSc3	Popole
di	di	k?	di
Tessaglia	Tessaglia	k1gFnSc1	Tessaglia
<g/>
...	...	k?	...
Io	Io	k1gFnSc1	Io
non	non	k?	non
chiedo	chieda	k1gMnSc5	chieda
<g/>
,	,	kIx,	,
eterni	eternit	k5eAaPmRp2nS	eternit
Dei	Dei	k1gMnSc1	Dei
KV	KV	kA	KV
316	[number]	k4	316
<g/>
;	;	kIx,	;
Ma	Ma	k1gMnSc1	Ma
che	che	k0	che
vi	vi	k?	vi
fece	fece	k1gNnPc2	fece
<g/>
,	,	kIx,	,
o	o	k7c6	o
stelle	stella	k1gFnSc6	stella
<g/>
...	...	k?	...
Sperai	Sperai	k1gNnSc1	Sperai
vicino	vicin	k2eAgNnSc1d1	vicin
il	il	k?	il
lido	lido	k1gNnSc1	lido
KV	KV	kA	KV
368	[number]	k4	368
<g/>
;	;	kIx,	;
Misera	Misera	k1gFnSc1	Misera
<g/>
,	,	kIx,	,
dove	dove	k1gFnSc1	dove
son	son	k1gInSc1	son
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
...	...	k?	...
Ah	ah	k0	ah
<g/>
,	,	kIx,	,
non	non	k?	non
son	son	k1gInSc1	son
io	io	k?	io
che	che	k0	che
parlo	parnout	k5eAaImAgNnS	parnout
KV	KV	kA	KV
369	[number]	k4	369
<g/>
;	;	kIx,	;
A	a	k9	a
questo	questo	k6eAd1	questo
seno	seno	k1gNnSc4	seno
deh	deh	k?	deh
vieni	vieň	k1gMnPc7	vieň
<g/>
...	...	k?	...
Or	Or	k1gMnPc7	Or
che	che	k0	che
il	il	k?	il
cielo	cielo	k1gNnSc1	cielo
a	a	k8xC	a
me	me	k?	me
ti	ten	k3xDgMnPc1	ten
rende	rende	k6eAd1	rende
KV	KV	kA	KV
374	[number]	k4	374
<g/>
;	;	kIx,	;
Nehmt	Nehmt	k2eAgInSc1d1	Nehmt
meinen	meinen	k2eAgInSc1d1	meinen
Dank	Dank	k1gInSc1	Dank
<g/>
,	,	kIx,	,
ihr	ihr	k?	ihr
holden	holdna	k1gFnPc2	holdna
Gönner	Gönnra	k1gFnPc2	Gönnra
KV	KV	kA	KV
383	[number]	k4	383
<g/>
;	;	kIx,	;
Mia	Mia	k1gFnSc1	Mia
speranza	speranza	k1gFnSc1	speranza
dorata	dorata	k1gFnSc1	dorata
<g/>
...	...	k?	...
<g />
.	.	kIx.	.
</s>
<s>
Ah	ah	k0	ah
<g/>
,	,	kIx,	,
non	non	k?	non
sai	sai	k?	sai
qual	qual	k1gMnSc1	qual
pena	pena	k1gMnSc1	pena
<g/>
,	,	kIx,	,
KV	KV	kA	KV
416	[number]	k4	416
<g/>
;	;	kIx,	;
Vorrei	Vorre	k1gFnPc1	Vorre
spiegarvi	spiegarev	k1gFnSc3	spiegarev
<g/>
,	,	kIx,	,
oh	oh	k0	oh
Dio	Dio	k1gMnPc4	Dio
KV	KV	kA	KV
418	[number]	k4	418
<g/>
;	;	kIx,	;
No	no	k9	no
<g/>
,	,	kIx,	,
no	no	k9	no
<g/>
,	,	kIx,	,
che	che	k0	che
non	non	k?	non
sei	sei	k?	sei
capace	capace	k1gFnSc2	capace
KV	KV	kA	KV
419	[number]	k4	419
<g/>
;	;	kIx,	;
In	In	k1gFnSc1	In
te	te	k?	te
spero	spero	k1gNnSc1	spero
<g/>
,	,	kIx,	,
o	o	k7c6	o
sposo	sposa	k1gFnSc5	sposa
amato	amat	k2eAgNnSc4d1	amat
KV	KV	kA	KV
440	[number]	k4	440
<g/>
;	;	kIx,	;
Basta	basta	k0	basta
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vincesti	vincest	k1gMnPc5	vincest
<g/>
...	...	k?	...
Ah	ah	k0	ah
<g/>
,	,	kIx,	,
non	non	k?	non
lasciarmi	lasciar	k1gFnPc7	lasciar
KV	KV	kA	KV
486	[number]	k4	486
<g/>
a	a	k8xC	a
<g/>
;	;	kIx,	;
Ch	Ch	kA	Ch
<g/>
'	'	kIx"	'
<g/>
io	io	k?	io
mi	já	k3xPp1nSc3	já
scordi	scord	k1gMnPc1	scord
di	di	k?	di
te	te	k?	te
<g/>
...	...	k?	...
Non	Non	k1gMnSc1	Non
temer	temer	k1gMnSc1	temer
<g/>
,	,	kIx,	,
amato	amat	k2eAgNnSc1d1	amat
bene	bene	k6eAd1	bene
KV	KV	kA	KV
505	[number]	k4	505
<g/>
;	;	kIx,	;
Bella	Bella	k1gFnSc1	Bella
mia	mia	k?	mia
fiamma	fiamma	k1gFnSc1	fiamma
<g/>
...	...	k?	...
Resta	Resta	k1gFnSc1	Resta
<g/>
,	,	kIx,	,
o	o	k7c4	o
cara	car	k1gMnSc4	car
KV	KV	kA	KV
528	[number]	k4	528
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
Ah	ah	k0	ah
se	se	k3xPyFc4	se
in	in	k?	in
ciel	ciel	k1gInSc1	ciel
<g/>
,	,	kIx,	,
benigne	benignout	k5eAaPmIp3nS	benignout
stelle	stelle	k6eAd1	stelle
KV	KV	kA	KV
538	[number]	k4	538
<g/>
;	;	kIx,	;
Alma	alma	k1gFnSc1	alma
grande	grand	k1gMnSc5	grand
e	e	k0	e
nobil	nobil	k1gMnSc1	nobil
cuore	cuor	k1gInSc5	cuor
KV	KV	kA	KV
578	[number]	k4	578
<g/>
;	;	kIx,	;
Schon	Schon	k1gMnSc1	Schon
lacht	lacht	k1gMnSc1	lacht
der	drát	k5eAaImRp2nS	drát
holde	hold	k1gInSc5	hold
Frühling	Frühling	k1gInSc4	Frühling
KV	KV	kA	KV
580	[number]	k4	580
<g/>
;	;	kIx,	;
Chi	chi	k0	chi
sà	sà	k?	sà
<g/>
,	,	kIx,	,
chi	chi	k0	chi
sà	sà	k?	sà
qual	qual	k1gInSc1	qual
sia	sia	k?	sia
K.	K.	kA	K.
582	[number]	k4	582
<g/>
,	,	kIx,	,
Vado	vada	k1gFnSc5	vada
<g/>
,	,	kIx,	,
ma	ma	k?	ma
dove	dove	k1gInSc1	dove
<g/>
?	?	kIx.	?
</s>
<s>
KV	KV	kA	KV
583	[number]	k4	583
Malá	malý	k2eAgFnSc1d1	malá
noční	noční	k2eAgFnSc1d1	noční
hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
Eine	Eine	k1gFnSc1	Eine
kleine	kleinout	k5eAaPmIp3nS	kleinout
Nachtmusik	Nachtmusik	k1gInSc4	Nachtmusik
<g/>
)	)	kIx)	)
O	o	k7c4	o
heiliges	heiliges	k1gInSc4	heiliges
Band	banda	k1gFnPc2	banda
KV	KV	kA	KV
148	[number]	k4	148
<g/>
,	,	kIx,	,
Oiseaux	Oiseaux	k1gInSc1	Oiseaux
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
tous	tous	k6eAd1	tous
les	les	k1gInSc1	les
ans	ans	k?	ans
KV	KV	kA	KV
307	[number]	k4	307
<g/>
,	,	kIx,	,
Dans	Dans	k1gInSc1	Dans
un	un	k?	un
bois	bois	k1gInSc1	bois
solitaire	solitair	k1gInSc5	solitair
KV	KV	kA	KV
308	[number]	k4	308
<g/>
,	,	kIx,	,
Die	Die	k1gFnSc1	Die
Zufriedenheit	Zufriedenheit	k1gInSc1	Zufriedenheit
KV	KV	kA	KV
349	[number]	k4	349
<g/>
,	,	kIx,	,
Komm	Komm	k1gInSc1	Komm
<g/>
,	,	kIx,	,
liebe	liebat	k5eAaPmIp3nS	liebat
Zither	Zithra	k1gFnPc2	Zithra
KV	KV	kA	KV
<g />
.	.	kIx.	.
</s>
<s>
351	[number]	k4	351
<g/>
,	,	kIx,	,
Warnung	Warnung	k1gInSc1	Warnung
KV	KV	kA	KV
433	[number]	k4	433
<g/>
,	,	kIx,	,
Gesellenreise	Gesellenreise	k1gFnSc1	Gesellenreise
KV	KV	kA	KV
468	[number]	k4	468
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Zauberer	Zauberer	k1gInSc4	Zauberer
KV	KV	kA	KV
472	[number]	k4	472
<g/>
,	,	kIx,	,
Das	Das	k1gFnSc1	Das
Veilchen	Veilchen	k1gInSc1	Veilchen
KV	KV	kA	KV
476	[number]	k4	476
<g/>
,	,	kIx,	,
Die	Die	k1gFnSc1	Die
Alte	alt	k1gInSc5	alt
KV	KV	kA	KV
517	[number]	k4	517
<g/>
,	,	kIx,	,
Die	Die	k1gFnSc1	Die
Verschweigung	Verschweigung	k1gInSc1	Verschweigung
KV	KV	kA	KV
518	[number]	k4	518
<g/>
,	,	kIx,	,
Das	Das	k1gMnSc1	Das
Lied	Lied	k1gMnSc1	Lied
der	drát	k5eAaImRp2nS	drát
Trennung	Trennung	k1gInSc4	Trennung
KV	KV	kA	KV
519	[number]	k4	519
<g/>
,	,	kIx,	,
Als	Als	k1gFnSc6	Als
Luise	Luisa	k1gFnSc6	Luisa
die	die	k?	die
Briefe	Brief	k1gInSc5	Brief
ihres	ihresa	k1gFnPc2	ihresa
<g />
.	.	kIx.	.
</s>
<s>
ungetreuen	ungetreuen	k2eAgInSc1d1	ungetreuen
Liebhabers	Liebhabers	k1gInSc1	Liebhabers
verbrannte	verbrannt	k1gInSc5	verbrannt
KV	KV	kA	KV
520	[number]	k4	520
<g/>
,	,	kIx,	,
Abendempfindung	Abendempfindung	k1gInSc1	Abendempfindung
KV	KV	kA	KV
523	[number]	k4	523
<g/>
,	,	kIx,	,
An	An	k1gMnSc1	An
Chloe	Chlo	k1gFnSc2	Chlo
KV	KV	kA	KV
524	[number]	k4	524
<g/>
,	,	kIx,	,
Des	des	k1gNnSc7	des
kleinen	kleinen	k1gInSc1	kleinen
Friedrichs	Friedrichs	k1gInSc1	Friedrichs
Geburtstag	Geburtstag	k1gInSc4	Geburtstag
KV	KV	kA	KV
529	[number]	k4	529
<g/>
,	,	kIx,	,
Das	Das	k1gFnSc1	Das
Traumbild	Traumbild	k1gInSc1	Traumbild
KV	KV	kA	KV
530	[number]	k4	530
<g/>
,	,	kIx,	,
Grazie	Grazie	k1gFnSc1	Grazie
agl	agl	k?	agl
<g/>
'	'	kIx"	'
<g/>
inganni	inganň	k1gMnSc3	inganň
tuoi	tuo	k1gFnSc2	tuo
KV	KV	kA	KV
532	[number]	k4	532
<g/>
,	,	kIx,	,
Sehnsucht	Sehnsucht	k2eAgInSc1d1	Sehnsucht
nach	nach	k1gInSc1	nach
dem	dem	k?	dem
Frühlinge	Frühling	k1gInSc2	Frühling
KV	KV	kA	KV
596	[number]	k4	596
<g/>
,	,	kIx,	,
Im	Im	k1gFnSc1	Im
Frühlingsanfang	Frühlingsanfang	k1gInSc1	Frühlingsanfang
KV	KV	kA	KV
597	[number]	k4	597
<g/>
,	,	kIx,	,
Das	Das	k1gFnSc1	Das
Kinderspiel	Kinderspiel	k1gInSc1	Kinderspiel
KV	KV	kA	KV
598	[number]	k4	598
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
nahrávka	nahrávka	k1gFnSc1	nahrávka
díla	dílo	k1gNnSc2	dílo
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
oceněna	ocenit	k5eAaPmNgFnS	ocenit
cenou	cena	k1gFnSc7	cena
Wiener	Wiener	k1gMnSc1	Wiener
Flötenuhr	Flötenuhr	k1gMnSc1	Flötenuhr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nekomerčním	komerční	k2eNgInSc6d1	nekomerční
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgInSc4d1	Musopen
postupně	postupně	k6eAd1	postupně
přibývají	přibývat	k5eAaImIp3nP	přibývat
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgInPc1d1	šiřitelný
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
public	publicum	k1gNnPc2	publicum
domain	domaina	k1gFnPc2	domaina
licencí	licence	k1gFnPc2	licence
<g/>
)	)	kIx)	)
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
nahráli	nahrát	k5eAaBmAgMnP	nahrát
a	a	k8xC	a
zpracovali	zpracovat	k5eAaPmAgMnP	zpracovat
přední	přední	k2eAgMnPc1d1	přední
světoví	světový	k2eAgMnPc1d1	světový
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnSc2d1	malá
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
modrooký	modrooký	k2eAgInSc1d1	modrooký
<g/>
,	,	kIx,	,
světlých	světlý	k2eAgInPc2d1	světlý
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
obličej	obličej	k1gInSc4	obličej
pouze	pouze	k6eAd1	pouze
slabě	slabě	k6eAd1	slabě
poďobaný	poďobaný	k2eAgMnSc1d1	poďobaný
od	od	k7c2	od
neštovic	neštovice	k1gFnPc2	neštovice
<g/>
;	;	kIx,	;
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
prý	prý	k9	prý
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
nějakého	nějaký	k3yIgMnSc4	nějaký
výrazně	výrazně	k6eAd1	výrazně
atraktivního	atraktivní	k2eAgMnSc4d1	atraktivní
krasavce	krasavec	k1gMnSc4	krasavec
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
byl	být	k5eAaImAgMnS	být
nicméně	nicméně	k8xC	nicméně
člověk	člověk	k1gMnSc1	člověk
nesmírně	smírně	k6eNd1	smírně
společenský	společenský	k2eAgMnSc1d1	společenský
a	a	k8xC	a
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
dámské	dámský	k2eAgFnSc6d1	dámská
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
pohybovat	pohybovat	k5eAaImF	pohybovat
a	a	k8xC	a
být	být	k5eAaImF	být
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
společenských	společenský	k2eAgFnPc6d1	společenská
vrstvách	vrstva	k1gFnPc6	vrstva
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
měl	mít	k5eAaImAgMnS	mít
fenomenální	fenomenální	k2eAgFnSc4d1	fenomenální
hudební	hudební	k2eAgFnSc4d1	hudební
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
hudebních	hudební	k2eAgFnPc2d1	hudební
skladeb	skladba	k1gFnPc2	skladba
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
kompletně	kompletně	k6eAd1	kompletně
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prý	prý	k9	prý
už	už	k6eAd1	už
od	od	k7c2	od
raného	raný	k2eAgNnSc2d1	rané
dětství	dětství	k1gNnSc2	dětství
<g/>
)	)	kIx)	)
na	na	k7c4	na
jediný	jediný	k2eAgInSc4d1	jediný
poslech	poslech	k1gInSc4	poslech
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
skladbu	skladba	k1gFnSc4	skladba
pak	pak	k6eAd1	pak
dokázal	dokázat	k5eAaPmAgMnS	dokázat
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
i	i	k9	i
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
v	v	k7c6	v
notách	nota	k1gFnPc6	nota
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
choval	chovat	k5eAaImAgMnS	chovat
jako	jako	k8xC	jako
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
kanárka	kanárek	k1gMnSc2	kanárek
<g/>
,	,	kIx,	,
špačka	špaček	k1gMnSc2	špaček
<g/>
,	,	kIx,	,
psa	pes	k1gMnSc2	pes
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
projížděl	projíždět	k5eAaImAgMnS	projíždět
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
rád	rád	k6eAd1	rád
kulečník	kulečník	k1gInSc1	kulečník
a	a	k8xC	a
tanec	tanec	k1gInSc1	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
také	také	k9	také
dosti	dosti	k6eAd1	dosti
náruživý	náruživý	k2eAgInSc1d1	náruživý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
karetním	karetní	k2eAgMnSc7d1	karetní
hráčem	hráč	k1gMnSc7	hráč
a	a	k8xC	a
ovládal	ovládat	k5eAaImAgInS	ovládat
prý	prý	k9	prý
velmi	velmi	k6eAd1	velmi
nadprůměrně	nadprůměrně	k6eAd1	nadprůměrně
až	až	k9	až
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
různých	různý	k2eAgFnPc2d1	různá
karetních	karetní	k2eAgFnPc2d1	karetní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
samotné	samotný	k2eAgFnSc2d1	samotná
hudby	hudba	k1gFnSc2	hudba
prý	prý	k9	prý
byly	být	k5eAaImAgFnP	být
karty	karta	k1gFnPc1	karta
jeho	jeho	k3xOp3gFnSc4	jeho
další	další	k2eAgFnSc4d1	další
velkou	velká	k1gFnSc4	velká
osobní	osobní	k2eAgFnSc7d1	osobní
vášní	vášeň	k1gFnSc7	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
mluvil	mluvit	k5eAaImAgMnS	mluvit
plynně	plynně	k6eAd1	plynně
několika	několik	k4yIc7	několik
světovými	světový	k2eAgInPc7d1	světový
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
němčiny	němčina	k1gFnSc2	němčina
ovládal	ovládat	k5eAaImAgInS	ovládat
výborně	výborně	k6eAd1	výborně
italštinu	italština	k1gFnSc4	italština
a	a	k8xC	a
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Mozartův	Mozartův	k2eAgInSc1d1	Mozartův
kult	kult	k1gInSc1	kult
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
tak	tak	k6eAd1	tak
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
živý	živý	k2eAgMnSc1d1	živý
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
nejen	nejen	k6eAd1	nejen
hudební	hudební	k2eAgInPc4d1	hudební
festivaly	festival	k1gInPc4	festival
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výrobky	výrobek	k1gInPc1	výrobek
naprosto	naprosto	k6eAd1	naprosto
nehudebního	hudební	k2eNgInSc2d1	nehudební
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
cukrovinky	cukrovinka	k1gFnPc1	cukrovinka
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Mozartovy	Mozartův	k2eAgFnPc4d1	Mozartova
koule	koule	k1gFnPc4	koule
z	z	k7c2	z
pistáciového	pistáciový	k2eAgInSc2d1	pistáciový
marcipánu	marcipán	k1gInSc2	marcipán
<g/>
,	,	kIx,	,
obalené	obalený	k2eAgInPc1d1	obalený
čokoládou	čokoláda	k1gFnSc7	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
miloval	milovat	k5eAaImAgMnS	milovat
smažené	smažený	k2eAgInPc4d1	smažený
kuřecí	kuřecí	k2eAgInPc4d1	kuřecí
kousky	kousek	k1gInPc4	kousek
a	a	k8xC	a
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mozartovi	Mozart	k1gMnSc6	Mozart
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
pár	pár	k1gInSc1	pár
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vlaků	vlak	k1gInPc2	vlak
railjet	railjeta	k1gFnPc2	railjeta
(	(	kIx(	(
<g/>
372	[number]	k4	372
<g/>
/	/	kIx~	/
<g/>
373	[number]	k4	373
<g/>
)	)	kIx)	)
společností	společnost	k1gFnPc2	společnost
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
ÖBB	ÖBB	kA	ÖBB
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Wien	Wien	k1gNnSc1	Wien
(	(	kIx(	(
<g/>
-	-	kIx~	-
Bruck	Bruck	k1gInSc1	Bruck
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Mur	mur	k1gInSc1	mur
-	-	kIx~	-
Graz	Graz	k1gInSc1	Graz
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Mozart	Mozart	k1gMnSc1	Mozart
auf	auf	k?	auf
der	drát	k5eAaImRp2nS	drát
Reise	Reise	k1gFnSc1	Reise
nach	nach	k1gInSc4	nach
Prag	Prag	k1gInSc1	Prag
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poetická	poetický	k2eAgFnSc1d1	poetická
novela	novela	k1gFnSc1	novela
německého	německý	k2eAgMnSc2d1	německý
romantického	romantický	k2eAgMnSc2d1	romantický
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
prozaika	prozaik	k1gMnSc2	prozaik
Eduarda	Eduard	k1gMnSc2	Eduard
Friedricha	Friedrich	k1gMnSc2	Friedrich
Mörikeho	Mörike	k1gMnSc2	Mörike
zachycující	zachycující	k2eAgFnSc4d1	zachycující
cestu	cesta	k1gFnSc4	cesta
Wolfganga	Wolfgang	k1gMnSc4	Wolfgang
Amadea	Amadeus	k1gMnSc4	Amadeus
Mozarta	Mozart	k1gMnSc4	Mozart
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
premiéru	premiéra	k1gFnSc4	premiéra
opery	opera	k1gFnSc2	opera
Don	dona	k1gFnPc2	dona
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
</s>
<s>
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
,	,	kIx,	,
krátká	krátký	k2eAgFnSc1d1	krátká
hra	hra	k1gFnSc1	hra
Alexandra	Alexandr	k1gMnSc2	Alexandr
Puškina	Puškin	k1gMnSc2	Puškin
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
operní	operní	k2eAgFnSc1d1	operní
adaptace	adaptace	k1gFnSc1	adaptace
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Rimského-Korsakova	Rimského-Korsakův	k2eAgMnSc2d1	Rimského-Korsakův
<g/>
.	.	kIx.	.
</s>
<s>
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
od	od	k7c2	od
Petera	Peter	k1gMnSc2	Peter
Shaffera	Shaffer	k1gMnSc2	Shaffer
-	-	kIx~	-
částečně	částečně	k6eAd1	částečně
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
životopisech	životopis	k1gInPc6	životopis
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc4	Mozart
a	a	k8xC	a
A.	A.	kA	A.
Salieriho	Salieri	k1gMnSc2	Salieri
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
natočen	natočen	k2eAgInSc1d1	natočen
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
Amadeus	Amadeus	k1gMnSc1	Amadeus
režiséra	režisér	k1gMnSc2	režisér
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
<g/>
.	.	kIx.	.
</s>
<s>
CASINI	CASINI	kA	CASINI
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Mozartův	Mozartův	k2eAgInSc1d1	Mozartův
(	(	kIx(	(
<g/>
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
,	,	kIx,	,
Vita	vit	k2eAgFnSc1d1	Vita
di	di	k?	di
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-0862-4	[number]	k4	80-200-0862-4
DEUTSCH	DEUTSCH	kA	DEUTSCH
<g/>
,	,	kIx,	,
O.	O.	kA	O.
E.	E.	kA	E.
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
Dokumente	dokument	k1gInSc5	dokument
seines	seines	k1gMnSc1	seines
Lebens	Lebens	k1gInSc1	Lebens
<g/>
.	.	kIx.	.
</s>
<s>
Kassel	Kassel	k1gInSc1	Kassel
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
EIBL	EIBL	kA	EIBL
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Heinz	Heinz	k1gMnSc1	Heinz
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
:	:	kIx,	:
Briefe	Brief	k1gInSc5	Brief
und	und	k?	und
Aufzeichnungen	Aufzeichnungen	k1gInSc1	Aufzeichnungen
<g/>
.	.	kIx.	.
</s>
<s>
Kassel	Kassel	k1gInSc1	Kassel
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
KREJČOVÁ	Krejčová	k1gFnSc1	Krejčová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
a	a	k8xC	a
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
studií	studio	k1gNnPc2	studio
k	k	k7c3	k
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
Wolfganga	Wolfgang	k1gMnSc4	Wolfgang
Amadea	Amadeus	k1gMnSc4	Amadeus
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
UP	UP	kA	UP
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7067-023-1	[number]	k4	80-7067-023-1
FREEMAN	FREEMAN	kA	FREEMAN
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
E.	E.	kA	E.
Mozart	Mozart	k1gMnSc1	Mozart
in	in	k?	in
Prague	Prague	k1gInSc1	Prague
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
:	:	kIx,	:
Bearclaw	Bearclaw	k1gMnSc1	Bearclaw
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-0-9794223-1-7	[number]	k4	978-0-9794223-1-7
LEONHARTOVÁ	LEONHARTOVÁ	kA	LEONHARTOVÁ
<g/>
,	,	kIx,	,
Dorothea	Dorotheum	k1gNnSc2	Dorotheum
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
zamlčená	zamlčený	k2eAgFnSc1d1	zamlčená
tvář	tvář	k1gFnSc1	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1721	[number]	k4	1721
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
Haló	haló	k0	haló
sobota	sobota	k1gFnSc1	sobota
<g/>
,	,	kIx,	,
čtvrtletník	čtvrtletník	k1gInSc1	čtvrtletník
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
,	,	kIx,	,
zima	zima	k1gFnSc1	zima
1990	[number]	k4	1990
<g/>
;	;	kIx,	;
INDEX	index	k1gInSc1	index
46864	[number]	k4	46864
MELOGRANI	MELOGRANI	kA	MELOGRANI
<g/>
,	,	kIx,	,
Piero	Piero	k1gNnSc1	Piero
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
:	:	kIx,	:
život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Amadea	Amadeus	k1gMnSc2	Amadeus
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
296	[number]	k4	296
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
62	[number]	k4	62
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
VEIGL	VEIGL	kA	VEIGL
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Friedhof	Friedhof	k1gInSc1	Friedhof
zu	zu	k?	zu
St.	st.	kA	st.
Marx	Marx	k1gMnSc1	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k6eAd1	Eine
letzte	letzit	k5eAaPmRp2nP	letzit
biedermeierliche	biedermeierliche	k1gFnSc4	biedermeierliche
Begräbnisstätte	Begräbnisstätt	k1gInSc5	Begräbnisstätt
in	in	k?	in
Wien	Wiena	k1gFnPc2	Wiena
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
:	:	kIx,	:
Böhlau	Böhlaus	k1gInSc2	Böhlaus
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
77389	[number]	k4	77389
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
WEISS	Weiss	k1gMnSc1	Weiss
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
-	-	kIx~	-
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
génius	génius	k1gMnSc1	génius
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Alena	Alena	k1gFnSc1	Alena
Jindrová-Špilarová	Jindrová-Špilarová	k1gFnSc1	Jindrová-Špilarová
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
480	[number]	k4	480
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7341	[number]	k4	7341
<g/>
-	-	kIx~	-
<g/>
773	[number]	k4	773
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Osoba	osoba	k1gFnSc1	osoba
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaImAgMnS	dít
od	od	k7c2	od
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
Mozart-archiv	Mozartrchiva	k1gFnPc2	Mozart-archiva
-	-	kIx~	-
většina	většina	k1gFnSc1	většina
Mozartových	Mozartových	k2eAgFnPc2d1	Mozartových
skladeb	skladba	k1gFnPc2	skladba
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
MP3	MP3	k1gFnSc2	MP3
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
