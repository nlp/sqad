<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnPc4	jeho
nejproslulejší	proslulý	k2eAgNnPc4d3	nejproslulejší
díla	dílo	k1gNnPc4	dílo
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
opery	opera	k1gFnSc2	opera
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
a	a	k8xC	a
Don	Don	k1gMnSc1	Don
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
monumentální	monumentální	k2eAgInSc1d1	monumentální
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
opus	opus	k1gInSc1	opus
<g/>
,	,	kIx,	,
zádušní	zádušní	k2eAgFnSc2d1	zádušní
mše	mše	k1gFnSc2	mše
Requiem	Requius	k1gMnSc7	Requius
d	d	k?	d
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
KV	KV	kA	KV
626	[number]	k4	626
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
