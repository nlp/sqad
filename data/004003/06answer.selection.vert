<s>
Večerníček	večerníček	k1gInSc1	večerníček
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
desetiminutový	desetiminutový	k2eAgInSc4d1	desetiminutový
večerní	večerní	k2eAgInSc4d1	večerní
televizní	televizní	k2eAgInSc4d1	televizní
pořad	pořad	k1gInSc4	pořad
s	s	k7c7	s
pohádkovým	pohádkový	k2eAgInSc7d1	pohádkový
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInSc7d1	jiný
příběhem	příběh	k1gInSc7	příběh
určeným	určený	k2eAgInSc7d1	určený
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
každý	každý	k3xTgInSc4	každý
večer	večer	k1gInSc4	večer
původně	původně	k6eAd1	původně
Československou	československý	k2eAgFnSc7d1	Československá
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
Slovenskou	slovenský	k2eAgFnSc7d1	slovenská
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
v	v	k7c4	v
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
čas	čas	k1gInSc4	čas
jejich	jejich	k3xOp3gNnSc2	jejich
uléhání	uléhání	k1gNnSc2	uléhání
do	do	k7c2	do
postele	postel	k1gFnSc2	postel
<g/>
.	.	kIx.	.
</s>
