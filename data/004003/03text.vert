<s>
Večerníček	večerníček	k1gInSc1	večerníček
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
desetiminutový	desetiminutový	k2eAgInSc4d1	desetiminutový
večerní	večerní	k2eAgInSc4d1	večerní
televizní	televizní	k2eAgInSc4d1	televizní
pořad	pořad	k1gInSc4	pořad
s	s	k7c7	s
pohádkovým	pohádkový	k2eAgInSc7d1	pohádkový
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInSc7d1	jiný
příběhem	příběh	k1gInSc7	příběh
určeným	určený	k2eAgInSc7d1	určený
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
každý	každý	k3xTgInSc4	každý
večer	večer	k1gInSc4	večer
původně	původně	k6eAd1	původně
Československou	československý	k2eAgFnSc7d1	Československá
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
Slovenskou	slovenský	k2eAgFnSc7d1	slovenská
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
v	v	k7c4	v
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
čas	čas	k1gInSc4	čas
jejich	jejich	k3xOp3gNnSc2	jejich
uléhání	uléhání	k1gNnSc2	uléhání
do	do	k7c2	do
postele	postel	k1gFnSc2	postel
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
pořadu	pořad	k1gInSc2	pořad
je	být	k5eAaImIp3nS	být
kreslený	kreslený	k2eAgMnSc1d1	kreslený
chlapec	chlapec	k1gMnSc1	chlapec
Večerníček	večerníček	k1gInSc1	večerníček
v	v	k7c6	v
papírové	papírový	k2eAgFnSc6d1	papírová
čepici	čepice	k1gFnSc6	čepice
ve	v	k7c6	v
znělce	znělka	k1gFnSc6	znělka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pohádkové	pohádkový	k2eAgFnSc6d1	pohádková
epizodě	epizoda	k1gFnSc6	epizoda
předchází	předcházet	k5eAaImIp3nS	předcházet
<g/>
;	;	kIx,	;
autorem	autor	k1gMnSc7	autor
postavičky	postavička	k1gFnSc2	postavička
je	být	k5eAaImIp3nS	být
malíř	malíř	k1gMnSc1	malíř
Radek	Radek	k1gMnSc1	Radek
Pilař	Pilař	k1gMnSc1	Pilař
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1	předchůdce
Večerníčků	večerníček	k1gInPc2	večerníček
byl	být	k5eAaImAgInS	být
pořad	pořad	k1gInSc1	pořad
Stříbrné	stříbrná	k1gFnSc2	stříbrná
zrcátko	zrcátko	k1gNnSc1	zrcátko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
své	svůj	k3xOyFgNnSc4	svůj
vysílání	vysílání	k1gNnSc4	vysílání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
v	v	k7c6	v
nedělním	nedělní	k2eAgInSc6d1	nedělní
podvečerním	podvečerní	k2eAgInSc6d1	podvečerní
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
večerníček	večerníček	k1gInSc4	večerníček
ucelenou	ucelený	k2eAgFnSc7d1	ucelená
formou	forma	k1gFnSc7	forma
s	s	k7c7	s
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
nedělním	nedělní	k2eAgNnSc7d1	nedělní
vysíláním	vysílání	k1gNnSc7	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarník	výtvarník	k1gMnSc1	výtvarník
Milan	Milan	k1gMnSc1	Milan
Nápravník	Nápravník	k1gMnSc1	Nápravník
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
tvůrcem	tvůrce	k1gMnSc7	tvůrce
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
dramaturgem	dramaturg	k1gMnSc7	dramaturg
Večerníčku	večerníček	k1gInSc2	večerníček
<g/>
.	.	kIx.	.
</s>
<s>
Animovaná	animovaný	k2eAgFnSc1d1	animovaná
znělka	znělka	k1gFnSc1	znělka
Večerníčku	večerníček	k1gInSc2	večerníček
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
televizní	televizní	k2eAgFnSc1d1	televizní
znělka	znělka	k1gFnSc1	znělka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
kreslené	kreslený	k2eAgFnSc2d1	kreslená
postavy	postava	k1gFnSc2	postava
"	"	kIx"	"
<g/>
kluka	kluk	k1gMnSc2	kluk
-	-	kIx~	-
večerníčka	večerníček	k1gMnSc2	večerníček
<g/>
"	"	kIx"	"
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
animovaném	animovaný	k2eAgNnSc6d1	animované
představení	představení	k1gNnSc6	představení
<g/>
,	,	kIx,	,
před	před	k7c7	před
uvedením	uvedení	k1gNnSc7	uvedení
pohádky	pohádka	k1gFnSc2	pohádka
je	být	k5eAaImIp3nS	být
výtvarník	výtvarník	k1gMnSc1	výtvarník
Radek	Radek	k1gMnSc1	Radek
Pilař	Pilař	k1gMnSc1	Pilař
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
hudby	hudba	k1gFnSc2	hudba
byl	být	k5eAaImAgMnS	být
Ladislav	Ladislav	k1gMnSc1	Ladislav
Simon	Simon	k1gMnSc1	Simon
<g/>
.	.	kIx.	.
</s>
<s>
Pozdrav	pozdrav	k1gInSc1	pozdrav
"	"	kIx"	"
<g/>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
večer	večer	k1gInSc1	večer
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dobrou	dobrý	k2eAgFnSc4d1	dobrá
noc	noc	k1gFnSc4	noc
<g/>
"	"	kIx"	"
namluvil	namluvit	k5eAaBmAgMnS	namluvit
tehdy	tehdy	k6eAd1	tehdy
šestiletý	šestiletý	k2eAgMnSc1d1	šestiletý
Michal	Michal	k1gMnSc1	Michal
Citavý	Citavý	k2eAgMnSc1d1	Citavý
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znělku	znělka	k1gFnSc4	znělka
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1964	[number]	k4	1964
režisér	režisér	k1gMnSc1	režisér
Václav	Václav	k1gMnSc1	Václav
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
barevné	barevný	k2eAgNnSc1d1	barevné
vysílání	vysílání	k1gNnSc1	vysílání
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
od	od	k7c2	od
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
pořad	pořad	k1gInSc1	pořad
vysílán	vysílat	k5eAaImNgInS	vysílat
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
Večerníčkům	večerníček	k1gInPc3	večerníček
patří	patřit	k5eAaImIp3nS	patřit
O	o	k7c6	o
krtkovi	krtek	k1gMnSc6	krtek
<g/>
,	,	kIx,	,
O	o	k7c6	o
loupežníku	loupežník	k1gMnSc6	loupežník
Rumcajsovi	Rumcajs	k1gMnSc6	Rumcajs
<g/>
,	,	kIx,	,
Pohádky	pohádka	k1gFnSc2	pohádka
z	z	k7c2	z
mechu	mech	k1gInSc2	mech
a	a	k8xC	a
kapradí	kapradí	k1gNnSc1	kapradí
<g/>
,	,	kIx,	,
O	o	k7c6	o
makové	makový	k2eAgFnSc6d1	maková
panence	panenka	k1gFnSc6	panenka
<g/>
,	,	kIx,	,
Krkonošské	krkonošský	k2eAgFnPc4d1	Krkonošská
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
Říkání	říkání	k1gNnSc4	říkání
o	o	k7c6	o
víle	víla	k1gFnSc6	víla
Amálce	Amálka	k1gFnSc6	Amálka
<g/>
,	,	kIx,	,
Maxipes	Maxipes	k1gInSc1	Maxipes
Fík	fík	k1gInSc1	fík
<g/>
,	,	kIx,	,
Pat	pat	k1gInSc1	pat
a	a	k8xC	a
Mat	mat	k1gInSc1	mat
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
a	a	k8xC	a
Bobek	Bobek	k1gMnSc1	Bobek
<g/>
,	,	kIx,	,
Mach	Mach	k1gMnSc1	Mach
a	a	k8xC	a
Šebestová	Šebestová	k1gFnSc1	Šebestová
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
večerníčků	večerníček	k1gInPc2	večerníček
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
třeba	třeba	k6eAd1	třeba
Pes	pes	k1gMnSc1	pes
Filipes	Filipes	k1gMnSc1	Filipes
<g/>
,	,	kIx,	,
Tip	tip	k1gInSc1	tip
a	a	k8xC	a
Tap	Tap	k1gFnSc1	Tap
<g/>
,	,	kIx,	,
Rodina	rodina	k1gFnSc1	rodina
Nessových	Nessových	k2eAgFnSc1d1	Nessových
<g/>
,	,	kIx,	,
Jen	jen	k8xS	jen
počkej	počkat	k5eAaPmRp2nS	počkat
<g/>
,	,	kIx,	,
zajíci	zajíc	k1gMnSc5	zajíc
<g/>
!	!	kIx.	!
</s>
<s>
nebo	nebo	k8xC	nebo
příběhy	příběh	k1gInPc4	příběh
Bolka	Bolek	k1gMnSc4	Bolek
a	a	k8xC	a
Lolka	Lolek	k1gMnSc4	Lolek
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
večerníček	večerníček	k1gInSc4	večerníček
vysílá	vysílat	k5eAaImIp3nS	vysílat
výhradně	výhradně	k6eAd1	výhradně
domácí	domácí	k2eAgFnSc4d1	domácí
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
se	se	k3xPyFc4	se
vysílání	vysílání	k1gNnSc1	vysílání
pořadů	pořad	k1gInPc2	pořad
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
nového	nový	k2eAgNnSc2d1	nové
vedení	vedení	k1gNnSc2	vedení
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
programu	program	k1gInSc2	program
na	na	k7c6	na
ČT	ČT	kA	ČT
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
ČT	ČT	kA	ČT
:	:	kIx,	:
<g/>
D	D	kA	D
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
vysílán	vysílán	k2eAgInSc1d1	vysílán
souběžně	souběžně	k6eAd1	souběžně
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Večerníček	večerníček	k1gInSc1	večerníček
je	být	k5eAaImIp3nS	být
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
dětským	dětský	k2eAgInSc7d1	dětský
pořadem	pořad	k1gInSc7	pořad
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
také	také	k9	také
nejdéle	dlouho	k6eAd3	dlouho
vysílaným	vysílaný	k2eAgInSc7d1	vysílaný
dětským	dětský	k2eAgInSc7d1	dětský
pořadem	pořad	k1gInSc7	pořad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
oslavil	oslavit	k5eAaPmAgInS	oslavit
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
Večerníček	večerníček	k1gInSc1	večerníček
a	a	k8xC	a
motiv	motiv	k1gInSc1	motiv
pozadí	pozadí	k1gNnSc2	pozadí
z	z	k7c2	z
animované	animovaný	k2eAgFnSc2d1	animovaná
znělky	znělka	k1gFnSc2	znělka
součástí	součást	k1gFnPc2	součást
upraveného	upravený	k2eAgNnSc2d1	upravené
loga	logo	k1gNnSc2	logo
Google	Google	k1gFnSc2	Google
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Google	Google	k1gFnSc1	Google
Doodle	Doodle	k1gFnSc1	Doodle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
připomenutí	připomenutí	k1gNnSc4	připomenutí
46	[number]	k4	46
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
prvního	první	k4xOgNnSc2	první
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
animované	animovaný	k2eAgFnSc6d1	animovaná
postavě	postava	k1gFnSc6	postava
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
33377	[number]	k4	33377
Večerníček	večerníček	k1gInSc1	večerníček
<g/>
,	,	kIx,	,
objevená	objevený	k2eAgFnSc1d1	objevená
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
Petrem	Petr	k1gMnSc7	Petr
Pravcem	Pravec	k1gMnSc7	Pravec
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Ondřejov	Ondřejov	k1gInSc4	Ondřejov
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdelší	dlouhý	k2eAgInPc4d3	nejdelší
české	český	k2eAgInPc4d1	český
večerníčkové	večerníčkové	k2eAgInPc4d1	večerníčkové
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
26	[number]	k4	26
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
52	[number]	k4	52
dílů	díl	k1gInPc2	díl
–	–	k?	–
Bob	bob	k1gInSc1	bob
a	a	k8xC	a
Bobek	bobek	k1gInSc1	bobek
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
49	[number]	k4	49
dílů	díl	k1gInPc2	díl
–	–	k?	–
O	o	k7c6	o
krtkovi	krtek	k1gMnSc6	krtek
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
–	–	k?	–
Bob	bob	k1gInSc1	bob
a	a	k8xC	a
Bobek	bobek	k1gInSc1	bobek
–	–	k?	–
králíci	králík	k1gMnPc1	králík
z	z	k7c2	z
klobouku	klobouk	k1gInSc2	klobouk
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
–	–	k?	–
O	o	k7c6	o
loupežníku	loupežník	k1gMnSc6	loupežník
Rumcajsovi	Rumcajs	k1gMnSc6	Rumcajs
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
–	–	k?	–
Pohádky	pohádka	k1gFnPc4	pohádka
z	z	k7c2	z
mechu	mech	k1gInSc2	mech
a	a	k8xC	a
kapradí	kapraď	k1gFnPc2	kapraď
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
–	–	k?	–
Štaflík	Štaflík	k1gInSc1	Štaflík
a	a	k8xC	a
Špagetka	Špagetka	k1gFnSc1	Špagetka
33	[number]	k4	33
dílů	díl	k1gInPc2	díl
–	–	k?	–
Matylda	Matylda	k1gFnSc1	Matylda
28	[number]	k4	28
dílů	díl	k1gInPc2	díl
–	–	k?	–
...	...	k?	...
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
Při	při	k7c6	při
započtení	započtení	k1gNnSc6	započtení
večerníčkových	večerníčkův	k2eAgInPc2d1	večerníčkův
seriálů	seriál	k1gInPc2	seriál
se	s	k7c7	s
společnými	společný	k2eAgFnPc7d1	společná
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
<g/>
:	:	kIx,	:
91	[number]	k4	91
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
Patem	pat	k1gInSc7	pat
a	a	k8xC	a
Matem	mat	k1gInSc7	mat
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
Kuťáci	Kuťák	k1gMnPc1	Kuťák
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
×	×	k?	×
...	...	k?	...
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
×	×	k?	×
Pat	pat	k1gInSc1	pat
&	&	k?	&
Mat	mat	k1gInSc1	mat
+	+	kIx~	+
1	[number]	k4	1
neautorizovaný	autorizovaný	k2eNgInSc1d1	neautorizovaný
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
×	×	k?	×
Pat	pat	k1gInSc1	pat
a	a	k8xC	a
Mat	mat	k1gInSc1	mat
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
×	×	k?	×
Pat	pat	k1gInSc1	pat
a	a	k8xC	a
Mat	mat	k6eAd1	mat
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Pat	pat	k1gInSc1	pat
a	a	k8xC	a
Mat	mat	k1gInSc1	mat
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
)	)	kIx)	)
91	[number]	k4	91
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
Bobem	Bob	k1gMnSc7	Bob
a	a	k8xC	a
Bobkem	Bobek	k1gMnSc7	Bobek
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
×	×	k?	×
Bob	Bob	k1gMnSc1	Bob
a	a	k8xC	a
Bobek	Bobek	k1gMnSc1	Bobek
–	–	k?	–
králíci	králík	k1gMnPc1	králík
z	z	k7c2	z
klobouku	klobouk	k1gInSc2	klobouk
<g/>
,	,	kIx,	,
52	[number]	k4	52
<g/>
×	×	k?	×
Bob	Bob	k1gMnSc1	Bob
a	a	k8xC	a
Bobek	Bobek	k1gMnSc1	Bobek
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
)	)	kIx)	)
52	[number]	k4	52
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc7	večerníček
s	s	k7c7	s
Rumcajsem	Rumcajso	k1gNnSc7	Rumcajso
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
×	×	k?	×
O	o	k7c6	o
loupežníku	loupežník	k1gMnSc6	loupežník
Rumcajsovi	Rumcajs	k1gMnSc6	Rumcajs
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
O	o	k7c6	o
loupežnickém	loupežnický	k2eAgMnSc6d1	loupežnický
synku	synek	k1gMnSc6	synek
Cipískovi	Cipísek	k1gMnSc6	Cipísek
<g/>
)	)	kIx)	)
49	[number]	k4	49
<g />
.	.	kIx.	.
</s>
<s>
dílů	díl	k1gInPc2	díl
–	–	k?	–
O	o	k7c6	o
krtkovi	krtek	k1gMnSc6	krtek
46	[number]	k4	46
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
Hurvínkem	Hurvínek	k1gMnSc7	Hurvínek
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
×	×	k?	×
Na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
u	u	k7c2	u
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Znovu	znovu	k6eAd1	znovu
u	u	k7c2	u
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
×	×	k?	×
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
vzduchoplavcem	vzduchoplavec	k1gMnSc7	vzduchoplavec
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Hurvínkův	Hurvínkův	k2eAgInSc1d1	Hurvínkův
rok	rok	k1gInSc1	rok
<g/>
)	)	kIx)	)
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
Křemýlkem	Křemýlek	k1gInSc7	Křemýlek
a	a	k8xC	a
Vochomůrkou	Vochomůrka	k1gFnSc7	Vochomůrka
(	(	kIx(	(
<g/>
Pohádky	pohádka	k1gFnSc2	pohádka
z	z	k7c2	z
mechu	mech	k1gInSc2	mech
a	a	k8xC	a
kapradí	kapradí	k1gNnSc2	kapradí
<g/>
)	)	kIx)	)
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
–	–	k?	–
Štaflík	Štaflík	k1gInSc1	Štaflík
a	a	k8xC	a
Špagetka	Špagetka	k1gFnSc1	Špagetka
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
Rákosníčkem	rákosníček	k1gMnSc7	rákosníček
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
×	×	k?	×
Rákosníček	rákosníček	k1gMnSc1	rákosníček
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Rákosníček	rákosníček	k1gMnSc1	rákosníček
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Rákosníček	rákosníček	k1gMnSc1	rákosníček
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
povětří	povětří	k1gNnSc1	povětří
<g/>
)	)	kIx)	)
34	[number]	k4	34
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
Machem	Mach	k1gMnSc7	Mach
a	a	k8xC	a
Šebestovou	Šebestová	k1gFnSc7	Šebestová
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
×	×	k?	×
Mach	Mach	k1gMnSc1	Mach
a	a	k8xC	a
Šebestová	Šebestová	k1gFnSc1	Šebestová
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Mach	Mach	k1gMnSc1	Mach
a	a	k8xC	a
Šebestová	Šebestová	k1gFnSc1	Šebestová
na	na	k7c6	na
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
×	×	k?	×
Mach	Mach	k1gMnSc1	Mach
a	a	k8xC	a
Šebestová	Šebestová	k1gFnSc1	Šebestová
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
)	)	kIx)	)
33	[number]	k4	33
dílů	díl	k1gInPc2	díl
–	–	k?	–
Matylda	Matylda	k1gFnSc1	Matylda
30	[number]	k4	30
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Broučky	Brouček	k1gMnPc7	Brouček
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
×	×	k?	×
Broučci	Brouček	k1gMnPc1	Brouček
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
×	×	k?	×
Broučci	Brouček	k1gMnPc1	Brouček
–	–	k?	–
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
na	na	k7c6	na
pasece	paseka	k1gFnSc6	paseka
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
×	×	k?	×
Broučkova	Broučkův	k2eAgFnSc1d1	Broučkova
rodina	rodina	k1gFnSc1	rodina
<g/>
)	)	kIx)	)
29	[number]	k4	29
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc7	večerníček
s	s	k7c7	s
Médi	méďa	k1gMnPc7	méďa
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
×	×	k?	×
Méďové	méďa	k1gMnPc1	méďa
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Méďové	méďa	k1gMnPc1	méďa
II	II	kA	II
-	-	kIx~	-
Povídání	povídání	k1gNnSc1	povídání
pro	pro	k7c4	pro
Aničku	Anička	k1gFnSc4	Anička
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
27	[number]	k4	27
dílů	díl	k1gInPc2	díl
–	–	k?	–
večerníčky	večerníček	k1gInPc1	večerníček
s	s	k7c7	s
Maxipsem	Maxips	k1gMnSc7	Maxips
Fíkem	fík	k1gInSc7	fík
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
×	×	k?	×
Maxipes	Maxipesa	k1gFnPc2	Maxipesa
Fík	fík	k1gInSc1	fík
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
×	×	k?	×
Maxipes	Maxipes	k1gInSc1	Maxipes
Fík	fík	k1gInSc1	fík
filmuje	filmovat	k5eAaImIp3nS	filmovat
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
×	×	k?	×
Divoké	divoký	k2eAgInPc1d1	divoký
sny	sen	k1gInPc1	sen
Maxipsa	Maxips	k1gMnSc2	Maxips
Fíka	Fíkus	k1gMnSc2	Fíkus
<g/>
)	)	kIx)	)
Nejsou	být	k5eNaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
seriály	seriál	k1gInPc7	seriál
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
českými	český	k2eAgMnPc7d1	český
tvůrci	tvůrce	k1gMnPc7	tvůrce
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
studiích	studie	k1gFnPc6	studie
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
pro	pro	k7c4	pro
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Slimák	slimák	k1gMnSc1	slimák
Maťo	Maťo	k1gMnSc1	Maťo
a	a	k8xC	a
škriatok	škriatok	k1gInSc1	škriatok
Klinček	Klinčka	k1gFnPc2	Klinčka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
dokonce	dokonce	k9	dokonce
100	[number]	k4	100
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
seriál	seriál	k1gInSc1	seriál
...	...	k?	...
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
němý	němý	k2eAgInSc1d1	němý
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
větší	veliký	k2eAgFnSc2d2	veliký
české	český	k2eAgFnSc2d1	Česká
série	série	k1gFnSc2	série
s	s	k7c7	s
Patem	pat	k1gInSc7	pat
a	a	k8xC	a
Matem	mat	k1gInSc7	mat
<g/>
.	.	kIx.	.
</s>
