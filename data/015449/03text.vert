<s>
Hausberk	Hausberk	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Hausberk	Hausberk	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Chomutov	Chomutov	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc1d1
hrad	hrad	k1gInSc1
u	u	k7c2
Chomutova	Chomutov	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hausberk	Hausberk	k1gInSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Zánik	zánik	k1gInSc1
</s>
<s>
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Stavebník	stavebník	k1gMnSc1
</s>
<s>
neznámý	známý	k2eNgMnSc1d1
Další	další	k2eAgMnSc1d1
majitelé	majitel	k1gMnPc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
vrch	vrch	k1gInSc1
Hradiště	Hradiště	k1gNnSc2
<g/>
,	,	kIx,
Maňávka	Maňávka	k1gFnSc1
u	u	k7c2
Českého	český	k2eAgInSc2d1
Krumlova	Krumlov	k1gInSc2
<g/>
,	,	kIx,
Horní	horní	k2eAgFnSc1d1
Planá	Planá	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Pohoří	pohoří	k1gNnSc1
</s>
<s>
Šumava	Šumava	k1gFnSc1
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
940	#num#	k4
m	m	kA
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
4,3	4,3	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
59,71	59,71	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Hausberk	Hausberk	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hausberk	Hausberk	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
38125	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
1367	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hrad	hrad	k1gInSc1
Hausberk	Hausberk	k1gInSc1
stával	stávat	k5eAaImAgInS
na	na	k7c6
pomezí	pomezí	k1gNnSc6
nynějších	nynější	k2eAgInPc2d1
okresů	okres	k1gInPc2
Prachatice	Prachatice	k1gFnPc1
a	a	k8xC
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
mezi	mezi	k7c7
obcí	obec	k1gFnSc7
Želnava	Želnava	k1gFnSc1
a	a	k8xC
vsí	ves	k1gFnSc7
Pernek	Pernky	k1gFnPc2
na	na	k7c6
kopci	kopec	k1gInSc6
Hrad	hrad	k1gInSc1
(	(	kIx(
<g/>
940	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
přísluší	příslušet	k5eAaImIp3nS
ke	k	k7c3
katastrálnímu	katastrální	k2eAgNnSc3d1
území	území	k1gNnSc3
Maňávka	Maňávka	k1gFnSc1
u	u	k7c2
Českého	český	k2eAgInSc2d1
Krumlova	Krumlov	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
území	území	k1gNnSc2
města	město	k1gNnSc2
Horní	horní	k2eAgFnSc1d1
Planá	Planá	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
O	o	k7c6
historii	historie	k1gFnSc6
strážního	strážní	k2eAgInSc2d1
hrádku	hrádek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
kontroloval	kontrolovat	k5eAaImAgInS
horní	horní	k2eAgInSc4d1
tok	tok	k1gInSc4
Vltavy	Vltava	k1gFnSc2
<g/>
,	,	kIx,
toho	ten	k3xDgNnSc2
moc	moc	k6eAd1
nevíme	vědět	k5eNaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archeologický	archeologický	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
datoval	datovat	k5eAaImAgInS
jeho	jeho	k3xOp3gFnSc4
existenci	existence	k1gFnSc4
do	do	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
majitelem	majitel	k1gMnSc7
byl	být	k5eAaImAgMnS
pravděpodobně	pravděpodobně	k6eAd1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Durdík	Durdík	k1gMnSc1
jej	on	k3xPp3gNnSc4
zařazuje	zařazovat	k5eAaImIp3nS
mezi	mezi	k7c4
šumavské	šumavský	k2eAgInPc4d1
horské	horský	k2eAgInPc4d1
hrádky	hrádek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
František	František	k1gMnSc1
Kubů	Kubů	k1gMnSc1
pokusil	pokusit	k5eAaPmAgMnS
ztotožnit	ztotožnit	k5eAaPmF
s	s	k7c7
historicky	historicky	k6eAd1
doloženým	doložený	k2eAgInSc7d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
dosud	dosud	k6eAd1
nelokalizovaným	lokalizovaný	k2eNgInSc7d1
hradem	hrad	k1gInSc7
Waltershausen	Waltershausna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
hrad	hrad	k1gInSc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1360	#num#	k4
založit	založit	k5eAaPmF
probošt	probošt	k1gMnSc1
vyšehradské	vyšehradský	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
Dětřich	Dětřich	k1gMnSc1
z	z	k7c2
Portic	Portice	k1gFnPc2
na	na	k7c4
ochranu	ochrana	k1gFnSc4
okolních	okolní	k2eAgFnPc2d1
proboštových	proboštův	k2eAgFnPc2d1
vsí	ves	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1395	#num#	k4
tyto	tento	k3xDgFnPc4
vsi	ves	k1gFnPc4
získal	získat	k5eAaPmAgInS
klášter	klášter	k1gInSc1
Zlatá	zlatý	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
dříve	dříve	k6eAd2
patřily	patřit	k5eAaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
hrad	hrad	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
známé	známý	k2eAgFnPc1d1
informace	informace	k1gFnPc1
o	o	k7c6
hradu	hrad	k1gInSc6
však	však	k9
této	tento	k3xDgFnSc3
teorii	teorie	k1gFnSc3
neodpovídají	odpovídat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgInSc1d1
strážní	strážní	k2eAgInSc1d1
hrádek	hrádek	k1gInSc1
Hausberk	Hausberk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Hrad	hrad	k1gInSc1
na	na	k7c4
hrady	hrad	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
