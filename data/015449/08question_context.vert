<s>
Hrad	hrad	k1gInSc1
Hausberk	Hausberk	k1gInSc1
stával	stávat	k5eAaImAgInS
na	na	k7c6
pomezí	pomezí	k1gNnSc6
nynějších	nynější	k2eAgInPc2d1
okresů	okres	k1gInPc2
Prachatice	Prachatice	k1gFnPc1
a	a	k8xC
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
mezi	mezi	k7c7
obcí	obec	k1gFnSc7
Želnava	Želnava	k1gFnSc1
a	a	k8xC
vsí	ves	k1gFnSc7
Pernek	Pernky	k1gFnPc2
na	na	k7c6
kopci	kopec	k1gInSc6
Hrad	hrad	k1gInSc1
(	(	kIx(
<g/>
940	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
přísluší	příslušet	k5eAaImIp3nS
ke	k	k7c3
katastrálnímu	katastrální	k2eAgNnSc3d1
území	území	k1gNnSc3
Maňávka	Maňávka	k1gFnSc1
u	u	k7c2
Českého	český	k2eAgInSc2d1
Krumlova	Krumlov	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
území	území	k1gNnSc2
města	město	k1gNnSc2
Horní	horní	k2eAgFnSc1d1
Planá	Planá	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>