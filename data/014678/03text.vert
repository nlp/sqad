<s>
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
</s>
<s>
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
Narození	narození	k1gNnPc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1963	#num#	k4
(	(	kIx(
<g/>
57	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
St	St	kA
Helens	Helens	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1963	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
filmový	filmový	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
režisérem	režisér	k1gMnSc7
několika	několik	k4yIc2
filmů	film	k1gInPc2
série	série	k1gFnSc2
o	o	k7c4
Harry	Harra	k1gFnPc4
Potterovi	Potter	k1gMnSc3
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
Harry	Harr	k1gInPc4
Potter	Pottrum	k1gNnPc2
a	a	k8xC
Fénixův	fénixův	k2eAgInSc1d1
řád	řád	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Harry	Harr	k1gInPc1
Potter	Potter	k1gInSc1
a	a	k8xC
princ	princ	k1gMnSc1
dvojí	dvojí	k4xRgFnSc2
krve	krev	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Harry	Harra	k1gMnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
Relikvie	relikvie	k1gFnSc1
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
část	část	k1gFnSc1
1	#num#	k4
i	i	k8xC
část	část	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gInPc4
další	další	k2eAgInPc4d1
filmy	film	k1gInPc4
patří	patřit	k5eAaImIp3nS
Sex	sex	k1gInSc1
Traffic	Traffice	k1gFnPc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
Tarzanovi	Tarzan	k1gMnSc6
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Fantastická	fantastický	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
a	a	k8xC
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
najít	najít	k5eAaPmF
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Fantastická	fantastický	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
<g/>
:	:	kIx,
Grindelwaldovy	Grindelwaldův	k2eAgInPc1d1
zločiny	zločin	k1gInPc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
David	David	k1gMnSc1
Yates	Yatesa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
112479	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
133913007	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8172	#num#	k4
537X	537X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006100914	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
107492257	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006100914	#num#	k4
</s>
