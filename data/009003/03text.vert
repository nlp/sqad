<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
map	mapa	k1gFnPc2	mapa
spojených	spojený	k2eAgFnPc2d1	spojená
účelem	účel	k1gInSc7	účel
<g/>
,	,	kIx,	,
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
měřítkem	měřítko	k1gNnSc7	měřítko
nebo	nebo	k8xC	nebo
měřítkovou	měřítkový	k2eAgFnSc7d1	měřítková
řadou	řada	k1gFnSc7	řada
<g/>
,	,	kIx,	,
generalizací	generalizace	k1gFnSc7	generalizace
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
systémovými	systémový	k2eAgFnPc7d1	systémová
hledisky	hledisko	k1gNnPc7	hledisko
<g/>
,	,	kIx,	,
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
koncepčně	koncepčně	k6eAd1	koncepčně
kartograficky	kartograficky	k6eAd1	kartograficky
a	a	k8xC	a
polygraficky	polygraficky	k6eAd1	polygraficky
jako	jako	k9	jako
jednotné	jednotný	k2eAgNnSc4d1	jednotné
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
jednotností	jednotnost	k1gFnSc7	jednotnost
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
prostého	prostý	k2eAgInSc2d1	prostý
souboru	soubor	k1gInSc2	soubor
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
atlasů	atlas	k1gInPc2	atlas
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
členění	členění	k1gNnSc1	členění
atlasů	atlas	k1gInPc2	atlas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
obsahu	obsah	k1gInSc2	obsah
(	(	kIx(	(
<g/>
geografické	geografický	k2eAgFnSc2d1	geografická
<g/>
,	,	kIx,	,
topografické	topografický	k2eAgFnSc2d1	topografická
<g/>
,	,	kIx,	,
fotoatlasy	fotoatlasa	k1gFnSc2	fotoatlasa
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
zobrazovaného	zobrazovaný	k2eAgNnSc2d1	zobrazované
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
atlasy	atlas	k1gInPc7	atlas
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
národní	národní	k2eAgInPc1d1	národní
atlasy	atlas	k1gInPc1	atlas
<g/>
,	,	kIx,	,
atlasy	atlas	k1gInPc1	atlas
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
formátu	formát	k1gInSc2	formát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
podoby	podoba	k1gFnSc2	podoba
výstupu	výstup	k1gInSc2	výstup
(	(	kIx(	(
<g/>
tištěné	tištěný	k2eAgFnPc1d1	tištěná
a	a	k8xC	a
digitální	digitální	k2eAgFnPc1d1	digitální
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
znázorňovaného	znázorňovaný	k2eAgNnSc2d1	znázorňovaný
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
aktuální	aktuální	k2eAgInPc1d1	aktuální
<g/>
,	,	kIx,	,
dějepisné	dějepisný	k2eAgInPc1d1	dějepisný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
atlasů	atlas	k1gInPc2	atlas
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Svět	svět	k1gInSc1	svět
antiky	antika	k1gFnSc2	antika
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
atlasy	atlas	k1gInPc1	atlas
začaly	začít	k5eAaPmAgInP	začít
vznikat	vznikat	k5eAaImF	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
autorem	autor	k1gMnSc7	autor
map	mapa	k1gFnPc2	mapa
a	a	k8xC	a
atlasů	atlas	k1gInPc2	atlas
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgMnS	být
kartograf	kartograf	k1gMnSc1	kartograf
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
Klaudios	Klaudios	k1gMnSc1	Klaudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nejstarší	starý	k2eAgMnSc1d3	nejstarší
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
zeměpisný	zeměpisný	k2eAgInSc4d1	zeměpisný
atlas	atlas	k1gInSc4	atlas
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
atlas	atlas	k1gInSc1	atlas
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
obsáhlého	obsáhlý	k2eAgInSc2d1	obsáhlý
spisu	spis	k1gInSc2	spis
Geografiké	Geografiký	k2eAgFnSc2d1	Geografiký
hyfégésis	hyfégésis	k1gFnSc2	hyfégésis
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
mapy	mapa	k1gFnPc1	mapa
tohoto	tento	k3xDgInSc2	tento
atlasu	atlas	k1gInSc2	atlas
mají	mít	k5eAaImIp3nP	mít
severní	severní	k2eAgFnSc4d1	severní
orientaci	orientace	k1gFnSc4	orientace
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
obecně	obecně	k6eAd1	obecně
zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
mapy	mapa	k1gFnPc4	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Islámský	islámský	k2eAgInSc1d1	islámský
svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
islámští	islámský	k2eAgMnPc1d1	islámský
kartografové	kartograf	k1gMnPc1	kartograf
se	se	k3xPyFc4	se
postarali	postarat	k5eAaPmAgMnP	postarat
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
atlasů	atlas	k1gInPc2	atlas
nepatřila	patřit	k5eNaImAgFnS	patřit
mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gFnPc4	jejich
priority	priorita	k1gFnPc4	priorita
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
islámský	islámský	k2eAgInSc1d1	islámský
atlas	atlas	k1gInSc1	atlas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
do	do	k7c2	do
dnešních	dnešní	k2eAgFnPc2d1	dnešní
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
21	[number]	k4	21
map	mapa	k1gFnPc2	mapa
tehdy	tehdy	k6eAd1	tehdy
známého	známý	k2eAgInSc2d1	známý
světa	svět	k1gInSc2	svět
obývaného	obývaný	k2eAgInSc2d1	obývaný
muslimy	muslim	k1gMnPc4	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
al-Balchi	al-Balchi	k6eAd1	al-Balchi
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
kartograf	kartograf	k1gMnSc1	kartograf
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
al-Idrisi	al-Idris	k1gMnPc7	al-Idris
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rovněž	rovněž	k9	rovněž
zabýval	zabývat	k5eAaImAgInS	zabývat
i	i	k9	i
tvorbou	tvorba	k1gFnSc7	tvorba
atlasů	atlas	k1gInPc2	atlas
<g/>
,	,	kIx,	,
věrohodné	věrohodný	k2eAgInPc1d1	věrohodný
důkazy	důkaz	k1gInPc1	důkaz
však	však	k9	však
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
kartografie	kartografie	k1gFnSc1	kartografie
úpadek	úpadek	k1gInSc4	úpadek
následkem	následkem	k7c2	následkem
feudální	feudální	k2eAgFnSc2d1	feudální
rozdrobenosti	rozdrobenost	k1gFnSc2	rozdrobenost
a	a	k8xC	a
scholastického	scholastický	k2eAgNnSc2d1	scholastické
zaměření	zaměření	k1gNnSc2	zaměření
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
tvorby	tvorba	k1gFnSc2	tvorba
map	mapa	k1gFnPc2	mapa
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
námořní	námořní	k2eAgFnSc2d1	námořní
plavby	plavba	k1gFnSc2	plavba
především	především	k9	především
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
tzv.	tzv.	kA	tzv.
portulánové	portulánový	k2eAgFnPc4d1	portulánový
mapy	mapa	k1gFnPc4	mapa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
skládaly	skládat	k5eAaImAgFnP	skládat
z	z	k7c2	z
relativně	relativně	k6eAd1	relativně
úzkých	úzký	k2eAgInPc2d1	úzký
a	a	k8xC	a
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
pásů	pás	k1gInPc2	pás
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
map	mapa	k1gFnPc2	mapa
postupně	postupně	k6eAd1	postupně
vznikaly	vznikat	k5eAaImAgInP	vznikat
první	první	k4xOgInPc1	první
tematické	tematický	k2eAgInPc1d1	tematický
atlasy	atlas	k1gInPc1	atlas
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Medicejský	Medicejský	k2eAgInSc1d1	Medicejský
atlas	atlas	k1gInSc4	atlas
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Katalánském	katalánský	k2eAgInSc6d1	katalánský
atlase	atlas	k1gInSc6	atlas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1375	[number]	k4	1375
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
vyobrazena	vyobrazit	k5eAaPmNgFnS	vyobrazit
i	i	k9	i
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Labe	Labe	k1gNnSc1	Labe
a	a	k8xC	a
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Abraham	Abraham	k1gMnSc1	Abraham
Cresques	Cresques	k1gMnSc1	Cresques
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevem	objev	k1gInSc7	objev
knihtisku	knihtisk	k1gInSc2	knihtisk
byla	být	k5eAaImAgFnS	být
zdokonalena	zdokonalen	k2eAgFnSc1d1	zdokonalena
i	i	k8xC	i
tvorba	tvorba	k1gFnSc1	tvorba
map	mapa	k1gFnPc2	mapa
a	a	k8xC	a
atlasů	atlas	k1gInPc2	atlas
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgFnSc2	první
manufaktury	manufaktura	k1gFnSc2	manufaktura
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
kartografickou	kartografický	k2eAgFnSc4d1	kartografická
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
byla	být	k5eAaImAgFnS	být
manufaktura	manufaktura	k1gFnSc1	manufaktura
benátčana	benátčan	k1gMnSc2	benátčan
Battisty	Battista	k1gMnSc2	Battista
Agnese	Agnese	k1gFnSc2	Agnese
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
přes	přes	k7c4	přes
80	[number]	k4	80
atlasů	atlas	k1gInPc2	atlas
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
portulánových	portulánových	k2eAgFnSc1d1	portulánových
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
bájný	bájný	k2eAgMnSc1d1	bájný
Titán	Titán	k1gMnSc1	Titán
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
podpírá	podpírat	k5eAaImIp3nS	podpírat
globus	globus	k1gInSc4	globus
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgNnSc4	první
spojení	spojení	k1gNnSc4	spojení
Atlanta	Atlant	k1gMnSc2	Atlant
se	s	k7c7	s
zeměpisným	zeměpisný	k2eAgInSc7d1	zeměpisný
atlasem	atlas	k1gInSc7	atlas
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
obrazu	obraz	k1gInSc2	obraz
získal	získat	k5eAaPmAgInS	získat
zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
atlas	atlas	k1gInSc1	atlas
své	svůj	k3xOyFgNnSc4	svůj
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
renesance	renesance	k1gFnSc2	renesance
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
mapová	mapový	k2eAgFnSc1d1	mapová
tvorba	tvorba	k1gFnSc1	tvorba
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
kartografie	kartografie	k1gFnSc2	kartografie
se	se	k3xPyFc4	se
postaraly	postarat	k5eAaPmAgFnP	postarat
nejen	nejen	k6eAd1	nejen
nové	nový	k2eAgFnPc1d1	nová
metody	metoda	k1gFnPc1	metoda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
zámořské	zámořský	k2eAgInPc4d1	zámořský
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vznikaly	vznikat	k5eAaImAgInP	vznikat
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgInPc1d1	velký
atlasy	atlas	k1gInPc1	atlas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
klenotům	klenot	k1gInPc3	klenot
kartografické	kartografický	k2eAgFnSc2d1	kartografická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
tvůrcem	tvůrce	k1gMnSc7	tvůrce
těchto	tento	k3xDgInPc2	tento
atlasů	atlas	k1gInPc2	atlas
byl	být	k5eAaImAgMnS	být
holandský	holandský	k2eAgMnSc1d1	holandský
obchodník	obchodník	k1gMnSc1	obchodník
Abraham	Abraham	k1gMnSc1	Abraham
Ortelius	Ortelius	k1gMnSc1	Ortelius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sestavil	sestavit	k5eAaPmAgInS	sestavit
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
map	mapa	k1gFnPc2	mapa
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
první	první	k4xOgInSc4	první
atlas	atlas	k1gInSc4	atlas
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
atlas	atlas	k1gInSc1	atlas
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Theatrum	Theatrum	k1gNnSc1	Theatrum
orbis	orbis	k1gInSc4	orbis
terrarum	terrarum	k1gNnSc1	terrarum
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
70	[number]	k4	70
map	mapa	k1gFnPc2	mapa
na	na	k7c6	na
53	[number]	k4	53
listech	list	k1gInPc6	list
a	a	k8xC	a
seznam	seznam	k1gInSc4	seznam
autorů	autor	k1gMnPc2	autor
těchto	tento	k3xDgFnPc2	tento
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
několika	několik	k4yIc2	několik
reedicí	reedice	k1gFnPc2	reedice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
poslední	poslední	k2eAgFnSc1d1	poslední
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1612	[number]	k4	1612
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
již	již	k6eAd1	již
128	[number]	k4	128
obecně	obecně	k6eAd1	obecně
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
map	mapa	k1gFnPc2	mapa
a	a	k8xC	a
38	[number]	k4	38
map	mapa	k1gFnPc2	mapa
historických	historický	k2eAgFnPc2d1	historická
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
i	i	k9	i
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
první	první	k4xOgInSc4	první
dějepisný	dějepisný	k2eAgInSc4d1	dějepisný
atlas	atlas	k1gInSc4	atlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
kartografem	kartograf	k1gMnSc7	kartograf
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgMnS	být
Gerhard	Gerhard	k1gMnSc1	Gerhard
Mercator	Mercator	k1gMnSc1	Mercator
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
celoživotní	celoživotní	k2eAgNnSc1d1	celoživotní
dílo	dílo	k1gNnSc1	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Atlas	Atlas	k1gInSc1	Atlas
sive	sive	k1gFnPc2	sive
cosmographicae	cosmographicae	k1gNnSc1	cosmographicae
meditationes	meditationes	k1gMnSc1	meditationes
de	de	k?	de
fabrica	fabrica	k1gMnSc1	fabrica
mundi	mund	k1gMnPc1	mund
et	et	k?	et
fabricati	fabricat	k5eAaImF	fabricat
figura	figura	k1gFnSc1	figura
(	(	kIx(	(
<g/>
Atlas	Atlas	k1gInSc1	Atlas
neboli	neboli	k8xC	neboli
kosmografické	kosmografický	k2eAgFnPc1d1	kosmografický
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
světa	svět	k1gInSc2	svět
a	a	k8xC	a
o	o	k7c6	o
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tím	ten	k3xDgNnSc7	ten
nabyl	nabýt	k5eAaPmAgMnS	nabýt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
názvu	název	k1gInSc6	název
použit	použít	k5eAaPmNgInS	použít
pojem	pojem	k1gInSc1	pojem
atlas	atlas	k1gInSc1	atlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgMnPc7d1	další
tvůrci	tvůrce	k1gMnPc7	tvůrce
atlasů	atlas	k1gInPc2	atlas
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byli	být	k5eAaImAgMnP	být
Jodocus	Jodocus	k1gMnSc1	Jodocus
Hondius	Hondius	k1gMnSc1	Hondius
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Janssonius	Janssonius	k1gInSc1	Janssonius
a	a	k8xC	a
Willem	Will	k1gInSc7	Will
Blaeu	Blaeus	k1gInSc2	Blaeus
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Willem	Willo	k1gNnSc7	Willo
Janszoon	Janszoon	k1gMnSc1	Janszoon
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
po	po	k7c6	po
Blaeuově	Blaeuův	k2eAgFnSc6d1	Blaeuův
smrti	smrt	k1gFnSc6	smrt
vydali	vydat	k5eAaPmAgMnP	vydat
superatlas	superatlas	k1gInSc4	superatlas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
2115	[number]	k4	2115
mapových	mapový	k2eAgInPc2d1	mapový
listů	list	k1gInPc2	list
a	a	k8xC	a
vyšel	vyjít	k5eAaPmAgMnS	vyjít
v	v	k7c6	v
46	[number]	k4	46
svazcích	svazek	k1gInPc6	svazek
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
jazykových	jazykový	k2eAgFnPc6d1	jazyková
mutacích	mutace	k1gFnPc6	mutace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
atlasů	atlas	k1gInPc2	atlas
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stává	stávat	k5eAaImIp3nS	stávat
nedělitelnou	dělitelný	k2eNgFnSc7d1	nedělitelná
součástí	součást	k1gFnSc7	součást
kartografické	kartografický	k2eAgFnSc2d1	kartografická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
první	první	k4xOgInPc1	první
atlasy	atlas	k1gInPc1	atlas
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
tematické	tematický	k2eAgInPc1d1	tematický
atlasy	atlas	k1gInPc1	atlas
a	a	k8xC	a
atlasy	atlas	k1gInPc1	atlas
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
atlasové	atlasový	k2eAgFnSc2d1	Atlasová
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
ruští	ruský	k2eAgMnPc1d1	ruský
kartografové	kartograf	k1gMnPc1	kartograf
<g/>
,	,	kIx,	,
především	především	k9	především
autor	autor	k1gMnSc1	autor
Atlasu	Atlas	k1gInSc2	Atlas
vserossijsoj	vserossijsoj	k1gInSc1	vserossijsoj
imperii	imperie	k1gFnSc6	imperie
Ivan	Ivana	k1gFnPc2	Ivana
Kirilov	Kirilov	k1gInSc1	Kirilov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Michail	Michail	k1gMnSc1	Michail
Lomonosov	Lomonosov	k1gInSc1	Lomonosov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
Atlas	Atlas	k1gInSc1	Atlas
Rossijskoj	Rossijskoj	k1gInSc1	Rossijskoj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
dostává	dostávat	k5eAaImIp3nS	dostávat
i	i	k9	i
německá	německý	k2eAgFnSc1d1	německá
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
národní	národní	k2eAgInSc1d1	národní
atlas	atlas	k1gInSc1	atlas
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
atlasů	atlas	k1gInPc2	atlas
zabývají	zabývat	k5eAaImIp3nP	zabývat
prakticky	prakticky	k6eAd1	prakticky
všechna	všechen	k3xTgNnPc4	všechen
významná	významný	k2eAgNnPc4d1	významné
kartografická	kartografický	k2eAgNnPc4d1	kartografické
nakladatelství	nakladatelství	k1gNnPc4	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejčastějším	častý	k2eAgInSc7d3	nejčastější
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
autoatlas	autoatlas	k1gInSc4	autoatlas
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
zažívají	zažívat	k5eAaImIp3nP	zažívat
digitální	digitální	k2eAgInPc1d1	digitální
atlasy	atlas	k1gInPc1	atlas
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
publikovány	publikovat	k5eAaBmNgFnP	publikovat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
CD-ROM	CD-ROM	k1gFnSc6	CD-ROM
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
tuto	tento	k3xDgFnSc4	tento
úlohu	úloha	k1gFnSc4	úloha
převzal	převzít	k5eAaPmAgInS	převzít
především	především	k9	především
internet	internet	k1gInSc1	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ČAPEK	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
373	[number]	k4	373
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
25153	[number]	k4	25153
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Václav	Václav	k1gMnSc1	Václav
Drápela	Drápela	k1gMnSc1	Drápela
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
kartografie	kartografie	k1gFnPc1	kartografie
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
multimediální	multimediální	k2eAgInSc1d1	multimediální
on-line	onin	k1gInSc5	on-lin
učebnice	učebnice	k1gFnPc4	učebnice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Hojovec	Hojovec	k1gMnSc1	Hojovec
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
Geodetický	geodetický	k2eAgInSc1d1	geodetický
a	a	k8xC	a
kartografický	kartografický	k2eAgInSc1d1	kartografický
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
</s>
</p>
<p>
<s>
Kartografie	kartografie	k1gFnSc1	kartografie
</s>
</p>
<p>
<s>
Glóbus	glóbus	k1gInSc1	glóbus
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
atlas	atlas	k1gInSc4	atlas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
