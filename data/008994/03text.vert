<p>
<s>
Hlasivky	hlasivka	k1gFnPc1	hlasivka
jsou	být	k5eAaImIp3nP	být
párový	párový	k2eAgInSc4d1	párový
orgán	orgán	k1gInSc4	orgán
v	v	k7c6	v
hrtanu	hrtan	k1gInSc6	hrtan
umožňující	umožňující	k2eAgInSc4d1	umožňující
vznik	vznik	k1gInSc4	vznik
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Strukturu	struktura	k1gFnSc4	struktura
hlasivky	hlasivka	k1gFnSc2	hlasivka
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlasivkový	hlasivkový	k2eAgInSc1d1	hlasivkový
sval	sval	k1gInSc1	sval
<g/>
,	,	kIx,	,
hlasový	hlasový	k2eAgInSc1d1	hlasový
vaz	vaz	k1gInSc1	vaz
a	a	k8xC	a
sliznice	sliznice	k1gFnSc1	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Hlasivky	hlasivka	k1gFnPc1	hlasivka
jsou	být	k5eAaImIp3nP	být
vepředu	vepředu	k6eAd1	vepředu
přichyceny	přichytit	k5eAaPmNgFnP	přichytit
ke	k	k7c3	k
štítné	štítný	k2eAgFnSc3d1	štítná
chrupavce	chrupavka	k1gFnSc3	chrupavka
a	a	k8xC	a
vzadu	vzadu	k6eAd1	vzadu
k	k	k7c3	k
hlasivkové	hlasivkový	k2eAgFnSc3d1	hlasivková
(	(	kIx(	(
<g/>
arytenoidní	arytenoidní	k2eAgFnSc3d1	arytenoidní
<g/>
)	)	kIx)	)
chrupavce	chrupavka	k1gFnSc3	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
prochází	procházet	k5eAaImIp3nS	procházet
hlasivkovou	hlasivkový	k2eAgFnSc7d1	hlasivková
štěrbinou	štěrbina	k1gFnSc7	štěrbina
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
rozechvívat	rozechvívat	k5eAaImF	rozechvívat
napjaté	napjatý	k2eAgFnPc4d1	napjatá
hlasivky	hlasivka	k1gFnPc4	hlasivka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
hlas	hlas	k1gInSc1	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Lékařský	lékařský	k2eAgInSc1d1	lékařský
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
zdravím	zdraví	k1gNnSc7	zdraví
hlasivek	hlasivka	k1gFnPc2	hlasivka
a	a	k8xC	a
hlasu	hlas	k1gInSc2	hlas
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
foniatrie	foniatrie	k1gFnSc1	foniatrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uložení	uložení	k1gNnSc1	uložení
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
jsou	být	k5eAaImIp3nP	být
hlasivky	hlasivka	k1gFnPc1	hlasivka
vidět	vidět	k5eAaImF	vidět
hned	hned	k6eAd1	hned
za	za	k7c7	za
hrtanovou	hrtanový	k2eAgFnSc7d1	hrtanová
příklopkou	příklopka	k1gFnSc7	příklopka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
chrání	chránit	k5eAaImIp3nS	chránit
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Hlasivky	hlasivka	k1gFnPc1	hlasivka
se	se	k3xPyFc4	se
napínají	napínat	k5eAaImIp3nP	napínat
mezi	mezi	k7c7	mezi
hlasivkovým	hlasivkový	k2eAgInSc7d1	hlasivkový
výběžkem	výběžek	k1gInSc7	výběžek
arytenoidní	arytenoidní	k2eAgFnSc2d1	arytenoidní
chrupavky	chrupavka	k1gFnSc2	chrupavka
a	a	k8xC	a
zadním	zadní	k2eAgInSc7d1	zadní
okrajem	okraj	k1gInSc7	okraj
štítné	štítný	k2eAgFnSc2d1	štítná
chrupavky	chrupavka	k1gFnSc2	chrupavka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Hlasivky	hlasivka	k1gFnPc1	hlasivka
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
pružné	pružný	k2eAgInPc1d1	pružný
-	-	kIx~	-
hlasový	hlasový	k2eAgInSc1d1	hlasový
vaz	vaz	k1gInSc1	vaz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
zesílená	zesílený	k2eAgFnSc1d1	zesílená
fibroelastická	fibroelastický	k2eAgFnSc1d1	fibroelastický
membrána	membrána	k1gFnSc1	membrána
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
elastických	elastický	k2eAgNnPc2d1	elastické
vláken	vlákno	k1gNnPc2	vlákno
a	a	k8xC	a
hlasivkový	hlasivkový	k2eAgInSc1d1	hlasivkový
výběžek	výběžek	k1gInSc1	výběžek
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
elastickou	elastický	k2eAgFnSc7d1	elastická
chrupavkou	chrupavka	k1gFnSc7	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Hlasový	hlasový	k2eAgInSc1d1	hlasový
vaz	vaz	k1gInSc1	vaz
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
opora	opora	k1gFnSc1	opora
měkčí	měkký	k2eAgFnSc2d2	měkčí
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgNnSc4d1	přirozené
napětí	napětí	k1gNnSc4	napětí
vazů	vaz	k1gInPc2	vaz
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
vůlí	vůle	k1gFnSc7	vůle
-	-	kIx~	-
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
hlasivkovému	hlasivkový	k2eAgInSc3d1	hlasivkový
vazu	vaz	k1gInSc3	vaz
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
příčně	příčně	k6eAd1	příčně
pruhovaný	pruhovaný	k2eAgInSc4d1	pruhovaný
hlasivkový	hlasivkový	k2eAgInSc4d1	hlasivkový
sval	sval	k1gInSc4	sval
(	(	kIx(	(
<g/>
musculus	musculus	k1gInSc1	musculus
vocalis	vocalis	k1gFnSc2	vocalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
vazy	vaz	k1gInPc4	vaz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlasivková	hlasivkový	k2eAgFnSc1d1	hlasivková
štěrbina	štěrbina	k1gFnSc1	štěrbina
neboli	neboli	k8xC	neboli
glottis	glottis	k1gFnSc1	glottis
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
rima	rim	k2eAgFnSc1d1	rim
glottidis	glottidis	k1gFnSc1	glottidis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
tvar	tvar	k1gInSc4	tvar
protáhlého	protáhlý	k2eAgMnSc2d1	protáhlý
<g/>
,	,	kIx,	,
rovnoramenného	rovnoramenný	k2eAgInSc2d1	rovnoramenný
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
-	-	kIx~	-
při	při	k7c6	při
fonaci	fonace	k1gFnSc6	fonace
však	však	k9	však
napětí	napětí	k1gNnSc1	napětí
svalů	sval	k1gInPc2	sval
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
hlasivky	hlasivka	k1gFnPc4	hlasivka
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
hrtan	hrtan	k1gInSc1	hrtan
</s>
</p>
<p>
<s>
syrinx	syrinx	k1gInSc1	syrinx
</s>
</p>
<p>
<s>
chrapot	chrapot	k1gInSc1	chrapot
</s>
</p>
<p>
<s>
šeptání	šeptání	k1gNnSc1	šeptání
</s>
</p>
