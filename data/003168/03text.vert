<s>
Membranofon	Membranofon	k1gInSc1	Membranofon
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vydává	vydávat	k5eAaImIp3nS	vydávat
zvuk	zvuk	k1gInSc4	zvuk
kmitáním	kmitání	k1gNnSc7	kmitání
napjaté	napjatý	k2eAgFnSc2d1	napjatá
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
typy	typ	k1gInPc1	typ
bubnů	buben	k1gInPc2	buben
patří	patřit	k5eAaImIp3nP	patřit
právě	právě	k9	právě
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
