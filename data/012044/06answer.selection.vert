<s>
Osobní	osobní	k2eAgInSc1d1	osobní
počítač	počítač	k1gInSc1	počítač
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
personal	personat	k5eAaPmAgInS	personat
computer	computer	k1gInSc1	computer
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
PC	PC	kA	PC
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
pí	pí	k1gNnSc1	pí
sí	sí	k?	sí
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
též	též	k9	též
pécéčko	pécéčko	k?	pécéčko
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
také	také	k9	také
osobní	osobní	k2eAgInSc4d1	osobní
mikropočítač	mikropočítač	k1gInSc4	mikropočítač
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
použití	použití	k1gNnSc2	použití
jednotlivcem	jednotlivec	k1gMnSc7	jednotlivec
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
serverů	server	k1gInPc2	server
a	a	k8xC	a
sálových	sálový	k2eAgInPc2d1	sálový
počítačů	počítač	k1gInPc2	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
