<p>
<s>
Osobní	osobní	k2eAgInSc1d1	osobní
počítač	počítač	k1gInSc1	počítač
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
personal	personat	k5eAaImAgInS	personat
computer	computer	k1gInSc1	computer
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
PC	PC	kA	PC
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
pí	pí	k1gNnSc1	pí
sí	sí	k?	sí
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
též	též	k9	též
pécéčko	pécéčko	k?	pécéčko
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
také	také	k9	také
osobní	osobní	k2eAgInSc4d1	osobní
mikropočítač	mikropočítač	k1gInSc4	mikropočítač
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
použití	použití	k1gNnSc2	použití
jednotlivcem	jednotlivec	k1gMnSc7	jednotlivec
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
serverů	server	k1gInPc2	server
a	a	k8xC	a
sálových	sálový	k2eAgInPc2d1	sálový
počítačů	počítač	k1gInPc2	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
hardwarovou	hardwarový	k2eAgFnSc7d1	hardwarová
architekturou	architektura	k1gFnSc7	architektura
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
je	být	k5eAaImIp3nS	být
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilní	kompatibilní	k2eAgFnPc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejích	její	k3xOp3gInPc6	její
počátcích	počátek	k1gInPc6	počátek
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
jednoúlohové	jednoúlohový	k2eAgInPc1d1	jednoúlohový
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
DOS	DOS	kA	DOS
(	(	kIx(	(
<g/>
MS-DOS	MS-DOS	k1gMnSc1	MS-DOS
<g/>
,	,	kIx,	,
DR-DOS	DR-DOS	k1gMnSc1	DR-DOS
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
též	též	k9	též
FreeDOS	FreeDOS	k1gFnSc1	FreeDOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
víceúlohovými	víceúlohův	k2eAgInPc7d1	víceúlohův
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgInPc7d1	současný
nejrozšířenějšími	rozšířený	k2eAgInPc7d3	nejrozšířenější
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
na	na	k7c6	na
osobních	osobní	k2eAgInPc6d1	osobní
počítačích	počítač	k1gInPc6	počítač
jsou	být	k5eAaImIp3nP	být
systémy	systém	k1gInPc1	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
a	a	k8xC	a
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
pak	pak	k6eAd1	pak
také	také	k9	také
unixové	unixový	k2eAgInPc1d1	unixový
systémy	systém	k1gInPc1	systém
Apple	Apple	kA	Apple
macOS	macOS	k?	macOS
a	a	k8xC	a
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
uvedly	uvést	k5eAaPmAgInP	uvést
první	první	k4xOgInPc1	první
osmibitové	osmibitový	k2eAgInPc1d1	osmibitový
osobní	osobní	k2eAgInPc1d1	osobní
počítače	počítač	k1gInPc1	počítač
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
systémy	systém	k1gInPc1	systém
technologicky	technologicky	k6eAd1	technologicky
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
prvním	první	k4xOgMnSc6	první
mikropočítačům	mikropočítač	k1gInPc3	mikropočítač
Apple	Apple	kA	Apple
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
společnost	společnost	k1gFnSc1	společnost
Hewlett-Packard	Hewlett-Packard	k1gInSc1	Hewlett-Packard
či	či	k8xC	či
Texas	Texas	k1gInSc1	Texas
Instrument	instrumentum	k1gNnPc2	instrumentum
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
další	další	k2eAgNnSc1d1	další
zlomové	zlomový	k2eAgNnSc1d1	zlomové
datum	datum	k1gNnSc1	datum
je	být	k5eAaImIp3nS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
datum	datum	k1gNnSc1	datum
kdesi	kdesi	k6eAd1	kdesi
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
Personal	Personal	k1gFnSc2	Personal
Computing	Computing	k1gInSc1	Computing
Magazine	Magazin	k1gInSc5	Magazin
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
počítače	počítač	k1gInSc2	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
(	(	kIx(	(
<g/>
IBM	IBM	kA	IBM
5150	[number]	k4	5150
<g/>
)	)	kIx)	)
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1981	[number]	k4	1981
se	se	k3xPyFc4	se
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
označení	označení	k1gNnSc1	označení
PC	PC	kA	PC
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Personal	Personal	k1gFnSc1	Personal
computer	computer	k1gInSc1	computer
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
86	[number]	k4	86
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
architekturou	architektura	k1gFnSc7	architektura
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
komponentami	komponenta	k1gFnPc7	komponenta
a	a	k8xC	a
programovým	programový	k2eAgNnSc7d1	programové
vybavením	vybavení	k1gNnSc7	vybavení
slučitelný	slučitelný	k2eAgMnSc1d1	slučitelný
<g/>
)	)	kIx)	)
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
modelem	model	k1gInSc7	model
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
probíhal	probíhat	k5eAaImAgMnS	probíhat
velmi	velmi	k6eAd1	velmi
překotným	překotný	k2eAgInPc3d1	překotný
ba	ba	k9	ba
až	až	k9	až
hektickým	hektický	k2eAgInSc7d1	hektický
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
počítačová	počítačový	k2eAgFnSc1d1	počítačová
platforma	platforma	k1gFnSc1	platforma
nakonec	nakonec	k6eAd1	nakonec
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
de	de	k?	de
facto	facto	k1gNnSc1	facto
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
standardem	standard	k1gInSc7	standard
bylo	být	k5eAaImAgNnS	být
dáno	dán	k2eAgNnSc1d1	dáno
shodou	shoda	k1gFnSc7	shoda
příznivých	příznivý	k2eAgFnPc2d1	příznivá
náhod	náhoda	k1gFnPc2	náhoda
a	a	k8xC	a
vynikající	vynikající	k2eAgFnSc7d1	vynikající
marketingovou	marketingový	k2eAgFnSc7d1	marketingová
prací	práce	k1gFnSc7	práce
zainteresovaných	zainteresovaný	k2eAgFnPc2d1	zainteresovaná
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nebylo	být	k5eNaImAgNnS	být
ještě	ještě	k9	ještě
vůbec	vůbec	k9	vůbec
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
směrem	směr	k1gInSc7	směr
půjde	jít	k5eAaImIp3nS	jít
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
systémy	systém	k1gInPc1	systém
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
technicky	technicky	k6eAd1	technicky
dokonalejší	dokonalý	k2eAgFnSc1d2	dokonalejší
než	než	k8xS	než
IBM	IBM	kA	IBM
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
však	však	k9	však
sehrál	sehrát	k5eAaPmAgInS	sehrát
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
zvolila	zvolit	k5eAaPmAgFnS	zvolit
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
třetím	třetí	k4xOgMnSc6	třetí
výrobcům	výrobce	k1gMnPc3	výrobce
vyrábět	vyrábět	k5eAaImF	vyrábět
komponenty	komponent	k1gInPc4	komponent
pro	pro	k7c4	pro
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
drahého	drahý	k2eAgInSc2d1	drahý
značkového	značkový	k2eAgInSc2d1	značkový
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
razila	razit	k5eAaImAgFnS	razit
firma	firma	k1gFnSc1	firma
Apple	Apple	kA	Apple
nakonec	nakonec	k6eAd1	nakonec
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
obrovské	obrovský	k2eAgFnSc3d1	obrovská
konkurenci	konkurence	k1gFnSc3	konkurence
uvnitř	uvnitř	k7c2	uvnitř
trhu	trh	k1gInSc2	trh
komponent	komponenta	k1gFnPc2	komponenta
pro	pro	k7c4	pro
potomky	potomek	k1gMnPc4	potomek
IBM	IBM	kA	IBM
PC	PC	kA	PC
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
nakonec	nakonec	k6eAd1	nakonec
její	její	k3xOp3gInPc4	její
výrobky	výrobek	k1gInPc4	výrobek
zaostávat	zaostávat	k5eAaImF	zaostávat
i	i	k9	i
po	po	k7c6	po
technické	technický	k2eAgFnSc6d1	technická
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
srdcem	srdce	k1gNnSc7	srdce
počítače	počítač	k1gInSc2	počítač
procesor	procesor	k1gInSc4	procesor
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
mikropočítač	mikropočítač	k1gInSc1	mikropočítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
model	model	k1gInSc1	model
IBM	IBM	kA	IBM
PC	PC	kA	PC
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
Intel	Intel	kA	Intel
8088	[number]	k4	8088
<g/>
,	,	kIx,	,
pracujícím	pracující	k1gMnSc7	pracující
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
4,77	[number]	k4	4,77
MHz	Mhz	kA	Mhz
a	a	k8xC	a
s	s	k7c7	s
operační	operační	k2eAgFnSc7d1	operační
pamětí	paměť	k1gFnSc7	paměť
RAM	RAM	kA	RAM
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
16	[number]	k4	16
nebo	nebo	k8xC	nebo
64	[number]	k4	64
kB	kb	kA	kb
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
byla	být	k5eAaImAgFnS	být
rozšiřitelná	rozšiřitelný	k2eAgFnSc1d1	rozšiřitelná
na	na	k7c4	na
256	[number]	k4	256
KByte	KByt	k1gInSc5	KByt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
dat	datum	k1gNnPc2	datum
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
kazetový	kazetový	k2eAgInSc1d1	kazetový
magnetofon	magnetofon	k1gInSc1	magnetofon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
5,25	[number]	k4	5,25
<g/>
"	"	kIx"	"
disketa	disketa	k1gFnSc1	disketa
<g/>
.	.	kIx.	.
</s>
<s>
Sběrnice	sběrnice	k1gFnSc1	sběrnice
toho	ten	k3xDgInSc2	ten
modelu	model	k1gInSc2	model
byla	být	k5eAaImAgFnS	být
osmibitová	osmibitový	k2eAgFnSc1d1	osmibitová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
vylepšení	vylepšení	k1gNnSc2	vylepšení
a	a	k8xC	a
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
obdobných	obdobný	k2eAgInPc2d1	obdobný
a	a	k8xC	a
navazujících	navazující	k2eAgInPc2d1	navazující
výrobků	výrobek	k1gInPc2	výrobek
jiných	jiný	k2eAgFnPc2d1	jiná
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k8xC	jako
klony	klon	k1gInPc1	klon
IBM	IBM	kA	IBM
PC	PC	kA	PC
nebo	nebo	k8xC	nebo
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilní	kompatibilní	k2eAgInPc4d1	kompatibilní
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
dodávány	dodávat	k5eAaImNgInP	dodávat
první	první	k4xOgInPc1	první
pevné	pevný	k2eAgInPc1d1	pevný
disky	disk	k1gInPc1	disk
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
10	[number]	k4	10
Mbyte	Mbyt	k1gInSc5	Mbyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
první	první	k4xOgInSc1	první
model	model	k1gInSc1	model
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
80286	[number]	k4	80286
(	(	kIx(	(
<g/>
IBM	IBM	kA	IBM
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
AT	AT	kA	AT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
běžící	běžící	k2eAgInSc4d1	běžící
na	na	k7c4	na
6	[number]	k4	6
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
16	[number]	k4	16
<g/>
bitovou	bitový	k2eAgFnSc4d1	bitová
sběrnici	sběrnice	k1gFnSc4	sběrnice
<g/>
,	,	kIx,	,
20	[number]	k4	20
MB	MB	kA	MB
HDD	HDD	kA	HDD
a	a	k8xC	a
adresovatelnou	adresovatelný	k2eAgFnSc7d1	adresovatelná
RAM	RAM	kA	RAM
až	až	k8xS	až
16	[number]	k4	16
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
Rychlejší	rychlý	k2eAgInSc4d2	rychlejší
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
běžící	běžící	k2eAgInSc4d1	běžící
na	na	k7c4	na
8	[number]	k4	8
MHz	Mhz	kA	Mhz
s	s	k7c7	s
30	[number]	k4	30
MB	MB	kA	MB
pevným	pevný	k2eAgInSc7d1	pevný
diskem	disk	k1gInSc7	disk
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byl	být	k5eAaImAgMnS	být
uveden	uveden	k2eAgMnSc1d1	uveden
nástupce	nástupce	k1gMnSc1	nástupce
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
80386	[number]	k4	80386
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
32	[number]	k4	32
<g/>
-bitový	itový	k2eAgInSc4d1	-bitový
procesor	procesor	k1gInSc4	procesor
<g/>
,	,	kIx,	,
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
mezi	mezi	k7c4	mezi
12	[number]	k4	12
MHz	Mhz	kA	Mhz
a	a	k8xC	a
40	[number]	k4	40
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Procesor	procesor	k1gInSc1	procesor
386SX	[number]	k4	386SX
byl	být	k5eAaImAgInS	být
oproti	oproti	k7c3	oproti
386DX	[number]	k4	386DX
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
verze	verze	k1gFnSc2	verze
i	i	k9	i
<g/>
386	[number]	k4	386
<g/>
SL	SL	kA	SL
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
energeticky	energeticky	k6eAd1	energeticky
efektivní	efektivní	k2eAgNnSc4d1	efektivní
provedení	provedení	k1gNnSc4	provedení
pro	pro	k7c4	pro
přenosné	přenosný	k2eAgInPc4d1	přenosný
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Disketové	disketový	k2eAgFnPc1d1	disketová
mechaniky	mechanika	k1gFnPc1	mechanika
se	se	k3xPyFc4	se
zmenšily	zmenšit	k5eAaPmAgFnP	zmenšit
na	na	k7c4	na
formát	formát	k1gInSc4	formát
5.25	[number]	k4	5.25
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
diskety	disketa	k1gFnPc4	disketa
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
360	[number]	k4	360
kByte	kByte	k5eAaPmIp2nP	kByte
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
1.2	[number]	k4	1.2
MB	MB	kA	MB
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
náhradě	náhrada	k1gFnSc3	náhrada
mechanikami	mechanika	k1gFnPc7	mechanika
pro	pro	k7c4	pro
3.5	[number]	k4	3.5
<g/>
"	"	kIx"	"
diskety	disketa	k1gFnSc2	disketa
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
1.44	[number]	k4	1.44
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
původních	původní	k2eAgInPc2d1	původní
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
a	a	k8xC	a
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgMnPc2d1	kompatibilní
býval	bývat	k5eAaImAgMnS	bývat
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
MS-DOS	MS-DOS	k1gMnPc2	MS-DOS
nebo	nebo	k8xC	nebo
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
klonů	klon	k1gInPc2	klon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
PC-DOS	PC-DOS	k1gMnSc1	PC-DOS
<g/>
,	,	kIx,	,
DR-DOS	DR-DOS	k1gMnSc1	DR-DOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
běžel	běžet	k5eAaImAgMnS	běžet
v	v	k7c6	v
textovém	textový	k2eAgInSc6d1	textový
režimu	režim	k1gInSc6	režim
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
max	max	kA	max
<g/>
.	.	kIx.	.
80	[number]	k4	80
znaky	znak	k1gInPc4	znak
na	na	k7c6	na
25	[number]	k4	25
řádcích	řádek	k1gInPc6	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
začalo	začít	k5eAaPmAgNnS	začít
u	u	k7c2	u
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
pozvolna	pozvolna	k6eAd1	pozvolna
prosazovat	prosazovat	k5eAaImF	prosazovat
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
GUI	GUI	kA	GUI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
Apple	Apple	kA	Apple
Macintosh	Macintosh	kA	Macintosh
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgMnPc2d1	kompatibilní
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
MS	MS	kA	MS
Windows	Windows	kA	Windows
3.0	[number]	k4	3.0
a	a	k8xC	a
novějších	nový	k2eAgFnPc6d2	novější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
dekádě	dekáda	k1gFnSc6	dekáda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
vývoji	vývoj	k1gInSc3	vývoj
hardwarových	hardwarový	k2eAgFnPc2d1	hardwarová
komponent	komponenta	k1gFnPc2	komponenta
i	i	k8xC	i
softwarových	softwarový	k2eAgFnPc2d1	softwarová
komponent	komponenta	k1gFnPc2	komponenta
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
Mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
80386	[number]	k4	80386
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
výrazně	výrazně	k6eAd1	výrazně
rychlejšími	rychlý	k2eAgFnPc7d2	rychlejší
80486	[number]	k4	80486
<g/>
,	,	kIx,	,
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
matematický	matematický	k2eAgInSc1d1	matematický
koprocesor	koprocesor	k1gInSc1	koprocesor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
.	.	kIx.	.
486	[number]	k4	486
byly	být	k5eAaImAgInP	být
32	[number]	k4	32
<g/>
-bitové	itové	k2eAgInPc7d1	-bitové
procesory	procesor	k1gInPc7	procesor
<g/>
,	,	kIx,	,
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
mezi	mezi	k7c4	mezi
16	[number]	k4	16
MHz	Mhz	kA	Mhz
až	až	k9	až
100	[number]	k4	100
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
procesoru	procesor	k1gInSc2	procesor
386	[number]	k4	386
<g/>
,	,	kIx,	,
také	také	k9	také
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
výkonnější	výkonný	k2eAgFnPc1d2	výkonnější
varianty	varianta	k1gFnPc1	varianta
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xS	jako
DX	DX	kA	DX
a	a	k8xC	a
energeticky	energeticky	k6eAd1	energeticky
efektivní	efektivní	k2eAgNnSc4d1	efektivní
provedení	provedení	k1gNnSc4	provedení
jako	jako	k8xC	jako
SL.	SL.	k1gFnSc4	SL.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
přišla	přijít	k5eAaPmAgFnS	přijít
architektura	architektura	k1gFnSc1	architektura
Intel	Intel	kA	Intel
Pentium	Pentium	kA	Pentium
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přinesla	přinést	k5eAaPmAgFnS	přinést
opět	opět	k6eAd1	opět
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zrychlení	zrychlení	k1gNnSc4	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
taktovací	taktovací	k2eAgFnPc1d1	taktovací
frekvence	frekvence	k1gFnPc1	frekvence
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
okolo	okolo	k7c2	okolo
10-20	[number]	k4	10-20
MHz	Mhz	kA	Mhz
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
okolo	okolo	k7c2	okolo
100-200	[number]	k4	100-200
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Monochromatické	monochromatický	k2eAgInPc1d1	monochromatický
monitory	monitor	k1gInPc1	monitor
byly	být	k5eAaImAgInP	být
zcela	zcela	k6eAd1	zcela
vytlačeny	vytlačen	k2eAgInPc4d1	vytlačen
barevnými	barevný	k2eAgInPc7d1	barevný
monitory	monitor	k1gInPc7	monitor
<g/>
,	,	kIx,	,
rozlišení	rozlišení	k1gNnSc1	rozlišení
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
z	z	k7c2	z
640	[number]	k4	640
x	x	k?	x
480	[number]	k4	480
pixelů	pixel	k1gInPc2	pixel
na	na	k7c4	na
1024	[number]	k4	1024
x	x	k?	x
768	[number]	k4	768
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
mechanika	mechanika	k1gFnSc1	mechanika
pro	pro	k7c4	pro
3.5	[number]	k4	3.5
<g/>
"	"	kIx"	"
diskety	disketa	k1gFnPc1	disketa
a	a	k8xC	a
mechanika	mechanika	k1gFnSc1	mechanika
CD-ROM	CD-ROM	k1gFnSc1	CD-ROM
<g/>
,	,	kIx,	,
zapojení	zapojení	k1gNnSc1	zapojení
počítače	počítač	k1gInSc2	počítač
do	do	k7c2	do
lokální	lokální	k2eAgFnSc2d1	lokální
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
zvuková	zvukový	k2eAgFnSc1d1	zvuková
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
běžným	běžný	k2eAgNnSc7d1	běžné
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
byl	být	k5eAaImAgInS	být
telefonní	telefonní	k2eAgInSc1d1	telefonní
modem	modem	k1gInSc1	modem
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
firemní	firemní	k2eAgFnSc6d1	firemní
a	a	k8xC	a
státní	státní	k2eAgFnSc6d1	státní
sféře	sféra	k1gFnSc6	sféra
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
jehličkových	jehličkový	k2eAgFnPc2d1	Jehličková
tiskáren	tiskárna	k1gFnPc2	tiskárna
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
laserových	laserový	k2eAgFnPc2d1	laserová
tiskáren	tiskárna	k1gFnPc2	tiskárna
<g/>
,	,	kIx,	,
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
sféře	sféra	k1gFnSc6	sféra
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
inkoustové	inkoustový	k2eAgFnPc1d1	inkoustová
tiskárny	tiskárna	k1gFnPc1	tiskárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
vakuové	vakuový	k2eAgFnPc1d1	vakuová
obrazovky	obrazovka	k1gFnPc1	obrazovka
vytlačeny	vytlačen	k2eAgFnPc1d1	vytlačena
LCD	LCD	kA	LCD
displeji	displej	k1gFnSc6	displej
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zlevnění	zlevnění	k1gNnSc1	zlevnění
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
rozšíření	rozšíření	k1gNnSc3	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
miniaturizace	miniaturizace	k1gFnSc1	miniaturizace
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
posun	posun	k1gInSc1	posun
od	od	k7c2	od
desktopových	desktopový	k2eAgInPc2d1	desktopový
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
k	k	k7c3	k
notebookům	notebook	k1gInPc3	notebook
a	a	k8xC	a
mobilním	mobilní	k2eAgNnPc3d1	mobilní
zařízením	zařízení	k1gNnPc3	zařízení
ve	v	k7c6	v
firemní	firemní	k2eAgFnSc6d1	firemní
sféře	sféra	k1gFnSc6	sféra
i	i	k9	i
u	u	k7c2	u
domácích	domácí	k2eAgMnPc2d1	domácí
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
multimédia	multimédium	k1gNnPc4	multimédium
a	a	k8xC	a
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
kromě	kromě	k7c2	kromě
PC	PC	kA	PC
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
tablety	tableta	k1gFnPc1	tableta
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
chytré	chytrý	k2eAgInPc4d1	chytrý
<g/>
"	"	kIx"	"
telefony	telefon	k1gInPc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
průniku	průnik	k1gInSc3	průnik
mobilních	mobilní	k2eAgNnPc2d1	mobilní
zařízení	zařízení	k1gNnPc2	zařízení
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c6	na
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
Android	android	k1gInSc1	android
a	a	k8xC	a
iOS	iOS	k?	iOS
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
donedávna	donedávna	k6eAd1	donedávna
monopol	monopol	k1gInSc1	monopol
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
ke	k	k7c3	k
klasickým	klasický	k2eAgMnPc3d1	klasický
univerzálním	univerzální	k2eAgMnPc3d1	univerzální
osobním	osobní	k2eAgMnPc3d1	osobní
počítačům	počítač	k1gMnPc3	počítač
je	být	k5eAaImIp3nS	být
také	také	k9	také
na	na	k7c6	na
konzumaci	konzumace	k1gFnSc6	konzumace
obsahu	obsah	k1gInSc2	obsah
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
Chromebook	Chromebook	k1gInSc4	Chromebook
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
domácí	domácí	k2eAgMnPc4d1	domácí
uživatele	uživatel	k1gMnPc4	uživatel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
a	a	k8xC	a
"	"	kIx"	"
<g/>
bezúdržbovost	bezúdržbovost	k1gFnSc4	bezúdržbovost
<g/>
"	"	kIx"	"
alternativních	alternativní	k2eAgNnPc2d1	alternativní
zařízení	zařízení	k1gNnPc2	zařízení
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vazbě	vazba	k1gFnSc3	vazba
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
na	na	k7c4	na
OS	OS	kA	OS
windows	windows	k1gInSc4	windows
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obavě	obava	k1gFnSc3	obava
průmyslu	průmysl	k1gInSc2	průmysl
z	z	k7c2	z
tříštění	tříštění	k1gNnSc2	tříštění
sil	síla	k1gFnPc2	síla
mezi	mezi	k7c4	mezi
různé	různý	k2eAgInPc4d1	různý
systémy	systém	k1gInPc4	systém
však	však	k8xC	však
osobní	osobní	k2eAgInSc4d1	osobní
počítač	počítač	k1gInSc4	počítač
s	s	k7c7	s
Windows	Windows	kA	Windows
zůstane	zůstat	k5eAaPmIp3nS	zůstat
ještě	ještě	k9	ještě
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
základním	základní	k2eAgInSc7d1	základní
pracovním	pracovní	k2eAgInSc7d1	pracovní
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Součásti	součást	k1gFnSc3	součást
PC	PC	kA	PC
==	==	k?	==
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
součástech	součást	k1gFnPc6	součást
osobního	osobní	k2eAgInSc2d1	osobní
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
rozlišit	rozlišit	k5eAaPmF	rozlišit
dvě	dva	k4xCgFnPc4	dva
roviny	rovina	k1gFnPc4	rovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
rovinu	rovina	k1gFnSc4	rovina
hardware	hardware	k1gInSc4	hardware
a	a	k8xC	a
software	software	k1gInSc4	software
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dobrou	dobrý	k2eAgFnSc4d1	dobrá
funkci	funkce	k1gFnSc4	funkce
počítače	počítač	k1gInSc2	počítač
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
a	a	k8xC	a
výkonný	výkonný	k2eAgInSc1d1	výkonný
hardware	hardware	k1gInSc1	hardware
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
to	ten	k3xDgNnSc1	ten
co	co	k3yInSc4	co
vdechuje	vdechovat	k5eAaImIp3nS	vdechovat
počítači	počítač	k1gInPc7	počítač
"	"	kIx"	"
<g/>
ducha	duch	k1gMnSc2	duch
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
především	především	k9	především
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hardware	hardware	k1gInSc4	hardware
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podíváme	podívat	k5eAaPmIp1nP	podívat
na	na	k7c4	na
osobní	osobní	k2eAgInSc4d1	osobní
počítač	počítač	k1gInSc4	počítač
zvenku	zvenku	k6eAd1	zvenku
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
snadno	snadno	k6eAd1	snadno
rozlišit	rozlišit	k5eAaPmF	rozlišit
několik	několik	k4yIc4	několik
součástí	součást	k1gFnPc2	součást
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
skříň	skříň	k1gFnSc1	skříň
====	====	k?	====
</s>
</p>
<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
box	box	k1gInSc4	box
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
komponenty	komponenta	k1gFnPc4	komponenta
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
také	také	k9	také
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
plast	plast	k1gInSc4	plast
a	a	k8xC	a
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Počítačové	počítačový	k2eAgFnSc2d1	počítačová
skříně	skříň	k1gFnSc2	skříň
můžou	můžou	k?	můžou
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
dekorativním	dekorativní	k2eAgInSc7d1	dekorativní
objektem	objekt	k1gInSc7	objekt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
modding	modding	k1gInSc1	modding
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skříni	skříň	k1gFnSc6	skříň
====	====	k?	====
</s>
</p>
<p>
<s>
Typický	typický	k2eAgInSc1d1	typický
osobní	osobní	k2eAgInSc1d1	osobní
počítač	počítač	k1gInSc1	počítač
dnes	dnes	k6eAd1	dnes
bývá	bývat	k5eAaImIp3nS	bývat
proveden	provést	k5eAaPmNgInS	provést
jako	jako	k9	jako
přístroj	přístroj	k1gInSc1	přístroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
položen	položit	k5eAaPmNgInS	položit
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
uložen	uložit	k5eAaPmNgInS	uložit
pod	pod	k7c7	pod
stolem	stol	k1gInSc7	stol
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
používají	používat	k5eAaImIp3nP	používat
notebooky	notebook	k1gInPc1	notebook
–	–	k?	–
přenosné	přenosný	k2eAgInPc1d1	přenosný
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
skříň	skříň	k1gFnSc4	skříň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vše	všechen	k3xTgNnSc1	všechen
potřebné	potřebné	k1gNnSc1	potřebné
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
integrováno	integrovat	k5eAaBmNgNnS	integrovat
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
běžné	běžný	k2eAgFnPc1d1	běžná
"	"	kIx"	"
<g/>
all	all	k?	all
in	in	k?	in
one	one	k?	one
<g/>
"	"	kIx"	"
PC	PC	kA	PC
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc1	obsah
počítačové	počítačový	k2eAgFnSc2d1	počítačová
skříně	skříň	k1gFnSc2	skříň
integrován	integrovat	k5eAaBmNgInS	integrovat
v	v	k7c6	v
tělě	těla	k1gFnSc6	těla
monitoru	monitor	k1gInSc2	monitor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
počítačová	počítačový	k2eAgFnSc1d1	počítačová
skříň	skříň	k1gFnSc1	skříň
bývá	bývat	k5eAaImIp3nS	bývat
provedena	provést	k5eAaPmNgFnS	provést
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
desktop	desktop	k1gInSc1	desktop
–	–	k?	–
pokládá	pokládat	k5eAaImIp3nS	pokládat
se	se	k3xPyFc4	se
naplocho	naplocho	k6eAd1	naplocho
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgNnSc2	tento
provedení	provedení	k1gNnSc2	provedení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
skříň	skříň	k1gFnSc1	skříň
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
současně	současně	k6eAd1	současně
jako	jako	k8xC	jako
podstavec	podstavec	k1gInSc1	podstavec
pro	pro	k7c4	pro
monitor	monitor	k1gInSc4	monitor
</s>
</p>
<p>
<s>
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
tower	tower	k1gInSc1	tower
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skříň	skříň	k1gFnSc1	skříň
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
podložce	podložka	k1gFnSc6	podložka
svojí	svojit	k5eAaImIp3nS	svojit
kratší	krátký	k2eAgFnSc7d2	kratší
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
připomíná	připomínat	k5eAaImIp3nS	připomínat
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
tower	tower	k1gInSc1	tower
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
jak	jak	k6eAd1	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
chlazení	chlazení	k1gNnSc2	chlazení
<g/>
,	,	kIx,	,
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
počítač	počítač	k1gInSc1	počítač
nemusí	muset	k5eNaImIp3nS	muset
zabírat	zabírat	k5eAaImF	zabírat
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
mini	mini	k2eAgInSc1d1	mini
tower	tower	k1gInSc1	tower
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
menší	malý	k2eAgNnSc4d2	menší
provedení	provedení	k1gNnSc4	provedení
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
midi	midit	k5eAaImRp2nS	midit
tower	tower	k1gInSc1	tower
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
středně	středně	k6eAd1	středně
velkou	velký	k2eAgFnSc4d1	velká
skříň	skříň	k1gFnSc4	skříň
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
skříni	skříň	k1gFnSc6	skříň
bývá	bývat	k5eAaImIp3nS	bývat
uložen	uložen	k2eAgInSc1d1	uložen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
napájecí	napájecí	k2eAgInSc1d1	napájecí
zdroj	zdroj	k1gInSc1	zdroj
(	(	kIx(	(
<g/>
SPS	SPS	kA	SPS
–	–	k?	–
system	syst	k1gMnSc7	syst
power	power	k1gInSc4	power
supply	suppnout	k5eAaPmAgInP	suppnout
<g/>
)	)	kIx)	)
–	–	k?	–
převádí	převádět	k5eAaImIp3nS	převádět
napětí	napětí	k1gNnSc4	napětí
elektrické	elektrický	k2eAgFnSc2d1	elektrická
sítě	síť	k1gFnSc2	síť
na	na	k7c4	na
úrovně	úroveň	k1gFnPc4	úroveň
potřebné	potřebný	k2eAgFnPc4d1	potřebná
pro	pro	k7c4	pro
napájení	napájení	k1gNnPc4	napájení
komponent	komponent	k1gInSc1	komponent
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
harddisk	harddisk	k1gInSc1	harddisk
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
výkon	výkon	k1gInSc4	výkon
od	od	k7c2	od
200	[number]	k4	200
do	do	k7c2	do
1000	[number]	k4	1000
Wattů	watt	k1gInPc2	watt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
základní	základní	k2eAgFnSc1d1	základní
deska	deska	k1gFnSc1	deska
–	–	k?	–
také	také	k9	také
mateřská	mateřský	k2eAgFnSc1d1	mateřská
deska	deska	k1gFnSc1	deska
nebo	nebo	k8xC	nebo
motherboard	motherboard	k1gInSc1	motherboard
–	–	k?	–
je	být	k5eAaImIp3nS	být
deska	deska	k1gFnSc1	deska
plošných	plošný	k2eAgInPc2d1	plošný
spojů	spoj	k1gInPc2	spoj
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
operační	operační	k2eAgFnSc1d1	operační
paměť	paměť	k1gFnSc1	paměť
(	(	kIx(	(
<g/>
RAM	RAM	kA	RAM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc4	paměť
BIOSu	BIOSus	k1gInSc2	BIOSus
(	(	kIx(	(
<g/>
FLASH	FLASH	kA	FLASH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hodiny	hodina	k1gFnPc4	hodina
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
čipset	čipset	k1gInSc4	čipset
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
základní	základní	k2eAgNnSc4d1	základní
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
konektory	konektor	k1gInPc1	konektor
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
,	,	kIx,	,
tiskárny	tiskárna	k1gFnSc2	tiskárna
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
desku	deska	k1gFnSc4	deska
často	často	k6eAd1	často
integruje	integrovat	k5eAaBmIp3nS	integrovat
i	i	k9	i
grafická	grafický	k2eAgFnSc1d1	grafická
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
zvuková	zvukový	k2eAgFnSc1d1	zvuková
karta	karta	k1gFnSc1	karta
a	a	k8xC	a
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rozšiřující	rozšiřující	k2eAgFnPc4d1	rozšiřující
desky	deska	k1gFnPc4	deska
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
desky	deska	k1gFnPc4	deska
plošných	plošný	k2eAgInPc2d1	plošný
spojů	spoj	k1gInPc2	spoj
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
zasunutí	zasunutí	k1gNnSc4	zasunutí
do	do	k7c2	do
slotů	slot	k1gInPc2	slot
základní	základní	k2eAgFnSc2d1	základní
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
grafická	grafický	k2eAgFnSc1d1	grafická
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
zvuková	zvukový	k2eAgFnSc1d1	zvuková
karta	karta	k1gFnSc1	karta
nebo	nebo	k8xC	nebo
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
integrací	integrace	k1gFnSc7	integrace
a	a	k8xC	a
popularitou	popularita	k1gFnSc7	popularita
přenosných	přenosný	k2eAgMnPc2d1	přenosný
počítačů	počítač	k1gMnPc2	počítač
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
používají	používat	k5eAaImIp3nP	používat
rozšiřující	rozšiřující	k2eAgFnPc4d1	rozšiřující
karty	karta	k1gFnPc4	karta
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zařízení	zařízení	k1gNnSc2	zařízení
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
pomocí	pomocí	k7c2	pomocí
USB	USB	kA	USB
</s>
</p>
<p>
<s>
harddisk	harddisk	k1gInSc1	harddisk
–	–	k?	–
také	také	k6eAd1	také
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
uchovávání	uchovávání	k1gNnSc3	uchovávání
programů	program	k1gInPc2	program
a	a	k8xC	a
dat	datum	k1gNnPc2	datum
potřebných	potřebný	k2eAgMnPc2d1	potřebný
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
mechanika	mechanika	k1gFnSc1	mechanika
DVD	DVD	kA	DVD
nebo	nebo	k8xC	nebo
CD-ROM	CD-ROM	k1gFnSc1	CD-ROM
–	–	k?	–
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
nahrávání	nahrávání	k1gNnSc3	nahrávání
a	a	k8xC	a
používání	používání	k1gNnSc4	používání
dat	datum	k1gNnPc2	datum
uložených	uložený	k2eAgNnPc2d1	uložené
na	na	k7c6	na
optických	optický	k2eAgInPc6d1	optický
discích	disk	k1gInPc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
mechaniky	mechanika	k1gFnPc1	mechanika
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
schopné	schopný	k2eAgInPc1d1	schopný
na	na	k7c4	na
optické	optický	k2eAgInPc4d1	optický
disky	disk	k1gInPc4	disk
i	i	k8xC	i
zapisovat	zapisovat	k5eAaImF	zapisovat
</s>
</p>
<p>
<s>
disketová	disketový	k2eAgFnSc1d1	disketová
mechanika	mechanika	k1gFnSc1	mechanika
–	–	k?	–
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
čtení	čtení	k1gNnSc3	čtení
a	a	k8xC	a
zapisování	zapisování	k1gNnSc4	zapisování
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
diskety	disketa	k1gFnPc4	disketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Monitor	monitor	k1gInSc1	monitor
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
,	,	kIx,	,
myš	myš	k1gFnSc1	myš
====	====	k?	====
</s>
</p>
<p>
<s>
Monitor	monitor	k1gInSc1	monitor
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnSc1	klávesnice
a	a	k8xC	a
myš	myš	k1gFnSc1	myš
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgNnSc1d1	jiné
ukazovací	ukazovací	k2eAgNnSc1d1	ukazovací
zařízení	zařízení	k1gNnSc1	zařízení
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
základními	základní	k2eAgInPc7d1	základní
prostředky	prostředek	k1gInPc7	prostředek
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
uživatele	uživatel	k1gMnSc2	uživatel
s	s	k7c7	s
osobním	osobní	k2eAgInSc7d1	osobní
počítačem	počítač	k1gInSc7	počítač
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Připojují	připojovat	k5eAaImIp3nP	připojovat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
ke	k	k7c3	k
skříni	skříň	k1gFnSc3	skříň
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Přenosné	přenosný	k2eAgInPc1d1	přenosný
počítače	počítač	k1gInPc1	počítač
mají	mít	k5eAaImIp3nP	mít
samozřejmě	samozřejmě	k6eAd1	samozřejmě
tyto	tento	k3xDgFnPc1	tento
součásti	součást	k1gFnPc1	součást
integrovány	integrovat	k5eAaBmNgFnP	integrovat
již	již	k6eAd1	již
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladším	mladý	k2eAgNnSc7d3	nejmladší
zařízením	zařízení	k1gNnSc7	zařízení
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
trojice	trojice	k1gFnSc2	trojice
je	být	k5eAaImIp3nS	být
myš	myš	k1gFnSc1	myš
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
také	také	k9	také
trackball	trackball	k1gInSc1	trackball
nebo	nebo	k8xC	nebo
touchpad	touchpad	k1gInSc1	touchpad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
grafickým	grafický	k2eAgNnSc7d1	grafické
uživatelským	uživatelský	k2eAgNnSc7d1	Uživatelské
rozhraním	rozhraní	k1gNnSc7	rozhraní
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
zařízení	zařízení	k1gNnPc4	zařízení
====	====	k?	====
</s>
</p>
<p>
<s>
K	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
připojují	připojovat	k5eAaImIp3nP	připojovat
nejrůznější	různý	k2eAgNnPc4d3	nejrůznější
periferní	periferní	k2eAgNnPc4d1	periferní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
naprostou	naprostý	k2eAgFnSc7d1	naprostá
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
například	například	k6eAd1	například
mikrofon	mikrofon	k1gInSc4	mikrofon
a	a	k8xC	a
reproduktory	reproduktor	k1gInPc4	reproduktor
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
sluchátka	sluchátko	k1gNnSc2	sluchátko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
z	z	k7c2	z
PC	PC	kA	PC
dělají	dělat	k5eAaImIp3nP	dělat
opravdové	opravdový	k2eAgFnPc1d1	opravdová
multimediální	multimediální	k2eAgFnPc1d1	multimediální
a	a	k8xC	a
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
internetem	internet	k1gInSc7	internet
také	také	k9	také
komunikační	komunikační	k2eAgNnSc4d1	komunikační
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častějším	častý	k2eAgInSc7d2	častější
multimediálním	multimediální	k2eAgInSc7d1	multimediální
doplňkem	doplněk	k1gInSc7	doplněk
je	být	k5eAaImIp3nS	být
i	i	k9	i
webová	webový	k2eAgFnSc1d1	webová
kamera	kamera	k1gFnSc1	kamera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
výhodou	výhoda	k1gFnSc7	výhoda
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
internetové	internetový	k2eAgFnSc2d1	internetová
telefonie	telefonie	k1gFnSc2	telefonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
oblíbenými	oblíbený	k2eAgInPc7d1	oblíbený
doplňky	doplněk	k1gInPc7	doplněk
jsou	být	k5eAaImIp3nP	být
tiskárna	tiskárna	k1gFnSc1	tiskárna
a	a	k8xC	a
čtečka	čtečka	k1gFnSc1	čtečka
paměťových	paměťový	k2eAgFnPc2d1	paměťová
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Software	software	k1gInSc1	software
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
bez	bez	k1gInSc1	bez
software	software	k1gInSc1	software
není	být	k5eNaImIp3nS	být
provoz	provoz	k1gInSc1	provoz
počítače	počítač	k1gInSc2	počítač
použitelný	použitelný	k2eAgInSc1d1	použitelný
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
veškerý	veškerý	k3xTgInSc1	veškerý
hardware	hardware	k1gInSc1	hardware
osobního	osobní	k2eAgInSc2d1	osobní
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgInPc7d1	současný
nejrozšířenějšími	rozšířený	k2eAgInPc7d3	nejrozšířenější
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
na	na	k7c6	na
osobních	osobní	k2eAgInPc6d1	osobní
počítačích	počítač	k1gInPc6	počítač
jsou	být	k5eAaImIp3nP	být
systémy	systém	k1gInPc1	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
a	a	k8xC	a
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
pak	pak	k6eAd1	pak
také	také	k9	také
unixové	unixový	k2eAgInPc1d1	unixový
systémy	systém	k1gInPc1	systém
Apple	Apple	kA	Apple
macOS	macOS	k?	macOS
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
lepší	dobrý	k2eAgFnSc7d2	lepší
volbou	volba	k1gFnSc7	volba
jsou	být	k5eAaImIp3nP	být
svobodné	svobodný	k2eAgInPc1d1	svobodný
a	a	k8xC	a
volně	volně	k6eAd1	volně
šířitelné	šířitelný	k2eAgInPc4d1	šířitelný
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc4d1	založený
zejména	zejména	k9	zejména
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Linuxu	linux	k1gInSc2	linux
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
na	na	k7c4	na
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
<g/>
,	,	kIx,	,
NetBSD	NetBSD	k1gMnPc5	NetBSD
<g/>
,	,	kIx,	,
OpenBSD	OpenBSD	k1gMnPc5	OpenBSD
<g/>
,	,	kIx,	,
PC-BSD	PC-BSD	k1gMnPc5	PC-BSD
(	(	kIx(	(
<g/>
TrueOS	TrueOS	k1gFnSc1	TrueOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DragonFly	DragonFla	k1gFnPc4	DragonFla
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
OpenSolaris	OpenSolaris	k1gFnSc2	OpenSolaris
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jádra	jádro	k1gNnSc2	jádro
Linuxu	linux	k1gInSc2	linux
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
založeny	založit	k5eAaPmNgInP	založit
mobilní	mobilní	k2eAgInPc1d1	mobilní
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
Android	android	k1gInSc4	android
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Google	Google	k1gInSc1	Google
a	a	k8xC	a
Tizen	Tizen	k1gInSc1	Tizen
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Samsung	Samsung	kA	Samsung
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Linuxu	linux	k1gInSc6	linux
je	být	k5eAaImIp3nS	být
také	také	k9	také
postaven	postavit	k5eAaPmNgInS	postavit
herní	herní	k2eAgInSc1d1	herní
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
SteamOS	SteamOS	k1gFnSc2	SteamOS
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Valve	Valev	k1gFnSc2	Valev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jádra	jádro	k1gNnSc2	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Orbis	orbis	k1gInSc4	orbis
OS	OS	kA	OS
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
herní	herní	k2eAgFnSc3d1	herní
konzole	konzola	k1gFnSc3	konzola
PlayStation	PlayStation	k1gInSc1	PlayStation
4	[number]	k4	4
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Sony	Sony	kA	Sony
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Patricka	Patricko	k1gNnSc2	Patricko
Zandla	Zandla	k1gFnSc2	Zandla
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
Windows	Windows	kA	Windows
XP	XP	kA	XP
končí	končit	k5eAaImIp3nS	končit
éra	éra	k1gFnSc1	éra
dominance	dominance	k1gFnSc1	dominance
počítačů	počítač	k1gMnPc2	počítač
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
====	====	k?	====
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
software	software	k1gInSc1	software
počítače	počítač	k1gInSc2	počítač
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
funkcí	funkce	k1gFnSc7	funkce
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
poskytovat	poskytovat	k5eAaImF	poskytovat
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
zpřístupňovat	zpřístupňovat	k5eAaImF	zpřístupňovat
hardware	hardware	k1gInSc4	hardware
počítače	počítač	k1gInSc2	počítač
aplikačním	aplikační	k2eAgMnPc3d1	aplikační
programům	program	k1gInPc3	program
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
obvykle	obvykle	k6eAd1	obvykle
představíme	představit	k5eAaPmIp1nP	představit
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
špička	špička	k1gFnSc1	špička
ledovce	ledovec	k1gInSc2	ledovec
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
méně	málo	k6eAd2	málo
podstatných	podstatný	k2eAgFnPc2d1	podstatná
funkcí	funkce	k1gFnPc2	funkce
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
aplikacím	aplikace	k1gFnPc3	aplikace
systémové	systémový	k2eAgFnPc1d1	systémová
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
původních	původní	k2eAgInPc2d1	původní
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgMnPc2d1	kompatibilní
byl	být	k5eAaImAgMnS	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
MS-DOS	MS-DOS	k1gMnPc2	MS-DOS
nebo	nebo	k8xC	nebo
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
klonů	klon	k1gInPc2	klon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpoužívanější	používaný	k2eAgInPc4d3	nejpoužívanější
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
pro	pro	k7c4	pro
PC	PC	kA	PC
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
systémů	systém	k1gInPc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
problémy	problém	k1gInPc4	problém
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
vyšší	vysoký	k2eAgFnPc4d2	vyšší
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
narazíte	narazit	k5eAaPmIp2nP	narazit
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
a	a	k8xC	a
na	na	k7c4	na
vysoké	vysoký	k2eAgInPc4d1	vysoký
nároky	nárok	k1gInPc4	nárok
hardware	hardware	k1gInSc1	hardware
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nefungují	fungovat	k5eNaImIp3nP	fungovat
se	s	k7c7	s
staršími	starý	k2eAgFnPc7d2	starší
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
rozšířenost	rozšířenost	k1gFnSc1	rozšířenost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
existuje	existovat	k5eAaImIp3nS	existovat
nepřeberné	přeberný	k2eNgNnSc1d1	nepřeberné
množství	množství	k1gNnSc1	množství
komerčních	komerční	k2eAgFnPc2d1	komerční
i	i	k8xC	i
volně	volně	k6eAd1	volně
dostupných	dostupný	k2eAgFnPc2d1	dostupná
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
alternativou	alternativa	k1gFnSc7	alternativa
desktopových	desktopový	k2eAgInPc2d1	desktopový
MS	MS	kA	MS
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
desktopech	desktop	k1gInPc6	desktop
začíná	začínat	k5eAaImIp3nS	začínat
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
prosazovat	prosazovat	k5eAaImF	prosazovat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgInPc4d1	nízký
pořizovací	pořizovací	k2eAgInPc4d1	pořizovací
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc4d1	rychlý
a	a	k8xC	a
spolehlivý	spolehlivý	k2eAgInSc4d1	spolehlivý
běh	běh	k1gInSc4	běh
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
přizpůsobitelnost	přizpůsobitelnost	k1gFnSc4	přizpůsobitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
uživatelské	uživatelský	k2eAgFnSc2d1	Uživatelská
přívětivosti	přívětivost	k1gFnSc2	přívětivost
a	a	k8xC	a
praktické	praktický	k2eAgFnSc2d1	praktická
použitelnosti	použitelnost	k1gFnSc2	použitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
pár	pár	k4xCyI	pár
počítačových	počítačový	k2eAgMnPc2d1	počítačový
nadšenců	nadšenec	k1gMnPc2	nadšenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
konkurenceschopnou	konkurenceschopný	k2eAgFnSc4d1	konkurenceschopná
platformu	platforma	k1gFnSc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
pod	pod	k7c7	pod
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
licencí	licence	k1gFnSc7	licence
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
GPL	GPL	kA	GPL
jeho	jeho	k3xOp3gFnSc7	jeho
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
i	i	k9	i
existence	existence	k1gFnSc1	existence
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
aplikací	aplikace	k1gFnPc2	aplikace
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
volně	volně	k6eAd1	volně
šiřitelného	šiřitelný	k2eAgInSc2d1	šiřitelný
software	software	k1gInSc1	software
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
i	i	k8xC	i
komerčních	komerční	k2eAgFnPc2d1	komerční
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
některých	některý	k3yIgFnPc2	některý
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Aplikační	aplikační	k2eAgInSc1d1	aplikační
software	software	k1gInSc1	software
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
možnosti	možnost	k1gFnPc1	možnost
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
takové	takový	k3xDgFnSc2	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
a	a	k8xC	a
rozumně	rozumně	k6eAd1	rozumně
definovat	definovat	k5eAaBmF	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k8xC	i
moře	moře	k1gNnSc4	moře
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tyto	tento	k3xDgFnPc4	tento
možnosti	možnost	k1gFnPc1	možnost
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
směru	směr	k1gInSc6	směr
lépe	dobře	k6eAd2	dobře
či	či	k8xC	či
hůře	zle	k6eAd2	zle
realizuje	realizovat	k5eAaBmIp3nS	realizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
software	software	k1gInSc1	software
–	–	k?	–
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	řečen	k2eAgNnSc1d1	řečeno
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
software	software	k1gInSc4	software
pro	pro	k7c4	pro
sekretářky	sekretářka	k1gFnPc4	sekretářka
<g/>
,	,	kIx,	,
o	o	k7c4	o
softwarový	softwarový	k2eAgInSc4d1	softwarový
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
kalkulačky	kalkulačka	k1gFnSc2	kalkulačka
a	a	k8xC	a
faxu	fax	k1gInSc2	fax
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
firemním	firemní	k2eAgNnSc6d1	firemní
prostředí	prostředí	k1gNnSc6	prostředí
jde	jít	k5eAaImIp3nS	jít
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
textového	textový	k2eAgInSc2d1	textový
editoru	editor	k1gInSc2	editor
<g/>
,	,	kIx,	,
tabulkového	tabulkový	k2eAgInSc2d1	tabulkový
kalkulátoru	kalkulátor	k1gInSc2	kalkulátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
poštovního	poštovní	k1gMnSc4	poštovní
klienta	klient	k1gMnSc4	klient
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
zdatným	zdatný	k2eAgMnSc7d1	zdatný
volně	volně	k6eAd1	volně
šiřitelným	šiřitelný	k2eAgMnSc7d1	šiřitelný
kolegou	kolega	k1gMnSc7	kolega
je	být	k5eAaImIp3nS	být
Open	Open	k1gMnSc1	Open
Office	Office	kA	Office
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zdarma	zdarma	k6eAd1	zdarma
nabízí	nabízet	k5eAaImIp3nS	nabízet
poměrně	poměrně	k6eAd1	poměrně
dobrou	dobrý	k2eAgFnSc4d1	dobrá
úroveň	úroveň	k1gFnSc4	úroveň
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
s	s	k7c7	s
MS	MS	kA	MS
Office	Office	kA	Office
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
i	i	k9	i
pro	pro	k7c4	pro
Linux	linux	k1gInSc4	linux
<g/>
.	.	kIx.	.
<g/>
komunikační	komunikační	k2eAgInSc4d1	komunikační
software	software	k1gInSc4	software
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
webový	webový	k2eAgInSc4d1	webový
prohlížeč	prohlížeč	k1gInSc4	prohlížeč
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
Instant	Instant	k1gInSc4	Instant
messaging	messaging	k1gInSc1	messaging
nebo	nebo	k8xC	nebo
internetovou	internetový	k2eAgFnSc4d1	internetová
telefonii	telefonie	k1gFnSc4	telefonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
multimediální	multimediální	k2eAgInSc1d1	multimediální
software	software	k1gInSc1	software
–	–	k?	–
nejrůznější	různý	k2eAgInSc1d3	nejrůznější
software	software	k1gInSc1	software
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
také	také	k9	také
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
vytváření	vytváření	k1gNnSc4	vytváření
a	a	k8xC	a
střih	střih	k1gInSc4	střih
</s>
</p>
<p>
<s>
software	software	k1gInSc1	software
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
designu	design	k1gInSc2	design
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
systémy	systém	k1gInPc4	systém
CAD	CAD	kA	CAD
a	a	k8xC	a
CAE	CAE	kA	CAE
–	–	k?	–
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
moderní	moderní	k2eAgFnSc4d1	moderní
náhradu	náhrada	k1gFnSc4	náhrada
rýsovacího	rýsovací	k2eAgNnSc2d1	rýsovací
prkna	prkno	k1gNnSc2	prkno
</s>
</p>
<p>
<s>
prostředky	prostředek	k1gInPc1	prostředek
vývoj	vývoj	k1gInSc1	vývoj
software	software	k1gInSc4	software
–	–	k?	–
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
programovací	programovací	k2eAgInPc4d1	programovací
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
pomocné	pomocný	k2eAgInPc4d1	pomocný
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
software	software	k1gInSc1	software
</s>
</p>
<p>
<s>
herní	herní	k2eAgInSc1d1	herní
software	software	k1gInSc1	software
–	–	k?	–
různé	různý	k2eAgFnPc4d1	různá
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
multimediální	multimediální	k2eAgNnSc1d1	multimediální
graficky	graficky	k6eAd1	graficky
orientované	orientovaný	k2eAgNnSc1d1	orientované
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Windows	Windows	kA	Windows
XP	XP	kA	XP
(	(	kIx(	(
<g/>
dostupnost	dostupnost	k1gFnSc1	dostupnost
od	od	k7c2	od
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
7	[number]	k4	7
(	(	kIx(	(
<g/>
dostupnost	dostupnost	k1gFnSc1	dostupnost
od	od	k7c2	od
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
8	[number]	k4	8
a	a	k8xC	a
8.1	[number]	k4	8.1
<g/>
(	(	kIx(	(
<g/>
dostupnost	dostupnost	k1gFnSc1	dostupnost
od	od	k7c2	od
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Windows	Windows	kA	Windows
10	[number]	k4	10
(	(	kIx(	(
<g/>
dostupnost	dostupnost	k1gFnSc1	dostupnost
od	od	k7c2	od
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
macOS	macOS	k?	macOS
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drtivou	drtivý	k2eAgFnSc4d1	drtivá
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
mají	mít	k5eAaImIp3nP	mít
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
přenositelné	přenositelný	k2eAgFnPc1d1	přenositelná
na	na	k7c4	na
architektury	architektura	k1gFnPc4	architektura
nekompatibilní	kompatibilní	k2eNgFnPc4d1	nekompatibilní
s	s	k7c7	s
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
komplikovaných	komplikovaný	k2eAgInPc2d1	komplikovaný
nestandardizovaných	standardizovaný	k2eNgInPc2d1	nestandardizovaný
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
nedokumentovaných	dokumentovaný	k2eNgInPc2d1	nedokumentovaný
proprietárních	proprietární	k2eAgInPc2d1	proprietární
datových	datový	k2eAgInPc2d1	datový
formátů	formát	k1gInPc2	formát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
monokulturou	monokultura	k1gFnSc7	monokultura
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
Windows	Windows	kA	Windows
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
prostředí	prostředí	k1gNnSc6	prostředí
internetu	internet	k1gInSc2	internet
souvisí	souviset	k5eAaImIp3nS	souviset
bohužel	bohužel	k9	bohužel
i	i	k9	i
nedobrá	dobrý	k2eNgFnSc1d1	nedobrá
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
současných	současný	k2eAgMnPc2d1	současný
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
proti	proti	k7c3	proti
počítačovým	počítačový	k2eAgInPc3d1	počítačový
virům	vir	k1gInPc3	vir
a	a	k8xC	a
krádežím	krádež	k1gFnPc3	krádež
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současné	současný	k2eAgInPc1d1	současný
osobní	osobní	k2eAgInPc1d1	osobní
počítače	počítač	k1gInPc1	počítač
jsou	být	k5eAaImIp3nP	být
použitelné	použitelný	k2eAgFnPc1d1	použitelná
nejen	nejen	k6eAd1	nejen
ke	k	k7c3	k
kancelářské	kancelářský	k2eAgFnSc3d1	kancelářská
práci	práce	k1gFnSc3	práce
nebo	nebo	k8xC	nebo
řízení	řízení	k1gNnSc3	řízení
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgFnSc3d1	kvalitní
reprodukci	reprodukce	k1gFnSc3	reprodukce
hudby	hudba	k1gFnSc2	hudba
nebo	nebo	k8xC	nebo
zpracování	zpracování	k1gNnSc2	zpracování
digitálních	digitální	k2eAgFnPc2d1	digitální
fotografií	fotografia	k1gFnPc2	fotografia
či	či	k8xC	či
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
osobní	osobní	k2eAgInPc1d1	osobní
počítače	počítač	k1gInPc1	počítač
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
částečně	částečně	k6eAd1	částečně
vytlačovány	vytlačován	k2eAgFnPc4d1	vytlačována
mobilními	mobilní	k2eAgNnPc7d1	mobilní
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
tablety	tableta	k1gFnPc4	tableta
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
systému	systém	k1gInSc6	systém
Android	android	k1gInSc1	android
nebo	nebo	k8xC	nebo
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
notebooky	notebook	k1gInPc4	notebook
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
cloudovém	cloudový	k2eAgNnSc6d1	cloudový
"	"	kIx"	"
<g/>
Chrome	chromat	k5eAaImIp3nS	chromat
OS	osa	k1gFnPc2	osa
<g/>
"	"	kIx"	"
firmy	firma	k1gFnSc2	firma
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgNnPc2	tento
zařízení	zařízení	k1gNnPc2	zařízení
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Android	android	k1gInSc1	android
a	a	k8xC	a
Chrome	chromat	k5eAaImIp3nS	chromat
OS	OS	kA	OS
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
provozovány	provozovat	k5eAaImNgInP	provozovat
i	i	k9	i
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
x	x	k?	x
<g/>
86	[number]	k4	86
resp.	resp.	kA	resp.
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
výborně	výborně	k6eAd1	výborně
poslouží	posloužit	k5eAaPmIp3nP	posloužit
pro	pro	k7c4	pro
prohlížení	prohlížení	k1gNnSc4	prohlížení
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
administrativní	administrativní	k2eAgFnPc4d1	administrativní
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
mnoha	mnoho	k4c2	mnoho
uživatelům	uživatel	k1gMnPc3	uživatel
postačovat	postačovat	k5eAaImF	postačovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
plnohodnotné	plnohodnotný	k2eAgFnSc3d1	plnohodnotná
náhradě	náhrada	k1gFnSc3	náhrada
PC	PC	kA	PC
mají	mít	k5eAaImIp3nP	mít
tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
dosud	dosud	k6eAd1	dosud
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
úroveň	úroveň	k1gFnSc4	úroveň
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
proti	proti	k7c3	proti
malware	malwar	k1gMnSc5	malwar
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
systému	systém	k1gInSc2	systém
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
záplatování	záplatování	k1gNnSc4	záplatování
a	a	k8xC	a
upgrade	upgrade	k1gInSc4	upgrade
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
zařízení	zařízení	k1gNnPc2	zařízení
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c4	na
OS	OS	kA	OS
Android	android	k1gInSc4	android
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
starat	starat	k5eAaImF	starat
výrobce	výrobce	k1gMnSc2	výrobce
hardware	hardware	k1gInSc4	hardware
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
spíše	spíše	k9	spíše
výjimkou	výjimka	k1gFnSc7	výjimka
než	než	k8xS	než
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
výkon	výkon	k1gInSc1	výkon
špičkových	špičkový	k2eAgInPc2d1	špičkový
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
výkonu	výkon	k1gInSc3	výkon
PC	PC	kA	PC
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
současně	současně	k6eAd1	současně
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
opačný	opačný	k2eAgInSc4d1	opačný
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířenost	rozšířenost	k1gFnSc1	rozšířenost
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
založených	založený	k2eAgInPc2d1	založený
na	na	k7c4	na
MS	MS	kA	MS
Windows	Windows	kA	Windows
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
Microsoftu	Microsofta	k1gFnSc4	Microsofta
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
tabletů	tablet	k1gInPc2	tablet
a	a	k8xC	a
mobilů	mobil	k1gInPc2	mobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
zrodu	zrod	k1gInSc2	zrod
design	design	k1gInSc1	design
osobního	osobní	k2eAgInSc2d1	osobní
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
od	od	k7c2	od
těch	ten	k3xDgMnPc2	ten
původních	původní	k2eAgMnPc2d1	původní
hodně	hodně	k6eAd1	hodně
vzdáleny	vzdálit	k5eAaPmNgFnP	vzdálit
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc1	všechen
jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
1000	[number]	k4	1000
<g/>
×	×	k?	×
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
odstavcích	odstavec	k1gInPc6	odstavec
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
procesory	procesor	k1gInPc1	procesor
nyní	nyní	k6eAd1	nyní
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
frekvencích	frekvence	k1gFnPc6	frekvence
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
GHz	GHz	k1gFnPc2	GHz
(	(	kIx(	(
<g/>
na	na	k7c6	na
každém	každý	k3xTgNnSc6	každý
z	z	k7c2	z
několika	několik	k4yIc2	několik
jader	jádro	k1gNnPc2	jádro
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
TB	TB	kA	TB
nejsou	být	k5eNaImIp3nP	být
žádnou	žádný	k3yNgFnSc7	žádný
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
GB	GB	kA	GB
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
běžným	běžný	k2eAgInSc7d1	běžný
standardem	standard	k1gInSc7	standard
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k8xS	tak
DVD	DVD	kA	DVD
mechaniky	mechanika	k1gFnSc2	mechanika
na	na	k7c4	na
disky	disk	k1gInPc4	disk
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
4,7	[number]	k4	4,7
GB	GB	kA	GB
a	a	k8xC	a
nastupující	nastupující	k2eAgFnPc4d1	nastupující
Blu-ray	Bluaa	k1gFnPc4	Blu-raa
na	na	k7c4	na
disky	disk	k1gInPc4	disk
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
GB	GB	kA	GB
<g/>
,	,	kIx,	,
diskety	disketa	k1gFnPc1	disketa
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
a	a	k8xC	a
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
již	již	k6eAd1	již
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
snadno	snadno	k6eAd1	snadno
přenášejí	přenášet	k5eAaImIp3nP	přenášet
přes	přes	k7c4	přes
Internet	Internet	k1gInSc4	Internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
probíhá	probíhat	k5eAaImIp3nS	probíhat
neustále	neustále	k6eAd1	neustále
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
za	za	k7c4	za
kratší	krátký	k2eAgFnSc4d2	kratší
či	či	k8xC	či
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
doplnit	doplnit	k5eAaPmF	doplnit
další	další	k2eAgInPc4d1	další
odstavce	odstavec	k1gInPc4	odstavec
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
platí	platit	k5eAaImIp3nS	platit
empirické	empirický	k2eAgNnSc1d1	empirické
Mooreovo	Mooreův	k2eAgNnSc1d1	Mooreovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každých	každý	k3xTgInPc2	každý
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
cenu	cena	k1gFnSc4	cena
integrovat	integrovat	k5eAaBmF	integrovat
do	do	k7c2	do
polovodičového	polovodičový	k2eAgInSc2d1	polovodičový
obvodu	obvod	k1gInSc2	obvod
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
však	však	k9	však
technologie	technologie	k1gFnSc1	technologie
křemíkových	křemíkový	k2eAgInPc2d1	křemíkový
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
všechny	všechen	k3xTgFnPc4	všechen
základní	základní	k2eAgFnPc4d1	základní
součásti	součást	k1gFnPc4	součást
současných	současný	k2eAgMnPc2d1	současný
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
narážet	narážet	k5eAaImF	narážet
na	na	k7c4	na
technologický	technologický	k2eAgInSc4d1	technologický
mantinel	mantinel	k1gInSc4	mantinel
<g/>
,	,	kIx,	,
daný	daný	k2eAgInSc1d1	daný
bariérovým	bariérový	k2eAgNnSc7d1	bariérové
napětím	napětí	k1gNnSc7	napětí
PN	PN	kA	PN
přechodu	přechod	k1gInSc6	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšování	zvyšování	k1gNnSc1	zvyšování
rychlosti	rychlost	k1gFnSc2	rychlost
procesorů	procesor	k1gInPc2	procesor
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
komponent	komponenta	k1gFnPc2	komponenta
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
umožněno	umožnit	k5eAaPmNgNnS	umožnit
snižováním	snižování	k1gNnSc7	snižování
napájecího	napájecí	k2eAgNnSc2d1	napájecí
napětí	napětí	k1gNnSc2	napětí
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
5	[number]	k4	5
Voltů	volt	k1gInPc2	volt
až	až	k9	až
na	na	k7c4	na
současných	současný	k2eAgInPc2d1	současný
1,2	[number]	k4	1,2
V.	V.	kA	V.
Napájecí	napájecí	k2eAgNnSc1d1	napájecí
napětí	napětí	k1gNnSc1	napětí
nelze	lze	k6eNd1	lze
snižovat	snižovat	k5eAaImF	snižovat
pod	pod	k7c4	pod
bariérové	bariérový	k2eAgNnSc4d1	bariérové
napětí	napětí	k1gNnSc4	napětí
křemíkového	křemíkový	k2eAgInSc2d1	křemíkový
PN	PN	kA	PN
přechodu	přechod	k1gInSc2	přechod
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
0,7	[number]	k4	0,7
V.	V.	kA	V.
Sice	sice	k8xC	sice
probíhá	probíhat	k5eAaImIp3nS	probíhat
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
výzkum	výzkum	k1gInSc4	výzkum
alternativních	alternativní	k2eAgInPc2d1	alternativní
polovodičů	polovodič	k1gInPc2	polovodič
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
technologická	technologický	k2eAgFnSc1d1	technologická
změna	změna	k1gFnSc1	změna
takových	takový	k3xDgInPc2	takový
rozměrů	rozměr	k1gInPc2	rozměr
bude	být	k5eAaImBp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
probíhat	probíhat	k5eAaImF	probíhat
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
technologický	technologický	k2eAgInSc1d1	technologický
mantinel	mantinel	k1gInSc1	mantinel
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
nárůstu	nárůst	k1gInSc6	nárůst
plochy	plocha	k1gFnSc2	plocha
čipu	čip	k1gInSc2	čip
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
problémem	problém	k1gInSc7	problém
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnPc1	světlo
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nutnosti	nutnost	k1gFnSc3	nutnost
hledat	hledat	k5eAaImF	hledat
energeticky	energeticky	k6eAd1	energeticky
úspornější	úsporný	k2eAgNnPc4d2	úspornější
obvodová	obvodový	k2eAgNnPc4d1	obvodové
zapojení	zapojení	k1gNnPc4	zapojení
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
integraci	integrace	k1gFnSc6	integrace
více	hodně	k6eAd2	hodně
procesorů	procesor	k1gInPc2	procesor
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
čip	čip	k1gInSc4	čip
roste	růst	k5eAaImIp3nS	růst
spotřeba	spotřeba	k1gFnSc1	spotřeba
lineárně	lineárně	k6eAd1	lineárně
<g/>
,	,	kIx,	,
při	při	k7c6	při
zvyšování	zvyšování	k1gNnSc6	zvyšování
výkonu	výkon	k1gInSc2	výkon
zrychlováním	zrychlování	k1gNnSc7	zrychlování
a	a	k8xC	a
zesložiťováním	zesložiťování	k1gNnSc7	zesložiťování
jednojádrového	jednojádrový	k2eAgInSc2d1	jednojádrový
procesoru	procesor	k1gInSc2	procesor
roste	růst	k5eAaImIp3nS	růst
spotřeba	spotřeba	k1gFnSc1	spotřeba
exponenciálně	exponenciálně	k6eAd1	exponenciálně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
jednodušších	jednoduchý	k2eAgNnPc2d2	jednodušší
jader	jádro	k1gNnPc2	jádro
může	moct	k5eAaImIp3nS	moct
zastat	zastat	k5eAaPmF	zastat
stejnou	stejný	k2eAgFnSc4d1	stejná
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc7d2	nižší
spotřebou	spotřeba	k1gFnSc7	spotřeba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
velkou	velký	k2eAgFnSc7d1	velká
překážkou	překážka	k1gFnSc7	překážka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
pro	pro	k7c4	pro
chod	chod	k1gInSc4	chod
na	na	k7c6	na
masivně	masivně	k6eAd1	masivně
paralelních	paralelní	k2eAgFnPc6d1	paralelní
architekturách	architektura	k1gFnPc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
výhody	výhoda	k1gFnPc1	výhoda
paralelních	paralelní	k2eAgFnPc2d1	paralelní
architektur	architektura	k1gFnPc2	architektura
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
ukážou	ukázat	k5eAaPmIp3nP	ukázat
v	v	k7c6	v
nových	nový	k2eAgFnPc6d1	nová
aplikacích	aplikace	k1gFnPc6	aplikace
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
a	a	k8xC	a
robotiky	robotika	k1gFnSc2	robotika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
osobním	osobní	k2eAgInSc7d1	osobní
počítačem	počítač	k1gInSc7	počítač
vybaveno	vybaven	k2eAgNnSc4d1	vybaveno
53	[number]	k4	53
%	%	kIx~	%
českých	český	k2eAgFnPc2d1	Česká
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
mělo	mít	k5eAaImAgNnS	mít
90	[number]	k4	90
%	%	kIx~	%
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
již	již	k6eAd1	již
mělo	mít	k5eAaImAgNnS	mít
osobní	osobní	k2eAgInSc4d1	osobní
počítač	počítač	k1gInSc4	počítač
73	[number]	k4	73
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
internet	internet	k1gInSc1	internet
používaly	používat	k5eAaImAgInP	používat
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Počítač	počítač	k1gInSc1	počítač
</s>
</p>
<p>
<s>
Hardware	hardware	k1gInSc1	hardware
</s>
</p>
<p>
<s>
Software	software	k1gInSc1	software
</s>
</p>
<p>
<s>
IBM	IBM	kA	IBM
Personal	Personal	k1gFnSc1	Personal
Computer	computer	k1gInSc1	computer
</s>
</p>
