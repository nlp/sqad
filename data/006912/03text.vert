<s>
Alina	Alina	k6eAd1	Alina
Talajová	Talajový	k2eAgFnSc1d1	Talajový
(	(	kIx(	(
<g/>
bělorusky	bělorusky	k6eAd1	bělorusky
А	А	k?	А
Т	Т	k?	Т
<g/>
;	;	kIx,	;
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Orša	Orša	k1gFnSc1	Orša
<g/>
,	,	kIx,	,
Vitebská	Vitebský	k2eAgFnSc1d1	Vitebská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
sprinterka	sprinterka	k1gFnSc1	sprinterka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
krátkým	krátký	k2eAgMnPc3d1	krátký
překážkovým	překážkový	k2eAgMnPc3d1	překážkový
běhům	běh	k1gInPc3	běh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
titul	titul	k1gInSc4	titul
halové	halový	k2eAgFnSc2d1	halová
mistryně	mistryně	k1gFnSc2	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c6	na
juniorském	juniorský	k2eAgNnSc6d1	juniorské
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Bydhošti	Bydhošti	k1gFnSc6	Bydhošti
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
skončila	skončit	k5eAaPmAgFnS	skončit
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
stupni	stupeň	k1gInPc7	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
Evropanek	Evropanka	k1gFnPc2	Evropanka
<g/>
.	.	kIx.	.
</s>
<s>
Bronz	bronz	k1gInSc4	bronz
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
v	v	k7c6	v
čase	čas	k1gInSc6	čas
13,49	[number]	k4	13,49
s	s	k7c7	s
Kubánka	Kubánek	k1gMnSc2	Kubánek
Belkis	Belkis	k1gFnSc1	Belkis
Milanésová	Milanésová	k1gFnSc1	Milanésová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
pouhou	pouhý	k2eAgFnSc4d1	pouhá
jednu	jeden	k4xCgFnSc4	jeden
setinu	setina	k1gFnSc4	setina
sekundy	sekunda	k1gFnSc2	sekunda
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc4	stříbro
získala	získat	k5eAaPmAgFnS	získat
Jamajčanka	Jamajčanka	k1gFnSc1	Jamajčanka
Shermaine	Shermain	k1gInSc5	Shermain
Williamsová	Williamsová	k1gFnSc1	Williamsová
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
Američanka	Američanka	k1gFnSc1	Američanka
Teona	Teona	k1gFnSc1	Teona
Rodgersová	Rodgersová	k1gFnSc1	Rodgersová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
v	v	k7c6	v
litevském	litevský	k2eAgInSc6d1	litevský
Kaunasu	Kaunas	k1gInSc6	Kaunas
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
halovém	halový	k2eAgInSc6d1	halový
MS	MS	kA	MS
2010	[number]	k4	2010
v	v	k7c6	v
katarském	katarský	k2eAgInSc6d1	katarský
Dauhá	Dauhý	k2eAgNnPc1d1	Dauhý
i	i	k9	i
na	na	k7c6	na
ME	ME	kA	ME
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
2010	[number]	k4	2010
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
postoupila	postoupit	k5eAaPmAgFnS	postoupit
z	z	k7c2	z
úvodních	úvodní	k2eAgInPc2d1	úvodní
rozběhů	rozběh	k1gInPc2	rozběh
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
finále	finále	k1gNnSc2	finále
se	se	k3xPyFc4	se
však	však	k9	však
neprobojovala	probojovat	k5eNaPmAgFnS	probojovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
ME	ME	kA	ME
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
doběhla	doběhnout	k5eAaPmAgFnS	doběhnout
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
v	v	k7c6	v
čase	čas	k1gInSc6	čas
7,98	[number]	k4	7,98
s	s	k7c7	s
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
mistryní	mistryně	k1gFnPc2	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
dokázala	dokázat	k5eAaPmAgFnS	dokázat
dostat	dostat	k5eAaPmF	dostat
pod	pod	k7c4	pod
13	[number]	k4	13
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
titul	titul	k1gInSc4	titul
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
čase	čas	k1gInSc6	čas
12,91	[number]	k4	12,91
s.	s.	k?	s.
Od	od	k7c2	od
rekordu	rekord	k1gInSc2	rekord
šampionátu	šampionát	k1gInSc2	šampionát
Švédky	Švédka	k1gFnSc2	Švédka
Susanny	Susanna	k1gFnSc2	Susanna
Kallurové	Kallurový	k2eAgFnSc2d1	Kallurová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
ji	on	k3xPp3gFnSc4	on
dělily	dělit	k5eAaImAgInP	dělit
tři	tři	k4xCgFnPc4	tři
setiny	setina	k1gFnPc4	setina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
uspěla	uspět	k5eAaPmAgFnS	uspět
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
MS	MS	kA	MS
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
doběhla	doběhnout	k5eAaPmAgFnS	doběhnout
v	v	k7c6	v
čase	čas	k1gInSc6	čas
7,97	[number]	k4	7,97
s	s	k7c7	s
pro	pro	k7c4	pro
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
setiny	setina	k1gFnPc4	setina
byla	být	k5eAaImAgFnS	být
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
jen	jen	k9	jen
Britka	Britka	k1gFnSc1	Britka
Tiffany	Tiffana	k1gFnSc2	Tiffana
Porterová	Porterová	k1gFnSc1	Porterová
a	a	k8xC	a
halovou	halový	k2eAgFnSc7d1	halová
mistryní	mistryně	k1gFnSc7	mistryně
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Sally	Salla	k1gFnPc1	Salla
Pearsonová	Pearsonový	k2eAgFnSc1d1	Pearsonový
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předvedla	předvést	k5eAaPmAgFnS	předvést
nejrychlejší	rychlý	k2eAgInPc4d3	nejrychlejší
časy	čas	k1gInPc4	čas
také	také	k9	také
v	v	k7c6	v
rozběhu	rozběh	k1gInSc6	rozběh
a	a	k8xC	a
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
cenný	cenný	k2eAgInSc4d1	cenný
kov	kov	k1gInSc4	kov
získala	získat	k5eAaPmAgFnS	získat
také	také	k9	také
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
nestačila	stačit	k5eNaBmAgFnS	stačit
jen	jen	k9	jen
na	na	k7c4	na
Nevin	nevina	k1gFnPc2	nevina
Yanı	Yanı	k1gMnPc4	Yanı
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
brala	brát	k5eAaImAgFnS	brát
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Bronz	bronz	k1gInSc4	bronz
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
její	její	k3xOp3gFnSc1	její
krajanka	krajanka	k1gFnSc1	krajanka
Jekatěrina	Jekatěrino	k1gNnSc2	Jekatěrino
Poplavská	Poplavský	k2eAgFnSc1d1	Poplavský
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
trenérem	trenér	k1gMnSc7	trenér
je	být	k5eAaImIp3nS	být
halový	halový	k2eAgMnSc1d1	halový
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
a	a	k8xC	a
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
medailista	medailista	k1gMnSc1	medailista
z	z	k7c2	z
halového	halový	k2eAgInSc2d1	halový
ME	ME	kA	ME
1977	[number]	k4	1977
Viktor	Viktor	k1gMnSc1	Viktor
Mjasnikov	Mjasnikov	k1gInSc1	Mjasnikov
<g/>
.	.	kIx.	.
60	[number]	k4	60
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
hala	hala	k1gFnSc1	hala
<g/>
)	)	kIx)	)
–	–	k?	–
7,85	[number]	k4	7,85
s	s	k7c7	s
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
NR	NR	kA	NR
<g/>
)	)	kIx)	)
100	[number]	k4	100
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
–	–	k?	–
12,71	[number]	k4	12,71
s	s	k7c7	s
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
