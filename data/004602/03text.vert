<s>
Antonymum	antonymum	k1gNnSc1	antonymum
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
α	α	k?	α
anti	ant	k1gMnSc3	ant
proti	proti	k7c3	proti
<g/>
;	;	kIx,	;
ὄ	ὄ	k?	ὄ
onoma	onoma	k1gFnSc1	onoma
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
opozitum	opozitum	k1gNnSc1	opozitum
označuje	označovat	k5eAaImIp3nS	označovat
slovo	slovo	k1gNnSc4	slovo
opačného	opačný	k2eAgInSc2d1	opačný
<g/>
,	,	kIx,	,
protikladného	protikladný	k2eAgInSc2d1	protikladný
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
slovo	slovo	k1gNnSc1	slovo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
antonym	antonymum	k1gNnPc2	antonymum
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
významy	význam	k1gInPc7	význam
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yRgInSc4	který
druh	druh	k1gInSc4	druh
antonymie	antonymie	k1gFnSc2	antonymie
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
kontextu	kontext	k1gInSc6	kontext
a	a	k8xC	a
na	na	k7c6	na
které	který	k3yIgFnSc6	který
významové	významový	k2eAgFnSc6d1	významová
škále	škála	k1gFnSc6	škála
k	k	k7c3	k
antonymii	antonymie	k1gFnSc3	antonymie
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
z	z	k7c2	z
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
synonymie	synonymie	k1gFnSc2	synonymie
a	a	k8xC	a
homonymie	homonymie	k1gFnSc2	homonymie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
náhodně	náhodně	k6eAd1	náhodně
(	(	kIx(	(
<g/>
nejsou	být	k5eNaImIp3nP	být
podmíněny	podmínit	k5eAaPmNgFnP	podmínit
vnější	vnější	k2eAgFnSc7d1	vnější
realitou	realita	k1gFnSc7	realita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
antonyma	antonymum	k1gNnPc1	antonymum
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
jazyce	jazyk	k1gInSc6	jazyk
přirozeně	přirozeně	k6eAd1	přirozeně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
významový	významový	k2eAgInSc1d1	významový
protiklad	protiklad	k1gInSc1	protiklad
k	k	k7c3	k
některému	některý	k3yIgInSc3	některý
výrazu	výraz	k1gInSc3	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
antonymní	antonymní	k2eAgInSc4d1	antonymní
protějšek	protějšek	k1gInSc4	protějšek
nemají	mít	k5eNaImIp3nP	mít
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
číslovky	číslovka	k1gFnPc1	číslovka
určité	určitý	k2eAgFnPc1d1	určitá
<g/>
,	,	kIx,	,
citoslovce	citoslovce	k1gNnSc1	citoslovce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antonyma	antonymum	k1gNnPc4	antonymum
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
polární	polární	k2eAgFnPc4d1	polární
<g/>
,	,	kIx,	,
komplementární	komplementární	k2eAgFnPc4d1	komplementární
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
antonymii	antonymie	k1gFnSc3	antonymie
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
antonyma	antonymum	k1gNnSc2	antonymum
<g/>
:	:	kIx,	:
konverzní	konverzní	k2eAgFnSc1d1	konverzní
kontextová	kontextový	k2eAgFnSc1d1	kontextová
<g/>
.	.	kIx.	.
</s>
<s>
Antonyma	antonymum	k1gNnPc1	antonymum
polární	polární	k2eAgNnPc1d1	polární
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
,	,	kIx,	,
graduální	graduální	k2eAgFnSc4d1	graduální
<g/>
,	,	kIx,	,
kontrastní	kontrastní	k2eAgFnSc4d1	kontrastní
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
logiky	logika	k1gFnSc2	logika
kontrární	kontrární	k2eAgFnPc1d1	kontrární
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
antonyma	antonymum	k1gNnPc1	antonymum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
protilehlé	protilehlý	k2eAgInPc4d1	protilehlý
body	bod	k1gInPc4	bod
na	na	k7c6	na
polární	polární	k2eAgFnSc6d1	polární
škále	škála	k1gFnSc6	škála
<g/>
,	,	kIx,	,
nepokrývají	pokrývat	k5eNaImIp3nP	pokrývat
však	však	k9	však
celý	celý	k2eAgInSc4d1	celý
rozsah	rozsah	k1gInSc4	rozsah
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
pojmu	pojem	k1gInSc2	pojem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
nebo	nebo	k8xC	nebo
vně	vně	k7c2	vně
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc1	jeden
další	další	k2eAgInSc1d1	další
stupeň	stupeň	k1gInSc1	stupeň
<g/>
:	:	kIx,	:
velký	velký	k2eAgInSc1d1	velký
×	×	k?	×
malý	malý	k2eAgInSc1d1	malý
obrovský	obrovský	k2eAgInSc1d1	obrovský
×	×	k?	×
malinký	malinký	k2eAgInSc1d1	malinký
teplý	teplý	k2eAgInSc1d1	teplý
×	×	k?	×
studený	studený	k2eAgInSc1d1	studený
sever	sever	k1gInSc1	sever
×	×	k?	×
jih	jih	k1gInSc1	jih
černý	černý	k2eAgInSc1d1	černý
×	×	k?	×
bílý	bílý	k2eAgInSc1d1	bílý
Komplementární	komplementární	k2eAgInSc1d1	komplementární
nebo	nebo	k8xC	nebo
též	též	k9	též
hlediska	hledisko	k1gNnPc1	hledisko
z	z	k7c2	z
logiky	logika	k1gFnSc2	logika
kontradiktorní	kontradiktorní	k2eAgNnPc1d1	kontradiktorní
antonyma	antonymum	k1gNnPc1	antonymum
svými	svůj	k3xOyFgInPc7	svůj
významy	význam	k1gInPc7	význam
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
rozsah	rozsah	k1gInSc4	rozsah
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
pojmu	pojem	k1gInSc2	pojem
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
dvojice	dvojice	k1gFnPc4	dvojice
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
druhé	druhý	k4xOgFnSc6	druhý
je	on	k3xPp3gInPc4	on
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
předponou	předpona	k1gFnSc7	předpona
ne-	ne-	k?	ne-
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
dvojice	dvojice	k1gFnPc1	dvojice
<g/>
:	:	kIx,	:
přítomný	přítomný	k2eAgInSc1d1	přítomný
×	×	k?	×
nepřítomný	přítomný	k2eNgMnSc1d1	nepřítomný
katolík	katolík	k1gMnSc1	katolík
×	×	k?	×
nekatolík	nekatolík	k1gMnSc1	nekatolík
pravda	pravda	k1gFnSc1	pravda
×	×	k?	×
lež	lež	k1gFnSc4	lež
hluchý	hluchý	k2eAgMnSc1d1	hluchý
×	×	k?	×
slyšící	slyšící	k2eAgMnSc1d1	slyšící
Některá	některý	k3yIgNnPc1	některý
antonyma	antonymum	k1gNnPc4	antonymum
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
polární	polární	k2eAgInPc4d1	polární
nebo	nebo	k8xC	nebo
komplementární	komplementární	k2eAgInPc4d1	komplementární
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
kontextech	kontext	k1gInPc6	kontext
<g/>
:	:	kIx,	:
muž	muž	k1gMnSc1	muž
<g />
.	.	kIx.	.
</s>
<s>
×	×	k?	×
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
v	v	k7c6	v
právním	právní	k2eAgInSc6d1	právní
a	a	k8xC	a
běžném	běžný	k2eAgInSc6d1	běžný
významu	význam	k1gInSc6	význam
komplementární	komplementární	k2eAgFnPc1d1	komplementární
<g/>
,	,	kIx,	,
biologicky	biologicky	k6eAd1	biologicky
i	i	k9	i
polární	polární	k2eAgFnSc1d1	polární
<g/>
)	)	kIx)	)
uvnitř	uvnitř	k7c2	uvnitř
×	×	k?	×
venku	venku	k6eAd1	venku
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nepočítá	počítat	k5eNaImIp3nS	počítat
s	s	k7c7	s
mezipolohou	mezipoloha	k1gFnSc7	mezipoloha
<g/>
)	)	kIx)	)
Mezi	mezi	k7c4	mezi
komplementární	komplementární	k2eAgNnPc4d1	komplementární
antonyma	antonymum	k1gNnPc4	antonymum
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
reverzní	reverzní	k2eAgNnPc4d1	reverzní
antonyma	antonymum	k1gNnPc4	antonymum
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
opačný	opačný	k2eAgInSc4d1	opačný
proces	proces	k1gInSc4	proces
nebo	nebo	k8xC	nebo
opačný	opačný	k2eAgInSc4d1	opačný
směr	směr	k1gInSc4	směr
(	(	kIx(	(
<g/>
vektorová	vektorový	k2eAgNnPc4d1	vektorové
antonyma	antonymum	k1gNnPc4	antonymum
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
vejít	vejít	k5eAaPmF	vejít
×	×	k?	×
vyjít	vyjít	k5eAaPmF	vyjít
od	od	k7c2	od
×	×	k?	×
k	k	k7c3	k
sestavit	sestavit	k5eAaPmF	sestavit
×	×	k?	×
rozebrat	rozebrat	k5eAaPmF	rozebrat
nakládka	nakládka	k1gFnSc1	nakládka
×	×	k?	×
vykládka	vykládka	k1gFnSc1	vykládka
svolaný	svolaný	k2eAgInSc1d1	svolaný
×	×	k?	×
rozpuštěný	rozpuštěný	k2eAgInSc1d1	rozpuštěný
Konverzní	konverzní	k2eAgInSc1d1	konverzní
nebo	nebo	k8xC	nebo
též	též	k9	též
vztahová	vztahový	k2eAgNnPc1d1	vztahové
antonyma	antonymum	k1gNnPc1	antonymum
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
takové	takový	k3xDgFnPc4	takový
dvojice	dvojice	k1gFnPc4	dvojice
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
jedno	jeden	k4xCgNnSc1	jeden
slovo	slovo	k1gNnSc1	slovo
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
vztah	vztah	k1gInSc4	vztah
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jednoho	jeden	k4xCgMnSc2	jeden
subjektu	subjekt	k1gInSc2	subjekt
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
slovo	slovo	k1gNnSc4	slovo
tentýž	týž	k3xTgInSc4	týž
vztah	vztah	k1gInSc4	vztah
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
druhého	druhý	k4xOgMnSc2	druhý
subjektu	subjekt	k1gInSc2	subjekt
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
rodič	rodič	k1gMnSc1	rodič
×	×	k?	×
dítě	dítě	k1gNnSc1	dítě
učitel	učitel	k1gMnSc1	učitel
×	×	k?	×
žák	žák	k1gMnSc1	žák
prodávat	prodávat	k5eAaImF	prodávat
×	×	k?	×
kupovat	kupovat	k5eAaImF	kupovat
Kontextová	kontextový	k2eAgFnSc1d1	kontextová
<g/>
,	,	kIx,	,
paralelní	paralelní	k2eAgNnPc1d1	paralelní
<g/>
,	,	kIx,	,
případová	případový	k2eAgNnPc1d1	případové
nebo	nebo	k8xC	nebo
nekompatibilní	kompatibilní	k2eNgNnPc1d1	nekompatibilní
antonyma	antonymum	k1gNnPc1	antonymum
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
dvojice	dvojice	k1gFnPc1	dvojice
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
kontextu	kontext	k1gInSc6	kontext
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
protiklady	protiklad	k1gInPc4	protiklad
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
dvěma	dva	k4xCgInPc7	dva
z	z	k7c2	z
více	hodně	k6eAd2	hodně
pojmů	pojem	k1gInPc2	pojem
téže	týž	k3xTgFnSc2	týž
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
:	:	kIx,	:
křesťan	křesťan	k1gMnSc1	křesťan
×	×	k?	×
muslim	muslim	k1gMnSc1	muslim
křesťan	křesťan	k1gMnSc1	křesťan
×	×	k?	×
<g />
.	.	kIx.	.
</s>
<s>
nevěřící	nevěřící	k1gFnSc4	nevěřící
mluvit	mluvit	k5eAaImF	mluvit
×	×	k?	×
zpívat	zpívat	k5eAaImF	zpívat
mluvit	mluvit	k5eAaImF	mluvit
×	×	k?	×
být	být	k5eAaImF	být
potichu	poticha	k1gFnSc4	poticha
Čech	Čechy	k1gFnPc2	Čechy
×	×	k?	×
Němec	Němec	k1gMnSc1	Němec
jablko	jablko	k1gNnSc4	jablko
×	×	k?	×
hruška	hruška	k1gFnSc1	hruška
černý	černý	k1gMnSc1	černý
×	×	k?	×
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
)	)	kIx)	)
elektrický	elektrický	k2eAgInSc4d1	elektrický
×	×	k?	×
plynový	plynový	k2eAgInSc4d1	plynový
(	(	kIx(	(
<g/>
sporák	sporák	k1gInSc4	sporák
<g/>
)	)	kIx)	)
Autoantonymum	Autoantonymum	k1gInSc4	Autoantonymum
<g/>
,	,	kIx,	,
též	též	k9	též
antagonymum	antagonymum	k1gInSc1	antagonymum
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
některé	některý	k3yIgInPc4	některý
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
některým	některý	k3yIgMnPc3	některý
z	z	k7c2	z
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgInPc2d1	uvedený
způsobů	způsob	k1gInPc2	způsob
protikladná	protikladný	k2eAgFnSc1d1	protikladná
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
homonymní	homonymní	k2eAgNnPc4d1	homonymní
antonyma	antonymum	k1gNnPc4	antonymum
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
například	například	k6eAd1	například
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
změnou	změna	k1gFnSc7	změna
významu	význam	k1gInSc2	význam
slova	slovo	k1gNnSc2	slovo
na	na	k7c4	na
protikladný	protikladný	k2eAgInSc4d1	protikladný
nebo	nebo	k8xC	nebo
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
či	či	k8xC	či
psanou	psaný	k2eAgFnSc7d1	psaná
shodou	shoda	k1gFnSc7	shoda
rozdílně	rozdílně	k6eAd1	rozdílně
vzniklých	vzniklý	k2eAgNnPc2d1	vzniklé
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
<g/>
:	:	kIx,	:
zřejmě	zřejmě	k6eAd1	zřejmě
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
zjevně	zjevně	k6eAd1	zjevně
<g/>
,	,	kIx,	,
pedofil	pedofil	k1gMnSc1	pedofil
–	–	k?	–
přítel	přítel	k1gMnSc1	přítel
dětí	dítě	k1gFnPc2	dítě
i	i	k9	i
vrah	vrah	k1gMnSc1	vrah
a	a	k8xC	a
prznitel	prznitel	k1gMnSc1	prznitel
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
soudruh	soudruh	k1gMnSc1	soudruh
–	–	k?	–
přítel	přítel	k1gMnSc1	přítel
i	i	k8xC	i
představitel	představitel	k1gMnSc1	představitel
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
mocenské	mocenský	k2eAgFnSc2d1	mocenská
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Autoantonyma	Autoantonymum	k1gNnPc1	Autoantonymum
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
pro	pro	k7c4	pro
archaické	archaický	k2eAgInPc4d1	archaický
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
čínštinu	čínština	k1gFnSc4	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
jsou	být	k5eAaImIp3nP	být
zachovaná	zachovaný	k2eAgNnPc1d1	zachované
například	například	k6eAd1	například
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
(	(	kIx(	(
<g/>
imago	imago	k6eAd1	imago
–	–	k?	–
obraz	obraz	k1gInSc1	obraz
i	i	k8xC	i
vzor	vzor	k1gInSc1	vzor
<g/>
,	,	kIx,	,
altus	altus	k1gInSc1	altus
–	–	k?	–
vysoký	vysoký	k2eAgInSc1d1	vysoký
i	i	k8xC	i
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
,	,	kIx,	,
clam	clam	k6eAd1	clam
–	–	k?	–
potichu	poticha	k1gFnSc4	poticha
×	×	k?	×
clamare	clamar	k1gMnSc5	clamar
–	–	k?	–
křičeti	křičet	k5eAaImF	křičet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řečtině	řečtina	k1gFnSc6	řečtina
(	(	kIx(	(
<g/>
farmakon	farmakon	k1gInSc1	farmakon
–	–	k?	–
lék	lék	k1gInSc1	lék
i	i	k8xC	i
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
hagios	hagios	k1gMnSc1	hagios
–	–	k?	–
svatý	svatý	k2eAgMnSc1d1	svatý
i	i	k8xC	i
ohavný	ohavný	k2eAgMnSc1d1	ohavný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
(	(	kIx(	(
<g/>
apparent	apparent	k1gInSc1	apparent
–	–	k?	–
zjevný	zjevný	k2eAgInSc1d1	zjevný
i	i	k8xC	i
domnělý	domnělý	k2eAgInSc1d1	domnělý
<g/>
,	,	kIx,	,
virtual	virtual	k1gInSc1	virtual
-	-	kIx~	-
opravdový	opravdový	k2eAgInSc1d1	opravdový
i	i	k8xC	i
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
slovní	slovní	k2eAgInPc1d1	slovní
kořeny	kořen	k1gInPc1	kořen
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
jazycích	jazyk	k1gInPc6	jazyk
nebo	nebo	k8xC	nebo
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
opačný	opačný	k2eAgInSc4d1	opačný
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
host	host	k1gMnSc1	host
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
,	,	kIx,	,
úžas	úžas	k1gInSc1	úžas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
hrůza	hrůza	k1gFnSc1	hrůza
<g/>
,	,	kIx,	,
strašlivý	strašlivý	k2eAgInSc1d1	strašlivý
ve	v	k7c6	v
staročeštině	staročeština	k1gFnSc6	staročeština
znamenalo	znamenat	k5eAaImAgNnS	znamenat
bojácný	bojácný	k2eAgMnSc1d1	bojácný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Synonymum	synonymum	k1gNnSc1	synonymum
Homonymum	homonymum	k1gNnSc1	homonymum
Hyperonymum	Hyperonymum	k1gInSc4	Hyperonymum
Frazém	frazém	k1gInSc1	frazém
Zápor	zápora	k1gFnPc2	zápora
KOLEKTIV	kolektivum	k1gNnPc2	kolektivum
AUTORŮ	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgNnPc2d1	české
synonym	synonymum	k1gNnPc2	synonymum
a	a	k8xC	a
antonym	antonymum	k1gNnPc2	antonymum
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Lingea	Lingea	k1gFnSc1	Lingea
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1	mluvnice
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
<g/>
,	,	kIx,	,
s.	s.	k?	s.
112	[number]	k4	112
<g/>
-	-	kIx~	-
<g/>
113	[number]	k4	113
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
antonymum	antonymum	k1gNnSc4	antonymum
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
