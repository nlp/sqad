<s>
Antonymum	antonymum	k1gNnSc1	antonymum
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
α	α	k?	α
anti	ant	k1gMnSc3	ant
proti	proti	k7c3	proti
<g/>
;	;	kIx,	;
ὄ	ὄ	k?	ὄ
onoma	onoma	k1gFnSc1	onoma
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
opozitum	opozitum	k1gNnSc1	opozitum
označuje	označovat	k5eAaImIp3nS	označovat
slovo	slovo	k1gNnSc4	slovo
opačného	opačný	k2eAgInSc2d1	opačný
<g/>
,	,	kIx,	,
protikladného	protikladný	k2eAgInSc2d1	protikladný
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
