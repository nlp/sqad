<s>
Taťánin	Taťánin	k2eAgInSc1d1	Taťánin
dopis	dopis	k1gInSc1	dopis
Oněginovi	Oněgin	k1gMnSc3	Oněgin
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
románu	román	k1gInSc2	román
složil	složit	k5eAaPmAgMnS	složit
ruský	ruský	k2eAgMnSc1d1	ruský
skladatel	skladatel	k1gMnSc1	skladatel
Petr	Petr	k1gMnSc1	Petr
Iljič	Iljič	k1gMnSc1	Iljič
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
