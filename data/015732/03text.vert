<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Oslu	Oslo	k1gNnSc6
</s>
<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Oslu	osel	k1gMnSc6
Místo	místo	k7c2
Stát	stát	k1gInSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
59	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
44,35	44,35	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
49,05	49,05	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc4
</s>
<s>
barokní	barokní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1697	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Oslu	Oslo	k1gNnSc6
<g/>
,	,	kIx,
norsky	norsky	k6eAd1
Oslo	Oslo	k1gNnSc1
domkirke	domkirk	k1gFnSc2
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Kostel	kostel	k1gInSc4
našeho	náš	k3xOp1gMnSc2
spasitele	spasitel	k1gMnSc2
<g/>
,	,	kIx,
norsky	norsky	k6eAd1
Vå	Vå	k1gInSc1
Frelsers	Frelsersa	k1gFnPc2
kirke	kirk	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
barokní	barokní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c6
norském	norský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Oslo	Oslo	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Stortorvet	Stortorveta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrám	chrám	k1gInSc1
užívá	užívat	k5eAaImIp3nS
protestantská	protestantský	k2eAgFnSc1d1
Norská	norský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gNnSc6
její	její	k3xOp3gMnSc1
biskup	biskup	k1gMnSc1
z	z	k7c2
Osla	Oslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postaven	postaven	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1694	#num#	k4
<g/>
-	-	kIx~
<g/>
1697	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
pravděpodobně	pravděpodobně	k6eAd1
navrhl	navrhnout	k5eAaPmAgInS
Jø	Jø	k1gInSc1
Wiggers	Wiggers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norská	norský	k2eAgFnSc1d1
královská	královský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
i	i	k8xC
norská	norský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
užívá	užívat	k5eAaImIp3nS
kostel	kostel	k1gInSc4
ke	k	k7c3
slavnostním	slavnostní	k2eAgFnPc3d1
událostem	událost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedrála	katedrála	k1gFnSc1
byla	být	k5eAaImAgFnS
přestavěna	přestavět	k5eAaPmNgFnS
v	v	k7c6
letech	let	k1gInPc6
1848	#num#	k4
<g/>
–	–	k?
<g/>
1850	#num#	k4
architekty	architekt	k1gMnPc4
Alexisem	Alexis	k1gInSc7
de	de	k?
Chateauneufem	Chateauneuf	k1gMnSc7
<g/>
,	,	kIx,
Heinrichem	Heinrich	k1gMnSc7
Ernstem	Ernst	k1gMnSc7
Schirmerem	Schirmer	k1gMnSc7
a	a	k8xC
Wilhelmem	Wilhelm	k1gInSc7
von	von	k1gInSc1
Hanno	Hanno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1616	#num#	k4
byly	být	k5eAaImAgFnP
instalovány	instalován	k2eAgFnPc1d1
vitráže	vitráž	k1gFnPc1
z	z	k7c2
dílny	dílna	k1gFnSc2
Emanuela	Emanuel	k1gMnSc2
Vigelanda	Vigelando	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
architekta	architekt	k1gMnSc2
Arnsteina	Arnstein	k1gMnSc2
Arneberga	Arneberg	k1gMnSc2
<g/>
,	,	kIx,
mj.	mj.	kA
autora	autor	k1gMnSc2
radnice	radnice	k1gFnSc2
v	v	k7c6
Oslu	Oslo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
odstranit	odstranit	k5eAaPmF
novogotické	novogotický	k2eAgInPc4d1
nánosy	nános	k1gInPc4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
zejm.	zejm.	k?
vnitřní	vnitřní	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
nejvíce	hodně	k6eAd3,k6eAd1
katedrálu	katedrála	k1gFnSc4
navrátit	navrátit	k5eAaPmF
do	do	k7c2
původního	původní	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Oslo	Oslo	k1gNnSc1
Cathedral	Cathedral	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Katedrála	katedrál	k1gMnSc2
v	v	k7c6
Oslu	Oslo	k1gNnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4681071-7	4681071-7	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
238783335	#num#	k4
</s>
