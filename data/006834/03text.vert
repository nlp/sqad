<s>
Jiří	Jiří	k1gMnSc1	Jiří
Matys	Matys	k1gMnSc1	Matys
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1927	[number]	k4	1927
Bakov	Bakovo	k1gNnPc2	Bakovo
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Františka	František	k1gMnSc2	František
Michálka	Michálek	k1gMnSc2	Michálek
a	a	k8xC	a
skladbu	skladba	k1gFnSc4	skladba
na	na	k7c6	na
JAMU	jam	k1gInSc6	jam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
vykonal	vykonat	k5eAaPmAgMnS	vykonat
státní	státní	k2eAgFnPc4d1	státní
zkoušky	zkouška	k1gFnPc4	zkouška
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Lidové	lidový	k2eAgFnSc2d1	lidová
školy	škola	k1gFnSc2	škola
umění	umění	k1gNnSc2	umění
v	v	k7c4	v
Brně-Královo	Brně-Králův	k2eAgNnSc4d1	Brně-Králův
Pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Ostravským	ostravský	k2eAgInSc7d1	ostravský
kvartetem	kvartet	k1gInSc7	kvartet
a	a	k8xC	a
ostravskou	ostravský	k2eAgFnSc7d1	Ostravská
konzervatoří	konzervatoř	k1gFnSc7	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
obligátní	obligátní	k2eAgInSc4d1	obligátní
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
korepetoval	korepetovat	k5eAaImAgMnS	korepetovat
na	na	k7c6	na
tanečním	taneční	k2eAgNnSc6d1	taneční
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
až	až	k9	až
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
Taneční	taneční	k2eAgFnSc2d1	taneční
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
převažují	převažovat	k5eAaImIp3nP	převažovat
komorní	komorní	k2eAgFnPc1d1	komorní
skladby	skladba	k1gFnPc1	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgFnPc2d1	tradiční
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
7	[number]	k4	7
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
kvartetů	kvartet	k1gInPc2	kvartet
<g/>
,	,	kIx,	,
dechová	dechový	k2eAgNnPc1d1	dechové
kvinteta	kvinteto	k1gNnPc1	kvinteto
<g/>
,	,	kIx,	,
sonáty	sonáta	k1gFnPc1	sonáta
<g/>
)	)	kIx)	)
psal	psát	k5eAaImAgInS	psát
hudbu	hudba	k1gFnSc4	hudba
i	i	k9	i
pro	pro	k7c4	pro
netradiční	tradiční	k2eNgFnPc4d1	netradiční
nástrojové	nástrojový	k2eAgFnPc4d1	nástrojová
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
řadu	řada	k1gFnSc4	řada
sborových	sborový	k2eAgFnPc2d1	sborová
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgNnPc7	jenž
vynikají	vynikat	k5eAaImIp3nP	vynikat
zejména	zejména	k9	zejména
dětské	dětský	k2eAgInPc1d1	dětský
sbory	sbor	k1gInPc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotná	hodnotný	k2eAgFnSc1d1	hodnotná
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnPc4	jeho
instruktivní	instruktivní	k2eAgNnPc4d1	instruktivní
díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
zobcovou	zobcový	k2eAgFnSc4d1	zobcová
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc4	akordeon
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
rovněž	rovněž	k9	rovněž
kantátu	kantáta	k1gFnSc4	kantáta
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
lidové	lidový	k2eAgFnSc2d1	lidová
poezie	poezie	k1gFnSc2	poezie
Ty	ten	k3xDgInPc1	ten
velické	velický	k2eAgInPc1d1	velický
zvony	zvon	k1gInPc1	zvon
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
soprán	soprán	k1gInSc4	soprán
<g/>
,	,	kIx,	,
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
varhany	varhany	k1gInPc4	varhany
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
17	[number]	k4	17
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
skladeb	skladba	k1gFnPc2	skladba
<g/>
:	:	kIx,	:
Orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
skladby	skladba	k1gFnSc2	skladba
Jitřní	jitřní	k2eAgFnSc1d1	jitřní
hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
trubky	trubka	k1gFnPc4	trubka
a	a	k8xC	a
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
33	[number]	k4	33
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
)	)	kIx)	)
Hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
smyčcové	smyčcový	k2eAgNnSc4d1	smyčcové
kvarteto	kvarteto	k1gNnSc4	kvarteto
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Symfonická	symfonický	k2eAgFnSc1d1	symfonická
předehra	předehra	k1gFnSc1	předehra
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
orchestr	orchestr	k1gInSc4	orchestr
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
)	)	kIx)	)
Hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
smyčce	smyčec	k1gInPc4	smyčec
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Naléhavost	naléhavost	k1gFnSc4	naléhavost
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Symfonický	symfonický	k2eAgInSc1d1	symfonický
obraz	obraz	k1gInSc1	obraz
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
s	s	k7c7	s
recitací	recitace	k1gFnSc7	recitace
XV	XV	kA	XV
<g/>
.	.	kIx.	.
sonetu	sonet	k1gInSc2	sonet
W.	W.	kA	W.
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
87	[number]	k4	87
<g/>
)	)	kIx)	)
Melodramy	melodram	k1gInPc1	melodram
Padlým	padlý	k2eAgNnPc3d1	padlé
v	v	k7c6	v
máji	máj	k1gInSc6	máj
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
symfonický	symfonický	k2eAgInSc1d1	symfonický
obraz	obraz	k1gInSc1	obraz
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
recitátora	recitátor	k1gMnSc2	recitátor
básně	báseň	k1gFnSc2	báseň
Jana	Jan	k1gMnSc2	Jan
Aldy	Alda	k1gFnSc2	Alda
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
7	[number]	k4	7
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Lyrické	lyrický	k2eAgInPc4d1	lyrický
melodramy	melodram	k1gInPc4	melodram
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
Josefa	Josef	k1gMnSc2	Josef
Kainara	Kainar	k1gMnSc2	Kainar
pro	pro	k7c4	pro
recitátora	recitátor	k1gMnSc4	recitátor
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
22	[number]	k4	22
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
pro	pro	k7c4	pro
smyčcové	smyčcový	k2eAgNnSc4d1	smyčcové
kvarteto	kvarteto	k1gNnSc4	kvarteto
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
a	a	k8xC	a
mužskou	mužský	k2eAgFnSc4d1	mužská
a	a	k8xC	a
ženskou	ženský	k2eAgFnSc4d1	ženská
recitaci	recitace	k1gFnSc4	recitace
básně	báseň	k1gFnSc2	báseň
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
27	[number]	k4	27
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
pro	pro	k7c4	pro
Viléma	Vilém	k1gMnSc4	Vilém
Závadu	závada	k1gFnSc4	závada
<g/>
,	,	kIx,	,
recitační	recitační	k2eAgInSc4d1	recitační
cyklus	cyklus	k1gInSc4	cyklus
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
flétny	flétna	k1gFnSc2	flétna
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
)	)	kIx)	)
Jarní	jarní	k2eAgNnPc4d1	jarní
bubnování	bubnování	k1gNnPc4	bubnování
<g/>
.	.	kIx.	.
</s>
<s>
Melodram	melodram	k1gInSc1	melodram
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Mikuláška	Mikulášek	k1gMnSc2	Mikulášek
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Chvíle	chvíle	k1gFnSc1	chvíle
zastavení	zastavení	k1gNnSc2	zastavení
<g/>
.	.	kIx.	.
4	[number]	k4	4
melodramy	melodram	k1gInPc7	melodram
na	na	k7c4	na
básně	báseň	k1gFnPc4	báseň
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
flétny	flétna	k1gFnSc2	flétna
a	a	k8xC	a
violy	viola	k1gFnSc2	viola
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
To	ten	k3xDgNnSc1	ten
laštovka	laštovka	k?	laštovka
jaro	jaro	k1gNnSc1	jaro
odnáší	odnášet	k5eAaImIp3nS	odnášet
<g/>
...	...	k?	...
čínská	čínský	k2eAgFnSc1d1	čínská
poezie	poezie	k1gFnSc1	poezie
v	v	k7c6	v
překladech	překlad	k1gInPc6	překlad
Bohumila	Bohumil	k1gMnSc2	Bohumil
Mathesia	Mathesius	k1gMnSc2	Mathesius
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
J.	J.	kA	J.
Suka	Suk	k1gMnSc2	Suk
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
soutěži	soutěž	k1gFnSc6	soutěž
SČS	SČS	kA	SČS
a	a	k8xC	a
ČHF	ČHF	kA	ČHF
(	(	kIx(	(
<g/>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
Cena	cena	k1gFnSc1	cena
ve	v	k7c6	v
skladatelské	skladatelský	k2eAgFnSc6d1	skladatelská
soutěži	soutěž	k1gFnSc6	soutěž
SACEM	SACEM	kA	SACEM
<g/>
/	/	kIx~	/
<g/>
UPAC	UPAC	kA	UPAC
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
variace	variace	k1gFnSc1	variace
pro	pro	k7c4	pro
akordeon	akordeon	k1gInSc4	akordeon
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Cena	cena	k1gFnSc1	cena
Svazu	svaz	k1gInSc2	svaz
českých	český	k2eAgMnPc2d1	český
skladatelů	skladatel	k1gMnPc2	skladatel
(	(	kIx(	(
<g/>
Poetické	poetický	k2eAgFnSc2d1	poetická
věty	věta	k1gFnSc2	věta
III	III	kA	III
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Cena	cena	k1gFnSc1	cena
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
Festivalu	festival	k1gInSc2	festival
sborového	sborový	k2eAgNnSc2d1	sborové
umění	umění	k1gNnSc2	umění
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
Vysočině	vysočina	k1gFnSc6	vysočina
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Cena	cena	k1gFnSc1	cena
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
duchovní	duchovní	k2eAgFnSc2d1	duchovní
hudby	hudba	k1gFnSc2	hudba
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
č.	č.	k?	č.
3	[number]	k4	3
"	"	kIx"	"
<g/>
Variační	variační	k2eAgFnSc2d1	variační
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Cena	cena	k1gFnSc1	cena
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
-	-	kIx~	-
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Salajková	Salajková	k1gFnSc1	Salajková
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Matys	Matys	k1gMnSc1	Matys
a	a	k8xC	a
Matysovo	Matysův	k2eAgNnSc1d1	Matysův
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2008	[number]	k4	2008
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Matys	Matys	k1gMnSc1	Matys
Životopis	životopis	k1gInSc4	životopis
Jiřího	Jiří	k1gMnSc2	Jiří
Matyse	Matys	k1gMnSc2	Matys
Životopis	životopis	k1gInSc1	životopis
</s>
