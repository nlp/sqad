<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
několik	několik	k4yIc4	několik
pokusů	pokus	k1gInPc2	pokus
vytvořit	vytvořit	k5eAaPmF	vytvořit
čistě	čistě	k6eAd1	čistě
surrealistické	surrealistický	k2eAgInPc4d1	surrealistický
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
nejznámější	známý	k2eAgInPc4d3	nejznámější
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
filmy	film	k1gInPc1	film
od	od	k7c2	od
Luise	Luisa	k1gFnSc3	Luisa
Bunuela	Bunuel	k1gMnSc4	Bunuel
Andaluský	andaluský	k2eAgMnSc1d1	andaluský
pes	pes	k1gMnSc1	pes
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
<g/>
.	.	kIx.	.
</s>
