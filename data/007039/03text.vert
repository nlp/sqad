<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
deuteria	deuterium	k1gNnSc2	deuterium
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
molekuly	molekula	k1gFnPc1	molekula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
místo	místo	k7c2	místo
obou	dva	k4xCgInPc2	dva
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
jeho	jeho	k3xOp3gInSc1	jeho
izotop	izotop	k1gInSc1	izotop
deuterium	deuterium	k1gNnSc1	deuterium
-	-	kIx~	-
tedy	tedy	k9	tedy
vodík	vodík	k1gInSc1	vodík
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
tvořeným	tvořený	k2eAgNnSc7d1	tvořené
1	[number]	k4	1
protonem	proton	k1gInSc7	proton
a	a	k8xC	a
1	[number]	k4	1
neutronem	neutron	k1gInSc7	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
běžné	běžný	k2eAgFnSc3d1	běžná
vodě	voda	k1gFnSc3	voda
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
D2O	D2O	k1gFnPc4	D2O
mírně	mírně	k6eAd1	mírně
odlišné	odlišný	k2eAgFnPc4d1	odlišná
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
lehká	lehký	k2eAgFnSc1d1	lehká
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
množstvích	množství	k1gNnPc6	množství
je	být	k5eAaImIp3nS	být
zdraví	zdravit	k5eAaImIp3nS	zdravit
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
narušuje	narušovat	k5eAaImIp3nS	narušovat
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
rovnováhu	rovnováha	k1gFnSc4	rovnováha
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
polotěžká	polotěžký	k2eAgFnSc1d1	polotěžká
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
HDO	HDO	kA	HDO
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
běžného	běžný	k2eAgInSc2d1	běžný
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
protia	protia	k1gFnSc1	protia
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
koncentracích	koncentrace	k1gFnPc6	koncentrace
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
radioaktivní	radioaktivní	k2eAgFnSc1d1	radioaktivní
tritiová	tritiový	k2eAgFnSc1d1	tritiová
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
atomy	atom	k1gInPc4	atom
tritia	tritium	k1gNnSc2	tritium
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
existují	existovat	k5eAaImIp3nP	existovat
sloučeniny	sloučenina	k1gFnPc1	sloučenina
HTO	HTO	kA	HTO
a	a	k8xC	a
DTO	DTO	kA	DTO
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
běžná	běžný	k2eAgFnSc1d1	běžná
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
horší	zlý	k2eAgFnPc4d2	horší
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
organismů	organismus	k1gInPc2	organismus
mírně	mírně	k6eAd1	mírně
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
na	na	k7c6	na
myších	myš	k1gFnPc6	myš
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
buněčné	buněčný	k2eAgNnSc1d1	buněčné
dělení	dělení	k1gNnSc1	dělení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
obnovujících	obnovující	k2eAgFnPc6d1	obnovující
tkáních	tkáň	k1gFnPc6	tkáň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
žaludeční	žaludeční	k2eAgFnSc6d1	žaludeční
stěně	stěna	k1gFnSc6	stěna
<g/>
)	)	kIx)	)
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
opakovaném	opakovaný	k2eAgNnSc6d1	opakované
působení	působení	k1gNnSc6	působení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
myší	myš	k1gFnPc2	myš
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
%	%	kIx~	%
jejich	jejich	k3xOp3gFnPc2	jejich
tělních	tělní	k2eAgFnPc2d1	tělní
tekutin	tekutina	k1gFnPc2	tekutina
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
těžkou	těžký	k2eAgFnSc7d1	těžká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
agresivní	agresivní	k2eAgNnPc1d1	agresivní
rakovinná	rakovinný	k2eAgNnPc1d1	rakovinné
onemocnění	onemocnění	k1gNnPc1	onemocnění
by	by	kYmCp3nP	by
jí	on	k3xPp3gFnSc3	on
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
zpomalována	zpomalován	k2eAgFnSc1d1	zpomalována
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
takové	takový	k3xDgFnSc2	takový
terapie	terapie	k1gFnSc2	terapie
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
však	však	k9	však
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
vyváženy	vyvážen	k2eAgFnPc1d1	vyvážena
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
10,6	[number]	k4	10,6
%	%	kIx~	%
větší	veliký	k2eAgFnSc4d2	veliký
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
běžně	běžně	k6eAd1	běžně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
zředěna	zředěn	k2eAgFnSc1d1	zředěna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
izolaci	izolace	k1gFnSc6	izolace
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vazba	vazba	k1gFnSc1	vazba
D-O	D-O	k1gFnSc2	D-O
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
H-	H-	k1gFnSc1	H-
<g/>
O.	O.	kA	O.
To	to	k9	to
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
běžné	běžný	k2eAgFnSc2d1	běžná
směsi	směs	k1gFnSc2	směs
těžké	těžký	k2eAgFnSc2d1	těžká
a	a	k8xC	a
lehké	lehký	k2eAgFnSc2d1	lehká
vody	voda	k1gFnSc2	voda
dochází	docházet	k5eAaImIp3nS	docházet
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
koncentrovanější	koncentrovaný	k2eAgInSc4d2	koncentrovanější
roztok	roztok	k1gInSc4	roztok
D2O	D2O	k1gFnSc2	D2O
a	a	k8xC	a
DHO	DHO	kA	DHO
(	(	kIx(	(
<g/>
polotěžké	polotěžký	k2eAgFnSc2d1	polotěžká
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
odebere	odebrat	k5eAaPmIp3nS	odebrat
zbylé	zbylý	k2eAgNnSc1d1	zbylé
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
elektrolytu	elektrolyt	k1gInSc2	elektrolyt
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
metodou	metoda	k1gFnSc7	metoda
ještě	ještě	k6eAd1	ještě
přečišťuje	přečišťovat	k5eAaImIp3nS	přečišťovat
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
moderátor	moderátor	k1gInSc1	moderátor
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
těžkovodní	těžkovodní	k2eAgFnSc1d1	těžkovodní
reaktor	reaktor	k1gInSc1	reaktor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
typ	typ	k1gInSc4	typ
Candu	Cand	k1gInSc2	Cand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
stejně	stejně	k6eAd1	stejně
silném	silný	k2eAgInSc6d1	silný
moderačním	moderační	k2eAgInSc6d1	moderační
účinku	účinek	k1gInSc6	účinek
pohltí	pohltit	k5eAaPmIp3nS	pohltit
méně	málo	k6eAd2	málo
neutronů	neutron	k1gInPc2	neutron
než	než	k8xS	než
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
upustit	upustit	k5eAaPmF	upustit
od	od	k7c2	od
<g/>
,	,	kIx,	,
u	u	k7c2	u
lehkovodních	lehkovodní	k2eAgInPc2d1	lehkovodní
reaktorů	reaktor	k1gInPc2	reaktor
nutného	nutný	k2eAgInSc2d1	nutný
<g/>
,	,	kIx,	,
obohacování	obohacování	k1gNnSc4	obohacování
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NMR	NMR	kA	NMR
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
se	se	k3xPyFc4	se
těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
stabilizaci	stabilizace	k1gFnSc4	stabilizace
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
protonových	protonový	k2eAgFnPc2d1	protonová
NMR	NMR	kA	NMR
spekter	spektrum	k1gNnPc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
ostatních	ostatní	k2eAgFnPc2d1	ostatní
sloučenin	sloučenina	k1gFnPc2	sloučenina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
deuterium	deuterium	k1gNnSc4	deuterium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1942-1944	[number]	k4	1942-1944
byl	být	k5eAaImAgInS	být
norský	norský	k2eAgInSc1d1	norský
Rjukan	Rjukan	k1gInSc1	Rjukan
místem	místem	k6eAd1	místem
velmi	velmi	k6eAd1	velmi
ostrých	ostrý	k2eAgInPc2d1	ostrý
střetů	střet	k1gInPc2	střet
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Vemork	Vemork	k1gInSc1	Vemork
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
<g/>
,	,	kIx,	,
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc1d1	jediné
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Norsk	Norsk	k1gInSc4	Norsk
Hydro	hydra	k1gFnSc5	hydra
<g/>
,	,	kIx,	,
těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k6eAd1	jen
díky	díky	k7c3	díky
obrovskému	obrovský	k2eAgInSc3d1	obrovský
přebytku	přebytek	k1gInSc3	přebytek
energie	energie	k1gFnSc2	energie
poskytovaném	poskytovaný	k2eAgInSc6d1	poskytovaný
vodní	vodní	k2eAgNnSc4d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
odhalili	odhalit	k5eAaPmAgMnP	odhalit
němečtí	německý	k2eAgMnPc1d1	německý
vědci	vědec	k1gMnPc1	vědec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Wernera	Werner	k1gMnSc2	Werner
Heisenberga	Heisenberg	k1gMnSc2	Heisenberg
princip	princip	k1gInSc4	princip
nukleární	nukleární	k2eAgFnSc2d1	nukleární
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gNnPc7	on
a	a	k8xC	a
spojenci	spojenec	k1gMnPc1	spojenec
"	"	kIx"	"
<g/>
závod	závod	k1gInSc1	závod
<g/>
"	"	kIx"	"
o	o	k7c4	o
převzetí	převzetí	k1gNnSc4	převzetí
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
továrnou	továrna	k1gFnSc7	továrna
Norsk	Norsk	k1gInSc4	Norsk
Hydro	hydra	k1gFnSc5	hydra
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Němci	Němec	k1gMnPc1	Němec
plánovali	plánovat	k5eAaImAgMnP	plánovat
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
využít	využít	k5eAaPmF	využít
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pokusných	pokusný	k2eAgInPc6d1	pokusný
reaktorech	reaktor	k1gInPc6	reaktor
jako	jako	k8xC	jako
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
reaktorech	reaktor	k1gInPc6	reaktor
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vyráběno	vyráběn	k2eAgNnSc4d1	vyráběno
plutonium	plutonium	k1gNnSc4	plutonium
pro	pro	k7c4	pro
válečné	válečný	k2eAgInPc4d1	válečný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgNnPc1d1	výrobní
zařízení	zařízení	k1gNnPc1	zařízení
v	v	k7c6	v
Rjukanu	Rjukan	k1gInSc6	Rjukan
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
zorného	zorný	k2eAgNnSc2d1	zorné
pole	pole	k1gNnSc2	pole
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gNnSc7	jeho
zničením	zničení	k1gNnSc7	zničení
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
program	program	k1gInSc1	program
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
zbraně	zbraň	k1gFnSc2	zbraň
zastaven	zastavit	k5eAaPmNgMnS	zastavit
jedinou	jediný	k2eAgFnSc7d1	jediná
ranou	rána	k1gFnSc7	rána
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
pokusech	pokus	k1gInPc6	pokus
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
podařilo	podařit	k5eAaPmAgNnS	podařit
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
osmi	osm	k4xCc2	osm
členů	člen	k1gMnPc2	člen
norské	norský	k2eAgFnSc2d1	norská
speciální	speciální	k2eAgFnSc2d1	speciální
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
schovali	schovat	k5eAaPmAgMnP	schovat
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
Hardangervidda	Hardangerviddo	k1gNnSc2	Hardangerviddo
<g/>
,	,	kIx,	,
odstřelit	odstřelit	k5eAaPmF	odstřelit
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c4	na
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Norsk	Norsk	k1gInSc4	Norsk
Hydro	hydra	k1gFnSc5	hydra
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Operace	operace	k1gFnSc1	operace
Telemark	telemark	k1gInSc1	telemark
<g/>
)	)	kIx)	)
Po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
byly	být	k5eAaImAgFnP	být
však	však	k9	však
škody	škoda	k1gFnPc1	škoda
opraveny	opraven	k2eAgFnPc1d1	opravena
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
opět	opět	k6eAd1	opět
spustili	spustit	k5eAaPmAgMnP	spustit
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
tedy	tedy	k9	tedy
další	další	k2eAgInPc1d1	další
bombové	bombový	k2eAgInPc1d1	bombový
útoky	útok	k1gInPc1	útok
spojenců	spojenec	k1gMnPc2	spojenec
až	až	k6eAd1	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
okupanti	okupant	k1gMnPc1	okupant
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
provoz	provoz	k1gInSc4	provoz
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
odvézt	odvézt	k5eAaPmF	odvézt
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
50	[number]	k4	50
již	již	k6eAd1	již
hotových	hotový	k2eAgInPc2d1	hotový
sudů	sud	k1gInPc2	sud
těžké	těžký	k2eAgFnSc2d1	těžká
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Její	její	k3xOp3gFnSc1	její
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
1	[number]	k4	1
a	a	k8xC	a
99	[number]	k4	99
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Hydro	hydra	k1gFnSc5	hydra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
převážející	převážející	k2eAgFnPc1d1	převážející
tento	tento	k3xDgInSc4	tento
náklad	náklad	k1gInSc4	náklad
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
představoval	představovat	k5eAaImAgMnS	představovat
zhruba	zhruba	k6eAd1	zhruba
desetinu	desetina	k1gFnSc4	desetina
množství	množství	k1gNnSc2	množství
potřebného	potřebné	k1gNnSc2	potřebné
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
spáchán	spáchán	k2eAgInSc4d1	spáchán
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Výbušnina	výbušnina	k1gFnSc1	výbušnina
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
ve	v	k7c6	v
strojovně	strojovna	k1gFnSc6	strojovna
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
potopila	potopit	k5eAaPmAgFnS	potopit
během	během	k7c2	během
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
430	[number]	k4	430
metrů	metr	k1gInPc2	metr
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
jezera	jezero	k1gNnSc2	jezero
Tinnsjø	Tinnsjø	k1gFnSc2	Tinnsjø
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc1	několik
sudů	sud	k1gInPc2	sud
nezmizelo	zmizet	k5eNaPmAgNnS	zmizet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
byly	být	k5eAaImAgInP	být
odeslány	odeslat	k5eAaPmNgInP	odeslat
(	(	kIx(	(
<g/>
Očití	očitý	k2eAgMnPc1d1	očitý
svědkové	svědek	k1gMnPc1	svědek
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
po	po	k7c4	po
potopení	potopení	k1gNnSc4	potopení
lodi	loď	k1gFnSc2	loď
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vyplavalo	vyplavat	k5eAaPmAgNnS	vyplavat
několik	několik	k4yIc1	několik
kovových	kovový	k2eAgInPc2d1	kovový
sudů	sud	k1gInPc2	sud
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
Němci	Němec	k1gMnPc1	Němec
vylovili	vylovit	k5eAaPmAgMnP	vylovit
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
sudech	sud	k1gInPc6	sud
bylo	být	k5eAaImAgNnS	být
-	-	kIx~	-
nebylo	být	k5eNaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
anebo	anebo	k8xC	anebo
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
zmatení	zmatení	k1gNnSc6	zmatení
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dokládal	dokládat	k5eAaImAgInS	dokládat
detailní	detailní	k2eAgInSc1d1	detailní
seznam	seznam	k1gInSc1	seznam
naložených	naložený	k2eAgFnPc2d1	naložená
věcí	věc	k1gFnPc2	věc
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
Hydro	hydra	k1gFnSc5	hydra
<g/>
,	,	kIx,	,
s	s	k7c7	s
detailním	detailní	k2eAgInSc7d1	detailní
popisem	popis	k1gInSc7	popis
množství	množství	k1gNnSc2	množství
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
sudů	sud	k1gInPc2	sud
s	s	k7c7	s
těžkou	těžký	k2eAgFnSc7d1	těžká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
tým	tým	k1gInSc1	tým
vědců	vědec	k1gMnPc2	vědec
prozkoumával	prozkoumávat	k5eAaImAgInS	prozkoumávat
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
pátraje	pátrat	k5eAaImSgMnS	pátrat
po	po	k7c6	po
neporušených	porušený	k2eNgInPc6d1	neporušený
sudech	sud	k1gInPc6	sud
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
jich	on	k3xPp3gMnPc2	on
nalezli	naleznout	k5eAaPmAgMnP	naleznout
na	na	k7c6	na
potopené	potopený	k2eAgFnSc6d1	potopená
lodi	loď	k1gFnSc6	loď
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
asi	asi	k9	asi
60	[number]	k4	60
m	m	kA	m
od	od	k7c2	od
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vylovili	vylovit	k5eAaPmAgMnP	vylovit
<g/>
.	.	kIx.	.
</s>
<s>
Změřili	změřit	k5eAaPmAgMnP	změřit
pH	ph	kA	ph
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
potom	potom	k8xC	potom
pH	ph	kA	ph
obsahu	obsah	k1gInSc2	obsah
sudu	sud	k1gInSc2	sud
-	-	kIx~	-
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
opatrností	opatrnost	k1gFnSc7	opatrnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
těžké	těžký	k2eAgFnSc2d1	těžká
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
a	a	k8xC	a
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
přidával	přidávat	k5eAaImAgInS	přidávat
louh	louh	k1gInSc1	louh
(	(	kIx(	(
<g/>
Hydroxid	hydroxid	k1gInSc1	hydroxid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
vodivosti	vodivost	k1gFnSc2	vodivost
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
měl	mít	k5eAaImAgInS	mít
jiné	jiný	k2eAgFnPc4d1	jiná
pH	ph	kA	ph
než	než	k8xS	než
pH	ph	kA	ph
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
měřením	měření	k1gNnSc7	měření
vědci	vědec	k1gMnPc1	vědec
odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sud	sud	k1gInSc4	sud
s	s	k7c7	s
těžkou	těžký	k2eAgFnSc7d1	těžká
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pH	ph	kA	ph
bude	být	k5eAaImBp3nS	být
14	[number]	k4	14
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
okamžité	okamžitý	k2eAgNnSc1d1	okamžité
měření	měření	k1gNnSc1	měření
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
-	-	kIx~	-
vzorek	vzorek	k1gInSc1	vzorek
vody	voda	k1gFnSc2	voda
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
důkladnější	důkladný	k2eAgInSc4d2	důkladnější
rozbor	rozbor	k1gInSc4	rozbor
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
napůl	napůl	k6eAd1	napůl
zrezlém	zrezlý	k2eAgInSc6d1	zrezlý
sudu	sud	k1gInSc6	sud
bylo	být	k5eAaImAgNnS	být
čitelné	čitelný	k2eAgNnSc1d1	čitelné
jeho	jeho	k3xOp3gNnSc4	jeho
číselné	číselný	k2eAgNnSc4d1	číselné
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
sud	sud	k1gInSc1	sud
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
naložených	naložený	k2eAgFnPc2d1	naložená
věcí	věc	k1gFnPc2	věc
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
Hydro	hydra	k1gFnSc5	hydra
a	a	k8xC	a
tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
pravost	pravost	k1gFnSc1	pravost
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
potopení	potopení	k1gNnSc6	potopení
Hydra	hydra	k1gFnSc1	hydra
<g/>
,	,	kIx,	,
do	do	k7c2	do
německého	německý	k2eAgNnSc2d1	německé
města	město	k1gNnSc2	město
Haigerloch	Haigerloch	k1gInSc1	Haigerloch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
stál	stát	k5eAaImAgInS	stát
pokusný	pokusný	k2eAgInSc1d1	pokusný
reaktor	reaktor	k1gInSc1	reaktor
Wernera	Werner	k1gMnSc2	Werner
Heisenberga	Heisenberg	k1gMnSc2	Heisenberg
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
až	až	k9	až
do	do	k7c2	do
úplného	úplný	k2eAgInSc2d1	úplný
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
jak	jak	k6eAd1	jak
vyvolat	vyvolat	k5eAaPmF	vyvolat
jadernou	jaderný	k2eAgFnSc4d1	jaderná
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
potopil	potopit	k5eAaPmAgInS	potopit
drátěný	drátěný	k2eAgInSc1d1	drátěný
model	model	k1gInSc1	model
se	s	k7c7	s
664	[number]	k4	664
uranovými	uranový	k2eAgFnPc7d1	uranová
kostkami	kostka	k1gFnPc7	kostka
do	do	k7c2	do
kovového	kovový	k2eAgInSc2d1	kovový
kontejneru	kontejner	k1gInSc2	kontejner
naplněného	naplněný	k2eAgInSc2d1	naplněný
těžkou	těžký	k2eAgFnSc7d1	těžká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kritický	kritický	k2eAgInSc1d1	kritický
bod	bod	k1gInSc1	bod
pro	pro	k7c4	pro
spuštění	spuštění	k1gNnSc4	spuštění
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
překonal	překonat	k5eAaPmAgMnS	překonat
jen	jen	k9	jen
o	o	k7c4	o
pár	pár	k4xCyI	pár
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
narazilo	narazit	k5eAaPmAgNnS	narazit
speciální	speciální	k2eAgNnSc1d1	speciální
americké	americký	k2eAgNnSc1d1	americké
komando	komando	k1gNnSc1	komando
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
experimentální	experimentální	k2eAgInSc4d1	experimentální
napůl	napůl	k6eAd1	napůl
rozestavěný	rozestavěný	k2eAgInSc4d1	rozestavěný
reaktor	reaktor	k1gInSc4	reaktor
ve	v	k7c6	v
sklepení	sklepení	k1gNnSc6	sklepení
pod	pod	k7c7	pod
zámeckým	zámecký	k2eAgInSc7d1	zámecký
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
předtím	předtím	k6eAd1	předtím
opustil	opustit	k5eAaPmAgMnS	opustit
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
tuto	tento	k3xDgFnSc4	tento
tajnou	tajný	k2eAgFnSc4d1	tajná
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
internován	internovat	k5eAaBmNgInS	internovat
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pátrání	pátrání	k1gNnSc2	pátrání
BBC	BBC	kA	BBC
dodala	dodat	k5eAaPmAgFnS	dodat
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Izraeli	Izrael	k1gInSc3	Izrael
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
dvacet	dvacet	k4xCc1	dvacet
tun	tuna	k1gFnPc2	tuna
těžké	těžký	k2eAgFnSc2d1	těžká
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
izraelského	izraelský	k2eAgInSc2d1	izraelský
jaderného	jaderný	k2eAgInSc2d1	jaderný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
dodávce	dodávka	k1gFnSc6	dodávka
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
i	i	k9	i
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
britského	britský	k2eAgInSc2d1	britský
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
uvedeného	uvedený	k2eAgInSc2d1	uvedený
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
použita	použít	k5eAaPmNgFnS	použít
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
plutonia	plutonium	k1gNnSc2	plutonium
v	v	k7c6	v
přísně	přísně	k6eAd1	přísně
střežené	střežený	k2eAgFnSc6d1	střežená
elektrárně	elektrárna	k1gFnSc6	elektrárna
Dimona	Dimona	k1gFnSc1	Dimona
v	v	k7c6	v
Negevské	Negevský	k2eAgFnSc6d1	Negevská
poušti	poušť	k1gFnSc6	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
Tritiová	Tritiový	k2eAgFnSc1d1	Tritiový
voda	voda	k1gFnSc1	voda
</s>
