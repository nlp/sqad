<s>
Zednická	zednický	k2eAgFnSc1d1	zednická
lžíce	lžíce	k1gFnSc1	lžíce
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
nástroj	nástroj	k1gInSc4	nástroj
zedníka	zedník	k1gMnSc2	zedník
<g/>
,	,	kIx,	,
omítkáře	omítkář	k1gMnSc2	omítkář
<g/>
,	,	kIx,	,
štukatéra	štukatér	k1gMnSc2	štukatér
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
zdění	zdění	k1gNnSc3	zdění
a	a	k8xC	a
k	k	k7c3	k
omítání	omítání	k1gNnSc3	omítání
zdiva	zdivo	k1gNnSc2	zdivo
cihlového	cihlový	k2eAgNnSc2d1	cihlové
<g/>
,	,	kIx,	,
tvárnicového	tvárnicový	k2eAgNnSc2d1	tvárnicové
a	a	k8xC	a
kamenného	kamenný	k2eAgNnSc2d1	kamenné
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
lžic	lžíce	k1gFnPc2	lžíce
<g/>
,	,	kIx,	,
mimoto	mimoto	k6eAd1	mimoto
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
zedníci	zedník	k1gMnPc1	zedník
individuálně	individuálně	k6eAd1	individuálně
upravují	upravovat	k5eAaImIp3nP	upravovat
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ocelové	ocelový	k2eAgInPc1d1	ocelový
nebo	nebo	k8xC	nebo
nerezové	rezový	k2eNgInPc1d1	nerezový
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
větší	veliký	k2eAgInSc4d2	veliký
cvik	cvik	k1gInSc4	cvik
a	a	k8xC	a
praxi	praxe	k1gFnSc4	praxe
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
pak	pak	k6eAd1	pak
zedník	zedník	k1gMnSc1	zedník
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
protáčet	protáčet	k5eAaImF	protáčet
<g/>
.	.	kIx.	.
</s>
<s>
Rukojeť	rukojeť	k1gFnSc1	rukojeť
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
nožkou	nožka	k1gFnSc7	nožka
s	s	k7c7	s
listem	list	k1gInSc7	list
lžíce	lžíce	k1gFnSc2	lžíce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
lichoběžníka	lichoběžník	k1gInSc2	lichoběžník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
list	list	k1gInSc1	list
lžíce	lžíce	k1gFnSc2	lžíce
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
osa	osa	k1gFnSc1	osa
rukojeti	rukojeť	k1gFnSc2	rukojeť
míří	mířit	k5eAaImIp3nS	mířit
zešikma	zešikma	k6eAd1	zešikma
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
lžíce	lžíce	k1gFnSc2	lžíce
<g/>
.	.	kIx.	.
</s>
<s>
Laik	laik	k1gMnSc1	laik
nemá	mít	k5eNaImIp3nS	mít
ponětí	ponětí	k1gNnSc4	ponětí
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
velice	velice	k6eAd1	velice
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
došlo	dojít	k5eAaPmAgNnS	dojít
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
zedník	zedník	k1gMnSc1	zedník
ale	ale	k8xC	ale
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
stačí	stačit	k5eAaBmIp3nS	stačit
malá	malý	k2eAgFnSc1d1	malá
odchylka	odchylka	k1gFnSc1	odchylka
listu	list	k1gInSc2	list
nahoru	nahoru	k6eAd1	nahoru
nebo	nebo	k8xC	nebo
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
a	a	k8xC	a
omítání	omítání	k1gNnSc1	omítání
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
mnohem	mnohem	k6eAd1	mnohem
namáhavější	namáhavý	k2eAgFnSc1d2	namáhavější
<g/>
.	.	kIx.	.
</s>
<s>
Lžíce	lžíce	k1gFnSc1	lžíce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
tenkého	tenký	k2eAgInSc2d1	tenký
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
ohebná	ohebný	k2eAgFnSc1d1	ohebná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
neohebná	ohebný	k2eNgFnSc1d1	neohebná
<g/>
.	.	kIx.	.
</s>
<s>
Neohebné	ohebný	k2eNgFnPc1d1	neohebná
lžíce	lžíce	k1gFnPc1	lžíce
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
k	k	k7c3	k
omítání	omítání	k1gNnSc3	omítání
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
pružení	pružení	k1gNnSc4	pružení
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Lžíce	lžíce	k1gFnPc1	lžíce
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
velikostech	velikost	k1gFnPc6	velikost
<g/>
:	:	kIx,	:
S	s	k7c7	s
délkou	délka	k1gFnSc7	délka
listu	list	k1gInSc2	list
16	[number]	k4	16
cm	cm	kA	cm
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
omítání	omítání	k1gNnSc4	omítání
kleneb	klenba	k1gFnPc2	klenba
<g/>
,	,	kIx,	,
vnitřků	vnitřek	k1gInPc2	vnitřek
výklenků	výklenek	k1gInPc2	výklenek
a	a	k8xC	a
stropů	strop	k1gInPc2	strop
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
délkou	délka	k1gFnSc7	délka
listu	list	k1gInSc2	list
18	[number]	k4	18
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
délkou	délka	k1gFnSc7	délka
listu	list	k1gInSc2	list
20	[number]	k4	20
cm	cm	kA	cm
k	k	k7c3	k
nahození	nahození	k1gNnSc3	nahození
špricu	špricu	k?	špricu
(	(	kIx(	(
<g/>
cementový	cementový	k2eAgInSc1d1	cementový
postřik	postřik	k1gInSc1	postřik
<g/>
)	)	kIx)	)
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
omítkou	omítka	k1gFnSc7	omítka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nabírá	nabírat	k5eAaImIp3nS	nabírat
rovnou	rovnou	k6eAd1	rovnou
z	z	k7c2	z
kolečka	kolečko	k1gNnSc2	kolečko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nanášení	nanášení	k1gNnSc3	nanášení
malty	malta	k1gFnSc2	malta
při	při	k7c6	při
zdění	zdění	k1gNnSc6	zdění
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vyplňování	vyplňování	k1gNnSc2	vyplňování
spár	spára	k1gFnPc2	spára
<g/>
.	.	kIx.	.
</s>
<s>
Tříhranná	tříhranný	k2eAgFnSc1d1	tříhranná
lžíce	lžíce	k1gFnSc1	lžíce
do	do	k7c2	do
špičky	špička	k1gFnSc2	špička
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
zdění	zdění	k1gNnSc6	zdění
kamenného	kamenný	k2eAgNnSc2d1	kamenné
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
,	,	kIx,	,
špičkou	špička	k1gFnSc7	špička
se	se	k3xPyFc4	se
zapravuje	zapravovat	k5eAaImIp3nS	zapravovat
malta	malta	k1gFnSc1	malta
do	do	k7c2	do
spár	spára	k1gFnPc2	spára
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vrcholila	vrcholit	k5eAaImAgFnS	vrcholit
panelová	panelový	k2eAgFnSc1d1	panelová
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ze	z	k7c2	z
zedníků	zedník	k1gMnPc2	zedník
stali	stát	k5eAaPmAgMnP	stát
montážníci	montážník	k1gMnPc1	montážník
<g/>
,	,	kIx,	,
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
svářečské	svářečský	k2eAgInPc4d1	svářečský
kurzy	kurz	k1gInPc4	kurz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
svářet	svářet	k5eAaImF	svářet
k	k	k7c3	k
sobě	se	k3xPyFc3	se
panely	panel	k1gInPc4	panel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
tisk	tisk	k1gInSc1	tisk
nadšeně	nadšeně	k6eAd1	nadšeně
psal	psát	k5eAaImAgInS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
zedník	zedník	k1gMnSc1	zedník
už	už	k6eAd1	už
lžíci	lžíce	k1gFnSc4	lžíce
vůbec	vůbec	k9	vůbec
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
důkaz	důkaz	k1gInSc1	důkaz
pokroku	pokrok	k1gInSc6	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
však	však	k9	však
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
řemesla	řemeslo	k1gNnSc2	řemeslo
<g/>
,	,	kIx,	,
panelákoví	panelákový	k2eAgMnPc1d1	panelákový
zedníci	zedník	k1gMnPc1	zedník
klasickou	klasický	k2eAgFnSc4d1	klasická
zedničinu	zedničina	k1gFnSc4	zedničina
neovládali	ovládat	k5eNaImAgMnP	ovládat
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
pak	pak	k9	pak
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
teprve	teprve	k6eAd1	teprve
učit	učit	k5eAaImF	učit
<g/>
.	.	kIx.	.
</s>
<s>
Klasických	klasický	k2eAgMnPc2d1	klasický
zedníků	zedník	k1gMnPc2	zedník
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Zednická	zednický	k2eAgFnSc1d1	zednická
naběračka	naběračka	k1gFnSc1	naběračka
Hladítko	hladítko	k1gNnSc1	hladítko
Omítkářský	omítkářský	k2eAgInSc4d1	omítkářský
talíř	talíř	k1gInSc4	talíř
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zednická	zednický	k2eAgFnSc1d1	zednická
lžíce	lžíce	k1gFnSc1	lžíce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
