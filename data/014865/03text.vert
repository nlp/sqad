<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaBobří	památkaBobřet	k5eAaImIp3nS,k5eAaPmIp3nS
soutěskaIUCN	soutěskaIUCN	k?
kategorie	kategorie	k1gFnPc4
III	III	kA
(	(	kIx(
<g/>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
Bobří	bobří	k2eAgNnSc1d1
potokZákladní	potokZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1968	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
400	#num#	k4
<g/>
–	–	k?
<g/>
450	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
0,98	0,98	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
Děčín	Děčín	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Verneřice	Verneřice	k1gFnSc1
<g/>
,	,	kIx,
Žandov	Žandov	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
27,16	27,16	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
44,57	44,57	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
2418	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
v	v	k7c6
údolí	údolí	k1gNnSc6
Bobřího	bobří	k2eAgInSc2d1
potoka	potok	k1gInSc2
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
Ústeckého	ústecký	k2eAgInSc2d1
a	a	k8xC
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
v	v	k7c6
okresech	okres	k1gInPc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
a	a	k8xC
Děčín	Děčín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Českém	český	k2eAgNnSc6d1
středohoří	středohoří	k1gNnSc6
východně	východně	k6eAd1
od	od	k7c2
Louček	loučka	k1gFnPc2
u	u	k7c2
Verneřic	Verneřice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
erozní	erozní	k2eAgInSc1d1
údolí	údolí	k1gNnSc2
s	s	k7c7
projevy	projev	k1gInPc7
zpětné	zpětný	k2eAgFnSc2d1
eroze	eroze	k1gFnSc2
v	v	k7c6
čedičovém	čedičový	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ve	v	k7c6
svahu	svah	k1gInSc6
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Bobřího	bobří	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
místech	místo	k1gNnPc6
pod	pod	k7c7
někdejším	někdejší	k2eAgInSc7d1
Čertovým	čertový	k2eAgInSc7d1
mlýnem	mlýn	k1gInSc7
a	a	k8xC
výše	vysoce	k6eAd2
položenou	položený	k2eAgFnSc4d1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
zaniklou	zaniklý	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
Starosti	starost	k1gFnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
letech	let	k1gInPc6
1860	#num#	k4
<g/>
–	–	k?
<g/>
1861	#num#	k4
k	k	k7c3
pokusům	pokus	k1gInPc3
o	o	k7c4
těžbu	těžba	k1gFnSc4
uhlí	uhlí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štola	štola	k1gFnSc1
<g/>
,	,	kIx,
ražená	ražený	k2eAgFnSc1d1
zhruba	zhruba	k6eAd1
v	v	k7c6
polovině	polovina	k1gFnSc6
svahu	svah	k1gInSc2
<g/>
,	,	kIx,
sice	sice	k8xC
dosáhla	dosáhnout	k5eAaPmAgFnS
na	na	k7c4
slojku	slojka	k1gFnSc4
hnědého	hnědý	k2eAgNnSc2d1
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
ukázalo	ukázat	k5eAaPmAgNnS
být	být	k5eAaImF
pro	pro	k7c4
spalování	spalování	k1gNnSc4
nevhodné	vhodný	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
opuštění	opuštění	k1gNnSc6
dolu	dol	k1gInSc2
se	se	k3xPyFc4
štola	štola	k1gFnSc1
zřítila	zřítit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
zařazení	zařazení	k1gNnSc6
do	do	k7c2
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
rozhodlo	rozhodnout	k5eAaPmAgNnS
usnesení	usnesení	k1gNnSc1
okresního	okresní	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
Děčín	Děčín	k1gInSc1
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1968	#num#	k4
a	a	k8xC
okresního	okresní	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgFnS
lokalita	lokalita	k1gFnSc1
zapsána	zapsat	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
chráněný	chráněný	k2eAgInSc4d1
přírodní	přírodní	k2eAgInSc4d1
výtvor	výtvor	k1gInSc4
v	v	k7c6
rozsahu	rozsah	k1gInSc6
0,66	0,66	k4
ha	ha	kA
a	a	k8xC
zahrnovala	zahrnovat	k5eAaImAgFnS
Bobří	bobří	k2eAgInSc4d1
vodopád	vodopád	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
evidenci	evidence	k1gFnSc6
evropských	evropský	k2eAgFnPc2d1
významných	významný	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
pod	pod	k7c7
názvem	název	k1gInSc7
Binov	Binov	k1gInSc4
–	–	k?
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1
území	území	k1gNnSc1
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
0,9831	0,9831	k4
hektarů	hektar	k1gInPc2
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
400	#num#	k4
<g/>
–	–	k?
<g/>
450	#num#	k4
metrů	metr	k1gInPc2
v	v	k7c6
katastrálních	katastrální	k2eAgNnPc6d1
územích	území	k1gNnPc6
Velká	velký	k2eAgFnSc1d1
Javorská	Javorská	k1gFnSc1
města	město	k1gNnSc2
Žandov	Žandovo	k1gNnPc2
v	v	k7c6
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
(	(	kIx(
<g/>
0,72	0,72	k4
hektarů	hektar	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
Loučky	loučka	k1gFnPc1
u	u	k7c2
Verneřic	Verneřice	k1gFnPc2
v	v	k7c6
Ústeckém	ústecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
(	(	kIx(
<g/>
0,27	0,27	k4
hektarů	hektar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stinné	stinný	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
erozní	erozní	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
v	v	k7c6
čedičovém	čedičový	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
<g/>
,	,	kIx,
vytvořeném	vytvořený	k2eAgInSc6d1
od	od	k7c2
západu	západ	k1gInSc3
přitékajícím	přitékající	k2eAgInSc7d1
Bobřím	bobří	k2eAgInSc7d1
potokem	potok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
ukázka	ukázka	k1gFnSc1
zpětné	zpětný	k2eAgFnSc2d1
eroze	eroze	k1gFnSc2
v	v	k7c6
čedičovém	čedičový	k2eAgNnSc6d1
tělesu	těleso	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
dva	dva	k4xCgInPc4
vodopády	vodopád	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgInSc1d2
pěnivý	pěnivý	k2eAgInSc1d1
vytéká	vytékat	k5eAaImIp3nS
z	z	k7c2
pukliny	puklina	k1gFnSc2
v	v	k7c6
čedičové	čedičový	k2eAgFnSc6d1
skále	skála	k1gFnSc6
pod	pod	k7c7
troskami	troska	k1gFnPc7
Čertova	čertův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
<g/>
,	,	kIx,
vysoký	vysoký	k2eAgInSc1d1
je	být	k5eAaImIp3nS
jen	jen	k9
2	#num#	k4
metry	metr	k1gInPc4
<g/>
,	,	kIx,
široký	široký	k2eAgInSc4d1
čtyři	čtyři	k4xCgInPc4
metry	metr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
od	od	k7c2
něj	on	k3xPp3gMnSc2
je	být	k5eAaImIp3nS
další	další	k2eAgInSc1d1
vodopád	vodopád	k1gInSc1
vysoký	vysoký	k2eAgInSc1d1
6	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
však	však	k9
tak	tak	k6eAd1
mohutný	mohutný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celá	k1gFnSc2
údolí	údolí	k1gNnSc2
je	být	k5eAaImIp3nS
zarostlé	zarostlý	k2eAgNnSc1d1
lesem	les	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
mnoho	mnoho	k4c4
vzácných	vzácný	k2eAgFnPc2d1
květin	květina	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
růže	růže	k1gFnSc1
alpská	alpský	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
i	i	k9
živočichů	živočich	k1gMnPc2
(	(	kIx(
<g/>
např.	např.	kA
skorec	skorec	k1gMnSc1
vodní	vodní	k2eAgMnSc1d1
<g/>
,	,	kIx,
konipas	konipas	k1gMnSc1
horský	horský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přístup	přístup	k1gInSc1
pro	pro	k7c4
turisty	turist	k1gMnPc4
</s>
<s>
Přes	přes	k7c4
údolí	údolí	k1gNnPc4
vede	vést	k5eAaImIp3nS
zeleně	zeleně	k6eAd1
značená	značený	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
od	od	k7c2
Kravař	Kravaře	k1gInPc2
na	na	k7c4
Verneřice	Verneřice	k1gFnPc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
zde	zde	k6eAd1
silnice	silnice	k1gFnSc1
ani	ani	k8xC
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbližší	blízký	k2eAgFnSc1d3
vlaková	vlakový	k2eAgFnSc1d1
zastávka	zastávka	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Kravařích	Kravaře	k1gInPc6
na	na	k7c6
železniční	železniční	k2eAgFnSc6d1
trati	trať	k1gFnSc6
Lovosice	Lovosice	k1gInPc1
–	–	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Průchodnost	průchodnost	k1gFnSc1
územím	území	k1gNnSc7
byla	být	k5eAaImAgFnS
zlepšena	zlepšit	k5eAaPmNgFnS
úpravou	úprava	k1gFnSc7
cest	cesta	k1gFnPc2
a	a	k8xC
vybudováním	vybudování	k1gNnSc7
lávek	lávka	k1gFnPc2
i	i	k8xC
zábradlí	zábradlí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vysokého	vysoký	k2eAgInSc2d1
stavu	stav	k1gInSc2
vody	voda	k1gFnSc2
se	se	k3xPyFc4
tudy	tudy	k6eAd1
nedoporučuje	doporučovat	k5eNaImIp3nS
chodit	chodit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
KÜHN	KÜHN	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geologické	geologický	k2eAgFnPc4d1
zajímavosti	zajímavost	k1gFnPc4
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
120	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
6366	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
AUTORSKÝ	autorský	k2eAgInSc4d1
KOLEKTIV	kolektiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
Informatorium	Informatorium	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85368	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
57	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
AUTORSKÝ	autorský	k2eAgInSc4d1
KOLEKTIV	kolektiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
Ústecko	Ústecko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc4
I.	I.	kA
Ústecko	Ústecko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
228	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RŮŽIČKA	Růžička	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českolipsko	Českolipsko	k1gNnSc1
do	do	k7c2
kapsy	kapsa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
KMa	KMa	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7309	#num#	k4
<g/>
-	-	kIx~
<g/>
488	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Českolipsko	Českolipsko	k1gNnSc4
do	do	k7c2
kapsy	kapsa	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1
popis	popis	k1gInSc1
oblasti	oblast	k1gFnSc2
</s>
<s>
Video	video	k1gNnSc1
z	z	k7c2
Bobří	bobří	k2eAgFnSc2d1
soutěsky	soutěska	k1gFnSc2
na	na	k7c6
YouTube	YouTub	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgMnSc1d1
kraj	kraj	k7c2
•	•	k?
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Břehyně	Břehyně	k1gFnSc1
–	–	k?
Pecopala	Pecopal	k1gMnSc2
•	•	k?
Jezevčí	jezevčí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Novozámecký	Novozámecký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Bezděz	Bezděz	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Jestřebské	Jestřebský	k2eAgFnPc1d1
slatiny	slatina	k1gFnPc1
•	•	k?
Panská	panská	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Peklo	peklo	k1gNnSc1
•	•	k?
Swamp	Swamp	k1gMnSc1
Přírodní	přírodní	k2eAgMnSc1d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Hradčanské	hradčanský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Jílovka	jílovka	k1gFnSc1
•	•	k?
Klíč	klíč	k1gInSc1
•	•	k?
Kokořínský	Kokořínský	k2eAgInSc4d1
důl	důl	k1gInSc4
•	•	k?
Kostelecké	Kostelecké	k2eAgInPc4d1
bory	bor	k1gInPc4
•	•	k?
Luž	Luž	k1gFnSc1
•	•	k?
Mokřady	mokřad	k1gInPc1
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Ralsko	Ralsko	k1gNnSc1
•	•	k?
Vlhošť	Vlhošť	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Brazilka	Brazilka	k1gFnSc1
•	•	k?
Cihelenské	Cihelenský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Černý	černý	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Červený	červený	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
–	–	k?
mokřad	mokřad	k1gInSc4
v	v	k7c6
nivě	niva	k1gFnSc6
Šporky	Šporka	k1gMnSc2
•	•	k?
Deštenské	Deštenský	k2eAgFnPc1d1
pastviny	pastvina	k1gFnPc1
•	•	k?
Děvín	Děvín	k1gInSc1
a	a	k8xC
Ostrý	ostrý	k2eAgMnSc1d1
•	•	k?
Divadlo	divadlo	k1gNnSc1
•	•	k?
Dutý	dutý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Farská	farský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Husa	Hus	k1gMnSc2
•	•	k?
Jelení	jelení	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
•	•	k?
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
u	u	k7c2
Křenova	Křenov	k1gInSc2
•	•	k?
Kaňon	kaňon	k1gInSc1
potoka	potok	k1gInSc2
Kolné	Kolná	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Manušické	Manušický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Martinské	martinský	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
•	•	k?
Meandry	meandr	k1gInPc1
Ploučnice	Ploučnice	k1gFnSc2
u	u	k7c2
Mimoně	Mimoň	k1gFnSc2
•	•	k?
Naděje	naděje	k1gFnSc1
•	•	k?
Niva	niva	k1gFnSc1
Ploučnice	Ploučnice	k1gFnSc1
u	u	k7c2
Žizníkova	Žizníkův	k2eAgInSc2d1
•	•	k?
Okřešické	Okřešický	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Osinalické	Osinalický	k2eAgFnPc1d1
bučiny	bučina	k1gFnPc1
•	•	k?
Pískovna	pískovna	k1gFnSc1
Žizníkov	Žizníkov	k1gInSc1
•	•	k?
Pod	pod	k7c7
Hvězdou	hvězda	k1gFnSc7
•	•	k?
Prameny	pramen	k1gInPc1
Pšovky	Pšovka	k1gFnSc2
•	•	k?
Provodínské	Provodínský	k2eAgInPc1d1
kameny	kámen	k1gInPc1
•	•	k?
Pustý	pustý	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Černého	Černý	k1gMnSc2
rybníka	rybník	k1gInSc2
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Mařeničky	Mařenička	k1gFnSc2
•	•	k?
Ronov	Ronov	k1gInSc1
•	•	k?
Skalice	Skalice	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
•	•	k?
Stohánek	Stohánek	k1gMnSc1
•	•	k?
Stružnické	Stružnický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Stříbrný	stříbrný	k1gInSc1
vrch	vrch	k1gInSc1
•	•	k?
Široký	široký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
U	u	k7c2
Rozmoklé	rozmoklý	k2eAgFnSc2d1
žáby	žába	k1gFnSc2
•	•	k?
Vranovské	vranovský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Zahrádky	zahrádka	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
•	•	k?
Růžovský	Růžovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Březinské	Březinský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Pravčická	Pravčická	k1gFnSc1
brána	brána	k1gFnSc1
•	•	k?
Zlatý	zlatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Arba	arba	k1gFnSc1
•	•	k?
Bohyňská	Bohyňský	k2eAgFnSc1d1
lada	lado	k1gNnSc2
•	•	k?
Čabel	Čabel	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Jílového	Jílové	k1gNnSc2
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
•	•	k?
Maiberg	Maiberg	k1gInSc1
•	•	k?
Marschnerova	Marschnerův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Pavlínino	Pavlínin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Pekelský	pekelský	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
Studený	studený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Světlík	světlík	k1gInSc1
•	•	k?
Vápenka	vápenka	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vrabinec	Vrabinec	k1gInSc1
•	•	k?
Za	za	k7c7
pilou	pila	k1gFnSc7
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Hofberg	Hofberg	k1gInSc1
•	•	k?
Jeskyně	jeskyně	k1gFnSc1
pod	pod	k7c7
Sněžníkem	Sněžník	k1gInSc7
•	•	k?
Jílovské	jílovský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Kytlice	kytlice	k1gFnSc2
•	•	k?
Líska	líska	k1gFnSc1
•	•	k?
Louka	louka	k1gFnSc1
u	u	k7c2
Brodských	Brodská	k1gFnPc2
•	•	k?
Meandry	meandr	k1gInPc1
Chřibské	chřibský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
•	•	k?
Nebočadský	Nebočadský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Noldenteich	Noldenteich	k1gInSc1
•	•	k?
Pod	pod	k7c7
lesem	les	k1gInSc7
•	•	k?
Rybník	rybník	k1gInSc1
u	u	k7c2
Králova	Králův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
•	•	k?
Sojčí	sojčí	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
Oleška	Olešek	k1gMnSc2
•	•	k?
Stříbrný	stříbrný	k2eAgInSc4d1
roh	roh	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
