<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
v	v	k7c6
údolí	údolí	k1gNnSc6
Bobřího	bobří	k2eAgInSc2d1
potoka	potok	k1gInSc2
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
Ústeckého	ústecký	k2eAgInSc2d1
a	a	k8xC
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
v	v	k7c6
okresech	okres	k1gInPc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
a	a	k8xC
Děčín	Děčín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
erozní	erozní	k2eAgInSc1d1
údolí	údolí	k1gNnSc2
s	s	k7c7
projevy	projev	k1gInPc7
zpětné	zpětný	k2eAgFnSc2d1
eroze	eroze	k1gFnSc2
v	v	k7c6
čedičovém	čedičový	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
<g/>
.	.	kIx.
</s>