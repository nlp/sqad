<s>
Umbriel	Umbriel	k1gInSc1	Umbriel
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gMnSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
1169	[number]	k4	1169
km	km	kA	km
<g/>
,	,	kIx,	,
od	od	k7c2	od
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
226	[number]	k4	226
300	[number]	k4	300
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
jeho	on	k3xPp3gInSc4	on
oběh	oběh	k1gInSc1	oběh
trvá	trvat	k5eAaImIp3nS	trvat
4,14	[number]	k4	4,14
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Uranových	Uranův	k2eAgInPc2d1	Uranův
hlavních	hlavní	k2eAgInPc2d1	hlavní
měsíců	měsíc	k1gInPc2	měsíc
má	mít	k5eAaImIp3nS	mít
nejtmavší	tmavý	k2eAgFnSc4d3	nejtmavší
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
odráží	odrážet	k5eAaImIp3nS	odrážet
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
16	[number]	k4	16
%	%	kIx~	%
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
hustě	hustě	k6eAd1	hustě
pokryt	pokryt	k2eAgInSc4d1	pokryt
krátery	kráter	k1gInPc4	kráter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
až	až	k9	až
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
aktivita	aktivita	k1gFnSc1	aktivita
zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jasný	jasný	k2eAgInSc1d1	jasný
atypický	atypický	k2eAgInSc1d1	atypický
útvar	útvar	k1gInSc1	útvar
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
cca	cca	kA	cca
130	[number]	k4	130
km	km	kA	km
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
Wunda	Wunda	k1gMnSc1	Wunda
<g/>
,	,	kIx,	,
o	o	k7c6	o
jehož	jehož	k3xOyRp3gInSc6	jehož
původu	původ	k1gInSc6	původ
a	a	k8xC	a
povaze	povaha	k1gFnSc6	povaha
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
formálně	formálně	k6eAd1	formálně
řazen	řadit	k5eAaImNgInS	řadit
mezi	mezi	k7c7	mezi
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
Williamem	William	k1gInSc7	William
Lassellem	Lassell	k1gMnSc7	Lassell
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
postavy	postava	k1gFnSc2	postava
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Alexandera	Alexander	k1gMnSc2	Alexander
Popea	Popeus	k1gMnSc2	Popeus
The	The	k1gMnSc2	The
Rape	rape	k1gNnSc2	rape
of	of	k?	of
the	the	k?	the
Lock	Lock	k1gInSc1	Lock
(	(	kIx(	(
<g/>
Uloupená	uloupený	k2eAgFnSc1d1	Uloupená
kadeř	kadeř	k1gFnSc1	kadeř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
