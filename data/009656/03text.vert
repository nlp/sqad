<p>
<s>
Prosincový	prosincový	k2eAgInSc1d1	prosincový
blok	blok	k1gInSc1	blok
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
uskupení	uskupení	k1gNnSc4	uskupení
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1935	[number]	k4	1935
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
prezidenta	prezident	k1gMnSc2	prezident
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
Tomáše	Tomáš	k1gMnSc2	Tomáš
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc4	pozadí
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
==	==	k?	==
</s>
</p>
<p>
<s>
Členy	člen	k1gInPc1	člen
Prosincového	prosincový	k2eAgInSc2d1	prosincový
bloku	blok	k1gInSc2	blok
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
</s>
</p>
<p>
<s>
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
a	a	k8xC	a
malorolnického	malorolnický	k2eAgInSc2d1	malorolnický
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
Agrární	agrární	k2eAgFnSc1d1	agrární
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
živnostensko-obchodnická	živnostenskobchodnický	k2eAgFnSc1d1	živnostensko-obchodnická
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
sjednocení	sjednocení	k1gNnSc1	sjednocení
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
obec	obec	k1gFnSc1	obec
fašistická	fašistický	k2eAgFnSc1d1	fašistická
</s>
</p>
<p>
<s>
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ludová	ludový	k2eAgFnSc1d1	ludová
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
stranaCílem	stranaCíl	k1gMnSc7	stranaCíl
tohoto	tento	k3xDgInSc2	tento
pravicového	pravicový	k2eAgInSc2d1	pravicový
politického	politický	k2eAgInSc2d1	politický
paktu	pakt	k1gInSc2	pakt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
iniciátorem	iniciátor	k1gMnSc7	iniciátor
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Agrárníci	agrárník	k1gMnPc1	agrárník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Beranem	Beran	k1gMnSc7	Beran
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
nominovat	nominovat	k5eAaBmF	nominovat
společného	společný	k2eAgMnSc4d1	společný
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
proti	proti	k7c3	proti
Edvardu	Edvard	k1gMnSc3	Edvard
Benešovi	Beneš	k1gMnSc3	Beneš
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc1	nástupce
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgMnSc7	tento
kandidátem	kandidát	k1gMnSc7	kandidát
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nepolitická	politický	k2eNgFnSc1d1	nepolitická
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Bohumil	Bohumil	k1gMnSc1	Bohumil
Němec	Němec	k1gMnSc1	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
debaty	debata	k1gFnPc1	debata
mezi	mezi	k7c7	mezi
demokratickými	demokratický	k2eAgFnPc7d1	demokratická
stranami	strana	k1gFnPc7	strana
bloku	blok	k1gInSc2	blok
s	s	k7c7	s
Hlinkovou	Hlinkův	k2eAgFnSc7d1	Hlinkova
ludovou	ludový	k2eAgFnSc7d1	ludová
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
následně	následně	k6eAd1	následně
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Prosincový	prosincový	k2eAgInSc1d1	prosincový
blok	blok	k1gInSc1	blok
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Němec	Němec	k1gMnSc1	Němec
den	den	k1gInSc4	den
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
za	za	k7c4	za
druhého	druhý	k4xOgMnSc4	druhý
československého	československý	k2eAgMnSc4d1	československý
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Hlasy	hlas	k1gInPc1	hlas
mu	on	k3xPp3gMnSc3	on
odevzdali	odevzdat	k5eAaPmAgMnP	odevzdat
volitelé	volitel	k1gMnPc1	volitel
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
národních	národní	k2eAgMnPc2d1	národní
socialistů	socialist	k1gMnPc2	socialist
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
lidovců	lidovec	k1gMnPc2	lidovec
<g/>
,	,	kIx,	,
slovenských	slovenský	k2eAgMnPc2d1	slovenský
luďáků	luďák	k1gMnPc2	luďák
<g/>
,	,	kIx,	,
agrárníků	agrárník	k1gMnPc2	agrárník
a	a	k8xC	a
živnostníků	živnostník	k1gMnPc2	živnostník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Beneše	Beneš	k1gMnSc4	Beneš
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
340	[number]	k4	340
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
obdržel	obdržet	k5eAaPmAgMnS	obdržet
24	[number]	k4	24
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
76	[number]	k4	76
hlasovacích	hlasovací	k2eAgInPc2d1	hlasovací
lístků	lístek	k1gInPc2	lístek
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
prázdných	prázdný	k2eAgNnPc2d1	prázdné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ŽALOUDEK	Žaloudek	k1gMnSc1	Žaloudek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
