<s>
Prosincový	prosincový	k2eAgInSc1d1	prosincový
blok	blok	k1gInSc1	blok
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
uskupení	uskupení	k1gNnSc4	uskupení
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1935	[number]	k4	1935
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
prezidenta	prezident	k1gMnSc2	prezident
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
Tomáše	Tomáš	k1gMnSc2	Tomáš
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
