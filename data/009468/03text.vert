<p>
<s>
"	"	kIx"	"
<g/>
Free	Fre	k1gInPc4	Fre
Bird	Bird	k1gInSc1	Bird
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Lynyrd	Lynyrd	k1gMnSc1	Lynyrd
Skynyrd	Skynyrd	k1gMnSc1	Skynyrd
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
studiové	studiový	k2eAgFnSc6d1	studiová
verzi	verze	k1gFnSc6	verze
nahrávka	nahrávka	k1gFnSc1	nahrávka
začíná	začínat	k5eAaImIp3nS	začínat
gospelově	gospelově	k6eAd1	gospelově
znějícími	znějící	k2eAgInPc7d1	znějící
varhany	varhany	k1gInPc7	varhany
<g/>
,	,	kIx,	,
podkreslenými	podkreslený	k2eAgFnPc7d1	podkreslená
jakoby	jakoby	k8xS	jakoby
volnými	volný	k2eAgFnPc7d1	volná
variacemi	variace	k1gFnPc7	variace
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
slide	slide	k1gInSc4	slide
kytaru	kytara	k1gFnSc4	kytara
se	s	k7c7	s
zpěvem	zpěv	k1gInSc7	zpěv
ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
připomínající	připomínající	k2eAgInSc1d1	připomínající
pomalý	pomalý	k2eAgInSc1d1	pomalý
song	song	k1gInSc1	song
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
southern	southerna	k1gFnPc2	southerna
rocku	rock	k1gInSc2	rock
<g/>
;	;	kIx,	;
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
skladba	skladba	k1gFnSc1	skladba
graduje	gradovat	k5eAaImIp3nS	gradovat
přitvrzeným	přitvrzený	k2eAgNnSc7d1	přitvrzené
parafrázovaním	parafrázovaní	k1gNnSc7	parafrázovaní
refrénu	refrén	k1gInSc2	refrén
do	do	k7c2	do
hard	harda	k1gFnPc2	harda
rockového	rockový	k2eAgInSc2d1	rockový
duelu	duel	k1gInSc2	duel
dvojice	dvojice	k1gFnSc1	dvojice
sólových	sólový	k2eAgFnPc2d1	sólová
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
časopisem	časopis	k1gInSc7	časopis
Guitar	Guitara	k1gFnPc2	Guitara
World	Worlda	k1gFnPc2	Worlda
ohodnocen	ohodnotit	k5eAaPmNgMnS	ohodnotit
jako	jako	k9	jako
třetí	třetí	k4xOgNnSc4	třetí
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
kytarové	kytarový	k2eAgNnSc4d1	kytarové
sólo	sólo	k1gNnSc4	sólo
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
2	[number]	k4	2
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Free	Free	k1gFnSc4	Free
Bird	Birda	k1gFnPc2	Birda
<g/>
"	"	kIx"	"
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
rockovou	rockový	k2eAgFnSc4d1	rocková
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
podle	podle	k7c2	podle
jejího	její	k3xOp3gNnSc2	její
hodnocení	hodnocení	k1gNnSc2	hodnocení
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
jen	jen	k9	jen
"	"	kIx"	"
<g/>
Stairway	Stairwa	k2eAgFnPc4d1	Stairwa
to	ten	k3xDgNnSc4	ten
Heaven	Heaven	k2eAgInSc1d1	Heaven
<g/>
"	"	kIx"	"
od	od	k7c2	od
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
dal	dát	k5eAaPmAgInS	dát
"	"	kIx"	"
<g/>
Free	Free	k1gNnPc1	Free
Bird	Birda	k1gFnPc2	Birda
<g/>
"	"	kIx"	"
na	na	k7c4	na
191	[number]	k4	191
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
500	[number]	k4	500
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
skladbami	skladba	k1gFnPc7	skladba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
Molly	Molla	k1gFnSc2	Molla
Hatchet	Hatchet	k1gMnSc1	Hatchet
cover	cover	k1gMnSc1	cover
verzi	verze	k1gFnSc4	verze
této	tento	k3xDgFnSc2	tento
skladby	skladba	k1gFnSc2	skladba
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
Regrinding	Regrinding	k1gInSc4	Regrinding
the	the	k?	the
Axes	Axes	k1gInSc1	Axes
<g/>
.	.	kIx.	.
<g/>
Skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Grand	grand	k1gMnSc1	grand
Theft	Theft	k1gMnSc1	Theft
Auto	auto	k1gNnSc1	auto
<g/>
:	:	kIx,	:
San	San	k1gMnSc1	San
Andreas	Andreas	k1gMnSc1	Andreas
v	v	k7c6	v
herní	herní	k2eAgFnSc6d1	herní
rozhlasové	rozhlasový	k2eAgFnSc6d1	rozhlasová
stanici	stanice	k1gFnSc6	stanice
K-DST	K-DST	k1gFnSc2	K-DST
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
skladby	skladba	k1gFnPc4	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgFnSc1d1	studiová
verze	verze	k1gFnSc1	verze
–	–	k?	–
1973	[number]	k4	1973
===	===	k?	===
</s>
</p>
<p>
<s>
Ronnie	Ronnie	k1gFnSc1	Ronnie
Van	vana	k1gFnPc2	vana
Zant	Zant	k2eAgInSc1d1	Zant
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Aaron	Aaron	k1gMnSc1	Aaron
Thiele	Thiel	k1gInSc2	Thiel
–	–	k?	–
1964	[number]	k4	1964
Gibson	Gibson	k1gInSc1	Gibson
Firebird	Firebird	k1gInSc4	Firebird
I	i	k9	i
(	(	kIx(	(
<g/>
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
/	/	kIx~	/
<g/>
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
nahrávka	nahrávka	k1gFnSc1	nahrávka
sólové	sólový	k2eAgFnSc2d1	sólová
kytary	kytara	k1gFnSc2	kytara
<g/>
/	/	kIx~	/
<g/>
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gary	Gara	k1gFnPc1	Gara
Rossington	Rossington	k1gInSc1	Rossington
–	–	k?	–
1969	[number]	k4	1969
Gibson	Gibson	k1gInSc1	Gibson
SG	SG	kA	SG
(	(	kIx(	(
<g/>
slide	slide	k1gInSc1	slide
kytara	kytara	k1gFnSc1	kytara
<g/>
/	/	kIx~	/
<g/>
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ed	Ed	k?	Ed
King	King	k1gMnSc1	King
–	–	k?	–
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
1964	[number]	k4	1964
Fender	Fendra	k1gFnPc2	Fendra
Jazz	jazz	k1gInSc1	jazz
Bass	Bass	k1gMnSc1	Bass
</s>
</p>
<p>
<s>
Billy	Bill	k1gMnPc4	Bill
Powell	Powella	k1gFnPc2	Powella
–	–	k?	–
elektrický	elektrický	k2eAgInSc4d1	elektrický
klavír	klavír	k1gInSc4	klavír
Wurlitzer	Wurlitzra	k1gFnPc2	Wurlitzra
</s>
</p>
<p>
<s>
Bob	Bob	k1gMnSc1	Bob
Burns	Burnsa	k1gFnPc2	Burnsa
–	–	k?	–
bicí	bicí	k2eAgFnSc2d1	bicí
</s>
</p>
<p>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
Gook	Gook	k1gMnSc1	Gook
(	(	kIx(	(
<g/>
producent	producent	k1gMnSc1	producent
Al	ala	k1gFnPc2	ala
Kooper	Kooper	k1gMnSc1	Kooper
<g/>
)	)	kIx)	)
–	–	k?	–
varhany	varhany	k1gFnPc1	varhany
<g/>
,	,	kIx,	,
melotron	melotron	k1gInSc1	melotron
</s>
</p>
<p>
<s>
Dalton	Dalton	k1gInSc1	Dalton
Teasley	Teaslea	k1gFnSc2	Teaslea
–	–	k?	–
Über	Über	k1gInSc1	Über
Badass	Badassa	k1gFnPc2	Badassa
Electric	Electric	k1gMnSc1	Electric
Fiddle	Fiddle	k1gMnSc1	Fiddle
</s>
</p>
<p>
<s>
===	===	k?	===
Koncertní	koncertní	k2eAgFnSc1d1	koncertní
verze	verze	k1gFnSc1	verze
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
===	===	k?	===
</s>
</p>
<p>
<s>
Ronnie	Ronnie	k1gFnSc1	Ronnie
Van	vana	k1gFnPc2	vana
Zant	Zant	k2eAgInSc1d1	Zant
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Allen	Allen	k1gMnSc1	Allen
Collins	Collinsa	k1gFnPc2	Collinsa
–	–	k?	–
1964	[number]	k4	1964
Gibson	Gibson	k1gInSc1	Gibson
Firebird	Firebird	k1gInSc4	Firebird
III	III	kA	III
(	(	kIx(	(
<g/>
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
a	a	k8xC	a
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gary	Gara	k1gFnPc1	Gara
Rossington	Rossington	k1gInSc1	Rossington
–	–	k?	–
1969	[number]	k4	1969
Gibson	Gibson	k1gInSc1	Gibson
SG	SG	kA	SG
(	(	kIx(	(
<g/>
slide	slide	k1gInSc1	slide
kytara	kytara	k1gFnSc1	kytara
<g/>
/	/	kIx~	/
<g/>
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stevie	Stevie	k1gFnSc1	Stevie
Gaines	Gainesa	k1gFnPc2	Gainesa
–	–	k?	–
1958	[number]	k4	1958
Gibson	Gibsona	k1gFnPc2	Gibsona
Les	les	k1gInSc1	les
Paul	Paul	k1gMnSc1	Paul
Custom	Custom	k1gInSc1	Custom
(	(	kIx(	(
<g/>
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leon	Leona	k1gFnPc2	Leona
Wilkeson	Wilkesona	k1gFnPc2	Wilkesona
–	–	k?	–
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
1962	[number]	k4	1962
Fender	Fendra	k1gFnPc2	Fendra
Jazz	jazz	k1gInSc1	jazz
Bass	Bass	k1gMnSc1	Bass
</s>
</p>
<p>
<s>
Billy	Bill	k1gMnPc4	Bill
Powell	Powellum	k1gNnPc2	Powellum
–	–	k?	–
Steinway	Steinwaa	k1gFnSc2	Steinwaa
and	and	k?	and
Sons	Sons	k1gInSc1	Sons
Concert	Concert	k1gMnSc1	Concert
Grand	grand	k1gMnSc1	grand
Piano	piano	k6eAd1	piano
(	(	kIx(	(
<g/>
velké	velký	k2eAgNnSc4d1	velké
koncertní	koncertní	k2eAgNnSc4d1	koncertní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Artimus	Artimus	k1gMnSc1	Artimus
Pyle	pyl	k1gInSc5	pyl
–	–	k?	–
Slingerland	Slingerlando	k1gNnPc2	Slingerlando
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
Journal	Journal	k1gMnSc1	Journal
<g/>
:	:	kIx,	:
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Oldest	Oldest	k1gInSc1	Oldest
Joke	Jok	k1gInPc1	Jok
<g/>
:	:	kIx,	:
Yelling	Yelling	k1gInSc1	Yelling
'	'	kIx"	'
<g/>
Free	Free	k1gNnSc1	Free
Bird	Birda	k1gFnPc2	Birda
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
'	'	kIx"	'
In	In	k1gMnSc1	In
a	a	k8xC	a
Crowded	Crowded	k1gMnSc1	Crowded
Theater	Theater	k1gMnSc1	Theater
</s>
</p>
<p>
<s>
The	The	k?	The
Curse	Curse	k1gFnSc1	Curse
of	of	k?	of
the	the	k?	the
Freebird	Freebird	k1gInSc1	Freebird
</s>
</p>
<p>
<s>
Sólo	sólo	k1gNnSc1	sólo
</s>
</p>
