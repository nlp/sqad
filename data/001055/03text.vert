<s>
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1964	[number]	k4	1964
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
kontinuálně	kontinuálně	k6eAd1	kontinuálně
občanskodemokratickým	občanskodemokratický	k2eAgMnSc7d1	občanskodemokratický
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
radě	rada	k1gFnSc6	rada
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
zastával	zastávat	k5eAaImAgInS	zastávat
posty	posta	k1gFnSc2	posta
místopředsedy	místopředseda	k1gMnSc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
první	první	k4xOgFnSc6	první
i	i	k8xC	i
druhé	druhý	k4xOgFnSc3	druhý
vládě	vláda	k1gFnSc3	vláda
Mirka	Mirek	k1gMnSc2	Mirek
Topolánka	Topolánek	k1gMnSc2	Topolánek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Topolánkově	Topolánkův	k2eAgInSc6d1	Topolánkův
odchodu	odchod	k1gInSc6	odchod
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetím	třetí	k4xOgNnSc6	třetí
předsedou	předseda	k1gMnSc7	předseda
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
<g/>
,	,	kIx,	,
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
do	do	k7c2	do
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
navíc	navíc	k6eAd1	navíc
dočasně	dočasně	k6eAd1	dočasně
pověřen	pověřit	k5eAaPmNgInS	pověřit
vedením	vedení	k1gNnSc7	vedení
rezortu	rezort	k1gInSc2	rezort
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tzv.	tzv.	kA	tzv.
kauzy	kauza	k1gFnPc4	kauza
Nagyová	Nagyový	k2eAgFnSc1d1	Nagyová
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
letecké	letecký	k2eAgFnSc2d1	letecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
až	až	k9	až
1983	[number]	k4	1983
studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Univerzity	univerzita	k1gFnSc2	univerzita
J.	J.	kA	J.
E.	E.	kA	E.
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obor	obor	k1gInSc1	obor
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
získal	získat	k5eAaPmAgMnS	získat
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
RNDr.	RNDr.	kA	RNDr.
prací	prací	k2eAgFnSc2d1	prací
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
fyziky	fyzika	k1gFnSc2	fyzika
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
službě	služba	k1gFnSc6	služba
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
až	až	k9	až
1989	[number]	k4	1989
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
Tesle	Tesla	k1gFnSc6	Tesla
Rožnov	Rožnov	k1gInSc1	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
technolog	technolog	k1gMnSc1	technolog
a	a	k8xC	a
výzkumný	výzkumný	k2eAgMnSc1d1	výzkumný
a	a	k8xC	a
vývojový	vývojový	k2eAgMnSc1d1	vývojový
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Nečas	Nečas	k1gMnSc1	Nečas
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
někdejší	někdejší	k2eAgFnSc7d1	někdejší
manželkou	manželka	k1gFnSc7	manželka
Radkou	Radka	k1gFnSc7	Radka
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Ondřeje	Ondřej	k1gMnPc4	Ondřej
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tomáše	Tomáš	k1gMnSc4	Tomáš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
Terezu	Tereza	k1gFnSc4	Tereza
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
vydali	vydat	k5eAaPmAgMnP	vydat
manželé	manžel	k1gMnPc1	manžel
Nečasovi	Nečasův	k2eAgMnPc1d1	Nečasův
oficiální	oficiální	k2eAgFnPc4d1	oficiální
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
prochází	procházet	k5eAaImIp3nS	procházet
krizí	krize	k1gFnSc7	krize
a	a	k8xC	a
že	že	k8xS	že
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
žijí	žít	k5eAaImIp3nP	žít
odloučeně	odloučeně	k6eAd1	odloučeně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
Nečas	nečas	k1gInSc1	nečas
podal	podat	k5eAaPmAgInS	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Nagyovou	Nagyový	k2eAgFnSc7d1	Nagyová
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc7d1	bývalá
vrchní	vrchní	k2eAgFnSc7d1	vrchní
ředitelkou	ředitelka	k1gFnSc7	ředitelka
svého	svůj	k3xOyFgInSc2	svůj
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
Nečas	Nečas	k1gMnSc1	Nečas
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Nagyovou	Nagyová	k1gFnSc7	Nagyová
veřejnosti	veřejnost	k1gFnSc2	veřejnost
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
lhal	lhát	k5eAaImAgMnS	lhát
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
Nečasovo	Nečasův	k2eAgNnSc1d1	Nečasovo
manželství	manželství	k1gNnSc1	manželství
po	po	k7c6	po
27	[number]	k4	27
letech	léto	k1gNnPc6	léto
rozvedeno	rozvést	k5eAaPmNgNnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Nečas	Nečas	k1gMnSc1	Nečas
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Nagyovou	Nagyová	k1gFnSc7	Nagyová
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
předsedou	předseda	k1gMnSc7	předseda
Oblastní	oblastní	k2eAgFnSc2d1	oblastní
rady	rada	k1gFnSc2	rada
ODS	ODS	kA	ODS
ve	v	k7c6	v
Vsetíně	Vsetín	k1gInSc6	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
prohrál	prohrát	k5eAaPmAgInS	prohrát
souboj	souboj	k1gInSc1	souboj
o	o	k7c4	o
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
Mirkem	Mirek	k1gMnSc7	Mirek
Topolánkem	Topolánek	k1gMnSc7	Topolánek
o	o	k7c4	o
2	[number]	k4	2
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2004	[number]	k4	2004
až	až	k8xS	až
2006	[number]	k4	2006
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedy	místopředseda	k1gMnSc2	místopředseda
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
neobhájil	obhájit	k5eNaPmAgMnS	obhájit
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Bémem	Bém	k1gMnSc7	Bém
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
řadových	řadový	k2eAgMnPc2d1	řadový
místopředsedů	místopředseda	k1gMnPc2	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
Mirka	Mirka	k1gFnSc1	Mirka
Topolánka	Topolánka	k1gFnSc1	Topolánka
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
volebním	volební	k2eAgMnSc7d1	volební
lídrem	lídr	k1gMnSc7	lídr
ODS	ODS	kA	ODS
ke	k	k7c3	k
sněmovním	sněmovní	k2eAgFnPc3d1	sněmovní
volbám	volba	k1gFnPc3	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
oficiálně	oficiálně	k6eAd1	oficiálně
převzal	převzít	k5eAaPmAgInS	převzít
pravomoci	pravomoc	k1gFnSc2	pravomoc
předsedy	předseda	k1gMnSc2	předseda
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
pověřený	pověřený	k2eAgMnSc1d1	pověřený
vedením	vedení	k1gNnSc7	vedení
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
volebního	volební	k2eAgInSc2d1	volební
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
kongresu	kongres	k1gInSc6	kongres
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
ODS	ODS	kA	ODS
a	a	k8xC	a
uspěl	uspět	k5eAaPmAgInS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
znovu	znovu	k6eAd1	znovu
obhájil	obhájit	k5eAaPmAgMnS	obhájit
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
ODS	ODS	kA	ODS
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
351	[number]	k4	351
<g/>
:	:	kIx,	:
<g/>
178	[number]	k4	178
porazil	porazit	k5eAaPmAgMnS	porazit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
rebelujících	rebelující	k2eAgMnPc2d1	rebelující
poslanců	poslanec	k1gMnPc2	poslanec
strany	strana	k1gFnSc2	strana
Ivana	Ivan	k1gMnSc4	Ivan
Fuksu	Fuksa	k1gMnSc4	Fuksa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
od	od	k7c2	od
1993	[number]	k4	1993
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
stálé	stálý	k2eAgFnSc2d1	stálá
delegace	delegace	k1gFnSc2	delegace
Meziparlamentní	meziparlamentní	k2eAgFnSc2d1	meziparlamentní
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
vedl	vést	k5eAaImAgMnS	vést
delegaci	delegace	k1gFnSc4	delegace
do	do	k7c2	do
Shromáždění	shromáždění	k1gNnSc2	shromáždění
Západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1995	[number]	k4	1995
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
náměstkem	náměstek	k1gMnSc7	náměstek
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
poslancem	poslanec	k1gMnSc7	poslanec
PS	PS	kA	PS
PČR	PČR	kA	PČR
(	(	kIx(	(
<g/>
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
zpravodajské	zpravodajský	k2eAgFnPc4d1	zpravodajská
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Společného	společný	k2eAgInSc2d1	společný
výboru	výbor	k1gInSc2	výbor
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
Stálého	stálý	k2eAgInSc2d1	stálý
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
orgánu	orgán	k1gInSc2	orgán
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
Vojenského	vojenský	k2eAgNnSc2d1	vojenské
obranného	obranný	k2eAgNnSc2d1	obranné
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
poslancem	poslanec	k1gMnSc7	poslanec
za	za	k7c4	za
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedou	místopředseda	k1gMnSc7	místopředseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
stálé	stálý	k2eAgFnSc2d1	stálá
delegace	delegace	k1gFnSc2	delegace
do	do	k7c2	do
Shromáždění	shromáždění	k1gNnSc2	shromáždění
Západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
zpravodajské	zpravodajský	k2eAgFnPc4d1	zpravodajská
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
poslancem	poslanec	k1gMnSc7	poslanec
PS	PS	kA	PS
PČR	PČR	kA	PČR
(	(	kIx(	(
<g/>
zvolen	zvolen	k2eAgMnSc1d1	zvolen
jako	jako	k8xS	jako
lídr	lídr	k1gMnSc1	lídr
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
evropské	evropský	k2eAgFnPc4d1	Evropská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
Podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
reformu	reforma	k1gFnSc4	reforma
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
členem	člen	k1gInSc7	člen
stálých	stálý	k2eAgFnPc2d1	stálá
delegací	delegace	k1gFnPc2	delegace
do	do	k7c2	do
Shromáždění	shromáždění	k1gNnSc2	shromáždění
Západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Parlamentního	parlamentní	k2eAgNnSc2d1	parlamentní
shromáždění	shromáždění	k1gNnSc2	shromáždění
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
náhradníkem	náhradník	k1gMnSc7	náhradník
delegáta	delegát	k1gMnSc2	delegát
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
konventu	konvent	k1gInSc6	konvent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
znovu	znovu	k6eAd1	znovu
poslanec	poslanec	k1gMnSc1	poslanec
PS	PS	kA	PS
PČR	PČR	kA	PČR
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
dlouhým	dlouhý	k2eAgInPc3d1	dlouhý
obstrukčním	obstrukční	k2eAgInPc3d1	obstrukční
projevům	projev	k1gInPc3	projev
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
komunistických	komunistický	k2eAgMnPc2d1	komunistický
poslanců	poslanec	k1gMnPc2	poslanec
přezdívku	přezdívka	k1gFnSc4	přezdívka
Fidel	Fidel	k1gMnSc1	Fidel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
zastával	zastávat	k5eAaImAgInS	zastávat
posty	posta	k1gFnSc2	posta
místopředsedy	místopředseda	k1gMnSc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
první	první	k4xOgFnSc6	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
vládě	vláda	k1gFnSc3	vláda
Mirka	Mirek	k1gMnSc2	Mirek
Topolánka	Topolánek	k1gMnSc2	Topolánek
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vláda	vláda	k1gFnSc1	vláda
Petra	Petr	k1gMnSc4	Petr
Nečase	Nečas	k1gMnSc4	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sněmovních	sněmovní	k2eAgFnPc6d1	sněmovní
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
za	za	k7c7	za
ODS	ODS	kA	ODS
jako	jako	k8xS	jako
lídr	lídr	k1gMnSc1	lídr
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ODS	ODS	kA	ODS
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
jako	jako	k8xC	jako
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
jednání	jednání	k1gNnSc4	jednání
za	za	k7c4	za
ustavení	ustavení	k1gNnSc4	ustavení
středopravicové	středopravicový	k2eAgFnSc2d1	středopravicová
koalice	koalice	k1gFnSc2	koalice
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
ODS	ODS	kA	ODS
nominován	nominován	k2eAgMnSc1d1	nominován
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
jej	on	k3xPp3gInSc4	on
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
pověřil	pověřit	k5eAaPmAgMnS	pověřit
jednáním	jednání	k1gNnSc7	jednání
o	o	k7c4	o
sestavení	sestavení	k1gNnSc4	sestavení
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
jej	on	k3xPp3gInSc4	on
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
nazval	nazvat	k5eAaBmAgInS	nazvat
Nečas	nečas	k1gInSc1	nečas
koalicí	koalice	k1gFnPc2	koalice
rozpočtové	rozpočtový	k2eAgFnSc2d1	rozpočtová
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
a	a	k8xC	a
v	v	k7c4	v
den	den	k1gInSc4	den
svého	svůj	k3xOyFgNnSc2	svůj
jmenování	jmenování	k1gNnSc2	jmenování
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
záměr	záměr	k1gInSc4	záměr
ji	on	k3xPp3gFnSc4	on
sestavit	sestavit	k5eAaPmF	sestavit
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgInPc2d1	veřejný
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
v	v	k7c4	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
složila	složit	k5eAaPmAgFnS	složit
jím	on	k3xPp3gNnSc7	on
sestavená	sestavený	k2eAgFnSc1d1	sestavená
vláda	vláda	k1gFnSc1	vláda
slib	slib	k1gInSc4	slib
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
předložila	předložit	k5eAaPmAgFnS	předložit
programové	programový	k2eAgNnSc4d1	programové
prohlášení	prohlášení	k1gNnSc4	prohlášení
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
po	po	k7c6	po
21	[number]	k4	21
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
jí	jíst	k5eAaImIp3nS	jíst
byla	být	k5eAaImAgFnS	být
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vyslovena	vysloven	k2eAgFnSc1d1	vyslovena
důvěra	důvěra	k1gFnSc1	důvěra
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
obdržela	obdržet	k5eAaPmAgFnS	obdržet
118	[number]	k4	118
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
bylo	být	k5eAaImAgNnS	být
82	[number]	k4	82
z	z	k7c2	z
200	[number]	k4	200
přítomných	přítomný	k2eAgMnPc2d1	přítomný
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
kvórum	kvórum	k1gNnSc1	kvórum
101	[number]	k4	101
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kauza	kauza	k1gFnSc1	kauza
Nagyová	Nagyová	k1gFnSc1	Nagyová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedělních	nedělní	k2eAgFnPc6d1	nedělní
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
po	po	k7c4	po
ukončení	ukončení	k1gNnSc4	ukončení
grémia	grémium	k1gNnSc2	grémium
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Nečas	Nečas	k1gMnSc1	Nečas
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
podá	podat	k5eAaPmIp3nS	podat
demisi	demise	k1gFnSc4	demise
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
odstoupí	odstoupit	k5eAaPmIp3nS	odstoupit
také	také	k9	také
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
kauze	kauza	k1gFnSc6	kauza
Nagyová	Nagyová	k1gFnSc1	Nagyová
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
pád	pád	k1gInSc4	pád
celé	celý	k2eAgFnSc2d1	celá
Nečasovy	Nečasův	k2eAgFnSc2d1	Nečasova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Nečas	Nečas	k1gMnSc1	Nečas
demisi	demise	k1gFnSc4	demise
předal	předat	k5eAaPmAgMnS	předat
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
v	v	k7c4	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
hod	hod	k1gInSc1	hod
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
Miloši	Miloš	k1gMnSc3	Miloš
Zemanovi	Zeman	k1gMnSc3	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jej	on	k3xPp3gMnSc4	on
pověřil	pověřit	k5eAaPmAgMnS	pověřit
výkonem	výkon	k1gInSc7	výkon
funkce	funkce	k1gFnSc2	funkce
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
nový	nový	k2eAgInSc1d1	nový
kabinet	kabinet	k1gInSc1	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Nastupující	nastupující	k2eAgFnSc1d1	nastupující
vláda	vláda	k1gFnSc1	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
policie	policie	k1gFnSc1	policie
obvinila	obvinit	k5eAaPmAgFnS	obvinit
Petra	Petr	k1gMnSc4	Petr
Nečase	Nečas	k1gMnSc4	Nečas
z	z	k7c2	z
podplácení	podplácení	k1gNnSc2	podplácení
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
kauze	kauza	k1gFnSc6	kauza
Nagyová	Nagyová	k1gFnSc1	Nagyová
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
politických	politický	k2eAgFnPc2d1	politická
trafik	trafika	k1gFnPc2	trafika
pro	pro	k7c4	pro
exposlance	exposlanec	k1gMnSc4	exposlanec
ODS	ODS	kA	ODS
Petra	Petr	k1gMnSc4	Petr
Tluchoře	Tluchora	k1gFnSc6	Tluchora
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc4	Ivan
Fuksu	Fuksa	k1gMnSc4	Fuksa
a	a	k8xC	a
Marka	Marek	k1gMnSc4	Marek
Šnajdra	Šnajdr	k1gMnSc4	Šnajdr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rezignací	rezignace	k1gFnSc7	rezignace
na	na	k7c4	na
premiérský	premiérský	k2eAgInSc4d1	premiérský
post	post	k1gInSc4	post
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
mandátu	mandát	k1gInSc2	mandát
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
zcela	zcela	k6eAd1	zcela
opustit	opustit	k5eAaPmF	opustit
politický	politický	k2eAgInSc4d1	politický
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
developera	developer	k1gMnSc4	developer
a	a	k8xC	a
majitele	majitel	k1gMnSc4	majitel
finanční	finanční	k2eAgFnSc2d1	finanční
skupiny	skupina	k1gFnSc2	skupina
SPGroup	SPGroup	k1gMnSc1	SPGroup
Pavla	Pavel	k1gMnSc4	Pavel
Sehnala	Sehnal	k1gMnSc4	Sehnal
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bývalým	bývalý	k2eAgInSc7d1	bývalý
europoslancem	europoslanec	k1gInSc7	europoslanec
za	za	k7c4	za
ODS	ODS	kA	ODS
Ivo	Ivo	k1gMnSc1	Ivo
Strejčkem	Strejček	k1gMnSc7	Strejček
je	on	k3xPp3gMnPc4	on
jednatelem	jednatel	k1gMnSc7	jednatel
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
NESTAC	NESTAC	kA	NESTAC
Consulting	Consulting	k1gInSc1	Consulting
<g/>
.	.	kIx.	.
</s>
<s>
Přednáší	přednášet	k5eAaImIp3nS	přednášet
také	také	k9	také
na	na	k7c6	na
CEVRO	CEVRO	kA	CEVRO
Institutu	institut	k1gInSc6	institut
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
Kauza	kauza	k1gFnSc1	kauza
Nagyová	Nagyová	k1gFnSc1	Nagyová
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Osobní	osobní	k2eAgFnSc2d1	osobní
stránky	stránka	k1gFnSc2	stránka
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
6	[number]	k4	6
<g/>
.	.	kIx.	.
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
lídr	lídr	k1gMnSc1	lídr
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
ODS	ODS	kA	ODS
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Impulsy	impuls	k1gInPc4	impuls
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
24	[number]	k4	24
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
Ondřej	Ondřej	k1gMnSc1	Ondřej
Kundra	Kundra	k1gFnSc1	Kundra
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Spurný	Spurný	k1gMnSc1	Spurný
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Švehla	Švehla	k1gMnSc1	Švehla
<g/>
:	:	kIx,	:
Zrod	zrod	k1gInSc1	zrod
lídra	lídr	k1gMnSc2	lídr
aneb	aneb	k?	aneb
Pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
příběh	příběh	k1gInSc4	příběh
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
,	,	kIx,	,
CS	CS	kA	CS
Magazín	magazín	k1gInSc1	magazín
<g/>
,	,	kIx,	,
květen	květen	k1gInSc1	květen
2010	[number]	k4	2010
</s>
