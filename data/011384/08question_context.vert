<s>
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgMnS
vrchním	vrchní	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
(	(	kIx(
<g/>
západních	západní	k2eAgFnPc2d1
<g/>
)	)	kIx)
spojeneckých	spojenecký	k2eAgFnPc2d1
expedičních	expediční	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
1944	[number]	k4
<g/>
–	–	k?
<g/>
1945	[number]	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>