<p>
<s>
Jaguar	Jaguar	k1gMnSc1	Jaguar
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
byl	být	k5eAaImAgInS	být
luxusní	luxusní	k2eAgInSc1d1	luxusní
automobil	automobil	k1gInSc1	automobil
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
britskou	britský	k2eAgFnSc7d1	britská
automobilkou	automobilka	k1gFnSc7	automobilka
Jaguar	Jaguara	k1gFnPc2	Jaguara
Cars	Carsa	k1gFnPc2	Carsa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgInS	být
model	model	k1gInSc1	model
Mark	Mark	k1gMnSc1	Mark
V	V	kA	V
<g/>
,	,	kIx,	,
nástupcem	nástupce	k1gMnSc7	nástupce
pak	pak	k8xC	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
model	model	k1gInSc1	model
Mark	Mark	k1gMnSc1	Mark
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
představil	představit	k5eAaPmAgInS	představit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1950	[number]	k4	1950
na	na	k7c6	na
londýnském	londýnský	k2eAgInSc6d1	londýnský
autosalonu	autosalon	k1gInSc6	autosalon
Motor	motor	k1gInSc1	motor
Show	show	k1gNnSc2	show
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgInS	nahradit
stárnoucí	stárnoucí	k2eAgInSc1d1	stárnoucí
model	model	k1gInSc1	model
Mark	Mark	k1gMnSc1	Mark
V	V	kA	V
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
společně	společně	k6eAd1	společně
se	s	k7c7	s
sportovním	sportovní	k2eAgInSc7d1	sportovní
Jaguarem	Jaguar	k1gInSc7	Jaguar
XK	XK	kA	XK
120	[number]	k4	120
první	první	k4xOgInSc4	první
poválečný	poválečný	k2eAgInSc4d1	poválečný
model	model	k1gInSc4	model
značky	značka	k1gFnSc2	značka
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
luxusního	luxusní	k2eAgInSc2d1	luxusní
výkonného	výkonný	k2eAgInSc2d1	výkonný
sedanu	sedan	k1gInSc2	sedan
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
Mark	Mark	k1gMnSc1	Mark
VI	VI	kA	VI
nebylo	být	k5eNaImAgNnS	být
použito	použit	k2eAgNnSc1d1	použito
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
již	již	k6eAd1	již
používala	používat	k5eAaImAgFnS	používat
automobilka	automobilka	k1gFnSc1	automobilka
Bentley	Bentlea	k1gFnSc2	Bentlea
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
předchůdcem	předchůdce	k1gMnSc7	předchůdce
sdílel	sdílet	k5eAaImAgInS	sdílet
mnohá	mnohý	k2eAgNnPc4d1	mnohé
konstrukční	konstrukční	k2eAgNnPc4d1	konstrukční
řešení	řešení	k1gNnPc4	řešení
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
a	a	k8xC	a
především	především	k6eAd1	především
jízdními	jízdní	k2eAgInPc7d1	jízdní
výkony	výkon	k1gInPc7	výkon
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
velmi	velmi	k6eAd1	velmi
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
evropské	evropský	k2eAgFnPc4d1	Evropská
poměry	poměra	k1gFnPc4	poměra
nezvykle	zvykle	k6eNd1	zvykle
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
bezmála	bezmála	k6eAd1	bezmála
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
rozvorem	rozvor	k1gInSc7	rozvor
3	[number]	k4	3
048	[number]	k4	048
mm	mm	kA	mm
byl	být	k5eAaImAgInS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
také	také	k9	také
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
předválečného	předválečný	k2eAgInSc2d1	předválečný
designu	design	k1gInSc2	design
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
vymizely	vymizet	k5eAaPmAgFnP	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Oblá	oblý	k2eAgFnSc1d1	oblá
majestátní	majestátní	k2eAgFnSc1d1	majestátní
pětimístná	pětimístný	k2eAgFnSc1d1	pětimístná
karoserie	karoserie	k1gFnSc1	karoserie
nyní	nyní	k6eAd1	nyní
zabírala	zabírat	k5eAaImAgFnS	zabírat
plnou	plný	k2eAgFnSc4d1	plná
šíři	šíře	k1gFnSc4	šíře
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
přední	přední	k2eAgInPc1d1	přední
světlomety	světlomet	k1gInPc1	světlomet
byly	být	k5eAaImAgInP	být
plně	plně	k6eAd1	plně
zapuštěny	zapuštěn	k2eAgInPc1d1	zapuštěn
do	do	k7c2	do
předních	přední	k2eAgInPc2d1	přední
blatníků	blatník	k1gInPc2	blatník
<g/>
,	,	kIx,	,
zmizely	zmizet	k5eAaPmAgInP	zmizet
boční	boční	k2eAgNnPc4d1	boční
stupátka	stupátko	k1gNnPc4	stupátko
a	a	k8xC	a
zadní	zadní	k2eAgNnPc4d1	zadní
kola	kolo	k1gNnPc4	kolo
byla	být	k5eAaImAgFnS	být
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
kamašovými	kamašův	k2eAgInPc7d1	kamašův
kryty	kryt	k1gInPc7	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
starším	starý	k2eAgInPc3d2	starší
modelům	model	k1gInPc3	model
značky	značka	k1gFnSc2	značka
nebyla	být	k5eNaImAgNnP	být
na	na	k7c6	na
masce	maska	k1gFnSc6	maska
chladiče	chladič	k1gInSc2	chladič
umístěna	umístit	k5eAaPmNgFnS	umístit
stylizovaná	stylizovaný	k2eAgFnSc1d1	stylizovaná
soška	soška	k1gFnSc1	soška
jaguára	jaguár	k1gMnSc2	jaguár
<g/>
,	,	kIx,	,
tu	ten	k3xDgFnSc4	ten
nahradila	nahradit	k5eAaPmAgFnS	nahradit
z	z	k7c2	z
kapoty	kapota	k1gFnSc2	kapota
vybíhající	vybíhající	k2eAgFnSc1d1	vybíhající
hlava	hlava	k1gFnSc1	hlava
šelmy	šelma	k1gFnSc2	šelma
s	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
rozevřenými	rozevřený	k2eAgMnPc7d1	rozevřený
křídli	křídlit	k5eAaImRp2nS	křídlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
modelu	model	k1gInSc2	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
podílel	podílet	k5eAaImAgInS	podílet
výkonný	výkonný	k2eAgInSc1d1	výkonný
řadový	řadový	k2eAgInSc1d1	řadový
šestiválec	šestiválec	k1gInSc1	šestiválec
XK	XK	kA	XK
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
3	[number]	k4	3
442	[number]	k4	442
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
ze	z	k7c2	z
sportovního	sportovní	k2eAgInSc2d1	sportovní
modelu	model	k1gInSc2	model
XK	XK	kA	XK
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
<s>
Poskytoval	poskytovat	k5eAaImAgInS	poskytovat
výkon	výkon	k1gInSc4	výkon
160	[number]	k4	160
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
119	[number]	k4	119
kW	kW	kA	kW
<g/>
)	)	kIx)	)
při	při	k7c6	při
5	[number]	k4	5
200	[number]	k4	200
ot	ot	k1gMnSc1	ot
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
1,7	[number]	k4	1,7
tuny	tuna	k1gFnPc4	tuna
těžký	těžký	k2eAgInSc1d1	těžký
vůz	vůz	k1gInSc1	vůz
dokázal	dokázat	k5eAaPmAgInS	dokázat
rozjet	rozjet	k5eAaPmNgInS	rozjet
až	až	k9	až
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
161	[number]	k4	161
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
100	[number]	k4	100
mph	mph	k?	mph
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
se	se	k3xPyFc4	se
vůz	vůz	k1gInSc1	vůz
dostal	dostat	k5eAaPmAgInS	dostat
zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
14	[number]	k4	14
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
proporcemi	proporce	k1gFnPc7	proporce
dělalo	dělat	k5eAaImAgNnS	dělat
svižný	svižný	k2eAgInSc4d1	svižný
vůz	vůz	k1gInSc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
prostorným	prostorný	k2eAgInSc7d1	prostorný
a	a	k8xC	a
luxusním	luxusní	k2eAgInSc7d1	luxusní
interiérem	interiér	k1gInSc7	interiér
<g/>
,	,	kIx,	,
kvalitnímu	kvalitní	k2eAgNnSc3d1	kvalitní
dílenskému	dílenský	k2eAgNnSc3d1	dílenské
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
,	,	kIx,	,
dobrými	dobrý	k2eAgFnPc7d1	dobrá
jízdními	jízdní	k2eAgFnPc7d1	jízdní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
také	také	k9	také
tradičně	tradičně	k6eAd1	tradičně
nižší	nízký	k2eAgFnSc6d2	nižší
ceně	cena	k1gFnSc6	cena
oproti	oproti	k7c3	oproti
konkurenci	konkurence	k1gFnSc3	konkurence
se	se	k3xPyFc4	se
vůz	vůz	k1gInSc1	vůz
dočkal	dočkat	k5eAaPmAgInS	dočkat
úspěchu	úspěch	k1gInSc3	úspěch
doma	doma	k6eAd1	doma
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
i	i	k8xC	i
na	na	k7c6	na
důležitém	důležitý	k2eAgInSc6d1	důležitý
trhu	trh	k1gInSc6	trh
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
předchůdci	předchůdce	k1gMnPc1	předchůdce
přední	přední	k2eAgMnPc1d1	přední
nezávisle	závisle	k6eNd1	závisle
zavěšená	zavěšený	k2eAgNnPc4d1	zavěšené
kola	kolo	k1gNnPc4	kolo
i	i	k8xC	i
zadní	zadní	k2eAgFnSc4d1	zadní
tuhou	tuhý	k2eAgFnSc4d1	tuhá
nápravu	náprava	k1gFnSc4	náprava
s	s	k7c7	s
listovými	listový	k2eAgNnPc7d1	listové
pery	pero	k1gNnPc7	pero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
kolech	kolo	k1gNnPc6	kolo
měl	mít	k5eAaImAgInS	mít
vůz	vůz	k1gInSc1	vůz
hydraulické	hydraulický	k2eAgFnSc2d1	hydraulická
bubnové	bubnový	k2eAgFnSc2d1	bubnová
brzdy	brzda	k1gFnSc2	brzda
nyní	nyní	k6eAd1	nyní
i	i	k9	i
s	s	k7c7	s
nezbytným	zbytný	k2eNgInSc7d1	zbytný
posilovačem	posilovač	k1gInSc7	posilovač
<g/>
.	.	kIx.	.
</s>
<s>
Nevyhovující	vyhovující	k2eNgFnSc4d1	nevyhovující
dvoustupňovou	dvoustupňový	k2eAgFnSc4d1	dvoustupňová
automatickou	automatický	k2eAgFnSc4d1	automatická
převodovku	převodovka	k1gFnSc4	převodovka
nahradila	nahradit	k5eAaPmAgFnS	nahradit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
moderní	moderní	k2eAgFnSc1d1	moderní
třístupňová	třístupňový	k2eAgFnSc1d1	třístupňová
převodovka	převodovka	k1gFnSc1	převodovka
Borg	Borg	k1gMnSc1	Borg
Warner	Warner	k1gMnSc1	Warner
<g/>
.	.	kIx.	.
</s>
<s>
Britští	britský	k2eAgMnPc1d1	britský
zájemci	zájemce	k1gMnPc1	zájemce
si	se	k3xPyFc3	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
museli	muset	k5eAaImAgMnP	muset
ovšem	ovšem	k9	ovšem
počkat	počkat	k5eAaPmF	počkat
další	další	k2eAgInSc4d1	další
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
model	model	k1gInSc4	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrobní	výrobní	k2eAgFnPc1d1	výrobní
kapacity	kapacita	k1gFnPc1	kapacita
automobilky	automobilka	k1gFnSc2	automobilka
již	již	k6eAd1	již
nestačily	stačit	k5eNaBmAgInP	stačit
uspokojovat	uspokojovat	k5eAaImF	uspokojovat
všechnu	všechen	k3xTgFnSc4	všechen
poptávku	poptávka	k1gFnSc4	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
výrobní	výrobní	k2eAgInPc1d1	výrobní
prostory	prostor	k1gInPc1	prostor
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgInP	najít
v	v	k7c4	v
Browns	Browns	k1gInSc4	Browns
Lane	Lan	k1gFnSc2	Lan
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Coventry	Coventr	k1gInPc1	Coventr
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
modelu	model	k1gInSc2	model
nebyla	být	k5eNaImAgFnS	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
ani	ani	k8xC	ani
během	během	k7c2	během
stěhování	stěhování	k1gNnSc2	stěhování
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
spuštěním	spuštění	k1gNnSc7	spuštění
nové	nový	k2eAgFnSc2d1	nová
lakovny	lakovna	k1gFnSc2	lakovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
modelu	model	k1gInSc2	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
M	M	kA	M
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
celkem	celkem	k6eAd1	celkem
20	[number]	k4	20
937	[number]	k4	937
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaguar	Jaguar	k1gMnSc1	Jaguar
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
M	M	kA	M
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
na	na	k7c6	na
londýnském	londýnský	k2eAgInSc6d1	londýnský
autosalonu	autosalon	k1gInSc6	autosalon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
společně	společně	k6eAd1	společně
se	s	k7c7	s
sportovním	sportovní	k2eAgInSc7d1	sportovní
modelem	model	k1gInSc7	model
XK	XK	kA	XK
140	[number]	k4	140
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
sdílel	sdílet	k5eAaImAgMnS	sdílet
silnější	silný	k2eAgFnSc4d2	silnější
verzi	verze	k1gFnSc4	verze
šestiválcového	šestiválcový	k2eAgInSc2d1	šestiválcový
motoru	motor	k1gInSc2	motor
XK	XK	kA	XK
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nyní	nyní	k6eAd1	nyní
nabízel	nabízet	k5eAaImAgInS	nabízet
výkon	výkon	k1gInSc1	výkon
190	[number]	k4	190
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
141	[number]	k4	141
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
rychlost	rychlost	k1gFnSc1	rychlost
vozu	vůz	k1gInSc2	vůz
tak	tak	k6eAd1	tak
těsně	těsně	k6eAd1	těsně
atakovala	atakovat	k5eAaBmAgFnS	atakovat
hranici	hranice	k1gFnSc4	hranice
170	[number]	k4	170
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Mezi	mezi	k7c4	mezi
nejslavnější	slavný	k2eAgMnPc4d3	nejslavnější
vlastníky	vlastník	k1gMnPc4	vlastník
modelu	model	k1gInSc2	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
M	M	kA	M
patří	patřit	k5eAaImIp3nS	patřit
bezpochyby	bezpochyby	k6eAd1	bezpochyby
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
vůz	vůz	k1gInSc1	vůz
dostal	dostat	k5eAaPmAgInS	dostat
tmavě	tmavě	k6eAd1	tmavě
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
automatickou	automatický	k2eAgFnSc4d1	automatická
převodovku	převodovka	k1gFnSc4	převodovka
a	a	k8xC	a
figurku	figurka	k1gFnSc4	figurka
lva	lev	k1gMnSc2	lev
na	na	k7c6	na
masce	maska	k1gFnSc6	maska
chladiče	chladič	k1gInSc2	chladič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
vůz	vůz	k1gInSc1	vůz
vrácen	vrácen	k2eAgInSc1d1	vrácen
automobilce	automobilka	k1gFnSc3	automobilka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
uložila	uložit	k5eAaPmAgFnS	uložit
do	do	k7c2	do
firemního	firemní	k2eAgNnSc2d1	firemní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
060	[number]	k4	060
exemplářů	exemplář	k1gInPc2	exemplář
modelu	model	k1gInSc2	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
také	také	k9	také
Suezská	suezský	k2eAgFnSc1d1	Suezská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
cenu	cena	k1gFnSc4	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
postihl	postihnout	k5eAaPmAgInS	postihnout
automobilku	automobilka	k1gFnSc4	automobilka
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modely	model	k1gInPc1	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
a	a	k8xC	a
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
M	M	kA	M
se	se	k3xPyFc4	se
proslavily	proslavit	k5eAaPmAgInP	proslavit
také	také	k9	také
na	na	k7c6	na
závodní	závodní	k2eAgFnSc6d1	závodní
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
sériových	sériový	k2eAgInPc2d1	sériový
cestovních	cestovní	k2eAgInPc2d1	cestovní
vozů	vůz	k1gInPc2	vůz
pořádaných	pořádaný	k2eAgInPc2d1	pořádaný
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Silverstone	Silverston	k1gInSc5	Silverston
získal	získat	k5eAaPmAgInS	získat
Model	model	k1gInSc1	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
cenná	cenný	k2eAgNnPc4d1	cenné
vítězství	vítězství	k1gNnPc4	vítězství
především	především	k6eAd1	především
s	s	k7c7	s
pilotem	pilot	k1gMnSc7	pilot
Stirlingem	Stirling	k1gInSc7	Stirling
Mossem	Moss	k1gMnSc7	Moss
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
vítězství	vítězství	k1gNnSc1	vítězství
posádky	posádka	k1gFnSc2	posádka
Digger	digger	k1gMnSc1	digger
<g/>
/	/	kIx~	/
<g/>
Johnstone	Johnston	k1gInSc5	Johnston
<g/>
/	/	kIx~	/
<g/>
Adams	Adamsa	k1gFnPc2	Adamsa
na	na	k7c6	na
Rallye	rallye	k1gNnSc6	rallye
Monte	Mont	k1gInSc5	Mont
Carlo	Carlo	k1gNnSc1	Carlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
Jaguar	Jaguara	k1gFnPc2	Jaguara
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgMnSc6	první
výrobcem	výrobce	k1gMnSc7	výrobce
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
získat	získat	k5eAaPmF	získat
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gMnSc6	Carl
i	i	k9	i
v	v	k7c6	v
24	[number]	k4	24
<g/>
hodinovém	hodinový	k2eAgInSc6d1	hodinový
závodě	závod	k1gInSc6	závod
Le	Le	k1gFnSc1	Le
Mans	Mans	k1gInSc1	Mans
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
