<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
model	model	k1gInSc4	model
Mark	Mark	k1gMnSc1	Mark
VII	VII	kA	VII
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrobní	výrobní	k2eAgFnPc1d1	výrobní
kapacity	kapacita	k1gFnPc1	kapacita
automobilky	automobilka	k1gFnSc2	automobilka
již	již	k6eAd1	již
nestačily	stačit	k5eNaBmAgInP	stačit
uspokojovat	uspokojovat	k5eAaImF	uspokojovat
všechnu	všechen	k3xTgFnSc4	všechen
poptávku	poptávka	k1gFnSc4	poptávka
<g/>
.	.	kIx.	.
</s>
