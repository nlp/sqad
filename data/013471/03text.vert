<s>
Godsmack	Godsmack	k6eAd1
</s>
<s>
Godsmack	Godsmack	k6eAd1
Zleva	zleva	k6eAd1
doprava	doprava	k1gFnSc1
<g/>
:	:	kIx,
Robbie	Robbie	k1gFnSc1
Merrill	Merrilla	k1gFnPc2
<g/>
,	,	kIx,
Sully	Sulla	k1gMnSc2
Erna	Ernus	k1gMnSc2
<g/>
,	,	kIx,
Criss	Criss	k1gInSc1
Angel	Angela	k1gFnPc2
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
člen	člen	k1gMnSc1
kapely	kapela	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
<g/>
,	,	kIx,
Tony	Tony	k1gMnSc1
Rombola	Rombola	k1gFnSc1
<g/>
.	.	kIx.
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Původ	původ	k1gInSc1
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
Žánry	žánr	k1gInPc7
</s>
<s>
Heavy	Heava	k1gFnPc1
metalAlternative	metalAlternativ	k1gInSc5
metalHard	metalHard	k1gInSc1
rock	rock	k1gInSc1
Aktivní	aktivní	k2eAgInPc1d1
roky	rok	k1gInPc1
</s>
<s>
1995	#num#	k4
-	-	kIx~
současnost	současnost	k1gFnSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Universal	Universat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
Web	web	k1gInSc1
</s>
<s>
www.godsmack.com	www.godsmack.com	k1gInSc4
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Sully	Sulla	k1gFnPc1
ErnaTony	ErnaTona	k1gFnSc2
RombolaRobbie	RombolaRobbie	k1gFnSc2
MerrillShannon	MerrillShannon	k1gNnSc1
Larkin	Larkin	k1gInSc4
Dřívější	dřívější	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Tommy	Tomma	k1gFnPc1
StewartLee	StewartLee	k1gNnSc2
RichardsJoe	RichardsJoe	k1gNnSc2
D	D	kA
<g/>
'	'	kIx"
<g/>
Arco	Arco	k6eAd1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Godsmack	Godsmack	k1gNnSc1
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
heavy	heavy	k6eAd1
metalová	metalový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
pocházející	pocházející	k2eAgFnSc1d1
z	z	k7c2
Lawrence	Lawrence	k1gFnSc2
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapelu	kapela	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
frontman	frontman	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
písní	píseň	k1gFnPc2
Sully	Sully	k1gMnSc1
Erna	Erna	k1gMnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
Tony	Tony	k1gMnSc1
Rombola	Rombola	k1gMnSc1
<g/>
,	,	kIx,
baskytarista	baskytarista	k1gMnSc1
Robbie	Robbie	k1gMnSc1
Merrill	Merrill	k1gMnSc1
a	a	k8xC
bubeník	bubeník	k1gMnSc1
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
působnosti	působnost	k1gFnSc2
vydali	vydat	k5eAaPmAgMnP
Godsmack	Godsmack	k1gInSc4
již	již	k9
pět	pět	k4xCc4
studiových	studiový	k2eAgNnPc2d1
alb	album	k1gNnPc2
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
EP	EP	kA
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
demo	demo	k2eAgNnSc1d1
<g/>
,	,	kIx,
tři	tři	k4xCgInPc1
DVD	DVD	kA
a	a	k8xC
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
prodali	prodat	k5eAaPmAgMnP
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
19	#num#	k4
milionů	milion	k4xCgInPc2
nahrávek	nahrávka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skupina	skupina	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
pyšnit	pyšnit	k5eAaImF
třemi	tři	k4xCgFnPc7
studiovými	studiový	k2eAgFnPc7d1
deskami	deska	k1gFnPc7
(	(	kIx(
<g/>
Faceless	Faceless	k1gInSc1
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
a	a	k8xC
The	The	k1gFnSc1
Oracle	Oracle	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
dobyly	dobýt	k5eAaPmAgFnP
vrchol	vrchol	k1gInSc4
Billboard	billboard	k1gInSc1
200	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
Billboard	billboard	k1gInSc4
Hot	hot	k0
Mainstream	Mainstream	k1gInSc4
Rock	rock	k1gInSc1
Tracks	Tracks	k1gInSc1
(	(	kIx(
<g/>
prestižní	prestižní	k2eAgFnSc1d1
rocková	rockový	k2eAgFnSc1d1
hitparáda	hitparáda	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
patnáct	patnáct	k4xCc1
písní	píseň	k1gFnPc2
od	od	k7c2
kapely	kapela	k1gFnSc2
umístilo	umístit	k5eAaPmAgNnS
v	v	k7c6
Top	topit	k5eAaImRp2nS
5	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
takového	takový	k3xDgNnSc2
úspěchu	úspěch	k1gInSc3
nedosáhly	dosáhnout	k5eNaPmAgFnP
ani	ani	k9
tak	tak	k6eAd1
světoznámé	světoznámý	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
jako	jako	k8xC,k8xS
Linkin	Linkin	k2eAgInSc1d1
Park	park	k1gInSc1
nebo	nebo	k8xC
Metallica	Metallica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Godsmack	Godsmacko	k1gNnPc2
také	také	k9
často	často	k6eAd1
vystupují	vystupovat	k5eAaImIp3nP
na	na	k7c6
velkých	velký	k2eAgInPc6d1
festivalech	festival	k1gInPc6
typu	typ	k1gInSc2
Ozzfest	Ozzfest	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejpopulárnějších	populární	k2eAgMnPc2d3
heavy	heava	k1gFnPc4
metalových	metalový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
97	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
1995	#num#	k4
se	s	k7c7
Sully	Sullo	k1gNnPc7
Erna	Erna	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
založit	založit	k5eAaPmF
kapelu	kapela	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
by	by	kYmCp3nS
figuroval	figurovat	k5eAaImAgInS
jako	jako	k9
zpěvák	zpěvák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutí	rozhodnutí	k1gNnSc1
to	ten	k3xDgNnSc4
bylo	být	k5eAaImAgNnS
zarážející	zarážející	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
Sully	Sull	k1gInPc4
Erna	Ern	k1gInSc2
měl	mít	k5eAaImAgMnS
dlouholetou	dlouholetý	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
v	v	k7c4
hraní	hraní	k1gNnSc4
na	na	k7c4
bicí	bicí	k2eAgInPc4d1
(	(	kIx(
<g/>
23	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokonce	dokonce	k9
hrál	hrát	k5eAaImAgInS
v	v	k7c6
již	již	k6eAd1
rozpadlé	rozpadlý	k2eAgFnSc3d1
kapele	kapela	k1gFnSc3
Strip	Strip	k1gMnSc1
Mind	Mind	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
zněl	znět	k5eAaImAgMnS
The	The	k1gMnSc1
Scam	Scam	k1gMnSc1
a	a	k8xC
skládala	skládat	k5eAaImAgFnS
se	se	k3xPyFc4
z	z	k7c2
již	již	k6eAd1
zmíněného	zmíněný	k2eAgMnSc2d1
Ernyho	Erny	k1gMnSc2
<g/>
,	,	kIx,
baskytaristy	baskytarista	k1gMnSc2
Robbieho	Robbie	k1gMnSc2
Merrillla	Merrilll	k1gMnSc2
<g/>
,	,	kIx,
kytaristy	kytarista	k1gMnSc2
Lee	Lea	k1gFnSc3
Richardse	Richards	k1gMnSc2
a	a	k8xC
bubeníka	bubeník	k1gMnSc2
Tommyho	Tommy	k1gMnSc2
Stewarta	Stewart	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nahrání	nahrání	k1gNnSc6
dema	dema	k6eAd1
si	se	k3xPyFc3
formace	formace	k1gFnSc1
rychle	rychle	k6eAd1
změnila	změnit	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
na	na	k7c4
Godsmack	Godsmack	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Kapela	kapela	k1gFnSc1
začala	začít	k5eAaPmAgFnS
vystupovat	vystupovat	k5eAaImF
po	po	k7c6
malých	malý	k2eAgInPc6d1
klubech	klub	k1gInPc6
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
domovském	domovský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Bostonu	Boston	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písně	píseň	k1gFnPc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Whatever	Whatever	k1gInSc1
<g/>
"	"	kIx"
a	a	k8xC
„	„	k?
<g/>
Keep	Keep	k1gInSc1
Away	Awaa	k1gFnSc2
<g/>
"	"	kIx"
si	se	k3xPyFc3
rychle	rychle	k6eAd1
získaly	získat	k5eAaPmAgFnP
slušný	slušný	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
u	u	k7c2
místních	místní	k2eAgMnPc2d1
posluchačů	posluchač	k1gMnPc2
a	a	k8xC
Godsmack	Godsmacka	k1gFnPc2
se	se	k3xPyFc4
stávali	stávat	k5eAaImAgMnP
v	v	k7c6
Bostonu	Boston	k1gInSc6
(	(	kIx(
<g/>
potažmo	potažmo	k6eAd1
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Nové	Nové	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
)	)	kIx)
poměrně	poměrně	k6eAd1
populární	populární	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
skupiny	skupina	k1gFnSc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
Merrilla	Merrill	k1gMnSc2
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
písně	píseň	k1gFnSc2
„	„	k?
<g/>
God	God	k1gMnSc1
Smack	Smack	k1gMnSc1
<g/>
"	"	kIx"
od	od	k7c2
Alice	Alice	k1gFnSc2
in	in	k?
Chains	Chains	k1gInSc1
(	(	kIx(
<g/>
baskytarista	baskytarista	k1gMnSc1
tak	tak	k6eAd1
informoval	informovat	k5eAaBmAgMnS
na	na	k7c6
DVD	DVD	kA
Smack	Smack	k1gInSc4
This	Thisa	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
Erna	Erna	k1gMnSc1
v	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
interviewů	interview	k1gInPc2
uskutečněném	uskutečněný	k2eAgInSc6d1
roku	rok	k1gInSc6
1999	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jméno	jméno	k1gNnSc1
Godsmack	Godsmacka	k1gFnPc2
je	být	k5eAaImIp3nS
inspirováno	inspirovat	k5eAaBmNgNnS
barovým	barový	k2eAgInSc7d1
humorem	humor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
frontman	frontman	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Byli	být	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
si	se	k3xPyFc3
vědomi	vědom	k2eAgMnPc1d1
existence	existence	k1gFnSc2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
písně	píseň	k1gFnSc2
od	od	k7c2
Alice	Alice	k1gFnSc2
in	in	k?
Chains	Chains	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
příliš	příliš	k6eAd1
nás	my	k3xPp1nPc4
to	ten	k3xDgNnSc1
neovlivnilo	ovlivnit	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
se	se	k3xPyFc4
ke	k	k7c3
skupině	skupina	k1gFnSc3
připojil	připojit	k5eAaPmAgMnS
kytarista	kytarista	k1gMnSc1
Tony	Tony	k1gMnSc1
Rombola	Rombola	k1gFnSc1
a	a	k8xC
bubeník	bubeník	k1gMnSc1
Joe	Joe	k1gMnSc1
D	D	kA
<g/>
'	'	kIx"
<g/>
Arco	Arco	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
po	po	k7c6
odchodu	odchod	k1gInSc6
Richardse	Richards	k1gMnSc2
a	a	k8xC
Stewarta	Stewart	k1gMnSc2
(	(	kIx(
<g/>
první	první	k4xOgMnSc1
opustil	opustit	k5eAaPmAgMnS
Godsmack	Godsmack	k1gMnSc1
kvůli	kvůli	k7c3
svému	svůj	k3xOyFgMnSc3
malému	malý	k2eAgMnSc3d1
synovi	syn	k1gMnSc3
a	a	k8xC
druhý	druhý	k4xOgInSc1
kvůli	kvůli	k7c3
osobním	osobní	k2eAgInPc3d1
problémům	problém	k1gInPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
v	v	k7c6
tomtéž	týž	k3xTgInSc6
roce	rok	k1gInSc6
kapela	kapela	k1gFnSc1
nahrála	nahrát	k5eAaBmAgFnS,k5eAaPmAgFnS
své	své	k1gNnSc4
první	první	k4xOgFnSc2
studiové	studiový	k2eAgFnSc2d1
CD	CD	kA
nazvané	nazvaný	k2eAgFnSc2d1
All	All	k1gFnSc2
Wound	Wound	k1gMnSc1
Up	Up	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvorba	tvorba	k1gFnSc1
desky	deska	k1gFnSc2
trvala	trvat	k5eAaImAgFnS
pouhé	pouhý	k2eAgInPc4d1
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
a	a	k8xC
stála	stát	k5eAaImAgFnS
2600	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
Godsmack	Godsmacko	k1gNnPc2
vystupovali	vystupovat	k5eAaImAgMnP
po	po	k7c6
celém	celý	k2eAgInSc6d1
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
bez	bez	k7c2
větších	veliký	k2eAgInPc2d2
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
All	All	k1gFnSc1
Wound	Wounda	k1gFnPc2
Up	Up	k1gMnPc2
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
rukou	ruka	k1gFnPc2
DJ	DJ	kA
Rocko	Rocko	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
pracoval	pracovat	k5eAaImAgMnS
pro	pro	k7c4
rádio	rádio	k1gNnSc4
WAAF	WAAF	kA
(	(	kIx(
<g/>
FM	FM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vysílání	vysílání	k1gNnSc6
této	tento	k3xDgFnSc2
stanice	stanice	k1gFnSc2
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
velmi	velmi	k6eAd1
často	často	k6eAd1
hrát	hrát	k5eAaImF
píseň	píseň	k1gFnSc4
„	„	k?
<g/>
Keep	Keep	k1gMnSc1
Away	Awaa	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
dokázala	dokázat	k5eAaPmAgFnS
probojovat	probojovat	k5eAaPmF
na	na	k7c4
první	první	k4xOgFnSc4
místo	místo	k7c2
bostonské	bostonský	k2eAgFnSc2d1
rockové	rockový	k2eAgFnSc2d1
hitparády	hitparáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Vydavatelství	vydavatelství	k1gNnSc1
Newbury	Newbura	k1gFnSc2
Comics	comics	k1gInSc1
souhlasilo	souhlasit	k5eAaImAgNnS
s	s	k7c7
prodáváním	prodávání	k1gNnSc7
nahrávky	nahrávka	k1gFnSc2
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
obchodech	obchod	k1gInPc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgInS
pro	pro	k7c4
Godsmack	Godsmack	k1gInSc4
velký	velký	k2eAgInSc4d1
skok	skok	k1gInSc4
vpřed	vpřed	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěchu	úspěch	k1gInSc6
„	„	k?
<g/>
Keep	Keep	k1gInSc1
Away	Awaa	k1gFnSc2
<g/>
"	"	kIx"
se	se	k3xPyFc4
kapela	kapela	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
nahrát	nahrát	k5eAaBmF,k5eAaPmF
nový	nový	k2eAgInSc4d1
song	song	k1gInSc4
<g/>
,	,	kIx,
„	„	k?
<g/>
Whatever	Whatever	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stejně	stejně	k6eAd1
jako	jako	k9
„	„	k?
<g/>
Keep	Keep	k1gInSc1
Away	Awaa	k1gFnSc2
<g/>
"	"	kIx"
stal	stát	k5eAaPmAgMnS
neobyčejně	obyčejně	k6eNd1
populární	populární	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
jednom	jeden	k4xCgInSc6
rozhovoru	rozhovor	k1gInSc6
Sully	Sulla	k1gFnSc2
Erna	Ernus	k1gMnSc2
konstatoval	konstatovat	k5eAaBmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
si	se	k3xPyFc3
našeho	náš	k3xOp1gNnSc2
CD	CD	kA
všimla	všimnout	k5eAaPmAgFnS
WAAF	WAAF	kA
(	(	kIx(
<g/>
FM	FM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prodali	prodat	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
tak	tak	k9
50	#num#	k4
kopií	kopie	k1gFnPc2
měsíčně	měsíčně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhle	náhle	k6eAd1
začaly	začít	k5eAaPmAgFnP
prodeje	prodej	k1gFnPc1
rychle	rychle	k6eAd1
stoupat	stoupat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
bláznivé	bláznivý	k2eAgNnSc1d1
a	a	k8xC
nejzvláštnější	zvláštní	k2eAgMnSc1d3
na	na	k7c6
tom	ten	k3xDgNnSc6
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
nemuseli	muset	k5eNaImAgMnP
hnout	hnout	k5eAaPmF
ani	ani	k8xC
prstem	prst	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tolika	tolik	k4yIc6,k4xDc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
daly	dát	k5eAaPmAgFnP
věci	věc	k1gFnPc1
konečně	konečně	k6eAd1
do	do	k7c2
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Godsmack	Godsmack	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1998	#num#	k4
podepsali	podepsat	k5eAaPmAgMnP
Godsmack	Godsmack	k1gMnSc1
nahrávací	nahrávací	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Universal	Universal	k1gFnSc7
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnSc1
D	D	kA
<g/>
'	'	kIx"
<g/>
arco	arco	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
měl	mít	k5eAaImAgInS
řadu	řada	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
problémů	problém	k1gInPc2
s	s	k7c7
Ernou	Erna	k1gFnSc7
a	a	k8xC
kapela	kapela	k1gFnSc1
přijala	přijmout	k5eAaPmAgFnS
Tommyho	Tommy	k1gMnSc4
Stewarta	Stewart	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c4
Godsmack	Godsmack	k1gInSc4
již	již	k6eAd1
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
působil	působit	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
All	All	k1gMnSc1
Wound	Wound	k1gMnSc1
Up	Up	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
studiová	studiový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
kapely	kapela	k1gFnSc2
byla	být	k5eAaImAgFnS
od	od	k7c2
základu	základ	k1gInSc2
předělána	předělán	k2eAgFnSc1d1
a	a	k8xC
její	její	k3xOp3gInSc1
materiál	materiál	k1gInSc1
použit	použít	k5eAaPmNgInS
na	na	k7c6
následujícím	následující	k2eAgNnSc6d1
albu	album	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
dokončila	dokončit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
stejnojmenný	stejnojmenný	k2eAgInSc4d1
debut	debut	k1gInSc4
Godsmack	Godsmacka	k1gFnPc2
a	a	k8xC
o	o	k7c4
šest	šest	k4xCc4
týdnů	týden	k1gInPc2
později	pozdě	k6eAd2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1998	#num#	k4
<g/>
)	)	kIx)
jej	on	k3xPp3gNnSc2
vydala	vydat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
Godsmack	Godsmack	k1gInSc4
odstartovali	odstartovat	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
turné	turné	k1gNnSc4
„	„	k?
<g/>
The	The	k1gMnSc1
Voodoo	Voodoo	k1gMnSc1
Tour	Tour	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Kapela	kapela	k1gFnSc1
samozřejmě	samozřejmě	k6eAd1
vyrazila	vyrazit	k5eAaPmAgFnS
i	i	k9
na	na	k7c4
další	další	k2eAgInPc4d1
koncerty	koncert	k1gInPc4
a	a	k8xC
velké	velký	k2eAgInPc4d1
festivaly	festival	k1gInPc4
<g/>
,	,	kIx,
příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
Ozzfest	Ozzfest	k1gFnSc4
a	a	k8xC
Woodstock	Woodstock	k1gInSc4
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
evropské	evropský	k2eAgNnSc1d1
turné	turné	k1gNnSc1
pod	pod	k7c7
křídly	křídlo	k1gNnPc7
legendárních	legendární	k2eAgFnPc2d1
Black	Blacka	k1gFnPc2
Sabbath	Sabbath	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Roxanne	Roxann	k1gInSc5
Blanford	Blanfordo	k1gNnPc2
z	z	k7c2
Allmusic	Allmusice	k1gFnPc2
dala	dát	k5eAaPmAgFnS
albu	alba	k1gFnSc4
Godsmack	Godsmacka	k1gFnPc2
tři	tři	k4xCgInPc4
z	z	k7c2
pěti	pět	k4xCc2
hvězdiček	hvězdička	k1gFnPc2
a	a	k8xC
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Godsmack	Godsmack	k1gInSc1
zcela	zcela	k6eAd1
jistě	jistě	k6eAd1
přivedli	přivést	k5eAaPmAgMnP
metal	metal	k1gInSc4
do	do	k7c2
nové	nový	k2eAgFnSc2d1
technologické	technologický	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Deska	deska	k1gFnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc7
nahrávkou	nahrávka	k1gFnSc7
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
v	v	k7c4
Billboard	billboard	k1gInSc4
200	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
CD	CD	kA
Godsmack	Godsmack	k1gInSc4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
zlaté	zlatá	k1gFnSc2
(	(	kIx(
<g/>
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
roku	rok	k1gInSc2
2001	#num#	k4
4	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgNnSc1d1
(	(	kIx(
<g/>
4000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Album	album	k1gNnSc1
je	být	k5eAaImIp3nS
vnímáno	vnímat	k5eAaImNgNnS
kontroverzně	kontroverzně	k6eAd1
<g/>
,	,	kIx,
hlavně	hlavně	k9
kvůli	kvůli	k7c3
textům	text	k1gInPc3
obsahující	obsahující	k2eAgFnSc2d1
časté	častý	k2eAgFnSc2d1
nadávky	nadávka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Jeden	jeden	k4xCgMnSc1
otec	otec	k1gMnSc1
si	se	k3xPyFc3
po	po	k7c6
poslechnutí	poslechnutí	k1gNnSc6
desky	deska	k1gFnSc2
patřící	patřící	k2eAgFnSc2d1
synovi	syn	k1gMnSc3
šel	jít	k5eAaImAgMnS
stěžovat	stěžovat	k5eAaImF
do	do	k7c2
obchodního	obchodní	k2eAgInSc2d1
řetězce	řetězec	k1gInSc2
Wal-Mart	Wal-Marta	k1gFnPc2
na	na	k7c4
dostupnost	dostupnost	k1gFnSc4
Godsmack	Godsmacka	k1gFnPc2
malým	malý	k2eAgFnPc3d1
dětem	dítě	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
to	ten	k3xDgNnSc4
Wal-Mart	Wal-Mart	k1gInSc1
a	a	k8xC
Kmart	Kmart	k1gInSc1
(	(	kIx(
<g/>
další	další	k2eAgInSc1d1
obchodní	obchodní	k2eAgInSc1d1
řetězec	řetězec	k1gInSc1
<g/>
)	)	kIx)
stáhli	stáhnout	k5eAaPmAgMnP
nahrávku	nahrávka	k1gFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
prodejen	prodejna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc4
a	a	k8xC
jejich	jejich	k3xOp3gMnSc1
hudební	hudební	k2eAgMnSc1d1
vydavatel	vydavatel	k1gMnSc1
později	pozdě	k6eAd2
opatřili	opatřit	k5eAaPmAgMnP
album	album	k1gNnSc4
nálepkou	nálepka	k1gFnSc7
Parental	Parental	k1gMnSc1
Advisory	Advisor	k1gInPc4
(	(	kIx(
<g/>
dostupné	dostupný	k2eAgInPc1d1
osobám	osoba	k1gFnPc3
starším	starý	k2eAgFnPc3d2
17	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
bylo	být	k5eAaImAgNnS
CD	CD	kA
opět	opět	k6eAd1
umístěno	umístit	k5eAaPmNgNnS
do	do	k7c2
velkoobchodů	velkoobchod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sully	Sulla	k1gFnPc4
Erna	Ern	k1gInSc2
mluvil	mluvit	k5eAaImAgMnS
o	o	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
v	v	k7c6
časopise	časopis	k1gInSc6
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
<g/>
:	:	kIx,
„	„	k?
<g/>
Naše	náš	k3xOp1gInPc1
deska	deska	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
obchodech	obchod	k1gInPc6
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
rok	rok	k1gInSc4
bez	bez	k1gInSc1
parental	parentat	k5eAaImAgInS,k5eAaPmAgInS
advisory	advisor	k1gInPc7
a	a	k8xC
toto	tento	k3xDgNnSc1
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
a	a	k8xC
jediná	jediný	k2eAgFnSc1d1
stížnost	stížnost	k1gFnSc1
<g/>
…	…	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
však	však	k9
rozhodli	rozhodnout	k5eAaPmAgMnP
opatřit	opatřit	k5eAaPmF
Godsmack	Godsmack	k1gInSc4
zmíněnou	zmíněný	k2eAgFnSc7d1
visačkou	visačka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
spory	spor	k1gInPc1
se	se	k3xPyFc4
prodejnosti	prodejnost	k1gFnSc6
alba	album	k1gNnSc2
vůbec	vůbec	k9
nedotkly	dotknout	k5eNaPmAgFnP
<g/>
,	,	kIx,
ba	ba	k9
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
Sully	Sulla	k1gFnPc4
Erna	Ern	k1gInSc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Kontroverze	kontroverze	k1gFnSc1
Godsmack	Godsmacko	k1gNnPc2
mladé	mladá	k1gFnSc2
přímo	přímo	k6eAd1
popichovala	popichovat	k5eAaImAgFnS
ke	k	k7c3
koupi	koupě	k1gFnSc3
nahrávky	nahrávka	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
desce	deska	k1gFnSc6
tak	tak	k6eAd1
zvláštní	zvláštní	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Awake	Awake	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
Godsmack	Godsmack	k1gMnSc1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
studia	studio	k1gNnSc2
a	a	k8xC
po	po	k7c6
multi-platinovém	multi-platinový	k2eAgInSc6d1
úspěchu	úspěch	k1gInSc6
Godsmack	Godsmacka	k1gFnPc2
začali	začít	k5eAaPmAgMnP
pracovat	pracovat	k5eAaImF
na	na	k7c6
novém	nový	k2eAgNnSc6d1
studiovém	studiový	k2eAgNnSc6d1
albu	album	k1gNnSc6
Awake	Awak	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
prodeje	prodej	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2000	#num#	k4
a	a	k8xC
debutovala	debutovat	k5eAaBmAgFnS
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Billboard	billboard	k1gInSc1
200	#num#	k4
s	s	k7c7
prodeji	prodej	k1gInPc7
činícími	činící	k2eAgInPc7d1
v	v	k7c6
otevíracím	otevírací	k2eAgInSc6d1
týdnu	týden	k1gInSc6
256	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Awake	Awak	k1gInSc2
se	se	k3xPyFc4
i	i	k9
přes	přes	k7c4
komerční	komerční	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
nepodařilo	podařit	k5eNaPmAgNnS
dosáhnout	dosáhnout	k5eAaPmF
prodejnosti	prodejnost	k1gFnSc2
Godsmack	Godsmacka	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
to	ten	k3xDgNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
nic	nic	k3yNnSc4
nemění	měnit	k5eNaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nahrávka	nahrávka	k1gFnSc1
v	v	k7c6
USA	USA	kA
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c4
2	#num#	k4
<g/>
x	x	k?
platinovou	platinový	k2eAgFnSc7d1
(	(	kIx(
<g/>
2000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Singl	singl	k1gInSc1
„	„	k?
<g/>
Awake	Awake	k1gInSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
prvním	první	k4xOgInSc7
songem	song	k1gInSc7
od	od	k7c2
Godsmack	Godsmacka	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
obsadil	obsadit	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
americké	americký	k2eAgFnSc6d1
Hot	hot	k0
Mainstream	Mainstream	k1gInSc4
Rock	rock	k1gInSc1
Tracks	Tracks	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Píseň	píseň	k1gFnSc1
„	„	k?
<g/>
Vampires	Vampires	k1gInSc1
<g/>
"	"	kIx"
obdržela	obdržet	k5eAaPmAgFnS
nominaci	nominace	k1gFnSc4
Grammy	Gramma	k1gFnSc2
za	za	k7c4
Best	Best	k2eAgInSc4d1
Rock	rock	k1gInSc4
Instrumental	Instrumental	k1gMnSc1
Performance	performance	k1gFnSc2
(	(	kIx(
<g/>
nejlepší	dobrý	k2eAgFnSc4d3
rockovou	rockový	k2eAgFnSc4d1
instrumentální	instrumentální	k2eAgFnSc4d1
píseň	píseň	k1gFnSc4
<g/>
)	)	kIx)
pro	pro	k7c4
rok	rok	k1gInSc4
2002	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vydání	vydání	k1gNnSc6
Awake	Awak	k1gInSc2
se	se	k3xPyFc4
Godsmack	Godsmack	k1gMnSc1
odebrali	odebrat	k5eAaPmAgMnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
podporovali	podporovat	k5eAaImAgMnP
turné	turné	k1gNnSc4
Limp	limpa	k1gFnPc2
Bizkit	Bizkita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Erna	Ern	k1gInSc2
o	o	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Vystupovali	vystupovat	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
nonstop	nonstop	k6eAd1
od	od	k7c2
srpna	srpen	k1gInSc2
1998	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
většina	většina	k1gFnSc1
textů	text	k1gInPc2
k	k	k7c3
Awake	Awake	k1gNnSc3
byla	být	k5eAaImAgFnS
napsána	napsat	k5eAaBmNgFnS,k5eAaPmNgFnS
během	během	k7c2
cestování	cestování	k1gNnSc2
mezi	mezi	k7c7
Amerikou	Amerika	k1gFnSc7
a	a	k8xC
Evropou	Evropa	k1gFnSc7
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Vystoupení	vystoupení	k1gNnSc1
na	na	k7c6
Ozzfestu	Ozzfest	k1gInSc6
bylo	být	k5eAaImAgNnS
vlastně	vlastně	k9
jediné	jediný	k2eAgNnSc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
Godsmack	Godsmack	k1gInSc4
nevévodili	vévodit	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapela	kapela	k1gFnSc1
hrála	hrát	k5eAaImAgFnS
na	na	k7c6
Ozzfestu	Ozzfest	k1gInSc6
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
již	již	k9
podruhé	podruhé	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dvě	dva	k4xCgFnPc1
písně	píseň	k1gFnPc1
z	z	k7c2
Awake	Awak	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Sick	Sick	k1gInSc1
of	of	k?
Life	Life	k1gInSc1
<g/>
"	"	kIx"
a	a	k8xC
„	„	k?
<g/>
Awake	Awake	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
armádou	armáda	k1gFnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
použity	použít	k5eAaPmNgFnP
v	v	k7c6
jejích	její	k3xOp3gFnPc6
televizních	televizní	k2eAgFnPc6d1
reklamách	reklama	k1gFnPc6
jako	jako	k9
doprovodná	doprovodný	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erna	Erna	k1gMnSc1
informoval	informovat	k5eAaBmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Někdo	někdo	k3yInSc1
v	v	k7c6
armádě	armáda	k1gFnSc6
je	být	k5eAaImIp3nS
náš	náš	k3xOp1gMnSc1
fanoušek	fanoušek	k1gMnSc1
a	a	k8xC
oslovil	oslovit	k5eAaPmAgInS
nás	my	k3xPp1nPc2
<g/>
,	,	kIx,
jestli	jestli	k8xS
nemůže	moct	k5eNaImIp3nS
použít	použít	k5eAaPmF
naši	náš	k3xOp1gFnSc4
hudbu	hudba	k1gFnSc4
na	na	k7c4
propagaci	propagace	k1gFnSc4
náboru	nábor	k1gInSc2
rekrutů	rekrut	k1gMnPc2
do	do	k7c2
armády	armáda	k1gFnSc2
a	a	k8xC
my	my	k3xPp1nPc1
jsme	být	k5eAaImIp1nP
souhlasili	souhlasit	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Avšak	avšak	k8xC
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
Erna	Erna	k1gMnSc1
doplnil	doplnit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Godsmack	Godsmacka	k1gFnPc2
nepodporují	podporovat	k5eNaImIp3nP
žádný	žádný	k3yNgInSc4
ozbrojený	ozbrojený	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
nebo	nebo	k8xC
agresi	agrese	k1gFnSc4
vůči	vůči	k7c3
cizí	cizí	k2eAgFnSc3d1
zemi	zem	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
nejsme	být	k5eNaImIp1nP
zastánci	zastánce	k1gMnPc1
naší	náš	k3xOp1gFnSc2
vlády	vláda	k1gFnSc2
a	a	k8xC
nestrkáme	strkat	k5eNaImIp1nP
nos	nos	k1gInSc4
do	do	k7c2
věcí	věc	k1gFnPc2
jiných	jiný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
děláme	dělat	k5eAaImIp1nP
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
podporujeme	podporovat	k5eAaImIp1nP
naše	náš	k3xOp1gMnPc4
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jdou	jít	k5eAaImIp3nP
kamkoliv	kamkoliv	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
bojovali	bojovat	k5eAaImAgMnP
za	za	k7c4
naši	náš	k3xOp1gFnSc4
vlast	vlast	k1gFnSc4
<g/>
,	,	kIx,
naše	náš	k3xOp1gInPc4
životy	život	k1gInPc4
a	a	k8xC
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
to	ten	k3xDgNnSc4
cítím	cítit	k5eAaImIp1nS
já	já	k3xPp1nSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Faceless	Faceless	k1gInSc1
a	a	k8xC
The	The	k1gFnSc1
Other	Othra	k1gFnPc2
Side	Side	k1gFnPc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
je	být	k5eAaImIp3nS
současný	současný	k2eAgMnSc1d1
bubeník	bubeník	k1gMnSc1
Godsmack	Godsmack	k1gMnSc1
</s>
<s>
Roku	rok	k1gInSc2
2002	#num#	k4
byl	být	k5eAaImAgMnS
Erna	Erna	k1gMnSc1
požádán	požádat	k5eAaPmNgMnS
o	o	k7c4
napsání	napsání	k1gNnSc4
a	a	k8xC
nahrání	nahrání	k1gNnSc4
písně	píseň	k1gFnSc2
k	k	k7c3
soundtracku	soundtrack	k1gInSc3
pro	pro	k7c4
film	film	k1gInSc4
The	The	k1gFnSc2
Scorpion	Scorpion	k1gInSc1
King	King	k1gMnSc1
(	(	kIx(
<g/>
Král	Král	k1gMnSc1
Škorpion	škorpion	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
film	film	k1gInSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
sérii	série	k1gFnSc3
snímků	snímek	k1gInPc2
Mumie	mumie	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gMnSc1
předchůdce	předchůdce	k1gMnSc1
je	být	k5eAaImIp3nS
The	The	k1gMnSc7
Mummy	Mumma	k1gFnSc2
Returns	Returnsa	k1gFnPc2
(	(	kIx(
<g/>
Mumie	mumie	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc4
tedy	tedy	k9
vytvořili	vytvořit	k5eAaPmAgMnP
píseň	píseň	k1gFnSc4
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gInSc5
<g/>
"	"	kIx"
a	a	k8xC
k	k	k7c3
ní	on	k3xPp3gFnSc3
i	i	k8xC
potřebný	potřebný	k2eAgInSc4d1
videoklip	videoklip	k1gInSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gMnSc5
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
největších	veliký	k2eAgInPc2d3
hitů	hit	k1gInPc2
kapely	kapela	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
dominoval	dominovat	k5eAaImAgInS
rockovým	rockový	k2eAgInSc7d1
rádiím	rádio	k1gNnPc3
v	v	k7c6
Americe	Amerika	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
certifikován	certifikovat	k5eAaImNgInS
jako	jako	k9
zlatý	zlatý	k1gInSc1
(	(	kIx(
<g/>
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
byla	být	k5eAaImAgFnS
také	také	k9
použita	použít	k5eAaPmNgFnS
ve	v	k7c6
hře	hra	k1gFnSc6
Prince	princ	k1gMnSc2
of	of	k?
Persia	Persius	k1gMnSc2
<g/>
:	:	kIx,
Warrior	Warrior	k1gMnSc1
Within	Within	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
před	před	k7c7
započetím	započetí	k1gNnSc7
tvorby	tvorba	k1gFnSc2
třetího	třetí	k4xOgNnSc2
studiového	studiový	k2eAgNnSc2d1
alba	album	k1gNnSc2
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
od	od	k7c2
Godsmack	Godsmacka	k1gFnPc2
znovu	znovu	k6eAd1
Tommy	Tomma	k1gFnSc2
Stewart	Stewart	k1gMnSc1
(	(	kIx(
<g/>
opět	opět	k6eAd1
kvůli	kvůli	k7c3
osobním	osobní	k2eAgInPc3d1
rozdílům	rozdíl	k1gInPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
bubeníka	bubeník	k1gMnSc2
rychle	rychle	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
(	(	kIx(
<g/>
dřívější	dřívější	k2eAgInSc1d1
člen	člen	k1gInSc1
skupin	skupina	k1gFnPc2
Ugly	Ugla	k1gFnSc2
Kid	Kid	k1gFnSc1
Joe	Joe	k1gFnSc1
<g/>
,	,	kIx,
Souls	Souls	k1gInSc1
at	at	k?
Zero	Zero	k6eAd1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Wrathchild	Wrathchild	k1gMnSc1
America	America	k1gMnSc1
<g/>
,	,	kIx,
MF	MF	kA
Pitbulls	Pitbulls	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Godsmack	Godsmack	k1gMnSc1
nahráli	nahrát	k5eAaBmAgMnP,k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
nové	nový	k2eAgNnSc4d1
album	album	k1gNnSc4
Faceless	Facelessa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Deska	deska	k1gFnSc1
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Billboard	billboard	k1gInSc1
200	#num#	k4
<g/>
,	,	kIx,
prodávající	prodávající	k2eAgFnSc6d1
v	v	k7c6
první	první	k4xOgFnSc6
týdnu	týden	k1gInSc6
269	#num#	k4
000	#num#	k4
kopií	kopie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Faceless	Faceless	k1gInSc1
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
dokázalo	dokázat	k5eAaPmAgNnS
vystrnadit	vystrnadit	k5eAaPmF
z	z	k7c2
prvního	první	k4xOgNnSc2
místa	místo	k1gNnSc2
CD	CD	kA
Meteora	Meteor	k1gMnSc4
od	od	k7c2
nu	nu	k9
metalové	metalový	k2eAgFnSc2d1
formace	formace	k1gFnSc2
Linkin	Linkin	k2eAgInSc1d1
Park	park	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahrávka	nahrávka	k1gFnSc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
9	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Top	topit	k5eAaImRp2nS
Canadian	Canadiany	k1gInPc2
Albums	Albums	k1gInSc1
a	a	k8xC
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Top	topit	k5eAaImRp2nS
Internet	Internet	k1gInSc1
Albums	Albums	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
kralovala	kralovat	k5eAaImAgFnS
po	po	k7c4
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
ohromující	ohromující	k2eAgNnSc1d1
turné	turné	k1gNnSc1
po	po	k7c6
boku	bok	k1gInSc6
Metallicy	Metallica	k1gFnSc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnSc4d1
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
Faceless	Faceless	k1gInSc4
v	v	k7c6
USA	USA	kA
certifikováno	certifikovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
platinové	platinový	k2eAgNnSc1d1
(	(	kIx(
<g/>
1000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úvodní	úvodní	k2eAgInSc1d1
singl	singl	k1gInSc1
z	z	k7c2
Faceless	Facelessa	k1gFnPc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
Straight	Straight	k1gInSc1
Out	Out	k1gFnSc1
of	of	k?
Line	linout	k5eAaImIp3nS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
píseň	píseň	k1gFnSc4
prorazila	prorazit	k5eAaPmAgFnS
do	do	k7c2
Billboard	billboard	k1gInSc1
Hot	hot	k0
100	#num#	k4
a	a	k8xC
okupovala	okupovat	k5eAaBmAgFnS
73	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
získala	získat	k5eAaPmAgFnS
nominaci	nominace	k1gFnSc4
Grammy	Gramma	k1gFnSc2
za	za	k7c4
Best	Best	k2eAgInSc4d1
Hard	Hard	k1gInSc4
Rock	rock	k1gInSc1
Performance	performance	k1gFnSc1
(	(	kIx(
<g/>
nejlepší	dobrý	k2eAgMnSc1d3
hard	hard	k1gMnSc1
rockové	rockový	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovšem	ovšem	k9
cenu	cena	k1gFnSc4
nevyhrála	vyhrát	k5eNaPmAgFnS
(	(	kIx(
<g/>
vítězem	vítěz	k1gMnSc7
byl	být	k5eAaImAgMnS
singl	singl	k1gInSc4
„	„	k?
<g/>
Bring	Bring	k1gMnSc1
Me	Me	k1gMnSc1
to	ten	k3xDgNnSc4
Life	Life	k1gNnSc4
<g/>
"	"	kIx"
od	od	k7c2
Evanescence	Evanescence	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Song	song	k1gInSc1
„	„	k?
<g/>
I	i	k8xC
Fucking	Fucking	k1gInSc1
Hate	Hat	k1gMnSc2
You	You	k1gMnSc2
<g/>
"	"	kIx"
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
spotu	spot	k1gInSc6
propagující	propagující	k2eAgInSc4d1
film	film	k1gInSc4
Piráti	pirát	k1gMnPc1
z	z	k7c2
Karibiku	Karibik	k1gInSc2
<g/>
:	:	kIx,
Prokletí	prokletí	k1gNnSc4
Černé	Černé	k2eAgFnSc2d1
perly	perla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Album	album	k1gNnSc1
dostalo	dostat	k5eAaPmAgNnS
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
po	po	k7c6
incidentu	incident	k1gInSc6
u	u	k7c2
bazénu	bazén	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
později	pozdě	k6eAd2
objasnil	objasnit	k5eAaPmAgMnS
Larkin	Larkin	k1gMnSc1
a	a	k8xC
Erna	Erna	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Baskytarista	baskytarista	k1gMnSc1
Merrill	Merrill	k1gMnSc1
ovšem	ovšem	k9
uvedl	uvést	k5eAaPmAgMnS
jiné	jiný	k2eAgNnSc4d1
tvrzení	tvrzení	k1gNnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Jméno	jméno	k1gNnSc1
Faceless	Faceless	k1gInSc1
vystihuje	vystihovat	k5eAaImIp3nS
pocity	pocit	k1gInPc4
celé	celý	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k7c3
úspěchu	úspěch	k1gInSc3
komerčnímu	komerční	k2eAgNnSc3d1
a	a	k8xC
prodejnímu	prodejní	k2eAgNnSc3d1
<g/>
,	,	kIx,
jsme	být	k5eAaImIp1nP
měli	mít	k5eAaImAgMnP
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
stále	stále	k6eAd1
není	být	k5eNaImIp3nS
ono	onen	k3xDgNnSc4
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2004	#num#	k4
realizovali	realizovat	k5eAaBmAgMnP
Godsmack	Godsmack	k1gInSc4
akustické	akustický	k2eAgFnSc2d1
EP	EP	kA
The	The	k1gMnSc1
Other	Other	k1gMnSc1
Side	Side	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
debutovala	debutovat	k5eAaBmAgFnS
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Billboard	billboard	k1gInSc1
200	#num#	k4
s	s	k7c7
98	#num#	k4
000	#num#	k4
prodanými	prodaný	k2eAgInPc7d1
nosiči	nosič	k1gInPc7
v	v	k7c6
otevíracím	otevírací	k2eAgInSc6d1
týdnu	týden	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
EP	EP	kA
(	(	kIx(
<g/>
navíc	navíc	k6eAd1
akustické	akustický	k2eAgNnSc1d1
<g/>
)	)	kIx)
neobvykle	obvykle	k6eNd1
vysoká	vysoký	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
CD	CD	kA
obsahuje	obsahovat	k5eAaImIp3nS
dříve	dříve	k6eAd2
nezveřejněné	zveřejněný	k2eNgFnPc4d1
písně	píseň	k1gFnPc4
i	i	k9
nově	nově	k6eAd1
předelané	předelaný	k2eAgInPc1d1
známé	známý	k2eAgInPc1d1
singly	singl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nových	nový	k2eAgInPc2d1
songů	song	k1gInPc2
(	(	kIx(
<g/>
druhý	druhý	k4xOgInSc1
singl	singl	k1gInSc1
z	z	k7c2
The	The	k1gFnSc2
Other	Other	k1gInSc1
Side	Side	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
Touché	Touchý	k2eAgFnPc1d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
nabízí	nabízet	k5eAaImIp3nS
k	k	k7c3
poslechu	poslech	k1gInSc2
přizvaného	přizvaný	k2eAgMnSc4d1
kytaristu	kytarista	k1gMnSc4
Lee	Lea	k1gFnSc3
Richardse	Richards	k1gMnSc2
a	a	k8xC
zpěváka	zpěvák	k1gMnSc2
Johna	John	k1gMnSc2
Kosco	Kosco	k6eAd1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
byl	být	k5eAaImAgMnS
frontman	frontman	k1gMnSc1
nyní	nyní	k6eAd1
již	již	k6eAd1
rozpadlé	rozpadlý	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Dropbox	Dropbox	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Další	další	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
akustické	akustický	k2eAgFnPc4d1
písně	píseň	k1gFnPc4
nesou	nést	k5eAaImIp3nP
titulky	titulek	k1gInPc1
„	„	k?
<g/>
Running	Running	k1gInSc1
Blind	Blind	k1gMnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
první	první	k4xOgInSc1
singl	singl	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
Voices	Voices	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Song	song	k1gInSc1
„	„	k?
<g/>
Asleep	Asleep	k1gInSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
předělanou	předělaný	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
singlu	singl	k1gInSc2
„	„	k?
<g/>
Awake	Awak	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
"	"	kIx"
z	z	k7c2
druhého	druhý	k4xOgNnSc2
studiového	studiový	k2eAgNnSc2d1
alba	album	k1gNnSc2
Awake	Awak	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
The	The	k1gFnSc6
Other	Othra	k1gFnPc2
Side	Side	k1gFnPc2
se	se	k3xPyFc4
Godsmack	Godsmack	k1gInSc1
vzdalují	vzdalovat	k5eAaImIp3nP
svým	svůj	k3xOyFgFnPc3
hutným	hutný	k2eAgFnPc3d1
<g/>
,	,	kIx,
silným	silný	k2eAgFnPc3d1
písním	píseň	k1gFnPc3
a	a	k8xC
přistupují	přistupovat	k5eAaImIp3nP
k	k	k7c3
zjemnělému	zjemnělý	k2eAgInSc3d1
akustickému	akustický	k2eAgInSc3d1
stylu	styl	k1gInSc3
<g/>
,	,	kIx,
podobnému	podobný	k2eAgNnSc3d1
na	na	k7c6
EP	EP	kA
od	od	k7c2
Alice	Alice	k1gFnSc2
in	in	k?
Chains	Chainsa	k1gFnPc2
(	(	kIx(
<g/>
Sap	sapa	k1gFnPc2
a	a	k8xC
Jar	Jara	k1gFnPc2
of	of	k?
Flies	Fliesa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
této	tento	k3xDgFnSc2
podobnosti	podobnost	k1gFnSc2
byli	být	k5eAaImAgMnP
Godsmack	Godsmack	k1gInSc4
některými	některý	k3yIgMnPc7
recenzenty	recenzent	k1gMnPc7
kritizováni	kritizovat	k5eAaImNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
I	i	k9
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
The	The	k1gMnSc3
Other	Other	k1gInSc1
Side	Side	k1gNnSc2
v	v	k7c4
USA	USA	kA
označeno	označen	k2eAgNnSc1d1
za	za	k7c4
zlaté	zlatý	k2eAgInPc4d1
(	(	kIx(
<g/>
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
předskakovala	předskakovat	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
na	na	k7c4
turné	turné	k1gNnSc4
Madly	Madla	k1gFnSc2
in	in	k?
Anger	Anger	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
tour	tour	k1gMnSc1
pro	pro	k7c4
Metallicu	Metallica	k1gFnSc4
a	a	k8xC
vystupovala	vystupovat	k5eAaImAgFnS
na	na	k7c6
koncertech	koncert	k1gInPc6
společně	společně	k6eAd1
s	s	k7c7
Dropbox	Dropbox	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
na	na	k7c4
podzim	podzim	k1gInSc4
2004	#num#	k4
<g/>
,	,	kIx,
uskutečnili	uskutečnit	k5eAaPmAgMnP
Godsmack	Godsmack	k1gInSc4
několik	několik	k4yIc4
akustických	akustický	k2eAgFnPc2d1
show	show	k1gFnPc2
na	na	k7c4
podporu	podpora	k1gFnSc4
The	The	k1gMnPc2
Other	Other	k1gInSc4
Side	Side	k1gFnPc2
a	a	k8xC
v	v	k7c6
té	ten	k3xDgFnSc6
samé	samý	k3xTgFnSc6
době	doba	k1gFnSc6
dokončili	dokončit	k5eAaPmAgMnP
turné	turné	k1gNnSc4
s	s	k7c7
Metallicou	Metallica	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IV	IV	kA
a	a	k8xC
Ten	ten	k3xDgInSc1
Years	Years	k1gInSc1
of	of	k?
Godsmack	Godsmack	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tony	Tony	k1gFnSc1
Rombola	Rombola	k1gFnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
Godsmack	Godsmack	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
spatřilo	spatřit	k5eAaPmAgNnS
světlo	světlo	k1gNnSc1
světa	svět	k1gInSc2
čtvrté	čtvrtá	k1gFnSc2
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
od	od	k7c2
Godsmack	Godsmacka	k1gFnPc2
jednoduše	jednoduše	k6eAd1
nazvané	nazvaný	k2eAgInPc1d1
IV	IV	kA
a	a	k8xC
následované	následovaný	k2eAgFnPc1d1
„	„	k?
<g/>
The	The	k1gFnPc1
IV	Iva	k1gFnPc2
tour	toura	k1gFnPc2
<g/>
"	"	kIx"
končící	končící	k2eAgFnSc2d1
až	až	k6eAd1
v	v	k7c6
srpnu	srpen	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Album	album	k1gNnSc1
bylo	být	k5eAaImAgNnS
produkováno	produkován	k2eAgNnSc1d1
Ernou	Erný	k2eAgFnSc7d1
a	a	k8xC
vytvořené	vytvořený	k2eAgInPc4d1
světoznámým	světoznámý	k2eAgMnSc7d1
producentem	producent	k1gMnSc7
Andy	Anda	k1gFnSc2
Johnsem	Johns	k1gMnSc7
<g/>
,	,	kIx,
známého	známý	k2eAgNnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
tvorbou	tvorba	k1gFnSc7
Led	led	k1gInSc4
Zeppelin	Zeppelin	k2eAgInSc4d1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Pilotním	pilotní	k2eAgInSc7d1
singlem	singl	k1gInSc7
z	z	k7c2
IV	Iva	k1gFnPc2
je	být	k5eAaImIp3nS
píseň	píseň	k1gFnSc1
„	„	k?
<g/>
Speak	Speak	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dominovala	dominovat	k5eAaImAgFnS
americkým	americký	k2eAgNnSc7d1
rockovým	rockový	k2eAgNnSc7d1
rádiím	rádio	k1gNnPc3
a	a	k8xC
probojovala	probojovat	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c4
85	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
Billboard	billboard	k1gInSc1
Hot	hot	k0
100	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Deska	deska	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
samotná	samotný	k2eAgFnSc1d1
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Billboard	billboard	k1gInSc1
200	#num#	k4
a	a	k8xC
v	v	k7c6
úvodním	úvodní	k2eAgInSc6d1
týdnu	týden	k1gInSc6
prodala	prodat	k5eAaPmAgFnS
211	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
IV	IV	kA
je	být	k5eAaImIp3nS
prozatím	prozatím	k6eAd1
v	v	k7c6
USA	USA	kA
certifikována	certifikovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
zlatá	zlatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Kapela	kapela	k1gFnSc1
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
pro	pro	k7c4
album	album	k1gNnSc4
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
čtyřicet	čtyřicet	k4xCc4
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
finální	finální	k2eAgInSc1d1
tracklist	tracklist	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
pouze	pouze	k6eAd1
jedenáct	jedenáct	k4xCc4
songů	song	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Larkin	Larkin	k1gMnSc1
to	ten	k3xDgNnSc4
okomentoval	okomentovat	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Sullyho	Sully	k1gMnSc2
skupina	skupina	k1gFnSc1
a	a	k8xC
on	on	k3xPp3gMnSc1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vizi	vize	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohrabal	prohrabat	k5eAaPmAgMnS
se	s	k7c7
všemi	všecek	k3xTgFnPc7
písněmi	píseň	k1gFnPc7
a	a	k8xC
vybral	vybrat	k5eAaPmAgMnS
si	se	k3xPyFc3
ty	ten	k3xDgInPc4
songy	song	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
chtěl	chtít	k5eAaImAgMnS
mít	mít	k5eAaImF
na	na	k7c6
CD	CD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
z	z	k7c2
nás	my	k3xPp1nPc2
říkal	říkat	k5eAaImAgMnS
'	'	kIx"
<g/>
vše	všechen	k3xTgNnSc1
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
měl	mít	k5eAaImAgInS
vždy	vždy	k6eAd1
představu	představa	k1gFnSc4
o	o	k7c6
všem	všecek	k3xTgNnSc6
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
Godsmack	Godsmack	k1gInSc1
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k9
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
pouhý	pouhý	k2eAgInSc1d1
obal	obal	k1gInSc1
alba	album	k1gNnSc2
<g/>
,	,	kIx,
produkce	produkce	k1gFnSc2
a	a	k8xC
tvorba	tvorba	k1gFnSc1
<g/>
,	,	kIx,
či	či	k8xC
vystupování	vystupování	k1gNnSc4
v	v	k7c6
televizních	televizní	k2eAgFnPc6d1
show	show	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostě	prostě	k6eAd1
všechno	všechen	k3xTgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
čas	čas	k1gInSc4
vybírat	vybírat	k5eAaImF
písně	píseň	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jen	jen	k9
Sullyho	Sully	k1gMnSc4
věc	věc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jméno	jméno	k1gNnSc1
desky	deska	k1gFnSc2
IV	Iva	k1gFnPc2
není	být	k5eNaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
pouze	pouze	k6eAd1
z	z	k7c2
holého	holý	k2eAgInSc2d1
faktu	fakt	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
čtvrtá	čtvrtý	k4xOgFnSc1
plnohodnotná	plnohodnotný	k2eAgFnSc1d1
nahrávka	nahrávka	k1gFnSc1
od	od	k7c2
Godsmack	Godsmacka	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
kořeny	kořen	k1gInPc4
v	v	k7c6
zákulisním	zákulisní	k2eAgInSc6d1
humoru	humor	k1gInSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
Larkin	Larkin	k1gMnSc1
a	a	k8xC
Erna	Erna	k1gMnSc1
sdělili	sdělit	k5eAaPmAgMnP
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Měli	mít	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
jednoho	jeden	k4xCgMnSc4
hlídače	hlídač	k1gMnSc4
<g/>
,	,	kIx,
velkého	velký	k2eAgMnSc4d1
<g/>
,	,	kIx,
tvrdého	tvrdý	k2eAgMnSc4d1
chlapa	chlap	k1gMnSc4
s	s	k7c7
jménem	jméno	k1gNnSc7
J.C.	J.C.	k1gFnSc2
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
další	další	k2eAgMnSc1d1
chlapík	chlapík	k1gMnSc1
z	z	k7c2
Bostonu	Boston	k1gInSc2
a	a	k8xC
v	v	k7c6
Bostonu	Boston	k1gInSc6
se	se	k3xPyFc4
čtyři	čtyři	k4xCgMnPc4
nevyslovuje	vyslovovat	k5eNaImIp3nS
„	„	k?
<g/>
four	four	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ale	ale	k8xC
„	„	k?
<g/>
fou	fou	k?
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořád	pořád	k6eAd1
si	se	k3xPyFc3
prohlížel	prohlížet	k5eAaImAgMnS
dívky	dívka	k1gFnPc4
v	v	k7c6
zákulisí	zákulisí	k1gNnSc6
a	a	k8xC
hodnotil	hodnotit	k5eAaImAgMnS
je	být	k5eAaImIp3nS
od	od	k7c2
jedničky	jednička	k1gFnSc2
do	do	k7c2
desítky	desítka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
to	ten	k3xDgNnSc1
ale	ale	k9
nebyla	být	k5eNaImAgFnS
desítka	desítka	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
vždy	vždy	k6eAd1
čtyřka	čtyřka	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
fou	fou	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
když	když	k8xS
viděl	vidět	k5eAaImAgMnS
další	další	k2eAgFnPc4d1
holky	holka	k1gFnPc4
tak	tak	k6eAd1
jenom	jenom	k6eAd1
ukázal	ukázat	k5eAaPmAgMnS
na	na	k7c6
prstech	prst	k1gInPc6
čtyři	čtyři	k4xCgFnPc4
a	a	k8xC
my	my	k3xPp1nPc1
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
hlasitě	hlasitě	k6eAd1
smát	smát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
teď	teď	k6eAd1
to	ten	k3xDgNnSc1
nejsrandovnější	srandovní	k2eAgNnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
se	se	k3xPyFc4
kolem	kolem	k7c2
něj	on	k3xPp3gInSc2
motal	motat	k5eAaImAgMnS
kluk	kluk	k1gMnSc1
s	s	k7c7
dvěma	dva	k4xCgInPc7
holkama	holkama	k?
po	po	k7c6
boku	bok	k1gInSc6
a	a	k8xC
on	on	k3xPp3gMnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Hey	Hey	k1gMnSc5
chlapče	chlapec	k1gMnSc5
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc1
čtyřky	čtyřka	k1gFnPc1
nedělají	dělat	k5eNaImIp3nP
osmičku	osmička	k1gFnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
A	a	k8xC
tak	tak	k6eAd1
jsem	být	k5eAaImIp1nS
své	svůj	k3xOyFgNnSc4
nadcházející	nadcházející	k2eAgNnSc4d1
album	album	k1gNnSc4
pojmenovali	pojmenovat	k5eAaPmAgMnP
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
CD	CD	kA
nechceme	chtít	k5eNaImIp1nP
být	být	k5eAaImF
nějak	nějak	k6eAd1
originální	originální	k2eAgInPc4d1
<g/>
,	,	kIx,
prostě	prostě	k9
chceme	chtít	k5eAaImIp1nP
udělat	udělat	k5eAaPmF
desku	deska	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
líbit	líbit	k5eAaImF
všem	všecek	k3xTgMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
existují	existovat	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc1
nahrávek	nahrávka	k1gFnPc2
s	s	k7c7
titulkem	titulek	k1gInSc7
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nám	my	k3xPp1nPc3
to	ten	k3xDgNnSc1
přijde	přijít	k5eAaPmIp3nS
vhodné	vhodný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Aby	aby	kYmCp3nP
oslavili	oslavit	k5eAaPmAgMnP
deset	deset	k4xCc1
let	léto	k1gNnPc2
své	svůj	k3xOyFgFnSc2
působnosti	působnost	k1gFnSc2
<g/>
,	,	kIx,
vydali	vydat	k5eAaPmAgMnP
Godsmack	Godsmack	k1gInSc4
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
své	svůj	k3xOyFgInPc4
největší	veliký	k2eAgInSc4d3
hity	hit	k1gInPc4
na	na	k7c6
desce	deska	k1gFnSc6
nazvané	nazvaný	k2eAgFnSc6d1
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
,	,	kIx,
Bad	Bad	k1gMnSc1
Times	Times	k1gInSc1
<g/>
...	...	k?
Ten	ten	k3xDgInSc1
Years	Years	k1gInSc1
of	of	k?
Godsmack	Godsmack	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
okupovalo	okupovat	k5eAaBmAgNnS
35	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c4
Billboard	billboard	k1gInSc4
200	#num#	k4
a	a	k8xC
prodalo	prodat	k5eAaPmAgNnS
v	v	k7c6
prvním	první	k4xOgInSc6
týdnu	týden	k1gInSc6
40	#num#	k4
000	#num#	k4
kopií	kopie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
CD	CD	kA
nabízí	nabízet	k5eAaImIp3nS
největší	veliký	k2eAgInPc4d3
hity	hit	k1gInPc4
Godsmack	Godsmacka	k1gFnPc2
<g/>
,	,	kIx,
cover	cover	k1gInSc1
verzi	verze	k1gFnSc4
známé	známý	k2eAgFnPc4d1
písně	píseň	k1gFnPc4
„	„	k?
<g/>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
Bad	Bad	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
"	"	kIx"
od	od	k7c2
Led	led	k1gInSc1
Zeppelin	Zeppelin	k2eAgMnSc1d1
a	a	k8xC
DVD	DVD	kA
obsahující	obsahující	k2eAgMnSc1d1
akustické	akustický	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
kapely	kapela	k1gFnSc2
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegas	k1gInSc1
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
v	v	k7c4
House	house	k1gNnSc4
of	of	k?
Blues	blues	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
alba	album	k1gNnSc2
pořádali	pořádat	k5eAaImAgMnP
Godsmack	Godsmack	k1gInSc4
akustické	akustický	k2eAgNnSc4d1
turné	turné	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Navzdory	navzdory	k7c3
zprávám	zpráva	k1gFnPc3
o	o	k7c6
plánované	plánovaný	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
plynoucí	plynoucí	k2eAgFnSc6d1
z	z	k7c2
tvorby	tvorba	k1gFnSc2
největších	veliký	k2eAgInPc2d3
hitů	hit	k1gInPc2
<g/>
,	,	kIx,
Erna	Erna	k1gMnSc1
informoval	informovat	k5eAaBmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
V	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
nechceme	chtít	k5eNaImIp1nP
skončit	skončit	k5eAaPmF
<g/>
,	,	kIx,
jen	jen	k9
si	se	k3xPyFc3
chceme	chtít	k5eAaImIp1nP
užít	užít	k5eAaPmF
své	svůj	k3xOyFgNnSc4
desetileté	desetiletý	k2eAgNnSc4d1
výročí	výročí	k1gNnSc4
<g/>
,	,	kIx,
toť	toť	k?
vše	všechen	k3xTgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
si	se	k3xPyFc3
dobijeme	dobít	k5eAaPmIp1nP
baterky	baterka	k1gFnPc4
tak	tak	k6eAd1
se	se	k3xPyFc4
vrátíme	vrátit	k5eAaPmIp1nP
větší	veliký	k2eAgFnPc4d2
a	a	k8xC
zkaženější	zkažený	k2eAgFnPc4d2
<g/>
,	,	kIx,
než	než	k8xS
kdykoliv	kdykoliv	k6eAd1
předtím	předtím	k6eAd1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
The	The	k?
Oracle	Oracle	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2008	#num#	k4
Larkin	Larkin	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
kapela	kapela	k1gFnSc1
nejspíše	nejspíše	k9
začne	začít	k5eAaPmIp3nS
pracovat	pracovat	k5eAaImF
na	na	k7c6
novém	nový	k2eAgNnSc6d1
albu	album	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
létě	léto	k1gNnSc6
2009	#num#	k4
Godsmack	Godsmacko	k1gNnPc2
podporovali	podporovat	k5eAaImAgMnP
Mötley	Mötlea	k1gFnPc4
Crüe	Crü	k1gFnSc2
na	na	k7c6
festivalu	festival	k1gInSc6
Crüe	Crü	k1gFnSc2
Fest	fest	k6eAd1
2	#num#	k4
a	a	k8xC
vypustili	vypustit	k5eAaPmAgMnP
do	do	k7c2
rádií	rádio	k1gNnPc2
nový	nový	k2eAgInSc1d1
singl	singl	k1gInSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Whiskey	Whiskey	k1gInPc1
Hangover	Hangover	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
neměl	mít	k5eNaImAgMnS
být	být	k5eAaImF
součástí	součást	k1gFnSc7
žádného	žádný	k1gMnSc2
CD	CD	kA
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
figuroval	figurovat	k5eAaImAgMnS
v	v	k7c6
limitované	limitovaný	k2eAgFnSc6d1
edici	edice	k1gFnSc6
nadcházejícího	nadcházející	k2eAgNnSc2d1
alba	album	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
turné	turné	k1gNnSc6
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
studia	studio	k1gNnSc2
a	a	k8xC
odstartovala	odstartovat	k5eAaPmAgFnS
nahrávací	nahrávací	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
měla	mít	k5eAaImAgFnS
dostat	dostat	k5eAaPmF
jméno	jméno	k1gNnSc4
Saints	Saintsa	k1gFnPc2
&	&	k?
Sinners	Sinnersa	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
název	název	k1gInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
změněn	změnit	k5eAaPmNgInS
<g/>
,	,	kIx,
protože	protože	k8xS
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
originální	originální	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Producentem	producent	k1gMnSc7
nahrávky	nahrávka	k1gFnSc2
je	být	k5eAaImIp3nS
Erna	Erna	k1gFnSc1
a	a	k8xC
přizvaný	přizvaný	k2eAgInSc1d1
Dave	Dav	k1gInSc5
Fortman	Fortman	k1gMnSc1
(	(	kIx(
<g/>
produkoval	produkovat	k5eAaImAgMnS
desky	deska	k1gFnPc4
kapely	kapela	k1gFnSc2
Slipknot	Slipknota	k1gFnPc2
a	a	k8xC
Evanescence	Evanescence	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Páté	pátý	k4xOgNnSc1
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
Godsmack	Godsmacka	k1gFnPc2
nese	nést	k5eAaImIp3nS
titulek	titulek	k1gInSc1
The	The	k1gMnSc2
Oracle	Oracl	k1gMnSc2
a	a	k8xC
zahajovalo	zahajovat	k5eAaImAgNnS
svůj	svůj	k3xOyFgInSc4
prodej	prodej	k1gInSc4
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
prodala	prodat	k5eAaPmAgFnS
117	#num#	k4
000	#num#	k4
nosičů	nosič	k1gInPc2
v	v	k7c6
otevíracím	otevírací	k2eAgInSc6d1
týdnu	týden	k1gInSc6
a	a	k8xC
debutovala	debutovat	k5eAaBmAgFnS
tak	tak	k9
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Billboard	billboard	k1gInSc1
200	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
The	The	k1gFnSc1
Oracle	Oracle	k1gFnSc1
je	být	k5eAaImIp3nS
členy	člen	k1gInPc4
Godsmack	Godsmacka	k1gFnPc2
popisována	popisován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
velmi	velmi	k6eAd1
silná	silný	k2eAgFnSc1d1
<g/>
,	,	kIx,
úderná	úderný	k2eAgFnSc1d1
nahrávka	nahrávka	k1gFnSc1
a	a	k8xC
sám	sám	k3xTgMnSc1
Erna	Erna	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
naše	náš	k3xOp1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
nejagresivnější	agresivní	k2eAgInSc4d3
album	album	k1gNnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
singl	singl	k1gInSc1
z	z	k7c2
desky	deska	k1gFnSc2
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
„	„	k?
<g/>
Crying	Crying	k1gInSc1
Like	Like	k1gFnPc2
A	a	k8xC
Bitch	Bitcha	k1gFnPc2
<g/>
"	"	kIx"
a	a	k8xC
stejně	stejně	k6eAd1
jako	jako	k9
předešlé	předešlý	k2eAgFnPc1d1
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gInSc5
<g/>
"	"	kIx"
nebo	nebo	k8xC
„	„	k?
<g/>
Speak	Speak	k1gInSc1
<g/>
"	"	kIx"
si	se	k3xPyFc3
vydobyl	vydobýt	k5eAaPmAgMnS
náležitý	náležitý	k2eAgInSc4d1
respekt	respekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
třetí	třetí	k4xOgInSc1
počin	počin	k1gInSc1
od	od	k7c2
kapely	kapela	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
prorazil	prorazit	k5eAaPmAgMnS
do	do	k7c2
Billboard	billboard	k1gInSc1
Hot	hot	k0
100	#num#	k4
(	(	kIx(
<g/>
74	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hudební	hudební	k2eAgInSc1d1
styl	styl	k1gInSc1
a	a	k8xC
vlivy	vliv	k1gInPc1
</s>
<s>
Sully	Sulla	k1gFnPc1
Erna	Ernum	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
</s>
<s>
Kapelu	kapela	k1gFnSc4
po	po	k7c6
hudební	hudební	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
ovlivnili	ovlivnit	k5eAaPmAgMnP
hlavně	hlavně	k9
Alice	Alice	k1gFnSc2
in	in	k?
Chains	Chains	k1gInSc1
<g/>
,	,	kIx,
Black	Black	k1gMnSc1
Sabbath	Sabbath	k1gMnSc1
<g/>
,	,	kIx,
Led	led	k1gInSc1
Zeppelin	Zeppelin	k2eAgInSc1d1
<g/>
,	,	kIx,
Aerosmith	Aerosmith	k1gMnSc1
<g/>
,	,	kIx,
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
<g/>
,	,	kIx,
Pantera	panter	k1gMnSc4
<g/>
,	,	kIx,
Metallica	Metallicus	k1gMnSc4
a	a	k8xC
Rush	Rush	k1gInSc4
(	(	kIx(
<g/>
souhlasně	souhlasně	k6eAd1
to	ten	k3xDgNnSc1
tvrdí	tvrdit	k5eAaImIp3nS
Erna	Erna	k1gFnSc1
<g/>
,	,	kIx,
Larkin	Larkin	k1gInSc1
a	a	k8xC
Rombola	Rombola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Erna	Erna	k1gMnSc1
označil	označit	k5eAaPmAgMnS
Laynea	Layne	k2eAgMnSc4d1
Staleyho	Staley	k1gMnSc4
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
přední	přední	k2eAgInSc4d1
hudební	hudební	k2eAgInSc4d1
vzor	vzor	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Web	web	k1gInSc1
Subvulture	Subvultur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
popisuje	popisovat	k5eAaImIp3nS
první	první	k4xOgNnPc1
dvě	dva	k4xCgNnPc1
alba	album	k1gNnPc1
od	od	k7c2
Godsmack	Godsmacko	k1gNnPc2
jako	jako	k8xC,k8xS
velmi	velmi	k6eAd1
podobná	podobný	k2eAgFnSc1d1
desce	deska	k1gFnSc6
Dirt	Dirt	k1gInSc4
od	od	k7c2
Alice	Alice	k1gFnSc2
in	in	k?
Chains	Chains	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Současnější	současný	k2eAgFnSc1d2
tvorba	tvorba	k1gFnSc1
se	se	k3xPyFc4
již	již	k6eAd1
vzdaluje	vzdalovat	k5eAaImIp3nS
produkci	produkce	k1gFnSc4
Alice	Alice	k1gFnSc2
in	in	k?
Chains	Chains	k1gInSc1
a	a	k8xC
sám	sám	k3xTgMnSc1
zpěvák	zpěvák	k1gMnSc1
Erna	Erna	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nikdy	nikdy	k6eAd1
jsem	být	k5eAaImIp1nS
je	být	k5eAaImIp3nS
v	v	k7c6
naší	náš	k3xOp1gFnSc6
hudbě	hudba	k1gFnSc6
neslyšel	slyšet	k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Server	server	k1gInSc1
Alternative	Alternativ	k1gInSc5
Press	Press	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
skupinu	skupina	k1gFnSc4
jako	jako	k8xC,k8xS
„	„	k?
<g/>
vřející	vřející	k1gFnSc1
<g/>
,	,	kIx,
silně	silně	k6eAd1
kytarový	kytarový	k2eAgInSc4d1
hybrid	hybrid	k1gInSc4
všeho	všecek	k3xTgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
je	být	k5eAaImIp3nS
těžké	těžký	k2eAgNnSc4d1
<g/>
,	,	kIx,
minulé	minulý	k2eAgNnSc4d1
a	a	k8xC
současné	současný	k2eAgNnSc4d1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Katherine	Katherin	k1gInSc5
Turmanová	Turmanová	k1gFnSc1
z	z	k7c2
Amazon	amazona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
označila	označit	k5eAaPmAgFnS
muziku	muzika	k1gFnSc4
Godsmack	Godsmacka	k1gFnPc2
za	za	k7c4
„	„	k?
<g/>
temnou	temný	k2eAgFnSc4d1
<g/>
,	,	kIx,
vířející	vířející	k2eAgFnSc4d1
a	a	k8xC
impozantní	impozantní	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zpěv	zpěv	k1gInSc1
Sullyho	Sully	k1gMnSc2
Erny	Erna	k1gMnSc2
je	být	k5eAaImIp3nS
přirovnáván	přirovnávat	k5eAaImNgInS
k	k	k7c3
hrdelně	hrdelně	k6eAd1
zlověstným	zlověstný	k2eAgInPc3d1
vokálům	vokál	k1gInPc3
Laynea	Layne	k1gInSc2
Stalyho	Stalyho	k?
a	a	k8xC
bručícím	bručící	k2eAgInSc7d1
<g/>
,	,	kIx,
metalovým	metalový	k2eAgInSc7d1
growlů	growl	k1gInPc2
Jamese	Jamese	k1gFnSc1
Hetfielda	Hetfielda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Merrillovo	Merrillův	k2eAgNnSc1d1
hraní	hraní	k1gNnSc1
na	na	k7c6
basu	bas	k1gInSc6
se	se	k3xPyFc4
popisuje	popisovat	k5eAaImIp3nS
jako	jako	k9
silné	silný	k2eAgNnSc1d1
a	a	k8xC
propracované	propracovaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Tony	Tony	k1gFnSc1
Rombola	Rombola	k1gFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
chválen	chválen	k2eAgInSc1d1
za	za	k7c4
svá	svůj	k3xOyFgNnPc4
osobitá	osobitý	k2eAgNnPc4d1
kytarová	kytarový	k2eAgNnPc4d1
sóla	sólo	k1gNnPc4
a	a	k8xC
bubnování	bubnování	k1gNnSc4
Shannona	Shannon	k1gMnSc2
Larkina	Larkin	k1gMnSc2
je	být	k5eAaImIp3nS
přirovnáváno	přirovnávat	k5eAaImNgNnS
k	k	k7c3
práci	práce	k1gFnSc3
Neila	Neil	k1gMnSc2
Pearta	Peart	k1gMnSc2
a	a	k8xC
Johnyho	Johny	k1gMnSc2
Bonhama	Bonham	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členové	člen	k1gMnPc1
kapely	kapela	k1gFnSc2
</s>
<s>
Současní	současný	k2eAgMnPc1d1
</s>
<s>
Sully	Sull	k1gInPc1
Erna	Ern	k1gInSc2
<g/>
:	:	kIx,
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
rytmická	rytmický	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
klávesy	klávesa	k1gFnPc1
<g/>
,	,	kIx,
bicí	bicí	k2eAgFnPc1d1
<g/>
,	,	kIx,
harmonika	harmonika	k1gFnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Tony	Tony	k1gFnSc1
Rombola	Rombola	k1gFnSc1
<g/>
:	:	kIx,
vedoucí	vedoucí	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
vokály	vokál	k1gInPc1
v	v	k7c6
pozadí	pozadí	k1gNnSc6
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Robbie	Robbie	k1gFnSc1
Merrill	Merrilla	k1gFnPc2
<g/>
:	:	kIx,
basa	basa	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
<g/>
:	:	kIx,
bicí	bicí	k2eAgMnSc1d1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
Erna	Erna	k1gMnSc1
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
bicí	bicí	k2eAgNnSc4d1
v	v	k7c6
albu	album	k1gNnSc6
Godsmack	Godsmacka	k1gFnPc2
a	a	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
příležitostně	příležitostně	k6eAd1
bubnuje	bubnovat	k5eAaImIp3nS
při	při	k7c6
živých	živý	k2eAgNnPc6d1
vystoupeních	vystoupení	k1gNnPc6
společně	společně	k6eAd1
s	s	k7c7
Larkinem	Larkin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1
</s>
<s>
Lee	Lea	k1gFnSc3
Richards	Richards	k1gInSc1
<g/>
:	:	kIx,
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Joe	Joe	k?
Darco	Darco	k1gNnSc1
<g/>
:	:	kIx,
bicí	bicí	k2eAgFnSc1d1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tommy	Tomma	k1gFnSc2
Stewart	Stewart	k1gMnSc1
<g/>
:	:	kIx,
bicí	bicí	k2eAgMnSc1d1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nominace	nominace	k1gFnSc1
Grammy	Gramma	k1gFnSc2
</s>
<s>
Grammy	Gramma	k1gFnPc1
Awards	Awardsa	k1gFnPc2
</s>
<s>
Godsmack	Godsmack	k6eAd1
byli	být	k5eAaImAgMnP
nominováni	nominovat	k5eAaBmNgMnP
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
ceny	cena	k1gFnPc4
Grammy	Gramma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Píseň	píseň	k1gFnSc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
„	„	k?
<g/>
Vampires	Vampires	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Best	Best	k2eAgInSc1d1
Rock	rock	k1gInSc1
Instrumental	Instrumental	k1gMnSc1
Performance	performance	k1gFnSc1
</s>
<s>
Nominace	nominace	k1gFnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gMnSc5
<g/>
"	"	kIx"
</s>
<s>
Best	Best	k2eAgInSc1d1
Rock	rock	k1gInSc1
Song	song	k1gInSc1
</s>
<s>
Nominace	nominace	k1gFnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gInSc5
<g/>
"	"	kIx"
</s>
<s>
Best	Best	k2eAgInSc1d1
Hard	Hard	k1gInSc1
Rock	rock	k1gInSc1
Performance	performance	k1gFnSc1
</s>
<s>
Nominace	nominace	k1gFnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
„	„	k?
<g/>
Straight	Straight	k1gInSc1
Out	Out	k1gFnSc1
of	of	k?
Line	linout	k5eAaImIp3nS
<g/>
"	"	kIx"
</s>
<s>
Best	Best	k2eAgInSc1d1
Hard	Hard	k1gInSc1
Rock	rock	k1gInSc1
Performance	performance	k1gFnSc1
</s>
<s>
Nominace	nominace	k1gFnSc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
alba	album	k1gNnSc2
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
hitparádách	hitparáda	k1gFnPc6
</s>
<s>
Certifikace	certifikace	k1gFnSc1
</s>
<s>
U.	U.	kA
<g/>
S.	S.	kA
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
AUS	AUS	kA
</s>
<s>
CAN	CAN	kA
</s>
<s>
SWI	SWI	kA
</s>
<s>
GER	Gera	k1gFnPc2
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
AUT	aut	k1gInSc1
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NLD	NLD	kA
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NZ	NZ	kA
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
GR	GR	kA
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1998	#num#	k4
</s>
<s>
Godsmack	Godsmack	k6eAd1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1998	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
<g/>
,	,	kIx,
CS	CS	kA
(	(	kIx(
<g/>
53190	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
4	#num#	k4
<g/>
×	×	k?
Platinová	platinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
CAN	CAN	kA
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
</s>
<s>
Awake	Awake	k6eAd1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2000	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
(	(	kIx(
<g/>
159688	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
9	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
59	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
38	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
2	#num#	k4
<g/>
×	×	k?
Platinová	platinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
CAN	CAN	kA
<g/>
:	:	kIx,
Platinová	platinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2003	#num#	k4
</s>
<s>
Faceless	Faceless	k6eAd1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2003	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
67854	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
9	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
70	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
98	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
Platinová	platinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
CAN	CAN	kA
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
</s>
<s>
IV	IV	kA
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
(	(	kIx(
<g/>
654802	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
65	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
56	#num#	k4
</s>
<s>
65	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
CAN	CAN	kA
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
</s>
<s>
The	The	k?
Oracle	Oracle	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
<g/>
,	,	kIx,
Digital	Digital	kA
Download	Download	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
2	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
72	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
11	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
Live	Live	k1gFnSc1
and	and	k?
Inspired	Inspired	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
2012	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
When	When	k1gInSc1
legends	legends	k6eAd1
rise	risat	k5eAaPmIp3nS
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
2018	#num#	k4
</s>
<s>
„	„	k?
<g/>
—	—	k?
<g/>
"	"	kIx"
značí	značit	k5eAaImIp3nS
album	album	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
neumístilo	umístit	k5eNaPmAgNnS
v	v	k7c6
dané	daný	k2eAgFnSc6d1
hitparádě	hitparáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
EP	EP	kA
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
alba	album	k1gNnSc2
</s>
<s>
Umístění	umístění	k1gNnSc1
vhitparádách	vhitparáda	k1gFnPc6
</s>
<s>
Certifikace	certifikace	k1gFnSc1
</s>
<s>
US	US	kA
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
CAN	CAN	kA
</s>
<s>
2004	#num#	k4
</s>
<s>
The	The	k?
Other	Other	k1gInSc1
Side	Side	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2004	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
240136	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kompilace	kompilace	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
alba	album	k1gNnSc2
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c4
Billboard	billboard	k1gInSc4
200	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2007	#num#	k4
</s>
<s>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
,	,	kIx,
Bad	Bad	k1gMnSc1
Times	Times	k1gInSc1
<g/>
...	...	k?
Ten	ten	k3xDgInSc1
Years	Years	k1gInSc1
of	of	k?
Godsmack	Godsmack	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1029600	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
35	#num#	k4
</s>
<s>
Dema	Dema	k6eAd1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
dema	dema	k6eAd1
</s>
<s>
1997	#num#	k4
</s>
<s>
All	All	k?
Wound	Wound	k1gMnSc1
Up	Up	k1gMnSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1997	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
EK	EK	kA
Records	Records	k1gInSc1
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Singl	singl	k1gInSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
hitparádách	hitparáda	k1gFnPc6
</s>
<s>
RIAAcert	RIAAcert	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
US	US	kA
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USMain	USMain	k1gInSc1
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USMod	USMod	k1gInSc1
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USRock	USRock	k1gInSc1
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1998	#num#	k4
</s>
<s>
„	„	k?
<g/>
Whatever	Whatever	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
116	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
Godsmack	Godsmack	k6eAd1
</s>
<s>
1999	#num#	k4
</s>
<s>
„	„	k?
<g/>
Keep	Keep	k1gInSc1
Away	Awaa	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
5	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
„	„	k?
<g/>
Voodoo	Voodoo	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
102	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
2000	#num#	k4
</s>
<s>
„	„	k?
<g/>
Bad	Bad	k1gFnSc1
Religion	religion	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
8	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
„	„	k?
<g/>
Awake	Awake	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
101	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
Awake	Awake	k6eAd1
</s>
<s>
2001	#num#	k4
</s>
<s>
„	„	k?
<g/>
Greed	Greed	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
123	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
„	„	k?
<g/>
Bad	Bad	k1gMnSc1
Magick	Magick	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
12	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
2002	#num#	k4
</s>
<s>
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gInSc5
<g/>
"	"	kIx"
</s>
<s>
103	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
</s>
<s>
Faceless	Faceless	k6eAd1
</s>
<s>
2003	#num#	k4
</s>
<s>
„	„	k?
<g/>
Straight	Straight	k1gInSc1
Out	Out	k1gFnSc1
of	of	k?
Line	linout	k5eAaImIp3nS
<g/>
"	"	kIx"
</s>
<s>
73	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
„	„	k?
<g/>
Serenity	Serenita	k1gFnPc1
<g/>
"	"	kIx"
</s>
<s>
113	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
„	„	k?
<g/>
Re-Align	Re-Align	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
3	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
2004	#num#	k4
</s>
<s>
„	„	k?
<g/>
Running	Running	k1gInSc1
Blind	Blind	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
123	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
The	The	k?
Other	Other	k1gInSc1
Side	Side	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Touché	Touchý	k2eAgFnPc4d1
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
7	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
2006	#num#	k4
</s>
<s>
„	„	k?
<g/>
Speak	Speak	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
85	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
IV	IV	kA
</s>
<s>
„	„	k?
<g/>
Shine	Shin	k1gMnSc5
Down	Down	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
4	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
„	„	k?
<g/>
The	The	k1gFnSc1
Enemy	Enema	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
4	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
2007	#num#	k4
</s>
<s>
„	„	k?
<g/>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
Bad	Bad	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
124	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
,	,	kIx,
Bad	Bad	k1gMnSc1
Times	Times	k1gInSc1
<g/>
...	...	k?
Ten	ten	k3xDgInSc1
Years	Years	k1gInSc1
of	of	k?
Godsmack	Godsmack	k1gInSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
„	„	k?
<g/>
Whiskey	Whiskey	k1gInPc1
Hangover	Hangover	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
102	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
The	The	k?
Oracle	Oracle	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
„	„	k?
<g/>
Cryin	Cryin	k1gInSc1
<g/>
'	'	kIx"
Like	Like	k1gInSc1
A	a	k8xC
Bitch	Bitch	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
74	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
„	„	k?
<g/>
Love-Hate-Sex-Pain	Love-Hate-Sex-Pain	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
—	—	k?
</s>
<s>
2	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
„	„	k?
<g/>
—	—	k?
<g/>
"	"	kIx"
značí	značit	k5eAaImIp3nP
neumisťující	umisťující	k2eNgFnPc1d1
se	se	k3xPyFc4
písně	píseň	k1gFnPc1
v	v	k7c6
dané	daný	k2eAgFnSc6d1
hitparádě	hitparáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Videoklipy	videoklip	k1gInPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Režisér	režisér	k1gMnSc1
</s>
<s>
1999	#num#	k4
</s>
<s>
„	„	k?
<g/>
Whatever	Whatever	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Michael	Michael	k1gMnSc1
Alperowitz	Alperowitz	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Keep	Keep	k1gInSc1
Away	Awaa	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
Peter	Peter	k1gMnSc1
Christopherson	Christopherson	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Voodoo	Voodoo	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
Dean	Dean	k1gMnSc1
Karr	Karr	k1gMnSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
„	„	k?
<g/>
Awake	Awake	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
Troy	Troa	k1gMnSc2
Smith	Smith	k1gMnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
„	„	k?
<g/>
Greed	Greed	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Troy	Troa	k1gMnSc2
Smith	Smith	k1gMnSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gInSc5
<g/>
"	"	kIx"
</s>
<s>
The	The	k?
Brothers	Brothers	k1gInSc1
Strause	Strause	k1gFnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
„	„	k?
<g/>
Straight	Straight	k1gInSc1
Out	Out	k1gFnSc1
of	of	k?
Line	linout	k5eAaImIp3nS
<g/>
"	"	kIx"
</s>
<s>
Dean	Dean	k1gMnSc1
Karr	Karr	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Serenity	Serenita	k1gFnPc1
<g/>
"	"	kIx"
</s>
<s>
Sully	Sulla	k1gFnPc1
Erna	Ernum	k1gNnSc2
</s>
<s>
2006	#num#	k4
</s>
<s>
„	„	k?
<g/>
Speak	Speak	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Wayne	Waynout	k5eAaImIp3nS,k5eAaPmIp3nS
Isham	Isham	k1gInSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
„	„	k?
<g/>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
Bad	Bad	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
Rocky	rock	k1gInPc1
Schenck	Schencka	k1gFnPc2
</s>
<s>
2010	#num#	k4
</s>
<s>
„	„	k?
<g/>
Cryin	Cryin	k1gInSc1
<g/>
'	'	kIx"
Like	Like	k1gInSc1
A	a	k8xC
Bitch	Bitch	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Paul	Paul	k1gMnSc1
Harb	Harb	k1gMnSc1
</s>
<s>
Video	video	k1gNnSc1
alba	album	k1gNnSc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
alba	album	k1gNnSc2
</s>
<s>
Certifikace	certifikace	k1gFnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
Godsmack	Godsmack	k1gInSc1
Live	Liv	k1gFnSc2
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
DVD	DVD	kA
(	(	kIx(
<g/>
1373	#num#	k4
<g/>
CM	cm	kA
<g/>
)	)	kIx)
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2002	#num#	k4
</s>
<s>
Smack	Smack	k6eAd1
This	This	k1gInSc1
<g/>
!	!	kIx.
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2002	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
DVD	DVD	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
16619	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
</s>
<s>
Changes	Changes	k1gMnSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Republic	Republice	k1gFnPc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
DVD	DVD	kA
(	(	kIx(
<g/>
461073	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
CAN	CAN	kA
<g/>
:	:	kIx,
Platinová	platinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Godsmack	Godsmacka	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Godsmack-lístky	Godsmack-lístka	k1gFnPc4
&	&	k?
biografie	biografie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
30	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://musicserver.cz/clanek/30087/Pohled-do-hitparad-Billboardu-platnych-od-13-kvetna/	http://musicserver.cz/clanek/30087/Pohled-do-hitparad-Billboardu-platnych-od-13-kvetna/	k4
<g/>
↑	↑	k?
Godsmack	Godsmacka	k1gFnPc2
vydají	vydat	k5eAaPmIp3nP
své	svůj	k3xOyFgFnPc4
páté	pátá	k1gFnPc4
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
The	The	k1gFnSc1
Oracle	Oracle	k1gNnSc1
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marketwire	Marketwir	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2010-03-29	2010-03-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rockeři	rocker	k1gMnPc1
Godsmack	Godsmacka	k1gFnPc2
opět	opět	k6eAd1
získali	získat	k5eAaPmAgMnP
#	#	kIx~
<g/>
1	#num#	k4
se	s	k7c7
singlem	singl	k1gInSc7
'	'	kIx"
<g/>
Whiskey	Whiskey	k1gInPc1
Hangover	Hangovra	k1gFnPc2
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009-09-14	2009-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Recenze	recenze	k1gFnSc1
The	The	k1gFnSc1
Oracle	Oracle	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
thenewreview	thenewreview	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sputnikmusic	Sputnikmusice	k1gFnPc2
o	o	k7c4
Godsmack	Godsmack	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sputnikmusuc	Sputnikmusuc	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
iTunes	iTunes	k1gInSc1
The	The	k1gMnSc2
Oracle	Oracl	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iTunes	iTunes	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Biografie	biografie	k1gFnSc1
Godsmack	Godsmacka	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vítězslav	Vítězslav	k1gMnSc1
Štefl	Štefl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tony	Tony	k1gFnSc1
Rombola	Rombola	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzikus	muzikus	k1gMnSc1
<g/>
,	,	kIx,
March	March	k1gMnSc1
4	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Interviewů	interview	k1gInPc2
se	se	k3xPyFc4
Sullym	Sullym	k1gInSc1
Ernou	Erná	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyrock	Nyrock	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
October	October	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Seaver	Seaver	k1gInSc1
<g/>
,	,	kIx,
Morley	Morley	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
z	z	k7c2
Godsmack	Godsmack	k1gMnSc1
v	v	k7c6
interviewů	interview	k1gInPc2
pro	pro	k7c4
MorleyView	MorleyView	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MorleyView	MorleyView	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biografie	biografie	k1gFnSc2
Godsmack	Godsmack	k1gMnSc1
–	–	k?
Barrystickets	Barrystickets	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
barrystickets	barrystickets	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sully	Sulla	k1gMnSc2
Erna	Ernus	k1gMnSc2
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
změně	změna	k1gFnSc6
bubeníka	bubeník	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blabbermouth	Blabbermouth	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
August	August	k1gMnSc1
13	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
21	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BLANFORD	BLANFORD	kA
<g/>
,	,	kIx,
Roxanne	Roxann	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recenze	recenze	k1gFnSc1
Godsmack	Godsmacka	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Godsmack	Godsmack	k1gMnSc1
–	–	k?
historie	historie	k1gFnSc2
hitparád	hitparáda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allmusic	Allmusice	k1gInPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ANKENY	ANKENY	kA
<g/>
,	,	kIx,
Jason	Jason	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gMnSc1
–	–	k?
Biografie	biografie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Allmusic	Allmusic	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
RIAA	RIAA	kA
databáze	databáze	k1gFnSc2
<g/>
–	–	k?
<g/>
Zlato	zlato	k1gNnSc1
a	a	k8xC
Platina	platina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recording	Recording	k1gInSc4
Industry	Industra	k1gFnSc2
Association	Association	k1gInSc1
of	of	k?
America	America	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
SCHWALBOSKI	SCHWALBOSKI	kA
<g/>
,	,	kIx,
Ann	Ann	k1gMnSc1
M.	M.	kA
Texty	text	k1gInPc1
a	a	k8xC
biografie	biografie	k1gFnPc1
Godsmack	Godsmack	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musician	Musiciany	k1gInPc2
guide	guide	k6eAd1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
<g/>
–	–	k?
<g/>
Artist	Artist	k1gInSc1
chart	charta	k1gFnPc2
history	histor	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Historie	historie	k1gFnSc1
hitparád	hitparáda	k1gFnPc2
Godsmack	Godsmack	k1gInSc4
–	–	k?
Singly	singl	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
44	#num#	k4
<g/>
.	.	kIx.
udílení	udílení	k1gNnSc1
cen	cena	k1gFnPc2
Grammy	Gramma	k1gFnSc2
–	–	k?
2002	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rock	rock	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Net	Net	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
–	–	k?
Turné	turné	k1gNnSc1
2001	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mitch	Mitch	k1gInSc1
Sanization	Sanization	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Frontman	Frontman	k1gMnSc1
Godsmack	Godsmack	k1gMnSc1
brání	bránit	k5eAaImIp3nS
kapelu	kapela	k1gFnSc4
před	před	k7c7
zaškatulkování	zaškatulkování	k?
do	do	k7c2
pro-militaristů	pro-militarista	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arthur	Arthur	k1gMnSc1
Magazine	Magazin	k1gInSc5
<g/>
,	,	kIx,
May	May	k1gMnSc1
4	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
19	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Mluvení	mluvení	k1gNnSc1
z	z	k7c2
Godsmack	Godsmacka	k1gFnPc2
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
k	k	k7c3
čemu	co	k3yRnSc3,k3yQnSc3,k3yInSc3
je	být	k5eAaImIp3nS
používána	používán	k2eAgFnSc1d1
jejich	jejich	k3xOp3gFnSc1
hudba	hudba	k1gFnSc1
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ArthurMag	ArthurMag	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
May	May	k1gMnSc1
6	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FARINELLA	FARINELLA	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modern	Modern	k1gMnSc1
drummer	drummer	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Faceless	Faceless	k1gInSc1
<g/>
'	'	kIx"
debutuje	debutovat	k5eAaBmIp3nS
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blabbermouth	Blabbermouth	k1gInSc1
<g/>
.	.	kIx.
<g/>
Net	Net	k1gFnSc1
<g/>
,	,	kIx,
April	April	k1gInSc1
16	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
23	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vítězové	vítěz	k1gMnPc1
cen	cena	k1gFnPc2
Grammy	Gramma	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grammy	Gramma	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WIEDERHORN	WIEDERHORN	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc1
<g/>
:	:	kIx,
Neuhlazení	neuhlazení	k1gNnSc1
<g/>
,	,	kIx,
nekompromisní	kompromisní	k2eNgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MTV	MTV	kA
<g/>
,	,	kIx,
April	April	k1gInSc1
18	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Metal	metat	k5eAaImAgMnS
Edge	Edge	k1gInSc4
Magazine	Magazin	k1gInSc5
–	–	k?
„	„	k?
<g/>
První	první	k4xOgFnSc1
skvělá	skvělý	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
nové	nový	k2eAgFnSc2d1
milénia	milénium	k1gNnPc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metal	metal	k1gInSc1
Edge	Edge	k1gInSc4
<g/>
,	,	kIx,
April	April	k1gInSc4
3	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Archiv	archiv	k1gInSc1
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allmusic	Allmusice	k1gInPc2
<g/>
,	,	kIx,
2004-03-24	2004-03-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WIEDERHORN	WIEDERHORN	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ex-kytarista	Ex-kytarista	k1gMnSc1
Godsmack	Godsmack	k1gMnSc1
našel	najít	k5eAaPmAgMnS
druhé	druhý	k4xOgNnSc4
uplatnění	uplatnění	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MTV	MTV	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
June	jun	k1gMnSc5
3	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Diskografie	diskografie	k1gFnSc1
–	–	k?
The	The	k1gFnSc1
Other	Other	k1gMnSc1
Side	Side	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gMnSc1
–	–	k?
Discography	Discographa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WEIDERHORN	WEIDERHORN	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nová	k1gFnSc2
EP	EP	kA
Godsmack	Godsmack	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MTV	MTV	kA
<g/>
,	,	kIx,
October	October	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Ultimate	Ultimat	k1gInSc5
Guitar	Guitar	k1gMnSc1
–	–	k?
akustické	akustický	k2eAgNnSc1d1
album	album	k1gNnSc4
Godsmack	Godsmacka	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MTV	MTV	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
October	October	k1gInSc1
13	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Metallica	Metallica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metallica	Metallica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
Metallica	Metallica	k1gMnSc1
tour	tour	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gMnSc1
–	–	k?
Tour	Tour	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gMnSc1
tour	tour	k1gMnSc1
'	'	kIx"
<g/>
0	#num#	k4
<g/>
7	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FUOCO	FUOCO	kA
<g/>
,	,	kIx,
Christina	Christina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interviewů	interview	k1gInPc2
<g/>
:	:	kIx,
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
z	z	k7c2
Godsmack	Godsmack	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gInSc1
News	News	k1gInSc4
<g/>
,	,	kIx,
June	jun	k1gMnSc5
23	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
"	"	kIx"
<g/>
IV	IV	kA
<g/>
"	"	kIx"
Debuts	Debuts	k1gInSc1
At	At	k1gFnSc2
#	#	kIx~
<g/>
1	#num#	k4
On	on	k3xPp3gInSc1
Billboard	billboard	k1gInSc1
200	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MetalUnderground	MetalUnderground	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2006-05-03	2006-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
MetalUnderground	MetalUnderground	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BOWAR	BOWAR	kA
<g/>
,	,	kIx,
Chad	Chad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konverzace	konverzace	k1gFnSc1
s	s	k7c7
bubeníka	bubeník	k1gMnSc2
Shannonem	Shannon	k1gMnSc7
Larkinem	Larkin	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heavy	Heavo	k1gNnPc7
metal	metat	k5eAaImAgMnS
about	about	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
‘	‘	k?
<g/>
Livin	Livin	k1gInSc1
in	in	k?
Sin	sin	kA
<g/>
'	'	kIx"
s	s	k7c7
Shannonem	Shannon	k1gMnSc7
Larkinem	Larkin	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Live-Metal	Live-Metal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Net	Net	k1gMnSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
6	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JENNY	JENNY	kA
<g/>
,	,	kIx,
Feniak	Feniak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudba	hudba	k1gFnSc1
a	a	k8xC
magie	magie	k1gFnSc1
Godsmack	Godsmacka	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edmunton	Edmunton	k1gInSc1
sun	sun	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
–	–	k?
'	'	kIx"
<g/>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
Bad	Bad	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
'	'	kIx"
Prodeje	prodej	k1gInSc2
v	v	k7c6
prvním	první	k4xOgInSc6
týdnu	týden	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blabbermouth	Blabbermouth	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
15	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Blabbermouth	Blabbermoutha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
net	net	k?
–	–	k?
Godsmack	Godsmacka	k1gFnPc2
se	se	k3xPyFc4
ohlédnou	ohlédnout	k5eAaPmIp3nP
za	za	k7c7
starými	starý	k2eAgInPc7d1
časy	čas	k1gInPc7
na	na	k7c4
'	'	kIx"
<g/>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
,	,	kIx,
Bad	Bad	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
'	'	kIx"
v	v	k7c6
listopadu	listopad	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blabbermouth	Blabbermouth	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
October	October	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Nové	Nové	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
o	o	k7c4
Godsmack	Godsmack	k1gInSc4
z	z	k7c2
úst	ústa	k1gNnPc2
Shannona	Shannon	k1gMnSc2
Larkina	Larkin	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Rock	rock	k1gInSc1
Pit	pit	k2eAgInSc1d1
uvádí	uvádět	k5eAaImIp3nS
Godsmack	Godsmack	k1gInSc1
-	-	kIx~
Tvoření	tvoření	k1gNnSc1
nového	nový	k2eAgNnSc2d1
studiového	studiový	k2eAgNnSc2d1
alba	album	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
RockPit	RockPit	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
Oracle	Oracle	k1gFnSc1
-	-	kIx~
Godsmack	Godsmack	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.godsmack.com	www.godsmack.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Cruz	Cruz	k1gInSc1
zakotvil	zakotvit	k5eAaPmAgInS
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
Vzestup	vzestup	k1gInSc4
Rihanny	Rihanna	k1gFnSc2
na	na	k7c4
Hot	hot	k0
100	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2010-03-11	2010-03-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lidé	člověk	k1gMnPc1
z	z	k7c2
New	New	k1gFnSc2
Hampshire	Hampshir	k1gInSc5
–	–	k?
Sully	Sulla	k1gMnSc2
Erna	Ernus	k1gMnSc2
<g/>
/	/	kIx~
<g/>
Godsmack	Godsmack	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
People	People	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
D	D	kA
<g/>
'	'	kIx"
<g/>
ANGELO	Angela	k1gFnSc5
<g/>
;	;	kIx,
VINEYARD	VINEYARD	kA
<g/>
;	;	kIx,
WIEDERHORN	WIEDERHORN	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
;	;	kIx,
Jennifer	Jennifer	k1gMnSc1
<g/>
;	;	kIx,
Jon	Jon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MTV	MTV	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
"	"	kIx"
<g/>
'	'	kIx"
<g/>
Přinutil	přinutit	k5eAaPmAgMnS
mě	já	k3xPp1nSc4
začít	začít	k5eAaPmF
zpívat	zpívat	k5eAaImF
<g/>
'	'	kIx"
<g/>
:	:	kIx,
Umělci	umělec	k1gMnPc1
vzpomínají	vzpomínat	k5eAaImIp3nP
na	na	k7c4
Laynea	Layneus	k1gMnSc4
Staleyho	Staley	k1gMnSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MTV	MTV	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
April	April	k1gInSc1
22	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Subvulture	Subvultur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
Godsmack	Godsmack	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Subvulture	Subvultur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ashare	Ashar	k1gMnSc5
<g/>
,	,	kIx,
Matt	Matt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sully	Sulla	k1gMnSc2
Erna	Ernus	k1gMnSc2
o	o	k7c6
The	The	k1gFnSc6
Other	Othra	k1gFnPc2
Side	Sid	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portlandphoenix	Portlandphoenix	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Turman	Turman	k1gMnSc1
<g/>
,	,	kIx,
Katherine	Katherin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkové	celkový	k2eAgInPc1d1
ohlédnutí	ohlédnutí	k1gNnSc3
za	za	k7c4
Godsmack	Godsmack	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amazon	amazona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Begrand	Begranda	k1gFnPc2
<g/>
,	,	kIx,
Adrien	Adriena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
Other	Other	k1gMnSc1
Side	Side	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popmatters	Popmatters	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
March	March	k1gInSc1
16	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Kot	kot	k1gMnSc1
<g/>
,	,	kIx,
Greg	Greg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Godsmack	Godsmack	k1gInSc1
Awake	Awak	k1gFnSc2
<g/>
,	,	kIx,
recenze	recenze	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
<g/>
,	,	kIx,
November	November	k1gInSc1
9	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gMnSc1
–	–	k?
Ken	Ken	k1gMnSc1
Philips	Philips	kA
Publicity	publicita	k1gFnPc1
Group	Group	k1gMnSc1
–	–	k?
KenPhillipsGroup	KenPhillipsGroup	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
www.kenphillipsgroup.com	www.kenphillipsgroup.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Godsmack	Godsmack	k1gInSc1
<g/>
–	–	k?
<g/>
Artist	Artist	k1gInSc1
chart	charta	k1gFnPc2
history	histor	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
allmusic	allmusic	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
musicline	musiclin	k1gInSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Rakouský	rakouský	k2eAgInSc1d1
portál	portál	k1gInSc1
o	o	k7c6
hitparádách	hitparáda	k1gFnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrian	Austrian	k1gInSc1
charts	charts	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Diskografie	diskografie	k1gFnSc1
Godsmack	Godsmack	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dutch	Dutch	k1gInSc1
charts	charts	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
nizozemština	nizozemština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Diskografie	diskografie	k1gFnSc1
Godsmack	Godsmack	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charts	Charts	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
nz	nz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Diskografie	diskografie	k1gFnSc1
Godsmack	Godsmack	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Greece	Greeec	k1gInSc2
Charts	Charts	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RIAA	RIAA	kA
Searchable	Searchable	k1gFnSc2
database	database	k6eAd1
<g/>
–	–	k?
<g/>
Gold	Gold	k1gInSc1
and	and	k?
Platinum	Platinum	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recording	Recording	k1gInSc4
Industry	Industra	k1gFnSc2
Association	Association	k1gInSc1
of	of	k?
America	America	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
CRIA	CRIA	kA
databáze	databáze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canadian	Canadian	k1gInSc1
Recording	Recording	k1gInSc4
Industry	Industra	k1gFnSc2
Association	Association	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bubbling	Bubbling	k1gInSc1
Under	Under	k1gInSc1
Hot	hot	k0
100	#num#	k4
Singles	Singles	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Godsmack	Godsmack	k1gMnSc1
Sully	Sulla	k1gFnSc2
Erna	Erna	k1gMnSc1
•	•	k?
Tony	Tony	k1gFnSc1
Rombola	Rombola	k1gFnSc1
•	•	k?
Robbie	Robbie	k1gFnSc2
Merrill	Merrill	k1gMnSc1
•	•	k?
Shannon	Shannon	k1gMnSc1
Larkin	Larkin	k1gMnSc1
Tommy	Tomma	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
Lee	Lea	k1gFnSc3
Richards	Richardsa	k1gFnPc2
•	•	k?
Joe	Joe	k1gMnSc1
D	D	kA
<g/>
'	'	kIx"
<g/>
arco	arco	k6eAd1
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Godsmack	Godsmack	k1gInSc1
•	•	k?
Awake	Awake	k1gInSc1
•	•	k?
Faceless	Faceless	k1gInSc1
•	•	k?
IV	IV	kA
•	•	k?
The	The	k1gMnSc1
Oracle	Oracle	k1gFnSc2
•	•	k?
1000	#num#	k4
<g/>
hp	hp	k?
Kompilace	kompilace	k1gFnSc2
</s>
<s>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
,	,	kIx,
Bad	Bad	k1gMnSc1
Times	Times	k1gInSc1
<g/>
...	...	k?
Ten	ten	k3xDgInSc1
Years	Years	k1gInSc1
of	of	k?
Godsmack	Godsmack	k1gMnSc1
Dema	Dema	k1gMnSc1
<g/>
/	/	kIx~
<g/>
EP	EP	kA
</s>
<s>
All	All	k?
Wound	Wound	k1gMnSc1
Up	Up	k1gMnSc1
•	•	k?
The	The	k1gFnSc1
Other	Othero	k1gNnPc2
Side	Sid	k1gFnSc2
Video	video	k1gNnSc1
alba	album	k1gNnSc2
</s>
<s>
Godsmack	Godsmack	k1gMnSc1
Live	Liv	k1gFnSc2
•	•	k?
Smack	Smack	k1gInSc1
This	This	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Changes	Changes	k1gMnSc1
Singly	singl	k1gInPc1
</s>
<s>
„	„	k?
<g/>
Whatever	Whatever	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Keep	Keep	k1gInSc4
Away	Awaa	k1gFnSc2
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Voodoo	Voodoo	k1gMnSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Bad	Bad	k1gFnSc1
Religion	religion	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Awake	Awake	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Bad	Bad	k1gMnSc1
Magick	Magick	k1gMnSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Greed	Greed	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
I	i	k8xC
Stand	Standa	k1gFnPc2
Alone	Alon	k1gInSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Straight	Straight	k1gInSc1
Out	Out	k1gFnSc1
of	of	k?
Line	linout	k5eAaImIp3nS
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Serenity	Serenita	k1gFnSc2
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Re-Align	Re-Align	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Running	Running	k1gInSc1
Blind	Blind	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Touché	Touchý	k2eAgFnPc1d1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Speak	Speak	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Shine	Shin	k1gInSc5
Down	Downa	k1gFnPc2
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
The	The	k1gFnSc2
Enemy	Enema	k1gFnSc2
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
Bad	Bad	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Whiskey	Whiskea	k1gFnSc2
Hangover	Hangovra	k1gFnPc2
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Cryin	Cryin	k1gInSc1
<g/>
'	'	kIx"
Like	Like	k1gInSc1
A	a	k8xC
Bitch	Bitch	k1gInSc1
<g/>
"	"	kIx"
•	•	k?
„	„	k?
<g/>
Love-Hate-Sex-Pain	Love-Hate-Sex-Pain	k1gInSc1
<g/>
"	"	kIx"
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
Godsmack	Godsmacka	k1gFnPc2
•	•	k?
Another	Anothra	k1gFnPc2
Animal	animal	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
osa	osa	k1gFnSc1
<g/>
2014843825	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
10327338-4	10327338-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0943	#num#	k4
8373	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22523	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
142153211	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22523	#num#	k4
</s>
