<s>
Gadolinium	gadolinium	k1gNnSc1	gadolinium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Gd	Gd	k1gFnSc2	Gd
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Gadolinium	gadolinium	k1gNnSc4	gadolinium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc1d1	přechodný
kovový	kovový	k2eAgInSc1d1	kovový
ferromagnetický	ferromagnetický	k2eAgInSc1d1	ferromagnetický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
osmý	osmý	k4xOgMnSc1	osmý
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nP	nacházet
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
počítačových	počítačový	k2eAgFnPc2d1	počítačová
pamětí	paměť	k1gFnPc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Gadolinium	gadolinium	k1gNnSc1	gadolinium
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgInSc1d1	měkký
přechodný	přechodný	k2eAgInSc1d1	přechodný
ferromagnetický	ferromagnetický	k2eAgInSc1d1	ferromagnetický
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
gadolinium	gadolinium	k1gNnSc4	gadolinium
méně	málo	k6eAd2	málo
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
než	než	k8xS	než
předchozí	předchozí	k2eAgInPc4d1	předchozí
prvky	prvek	k1gInPc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
oxidu	oxid	k1gInSc2	oxid
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
gadolinium	gadolinium	k1gNnSc1	gadolinium
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Gd	Gd	k1gFnSc2	Gd
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
podobné	podobný	k2eAgInPc1d1	podobný
sloučeninám	sloučenina	k1gFnPc3	sloučenina
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgInPc1d1	stabilní
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nereagují	reagovat	k5eNaBmIp3nP	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
především	především	k9	především
fluoridy	fluorid	k1gInPc1	fluorid
a	a	k8xC	a
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
nerozpustnost	nerozpustnost	k1gFnSc1	nerozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Gadolinité	Gadolinitý	k2eAgFnPc1d1	Gadolinitý
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Gadolinium	gadolinium	k1gNnSc1	gadolinium
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
lanthanoid	lanthanoid	k1gInSc1	lanthanoid
a	a	k8xC	a
jediný	jediný	k2eAgInSc1d1	jediný
kov	kov	k1gInSc1	kov
mimo	mimo	k7c4	mimo
skupinu	skupina	k1gFnSc4	skupina
kovů	kov	k1gInPc2	kov
triády	triáda	k1gFnSc2	triáda
železa	železo	k1gNnSc2	železo
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
ferromagnetické	ferromagnetický	k2eAgFnPc4d1	ferromagnetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
silně	silně	k6eAd1	silně
přitahován	přitahován	k2eAgInSc4d1	přitahován
magnety	magnet	k1gInPc4	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
vlastnosti	vlastnost	k1gFnSc2	vlastnost
gadolinia	gadolinium	k1gNnSc2	gadolinium
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
účinný	účinný	k2eAgInSc1d1	účinný
průřez	průřez	k1gInSc1	průřez
pro	pro	k7c4	pro
záchyt	záchyt	k1gInSc4	záchyt
tepelných	tepelný	k2eAgInPc2d1	tepelný
neutronů	neutron	k1gInPc2	neutron
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
objevil	objevit	k5eAaPmAgMnS	objevit
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
chemik	chemik	k1gMnSc1	chemik
Jean	Jean	k1gMnSc1	Jean
Charles	Charles	k1gMnSc1	Charles
Galissard	Galissard	k1gMnSc1	Galissard
de	de	k?	de
Marignac	Marignac	k1gFnSc4	Marignac
neznámé	známý	k2eNgFnSc2d1	neznámá
emisní	emisní	k2eAgFnSc2d1	emisní
linie	linie	k1gFnSc2	linie
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
didymu	didym	k1gInSc2	didym
a	a	k8xC	a
minerálu	minerál	k1gInSc2	minerál
gadolinitu	gadolinit	k1gInSc2	gadolinit
a	a	k8xC	a
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
neznámému	známý	k2eNgInSc3d1	neznámý
prvku	prvek	k1gInSc3	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
oxid	oxid	k1gInSc1	oxid
gadolinitý	gadolinitý	k2eAgMnSc1d1	gadolinitý
izoloval	izolovat	k5eAaBmAgMnS	izolovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Paul	Paul	k1gMnSc1	Paul
Émile	Émile	k1gFnSc2	Émile
Lecoq	Lecoq	k1gMnSc1	Lecoq
de	de	k?	de
Boisbaudran	Boisbaudran	k1gInSc1	Boisbaudran
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
yttria	yttrium	k1gNnSc2	yttrium
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
získalo	získat	k5eAaPmAgNnS	získat
gadolinium	gadolinium	k1gNnSc4	gadolinium
podle	podle	k7c2	podle
minerálu	minerál	k1gInSc2	minerál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
finském	finský	k2eAgNnSc6d1	finské
chemiku	chemik	k1gMnSc6	chemik
a	a	k8xC	a
geologovi	geolog	k1gMnSc6	geolog
Johanu	Johan	k1gMnSc6	Johan
Gadolinovi	Gadolin	k1gMnSc6	Gadolin
<g/>
.	.	kIx.	.
</s>
<s>
Gadolinium	gadolinium	k1gNnSc1	gadolinium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
5,4	[number]	k4	5,4
<g/>
-	-	kIx~	-
<g/>
6,4	[number]	k4	6,4
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
obsahu	obsah	k1gInSc6	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
údaje	údaj	k1gInPc1	údaj
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
gadolinia	gadolinium	k1gNnSc2	gadolinium
na	na	k7c4	na
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
gadolinium	gadolinium	k1gNnSc1	gadolinium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
ani	ani	k9	ani
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
lanthanoidy	lanthanoida	k1gFnPc1	lanthanoida
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
minerály	minerál	k1gInPc4	minerál
směsné	směsný	k2eAgInPc4d1	směsný
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
monazity	monazit	k1gInPc1	monazit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Th	Th	k1gFnSc1	Th
<g/>
,	,	kIx,	,
Nd	Nd	k1gFnSc1	Nd
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
a	a	k8xC	a
xenotim	xenotim	k1gInSc1	xenotim
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bastnäsity	bastnäsit	k1gInPc1	bastnäsit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
směsné	směsný	k2eAgInPc1d1	směsný
fluorouhličitany	fluorouhličitan	k1gInPc1	fluorouhličitan
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
např.	např.	kA	např.
minerál	minerál	k1gInSc1	minerál
gadolinit	gadolinit	k5eAaPmF	gadolinit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
<g/>
Nd	Nd	k1gFnSc1	Nd
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
FeBe	FeBe	k1gInSc1	FeBe
<g/>
2	[number]	k4	2
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
ve	v	k7c4	v
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fosfátové	fosfátový	k2eAgFnPc4d1	fosfátová
suroviny	surovina	k1gFnPc4	surovina
-	-	kIx~	-
apatity	apatit	k1gInPc4	apatit
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgInPc2d1	vzácný
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
rudy	ruda	k1gFnPc1	ruda
nejprve	nejprve	k6eAd1	nejprve
louží	loužit	k5eAaImIp3nP	loužit
směsí	směs	k1gFnSc7	směs
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
ze	z	k7c2	z
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
roztoku	roztok	k1gInSc2	roztok
solí	solit	k5eAaImIp3nS	solit
se	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Separace	separace	k1gFnSc1	separace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgInPc2d1	různý
postupů	postup	k1gInPc2	postup
-	-	kIx~	-
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
extrakcí	extrakce	k1gFnSc7	extrakce
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
ionexových	ionexový	k2eAgFnPc2d1	ionexová
kolon	kolona	k1gFnPc2	kolona
nebo	nebo	k8xC	nebo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
srážením	srážení	k1gNnSc7	srážení
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
komplexních	komplexní	k2eAgFnPc2d1	komplexní
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
gadolinitého	gadolinitý	k2eAgMnSc4d1	gadolinitý
Gd	Gd	k1gMnSc4	Gd
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
elementárním	elementární	k2eAgInSc7d1	elementární
vápníkem	vápník	k1gInSc7	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Gd	Gd	k?	Gd
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
Ca	ca	kA	ca
→	→	k?	→
2	[number]	k4	2
Gd	Gd	k1gFnPc2	Gd
+	+	kIx~	+
3	[number]	k4	3
CaO	CaO	k1gFnSc7	CaO
Ferromagnetické	ferromagnetický	k2eAgFnSc2d1	ferromagnetická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
gadolinia	gadolinium	k1gNnSc2	gadolinium
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
počítačových	počítačový	k2eAgInPc2d1	počítačový
harddisků	harddisk	k1gInPc2	harddisk
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
paměťových	paměťový	k2eAgNnPc2d1	paměťové
médií	médium	k1gNnPc2	médium
pro	pro	k7c4	pro
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
techniku	technika	k1gFnSc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
účinný	účinný	k2eAgInSc1d1	účinný
průřez	průřez	k1gInSc1	průřez
pro	pro	k7c4	pro
záchyt	záchyt	k1gInSc4	záchyt
neutronů	neutron	k1gInPc2	neutron
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
především	především	k9	především
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Gadolinium	gadolinium	k1gNnSc1	gadolinium
je	být	k5eAaImIp3nS	být
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
součástí	součást	k1gFnSc7	součást
moderátorových	moderátorův	k2eAgFnPc2d1	moderátorova
tyčí	tyč	k1gFnPc2	tyč
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
regulaci	regulace	k1gFnSc3	regulace
intenzity	intenzita	k1gFnSc2	intenzita
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zasunutí	zasunutí	k1gNnSc6	zasunutí
těchto	tento	k3xDgFnPc2	tento
tyčí	tyč	k1gFnPc2	tyč
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
reaktoru	reaktor	k1gInSc2	reaktor
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
záchytu	záchyt	k1gInSc3	záchyt
většiny	většina	k1gFnSc2	většina
uvolněných	uvolněný	k2eAgInPc2d1	uvolněný
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
zpomalení	zpomalení	k1gNnSc3	zpomalení
nebo	nebo	k8xC	nebo
úplnému	úplný	k2eAgNnSc3d1	úplné
zastavení	zastavení	k1gNnSc3	zastavení
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
výrobní	výrobní	k2eAgFnSc3d1	výrobní
ceně	cena	k1gFnSc3	cena
gadolinia	gadolinium	k1gNnSc2	gadolinium
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
materiály	materiál	k1gInPc1	materiál
používány	používat	k5eAaImNgInP	používat
jen	jen	k9	jen
ve	v	k7c6	v
speciálních	speciální	k2eAgInPc6d1	speciální
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
reaktorech	reaktor	k1gInPc6	reaktor
atomových	atomový	k2eAgFnPc2d1	atomová
ponorek	ponorka	k1gFnPc2	ponorka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metalurgickém	metalurgický	k2eAgInSc6d1	metalurgický
průmyslu	průmysl	k1gInSc6	průmysl
slouží	sloužit	k5eAaImIp3nS	sloužit
přídavky	přídavek	k1gInPc4	přídavek
malých	malý	k1gMnPc2	malý
množství	množství	k1gNnSc2	množství
gadolinia	gadolinium	k1gNnSc2	gadolinium
k	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
vlastností	vlastnost	k1gFnPc2	vlastnost
vysoce	vysoce	k6eAd1	vysoce
legovaných	legovaný	k2eAgFnPc2d1	legovaná
ocelí	ocel	k1gFnPc2	ocel
<g/>
,	,	kIx,	,
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
jejich	jejich	k3xOp3gFnSc4	jejich
opracovatelnost	opracovatelnost	k1gFnSc4	opracovatelnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
se	se	k3xPyFc4	se
sloučeniny	sloučenina	k1gFnPc1	sloučenina
gadolinia	gadolinium	k1gNnSc2	gadolinium
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
kontrastní	kontrastní	k2eAgFnPc1d1	kontrastní
látky	látka	k1gFnPc1	látka
při	při	k7c6	při
vyšetření	vyšetření	k1gNnSc6	vyšetření
pacienta	pacient	k1gMnSc2	pacient
metodou	metoda	k1gFnSc7	metoda
magnetické	magnetický	k2eAgFnSc2d1	magnetická
rezonance	rezonance	k1gFnSc2	rezonance
<g/>
.	.	kIx.	.
</s>
<s>
Injekčně	injekčně	k6eAd1	injekčně
aplikované	aplikovaný	k2eAgFnPc1d1	aplikovaná
soli	sůl	k1gFnPc1	sůl
gadolinia	gadolinium	k1gNnSc2	gadolinium
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
odezvy	odezva	k1gFnSc2	odezva
vyšetřované	vyšetřovaný	k2eAgFnSc2d1	vyšetřovaná
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gadolinium	gadolinium	k1gNnSc4	gadolinium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gadolinium	gadolinium	k1gNnSc4	gadolinium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
