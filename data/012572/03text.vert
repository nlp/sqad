<p>
<s>
Twist	twist	k1gInSc1	twist
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
kroutit	kroutit	k5eAaImF	kroutit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
provozován	provozovat	k5eAaImNgInS	provozovat
sólově	sólově	k6eAd1	sólově
<g/>
,	,	kIx,	,
partneři	partner	k1gMnPc1	partner
se	se	k3xPyFc4	se
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
nedrží	držet	k5eNaImIp3nS	držet
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
stojí	stát	k5eAaImIp3nP	stát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
žádná	žádný	k3yNgNnPc4	žádný
přesně	přesně	k6eAd1	přesně
daná	daný	k2eAgNnPc4d1	dané
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
rychlých	rychlý	k2eAgInPc2d1	rychlý
kroutivých	kroutivý	k2eAgInPc2d1	kroutivý
pohybů	pohyb	k1gInPc2	pohyb
pánve	pánev	k1gFnSc2	pánev
a	a	k8xC	a
rukou	ruka	k1gFnPc2	ruka
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
;	;	kIx,	;
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
rozkročeny	rozkročen	k2eAgFnPc1d1	rozkročena
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k9	jen
k	k	k7c3	k
přenášení	přenášení	k1gNnSc3	přenášení
váhy	váha	k1gFnSc2	váha
z	z	k7c2	z
nohy	noha	k1gFnSc2	noha
na	na	k7c4	na
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
sestup	sestup	k1gInSc4	sestup
do	do	k7c2	do
dřepu	dřep	k1gInSc2	dřep
<g/>
,	,	kIx,	,
následné	následný	k2eAgNnSc1d1	následné
vyskočení	vyskočení	k1gNnSc1	vyskočení
se	s	k7c7	s
zatleskáním	zatleskání	k1gNnSc7	zatleskání
apod.	apod.	kA	apod.
Twist	twist	k1gInSc1	twist
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tančí	tančit	k5eAaImIp3nS	tančit
na	na	k7c4	na
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollové	rollový	k2eAgFnPc4d1	rollová
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svobodný	svobodný	k2eAgInSc1d1	svobodný
tanec	tanec	k1gInSc1	tanec
twist	twist	k1gInSc1	twist
byl	být	k5eAaImAgMnS	být
prvotní	prvotní	k2eAgFnSc7d1	prvotní
změnou	změna	k1gFnSc7	změna
tzv.	tzv.	kA	tzv.
éry	éra	k1gFnSc2	éra
květinových	květinový	k2eAgFnPc2d1	květinová
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Startovalo	startovat	k5eAaBmAgNnS	startovat
hippies	hippiesa	k1gFnPc2	hippiesa
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
chtěly	chtít	k5eAaImAgFnP	chtít
bavit	bavit	k5eAaImF	bavit
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
free	free	k1gInSc4	free
a	a	k8xC	a
cool	cool	k1gInSc4	cool
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hudba	hudba	k1gFnSc1	hudba
plná	plný	k2eAgFnSc1d1	plná
dobré	dobrý	k2eAgFnPc4d1	dobrá
nálady	nálada	k1gFnPc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Melodie	melodie	k1gFnSc1	melodie
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
zpívá	zpívat	k5eAaImIp3nS	zpívat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
linku	linka	k1gFnSc4	linka
a	a	k8xC	a
v	v	k7c6	v
akordových	akordový	k2eAgInPc6d1	akordový
intervalech	interval	k1gInPc6	interval
ho	on	k3xPp3gInSc2	on
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
vokalisté	vokalista	k1gMnPc1	vokalista
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
názvu	název	k1gInSc6	název
písniček	písnička	k1gFnPc2	písnička
objevuje	objevovat	k5eAaImIp3nS	objevovat
twist	twist	k1gInSc1	twist
<g/>
.	.	kIx.	.
</s>
<s>
Twist	twist	k1gInSc1	twist
and	and	k?	and
shout	shoout	k5eAaPmNgInS	shoout
od	od	k7c2	od
Beatles	Beatles	k1gFnPc2	Beatles
nebo	nebo	k8xC	nebo
The	The	k1gFnSc1	The
twist	twist	k1gInSc4	twist
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
Chubbyho	Chubby	k1gMnSc2	Chubby
Checkera	Checker	k1gMnSc2	Checker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudba	hudba	k1gFnSc1	hudba
twistu	twist	k1gInSc2	twist
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollu	roll	k1gInSc3	roll
<g/>
.	.	kIx.	.
</s>
<s>
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
bicím	bicí	k2eAgNnSc7d1	bicí
<g/>
,	,	kIx,	,
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
kytarou	kytara	k1gFnSc7	kytara
<g/>
,	,	kIx,	,
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
klavírem	klavír	k1gInSc7	klavír
nebo	nebo	k8xC	nebo
basou	basa	k1gFnSc7	basa
<g/>
.	.	kIx.	.
</s>
<s>
Basa	basa	k1gFnSc1	basa
twistu	twist	k1gInSc2	twist
dodává	dodávat	k5eAaImIp3nS	dodávat
rytmus	rytmus	k1gInSc1	rytmus
<g/>
,	,	kIx,	,
posunuje	posunovat	k5eAaImIp3nS	posunovat
ho	on	k3xPp3gMnSc4	on
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
vybízí	vybízet	k5eAaImIp3nS	vybízet
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
typický	typický	k2eAgInSc1d1	typický
taneční	taneční	k2eAgInSc1d1	taneční
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kroucení	kroucení	k1gNnSc1	kroucení
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tanečníci	tanečník	k1gMnPc1	tanečník
přenášejí	přenášet	k5eAaImIp3nP	přenášet
váhu	váha	k1gFnSc4	váha
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
kroutí	kroutit	k5eAaImIp3nS	kroutit
přitom	přitom	k6eAd1	přitom
pánví	pánev	k1gFnPc2	pánev
a	a	k8xC	a
ještě	ještě	k9	ještě
jakoby	jakoby	k8xS	jakoby
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
do	do	k7c2	do
dřepu	dřep	k1gInSc2	dřep
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
vyskočit	vyskočit	k5eAaPmF	vyskočit
a	a	k8xC	a
zatleskat	zatleskat	k5eAaPmF	zatleskat
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
potlesk	potlesk	k1gInSc4	potlesk
mladých	mladý	k2eAgMnPc2d1	mladý
a	a	k8xC	a
nespoutaných	spoutaný	k2eNgMnPc2d1	nespoutaný
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
plných	plný	k2eAgNnPc2d1	plné
nadšení	nadšení	k1gNnPc2	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Do	do	k7c2	do
you	you	k?	you
love	lov	k1gInSc5	lov
me	me	k?	me
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
slyšet	slyšet	k5eAaImF	slyšet
i	i	k9	i
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
dílu	díl	k1gInSc6	díl
Hříšného	hříšný	k2eAgInSc2d1	hříšný
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Contours	Contoursa	k1gFnPc2	Contoursa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Twist	twist	k1gInSc1	twist
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
tanci	tanec	k1gInPc7	tanec
určitě	určitě	k6eAd1	určitě
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
živý	živý	k2eAgMnSc1d1	živý
<g/>
,	,	kIx,	,
originální	originální	k2eAgMnSc1d1	originální
a	a	k8xC	a
svérázný	svérázný	k2eAgMnSc1d1	svérázný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Twist	twist	k1gInSc1	twist
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
twist	twist	k1gInSc1	twist
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
