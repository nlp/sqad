<s>
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Plánický	Plánický	k2eAgMnSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
PlánickýZákladní	PlánickýZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Narození	narození	k1gNnSc3
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1691	#num#	k4
ManětínHabsburská	ManětínHabsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1732	#num#	k4
Freising	Freising	k1gInSc4
Bavorské	bavorský	k2eAgNnSc4d1
kurfiřtství	kurfiřtství	k1gNnSc4
Žánry	žánr	k1gInPc1
</s>
<s>
duchovní	duchovní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
dirigent	dirigent	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
hlas	hlas	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Plánický	Plánický	k2eAgMnSc1d1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1691	#num#	k4
Manětín	Manětín	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1732	#num#	k4
Freising	Freising	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Byl	být	k5eAaImAgMnS
synem	syn	k1gMnSc7
manětínského	manětínský	k2eAgMnSc2d1
kantora	kantor	k1gMnSc2
a	a	k8xC
varhaníka	varhaník	k1gMnSc2
Jaroslava	Jaroslav	k1gMnSc2
Plánického	Plánický	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
hudební	hudební	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
získal	získat	k5eAaPmAgMnS
v	v	k7c6
rodině	rodina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
patrně	patrně	k6eAd1
na	na	k7c6
některé	některý	k3yIgFnSc6
z	z	k7c2
jezuitských	jezuitský	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1715	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vychovatelem	vychovatel	k1gMnSc7
v	v	k7c6
rodině	rodina	k1gFnSc6
Václava	Václav	k1gMnSc2
Josefa	Josef	k1gMnSc2
Lažanského	Lažanský	k2eAgMnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Marie	Maria	k1gFnSc2
Gabriely	Gabriela	k1gFnSc2
Černínové	Černínová	k1gFnSc2
z	z	k7c2
Chudenic	Chudenice	k1gFnPc2
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Manětíně	Manětín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
propuštění	propuštění	k1gNnSc4
ze	z	k7c2
služby	služba	k1gFnSc2
a	a	k8xC
vystřídal	vystřídat	k5eAaPmAgInS
zaměstnání	zaměstnání	k1gNnSc4
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1720	#num#	k4
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
již	již	k6eAd1
s	s	k7c7
rodinou	rodina	k1gFnSc7
v	v	k7c6
hornobavorském	hornobavorský	k2eAgInSc6d1
Freisingu	Freising	k1gInSc6
a	a	k8xC
na	na	k7c4
přímluvu	přímluva	k1gFnSc4
kanovníka	kanovník	k1gMnSc2
Phillipa	Phillip	k1gMnSc2
Franze	Franze	k1gFnSc2
Lindmayera	Lindmayer	k1gMnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
tenoristou	tenorista	k1gMnSc7
v	v	k7c6
biskupské	biskupský	k2eAgFnSc6d1
kapele	kapela	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
povinností	povinnost	k1gFnPc2
byla	být	k5eAaImAgFnS
i	i	k9
hra	hra	k1gFnSc1
v	v	k7c6
instrumentálních	instrumentální	k2eAgInPc6d1
souborech	soubor	k1gInPc6
a	a	k8xC
hudební	hudební	k2eAgFnSc1d1
výuka	výuka	k1gFnSc1
chlapců	chlapec	k1gMnPc2
v	v	k7c6
semináři	seminář	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tady	tady	k6eAd1
zkomponoval	zkomponovat	k5eAaPmAgInS
své	svůj	k3xOyFgInPc4
nejslavnější	slavný	k2eAgInPc4d3
a	a	k8xC
vlastně	vlastně	k9
jediné	jediný	k2eAgNnSc1d1
v	v	k7c6
úplnosti	úplnost	k1gFnSc6
dochované	dochovaný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
sbírku	sbírka	k1gFnSc4
dvanácti	dvanáct	k4xCc2
duchovních	duchovní	k2eAgFnPc2d1
árií	árie	k1gFnPc2
Opella	Opella	k1gMnSc1
ecclesiastica	ecclesiastica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1724	#num#	k4
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
pověřen	pověřit	k5eAaPmNgMnS
napsat	napsat	k5eAaPmF,k5eAaBmF
slavnostní	slavnostní	k2eAgFnSc4d1
operu	opera	k1gFnSc4
k	k	k7c3
oslavám	oslava	k1gFnPc3
tisíciletého	tisíciletý	k2eAgNnSc2d1
jubilea	jubileum	k1gNnSc2
biskupství	biskupství	k1gNnSc4
<g/>
:	:	kIx,
Zelus	Zelus	k1gInSc1
divi	div	k1gFnSc2
Corbiniani	Corbiniaň	k1gFnSc3
Ecclesiae	Ecclesiae	k1gFnSc3
Frisigensis	Frisigensis	k1gFnSc2
Fundamentum	Fundamentum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
s	s	k7c7
úspěchem	úspěch	k1gInSc7
provedena	proveden	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
se	se	k3xPyFc4
nedochovala	dochovat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
zkomponoval	zkomponovat	k5eAaPmAgMnS
četné	četný	k2eAgFnPc4d1
litanie	litanie	k1gFnPc4
<g/>
,	,	kIx,
moteta	moteto	k1gNnPc4
<g/>
,	,	kIx,
Te	Te	k1gFnPc4
Deum	Deuma	k1gFnPc2
<g/>
,	,	kIx,
Rekviem	rekviem	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
zábavnou	zábavný	k2eAgFnSc4d1
společenskou	společenský	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
musica	musica	k6eAd1
navalis	navalis	k1gFnSc4
pro	pro	k7c4
pražské	pražský	k2eAgFnPc4d1
vyjížďky	vyjížďka	k1gFnPc4
po	po	k7c6
Vltavě	Vltava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nic	nic	k3yNnSc1
z	z	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
však	však	k9
nedochovalo	dochovat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biskupská	biskupský	k2eAgFnSc1d1
rezidence	rezidence	k1gFnSc1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
a	a	k8xC
její	její	k3xOp3gInSc1
archiv	archiv	k1gInSc1
byl	být	k5eAaImAgInS
rozebrán	rozebrat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Opella	Opella	k1gMnSc1
ecclesiastica	ecclesiastica	k1gMnSc1
seu	seu	k?
Ariae	Aria	k1gFnSc2
duodecim	duodecima	k1gFnPc2
nova	nova	k1gFnSc1
idea	idea	k1gFnSc1
excornatae	excornatae	k1gFnSc1
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
dvanácti	dvanáct	k4xCc2
duchovních	duchovní	k2eAgFnPc2d1
kantát	kantáta	k1gFnPc2
(	(	kIx(
<g/>
1723	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sbírka	sbírka	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
7	#num#	k4
sopránových	sopránový	k2eAgMnPc2d1
<g/>
,	,	kIx,
3	#num#	k4
altové	altový	k2eAgInPc1d1
a	a	k8xC
2	#num#	k4
basové	basový	k2eAgFnSc2d1
árie	árie	k1gFnSc2
s	s	k7c7
doprovodem	doprovod	k1gInSc7
varhan	varhany	k1gFnPc2
nebo	nebo	k8xC
cembala	cembalo	k1gNnSc2
<g/>
,	,	kIx,
dvojích	dvojí	k4xRgInPc2
houslí	housle	k1gFnPc2
<g/>
,	,	kIx,
violoncella	violoncello	k1gNnSc2
nebo	nebo	k8xC
violy	viola	k1gFnSc2
<g/>
,	,	kIx,
sólových	sólový	k2eAgFnPc2d1
houslí	housle	k1gFnPc2
nebo	nebo	k8xC
hoboje	hoboj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodné	pozoruhodný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ač	ač	k8xS
skladatel	skladatel	k1gMnSc1
sám	sám	k3xTgMnSc1
byl	být	k5eAaImAgMnS
tenor	tenor	k1gInSc4
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
árii	árie	k1gFnSc4
pro	pro	k7c4
tento	tento	k3xDgInSc4
hlas	hlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
De	De	k?
amore	amor	k1gMnSc5
erga	ergus	k1gMnSc2
deum	deu	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonata	Sonata	k1gFnSc1
<g/>
,	,	kIx,
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Allegro	allegro	k1gNnSc4
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
communione	communion	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Tarde	Tard	k1gInSc5
et	et	k?
affectuose	affectuosa	k1gFnSc3
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
confessore	confessor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recitativo	Recitativa	k1gFnSc5
Aria	Ari	k2eAgFnSc1d1
–	–	k?
Vivace	vivace	k1gFnSc1
(	(	kIx(
<g/>
alt	alt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
tempore	tempor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonata	Sonata	k1gFnSc1
<g/>
,	,	kIx,
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Vivace	vivace	k1gFnSc1
(	(	kIx(
<g/>
bas	bas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
venerabili	venerabit	k5eAaPmAgMnP,k5eAaImAgMnP
sacramento	sacramento	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonata	Sonata	k1gFnSc1
<g/>
,	,	kIx,
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Tarde	Tard	k1gInSc5
e	e	k0
con	con	k?
affetto	affetto	k1gNnSc1
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
tempore	tempor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Andante	andante	k1gNnSc4
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
ss	ss	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martyribus	Martyribus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Vivace	vivace	k1gFnSc1
(	(	kIx(
<g/>
alt	alt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
omni	omnout	k5eAaPmRp2nS
tempore	tempor	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Presto	presto	k6eAd1
assai	assai	k1gNnSc1
(	(	kIx(
<g/>
bas	bas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
beata	beata	k1gFnSc1
virgine	virginout	k5eAaPmIp3nS
Maria	Maria	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonata	Sonata	k1gFnSc1
<g/>
,	,	kIx,
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Andante	andante	k1gNnSc4
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
virgine	virginout	k5eAaPmIp3nS
et	et	k?
martyre	martyr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Allegro	allegro	k1gNnSc4
(	(	kIx(
<g/>
alt	alt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
sacratissima	sacratissima	k1gFnSc1
eucharistia	eucharistia	k1gFnSc1
Sonatella	Sonatella	k1gFnSc1
<g/>
,	,	kIx,
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Andante	andante	k1gNnSc4
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Funebris	Funebris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonata	Sonata	k1gFnSc1
<g/>
,	,	kIx,
Recitativo	Recitativa	k1gFnSc5
<g/>
,	,	kIx,
Aria	Aria	k1gMnSc1
–	–	k?
Tarde	Tard	k1gInSc5
(	(	kIx(
<g/>
soprán	soprán	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Československý	československý	k2eAgInSc1d1
hudební	hudební	k2eAgInSc1d1
slovník	slovník	k1gInSc1
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
M	M	kA
<g/>
–	–	k?
<g/>
Ž	Ž	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
hudební	hudební	k2eAgNnSc1d1
vydavatelství	vydavatelství	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
Manětín	Manětín	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Plánický	Plánický	k2eAgMnSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Plánický	Plánický	k2eAgMnSc1d1
v	v	k7c6
Českém	český	k2eAgInSc6d1
hudebním	hudební	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
<g/>
?	?	kIx.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1
díla	dílo	k1gNnSc2
v	v	k7c6
realizaci	realizace	k1gFnSc6
Jaroslava	Jaroslav	k1gMnSc2
Krčka	Krček	k1gMnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2002145118	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
103848584	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0996	#num#	k4
8104	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
88008086	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
88078329	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
88008086	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
