<p>
<s>
V	v	k7c6	v
soubojích	souboj	k1gInPc6	souboj
16	[number]	k4	16
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
krajského	krajský	k2eAgInSc2d1	krajský
přeboru	přebor	k1gInSc2	přebor
1975	[number]	k4	1975
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
5	[number]	k4	5
<g/>
.	.	kIx.	.
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
utkalo	utkat	k5eAaPmAgNnS	utkat
16	[number]	k4	16
týmů	tým	k1gInPc2	tým
každý	každý	k3xTgInSc1	každý
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
dvoukolovým	dvoukolový	k2eAgInSc7d1	dvoukolový
systémem	systém	k1gInSc7	systém
podzim	podzim	k1gInSc1	podzim
<g/>
–	–	k?	–
<g/>
jaro	jaro	k1gNnSc1	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ročník	ročník	k1gInSc1	ročník
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1975	[number]	k4	1975
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nové	Nové	k2eAgInPc1d1	Nové
týmy	tým	k1gInPc1	tým
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1975	[number]	k4	1975
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Divize	divize	k1gFnSc2	divize
D	D	kA	D
1974	[number]	k4	1974
<g/>
/	/	kIx~	/
<g/>
75	[number]	k4	75
sestoupilo	sestoupit	k5eAaPmAgNnS	sestoupit
do	do	k7c2	do
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
krajského	krajský	k2eAgInSc2d1	krajský
přeboru	přebor	k1gInSc2	přebor
mužstvo	mužstvo	k1gNnSc4	mužstvo
TJ	tj	kA	tj
Spartak	Spartak	k1gInSc4	Spartak
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
I.	I.	kA	I.
A	a	k9	a
třídy	třída	k1gFnSc2	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
1974	[number]	k4	1974
<g/>
/	/	kIx~	/
<g/>
75	[number]	k4	75
postoupila	postoupit	k5eAaPmAgFnS	postoupit
mužstva	mužstvo	k1gNnSc2	mužstvo
TJ	tj	kA	tj
Spartak	Spartak	k1gInSc1	Spartak
Třebíč	Třebíč	k1gFnSc1	Třebíč
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TJ	tj	kA	tj
Sokol	Sokol	k1gMnSc1	Sokol
Lanžhot	Lanžhot	k1gMnSc1	Lanžhot
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
)	)	kIx)	)
a	a	k8xC	a
TJ	tj	kA	tj
Spartak	Spartak	k1gInSc1	Spartak
Hluk	hluk	k1gInSc1	hluk
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
skupiny	skupina	k1gFnSc2	skupina
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konečná	konečný	k2eAgFnSc1d1	konečná
tabulka	tabulka	k1gFnSc1	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Z	z	k7c2	z
=	=	kIx~	=
Odehrané	odehraný	k2eAgInPc1d1	odehraný
zápasy	zápas	k1gInPc1	zápas
<g/>
;	;	kIx,	;
V	v	k7c4	v
=	=	kIx~	=
Vítězství	vítězství	k1gNnSc4	vítězství
<g/>
;	;	kIx,	;
R	R	kA	R
=	=	kIx~	=
Remízy	remíza	k1gFnSc2	remíza
<g/>
;	;	kIx,	;
P	P	kA	P
=	=	kIx~	=
Prohry	prohra	k1gFnSc2	prohra
<g/>
;	;	kIx,	;
VG	VG	kA	VG
=	=	kIx~	=
Vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
OG	OG	kA	OG
=	=	kIx~	=
Obdržené	obdržený	k2eAgInPc1d1	obdržený
góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
B	B	kA	B
=	=	kIx~	=
Body	bod	k1gInPc4	bod
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
=	=	kIx~	=
Mužstvo	mužstvo	k1gNnSc1	mužstvo
sestoupivší	sestoupivší	k2eAgNnSc1d1	sestoupivší
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
=	=	kIx~	=
Mužstvo	mužstvo	k1gNnSc1	mužstvo
postoupivší	postoupivší	k2eAgNnSc1d1	postoupivší
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
soutěže	soutěž	k1gFnSc2	soutěž
(	(	kIx(	(
<g/>
nováček	nováček	k1gMnSc1	nováček
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
krajský	krajský	k2eAgInSc1d1	krajský
přebor	přebor	k1gInSc1	přebor
1975	[number]	k4	1975
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
<g/>
,	,	kIx,	,
fkbanikdubnany	fkbanikdubnana	k1gFnPc1	fkbanikdubnana
<g/>
.	.	kIx.	.
<g/>
estranky	estranka	k1gFnPc1	estranka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Jihomoravského	jihomoravský	k2eAgNnSc2d1	Jihomoravské
KFS	KFS	kA	KFS
<g/>
,	,	kIx,	,
jmkfs	jmkfs	k1gInSc1	jmkfs
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
