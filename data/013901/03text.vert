<s>
Pelerína	pelerína	k1gFnSc1
</s>
<s>
cestovní	cestovní	k2eAgInSc4d1
kabát	kabát	k1gInSc4
s	s	k7c7
pelerínou	pelerína	k1gFnSc7
<g/>
,	,	kIx,
dobový	dobový	k2eAgInSc1d1
tisk	tisk	k1gInSc1
</s>
<s>
Pelerína	pelerína	k1gFnSc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
pellerine	pellerin	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
volně	volně	k6eAd1
splývající	splývající	k2eAgInSc1d1
oděv	oděv	k1gInSc1
<g/>
,	,	kIx,
bez	bez	k7c2
rukávů	rukáv	k1gInPc2
a	a	k8xC
kapes	kapsa	k1gFnPc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
s	s	k7c7
průhmatem	průhmat	k1gInSc7
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
lištou	lišta	k1gFnSc7
krytý	krytý	k2eAgInSc1d1
otvor	otvor	k1gInSc1
v	v	k7c6
předním	přední	k2eAgInSc6d1
dílu	díl	k1gInSc6
oděvu	oděv	k1gInSc2
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
provléknutí	provléknutí	k1gNnSc3
ruky	ruka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
většinou	většina	k1gFnSc7
jako	jako	k9
svrchní	svrchní	k2eAgFnPc1d1
(	(	kIx(
<g/>
plášť	plášť	k1gInSc1
<g/>
,	,	kIx,
pláštěnka	pláštěnka	k1gFnSc1
<g/>
)	)	kIx)
oblečení	oblečení	k1gNnSc1
<g/>
,	,	kIx,
pokrývající	pokrývající	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc7
volností	volnost	k1gFnSc7
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
zvonové	zvonový	k2eAgInPc4d1
záhyby	záhyb	k1gInPc4
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dlouhý	dlouhý	k2eAgInSc4d1
nebo	nebo	k8xC
krátký	krátký	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
stejnokroj	stejnokroj	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
i	i	k9
součástí	součást	k1gFnSc7
večerních	večerní	k2eAgInPc2d1
oděvů	oděv	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
nosí	nosit	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
módní	módní	k2eAgInSc4d1
oděv	oděv	k1gInSc4
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
pláště	plášť	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
byla	být	k5eAaImAgFnS
pelerína	pelerína	k1gFnSc1
přehozem	přehoz	k1gInSc7
bez	bez	k7c2
rukávů	rukáv	k1gInPc2
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
pončo	pončo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
nosili	nosit	k5eAaImAgMnP
poutníci	poutník	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
<g/>
)	)	kIx)
z	z	k7c2
účelu	účel	k1gInSc2
využití	využití	k1gNnSc4
–	–	k?
poutníci	poutník	k1gMnPc1
jí	on	k3xPp3gFnSc3
nosili	nosit	k5eAaImAgMnP
jako	jako	k8xC,k8xS
cestovní	cestovní	k2eAgInSc4d1
plášť	plášť	k1gInSc4
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
z	z	k7c2
ní	on	k3xPp3gFnSc2
pokrývku	pokrývka	k1gFnSc4
pro	pro	k7c4
spaní	spaní	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
na	na	k7c6
ní	on	k3xPp3gFnSc6
seděli	sedět	k5eAaImAgMnP
(	(	kIx(
<g/>
i	i	k9
jedli	jedle	k1gFnSc4
<g/>
)	)	kIx)
při	při	k7c6
odpočinku	odpočinek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
latinské	latinský	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
<g/>
,	,	kIx,
označující	označující	k2eAgFnSc4d1
pouť	pouť	k1gFnSc4
(	(	kIx(
<g/>
peregrinatio	peregrinatio	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
něj	on	k3xPp3gNnSc2
odvozené	odvozený	k2eAgInPc1d1
italské	italský	k2eAgInPc1d1
pellegrinaggio	pellegrinaggio	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
i	i	k9
z	z	k7c2
francouzského	francouzský	k2eAgMnSc2d1
pelerin	pelerin	k1gInSc1
–	–	k?
poutník	poutník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Módní	módní	k2eAgFnSc1d1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
obraz	obraz	k1gInSc1
E.	E.	kA
Maneta	manet	k1gInSc2
–	–	k?
Mladá	mladý	k2eAgFnSc1d1
žena	žena	k1gFnSc1
v	v	k7c6
peleríně	pelerína	k1gFnSc6
<g/>
,	,	kIx,
1881	#num#	k4
</s>
<s>
Oděv	oděv	k1gInSc1
</s>
<s>
Oděvem	oděv	k1gInSc7
vždy	vždy	k6eAd1
ukazoval	ukazovat	k5eAaImAgMnS
jedinec	jedinec	k1gMnSc1
své	svůj	k3xOyFgNnSc4
postavení	postavení	k1gNnSc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
současně	současně	k6eAd1
se	se	k3xPyFc4
vymezoval	vymezovat	k5eAaImAgMnS
<g/>
,	,	kIx,
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
osobitost	osobitost	k1gFnSc4
<g/>
,	,	kIx,
bohatství	bohatství	k1gNnSc4
i	i	k8xC
dobrý	dobrý	k2eAgInSc4d1
vkus	vkus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oděv	oděv	k1gInSc1
podtrhoval	podtrhovat	k5eAaImAgInS
krásu	krása	k1gFnSc4
žen	žena	k1gFnPc2
i	i	k8xC
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
investicí	investice	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
nešetřilo	šetřit	k5eNaImAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
oděv	oděv	k1gInSc1
mohl	moct	k5eAaImAgInS
člověka	člověk	k1gMnSc4
pozdvihnout	pozdvihnout	k5eAaPmF
na	na	k7c6
společenském	společenský	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
o	o	k7c4
úroveň	úroveň	k1gFnSc4
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
poznávacím	poznávací	k2eAgInSc7d1
znakem	znak	k1gInSc7
profesního	profesní	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
oděv	oděv	k1gInSc4
řeholníků	řeholník	k1gMnPc2
nebo	nebo	k8xC
řemeslníků	řemeslník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
historických	historický	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
se	se	k3xPyFc4
na	na	k7c6
výrobě	výroba	k1gFnSc6
jediného	jediný	k2eAgInSc2d1
kusu	kus	k1gInSc2
oděvu	oděv	k1gInSc2
podílelo	podílet	k5eAaImAgNnS
mnoho	mnoho	k4c1
řemeslníků	řemeslník	k1gMnPc2
(	(	kIx(
<g/>
od	od	k7c2
výroby	výroba	k1gFnSc2
materiálu	materiál	k1gInSc2
po	po	k7c4
konečný	konečný	k2eAgInSc4d1
produkt	produkt	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
velmi	velmi	k6eAd1
dlouhé	dlouhý	k2eAgNnSc4d1
období	období	k1gNnSc4
byla	být	k5eAaImAgFnS
oděvní	oděvní	k2eAgFnSc1d1
a	a	k8xC
textilní	textilní	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
hnacím	hnací	k2eAgInSc7d1
motorem	motor	k1gInSc7
evropského	evropský	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3
doklady	doklad	k1gInPc4
o	o	k7c6
výrobě	výroba	k1gFnSc6
tkanin	tkanina	k1gFnPc2
na	na	k7c4
oděvy	oděv	k1gInPc4
(	(	kIx(
<g/>
z	z	k7c2
živočišných	živočišný	k2eAgInPc2d1
či	či	k8xC
rostlinných	rostlinný	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
)	)	kIx)
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
až	až	k9
do	do	k7c2
šestého	šestý	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
nalezeny	naleznout	k5eAaPmNgFnP,k5eAaBmNgFnP
na	na	k7c4
území	území	k1gNnSc4
Turecka	Turecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInPc1d3
doklady	doklad	k1gInPc1
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
pak	pak	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
mladší	mladý	k2eAgFnSc2d2
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
u	u	k7c2
lněných	lněný	k2eAgFnPc2d1
tkanin	tkanina	k1gFnPc2
dokonce	dokonce	k9
z	z	k7c2
mladší	mladý	k2eAgFnSc2d2
doby	doba	k1gFnSc2
kamenné	kamenný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Řemesla	řemeslo	k1gNnPc1
v	v	k7c6
oděvnictví	oděvnictví	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
historie	historie	k1gFnSc2
vyvíjela	vyvíjet	k5eAaImAgFnS
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
první	první	k4xOgInPc4
krejčovské	krejčovský	k2eAgInPc4d1
cechy	cech	k1gInPc4
vznikaly	vznikat	k5eAaImAgFnP
až	až	k6eAd1
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Čechách	Čechy	k1gFnPc6
byl	být	k5eAaImAgInS
první	první	k4xOgInSc1
krejčovský	krejčovský	k2eAgInSc1d1
cech	cech	k1gInSc1
založen	založit	k5eAaPmNgInS
dokonce	dokonce	k9
až	až	k6eAd1
roku	rok	k1gInSc2
1318	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církevní	církevní	k2eAgNnSc1d1
roucha	roucho	k1gNnPc1
se	se	k3xPyFc4
šila	šít	k5eAaImAgNnP
v	v	k7c6
klášterech	klášter	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Starověk	starověk	k1gInSc1
</s>
<s>
Strabón	Strabón	k1gInSc1
(	(	kIx(
<g/>
64	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l	l	kA
<g/>
)	)	kIx)
popisuje	popisovat	k5eAaImIp3nS
detailně	detailně	k6eAd1
vzhled	vzhled	k1gInSc1
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
galských	galský	k2eAgMnPc2d1
kmenů	kmen	k1gInPc2
–	–	k?
Belgů	Belg	k1gMnPc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Nosí	nosit	k5eAaImIp3nP
peleríny	pelerína	k1gFnPc1
(	(	kIx(
<g/>
sagum	sagum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
,,,	,,,	k?
<g/>
tkané	tkaný	k2eAgInPc1d1
ze	z	k7c2
silné	silný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
..	..	k?
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
románské	románský	k2eAgFnSc6d1
se	se	k3xPyFc4
užíval	užívat	k5eAaImAgInS
dlouhý	dlouhý	k2eAgInSc1d1
čtverhranný	čtverhranný	k2eAgInSc1d1
plášť	plášť	k1gInSc1
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
antické	antický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
(	(	kIx(
<g/>
lacerna	lacerna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
běžně	běžně	k6eAd1
nosil	nosit	k5eAaImAgInS
plášť	plášť	k1gInSc1
kolový	kolový	k2eAgInSc1d1
(	(	kIx(
<g/>
z	z	k7c2
němčiny	němčina	k1gFnSc2
mantel	mantel	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rovněž	rovněž	k9
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
antické	antický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
(	(	kIx(
<g/>
paenula	paenout	k5eAaPmAgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neměl	mít	k5eNaImAgMnS
zpravidla	zpravidla	k6eAd1
žádný	žádný	k3yNgInSc4
límec	límec	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
dosedal	dosedat	k5eAaImAgMnS
přímo	přímo	k6eAd1
na	na	k7c4
krk	krk	k1gInSc4
<g/>
,	,	kIx,
převislý	převislý	k2eAgInSc1d1
cíp	cíp	k1gInSc1
pláště	plášť	k1gInSc2
se	se	k3xPyFc4
upravoval	upravovat	k5eAaImAgMnS
do	do	k7c2
podoby	podoba	k1gFnSc2
kápě	kápě	k1gFnSc2
<g/>
,	,	kIx,
kryjící	kryjící	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
–	–	k?
z	z	k7c2
tohoto	tento	k3xDgInSc2
cípu	cíp	k1gInSc2
se	se	k3xPyFc4
následně	následně	k6eAd1
vyvinula	vyvinout	k5eAaPmAgFnS
samostatná	samostatný	k2eAgFnSc1d1
kapuce	kapuce	k1gFnSc1
(	(	kIx(
<g/>
bardocuccullus	bardocuccullus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
na	na	k7c6
výjevech	výjev	k1gInPc6
(	(	kIx(
<g/>
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
<g/>
st.	st.	kA
<g/>
)	)	kIx)
objevují	objevovat	k5eAaImIp3nP
také	také	k9
jednoduché	jednoduchý	k2eAgFnPc1d1
kukly	kukla	k1gFnPc1
<g/>
,	,	kIx,
chránící	chránící	k2eAgMnPc1d1
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
délce	délka	k1gFnSc6
límce	límec	k1gInSc2
částečně	částečně	k6eAd1
i	i	k9
ramena	rameno	k1gNnPc4
<g/>
,	,	kIx,
záda	záda	k1gNnPc4
a	a	k8xC
hruď	hruď	k1gFnSc4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
peleríny	pelerína	k1gFnPc1
s	s	k7c7
kapucí	kapuce	k1gFnSc7
(	(	kIx(
<g/>
cucullum	cucullum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Plášť	plášť	k1gInSc1
byl	být	k5eAaImAgInS
nákladnou	nákladný	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
prostá	prostý	k2eAgFnSc1d1
vlněná	vlněný	k2eAgFnSc1d1
houně	houně	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c4
způsob	způsob	k1gInSc4
ponča	pončo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
pelerína	pelerína	k1gFnSc1
byla	být	k5eAaImAgFnS
levnější	levný	k2eAgFnSc1d2
a	a	k8xC
dostupnější	dostupný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dobových	dobový	k2eAgInPc2d1
obrazů	obraz	k1gInPc2
známe	znát	k5eAaImIp1nP
jakousi	jakýsi	k3yIgFnSc4
pelerínu	pelerína	k1gFnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
s	s	k7c7
pevně	pevně	k6eAd1
připojenou	připojený	k2eAgFnSc7d1
kápí	kápě	k1gFnSc7
<g/>
,	,	kIx,
dosahující	dosahující	k2eAgFnSc4d1
zpravidla	zpravidla	k6eAd1
k	k	k7c3
pasu	pas	k1gInSc3
nebo	nebo	k8xC
mírně	mírně	k6eAd1
pod	pod	k7c4
pas	pas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nosila	nosit	k5eAaImAgFnS
se	se	k3xPyFc4
hlavně	hlavně	k9
do	do	k7c2
nepohody	nepohoda	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
aby	aby	kYmCp3nS
nebránila	bránit	k5eNaImAgFnS
při	při	k7c6
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
shrnovala	shrnovat	k5eAaImAgFnS
se	se	k3xPyFc4
dozadu	dozadu	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ruce	ruka	k1gFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
volné	volný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc4d1
typ	typ	k1gInSc4
oděvu	oděv	k1gInSc2
pak	pak	k6eAd1
vídáme	vídat	k5eAaImIp1nP
i	i	k9
na	na	k7c6
výjevech	výjev	k1gInPc6
ze	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
kněžský	kněžský	k2eAgInSc1d1
oděv	oděv	k1gInSc1
s	s	k7c7
pelerínou	pelerína	k1gFnSc7
</s>
<s>
Středověk	středověk	k1gInSc1
</s>
<s>
V	v	k7c6
antických	antický	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
byla	být	k5eAaImAgFnS
uniformita	uniformita	k1gFnSc1
oděvů	oděv	k1gInPc2
zachována	zachovat	k5eAaPmNgFnS
tisíce	tisíc	k4xCgInPc1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
postupem	postup	k1gInSc7
času	čas	k1gInSc2
se	se	k3xPyFc4
však	však	k9
úseky	úsek	k1gInPc1
mezi	mezi	k7c7
novými	nový	k2eAgInPc7d1
vlivy	vliv	k1gInPc7
zkracovaly	zkracovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblečení	oblečení	k1gNnSc1
se	se	k3xPyFc4
historicky	historicky	k6eAd1
vyvíjelo	vyvíjet	k5eAaImAgNnS
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
dnešních	dnešní	k2eAgInPc2d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
rychlých	rychlý	k2eAgFnPc2d1
módních	módní	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
relativně	relativně	k6eAd1
pomalu	pomalu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
historii	historie	k1gFnSc6
módy	móda	k1gFnSc2
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
hovořit	hovořit	k5eAaImF
zhruba	zhruba	k6eAd1
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pelerína	pelerína	k1gFnSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
oděvu	oděv	k1gInSc2
<g/>
,	,	kIx,
prodělala	prodělat	k5eAaPmAgFnS
také	také	k9
svůj	svůj	k3xOyFgInSc4
vývoj	vývoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijímala	přijímat	k5eAaImAgFnS
různé	různý	k2eAgInPc4d1
tvary	tvar	k1gInPc4
a	a	k8xC
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	s	k7c7
standardním	standardní	k2eAgNnSc7d1
oblečením	oblečení	k1gNnSc7
anglikánského	anglikánský	k2eAgNnSc2d1
duchovenstva	duchovenstvo	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
součástmi	součást	k1gFnPc7
„	„	k?
<g/>
oblečení	oblečení	k1gNnSc3
do	do	k7c2
chóru	chór	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
Zvláštním	zvláštní	k2eAgInSc7d1
typem	typ	k1gInSc7
peleríny	pelerína	k1gFnSc2
je	být	k5eAaImIp3nS
expositorium	expositorium	k1gNnSc4
canonicale	canonicale	k6eAd1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
oděvu	oděv	k1gInSc2
katolických	katolický	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
většině	většina	k1gFnSc6
západních	západní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vidět	vidět	k5eAaImF
určitý	určitý	k2eAgInSc4d1
typ	typ	k1gInSc4
peleríny	pelerína	k1gFnSc2
např.	např.	kA
u	u	k7c2
soudů	soud	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
součástí	součást	k1gFnSc7
uniforem	uniforma	k1gFnPc2
pracovníků	pracovník	k1gMnPc2
pošt	pošta	k1gFnPc2
<g/>
,	,	kIx,
drah	draha	k1gFnPc2
<g/>
,	,	kIx,
atd.	atd.	kA
</s>
<s>
Pelerínu	pelerína	k1gFnSc4
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
bílého	bílý	k2eAgNnSc2d1
sukna	sukno	k1gNnSc2
<g/>
,	,	kIx,
se	s	k7c7
stojatým	stojatý	k2eAgInSc7d1
límcem	límec	k1gInSc7
<g/>
,	,	kIx,
bohatě	bohatě	k6eAd1
vyšívanou	vyšívaný	k2eAgFnSc4d1
zlatým	zlatý	k2eAgInSc7d1
dracounem	dracoun	k1gInSc7
orientálními	orientální	k2eAgInPc7d1
motivy	motiv	k1gInPc7
<g/>
,	,	kIx,
podšitou	podšitý	k2eAgFnSc4d1
růžovým	růžový	k2eAgInSc7d1
saténem	satén	k1gInSc7
<g/>
,	,	kIx,
se	s	k7c7
zapínáním	zapínání	k1gNnSc7
na	na	k7c4
jeden	jeden	k4xCgInSc4
knoflík	knoflík	k1gInSc4
<g/>
,	,	kIx,
s	s	k7c7
okrajem	okraj	k1gInSc7
lemovaným	lemovaný	k2eAgInSc7d1
silnějším	silný	k2eAgInSc7d2
zlatým	zlatý	k2eAgInSc7d1
dracounem	dracoun	k1gInSc7
<g/>
,	,	kIx,
restaurovali	restaurovat	k5eAaBmAgMnP
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pelerína	pelerína	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
symbolem	symbol	k1gInSc7
poutníka	poutník	k1gMnSc2
<g/>
,	,	kIx,
vytvářela	vytvářet	k5eAaImAgFnS
pouto	pouto	k1gNnSc4
napříč	napříč	k7c7
všemi	všecek	k3xTgFnPc7
společenskými	společenský	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
<g/>
,	,	kIx,
protože	protože	k8xS
ji	on	k3xPp3gFnSc4
nosili	nosit	k5eAaImAgMnP
nejen	nejen	k6eAd1
putující	putující	k2eAgMnPc1d1
aristokraté	aristokrat	k1gMnPc1
<g/>
,	,	kIx,
důstojníci	důstojník	k1gMnPc1
<g/>
,	,	kIx,
vyšší	vysoký	k2eAgFnPc1d2
i	i	k8xC
střední	střední	k2eAgFnSc1d1
třída	třída	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
zemědělci	zemědělec	k1gMnPc1
a	a	k8xC
pastevci	pastevec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholu	vrchol	k1gInSc2
oblíbenosti	oblíbenost	k1gFnSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
kožešinová	kožešinový	k2eAgFnSc1d1
pelerína	pelerína	k1gFnSc1
<g/>
,	,	kIx,
1902	#num#	k4
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Velkou	velký	k2eAgFnSc7d1
módní	módní	k2eAgFnSc7d1
novinkou	novinka	k1gFnSc7
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
krátké	krátký	k2eAgFnPc1d1
peleríny	pelerína	k1gFnPc1
a	a	k8xC
bolerka	bolerko	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
mohla	moct	k5eAaImAgNnP
nosit	nosit	k5eAaImF
jak	jak	k6eAd1
k	k	k7c3
šatům	šat	k1gInPc3
<g/>
,	,	kIx,
tak	tak	k6eAd1
u	u	k7c2
plášťů	plášť	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bolerko	bolerko	k1gNnSc1
se	se	k3xPyFc4
oblékalo	oblékat	k5eAaImAgNnS
přes	přes	k7c4
živůtek	živůtek	k1gInSc4
<g/>
,	,	kIx,
bývalo	bývat	k5eAaImAgNnS
ušito	ušít	k5eAaPmNgNnS
ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
jako	jako	k8xS,k8xC
šaty	šat	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
protektorátu	protektorát	k1gInSc2
byly	být	k5eAaImAgInP
oblíbeným	oblíbený	k2eAgInSc7d1
doplňkem	doplněk	k1gInSc7
společenského	společenský	k2eAgInSc2d1
oděvu	oděv	k1gInSc2
kožešinové	kožešinový	k2eAgInPc1d1
kabátky	kabátek	k1gInPc1
<g/>
,	,	kIx,
peleríny	pelerína	k1gFnPc1
a	a	k8xC
přehozy	přehoz	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pelerína	pelerína	k1gFnSc1
<g/>
,	,	kIx,
1936	#num#	k4
</s>
<s>
Módní	módní	k2eAgFnSc7d1
novinkou	novinka	k1gFnSc7
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
volná	volný	k2eAgFnSc1d1
paleta	paleta	k1gFnSc1
<g/>
,	,	kIx,
tříčtvrteční	tříčtvrteční	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paleta	paleta	k1gFnSc1
měla	mít	k5eAaImAgFnS
oblá	oblý	k2eAgNnPc1d1
ramena	rameno	k1gNnPc4
<g/>
,	,	kIx,
řasené	řasený	k2eAgInPc4d1
rukávy	rukáv	k1gInPc4
a	a	k8xC
volný	volný	k2eAgInSc4d1
střih	střih	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
elegantní	elegantní	k2eAgFnPc1d1
splývavé	splývavý	k2eAgFnPc1d1
peleríny	pelerína	k1gFnPc1
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yIgFnPc3,k3yRgFnPc3,k3yQgFnPc3
se	se	k3xPyFc4
nosily	nosit	k5eAaImAgFnP
bílé	bílý	k2eAgFnPc1d1
rukavičky	rukavička	k1gFnPc1
a	a	k8xC
maličké	maličký	k2eAgFnPc1d1
kabelky	kabelka	k1gFnPc1
do	do	k7c2
ruky	ruka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hnutí	hnutí	k1gNnSc1
hippies	hippiesa	k1gFnPc2
je	být	k5eAaImIp3nS
především	především	k9
spojováno	spojovat	k5eAaImNgNnS
s	s	k7c7
naprostou	naprostý	k2eAgFnSc7d1
svobodou	svoboda	k1gFnSc7
<g/>
,	,	kIx,
rozmanitosti	rozmanitost	k1gFnSc6
a	a	k8xC
kreativnosti	kreativnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
oblékání	oblékání	k1gNnSc4
byly	být	k5eAaImAgInP
typické	typický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
–	–	k?
čelenky	čelenka	k1gFnSc2
<g/>
,	,	kIx,
džíny	džíny	k1gFnPc1
do	do	k7c2
zvonu	zvon	k1gInSc2
<g/>
,	,	kIx,
květinové	květinový	k2eAgInPc1d1
vzory	vzor	k1gInPc1
<g/>
,	,	kIx,
indiánské	indiánský	k2eAgFnPc1d1
peleríny	pelerína	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pončo	pončo	k1gNnSc1
(	(	kIx(
<g/>
poncho	poncho	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oděv	oděv	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
původně	původně	k6eAd1
nosili	nosit	k5eAaImAgMnP
obyvatelé	obyvatel	k1gMnPc1
jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblékali	oblékat	k5eAaImAgMnP
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
muži	muž	k1gMnPc1
i	i	k8xC
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
vzory	vzor	k1gInPc1
vyobrazovaly	vyobrazovat	k5eAaImAgInP
kmenovou	kmenový	k2eAgFnSc4d1
či	či	k8xC
rodinnou	rodinný	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
jako	jako	k9
módní	módní	k2eAgInSc4d1
trend	trend	k1gInSc4
objevilo	objevit	k5eAaPmAgNnS
pončo	pončo	k1gNnSc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
pelerínou	pelerína	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
stále	stále	k6eAd1
na	na	k7c6
výsluní	výsluní	k1gNnSc6
(	(	kIx(
<g/>
zvlášť	zvlášť	k6eAd1
v	v	k7c6
chladném	chladný	k2eAgNnSc6d1
počasí	počasí	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
variant	varianta	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
pléd	pléd	k1gInSc1
<g/>
,	,	kIx,
pelerína	pelerína	k1gFnSc1
<g/>
,	,	kIx,
pončo	pončo	k1gNnSc1
přes	přes	k7c4
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
přes	přes	k7c4
ramena	rameno	k1gNnPc4
<g/>
,	,	kIx,
se	s	k7c7
zipem	zip	k1gInSc7
či	či	k8xC
na	na	k7c4
knoflíky	knoflík	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
buď	buď	k8xC
hladký	hladký	k2eAgInSc4d1
lem	lem	k1gInSc4
<g/>
,	,	kIx,
anebo	anebo	k8xC
je	být	k5eAaImIp3nS
zdobené	zdobený	k2eAgFnPc1d1
třásněmi	třáseň	k1gFnPc7
<g/>
,	,	kIx,
nosí	nosit	k5eAaImIp3nP
se	se	k3xPyFc4
k	k	k7c3
přiléhavým	přiléhavý	k2eAgFnPc3d1
kalhotám	kalhoty	k1gFnPc3
<g/>
,	,	kIx,
úzkým	úzký	k2eAgFnPc3d1
džínám	džíny	k1gFnPc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
k	k	k7c3
pouzdrové	pouzdrový	k2eAgFnSc3d1
sukni	sukně	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
hodící	hodící	k2eAgFnSc1d1
se	se	k3xPyFc4
k	k	k7c3
vlněnému	vlněný	k2eAgInSc3d1
plédu	pléd	k1gInSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
plstěný	plstěný	k2eAgInSc1d1
klobouk	klobouk	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
pelerínu	pelerína	k1gFnSc4
mohou	moct	k5eAaImIp3nP
nosit	nosit	k5eAaImF
i	i	k9
ženy	žena	k1gFnPc1
s	s	k7c7
nějakým	nějaký	k3yIgInSc7
tím	ten	k3xDgNnSc7
kilem	kilo	k1gNnSc7
navíc	navíc	k6eAd1
<g/>
,	,	kIx,
oděv	oděv	k1gInSc1
musí	muset	k5eAaImIp3nS
sedět	sedět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
příliš	příliš	k6eAd1
obepínat	obepínat	k5eAaImF
ani	ani	k8xC
být	být	k5eAaImF
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vlněná	vlněný	k2eAgFnSc1d1
pelerína	pelerína	k1gFnSc1
<g/>
,	,	kIx,
nošená	nošený	k2eAgFnSc1d1
na	na	k7c4
mnoho	mnoho	k4c4
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
hitových	hitový	k2eAgFnPc6d1
kolekcích	kolekce	k1gFnPc6
ready-to-wear	ready-to-weara	k1gFnPc2
Marca	Marca	k1gFnSc1
Jacobse	Jacobse	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
značku	značka	k1gFnSc4
Louis	louis	k1gInSc2
Vuitton	Vuitton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelerína	pelerína	k1gFnSc1
s	s	k7c7
kapucí	kapuce	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
zapínáním	zapínání	k1gNnSc7
na	na	k7c4
kožené	kožený	k2eAgInPc4d1
pásky	pásek	k1gInPc4
s	s	k7c7
přezkami	přezka	k1gFnPc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
černé	černý	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
ní	on	k3xPp3gFnSc3
vesta	vesta	k1gFnSc1
z	z	k7c2
liščí	liščí	k2eAgFnSc2d1
kožešiny	kožešina	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
lze	lze	k6eAd1
nosit	nosit	k5eAaImF
pod	pod	k7c7
pelerínou	pelerína	k1gFnSc7
<g/>
,	,	kIx,
i	i	k8xC
na	na	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barevnými	barevný	k2eAgFnPc7d1
kombinacemi	kombinace	k1gFnPc7
jsou	být	k5eAaImIp3nP
červená	červený	k2eAgFnSc1d1
–	–	k?
černá	černý	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
–	–	k?
bílá	bílé	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Pelerína	pelerína	k1gFnSc1
<g/>
,	,	kIx,
široká	široký	k2eAgFnSc1d1
pláštěnka	pláštěnka	k1gFnSc1
bez	bez	k7c2
rukávů	rukáv	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
jakými	jaký	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
jsou	být	k5eAaImIp3nP
např.	např.	kA
sport	sport	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
jako	jako	k9
pracovní	pracovní	k2eAgInSc1d1
oděv	oděv	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Většinou	většina	k1gFnSc7
jsou	být	k5eAaImIp3nP
ze	z	k7c2
syntetického	syntetický	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
igelitu	igelit	k1gInSc2
<g/>
,	,	kIx,
Nylidu	Nylid	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
lze	lze	k6eAd1
složit	složit	k5eAaPmF
od	od	k7c2
malého	malý	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
oblíbené	oblíbený	k2eAgFnPc1d1
při	při	k7c6
cestách	cesta	k1gFnPc6
do	do	k7c2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jezdeckých	jezdecký	k2eAgInPc6d1
portrétech	portrét	k1gInPc6
královen	královna	k1gFnPc2
a	a	k8xC
šlechtičen	šlechtična	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
dobovém	dobový	k2eAgInSc6d1
oděvu	oděv	k1gInSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jezdecká	jezdecký	k2eAgFnSc1d1
móda	móda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
také	také	k6eAd1
podřizovala	podřizovat	k5eAaImAgFnS
aktuálním	aktuální	k2eAgInPc3d1
trendům	trend	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
samozřejmostí	samozřejmost	k1gFnPc2
byla	být	k5eAaImAgFnS
pelerína	pelerína	k1gFnSc1
nebo	nebo	k8xC
pláštěnka	pláštěnka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pelerína	pelerína	k1gFnSc1
knížete	kníže	k1gMnSc2
Čolokašviliho	Čolokašvili	k1gMnSc2
<g/>
,	,	kIx,
1956	#num#	k4
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1
pláštěnka	pláštěnka	k1gFnSc1
s	s	k7c7
kapucí	kapuce	k1gFnSc7
<g/>
,	,	kIx,
tzv.	tzv.	kA
bašlyk	bašlyk	k1gInSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
také	také	k9
částí	část	k1gFnSc7
oděvu	oděv	k1gInSc2
kozáků	kozák	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nosila	nosit	k5eAaImAgFnS
se	se	k3xPyFc4
přes	přes	k7c4
ramena	rameno	k1gNnPc4
<g/>
,	,	kIx,
na	na	k7c6
hrudi	hruď	k1gFnSc6
byla	být	k5eAaImAgFnS
volně	volně	k6eAd1
sepnutá	sepnutý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
se	se	k3xPyFc4
nosila	nosit	k5eAaImAgFnS
přehozená	přehozený	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
zimě	zima	k1gFnSc6
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
zabalení	zabalení	k1gNnSc3
hlavy	hlava	k1gFnSc2
a	a	k8xC
krku	krk	k1gInSc2
<g/>
,	,	kIx,
přes	přes	k7c4
kožešinovou	kožešinový	k2eAgFnSc4d1
čepici	čepice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Pè	Pè	k1gInSc5
na	na	k7c6
francouzské	francouzský	k2eAgFnSc3d1
Wikipedii	Wikipedie	k1gFnSc3
a	a	k8xC
Pelerine	Pelerin	k1gInSc5
na	na	k7c6
německé	německý	k2eAgFnSc3d1
Wikipedii	Wikipedie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bety	Bety	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.bety.cz	www.bety.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Škola	škola	k1gFnSc1
textilu	textil	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Móda	móda	k1gFnSc1
v	v	k7c6
ulicích	ulice	k1gFnPc6
protektorátu	protektorát	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
www.npu.cz	www.npu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Publikace	publikace	k1gFnSc1
textil	textil	k1gInSc1
-	-	kIx~
pedagogická	pedagogický	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
emuzeum	emuzeum	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Curia	curium	k1gNnSc2
Vitkov	Vitkov	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
curiavitkov	curiavitkov	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MAREŠOVA	Marešův	k2eAgFnSc1d1
DIVADELNÍ	divadelní	k2eAgFnSc1d1
SPOLEČNOST	společnost	k1gFnSc1
<g/>
.	.	kIx.
marediv	maredit	k5eAaPmDgInS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Starokatolická	starokatolický	k2eAgFnSc1d1
farní	farní	k2eAgFnSc1d1
obec	obec	k1gFnSc1
Brno	Brno	k1gNnSc4
<g/>
.	.	kIx.
starokatolici-brno	starokatolici-brno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
-	-	kIx~
Historické	historický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNOVÁ	Buriánová	k1gFnSc1
<g/>
,	,	kIx,
Miroslava	Miroslava	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
v	v	k7c6
ulicích	ulice	k1gFnPc6
protektorátu	protektorát	k1gInSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Grada	Grada	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ABSOLVENTSKÁ	absolventský	k2eAgFnSc1d1
PRÁCE	práce	k1gFnSc1
<g/>
,	,	kIx,
Vývoj	vývoj	k1gInSc1
módy	móda	k1gFnSc2
za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
100	#num#	k4
let	léto	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Styl	styl	k1gInSc1
<g/>
.	.	kIx.
<g/>
instory	instor	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
styl	styl	k1gInSc1
<g/>
.	.	kIx.
<g/>
instory	instor	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Móda	móda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.moda.cz	www.moda.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pracovní	pracovní	k2eAgInPc4d1
oděvy	oděv	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
pracovni-odev	pracovni-odev	k1gFnSc1
<g/>
.	.	kIx.
<g/>
heureka	heureka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
100	#num#	k4
plus	plus	k6eAd1
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.stoplusjednicka.cz	www.stoplusjednicka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
STANISLAV	Stanislav	k1gMnSc1
A.	A.	kA
AUSKÝ	AUSKÝ	kA
[	[	kIx(
<g/>
KOZÁCI	Kozák	k1gMnPc1
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mozetta	Mozetta	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
pelerína	pelerína	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
pelerína	pelerína	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
