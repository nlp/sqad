<s>
Pelerína	pelerína	k1gFnSc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
pellerine	pellerin	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
volně	volně	k6eAd1
splývající	splývající	k2eAgInSc1d1
oděv	oděv	k1gInSc1
<g/>
,	,	kIx,
bez	bez	k7c2
rukávů	rukáv	k1gInPc2
a	a	k8xC
kapes	kapsa	k1gFnPc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
s	s	k7c7
průhmatem	průhmat	k1gInSc7
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
lištou	lišta	k1gFnSc7
krytý	krytý	k2eAgInSc1d1
otvor	otvor	k1gInSc1
v	v	k7c6
předním	přední	k2eAgInSc6d1
dílu	díl	k1gInSc6
oděvu	oděv	k1gInSc2
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
provléknutí	provléknutí	k1gNnSc3
ruky	ruka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>