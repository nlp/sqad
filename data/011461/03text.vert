<p>
<s>
Ponorka	ponorka	k1gFnSc1	ponorka
je	být	k5eAaImIp3nS	být
plavidlo	plavidlo	k1gNnSc4	plavidlo
schopné	schopný	k2eAgNnSc4d1	schopné
ponoru	ponor	k1gInSc6	ponor
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
ovládání	ovládání	k1gNnSc2	ovládání
pohybu	pohyb	k1gInSc2	pohyb
i	i	k8xC	i
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
mohou	moct	k5eAaImIp3nP	moct
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgNnPc2	který
se	se	k3xPyFc4	se
potápěči	potápěč	k1gInPc7	potápěč
nedostanou	dostat	k5eNaPmIp3nP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Ponorka	ponorka	k1gFnSc1	ponorka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
civilní	civilní	k2eAgFnSc1d1	civilní
(	(	kIx(	(
<g/>
vědecká	vědecký	k2eAgFnSc1d1	vědecká
nebo	nebo	k8xC	nebo
turistická	turistický	k2eAgFnSc1d1	turistická
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vojenská	vojenský	k2eAgFnSc1d1	vojenská
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
ponorka	ponorka	k1gFnSc1	ponorka
bývá	bývat	k5eAaImIp3nS	bývat
vyzbrojena	vyzbrojit	k5eAaPmNgFnS	vyzbrojit
širokým	široký	k2eAgInSc7d1	široký
arzenálem	arzenál	k1gInSc7	arzenál
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
torpédy	torpédo	k1gNnPc7	torpédo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
střelami	střela	k1gFnPc7	střela
s	s	k7c7	s
balistickou	balistický	k2eAgFnSc7d1	balistická
nebo	nebo	k8xC	nebo
plochou	plochý	k2eAgFnSc7d1	plochá
dráhou	dráha	k1gFnSc7	dráha
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pokrok	pokrok	k1gInSc1	pokrok
pro	pro	k7c4	pro
ponorky	ponorka	k1gFnPc4	ponorka
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zavedení	zavedení	k1gNnSc4	zavedení
jaderného	jaderný	k2eAgInSc2d1	jaderný
pohonu	pohon	k1gInSc2	pohon
(	(	kIx(	(
<g/>
jaderná	jaderný	k2eAgFnSc1d1	jaderná
ponorka	ponorka	k1gFnSc1	ponorka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podstatně	podstatně	k6eAd1	podstatně
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
jejich	jejich	k3xOp3gInSc1	jejich
akční	akční	k2eAgInSc1d1	akční
rádius	rádius	k1gInSc1	rádius
a	a	k8xC	a
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
je	on	k3xPp3gFnPc4	on
nutnosti	nutnost	k1gFnPc4	nutnost
pravidelného	pravidelný	k2eAgMnSc2d1	pravidelný
a	a	k8xC	a
častého	častý	k2eAgNnSc2d1	časté
vynořování	vynořování	k1gNnSc2	vynořování
(	(	kIx(	(
<g/>
dostatek	dostatek	k1gInSc1	dostatek
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvářet	vytvářet	k5eAaImF	vytvářet
si	se	k3xPyFc3	se
kyslík	kyslík	k1gInSc4	kyslík
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
pohon	pohon	k1gInSc1	pohon
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
drahý	drahý	k2eAgInSc1d1	drahý
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
je	být	k5eAaImIp3nS	být
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
jen	jen	k9	jen
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgFnPc4d1	vojenská
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
jaderná	jaderný	k2eAgFnSc1d1	jaderná
ponorka	ponorka	k1gFnSc1	ponorka
má	mít	k5eAaImIp3nS	mít
zásobu	zásoba	k1gFnSc4	zásoba
paliva	palivo	k1gNnSc2	palivo
již	již	k6eAd1	již
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dodanou	dodaná	k1gFnSc4	dodaná
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokrylo	pokrýt	k5eAaPmAgNnS	pokrýt
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
životnosti	životnost	k1gFnSc2	životnost
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
nijak	nijak	k6eAd1	nijak
doplňovat	doplňovat	k5eAaImF	doplňovat
palivo	palivo	k1gNnSc4	palivo
a	a	k8xC	a
ponorka	ponorka	k1gFnSc1	ponorka
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vydržet	vydržet	k5eAaPmF	vydržet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
téměř	téměř	k6eAd1	téměř
neomezenou	omezený	k2eNgFnSc4d1	neomezená
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
omezení	omezení	k1gNnSc1	omezení
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
zásoby	zásoba	k1gFnPc4	zásoba
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
jiného	jiný	k2eAgNnSc2d1	jiné
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
vybavení	vybavení	k1gNnSc2	vybavení
(	(	kIx(	(
<g/>
pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
toaletní	toaletní	k2eAgFnPc1d1	toaletní
potřeby	potřeba	k1gFnPc1	potřeba
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
podélné	podélný	k2eAgFnSc2d1	podélná
dutiny	dutina	k1gFnSc2	dutina
(	(	kIx(	(
<g/>
vyrovnávací	vyrovnávací	k2eAgFnSc7d1	vyrovnávací
tzv.	tzv.	kA	tzv.
balastní	balastní	k2eAgFnSc2d1	balastní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lze	lze	k6eAd1	lze
naplnit	naplnit	k5eAaPmF	naplnit
vzduchem	vzduch	k1gInSc7	vzduch
nebo	nebo	k8xC	nebo
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ponoření	ponoření	k1gNnSc3	ponoření
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
nádrže	nádrž	k1gFnPc1	nádrž
plní	plnit	k5eAaImIp3nP	plnit
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ponorka	ponorka	k1gFnSc1	ponorka
poháněná	poháněný	k2eAgFnSc1d1	poháněná
lodním	lodní	k2eAgInSc7d1	lodní
šroubem	šroub	k1gInSc7	šroub
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
stabilizována	stabilizován	k2eAgFnSc1d1	stabilizována
i	i	k8xC	i
dynamicky	dynamicky	k6eAd1	dynamicky
pomocí	pomocí	k7c2	pomocí
hloubkových	hloubkový	k2eAgNnPc2d1	hloubkové
kormidel	kormidlo	k1gNnPc2	kormidlo
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ponorka	ponorka	k1gFnSc1	ponorka
mohla	moct	k5eAaImAgFnS	moct
vynořit	vynořit	k5eAaPmF	vynořit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stlačeným	stlačený	k2eAgInSc7d1	stlačený
vzduchem	vzduch	k1gInSc7	vzduch
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
nádrží	nádrž	k1gFnPc2	nádrž
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
ponorkách	ponorka	k1gFnPc6	ponorka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
elektromotory	elektromotor	k1gInPc1	elektromotor
napájené	napájený	k2eAgInPc1d1	napájený
z	z	k7c2	z
akumulátorů	akumulátor	k1gInPc2	akumulátor
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc2d2	veliký
vojenské	vojenský	k2eAgFnSc2d1	vojenská
ponorky	ponorka	k1gFnSc2	ponorka
mají	mít	k5eAaImIp3nP	mít
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgMnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
Angličan	Angličan	k1gMnSc1	Angličan
William	William	k1gInSc1	William
Bourne	Bourn	k1gInSc5	Bourn
objevuje	objevovat	k5eAaImIp3nS	objevovat
balastní	balastní	k2eAgFnSc2d1	balastní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
zakladní	zakladný	k2eAgMnPc1d1	zakladný
stavební	stavební	k2eAgMnPc1d1	stavební
prvek	prvek	k1gInSc1	prvek
všech	všecek	k3xTgFnPc2	všecek
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
historicky	historicky	k6eAd1	historicky
doloženou	doložený	k2eAgFnSc7d1	doložená
ponorkou	ponorka	k1gFnSc7	ponorka
bylo	být	k5eAaImAgNnS	být
plavidlo	plavidlo	k1gNnSc1	plavidlo
zkonstruované	zkonstruovaný	k2eAgNnSc1d1	zkonstruované
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
nizozemským	nizozemský	k2eAgMnSc7d1	nizozemský
konstruktérem	konstruktér	k1gMnSc7	konstruktér
Corneliusem	Cornelius	k1gMnSc7	Cornelius
van	van	k1gInSc4	van
Drebelem	Drebel	k1gInSc7	Drebel
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
konstrukci	konstrukce	k1gFnSc3	konstrukce
údajně	údajně	k6eAd1	údajně
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
expedici	expedice	k1gFnSc6	expedice
k	k	k7c3	k
obleženému	obležený	k2eAgNnSc3d1	obležené
La	la	k1gNnSc3	la
Rochelle	Rochelle	k1gFnSc2	Rochelle
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
člun	člun	k1gInSc4	člun
upravený	upravený	k2eAgInSc1d1	upravený
důkladným	důkladný	k2eAgNnSc7d1	důkladné
utěsněním	utěsnění	k1gNnSc7	utěsnění
a	a	k8xC	a
nahrazením	nahrazení	k1gNnSc7	nahrazení
paluby	paluba	k1gFnSc2	paluba
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
člun	člun	k1gInSc1	člun
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
obveselení	obveselení	k1gNnSc3	obveselení
londýnské	londýnský	k2eAgFnSc2d1	londýnská
smetánky	smetánka	k1gFnSc2	smetánka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
konstrukce	konstrukce	k1gFnSc1	konstrukce
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
válečné	válečný	k2eAgInPc4d1	válečný
účely	účel	k1gInPc4	účel
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
panování	panování	k1gNnSc2	panování
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Konstruktérem	konstruktér	k1gMnSc7	konstruktér
byl	být	k5eAaImAgInS	být
Jefin	Jefin	k2eAgInSc1d1	Jefin
Nikonov	Nikonov	k1gInSc1	Nikonov
a	a	k8xC	a
s	s	k7c7	s
ponorkou	ponorka	k1gFnSc7	ponorka
prý	prý	k9	prý
provedl	provést	k5eAaPmAgMnS	provést
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
ponoření	ponoření	k1gNnPc2	ponoření
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
při	při	k7c6	při
předvádění	předvádění	k1gNnSc6	předvádění
panovníkovi	panovník	k1gMnSc3	panovník
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
ponorka	ponorka	k1gFnSc1	ponorka
potopila	potopit	k5eAaPmAgFnS	potopit
poněkud	poněkud	k6eAd1	poněkud
definitivněji	definitivně	k6eAd2	definitivně
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Petra	Petr	k1gMnSc2	Petr
I	i	k8xC	i
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
admiralitou	admiralita	k1gFnSc7	admiralita
ukončen	ukončit	k5eAaPmNgMnS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgInSc1d1	funkční
bojově	bojově	k6eAd1	bojově
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
ponorkou	ponorka	k1gFnSc7	ponorka
byla	být	k5eAaImAgFnS	být
ponorka	ponorka	k1gFnSc1	ponorka
Turtle	Turtle	k1gFnSc1	Turtle
(	(	kIx(	(
<g/>
Želva	želva	k1gFnSc1	želva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestrojená	sestrojený	k2eAgFnSc1d1	sestrojená
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
Davidem	David	k1gMnSc7	David
Bushnellem	Bushnell	k1gMnSc7	Bushnell
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrhem	návrh	k1gInSc7	návrh
ponorek	ponorka	k1gFnPc2	ponorka
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Robert	Roberta	k1gFnPc2	Roberta
Fulton	Fulton	k1gInSc1	Fulton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
ponorku	ponorka	k1gFnSc4	ponorka
Nautilus	Nautilus	k1gMnSc1	Nautilus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
předvedl	předvést	k5eAaPmAgMnS	předvést
první	první	k4xOgInSc4	první
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
cvičný	cvičný	k2eAgInSc4d1	cvičný
útok	útok	k1gInSc4	útok
a	a	k8xC	a
potopení	potopení	k1gNnSc4	potopení
čtyřicetitunové	čtyřicetitunový	k2eAgFnSc2d1	čtyřicetitunová
šalupy	šalupa	k1gFnSc2	šalupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
zabýval	zabývat	k5eAaImAgMnS	zabývat
stavbou	stavba	k1gFnSc7	stavba
ponorek	ponorka	k1gFnPc2	ponorka
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
porouchané	porouchaný	k2eAgFnSc2d1	porouchaná
ponorky	ponorka	k1gFnSc2	ponorka
Brandtaucher	Brandtauchra	k1gFnPc2	Brandtauchra
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
provést	provést	k5eAaPmF	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
první	první	k4xOgFnSc4	první
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
záchranu	záchrana	k1gFnSc4	záchrana
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
nasazením	nasazení	k1gNnSc7	nasazení
ponorky	ponorka	k1gFnSc2	ponorka
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
boji	boj	k1gInSc6	boj
bylo	být	k5eAaImAgNnS	být
nasazení	nasazení	k1gNnSc1	nasazení
konfederační	konfederační	k2eAgFnSc2d1	konfederační
ponorky	ponorka	k1gFnSc2	ponorka
Hunley	Hunlea	k1gFnSc2	Hunlea
proti	proti	k7c3	proti
unijní	unijní	k2eAgFnSc3d1	unijní
obrněné	obrněný	k2eAgFnSc3d1	obrněná
lodi	loď	k1gFnSc3	loď
Houstanic	Houstanice	k1gFnPc2	Houstanice
<g/>
.	.	kIx.	.
</s>
<s>
Unijní	unijní	k2eAgFnSc1d1	unijní
loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
minou	mina	k1gFnSc7	mina
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
tyči	tyč	k1gFnSc6	tyč
<g/>
,	,	kIx,	,
ponorka	ponorka	k1gFnSc1	ponorka
však	však	k9	však
byla	být	k5eAaImAgFnS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
poté	poté	k6eAd1	poté
co	co	k8xS	co
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
udusila	udusit	k5eAaPmAgFnS	udusit
když	když	k8xS	když
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c4	na
proud	proud	k1gInSc4	proud
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
návrat	návrat	k1gInSc1	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc4	léto
neznáma	neznámo	k1gNnSc2	neznámo
<g/>
.	.	kIx.	.
</s>
<s>
Ponorka	ponorka	k1gFnSc1	ponorka
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2000	[number]	k4	2000
vyzdvižena	vyzdvihnout	k5eAaPmNgFnS	vyzdvihnout
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
při	při	k7c6	při
porovnání	porovnání	k1gNnSc6	porovnání
ceny	cena	k1gFnSc2	cena
ponorného	ponorný	k2eAgInSc2d1	ponorný
člunu	člun	k1gInSc2	člun
a	a	k8xC	a
moderní	moderní	k2eAgFnSc2d1	moderní
unijní	unijní	k2eAgFnSc2d1	unijní
obrněné	obrněný	k2eAgFnSc2d1	obrněná
lodi	loď	k1gFnSc2	loď
jde	jít	k5eAaImIp3nS	jít
nepochybně	pochybně	k6eNd1	pochybně
o	o	k7c4	o
první	první	k4xOgInSc4	první
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nepřítele	nepřítel	k1gMnSc4	nepřítel
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
útočníka	útočník	k1gMnSc4	útočník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
desítky	desítka	k1gFnPc1	desítka
ponorek	ponorka	k1gFnPc2	ponorka
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
konstrukcí	konstrukce	k1gFnPc2	konstrukce
i	i	k8xC	i
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
konstruktér	konstruktér	k1gMnSc1	konstruktér
John	John	k1gMnSc1	John
Philip	Philip	k1gMnSc1	Philip
Holland	Hollando	k1gNnPc2	Hollando
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
ponorka	ponorka	k1gFnSc1	ponorka
byla	být	k5eAaImAgFnS	být
americkému	americký	k2eAgInSc3d1	americký
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
dodána	dodán	k2eAgFnSc1d1	dodána
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
a	a	k8xC	a
nesla	nést	k5eAaImAgFnS	nést
jméno	jméno	k1gNnSc4	jméno
Plunger	Plungra	k1gFnPc2	Plungra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc7	první
hromadně	hromadně	k6eAd1	hromadně
nasadilo	nasadit	k5eAaPmAgNnS	nasadit
ponorky	ponorka	k1gFnSc2	ponorka
Německo	Německo	k1gNnSc1	Německo
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Využilo	využít	k5eAaPmAgNnS	využít
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
narušování	narušování	k1gNnSc3	narušování
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
tras	trasa	k1gFnPc2	trasa
spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
britským	britský	k2eAgFnPc3d1	britská
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
efektivita	efektivita	k1gFnSc1	efektivita
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
prokázala	prokázat	k5eAaPmAgFnS	prokázat
již	již	k6eAd1	již
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
německá	německý	k2eAgFnSc1d1	německá
ponorka	ponorka	k1gFnSc1	ponorka
U	u	k7c2	u
9	[number]	k4	9
dokázala	dokázat	k5eAaPmAgFnS	dokázat
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
potopit	potopit	k5eAaPmF	potopit
tři	tři	k4xCgInPc1	tři
britské	britský	k2eAgInPc1d1	britský
pancéřové	pancéřový	k2eAgInPc1d1	pancéřový
křižníky	křižník	k1gInPc1	křižník
(	(	kIx(	(
<g/>
Abourkir	Abourkir	k1gInSc1	Abourkir
<g/>
,	,	kIx,	,
Hogue	Hogu	k1gInPc1	Hogu
a	a	k8xC	a
Cressy	Cress	k1gInPc1	Cress
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velitelem	velitel	k1gMnSc7	velitel
této	tento	k3xDgFnSc2	tento
ponorky	ponorka	k1gFnSc2	ponorka
byl	být	k5eAaImAgMnS	být
Otto	Otto	k1gMnSc1	Otto
Weddigen	Weddigen	k1gInSc4	Weddigen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
získal	získat	k5eAaPmAgMnS	získat
nechtěné	chtěný	k2eNgNnSc4d1	nechtěné
prvenství	prvenství	k1gNnSc4	prvenství
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
U	u	k7c2	u
29	[number]	k4	29
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
velel	velet	k5eAaImAgInS	velet
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc4	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
jedinou	jediný	k2eAgFnSc7d1	jediná
<g/>
)	)	kIx)	)
ponorkou	ponorka	k1gFnSc7	ponorka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
potopena	potopit	k5eAaPmNgFnS	potopit
bitevní	bitevní	k2eAgFnSc7d1	bitevní
lodí	loď	k1gFnSc7	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
však	však	k9	však
využívaly	využívat	k5eAaPmAgFnP	využívat
k	k	k7c3	k
bojové	bojový	k2eAgFnSc3d1	bojová
činnosti	činnost	k1gFnSc3	činnost
i	i	k8xC	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
důležité	důležitý	k2eAgInPc1d1	důležitý
přímořské	přímořský	k2eAgInPc1d1	přímořský
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
účastnící	účastnící	k2eAgInPc4d1	účastnící
se	se	k3xPyFc4	se
bojů	boj	k1gInPc2	boj
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Britové	Brit	k1gMnPc1	Brit
přes	přes	k7c4	přes
horší	zlý	k2eAgFnSc4d2	horší
kvalitu	kvalita	k1gFnSc4	kvalita
ponorek	ponorka	k1gFnPc2	ponorka
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
úspěšně	úspěšně	k6eAd1	úspěšně
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
například	například	k6eAd1	například
proti	proti	k7c3	proti
Turecku	Turecko	k1gNnSc3	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
byl	být	k5eAaImAgInS	být
velitel	velitel	k1gMnSc1	velitel
Martin	Martin	k1gMnSc1	Martin
E.	E.	kA	E.
Nasmith	Nasmith	k1gMnSc1	Nasmith
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
během	během	k7c2	během
Dardanelské	dardanelský	k2eAgFnSc2d1	dardanelský
operace	operace	k1gFnSc2	operace
potopil	potopit	k5eAaPmAgMnS	potopit
Turecku	Turecko	k1gNnSc3	Turecko
v	v	k7c6	v
Marmanském	Marmanský	k2eAgNnSc6d1	Marmanský
moři	moře	k1gNnSc6	moře
11	[number]	k4	11
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
ponorek	ponorka	k1gFnPc2	ponorka
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
rychle	rychle	k6eAd1	rychle
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
razantní	razantní	k2eAgFnSc7d1	razantní
reakcí	reakce	k1gFnSc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
jejich	jejich	k3xOp3gMnSc7	jejich
hlavním	hlavní	k2eAgMnSc7d1	hlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
maskované	maskovaný	k2eAgFnSc2d1	maskovaná
válečné	válečný	k2eAgFnSc2d1	válečná
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
lehké	lehký	k2eAgInPc1d1	lehký
útočné	útočný	k2eAgInPc1d1	útočný
čluny	člun	k1gInPc1	člun
a	a	k8xC	a
torpedoborce	torpedoborka	k1gFnSc3	torpedoborka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
válečného	válečný	k2eAgNnSc2d1	válečné
letectva	letectvo	k1gNnSc2	letectvo
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
nepřítel	nepřítel	k1gMnSc1	nepřítel
ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgMnSc1d2	horší
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1916	[number]	k4	1916
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
ponorka	ponorka	k1gFnSc1	ponorka
Foucault	Foucaulta	k1gFnPc2	Foucaulta
napadena	napaden	k2eAgFnSc1d1	napadena
a	a	k8xC	a
potopena	potopen	k2eAgFnSc1d1	potopena
dvěma	dva	k4xCgInPc7	dva
rakouskými	rakouský	k2eAgInPc7d1	rakouský
létajícími	létající	k2eAgInPc7d1	létající
čluny	člun	k1gInPc7	člun
typu	typ	k1gInSc2	typ
Lohner	Lohner	k1gInSc1	Lohner
(	(	kIx(	(
<g/>
L	L	kA	L
132	[number]	k4	132
a	a	k8xC	a
L	L	kA	L
135	[number]	k4	135
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
ponorkou	ponorka	k1gFnSc7	ponorka
potopenou	potopený	k2eAgFnSc7d1	potopená
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
leteckým	letecký	k2eAgInSc7d1	letecký
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
uzavřen	uzavřen	k2eAgInSc1d1	uzavřen
Versailleský	versailleský	k2eAgInSc1d1	versailleský
mír	mír	k1gInSc1	mír
mezi	mezi	k7c7	mezi
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
Spojenci	spojenec	k1gMnPc7	spojenec
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
Němci	Němec	k1gMnPc1	Němec
předat	předat	k5eAaPmF	předat
všechny	všechen	k3xTgFnPc4	všechen
ponorky	ponorka	k1gFnPc4	ponorka
vítězům	vítěz	k1gMnPc3	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
příměří	příměří	k1gNnSc2	příměří
dále	daleko	k6eAd2	daleko
výslovně	výslovně	k6eAd1	výslovně
stanovily	stanovit	k5eAaPmAgFnP	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
provozuschopné	provozuschopný	k2eAgFnPc1d1	provozuschopná
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
předány	předat	k5eAaPmNgInP	předat
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
výzbrojí	výzbroj	k1gFnSc7	výzbroj
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
budou	být	k5eAaImBp3nP	být
odzbrojeny	odzbrojit	k5eAaPmNgFnP	odzbrojit
a	a	k8xC	a
umístěny	umístit	k5eAaPmNgFnP	umístit
do	do	k7c2	do
doků	dok	k1gInPc2	dok
<g/>
.	.	kIx.	.
</s>
<s>
Předáno	předán	k2eAgNnSc1d1	předáno
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
176	[number]	k4	176
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
zredukovaly	zredukovat	k5eAaPmAgFnP	zredukovat
německé	německý	k2eAgNnSc4d1	německé
loďstvo	loďstvo	k1gNnSc4	loďstvo
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
nejnutnější	nutný	k2eAgFnPc4d3	nejnutnější
složky	složka	k1gFnPc4	složka
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
vlastnit	vlastnit	k5eAaImF	vlastnit
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
většina	většina	k1gFnSc1	většina
ponorek	ponorka	k1gFnPc2	ponorka
v	v	k7c6	v
diesel-elektrickém	diesellektrický	k2eAgNnSc6d1	diesel-elektrický
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
výjimek	výjimka	k1gFnPc2	výjimka
byly	být	k5eAaImAgFnP	být
britské	britský	k2eAgFnPc1d1	britská
ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
používaly	používat	k5eAaImAgFnP	používat
kombinaci	kombinace	k1gFnSc4	kombinace
benzínového	benzínový	k2eAgMnSc2d1	benzínový
a	a	k8xC	a
elektrického	elektrický	k2eAgInSc2d1	elektrický
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
častějším	častý	k2eAgInPc3d2	častější
požárům	požár	k1gInPc3	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
opět	opět	k6eAd1	opět
nasadilo	nasadit	k5eAaPmAgNnS	nasadit
stovky	stovka	k1gFnPc4	stovka
ponorek	ponorka	k1gFnPc2	ponorka
operujících	operující	k2eAgFnPc2d1	operující
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Taktika	taktika	k1gFnSc1	taktika
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
účinnější	účinný	k2eAgFnSc1d2	účinnější
než	než	k8xS	než
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
operovaly	operovat	k5eAaImAgFnP	operovat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
odhalení	odhalení	k1gNnSc2	odhalení
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
konvoje	konvoj	k1gInSc2	konvoj
se	se	k3xPyFc4	se
seskupily	seskupit	k5eAaPmAgFnP	seskupit
do	do	k7c2	do
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
vlčí	vlčí	k2eAgFnSc2d1	vlčí
smečky	smečka	k1gFnSc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
vážně	vážně	k6eAd1	vážně
ohrozily	ohrozit	k5eAaPmAgFnP	ohrozit
zásobování	zásobování	k1gNnSc4	zásobování
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Zavedením	zavedení	k1gNnSc7	zavedení
konvojů	konvoj	k1gInPc2	konvoj
<g/>
,	,	kIx,	,
leteckého	letecký	k2eAgNnSc2d1	letecké
hlídkování	hlídkování	k1gNnSc2	hlídkování
a	a	k8xC	a
nových	nový	k2eAgFnPc2d1	nová
protiponorkových	protiponorkový	k2eAgFnPc2d1	protiponorková
zbraní	zbraň	k1gFnPc2	zbraň
byla	být	k5eAaImAgFnS	být
však	však	k9	však
jejich	jejich	k3xOp3gFnSc1	jejich
hrozba	hrozba	k1gFnSc1	hrozba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
silně	silně	k6eAd1	silně
utlumena	utlumit	k5eAaPmNgFnS	utlumit
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
ponorek	ponorka	k1gFnPc2	ponorka
však	však	k9	však
trvaly	trvat	k5eAaImAgInP	trvat
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
fáze	fáze	k1gFnSc1	fáze
bojů	boj	k1gInPc2	boj
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
velkoadmirál	velkoadmirál	k1gMnSc1	velkoadmirál
Karl	Karl	k1gMnSc1	Karl
Dönitz	Dönitz	k1gMnSc1	Dönitz
nařídil	nařídit	k5eAaPmAgMnS	nařídit
kapitulaci	kapitulace	k1gFnSc4	kapitulace
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
operovalo	operovat	k5eAaImAgNnS	operovat
49	[number]	k4	49
německých	německý	k2eAgFnPc2d1	německá
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
postavilo	postavit	k5eAaPmAgNnS	postavit
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
celkem	celkem	k6eAd1	celkem
1162	[number]	k4	1162
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
zbraní	zbraň	k1gFnSc7	zbraň
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
zejména	zejména	k9	zejména
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
na	na	k7c6	na
pacifickém	pacifický	k2eAgNnSc6d1	pacifické
bojišti	bojiště	k1gNnSc6	bojiště
hlavně	hlavně	k9	hlavně
USA	USA	kA	USA
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
vývoji	vývoj	k1gInSc3	vývoj
na	na	k7c6	na
ponorkovém	ponorkový	k2eAgNnSc6d1	ponorkové
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
byly	být	k5eAaImAgFnP	být
ponorky	ponorka	k1gFnPc1	ponorka
téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xS	jako
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
stala	stát	k5eAaPmAgFnS	stát
skutečná	skutečný	k2eAgNnPc1d1	skutečné
podmořská	podmořský	k2eAgNnPc1d1	podmořské
plavidla	plavidlo	k1gNnPc1	plavidlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
Schnorchelu	Schnorchel	k1gInSc3	Schnorchel
nemusela	muset	k5eNaImAgFnS	muset
vynořovat	vynořovat	k5eAaImF	vynořovat
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
pestrou	pestrý	k2eAgFnSc4d1	pestrá
škálu	škála	k1gFnSc4	škála
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
torpéda	torpédo	k1gNnPc1	torpédo
klasická	klasický	k2eAgNnPc1d1	klasické
<g/>
,	,	kIx,	,
samonaváděcí	samonaváděcí	k2eAgNnSc1d1	samonaváděcí
<g/>
,	,	kIx,	,
miny	mina	k1gFnPc1	mina
a	a	k8xC	a
děla	dělo	k1gNnPc1	dělo
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
experimentovalo	experimentovat	k5eAaImAgNnS	experimentovat
s	s	k7c7	s
odpáleními	odpálení	k1gNnPc7	odpálení
raket	raketa	k1gFnPc2	raketa
či	či	k8xC	či
přepravou	přeprava	k1gFnSc7	přeprava
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ponorky	ponorka	k1gFnPc1	ponorka
opět	opět	k6eAd1	opět
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
"	"	kIx"	"
<g/>
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zachování	zachování	k1gNnSc4	zachování
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
USA	USA	kA	USA
i	i	k8xC	i
SSSR	SSSR	kA	SSSR
snažily	snažit	k5eAaImAgFnP	snažit
vyrobit	vyrobit	k5eAaPmF	vyrobit
účinnější	účinný	k2eAgFnPc4d2	účinnější
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
potřebovaly	potřebovat	k5eAaImAgFnP	potřebovat
znát	znát	k5eAaImF	znát
další	další	k2eAgInPc4d1	další
kroky	krok	k1gInPc4	krok
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
přesuny	přesun	k1gInPc4	přesun
armád	armáda	k1gFnPc2	armáda
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
stále	stále	k6eAd1	stále
tišší	tichý	k2eAgFnPc1d2	tišší
a	a	k8xC	a
tišší	tichý	k2eAgFnPc1d2	tišší
ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
pozorovat	pozorovat	k5eAaImF	pozorovat
protivníka	protivník	k1gMnSc4	protivník
nebo	nebo	k8xC	nebo
připlout	připlout	k5eAaPmF	připlout
nepozorovány	pozorován	k2eNgInPc4d1	nepozorován
až	až	k8xS	až
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
a	a	k8xC	a
odpálit	odpálit	k5eAaPmF	odpálit
jaderné	jaderný	k2eAgFnPc4d1	jaderná
hlavice	hlavice	k1gFnPc4	hlavice
nesené	nesený	k2eAgFnPc4d1	nesená
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
ponorek	ponorka	k1gFnPc2	ponorka
==	==	k?	==
</s>
</p>
<p>
<s>
dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
pohonu	pohon	k1gInSc2	pohon
</s>
</p>
<p>
<s>
ruční	ruční	k2eAgInSc4d1	ruční
-	-	kIx~	-
první	první	k4xOgFnPc1	první
ponorky	ponorka	k1gFnPc1	ponorka
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
parní	parní	k2eAgFnSc1d1	parní
ponorka	ponorka	k1gFnSc1	ponorka
(	(	kIx(	(
<g/>
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
výkonný	výkonný	k2eAgInSc4d1	výkonný
pohon	pohon	k1gInSc4	pohon
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
neosvědčily	osvědčit	k5eNaPmAgFnP	osvědčit
se	s	k7c7	s
-	-	kIx~	-
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
zbytkovým	zbytkový	k2eAgNnSc7d1	zbytkové
teplem	teplo	k1gNnSc7	teplo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
elektroponorka	elektroponorka	k1gFnSc1	elektroponorka
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc1	první
ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jen	jen	k9	jen
u	u	k7c2	u
miniponorek	miniponorka	k1gFnPc2	miniponorka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
benzinová	benzinový	k2eAgFnSc1d1	benzinová
ponorka	ponorka	k1gFnSc1	ponorka
-	-	kIx~	-
jen	jen	k9	jen
několik	několik	k4yIc4	několik
málo	málo	k1gNnSc4	málo
typů	typ	k1gInPc2	typ
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
opuštěno	opuštěn	k2eAgNnSc1d1	opuštěno
pro	pro	k7c4	pro
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
benzinovými	benzinový	k2eAgInPc7d1	benzinový
výpary	výpar	k1gInPc7	výpar
</s>
</p>
<p>
<s>
dieselová	dieselový	k2eAgFnSc1d1	dieselová
ponorka	ponorka	k1gFnSc1	ponorka
</s>
</p>
<p>
<s>
diesel-elektrická	diesellektrický	k2eAgFnSc1d1	diesel-elektrická
ponorka	ponorka	k1gFnSc1	ponorka
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
využívala	využívat	k5eAaPmAgFnS	využívat
akumulátory	akumulátor	k1gInPc4	akumulátor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
ponorka	ponorka	k1gFnSc1	ponorka
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
terminologie	terminologie	k1gFnSc2	terminologie
US	US	kA	US
Navy	Navy	k?	Navy
zkratka	zkratka	k1gFnSc1	zkratka
SSN	SSN	kA	SSN
<g/>
;	;	kIx,	;
dle	dle	k7c2	dle
ruského	ruský	k2eAgNnSc2d1	ruské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
П	П	k?	П
<g/>
,	,	kIx,	,
А	А	k?	А
п	п	k?	п
л	л	k?	л
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ponorka	ponorka	k1gFnSc1	ponorka
na	na	k7c4	na
stlačený	stlačený	k2eAgInSc4d1	stlačený
vzduch	vzduch	k1gInSc4	vzduch
</s>
</p>
<p>
<s>
vojenské	vojenský	k2eAgFnPc1d1	vojenská
ponorky	ponorka	k1gFnPc1	ponorka
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
určení	určení	k1gNnSc2	určení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
stíhací	stíhací	k2eAgFnPc1d1	stíhací
(	(	kIx(	(
<g/>
přepadové	přepadový	k2eAgFnPc1d1	přepadová
neboli	neboli	k8xC	neboli
útočné	útočný	k2eAgFnPc1d1	útočná
<g/>
)	)	kIx)	)
ponorky	ponorka	k1gFnPc1	ponorka
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgFnPc1d1	jaderná
dle	dle	k7c2	dle
terminologie	terminologie	k1gFnSc2	terminologie
US	US	kA	US
Navy	Navy	k?	Navy
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SSN	SSN	kA	SSN
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
ruské	ruský	k2eAgFnSc2d1	ruská
П	П	k?	П
<g/>
,	,	kIx,	,
П	П	k?	П
Л	Л	k?	Л
А	А	k?	А
Т	Т	k?	Т
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nosiče	nosič	k1gInPc4	nosič
strategických	strategický	k2eAgFnPc2d1	strategická
jaderných	jaderný	k2eAgFnPc2d1	jaderná
balistických	balistický	k2eAgFnPc2d1	balistická
střel	střela	k1gFnPc2	střela
(	(	kIx(	(
<g/>
SLBM	SLBM	kA	SLBM
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgInPc1d1	jaderný
dle	dle	k7c2	dle
terminologie	terminologie	k1gFnSc2	terminologie
US	US	kA	US
Navy	Navy	k?	Navy
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SSBN	SSBN	kA	SSBN
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
ruské	ruský	k2eAgFnSc2d1	ruská
П	П	k?	П
<g/>
,	,	kIx,	,
п	п	k?	п
л	л	k?	л
а	а	k?	а
с	с	k?	с
р	р	k?	р
б	б	k?	б
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nosiče	nosič	k1gInPc4	nosič
křižujících	křižující	k2eAgFnPc2d1	křižující
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
SLCM	SLCM	kA	SLCM
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgInPc1d1	jaderný
dle	dle	k7c2	dle
terminologie	terminologie	k1gFnSc2	terminologie
US	US	kA	US
Navy	Navy	k?	Navy
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SSGN	SSGN	kA	SSGN
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
ruské	ruský	k2eAgFnSc2d1	ruská
П	П	k?	П
<g/>
,	,	kIx,	,
п	п	k?	п
л	л	k?	л
а	а	k?	а
с	с	k?	с
р	р	k?	р
к	к	k?	к
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zásobovací	zásobovací	k2eAgFnPc1d1	zásobovací
ponorky	ponorka	k1gFnPc1	ponorka
(	(	kIx(	(
<g/>
používané	používaný	k2eAgInPc1d1	používaný
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
války	válka	k1gFnSc2	válka
k	k	k7c3	k
doplňování	doplňování	k1gNnSc3	doplňování
zásob	zásoba	k1gFnPc2	zásoba
bojových	bojový	k2eAgFnPc2d1	bojová
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
miniponorky	miniponorka	k1gFnPc1	miniponorka
(	(	kIx(	(
<g/>
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
války	válka	k1gFnSc2	válka
používané	používaný	k2eAgFnSc2d1	používaná
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
sabotážním	sabotážní	k2eAgInPc3d1	sabotážní
účelům	účel	k1gInPc3	účel
proti	proti	k7c3	proti
zakotveným	zakotvený	k2eAgFnPc3d1	zakotvená
lodím	loď	k1gFnPc3	loď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výzkumné	výzkumný	k2eAgFnPc1d1	výzkumná
ponorky	ponorka	k1gFnPc1	ponorka
</s>
</p>
<p>
<s>
Batysféra	batysféra	k1gFnSc1	batysféra
-	-	kIx~	-
upoutaná	upoutaný	k2eAgFnSc1d1	upoutaná
kabina	kabina	k1gFnSc1	kabina
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
</s>
</p>
<p>
<s>
Batyskaf	batyskaf	k1gInSc1	batyskaf
-	-	kIx~	-
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
ponorka	ponorka	k1gFnSc1	ponorka
pro	pro	k7c4	pro
ponoření	ponoření	k1gNnSc4	ponoření
do	do	k7c2	do
zvláště	zvláště	k6eAd1	zvláště
velkých	velký	k2eAgFnPc2d1	velká
hloubek	hloubka	k1gFnPc2	hloubka
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
ponorka	ponorka	k1gFnSc1	ponorka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tlakového	tlakový	k2eAgInSc2d1	tlakový
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
a	a	k8xC	a
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ponorek	ponorka	k1gFnPc2	ponorka
nad	nad	k7c4	nad
trup	trup	k1gInSc4	trup
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
velitelská	velitelský	k2eAgFnSc1d1	velitelská
věž	věž	k1gFnSc1	věž
se	s	k7c7	s
vstupním	vstupní	k2eAgInSc7d1	vstupní
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
periskopy	periskop	k1gInPc7	periskop
<g/>
,	,	kIx,	,
anténami	anténa	k1gFnPc7	anténa
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
jiným	jiný	k2eAgNnSc7d1	jiné
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
daný	daný	k2eAgInSc1d1	daný
typ	typ	k1gInSc1	typ
ponorky	ponorka	k1gFnSc2	ponorka
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
vojenské	vojenský	k2eAgFnSc2d1	vojenská
ponorky	ponorka	k1gFnSc2	ponorka
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
nesly	nést	k5eAaImAgInP	nést
na	na	k7c6	na
trupu	trup	k1gInSc6	trup
ještě	ještě	k9	ještě
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
výzbroj	výzbroj	k1gInSc4	výzbroj
(	(	kIx(	(
<g/>
protiletadlové	protiletadlový	k2eAgInPc4d1	protiletadlový
kulomety	kulomet	k1gInPc4	kulomet
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
dělo	dít	k5eAaBmAgNnS	dít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vybavují	vybavovat	k5eAaImIp3nP	vybavovat
ještě	ještě	k9	ještě
druhým	druhý	k4xOgInSc7	druhý
vnějším	vnější	k2eAgInSc7d1	vnější
trupem	trup	k1gInSc7	trup
(	(	kIx(	(
<g/>
double	double	k2eAgInSc4d1	double
hull	hull	k1gInSc4	hull
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
není	být	k5eNaImIp3nS	být
tlakový	tlakový	k2eAgMnSc1d1	tlakový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dává	dávat	k5eAaImIp3nS	dávat
ponorce	ponorka	k1gFnSc3	ponorka
vnější	vnější	k2eAgInSc4d1	vnější
hydrodynamický	hydrodynamický	k2eAgInSc4d1	hydrodynamický
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obvykle	obvykle	k6eAd1	obvykle
balastní	balastní	k2eAgFnSc2d1	balastní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ponorek	ponorka	k1gFnPc2	ponorka
má	mít	k5eAaImIp3nS	mít
sonar	sonar	k1gInSc1	sonar
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Batyskaf	batyskaf	k1gInSc1	batyskaf
</s>
</p>
<p>
<s>
Ponorková	ponorkový	k2eAgFnSc1d1	ponorková
pošta	pošta	k1gFnSc1	pošta
</s>
</p>
<p>
<s>
Ponorková	ponorkový	k2eAgFnSc1d1	ponorková
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ponorka	ponorka	k1gFnSc1	ponorka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ponorka	ponorka	k1gFnSc1	ponorka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Ponorky	ponorka	k1gFnPc1	ponorka
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Německé	německý	k2eAgFnSc2d1	německá
druhoválečné	druhoválečný	k2eAgFnSc2d1	druhoválečná
ponorky	ponorka	k1gFnSc2	ponorka
</s>
</p>
