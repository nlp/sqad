<s>
Svante	Svant	k1gMnSc5	Svant
August	August	k1gMnSc1	August
Arrhenius	Arrhenius	k1gInSc1	Arrhenius
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Vik	vika	k1gFnPc2	vika
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
elektrolytickou	elektrolytický	k2eAgFnSc4d1	elektrolytická
teorii	teorie	k1gFnSc4	teorie
disociace	disociace	k1gFnSc2	disociace
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Státního	státní	k2eAgInSc2d1	státní
institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
rasovou	rasový	k2eAgFnSc4d1	rasová
biologii	biologie	k1gFnSc4	biologie
(	(	kIx(	(
<g/>
Statens	Statens	k1gInSc1	Statens
institut	institut	k1gInSc1	institut
för	för	k?	för
rasbiologi	rasbiolog	k1gFnSc2	rasbiolog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
eugenikou	eugenika	k1gFnSc7	eugenika
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svante	Svant	k1gInSc5	Svant
Arrhenius	Arrhenius	k1gInSc4	Arrhenius
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
