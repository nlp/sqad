Tapisérie	tapisérie	k1gFnPc1
jsou	být	k5eAaImIp3nP
tkané	tkaný	k2eAgInPc1d1
koberce	koberec	k1gInPc1
<g/>
,	,	kIx,
pokrývky	pokrývka	k1gFnPc1
či	či	k8xC
potahy	potah	k1gInPc1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
nástěnné	nástěnný	k2eAgFnPc1d1
<g/>
,	,	kIx,
podle	podle	k7c2
pařížské	pařížský	k2eAgFnSc2d1
manufaktury	manufaktura	k1gFnSc2
nazývané	nazývaný	k2eAgFnPc1d1
také	také	k6eAd1
ekvivalentním	ekvivalentní	k2eAgInSc7d1
termínem	termín	k1gInSc7
gobelíny	gobelín	k1gInPc1
