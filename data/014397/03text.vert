<s>
Tapiserie	tapiserie	k1gFnSc1
</s>
<s>
Lov	lov	k1gInSc1
na	na	k7c4
jednorožce	jednorožec	k1gMnPc4
ze	z	k7c2
série	série	k1gFnSc2
panna	panna	k1gFnSc1
a	a	k8xC
jednorožec	jednorožec	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Portrétní	portrétní	k2eAgFnSc1d1
tapiserie	tapiserie	k1gFnSc1
<g/>
:	:	kIx,
král	král	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudor	tudor	k1gInSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
Janou	Jana	k1gFnSc7
Seymourovou	Seymourův	k2eAgFnSc7d1
a	a	k8xC
synem	syn	k1gMnSc7
(	(	kIx(
<g/>
cca	cca	kA
1545	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tapisérie	tapisérie	k1gFnPc1
jsou	být	k5eAaImIp3nP
tkané	tkaný	k2eAgInPc1d1
koberce	koberec	k1gInPc1
<g/>
,	,	kIx,
pokrývky	pokrývka	k1gFnPc1
či	či	k8xC
potahy	potah	k1gInPc1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
nástěnné	nástěnný	k2eAgFnPc1d1
<g/>
,	,	kIx,
podle	podle	k7c2
pařížské	pařížský	k2eAgFnSc2d1
manufaktury	manufaktura	k1gFnSc2
nazývané	nazývaný	k2eAgFnPc1d1
také	také	k6eAd1
ekvivalentním	ekvivalentní	k2eAgInSc7d1
termínem	termín	k1gInSc7
gobelíny	gobelín	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
manufaktury	manufaktura	k1gFnSc2
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Arrasu	Arras	k1gInSc6
se	se	k3xPyFc4
v	v	k7c6
polštině	polština	k1gFnSc6
nazývají	nazývat	k5eAaImIp3nP
arrasy	arras	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
ruštině	ruština	k1gFnSc6
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
špalier	špalira	k1gFnPc2
odvozeno	odvodit	k5eAaPmNgNnS
ze	z	k7c2
staroněmeckého	staroněmecký	k2eAgInSc2d1
termínu	termín	k1gInSc2
Spalier	Spalira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tapisérie	tapisérie	k1gFnSc1
mívá	mívat	k5eAaImIp3nS
různé	různý	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
:	:	kIx,
reprezentační	reprezentační	k2eAgFnSc4d1
<g/>
,	,	kIx,
estetickou	estetický	k2eAgFnSc4d1
dekorační	dekorační	k2eAgInPc4d1
<g/>
,	,	kIx,
praktické	praktický	k2eAgInPc4d1
účely	účel	k1gInPc4
tepelné	tepelný	k2eAgFnSc2d1
a	a	k8xC
akustické	akustický	k2eAgFnSc2d1
izolace	izolace	k1gFnSc2
interiéru	interiér	k1gInSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
opticky	opticky	k6eAd1
odděluje	oddělovat	k5eAaImIp3nS
místnost	místnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Vertikální	vertikální	k2eAgInSc1d1
tkalcovský	tkalcovský	k2eAgInSc1d1
stav	stav	k1gInSc1
ve	v	k7c6
státní	státní	k2eAgFnSc6d1
manufaktuře	manufaktura	k1gFnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
V	v	k7c6
celé	celý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
byla	být	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
vždy	vždy	k6eAd1
rukodělná	rukodělný	k2eAgFnSc1d1
<g/>
,	,	kIx,
považovaná	považovaný	k2eAgFnSc1d1
jak	jak	k8xS,k8xC
za	za	k7c4
monumentální	monumentální	k2eAgNnSc4d1
umění	umění	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k9
za	za	k7c4
umělecké	umělecký	k2eAgNnSc4d1
řemeslo	řemeslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravý	pravý	k2eAgInSc1d1
gobelín	gobelín	k1gInSc1
se	se	k3xPyFc4
zhotovuje	zhotovovat	k5eAaImIp3nS
na	na	k7c6
ručním	ruční	k2eAgInSc6d1
tkalcovském	tkalcovský	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
vertikálním	vertikální	k2eAgMnSc6d1
nebo	nebo	k8xC
horizontálním	horizontální	k2eAgMnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobní	výrobní	k2eAgFnSc1d1
technika	technika	k1gFnSc1
se	se	k3xPyFc4
za	za	k7c4
tkaní	tkaní	k1gNnSc4
označuje	označovat	k5eAaImIp3nS
s	s	k7c7
výhradami	výhrada	k1gFnPc7
–	–	k?
práce	práce	k1gFnSc2
na	na	k7c6
stavu	stav	k1gInSc6
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
spíše	spíše	k9
paličkování	paličkování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
barevná	barevný	k2eAgFnSc1d1
předloha	předloha	k1gFnSc1
v	v	k7c6
měřítku	měřítko	k1gNnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
nazývaná	nazývaný	k2eAgFnSc1d1
podle	podle	k7c2
nejčastějšího	častý	k2eAgInSc2d3
materiálu	materiál	k1gInSc2
kartón	kartón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Figurální	figurální	k2eAgFnPc4d1
kompozice	kompozice	k1gFnPc4
a	a	k8xC
krajinné	krajinný	k2eAgInPc4d1
motivy	motiv	k1gInPc4
předkreslují	předkreslovat	k5eAaImIp3nP
tkalcům	tkadlec	k1gMnPc3
malíři	malíř	k1gMnPc1
<g/>
,	,	kIx,
často	často	k6eAd1
pastelem	pastel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technické	technický	k2eAgInPc1d1
a	a	k8xC
barevné	barevný	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
předloh	předloha	k1gFnPc2
lze	lze	k6eAd1
posoudit	posoudit	k5eAaPmF
například	například	k6eAd1
z	z	k7c2
kartónů	kartón	k1gInPc2
Jana	Jan	k1gMnSc2
Cornelisze	Cornelisze	k1gFnSc2
Vermeyena	Vermeyen	k1gMnSc2
v	v	k7c6
Uměleckohistorickém	uměleckohistorický	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kartón	kartón	k1gInSc1
se	se	k3xPyFc4
podkládá	podkládat	k5eAaImIp3nS
pod	pod	k7c4
osnovu	osnova	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
tkalci	tkadlec	k1gMnPc1
si	se	k3xPyFc3
z	z	k7c2
kartónu	kartón	k1gInSc2
křídou	křída	k1gFnSc7
překreslovali	překreslovat	k5eAaImAgMnP
na	na	k7c4
osnovu	osnova	k1gFnSc4
obrysy	obrys	k1gInPc4
předmětů	předmět	k1gInPc2
či	či	k8xC
postav	postava	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
napnuté	napnutý	k2eAgFnPc4d1
osnovní	osnovní	k2eAgFnPc4d1
nitě	nit	k1gFnPc4
se	se	k3xPyFc4
zanášejí	zanášet	k5eAaImIp3nP
útky	útka	k1gFnPc1
(	(	kIx(
<g/>
navinuté	navinutý	k2eAgNnSc1d1
v	v	k7c6
malém	malý	k2eAgInSc6d1
člunku	člunek	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
tyčince	tyčinka	k1gFnSc6
<g/>
)	)	kIx)
většinou	většinou	k6eAd1
jen	jen	k9
do	do	k7c2
části	část	k1gFnSc2
celé	celý	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
„	„	k?
<g/>
tkaniny	tkanina	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
vracejí	vracet	k5eAaImIp3nP
se	se	k3xPyFc4
ke	k	k7c3
kraji	kraj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
barevné	barevný	k2eAgInPc4d1
vzory	vzor	k1gInPc4
s	s	k7c7
ostrými	ostrý	k2eAgFnPc7d1
konturami	kontura	k1gFnPc7
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
i	i	k9
s	s	k7c7
drážkami	drážka	k1gFnPc7
v	v	k7c6
povrchu	povrch	k1gInSc6
textilie	textilie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tkaný	tkaný	k2eAgInSc1d1
dekorativní	dekorativní	k2eAgInSc1d1
rám	rám	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
bordura	bordura	k1gFnSc1
<g/>
,	,	kIx,
nahoře	nahoře	k6eAd1
uprostřed	uprostřed	k6eAd1
bývá	bývat	k5eAaImIp3nS
vytkaný	vytkaný	k2eAgInSc1d1
znak	znak	k1gInSc1
objednavatele	objednavatel	k1gMnSc2
<g/>
,	,	kIx,
dole	dole	k6eAd1
se	se	k3xPyFc4
vytkávalo	vytkávat	k5eAaImAgNnS
vlevo	vlevo	k6eAd1
jméno	jméno	k1gNnSc1
nebo	nebo	k8xC
značka	značka	k1gFnSc1
autora	autor	k1gMnSc2
předlohy	předloha	k1gFnSc2
a	a	k8xC
vpravo	vpravo	k6eAd1
značka	značka	k1gFnSc1
provádějícího	provádějící	k2eAgMnSc2d1
tkalce	tkadlec	k1gMnSc2
či	či	k8xC
dílny	dílna	k1gFnSc2
<g/>
,	,	kIx,
obojí	oboj	k1gFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dole	dole	k6eAd1
uprostřed	uprostřed	k7c2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tapisérie	tapisérie	k1gFnPc1
se	se	k3xPyFc4
tkaly	tkát	k5eAaImAgFnP
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
z	z	k7c2
ovčí	ovčí	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
hedvábnou	hedvábný	k2eAgFnSc7d1
přízí	příz	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
pro	pro	k7c4
vázané	vázaný	k2eAgInPc4d1
orientální	orientální	k2eAgInPc4d1
koberce	koberec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útky	útka	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
z	z	k7c2
mykané	mykaný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
v	v	k7c6
neomezeném	omezený	k2eNgInSc6d1
počtu	počet	k1gInSc6
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
částečně	částečně	k6eAd1
z	z	k7c2
leonských	leonský	k2eAgFnPc2d1
nití	nit	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruční	ruční	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zdokonalovala	zdokonalovat	k5eAaImAgFnS
a	a	k8xC
mechanizovala	mechanizovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
takže	takže	k8xS
osnov	osnova	k1gFnPc2
i	i	k8xC
útků	útek	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
zdokonalení	zdokonalení	k1gNnSc4
stavu	stav	k1gInSc2
provedl	provést	k5eAaPmAgMnS
Francouz	Francouz	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Marie	Maria	k1gFnSc2
Jacquard	Jacquard	k1gMnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něj	on	k3xPp3gInSc2
se	se	k3xPyFc4
vzorování	vzorování	k1gNnSc1
nazývá	nazývat	k5eAaImIp3nS
žakárská	žakárský	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Variace	variace	k1gFnPc1
pojmu	pojem	k1gInSc2
</s>
<s>
Termínem	termín	k1gInSc7
tapisérie	tapisérie	k1gFnSc2
jsou	být	k5eAaImIp3nP
někdy	někdy	k6eAd1
nesprávně	správně	k6eNd1
označovány	označovat	k5eAaImNgFnP
technicky	technicky	k6eAd1
příbuzné	příbuzný	k2eAgFnPc1d1
textilie	textilie	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
styl	styl	k1gInSc1
vyšívání	vyšívání	k1gNnSc2
<g/>
,	,	kIx,
prošívání	prošívání	k1gNnSc2
nebo	nebo	k8xC
tepelné	tepelný	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
lisováním	lisování	k1gNnSc7
vizuálně	vizuálně	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
tkané	tkaný	k2eAgFnPc4d1
tapiserie	tapiserie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
ručně	ručně	k6eAd1
vyšívaná	vyšívaný	k2eAgFnSc1d1
či	či	k8xC
aplikovaná	aplikovaný	k2eAgFnSc1d1
práce	práce	k1gFnSc1
na	na	k7c6
podkladu	podklad	k1gInSc6
z	z	k7c2
lněného	lněný	k2eAgNnSc2d1
plátna	plátno	k1gNnSc2
(	(	kIx(
<g/>
nejslavnější	slavný	k2eAgMnSc1d3
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
tapisérie	tapisérie	k1gFnSc1
z	z	k7c2
Bayeux	Bayeux	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1065	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
to	ten	k3xDgNnSc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
strojově	strojově	k6eAd1
řadou	řada	k1gFnSc7
stehů	steh	k1gInPc2
prošívaná	prošívaný	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
z	z	k7c2
ovčího	ovčí	k2eAgNnSc2d1
rouna	rouno	k1gNnSc2
<g/>
,	,	kIx,
příze	příz	k1gFnSc2
i	i	k8xC
jiných	jiný	k2eAgFnPc2d1
textilií	textilie	k1gFnPc2
včetně	včetně	k7c2
syntetických	syntetický	k2eAgInPc2d1
<g/>
,	,	kIx,
česky	česky	k6eAd1
artprotis	artprotis	k1gInSc1
<g/>
,	,	kIx,
vyvinutá	vyvinutý	k2eAgFnSc1d1
v	v	k7c6
n.	n.	k?
p.	p.	k?
Vlněna	vlněn	k2eAgFnSc1d1
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
do	do	k7c2
její	její	k3xOp3gFnSc2
dílny	dílna	k1gFnSc2
ji	on	k3xPp3gFnSc4
také	také	k9
výtvarníci	výtvarník	k1gMnPc1
a	a	k8xC
výtvarnice	výtvarnice	k1gFnPc4
jezdili	jezdit	k5eAaImAgMnP
zhotovovat	zhotovovat	k5eAaImF
<g/>
;	;	kIx,
továrna	továrna	k1gFnSc1
i	i	k8xC
výtvarnická	výtvarnický	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
zanikly	zaniknout	k5eAaPmAgFnP
počátkem	počátek	k1gInSc7
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technicky	technicky	k6eAd1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
též	též	k9
nazývala	nazývat	k5eAaImAgFnS
textilní	textilní	k2eAgFnSc1d1
intarzie	intarzie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
jako	jako	k9
tapisérie	tapisérie	k1gFnPc1
nesprávně	správně	k6eNd1
označují	označovat	k5eAaImIp3nP
i	i	k9
strojově	strojově	k6eAd1
lisovaná	lisovaný	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
z	z	k7c2
ovčího	ovčí	k2eAgNnSc2d1
rouna	rouno	k1gNnSc2
<g/>
,	,	kIx,
příze	příz	k1gFnSc2
či	či	k8xC
jiných	jiný	k2eAgFnPc2d1
textilií	textilie	k1gFnPc2
včetně	včetně	k7c2
syntetických	syntetický	k2eAgFnPc2d1
fólií	fólie	k1gFnPc2
<g/>
,	,	kIx,
česky	česky	k6eAd1
aradecor	aradecor	k1gInSc1
<g/>
,	,	kIx,
vyvinutá	vyvinutý	k2eAgFnSc1d1
při	při	k7c6
továrně	továrna	k1gFnSc6
vlněných	vlněný	k2eAgFnPc2d1
tkanin	tkanina	k1gFnPc2
ve	v	k7c6
Kdyni	Kdyně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Továrna	továrna	k1gFnSc1
i	i	k9
výtvarnická	výtvarnický	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
zanikly	zaniknout	k5eAaPmAgFnP
konce	konec	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Imitace	imitace	k1gFnSc1
gobelínu	gobelín	k1gInSc2
(	(	kIx(
<g/>
malba	malba	k1gFnSc1
na	na	k7c6
plátně	plátno	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zámek	zámek	k1gInSc1
Linderhof	Linderhof	k1gInSc1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Gobelíny	gobelín	k1gInPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
brzy	brzy	k6eAd1
imitovat	imitovat	k5eAaBmF
výrobky	výrobek	k1gInPc4
z	z	k7c2
mechanických	mechanický	k2eAgInPc2d1
tkacích	tkací	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tkají	tkát	k5eAaImIp3nP
se	se	k3xPyFc4
zejména	zejména	k9
žakárovou	žakárový	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
ve	v	k7c6
dvou	dva	k4xCgInPc6
základních	základní	k2eAgInPc6d1
druzích	druh	k1gInPc6
<g/>
:	:	kIx,
útkový	útkový	k2eAgInSc1d1
a	a	k8xC
osnovní	osnovní	k2eAgInSc1d1
gobelín	gobelín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
útkových	útkový	k2eAgInPc2d1
gobelínů	gobelín	k1gInPc2
se	se	k3xPyFc4
tkanina	tkanina	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
výplňkové	výplňkový	k2eAgFnSc2d1
osnovy	osnova	k1gFnSc2
z	z	k7c2
pevných	pevný	k2eAgFnPc2d1
nití	nit	k1gFnPc2
a	a	k8xC
vazné	vazný	k2eAgFnPc1d1
osnovy	osnova	k1gFnPc1
z	z	k7c2
jemné	jemný	k2eAgFnSc2d1
příze	příz	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výplňková	výplňkový	k2eAgFnSc1d1
osnova	osnova	k1gFnSc1
dělí	dělit	k5eAaImIp3nS
po	po	k7c6
celé	celý	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
tkaniny	tkanina	k1gFnSc2
útkové	útkový	k2eAgFnSc2d1
niti	nit	k1gFnSc2
na	na	k7c6
vzorovací	vzorovací	k2eAgFnSc6d1
a	a	k8xC
výplňkové	výplňkový	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzorování	vzorování	k1gNnSc1
je	být	k5eAaImIp3nS
řízeno	řídit	k5eAaImNgNnS
žakárovým	žakárový	k2eAgNnSc7d1
ústrojím	ústrojí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tkaniny	tkanina	k1gFnPc1
s	s	k7c7
jednou	jeden	k4xCgFnSc7
výplňkovou	výplňkový	k2eAgFnSc7d1
osnovou	osnova	k1gFnSc7
mají	mít	k5eAaImIp3nP
požadované	požadovaný	k2eAgNnSc4d1
vzorování	vzorování	k1gNnSc4
jen	jen	k9
na	na	k7c6
lícní	lícní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
(	(	kIx(
<g/>
na	na	k7c6
rubu	rub	k1gInSc6
jsou	být	k5eAaImIp3nP
náhodně	náhodně	k6eAd1
sestavené	sestavený	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výrobě	výroba	k1gFnSc3
tkanin	tkanina	k1gFnPc2
s	s	k7c7
oboustranným	oboustranný	k2eAgNnSc7d1
vzorováním	vzorování	k1gNnSc7
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
použít	použít	k5eAaPmF
dvě	dva	k4xCgFnPc4
výplňkové	výplňkový	k2eAgFnPc4d1
osnovy	osnova	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
útkových	útkový	k2eAgInPc2d1
gobelínů	gobelín	k1gInPc2
zaznamenala	zaznamenat	k5eAaPmAgFnS
velký	velký	k2eAgInSc4d1
rozmach	rozmach	k1gInSc4
v	v	k7c6
první	první	k4xOgFnSc6
třetině	třetina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
velmi	velmi	k6eAd1
husté	hustý	k2eAgFnPc1d1
tkaniny	tkanina	k1gFnPc1
až	až	k9
s	s	k7c7
84	#num#	k4
útky	útek	k1gInPc7
na	na	k7c4
centimetr	centimetr	k1gInSc4
v	v	k7c6
maximálně	maximálně	k6eAd1
11	#num#	k4
barvách	barva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Tkanina	tkanina	k1gFnSc1
osnovních	osnovní	k2eAgInPc2d1
gobelínů	gobelín	k1gInPc2
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
2-6	2-6	k4
barevných	barevný	k2eAgFnPc2d1
(	(	kIx(
<g/>
vzorovacích	vzorovací	k2eAgFnPc2d1
<g/>
)	)	kIx)
osnov	osnova	k1gFnPc2
<g/>
,	,	kIx,
jedné	jeden	k4xCgFnSc2
vazné	vazný	k2eAgFnSc2d1
osnovy	osnova	k1gFnSc2
<g/>
,	,	kIx,
dvou	dva	k4xCgInPc2
vzorovacích	vzorovací	k2eAgInPc2d1
a	a	k8xC
jednoho	jeden	k4xCgInSc2
vazného	vazný	k2eAgInSc2d1
útku	útek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobek	výrobek	k1gInSc1
mívá	mívat	k5eAaImIp3nS
vzhled	vzhled	k1gInSc4
příčného	příčný	k2eAgInSc2d1
rypsu	ryps	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osnovy	osnova	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
ze	z	k7c2
skané	skaný	k2eAgFnSc2d1
příze	příz	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
14-18	14-18	k4
tex	tex	k?
x	x	k?
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzorovací	vzorovací	k2eAgFnSc2d1
útky	útka	k1gFnSc2
ze	z	k7c2
166	#num#	k4
tex	tex	k?
a	a	k8xC
vazný	vazný	k2eAgInSc1d1
útek	útek	k1gInSc1
14	#num#	k4
tex	tex	k?
x	x	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
oboustrannému	oboustranný	k2eAgNnSc3d1
vzorování	vzorování	k1gNnSc3
tkaniny	tkanina	k1gFnSc2
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
použít	použít	k5eAaPmF
dva	dva	k4xCgInPc4
vazné	vazný	k2eAgInPc4d1
útky	útek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Též	též	k9
vyšívaný	vyšívaný	k2eAgInSc1d1
gobelín	gobelín	k1gInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
imitaci	imitace	k1gFnSc4
pravého	pravý	k2eAgInSc2d1
gobelínu	gobelín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristický	charakteristický	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
vhodným	vhodný	k2eAgNnSc7d1
seskupením	seskupení	k1gNnSc7
křížkových	křížkový	k2eAgInPc2d1
stehů	steh	k1gInPc2
z	z	k7c2
nití	nit	k1gFnPc2
různé	různý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
na	na	k7c6
podkladové	podkladový	k2eAgFnSc6d1
tkanině	tkanina	k1gFnSc6
<g/>
,	,	kIx,
barevná	barevný	k2eAgFnSc1d1
předloha	předloha	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
na	na	k7c6
tkanině	tkanina	k1gFnSc6
natištěna	natištěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšívací	vyšívací	k2eAgInSc1d1
nit	nit	k1gInSc1
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
z	z	k7c2
čisté	čistý	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Malovaný	malovaný	k2eAgInSc1d1
gobelín	gobelín	k1gInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
napodobenina	napodobenina	k1gFnSc1
gobelínu	gobelín	k1gInSc2
malovaná	malovaný	k2eAgFnSc1d1
na	na	k7c6
rypsové	rypsový	k2eAgFnSc6d1
tkanině	tkanina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
zhotovují	zhotovovat	k5eAaImIp3nP
také	také	k9
předlohy	předloha	k1gFnPc4
na	na	k7c4
pravé	pravý	k2eAgInPc4d1
gobelíny	gobelín	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Petit	petit	k1gInSc1
point	pointa	k1gFnPc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
(	(	kIx(
<g/>
bodová	bodový	k2eAgFnSc1d1
<g/>
)	)	kIx)
gobelínová	gobelínový	k2eAgFnSc1d1
výšivka	výšivka	k1gFnSc1
z	z	k7c2
drobných	drobný	k2eAgInPc2d1
křížkových	křížkový	k2eAgInPc2d1
stehů	steh	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
zanášejí	zanášet	k5eAaImIp3nP
na	na	k7c4
jemný	jemný	k2eAgInSc4d1
hedvábný	hedvábný	k2eAgInSc4d1
gáz	gáz	k1gInSc4
(	(	kIx(
<g/>
až	až	k9
484	#num#	k4
stehů	steh	k1gInPc2
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
zpravidla	zpravidla	k6eAd1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
lupy	lupa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technika	technika	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
období	období	k1gNnSc6
rokoka	rokoko	k1gNnSc2
a	a	k8xC
uplatňuje	uplatňovat	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
na	na	k7c6
ozdobných	ozdobný	k2eAgInPc6d1
polštářích	polštář	k1gInPc6
<g/>
,	,	kIx,
kabelkách	kabelka	k1gFnPc6
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
A	a	k8xC
gobelínová	gobelínový	k2eAgFnSc1d1
příze	příze	k1gFnSc1
je	být	k5eAaImIp3nS
čtyřnásobně	čtyřnásobně	k6eAd1
skaná	skaný	k2eAgFnSc1d1
příze	příze	k1gFnSc1
z	z	k7c2
česané	česaný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
nebo	nebo	k8xC
směsí	směs	k1gFnPc2
s	s	k7c7
vlnou	vlna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
nábytkové	nábytkový	k2eAgFnPc4d1
tkaniny	tkanina	k1gFnPc4
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
na	na	k7c4
sportovní	sportovní	k2eAgInPc4d1
oděvy	oděv	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
tapisérie	tapisérie	k1gFnSc2
</s>
<s>
Stvoření	stvoření	k1gNnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Giulio	Giulio	k1gMnSc1
Romano	Romano	k1gMnSc1
(	(	kIx(
<g/>
navrh	navrh	k1gMnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Bitva	bitva	k1gFnSc1
u	u	k7c2
Zamy	Zama	k1gFnSc2
(	(	kIx(
<g/>
1688	#num#	k4
<g/>
–	–	k?
<g/>
1690	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manufaktura	manufaktura	k1gFnSc1
Les	les	k1gInSc1
Gobelins	Gobelins	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3
vyobrazení	vyobrazení	k1gNnSc1
výroby	výroba	k1gFnSc2
tapisérií	tapisérie	k1gFnPc2
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
starověkého	starověký	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
z	z	k7c2
období	období	k1gNnSc2
2000	#num#	k4
let	léto	k1gNnPc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
je	být	k5eAaImIp3nS
doložena	doložit	k5eAaPmNgFnS
také	také	k9
ve	v	k7c6
starověkém	starověký	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
jednak	jednak	k8xC
malbou	malba	k1gFnSc7
na	na	k7c6
keramice	keramika	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
archeologickými	archeologický	k2eAgInPc7d1
nálezy	nález	k1gInPc7
ze	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
před	před	k7c7
n.	n.	k?
<g/>
l.	l.	k?
Ke	k	k7c3
tkaní	tkaní	k1gNnSc3
tapisérií	tapisérie	k1gFnPc2
se	se	k3xPyFc4
váže	vázat	k5eAaImIp3nS
mytologický	mytologický	k2eAgInSc1d1
příběh	příběh	k1gInSc1
o	o	k7c6
tkadleně	tkadlena	k1gFnSc6
Arachné	Arachná	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
utkala	utkat	k5eAaPmAgFnS
o	o	k7c4
umělecké	umělecký	k2eAgNnSc4d1
prvenství	prvenství	k1gNnSc4
s	s	k7c7
bohyní	bohyně	k1gFnSc7
Pallas	Pallas	k1gMnSc1
Athénou	Athéna	k1gFnSc7
<g/>
,	,	kIx,
ze	z	k7c2
zoufalství	zoufalství	k1gNnSc2
se	se	k3xPyFc4
oběsila	oběsit	k5eAaPmAgFnS
a	a	k8xC
byla	být	k5eAaImAgFnS
proměněna	proměnit	k5eAaPmNgFnS
v	v	k7c4
pavouka	pavouk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
nástrojů	nástroj	k1gInPc2
znaly	znát	k5eAaImAgFnP
a	a	k8xC
užívaly	užívat	k5eAaImAgFnP
techniky	technika	k1gFnPc1
tapisérie	tapisérie	k1gFnSc2
také	také	k9
starověké	starověký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
Indii	Indie	k1gFnSc6
a	a	k8xC
předkolumbovské	předkolumbovský	k2eAgFnSc3d1
Amnerice	Amnerika	k1gFnSc3
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
v	v	k7c6
muzeích	muzeum	k1gNnPc6
dochované	dochovaný	k2eAgInPc1d1
fragmenty	fragment	k1gInPc1
textilií	textilie	k1gFnPc2
tkané	tkaný	k2eAgNnSc1d1
technikou	technika	k1gFnSc7
tapisérií	tapisérie	k1gFnPc2
jsou	být	k5eAaImIp3nP
koptské	koptský	k2eAgFnPc1d1
pohřební	pohřební	k2eAgFnPc1d1
textilie	textilie	k1gFnPc1
ze	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
v	v	k7c6
Louvru	Louvre	k1gInSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
Náprstkově	náprstkově	k6eAd1
nebo	nebo	k8xC
v	v	k7c6
Uměleckoprůmyslovém	uměleckoprůmyslový	k2eAgNnSc6d1
museu	museum	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
tapisérie	tapisérie	k1gFnSc1
v	v	k7c6
hornosaském	hornosaský	k2eAgMnSc6d1
Halberstadtu	Halberstadt	k1gMnSc6
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
někdy	někdy	k6eAd1
za	za	k7c4
nejstarší	starý	k2eAgInSc4d3
evropský	evropský	k2eAgInSc4d1
příklad	příklad	k1gInSc4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
tapisérie	tapisérie	k1gFnSc1
z	z	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Gereona	Gereon	k1gMnSc2
v	v	k7c6
Kolíně	Kolín	k1gInSc6
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
až	až	k9
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
tapisérie	tapisérie	k1gFnSc1
v	v	k7c6
katalánské	katalánský	k2eAgFnSc6d1
Gironě	Girona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
řemeslo	řemeslo	k1gNnSc1
systematicky	systematicky	k6eAd1
rozvíjelo	rozvíjet	k5eAaImAgNnS
od	od	k7c2
počátku	počátek	k1gInSc2
gotiky	gotika	k1gFnSc2
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
hlavně	hlavně	k9
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
pozdní	pozdní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
série	série	k1gFnSc1
Dáma	dáma	k1gFnSc1
a	a	k8xC
jednorožec	jednorožec	k1gMnSc1
a	a	k8xC
především	především	k6eAd1
v	v	k7c6
renesanci	renesance	k1gFnSc6
a	a	k8xC
baroku	baroko	k1gNnSc6
také	také	k9
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Nizozemí	Nizozemí	k1gNnSc6
<g/>
,	,	kIx,
pod	pod	k7c4
něž	jenž	k3xRgMnPc4
patřily	patřit	k5eAaImAgFnP
nyní	nyní	k6eAd1
belgické	belgický	k2eAgNnSc4d1
Vlámsko	Vlámsko	k1gNnSc4
a	a	k8xC
Flandry	Flandry	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlatým	zlatý	k2eAgInSc7d1
věkem	věk	k1gInSc7
tapisérie	tapisérie	k1gFnSc2
bylo	být	k5eAaImAgNnS
16	#num#	k4
<g/>
.	.	kIx.
až	až	k9
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
působily	působit	k5eAaImAgFnP
asi	asi	k9
dvě	dva	k4xCgFnPc1
desítky	desítka	k1gFnPc1
vyhlášených	vyhlášený	k2eAgFnPc2d1
manufaktur	manufaktura	k1gFnPc2
v	v	k7c6
několika	několik	k4yIc6
městech	město	k1gNnPc6
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
velkoformátové	velkoformátový	k2eAgInPc1d1
gobelíny	gobelín	k1gInPc1
vznikaly	vznikat	k5eAaImAgInP
podle	podle	k7c2
předloh	předloha	k1gFnPc2
předních	přední	k2eAgMnPc2d1
malířů	malíř	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnPc1d3
byly	být	k5eAaImAgFnP
dílny	dílna	k1gFnPc1
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
,	,	kIx,
jimž	jenž	k3xRgInPc3
předlohy	předloha	k1gFnPc1
dodávali	dodávat	k5eAaImAgMnP
slavní	slavný	k2eAgMnPc1d1
vlámští	vlámský	k2eAgMnPc1d1
malíři	malíř	k1gMnPc1
Petr	Petr	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Rubens	Rubensa	k1gFnPc2
<g/>
,	,	kIx,
Jacob	Jacoba	k1gFnPc2
Jordaens	Jordaensa	k1gFnPc2
<g/>
,	,	kIx,
David	David	k1gMnSc1
Teniers	Teniersa	k1gFnPc2
ml.	ml.	kA
<g/>
,	,	kIx,
Jodocus	Jodocus	k1gInSc1
De	De	k?
Vos	vosa	k1gFnPc2
a	a	k8xC
v	v	k7c6
Antverpách	Antverpy	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
předlohy	předloha	k1gFnPc4
navrhovali	navrhovat	k5eAaImAgMnP
Giovanni	Giovann	k1gMnPc1
da	da	k?
Udine	Udin	k1gMnSc5
<g/>
,	,	kIx,
Raffael	Raffael	k1gMnSc1
Santi	Sanť	k1gFnSc2
Giuseppe	Giusepp	k1gInSc5
Arcimboldo	Arcimboldo	k1gNnSc1
nebo	nebo	k8xC
Giulio	Giulio	k1gMnSc1
Romano	Romano	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc4
kartóny	kartón	k1gInPc4
prováděli	provádět	k5eAaImAgMnP
francouzští	francouzský	k2eAgMnPc1d1
tkalci	tkadlec	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jeho	jeho	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
psali	psát	k5eAaImAgMnP
francouzsky	francouzsky	k6eAd1
Jules	Jules	k1gMnSc1
Romain	Romain	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
se	se	k3xPyFc4
tapisérie	tapisérie	k1gFnPc1
pouze	pouze	k6eAd1
dovážely	dovážet	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1662	#num#	k4
dal	dát	k5eAaPmAgMnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
ministr	ministr	k1gMnSc1
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Colbert	Colbert	k1gInSc1
přestavět	přestavět	k5eAaPmF
dům	dům	k1gInSc4
s	s	k7c7
dílnami	dílna	k1gFnPc7
středověkých	středověký	k2eAgMnPc2d1
barvířů	barvíř	k1gMnPc2
textilií	textilie	k1gFnPc2
a	a	k8xC
založil	založit	k5eAaPmAgMnS
v	v	k7c6
nich	on	k3xPp3gInPc6
pro	pro	k7c4
francouzského	francouzský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Ludvíka	Ludvík	k1gMnSc4
XIV	XIV	kA
<g/>
.	.	kIx.
královskou	královský	k2eAgFnSc4d1
manufakturu	manufaktura	k1gFnSc4
uměleckých	umělecký	k2eAgNnPc2d1
řemesel	řemeslo	k1gNnPc2
<g/>
,	,	kIx,
zvanou	zvaný	k2eAgFnSc7d1
Les	les	k1gInSc4
Gobelins	Gobelinsa	k1gFnPc2
podle	podle	k7c2
zdejší	zdejší	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
zakladatele	zakladatel	k1gMnSc2
Jehana	Jehan	k1gMnSc2
Gobelina	Gobelin	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
se	se	k3xPyFc4
vžilo	vžít	k5eAaPmAgNnS
pro	pro	k7c4
tapisérie	tapisérie	k1gFnPc4
také	také	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
v	v	k7c6
pařížské	pařížský	k2eAgFnSc6d1
manufaktuře	manufaktura	k1gFnSc6
Gobelins	Gobelins	k1gInSc1
kromě	kromě	k7c2
tkalců	tkadlec	k1gMnPc2
tapisérií	tapisérie	k1gFnPc2
a	a	k8xC
čalouníků	čalouník	k1gMnPc2
pracovali	pracovat	k5eAaImAgMnP
také	také	k9
další	další	k2eAgMnPc1d1
řemeslníci	řemeslník	k1gMnPc1
<g/>
,	,	kIx,
zejména	zejména	k9
truhláři	truhlář	k1gMnPc1
<g/>
,	,	kIx,
bronzíři	bronzíř	k1gMnPc1
a	a	k8xC
marketéři	marketér	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
užším	úzký	k2eAgInSc6d2
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
proto	proto	k8xC
platí	platit	k5eAaImIp3nS
označení	označení	k1gNnSc1
gobelín	gobelín	k1gInSc1
výlučně	výlučně	k6eAd1
pro	pro	k7c4
výrobky	výrobek	k1gInPc4
této	tento	k3xDgFnSc2
pařížské	pařížský	k2eAgFnSc2d1
manufaktury	manufaktura	k1gFnSc2
<g/>
,	,	kIx,
všeobecně	všeobecně	k6eAd1
se	se	k3xPyFc4
však	však	k9
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
veškeré	veškerý	k3xTgNnSc4
ručně	ručně	k6eAd1
i	i	k9
strojně	strojně	k6eAd1
tkané	tkaný	k2eAgFnPc4d1
či	či	k8xC
vyšívané	vyšívaný	k2eAgFnPc4d1
tapiserie	tapiserie	k1gFnPc4
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
pojmy	pojem	k1gInPc1
gobelín	gobelín	k1gInSc1
a	a	k8xC
tapisérie	tapisérie	k1gFnSc1
ztotožňují	ztotožňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Výroba	výroba	k1gFnSc1
gobelínů	gobelín	k1gInPc2
se	se	k3xPyFc4
v	v	k7c6
budově	budova	k1gFnSc6
manufaktury	manufaktura	k1gFnSc2
předvádí	předvádět	k5eAaImIp3nS
doposud	doposud	k6eAd1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
gobelíny	gobelín	k1gInPc1
jsou	být	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
napodobovány	napodobován	k2eAgInPc1d1
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
)	)	kIx)
v	v	k7c6
několika	několik	k4yIc6
variantách	varianta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
technikou	technika	k1gFnSc7
tapisérie	tapisérie	k1gFnPc1
tkaly	tkát	k5eAaImAgFnP
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
také	také	k9
tapety	tapet	k1gInPc4
<g/>
,	,	kIx,
pokrývky	pokrývka	k1gFnPc4
a	a	k8xC
potahy	potah	k1gInPc4
čalouněného	čalouněný	k2eAgInSc2d1
nábytku	nábytek	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
často	často	k6eAd1
jen	jen	k6eAd1
kopírovaly	kopírovat	k5eAaImAgFnP
starší	starý	k2eAgFnPc4d2
předlohy	předloha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
období	období	k1gNnSc4
rozkvětu	rozkvět	k1gInSc2
zažila	zažít	k5eAaPmAgFnS
tapisérie	tapisérie	k1gFnSc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
obnovitelům	obnovitel	k1gMnPc3
tradice	tradice	k1gFnSc2
patřil	patřit	k5eAaImAgMnS
v	v	k7c6
Anglii	Anglie	k1gFnSc6
Charles	Charles	k1gMnSc1
W.	W.	kA
Morris	Morris	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
Francii	Francie	k1gFnSc6
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc3
Jean	Jean	k1gMnSc1
Lurçat	Lurçat	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
návrhářům	návrhář	k1gMnPc3
se	se	k3xPyFc4
počítají	počítat	k5eAaImIp3nP
slavní	slavný	k2eAgMnPc1d1
malíři	malíř	k1gMnPc1
jako	jako	k8xC,k8xS
Pablo	Pablo	k1gNnSc1
Picasso	Picassa	k1gFnSc5
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
Vasarely	Vasarela	k1gFnSc2
nebo	nebo	k8xC
Alexander	Alexandra	k1gFnPc2
Colder	Coldra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
sehráli	sehrát	k5eAaPmAgMnP
zakladatelskou	zakladatelský	k2eAgFnSc4d1
roli	role	k1gFnSc4
organizátorů	organizátor	k1gMnPc2
výroby	výroba	k1gFnSc2
dvě	dva	k4xCgFnPc4
osobnosti	osobnost	k1gFnPc4
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
Schlattauer	Schlattauer	k1gMnSc1
založil	založit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1898	#num#	k4
dílny	dílna	k1gFnSc2
v	v	k7c6
Zašové	Zašová	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
10	#num#	k4
letech	let	k1gInPc6
přenesené	přenesený	k2eAgInPc1d1
do	do	k7c2
Valašského	valašský	k2eAgNnSc2d1
Meziříčí	Meziříčí	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
výtvarnice	výtvarnice	k1gFnSc1
Marie	Marie	k1gFnSc1
Hoppe-Teinitzerová	Hoppe-Teinitzerová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
roku	rok	k1gInSc2
1910	#num#	k4
založila	založit	k5eAaPmAgFnS
dílny	dílna	k1gFnPc4
v	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
a	a	k8xC
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
na	na	k7c6
státních	státní	k2eAgFnPc6d1
zakázkách	zakázka	k1gFnPc6
nově	nově	k6eAd1
ustaveného	ustavený	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
angažovala	angažovat	k5eAaBmAgFnS
například	například	k6eAd1
Františka	František	k1gMnSc4
Kyselu	Kysela	k1gMnSc4
<g/>
,	,	kIx,
Karla	Karel	k1gMnSc4
Svolinského	Svolinský	k2eAgMnSc4d1
či	či	k8xC
Cyrila	Cyril	k1gMnSc4
Boudu	Bouda	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předlohy	předloha	k1gFnPc4
ke	k	k7c3
gobelínům	gobelín	k1gInPc3
zhotovovali	zhotovovat	k5eAaImAgMnP
často	často	k6eAd1
i	i	k8xC
další	další	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
výtvarní	výtvarný	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
práce	práce	k1gFnSc2
dostaly	dostat	k5eAaPmAgFnP
několik	několik	k4yIc4
mezinárodních	mezinárodní	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
na	na	k7c6
světových	světový	k2eAgFnPc6d1
výstavách	výstava	k1gFnPc6
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatelem	zakladatel	k1gMnSc7
české	český	k2eAgFnSc2d1
vysokoškolské	vysokoškolský	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
tapisérií	tapisérie	k1gFnPc2
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1
v	v	k7c6
Praze	Praha	k1gFnSc6
byl	být	k5eAaImAgMnS
Antonín	Antonín	k1gMnSc1
Kybal	Kybal	k1gMnSc1
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
speciálkou	speciálka	k1gFnSc7
prošly	projít	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
generace	generace	k1gFnPc1
textilních	textilní	k2eAgMnPc2d1
výtvarníků	výtvarník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
i	i	k8xC
v	v	k7c6
Moravské	moravský	k2eAgFnSc6d1
gobelínové	gobelínový	k2eAgFnSc6d1
manufaktuře	manufaktura	k1gFnSc6
ve	v	k7c6
Valašském	valašský	k2eAgNnSc6d1
Meziříčí	Meziříčí	k1gNnSc6
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
zachovává	zachovávat	k5eAaImIp3nS
výroba	výroba	k1gFnSc1
klasickou	klasický	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhotovují	zhotovovat	k5eAaImIp3nP
se	se	k3xPyFc4
gobelíny	gobelín	k1gInPc7
podle	podle	k7c2
návrhů	návrh	k1gInPc2
současných	současný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
a	a	k8xC
restaurují	restaurovat	k5eAaBmIp3nP
se	se	k3xPyFc4
historické	historický	k2eAgFnPc1d1
textilie	textilie	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historické	historický	k2eAgFnPc1d1
tapisérie	tapisérie	k1gFnPc1
podle	podle	k7c2
námětů	námět	k1gInPc2
</s>
<s>
figurální	figurální	k2eAgInSc4d1
<g/>
:	:	kIx,
</s>
<s>
mytologie	mytologie	k1gFnSc1
řecká	řecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
římská	římský	k2eAgFnSc1d1
<g/>
,	,	kIx,
bible	bible	k1gFnSc1
</s>
<s>
historické	historický	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
a	a	k8xC
oslavné	oslavný	k2eAgFnPc4d1
alegorické	alegorický	k2eAgFnPc4d1
scény	scéna	k1gFnPc4
</s>
<s>
série	série	k1gFnSc1
alegorií	alegorie	k1gFnPc2
nebo	nebo	k8xC
personifikací	personifikace	k1gFnPc2
(	(	kIx(
<g/>
12	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
12	#num#	k4
múz	múza	k1gFnPc2
<g/>
,	,	kIx,
7	#num#	k4
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
,	,	kIx,
5	#num#	k4
smyslů	smysl	k1gInPc2
<g/>
,	,	kIx,
4	#num#	k4
roční	roční	k2eAgInSc1d1
období	období	k1gNnSc4
<g/>
,	,	kIx,
4	#num#	k4
živly	živel	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
Přísloví	přísloví	k1gNnSc1
(	(	kIx(
<g/>
Proverby	proverb	k1gInPc1
-	-	kIx~
na	na	k7c6
zámku	zámek	k1gInSc6
Hluboká	Hluboká	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
skupinové	skupinový	k2eAgInPc4d1
portréty	portrét	k1gInPc4
</s>
<s>
verdury	verdura	k1gFnPc1
<g/>
:	:	kIx,
krajinné	krajinný	k2eAgFnPc1d1
scenérie	scenérie	k1gFnPc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
doplněné	doplněný	k2eAgInPc1d1
</s>
<s>
se	s	k7c7
zvěří	zvěř	k1gFnSc7
<g/>
,	,	kIx,
</s>
<s>
loveckými	lovecký	k2eAgInPc7d1
motivy	motiv	k1gInPc7
<g/>
,	,	kIx,
</s>
<s>
exotické	exotický	k2eAgFnPc1d1
krajiny	krajina	k1gFnPc1
s	s	k7c7
postavami	postava	k1gFnPc7
domorodců	domorodec	k1gMnPc2
-	-	kIx~
divochů	divoch	k1gMnPc2
</s>
<s>
portiéry	portiéra	k1gFnPc1
(	(	kIx(
<g/>
porte-	porte-	k?
francouzsky	francouzsky	k6eAd1
brána	brán	k2eAgFnSc1d1
<g/>
)	)	kIx)
dekorativní	dekorativní	k2eAgFnSc1d1
kompozice	kompozice	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
je	být	k5eAaImIp3nS
plocha	plocha	k1gFnSc1
rozvržena	rozvrhnout	k5eAaPmNgFnS
architektonickými	architektonický	k2eAgInPc7d1
motivy	motiv	k1gInPc7
<g/>
,	,	kIx,
sloupy	sloup	k1gInPc7
nebo	nebo	k8xC
groteskním	groteskní	k2eAgInSc7d1
ornamentem	ornament	k1gInSc7
<g/>
,	,	kIx,
uvnitř	uvnitř	k6eAd1
bývají	bývat	k5eAaImIp3nP
drobné	drobný	k2eAgFnPc4d1
figurky	figurka	k1gFnPc4
</s>
<s>
heraldické	heraldický	k2eAgFnPc4d1
<g/>
:	:	kIx,
znakové	znakový	k2eAgFnPc4d1
kompozice	kompozice	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
jeden	jeden	k4xCgInSc4
erb	erb	k1gInSc4
nebo	nebo	k8xC
dva	dva	k4xCgInPc4
alianční	alianční	k2eAgInPc4d1
jsou	být	k5eAaImIp3nP
hlavním	hlavní	k2eAgInSc7d1
námětem	námět	k1gInSc7
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
doprovázeným	doprovázený	k2eAgMnPc3d1
pouze	pouze	k6eAd1
štítonoši	štítonoš	k1gMnPc1
<g/>
;	;	kIx,
erby	erb	k1gInPc1
objednavatele	objednavatel	k1gMnSc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
objevovat	objevovat	k5eAaImF
také	také	k9
na	na	k7c6
borduře	bordura	k1gFnSc6
ostatních	ostatní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
</s>
<s>
speciální	speciální	k2eAgFnSc1d1
tematika	tematika	k1gFnSc1
<g/>
,	,	kIx,
Španělská	španělský	k2eAgFnSc1d1
jezdecká	jezdecký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Historická	historický	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
výroby	výroba	k1gFnSc2
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Amiens	Amiens	k1gInSc1
<g/>
,	,	kIx,
Aubusson	Aubusson	k1gInSc1
<g/>
,	,	kIx,
Arras	Arras	k1gInSc1
<g/>
,	,	kIx,
Beauvais	Beauvais	k1gInSc1
<g/>
,	,	kIx,
Lille	Lille	k1gInSc1
<g/>
,	,	kIx,
Malgrange	Malgrange	k1gInSc1
u	u	k7c2
Nancy	Nancy	k1gNnSc2
<g/>
,	,	kIx,
Štrasburk	Štrasburk	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
<g/>
:	:	kIx,
Vlámsko	Vlámsko	k1gNnSc1
<g/>
:	:	kIx,
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
<g/>
Antverpy	Antverpy	k1gFnPc1
<g/>
,	,	kIx,
Bruggy	Bruggy	k1gFnPc1
<g/>
,	,	kIx,
Enghien	Enghien	k1gInSc1
<g/>
,	,	kIx,
Gent	Gent	k1gInSc1
<g/>
,	,	kIx,
Geraardsbergen	Geraardsbergen	k1gInSc1
<g/>
,	,	kIx,
Malines	Malines	k1gInSc1
<g/>
,	,	kIx,
Oudenaarde	Oudenaard	k1gMnSc5
<g/>
,	,	kIx,
Tournai	Tourna	k1gMnSc5
</s>
<s>
Nizozemí	Nizozemí	k1gNnSc1
<g/>
:	:	kIx,
Amsterdam	Amsterdam	k1gInSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
<g/>
:	:	kIx,
Mortlake	Mortlake	k1gInSc1
u	u	k7c2
Londýna	Londýn	k1gInSc2
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
<g/>
:	:	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
<g/>
:	:	kIx,
Madrid	Madrid	k1gInSc1
<g/>
;	;	kIx,
Katedrála	katedrála	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
(	(	kIx(
<g/>
Girona	Girona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
:	:	kIx,
Basilej	Basilej	k1gFnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
<g/>
:	:	kIx,
Řím	Řím	k1gInSc1
manufaktura	manufaktura	k1gFnSc1
Barberini	Barberin	k1gMnPc1
<g/>
,	,	kIx,
Florencie	Florencie	k1gFnPc1
<g/>
;	;	kIx,
</s>
<s>
Norsko	Norsko	k1gNnSc1
<g/>
:	:	kIx,
Grundbrandsdal	Grundbrandsdal	k1gFnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
<g/>
:	:	kIx,
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Drážďany	Drážďany	k1gInPc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
<g/>
:	:	kIx,
Uměleckohistorické	uměleckohistorický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Rusko	Rusko	k1gNnSc1
<g/>
:	:	kIx,
Petrohrad	Petrohrad	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1719	#num#	k4
za	za	k7c2
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Sbírky	sbírka	k1gFnPc1
historických	historický	k2eAgFnPc2d1
tapisérií	tapisérie	k1gFnPc2
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
<g/>
:	:	kIx,
Antverpy	Antverpy	k1gFnPc1
<g/>
,	,	kIx,
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
Gent	Gent	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
Angers	Angers	k1gInSc1
<g/>
,	,	kIx,
Dijon	Dijon	k1gInSc1
<g/>
,	,	kIx,
Lille	Lille	k1gFnSc1
<g/>
,	,	kIx,
Nancy	Nancy	k1gFnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
(	(	kIx(
<g/>
Museé	Museé	k1gNnSc1
de	de	k?
Cluny	Cluna	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Versailles	Versailles	k1gFnSc1
<g/>
,	,	kIx,
Beauvais	Beauvais	k1gInSc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
Indre	Indr	k1gInSc5
en	en	k?
Loire	Loir	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
<g/>
:	:	kIx,
Katedrála	katedrála	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Como	Como	k6eAd1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Florencie	Florencie	k1gFnSc1
(	(	kIx(
<g/>
Gallerie	Gallerie	k1gFnSc1
Uffizi	Uffize	k1gFnSc4
<g/>
,	,	kIx,
Museo	Museo	k6eAd1
degli	degnout	k5eAaImAgMnP,k5eAaPmAgMnP
Argenti	Argent	k1gMnPc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Benátky	Benátky	k1gFnPc1
(	(	kIx(
<g/>
Galleria	Gallerium	k1gNnPc1
dell	della	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Academia	academia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Janov	Janov	k1gInSc1
<g/>
;	;	kIx,
Řím	Řím	k1gInSc1
(	(	kIx(
<g/>
Vatikánská	vatikánský	k2eAgNnPc1d1
muzea	muzeum	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Německo	Německo	k1gNnSc1
<g/>
:	:	kIx,
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Greifswald	Greifswald	k1gInSc1
(	(	kIx(
<g/>
univerzita	univerzita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Halberstadt	Halberstadt	k1gInSc1
(	(	kIx(
<g/>
dóm	dóm	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Postupim	Postupim	k1gFnSc4
(	(	kIx(
<g/>
Sansoucci	Sansoucce	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Nizozemí	Nizozemí	k1gNnSc1
<g/>
:	:	kIx,
Amsterdam	Amsterdam	k1gInSc1
(	(	kIx(
<g/>
Rijksmuseum	Rijksmuseum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Polsko	Polsko	k1gNnSc1
<g/>
:	:	kIx,
Krakov	Krakov	k1gInSc1
(	(	kIx(
<g/>
Wawel	Wawel	k1gInSc1
<g/>
,	,	kIx,
katedrála	katedrála	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
<g/>
:	:	kIx,
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
Kunsthistorisches	Kunsthistorisches	k1gInSc1
Museum	museum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Rusko	Rusko	k1gNnSc1
<g/>
:	:	kIx,
Moskva	Moskva	k1gFnSc1
(	(	kIx(
<g/>
Kreml	Kreml	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
<g/>
:	:	kIx,
Bratislava	Bratislava	k1gFnSc1
(	(	kIx(
<g/>
Primaciálny	Primaciálna	k1gFnSc2
palác	palác	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
<g/>
:	:	kIx,
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
El	Ela	k1gFnPc2
Escorial	Escorial	k1gInSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
:	:	kIx,
Hardwick	Hardwick	k1gMnSc1
Hall	Hall	k1gMnSc1
Derbyshire	Derbyshir	k1gInSc5
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
New	New	k?
York	York	k1gInSc1
<g/>
:	:	kIx,
Metropolitní	metropolitní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Cloisters	Cloistersa	k1gFnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Hrady	hrad	k1gInPc1
a	a	k8xC
zámky	zámek	k1gInPc1
<g/>
:	:	kIx,
Benešov	Benešov	k1gInSc1
nad	nad	k7c7
Ploučnicí	Ploučnice	k1gFnSc7
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
Hluboká	Hluboká	k1gFnSc1
<g/>
,	,	kIx,
Horažďovice	Horažďovice	k1gFnPc1
<g/>
,	,	kIx,
Horšovský	horšovský	k2eAgInSc1d1
Týn	Týn	k1gInSc1
<g/>
,	,	kIx,
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
,	,	kIx,
Kroměříž	Kroměříž	k1gFnSc1
<g/>
,	,	kIx,
Kynžvart	Kynžvart	k1gInSc1
<g/>
,	,	kIx,
Libochovice	Libochovice	k1gFnPc1
<g/>
,	,	kIx,
Moravský	moravský	k2eAgMnSc1d1
Třebová	Třebová	k1gFnSc1
<g/>
,	,	kIx,
Náchod	Náchod	k1gInSc1
<g/>
,	,	kIx,
Náměšť	Náměšť	k1gFnSc1
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Černínský	Černínský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pražský	pražský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
(	(	kIx(
<g/>
Kancelář	kancelář	k1gFnSc1
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrad	hrad	k1gInSc1
Švihov	Švihov	k1gInSc1
<g/>
,	,	kIx,
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
(	(	kIx(
<g/>
muzeum	muzeum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Velké	velký	k2eAgFnPc1d1
Losiny	Losiny	k1gFnPc1
<g/>
,	,	kIx,
Vizovice	Vizovice	k1gFnPc1
</s>
<s>
Paláce	palác	k1gInPc1
<g/>
:	:	kIx,
Olomouc	Olomouc	k1gFnSc1
(	(	kIx(
<g/>
Arcibiskupská	arcibiskupský	k2eAgFnSc1d1
rezidence	rezidence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Buquoyský	Buquoyský	k2eAgInSc1d1
palác	palác	k1gInSc1
-	-	kIx~
Francouzské	francouzský	k2eAgNnSc1d1
velvyslanectví	velvyslanectví	k1gNnSc1
<g/>
,	,	kIx,
Černínský	Černínský	k2eAgMnSc1d1
palác-	palác-	k?
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
Knihovny	knihovna	k1gFnSc2
hl.	hl.	k?
<g/>
m.	m.	k?
Prahy	Praha	k1gFnSc2
-	-	kIx~
rezidence	rezidence	k1gFnSc2
pražského	pražský	k2eAgMnSc2d1
primátora	primátor	k1gMnSc2
</s>
<s>
Muzea	muzeum	k1gNnPc4
<g/>
:	:	kIx,
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Moravská	moravský	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Severočeské	severočeský	k2eAgInPc1d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
Slezské	slezský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
<g/>
,	,	kIx,
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgFnPc1d1
tapisérie	tapisérie	k1gFnPc1
</s>
<s>
Gobelín	gobelín	k1gInSc1
podle	podle	k7c2
návrhu	návrh	k1gInSc2
Cyrila	Cyril	k1gMnSc2
Boudy	Bouda	k1gMnSc2
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1951	#num#	k4
</s>
<s>
Ateliéry	ateliér	k1gInPc1
</s>
<s>
Gobelínka	Gobelínka	k1gFnSc1
Marie	Maria	k1gFnSc2
Teinitzerové	Teinitzerová	k1gFnSc2
<g/>
,	,	kIx,
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1910	#num#	k4
(	(	kIx(
<g/>
web	web	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
gobelínová	gobelínový	k2eAgFnSc1d1
manufaktura	manufaktura	k1gFnSc1
ve	v	k7c6
Valašském	valašský	k2eAgNnSc6d1
Meziříčí	Meziříčí	k1gNnSc6
(	(	kIx(
<g/>
web	web	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ÚLUV	ÚLUV	kA
-	-	kIx~
Ústředí	ústředí	k1gNnSc1
lidové	lidový	k2eAgFnSc2d1
umělecké	umělecký	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Umělci	umělec	k1gMnPc1
</s>
<s>
Malíři	malíř	k1gMnPc1
-	-	kIx~
návrháři	návrhář	k1gMnPc1
často	často	k6eAd1
dávají	dávat	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc4
malby	malba	k1gFnPc4
tkadlenám	tkadlena	k1gFnPc3
převést	převést	k5eAaPmF
do	do	k7c2
techniky	technika	k1gFnSc2
tapisérie	tapisérie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
přehledu	přehled	k1gInSc6
jsou	být	k5eAaImIp3nP
jak	jak	k8xS,k8xC
malíři	malíř	k1gMnSc3
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
vystudovaní	vystudovaný	k2eAgMnPc1d1
textilní	textilní	k2eAgMnPc1d1
výtvarníci	výtvarník	k1gMnPc1
či	či	k8xC
tkadleny	tkadlena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
nadáním	nadání	k1gNnSc7
a	a	k8xC
praxí	praxe	k1gFnSc7
vypracovaly	vypracovat	k5eAaPmAgFnP
na	na	k7c4
výtvarnice	výtvarnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Karla	Karla	k1gFnSc1
Adámková	Adámková	k1gFnSc1
</s>
<s>
Libuše	Libuše	k1gFnSc1
Benešová	Benešová	k1gFnSc1
-	-	kIx~
Linhartová	Linhartová	k1gFnSc1
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1
Bloudková	Bloudkový	k2eAgFnSc1d1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Boďa	Boďa	k1gMnSc1
</s>
<s>
Viera	Viera	k6eAd1
Bošková	Boškový	k2eAgFnSc1d1
</s>
<s>
Eva	Eva	k1gFnSc1
Brodská-Bartoňová	Brodská-Bartoňová	k1gFnSc1
</s>
<s>
Miroslava	Miroslava	k1gFnSc1
Bradová	Bradová	k1gFnSc1
</s>
<s>
Věra	Věra	k1gFnSc1
Bruneová	Bruneová	k1gFnSc1
</s>
<s>
Valentina	Valentina	k1gFnSc1
Ceplechová-Kortanová	Ceplechová-Kortanový	k2eAgFnSc1d1
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Červenková-Schrutzová	Červenková-Schrutzová	k1gFnSc1
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Člek	Čleka	k1gFnPc2
</s>
<s>
Věra	Věra	k1gFnSc1
Drnková-Zářecká	Drnková-Zářecká	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Ducháč	Ducháč	k1gMnSc1
-	-	kIx~
Machov	Machov	k1gInSc1
</s>
<s>
Sylvia	Sylvia	k1gFnSc1
Fedorová	Fedorová	k1gFnSc1
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1
Felcman	Felcman	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Frýbl	Frýbl	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Fusek	Fusek	k1gMnSc1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Gutová-Pirklová	Gutová-Pirklová	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Halla	Halla	k1gMnSc1
</s>
<s>
Květa	Květa	k1gFnSc1
Hamsíková	Hamsíková	k1gFnSc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Hanzelka	Hanzelka	k1gMnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Hejcmanová-Moravčíková	Hejcmanová-Moravčíková	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Hladík	Hladík	k1gMnSc1
</s>
<s>
Jenny	Jenn	k1gInPc1
Hladíková	Hladíková	k1gFnSc1
</s>
<s>
Danuše	Danuše	k1gFnSc1
Hlobilová	Hlobilová	k1gFnSc1
</s>
<s>
Emanuel	Emanuel	k1gMnSc1
Holek	holka	k1gFnPc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
Holek	holka	k1gFnPc2
</s>
<s>
Brigita	Brigita	k1gFnSc1
Hollerová	Hollerová	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Hoppe-Teinitzerová	Hoppe-Teinitzerová	k1gFnSc1
</s>
<s>
Zdeňka	Zdeňka	k1gFnSc1
Hovorková-Vocásková	Hovorková-Vocásková	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Hrůzová-Maťhová	Hrůzová-Maťhová	k1gFnSc1
</s>
<s>
Jindra	Jindra	k1gFnSc1
Husáriková-Pelcová	Husáriková-Pelcová	k1gFnSc1
</s>
<s>
Alfréd	Alfréd	k1gMnSc1
Hynek	Hynek	k1gMnSc1
</s>
<s>
Dana	Dana	k1gFnSc1
Charvátová	Charvátová	k1gFnSc1
</s>
<s>
Dagmar	Dagmar	k1gFnSc1
Jandová	Jandová	k1gFnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Jeníček	Jeníček	k1gMnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Junková-Hellebrandová	Junková-Hellebrandový	k2eAgFnSc1d1
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
Kaprasová	Kaprasová	k1gFnSc1
</s>
<s>
Olga	Olga	k1gFnSc1
Karlíková-Nováková	Karlíková-Nováková	k1gFnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Kazda	Kazda	k1gMnSc1
</s>
<s>
Iris	iris	k1gFnSc1
Kolářová	Kolářová	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Kopáčová	Kopáčová	k1gFnSc1
</s>
<s>
Milada	Milada	k1gFnSc1
Koutná-Mohrová	Koutná-Mohrová	k1gFnSc1
</s>
<s>
Gabriela	Gabriela	k1gFnSc1
Kováčová-Tóthová	Kováčová-Tóthová	k1gFnSc1
</s>
<s>
Libuše	Libuše	k1gFnSc1
Kovářová-Miková	Kovářová-Mikový	k2eAgFnSc1d1
</s>
<s>
Hana	Hana	k1gFnSc1
Králová	Králová	k1gFnSc1
</s>
<s>
Milena	Milena	k1gFnSc1
Kráslová-Barvínková	Kráslová-Barvínkový	k2eAgFnSc1d1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Křečan	Křečan	k1gMnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
Kuchařová-Dorazilová	Kuchařová-Dorazilová	k1gFnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Kybal	Kybal	k1gMnSc1
-	-	kIx~
zakladatel	zakladatel	k1gMnSc1
české	český	k2eAgFnSc2d1
školy	škola	k1gFnSc2
tapisérie	tapisérie	k1gFnSc2
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
Kybalová	Kybalová	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Lapka	lapka	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Láznička	Láznička	k1gMnSc1
(	(	kIx(
<g/>
výtvarník	výtvarník	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Hana	Hana	k1gFnSc1
Lendrová-Vonkeová	Lendrová-Vonkeová	k1gFnSc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Machálek	Machálek	k1gMnSc1
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Machová	Machová	k1gFnSc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Masejdek	Masejdek	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Mejsnar	Mejsnar	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Měšťan	Měšťan	k1gMnSc1
</s>
<s>
Rosvita	Rosvita	k1gFnSc1
Mikulová-Korbelářová	Mikulová-Korbelářová	k1gFnSc1
</s>
<s>
Miluše	Miluše	k1gFnSc1
Mikušová	Mikušová	k1gFnSc1
</s>
<s>
Věra	Věra	k1gFnSc1
Milčinská	Milčinský	k2eAgFnSc1d1
</s>
<s>
Bohdan	Bohdan	k1gMnSc1
Mrázek	Mrázek	k1gMnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Mrázová	Mrázová	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
textilní	textilní	k2eAgMnSc1d1
výtvarník	výtvarník	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
vedl	vést	k5eAaImAgInS
dílny	dílna	k1gFnSc2
ÚUŘ	ÚUŘ	kA
v	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Hana	Hana	k1gFnSc1
Müllerová	Müllerová	k1gFnSc1
</s>
<s>
Hana	Hana	k1gFnSc1
Nálepová	Nálepový	k2eAgFnSc1d1
-	-	kIx~
se	s	k7c7
signaturou	signatura	k1gFnSc7
navrhujícího	navrhující	k2eAgMnSc2d1
manžela	manžel	k1gMnSc2
Josefa	Josef	k1gMnSc2
Nálepy	nálep	k1gInPc1
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
Neumannová	Neumannová	k1gFnSc1
</s>
<s>
Martina	Martina	k1gFnSc1
Novotná	Novotná	k1gFnSc1
(	(	kIx(
<g/>
výtvarnice	výtvarnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Jana	Jana	k1gFnSc1
Paulová-Rossmannová	Paulová-Rossmannová	k1gFnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pech	Pech	k1gMnSc1
</s>
<s>
Věra	Věra	k1gFnSc1
Pechová-Ponikelská	Pechová-Ponikelský	k2eAgFnSc1d1
</s>
<s>
Alice	Alice	k1gFnSc1
Pejchlová-Obdržálková	Pejchlová-Obdržálková	k1gFnSc1
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1
Pešicová	Pešicová	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Petrlík	petrlík	k1gInSc4
</s>
<s>
Josef	Josef	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
(	(	kIx(
<g/>
výtvarník	výtvarník	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Božena	Božena	k1gFnSc1
Pošepná	Pošepný	k2eAgFnSc1d1
</s>
<s>
Eva	Eva	k1gFnSc1
Prošková	Prošková	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Prudilová	Prudilová	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Příborská-Kerhartová	Příborská-Kerhartová	k1gFnSc1
</s>
<s>
Jindřiška	Jindřiška	k1gFnSc1
Radová-Švadlenková	Radová-Švadlenkový	k2eAgFnSc1d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Raková-Venhodová	Raková-Venhodový	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
Rauš	rauš	k1gInSc4
</s>
<s>
Jana	Jana	k1gFnSc1
Rossmannová	Rossmannová	k1gFnSc1
</s>
<s>
Tamara	Tamara	k1gFnSc1
Rotreklová-Matoušková	Rotreklová-Matoušková	k1gFnSc1
</s>
<s>
Adéla	Adéla	k1gFnSc1
Rozinková	rozinkový	k2eAgFnSc1d1
-	-	kIx~
tapiserie	tapiserie	k1gFnSc1
</s>
<s>
Renata	Renata	k1gFnSc1
Rozsívalová	Rozsívalová	k1gFnSc1
</s>
<s>
Blanka	Blanka	k1gFnSc1
Růžičková-Janíčková	Růžičková-Janíčková	k1gFnSc1
</s>
<s>
Sylva	Sylva	k1gFnSc1
Řepková-Koníčková	Řepková-Koníčková	k1gFnSc1
</s>
<s>
Zorka	Zorka	k1gFnSc1
Ságlová-Jirousová	Ságlová-Jirousový	k2eAgFnSc1d1
</s>
<s>
Radana	Radana	k1gFnSc1
Salmonová-Podzemná	Salmonová-Podzemný	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Sečkář	Sečkář	k1gMnSc1
</s>
<s>
Rosa	Rosa	k1gMnSc1
Servítovál	Servítovál	k1gMnSc1
</s>
<s>
Zorka	Zorka	k1gFnSc1
Smetanová-Járová	Smetanová-Járový	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Součková	Součková	k1gFnSc1
(	(	kIx(
<g/>
výtvarnice	výtvarnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
T.	T.	kA
Strýček	strýček	k1gMnSc1
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Strýčková-Věchtová	Strýčková-Věchtová	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Škochová-Barešová	Škochová-Barešová	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Helena	Helena	k1gFnSc1
Štecherová	Štecherová	k1gFnSc1
</s>
<s>
Danuše	Danuše	k1gFnSc1
Štěpánková	Štěpánková	k1gFnSc1
</s>
<s>
Zdenka	Zdenka	k1gFnSc1
Štipáková	Štipáková	k1gFnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Štulcová-Divilová	Štulcová-Divilová	k1gFnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Šupák	šupák	k1gInSc1
-	-	kIx~
[	[	kIx(
<g/>
www.modernigobeliny.cz	www.modernigobeliny.cz	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Květa	Květa	k1gFnSc1
Tajovská	Tajovská	k1gFnSc1
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1
Těšínská	Těšínská	k1gFnSc1
-	-	kIx~
tapiserie	tapiserie	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Tichý	Tichý	k1gMnSc1
(	(	kIx(
<g/>
výtvarník	výtvarník	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Tošenovská	Tošenovský	k2eAgFnSc1d1
</s>
<s>
Jarmila	Jarmila	k1gFnSc1
Trösterová	Trösterová	k1gFnSc1
</s>
<s>
Inez	Inez	k1gInSc1
Tuschnerová	Tuschnerová	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Vajceová-Bednářová	Vajceová-Bednářová	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Vávrová	Vávrová	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Večerník	večerník	k1gInSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Veleba	Veleba	k1gMnSc1
</s>
<s>
Růžena	Růžena	k1gFnSc1
Veselá	veselý	k2eAgFnSc1d1
</s>
<s>
Věra	Věra	k1gFnSc1
Vinařová	Vinařová	k1gFnSc1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Vítečková	Vítečková	k1gFnSc1
</s>
<s>
Oluša	Oluš	k2eAgFnSc1d1
Vítková	Vítková	k1gFnSc1
</s>
<s>
Tatiana	Tatiana	k1gFnSc1
Víznerová-Kratochvílová	Víznerová-Kratochvílová	k1gFnSc1
</s>
<s>
Dagmar	Dagmar	k1gFnSc1
Vlková	Vlková	k1gFnSc1
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1
Vodák	Vodák	k1gMnSc1
</s>
<s>
Radka	Radka	k1gFnSc1
Vodáková-Šrotová	Vodáková-Šrotový	k2eAgFnSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Vohánka	Vohánek	k1gMnSc2
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1
Vondráčková	Vondráčková	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Šimková	Šimková	k1gFnSc1
<g/>
,	,	kIx,
spoluzakladatelka	spoluzakladatelka	k1gFnSc1
oboru	obor	k1gInSc2
</s>
<s>
Vlasta	Vlasta	k1gFnSc1
Vopavová	Vopavový	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Záveská	Záveský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Žertová	Žertová	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
podkresba	podkresba	k1gFnSc1
byla	být	k5eAaImAgFnS
nalezena	naleznout	k5eAaPmNgFnS,k5eAaBmNgFnS
při	při	k7c6
restaurování	restaurování	k1gNnSc6
tapisérie	tapisérie	k1gFnSc2
ze	z	k7c2
zámku	zámek	k1gInSc2
v	v	k7c6
Benešově	Benešov	k1gInSc6
nad	nad	k7c7
Ploučnicí	Ploučnice	k1gFnSc7
<g/>
↑	↑	k?
Identifikace	identifikace	k1gFnSc2
značek	značka	k1gFnPc2
například	například	k6eAd1
E.	E.	kA
Müntz	Müntza	k1gFnPc2
<g/>
:	:	kIx,
Short	Short	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Tapestry	Tapestra	k1gFnSc2
<g/>
,	,	kIx,
1885	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
367	#num#	k4
<g/>
-	-	kIx~
<g/>
385	#num#	k4
<g/>
↑	↑	k?
Návod	návod	k1gInSc1
na	na	k7c4
vyšívání	vyšívání	k1gNnSc4
gobelínů	gobelín	k1gInPc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
http://www.experto.de/b2c/hobby-freizeit/gestalten-dekorieren/gobelinstickerei-eine-schritt-fuer-schritt-anleitung.html	http://www.experto.de/b2c/hobby-freizeit/gestalten-dekorieren/gobelinstickerei-eine-schritt-fuer-schritt-anleitung.htmnout	k5eAaPmAgMnS
<g/>
↑	↑	k?
Definice	definice	k1gFnPc4
malovaného	malovaný	k2eAgInSc2d1
gobelínu	gobelín	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://www.infobitte.de/free/lex/artsLex0/g/gobelinmalerei.htm	http://www.infobitte.de/free/lex/artsLex0/g/gobelinmalerei.htm	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
24	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Petitpoint	Petitpoint	k1gInSc4
výšivky	výšivka	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://www.petitpoint.eu/deutsch/geschichte.html	http://www.petitpoint.eu/deutsch/geschichte.html	k1gInSc1
<g/>
↑	↑	k?
Gobelínová	gobelínový	k2eAgFnSc1d1
příze	příze	k1gFnSc1
<g/>
:	:	kIx,
http://cz.texsite.info/Gobel%C3%ADnov%C3%A1_p%C5%99%C3%ADze	http://cz.texsite.info/Gobel%C3%ADnov%C3%A1_p%C5%99%C3%ADze	k1gFnSc1
<g/>
↑	↑	k?
Julien	Julien	k1gInSc1
Coffinet	Coffinet	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Pratique	Pratique	k1gFnSc1
de	de	k?
la	la	k1gNnSc7
tapisserie	tapisserie	k1gFnSc2
<g/>
,	,	kIx,
Genè	Genè	k1gFnSc2
<g/>
,	,	kIx,
Éditions	Éditions	k1gInSc1
du	du	k?
Tricorne	Tricorn	k1gInSc5
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
tapisérie	tapisérie	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://www.gutenberg.org/ebooks/20386	http://www.gutenberg.org/ebooks/20386	k4
<g/>
↑	↑	k?
Nejstarší	starý	k2eAgFnSc1d3
evropská	evropský	k2eAgFnSc1d1
tapisérie	tapisérie	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://www.dom-und-domschatz.de/domschatz.htm	http://www.dom-und-domschatz.de/domschatz.htm	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
6	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
2005	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
nejstarší	starý	k2eAgNnSc4d3
podle	podle	k7c2
Jarmily	Jarmila	k1gFnSc2
Blažkové	Blažková	k1gFnSc2
<g/>
,	,	kIx,
Tapisérie	tapisérie	k1gFnSc2
<g/>
...	...	k?
1975	#num#	k4
<g/>
,	,	kIx,
nestránkováno	stránkován	k2eNgNnSc1d1
<g/>
↑	↑	k?
Definice	definice	k1gFnPc4
gobelínu	gobelín	k1gInSc2
č.	č.	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://dunkelsuess.de/site/index.php/Lexikon/Gobelin	http://dunkelsuess.de/site/index.php/Lexikon/Gobelina	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
7	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Definice	definice	k1gFnPc4
gobelínu	gobelín	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
č.	č.	k?
<g/>
2	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://de.economypoint.org/g/gobelin-manufaktur.html	http://de.economypoint.org/g/gobelin-manufaktur.html	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Informace	informace	k1gFnPc1
o	o	k7c4
Manufacture	Manufactur	k1gMnSc5
des	des	k1gNnPc6
Gobelins	Gobelins	k1gInSc4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://www.uwefreund.com/paris/manufacturedegobelins.html	http://www.uwefreund.com/paris/manufacturedegobelins.html	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
14	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Gobelínka	Gobelínko	k1gNnPc1
ve	v	k7c6
Valašském	valašský	k2eAgNnSc6d1
Meziříčí	Meziříčí	k1gNnSc6
<g/>
:	:	kIx,
http://www.gobelin.cz/cs	http://www.gobelin.cz/cs	k6eAd1
<g/>
↑	↑	k?
https://www.nationaltrust.org.uk/features/great-tapestries-in-our-collections	https://www.nationaltrust.org.uk/features/great-tapestries-in-our-collections	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Koberec	koberec	k1gInSc1
</s>
<s>
Nástěnná	nástěnný	k2eAgFnSc1d1
malba	malba	k1gFnSc1
</s>
<s>
Textilní	textilní	k2eAgFnSc1d1
intarzie	intarzie	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BAUER	Bauer	k1gMnSc1
<g/>
,	,	kIx,
Rotraud	Rotraud	k1gMnSc1
<g/>
:	:	kIx,
Barocke	Barocke	k1gInSc1
Tapisserien	Tapisserien	k1gInSc1
aus	aus	k?
dem	dem	k?
Besitz	Besitz	k1gInSc1
des	des	k1gNnSc2
Kunsthistorischen	Kunsthistorischen	k2eAgInSc1d1
Museums	Museums	k1gInSc1
Wien	Wiena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
der	drát	k5eAaImRp2nS
Ausstellung	Ausstellung	k1gInSc1
in	in	k?
Schloss	Schloss	k1gInSc1
Halbturn	Halbturn	k1gInSc1
<g/>
,	,	kIx,
Wien	Wien	k1gInSc1
1975	#num#	k4
</s>
<s>
BLAŽKOVÁ	Blažková	k1gFnSc1
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
<g/>
:	:	kIx,
Tapiserie	tapiserie	k1gFnSc1
XVI	XVI	kA
<g/>
.	.	kIx.
-	-	kIx~
XVIII	XVIII	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
v	v	k7c6
Uměleckoprůmyslovém	uměleckoprůmyslový	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
UPM	UPM	kA
Praha	Praha	k1gFnSc1
1975	#num#	k4
</s>
<s>
BLAŽKOVÁ	Blažková	k1gFnSc1
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
<g/>
:	:	kIx,
Nástěnné	nástěnný	k2eAgInPc1d1
koberce	koberec	k1gInPc1
na	na	k7c6
státních	státní	k2eAgInPc6d1
zámcích	zámek	k1gInPc6
Hluboká	Hluboká	k1gFnSc1
a	a	k8xC
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympia	Olympia	k1gFnSc1
Praha	Praha	k1gFnSc1
1969	#num#	k4
</s>
<s>
CAMPBELL	CAMPBELL	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
P.	P.	kA
<g/>
:	:	kIx,
Henry	Henry	k1gMnSc1
VIII	VIII	kA
and	and	k?
the	the	k?
Art	Art	k1gFnSc2
of	of	k?
Majesty	Majest	k1gInPc4
<g/>
:	:	kIx,
Tapestries	Tapestries	k1gMnSc1
at	at	k?
the	the	k?
Tudor	tudor	k1gInSc1
Court	Court	k1gInSc1
<g/>
,	,	kIx,
Yale	Yale	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-300-12234-3	978-0-300-12234-3	k4
</s>
<s>
MRÁZ	Mráz	k1gMnSc1
<g/>
,	,	kIx,
Bohumír	Bohumír	k1gMnSc1
a	a	k8xC
MRÁZOVÁ	Mrázová	k1gFnSc1
<g/>
,	,	kIx,
Marcela	Marcela	k1gFnSc1
<g/>
:	:	kIx,
Současná	současný	k2eAgFnSc1d1
tapisérie	tapisérie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
1980	#num#	k4
</s>
<s>
MÜNTZ	MÜNTZ	kA
Eugè	Eugè	k1gInSc5
<g/>
:	:	kIx,
Short	Short	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Tapestry	Tapestra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
1885	#num#	k4
<g/>
,	,	kIx,
online	onlinout	k5eAaPmIp3nS
</s>
<s>
RUSSELL	RUSSELL	kA
<g/>
,	,	kIx,
Carol	Carol	k1gInSc1
K.	K.	kA
<g/>
:	:	kIx,
Tapestry	Tapestra	k1gFnSc2
Handbook	handbook	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnSc1
illustrated	illustrated	k1gMnSc1
Manual	Manual	k1gMnSc1
of	of	k?
Textile	textil	k1gInSc5
techniques	techniques	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Next	Nexta	k1gFnPc2
Generation	Generation	k1gInSc1
<g/>
,	,	kIx,
Schiffer	Schiffer	k1gInSc1
Publishing	Publishing	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Atglen	Atglen	k1gInSc1
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-7643-2756-8	978-0-7643-2756-8	k4
</s>
<s>
HENNIG	HENNIG	kA
Heinz	Heinz	k1gMnSc1
<g/>
,	,	kIx,
AIPPERSPACH	AIPPERSPACH	kA
Christa	Christa	k1gMnSc1
<g/>
,	,	kIx,
BAUER	Bauer	k1gMnSc1
Johann	Johann	k1gMnSc1
<g/>
:	:	kIx,
Gewebetechnik	Gewebetechnika	k1gFnPc2
<g/>
,	,	kIx,
VEB	Veba	k1gFnPc2
Fachbuchverlag	Fachbuchverlag	k1gMnSc1
Leipzig	Leipzig	k1gMnSc1
1978	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
229	#num#	k4
<g/>
-	-	kIx~
<g/>
235	#num#	k4
<g/>
;	;	kIx,
ISBN	ISBN	kA
978-3528041144	978-3528041144	k4
</s>
<s>
Denninger	Denninger	k1gInSc1
<g/>
/	/	kIx~
<g/>
Giese	Giese	k1gFnSc1
<g/>
:	:	kIx,
Textil-	Textil-	k1gFnSc1
und	und	k?
Modelexikon	Modelexikona	k1gFnPc2
<g/>
,	,	kIx,
Deutscher	Deutschra	k1gFnPc2
Fachverlag	Fachverlaga	k1gFnPc2
Frankfurt	Frankfurt	k1gInSc1
<g/>
/	/	kIx~
<g/>
Main	Main	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
87150	#num#	k4
<g/>
-	-	kIx~
<g/>
848	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
280	#num#	k4
</s>
<s>
TALAVÁŠEK	TALAVÁŠEK	kA
Oldřich	Oldřich	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
Tkalcovská	tkalcovský	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
Praha	Praha	k1gFnSc1
1980	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
142	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
tapiserie	tapiserie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
gobelín	gobelín	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
https://web.archive.org/web/20130129031809/http://www.wawel.krakow.pl/en/index.php?op=22,33	https://web.archive.org/web/20130129031809/http://www.wawel.krakow.pl/en/index.php?op=22,33	k4
</s>
<s>
http://www.all-art.org/history194-28.html	http://www.all-art.org/history194-28.html	k1gMnSc1
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc1d1
tapiserie	tapiserie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4006648-4	4006648-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
11810	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85132407	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85132407	#num#	k4
</s>
