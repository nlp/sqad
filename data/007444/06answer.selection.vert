<s>
Ježek	Ježek	k1gMnSc1	Ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nejznámější	známý	k2eAgInSc1d3	nejznámější
hlavolam	hlavolam	k1gInSc1	hlavolam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
dobrodružné	dobrodružný	k2eAgFnSc6d1	dobrodružná
knižní	knižní	k2eAgFnSc6d1	knižní
stínadelské	stínadelský	k2eAgFnSc6d1	stínadelská
trilogii	trilogie	k1gFnSc6	trilogie
od	od	k7c2	od
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
