<s>
Ježek	Ježek	k1gMnSc1	Ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nejznámější	známý	k2eAgInSc1d3	nejznámější
hlavolam	hlavolam	k1gInSc1	hlavolam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
dobrodružné	dobrodružný	k2eAgFnSc6d1	dobrodružná
knižní	knižní	k2eAgFnSc6d1	knižní
stínadelské	stínadelský	k2eAgFnSc6d1	stínadelská
trilogii	trilogie	k1gFnSc6	trilogie
od	od	k7c2	od
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
kuličky	kulička	k1gFnSc2	kulička
s	s	k7c7	s
různě	různě	k6eAd1	různě
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
ostny	osten	k1gInPc7	osten
(	(	kIx(	(
<g/>
ježek	ježek	k1gMnSc1	ježek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
uvnitř	uvnitř	k7c2	uvnitř
válcového	válcový	k2eAgNnSc2d1	válcové
pouzdra	pouzdro	k1gNnSc2	pouzdro
s	s	k7c7	s
různě	různě	k6eAd1	různě
velkými	velký	k2eAgInPc7d1	velký
otvory	otvor	k1gInPc7	otvor
(	(	kIx(	(
<g/>
klec	klec	k1gFnSc1	klec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
hlavolamu	hlavolam	k1gInSc2	hlavolam
je	být	k5eAaImIp3nS	být
vyndat	vyndat	k5eAaPmF	vyndat
ježka	ježek	k1gMnSc4	ježek
ven	ven	k6eAd1	ven
z	z	k7c2	z
klece	klec	k1gFnSc2	klec
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
obecně	obecně	k6eAd1	obecně
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavolam	hlavolam	k1gInSc1	hlavolam
je	být	k5eAaImIp3nS	být
vynálezem	vynález	k1gInSc7	vynález
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Foglara	Foglara	k1gFnSc1	Foglara
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
se	se	k3xPyFc4	se
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
patrně	patrně	k6eAd1	patrně
pouze	pouze	k6eAd1	pouze
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
již	již	k6eAd1	již
existujícím	existující	k2eAgInSc7d1	existující
hlavolamem	hlavolam	k1gInSc7	hlavolam
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
s	s	k7c7	s
ježkem	ježek	k1gMnSc7	ježek
setkáváme	setkávat	k5eAaImIp1nP	setkávat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgMnS	být
Ježek	Ježek	k1gMnSc1	Ježek
publikován	publikován	k2eAgMnSc1d1	publikován
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
US	US	kA	US
Trick	Trick	k1gMnSc1	Trick
&	&	k?	&
Novelty	Novelt	k1gInPc4	Novelt
Co	co	k9	co
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
!	!	kIx.	!
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
ježek	ježek	k1gMnSc1	ježek
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
novinách	novina	k1gFnPc6	novina
The	The	k1gMnSc1	The
American	American	k1gMnSc1	American
Stationer	Stationer	k1gMnSc1	Stationer
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
originál	originál	k1gInSc1	originál
<g/>
,	,	kIx,	,
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
zdánlivě	zdánlivě	k6eAd1	zdánlivě
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
bronzové	bronzový	k2eAgFnSc2d1	bronzová
slitiny	slitina	k1gFnSc2	slitina
s	s	k7c7	s
lakovaným	lakovaný	k2eAgInSc7d1	lakovaný
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
otvory	otvor	k1gInPc4	otvor
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
klece	klec	k1gFnSc2	klec
ježka	ježek	k1gMnSc2	ježek
se	s	k7c7	s
šesti	šest	k4xCc7	šest
kuželovitými	kuželovitý	k2eAgInPc7d1	kuželovitý
ostny	osten	k1gInPc7	osten
<g/>
.	.	kIx.	.
</s>
<s>
Klec	klec	k1gFnSc1	klec
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc1	dva
z	z	k7c2	z
otvorů	otvor	k1gInPc2	otvor
oválné	oválný	k2eAgFnSc2d1	oválná
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
jakési	jakýsi	k3yIgInPc1	jakýsi
zoubky	zoubek	k1gInPc1	zoubek
a	a	k8xC	a
zářezy	zářez	k1gInPc1	zářez
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
kdo	kdo	k3yQnSc1	kdo
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
hlavolam	hlavolam	k1gInSc4	hlavolam
na	na	k7c6	na
principu	princip	k1gInSc6	princip
ježka	ježek	k1gMnSc2	ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
patentovat	patentovat	k5eAaBmF	patentovat
byl	být	k5eAaImAgInS	být
jistý	jistý	k2eAgMnSc1d1	jistý
Clarence	Clarenec	k1gInPc4	Clarenec
A.	A.	kA	A.
Worrall	Worrall	k1gInSc4	Worrall
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Foglar	Foglar	k1gMnSc1	Foglar
byl	být	k5eAaImAgMnS	být
údajně	údajně	k6eAd1	údajně
prvním	první	k4xOgInPc3	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
šroubovacího	šroubovací	k2eAgMnSc2d1	šroubovací
ježka	ježek	k1gMnSc2	ježek
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
Rychlých	Rychlých	k2eAgInPc6d1	Rychlých
šípech	šíp	k1gInPc6	šíp
a	a	k8xC	a
ježkovi	ježkův	k2eAgMnPc1d1	ježkův
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Mladý	mladý	k2eAgMnSc1d1	mladý
hlasatel	hlasatel	k1gMnSc1	hlasatel
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
objevuje	objevovat	k5eAaImIp3nS	objevovat
první	první	k4xOgMnSc1	první
ježek	ježek	k1gMnSc1	ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
ho	on	k3xPp3gNnSc4	on
firma	firma	k1gFnSc1	firma
K.	K.	kA	K.
M.	M.	kA	M.
Moučky	Moučka	k1gMnPc7	Moučka
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
knižní	knižní	k2eAgFnSc2d1	knižní
předlohy	předloha	k1gFnSc2	předloha
byl	být	k5eAaImAgInS	být
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
a	a	k8xC	a
nešel	jít	k5eNaImAgMnS	jít
rozšroubovat	rozšroubovat	k5eAaPmF	rozšroubovat
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgMnSc2	první
šroubovacího	šroubovací	k2eAgMnSc2d1	šroubovací
ježka	ježek	k1gMnSc2	ježek
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
dle	dle	k7c2	dle
podoby	podoba	k1gFnSc2	podoba
z	z	k7c2	z
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
produkce	produkce	k1gFnSc1	produkce
originálních	originální	k2eAgInPc2d1	originální
českých	český	k2eAgInPc2d1	český
hlavolamů	hlavolam	k1gInPc2	hlavolam
RADEMIC	RADEMIC	kA	RADEMIC
PUZZLES	PUZZLES	kA	PUZZLES
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Širší	široký	k2eAgFnSc2d2	širší
čtenářské	čtenářský	k2eAgFnSc2d1	čtenářská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
hlavolam	hlavolam	k1gInSc4	hlavolam
prvně	prvně	k?	prvně
představil	představit	k5eAaPmAgMnS	představit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
stínadelském	stínadelský	k2eAgInSc6d1	stínadelský
příběhu	příběh	k1gInSc6	příběh
Záhada	záhada	k1gFnSc1	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgMnSc7d1	původní
vlastníkem	vlastník	k1gMnSc7	vlastník
hlavolamu	hlavolam	k1gInSc2	hlavolam
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
románu	román	k1gInSc6	román
zámečnický	zámečnický	k2eAgMnSc1d1	zámečnický
učeň	učeň	k1gMnSc1	učeň
Jan	Jan	k1gMnSc1	Jan
Tleskač	tleskač	k1gMnSc1	tleskač
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ježka	ježek	k1gMnSc2	ježek
z	z	k7c2	z
klece	klec	k1gFnSc2	klec
vyndat	vyndat	k5eAaPmF	vyndat
<g/>
,	,	kIx,	,
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
dvě	dva	k4xCgFnPc1	dva
polokoule	polokoule	k1gFnPc1	polokoule
spojené	spojený	k2eAgFnPc1d1	spojená
závitem	závit	k1gInSc7	závit
a	a	k8xC	a
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
uvnitř	uvnitř	k6eAd1	uvnitř
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
plánek	plánek	k1gInSc4	plánek
vlastního	vlastní	k2eAgInSc2d1	vlastní
vynálezu	vynález	k1gInSc2	vynález
–	–	k?	–
létajícího	létající	k2eAgNnSc2d1	létající
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Tleskačově	tleskačův	k2eAgFnSc6d1	Tleskačova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
hlavolam	hlavolam	k1gInSc1	hlavolam
stal	stát	k5eAaPmAgInS	stát
odznakem	odznak	k1gInSc7	odznak
moci	moc	k1gFnSc2	moc
vůdce	vůdce	k1gMnSc2	vůdce
vontské	vontský	k2eAgFnSc2d1	vontská
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
získání	získání	k1gNnSc6	získání
hlavolamu	hlavolam	k1gInSc2	hlavolam
usilují	usilovat	k5eAaImIp3nP	usilovat
také	také	k9	také
Foglarovi	Foglarův	k2eAgMnPc1d1	Foglarův
hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
chlapecký	chlapecký	k2eAgInSc4d1	chlapecký
klub	klub	k1gInSc4	klub
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
náhodou	náhodou	k6eAd1	náhodou
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
o	o	k7c4	o
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
<g/>
.	.	kIx.	.
</s>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ostnatý	ostnatý	k2eAgInSc1d1	ostnatý
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
ostny	osten	k1gInPc1	osten
mají	mít	k5eAaImIp3nP	mít
nestejné	stejný	k2eNgInPc1d1	nestejný
rozměry	rozměr	k1gInPc1	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
ve	v	k7c6	v
válci	válec	k1gInSc6	válec
s	s	k7c7	s
nevyplněnými	vyplněný	k2eNgFnPc7d1	nevyplněná
stěnami	stěna	k1gFnPc7	stěna
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
něž	jenž	k3xRgNnSc4	jenž
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
ježka	ježek	k1gMnSc2	ježek
důmyslným	důmyslný	k2eAgNnPc3d1	důmyslné
natáčením	natáčení	k1gNnPc3	natáčení
protáhnout	protáhnout	k5eAaPmF	protáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Hlavolam	hlavolam	k1gInSc1	hlavolam
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
dá	dát	k5eAaPmIp3nS	dát
zakoupit	zakoupit	k5eAaPmF	zakoupit
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
obchodech	obchod	k1gInPc6	obchod
v	v	k7c6	v
kovovém	kovový	k2eAgNnSc6d1	kovové
<g/>
,	,	kIx,	,
dřevěném	dřevěný	k2eAgNnSc6d1	dřevěné
<g/>
,	,	kIx,	,
umělohmotném	umělohmotný	k2eAgNnSc6d1	umělohmotné
i	i	k8xC	i
jiném	jiný	k2eAgNnSc6d1	jiné
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
potkat	potkat	k5eAaPmF	potkat
i	i	k9	i
s	s	k7c7	s
variantami	varianta	k1gFnPc7	varianta
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
neschovává	schovávat	k5eNaImIp3nS	schovávat
ježek	ježek	k1gMnSc1	ježek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
lev	lev	k1gMnSc1	lev
nebo	nebo	k8xC	nebo
kanárek	kanárek	k1gMnSc1	kanárek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavolam	hlavolam	k1gInSc1	hlavolam
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
dovednostní	dovednostní	k2eAgFnSc7d1	dovednostní
hříčkou	hříčka	k1gFnSc7	hříčka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
legendárního	legendární	k2eAgMnSc2d1	legendární
komiksu	komiks	k1gInSc6	komiks
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
přibyl	přibýt	k5eAaPmAgInS	přibýt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
k	k	k7c3	k
již	již	k6eAd1	již
existujícím	existující	k2eAgInPc3d1	existující
obřím	obří	k2eAgInPc3d1	obří
exponátům	exponát	k1gInPc3	exponát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čajová	čajový	k2eAgFnSc1d1	čajová
konvice	konvice	k1gFnSc1	konvice
<g/>
,	,	kIx,	,
hrnec	hrnec	k1gInSc1	hrnec
<g/>
,	,	kIx,	,
mamut	mamut	k1gMnSc1	mamut
<g/>
)	)	kIx)	)
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
muzea	muzeum	k1gNnSc2	muzeum
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
české	český	k2eAgFnSc2d1	Česká
ručičky	ručička	k1gFnSc2	ručička
(	(	kIx(	(
<g/>
samostatná	samostatný	k2eAgFnSc1d1	samostatná
část	část	k1gFnSc1	část
Muzea	muzeum	k1gNnSc2	muzeum
rekordů	rekord	k1gInPc2	rekord
a	a	k8xC	a
kuriozit	kuriozita	k1gFnPc2	kuriozita
v	v	k7c6	v
Pelhřimově	Pelhřimov	k1gInSc6	Pelhřimov
<g/>
)	)	kIx)	)
také	také	k9	také
největší	veliký	k2eAgMnSc1d3	veliký
ježek	ježek	k1gMnSc1	ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
patnáctinásobnou	patnáctinásobný	k2eAgFnSc4d1	patnáctinásobný
zvětšeninu	zvětšenina	k1gFnSc4	zvětšenina
hlavolamu	hlavolam	k1gInSc2	hlavolam
<g/>
:	:	kIx,	:
klec	klec	k1gFnSc1	klec
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
110	[number]	k4	110
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
73	[number]	k4	73
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
ježek	ježek	k1gMnSc1	ježek
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
30	[number]	k4	30
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgInSc1d1	obří
hlavolam	hlavolam	k1gInSc1	hlavolam
společně	společně	k6eAd1	společně
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
odborné	odborný	k2eAgFnPc4d1	odborná
učiliště	učiliště	k1gNnSc2	učiliště
Horní	horní	k2eAgInSc1d1	horní
Slavkov	Slavkov	k1gInSc1	Slavkov
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
NADE	nad	k7c4	nad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
další	další	k2eAgInSc4d1	další
informační	informační	k2eAgInSc4d1	informační
panel	panel	k1gInSc4	panel
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
památce	památka	k1gFnSc3	památka
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
a	a	k8xC	a
jako	jako	k9	jako
poděkování	poděkování	k1gNnSc4	poděkování
za	za	k7c4	za
jeho	jeho	k3xOp3gInPc4	jeho
příběhy	příběh	k1gInPc4	příběh
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
zahradu	zahrada	k1gFnSc4	zahrada
vysazen	vysazen	k2eAgInSc1d1	vysazen
jinan	jinan	k1gInSc1	jinan
dvoulaločný	dvoulaločný	k2eAgInSc1d1	dvoulaločný
(	(	kIx(	(
<g/>
ginkgo	ginkgo	k1gNnSc1	ginkgo
biloba	biloba	k1gFnSc1	biloba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
