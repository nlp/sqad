<s>
V	v	k7c6	v
programování	programování	k1gNnSc6	programování
a	a	k8xC	a
robotice	robotika	k1gFnSc6	robotika
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
evoluce	evoluce	k1gFnSc2	evoluce
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
optimalizační	optimalizační	k2eAgFnSc4d1	optimalizační
metodu	metoda	k1gFnSc4	metoda
genetického	genetický	k2eAgNnSc2d1	genetické
programování	programování	k1gNnSc2	programování
(	(	kIx(	(
<g/>
obecněji	obecně	k6eAd2	obecně
evolučního	evoluční	k2eAgNnSc2d1	evoluční
programování	programování	k1gNnSc2	programování
<g/>
)	)	kIx)	)
připomínající	připomínající	k2eAgFnSc4d1	připomínající
biologickou	biologický	k2eAgFnSc4d1	biologická
evoluci	evoluce	k1gFnSc4	evoluce
<g/>
:	:	kIx,	:
Vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
heuristická	heuristický	k2eAgFnSc1d1	heuristická
funkce	funkce	k1gFnSc1	funkce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fitness	fitness	k1gInSc1	fitness
funkce	funkce	k1gFnSc1	funkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
ohodnotí	ohodnotit	k5eAaPmIp3nS	ohodnotit
chování	chování	k1gNnSc1	chování
populace	populace	k1gFnSc2	populace
robotů	robot	k1gInPc2	robot
(	(	kIx(	(
<g/>
skutečných	skutečný	k2eAgMnPc2d1	skutečný
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
emulovaných	emulovaný	k2eAgInPc2d1	emulovaný
software	software	k1gInSc4	software
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
populace	populace	k1gFnSc1	populace
křížením	křížení	k1gNnSc7	křížení
a	a	k8xC	a
náhodnou	náhodný	k2eAgFnSc7d1	náhodná
modifikací	modifikace	k1gFnSc7	modifikace
těch	ten	k3xDgMnPc2	ten
(	(	kIx(	(
<g/>
nej	nej	k?	nej
<g/>
)	)	kIx)	)
<g/>
úspěšnějších	úspěšný	k2eAgFnPc2d2	úspěšnější
<g/>
.	.	kIx.	.
</s>
