<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
před	před	k7c7	před
Napoleonem	napoleon	k1gInSc7	napoleon
portugalský	portugalský	k2eAgInSc1d1	portugalský
královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
aktů	akt	k1gInPc2	akt
monarchy	monarcha	k1gMnSc2	monarcha
bylo	být	k5eAaImAgNnS	být
otevření	otevření	k1gNnSc1	otevření
brazilských	brazilský	k2eAgInPc2d1	brazilský
přístavů	přístav	k1gInPc2	přístav
cizím	cizí	k2eAgFnPc3d1	cizí
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
