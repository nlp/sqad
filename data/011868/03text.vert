<p>
<s>
Haugsdorf	Haugsdorf	k1gInSc1	Haugsdorf
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgNnSc4d1	obchodní
město	město	k1gNnSc4	město
s	s	k7c7	s
1569	[number]	k4	1569
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hollabrunn	Hollabrunna	k1gFnPc2	Hollabrunna
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Haugsdorf	Haugsdorf	k1gInSc1	Haugsdorf
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
Weinviertel	Weinviertel	k1gInSc1	Weinviertel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
prochází	procházet	k5eAaImIp3nS	procházet
zemská	zemský	k2eAgFnSc1d1	zemská
silnice	silnice	k1gFnSc1	silnice
B	B	kA	B
<g/>
303	[number]	k4	303
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
města	město	k1gNnSc2	město
Znojmo	Znojmo	k1gNnSc4	Znojmo
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
Stockerau	Stockeraa	k1gFnSc4	Stockeraa
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Členění	členění	k1gNnSc1	členění
obce	obec	k1gFnSc2	obec
===	===	k?	===
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
osad	osada	k1gFnPc2	osada
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Auggenthal	Auggenthal	k1gMnSc1	Auggenthal
</s>
</p>
<p>
<s>
Haugsdorf	Haugsdorf	k1gMnSc1	Haugsdorf
</s>
</p>
<p>
<s>
Jetzelsdorf	Jetzelsdorf	k1gInSc1	Jetzelsdorf
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
říčky	říčka	k1gFnSc2	říčka
Pulkava	Pulkava	k1gFnSc1	Pulkava
(	(	kIx(	(
<g/>
Pulkau	Pulkaus	k1gInSc2	Pulkaus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kleinhaugsdorf	Kleinhaugsdorf	k1gMnSc1	Kleinhaugsdorf
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgMnSc1d1	ležící
severně	severně	k6eAd1	severně
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
kopcích	kopec	k1gInPc6	kopec
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
osady	osada	k1gFnSc2	osada
Kleinhaugsdorf	Kleinhaugsdorf	k1gInSc1	Kleinhaugsdorf
je	být	k5eAaImIp3nS	být
hraniční	hraniční	k2eAgInSc4d1	hraniční
přechod	přechod	k1gInSc4	přechod
z	z	k7c2	z
ČR	ČR	kA	ČR
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
:	:	kIx,	:
Hatě	hať	k1gFnSc2	hať
–	–	k?	–
Kleinhaugsdorf	Kleinhaugsdorf	k1gMnSc1	Kleinhaugsdorf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Haugsdorf	Haugsdorf	k1gInSc1	Haugsdorf
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
