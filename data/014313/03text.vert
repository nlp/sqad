<s>
GSM	GSM	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
GSM	GSM	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
francouzského	francouzský	k2eAgMnSc2d1
„	„	k?
<g/>
Groupe	Groupe	k1gFnSc1
Spécial	Spécial	k2eAgFnSc1d1
Mobile	mobile	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
celosvětově	celosvětově	k6eAd1
nejrozšířenější	rozšířený	k2eAgInSc1d3
standard	standard	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vytvořil	vytvořit	k5eAaPmAgInS
Evropský	evropský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
(	(	kIx(
<g/>
ETSI	ETSI	kA
<g/>
)	)	kIx)
pro	pro	k7c4
digitální	digitální	k2eAgFnPc4d1
mobilní	mobilní	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
označované	označovaný	k2eAgFnPc4d1
jako	jako	k8xS,k8xC
2	#num#	k4
<g/>
G.	G.	kA
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
zprovozněn	zprovoznit	k5eAaPmNgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1991	#num#	k4
ve	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
Finsku	Finsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
má	mít	k5eAaImIp3nS
90	#num#	k4
<g/>
%	%	kIx~
podíl	podíl	k1gInSc4
na	na	k7c6
trhu	trh	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
ve	v	k7c6
193	#num#	k4
zemích	zem	k1gFnPc6
a	a	k8xC
teritoriích	teritorium	k1gNnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgInS
standard	standard	k1gInSc1
doplněn	doplnit	k5eAaPmNgInS
o	o	k7c4
datové	datový	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
(	(	kIx(
<g/>
GPRS	GPRS	kA
<g/>
,	,	kIx,
EDGE	EDGE	kA
<g/>
)	)	kIx)
využívané	využívaný	k2eAgNnSc1d1
zejména	zejména	k9
pro	pro	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
Internetu	Internet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchůdcem	předchůdce	k1gMnSc7
GSM	GSM	kA
byly	být	k5eAaImAgFnP
analogové	analogový	k2eAgFnPc4d1
mobilní	mobilní	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
1	#num#	k4
<g/>
G.	G.	kA
Nástupcem	nástupce	k1gMnSc7
jsou	být	k5eAaImIp3nP
sítě	síť	k1gFnSc2
třetí	třetí	k4xOgFnSc2
generace	generace	k1gFnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
standardu	standard	k1gInSc2
UMTS	UMTS	kA
<g/>
,	,	kIx,
čtvrté	čtvrtý	k4xOgFnPc1
generace	generace	k1gFnPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
a	a	k8xC
páté	pátý	k4xOgFnSc2
generace	generace	k1gFnSc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
GSM	GSM	kA
telefony	telefon	k1gInPc1
z	z	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
</s>
<s>
Jméno	jméno	k1gNnSc1
systému	systém	k1gInSc2
GSM	GSM	kA
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
názvu	název	k1gInSc2
pracovní	pracovní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Groupe	Group	k1gInSc5
Spécial	Spécial	k1gMnSc1
Mobile	mobile	k1gNnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
standardu	standard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GSM	GSM	kA
je	být	k5eAaImIp3nS
ochranná	ochranný	k2eAgFnSc1d1
známka	známka	k1gFnSc1
organizace	organizace	k1gFnSc2
GSMA	GSMA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
dnešní	dnešní	k2eAgFnSc3d1
popularitě	popularita	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
GSM	GSM	kA
založena	založen	k2eAgFnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
bude	být	k5eAaImBp3nS
používat	používat	k5eAaImF
jen	jen	k9
malý	malý	k2eAgInSc4d1
zlomek	zlomek	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
uváděny	uvádět	k5eAaImNgFnP
do	do	k7c2
provozu	provoz	k1gInSc2
buňkové	buňkový	k2eAgFnSc2d1
mobilní	mobilní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
první	první	k4xOgFnSc2
generace	generace	k1gFnSc2
systémů	systém	k1gInPc2
NMT	NMT	kA
a	a	k8xC
AMPS	AMPS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telefony	telefon	k1gInPc1
pro	pro	k7c4
tyto	tento	k3xDgFnPc4
sítě	síť	k1gFnPc4
nebyly	být	k5eNaImAgInP
přenosné	přenosný	k2eAgInPc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
hmotnosti	hmotnost	k1gFnSc3
<g/>
,	,	kIx,
spotřebě	spotřeba	k1gFnSc3
a	a	k8xC
rozměrům	rozměr	k1gInPc3
se	se	k3xPyFc4
montovaly	montovat	k5eAaImAgFnP
do	do	k7c2
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Signál	signál	k1gInSc1
pokrýval	pokrývat	k5eAaImAgInS
pouze	pouze	k6eAd1
největší	veliký	k2eAgInSc1d3
města	město	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
blízké	blízký	k2eAgNnSc4d1
okolí	okolí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
ceně	cena	k1gFnSc3
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
a	a	k8xC
hovorného	hovorné	k1gNnSc2
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
luxusní	luxusní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
i	i	k9
pro	pro	k7c4
obyvatele	obyvatel	k1gMnPc4
vyspělých	vyspělý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepříjemná	příjemný	k2eNgFnSc1d1
byla	být	k5eAaImAgFnS
i	i	k9
nejednotnost	nejednotnost	k1gFnSc1
systémů	systém	k1gInPc2
v	v	k7c6
různých	různý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
neexistence	neexistence	k1gFnSc2
roamingových	roamingový	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
mezi	mezi	k7c7
operátory	operátor	k1gMnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
omezovalo	omezovat	k5eAaImAgNnS
použitelnost	použitelnost	k1gFnSc4
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
na	na	k7c4
jednu	jeden	k4xCgFnSc4
nebo	nebo	k8xC
několik	několik	k4yIc1
málo	málo	k4c1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Skupina	skupina	k1gFnSc1
GSM	GSM	kA
navrhla	navrhnout	k5eAaPmAgFnS
systém	systém	k1gInSc4
používající	používající	k2eAgInSc4d1
digitální	digitální	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
pro	pro	k7c4
přenos	přenos	k1gInSc4
hovorů	hovor	k1gInPc2
i	i	k8xC
signalizace	signalizace	k1gFnSc2
<g/>
;	;	kIx,
digitální	digitální	k2eAgInPc1d1
systémy	systém	k1gInPc1
mobilní	mobilní	k2eAgFnSc2d1
telefonie	telefonie	k1gFnSc2
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
systémy	systém	k1gInPc1
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
digitální	digitální	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
poskytování	poskytování	k1gNnSc1
dalších	další	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
komunikace	komunikace	k1gFnSc2
pomocí	pomocí	k7c2
krátkých	krátká	k1gFnPc2
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
(	(	kIx(
<g/>
SMS	SMS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
datových	datový	k2eAgInPc2d1
přenosů	přenos	k1gInPc2
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
menší	malý	k2eAgFnPc4d2
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
zvyšují	zvyšovat	k5eAaImIp3nP
komfort	komfort	k1gInSc4
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
zobrazování	zobrazování	k1gNnSc4
čísla	číslo	k1gNnSc2
volajícího	volající	k2eAgNnSc2d1
<g/>
,	,	kIx,
hlasová	hlasový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
<g/>
,	,	kIx,
přesměrování	přesměrování	k1gNnSc1
hovorů	hovor	k1gInPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitalizace	digitalizace	k1gFnSc1
přináší	přinášet	k5eAaImIp3nS
zvýšení	zvýšení	k1gNnSc4
kvality	kvalita	k1gFnSc2
zvuku	zvuk	k1gInSc2
<g/>
,	,	kIx,
značně	značně	k6eAd1
ztěžuje	ztěžovat	k5eAaImIp3nS
odposlech	odposlech	k1gInSc1
hovorů	hovor	k1gInPc2
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
i	i	k9
jejich	jejich	k3xOp3gNnSc4
šifrování	šifrování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
časového	časový	k2eAgNnSc2d1
dělení	dělení	k1gNnSc2
umožňuje	umožňovat	k5eAaImIp3nS
využití	využití	k1gNnSc3
jednoho	jeden	k4xCgInSc2
kmitočtu	kmitočet	k1gInSc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
buňce	buňka	k1gFnSc6
více	hodně	k6eAd2
uživateli	uživatel	k1gMnPc7
současně	současně	k6eAd1
a	a	k8xC
zároveň	zároveň	k6eAd1
značně	značně	k6eAd1
snižuje	snižovat	k5eAaImIp3nS
spotřebu	spotřeba	k1gFnSc4
(	(	kIx(
<g/>
telefon	telefon	k1gInSc4
vysílá	vysílat	k5eAaImIp3nS
jenom	jenom	k9
zlomek	zlomek	k1gInSc4
času	čas	k1gInSc2
hovoru	hovor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
SIM	sima	k1gFnPc2
karty	karta	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
snadnou	snadný	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
mobilního	mobilní	k2eAgInSc2d1
telefonu	telefon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Skupina	skupina	k1gFnSc1
GSM	GSM	kA
původně	původně	k6eAd1
patřila	patřit	k5eAaImAgFnS
CEPT	CEPT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technické	technický	k2eAgInPc1d1
základy	základ	k1gInPc1
systému	systém	k1gInSc2
GSM	GSM	kA
byly	být	k5eAaImAgFnP
definovány	definovat	k5eAaBmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
převzala	převzít	k5eAaPmAgFnS
kontrolu	kontrola	k1gFnSc4
ETSI	ETSI	kA
a	a	k8xC
kolem	kolem	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
specifika	specifika	k1gFnSc1
GSM	GSM	kA
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
obsahovala	obsahovat	k5eAaImAgFnS
přes	přes	k7c4
6000	#num#	k4
stran	strana	k1gFnPc2
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodní	obchodní	k2eAgFnSc1d1
operace	operace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
Radiolinja	Radiolinjus	k1gMnSc4
z	z	k7c2
Finska	Finsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zachovají	zachovat	k5eAaPmIp3nP
iniciály	iniciála	k1gFnPc1
GSM	GSM	kA
<g/>
,	,	kIx,
ale	ale	k8xC
změní	změnit	k5eAaPmIp3nS
se	se	k3xPyFc4
význam	význam	k1gInSc1
zkratky	zkratka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
datových	datový	k2eAgInPc2d1
přenosů	přenos	k1gInPc2
CSD	CSD	kA
(	(	kIx(
<g/>
Circuit	Circuit	k2eAgInSc1d1
Switched	Switched	k1gInSc1
Data	datum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
platí	platit	k5eAaImIp3nP
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
telefonní	telefonní	k2eAgInPc4d1
hovory	hovor	k1gInPc4
<g/>
,	,	kIx,
zavádí	zavádět	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
GSM	GSM	kA
standard	standard	k1gInSc4
levnější	levný	k2eAgInPc4d2
datové	datový	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
pomocí	pomocí	k7c2
paketových	paketový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
pod	pod	k7c7
zkratkou	zkratka	k1gFnSc7
GPRS	GPRS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
i	i	k9
zvyšování	zvyšování	k1gNnSc4
rychlosti	rychlost	k1gFnSc2
přenosu	přenos	k1gInSc2
dat	datum	k1gNnPc2
–	–	k?
EDGE	EDGE	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
byl	být	k5eAaImAgInS
zformován	zformován	k2eAgInSc1d1
Projekt	projekt	k1gInSc1
Partnerství	partnerství	k1gNnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generace	generace	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
GPP	GPP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
měl	mít	k5eAaImAgInS
pouze	pouze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
specifikaci	specifikace	k1gFnSc4
pro	pro	k7c4
příští	příští	k2eAgInSc4d1
(	(	kIx(
<g/>
třetí	třetí	k4xOgFnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
generaci	generace	k1gFnSc4
mobilních	mobilní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
3GPP	3GPP	k4
převzal	převzít	k5eAaPmAgInS
také	také	k9
údržbu	údržba	k1gFnSc4
a	a	k8xC
vývoj	vývoj	k1gInSc1
GSM	GSM	kA
specifikace	specifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ETSI	ETSI	kA
je	být	k5eAaImIp3nS
partnerem	partner	k1gMnSc7
3	#num#	k4
<g/>
GPP	GPP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
projektu	projekt	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
publikována	publikován	k2eAgFnSc1d1
specifikace	specifikace	k1gFnSc1
Universal	Universal	k1gFnSc2
Mobile	mobile	k1gNnSc2
Telecommunications	Telecommunicationsa	k1gFnPc2
System	Syst	k1gInSc7
(	(	kIx(
<g/>
UMTS	UMTS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
<g/>
,	,	kIx,
přestože	přestože	k8xS
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
kódového	kódový	k2eAgInSc2d1
multiplexu	multiplex	k1gInSc2
používaného	používaný	k2eAgInSc2d1
v	v	k7c6
systému	systém	k1gInSc6
CDMA	CDMA	kA
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pokračování	pokračování	k1gNnSc1
projektu	projekt	k1gInSc2
GSM	GSM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
uživatelů	uživatel	k1gMnPc2
„	„	k?
<g/>
mobilních	mobilní	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
“	“	k?
v	v	k7c6
síti	síť	k1gFnSc6
„	„	k?
<g/>
GSM	GSM	kA
<g/>
“	“	k?
si	se	k3xPyFc3
ani	ani	k9
neuvědomuje	uvědomovat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
používají	používat	k5eAaImIp3nP
síť	síť	k1gFnSc4
UMTS	UMTS	kA
<g/>
.	.	kIx.
</s>
<s>
Všudypřítomnost	všudypřítomnost	k1gFnSc1
GSM	GSM	kA
standardu	standard	k1gInSc2
a	a	k8xC
„	„	k?
<g/>
roamingové	roamingový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
“	“	k?
mezi	mezi	k7c7
mobilními	mobilní	k2eAgInPc7d1
operátory	operátor	k1gInPc7
dělají	dělat	k5eAaImIp3nP
z	z	k7c2
mezinárodního	mezinárodní	k2eAgNnSc2d1
telefonování	telefonování	k1gNnSc2
běžnou	běžný	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
svůj	svůj	k3xOyFgInSc4
vývoj	vývoj	k1gInSc4
zachovává	zachovávat	k5eAaImIp3nS
GSM	GSM	kA
zpětnou	zpětný	k2eAgFnSc4d1
kompatibilitu	kompatibilita	k1gFnSc4
s	s	k7c7
původními	původní	k2eAgInPc7d1
GSM	GSM	kA
telefony	telefon	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GSM	GSM	kA
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
vyvíjeno	vyvíjet	k5eAaImNgNnS
skupinou	skupina	k1gFnSc7
3GPP	3GPP	k4
převážně	převážně	k6eAd1
jako	jako	k8xC,k8xS
otevřený	otevřený	k2eAgInSc4d1
standard	standard	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1
situace	situace	k1gFnSc1
</s>
<s>
Telefony	telefon	k1gInPc1
podle	podle	k7c2
standardů	standard	k1gInPc2
z	z	k7c2
rodiny	rodina	k1gFnSc2
GSM	GSM	kA
(	(	kIx(
<g/>
GSM	GSM	kA
<g/>
,	,	kIx,
EDGE	EDGE	kA
<g/>
,	,	kIx,
UMTS	UMTS	kA
W-CDMA	W-CDMA	k1gFnSc1
a	a	k8xC
LTE	LTE	kA
<g/>
)	)	kIx)
používá	používat	k5eAaImIp3nS
celosvětově	celosvětově	k6eAd1
téměř	téměř	k6eAd1
90	#num#	k4
%	%	kIx~
mobilních	mobilní	k2eAgMnPc2d1
účastníků	účastník	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2011	#num#	k4
používalo	používat	k5eAaImAgNnS
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
(	(	kIx(
<g/>
všech	všecek	k3xTgInPc2
standardů	standard	k1gInPc2
<g/>
)	)	kIx)
přibližně	přibližně	k6eAd1
5,9	5,9	k4
miliardy	miliarda	k4xCgFnSc2
účastníků	účastník	k1gMnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgMnSc7d3
soupeřem	soupeř	k1gMnSc7
GSM	GSM	kA
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
CDMA	CDMA	kA
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2011	#num#	k4
používalo	používat	k5eAaImAgNnS
přibližně	přibližně	k6eAd1
626	#num#	k4
miliónů	milión	k4xCgInPc2
účastníků	účastník	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
1	#num#	k4
miliarda	miliarda	k4xCgFnSc1
z	z	k7c2
GSM	GSM	kA
<g/>
/	/	kIx~
<g/>
UMTS	UMTS	kA
účastníků	účastník	k1gMnPc2
používá	používat	k5eAaImIp3nS
datové	datový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
3G	3G	k4
technologie	technologie	k1gFnSc2
W-CDMA	W-CDMA	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
CDMA	CDMA	kA
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
důvod	důvod	k1gInSc1
pro	pro	k7c4
růst	růst	k1gInSc4
používání	používání	k1gNnSc2
GSM	GSM	kA
<g/>
,	,	kIx,
hlavně	hlavně	k9
mezi	mezi	k7c7
roky	rok	k1gInPc7
1998	#num#	k4
až	až	k9
2002	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dostupnost	dostupnost	k1gFnSc1
předplaceného	předplacený	k2eAgNnSc2d1
volání	volání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
lidem	člověk	k1gMnPc3
vlastnit	vlastnit	k5eAaImF
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
i	i	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
nechtěli	chtít	k5eNaImAgMnP
smlouvou	smlouva	k1gFnSc7
zavazovat	zavazovat	k5eAaImF
operátorovi	operátorův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Radiové	radiový	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
</s>
<s>
Frekvence	frekvence	k1gFnSc1
používané	používaný	k2eAgNnSc1d1
v	v	k7c6
ČR	ČR	kA
(	(	kIx(
<g/>
MHz	Mhz	kA
<g/>
)	)	kIx)
</s>
<s>
Operátor	operátor	k1gMnSc1
</s>
<s>
2G	2G	k4
</s>
<s>
3G	3G	k4
</s>
<s>
4	#num#	k4
<g/>
G	G	kA
<g/>
/	/	kIx~
<g/>
LTE	LTE	kA
</s>
<s>
O2-CZ	O2-CZ	k4
</s>
<s>
900	#num#	k4
<g/>
/	/	kIx~
<g/>
1800	#num#	k4
</s>
<s>
2100	#num#	k4
</s>
<s>
800	#num#	k4
<g/>
/	/	kIx~
<g/>
1800	#num#	k4
</s>
<s>
T-Mobile	T-Mobile	k6eAd1
CZ	CZ	kA
</s>
<s>
900	#num#	k4
<g/>
/	/	kIx~
<g/>
1800	#num#	k4
</s>
<s>
2100	#num#	k4
</s>
<s>
800	#num#	k4
<g/>
/	/	kIx~
<g/>
1800	#num#	k4
<g/>
/	/	kIx~
<g/>
2100	#num#	k4
</s>
<s>
Vodafone	Vodafon	k1gInSc5
CZ	CZ	kA
</s>
<s>
900	#num#	k4
<g/>
/	/	kIx~
<g/>
1800	#num#	k4
</s>
<s>
2100	#num#	k4
</s>
<s>
800	#num#	k4
<g/>
/	/	kIx~
<g/>
900	#num#	k4
<g/>
/	/	kIx~
<g/>
1800	#num#	k4
<g/>
/	/	kIx~
<g/>
2100	#num#	k4
</s>
<s>
GSM	GSM	kA
je	být	k5eAaImIp3nS
buňková	buňkový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
se	se	k3xPyFc4
připojují	připojovat	k5eAaImIp3nP
do	do	k7c2
sítě	síť	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
nejbližší	blízký	k2eAgFnSc2d3
buňky	buňka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GSM	GSM	kA
síť	síť	k1gFnSc4
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
několika	několik	k4yIc6
radiových	radiový	k2eAgFnPc6d1
frekvencích	frekvence	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
pro	pro	k7c4
2G	2G	k4
sítě	síť	k1gFnSc2
frekvence	frekvence	k1gFnSc1
900	#num#	k4
a	a	k8xC
1800	#num#	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
3G	3G	k4
sítě	síť	k1gFnPc4
na	na	k7c4
2100	#num#	k4
MHz	Mhz	kA
a	a	k8xC
LTE	LTE	kA
sítě	síť	k1gFnSc2
na	na	k7c4
800	#num#	k4
<g/>
,	,	kIx,
900	#num#	k4
<g/>
,	,	kIx,
1800	#num#	k4
a	a	k8xC
2100	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
tabulka	tabulka	k1gFnSc1
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc1
různé	různý	k2eAgFnPc1d1
velikosti	velikost	k1gFnPc1
buněk	buňka	k1gFnPc2
–	–	k?
Makro	makro	k1gNnSc1
<g/>
,	,	kIx,
mikro	mikro	k1gNnSc1
<g/>
,	,	kIx,
piko	piko	k1gNnSc1
a	a	k8xC
deštníkové	deštníkový	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
pokrytí	pokrytí	k1gNnSc2
každé	každý	k3xTgFnSc2
buňky	buňka	k1gFnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
makro	makro	k1gNnSc4
buňky	buňka	k1gFnSc2
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc4d1
ty	ten	k3xDgInPc4
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
anténa	anténa	k1gFnSc1
základové	základový	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
na	na	k7c6
stožáru	stožár	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
budově	budova	k1gFnSc6
nad	nad	k7c7
úrovní	úroveň	k1gFnSc7
střech	střecha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikro	Mikro	k1gNnSc4
buňky	buňka	k1gFnSc2
mají	mít	k5eAaImIp3nP
anténu	anténa	k1gFnSc4
umístěnou	umístěný	k2eAgFnSc4d1
pod	pod	k7c7
úrovní	úroveň	k1gFnSc7
střech	střecha	k1gFnPc2
<g/>
;	;	kIx,
typické	typický	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
v	v	k7c6
zastavěných	zastavěný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pikobuňky	Pikobuňk	k1gInPc7
jsou	být	k5eAaImIp3nP
malé	malý	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
s	s	k7c7
průměrem	průměr	k1gInSc7
pár	pár	k4xCyI
desítek	desítka	k1gFnPc2
metrů	metr	k1gInPc2
<g/>
;	;	kIx,
používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
hlavně	hlavně	k9
uvnitř	uvnitř	k7c2
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
deštníkové	deštníkový	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
pro	pro	k7c4
pokrytí	pokrytí	k1gNnSc4
oblastí	oblast	k1gFnPc2
ve	v	k7c6
stínech	stín	k1gInPc6
a	a	k8xC
na	na	k7c4
vyplnění	vyplnění	k1gNnSc4
mezer	mezera	k1gFnPc2
mezi	mezi	k7c7
buňkami	buňka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Velikost	velikost	k1gFnSc1
pokrytí	pokrytí	k1gNnSc2
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
výšce	výška	k1gFnSc6
antény	anténa	k1gFnSc2
<g/>
,	,	kIx,
výkonu	výkon	k1gInSc3
antény	anténa	k1gFnSc2
a	a	k8xC
na	na	k7c6
podmínkách	podmínka	k1gFnPc6
šíření	šíření	k1gNnSc2
a	a	k8xC
pohybuje	pohybovat	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
několika	několik	k4yIc2
stovek	stovka	k1gFnPc2
metrů	metr	k1gInPc2
až	až	k6eAd1
do	do	k7c2
desítky	desítka	k1gFnSc2
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
vzdálenost	vzdálenost	k1gFnSc1
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
se	se	k3xPyFc4
podle	podle	k7c2
specifikace	specifikace	k1gFnSc2
GSM	GSM	kA
prakticky	prakticky	k6eAd1
používá	používat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
35	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
však	však	k9
koncept	koncept	k1gInSc1
rozšířené	rozšířený	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
kde	kde	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
oblast	oblast	k1gFnSc1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
dvojnásobná	dvojnásobný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Pokrytí	pokrytí	k1gNnSc1
uvnitř	uvnitř	k7c2
budov	budova	k1gFnPc2
podporuje	podporovat	k5eAaImIp3nS
GSM	GSM	kA
také	také	k6eAd1
a	a	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
děličem	dělič	k1gInSc7
výkonu	výkon	k1gInSc6
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
přenáší	přenášet	k5eAaImIp3nS
radiový	radiový	k2eAgInSc4d1
signál	signál	k1gInSc4
z	z	k7c2
vnějšku	vnějšek	k1gInSc2
do	do	k7c2
odděleného	oddělený	k2eAgInSc2d1
systému	systém	k1gInSc2
antén	anténa	k1gFnPc2
uvnitř	uvnitř	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
hlavně	hlavně	k9
když	když	k8xS
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
velká	velký	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
hovorů	hovor	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
obchodních	obchodní	k2eAgInPc6d1
centrech	centrum	k1gNnPc6
nebo	nebo	k8xC
na	na	k7c6
letištích	letiště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
nezbytně	nezbytně	k6eAd1,k6eNd1
nutné	nutný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
radiový	radiový	k2eAgInSc1d1
signál	signál	k1gInSc1
z	z	k7c2
venku	venek	k1gInSc2
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
i	i	k9
skrz	skrz	k7c4
zdi	zeď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
sítě	síť	k1gFnSc2
</s>
<s>
Struktura	struktura	k1gFnSc1
GSM	GSM	kA
sítě	síť	k1gFnSc2
</s>
<s>
Síť	síť	k1gFnSc1
za	za	k7c7
systémem	systém	k1gInSc7
GSM	GSM	kA
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
a	a	k8xC
složitá	složitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
poskytovat	poskytovat	k5eAaImF
veškeré	veškerý	k3xTgFnPc4
vyžadované	vyžadovaný	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
několika	několik	k4yIc2
sekcí	sekce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
–	–	k?
na	na	k7c6
obrázku	obrázek	k1gInSc6
vpravo	vpravo	k6eAd1
část	část	k1gFnSc1
označená	označený	k2eAgFnSc1d1
MS	MS	kA
<g/>
:	:	kIx,
Mobile	mobile	k1gNnSc4
Station	station	k1gInSc4
-	-	kIx~
koncová	koncový	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
pro	pro	k7c4
většinu	většina	k1gFnSc4
služeb	služba	k1gFnPc2
sítě	síť	k1gFnSc2
GSM	GSM	kA
</s>
<s>
Systém	systém	k1gInSc1
základnových	základnový	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
–	–	k?
na	na	k7c6
obrázku	obrázek	k1gInSc6
vpravo	vpravo	k6eAd1
část	část	k1gFnSc1
označená	označený	k2eAgFnSc1d1
AN	AN	kA
<g/>
:	:	kIx,
Access	Access	k1gInSc1
Network	network	k1gInSc1
–	–	k?
umožňuje	umožňovat	k5eAaImIp3nS
přístup	přístup	k1gInSc4
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
do	do	k7c2
sítě	síť	k1gFnSc2
</s>
<s>
Síťový	síťový	k2eAgInSc1d1
spojovací	spojovací	k2eAgInSc1d1
subsystém	subsystém	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
Síťový	síťový	k2eAgInSc1d1
a	a	k8xC
přepínací	přepínací	k2eAgInSc1d1
podsystém	podsystém	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
na	na	k7c6
obrázku	obrázek	k1gInSc6
vpravo	vpravo	k6eAd1
označený	označený	k2eAgInSc4d1
CS	CS	kA
<g/>
:	:	kIx,
Circuit	Circuit	k1gMnSc1
Switched	Switched	k1gMnSc1
–	–	k?
část	část	k1gFnSc1
sítě	síť	k1gFnSc2
zajišťující	zajišťující	k2eAgNnSc4d1
spojování	spojování	k1gNnSc4
telefonních	telefonní	k2eAgInPc2d1
hovorů	hovor	k1gInPc2
nejvíce	nejvíce	k6eAd1,k6eAd3
podobná	podobný	k2eAgFnSc1d1
pevné	pevný	k2eAgFnSc3d1
síti	síť	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
říká	říkat	k5eAaImIp3nS
prostě	prostě	k6eAd1
hlavní	hlavní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
GPRS	GPRS	kA
–	–	k?
na	na	k7c6
obrázku	obrázek	k1gInSc6
vpravo	vpravo	k6eAd1
označená	označený	k2eAgFnSc1d1
GPRS	GPRS	kA
PS	PS	kA
<g/>
:	:	kIx,
Packet	Packet	k1gMnSc1
Switched	Switched	k1gMnSc1
–	–	k?
volitelná	volitelný	k2eAgNnPc4d1
(	(	kIx(
<g/>
ale	ale	k8xC
prakticky	prakticky	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
sítích	síť	k1gFnPc6
přítomná	přítomný	k2eAgFnSc1d1
<g/>
)	)	kIx)
část	část	k1gFnSc1
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
internetové	internetový	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
na	na	k7c6
bázi	báze	k1gFnSc6
paketů	paket	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Společné	společný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
–	–	k?
na	na	k7c6
obrázku	obrázek	k1gInSc6
vpravo	vpravo	k6eAd1
označené	označený	k2eAgFnSc6d1
CS	CS	kA
&	&	k?
PS	PS	kA
–	–	k?
především	především	k9
databáze	databáze	k1gFnSc2
domovský	domovský	k2eAgInSc1d1
registr	registr	k1gInSc1
(	(	kIx(
<g/>
HLR	HLR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
návštěvnický	návštěvnický	k2eAgInSc1d1
registr	registr	k1gInSc1
VLR	VLR	kA
<g/>
,	,	kIx,
Autentizační	autentizační	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
(	(	kIx(
<g/>
AuC	AuC	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Equipment	Equipment	k1gMnSc1
Identity	identita	k1gFnSc2
Register	registrum	k1gNnPc2
(	(	kIx(
<g/>
EIR	EIR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Středisko	středisko	k1gNnSc1
krátkých	krátká	k1gFnPc2
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
(	(	kIx(
<g/>
SMS-GMSC	SMS-GMSC	k1gFnSc1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
vstupní	vstupní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Všechny	všechen	k3xTgInPc1
elementy	element	k1gInPc1
spolupracují	spolupracovat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohly	moct	k5eAaImAgFnP
poskytovat	poskytovat	k5eAaImF
GSM	GSM	kA
služby	služba	k1gFnPc4
jako	jako	k8xC,k8xS
hovory	hovor	k1gInPc4
<g/>
,	,	kIx,
SMS	SMS	kA
a	a	k8xC
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Subscriber	Subscriber	k1gInSc1
Identity	identita	k1gFnSc2
Module	modul	k1gInSc5
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
klíčových	klíčový	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
GSM	GSM	kA
je	být	k5eAaImIp3nS
Subscriber	Subscriber	k1gInSc1
Identity	identita	k1gFnSc2
Module	modul	k1gInSc5
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
SIM	SIM	kA
karta	karta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SIM	sima	k1gFnPc2
karta	karta	k1gFnSc1
je	být	k5eAaImIp3nS
vyjímatelná	vyjímatelný	k2eAgFnSc1d1
čipová	čipový	k2eAgFnSc1d1
karta	karta	k1gFnSc1
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgFnPc1d1
informace	informace	k1gFnPc1
potřebné	potřebný	k2eAgFnPc1d1
k	k	k7c3
přihlášení	přihlášení	k1gNnSc3
uživatele	uživatel	k1gMnSc2
do	do	k7c2
sítě	síť	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
na	na	k7c6
ní	on	k3xPp3gFnSc6
uložen	uložit	k5eAaPmNgInS
telefonní	telefonní	k2eAgInSc1d1
seznam	seznam	k1gInSc1
a	a	k8xC
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatel	uživatel	k1gMnSc1
může	moct	k5eAaImIp3nS
kartu	karta	k1gFnSc4
vytáhnout	vytáhnout	k5eAaPmF
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
mobilního	mobilní	k2eAgInSc2d1
telefonu	telefon	k1gInSc2
a	a	k8xC
jednoduše	jednoduše	k6eAd1
ji	on	k3xPp3gFnSc4
použít	použít	k5eAaPmF
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
telefonu	telefon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
naopak	naopak	k6eAd1
může	moct	k5eAaImIp3nS
v	v	k7c6
jednom	jeden	k4xCgInSc6
mobilním	mobilní	k2eAgInSc6d1
telefonu	telefon	k1gInSc6
střídat	střídat	k5eAaImF
více	hodně	k6eAd2
operátorů	operátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
operátoři	operátor	k1gMnPc1
však	však	k9
prodávají	prodávat	k5eAaImIp3nP
tzv.	tzv.	kA
SIM	SIM	kA
lock	lock	k1gMnSc1
telefony	telefon	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
dovolují	dovolovat	k5eAaImIp3nP
používat	používat	k5eAaImF
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
skupinu	skupina	k1gFnSc4
SIM	sima	k1gFnPc2
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
blokování	blokování	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
zakázáno	zakázán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
USA	USA	kA
a	a	k8xC
Evropě	Evropa	k1gFnSc6
většina	většina	k1gFnSc1
operátorů	operátor	k1gMnPc2
prodává	prodávat	k5eAaImIp3nS
zamčené	zamčený	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělají	dělat	k5eAaImIp3nP
to	ten	k3xDgNnSc4
z	z	k7c2
důvodu	důvod	k1gInSc2
nižší	nízký	k2eAgFnSc2d2
ceny	cena	k1gFnSc2
telefonu	telefon	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
si	se	k3xPyFc3
zákazník	zákazník	k1gMnSc1
může	moct	k5eAaImIp3nS
koupit	koupit	k5eAaPmF
při	při	k7c6
podepsání	podepsání	k1gNnSc6
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatel	uživatel	k1gMnSc1
může	moct	k5eAaImIp3nS
kontaktovat	kontaktovat	k5eAaImF
svého	svůj	k3xOyFgMnSc4
operátora	operátor	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
jeho	jeho	k3xOp3gNnSc4
mobilní	mobilní	k2eAgInSc1d1
telefon	telefon	k1gInSc1
odblokoval	odblokovat	k5eAaPmAgInS
<g/>
,	,	kIx,
jenže	jenže	k8xC
dost	dost	k6eAd1
operátorů	operátor	k1gInPc2
toto	tento	k3xDgNnSc1
ignoruje	ignorovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Někteří	některý	k3yIgMnPc1
operátoři	operátor	k1gMnPc1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
T-Mobile	T-Mobila	k1gFnSc3
a	a	k8xC
Cingular	Cingular	k1gInSc4
umožňují	umožňovat	k5eAaImIp3nP
nechat	nechat	k5eAaPmF
si	se	k3xPyFc3
odemknout	odemknout	k5eAaPmF
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
zadarmo	zadarmo	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
jste	být	k5eAaImIp2nP
již	již	k6eAd1
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
jejich	jejich	k3xOp3gMnPc7
zákazníkem	zákazník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
také	také	k9
možnost	možnost	k1gFnSc4
odemknout	odemknout	k5eAaPmF
si	se	k3xPyFc3
mobilní	mobilní	k2eAgInSc1d1
telefon	telefon	k1gInSc1
sám	sám	k3xTgInSc1
<g/>
,	,	kIx,
softwarem	software	k1gInSc7
staženým	stažený	k2eAgInSc7d1
z	z	k7c2
Internetu	Internet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
nechat	nechat	k5eAaPmF
si	se	k3xPyFc3
odemknout	odemknout	k5eAaPmF
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
třetí	třetí	k4xOgFnSc7
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
a	a	k8xC
levnější	levný	k2eAgMnSc1d2
než	než	k8xS
u	u	k7c2
operátora	operátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
zemí	zem	k1gFnPc2
je	být	k5eAaImIp3nS
odemknutí	odemknutí	k1gNnSc1
telefonu	telefon	k1gInSc2
legální	legální	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mobilní	mobilní	k2eAgInSc4d1
operátor	operátor	k1gInSc4
T-Mobile	T-Mobila	k1gFnSc3
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
již	již	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
všechna	všechen	k3xTgNnPc4
svá	svůj	k3xOyFgNnPc4
mobilní	mobilní	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
(	(	kIx(
<g/>
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
,	,	kIx,
modemy	modem	k1gInPc4
a	a	k8xC
PC	PC	kA
karty	karta	k1gFnSc2
<g/>
)	)	kIx)
bez	bez	k7c2
SIM	sima	k1gFnPc2
lock	lock	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodafone	Vodafon	k1gInSc5
Czech	Czecha	k1gFnPc2
Republic	Republice	k1gFnPc2
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
nástupce	nástupce	k1gMnSc4
Oskara	Oskar	k1gMnSc4
<g/>
)	)	kIx)
telefony	telefon	k1gInPc4
neblokoval	blokovat	k5eNaImAgInS
již	již	k6eAd1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vstupu	vstup	k1gInSc2
na	na	k7c4
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
GSM	GSM	kA
</s>
<s>
Síť	síť	k1gFnSc1
GSM	GSM	kA
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
úrovní	úroveň	k1gFnSc7
zabezpečení	zabezpečení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ověřoval	ověřovat	k5eAaImAgMnS
uživatele	uživatel	k1gMnSc4
použitím	použití	k1gNnSc7
sdíleným-tajným	sdíleným-tajný	k2eAgNnSc7d1
šifrováním	šifrování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunikace	komunikace	k1gFnSc1
mezi	mezi	k7c7
uživatelem	uživatel	k1gMnSc7
a	a	k8xC
základovou	základový	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
šifrována	šifrován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
UMTS	UMTS	kA
představil	představit	k5eAaPmAgInS
možnost	možnost	k1gFnSc4
USIM	USIM	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
používá	používat	k5eAaImIp3nS
delší	dlouhý	k2eAgInSc4d2
autorizační	autorizační	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc4d2
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
oboustrannou	oboustranný	k2eAgFnSc4d1
autorizaci	autorizace	k1gFnSc4
mezi	mezi	k7c7
uživatelem	uživatel	k1gMnSc7
a	a	k8xC
sítí	síť	k1gFnSc7
–	–	k?
zatímco	zatímco	k8xS
GSM	GSM	kA
autorizuje	autorizovat	k5eAaBmIp3nS
jen	jen	k9
uživatele	uživatel	k1gMnPc4
do	do	k7c2
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
ne	ne	k9
obráceně	obráceně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezpečnostní	bezpečnostní	k2eAgInSc4d1
model	model	k1gInSc4
proto	proto	k8xC
nabízí	nabízet	k5eAaImIp3nS
důvěrnost	důvěrnost	k1gFnSc4
a	a	k8xC
autentičnost	autentičnost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
omezené	omezený	k2eAgFnPc4d1
autorizační	autorizační	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
GSM	GSM	kA
pro	pro	k7c4
zabezpečení	zabezpečení	k1gNnSc4
používá	používat	k5eAaImIp3nS
některé	některý	k3yIgInPc1
šifrovací	šifrovací	k2eAgInPc1d1
algoritmy	algoritmus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šifry	šifra	k1gFnSc2
A	A	kA
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
a	a	k8xC
A	A	kA
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
pro	pro	k7c4
zajištění	zajištění	k1gNnSc4
bezpečnosti	bezpečnost	k1gFnSc2
hovoru	hovor	k1gInSc2
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
je	být	k5eAaImIp3nS
silnější	silný	k2eAgInSc1d2
algoritmus	algoritmus	k1gInSc1
používaný	používaný	k2eAgInSc1d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
;	;	kIx,
A	a	k9
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
slabší	slabý	k2eAgFnSc1d2
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vážné	vážný	k2eAgFnPc1d1
slabiny	slabina	k1gFnPc1
byly	být	k5eAaImAgFnP
nalezeny	nalézt	k5eAaBmNgFnP,k5eAaPmNgFnP
v	v	k7c6
obou	dva	k4xCgInPc6
algoritmech	algoritmus	k1gInPc6
a	a	k8xC
A	a	k9
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
prolomit	prolomit	k5eAaPmF
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
více	hodně	k6eAd2
algoritmů	algoritmus	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
operátoři	operátor	k1gMnPc1
mohou	moct	k5eAaImIp3nP
nahradit	nahradit	k5eAaPmF
tuto	tento	k3xDgFnSc4
šifru	šifra	k1gFnSc4
silnější	silný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Anton	Anton	k1gMnSc1
A.	A.	kA
Huurdeman	Huurdeman	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc5
Worldwide	Worldwid	k1gMnSc5
History	Histor	k1gInPc1
of	of	k?
Telecommunications	Telecommunications	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
<g/>
,	,	kIx,
31	#num#	k4
July	Jula	k1gFnSc2
2003	#num#	k4
<g/>
,	,	kIx,
page	page	k1gFnSc1
529	#num#	k4
<g/>
↑	↑	k?
GSM	GSM	kA
Global	globat	k5eAaImAgInS
system	systo	k1gNnSc7
for	forum	k1gNnPc2
Mobile	mobile	k1gNnPc6
Communications	Communications	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4G	4G	k4
Americas	Americas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
8	#num#	k4
February	Februara	k1gFnSc2
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.gsacom.com	www.gsacom.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.itu.int/ITU-D/ict/facts/2011/material/ICTFactsFigures2011.pdf	http://www.itu.int/ITU-D/ict/facts/2011/material/ICTFactsFigures2011.pdf	k1gMnSc1
<g/>
↑	↑	k?
http://www.cdg.org/worldwide/cdma_world_subscriber.asp	http://www.cdg.org/worldwide/cdma_world_subscriber.asp	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Siegmund	Siegmund	k1gMnSc1
M.	M.	kA
Redl	Redl	k1gMnSc1
<g/>
,	,	kIx,
Matthias	Matthias	k1gMnSc1
K.	K.	kA
Weber	Weber	k1gMnSc1
<g/>
,	,	kIx,
Malcolm	Malcolm	k1gMnSc1
W.	W.	kA
Oliphant	Oliphant	k1gMnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc4
GSM	GSM	kA
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Artech	Artech	k1gInSc4
House	house	k1gNnSc4
<g/>
,	,	kIx,
March	March	k1gInSc1
1995	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-89006-785-7	978-0-89006-785-7	k4
</s>
<s>
Siegmund	Siegmund	k1gMnSc1
M.	M.	kA
Redl	Redl	k1gMnSc1
<g/>
,	,	kIx,
Matthias	Matthias	k1gMnSc1
K.	K.	kA
Weber	Weber	k1gMnSc1
<g/>
,	,	kIx,
Malcolm	Malcolm	k1gMnSc1
W.	W.	kA
Oliphant	Oliphant	k1gMnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
GSM	GSM	kA
and	and	k?
Personal	Personal	k1gFnSc2
Communications	Communicationsa	k1gFnPc2
Handbook	handbook	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Artech	Artech	k1gInSc4
House	house	k1gNnSc4
<g/>
,	,	kIx,
May	May	k1gMnSc1
1998	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-89006-957-8	978-0-89006-957-8	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Universal	Universat	k5eAaImAgMnS,k5eAaPmAgMnS
Mobile	mobile	k1gNnSc4
Telecommunications	Telecommunications	k1gInSc1
System	Syst	k1gMnSc7
(	(	kIx(
<g/>
UMTS	UMTS	kA
<g/>
)	)	kIx)
</s>
<s>
General	Generat	k5eAaPmAgInS,k5eAaImAgInS
Packet	Packet	k1gInSc1
Radio	radio	k1gNnSc1
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
GPRS	GPRS	kA
<g/>
)	)	kIx)
</s>
<s>
Enhanced	Enhanced	k1gMnSc1
Data	datum	k1gNnSc2
Rates	Rates	k1gInSc1
for	forum	k1gNnPc2
GSM	GSM	kA
Evolution	Evolution	k1gInSc1
(	(	kIx(
<g/>
EDGE	EDGE	kA
<g/>
)	)	kIx)
</s>
<s>
EGPRS	EGPRS	kA
</s>
<s>
SIM	SIM	kA
karta	karta	k1gFnSc1
</s>
<s>
International	Internationat	k5eAaImAgInS,k5eAaPmAgInS
Mobile	mobile	k1gNnSc4
Subscriber	Subscribero	k1gNnPc2
Identity	identita	k1gFnSc2
(	(	kIx(
<g/>
IMSI	IMSI	kA
<g/>
)	)	kIx)
</s>
<s>
IMEI	IMEI	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Global	globat	k5eAaImAgInS
System	Systo	k1gNnSc7
for	forum	k1gNnPc2
Mobile	mobile	k1gNnSc2
Communications	Communications	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Richtr	Richtr	k1gMnSc1
<g/>
:	:	kIx,
GSM	GSM	kA
</s>
<s>
GSMweb	GSMwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1
<g/>
:	:	kIx,
</s>
<s>
3GPP	3GPP	k4
The	The	k1gMnSc1
current	current	k1gMnSc1
standardisation	standardisation	k1gInSc4
body	bod	k1gInPc4
for	forum	k1gNnPc2
GSM	GSM	kA
with	witha	k1gFnPc2
free	freat	k5eAaPmIp3nS
standards	standards	k6eAd1
available	available	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
OMA	OMA	kA
The	The	k1gMnSc1
current	current	k1gMnSc1
standardisation	standardisation	k1gInSc4
body	bod	k1gInPc4
for	forum	k1gNnPc2
services	servicesa	k1gFnPc2
aspects	aspectsa	k1gFnPc2
of	of	k?
mobile	mobile	k1gNnSc4
networks	tworks	k6eNd1
-	-	kIx~
some	some	k6eAd1
free	freat	k5eAaPmIp3nS
standards	standards	k6eAd1
available	available	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
ETSI	ETSI	kA
the	the	k?
original	originat	k5eAaImAgMnS,k5eAaPmAgMnS
GSM	GSM	kA
standardisation	standardisation	k1gInSc4
body	bod	k1gInPc4
</s>
<s>
GSM	GSM	kA
Association	Association	k1gInSc1
-	-	kIx~
the	the	k?
group	group	k1gInSc1
representing	representing	k1gInSc1
GSM	GSM	kA
operators	operators	k1gInSc1
(	(	kIx(
<g/>
official	official	k1gInSc1
site	sit	k1gInSc2
<g/>
)	)	kIx)
-	-	kIx~
includes	includes	k1gInSc1
coverage	coverage	k1gNnSc1
maps	mapsa	k1gFnPc2
for	forum	k1gNnPc2
all	all	k?
members	members	k6eAd1
</s>
<s>
Number	Number	k1gInSc1
of	of	k?
GSM	GSM	kA
Subscribers	Subscribers	k1gInSc1
</s>
<s>
List	list	k1gInSc1
of	of	k?
acronyms	acronyms	k1gInSc1
of	of	k?
GSM	GSM	kA
network	network	k1gInSc1
parameters	parameters	k1gInSc1
</s>
<s>
Overview	Overview	k?
of	of	k?
GSM	GSM	kA
by	by	kYmCp3nS
John	John	k1gMnSc1
Scourias	Scourias	k1gMnSc1
</s>
<s>
How	How	k?
GSM	GSM	kA
Works	Works	kA
</s>
<s>
Visualtron	Visualtron	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
tutorial	tutorial	k1gInSc1
on	on	k3xPp3gMnSc1
GSM	GSM	kA
</s>
<s>
GSM	GSM	kA
-	-	kIx~
ideas	ideas	k1gMnSc1
<g/>
,	,	kIx,
origin	origin	k1gMnSc1
and	and	k?
milestones	milestones	k1gMnSc1
-	-	kIx~
a	a	k8xC
Norwegian	Norwegian	k1gMnSc1
perspective	perspectiv	k1gInSc5
from	from	k1gFnPc3
Telenor	Telenor	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
journal	journat	k5eAaPmAgInS,k5eAaImAgInS
of	of	k?
technology	technolog	k1gMnPc7
Telektronikk	Telektronikk	k1gInSc1
</s>
<s>
GSM	GSM	kA
Service	Service	k1gFnSc1
and	and	k?
TriBand	TriBand	k1gInSc4
International	International	k1gMnPc2
Roaming	Roaming	k1gInSc1
FAQs	FAQsa	k1gFnPc2
</s>
<s>
Cell	cello	k1gNnPc2
Phone	Phon	k1gInSc5
Safety	Safet	k1gInPc1
and	and	k?
Wireless	Wireless	k1gInSc1
Facts	Facts	k1gInSc1
</s>
<s>
GSM	GSM	kA
Phone	Phon	k1gInSc5
World	World	k1gInSc4
Network	network	k1gInSc1
List	list	k1gInSc1
</s>
<s>
GSM	GSM	kA
Call	Call	k1gMnSc1
Flow	Flow	k1gMnSc1
Diagrams	Diagrams	k1gInSc4
</s>
<s>
Article	Article	k1gFnSc1
for	forum	k1gNnPc2
Americans	Americans	k1gInSc4
on	on	k3xPp3gMnSc1
using	using	k1gMnSc1
GSM	GSM	kA
cellphones	cellphones	k1gMnSc1
in	in	k?
Europe	Europ	k1gInSc5
and	and	k?
ROW	ROW	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Standardy	standard	k1gInPc1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
0G	0G	k4
</s>
<s>
PTT	PTT	kA
</s>
<s>
MTS	MTS	kA
</s>
<s>
IMTS	IMTS	kA
</s>
<s>
AMTS	AMTS	kA
</s>
<s>
AMR	AMR	kA
</s>
<s>
0.5	0.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
Autotel	Autotel	k1gMnSc1
<g/>
/	/	kIx~
<g/>
PALM	PALM	kA
</s>
<s>
ARP	ARP	kA
1G	1G	k4
</s>
<s>
NMT	NMT	kA
</s>
<s>
AMPS	AMPS	kA
2G	2G	k4
</s>
<s>
GSM	GSM	kA
</s>
<s>
cdmaOne	cdmaOnout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
iDEN	iDEN	k?
</s>
<s>
D-AMPS	D-AMPS	k?
<g/>
/	/	kIx~
<g/>
IS-	IS-	k1gFnSc1
<g/>
136	#num#	k4
<g/>
/	/	kIx~
<g/>
TDMA	TDMA	kA
</s>
<s>
PDC	PDC	kA
</s>
<s>
2.5	2.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
GPRS	GPRS	kA
</s>
<s>
2.75	2.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
CDMA2000	CDMA2000	k1gFnSc1
1	#num#	k4
<g/>
xRTT	xRTT	k?
</s>
<s>
EDGE	EDGE	kA
</s>
<s>
EGPRS	EGPRS	kA
3G	3G	k4
</s>
<s>
W-CDMA	W-CDMA	k?
</s>
<s>
UMTS	UMTS	kA
</s>
<s>
FOMA	FOMA	kA
</s>
<s>
CDMA2000	CDMA2000	k4
1	#num#	k4
<g/>
xEV	xEV	k?
</s>
<s>
TD-SCDMA	TD-SCDMA	k?
</s>
<s>
3.5	3.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSDPA	HSDPA	kA
</s>
<s>
3.75	3.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSUPA	HSUPA	kA
Pre-	Pre-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
G	G	kA
</s>
<s>
Mobile	mobile	k1gNnSc1
WiMAX	WiMAX	k1gFnSc2
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
e	e	k0
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
(	(	kIx(
<g/>
E-UTRA	E-UTRA	k1gMnSc1
<g/>
)	)	kIx)
4G	4G	k4
</s>
<s>
WiMAX-Advanced	WiMAX-Advanced	k1gMnSc1
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
Advanced	Advanced	k1gInSc4
5G	5G	k4
</s>
<s>
eMBB	eMBB	k?
</s>
<s>
URLLC	URLLC	kA
</s>
<s>
MMTC	MMTC	kA
</s>
