<s>
GSM	GSM	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
francouzského	francouzský	k2eAgMnSc2d1
„	„	k?
<g/>
Groupe	Groupe	k1gFnSc1
Spécial	Spécial	k2eAgFnSc1d1
Mobile	mobile	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
celosvětově	celosvětově	k6eAd1
nejrozšířenější	rozšířený	k2eAgInSc1d3
standard	standard	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vytvořil	vytvořit	k5eAaPmAgInS
Evropský	evropský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
(	(	kIx(
<g/>
ETSI	ETSI	kA
<g/>
)	)	kIx)
pro	pro	k7c4
digitální	digitální	k2eAgFnPc4d1
mobilní	mobilní	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
označované	označovaný	k2eAgFnPc4d1
jako	jako	k8xS,k8xC
2	#num#	k4
<g/>
G.	G.	kA
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
zprovozněn	zprovoznit	k5eAaPmNgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1991	#num#	k4
ve	v	k7c6
<g />
.	.	kIx.
</s>