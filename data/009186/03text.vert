<p>
<s>
Čedok	Čedok	k1gInSc1	Čedok
a.s.	a.s.	k?	a.s.
je	být	k5eAaImIp3nS	být
cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
polská	polský	k2eAgFnSc1d1	polská
cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
Itaka	Itaka	k1gFnSc1	Itaka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
společnosti	společnost	k1gFnSc6	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
Čedok	Čedok	k1gInSc1	Čedok
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
cestovní	cestovní	k2eAgFnSc7d1	cestovní
kanceláří	kancelář	k1gFnSc7	kancelář
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
historie	historie	k1gFnSc1	historie
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Československá	československý	k2eAgFnSc1d1	Československá
cestovní	cestovní	k2eAgFnSc1d1	cestovní
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc1d1	dopravní
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
ČEDOK	Čedok	k1gInSc1	Čedok
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
akronym	akronym	k1gInSc1	akronym
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
názvem	název	k1gInSc7	název
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
socialismu	socialismus	k1gInSc2	socialismus
byl	být	k5eAaImAgInS	být
Čedok	Čedok	k1gInSc1	Čedok
po	po	k7c4	po
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
roky	rok	k1gInPc4	rok
dominantním	dominantní	k2eAgMnSc7d1	dominantní
hráčem	hráč	k1gMnSc7	hráč
výjezdového	výjezdový	k2eAgInSc2d1	výjezdový
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
společnosti	společnost	k1gFnSc2	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
Založena	založen	k2eAgFnSc1d1	založena
Československá	československý	k2eAgFnSc1d1	Československá
cestovní	cestovní	k2eAgFnSc1d1	cestovní
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc1d1	dopravní
kancelář	kancelář	k1gFnSc1	kancelář
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
Poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
název	název	k1gInSc1	název
ČEDOK	Čedok	k1gInSc1	Čedok
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
Znárodnění	znárodnění	k1gNnSc3	znárodnění
společnosti	společnost	k1gFnSc2	společnost
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Rozdělení	rozdělení	k1gNnSc1	rozdělení
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
Čedok	Čedok	k1gInSc4	Čedok
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
Satur	Satur	k1gMnSc1	Satur
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Nový	nový	k2eAgMnSc1d1	nový
majoritní	majoritní	k2eAgMnSc1d1	majoritní
akcionář	akcionář	k1gMnSc1	akcionář
–	–	k?	–
Unimex	Unimex	k1gInSc1	Unimex
Group	Group	k1gInSc1	Group
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Založení	založení	k1gNnSc1	založení
Prague	Pragu	k1gInSc2	Pragu
Sightseeing	Sightseeing	k1gInSc1	Sightseeing
Tours	Tours	k1gInSc1	Tours
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Za	za	k7c4	za
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
zakoupen	zakoupen	k2eAgMnSc1d1	zakoupen
50	[number]	k4	50
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
cestovní	cestovní	k2eAgFnSc6d1	cestovní
kanceláři	kancelář	k1gFnSc6	kancelář
ESO	eso	k1gNnSc1	eso
travel	travela	k1gFnPc2	travela
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Čedok	Čedok	k1gInSc1	Čedok
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
–	–	k?	–
založení	založení	k1gNnSc2	založení
společnosti	společnost	k1gFnSc2	společnost
Čedok	Čedok	k1gInSc1	Čedok
Slovakia	Slovakius	k1gMnSc4	Slovakius
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Certifikát	certifikát	k1gInSc1	certifikát
systému	systém	k1gInSc2	systém
managementu	management	k1gInSc2	management
jakosti	jakost	k1gFnSc2	jakost
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
9001	[number]	k4	9001
<g/>
:	:	kIx,	:
<g/>
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Nový	nový	k2eAgMnSc1d1	nový
majoritní	majoritní	k2eAgMnSc1d1	majoritní
akcionář	akcionář	k1gMnSc1	akcionář
–	–	k?	–
ODIEN	ODIEN	kA	ODIEN
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Založena	založen	k2eAgFnSc1d1	založena
pobočka	pobočka	k1gFnSc1	pobočka
se	s	k7c7	s
100	[number]	k4	100
<g/>
%	%	kIx~	%
majetkovou	majetkový	k2eAgFnSc7d1	majetková
účastí	účast	k1gFnSc7	účast
Čedoku	Čedok	k1gInSc2	Čedok
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ahoy	Ahoa	k1gFnSc2	Ahoa
Tourizm	Tourizm	k1gInSc1	Tourizm
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Čedok	Čedok	k1gInSc1	Čedok
s	s	k7c7	s
Fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
asociací	asociace	k1gFnSc7	asociace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
založil	založit	k5eAaPmAgInS	založit
společnost	společnost	k1gFnSc4	společnost
Fotbal	fotbal	k1gInSc1	fotbal
Travel	Travel	k1gMnSc1	Travel
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
českých	český	k2eAgMnPc2d1	český
fanoušků	fanoušek	k1gMnPc2	fanoušek
na	na	k7c4	na
zahraniční	zahraniční	k2eAgNnSc4d1	zahraniční
utkání	utkání	k1gNnSc4	utkání
národního	národní	k2eAgInSc2d1	národní
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
týmu	tým	k1gInSc2	tým
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
Čedok	Čedok	k1gInSc1	Čedok
převzala	převzít	k5eAaPmAgFnS	převzít
polská	polský	k2eAgFnSc1d1	polská
cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
Itaka	Itaka	k1gFnSc1	Itaka
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čedok	Čedok	k1gInSc1	Čedok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Čedok	Čedok	k1gInSc1	Čedok
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
