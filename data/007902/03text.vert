<s>
Lenka	Lenka	k1gFnSc1	Lenka
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
slovenského	slovenský	k2eAgMnSc2d1	slovenský
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojí	dvojí	k4xRgMnSc1	dvojí
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
domáckou	domácký	k2eAgFnSc4d1	domácká
variantu	varianta	k1gFnSc4	varianta
jména	jméno	k1gNnSc2	jméno
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
řecký	řecký	k2eAgInSc4d1	řecký
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
světlo	světlo	k1gNnSc1	světlo
nebo	nebo	k8xC	nebo
pochodeň	pochodeň	k1gFnSc1	pochodeň
(	(	kIx(	(
<g/>
srovnejte	srovnat	k5eAaPmRp2nP	srovnat
s	s	k7c7	s
Helios	Heliosa	k1gFnPc2	Heliosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
verzi	verze	k1gFnSc4	verze
jména	jméno	k1gNnSc2	jméno
Magdalena	Magdalena	k1gFnSc1	Magdalena
–	–	k?	–
jména	jméno	k1gNnSc2	jméno
odvozeného	odvozený	k2eAgNnSc2d1	odvozené
od	od	k7c2	od
biblického	biblický	k2eAgNnSc2d1	biblické
místa	místo	k1gNnSc2	místo
Magdala	Magdal	k1gMnSc2	Magdal
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zkrácenina	zkrácenina	k1gFnSc1	zkrácenina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
Apolena	Apolena	k1gFnSc1	Apolena
či	či	k8xC	či
Eleonor	Eleonora	k1gFnPc2	Eleonora
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
podoba	podoba	k1gFnSc1	podoba
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Lena	Lena	k1gFnSc1	Lena
<g/>
,	,	kIx,	,
Léňa	Léňa	k?	Léňa
<g/>
,	,	kIx,	,
Lenča	Lenča	k1gFnSc1	Lenča
<g/>
,	,	kIx,	,
Lenička	Lenička	k1gFnSc1	Lenička
apod.	apod.	kA	apod.
Miloslava	Miloslava	k1gFnSc1	Miloslava
Knappová	Knappová	k1gFnSc1	Knappová
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
domácké	domácký	k2eAgFnPc4d1	domácká
podoby	podoba	k1gFnPc4	podoba
jména	jméno	k1gNnSc2	jméno
Léňa	Léňa	k?	Léňa
<g/>
,	,	kIx,	,
Lenča	Lenča	k1gFnSc1	Lenča
<g/>
,	,	kIx,	,
Lenči	Lenča	k1gFnPc1	Lenča
<g/>
,	,	kIx,	,
Leni	Leni	k?	Leni
<g/>
,	,	kIx,	,
Lenny	Lenny	k?	Lenny
<g/>
,	,	kIx,	,
Eleonora	Eleonora	k1gFnSc1	Eleonora
<g/>
,	,	kIx,	,
Lenička	Lenička	k1gFnSc1	Lenička
<g/>
,	,	kIx,	,
Lenuš	Lenuš	k1gMnSc1	Lenuš
<g/>
,	,	kIx,	,
Lenuška	Lenuška	k1gMnSc1	Lenuška
<g/>
,	,	kIx,	,
Lenďa	Lenďa	k1gMnSc1	Lenďa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Láďa	Láďa	k1gMnSc1	Láďa
<g/>
,	,	kIx,	,
Leník	leník	k1gMnSc1	leník
<g/>
,	,	kIx,	,
Lendula	Lendula	k1gFnSc1	Lendula
<g/>
,	,	kIx,	,
Lendulka	Lendulka	k1gFnSc1	Lendulka
<g/>
,	,	kIx,	,
Lenulka	Lenulka	k1gFnSc1	Lenulka
<g/>
,	,	kIx,	,
Lendule	Lendule	k1gFnSc5	Lendule
<g/>
,	,	kIx,	,
Levandule	levandule	k1gFnSc5	levandule
<g/>
,	,	kIx,	,
Leňule	Leňule	k1gFnSc5	Leňule
<g/>
,	,	kIx,	,
Lenice	Lenice	k1gFnSc5	Lenice
<g/>
,	,	kIx,	,
Lenuša	Lenuša	k1gMnSc1	Lenuša
<g/>
,	,	kIx,	,
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
Leninka	Leninka	k1gFnSc1	Leninka
<g/>
,	,	kIx,	,
Lenora	Lenora	k1gFnSc1	Lenora
<g/>
,	,	kIx,	,
Leňoušek	Leňoušek	k1gMnSc1	Leňoušek
<g/>
,	,	kIx,	,
Lenušák	Lenušák	k1gMnSc1	Lenušák
<g/>
,	,	kIx,	,
Leňour	Leňour	k1gMnSc1	Leňour
<g/>
,	,	kIx,	,
Leňourek	Leňourek	k1gMnSc1	Leňourek
<g/>
,	,	kIx,	,
Lenčička	Lenčička	k1gFnSc1	Lenčička
<g/>
,	,	kIx,	,
Lenčinka	Lenčinka	k1gFnSc1	Lenčinka
<g/>
,	,	kIx,	,
Lendeš	Lendeš	k1gFnSc1	Lendeš
či	či	k8xC	či
Leňásek	Leňásek	k1gInSc1	Leňásek
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+1,5	+1,5	k4	+1,5
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Bradáčová	Bradáčová	k1gFnSc1	Bradáčová
–	–	k?	–
vrchní	vrchní	k2eAgFnSc1d1	vrchní
státní	státní	k2eAgFnSc1d1	státní
zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
Lenka	Lenka	k1gFnSc1	Lenka
Dusilová	Dusilová	k1gFnSc1	Dusilová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Lenka	Lenka	k1gFnSc1	Lenka
Filipová	Filipová	k1gFnSc1	Filipová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Lenka	Lenka	k1gFnSc1	Lenka
Filipová	Filipová	k1gFnSc1	Filipová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
vystupující	vystupující	k2eAgFnSc1d1	vystupující
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Lenny	Lenny	k?	Lenny
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Lenky	Lenka	k1gFnSc2	Lenka
Filipové	Filipové	k2eAgFnSc1d1	Filipové
Lenka	Lenka	k1gFnSc1	Lenka
–	–	k?	–
australská	australský	k2eAgFnSc1d1	australská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Lenka	lenka	k1gFnSc2	lenka
Kripac	Kripac	k1gFnSc1	Kripac
Lenka	Lenka	k1gFnSc1	Lenka
Hanáková	Hanáková	k1gFnSc1	Hanáková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
topmodelka	topmodelka	k1gFnSc1	topmodelka
Lenka	Lenka	k1gFnSc1	Lenka
Hašková	Hašková	k1gFnSc1	Hašková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
autorka	autorka	k1gFnSc1	autorka
televizních	televizní	k2eAgFnPc2d1	televizní
her	hra	k1gFnPc2	hra
Lenka	Lenka	k1gFnSc1	Lenka
Klicperová	Klicperová	k1gFnSc1	Klicperová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
cestovatelka	cestovatelka	k1gFnSc1	cestovatelka
<g/>
,	,	kIx,	,
dokumentaristka	dokumentaristka	k1gFnSc1	dokumentaristka
<g/>
,	,	kIx,	,
šéfredaktorka	šéfredaktorka	k1gFnSc1	šéfredaktorka
časopisu	časopis	k1gInSc6	časopis
Lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
Země	země	k1gFnSc1	země
Lenka	Lenka	k1gFnSc1	Lenka
Kotková	Kotková	k1gFnSc1	Kotková
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Šarounová	Šarounový	k2eAgFnSc1d1	Šarounová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
astronomka	astronomka	k1gFnSc1	astronomka
Lenka	Lenka	k1gFnSc1	Lenka
Kořínková	Kořínková	k1gFnSc1	Kořínková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Lenka	Lenka	k1gFnSc1	Lenka
Krobotová	Krobotový	k2eAgFnSc1d1	Krobotová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Lenka	Lenka	k1gFnSc1	Lenka
Lanczová	Lanczová	k1gFnSc1	Lanczová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Lenka	Lenka	k1gFnSc1	Lenka
Ouhrabková	Ouhrabkový	k2eAgFnSc1d1	Ouhrabková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Lenka	Lenka	k1gFnSc1	Lenka
Procházková	procházkový	k2eAgFnSc1d1	Procházková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
signatářka	signatářka	k1gFnSc1	signatářka
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
Lenka	Lenka	k1gFnSc1	Lenka
Reinerová	Reinerová	k1gFnSc1	Reinerová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
píšící	píšící	k2eAgFnSc1d1	píšící
německy	německy	k6eAd1	německy
Lenka	Lenka	k1gFnSc1	Lenka
Šmídová	Šmídová	k1gFnSc1	Šmídová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
<g/>
,	,	kIx,	,
jachtařka	jachtařka	k1gFnSc1	jachtařka
Lenka	Lenka	k1gFnSc1	Lenka
Termerová	Termerová	k1gFnSc1	Termerová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Lenka	Lenka	k1gFnSc1	Lenka
Vlasáková	Vlasáková	k1gFnSc1	Vlasáková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Lenka	Lenka	k1gFnSc1	Lenka
Zbranková	Zbranková	k1gFnSc1	Zbranková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
