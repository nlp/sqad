<s>
Eliška	Eliška	k1gFnSc1	Eliška
Bučková	Bučková	k1gFnSc1	Bučková
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1989	[number]	k4	1989
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
vítězka	vítězka	k1gFnSc1	vítězka
soutěže	soutěž	k1gFnSc2	soutěž
Česká	český	k2eAgFnSc1d1	Česká
Miss	miss	k1gFnSc1	miss
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Chodila	chodit	k5eAaImAgFnS	chodit
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
Strážnici	Strážnice	k1gFnSc6	Strážnice
na	na	k7c6	na
Hodonínsku	Hodonínsko	k1gNnSc6	Hodonínsko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
studovat	studovat	k5eAaImF	studovat
Arcibiskupské	arcibiskupský	k2eAgNnSc4d1	Arcibiskupské
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
ročníku	ročník	k1gInSc6	ročník
studium	studium	k1gNnSc4	studium
přerušila	přerušit	k5eAaPmAgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
Střední	střední	k2eAgFnSc4d1	střední
odbornou	odborný	k2eAgFnSc4d1	odborná
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
Strážnici	Strážnice	k1gFnSc6	Strážnice
(	(	kIx(	(
<g/>
obor	obor	k1gInSc1	obor
kosmetika	kosmetika	k1gFnSc1	kosmetika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
začala	začít	k5eAaPmAgFnS	začít
udržovat	udržovat	k5eAaImF	udržovat
vztah	vztah	k1gInSc4	vztah
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Václavem	Václav	k1gMnSc7	Václav
Noidem	Noid	k1gMnSc7	Noid
Bártou	Bárta	k1gMnSc7	Bárta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
dobrém	dobré	k1gNnSc6	dobré
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
se	se	k3xPyFc4	se
brát	brát	k5eAaImF	brát
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
přítelem	přítel	k1gMnSc7	přítel
stal	stát	k5eAaPmAgMnS	stát
fotbalista	fotbalista	k1gMnSc1	fotbalista
Jakub	Jakub	k1gMnSc1	Jakub
Malina	Malina	k1gMnSc1	Malina
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
miliardáře	miliardář	k1gMnSc2	miliardář
Tomáše	Tomáš	k1gMnSc2	Tomáš
Maliny	Malina	k1gMnSc2	Malina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
přiznala	přiznat	k5eAaPmAgFnS	přiznat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nS	trpět
poruchou	porucha	k1gFnSc7	porucha
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Kosmetické	kosmetický	k2eAgFnPc4d1	kosmetická
úpravy	úprava	k1gFnPc4	úprava
-	-	kIx~	-
nechala	nechat	k5eAaPmAgFnS	nechat
si	se	k3xPyFc3	se
laserem	laser	k1gInSc7	laser
odstranit	odstranit	k5eAaPmF	odstranit
mateřská	mateřský	k2eAgNnPc4d1	mateřské
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
si	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
odstranit	odstranit	k5eAaPmF	odstranit
pocení	pocený	k2eAgMnPc1d1	pocený
botulotoxinem	botulotoxin	k1gInSc7	botulotoxin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
soutěž	soutěž	k1gFnSc1	soutěž
Česká	český	k2eAgFnSc1d1	Česká
Miss	miss	k1gFnSc1	miss
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
Českou	český	k2eAgFnSc7d1	Česká
republika	republika	k1gFnSc1	republika
na	na	k7c4	na
Miss	miss	k1gFnSc4	miss
Universe	Universe	k1gFnSc2	Universe
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
80	[number]	k4	80
soutěžících	soutěžící	k1gMnPc2	soutěžící
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
TOP	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
soutěži	soutěž	k1gFnSc6	soutěž
Top	topit	k5eAaImRp2nS	topit
Model	model	k1gInSc1	model
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
působí	působit	k5eAaImIp3nP	působit
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
muzikálové	muzikálový	k2eAgFnSc6d1	muzikálová
scéně	scéna	k1gFnSc6	scéna
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Broadway	Broadwaa	k1gFnSc2	Broadwaa
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
a	a	k8xC	a
zpívá	zpívat	k5eAaImIp3nS	zpívat
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Hamlet	Hamlet	k1gMnSc1	Hamlet
The	The	k1gMnSc1	The
rock	rock	k1gInSc4	rock
opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
