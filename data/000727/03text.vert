<s>
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
je	být	k5eAaImIp3nS	být
pohádkový	pohádkový	k2eAgInSc4d1	pohádkový
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
mořské	mořský	k2eAgFnSc6d1	mořská
panně	panna	k1gFnSc6	panna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
lidského	lidský	k2eAgMnSc2d1	lidský
prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
stát	stát	k5eAaPmF	stát
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
jej	on	k3xPp3gNnSc4	on
dánský	dánský	k2eAgMnSc1d1	dánský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
byla	být	k5eAaImAgFnS	být
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
dcerou	dcera	k1gFnSc7	dcera
mořského	mořský	k2eAgMnSc2d1	mořský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
<g/>
,	,	kIx,	,
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
pěti	pět	k4xCc7	pět
sestrami	sestra	k1gFnPc7	sestra
v	v	k7c6	v
korálovém	korálový	k2eAgInSc6d1	korálový
zámku	zámek	k1gInSc6	zámek
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sester	sestra	k1gFnPc2	sestra
nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
se	se	k3xPyFc4	se
v	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgFnPc2	svůj
patnáctých	patnáctý	k4xOgFnPc2	patnáctý
narozenin	narozeniny	k1gFnPc2	narozeniny
mohla	moct	k5eAaImAgFnS	moct
poprvé	poprvé	k6eAd1	poprvé
vynořit	vynořit	k5eAaPmF	vynořit
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
spatřit	spatřit	k5eAaPmF	spatřit
svět	svět	k1gInSc4	svět
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
konečně	konečně	k6eAd1	konečně
nadešel	nadejít	k5eAaPmAgInS	nadejít
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
i	i	k9	i
pro	pro	k7c4	pro
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
mořskou	mořský	k2eAgFnSc4d1	mořská
princeznu	princezna	k1gFnSc4	princezna
<g/>
,	,	kIx,	,
spatřila	spatřit	k5eAaPmAgFnS	spatřit
loď	loď	k1gFnSc1	loď
krásného	krásný	k2eAgMnSc2d1	krásný
prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobal	podobat	k5eAaImAgMnS	podobat
mramorové	mramorový	k2eAgFnSc3d1	mramorová
soše	socha	k1gFnSc3	socha
mladíka	mladík	k1gMnSc2	mladík
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
zahrádce	zahrádka	k1gFnSc6	zahrádka
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
chodila	chodit	k5eAaImAgFnS	chodit
a	a	k8xC	a
kde	kde	k6eAd1	kde
také	také	k9	také
snila	snít	k5eAaImAgFnS	snít
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touhou	touha	k1gFnSc7	touha
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
pozorovala	pozorovat	k5eAaImAgNnP	pozorovat
veselící	veselící	k2eAgInSc4d1	veselící
se	s	k7c7	s
dvořany	dvořan	k1gMnPc7	dvořan
<g/>
,	,	kIx,	,
oslavující	oslavující	k2eAgFnPc4d1	oslavující
princovy	princův	k2eAgFnPc4d1	princova
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Vtom	vtom	k6eAd1	vtom
se	se	k3xPyFc4	se
strhla	strhnout	k5eAaPmAgFnS	strhnout
silná	silný	k2eAgFnSc1d1	silná
bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
potopila	potopit	k5eAaPmAgFnS	potopit
loď	loď	k1gFnSc1	loď
i	i	k9	i
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
princezna	princezna	k1gFnSc1	princezna
se	se	k3xPyFc4	se
zaradovala	zaradovat	k5eAaPmAgFnS	zaradovat
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
si	se	k3xPyFc3	se
však	však	k9	však
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
zámku	zámek	k1gInSc2	zámek
dostávají	dostávat	k5eAaImIp3nP	dostávat
pouze	pouze	k6eAd1	pouze
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
prince	princa	k1gFnSc3	princa
zachránit	zachránit	k5eAaPmF	zachránit
a	a	k8xC	a
odnesla	odnést	k5eAaPmAgFnS	odnést
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
ujala	ujmout	k5eAaPmAgFnS	ujmout
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
právě	právě	k6eAd1	právě
přicházely	přicházet	k5eAaImAgFnP	přicházet
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
stojícího	stojící	k2eAgMnSc4d1	stojící
poblíž	poblíž	k6eAd1	poblíž
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
co	co	k9	co
den	den	k1gInSc4	den
pak	pak	k6eAd1	pak
tesknila	tesknit	k5eAaImAgFnS	tesknit
po	po	k7c6	po
princi	princa	k1gFnSc6	princa
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jí	on	k3xPp3gFnSc2	on
sestry	sestra	k1gFnSc2	sestra
nepomohly	pomoct	k5eNaPmAgInP	pomoct
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
zámek	zámek	k1gInSc4	zámek
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
trávila	trávit	k5eAaImAgFnS	trávit
mnoho	mnoho	k4c4	mnoho
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
babiččina	babiččin	k2eAgNnSc2d1	Babiččino
vyprávění	vyprávění	k1gNnSc2	vyprávění
se	se	k3xPyFc4	se
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mořská	mořský	k2eAgFnSc1d1	mořská
žena	žena	k1gFnSc1	žena
stala	stát	k5eAaPmAgFnS	stát
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
lidskou	lidský	k2eAgFnSc4d1	lidská
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamilovat	zamilovat	k5eAaPmF	zamilovat
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
pojmout	pojmout	k5eAaPmF	pojmout
jí	on	k3xPp3gFnSc7	on
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
ruce	ruka	k1gFnPc1	ruka
spojí	spojit	k5eAaPmIp3nP	spojit
<g/>
,	,	kIx,	,
přejde	přejít	k5eAaPmIp3nS	přejít
jeho	jeho	k3xOp3gFnSc1	jeho
duše	duše	k1gFnSc1	duše
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
ztratil	ztratit	k5eAaPmAgInS	ztratit
svou	svůj	k3xOyFgFnSc7	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
ale	ale	k9	ale
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
první	první	k4xOgNnSc4	první
jitro	jitro	k1gNnSc4	jitro
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
víla	víla	k1gFnSc1	víla
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
toto	tento	k3xDgNnSc4	tento
riziko	riziko	k1gNnSc4	riziko
podstoupit	podstoupit	k5eAaPmF	podstoupit
a	a	k8xC	a
navštíví	navštívit	k5eAaPmIp3nS	navštívit
mořskou	mořský	k2eAgFnSc4d1	mořská
čarodějnici	čarodějnice	k1gFnSc4	čarodějnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
<g/>
,	,	kIx,	,
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
nejcennější	cenný	k2eAgFnSc4d3	nejcennější
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
nejkrásnější	krásný	k2eAgInSc1d3	nejkrásnější
hlas	hlas	k1gInSc1	hlas
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
připraví	připravit	k5eAaPmIp3nS	připravit
nápoj	nápoj	k1gInSc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nápoj	nápoj	k1gInSc1	nápoj
promění	proměnit	k5eAaPmIp3nS	proměnit
rybí	rybí	k2eAgInSc1d1	rybí
ocas	ocas	k1gInSc1	ocas
v	v	k7c4	v
překrásné	překrásný	k2eAgFnPc4d1	překrásná
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
ladnosti	ladnost	k1gFnPc1	ladnost
v	v	k7c6	v
chůzi	chůze	k1gFnSc6	chůze
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
žádná	žádný	k3yNgFnSc1	žádný
smrtelnice	smrtelnice	k1gFnSc2	smrtelnice
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
poté	poté	k6eAd1	poté
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc7	on
okouzlen	okouzlit	k5eAaPmNgMnS	okouzlit
a	a	k8xC	a
tráví	trávit	k5eAaImIp3nS	trávit
s	s	k7c7	s
půvabnou	půvabný	k2eAgFnSc7d1	půvabná
neznámou	neznámá	k1gFnSc7	neznámá
veškerý	veškerý	k3xTgInSc4	veškerý
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
ale	ale	k8xC	ale
již	již	k6eAd1	již
věnoval	věnovat	k5eAaImAgMnS	věnovat
dívce	dívka	k1gFnSc3	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
zachránila	zachránit	k5eAaPmAgFnS	zachránit
po	po	k7c6	po
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
jíž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jí	jíst	k5eAaImIp3nS	jíst
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
princem	princ	k1gMnSc7	princ
sklonila	sklonit	k5eAaPmAgFnS	sklonit
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
u	u	k7c2	u
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otcovo	otcův	k2eAgNnSc4d1	otcovo
přání	přání	k1gNnSc4	přání
se	se	k3xPyFc4	se
princ	princ	k1gMnSc1	princ
vydává	vydávat	k5eAaPmIp3nS	vydávat
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dvořil	dvořit	k5eAaImAgInS	dvořit
dceři	dcera	k1gFnSc3	dcera
sousedního	sousední	k2eAgMnSc2d1	sousední
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
je	být	k5eAaImIp3nS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
královou	králův	k2eAgFnSc7d1	králova
dcerou	dcera	k1gFnSc7	dcera
je	být	k5eAaImIp3nS	být
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
našla	najít	k5eAaPmAgFnS	najít
prince	princ	k1gMnPc4	princ
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Oslav	oslava	k1gFnPc2	oslava
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
před	před	k7c7	před
úsvitem	úsvit	k1gInSc7	úsvit
navštíví	navštívit	k5eAaPmIp3nP	navštívit
její	její	k3xOp3gFnPc1	její
sestry	sestra	k1gFnPc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Nechaly	nechat	k5eAaPmAgFnP	nechat
si	se	k3xPyFc3	se
od	od	k7c2	od
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
ustřihnout	ustřihnout	k5eAaPmF	ustřihnout
vlasy	vlas	k1gInPc4	vlas
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
mořské	mořský	k2eAgFnSc2d1	mořská
víle	víla	k1gFnSc6	víla
změnila	změnit	k5eAaPmAgFnS	změnit
rybí	rybí	k2eAgInSc4d1	rybí
ocas	ocas	k1gInSc4	ocas
v	v	k7c4	v
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
může	moct	k5eAaImIp3nS	moct
kouzlo	kouzlo	k1gNnSc4	kouzlo
opět	opět	k6eAd1	opět
zvrátit	zvrátit	k5eAaPmF	zvrátit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dýkou	dýka	k1gFnSc7	dýka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
se	se	k3xPyFc4	se
víla	víla	k1gFnSc1	víla
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
ke	k	k7c3	k
stanu	stan	k1gInSc3	stan
novomanželů	novomanžel	k1gMnPc2	novomanžel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gInPc4	on
uvidí	uvidět	k5eAaPmIp3nP	uvidět
spát	spát	k5eAaImF	spát
v	v	k7c6	v
objetí	objetí	k1gNnSc6	objetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mžiku	mžik	k1gInSc6	mžik
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
odhodí	odhodit	k5eAaPmIp3nS	odhodit
nůž	nůž	k1gInSc4	nůž
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgInSc7	jenž
skočí	skočit	k5eAaPmIp3nP	skočit
pak	pak	k6eAd1	pak
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
paprsky	paprsek	k1gInPc7	paprsek
slunce	slunce	k1gNnSc1	slunce
do	do	k7c2	do
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Nezemře	zemřít	k5eNaPmIp3nS	zemřít
však	však	k9	však
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přidá	přidat	k5eAaPmIp3nS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
sestrám	sestra	k1gFnPc3	sestra
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
stech	sto	k4xCgNnPc6	sto
letech	léto	k1gNnPc6	léto
služby	služba	k1gFnSc2	služba
získají	získat	k5eAaPmIp3nP	získat
nesmrtelnou	smrtelný	k2eNgFnSc4d1	nesmrtelná
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
stavení	stavení	k1gNnSc6	stavení
naleznou	nalézt	k5eAaBmIp3nP	nalézt
hodné	hodný	k2eAgNnSc4d1	hodné
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
ale	ale	k8xC	ale
musejí	muset	k5eAaImIp3nP	muset
zaplakat	zaplakat	k5eAaPmF	zaplakat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
zkušební	zkušební	k2eAgFnSc1d1	zkušební
doba	doba	k1gFnSc1	doba
se	se	k3xPyFc4	se
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
prolitou	prolitý	k2eAgFnSc7d1	prolitá
slzou	slza	k1gFnSc7	slza
o	o	k7c4	o
den	den	k1gInSc4	den
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
(	(	kIx(	(
<g/>
Andersen	Andersen	k1gMnSc1	Andersen
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
napsal	napsat	k5eAaBmAgMnS	napsat
jen	jen	k9	jen
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
víla	víla	k1gFnSc1	víla
pouze	pouze	k6eAd1	pouze
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
mořskou	mořský	k2eAgFnSc4d1	mořská
pěnu	pěna	k1gFnSc4	pěna
či	či	k8xC	či
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
dokonce	dokonce	k9	dokonce
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
ustrne	ustrnout	k5eAaPmIp3nS	ustrnout
a	a	k8xC	a
změní	změnit	k5eAaPmIp3nS	změnit
ji	on	k3xPp3gFnSc4	on
zpět	zpět	k6eAd1	zpět
v	v	k7c4	v
mořskou	mořský	k2eAgFnSc4d1	mořská
vílu	víla	k1gFnSc4	víla
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc4	režisér
Karla	Karel	k1gMnSc4	Karel
Kachyni	Kachyně	k1gFnSc6	Kachyně
<g/>
,	,	kIx,	,
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
s	s	k7c7	s
Miroslavou	Miroslava	k1gFnSc7	Miroslava
Šafránkovou	Šafránková	k1gFnSc7	Šafránková
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
-	-	kIx~	-
kreslená	kreslený	k2eAgFnSc1d1	kreslená
pohádka	pohádka	k1gFnSc1	pohádka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
filmového	filmový	k2eAgNnSc2d1	filmové
studia	studio	k1gNnSc2	studio
The	The	k1gMnSc1	The
Walt	Walt	k1gMnSc1	Walt
Disney	Disne	k2eAgFnPc4d1	Disne
Company	Compana	k1gFnPc4	Compana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc1	tento
studio	studio	k1gNnSc1	studio
uvedlo	uvést	k5eAaPmAgNnS	uvést
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
animovaný	animovaný	k2eAgInSc4d1	animovaný
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gFnSc1	The
Little	Little	k1gFnSc2	Little
Mermaid	Mermaida	k1gFnPc2	Mermaida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
pohádkový	pohádkový	k2eAgInSc4d1	pohádkový
příběh	příběh	k1gInSc4	příběh
navazuje	navazovat	k5eAaImIp3nS	navazovat
velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
i	i	k9	i
netradičně	tradičně	k6eNd1	tradičně
<g/>
.	.	kIx.	.
</s>
<s>
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
<g/>
:	:	kIx,	:
Pohádky	pohádka	k1gFnSc2	pohádka
a	a	k8xC	a
povídky	povídka	k1gFnSc2	povídka
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
-	-	kIx~	-
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
il	il	k?	il
<g/>
.	.	kIx.	.
</s>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
Bouda	Bouda	k1gMnSc1	Bouda
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
<g/>
:	:	kIx,	:
Pohádky	pohádka	k1gFnPc1	pohádka
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
<g/>
:	:	kIx,	:
Pohádky	pohádka	k1gFnPc1	pohádka
<g/>
,	,	kIx,	,
Readers	Readers	k1gInSc1	Readers
Digest	Digest	k1gInSc1	Digest
VÝBĚR	výběr	k1gInSc4	výběr
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Malá	Malá	k1gFnSc1	Malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc4	dílo
Vodní	vodní	k2eAgFnSc1d1	vodní
panna	panna	k1gFnSc1	panna
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
