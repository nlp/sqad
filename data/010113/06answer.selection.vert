<s>
Kniha	kniha	k1gFnSc1	kniha
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originálu	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Magician	Magician	k1gMnSc1	Magician
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Nephew	Nephew	k1gFnSc7	Nephew
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
knih	kniha	k1gFnPc2	kniha
série	série	k1gFnSc2	série
Letopisů	letopis	k1gInPc2	letopis
Narnie	Narnie	k1gFnSc2	Narnie
C.	C.	kA	C.
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc1	Lewise
<g/>
.	.	kIx.	.
</s>
