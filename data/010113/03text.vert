<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originálu	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Magician	Magician	k1gMnSc1	Magician
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Nephew	Nephew	k1gFnSc7	Nephew
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
knih	kniha	k1gFnPc2	kniha
série	série	k1gFnSc2	série
Letopisů	letopis	k1gInPc2	letopis
Narnie	Narnie	k1gFnSc2	Narnie
C.	C.	kA	C.
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc1	Lewise
<g/>
.	.	kIx.	.
</s>
<s>
Vydána	vydán	k2eAgFnSc1d1	vydána
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
předposlední	předposlední	k2eAgFnSc1d1	předposlední
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chronologicky	chronologicky	k6eAd1	chronologicky
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc1	první
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
prequel	prequel	k1gInSc4	prequel
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
knihy	kniha	k1gFnSc2	kniha
Kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
dosud	dosud	k6eAd1	dosud
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Orbis	orbis	k1gInSc1	orbis
pictus	pictus	k1gInSc4	pictus
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
a	a	k8xC	a
potřetí	potřetí	k4xO	potřetí
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Fragment	fragment	k1gInSc4	fragment
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Pauline	Paulin	k1gInSc5	Paulin
Baynesové	Baynesový	k2eAgFnSc6d1	Baynesová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
světě	svět	k1gInSc6	svět
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
světa	svět	k1gInSc2	svět
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	on	k3xPp3gNnSc2	on
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
kontinuitu	kontinuita	k1gFnSc4	kontinuita
celé	celý	k2eAgFnSc2d1	celá
sedmidílné	sedmidílný	k2eAgFnSc2d1	sedmidílná
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
původ	původ	k1gInSc4	původ
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
Jadis	Jadis	k1gFnSc2	Jadis
<g/>
,	,	kIx,	,
skříně	skříň	k1gFnSc2	skříň
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
knize	kniha	k1gFnSc6	kniha
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
Pevensiovi	Pevensiův	k2eAgMnPc1d1	Pevensiův
a	a	k8xC	a
také	také	k9	také
profesora	profesor	k1gMnSc4	profesor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jim	on	k3xPp3gMnPc3	on
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
V	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
jsou	být	k5eAaImIp3nP	být
jména	jméno	k1gNnPc1	jméno
použitá	použitý	k2eAgNnPc1d1	Použité
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
českém	český	k2eAgNnSc6d1	české
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
většinou	většinou	k6eAd1	většinou
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
originálu	originál	k1gInSc3	originál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Diviš	Diviš	k1gMnSc1	Diviš
(	(	kIx(	(
<g/>
Digory	Digor	k1gInPc1	Digor
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gábina	Gábin	k2eAgMnSc2d1	Gábin
(	(	kIx(	(
<g/>
Polly	Polla	k1gMnSc2	Polla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
půdu	půda	k1gFnSc4	půda
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
kterého	který	k3yRgNnSc2	který
bydlí	bydlet	k5eAaImIp3nS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěně	chtěně	k6eNd1	chtěně
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
tak	tak	k6eAd1	tak
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
pracovny	pracovna	k1gFnSc2	pracovna
Divišova	Divišův	k2eAgMnSc2d1	Divišův
strýčka	strýček	k1gMnSc2	strýček
Ondřeje	Ondřej	k1gMnSc2	Ondřej
(	(	kIx(	(
<g/>
Andrewa	Andrewus	k1gMnSc2	Andrewus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
Gábině	Gábina	k1gFnSc3	Gábina
žlutý	žlutý	k2eAgInSc4d1	žlutý
prsten	prsten	k1gInSc4	prsten
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
po	po	k7c6	po
dotyku	dotyk	k1gInSc6	dotyk
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diviš	Diviš	k1gMnSc1	Diviš
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
od	od	k7c2	od
strýčka	strýček	k1gMnSc2	strýček
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
provádí	provádět	k5eAaImIp3nS	provádět
pokusy	pokus	k1gInPc4	pokus
s	s	k7c7	s
magií	magie	k1gFnSc7	magie
a	a	k8xC	a
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
si	se	k3xPyFc3	se
ověřit	ověřit	k5eAaPmF	ověřit
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
žluté	žlutý	k2eAgInPc1d1	žlutý
prsteny	prsten	k1gInPc1	prsten
přenesou	přenést	k5eAaPmIp3nP	přenést
člověka	člověk	k1gMnSc4	člověk
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
a	a	k8xC	a
zelené	zelená	k1gFnSc2	zelená
zase	zase	k9	zase
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Gábina	Gábina	k1gFnSc1	Gábina
bude	být	k5eAaImBp3nS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
Diviš	Diviš	k1gMnSc1	Diviš
nevypraví	vypravit	k5eNaPmIp3nS	vypravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diviš	Diviš	k1gMnSc1	Diviš
je	být	k5eAaImIp3nS	být
znechucen	znechutit	k5eAaPmNgMnS	znechutit
Ondřejovou	Ondřejův	k2eAgFnSc7d1	Ondřejova
lstivostí	lstivost	k1gFnSc7	lstivost
a	a	k8xC	a
zbabělostí	zbabělost	k1gFnSc7	zbabělost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydá	vydat	k5eAaPmIp3nS	vydat
se	se	k3xPyFc4	se
přivést	přivést	k5eAaPmF	přivést
Gábinu	Gábina	k1gFnSc4	Gábina
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
vybaven	vybaven	k2eAgInSc4d1	vybaven
žlutým	žlutý	k2eAgInSc7d1	žlutý
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
zelenými	zelený	k2eAgInPc7d1	zelený
prsteny	prsten	k1gInPc7	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
se	se	k3xPyFc4	se
u	u	k7c2	u
Gábiny	Gábina	k1gFnSc2	Gábina
v	v	k7c6	v
lese	les	k1gInSc6	les
plném	plný	k2eAgInSc6d1	plný
jezírek	jezírko	k1gNnPc2	jezírko
<g/>
,	,	kIx,	,
zvaném	zvaný	k2eAgInSc6d1	zvaný
Les	les	k1gInSc4	les
mezi	mezi	k7c7	mezi
světy	svět	k1gInPc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgNnSc7	každý
jezírkem	jezírko	k1gNnSc7	jezírko
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
návratem	návrat	k1gInSc7	návrat
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
zpustošeného	zpustošený	k2eAgInSc2d1	zpustošený
světa	svět	k1gInSc2	svět
jménem	jméno	k1gNnSc7	jméno
Šárn	Šárn	k1gNnSc1	Šárn
(	(	kIx(	(
<g/>
Charn	Charn	k1gInSc1	Charn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozpadlém	rozpadlý	k2eAgInSc6d1	rozpadlý
paláci	palác	k1gInSc6	palác
najdou	najít	k5eAaPmIp3nP	najít
paličku	palička	k1gFnSc4	palička
a	a	k8xC	a
zvonek	zvonek	k1gInSc4	zvonek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Gábina	Gábina	k1gFnSc1	Gábina
chce	chtít	k5eAaImIp3nS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
Diviš	Diviš	k1gMnSc1	Diviš
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
násilím	násilí	k1gNnSc7	násilí
zabrání	zabrání	k1gNnPc2	zabrání
a	a	k8xC	a
zazvoní	zazvonit	k5eAaPmIp3nS	zazvonit
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
oživí	oživit	k5eAaPmIp3nP	oživit
čarodějnici	čarodějnice	k1gFnSc4	čarodějnice
Jadis	Jadis	k1gFnSc2	Jadis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kdysi	kdysi	k6eAd1	kdysi
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
světě	svět	k1gInSc6	svět
zkázonosnou	zkázonosný	k2eAgFnSc4d1	zkázonosná
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
pověsí	pověsit	k5eAaPmIp3nP	pověsit
na	na	k7c4	na
paty	pata	k1gFnPc4	pata
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
sice	sice	k8xC	sice
nemá	mít	k5eNaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
magické	magický	k2eAgFnPc4d1	magická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neztratí	ztratit	k5eNaPmIp3nP	ztratit
svoji	svůj	k3xOyFgFnSc4	svůj
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
touhu	touha	k1gFnSc4	touha
dobývat	dobývat	k5eAaImF	dobývat
<g/>
.	.	kIx.	.
</s>
<s>
Strýc	strýc	k1gMnSc1	strýc
Ondřej	Ondřej	k1gMnSc1	Ondřej
jí	on	k3xPp3gFnSc3	on
posluhuje	posluhovat	k5eAaImIp3nS	posluhovat
<g/>
.	.	kIx.	.
</s>
<s>
Jadis	Jadis	k1gFnSc1	Jadis
všem	všecek	k3xTgMnPc3	všecek
vyhrožuje	vyhrožovat	k5eAaImIp3nS	vyhrožovat
a	a	k8xC	a
fyzicky	fyzicky	k6eAd1	fyzicky
je	on	k3xPp3gMnPc4	on
napadá	napadat	k5eAaImIp3nS	napadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diviš	Diviš	k1gMnSc1	Diviš
ji	on	k3xPp3gFnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
našeho	náš	k3xOp1gInSc2	náš
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
s	s	k7c7	s
Gábinou	Gábina	k1gFnSc7	Gábina
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
dotknout	dotknout	k5eAaPmF	dotknout
a	a	k8xC	a
současně	současně	k6eAd1	současně
si	se	k3xPyFc3	se
nasadit	nasadit	k5eAaPmF	nasadit
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
udělá	udělat	k5eAaPmIp3nS	udělat
<g/>
,	,	kIx,	,
nechtěně	chtěně	k6eNd1	chtěně
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vezme	vzít	k5eAaPmIp3nS	vzít
i	i	k9	i
všechny	všechen	k3xTgMnPc4	všechen
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
dotýkala	dotýkat	k5eAaImAgFnS	dotýkat
Jadis	Jadis	k1gFnSc1	Jadis
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
Lese	les	k1gInSc6	les
mezi	mezi	k7c7	mezi
světy	svět	k1gInPc7	svět
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
vedle	vedle	k7c2	vedle
Gábiny	Gábina	k1gFnSc2	Gábina
a	a	k8xC	a
Jadis	Jadis	k1gFnSc2	Jadis
i	i	k9	i
drožkář	drožkář	k1gMnSc1	drožkář
František	František	k1gMnSc1	František
s	s	k7c7	s
koněm	kůň	k1gMnSc7	kůň
Jahůdkou	jahůdka	k1gFnSc7	jahůdka
(	(	kIx(	(
<g/>
Jahodou	jahoda	k1gFnSc7	jahoda
<g/>
)	)	kIx)	)
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
bez	bez	k7c2	bez
Jadis	Jadis	k1gFnSc2	Jadis
<g/>
,	,	kIx,	,
nasadí	nasadit	k5eAaPmIp3nP	nasadit
si	se	k3xPyFc3	se
zelené	zelený	k2eAgInPc4d1	zelený
prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
chystají	chystat	k5eAaImIp3nP	chystat
se	se	k3xPyFc4	se
skočit	skočit	k5eAaPmF	skočit
do	do	k7c2	do
jezírka	jezírko	k1gNnSc2	jezírko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
našeho	náš	k3xOp1gInSc2	náš
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
ale	ale	k9	ale
Jahoda	jahoda	k1gFnSc1	jahoda
napije	napít	k5eAaBmIp3nS	napít
z	z	k7c2	z
jiného	jiný	k2eAgNnSc2d1	jiné
jezírka	jezírko	k1gNnSc2	jezírko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Jadis	Jadis	k1gFnSc2	Jadis
<g/>
,	,	kIx,	,
přeneseni	přenesen	k2eAgMnPc1d1	přenesen
do	do	k7c2	do
světa	svět	k1gInSc2	svět
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
stvořen	stvořit	k5eAaPmNgMnS	stvořit
před	před	k7c7	před
několika	několik	k4yIc7	několik
hodinami	hodina	k1gFnPc7	hodina
a	a	k8xC	a
laskavý	laskavý	k2eAgInSc1d1	laskavý
lev	lev	k1gInSc1	lev
Aslan	Aslana	k1gFnPc2	Aslana
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
právě	právě	k6eAd1	právě
dává	dávat	k5eAaImIp3nS	dávat
vzejít	vzejít	k5eAaPmF	vzejít
rostlinám	rostlina	k1gFnPc3	rostlina
a	a	k8xC	a
živočichům	živočich	k1gMnPc3	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
zvířatům	zvíře	k1gNnPc3	zvíře
daruje	darovat	k5eAaPmIp3nS	darovat
rozum	rozum	k1gInSc1	rozum
a	a	k8xC	a
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
ovšem	ovšem	k9	ovšem
nepoznají	poznat	k5eNaPmIp3nP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ondřej	Ondřej	k1gMnSc1	Ondřej
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jej	on	k3xPp3gMnSc4	on
zasadí	zasadit	k5eAaPmIp3nP	zasadit
a	a	k8xC	a
zalévají	zalévat	k5eAaImIp3nP	zalévat
jako	jako	k9	jako
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Aslan	Aslan	k1gInSc1	Aslan
chce	chtít	k5eAaImIp3nS	chtít
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
mu	on	k3xPp3gMnSc3	on
kvůli	kvůli	k7c3	kvůli
svých	svůj	k3xOyFgNnPc6	svůj
hříchům	hřích	k1gInPc3	hřích
vůbec	vůbec	k9	vůbec
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
na	na	k7c4	na
něho	on	k3xPp3gInSc4	on
Aslan	Aslan	k1gInSc4	Aslan
alespoň	alespoň	k9	alespoň
sešle	sešle	k6eAd1	sešle
posilující	posilující	k2eAgInSc1d1	posilující
spánek	spánek	k1gInSc1	spánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aslan	Aslan	k1gInSc1	Aslan
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
drožkáři	drožkář	k1gMnSc3	drožkář
Františkovi	František	k1gMnSc3	František
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
učiní	učinit	k5eAaPmIp3nS	učinit
prvním	první	k4xOgMnSc7	první
narnijským	narnijský	k2eAgMnSc7d1	narnijský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Aslan	Aslan	k1gInSc1	Aslan
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
přenese	přenést	k5eAaPmIp3nS	přenést
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
Helenu	Helena	k1gFnSc4	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Aslan	Aslan	k1gInSc1	Aslan
napomene	napomenout	k5eAaPmIp3nS	napomenout
Diviše	Diviš	k1gMnSc4	Diviš
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ke	k	k7c3	k
Gábině	Gábina	k1gFnSc3	Gábina
zachoval	zachovat	k5eAaPmAgMnS	zachovat
v	v	k7c6	v
Charnu	Charn	k1gInSc6	Charn
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc7	jeho
vinou	vina	k1gFnSc7	vina
je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
v	v	k7c6	v
Narnii	Narnie	k1gFnSc6	Narnie
zlá	zlý	k2eAgFnSc1d1	zlá
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
vnese	vnést	k5eAaPmIp3nS	vnést
zlo	zlo	k1gNnSc4	zlo
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
zlem	zlo	k1gNnSc7	zlo
je	být	k5eAaImIp3nS	být
stoletá	stoletý	k2eAgFnSc1d1	stoletá
zima	zima	k1gFnSc1	zima
popsaná	popsaný	k2eAgFnSc1d1	popsaná
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aslan	Aslan	k1gInSc1	Aslan
pošle	poslat	k5eAaPmIp3nS	poslat
Diviše	Diviš	k1gMnSc4	Diviš
pro	pro	k7c4	pro
kouzelné	kouzelný	k2eAgNnSc4d1	kouzelné
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odčinil	odčinit	k5eAaPmAgMnS	odčinit
svůj	svůj	k3xOyFgInSc4	svůj
hřích	hřích	k1gInSc4	hřích
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
Narnie	Narnie	k1gFnSc1	Narnie
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
staletí	staletí	k1gNnPc2	staletí
chráněná	chráněný	k2eAgFnSc1d1	chráněná
před	před	k7c4	před
Jadis	Jadis	k1gInSc4	Jadis
ochranným	ochranný	k2eAgInSc7d1	ochranný
stromem	strom	k1gInSc7	strom
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Diviš	Diviš	k1gMnSc1	Diviš
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vydává	vydávat	k5eAaPmIp3nS	vydávat
s	s	k7c7	s
Gábinou	Gábina	k1gFnSc7	Gábina
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
Jahodovi	Jahoda	k1gMnSc6	Jahoda
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
Aslan	Aslan	k1gInSc1	Aslan
daroval	darovat	k5eAaPmAgInS	darovat
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
mluvit	mluvit	k5eAaImF	mluvit
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Křídlín	Křídlín	k1gInSc1	Křídlín
(	(	kIx(	(
<g/>
Peruť	peruť	k1gFnSc1	peruť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Diviš	Diviš	k1gMnSc1	Diviš
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
jabloni	jabloň	k1gFnSc3	jabloň
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
Jadis	Jadis	k1gFnSc1	Jadis
a	a	k8xC	a
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
neodnášel	odnášet	k5eNaImAgInS	odnášet
Aslanovi	Aslan	k1gMnSc3	Aslan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
světa	svět	k1gInSc2	svět
a	a	k8xC	a
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
jím	jíst	k5eAaImIp1nS	jíst
svoji	svůj	k3xOyFgFnSc4	svůj
nemocnou	nemocný	k2eAgFnSc4d1	nemocná
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diviš	Diviš	k1gMnSc1	Diviš
to	ten	k3xDgNnSc4	ten
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
a	a	k8xC	a
donese	donést	k5eAaPmIp3nS	donést
jablko	jablko	k1gNnSc4	jablko
Aslanovi	Aslan	k1gMnSc3	Aslan
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
matčino	matčin	k2eAgNnSc1d1	matčino
uzdravení	uzdravení	k1gNnSc1	uzdravení
by	by	kYmCp3nP	by
přineslo	přinést	k5eAaPmAgNnS	přinést
mnoho	mnoho	k4c4	mnoho
žalu	žal	k1gInSc2	žal
a	a	k8xC	a
zármutku	zármutek	k1gInSc2	zármutek
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
dá	dát	k5eAaPmIp3nS	dát
Divišovi	Diviš	k1gMnSc3	Diviš
jiné	jiný	k2eAgNnSc1d1	jiné
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přinese	přinést	k5eAaPmIp3nS	přinést
jeho	jeho	k3xOp3gFnSc3	jeho
matce	matka	k1gFnSc3	matka
zdraví	zdravit	k5eAaImIp3nS	zdravit
i	i	k9	i
radost	radost	k1gFnSc1	radost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aslan	Aslan	k1gInSc1	Aslan
pak	pak	k6eAd1	pak
přenese	přenést	k5eAaPmIp3nS	přenést
Diviše	Diviš	k1gMnSc4	Diviš
s	s	k7c7	s
Gábinou	Gábina	k1gFnSc7	Gábina
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
tam	tam	k6eAd1	tam
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
pokyn	pokyn	k1gInSc4	pokyn
prsteny	prsten	k1gInPc1	prsten
zakopou	zakopat	k5eAaPmIp3nP	zakopat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gFnPc4	on
již	již	k6eAd1	již
nikdo	nikdo	k3yNnSc1	nikdo
nemohl	moct	k5eNaImAgMnS	moct
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
místě	místo	k1gNnSc6	místo
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
strom	strom	k1gInSc1	strom
a	a	k8xC	a
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
dřeva	dřevo	k1gNnSc2	dřevo
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
skříň	skříň	k1gFnSc1	skříň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Diviš	Diviš	k1gMnSc1	Diviš
stane	stanout	k5eAaPmIp3nS	stanout
učeným	učený	k2eAgMnSc7d1	učený
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
sídle	sídlo	k1gNnSc6	sídlo
<g/>
,	,	kIx,	,
projdou	projít	k5eAaPmIp3nP	projít
touto	tento	k3xDgFnSc7	tento
skříní	skříň	k1gFnSc7	skříň
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
sourozenci	sourozenec	k1gMnSc3	sourozenec
Pevensieovi	Pevensieus	k1gMnSc3	Pevensieus
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
<g/>
.	.	kIx.	.
</s>
</p>
