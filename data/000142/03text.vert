<s>
John	John	k1gMnSc1	John
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gInSc5	Stip
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
v	v	k7c6	v
Decatur	Decatura	k1gFnPc2	Decatura
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
kapely	kapela	k1gFnSc2	kapela
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
mumlavý	mumlavý	k2eAgInSc4d1	mumlavý
styl	styl	k1gInSc4	styl
zpěvu	zpěv	k1gInSc2	zpěv
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
komplexní	komplexní	k2eAgInPc4d1	komplexní
a	a	k8xC	a
surrealistické	surrealistický	k2eAgInPc4d1	surrealistický
texty	text	k1gInPc4	text
a	a	k8xC	a
politický	politický	k2eAgInSc4d1	politický
aktivismus	aktivismus	k1gInSc4	aktivismus
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
členy	člen	k1gInPc7	člen
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
kmotrem	kmotr	k1gMnSc7	kmotr
<g/>
"	"	kIx"	"
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
možná	možná	k9	možná
o	o	k7c4	o
první	první	k4xOgFnSc4	první
kapelu	kapela	k1gFnSc4	kapela
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
věhlas	věhlas	k1gInSc4	věhlas
a	a	k8xC	a
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
řadu	řada	k1gFnSc4	řada
"	"	kIx"	"
<g/>
alternativních	alternativní	k2eAgFnPc2d1	alternativní
<g/>
"	"	kIx"	"
kapel	kapela	k1gFnPc2	kapela
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Radiohead	Radiohead	k1gInSc1	Radiohead
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gInSc1	Pearl
Jam	jáma	k1gFnPc2	jáma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Nirvanu	Nirvan	k1gMnSc3	Nirvan
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
seznamámil	seznamámit	k5eAaPmAgMnS	seznamámit
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Buckem	bucek	k1gMnSc7	bucek
<g/>
,	,	kIx,	,
Billem	Bill	k1gMnSc7	Bill
Berrym	Berrym	k1gInSc4	Berrym
a	a	k8xC	a
Mikem	Mik	k1gMnSc7	Mik
Millsem	Mills	k1gMnSc7	Mills
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
studoval	studovat	k5eAaImAgMnS	studovat
fotografii	fotografia	k1gFnSc4	fotografia
a	a	k8xC	a
malbu	malba	k1gFnSc4	malba
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
kapelu	kapela	k1gFnSc4	kapela
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc4	radio
Free	Fre	k1gFnSc2	Fre
Europe	Europ	k1gInSc5	Europ
<g/>
"	"	kIx"	"
vydali	vydat	k5eAaPmAgMnP	vydat
pod	pod	k7c7	pod
labelem	label	k1gInSc7	label
Hib-Tone	Hib-Ton	k1gMnSc5	Hib-Ton
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
úspěchu	úspěch	k1gInSc3	úspěch
na	na	k7c6	na
univerzitním	univerzitní	k2eAgNnSc6d1	univerzitní
rádiu	rádio	k1gNnSc6	rádio
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
vydala	vydat	k5eAaPmAgFnS	vydat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
EP	EP	kA	EP
Chronic	Chronice	k1gFnPc2	Chronice
Town	Towna	k1gFnPc2	Towna
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Murmur	Murmura	k1gFnPc2	Murmura
vydali	vydat	k5eAaPmAgMnP	vydat
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
následováno	následován	k2eAgNnSc1d1	následováno
sérií	série	k1gFnSc7	série
dalších	další	k2eAgNnPc2d1	další
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
pozitivně	pozitivně	k6eAd1	pozitivně
hodnoceny	hodnocen	k2eAgFnPc1d1	hodnocena
kritikou	kritika	k1gFnSc7	kritika
a	a	k8xC	a
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
relativně	relativně	k6eAd1	relativně
slušeného	slušený	k2eAgInSc2d1	slušený
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
singlů	singl	k1gInPc2	singl
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gInSc1	jeho
aktivismus	aktivismus	k1gInSc1	aktivismus
mu	on	k3xPp3gMnSc3	on
přinesly	přinést	k5eAaPmAgFnP	přinést
status	status	k1gInSc4	status
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
oddanou	oddaný	k2eAgFnSc4d1	oddaná
fandovskou	fandovský	k2eAgFnSc4d1	fandovská
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
River	River	k1gMnSc1	River
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
bylo	být	k5eAaImAgNnS	být
věnováno	věnovat	k5eAaPmNgNnS	věnovat
album	album	k1gNnSc4	album
Monster	monstrum	k1gNnPc2	monstrum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
byl	být	k5eAaImAgMnS	být
i	i	k9	i
zpěvák	zpěvák	k1gMnSc1	zpěvák
Nirvany	Nirvan	k1gMnPc4	Nirvan
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Cobainem	Cobaino	k1gNnSc7	Cobaino
plánovali	plánovat	k5eAaImAgMnP	plánovat
společný	společný	k2eAgInSc4d1	společný
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
Cobainovou	Cobainový	k2eAgFnSc7d1	Cobainový
sebevraždou	sebevražda	k1gFnSc7	sebevražda
nestihli	stihnout	k5eNaPmAgMnP	stihnout
nic	nic	k3yNnSc1	nic
složit	složit	k5eAaPmF	složit
ani	ani	k8xC	ani
nahrát	nahrát	k5eAaPmF	nahrát
<g/>
.	.	kIx.	.
</s>
<s>
Stipe	Stipat	k5eAaPmIp3nS	Stipat
je	být	k5eAaImIp3nS	být
také	také	k9	také
kmotrem	kmotr	k1gMnSc7	kmotr
dcery	dcera	k1gFnSc2	dcera
Kurta	Kurt	k1gMnSc2	Kurt
Cobaina	Cobain	k1gMnSc2	Cobain
a	a	k8xC	a
Courtney	Courtnea	k1gMnSc2	Courtnea
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Frances	Frances	k1gMnSc1	Frances
Bean	Bean	k1gMnSc1	Bean
Cobain	Cobain	k1gMnSc1	Cobain
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Cobain	Cobain	k1gInSc1	Cobain
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
přehrávači	přehrávač	k1gInSc6	přehrávač
nalezeno	nalezen	k2eAgNnSc1d1	Nalezeno
album	album	k1gNnSc1	album
Automatic	Automatice	k1gFnPc2	Automatice
for	forum	k1gNnPc2	forum
the	the	k?	the
People	People	k1gFnSc2	People
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
zdrojem	zdroj	k1gInSc7	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
stala	stát	k5eAaPmAgFnS	stát
legendární	legendární	k2eAgFnSc1d1	legendární
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Patti	Patť	k1gFnSc2	Patť
Smith	Smitha	k1gFnPc2	Smitha
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Horses	Horsesa	k1gFnPc2	Horsesa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Patti	Patť	k1gFnSc3	Patť
Smith	Smith	k1gMnSc1	Smith
zpívá	zpívat	k5eAaImIp3nS	zpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
"	"	kIx"	"
<g/>
E-Bow	E-Bow	k1gMnSc1	E-Bow
the	the	k?	the
Letter	Letter	k1gMnSc1	Letter
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
lapela	lapela	k1gFnSc1	lapela
kapely	kapela	k1gFnSc2	kapela
má	mít	k5eAaImIp3nS	mít
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
také	také	k9	také
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Radiohead	Radiohead	k1gInSc1	Radiohead
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jim	on	k3xPp3gMnPc3	on
dělala	dělat	k5eAaImAgFnS	dělat
předkapelu	předkapela	k1gFnSc4	předkapela
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
Monster	monstrum	k1gNnPc2	monstrum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
turné	turné	k1gNnSc6	turné
kapely	kapela	k1gFnSc2	kapela
Radiohead	Radiohead	k1gInSc1	Radiohead
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zpíval	zpívat	k5eAaImAgMnS	zpívat
při	při	k7c6	při
některých	některý	k3yIgInPc6	některý
koncertech	koncert	k1gInPc6	koncert
Stipe	Stip	k1gInSc5	Stip
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Lucky	lucky	k6eAd1	lucky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Radiohead	Radiohead	k1gInSc4	Radiohead
<g/>
,	,	kIx,	,
Thom	Thom	k1gInSc4	Thom
Yorke	York	k1gFnSc2	York
<g/>
,	,	kIx,	,
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gInSc5	Stip
je	být	k5eAaImIp3nS	být
integrální	integrální	k2eAgFnSc7d1	integrální
součástí	součást	k1gFnSc7	součást
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
se	se	k3xPyFc4	se
s	s	k7c7	s
depresemi	deprese	k1gFnPc7	deprese
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
trpěl	trpět	k5eAaImAgMnS	trpět
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
také	také	k9	také
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xS	jako
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
hudebních	hudební	k2eAgInPc2d1	hudební
vzorů	vzor	k1gInPc2	vzor
a	a	k8xC	a
inspirací	inspirace	k1gFnPc2	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
a	a	k8xC	a
text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
How	How	k1gMnSc1	How
to	ten	k3xDgNnSc4	ten
Disappear	Disappear	k1gMnSc1	Disappear
Completely	Completela	k1gFnSc2	Completela
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dal	dát	k5eAaPmAgMnS	dát
Stipe	Stip	k1gInSc5	Stip
Yorkeovi	Yorkeův	k2eAgMnPc1d1	Yorkeův
<g/>
,	,	kIx,	,
když	když	k8xS	když
trpěl	trpět	k5eAaImAgMnS	trpět
depresí	deprese	k1gFnSc7	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
blízký	blízký	k2eAgInSc1d1	blízký
si	se	k3xPyFc3	se
byl	být	k5eAaImAgInS	být
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Natalií	Natalie	k1gFnPc2	Natalie
Merchant	Merchanta	k1gFnPc2	Merchanta
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
natočil	natočit	k5eAaBmAgMnS	natočit
i	i	k9	i
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Photograph	Photograph	k1gInSc1	Photograph
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
benefiční	benefiční	k2eAgNnSc4d1	benefiční
album	album	k1gNnSc4	album
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc1	ten
Choose	Choosa	k1gFnSc6	Choosa
propagující	propagující	k2eAgNnSc4d1	propagující
hnutí	hnutí	k1gNnSc4	hnutí
pro-choice	prohoice	k1gFnSc2	pro-choice
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
také	také	k9	také
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Gabrielem	Gabriel	k1gMnSc7	Gabriel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vydal	vydat	k5eAaPmAgMnS	vydat
kolekci	kolekce	k1gFnSc4	kolekce
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Two	Two	k1gMnSc1	Two
Times	Times	k1gMnSc1	Times
Intro	Intro	k1gNnSc4	Intro
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Road	Road	k1gMnSc1	Road
with	with	k1gMnSc1	with
Patti	Patť	k1gFnSc2	Patť
Smith	Smith	k1gMnSc1	Smith
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
také	také	k9	také
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Single	singl	k1gInSc5	singl
Cell	cello	k1gNnPc2	cello
a	a	k8xC	a
podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
produkci	produkce	k1gFnSc6	produkce
několika	několik	k4yIc2	několik
jejich	jejich	k3xOp3gInPc2	jejich
filmů	film	k1gInPc2	film
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Velvet	Velvet	k1gMnSc1	Velvet
Goldmine	Goldmin	k1gInSc5	Goldmin
<g/>
,	,	kIx,	,
Being	Being	k1gMnSc1	Being
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
a	a	k8xC	a
American	American	k1gMnSc1	American
Movie	Movie	k1gFnSc2	Movie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Americké	americký	k2eAgNnSc1d1	americké
psycho	psycha	k1gFnSc5	psycha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
Saved	Saved	k1gMnSc1	Saved
<g/>
!	!	kIx.	!
</s>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
knihu	kniha	k1gFnSc4	kniha
poezie	poezie	k1gFnSc2	poezie
The	The	k1gFnSc4	The
Haiku	Haika	k1gFnSc4	Haika
Year	Yeara	k1gFnPc2	Yeara
vydanou	vydaný	k2eAgFnSc4d1	vydaná
nakladatelstvím	nakladatelství	k1gNnPc3	nakladatelství
Soft	Soft	k?	Soft
Skull	Skullum	k1gNnPc2	Skullum
Press	Press	k1gInSc1	Press
napsal	napsat	k5eAaPmAgInS	napsat
jednu	jeden	k4xCgFnSc4	jeden
haiku	haika	k1gFnSc4	haika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgMnS	vydat
EP	EP	kA	EP
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
šest	šest	k4xCc1	šest
různých	různý	k2eAgFnPc2d1	různá
cover	covra	k1gFnPc2	covra
verzí	verze	k1gFnPc2	verze
písně	píseň	k1gFnSc2	píseň
Josepha	Josepha	k1gFnSc1	Josepha
Arthura	Arthura	k1gFnSc1	Arthura
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
humanitární	humanitární	k2eAgFnPc4d1	humanitární
organizace	organizace	k1gFnPc4	organizace
pomáhající	pomáhající	k2eAgFnPc4d1	pomáhající
obětem	oběť	k1gFnPc3	oběť
hurikánu	hurikán	k1gInSc3	hurikán
Katrina	Katrina	k1gMnSc1	Katrina
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Coldplay	Coldplaa	k1gFnSc2	Coldplaa
Chrisem	Chris	k1gInSc7	Chris
Martinem	Martin	k1gMnSc7	Martin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
kanadském	kanadský	k2eAgInSc6d1	kanadský
žebříčku	žebříček	k1gInSc6	žebříček
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
albu	album	k1gNnSc6	album
Meds	Medsa	k1gFnPc2	Medsa
kapely	kapela	k1gFnSc2	kapela
Placebo	placebo	k1gNnSc1	placebo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Brianem	Brian	k1gMnSc7	Brian
Molkem	Molek	k1gMnSc7	Molek
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Broken	Broken	k1gInSc4	Broken
Promise	Promise	k1gFnSc2	Promise
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
také	také	k9	také
zpíval	zpívat	k5eAaImAgMnS	zpívat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Hôtel	Hôtel	k1gMnSc1	Hôtel
<g/>
"	"	kIx"	"
na	na	k7c6	na
albu	album	k1gNnSc6	album
Monsieur	Monsieur	k1gMnSc1	Monsieur
Gainsbourg	Gainsbourg	k1gMnSc1	Gainsbourg
Revisited	Revisited	k1gMnSc1	Revisited
věnovaném	věnovaný	k2eAgInSc6d1	věnovaný
památce	památka	k1gFnSc3	památka
Serge	Serge	k1gFnSc1	Serge
Gainsbourga	Gainsbourga	k1gFnSc1	Gainsbourga
a	a	k8xC	a
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Dancing	dancing	k1gInSc1	dancing
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Lip	lípa	k1gFnPc2	lípa
of	of	k?	of
a	a	k8xC	a
Volcano	Volcana	k1gFnSc5	Volcana
<g/>
"	"	kIx"	"
na	na	k7c6	na
albu	album	k1gNnSc6	album
One	One	k1gFnSc2	One
Day	Day	k1gMnSc1	Day
It	It	k1gMnSc1	It
Will	Will	k1gMnSc1	Will
Please	Pleasa	k1gFnSc6	Pleasa
Us	Us	k1gFnSc2	Us
to	ten	k3xDgNnSc4	ten
Remember	Remember	k1gMnSc1	Remember
Even	Even	k1gMnSc1	Even
This	This	k1gInSc4	This
kapely	kapela	k1gFnSc2	kapela
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Dolls	Dollsa	k1gFnPc2	Dollsa
<g/>
.	.	kIx.	.
</s>
