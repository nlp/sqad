<s>
Concord	Concord	k6eAd1	Concord
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
a	a	k8xC	a
též	též	k9	též
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
kraje	kraj	k1gInSc2	kraj
Merrimack	Merrimacka	k1gFnPc2	Merrimacka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
řece	řeka	k1gFnSc6	řeka
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
mělo	mít	k5eAaImAgNnS	mít
40	[number]	k4	40
765	[number]	k4	765
a	a	k8xC	a
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
přes	přes	k7c4	přes
42	[number]	k4	42
392	[number]	k4	392
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
vesnic	vesnice	k1gFnPc2	vesnice
Penacook	Penacook	k1gInSc4	Penacook
<g/>
,	,	kIx,	,
East	East	k2eAgInSc4d1	East
Concord	Concord	k1gInSc4	Concord
a	a	k8xC	a
West	West	k2eAgInSc4d1	West
Concord	Concord	k1gInSc4	Concord
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
Concord	Concord	k1gMnSc1	Concord
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
dávnověku	dávnověk	k1gInSc2	dávnověk
obýváno	obýván	k2eAgNnSc1d1	obýváno
indiánským	indiánský	k2eAgInSc7d1	indiánský
kmenem	kmen	k1gInSc7	kmen
Penacook	Penacook	k1gInSc1	Penacook
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
u	u	k7c2	u
kaskádovitých	kaskádovitý	k2eAgInPc2d1	kaskádovitý
vodopádů	vodopád	k1gInPc2	vodopád
Merrimacku	Merrimack	k1gInSc2	Merrimack
lovil	lovit	k5eAaImAgMnS	lovit
migrující	migrující	k2eAgMnPc4d1	migrující
lososy	losos	k1gMnPc4	losos
a	a	k8xC	a
jesetery	jeseter	k1gMnPc4	jeseter
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
častými	častý	k2eAgFnPc7d1	častá
záplavami	záplava	k1gFnPc7	záplava
tvořila	tvořit	k5eAaImAgFnS	tvořit
v	v	k7c6	v
přilehlých	přilehlý	k2eAgNnPc6d1	přilehlé
údolích	údolí	k1gNnPc6	údolí
půdu	půda	k1gFnSc4	půda
obzvláště	obzvláště	k6eAd1	obzvláště
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
fazolí	fazole	k1gFnPc2	fazole
<g/>
,	,	kIx,	,
dýní	dýně	k1gFnPc2	dýně
<g/>
,	,	kIx,	,
melounů	meloun	k1gInPc2	meloun
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1725	[number]	k4	1725
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
provincie	provincie	k1gFnSc2	provincie
Massatchusettského	Massatchusettský	k2eAgInSc2d1	Massatchusettský
zálivu	záliv	k1gInSc2	záliv
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
plantáž	plantáž	k1gFnSc1	plantáž
Penacook	Penacook	k1gInSc1	Penacook
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
vybudována	vybudován	k2eAgFnSc1d1	vybudována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
osady	osada	k1gFnPc1	osada
Rumford	Rumfordo	k1gNnPc2	Rumfordo
a	a	k8xC	a
Bow	Bow	k1gFnPc2	Bow
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
získaly	získat	k5eAaPmAgInP	získat
statut	statut	k1gInSc4	statut
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
v	v	k7c6	v
Concordu	Concord	k1gInSc6	Concord
<g/>
,	,	kIx,	,
Lexingtonu	Lexington	k1gInSc6	Lexington
a	a	k8xC	a
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
probíhaly	probíhat	k5eAaImAgFnP	probíhat
půtky	půtka	k1gFnPc1	půtka
a	a	k8xC	a
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
zastánci	zastánce	k1gMnPc7	zastánce
nezávislosti	nezávislost	k1gFnSc2	nezávislost
původních	původní	k2eAgFnPc2d1	původní
13	[number]	k4	13
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
vojskem	vojsko	k1gNnSc7	vojsko
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
Samuel	Samuel	k1gMnSc1	Samuel
Bloget	Bloget	k1gMnSc1	Bloget
postavil	postavit	k5eAaPmAgMnS	postavit
Middlesexský	Middlesexský	k2eAgInSc4d1	Middlesexský
kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
město	město	k1gNnSc4	město
spojoval	spojovat	k5eAaImAgMnS	spojovat
s	s	k7c7	s
Bostonem	Boston	k1gInSc7	Boston
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
výhodné	výhodný	k2eAgFnSc3d1	výhodná
centrální	centrální	k2eAgFnSc3d1	centrální
poloze	poloha	k1gFnSc3	poloha
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc2	New
Hampshire	Hampshir	k1gInSc5	Hampshir
stal	stát	k5eAaPmAgMnS	stát
Concord	Concord	k1gMnSc1	Concord
jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Concorde	Concord	k1gInSc5	Concord
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stala	stát	k5eAaPmAgFnS	stát
důležitá	důležitý	k2eAgFnSc1d1	důležitá
železniční	železniční	k2eAgFnSc1d1	železniční
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
též	též	k9	též
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
textil	textil	k1gInSc4	textil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Concord	Concord	k1gInSc4	Concord
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
několik	několik	k4yIc4	několik
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
a	a	k8xC	a
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
centrem	centrum	k1gNnSc7	centrum
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
severovýchodního	severovýchodní	k2eAgNnSc2d1	severovýchodní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
též	též	k9	též
Concord	Concord	k1gInSc1	Concord
Litho	Lit	k1gMnSc2	Lit
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
tiskáren	tiskárna	k1gFnPc2	tiskárna
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Concord	Concord	k6eAd1	Concord
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
43,2	[number]	k4	43,2
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
71,5	[number]	k4	71,5
poledníku	poledník	k1gInSc2	poledník
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
spádové	spádový	k2eAgFnSc6d1	spádová
oblasti	oblast	k1gFnSc6	oblast
řeky	řeka	k1gFnSc2	řeka
Merrimack	Merrimack	k1gInSc1	Merrimack
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgNnPc7d3	nejbližší
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Bow	Bow	k1gMnPc1	Bow
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Webster	Webster	k1gInSc1	Webster
na	na	k7c6	na
severu	sever	k1gInSc6	sever
či	či	k8xC	či
Hopkinton	Hopkinton	k1gInSc1	Hopkinton
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
blízkost	blízkost	k1gFnSc4	blízkost
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
nuly	nula	k1gFnSc2	nula
nebo	nebo	k8xC	nebo
poklesnou	poklesnout	k5eAaPmIp3nP	poklesnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
stupňů	stupeň	k1gInPc2	stupeň
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
vyšplhají	vyšplhat	k5eAaPmIp3nP	vyšplhat
typicky	typicky	k6eAd1	typicky
na	na	k7c4	na
20-30	[number]	k4	20-30
°	°	k?	°
<g/>
C.	C.	kA	C.
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
s	s	k7c7	s
minimálními	minimální	k2eAgInPc7d1	minimální
výkyvy	výkyv	k1gInPc7	výkyv
<g/>
,	,	kIx,	,
a	a	k8xC	a
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
75-85	[number]	k4	75-85
mm	mm	kA	mm
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
42	[number]	k4	42
695	[number]	k4	695
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
91,8	[number]	k4	91,8
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,4	[number]	k4	3,4
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
2,1	[number]	k4	2,1
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Věkové	věkový	k2eAgNnSc1d1	věkové
složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
:	:	kIx,	:
23,1	[number]	k4	23,1
%	%	kIx~	%
pod	pod	k7c4	pod
18	[number]	k4	18
let	léto	k1gNnPc2	léto
8,3	[number]	k4	8,3
%	%	kIx~	%
18-24	[number]	k4	18-24
let	léto	k1gNnPc2	léto
33	[number]	k4	33
%	%	kIx~	%
25-44	[number]	k4	25-44
let	léto	k1gNnPc2	léto
22	[number]	k4	22
%	%	kIx~	%
45-64	[number]	k4	45-64
let	léto	k1gNnPc2	léto
13,7	[number]	k4	13,7
%	%	kIx~	%
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
činí	činit	k5eAaImIp3nS	činit
42	[number]	k4	42
447	[number]	k4	447
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
a	a	k8xC	a
52	[number]	k4	52
418	[number]	k4	418
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
procent	procento	k1gNnPc2	procento
nad	nad	k7c4	nad
celostátní	celostátní	k2eAgInSc4d1	celostátní
průměr	průměr	k1gInSc4	průměr
<g/>
.	.	kIx.	.
44,3	[number]	k4	44,3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
žije	žít	k5eAaImIp3nS	žít
ženatých	ženatý	k2eAgInPc2d1	ženatý
nebo	nebo	k8xC	nebo
vdaných	vdaný	k2eAgInPc2d1	vdaný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
vychází	vycházet	k5eAaImIp3nS	vycházet
deník	deník	k1gInSc1	deník
The	The	k1gFnSc2	The
Concord	Concorda	k1gFnPc2	Concorda
Monitor	monitor	k1gInSc1	monitor
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
týdenní	týdenní	k2eAgInSc1d1	týdenní
výběr	výběr	k1gInSc1	výběr
The	The	k1gFnSc2	The
Weekly	Weekl	k1gInPc7	Weekl
Insider	insider	k1gMnSc1	insider
či	či	k8xC	či
týdeník	týdeník	k1gInSc1	týdeník
The	The	k1gFnSc2	The
Hippo	Hippa	k1gFnSc5	Hippa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
funguje	fungovat	k5eAaImIp3nS	fungovat
jedna	jeden	k4xCgFnSc1	jeden
rádiová	rádiový	k2eAgFnSc1d1	rádiová
stanice	stanice	k1gFnSc1	stanice
ve	v	k7c6	v
vlnách	vlna	k1gFnPc6	vlna
AM	AM	kA	AM
a	a	k8xC	a
pět	pět	k4xCc4	pět
ve	v	k7c6	v
vlnách	vlna	k1gFnPc6	vlna
FM	FM	kA	FM
<g/>
/	/	kIx~	/
<g/>
VKV	VKV	kA	VKV
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgFnPc4d1	dostupná
veřejné	veřejný	k2eAgFnPc4d1	veřejná
New	New	k1gFnPc4	New
Hampshire	Hampshir	k1gInSc5	Hampshir
Public	publicum	k1gNnPc2	publicum
Radio	radio	k1gNnSc5	radio
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
zde	zde	k6eAd1	zde
vysílá	vysílat	k5eAaImIp3nS	vysílat
komerční	komerční	k2eAgFnSc1d1	komerční
WPXG-TV	WPXG-TV	k1gFnSc1	WPXG-TV
a	a	k8xC	a
veřejná	veřejný	k2eAgFnSc1d1	veřejná
Concord	Concord	k1gInSc1	Concord
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
State	status	k1gInSc5	status
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
zbudovaný	zbudovaný	k2eAgMnSc1d1	zbudovaný
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1815-1818	[number]	k4	1815-1818
Eagle	Eagle	k1gNnSc2	Eagle
Hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
<g/>
)	)	kIx)	)
Phenix	Phenix	k1gInSc1	Phenix
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
Phenix	Phenix	k1gInSc1	Phenix
Hall	Hall	k1gMnSc1	Hall
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Walker-Woodman	Walker-Woodman	k1gMnSc1	Walker-Woodman
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
dům	dům	k1gInSc1	dům
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
zbudovaný	zbudovaný	k2eAgMnSc1d1	zbudovaný
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1733-1735	[number]	k4	1733-1735
New	New	k1gFnSc2	New
Hampshire	Hampshir	k1gInSc5	Hampshir
Historical	Historical	k1gMnSc1	Historical
Society	societa	k1gFnSc2	societa
<g />
.	.	kIx.	.
</s>
<s>
Christa	Christa	k1gMnSc1	Christa
McAuliffe	McAuliff	k1gInSc5	McAuliff
Planetarium	Planetarium	k1gNnSc1	Planetarium
Franklin	Franklin	k2eAgMnSc1d1	Franklin
Pierce	Pierce	k1gMnSc1	Pierce
Law	Law	k1gFnSc2	Law
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
Technical	Technical	k1gMnSc5	Technical
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
Hesser	Hesser	k1gInSc4	Hesser
Collage	Collag	k1gInSc2	Collag
Franklin	Franklin	k1gInSc1	Franklin
Pierce	Pierec	k1gInPc1	Pierec
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
-	-	kIx~	-
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Tony	Tony	k1gMnSc1	Tony
Conrad	Conrad	k1gInSc1	Conrad
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
-	-	kIx~	-
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgMnSc1d1	experimentální
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
