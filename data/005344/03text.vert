<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Budweis	Budweis	k1gFnSc1	Budweis
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
Böhmisch	Böhmisch	k1gMnSc1	Böhmisch
Budweis	Budweis	k1gFnSc2	Budweis
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
a	a	k8xC	a
správní	správní	k2eAgFnSc1d1	správní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
metropole	metropole	k1gFnSc1	metropole
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Českobudějovické	českobudějovický	k2eAgFnSc6d1	českobudějovická
pánvi	pánev	k1gFnSc6	pánev
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Vltava	Vltava	k1gFnSc1	Vltava
a	a	k8xC	a
Malše	Malše	k1gFnSc1	Malše
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
přes	přes	k7c4	přes
93	[number]	k4	93
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
řada	řada	k1gFnSc1	řada
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
a	a	k8xC	a
blízkost	blízkost	k1gFnSc4	blízkost
dalších	další	k2eAgNnPc2d1	další
historicky	historicky	k6eAd1	historicky
cenných	cenný	k2eAgNnPc2d1	cenné
míst	místo	k1gNnPc2	místo
(	(	kIx(	(
<g/>
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Třeboň	Třeboň	k1gFnSc1	Třeboň
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
staré	starý	k2eAgFnSc2d1	stará
osady	osada	k1gFnSc2	osada
Budivojovice	Budivojovice	k1gFnSc2	Budivojovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
věků	věk	k1gInPc2	věk
výslovnost	výslovnost	k1gFnSc1	výslovnost
a	a	k8xC	a
transkripce	transkripce	k1gFnSc1	transkripce
jména	jméno	k1gNnSc2	jméno
mírně	mírně	k6eAd1	mírně
kolísala	kolísat	k5eAaImAgFnS	kolísat
(	(	kIx(	(
<g/>
v	v	k7c6	v
jihočeském	jihočeský	k2eAgNnSc6d1	Jihočeské
nářečí	nářečí	k1gNnSc6	nářečí
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Budějce	Budějce	k1gMnSc1	Budějce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přídomek	přídomek	k1gInSc1	přídomek
České	český	k2eAgFnSc2d1	Česká
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
stejnou	stejný	k2eAgFnSc7d1	stejná
měrou	míra	k1gFnSc7wR	míra
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
i	i	k8xC	i
němčině	němčina	k1gFnSc6	němčina
(	(	kIx(	(
<g/>
Böhmisch	Böhmisch	k1gInSc1	Böhmisch
Budweis	Budweis	k1gFnSc2	Budweis
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
čistě	čistě	k6eAd1	čistě
územním	územní	k2eAgInSc6d1	územní
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kulminovalo	kulminovat	k5eAaImAgNnS	kulminovat
národnostní	národnostní	k2eAgNnSc1d1	národnostní
pnutí	pnutí	k1gNnSc1	pnutí
a	a	k8xC	a
název	název	k1gInSc1	název
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
brán	brát	k5eAaImNgInS	brát
i	i	k9	i
z	z	k7c2	z
národnostního	národnostní	k2eAgInSc2d1	národnostní
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
diferenciaci	diferenciace	k1gFnSc3	diferenciace
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
čeština	čeština	k1gFnSc1	čeština
dále	daleko	k6eAd2	daleko
používala	používat	k5eAaImAgFnS	používat
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
,	,	kIx,	,
u	u	k7c2	u
Němců	Němec	k1gMnPc2	Němec
převážilo	převážit	k5eAaPmAgNnS	převážit
jméno	jméno	k1gNnSc4	jméno
bez	bez	k7c2	bez
přídomku	přídomek	k1gInSc2	přídomek
(	(	kIx(	(
<g/>
záměna	záměna	k1gFnSc1	záměna
nehrozila	hrozit	k5eNaImAgFnS	hrozit
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
jsou	být	k5eAaImIp3nP	být
německy	německy	k6eAd1	německy
Budwitz	Budwitz	k1gInSc4	Budwitz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
už	už	k6eAd1	už
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
československé	československý	k2eAgInPc4d1	československý
úřady	úřad	k1gInPc4	úřad
za	za	k7c4	za
oficiální	oficiální	k2eAgNnSc4d1	oficiální
jméno	jméno	k1gNnSc4	jméno
města	město	k1gNnSc2	město
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
Böhmisch	Böhmisch	k1gInSc1	Böhmisch
Budweis	Budweis	k1gFnSc2	Budweis
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
bylo	být	k5eAaImAgNnS	být
oficiální	oficiální	k2eAgNnSc4d1	oficiální
jméno	jméno	k1gNnSc4	jméno
naopak	naopak	k6eAd1	naopak
pouze	pouze	k6eAd1	pouze
Budweis	Budweis	k1gFnPc2	Budweis
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
Budějovice	Budějovice	k1gInPc1	Budějovice
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Budvicium	Budvicium	k1gNnSc4	Budvicium
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
nechal	nechat	k5eAaPmAgMnS	nechat
založit	založit	k5eAaPmF	založit
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1265	[number]	k4	1265
<g/>
;	;	kIx,	;
lokaci	lokace	k1gFnSc6	lokace
a	a	k8xC	a
projekci	projekce	k1gFnSc6	projekce
města	město	k1gNnSc2	město
provedl	provést	k5eAaPmAgMnS	provést
králův	králův	k2eAgMnSc1d1	králův
rytíř	rytíř	k1gMnSc1	rytíř
Hirzo	Hirza	k1gFnSc5	Hirza
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
představovat	představovat	k5eAaImF	představovat
doposud	doposud	k6eAd1	doposud
chybějící	chybějící	k2eAgFnSc4d1	chybějící
základnu	základna	k1gFnSc4	základna
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
být	být	k5eAaImF	být
protiváhou	protiváha	k1gFnSc7	protiváha
moci	moc	k1gFnSc2	moc
Vítkovců	Vítkovec	k1gMnPc2	Vítkovec
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Rožmberků	Rožmberk	k1gInPc2	Rožmberk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
účel	účel	k1gInSc1	účel
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
zdatně	zdatně	k6eAd1	zdatně
plnilo	plnit	k5eAaImAgNnS	plnit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
několikasetletého	několikasetletý	k2eAgNnSc2d1	několikasetleté
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
"	"	kIx"	"
<g/>
lokálními	lokální	k2eAgFnPc7d1	lokální
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
potlačil	potlačit	k5eAaPmAgMnS	potlačit
společný	společný	k2eAgMnSc1d1	společný
mocný	mocný	k2eAgMnSc1d1	mocný
nepřítel	nepřítel	k1gMnSc1	nepřítel
–	–	k?	–
husité	husita	k1gMnPc1	husita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
i	i	k9	i
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
Budějovice	Budějovice	k1gInPc4	Budějovice
upadaly	upadat	k5eAaImAgFnP	upadat
kvůli	kvůli	k7c3	kvůli
neuspořádaným	uspořádaný	k2eNgInPc3d1	neuspořádaný
poměrům	poměr	k1gInPc3	poměr
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
odklonily	odklonit	k5eAaPmAgFnP	odklonit
obchodní	obchodní	k2eAgFnPc4d1	obchodní
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
rozkvětu	rozkvět	k1gInSc6	rozkvět
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
těžbě	těžba	k1gFnSc3	těžba
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
příjmům	příjem	k1gInPc3	příjem
z	z	k7c2	z
vaření	vaření	k1gNnSc2	vaření
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
suknem	sukno	k1gNnSc7	sukno
či	či	k8xC	či
rybníkářství	rybníkářství	k1gNnSc1	rybníkářství
<g/>
)	)	kIx)	)
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
klidném	klidný	k2eAgInSc6d1	klidný
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
Budějovice	Budějovice	k1gInPc4	Budějovice
opět	opět	k6eAd1	opět
čelily	čelit	k5eAaImAgFnP	čelit
těžkým	těžký	k2eAgInPc3d1	těžký
časům	čas	k1gInPc3	čas
<g/>
,	,	kIx,	,
za	za	k7c2	za
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
stály	stát	k5eAaImAgFnP	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
přečkaly	přečkat	k5eAaPmAgInP	přečkat
několikeré	několikerý	k4xRyIgNnSc4	několikerý
obležení	obležení	k1gNnSc4	obležení
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
celkově	celkově	k6eAd1	celkově
neradostnou	radostný	k2eNgFnSc4d1	neradostná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
Budějovičtí	budějovický	k2eAgMnPc1d1	budějovický
jí	on	k3xPp3gFnSc3	on
využili	využít	k5eAaPmAgMnP	využít
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
konkurenčního	konkurenční	k2eAgInSc2d1	konkurenční
Rudolfova	Rudolfův	k2eAgInSc2d1	Rudolfův
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
bojům	boj	k1gInPc3	boj
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
staly	stát	k5eAaPmAgInP	stát
Budějovice	Budějovice	k1gInPc1	Budějovice
dočasně	dočasně	k6eAd1	dočasně
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
přesunuly	přesunout	k5eAaPmAgInP	přesunout
některé	některý	k3yIgInPc1	některý
důležité	důležitý	k2eAgInPc1d1	důležitý
úřady	úřad	k1gInPc1	úřad
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1641	[number]	k4	1641
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
požár	požár	k1gInSc4	požár
a	a	k8xC	a
popelem	popel	k1gInSc7	popel
lehly	lehnout	k5eAaPmAgFnP	lehnout
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Budějovice	Budějovice	k1gInPc1	Budějovice
zažily	zažít	k5eAaPmAgInP	zažít
okupaci	okupace	k1gFnSc4	okupace
vojsky	vojsky	k6eAd1	vojsky
bavorského	bavorský	k2eAgMnSc2d1	bavorský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Karla	Karel	k1gMnSc2	Karel
Albrechta	Albrecht	k1gMnSc2	Albrecht
během	během	k7c2	během
první	první	k4xOgFnSc2	první
slezské	slezský	k2eAgFnSc2d1	Slezská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
boje	boj	k1gInSc2	boj
mezi	mezi	k7c7	mezi
habsburskými	habsburský	k2eAgNnPc7d1	habsburské
vojsky	vojsko	k1gNnPc7	vojsko
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
armádou	armáda	k1gFnSc7	armáda
mezi	mezi	k7c7	mezi
Budějovicemi	Budějovice	k1gInPc7	Budějovice
a	a	k8xC	a
Hlubokou	Hluboká	k1gFnSc7	Hluboká
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1742	[number]	k4	1742
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
slezské	slezský	k2eAgFnSc2d1	Slezská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Budějovic	Budějovice	k1gInPc2	Budějovice
sváděly	svádět	k5eAaImAgFnP	svádět
boje	boj	k1gInPc4	boj
rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
pruská	pruský	k2eAgFnSc1d1	pruská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
přitom	přitom	k6eAd1	přitom
bylo	být	k5eAaImAgNnS	být
dočasně	dočasně	k6eAd1	dočasně
Prusy	Prus	k1gMnPc7	Prus
obsazeno	obsazen	k2eAgNnSc1d1	obsazeno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
boje	boj	k1gInPc1	boj
vyhýbaly	vyhýbat	k5eAaImAgInP	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nabylo	nabýt	k5eAaPmAgNnS	nabýt
na	na	k7c6	na
významu	význam	k1gInSc6	význam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
biskupa	biskup	k1gInSc2	biskup
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
diecéze	diecéze	k1gFnPc1	diecéze
českobudějovické	českobudějovický	k2eAgFnPc1d1	českobudějovická
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
výrazný	výrazný	k2eAgInSc1d1	výrazný
vzestup	vzestup	k1gInSc1	vzestup
Budějovic	Budějovice	k1gInPc2	Budějovice
nastal	nastat	k5eAaPmAgInS	nastat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
sem	sem	k6eAd1	sem
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
správa	správa	k1gFnSc1	správa
kraje	kraj	k1gInSc2	kraj
z	z	k7c2	z
Písku	Písek	k1gInSc2	Písek
a	a	k8xC	a
Tábora	Tábor	k1gInSc2	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
málo	málo	k6eAd1	málo
významného	významný	k2eAgNnSc2d1	významné
devítitisícového	devítitisícový	k2eAgNnSc2d1	devítitisícové
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
a	a	k8xC	a
železnici	železnice	k1gFnSc3	železnice
stalo	stát	k5eAaPmAgNnS	stát
důležité	důležitý	k2eAgNnSc1d1	důležité
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Ustavení	ustavení	k1gNnSc1	ustavení
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
obešlo	obejít	k5eAaPmAgNnS	obejít
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
(	(	kIx(	(
<g/>
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
krom	krom	k7c2	krom
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legií	legie	k1gFnPc2	legie
i	i	k8xC	i
ustavená	ustavený	k2eAgFnSc1d1	ustavená
italská	italský	k2eAgFnSc1d1	italská
posádka	posádka	k1gFnSc1	posádka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právě	právě	k9	právě
přes	přes	k7c4	přes
Budějovice	Budějovice	k1gInPc4	Budějovice
se	se	k3xPyFc4	se
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgMnS	být
Masaryk	Masaryk	k1gMnSc1	Masaryk
již	již	k6eAd1	již
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
)	)	kIx)	)
1918	[number]	k4	1918
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
také	také	k9	také
dokládá	dokládat	k5eAaImIp3nS	dokládat
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
železničním	železniční	k2eAgInSc6d1	železniční
vagóně	vagón	k1gInSc6	vagón
jeho	jeho	k3xOp3gInSc2	jeho
vlaku	vlak	k1gInSc2	vlak
z	z	k7c2	z
pátku	pátek	k1gInSc2	pátek
na	na	k7c4	na
sobotu	sobota	k1gFnSc4	sobota
přenocoval	přenocovat	k5eAaPmAgMnS	přenocovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
(	(	kIx(	(
<g/>
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
na	na	k7c6	na
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nově	nov	k1gInSc6	nov
pojmenované	pojmenovaný	k2eAgNnSc4d1	pojmenované
centrální	centrální	k2eAgNnSc4d1	centrální
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
oficiální	oficiální	k2eAgNnSc1d1	oficiální
přivítání	přivítání	k1gNnSc1	přivítání
s	s	k7c7	s
projevy	projev	k1gInPc7	projev
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
14	[number]	k4	14
nablýskaných	nablýskaný	k2eAgMnPc2d1	nablýskaný
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
nové	nový	k2eAgFnSc2d1	nová
a	a	k8xC	a
demokratické	demokratický	k2eAgFnSc2d1	demokratická
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
také	také	k9	také
doručen	doručit	k5eAaPmNgInS	doručit
starostou	starosta	k1gMnSc7	starosta
Zátkou	zátka	k1gFnSc7	zátka
pozdravný	pozdravný	k2eAgInSc4d1	pozdravný
telegram	telegram	k1gInSc4	telegram
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Karla	Karel	k1gMnSc4	Karel
Kramáře	kramář	k1gMnSc4	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
také	také	k6eAd1	také
ve	v	k7c6	v
městě	město	k1gNnSc6	město
získalo	získat	k5eAaPmAgNnS	získat
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
slovo	slovo	k1gNnSc1	slovo
české	český	k2eAgNnSc1d1	české
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
stav	stav	k1gInSc1	stav
trval	trvat	k5eAaImAgInS	trvat
až	až	k9	až
do	do	k7c2	do
okupace	okupace	k1gFnSc2	okupace
města	město	k1gNnSc2	město
jednotkami	jednotka	k1gFnPc7	jednotka
wehrmachtu	wehrmacht	k1gInSc3	wehrmacht
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Okupační	okupační	k2eAgFnSc1d1	okupační
správa	správa	k1gFnSc1	správa
poté	poté	k6eAd1	poté
rychle	rychle	k6eAd1	rychle
zlikvidovala	zlikvidovat	k5eAaPmAgFnS	zlikvidovat
českou	český	k2eAgFnSc4d1	Česká
obecní	obecní	k2eAgFnSc4d1	obecní
samosprávu	samospráva	k1gFnSc4	samospráva
(	(	kIx(	(
<g/>
budějovické	budějovický	k2eAgNnSc1d1	Budějovické
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
muselo	muset	k5eAaImAgNnS	muset
ukončit	ukončit	k5eAaPmF	ukončit
činnost	činnost	k1gFnSc4	činnost
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholné	vrcholný	k2eAgInPc4d1	vrcholný
posty	post	k1gInPc4	post
na	na	k7c6	na
městských	městský	k2eAgInPc6d1	městský
úřadech	úřad	k1gInPc6	úřad
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Budějovice	Budějovice	k1gInPc1	Budějovice
dvakrát	dvakrát	k6eAd1	dvakrát
staly	stát	k5eAaPmAgInP	stát
cílem	cíl	k1gInSc7	cíl
náletů	nálet	k1gInPc2	nálet
amerického	americký	k2eAgNnSc2d1	americké
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
značně	značně	k6eAd1	značně
poškodily	poškodit	k5eAaPmAgInP	poškodit
město	město	k1gNnSc4	město
a	a	k8xC	a
způsobily	způsobit	k5eAaPmAgFnP	způsobit
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
německá	německý	k2eAgFnSc1d1	německá
posádka	posádka	k1gFnSc1	posádka
město	město	k1gNnSc1	město
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
vyklidila	vyklidit	k5eAaPmAgFnS	vyklidit
a	a	k8xC	a
přenechala	přenechat	k5eAaPmAgFnS	přenechat
je	být	k5eAaImIp3nS	být
sovětským	sovětský	k2eAgFnPc3d1	sovětská
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
ležely	ležet	k5eAaImAgInP	ležet
na	na	k7c6	na
demarkační	demarkační	k2eAgFnSc6d1	demarkační
čáře	čára	k1gFnSc6	čára
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
centrum	centrum	k1gNnSc1	centrum
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
leželo	ležet	k5eAaImAgNnS	ležet
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jednotky	jednotka	k1gFnPc1	jednotka
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
kontrolovaly	kontrolovat	k5eAaImAgFnP	kontrolovat
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oblasti	oblast	k1gFnPc1	oblast
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
průběhu	průběh	k1gInSc3	průběh
demarkační	demarkační	k2eAgFnSc2d1	demarkační
čáry	čára	k1gFnSc2	čára
nemohla	moct	k5eNaImAgNnP	moct
americká	americký	k2eAgNnPc1d1	americké
vojska	vojsko	k1gNnPc1	vojsko
do	do	k7c2	do
města	město	k1gNnSc2	město
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Předsunuté	předsunutý	k2eAgFnPc1d1	předsunutá
jednotky	jednotka	k1gFnPc1	jednotka
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
dojely	dojet	k5eAaPmAgFnP	dojet
k	k	k7c3	k
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
Dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
mostu	most	k1gInSc3	most
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
otočily	otočit	k5eAaPmAgFnP	otočit
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
86	[number]	k4	86
<g/>
.	.	kIx.	.
gardovou	gardový	k2eAgFnSc4d1	gardová
střeleckou	střelecký	k2eAgFnSc4d1	střelecká
divizi	divize	k1gFnSc4	divize
generála	generál	k1gMnSc2	generál
Vasila	Vasil	k1gMnSc2	Vasil
Danoviče	Danovič	k1gMnSc2	Danovič
Sokolovského	sokolovský	k2eAgMnSc2d1	sokolovský
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
2	[number]	k4	2
<g/>
.	.	kIx.	.
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
frontu	front	k1gInSc2	front
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
maršála	maršál	k1gMnSc2	maršál
Rodiona	Rodion	k1gMnSc2	Rodion
Jakovleviče	Jakovlevič	k1gMnSc2	Jakovlevič
Malinovského	Malinovský	k2eAgMnSc2d1	Malinovský
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
jednotky	jednotka	k1gFnPc1	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
setkaly	setkat	k5eAaPmAgFnP	setkat
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c6	na
společných	společný	k2eAgFnPc6d1	společná
oslavách	oslava	k1gFnPc6	oslava
osvobození	osvobození	k1gNnSc2	osvobození
města	město	k1gNnSc2	město
a	a	k8xC	a
konce	konec	k1gInSc2	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
vysídlení	vysídlení	k1gNnSc1	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
postihlo	postihnout	k5eAaPmAgNnS	postihnout
asi	asi	k9	asi
7500	[number]	k4	7500
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
16	[number]	k4	16
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
se	se	k3xPyFc4	se
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
staly	stát	k5eAaPmAgFnP	stát
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
nově	nově	k6eAd1	nově
zřízeného	zřízený	k2eAgInSc2d1	zřízený
Českobudějovického	českobudějovický	k2eAgInSc2d1	českobudějovický
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
správní	správní	k2eAgFnSc6d1	správní
reformě	reforma	k1gFnSc6	reforma
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
centrem	centrum	k1gNnSc7	centrum
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nového	nový	k2eAgInSc2d1	nový
Budějovického	budějovický	k2eAgInSc2d1	budějovický
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2001	[number]	k4	2001
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
Jihočeský	jihočeský	k2eAgInSc4d1	jihočeský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
postihly	postihnout	k5eAaPmAgFnP	postihnout
Budějovicko	Budějovicko	k1gNnSc1	Budějovicko
mohutné	mohutný	k2eAgFnPc1d1	mohutná
povodně	povodeň	k1gFnPc1	povodeň
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
Malše	Malše	k1gFnSc1	Malše
a	a	k8xC	a
Vltava	Vltava	k1gFnSc1	Vltava
zaplavily	zaplavit	k5eAaPmAgInP	zaplavit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
včetně	včetně	k7c2	včetně
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
způsobily	způsobit	k5eAaPmAgFnP	způsobit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Malše	Malš	k1gInSc2	Malš
a	a	k8xC	a
Vltava	Vltava	k1gFnSc1	Vltava
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
severně	severně	k6eAd1	severně
a	a	k8xC	a
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
rybníky	rybník	k1gInPc4	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
rybníky	rybník	k1gInPc7	rybník
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
Starohaklovský	Starohaklovský	k2eAgInSc4d1	Starohaklovský
rybník	rybník	k1gInSc4	rybník
(	(	kIx(	(
<g/>
44	[number]	k4	44
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Černiš	Černiš	k1gMnSc1	Černiš
(	(	kIx(	(
<g/>
42	[number]	k4	42
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Novohaklovský	Novohaklovský	k2eAgInSc1d1	Novohaklovský
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
41	[number]	k4	41
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
vrbenský	vrbenský	k2eAgInSc1d1	vrbenský
zadní	zadní	k2eAgInSc1d1	zadní
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
22	[number]	k4	22
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
houženský	houženský	k2eAgInSc1d1	houženský
rybník	rybník	k1gInSc1	rybník
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
21	[number]	k4	21
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Domin	domino	k1gNnPc2	domino
(	(	kIx(	(
<g/>
17	[number]	k4	17
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čertík	čertík	k1gMnSc1	čertík
(	(	kIx(	(
<g/>
15	[number]	k4	15
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
vrbenský	vrbenský	k2eAgInSc1d1	vrbenský
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
13	[number]	k4	13
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
vrbenský	vrbenský	k2eAgInSc4d1	vrbenský
přední	přední	k2eAgInSc4d1	přední
rybník	rybník	k1gInSc4	rybník
(	(	kIx(	(
<g/>
13	[number]	k4	13
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bor	bor	k1gInSc1	bor
(	(	kIx(	(
<g/>
10	[number]	k4	10
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dubský	dubský	k2eAgInSc1d1	dubský
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
10	[number]	k4	10
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
směrů	směr	k1gInPc2	směr
je	být	k5eAaImIp3nS	být
Českobudějovická	českobudějovický	k2eAgFnSc1d1	českobudějovická
pánev	pánev	k1gFnSc1	pánev
v	v	k7c6	v
relativní	relativní	k2eAgFnSc6d1	relativní
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
zřetelně	zřetelně	k6eAd1	zřetelně
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
terénními	terénní	k2eAgFnPc7d1	terénní
vyvýšeninami	vyvýšenina	k1gFnPc7	vyvýšenina
<g/>
:	:	kIx,	:
Lišovský	Lišovský	k2eAgInSc1d1	Lišovský
práh	práh	k1gInSc1	práh
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Třeboňské	třeboňský	k2eAgFnSc2d1	Třeboňská
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
podhůří	podhůří	k1gNnSc2	podhůří
Novohradských	novohradský	k2eAgFnPc2d1	Novohradská
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
západě	západ	k1gInSc6	západ
pak	pak	k6eAd1	pak
podhůří	podhůří	k1gNnSc1	podhůří
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Blanský	blanský	k2eAgInSc1d1	blanský
les	les	k1gInSc1	les
s	s	k7c7	s
Kletí	kletí	k1gNnPc5	kletí
<g/>
.	.	kIx.	.
</s>
<s>
Budějovické	budějovický	k2eAgNnSc1d1	Budějovické
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgInPc1d1	teplý
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgInPc1d1	vlhký
a	a	k8xC	a
s	s	k7c7	s
mírnou	mírný	k2eAgFnSc7d1	mírná
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
efekt	efekt	k1gInSc1	efekt
blízkých	blízký	k2eAgMnPc2d1	blízký
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
Novohradské	novohradský	k2eAgFnPc1d1	Novohradská
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
slabě	slabě	k6eAd1	slabě
i	i	k9	i
vliv	vliv	k1gInSc1	vliv
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
fénové	fénové	k2eAgInPc4d1	fénové
efekty	efekt	k1gInPc4	efekt
při	při	k7c6	při
jižních	jižní	k2eAgInPc6d1	jižní
a	a	k8xC	a
jihovýchodních	jihovýchodní	k2eAgInPc6d1	jihovýchodní
větrech	vítr	k1gInPc6	vítr
(	(	kIx(	(
<g/>
srážkový	srážkový	k2eAgInSc1d1	srážkový
stín	stín	k1gInSc1	stín
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
k	k	k7c3	k
opačnému	opačný	k2eAgInSc3d1	opačný
efektu	efekt	k1gInSc3	efekt
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
severních	severní	k2eAgInPc6d1	severní
a	a	k8xC	a
severozápadních	severozápadní	k2eAgInPc6d1	severozápadní
větrech	vítr	k1gInPc6	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
zde	zde	k6eAd1	zde
vanou	vanout	k5eAaImIp3nP	vanout
západní	západní	k2eAgInPc1d1	západní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgInPc1d1	severozápadní
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
i	i	k9	i
podíl	podíl	k1gInSc1	podíl
větrů	vítr	k1gInPc2	vítr
východních	východní	k2eAgInPc2d1	východní
a	a	k8xC	a
jihovýchodních	jihovýchodní	k2eAgInPc2d1	jihovýchodní
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
mělké	mělký	k2eAgFnSc2d1	mělká
široké	široký	k2eAgFnSc2d1	široká
pánve	pánev	k1gFnSc2	pánev
omezuje	omezovat	k5eAaImIp3nS	omezovat
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
za	za	k7c2	za
zimních	zimní	k2eAgFnPc2d1	zimní
inverzí	inverze	k1gFnPc2	inverze
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
časté	častý	k2eAgFnPc1d1	častá
a	a	k8xC	a
husté	hustý	k2eAgFnPc1d1	hustá
mlhy	mlha	k1gFnPc1	mlha
zejména	zejména	k9	zejména
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Hustá	hustý	k2eAgFnSc1d1	hustá
zástavba	zástavba	k1gFnSc1	zástavba
a	a	k8xC	a
široké	široký	k2eAgFnPc1d1	široká
betonové	betonový	k2eAgFnPc1d1	betonová
či	či	k8xC	či
vydlážděné	vydlážděný	k2eAgFnPc1d1	vydlážděná
plochy	plocha	k1gFnPc1	plocha
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
obecně	obecně	k6eAd1	obecně
nižší	nízký	k2eAgFnSc4d2	nižší
rychlost	rychlost	k1gFnSc4	rychlost
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc2d2	vyšší
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
městským	městský	k2eAgInPc3d1	městský
okrajům	okraj	k1gInPc3	okraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
roční	roční	k2eAgInSc1d1	roční
průměr	průměr	k1gInSc1	průměr
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
8,1	[number]	k4	8,1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
-42,2	-42,2	k4	-42,2
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1929	[number]	k4	1929
v	v	k7c6	v
Litvínovicích	Litvínovice	k1gFnPc6	Litvínovice
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
37,8	[number]	k4	37,8
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrzne	mrznout	k5eAaImIp3nS	mrznout
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
111	[number]	k4	111
dnů	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
celodenní	celodenní	k2eAgInPc1d1	celodenní
mrazy	mráz	k1gInPc1	mráz
trvají	trvat	k5eAaImIp3nP	trvat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
31	[number]	k4	31
dní	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
tropických	tropický	k2eAgMnPc2d1	tropický
dnů	den	k1gInPc2	den
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
průměrný	průměrný	k2eAgInSc1d1	průměrný
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
na	na	k7c4	na
rok	rok	k1gInSc4	rok
činí	činit	k5eAaImIp3nS	činit
623	[number]	k4	623
mm	mm	kA	mm
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgInSc1d1	rekordní
denní	denní	k2eAgInSc1d1	denní
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
127,7	[number]	k4	127,7
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měsíční	měsíční	k2eAgInSc4d1	měsíční
z	z	k7c2	z
povodňového	povodňový	k2eAgInSc2d1	povodňový
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
403,5	[number]	k4	403,5
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
mělké	mělký	k2eAgFnSc6d1	mělká
pánvi	pánev	k1gFnSc6	pánev
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
dvou	dva	k4xCgFnPc2	dva
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
značnému	značný	k2eAgNnSc3d1	značné
riziku	riziko	k1gNnSc3	riziko
povodní	povodeň	k1gFnPc2	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
rozvodňovaly	rozvodňovat	k5eAaImAgFnP	rozvodňovat
Dobrovodský	Dobrovodský	k2eAgInSc4d1	Dobrovodský
potok	potok	k1gInSc4	potok
a	a	k8xC	a
Malše	Malše	k1gFnPc4	Malše
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgInPc1d1	lokální
deště	dešť	k1gInPc1	dešť
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
často	často	k6eAd1	často
problémy	problém	k1gInPc1	problém
též	též	k9	též
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
ulice	ulice	k1gFnSc1	ulice
Branišovská	Branišovská	k1gFnSc1	Branišovská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
větších	veliký	k2eAgFnPc6d2	veliký
srážkách	srážka	k1gFnPc6	srážka
kanalizace	kanalizace	k1gFnSc2	kanalizace
nestíhá	stíhat	k5eNaImIp3nS	stíhat
odvádět	odvádět	k5eAaImF	odvádět
vodu	voda	k1gFnSc4	voda
stékající	stékající	k2eAgFnSc4d1	stékající
z	z	k7c2	z
okolních	okolní	k2eAgMnPc2d1	okolní
mírně	mírně	k6eAd1	mírně
výše	vysoce	k6eAd2	vysoce
položených	položený	k2eAgFnPc2d1	položená
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
doplácí	doplácet	k5eAaImIp3nS	doplácet
i	i	k8xC	i
kampus	kampus	k1gInSc1	kampus
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kronik	kronika	k1gFnPc2	kronika
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
četné	četný	k2eAgInPc1d1	četný
případy	případ	k1gInPc1	případ
velkých	velký	k2eAgFnPc2d1	velká
povodní	povodeň	k1gFnPc2	povodeň
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
voda	voda	k1gFnSc1	voda
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
městském	městský	k2eAgNnSc6d1	Městské
opevnění	opevnění	k1gNnSc6	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
povodně	povodně	k6eAd1	povodně
omezeny	omezit	k5eAaPmNgInP	omezit
regulací	regulace	k1gFnSc7	regulace
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
(	(	kIx(	(
<g/>
Malše	Malše	k1gFnSc1	Malše
<g/>
,	,	kIx,	,
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
)	)	kIx)	)
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
jejich	jejich	k3xOp3gNnSc7	jejich
přesměrováním	přesměrování	k1gNnSc7	přesměrování
(	(	kIx(	(
<g/>
Dobrovodský	Dobrovodský	k2eAgInSc4d1	Dobrovodský
potok	potok	k1gInSc4	potok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
povodně	povodeň	k1gFnPc1	povodeň
se	se	k3xPyFc4	se
i	i	k9	i
přesto	přesto	k8xC	přesto
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
srpnu	srpen	k1gInSc6	srpen
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
březnu	březen	k1gInSc6	březen
1940	[number]	k4	1940
a	a	k8xC	a
červenci	červenec	k1gInSc3	červenec
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
povodní	povodeň	k1gFnPc2	povodeň
od	od	k7c2	od
Vltavy	Vltava	k1gFnSc2	Vltava
výrazně	výrazně	k6eAd1	výrazně
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Lipno	Lipno	k1gNnSc4	Lipno
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
tak	tak	k8xS	tak
postihly	postihnout	k5eAaPmAgFnP	postihnout
až	až	k6eAd1	až
pětisetletá	pětisetletý	k2eAgFnSc1d1	pětisetletá
povodeň	povodeň	k1gFnSc1	povodeň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
(	(	kIx(	(
<g/>
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
Malší	Malš	k1gFnPc2	Malš
a	a	k8xC	a
Vltavou	Vltava	k1gFnSc7	Vltava
byl	být	k5eAaImAgMnS	být
deset	deset	k4xCc4	deset
až	až	k9	až
patnáctkrát	patnáctkrát	k6eAd1	patnáctkrát
vyšší	vysoký	k2eAgFnSc4d2	vyšší
oproti	oproti	k7c3	oproti
průměru	průměr	k1gInSc3	průměr
a	a	k8xC	a
asi	asi	k9	asi
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
při	při	k7c6	při
rozsahem	rozsah	k1gInSc7	rozsah
následujících	následující	k2eAgNnPc6d1	následující
povodních	povodeň	k1gFnPc6	povodeň
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1888	[number]	k4	1888
a	a	k8xC	a
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
všech	všecek	k3xTgFnPc6	všecek
těchto	tento	k3xDgFnPc6	tento
povodních	povodeň	k1gFnPc6	povodeň
trpělo	trpět	k5eAaImAgNnS	trpět
jak	jak	k6eAd1	jak
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
některé	některý	k3yIgFnPc4	některý
jeho	jeho	k3xOp3gFnPc4	jeho
další	další	k2eAgFnPc4d1	další
části	část	k1gFnPc4	část
(	(	kIx(	(
<g/>
obzvlášť	obzvlášť	k6eAd1	obzvlášť
exponovaná	exponovaný	k2eAgFnSc1d1	exponovaná
je	být	k5eAaImIp3nS	být
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
kolonie	kolonie	k1gFnSc1	kolonie
podél	podél	k7c2	podél
Malše	Malše	k1gFnSc2	Malše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
Vrbenské	Vrbenský	k2eAgInPc4d1	Vrbenský
rybníky	rybník	k1gInPc4	rybník
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Kaliště	kaliště	k1gNnSc2	kaliště
na	na	k7c6	na
východě	východ	k1gInSc6	východ
města	město	k1gNnSc2	město
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Vrbenská	Vrbenský	k2eAgFnSc1d1	Vrbenská
tůň	tůň	k1gFnSc1	tůň
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Tůně	tůně	k1gFnSc1	tůně
u	u	k7c2	u
Špačků	špaček	k1gInPc2	špaček
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Malše	Malše	k1gFnSc2	Malše
Stromovka	Stromovka	k1gFnSc1	Stromovka
–	–	k?	–
park	park	k1gInSc1	park
na	na	k7c4	na
západojihozápad	západojihozápad	k1gInSc4	západojihozápad
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
významný	významný	k2eAgInSc1d1	významný
krajinný	krajinný	k2eAgInSc1d1	krajinný
prvek	prvek	k1gInSc1	prvek
<g/>
)	)	kIx)	)
Památné	památný	k2eAgInPc1d1	památný
stromy	strom	k1gInPc1	strom
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
celkem	celek	k1gInSc7	celek
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jinan	jinan	k1gInSc4	jinan
dvoulaločný	dvoulaločný	k2eAgInSc4d1	dvoulaločný
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
piaristického	piaristický	k2eAgInSc2d1	piaristický
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
u	u	k7c2	u
plaveckého	plavecký	k2eAgInSc2d1	plavecký
stadionu	stadion	k1gInSc2	stadion
<g/>
,	,	kIx,	,
jinan	jinan	k1gInSc1	jinan
dvoulaločný	dvoulaločný	k2eAgInSc1d1	dvoulaločný
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Lannovy	Lannův	k2eAgFnSc2d1	Lannova
loděnice	loděnice	k1gFnSc2	loděnice
<g/>
,	,	kIx,	,
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
červenolistý	červenolistý	k2eAgInSc4d1	červenolistý
v	v	k7c6	v
Dukelské	dukelský	k2eAgFnSc6d1	Dukelská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
duby	dub	k1gInPc4	dub
U	u	k7c2	u
Špačků	špaček	k1gInPc2	špaček
<g/>
,	,	kIx,	,
u	u	k7c2	u
Nového	Nového	k2eAgNnSc2d1	Nového
Roudného	Roudné	k1gNnSc2	Roudné
a	a	k8xC	a
u	u	k7c2	u
Starohaklovského	Starohaklovský	k2eAgInSc2d1	Starohaklovský
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Třebotovicích	Třebotovice	k1gFnPc6	Třebotovice
<g/>
.	.	kIx.	.
park	park	k1gInSc4	park
Na	na	k7c6	na
Sadech	sad	k1gInPc6	sad
–	–	k?	–
parčík	parčík	k1gInSc4	parčík
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
kolonií	kolonie	k1gFnSc7	kolonie
havranů	havran	k1gMnPc2	havran
polních	polní	k2eAgFnPc2d1	polní
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2016	[number]	k4	2016
sedmým	sedmý	k4xOgNnSc7	sedmý
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
99	[number]	k4	99
872	[number]	k4	872
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
už	už	k6eAd1	už
ale	ale	k8xC	ale
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
České	český	k2eAgFnSc2d1	Česká
Budějovice	Budějovice	k1gInPc4	Budějovice
činil	činit	k5eAaImAgInS	činit
jen	jen	k9	jen
93	[number]	k4	93
883	[number]	k4	883
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
poslední	poslední	k2eAgNnPc4d1	poslední
desetiletí	desetiletí	k1gNnPc4	desetiletí
snížení	snížení	k1gNnSc2	snížení
o	o	k7c4	o
3,5	[number]	k4	3,5
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
projev	projev	k1gInSc4	projev
suburbanizace	suburbanizace	k1gFnSc2	suburbanizace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
obcích	obec	k1gFnPc6	obec
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
značně	značně	k6eAd1	značně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
správním	správní	k2eAgInSc6d1	správní
obvodu	obvod	k1gInSc6	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
celkově	celkově	k6eAd1	celkově
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obvod	obvod	k1gInSc1	obvod
tvoří	tvořit	k5eAaImIp3nS	tvořit
českobudějovickou	českobudějovický	k2eAgFnSc4d1	českobudějovická
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žije	žít	k5eAaImIp3nS	žít
cca	cca	kA	cca
155	[number]	k4	155
tisíc	tisíc	k4xCgInSc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
budějovických	budějovický	k2eAgMnPc2d1	budějovický
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Češi	Čech	k1gMnPc1	Čech
(	(	kIx(	(
<g/>
94,9	[number]	k4	94,9
%	%	kIx~	%
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
etnickou	etnický	k2eAgFnSc4d1	etnická
menšinu	menšina	k1gFnSc4	menšina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
většinou	většinou	k6eAd1	většinou
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
odhadovanému	odhadovaný	k2eAgInSc3d1	odhadovaný
počtu	počet	k1gInSc3	počet
necelých	celý	k2eNgInPc2d1	necelý
2000	[number]	k4	2000
Romů	Rom	k1gMnPc2	Rom
se	se	k3xPyFc4	se
k	k	k7c3	k
romské	romský	k2eAgFnSc3d1	romská
národnosti	národnost	k1gFnSc3	národnost
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
jen	jen	k9	jen
125	[number]	k4	125
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
slovenské	slovenský	k2eAgFnSc2d1	slovenská
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
1,15	[number]	k4	1,15
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
celostátním	celostátní	k2eAgInSc7d1	celostátní
průměrem	průměr	k1gInSc7	průměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
českých	český	k2eAgMnPc2d1	český
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
německého	německý	k2eAgMnSc2d1	německý
a	a	k8xC	a
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
kolísal	kolísat	k5eAaImAgInS	kolísat
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Budějovic	Budějovice	k1gInPc2	Budějovice
po	po	k7c6	po
asi	asi	k9	asi
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Němci	Němec	k1gMnPc1	Němec
získali	získat	k5eAaPmAgMnP	získat
značnou	značný	k2eAgFnSc4d1	značná
převahu	převaha	k1gFnSc4	převaha
jak	jak	k8xC	jak
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
představitelů	představitel	k1gMnPc2	představitel
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
nutno	nutno	k6eAd1	nutno
však	však	k9	však
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
národnosti	národnost	k1gFnPc1	národnost
se	se	k3xPyFc4	se
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obecně	obecně	k6eAd1	obecně
nepřikládal	přikládat	k5eNaImAgInS	přikládat
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
a	a	k8xC	a
asi	asi	k9	asi
nejvýraznější	výrazný	k2eAgInSc4d3	nejvýraznější
projev	projev	k1gInSc4	projev
národnostního	národnostní	k2eAgNnSc2d1	národnostní
rozštěpení	rozštěpení	k1gNnSc2	rozštěpení
města	město	k1gNnSc2	město
spočíval	spočívat	k5eAaImAgMnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaměstnávalo	zaměstnávat	k5eAaImAgNnS	zaměstnávat
dva	dva	k4xCgMnPc4	dva
písaře	písař	k1gMnPc4	písař
<g/>
,	,	kIx,	,
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
politické	politický	k2eAgFnSc3d1	politická
a	a	k8xC	a
společenské	společenský	k2eAgFnSc3d1	společenská
polarizaci	polarizace	k1gFnSc3	polarizace
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
klesat	klesat	k5eAaImF	klesat
podíl	podíl	k1gInSc1	podíl
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
díky	díky	k7c3	díky
vyšší	vysoký	k2eAgFnSc3d2	vyšší
porodnosti	porodnost	k1gFnSc3	porodnost
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
zejména	zejména	k9	zejména
příchodu	příchod	k1gInSc2	příchod
nových	nový	k2eAgMnPc2d1	nový
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
industrializovaném	industrializovaný	k2eAgNnSc6d1	industrializované
městě	město	k1gNnSc6	město
hledali	hledat	k5eAaImAgMnP	hledat
práci	práce	k1gFnSc3	práce
coby	coby	k?	coby
dělníci	dělník	k1gMnPc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
už	už	k6eAd1	už
poměr	poměr	k1gInSc4	poměr
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
klesl	klesnout	k5eAaPmAgInS	klesnout
podíl	podíl	k1gInSc4	podíl
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c4	na
14	[number]	k4	14
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
2352	[number]	k4	2352
domech	dům	k1gInPc6	dům
44	[number]	k4	44
022	[number]	k4	022
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
22	[number]	k4	22
878	[number]	k4	878
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
35	[number]	k4	35
577	[number]	k4	577
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
7	[number]	k4	7
006	[number]	k4	006
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
212	[number]	k4	212
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
34	[number]	k4	34
926	[number]	k4	926
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
871	[number]	k4	871
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
4562	[number]	k4	4562
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
1423	[number]	k4	1423
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
3	[number]	k4	3
122	[number]	k4	122
domech	dům	k1gInPc6	dům
43	[number]	k4	43
788	[number]	k4	788
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
36	[number]	k4	36
252	[number]	k4	252
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
6	[number]	k4	6
681	[number]	k4	681
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
33	[number]	k4	33
006	[number]	k4	006
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
1	[number]	k4	1
191	[number]	k4	191
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
6	[number]	k4	6
278	[number]	k4	278
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
1	[number]	k4	1
138	[number]	k4	138
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
přestala	přestat	k5eAaPmAgFnS	přestat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
existovat	existovat	k5eAaImF	existovat
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
vysídlena	vysídlit	k5eAaPmNgFnS	vysídlit
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
či	či	k8xC	či
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
výraznou	výrazný	k2eAgFnSc4d1	výrazná
etnickou	etnický	k2eAgFnSc4d1	etnická
menšinu	menšina	k1gFnSc4	menšina
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
představovali	představovat	k5eAaImAgMnP	představovat
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
židovské	židovský	k2eAgFnPc1d1	židovská
rodiny	rodina	k1gFnPc1	rodina
se	se	k3xPyFc4	se
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
usídlily	usídlit	k5eAaPmAgFnP	usídlit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1341	[number]	k4	1341
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
byly	být	k5eAaImAgFnP	být
Židům	Žid	k1gMnPc3	Žid
odebrány	odebrán	k2eAgFnPc4d1	odebrána
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
dospělí	dospělí	k1gMnPc1	dospělí
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
byly	být	k5eAaImAgFnP	být
státní	státní	k2eAgFnPc1d1	státní
i	i	k8xC	i
městské	městský	k2eAgFnPc1d1	městská
protižidovské	protižidovský	k2eAgFnPc1d1	protižidovská
restrikce	restrikce	k1gFnPc1	restrikce
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
činil	činit	k5eAaImAgInS	činit
podíl	podíl	k1gInSc1	podíl
Židů	Žid	k1gMnPc2	Žid
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnSc2	město
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
výrazně	výrazně	k6eAd1	výrazně
klesl	klesnout	k5eAaPmAgInS	klesnout
kvůli	kvůli	k7c3	kvůli
emigraci	emigrace	k1gFnSc3	emigrace
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
i	i	k9	i
opouštění	opouštění	k1gNnSc4	opouštění
židovského	židovský	k2eAgNnSc2d1	Židovské
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
asimilace	asimilace	k1gFnSc2	asimilace
s	s	k7c7	s
ostatním	ostatní	k2eAgNnSc7d1	ostatní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
národnosti	národnost	k1gFnSc2	národnost
již	již	k6eAd1	již
jen	jen	k9	jen
168	[number]	k4	168
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
menšina	menšina	k1gFnSc1	menšina
přestala	přestat	k5eAaPmAgFnS	přestat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
existovat	existovat	k5eAaImF	existovat
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
budějovických	budějovický	k2eAgMnPc2d1	budějovický
Židů	Žid	k1gMnPc2	Žid
stala	stát	k5eAaPmAgFnS	stát
obětí	oběť	k1gFnSc7	oběť
holokaustu	holokaust	k1gInSc2	holokaust
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
se	se	k3xPyFc4	se
rozprchl	rozprchnout	k5eAaPmAgInS	rozprchnout
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
demografické	demografický	k2eAgNnSc4d1	demografické
složení	složení	k1gNnSc4	složení
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
měla	mít	k5eAaImAgFnS	mít
přes	přes	k7c4	přes
11	[number]	k4	11
000	[number]	k4	000
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přespolní	přespolní	k2eAgMnPc1d1	přespolní
studenti	student	k1gMnPc1	student
bydlí	bydlet	k5eAaImIp3nP	bydlet
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
pronajímají	pronajímat	k5eAaImIp3nP	pronajímat
byty	byt	k1gInPc4	byt
především	především	k9	především
na	na	k7c6	na
blízkém	blízký	k2eAgNnSc6d1	blízké
sídlišti	sídliště	k1gNnSc6	sídliště
Máj	Mája	k1gFnPc2	Mája
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
vyšší	vysoký	k2eAgFnSc3d2	vyšší
koncentraci	koncentrace	k1gFnSc3	koncentrace
sociálně	sociálně	k6eAd1	sociálně
slabých	slabý	k2eAgMnPc2d1	slabý
obyvatel	obyvatel	k1gMnPc2	obyvatel
nižší	nízký	k2eAgInPc1d2	nižší
nájmy	nájem	k1gInPc1	nájem
i	i	k8xC	i
ceny	cena	k1gFnPc1	cena
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Českobudějovické	českobudějovický	k2eAgInPc1d1	českobudějovický
kostely	kostel	k1gInPc1	kostel
a	a	k8xC	a
modlitebny	modlitebna	k1gFnPc1	modlitebna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
58,6	[number]	k4	58,6
%	%	kIx~	%
z	z	k7c2	z
97	[number]	k4	97
339	[number]	k4	339
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
12,6	[number]	k4	12,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roku	rok	k1gInSc6	rok
2001	[number]	k4	2001
vyznání	vyznání	k1gNnSc1	vyznání
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnějším	silný	k2eAgNnSc7d3	nejsilnější
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
23,9	[number]	k4	23,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
pozici	pozice	k1gFnSc4	pozice
posiluje	posilovat	k5eAaImIp3nS	posilovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
několik	několik	k4yIc1	několik
katolických	katolický	k2eAgInPc2d1	katolický
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
kongregací	kongregace	k1gFnPc2	kongregace
<g/>
:	:	kIx,	:
salesiáni	salesián	k1gMnPc1	salesián
<g/>
,	,	kIx,	,
petríni	petrín	k1gMnPc1	petrín
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
petrinky	petrinka	k1gFnSc2	petrinka
a	a	k8xC	a
Školské	školský	k2eAgFnSc2d1	školská
sestry	sestra	k1gFnSc2	sestra
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gFnSc1	Dame
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
i	i	k8xC	i
řády	řád	k1gInPc1	řád
dominikánů	dominikán	k1gMnPc2	dominikán
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
piaristů	piarista	k1gMnPc2	piarista
<g/>
,	,	kIx,	,
redemptoristů	redemptorista	k1gMnPc2	redemptorista
<g/>
,	,	kIx,	,
kapucínů	kapucín	k1gMnPc2	kapucín
a	a	k8xC	a
boromejek	boromejka	k1gFnPc2	boromejka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
katolické	katolický	k2eAgNnSc1d1	katolické
školství	školství	k1gNnSc1	školství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
tvoří	tvořit	k5eAaImIp3nP	tvořit
2	[number]	k4	2
mateřské	mateřský	k2eAgInPc1d1	mateřský
<g/>
,	,	kIx,	,
1	[number]	k4	1
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Biskupské	biskupský	k2eAgNnSc1d1	Biskupské
gymnázium	gymnázium	k1gNnSc1	gymnázium
J.	J.	kA	J.
N.	N.	kA	N.
Neumanna	Neumann	k1gMnSc2	Neumann
<g/>
;	;	kIx,	;
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
Teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
působí	působit	k5eAaImIp3nS	působit
Salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
středisko	středisko	k1gNnSc1	středisko
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
(	(	kIx(	(
<g/>
1180	[number]	k4	1180
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
(	(	kIx(	(
<g/>
505	[number]	k4	505
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
146	[number]	k4	146
<g/>
)	)	kIx)	)
a	a	k8xC	a
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
(	(	kIx(	(
<g/>
176	[number]	k4	176
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
bratrská	bratrský	k2eAgFnSc1d1	bratrská
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
příliš	příliš	k6eAd1	příliš
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
její	její	k3xOp3gInSc1	její
samostatný	samostatný	k2eAgInSc1d1	samostatný
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hlavní	hlavní	k2eAgFnSc1d1	hlavní
modlitebna	modlitebna	k1gFnSc1	modlitebna
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgNnSc1d1	aktivní
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
nových	nový	k2eAgMnPc2d1	nový
věřících	věřící	k1gMnPc2	věřící
pomocí	pomocí	k7c2	pomocí
podomních	podomní	k2eAgFnPc2d1	podomní
pochůzek	pochůzka	k1gFnPc2	pochůzka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
provádějí	provádět	k5eAaImIp3nP	provádět
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
rozsahu	rozsah	k1gInSc6	rozsah
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
a	a	k8xC	a
zejména	zejména	k9	zejména
mormoni	mormon	k1gMnPc1	mormon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
hrála	hrát	k5eAaImAgFnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
i	i	k9	i
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ta	ten	k3xDgFnSc1	ten
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
,	,	kIx,	,
a	a	k8xC	a
monumentální	monumentální	k2eAgFnSc1d1	monumentální
synagoga	synagoga	k1gFnSc1	synagoga
byla	být	k5eAaImAgFnS	být
okupačními	okupační	k2eAgMnPc7d1	okupační
úřady	úřada	k1gMnPc7	úřada
zbořena	zbořen	k2eAgFnSc1d1	zbořena
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
velký	velký	k2eAgInSc1d1	velký
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
hale	hala	k1gFnSc6	hala
společnosti	společnost	k1gFnSc2	společnost
E.	E.	kA	E.
<g/>
ON	on	k3xPp3gMnSc1	on
přímo	přímo	k6eAd1	přímo
naproti	naproti	k7c3	naproti
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stávala	stávat	k5eAaImAgFnS	stávat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
jsou	být	k5eAaImIp3nP	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
samy	sám	k3xTgFnPc1	sám
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c4	mnoho
cenných	cenný	k2eAgFnPc2d1	cenná
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jsou	být	k5eAaImIp3nP	být
přirozeným	přirozený	k2eAgInSc7d1	přirozený
centrem	centr	k1gInSc7	centr
turisticky	turisticky	k6eAd1	turisticky
atraktivního	atraktivní	k2eAgInSc2d1	atraktivní
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
zastávkou	zastávka	k1gFnSc7	zastávka
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc2d3	veliký
turistické	turistický	k2eAgFnSc2d1	turistická
atrakce	atrakce	k1gFnSc2	atrakce
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
rezervace	rezervace	k1gFnSc1	rezervace
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
Náměstí	náměstí	k1gNnSc1	náměstí
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
s	s	k7c7	s
barokní	barokní	k2eAgFnSc7d1	barokní
Samsonovou	Samsonův	k2eAgFnSc7d1	Samsonova
kašnou	kašna	k1gFnSc7	kašna
a	a	k8xC	a
radnicí	radnice	k1gFnSc7	radnice
postavenou	postavený	k2eAgFnSc4d1	postavená
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1727-1730	[number]	k4	1727-1730
Barokní	barokní	k2eAgFnSc1d1	barokní
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
a	a	k8xC	a
Černá	černat	k5eAaImIp3nS	černat
věž	věž	k1gFnSc4	věž
Piaristické	piaristický	k2eAgNnSc1d1	Piaristické
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
gotický	gotický	k2eAgInSc1d1	gotický
dominikánský	dominikánský	k2eAgInSc1d1	dominikánský
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
Obětování	obětování	k1gNnSc1	obětování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
pozdně	pozdně	k6eAd1	pozdně
renesanční	renesanční	k2eAgFnPc4d1	renesanční
Solnice	solnice	k1gFnPc4	solnice
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Jihočeské	jihočeský	k2eAgNnSc1d1	Jihočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
činohry	činohra	k1gFnSc2	činohra
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
operní	operní	k2eAgInSc4d1	operní
<g/>
,	,	kIx,	,
loutkoherecký	loutkoherecký	k2eAgInSc4d1	loutkoherecký
a	a	k8xC	a
baletní	baletní	k2eAgInSc4d1	baletní
soubor	soubor	k1gInSc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Jihočeské	jihočeský	k2eAgNnSc1d1	Jihočeské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
koněspřežky	koněspřežka	k1gFnSc2	koněspřežka
<g/>
,	,	kIx,	,
Jihočeské	jihočeský	k2eAgNnSc1d1	Jihočeské
motocyklové	motocyklový	k2eAgNnSc1d1	motocyklové
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Budvar	budvar	k1gInSc1	budvar
muzeum	muzeum	k1gNnSc1	muzeum
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
Budějovic	Budějovice	k1gInPc2	Budějovice
leží	ležet	k5eAaImIp3nS	ležet
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgInPc2d3	nejnavštěvovanější
českých	český	k2eAgInPc2d1	český
zámků	zámek	k1gInPc2	zámek
a	a	k8xC	a
ves	ves	k1gFnSc4	ves
Hosín	Hosína	k1gFnPc2	Hosína
se	s	k7c7	s
známým	známý	k2eAgInSc7d1	známý
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
též	též	k9	též
bývalé	bývalý	k2eAgNnSc1d1	bývalé
hornické	hornický	k2eAgNnSc1d1	Hornické
městečko	městečko	k1gNnSc1	městečko
Rudolfov	Rudolfovo	k1gNnPc2	Rudolfovo
s	s	k7c7	s
naučnou	naučný	k2eAgFnSc7d1	naučná
stezkou	stezka	k1gFnSc7	stezka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
s	s	k7c7	s
barokním	barokní	k2eAgInSc7d1	barokní
kostelem	kostel	k1gInSc7	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
<g/>
.	.	kIx.	.
</s>
<s>
Turistické	turistický	k2eAgFnSc3d1	turistická
atraktivitě	atraktivita	k1gFnSc3	atraktivita
města	město	k1gNnSc2	město
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
hotelů	hotel	k1gInPc2	hotel
a	a	k8xC	a
penzionů	penzion	k1gInPc2	penzion
<g/>
;	;	kIx,	;
známý	známý	k2eAgInSc1d1	známý
Grandhotel	grandhotel	k1gInSc1	grandhotel
Zvon	zvon	k1gInSc1	zvon
stojí	stát	k5eAaImIp3nS	stát
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
stojí	stát	k5eAaImIp3nS	stát
Hotel	hotel	k1gInSc1	hotel
Clarion	Clarion	k1gInSc1	Clarion
<g/>
,	,	kIx,	,
výšková	výškový	k2eAgFnSc1d1	výšková
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
r.	r.	kA	r.
1982	[number]	k4	1982
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
původně	původně	k6eAd1	původně
Gomel	Gomela	k1gFnPc2	Gomela
podle	podle	k7c2	podle
ruského	ruský	k2eAgInSc2d1	ruský
názvu	název	k1gInSc2	název
družebního	družební	k2eAgNnSc2d1	družební
běloruského	běloruský	k2eAgNnSc2d1	běloruské
města	město	k1gNnSc2	město
Homel	Homela	k1gFnPc2	Homela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
představil	představit	k5eAaPmAgMnS	představit
Jan	Jan	k1gMnSc1	Jan
Kaplický	Kaplický	k2eAgInSc4d1	Kaplický
návrh	návrh	k1gInSc4	návrh
koncertní	koncertní	k2eAgFnSc2d1	koncertní
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
moderní	moderní	k2eAgFnSc4d1	moderní
stavbu	stavba	k1gFnSc4	stavba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
jsou	být	k5eAaImIp3nP	být
statutární	statutární	k2eAgNnSc4d1	statutární
město	město	k1gNnSc4	město
s	s	k7c7	s
magistrátem	magistrát	k1gInSc7	magistrát
a	a	k8xC	a
primátorem	primátor	k1gMnSc7	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
45	[number]	k4	45
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
rada	rada	k1gFnSc1	rada
dohromady	dohromady	k6eAd1	dohromady
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
bývaly	bývat	k5eAaImAgInP	bývat
baštou	bašta	k1gFnSc7	bašta
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
od	od	k7c2	od
r.	r.	kA	r.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měla	mít	k5eAaImAgFnS	mít
primátora	primátor	k1gMnSc4	primátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
sjednotily	sjednotit	k5eAaPmAgFnP	sjednotit
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
US	US	kA	US
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
ovládly	ovládnout	k5eAaPmAgFnP	ovládnout
vedení	vedení	k1gNnSc4	vedení
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
na	na	k7c6	na
čemž	což	k3yRnSc6	což
nic	nic	k3yNnSc1	nic
nezměnily	změnit	k5eNaPmAgFnP	změnit
ani	ani	k8xC	ani
mimořádné	mimořádný	k2eAgFnPc1d1	mimořádná
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
rezignací	rezignace	k1gFnSc7	rezignace
16	[number]	k4	16
zastupitelů	zastupitel	k1gMnPc2	zastupitel
ODS	ODS	kA	ODS
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
2002	[number]	k4	2002
ODS	ODS	kA	ODS
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
KDU-ČSL	KDU-ČSL	k1gMnPc7	KDU-ČSL
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
zůstal	zůstat	k5eAaPmAgMnS	zůstat
primátor	primátor	k1gMnSc1	primátor
<g/>
;	;	kIx,	;
učinila	učinit	k5eAaImAgFnS	učinit
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získala	získat	k5eAaPmAgFnS	získat
těsnou	těsný	k2eAgFnSc4d1	těsná
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
a	a	k8xC	a
primátora	primátor	k1gMnSc2	primátor
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
dominuje	dominovat	k5eAaImIp3nS	dominovat
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
de	de	k?	de
Sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
senátní	senátní	k2eAgFnPc1d1	senátní
volby	volba	k1gFnPc1	volba
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
14	[number]	k4	14
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
budějovického	budějovický	k2eAgInSc2d1	budějovický
okresu	okres	k1gInSc2	okres
a	a	k8xC	a
západní	západní	k2eAgInSc4d1	západní
cíp	cíp	k1gInSc4	cíp
jindřichohradeckého	jindřichohradecký	k2eAgMnSc2d1	jindřichohradecký
<g/>
,	,	kIx,	,
volby	volba	k1gFnPc4	volba
r.	r.	kA	r.
2000	[number]	k4	2000
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Jiří	Jiří	k1gMnSc1	Jiří
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
a	a	k8xC	a
přilehlých	přilehlý	k2eAgNnPc6d1	přilehlé
městech	město	k1gNnPc6	město
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
všeobecně	všeobecně	k6eAd1	všeobecně
neúspěšných	úspěšný	k2eNgFnPc6d1	neúspěšná
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
nezabránilo	zabránit	k5eNaPmAgNnS	zabránit
její	její	k3xOp3gFnSc3	její
celkové	celkový	k2eAgFnSc3d1	celková
porážce	porážka	k1gFnSc3	porážka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Českobudějovické	českobudějovický	k2eAgFnSc6d1	českobudějovická
ODS	ODS	kA	ODS
k	k	k7c3	k
rozkolu	rozkol	k1gInSc3	rozkol
<g/>
,	,	kIx,	,
primátor	primátor	k1gMnSc1	primátor
za	za	k7c4	za
ODS	ODS	kA	ODS
Juraj	Juraj	k1gInSc4	Juraj
Thoma	Thomum	k1gNnSc2	Thomum
by	by	kYmCp3nS	by
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
založil	založit	k5eAaPmAgMnS	založit
nové	nový	k2eAgNnSc4d1	nové
politické	politický	k2eAgNnSc4d1	politické
uskupení	uskupení	k1gNnSc4	uskupení
Občané	občan	k1gMnPc1	občan
pro	pro	k7c4	pro
Budějovice	Budějovice	k1gInPc4	Budějovice
(	(	kIx(	(
<g/>
OPB	OPB	kA	OPB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
následně	následně	k6eAd1	následně
komunální	komunální	k2eAgFnPc4d1	komunální
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sestavení	sestavení	k1gNnSc6	sestavení
koalice	koalice	k1gFnSc2	koalice
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
5	[number]	k4	5
členů	člen	k1gInPc2	člen
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TOP09	TOP09	k1gMnSc1	TOP09
(	(	kIx(	(
<g/>
2	[number]	k4	2
členy	člen	k1gInPc7	člen
<g/>
)	)	kIx)	)
a	a	k8xC	a
OPB	OPB	kA	OPB
(	(	kIx(	(
<g/>
4	[number]	k4	4
členové	člen	k1gMnPc1	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Thoma	Thomum	k1gNnSc2	Thomum
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
primátorem	primátor	k1gMnSc7	primátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
volby	volba	k1gFnPc4	volba
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
a	a	k8xC	a
primátorem	primátor	k1gMnSc7	primátor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Budějovice	Budějovice	k1gInPc1	Budějovice
dělily	dělit	k5eAaImAgInP	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
osady	osada	k1gFnPc4	osada
<g/>
:	:	kIx,	:
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
,	,	kIx,	,
Linecké	linecký	k2eAgNnSc1d1	linecké
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgNnSc1d1	Pražské
předměstí	předměstí	k1gNnSc1	předměstí
a	a	k8xC	a
Vídeňské	vídeňský	k2eAgNnSc1d1	Vídeňské
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
členění	členění	k1gNnSc1	členění
oficiálně	oficiálně	k6eAd1	oficiálně
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výměra	výměra	k1gFnSc1	výměra
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1930	[number]	k4	1930
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
5180	[number]	k4	5180
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
na	na	k7c6	na
Lineckém	linecký	k2eAgNnSc6d1	linecké
předměstí	předměstí	k1gNnSc6	předměstí
6582	[number]	k4	6582
<g/>
,	,	kIx,	,
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgNnSc6d1	Vídeňské
předměstí	předměstí	k1gNnSc6	předměstí
13	[number]	k4	13
601	[number]	k4	601
a	a	k8xC	a
na	na	k7c6	na
Pražském	pražský	k2eAgNnSc6d1	Pražské
předměstí	předměstí	k1gNnSc6	předměstí
18	[number]	k4	18
425	[number]	k4	425
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
městské	městský	k2eAgFnSc2d1	městská
aglomerace	aglomerace	k1gFnSc2	aglomerace
tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgInP	být
i	i	k8xC	i
Čtyři	čtyři	k4xCgInPc1	čtyři
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Kněžské	kněžský	k2eAgInPc1d1	kněžský
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Mladé	mladá	k1gFnPc1	mladá
<g/>
,	,	kIx,	,
Nemanice	Nemanice	k1gFnPc1	Nemanice
<g/>
,	,	kIx,	,
Rožnov	Rožnov	k1gInSc1	Rožnov
a	a	k8xC	a
Suché	Suché	k2eAgFnPc1d1	Suché
Vrbné	Vrbná	k1gFnPc1	Vrbná
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ale	ale	k8xC	ale
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
obcemi	obec	k1gFnPc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
Statistická	statistický	k2eAgFnSc1d1	statistická
ročenka	ročenka	k1gFnSc1	ročenka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
do	do	k7c2	do
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1930	[number]	k4	1930
žilo	žít	k5eAaImAgNnS	žít
56	[number]	k4	56
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
započítala	započítat	k5eAaPmAgFnS	započítat
jen	jen	k9	jen
Čtyři	čtyři	k4xCgInPc4	čtyři
Dvory	Dvůr	k1gInPc4	Dvůr
<g/>
,	,	kIx,	,
Mladé	mladá	k1gFnPc4	mladá
<g/>
,	,	kIx,	,
Rožnov	Rožnov	k1gInSc1	Rožnov
a	a	k8xC	a
Suché	Suché	k2eAgNnSc1d1	Suché
Vrbné	Vrbné	k1gNnSc1	Vrbné
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
města	město	k1gNnSc2	město
a	a	k8xC	a
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
11	[number]	k4	11
osad	osada	k1gFnPc2	osada
<g/>
:	:	kIx,	:
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Čtyři	čtyři	k4xCgInPc1	čtyři
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
<g/>
,	,	kIx,	,
Kněžské	kněžský	k2eAgInPc1d1	kněžský
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Mladé	mladá	k1gFnPc1	mladá
<g/>
,	,	kIx,	,
Nemanice	Nemanice	k1gFnPc1	Nemanice
<g/>
,	,	kIx,	,
Pohůrka	Pohůrko	k1gNnPc1	Pohůrko
<g/>
,	,	kIx,	,
Rožnov	Rožnov	k1gInSc1	Rožnov
<g/>
,	,	kIx,	,
Suché	Suché	k2eAgNnSc1d1	Suché
Vrbné	Vrbné	k1gNnSc1	Vrbné
a	a	k8xC	a
Vráto	Vráto	k1gNnSc1	Vráto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Litvínovic	Litvínovice	k1gFnPc2	Litvínovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
Vráta	Vráto	k1gNnSc2	Vráto
a	a	k8xC	a
Hlinska	Hlinsko	k1gNnSc2	Hlinsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
členěné	členěný	k2eAgNnSc1d1	členěné
na	na	k7c4	na
10	[number]	k4	10
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Čtyři	čtyři	k4xCgInPc1	čtyři
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Kněžské	kněžský	k2eAgInPc1d1	kněžský
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Mladé	mladá	k1gFnPc1	mladá
<g/>
,	,	kIx,	,
Nemanice	Nemanice	k1gFnPc1	Nemanice
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Vráto	Vráto	k1gNnSc1	Vráto
<g/>
,	,	kIx,	,
Pohůrka	Pohůrka	k1gFnSc1	Pohůrka
<g/>
,	,	kIx,	,
Rožnov	Rožnov	k1gInSc1	Rožnov
<g/>
,	,	kIx,	,
a	a	k8xC	a
Suché	Suché	k2eAgNnSc1d1	Suché
Vrbné	Vrbné	k1gNnSc1	Vrbné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
k	k	k7c3	k
městu	město	k1gNnSc3	město
připojily	připojit	k5eAaPmAgInP	připojit
Nové	Nové	k2eAgInPc1d1	Nové
Hodějovice	Hodějovice	k1gFnPc4	Hodějovice
a	a	k8xC	a
částí	část	k1gFnPc2	část
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
územní	územní	k2eAgFnSc3d1	územní
reorganizaci	reorganizace	k1gFnSc3	reorganizace
<g/>
,	,	kIx,	,
stávajících	stávající	k2eAgFnPc2d1	stávající
11	[number]	k4	11
částí	část	k1gFnPc2	část
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
nově	nově	k6eAd1	nově
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
7	[number]	k4	7
číslovaných	číslovaný	k2eAgFnPc2d1	číslovaná
částí	část	k1gFnPc2	část
a	a	k8xC	a
jim	on	k3xPp3gMnPc3	on
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
členění	členění	k1gNnSc1	členění
platí	platit	k5eAaImIp3nS	platit
podnes	podnes	k6eAd1	podnes
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
připojené	připojený	k2eAgFnPc1d1	připojená
obce	obec	k1gFnPc1	obec
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
sice	sice	k8xC	sice
připojeny	připojit	k5eAaPmNgInP	připojit
k	k	k7c3	k
číslovaným	číslovaný	k2eAgFnPc3d1	číslovaná
částem	část	k1gFnPc3	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnPc1	jejich
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
zůstala	zůstat	k5eAaPmAgNnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
na	na	k7c4	na
11	[number]	k4	11
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
Kaliště	kaliště	k1gNnPc1	kaliště
a	a	k8xC	a
Třebotovice	Třebotovice	k1gFnPc1	Třebotovice
tvoří	tvořit	k5eAaImIp3nP	tvořit
exklávu	exkláva	k1gFnSc4	exkláva
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
1	[number]	k4	1
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
město	město	k1gNnSc4	město
–	–	k?	–
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
,	,	kIx,	,
Sokolský	sokolský	k2eAgInSc4d1	sokolský
ostrov	ostrov	k1gInSc4	ostrov
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
2	[number]	k4	2
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
2	[number]	k4	2
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
Vrbné	Vrbné	k1gNnSc1	Vrbné
a	a	k8xC	a
Haklovy	Haklův	k2eAgInPc1d1	Haklův
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
)	)	kIx)	)
–	–	k?	–
Stromovka	Stromovka	k1gFnSc1	Stromovka
<g/>
,	,	kIx,	,
Čtyři	čtyři	k4xCgInPc1	čtyři
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
sídliště	sídliště	k1gNnSc1	sídliště
Máj	Mája	k1gFnPc2	Mája
<g/>
,	,	kIx,	,
sídliště	sídliště	k1gNnSc4	sídliště
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
sídliště	sídliště	k1gNnSc1	sídliště
Vltava	Vltava	k1gFnSc1	Vltava
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
3	[number]	k4	3
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
3	[number]	k4	3
<g/>
)	)	kIx)	)
–	–	k?	–
Pražské	pražský	k2eAgNnSc1d1	Pražské
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
,	,	kIx,	,
Kněžské	kněžský	k2eAgInPc4d1	kněžský
Dvory	Dvůr	k1gInPc4	Dvůr
<g/>
,	,	kIx,	,
Nemanice	Nemanice	k1gFnPc4	Nemanice
České	český	k2eAgFnSc2d1	Česká
Budějovice	Budějovice	k1gInPc1	Budějovice
4	[number]	k4	4
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
4	[number]	k4	4
<g/>
)	)	kIx)	)
–	–	k?	–
Husova	Husův	k2eAgFnSc1d1	Husova
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
<g />
.	.	kIx.	.
</s>
<s>
Vráto	Vráto	k1gNnSc1	Vráto
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
5	[number]	k4	5
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
5	[number]	k4	5
<g/>
,	,	kIx,	,
Kaliště	kaliště	k1gNnSc4	kaliště
u	u	k7c2	u
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Třebotovice	Třebotovice	k1gFnSc1	Třebotovice
<g/>
)	)	kIx)	)
–	–	k?	–
Pětidomí	Pětidomí	k1gNnSc1	Pětidomí
<g/>
,	,	kIx,	,
Suché	Suché	k2eAgNnSc1d1	Suché
Vrbné	Vrbné	k1gNnSc1	Vrbné
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
<g/>
,	,	kIx,	,
Pohůrka	Pohůrko	k1gNnSc2	Pohůrko
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
6	[number]	k4	6
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
6	[number]	k4	6
<g/>
)	)	kIx)	)
–	–	k?	–
Lannova	Lannův	k2eAgFnSc1d1	Lannova
třída	třída	k1gFnSc1	třída
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
Vídeňské	vídeňský	k2eAgNnSc1d1	Vídeňské
předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
Mladé	mladý	k2eAgFnSc2d1	mladá
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc2d1	Nové
Hodějovice	Hodějovice	k1gFnSc2	Hodějovice
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
7	[number]	k4	7
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
7	[number]	k4	7
<g/>
)	)	kIx)	)
–	–	k?	–
Střelecký	střelecký	k2eAgInSc4d1	střelecký
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
Linecké	linecký	k2eAgNnSc4d1	linecké
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
,	,	kIx,	,
Rožnov	Rožnov	k1gInSc1	Rožnov
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Roudné	Roudné	k1gNnSc1	Roudné
Od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
jsou	být	k5eAaImIp3nP	být
Budějovice	Budějovice	k1gInPc1	Budějovice
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
samosprávu	samospráva	k1gFnSc4	samospráva
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
(	(	kIx(	(
<g/>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
2	[number]	k4	2
<g/>
-Čtyři	-Čtyř	k1gInSc6	-Čtyř
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
rozrůstáním	rozrůstání	k1gNnSc7	rozrůstání
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
zástavba	zástavba	k1gFnSc1	zástavba
blížila	blížit	k5eAaImAgFnS	blížit
k	k	k7c3	k
okolním	okolní	k2eAgFnPc3d1	okolní
obcím	obec	k1gFnPc3	obec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
případném	případný	k2eAgNnSc6d1	případné
sloučení	sloučení	k1gNnSc6	sloučení
se	se	k3xPyFc4	se
mluvilo	mluvit	k5eAaImAgNnS	mluvit
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
byly	být	k5eAaImAgFnP	být
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
k	k	k7c3	k
Budějovicím	Budějovice	k1gInPc3	Budějovice
připojeny	připojit	k5eAaPmNgInP	připojit
Čtyři	čtyři	k4xCgInPc1	čtyři
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Kněžské	kněžský	k2eAgInPc1d1	kněžský
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Litvínovice	Litvínovice	k1gFnPc1	Litvínovice
<g/>
,	,	kIx,	,
Mladé	mladá	k1gFnPc1	mladá
<g/>
,	,	kIx,	,
Pohůrka	Pohůrko	k1gNnPc1	Pohůrko
<g/>
,	,	kIx,	,
Rožnov	Rožnov	k1gInSc1	Rožnov
<g/>
,	,	kIx,	,
Suché	Suché	k2eAgNnSc1d1	Suché
Vrbné	Vrbné	k1gNnSc1	Vrbné
a	a	k8xC	a
Vráto	Vráto	k1gNnSc1	Vráto
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
osad	osada	k1gFnPc2	osada
Nové	Nové	k2eAgNnSc1d1	Nové
Vráto	Vráto	k1gNnSc1	Vráto
a	a	k8xC	a
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgFnP	připojit
Nemanice	Nemanice	k1gFnPc1	Nemanice
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
Litvínovice	Litvínovice	k1gFnPc1	Litvínovice
<g/>
,	,	kIx,	,
Vráto	Vráto	k1gNnSc1	Vráto
a	a	k8xC	a
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
následovaly	následovat	k5eAaImAgInP	následovat
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
byly	být	k5eAaImAgFnP	být
pohlceny	pohlcen	k2eAgFnPc1d1	pohlcena
Nové	Nové	k2eAgFnPc1d1	Nové
Hodějovice	Hodějovice	k1gFnPc1	Hodějovice
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
1976	[number]	k4	1976
Haklovy	Haklův	k2eAgInPc4d1	Haklův
Dvory	Dvůr	k1gInPc4	Dvůr
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
České	český	k2eAgFnSc2d1	Česká
Vrbné	Vrbná	k1gFnSc2	Vrbná
<g/>
,	,	kIx,	,
Třebotovice	Třebotovice	k1gFnSc2	Třebotovice
a	a	k8xC	a
Kaliště	kaliště	k1gNnSc2	kaliště
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
u	u	k7c2	u
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
působnost	působnost	k1gFnSc1	působnost
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
původního	původní	k2eAgInSc2d1	původní
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
a	a	k8xC	a
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
soudům	soud	k1gInPc3	soud
přísluší	příslušet	k5eAaImIp3nS	příslušet
krajské	krajský	k2eAgNnSc1d1	krajské
a	a	k8xC	a
okresní	okresní	k2eAgNnSc1d1	okresní
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
Vazební	vazební	k2eAgFnPc1d1	vazební
věznice	věznice	k1gFnPc1	věznice
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
krajským	krajský	k2eAgInSc7d1	krajský
soudem	soud	k1gInSc7	soud
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
justičního	justiční	k2eAgInSc2d1	justiční
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
vybudovaného	vybudovaný	k2eAgInSc2d1	vybudovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
stavební	stavební	k2eAgInSc1d1	stavební
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
metropole	metropol	k1gFnSc2	metropol
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
a	a	k8xC	a
modernizací	modernizace	k1gFnSc7	modernizace
<g/>
.	.	kIx.	.
</s>
<s>
Ubytovací	ubytovací	k2eAgFnSc1d1	ubytovací
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
289	[number]	k4	289
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
117	[number]	k4	117
pro	pro	k7c4	pro
obviněné	obviněný	k1gMnPc4	obviněný
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
a	a	k8xC	a
172	[number]	k4	172
pro	pro	k7c4	pro
odsouzené	odsouzený	k1gMnPc4	odsouzený
ve	v	k7c6	v
výkonu	výkon	k1gInSc6	výkon
trestu	trest	k1gInSc2	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc4	výkon
vazby	vazba	k1gFnSc2	vazba
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
jednak	jednak	k8xC	jednak
separované	separovaný	k2eAgFnPc1d1	separovaná
cely	cela	k1gFnPc1	cela
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
oddělení	oddělení	k1gNnSc1	oddělení
se	s	k7c7	s
zmírněným	zmírněný	k2eAgInSc7d1	zmírněný
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
se	se	k3xPyFc4	se
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
staly	stát	k5eAaPmAgInP	stát
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
dráhy	dráha	k1gFnSc2	dráha
do	do	k7c2	do
Lince	Linec	k1gInSc2	Linec
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
50	[number]	k4	50
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
na	na	k7c4	na
standardní	standardní	k2eAgFnSc4d1	standardní
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
o	o	k7c6	o
rozchodu	rozchod	k1gInSc6	rozchod
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
vychází	vycházet	k5eAaImIp3nS	vycházet
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
220	[number]	k4	220
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
IV	IV	kA	IV
<g/>
.	.	kIx.	.
tranzitního	tranzitní	k2eAgInSc2d1	tranzitní
železničního	železniční	k2eAgInSc2d1	železniční
koridoru	koridor	k1gInSc2	koridor
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
přes	přes	k7c4	přes
Prahu	Praha	k1gFnSc4	Praha
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Rychlíky	rychlík	k1gInPc4	rychlík
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
zde	zde	k6eAd1	zde
jezdí	jezdit	k5eAaImIp3nP	jezdit
v	v	k7c6	v
hodinovém	hodinový	k2eAgInSc6d1	hodinový
taktu	takt	k1gInSc6	takt
a	a	k8xC	a
jízdní	jízdní	k2eAgFnSc1d1	jízdní
doba	doba	k1gFnSc1	doba
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Praha	Praha	k1gFnSc1	Praha
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
h	h	k?	h
36	[number]	k4	36
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
se	s	k7c7	s
šesti	šest	k4xCc7	šest
mezilehlými	mezilehlý	k2eAgFnPc7d1	mezilehlá
zastávkami	zastávka	k1gFnPc7	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozestavěním	rozestavění	k1gNnSc7	rozestavění
železničního	železniční	k2eAgInSc2d1	železniční
koridoru	koridor	k1gInSc2	koridor
byla	být	k5eAaImAgFnS	být
jízdní	jízdní	k2eAgFnSc1d1	jízdní
doba	doba	k1gFnSc1	doba
i	i	k8xC	i
kratší	krátký	k2eAgInPc1d2	kratší
(	(	kIx(	(
<g/>
např.	např.	kA	např.
2	[number]	k4	2
h	h	k?	h
12	[number]	k4	12
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
se	s	k7c7	s
zastavením	zastavení	k1gNnSc7	zastavení
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
se	s	k7c7	s
zkrácením	zkrácení	k1gNnSc7	zkrácení
jízdní	jízdní	k2eAgFnSc2d1	jízdní
doby	doba	k1gFnSc2	doba
rychlíku	rychlík	k1gInSc2	rychlík
ke	k	k7c3	k
2	[number]	k4	2
hodinám	hodina	k1gFnPc3	hodina
a	a	k8xC	a
nejrychlejšího	rychlý	k2eAgInSc2d3	nejrychlejší
vlaku	vlak	k1gInSc2	vlak
až	až	k9	až
k	k	k7c3	k
1,5	[number]	k4	1,5
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
jsou	být	k5eAaImIp3nP	být
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
spojené	spojený	k2eAgInPc1d1	spojený
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
koridoru	koridor	k1gInSc6	koridor
železniční	železniční	k2eAgFnSc2d1	železniční
tratí	tratit	k5eAaImIp3nS	tratit
196	[number]	k4	196
do	do	k7c2	do
Summerau	Summeraus	k1gInSc2	Summeraus
a	a	k8xC	a
na	na	k7c4	na
rakouský	rakouský	k2eAgInSc4d1	rakouský
Linec	Linec	k1gInSc4	Linec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Rakouským	rakouský	k2eAgInSc7d1	rakouský
Gmündem	Gmünd	k1gInSc7	Gmünd
a	a	k8xC	a
Českými	český	k2eAgFnPc7d1	Česká
Velenicemi	Velenice	k1gFnPc7	Velenice
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgFnSc2d1	spojená
tratí	tratit	k5eAaImIp3nS	tratit
199	[number]	k4	199
a	a	k8xC	a
se	s	k7c7	s
šumavským	šumavský	k2eAgNnSc7d1	Šumavské
Novým	nový	k2eAgNnSc7d1	nové
Údolím	údolí	k1gNnSc7	údolí
tratí	trať	k1gFnPc2	trať
194	[number]	k4	194
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Planá	Planá	k1gFnSc1	Planá
leží	ležet	k5eAaImIp3nS	ležet
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
a	a	k8xC	a
neveřejné	veřejný	k2eNgNnSc4d1	neveřejné
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
Letiště	letiště	k1gNnSc4	letiště
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bývalé	bývalý	k2eAgNnSc1d1	bývalé
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letiště	letiště	k1gNnSc1	letiště
kategorie	kategorie	k1gFnSc2	kategorie
4	[number]	k4	4
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
civilní	civilní	k2eAgNnSc1d1	civilní
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
využití	využití	k1gNnSc1	využití
po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
terminálu	terminál	k1gInSc2	terminál
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
etapa	etapa	k1gFnSc1	etapa
modernizace	modernizace	k1gFnSc2	modernizace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
následovala	následovat	k5eAaImAgFnS	následovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
za	za	k7c7	za
Hosínem	Hosín	k1gInSc7	Hosín
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
menší	malý	k2eAgNnSc1d2	menší
letiště	letiště	k1gNnSc1	letiště
s	s	k7c7	s
jednou	jednou	k6eAd1	jednou
800	[number]	k4	800
<g/>
m	m	kA	m
dráhou	dráha	k1gFnSc7	dráha
bez	bez	k7c2	bez
navigace	navigace	k1gFnSc2	navigace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Aeroklub	aeroklub	k1gInSc1	aeroklub
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
jsou	být	k5eAaImIp3nP	být
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
napojeny	napojen	k2eAgInPc1d1	napojen
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
síť	síť	k1gFnSc4	síť
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
splavnou	splavný	k2eAgFnSc7d1	splavná
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
lodí	loď	k1gFnPc2	loď
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
plavebními	plavební	k2eAgFnPc7d1	plavební
komorami	komora	k1gFnPc7	komora
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
45	[number]	k4	45
x	x	k?	x
6	[number]	k4	6
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc7d1	maximální
tonáží	tonáž	k1gFnSc7	tonáž
300	[number]	k4	300
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přístaviště	přístaviště	k1gNnSc1	přístaviště
Lannova	Lannov	k1gInSc2	Lannov
loděnice	loděnice	k1gFnSc2	loděnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
České	český	k2eAgFnSc2d1	Česká
Vrbné	Vrbná	k1gFnSc2	Vrbná
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
hlavní	hlavní	k2eAgInSc1d1	hlavní
městský	městský	k2eAgInSc1d1	městský
přístav	přístav	k1gInSc1	přístav
pro	pro	k7c4	pro
rekreační	rekreační	k2eAgFnPc4d1	rekreační
a	a	k8xC	a
osobní	osobní	k2eAgFnPc4d1	osobní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnPc1	třída
číslo	číslo	k1gNnSc1	číslo
3	[number]	k4	3
<g/>
,	,	kIx,	,
20	[number]	k4	20
a	a	k8xC	a
34	[number]	k4	34
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
156	[number]	k4	156
a	a	k8xC	a
157	[number]	k4	157
<g/>
.	.	kIx.	.
</s>
<s>
Budějovicemi	Budějovice	k1gInPc7	Budějovice
prochází	procházet	k5eAaImIp3nP	procházet
tři	tři	k4xCgFnPc1	tři
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
silnice	silnice	k1gFnPc1	silnice
E.	E.	kA	E.
Silnice	silnice	k1gFnSc1	silnice
3	[number]	k4	3
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc4	označení
E55	E55	k1gFnSc2	E55
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
Prahu	Praha	k1gFnSc4	Praha
s	s	k7c7	s
Českými	český	k2eAgInPc7d1	český
Budějovicemi	Budějovice	k1gInPc7	Budějovice
a	a	k8xC	a
Lincem	Linec	k1gInSc7	Linec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
20	[number]	k4	20
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
platí	platit	k5eAaImIp3nS	platit
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
označení	označení	k1gNnSc1	označení
E49	E49	k1gFnSc2	E49
spojuje	spojovat	k5eAaImIp3nS	spojovat
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
s	s	k7c7	s
Plzní	Plzeň	k1gFnSc7	Plzeň
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
Německo	Německo	k1gNnSc4	Německo
<g/>
.34	.34	k4	.34
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
statuty	statut	k1gInPc4	statut
E49	E49	k1gFnSc1	E49
a	a	k8xC	a
E551	E551	k1gFnSc1	E551
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Budějovic	Budějovice	k1gInPc2	Budějovice
na	na	k7c4	na
Humpolec	Humpolec	k1gInSc4	Humpolec
jako	jako	k8xC	jako
E551	E551	k1gFnSc4	E551
a	a	k8xC	a
u	u	k7c2	u
Třeboně	Třeboň	k1gFnSc2	Třeboň
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
jako	jako	k9	jako
E49	E49	k1gFnSc1	E49
na	na	k7c4	na
Wien	Wien	k1gInSc4	Wien
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
také	také	k9	také
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
spojovat	spojovat	k5eAaImF	spojovat
Prahu	Praha	k1gFnSc4	Praha
s	s	k7c7	s
Budějovicemi	Budějovice	k1gInPc7	Budějovice
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
až	až	k9	až
do	do	k7c2	do
Lince	Linec	k1gInSc2	Linec
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
autobusy	autobus	k1gInPc1	autobus
RegioJet	RegioJet	k1gInSc1	RegioJet
a	a	k8xC	a
také	také	k9	také
M	M	kA	M
express	express	k1gInSc1	express
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
LEO	Leo	k1gMnSc1	Leo
Express	express	k1gInSc4	express
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
LEO	Leo	k1gMnSc1	Leo
Express	express	k1gInSc4	express
Easy	Easa	k1gFnSc2	Easa
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
spojů	spoj	k1gInPc2	spoj
provozuje	provozovat	k5eAaImIp3nS	provozovat
také	také	k9	také
Arriva	Arriva	k1gFnSc1	Arriva
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Jihotrans	Jihotrans	k1gInSc1	Jihotrans
<g/>
.	.	kIx.	.
</s>
<s>
Spoje	spoj	k1gInPc1	spoj
většinou	většinou	k6eAd1	většinou
končí	končit	k5eAaImIp3nP	končit
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
Praha-Roztyly	Praha-Roztyl	k1gInPc1	Praha-Roztyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
autobusy	autobus	k1gInPc4	autobus
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
až	až	k9	až
na	na	k7c4	na
smíchovské	smíchovský	k2eAgNnSc4d1	Smíchovské
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
autobusy	autobus	k1gInPc4	autobus
LEO	Leo	k1gMnSc1	Leo
Express	express	k1gInSc4	express
Easy	Easa	k1gFnSc2	Easa
na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
vlakové	vlakový	k2eAgNnSc4d1	vlakové
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
ÚAN	ÚAN	kA	ÚAN
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
a	a	k8xC	a
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
zavedením	zavedení	k1gNnSc7	zavedení
tramvajové	tramvajový	k2eAgMnPc4d1	tramvajový
(	(	kIx(	(
<g/>
k	k	k7c3	k
nádraží	nádraží	k1gNnSc3	nádraží
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
předváděcí	předváděcí	k2eAgFnSc1d1	předváděcí
jízda	jízda	k1gFnSc1	jízda
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
a	a	k8xC	a
trolejbusové	trolejbusový	k2eAgMnPc4d1	trolejbusový
(	(	kIx(	(
<g/>
ke	k	k7c3	k
hřbitovu	hřbitov	k1gInSc3	hřbitov
<g/>
)	)	kIx)	)
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
označena	označen	k2eAgFnSc1d1	označena
P	P	kA	P
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
asi	asi	k9	asi
3	[number]	k4	3
kilometry	kilometr	k1gInPc7	kilometr
od	od	k7c2	od
nádraží	nádraží	k1gNnSc2	nádraží
přes	přes	k7c4	přes
Pražské	pražský	k2eAgNnSc4d1	Pražské
předměstí	předměstí	k1gNnSc4	předměstí
k	k	k7c3	k
dělostřeleckým	dělostřelecký	k2eAgFnPc3d1	dělostřelecká
kasárnám	kasárny	k1gFnPc3	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
druhá	druhý	k4xOgFnSc1	druhý
označená	označený	k2eAgFnSc1d1	označená
L	L	kA	L
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Linecké	linecký	k2eAgNnSc1d1	linecké
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
do	do	k7c2	do
systému	systém	k1gInSc2	systém
zvláště	zvláště	k6eAd1	zvláště
přes	přes	k7c4	přes
válku	válka	k1gFnSc4	válka
neinvestovalo	investovat	k5eNaBmAgNnS	investovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
existovaly	existovat	k5eAaImAgInP	existovat
plány	plán	k1gInPc1	plán
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusy	trolejbus	k1gInPc1	trolejbus
ji	on	k3xPp3gFnSc4	on
začaly	začít	k5eAaPmAgInP	začít
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
provoz	provoz	k1gInSc4	provoz
vydržel	vydržet	k5eAaPmAgMnS	vydržet
jen	jen	k9	jen
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
rozšiřována	rozšiřovat	k5eAaImNgFnS	rozšiřovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
měly	mít	k5eAaImAgFnP	mít
tratě	trať	k1gFnPc1	trať
cca	cca	kA	cca
200	[number]	k4	200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
síť	síť	k1gFnSc1	síť
trolejbusů	trolejbus	k1gInPc2	trolejbus
znovu	znovu	k6eAd1	znovu
obnovena	obnoven	k2eAgFnSc1d1	obnovena
a	a	k8xC	a
přes	přes	k7c4	přes
finanční	finanční	k2eAgFnPc4d1	finanční
obtíže	obtíž	k1gFnPc4	obtíž
postupně	postupně	k6eAd1	postupně
rozšiřována	rozšiřován	k2eAgFnSc1d1	rozšiřována
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusy	trolejbus	k1gInPc1	trolejbus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c4	na
6	[number]	k4	6
denních	denní	k2eAgMnPc2d1	denní
a	a	k8xC	a
2	[number]	k4	2
nočních	noční	k2eAgFnPc6d1	noční
linkách	linka	k1gFnPc6	linka
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
70,2	[number]	k4	70,2
kilometru	kilometr	k1gInSc2	kilometr
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
na	na	k7c6	na
čtrnácti	čtrnáct	k4xCc6	čtrnáct
linkách	linka	k1gFnPc6	linka
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
164,2	[number]	k4	164,2
km	km	kA	km
a	a	k8xC	a
zajíždějí	zajíždět	k5eAaImIp3nP	zajíždět
i	i	k9	i
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
vzdálenějších	vzdálený	k2eAgFnPc2d2	vzdálenější
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
vnějších	vnější	k2eAgNnPc2d1	vnější
tarifních	tarifní	k2eAgNnPc2d1	tarifní
pásem	pásmo	k1gNnPc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Školství	školství	k1gNnSc2	školství
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
27	[number]	k4	27
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
24	[number]	k4	24
státních	státní	k2eAgMnPc2d1	státní
<g/>
,	,	kIx,	,
2	[number]	k4	2
katolické	katolický	k2eAgInPc1d1	katolický
a	a	k8xC	a
1	[number]	k4	1
soukromá	soukromý	k2eAgNnPc1d1	soukromé
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
2465	[number]	k4	2465
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
19	[number]	k4	19
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
18	[number]	k4	18
státních	státní	k2eAgMnPc2d1	státní
a	a	k8xC	a
1	[number]	k4	1
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
;	;	kIx,	;
9458	[number]	k4	9458
žáků	žák	k1gMnPc2	žák
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
základních	základní	k2eAgFnPc2d1	základní
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
4	[number]	k4	4
státní	státní	k2eAgMnSc1d1	státní
a	a	k8xC	a
2	[number]	k4	2
soukromé	soukromý	k2eAgFnSc2d1	soukromá
<g/>
;	;	kIx,	;
2700	[number]	k4	2700
žáků	žák	k1gMnPc2	žák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
19	[number]	k4	19
středních	střední	k2eAgFnPc2d1	střední
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
či	či	k8xC	či
učilišť	učiliště	k1gNnPc2	učiliště
(	(	kIx(	(
<g/>
13	[number]	k4	13
státních	státní	k2eAgMnPc2d1	státní
a	a	k8xC	a
6	[number]	k4	6
soukromých	soukromý	k2eAgMnPc2d1	soukromý
<g/>
;	;	kIx,	;
8	[number]	k4	8
180	[number]	k4	180
žáků	žák	k1gMnPc2	žák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
gymnázií	gymnázium	k1gNnPc2	gymnázium
(	(	kIx(	(
<g/>
4	[number]	k4	4
státní	státní	k2eAgMnSc1d1	státní
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2	[number]	k4	2
soukromá	soukromý	k2eAgFnSc1d1	soukromá
a	a	k8xC	a
1	[number]	k4	1
katolické	katolický	k2eAgFnSc2d1	katolická
<g/>
;	;	kIx,	;
2899	[number]	k4	2899
studentů	student	k1gMnPc2	student
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
vyšších	vysoký	k2eAgFnPc2d2	vyšší
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
1241	[number]	k4	1241
studentů	student	k1gMnPc2	student
<g/>
)	)	kIx)	)
a	a	k8xC	a
3	[number]	k4	3
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13.000	[number]	k4	13.000
studenty	student	k1gMnPc7	student
a	a	k8xC	a
cca	cca	kA	cca
700	[number]	k4	700
pedagogy	pedagog	k1gMnPc7	pedagog
<g/>
,	,	kIx,	,
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
regionálních	regionální	k2eAgFnPc2d1	regionální
studií	studie	k1gFnPc2	studie
s	s	k7c7	s
240	[number]	k4	240
studenty	student	k1gMnPc7	student
a	a	k8xC	a
čerstvě	čerstvě	k6eAd1	čerstvě
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
s	s	k7c7	s
asi	asi	k9	asi
3500	[number]	k4	3500
studenty	student	k1gMnPc7	student
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
šest	šest	k4xCc4	šest
ústavů	ústav	k1gInPc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
Entomologický	entomologický	k2eAgInSc1d1	entomologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
Hydrobiologický	hydrobiologický	k2eAgInSc1d1	hydrobiologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
Parazitologický	parazitologický	k2eAgInSc1d1	parazitologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
systémové	systémový	k2eAgFnSc2d1	systémová
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
ekologie	ekologie	k1gFnSc2	ekologie
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
Ústav	ústav	k1gInSc1	ústav
ekologie	ekologie	k1gFnSc2	ekologie
krajiny	krajina	k1gFnSc2	krajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
Ústav	ústav	k1gInSc1	ústav
půdní	půdní	k2eAgFnSc2d1	půdní
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Budějovický	budějovický	k2eAgInSc1d1	budějovický
Budvar	budvar	k1gInSc1	budvar
–	–	k?	–
pivovar	pivovar	k1gInSc1	pivovar
Budějovický	budějovický	k2eAgInSc1d1	budějovický
měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
–	–	k?	–
pivo	pivo	k1gNnSc1	pivo
Samson	Samson	k1gMnSc1	Samson
Mondi	Mond	k1gMnPc1	Mond
Bupak	Bupak	k1gMnSc1	Bupak
–	–	k?	–
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
vlnité	vlnitý	k2eAgFnSc2d1	vlnitá
lepenky	lepenka	k1gFnSc2	lepenka
a	a	k8xC	a
obalů	obal	k1gInPc2	obal
z	z	k7c2	z
vlnité	vlnitý	k2eAgFnSc2d1	vlnitá
lepenky	lepenka	k1gFnSc2	lepenka
GAMA	gama	k1gNnSc3	gama
–	–	k?	–
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
spotřebního	spotřební	k2eAgInSc2d1	spotřební
zdravotnického	zdravotnický	k2eAgInSc2d1	zdravotnický
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
a	a	k8xC	a
veterinárních	veterinární	k2eAgInPc2d1	veterinární
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
plastů	plast	k1gInPc2	plast
Gamex	Gamex	k1gInSc4	Gamex
–	–	k?	–
výroba	výroba	k1gFnSc1	výroba
potravinářských	potravinářský	k2eAgInPc2d1	potravinářský
obalů	obal	k1gInPc2	obal
<g />
.	.	kIx.	.
</s>
<s>
Jihočeské	jihočeský	k2eAgFnPc1d1	Jihočeská
tiskárny	tiskárna	k1gFnPc1	tiskárna
–	–	k?	–
výroba	výroba	k1gFnSc1	výroba
etiket	etiketa	k1gFnPc2	etiketa
a	a	k8xC	a
papírových	papírový	k2eAgInPc2d1	papírový
obalů	obal	k1gInPc2	obal
Koh-i-noor	Kohoor	k1gMnSc1	Koh-i-noor
Hardtmuth	Hardtmuth	k1gMnSc1	Hardtmuth
–	–	k?	–
výroba	výroba	k1gFnSc1	výroba
školních	školní	k2eAgInPc2d1	školní
a	a	k8xC	a
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
potřeb	potřeba	k1gFnPc2	potřeba
Madeta	Madeta	k1gFnSc1	Madeta
–	–	k?	–
mlékárny	mlékárna	k1gFnSc2	mlékárna
Motor	motor	k1gInSc1	motor
Jikov	Jikov	k1gInSc1	Jikov
–	–	k?	–
strojírenská	strojírenský	k2eAgFnSc1d1	strojírenská
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
tlaková	tlakový	k2eAgFnSc1d1	tlaková
slévárna	slévárna	k1gFnSc1	slévárna
<g/>
,	,	kIx,	,
slévárna	slévárna	k1gFnSc1	slévárna
litiny	litina	k1gFnSc2	litina
<g/>
,	,	kIx,	,
nástrojárna	nástrojárna	k1gFnSc1	nástrojárna
Robert	Robert	k1gMnSc1	Robert
Bosch	Bosch	kA	Bosch
–	–	k?	–
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
komponent	komponenta	k1gFnPc2	komponenta
motorů	motor	k1gInPc2	motor
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
automobily	automobil	k1gInPc4	automobil
Slévárna	slévárna	k1gFnSc1	slévárna
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
V	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
mají	mít	k5eAaImIp3nP	mít
redakce	redakce	k1gFnPc4	redakce
všechna	všechen	k3xTgNnPc1	všechen
regionální	regionální	k2eAgNnPc1d1	regionální
média	médium	k1gNnPc1	médium
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Vysílá	vysílat	k5eAaImIp3nS	vysílat
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Hitrádio	Hitrádio	k6eAd1	Hitrádio
Faktor	faktor	k1gInSc1	faktor
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Rádio	rádio	k1gNnSc1	rádio
Podzemí	podzemí	k1gNnSc2	podzemí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rádio	rádio	k1gNnSc1	rádio
Gold	Gold	k1gMnSc1	Gold
<g/>
,	,	kIx,	,
Eldorádio	Eldorádio	k1gMnSc1	Eldorádio
<g/>
,	,	kIx,	,
Rockrádio	Rockrádio	k1gMnSc1	Rockrádio
Gold	Gold	k1gMnSc1	Gold
a	a	k8xC	a
Kiss	Kiss	k1gInSc1	Kiss
Jižní	jižní	k2eAgFnSc2d1	jižní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
novinami	novina	k1gFnPc7	novina
je	být	k5eAaImIp3nS	být
Českobudějovický	českobudějovický	k2eAgInSc4d1	českobudějovický
deník	deník	k1gInSc4	deník
(	(	kIx(	(
<g/>
do	do	k7c2	do
r.	r.	kA	r.
2006	[number]	k4	2006
Českobudějovické	českobudějovický	k2eAgInPc1d1	českobudějovický
listy	list	k1gInPc1	list
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Budějovicích	Budějovice	k1gInPc6	Budějovice
má	mít	k5eAaImIp3nS	mít
oblastní	oblastní	k2eAgFnSc3d1	oblastní
redakci	redakce	k1gFnSc3	redakce
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
několik	několik	k4yIc1	několik
malých	malý	k2eAgFnPc2d1	malá
soukromých	soukromý	k2eAgFnPc2d1	soukromá
televizních	televizní	k2eAgFnPc2d1	televizní
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
poskytujících	poskytující	k2eAgFnPc2d1	poskytující
služby	služba	k1gFnPc4	služba
větším	veliký	k2eAgFnPc3d2	veliký
soukromým	soukromý	k2eAgFnPc3d1	soukromá
televizím	televize	k1gFnPc3	televize
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
plně	plně	k6eAd1	plně
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
lokální	lokální	k2eAgFnSc4d1	lokální
DVB-T	DVB-T	k1gFnSc4	DVB-T
multiplex	multiplex	k2eAgFnSc2d1	multiplex
společnosti	společnost	k1gFnSc2	společnost
Gimi	Gim	k1gFnSc2	Gim
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
mimo	mimo	k6eAd1	mimo
jiné	jiná	k1gFnSc2	jiná
vysílá	vysílat	k5eAaImIp3nS	vysílat
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
regionální	regionální	k2eAgFnSc1d1	regionální
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Českobudějovicko	Českobudějovicko	k1gNnSc1	Českobudějovicko
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
s	s	k7c7	s
lokálním	lokální	k2eAgInSc7d1	lokální
multiplexem	multiplex	k1gInSc7	multiplex
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
mají	mít	k5eAaImIp3nP	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
celostátních	celostátní	k2eAgFnPc6d1	celostátní
soutěžích	soutěž	k1gFnPc6	soutěž
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
fotbale	fotbal	k1gInSc6	fotbal
a	a	k8xC	a
volejbalu	volejbal	k1gInSc6	volejbal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
též	též	k9	též
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
sportovních	sportovní	k2eAgNnPc2d1	sportovní
zařízení	zařízení	k1gNnPc2	zařízení
poskytujících	poskytující	k2eAgNnPc2d1	poskytující
služby	služba	k1gFnPc4	služba
veřejnosti	veřejnost	k1gFnSc2	veřejnost
(	(	kIx(	(
<g/>
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
Střelecký	střelecký	k2eAgInSc1d1	střelecký
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Budvar	budvar	k1gInSc1	budvar
aréna	aréna	k1gFnSc1	aréna
<g/>
,	,	kIx,	,
Plavecký	plavecký	k2eAgInSc4d1	plavecký
stadion	stadion	k1gInSc4	stadion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
kluby	klub	k1gInPc1	klub
<g/>
:	:	kIx,	:
SK	Sk	kA	Sk
Dynamo	dynamo	k1gNnSc1	dynamo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
ČEZ	ČEZ	kA	ČEZ
Motor	motor	k1gInSc1	motor
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
(	(	kIx(	(
<g/>
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
)	)	kIx)	)
VK	VK	kA	VK
Jihostroj	Jihostroj	k1gInSc1	Jihostroj
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
volejbal	volejbal	k1gInSc1	volejbal
<g/>
)	)	kIx)	)
T.	T.	kA	T.
J.	J.	kA	J.
Sokol	Sokol	k1gInSc4	Sokol
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
atletika	atletika	k1gFnSc1	atletika
<g/>
)	)	kIx)	)
Hellboys	Hellboys	k1gInSc1	Hellboys
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
RC	RC	kA	RC
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
Rugby	rugby	k1gNnSc1	rugby
<g/>
)	)	kIx)	)
Budějovické	budějovický	k2eAgFnPc1d1	Budějovická
barakudy	barakuda	k1gFnPc1	barakuda
(	(	kIx(	(
<g/>
kriket	kriket	k1gInSc1	kriket
<g/>
)	)	kIx)	)
FBC	FBC	kA	FBC
Štíři	štír	k1gMnPc1	štír
České	český	k2eAgFnSc2d1	Česká
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
florbal	florbal	k1gInSc1	florbal
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
méně	málo	k6eAd2	málo
známé	známý	k2eAgInPc4d1	známý
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
působí	působit	k5eAaImIp3nP	působit
jediný	jediný	k2eAgInSc4d1	jediný
oficiální	oficiální	k2eAgInSc4d1	oficiální
klub	klub	k1gInSc4	klub
podvodního	podvodní	k2eAgInSc2d1	podvodní
hokeje	hokej	k1gInSc2	hokej
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Serrasalmus	Serrasalmus	k1gMnSc1	Serrasalmus
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vědecké	vědecký	k2eAgNnSc4d1	vědecké
pojmenování	pojmenování	k1gNnSc4	pojmenování
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
piraní	piranit	k5eAaPmIp3nS	piranit
<g/>
)	)	kIx)	)
založili	založit	k5eAaPmAgMnP	založit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
členové	člen	k1gMnPc1	člen
Klubu	klub	k1gInSc2	klub
sportovního	sportovní	k2eAgNnSc2d1	sportovní
potápění	potápění	k1gNnSc2	potápění
při	při	k7c6	při
Jihočeské	jihočeský	k2eAgFnSc6d1	Jihočeská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
oficiálním	oficiální	k2eAgInSc7d1	oficiální
týmem	tým	k1gInSc7	tým
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
turnajích	turnaj	k1gInPc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
významných	významný	k2eAgFnPc2d1	významná
postav	postava	k1gFnPc2	postava
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc4	seznam
primátorů	primátor	k1gMnPc2	primátor
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
českobudějovických	českobudějovický	k2eAgMnPc2d1	českobudějovický
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Hirzo	Hirza	k1gFnSc5	Hirza
z	z	k7c2	z
Klingenbergu	Klingenberg	k1gInSc2	Klingenberg
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1275	[number]	k4	1275
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
lokátor	lokátor	k1gMnSc1	lokátor
města	město	k1gNnSc2	město
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Jírovec	jírovec	k1gInSc1	jírovec
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Jan	Jan	k1gMnSc1	Jan
Valerián	valeriána	k1gFnPc2	valeriána
Jirsík	Jirsík	k1gMnSc1	Jirsík
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
–	–	k?	–
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
českobudějovický	českobudějovický	k2eAgMnSc1d1	českobudějovický
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
zakladatel	zakladatel	k1gMnSc1	zakladatel
českobudějovického	českobudějovický	k2eAgNnSc2d1	českobudějovické
českého	český	k2eAgNnSc2d1	české
školství	školství	k1gNnSc2	školství
–	–	k?	–
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Jana	Jan	k1gMnSc2	Jan
Valeriána	valeriána	k1gFnSc1	valeriána
Jirsíka	Jirsík	k1gMnSc2	Jirsík
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Lanna	Lann	k1gMnSc2	Lann
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
Franz	Franz	k1gMnSc1	Franz
Schuselka	Schuselka	k1gMnSc1	Schuselka
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Neumann	Neumann	k1gMnSc1	Neumann
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
philadelphský	philadelphský	k1gMnSc1	philadelphský
Václav	Václav	k1gMnSc1	Václav
Klement	Klement	k1gMnSc1	Klement
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
řeholník	řeholník	k1gMnSc1	řeholník
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
řádu	řád	k1gInSc2	řád
petrínů	petrín	k1gMnPc2	petrín
Eduard	Eduard	k1gMnSc1	Eduard
Kohout	Kohout	k1gMnSc1	Kohout
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Norbert	Norbert	k1gMnSc1	Norbert
Frýd	Frýd	k1gMnSc1	Frýd
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Zdenka	Zdenka	k1gFnSc1	Zdenka
Sulanová	Sulanová	k1gFnSc1	Sulanová
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Libuše	Libuše	k1gFnSc1	Libuše
Havelková	Havelková	k1gFnSc1	Havelková
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Pavel	Pavel	k1gMnSc1	Pavel
Vondruška	Vondruška	k1gMnSc1	Vondruška
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
herec	herec	k1gMnSc1	herec
Jiří	Jiří	k1gMnSc1	Jiří
Císler	Císler	k1gMnSc1	Císler
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Oldřich	Oldřich	k1gMnSc1	Oldřich
Unger	Unger	k1gMnSc1	Unger
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozhlasový	rozhlasový	k2eAgMnSc1d1	rozhlasový
reportér	reportér	k1gMnSc1	reportér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Marta	Marta	k1gFnSc1	Marta
Kubišová	Kubišová	k1gFnSc1	Kubišová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Vratislav	Vratislav	k1gMnSc1	Vratislav
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
businessman	businessman	k1gMnSc1	businessman
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Jan	Jan	k1gMnSc1	Jan
Rejžek	Rejžka	k1gFnPc2	Rejžka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sypal	sypat	k5eAaImAgMnS	sypat
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Tůma	Tůma	k1gMnSc1	Tůma
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
<g />
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
Karel	Karel	k1gMnSc1	Karel
Roden	Roden	k2eAgMnSc1d1	Roden
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Barbora	Barbora	k1gFnSc1	Barbora
Hrzánová	Hrzánová	k1gFnSc1	Hrzánová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
herce	herec	k1gMnSc2	herec
Jiřího	Jiří	k1gMnSc2	Jiří
Hrzána	Hrzán	k1gMnSc2	Hrzán
Jiří	Jiří	k1gMnSc2	Jiří
Pomeje	Pomeje	k?	Pomeje
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Petr	Petr	k1gMnSc1	Petr
Kolář	Kolář	k1gMnSc1	Kolář
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Jiří	Jiří	k1gMnSc1	Jiří
Hájíček	hájíček	k1gInSc1	hájíček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Radek	Radek	k1gMnSc1	Radek
Bělohlav	Bělohlav	k1gMnSc1	Bělohlav
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
Roman	Roman	k1gMnSc1	Roman
Turek	Turek	k1gMnSc1	Turek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
Stanleyova	Stanleyův	k2eAgInSc2d1	Stanleyův
poháru	pohár	k1gInSc2	pohár
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Jennings	Jennings	k1gInSc4	Jennings
Trophy	Tropha	k1gFnSc2	Tropha
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
David	David	k1gMnSc1	David
Tonzar	Tonzar	k1gMnSc1	Tonzar
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
Martin	Martin	k1gMnSc1	Martin
Kuba	Kuba	k1gFnSc1	Kuba
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
ODS	ODS	kA	ODS
Pavel	Pavel	k1gMnSc1	Pavel
Šporcl	Šporcl	k1gMnSc1	Šporcl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
Václav	Václav	k1gMnSc1	Václav
Prospal	prospat	k5eAaPmAgMnS	prospat
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
Stanislav	Stanislav	k1gMnSc1	Stanislav
Neckář	Neckář	k1gMnSc1	Neckář
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
Stanleyova	Stanleyův	k2eAgInSc2d1	Stanleyův
poháru	pohár	k1gInSc2	pohár
Jiří	Jiří	k1gMnSc1	Jiří
Mádl	Mádl	k1gMnSc1	Mádl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Ferjenčík	Ferjenčík	k1gMnSc1	Ferjenčík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
Pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
Daniel	Daniel	k1gMnSc1	Daniel
Stach	Stach	k1gMnSc1	Stach
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
Jitka	Jitka	k1gFnSc1	Jitka
Nováčková	Nováčková	k1gFnSc1	Nováčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
vítězka	vítězka	k1gFnSc1	vítězka
soutěže	soutěž	k1gFnSc2	soutěž
Česká	český	k2eAgFnSc1d1	Česká
Miss	miss	k1gFnSc1	miss
2011	[number]	k4	2011
Za	za	k7c4	za
patrony	patron	k1gInPc4	patron
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
<g/>
:	:	kIx,	:
svatý	svatý	k1gMnSc1	svatý
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
je	být	k5eAaImIp3nS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
panna	panna	k1gFnSc1	panna
Maria	Maria	k1gFnSc1	Maria
Budějovická	budějovický	k2eAgFnSc1d1	Budějovická
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
obraz	obraz	k1gInSc4	obraz
přinesl	přinést	k5eAaPmAgMnS	přinést
měšťan	měšťan	k1gMnSc1	měšťan
a	a	k8xC	a
kupec	kupec	k1gMnSc1	kupec
Václav	Václav	k1gMnSc1	Václav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1410	[number]	k4	1410
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
umístěn	umístit	k5eAaPmNgMnS	umístit
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
oltáři	oltář	k1gInSc6	oltář
u	u	k7c2	u
sloupu	sloup	k1gInSc2	sloup
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
kapli	kaple	k1gFnSc6	kaple
kostela	kostel	k1gInSc2	kostel
Obětování	obětování	k1gNnSc2	obětování
panny	panna	k1gFnSc2	panna
Marie	Maria	k1gFnSc2	Maria
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
nechal	nechat	k5eAaPmAgInS	nechat
zrestaurovat	zrestaurovat	k5eAaPmF	zrestaurovat
Baltasar	Baltasar	k1gInSc1	Baltasar
de	de	k?	de
Marradas	Marradas	k1gInSc1	Marradas
<g/>
)	)	kIx)	)
svatý	svatý	k2eAgMnSc1d1	svatý
Auracián	Auracián	k1gMnSc1	Auracián
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
města	město	k1gNnSc2	město
doneseny	donesen	k2eAgInPc4d1	donesen
roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
Jako	jako	k8xS	jako
ochránci	ochránce	k1gMnPc1	ochránce
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
před	před	k7c7	před
morem	mor	k1gInSc7	mor
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
krom	krom	k7c2	krom
patronů	patron	k1gMnPc2	patron
zpodobňováni	zpodobňován	k2eAgMnPc1d1	zpodobňován
svatý	svatý	k2eAgMnSc1d1	svatý
Roch	roch	k0	roch
<g/>
,	,	kIx,	,
svatá	svatat	k5eAaImIp3nS	svatat
Rozálie	Rozálie	k1gFnSc1	Rozálie
a	a	k8xC	a
svatý	svatý	k2eAgMnSc1d1	svatý
Šebestián	Šebestián	k1gMnSc1	Šebestián
<g/>
.	.	kIx.	.
</s>
<s>
Astana	Astana	k1gFnSc1	Astana
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
Homel	Homel	k1gInSc1	Homel
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Lorient	Lorient	k1gInSc1	Lorient
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Nitra	Nitra	k1gFnSc1	Nitra
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Pasov	Pasov	k1gInSc1	Pasov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Suhl	Suhla	k1gFnPc2	Suhla
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Almere	Almer	k1gInSc5	Almer
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
</s>
