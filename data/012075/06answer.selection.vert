<s>
Virginia	Virginium	k1gNnPc4	Virginium
Woolfová	Woolfový	k2eAgFnSc1d1	Woolfová
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
plodnou	plodný	k2eAgFnSc7d1	plodná
esejistkou	esejistka	k1gFnSc7	esejistka
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaPmAgFnS	napsat
přes	přes	k7c4	přes
500	[number]	k4	500
esejí	esej	k1gFnPc2	esej
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
