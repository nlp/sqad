<s>
Pravoslaví	pravoslaví	k1gNnSc1
</s>
<s>
Charakteristický	charakteristický	k2eAgInSc1d1
osmikonečný	osmikonečný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
ikonami	ikona	k1gFnPc7
v	v	k7c6
interiéru	interiér	k1gInSc6
pravoslavného	pravoslavný	k2eAgInSc2d1
kostela	kostel	k1gInSc2
</s>
<s>
Pravoslaví	pravoslaví	k1gNnSc1
neboli	neboli	k8xC
ortodoxie	ortodoxie	k1gFnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
či	či	k8xC
ortodoxní	ortodoxní	k2eAgFnSc1d1
církev	církev	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
ὀ	ὀ	k?
orthos	orthos	k1gInSc1
správný	správný	k2eAgInSc1d1
<g/>
,	,	kIx,
pravdivý	pravdivý	k2eAgInSc1d1
+	+	kIx~
δ	δ	k?
doxa	dox	k2eAgFnSc1d1
sláva	sláva	k1gFnSc1
<g/>
,	,	kIx,
názor	názor	k1gInSc1
<g/>
,	,	kIx,
učení	učení	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
křesťanské	křesťanský	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
a	a	k8xC
církev	církev	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
více	hodně	k6eAd2
územních	územní	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dohromady	dohromady	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
pravoslavné	pravoslavný	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
mají	mít	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
počátky	počátek	k1gInPc4
v	v	k7c6
prvotní	prvotní	k2eAgFnSc6d1
křesťanské	křesťanský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
v	v	k7c6
Jeruzalému	Jeruzalém	k1gInSc6
a	a	k8xC
dále	daleko	k6eAd2
pak	pak	k6eAd1
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
bývají	bývat	k5eAaImIp3nP
někdy	někdy	k6eAd1
označovány	označovat	k5eAaImNgInP
též	též	k9
jako	jako	k9
východní	východní	k2eAgFnSc4d1
církev	církev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavné	pravoslavný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
sdílejí	sdílet	k5eAaImIp3nP
církevní	církevní	k2eAgFnSc4d1
nauku	nauka	k1gFnSc4
a	a	k8xC
praxi	praxe	k1gFnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
o	o	k7c6
nich	on	k3xPp3gMnPc6
jako	jako	k9
o	o	k7c6
celku	celek	k1gInSc6
<g/>
,	,	kIx,
o	o	k7c6
společenství	společenství	k1gNnSc6
pravoslavných	pravoslavný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
hovoří	hovořit	k5eAaImIp3nS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
by	by	kYmCp3nS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
jedinou	jediný	k2eAgFnSc4d1
pravoslavnou	pravoslavný	k2eAgFnSc4d1
církev	církev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
v	v	k7c6
nuancích	nuance	k1gFnPc6
církevní	církevní	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
a	a	k8xC
obyčejů	obyčej	k1gInPc2
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
místními	místní	k2eAgFnPc7d1
církvemi	církev	k1gFnPc7
objevují	objevovat	k5eAaImIp3nP
odlišnosti	odlišnost	k1gFnPc1
podle	podle	k7c2
místní	místní	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
,	,	kIx,
zvyklosti	zvyklost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
pravoslavného	pravoslavný	k2eAgNnSc2d1
křesťanství	křesťanství	k1gNnSc2
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
K	k	k7c3
pravoslaví	pravoslaví	k1gNnSc3
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nP
především	především	k9
obyvatelé	obyvatel	k1gMnPc1
jihovýchodních	jihovýchodní	k2eAgInPc2d1
a	a	k8xC
východních	východní	k2eAgInPc2d1
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
z	z	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
Rumunska	Rumunsko	k1gNnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Srbska	Srbsko	k1gNnSc2
<g/>
,	,	kIx,
Ukrajiny	Ukrajina	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
tamních	tamní	k2eAgFnPc2d1
slovanských	slovanský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavní	pravoslavný	k2eAgMnPc1d1
věřící	věřící	k1gMnPc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
emigranti	emigrant	k1gMnPc1
<g/>
,	,	kIx,
však	však	k9
obývají	obývat	k5eAaImIp3nP
i	i	k9
mnoho	mnoho	k4c4
jiných	jiný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
velké	velký	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
pravoslavných	pravoslavný	k2eAgMnPc2d1
věřících	věřící	k1gMnPc2
existují	existovat	k5eAaImIp3nP
např.	např.	kA
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
Kanadě	Kanada	k1gFnSc6
nebo	nebo	k8xC
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
zachovává	zachovávat	k5eAaImIp3nS
apoštolskou	apoštolský	k2eAgFnSc4d1
posloupnost	posloupnost	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gNnSc1
učení	učení	k1gNnSc1
bylo	být	k5eAaImAgNnS
formulováno	formulovat	k5eAaImNgNnS
na	na	k7c6
církevních	církevní	k2eAgInPc6d1
sněmech	sněm	k1gInPc6
<g/>
,	,	kIx,
koncilech	koncil	k1gInPc6
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
má	mít	k5eAaImIp3nS
význačné	význačný	k2eAgNnSc1d1
místo	místo	k1gNnSc1
sedm	sedm	k4xCc4
ekumenických	ekumenický	k2eAgInPc2d1
koncilů	koncil	k1gInPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
až	až	k9
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc6
prvního	první	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
postupně	postupně	k6eAd1
narůstalo	narůstat	k5eAaImAgNnS
napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
východní	východní	k2eAgFnSc7d1
a	a	k8xC
západní	západní	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vedlo	vést	k5eAaImAgNnS
až	až	k9
k	k	k7c3
velkému	velký	k2eAgNnSc3d1
schizmatu	schizma	k1gNnSc3
(	(	kIx(
<g/>
1054	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
dodnes	dodnes	k6eAd1
nebylo	být	k5eNaImAgNnS
překonáno	překonat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definitivní	definitivní	k2eAgInSc1d1
rozkol	rozkol	k1gInSc1
církví	církev	k1gFnPc2
však	však	k9
nastal	nastat	k5eAaPmAgInS
teprve	teprve	k6eAd1
po	po	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
nauka	nauka	k1gFnSc1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
některých	některý	k3yIgInPc6
ohledech	ohled	k1gInPc6
relativně	relativně	k6eAd1
blízká	blízký	k2eAgFnSc1d1
nauce	nauka	k1gFnSc3
římskokatolické	římskokatolický	k2eAgFnSc3d1
a	a	k8xC
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
církvemi	církev	k1gFnPc7
pokračuje	pokračovat	k5eAaImIp3nS
dialog	dialog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
rozkol	rozkol	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
fixován	fixovat	k5eAaImNgInS
vývojem	vývoj	k1gInSc7
věrouky	věrouka	k1gFnSc2
římskokatolické	římskokatolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
druhého	druhý	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
–	–	k?
všechna	všechen	k3xTgNnPc4
tato	tento	k3xDgNnPc4
nová	nový	k2eAgNnPc4d1
dogmata	dogma	k1gNnPc4
přijatá	přijatý	k2eAgNnPc4d1
na	na	k7c6
západě	západ	k1gInSc6
po	po	k7c6
roce	rok	k1gInSc6
1054	#num#	k4
jsou	být	k5eAaImIp3nP
neslučitelná	slučitelný	k2eNgFnSc1d1
s	s	k7c7
pravoslavnou	pravoslavný	k2eAgFnSc7d1
věroukou	věrouka	k1gFnSc7
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
odlišnou	odlišný	k2eAgFnSc7d1
římskokatolickou	římskokatolický	k2eAgFnSc7d1
spiritualitou	spiritualita	k1gFnSc7
znemožňují	znemožňovat	k5eAaImIp3nP
obnovení	obnovení	k1gNnSc3
jednoty	jednota	k1gFnSc2
římské	římský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
protestantských	protestantský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
)	)	kIx)
s	s	k7c7
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
církve	církev	k1gFnSc2
</s>
<s>
Pravoslavný	pravoslavný	k2eAgInSc1d1
chrám	chrám	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Gorazda	Gorazd	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
je	být	k5eAaImIp3nS
společenstvím	společenství	k1gNnSc7
samosprávných	samosprávný	k2eAgFnPc2d1
místních	místní	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
každá	každý	k3xTgFnSc1
je	být	k5eAaImIp3nS
autokefální	autokefální	k2eAgFnSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
řízená	řízený	k2eAgFnSc1d1
vlastním	vlastní	k2eAgMnSc7d1
nejvyšším	vysoký	k2eAgMnSc7d3
představitelem	představitel	k1gMnSc7
–	–	k?
prvním	první	k4xOgMnSc7
z	z	k7c2
místních	místní	k2eAgMnPc2d1
biskupů	biskup	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
světové	světový	k2eAgFnPc1d1
místní	místní	k2eAgFnPc1d1
pravoslavné	pravoslavný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
jsou	být	k5eAaImIp3nP
sjednoceny	sjednotit	k5eAaPmNgFnP
nikoliv	nikoliv	k9
na	na	k7c6
správním	správní	k2eAgInSc6d1
principu	princip	k1gInSc6
jednoho	jeden	k4xCgNnSc2
pozemského	pozemský	k2eAgNnSc2d1
ústředí	ústředí	k1gNnSc2
(	(	kIx(
<g/>
hlavy	hlava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
na	na	k7c6
principu	princip	k1gInSc6
společné	společný	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
církevního	církevní	k2eAgNnSc2d1
učení	učení	k1gNnSc2
zakládajícího	zakládající	k2eAgInSc2d1
se	se	k3xPyFc4
na	na	k7c6
Písmu	písmo	k1gNnSc6
svatém	svatý	k2eAgNnSc6d1
a	a	k8xC
posvátné	posvátný	k2eAgFnSc3d1
Tradici	tradice	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
jednotou	jednota	k1gFnSc7
ve	v	k7c6
svatých	svatý	k2eAgFnPc6d1
Tajinách	tajina	k1gFnPc6
(	(	kIx(
<g/>
svátostech	svátost	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgFnPc1d1
světové	světový	k2eAgFnPc1d1
pravoslavné	pravoslavný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
tedy	tedy	k9
sdílejí	sdílet	k5eAaImIp3nP
tutéž	týž	k3xTgFnSc4
víru	víra	k1gFnSc4
<g/>
,	,	kIx,
základní	základní	k2eAgNnSc4d1
správní	správní	k2eAgNnSc4d1
–	–	k?
politické	politický	k2eAgInPc4d1
principy	princip	k1gInPc4
a	a	k8xC
liturgickou	liturgický	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavné	pravoslavný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
užívají	užívat	k5eAaImIp3nP
při	při	k7c6
liturgii	liturgie	k1gFnSc6
buď	buď	k8xC
starých	starý	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
ve	v	k7c6
slovanských	slovanský	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
často	často	k6eAd1
církevní	církevní	k2eAgFnPc1d1
slovanštiny	slovanština	k1gFnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
novější	nový	k2eAgFnSc1d2
forma	forma	k1gFnSc1
staroslověnštiny	staroslověnština	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
současných	současný	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
národních	národní	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biskupové	biskup	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
představují	představovat	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
místní	místní	k2eAgFnSc4d1
církev	církev	k1gFnSc4
<g/>
,	,	kIx,
bývají	bývat	k5eAaImIp3nP
nazýváni	nazýván	k2eAgMnPc1d1
patriarchové	patriarcha	k1gMnPc1
<g/>
,	,	kIx,
metropolité	metropolita	k1gMnPc1
nebo	nebo	k8xC
arcibiskupové	arcibiskup	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
předsedají	předsedat	k5eAaImIp3nP
biskupskému	biskupský	k2eAgInSc3d1
synodu	synod	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
představuje	představovat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc4d3
kanonickou	kanonický	k2eAgFnSc4d1
<g/>
,	,	kIx,
věroučnou	věroučný	k2eAgFnSc4d1
a	a	k8xC
administrativní	administrativní	k2eAgFnSc4d1
autoritu	autorita	k1gFnSc4
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc7d1
organizační	organizační	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
Církve	církev	k1gFnSc2
Kristovy	Kristův	k2eAgFnSc2d1
je	být	k5eAaImIp3nS
církevní	církevní	k2eAgFnSc1d1
obec	obec	k1gFnSc1
–	–	k?
čili	čili	k8xC
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
parochie	parochie	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
případně	případně	k6eAd1
monastýry	monastýr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
duchovního	duchovní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
částicí	částice	k1gFnSc7
Církve	církev	k1gFnSc2
církevní	církevní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
místních	místní	k2eAgMnPc2d1
pravoslavných	pravoslavný	k2eAgMnPc2d1
věřících	věřící	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
shromáždili	shromáždit	k5eAaPmAgMnP
spolu	spolu	k6eAd1
s	s	k7c7
řádně	řádně	k6eAd1
ustanoveným	ustanovený	k2eAgMnSc7d1
knězem	kněz	k1gMnSc7
kolem	kolem	k7c2
svatého	svatý	k2eAgNnSc2d1
prestolu	prestolu	k?
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
společné	společný	k2eAgFnPc4d1
svatou	svatý	k2eAgFnSc4d1
Tajinu	tajina	k1gFnSc4
Eucharistie	eucharistie	k1gFnSc2
v	v	k7c6
jednotě	jednota	k1gFnSc6
s	s	k7c7
celou	celý	k2eAgFnSc7d1
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
takové	takový	k3xDgNnSc1
eucharistické	eucharistický	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
je	být	k5eAaImIp3nS
plným	plný	k2eAgNnSc7d1
projevením	projevení	k1gNnSc7
celé	celý	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
tom	ten	k3xDgInSc6
čase	čas	k1gInSc6
a	a	k8xC
na	na	k7c6
daném	daný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
s	s	k7c7
veškerou	veškerý	k3xTgFnSc7
plností	plnost	k1gFnSc7
jejích	její	k3xOp3gInPc2
duchovních	duchovní	k2eAgInPc2d1
darů	dar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgFnPc4d1
Tajiny	tajina	k1gFnPc4
takto	takto	k6eAd1
sloužené	sloužený	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
identické	identický	k2eAgInPc1d1
se	s	k7c7
svatými	svatý	k2eAgFnPc7d1
Tajinami	tajina	k1gFnPc7
vykonávanými	vykonávaný	k2eAgFnPc7d1
kdekoliv	kdekoliv	k6eAd1
jinde	jinde	k6eAd1
a	a	k8xC
jindy	jindy	k6eAd1
v	v	k7c6
jednotě	jednota	k1gFnSc6
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
celou	celý	k2eAgFnSc7d1
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církví	církev	k1gFnSc7
uznávané	uznávaný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
pravé	pravý	k2eAgFnPc1d1
a	a	k8xC
spasitelné	spasitelný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Společenství	společenství	k1gNnSc1
pravoslavných	pravoslavný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
</s>
<s>
Pravoslavné	pravoslavný	k2eAgInPc1d1
kříže	kříž	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
řecký	řecký	k2eAgMnSc1d1
</s>
<s>
osmikonečný	osmikonečný	k2eAgInSc1d1
(	(	kIx(
<g/>
zejména	zejména	k9
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
jinde	jinde	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ruský	ruský	k2eAgMnSc1d1
</s>
<s>
byzantský	byzantský	k2eAgInSc1d1
</s>
<s>
etiopský	etiopský	k2eAgInSc1d1
</s>
<s>
bulharský	bulharský	k2eAgInSc1d1
</s>
<s>
srbský	srbský	k2eAgInSc1d1
</s>
<s>
srbský	srbský	k2eAgInSc1d1
tetragrammický	tetragrammický	k2eAgInSc1d1
</s>
<s>
koptský	koptský	k2eAgInSc1d1
</s>
<s>
řecký	řecký	k2eAgMnSc1d1
rovnoramenný	rovnoramenný	k2eAgMnSc1d1
</s>
<s>
Konstantinopolský	konstantinopolský	k2eAgInSc1d1
ekumenický	ekumenický	k2eAgInSc1d1
patriarchát	patriarchát	k1gInSc1
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Alexandrijský	alexandrijský	k2eAgInSc1d1
pravoslavný	pravoslavný	k2eAgInSc1d1
patriarchát	patriarchát	k1gInSc1
</s>
<s>
Koptská	koptský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Antiochijský	antiochijský	k2eAgInSc1d1
pravoslavný	pravoslavný	k2eAgInSc1d1
patriarchát	patriarchát	k1gInSc1
</s>
<s>
Syrská	syrský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
pravoslavný	pravoslavný	k2eAgInSc1d1
patriarchát	patriarchát	k1gInSc1
</s>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Makedonská	makedonský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Černohorská	černohorský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Gruzínská	gruzínský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Bulharská	bulharský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Etiopská	etiopský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Kyperská	kyperský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Polská	polský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Albánská	albánský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Ukrajiny	Ukrajina	k1gFnSc2
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Konstantinopolský	konstantinopolský	k2eAgMnSc1d1
patriarcha	patriarcha	k1gMnSc1
Bartoloměj	Bartoloměj	k1gMnSc1
I.	I.	kA
zrušil	zrušit	k5eAaPmAgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
rozhodnutí	rozhodnutí	k1gNnSc4
Konstantinopolského	konstantinopolský	k2eAgInSc2d1
patriarchátu	patriarchát	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1686	#num#	k4
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
se	se	k3xPyFc4
Ukrajinská	ukrajinský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
připojila	připojit	k5eAaPmAgFnS
k	k	k7c3
patriarchátu	patriarchát	k1gInSc3
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
jako	jako	k8xS,k8xC
autonomní	autonomní	k2eAgFnSc1d1
součást	součást	k1gFnSc1
Ruské	ruský	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
ukrajinského	ukrajinský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Petra	Petra	k1gFnSc1
Porošenka	Porošenka	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
s	s	k7c7
požehnáním	požehnání	k1gNnSc7
Bartoloměje	Bartoloměj	k1gMnSc2
I.	I.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
sloučením	sloučení	k1gNnSc7
a	a	k8xC
zánikem	zánik	k1gInSc7
dosavadních	dosavadní	k2eAgFnPc2d1
tří	tři	k4xCgFnPc2
pravoslavných	pravoslavný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
Moskevského	moskevský	k2eAgInSc2d1
patriarchátu	patriarchát	k1gInSc2
<g/>
,	,	kIx,
sjednocená	sjednocený	k2eAgFnSc1d1
místní	místní	k2eAgFnSc1d1
autocefální	autocefální	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
Bartoloměje	Bartoloměj	k1gMnSc2
přerušila	přerušit	k5eAaPmAgFnS
Ruská	ruský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
s	s	k7c7
Konstantinopolským	konstantinopolský	k2eAgInSc7d1
patriarchátem	patriarchát	k1gInSc7
veškeré	veškerý	k3xTgInPc4
styky	styk	k1gInPc4
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
tak	tak	k9
k	k	k7c3
rozkolu	rozkol	k1gInSc3
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
uvnitř	uvnitř	k7c2
pravoslavného	pravoslavný	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Osmikonečný	osmikonečný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
před	před	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
kostela	kostel	k1gInSc2
ve	v	k7c6
Svidníku	Svidník	k1gInSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Místní	místní	k2eAgFnSc4d1
pravoslavnou	pravoslavný	k2eAgFnSc4d1
církev	církev	k1gFnSc4
v	v	k7c6
těchto	tento	k3xDgFnPc6
zemích	zem	k1gFnPc6
tvoří	tvořit	k5eAaImIp3nS
autokefální	autokefální	k2eAgFnSc4d1
(	(	kIx(
<g/>
samostatnou	samostatný	k2eAgFnSc4d1
a	a	k8xC
nezávislou	závislý	k2eNgFnSc4d1
<g/>
)	)	kIx)
jednotku	jednotka	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
z	z	k7c2
Pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
Pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
česká	český	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
eparchie	eparchie	k1gFnPc4
(	(	kIx(
<g/>
biskupství	biskupství	k1gNnSc4
<g/>
)	)	kIx)
–	–	k?
pražskou	pražský	k2eAgFnSc7d1
a	a	k8xC
olomoucko-brněnskou	olomoucko-brněnský	k2eAgFnSc7d1
<g/>
;	;	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
sídlí	sídlet	k5eAaImIp3nS
arcibiskup	arcibiskup	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
se	se	k3xPyFc4
slovenská	slovenský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
dělí	dělit	k5eAaImIp3nS
taktéž	taktéž	k?
na	na	k7c4
dvě	dva	k4xCgFnPc4
eparchie	eparchie	k1gFnPc4
–	–	k?
prešovskou	prešovský	k2eAgFnSc4d1
a	a	k8xC
michaloveckou	michalovecký	k2eAgFnSc4d1
<g/>
,	,	kIx,
arcibiskup	arcibiskup	k1gMnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Prešově	Prešov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
místní	místní	k2eAgFnSc7d1
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církvi	církev	k1gFnSc3
oficiálně	oficiálně	k6eAd1
registrovanou	registrovaný	k2eAgFnSc4d1
Ministerstvem	ministerstvo	k1gNnSc7
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
je	být	k5eAaImIp3nS
Ruská	ruský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
podvorje	podvorje	k1gFnSc1
patriarchy	patriarcha	k1gMnSc2
moskevského	moskevský	k2eAgMnSc2d1
a	a	k8xC
celé	celý	k2eAgFnSc6d1
Rusi	Rus	k1gFnSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Vrcholným	vrcholný	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
české	český	k2eAgFnSc2d1
místní	místní	k2eAgFnSc2d1
církve	církev	k1gFnSc2
je	být	k5eAaImIp3nS
metropolita	metropolita	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnSc7,k3yQgMnSc7,k3yIgMnSc7
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
tradičně	tradičně	k6eAd1
volen	volit	k5eAaImNgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
arcibiskupů	arcibiskup	k1gMnPc2
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
místní	místní	k2eAgFnSc2d1
církevní	církevní	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
metropolitou	metropolita	k1gMnSc7
buď	buď	k8xC
arcibiskup	arcibiskup	k1gMnSc1
pražský	pražský	k2eAgMnSc1d1
nebo	nebo	k8xC
prešovský	prešovský	k2eAgMnSc1d1
<g/>
;	;	kIx,
metropolitu	metropolita	k1gMnSc4
volí	volit	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
správní	správní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
místní	místní	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
je	být	k5eAaImIp3nS
sněm	sněm	k1gInSc1
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
ze	z	k7c2
zvolených	zvolený	k2eAgMnPc2d1
duchovních	duchovní	k1gMnPc2
i	i	k8xC
laiků	laik	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nauka	nauka	k1gFnSc1
církve	církev	k1gFnSc2
</s>
<s>
Trojjediný	trojjediný	k2eAgMnSc1d1
Bůh	bůh	k1gMnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
Nicejsko-konstantinopolské	Nicejsko-konstantinopolský	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
a	a	k8xC
Filioque	Filioque	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Starozákonní	starozákonní	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
<g/>
,	,	kIx,
Andrej	Andrej	k1gMnSc1
Rublev	Rublev	k1gFnSc2
(	(	kIx(
<g/>
vytvořeno	vytvořit	k5eAaPmNgNnS
mezi	mezi	k7c7
lety	let	k1gInPc7
1408	#num#	k4
<g/>
–	–	k?
<g/>
1425	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Novozákonní	novozákonní	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
ikony	ikona	k1gFnSc2
</s>
<s>
Pravoslavní	pravoslavný	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
sdílejí	sdílet	k5eAaImIp3nP
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
křesťany	křesťan	k1gMnPc7
víru	vír	k1gInSc2
v	v	k7c4
jednoho	jeden	k4xCgMnSc4
Boha	bůh	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
trojjediný	trojjediný	k2eAgMnSc1d1
–	–	k?
Všemohoucího	Všemohoucí	k1gMnSc2
a	a	k8xC
bez	bez	k7c2
počátku	počátek	k1gInSc2
Boha	bůh	k1gMnSc2
Otce	otec	k1gMnSc2
<g/>
,	,	kIx,
Všemohoucího	Všemohoucí	k1gMnSc2
a	a	k8xC
bez	bez	k7c2
počátku	počátek	k1gInSc2
Boha	bůh	k1gMnSc2
Syna	syn	k1gMnSc2
a	a	k8xC
Všemohoucího	Všemohoucí	k1gMnSc2
a	a	k8xC
bez	bez	k7c2
počátku	počátek	k1gInSc2
Boha	bůh	k1gMnSc2
Ducha	duch	k1gMnSc2
Svatého	svatý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pravoslavné	pravoslavný	k2eAgFnSc6d1
teologii	teologie	k1gFnSc6
vztahu	vztah	k1gInSc2
Boha	bůh	k1gMnSc2
a	a	k8xC
světa	svět	k1gInSc2
hraje	hrát	k5eAaImIp3nS
významnou	významný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
pojem	pojem	k1gInSc1
energie	energie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
odlišné	odlišný	k2eAgFnPc1d1
od	od	k7c2
boží	boží	k2eAgFnSc2d1
podstaty	podstata	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
zároveň	zároveň	k6eAd1
nevytvářejí	vytvářet	k5eNaImIp3nP
v	v	k7c6
Bohu	bůh	k1gMnSc3
žádné	žádný	k3yNgNnSc4
rozdělení	rozdělení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tohoto	tento	k3xDgInSc2
pojmu	pojem	k1gInSc2
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
pro	pro	k7c4
vysvětlení	vysvětlení	k1gNnSc4
současné	současný	k2eAgFnSc2d1
boží	boží	k2eAgFnSc2d1
transcendence	transcendence	k1gFnSc2
a	a	k8xC
přítomnosti	přítomnost	k1gFnSc2
ve	v	k7c6
světě	svět	k1gInSc6
a	a	k8xC
způsobu	způsob	k1gInSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
Boha	bůh	k1gMnSc4
vnímat	vnímat	k5eAaImF
a	a	k8xC
zakoušet	zakoušet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdrojem	zdroj	k1gInSc7
Božství	božství	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Božské	božský	k2eAgFnSc6d1
Trojici	trojice	k1gFnSc6
Bůh	bůh	k1gMnSc1
Otec	otec	k1gMnSc1
<g/>
;	;	kIx,
z	z	k7c2
Otce	otec	k1gMnSc2
pochází	pocházet	k5eAaImIp3nS
Bůh	bůh	k1gMnSc1
Syn	syn	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
způsobem	způsob	k1gInSc7
věčného	věčné	k1gNnSc2
rození	rození	k1gNnSc2
<g/>
;	;	kIx,
z	z	k7c2
Otce	otec	k1gMnSc2
pochází	pocházet	k5eAaImIp3nS
i	i	k9
Duch	duch	k1gMnSc1
Svatý	svatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
způsobem	způsob	k1gInSc7
věčného	věčné	k1gNnSc2
vycházení	vycházení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavnou	pravoslavný	k2eAgFnSc4d1
nauku	nauka	k1gFnSc4
shrnuje	shrnovat	k5eAaImIp3nS
nicejsko-konstantinopolské	nicejsko-konstantinopolský	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Spása	spása	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Spása	spása	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
pravoslavné	pravoslavný	k2eAgFnSc2d1
víry	víra	k1gFnSc2
byl	být	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
původně	původně	k6eAd1
stvořen	stvořen	k2eAgMnSc1d1
dokonalý	dokonalý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
vlastní	vlastní	k2eAgFnSc7d1
vůlí	vůle	k1gFnSc7
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
pro	pro	k7c4
zlo	zlo	k1gNnSc4
a	a	k8xC
neposlušnost	neposlušnost	k1gFnSc4
vůči	vůči	k7c3
Bohu	bůh	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pád	Pád	k1gInSc1
člověka	člověk	k1gMnSc4
odsoudil	odsoudit	k5eAaPmAgInS
ke	k	k7c3
smrti	smrt	k1gFnSc3
<g/>
;	;	kIx,
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
od	od	k7c2
Adama	Adam	k1gMnSc2
až	až	k9
po	po	k7c4
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc2
(	(	kIx(
<g/>
nazývaného	nazývaný	k2eAgMnSc2d1
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
Předchůdce	předchůdce	k1gMnSc1
<g/>
)	)	kIx)
přišli	přijít	k5eAaPmAgMnP
po	po	k7c6
smrti	smrt	k1gFnSc6
do	do	k7c2
místa	místo	k1gNnSc2
odloučení	odloučení	k1gNnSc2
od	od	k7c2
Boha	bůh	k1gMnSc2
(	(	kIx(
<g/>
hádés	hádés	k6eAd1
–	–	k?
podsvětí	podsvětí	k1gNnSc1
<g/>
,	,	kIx,
předpeklí	předpeklí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
Bůh	bůh	k1gMnSc1
Otec	otec	k1gMnSc1
poslal	poslat	k5eAaPmAgMnS
do	do	k7c2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
spasil	spasit	k5eAaPmAgMnS
(	(	kIx(
<g/>
zachránil	zachránit	k5eAaPmAgMnS
<g/>
)	)	kIx)
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
však	však	k9
dokonalý	dokonalý	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
a	a	k8xC
dokonalý	dokonalý	k2eAgMnSc1d1
Bůh	bůh	k1gMnSc1
a	a	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
sebe	sebe	k3xPyFc4
přijal	přijmout	k5eAaPmAgMnS
lidskou	lidský	k2eAgFnSc4d1
přirozenost	přirozenost	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
přirozenost	přirozenost	k1gFnSc1
člověka	člověk	k1gMnSc2
proměněna	proměněn	k2eAgNnPc1d1
a	a	k8xC
člověk	člověk	k1gMnSc1
tak	tak	k8xS,k8xC
skrze	skrze	k?
Ježíše	Ježíš	k1gMnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
účastněn	účastněn	k2eAgInSc1d1
božské	božská	k1gFnSc3
přirozenosti	přirozenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
proměny	proměna	k1gFnSc2
lidské	lidský	k2eAgFnSc2d1
přirozenosti	přirozenost	k1gFnSc2
se	se	k3xPyFc4
zpětně	zpětně	k6eAd1
týká	týkat	k5eAaImIp3nS
i	i	k9
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
zemřeli	zemřít	k5eAaPmAgMnP
před	před	k7c7
Kristem	Kristus	k1gMnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
Kristus	Kristus	k1gMnSc1
po	po	k7c6
své	svůj	k3xOyFgFnSc6
smrti	smrt	k1gFnSc6
a	a	k8xC
pohřbení	pohřbení	k1gNnSc6
duší	duše	k1gFnPc2
sestoupil	sestoupit	k5eAaPmAgInS
do	do	k7c2
předpeklí	předpeklí	k1gNnSc2
a	a	k8xC
osvobozoval	osvobozovat	k5eAaImAgMnS
odtud	odtud	k6eAd1
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
zde	zde	k6eAd1
byli	být	k5eAaImAgMnP
smrtí	smrt	k1gFnSc7
zajati	zajat	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spása	spása	k1gFnSc1
tedy	tedy	k9
označuje	označovat	k5eAaImIp3nS
proces	proces	k1gInSc1
záchrany	záchrana	k1gFnSc2
z	z	k7c2
mučivého	mučivý	k2eAgNnSc2d1
odloučení	odloučení	k1gNnSc2
od	od	k7c2
Boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Zemřelé	zemřelá	k1gFnPc1
poroučí	poroučet	k5eAaImIp3nP
Pravoslaví	pravoslaví	k1gNnSc4
Bohu	bůh	k1gMnSc3
a	a	k8xC
církev	církev	k1gFnSc1
se	se	k3xPyFc4
za	za	k7c4
ně	on	k3xPp3gInPc4
modlí	modlit	k5eAaImIp3nP
<g/>
;	;	kIx,
pravoslavní	pravoslavný	k2eAgMnPc1d1
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nejlepší	dobrý	k2eAgInSc1d3
způsob	způsob	k1gInSc1
života	život	k1gInSc2
je	být	k5eAaImIp3nS
ve	v	k7c6
společenství	společenství	k1gNnSc6
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
ke	k	k7c3
spáse	spása	k1gFnSc3
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
přijímat	přijímat	k5eAaImF
svátostí	svátost	k1gFnSc7
(	(	kIx(
<g/>
svaté	svatý	k2eAgFnSc2d1
Tajiny	tajina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účasti	účast	k1gFnSc2
na	na	k7c6
nebeském	nebeský	k2eAgInSc6d1
životě	život	k1gInSc6
nemůže	moct	k5eNaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
člověk	člověk	k1gMnSc1
sám	sám	k3xTgInSc1
–	–	k?
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dar	dar	k1gInSc1
od	od	k7c2
Boha	bůh	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
chce	chtít	k5eAaImIp3nS
navrátit	navrátit	k5eAaPmF
původní	původní	k2eAgInSc4d1
vztah	vztah	k1gInSc4
s	s	k7c7
lidstvem	lidstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
dar	dar	k1gInSc1
ovšem	ovšem	k9
věřící	věřící	k2eAgMnSc1d1
musí	muset	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
přijmout	přijmout	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
nebe	nebe	k1gNnSc1
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
nikomu	nikdo	k3yNnSc3
vnuceno	vnutit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
cílem	cíl	k1gInSc7
člověka	člověk	k1gMnSc2
je	být	k5eAaImIp3nS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
teologii	teologie	k1gFnSc6
θ	θ	k?
(	(	kIx(
<g/>
theósis	theósis	k1gInSc1
zbožštění	zbožštění	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
západní	západní	k2eAgFnSc6d1
teologii	teologie	k1gFnSc6
pojmu	pojem	k1gInSc2
odpovídá	odpovídat	k5eAaImIp3nS
lat.	lat.	k?
divinisatio	divinisatio	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sjednocení	sjednocení	k1gNnSc1
s	s	k7c7
Bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Tradice	tradice	k1gFnSc1
a	a	k8xC
Písmo	písmo	k1gNnSc1
svaté	svatý	k2eAgFnSc2d1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Tradice	tradice	k1gFnSc1
<g/>
,	,	kIx,
Tradice	tradice	k1gFnSc1
v	v	k7c6
křesťanství	křesťanství	k1gNnSc6
a	a	k8xC
Biblický	biblický	k2eAgInSc1d1
kánon	kánon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kristus	Kristus	k1gMnSc1
Pantokrator	Pantokrator	k1gMnSc1
v	v	k7c6
bělehradském	bělehradský	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
sv.	sv.	kA
Alexandra	Alexandr	k1gMnSc2
Něvského	něvský	k2eAgMnSc2d1
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
protestantství	protestantství	k1gNnSc2
a	a	k8xC
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
katolíci	katolík	k1gMnPc1
považují	považovat	k5eAaImIp3nP
pravoslavní	pravoslavný	k2eAgMnPc1d1
Bibli	bible	k1gFnSc4
za	za	k7c4
ústřední	ústřední	k2eAgFnSc4d1
součást	součást	k1gFnSc4
„	„	k?
<g/>
tradice	tradice	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
pravoslavní	pravoslavný	k2eAgMnPc1d1
však	však	k8xC
nepovažují	považovat	k5eNaImIp3nP
tzv.	tzv.	kA
„	„	k?
<g/>
deuterokanonické	deuterokanonický	k2eAgFnSc2d1
<g/>
“	“	k?
knihy	kniha	k1gFnSc2
za	za	k7c4
stejně	stejně	k6eAd1
posvátné	posvátný	k2eAgNnSc4d1
jako	jako	k8xS,k8xC
kanonické	kanonický	k2eAgNnSc4d1
knihy	kniha	k1gFnPc4
Bible	bible	k1gFnSc2
<g/>
;	;	kIx,
považujeme	považovat	k5eAaImIp1nP
za	za	k7c4
posvátné	posvátný	k2eAgNnSc4d1
<g/>
,	,	kIx,
užitečné	užitečný	k2eAgNnSc4d1
a	a	k8xC
poučné	poučný	k2eAgNnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
nikoliv	nikoliv	k9
za	za	k7c4
inspirované	inspirovaný	k2eAgFnPc4d1
Bohem	bůh	k1gMnSc7
v	v	k7c6
plném	plný	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
toho	ten	k3xDgNnSc2
slova	slovo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradice	tradice	k1gFnSc1
dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
vyznání	vyznání	k1gNnSc4
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
dogmata	dogma	k1gNnPc4
a	a	k8xC
pravidla	pravidlo	k1gNnPc4
(	(	kIx(
<g/>
kánony	kánon	k1gInPc1
<g/>
)	)	kIx)
–	–	k?
ustanovení	ustanovení	k1gNnSc4
sedmi	sedm	k4xCc2
všeobecných	všeobecný	k2eAgInPc2d1
koncilů	koncil	k1gInPc2
a	a	k8xC
ostatních	ostatní	k2eAgInPc2d1
všeobecně	všeobecně	k6eAd1
uznávaných	uznávaný	k2eAgInPc2d1
místních	místní	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
<g/>
,	,	kIx,
díla	dílo	k1gNnSc2
–	–	k?
spisy	spis	k1gInPc1
církevních	církevní	k2eAgMnPc2d1
Otců	otec	k1gMnPc2
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInPc1d1
pravoslavné	pravoslavný	k2eAgInPc1d1
kánony	kánon	k1gInPc1
<g/>
,	,	kIx,
liturgické	liturgický	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
životy	život	k1gInPc1
svatých	svatá	k1gFnPc2
<g/>
,	,	kIx,
ikony	ikona	k1gFnPc4
atd.	atd.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Podle	podle	k7c2
pravoslaví	pravoslaví	k1gNnSc2
Duch	duch	k1gMnSc1
Svatý	svatý	k1gMnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
dějin	dějiny	k1gFnPc2
postupně	postupně	k6eAd1
odhaluje	odhalovat	k5eAaImIp3nS
církvi	církev	k1gFnSc3
další	další	k2eAgFnSc3d1
a	a	k8xC
další	další	k2eAgInPc4d1
projevy	projev	k1gInPc4
pravdy	pravda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
plnosti	plnost	k1gFnSc6
přítomna	přítomen	k2eAgFnSc1d1
v	v	k7c6
Církvi	církev	k1gFnSc6
už	už	k9
od	od	k7c2
svatých	svatý	k2eAgMnPc2d1
apoštolů	apoštol	k1gMnPc2
(	(	kIx(
<g/>
Evangelium	evangelium	k1gNnSc1
je	být	k5eAaImIp3nS
plnost	plnost	k1gFnSc4
pravdy	pravda	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
nelze	lze	k6eNd1
už	už	k6eAd1
nic	nic	k3yNnSc1
nového	nový	k2eAgNnSc2d1
přidat	přidat	k5eAaPmF
<g/>
,	,	kIx,
jen	jen	k9
stále	stále	k6eAd1
hlouběji	hluboko	k6eAd2
a	a	k8xC
nových	nový	k2eAgInPc6d1
úkazech	úkaz	k1gInPc6
ji	on	k3xPp3gFnSc4
poznávat	poznávat	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církev	církev	k1gFnSc1
obhajuje	obhajovat	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
tradici	tradice	k1gFnSc4
výrokem	výrok	k1gInSc7
apoštola	apoštol	k1gMnSc2
Pavla	Pavel	k1gMnSc2
<g/>
:	:	kIx,
</s>
<s>
Nuže	nuže	k9
tedy	tedy	k9
<g/>
,	,	kIx,
bratří	bratřit	k5eAaImIp3nS
<g/>
,	,	kIx,
stůjte	stát	k5eAaImRp2nP
pevně	pevně	k6eAd1
a	a	k8xC
držte	držet	k5eAaImRp2nP
se	se	k3xPyFc4
toho	ten	k3xDgNnSc2
učení	učení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsme	být	k5eAaImIp1nP
vám	vy	k3xPp2nPc3
odevzdali	odevzdat	k5eAaPmAgMnP
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k9
slovem	slovo	k1gNnSc7
nebo	nebo	k8xC
dopisem	dopis	k1gInSc7
(	(	kIx(
<g/>
2	#num#	k4
Sol	sol	k1gInSc1
2,15	2,15	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Písmu	písmo	k1gNnSc6
svatém	svatý	k2eAgMnSc6d1
Nového	Nového	k2eAgInSc2d1
zákona	zákon	k1gInSc2
je	být	k5eAaImIp3nS
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
dokladů	doklad	k1gInPc2
<g/>
,	,	kIx,
že	že	k8xS
vedle	vedle	k7c2
psané	psaný	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
měl	mít	k5eAaImAgInS
Boží	boží	k2eAgInSc1d1
lid	lid	k1gInSc1
i	i	k9
ústně	ústně	k6eAd1
předávanou	předávaný	k2eAgFnSc4d1
nepsanou	psaný	k2eNgFnSc4d1,k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
novozákonní	novozákonní	k2eAgMnSc1d1
vidíme	vidět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
období	období	k1gNnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Církev	církev	k1gFnSc1
neměla	mít	k5eNaImAgFnS
ani	ani	k8xC
Evangelium	evangelium	k1gNnSc1
zapsáno	zapsat	k5eAaPmNgNnS
a	a	k8xC
celá	celý	k2eAgFnSc1d1
křesťanská	křesťanský	k2eAgFnSc1d1
víra	víra	k1gFnSc1
a	a	k8xC
zvěst	zvěst	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
ústní	ústní	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Bohorodička	Bohorodička	k1gFnSc1
<g/>
,	,	kIx,
Bohorodice	bohorodice	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Theotokos	Theotokos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
ikona	ikona	k1gFnSc1
s	s	k7c7
Bohorodičkou	Bohorodička	k1gFnSc7
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Mnoho	mnoho	k4c1
tradic	tradice	k1gFnPc2
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
má	mít	k5eAaImIp3nS
vztah	vztah	k1gInSc1
k	k	k7c3
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
uctívána	uctíván	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Theotokos	Theotokos	k1gInSc1
<g/>
,	,	kIx,
Bohorodička	Bohorodička	k1gFnSc1
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
liturgických	liturgický	k2eAgInPc6d1
a	a	k8xC
teologických	teologický	k2eAgInPc6d1
textech	text	k1gInPc6
je	být	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
Přesvatou	přesvatý	k2eAgFnSc7d1
Bohorodicí	bohorodice	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pravoslaví	pravoslaví	k1gNnSc2
zůstala	zůstat	k5eAaPmAgFnS
i	i	k9
po	po	k7c6
Kristově	Kristův	k2eAgInSc6d1
porodu	porod	k1gInSc6
pannou	panna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavní	pravoslavný	k2eAgMnPc1d1
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Marie	Marie	k1gFnSc1
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
nikdy	nikdy	k6eAd1
nezhřešila	zhřešit	k5eNaPmAgFnS
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
však	však	k9
odmítají	odmítat	k5eAaImIp3nP
katolické	katolický	k2eAgNnSc4d1
dogma	dogma	k1gNnSc4
o	o	k7c6
neposkvrněném	poskvrněný	k2eNgNnSc6d1
početí	početí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
pravoslavní	pravoslavný	k2eAgMnPc1d1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Marie	Marie	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
a	a	k8xC
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
i	i	k9
s	s	k7c7
tělem	tělo	k1gNnSc7
vzata	vzat	k2eAgFnSc1d1
do	do	k7c2
nebe	nebe	k1gNnSc2
<g/>
,	,	kIx,
neztotožňují	ztotožňovat	k5eNaImIp3nP
se	se	k3xPyFc4
s	s	k7c7
podobnou	podobný	k2eAgFnSc7d1
římskokatolickou	římskokatolický	k2eAgFnSc7d1
naukou	nauka	k1gFnSc7
o	o	k7c4
nanebevzetí	nanebevzetí	k1gNnSc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
slaví	slavit	k5eAaImIp3nP
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
svátek	svátek	k1gInSc1
Zesnutí	zesnutí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
známé	známý	k2eAgNnSc1d1
i	i	k9
ze	z	k7c2
západní	západní	k2eAgFnSc2d1
liturgické	liturgický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
pod	pod	k7c7
názvem	název	k1gInSc7
dormitio	dormitio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzkříšení	vzkříšení	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Vzkříšení	vzkříšení	k1gNnSc1
<g/>
#	#	kIx~
<g/>
Křesťanství	křesťanství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vzkříšení	vzkříšení	k1gNnSc1
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
pascha	pascha	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
oslavou	oslava	k1gFnSc7
pravoslavného	pravoslavný	k2eAgInSc2d1
liturgického	liturgický	k2eAgInSc2d1
roku	rok	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
za	za	k7c4
historickou	historický	k2eAgFnSc4d1
událost	událost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	své	k1gNnSc7
vzkříšením	vzkříšení	k1gNnSc7
Ježíš	Ježíš	k1gMnSc1
vysvobodil	vysvobodit	k5eAaPmAgMnS
lidstvo	lidstvo	k1gNnSc4
z	z	k7c2
moci	moc	k1gFnSc2
smrti	smrt	k1gFnSc2
a	a	k8xC
umožnil	umožnit	k5eAaPmAgInS
každému	každý	k3xTgMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měl	mít	k5eAaImAgInS
účast	účast	k1gFnSc4
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
nesmrtelnosti	nesmrtelnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
pravoslavný	pravoslavný	k2eAgInSc1d1
svátek	svátek	k1gInSc1
má	mít	k5eAaImIp3nS
vztah	vztah	k1gInSc4
ke	k	k7c3
vzkříšení	vzkříšení	k1gNnSc3
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
každá	každý	k3xTgFnSc1
neděle	neděle	k1gFnSc1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc7
připomínkou	připomínka	k1gFnSc7
(	(	kIx(
<g/>
proto	proto	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
řečtině	řečtina	k1gFnSc6
neděle	neděle	k1gFnSc2
Κ	Κ	k?
=	=	kIx~
den	den	k1gInSc4
Páně	páně	k2eAgInSc4d1
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
ruštině	ruština	k1gFnSc6
В	В	k?
=	=	kIx~
Vzkříšení	vzkříšení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
svátku	svátek	k1gInSc2
stručně	stručně	k6eAd1
a	a	k8xC
slavnostně	slavnostně	k6eAd1
postihuje	postihovat	k5eAaImIp3nS
paschální	paschální	k2eAgInSc1d1
tropar	tropar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zpívá	zpívat	k5eAaImIp3nS
v	v	k7c6
pravoslavné	pravoslavný	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
od	od	k7c2
Paschy	Pascha	k1gFnSc2
po	po	k7c6
celé	celá	k1gFnSc6
39	#num#	k4
dní	den	k1gInPc2
trvající	trvající	k2eAgFnSc1d1
paschální	paschální	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
:	:	kIx,
Vstal	vstát	k5eAaPmAgInS
z	z	k7c2
mrtvých	mrtvý	k1gMnPc2
Kristus	Kristus	k1gMnSc1
<g/>
,	,	kIx,
smrtí	smrtit	k5eAaImIp3nS
smrt	smrt	k1gFnSc4
překonal	překonat	k5eAaPmAgMnS
a	a	k8xC
jsoucím	jsoucí	k2eAgFnPc3d1
ve	v	k7c6
hrobech	hrob	k1gInPc6
život	život	k1gInSc4
daroval	darovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Svatí	svatý	k1gMnPc1
a	a	k8xC
zemřelí	zemřelý	k1gMnPc1
</s>
<s>
Za	za	k7c4
svaté	svatá	k1gFnPc4
označují	označovat	k5eAaImIp3nP
pravoslavní	pravoslavný	k2eAgMnPc1d1
takové	takový	k3xDgMnPc4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
přesvědčení	přesvědčení	k1gNnSc2
církve	církev	k1gFnSc2
v	v	k7c6
nebi	nebe	k1gNnSc6
a	a	k8xC
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
u	u	k7c2
Boha	bůh	k1gMnSc2
přimlouvat	přimlouvat	k5eAaImF
za	za	k7c2
žijící	žijící	k2eAgFnSc2d1
zde	zde	k6eAd1
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přízvisko	přízvisko	k1gNnSc1
svatý	svatý	k2eAgMnSc1d1
se	se	k3xPyFc4
dává	dávat	k5eAaImIp3nS
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
např.	např.	kA
i	i	k8xC
Adamovi	Adam	k1gMnSc3
a	a	k8xC
Evě	Eva	k1gFnSc3
<g/>
,	,	kIx,
Mojžíšovi	Mojžíš	k1gMnSc6
<g/>
,	,	kIx,
různým	různý	k2eAgMnPc3d1
prorokům	prorok	k1gMnPc3
<g/>
,	,	kIx,
mučedníkům	mučedník	k1gMnPc3
a	a	k8xC
andělům	anděl	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedávno	nedávno	k6eAd1
začal	začít	k5eAaPmAgMnS
konstantinopolský	konstantinopolský	k2eAgMnSc1d1
patriarcha	patriarcha	k1gMnSc1
podle	podle	k7c2
staré	starý	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
vydávat	vydávat	k5eAaImF,k5eAaPmF
zvláštní	zvláštní	k2eAgInSc4d1
okružní	okružní	k2eAgInSc4d1
list	list	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
církev	církev	k1gFnSc1
uznává	uznávat	k5eAaImIp3nS
lidovou	lidový	k2eAgFnSc4d1
úctu	úcta	k1gFnSc4
ke	k	k7c3
svatým	svatá	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svatého	svatý	k1gMnSc4
je	být	k5eAaImIp3nS
křesťan	křesťan	k1gMnSc1
prohlášen	prohlásit	k5eAaPmNgMnS
při	při	k7c6
bohoslužbě	bohoslužba	k1gFnSc6
zvané	zvaný	k2eAgNnSc1d1
proslavení	proslavení	k1gNnSc1
(	(	kIx(
<g/>
kanonizace	kanonizace	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
jí	on	k3xPp3gFnSc3
se	se	k3xPyFc4
ovšem	ovšem	k9
křesťan	křesťan	k1gMnSc1
svatým	svatý	k1gMnPc3
nestává	stávat	k5eNaImIp3nS
<g/>
,	,	kIx,
církev	církev	k1gFnSc1
ho	on	k3xPp3gNnSc4
pouze	pouze	k6eAd1
za	za	k7c4
svatého	svatý	k1gMnSc4
oficiálně	oficiálně	k6eAd1
uznává	uznávat	k5eAaImIp3nS
a	a	k8xC
tím	ten	k3xDgNnSc7
věřícím	věřící	k2eAgNnSc7d1
doporučuje	doporučovat	k5eAaImIp3nS
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
uctívání	uctívání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Svatí	svatý	k1gMnPc1
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
<g/>
,	,	kIx,
Chanty-Mansijsk	Chanty-Mansijsk	k1gInSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
</s>
<s>
Pro	pro	k7c4
kanonizaci	kanonizace	k1gFnSc4
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
církvi	církev	k1gFnSc6
rozšířilo	rozšířit	k5eAaPmAgNnS
a	a	k8xC
trvalo	trvat	k5eAaImAgNnS
uctívání	uctívání	k1gNnSc4
zesnulé	zesnulý	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
jakožto	jakožto	k8xS
svatého	svatý	k2eAgMnSc2d1
a	a	k8xC
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
znám	znám	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
vykonaný	vykonaný	k2eAgInSc1d1
tímto	tento	k3xDgMnSc7
světcem	světec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svědectvím	svědectví	k1gNnSc7
o	o	k7c6
svatosti	svatost	k1gFnSc6
dotyčného	dotyčný	k2eAgMnSc2d1
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
např.	např.	kA
nerozkládající	rozkládající	k2eNgInPc1d1
se	se	k3xPyFc4
ostatky	ostatek	k1gInPc7
<g/>
,	,	kIx,
či	či	k8xC
myrotočivé	myrotočivý	k2eAgInPc1d1
ostatky	ostatek	k1gInPc1
<g/>
,	,	kIx,
myrotočivá	myrotočivý	k2eAgFnSc1d1
ikona	ikona	k1gFnSc1
(	(	kIx(
<g/>
ke	k	k7c3
kanonizaci	kanonizace	k1gFnSc3
však	však	k9
ostatky	ostatek	k1gInPc1
nejsou	být	k5eNaImIp3nP
nutné	nutný	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
samotné	samotný	k2eAgNnSc1d1
netlející	tlející	k2eNgInPc1d1
ostatky	ostatek	k1gInPc1
nejsou	být	k5eNaImIp3nP
ještě	ještě	k9
důkazem	důkaz	k1gInSc7
svatosti	svatost	k1gFnSc2
a	a	k8xC
není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
dotyčný	dotyčný	k2eAgMnSc1d1
známý	známý	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc7
pravoslavnou	pravoslavný	k2eAgFnSc7d1
vírou	víra	k1gFnSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
tento	tento	k3xDgInSc4
jev	jev	k1gInSc4
nebere	brát	k5eNaImIp3nS
zřetel	zřetel	k1gInSc1
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
totiž	totiž	k9
případy	případ	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
ostatky	ostatek	k1gInPc7
nerozkládají	rozkládat	k5eNaImIp3nP
z	z	k7c2
důvodu	důvod	k1gInSc2
prokletí	prokletí	k1gNnSc4
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Ke	k	k7c3
světcům	světec	k1gMnPc3
se	se	k3xPyFc4
pravoslavní	pravoslavný	k2eAgMnPc1d1
obracejí	obracet	k5eAaImIp3nP
s	s	k7c7
úctou	úcta	k1gFnSc7
a	a	k8xC
prosbou	prosba	k1gFnSc7
o	o	k7c4
přímluvu	přímluva	k1gFnSc4
–	–	k?
modlitbou	modlitba	k1gFnSc7
<g/>
;	;	kIx,
podle	podle	k7c2
pravoslavné	pravoslavný	k2eAgFnSc2d1
víry	víra	k1gFnSc2
svatí	svatý	k2eAgMnPc1d1
v	v	k7c6
nebi	nebe	k1gNnSc6
prosbám	prosba	k1gFnPc3
naslouchají	naslouchat	k5eAaImIp3nP
a	a	k8xC
mohou	moct	k5eAaImIp3nP
pomoci	pomoct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
oltář	oltář	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
se	se	k3xPyFc4
oltářnímu	oltářní	k2eAgInSc3d1
stolu	stol	k1gInSc3
říká	říkat	k5eAaImIp3nS
„	„	k?
<g/>
prestol	prestol	k?
<g/>
“	“	k?
–	–	k?
tj.	tj.	kA
trůn	trůn	k1gInSc4
<g/>
)	)	kIx)
v	v	k7c6
pravoslavném	pravoslavný	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
obsahuje	obsahovat	k5eAaImIp3nS
ostatek	ostatek	k1gInSc1
světce	světec	k1gMnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
mučedníka	mučedník	k1gMnSc2
<g/>
,	,	kIx,
interiér	interiér	k1gInSc1
chrámů	chrám	k1gInPc2
zdobí	zdobit	k5eAaImIp3nS
ikony	ikona	k1gFnPc4
světců	světec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
pravoslavných	pravoslavný	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
se	se	k3xPyFc4
vydá	vydat	k5eAaPmIp3nS
i	i	k9
na	na	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
pouť	pouť	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
uctili	uctít	k5eAaPmAgMnP
svaté	svatý	k2eAgInPc4d1
ostatky	ostatek	k1gInPc4
světce	světec	k1gMnSc2
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
jméno	jméno	k1gNnSc4
nosí	nosit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc4d1
kapitlou	kapitlý	k2eAgFnSc4d1
tzv.	tzv.	kA
světců	světec	k1gMnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
jurodiví	jurodivý	k2eAgMnPc1d1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Svaté	svatý	k2eAgFnPc1d1
tajiny	tajina	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Svátost	svátost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ikonostas	ikonostas	k1gInSc1
v	v	k7c6
katedrále	katedrála	k1gFnSc6
Zvěstování	zvěstování	k1gNnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
ohledech	ohled	k1gInPc6
je	být	k5eAaImIp3nS
pravoslavný	pravoslavný	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c6
svaté	svatá	k1gFnSc6
Tajiny	tajina	k1gFnSc2
podobný	podobný	k2eAgInSc1d1
římskokatolickému	římskokatolický	k2eAgNnSc3d1
pojímání	pojímání	k1gNnSc3
svátostí	svátost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
by	by	kYmCp3nS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
zdát	zdát	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
Pravoslaví	pravoslaví	k1gNnSc1
uznává	uznávat	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k9
římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
sedm	sedm	k4xCc4
svátostí	svátost	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
ale	ale	k8xC
tento	tento	k3xDgInSc1
počet	počet	k1gInSc1
není	být	k5eNaImIp3nS
pevně	pevně	k6eAd1
určen	určit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavná	pravoslavný	k2eAgFnSc1d1
Církev	církev	k1gFnSc1
téměř	téměř	k6eAd1
nikdy	nikdy	k6eAd1
nemluví	mluvit	k5eNaImIp3nS
o	o	k7c6
svátostech	svátost	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
označuje	označovat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
raději	rád	k6eAd2
jako	jako	k9
svaté	svatý	k2eAgFnSc2d1
tajiny	tajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
„	„	k?
<g/>
Tajina	tajina	k1gFnSc1
<g/>
“	“	k?
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
řečtiny	řečtina	k1gFnSc2
(	(	kIx(
<g/>
mysterium	mysterium	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
akt	akt	k1gInSc4
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yIgInSc3,k3yRgInSc3
nelze	lze	k6eNd1
porozumět	porozumět	k5eAaPmF
skrze	skrze	k?
analytické	analytický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
<g/>
,	,	kIx,
přesahuje	přesahovat	k5eAaImIp3nS
obecné	obecný	k2eAgNnSc1d1
lidské	lidský	k2eAgNnSc1d1
chápání	chápání	k1gNnSc1
–	–	k?
jediný	jediný	k2eAgInSc1d1
možný	možný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
porozumění	porozumění	k1gNnSc2
je	být	k5eAaImIp3nS
skrze	skrze	k?
osobní	osobní	k2eAgFnSc4d1
účast	účast	k1gFnSc4
a	a	k8xC
zkušenost	zkušenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
Svatým	svatý	k2eAgFnPc3d1
Tajinám	tajina	k1gFnPc3
nemají	mít	k5eNaImIp3nP
přístup	přístup	k1gInSc4
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
nejsou	být	k5eNaImIp3nP
pravoslavnými	pravoslavný	k2eAgMnPc7d1
křesťany	křesťan	k1gMnPc7
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
nesměli	smět	k5eNaImAgMnP
lidé	člověk	k1gMnPc1
mimo	mimo	k7c4
církev	církev	k1gFnSc4
konání	konání	k1gNnSc2
svatých	svatý	k2eAgFnPc2d1
Tajin	tajina	k1gFnPc2
ani	ani	k8xC
spatřit	spatřit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
se	se	k3xPyFc4
nás	my	k3xPp1nPc2
dotýká	dotýkat	k5eAaImIp3nS
skrze	skrze	k?
hmotné	hmotný	k2eAgFnPc4d1
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
např.	např.	kA
vodu	voda	k1gFnSc4
<g/>
,	,	kIx,
víno	víno	k1gNnSc4
<g/>
,	,	kIx,
chléb	chléb	k1gInSc1
<g/>
,	,	kIx,
olej	olej	k1gInSc1
<g/>
,	,	kIx,
kadidlo	kadidlo	k1gNnSc1
<g/>
,	,	kIx,
svíce	svíce	k1gFnSc1
<g/>
,	,	kIx,
oltář	oltář	k1gInSc1
<g/>
,	,	kIx,
ikony	ikona	k1gFnPc1
atd.	atd.	kA
Tajina	tajina	k1gFnSc1
je	být	k5eAaImIp3nS
hlubší	hluboký	k2eAgInSc4d2
způsob	způsob	k1gInSc4
Božího	boží	k2eAgNnSc2d1
působení	působení	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Bůh	bůh	k1gMnSc1
proměňuje	proměňovat	k5eAaImIp3nS
nějakou	nějaký	k3yIgFnSc4
materii	materie	k1gFnSc4
<g/>
,	,	kIx,
posvěcuje	posvěcovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ustanovuje	ustanovovat	k5eAaImIp3nS
k	k	k7c3
církevní	církevní	k2eAgFnSc3d1
službě	služba	k1gFnSc3
a	a	k8xC
proměňuje	proměňovat	k5eAaImIp3nS
(	(	kIx(
<g/>
očišťuje	očišťovat	k5eAaImIp3nS
<g/>
)	)	kIx)
nitro	nitro	k1gNnSc1
člověka	člověk	k1gMnSc2
a	a	k8xC
sjednocuje	sjednocovat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
se	s	k7c7
Sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nejhlubší	hluboký	k2eAgFnSc4d3
Tajinu	tajina	k1gFnSc4
považují	považovat	k5eAaImIp3nP
pravoslavní	pravoslavný	k2eAgMnPc1d1
eucharistii	eucharistie	k1gFnSc4
<g/>
,	,	kIx,
skrz	skrz	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
dochází	docházet	k5eAaImIp3nP
k	k	k7c3
bezprostřednímu	bezprostřední	k2eAgNnSc3d1
společenství	společenství	k1gNnSc3
s	s	k7c7
Bohem	bůh	k1gMnSc7
a	a	k8xC
k	k	k7c3
průniku	průnik	k1gInSc3
Božího	boží	k2eAgNnSc2d1
Království	království	k1gNnSc2
s	s	k7c7
naším	náš	k3xOp1gInSc7
světem	svět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Křest	křest	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Křest	křest	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Křtem	křest	k1gInSc7
se	se	k3xPyFc4
člověku	člověk	k1gMnSc3
odpouštějí	odpouštět	k5eAaImIp3nP
hříchy	hřích	k1gInPc4
(	(	kIx(
<g/>
prarodičovský	prarodičovský	k2eAgInSc4d1
hřích	hřích	k1gInSc4
i	i	k8xC
jeho	jeho	k3xOp3gInPc4
osobní	osobní	k2eAgInPc4d1
hříchy	hřích	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
začleněn	začlenit	k5eAaPmNgInS
do	do	k7c2
mystického	mystický	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
Kristova	Kristův	k2eAgFnSc1d1
<g/>
,	,	kIx,
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
členem	člen	k1gInSc7
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křest	křest	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
provádí	provádět	k5eAaImIp3nS
ponořením	ponoření	k1gNnSc7
třikrát	třikrát	k6eAd1
ve	v	k7c6
jménu	jméno	k1gNnSc6
tří	tři	k4xCgFnPc2
Osob	osoba	k1gFnPc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
;	;	kIx,
třikrát	třikrát	k6eAd1
na	na	k7c4
památku	památka	k1gFnSc4
třídenního	třídenní	k2eAgNnSc2d1
Kristova	Kristův	k2eAgNnSc2d1
pohřbení	pohřbení	k1gNnSc2
a	a	k8xC
Jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
následného	následný	k2eAgNnSc2d1
vzkříšení	vzkříšení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křest	křest	k1gInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
smrt	smrt	k1gFnSc4
starého	starý	k2eAgMnSc2d1
člověka	člověk	k1gMnSc2
účastí	účast	k1gFnPc2
na	na	k7c6
ukřižování	ukřižování	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
Kristově	kristově	k6eAd1
a	a	k8xC
znovuzrozením	znovuzrození	k1gNnSc7
do	do	k7c2
nového	nový	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c4
Kristu	Krista	k1gFnSc4
účastí	účast	k1gFnSc7
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
vzkříšení	vzkříšení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
křtí	křtít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
děti	dítě	k1gFnPc4
krátce	krátce	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konvertité	konvertita	k1gMnPc1
k	k	k7c3
pravoslaví	pravoslaví	k1gNnPc1
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
křesťanských	křesťanský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
jsou	být	k5eAaImIp3nP
přijímáni	přijímat	k5eAaImNgMnP
na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
přicházejí	přicházet	k5eAaImIp3nP
<g/>
,	,	kIx,
buď	buď	k8xC
křtem	křest	k1gInSc7
nebo	nebo	k8xC
myropomazáním	myropomazání	k1gNnSc7
(	(	kIx(
<g/>
sjednocení	sjednocení	k1gNnSc1
pouhým	pouhý	k2eAgNnSc7d1
vyznáním	vyznání	k1gNnSc7
pravoslavné	pravoslavný	k2eAgFnSc2d1
víry	víra	k1gFnSc2
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
v	v	k7c6
části	část	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
už	už	k6eAd1
prakticky	prakticky	k6eAd1
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
;	;	kIx,
řada	řada	k1gFnSc1
světových	světový	k2eAgFnPc2d1
pravoslavných	pravoslavný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
přijímá	přijímat	k5eAaImIp3nS
konvertity	konvertita	k1gMnPc4
pouze	pouze	k6eAd1
skrze	skrze	k?
pravoslavný	pravoslavný	k2eAgInSc1d1
křest	křest	k1gInSc1
<g/>
:	:	kIx,
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Athos	Athos	k1gInSc1
<g/>
,	,	kIx,
Jeruzalém	Jeruzalém	k1gInSc1
a	a	k8xC
nově	nově	k6eAd1
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
výrazem	výraz	k1gInSc7
teologického	teologický	k2eAgNnSc2d1
ekleziologického	ekleziologický	k2eAgNnSc2d1
zhodnocení	zhodnocení	k1gNnSc2
vzdálenosti	vzdálenost	k1gFnSc2
mezi	mezi	k7c7
pravoslavím	pravoslaví	k1gNnSc7
a	a	k8xC
římským	římský	k2eAgInSc7d1
katolicismem	katolicismus	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Myropomazání	Myropomazání	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Biřmování	biřmování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Myropomazání	Myropomazání	k1gNnSc1
(	(	kIx(
<g/>
jemuž	jenž	k3xRgInSc3
v	v	k7c6
západní	západní	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
odpovídá	odpovídat	k5eAaImIp3nS
biřmování	biřmování	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tajina	tajina	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
pokřtěnému	pokřtěný	k2eAgMnSc3d1
darován	darován	k2eAgInSc4d1
dar	dar	k1gInSc4
Ducha	duch	k1gMnSc2
Svatého	svatý	k1gMnSc2
pomazáním	pomazání	k1gNnSc7
svatým	svatý	k2eAgNnSc7d1
myrem	myrum	k1gNnSc7
(	(	kIx(
<g/>
pravoslavný	pravoslavný	k2eAgInSc4d1
termín	termín	k1gInSc4
pro	pro	k7c4
křižmo	křižmo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pravoslavné	pravoslavný	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
koná	konat	k5eAaImIp3nS
Tajinu	tajina	k1gFnSc4
myropomazání	myropomazání	k1gNnSc6
každý	každý	k3xTgMnSc1
kněz	kněz	k1gMnSc1
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
římskokatolické	římskokatolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
biřmování	biřmování	k1gNnSc1
může	moct	k5eAaImIp3nS
vykonat	vykonat	k5eAaPmF
jenom	jenom	k9
biskup	biskup	k1gMnSc1
<g/>
;	;	kIx,
pravoslavný	pravoslavný	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
však	však	k9
svaté	svatý	k1gMnPc4
myro	myro	k6eAd1
nikdy	nikdy	k6eAd1
sám	sám	k3xTgMnSc1
nepřipravuje	připravovat	k5eNaImIp3nS
ani	ani	k8xC
nesvětí	světit	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
dostává	dostávat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
od	od	k7c2
biskupa	biskup	k1gMnSc2
již	již	k6eAd1
posvěcené	posvěcený	k2eAgInPc1d1
(	(	kIx(
<g/>
myro	myro	k6eAd1
může	moct	k5eAaImIp3nS
světit	světit	k5eAaImF
jenom	jenom	k9
hlavní	hlavní	k2eAgMnSc1d1
představitel	představitel	k1gMnSc1
místní	místní	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
je	být	k5eAaImIp3nS
světí	světit	k5eAaImIp3nS
jenom	jenom	k9
patriarchové	patriarcha	k1gMnPc1
a	a	k8xC
rozdávají	rozdávat	k5eAaImIp3nP
je	být	k5eAaImIp3nS
ostatním	ostatní	k2eAgMnPc3d1
biskupům	biskup	k1gMnPc3
a	a	k8xC
představitelům	představitel	k1gMnPc3
menších	malý	k2eAgFnPc2d2
pravoslavných	pravoslavný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myropomazání	Myropomazání	k1gNnSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
hned	hned	k6eAd1
po	po	k7c6
křtu	křest	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
u	u	k7c2
malých	malý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokřtěný	pokřtěný	k2eAgInSc4d1
a	a	k8xC
myrem	myrem	k6eAd1
pomazaný	pomazaný	k2eAgMnSc1d1
pravoslavný	pravoslavný	k2eAgMnSc1d1
křesťan	křesťan	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
plným	plný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
církve	církev	k1gFnSc2
a	a	k8xC
smí	smět	k5eAaImIp3nS
se	se	k3xPyFc4
účastnit	účastnit	k5eAaImF
nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
věk	věk	k1gInSc4
eucharistie	eucharistie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Božská	božský	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Božská	božský	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
a	a	k8xC
Eucharistie	eucharistie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Analoj	Analoj	k1gInSc1
(	(	kIx(
<g/>
analogion	analogion	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podstavec	podstavec	k1gInSc1
pro	pro	k7c4
knihy	kniha	k1gFnPc4
při	při	k7c6
čtení	čtení	k1gNnSc6
či	či	k8xC
zpěv	zpěv	k1gInSc1
žalmů	žalm	k1gInPc2
</s>
<s>
Procesí	procesí	k1gNnSc1
ruských	ruský	k2eAgMnPc2d1
pravoslavných	pravoslavný	k2eAgMnPc2d1
</s>
<s>
Eucharistie	eucharistie	k1gFnSc1
je	být	k5eAaImIp3nS
středem	střed	k1gInSc7
pravoslavné	pravoslavný	k2eAgFnSc2d1
liturgie	liturgie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavní	pravoslavný	k2eAgMnPc1d1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
katolíci	katolík	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
chléb	chléb	k1gInSc1
a	a	k8xC
víno	víno	k1gNnSc1
posvěcené	posvěcený	k2eAgNnSc1d1
při	při	k7c6
božské	božský	k2eAgFnSc6d1
liturgii	liturgie	k1gFnSc6
jsou	být	k5eAaImIp3nP
proměněny	proměněn	k2eAgInPc1d1
v	v	k7c4
tělo	tělo	k1gNnSc4
a	a	k8xC
krev	krev	k1gFnSc4
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
;	;	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
katolíků	katolík	k1gMnPc2
se	se	k3xPyFc4
ovšem	ovšem	k9
nesnaží	snažit	k5eNaImIp3nS
popsat	popsat	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
dochází	docházet	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
považují	považovat	k5eAaImIp3nP
to	ten	k3xDgNnSc4
za	za	k7c2
lidskému	lidský	k2eAgInSc3d1
rozumu	rozum	k1gInSc3
nepřístupné	přístupný	k2eNgNnSc1d1
tajemství	tajemství	k1gNnSc1
<g/>
;	;	kIx,
věří	věřit	k5eAaImIp3nS
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
Eucharistie	eucharistie	k1gFnSc1
je	být	k5eAaImIp3nS
pravým	pravý	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
a	a	k8xC
skutečnou	skutečný	k2eAgFnSc7d1
krví	krev	k1gFnSc7
Kristovou	Kristův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učení	učení	k1gNnSc1
o	o	k7c6
transsubstanciaci	transsubstanciace	k1gFnSc6
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
až	až	k9
po	po	k7c6
schizmatu	schizma	k1gNnSc6
mezi	mezi	k7c7
východní	východní	k2eAgFnSc7d1
a	a	k8xC
západní	západní	k2eAgFnSc7d1
církví	církev	k1gFnSc7
a	a	k8xC
pravoslaví	pravoslaví	k1gNnSc1
je	být	k5eAaImIp3nS
nikdy	nikdy	k6eAd1
formálně	formálně	k6eAd1
nepřijalo	přijmout	k5eNaPmAgNnS
ani	ani	k8xC
neodmítlo	odmítnout	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účast	účast	k1gFnSc1
na	na	k7c4
eucharistii	eucharistie	k1gFnSc4
smějí	smát	k5eAaImIp3nP
mít	mít	k5eAaImF
pouze	pouze	k6eAd1
pokřtění	pokřtění	k1gNnSc4
a	a	k8xC
myrem	myrem	k6eAd1
pomazaní	pomazaný	k2eAgMnPc1d1
pravoslavní	pravoslavný	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
připravili	připravit	k5eAaPmAgMnP
postem	post	k1gInSc7
<g/>
,	,	kIx,
modlitbou	modlitba	k1gFnSc7
a	a	k8xC
zpovědí	zpověď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijímání	přijímání	k1gNnPc4
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
přijetí	přijetí	k1gNnSc2
těla	tělo	k1gNnSc2
Kristova	Kristův	k2eAgNnSc2d1
a	a	k8xC
krve	krev	k1gFnSc2
Kristovy	Kristův	k2eAgFnSc2d1
(	(	kIx(
<g/>
přijímání	přijímání	k1gNnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
zásadně	zásadně	k6eAd1
pod	pod	k7c7
obojí	oboj	k1gFnSc7
způsobou	způsoba	k1gFnSc7
<g/>
)	)	kIx)
–	–	k?
podává	podávat	k5eAaImIp3nS
je	on	k3xPp3gFnPc4
kněz	kněz	k1gMnSc1
na	na	k7c6
lžičce	lžička	k1gFnSc6
přímo	přímo	k6eAd1
do	do	k7c2
úst	ústa	k1gNnPc2
přijímajícího	přijímající	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chléb	chléb	k1gInSc1
k	k	k7c3
eucharistii	eucharistie	k1gFnSc3
se	se	k3xPyFc4
namáčí	namáčet	k5eAaImIp3nS
do	do	k7c2
vína	víno	k1gNnSc2
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
západní	západní	k2eAgFnSc2d1
církve	církev	k1gFnSc2
jedině	jedině	k6eAd1
kvašený	kvašený	k2eAgInSc1d1
(	(	kIx(
<g/>
artos	artos	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokřtěné	pokřtěný	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
pravidelně	pravidelně	k6eAd1
přijímají	přijímat	k5eAaImIp3nP
už	už	k6eAd1
od	od	k7c2
nejútlejšího	útlý	k2eAgInSc2d3
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zpověď	zpověď	k1gFnSc1
–	–	k?
tajina	tajina	k1gFnSc1
Pokání	pokání	k1gNnPc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Svátost	svátost	k1gFnSc4
smíření	smíření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hřích	hřích	k1gInSc1
odděluje	oddělovat	k5eAaImIp3nS
člověka	člověk	k1gMnSc4
od	od	k7c2
Boha	bůh	k1gMnSc2
i	i	k9
od	od	k7c2
Církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
bývá	bývat	k5eAaImIp3nS
pokání	pokání	k1gNnSc1
nazýváno	nazývat	k5eAaImNgNnS
druhým	druhý	k4xOgNnSc7
křtem	křest	k1gInSc7
slzami	slza	k1gFnPc7
(	(	kIx(
<g/>
sv.	sv.	kA
Efrém	Efré	k1gNnSc6
Syrský	syrský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
vrací	vracet	k5eAaImIp3nS
zpět	zpět	k6eAd1
k	k	k7c3
Bohu	bůh	k1gMnSc3
a	a	k8xC
k	k	k7c3
Církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
modlitbě	modlitba	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
kněz	kněz	k1gMnSc1
čte	číst	k5eAaImIp3nS
nad	nad	k7c7
kajícníkem	kajícník	k1gMnSc7
a	a	k8xC
uděluje	udělovat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
tím	ten	k3xDgNnSc7
rozhřešení	rozhřešení	k1gNnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
k	k	k7c3
Bohu	bůh	k1gMnSc3
obrací	obracet	k5eAaImIp3nS
mj.	mj.	kA
s	s	k7c7
těmito	tento	k3xDgNnPc7
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
„	„	k?
<g/>
…	…	k?
<g/>
daruj	darovat	k5eAaPmRp2nS
mu	on	k3xPp3gMnSc3
pravé	pravý	k2eAgNnSc1d1
pokání	pokání	k1gNnSc1
<g/>
,	,	kIx,
odpuštění	odpuštění	k1gNnSc1
a	a	k8xC
prominutí	prominutí	k1gNnSc1
hříchů	hřích	k1gInPc2
<g/>
…	…	k?
<g/>
,	,	kIx,
smiř	smířit	k5eAaPmRp2nS
ho	on	k3xPp3gMnSc4
a	a	k8xC
sjednoť	sjednotit	k5eAaPmRp2nS
se	se	k3xPyFc4
svatou	svatý	k2eAgFnSc7d1
Církví	církev	k1gFnSc7
svou	svůj	k3xOyFgFnSc4
<g/>
…	…	k?
<g/>
“	“	k?
</s>
<s>
Pravoslavní	pravoslavný	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
spáchali	spáchat	k5eAaPmAgMnP
hříchy	hřích	k1gInPc4
(	(	kIx(
<g/>
a	a	k8xC
každý	každý	k3xTgMnSc1
se	se	k3xPyFc4
dopouští	dopouštět	k5eAaImIp3nS
hříchů	hřích	k1gInPc2
–	–	k?
úmyslných	úmyslný	k2eAgMnPc2d1
i	i	k8xC
neúmyslných	úmyslný	k2eNgMnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
kají	kát	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
pevné	pevný	k2eAgNnSc4d1
odhodlání	odhodlání	k1gNnSc4
se	se	k3xPyFc4
napravit	napravit	k5eAaPmF
a	a	k8xC
touží	toužit	k5eAaImIp3nS
znovu	znovu	k6eAd1
se	se	k3xPyFc4
sjednotit	sjednotit	k5eAaPmF
s	s	k7c7
Církví	církev	k1gFnSc7
s	s	k7c7
Bohem	bůh	k1gMnSc7
a	a	k8xC
obnovit	obnovit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
<g/>
,	,	kIx,
vyznávají	vyznávat	k5eAaImIp3nP
při	při	k7c6
obřadu	obřad	k1gInSc6
svaté	svatý	k2eAgFnSc2d1
Tajiny	tajina	k1gFnSc2
pokání	pokání	k1gNnSc6
své	svůj	k3xOyFgInPc4
hříchy	hřích	k1gInPc4
Bohu	bůh	k1gMnSc3
před	před	k7c7
ikonou	ikona	k1gFnSc7
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
a	a	k8xC
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
kněze	kněz	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
modlí	modlit	k5eAaImIp3nS
za	za	k7c4
Boží	boží	k2eAgNnSc4d1
milosrdenství	milosrdenství	k1gNnSc4
a	a	k8xC
potvrzuje	potvrzovat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
požehnáním	požehnání	k1gNnSc7
a	a	k8xC
udělením	udělení	k1gNnSc7
rozhřešení	rozhřešení	k1gNnSc2
všech	všecek	k3xTgInPc2
hříchů	hřích	k1gInPc2
úmyslných	úmyslný	k2eAgInPc2d1
i	i	k8xC
neúmyslných	úmyslný	k2eNgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
zpověď	zpověď	k1gFnSc4
je	být	k5eAaImIp3nS
často	často	k6eAd1
také	také	k6eAd1
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
k	k	k7c3
žádosti	žádost	k1gFnSc3
o	o	k7c4
duchovní	duchovní	k2eAgFnSc4d1
radu	rada	k1gFnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
bývá	bývat	k5eAaImIp3nS
součástí	součást	k1gFnSc7
tajiny	tajina	k1gFnSc2
pokání	pokání	k1gNnSc2
i	i	k9
duchovní	duchovní	k2eAgInSc1d1
rozhovor	rozhovor	k1gInSc1
mezi	mezi	k7c7
kajícníkem	kajícník	k1gMnSc7
a	a	k8xC
zpovědníkem	zpovědník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslaví	pravoslaví	k1gNnSc1
nehledí	hledět	k5eNaImIp3nS
na	na	k7c4
hřích	hřích	k1gInSc4
jako	jako	k8xS,k8xC
na	na	k7c4
právní	právní	k2eAgInSc4d1
prohřešek	prohřešek	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
nějž	jenž	k3xRgMnSc4
musí	muset	k5eAaImIp3nS
následovat	následovat	k5eAaImF
trest	trest	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
jako	jako	k9
na	na	k7c4
nemoc	nemoc	k1gFnSc4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
vyléčit	vyléčit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nějaká	nějaký	k3yIgFnSc1
forma	forma	k1gFnSc1
udělení	udělení	k1gNnSc2
pokání	pokání	k1gNnSc2
(	(	kIx(
<g/>
epitimie	epitimie	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
proto	proto	k6eAd1
má	mít	k5eAaImIp3nS
udělovat	udělovat	k5eAaImF
vždy	vždy	k6eAd1
<g/>
,	,	kIx,
zváží	zvážit	k5eAaPmIp3nS
<g/>
-li	-li	k?
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
toho	ten	k3xDgNnSc2
zapotřebí	zapotřebí	k6eAd1
(	(	kIx(
<g/>
bývá	bývat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
čtení	čtení	k1gNnSc1
modlitebních	modlitební	k2eAgInPc2d1
kánonů	kánon	k1gInPc2
<g/>
,	,	kIx,
konání	konání	k1gNnSc2
poklon	poklona	k1gFnPc2
<g/>
,	,	kIx,
mimořádný	mimořádný	k2eAgInSc4d1
půst	půst	k1gInSc4
a	a	k8xC
ve	v	k7c6
vážnějších	vážní	k2eAgInPc6d2
případech	případ	k1gInPc6
dočasné	dočasný	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
od	od	k7c2
eucharistie	eucharistie	k1gFnSc2
doprovázené	doprovázený	k2eAgNnSc1d1
intenzivní	intenzivní	k2eAgFnSc7d1
modlitbou	modlitba	k1gFnSc7
a	a	k8xC
postem	post	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Manželství	manželství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
svatba	svatba	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
pravoslavných	pravoslavný	k2eAgInPc2d1
je	on	k3xPp3gMnPc4
manželství	manželství	k1gNnSc1
Božím	božit	k5eAaImIp1nS
aktem	akt	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Bůh	bůh	k1gMnSc1
dva	dva	k4xCgInPc4
spojuje	spojovat	k5eAaImIp3nS
v	v	k7c4
jediného	jediný	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plození	plození	k1gNnSc1
dětí	dítě	k1gFnPc2
není	být	k5eNaImIp3nS
jediným	jediný	k2eAgInSc7d1
smyslem	smysl	k1gInSc7
manželství	manželství	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
spojení	spojení	k1gNnSc1
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnSc2
je	být	k5eAaImIp3nS
chápáno	chápat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
odraz	odraz	k1gInSc1
sjednocení	sjednocení	k1gNnSc2
s	s	k7c7
Bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavná	pravoslavný	k2eAgFnSc1d1
svatba	svatba	k1gFnSc1
má	mít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
dvě	dva	k4xCgFnPc1
části	část	k1gFnPc1
–	–	k?
výměnu	výměna	k1gFnSc4
prstenů	prsten	k1gInPc2
a	a	k8xC
korunovaci	korunovace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
západnímu	západní	k2eAgNnSc3d1
pojetí	pojetí	k1gNnSc3
svatby	svatba	k1gFnSc2
netvoří	tvořit	k5eNaImIp3nP
součást	součást	k1gFnSc4
pravoslavného	pravoslavný	k2eAgNnSc2d1
uzavírání	uzavírání	k1gNnSc2
manželství	manželství	k1gNnSc2
výměna	výměna	k1gFnSc1
manželského	manželský	k2eAgInSc2d1
slibu	slib	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tajina	tajina	k1gFnSc1
svěcení	svěcení	k1gNnSc2
kněžstva	kněžstvo	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Svěcení	svěcení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pravoslavný	pravoslavný	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
oblečený	oblečený	k2eAgMnSc1d1
do	do	k7c2
liturgického	liturgický	k2eAgInSc2d1
oděvu	oděv	k1gInSc2
byzantského	byzantský	k2eAgInSc2d1
ritu	rit	k1gInSc2
</s>
<s>
Pravoslaví	pravoslaví	k1gNnSc1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
další	další	k2eAgFnPc1d1
církve	církev	k1gFnPc1
liturgické	liturgický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
uznává	uznávat	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
úrovně	úroveň	k1gFnPc4
kněžského	kněžský	k2eAgNnSc2d1
svěcení	svěcení	k1gNnSc2
(	(	kIx(
<g/>
čili	čili	k8xC
chirotonie	chirotonie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgInPc4
kněžské	kněžský	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
církevních	církevní	k2eAgFnPc2d1
představených	představená	k1gFnPc2
–	–	k?
biskupové	biskup	k1gMnPc1
(	(	kIx(
<g/>
čili	čili	k8xC
episkopové	episkop	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kněží	kněz	k1gMnPc1
(	(	kIx(
<g/>
presbyteři	presbyter	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
diákoni	diákon	k1gMnPc1
(	(	kIx(
<g/>
jáhnové	jáhen	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
těchto	tento	k3xDgNnPc2
svěcení	svěcení	k1gNnPc2
existují	existovat	k5eAaImIp3nP
další	další	k2eAgInPc1d1
nižší	nízký	k2eAgInPc1d2
stupně	stupeň	k1gInPc1
duchovenstva	duchovenstvo	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
nichž	jenž	k3xRgMnPc2
je	být	k5eAaImIp3nS
uváděno	uvádět	k5eAaImNgNnS
nikoliv	nikoliv	k9
svěcením	svěcení	k1gNnSc7
ale	ale	k8xC
postřižením	postřižení	k1gNnSc7
(	(	kIx(
<g/>
chirotesií	chirotesie	k1gFnSc7
<g/>
)	)	kIx)
(	(	kIx(
<g/>
zpěváci	zpěvák	k1gMnPc1
<g/>
,	,	kIx,
žalmisté	žalmista	k1gMnPc1
<g/>
,	,	kIx,
hypodiakoni	hypodiakon	k1gMnPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavní	pravoslavný	k2eAgMnPc1d1
kněží	kněz	k1gMnPc1
a	a	k8xC
diákoni	diákon	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
před	před	k7c7
svěcením	svěcení	k1gNnSc7
oženit	oženit	k5eAaPmF
<g/>
;	;	kIx,
po	po	k7c6
vysvěcení	vysvěcení	k1gNnSc6
se	se	k3xPyFc4
duchovní	duchovní	k1gMnSc1
již	již	k6eAd1
oženit	oženit	k5eAaPmF
nemůže	moct	k5eNaImIp3nS
<g/>
;	;	kIx,
na	na	k7c4
biskupa	biskup	k1gMnSc4
však	však	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vysvěcen	vysvětit	k5eAaPmNgInS
pouze	pouze	k6eAd1
mnich	mnich	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pomazání	pomazání	k1gNnSc1
svatým	svatý	k2eAgInSc7d1
olejem	olej	k1gInSc7
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Svátost	svátost	k1gFnSc4
nemocných	nemocný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Pomazání	pomazání	k1gNnSc1
nemocných	nemocný	k1gMnPc2
se	se	k3xPyFc4
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
uděluje	udělovat	k5eAaImIp3nS
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
potřebují	potřebovat	k5eAaImIp3nP
duchovní	duchovní	k2eAgFnPc1d1
nebo	nebo	k8xC
tělesné	tělesný	k2eAgNnSc1d1
uzdravení	uzdravení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
uděluje	udělovat	k5eAaImIp3nS
na	na	k7c4
Čistou	čistý	k2eAgFnSc4d1
středu	středa	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
tzv.	tzv.	kA
„	„	k?
<g/>
čistém	čistý	k2eAgInSc6d1
týdnu	týden	k1gInSc6
<g/>
“	“	k?
–	–	k?
tj.	tj.	kA
prvním	první	k4xOgInSc6
týdnu	týden	k1gInSc6
Velikého	veliký	k2eAgInSc2d1
půstu	půst	k1gInSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
Velkou	velký	k2eAgFnSc4d1
středu	středa	k1gFnSc4
(	(	kIx(
<g/>
ve	v	k7c6
strastném	strastný	k2eAgMnSc6d1
<g/>
,	,	kIx,
čili	čili	k8xC
pašjiovém	pašjiový	k2eAgInSc6d1
týdnu	týden	k1gInSc6
před	před	k7c7
Paschou	Pascha	k1gFnSc7
<g/>
)	)	kIx)
všem	všecek	k3xTgFnPc3
věřícím	věřící	k1gFnPc3
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
uděluje	udělovat	k5eAaImIp3nS
o	o	k7c6
velkých	velký	k2eAgInPc6d1
svátcích	svátek	k1gInPc6
i	i	k8xC
o	o	k7c6
jiných	jiný	k2eAgFnPc6d1
příležitostech	příležitost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správně	správně	k6eAd1
má	mít	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
tajinu	tajina	k1gFnSc4
udělovat	udělovat	k5eAaImF
sedm	sedm	k4xCc4
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgFnSc1d1
zbožnost	zbožnost	k1gFnSc1
</s>
<s>
Jejími	její	k3xOp3gInPc7
hlavními	hlavní	k2eAgInPc7d1
znaky	znak	k1gInPc7
jsou	být	k5eAaImIp3nP
konzervatismus	konzervatismus	k1gInSc4
a	a	k8xC
tradice	tradice	k1gFnPc4
<g/>
,	,	kIx,
individualismus	individualismus	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
přímý	přímý	k2eAgInSc4d1
<g/>
,	,	kIx,
subjektivní	subjektivní	k2eAgInSc4d1
vztah	vztah	k1gInSc4
k	k	k7c3
trojičnímu	trojiční	k2eAgNnSc3d1
božstvu	božstvo	k1gNnSc3
<g/>
,	,	kIx,
o	o	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
věřící	věřící	k1gMnPc1
usilují	usilovat	k5eAaImIp3nP
v	v	k7c6
různých	různý	k2eAgFnPc6d1
záhadách	záhada	k1gFnPc6
a	a	k8xC
především	především	k6eAd1
prostřednictvím	prostřednictvím	k7c2
kontemplace	kontemplace	k1gFnSc2
–	–	k?
tj.	tj.	kA
rozjímáním	rozjímání	k1gNnSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc7
prostřednictvím	prostřednictví	k1gNnSc7
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
pravoslavný	pravoslavný	k2eAgMnSc1d1
věřící	věřící	k1gMnSc1
o	o	k7c4
navázání	navázání	k1gNnSc4
kontaktu	kontakt	k1gInSc2
s	s	k7c7
Bohem	bůh	k1gMnSc7
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
nitru	nitro	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Východní	východní	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
je	být	k5eAaImIp3nS
emotivní	emotivní	k2eAgInSc1d1
a	a	k8xC
spekulativní	spekulativní	k2eAgInSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
západní	západní	k2eAgFnSc1d1
má	mít	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
racionální	racionální	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Liturgická	liturgický	k2eAgFnSc1d1
praxe	praxe	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Liturgie	liturgie	k1gFnSc1
a	a	k8xC
Liturgický	liturgický	k2eAgInSc1d1
rok	rok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Interiér	interiér	k1gInSc1
pravoslavného	pravoslavný	k2eAgInSc2d1
kostela	kostel	k1gInSc2
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
bohoslužby	bohoslužba	k1gFnPc4
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
podle	podle	k7c2
východních	východní	k2eAgInPc2d1
ritů	rit	k1gInPc2
(	(	kIx(
<g/>
obřadů	obřad	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovanské	slovanský	k2eAgInPc1d1
a	a	k8xC
řecké	řecký	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
používají	používat	k5eAaImIp3nP
byzantský	byzantský	k2eAgInSc4d1
ritus	ritus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
otázce	otázka	k1gFnSc6
obřadů	obřad	k1gInPc2
je	být	k5eAaImIp3nS
pravoslaví	pravoslaví	k1gNnSc1
značně	značně	k6eAd1
konzervativní	konzervativní	k2eAgMnSc1d1
a	a	k8xC
nejčastěji	často	k6eAd3
užívaná	užívaný	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Zlatoústého	zlatoústý	k2eAgMnSc2d1
prošla	projít	k5eAaPmAgFnS
od	od	k7c2
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jen	jen	k9
malými	malý	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
katolickou	katolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
bývají	bývat	k5eAaImIp3nP
pravoslavné	pravoslavný	k2eAgFnPc1d1
bohoslužby	bohoslužba	k1gFnPc1
delší	dlouhý	k2eAgFnPc1d2
<g/>
,	,	kIx,
nepoužívají	používat	k5eNaImIp3nP
se	se	k3xPyFc4
při	při	k7c6
nich	on	k3xPp3gInPc6
hudební	hudební	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
a	a	k8xC
kněz	kněz	k1gMnSc1
většinu	většina	k1gFnSc4
bohoslužby	bohoslužba	k1gFnSc2
hledí	hledět	k5eAaImIp3nS
stejným	stejný	k2eAgInSc7d1
směrem	směr	k1gInSc7
jako	jako	k8xC,k8xS
lid	lid	k1gInSc1
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
tedy	tedy	k9
k	k	k7c3
němu	on	k3xPp3gNnSc3
obrácen	obrácen	k2eAgInSc1d1
zády	záda	k1gNnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
užívání	užívání	k1gNnSc4
východních	východní	k2eAgInPc2d1
ritů	rit	k1gInPc2
při	při	k7c6
svých	svůj	k3xOyFgFnPc6
bohoslužbách	bohoslužba	k1gFnPc6
<g/>
,	,	kIx,
tuto	tento	k3xDgFnSc4
otázku	otázka	k1gFnSc4
tedy	tedy	k9
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
vztahu	vztah	k1gInSc6
k	k	k7c3
pravoslaví	pravoslaví	k1gNnSc3
nevnímá	vnímat	k5eNaImIp3nS
jako	jako	k9
překážku	překážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
současný	současný	k2eAgInSc4d1
západní	západní	k2eAgInSc4d1
ritus	ritus	k1gInSc4
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Většinu	většina	k1gFnSc4
dní	den	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
konat	konat	k5eAaImF
Božská	božský	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Zlatoústého	zlatoústý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desetkrát	desetkrát	k6eAd1
do	do	k7c2
roka	rok	k1gInSc2
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
Božská	božský	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
sv.	sv.	kA
Basila	Basila	k1gFnSc1
Velikého	veliký	k2eAgMnSc2d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
starobylejší	starobylý	k2eAgInSc4d2
a	a	k8xC
delší	dlouhý	k2eAgInSc4d2
než	než	k8xS
předchozí	předchozí	k2eAgInSc4d1
druh	druh	k1gInSc4
liturgie	liturgie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
liturgie	liturgie	k1gFnSc1
se	se	k3xPyFc4
buď	buď	k8xC
slouží	sloužit	k5eAaImIp3nS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
variantách	varianta	k1gFnPc6
<g/>
:	:	kIx,
jako	jako	k8xS,k8xC
večerní	večerní	k2eAgFnSc1d1
bohoslužba	bohoslužba	k1gFnSc1
nebo	nebo	k8xC
jako	jako	k9
bohoslužba	bohoslužba	k1gFnSc1
jitřní	jitřní	k2eAgFnSc1d1
(	(	kIx(
<g/>
někde	někde	k6eAd1
se	se	k3xPyFc4
však	však	k9
večerní	večerní	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
v	v	k7c6
praxi	praxe	k1gFnSc6
stejně	stejně	k6eAd1
slouží	sloužit	k5eAaImIp3nS
dopoledne	dopoledne	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dny	den	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
předepsáno	předepsán	k2eAgNnSc1d1
konat	konat	k5eAaImF
liturgii	liturgie	k1gFnSc4
sv.	sv.	kA
Basila	Basila	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
předvečer	předvečer	k1gInSc4
svátku	svátek	k1gInSc2
Narození	narození	k1gNnSc3
Páně	páně	k2eAgNnSc2d1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
/	/	kIx~
<g/>
večerní	večerní	k2eAgFnSc1d1
<g/>
/	/	kIx~
liturgie	liturgie	k1gFnSc1
sv.	sv.	kA
Basila	Basila	k1gFnSc1
(	(	kIx(
<g/>
nepřipadne	připadnout	k5eNaPmIp3nS
<g/>
-li	-li	k?
však	však	k9
den	den	k1gInSc4
před	před	k7c7
svátkem	svátek	k1gInSc7
Narození	narozený	k2eAgMnPc1d1
na	na	k7c4
sobotu	sobota	k1gFnSc4
nebo	nebo	k8xC
na	na	k7c4
neděli	neděle	k1gFnSc4
<g/>
;	;	kIx,
v	v	k7c6
takových	takový	k3xDgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
liturgie	liturgie	k1gFnSc1
sv.	sv.	kA
Basila	Basila	k1gFnSc1
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
v	v	k7c4
den	den	k1gInSc4
samotného	samotný	k2eAgInSc2d1
svátku	svátek	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
svátek	svátek	k1gInSc1
sv.	sv.	kA
Basila	Basil	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
a	a	k8xC
svátek	svátek	k1gInSc1
Obřezání	obřezání	k1gNnSc2
Páně	páně	k2eAgNnSc2d1
<g/>
)	)	kIx)
</s>
<s>
předvečer	předvečer	k1gInSc1
svátku	svátek	k1gInSc2
Zjevení	zjevení	k1gNnSc4
Páně	páně	k2eAgFnSc2d1
(	(	kIx(
<g/>
Theofanie	Theofanie	k1gFnSc2
<g/>
,	,	kIx,
Bogojavlenije	Bogojavlenije	k1gFnSc1
<g/>
,	,	kIx,
Křest	křest	k1gInSc1
Páně	páně	k2eAgInSc1d1
<g/>
)	)	kIx)
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
/	/	kIx~
<g/>
večerní	večerní	k2eAgFnSc1d1
<g/>
/	/	kIx~
liturgie	liturgie	k1gFnSc1
sv.	sv.	kA
Basila	Basila	k1gFnSc1
(	(	kIx(
<g/>
nepřipadne	připadnout	k5eNaPmIp3nS
<g/>
-li	-li	k?
však	však	k9
den	den	k1gInSc4
před	před	k7c7
svátkem	svátek	k1gInSc7
Zjevení	zjevení	k1gNnSc2
Páně	páně	k2eAgFnSc2d1
na	na	k7c4
sobotu	sobota	k1gFnSc4
nebo	nebo	k8xC
na	na	k7c4
neděli	neděle	k1gFnSc4
<g/>
;	;	kIx,
v	v	k7c6
takových	takový	k3xDgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
liturgie	liturgie	k1gFnSc1
sv.	sv.	kA
Basila	Basila	k1gFnSc1
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
v	v	k7c4
den	den	k1gInSc4
samotného	samotný	k2eAgInSc2d1
svátku	svátek	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgFnSc1
neděle	neděle	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
půstu	půst	k1gInSc2
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
tzv.	tzv.	kA
neděle	neděle	k1gFnSc2
oslavy	oslava	k1gFnSc2
Pravoslaví	pravoslaví	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
neděle	neděle	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
půstu	půst	k1gInSc2
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
sv.	sv.	kA
Řehoře	Řehoř	k1gMnSc2
Palamy	Palam	k1gInPc5
<g/>
)	)	kIx)
</s>
<s>
Třetí	třetí	k4xOgFnSc1
neděle	neděle	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
půstu	půst	k1gInSc2
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
uctívání	uctívání	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
neděle	neděle	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
půstu	půst	k1gInSc2
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
ct	ct	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jana	Jan	k1gMnSc2
Klimaka	Klimak	k1gMnSc2
–	–	k?
Sinajského	sinajský	k2eAgMnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Pátá	pátý	k4xOgFnSc1
neděle	neděle	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
půstu	půst	k1gInSc2
/	/	kIx~
<g/>
ráno	ráno	k6eAd1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
ct	ct	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Egyptské	egyptský	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Velký	velký	k2eAgInSc4d1
čtvrtek	čtvrtek	k1gInSc4
/	/	kIx~
<g/>
večerní	večerní	k2eAgInSc4d1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
čtvrtek	čtvrtek	k1gInSc4
před	před	k7c7
Paschou	Pascha	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Veliká	veliký	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
/	/	kIx~
<g/>
večerní	večerní	k2eAgFnSc1d1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
sobota	sobota	k1gFnSc1
před	před	k7c7
paschální	paschální	k2eAgFnSc7d1
nedělí	neděle	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c4
všední	všední	k2eAgInPc4d1
dny	den	k1gInPc4
Velkého	velký	k2eAgInSc2d1
půstu	půst	k1gInSc2
se	se	k3xPyFc4
nekoná	konat	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
z	z	k7c2
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
typů	typ	k1gInPc2
liturgie	liturgie	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
podle	podle	k7c2
tradice	tradice	k1gFnSc2
v	v	k7c4
tyto	tento	k3xDgInPc4
dny	den	k1gInPc4
nekoná	konat	k5eNaImIp3nS
proměňování	proměňování	k1gNnSc1
chleba	chléb	k1gInSc2
a	a	k8xC
vína	víno	k1gNnSc2
na	na	k7c4
Tělo	tělo	k1gNnSc4
a	a	k8xC
Krev	krev	k1gFnSc4
Kristovy	Kristův	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
dary	dar	k1gInPc1
jsou	být	k5eAaImIp3nP
proměňovány	proměňovat	k5eAaImNgInP
v	v	k7c4
předcházející	předcházející	k2eAgFnSc4d1
neděli	neděle	k1gFnSc4
a	a	k8xC
následující	následující	k2eAgInPc4d1
velkopostní	velkopostní	k2eAgInPc4d1
všední	všední	k2eAgInPc4d1
dny	den	k1gInPc4
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgFnSc1d1
večerní	večerní	k2eAgFnSc1d1
bohoslužba	bohoslužba	k1gFnSc1
<g/>
:	:	kIx,
liturgie	liturgie	k1gFnSc1
předem	předem	k6eAd1
posvěcených	posvěcený	k2eAgInPc2d1
Darů	dar	k1gInPc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
je	být	k5eAaImIp3nS
k	k	k7c3
přijímání	přijímání	k1gNnSc3
podávána	podáván	k2eAgFnSc1d1
eucharistie	eucharistie	k1gFnSc1
proměněná	proměněný	k2eAgFnSc1d1
v	v	k7c6
neděli	neděle	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
bohoslužba	bohoslužba	k1gFnSc1
má	mít	k5eAaImIp3nS
kající	kající	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
kněz	kněz	k1gMnSc1
ji	on	k3xPp3gFnSc4
vykonává	vykonávat	k5eAaImIp3nS
v	v	k7c6
tmavém	tmavý	k2eAgNnSc6d1
bohoslužebném	bohoslužebný	k2eAgNnSc6d1
rouchu	roucho	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
svátek	svátek	k1gInSc4
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
se	se	k3xPyFc4
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
konává	konávat	k5eAaImIp3nS
nejstarobylejší	starobylý	k2eAgFnSc2d3
liturgie	liturgie	k1gFnSc2
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
<g/>
,	,	kIx,
bratra	bratr	k1gMnSc2
Páně	páně	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
několik	několik	k4yIc4
dnů	den	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
liturgie	liturgie	k1gFnSc1
nekoná	konat	k5eNaImIp3nS
<g/>
;	;	kIx,
za	za	k7c4
všechny	všechen	k3xTgInPc4
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
Velký	velký	k2eAgInSc4d1
pátek	pátek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
tento	tento	k3xDgInSc4
den	den	k1gInSc4
se	se	k3xPyFc4
však	však	k9
slouží	sloužit	k5eAaImIp3nP
jiné	jiný	k2eAgInPc1d1
druhy	druh	k1gInPc1
bohoslužeb	bohoslužba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
liturgii	liturgie	k1gFnSc6
východních	východní	k2eAgInPc2d1
ritů	rit	k1gInPc2
hraje	hrát	k5eAaImIp3nS
církevní	církevní	k2eAgInSc4d1
kalendář	kalendář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liturgický	liturgický	k2eAgInSc1d1
rok	rok	k1gInSc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
dvěma	dva	k4xCgMnPc7
druhy	druh	k1gInPc1
kalendáře	kalendář	k1gInSc2
(	(	kIx(
<g/>
toto	tento	k3xDgNnSc4
rozdělení	rozdělení	k1gNnSc4
nijak	nijak	k6eAd1
nesouvisí	souviset	k5eNaImIp3nS
s	s	k7c7
jiným	jiný	k2eAgNnSc7d1
dělením	dělení	k1gNnSc7
na	na	k7c4
kalendář	kalendář	k1gInSc4
juliánský	juliánský	k2eAgInSc4d1
a	a	k8xC
gregoriánský	gregoriánský	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohyblivý	pohyblivý	k2eAgInSc1d1
kalendář	kalendář	k1gInSc1
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
posunutý	posunutý	k2eAgInSc1d1
jinak	jinak	k6eAd1
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
základ	základ	k1gInSc4
tvoří	tvořit	k5eAaImIp3nP
Velikonoce	Velikonoce	k1gFnPc1
<g/>
;	;	kIx,
určuje	určovat	k5eAaImIp3nS
dobu	doba	k1gFnSc4
Velkého	velký	k2eAgInSc2d1
postu	post	k1gInSc2
<g/>
,	,	kIx,
data	datum	k1gNnPc1
některých	některý	k3yIgInPc2
velkých	velký	k2eAgInPc2d1
svátků	svátek	k1gInPc2
a	a	k8xC
rozložení	rozložení	k1gNnSc6
evangelijních	evangelijní	k2eAgNnPc2d1
čtení	čtení	k1gNnPc2
na	na	k7c4
neděle	neděle	k1gFnPc4
po	po	k7c4
celý	celý	k2eAgInSc4d1
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepohyblivý	pohyblivý	k2eNgInSc1d1
kalendář	kalendář	k1gInSc1
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
stejný	stejný	k2eAgInSc1d1
a	a	k8xC
určuje	určovat	k5eAaImIp3nS
zvláště	zvláště	k6eAd1
datum	datum	k1gNnSc1
svátku	svátek	k1gInSc2
každého	každý	k3xTgMnSc2
světce	světec	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
některé	některý	k3yIgInPc1
velké	velký	k2eAgInPc1d1
svátky	svátek	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
Vánoce	Vánoce	k1gFnPc1
jsou	být	k5eAaImIp3nP
nepohyblivý	pohyblivý	k2eNgInSc4d1
svátek	svátek	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
otázce	otázka	k1gFnSc6
užívání	užívání	k1gNnSc2
kalendáře	kalendář	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
jednotlivé	jednotlivý	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
rozděleny	rozdělen	k2eAgFnPc1d1
na	na	k7c4
dva	dva	k4xCgInPc4
tábory	tábor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převážná	převážný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
pravoslavných	pravoslavný	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
pouze	pouze	k6eAd1
juliánským	juliánský	k2eAgInSc7d1
kalendářem	kalendář	k1gInSc7
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
kontextu	kontext	k1gInSc6
tzv.	tzv.	kA
„	„	k?
<g/>
církevním	církevní	k2eAgInSc6d1
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
starým	starý	k1gMnSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jiní	jiný	k2eAgMnPc1d1
začali	začít	k5eAaPmAgMnP
jako	jako	k8xC,k8xS
základ	základ	k1gInSc4
nepohyblivého	pohyblivý	k2eNgInSc2d1
kalendáře	kalendář	k1gInSc2
používat	používat	k5eAaImF
gregoriánský	gregoriánský	k2eAgInSc4d1
kalendář	kalendář	k1gInSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
občanský	občanský	k2eAgInSc4d1
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
nový	nový	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
na	na	k7c4
výjimky	výjimka	k1gFnPc4
však	však	k9
všichni	všechen	k3xTgMnPc1
pravoslavní	pravoslavný	k2eAgMnPc1d1
od	od	k7c2
juliánského	juliánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
stále	stále	k6eAd1
odvozují	odvozovat	k5eAaImIp3nP
datum	datum	k1gNnSc4
Velikonoc	Velikonoce	k1gFnPc2
(	(	kIx(
<g/>
paschy	pasch	k1gInPc7
<g/>
)	)	kIx)
a	a	k8xC
tedy	tedy	k9
i	i	k9
celý	celý	k2eAgInSc4d1
pohyblivý	pohyblivý	k2eAgInSc4d1
kalendář	kalendář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Výpočtu	výpočet	k1gInSc2
data	datum	k1gNnSc2
Velikonoc	Velikonoce	k1gFnPc2
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
některé	některý	k3yIgInPc1
posvátné	posvátný	k2eAgInPc1d1
kánony	kánon	k1gInPc1
svatých	svatý	k2eAgMnPc2d1
otců	otec	k1gMnPc2
a	a	k8xC
další	další	k2eAgNnPc4d1
ustanovení	ustanovení	k1gNnPc4
církevní	církevní	k2eAgFnPc4d1
tradice	tradice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavné	pravoslavný	k2eAgFnPc4d1
Velikonoce	Velikonoce	k1gFnPc4
nesmí	smět	k5eNaImIp3nS
podle	podle	k7c2
posvátné	posvátný	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
být	být	k5eAaImF
nikdy	nikdy	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
židovským	židovský	k2eAgInSc7d1
svátkem	svátek	k1gInSc7
Pesach	pesach	k1gInSc1
nebo	nebo	k8xC
před	před	k7c7
ním	on	k3xPp3gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Kristus	Kristus	k1gMnSc1
vstal	vstát	k5eAaPmAgMnS
z	z	k7c2
hrobu	hrob	k1gInSc2
až	až	k6eAd1
po	po	k7c6
svátku	svátek	k1gInSc6
Pesach	pesach	k1gInSc1
(	(	kIx(
<g/>
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
žádný	žádný	k3yNgInSc4
antisemitismus	antisemitismus	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikonoce	Velikonoce	k1gFnPc1
v	v	k7c6
římsko-katolické	římsko-katolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
v	v	k7c6
některých	některý	k3yIgNnPc6
letech	léto	k1gNnPc6
nesplňují	splňovat	k5eNaImIp3nP
toto	tento	k3xDgNnSc4
svatootcovské	svatootcovský	k2eAgNnSc4d1
ustanovení	ustanovení	k1gNnSc4
a	a	k8xC
připadají	připadat	k5eAaPmIp3nP,k5eAaImIp3nP
na	na	k7c4
Pesach	pesach	k1gInSc4
nebo	nebo	k8xC
dokonce	dokonce	k9
před	před	k7c4
něj	on	k3xPp3gNnSc4
(	(	kIx(
<g/>
naposled	naposled	k6eAd1
byly	být	k5eAaImAgFnP
Velikonoce	Velikonoce	k1gFnPc1
v	v	k7c6
římsko-katolické	římsko-katolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
před	před	k7c7
svátkem	svátek	k1gInSc7
Pesach	pesach	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgNnSc1d1
mnišství	mnišství	k1gNnSc1
</s>
<s>
Schéma	schéma	k1gNnSc1
(	(	kIx(
<g/>
schima	schima	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
nosí	nosit	k5eAaImIp3nP
pravoslavní	pravoslavný	k2eAgMnPc1d1
mniši	mnich	k1gMnPc1
</s>
<s>
Poustevnické	poustevnický	k2eAgNnSc1d1
a	a	k8xC
mnišské	mnišský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
kořeny	kořen	k1gInPc4
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jeho	jeho	k3xOp3gMnSc7
prvními	první	k4xOgMnPc7
projevy	projev	k1gInPc1
se	se	k3xPyFc4
setkáváme	setkávat	k5eAaImIp1nP
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
vznikly	vzniknout	k5eAaPmAgInP
nejstarší	starý	k2eAgInPc1d3
kláštery	klášter	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odsud	odsud	k6eAd1
se	se	k3xPyFc4
šířilo	šířit	k5eAaImAgNnS
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
Kypr	Kypr	k1gInSc4
a	a	k8xC
také	také	k9
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
dávali	dávat	k5eAaImAgMnP
přednost	přednost	k1gFnSc4
poustevnickým	poustevnický	k2eAgFnPc3d1
formám	forma	k1gFnPc3
asketického	asketický	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jediná	jediné	k1gNnPc1
východní	východní	k2eAgFnSc2d1
řehole	řehole	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
tvůrcem	tvůrce	k1gMnSc7
byl	být	k5eAaImAgMnS
Basileios	Basileios	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
proniklo	proniknout	k5eAaPmAgNnS
mnišské	mnišský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zásluhou	zásluha	k1gFnSc7
císaře	císař	k1gMnSc2
Theodosia	Theodosium	k1gNnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
se	se	k3xPyFc4
tu	tu	k6eAd1
rozšířilo	rozšířit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
existovalo	existovat	k5eAaImAgNnS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
65	#num#	k4
mužských	mužský	k2eAgInPc2d1
klášterů	klášter	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kláštery	klášter	k1gInPc1
východního	východní	k2eAgInSc2d1
ritu	rit	k1gInSc2
měly	mít	k5eAaImAgInP
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
individualistické	individualistický	k2eAgInPc4d1
rysy	rys	k1gInPc4
než	než	k8xS
na	na	k7c6
latinském	latinský	k2eAgInSc6d1
Západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnišstvo	mnišstvo	k1gNnSc1
se	se	k3xPyFc4
neřídilo	řídit	k5eNaImAgNnS
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
basiliánské	basiliánský	k2eAgFnSc2d1
žádnými	žádný	k3yNgFnPc7
řeholemi	řehole	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměření	zaměření	k1gNnSc1
kláštera	klášter	k1gInSc2
určovaly	určovat	k5eAaImAgFnP
buď	buď	k8xC
jeho	jeho	k3xOp3gFnPc1
vedoucí	vedoucí	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
zde	zde	k6eAd1
také	také	k9
udržovaly	udržovat	k5eAaImAgFnP
pořádek	pořádek	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
zakladatelé	zakladatel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavný	pravoslavný	k2eAgInSc1d1
klášter	klášter	k1gInSc1
tak	tak	k6eAd1
představoval	představovat	k5eAaImAgInS
zcela	zcela	k6eAd1
nezávislou	závislý	k2eNgFnSc4d1
jednotku	jednotka	k1gFnSc4
s	s	k7c7
vlastními	vlastní	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
způsobu	způsob	k1gInSc2
života	život	k1gInSc2
mnichů	mnich	k1gMnPc2
a	a	k8xC
mnišek	mniška	k1gFnPc2
se	se	k3xPyFc4
rozlišuje	rozlišovat	k5eAaImIp3nS
několik	několik	k4yIc1
typů	typ	k1gInPc2
pravoslavných	pravoslavný	k2eAgInPc2d1
klášterů	klášter	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koinobion	koinobion	k1gInSc1
představuje	představovat	k5eAaImIp3nS
komunitu	komunita	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
mniši	mnich	k1gMnPc1
žili	žít	k5eAaImAgMnP
společně	společně	k6eAd1
a	a	k8xC
byli	být	k5eAaImAgMnP
podřízeni	podřídit	k5eAaPmNgMnP
představenému	představený	k1gMnSc3
–	–	k?
hegumenovi	hegumen	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
laurách	laurách	k?
měli	mít	k5eAaImAgMnP
oddělené	oddělený	k2eAgFnPc4d1
cely	cela	k1gFnPc4
a	a	k8xC
scházeli	scházet	k5eAaImAgMnP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
týdně	týdně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
neděli	neděle	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
o	o	k7c6
svátcích	svátek	k1gInPc6
ke	k	k7c3
společným	společný	k2eAgFnPc3d1
bohoslužbám	bohoslužba	k1gFnPc3
<g/>
,	,	kIx,
modlitbám	modlitba	k1gFnPc3
a	a	k8xC
jídlu	jídlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mniši	mnich	k1gMnPc1
v	v	k7c6
idiorytmických	idiorytmický	k2eAgInPc6d1
klášterech	klášter	k1gInPc6
se	se	k3xPyFc4
neshromažďovali	shromažďovat	k5eNaImAgMnP
vůbec	vůbec	k9
<g/>
,	,	kIx,
mohli	moct	k5eAaImAgMnP
mít	mít	k5eAaImF
vlastní	vlastní	k2eAgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
vydělávat	vydělávat	k5eAaImF
si	se	k3xPyFc3
na	na	k7c4
živobytí	živobytí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eremité	eremit	k1gMnPc1
–	–	k?
poustevníci	poustevník	k1gMnPc1
žili	žít	k5eAaImAgMnP
na	na	k7c6
osamělých	osamělý	k2eAgNnPc6d1
<g/>
,	,	kIx,
nehostinných	hostinný	k2eNgNnPc6d1
místech	místo	k1gNnPc6
nebo	nebo	k8xC
dávali	dávat	k5eAaImAgMnP
přednost	přednost	k1gFnSc4
nápadným	nápadný	k2eAgInPc3d1
asketickým	asketický	k2eAgInPc3d1
projevům	projev	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Simeon	Simeon	k1gMnSc1
Stylita	stylita	k1gMnSc1
starší	starší	k1gMnSc1
žil	žít	k5eAaImAgMnS
téměř	téměř	k6eAd1
čtyřicet	čtyřicet	k4xCc4
let	léto	k1gNnPc2
na	na	k7c6
sloupu	sloup	k1gInSc6
(	(	kIx(
<g/>
zemřel	zemřít	k5eAaPmAgMnS
jako	jako	k9
sedmdesátiletý	sedmdesátiletý	k2eAgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
byl	být	k5eAaImAgMnS
mnohými	mnohý	k2eAgMnPc7d1
napodobován	napodobován	k2eAgMnSc1d1
(	(	kIx(
<g/>
stylité	stylita	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k1gMnPc1
přebývali	přebývat	k5eAaImAgMnP
například	například	k6eAd1
ve	v	k7c6
starých	stará	k1gFnPc6
<g/>
,	,	kIx,
vykotlaných	vykotlaný	k2eAgInPc6d1
stromech	strom	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
byzantské	byzantský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
existovalo	existovat	k5eAaImAgNnS
mnoho	mnoho	k4c1
potulných	potulný	k2eAgMnPc2d1
mnichů	mnich	k1gMnPc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
synody	synoda	k1gFnSc2
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
mnišského	mnišský	k2eAgInSc2d1
života	život	k1gInSc2
odmítaly	odmítat	k5eAaImAgInP
a	a	k8xC
potíraly	potírat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
původním	původní	k2eAgInPc3d1
požadavkům	požadavek	k1gInPc3
asketického	asketický	k2eAgInSc2d1
života	život	k1gInSc2
byzantské	byzantský	k2eAgInPc1d1
kláštery	klášter	k1gInPc1
rychle	rychle	k6eAd1
bohatly	bohatnout	k5eAaImAgInP
a	a	k8xC
mniši	mnich	k1gMnPc1
měli	mít	k5eAaImAgMnP
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
jedním	jeden	k4xCgInSc7
z	z	k7c2
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vedly	vést	k5eAaImAgInP
císaře	císař	k1gMnSc2
isaurijské	isaurijský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
k	k	k7c3
nastolení	nastolení	k1gNnSc3
ikonoklasmu	ikonoklasmus	k1gInSc2
(	(	kIx(
<g/>
obrazoborectví	obrazoborectví	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
nedosáhl	dosáhnout	k5eNaPmAgInS
žádný	žádný	k3yNgInSc1
byzantský	byzantský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
takového	takový	k3xDgInSc2
významu	význam	k1gInSc2
jako	jako	k8xS,k8xC
západní	západní	k2eAgNnSc4d1
opatství	opatství	k1gNnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c4
Cluny	Cluna	k1gFnPc4
nebo	nebo	k8xC
Chartres	Chartres	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantské	byzantský	k2eAgInPc1d1
kláštery	klášter	k1gInPc1
také	také	k9
nevyvíjely	vyvíjet	k5eNaImAgFnP
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
kulturní	kulturní	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
<g/>
,	,	kIx,
srovnatelné	srovnatelný	k2eAgInPc1d1
se	s	k7c7
Západem	západ	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
vzdělanost	vzdělanost	k1gFnSc1
nebyla	být	k5eNaImAgFnS
v	v	k7c6
Byzanci	Byzanc	k1gFnSc6
výsadou	výsada	k1gFnSc7
duchovních	duchovní	k1gMnPc2
<g/>
,	,	kIx,
natož	natož	k6eAd1
mnichů	mnich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimku	výjimek	k1gInSc2
tvořil	tvořit	k5eAaImAgInS
například	například	k6eAd1
konstantinopolský	konstantinopolský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
Studios	Studios	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
existovalo	existovat	k5eAaImAgNnS
skriptorium	skriptorium	k1gNnSc1
i	i	k8xC
knihovna	knihovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
mnišské	mnišský	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
spekulativní	spekulativní	k2eAgNnSc1d1
a	a	k8xC
vykazovalo	vykazovat	k5eAaImAgNnS
větší	veliký	k2eAgInSc4d2
odvrat	odvrat	k1gInSc4
od	od	k7c2
světských	světský	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mniši	mnich	k1gMnPc1
se	se	k3xPyFc4
nejméně	málo	k6eAd3
půl	půl	k1xP
dne	den	k1gInSc2
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
modlitbám	modlitba	k1gFnPc3
<g/>
,	,	kIx,
jimiž	jenž	k3xRgMnPc7
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
dosáhnout	dosáhnout	k5eAaPmF
stavu	stav	k1gInSc3
odpoutání	odpoutání	k1gNnSc2
od	od	k7c2
vnějšího	vnější	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgMnSc2
theóse	theós	k1gMnSc2
–	–	k?
zbožtění	zbožtění	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vztah	vztah	k1gInSc1
k	k	k7c3
ostatním	ostatní	k2eAgFnPc3d1
církvím	církev	k1gFnPc3
</s>
<s>
Mezi	mezi	k7c7
pravoslavím	pravoslaví	k1gNnSc7
a	a	k8xC
ostatními	ostatní	k2eAgFnPc7d1
křesťanskými	křesťanský	k2eAgFnPc7d1
církvemi	církev	k1gFnPc7
leží	ležet	k5eAaImIp3nP
mnoho	mnoho	k4c4
podstatných	podstatný	k2eAgInPc2d1
věroučných	věroučný	k2eAgInPc2d1
rozdílů	rozdíl	k1gInPc2
<g/>
,	,	kIx,
odlišnosti	odlišnost	k1gFnPc1
v	v	k7c6
církevní	církevní	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
<g/>
,	,	kIx,
některé	některý	k3yIgInPc4
neslučitelné	slučitelný	k2eNgInPc4d1
církevní	církevní	k2eAgInPc4d1
obyčeje	obyčej	k1gInPc4
a	a	k8xC
hlavně	hlavně	k9
rozdílnost	rozdílnost	k1gFnSc1
ve	v	k7c6
spiritualitě	spiritualita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dialog	dialog	k1gInSc1
s	s	k7c7
katolickou	katolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
navíc	navíc	k6eAd1
komplikují	komplikovat	k5eAaBmIp3nP
překážky	překážka	k1gFnPc1
politického	politický	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
dialogu	dialog	k1gInSc6
s	s	k7c7
církvemi	církev	k1gFnPc7
vzešlými	vzešlý	k2eAgFnPc7d1
z	z	k7c2
protestantské	protestantský	k2eAgFnSc2d1
reformace	reformace	k1gFnSc2
nacházíme	nacházet	k5eAaImIp1nP
ještě	ještě	k6eAd1
výraznější	výrazný	k2eAgInPc1d2
rozdíly	rozdíl	k1gInPc1
jak	jak	k8xC,k8xS
ve	v	k7c6
věrouce	věrouka	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
chápání	chápání	k1gNnSc6
svátostí	svátost	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
v	v	k7c6
pojetí	pojetí	k1gNnSc6
církevní	církevní	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
ke	k	k7c3
katolické	katolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
</s>
<s>
S	s	k7c7
katolickou	katolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
spojuje	spojovat	k5eAaImIp3nS
pravoslaví	pravoslaví	k1gNnSc1
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
mnoho	mnoho	k6eAd1
–	–	k?
koncept	koncept	k1gInSc4
apoštolské	apoštolský	k2eAgFnSc2d1
posloupnosti	posloupnost	k1gFnSc2
<g/>
,	,	kIx,
svátosti	svátost	k1gFnSc2
<g/>
,	,	kIx,
hierarchické	hierarchický	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
pohled	pohled	k1gInSc4
na	na	k7c4
tradici	tradice	k1gFnSc4
a	a	k8xC
spojitost	spojitost	k1gFnSc4
s	s	k7c7
církví	církev	k1gFnSc7
apoštolů	apoštol	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulturní	kulturní	k2eAgInPc1d1
a	a	k8xC
teologické	teologický	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
východní	východní	k2eAgFnSc7d1
a	a	k8xC
západní	západní	k2eAgFnSc7d1
církví	církev	k1gFnSc7
existovaly	existovat	k5eAaImAgFnP
prakticky	prakticky	k6eAd1
od	od	k7c2
samého	samý	k3xTgInSc2
počátku	počátek	k1gInSc2
křesťanství	křesťanství	k1gNnSc2
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
zde	zde	k6eAd1
k	k	k7c3
výraznému	výrazný	k2eAgInSc3d1
historickému	historický	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslaví	pravoslaví	k1gNnSc1
pohlíží	pohlížet	k5eAaImIp3nS
na	na	k7c4
církevní	církevní	k2eAgNnSc4d1
učení	učení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
teologii	teologie	k1gFnSc6
objevilo	objevit	k5eAaPmAgNnS
s	s	k7c7
příchodem	příchod	k1gInSc7
středověku	středověk	k1gInSc2
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
na	na	k7c4
filioque	filioque	k1gFnSc4
a	a	k8xC
papežský	papežský	k2eAgInSc4d1
primát	primát	k1gInSc4
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
na	na	k7c4
novoty	novota	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
vedly	vést	k5eAaImAgFnP
k	k	k7c3
rozbití	rozbití	k1gNnSc3
společenství	společenství	k1gNnSc2
mezi	mezi	k7c7
církvemi	církev	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpor	rozpor	k1gInSc1
mezi	mezi	k7c7
církvemi	církev	k1gFnPc7
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
i	i	k9
u	u	k7c2
katolických	katolický	k2eAgNnPc2d1
dogmat	dogma	k1gNnPc2
přijatých	přijatý	k2eAgNnPc2d1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
;	;	kIx,
závěry	závěra	k1gFnPc4
Druhého	druhý	k4xOgInSc2
vatikánského	vatikánský	k2eAgInSc2d1
koncilu	koncil	k1gInSc2
přiblížily	přiblížit	k5eAaPmAgFnP
v	v	k7c6
některých	některý	k3yIgInPc6
detailech	detail	k1gInPc6
římskokatolickou	římskokatolický	k2eAgFnSc4d1
církev	církev	k1gFnSc4
k	k	k7c3
pravoslaví	pravoslaví	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
zásadního	zásadní	k2eAgNnSc2d1
nezměnily	změnit	k5eNaPmAgFnP
nic	nic	k3yNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
církve	církev	k1gFnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
o	o	k7c4
vzájemný	vzájemný	k2eAgInSc4d1
dialog	dialog	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
obnovení	obnovení	k1gNnSc2
církevního	církevní	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mnoha	mnoho	k4c6
letech	léto	k1gNnPc6
nebylo	být	k5eNaImAgNnS
však	však	k9
dosaženo	dosáhnout	k5eAaPmNgNnS
žádného	žádný	k3yNgInSc2
průlomu	průlom	k1gInSc2
v	v	k7c6
zásadních	zásadní	k2eAgInPc6d1
věroučných	věroučný	k2eAgInPc6d1
rozdílech	rozdíl	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
po	po	k7c6
velkém	velký	k2eAgInSc6d1
rozkolu	rozkol	k1gInSc6
(	(	kIx(
<g/>
1054	#num#	k4
<g/>
)	)	kIx)
k	k	k7c3
vývoji	vývoj	k1gInSc3
ve	v	k7c6
věrouce	věrouka	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
projevil	projevit	k5eAaPmAgInS
formulováním	formulování	k1gNnSc7
nových	nový	k2eAgNnPc2d1
dogmat	dogma	k1gNnPc2
(	(	kIx(
<g/>
definitivní	definitivní	k2eAgNnSc1d1
zařazení	zařazení	k1gNnSc1
vsuvky	vsuvka	k1gFnSc2
„	„	k?
<g/>
filioque	filioqu	k1gFnSc2
<g/>
“	“	k?
do	do	k7c2
vyznání	vyznání	k1gNnSc2
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
primát	primát	k1gMnSc1
římského	římský	k2eAgMnSc2d1
papeže	papež	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
není	být	k5eNaImIp3nS
chápán	chápat	k5eAaImNgMnS
jen	jen	k9
jako	jako	k9
čestné	čestný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k9
jeho	jeho	k3xOp3gFnSc4
pravomoc	pravomoc	k1gFnSc4
nad	nad	k7c7
celou	celý	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
;	;	kIx,
učení	učení	k1gNnSc1
o	o	k7c6
odpustcích	odpustek	k1gInPc6
<g/>
,	,	kIx,
očistci	očistec	k1gInSc3
<g/>
;	;	kIx,
později	pozdě	k6eAd2
i	i	k9
dogma	dogma	k1gNnSc4
o	o	k7c6
neomylnosti	neomylnost	k1gFnSc6
papežské	papežský	k2eAgNnSc1d1
<g/>
;	;	kIx,
dogma	dogma	k1gNnSc1
o	o	k7c6
neposkvrněném	poskvrněný	k2eNgNnSc6d1
početí	početí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
dále	daleko	k6eAd2
byly	být	k5eAaImAgInP
na	na	k7c6
západě	západ	k1gInSc6
postupně	postupně	k6eAd1
přijaty	přijat	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
zvyky	zvyk	k1gInPc1
a	a	k8xC
změněna	změněn	k2eAgFnSc1d1
církevní	církevní	k2eAgFnSc1d1
praxe	praxe	k1gFnSc1
(	(	kIx(
<g/>
používání	používání	k1gNnSc1
nekvašeného	kvašený	k2eNgInSc2d1
chleba	chléb	k1gInSc2
pro	pro	k7c4
eucharistii	eucharistie	k1gFnSc4
<g/>
,	,	kIx,
povinný	povinný	k2eAgInSc4d1
celibát	celibát	k1gInSc4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
půst	půst	k1gInSc4
v	v	k7c4
pátek	pátek	k1gInSc4
<g/>
,	,	kIx,
uctívání	uctívání	k1gNnSc1
soch	socha	k1gFnPc2
–	–	k?
mnohé	mnohé	k1gNnSc4
z	z	k7c2
toho	ten	k3xDgInSc2
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
před	před	k7c7
tím	ten	k3xDgNnSc7
na	na	k7c6
církevních	církevní	k2eAgInPc6d1
sněmech	sněm	k1gInPc6
kanonicky	kanonicky	k6eAd1
výslovně	výslovně	k6eAd1
zakázáno	zakázat	k5eAaPmNgNnS
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
Písmem	písmo	k1gNnSc7
svatým	svatý	k2eAgNnSc7d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
–	–	k?
např.	např.	kA
nekvašený	kvašený	k2eNgInSc4d1
chléb	chléb	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
„	„	k?
<g/>
obřadových	obřadový	k2eAgFnPc6d1
odlišnostech	odlišnost	k1gFnPc6
<g/>
“	“	k?
obou	dva	k4xCgFnPc2
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
nelze	lze	k6eNd1
mít	mít	k5eAaImF
na	na	k7c6
mysli	mysl	k1gFnSc6
jen	jen	k9
rozdílnost	rozdílnost	k1gFnSc4
Byzantského	byzantský	k2eAgInSc2d1
ritu	rit	k1gInSc2
od	od	k7c2
západního	západní	k2eAgInSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
výklad	výklad	k1gInSc1
<g/>
,	,	kIx,
chápání	chápání	k1gNnSc1
ritu	rit	k1gInSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
např.	např.	kA
rozdílné	rozdílný	k2eAgInPc1d1
názory	názor	k1gInPc1
na	na	k7c4
proměnění	proměnění	k1gNnSc4
svatých	svatý	k2eAgInPc2d1
Darů	dar	k1gInPc2
při	při	k7c6
konání	konání	k1gNnSc6
Eucharistie	eucharistie	k1gFnSc2
<g/>
,	,	kIx,
epiklesi	epiklese	k1gFnSc3
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nepodstatný	podstatný	k2eNgInSc1d1
není	být	k5eNaImIp3nS
ani	ani	k9
vstup	vstup	k1gInSc1
papeže	papež	k1gMnSc2
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
světské	světský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
(	(	kIx(
<g/>
zabývat	zabývat	k5eAaImF
se	s	k7c7
světskou	světský	k2eAgFnSc7d1
politikou	politika	k1gFnSc7
bylo	být	k5eAaImAgNnS
už	už	k9
dříve	dříve	k6eAd2
kánony	kánon	k1gInPc4
svatých	svatý	k2eAgMnPc2d1
Otců	otec	k1gMnPc2
pro	pro	k7c4
duchovní	duchovní	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
zakázáno	zakázat	k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
tato	tento	k3xDgNnPc4
nová	nový	k2eAgNnPc4d1
dogmata	dogma	k1gNnPc4
a	a	k8xC
změny	změna	k1gFnPc4
v	v	k7c6
církevní	církevní	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
pravoslavné	pravoslavný	k2eAgMnPc4d1
křesťany	křesťan	k1gMnPc4
nepřijatelné	přijatelný	k2eNgMnPc4d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
vytvářejí	vytvářet	k5eAaImIp3nP
překážku	překážka	k1gFnSc4
pro	pro	k7c4
sjednocení	sjednocení	k1gNnSc4
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
s	s	k7c7
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
vývojem	vývoj	k1gInSc7
na	na	k7c6
západě	západ	k1gInSc6
se	se	k3xPyFc4
rozkol	rozkol	k1gInSc1
mezi	mezi	k7c7
západními	západní	k2eAgMnPc7d1
křesťany	křesťan	k1gMnPc7
a	a	k8xC
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církví	církev	k1gFnSc7
zafixoval	zafixovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pravoslavné	pravoslavný	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
jsou	být	k5eAaImIp3nP
tato	tento	k3xDgNnPc4
nová	nový	k2eAgNnPc4d1
římská	římský	k2eAgNnPc4d1
dogmata	dogma	k1gNnPc4
a	a	k8xC
zavedené	zavedený	k2eAgFnPc4d1
odchylky	odchylka	k1gFnPc4
od	od	k7c2
tradiční	tradiční	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
odchýlením	odchýlení	k1gNnSc7
od	od	k7c2
původní	původní	k2eAgFnSc2d1
křesťanské	křesťanský	k2eAgFnSc2d1
víry	víra	k1gFnSc2
a	a	k8xC
autentické	autentický	k2eAgFnPc1d1
církevní	církevní	k2eAgFnPc1d1
praxe	praxe	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1054	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velkému	velký	k2eAgNnSc3d1
schizmatu	schizma	k1gNnSc3
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
bezprostřední	bezprostřední	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
byl	být	k5eAaImAgInS
spor	spor	k1gInSc1
o	o	k7c4
primát	primát	k1gInSc4
římského	římský	k2eAgMnSc2d1
papeže	papež	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papežští	papežský	k2eAgMnPc1d1
legáti	legát	k1gMnPc1
uvalili	uvalit	k5eAaPmAgMnP
klatbu	klatba	k1gFnSc4
na	na	k7c4
konstantinopolského	konstantinopolský	k2eAgMnSc4d1
patriarchu	patriarcha	k1gMnSc4
a	a	k8xC
na	na	k7c4
všechny	všechen	k3xTgMnPc4
<g/>
,	,	kIx,
když	když	k8xS
jsou	být	k5eAaImIp3nP
s	s	k7c7
ním	on	k3xPp3gNnSc7
v	v	k7c6
jednotě	jednota	k1gFnSc6
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
praxi	praxe	k1gFnSc6
znamenalo	znamenat	k5eAaImAgNnS
celý	celý	k2eAgInSc4d1
Východ	východ	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
to	ten	k3xDgNnSc4
odpověděla	odpovědět	k5eAaPmAgFnS
konstantinopolská	konstantinopolský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
Nespravedlivě	spravedlivě	k6eNd1
uvalená	uvalený	k2eAgFnSc1d1
klatba	klatba	k1gFnSc1
ať	ať	k9
dopadne	dopadnout	k5eAaPmIp3nS
na	na	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kdož	kdož	k3yRnSc1
ji	on	k3xPp3gFnSc4
uvalili	uvalit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkol	rozkol	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
v	v	k7c6
praxi	praxe	k1gFnSc6
zpečetěn	zpečetit	k5eAaPmNgMnS
až	až	k9
o	o	k7c4
dvě	dva	k4xCgNnPc4
století	století	k1gNnPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
křižácká	křižácký	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
západních	západní	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
přepadla	přepadnout	k5eAaPmAgFnS
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
,	,	kIx,
zabíjela	zabíjet	k5eAaImAgFnS
východní	východní	k2eAgFnPc4d1
křesťany	křesťan	k1gMnPc7
<g/>
,	,	kIx,
znesvěcovala	znesvěcovat	k5eAaImAgFnS
chrámy	chrám	k1gInPc4
a	a	k8xC
svátosti	svátost	k1gFnPc4
a	a	k8xC
drancovala	drancovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
těch	ten	k3xDgFnPc2
dob	doba	k1gFnPc2
už	už	k6eAd1
nechtěli	chtít	k5eNaImAgMnP
lidé	člověk	k1gMnPc1
na	na	k7c6
Východě	východ	k1gInSc6
o	o	k7c6
jednotě	jednota	k1gFnSc6
s	s	k7c7
papežem	papež	k1gMnSc7
ani	ani	k8xC
slyšet	slyšet	k5eAaImF
a	a	k8xC
podle	podle	k7c2
některých	některý	k3yIgFnPc2
zmínek	zmínka	k1gFnPc2
dávali	dávat	k5eAaImAgMnP
přednost	přednost	k1gFnSc4
muslimům	muslim	k1gMnPc3
před	před	k7c7
římskými	římský	k2eAgMnPc7d1
katolíky	katolík	k1gMnPc7
(	(	kIx(
<g/>
známý	známý	k2eAgInSc1d1
výrok	výrok	k1gInSc1
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
<g/>
:	:	kIx,
Raději	rád	k6eAd2
uvidíme	uvidět	k5eAaPmIp1nP
na	na	k7c6
našich	náš	k3xOp1gFnPc6
ulicích	ulice	k1gFnPc6
muslimské	muslimský	k2eAgInPc4d1
turbany	turban	k1gInPc4
než	než	k8xS
kardinálské	kardinálský	k2eAgFnPc4d1
čepice	čepice	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Církve	církev	k1gFnPc1
se	se	k3xPyFc4
pokoušely	pokoušet	k5eAaImAgFnP
několikrát	několikrát	k6eAd1
o	o	k7c6
obnovení	obnovení	k1gNnSc6
jednoty	jednota	k1gFnSc2
různými	různý	k2eAgNnPc7d1
jednáními	jednání	k1gNnPc7
o	o	k7c6
unii	unie	k1gFnSc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
příčiny	příčina	k1gFnPc1
byly	být	k5eAaImAgFnP
často	často	k6eAd1
politické	politický	k2eAgFnPc1d1
(	(	kIx(
<g/>
tlak	tlak	k1gInSc1
Osmanských	osmanský	k2eAgInPc2d1
Turků	turek	k1gInPc2
na	na	k7c4
Byzanc	Byzanc	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
roku	rok	k1gInSc2
1439	#num#	k4
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
však	však	k9
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
církvemi	církev	k1gFnPc7
jsou	být	k5eAaImIp3nP
už	už	k6eAd1
neslučitelné	slučitelný	k2eNgInPc1d1
věroučné	věroučný	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
tedy	tedy	k9
pouze	pouze	k6eAd1
o	o	k7c4
velmi	velmi	k6eAd1
časově	časově	k6eAd1
omezené	omezený	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
se	s	k7c7
zápornými	záporný	k2eAgInPc7d1
dopady	dopad	k1gInPc7
na	na	k7c4
dlouhodobý	dlouhodobý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
obou	dva	k4xCgFnPc2
církví	církev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavní	pravoslavný	k2eAgMnPc1d1
delegáti	delegát	k1gMnPc1
vypovídali	vypovídat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
pod	pod	k7c7
tlakem	tlak	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
unii	unie	k1gFnSc4
podepsali	podepsat	k5eAaPmAgMnP
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
pak	pak	k6eAd1
doma	doma	k6eAd1
lidem	člověk	k1gMnPc3
vnímáni	vnímat	k5eAaImNgMnP
jako	jako	k9
zrádci	zrádce	k1gMnPc1
pravoslaví	pravoslaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
věřících	věřící	k1gMnPc2
obou	dva	k4xCgFnPc2
církví	církev	k1gFnPc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
kromě	kromě	k7c2
rozdílů	rozdíl	k1gInPc2
ve	v	k7c6
víře	víra	k1gFnSc6
a	a	k8xC
církevní	církevní	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
zatížen	zatížit	k5eAaPmNgInS
i	i	k9
dalšími	další	k2eAgFnPc7d1
historickými	historický	k2eAgFnPc7d1
skutečnostmi	skutečnost	k1gFnPc7
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
vypleněním	vyplenění	k1gNnSc7
Konstantinopole	Konstantinopol	k1gInSc2
čtvrtou	čtvrtý	k4xOgFnSc7
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
roku	rok	k1gInSc2
1204	#num#	k4
a	a	k8xC
selháním	selhání	k1gNnSc7
západní	západní	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
při	při	k7c6
obléhání	obléhání	k1gNnSc6
Konstantinopole	Konstantinopol	k1gInSc2
sultánem	sultán	k1gMnSc7
Mehmedem	Mehmed	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1453	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1964	#num#	k4
však	však	k9
papež	papež	k1gMnSc1
Pavel	Pavel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
patriarcha	patriarcha	k1gMnSc1
Athenagoras	Athenagoras	k1gMnSc1
sňali	snít	k5eAaPmAgMnP
devět	devět	k4xCc4
set	sto	k4xCgNnPc2
let	léto	k1gNnPc2
starou	starý	k2eAgFnSc4d1
vzájemnou	vzájemný	k2eAgFnSc4d1
exkomunikaci	exkomunikace	k1gFnSc4
a	a	k8xC
roku	rok	k1gInSc3
1967	#num#	k4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
k	k	k7c3
historicky	historicky	k6eAd1
prvnímu	první	k4xOgNnSc3
setkání	setkání	k1gNnSc3
papeže	papež	k1gMnSc2
a	a	k8xC
konstantinopolského	konstantinopolský	k2eAgMnSc2d1
patriarchy	patriarcha	k1gMnSc2
po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
schizmatu	schizma	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
ovšem	ovšem	k9
stále	stále	k6eAd1
trvá	trvat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2001	#num#	k4
se	se	k3xPyFc4
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
omluvil	omluvit	k5eAaPmAgMnS
za	za	k7c4
veškerá	veškerý	k3xTgNnPc4
příkoří	příkoří	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
katolíci	katolík	k1gMnPc1
pravoslavným	pravoslavný	k2eAgFnPc3d1
způsobili	způsobit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
při	při	k7c6
800	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
vyplenění	vyplenění	k1gNnSc2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
tuto	tento	k3xDgFnSc4
prosbu	prosba	k1gFnSc4
o	o	k7c4
odpuštění	odpuštění	k1gNnSc4
zopakoval	zopakovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patriarcha	patriarcha	k1gMnSc1
Bartoloměj	Bartoloměj	k1gMnSc1
I.	I.	kA
byl	být	k5eAaImAgMnS
naopak	naopak	k6eAd1
roku	rok	k1gInSc2
2005	#num#	k4
jako	jako	k8xS,k8xC
vůbec	vůbec	k9
první	první	k4xOgMnSc1
konstantinopolský	konstantinopolský	k2eAgMnSc1d1
patriarcha	patriarcha	k1gMnSc1
přítomen	přítomen	k2eAgMnSc1d1
pohřbu	pohřeb	k1gInSc2
papeže	papež	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pohledu	pohled	k1gInSc2
pravoslavných	pravoslavný	k2eAgFnPc2d1
jsou	být	k5eAaImIp3nP
však	však	k9
papežova	papežův	k2eAgNnPc4d1
omluvná	omluvný	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
a	a	k8xC
vstřícné	vstřícný	k2eAgFnPc4d1
nabídky	nabídka	k1gFnPc4
k	k	k7c3
dialogu	dialog	k1gInSc3
v	v	k7c6
ostrém	ostrý	k2eAgInSc6d1
protikladu	protiklad	k1gInSc6
s	s	k7c7
praktickým	praktický	k2eAgNnSc7d1
působením	působení	k1gNnSc7
Vatikánu	Vatikán	k1gInSc2
vůči	vůči	k7c3
pravoslavným	pravoslavný	k2eAgFnPc3d1
církvím	církev	k1gFnPc3
–	–	k?
zvláště	zvláště	k6eAd1
máme	mít	k5eAaImIp1nP
na	na	k7c6
mysli	mysl	k1gFnSc6
papežovu	papežův	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
výsledků	výsledek	k1gInPc2
dřívějších	dřívější	k2eAgFnPc2d1
unií	unie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
pravoslavní	pravoslavný	k2eAgMnPc1d1
vnímají	vnímat	k5eAaImIp3nP
jako	jako	k9
neblahé	blahý	k2eNgInPc4d1
a	a	k8xC
protipravoslavné	protipravoslavný	k2eAgInPc4d1
akty	akt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Významným	významný	k2eAgInSc7d1
problémovým	problémový	k2eAgInSc7d1
bodem	bod	k1gInSc7
dialogu	dialog	k1gInSc2
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
otázka	otázka	k1gFnSc1
uniatských	uniatský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
totiž	totiž	k9
východních	východní	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
před	před	k7c4
půl	půl	k1xP
tisíciletím	tisíciletí	k1gNnSc7
na	na	k7c6
původně	původně	k6eAd1
pravoslavném	pravoslavný	k2eAgNnSc6d1
území	území	k1gNnSc6
pod	pod	k7c7
polskou	polský	k2eAgFnSc7d1
či	či	k8xC
habsburskou	habsburský	k2eAgFnSc7d1
nadvládou	nadvláda	k1gFnSc7
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
zdejší	zdejší	k2eAgMnPc1d1
původně	původně	k6eAd1
pravoslavní	pravoslavný	k2eAgMnPc1d1
věřící	věřící	k1gMnPc1
a	a	k8xC
kněží	kněz	k1gMnPc1
museli	muset	k5eAaImAgMnP
přijmout	přijmout	k5eAaPmF
papežský	papežský	k2eAgInSc4d1
primát	primát	k1gInSc4
a	a	k8xC
byli	být	k5eAaImAgMnP
pod	pod	k7c7
tlakem	tlak	k1gInSc7
světské	světský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
podrobeni	podroben	k2eAgMnPc1d1
jurisdikci	jurisdikce	k1gFnSc4
římské	římský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
–	–	k?
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
ponechat	ponechat	k5eAaPmF
východní	východní	k2eAgInSc4d1
obřad	obřad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
tlak	tlak	k1gInSc1
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
gardu	gard	k1gInSc6
pak	pak	k6eAd1
vyvíjelo	vyvíjet	k5eAaImAgNnS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
carské	carský	k2eAgNnSc4d1
samoděržaví	samoděržaví	k1gNnSc4
<g/>
,	,	kIx,
ruští	ruský	k2eAgMnPc1d1
bolševici	bolševik	k1gMnPc1
<g/>
,	,	kIx,
komunisté	komunista	k1gMnPc1
v	v	k7c6
Československu	Československo	k1gNnSc6
či	či	k8xC
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
zde	zde	k6eAd1
dodnes	dodnes	k6eAd1
mnoho	mnoho	k4c1
vzájemných	vzájemný	k2eAgInPc2d1
majetkových	majetkový	k2eAgInPc2d1
sporů	spor	k1gInPc2
i	i	k8xC
vědomí	vědomí	k1gNnSc2
křivd	křivda	k1gFnPc2
dané	daný	k2eAgFnSc2d1
komplikovaným	komplikovaný	k2eAgNnSc7d1
soužitím	soužití	k1gNnSc7
věřících	věřící	k1gMnPc2
z	z	k7c2
obou	dva	k4xCgFnPc2
církví	církev	k1gFnPc2
na	na	k7c6
jednom	jeden	k4xCgNnSc6
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
ocitly	ocitnout	k5eAaPmAgFnP
pod	pod	k7c7
nadvládou	nadvláda	k1gFnSc7
ateistického	ateistický	k2eAgInSc2d1
komunismu	komunismus	k1gInSc2
<g/>
,	,	kIx,
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
násilné	násilný	k2eAgFnSc3d1
likvidaci	likvidace	k1gFnSc3
uniatských	uniatský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
kněžstvo	kněžstvo	k1gNnSc1
i	i	k8xC
věřící	věřící	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
k	k	k7c3
přestupu	přestup	k1gInSc3
k	k	k7c3
pravoslaví	pravoslaví	k1gNnSc3
(	(	kIx(
<g/>
lvovský	lvovský	k2eAgInSc4d1
synod	synod	k1gInSc4
v	v	k7c6
r.	r.	kA
1946	#num#	k4
<g/>
,	,	kIx,
prešovský	prešovský	k2eAgInSc1d1
synod	synod	k1gInSc1
v	v	k7c6
r.	r.	kA
1950	#num#	k4
<g/>
,	,	kIx,
zrušení	zrušení	k1gNnSc1
unie	unie	k1gFnSc2
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
v	v	k7c6
r.	r.	kA
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Restituci	restituce	k1gFnSc6
uniatů	uniat	k1gMnPc2
umožnil	umožnit	k5eAaPmAgInS
až	až	k9
postupný	postupný	k2eAgInSc1d1
rozklad	rozklad	k1gInSc1
komunismu	komunismus	k1gInSc2
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pravoslavných	pravoslavný	k2eAgInPc2d1
patří	patřit	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
uniatské	uniatský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
k	k	k7c3
největším	veliký	k2eAgMnPc3d3
nepřátelům	nepřítel	k1gMnPc3
pravoslaví	pravoslaví	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
má	mít	k5eAaImIp3nS
tento	tento	k3xDgInSc4
náhled	náhled	k1gInSc4
oporu	opora	k1gFnSc4
v	v	k7c6
dění	dění	k1gNnSc6
na	na	k7c6
východním	východní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
a	a	k8xC
Ukrajině	Ukrajina	k1gFnSc6
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
uniaté	uniat	k1gMnPc1
po	po	k7c6
pádu	pád	k1gInSc6
komunismu	komunismus	k1gInSc2
žádali	žádat	k5eAaImAgMnP
navrácení	navrácení	k1gNnSc3
chrámů	chrám	k1gInPc2
a	a	k8xC
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
komunisté	komunista	k1gMnPc1
v	v	k7c6
předchozích	předchozí	k2eAgNnPc6d1
letech	léto	k1gNnPc6
uniatům	uniat	k1gMnPc3
násilně	násilně	k6eAd1
zabrali	zabrat	k5eAaPmAgMnP
a	a	k8xC
následně	následně	k6eAd1
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
pravoslavným	pravoslavný	k2eAgFnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavní	pravoslavný	k2eAgMnPc5d1
drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
tohoto	tento	k3xDgInSc2
majetku	majetek	k1gInSc2
vrátit	vrátit	k5eAaPmF
odmítali	odmítat	k5eAaImAgMnP
<g/>
,	,	kIx,
úspěšní	úspěšný	k2eAgMnPc1d1
v	v	k7c6
tomto	tento	k3xDgInSc6
postoji	postoj	k1gInSc6
však	však	k9
byli	být	k5eAaImAgMnP
jen	jen	k9
někde	někde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
důvod	důvod	k1gInSc1
proč	proč	k6eAd1
koncem	koncem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
zmrazily	zmrazit	k5eAaPmAgFnP
pravoslavné	pravoslavný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
dialog	dialog	k1gInSc4
s	s	k7c7
římskokatolickou	římskokatolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
a	a	k8xC
jediným	jediný	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
<g/>
,	,	kIx,
o	o	k7c6
němž	jenž	k3xRgInSc6
byly	být	k5eAaImAgInP
ochotny	ochoten	k2eAgInPc4d1
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
uniatství	uniatství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
k	k	k7c3
anglikánskému	anglikánský	k2eAgNnSc3d1
společenství	společenství	k1gNnSc3
a	a	k8xC
starokatolické	starokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
</s>
<s>
K	k	k7c3
výraznějšímu	výrazný	k2eAgNnSc3d2
přiblížení	přiblížení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
anglikánskému	anglikánský	k2eAgNnSc3d1
společenství	společenství	k1gNnSc3
a	a	k8xC
starokatolické	starokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
;	;	kIx,
problémem	problém	k1gInSc7
zde	zde	k6eAd1
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
nově	nově	k6eAd1
zavedené	zavedený	k2eAgNnSc1d1
svěcení	svěcení	k1gNnSc1
žen	žena	k1gFnPc2
v	v	k7c6
anglikánské	anglikánský	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
a	a	k8xC
další	další	k2eAgFnSc1d1
protestantizující	protestantizující	k2eAgFnSc1d1
tendence	tendence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
akceptování	akceptování	k1gNnSc1
nebo	nebo	k8xC
tolerování	tolerování	k1gNnSc1
homosexuality	homosexualita	k1gFnSc2
u	u	k7c2
některých	některý	k3yIgFnPc2
protestantských	protestantský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
Pravoslavnou	pravoslavný	k2eAgFnSc4d1
církev	církev	k1gFnSc4
zcela	zcela	k6eAd1
nepřijatelná	přijatelný	k2eNgFnSc1d1
doktrína	doktrína	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Ekumenická	ekumenický	k2eAgFnSc1d1
rada	rada	k1gFnSc1
církví	církev	k1gFnPc2
</s>
<s>
Pravoslavné	pravoslavný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
jsou	být	k5eAaImIp3nP
až	až	k9
na	na	k7c4
několik	několik	k4yIc4
výjimek	výjimek	k1gInSc4
členy	člen	k1gMnPc7
Ekumenické	ekumenický	k2eAgFnSc2d1
rady	rada	k1gFnSc2
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nabízí	nabízet	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
dialogu	dialog	k1gInSc2
s	s	k7c7
protestantskými	protestantský	k2eAgFnPc7d1
a	a	k8xC
reformovanými	reformovaný	k2eAgFnPc7d1
církvemi	církev	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoslavných	pravoslavný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
ovšem	ovšem	k9
v	v	k7c6
radě	rada	k1gFnSc6
k	k	k7c3
poměru	poměr	k1gInSc3
k	k	k7c3
ostatním	ostatní	k2eAgFnPc3d1
církvím	církev	k1gFnPc3
není	být	k5eNaImIp3nS
mnoho	mnoho	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
při	při	k7c6
rozhodování	rozhodování	k1gNnSc6
v	v	k7c6
menšině	menšina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gruzínská	gruzínský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
ovšem	ovšem	k9
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
misii	misie	k1gFnSc3
protestantských	protestantský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
v	v	k7c6
Gruzii	Gruzie	k1gFnSc6
radu	rad	k1gInSc2
zcela	zcela	k6eAd1
opustila	opustit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odchod	odchod	k1gInSc1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
ze	z	k7c2
Světové	světový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
církví	církev	k1gFnPc2
je	být	k5eAaImIp3nS
uvnitř	uvnitř	k7c2
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
intenzivně	intenzivně	k6eAd1
diskutovaná	diskutovaný	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
biskupů	biskup	k1gInPc2
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
účasti	účast	k1gFnSc3
pravoslavných	pravoslavný	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
v	v	k7c6
ekumenickém	ekumenický	k2eAgNnSc6d1
hnutí	hnutí	k1gNnSc6
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
ohrazení	ohrazení	k1gNnSc2
a	a	k8xC
nemají	mít	k5eNaImIp3nP
nyní	nyní	k6eAd1
s	s	k7c7
tzv.	tzv.	kA
oficiálními	oficiální	k2eAgFnPc7d1
pravoslavnými	pravoslavný	k2eAgFnPc7d1
církvemi	církev	k1gFnPc7
společenství	společenství	k1gNnSc2
<g/>
,	,	kIx,
uznávají	uznávat	k5eAaImIp3nP
však	však	k9
tajiny	tajina	k1gFnSc2
(	(	kIx(
<g/>
svátosti	svátost	k1gFnSc2
<g/>
)	)	kIx)
udělované	udělovaný	k2eAgFnSc2d1
ve	v	k7c6
většině	většina	k1gFnSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
starokalendářní	starokalendářní	k2eAgFnPc1d1
synody	synoda	k1gFnPc1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
rumunský	rumunský	k2eAgInSc1d1
starokalendářní	starokalendářní	k2eAgInSc1d1
synod	synod	k1gInSc1
nebo	nebo	k8xC
část	část	k1gFnSc1
ruské	ruský	k2eAgFnSc2d1
zahraniční	zahraniční	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ruské	ruský	k2eAgNnSc1d1
pravoslaví	pravoslaví	k1gNnSc1
ruší	rušit	k5eAaImIp3nS
styky	styk	k1gInPc4
s	s	k7c7
konstantinopolským	konstantinopolský	k2eAgMnSc7d1
patriarchou	patriarcha	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Opozici	opozice	k1gFnSc6
nevoní	vonět	k5eNaImIp3nS
požadavek	požadavek	k1gInSc4
Porošenka	Porošenka	k1gFnSc1
na	na	k7c4
církevní	církevní	k2eAgFnSc4d1
samostatnost	samostatnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
‚	‚	k?
<g/>
Slouží	sloužit	k5eAaImIp3nS
ruské	ruský	k2eAgFnSc3d1
agresi	agrese	k1gFnSc3
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Ukrajinští	ukrajinský	k2eAgMnPc1d1
policisté	policista	k1gMnPc1
prohledávali	prohledávat	k5eAaImAgMnP
budovy	budova	k1gFnPc4
církve	církev	k1gFnSc2
loajální	loajální	k2eAgFnSc2d1
k	k	k7c3
Moskvě	Moskva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Konec	konec	k1gInSc1
(	(	kIx(
<g/>
iluze	iluze	k1gFnSc1
<g/>
)	)	kIx)
ruského	ruský	k2eAgInSc2d1
světa	svět	k1gInSc2
a	a	k8xC
hrozba	hrozba	k1gFnSc1
rozkolu	rozkol	k1gInSc2
světového	světový	k2eAgNnSc2d1
pravoslaví	pravoslaví	k1gNnSc2
<g/>
.	.	kIx.
!	!	kIx.
</s>
<s desamb="1">
<g/>
Argument	argument	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
IS	IS	kA
CNS	CNS	kA
<g/>
.	.	kIx.
www	www	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
mkcr	mkcra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Encyklopedie	encyklopedie	k1gFnSc1
Byzance	Byzanc	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2011	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
210	#num#	k4
<g/>
,	,	kIx,
251	#num#	k4
<g/>
–	–	k?
<g/>
252	#num#	k4
<g/>
,	,	kIx,
254	#num#	k4
<g/>
,	,	kIx,
282	#num#	k4
<g/>
↑	↑	k?
Dostálová	Dostálová	k1gFnSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
,	,	kIx,
Byzantská	byzantský	k2eAgFnSc1d1
vzdělanost	vzdělanost	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
49	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hésychasmus	Hésychasmus	k1gMnSc1
</s>
<s>
Katolictví	katolictví	k1gNnSc1
</s>
<s>
Ortodoxie	ortodoxie	k1gFnSc1
</s>
<s>
Pravoslavný	pravoslavný	k2eAgInSc1d1
fundamentalismus	fundamentalismus	k1gInSc1
</s>
<s>
Protestantství	protestantství	k1gNnSc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
Athos	Athos	k1gInSc1
</s>
<s>
Východní	východní	k2eAgNnSc4d1
křesťanství	křesťanství	k1gNnSc4
</s>
<s>
Jurodiví	jurodivý	k2eAgMnPc1d1
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Ukrajiny	Ukrajina	k1gFnSc2
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
eparchie	eparchie	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
současných	současný	k2eAgMnPc2d1
nejvyšších	vysoký	k2eAgMnPc2d3
představitelů	představitel	k1gMnPc2
pravoslavných	pravoslavný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
pravoslaví	pravoslaví	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
orthodox	orthodox	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
/	/	kIx~
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
pp-eparchie	pp-eparchie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
/	/	kIx~
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Pražské	pražský	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
eparchie	eparchie	k1gFnSc2
</s>
<s>
www.ob-eparchie.cz/	www.ob-eparchie.cz/	k?
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Olomoucko-brněnské	olomoucko-brněnský	k2eAgFnSc2d1
eparchie	eparchie	k1gFnSc2
Pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
www.pravoslavi.cz	www.pravoslavi.cz	k1gInSc1
–	–	k?
Český	český	k2eAgInSc1d1
pravoslavný	pravoslavný	k2eAgInSc1d1
rozcestník	rozcestník	k1gInSc1
</s>
<s>
www.orthodoxia.cz	www.orthodoxia.cz	k1gInSc1
–	–	k?
Křesťanská	křesťanský	k2eAgFnSc1d1
orthodoxie	orthodoxie	k1gFnSc1
</s>
<s>
orthodoxiachristiana	orthodoxiachristiana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Řecká	řecký	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
starostylní	starostylní	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
;	;	kIx,
dogmatická	dogmatický	k2eAgFnSc1d1
theologie	theologie	k1gFnSc1
<g/>
,	,	kIx,
eschatologie	eschatologie	k1gFnSc1
a	a	k8xC
příběhy	příběh	k1gInPc1
z	z	k7c2
duchovního	duchovní	k2eAgInSc2d1
života	život	k1gInSc2
</s>
<s>
pravoslavi	pravoslavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
org	org	k?
–	–	k?
Pravoslavné	pravoslavný	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
</s>
<s>
www.pravoslavne-krestanstvo.com/	www.pravoslavne-krestanstvo.com/	k?
–	–	k?
ruský	ruský	k2eAgInSc4d1
rozcestník	rozcestník	k1gInSc4
pravoslavného	pravoslavný	k2eAgInSc2d1
internetu	internet	k1gInSc2
</s>
<s>
www.hlas.pravoslavi.cz/	www.hlas.pravoslavi.cz/	k?
–	–	k?
archiv	archiv	k1gInSc4
jediné	jediný	k2eAgFnSc2d1
publikace	publikace	k1gFnSc2
v	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
„	„	k?
<g/>
Hlas	hlas	k1gInSc1
pravoslaví	pravoslaví	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
Pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
7805	#num#	k4
</s>
