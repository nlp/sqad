<s>
Pravoslaví	pravoslaví	k1gNnSc1
neboli	neboli	k8xC
ortodoxie	ortodoxie	k1gFnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
či	či	k8xC
ortodoxní	ortodoxní	k2eAgFnSc1d1
církev	církev	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
ὀ	ὀ	k?
orthos	orthos	k1gInSc1
správný	správný	k2eAgInSc1d1
<g/>
,	,	kIx,
pravdivý	pravdivý	k2eAgInSc1d1
+	+	kIx~
δ	δ	k?
doxa	dox	k2eAgFnSc1d1
sláva	sláva	k1gFnSc1
<g/>
,	,	kIx,
názor	názor	k1gInSc1
<g/>
,	,	kIx,
učení	učení	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
křesťanské	křesťanský	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
a	a	k8xC
církev	církev	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
více	hodně	k6eAd2
územních	územní	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dohromady	dohromady	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
pravoslavné	pravoslavný	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
<g/>
.	.	kIx.
</s>