<p>
<s>
SS	SS	kA	SS
Romanic	Romanice	k1gFnPc2	Romanice
byl	být	k5eAaImAgInS	být
parník	parník	k1gInSc1	parník
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
v	v	k7c6	v
loděnicích	loděnice	k1gFnPc6	loděnice
Harland	Harlanda	k1gFnPc2	Harlanda
&	&	k?	&
Wolff	Wolff	k1gMnSc1	Wolff
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
původně	původně	k6eAd1	původně
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
New	New	k1gFnSc2	New
England	Englanda	k1gFnPc2	Englanda
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
Dominion	dominion	k1gNnSc4	dominion
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
plavbu	plavba	k1gFnSc4	plavba
z	z	k7c2	z
Liverpoolu	Liverpool	k1gInSc2	Liverpool
do	do	k7c2	do
Bostonu	Boston	k1gInSc2	Boston
vyplul	vyplout	k5eAaPmAgInS	vyplout
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1903	[number]	k4	1903
byl	být	k5eAaImAgInS	být
předán	předat	k5eAaPmNgInS	předat
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Romanic	Romanice	k1gFnPc2	Romanice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
plavbu	plavba	k1gFnSc4	plavba
pod	pod	k7c7	pod
novou	nový	k2eAgFnSc7d1	nová
společností	společnost	k1gFnSc7	společnost
z	z	k7c2	z
Liverpoolu	Liverpool	k1gInSc2	Liverpool
do	do	k7c2	do
Bostonu	Boston	k1gInSc2	Boston
vyplul	vyplout	k5eAaPmAgInS	vyplout
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
sloužil	sloužit	k5eAaImAgMnS	sloužit
na	na	k7c6	na
trasách	trasa	k1gFnPc6	trasa
ze	z	k7c2	z
Středozemí	Středozem	k1gFnPc2	Středozem
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1912	[number]	k4	1912
byl	být	k5eAaImAgInS	být
předán	předat	k5eAaPmNgInS	předat
společnosti	společnost	k1gFnSc3	společnost
Allan	Allan	k1gMnSc1	Allan
Line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Scandinavian	Scandinavian	k1gInSc4	Scandinavian
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Glasgow	Glasgow	k1gInSc1	Glasgow
–	–	k?	–
Montreal	Montreal	k1gInSc1	Montreal
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1915	[number]	k4	1915
byla	být	k5eAaImAgFnS	být
Allan	Allan	k1gMnSc1	Allan
Line	linout	k5eAaImIp3nS	linout
převzata	převzít	k5eAaPmNgFnS	převzít
společností	společnost	k1gFnSc7	společnost
Canadian	Canadian	k1gMnSc1	Canadian
Pacific	Pacific	k1gMnSc1	Pacific
<g/>
.	.	kIx.	.
</s>
<s>
Scandinavian	Scandinavian	k1gMnSc1	Scandinavian
pak	pak	k6eAd1	pak
sloužil	sloužit	k5eAaImAgMnS	sloužit
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Liverpool	Liverpool	k1gInSc1	Liverpool
–	–	k?	–
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Antverpy	Antverpy	k1gFnPc1	Antverpy
–	–	k?	–
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1922	[number]	k4	1922
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
rok	rok	k1gInSc4	rok
odstaven	odstavit	k5eAaPmNgInS	odstavit
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
do	do	k7c2	do
šrotu	šrot	k1gInSc2	šrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
