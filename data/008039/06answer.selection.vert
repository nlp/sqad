<s>
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
<g/>
,	,	kIx,	,
CBE	CBE	kA	CBE
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1892	[number]	k4	1892
Bloemfontein	Bloemfontein	k1gInSc1	Bloemfontein
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1973	[number]	k4	1973
Bournemouth	Bournemoutha	k1gFnPc2	Bournemoutha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnSc1d3	nejznámější
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
Hobita	hobit	k1gMnSc2	hobit
a	a	k8xC	a
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
