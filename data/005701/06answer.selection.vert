<s>
Rabín	rabín	k1gMnSc1	rabín
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
rabi	rab	k1gMnPc1	rab
<g/>
,	,	kIx,	,
z	z	k7c2	z
hebr.	hebr.	k?	hebr.
ר	ר	k?	ר
rav	rav	k?	rav
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
ר	ר	k?	ר
rabi	rabi	k1gMnSc1	rabi
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysokou	vysoký	k2eAgFnSc7d1	vysoká
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
autoritou	autorita	k1gFnSc7	autorita
v	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
<g/>
.	.	kIx.	.
</s>
