<s>
Vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
také	také	k9	také
antirachitický	antirachitický	k2eAgInSc1d1	antirachitický
vitamín	vitamín	k1gInSc1	vitamín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
steroidní	steroidní	k2eAgFnPc4d1	steroidní
hormonální	hormonální	k2eAgFnPc4d1	hormonální
prekurzory	prekurzora	k1gFnPc4	prekurzora
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
označované	označovaný	k2eAgFnPc1d1	označovaná
též	též	k9	též
jako	jako	k9	jako
kalciferoly	kalciferola	k1gFnPc1	kalciferola
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
výchozí	výchozí	k2eAgInPc1d1	výchozí
látkou	látka	k1gFnSc7	látka
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
kalcitriolu	kalcitriol	k1gInSc2	kalcitriol
<g/>
,	,	kIx,	,
hormonu	hormon	k1gInSc2	hormon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
významně	významně	k6eAd1	významně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
metabolismus	metabolismus	k1gInSc4	metabolismus
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
vitamín	vitamín	k1gInSc1	vitamín
D2	D2	k1gFnSc2	D2
(	(	kIx(	(
<g/>
ergokalciferol	ergokalciferol	k1gInSc1	ergokalciferol
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitamín	vitamín	k1gInSc1	vitamín
D3	D3	k1gFnSc2	D3
(	(	kIx(	(
<g/>
cholekalciferol	cholekalciferol	k1gInSc1	cholekalciferol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c4	v
kůži	kůže	k1gFnSc4	kůže
působením	působení	k1gNnSc7	působení
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
z	z	k7c2	z
provitamínu	provitamín	k1gInSc2	provitamín
7	[number]	k4	7
<g/>
-dehydrocholesterolu	ehydrocholesterol	k1gInSc2	-dehydrocholesterol
<g/>
,	,	kIx,	,
derivátu	derivát	k1gInSc2	derivát
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Fotony	foton	k1gInPc1	foton
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
štěpí	štěpit	k5eAaImIp3nS	štěpit
B	B	kA	B
jádro	jádro	k1gNnSc1	jádro
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
dehydrocholesterolu	dehydrocholesterol	k1gInSc2	dehydrocholesterol
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
cholekalciferolu	cholekalciferol	k1gInSc2	cholekalciferol
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
výroby	výroba	k1gFnSc2	výroba
vitamínu	vitamín	k1gInSc2	vitamín
v	v	k7c4	v
kůži	kůže	k1gFnSc4	kůže
si	se	k3xPyFc3	se
osvojili	osvojit	k5eAaPmAgMnP	osvojit
suchozemští	suchozemský	k2eAgMnPc1d1	suchozemský
obratlovci	obratlovec	k1gMnPc1	obratlovec
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
300	[number]	k4	300
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
vápník	vápník	k1gInSc4	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Syntéza	syntéza	k1gFnSc1	syntéza
působením	působení	k1gNnSc7	působení
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
stačit	stačit	k5eAaBmF	stačit
na	na	k7c4	na
pokrytí	pokrytí	k1gNnSc4	pokrytí
až	až	k6eAd1	až
80	[number]	k4	80
%	%	kIx~	%
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
a	a	k8xC	a
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
se	se	k3xPyFc4	se
cholekalciferol	cholekalciferola	k1gFnPc2	cholekalciferola
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
rybím	rybí	k2eAgInSc6d1	rybí
tuku	tuk	k1gInSc6	tuk
<g/>
,	,	kIx,	,
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
vaječném	vaječný	k2eAgInSc6d1	vaječný
žloutku	žloutek	k1gInSc6	žloutek
a	a	k8xC	a
mléce	mléko	k1gNnSc6	mléko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
prekurzorem	prekurzor	k1gInSc7	prekurzor
ergosterol	ergosterol	k1gInSc1	ergosterol
<g/>
,	,	kIx,	,
morfin	morfin	k1gInSc1	morfin
a	a	k8xC	a
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
ergokalciferol	ergokalciferol	k1gInSc4	ergokalciferol
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
Údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
Recommended	Recommended	k1gMnSc1	Recommended
Dietary	Dietara	k1gFnSc2	Dietara
Allowances	Allowances	k1gMnSc1	Allowances
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
th	th	k?	th
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Food	Food	k1gInSc1	Food
and	and	k?	and
Nutrition	Nutrition	k1gInSc1	Nutrition
Board	Board	k1gInSc1	Board
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
Research	Research	k1gMnSc1	Research
Ccouncil	Ccouncil	k1gMnSc1	Ccouncil
<g/>
–	–	k?	–
<g/>
National	National	k1gMnSc1	National
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Jeho	jeho	k3xOp3gFnSc4	jeho
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
účasti	účast	k1gFnSc6	účast
při	při	k7c6	při
resorpci	resorpce	k1gFnSc6	resorpce
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
fosfátu	fosfát	k1gInSc2	fosfát
ze	z	k7c2	z
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
přispívá	přispívat	k5eAaImIp3nS	přispívat
tak	tak	k6eAd1	tak
k	k	k7c3	k
regulaci	regulace	k1gFnSc3	regulace
a	a	k8xC	a
optimalizaci	optimalizace	k1gFnSc3	optimalizace
hladiny	hladina	k1gFnSc2	hladina
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
fosforu	fosfor	k1gInSc2	fosfor
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Fosfor	fosfor	k1gInSc4	fosfor
i	i	k8xC	i
vápník	vápník	k1gInSc4	vápník
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
významný	významný	k2eAgInSc4d1	významný
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
kostí	kost	k1gFnPc2	kost
silných	silný	k2eAgNnPc2d1	silné
a	a	k8xC	a
nepoškozených	poškozený	k2eNgNnPc2d1	nepoškozené
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
a	a	k8xC	a
chřipka	chřipka	k1gFnSc1	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
fungování	fungování	k1gNnSc4	fungování
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
je	být	k5eAaImIp3nS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
náchylností	náchylnost	k1gFnSc7	náchylnost
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
akutním	akutní	k2eAgFnPc3d1	akutní
respiračním	respirační	k2eAgFnPc3d1	respirační
infekcím	infekce	k1gFnPc3	infekce
a	a	k8xC	a
chřipce	chřipka	k1gFnSc3	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
metabolit	metabolit	k1gInSc1	metabolit
1,25	[number]	k4	1,25
<g/>
-dihydroxycholekalciferol	ihydroxycholekalciferola	k1gFnPc2	-dihydroxycholekalciferola
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
regulační	regulační	k2eAgFnSc4d1	regulační
roli	role	k1gFnSc4	role
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
antimikrobiálních	antimikrobiální	k2eAgInPc2d1	antimikrobiální
peptidů	peptid	k1gInPc2	peptid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
katelicidinu	katelicidin	k1gInSc2	katelicidin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
imunity	imunita	k1gFnSc2	imunita
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vitamín	vitamín	k1gInSc1	vitamín
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
důležitý	důležitý	k2eAgInSc1d1	důležitý
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
imunodermatologických	imunodermatologický	k2eAgInPc6d1	imunodermatologický
procesech	proces	k1gInPc6	proces
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
léčba	léčba	k1gFnSc1	léčba
lupénky	lupénka	k1gFnSc2	lupénka
kalcitriolem	kalcitriol	k1gInSc7	kalcitriol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
výzkum	výzkum	k1gInSc1	výzkum
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
imunitním	imunitní	k2eAgInSc6d1	imunitní
systému	systém	k1gInSc6	systém
spíše	spíše	k9	spíše
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
sluneční	sluneční	k2eAgNnSc1d1	sluneční
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
změknutím	změknutí	k1gNnSc7	změknutí
kostí	kost	k1gFnPc2	kost
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ztrát	ztráta	k1gFnPc2	ztráta
a	a	k8xC	a
nedostatečné	dostatečný	k2eNgInPc4d1	nedostatečný
resorbce	resorbec	k1gInPc4	resorbec
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
fosfátu	fosfát	k1gInSc2	fosfát
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
projeví	projevit	k5eAaPmIp3nS	projevit
jako	jako	k9	jako
křivice	křivice	k1gFnSc1	křivice
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
osteomalacie	osteomalacie	k1gFnSc1	osteomalacie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
kazivostí	kazivost	k1gFnSc7	kazivost
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
má	mít	k5eAaImIp3nS	mít
hlavně	hlavně	k9	hlavně
jídelníček	jídelníček	k1gInSc1	jídelníček
<g/>
,	,	kIx,	,
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
a	a	k8xC	a
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitaminu	vitamin	k1gInSc2	vitamin
D	D	kA	D
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
hlavně	hlavně	k9	hlavně
řídnutím	řídnutí	k1gNnSc7	řídnutí
kostí	kost	k1gFnPc2	kost
jak	jak	k8xC	jak
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
osteoporóze	osteoporóza	k1gFnSc3	osteoporóza
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
vitaminu	vitamin	k1gInSc2	vitamin
D	D	kA	D
pro	pro	k7c4	pro
náš	náš	k3xOp1gInSc4	náš
organismus	organismus	k1gInSc4	organismus
je	být	k5eAaImIp3nS	být
však	však	k9	však
komplexnější	komplexní	k2eAgInSc1d2	komplexnější
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivně	pozitivně	k6eAd1	pozitivně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
celkovou	celkový	k2eAgFnSc4d1	celková
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
chrání	chránit	k5eAaImIp3nS	chránit
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
nádorových	nádorový	k2eAgNnPc2d1	nádorové
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
dávkách	dávka	k1gFnPc6	dávka
vitamín	vitamín	k1gInSc4	vitamín
D	D	kA	D
naopak	naopak	k6eAd1	naopak
metabolismus	metabolismus	k1gInSc4	metabolismus
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
fosforu	fosfor	k1gInSc2	fosfor
narušuje	narušovat	k5eAaImIp3nS	narušovat
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
hyperkalcémii	hyperkalcémie	k1gFnSc3	hyperkalcémie
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
skončit	skončit	k5eAaPmF	skončit
i	i	k9	i
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
regulačním	regulační	k2eAgInPc3d1	regulační
mechanismům	mechanismus	k1gInPc3	mechanismus
syntézy	syntéza	k1gFnSc2	syntéza
nikdy	nikdy	k6eAd1	nikdy
nevede	vést	k5eNaImIp3nS	vést
k	k	k7c3	k
hypervitaminose	hypervitaminosa	k1gFnSc3	hypervitaminosa
<g/>
.	.	kIx.	.
</s>
