<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c4	v
kůži	kůže	k1gFnSc4	kůže
působením	působení	k1gNnSc7	působení
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
z	z	k7c2	z
provitamínu	provitamín	k1gInSc2	provitamín
7	[number]	k4	7
<g/>
-dehydrocholesterolu	ehydrocholesterol	k1gInSc2	-dehydrocholesterol
<g/>
,	,	kIx,	,
derivátu	derivát	k1gInSc2	derivát
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
