<s>
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
Galileovským	Galileovský	k2eAgInPc3d1	Galileovský
měsícům	měsíc	k1gInPc3	měsíc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jej	on	k3xPp3gMnSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnSc6	Galile
již	již	k9	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
<g/>
.	.	kIx.	.
</s>
<s>
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
měsícem	měsíc	k1gInSc7	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
největším	veliký	k2eAgMnPc3d3	veliký
z	z	k7c2	z
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
Ganymedu	Ganymed	k1gMnSc6	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Callisto	Callista	k1gMnSc5	Callista
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
4820	[number]	k4	4820
km	km	kA	km
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
99	[number]	k4	99
%	%	kIx~	%
velikosti	velikost	k1gFnSc2	velikost
planety	planeta	k1gFnSc2	planeta
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
třetiny	třetina	k1gFnPc1	třetina
jeho	jeho	k3xOp3gFnSc2	jeho
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Obíhá	obíhat	k5eAaImIp3nS	obíhat
jako	jako	k9	jako
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
nejvzdálenější	vzdálený	k2eAgInSc4d3	nejvzdálenější
měsíc	měsíc	k1gInSc4	měsíc
z	z	k7c2	z
galileovských	galileovský	k2eAgInPc2d1	galileovský
měsíců	měsíc	k1gInPc2	měsíc
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
oběhu	oběh	k1gInSc2	oběh
asi	asi	k9	asi
1	[number]	k4	1
880	[number]	k4	880
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Io	Io	k1gFnSc3	Io
<g/>
,	,	kIx,	,
Europě	Europa	k1gFnSc3	Europa
a	a	k8xC	a
Ganymedu	Ganymed	k1gMnSc3	Ganymed
se	se	k3xPyFc4	se
nepodílí	podílet	k5eNaImIp3nS	podílet
na	na	k7c4	na
orbitální	orbitální	k2eAgFnSc4d1	orbitální
rezonanci	rezonance	k1gFnSc4	rezonance
zmiňovaných	zmiňovaný	k2eAgInPc2d1	zmiňovaný
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemá	mít	k5eNaImIp3nS	mít
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
zdroj	zdroj	k1gInSc4	zdroj
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
slapového	slapový	k2eAgNnSc2d1	slapové
působení	působení	k1gNnSc2	působení
jako	jako	k9	jako
ony	onen	k3xDgInPc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
neustále	neustále	k6eAd1	neustále
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
stejnou	stejný	k2eAgFnSc7d1	stejná
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
obíhá	obíhat	k5eAaImIp3nS	obíhat
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
méně	málo	k6eAd2	málo
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
působením	působení	k1gNnSc7	působení
jeho	jeho	k3xOp3gFnSc2	jeho
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
horninami	hornina	k1gFnPc7	hornina
a	a	k8xC	a
ledem	led	k1gInSc7	led
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
stejném	stejný	k2eAgInSc6d1	stejný
poměru	poměr	k1gInSc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
měsíce	měsíc	k1gInSc2	měsíc
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,83	[number]	k4	1,83
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Spektroskopická	spektroskopický	k2eAgNnPc1d1	spektroskopické
měření	měření	k1gNnPc1	měření
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
křemičitany	křemičitan	k1gInPc4	křemičitan
a	a	k8xC	a
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
až	až	k9	až
150	[number]	k4	150
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ledovou	ledový	k2eAgFnSc7d1	ledová
kůrou	kůra	k1gFnSc7	kůra
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
okolo	okolo	k7c2	okolo
100	[number]	k4	100
km	km	kA	km
zřejmě	zřejmě	k6eAd1	zřejmě
relativně	relativně	k6eAd1	relativně
tenký	tenký	k2eAgInSc1d1	tenký
oceán	oceán	k1gInSc1	oceán
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
již	již	k9	již
jen	jen	k9	jen
nediferencované	diferencovaný	k2eNgNnSc1d1	nediferencované
či	či	k8xC	či
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
diferencované	diferencovaný	k2eAgNnSc1d1	diferencované
jádro	jádro	k1gNnSc1	jádro
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
křemičitanů	křemičitan	k1gInPc2	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
rozryt	rozrýt	k5eAaPmNgInS	rozrýt
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
velice	velice	k6eAd1	velice
starý	starý	k2eAgInSc1d1	starý
<g/>
.	.	kIx.	.
</s>
<s>
Neukazuje	ukazovat	k5eNaImIp3nS	ukazovat
žádné	žádný	k3yNgFnPc4	žádný
stopy	stopa	k1gFnPc4	stopa
podpovrchových	podpovrchový	k2eAgInPc2d1	podpovrchový
procesů	proces	k1gInPc2	proces
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
či	či	k8xC	či
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
zformován	zformovat	k5eAaPmNgInS	zformovat
pouze	pouze	k6eAd1	pouze
dopady	dopad	k1gInPc1	dopad
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgInPc1d1	výrazný
povrchové	povrchový	k2eAgInPc1d1	povrchový
útvary	útvar	k1gInPc1	útvar
tvoří	tvořit	k5eAaImIp3nP	tvořit
četné	četný	k2eAgFnPc1d1	četná
prstencové	prstencový	k2eAgFnPc1d1	prstencová
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
pásy	pás	k1gInPc1	pás
sekundárních	sekundární	k2eAgInPc2d1	sekundární
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
jizev	jizva	k1gFnPc2	jizva
<g/>
,	,	kIx,	,
hřebenů	hřeben	k1gInPc2	hřeben
a	a	k8xC	a
uloženin	uloženina	k1gFnPc2	uloženina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc1	povrch
členitý	členitý	k2eAgInSc1d1	členitý
a	a	k8xC	a
tvořený	tvořený	k2eAgInSc1d1	tvořený
malými	malý	k2eAgInPc7d1	malý
světlými	světlý	k2eAgInPc7d1	světlý
zmrzlými	zmrzlý	k2eAgInPc7d1	zmrzlý
depozity	depozit	k1gInPc7	depozit
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
vyvýšenin	vyvýšenina	k1gFnPc2	vyvýšenina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
jsou	být	k5eAaImIp3nP	být
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
hladkou	hladký	k2eAgFnSc7d1	hladká
vrstvou	vrstva	k1gFnSc7	vrstva
tmavého	tmavý	k2eAgInSc2d1	tmavý
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výsledek	výsledek	k1gInSc1	výsledek
degradace	degradace	k1gFnSc2	degradace
malých	malý	k2eAgInPc2d1	malý
útvarů	útvar	k1gInPc2	útvar
vlivem	vlivem	k7c2	vlivem
sublimace	sublimace	k1gFnSc2	sublimace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
absence	absence	k1gFnSc1	absence
malých	malý	k2eAgInPc2d1	malý
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
přítomnost	přítomnost	k1gFnSc1	přítomnost
množství	množství	k1gNnSc2	množství
malých	malý	k2eAgInPc2d1	malý
pahorků	pahorek	k1gInPc2	pahorek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
představují	představovat	k5eAaImIp3nP	představovat
jejich	jejich	k3xOp3gInPc4	jejich
zbytky	zbytek	k1gInPc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgNnSc1d1	absolutní
stáří	stáří	k1gNnSc1	stáří
povrchu	povrch	k1gInSc2	povrch
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Callisto	Callista	k1gMnSc5	Callista
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velice	velice	k6eAd1	velice
slabá	slabý	k2eAgFnSc1d1	slabá
atmosféra	atmosféra	k1gFnSc1	atmosféra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
molekulárním	molekulární	k2eAgInSc7d1	molekulární
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
pak	pak	k9	pak
poměrně	poměrně	k6eAd1	poměrně
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
ionosféra	ionosféra	k1gFnSc1	ionosféra
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pomalou	pomalý	k2eAgFnSc7d1	pomalá
akrecí	akrece	k1gFnSc7	akrece
z	z	k7c2	z
disku	disk	k1gInSc2	disk
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
obklopoval	obklopovat	k5eAaImAgMnS	obklopovat
Jupiter	Jupiter	k1gMnSc1	Jupiter
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
akrece	akrece	k1gFnSc1	akrece
probíhala	probíhat	k5eAaImAgFnS	probíhat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
slapové	slapový	k2eAgNnSc1d1	slapové
ohřívání	ohřívání	k1gNnSc1	ohřívání
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
Callisto	Callista	k1gMnSc5	Callista
dostatek	dostatek	k1gInSc4	dostatek
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
jeho	jeho	k3xOp3gFnSc1	jeho
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
diferenciace	diferenciace	k1gFnSc1	diferenciace
<g/>
.	.	kIx.	.
</s>
<s>
Pomalá	pomalý	k2eAgFnSc1d1	pomalá
konvekce	konvekce	k1gFnSc1	konvekce
uvnitř	uvnitř	k7c2	uvnitř
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
započala	započnout	k5eAaPmAgFnS	započnout
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
diferenciaci	diferenciace	k1gFnSc3	diferenciace
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
ke	k	k7c3	k
zformování	zformování	k1gNnSc3	zformování
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
okolo	okolo	k7c2	okolo
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
km	km	kA	km
a	a	k8xC	a
malého	malý	k2eAgNnSc2d1	malé
kamenitého	kamenitý	k2eAgNnSc2d1	kamenité
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
přítomnost	přítomnost	k1gFnSc1	přítomnost
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
nechává	nechávat	k5eAaImIp3nS	nechávat
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Callisto	Callista	k1gMnSc5	Callista
mohl	moct	k5eAaImAgMnS	moct
hostit	hostit	k5eAaImF	hostit
potenciální	potenciální	k2eAgInSc4d1	potenciální
mimozemský	mimozemský	k2eAgInSc4d1	mimozemský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
vznik	vznik	k1gInSc4	vznik
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
přívětivé	přívětivý	k2eAgFnPc1d1	přívětivá
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sousední	sousední	k2eAgFnSc2d1	sousední
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
zkoumaly	zkoumat	k5eAaImAgFnP	zkoumat
sondy	sonda	k1gFnPc1	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
<g/>
,	,	kIx,	,
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
<g/>
,	,	kIx,	,
Galileo	Galilea	k1gFnSc5	Galilea
a	a	k8xC	a
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
míře	míra	k1gFnSc3	míra
radiace	radiace	k1gFnSc2	radiace
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
Callisto	Callista	k1gMnSc5	Callista
jako	jako	k8xS	jako
o	o	k7c6	o
nejvhodnějším	vhodný	k2eAgNnSc6d3	nejvhodnější
místě	místo	k1gNnSc6	místo
pro	pro	k7c4	pro
případnou	případný	k2eAgFnSc4d1	případná
lidskou	lidský	k2eAgFnSc4d1	lidská
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
-130	-130	k4	-130
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
-190	-190	k4	-190
°	°	k?	°
<g/>
C.	C.	kA	C.
Callisto	Callista	k1gMnSc5	Callista
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
Galileem	Galileus	k1gMnSc7	Galileus
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1610	[number]	k4	1610
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
třemi	tři	k4xCgInPc7	tři
měsíci	měsíc	k1gInPc7	měsíc
Jupiteru	Jupiter	k1gMnSc3	Jupiter
<g/>
:	:	kIx,	:
Ganymedem	Ganymed	k1gMnSc7	Ganymed
<g/>
,	,	kIx,	,
Io	Io	k1gMnSc7	Io
a	a	k8xC	a
Europou	Europa	k1gFnSc7	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nespočtu	nespočet	k1gInSc2	nespočet
milenek	milenka	k1gFnPc2	milenka
Dia	Dia	k1gMnSc1	Dia
Kallistó	Kallistó	k1gMnSc1	Kallistó
(	(	kIx(	(
<g/>
Κ	Κ	k?	Κ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
nymfa	nymfa	k1gFnSc1	nymfa
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
lovu	lov	k1gInSc2	lov
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc4	jméno
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
Galileem	Galileum	k1gNnSc7	Galileum
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
ohledně	ohledně	k7c2	ohledně
připsání	připsání	k1gNnSc2	připsání
prvenství	prvenství	k1gNnSc2	prvenství
v	v	k7c6	v
objevení	objevení	k1gNnSc6	objevení
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Marius	Marius	k1gMnSc1	Marius
připsal	připsat	k5eAaPmAgMnS	připsat
nápad	nápad	k1gInSc4	nápad
Johnanu	Johnan	k1gMnSc3	Johnan
Keplerovi	Kepler	k1gMnSc3	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
pojmenování	pojmenování	k1gNnSc1	pojmenování
Callisto	Callista	k1gMnSc5	Callista
pro	pro	k7c4	pro
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
neujalo	ujmout	k5eNaPmAgNnS	ujmout
a	a	k8xC	a
měsíc	měsíc	k1gInSc4	měsíc
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Jupiter	Jupiter	k1gMnSc1	Jupiter
IV	IV	kA	IV
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
měsíc	měsíc	k1gInSc1	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
"	"	kIx"	"
značící	značící	k2eAgFnSc1d1	značící
jeho	jeho	k3xOp3gNnSc4	jeho
pořadí	pořadí	k1gNnSc4	pořadí
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
zase	zase	k9	zase
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
nejvzdálenější	vzdálený	k2eAgInSc4d3	nejvzdálenější
měsíc	měsíc	k1gInSc4	měsíc
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
obíhajících	obíhající	k2eAgInPc2d1	obíhající
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
obíhá	obíhat	k5eAaImIp3nS	obíhat
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
880	[number]	k4	880
000	[number]	k4	000
km	km	kA	km
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
26,3	[number]	k4	26,3
poloměrům	poloměr	k1gInPc3	poloměr
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
u	u	k7c2	u
třetího	třetí	k4xOgNnSc2	třetí
Galileovo	Galileův	k2eAgNnSc1d1	Galileovo
měsíce	měsíc	k1gInSc2	měsíc
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
070	[number]	k4	070
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
této	tento	k3xDgFnSc2	tento
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Callisto	Callista	k1gMnSc5	Callista
nepodílí	podílet	k5eNaImIp3nP	podílet
na	na	k7c4	na
orbitální	orbitální	k2eAgFnSc4d1	orbitální
rezonanci	rezonance	k1gFnSc4	rezonance
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
dalšími	další	k2eAgInPc7d1	další
Galileovými	Galileův	k2eAgInPc7d1	Galileův
měsíci	měsíc	k1gInPc7	měsíc
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
nepodílel	podílet	k5eNaImAgMnS	podílet
ani	ani	k8xC	ani
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
dalších	další	k2eAgInPc2d1	další
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
planetárních	planetární	k2eAgInPc2d1	planetární
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
i	i	k9	i
rotace	rotace	k1gFnSc1	rotace
Callisto	Callista	k1gMnSc5	Callista
vázaná	vázaný	k2eAgNnPc1d1	vázané
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Callisto	Callista	k1gMnSc5	Callista
stejně	stejně	k6eAd1	stejně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
jako	jako	k8xC	jako
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přibližně	přibližně	k6eAd1	přibližně
16,7	[number]	k4	16,7
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
excentrická	excentrický	k2eAgFnSc1d1	excentrická
a	a	k8xC	a
ukloněná	ukloněný	k2eAgFnSc1d1	ukloněná
k	k	k7c3	k
Jupiterovu	Jupiterův	k2eAgInSc3d1	Jupiterův
rovníku	rovník	k1gInSc3	rovník
s	s	k7c7	s
orbitální	orbitální	k2eAgFnSc7d1	orbitální
excentricitou	excentricita	k1gFnSc7	excentricita
a	a	k8xC	a
inklinací	inklinace	k1gFnSc7	inklinace
měnící	měnící	k2eAgFnSc2d1	měnící
se	se	k3xPyFc4	se
kvazi-periodicky	kvazieriodicky	k6eAd1	kvazi-periodicky
vlivem	vlivem	k7c2	vlivem
slunečních	sluneční	k2eAgFnPc2d1	sluneční
a	a	k8xC	a
planetárních	planetární	k2eAgFnPc2d1	planetární
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
perturbací	perturbace	k1gFnPc2	perturbace
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
změn	změna	k1gFnPc2	změna
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
0,0072	[number]	k4	0,0072
<g/>
–	–	k?	–
<g/>
0,0076	[number]	k4	0,0076
respektive	respektive	k9	respektive
0,20	[number]	k4	0,20
<g/>
–	–	k?	–
<g/>
0,60	[number]	k4	0,60
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
orbitální	orbitální	k2eAgFnPc4d1	orbitální
variace	variace	k1gFnPc4	variace
způsobuji	způsobovat	k5eAaImIp1nS	způsobovat
sklony	sklon	k1gInPc4	sklon
v	v	k7c6	v
rotační	rotační	k2eAgFnSc6d1	rotační
ose	osa	k1gFnSc6	osa
(	(	kIx(	(
<g/>
úhel	úhel	k1gInSc4	úhel
mezi	mezi	k7c7	mezi
rotační	rotační	k2eAgFnSc7d1	rotační
a	a	k8xC	a
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
osou	osa	k1gFnSc7	osa
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
0,4	[number]	k4	0,4
až	až	k9	až
1,6	[number]	k4	1,6
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
izolace	izolace	k1gFnSc1	izolace
Callisto	Callista	k1gMnSc5	Callista
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
znatelně	znatelně	k6eAd1	znatelně
zahřát	zahřát	k5eAaPmF	zahřát
slapovým	slapový	k2eAgNnSc7d1	slapové
teplem	teplo	k1gNnSc7	teplo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
důležité	důležitý	k2eAgInPc4d1	důležitý
důsledky	důsledek	k1gInPc4	důsledek
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
evoluci	evoluce	k1gFnSc4	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
taktéž	taktéž	k?	taktéž
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tok	tok	k1gInSc1	tok
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
planetární	planetární	k2eAgFnSc2d1	planetární
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
na	na	k7c4	na
měsíční	měsíční	k2eAgInSc4d1	měsíční
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
až	až	k9	až
300	[number]	k4	300
krát	krát	k6eAd1	krát
méně	málo	k6eAd2	málo
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
u	u	k7c2	u
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgInPc2d1	další
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
ozáření	ozáření	k1gNnSc1	ozáření
nabitými	nabitý	k2eAgFnPc7d1	nabitá
částicemi	částice	k1gFnPc7	částice
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc4d1	malý
efekt	efekt	k1gInSc4	efekt
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
radiace	radiace	k1gFnSc1	radiace
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
0,01	[number]	k4	0,01
rem	rem	k?	rem
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
mSv	mSv	k?	mSv
<g/>
)	)	kIx)	)
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
1,83	[number]	k4	1,83
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
složení	složení	k1gNnSc4	složení
z	z	k7c2	z
kamenného	kamenný	k2eAgInSc2d1	kamenný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
stejném	stejný	k2eAgInSc6d1	stejný
poměru	poměr	k1gInSc6	poměr
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
zastoupením	zastoupení	k1gNnSc7	zastoupení
nestálých	stálý	k2eNgInPc2d1	nestálý
ledů	led	k1gInPc2	led
jako	jako	k8xS	jako
například	například	k6eAd1	například
čpavek	čpavek	k1gInSc1	čpavek
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnostní	hmotnostní	k2eAgNnSc1d1	hmotnostní
zastoupení	zastoupení	k1gNnSc1	zastoupení
ledů	led	k1gInPc2	led
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
49	[number]	k4	49
až	až	k9	až
55	[number]	k4	55
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
složení	složení	k1gNnSc4	složení
horninového	horninový	k2eAgInSc2d1	horninový
pláště	plášť	k1gInSc2	plášť
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
chondritů	chondrit	k1gInPc2	chondrit
typu	typ	k1gInSc2	typ
L	L	kA	L
či	či	k8xC	či
LL	LL	kA	LL
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
chondritů	chondrit	k1gInPc2	chondrit
typu	typ	k1gInSc2	typ
H	H	kA	H
liší	lišit	k5eAaImIp3nP	lišit
především	především	k6eAd1	především
menším	malý	k2eAgNnSc7d2	menší
zastoupením	zastoupení	k1gNnSc7	zastoupení
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
vyskytujícím	vyskytující	k2eAgMnSc7d1	vyskytující
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
oxidů	oxid	k1gInPc2	oxid
a	a	k8xC	a
jen	jen	k9	jen
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
železa	železo	k1gNnSc2	železo
metalického	metalický	k2eAgNnSc2d1	metalické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
poměr	poměr	k1gInSc1	poměr
železa	železo	k1gNnSc2	železo
vůči	vůči	k7c3	vůči
křemičitanům	křemičitan	k1gInPc3	křemičitan
0,9	[number]	k4	0,9
ku	k	k7c3	k
1,3	[number]	k4	1,3
<g/>
,	,	kIx,	,
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgNnSc1d1	povrchové
albedo	albedo	k1gNnSc1	albedo
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
povrchové	povrchový	k2eAgNnSc1d1	povrchové
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc4d1	podobné
jeho	jeho	k3xOp3gNnSc3	jeho
celkovému	celkový	k2eAgNnSc3d1	celkové
složení	složení	k1gNnSc3	složení
<g/>
.	.	kIx.	.
</s>
<s>
Infračervená	infračervený	k2eAgFnSc1d1	infračervená
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
odhalila	odhalit	k5eAaPmAgFnS	odhalit
přítomnost	přítomnost	k1gFnSc4	přítomnost
absorpčních	absorpční	k2eAgFnPc2d1	absorpční
čar	čára	k1gFnPc2	čára
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
na	na	k7c6	na
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
1,04	[number]	k4	1,04
<g/>
,	,	kIx,	,
1,25	[number]	k4	1,25
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
<g/>
,	,	kIx,	,
2,0	[number]	k4	2,0
a	a	k8xC	a
3,0	[number]	k4	3,0
mikrometru	mikrometr	k1gInSc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Callisto	Callista	k1gMnSc5	Callista
všudypřítomným	všudypřítomný	k2eAgFnPc3d1	všudypřítomná
<g/>
,	,	kIx,	,
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
podílem	podíl	k1gInSc7	podíl
asi	asi	k9	asi
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
snímků	snímek	k1gInPc2	snímek
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
a	a	k8xC	a
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
spektru	spektrum	k1gNnSc6	spektrum
získaných	získaný	k2eAgInPc2d1	získaný
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
a	a	k8xC	a
pozorování	pozorování	k1gNnSc2	pozorování
provedená	provedený	k2eAgFnSc1d1	provedená
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
odhalila	odhalit	k5eAaPmAgFnS	odhalit
také	také	k9	také
různé	různý	k2eAgInPc4d1	různý
neledové	ledový	k2eNgInPc4d1	ledový
materiály	materiál	k1gInPc4	materiál
<g/>
:	:	kIx,	:
hořčíkové	hořčíkový	k2eAgNnSc4d1	hořčíkové
a	a	k8xC	a
železité	železitý	k2eAgNnSc4d1	železité
ložisko	ložisko	k1gNnSc4	ložisko
hydratovaných	hydratovaný	k2eAgInPc2d1	hydratovaný
křemičitanů	křemičitan	k1gInPc2	křemičitan
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
a	a	k8xC	a
možná	možná	k9	možná
amoniak	amoniak	k1gInSc1	amoniak
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Spektrální	spektrální	k2eAgNnPc1d1	spektrální
data	datum	k1gNnPc1	datum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíční	měsíční	k2eAgInSc1d1	měsíční
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
extrémně	extrémně	k6eAd1	extrémně
různorodý	různorodý	k2eAgMnSc1d1	různorodý
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgInPc4d1	Malé
kousky	kousek	k1gInPc4	kousek
ledu	led	k1gInSc2	led
z	z	k7c2	z
čisté	čistý	k2eAgFnSc2d1	čistá
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
smíšeny	smísit	k5eAaPmNgInP	smísit
s	s	k7c7	s
kousky	kousek	k1gInPc7	kousek
směsi	směs	k1gFnSc2	směs
ledu	led	k1gInSc2	led
a	a	k8xC	a
kamení	kamení	k1gNnSc1	kamení
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
navazují	navazovat	k5eAaImIp3nP	navazovat
tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
složené	složený	k2eAgFnPc1d1	složená
z	z	k7c2	z
neledového	ledový	k2eNgInSc2d1	ledový
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
asymetrický	asymetrický	k2eAgInSc1d1	asymetrický
<g/>
;	;	kIx,	;
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
strana	strana	k1gFnSc1	strana
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
oběhu	oběh	k1gInSc2	oběh
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tmavší	tmavý	k2eAgFnSc1d2	tmavší
než	než	k8xS	než
strana	strana	k1gFnSc1	strana
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
obrácená	obrácený	k2eAgFnSc1d1	obrácená
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgFnSc1d2	světlejší
než	než	k8xS	než
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
obohacena	obohatit	k5eAaPmNgFnS	obohatit
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
čerstvých	čerstvý	k2eAgInPc2d1	čerstvý
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
taktéž	taktéž	k?	taktéž
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
známky	známka	k1gFnPc4	známka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
obohaceny	obohatit	k5eAaPmNgFnP	obohatit
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
tmavých	tmavý	k2eAgFnPc2d1	tmavá
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
asteroidů	asteroid	k1gInPc2	asteroid
typu	typ	k1gInSc2	typ
D	D	kA	D
<g/>
,	,	kIx,	,
jejíchž	jejíž	k3xOyRp3gInPc6	jejíž
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
uhlíkatým	uhlíkatý	k2eAgInSc7d1	uhlíkatý
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Callisto	Callista	k1gMnSc5	Callista
posetý	posetý	k2eAgInSc4d1	posetý
krátery	kráter	k1gInPc4	kráter
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
studené	studený	k2eAgFnSc6d1	studená
<g/>
,	,	kIx,	,
ztuhlé	ztuhlý	k2eAgFnSc3d1	ztuhlá
a	a	k8xC	a
ledové	ledový	k2eAgFnSc3d1	ledová
litosféře	litosféra	k1gFnSc3	litosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
80	[number]	k4	80
až	až	k9	až
150	[number]	k4	150
km	km	kA	km
mocná	mocný	k2eAgFnSc1d1	mocná
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
slaný	slaný	k2eAgInSc1d1	slaný
oceán	oceán	k1gInSc1	oceán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
50	[number]	k4	50
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
studie	studie	k1gFnPc4	studie
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Callisto	Callista	k1gMnSc5	Callista
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
proměnné	proměnná	k1gFnPc4	proměnná
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
jako	jako	k8xS	jako
ideálně	ideálně	k6eAd1	ideálně
vodivá	vodivý	k2eAgFnSc1d1	vodivá
koule	koule	k1gFnSc1	koule
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pole	pole	k1gNnSc1	pole
nemůže	moct	k5eNaImIp3nS	moct
proniknout	proniknout	k5eAaPmF	proniknout
dovnitř	dovnitř	k6eAd1	dovnitř
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc4	možnost
vrstvy	vrstva	k1gFnSc2	vrstva
tvořené	tvořený	k2eAgFnSc2d1	tvořená
vysoce	vysoce	k6eAd1	vysoce
vodivou	vodivý	k2eAgFnSc7d1	vodivá
tekutinou	tekutina	k1gFnSc7	tekutina
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
více	hodně	k6eAd2	hodně
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
voda	voda	k1gFnSc1	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
čpavku	čpavek	k1gInSc2	čpavek
či	či	k8xC	či
jiné	jiný	k2eAgFnSc2d1	jiná
nemrznoucí	mrznoucí	k2eNgFnSc2d1	nemrznoucí
směsi	směs	k1gFnSc2	směs
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
zastoupení	zastoupení	k1gNnSc6	zastoupení
minimálně	minimálně	k6eAd1	minimálně
5	[number]	k4	5
hmotnostních	hmotnostní	k2eAgNnPc2d1	hmotnostní
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
oceán	oceán	k1gInSc1	oceán
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
250	[number]	k4	250
až	až	k9	až
300	[number]	k4	300
km	km	kA	km
mocný	mocný	k2eAgMnSc1d1	mocný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
oceán	oceán	k1gInSc1	oceán
neexistoval	existovat	k5eNaImAgInS	existovat
<g/>
,	,	kIx,	,
ledová	ledový	k2eAgFnSc1d1	ledová
kůra	kůra	k1gFnSc1	kůra
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tlustší	tlustý	k2eAgFnSc1d2	tlustší
a	a	k8xC	a
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
mocnosti	mocnost	k1gFnPc4	mocnost
okolo	okolo	k7c2	okolo
300	[number]	k4	300
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
litosférou	litosféra	k1gFnSc7	litosféra
a	a	k8xC	a
případným	případný	k2eAgInSc7d1	případný
oceánem	oceán	k1gInSc7	oceán
není	být	k5eNaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
Callisto	Callista	k1gMnSc5	Callista
zcela	zcela	k6eAd1	zcela
jednotvárná	jednotvárný	k2eAgNnPc1d1	jednotvárné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
výrazně	výrazně	k6eAd1	výrazně
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
pořízené	pořízený	k2eAgInPc1d1	pořízený
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
bezrozměrný	bezrozměrný	k2eAgInSc1d1	bezrozměrný
moment	moment	k1gInSc1	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
–	–	k?	–
<g/>
0.3549	[number]	k4	0.3549
±	±	k?	±
0.0042	[number]	k4	0.0042
<g/>
–	–	k?	–
<g/>
určený	určený	k2eAgMnSc1d1	určený
během	během	k7c2	během
těsných	těsný	k2eAgInPc2d1	těsný
průletů	průlet	k1gInPc2	průlet
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřek	vnitřek	k1gInSc1	vnitřek
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
stlačenými	stlačený	k2eAgFnPc7d1	stlačená
horninami	hornina	k1gFnPc7	hornina
a	a	k8xC	a
směsí	směs	k1gFnSc7	směs
ledů	led	k1gInPc2	led
s	s	k7c7	s
narůstajícím	narůstající	k2eAgInSc7d1	narůstající
obsahem	obsah	k1gInSc7	obsah
hornin	hornina	k1gFnPc2	hornina
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
hloubkou	hloubka	k1gFnSc7	hloubka
způsobeném	způsobený	k2eAgInSc6d1	způsobený
částečným	částečný	k2eAgNnSc7d1	částečné
usazováním	usazování	k1gNnSc7	usazování
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
Callisto	Callista	k1gMnSc5	Callista
je	on	k3xPp3gNnPc4	on
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
diferenciovaný	diferenciovaný	k2eAgMnSc1d1	diferenciovaný
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
moment	moment	k1gInSc4	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
malého	malý	k2eAgNnSc2d1	malé
silikátového	silikátový	k2eAgNnSc2d1	silikátové
jádra	jádro	k1gNnSc2	jádro
uprostřed	uprostřed	k7c2	uprostřed
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
takového	takový	k3xDgNnSc2	takový
jádra	jádro	k1gNnSc2	jádro
by	by	kYmCp3nS	by
nemohl	moct	k5eNaImAgInS	moct
překročit	překročit	k5eAaPmF	překročit
600	[number]	k4	600
km	km	kA	km
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hustota	hustota	k1gFnSc1	hustota
by	by	kYmCp3nS	by
ležela	ležet	k5eAaImAgFnS	ležet
mezi	mezi	k7c7	mezi
3,1	[number]	k4	3,1
<g/>
–	–	k?	–
<g/>
3,6	[number]	k4	3,6
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
geologických	geologický	k2eAgInPc2d1	geologický
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Prastarý	prastarý	k2eAgInSc4d1	prastarý
povrch	povrch	k1gInSc4	povrch
Callisto	Callista	k1gMnSc5	Callista
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
krátery	kráter	k1gInPc1	kráter
posetých	posetý	k2eAgInPc2d1	posetý
povrchů	povrch	k1gInPc2	povrch
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
četnost	četnost	k1gFnSc4	četnost
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
blízko	blízko	k7c2	blízko
nasycení	nasycení	k1gNnSc2	nasycení
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc1	vznik
nového	nový	k2eAgInSc2d1	nový
kráteru	kráter	k1gInSc2	kráter
by	by	kYmCp3nS	by
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
starší	starý	k2eAgInSc1d2	starší
kráter	kráter	k1gInSc1	kráter
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
erodován	erodovat	k5eAaImNgInS	erodovat
<g/>
.	.	kIx.	.
</s>
<s>
Morfologie	morfologie	k1gFnSc1	morfologie
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadná	snadný	k2eAgFnSc1d1	snadná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nenachází	nacházet	k5eNaImIp3nP	nacházet
žádné	žádný	k3yNgFnPc1	žádný
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
sopky	sopka	k1gFnPc1	sopka
a	a	k8xC	a
ani	ani	k8xC	ani
tektonické	tektonický	k2eAgInPc4d1	tektonický
útvary	útvar	k1gInPc4	útvar
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
endogenními	endogenní	k2eAgInPc7d1	endogenní
pochody	pochod	k1gInPc7	pochod
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Impaktní	Impaktní	k2eAgInPc1d1	Impaktní
krátery	kráter	k1gInPc1	kráter
a	a	k8xC	a
několik	několik	k4yIc1	několik
prstencových	prstencový	k2eAgFnPc2d1	prstencová
struktur	struktura	k1gFnPc2	struktura
společně	společně	k6eAd1	společně
s	s	k7c7	s
doprovodnými	doprovodný	k2eAgFnPc7d1	doprovodná
trhlinami	trhlina	k1gFnPc7	trhlina
<g/>
,	,	kIx,	,
srázy	sráz	k1gInPc7	sráz
a	a	k8xC	a
usazeným	usazený	k2eAgInSc7d1	usazený
materiálem	materiál	k1gInSc7	materiál
tvoří	tvořit	k5eAaImIp3nP	tvořit
jediné	jediný	k2eAgInPc1d1	jediný
velké	velký	k2eAgInPc1d1	velký
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Callisto	Callista	k1gMnSc5	Callista
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
geologicky	geologicky	k6eAd1	geologicky
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
:	:	kIx,	:
pláně	pláň	k1gFnPc4	pláň
poseté	posetý	k2eAgFnPc4d1	posetá
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
světlé	světlý	k2eAgFnPc4d1	světlá
pláně	pláň	k1gFnPc4	pláň
<g/>
,	,	kIx,	,
jasné	jasný	k2eAgFnPc4d1	jasná
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc4d1	tmavá
hladké	hladký	k2eAgFnPc4d1	hladká
pláně	pláň	k1gFnPc4	pláň
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
jednotek	jednotka	k1gFnPc2	jednotka
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
několika	několik	k4yIc7	několik
prstencovými	prstencový	k2eAgFnPc7d1	prstencová
strukturami	struktura	k1gFnPc7	struktura
a	a	k8xC	a
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Pláně	pláň	k1gFnPc4	pláň
poseté	posetý	k2eAgFnPc4d1	posetá
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
starou	starý	k2eAgFnSc4d1	stará
litosféru	litosféra	k1gFnSc4	litosféra
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
směsí	směs	k1gFnSc7	směs
ledu	led	k1gInSc2	led
a	a	k8xC	a
horninového	horninový	k2eAgInSc2d1	horninový
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgFnPc1d1	světlá
pláně	pláň	k1gFnPc1	pláň
tvoří	tvořit	k5eAaImIp3nP	tvořit
jasné	jasný	k2eAgInPc1d1	jasný
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
jako	jako	k8xS	jako
Burr	Burr	k1gInSc1	Burr
a	a	k8xC	a
Lofn	Lofn	k1gInSc1	Lofn
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
zbytky	zbytek	k1gInPc1	zbytek
téměř	téměř	k6eAd1	téměř
smazaných	smazaný	k2eAgInPc2d1	smazaný
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
centrální	centrální	k2eAgFnSc6d1	centrální
oblasti	oblast	k1gFnSc6	oblast
prstencových	prstencový	k2eAgFnPc2d1	prstencová
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlé	světlý	k2eAgFnPc1d1	světlá
pláně	pláň	k1gFnPc1	pláň
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
depozice	depozice	k1gFnSc2	depozice
ledových	ledový	k2eAgFnPc2d1	ledová
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
impaktů	impakt	k1gInPc2	impakt
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgFnPc1d1	světlá
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgFnPc1d1	hladká
planiny	planina	k1gFnPc1	planina
tvoří	tvořit	k5eAaImIp3nP	tvořit
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hřbetů	hřbet	k1gInPc2	hřbet
a	a	k8xC	a
údolí	údolí	k1gNnSc2	údolí
spojených	spojený	k2eAgFnPc2d1	spojená
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
kráterů	kráter	k1gInPc2	kráter
Valhalla	Valhallo	k1gNnSc2	Valhallo
a	a	k8xC	a
Asgard	Asgarda	k1gFnPc2	Asgarda
a	a	k8xC	a
jako	jako	k9	jako
izolovaná	izolovaný	k2eAgNnPc4d1	izolované
místa	místo	k1gNnPc4	místo
v	v	k7c4	v
krátery	kráter	k1gInPc4	kráter
posetých	posetý	k2eAgFnPc6d1	posetá
planinách	planina	k1gFnPc6	planina
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
endogenní	endogenní	k2eAgFnSc7d1	endogenní
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snímky	snímek	k1gInPc1	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlé	světlý	k2eAgFnPc1d1	světlá
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgFnPc1d1	hladká
planiny	planina	k1gFnPc1	planina
korelují	korelovat	k5eAaImIp3nP	korelovat
silně	silně	k6eAd1	silně
popraskaným	popraskaný	k2eAgInSc7d1	popraskaný
a	a	k8xC	a
kopcovitým	kopcovitý	k2eAgInSc7d1	kopcovitý
terénem	terén	k1gInSc7	terén
a	a	k8xC	a
neukazují	ukazovat	k5eNaImIp3nP	ukazovat
žádné	žádný	k3yNgFnPc4	žádný
známky	známka	k1gFnPc4	známka
přetvoření	přetvoření	k1gNnSc2	přetvoření
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
odhalily	odhalit	k5eAaPmAgFnP	odhalit
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgFnPc1d1	hladká
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
obecnou	obecný	k2eAgFnSc7d1	obecná
velikostí	velikost	k1gFnSc7	velikost
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vypadají	vypadat	k5eAaPmIp3nP	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
obepínaly	obepínat	k5eAaImAgFnP	obepínat
okolní	okolní	k2eAgInSc4d1	okolní
terén	terén	k1gInSc4	terén
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
depozity	depozit	k1gInPc4	depozit
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
kryovulkanismem	kryovulkanismus	k1gInSc7	kryovulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
světlé	světlý	k2eAgFnPc1d1	světlá
tak	tak	k9	tak
i	i	k9	i
různorodé	různorodý	k2eAgFnSc2d1	různorodá
hladké	hladký	k2eAgFnSc2d1	hladká
planiny	planina	k1gFnSc2	planina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgInPc1d2	mladší
a	a	k8xC	a
méně	málo	k6eAd2	málo
poseté	posetý	k2eAgInPc1d1	posetý
krátery	kráter	k1gInPc1	kráter
než	než	k8xS	než
okolní	okolní	k2eAgInPc1d1	okolní
krátery	kráter	k1gInPc1	kráter
poseté	posetý	k2eAgFnSc2d1	posetá
planiny	planina	k1gFnSc2	planina
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
0,1	[number]	k4	0,1
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
rozlišení	rozlišení	k1gNnSc2	rozlišení
pořízených	pořízený	k2eAgInPc2d1	pořízený
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
až	až	k9	až
přes	přes	k7c4	přes
100	[number]	k4	100
km	km	kA	km
bez	bez	k7c2	bez
započítání	započítání	k1gNnSc2	započítání
prstencových	prstencový	k2eAgFnPc2d1	prstencová
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgInPc1d1	Malé
krátery	kráter	k1gInPc1	kráter
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
5	[number]	k4	5
km	km	kA	km
mají	mít	k5eAaImIp3nP	mít
jednoduše	jednoduše	k6eAd1	jednoduše
mísovitý	mísovitý	k2eAgInSc4d1	mísovitý
tvar	tvar	k1gInSc4	tvar
či	či	k8xC	či
rovné	rovný	k2eAgNnSc4d1	rovné
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
5	[number]	k4	5
km	km	kA	km
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
40	[number]	k4	40
km	km	kA	km
mají	mít	k5eAaImIp3nP	mít
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
centrální	centrální	k2eAgInSc1d1	centrální
vrcholek	vrcholek	k1gInSc1	vrcholek
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
impaktní	impaktní	k2eAgFnPc1d1	impaktní
struktury	struktura	k1gFnPc1	struktura
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
25	[number]	k4	25
až	až	k8xS	až
100	[number]	k4	100
km	km	kA	km
mají	mít	k5eAaImIp3nP	mít
centrální	centrální	k2eAgFnSc4d1	centrální
depresi	deprese	k1gFnSc4	deprese
namísto	namísto	k7c2	namísto
vrcholku	vrcholek	k1gInSc2	vrcholek
jako	jako	k8xC	jako
například	například	k6eAd1	například
kráter	kráter	k1gInSc1	kráter
Tindr	Tindr	k1gInSc1	Tindr
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
krátery	kráter	k1gInPc1	kráter
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
přes	přes	k7c4	přes
60	[number]	k4	60
km	km	kA	km
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
centrální	centrální	k2eAgInSc4d1	centrální
dóm	dóm	k1gInSc4	dóm
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
tektonického	tektonický	k2eAgInSc2d1	tektonický
výzdvihu	výzdvih	k1gInSc2	výzdvih
centrální	centrální	k2eAgFnSc2d1	centrální
části	část	k1gFnSc2	část
kráteru	kráter	k1gInSc2	kráter
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kráterů	kráter	k1gInPc2	kráter
Doh	Doh	k1gFnPc2	Doh
a	a	k8xC	a
Hár	Hár	k1gFnPc2	Hár
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
množství	množství	k1gNnSc4	množství
velmi	velmi	k6eAd1	velmi
velkých	velký	k2eAgInPc2d1	velký
kráterů	kráter	k1gInPc2	kráter
přesahujících	přesahující	k2eAgInPc2d1	přesahující
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
světlé	světlý	k2eAgInPc1d1	světlý
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
anomální	anomální	k2eAgFnSc4d1	anomální
geometrii	geometrie	k1gFnSc4	geometrie
centrálního	centrální	k2eAgInSc2d1	centrální
dómu	dóm	k1gInSc2	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
na	na	k7c4	na
Callisto	Callista	k1gMnSc5	Callista
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mělčí	mělký	k2eAgInPc1d2	mělčí
než	než	k8xS	než
obdobné	obdobný	k2eAgInPc1d1	obdobný
krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
impaktními	impaktní	k2eAgInPc7d1	impaktní
útvary	útvar	k1gInPc7	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Callisto	Callista	k1gMnSc5	Callista
jsou	být	k5eAaImIp3nP	být
mnohočetné	mnohočetný	k2eAgFnSc2d1	mnohočetná
prstencové	prstencový	k2eAgFnSc2d1	prstencová
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
enormní	enormní	k2eAgFnPc1d1	enormní
<g/>
.	.	kIx.	.
</s>
<s>
Kráter	kráter	k1gInSc1	kráter
Valhalla	Valhallo	k1gNnSc2	Valhallo
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
světlejší	světlý	k2eAgFnSc4d2	světlejší
centrální	centrální	k2eAgFnSc4d1	centrální
oblast	oblast	k1gFnSc4	oblast
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
600	[number]	k4	600
km	km	kA	km
a	a	k8xC	a
prstence	prstenec	k1gInPc4	prstenec
sahající	sahající	k2eAgInPc4d1	sahající
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
1800	[number]	k4	1800
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
kráter	kráter	k1gInSc1	kráter
je	být	k5eAaImIp3nS	být
kráter	kráter	k1gInSc4	kráter
Asgard	Asgard	k1gMnSc1	Asgard
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
1600	[number]	k4	1600
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Prstencové	prstencový	k2eAgFnPc1d1	prstencová
struktury	struktura	k1gFnPc1	struktura
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc4	důsledek
podopadových	podopadový	k2eAgFnPc2d1	podopadový
deformací	deformace	k1gFnPc2	deformace
projevujících	projevující	k2eAgFnPc2d1	projevující
se	se	k3xPyFc4	se
soustředným	soustředný	k2eAgNnSc7d1	soustředné
popraskáním	popraskání	k1gNnSc7	popraskání
litosféry	litosféra	k1gFnSc2	litosféra
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
vrstvě	vrstva	k1gFnSc6	vrstva
měkkého	měkký	k2eAgInSc2d1	měkký
či	či	k8xC	či
tekutého	tekutý	k2eAgInSc2d1	tekutý
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
útvary	útvar	k1gInPc7	útvar
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
Catenae	Catenae	k1gFnSc4	Catenae
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Gomul	Gomul	k1gInSc1	Gomul
Catena	Cateno	k1gNnSc2	Cateno
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
řetězy	řetěz	k1gInPc4	řetěz
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
napříč	napříč	k7c7	napříč
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dopadem	dopad	k1gInSc7	dopad
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
slapovými	slapový	k2eAgFnPc7d1	slapová
silami	síla	k1gFnPc7	síla
při	při	k7c6	při
blízkém	blízký	k2eAgInSc6d1	blízký
průletu	průlet	k1gInSc6	průlet
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
roztrhány	roztrhán	k2eAgInPc4d1	roztrhán
a	a	k8xC	a
následně	následně	k6eAd1	následně
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
dopadu	dopad	k1gInSc2	dopad
tělesa	těleso	k1gNnPc1	těleso
pod	pod	k7c7	pod
nízkým	nízký	k2eAgInSc7d1	nízký
úhlem	úhel	k1gInSc7	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Historickým	historický	k2eAgInSc7d1	historický
případem	případ	k1gInSc7	případ
rozpadu	rozpad	k1gInSc2	rozpad
tělesa	těleso	k1gNnSc2	těleso
vlivem	vlivem	k7c2	vlivem
gravitace	gravitace	k1gFnSc2	gravitace
Jupiteru	Jupiter	k1gInSc2	Jupiter
byl	být	k5eAaImAgInS	být
rozpad	rozpad	k1gInSc1	rozpad
komety	kometa	k1gFnSc2	kometa
Shoemaker-Levy	Shoemaker-Leva	k1gFnSc2	Shoemaker-Leva
9	[number]	k4	9
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
následně	následně	k6eAd1	následně
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
narazila	narazit	k5eAaPmAgFnS	narazit
do	do	k7c2	do
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
oblasti	oblast	k1gFnPc1	oblast
tvořené	tvořený	k2eAgFnPc1d1	tvořená
čistým	čistý	k2eAgInSc7d1	čistý
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
albedem	albed	k1gInSc7	albed
okolo	okolo	k7c2	okolo
80	[number]	k4	80
%	%	kIx~	%
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Callisto	Callista	k1gMnSc5	Callista
obklopené	obklopený	k2eAgFnPc4d1	obklopená
mnohem	mnohem	k6eAd1	mnohem
tmavším	tmavý	k2eAgInSc7d2	tmavší
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
pořízené	pořízený	k2eAgFnSc2d1	pořízená
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
světlejší	světlý	k2eAgFnPc1d2	světlejší
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c4	na
vyvýšená	vyvýšený	k2eAgNnPc4d1	vyvýšené
místa	místo	k1gNnPc4	místo
povrchu	povrch	k1gInSc2	povrch
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
okraje	okraj	k1gInPc1	okraj
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
srázy	sráz	k1gInPc4	sráz
<g/>
,	,	kIx,	,
hřbety	hřbet	k1gInPc4	hřbet
a	a	k8xC	a
pahorky	pahorek	k1gInPc4	pahorek
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
zmrzlých	zmrzlý	k2eAgInPc2d1	zmrzlý
vodních	vodní	k2eAgInPc2d1	vodní
depozitů	depozit	k1gInPc2	depozit
<g/>
.	.	kIx.	.
</s>
<s>
Tmavý	tmavý	k2eAgInSc1d1	tmavý
materiál	materiál	k1gInSc1	materiál
obvykle	obvykle	k6eAd1	obvykle
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
a	a	k8xC	a
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
přikrývá	přikrývat	k5eAaImIp3nS	přikrývat
světlejší	světlý	k2eAgInPc4d2	světlejší
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
dna	dna	k1gFnSc1	dna
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
větších	veliký	k2eAgInPc2d2	veliký
než	než	k8xS	než
5	[number]	k4	5
km	km	kA	km
a	a	k8xC	a
mezikráterové	mezikráterový	k2eAgFnSc2d1	mezikráterový
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
méně	málo	k6eAd2	málo
než	než	k8xS	než
kilometru	kilometr	k1gInSc2	kilometr
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
povrch	povrch	k1gInSc1	povrch
Callisto	Callista	k1gMnSc5	Callista
více	hodně	k6eAd2	hodně
degradován	degradován	k2eAgInSc4d1	degradován
než	než	k8xS	než
povrch	povrch	k1gInSc4	povrch
ostatních	ostatní	k2eAgInPc2d1	ostatní
ledových	ledový	k2eAgInPc2d1	ledový
měsíců	měsíc	k1gInPc2	měsíc
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
chybí	chybět	k5eAaImIp3nP	chybět
malé	malý	k2eAgInPc1d1	malý
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
menším	malý	k2eAgInSc7d2	menší
než	než	k8xS	než
1	[number]	k4	1
km	km	kA	km
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
například	například	k6eAd1	například
s	s	k7c7	s
tmavými	tmavý	k2eAgFnPc7d1	tmavá
planinami	planina	k1gFnPc7	planina
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
malých	malý	k2eAgInPc2d1	malý
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všudypřítomnými	všudypřítomný	k2eAgInPc7d1	všudypřítomný
povrchovými	povrchový	k2eAgInPc7d1	povrchový
útvary	útvar	k1gInPc7	útvar
malé	malý	k2eAgInPc4d1	malý
pahorky	pahorek	k1gInPc4	pahorek
a	a	k8xC	a
deprese	deprese	k1gFnPc4	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pahorky	pahorek	k1gInPc1	pahorek
představují	představovat	k5eAaImIp3nP	představovat
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
okrajů	okraj	k1gInPc2	okraj
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
erodovány	erodovat	k5eAaImNgInP	erodovat
zatím	zatím	k6eAd1	zatím
neznámým	známý	k2eNgInSc7d1	neznámý
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejpravděpodobnější	pravděpodobný	k2eAgMnSc1d3	nejpravděpodobnější
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
pomalý	pomalý	k2eAgInSc1d1	pomalý
proces	proces	k1gInSc1	proces
sublimace	sublimace	k1gFnSc2	sublimace
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
teplotou	teplota	k1gFnSc7	teplota
156	[number]	k4	156
K	K	kA	K
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Callisto	Callista	k1gMnSc5	Callista
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
v	v	k7c6	v
subsolárním	subsolární	k2eAgInSc6d1	subsolární
bodu	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
sublimace	sublimace	k1gFnSc1	sublimace
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
těkavých	těkavý	k2eAgFnPc2d1	těkavá
složek	složka	k1gFnPc2	složka
ze	z	k7c2	z
špinavého	špinavý	k2eAgInSc2d1	špinavý
ledu	led	k1gInSc2	led
tvořícího	tvořící	k2eAgNnSc2d1	tvořící
podloží	podloží	k1gNnSc2	podloží
způsobí	způsobit	k5eAaPmIp3nS	způsobit
jeho	jeho	k3xOp3gInSc4	jeho
rozklad	rozklad	k1gInSc4	rozklad
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
neobsahující	obsahující	k2eNgInSc1d1	neobsahující
led	led	k1gInSc1	led
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
úlomkové	úlomkový	k2eAgFnPc1d1	úlomkový
laviny	lavina	k1gFnPc1	lavina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
sesouvají	sesouvat	k5eAaImIp3nP	sesouvat
po	po	k7c6	po
svazích	svah	k1gInPc6	svah
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
laviny	lavina	k1gFnPc1	lavina
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
poblíž	poblíž	k6eAd1	poblíž
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
kráterů	kráter	k1gInPc2	kráter
jsou	být	k5eAaImIp3nP	být
příležitostně	příležitostně	k6eAd1	příležitostně
přerušeny	přerušit	k5eAaPmNgInP	přerušit
malými	malý	k2eAgFnPc7d1	malá
stružkami	stružka	k1gFnPc7	stružka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
nazývanými	nazývaný	k2eAgFnPc7d1	nazývaná
gullies	gulliesa	k1gFnPc2	gulliesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hypotéze	hypotéza	k1gFnSc6	hypotéza
sublimace	sublimace	k1gFnSc1	sublimace
ledu	led	k1gInSc2	led
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nízko	nízko	k6eAd1	nízko
ležící	ležící	k2eAgInSc4d1	ležící
tmavý	tmavý	k2eAgInSc4d1	tmavý
materiál	materiál	k1gInSc4	materiál
interpretován	interpretován	k2eAgInSc1d1	interpretován
jako	jako	k8xC	jako
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
částicemi	částice	k1gFnPc7	částice
pocházejícím	pocházející	k2eAgFnPc3d1	pocházející
z	z	k7c2	z
okraje	okraj	k1gInSc2	okraj
kráterů	kráter	k1gInPc2	kráter
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgNnSc1d1	relativní
stáří	stáří	k1gNnSc1	stáří
různých	různý	k2eAgFnPc2d1	různá
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Callisto	Callista	k1gMnSc5	Callista
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
určit	určit	k5eAaPmF	určit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
četnosti	četnost	k1gFnSc2	četnost
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
povrch	povrch	k6eAd1wR	povrch
starší	starší	k1gMnPc4	starší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgNnSc1d1	absolutní
datování	datování	k1gNnSc1	datování
povrchu	povrch	k1gInSc2	povrch
zatím	zatím	k6eAd1	zatím
neproběhlo	proběhnout	k5eNaPmAgNnS	proběhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
teoretických	teoretický	k2eAgFnPc2d1	teoretická
úvah	úvaha	k1gFnPc2	úvaha
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátery	kráter	k1gInPc1	kráter
poseté	posetý	k2eAgFnSc2d1	posetá
planiny	planina	k1gFnSc2	planina
jsou	být	k5eAaImIp3nP	být
okolo	okolo	k7c2	okolo
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
staré	starý	k2eAgInPc1d1	starý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
téměř	téměř	k6eAd1	téměř
době	doba	k1gFnSc3	doba
vzniku	vznik	k1gInSc2	vznik
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
multi-prstencových	multirstencův	k2eAgFnPc2d1	multi-prstencův
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
zvolené	zvolený	k2eAgFnSc3d1	zvolená
rychlosti	rychlost	k1gFnSc3	rychlost
vzniku	vznik	k1gInSc2	vznik
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
různí	různý	k2eAgMnPc1d1	různý
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
v	v	k7c6	v
datování	datování	k1gNnSc6	datování
mezi	mezi	k7c7	mezi
1	[number]	k4	1
až	až	k9	až
4	[number]	k4	4
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Callisto	Callista	k1gMnSc5	Callista
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
slabou	slabý	k2eAgFnSc4d1	slabá
atmosféru	atmosféra	k1gFnSc4	atmosféra
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
detekována	detekovat	k5eAaImNgFnS	detekovat
zařízením	zařízení	k1gNnSc7	zařízení
Near	Near	k1gInSc1	Near
Infrared	Infrared	k1gInSc1	Infrared
Mapping	Mapping	k1gInSc1	Mapping
Spectrometer	Spectrometer	k1gInSc1	Spectrometer
(	(	kIx(	(
<g/>
NIMS	NIMS	kA	NIMS
<g/>
)	)	kIx)	)
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
z	z	k7c2	z
absorpcí	absorpce	k1gFnPc2	absorpce
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
4,2	[number]	k4	4,2
mikrometru	mikrometr	k1gInSc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Povrchový	povrchový	k2eAgInSc1d1	povrchový
tlak	tlak	k1gInSc1	tlak
atmosféry	atmosféra	k1gFnSc2	atmosféra
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
na	na	k7c6	na
7,5	[number]	k4	7,5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
12	[number]	k4	12
baru	bar	k1gInSc2	bar
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
108	[number]	k4	108
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
takto	takto	k6eAd1	takto
slabá	slabý	k2eAgFnSc1d1	slabá
atmosféra	atmosféra	k1gFnSc1	atmosféra
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ztratila	ztratit	k5eAaPmAgFnS	ztratit
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
konstantně	konstantně	k6eAd1	konstantně
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
sublimací	sublimace	k1gFnSc7	sublimace
suchého	suchý	k2eAgInSc2d1	suchý
ledu	led	k1gInSc2	led
z	z	k7c2	z
měsíční	měsíční	k2eAgFnSc2d1	měsíční
ledové	ledový	k2eAgFnSc2d1	ledová
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
hypotézou	hypotéza	k1gFnSc7	hypotéza
sublimační	sublimační	k2eAgFnSc2d1	sublimační
degradace	degradace	k1gFnSc2	degradace
povrchu	povrch	k1gInSc2	povrch
vysvětlující	vysvětlující	k2eAgInSc4d1	vysvětlující
vznik	vznik	k1gInSc4	vznik
povrchových	povrchový	k2eAgInPc2d1	povrchový
pahorků	pahorek	k1gInPc2	pahorek
<g/>
.	.	kIx.	.
</s>
<s>
Ionosféra	ionosféra	k1gFnSc1	ionosféra
Callisto	Callista	k1gMnSc5	Callista
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
detekována	detekovat	k5eAaImNgFnS	detekovat
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
hustota	hustota	k1gFnSc1	hustota
elektronů	elektron	k1gInPc2	elektron
dosahující	dosahující	k2eAgFnSc4d1	dosahující
7	[number]	k4	7
až	až	k8xS	až
17	[number]	k4	17
<g/>
×	×	k?	×
<g/>
104	[number]	k4	104
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vysvětlena	vysvětlit	k5eAaPmNgFnS	vysvětlit
pouhou	pouhý	k2eAgFnSc7d1	pouhá
fotoionizací	fotoionizace	k1gFnSc7	fotoionizace
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
existuje	existovat	k5eAaImIp3nS	existovat
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Callisto	Callista	k1gMnSc5	Callista
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dominuje	dominovat	k5eAaImIp3nS	dominovat
molekulární	molekulární	k2eAgInSc4d1	molekulární
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
až	až	k9	až
100	[number]	k4	100
krát	krát	k6eAd1	krát
četnější	četný	k2eAgFnSc1d2	četnější
než	než	k8xS	než
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
kyslík	kyslík	k1gInSc1	kyslík
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Callisto	Callista	k1gMnSc5	Callista
detekován	detekovat	k5eAaImNgInS	detekovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
HST	HST	kA	HST
<g/>
)	)	kIx)	)
určily	určit	k5eAaPmAgInP	určit
horní	horní	k2eAgInSc4d1	horní
limit	limit	k1gInSc4	limit
jeho	jeho	k3xOp3gFnSc2	jeho
možné	možný	k2eAgFnSc2d1	možná
koncentrace	koncentrace	k1gFnSc2	koncentrace
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
čas	čas	k1gInSc1	čas
byl	být	k5eAaImAgInS	být
HST	HST	kA	HST
schopen	schopen	k2eAgInSc1d1	schopen
detekovat	detekovat	k5eAaImF	detekovat
kondenzovaný	kondenzovaný	k2eAgInSc4d1	kondenzovaný
kyslík	kyslík	k1gInSc4	kyslík
zachycený	zachycený	k2eAgInSc4d1	zachycený
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Částečná	částečný	k2eAgFnSc1d1	částečná
diferenciace	diferenciace	k1gFnSc1	diferenciace
Callisto	Callista	k1gMnSc5	Callista
(	(	kIx(	(
<g/>
odvozena	odvozen	k2eAgNnPc1d1	odvozeno
například	například	k6eAd1	například
z	z	k7c2	z
měření	měření	k1gNnSc2	měření
momentu	moment	k1gInSc2	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
uvnitř	uvnitř	k6eAd1	uvnitř
nezahřál	zahřát	k5eNaPmAgMnS	zahřát
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
roztavení	roztavení	k1gNnSc3	roztavení
jeho	jeho	k3xOp3gFnSc2	jeho
ledové	ledový	k2eAgFnSc2d1	ledová
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jako	jako	k9	jako
nejvíce	nejvíce	k6eAd1	nejvíce
pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
model	model	k1gInSc1	model
jeví	jevit	k5eAaImIp3nS	jevit
vznik	vznik	k1gInSc4	vznik
měsíce	měsíc	k1gInSc2	měsíc
pomocí	pomocí	k7c2	pomocí
pomalé	pomalý	k2eAgFnSc2d1	pomalá
akrece	akrece	k1gFnSc2	akrece
v	v	k7c6	v
nízkohustotní	nízkohustotní	k2eAgFnSc6d1	nízkohustotní
mlhovině	mlhovina	k1gFnSc6	mlhovina
tvořené	tvořený	k2eAgFnSc2d1	tvořená
plynem	plyn	k1gInSc7	plyn
a	a	k8xC	a
prachem	prach	k1gInSc7	prach
obíhající	obíhající	k2eAgFnSc2d1	obíhající
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
zformování	zformování	k1gNnSc4	zformování
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
pomalý	pomalý	k2eAgInSc1d1	pomalý
stupeň	stupeň	k1gInSc1	stupeň
akrece	akrece	k1gFnSc2	akrece
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
umožnit	umožnit	k5eAaPmF	umožnit
držet	držet	k5eAaImF	držet
krok	krok	k1gInSc4	krok
ochlazování	ochlazování	k1gNnSc2	ochlazování
měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
akumulací	akumulace	k1gFnSc7	akumulace
tepla	teplo	k1gNnSc2	teplo
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
impakty	impakt	k1gInPc1	impakt
<g/>
,	,	kIx,	,
rozpadem	rozpad	k1gInSc7	rozpad
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
kontrakcí	kontrakce	k1gFnPc2	kontrakce
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zabránit	zabránit	k5eAaPmF	zabránit
roztavení	roztavení	k1gNnSc1	roztavení
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
rychlé	rychlý	k2eAgFnSc3d1	rychlá
diferenciaci	diferenciace	k1gFnSc3	diferenciace
<g/>
.	.	kIx.	.
</s>
<s>
Možný	možný	k2eAgInSc1d1	možný
čas	čas	k1gInSc1	čas
potřebný	potřebný	k2eAgInSc1d1	potřebný
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
Callisto	Callista	k1gMnSc5	Callista
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
evoluce	evoluce	k1gFnSc1	evoluce
Callisto	Callista	k1gMnSc5	Callista
po	po	k7c6	po
akreci	akrece	k1gFnSc6	akrece
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c4	na
bilanci	bilance	k1gFnSc4	bilance
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
rozpadů	rozpad	k1gInPc2	rozpad
<g/>
,	,	kIx,	,
ochlazování	ochlazování	k1gNnSc2	ochlazování
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
kondukcí	kondukce	k1gFnSc7	kondukce
poblíž	poblíž	k7c2	poblíž
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
subsolidovou	subsolidový	k2eAgFnSc7d1	subsolidový
konvekcí	konvekce	k1gFnSc7	konvekce
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
subsolidové	subsolidový	k2eAgFnSc2d1	subsolidový
konvekce	konvekce	k1gFnSc2	konvekce
v	v	k7c6	v
ledu	led	k1gInSc6	led
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
největších	veliký	k2eAgFnPc2d3	veliký
nejistot	nejistota	k1gFnPc2	nejistota
v	v	k7c6	v
modelech	model	k1gInPc6	model
všech	všecek	k3xTgMnPc2	všecek
ledových	ledový	k2eAgMnPc2d1	ledový
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k7c2	blízko
bodu	bod	k1gInSc2	bod
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
teplotní	teplotní	k2eAgFnSc3d1	teplotní
závislosti	závislost	k1gFnSc3	závislost
viskozity	viskozita	k1gFnSc2	viskozita
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Subsolidová	Subsolidový	k2eAgFnSc1d1	Subsolidový
konvekce	konvekce	k1gFnSc1	konvekce
v	v	k7c6	v
ledových	ledový	k2eAgNnPc6d1	ledové
tělesech	těleso	k1gNnPc6	těleso
je	být	k5eAaImIp3nS	být
pomalý	pomalý	k2eAgInSc4d1	pomalý
proces	proces	k1gInSc4	proces
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
ledu	led	k1gInSc2	led
okolo	okolo	k7c2	okolo
1	[number]	k4	1
cm	cm	kA	cm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
i	i	k9	i
tak	tak	k6eAd1	tak
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velice	velice	k6eAd1	velice
efektivní	efektivní	k2eAgInSc4d1	efektivní
chladící	chladící	k2eAgInSc4d1	chladící
mechanismus	mechanismus	k1gInSc4	mechanismus
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pevná	pevný	k2eAgFnSc1d1	pevná
chladná	chladný	k2eAgFnSc1d1	chladná
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
měsíce	měsíc	k1gInSc2	měsíc
vede	vést	k5eAaImIp3nS	vést
teplo	teplo	k1gNnSc4	teplo
kondukcí	kondukce	k1gFnPc2	kondukce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
led	led	k1gInSc1	led
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
subsolidovém	subsolidový	k2eAgInSc6d1	subsolidový
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
teplo	teplo	k1gNnSc4	teplo
konvekcí	konvekce	k1gFnPc2	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
konduktivní	konduktivní	k2eAgFnSc1d1	konduktivní
vrstva	vrstva	k1gFnSc1	vrstva
u	u	k7c2	u
Callisto	Callista	k1gMnSc5	Callista
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
chladné	chladný	k2eAgFnSc3d1	chladná
a	a	k8xC	a
pevné	pevný	k2eAgFnSc3d1	pevná
litosféře	litosféra	k1gFnSc3	litosféra
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
přítomnost	přítomnost	k1gFnSc1	přítomnost
by	by	kYmCp3nS	by
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
známek	známka	k1gFnPc2	známka
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
aktivity	aktivita	k1gFnSc2	aktivita
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Konvekce	konvekce	k1gFnSc1	konvekce
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
částech	část	k1gFnPc6	část
měsíce	měsíc	k1gInSc2	měsíc
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
vrstvách	vrstva	k1gFnPc6	vrstva
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vlivem	vliv	k1gInSc7	vliv
vysokých	vysoký	k2eAgInPc2d1	vysoký
tlaků	tlak	k1gInPc2	tlak
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
krystalických	krystalický	k2eAgFnPc6d1	krystalická
fázích	fáze	k1gFnPc6	fáze
od	od	k7c2	od
tzv.	tzv.	kA	tzv.
ledu	led	k1gInSc2	led
I	i	k9	i
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
až	až	k9	až
po	po	k7c4	po
led	led	k1gInSc4	led
VII	VII	kA	VII
hluboko	hluboko	k6eAd1	hluboko
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Subsolidová	Subsolidový	k2eAgFnSc1d1	Subsolidový
konvekce	konvekce	k1gFnSc1	konvekce
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
Callisto	Callista	k1gMnSc5	Callista
mohla	moct	k5eAaImAgFnS	moct
bránit	bránit	k5eAaImF	bránit
tání	tání	k1gNnSc4	tání
ledu	led	k1gInSc2	led
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohla	moct	k5eNaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
žádná	žádný	k3yNgFnSc1	žádný
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
diferenciace	diferenciace	k1gFnSc1	diferenciace
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
velkého	velký	k2eAgNnSc2d1	velké
kamenného	kamenný	k2eAgNnSc2d1	kamenné
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
ledové	ledový	k2eAgFnSc2d1	ledová
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
konvekčních	konvekční	k2eAgInPc2d1	konvekční
procesů	proces	k1gInPc2	proces
zde	zde	k6eAd1	zde
však	však	k9	však
probíhalo	probíhat	k5eAaImAgNnS	probíhat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
pomalé	pomalý	k2eAgNnSc1d1	pomalé
a	a	k8xC	a
částečné	částečný	k2eAgNnSc1d1	částečné
oddělování	oddělování	k1gNnSc1	oddělování
kamenných	kamenný	k2eAgInPc2d1	kamenný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
časovém	časový	k2eAgNnSc6d1	časové
měřítku	měřítko	k1gNnSc6	měřítko
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zatím	zatím	k6eAd1	zatím
o	o	k7c6	o
Callisto	Callista	k1gMnSc5	Callista
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
existenci	existence	k1gFnSc4	existence
vrstvy	vrstva	k1gFnSc2	vrstva
či	či	k8xC	či
"	"	kIx"	"
<g/>
oceánu	oceán	k1gInSc6	oceán
<g/>
"	"	kIx"	"
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
anomálním	anomální	k2eAgNnSc7d1	anomální
chováním	chování	k1gNnSc7	chování
ledu	led	k1gInSc2	led
krystalické	krystalický	k2eAgFnSc2d1	krystalická
fáze	fáze	k1gFnSc2	fáze
I	I	kA	I
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
na	na	k7c4	na
251	[number]	k4	251
Kelvinů	kelvin	k1gInPc2	kelvin
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
2070	[number]	k4	2070
barů	bar	k1gInPc2	bar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
realistických	realistický	k2eAgInPc6d1	realistický
modelech	model	k1gInPc6	model
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
stavby	stavba	k1gFnSc2	stavba
Callisto	Callista	k1gMnSc5	Callista
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
mezi	mezi	k7c7	mezi
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
km	km	kA	km
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
této	tento	k3xDgFnSc2	tento
anomální	anomální	k2eAgFnSc2d1	anomální
teploty	teplota	k1gFnSc2	teplota
tání	tání	k1gNnSc2	tání
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
lehce	lehko	k6eAd1	lehko
překračuje	překračovat	k5eAaImIp3nS	překračovat
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
i	i	k9	i
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
čpavku	čpavek	k1gInSc2	čpavek
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
hmotnostních	hmotnostní	k2eAgFnPc2d1	hmotnostní
%	%	kIx~	%
<g/>
)	)	kIx)	)
téměř	téměř	k6eAd1	téměř
garantuje	garantovat	k5eAaBmIp3nS	garantovat
existenci	existence	k1gFnSc4	existence
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
čpavek	čpavek	k1gInSc1	čpavek
dále	daleko	k6eAd2	daleko
snižuje	snižovat	k5eAaImIp3nS	snižovat
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
objemově	objemově	k6eAd1	objemově
je	být	k5eAaImIp3nS	být
Callisto	Callista	k1gMnSc5	Callista
velice	velice	k6eAd1	velice
podobný	podobný	k2eAgInSc4d1	podobný
Ganymedu	Ganymed	k1gMnSc6	Ganymed
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
geologická	geologický	k2eAgFnSc1d1	geologická
historie	historie	k1gFnSc1	historie
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Callisto	Callista	k1gMnSc5	Callista
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
byl	být	k5eAaImAgInS	být
formován	formovat	k5eAaImNgInS	formovat
impakty	impakt	k1gInPc7	impakt
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
exogenními	exogenní	k2eAgInPc7d1	exogenní
pochody	pochod	k1gInPc7	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sousedního	sousední	k2eAgNnSc2d1	sousední
Ganymedu	Ganymed	k1gMnSc6	Ganymed
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
povrch	povrch	k1gInSc4	povrch
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
rýhami	rýha	k1gFnPc7	rýha
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
málo	málo	k4c1	málo
náznaků	náznak	k1gInPc2	náznak
o	o	k7c6	o
tektonických	tektonický	k2eAgInPc6d1	tektonický
procesech	proces	k1gInPc6	proces
na	na	k7c6	na
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
geologická	geologický	k2eAgFnSc1d1	geologická
historie	historie	k1gFnSc1	historie
Callisto	Callista	k1gMnSc5	Callista
tak	tak	k6eAd1	tak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
planetologům	planetolog	k1gMnPc3	planetolog
využívat	využívat	k5eAaImF	využívat
měsíc	měsíc	k1gInSc4	měsíc
jako	jako	k8xS	jako
referenční	referenční	k2eAgNnSc4d1	referenční
těleso	těleso	k1gNnSc4	těleso
pro	pro	k7c4	pro
srovnávací	srovnávací	k2eAgFnPc4d1	srovnávací
studie	studie	k1gFnPc4	studie
s	s	k7c7	s
více	hodně	k6eAd2	hodně
aktivními	aktivní	k2eAgInPc7d1	aktivní
a	a	k8xC	a
komplexními	komplexní	k2eAgInPc7d1	komplexní
světy	svět	k1gInPc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
,	,	kIx,	,
i	i	k9	i
na	na	k7c4	na
Callisto	Callista	k1gMnSc5	Callista
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
potenciálně	potenciálně	k6eAd1	potenciálně
mohl	moct	k5eAaImAgInS	moct
nacházet	nacházet	k5eAaImF	nacházet
mimozemský	mimozemský	k2eAgInSc1d1	mimozemský
mikrobiální	mikrobiální	k2eAgInSc1d1	mikrobiální
život	život	k1gInSc1	život
ve	v	k7c6	v
slaném	slaný	k2eAgInSc6d1	slaný
oceánu	oceán	k1gInSc6	oceán
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
případné	případný	k2eAgFnPc4d1	případná
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
na	na	k7c4	na
Callisto	Callista	k1gMnSc5	Callista
jsou	být	k5eAaImIp3nP	být
nehostinnější	hostinný	k2eNgInSc1d2	hostinný
než	než	k8xS	než
u	u	k7c2	u
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
důvody	důvod	k1gInPc7	důvod
jsou	být	k5eAaImIp3nP	být
nedostatek	nedostatek	k1gInSc4	nedostatek
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
materiálem	materiál	k1gInSc7	materiál
a	a	k8xC	a
nižší	nízký	k2eAgInSc4d2	nižší
tepelný	tepelný	k2eAgInSc4d1	tepelný
tok	tok	k1gInSc4	tok
z	z	k7c2	z
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
oblastí	oblast	k1gFnPc2	oblast
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Torrence	Torrence	k1gFnSc1	Torrence
Johnson	Johnson	k1gInSc1	Johnson
k	k	k7c3	k
možnosti	možnost	k1gFnSc3	možnost
života	život	k1gInSc2	život
na	na	k7c4	na
Callisto	Callista	k1gMnSc5	Callista
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
Galileovo	Galileův	k2eAgNnSc1d1	Galileovo
měsíci	měsíc	k1gInSc6	měsíc
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
názoru	názor	k1gInSc2	názor
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
pozorování	pozorování	k1gNnPc2	pozorování
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgFnSc1d3	veliký
možnost	možnost	k1gFnSc1	možnost
výskytu	výskyt	k1gInSc2	výskyt
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Průlety	průlet	k1gInPc1	průlet
amerických	americký	k2eAgFnPc2d1	americká
sond	sonda	k1gFnPc2	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
a	a	k8xC	a
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přinesly	přinést	k5eAaPmAgFnP	přinést
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
nových	nový	k2eAgFnPc2d1	nová
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgFnP	být
známé	známá	k1gFnPc1	známá
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
pozemskými	pozemský	k2eAgInPc7d1	pozemský
teleskopy	teleskop	k1gInPc7	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Průlom	průlom	k1gInSc1	průlom
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
generací	generace	k1gFnSc7	generace
amerických	americký	k2eAgFnPc2d1	americká
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Joviánskou	Joviánský	k2eAgFnSc7d1	Joviánský
soustavou	soustava	k1gFnSc7	soustava
prolétly	prolétnout	k5eAaPmAgFnP	prolétnout
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1979	[number]	k4	1979
až	až	k8xS	až
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Sondy	sonda	k1gFnPc1	sonda
pořídily	pořídit	k5eAaPmAgFnP	pořídit
snímky	snímek	k1gInPc4	snímek
téměř	téměř	k6eAd1	téměř
poloviny	polovina	k1gFnSc2	polovina
povrchu	povrch	k1gInSc2	povrch
Callisto	Callista	k1gMnSc5	Callista
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
mezi	mezi	k7c4	mezi
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
kilometry	kilometr	k1gInPc4	kilometr
na	na	k7c4	na
pixel	pixel	k1gInSc4	pixel
<g/>
,	,	kIx,	,
určily	určit	k5eAaPmAgInP	určit
přesně	přesně	k6eAd1	přesně
teplotu	teplota	k1gFnSc4	teplota
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
výzkumu	výzkum	k1gInSc2	výzkum
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1994	[number]	k4	1994
až	až	k9	až
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
když	když	k8xS	když
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
osmkrát	osmkrát	k6eAd1	osmkrát
těsně	těsně	k6eAd1	těsně
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
další	další	k2eAgFnSc1d1	další
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
průlet	průlet	k1gInSc1	průlet
C30	C30	k1gFnSc2	C30
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
jen	jen	k9	jen
138	[number]	k4	138
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
dokončila	dokončit	k5eAaPmAgFnS	dokončit
snímkování	snímkování	k1gNnSc4	snímkování
povrchu	povrch	k1gInSc2	povrch
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
snímků	snímek	k1gInPc2	snímek
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
u	u	k7c2	u
vybraných	vybraný	k2eAgFnPc2d1	vybraná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pořídila	pořídit	k5eAaPmAgFnS	pořídit
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
snímky	snímek	k1gInPc4	snímek
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
všech	všecek	k3xTgInPc2	všecek
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
včetně	včetně	k7c2	včetně
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
únorem	únor	k1gInSc7	únor
až	až	k8xS	až
březnem	březen	k1gInSc7	březen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
pořídila	pořídit	k5eAaPmAgFnS	pořídit
nové	nový	k2eAgInPc4d1	nový
snímky	snímek	k1gInPc4	snímek
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
a	a	k8xC	a
provedla	provést	k5eAaPmAgFnS	provést
spektrální	spektrální	k2eAgNnSc4d1	spektrální
měření	měření	k1gNnSc4	měření
sonda	sonda	k1gFnSc1	sonda
New	New	k1gMnSc1	New
Horizons	Horizons	k1gInSc1	Horizons
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2020	[number]	k4	2020
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
start	start	k1gInSc1	start
společného	společný	k2eAgInSc2d1	společný
projektu	projekt	k1gInSc2	projekt
americké	americký	k2eAgFnSc2d1	americká
NASA	NASA	kA	NASA
a	a	k8xC	a
evropské	evropský	k2eAgInPc4d1	evropský
ESA	eso	k1gNnPc1	eso
s	s	k7c7	s
názvem	název	k1gInSc7	název
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
EJSM	EJSM	kA	EJSM
<g/>
)	)	kIx)	)
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
výzkumu	výzkum	k1gInSc2	výzkum
Jupiterových	Jupiterův	k2eAgMnPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc1	přednost
před	před	k7c7	před
misí	mise	k1gFnSc7	mise
Titan	titan	k1gInSc4	titan
Saturn	Saturn	k1gInSc1	Saturn
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
EJSM	EJSM	kA	EJSM
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
Jupiter	Jupiter	k1gMnSc1	Jupiter
Europa	Europ	k1gMnSc2	Europ
Orbiter	Orbiter	k1gMnSc1	Orbiter
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
NASA	NASA	kA	NASA
a	a	k8xC	a
Jupiter	Jupiter	k1gMnSc1	Jupiter
Ganymed	Ganymed	k1gMnSc1	Ganymed
Orbiter	Orbiter	k1gMnSc1	Orbiter
vedenou	vedený	k2eAgFnSc4d1	vedená
ESA	eso	k1gNnPc5	eso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
provedla	provést	k5eAaPmAgFnS	provést
americká	americký	k2eAgFnSc1d1	americká
NASA	NASA	kA	NASA
studii	studie	k1gFnSc4	studie
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Human	Human	k1gMnSc1	Human
Outer	Outer	k1gMnSc1	Outer
Planets	Planetsa	k1gFnPc2	Planetsa
Exploration	Exploration	k1gInSc1	Exploration
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
HOPE	HOPE	kA	HOPE
<g/>
)	)	kIx)	)
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
budoucího	budoucí	k2eAgInSc2d1	budoucí
pilotovaného	pilotovaný	k2eAgInSc2d1	pilotovaný
průzkumu	průzkum	k1gInSc2	průzkum
vnějších	vnější	k2eAgFnPc2d1	vnější
oblastí	oblast	k1gFnPc2	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
detailního	detailní	k2eAgInSc2d1	detailní
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
měsíc	měsíc	k1gInSc4	měsíc
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
studie	studie	k1gFnSc2	studie
se	se	k3xPyFc4	se
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
využití	využití	k1gNnSc1	využití
Callisto	Callista	k1gMnSc5	Callista
jako	jako	k8xS	jako
potenciálního	potenciální	k2eAgNnSc2d1	potenciální
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
postavit	postavit	k5eAaPmF	postavit
povrchová	povrchový	k2eAgFnSc1d1	povrchová
základna	základna	k1gFnSc1	základna
využívaná	využívaný	k2eAgFnSc1d1	využívaná
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
paliva	palivo	k1gNnSc2	palivo
potřebného	potřebný	k2eAgNnSc2d1	potřebné
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
vnějších	vnější	k2eAgFnPc2d1	vnější
oblastí	oblast	k1gFnPc2	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
Callisto	Callista	k1gMnSc5	Callista
jsou	být	k5eAaImIp3nP	být
nižší	nízký	k2eAgFnSc1d2	nižší
radiace	radiace	k1gFnSc1	radiace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
měsíc	měsíc	k1gInSc1	měsíc
nachází	nacházet	k5eAaImIp3nS	nacházet
nejdále	daleko	k6eAd3	daleko
z	z	k7c2	z
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
geologická	geologický	k2eAgFnSc1d1	geologická
stabilita	stabilita	k1gFnSc1	stabilita
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Stálá	stálý	k2eAgFnSc1d1	stálá
základna	základna	k1gFnSc1	základna
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
využita	využít	k5eAaPmNgFnS	využít
během	během	k7c2	během
průzkumu	průzkum	k1gInSc2	průzkum
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
či	či	k8xC	či
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ideálně	ideálně	k6eAd1	ideálně
umístěna	umístit	k5eAaPmNgFnS	umístit
pro	pro	k7c4	pro
servis	servis	k1gInSc4	servis
lodí	loď	k1gFnPc2	loď
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
vnějších	vnější	k2eAgInPc2d1	vnější
okrajů	okraj	k1gInPc2	okraj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
prolétaly	prolétat	k5eAaPmAgFnP	prolétat
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
efektu	efekt	k1gInSc2	efekt
gravitačního	gravitační	k2eAgInSc2d1	gravitační
praku	prak	k1gInSc2	prak
po	po	k7c6	po
zastávce	zastávka	k1gFnSc6	zastávka
na	na	k7c4	na
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
NASA	NASA	kA	NASA
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
pilotovanou	pilotovaný	k2eAgFnSc4d1	pilotovaná
misi	mise	k1gFnSc4	mise
ke	k	k7c3	k
Callisto	Callista	k1gMnSc5	Callista
mohl	moct	k5eAaImAgInS	moct
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
