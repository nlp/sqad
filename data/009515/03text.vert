<p>
<s>
Fudži	Fudzat	k5eAaPmIp1nS	Fudzat
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
富	富	k?	富
<g/>
;	;	kIx,	;
Fudži-san	Fudžian	k1gMnSc1	Fudži-san
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
regionu	region	k1gInSc6	region
Čúbu	Čúbus	k1gInSc2	Čúbus
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
prefekturami	prefektura	k1gFnPc7	prefektura
Šizuoka	Šizuoko	k1gNnSc2	Šizuoko
a	a	k8xC	a
Jamanaši	Jamanaše	k1gFnSc4	Jamanaše
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
jasného	jasný	k2eAgNnSc2d1	jasné
počasí	počasí	k1gNnSc2	počasí
viditelná	viditelný	k2eAgFnSc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
stojí	stát	k5eAaImIp3nS	stát
osamoceně	osamoceně	k6eAd1	osamoceně
uprostřed	uprostřed	k7c2	uprostřed
nížin	nížina	k1gFnPc2	nížina
při	při	k7c6	při
tichomořském	tichomořský	k2eAgNnSc6d1	Tichomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Honšú	Honšú	k1gFnSc2	Honšú
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sopka	sopka	k1gFnSc1	sopka
==	==	k?	==
</s>
</p>
<p>
<s>
Sopka	sopka	k1gFnSc1	sopka
vznikala	vznikat	k5eAaImAgFnS	vznikat
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
vyvrhováním	vyvrhování	k1gNnSc7	vyvrhování
lávy	láva	k1gFnSc2	láva
a	a	k8xC	a
popela	popel	k1gInSc2	popel
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
stratovulkán	stratovulkán	k1gInSc4	stratovulkán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Fudži	Fudž	k1gFnSc3	Fudž
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
aktivní	aktivní	k2eAgFnSc4d1	aktivní
sopku	sopka	k1gFnSc4	sopka
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
záznamem	záznam	k1gInSc7	záznam
o	o	k7c6	o
erupci	erupce	k1gFnSc6	erupce
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
800	[number]	k4	800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jich	on	k3xPp3gMnPc2	on
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
zaznamenaným	zaznamenaný	k2eAgInSc7d1	zaznamenaný
výbuchem	výbuch	k1gInSc7	výbuch
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
erupce	erupce	k1gFnSc1	erupce
Hóei	Hóe	k1gFnSc2	Hóe
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc1	město
Edo	Eda	k1gMnSc5	Eda
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Tokio	Tokio	k1gNnSc4	Tokio
<g/>
)	)	kIx)	)
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
vrstva	vrstva	k1gFnSc1	vrstva
popela	popel	k1gInSc2	popel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kráter	kráter	k1gInSc1	kráter
(	(	kIx(	(
<g/>
Nai-in	Nain	k1gInSc1	Nai-in
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
šířku	šířka	k1gFnSc4	šířka
kolem	kolem	k7c2	kolem
700	[number]	k4	700
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Okraj	okraj	k1gInSc1	okraj
sopečného	sopečný	k2eAgInSc2d1	sopečný
kráteru	kráter	k1gInSc2	kráter
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
kruhový	kruhový	k2eAgInSc4d1	kruhový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
šest	šest	k4xCc4	šest
výstupků	výstupek	k1gInPc2	výstupek
zvaných	zvaný	k2eAgInPc2d1	zvaný
"	"	kIx"	"
<g/>
šest	šest	k4xCc4	šest
okvětních	okvětní	k2eAgInPc2d1	okvětní
plátků	plátek	k1gInPc2	plátek
Fudži	Fudž	k1gFnSc3	Fudž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výstupky	výstupek	k1gInPc1	výstupek
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
dojem	dojem	k1gInSc4	dojem
hrbolatého	hrbolatý	k2eAgInSc2d1	hrbolatý
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc4	její
elegantní	elegantní	k2eAgInSc4d1	elegantní
kuželovitý	kuželovitý	k2eAgInSc4d1	kuželovitý
vrchol	vrchol	k1gInSc4	vrchol
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
hory	hora	k1gFnSc2	hora
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
je	být	k5eAaImIp3nS	být
125	[number]	k4	125
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
hory	hora	k1gFnPc4	hora
Fudži	Fudž	k1gFnSc3	Fudž
leží	ležet	k5eAaImIp3nS	ležet
pět	pět	k4xCc1	pět
jezer	jezero	k1gNnPc2	jezero
<g/>
:	:	kIx,	:
Kawaguči	Kawaguč	k1gFnSc6	Kawaguč
<g/>
,	,	kIx,	,
Jamanaka	Jamanak	k1gMnSc4	Jamanak
<g/>
,	,	kIx,	,
Sai	Sai	k1gMnSc4	Sai
<g/>
,	,	kIx,	,
Motosu	Motosa	k1gFnSc4	Motosa
a	a	k8xC	a
Šódži	Šódž	k1gFnSc6	Šódž
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
uprostřed	uprostřed	k7c2	uprostřed
lesů	les	k1gInPc2	les
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
propojená	propojený	k2eAgFnSc1d1	propojená
potůčky	potůček	k1gInPc7	potůček
a	a	k8xC	a
vodopády	vodopád	k1gInPc7	vodopád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turismus	turismus	k1gInSc4	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
byl	být	k5eAaImAgInS	být
přístup	přístup	k1gInSc1	přístup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
povolen	povolit	k5eAaPmNgInS	povolit
jen	jen	k9	jen
mužům	muž	k1gMnPc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnějším	vhodný	k2eAgNnSc7d3	nejvhodnější
obdobím	období	k1gNnSc7	období
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Fudži	Fudž	k1gFnSc6	Fudž
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc4	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
turistické	turistický	k2eAgFnPc1d1	turistická
chaty	chata	k1gFnPc1	chata
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyjít	vyjít	k5eAaPmF	vyjít
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
šesti	šest	k4xCc2	šest
upravených	upravený	k2eAgFnPc2d1	upravená
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
hory	hora	k1gFnSc2	hora
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
;	;	kIx,	;
její	její	k3xOp3gInSc4	její
provoz	provoz	k1gInSc4	provoz
je	být	k5eAaImIp3nS	být
však	však	k9	však
limitován	limitovat	k5eAaBmNgInS	limitovat
aktivitou	aktivita	k1gFnSc7	aktivita
sopky	sopka	k1gFnPc1	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
lanovka	lanovka	k1gFnSc1	lanovka
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
kardiakům	kardiak	k1gMnPc3	kardiak
<g/>
,	,	kIx,	,
epileptikům	epileptik	k1gMnPc3	epileptik
a	a	k8xC	a
alergikům	alergik	k1gMnPc3	alergik
–	–	k?	–
neboť	neboť	k8xC	neboť
její	její	k3xOp3gFnSc1	její
cesta	cesta	k1gFnSc1	cesta
vede	vést	k5eAaImIp3nS	vést
nad	nad	k7c7	nad
lávovými	lávový	k2eAgNnPc7d1	lávové
poli	pole	k1gNnPc7	pole
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
mohou	moct	k5eAaImIp3nP	moct
stoupat	stoupat	k5eAaImF	stoupat
plynové	plynový	k2eAgInPc1d1	plynový
sopečné	sopečný	k2eAgInPc1d1	sopečný
výpary	výpar	k1gInPc1	výpar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
západní	západní	k2eAgFnSc3d1	západní
části	část	k1gFnSc3	část
hory	hora	k1gFnSc2	hora
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
výletní	výletní	k2eAgFnSc7d1	výletní
lodí	loď	k1gFnSc7	loď
po	po	k7c6	po
jezeru	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Fudži	Fudž	k1gFnSc6	Fudž
se	se	k3xPyFc4	se
turisté	turist	k1gMnPc1	turist
vypravují	vypravovat	k5eAaImIp3nP	vypravovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
specializované	specializovaný	k2eAgFnSc2d1	specializovaná
cestovní	cestovní	k2eAgFnSc2d1	cestovní
kanceláře	kancelář	k1gFnSc2	kancelář
se	s	k7c7	s
zajištěným	zajištěný	k2eAgInSc7d1	zajištěný
programem	program	k1gInSc7	program
nebo	nebo	k8xC	nebo
individuálně	individuálně	k6eAd1	individuálně
<g/>
,	,	kIx,	,
z	z	k7c2	z
Tokia	Tokio	k1gNnSc2	Tokio
do	do	k7c2	do
Hakone	Hakon	k1gInSc5	Hakon
a	a	k8xC	a
následně	následně	k6eAd1	následně
místním	místní	k2eAgInSc7d1	místní
vláčkem	vláček	k1gInSc7	vláček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
zastávkách	zastávka	k1gFnPc6	zastávka
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
další	další	k2eAgFnPc1d1	další
atrakce	atrakce	k1gFnPc1	atrakce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vesnici	vesnice	k1gFnSc3	vesnice
se	se	k3xPyFc4	se
zahradou	zahrada	k1gFnSc7	zahrada
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
navštíví	navštívit	k5eAaPmIp3nS	navštívit
horu	hora	k1gFnSc4	hora
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
30	[number]	k4	30
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Fudži	Fudž	k1gFnSc3	Fudž
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nedalekým	daleký	k2eNgNnSc7d1	nedaleké
pobřežím	pobřeží	k1gNnSc7	pobřeží
součástí	součást	k1gFnPc2	součást
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
až	až	k9	až
80	[number]	k4	80
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příroda	příroda	k1gFnSc1	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejnižším	nízký	k2eAgNnSc6d3	nejnižší
patře	patro	k1gNnSc6	patro
mezi	mezi	k7c7	mezi
modřínovými	modřínový	k2eAgInPc7d1	modřínový
a	a	k8xC	a
smrkovými	smrkový	k2eAgInPc7d1	smrkový
lesy	les	k1gInPc7	les
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
také	také	k9	také
třešně	třešeň	k1gFnPc1	třešeň
a	a	k8xC	a
azalky	azalka	k1gFnPc1	azalka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgInPc6d1	horní
svazích	svah	k1gInPc6	svah
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
drží	držet	k5eAaImIp3nS	držet
již	již	k9	již
jen	jen	k9	jen
odolnější	odolný	k2eAgFnPc4d2	odolnější
křoviny	křovina	k1gFnPc4	křovina
a	a	k8xC	a
trávy	tráva	k1gFnPc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
na	na	k7c6	na
zasněženém	zasněžený	k2eAgInSc6d1	zasněžený
vrcholu	vrchol	k1gInSc6	vrchol
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
teploty	teplota	k1gFnPc1	teplota
kolem	kolem	k7c2	kolem
6	[number]	k4	6
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Fudžisan	Fudžisan	k1gInSc1	Fudžisan
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
euroamerické	euroamerický	k2eAgFnSc6d1	euroamerická
kultuře	kultura	k1gFnSc6	kultura
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
Fudžijama	Fudžijamum	k1gNnPc1	Fudžijamum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgNnPc2d1	možné
čtení	čtení	k1gNnPc2	čtení
znaku	znak	k1gInSc2	znak
山	山	k?	山
(	(	kIx(	(
<g/>
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
jama	jama	k6eAd1	jama
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Japonci	Japonec	k1gMnPc1	Japonec
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
zastaralé	zastaralý	k2eAgInPc1d1	zastaralý
nebo	nebo	k8xC	nebo
básnické	básnický	k2eAgInPc1d1	básnický
japonské	japonský	k2eAgInPc1d1	japonský
názvy	název	k1gInPc1	název
hory	hora	k1gFnSc2	hora
Fudži	Fudž	k1gFnSc3	Fudž
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Fudži-no-Jama	Fudžio-Jama	k1gFnSc1	Fudži-no-Jama
(	(	kIx(	(
<g/>
ふ	ふ	k?	ふ
<g/>
,	,	kIx,	,
Fudžijská	Fudžijský	k2eAgFnSc1d1	Fudžijský
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fudži-no-Takane	Fudžio-Takan	k1gMnSc5	Fudži-no-Takan
(	(	kIx(	(
<g/>
ふ	ふ	k?	ふ
<g/>
,	,	kIx,	,
Vysoký	vysoký	k2eAgInSc1d1	vysoký
vrchol	vrchol	k1gInSc1	vrchol
Fudži	Fudž	k1gFnSc3	Fudž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fujó-hó	Fujóó	k1gFnSc1	Fujó-hó
(	(	kIx(	(
<g/>
芙	芙	k?	芙
<g/>
,	,	kIx,	,
Lotosový	lotosový	k2eAgInSc1d1	lotosový
vrchol	vrchol	k1gInSc1	vrchol
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fu-gaku	Fuak	k1gInSc2	Fu-gak
(	(	kIx(	(
<g/>
富	富	k?	富
nebo	nebo	k8xC	nebo
富	富	k?	富
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
znak	znak	k1gInSc4	znak
z	z	k7c2	z
富	富	k?	富
<g/>
,	,	kIx,	,
Fudži	Fudž	k1gFnSc3	Fudž
a	a	k8xC	a
岳	岳	k?	岳
<g/>
,	,	kIx,	,
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
Fudži-	Fudži-	k1gFnSc2	Fudži-
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
autochtonních	autochtonní	k2eAgInPc2d1	autochtonní
Ainů	Ain	k1gInPc2	Ain
s	s	k7c7	s
významem	význam	k1gInSc7	význam
bohyně	bohyně	k1gFnSc2	bohyně
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náboženství	náboženství	k1gNnSc2	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Japonska	Japonsko	k1gNnSc2	Japonsko
–	–	k?	–
národ	národ	k1gInSc4	národ
Ainu	Ainus	k1gInSc2	Ainus
–	–	k?	–
uctívali	uctívat	k5eAaImAgMnP	uctívat
horu	hora	k1gFnSc4	hora
jako	jako	k8xC	jako
posvátnou	posvátný	k2eAgFnSc4d1	posvátná
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
posvátná	posvátný	k2eAgFnSc1d1	posvátná
také	také	k9	také
pro	pro	k7c4	pro
vyznavače	vyznavač	k1gMnPc4	vyznavač
šintóismu	šintóismus	k1gInSc2	šintóismus
a	a	k8xC	a
buddhisty	buddhista	k1gMnSc2	buddhista
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
náboženská	náboženský	k2eAgFnSc1d1	náboženská
sekta	sekta	k1gFnSc1	sekta
Fudži-ko	Fudžio	k1gNnSc4	Fudži-ko
(	(	kIx(	(
<g/>
Společenství	společenství	k1gNnSc2	společenství
Fudži	Fudž	k1gFnSc6	Fudž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zasvěcuje	zasvěcovat	k5eAaImIp3nS	zasvěcovat
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
jenom	jenom	k9	jenom
této	tento	k3xDgFnSc3	tento
hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umění	umění	k1gNnSc1	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Hora	Hora	k1gMnSc1	Hora
Fudži	Fudž	k1gFnSc3	Fudž
je	být	k5eAaImIp3nS	být
světoznámým	světoznámý	k2eAgInSc7d1	světoznámý
symbolem	symbol	k1gInSc7	symbol
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
na	na	k7c6	na
uměleckých	umělecký	k2eAgInPc6d1	umělecký
dílech	díl	k1gInPc6	díl
a	a	k8xC	a
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
zabývajícím	zabývající	k2eAgInSc7d1	zabývající
se	se	k3xPyFc4	se
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
malíře	malíř	k1gMnSc2	malíř
stylu	styl	k1gInSc2	styl
ukijo-e	ukijo	k1gNnSc2	ukijo-e
Kacušiky	Kacušika	k1gFnSc2	Kacušika
Hokusaie	Hokusaie	k1gFnSc2	Hokusaie
36	[number]	k4	36
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
Fudži	Fudž	k1gFnSc3	Fudž
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
buddhističtí	buddhistický	k2eAgMnPc1d1	buddhistický
malíři	malíř	k1gMnPc1	malíř
zobrazovali	zobrazovat	k5eAaImAgMnP	zobrazovat
často	často	k6eAd1	často
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
jezero	jezero	k1gNnSc4	jezero
Kawaguči	Kawaguč	k1gFnSc3	Kawaguč
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
hladině	hladina	k1gFnSc6	hladina
se	se	k3xPyFc4	se
hora	hora	k1gFnSc1	hora
Fudži	Fudž	k1gFnSc3	Fudž
zrcadlí	zrcadlit	k5eAaImIp3nP	zrcadlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgFnSc7d1	tradiční
barvou	barva	k1gFnSc7	barva
používanou	používaný	k2eAgFnSc7d1	používaná
při	při	k7c6	při
zobrazování	zobrazování	k1gNnSc6	zobrazování
hory	hora	k1gFnPc4	hora
Fudži	Fudž	k1gFnSc3	Fudž
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Renomovaný	renomovaný	k2eAgMnSc1d1	renomovaný
japonský	japonský	k2eAgMnSc1d1	japonský
fotograf	fotograf	k1gMnSc1	fotograf
Kójó	Kójó	k1gMnSc1	Kójó
Okada	Okada	k1gMnSc1	Okada
(	(	kIx(	(
<g/>
岡	岡	k?	岡
紅	紅	k?	紅
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
–	–	k?	–
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
horu	hora	k1gFnSc4	hora
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Snímal	snímat	k5eAaImAgMnS	snímat
ji	on	k3xPp3gFnSc4	on
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
náladách	nálada	k1gFnPc6	nálada
<g/>
,	,	kIx,	,
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
úhlu	úhel	k1gInSc2	úhel
i	i	k9	i
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
denní	denní	k2eAgFnSc6d1	denní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Mount	Mount	k1gInSc1	Mount
Taranaki	Taranak	k1gFnSc2	Taranak
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
úhlů	úhel	k1gInPc2	úhel
tak	tak	k9	tak
podobná	podobný	k2eAgFnSc1d1	podobná
hoře	hoře	k6eAd1	hoře
Fudži	Fudž	k1gFnSc3	Fudž
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xS	jako
její	její	k3xOp3gFnSc4	její
zástupce	zástupce	k1gMnSc1	zástupce
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Posledním	poslední	k2eAgMnSc6d1	poslední
samuraji	samuraj	k1gMnSc6	samuraj
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
hoře	hoře	k6eAd1	hoře
Fudži	Fudž	k1gFnSc3	Fudž
je	být	k5eAaImIp3nS	být
také	také	k9	také
Volcán	Volcán	k2eAgMnSc1d1	Volcán
Osorno	Osorno	k1gNnSc4	Osorno
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
sopka	sopka	k1gFnSc1	sopka
St.	st.	kA	st.
Helens	Helens	k1gInSc1	Helens
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc4	Washington
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
symetrii	symetrie	k1gFnSc3	symetrie
a	a	k8xC	a
vrcholku	vrcholek	k1gInSc3	vrcholek
pokrytém	pokrytý	k2eAgInSc6d1	pokrytý
sněhem	sníh	k1gInSc7	sníh
a	a	k8xC	a
ledem	led	k1gInSc7	led
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Fudžisan	Fudžisan	k1gInSc1	Fudžisan
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnPc1	erupce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
změnila	změnit	k5eAaPmAgFnS	změnit
její	její	k3xOp3gFnSc4	její
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Erupce	erupce	k1gFnPc1	erupce
Hóei	Hóe	k1gFnSc2	Hóe
hory	hora	k1gFnSc2	hora
Fudži	Fudž	k1gFnSc3	Fudž
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fudži	Fudž	k1gFnSc6	Fudž
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Webové	webový	k2eAgFnSc2d1	webová
kamery	kamera	k1gFnSc2	kamera
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Fudži	Fudž	k1gFnSc6	Fudž
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Satelitní	satelitní	k2eAgInSc1d1	satelitní
snímek	snímek	k1gInSc1	snímek
na	na	k7c4	na
Google	Google	k1gNnSc4	Google
Maps	Mapsa	k1gFnPc2	Mapsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hora	hora	k1gFnSc1	hora
Fudži	Fudž	k1gFnSc3	Fudž
na	na	k7c4	na
Wikivoyage	Wikivoyage	k1gFnSc4	Wikivoyage
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
PDMZ	PDMZ	kA	PDMZ
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
year-round	yearound	k1gMnSc1	year-round
pictures	pictures	k1gMnSc1	pictures
of	of	k?	of
Mt	Mt	k1gMnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Fuji	Fuji	kA	Fuji
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
výstupu	výstup	k1gInSc6	výstup
na	na	k7c6	na
Fudži	Fudž	k1gFnSc6	Fudž
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Fudži	Fudž	k1gFnSc6	Fudž
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
