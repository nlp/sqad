<s>
Ostrovnoj	Ostrovnoj	k1gInSc1
</s>
<s>
Ostrovnoj	Ostrovnoj	k1gInSc1
О	О	k?
Pohled	pohled	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
68	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
39	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
10	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
3	#num#	k4
Stát	stát	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc4
federální	federální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
</s>
<s>
Severozápadní	severozápadní	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
oblast	oblast	k1gFnSc1
</s>
<s>
Murmanská	murmanský	k2eAgFnSc1d1
</s>
<s>
Murmanská	murmanský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Ruska	Rusko	k1gNnSc2
</s>
<s>
Ostrovnoj	Ostrovnoj	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Murmanské	murmanský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
76	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
847	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
24,3	24,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1981	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.zato-ostrov.ru	www.zato-ostrov.ra	k1gFnSc4
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
81558	#num#	k4
PSČ	PSČ	kA
</s>
<s>
184640	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ostrovnoj	Ostrovnoj	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
О	О	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
uzavřené	uzavřený	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
Murmanské	murmanský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
v	v	k7c6
Ruské	ruský	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
měl	mít	k5eAaImAgInS
2171	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
a	a	k8xC
doprava	doprava	k1gFnSc1
</s>
<s>
Ostrovnoj	Ostrovnoj	k1gInSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c4
severovýchodně	severovýchodně	k6eAd1
poloostrova	poloostrov	k1gInSc2
Koly	Kola	k1gFnSc2
u	u	k7c2
Svjatonosského	Svjatonosský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
Barentsova	Barentsův	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
blízko	blízko	k7c2
ústí	ústí	k1gNnSc2
řeky	řeka	k1gFnSc2
Jokangy	Jokang	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Ostrovného	Ostrovné	k1gNnSc2
měla	mít	k5eAaImAgFnS
vést	vést	k5eAaImF
Kolská	Kolský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
smrti	smrt	k1gFnSc6
Stalina	Stalin	k1gMnSc4
zastavena	zastaven	k2eAgMnSc4d1
a	a	k8xC
z	z	k7c2
níž	jenž	k3xRgFnSc2
byl	být	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
v	v	k7c6
provozu	provoz	k1gInSc6
pouze	pouze	k6eAd1
krátký	krátký	k2eAgInSc4d1
úsek	úsek	k1gInSc4
z	z	k7c2
Apatit	apatit	k1gInSc1
do	do	k7c2
Kirovsku	Kirovsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Ostrovnoj	Ostrovnoj	k1gInSc1
je	být	k5eAaImIp3nS
proto	proto	k8xC
běžně	běžně	k6eAd1
dostupné	dostupný	k2eAgFnPc4d1
pouze	pouze	k6eAd1
lodí	loď	k1gFnSc7
nebo	nebo	k8xC
letecky	letecky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ostrownoi	Ostrowno	k1gFnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ostrovnoj	Ostrovnoj	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
↑	↑	k?
26	#num#	k4
<g/>
.	.	kIx.
Ч	Ч	k?
п	п	k?
н	н	k?
Р	Р	k?
Ф	Ф	k?
п	п	k?
м	м	k?
о	о	k?
н	н	k?
1	#num#	k4
я	я	k?
2018	#num#	k4
г	г	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
