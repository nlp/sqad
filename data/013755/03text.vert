<s>
Chrám	chrám	k1gInSc1
Afaia	Afaium	k1gNnSc2
</s>
<s>
Chrám	chrám	k1gInSc1
Aphaia	Aphaium	k1gNnSc2
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
model	model	k1gInSc1
v	v	k7c6
mnichovské	mnichovský	k2eAgFnSc6d1
glyptotéce	glyptotéka	k1gFnSc6
</s>
<s>
Chrám	chrám	k1gInSc1
Afaia	Afaia	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
řečtině	řečtina	k1gFnSc6
Ἀ	Ἀ	kA
<g/>
,	,	kIx,
Ν	Ν	kA
Α	Α	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zasvěcený	zasvěcený	k2eAgInSc1d1
bohyni	bohyně	k1gFnSc4
Afaia	Afaia	k1gFnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Aigina	Aigina	k1gNnSc1
v	v	k7c6
Saronském	Saronský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
ostrova	ostrov	k1gInSc2
na	na	k7c4
160	#num#	k4
<g/>
m	m	kA
vysokém	vysoký	k2eAgInSc6d1
pahorku	pahorek	k1gInSc6
přibližně	přibližně	k6eAd1
13	#num#	k4
<g/>
km	km	kA
východně	východně	k6eAd1
po	po	k7c6
silnici	silnice	k1gFnSc6
z	z	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
přístavu	přístav	k1gInSc2
Aigina	Aigino	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
hlavní	hlavní	k2eAgFnSc6d1
svatyni	svatyně	k1gFnSc6
starověké	starověký	k2eAgFnSc2d1
Aiginy	Aigina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
také	také	k9
za	za	k7c4
chrám	chrám	k1gInSc4
Dia	Dia	k1gMnSc1
panhelénios	panhelénios	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
za	za	k7c4
chrám	chrám	k1gInSc4
Athény	Athéna	k1gFnSc2
a	a	k8xC
někdy	někdy	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
ještě	ještě	k9
říká	říkat	k5eAaImIp3nS
chrám	chrám	k1gInSc1
Athena-Afaia	Athena-Afaium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Již	již	k9
v	v	k7c6
mykénských	mykénský	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
se	se	k3xPyFc4
zde	zde	k6eAd1
obětovalo	obětovat	k5eAaBmAgNnS
bohyni	bohyně	k1gFnSc4
plodnosti	plodnost	k1gFnSc2
Afaia	Afaium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
570	#num#	k4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
byl	být	k5eAaImAgInS
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
postaven	postavit	k5eAaPmNgInS
kamenný	kamenný	k2eAgInSc1d1
chrám	chrám	k1gInSc1
prostylos	prostylos	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
o	o	k7c4
70	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
nahrazen	nahradit	k5eAaPmNgInS
pozdně	pozdně	k6eAd1
archaickým	archaický	k2eAgInSc7d1
peripterem	peripter	k1gInSc7
<g/>
,	,	kIx,
chrámem	chrám	k1gInSc7
obklopeným	obklopený	k2eAgInSc7d1
ze	z	k7c2
všech	všecek	k3xTgFnPc2
stran	strana	k1gFnPc2
sloupy	sloup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
svatyně	svatyně	k1gFnSc2
chátrala	chátrat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Jedinečné	jedinečný	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
skulptury	skulptura	k1gFnPc1
ve	v	k7c6
štítu	štít	k1gInSc6
chrámu	chrám	k1gInSc2
(	(	kIx(
<g/>
Aigineté	Aiginetý	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
objevené	objevený	k2eAgInPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavy	postav	k1gInPc7
ze	z	k7c2
západního	západní	k2eAgInSc2d1
štítu	štít	k1gInSc2
ještě	ještě	k9
patří	patřit	k5eAaImIp3nS
do	do	k7c2
archaického	archaický	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ty	ten	k3xDgInPc4
z	z	k7c2
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
již	již	k6eAd1
do	do	k7c2
období	období	k1gNnSc2
rané	raný	k2eAgFnSc2d1
klasiky	klasika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
tak	tak	k6eAd1
důležité	důležitý	k2eAgNnSc1d1
vodítko	vodítko	k1gNnSc1
pro	pro	k7c4
datování	datování	k1gNnSc4
stylů	styl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
mnichovské	mnichovský	k2eAgFnSc6d1
glyptotéce	glyptotéka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Svatyně	svatyně	k1gFnSc1
Afaia	Afaia	k1gFnSc1
</s>
<s>
Chrám	chrám	k1gInSc1
bohyně	bohyně	k1gFnSc2
Afaia	Afaium	k1gNnSc2
</s>
<s>
Uctívání	uctívání	k1gNnSc1
bohyně	bohyně	k1gFnSc2
Afaia	Afaium	k1gNnSc2
sahá	sahat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
bohyně	bohyně	k1gFnSc1
uctívána	uctívat	k5eAaImNgFnS
v	v	k7c6
hájku	hájek	k1gInSc6
s	s	k7c7
jeskyní	jeskyně	k1gFnSc7
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
chrám	chrám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
ohraničeno	ohraničit	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
temenos	temenos	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
vápencový	vápencový	k2eAgInSc1d1
chrám	chrám	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c4
počest	počest	k1gFnSc4
bohyně	bohyně	k1gFnSc2
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
kolem	kolem	k7c2
roku	rok	k1gInSc2
570	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
pronaos	pronaos	k1gMnSc1
<g/>
,	,	kIx,
cellu	cello	k1gNnSc6
a	a	k8xC
opistodom	opistodom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
chrámu	chrám	k1gInSc2
nebyl	být	k5eNaImAgMnS
žádný	žádný	k3yNgInSc4
sloup	sloup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
chrámu	chrám	k1gInSc2
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
14	#num#	k4
m	m	kA
vysoký	vysoký	k2eAgInSc1d1
sloup	sloup	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nesl	nést	k5eAaImAgInS
mramorovou	mramorový	k2eAgFnSc4d1
sfingu	sfinga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
520	#num#	k4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
chrám	chrám	k1gInSc1
zničen	zničit	k5eAaPmNgInS
požárem	požár	k1gInSc7
<g/>
,	,	kIx,
takže	takže	k8xS
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
stržen	strhnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
však	však	k9
v	v	k7c6
době	doba	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
510	#num#	k4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
do	do	k7c2
roku	rok	k1gInSc2
490	#num#	k4
<g/>
/	/	kIx~
<g/>
480	#num#	k4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
již	již	k9
jako	jako	k9
peripteros	peripterosa	k1gFnPc2
znovu	znovu	k6eAd1
postaven	postavit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
byl	být	k5eAaImAgInS
posunut	posunout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
vyrovnán	vyrovnán	k2eAgInSc1d1
s	s	k7c7
centrální	centrální	k2eAgFnSc7d1
osou	osa	k1gFnSc7
chrámu	chrám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloup	sloup	k1gInSc1
s	s	k7c7
mramorovou	mramorový	k2eAgFnSc7d1
sfingou	sfinga	k1gFnSc7
nebyl	být	k5eNaImAgInS
požárem	požár	k1gInSc7
zničen	zničit	k5eAaPmNgInS
a	a	k8xC
tedy	tedy	k9
ani	ani	k9
rekonstrukcí	rekonstrukce	k1gFnPc2
nijak	nijak	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
v	v	k7c6
mnichovské	mnichovský	k2eAgFnSc6d1
glyptotéce	glyptotéka	k1gFnSc6
</s>
<s>
Chrám	chrám	k1gInSc1
Athéna-Afaia	Athéna-Afaium	k1gNnSc2
je	být	k5eAaImIp3nS
peripteros	peripterosa	k1gFnPc2
dórského	dórský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
:	:	kIx,
má	mít	k5eAaImIp3nS
6	#num#	k4
x	x	k?
12	#num#	k4
sloupů	sloup	k1gInPc2
(	(	kIx(
<g/>
podél	podél	k6eAd1
hlavní	hlavní	k2eAgFnPc1d1
a	a	k8xC
boční	boční	k2eAgFnPc1d1
fasády	fasáda	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
tříúrovňovém	tříúrovňový	k2eAgInSc6d1
stylobatu	stylobat	k1gInSc6
je	být	k5eAaImIp3nS
pronaos	pronaos	k1gInSc1
<g/>
,	,	kIx,
opistodom	opistodom	k1gInSc1
a	a	k8xC
naos	naos	k1gInSc1
s	s	k7c7
vnitřní	vnitřní	k2eAgFnSc7d1
kolonádou	kolonáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
úhlové	úhlový	k2eAgFnSc3d1
kontrakci	kontrakce	k1gFnSc3
(	(	kIx(
<g/>
zesílení	zesílení	k1gNnSc2
<g/>
)	)	kIx)
sloupů	sloup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgFnSc1d1
kolonáda	kolonáda	k1gFnSc1
je	být	k5eAaImIp3nS
dvouřadá	dvouřadý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
přežilo	přežít	k5eAaPmAgNnS
pouze	pouze	k6eAd1
24	#num#	k4
sloupů	sloup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
naosu	naos	k1gInSc6
chrámu	chrám	k1gInSc2
byla	být	k5eAaImAgFnS
socha	socha	k1gFnSc1
bohyně	bohyně	k1gFnSc1
Afaia	Afaia	k1gFnSc1
<g/>
,	,	kIx,
vedle	vedle	k7c2
ní	on	k3xPp3gFnSc2
byl	být	k5eAaImAgInS
bazének	bazének	k1gInSc1
naplněný	naplněný	k2eAgInSc1d1
olivovým	olivový	k2eAgInSc7d1
olejem	olej	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
tam	tam	k6eAd1
prováděli	provádět	k5eAaImAgMnP
rituální	rituální	k2eAgInPc4d1
obřady	obřad	k1gInPc4
(	(	kIx(
<g/>
socha	socha	k1gFnSc1
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
na	na	k7c6
počátku	počátek	k1gInSc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
500	#num#	k4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
dostala	dostat	k5eAaPmAgFnS
původní	původní	k2eAgFnPc4d1
bohyně	bohyně	k1gFnPc4
Aiginetů	Aiginet	k1gInPc2
<g/>
,	,	kIx,
Afaia	Afaia	k1gFnSc1
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
bohyně	bohyně	k1gFnSc2
Athény	Athéna	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
působí	působit	k5eAaImIp3nS
jako	jako	k9
ochránkyně	ochránkyně	k1gFnSc1
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Archeologické	archeologický	k2eAgFnPc1d1
vykopávky	vykopávka	k1gFnPc1
</s>
<s>
Plán	plán	k1gInSc1
svatyně	svatyně	k1gFnSc2
Afaia	Afaium	k1gNnSc2
</s>
<s>
Místo	místo	k1gNnSc1
bylo	být	k5eAaImAgNnS
zmíněno	zmínit	k5eAaPmNgNnS
geografem	geograf	k1gMnSc7
Pausaniem	Pausanium	k1gNnSc7
(	(	kIx(
<g/>
Cesty	cesta	k1gFnSc2
po	po	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
)	)	kIx)
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
cestovali	cestovat	k5eAaImAgMnP
po	po	k7c6
Řecku	Řecko	k1gNnSc6
John	John	k1gMnSc1
Foster	Foster	k1gMnSc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Robert	Robert	k1gMnSc1
Cockerell	Cockerell	k1gMnSc1
<g/>
,	,	kIx,
baron	baron	k1gMnSc1
Stackelberg	Stackelberg	k1gMnSc1
a	a	k8xC
baron	baron	k1gMnSc1
Haller	Haller	k1gMnSc1
von	von	k1gInSc4
Hallerstein	Hallerstein	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoumali	zkoumat	k5eAaImAgMnP
starověké	starověký	k2eAgInPc4d1
chrámy	chrám	k1gInPc4
a	a	k8xC
s	s	k7c7
pomocí	pomoc	k1gFnSc7
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
označili	označit	k5eAaPmAgMnP
správné	správný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
vykopali	vykopat	k5eAaPmAgMnP
sochy	socha	k1gFnPc4
a	a	k8xC
fragmenty	fragment	k1gInPc4
postav	postava	k1gFnPc2
západního	západní	k2eAgInSc2d1
a	a	k8xC
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
spadly	spadnout	k5eAaPmAgInP
během	během	k7c2
zemětřesení	zemětřesení	k1gNnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
pokryty	pokrýt	k5eAaPmNgFnP
zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
15	#num#	k4
postav	postava	k1gFnPc2
<g/>
,	,	kIx,
13	#num#	k4
hlav	hlava	k1gFnPc2
a	a	k8xC
desítky	desítka	k1gFnPc4
úlomků	úlomek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místním	místní	k2eAgInPc3d1
osmanským	osmanský	k2eAgInPc3d1
úřadům	úřad	k1gInPc3
zaplatili	zaplatit	k5eAaPmAgMnP
pouhých	pouhý	k2eAgInPc2d1
40	#num#	k4
liber	libra	k1gFnPc2
a	a	k8xC
přepravili	přepravit	k5eAaPmAgMnP
sochy	socha	k1gFnPc4
a	a	k8xC
fragmenty	fragment	k1gInPc4
do	do	k7c2
Pirea	Pire	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
pomoci	pomoc	k1gFnSc2
rakouského	rakouský	k2eAgMnSc2d1
konzula	konzul	k1gMnSc2
Gropia	Gropius	k1gMnSc2
<g/>
,	,	kIx,
francouzského	francouzský	k2eAgMnSc2d1
konzula	konzul	k1gMnSc2
Fauvela	Fauvel	k1gMnSc2
a	a	k8xC
Giovanniho	Giovanni	k1gMnSc2
Luzieriho	Luzieri	k1gMnSc2
(	(	kIx(
<g/>
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
kdysi	kdysi	k6eAd1
asistovali	asistovat	k5eAaImAgMnP
při	při	k7c6
podobné	podobný	k2eAgFnSc6d1
operaci	operace	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
postarala	postarat	k5eAaPmAgFnS
o	o	k7c6
zaslání	zaslání	k1gNnSc6
starožitností	starožitnost	k1gFnPc2
na	na	k7c4
Brity	Brit	k1gMnPc4
ovládaný	ovládaný	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
Zakynthos	Zakynthosa	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
listopadu	listopad	k1gInSc6
1812	#num#	k4
uspořádána	uspořádán	k2eAgFnSc1d1
dražba	dražba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
mnoha	mnoho	k4c2
přemístěních	přemístění	k1gNnPc6
byly	být	k5eAaImAgFnP
sochy	socha	k1gFnPc1
restaurovány	restaurován	k2eAgFnPc1d1
v	v	k7c6
Římě	Řím	k1gInSc6
(	(	kIx(
<g/>
se	s	k7c7
změnami	změna	k1gFnPc7
podle	podle	k7c2
dobového	dobový	k2eAgInSc2d1
zvyku	zvyk	k1gInSc2
<g/>
)	)	kIx)
dánským	dánský	k2eAgMnSc7d1
sochařem	sochař	k1gMnSc7
Bertelem	Bertel	k1gMnSc7
Thorvaldsenem	Thorvaldsen	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
doporučení	doporučení	k1gNnSc4
a	a	k8xC
za	za	k7c4
zprostředkování	zprostředkování	k1gNnSc4
barona	baron	k1gMnSc2
Carl	Carl	k1gMnSc1
Haller	Haller	k1gMnSc1
von	von	k1gInSc4
Hallersteina	Hallersteino	k1gNnSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1814	#num#	k4
získal	získat	k5eAaPmAgMnS
bavorský	bavorský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
(	(	kIx(
<g/>
budoucí	budoucí	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Systematické	systematický	k2eAgFnPc1d1
vykopávky	vykopávka	k1gFnPc1
na	na	k7c6
místě	místo	k1gNnSc6
prováděl	provádět	k5eAaImAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Adolf	Adolf	k1gMnSc1
Furtwängler	Furtwängler	k1gMnSc1
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1966	#num#	k4
až	až	k9
1979	#num#	k4
Dieter	Dietrum	k1gNnPc2
Ohly	Ohla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
pokračovali	pokračovat	k5eAaImAgMnP
Erns	Erns	k1gInSc4
Ludwig	Ludwig	k1gMnSc1
Schwandner	Schwandner	k1gMnSc1
a	a	k8xC
Martha	Martha	k1gFnSc1
Ohly	Ohl	k2eAgFnPc1d1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
:	:	kIx,
Během	během	k7c2
těchto	tento	k3xDgFnPc2
vykopávek	vykopávka	k1gFnPc2
byly	být	k5eAaImAgInP
nalezeny	nalezen	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
prvního	první	k4xOgInSc2
kamenného	kamenný	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
kladí	kladí	k1gNnSc1
bylo	být	k5eAaImAgNnS
částečně	částečně	k6eAd1
obnoveno	obnovit	k5eAaPmNgNnS
v	v	k7c6
místním	místní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Štíty	štít	k1gInPc1
</s>
<s>
Skulptury	skulptura	k1gFnPc1
štítů	štít	k1gInPc2
jsou	být	k5eAaImIp3nP
nyní	nyní	k6eAd1
v	v	k7c6
Glyptotéce	glyptotéka	k1gFnSc6
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Čas	čas	k1gInSc4
výstavby	výstavba	k1gFnSc2
druhého	druhý	k4xOgInSc2
chrámu	chrám	k1gInSc2
od	od	k7c2
roku	rok	k1gInSc2
510	#num#	k4
před	před	k7c7
Kristem	Kristus	k1gMnSc7
znamená	znamenat	k5eAaImIp3nS
zlom	zlom	k1gInSc4
v	v	k7c6
kulturněhistorickém	kulturněhistorický	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
přechod	přechod	k1gInSc4
z	z	k7c2
archaického	archaický	k2eAgMnSc2d1
do	do	k7c2
raného	raný	k2eAgNnSc2d1
klasického	klasický	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
ze	z	k7c2
stylu	styl	k1gInSc2
štítových	štítový	k2eAgFnPc2d1
postav	postava	k1gFnPc2
<g/>
:	:	kIx,
postavy	postava	k1gFnPc1
v	v	k7c6
západním	západní	k2eAgInSc6d1
štítu	štít	k1gInSc6
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
archaickým	archaický	k2eAgInSc7d1
stylem	styl	k1gInSc7
<g/>
,	,	kIx,
postavy	postava	k1gFnPc1
ve	v	k7c6
východním	východní	k2eAgInSc6d1
štítu	štít	k1gInSc6
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
klasickým	klasický	k2eAgInSc7d1
stylem	styl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Starší	starý	k2eAgInSc1d2
západní	západní	k2eAgInSc1d1
štít	štít	k1gInSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc4
bitvu	bitva	k1gFnSc4
o	o	k7c4
Tróju	Trója	k1gFnSc4
<g/>
,	,	kIx,
mladší	mladý	k2eAgInSc1d2
východní	východní	k2eAgInSc1d1
štít	štít	k1gInSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
první	první	k4xOgFnSc4
bitvu	bitva	k1gFnSc4
o	o	k7c4
Tróju	Trója	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Západní	západní	k2eAgInSc1d1
štít	štít	k1gInSc1
</s>
<s>
Postavy	postava	k1gFnPc1
západního	západní	k2eAgInSc2d1
štítu	štít	k1gInSc2
</s>
<s>
Postavy	postava	k1gFnPc1
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
tuhé	tuhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nP
samy	sám	k3xTgInPc1
o	o	k7c4
sobě	se	k3xPyFc3
a	a	k8xC
netvoří	tvořit	k5eNaImIp3nS
celek	celek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
odpovídá	odpovídat	k5eAaImIp3nS
archaickému	archaický	k2eAgInSc3d1
stylu	styl	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
spíše	spíše	k9
řada	řada	k1gFnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aias	Aias	k1gInSc1
(	(	kIx(
<g/>
Ajax	Ajax	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
identifikován	identifikovat	k5eAaBmNgInS
podle	podle	k7c2
orla	orel	k1gMnSc2
na	na	k7c6
štítu	štít	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k6eAd1
stojí	stát	k5eAaImIp3nS
bohyně	bohyně	k1gFnSc1
Athéna	Athéna	k1gFnSc1
<g/>
,	,	kIx,
ochránkyně	ochránkyně	k1gFnSc1
Aiginetů	Aiginet	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
obyvatel	obyvatel	k1gMnSc1
ostrova	ostrov	k1gInSc2
Aigina	Aigina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéna	Athéna	k1gFnSc1
zde	zde	k6eAd1
stěží	stěží	k6eAd1
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
její	její	k3xOp3gFnSc1
levá	levý	k2eAgFnSc1d1
noha	noha	k1gFnSc1
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
doleva	doleva	k6eAd1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
lhostejná	lhostejný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgInSc1d1
štít	štít	k1gInSc1
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
původní	původní	k2eAgFnSc2d1
barevnosti	barevnost	k1gFnSc2
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
provedl	provést	k5eAaPmAgMnS
Adolf	Adolf	k1gMnSc1
Furtwängler	Furtwängler	k1gMnSc1
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
štítu	štít	k1gInSc6
již	již	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
styl	styl	k1gInSc4
raného	raný	k2eAgNnSc2d1
klasického	klasický	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k6eAd1
je	být	k5eAaImIp3nS
sice	sice	k8xC
také	také	k9
Athéna	Athéna	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
jasně	jasně	k6eAd1
ve	v	k7c6
výpadu	výpad	k1gInSc6
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
dění	dění	k1gNnSc2
vlevo	vlevo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivci	jednotlivec	k1gMnPc1
jsou	být	k5eAaImIp3nP
navíc	navíc	k6eAd1
zastoupeni	zastoupen	k2eAgMnPc1d1
realističtěji	realisticky	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavy	postav	k1gInPc4
v	v	k7c6
okrajových	okrajový	k2eAgInPc6d1
klínech	klín	k1gInPc6
štítu	štít	k1gInSc2
již	již	k6eAd1
nemají	mít	k5eNaImIp3nP
na	na	k7c6
tvářích	tvář	k1gFnPc6
takzvaný	takzvaný	k2eAgInSc1d1
„	„	k?
<g/>
archaický	archaický	k2eAgInSc4d1
úsměv	úsměv	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
při	při	k7c6
umírání	umírání	k1gNnSc6
„	„	k?
<g/>
vážné	vážná	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
vyobrazená	vyobrazený	k2eAgFnSc1d1
situace	situace	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
přes	přes	k7c4
střed	střed	k1gInSc4
propojena	propojit	k5eAaPmNgFnS
<g/>
:	:	kIx,
lukostřelci	lukostřelec	k1gMnPc7
míří	mířit	k5eAaImIp3nP
přes	přes	k7c4
středovou	středový	k2eAgFnSc4d1
osu	osa	k1gFnSc4
a	a	k8xC
zasahují	zasahovat	k5eAaImIp3nP
lidi	člověk	k1gMnPc4
ležící	ležící	k2eAgFnSc1d1
ve	v	k7c6
štítovém	štítový	k2eAgInSc6d1
klíně	klín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Héraklés	Héraklés	k1gInSc1
byl	být	k5eAaImAgInS
identifikován	identifikovat	k5eAaBmNgInS
podle	podle	k7c2
oděvu	oděv	k1gInSc2
<g/>
:	:	kIx,
jako	jako	k8xC,k8xS
helmu	helma	k1gFnSc4
nosil	nosit	k5eAaImAgInS
horní	horní	k2eAgFnSc4d1
část	část	k1gFnSc4
lví	lví	k2eAgFnSc2d1
hlavy	hlava	k1gFnSc2
(	(	kIx(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1
kithaironský	kithaironský	k2eAgInSc1d1
lev	lev	k1gInSc1
<g/>
,	,	kIx,
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vyobrazení	vyobrazení	k1gNnSc6
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
lví	lví	k2eAgFnSc1d1
kůže	kůže	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
přičítá	přičítat	k5eAaImIp3nS
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
Héraklés	Héraklés	k1gInSc1
zobrazen	zobrazit	k5eAaPmNgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
jako	jako	k9
válečník	válečník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnPc4d1
postavy	postava	k1gFnPc4
lze	lze	k6eAd1
odvodit	odvodit	k5eAaPmF
z	z	k7c2
tradice	tradice	k1gFnSc2
<g/>
:	:	kIx,
umírající	umírající	k2eAgMnSc1d1
muž	muž	k1gMnSc1
v	v	k7c6
levém	levý	k2eAgInSc6d1
štítovém	štítový	k2eAgInSc6d1
klínu	klín	k1gInSc6
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
Láomedón	Láomedón	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
Héraklés	Héraklés	k1gInSc4
v	v	k7c6
první	první	k4xOgFnSc6
bitvě	bitva	k1gFnSc6
o	o	k7c4
Tróju	Trója	k1gFnSc4
smrtelně	smrtelně	k6eAd1
zranil	zranit	k5eAaPmAgMnS
šípem	šíp	k1gInSc7
a	a	k8xC
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
rozhodující	rozhodující	k2eAgInSc1d1
čin	čin	k1gInSc1
pro	pro	k7c4
vítězství	vítězství	k1gNnSc4
Řeků	Řek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Athéna	Athéna	k1gFnSc1
drží	držet	k5eAaImIp3nS
aigis	aigis	k1gInSc4
hrozivě	hrozivě	k6eAd1
nalevo	nalevo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
lze	lze	k6eAd1
usoudit	usoudit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
ohrožená	ohrožený	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
je	být	k5eAaImIp3nS
nepřítelem	nepřítel	k1gMnSc7
Aiginetů	Aiginet	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
Řeků	Řek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
celé	celý	k2eAgNnSc1d1
štítové	štítový	k2eAgNnSc1d1
vyobrazení	vyobrazení	k1gNnSc1
je	být	k5eAaImIp3nS
zjevně	zjevně	k6eAd1
o	o	k7c4
historicky	historicky	k6eAd1
nebo	nebo	k8xC
mýticky	mýticky	k6eAd1
významných	významný	k2eAgFnPc6d1
osobách	osoba	k1gFnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
zobrazen	zobrazen	k2eAgMnSc1d1
trójský	trójský	k2eAgMnSc1d1
král	král	k1gMnSc1
Priamos	Priamos	k1gMnSc1
</s>
<s>
Barevnost	barevnost	k1gFnSc1
chrámu	chrám	k1gInSc2
</s>
<s>
Architektura	architektura	k1gFnSc1
a	a	k8xC
figurální	figurální	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
chrámu	chrám	k1gInSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
antice	antika	k1gFnSc6
barevné	barevný	k2eAgFnPc1d1
(	(	kIx(
<g/>
polychromie	polychromie	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
dnes	dnes	k6eAd1
téměř	téměř	k6eAd1
neviditelné	viditelný	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Objevitel	objevitel	k1gMnSc1
aiginských	aiginský	k2eAgFnPc2d1
štítových	štítový	k2eAgFnPc2d1
skulptur	skulptura	k1gFnPc2
Carl	Carl	k1gMnSc1
Haller	Haller	k1gMnSc1
von	von	k1gInSc4
Hallerstein	Hallerstein	k2eAgInSc4d1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
společník	společník	k1gMnSc1
Charles	Charles	k1gMnSc1
Robert	Robert	k1gMnSc1
Cockerell	Cockerell	k1gMnSc1
nakreslil	nakreslit	k5eAaPmAgMnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
první	první	k4xOgFnSc2
skici	skica	k1gFnSc2
s	s	k7c7
barevnými	barevný	k2eAgFnPc7d1
informacemi	informace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Haller	Haller	k1gMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
bavorskému	bavorský	k2eAgMnSc3d1
korunnímu	korunní	k2eAgMnSc3d1
princi	princ	k1gMnSc3
Ludvíkovi	Ludvík	k1gMnSc3
a	a	k8xC
jednotlivé	jednotlivý	k2eAgFnPc1d1
architektonické	architektonický	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
Mnichova	Mnichov	k1gInSc2
spolu	spolu	k6eAd1
s	s	k7c7
aiginety	aiginet	k1gInPc7
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
jasné	jasný	k2eAgInPc1d1
důkazy	důkaz	k1gInPc1
barevnosti	barevnost	k1gFnSc2
chrámu	chrám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1816	#num#	k4
navrhl	navrhnout	k5eAaPmAgMnS
Ludvíkův	Ludvíkův	k2eAgMnSc1d1
umělecký	umělecký	k2eAgMnSc1d1
agent	agent	k1gMnSc1
Johann	Johann	k1gMnSc1
Martin	Martin	k2eAgInSc4d1
von	von	k1gInSc4
Wagner	Wagner	k1gMnSc1
korunnímu	korunní	k2eAgMnSc3d1
princi	princ	k1gMnSc3
vytvořit	vytvořit	k5eAaPmF
barevné	barevný	k2eAgInPc4d1
modely	model	k1gInPc4
chrámu	chrám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
uskutečnění	uskutečnění	k1gNnSc4
byl	být	k5eAaImAgMnS
zodpovědný	zodpovědný	k2eAgMnSc1d1
Leo	Leo	k1gMnSc1
von	von	k1gInSc4
Klenze	Klenze	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
kolega	kolega	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Daniel	Daniel	k1gMnSc1
Ohlmüller	Ohlmüller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadšení	nadšení	k1gNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
tak	tak	k6eAd1
velké	velký	k2eAgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
očekávalo	očekávat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barevný	barevný	k2eAgInSc4d1
reliéf	reliéf	k1gInSc4
ze	z	k7c2
sádry	sádra	k1gFnSc2
vypadal	vypadat	k5eAaImAgInS,k5eAaPmAgInS
pestře	pestro	k6eAd1
a	a	k8xC
neohrabaně	neohrabaně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existence	existence	k1gFnSc1
tohoto	tento	k3xDgInSc2
barevného	barevný	k2eAgInSc2d1
reliéfu	reliéf	k1gInSc2
se	se	k3xPyFc4
však	však	k9
rozkřikla	rozkřiknout	k5eAaPmAgFnS
a	a	k8xC
francouzský	francouzský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Guillaume-Abel	Guillaume-Abel	k1gMnSc1
Blouet	Blouet	k1gMnSc1
(	(	kIx(
<g/>
1795	#num#	k4
<g/>
-	-	kIx~
<g/>
1853	#num#	k4
<g/>
)	)	kIx)
poslal	poslat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1829	#num#	k4
do	do	k7c2
Mnichova	Mnichov	k1gInSc2
svého	svůj	k3xOyFgMnSc4
kolegu	kolega	k1gMnSc4
Pierra	Pierr	k1gMnSc4
Félixa	Félixus	k1gMnSc4
Trézela	Trézela	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
kresby	kresba	k1gFnPc4
chrámu	chrám	k1gInSc2
doplnil	doplnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1846	#num#	k4
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
další	další	k2eAgFnSc1d1
barevná	barevný	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
od	od	k7c2
Jakoba	Jakob	k1gMnSc2
Ignaze	Ignaha	k1gFnSc6
Hittorffa	Hittorff	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
hlavně	hlavně	k9
na	na	k7c6
díle	dílo	k1gNnSc6
Klenzeho	Klenze	k1gMnSc4
a	a	k8xC
Bloueta	Blouet	k1gMnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
on	on	k3xPp3gInSc1
sám	sám	k3xTgInSc1
chrám	chrám	k1gInSc1
nikdy	nikdy	k6eAd1
neviděl	vidět	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Cockerell	Cockerell	k1gInSc1
zveřejnil	zveřejnit	k5eAaPmAgInS
své	svůj	k3xOyFgFnPc4
barevné	barevný	k2eAgFnPc4d1
rekonstrukce	rekonstrukce	k1gFnPc4
chrámu	chrám	k1gInSc2
v	v	k7c6
ručně	ručně	k6eAd1
vybarvených	vybarvený	k2eAgInPc6d1
listech	list	k1gInPc6
až	až	k9
roku	rok	k1gInSc2
1860	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
byly	být	k5eAaImAgInP
provedeny	provést	k5eAaPmNgInP
„	„	k?
<g/>
podle	podle	k7c2
nálezu	nález	k1gInSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
nach	nach	k1gInSc1
Befund	Befund	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Charles	Charles	k1gMnSc1
Garnier	Garnier	k1gMnSc1
toto	tento	k3xDgNnSc4
důležité	důležitý	k2eAgNnSc4d1
stavební	stavební	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
zaměřil	zaměřit	k5eAaPmAgInS
a	a	k8xC
vypracoval	vypracovat	k5eAaPmAgInS
kompletní	kompletní	k2eAgFnSc4d1
rekonstrukci	rekonstrukce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
v	v	k7c6
Revue	revue	k1gFnSc6
archéologique	archéologique	k1gInSc1
zveřejnil	zveřejnit	k5eAaPmAgInS
tři	tři	k4xCgMnPc4
ze	z	k7c2
svých	svůj	k3xOyFgMnPc2
kreslených	kreslený	k2eAgMnPc2d1
záznamů	záznam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
úplné	úplný	k2eAgFnSc2d1
kresby	kresba	k1gFnSc2
a	a	k8xC
rekonstrukce	rekonstrukce	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
objevily	objevit	k5eAaPmAgInP
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Popisy	popis	k1gInPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
podrobné	podrobný	k2eAgInPc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
z	z	k7c2
nich	on	k3xPp3gFnPc2
zřejmé	zřejmý	k2eAgInPc4d1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
viděno	vidět	k5eAaImNgNnS
a	a	k8xC
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
vyplynulo	vyplynout	k5eAaPmAgNnS
za	za	k7c4
použití	použití	k1gNnSc4
analogií	analogie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ernst	Ernst	k1gMnSc1
Fiechter	Fiechter	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
přítomen	přítomen	k2eAgMnSc1d1
při	při	k7c6
vykopávkách	vykopávka	k1gFnPc6
Adolfa	Adolf	k1gMnSc2
Furtwänglera	Furtwängler	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgInS
barevnou	barevný	k2eAgFnSc4d1
rekonstrukci	rekonstrukce	k1gFnSc4
chrámu	chrámat	k5eAaImIp1nS
Afaia	Afaia	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
platná	platný	k2eAgFnSc1d1
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výzkum	výzkum	k1gInSc1
vykopávek	vykopávka	k1gFnPc2
Dietera	Dieter	k1gMnSc2
Ohlyho	Ohly	k1gMnSc2
ve	v	k7c6
dvacátém	dvacátý	k4xOgNnSc6
století	století	k1gNnSc6
Fiechterovy	Fiechterův	k2eAgFnSc2d1
barevné	barevný	k2eAgFnSc2d1
rekonstrukce	rekonstrukce	k1gFnSc2
potvrdil	potvrdit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Barevnost	barevnost	k1gFnSc1
skulptur	skulptura	k1gFnPc2
ve	v	k7c6
štítech	štít	k1gInPc6
</s>
<s>
Otázka	otázka	k1gFnSc1
barevnosti	barevnost	k1gFnSc2
figur	figura	k1gFnPc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nepřicházela	přicházet	k5eNaImAgFnS
v	v	k7c4
úvahu	úvaha	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
Cockerell	Cockerell	k1gInSc1
a	a	k8xC
Haller	Haller	k1gInSc1
von	von	k1gInSc1
Hallerstein	Hallerstein	k2eAgInSc1d1
zbytky	zbytek	k1gInPc4
barev	barva	k1gFnPc2
na	na	k7c6
skulpturách	skulptura	k1gFnPc6
přímo	přímo	k6eAd1
nalezli	naleznout	k5eAaPmAgMnP,k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obzvláště	obzvláště	k6eAd1
nápadná	nápadný	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
červená	červený	k2eAgFnSc1d1
barva	barva	k1gFnSc1
na	na	k7c6
pouzdrech	pouzdro	k1gNnPc6
pro	pro	k7c4
péra	péro	k1gNnPc4
na	na	k7c6
přilbách	přilba	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
poraněních	poranění	k1gNnPc6
<g/>
,	,	kIx,
na	na	k7c6
štítové	štítový	k2eAgFnSc6d1
podlaze	podlaha	k1gFnSc6
a	a	k8xC
na	na	k7c6
plintech	plint	k1gInPc6
(	(	kIx(
<g/>
čtvercová	čtvercový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
<g/>
,	,	kIx,
základna	základna	k1gFnSc1
sloupové	sloupový	k2eAgFnSc2d1
patky	patka	k1gFnSc2
nebo	nebo	k8xC
sochy	socha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
byly	být	k5eAaImAgFnP
postavy	postava	k1gFnPc1
ukotveny	ukotven	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přilbách	přilba	k1gFnPc6
a	a	k8xC
na	na	k7c6
zadní	zadní	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
štítů	štít	k1gInPc2
byly	být	k5eAaImAgInP
nalezeny	naleznout	k5eAaPmNgInP,k5eAaBmNgInP
také	také	k9
zbytky	zbytek	k1gInPc1
barvy	barva	k1gFnSc2
modré	modrý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
zprávě	zpráva	k1gFnSc6
Johanna	Johanno	k1gNnSc2
Martina	Martin	k1gInSc2
von	von	k1gInSc1
Wagnera	Wagner	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1817	#num#	k4
je	být	k5eAaImIp3nS
již	již	k6eAd1
barevnost	barevnost	k1gFnSc1
soch	socha	k1gFnPc2
zmíněna	zmínit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Barevná	barevný	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
štítu	štít	k1gInSc2
publikovaná	publikovaný	k2eAgFnSc1d1
Blouetem	Bloue	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
1838	#num#	k4
byla	být	k5eAaImAgFnS
až	až	k9
na	na	k7c4
plášť	plášť	k1gInSc4
lukostřelce	lukostřelec	k1gMnSc2
ze	z	k7c2
západního	západní	k2eAgInSc2d1
štítu	štít	k1gInSc2
spíše	spíše	k9
opatrná	opatrný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
od	od	k7c2
Hittorffa	Hittorff	k1gMnSc2
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
mnohem	mnohem	k6eAd1
pestřejší	pestrý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlasy	vlas	k1gInPc1
válečníků	válečník	k1gMnPc2
zčervenaly	zčervenat	k5eAaPmAgInP
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc7
přilby	přilba	k1gFnPc1
a	a	k8xC
štíty	štít	k1gInPc1
byly	být	k5eAaImAgInP
modré	modré	k1gNnSc4
<g/>
,	,	kIx,
žlutě	žlutě	k6eAd1
okrové	okrový	k2eAgFnPc1d1
a	a	k8xC
červené	červený	k2eAgFnPc1d1
<g/>
;	;	kIx,
základ	základ	k1gInSc1
reliéfu	reliéf	k1gInSc2
byl	být	k5eAaImAgInS
znázorněn	znázornit	k5eAaPmNgInS
modrou	modrý	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Cockerell	Cockerell	k1gMnSc1
publikoval	publikovat	k5eAaBmAgMnS
své	svůj	k3xOyFgInPc4
barevné	barevný	k2eAgInPc4d1
tisky	tisk	k1gInPc4
s	s	k7c7
přilbami	přilba	k1gFnPc7
a	a	k8xC
štíty	štít	k1gInPc7
tónovanými	tónovaný	k2eAgInPc7d1
jemnou	jemný	k2eAgFnSc7d1
okrově	okrově	k6eAd1
žlutou	žlutý	k2eAgFnSc7d1
a	a	k8xC
červenou	červený	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
publikované	publikovaný	k2eAgInPc1d1
Garnierem	Garnier	k1gInSc7
jsou	být	k5eAaImIp3nP
nahá	nahý	k2eAgNnPc1d1
těla	tělo	k1gNnPc1
válečníků	válečník	k1gMnPc2
zbarvena	zbarven	k2eAgMnSc4d1
hnědě	hnědě	k6eAd1
a	a	k8xC
plášť	plášť	k1gInSc1
lukostřelce	lukostřelec	k1gMnSc2
je	být	k5eAaImIp3nS
se	se	k3xPyFc4
šupinkovým	šupinkový	k2eAgInSc7d1
vzorem	vzor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
komplexní	komplexní	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
publikace	publikace	k1gFnSc1
o	o	k7c6
chrámu	chrám	k1gInSc6
Afaia	Afaium	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
Adolfa	Adolf	k1gMnSc2
Furtwänglera	Furtwängler	k1gMnSc2
a	a	k8xC
objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starověká	starověký	k2eAgFnSc1d1
polychromie	polychromie	k1gFnSc1
byla	být	k5eAaImAgFnS
důležitým	důležitý	k2eAgInSc7d1
bodem	bod	k1gInSc7
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
štítové	štítový	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
pouze	pouze	k6eAd1
červenou	červený	k2eAgFnSc4d1
a	a	k8xC
modrou	modrý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
na	na	k7c6
figurách	figura	k1gFnPc6
tyto	tento	k3xDgFnPc4
barevné	barevný	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Novější	nový	k2eAgNnSc4d2
zkoumání	zkoumání	k1gNnSc4
polychromie	polychromie	k1gFnSc2
štítových	štítový	k2eAgFnPc2d1
figur	figura	k1gFnPc2
provedl	provést	k5eAaPmAgMnS
Vinzenz	Vinzenz	k1gMnSc1
Brinkmann	Brinkmann	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
trojský	trojský	k2eAgMnSc1d1
lukostřelec	lukostřelec	k1gMnSc1
na	na	k7c6
západním	západní	k2eAgInSc6d1
štítu	štít	k1gInSc6
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
barevného	barevný	k2eAgNnSc2d1
provedení	provedení	k1gNnSc2
od	od	k7c2
Vinzenza	Vinzenz	k1gMnSc4
Brinkmanna	Brinkmann	k1gMnSc2
</s>
<s>
Postava	postava	k1gFnSc1
Parida	Paris	k1gMnSc2
byla	být	k5eAaImAgFnS
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
drobné	drobný	k2eAgNnSc4d1
poškození	poškození	k1gNnSc4
chodidel	chodidlo	k1gNnPc2
a	a	k8xC
obličeje	obličej	k1gInSc2
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
úplně	úplně	k6eAd1
zachována	zachovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
na	na	k7c4
sobě	se	k3xPyFc3
úzké	úzký	k2eAgFnPc4d1
přiléhavé	přiléhavý	k2eAgFnPc4d1
kalhoty	kalhoty	k1gFnPc4
z	z	k7c2
pevného	pevný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nohou	noha	k1gFnPc6
jsou	být	k5eAaImIp3nP
rozeznatelné	rozeznatelný	k2eAgInPc1d1
svaly	sval	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hlavě	hlava	k1gFnSc6
má	mít	k5eAaImIp3nS
skytskou	skytský	k2eAgFnSc4d1
čepici	čepice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
podrobném	podrobný	k2eAgNnSc6d1
zkoumání	zkoumání	k1gNnSc6
byl	být	k5eAaImAgInS
povrch	povrch	k1gInSc4
osvětlen	osvětlen	k2eAgInSc1d1
bočním	boční	k2eAgNnSc7d1
světlem	světlo	k1gNnSc7
a	a	k8xC
vynořila	vynořit	k5eAaPmAgFnS
se	se	k3xPyFc4
struktura	struktura	k1gFnSc1
oděvu	oděv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
těla	tělo	k1gNnSc2
má	mít	k5eAaImIp3nS
vestu	vesta	k1gFnSc4
<g/>
,	,	kIx,
pod	pod	k7c7
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
je	být	k5eAaImIp3nS
jakýsi	jakýsi	k3yIgInSc4
„	„	k?
<g/>
svetr	svetr	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
rukávy	rukáv	k1gInPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
celé	celý	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
kosočtvercový	kosočtvercový	k2eAgInSc4d1
vzor	vzor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
čepici	čepice	k1gFnSc6
byly	být	k5eAaImAgInP
nalezeny	naleznout	k5eAaPmNgInP,k5eAaBmNgInP
zbytky	zbytek	k1gInPc1
červené	červený	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
rozeznatelná	rozeznatelný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
sedmilistá	sedmilistý	k2eAgFnSc1d1
palmeta	palmeta	k1gFnSc1
na	na	k7c6
dvojité	dvojitý	k2eAgFnSc6d1
volutě	voluta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vestě	vesta	k1gFnSc6
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
detailně	detailně	k6eAd1
vypracovaná	vypracovaný	k2eAgNnPc1d1
malá	malý	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
(	(	kIx(
<g/>
gryf	gryf	k1gInSc1
<g/>
,	,	kIx,
lev	lev	k1gMnSc1
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s>
U	u	k7c2
bohatě	bohatě	k6eAd1
zdobeného	zdobený	k2eAgInSc2d1
ornamentu	ornament	k1gInSc2
kalhot	kalhoty	k1gFnPc2
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc4d1
další	další	k2eAgNnSc4d1
zkoumání	zkoumání	k1gNnSc4
<g/>
,	,	kIx,
UV	UV	kA
reflektografie	reflektografie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
tvar	tvar	k1gInSc1
je	být	k5eAaImIp3nS
cikcak	cikcak	k6eAd1
pás	pás	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
nahoře	nahoře	k6eAd1
a	a	k8xC
dole	dole	k6eAd1
končí	končit	k5eAaImIp3nS
ve	v	k7c6
tvaru	tvar	k1gInSc6
kosočtverce	kosočtverec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
tohoto	tento	k3xDgInSc2
vzoru	vzor	k1gInSc2
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
vést	vést	k5eAaImF
cikcak	cikcak	k6eAd1
pás	pás	k1gInSc4
rovnoměrně	rovnoměrně	k6eAd1
a	a	k8xC
bez	bez	k7c2
švů	šev	k1gInPc2
kolem	kolem	k7c2
nohy	noha	k1gFnSc2
<g/>
,	,	kIx,
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
vzdálenosti	vzdálenost	k1gFnPc4
mezi	mezi	k7c7
špičkami	špička	k1gFnPc7
přesně	přesně	k6eAd1
vypočítány	vypočítat	k5eAaPmNgInP
a	a	k8xC
změřeny	změřit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
několika	několik	k4yIc2
pokusů	pokus	k1gInPc2
vyšlo	vyjít	k5eAaPmAgNnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
starověký	starověký	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
použil	použít	k5eAaPmAgMnS
před	před	k7c7
nanesením	nanesení	k1gNnSc7
barvy	barva	k1gFnSc2
na	na	k7c4
figuru	figura	k1gFnSc4
speciální	speciální	k2eAgFnSc4d1
mřížku	mřížka	k1gFnSc4
jako	jako	k8xC,k8xS
vodicí	vodicí	k2eAgFnPc4d1
čáry	čára	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosočtvercový	kosočtvercový	k2eAgInSc1d1
vzor	vzor	k1gInSc1
byl	být	k5eAaImAgInS
přizpůsoben	přizpůsobit	k5eAaPmNgInS
pohybu	pohyb	k1gInSc3
a	a	k8xC
objemu	objem	k1gInSc3
nohou	noha	k1gFnPc2
a	a	k8xC
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
přitom	přitom	k6eAd1
o	o	k7c4
statický	statický	k2eAgInSc4d1
vzor	vzor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Athéna	Athéna	k1gFnSc1
na	na	k7c6
západním	západní	k2eAgInSc6d1
štítu	štít	k1gInSc6
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnPc1
barevnosti	barevnost	k1gFnSc2
některých	některý	k3yIgFnPc2
skulptur	skulptura	k1gFnPc2
na	na	k7c6
západním	západní	k2eAgInSc6d1
štítu	štít	k1gInSc6
chrámu	chrám	k1gInSc2
(	(	kIx(
<g/>
výstava	výstava	k1gFnSc1
„	„	k?
<g/>
Bunte	bunt	k1gInSc5
Götter	Götter	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stojící	stojící	k2eAgFnSc1d1
Athéna	Athéna	k1gFnSc1
byla	být	k5eAaImAgFnS
rovněž	rovněž	k9
prozkoumána	prozkoumat	k5eAaPmNgFnS
bočním	boční	k2eAgNnSc7d1
světlem	světlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
na	na	k7c6
sobě	sebe	k3xPyFc6
bohatě	bohatě	k6eAd1
zbarvené	zbarvený	k2eAgInPc4d1
šaty	šat	k1gInPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
lukostřelec	lukostřelec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
chitonem	chiton	k1gInSc7
má	mít	k5eAaImIp3nS
aigis	aigis	k1gFnSc1
a	a	k8xC
na	na	k7c6
hlavě	hlava	k1gFnSc6
přilbu	přilba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aigis	Aigis	k1gInSc1
lemovaný	lemovaný	k2eAgInSc4d1
hady	had	k1gMnPc4
je	být	k5eAaImIp3nS
opancéřován	opancéřovat	k5eAaPmNgInS
mnoha	mnoho	k4c7
šupinami	šupina	k1gFnPc7
-	-	kIx~
se	s	k7c7
zužujícím	zužující	k2eAgInSc7d1
se	s	k7c7
středním	střední	k2eAgNnSc7d1
žebrem	žebro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpoznat	rozpoznat	k5eAaPmF
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
střední	střední	k2eAgFnSc1d1
barevná	barevný	k2eAgFnSc1d1
lemovka	lemovka	k1gFnSc1
sukně	sukně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlava	hlava	k1gFnSc1
bojovníka	bojovník	k1gMnSc2
z	z	k7c2
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
</s>
<s>
Východní	východní	k2eAgInSc1d1
štít	štít	k1gInSc1
není	být	k5eNaImIp3nS
tak	tak	k6eAd1
dobře	dobře	k6eAd1
zachován	zachovat	k5eAaPmNgInS
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
pozorování	pozorování	k1gNnSc4
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
polychromie	polychromie	k1gFnSc2
soustředěna	soustředit	k5eAaPmNgFnS
jen	jen	k9
na	na	k7c4
několik	několik	k4yIc4
detailů	detail	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlava	hlava	k1gFnSc1
bojovníka	bojovník	k1gMnSc2
je	být	k5eAaImIp3nS
skloněna	skloněn	k2eAgFnSc1d1
mírně	mírně	k6eAd1
dopředu	dopředu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
hlavy	hlava	k1gFnSc2
se	se	k3xPyFc4
z	z	k7c2
této	tento	k3xDgFnSc2
postavy	postava	k1gFnSc2
zachovalo	zachovat	k5eAaPmAgNnS
jen	jen	k9
několik	několik	k4yIc1
fragmentů	fragment	k1gInPc2
a	a	k8xC
nohy	noha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přilbě	přilba	k1gFnSc6
jsou	být	k5eAaImIp3nP
pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
vidět	vidět	k5eAaImF
světlé	světlý	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
kosočtverečnou	kosočtverečný	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
oboustraně	oboustraně	k6eAd1
táhne	táhnout	k5eAaImIp3nS
přes	přes	k7c4
horní	horní	k2eAgFnSc4d1
část	část	k1gFnSc4
lebky	lebka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
publikacích	publikace	k1gFnPc6
Furtwänglera	Furtwängler	k1gMnSc2
a	a	k8xC
Ohlyho	Ohly	k1gMnSc2
jsou	být	k5eAaImIp3nP
stopy	stopa	k1gFnPc1
malby	malba	k1gFnSc2
na	na	k7c6
ilustracích	ilustrace	k1gFnPc6
zvýrazněny	zvýrazněn	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1
bočním	boční	k2eAgNnSc7d1
světlem	světlo	k1gNnSc7
a	a	k8xC
UV	UV	kA
zářením	záření	k1gNnPc3
odhalilo	odhalit	k5eAaPmAgNnS
celoplošný	celoplošný	k2eAgInSc4d1
šupinový	šupinový	k2eAgInSc4d1
vzor	vzor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
znovuobjevit	znovuobjevit	k5eAaPmF
konstrukční	konstrukční	k2eAgInSc4d1
princip	princip	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
vůbec	vůbec	k9
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
provést	provést	k5eAaPmF
jejich	jejich	k3xOp3gNnSc4
rovnoměrné	rovnoměrný	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šupiny	šupina	k1gFnSc2
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
přilby	přilba	k1gFnSc2
jsou	být	k5eAaImIp3nP
navzájem	navzájem	k6eAd1
zrcadlově	zrcadlově	k6eAd1
symetrické	symetrický	k2eAgFnPc1d1
a	a	k8xC
směrem	směr	k1gInSc7
k	k	k7c3
oblasti	oblast	k1gFnSc3
čela	čelo	k1gNnSc2
a	a	k8xC
krku	krk	k1gInSc2
jsou	být	k5eAaImIp3nP
zhuštěné	zhuštěný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Temenos	Temenos	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
homérské	homérský	k2eAgFnSc6d1
řečtině	řečtina	k1gFnSc6
královský	královský	k2eAgInSc1d1
nebo	nebo	k8xC
chrámový	chrámový	k2eAgInSc1d1
majetek	majetek	k1gInSc1
<g/>
,	,	kIx,
poklad	poklad	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
jen	jen	k9
majetek	majetek	k1gInSc1
chrámový	chrámový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
posvátný	posvátný	k2eAgInSc4d1
okrsek	okrsek	k1gInSc4
<g/>
,	,	kIx,
uzavřený	uzavřený	k2eAgInSc4d1
prostor	prostor	k1gInSc4
obklopující	obklopující	k2eAgInSc4d1
oltář	oltář	k1gInSc4
božstva	božstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
výrazem	výraz	k1gInSc7
označoval	označovat	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
chrám	chrám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
širším	široký	k2eAgInSc6d2
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
je	být	k5eAaImIp3nS
temenos	temenos	k1gInSc4
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
kultem	kult	k1gInSc7
boha	bůh	k1gMnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
i	i	k9
půda	půda	k1gFnSc1
<g/>
,	,	kIx,
lesy	les	k1gInPc1
<g/>
,	,	kIx,
pastviny	pastvina	k1gFnPc1
<g/>
,	,	kIx,
dokonce	dokonce	k9
i	i	k9
výrobní	výrobní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
pod	pod	k7c7
správou	správa	k1gFnSc7
chrámu	chrám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Aphaiatempel	Aphaiatemplo	k1gNnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Х	Х	k?
А	А	k?
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Tempio	Tempio	k6eAd1
di	di	k?
Afaia	Afaium	k1gNnPc1
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Pausaniás	Pausaniás	k1gInSc4
<g/>
,	,	kIx,
Periegesi	Periegese	k1gFnSc4
della	dell	k1gMnSc2
Grecia	Grecius	k1gMnSc2
<g/>
,	,	kIx,
2.30	2.30	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Charles	Charles	k1gMnSc1
Garnier	Garnier	k1gMnSc1
<g/>
:	:	kIx,
Le	Le	k1gMnSc1
Temple	templ	k1gInSc5
de	de	k?
Temple	templ	k1gInSc5
de	de	k?
Jupiter	Jupiter	k1gMnSc1
Panhellénien	Panhellénien	k2eAgMnSc1d1
à	à	k?
Égine	Égin	k1gInSc5
(	(	kIx(
<g/>
=	=	kIx~
Restaurations	Restaurations	k1gInSc1
des	des	k1gNnSc1
monuments	monuments	k1gInSc1
antiques	antiques	k1gMnSc1
par	para	k1gFnPc2
les	les	k1gInSc1
architectes	architectes	k1gMnSc1
pensionnaires	pensionnaires	k1gMnSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Académie	Académie	k1gFnSc1
de	de	k?
France	Franc	k1gMnSc2
à	à	k?
Rome	Rom	k1gMnSc5
depuis	depuis	k1gFnPc1
1788	#num#	k4
jusqu	jusqus	k1gInSc2
<g/>
'	'	kIx"
<g/>
à	à	k?
nos	nos	k1gInSc1
jours	jours	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bd	Bd	k1gFnSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paříž	Paříž	k1gFnSc1
1884	#num#	k4
(	(	kIx(
<g/>
Digitalisat	Digitalisat	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dieter	Dieter	k1gInSc1
Ohly	Ohla	k1gFnSc2
<g/>
:	:	kIx,
Die	Die	k1gFnSc7
Aegineten	Aegineten	k2eAgInSc4d1
<g/>
:	:	kIx,
die	die	k?
Marmorskulpturen	Marmorskulpturno	k1gNnPc2
des	des	k1gNnPc2
Tempels	Tempels	k1gInSc1
der	drát	k5eAaImRp2nS
Aphaia	Aphaium	k1gNnPc1
auf	auf	k?
Aegina	Aegin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glyptotheka	Glyptotheek	k1gInSc2
Mnichov	Mnichov	k1gInSc1
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
svazek	svazek	k1gInSc1
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
1976	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vinzenz	Vinzenz	k1gMnSc1
Brinkmann	Brinkmann	k1gMnSc1
<g/>
:	:	kIx,
Bunte	bunt	k1gInSc5
Götter	Göttrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gMnSc1
Farbigkeit	Farbigkeit	k1gMnSc1
antiker	antiker	k1gMnSc1
Skulptur	skulptura	k1gFnPc2
<g/>
;	;	kIx,
eine	einat	k5eAaPmIp3nS
Ausstellung	Ausstellung	k1gMnSc1
der	drát	k5eAaImRp2nS
Staatlichen	Staatlichen	k1gInSc1
Antikensammlung	Antikensammlung	k1gMnSc1
und	und	k?
Glyptothek	Glyptothek	k1gMnSc1
München	München	k2eAgMnSc1d1
in	in	k?
Zusammenarbeit	Zusammenarbeit	k1gMnSc1
mit	mit	k?
der	drát	k5eAaImRp2nS
Ny	Ny	k1gMnSc1
Carlsberg	Carlsberg	k1gMnSc1
Glyptotek	Glyptotka	k1gFnPc2
Kopenhagen	Kopenhagen	k1gInSc4
und	und	k?
den	den	k1gInSc1
Vatikanischen	Vatikanischen	k2eAgInSc1d1
Museen	Museen	k1gInSc1
<g/>
,	,	kIx,
Rom	Rom	k1gMnSc1
<g/>
;	;	kIx,
Glyptothek	Glyptothek	k1gInSc1
München	Münchna	k1gFnPc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dezember	Dezember	k1gInSc1
2003	#num#	k4
bis	bis	k?
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Februar	Februar	k1gInSc1
2004	#num#	k4
<g/>
;	;	kIx,
[	[	kIx(
<g/>
Katalog	katalog	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnichov	Mnichov	k1gInSc1
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vinzenz	Vinzenz	k1gMnSc1
Brinkmann	Brinkmann	k1gMnSc1
<g/>
,	,	kIx,
Ulrike	Ulrike	k1gFnSc1
Koch-Brinkmann	Koch-Brinkmanna	k1gFnPc2
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
prächtige	prächtige	k1gNnSc4
Prinz	Prinz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakl	Nakla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biering	Biering	k1gInSc1
und	und	k?
Brinkmann	Brinkmann	k1gInSc1
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-3-930609-20-8	978-3-930609-20-8	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Furtwängler	Furtwängler	k1gMnSc1
<g/>
:	:	kIx,
Aegina	Aegina	k1gMnSc1
<g/>
,	,	kIx,
das	das	k?
Heiligtum	Heiligtum	k1gNnSc1
der	drát	k5eAaImRp2nS
Aphaia	Aphaium	k1gNnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnichov	Mnichov	k1gInSc1
1906	#num#	k4
(	(	kIx(
<g/>
Digitalisat	Digitalisat	k1gFnPc2
Text	text	k1gInSc1
<g/>
,	,	kIx,
Tabulky	tabulka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
Furtwängler	Furtwängler	k1gMnSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Aegineten	Aegineten	k2eAgMnSc1d1
der	drát	k5eAaImRp2nS
Glyptothek	Glyptothek	k1gMnSc1
König	König	k1gMnSc1
Ludwigs	Ludwigsa	k1gFnPc2
I.	I.	kA
Mnichov	Mnichov	k1gInSc1
1906	#num#	k4
(	(	kIx(
<g/>
Digitalisat	Digitalisat	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Catherina	Catherin	k2eAgFnSc1d1
Philippa	Philippa	k1gFnSc1
Bracken	Brackna	k1gFnPc2
<g/>
:	:	kIx,
Antikenjagd	Antikenjagd	k1gInSc1
in	in	k?
Griechenland	Griechenland	k1gInSc1
<g/>
:	:	kIx,
1800	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakl	Nakla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prestel	Prestel	k1gInSc1
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
1975	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7913	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
418	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
204	#num#	k4
<g/>
–	–	k?
<g/>
234	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dieter	Dieter	k1gMnSc1
Ohly	Ohla	k1gMnSc2
<g/>
:	:	kIx,
Die	Die	k1gMnSc2
Aegineten	Aegineten	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnPc2
Marmorskulpturen	Marmorskulpturna	k1gFnPc2
des	des	k1gNnSc2
Tempels	Tempelsa	k1gFnPc2
der	drát	k5eAaImRp2nS
Aphaia	Aphaium	k1gNnPc1
auf	auf	k?
Aegina	Aegin	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beck	Beck	k1gInSc1
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
1976	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
406	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6271	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
;	;	kIx,
ISBN	ISBN	kA
3-406-06272-5	3-406-06272-5	k4
</s>
<s>
Dieter	Dieter	k1gInSc1
Ohly	Ohla	k1gFnSc2
<g/>
:	:	kIx,
Tempel	Tempel	k1gInSc1
und	und	k?
Heiligtum	Heiligtum	k1gNnSc1
der	drát	k5eAaImRp2nS
Aphaia	Aphaium	k1gNnPc1
auf	auf	k?
Ägina	Ägin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erläutert	Erläutert	k1gInSc1
an	an	k?
den	den	k1gInSc1
Holzmodellen	Holzmodellen	k1gInSc1
in	in	k?
der	drát	k5eAaImRp2nS
Glyptothek	Glyptothek	k1gInSc1
in	in	k?
München	München	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnichov	Mnichov	k1gInSc1
1977	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-406-04115-9	3-406-04115-9	k4
</s>
<s>
Ernst-Ludwig	Ernst-Ludwig	k1gInSc1
Schwandner	Schwandnra	k1gFnPc2
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
ältere	älter	k1gMnSc5
Porostempel	Porostempela	k1gFnPc2
der	drát	k5eAaImRp2nS
Aphaia	Aphaium	k1gNnPc1
auf	auf	k?
Aegina	Aegin	k1gMnSc2
(	(	kIx(
<g/>
=	=	kIx~
Denkmäler	Denkmäler	k1gInSc1
antiker	antikra	k1gFnPc2
Architektur	architektura	k1gFnPc2
<g/>
,	,	kIx,
sv.	sv.	kA
16	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
de	de	k?
Gruyter	Gruytra	k1gFnPc2
<g/>
,	,	kIx,
Berlin	berlina	k1gFnPc2
1985	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-11-010279-X	3-11-010279-X	k4
</s>
<s>
Hansgeorg	Hansgeorg	k1gInSc1
Bankel	Bankela	k1gFnPc2
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
spätarchaische	spätarchaische	k1gNnSc4
Tempel	Tempel	k1gInSc1
der	drát	k5eAaImRp2nS
Aphaia	Aphaium	k1gNnPc1
auf	auf	k?
Aegina	Aegin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architektur	architektura	k1gFnPc2
und	und	k?
Entwurf	Entwurf	k1gMnSc1
(	(	kIx(
<g/>
=	=	kIx~
Denkmäler	Denkmäler	k1gInSc1
Antiker	Antikra	k1gFnPc2
Architektur	architektura	k1gFnPc2
<g/>
,	,	kIx,
sv.	sv.	kA
19	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
de	de	k?
Gruyter	Gruytra	k1gFnPc2
<g/>
,	,	kIx,
Berlin	berlina	k1gFnPc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Heiner	Heiner	k1gMnSc1
Knell	Knell	k1gMnSc1
<g/>
:	:	kIx,
Mythos	Mythos	k1gMnSc1
und	und	k?
Polis	Polis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
WBG	WBG	kA
<g/>
,	,	kIx,
Darmstadt	Darmstadt	k1gInSc1
1990	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
68	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Gottfried	Gottfried	k1gInSc1
Gruben	Gruben	k2eAgInSc1d1
<g/>
:	:	kIx,
Griechische	Griechische	k1gInSc1
Tempel	Tempel	k1gMnSc1
und	und	k?
Heiligtümer	Heiligtümer	k1gMnSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hirmer	Hirmer	k1gInSc1
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
2001	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
121	#num#	k4
<g/>
–	–	k?
<g/>
127	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tonio	Tonio	k1gNnSc1
Hölscher	Hölschra	k1gFnPc2
<g/>
:	:	kIx,
Klassische	Klassische	k1gFnSc1
Archäologie	Archäologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grundwissen	Grundwissen	k1gInSc1
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
119	#num#	k4
<g/>
–	–	k?
<g/>
127	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Aphaiatempel	Aphaiatempela	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
heslo	heslo	k1gNnSc1
temenos	temenos	k1gInSc4
</s>
<s>
Postavy	postav	k1gInPc1
štítů	štít	k1gInPc2
(	(	kIx(
<g/>
Giebelfiguren	Giebelfigurna	k1gFnPc2
<g/>
)	)	kIx)
německy	německy	k6eAd1
</s>
<s>
Chrám	chrám	k1gInSc4
a	a	k8xC
postavy	postava	k1gFnPc4
štítů	štít	k1gInPc2
(	(	kIx(
<g/>
Giebelfiguren	Giebelfigurna	k1gFnPc2
und	und	k?
Tempel	Tempela	k1gFnPc2
<g/>
)	)	kIx)
německy	německy	k6eAd1
</s>
