<s>
Chrám	chrám	k1gInSc1
Afaia	Afaia	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
řečtině	řečtina	k1gFnSc6
Ἀ	Ἀ	kA
<g/>
,	,	kIx,
Ν	Ν	kA
Α	Α	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zasvěcený	zasvěcený	k2eAgInSc1d1
bohyni	bohyně	k1gFnSc4
Afaia	Afaia	k1gFnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Aigina	Aigina	k1gNnSc1
v	v	k7c6
Saronském	Saronský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
ostrova	ostrov	k1gInSc2
na	na	k7c4
160	#num#	k4
<g/>
m	m	kA
vysokém	vysoký	k2eAgInSc6d1
pahorku	pahorek	k1gInSc6
přibližně	přibližně	k6eAd1
13	#num#	k4
<g/>
km	km	kA
východně	východně	k6eAd1
po	po	k7c6
silnici	silnice	k1gFnSc6
z	z	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
přístavu	přístav	k1gInSc2
Aigina	Aigino	k1gNnSc2
<g/>
.	.	kIx.
</s>