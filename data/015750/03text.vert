<s>
Guicciardiniové	Guicciardinius	k1gMnPc1
</s>
<s>
Erb	erb	k1gInSc1
Guicciardiniů	Guicciardini	k1gMnPc2
</s>
<s>
Guicciardiniové	Guicciardinius	k1gMnPc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
Guicciardini	Guicciardin	k2eAgMnPc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
toskánský	toskánský	k2eAgInSc4d1
šlechtický	šlechtický	k2eAgInSc4d1
a	a	k8xC
obchodnický	obchodnický	k2eAgInSc4d1
rod	rod	k1gInSc4
původem	původ	k1gInSc7
z	z	k7c2
Florentské	florentský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
známý	známý	k2eAgInSc1d1
člen	člen	k1gInSc1
rodu	rod	k1gInSc2
Guicciardino	Guicciardin	k2eAgNnSc1d1
Guicciardini	Guicciardin	k2eAgMnPc1d1
působil	působit	k5eAaImAgMnS
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guicciardiniové	Guicciardinius	k1gMnPc1
byli	být	k5eAaImAgMnP
spřízněni	spřízněn	k2eAgMnPc1d1
s	s	k7c7
Mediceji	Medicej	k1gMnPc7
a	a	k8xC
často	často	k6eAd1
vystupovali	vystupovat	k5eAaImAgMnP
jako	jako	k8xS,k8xC
jejich	jejich	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
<g/>
,	,	kIx,
tak	tak	k9
Jacopo	Jacopa	k1gFnSc5
Guicciardini	Guicciardin	k2eAgMnPc1d1
byl	být	k5eAaImAgInS
rádcem	rádce	k1gMnSc7
a	a	k8xC
důvěrníkem	důvěrník	k1gMnSc7
Lorenza	Lorenza	k?
Nádherného	nádherný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejslavnějším	slavný	k2eAgInSc7d3
členem	člen	k1gInSc7
rodu	rod	k1gInSc2
je	být	k5eAaImIp3nS
politik	politik	k1gMnSc1
a	a	k8xC
myslitel	myslitel	k1gMnSc1
Francesco	Francesco	k1gMnSc1
Guicciardini	Guicciardin	k2eAgMnPc1d1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
moderního	moderní	k2eAgNnSc2d1
italského	italský	k2eAgNnSc2d1
dějepisectví	dějepisectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
