<s>
Guicciardiniové	Guicciardinius	k1gMnPc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
Guicciardini	Guicciardin	k2eAgMnPc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
toskánský	toskánský	k2eAgInSc4d1
šlechtický	šlechtický	k2eAgInSc4d1
a	a	k8xC
obchodnický	obchodnický	k2eAgInSc4d1
rod	rod	k1gInSc4
původem	původ	k1gInSc7
z	z	k7c2
Florentské	florentský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>