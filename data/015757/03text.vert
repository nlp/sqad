<s>
Vlajka	vlajka	k1gFnSc1
státu	stát	k1gInSc2
Mandžukuo	Mandžukuo	k6eAd1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Mandžukua-	Mandžukua-	k1gFnSc2
státní	státní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
pro	pro	k7c4
civilní	civilní	k2eAgInPc4d1
účely	účel	k1gInPc4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Mandžukua	Mandžukua	k1gFnSc1
byla	být	k5eAaImAgFnS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
státních	státní	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
(	(	kIx(
<g/>
vedle	vedle	k7c2
hymny	hymna	k1gFnSc2
a	a	k8xC
císařského	císařský	k2eAgInSc2d1
znaku	znak	k1gInSc2
)	)	kIx)
loutkového	loutkový	k2eAgInSc2d1
státu	stát	k1gInSc2
Mandžukuo	Mandžukuo	k6eAd1
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlajka	vlajka	k1gFnSc1
Mandžukua	Mandžukua	k1gFnSc1
byla	být	k5eAaImAgFnS
inspirována	inspirovat	k5eAaBmNgFnS
čínskou	čínský	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Barvy	barva	k1gFnPc1
</s>
<s>
Pět	pět	k4xCc1
barev	barva	k1gFnPc2
je	být	k5eAaImIp3nS
přejatých	přejatý	k2eAgFnPc2d1
z	z	k7c2
čínské	čínský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
roku	rok	k1gInSc2
1912	#num#	k4
<g/>
,	,	kIx,
symbolizující	symbolizující	k2eAgNnPc1d1
pět	pět	k4xCc1
ras	rasa	k1gFnPc2
pod	pod	k7c7
jednou	jeden	k4xCgFnSc7
střechou	střecha	k1gFnSc7
<g/>
,	,	kIx,
výklad	výklad	k1gInSc1
je	být	k5eAaImIp3nS
ale	ale	k9
poněkud	poněkud	k6eAd1
jiný	jiný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
každé	každý	k3xTgFnSc3
barvě	barva	k1gFnSc3
byl	být	k5eAaImAgInS
mimo	mimo	k7c4
národa	národ	k1gInSc2
přiřazen	přiřazen	k2eAgInSc4d1
také	také	k9
smysl	smysl	k1gInSc4
různých	různý	k2eAgFnPc2d1
lidských	lidský	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
1912	#num#	k4
</s>
<s>
Žlutá-	Žlutá-	k?
Mandžuové	Mandžu	k1gMnPc1
<g/>
,	,	kIx,
jednota	jednota	k1gFnSc1
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1
-	-	kIx~
Japonci	Japonec	k1gMnPc1
<g/>
,	,	kIx,
statečnost	statečnost	k1gFnSc4
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
-	-	kIx~
Chanové	Chanové	k2eAgFnSc1d1
<g/>
,	,	kIx,
spravedlnost	spravedlnost	k1gFnSc1
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
-	-	kIx~
Mongolové	Mongol	k1gMnPc1
<g/>
,	,	kIx,
čistota	čistota	k1gFnSc1
</s>
<s>
Černá	Černá	k1gFnSc1
-	-	kIx~
Korejci	Korejec	k1gMnPc1
<g/>
,	,	kIx,
odhodlanost	odhodlanost	k1gFnSc4
</s>
<s>
Obecný	obecný	k2eAgInSc1d1
popis	popis	k1gInSc1
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
,	,	kIx,
symbolizující	symbolizující	k2eAgInSc1d1
Mandžuský	mandžuský	k2eAgInSc1d1
národ	národ	k1gInSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
vlajky	vlajka	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
Mandžukuo	Mandžukuo	k6eAd1
bylo	být	k5eAaImAgNnS
Japonci	Japonec	k1gMnPc1
zamýšlené	zamýšlený	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
stát	stát	k1gInSc1
Mandžuů	Mandžu	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInPc1d1
čtyři	čtyři	k4xCgFnPc1
barvy	barva	k1gFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
kantonu	kanton	k1gInSc6
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
horním	horní	k2eAgInSc6d1
rohu	roh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Strany	strana	k1gFnPc1
vlajky	vlajka	k1gFnSc2
byly	být	k5eAaImAgFnP
rozmezeny	rozmezit	k5eAaPmNgFnP
v	v	k7c6
poměru	poměr	k1gInSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Služební	služební	k2eAgFnPc1d1
vlajky	vlajka	k1gFnPc1
a	a	k8xC
varianty	varianta	k1gFnPc1
vlajky	vlajka	k1gFnSc2
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Císařského	císařský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
Mandžukua	Mandžuku	k1gInSc2
</s>
<s>
Standarty	standarta	k1gFnPc1
vlajkových	vlajkový	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
Císařského	císařský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
Mandžukua	Mandžuku	k1gInSc2
</s>
<s>
Admirál	admirál	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Viceadmirál	viceadmirál	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Kontradmirál	kontradmirál	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Komodor	komodor	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Vlajka	vlajka	k1gFnSc1
udělovaná	udělovaný	k2eAgFnSc1d1
z	z	k7c2
zásluhy	zásluha	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Služební	služební	k2eAgFnSc4d1
</s>
<s>
Válečná	válečný	k2eAgFnSc1d1
a	a	k8xC
námořní	námořní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poštovní	poštovní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Služební	služební	k2eAgFnSc4d1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
hlídkových	hlídkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
Vlajka	vlajka	k1gFnSc1
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
výsostný	výsostný	k2eAgInSc1d1
znak	znak	k1gInSc1
</s>
<s>
Civilní	civilní	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
výsostný	výsostný	k2eAgInSc1d1
znak	znak	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Flags	Flags	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gMnSc1
-	-	kIx~
Manchukuo	Manchukuo	k1gMnSc1
(	(	kIx(
<g/>
Japanese	Japanese	k1gFnSc1
Puppet	Puppeta	k1gFnPc2
State	status	k1gInSc5
in	in	k?
China	China	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Mandžukuo	Mandžukuo	k1gMnSc1
(	(	kIx(
<g/>
滿	滿	k?
<g/>
/	/	kIx~
<g/>
Mǎ	Mǎ	k1gMnSc1
/	/	kIx~
Manshū	Manshū	k1gInSc2
<g/>
)	)	kIx)
Státní	státní	k2eAgFnSc1d1
symboly	symbol	k1gInPc7
</s>
<s>
Vlajka	vlajka	k1gFnSc1
•	•	k?
Znak	znak	k1gInSc1
•	•	k?
Hymna	hymna	k1gFnSc1
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Chuan	Chuan	k1gInSc1
•	•	k?
Centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Mandžuska	Mandžusko	k1gNnSc2
•	•	k?
Jihomandžuská	Jihomandžuský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
•	•	k?
Letecká	letecký	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
Manšú	Manšú	k1gFnSc2
•	•	k?
Automobilka	automobilka	k1gFnSc1
Dowa	Dow	k2eAgFnSc1d1
•	•	k?
Aerolinie	aerolinie	k1gFnSc1
Mandžukua	Mandžuku	k1gInSc2
Ozbrojené	ozbrojený	k2eAgFnSc2d1
síly	síla	k1gFnSc2
</s>
<s>
Letectvo	letectvo	k1gNnSc1
•	•	k?
Námořnictvo	námořnictvo	k1gNnSc1
•	•	k?
Armáda	armáda	k1gFnSc1
•	•	k?
Císařská	císařský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
Významné	významný	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
<g/>
:	:	kIx,
Pchu	Pch	k1gMnSc3
I	I	kA
(	(	kIx(
<g/>
císař	císař	k1gMnSc1
<g/>
)	)	kIx)
Velitelé	velitel	k1gMnPc1
kwantungské	kwantungský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
:	:	kIx,
Otozó	Otozó	k1gFnSc1
Jamada	Jamada	k1gFnSc1
<g/>
,	,	kIx,
Jošihiro	Jošihiro	k1gNnSc1
Umezu	Umez	k1gInSc2
Generálové	generálová	k1gFnSc2
<g/>
,	,	kIx,
předsedové	předseda	k1gMnPc1
vlád	vláda	k1gFnPc2
<g/>
:	:	kIx,
Čang	Čang	k1gMnSc1
Chaj-pcheng	Chaj-pcheng	k1gMnSc1
<g/>
,	,	kIx,
Si	se	k3xPyFc3
Čchia	Čchius	k1gMnSc2
(	(	kIx(
<g/>
generálové	generál	k1gMnPc1
MCA	MCA	kA
<g/>
)	)	kIx)
•	•	k?
Čeng	Čeng	k1gMnSc1
Siao-sü	Siao-sü	k1gMnSc1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
1935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čang	Čang	k1gInSc1
Ťing-chuej	Ťing-chuej	k1gInSc1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1935	#num#	k4
<g/>
)	)	kIx)
</s>
