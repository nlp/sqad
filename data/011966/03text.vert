<p>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Polydore-Marie-Bernard	Polydore-Marie-Bernard	k1gMnSc1	Polydore-Marie-Bernard
Maeterlinck	Maeterlinck	k1gMnSc1	Maeterlinck
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1862	[number]	k4	1862
Gent	Gent	k1gInSc1	Gent
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
Nice	Nice	k1gFnPc2	Nice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
belgický	belgický	k2eAgMnSc1d1	belgický
(	(	kIx(	(
<g/>
vlámský	vlámský	k2eAgMnSc1d1	vlámský
<g/>
)	)	kIx)	)
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
píšící	píšící	k2eAgMnSc1d1	píšící
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
symbolismu	symbolismus	k1gInSc2	symbolismus
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
finančně	finančně	k6eAd1	finančně
dobře	dobře	k6eAd1	dobře
zabezpečené	zabezpečený	k2eAgFnSc2d1	zabezpečená
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Gentu	Gent	k1gInSc6	Gent
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
advokátem	advokát	k1gMnSc7	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
se	se	k3xPyFc4	se
po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
svého	svůj	k3xOyFgNnSc2	svůj
povolání	povolání	k1gNnSc2	povolání
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
pocity	pocit	k1gInPc1	pocit
životní	životní	k2eAgFnSc2d1	životní
osamělosti	osamělost	k1gFnSc2	osamělost
silně	silně	k6eAd1	silně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
básnickou	básnický	k2eAgFnSc4d1	básnická
prvotinu	prvotina	k1gFnSc4	prvotina
<g/>
,	,	kIx,	,
sbírku	sbírka	k1gFnSc4	sbírka
veršů	verš	k1gInPc2	verš
Serres	Serres	k1gMnSc1	Serres
chaudes	chaudes	k1gMnSc1	chaudes
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Skleníky	skleník	k1gInPc1	skleník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
duševní	duševní	k2eAgFnPc4d1	duševní
tesknoty	tesknota	k1gFnPc4	tesknota
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dramatik	dramatik	k1gMnSc1	dramatik
Maeterlinck	Maeterlinck	k1gMnSc1	Maeterlinck
již	již	k6eAd1	již
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
pohádkovém	pohádkový	k2eAgNnSc6d1	pohádkové
dramatu	drama	k1gNnSc6	drama
La	la	k1gNnSc2	la
Princesse	Princess	k1gMnSc5	Princess
Maleine	Malein	k1gMnSc5	Malein
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
Maleina	Maleina	k1gFnSc1	Maleina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nejde	jít	k5eNaImIp3nS	jít
ani	ani	k8xC	ani
o	o	k7c4	o
kresbu	kresba	k1gFnSc4	kresba
charakterů	charakter	k1gInPc2	charakter
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
o	o	k7c4	o
konflikty	konflikt	k1gInPc4	konflikt
vášní	vášeň	k1gFnPc2	vášeň
či	či	k8xC	či
o	o	k7c4	o
psychologicky	psychologicky	k6eAd1	psychologicky
podrobné	podrobný	k2eAgNnSc4d1	podrobné
a	a	k8xC	a
popisné	popisný	k2eAgNnSc4d1	popisné
vystižení	vystižení	k1gNnSc4	vystižení
duševních	duševní	k2eAgInPc2d1	duševní
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
o	o	k7c6	o
básnické	básnický	k2eAgFnSc6d1	básnická
a	a	k8xC	a
mystické	mystický	k2eAgFnSc6d1	mystická
(	(	kIx(	(
<g/>
občas	občas	k6eAd1	občas
i	i	k9	i
s	s	k7c7	s
náznaky	náznak	k1gInPc1	náznak
okultismu	okultismus	k1gInSc2	okultismus
<g/>
)	)	kIx)	)
zobrazení	zobrazení	k1gNnSc2	zobrazení
hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
,	,	kIx,	,
temnoty	temnota	k1gFnSc2	temnota
a	a	k8xC	a
osudovosti	osudovost	k1gFnSc2	osudovost
života	život	k1gInSc2	život
a	a	k8xC	a
bezmocnosti	bezmocnost	k1gFnSc2	bezmocnost
člověka	člověk	k1gMnSc2	člověk
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
lásce	láska	k1gFnSc3	láska
a	a	k8xC	a
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podobného	podobný	k2eAgInSc2d1	podobný
rázu	ráz	k1gInSc2	ráz
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
další	další	k2eAgFnPc1d1	další
dramatické	dramatický	k2eAgFnPc1d1	dramatická
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
Pelléas	Pelléas	k1gInSc4	Pelléas
et	et	k?	et
Mélisande	Mélisand	k1gInSc5	Mélisand
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Pelleas	Pelleas	k1gInSc1	Pelleas
a	a	k8xC	a
Melisanda	Melisanda	k1gFnSc1	Melisanda
<g/>
)	)	kIx)	)
a	a	k8xC	a
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Oiseau	Oisea	k2eAgFnSc4d1	Oisea
Bleu	Blea	k1gFnSc4	Blea
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
básnických	básnický	k2eAgFnPc2d1	básnická
a	a	k8xC	a
dramatických	dramatický	k2eAgFnPc2d1	dramatická
prací	práce	k1gFnPc2	práce
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
filosofické	filosofický	k2eAgFnPc1d1	filosofická
eseje	esej	k1gFnPc1	esej
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	se	k3xPyFc4	se
s	s	k7c7	s
absurditou	absurdita	k1gFnSc7	absurdita
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
mnohostrannou	mnohostranný	k2eAgFnSc4d1	mnohostranná
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
zejména	zejména	k9	zejména
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
dramatické	dramatický	k2eAgFnSc2d1	dramatická
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
bohatou	bohatý	k2eAgFnSc7d1	bohatá
fantazií	fantazie	k1gFnSc7	fantazie
a	a	k8xC	a
poetickým	poetický	k2eAgInSc7d1	poetický
idealismem	idealismus	k1gInSc7	idealismus
a	a	k8xC	a
které	který	k3yQgNnSc1	který
občas	občas	k6eAd1	občas
v	v	k7c6	v
závoji	závoj	k1gInSc6	závoj
pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
podoby	podoba	k1gFnSc2	podoba
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
inspiraci	inspirace	k1gFnSc4	inspirace
a	a	k8xC	a
tajemně	tajemně	k6eAd1	tajemně
oslovují	oslovovat	k5eAaImIp3nP	oslovovat
lidský	lidský	k2eAgInSc4d1	lidský
cit	cit	k1gInSc4	cit
a	a	k8xC	a
imaginaci	imaginace	k1gFnSc4	imaginace
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
mu	on	k3xPp3gMnSc3	on
belgický	belgický	k2eAgMnSc1d1	belgický
král	král	k1gMnSc1	král
Albert	Albert	k1gMnSc1	Albert
I.	I.	kA	I.
udělil	udělit	k5eAaPmAgMnS	udělit
titul	titul	k1gInSc4	titul
hraběte	hrabě	k1gMnSc2	hrabě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
žil	žíla	k1gFnPc2	žíla
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
zámeček	zámeček	k1gInSc4	zámeček
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
koupil	koupit	k5eAaPmAgMnS	koupit
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
a	a	k8xC	a
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
záchvat	záchvat	k1gInSc4	záchvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlásil	hlásit	k5eAaImAgMnS	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
vegetariánství	vegetariánství	k1gNnSc3	vegetariánství
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
přístup	přístup	k1gInSc4	přístup
eticky	eticky	k6eAd1	eticky
a	a	k8xC	a
filosoficky	filosoficky	k6eAd1	filosoficky
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Pohřbený	pohřbený	k2eAgInSc1d1	pohřbený
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
<g/>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
veškeré	veškerý	k3xTgInPc4	veškerý
Maeterlinckovy	Maeterlinckův	k2eAgInPc4d1	Maeterlinckův
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
omnia	omnium	k1gNnSc2	omnium
<g/>
)	)	kIx)	)
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Básnická	básnický	k2eAgFnSc1d1	básnická
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Serres	Serres	k1gMnSc1	Serres
chaudes	chaudes	k1gMnSc1	chaudes
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Skleníky	skleník	k1gInPc1	skleník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Douze	Douze	k6eAd1	Douze
chansons	chansons	k6eAd1	chansons
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Dvanáct	dvanáct	k4xCc1	dvanáct
písní	píseň	k1gFnPc2	píseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Quinze	Quinze	k6eAd1	Quinze
chansons	chansons	k6eAd1	chansons
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Patnáct	patnáct	k4xCc1	patnáct
písní	píseň	k1gFnPc2	píseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
sbírky	sbírka	k1gFnSc2	sbírka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Princesse	Princess	k1gMnSc5	Princess
Maleine	Malein	k1gMnSc5	Malein
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
Maleina	Maleina	k1gFnSc1	Maleina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádkové	pohádkový	k2eAgNnSc1d1	pohádkové
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Intruse	Intruse	k1gFnSc1	Intruse
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Vetřelkyně	vetřelkyně	k1gFnSc1	vetřelkyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Les	les	k1gInSc1	les
Aveugles	Aveugles	k1gInSc1	Aveugles
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Slepci	slepec	k1gMnPc7	slepec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Les	les	k1gInSc4	les
sept	septum	k1gNnPc2	septum
princesses	princesses	k1gInSc1	princesses
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Sedm	sedm	k4xCc1	sedm
princezen	princezna	k1gFnPc2	princezna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutková	loutkový	k2eAgFnSc1d1	loutková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pelléas	Pelléas	k1gInSc1	Pelléas
et	et	k?	et
Mélisande	Mélisand	k1gInSc5	Mélisand
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Pelleas	Pelleas	k1gInSc1	Pelleas
a	a	k8xC	a
Melisanda	Melisanda	k1gFnSc1	Melisanda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
Claude	Claud	k1gInSc5	Claud
Debussy	Debussa	k1gFnSc2	Debussa
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
téma	téma	k1gNnSc4	téma
skomponoval	skomponovat	k5eAaBmAgMnS	skomponovat
skladatel	skladatel	k1gMnSc1	skladatel
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
školy	škola	k1gFnSc2	škola
A.	A.	kA	A.
<g/>
Schonberg	Schonberg	k1gMnSc1	Schonberg
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
báseň	báseň	k1gFnSc4	báseň
</s>
</p>
<p>
<s>
Alladine	Alladinout	k5eAaPmIp3nS	Alladinout
et	et	k?	et
Palomides	Palomides	k1gInSc1	Palomides
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Aladina	Aladina	k1gFnSc1	Aladina
a	a	k8xC	a
Palomides	Palomides	k1gInSc1	Palomides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutková	loutkový	k2eAgFnSc1d1	loutková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Intérieur	Intérieur	k1gMnSc1	Intérieur
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Vnitro	vnitro	k1gNnSc1	vnitro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutková	loutkový	k2eAgFnSc1d1	loutková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Mort	Mort	k1gMnSc1	Mort
de	de	k?	de
Tintagiles	Tintagiles	k1gMnSc1	Tintagiles
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
Tintagilova	Tintagilův	k2eAgFnSc1d1	Tintagilova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutková	loutkový	k2eAgFnSc1d1	loutková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Aglavaine	Aglavainout	k5eAaPmIp3nS	Aglavainout
et	et	k?	et
Sélysette	Sélysett	k1gInSc5	Sélysett
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Aglavena	Aglaven	k2eAgFnSc1d1	Aglaven
a	a	k8xC	a
Selysetta	Selysetta	k1gFnSc1	Selysetta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ariane	Arianout	k5eAaPmIp3nS	Arianout
et	et	k?	et
Barbebleue	Barbebleue	k1gFnSc1	Barbebleue
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Ariana	Ariana	k1gFnSc1	Ariana
a	a	k8xC	a
Modrovous	modrovous	k1gMnSc1	modrovous
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutková	loutkový	k2eAgFnSc1d1	loutková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
Paul	Paul	k1gMnSc1	Paul
Dukas	Dukas	k1gMnSc1	Dukas
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Soeur	Soeur	k1gMnSc1	Soeur
Béatrice	Béatrika	k1gFnSc3	Béatrika
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Sestra	sestra	k1gFnSc1	sestra
Beatrice	Beatrice	k1gFnSc1	Beatrice
(	(	kIx(	(
<g/>
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
text	text	k1gInSc4	text
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
řecký	řecký	k2eAgMnSc1d1	řecký
skladatel	skladatel	k1gMnSc1	skladatel
Dimitris	Dimitris	k1gFnSc2	Dimitris
Mitropoulos	Mitropoulos	k1gMnSc1	Mitropoulos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Monna	Monen	k2eAgFnSc1d1	Monna
Vanna	Vanna	k1gFnSc1	Vanna
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
duchovna	duchovno	k1gNnSc2	duchovno
moderní	moderní	k2eAgFnSc2d1	moderní
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Joyzelle	Joyzelle	k1gFnSc1	Joyzelle
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Joyzella	Joyzella	k1gFnSc1	Joyzella
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Oiseau	Oisea	k2eAgFnSc4d1	Oisea
Bleu	Blea	k1gFnSc4	Blea
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
autor	autor	k1gMnSc1	autor
ve	v	k7c6	v
snovém	snový	k2eAgInSc6d1	snový
příběhu	příběh	k1gInSc6	příběh
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
chudého	chudý	k2eAgMnSc2d1	chudý
drvoštěpa	drvoštěp	k1gMnSc2	drvoštěp
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
putují	putovat	k5eAaImIp3nP	putovat
za	za	k7c7	za
modrým	modrý	k2eAgMnSc7d1	modrý
ptákem	pták	k1gMnSc7	pták
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
odvěkou	odvěký	k2eAgFnSc4d1	odvěká
lidskou	lidský	k2eAgFnSc4d1	lidská
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
štěstí	štěstí	k1gNnSc6	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Modrého	modrý	k2eAgMnSc4d1	modrý
ptáka	pták	k1gMnSc4	pták
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
tohoto	tento	k3xDgNnSc2	tento
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
najdou	najít	k5eAaPmIp3nP	najít
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
pouti	pouť	k1gFnSc2	pouť
doma	doma	k6eAd1	doma
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
darují	darovat	k5eAaPmIp3nP	darovat
sousedčinu	sousedčin	k2eAgNnSc3d1	sousedčino
nemocnému	mocný	k2eNgNnSc3d1	nemocné
děvčátku	děvčátko	k1gNnSc3	děvčátko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
radosti	radost	k1gFnSc2	radost
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
<g/>
,	,	kIx,	,
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
štěstí	štěstí	k1gNnSc1	štěstí
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
ve	v	k7c6	v
vůli	vůle	k1gFnSc6	vůle
zmírnit	zmírnit	k5eAaPmF	zmírnit
jeho	jeho	k3xOp3gNnSc4	jeho
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
úspěšně	úspěšně	k6eAd1	úspěšně
zfilmována	zfilmovat	k5eAaPmNgFnS	zfilmovat
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgInSc4	první
americko-sovětský	americkoovětský	k2eAgInSc4d1	americko-sovětský
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Velice	velice	k6eAd1	velice
modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Alena	Alena	k1gFnSc1	Alena
Morávková	Morávková	k1gFnSc1	Morávková
a	a	k8xC	a
Šárka	Šárka	k1gFnSc1	Šárka
Belisová	Belisový	k2eAgFnSc1d1	Belisová
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Uhde	Uhde	k1gFnSc1	Uhde
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
představení	představení	k1gNnSc1	představení
bylo	být	k5eAaImAgNnS	být
zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
Brno	Brno	k1gNnSc1	Brno
jako	jako	k8xS	jako
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
dramatizace	dramatizace	k1gFnSc1	dramatizace
<g/>
.	.	kIx.	.
<g/>
Le	Le	k1gFnSc1	Le
bourgmestre	bourgmestr	k1gInSc5	bourgmestr
de	de	k?	de
Stilmonde	Stilmond	k1gInSc5	Stilmond
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Stilmondský	Stilmondský	k2eAgMnSc1d1	Stilmondský
starosta	starosta	k1gMnSc1	starosta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Le	Le	k?	Le
miracle	miracle	k1gInSc1	miracle
de	de	k?	de
Saint-Antoine	Saint-Antoin	k1gInSc5	Saint-Antoin
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Zázrak	zázrak	k1gInSc1	zázrak
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veselohra	veselohra	k1gFnSc1	veselohra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eseje	esej	k1gInPc4	esej
===	===	k?	===
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Trésor	Trésor	k1gMnSc1	Trésor
des	des	k1gNnSc1	des
Humbles	Humbles	k1gMnSc1	Humbles
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Poklad	poklad	k1gInSc1	poklad
pokorných	pokorný	k2eAgMnPc2d1	pokorný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
sagesse	sagesse	k1gFnSc2	sagesse
et	et	k?	et
la	la	k1gNnSc1	la
destinée	destinée	k1gFnSc1	destinée
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Moudrost	moudrost	k1gFnSc1	moudrost
a	a	k8xC	a
osud	osud	k1gInSc1	osud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Vie	Vie	k1gFnPc2	Vie
des	des	k1gNnSc1	des
abeilles	abeilles	k1gMnSc1	abeilles
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
včel	včela	k1gFnPc2	včela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
autor	autor	k1gMnSc1	autor
spojil	spojit	k5eAaPmAgMnS	spojit
filozofii	filozofie	k1gFnSc4	filozofie
se	s	k7c7	s
zoologií	zoologie	k1gFnSc7	zoologie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Temple	templ	k1gInSc5	templ
Enseveli	Ensevel	k1gInSc6	Ensevel
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
Pohřbený	pohřbený	k2eAgInSc1d1	pohřbený
chrám	chrám	k1gInSc1	chrám
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Double	double	k2eAgMnSc1d1	double
Jardin	Jardin	k2eAgMnSc1d1	Jardin
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Intelligence	Intelligenec	k1gInSc2	Intelligenec
des	des	k1gNnSc2	des
Fleurs	Fleursa	k1gFnPc2	Fleursa
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Inteligence	inteligence	k1gFnSc1	inteligence
květin	květina	k1gFnPc2	květina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Mort	Morta	k1gFnPc2	Morta
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Les	les	k1gInSc1	les
Sentiers	Sentiersa	k1gFnPc2	Sentiersa
dans	dansa	k1gFnPc2	dansa
la	la	k1gNnSc2	la
montagne	montagne	k1gFnPc2	montagne
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Cesty	cesta	k1gFnSc2	cesta
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Grand	grand	k1gMnSc1	grand
Secret	Secret	k1gMnSc1	Secret
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
vie	vie	k?	vie
des	des	k1gNnPc2	des
termites	termites	k1gInSc1	termites
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
všekazů	všekaz	k1gMnPc2	všekaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plagiát	plagiát	k1gInSc4	plagiát
knihy	kniha	k1gFnSc2	kniha
jihoafrického	jihoafrický	k2eAgMnSc2d1	jihoafrický
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Eugena	Eugen	k1gMnSc2	Eugen
Maraise	Maraise	k1gFnSc2	Maraise
Duše	duše	k1gFnSc2	duše
bílých	bílý	k2eAgMnPc2d1	bílý
mravenců	mravenec	k1gMnPc2	mravenec
(	(	kIx(	(
<g/>
afrikánsky	afrikánsky	k6eAd1	afrikánsky
Die	Die	k1gMnSc1	Die
siel	sienout	k5eAaPmAgMnS	sienout
van	van	k1gInSc4	van
die	die	k?	die
mier	mier	k1gInSc1	mier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
Maeterlinck	Maeterlinck	k1gMnSc1	Maeterlinck
ostudně	ostudně	k6eAd1	ostudně
vydával	vydávat	k5eAaImAgMnS	vydávat
Maraisovy	Maraisův	k2eAgFnPc4d1	Maraisův
myšlenky	myšlenka	k1gFnPc4	myšlenka
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Vie	Vie	k1gFnSc2	Vie
des	des	k1gNnSc2	des
fourmis	fourmis	k1gFnSc2	fourmis
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Grande	grand	k1gMnSc5	grand
Loi	Loi	k1gMnSc6	Loi
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Avant	Avant	k1gMnSc1	Avant
le	le	k?	le
grand	grand	k1gMnSc1	grand
silence	silenka	k1gFnSc3	silenka
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Před	před	k7c7	před
velkým	velký	k2eAgNnSc7d1	velké
mlčením	mlčení	k1gNnSc7	mlčení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Le	Le	k?	Le
sabiler	sabiler	k1gInSc1	sabiler
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Přesýpací	přesýpací	k2eAgFnPc4d1	přesýpací
hodiny	hodina	k1gFnPc4	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Autre	Autr	k1gMnSc5	Autr
Monde	Mond	k1gMnSc5	Mond
ou	ou	k0	ou
le	le	k?	le
Cadran	Cadran	k1gInSc1	Cadran
stellaire	stellair	k1gInSc5	stellair
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Jiné	jiný	k2eAgNnSc1d1	jiné
lidstvo	lidstvo	k1gNnSc1	lidstvo
aneb	aneb	k?	aneb
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
hodiny	hodina	k1gFnSc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bulles	Bulles	k1gMnSc1	Bulles
Bleus	Bleus	k1gMnSc1	Bleus
<g/>
,	,	kIx,	,
Souvenirs	Souvenirs	k1gInSc1	Souvenirs
Heureux	Heureux	k1gInSc1	Heureux
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydané	vydaný	k2eAgFnSc2d1	vydaná
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Maleina	Maleina	k1gFnSc1	Maleina
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc4	tři
básně	báseň	k1gFnPc4	báseň
dramatické	dramatický	k2eAgFnSc2d1	dramatická
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Vetřelkyně	vetřelkyně	k1gFnSc2	vetřelkyně
<g/>
,	,	kIx,	,
Slepci	slepec	k1gMnPc1	slepec
a	a	k8xC	a
Vnitro	vnitro	k1gNnSc1	vnitro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aglavena	Aglaven	k2eAgFnSc1d1	Aglaven
a	a	k8xC	a
Selysetta	Selysetta	k1gFnSc1	Selysetta
<g/>
,	,	kIx,	,
Vzdělavací	Vzdělavací	k2eAgFnSc1d1	Vzdělavací
bibliotéka	bibliotéka	k1gFnSc1	bibliotéka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pelléas	Pelléas	k1gInSc1	Pelléas
a	a	k8xC	a
Melisanda	Melisanda	k1gFnSc1	Melisanda
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Monna	Monen	k2eAgFnSc1d1	Monna
Vanna	Vanna	k1gFnSc1	Vanna
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Sestra	sestra	k1gFnSc1	sestra
Beatrice	Beatrice	k1gFnSc1	Beatrice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
pokorných	pokorný	k2eAgMnPc2d1	pokorný
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgNnPc4	dva
loutková	loutkový	k2eAgNnPc4d1	loutkové
dramata	drama	k1gNnPc4	drama
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Alladina	Alladina	k1gFnSc1	Alladina
a	a	k8xC	a
Palomid	Palomid	k1gInSc1	Palomid
a	a	k8xC	a
Smrt	smrt	k1gFnSc1	smrt
Tintagilova	Tintagilův	k2eAgFnSc1d1	Tintagilova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedm	sedm	k4xCc1	sedm
princezen	princezna	k1gFnPc2	princezna
<g/>
,	,	kIx,	,
Vetřelkyně	vetřelkyně	k1gFnSc1	vetřelkyně
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Essaye	Essaye	k1gFnSc1	Essaye
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Helm	Helm	k1gMnSc1	Helm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pohřbený	pohřbený	k2eAgInSc1d1	pohřbený
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Šafář	Šafář	k1gMnSc1	Šafář
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Joyzella	Joyzella	k1gFnSc1	Joyzella
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Iza	Iza	k1gFnSc2	Iza
Liemertová	Liemertová	k1gFnSc1	Liemertová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
pokorných	pokorný	k2eAgFnPc2d1	pokorná
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
Laichter	Laichter	k1gInSc1	Laichter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Kubišta	Kubišta	k1gMnSc1	Kubišta
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Moudrost	moudrost	k1gFnSc1	moudrost
a	a	k8xC	a
osud	osud	k1gInSc1	osud
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ladislav	Ladislav	k1gMnSc1	Ladislav
Vojtig	Vojtig	k1gMnSc1	Vojtig
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Černovický	Černovický	k2eAgMnSc1d1	Černovický
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Inteligence	inteligence	k1gFnSc1	inteligence
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stilmondský	Stilmondský	k2eAgMnSc1d1	Stilmondský
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
pokorných	pokorný	k2eAgMnPc2d1	pokorný
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zázrak	zázrak	k1gInSc1	zázrak
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nevole	nevole	k1gFnSc1	nevole
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Neubert	Neubert	k1gInSc1	Neubert
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
všekazů	všekaz	k1gMnPc2	všekaz
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Topič	topič	k1gInSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
E.	E.	kA	E.
K.	K.	kA	K.
Rašínová	Rašínová	k1gFnSc1	Rašínová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Topič	topič	k1gInSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
B.	B.	kA	B.
Polívková	polívkový	k2eAgFnSc1d1	polívková
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Přesýpací	přesýpací	k2eAgFnPc1d1	přesýpací
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Topič	topič	k1gInSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Renč	Renč	k?	Renč
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Topič	topič	k1gInSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
B.	B.	kA	B.
Polívková	polívkový	k2eAgFnSc1d1	polívková
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Před	před	k7c7	před
velkým	velký	k2eAgNnSc7d1	velké
mlčením	mlčení	k1gNnSc7	mlčení
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
úvod	úvod	k1gInSc4	úvod
napsal	napsat	k5eAaBmAgMnS	napsat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Stupka	Stupka	k1gMnSc1	Stupka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ariana	Ariana	k1gFnSc1	Ariana
a	a	k8xC	a
Modrovous	modrovous	k1gMnSc1	modrovous
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
dramata	drama	k1gNnPc1	drama
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Svatava	Svatava	k1gFnSc1	Svatava
Bartošová	Bartošová	k1gFnSc1	Bartošová
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Ariana	Ariana	k1gFnSc1	Ariana
a	a	k8xC	a
Modrovous	modrovous	k1gMnSc1	modrovous
<g/>
,	,	kIx,	,
Aladina	Aladina	k1gFnSc1	Aladina
a	a	k8xC	a
Palomides	Palomides	k1gInSc1	Palomides
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
Tintagilova	Tintagilův	k2eAgInSc2d1	Tintagilův
a	a	k8xC	a
Sedm	sedm	k4xCc1	sedm
princezen	princezna	k1gFnPc2	princezna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Svatava	Svatava	k1gFnSc1	Svatava
Bartošová	Bartošová	k1gFnSc1	Bartošová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Kubišta	Kubišta	k1gMnSc1	Kubišta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
TILLE	TILLE	kA	TILLE
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Maeterlinck	Maeterlinck	k1gMnSc1	Maeterlinck
:	:	kIx,	:
analytická	analytický	k2eAgFnSc1d1	analytická
studie	studie	k1gFnSc1	studie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Analytická	analytický	k2eAgFnSc1d1	analytická
studie	studie	k1gFnSc1	studie
o	o	k7c6	o
významném	významný	k2eAgMnSc6d1	významný
belgickém	belgický	k2eAgMnSc6d1	belgický
dramatikovi	dramatik	k1gMnSc6	dramatik
Maurici	Mauric	k1gMnSc6	Mauric
Maeterlinckovi	Maeterlincek	k1gMnSc6	Maeterlincek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vývoj	vývoj	k1gInSc1	vývoj
evropského	evropský	k2eAgNnSc2d1	Evropské
dramatu	drama	k1gNnSc2	drama
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
FISCHER	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otokar	Otokar	k1gMnSc1	Otokar
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
francouzské	francouzský	k2eAgFnSc2d1	francouzská
literatury	literatura	k1gFnSc2	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
770	[number]	k4	770
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Stať	stať	k1gFnSc1	stať
"	"	kIx"	"
<g/>
Symbolistické	symbolistický	k2eAgNnSc1d1	symbolistické
drama	drama	k1gNnSc1	drama
<g/>
:	:	kIx,	:
Maurice	Maurika	k1gFnSc6	Maurika
Maeterlinck	Maeterlincka	k1gFnPc2	Maeterlincka
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
253	[number]	k4	253
<g/>
–	–	k?	–
<g/>
258	[number]	k4	258
<g/>
;	;	kIx,	;
bibliografie	bibliografie	k1gFnSc2	bibliografie
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
285	[number]	k4	285
<g/>
–	–	k?	–
<g/>
286	[number]	k4	286
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
459	[number]	k4	459
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Stať	stať	k1gFnSc1	stať
"	"	kIx"	"
<g/>
Modrý	modrý	k2eAgMnSc1d1	modrý
pták	pták	k1gMnSc1	pták
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
ŠALDA	Šalda	k1gMnSc1	Šalda
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
a	a	k8xC	a
BLAHYNKA	BLAHYNKA	kA	BLAHYNKA
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Šaldův	Šaldův	k2eAgInSc1d1	Šaldův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
:	:	kIx,	:
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
hesel	heslo	k1gNnPc2	heslo
F.	F.	kA	F.
<g/>
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
353	[number]	k4	353
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Heslo	heslo	k1gNnSc4	heslo
"	"	kIx"	"
<g/>
Maurice	Maurika	k1gFnSc6	Maurika
Maeterlinck	Maeterlincka	k1gFnPc2	Maeterlincka
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
otištěno	otisknout	k5eAaPmNgNnS	otisknout
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
175	[number]	k4	175
<g/>
–	–	k?	–
<g/>
177	[number]	k4	177
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Maurice	Maurika	k1gFnSc3	Maurika
Maeterlinck	Maeterlinck	k1gInSc4	Maeterlinck
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Maurice	Maurika	k1gFnSc3	Maurika
Maeterlinck	Maeterlinck	k1gInSc4	Maeterlinck
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Maurice	Maurika	k1gFnSc3	Maurika
Maeterlinck	Maeterlincka	k1gFnPc2	Maeterlincka
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Maurice	Maurika	k1gFnSc3	Maurika
Maeterlinck	Maeterlincka	k1gFnPc2	Maeterlincka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
</s>
</p>
<p>
<s>
http://nobelprize.org/nobel_prizes/literature/laureates/1911/maeterlinck-bio.html	[url]	k1gMnSc1	http://nobelprize.org/nobel_prizes/literature/laureates/1911/maeterlinck-bio.html
</s>
</p>
<p>
<s>
https://web.archive.org/web/20060818124410/http://kirjasto.sci.fi/maeterli.htm	[url]	k1gInSc1	https://web.archive.org/web/20060818124410/http://kirjasto.sci.fi/maeterli.htm
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
http://www.skolavpohode.cz/clanek.asp?polozkaID=6653	[url]	k4	http://www.skolavpohode.cz/clanek.asp?polozkaID=6653
-	-	kIx~	-
česky	česky	k6eAd1	česky
</s>
</p>
