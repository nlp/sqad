<s>
Jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
bolševickou	bolševický	k2eAgFnSc4d1	bolševická
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
?	?	kIx.	?
</s>
