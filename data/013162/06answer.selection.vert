<s>
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xS	jako
bolševická	bolševický	k2eAgFnSc1d1	bolševická
revoluce	revoluce	k1gFnPc1	revoluce
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
samotných	samotný	k2eAgMnPc2d1	samotný
komunistů	komunista	k1gMnPc2	komunista
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
říjnová	říjnový	k2eAgFnSc1d1	říjnová
socialistická	socialistický	k2eAgFnSc1d1	socialistická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
VŘSR	VŘSR	kA	VŘSR
či	či	k8xC	či
Velký	velký	k2eAgInSc4d1	velký
říjen	říjen	k1gInSc4	říjen
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
ruské	ruský	k2eAgFnSc2d1	ruská
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc1d1	následující
po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
