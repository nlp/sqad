<s>
Státní	státní	k2eAgFnSc1d1	státní
poznávací	poznávací	k2eAgFnSc1d1	poznávací
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SPZ	SPZ	kA	SPZ
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
espézetka	espézetka	k1gFnSc1	espézetka
<g/>
,	,	kIx,	,
též	též	k9	též
rozeznávací	rozeznávací	k2eAgFnSc1d1	rozeznávací
<g/>
,	,	kIx,	,
evidenční	evidenční	k2eAgFnSc1d1	evidenční
<g/>
,	,	kIx,	,
rejstříková	rejstříkový	k2eAgFnSc1d1	rejstříková
nebo	nebo	k8xC	nebo
policejní	policejní	k2eAgFnSc1d1	policejní
značka	značka	k1gFnSc1	značka
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
legislativní	legislativní	k2eAgFnSc2d1	legislativní
zkratky	zkratka	k1gFnSc2	zkratka
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
zákoně	zákon	k1gInSc6	zákon
také	také	k9	také
registrační	registrační	k2eAgFnSc1d1	registrační
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
RZ	RZ	kA	RZ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
písmeno-číselné	písmeno-číselný	k2eAgNnSc1d1	písmeno-číselný
označení	označení	k1gNnSc1	označení
motorového	motorový	k2eAgNnSc2d1	motorové
vozidla	vozidlo	k1gNnSc2	vozidlo
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc2	jeho
přívěsu	přívěs	k1gInSc2	přívěs
či	či	k8xC	či
návěsu	návěs	k1gInSc2	návěs
<g/>
,	,	kIx,	,
zaregistrovaného	zaregistrovaný	k2eAgInSc2d1	zaregistrovaný
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
státu	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
označením	označení	k1gNnSc7	označení
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bílé	bílý	k2eAgFnSc2d1	bílá
obdélníkové	obdélníkový	k2eAgFnSc2d1	obdélníková
destičky	destička	k1gFnSc2	destička
s	s	k7c7	s
černými	černý	k2eAgNnPc7d1	černé
písmeny	písmeno	k1gNnPc7	písmeno
a	a	k8xC	a
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinně	povinně	k6eAd1	povinně
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
každém	každý	k3xTgNnSc6	každý
motorovém	motorový	k2eAgNnSc6d1	motorové
vozidle	vozidlo	k1gNnSc6	vozidlo
podléhajícím	podléhající	k2eAgNnSc6d1	podléhající
registraci	registrace	k1gFnSc3	registrace
a	a	k8xC	a
na	na	k7c6	na
přípojných	přípojný	k2eAgNnPc6d1	přípojné
vozidlech	vozidlo	k1gNnPc6	vozidlo
k	k	k7c3	k
motorovým	motorový	k2eAgNnPc3d1	motorové
vozidlům	vozidlo	k1gNnPc3	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
lze	lze	k6eAd1	lze
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
státních	státní	k2eAgFnPc2d1	státní
poznávacích	poznávací	k2eAgFnPc2d1	poznávací
značek	značka	k1gFnPc2	značka
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
oblasti	oblast	k1gFnSc6	oblast
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vozidlo	vozidlo	k1gNnSc1	vozidlo
registrováno	registrovat	k5eAaBmNgNnS	registrovat
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
země	zem	k1gFnPc1	zem
volí	volit	k5eAaImIp3nP	volit
odlišné	odlišný	k2eAgInPc4d1	odlišný
přístupy	přístup	k1gInPc4	přístup
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
a	a	k8xC	a
zda	zda	k8xS	zda
vůbec	vůbec	k9	vůbec
má	mít	k5eAaImIp3nS	mít
SPZ	SPZ	kA	SPZ
prozrazovat	prozrazovat	k5eAaImF	prozrazovat
místo	místo	k1gNnSc4	místo
registrace	registrace	k1gFnSc2	registrace
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
praxe	praxe	k1gFnSc2	praxe
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
údajně	údajně	k6eAd1	údajně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotbalovým	fotbalový	k2eAgMnPc3d1	fotbalový
fanouškům	fanoušek	k1gMnPc3	fanoušek
z	z	k7c2	z
cizího	cizí	k2eAgNnSc2d1	cizí
města	město	k1gNnSc2	město
hrozilo	hrozit	k5eAaImAgNnS	hrozit
poškození	poškození	k1gNnSc1	poškození
jejich	jejich	k3xOp3gNnSc2	jejich
vozidla	vozidlo	k1gNnSc2	vozidlo
domácími	domácí	k2eAgInPc7d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
poznávací	poznávací	k2eAgFnPc1d1	poznávací
značky	značka	k1gFnPc1	značka
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
odlišení	odlišení	k1gNnSc1	odlišení
kromě	kromě	k7c2	kromě
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
písmen	písmeno	k1gNnPc2	písmeno
také	také	k9	také
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
podkladu	podklad	k1gInSc2	podklad
nebo	nebo	k8xC	nebo
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
rozlišena	rozlišen	k2eAgFnSc1d1	rozlišena
například	například	k6eAd1	například
vozidla	vozidlo	k1gNnPc4	vozidlo
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
a	a	k8xC	a
konzulárních	konzulární	k2eAgMnPc2d1	konzulární
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
cizích	cizí	k2eAgMnPc2d1	cizí
státních	státní	k2eAgMnPc2d1	státní
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
,	,	kIx,	,
testovací	testovací	k2eAgNnPc1d1	testovací
nebo	nebo	k8xC	nebo
historická	historický	k2eAgNnPc1d1	historické
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
,	,	kIx,	,
vozidla	vozidlo	k1gNnPc1	vozidlo
z	z	k7c2	z
půjčoven	půjčovna	k1gFnPc2	půjčovna
nebo	nebo	k8xC	nebo
užitková	užitkový	k2eAgNnPc4d1	užitkové
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
slouží	sloužit	k5eAaImIp3nS	sloužit
ještě	ještě	k9	ještě
tzv.	tzv.	kA	tzv.
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
poznávací	poznávací	k2eAgFnSc2d1	poznávací
značky	značka	k1gFnSc2	značka
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
poznávací	poznávací	k2eAgFnPc1d1	poznávací
značky	značka	k1gFnPc1	značka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Státní	státní	k2eAgFnSc2d1	státní
poznávací	poznávací	k2eAgFnSc2d1	poznávací
značky	značka	k1gFnSc2	značka
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
vzor	vzor	k1gInSc1	vzor
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
platný	platný	k2eAgInSc1d1	platný
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Registrační	registrační	k2eAgFnSc2d1	registrační
značky	značka	k1gFnSc2	značka
letadel	letadlo	k1gNnPc2	letadlo
Státní	státní	k2eAgFnSc2d1	státní
poznávací	poznávací	k2eAgFnSc2d1	poznávací
značky	značka	k1gFnSc2	značka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
FE-Schrift	FE-Schrift	k1gInSc1	FE-Schrift
-	-	kIx~	-
písmo	písmo	k1gNnSc1	písmo
německých	německý	k2eAgFnPc2d1	německá
značek	značka	k1gFnPc2	značka
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Registrační	registrační	k2eAgFnSc2d1	registrační
značky	značka	k1gFnSc2	značka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Historie	historie	k1gFnSc1	historie
SPZ	SPZ	kA	SPZ
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
<g/>
/	/	kIx~	/
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
/	/	kIx~	/
<g/>
ČR	ČR	kA	ČR
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
SPZ	SPZ	kA	SPZ
automobilů	automobil	k1gInPc2	automobil
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
od	od	k7c2	od
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
Německá	německý	k2eAgFnSc1d1	německá
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sbírku	sbírka	k1gFnSc4	sbírka
SPZ	SPZ	kA	SPZ
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
