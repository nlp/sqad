<s>
Barva	barva	k1gFnSc1
drahokamového	drahokamový	k2eAgInSc2d1
nefritu	nefrit	k1gInSc2
je	být	k5eAaImIp3nS
sytě	sytě	k6eAd1
špenátově	špenátově	k6eAd1
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
minerál	minerál	k1gInSc1
má	mít	k5eAaImIp3nS
tvrdost	tvrdost	k1gFnSc4
kolem	kolem	k7c2
6	[number]	k4
<g/>
-	-	kIx~
<g/>
6,5	[number]	k4
stupně	stupeň	k1gInSc2
Mohsovy	Mohsův	k2eAgFnSc2d1
stupnice	stupnice	k1gFnSc2
podobně	podobně	k6eAd1
jako	jako	k8xS
křemen	křemen	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
houževnatější	houževnatý	k2eAgMnSc1d2
díky	díky	k7c3
mikrokrystalické	mikrokrystalický	k2eAgFnSc3d1
struktuře	struktura	k1gFnSc3
<g/>
.	.	kIx.
</s>