<p>
<s>
Nefrit	nefrit	k1gInSc1	nefrit
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
Si	se	k3xPyFc3	se
<g/>
8	[number]	k4	8
<g/>
O	o	k7c4	o
<g/>
22	[number]	k4	22
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
drahokam	drahokam	k1gInSc1	drahokam
<g/>
,	,	kIx,	,
odrůda	odrůda	k1gFnSc1	odrůda
amfibolu	amfibol	k1gInSc2	amfibol
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jadeitem	jadeit	k1gInSc7	jadeit
označován	označovat	k5eAaImNgInS	označovat
názvem	název	k1gInSc7	název
jade	jade	k1gNnPc2	jade
<g/>
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Jadeit	jadeit	k1gInSc1	jadeit
je	být	k5eAaImIp3nS	být
pyroxen	pyroxen	k1gInSc4	pyroxen
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Barva	barva	k1gFnSc1	barva
drahokamového	drahokamový	k2eAgInSc2d1	drahokamový
nefritu	nefrit	k1gInSc2	nefrit
je	být	k5eAaImIp3nS	být
sytě	sytě	k6eAd1	sytě
špenátově	špenátově	k6eAd1	špenátově
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
minerál	minerál	k1gInSc1	minerál
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc4	tvrdost
kolem	kolem	k7c2	kolem
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
6,5	[number]	k4	6,5
stupně	stupeň	k1gInSc2	stupeň
Mohsovy	Mohsův	k2eAgFnSc2d1	Mohsova
stupnice	stupnice	k1gFnSc2	stupnice
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
křemen	křemen	k1gInSc4	křemen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
houževnatější	houževnatý	k2eAgMnSc1d2	houževnatější
díky	díky	k7c3	díky
mikrokrystalické	mikrokrystalický	k2eAgFnSc3d1	mikrokrystalická
struktuře	struktura	k1gFnSc3	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
naleštění	naleštění	k1gNnSc6	naleštění
velmi	velmi	k6eAd1	velmi
estetický	estetický	k2eAgInSc1d1	estetický
s	s	k7c7	s
dokonalým	dokonalý	k2eAgInSc7d1	dokonalý
skelným	skelný	k2eAgInSc7d1	skelný
leskem	lesk	k1gInSc7	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
a	a	k8xC	a
stále	stále	k6eAd1	stále
využívané	využívaný	k2eAgNnSc4d1	využívané
naleziště	naleziště	k1gNnSc4	naleziště
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
například	například	k6eAd1	například
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
ruského	ruský	k2eAgInSc2d1	ruský
nefritu	nefrit	k1gInSc2	nefrit
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pojmem	pojem	k1gInSc7	pojem
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Nefrit	nefrit	k1gInSc1	nefrit
je	být	k5eAaImIp3nS	být
drahokam	drahokam	k1gInSc4	drahokam
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
amfibolů	amfibol	k1gInPc2	amfibol
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
2,9	[number]	k4	2,9
<g/>
-	-	kIx~	-
<g/>
3,03	[number]	k4	3,03
<g/>
,	,	kIx,	,
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
1,654	[number]	k4	1,654
<g/>
-	-	kIx~	-
<g/>
1,667	[number]	k4	1,667
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
má	mít	k5eAaImIp3nS	mít
tmavězelenou	tmavězelený	k2eAgFnSc4d1	tmavězelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
plete	plést	k5eAaImIp3nS	plést
s	s	k7c7	s
jadeitem	jadeit	k1gInSc7	jadeit
a	a	k8xC	a
i	i	k9	i
Číňané	Číňan	k1gMnPc1	Číňan
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
kameny	kámen	k1gInPc4	kámen
stejné	stejný	k2eAgNnSc4d1	stejné
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Nefritu	nefrit	k1gInSc2	nefrit
bývaly	bývat	k5eAaImAgFnP	bývat
připisovány	připisovat	k5eAaImNgInP	připisovat
léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
staří	starý	k2eAgMnPc1d1	starý
Číňané	Číňan	k1gMnPc1	Číňan
si	se	k3xPyFc3	se
z	z	k7c2	z
nefritu	nefrit	k1gInSc2	nefrit
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
číše	číš	k1gFnPc4	číš
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgMnPc2	který
pili	pít	k5eAaImAgMnP	pít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
očistnou	očistný	k2eAgFnSc4d1	očistná
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
byly	být	k5eAaImAgFnP	být
i	i	k9	i
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
druhy	druh	k1gInPc4	druh
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
amuletů	amulet	k1gInPc2	amulet
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
přikládaly	přikládat	k5eAaImAgInP	přikládat
léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
detoxikace	detoxikace	k1gFnSc1	detoxikace
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
léčení	léčení	k1gNnSc2	léčení
zánětů	zánět	k1gInPc2	zánět
ledvin	ledvina	k1gFnPc2	ledvina
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
nephritis	nephritis	k1gFnSc1	nephritis
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
drahokamu	drahokam	k1gInSc2	drahokam
<g/>
)	)	kIx)	)
i	i	k8xC	i
močových	močový	k2eAgFnPc2d1	močová
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duševní	duševní	k2eAgFnSc6d1	duševní
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nositel	nositel	k1gMnSc1	nositel
nefritu	nefrit	k1gInSc2	nefrit
nabude	nabýt	k5eAaPmIp3nS	nabýt
duševního	duševní	k2eAgInSc2d1	duševní
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
vyrovnanosti	vyrovnanost	k1gFnSc2	vyrovnanost
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
klidný	klidný	k2eAgInSc4d1	klidný
a	a	k8xC	a
nerušený	rušený	k2eNgInSc4d1	nerušený
spánek	spánek	k1gInSc4	spánek
<g/>
,	,	kIx,	,
uleví	ulevit	k5eAaPmIp3nS	ulevit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
od	od	k7c2	od
bolesti	bolest	k1gFnSc2	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
tlumí	tlumit	k5eAaImIp3nS	tlumit
agresivitu	agresivita	k1gFnSc4	agresivita
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
utišit	utišit	k5eAaPmF	utišit
žal	žal	k1gInSc1	žal
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nefrit	nefrit	k1gInSc1	nefrit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
