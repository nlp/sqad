<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
provedla	provést	k5eAaPmAgFnS	provést
mexická	mexický	k2eAgFnSc1d1	mexická
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
měnovou	měnový	k2eAgFnSc4d1	měnová
reformu	reforma	k1gFnSc4	reforma
-	-	kIx~	-
ta	ten	k3xDgFnSc1	ten
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
1000	[number]	k4	1000
"	"	kIx"	"
<g/>
starých	starý	k2eAgFnPc2d1	stará
<g/>
"	"	kIx"	"
pesos	pesosa	k1gFnPc2	pesosa
stalo	stát	k5eAaPmAgNnS	stát
1	[number]	k4	1
"	"	kIx"	"
<g/>
nové	nový	k2eAgNnSc1d1	nové
<g/>
"	"	kIx"	"
peso	peso	k1gNnSc1	peso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dostalo	dostat	k5eAaPmAgNnS	dostat
současný	současný	k2eAgInSc4d1	současný
kód	kód	k1gInSc4	kód
MXN	MXN	kA	MXN
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
mexické	mexický	k2eAgNnSc4d1	mexické
peso	peso	k1gNnSc4	peso
byl	být	k5eAaImAgMnS	být
MXP	MXP	kA	MXP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
