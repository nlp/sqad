<p>
<s>
Mexické	mexický	k2eAgNnSc1d1	mexické
peso	peso	k1gNnSc1	peso
(	(	kIx(	(
<g/>
$	$	kIx~	$
<g/>
,	,	kIx,	,
Mex	Mex	k1gMnSc1	Mex
<g/>
$	$	kIx~	$
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zákonným	zákonný	k2eAgNnSc7d1	zákonné
platidlem	platidlo	k1gNnSc7	platidlo
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
peso	peso	k1gNnSc1	peso
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
mexická	mexický	k2eAgFnSc1d1	mexická
měna	měna	k1gFnSc1	měna
společný	společný	k2eAgInSc4d1	společný
s	s	k7c7	s
měnami	měna	k1gFnPc7	měna
několika	několik	k4yIc2	několik
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
bývaly	bývat	k5eAaImAgFnP	bývat
španělskými	španělský	k2eAgFnPc7d1	španělská
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
pro	pro	k7c4	pro
peso	peso	k1gNnSc4	peso
je	být	k5eAaImIp3nS	být
$	$	kIx~	$
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
měn	měna	k1gFnPc2	měna
se	se	k3xPyFc4	se
před	před	k7c4	před
tento	tento	k3xDgInSc4	tento
znak	znak	k1gInSc4	znak
dávají	dávat	k5eAaImIp3nP	dávat
písmena	písmeno	k1gNnPc1	písmeno
Mex	Mex	k1gFnSc2	Mex
(	(	kIx(	(
<g/>
Mex	Mex	k1gMnSc1	Mex
<g/>
$	$	kIx~	$
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISO	ISO	kA	ISO
4217	[number]	k4	4217
kód	kód	k1gInSc4	kód
mexické	mexický	k2eAgFnSc2d1	mexická
měny	měna	k1gFnSc2	měna
je	být	k5eAaImIp3nS	být
MXN	MXN	kA	MXN
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
setina	setina	k1gFnSc1	setina
pesa	peso	k1gNnSc2	peso
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
centavo	centava	k1gFnSc5	centava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1861	[number]	k4	1861
se	se	k3xPyFc4	se
mexické	mexický	k2eAgNnSc1d1	mexické
peso	peso	k1gNnSc1	peso
dělilo	dělit	k5eAaImAgNnS	dělit
na	na	k7c4	na
8	[number]	k4	8
realů	real	k1gInPc2	real
<g/>
,	,	kIx,	,
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
používá	používat	k5eAaImIp3nS	používat
desítkovou	desítkový	k2eAgFnSc4d1	desítková
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
provedla	provést	k5eAaPmAgFnS	provést
mexická	mexický	k2eAgFnSc1d1	mexická
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
měnovou	měnový	k2eAgFnSc4d1	měnová
reformu	reforma	k1gFnSc4	reforma
-	-	kIx~	-
ta	ten	k3xDgFnSc1	ten
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
1000	[number]	k4	1000
"	"	kIx"	"
<g/>
starých	starý	k2eAgFnPc2d1	stará
<g/>
"	"	kIx"	"
pesos	pesosa	k1gFnPc2	pesosa
stalo	stát	k5eAaPmAgNnS	stát
1	[number]	k4	1
"	"	kIx"	"
<g/>
nové	nový	k2eAgNnSc1d1	nové
<g/>
"	"	kIx"	"
peso	peso	k1gNnSc1	peso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dostalo	dostat	k5eAaPmAgNnS	dostat
současný	současný	k2eAgInSc4d1	současný
kód	kód	k1gInSc4	kód
MXN	MXN	kA	MXN
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
mexické	mexický	k2eAgNnSc4d1	mexické
peso	peso	k1gNnSc4	peso
byl	být	k5eAaImAgMnS	být
MXP	MXP	kA	MXP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mince	Minka	k1gFnSc6	Minka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
kolují	kolovat	k5eAaImIp3nP	kolovat
mince	mince	k1gFnPc4	mince
v	v	k7c6	v
nominálních	nominální	k2eAgFnPc6d1	nominální
hodnotách	hodnota	k1gFnPc6	hodnota
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
a	a	k8xC	a
50	[number]	k4	50
centavos	centavosa	k1gFnPc2	centavosa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
a	a	k8xC	a
100	[number]	k4	100
pesos	pesosa	k1gFnPc2	pesosa
<g/>
.	.	kIx.	.
</s>
<s>
Cirkulace	cirkulace	k1gFnSc1	cirkulace
mincí	mince	k1gFnPc2	mince
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
20	[number]	k4	20
a	a	k8xC	a
100	[number]	k4	100
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
doplňková	doplňkový	k2eAgFnSc1d1	doplňková
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
raženy	ražen	k2eAgFnPc4d1	ražena
jako	jako	k8xC	jako
pamětní	pamětní	k2eAgFnPc4d1	pamětní
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
hodnotách	hodnota	k1gFnPc6	hodnota
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
bankovky	bankovka	k1gFnPc1	bankovka
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
nepoměrně	poměrně	k6eNd1	poměrně
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
aversní	aversní	k2eAgFnSc6d1	aversní
straně	strana	k1gFnSc6	strana
všech	všecek	k3xTgFnPc2	všecek
mincí	mince	k1gFnPc2	mince
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
reversních	reversní	k2eAgFnPc6d1	reversní
stranách	strana	k1gFnPc6	strana
vyobrazeny	vyobrazen	k2eAgFnPc1d1	vyobrazena
části	část	k1gFnPc1	část
tzv.	tzv.	kA	tzv.
Slunečního	sluneční	k2eAgInSc2d1	sluneční
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
5	[number]	k4	5
pesos	pesos	k1gInSc1	pesos
<g/>
:	:	kIx,	:
Kromě	kromě	k7c2	kromě
základní	základní	k2eAgFnSc2d1	základní
mince	mince	k1gFnSc2	mince
5	[number]	k4	5
pesos	pesosa	k1gFnPc2	pesosa
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
pamětní	pamětní	k2eAgFnPc4d1	pamětní
mince	mince	k1gFnPc4	mince
této	tento	k3xDgFnSc2	tento
nominální	nominální	k2eAgFnSc2d1	nominální
hodnoty	hodnota	k1gFnSc2	hodnota
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
k	k	k7c3	k
výročím	výročí	k1gNnSc7	výročí
Mexické	mexický	k2eAgFnSc2d1	mexická
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Mexika	Mexiko	k1gNnSc2	Mexiko
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
celkem	celkem	k6eAd1	celkem
37	[number]	k4	37
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
vyraženy	vyrazit	k5eAaPmNgInP	vyrazit
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
2008	[number]	k4	2008
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
na	na	k7c6	na
reversních	reversní	k2eAgFnPc6d1	reversní
stranách	strana	k1gFnPc6	strana
těchto	tento	k3xDgFnPc2	tento
mincí	mince	k1gFnPc2	mince
jsou	být	k5eAaImIp3nP	být
vyobrazeny	vyobrazen	k2eAgFnPc4d1	vyobrazena
přední	přední	k2eAgFnPc4d1	přední
osobnosti	osobnost	k1gFnPc4	osobnost
Mexické	mexický	k2eAgFnSc2d1	mexická
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
bojů	boj	k1gInPc2	boj
Mexika	Mexiko	k1gNnSc2	Mexiko
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
existuje	existovat	k5eAaImIp3nS	existovat
38	[number]	k4	38
různých	různý	k2eAgFnPc2d1	různá
mincí	mince	k1gFnPc2	mince
5	[number]	k4	5
pesos	pesosa	k1gFnPc2	pesosa
<g/>
.10	.10	k4	.10
pesos	pesosa	k1gFnPc2	pesosa
<g/>
:	:	kIx,	:
Kromě	kromě	k7c2	kromě
základní	základní	k2eAgFnSc2d1	základní
mince	mince	k1gFnSc2	mince
této	tento	k3xDgFnSc2	tento
nominální	nominální	k2eAgFnSc2d1	nominální
hodnoty	hodnota	k1gFnSc2	hodnota
byla	být	k5eAaImAgNnP	být
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pamětní	pamětní	k2eAgFnSc1d1	pamětní
oběžná	oběžný	k2eAgFnSc1d1	oběžná
mince	mince	k1gFnSc1	mince
připomínající	připomínající	k2eAgFnSc1d1	připomínající
150	[number]	k4	150
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Puebly	Puebly	k1gFnSc2	Puebly
<g/>
.20	.20	k4	.20
pesos	pesosa	k1gFnPc2	pesosa
<g/>
:	:	kIx,	:
Mince	mince	k1gFnSc1	mince
20	[number]	k4	20
pesos	pesosa	k1gFnPc2	pesosa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
3	[number]	k4	3
variantách	varianta	k1gFnPc6	varianta
(	(	kIx(	(
<g/>
Miguel	Miguel	k1gMnSc1	Miguel
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
y	y	k?	y
Costilla	Costilla	k1gMnSc1	Costilla
<g/>
;	;	kIx,	;
Señ	Señ	k1gMnSc1	Señ
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
;	;	kIx,	;
Octavio	Octavio	k1gMnSc1	Octavio
Paz	Paz	k1gMnSc1	Paz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nerazí	razit	k5eNaImIp3nP	razit
a	a	k8xC	a
stahují	stahovat	k5eAaImIp3nP	stahovat
se	se	k3xPyFc4	se
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgMnSc1d3	Nejnovější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
používaná	používaný	k2eAgFnSc1d1	používaná
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyobrazuje	vyobrazovat	k5eAaImIp3nS	vyobrazovat
mexického	mexický	k2eAgMnSc4d1	mexický
držitele	držitel	k1gMnSc4	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Octavia	octavia	k1gFnSc1	octavia
Paz	Paz	k1gFnSc1	Paz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
základní	základní	k2eAgFnSc2d1	základní
mince	mince	k1gFnSc2	mince
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
6	[number]	k4	6
dalších	další	k2eAgFnPc2d1	další
pamětních	pamětní	k2eAgFnPc2d1	pamětní
oběžných	oběžný	k2eAgFnPc2d1	oběžná
mincí	mince	k1gFnPc2	mince
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.50	.50	k4	.50
pesos	pesosa	k1gFnPc2	pesosa
<g/>
:	:	kIx,	:
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1993	[number]	k4	1993
a	a	k8xC	a
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
ražena	ražen	k2eAgFnSc1d1	ražena
mince	mince	k1gFnSc1	mince
50	[number]	k4	50
pesos	pesosa	k1gFnPc2	pesosa
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc4	její
výroba	výroba	k1gFnSc1	výroba
již	již	k9	již
ale	ale	k8xC	ale
nepokračuje	pokračovat	k5eNaImIp3nS	pokračovat
a	a	k8xC	a
mince	mince	k1gFnSc1	mince
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stahuje	stahovat	k5eAaImIp3nS	stahovat
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
<g/>
.100	.100	k4	.100
pesos	pesosa	k1gFnPc2	pesosa
<g/>
:	:	kIx,	:
Mince	mince	k1gFnSc1	mince
100	[number]	k4	100
pesos	pesosa	k1gFnPc2	pesosa
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
s	s	k7c7	s
38	[number]	k4	38
různými	různý	k2eAgInPc7d1	různý
motivy	motiv	k1gInPc7	motiv
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
stahovány	stahovat	k5eAaImNgInP	stahovat
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
32	[number]	k4	32
různých	různý	k2eAgFnPc2d1	různá
mincí	mince	k1gFnPc2	mince
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
31	[number]	k4	31
mexických	mexický	k2eAgInPc2d1	mexický
států	stát	k1gInPc2	stát
a	a	k8xC	a
federálního	federální	k2eAgInSc2d1	federální
distriktu	distrikt	k1gInSc2	distrikt
Cuidad	Cuidad	k1gInSc1	Cuidad
de	de	k?	de
México	México	k6eAd1	México
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
4	[number]	k4	4
nové	nový	k2eAgFnSc2d1	nová
mince	mince	k1gFnSc2	mince
100	[number]	k4	100
pesos	pesosa	k1gFnPc2	pesosa
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
čtyřsté	čtyřstý	k2eAgNnSc4d1	čtyřstý
výročí	výročí	k1gNnSc4	výročí
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
díla	dílo	k1gNnSc2	dílo
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc2	la
Mancha	Manch	k1gMnSc2	Manch
</s>
</p>
<p>
<s>
80	[number]	k4	80
let	let	k1gInSc4	let
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Mexické	mexický	k2eAgFnSc2d1	mexická
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
</s>
</p>
<p>
<s>
470	[number]	k4	470
let	let	k1gInSc4	let
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
první	první	k4xOgFnSc2	první
mincovny	mincovna	k1gFnSc2	mincovna
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
</s>
</p>
<p>
<s>
100	[number]	k4	100
let	let	k1gInSc4	let
od	od	k7c2	od
měnové	měnový	k2eAgFnSc2d1	měnová
reformy	reforma	k1gFnSc2	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
dvousté	dvoustý	k2eAgNnSc1d1	dvoustý
výročí	výročí	k1gNnSc1	výročí
narození	narození	k1gNnSc2	narození
Benita	Benit	k1gMnSc2	Benit
Juáreze	Juáreze	k1gFnSc2	Juáreze
</s>
</p>
<p>
<s>
==	==	k?	==
Bankovky	bankovka	k1gFnPc1	bankovka
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
zavedení	zavedení	k1gNnSc2	zavedení
mexického	mexický	k2eAgNnSc2d1	mexické
pesa	peso	k1gNnSc2	peso
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
sérií	série	k1gFnPc2	série
bankovek	bankovka	k1gFnPc2	bankovka
(	(	kIx(	(
<g/>
AA	AA	kA	AA
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc4d1	aktuální
bankovky	bankovka	k1gFnPc4	bankovka
série	série	k1gFnSc2	série
F	F	kA	F
jsou	být	k5eAaImIp3nP	být
tisknuty	tisknout	k5eAaImNgInP	tisknout
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
20	[number]	k4	20
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
200	[number]	k4	200
<g/>
,	,	kIx,	,
500	[number]	k4	500
a	a	k8xC	a
1000	[number]	k4	1000
pesos	pesosa	k1gFnPc2	pesosa
<g/>
.	.	kIx.	.
</s>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
20	[number]	k4	20
a	a	k8xC	a
50	[number]	k4	50
pesos	pesosa	k1gFnPc2	pesosa
z	z	k7c2	z
polymeru	polymer	k1gInSc2	polymer
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
balvněného	balvněný	k2eAgInSc2d1	balvněný
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
běžných	běžný	k2eAgFnPc2d1	běžná
bankovek	bankovka	k1gFnPc2	bankovka
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
vytištěny	vytisknout	k5eAaPmNgFnP	vytisknout
i	i	k9	i
pamětní	pamětní	k2eAgFnPc1d1	pamětní
bankovky	bankovka	k1gFnPc1	bankovka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
polymerovou	polymerový	k2eAgFnSc4d1	polymerová
bankovku	bankovka	k1gFnSc4	bankovka
100	[number]	k4	100
pesos	pesosa	k1gFnPc2	pesosa
jako	jako	k8xC	jako
připomínku	připomínka	k1gFnSc4	připomínka
stého	stý	k4xOgNnSc2	stý
výročí	výročí	k1gNnSc2	výročí
od	od	k7c2	od
propuknutí	propuknutí	k1gNnSc2	propuknutí
Mexické	mexický	k2eAgFnSc2d1	mexická
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
papírovou	papírový	k2eAgFnSc4d1	papírová
bankovku	bankovka	k1gFnSc4	bankovka
200	[number]	k4	200
pesos	pesosa	k1gFnPc2	pesosa
pro	pro	k7c4	pro
připomenutí	připomenutí	k1gNnSc4	připomenutí
dvoustého	dvoustý	k2eAgNnSc2d1	dvoustý
výročí	výročí	k1gNnSc2	výročí
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Mexika	Mexiko	k1gNnSc2	Mexiko
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mexické	mexický	k2eAgFnSc2d1	mexická
peso	peso	k1gNnSc4	peso
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
