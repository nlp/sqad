<s>
Bobtnání	bobtnání	k1gNnSc1	bobtnání
(	(	kIx(	(
<g/>
též	též	k9	též
botnání	botnání	k1gNnSc2	botnání
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
zvětšování	zvětšování	k1gNnSc2	zvětšování
objemu	objem	k1gInSc2	objem
látky	látka	k1gFnSc2	látka
při	při	k7c6	při
namočení	namočení	k1gNnSc6	namočení
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
osmózy	osmóza	k1gFnSc2	osmóza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
voda	voda	k1gFnSc1	voda
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
pórů	pór	k1gInPc2	pór
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
skelet	skelet	k1gInSc4	skelet
tak	tak	k6eAd1	tak
působí	působit	k5eAaImIp3nS	působit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
botnací	botnací	k2eAgFnSc2d1	botnací
(	(	kIx(	(
<g/>
bobtnací	bobtnací	k2eAgFnSc2d1	bobtnací
<g/>
)	)	kIx)	)
síly	síla	k1gFnSc2	síla
botnací	botnací	k2eAgMnSc1d1	botnací
(	(	kIx(	(
<g/>
bobtnací	bobtnací	k2eAgInSc1d1	bobtnací
<g/>
)	)	kIx)	)
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tlak	tlak	k1gInSc1	tlak
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
značných	značný	k2eAgFnPc2d1	značná
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
při	při	k7c6	při
prudkých	prudký	k2eAgInPc6d1	prudký
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
šocích	šok	k1gInPc6	šok
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
mechanicky	mechanicky	k6eAd1	mechanicky
látku	látka	k1gFnSc4	látka
poškodit	poškodit	k5eAaPmF	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Botnací	Botnací	k2eAgInSc1d1	Botnací
tlak	tlak	k1gInSc1	tlak
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
osmotickému	osmotický	k2eAgInSc3d1	osmotický
tlaku	tlak	k1gInSc3	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Bobtnání	bobtnání	k1gNnSc1	bobtnání
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc4d1	typické
např.	např.	kA	např.
pro	pro	k7c4	pro
některá	některý	k3yIgNnPc4	některý
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
na	na	k7c6	na
klíčení	klíčení	k1gNnSc6	klíčení
<g/>
.	.	kIx.	.
</s>
