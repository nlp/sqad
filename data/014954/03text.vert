<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
Náves	náves	k1gFnSc1
v	v	k7c6
Dobčicích	Dobčice	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
natáčely	natáčet	k5eAaImAgInP
některé	některý	k3yIgInPc1
záběry	záběr	k1gInPc1
filmuZákladní	filmuZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Země	zem	k1gFnSc2
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
komedie	komedie	k1gFnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
TroškaMarek	TroškaMarka	k1gFnPc2
Kališ	kališ	k1gInSc4
Režie	režie	k1gFnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Troška	troška	k6eAd1
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
LangmajerLucie	LangmajerLucie	k1gFnSc2
VondráčkováJan	VondráčkováJan	k1gMnSc1
DolanskýVeronika	DolanskýVeronik	k1gMnSc2
ŽilkováPavel	ŽilkováPavel	k1gMnSc1
KikinčukJana	KikinčukJana	k1gFnSc1
Synková	Synková	k1gFnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Vágner	Vágner	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
34	#num#	k4
000	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Předchozí	předchozí	k2eAgInSc1d1
a	a	k8xC
následující	následující	k2eAgInSc1d1
díl	díl	k1gInSc1
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
2	#num#	k4
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
na	na	k7c4
FP	FP	kA
<g/>
,	,	kIx,
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
film	film	k1gInSc1
režiséra	režisér	k1gMnSc2
Zdeňka	Zdeněk	k1gMnSc2
Trošky	troška	k1gFnSc2
<g/>
,	,	kIx,
třetí	třetí	k4xOgNnSc1
pokračování	pokračování	k1gNnSc1
filmu	film	k1gInSc2
Babovřesky	Babovřeska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
kin	kino	k1gNnPc2
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Sponzoruje	sponzorovat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
TV	TV	kA
Nova	nova	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Televizní	televizní	k2eAgFnSc4d1
premiéru	premiéra	k1gFnSc4
sledovalo	sledovat	k5eAaImAgNnS
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
na	na	k7c6
TV	TV	kA
Nova	nova	k1gFnSc1
1,302	1,302	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Langmajer	Langmajer	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Šoustal	šoustat	k5eAaImAgMnS
–	–	k?
bývalý	bývalý	k2eAgMnSc1d1
farář	farář	k1gMnSc1
</s>
<s>
Lucie	Lucie	k1gFnSc1
Vondráčková	Vondráčková	k1gFnSc1
</s>
<s>
Ivana	Ivana	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Dolanský	Dolanský	k1gMnSc1
</s>
<s>
Adam	Adam	k1gMnSc1
</s>
<s>
Veronika	Veronika	k1gFnSc1
Žilková	Žilková	k1gFnSc1
</s>
<s>
starostova	starostův	k2eAgFnSc1d1
žena	žena	k1gFnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kikinčuk	Kikinčuk	k1gMnSc1
</s>
<s>
starosta	starosta	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
</s>
<s>
Lucie	Lucie	k1gFnSc1
Bílá	bílý	k2eAgFnSc1d1
</s>
<s>
cikánka	cikánka	k1gFnSc1
Aranka	Aranko	k1gNnSc2
</s>
<s>
Tereza	Tereza	k1gFnSc1
Bebarová	Bebarová	k1gFnSc1
</s>
<s>
profesorka	profesorka	k1gFnSc1
<g/>
,	,	kIx,
doktorka	doktorka	k1gFnSc1
Dobromila	Dobromila	k1gFnSc1
Dočistilová	Dočistilová	k1gFnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Trapl	Trapl	k1gMnSc1
</s>
<s>
farář	farář	k1gMnSc1
Zbygniew	Zbygniew	k1gFnSc2
Krapuščinski	Krapuščinsk	k1gFnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Kuželka	kuželka	k1gFnSc1
</s>
<s>
hostinský	hostinský	k1gMnSc1
</s>
<s>
Radek	Radek	k1gMnSc1
Zima	Zima	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
Pavla	Pavla	k1gFnSc1
Bečková	Bečková	k1gFnSc1
</s>
<s>
jeptiška	jeptiška	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Synková	Synková	k1gFnSc1
</s>
<s>
drbna	drbna	k1gFnSc1
Božena	Božena	k1gFnSc1
Horáčková	Horáčková	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Altmannová	Altmannová	k1gFnSc1
</s>
<s>
Bronislav	Bronislav	k1gMnSc1
Kotiš	Kotiš	k1gMnSc1
</s>
<s>
kontrolor	kontrolor	k1gMnSc1
z	z	k7c2
ministerstva	ministerstvo	k1gNnSc2
Dočistil	dočistit	k5eAaPmAgMnS
</s>
<s>
Jindřiška	Jindřiška	k1gFnSc1
Kikinčuková	Kikinčukový	k2eAgFnSc1d1
</s>
<s>
Božena	Božena	k1gFnSc1
Němcová	Němcová	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Pecha	Pecha	k1gMnSc1
</s>
<s>
dědek	dědek	k1gMnSc1
Venda	Venda	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.asaf.cz/wp-content/uploads/2014/09/8097-nove-ceske-filmy_2014.pdf%5B%5D	http://www.asaf.cz/wp-content/uploads/2014/09/8097-nove-ceske-filmy_2014.pdf%5B%5D	k4
<g/>
↑	↑	k?
http://babovresky3.com/	http://babovresky3.com/	k4
↑	↑	k?
Babovřesky	Babovřeska	k1gFnPc4
vidělo	vidět	k5eAaImAgNnS
1,3	1,3	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
Ohnivý	ohnivý	k2eAgMnSc1d1
kuře	kuře	k1gNnSc1
925	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MediaGuru	MediaGur	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
ve	v	k7c6
Filmovém	filmový	k2eAgInSc6d1
přehledu	přehled	k1gInSc6
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Babovřesky	Babovřesky	k6eAd1
3	#num#	k4
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Filmy	film	k1gInPc1
režírované	režírovaný	k2eAgInPc1d1
Zdeňkem	Zdeněk	k1gMnSc7
Troškou	troška	k1gFnSc7
</s>
<s>
Bota	bota	k1gFnSc1
jménem	jméno	k1gNnSc7
Melichar	Melichar	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
seno	seno	k1gNnSc1
<g/>
,	,	kIx,
jahody	jahoda	k1gFnPc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Poklad	poklad	k1gInSc1
hraběte	hrabě	k1gMnSc2
Chamaré	Chamarý	k2eAgFnSc2d1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
O	o	k7c6
princezně	princezna	k1gFnSc6
Jasněnce	Jasněnka	k1gFnSc6
a	a	k8xC
létajícím	létající	k2eAgMnSc6d1
ševci	švec	k1gMnSc6
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slunce	slunce	k1gNnSc4
<g/>
,	,	kIx,
seno	seno	k1gNnSc4
a	a	k8xC
pár	pár	k4xCyI
facek	facka	k1gFnPc2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zkouškové	zkouškový	k2eAgNnSc4d1
období	období	k1gNnSc4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
seno	seno	k1gNnSc1
<g/>
,	,	kIx,
erotika	erotika	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Princezna	princezna	k1gFnSc1
ze	z	k7c2
mlejna	mlejn	k1gInSc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Z	z	k7c2
pekla	peklo	k1gNnSc2
štěstí	štěstí	k1gNnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Princezna	princezna	k1gFnSc1
ze	z	k7c2
mlejna	mlejn	k1gInSc2
2	#num#	k4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Z	z	k7c2
pekla	peklo	k1gNnSc2
štěstí	štěstit	k5eAaImIp3nS
2	#num#	k4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Andělská	andělský	k2eAgFnSc1d1
tvář	tvář	k1gFnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kameňák	Kameňák	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kameňák	Kameňák	k?
2	#num#	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kameňák	Kameňák	k?
3	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nejkrásnější	krásný	k2eAgFnSc1d3
hádanka	hádanka	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Bez	bez	k7c2
předsudků	předsudek	k1gInPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Doktor	doktor	k1gMnSc1
od	od	k7c2
jezera	jezero	k1gNnSc2
hrochů	hroch	k1gMnPc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čertova	čertův	k2eAgFnSc1d1
nevěsta	nevěsta	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Babovřesky	Babovřeska	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Babovřesky	Babovřesk	k1gInPc7
2	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Babovřesky	Babovřesk	k1gInPc7
3	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Strašidla	strašidlo	k1gNnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čertoviny	čertovina	k1gFnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
