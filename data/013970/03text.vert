<s>
Organizace	organizace	k1gFnSc1
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Organizace	organizace	k1gFnSc1
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
partnerské	partnerský	k2eAgInPc1d1
státy	stát	k1gInPc1
pro	pro	k7c4
spolupráci	spolupráce	k1gFnSc4
Zkratka	zkratka	k1gFnSc1
</s>
<s>
OBSE	OBSE	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1995	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
Generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Greminger	Greminger	k1gMnSc1
Ocenění	ocenění	k1gNnSc4
</s>
<s>
Ewald	Ewald	k6eAd1
von	von	k1gInSc1
Kleist	Kleist	k1gInSc1
award	award	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
emperor	emperor	k1gMnSc1
Otto	Otto	k1gMnSc1
award	award	k1gMnSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.osce.org	www.osce.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
OBSE	OBSE	kA
<g/>
;	;	kIx,
anglicky	anglicky	k6eAd1
Organization	Organization	k1gInSc1
for	forum	k1gNnPc2
Security	Securita	k1gFnSc2
and	and	k?
Cooperation	Cooperation	k1gInSc1
in	in	k?
Europe	Europ	k1gMnSc5
<g/>
,	,	kIx,
OSCE	OSCE	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
roku	rok	k1gInSc2
1995	#num#	k4
transformací	transformace	k1gFnSc7
Konference	konference	k1gFnSc2
o	o	k7c6
bezpečnosti	bezpečnost	k1gFnSc6
a	a	k8xC
spolupráci	spolupráce	k1gFnSc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
KBSE	KBSE	kA
<g/>
,	,	kIx,
též	též	k9
Helsinská	helsinský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OBSE	OBSE	kA
sdružuje	sdružovat	k5eAaImIp3nS
převážně	převážně	k6eAd1
evropské	evropský	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gMnSc7
je	být	k5eAaImIp3nS
57	#num#	k4
států	stát	k1gInPc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
států	stát	k1gInPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
při	při	k7c6
Středozemním	středozemní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
také	také	k9
státy	stát	k1gInPc4
zakavkazské	zakavkazský	k2eAgInPc4d1
<g/>
,	,	kIx,
středoasijské	středoasijský	k2eAgInPc4d1
a	a	k8xC
USA	USA	kA
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
Zasedání	zasedání	k1gNnSc1
Stálé	stálý	k2eAgFnSc2d1
rady	rada	k1gFnSc2
OBSE	OBSE	kA
</s>
<s>
Orgány	orgán	k1gInPc1
</s>
<s>
Následná	následný	k2eAgFnSc1d1
schůze	schůze	k1gFnSc1
<g/>
:	:	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
orgán	orgán	k1gInSc1
<g/>
;	;	kIx,
shromáždění	shromáždění	k1gNnSc1
nejvyšších	vysoký	k2eAgMnPc2d3
představitelů	představitel	k1gMnPc2
států	stát	k1gInPc2
(	(	kIx(
<g/>
nepravidelné	pravidelný	k2eNgFnPc4d1
<g/>
)	)	kIx)
</s>
<s>
Summit	summit	k1gInSc1
</s>
<s>
Ministerská	ministerský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
;	;	kIx,
ekonomické	ekonomický	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
(	(	kIx(
<g/>
jednou	jednou	k6eAd1
ročně	ročně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Stálá	stálý	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Fórum	fórum	k1gNnSc1
pro	pro	k7c4
bezpečnostní	bezpečnostní	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
</s>
<s>
Instituce	instituce	k1gFnSc1
</s>
<s>
Výkonný	výkonný	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
<g/>
:	:	kIx,
řídí	řídit	k5eAaImIp3nP
OBSE	OBSE	kA
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nezasedá	zasedat	k5eNaImIp3nS
Následná	následný	k2eAgFnSc1d1
schůze	schůze	k1gFnSc1
nebo	nebo	k8xC
Summit	summit	k1gInSc1
<g/>
;	;	kIx,
pochází	pocházet	k5eAaImIp3nS
vždy	vždy	k6eAd1
z	z	k7c2
předsednické	předsednický	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
Generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
(	(	kIx(
<g/>
sídlo	sídlo	k1gNnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Sekretariát	sekretariát	k1gInSc1
(	(	kIx(
<g/>
sídlo	sídlo	k1gNnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
demokratické	demokratický	k2eAgFnPc4d1
instituce	instituce	k1gFnPc4
a	a	k8xC
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
(	(	kIx(
<g/>
sídlo	sídlo	k1gNnSc1
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Vysoký	vysoký	k2eAgMnSc1d1
komisař	komisař	k1gMnSc1
pro	pro	k7c4
národnostní	národnostní	k2eAgFnPc4d1
menšiny	menšina	k1gFnPc4
(	(	kIx(
<g/>
sídlo	sídlo	k1gNnSc4
v	v	k7c6
Haagu	Haag	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
OBSE	OBSE	kA
uskutečňuje	uskutečňovat	k5eAaImIp3nS
mise	mise	k1gFnPc4
v	v	k7c6
krizových	krizový	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
Makedonie	Makedonie	k1gFnSc1
<g/>
,	,	kIx,
Kosovo	Kosův	k2eAgNnSc1d1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
<g/>
)	)	kIx)
s	s	k7c7
cílem	cíl	k1gInSc7
pokoušet	pokoušet	k5eAaImF
se	se	k3xPyFc4
o	o	k7c6
nastolení	nastolení	k1gNnSc6
demokratických	demokratický	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
Konference	konference	k1gFnSc1
o	o	k7c6
bezpečnosti	bezpečnost	k1gFnSc6
a	a	k8xC
spolupráci	spolupráce	k1gFnSc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Konference	konference	k1gFnSc1
o	o	k7c6
bezpečnosti	bezpečnost	k1gFnSc6
a	a	k8xC
spolupráci	spolupráce	k1gFnSc6
(	(	kIx(
<g/>
KBSE	KBSE	kA
<g/>
)	)	kIx)
v	v	k7c6
Evropě	Evropa	k1gFnSc6
byla	být	k5eAaImAgFnS
tvořena	tvořit	k5eAaImNgFnS
systémem	systém	k1gInSc7
mezinárodních	mezinárodní	k2eAgNnPc2d1
jednání	jednání	k1gNnPc2
a	a	k8xC
smluv	smlouva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
cíl	cíl	k1gInSc4
zajistit	zajistit	k5eAaPmF
mír	mír	k1gInSc4
a	a	k8xC
prohloubit	prohloubit	k5eAaPmF
spolupráci	spolupráce	k1gFnSc4
mezi	mezi	k7c7
evropskými	evropský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1966	#num#	k4
státy	stát	k1gInPc7
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
iniciovaly	iniciovat	k5eAaBmAgFnP
úsilí	úsilí	k1gNnSc4
o	o	k7c4
svolání	svolání	k1gNnSc4
KBSE	KBSE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
ze	z	k7c2
série	série	k1gFnSc2
jednání	jednání	k1gNnSc2
byla	být	k5eAaImAgFnS
přípravná	přípravný	k2eAgFnSc1d1
konference	konference	k1gFnSc1
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
v	v	k7c6
listopadu	listopad	k1gInSc6
1972	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
účastnili	účastnit	k5eAaImAgMnP
diplomatičtí	diplomatický	k2eAgMnPc1d1
zastupitelé	zastupitel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následná	následný	k2eAgFnSc1d1
konference	konference	k1gFnSc1
ministrů	ministr	k1gMnPc2
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
byla	být	k5eAaImAgFnS
uskutečněna	uskutečněn	k2eAgFnSc1d1
v	v	k7c6
červenci	červenec	k1gInSc6
1973	#num#	k4
a	a	k8xC
odstartovala	odstartovat	k5eAaPmAgFnS
vlastní	vlastní	k2eAgInSc4d1
proces	proces	k1gInSc4
jednání	jednání	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
stanovena	stanoven	k2eAgNnPc1d1
pevná	pevný	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
pro	pro	k7c4
proces	proces	k1gInSc4
KBSE	KBSE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
září	září	k1gNnSc2
1973	#num#	k4
do	do	k7c2
června	červen	k1gInSc2
1975	#num#	k4
následovala	následovat	k5eAaImAgFnS
konference	konference	k1gFnSc1
expertů	expert	k1gMnPc2
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
do	do	k7c2
července	červenec	k1gInSc2
1975	#num#	k4
probíhala	probíhat	k5eAaImAgFnS
jednání	jednání	k1gNnSc4
o	o	k7c6
podobě	podoba	k1gFnSc6
Závěrečného	závěrečný	k2eAgInSc2d1
aktu	akt	k1gInSc2
Konference	konference	k1gFnSc1
o	o	k7c6
bezpečnosti	bezpečnost	k1gFnSc6
a	a	k8xC
spolupráci	spolupráce	k1gFnSc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
dokument	dokument	k1gInSc1
byl	být	k5eAaImAgInS
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1975	#num#	k4
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
podepsán	podepsat	k5eAaPmNgInS
nejvyššími	vysoký	k2eAgMnPc7d3
představiteli	představitel	k1gMnPc7
33	#num#	k4
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Kanady	Kanada	k1gFnSc2
a	a	k8xC
USA	USA	kA
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
skončila	skončit	k5eAaPmAgFnS
nejdůležitější	důležitý	k2eAgFnPc4d3
fáze	fáze	k1gFnPc4
procesu	proces	k1gInSc2
KBSE	KBSE	kA
<g/>
.	.	kIx.
</s>
<s>
Závěrečný	závěrečný	k2eAgInSc1d1
akt	akt	k1gInSc1
KBSE	KBSE	kA
byl	být	k5eAaImAgInS
složen	složit	k5eAaPmNgInS
z	z	k7c2
pěti	pět	k4xCc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
otázky	otázka	k1gFnPc1
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
spolupráce	spolupráce	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
vědy	věda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
</s>
<s>
bezpečnost	bezpečnost	k1gFnSc1
a	a	k8xC
spolupráce	spolupráce	k1gFnSc1
států	stát	k1gInPc2
při	při	k7c6
Středozemním	středozemní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
</s>
<s>
spolupráce	spolupráce	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
humanitární	humanitární	k2eAgFnSc6d1
ad	ad	k7c4
<g/>
.	.	kIx.
</s>
<s>
pokračování	pokračování	k1gNnSc1
procesu	proces	k1gInSc2
KBSE	KBSE	kA
<g/>
,	,	kIx,
vč.	vč.	k?
dohody	dohoda	k1gFnPc1
o	o	k7c4
uskutečnění	uskutečnění	k1gNnSc4
Bělehradské	bělehradský	k2eAgFnSc2d1
schůzky	schůzka	k1gFnSc2
účastníků	účastník	k1gMnPc2
KBSE	KBSE	kA
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
</s>
<s>
Proces	proces	k1gInSc1
KBSE	KBSE	kA
pokračoval	pokračovat	k5eAaImAgInS
následnými	následný	k2eAgFnPc7d1
schůzkami	schůzka	k1gFnPc7
v	v	k7c6
Bělehradě	Bělehrad	k1gInSc6
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1977	#num#	k4
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Madridu	Madrid	k1gInSc6
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1980	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1986	#num#	k4
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Celý	celý	k2eAgInSc1d1
systém	systém	k1gInSc1
konferencí	konference	k1gFnPc2
KBSE	KBSE	kA
a	a	k8xC
výsledek	výsledek	k1gInSc4
procesu	proces	k1gInSc2
jednání	jednání	k1gNnSc3
znamenal	znamenat	k5eAaImAgInS
posílení	posílení	k1gNnSc4
stability	stabilita	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
rozdělené	rozdělený	k2eAgInPc1d1
geopoliticky	geopoliticky	k6eAd1
na	na	k7c4
západní	západní	k2eAgInSc4d1
a	a	k8xC
východní	východní	k2eAgInSc4d1
blok	blok	k1gInSc4
<g/>
,	,	kIx,
KBSE	KBSE	kA
jako	jako	k8xS,k8xC
taková	takový	k3xDgFnSc1
znamenala	znamenat	k5eAaImAgFnS
významný	významný	k2eAgInSc4d1
mezník	mezník	k1gInSc4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
Studené	Studené	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
nejen	nejen	k6eAd1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Středozemním	středozemní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc1
KBSE	KBSE	kA
zasáhl	zasáhnout	k5eAaPmAgInS
i	i	k9
do	do	k7c2
vnitropolitické	vnitropolitický	k2eAgFnSc2d1
situace	situace	k1gFnSc2
zúčastněných	zúčastněný	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
Československu	Československo	k1gNnSc6
byla	být	k5eAaImAgFnS
produktem	produkt	k1gInSc7
odezvy	odezva	k1gFnSc2
KBSE	KBSE	kA
např.	např.	kA
Charta	charta	k1gFnSc1
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
zmínit	zmínit	k5eAaPmF
(	(	kIx(
<g/>
a	a	k8xC
vyzdvihnout	vyzdvihnout	k5eAaPmF
tak	tak	k9
jejich	jejich	k3xOp3gInSc4
význam	význam	k1gInSc4
<g/>
)	)	kIx)
množství	množství	k1gNnSc1
jednotlivců	jednotlivec	k1gMnPc2
i	i	k8xC
nevládních	vládní	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
si	se	k3xPyFc3
vytkli	vytknout	k5eAaPmAgMnP
za	za	k7c4
cíl	cíl	k1gInSc4
sledovat	sledovat	k5eAaImF
dodržování	dodržování	k1gNnSc4
Helsinských	helsinský	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
signatářských	signatářský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
nezřídka	nezřídka	k6eAd1
za	za	k7c4
cenu	cena	k1gFnSc4
vysokých	vysoký	k2eAgFnPc2d1
osobních	osobní	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
např.	např.	kA
Český	český	k2eAgInSc1d1
helsinský	helsinský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
kolapsem	kolaps	k1gInSc7
komunistických	komunistický	k2eAgInPc2d1
režimů	režim	k1gInPc2
ve	v	k7c6
státech	stát	k1gInPc6
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
byla	být	k5eAaImAgFnS
patrná	patrný	k2eAgFnSc1d1
potřeba	potřeba	k1gFnSc1
změn	změna	k1gFnPc2
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
KBSE	KBSE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
pařížská	pařížský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
započala	započnout	k5eAaPmAgFnS
s	s	k7c7
proměnou	proměna	k1gFnSc7
KBSE	KBSE	kA
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
ukončena	ukončen	k2eAgFnSc1d1
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
transformací	transformace	k1gFnSc7
KBSE	KBSE	kA
na	na	k7c6
Organizaci	organizace	k1gFnSc6
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
OBSE	OBSE	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
v	v	k7c6
OBSE	OBSE	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
je	být	k5eAaImIp3nS
–	–	k?
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
další	další	k2eAgInPc1d1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
–	–	k?
zastoupena	zastoupit	k5eAaPmNgFnS
v	v	k7c6
Parlamentním	parlamentní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
OBSE	OBSE	kA
poslanci	poslanec	k1gMnPc7
a	a	k8xC
senátory	senátor	k1gMnPc4
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současnou	současný	k2eAgFnSc7d1
vedoucí	vedoucí	k1gFnSc7
české	český	k2eAgFnSc2d1
delegace	delegace	k1gFnSc2
je	být	k5eAaImIp3nS
poslankyně	poslankyně	k1gFnSc1
Ivana	Ivana	k1gFnSc1
Dobešová	Dobešová	k1gFnSc1
(	(	kIx(
<g/>
nestraník	nestraník	k1gMnSc1
za	za	k7c4
ANO	ano	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
červenci	červenec	k1gInSc6
2016	#num#	k4
prosadila	prosadit	k5eAaPmAgFnS
rezoluci	rezoluce	k1gFnSc4
<g/>
,	,	kIx,
týkající	týkající	k2eAgNnSc4d1
se	se	k3xPyFc4
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
výzev	výzva	k1gFnPc2
<g/>
,	,	kIx,
spojených	spojený	k2eAgInPc2d1
s	s	k7c7
migrací	migrace	k1gFnSc7
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
první	první	k4xOgFnSc4
rezoluci	rezoluce	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
OBSE	OBSE	kA
za	za	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ivana	Ivana	k1gFnSc1
Dobešová	Dobešová	k1gFnSc1
zároveň	zároveň	k6eAd1
v	v	k7c6
létě	léto	k1gNnSc6
2016	#num#	k4
obhájila	obhájit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
mandát	mandát	k1gInSc4
místopředsedkyně	místopředsedkyně	k1gFnSc2
Hlavního	hlavní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
demokracii	demokracie	k1gFnSc4
<g/>
,	,	kIx,
lidská	lidský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
a	a	k8xC
humanitární	humanitární	k2eAgFnPc1d1
otázky	otázka	k1gFnPc1
PS	PS	kA
OBSE	OBSE	kA
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Komise	komise	k1gFnSc1
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Známá	známá	k1gFnSc1
také	také	k9
jako	jako	k9
U.	U.	kA
<g/>
S.	S.	kA
Helsinki	Helsinki	k1gNnSc1
Commission	Commission	k1gInSc1
je	být	k5eAaImIp3nS
nezávislá	závislý	k2eNgFnSc1d1
agentura	agentura	k1gFnSc1
Federální	federální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gMnPc7
členy	člen	k1gMnPc7
je	být	k5eAaImIp3nS
devět	devět	k4xCc1
senátorů	senátor	k1gMnPc2
<g/>
,	,	kIx,
devět	devět	k4xCc1
členů	člen	k1gMnPc2
Sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
a	a	k8xC
po	po	k7c6
jednom	jeden	k4xCgInSc6
členu	člen	k1gInSc6
z	z	k7c2
ministerstev	ministerstvo	k1gNnPc2
Zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
Obrany	obrana	k1gFnSc2
a	a	k8xC
Obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
sleduje	sledovat	k5eAaImIp3nS
dodržování	dodržování	k1gNnSc4
Helsinských	helsinský	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
v	v	k7c6
57	#num#	k4
zemích	zem	k1gFnPc6
a	a	k8xC
dbá	dbát	k5eAaImIp3nS
o	o	k7c4
zvyšování	zvyšování	k1gNnSc4
mezinárodní	mezinárodní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
podporou	podpora	k1gFnSc7
dodržování	dodržování	k1gNnSc4
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
demokracie	demokracie	k1gFnSc2
a	a	k8xC
vzájemnou	vzájemný	k2eAgFnSc7d1
spoluprací	spolupráce	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
vojenské	vojenský	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zúčastněné	zúčastněný	k2eAgInPc1d1
státy	stát	k1gInPc1
OBSE	OBSE	kA
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Monako	Monako	k1gNnSc1
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
Vatikán	Vatikán	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Organizace	organizace	k1gFnSc2
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.osce.org	www.osce.org	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Evropa	Evropa	k1gFnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711307	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2149347-9	2149347-9	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2065	#num#	k4
9556	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
95037357	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
141482944	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
95037357	#num#	k4
</s>
