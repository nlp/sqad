<p>
<s>
Andreas	Andreas	k1gMnSc1	Andreas
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
"	"	kIx"	"
<g/>
Niki	Niki	k1gNnSc1	Niki
<g/>
"	"	kIx"	"
Lauda	Lauda	k1gFnSc1	Lauda
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1949	[number]	k4	1949
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rakouský	rakouský	k2eAgInSc1d1	rakouský
pilot	pilot	k1gInSc1	pilot
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
a	a	k8xC	a
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
spolu-komentátor	spoluomentátor	k1gMnSc1	spolu-komentátor
závodů	závod	k1gInPc2	závod
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
televizi	televize	k1gFnSc6	televize
RTL	RTL	kA	RTL
a	a	k8xC	a
pilot	pilot	k1gMnSc1	pilot
a	a	k8xC	a
majitel	majitel	k1gMnSc1	majitel
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Lauda	Laudo	k1gNnSc2	Laudo
Air	Air	k1gFnSc2	Air
a	a	k8xC	a
Niki	Nik	k1gFnSc2	Nik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nehoda	nehoda	k1gFnSc1	nehoda
na	na	k7c6	na
Nürburgringu	Nürburgring	k1gInSc6	Nürburgring
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1976	[number]	k4	1976
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
trati	trať	k1gFnSc6	trať
Nürburgring	Nürburgring	k1gInSc1	Nürburgring
měl	mít	k5eAaImAgInS	mít
Niki	Nike	k1gFnSc4	Nike
Lauda	Laudo	k1gNnSc2	Laudo
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
obhajobě	obhajoba	k1gFnSc3	obhajoba
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
závodě	závod	k1gInSc6	závod
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Lauda	Lauda	k1gMnSc1	Lauda
se	se	k3xPyFc4	se
řítil	řítit	k5eAaImAgMnS	řítit
po	po	k7c6	po
trati	trať	k1gFnSc6	trať
svým	svůj	k3xOyFgInSc7	svůj
vozem	vůz	k1gInSc7	vůz
Ferrari	Ferrari	k1gMnSc2	Ferrari
rychlostí	rychlost	k1gFnSc7	rychlost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
zvanému	zvaný	k2eAgInSc3d1	zvaný
Bergwerk	Bergwerk	k1gInSc4	Bergwerk
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gInSc1	jeho
vůz	vůz	k1gInSc1	vůz
neovladatelně	ovladatelně	k6eNd1	ovladatelně
zatočil	zatočit	k5eAaPmAgInS	zatočit
doprava	doprava	k6eAd1	doprava
a	a	k8xC	a
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
plotu	plot	k1gInSc2	plot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gNnSc4	on
následně	následně	k6eAd1	následně
vymrštil	vymrštit	k5eAaPmAgMnS	vymrštit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vraku	vrak	k1gInSc2	vrak
poté	poté	k6eAd1	poté
narazil	narazit	k5eAaPmAgMnS	narazit
pilot	pilot	k1gMnSc1	pilot
Brett	Brett	k1gMnSc1	Brett
Lunger	Lunger	k1gMnSc1	Lunger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
Laudův	Laudův	k2eAgInSc4d1	Laudův
vůz	vůz	k1gInSc4	vůz
začal	začít	k5eAaPmAgInS	začít
hořet	hořet	k5eAaImF	hořet
následkem	následkem	k7c2	následkem
exploze	exploze	k1gFnSc2	exploze
palivové	palivový	k2eAgFnSc2d1	palivová
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Lauda	Lauda	k1gFnSc1	Lauda
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
Lungerem	Lungero	k1gNnSc7	Lungero
ztratil	ztratit	k5eAaPmAgInS	ztratit
přilbu	přilba	k1gFnSc4	přilba
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstal	zůstat	k5eAaPmAgMnS	zůstat
uprostřed	uprostřed	k7c2	uprostřed
plamenů	plamen	k1gInPc2	plamen
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vyprostit	vyprostit	k5eAaPmF	vyprostit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
vážném	vážný	k2eAgInSc6d1	vážný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
poškozené	poškozený	k2eAgFnPc4d1	poškozená
plíce	plíce	k1gFnPc4	plíce
<g/>
,	,	kIx,	,
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
i	i	k8xC	i
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
měl	mít	k5eAaImAgInS	mít
těžce	těžce	k6eAd1	těžce
popálené	popálený	k2eAgFnPc4d1	popálená
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
podstoupit	podstoupit	k5eAaPmF	podstoupit
plastickou	plastický	k2eAgFnSc4d1	plastická
operaci	operace	k1gFnSc4	operace
obličeje	obličej	k1gInSc2	obličej
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
následky	následek	k1gInPc7	následek
nehody	nehoda	k1gFnSc2	nehoda
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgMnS	potýkat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c4	o
42	[number]	k4	42
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Niki	Niki	k1gNnSc1	Niki
Lauda	Laudo	k1gNnSc2	Laudo
opět	opět	k6eAd1	opět
usadil	usadit	k5eAaPmAgInS	usadit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
monopostu	monopost	k1gInSc2	monopost
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgMnS	bojovat
o	o	k7c4	o
titul	titul	k1gInSc4	titul
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezony	sezona	k1gFnSc2	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
nakonec	nakonec	k6eAd1	nakonec
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
neuhájil	uhájit	k5eNaPmAgMnS	uhájit
před	před	k7c7	před
Jamesem	James	k1gMnSc7	James
Huntem	hunt	k1gInSc7	hunt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
opět	opět	k6eAd1	opět
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ukončil	ukončit	k5eAaPmAgMnS	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
získal	získat	k5eAaPmAgInS	získat
potřetí	potřetí	k4xO	potřetí
titul	titul	k1gInSc1	titul
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
létání	létání	k1gNnSc4	létání
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pilotem	pilot	k1gMnSc7	pilot
a	a	k8xC	a
majitelem	majitel	k1gMnSc7	majitel
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Lauda	Lauda	k1gFnSc1	Lauda
Air	Air	k1gFnSc1	Air
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
později	pozdě	k6eAd2	pozdě
prodal	prodat	k5eAaPmAgMnS	prodat
Austrian	Austrian	k1gMnSc1	Austrian
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
založil	založit	k5eAaPmAgMnS	založit
další	další	k2eAgFnSc4d1	další
leteckou	letecký	k2eAgFnSc4d1	letecká
společnost	společnost	k1gFnSc4	společnost
Niki	Nik	k1gFnSc2	Nik
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
i	i	k8xC	i
dopravní	dopravní	k2eAgNnPc1d1	dopravní
letadla	letadlo	k1gNnPc1	letadlo
Airbus	airbus	k1gInSc1	airbus
A	a	k9	a
<g/>
330	[number]	k4	330
<g/>
.	.	kIx.	.
</s>
<s>
Zbankrotovala	zbankrotovat	k5eAaPmAgFnS	zbankrotovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
nespadala	spadat	k5eNaImAgFnS	spadat
pod	pod	k7c4	pod
Nikyho	Niky	k1gMnSc4	Niky
Laudu	Lauda	k1gMnSc4	Lauda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pod	pod	k7c4	pod
zkrachovalou	zkrachovalý	k2eAgFnSc4d1	zkrachovalá
společnost	společnost	k1gFnSc4	společnost
Air	Air	k1gFnSc2	Air
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
Lauda	Lauda	k1gMnSc1	Lauda
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
koupit	koupit	k5eAaPmF	koupit
aerolinky	aerolinka	k1gFnPc4	aerolinka
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
transplantaci	transplantace	k1gFnSc4	transplantace
plic	plíce	k1gFnPc2	plíce
poškozených	poškozený	k2eAgFnPc2d1	poškozená
následkem	následkem	k7c2	následkem
nehody	nehoda	k1gFnSc2	nehoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
po	po	k7c6	po
vleklých	vleklý	k2eAgInPc6d1	vleklý
zdravotních	zdravotní	k2eAgInPc6d1	zdravotní
problémech	problém	k1gInPc6	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
Americko	Americko	k1gNnSc1	Americko
<g/>
/	/	kIx~	/
<g/>
německo	německo	k1gNnSc1	německo
<g/>
/	/	kIx~	/
<g/>
britský	britský	k2eAgInSc4d1	britský
film	film	k1gInSc4	film
Rivalové	rival	k1gMnPc1	rival
(	(	kIx(	(
<g/>
Rush	Rush	k1gMnSc1	Rush
<g/>
)	)	kIx)	)
režiséra	režisér	k1gMnSc2	režisér
Rona	Ron	k1gMnSc2	Ron
Howarda	Howard	k1gMnSc2	Howard
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
soupeření	soupeření	k1gNnSc6	soupeření
Jamese	Jamese	k1gFnSc2	Jamese
Hunta	Hunt	k1gMnSc2	Hunt
a	a	k8xC	a
Niki	Nik	k1gFnPc4	Nik
Laudy	laudy	k1gFnPc4	laudy
<g/>
.	.	kIx.	.
</s>
<s>
Niki	Nik	k1gMnPc1	Nik
Laudu	Laud	k1gInSc2	Laud
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
německý	německý	k2eAgMnSc1d1	německý
herec	herec	k1gMnSc1	herec
Daniel	Daniel	k1gMnSc1	Daniel
Brühl	Brühl	k1gMnSc1	Brühl
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc1	Jamese
Hunta	Hunt	k1gInSc2	Hunt
Chris	Chris	k1gFnSc2	Chris
Hemsworth	Hemswortha	k1gFnPc2	Hemswortha
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hráli	hrát	k5eAaImAgMnP	hrát
Alexandra	Alexandr	k1gMnSc4	Alexandr
Maria	Mario	k1gMnSc4	Mario
Lara	Larus	k1gMnSc4	Larus
<g/>
,	,	kIx,	,
Olivia	Olivius	k1gMnSc4	Olivius
Wilde	Wild	k1gInSc5	Wild
a	a	k8xC	a
Natalie	Natalie	k1gFnSc2	Natalie
Dormer	Dormra	k1gFnPc2	Dormra
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Hans	Hans	k1gMnSc1	Hans
Zimmer	Zimmer	k1gMnSc1	Zimmer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lauda	Lauda	k1gMnSc1	Lauda
Air	Air	k1gMnSc1	Air
</s>
</p>
<p>
<s>
Niki	Niki	k6eAd1	Niki
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Niki	Nik	k1gFnSc2	Nik
Lauda	Laudo	k1gNnSc2	Laudo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Niki	Nik	k1gFnSc2	Nik
Lauda	Laudo	k1gNnSc2	Laudo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c4	na
austria-forum	austriaorum	k1gInSc4	austria-forum
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
