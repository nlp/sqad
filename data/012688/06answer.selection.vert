<s>
Lauda	Lauda	k1gFnSc1	Lauda
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
Lungerem	Lungero	k1gNnSc7	Lungero
ztratil	ztratit	k5eAaPmAgInS	ztratit
přilbu	přilba	k1gFnSc4	přilba
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstal	zůstat	k5eAaPmAgMnS	zůstat
uprostřed	uprostřed	k7c2	uprostřed
plamenů	plamen	k1gInPc2	plamen
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vyprostit	vyprostit	k5eAaPmF	vyprostit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
vážném	vážný	k2eAgInSc6d1	vážný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
