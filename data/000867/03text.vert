<s>
Rutin	rutina	k1gFnPc2	rutina
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
bioflavonoidní	bioflavonoidní	k2eAgInPc4d1	bioflavonoidní
glykosidy	glykosid	k1gInPc4	glykosid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
látka	látka	k1gFnSc1	látka
světle	světle	k6eAd1	světle
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Molární	molární	k2eAgFnSc1d1	molární
hmotnost	hmotnost	k1gFnSc1	hmotnost
rutinu	rutina	k1gFnSc4	rutina
je	být	k5eAaImIp3nS	být
610,52	[number]	k4	610,52
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
aglykon	aglykon	k1gInSc1	aglykon
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
quercetin	quercetin	k2eAgInSc1d1	quercetin
a	a	k8xC	a
cukernatá	cukernatý	k2eAgFnSc1d1	cukernatá
část	část	k1gFnSc1	část
rutinosa	rutinosa	k1gFnSc1	rutinosa
(	(	kIx(	(
<g/>
β	β	k?	β
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
→	→	k?	→
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
-β	-β	k?	-β
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
největší	veliký	k2eAgInPc1d3	veliký
přínosy	přínos	k1gInPc1	přínos
patří	patřit	k5eAaImIp3nP	patřit
především	především	k9	především
schopnost	schopnost	k1gFnSc4	schopnost
léčit	léčit	k5eAaImF	léčit
křehkost	křehkost	k1gFnSc4	křehkost
krevních	krevní	k2eAgFnPc2d1	krevní
kapilár	kapilára	k1gFnPc2	kapilára
a	a	k8xC	a
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
pružnost	pružnost	k1gFnSc4	pružnost
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
LDL	LDL	kA	LDL
cholesterol	cholesterol	k1gInSc1	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
jeho	jeho	k3xOp3gFnSc1	jeho
antioxidační	antioxidační	k2eAgFnSc1d1	antioxidační
aktivita	aktivita	k1gFnSc1	aktivita
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgInPc4d1	související
antikarcinogenní	antikarcinogenní	k2eAgInSc4d1	antikarcinogenní
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
zhášet	zhášet	k5eAaImF	zhášet
volné	volný	k2eAgInPc4d1	volný
radikály	radikál	k1gInPc4	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
účinek	účinek	k1gInSc4	účinek
vitaminu	vitamin	k1gInSc2	vitamin
C.	C.	kA	C.
Rutin	rutina	k1gFnPc2	rutina
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
v	v	k7c6	v
listech	list	k1gInPc6	list
routy	routa	k1gFnSc2	routa
vonné	vonný	k2eAgFnSc2d1	vonná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
rostlinné	rostlinný	k2eAgFnSc6d1	rostlinná
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
zdroje	zdroj	k1gInPc4	zdroj
rutinu	rutina	k1gFnSc4	rutina
patří	patřit	k5eAaImIp3nS	patřit
pohanka	pohanka	k1gFnSc1	pohanka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
routa	routa	k1gFnSc1	routa
vonná	vonný	k2eAgFnSc1d1	vonná
a	a	k8xC	a
jerlín	jerlín	k1gInSc1	jerlín
japonský	japonský	k2eAgInSc1d1	japonský
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rutin	rutina	k1gFnPc2	rutina
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
chřestu	chřest	k1gInSc6	chřest
<g/>
,	,	kIx,	,
ve	v	k7c6	v
slupkách	slupka	k1gFnPc6	slupka
rajčat	rajče	k1gNnPc2	rajče
<g/>
,	,	kIx,	,
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
citrusových	citrusový	k2eAgInPc2d1	citrusový
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
v	v	k7c6	v
jablečné	jablečný	k2eAgFnSc6d1	jablečná
slupce	slupka	k1gFnSc6	slupka
<g/>
,	,	kIx,	,
v	v	k7c6	v
broskvích	broskev	k1gFnPc6	broskev
<g/>
,	,	kIx,	,
nektarinkách	nektarinka	k1gFnPc6	nektarinka
<g/>
,	,	kIx,	,
lesních	lesní	k2eAgInPc6d1	lesní
plodech	plod	k1gInPc6	plod
<g/>
,	,	kIx,	,
kiwi	kiwi	k1gNnSc6	kiwi
<g/>
,	,	kIx,	,
banánech	banán	k1gInPc6	banán
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
výluhu	výluh	k1gInSc6	výluh
zeleného	zelený	k2eAgInSc2d1	zelený
a	a	k8xC	a
černého	černý	k2eAgInSc2d1	černý
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
pohanka	pohanka	k1gFnSc1	pohanka
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zdrojem	zdroj	k1gInSc7	zdroj
rutinu	rutina	k1gFnSc4	rutina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
množství	množství	k1gNnSc1	množství
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
odrůdě	odrůda	k1gFnSc6	odrůda
pohanky	pohanka	k1gFnSc2	pohanka
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
přijatého	přijatý	k2eAgNnSc2d1	přijaté
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
rutinu	rutina	k1gFnSc4	rutina
v	v	k7c6	v
pohance	pohanka	k1gFnSc6	pohanka
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
rostliny	rostlina	k1gFnSc2	rostlina
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
rutinu	rutina	k1gFnSc4	rutina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
květech	květ	k1gInPc6	květ
(	(	kIx(	(
<g/>
až	až	k9	až
400	[number]	k4	400
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
g	g	kA	g
sušiny	sušina	k1gFnSc2	sušina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
stonek	stonek	k1gInSc1	stonek
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
rutinu	rutina	k1gFnSc4	rutina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kořenu	kořen	k1gInSc6	kořen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
loupaných	loupaný	k2eAgNnPc6d1	loupané
semenech	semeno	k1gNnPc6	semeno
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kroupy	kroupa	k1gFnSc2	kroupa
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
20	[number]	k4	20
mg	mg	kA	mg
rutinu	rutina	k1gFnSc4	rutina
ve	v	k7c6	v
100	[number]	k4	100
g	g	kA	g
sušiny	sušina	k1gFnSc2	sušina
<g/>
.	.	kIx.	.
</s>
<s>
Tepelným	tepelný	k2eAgNnSc7d1	tepelné
zpracováním	zpracování	k1gNnSc7	zpracování
pohanky	pohanka	k1gFnSc2	pohanka
však	však	k9	však
obsah	obsah	k1gInSc1	obsah
rutinu	rutina	k1gFnSc4	rutina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
CROZIER	CROZIER	kA	CROZIER
A.	A.	kA	A.
<g/>
;	;	kIx,	;
CLIFFORD	CLIFFORD	kA	CLIFFORD
M.	M.	kA	M.
<g/>
N.	N.	kA	N.
<g/>
;	;	kIx,	;
ASHIHARA	ASHIHARA	kA	ASHIHARA
H.	H.	kA	H.
Plant	planta	k1gFnPc2	planta
secondary	secondara	k1gFnSc2	secondara
metabolites	metabolitesa	k1gFnPc2	metabolitesa
<g/>
.	.	kIx.	.
</s>
<s>
Blackwell	Blackwell	k1gInSc1	Blackwell
publishing	publishing	k1gInSc1	publishing
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
384	[number]	k4	384
p.	p.	k?	p.
ISBN	ISBN	kA	ISBN
1-4051-2509-8	[number]	k4	1-4051-2509-8
PARK	park	k1gInSc1	park
<g/>
,	,	kIx,	,
B.J.	B.J.	k1gFnSc1	B.J.
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
:	:	kIx,	:
Comparison	Comparison	k1gNnSc1	Comparison
in	in	k?	in
Rutin	rutina	k1gFnPc2	rutina
Content	Content	k1gMnSc1	Content
in	in	k?	in
Seed	Seed	k1gInSc1	Seed
and	and	k?	and
Plant	planta	k1gFnPc2	planta
of	of	k?	of
Tartary	Tartar	k1gInPc1	Tartar
Buckwheat	Buckwheat	k1gInSc1	Buckwheat
(	(	kIx(	(
<g/>
Fagopyrum	Fagopyrum	k1gInSc1	Fagopyrum
tataricum	tataricum	k1gInSc1	tataricum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
9	[number]	k4	9
<g/>
th	th	k?	th
International	International	k1gMnSc1	International
Symposium	symposium	k1gNnSc1	symposium
on	on	k3xPp3gInSc1	on
Buckwheat	Buckwheat	k2eAgMnSc1d1	Buckwheat
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gNnSc1	Prague
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
p.	p.	k?	p.
626-629	[number]	k4	626-629
JIANG	JIANG	kA	JIANG
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Rutin	rutina	k1gFnPc2	rutina
and	and	k?	and
Flavonoid	flavonoid	k1gInSc1	flavonoid
contents	contents	k1gInSc1	contents
in	in	k?	in
three	threat	k5eAaPmIp3nS	threat
buckwheat	buckwheat	k5eAaPmF	buckwheat
species	species	k1gFnPc4	species
Fagopyrum	Fagopyrum	k1gInSc4	Fagopyrum
esculentum	esculentum	k1gNnSc1	esculentum
<g/>
,	,	kIx,	,
F.	F.	kA	F.
tataricum	tataricum	k1gInSc1	tataricum
<g/>
,	,	kIx,	,
and	and	k?	and
F.	F.	kA	F.
homotropicum	homotropicum	k1gInSc1	homotropicum
and	and	k?	and
their	their	k1gInSc1	their
protective	protectiv	k1gInSc5	protectiv
effects	effects	k1gInSc4	effects
against	against	k1gInSc1	against
lipid	lipid	k1gInSc1	lipid
peroxidation	peroxidation	k1gInSc1	peroxidation
<g/>
.	.	kIx.	.
</s>
<s>
Food	Food	k1gMnSc1	Food
Research	Research	k1gMnSc1	Research
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
,	,	kIx,	,
356-364	[number]	k4	356-364
</s>
