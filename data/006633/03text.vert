<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
liberálně	liberálně	k6eAd1	liberálně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
středopravicová	středopravicový	k2eAgFnSc1d1	středopravicová
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
výrazným	výrazný	k2eAgInSc7d1	výrazný
politickým	politický	k2eAgInSc7d1	politický
subjektem	subjekt	k1gInSc7	subjekt
a	a	k8xC	a
také	také	k9	také
součástí	součást	k1gFnSc7	součást
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
strany	strana	k1gFnSc2	strana
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
základní	základní	k2eAgInPc4d1	základní
principy	princip	k1gInPc4	princip
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
strana	strana	k1gFnSc1	strana
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
Odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
,	,	kIx,	,
Prosperita	prosperita	k1gFnSc1	prosperita
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgFnSc2d1	doplněná
rokem	rok	k1gInSc7	rok
vzniku	vznik	k1gInSc2	vznik
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
je	být	k5eAaImIp3nS	být
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
tradic	tradice	k1gFnPc2	tradice
křesťansko-židovské	křesťansko-židovský	k2eAgFnSc2d1	křesťansko-židovská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
charakteristikou	charakteristika	k1gFnSc7	charakteristika
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc4	její
proevropský	proevropský	k2eAgInSc4d1	proevropský
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
populismu	populismus	k1gInSc3	populismus
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
klade	klást	k5eAaImIp3nS	klást
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
udržet	udržet	k5eAaPmF	udržet
stabilitu	stabilita	k1gFnSc4	stabilita
veřejných	veřejný	k2eAgInPc2d1	veřejný
rozpočtů	rozpočet	k1gInPc2	rozpočet
a	a	k8xC	a
nezvyšovat	zvyšovat	k5eNaImF	zvyšovat
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
předsedou	předseda	k1gMnSc7	předseda
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
je	být	k5eAaImIp3nS	být
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
nové	nový	k2eAgFnSc2d1	nová
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
myšlence	myšlenka	k1gFnSc6	myšlenka
jednal	jednat	k5eAaImAgMnS	jednat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
(	(	kIx(	(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
)	)	kIx)	)
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
senátorem	senátor	k1gMnSc7	senátor
Karlem	Karel	k1gMnSc7	Karel
Schwarzenbergem	Schwarzenberg	k1gMnSc7	Schwarzenberg
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
Zelené	zelené	k1gNnSc4	zelené
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Gazdíkem	Gazdík	k1gInSc7	Gazdík
(	(	kIx(	(
<g/>
STAN	stan	k1gInSc1	stan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgMnS	datovat
k	k	k7c3	k
26	[number]	k4	26
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zaregistrována	zaregistrovat	k5eAaPmNgFnS	zaregistrovat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Ustavující	ustavující	k2eAgInSc1d1	ustavující
sněm	sněm	k1gInSc1	sněm
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
čela	čelo	k1gNnSc2	čelo
byl	být	k5eAaImAgInS	být
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
místopředsedou	místopředseda	k1gMnSc7	místopředseda
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
strana	strana	k1gFnSc1	strana
čítala	čítat	k5eAaImAgFnS	čítat
1900	[number]	k4	1900
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
před	před	k7c7	před
předčasnými	předčasný	k2eAgFnPc7d1	předčasná
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
neuskutečnily	uskutečnit	k5eNaPmAgFnP	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
účastnila	účastnit	k5eAaImAgFnS	účastnit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
získala	získat	k5eAaPmAgFnS	získat
16,7	[number]	k4	16,7
%	%	kIx~	%
platných	platný	k2eAgInPc2d1	platný
odevzdaných	odevzdaný	k2eAgInPc2d1	odevzdaný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
STANem	stan	k1gInSc7	stan
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
podpoře	podpora	k1gFnSc6	podpora
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
navazující	navazující	k2eAgFnSc7d1	navazující
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
trvala	trvat	k5eAaImAgFnS	trvat
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
a	a	k8xC	a
2016	[number]	k4	2016
a	a	k8xC	a
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
a	a	k8xC	a
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
oba	dva	k4xCgInPc1	dva
subjekty	subjekt	k1gInPc1	subjekt
postupovaly	postupovat	k5eAaImAgInP	postupovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ve	v	k7c6	v
společných	společný	k2eAgFnPc6d1	společná
koalicích	koalice	k1gFnPc6	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
STAN	stan	k1gInSc1	stan
domluvily	domluvit	k5eAaPmAgInP	domluvit
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zasedla	zasednout	k5eAaPmAgFnS	zasednout
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
s	s	k7c7	s
ODS	ODS	kA	ODS
a	a	k8xC	a
VV	VV	kA	VV
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
disponovala	disponovat	k5eAaBmAgFnS	disponovat
118	[number]	k4	118
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
200	[number]	k4	200
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
pak	pak	k6eAd1	pak
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
vlády	vláda	k1gFnSc2	vláda
za	za	k7c4	za
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
<g/>
:	:	kIx,	:
V	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
získala	získat	k5eAaPmAgFnS	získat
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
celkem	celek	k1gInSc7	celek
11,99	[number]	k4	11,99
%	%	kIx~	%
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
přepočítání	přepočítání	k1gNnSc6	přepočítání
představovaly	představovat	k5eAaImAgInP	představovat
26	[number]	k4	26
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
sestavily	sestavit	k5eAaPmAgFnP	sestavit
strany	strana	k1gFnPc1	strana
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
ANO	ano	k9	ano
2011	[number]	k4	2011
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
celostátní	celostátní	k2eAgFnSc1d1	celostátní
<g/>
,	,	kIx,	,
krajská	krajský	k2eAgFnSc1d1	krajská
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc1d1	regionální
a	a	k8xC	a
místní	místní	k2eAgFnSc1d1	místní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
úrovni	úroveň	k1gFnSc6	úroveň
strany	strana	k1gFnSc2	strana
funguje	fungovat	k5eAaImIp3nS	fungovat
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
sněm	sněm	k1gInSc1	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
regionální	regionální	k2eAgFnSc2d1	regionální
úrovně	úroveň	k1gFnSc2	úroveň
jsou	být	k5eAaImIp3nP	být
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
revizní	revizní	k2eAgFnPc1d1	revizní
komise	komise	k1gFnPc1	komise
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
samostatné	samostatný	k2eAgNnSc4d1	samostatné
hospodaření	hospodaření	k1gNnSc4	hospodaření
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
úrovních	úroveň	k1gFnPc6	úroveň
je	být	k5eAaImIp3nS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
funguje	fungovat	k5eAaImIp3nS	fungovat
smírčí	smírčí	k2eAgInSc1d1	smírčí
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
celostátní	celostátní	k2eAgFnSc2d1	celostátní
revizní	revizní	k2eAgFnSc2d1	revizní
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
strany	strana	k1gFnSc2	strana
zřízen	zřízen	k2eAgInSc1d1	zřízen
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
předsednictvem	předsednictvo	k1gNnSc7	předsednictvo
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc7	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
poslanci	poslanec	k1gMnPc1	poslanec
a	a	k8xC	a
senátory	senátor	k1gMnPc4	senátor
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
europoslanci	europoslanec	k1gMnPc7	europoslanec
<g/>
,	,	kIx,	,
primátorem	primátor	k1gMnSc7	primátor
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
hejtmany	hejtman	k1gMnPc4	hejtman
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
voleni	volen	k2eAgMnPc1d1	volen
členové	člen	k1gMnPc1	člen
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
na	na	k7c6	na
celostátním	celostátní	k2eAgInSc6d1	celostátní
sněmu	sněm	k1gInSc6	sněm
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
strana	strana	k1gFnSc1	strana
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
seznam	seznam	k1gInSc4	seznam
svých	svůj	k3xOyFgMnPc2	svůj
dárců	dárce	k1gMnPc2	dárce
a	a	k8xC	a
výši	výše	k1gFnSc4	výše
darů	dar	k1gInPc2	dar
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
změnil	změnit	k5eAaPmAgInS	změnit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
platnou	platný	k2eAgFnSc4d1	platná
praxi	praxe	k1gFnSc4	praxe
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
strany	strana	k1gFnPc1	strana
čekaly	čekat	k5eAaImAgFnP	čekat
se	s	k7c7	s
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
na	na	k7c4	na
poslední	poslední	k2eAgInSc4d1	poslední
březnový	březnový	k2eAgInSc4d1	březnový
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jim	on	k3xPp3gMnPc3	on
ukládá	ukládat	k5eAaImIp3nS	ukládat
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
strana	strana	k1gFnSc1	strana
pokročila	pokročit	k5eAaPmAgFnS	pokročit
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
krok	krok	k1gInSc4	krok
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
nejpodrobnějšího	podrobný	k2eAgNnSc2d3	nejpodrobnější
vyúčtování	vyúčtování	k1gNnSc2	vyúčtování
volebních	volební	k2eAgFnPc2d1	volební
kampaní	kampaň	k1gFnPc2	kampaň
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
Senátu	senát	k1gInSc2	senát
i	i	k8xC	i
místních	místní	k2eAgNnPc2d1	místní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zveřejnění	zveřejnění	k1gNnSc1	zveřejnění
veškerých	veškerý	k3xTgInPc2	veškerý
svých	svůj	k3xOyFgInPc2	svůj
příjmů	příjem	k1gInPc2	příjem
a	a	k8xC	a
výdajů	výdaj	k1gInPc2	výdaj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
transparentnosti	transparentnost	k1gFnSc2	transparentnost
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
ČR	ČR	kA	ČR
starší	starší	k1gMnSc1	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
k	k	k7c3	k
právním	právní	k2eAgInPc3d1	právní
úkonům	úkon	k1gInPc3	úkon
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
jiné	jiný	k2eAgFnSc2d1	jiná
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
nebo	nebo	k8xC	nebo
politického	politický	k2eAgNnSc2d1	politické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podání	podání	k1gNnSc6	podání
písemné	písemný	k2eAgFnSc2d1	písemná
přihlášky	přihláška	k1gFnSc2	přihláška
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
člena	člen	k1gMnSc2	člen
místní	místní	k2eAgMnSc1d1	místní
a	a	k8xC	a
regionální	regionální	k2eAgInSc1d1	regionální
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
o	o	k7c6	o
členství	členství	k1gNnSc6	členství
upravuje	upravovat	k5eAaImIp3nS	upravovat
směrnice	směrnice	k1gFnSc1	směrnice
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
během	během	k7c2	během
září	září	k1gNnSc2	září
2010	[number]	k4	2010
zažádala	zažádat	k5eAaPmAgFnS	zažádat
o	o	k7c6	o
členství	členství	k1gNnSc6	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
lidové	lidový	k2eAgFnSc6d1	lidová
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
EPP	EPP	kA	EPP
<g/>
/	/	kIx~	/
<g/>
ELS	Elsa	k1gFnPc2	Elsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
evropskou	evropský	k2eAgFnSc4d1	Evropská
afilaci	afilace	k1gFnSc4	afilace
<g/>
.	.	kIx.	.
</s>
<s>
Přijímací	přijímací	k2eAgNnSc1d1	přijímací
řízení	řízení	k1gNnSc1	řízení
trvalo	trvat	k5eAaImAgNnS	trvat
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
i	i	k9	i
několik	několik	k4yIc4	několik
cest	cesta	k1gFnPc2	cesta
Karla	Karel	k1gMnSc4	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
EPP	EPP	kA	EPP
a	a	k8xC	a
především	především	k9	především
přezkum	přezkum	k1gInSc1	přezkum
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
skupiny	skupina	k1gFnSc2	skupina
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
EPP	EPP	kA	EPP
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
posuzovala	posuzovat	k5eAaImAgFnS	posuzovat
způsobilost	způsobilost	k1gFnSc4	způsobilost
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
ke	k	k7c3	k
členství	členství	k1gNnSc3	členství
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
nakonec	nakonec	k9	nakonec
byla	být	k5eAaImAgFnS	být
shledána	shledat	k5eAaPmNgFnS	shledat
způsobilou	způsobilý	k2eAgFnSc7d1	způsobilá
a	a	k8xC	a
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
jí	jíst	k5eAaImIp3nS	jíst
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EPP	EPP	kA	EPP
udělen	udělen	k2eAgInSc4d1	udělen
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
členstvím	členství	k1gNnSc7	členství
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
celostátním	celostátní	k2eAgInSc6d1	celostátní
sněmu	sněm	k1gInSc6	sněm
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
–	–	k?	–
čestný	čestný	k2eAgMnSc1d1	čestný
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
Čestný	čestný	k2eAgMnSc1d1	čestný
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
Předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
–	–	k?	–
Marek	Marek	k1gMnSc1	Marek
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
Místopředsedové	místopředseda	k1gMnPc1	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
:	:	kIx,	:
Leoš	Leoš	k1gMnSc1	Leoš
Heger	Heger	k1gMnSc1	Heger
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
Jitka	Jitka	k1gFnSc1	Jitka
Chalánková	Chalánková	k1gFnSc1	Chalánková
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
Helena	Helena	k1gFnSc1	Helena
Langšádlová	Langšádlový	k2eAgFnSc1d1	Langšádlová
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
<g />
.	.	kIx.	.
</s>
<s>
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
Markéta	Markéta	k1gFnSc1	Markéta
Pekarová	Pekarová	k1gFnSc1	Pekarová
Adamová	Adamová	k1gFnSc1	Adamová
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
zastupitelka	zastupitelka	k1gFnSc1	zastupitelka
MČ	MČ	kA	MČ
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
Členové	člen	k1gMnPc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bezecný	Bezecný	k2eAgMnSc1d1	Bezecný
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
Lukáš	Lukáš	k1gMnSc1	Lukáš
Otys	Otys	k1gInSc1	Otys
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
v	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
Herbert	Herbert	k1gMnSc1	Herbert
Pavera	Pavero	k1gNnSc2	Pavero
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
starosta	starosta	k1gMnSc1	starosta
obce	obec	k1gFnSc2	obec
Bolatice	Bolatice	k1gFnSc2	Bolatice
Jiří	Jiří	k1gMnSc1	Jiří
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Pardubice	Pardubice	k1gInPc4	Pardubice
Tomáš	Tomáš	k1gMnSc1	Tomáš
Tesař	Tesař	k1gMnSc1	Tesař
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
Předseda	předseda	k1gMnSc1	předseda
klubu	klub	k1gInSc2	klub
poslanců	poslanec	k1gMnPc2	poslanec
–	–	k?	–
Michal	Michal	k1gMnSc1	Michal
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
radní	radní	k1gMnSc1	radní
města	město	k1gNnSc2	město
Louny	Louny	k1gInPc4	Louny
Předseda	předseda	k1gMnSc1	předseda
Klubu	klub	k1gInSc3	klub
starostů	starosta	k1gMnPc2	starosta
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
z.	z.	k?	z.
<g/>
s.	s.	k?	s.
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Ducháček	Ducháček	k1gMnSc1	Ducháček
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
obce	obec	k1gFnSc2	obec
Drnovice	Drnovice	k1gInPc4	Drnovice
Předseda	předseda	k1gMnSc1	předseda
klubu	klub	k1gInSc2	klub
–	–	k?	–
Michal	Michal	k1gMnSc1	Michal
Kučera	Kučera	k1gMnSc1	Kučera
Místopředsedové	místopředseda	k1gMnPc1	místopředseda
klubu	klub	k1gInSc2	klub
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Horáček	Horáček	k1gMnSc1	Horáček
Anna	Anna	k1gFnSc1	Anna
Putnová	Putnová	k1gFnSc1	Putnová
Marek	Marek	k1gMnSc1	Marek
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
Martin	Martin	k1gMnSc1	Martin
Plíšek	plíšek	k1gInSc4	plíšek
Do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
jde	jít	k5eAaImIp3nS	jít
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Klubu	klub	k1gInSc2	klub
angažovaných	angažovaný	k2eAgMnPc2d1	angažovaný
nestraníků	nestraník	k1gMnPc2	nestraník
<g/>
,	,	kIx,	,
Liberálně	liberálně	k6eAd1	liberálně
ekologické	ekologický	k2eAgFnSc2d1	ekologická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Strany	strana	k1gFnSc2	strana
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
podporu	podpor	k1gInSc2	podpor
také	také	k9	také
Sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
Evropští	evropský	k2eAgMnPc1d1	evropský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
občané	občan	k1gMnPc1	občan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
volili	volit	k5eAaImAgMnP	volit
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
volebním	volební	k2eAgInSc6d1	volební
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
7	[number]	k4	7
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
počet	počet	k1gInSc4	počet
získaných	získaný	k2eAgInPc2d1	získaný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
obcí	obec	k1gFnPc2	obec
taktéž	taktéž	k?	taktéž
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
krajích	kraj	k1gInPc6	kraj
buď	buď	k8xC	buď
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
koalicích	koalice	k1gFnPc6	koalice
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
<g/>
,	,	kIx,	,
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
<g/>
,	,	kIx,	,
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
a	a	k8xC	a
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
13	[number]	k4	13
krajů	kraj	k1gInPc2	kraj
se	se	k3xPyFc4	se
samostatná	samostatný	k2eAgFnSc1d1	samostatná
strana	strana	k1gFnSc1	strana
či	či	k8xC	či
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
koalice	koalice	k1gFnSc2	koalice
dostala	dostat	k5eAaPmAgFnS	dostat
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
6	[number]	k4	6
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
samostatná	samostatný	k2eAgFnSc1d1	samostatná
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
koalice	koalice	k1gFnSc1	koalice
obdržely	obdržet	k5eAaPmAgFnP	obdržet
5,46	[number]	k4	5,46
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
získaly	získat	k5eAaPmAgFnP	získat
26	[number]	k4	26
zastupitelů	zastupitel	k1gMnPc2	zastupitel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
19	[number]	k4	19
jich	on	k3xPp3gMnPc2	on
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
přímou	přímý	k2eAgFnSc4d1	přímá
volbu	volba	k1gFnSc4	volba
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
poslance	poslanec	k1gMnPc4	poslanec
a	a	k8xC	a
senátory	senátor	k1gMnPc4	senátor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nominovali	nominovat	k5eAaBmAgMnP	nominovat
jako	jako	k8xC	jako
kandidáta	kandidát	k1gMnSc4	kandidát
svého	svůj	k3xOyFgMnSc4	svůj
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
předsedu	předseda	k1gMnSc4	předseda
Karla	Karel	k1gMnSc4	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc4	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
podpořen	podpořit	k5eAaPmNgInS	podpořit
skupinou	skupina	k1gFnSc7	skupina
38	[number]	k4	38
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
zmocněncem	zmocněnec	k1gMnSc7	zmocněnec
Radkem	Radek	k1gMnSc7	Radek
Lojdou	Lojda	k1gMnSc7	Lojda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
skončil	skončit	k5eAaPmAgMnS	skončit
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
druhý	druhý	k4xOgInSc4	druhý
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
počtem	počet	k1gInSc7	počet
2	[number]	k4	2
241	[number]	k4	241
171	[number]	k4	171
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
45,2	[number]	k4	45,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
TOPAZ	topaz	k1gInSc1	topaz
je	být	k5eAaImIp3nS	být
think-tank	thinkank	k6eAd1	think-tank
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
jako	jako	k8xC	jako
občanské	občanský	k2eAgNnSc4d1	občanské
sdružení	sdružení	k1gNnSc4	sdružení
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
spolek	spolek	k1gInSc1	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
interní	interní	k2eAgFnSc2d1	interní
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
TOPAZ	topaz	k1gInSc1	topaz
organizuje	organizovat	k5eAaBmIp3nS	organizovat
konference	konference	k1gFnPc4	konference
<g/>
,	,	kIx,	,
školení	školení	k1gNnPc4	školení
<g/>
,	,	kIx,	,
debaty	debata	k1gFnPc4	debata
a	a	k8xC	a
přednášky	přednáška	k1gFnPc4	přednáška
o	o	k7c6	o
celospolečenských	celospolečenský	k2eAgNnPc6d1	celospolečenské
tématech	téma	k1gNnPc6	téma
s	s	k7c7	s
odbornou	odborný	k2eAgFnSc7d1	odborná
i	i	k8xC	i
laickou	laický	k2eAgFnSc7d1	laická
veřejností	veřejnost	k1gFnSc7	veřejnost
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
expertními	expertní	k2eAgFnPc7d1	expertní
komisemi	komise	k1gFnPc7	komise
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
koncepčních	koncepční	k2eAgInPc2d1	koncepční
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
TOPAZ	topaz	k1gInSc1	topaz
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
think-tanky	thinkanka	k1gFnSc2	think-tanka
a	a	k8xC	a
nadacemi	nadace	k1gFnPc7	nadace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Konrad-Adenauer-Stiftung	Konrad-Adenauer-Stiftung	k1gInSc4	Konrad-Adenauer-Stiftung
nebo	nebo	k8xC	nebo
Wilfried	Wilfried	k1gInSc4	Wilfried
Martens	Martensa	k1gFnPc2	Martensa
Centre	centr	k1gInSc5	centr
for	forum	k1gNnPc2	forum
European	European	k1gMnSc1	European
Studies	Studies	k1gMnSc1	Studies
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
starostů	starosta	k1gMnPc2	starosta
je	být	k5eAaImIp3nS	být
platformou	platforma	k1gFnSc7	platforma
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
komunálních	komunální	k2eAgMnPc2d1	komunální
politiků	politik	k1gMnPc2	politik
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
vzniku	vznik	k1gInSc2	vznik
Klubu	klub	k1gInSc2	klub
starostů	starosta	k1gMnPc2	starosta
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
je	být	k5eAaImIp3nS	být
sdružení	sdružení	k1gNnSc4	sdružení
komunálních	komunální	k2eAgMnPc2d1	komunální
politiků	politik	k1gMnPc2	politik
zvolených	zvolený	k2eAgMnPc2d1	zvolený
za	za	k7c4	za
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
výměna	výměna	k1gFnSc1	výměna
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
získávání	získávání	k1gNnSc1	získávání
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
a	a	k8xC	a
přinášení	přinášení	k1gNnSc2	přinášení
nových	nový	k2eAgNnPc2d1	nové
témat	téma	k1gNnPc2	téma
pro	pro	k7c4	pro
komunální	komunální	k2eAgFnSc4d1	komunální
i	i	k8xC	i
celostátní	celostátní	k2eAgFnSc4d1	celostátní
politiku	politika	k1gFnSc4	politika
na	na	k7c6	na
základě	základ	k1gInSc6	základ
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
hodnot	hodnota	k1gFnPc2	hodnota
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
klubu	klub	k1gInSc2	klub
starostů	starosta	k1gMnPc2	starosta
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Ducháček	Ducháček	k1gMnSc1	Ducháček
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
obce	obec	k1gFnSc2	obec
Drnovice	Drnovice	k1gInPc4	Drnovice
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
tým	tým	k1gInSc1	tým
je	být	k5eAaImIp3nS	být
mládežnickou	mládežnický	k2eAgFnSc7d1	mládežnická
organizací	organizace	k1gFnSc7	organizace
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
a	a	k8xC	a
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
čela	čelo	k1gNnSc2	čelo
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jan	Jan	k1gMnSc1	Jan
Kavalírek	kavalírek	k1gMnSc1	kavalírek
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
osoby	osoba	k1gFnPc4	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
otevřeně	otevřeně	k6eAd1	otevřeně
podporují	podporovat	k5eAaImIp3nP	podporovat
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
svými	svůj	k3xOyFgFnPc7	svůj
liberálně-konzervativními	liberálněonzervativní	k2eAgFnPc7d1	liberálně-konzervativní
idejemi	idea	k1gFnPc7	idea
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
tým	tým	k1gInSc1	tým
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
pořádání	pořádání	k1gNnSc4	pořádání
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
besed	beseda	k1gFnPc2	beseda
<g/>
,	,	kIx,	,
seminářů	seminář	k1gInPc2	seminář
či	či	k8xC	či
konferencí	konference	k1gFnPc2	konference
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
setkávání	setkávání	k1gNnSc6	setkávání
s	s	k7c7	s
politiky	politik	k1gMnPc7	politik
a	a	k8xC	a
pořádá	pořádat	k5eAaImIp3nS	pořádat
různé	různý	k2eAgFnPc4d1	různá
akce	akce	k1gFnPc4	akce
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
prohloubení	prohloubení	k1gNnSc1	prohloubení
komunikace	komunikace	k1gFnSc2	komunikace
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
celorepubliková	celorepublikový	k2eAgFnSc1d1	celorepubliková
organizace	organizace	k1gFnSc1	organizace
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
krajských	krajský	k2eAgFnPc6d1	krajská
organizacích	organizace	k1gFnPc6	organizace
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
Jihomoravském	jihomoravský	k2eAgNnSc6d1	Jihomoravské
<g/>
,	,	kIx,	,
Středočeském	středočeský	k2eAgNnSc6d1	Středočeské
<g/>
,	,	kIx,	,
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
tým	tým	k1gInSc1	tým
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
evropské	evropský	k2eAgFnSc2d1	Evropská
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
organizace	organizace	k1gFnSc2	organizace
Youth	Youth	k1gMnSc1	Youth
of	of	k?	of
European	European	k1gMnSc1	European
People	People	k1gMnSc1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Party	party	k1gFnSc7	party
(	(	kIx(	(
<g/>
YEPP	YEPP	kA	YEPP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
