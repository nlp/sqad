<s>
Švédština	švédština	k1gFnSc1	švédština
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
germánský	germánský	k2eAgInSc1d1	germánský
jazyk	jazyk	k1gInSc1	jazyk
používá	používat	k5eAaImIp3nS	používat
supinum	supinum	k1gNnSc4	supinum
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
slovesem	sloveso	k1gNnSc7	sloveso
ha	ha	kA	ha
(	(	kIx(	(
<g/>
mít	mít	k5eAaImF	mít
<g/>
)	)	kIx)	)
k	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
perfekta	perfektum	k1gNnSc2	perfektum
a	a	k8xC	a
plusquamperfekta	plusquamperfektum	k1gNnSc2	plusquamperfektum
<g/>
:	:	kIx,	:
jag	jaga	k1gFnPc2	jaga
har	har	k?	har
skrivit	skrivita	k1gFnPc2	skrivita
(	(	kIx(	(
<g/>
napsal	napsat	k5eAaPmAgInS	napsat
jsem	být	k5eAaImIp1nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
