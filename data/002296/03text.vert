<s>
Supinum	supinum	k1gNnSc1	supinum
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
supinus	supinus	k1gInSc1	supinus
-	-	kIx~	-
"	"	kIx"	"
<g/>
obrácený	obrácený	k2eAgMnSc1d1	obrácený
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
motivace	motivace	k1gFnSc1	motivace
názvu	název	k1gInSc2	název
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neurčitý	určitý	k2eNgInSc4d1	neurčitý
tvar	tvar	k1gInSc4	tvar
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
příčestí	příčestí	k1gNnSc2	příčestí
<g/>
)	)	kIx)	)
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
slovesech	sloveso	k1gNnPc6	sloveso
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
blízké	blízký	k2eAgNnSc1d1	blízké
infinitivu	infinitiv	k1gInSc3	infinitiv
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
účelovost	účelovost	k1gFnSc4	účelovost
<g/>
,	,	kIx,	,
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
cíl	cíl	k1gInSc4	cíl
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
záměru	záměr	k1gInSc2	záměr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
jít	jít	k5eAaImF	jít
spat	spát	k5eAaImF	spát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
se	se	k3xPyFc4	se
supinum	supinum	k1gNnSc1	supinum
tvořilo	tvořit	k5eAaImAgNnS	tvořit
od	od	k7c2	od
nedokonavých	dokonavý	k2eNgNnPc2d1	nedokonavé
sloves	sloveso	k1gNnPc2	sloveso
a	a	k8xC	a
užívalo	užívat	k5eAaImAgNnS	užívat
se	se	k3xPyFc4	se
po	po	k7c6	po
slovesech	sloveso	k1gNnPc6	sloveso
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
předmět	předmět	k1gInSc1	předmět
měl	mít	k5eAaImAgInS	mít
genitivní	genitivní	k2eAgFnSc4d1	genitivní
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
V	v	k7c6	v
pračeštině	pračeština	k1gFnSc6	pračeština
se	se	k3xPyFc4	se
supinum	supinum	k1gNnSc1	supinum
tvořilo	tvořit	k5eAaImAgNnS	tvořit
koncovkou	koncovka	k1gFnSc7	koncovka
-	-	kIx~	-
<g/>
t	t	k?	t
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
-	-	kIx~	-
<g/>
c	c	k0	c
<g/>
;	;	kIx,	;
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
infinitiv	infinitiv	k1gInSc1	infinitiv
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
supinu	supinum	k1gNnSc6	supinum
samohláska	samohláska	k1gFnSc1	samohláska
krátká	krátká	k1gFnSc1	krátká
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
spáti	spát	k5eAaImF	spát
-	-	kIx~	-
spat	spát	k5eAaImF	spát
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
vazbě	vazba	k1gFnSc6	vazba
jít	jít	k5eAaImF	jít
spat	spát	k5eAaImF	spát
<g/>
)	)	kIx)	)
bráti	brát	k5eAaImF	brát
-	-	kIx~	-
brat	brat	k1gMnSc1	brat
péci	péct	k5eAaImF	péct
-	-	kIx~	-
pec	pec	k1gFnSc1	pec
Příklad	příklad	k1gInSc1	příklad
použití	použití	k1gNnSc1	použití
<g/>
:	:	kIx,	:
Příde	Příd	k1gMnSc5	Příd
súdit	súdit	k1gInSc4	súdit
živých	živý	k1gMnPc2	živý
i	i	k8xC	i
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rané	raný	k2eAgFnSc2d1	raná
staré	starý	k2eAgFnSc2d1	stará
češtiny	čeština	k1gFnSc2	čeština
bylo	být	k5eAaImAgNnS	být
supinum	supinum	k1gNnSc1	supinum
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
infinitivem	infinitiv	k1gInSc7	infinitiv
či	či	k8xC	či
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
splynulo	splynout	k5eAaPmAgNnS	splynout
(	(	kIx(	(
<g/>
např.	např.	kA	např.
král	král	k1gMnSc1	král
šel	jít	k5eAaImAgMnS	jít
spáti	spát	k5eAaImF	spát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Supinum	supinum	k1gNnSc1	supinum
však	však	k9	však
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nezaniklo	zaniknout	k5eNaPmAgNnS	zaniknout
beze	beze	k7c2	beze
stopy	stopa	k1gFnSc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
t	t	k?	t
(	(	kIx(	(
<g/>
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
,	,	kIx,	,
supinová	supinový	k2eAgFnSc1d1	supinový
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
u	u	k7c2	u
infinitivu	infinitiv	k1gInSc2	infinitiv
<g/>
,	,	kIx,	,
vznikaly	vznikat	k5eAaImAgInP	vznikat
dubletní	dubletní	k2eAgInPc1d1	dubletní
tvary	tvar	k1gInPc1	tvar
k	k	k7c3	k
původním	původní	k2eAgMnPc3d1	původní
tvarům	tvar	k1gInPc3	tvar
na	na	k7c6	na
(	(	kIx(	(
<g/>
měkké	měkký	k2eAgFnSc2d1	měkká
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g/>
ti	ten	k3xDgMnPc1	ten
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infinitivní	infinitivní	k2eAgNnSc1d1	infinitivní
zakončení	zakončení	k1gNnSc1	zakončení
-	-	kIx~	-
<g/>
t	t	k?	t
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
převládlo	převládnout	k5eAaPmAgNnS	převládnout
<g/>
,	,	kIx,	,
infinitivní	infinitivní	k2eAgInPc4d1	infinitivní
tvary	tvar	k1gInPc4	tvar
zakončené	zakončený	k2eAgInPc4d1	zakončený
-	-	kIx~	-
<g/>
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
zastaralé	zastaralý	k2eAgInPc4d1	zastaralý
nebo	nebo	k8xC	nebo
knižní	knižní	k2eAgInPc4d1	knižní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
forma	forma	k1gFnSc1	forma
infinitivu	infinitiv	k1gInSc2	infinitiv
od	od	k7c2	od
zakočení	zakočení	k1gNnSc2	zakočení
na	na	k7c6	na
-	-	kIx~	-
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachovala	zachovat	k5eAaPmAgFnS	zachovat
si	se	k3xPyFc3	se
oproti	oproti	k7c3	oproti
češtině	čeština	k1gFnSc3	čeština
infinitivní	infinitivní	k2eAgFnSc4d1	infinitivní
měkkost	měkkost	k1gFnSc4	měkkost
s	s	k7c7	s
-	-	kIx~	-
<g/>
ť.	ť.	k?	ť.
Ostatní	ostatní	k2eAgFnSc1d1	ostatní
slovanština	slovanština	k1gFnSc1	slovanština
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
supinum	supinum	k1gNnSc1	supinum
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
resp.	resp.	kA	resp.
splynulo	splynout	k5eAaPmAgNnS	splynout
s	s	k7c7	s
tvarově	tvarově	k6eAd1	tvarově
i	i	k8xC	i
významem	význam	k1gInSc7	význam
blízkým	blízký	k2eAgInSc7d1	blízký
infinitivem	infinitiv	k1gInSc7	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Zachováno	zachován	k2eAgNnSc4d1	zachováno
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ve	v	k7c6	v
slovinštině	slovinština	k1gFnSc6	slovinština
a	a	k8xC	a
dolnolužické	dolnolužický	k2eAgFnSc3d1	dolnolužická
srbštině	srbština	k1gFnSc3	srbština
<g/>
:	:	kIx,	:
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
<g/>
:	:	kIx,	:
supinum	supinum	k1gNnSc1	supinum
končilo	končit	k5eAaImAgNnS	končit
na	na	k7c6	na
-tъ	-tъ	k?	-tъ
<g/>
:	:	kIx,	:
Ne	ne	k9	ne
minte	minte	k5eAaPmIp2nP	minte
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pridъ	pridъ	k?	pridъ
razoritъ	razoritъ	k?	razoritъ
zakona	zakon	k1gMnSc2	zakon
li	li	k8xS	li
prorokъ	prorokъ	k?	prorokъ
<g/>
.	.	kIx.	.
dolnolužická	dolnolužický	k2eAgFnSc1d1	dolnolužická
srbština	srbština	k1gFnSc1	srbština
<g/>
:	:	kIx,	:
Tvar	tvar	k1gInSc1	tvar
supina	supinum	k1gNnSc2	supinum
vzniká	vznikat	k5eAaImIp3nS	vznikat
záměnou	záměna	k1gFnSc7	záměna
koncového	koncový	k2eAgInSc2d1	koncový
měkkého	měkký	k2eAgInSc2d1	měkký
-ś	-ś	k?	-ś
<g/>
,	,	kIx,	,
-ć	-ć	k?	-ć
infinitivu	infinitiv	k1gInSc6	infinitiv
na	na	k7c6	na
-t	-t	k?	-t
(	(	kIx(	(
<g/>
Źi	Źi	k1gFnSc1	Źi
knigły	knigła	k1gFnSc2	knigła
tam	tam	k6eAd1	tam
donjast	donjast	k5eAaPmF	donjast
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
infinitiv	infinitiv	k1gInSc1	infinitiv
kupowaś	kupowaś	k?	kupowaś
<g/>
,	,	kIx,	,
supinum	supinum	k1gNnSc1	supinum
kupowat	kupowat	k1gInSc1	kupowat
<g/>
,	,	kIx,	,
inf.	inf.	k?	inf.
źěłaś	źěłaś	k?	źěłaś
-	-	kIx~	-
sup	sup	k1gMnSc1	sup
<g/>
.	.	kIx.	.
źěłat	źěłat	k2eAgMnSc1d1	źěłat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
infinitiv	infinitiv	k1gInSc1	infinitiv
kočí	kočí	k2eAgInSc1d1	kočí
na	na	k7c4	na
-c	-c	k?	-c
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
t	t	k?	t
(	(	kIx(	(
<g/>
inf.	inf.	k?	inf.
woblac	woblac	k1gInSc1	woblac
-	-	kIx~	-
sup	sup	k1gMnSc1	sup
<g/>
.	.	kIx.	.
woblact	woblact	k1gMnSc1	woblact
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
slovinština	slovinština	k1gFnSc1	slovinština
<g/>
:	:	kIx,	:
Supinum	supinum	k1gNnSc1	supinum
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
tvoří	tvořit	k5eAaImIp3nP	tvořit
z	z	k7c2	z
infinitivu	infinitiv	k1gInSc2	infinitiv
vypuštěním	vypuštění	k1gNnSc7	vypuštění
koncového	koncový	k2eAgNnSc2d1	koncové
i	i	k9	i
<g/>
,	,	kIx,	,
např.	např.	kA	např.
inf.	inf.	k?	inf.
spati	spať	k1gFnSc2	spať
-	-	kIx~	-
sup	sup	k1gMnSc1	sup
<g/>
.	.	kIx.	.
spat	spát	k5eAaImF	spát
<g/>
,	,	kIx,	,
inf.	inf.	k?	inf.
speči	speč	k1gInSc6	speč
-	-	kIx~	-
sup	supit	k5eAaImRp2nS	supit
<g/>
.	.	kIx.	.
speč	spéct	k5eAaPmRp2nS	spéct
(	(	kIx(	(
<g/>
Pojdi	pojít	k5eAaPmRp2nS	pojít
z	z	k7c2	z
menoj	menoj	k1gInSc1	menoj
na	na	k7c4	na
morje	morj	k1gFnPc4	morj
veslat	vesle	k1gNnPc2	vesle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
litevštině	litevština	k1gFnSc6	litevština
se	s	k7c7	s
<g/>
:	:	kIx,	:
<g/>
supinum	supinum	k1gNnSc1	supinum
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
infinitivu	infinitiv	k1gInSc2	infinitiv
koncovkou	koncovka	k1gFnSc7	koncovka
-tų	ų	k?	-tų
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Jis	Jis	k1gFnSc1	Jis
nuė	nuė	k?	nuė
medžiotų	medžiotų	k?	medžiotų
(	(	kIx(	(
<g/>
Šel	jít	k5eAaImAgMnS	jít
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
již	již	k6eAd1	již
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
východních	východní	k2eAgNnPc6d1	východní
aukštockých	aukštocký	k2eAgNnPc6d1	aukštocký
nářečích	nářečí	k1gNnPc6	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spisovném	spisovný	k2eAgInSc6d1	spisovný
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
nahrazované	nahrazovaný	k2eAgInPc1d1	nahrazovaný
infinitivem	infinitiv	k1gInSc7	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latině	latina	k1gFnSc6	latina
bývá	bývat	k5eAaImIp3nS	bývat
supinum	supinum	k1gNnSc1	supinum
zakončeno	zakončen	k2eAgNnSc1d1	zakončeno
na	na	k7c6	na
-	-	kIx~	-
<g/>
tum	tum	k?	tum
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
-	-	kIx~	-
<g/>
sum	suma	k1gFnPc2	suma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
trojí	trojit	k5eAaImIp3nS	trojit
<g/>
:	:	kIx,	:
Samostatně	samostatně	k6eAd1	samostatně
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
po	po	k7c6	po
slovesech	sloveso	k1gNnPc6	sloveso
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
ire	ire	k?	ire
<g/>
,	,	kIx,	,
venire	venir	k1gInSc5	venir
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
posílání	posílání	k1gNnSc1	posílání
(	(	kIx(	(
<g/>
mittere	mitter	k1gMnSc5	mitter
<g/>
)	)	kIx)	)
namísto	namísto	k7c2	namísto
infinitivu	infinitiv	k1gInSc2	infinitiv
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
cíl	cíl	k1gInSc1	cíl
<g/>
/	/	kIx~	/
účel	účel	k1gInSc1	účel
daného	daný	k2eAgInSc2d1	daný
děje	děj	k1gInSc2	děj
<g/>
:	:	kIx,	:
např.	např.	kA	např.
Dormitum	Dormitum	k1gNnSc1	Dormitum
imus	imus	k1gInSc1	imus
(	(	kIx(	(
<g/>
=	=	kIx~	=
Jdeme	jít	k5eAaImIp1nP	jít
spát	spát	k5eAaImF	spát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xS	jako
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
objevující	objevující	k2eAgInSc1d1	objevující
archaismus	archaismus	k1gInSc1	archaismus
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
spojení	spojení	k1gNnSc6	spojení
jít	jít	k5eAaImF	jít
spat	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
též	též	k9	též
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
supinum	supinum	k1gNnSc4	supinum
I.	I.	kA	I.
V	v	k7c6	v
redukované	redukovaný	k2eAgFnSc6d1	redukovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
zakončené	zakončený	k2eAgFnSc6d1	zakončená
na	na	k7c6	na
-tu	u	k?	-tu
nebo	nebo	k8xC	nebo
-su	u	k?	-su
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
supinum	supinum	k1gNnSc1	supinum
klade	klást	k5eAaImIp3nS	klást
po	po	k7c6	po
určitých	určitý	k2eAgNnPc6d1	určité
adjektivech	adjektivum	k1gNnPc6	adjektivum
zpravidla	zpravidla	k6eAd1	zpravidla
zakončených	zakončený	k2eAgFnPc2d1	zakončená
na	na	k7c4	na
-ilis	lis	k1gFnSc4	-ilis
<g/>
/	/	kIx~	/
<g/>
e	e	k0	e
<g/>
:	:	kIx,	:
e.	e.	k?	e.
g.	g.	k?	g.
horribile	horribila	k1gFnSc3	horribila
visu	vis	k1gInSc2	vis
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
strašné	strašný	k2eAgNnSc1d1	strašné
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
strašná	strašný	k2eAgFnSc1d1	strašná
věc	věc	k1gFnSc1	věc
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
strašné	strašný	k2eAgNnSc1d1	strašné
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dívat	dívat	k5eAaImF	dívat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
difficilis	difficilis	k1gFnSc1	difficilis
factu	fact	k1gInSc2	fact
(	(	kIx(	(
<g/>
obtížný	obtížný	k2eAgInSc1d1	obtížný
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
též	též	k9	též
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
supinum	supinum	k1gNnSc4	supinum
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
supina	supinum	k1gNnSc2	supinum
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
tzv.	tzv.	kA	tzv.
participium	participium	k1gNnSc1	participium
perfekta	perfektum	k1gNnSc2	perfektum
pasiva	pasivum	k1gNnSc2	pasivum
(	(	kIx(	(
<g/>
≈	≈	k?	≈
české	český	k2eAgNnSc4d1	české
příčestí	příčestí	k1gNnSc4	příčestí
minulé	minulý	k2eAgFnSc2d1	minulá
trpné	trpný	k2eAgFnSc2d1	trpná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vocatus	vocatus	k1gInSc1	vocatus
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
um	um	k1gInSc1	um
(	(	kIx(	(
<g/>
zavolaný	zavolaný	k2eAgMnSc1d1	zavolaný
<g/>
/	/	kIx~	/
<g/>
á	á	k0	á
<g/>
/	/	kIx~	/
<g/>
é	é	k0	é
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
tvoření	tvoření	k1gNnSc6	tvoření
různých	různý	k2eAgInPc2d1	různý
pasivních	pasivní	k2eAgInPc2d1	pasivní
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
perfektu	perfektum	k1gNnSc6	perfektum
(	(	kIx(	(
<g/>
sanatus	sanatus	k1gMnSc1	sanatus
(	(	kIx(	(
<g/>
vyléčený	vyléčený	k2eAgMnSc1d1	vyléčený
<g/>
)	)	kIx)	)
+	+	kIx~	+
tvar	tvar	k1gInSc1	tvar
slovesa	sloveso	k1gNnSc2	sloveso
esse	essat	k5eAaPmIp3nS	essat
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
=	=	kIx~	=
sanatus	sanatus	k1gInSc1	sanatus
sum	suma	k1gFnPc2	suma
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
vyléčen	vyléčen	k2eAgMnSc1d1	vyléčen
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jsem	být	k5eAaImIp1nS	být
vyléčený	vyléčený	k2eAgInSc1d1	vyléčený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
od	od	k7c2	od
supinového	supinový	k2eAgInSc2d1	supinový
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
také	také	k9	také
řada	řada	k1gFnSc1	řada
substantiv	substantivum	k1gNnPc2	substantivum
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
sloveso	sloveso	k1gNnSc1	sloveso
vincere	vincrat	k5eAaPmIp3nS	vincrat
(	(	kIx(	(
<g/>
vítězit	vítězit	k5eAaImF	vítězit
<g/>
,	,	kIx,	,
porážet	porážet	k5eAaImF	porážet
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
supinum	supinum	k1gNnSc4	supinum
victum	victum	k1gNnSc1	victum
<g/>
,	,	kIx,	,
od	od	k7c2	od
jehož	jenž	k3xRgInSc2	jenž
kmene	kmen	k1gInSc2	kmen
vict-	vict-	k?	vict-
jsou	být	k5eAaImIp3nP	být
odvozena	odvozen	k2eAgNnPc1d1	odvozeno
substantiva	substantivum	k1gNnPc1	substantivum
victor	victor	k1gInSc1	victor
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
)	)	kIx)	)
a	a	k8xC	a
victoria	victorium	k1gNnSc2	victorium
(	(	kIx(	(
<g/>
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Švédština	švédština	k1gFnSc1	švédština
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
germánský	germánský	k2eAgInSc1d1	germánský
jazyk	jazyk	k1gInSc1	jazyk
používá	používat	k5eAaImIp3nS	používat
supinum	supinum	k1gNnSc4	supinum
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
slovesem	sloveso	k1gNnSc7	sloveso
ha	ha	kA	ha
(	(	kIx(	(
<g/>
mít	mít	k5eAaImF	mít
<g/>
)	)	kIx)	)
k	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
perfekta	perfektum	k1gNnSc2	perfektum
a	a	k8xC	a
plusquamperfekta	plusquamperfektum	k1gNnSc2	plusquamperfektum
<g/>
:	:	kIx,	:
jag	jaga	k1gFnPc2	jaga
har	har	k?	har
skrivit	skrivita	k1gFnPc2	skrivita
(	(	kIx(	(
<g/>
napsal	napsat	k5eAaBmAgInS	napsat
jsem	být	k5eAaImIp1nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
slabých	slabý	k2eAgNnPc2d1	slabé
sloves	sloveso	k1gNnPc2	sloveso
má	mít	k5eAaImIp3nS	mít
shodný	shodný	k2eAgInSc1d1	shodný
tvar	tvar	k1gInSc1	tvar
jako	jako	k8xC	jako
pasivní	pasivní	k2eAgNnSc1d1	pasivní
participium	participium	k1gNnSc1	participium
(	(	kIx(	(
<g/>
příčestí	příčestí	k1gNnSc1	příčestí
trpné	trpný	k2eAgNnSc1d1	trpné
<g/>
)	)	kIx)	)
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
středního	střední	k2eAgInSc2d1	střední
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
älskat	älskat	k5eAaBmF	älskat
(	(	kIx(	(
<g/>
milováno	milovat	k5eAaImNgNnS	milovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
köpt	köpt	k1gInSc1	köpt
(	(	kIx(	(
<g/>
koupeno	koupen	k2eAgNnSc1d1	koupeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
silných	silný	k2eAgNnPc2d1	silné
a	a	k8xC	a
nepravidelných	pravidelný	k2eNgNnPc2d1	nepravidelné
sloves	sloveso	k1gNnPc2	sloveso
má	mít	k5eAaImIp3nS	mít
supinum	supinum	k1gNnSc1	supinum
zakončení	zakončení	k1gNnSc2	zakončení
-	-	kIx~	-
<g/>
it	it	k?	it
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
střední	střední	k2eAgInSc4d1	střední
rod	rod	k1gInSc4	rod
trpného	trpný	k2eAgNnSc2d1	trpné
příčestí	příčestí	k1gNnSc2	příčestí
-	-	kIx~	-
<g/>
et	et	k?	et
<g/>
:	:	kIx,	:
brevet	brevet	k1gMnSc1	brevet
är	är	k?	är
skrivet	skrivet	k1gMnSc1	skrivet
(	(	kIx(	(
<g/>
dopis	dopis	k1gInSc1	dopis
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaPmNgInS	napsat
<g/>
)	)	kIx)	)
×	×	k?	×
jag	jaga	k1gFnPc2	jaga
har	har	k?	har
skrivit	skrivita	k1gFnPc2	skrivita
ett	ett	k?	ett
brev	brev	k1gMnSc1	brev
(	(	kIx(	(
<g/>
napsal	napsat	k5eAaBmAgMnS	napsat
jsem	být	k5eAaImIp1nS	být
dopis	dopis	k1gInSc4	dopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Důsledné	důsledný	k2eAgNnSc1d1	důsledné
rozlišení	rozlišení	k1gNnSc1	rozlišení
tvarů	tvar	k1gInPc2	tvar
-	-	kIx~	-
<g/>
it	it	k?	it
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
et	et	k?	et
je	být	k5eAaImIp3nS	být
kodifikováno	kodifikovat	k5eAaBmNgNnS	kodifikovat
ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
švédštině	švédština	k1gFnSc6	švédština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
nářečích	nářečí	k1gNnPc6	nářečí
se	se	k3xPyFc4	se
však	však	k9	však
tyto	tento	k3xDgInPc1	tento
tvary	tvar	k1gInPc1	tvar
často	často	k6eAd1	často
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
podle	podle	k7c2	podle
místních	místní	k2eAgFnPc2d1	místní
zvyklostí	zvyklost	k1gFnPc2	zvyklost
<g/>
.	.	kIx.	.
</s>
