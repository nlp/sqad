<s>
Kanton	Kanton	k1gInSc1
Bénévent-l	Bénévent-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Abbaye	Abbaye	k1gInSc1
</s>
<s>
Kanton	Kanton	k1gInSc1
Bénévent-l	Bénévent-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Abbaye	Abbaye	k1gInSc1
Kanton	Kanton	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
arrondissementu	arrondissement	k1gInSc2
Guéret	Guéret	k1gInSc4
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Region	region	k1gInSc1
</s>
<s>
Limousin	Limousin	k2eAgInSc1d1
Departement	departement	k1gInSc1
</s>
<s>
Creuse	Creuse	k6eAd1
Arrondissement	Arrondissement	k1gInSc1
</s>
<s>
Guéret	Guéret	k1gInSc1
Počet	počet	k1gInSc1
obcí	obec	k1gFnPc2
</s>
<s>
10	#num#	k4
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
</s>
<s>
Bénévent-l	Bénévent-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Abbaye	Abbaye	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
191,42	191,42	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
3	#num#	k4
407	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
17,8	17,8	k4
ob.	ob.	k?
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
Kanton	Kanton	k1gInSc1
Bénévent-l	Bénévent-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Abbaye	Abbaye	k1gInSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Canton	Canton	k1gInSc1
de	de	k?
Bénévent-l	Bénévent-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Abbaye	Abbaye	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgInSc4d1
kanton	kanton	k1gInSc4
v	v	k7c6
departementu	departement	k1gInSc6
Creuse	Creuse	k1gFnSc2
v	v	k7c6
regionu	region	k1gInSc6
Limousin	Limousin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
10	#num#	k4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
kantonu	kanton	k1gInSc2
</s>
<s>
Arrè	Arrè	k1gMnSc1
</s>
<s>
Augè	Augè	k1gMnSc1
</s>
<s>
Aulon	Aulon	k1gMnSc1
</s>
<s>
Azat-Châtenet	Azat-Châtenet	k1gMnSc1
</s>
<s>
Bénévent-l	Bénévent-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Abbaye	Abbaye	k1gInSc1
</s>
<s>
Ceyroux	Ceyroux	k1gInSc1
</s>
<s>
Châtelus-le-Marcheix	Châtelus-le-Marcheix	k1gInSc1
</s>
<s>
Marsac	Marsac	k6eAd1
</s>
<s>
Mourioux-Vieilleville	Mourioux-Vieilleville	k6eAd1
</s>
<s>
Saint-Goussaud	Saint-Goussaud	k6eAd1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kantony	Kanton	k1gInPc1
v	v	k7c6
departementu	departement	k1gInSc6
Creuse	Creuse	k1gFnSc2
</s>
<s>
Ahun	Ahun	k1gMnSc1
•	•	k?
Aubusson	Aubusson	k1gMnSc1
•	•	k?
Auzances	Auzances	k1gMnSc1
•	•	k?
Bellegarde-en-Marche	Bellegarde-en-Marche	k1gInSc1
•	•	k?
Bénévent-l	Bénévent-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Abbaye	Abbaye	k1gNnSc1
•	•	k?
Bonnat	Bonnat	k1gMnSc1
•	•	k?
Bourganeuf	Bourganeuf	k1gMnSc1
•	•	k?
Boussac	Boussac	k1gInSc1
•	•	k?
Chambon-sur-Voueize	Chambon-sur-Voueize	k1gFnSc2
•	•	k?
Châtelus-Malvaleix	Châtelus-Malvaleix	k1gInSc1
•	•	k?
Chénérailles	Chénérailles	k1gInSc1
•	•	k?
La	la	k1gNnSc1
Courtine	Courtin	k1gInSc5
•	•	k?
Crocq	Crocq	k1gMnSc3
•	•	k?
Dun-le-Palestel	Dun-le-Palestel	k1gMnSc1
•	•	k?
Évaux-les-Bains	Évaux-les-Bains	k1gInSc1
•	•	k?
Felletin	Felletin	k2eAgInSc1d1
•	•	k?
Gentioux-Pigerolles	Gentioux-Pigerolles	k1gInSc1
•	•	k?
Le	Le	k1gFnSc2
Grand-Bourg	Grand-Bourg	k1gMnSc1
•	•	k?
Guéret-Nord	Guéret-Nord	k1gMnSc1
•	•	k?
Guéret-Sud-Est	Guéret-Sud-Est	k1gMnSc1
•	•	k?
Guéret-Sud-Ouest	Guéret-Sud-Ouest	k1gMnSc1
•	•	k?
Jarnages	Jarnages	k1gMnSc1
•	•	k?
Pontarion	Pontarion	k1gInSc1
•	•	k?
Royè	Royè	k1gInSc5
•	•	k?
Saint-Sulpice-les-Champs	Saint-Sulpice-les-Champs	k1gInSc1
•	•	k?
Saint-Vaury	Saint-Vaura	k1gFnSc2
•	•	k?
La	la	k1gNnSc1
Souterraine	Souterrain	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
91005436	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
160252441	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
91005436	#num#	k4
</s>
