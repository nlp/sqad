<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
15	[number]	k4	15
%	%	kIx~	%
jeho	jeho	k3xOp3gNnSc2	jeho
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
asi	asi	k9	asi
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
trvale	trvale	k6eAd1	trvale
zaledněno	zaledněn	k2eAgNnSc1d1	zaledněno
<g/>
.	.	kIx.	.
</s>
