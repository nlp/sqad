<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
Atlantiku	Atlantik	k1gInSc2	Atlantik
a	a	k8xC	a
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
geograficky	geograficky	k6eAd1	geograficky
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
historicky	historicky	k6eAd1	historicky
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
zemi	zem	k1gFnSc6	zem
velmi	velmi	k6eAd1	velmi
propojenou	propojený	k2eAgFnSc4d1	propojená
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
autonomní	autonomní	k2eAgFnSc4d1	autonomní
součást	součást	k1gFnSc4	součást
Dánského	dánský	k2eAgNnSc2d1	dánské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
dánský	dánský	k2eAgMnSc1d1	dánský
monarcha	monarcha	k1gMnSc1	monarcha
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Nuuk	Nuuk	k1gInSc1	Nuuk
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
15	[number]	k4	15
%	%	kIx~	%
jeho	jeho	k3xOp3gNnSc2	jeho
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
asi	asi	k9	asi
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
trvale	trvale	k6eAd1	trvale
zaledněno	zaledněn	k2eAgNnSc1d1	zaledněno
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
led	led	k1gInSc4	led
dosahující	dosahující	k2eAgInSc4d1	dosahující
místy	místo	k1gNnPc7	místo
mocnosti	mocnost	k1gFnSc2	mocnost
až	až	k8xS	až
3	[number]	k4	3
000	[number]	k4	000
m.	m.	k?	m.
Tento	tento	k3xDgInSc4	tento
ledovec	ledovec	k1gInSc4	ledovec
(	(	kIx(	(
<g/>
pokrývající	pokrývající	k2eAgFnSc4d1	pokrývající
přibližně	přibližně	k6eAd1	přibližně
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
ledovci	ledovec	k1gInSc6	ledovec
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgFnSc4d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
roztátí	roztátý	k2eAgMnPc1d1	roztátý
by	by	kYmCp3nS	by
hladina	hladina	k1gFnSc1	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
o	o	k7c4	o
7	[number]	k4	7
m.	m.	k?	m.
Oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
země	zem	k1gFnSc2	zem
v	v	k7c6	v
grónštině	grónština	k1gFnSc6	grónština
zní	znět	k5eAaImIp3nS	znět
"	"	kIx"	"
<g/>
Kalaallit	Kalaallit	k1gInSc1	Kalaallit
Nunaat	Nunaat	k1gInSc1	Nunaat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgNnSc4	první
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kalaallit	kalaallit	k1gInSc1	kalaallit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plurál	plurál	k1gInSc1	plurál
od	od	k7c2	od
"	"	kIx"	"
<g/>
kalaaleq	kalaaleq	k?	kalaaleq
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
obyvatel	obyvatel	k1gMnSc1	obyvatel
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Gróňan	Gróňan	k1gInSc1	Gróňan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nunaat	Nunaat	k1gInSc1	Nunaat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kalaallit	Kalaallit	k1gInSc1	Kalaallit
Nunaat	Nunaat	k1gInSc1	Nunaat
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
dá	dát	k5eAaPmIp3nS	dát
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Země	země	k1gFnSc1	země
Kalaallitů	Kalaallit	k1gInPc2	Kalaallit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
země	zem	k1gFnPc1	zem
Gróňanů	Gróňan	k1gMnPc2	Gróňan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
výraz	výraz	k1gInSc1	výraz
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
převzat	převzat	k2eAgMnSc1d1	převzat
z	z	k7c2	z
dánského	dánský	k2eAgInSc2d1	dánský
"	"	kIx"	"
<g/>
Grø	Grø	k1gFnSc1	Grø
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zelená	zelený	k2eAgFnSc1d1	zelená
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
příběhu	příběh	k1gInSc2	příběh
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Ságy	sága	k1gFnSc2	sága
o	o	k7c6	o
Gróňanech	Gróňan	k1gMnPc6	Gróňan
(	(	kIx(	(
<g/>
Groenlendiga	Groenlendig	k1gMnSc2	Groenlendig
Saga	Sagus	k1gMnSc2	Sagus
<g/>
)	)	kIx)	)
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
exemplář	exemplář	k1gInSc1	exemplář
ovšem	ovšem	k9	ovšem
datuje	datovat	k5eAaImIp3nS	datovat
rok	rok	k1gInSc4	rok
1380	[number]	k4	1380
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
dal	dát	k5eAaPmAgMnS	dát
tomuto	tento	k3xDgInSc3	tento
ostrovu	ostrov	k1gInSc3	ostrov
Erik	Erik	k1gMnSc1	Erik
Rudý	rudý	k1gMnSc1	rudý
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
přilákat	přilákat	k5eAaPmF	přilákat
sem	sem	k6eAd1	sem
další	další	k2eAgMnPc4d1	další
osadníky	osadník	k1gMnPc4	osadník
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
985	[number]	k4	985
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přicházejí	přicházet	k5eAaImIp3nP	přicházet
předci	předek	k1gMnPc1	předek
prvních	první	k4xOgFnPc2	první
Inuitů	Inuit	k1gInPc2	Inuit
(	(	kIx(	(
<g/>
Eskymáci	Eskymák	k1gMnPc1	Eskymák
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Beringovu	Beringův	k2eAgFnSc4d1	Beringova
úžinu	úžina	k1gFnSc4	úžina
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
na	na	k7c4	na
Aljašku	Aljaška	k1gFnSc4	Aljaška
cca	cca	kA	cca
2500	[number]	k4	2500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
probíhá	probíhat	k5eAaImIp3nS	probíhat
první	první	k4xOgNnSc1	první
stěhování	stěhování	k1gNnSc1	stěhování
Indiánů	Indián	k1gMnPc2	Indián
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
(	(	kIx(	(
<g/>
kultura	kultura	k1gFnSc1	kultura
Sarqaq	Sarqaq	k1gMnSc1	Sarqaq
<g/>
)	)	kIx)	)
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
1000	[number]	k4	1000
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
indiánská	indiánský	k2eAgFnSc1d1	indiánská
kultura	kultura	k1gFnSc1	kultura
Dorset	Dorseta	k1gFnPc2	Dorseta
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
875	[number]	k4	875
objevuje	objevovat	k5eAaImIp3nS	objevovat
tento	tento	k3xDgInSc4	tento
ostrov	ostrov	k1gInSc4	ostrov
viking	viking	k1gMnSc1	viking
Gunnbjø	Gunnbjø	k1gMnSc1	Gunnbjø
Ulfsson	Ulfsson	k1gMnSc1	Ulfsson
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
ho	on	k3xPp3gMnSc4	on
Gunnbjø	Gunnbjø	k1gFnSc1	Gunnbjø
982	[number]	k4	982
Erik	Erik	k1gMnSc1	Erik
Rudý	rudý	k1gMnSc1	rudý
odplouvá	odplouvat	k5eAaImIp3nS	odplouvat
z	z	k7c2	z
Islandu	Island	k1gInSc2	Island
a	a	k8xC	a
přistává	přistávat	k5eAaImIp3nS	přistávat
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
986	[number]	k4	986
osídluje	osídlovat	k5eAaImIp3nS	osídlovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
okolí	okolí	k1gNnSc2	okolí
Brattahlidu	Brattahlid	k1gInSc2	Brattahlid
986	[number]	k4	986
-	-	kIx~	-
z	z	k7c2	z
25	[number]	k4	25
lodí	loď	k1gFnPc2	loď
vystěhovalců	vystěhovalec	k1gMnPc2	vystěhovalec
jich	on	k3xPp3gFnPc2	on
pouze	pouze	k6eAd1	pouze
14	[number]	k4	14
připlouvá	připlouvat	k5eAaImIp3nS	připlouvat
až	až	k9	až
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
asi	asi	k9	asi
o	o	k7c4	o
700	[number]	k4	700
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Grónska	Grónsko	k1gNnSc2	Grónsko
zachovány	zachován	k2eAgInPc1d1	zachován
ruiny	ruina	k1gFnPc4	ruina
obydlí	obydlí	k1gNnPc2	obydlí
a	a	k8xC	a
kostelů	kostel	k1gInPc2	kostel
1000	[number]	k4	1000
Leif	Leif	k1gMnSc1	Leif
Eriksson	Eriksson	k1gMnSc1	Eriksson
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Erika	Erik	k1gMnSc4	Erik
Rudého	rudý	k1gMnSc4	rudý
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
již	již	k6eAd1	již
jako	jako	k8xS	jako
křesťan	křesťan	k1gMnSc1	křesťan
a	a	k8xC	a
přiváží	přivážet	k5eAaImIp3nS	přivážet
s	s	k7c7	s
sebou	se	k3xPyFc7	se
i	i	k8xC	i
jednoho	jeden	k4xCgMnSc2	jeden
misionáře	misionář	k1gMnSc2	misionář
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
grónských	grónský	k2eAgMnPc2d1	grónský
vikingů	viking	k1gMnPc2	viking
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zbudován	zbudovat	k5eAaPmNgInS	zbudovat
první	první	k4xOgInSc1	první
kostel	kostel	k1gInSc1	kostel
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
objevil	objevit	k5eAaPmAgMnS	objevit
Leif	Leif	k1gMnSc1	Leif
Eriksson	Eriksson	k1gMnSc1	Eriksson
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Vinland	Vinland	k1gInSc1	Vinland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInPc1d1	obchodní
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Vinlandem	Vinlando	k1gNnSc7	Vinlando
trvají	trvat	k5eAaImIp3nP	trvat
až	až	k9	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
-	-	kIx~	-
stěhování	stěhování	k1gNnSc1	stěhování
Inuitů	Inuit	k1gInPc2	Inuit
z	z	k7c2	z
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Kanady	Kanada	k1gFnSc2	Kanada
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
(	(	kIx(	(
<g/>
kultura	kultura	k1gFnSc1	kultura
Thule	Thule	k1gFnSc1	Thule
<g/>
)	)	kIx)	)
1076	[number]	k4	1076
Adam	Adam	k1gMnSc1	Adam
Brémský	brémský	k2eAgMnSc1d1	brémský
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kronice	kronika	k1gFnSc6	kronika
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
Hamburk	Hamburk	k1gInSc4	Hamburk
první	první	k4xOgFnSc4	první
písemnou	písemný	k2eAgFnSc4d1	písemná
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
a	a	k8xC	a
christianizaci	christianizace	k1gFnSc4	christianizace
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nazývá	nazývat	k5eAaImIp3nS	nazývat
Gronland	Gronland	k1gInSc1	Gronland
1124	[number]	k4	1124
<g/>
-	-	kIx~	-
<g/>
1126	[number]	k4	1126
zřízena	zřízen	k2eAgFnSc1d1	zřízena
vlastní	vlastní	k2eAgFnSc1d1	vlastní
diecéze	diecéze	k1gFnSc1	diecéze
pro	pro	k7c4	pro
Grónsko	Grónsko	k1gNnSc4	Grónsko
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c4	v
Gardar	Gardar	k1gInSc4	Gardar
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgMnSc6d1	dnešní
Igaliku	Igalik	k1gMnSc6	Igalik
1350	[number]	k4	1350
Ivar	Ivar	k1gMnSc1	Ivar
Bardarsson	Bardarsson	k1gMnSc1	Bardarsson
<g/>
,	,	kIx,	,
islandský	islandský	k2eAgMnSc1d1	islandský
církevní	církevní	k2eAgMnSc1d1	církevní
činitel	činitel	k1gMnSc1	činitel
<g/>
,	,	kIx,	,
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
osídlení	osídlení	k1gNnSc1	osídlení
na	na	k7c6	na
západě	západ	k1gInSc6	západ
byla	být	k5eAaImAgFnS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
území	území	k1gNnPc2	území
Norů	Nor	k1gMnPc2	Nor
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
Inuité	Inuitý	k2eAgInPc1d1	Inuitý
a	a	k8xC	a
zachraňují	zachraňovat	k5eAaImIp3nP	zachraňovat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
1408	[number]	k4	1408
poslední	poslední	k2eAgFnSc1d1	poslední
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Norech	Nor	k1gMnPc6	Nor
<g/>
,	,	kIx,	,
svatba	svatba	k1gFnSc1	svatba
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
v	v	k7c4	v
Hvalsey	Hvalsea	k1gFnPc4	Hvalsea
<g/>
.	.	kIx.	.
</s>
<s>
Kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
a	a	k8xC	a
Islandem	Island	k1gInSc7	Island
se	se	k3xPyFc4	se
přerušuje	přerušovat	k5eAaImIp3nS	přerušovat
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
nedostatek	nedostatek	k1gInSc1	nedostatek
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
cca	cca	kA	cca
<g/>
.	.	kIx.	.
1550	[number]	k4	1550
mizí	mizet	k5eAaImIp3nS	mizet
poslední	poslední	k2eAgFnSc1d1	poslední
norská	norský	k2eAgFnSc1d1	norská
osada	osada	k1gFnSc1	osada
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Normané	Norman	k1gMnPc1	Norman
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
či	či	k8xC	či
se	se	k3xPyFc4	se
smísili	smísit	k5eAaPmAgMnP	smísit
s	s	k7c7	s
Inuity	Inuita	k1gMnPc7	Inuita
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
normanských	normanský	k2eAgMnPc2d1	normanský
kolonistů	kolonista	k1gMnPc2	kolonista
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přispělo	přispět	k5eAaPmAgNnS	přispět
také	také	k9	také
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zhoršující	zhoršující	k2eAgNnSc1d1	zhoršující
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1000	[number]	k4	1000
<g/>
-	-	kIx~	-
<g/>
1500	[number]	k4	1500
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
globálnímu	globální	k2eAgNnSc3d1	globální
ochlazování	ochlazování	k1gNnSc3	ochlazování
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
volen	volen	k2eAgMnSc1d1	volen
biskup	biskup	k1gMnSc1	biskup
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pochopitelně	pochopitelně	k6eAd1	pochopitelně
trvale	trvale	k6eAd1	trvale
zůstával	zůstávat	k5eAaImAgInS	zůstávat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
1721	[number]	k4	1721
nové	nový	k2eAgNnSc4d1	nové
evropské	evropský	k2eAgNnSc4d1	Evropské
osídlení	osídlení	k1gNnSc4	osídlení
Grónska	Grónsko	k1gNnSc2	Grónsko
od	od	k7c2	od
norského	norský	k2eAgMnSc2d1	norský
misionáře	misionář	k1gMnSc2	misionář
Hanse	Hans	k1gMnSc2	Hans
Egede	Eged	k1gInSc5	Eged
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
potomek	potomek	k1gMnSc1	potomek
českých	český	k2eAgMnPc2d1	český
vystěhovalců	vystěhovalec	k1gMnPc2	vystěhovalec
Jakub	Jakub	k1gMnSc1	Jakub
Severin	Severin	k1gMnSc1	Severin
zakládá	zakládat	k5eAaImIp3nS	zakládat
tři	tři	k4xCgFnPc4	tři
obchodní	obchodní	k2eAgFnPc4d1	obchodní
osady	osada	k1gFnPc4	osada
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Jakobshaven	Jakobshaven	k2eAgInSc4d1	Jakobshaven
-	-	kIx~	-
Jakubův	Jakubův	k2eAgInSc4d1	Jakubův
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
město	město	k1gNnSc1	město
Ilulissat	Ilulissat	k1gFnSc2	Ilulissat
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
hlavně	hlavně	k6eAd1	hlavně
holandští	holandský	k2eAgMnPc1d1	holandský
<g/>
,	,	kIx,	,
dánští	dánský	k2eAgMnPc1d1	dánský
a	a	k8xC	a
němečtí	německý	k2eAgMnPc1d1	německý
velrybáři	velrybář	k1gMnPc1	velrybář
opakovaně	opakovaně	k6eAd1	opakovaně
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
Grónsko	Grónsko	k1gNnSc4	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plavbách	plavba	k1gFnPc6	plavba
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
bohatne	bohatnout	k5eAaImIp3nS	bohatnout
německé	německý	k2eAgNnSc1d1	německé
město	město	k1gNnSc1	město
Flensburg	Flensburg	k1gInSc1	Flensburg
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
dánský	dánský	k2eAgInSc1d1	dánský
přístav	přístav	k1gInSc1	přístav
<g/>
)	)	kIx)	)
1814	[number]	k4	1814
Kielský	kielský	k2eAgInSc1d1	kielský
mír	mír	k1gInSc1	mír
<g/>
:	:	kIx,	:
Grónsko	Grónsko	k1gNnSc1	Grónsko
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
pod	pod	k7c4	pod
dánskou	dánský	k2eAgFnSc4d1	dánská
nadvládu	nadvláda	k1gFnSc4	nadvláda
1931	[number]	k4	1931
Norsko	Norsko	k1gNnSc1	Norsko
okupuje	okupovat	k5eAaBmIp3nS	okupovat
neobydlenou	obydlený	k2eNgFnSc4d1	neobydlená
část	část	k1gFnSc4	část
východního	východní	k2eAgNnSc2d1	východní
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
zde	zde	k6eAd1	zde
Zemi	zem	k1gFnSc4	zem
Erika	Erik	k1gMnSc4	Erik
Rudého	rudý	k1gMnSc4	rudý
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Nor	k1gMnPc1	Nor
oblast	oblast	k1gFnSc4	oblast
vyklidili	vyklidit	k5eAaPmAgMnP	vyklidit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zřizují	zřizovat	k5eAaImIp3nP	zřizovat
USA	USA	kA	USA
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
svou	svůj	k3xOyFgFnSc4	svůj
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
(	(	kIx(	(
<g/>
Thule	Thule	k1gNnSc1	Thule
Air	Air	k1gFnSc3	Air
Base	basa	k1gFnSc3	basa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
je	být	k5eAaImIp3nS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
Dánskem	Dánsko	k1gNnSc7	Dánsko
a	a	k8xC	a
USA	USA	kA	USA
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
obraně	obrana	k1gFnSc6	obrana
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
Grónsko	Grónsko	k1gNnSc1	Grónsko
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
součástí	součást	k1gFnSc7	součást
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
Grónsko	Grónsko	k1gNnSc1	Grónsko
dostává	dostávat	k5eAaImIp3nS	dostávat
autonomii	autonomie	k1gFnSc4	autonomie
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgFnSc1d1	vlastní
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
parlament	parlament	k1gInSc1	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
Referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
výstupu	výstup	k1gInSc6	výstup
z	z	k7c2	z
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
1985	[number]	k4	1985
Grónsko	Grónsko	k1gNnSc4	Grónsko
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
ES	es	k1gNnPc2	es
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
statut	statut	k1gInSc4	statut
zámořského	zámořský	k2eAgNnSc2d1	zámořské
teritoria	teritorium	k1gNnSc2	teritorium
ES	es	k1gNnSc2	es
s	s	k7c7	s
výhodami	výhoda	k1gFnPc7	výhoda
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
se	s	k7c7	s
75,5	[number]	k4	75,5
%	%	kIx~	%
grónských	grónský	k2eAgMnPc2d1	grónský
voličů	volič	k1gMnPc2	volič
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
postupně	postupně	k6eAd1	postupně
přebírat	přebírat	k5eAaImF	přebírat
od	od	k7c2	od
Dánska	Dánsko	k1gNnSc2	Dánsko
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
soudnictvím	soudnictví	k1gNnSc7	soudnictví
<g/>
,	,	kIx,	,
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
vězeňstvím	vězeňství	k1gNnSc7	vězeňství
a	a	k8xC	a
přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
Grónsko	Grónsko	k1gNnSc1	Grónsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Kanadským	kanadský	k2eAgInSc7d1	kanadský
štítem	štít	k1gInSc7	štít
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tento	tento	k3xDgMnSc1	tento
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zejména	zejména	k9	zejména
z	z	k7c2	z
ruly	rula	k1gFnSc2	rula
a	a	k8xC	a
žuly	žula	k1gFnPc1	žula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prošly	projít	k5eAaPmAgFnP	projít
vrásněním	vrásnění	k1gNnSc7	vrásnění
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
proměnami	proměna	k1gFnPc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kambricko-silurský	kambrickoilurský	k2eAgInSc1d1	kambricko-silurský
vápenec	vápenec	k1gInSc1	vápenec
a	a	k8xC	a
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
určit	určit	k5eAaPmF	určit
části	část	k1gFnSc2	část
kaledonského	kaledonský	k2eAgNnSc2d1	kaledonské
zvrásněného	zvrásněný	k2eAgNnSc2d1	zvrásněné
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc4d1	podobný
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc1	jaký
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Ellesmerova	Ellesmerův	k2eAgInSc2d1	Ellesmerův
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
Špicberkách	Špicberky	k1gInPc6	Špicberky
<g/>
,	,	kIx,	,
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
další	další	k2eAgNnSc4d1	další
velké	velký	k2eAgNnSc4d1	velké
sedimentační	sedimentační	k2eAgNnSc4d1	sedimentační
území	území	k1gNnSc4	území
s	s	k7c7	s
usazeninami	usazenina	k1gFnPc7	usazenina
z	z	k7c2	z
karbonu	karbon	k1gInSc2	karbon
<g/>
,	,	kIx,	,
triasu	trias	k1gInSc2	trias
<g/>
,	,	kIx,	,
jury	jura	k1gFnSc2	jura
<g/>
,	,	kIx,	,
křídy	křída	k1gFnSc2	křída
a	a	k8xC	a
třetihor	třetihory	k1gFnPc2	třetihory
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Grónsku	Grónsko	k1gNnSc6	Grónsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bohaté	bohatý	k2eAgFnSc2d1	bohatá
zásoby	zásoba	k1gFnSc2	zásoba
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
objeveno	objevit	k5eAaPmNgNnS	objevit
kolem	kolem	k7c2	kolem
500	[number]	k4	500
různých	různý	k2eAgInPc2d1	různý
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
a	a	k8xC	a
popsáno	popsat	k5eAaPmNgNnS	popsat
právě	právě	k6eAd1	právě
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
minerály	minerál	k1gInPc4	minerál
nejbohatší	bohatý	k2eAgNnSc4d3	nejbohatší
území	území	k1gNnSc4	území
je	být	k5eAaImIp3nS	být
Ilimmaasaq	Ilimmaasaq	k1gFnSc7	Ilimmaasaq
nacházející	nacházející	k2eAgFnSc7d1	nacházející
se	s	k7c7	s
11	[number]	k4	11
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
jihogrónského	jihogrónský	k2eAgNnSc2d1	jihogrónský
města	město	k1gNnSc2	město
Narsaq	Narsaq	k1gFnSc2	Narsaq
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
na	na	k7c4	na
200	[number]	k4	200
různých	různý	k2eAgInPc2d1	různý
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
křemičitany	křemičitan	k1gInPc4	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
zdejších	zdejší	k2eAgInPc2d1	zdejší
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
naujakasit	naujakasit	k5eAaPmF	naujakasit
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
doposud	doposud	k6eAd1	doposud
objeveno	objevit	k5eAaPmNgNnS	objevit
jinde	jinde	k6eAd1	jinde
než	než	k8xS	než
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
minerálů	minerál	k1gInPc2	minerál
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
sorensenit	sorensenit	k5eAaImF	sorensenit
<g/>
,	,	kIx,	,
tundrit	tundrit	k1gInSc4	tundrit
<g/>
,	,	kIx,	,
semenovit	semenovit	k5eAaPmF	semenovit
<g/>
,	,	kIx,	,
villiaumit	villiaumit	k5eAaPmF	villiaumit
a	a	k8xC	a
zejména	zejména	k9	zejména
tuttupit	tuttupit	k5eAaPmF	tuttupit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
zdobný	zdobný	k2eAgInSc1d1	zdobný
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Grónské	grónský	k2eAgNnSc1d1	grónské
nerostné	nerostný	k2eAgNnSc1d1	nerostné
bohatství	bohatství	k1gNnSc1	bohatství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hospodářsky	hospodářsky	k6eAd1	hospodářsky
využíváno	využíván	k2eAgNnSc1d1	využíváno
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
hlavně	hlavně	k9	hlavně
kryolit	kryolit	k1gInSc1	kryolit
<g/>
,	,	kIx,	,
galenit	galenit	k1gInSc1	galenit
neboli	neboli	k8xC	neboli
leštěnec	leštěnec	k1gInSc1	leštěnec
olověný	olověný	k2eAgInSc1d1	olověný
<g/>
,	,	kIx,	,
sfalerit	sfalerit	k1gInSc1	sfalerit
neboli	neboli	k8xC	neboli
blejno	blejno	k1gNnSc1	blejno
zinkové	zinkový	k2eAgNnSc1d1	zinkové
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
a	a	k8xC	a
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
obklopující	obklopující	k2eAgFnSc2d1	obklopující
grónské	grónský	k2eAgFnSc2d1	grónská
pobřeží	pobřeží	k1gNnSc3	pobřeží
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
zásoby	zásoba	k1gFnPc1	zásoba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgMnPc4	jenž
probíhaly	probíhat	k5eAaImAgInP	probíhat
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
grónskou	grónský	k2eAgFnSc7d1	grónská
samosprávou	samospráva	k1gFnSc7	samospráva
a	a	k8xC	a
dánskou	dánský	k2eAgFnSc7d1	dánská
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ustanovení	ustanovení	k1gNnSc6	ustanovení
autonomie	autonomie	k1gFnSc2	autonomie
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
Ustanovení	ustanovení	k1gNnSc1	ustanovení
o	o	k7c6	o
nerostném	nerostný	k2eAgNnSc6d1	nerostné
bohatství	bohatství	k1gNnSc6	bohatství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bližší	blízký	k2eAgFnPc4d2	bližší
podmínky	podmínka	k1gFnPc4	podmínka
částečně	částečně	k6eAd1	částečně
v	v	k7c6	v
Zákoně	zákon	k1gInSc6	zákon
o	o	k7c4	o
autonomii	autonomie	k1gFnSc4	autonomie
(	(	kIx(	(
<g/>
Lov	lov	k1gInSc1	lov
om	om	k?	om
Grø	Grø	k1gMnSc5	Grø
hjemmestyre	hjemmestyr	k1gMnSc5	hjemmestyr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
revidovaném	revidovaný	k2eAgInSc6d1	revidovaný
Zákoně	zákon	k1gInSc6	zákon
o	o	k7c6	o
zásobách	zásoba	k1gFnPc6	zásoba
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
ustanovení	ustanovení	k1gNnSc1	ustanovení
těchto	tento	k3xDgInPc2	tento
zákonů	zákon	k1gInPc2	zákon
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
grónský	grónský	k2eAgInSc1d1	grónský
lid	lid	k1gInSc1	lid
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
nakládat	nakládat	k5eAaImF	nakládat
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
budoucím	budoucí	k2eAgNnSc6d1	budoucí
využívání	využívání	k1gNnSc6	využívání
těchto	tento	k3xDgInPc2	tento
zdrojů	zdroj	k1gInPc2	zdroj
má	mít	k5eAaImIp3nS	mít
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
společný	společný	k2eAgInSc4d1	společný
grónsko-dánský	grónskoánský	k2eAgInSc4d1	grónsko-dánský
výbor	výbor	k1gInSc4	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
vydávány	vydáván	k2eAgFnPc1d1	vydávána
koncese	koncese	k1gFnPc1	koncese
na	na	k7c4	na
pokusné	pokusný	k2eAgInPc4d1	pokusný
vrty	vrt	k1gInPc4	vrt
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Grónský	grónský	k2eAgInSc1d1	grónský
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Grónský	grónský	k2eAgInSc1d1	grónský
ledovec	ledovec	k1gInSc1	ledovec
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
elevace	elevace	k1gFnPc4	elevace
<g/>
,	,	kIx,	,
kupole	kupole	k1gFnSc1	kupole
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
styk	styk	k1gInSc4	styk
je	být	k5eAaImIp3nS	být
kolizní	kolizní	k2eAgNnSc1d1	kolizní
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
ledovcové	ledovcový	k2eAgInPc4d1	ledovcový
proudy	proud	k1gInPc4	proud
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
neobvyklou	obvyklý	k2eNgFnSc7d1	neobvyklá
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
grónského	grónský	k2eAgNnSc2d1	grónské
zalednění	zalednění	k1gNnSc2	zalednění
je	být	k5eAaImIp3nS	být
1,79	[number]	k4	1,79
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
ledu	led	k1gInSc2	led
je	být	k5eAaImIp3nS	být
2,6	[number]	k4	2,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ledový	ledový	k2eAgInSc1d1	ledový
vrchol	vrchol	k1gInSc4	vrchol
severní	severní	k2eAgInSc4d1	severní
(	(	kIx(	(
<g/>
větší	veliký	k2eAgInSc4d2	veliký
<g/>
)	)	kIx)	)
elevace	elevace	k1gFnSc1	elevace
je	být	k5eAaImIp3nS	být
3270	[number]	k4	3270
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
skalní	skalní	k2eAgInSc1d1	skalní
bod	bod	k1gInSc1	bod
Gunnbjörnskjöld	Gunnbjörnskjöld	k1gInSc4	Gunnbjörnskjöld
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
3734	[number]	k4	3734
m.	m.	k?	m.
Největší	veliký	k2eAgFnSc1d3	veliký
známá	známý	k2eAgFnSc1d1	známá
mocnost	mocnost	k1gFnSc1	mocnost
ledovce	ledovec	k1gInSc2	ledovec
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
3410	[number]	k4	3410
m.	m.	k?	m.
Stáří	stář	k1gFnPc2	stář
grónského	grónský	k2eAgNnSc2d1	grónské
zalednění	zalednění	k1gNnSc2	zalednění
sahá	sahat	k5eAaImIp3nS	sahat
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
třetihor	třetihory	k1gFnPc2	třetihory
(	(	kIx(	(
<g/>
miocén	miocén	k1gInSc1	miocén
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
zpátky	zpátky	k6eAd1	zpátky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
samotný	samotný	k2eAgInSc1d1	samotný
led	led	k1gInSc1	led
díky	díky	k7c3	díky
koloběhu	koloběh	k1gInSc3	koloběh
tak	tak	k6eAd1	tak
starý	starý	k2eAgMnSc1d1	starý
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnPc1d3	nejstarší
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc1d1	stará
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
letech	let	k1gInPc6	let
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorován	k2eAgNnSc1d1	pozorováno
zrychlení	zrychlení	k1gNnSc1	zrychlení
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
tání	tání	k1gNnSc2	tání
grónských	grónský	k2eAgInPc2d1	grónský
ledovců	ledovec	k1gInPc2	ledovec
spojené	spojený	k2eAgNnSc1d1	spojené
se	s	k7c7	s
ztenčováním	ztenčování	k1gNnSc7	ztenčování
jejich	jejich	k3xOp3gFnSc2	jejich
spodní	spodní	k2eAgFnSc2d1	spodní
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
povrchové	povrchový	k2eAgNnSc1d1	povrchové
tání	tání	k1gNnSc1	tání
grónských	grónský	k2eAgInPc2d1	grónský
ledovců	ledovec	k1gInPc2	ledovec
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledová	ledový	k2eAgFnSc1d1	ledová
masa	masa	k1gFnSc1	masa
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
až	až	k9	až
2007	[number]	k4	2007
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
asi	asi	k9	asi
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
amerického	americký	k2eAgInSc2d1	americký
Národního	národní	k2eAgInSc2d1	národní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
vesmír	vesmír	k1gInSc4	vesmír
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
Geoscience	Geoscienka	k1gFnSc6	Geoscienka
ukázala	ukázat	k5eAaPmAgNnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledovce	ledovec	k1gInPc1	ledovec
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Grónsku	Grónsko	k1gNnSc6	Grónsko
tají	tajit	k5eAaImIp3nP	tajit
stokrát	stokrát	k6eAd1	stokrát
rychleji	rychle	k6eAd2	rychle
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
oceánu	oceán	k1gInSc2	oceán
než	než	k8xS	než
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Oceánská	oceánský	k2eAgFnSc1d1	oceánská
voda	voda	k1gFnSc1	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
3	[number]	k4	3
<g/>
°	°	k?	°
Celsia	Celsius	k1gMnSc4	Celsius
může	moct	k5eAaImIp3nS	moct
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
ledovec	ledovec	k1gInSc1	ledovec
rychlostí	rychlost	k1gFnPc2	rychlost
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Grónská	grónský	k2eAgFnSc1d1	grónská
fauna	fauna	k1gFnSc1	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
zástupce	zástupce	k1gMnSc1	zástupce
grónské	grónský	k2eAgFnSc2d1	grónská
fauny	fauna	k1gFnSc2	fauna
je	být	k5eAaImIp3nS	být
lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
(	(	kIx(	(
<g/>
grónsky	grónsky	k6eAd1	grónsky
nanoq	nanoq	k?	nanoq
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
severu	sever	k1gInSc6	sever
a	a	k8xC	a
v	v	k7c6	v
Severogrónském	Severogrónský	k2eAgInSc6d1	Severogrónský
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krách	kra	k1gFnPc6	kra
unášených	unášený	k2eAgInPc2d1	unášený
Východogrónským	Východogrónský	k2eAgInSc7d1	Východogrónský
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
tuleňů	tuleň	k1gMnPc2	tuleň
až	až	k9	až
na	na	k7c4	na
jih	jih	k1gInSc4	jih
(	(	kIx(	(
<g/>
Nanortalik	Nanortalik	k1gMnSc1	Nanortalik
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
prouděním	proudění	k1gNnSc7	proudění
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
až	až	k9	až
do	do	k7c2	do
Upernaviku	Upernavik	k1gInSc2	Upernavik
v	v	k7c6	v
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgMnSc1d3	veliký
grónský	grónský	k2eAgMnSc1d1	grónský
savec	savec	k1gMnSc1	savec
je	být	k5eAaImIp3nS	být
tur	tur	k1gMnSc1	tur
pižmový	pižmový	k2eAgMnSc1d1	pižmový
(	(	kIx(	(
<g/>
pižmoň	pižmoň	k1gMnSc1	pižmoň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
jen	jen	k9	jen
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
jedinců	jedinec	k1gMnPc2	jedinec
bylo	být	k5eAaImAgNnS	být
ale	ale	k8xC	ale
nasazeno	nasadit	k5eAaPmNgNnS	nasadit
i	i	k9	i
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Kangerlussuaq	Kangerlussuaq	k1gFnSc6	Kangerlussuaq
<g/>
,	,	kIx,	,
Ivittuut	Ivittuut	k2eAgMnSc1d1	Ivittuut
nebo	nebo	k8xC	nebo
u	u	k7c2	u
Upernavik	Upernavika	k1gFnPc2	Upernavika
a	a	k8xC	a
rozmnožili	rozmnožit	k5eAaPmAgMnP	rozmnožit
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Sobi	sob	k1gMnPc1	sob
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
stádech	stádo	k1gNnPc6	stádo
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgMnPc1d2	menší
savci	savec	k1gMnPc1	savec
jako	jako	k9	jako
hranostaj	hranostaj	k1gMnSc1	hranostaj
či	či	k8xC	či
lumík	lumík	k1gMnSc1	lumík
žijí	žít	k5eAaImIp3nP	žít
jen	jen	k9	jen
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgMnSc1d1	polární
vlk	vlk	k1gMnSc1	vlk
zase	zase	k9	zase
jen	jen	k9	jen
na	na	k7c6	na
nejvzdálenějším	vzdálený	k2eAgInSc6d3	nejvzdálenější
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozšíření	rozšíření	k1gNnSc2	rozšíření
jsou	být	k5eAaImIp3nP	být
sněžný	sněžný	k2eAgMnSc1d1	sněžný
zajíc	zajíc	k1gMnSc1	zajíc
a	a	k8xC	a
polární	polární	k2eAgFnSc1d1	polární
liška	liška	k1gFnSc1	liška
<g/>
.	.	kIx.	.
</s>
<s>
Druhově	druhově	k6eAd1	druhově
bohaté	bohatý	k2eAgInPc1d1	bohatý
je	být	k5eAaImIp3nS	být
obzvláště	obzvláště	k6eAd1	obzvláště
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
velryb	velryba	k1gFnPc2	velryba
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
narval	narvat	k5eAaPmAgMnS	narvat
jednorohý	jednorohý	k2eAgMnSc1d1	jednorohý
<g/>
,	,	kIx,	,
velryba	velryba	k1gFnSc1	velryba
grónská	grónský	k2eAgFnSc1d1	grónská
<g/>
,	,	kIx,	,
běluhy	běluha	k1gFnPc1	běluha
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
dále	daleko	k6eAd2	daleko
mroži	mrož	k1gMnPc1	mrož
a	a	k8xC	a
tuleni	tuleň	k1gMnPc1	tuleň
(	(	kIx(	(
<g/>
tuleň	tuleň	k1gMnSc1	tuleň
grónský	grónský	k2eAgMnSc1d1	grónský
<g/>
,	,	kIx,	,
tuleň	tuleň	k1gMnSc1	tuleň
vousatý	vousatý	k2eAgMnSc1d1	vousatý
<g/>
,	,	kIx,	,
tuleň	tuleň	k1gMnSc1	tuleň
kroužkovaný	kroužkovaný	k2eAgMnSc1d1	kroužkovaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inuité	Inuitý	k2eAgNnSc1d1	Inuité
za	za	k7c4	za
mořské	mořský	k2eAgMnPc4d1	mořský
živočichy	živočich	k1gMnPc4	živočich
považují	považovat	k5eAaImIp3nP	považovat
i	i	k8xC	i
lední	lední	k2eAgMnPc4d1	lední
medvědy	medvěd	k1gMnPc4	medvěd
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
převážnou	převážný	k2eAgFnSc4d1	převážná
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
prožijí	prožít	k5eAaPmIp3nP	prožít
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
nebo	nebo	k8xC	nebo
na	na	k7c6	na
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgInSc1d1	bohatý
je	být	k5eAaImIp3nS	být
i	i	k9	i
ptačí	ptačí	k2eAgInSc1d1	ptačí
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
ptačích	ptačí	k2eAgFnPc6d1	ptačí
skalách	skála	k1gFnPc6	skála
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
(	(	kIx(	(
<g/>
Upernavik	Upernavik	k1gMnSc1	Upernavik
<g/>
,	,	kIx,	,
Qaanaaq	Qaanaaq	k1gMnSc1	Qaanaaq
a	a	k8xC	a
Ittoqqortoormiit	Ittoqqortoormiit	k1gMnSc1	Ittoqqortoormiit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgMnSc1d3	nejrozšířenější
jsou	být	k5eAaImIp3nP	být
havrani	havran	k1gMnPc1	havran
<g/>
,	,	kIx,	,
alkoun	alkoun	k1gMnSc1	alkoun
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
racek	racek	k1gMnSc1	racek
tříprstý	tříprstý	k2eAgMnSc1d1	tříprstý
<g/>
,	,	kIx,	,
sněhule	sněhule	k1gFnSc1	sněhule
severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
kajky	kajka	k1gFnSc2	kajka
<g/>
;	;	kIx,	;
na	na	k7c6	na
ptačích	ptačí	k2eAgFnPc6d1	ptačí
skalách	skála	k1gFnPc6	skála
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
též	též	k9	též
kormoráni	kormorán	k1gMnPc1	kormorán
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
i	i	k9	i
alkoun	alkoun	k1gMnSc1	alkoun
tlustozobý	tlustozobý	k2eAgMnSc1d1	tlustozobý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
velke	velke	k1gNnSc4	velke
kolonie	kolonie	k1gFnSc2	kolonie
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Upernaviku	Upernavik	k1gInSc2	Upernavik
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
distriktech	distrikt	k1gInPc6	distrikt
Qaanaaq	Qaanaaq	k1gFnSc2	Qaanaaq
<g/>
,	,	kIx,	,
Ilulissat	Ilulissat	k1gMnSc1	Ilulissat
<g/>
,	,	kIx,	,
Maniitsoq	Maniitsoq	k1gMnSc1	Maniitsoq
<g/>
,	,	kIx,	,
Nuuk	Nuuk	k1gMnSc1	Nuuk
<g/>
,	,	kIx,	,
Ivittuut	Ivittuut	k1gMnSc1	Ivittuut
<g/>
,	,	kIx,	,
Ittoqqortoormiit	Ittoqqortoormiit	k1gMnSc1	Ittoqqortoormiit
a	a	k8xC	a
na	na	k7c6	na
souostroví	souostroví	k1gNnSc6	souostroví
Ydre	Ydr	k1gInSc2	Ydr
Kitsitsut	Kitsitsut	k1gInSc1	Kitsitsut
<g/>
.	.	kIx.	.
</s>
<s>
Alkoun	alkoun	k1gMnSc1	alkoun
úzkozobý	úzkozobý	k2eAgMnSc1d1	úzkozobý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
relativně	relativně	k6eAd1	relativně
málo	málo	k4c1	málo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
pračích	pračí	k2eAgFnPc6d1	pračí
skálách	skála	k1gFnPc6	skála
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Papuchalk	Papuchalk	k6eAd1	Papuchalk
ploskozobý	ploskozobý	k2eAgInSc1d1	ploskozobý
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
severoarktické	severoarktický	k2eAgInPc4d1	severoarktický
poměry	poměr	k1gInPc4	poměr
spíše	spíše	k9	spíše
malé	malý	k2eAgFnPc4d1	malá
kolonie	kolonie	k1gFnPc4	kolonie
u	u	k7c2	u
Aasiaat	Aasiaat	k1gInSc1	Aasiaat
<g/>
,	,	kIx,	,
Upernavik	Upernavik	k1gInSc1	Upernavik
<g/>
,	,	kIx,	,
Nuuk	Nuuk	k1gInSc1	Nuuk
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ydre	Ydre	k1gFnSc6	Ydre
Kitsitsut	Kitsitsut	k1gMnSc1	Kitsitsut
(	(	kIx(	(
<g/>
Qaqortoq	Qaqortoq	k1gMnSc1	Qaqortoq
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
u	u	k7c2	u
Nanortalik	Nanortalika	k1gFnPc2	Nanortalika
<g/>
,	,	kIx,	,
Ittoqqortoormiit	Ittoqqortoormiita	k1gFnPc2	Ittoqqortoormiita
a	a	k8xC	a
Qaanaaq	Qaanaaq	k1gFnPc2	Qaanaaq
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Grönne	Grönn	k1gInSc5	Grönn
Ejland	Ejlanda	k1gFnPc2	Ejlanda
se	se	k3xPyFc4	se
mimo	mimo	k6eAd1	mimo
alkounů	alkoun	k1gMnPc2	alkoun
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
největší	veliký	k2eAgFnSc1d3	veliký
kolonie	kolonie	k1gFnSc1	kolonie
rybáka	rybák	k1gMnSc2	rybák
dlouhoocasého	dlouhoocasý	k2eAgMnSc2d1	dlouhoocasý
a	a	k8xC	a
také	také	k9	také
lyskonoha	lyskonoha	k1gFnSc1	lyskonoha
a	a	k8xC	a
kajky	kajka	k1gFnSc2	kajka
královské	královský	k2eAgFnSc2d1	královská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
hus	husa	k1gFnPc2	husa
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
berneška	berneška	k1gFnSc1	berneška
bělolící	bělolící	k2eAgFnSc1d1	bělolící
<g/>
,	,	kIx,	,
husa	husa	k1gFnSc1	husa
sněžní	sněžní	k2eAgFnSc1d1	sněžní
<g/>
,	,	kIx,	,
husa	husa	k1gFnSc1	husa
běločelá	běločelý	k2eAgFnSc1d1	běločelá
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
mořské	mořský	k2eAgFnPc1d1	mořská
kachny	kachna	k1gFnPc1	kachna
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sněžné	sněžný	k2eAgFnPc1d1	sněžná
sovy	sova	k1gFnPc1	sova
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
a	a	k8xC	a
severovýchodním	severovýchodní	k2eAgNnSc6d1	severovýchodní
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
severu	sever	k1gInSc6	sever
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
racek	racek	k1gMnSc1	racek
růžový	růžový	k2eAgMnSc1d1	růžový
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
sokol	sokol	k1gMnSc1	sokol
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
orel	orel	k1gMnSc1	orel
mořský	mořský	k2eAgInSc4d1	mořský
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
zejména	zejména	k9	zejména
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
sídlí	sídlet	k5eAaImIp3nS	sídlet
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Landsting	Landsting	k1gInSc1	Landsting
<g/>
/	/	kIx~	/
<g/>
Inatsisartut	Inatsisartut	k1gInSc1	Inatsisartut
<g/>
)	)	kIx)	)
volený	volený	k2eAgMnSc1d1	volený
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
31	[number]	k4	31
poslanců	poslanec	k1gMnPc2	poslanec
volí	volit	k5eAaImIp3nS	volit
premiéra	premiér	k1gMnSc4	premiér
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
(	(	kIx(	(
<g/>
Selvstyre	Selvstyr	k1gMnSc5	Selvstyr
<g/>
/	/	kIx~	/
<g/>
Naalakkersuisut	Naalakkersuisut	k1gInSc4	Naalakkersuisut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
řídí	řídit	k5eAaImIp3nS	řídit
veškeré	veškerý	k3xTgFnPc4	veškerý
záležitosti	záležitost	k1gFnPc4	záležitost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
grónské	grónský	k2eAgFnSc2d1	grónská
domácí	domácí	k2eAgFnSc2d1	domácí
politiky	politika	k1gFnSc2	politika
včetně	včetně	k7c2	včetně
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
<g/>
.	.	kIx.	.
</s>
<s>
Dánsku	Dánsko	k1gNnSc3	Dánsko
přísluší	příslušet	k5eAaImIp3nP	příslušet
jen	jen	k9	jen
obrana	obrana	k1gFnSc1	obrana
země	země	k1gFnSc1	země
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ovšem	ovšem	k9	ovšem
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
aspektů	aspekt	k1gInPc2	aspekt
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
spravují	spravovat	k5eAaImIp3nP	spravovat
sami	sám	k3xTgMnPc1	sám
Gróňané	Gróňan	k1gMnPc1	Gróňan
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
Inuity	Inuita	k1gMnPc7	Inuita
či	či	k8xC	či
s	s	k7c7	s
nečlenskými	členský	k2eNgInPc7d1	nečlenský
státy	stát	k1gInPc7	stát
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
dánský	dánský	k2eAgMnSc1d1	dánský
monarcha	monarcha	k1gMnSc1	monarcha
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Margrethe	Margreth	k1gFnSc2	Margreth
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Premiérem	premiér	k1gMnSc7	premiér
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
předseda	předseda	k1gMnSc1	předseda
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
grónském	grónský	k2eAgInSc6d1	grónský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
teritorium	teritorium	k1gNnSc4	teritorium
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
o	o	k7c6	o
vyčlenění	vyčlenění	k1gNnSc6	vyčlenění
se	se	k3xPyFc4	se
z	z	k7c2	z
unijního	unijní	k2eAgNnSc2d1	unijní
území	území	k1gNnSc2	území
a	a	k8xC	a
Grónsko	Grónsko	k1gNnSc4	Grónsko
tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
přestalo	přestat	k5eAaPmAgNnS	přestat
být	být	k5eAaImF	být
přímou	přímý	k2eAgFnSc7d1	přímá
součástí	součást	k1gFnSc7	součást
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
zámořským	zámořský	k2eAgNnSc7d1	zámořské
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
dánskému	dánský	k2eAgNnSc3d1	dánské
občanství	občanství	k1gNnSc3	občanství
<g/>
,	,	kIx,	,
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
občany	občan	k1gMnPc4	občan
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
Grónsko	Grónsko	k1gNnSc1	Grónsko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
ovšem	ovšem	k9	ovšem
zavedena	zaveden	k2eAgFnSc1d1	zavedena
samospráva	samospráva	k1gFnSc1	samospráva
a	a	k8xC	a
v	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
národním	národní	k2eAgNnSc6d1	národní
referendu	referendum	k1gNnSc6	referendum
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
občané	občan	k1gMnPc1	občan
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
pro	pro	k7c4	pro
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
ES	ES	kA	ES
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
právně	právně	k6eAd1	právně
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Grónská	grónský	k2eAgFnSc1d1	grónská
vláda	vláda	k1gFnSc1	vláda
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
vyjednala	vyjednat	k5eAaPmAgFnS	vyjednat
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
daňových	daňový	k2eAgFnPc6d1	daňová
úlevách	úleva	k1gFnPc6	úleva
a	a	k8xC	a
právech	právo	k1gNnPc6	právo
na	na	k7c4	na
lov	lov	k1gInSc4	lov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
Grónsko	Grónsko	k1gNnSc1	Grónsko
členem	člen	k1gInSc7	člen
řady	řada	k1gFnSc2	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
Severské	severský	k2eAgFnSc3d1	severská
radě	rada	k1gFnSc3	rada
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
jednání	jednání	k1gNnSc1	jednání
Severské	severský	k2eAgFnSc2d1	severská
rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Grónský	grónský	k2eAgInSc1d1	grónský
parlament	parlament	k1gInSc1	parlament
vysílá	vysílat	k5eAaImIp3nS	vysílat
dva	dva	k4xCgInPc4	dva
členy	člen	k1gInPc4	člen
do	do	k7c2	do
dánské	dánský	k2eAgFnSc2d1	dánská
delegace	delegace	k1gFnSc2	delegace
v	v	k7c6	v
Severské	severský	k2eAgFnSc6d1	severská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Západoseverská	Západoseverský	k2eAgFnSc1d1	Západoseverský
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
West-Nordic	West-Nordic	k1gMnSc1	West-Nordic
Council	Council	k1gMnSc1	Council
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
forum	forum	k1gNnSc4	forum
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
nadací	nadace	k1gFnSc7	nadace
Nordic	Nordice	k1gFnPc2	Nordice
Atlantic	Atlantice	k1gFnPc2	Atlantice
Cooperation	Cooperation	k1gInSc1	Cooperation
a	a	k8xC	a
West-Nordic	West-Nordic	k1gMnSc1	West-Nordic
Foundation	Foundation	k1gInSc4	Foundation
usilující	usilující	k2eAgInSc4d1	usilující
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
Grónska	Grónsko	k1gNnPc4	Grónsko
ještě	ještě	k9	ještě
Island	Island	k1gInSc1	Island
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
zeměmi	zem	k1gFnPc7	zem
bilaterální	bilaterální	k2eAgFnPc1d1	bilaterální
smlouvy	smlouva	k1gFnPc1	smlouva
o	o	k7c6	o
rybaření	rybařený	k2eAgMnPc1d1	rybařený
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
,	,	kIx,	,
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
Faerskymi	Faersky	k1gFnPc7	Faersky
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
Islandem	Island	k1gInSc7	Island
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Islandem	Island	k1gInSc7	Island
a	a	k8xC	a
Norskem	Norsko	k1gNnSc7	Norsko
existuje	existovat	k5eAaImIp3nS	existovat
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
lovu	lov	k1gInSc6	lov
huňáčka	huňáček	k1gMnSc2	huňáček
severního	severní	k2eAgMnSc2d1	severní
(	(	kIx(	(
<g/>
capelin	capelin	k2eAgMnSc1d1	capelin
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
o	o	k7c6	o
lovu	lov	k1gInSc6	lov
narvala	narval	k1gMnSc2	narval
(	(	kIx(	(
<g/>
narwhal	narwhal	k1gMnSc1	narwhal
<g/>
)	)	kIx)	)
a	a	k8xC	a
běluhy	běluha	k1gFnSc2	běluha
mořské	mořský	k2eAgInPc1d1	mořský
(	(	kIx(	(
<g/>
beluga	beluga	k1gFnSc1	beluga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
aktivním	aktivní	k2eAgInSc7d1	aktivní
členem	člen	k1gInSc7	člen
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
organizacích	organizace	k1gFnPc6	organizace
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
NAFO	NAFO	kA	NAFO
<g/>
,	,	kIx,	,
NEAFC	NEAFC	kA	NEAFC
<g/>
,	,	kIx,	,
NASCO	NASCO	kA	NASCO
<g/>
,	,	kIx,	,
NAMMCO	NAMMCO	kA	NAMMCO
<g/>
,	,	kIx,	,
WCW	WCW	kA	WCW
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
dánské	dánský	k2eAgFnSc2d1	dánská
delegace	delegace	k1gFnSc2	delegace
u	u	k7c2	u
IWC	IWC	kA	IWC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
finské	finský	k2eAgFnSc2d1	finská
iniciativy	iniciativa	k1gFnSc2	iniciativa
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organiace	organiace	k1gFnSc2	organiace
Arktická	arktický	k2eAgFnSc1d1	arktická
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c6	na
regionální	regionální	k2eAgFnSc6d1	regionální
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
členy	člen	k1gMnPc7	člen
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc1	osm
arktických	arktický	k2eAgFnPc2d1	arktická
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
/	/	kIx~	/
<g/>
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
v	v	k7c6	v
ICC	ICC	kA	ICC
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnPc4	organizace
grónských	grónský	k2eAgInPc2d1	grónský
<g/>
,	,	kIx,	,
aljašských	aljašský	k2eAgInPc2d1	aljašský
<g/>
,	,	kIx,	,
kanadských	kanadský	k2eAgInPc2d1	kanadský
a	a	k8xC	a
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
Inuitů	Inuit	k1gInPc2	Inuit
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
zlepšování	zlepšování	k1gNnSc4	zlepšování
životních	životní	k2eAgFnPc2d1	životní
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
ICC	ICC	kA	ICC
ma	ma	k?	ma
oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
nevládní	vládní	k2eNgFnSc2d1	nevládní
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
v	v	k7c6	v
systému	systém	k1gInSc6	systém
OSN	OSN	kA	OSN
i	i	k8xC	i
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
v	v	k7c6	v
Arktické	arktický	k2eAgFnSc6d1	arktická
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
je	být	k5eAaImIp3nS	být
grónský	grónský	k2eAgInSc1d1	grónský
samosprávní	samosprávní	k2eAgInSc1d1	samosprávní
parlament	parlament	k1gInSc1	parlament
dvěma	dva	k4xCgFnPc7	dva
členy	člen	k1gInPc4	člen
zastoupen	zastoupen	k2eAgInSc4d1	zastoupen
v	v	k7c6	v
dánské	dánský	k2eAgFnSc6d1	dánská
delegaci	delegace	k1gFnSc6	delegace
při	při	k7c6	při
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
/	/	kIx~	/
<g/>
Grónsko	Grónsko	k1gNnSc1	Grónsko
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zasazovalo	zasazovat	k5eAaImAgNnS	zasazovat
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
stálého	stálý	k2eAgNnSc2d1	stálé
fóra	fórum	k1gNnSc2	fórum
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
domorodé	domorodý	k2eAgInPc4d1	domorodý
národy	národ	k1gInPc4	národ
(	(	kIx(	(
<g/>
Indigenous	Indigenous	k1gMnSc1	Indigenous
Peoples	Peoples	k1gMnSc1	Peoples
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
při	při	k7c6	při
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dánským	dánský	k2eAgNnSc7d1	dánské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
začal	začít	k5eAaPmAgMnS	začít
grónský	grónský	k2eAgMnSc1d1	grónský
expert	expert	k1gMnSc1	expert
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
domorodých	domorodý	k2eAgInPc2d1	domorodý
národů	národ	k1gInPc2	národ
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
organizaci	organizace	k1gFnSc6	organizace
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
post	post	k1gInSc1	post
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
je	být	k5eAaImIp3nS	být
administrativně	administrativně	k6eAd1	administrativně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
kraje	kraj	k1gInPc4	kraj
-	-	kIx~	-
Východ-Západ	Východ-Západ	k1gInSc4	Východ-Západ
(	(	kIx(	(
<g/>
grónsky	grónsky	k6eAd1	grónsky
Sermersooq	Sermersooq	k1gMnSc2	Sermersooq
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severní	severní	k2eAgNnSc4d1	severní
Grónsko	Grónsko	k1gNnSc4	Grónsko
(	(	kIx(	(
<g/>
Qaasuitsup	Qaasuitsup	k1gInSc1	Qaasuitsup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgNnSc4d1	jižní
Grónsko	Grónsko	k1gNnSc4	Grónsko
(	(	kIx(	(
<g/>
Kujalleq	Kujalleq	k1gMnPc5	Kujalleq
<g/>
)	)	kIx)	)
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
Grónsko	Grónsko	k1gNnSc1	Grónsko
(	(	kIx(	(
<g/>
Qeqqata	Qeqqata	k1gFnSc1	Qeqqata
<g/>
)	)	kIx)	)
.	.	kIx.	.
<g/>
Současné	současný	k2eAgNnSc1d1	současné
administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
od	od	k7c2	od
1.1	[number]	k4	1.1
<g/>
.2009	.2009	k4	.2009
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
krajů	kraj	k1gInPc2	kraj
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
ještě	ještě	k6eAd1	ještě
vojenská	vojenský	k2eAgFnSc1d1	vojenská
základna	základna	k1gFnSc1	základna
Thule	Thule	k1gFnSc2	Thule
která	který	k3yIgFnSc1	který
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
suverenitu	suverenita	k1gFnSc4	suverenita
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
Grónský	grónský	k2eAgInSc1d1	grónský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
inkorporovaný	inkorporovaný	k2eAgInSc1d1	inkorporovaný
do	do	k7c2	do
žádného	žádný	k3yNgInSc2	žádný
z	z	k7c2	z
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
55	[number]	k4	55
847	[number]	k4	847
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
88	[number]	k4	88
%	%	kIx~	%
Inuité	Inuita	k1gMnPc1	Inuita
a	a	k8xC	a
dánsko-inuitští	dánskonuitský	k2eAgMnPc1d1	dánsko-inuitský
míšenci	míšenec	k1gMnPc1	míšenec
a	a	k8xC	a
12	[number]	k4	12
%	%	kIx~	%
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
Dánové	Dán	k1gMnPc1	Dán
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
stagnuje	stagnovat	k5eAaImIp3nS	stagnovat
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
mírně	mírně	k6eAd1	mírně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Nuuk	Nuuka	k1gFnPc2	Nuuka
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
dánská	dánský	k2eAgFnSc1d1	dánská
osada	osada	k1gFnSc1	osada
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1721	[number]	k4	1721
jako	jako	k8xC	jako
Godthaab	Godthaab	k1gMnSc1	Godthaab
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
grónské	grónský	k2eAgNnSc4d1	grónské
město	město	k1gNnSc4	město
je	být	k5eAaImIp3nS	být
Sisimiut	Sisimiut	k1gMnSc1	Sisimiut
(	(	kIx(	(
<g/>
Holsteinsborg	Holsteinsborg	k1gMnSc1	Holsteinsborg
<g/>
)	)	kIx)	)
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
osady	osada	k1gFnPc1	osada
jsou	být	k5eAaImIp3nP	být
Qaqortoq	Qaqortoq	k1gFnSc4	Qaqortoq
(	(	kIx(	(
<g/>
Julianehaab	Julianehaab	k1gInSc4	Julianehaab
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paamiut	Paamiut	k1gMnSc1	Paamiut
(	(	kIx(	(
<g/>
Frederikshaab	Frederikshaab	k1gMnSc1	Frederikshaab
<g/>
)	)	kIx)	)
a	a	k8xC	a
Narsaq	Narsaq	k1gFnSc1	Narsaq
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
Qaanaaq	Qaanaaq	k1gFnSc1	Qaanaaq
(	(	kIx(	(
<g/>
Thule	Thule	k1gInSc1	Thule
<g/>
)	)	kIx)	)
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
a	a	k8xC	a
Ammassalik	Ammassalika	k1gFnPc2	Ammassalika
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
osadami	osada	k1gFnPc7	osada
je	být	k5eAaImIp3nS	být
čilá	čilý	k2eAgFnSc1d1	čilá
letadlová	letadlový	k2eAgFnSc1d1	letadlová
<g/>
,	,	kIx,	,
vrtulníková	vrtulníkový	k2eAgFnSc1d1	vrtulníková
a	a	k8xC	a
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
motorové	motorový	k2eAgFnPc1d1	motorová
saně	saně	k1gFnPc1	saně
nebo	nebo	k8xC	nebo
psí	psí	k2eAgNnSc1d1	psí
spřežení	spřežení	k1gNnSc1	spřežení
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
je	být	k5eAaImIp3nS	být
grónština	grónština	k1gFnSc1	grónština
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
dánština	dánština	k1gFnSc1	dánština
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Nuuku	Nuuk	k1gInSc2	Nuuk
<g/>
.	.	kIx.	.
</s>
<s>
Grónštinu	grónština	k1gFnSc4	grónština
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
hlavním	hlavní	k2eAgInSc7d1	hlavní
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
pracovištích	pracoviště	k1gNnPc6	pracoviště
je	být	k5eAaImIp3nS	být
dánština	dánština	k1gFnSc1	dánština
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
většina	většina	k1gFnSc1	většina
mladších	mladý	k2eAgMnPc2d2	mladší
Gróňanů	Gróňan	k1gMnPc2	Gróňan
i	i	k8xC	i
Dánů	Dán	k1gMnPc2	Dán
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
lidé	člověk	k1gMnPc1	člověk
pak	pak	k6eAd1	pak
ovládají	ovládat	k5eAaImIp3nP	ovládat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejstarší	starý	k2eAgNnSc4d3	nejstarší
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
preliterární	preliterární	k2eAgFnPc1d1	preliterární
<g/>
,	,	kIx,	,
projevy	projev	k1gInPc1	projev
inuitské	inuitský	k2eAgFnSc2d1	inuitská
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
považovat	považovat	k5eAaImF	považovat
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zpívané	zpívaný	k2eAgInPc4d1	zpívaný
s	s	k7c7	s
bubínky	bubínek	k1gInPc7	bubínek
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
pověsti	pověst	k1gFnPc4	pověst
<g/>
,	,	kIx,	,
ságy	sága	k1gFnPc4	sága
<g/>
,	,	kIx,	,
mýty	mýtus	k1gInPc4	mýtus
a	a	k8xC	a
pohádky	pohádka	k1gFnPc4	pohádka
tradované	tradovaný	k2eAgFnPc4d1	tradovaná
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
západními	západní	k2eAgInPc7d1	západní
záznamy	záznam	k1gInPc7	záznam
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc1	základ
grónské	grónský	k2eAgFnSc2d1	grónská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Písemné	písemný	k2eAgNnSc4d1	písemné
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
této	tento	k3xDgFnSc2	tento
nejstarší	starý	k2eAgFnSc2d3	nejstarší
<g/>
,	,	kIx,	,
ústně	ústně	k6eAd1	ústně
tradované	tradovaný	k2eAgFnSc2d1	tradovaná
grónské	grónský	k2eAgFnSc2d1	grónská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nedávné	dávný	k2eNgNnSc1d1	nedávné
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prvních	první	k4xOgFnPc2	první
evropských	evropský	k2eAgFnPc2d1	Evropská
misií	misie	k1gFnPc2	misie
(	(	kIx(	(
<g/>
přímo	přímo	k6eAd1	přímo
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
poli	pole	k1gNnSc6	pole
angažoval	angažovat	k5eAaBmAgMnS	angažovat
i	i	k9	i
Hans	Hans	k1gMnSc1	Hans
Egede	Eged	k1gMnSc5	Eged
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
záznamy	záznam	k1gInPc1	záznam
této	tento	k3xDgFnSc2	tento
literatury	literatura	k1gFnSc2	literatura
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
významné	významný	k2eAgMnPc4d1	významný
badatele	badatel	k1gMnPc4	badatel
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
zejména	zejména	k9	zejména
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rink	Rink	k1gMnSc1	Rink
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
do	do	k7c2	do
dánštiny	dánština	k1gFnSc2	dánština
přeložil	přeložit	k5eAaPmAgMnS	přeložit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
eskimáckých	eskimácký	k2eAgFnPc2d1	eskimácký
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
ság	sága	k1gFnPc2	sága
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Eskimoiske	Eskimoiske	k1gInSc1	Eskimoiske
Eventyr	Eventyr	k1gMnSc1	Eventyr
og	og	k?	og
Sagn	Sagn	k1gMnSc1	Sagn
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
a	a	k8xC	a
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
a	a	k8xC	a
Knud	Knud	k1gInSc1	Knud
Rasmussen	Rasmussna	k1gFnPc2	Rasmussna
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
veledílo	veledílo	k1gNnSc4	veledílo
Myter	Myter	k1gMnSc1	Myter
og	og	k?	og
Sagn	Sagn	k1gMnSc1	Sagn
fra	fra	k?	fra
Grø	Grø	k1gMnSc1	Grø
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
I-III	I-III	k?	I-III
vycházelo	vycházet	k5eAaImAgNnS	vycházet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
a	a	k8xC	a
doplněno	doplnit	k5eAaPmNgNnS	doplnit
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Inuit	Inuit	k1gInSc4	Inuit
fortæ	fortæ	k?	fortæ
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
vydal	vydat	k5eAaPmAgInS	vydat
Regitze	Regitze	k1gFnSc2	Regitze
Sæ	Sæ	k1gMnSc2	Sæ
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jako	jako	k8xC	jako
další	další	k2eAgNnPc1d1	další
reprezentativní	reprezentativní	k2eAgNnPc1d1	reprezentativní
díla	dílo	k1gNnPc1	dílo
bývají	bývat	k5eAaImIp3nP	bývat
dále	daleko	k6eAd2	daleko
uváděny	uváděn	k2eAgMnPc4d1	uváděn
ještě	ještě	k6eAd1	ještě
sbírka	sbírka	k1gFnSc1	sbírka
východogrónských	východogrónský	k2eAgFnPc2d1	východogrónský
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
ság	sága	k1gFnPc2	sága
Jense	Jens	k1gMnSc4	Jens
Rosinga	Rosing	k1gMnSc4	Rosing
(	(	kIx(	(
<g/>
Sagn	Sagn	k1gMnSc1	Sagn
og	og	k?	og
saga	saga	k1gMnSc1	saga
fra	fra	k?	fra
Angmagssalik	Angmagssalik	k1gMnSc1	Angmagssalik
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
zápis	zápis	k1gInSc1	zápis
staré	starý	k2eAgFnSc2d1	stará
eskymácké	eskymácký	k2eAgFnSc2d1	eskymácká
poezie	poezie	k1gFnSc2	poezie
Knuda	Knuda	k1gFnSc1	Knuda
Rasmussena	Rasmussen	k2eAgFnSc1d1	Rasmussena
(	(	kIx(	(
<g/>
Snehyttens	Snehyttens	k1gInSc1	Snehyttens
Sange	Sang	k1gInSc2	Sang
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
zabývá	zabývat	k5eAaImIp3nS	zabývat
i	i	k9	i
starými	starý	k2eAgFnPc7d1	stará
písněmi	píseň	k1gFnPc7	píseň
Inuitů	Inuita	k1gMnPc2	Inuita
z	z	k7c2	z
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nuuk	Nuuk	k1gMnSc1	Nuuk
TV	TV	kA	TV
</s>
