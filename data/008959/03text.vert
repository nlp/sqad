<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1936	[number]	k4	1936
–	–	k?	–
duben	duben	k1gInSc1	duben
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejkrvavějším	krvavý	k2eAgMnPc3d3	nejkrvavější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nejnepřehlednějším	přehlední	k2eNgInPc3d3	přehlední
konfliktům	konflikt	k1gInPc3	konflikt
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
hluboce	hluboko	k6eAd1	hluboko
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
nejen	nejen	k6eAd1	nejen
španělskou	španělský	k2eAgFnSc4d1	španělská
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proti	proti	k7c3	proti
demokraticky	demokraticky	k6eAd1	demokraticky
zvolené	zvolený	k2eAgFnSc3d1	zvolená
republikánské	republikánský	k2eAgFnSc3d1	republikánská
vládě	vláda	k1gFnSc3	vláda
vojensky	vojensky	k6eAd1	vojensky
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Francisca	Franciscus	k1gMnSc2	Franciscus
Franca	Franca	k?	Franca
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přes	přes	k7c4	přes
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
z	z	k7c2	z
nepřehledného	přehledný	k2eNgNnSc2d1	nepřehledné
společenského	společenský	k2eAgNnSc2d1	společenské
a	a	k8xC	a
politického	politický	k2eAgNnSc2d1	politické
spektra	spektrum	k1gNnSc2	spektrum
španělské	španělský	k2eAgFnPc1d1	španělská
společnosti	společnost	k1gFnPc1	společnost
vyprofilovaly	vyprofilovat	k5eAaPmAgFnP	vyprofilovat
dvě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
<g/>
:	:	kIx,	:
převážně	převážně	k6eAd1	převážně
levicoví	levicový	k2eAgMnPc1d1	levicový
republikáni	republikán	k1gMnPc1	republikán
bojující	bojující	k2eAgMnPc1d1	bojující
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
oficiální	oficiální	k2eAgFnSc2d1	oficiální
vlády	vláda	k1gFnSc2	vláda
druhé	druhý	k4xOgFnSc2	druhý
španělské	španělský	k2eAgFnSc2d1	španělská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
trockisté	trockista	k1gMnPc1	trockista
a	a	k8xC	a
anarchisté	anarchista	k1gMnPc1	anarchista
<g/>
,	,	kIx,	,
baskičtí	baskický	k2eAgMnPc1d1	baskický
a	a	k8xC	a
katalánští	katalánský	k2eAgMnPc1d1	katalánský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravicoví	pravicový	k2eAgMnPc1d1	pravicový
povstalci	povstalec	k1gMnPc1	povstalec
(	(	kIx(	(
<g/>
fašisté	fašista	k1gMnPc1	fašista
<g/>
,	,	kIx,	,
španělští	španělský	k2eAgMnPc1d1	španělský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
<g/>
,	,	kIx,	,
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
demokraté	demokrat	k1gMnPc1	demokrat
a	a	k8xC	a
roajalisté	roajalista	k1gMnPc1	roajalista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
frankisté	frankista	k1gMnPc1	frankista
podle	podle	k7c2	podle
generála	generál	k1gMnSc2	generál
Franca	Franca	k?	Franca
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
vyprofiloval	vyprofilovat	k5eAaPmAgInS	vyprofilovat
v	v	k7c4	v
hlavního	hlavní	k2eAgMnSc4d1	hlavní
povstaleckého	povstalecký	k2eAgMnSc4d1	povstalecký
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
hájila	hájit	k5eAaImAgFnS	hájit
politiku	politika	k1gFnSc4	politika
nezasahování	nezasahování	k1gNnSc2	nezasahování
a	a	k8xC	a
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
i	i	k9	i
zbrojní	zbrojní	k2eAgNnSc4d1	zbrojní
embargo	embargo	k1gNnSc4	embargo
<g/>
,	,	kIx,	,
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
významně	významně	k6eAd1	významně
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
frankistů	frankista	k1gMnPc2	frankista
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
prvopočátku	prvopočátek	k1gInSc2	prvopočátek
konfliktu	konflikt	k1gInSc2	konflikt
byly	být	k5eAaImAgFnP	být
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
republikánů	republikán	k1gMnPc2	republikán
pak	pak	k6eAd1	pak
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1936	[number]	k4	1936
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
republikánů	republikán	k1gMnPc2	republikán
působily	působit	k5eAaImAgFnP	působit
i	i	k9	i
dobrovolnické	dobrovolnický	k2eAgFnPc1d1	dobrovolnická
jednotky	jednotka	k1gFnPc1	jednotka
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
–	–	k?	–
interbrigády	interbrigáda	k1gFnPc4	interbrigáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
jasným	jasný	k2eAgNnSc7d1	jasné
vítězstvím	vítězství	k1gNnSc7	vítězství
lépe	dobře	k6eAd2	dobře
organizovaných	organizovaný	k2eAgMnPc2d1	organizovaný
frankistů	frankista	k1gMnPc2	frankista
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgMnSc6	jenž
následovalo	následovat	k5eAaImAgNnS	následovat
důkladné	důkladný	k2eAgNnSc1d1	důkladné
"	"	kIx"	"
<g/>
očištění	očištění	k1gNnSc1	očištění
<g/>
"	"	kIx"	"
španělské	španělský	k2eAgFnSc2d1	španělská
společnosti	společnost	k1gFnSc2	společnost
od	od	k7c2	od
všeho	všecek	k3xTgInSc2	všecek
"	"	kIx"	"
<g/>
rudého	rudý	k2eAgInSc2d1	rudý
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
připomínajícího	připomínající	k2eAgInSc2d1	připomínající
druhou	druhý	k4xOgFnSc4	druhý
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
především	především	k9	především
odbory	odbor	k1gInPc1	odbor
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
či	či	k8xC	či
znepřístupněna	znepřístupněn	k2eAgFnSc1d1	znepřístupněna
řada	řada	k1gFnSc1	řada
archivů	archiv	k1gInPc2	archiv
<g/>
,	,	kIx,	,
nepohodlní	pohodlný	k2eNgMnPc1d1	nepohodlný
jedinci	jedinec	k1gMnPc1	jedinec
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
uvězněni	uvěznit	k5eAaPmNgMnP	uvěznit
<g/>
,	,	kIx,	,
nuceni	nutit	k5eAaImNgMnP	nutit
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
či	či	k8xC	či
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
obětí	oběť	k1gFnPc2	oběť
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
;	;	kIx,	;
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c4	mezi
půl	půl	k1xP	půl
milionem	milion	k4xCgInSc7	milion
a	a	k8xC	a
milionem	milion	k4xCgInSc7	milion
mrtvých	mrtvý	k1gMnPc6	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
těchto	tento	k3xDgFnPc2	tento
obětí	oběť	k1gFnPc2	oběť
nebyla	být	k5eNaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
válečných	válečný	k2eAgInPc2d1	válečný
střetů	střet	k1gInPc2	střet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hromadných	hromadný	k2eAgFnPc2d1	hromadná
poprav	poprava	k1gFnPc2	poprava
prováděných	prováděný	k2eAgFnPc2d1	prováděná
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
čistě	čistě	k6eAd1	čistě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ideologické	ideologický	k2eAgFnSc2d1	ideologická
nebo	nebo	k8xC	nebo
třídní	třídní	k2eAgFnSc2d1	třídní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
válka	válka	k1gFnSc1	válka
trvala	trvat	k5eAaImAgFnS	trvat
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
Španělsko	Španělsko	k1gNnSc4	Španělsko
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
vzpamatovávala	vzpamatovávat	k5eAaImAgFnS	vzpamatovávat
ještě	ještě	k9	ještě
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
zažehla	zažehnout	k5eAaPmAgFnS	zažehnout
vášně	vášeň	k1gFnPc4	vášeň
mezi	mezi	k7c7	mezi
intelektuály	intelektuál	k1gMnPc7	intelektuál
a	a	k8xC	a
politickými	politický	k2eAgInPc7d1	politický
spolky	spolek	k1gInPc7	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Republikánští	republikánský	k2eAgMnPc1d1	republikánský
stoupenci	stoupenec	k1gMnPc1	stoupenec
ji	on	k3xPp3gFnSc4	on
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
zápas	zápas	k1gInSc4	zápas
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
tyranií	tyranie	k1gFnSc7	tyranie
a	a	k8xC	a
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
fašismem	fašismus	k1gInSc7	fašismus
a	a	k8xC	a
svobodou	svoboda	k1gFnSc7	svoboda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
levicových	levicový	k2eAgMnPc2d1	levicový
idealistů	idealista	k1gMnPc2	idealista
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
k	k	k7c3	k
interbrigadistům	interbrigadista	k1gMnPc3	interbrigadista
<g/>
.	.	kIx.	.
</s>
<s>
Frankovi	Frankův	k2eAgMnPc1d1	Frankův
stoupenci	stoupenec	k1gMnPc1	stoupenec
zase	zase	k9	zase
tuto	tento	k3xDgFnSc4	tento
válku	válka	k1gFnSc4	válka
vnímali	vnímat	k5eAaImAgMnP	vnímat
jako	jako	k8xS	jako
boj	boj	k1gInSc4	boj
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
rudými	rudý	k2eAgFnPc7d1	rudá
hordami	horda	k1gFnPc7	horda
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
měla	mít	k5eAaImAgFnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
význam	význam	k1gInSc4	význam
také	také	k9	také
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
počáteční	počáteční	k2eAgFnSc2d1	počáteční
fáze	fáze	k1gFnSc2	fáze
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jak	jak	k8xC	jak
síly	síla	k1gFnPc1	síla
Osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
si	se	k3xPyFc3	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyzkoušely	vyzkoušet	k5eAaPmAgFnP	vyzkoušet
svoji	svůj	k3xOyFgFnSc4	svůj
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
bojové	bojový	k2eAgFnSc2d1	bojová
doktríny	doktrína	k1gFnSc2	doktrína
a	a	k8xC	a
získaným	získaný	k2eAgFnPc3d1	získaná
zkušenostem	zkušenost	k1gFnPc3	zkušenost
přizpůsobily	přizpůsobit	k5eAaPmAgInP	přizpůsobit
výcvik	výcvik	k1gInSc4	výcvik
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
zbrojní	zbrojní	k2eAgInPc4d1	zbrojní
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předehra	předehra	k1gFnSc1	předehra
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Příčiny	příčina	k1gFnPc1	příčina
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
tohoto	tento	k3xDgInSc2	tento
konfliktu	konflikt	k1gInSc2	konflikt
lze	lze	k6eAd1	lze
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
společenské	společenský	k2eAgFnSc3d1	společenská
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
Španělsko	Španělsko	k1gNnSc4	Španělsko
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
radikalizaci	radikalizace	k1gFnSc3	radikalizace
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
své	svůj	k3xOyFgInPc4	svůj
problémy	problém	k1gInPc4	problém
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
řešit	řešit	k5eAaImF	řešit
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
politicky	politicky	k6eAd1	politicky
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
,	,	kIx,	,
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
rostlo	růst	k5eAaImAgNnS	růst
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
připravovali	připravovat	k5eAaImAgMnP	připravovat
<g/>
"	"	kIx"	"
na	na	k7c6	na
válku	válek	k1gInSc6	válek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Průběh	průběh	k1gInSc4	průběh
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Situace	situace	k1gFnSc1	situace
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
pravicové	pravicový	k2eAgFnPc1d1	pravicová
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
utvořily	utvořit	k5eAaPmAgInP	utvořit
středo-pravou	středoravý	k2eAgFnSc4d1	středo-pravá
koalici	koalice	k1gFnSc4	koalice
(	(	kIx(	(
<g/>
Lerroux	Lerroux	k1gInSc4	Lerroux
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
koalice	koalice	k1gFnSc2	koalice
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1934	[number]	k4	1934
po	po	k7c6	po
prohraných	prohraný	k2eAgFnPc6d1	prohraná
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
katolická	katolický	k2eAgFnSc1d1	katolická
strana	strana	k1gFnSc1	strana
Confederación	Confederación	k1gMnSc1	Confederación
Españ	Españ	k1gMnSc1	Españ
de	de	k?	de
Derechas	Derechas	k1gMnSc1	Derechas
Autónomas	Autónomas	k1gMnSc1	Autónomas
(	(	kIx(	(
<g/>
CEDA	CEDA	kA	CEDA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
rozpory	rozpor	k1gInPc1	rozpor
vládních	vládní	k2eAgFnPc2d1	vládní
stran	strana	k1gFnPc2	strana
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
omezené	omezený	k2eAgFnSc3d1	omezená
schopnosti	schopnost	k1gFnSc3	schopnost
podniknout	podniknout	k5eAaPmF	podniknout
akci	akce	k1gFnSc4	akce
nebo	nebo	k8xC	nebo
učinit	učinit	k5eAaPmF	učinit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
vyzvaly	vyzvat	k5eAaPmAgFnP	vyzvat
levicové	levicový	k2eAgFnPc1d1	levicová
strany	strana	k1gFnPc1	strana
ke	k	k7c3	k
generální	generální	k2eAgFnSc3d1	generální
stávce	stávka	k1gFnSc3	stávka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
vzešla	vzejít	k5eAaPmAgFnS	vzejít
hornická	hornický	k2eAgFnSc1d1	hornická
vzpoura	vzpoura	k1gFnSc1	vzpoura
tzv.	tzv.	kA	tzv.
Španělská	španělský	k2eAgFnSc1d1	španělská
revoluce	revoluce	k1gFnSc1	revoluce
z	z	k7c2	z
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
Diega	Dieg	k1gMnSc2	Dieg
Hidalga	Hidalgo	k1gMnSc2	Hidalgo
vojensky	vojensky	k6eAd1	vojensky
potlačena	potlačit	k5eAaPmNgNnP	potlačit
generálem	generál	k1gMnSc7	generál
Lópezem	López	k1gInSc7	López
Ochoou	Ochoa	k1gFnSc7	Ochoa
a	a	k8xC	a
legionáři	legionář	k1gMnPc1	legionář
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
velel	velet	k5eAaImAgMnS	velet
podplukovník	podplukovník	k1gMnSc1	podplukovník
Juan	Juan	k1gMnSc1	Juan
Yagüe	Yagüe	k1gFnSc1	Yagüe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
radikalizaci	radikalizace	k1gFnSc3	radikalizace
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
množství	množství	k1gNnSc3	množství
stávek	stávka	k1gFnPc2	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
akcích	akce	k1gFnPc6	akce
běžně	běžně	k6eAd1	běžně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
násilí	násilí	k1gNnSc3	násilí
a	a	k8xC	a
úmrtím	úmrť	k1gFnPc3	úmrť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
vládních	vládní	k2eAgFnPc6d1	vládní
krizích	krize	k1gFnPc6	krize
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1936	[number]	k4	1936
vypsány	vypsán	k2eAgFnPc1d1	vypsána
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přinesly	přinést	k5eAaPmAgFnP	přinést
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
levici	levice	k1gFnSc4	levice
sdružené	sdružený	k2eAgNnSc1d1	sdružené
do	do	k7c2	do
Lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
sestavil	sestavit	k5eAaPmAgMnS	sestavit
vůdce	vůdce	k1gMnSc1	vůdce
levého	levý	k2eAgNnSc2d1	levé
křídla	křídlo	k1gNnSc2	křídlo
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
Manuel	Manuel	k1gMnSc1	Manuel
Azañ	Azañ	k1gMnSc1	Azañ
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
jak	jak	k8xS	jak
extrémní	extrémní	k2eAgFnSc2d1	extrémní
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
levice	levice	k1gFnSc2	levice
<g/>
.	.	kIx.	.
</s>
<s>
Manuel	Manuel	k1gMnSc1	Manuel
Azañ	Azañ	k1gMnSc1	Azañ
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
napjatému	napjatý	k2eAgInSc3d1	napjatý
stavu	stav	k1gInSc3	stav
čelit	čelit	k5eAaImF	čelit
rázným	rázný	k2eAgNnSc7d1	rázné
vystupováním	vystupování	k1gNnSc7	vystupování
proti	proti	k7c3	proti
fašistickým	fašistický	k2eAgNnPc3d1	fašistické
<g/>
,	,	kIx,	,
národně	národně	k6eAd1	národně
syndikalistickým	syndikalistický	k2eAgFnPc3d1	syndikalistická
a	a	k8xC	a
konzervativním	konzervativní	k2eAgFnPc3d1	konzervativní
organizacím	organizace	k1gFnPc3	organizace
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
zatknout	zatknout	k5eAaPmF	zatknout
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
vysoké	vysoký	k2eAgMnPc4d1	vysoký
důstojníky	důstojník	k1gMnPc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1936	[number]	k4	1936
byla	být	k5eAaImAgFnS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
Falanga	falanga	k1gFnSc1	falanga
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
zatčeno	zatčen	k2eAgNnSc4d1	zatčeno
i	i	k9	i
její	její	k3xOp3gNnSc4	její
vedení	vedení	k1gNnSc4	vedení
včetně	včetně	k7c2	včetně
José	Josá	k1gFnSc2	Josá
Antonia	Antonio	k1gMnSc2	Antonio
Primo	primo	k1gNnSc4	primo
de	de	k?	de
Rivery	Rivera	k1gFnSc2	Rivera
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
však	však	k9	však
násilnosti	násilnost	k1gFnPc4	násilnost
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
spíše	spíše	k9	spíše
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
se	se	k3xPyFc4	se
parlament	parlament	k1gInSc1	parlament
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
pravicí	pravice	k1gFnSc7	pravice
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odvolat	odvolat	k5eAaPmF	odvolat
prezidenta	prezident	k1gMnSc4	prezident
Nicetu	Nicet	k1gInSc2	Nicet
Alcalá-Zamoru	Alcalá-Zamor	k1gMnSc3	Alcalá-Zamor
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
rozpustit	rozpustit	k5eAaPmF	rozpustit
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gInSc1	jeho
pokus	pokus	k1gInSc1	pokus
byl	být	k5eAaImAgInS	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
zneužití	zneužití	k1gNnSc1	zneužití
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
byly	být	k5eAaImAgFnP	být
vypsány	vypsat	k5eAaPmNgFnP	vypsat
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravice	pravice	k1gFnSc1	pravice
v	v	k7c6	v
nich	on	k3xPp3gFnPc2	on
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
nepostavila	postavit	k5eNaPmAgFnS	postavit
svého	svůj	k3xOyFgMnSc4	svůj
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
stoupalo	stoupat	k5eAaImAgNnS	stoupat
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
zdrojů	zdroj	k1gInPc2	zdroj
bylo	být	k5eAaImAgNnS	být
330	[number]	k4	330
lidí	člověk	k1gMnPc2	člověk
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
a	a	k8xC	a
1511	[number]	k4	1511
zraněno	zranit	k5eAaPmNgNnS	zranit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
politického	politický	k2eAgNnSc2d1	politické
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
113	[number]	k4	113
generálním	generální	k2eAgFnPc3d1	generální
stávkám	stávka	k1gFnPc3	stávka
<g/>
,	,	kIx,	,
zničení	zničení	k1gNnSc4	zničení
160	[number]	k4	160
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
vražděni	vražděn	k2eAgMnPc1d1	vražděn
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
řeholní	řeholní	k2eAgFnPc1d1	řeholní
sestry	sestra	k1gFnPc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
nadporučík	nadporučík	k1gMnSc1	nadporučík
"	"	kIx"	"
<g/>
Útočných	útočný	k2eAgFnPc2d1	útočná
stráží	stráž	k1gFnPc2	stráž
<g/>
"	"	kIx"	"
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
José	Josý	k2eAgNnSc4d1	José
Castillo	Castillo	k1gNnSc4	Castillo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
skupina	skupina	k1gFnSc1	skupina
důstojníků	důstojník	k1gMnPc2	důstojník
"	"	kIx"	"
<g/>
Útočných	útočný	k2eAgFnPc2d1	útočná
stráží	stráž	k1gFnPc2	stráž
<g/>
"	"	kIx"	"
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
odvety	odveta	k1gFnSc2	odveta
zavraždila	zavraždit	k5eAaPmAgFnS	zavraždit
vůdce	vůdce	k1gMnPc4	vůdce
monarchistů	monarchista	k1gMnPc2	monarchista
José	Josý	k2eAgNnSc1d1	José
Calvo	Calvo	k1gNnSc1	Calvo
Sotela	Sotela	k1gFnSc1	Sotela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Začátek	začátek	k1gInSc1	začátek
války	válka	k1gFnSc2	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
ve	v	k7c6	v
Španělském	španělský	k2eAgNnSc6d1	španělské
Maroku	Maroko	k1gNnSc6	Maroko
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
začátek	začátek	k1gInSc4	začátek
této	tento	k3xDgFnSc2	tento
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
generálové	generál	k1gMnPc1	generál
Manuel	Manuel	k1gMnSc1	Manuel
Goded	Goded	k1gMnSc1	Goded
y	y	k?	y
Llopis	Llopis	k1gInSc1	Llopis
<g/>
,	,	kIx,	,
Francisco	Francisco	k1gMnSc1	Francisco
Franco	Franco	k1gMnSc1	Franco
y	y	k?	y
Bahamonde	Bahamond	k1gMnSc5	Bahamond
<g/>
,	,	kIx,	,
Emilio	Emilio	k6eAd1	Emilio
Mola	mol	k1gMnSc4	mol
<g/>
,	,	kIx,	,
Gonzalo	Gonzalo	k1gMnSc4	Gonzalo
Queipo	Queipa	k1gFnSc5	Queipa
de	de	k?	de
Llano	Llaen	k2eAgNnSc4d1	Llano
a	a	k8xC	a
José	Josý	k2eAgNnSc4d1	José
Sanjurjo	Sanjurjo	k1gNnSc4	Sanjurjo
y	y	k?	y
Sacanell	Sacanell	k1gInSc1	Sacanell
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
vůdcem	vůdce	k1gMnSc7	vůdce
vzpoury	vzpoura	k1gFnSc2	vzpoura
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
Sanjurjo	Sanjurjo	k1gMnSc1	Sanjurjo
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
obsadili	obsadit	k5eAaPmAgMnP	obsadit
strategická	strategický	k2eAgNnPc4d1	strategické
místa	místo	k1gNnPc4	místo
ve	v	k7c6	v
Španělském	španělský	k2eAgNnSc6d1	španělské
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
,	,	kIx,	,
potlačili	potlačit	k5eAaPmAgMnP	potlačit
stávku	stávka	k1gFnSc4	stávka
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
povstání	povstání	k1gNnSc1	povstání
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
povstalcům	povstalec	k1gMnPc3	povstalec
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
španělská	španělský	k2eAgFnSc1d1	španělská
Africká	africký	k2eAgFnSc1d1	africká
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
Španělské	španělský	k2eAgFnSc2d1	španělská
Legie	legie	k1gFnSc2	legie
a	a	k8xC	a
z	z	k7c2	z
marockých	marocký	k2eAgFnPc2d1	marocká
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
jednotek	jednotka	k1gFnPc2	jednotka
Regulares	Regulares	k1gMnSc1	Regulares
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
opřít	opřít	k5eAaPmF	opřít
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
civilní	civilní	k2eAgFnSc4d1	civilní
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
brzy	brzy	k6eAd1	brzy
podaří	podařit	k5eAaPmIp3nS	podařit
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
ekonomicky	ekonomicky	k6eAd1	ekonomicky
důležitými	důležitý	k2eAgFnPc7d1	důležitá
oblastmi	oblast	k1gFnPc7	oblast
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
již	již	k9	již
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Baleáry	Baleáry	k1gFnPc4	Baleáry
a	a	k8xC	a
23	[number]	k4	23
významných	významný	k2eAgNnPc2d1	významné
měst	město	k1gNnPc2	město
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Pamplona	Pamplona	k1gFnSc1	Pamplona
<g/>
,	,	kIx,	,
Cádiz	Cádiz	k1gInSc1	Cádiz
<g/>
,	,	kIx,	,
Córdoba	Córdoba	k1gFnSc1	Córdoba
a	a	k8xC	a
Zaragoza	Zaragoza	k1gFnSc1	Zaragoza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Santiaga	Santiago	k1gNnSc2	Santiago
Casarese	Casarese	k1gFnSc2	Casarese
Quirogy	Quirog	k1gMnPc4	Quirog
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
reagovat	reagovat	k5eAaBmF	reagovat
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
sestavil	sestavit	k5eAaPmAgMnS	sestavit
vládu	vláda	k1gFnSc4	vláda
Diego	Diego	k1gMnSc1	Diego
Martinez	Martinez	k1gMnSc1	Martinez
Barrio	Barrio	k1gMnSc1	Barrio
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k9	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
dát	dát	k5eAaPmF	dát
zbraně	zbraň	k1gFnPc4	zbraň
demonstrujícím	demonstrující	k2eAgFnPc3d1	demonstrující
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
požadavek	požadavek	k1gInSc4	požadavek
splnila	splnit	k5eAaPmAgFnS	splnit
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
už	už	k6eAd1	už
vedl	vést	k5eAaImAgMnS	vést
republikán	republikán	k1gMnSc1	republikán
José	Josá	k1gFnSc2	Josá
Giralo	Girala	k1gFnSc5	Girala
y	y	k?	y
Pereira	Pereiro	k1gNnSc2	Pereiro
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyzbrojil	vyzbrojit	k5eAaPmAgInS	vyzbrojit
dělnické	dělnický	k2eAgFnPc4d1	Dělnická
milice	milice	k1gFnPc4	milice
a	a	k8xC	a
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
nastal	nastat	k5eAaPmAgInS	nastat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
obratů	obrat	k1gInPc2	obrat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
také	také	k9	také
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
vůdce	vůdce	k1gMnSc1	vůdce
nacionalistů	nacionalista	k1gMnPc2	nacionalista
Sanjurjo	Sanjurjo	k6eAd1	Sanjurjo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Od	od	k7c2	od
ozbrojení	ozbrojení	k1gNnSc2	ozbrojení
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
do	do	k7c2	do
obležení	obležení	k1gNnSc2	obležení
Madridu	Madrid	k1gInSc2	Madrid
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
k	k	k7c3	k
absolutnímu	absolutní	k2eAgNnSc3d1	absolutní
ignorování	ignorování	k1gNnSc3	ignorování
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
konvencí	konvence	k1gFnPc2	konvence
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
hned	hned	k6eAd1	hned
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
dle	dle	k7c2	dle
odhadů	odhad	k1gInPc2	odhad
povražděno	povraždit	k5eAaPmNgNnS	povraždit
přes	přes	k7c4	přes
50	[number]	k4	50
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
civilisty	civilista	k1gMnPc4	civilista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
"	"	kIx"	"
<g/>
špatné	špatný	k2eAgFnSc6d1	špatná
straně	strana	k1gFnSc6	strana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
popravených	popravený	k2eAgMnPc2d1	popravený
byly	být	k5eAaImAgInP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
popravy	poprava	k1gFnPc1	poprava
byly	být	k5eAaImAgFnP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
paseos	paseos	k1gMnSc1	paseos
(	(	kIx(	(
<g/>
paseo	paseo	k1gMnSc1	paseo
=	=	kIx~	=
"	"	kIx"	"
<g/>
procházka	procházka	k1gFnSc1	procházka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
projížďka	projížďka	k1gFnSc1	projížďka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
probíhaly	probíhat	k5eAaImAgFnP	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
město	město	k1gNnSc4	město
byli	být	k5eAaImAgMnP	být
odvezeni	odvezen	k2eAgMnPc1d1	odvezen
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
pak	pak	k6eAd1	pak
ozbrojení	ozbrojený	k2eAgMnPc1d1	ozbrojený
příslušníci	příslušník	k1gMnPc1	příslušník
postříleli	postřílet	k5eAaPmAgMnP	postřílet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
případy	případ	k1gInPc7	případ
takového	takový	k3xDgNnSc2	takový
vraždění	vraždění	k1gNnSc2	vraždění
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
zastřelení	zastřelení	k1gNnSc3	zastřelení
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Federico	Federico	k6eAd1	Federico
García	Garcí	k2eAgFnSc1d1	García
Lorca	Lorca	k1gFnSc1	Lorca
frankisty	frankista	k1gMnSc2	frankista
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vyvraždění	vyvraždění	k1gNnSc1	vyvraždění
osazenstva	osazenstvo	k1gNnSc2	osazenstvo
barbasterského	barbasterský	k2eAgInSc2d1	barbasterský
semináře	seminář	k1gInSc2	seminář
republikány	republikán	k1gMnPc7	republikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdání	rozdání	k1gNnSc1	rozdání
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
krok	krok	k1gInSc4	krok
jak	jak	k8xS	jak
čelit	čelit	k5eAaImF	čelit
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
povstalců	povstalec	k1gMnPc2	povstalec
byla	být	k5eAaImAgFnS	být
nemožnost	nemožnost	k1gFnSc1	nemožnost
přepravy	přeprava	k1gFnSc2	přeprava
vojska	vojsko	k1gNnSc2	vojsko
ze	z	k7c2	z
španělského	španělský	k2eAgNnSc2d1	španělské
Maroka	Maroko	k1gNnSc2	Maroko
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
námořní	námořní	k2eAgFnPc1d1	námořní
síly	síla	k1gFnPc1	síla
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
loajální	loajální	k2eAgInPc4d1	loajální
vůči	vůči	k7c3	vůči
republice	republika	k1gFnSc3	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
jim	on	k3xPp3gMnPc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
vyřešit	vyřešit	k5eAaPmF	vyřešit
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
když	když	k8xS	když
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
letecky	letecky	k6eAd1	letecky
přepravily	přepravit	k5eAaPmAgFnP	přepravit
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
cizineckou	cizinecký	k2eAgFnSc4d1	cizinecká
legii	legie	k1gFnSc4	legie
z	z	k7c2	z
Tetuánu	Tetuán	k1gInSc2	Tetuán
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
též	též	k9	též
nacházeli	nacházet	k5eAaImAgMnP	nacházet
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
režim	režim	k1gInSc1	režim
Antónia	Antónium	k1gNnSc2	Antónium
Salazara	Salazara	k1gFnSc1	Salazara
<g/>
.	.	kIx.	.
</s>
<s>
Pomoc	pomoc	k1gFnSc1	pomoc
frankistům	frankista	k1gMnPc3	frankista
byla	být	k5eAaImAgNnP	být
prováděna	provádět	k5eAaImNgNnP	provádět
tajně	tajně	k6eAd1	tajně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tyto	tento	k3xDgFnPc4	tento
dodávky	dodávka	k1gFnPc4	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
utajit	utajit	k5eAaPmF	utajit
zvláště	zvláště	k6eAd1	zvláště
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
německá	německý	k2eAgNnPc1d1	německé
bojová	bojový	k2eAgNnPc1d1	bojové
letadla	letadlo	k1gNnPc1	letadlo
přistála	přistát	k5eAaImAgNnP	přistát
na	na	k7c6	na
území	území	k1gNnSc6	území
ovládané	ovládaný	k2eAgInPc1d1	ovládaný
republikánskou	republikánský	k2eAgFnSc7d1	republikánská
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vláda	vláda	k1gFnSc1	vláda
požádala	požádat	k5eAaPmAgFnS	požádat
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
o	o	k7c4	o
dodávky	dodávka	k1gFnPc4	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
mělo	mít	k5eAaImAgNnS	mít
Španělsko	Španělsko	k1gNnSc1	Španělsko
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
socialistický	socialistický	k2eAgMnSc1d1	socialistický
premiér	premiér	k1gMnSc1	premiér
León	Leóna	k1gFnPc2	Leóna
Blum	bluma	k1gFnPc2	bluma
nejprve	nejprve	k6eAd1	nejprve
pomoc	pomoc	k1gFnSc4	pomoc
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
francouzských	francouzský	k2eAgFnPc2d1	francouzská
pravicových	pravicový	k2eAgFnPc2d1	pravicová
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
tuto	tento	k3xDgFnSc4	tento
pomoc	pomoc	k1gFnSc4	pomoc
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
dotlačit	dotlačit	k5eAaPmF	dotlačit
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Itálii	Itálie	k1gFnSc4	Itálie
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
dodávky	dodávka	k1gFnPc4	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
podpis	podpis	k1gInSc4	podpis
těchto	tento	k3xDgFnPc2	tento
smluv	smlouva	k1gFnPc2	smlouva
však	však	k9	však
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
dodávky	dodávka	k1gFnPc4	dodávka
německých	německý	k2eAgFnPc2d1	německá
a	a	k8xC	a
italských	italský	k2eAgFnPc2d1	italská
zbraní	zbraň	k1gFnPc2	zbraň
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nacionalisté	nacionalista	k1gMnPc1	nacionalista
"	"	kIx"	"
<g/>
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Burgosu	Burgos	k1gInSc6	Burgos
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahájila	zahájit	k5eAaPmAgFnS	zahájit
nový	nový	k2eAgInSc4d1	nový
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
postup	postup	k1gInSc1	postup
těchto	tento	k3xDgNnPc2	tento
vojsk	vojsko	k1gNnPc2	vojsko
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
pád	pád	k1gInSc1	pád
Giralovy	Giralův	k2eAgFnSc2d1	Giralův
republikánské	republikánský	k2eAgFnSc2d1	republikánská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
vláda	vláda	k1gFnSc1	vláda
lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
především	především	k9	především
ze	z	k7c2	z
socialistů	socialist	k1gMnPc2	socialist
a	a	k8xC	a
komunistů	komunista	k1gMnPc2	komunista
(	(	kIx(	(
<g/>
premiér	premiér	k1gMnSc1	premiér
Francisco	Francisco	k1gMnSc1	Francisco
Largo	largo	k1gNnSc1	largo
Caballero	caballero	k1gMnSc1	caballero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fungování	fungování	k1gNnSc4	fungování
této	tento	k3xDgFnSc2	tento
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uvnitř	uvnitř	k7c2	uvnitř
vlády	vláda	k1gFnSc2	vláda
neexistovala	existovat	k5eNaImAgFnS	existovat
shoda	shoda	k1gFnSc1	shoda
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
postupu	postup	k1gInSc6	postup
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
vláda	vláda	k1gFnSc1	vláda
nakonec	nakonec	k6eAd1	nakonec
odhodlala	odhodlat	k5eAaPmAgFnS	odhodlat
k	k	k7c3	k
razantním	razantní	k2eAgInPc3d1	razantní
krokům	krok	k1gInPc3	krok
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
autonomii	autonomie	k1gFnSc4	autonomie
Baskicku	Baskicko	k1gNnSc3	Baskicko
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vyvlastnila	vyvlastnit	k5eAaPmAgFnS	vyvlastnit
půdu	půda	k1gFnSc4	půda
velkostatkářům	velkostatkář	k1gMnPc3	velkostatkář
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
ji	on	k3xPp3gFnSc4	on
rozdávat	rozdávat	k5eAaImF	rozdávat
malým	malý	k2eAgMnPc3d1	malý
rolníkům	rolník	k1gMnPc3	rolník
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
spojila	spojit	k5eAaPmAgFnS	spojit
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
armádu	armáda	k1gFnSc4	armáda
s	s	k7c7	s
milicemi	milice	k1gFnPc7	milice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
dobyla	dobýt	k5eAaPmAgFnS	dobýt
armáda	armáda	k1gFnSc1	armáda
povstalců	povstalec	k1gMnPc2	povstalec
San	San	k1gFnSc2	San
Sebastián	Sebastián	k1gMnSc1	Sebastián
a	a	k8xC	a
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
frontě	fronta	k1gFnSc6	fronta
pak	pak	k6eAd1	pak
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Frankovy	Frankův	k2eAgFnPc4d1	Frankova
jednotky	jednotka	k1gFnPc4	jednotka
Toledo	Toledo	k1gNnSc4	Toledo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
i	i	k9	i
obránce	obránce	k1gMnSc1	obránce
Toledského	toledský	k2eAgInSc2d1	toledský
Alcázaru	Alcázar	k1gInSc2	Alcázar
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stali	stát	k5eAaPmAgMnP	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
legend	legenda	k1gFnPc2	legenda
frankistického	frankistický	k2eAgInSc2d1	frankistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
si	se	k3xPyFc3	se
povstalci	povstalec	k1gMnPc1	povstalec
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Madrid	Madrid	k1gInSc4	Madrid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1936	[number]	k4	1936
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
se	se	k3xPyFc4	se
frankisté	frankista	k1gMnPc1	frankista
politicky	politicky	k6eAd1	politicky
stabilizovali	stabilizovat	k5eAaBmAgMnP	stabilizovat
a	a	k8xC	a
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
přijal	přijmout	k5eAaPmAgMnS	přijmout
Francisco	Francisco	k1gMnSc1	Francisco
Franco	Franco	k1gMnSc1	Franco
moc	moc	k6eAd1	moc
od	od	k7c2	od
Výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
Salamance	Salamanka	k1gFnSc6	Salamanka
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgMnS	obdržet
titul	titul	k1gInSc4	titul
generalissimus	generalissimus	k1gMnSc1	generalissimus
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
faktickým	faktický	k2eAgMnSc7d1	faktický
vládcem	vládce	k1gMnSc7	vládce
povstalci	povstalec	k1gMnPc7	povstalec
kontrolovaných	kontrolovaný	k2eAgNnPc2d1	kontrolované
území	území	k1gNnPc2	území
a	a	k8xC	a
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
vojenským	vojenský	k2eAgMnSc7d1	vojenský
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neutralita	neutralita	k1gFnSc1	neutralita
a	a	k8xC	a
zbrojní	zbrojní	k2eAgNnSc1d1	zbrojní
embargo	embargo	k1gNnSc1	embargo
již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
porušovány	porušovat	k5eAaImNgFnP	porušovat
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
operovala	operovat	k5eAaImAgFnS	operovat
německá	německý	k2eAgFnSc1d1	německá
Legie	legie	k1gFnSc1	legie
Condor	Condora	k1gFnPc2	Condora
a	a	k8xC	a
italské	italský	k2eAgFnSc2d1	italská
jednotky	jednotka	k1gFnSc2	jednotka
Corpo	Corpa	k1gFnSc5	Corpa
Truppe	Trupp	k1gInSc5	Trupp
Volontarie	Volontarie	k1gFnSc1	Volontarie
(	(	kIx(	(
<g/>
CTV	CTV	kA	CTV
<g/>
)	)	kIx)	)
bojující	bojující	k2eAgMnSc1d1	bojující
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
generála	generál	k1gMnSc2	generál
Franca	Franca	k?	Franca
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydal	vydat	k5eAaPmAgInS	vydat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
protest	protest	k1gInSc1	protest
proti	proti	k7c3	proti
porušování	porušování	k1gNnSc3	porušování
embarga	embargo	k1gNnSc2	embargo
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
španělským	španělský	k2eAgMnPc3d1	španělský
frankistickým	frankistický	k2eAgMnPc3d1	frankistický
povstalcům	povstalec	k1gMnPc3	povstalec
a	a	k8xC	a
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
začal	začít	k5eAaPmAgMnS	začít
dodávat	dodávat	k5eAaImF	dodávat
republikánské	republikánský	k2eAgFnSc3d1	republikánská
vládě	vláda	k1gFnSc3	vláda
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
placeny	platit	k5eAaImNgInP	platit
zlatem	zlato	k1gNnSc7	zlato
ze	z	k7c2	z
španělských	španělský	k2eAgFnPc2d1	španělská
zlatých	zlatý	k2eAgFnPc2d1	zlatá
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
republikánů	republikán	k1gMnPc2	republikán
bojovali	bojovat	k5eAaImAgMnP	bojovat
tzv.	tzv.	kA	tzv.
interbrigadisté	interbrigadista	k1gMnPc1	interbrigadista
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byli	být	k5eAaImAgMnP	být
antifašističtí	antifašistický	k2eAgMnPc1d1	antifašistický
levicoví	levicový	k2eAgMnPc1d1	levicový
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
ze	z	k7c2	z
17	[number]	k4	17
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Od	od	k7c2	od
obležení	obležení	k1gNnSc2	obležení
Madridu	Madrid	k1gInSc2	Madrid
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
do	do	k7c2	do
dobytí	dobytí	k1gNnSc2	dobytí
Gijónu	Gijón	k1gInSc2	Gijón
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
se	se	k3xPyFc4	se
Francovy	Francův	k2eAgInPc1d1	Francův
oddíly	oddíl	k1gInPc1	oddíl
dostaly	dostat	k5eAaPmAgInP	dostat
k	k	k7c3	k
Madridu	Madrid	k1gInSc3	Madrid
<g/>
,	,	kIx,	,
republikáni	republikán	k1gMnPc1	republikán
přeložili	přeložit	k5eAaPmAgMnP	přeložit
sídlo	sídlo	k1gNnSc4	sídlo
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
Valencie	Valencie	k1gFnSc2	Valencie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
popravili	popravit	k5eAaPmAgMnP	popravit
republikáni	republikán	k1gMnPc1	republikán
José	Josý	k2eAgFnSc2d1	Josý
Antonis	Antonis	k1gFnSc2	Antonis
Primo	primo	k1gNnSc1	primo
de	de	k?	de
Riveru	River	k1gInSc2	River
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
Madridu	Madrid	k1gInSc2	Madrid
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
interbrigadisté	interbrigadista	k1gMnPc1	interbrigadista
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gNnSc3	jejich
odhodlání	odhodlání	k1gNnSc3	odhodlání
skončilo	skončit	k5eAaPmAgNnS	skončit
toto	tento	k3xDgNnSc1	tento
obléhání	obléhání	k1gNnSc1	obléhání
neúspěšně	úspěšně	k6eNd1	úspěšně
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1937	[number]	k4	1937
se	se	k3xPyFc4	se
frankistické	frankistický	k2eAgInPc1d1	frankistický
oddíly	oddíl	k1gInPc1	oddíl
musely	muset	k5eAaImAgInP	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgFnSc4d1	další
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
neúspěšnou	úspěšný	k2eNgFnSc4d1	neúspěšná
<g/>
)	)	kIx)	)
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
na	na	k7c4	na
Madrid	Madrid	k1gInSc4	Madrid
se	se	k3xPyFc4	se
Franco	Franco	k6eAd1	Franco
pokusil	pokusit	k5eAaPmAgMnS	pokusit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
dobyli	dobýt	k5eAaPmAgMnP	dobýt
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
bitvě	bitva	k1gFnSc6	bitva
frankisté	frankista	k1gMnPc1	frankista
Malagu	Malaga	k1gFnSc4	Malaga
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
března	březen	k1gInSc2	březen
zahájili	zahájit	k5eAaPmAgMnP	zahájit
ofenzivu	ofenziva	k1gFnSc4	ofenziva
severně	severně	k6eAd1	severně
od	od	k7c2	od
Madridu	Madrid	k1gInSc2	Madrid
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
nepřenesla	přenést	k5eNaPmAgFnS	přenést
boje	boj	k1gInSc2	boj
k	k	k7c3	k
Madridu	Madrid	k1gInSc3	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
frankisté	frankista	k1gMnPc1	frankista
vyšší	vysoký	k2eAgFnSc4d2	vyšší
aktivitu	aktivita	k1gFnSc4	aktivita
v	v	k7c6	v
Baskicku	Baskicko	k1gNnSc6	Baskicko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Guernicu	Guernica	k1gFnSc4	Guernica
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
vybombardována	vybombardovat	k5eAaPmNgNnP	vybombardovat
německými	německý	k2eAgNnPc7d1	německé
letadly	letadlo	k1gNnPc7	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
mezitím	mezitím	k6eAd1	mezitím
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
zákaz	zákaz	k1gInSc1	zákaz
Ligy	liga	k1gFnSc2	liga
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
vměšovat	vměšovat	k5eAaImF	vměšovat
a	a	k8xC	a
poskytovat	poskytovat	k5eAaImF	poskytovat
Španělsku	Španělsko	k1gNnSc3	Španělsko
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
<g/>
,	,	kIx,	,
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc4d1	silný
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
odchod	odchod	k1gInSc4	odchod
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
by	by	kYmCp3nS	by
více	hodně	k6eAd2	hodně
postihl	postihnout	k5eAaPmAgMnS	postihnout
republikány	republikán	k1gMnPc4	republikán
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
řadách	řada	k1gFnPc6	řada
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
odhadů	odhad	k1gInPc2	odhad
<g/>
)	)	kIx)	)
bojovalo	bojovat	k5eAaImAgNnS	bojovat
asi	asi	k9	asi
35	[number]	k4	35
000	[number]	k4	000
interbrigadistů	interbrigadista	k1gMnPc2	interbrigadista
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
minimálně	minimálně	k6eAd1	minimálně
20	[number]	k4	20
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
i	i	k9	i
dvě	dva	k4xCgNnPc4	dva
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgFnPc4d1	důležitá
encykliky	encyklika	k1gFnPc4	encyklika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Mit	Mit	k1gFnSc1	Mit
brennender	brennendra	k1gFnPc2	brennendra
Sorge	Sorge	k1gFnPc2	Sorge
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
nacismus	nacismus	k1gInSc1	nacismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Divini	divin	k2eAgMnPc1d1	divin
redemptoris	redemptoris	k1gFnPc2	redemptoris
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
komunismus	komunismus	k1gInSc1	komunismus
a	a	k8xC	a
výslovně	výslovně	k6eAd1	výslovně
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
kruté	krutý	k2eAgNnSc4d1	kruté
pronásledování	pronásledování	k1gNnSc4	pronásledování
a	a	k8xC	a
vraždění	vraždění	k1gNnSc4	vraždění
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
duchovních	duchovní	k1gMnPc2	duchovní
španělskými	španělský	k2eAgMnPc7d1	španělský
republikány	republikán	k1gMnPc7	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
nepřímo	přímo	k6eNd1	přímo
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
španělské	španělský	k2eAgNnSc4d1	španělské
politické	politický	k2eAgNnSc4d1	politické
smýšlení	smýšlení	k1gNnSc4	smýšlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
ještě	ještě	k6eAd1	ještě
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
vnitrorepublikánskými	vnitrorepublikánský	k2eAgInPc7d1	vnitrorepublikánský
střety	střet	k1gInPc7	střet
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
největší	veliký	k2eAgFnSc4d3	veliký
gradaci	gradace	k1gFnSc4	gradace
představovaly	představovat	k5eAaImAgInP	představovat
májové	májový	k2eAgInPc1d1	májový
nepokoje	nepokoj	k1gInPc1	nepokoj
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
započaly	započnout	k5eAaPmAgFnP	započnout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
komunistické	komunistický	k2eAgInPc1d1	komunistický
oddíly	oddíl	k1gInPc1	oddíl
pokusily	pokusit	k5eAaPmAgInP	pokusit
násilím	násilí	k1gNnSc7	násilí
obsadit	obsadit	k5eAaPmF	obsadit
důležitá	důležitý	k2eAgNnPc4d1	důležité
stanoviště	stanoviště	k1gNnPc4	stanoviště
anarchistů	anarchista	k1gMnPc2	anarchista
a	a	k8xC	a
POUM	POUM	kA	POUM
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
několik	několik	k4yIc4	několik
ozbrojených	ozbrojený	k2eAgInPc2d1	ozbrojený
střetů	střet	k1gInPc2	střet
<g/>
.	.	kIx.	.
</s>
<s>
Socialisté	socialist	k1gMnPc1	socialist
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
akci	akce	k1gFnSc4	akce
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
anarchistický	anarchistický	k2eAgInSc4d1	anarchistický
puč	puč	k1gInSc4	puč
a	a	k8xC	a
reagovali	reagovat	k5eAaBmAgMnP	reagovat
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
padla	padnout	k5eAaImAgFnS	padnout
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Republikáni	republikán	k1gMnPc1	republikán
sestavili	sestavit	k5eAaPmAgMnP	sestavit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
socialista	socialista	k1gMnSc1	socialista
Juan	Juan	k1gMnSc1	Juan
Negrín	Negrín	k1gMnSc1	Negrín
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
zásadní	zásadní	k2eAgInPc4d1	zásadní
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
posílit	posílit	k5eAaPmF	posílit
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
situaci	situace	k1gFnSc4	situace
republikánů	republikán	k1gMnPc2	republikán
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
některé	některý	k3yIgFnPc4	některý
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
(	(	kIx(	(
<g/>
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
výrazně	výrazně	k6eAd1	výrazně
zasahovali	zasahovat	k5eAaImAgMnP	zasahovat
sovětští	sovětský	k2eAgMnPc1d1	sovětský
poradci	poradce	k1gMnPc1	poradce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
republikáni	republikán	k1gMnPc1	republikán
porazili	porazit	k5eAaPmAgMnP	porazit
frankisty	frankista	k1gMnPc7	frankista
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Guadalajary	Guadalajara	k1gFnSc2	Guadalajara
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
si	se	k3xPyFc3	se
Franco	Franco	k1gMnSc1	Franco
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ani	ani	k8xC	ani
dobýt	dobýt	k5eAaPmF	dobýt
Madrid	Madrid	k1gInSc4	Madrid
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
přerušit	přerušit	k5eAaPmF	přerušit
jeho	jeho	k3xOp3gNnSc4	jeho
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Valencií	Valencie	k1gFnSc7	Valencie
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Republikáni	republikán	k1gMnPc1	republikán
vítězství	vítězství	k1gNnPc2	vítězství
u	u	k7c2	u
Guadalajary	Guadalajara	k1gFnSc2	Guadalajara
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
nijak	nijak	k6eAd1	nijak
významněji	významně	k6eAd2	významně
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
prosovětský	prosovětský	k2eAgInSc4d1	prosovětský
J.	J.	kA	J.
Negrín	Negrín	k1gInSc1	Negrín
nastolil	nastolit	k5eAaPmAgInS	nastolit
diktaturu	diktatura	k1gFnSc4	diktatura
sovětského	sovětský	k2eAgInSc2d1	sovětský
typu	typ	k1gInSc2	typ
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
čistky	čistka	k1gFnPc4	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byly	být	k5eAaImAgFnP	být
pozatýkáni	pozatýkán	k2eAgMnPc1d1	pozatýkán
členové	člen	k1gMnPc1	člen
vedení	vedení	k1gNnSc4	vedení
POUMu	POUMus	k1gInSc2	POUMus
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
republikáni	republikán	k1gMnPc1	republikán
ještě	ještě	k6eAd1	ještě
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
silnou	silný	k2eAgFnSc4d1	silná
protiofenzivu	protiofenziva	k1gFnSc4	protiofenziva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Franco	Franco	k6eAd1	Franco
zatím	zatím	k6eAd1	zatím
na	na	k7c6	na
severu	sever	k1gInSc6	sever
dobyl	dobýt	k5eAaPmAgInS	dobýt
Bilbao	Bilbao	k6eAd1	Bilbao
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
zahájil	zahájit	k5eAaPmAgMnS	zahájit
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Santander	Santander	k1gInSc4	Santander
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgInS	být
odražen	odražen	k2eAgInSc1d1	odražen
u	u	k7c2	u
Aragonu	Aragon	k1gInSc2	Aragon
<g/>
.	.	kIx.	.
</s>
<s>
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
protiofenziva	protiofenziva	k1gFnSc1	protiofenziva
však	však	k9	však
nepřinesla	přinést	k5eNaPmAgFnS	přinést
žádné	žádný	k3yNgInPc4	žádný
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Gijónu	Gijón	k1gInSc2	Gijón
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
republikánská	republikánský	k2eAgFnSc1d1	republikánská
severní	severní	k2eAgFnSc1d1	severní
fronta	fronta	k1gFnSc1	fronta
zcela	zcela	k6eAd1	zcela
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	s	k7c7	s
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
republikánská	republikánský	k2eAgFnSc1d1	republikánská
vláda	vláda	k1gFnSc1	vláda
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
úspěchy	úspěch	k1gInPc1	úspěch
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
krutým	krutý	k2eAgNnSc7d1	kruté
pronásledováním	pronásledování	k1gNnSc7	pronásledování
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
masovým	masový	k2eAgNnSc7d1	masové
vražděním	vraždění	k1gNnSc7	vraždění
duchovních	duchovní	k1gMnPc2	duchovní
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
republikánského	republikánský	k2eAgNnSc2d1	republikánské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Vatikán	Vatikán	k1gInSc1	Vatikán
uznal	uznat	k5eAaPmAgMnS	uznat
Franca	Franca	k?	Franca
jako	jako	k8xS	jako
legitimního	legitimní	k2eAgMnSc4d1	legitimní
představitele	představitel	k1gMnSc4	představitel
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Od	od	k7c2	od
dobytí	dobytí	k1gNnSc2	dobytí
Gijónu	Gijón	k1gInSc2	Gijón
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Gijónu	Gijón	k1gInSc2	Gijón
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
k	k	k7c3	k
přeskupování	přeskupování	k1gNnSc3	přeskupování
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
zahájila	zahájit	k5eAaPmAgFnS	zahájit
republikánská	republikánský	k2eAgFnSc1d1	republikánská
vojska	vojsko	k1gNnSc2	vojsko
protiofenzívu	protiofenzíva	k1gFnSc4	protiofenzíva
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
frankisté	frankista	k1gMnPc1	frankista
bojově	bojově	k6eAd1	bojově
výrazně	výrazně	k6eAd1	výrazně
nereagovali	reagovat	k5eNaBmAgMnP	reagovat
a	a	k8xC	a
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
republikáni	republikán	k1gMnPc1	republikán
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Teruel	Teruel	k1gInSc4	Teruel
v	v	k7c6	v
Aragonii	Aragonie	k1gFnSc6	Aragonie
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
důležité	důležitý	k2eAgNnSc1d1	důležité
město	město	k1gNnSc1	město
však	však	k9	však
ztratili	ztratit	k5eAaPmAgMnP	ztratit
již	již	k6eAd1	již
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1938	[number]	k4	1938
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Teruel	Teruel	k1gInSc4	Teruel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bitva	bitva	k1gFnSc1	bitva
měla	mít	k5eAaImAgFnS	mít
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
dopad	dopad	k1gInSc4	dopad
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
frankisté	frankista	k1gMnPc1	frankista
se	se	k3xPyFc4	se
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
svému	svůj	k3xOyFgInSc3	svůj
cíli	cíl	k1gInSc3	cíl
rozdělit	rozdělit	k5eAaPmF	rozdělit
republikánské	republikánský	k2eAgNnSc4d1	republikánské
území	území	k1gNnSc4	území
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
Burgosu	Burgos	k1gInSc6	Burgos
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
generál	generál	k1gMnSc1	generál
Franco	Franco	k1gMnSc1	Franco
vojensko-civilní	vojenskoivilní	k2eAgFnSc4d1	vojensko-civilní
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
republikáni	republikán	k1gMnPc1	republikán
posledního	poslední	k2eAgInSc2d1	poslední
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
jejich	jejich	k3xOp3gNnSc1	jejich
loďstvo	loďstvo	k1gNnSc1	loďstvo
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
v	v	k7c6	v
noční	noční	k2eAgFnSc6d1	noční
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
mysu	mys	k1gInSc2	mys
Palos	Palosa	k1gFnPc2	Palosa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
války	válka	k1gFnSc2	válka
to	ten	k3xDgNnSc1	ten
však	však	k9	však
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
nemělo	mít	k5eNaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchem	úspěch	k1gInSc7	úspěch
frankistů	frankista	k1gMnPc2	frankista
bylo	být	k5eAaImAgNnS	být
bombardování	bombardování	k1gNnSc1	bombardování
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
postupu	postup	k1gInSc6	postup
pronikli	proniknout	k5eAaPmAgMnP	proniknout
frankisté	frankista	k1gMnPc1	frankista
až	až	k6eAd1	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Ebro	Ebro	k6eAd1	Ebro
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
rozdělit	rozdělit	k5eAaPmF	rozdělit
republikánské	republikánský	k2eAgNnSc4d1	republikánské
území	území	k1gNnSc4	území
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
dojednat	dojednat	k5eAaPmF	dojednat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
Franco	Franco	k6eAd1	Franco
požadoval	požadovat	k5eAaImAgMnS	požadovat
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1	bezpodmínečná
kapitulaci	kapitulace	k1gFnSc4	kapitulace
<g/>
,	,	kIx,	,
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
republikánům	republikán	k1gMnPc3	republikán
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
Bitvu	bitva	k1gFnSc4	bitva
o	o	k7c4	o
Ebro	Ebro	k1gNnSc4	Ebro
<g/>
.	.	kIx.	.
</s>
<s>
Republikáni	republikán	k1gMnPc1	republikán
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přednést	přednést	k5eAaPmF	přednést
Radě	rada	k1gFnSc3	rada
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
návrh	návrh	k1gInSc1	návrh
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
politiky	politika	k1gFnSc2	politika
nezasahování	nezasahování	k1gNnSc2	nezasahování
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgInS	být
dohodnut	dohodnut	k2eAgInSc1d1	dohodnut
odchod	odchod	k1gInSc1	odchod
interbrigád	interbrigáda	k1gFnPc2	interbrigáda
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
republikáni	republikán	k1gMnPc1	republikán
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
udržet	udržet	k5eAaPmF	udržet
stav	stav	k1gInSc4	stav
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ebro	Ebro	k1gMnSc1	Ebro
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
dobýt	dobýt	k5eAaPmF	dobýt
ztracené	ztracený	k2eAgFnPc4d1	ztracená
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
stránce	stránka	k1gFnSc6	stránka
si	se	k3xPyFc3	se
vedli	vést	k5eAaImAgMnP	vést
víceméně	víceméně	k9	víceméně
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
ztratili	ztratit	k5eAaPmAgMnP	ztratit
poslední	poslední	k2eAgInPc4d1	poslední
zbytky	zbytek	k1gInPc4	zbytek
naděje	naděje	k1gFnSc2	naděje
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
antifašistické	antifašistický	k2eAgFnSc2d1	Antifašistická
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
morálka	morálka	k1gFnSc1	morálka
definitivně	definitivně	k6eAd1	definitivně
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
však	však	k9	však
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
nutnost	nutnost	k1gFnSc4	nutnost
udržet	udržet	k5eAaPmF	udržet
tuto	tento	k3xDgFnSc4	tento
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
bráněna	bráněn	k2eAgFnSc1d1	bráněna
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
zahájili	zahájit	k5eAaPmAgMnP	zahájit
frankisté	frankista	k1gMnPc1	frankista
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Katalánsko	Katalánsko	k1gNnSc4	Katalánsko
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
dobyli	dobýt	k5eAaPmAgMnP	dobýt
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
<g/>
:	:	kIx,	:
Tarragona	Tarragona	k1gFnSc1	Tarragona
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Girona	Girona	k1gFnSc1	Girona
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byl	být	k5eAaImAgInS	být
zlomen	zlomen	k2eAgInSc1d1	zlomen
poslední	poslední	k2eAgInSc1d1	poslední
odpor	odpor	k1gInSc1	odpor
v	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
vlády	vláda	k1gFnSc2	vláda
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
uznaly	uznat	k5eAaPmAgFnP	uznat
Frankův	Frankův	k2eAgInSc4d1	Frankův
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
republikánům	republikán	k1gMnPc3	republikán
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k6eAd1	jen
Madrid	Madrid	k1gInSc1	Madrid
a	a	k8xC	a
několik	několik	k4yIc1	několik
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
Valencie	Valencie	k1gFnSc1	Valencie
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
zlomen	zlomit	k5eAaPmNgInS	zlomit
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účastníci	účastník	k1gMnPc1	účastník
konfliktu	konflikt	k1gInSc2	konflikt
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
vtaženy	vtáhnout	k5eAaPmNgFnP	vtáhnout
všechny	všechen	k3xTgFnPc1	všechen
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nacionalistická	nacionalistický	k2eAgFnSc1d1	nacionalistická
strana	strana	k1gFnSc1	strana
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
fašisty	fašista	k1gMnPc4	fašista
z	z	k7c2	z
Falangy	falanga	k1gFnSc2	falanga
<g/>
,	,	kIx,	,
karlisty	karlista	k1gMnPc4	karlista
<g/>
,	,	kIx,	,
monarchisty	monarchista	k1gMnPc4	monarchista
<g/>
,	,	kIx,	,
španělské	španělský	k2eAgMnPc4d1	španělský
nacionalisty	nacionalista	k1gMnPc4	nacionalista
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
konzervativců	konzervativec	k1gMnPc2	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
primární	primární	k2eAgFnSc7d1	primární
základnou	základna	k1gFnSc7	základna
byl	být	k5eAaImAgInS	být
venkov	venkov	k1gInSc1	venkov
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
vlastníci	vlastník	k1gMnPc1	vlastník
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Podporovala	podporovat	k5eAaImAgFnS	podporovat
je	být	k5eAaImIp3nS	být
i	i	k9	i
většina	většina	k1gFnSc1	většina
katolického	katolický	k2eAgNnSc2d1	katolické
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
i	i	k8xC	i
katolíků	katolík	k1gMnPc2	katolík
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Baskicko	Baskicko	k1gNnSc4	Baskicko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
vyšších	vysoký	k2eAgMnPc2d2	vyšší
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Republikánů	republikán	k1gMnPc2	republikán
bojovala	bojovat	k5eAaImAgFnS	bojovat
většina	většina	k1gFnSc1	většina
liberálů	liberál	k1gMnPc2	liberál
<g/>
,	,	kIx,	,
Baskové	Bask	k1gMnPc1	Bask
a	a	k8xC	a
Katalánští	katalánský	k2eAgMnPc1d1	katalánský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
<g/>
,	,	kIx,	,
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
stalinističtí	stalinistický	k2eAgMnPc1d1	stalinistický
a	a	k8xC	a
trockističtí	trockistický	k2eAgMnPc1d1	trockistický
komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
anarchisté	anarchista	k1gMnPc1	anarchista
<g/>
.	.	kIx.	.
</s>
<s>
Minoritní	minoritní	k2eAgFnSc7d1	minoritní
součástí	součást	k1gFnSc7	součást
republikánských	republikánský	k2eAgFnPc2d1	republikánská
sil	síla	k1gFnPc2	síla
byli	být	k5eAaImAgMnP	být
takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
centristé	centrista	k1gMnPc1	centrista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podporovali	podporovat	k5eAaImAgMnP	podporovat
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
stran	strana	k1gFnPc2	strana
však	však	k9	však
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stran	strana	k1gFnPc2	strana
měly	mít	k5eAaImAgFnP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k9	i
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
mezi	mezi	k7c7	mezi
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
primární	primární	k2eAgFnSc7d1	primární
základnou	základna	k1gFnSc7	základna
byla	být	k5eAaImAgFnS	být
nižší	nízký	k2eAgFnSc1d2	nižší
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
rolníci	rolník	k1gMnPc1	rolník
a	a	k8xC	a
intelektuálové	intelektuál	k1gMnPc1	intelektuál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnSc1	reakce
světa	svět	k1gInSc2	svět
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
postavila	postavit	k5eAaPmAgFnS	postavit
politikou	politika	k1gFnSc7	politika
nevměšování	nevměšování	k1gNnSc2	nevměšování
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
propagátorem	propagátor	k1gMnSc7	propagátor
této	tento	k3xDgFnSc2	tento
politiky	politika	k1gFnSc2	politika
byla	být	k5eAaImAgNnP	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
války	válka	k1gFnSc2	válka
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
původní	původní	k2eAgFnSc4d1	původní
pomoc	pomoc	k1gFnSc4	pomoc
republikánům	republikán	k1gMnPc3	republikán
tuto	tento	k3xDgFnSc4	tento
politiku	politika	k1gFnSc4	politika
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
přijala	přijmout	k5eAaPmAgFnS	přijmout
i	i	k9	i
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
prosadily	prosadit	k5eAaPmAgFnP	prosadit
tuto	tento	k3xDgFnSc4	tento
politiku	politika	k1gFnSc4	politika
jako	jako	k8xC	jako
celoevropský	celoevropský	k2eAgInSc4d1	celoevropský
trend	trend	k1gInSc4	trend
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
zbrojního	zbrojní	k2eAgNnSc2d1	zbrojní
embarga	embargo	k1gNnSc2	embargo
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
teorií	teorie	k1gFnSc7	teorie
izolacionismu	izolacionismus	k1gInSc2	izolacionismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
velké	velký	k2eAgFnPc1d1	velká
firmy	firma	k1gFnPc1	firma
obchodující	obchodující	k2eAgFnPc1d1	obchodující
s	s	k7c7	s
pohonnými	pohonný	k2eAgFnPc7d1	pohonná
hmotami	hmota	k1gFnPc7	hmota
daly	dát	k5eAaPmAgFnP	dát
najevo	najevo	k6eAd1	najevo
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
neprodávaly	prodávat	k5eNaImAgFnP	prodávat
republikánským	republikánský	k2eAgNnPc3d1	republikánské
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
politiku	politika	k1gFnSc4	politika
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
frankistům	frankista	k1gMnPc3	frankista
nejen	nejen	k6eAd1	nejen
technickou	technický	k2eAgFnSc7d1	technická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
Legie	legie	k1gFnSc1	legie
Condor	Condora	k1gFnPc2	Condora
a	a	k8xC	a
Corpo	Corpa	k1gFnSc5	Corpa
Truppe	Trupp	k1gInSc5	Trupp
Volontarie	Volontarie	k1gFnSc2	Volontarie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mussoliniho	Mussolinize	k6eAd1	Mussolinize
Itálie	Itálie	k1gFnSc1	Itálie
vyslala	vyslat	k5eAaPmAgFnS	vyslat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
frankistům	frankista	k1gMnPc3	frankista
na	na	k7c4	na
50	[number]	k4	50
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mezi	mezi	k7c7	mezi
frankisty	frankista	k1gMnPc7	frankista
bojovali	bojovat	k5eAaImAgMnP	bojovat
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počet	počet	k1gInSc1	počet
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
se	se	k3xPyFc4	se
zdaleka	zdaleka	k6eAd1	zdaleka
neblížil	blížit	k5eNaImAgMnS	blížit
počtu	počet	k1gInSc3	počet
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Republikánům	republikán	k1gMnPc3	republikán
posílal	posílat	k5eAaImAgMnS	posílat
technickou	technický	k2eAgFnSc4d1	technická
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Republikáni	republikán	k1gMnPc1	republikán
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
pomoc	pomoc	k1gFnSc4	pomoc
platili	platit	k5eAaImAgMnP	platit
zlatými	zlatý	k2eAgFnPc7d1	zlatá
rezervami	rezerva	k1gFnPc7	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
republikánů	republikán	k1gMnPc2	republikán
přišlo	přijít	k5eAaPmAgNnS	přijít
mnoho	mnoho	k6eAd1	mnoho
(	(	kIx(	(
<g/>
asi	asi	k9	asi
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
antifašistických	antifašistický	k2eAgMnPc2d1	antifašistický
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
organizovali	organizovat	k5eAaBmAgMnP	organizovat
do	do	k7c2	do
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
brigád	brigáda	k1gFnPc2	brigáda
–	–	k?	–
odtud	odtud	k6eAd1	odtud
výraz	výraz	k1gInSc1	výraz
interbrigadisté	interbrigadista	k1gMnPc1	interbrigadista
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
2000	[number]	k4	2000
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
bojovali	bojovat	k5eAaImAgMnP	bojovat
jako	jako	k9	jako
členové	člen	k1gMnPc1	člen
milicí	milice	k1gFnPc2	milice
CNT	CNT	kA	CNT
a	a	k8xC	a
POUM	POUM	kA	POUM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc1	odraz
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
faktu	fakt	k1gInSc3	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
uměleckého	umělecký	k2eAgInSc2d1	umělecký
života	život	k1gInSc2	život
bojovalo	bojovat	k5eAaImAgNnS	bojovat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
republikánů	republikán	k1gMnPc2	republikán
<g/>
,	,	kIx,	,
odrazila	odrazit	k5eAaPmAgFnS	odrazit
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
poměrně	poměrně	k6eAd1	poměrně
masově	masově	k6eAd1	masově
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
interbrigádách	interbrigáda	k1gFnPc6	interbrigáda
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
ideály	ideál	k1gInPc7	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejslavnější	slavný	k2eAgMnPc4d3	nejslavnější
cizince	cizinec	k1gMnPc4	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
na	na	k7c6	na
republikánské	republikánský	k2eAgFnSc6d1	republikánská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
a	a	k8xC	a
George	Georg	k1gFnSc2	Georg
Orwell	Orwella	k1gFnPc2	Orwella
<g/>
.	.	kIx.	.
</s>
<s>
Orwella	Orwella	k6eAd1	Orwella
jeho	jeho	k3xOp3gFnSc4	jeho
činnost	činnost	k1gFnSc4	činnost
zde	zde	k6eAd1	zde
nakonec	nakonec	k6eAd1	nakonec
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
jeho	jeho	k3xOp3gFnPc2	jeho
tří	tři	k4xCgFnPc2	tři
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
knih	kniha	k1gFnPc2	kniha
Hold	hold	k1gInSc1	hold
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
,	,	kIx,	,
Farma	farma	k1gFnSc1	farma
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hemingwayově	Hemingwayův	k2eAgFnSc6d1	Hemingwayova
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
odrazila	odrazit	k5eAaPmAgFnS	odrazit
především	především	k6eAd1	především
v	v	k7c6	v
románu	román	k1gInSc6	román
Komu	kdo	k3yQnSc3	kdo
zvoní	zvonit	k5eAaImIp3nP	zvonit
hrana	hrana	k1gNnPc4	hrana
a	a	k8xC	a
dramatu	drama	k1gNnSc2	drama
Pátá	pátá	k1gFnSc1	pátá
kolona	kolona	k1gFnSc1	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
trilogie	trilogie	k1gFnSc2	trilogie
Moment	moment	k1gInSc1	moment
války	válka	k1gFnSc2	válka
od	od	k7c2	od
Laurie	Laurie	k1gFnSc2	Laurie
Lee	Lea	k1gFnSc3	Lea
je	být	k5eAaImIp3nS	být
také	také	k9	také
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
civilních	civilní	k2eAgInPc6d1	civilní
válečných	válečný	k2eAgInPc6d1	válečný
zážitcích	zážitek	k1gInPc6	zážitek
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
správnost	správnost	k1gFnSc4	správnost
některých	některý	k3yIgFnPc2	některý
jeho	jeho	k3xOp3gFnPc2	jeho
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
většinově	většinově	k6eAd1	většinově
levicový	levicový	k2eAgInSc4d1	levicový
postoj	postoj	k1gInSc4	postoj
uměleckého	umělecký	k2eAgInSc2d1	umělecký
světa	svět	k1gInSc2	svět
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
slavné	slavný	k2eAgMnPc4d1	slavný
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
či	či	k8xC	či
sympatizanty	sympatizant	k1gMnPc4	sympatizant
na	na	k7c6	na
Francově	Francův	k2eAgFnSc6d1	Francova
straně	strana	k1gFnSc6	strana
např.	např.	kA	např.
Ezra	Ezrus	k1gMnSc2	Ezrus
Pound	pound	k1gInSc4	pound
<g/>
,	,	kIx,	,
Gertrude	Gertrud	k1gInSc5	Gertrud
Steinová	Steinová	k1gFnSc1	Steinová
a	a	k8xC	a
Evelyn	Evelyn	k1gMnSc1	Evelyn
Waugh	Waugh	k1gMnSc1	Waugh
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
také	také	k9	také
projevila	projevit	k5eAaPmAgFnS	projevit
v	v	k7c6	v
malířském	malířský	k2eAgNnSc6d1	malířské
díle	dílo	k1gNnSc6	dílo
Pabla	Pabl	k1gMnSc2	Pabl
Picassa	Picass	k1gMnSc2	Picass
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgInPc2d3	Nejhorší
okamžiků	okamžik	k1gInPc2	okamžik
této	tento	k3xDgFnSc2	tento
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
bombardování	bombardování	k1gNnSc2	bombardování
města	město	k1gNnSc2	město
Guernica	Guernicum	k1gNnSc2	Guernicum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filmy	film	k1gInPc7	film
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
a	a	k8xC	a
svoboda	svoboda	k1gFnSc1	svoboda
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Ken	Ken	k1gMnSc1	Ken
Loach	Loach	k1gMnSc1	Loach
</s>
</p>
<p>
<s>
Libertarias	Libertarias	k1gInSc1	Libertarias
(	(	kIx(	(
<g/>
Anarchistky	anarchistka	k1gFnSc2	anarchistka
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Vicente	Vicent	k1gInSc5	Vicent
Aranda	Arand	k1gMnSc4	Arand
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlava	hlava	k1gFnSc1	hlava
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Duigan	Duigan	k1gMnSc1	Duigan
</s>
</p>
<p>
<s>
==	==	k?	==
Sociální	sociální	k2eAgFnSc1d1	sociální
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
příčiny	příčina	k1gFnPc1	příčina
porážky	porážka	k1gFnSc2	porážka
republikánů	republikán	k1gMnPc2	republikán
==	==	k?	==
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
masivní	masivní	k2eAgFnSc4d1	masivní
podporu	podpora	k1gFnSc4	podpora
evropské	evropský	k2eAgFnSc2d1	Evropská
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
porážky	porážka	k1gFnSc2	porážka
byla	být	k5eAaImAgFnS	být
především	především	k9	především
vlastní	vlastní	k2eAgFnSc1d1	vlastní
roztříštěnost	roztříštěnost	k1gFnSc1	roztříštěnost
republikánského	republikánský	k2eAgInSc2d1	republikánský
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc4	všechen
centristické	centristický	k2eAgFnPc4d1	centristická
a	a	k8xC	a
levicové	levicový	k2eAgFnPc4d1	levicová
politické	politický	k2eAgFnPc4d1	politická
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
odborových	odborový	k2eAgFnPc2d1	odborová
organizací	organizace	k1gFnPc2	organizace
UGT	UGT	kA	UGT
a	a	k8xC	a
CNT	CNT	kA	CNT
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
komunisty	komunista	k1gMnPc7	komunista
a	a	k8xC	a
anarchisty	anarchista	k1gMnPc7	anarchista
stály	stát	k5eAaImAgFnP	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
politické	politický	k2eAgFnSc6d1	politická
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc6d1	vojenská
jednotě	jednota	k1gFnSc6	jednota
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
v	v	k7c4	v
anarchisty	anarchista	k1gMnPc4	anarchista
kontrolovaných	kontrolovaný	k2eAgFnPc6d1	kontrolovaná
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
Aragon	Aragon	k1gInSc4	Aragon
a	a	k8xC	a
Katalánsko	Katalánsko	k1gNnSc1	Katalánsko
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovské	obrovský	k2eAgFnSc3d1	obrovská
sociální	sociální	k2eAgFnSc3d1	sociální
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kolektivizaci	kolektivizace	k1gFnSc3	kolektivizace
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
revolucí	revoluce	k1gFnSc7	revoluce
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
ani	ani	k8xC	ani
sovětští	sovětský	k2eAgMnPc1d1	sovětský
komunisté	komunista	k1gMnPc1	komunista
ani	ani	k8xC	ani
demokratičtí	demokratický	k2eAgMnPc1d1	demokratický
republikáni	republikán	k1gMnPc1	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
republikáni	republikán	k1gMnPc1	republikán
porazili	porazit	k5eAaPmAgMnP	porazit
anarchisty	anarchista	k1gMnPc7	anarchista
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
anarchisté	anarchista	k1gMnPc1	anarchista
začleněni	začlenit	k5eAaPmNgMnP	začlenit
do	do	k7c2	do
oficiální	oficiální	k2eAgFnSc2d1	oficiální
republikánské	republikánský	k2eAgFnSc2d1	republikánská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
POUM	POUM	kA	POUM
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
jako	jako	k8xS	jako
údajný	údajný	k2eAgMnSc1d1	údajný
"	"	kIx"	"
<g/>
nástroj	nástroj	k1gInSc1	nástroj
fašistů	fašista	k1gMnPc2	fašista
<g/>
"	"	kIx"	"
–	–	k?	–
takto	takto	k6eAd1	takto
POUM	POUM	kA	POUM
označili	označit	k5eAaPmAgMnP	označit
komunisté	komunista	k1gMnPc1	komunista
řízení	řízení	k1gNnSc4	řízení
Kominternou	Kominterna	k1gFnSc7	Kominterna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
květnových	květnový	k2eAgInPc6d1	květnový
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
set	set	k1gInSc4	set
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
tisíc	tisíc	k4xCgInPc2	tisíc
republikánských	republikánský	k2eAgMnPc2d1	republikánský
vojáků	voják	k1gMnPc2	voják
vzájemně	vzájemně	k6eAd1	vzájemně
postřílelo	postřílet	k5eAaPmAgNnS	postřílet
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Barcelonou	Barcelona	k1gFnSc7	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1939	[number]	k4	1939
pak	pak	k6eAd1	pak
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
napětí	napětí	k1gNnSc4	napětí
na	na	k7c6	na
republikánské	republikánský	k2eAgFnSc6d1	republikánská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ještě	ještě	k6eAd1	ještě
posílily	posílit	k5eAaPmAgFnP	posílit
spory	spora	k1gFnPc1	spora
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
,	,	kIx,	,
bezpodmínečně	bezpodmínečně	k6eAd1	bezpodmínečně
se	se	k3xPyFc4	se
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
či	či	k8xC	či
bojovat	bojovat	k5eAaImF	bojovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
povstání	povstání	k1gNnSc6	povstání
a	a	k8xC	a
boje	boj	k1gInPc4	boj
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Probíhaly	probíhat	k5eAaImAgFnP	probíhat
mezi	mezi	k7c4	mezi
demokraticky	demokraticky	k6eAd1	demokraticky
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
anarchisticky	anarchisticky	k6eAd1	anarchisticky
orientovanými	orientovaný	k2eAgMnPc7d1	orientovaný
republikány	republikán	k1gMnPc7	republikán
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
komunisty	komunista	k1gMnPc7	komunista
a	a	k8xC	a
radikálními	radikální	k2eAgMnPc7d1	radikální
socialisty	socialist	k1gMnPc7	socialist
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc2	druhý
a	a	k8xC	a
skončily	skončit	k5eAaPmAgFnP	skončit
porážkou	porážka	k1gFnSc7	porážka
komunistické	komunistický	k2eAgFnPc1d1	komunistická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
lidé	člověk	k1gMnPc1	člověk
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Lidová	lidový	k2eAgFnSc1d1	lidová
fronta	fronta	k1gFnSc1	fronta
===	===	k?	===
</s>
</p>
<p>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
fronta	fronta	k1gFnSc1	fronta
byla	být	k5eAaImAgFnS	být
volební	volební	k2eAgNnSc4d1	volební
uskupení	uskupení	k1gNnSc4	uskupení
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
r.	r.	kA	r.
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
řada	řada	k1gFnSc1	řada
levicových	levicový	k2eAgFnPc2d1	levicová
a	a	k8xC	a
středových	středový	k2eAgFnPc2d1	středová
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uskupení	uskupení	k1gNnSc1	uskupení
volby	volba	k1gFnSc2	volba
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
další	další	k2eAgFnSc1d1	další
spolupráce	spolupráce	k1gFnSc1	spolupráce
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UR	Ur	k1gInSc1	Ur
(	(	kIx(	(
<g/>
Unión	Unión	k1gInSc1	Unión
Republicana	Republican	k1gMnSc2	Republican
–	–	k?	–
Republikánský	republikánský	k2eAgInSc4d1	republikánský
svaz	svaz	k1gInSc4	svaz
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vedl	vést	k5eAaImAgInS	vést
je	on	k3xPp3gNnSc4	on
Diego	Diego	k6eAd1	Diego
Martínez	Martínez	k1gMnSc1	Martínez
Barrio	Barrio	k1gMnSc1	Barrio
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
r.	r.	kA	r.
1934	[number]	k4	1934
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
PRR	PRR	kA	PRR
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Alejandro	Alejandra	k1gFnSc5	Alejandra
Lerroux	Lerroux	k1gInSc4	Lerroux
koaličně	koaličně	k6eAd1	koaličně
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
CEDA	CEDA	kA	CEDA
<g/>
.	.	kIx.	.
</s>
<s>
Podporu	podpora	k1gFnSc4	podpora
měl	mít	k5eAaImAgInS	mít
především	především	k9	především
v	v	k7c6	v
kvalifikovaných	kvalifikovaný	k2eAgFnPc6d1	kvalifikovaná
profesích	profes	k1gFnPc6	profes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IR	Ir	k1gMnSc1	Ir
(	(	kIx(	(
<g/>
Izquierda	Izquierda	k1gMnSc1	Izquierda
Republicana	Republican	k1gMnSc2	Republican
–	–	k?	–
Leví	levit	k5eAaImIp3nP	levit
republikáni	republikán	k1gMnPc1	republikán
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vedl	vést	k5eAaImAgInS	vést
ji	on	k3xPp3gFnSc4	on
bývalý	bývalý	k2eAgMnSc1d1	bývalý
premiér	premiér	k1gMnSc1	premiér
Manuel	Manuel	k1gMnSc1	Manuel
Azañ	Azañ	k1gMnSc1	Azañ
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
Acción	Acción	k1gInSc4	Acción
Republicana	Republicana	k1gFnSc1	Republicana
spojila	spojit	k5eAaPmAgFnS	spojit
se	s	k7c7	s
stranou	strana	k1gFnSc7	strana
Santiago	Santiago	k1gNnSc1	Santiago
Casares	Casares	k1gMnSc1	Casares
Quiroga	Quirog	k1gMnSc2	Quirog
PRRS	PRRS	kA	PRRS
(	(	kIx(	(
<g/>
Partido	Partida	k1gFnSc5	Partida
Republicano	Republicana	k1gFnSc5	Republicana
Radical	Radical	k1gMnPc4	Radical
Socialista	socialist	k1gMnSc2	socialist
<g/>
,	,	kIx,	,
Radikální	radikální	k2eAgFnSc1d1	radikální
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
členská	členský	k2eAgFnSc1d1	členská
základna	základna	k1gFnSc1	základna
se	se	k3xPyFc4	se
rekrutovala	rekrutovat	k5eAaImAgFnS	rekrutovat
z	z	k7c2	z
kvalifikovaných	kvalifikovaný	k2eAgFnPc2d1	kvalifikovaná
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
malých	malý	k2eAgMnPc2d1	malý
obchodníků	obchodník	k1gMnPc2	obchodník
a	a	k8xC	a
státního	státní	k2eAgNnSc2d1	státní
úřednictva	úřednictvo	k1gNnSc2	úřednictvo
<g/>
.	.	kIx.	.
</s>
<s>
Azañ	Azañ	k?	Azañ
vedl	vést	k5eAaImAgMnS	vést
Lidovou	lidový	k2eAgFnSc4d1	lidová
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
IR	Ir	k1gMnSc1	Ir
tvořila	tvořit	k5eAaImAgFnS	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
stranu	strana	k1gFnSc4	strana
první	první	k4xOgFnSc2	první
vlády	vláda	k1gFnSc2	vláda
po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ERC	ERC	kA	ERC
(	(	kIx(	(
<g/>
Esquerra	Esquerr	k1gMnSc2	Esquerr
Republicana	Republican	k1gMnSc2	Republican
de	de	k?	de
Catalunya	Cataluny	k1gInSc2	Cataluny
–	–	k?	–
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
separatistická	separatistický	k2eAgFnSc1d1	separatistická
strana	strana	k1gFnSc1	strana
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Katalánská	katalánský	k2eAgFnSc1d1	katalánská
frakce	frakce	k1gFnSc1	frakce
Azañ	Azañ	k1gFnSc2	Azañ
republikánů	republikán	k1gMnPc2	republikán
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
ji	on	k3xPp3gFnSc4	on
Lluís	Lluís	k1gInSc4	Lluís
Companys	Companysa	k1gFnPc2	Companysa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PSOE	PSOE	kA	PSOE
(	(	kIx(	(
<g/>
Partido	Partida	k1gFnSc5	Partida
Socialista	socialist	k1gMnSc2	socialist
Obrero	Obrero	k1gNnSc1	Obrero
Españ	Españ	k1gMnSc1	Españ
–	–	k?	–
(	(	kIx(	(
<g/>
Španělská	španělský	k2eAgFnSc1d1	španělská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
r.	r.	kA	r.
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
r.	r.	kA	r.
1931	[number]	k4	1931
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
s	s	k7c7	s
Acción	Acción	k1gInSc4	Acción
Republicana	Republicana	k1gFnSc1	Republicana
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
přesvědčivé	přesvědčivý	k2eAgNnSc1d1	přesvědčivé
vítězství	vítězství	k1gNnSc1	vítězství
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
abdikaci	abdikace	k1gFnSc3	abdikace
krále	král	k1gMnSc2	král
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc6	vytvoření
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
následující	následující	k2eAgFnPc1d1	následující
všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
PSOE	PSOE	kA	PSOE
opustila	opustit	k5eAaPmAgFnS	opustit
v	v	k7c6	v
r.	r.	kA	r.
1933	[number]	k4	1933
koalici	koalice	k1gFnSc3	koalice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
PSOE	PSOE	kA	PSOE
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
Indalecia	Indalecius	k1gMnSc2	Indalecius
Prieta	Priet	k1gMnSc2	Priet
<g/>
,	,	kIx,	,
středovou	středový	k2eAgFnSc4d1	středová
frakci	frakce	k1gFnSc4	frakce
Juana	Juan	k1gMnSc2	Juan
Negrína	Negrín	k1gMnSc2	Negrín
(	(	kIx(	(
<g/>
spolupracující	spolupracující	k2eAgNnSc1d1	spolupracující
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
levice	levice	k1gFnSc1	levice
měla	mít	k5eAaImAgFnS	mít
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
POUM	POUM	kA	POUM
a	a	k8xC	a
anarchistům	anarchista	k1gMnPc3	anarchista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
levé	levý	k2eAgNnSc4d1	levé
křídlo	křídlo	k1gNnSc4	křídlo
Larga	largo	k1gNnSc2	largo
Caballera	caballero	k1gMnSc2	caballero
<g/>
.	.	kIx.	.
</s>
<s>
Voličskou	voličský	k2eAgFnSc7d1	voličská
základnou	základna	k1gFnSc7	základna
byli	být	k5eAaImAgMnP	být
především	především	k9	především
dělníci	dělník	k1gMnPc1	dělník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UGT	UGT	kA	UGT
(	(	kIx(	(
<g/>
Unión	Unión	k1gMnSc1	Unión
General	General	k1gMnSc1	General
de	de	k?	de
Trabajadores	Trabajadores	k1gInSc1	Trabajadores
–	–	k?	–
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
svaz	svaz	k1gInSc1	svaz
pracujících	pracující	k1gMnPc2	pracující
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
UGT	UGT	kA	UGT
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
PSOE	PSOE	kA	PSOE
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Federacion	Federacion	k1gInSc1	Federacion
de	de	k?	de
Juventudes	Juventudes	k1gInSc1	Juventudes
Socialistas	Socialistas	k1gInSc1	Socialistas
(	(	kIx(	(
<g/>
Federace	federace	k1gFnSc1	federace
mladých	mladý	k2eAgMnPc2d1	mladý
socialistů	socialist	k1gMnPc2	socialist
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PSUC	PSUC	kA	PSUC
(	(	kIx(	(
<g/>
Partit	partita	k1gFnPc2	partita
Socialista	socialist	k1gMnSc2	socialist
Unificat	Unificat	k1gMnSc2	Unificat
de	de	k?	de
Catalunya	Cataluny	k1gInSc2	Cataluny
–	–	k?	–
Katalánská	katalánský	k2eAgFnSc1d1	katalánská
sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Aliance	aliance	k1gFnSc1	aliance
různých	různý	k2eAgFnPc2d1	různá
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JSU	JSU	k?	JSU
(	(	kIx(	(
<g/>
Juventudes	Juventudes	k1gMnSc1	Juventudes
Socialistas	Socialistas	k1gMnSc1	Socialistas
Unificadas	Unificadas	k1gMnSc1	Unificadas
–	–	k?	–
Sjednocení	sjednocení	k1gNnSc4	sjednocení
socialistické	socialistický	k2eAgFnSc2d1	socialistická
mládeže	mládež	k1gFnSc2	mládež
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Militantní	militantní	k2eAgFnSc1d1	militantní
mládežnická	mládežnický	k2eAgFnSc1d1	mládežnická
skupina	skupina	k1gFnSc1	skupina
sestavená	sestavený	k2eAgFnSc1d1	sestavená
spojením	spojení	k1gNnSc7	spojení
socialistických	socialistický	k2eAgInPc2d1	socialistický
a	a	k8xC	a
komunistických	komunistický	k2eAgInPc2d1	komunistický
spolků	spolek	k1gInPc2	spolek
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
vůdcem	vůdce	k1gMnSc7	vůdce
byl	být	k5eAaImAgMnS	být
Santiago	Santiago	k1gNnSc4	Santiago
Carrillo	Carrilla	k1gFnSc5	Carrilla
</s>
</p>
<p>
<s>
PCE	PCE	kA	PCE
(	(	kIx(	(
<g/>
Partido	Partida	k1gFnSc5	Partida
Comunista	Comunista	k1gMnSc1	Comunista
de	de	k?	de
Españ	Españ	k1gFnSc2	Españ
–	–	k?	–
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vůdcem	vůdce	k1gMnSc7	vůdce
byl	být	k5eAaImAgMnS	být
José	José	k1gNnSc4	José
Díaz	Díaza	k1gFnPc2	Díaza
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgFnSc1d1	malá
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
význam	význam	k1gInSc4	význam
sovětské	sovětský	k2eAgFnSc2d1	sovětská
podpory	podpora	k1gFnSc2	podpora
nebyla	být	k5eNaImAgFnS	být
nevýznamná	významný	k2eNgFnSc1d1	nevýznamná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POUM	POUM	kA	POUM
(	(	kIx(	(
<g/>
Partido	Partida	k1gFnSc5	Partida
Obrero	Obrero	k1gNnSc1	Obrero
de	de	k?	de
Unificación	Unificación	k1gMnSc1	Unificación
Marxista	marxista	k1gMnSc1	marxista
–	–	k?	–
Dělnické	dělnický	k2eAgNnSc1d1	dělnické
strana	strana	k1gFnSc1	strana
marxistického	marxistický	k2eAgNnSc2d1	marxistické
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
bývalí	bývalý	k2eAgMnPc1d1	bývalý
trockisté	trockista	k1gMnPc1	trockista
a	a	k8xC	a
leví	levý	k2eAgMnPc1d1	levý
komunisté	komunista	k1gMnPc1	komunista
(	(	kIx(	(
<g/>
antistalinisté	antistalinista	k1gMnPc1	antistalinista
<g/>
)	)	kIx)	)
z	z	k7c2	z
BOC	BOC	kA	BOC
(	(	kIx(	(
<g/>
Bloc	Bloc	k1gFnSc1	Bloc
Obrer	Obrra	k1gFnPc2	Obrra
i	i	k8xC	i
Camperol	Camperola	k1gFnPc2	Camperola
<g/>
,	,	kIx,	,
Bloque	Bloqu	k1gFnPc1	Bloqu
Obrero	Obrero	k1gNnSc1	Obrero
y	y	k?	y
Campesino	Campesin	k2eAgNnSc1d1	Campesino
<g/>
,	,	kIx,	,
Blok	blok	k1gInSc1	blok
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
rolníků	rolník	k1gMnPc2	rolník
<g/>
)	)	kIx)	)
r.	r.	kA	r.
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
Andrés	Andrés	k1gInSc1	Andrés
Nin	Nina	k1gFnPc2	Nina
<g/>
,	,	kIx,	,
Joaquín	Joaquín	k1gMnSc1	Joaquín
Maurín	Maurín	k1gMnSc1	Maurín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JCI	JCI	kA	JCI
(	(	kIx(	(
<g/>
Juventud	Juventud	k1gMnSc1	Juventud
Comunista	Comunista	k1gMnSc1	Comunista
Ibérica	Ibérica	k1gMnSc1	Ibérica
–	–	k?	–
Iberijští	Iberijský	k2eAgMnPc1d1	Iberijský
mladí	mladý	k2eAgMnPc1d1	mladý
komunisté	komunista	k1gMnPc1	komunista
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mládežnické	mládežnický	k2eAgNnSc4d1	mládežnické
hnutí	hnutí	k1gNnSc4	hnutí
POUM	POUM	kA	POUM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PS	PS	kA	PS
(	(	kIx(	(
<g/>
Partido	Partida	k1gFnSc5	Partida
Sindicalista	Sindicalista	k1gMnSc1	Sindicalista
–	–	k?	–
Syndikalistická	syndikalistický	k2eAgFnSc1d1	syndikalistická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zastánci	zastánce	k1gMnPc1	zastánce
Lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
===	===	k?	===
</s>
</p>
<p>
<s>
Unión	Unión	k1gInSc1	Unión
Militar	Militar	k1gInSc1	Militar
Republicana	Republicana	k1gFnSc1	Republicana
Antifascista	Antifascista	k1gMnSc1	Antifascista
(	(	kIx(	(
<g/>
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
antifašistická	antifašistický	k2eAgFnSc1d1	Antifašistická
vojenská	vojenský	k2eAgFnSc1d1	vojenská
unie	unie	k1gFnSc1	unie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
důstojníky	důstojník	k1gMnPc7	důstojník
jako	jako	k8xC	jako
protiklad	protiklad	k1gInSc1	protiklad
Unión	Unión	k1gMnSc1	Unión
Militar	Militar	k1gMnSc1	Militar
Españ	Españ	k1gMnSc1	Españ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CNT	CNT	kA	CNT
(	(	kIx(	(
<g/>
Confederación	Confederación	k1gMnSc1	Confederación
Nacional	Nacional	k1gMnSc1	Nacional
del	del	k?	del
Trabajo	Trabajo	k6eAd1	Trabajo
–	–	k?	–
Národní	národní	k2eAgFnSc2d1	národní
konfederace	konfederace	k1gFnSc2	konfederace
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Konfederace	konfederace	k1gFnSc1	konfederace
anarchosyndikalistických	anarchosyndikalistický	k2eAgInPc2d1	anarchosyndikalistický
odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FAI	FAI	kA	FAI
(	(	kIx(	(
<g/>
Federación	Federación	k1gMnSc1	Federación
Anarquista	Anarquist	k1gMnSc2	Anarquist
Ibérica	Ibéricum	k1gNnSc2	Ibéricum
–	–	k?	–
Iberská	iberský	k2eAgFnSc1d1	iberská
anarchistická	anarchistický	k2eAgFnSc1d1	anarchistická
federace	federace	k1gFnSc1	federace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Federace	federace	k1gFnSc1	federace
anarchistických	anarchistický	k2eAgFnPc2d1	anarchistická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnPc1d1	aktivní
v	v	k7c6	v
republikánských	republikánský	k2eAgFnPc6d1	republikánská
milicích	milice	k1gFnPc6	milice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
AIT	AIT	kA	AIT
(	(	kIx(	(
<g/>
Asociación	Asociación	k1gMnSc1	Asociación
Internacional	Internacional	k1gMnSc1	Internacional
de	de	k?	de
los	los	k1gInSc1	los
Trabajadores	Trabajadoresa	k1gFnPc2	Trabajadoresa
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
asociace	asociace	k1gFnSc2	asociace
pracujících	pracující	k1gMnPc2	pracující
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mujeres	Mujeres	k1gMnSc1	Mujeres
Libres	Libres	k1gMnSc1	Libres
(	(	kIx(	(
<g/>
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
ženy	žena	k1gFnSc2	žena
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Anarchofeministická	Anarchofeministický	k2eAgFnSc1d1	Anarchofeministická
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIJL	FIJL	kA	FIJL
(	(	kIx(	(
<g/>
Federación	Federación	k1gMnSc1	Federación
Ibérica	Ibérica	k1gMnSc1	Ibérica
de	de	k?	de
Juventudes	Juventudes	k1gMnSc1	Juventudes
Libertarias	Libertarias	k1gMnSc1	Libertarias
–	–	k?	–
Iberská	iberský	k2eAgFnSc1d1	iberská
federace	federace	k1gFnSc1	federace
anarchistické	anarchistický	k2eAgFnSc2d1	anarchistická
mládeže	mládež	k1gFnSc2	mládež
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Baskičtí	baskický	k2eAgMnPc1d1	baskický
separatisté	separatista	k1gMnPc1	separatista
====	====	k?	====
</s>
</p>
<p>
<s>
PNV	PNV	kA	PNV
(	(	kIx(	(
<g/>
Partido	Partida	k1gFnSc5	Partida
Nacionalista	nacionalista	k1gMnSc1	nacionalista
Vasco	Vasco	k6eAd1	Vasco
–	–	k?	–
Baskická	baskický	k2eAgFnSc1d1	baskická
nacionalistická	nacionalistický	k2eAgFnSc1d1	nacionalistická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Katolická	katolický	k2eAgFnSc1d1	katolická
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedl	vést	k5eAaImAgInS	vést
José	Josá	k1gFnSc3	Josá
Antonio	Antonio	k1gMnSc5	Antonio
Aguirre	Aguirr	k1gMnSc5	Aguirr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
propagoval	propagovat	k5eAaImAgMnS	propagovat
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
nebo	nebo	k8xC	nebo
nezávislost	nezávislost	k1gFnSc4	nezávislost
pro	pro	k7c4	pro
Basky	Bask	k1gMnPc4	Bask
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
vládu	vláda	k1gFnSc4	vláda
Lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
jak	jak	k8xC	jak
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
tak	tak	k8xC	tak
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ANV	ANV	kA	ANV
(	(	kIx(	(
<g/>
Acción	Acción	k1gMnSc1	Acción
Nacionalista	nacionalista	k1gMnSc1	nacionalista
Vasca	Vasc	k1gInSc2	Vasc
–	–	k?	–
Baskická	baskický	k2eAgFnSc1d1	baskická
nacionalistická	nacionalistický	k2eAgFnSc1d1	nacionalistická
akce	akce	k1gFnSc1	akce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
propagovala	propagovat	k5eAaImAgFnS	propagovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
pro	pro	k7c4	pro
Baskicko	Baskicko	k1gNnSc4	Baskicko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STV	STV	kA	STV
(	(	kIx(	(
<g/>
Solidaridad	Solidaridad	k1gInSc1	Solidaridad
de	de	k?	de
Trabajadores	Trabajadores	k1gInSc1	Trabajadores
Vascos	Vascos	k1gInSc1	Vascos
–	–	k?	–
Solidarita	solidarita	k1gFnSc1	solidarita
baskických	baskický	k2eAgMnPc2d1	baskický
pracujících	pracující	k1gMnPc2	pracující
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SRI	SRI	kA	SRI
(	(	kIx(	(
<g/>
Socorro	Socorro	k1gNnSc4	Socorro
Rojo	Rojo	k6eAd1	Rojo
Internacional	Internacional	k1gFnSc7	Internacional
–	–	k?	–
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
rudá	rudý	k2eAgFnSc1d1	rudá
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Organizace	organizace	k1gFnSc1	organizace
spojila	spojit	k5eAaPmAgFnS	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
Kominternou	Kominterna	k1gFnSc7	Kominterna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
značnou	značný	k2eAgFnSc4d1	značná
pomoc	pomoc	k1gFnSc4	pomoc
republikánům	republikán	k1gMnPc3	republikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nacionalisté	nacionalista	k1gMnPc5	nacionalista
===	===	k?	===
</s>
</p>
<p>
<s>
Unión	Unión	k1gMnSc1	Unión
Militar	Militar	k1gMnSc1	Militar
Españ	Españ	k1gMnSc1	Españ
(	(	kIx(	(
<g/>
Španělská	španělský	k2eAgFnSc1d1	španělská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
unie	unie	k1gFnSc1	unie
<g/>
)	)	kIx)	)
–	–	k?	–
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
politická	politický	k2eAgFnSc1d1	politická
organizace	organizace	k1gFnSc1	organizace
důstojníků	důstojník	k1gMnPc2	důstojník
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
sílách	síla	k1gFnPc6	síla
<g/>
,	,	kIx,	,
přímí	přímý	k2eAgMnPc1d1	přímý
kritici	kritik	k1gMnPc1	kritik
Druhé	druhý	k4xOgFnPc4	druhý
republiky	republika	k1gFnPc4	republika
<g/>
,	,	kIx,	,
podporovali	podporovat	k5eAaImAgMnP	podporovat
Francisca	Francisca	k1gMnSc1	Francisca
Franca	Franca	k?	Franca
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
r.	r.	kA	r.
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
a	a	k8xC	a
navázala	navázat	k5eAaPmAgFnS	navázat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
fašistickou	fašistický	k2eAgFnSc7d1	fašistická
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volebním	volební	k2eAgNnSc6d1	volební
vítězství	vítězství	k1gNnSc6	vítězství
Lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
začala	začít	k5eAaPmAgFnS	začít
připravovat	připravovat	k5eAaImF	připravovat
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
monarchistickými	monarchistický	k2eAgFnPc7d1	monarchistická
a	a	k8xC	a
fašistickými	fašistický	k2eAgFnPc7d1	fašistická
skupinami	skupina	k1gFnPc7	skupina
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alfonsine	Alfonsinout	k5eAaPmIp3nS	Alfonsinout
Monarchista	monarchista	k1gMnSc1	monarchista
(	(	kIx(	(
<g/>
Alfonsovi	Alfonsův	k2eAgMnPc1d1	Alfonsův
monarchisté	monarchista	k1gMnPc1	monarchista
<g/>
)	)	kIx)	)
–	–	k?	–
podporovali	podporovat	k5eAaImAgMnP	podporovat
návrat	návrat	k1gInSc4	návrat
Alfonsa	Alfons	k1gMnSc2	Alfons
XIII	XIII	kA	XIII
<g/>
..	..	k?	..
Členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
tvořili	tvořit	k5eAaImAgMnP	tvořit
armádní	armádní	k2eAgMnPc1d1	armádní
důstojníci	důstojník	k1gMnPc1	důstojník
<g/>
,	,	kIx,	,
aristokraté	aristokrat	k1gMnPc1	aristokrat
a	a	k8xC	a
vlastníci	vlastník	k1gMnPc1	vlastník
<g/>
,	,	kIx,	,
nezískali	získat	k5eNaPmAgMnP	získat
podporu	podpora	k1gFnSc4	podpora
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Renovación	Renovación	k1gMnSc1	Renovación
Españ	Españ	k1gMnSc1	Españ
(	(	kIx(	(
<g/>
Španělský	španělský	k2eAgInSc1d1	španělský
návrat	návrat	k1gInSc1	návrat
<g/>
)	)	kIx)	)
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
podporující	podporující	k2eAgFnSc1d1	podporující
návrat	návrat	k1gInSc4	návrat
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Acción	Acción	k1gMnSc1	Acción
Españ	Españ	k1gMnSc1	Españ
(	(	kIx(	(
<g/>
Španělská	španělský	k2eAgFnSc1d1	španělská
akce	akce	k1gFnSc1	akce
<g/>
)	)	kIx)	)
–	–	k?	–
fašistická	fašistický	k2eAgFnSc1d1	fašistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
ji	on	k3xPp3gFnSc4	on
Jose	Jose	k1gFnSc4	Jose
Calvo	Calvo	k1gNnSc4	Calvo
Sotelo	Sotela	k1gFnSc5	Sotela
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
r.	r.	kA	r.
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bloque	Bloque	k1gFnSc1	Bloque
Nacional	Nacional	k1gFnSc1	Nacional
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc1d1	národní
blok	blok	k1gInSc1	blok
<g/>
)	)	kIx)	)
–	–	k?	–
militantní	militantní	k2eAgNnSc4d1	militantní
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
založil	založit	k5eAaPmAgInS	založit
Jose	Jose	k1gInSc1	Jose
Calvo	Calvo	k1gNnSc1	Calvo
Sotelo	Sotela	k1gFnSc5	Sotela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karlističtí	karlistický	k2eAgMnPc1d1	karlistický
Monarchisté	monarchista	k1gMnPc1	monarchista
–	–	k?	–
podporovali	podporovat	k5eAaImAgMnP	podporovat
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vznesl	vznést	k5eAaPmAgMnS	vznést
Alfonso	Alfonso	k1gMnSc1	Alfonso
Carlos	Carlos	k1gMnSc1	Carlos
I	i	k8xC	i
de	de	k?	de
Borbón	Borbón	k1gInSc1	Borbón
y	y	k?	y
Austria-Este	Austria-Est	k1gInSc5	Austria-Est
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Comunión	Comunión	k1gMnSc1	Comunión
Tradicionalista	tradicionalista	k1gMnSc1	tradicionalista
(	(	kIx(	(
<g/>
Tradicionalistické	tradicionalistický	k2eAgNnSc1d1	tradicionalistické
spojení	spojení	k1gNnSc1	spojení
<g/>
)	)	kIx)	)
–	–	k?	–
Karlistická	karlistický	k2eAgFnSc1d1	karlistická
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
Requetés	Requetés	k1gInSc1	Requetés
(	(	kIx(	(
<g/>
Dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
)	)	kIx)	)
–	–	k?	–
milice	milice	k1gFnSc1	milice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pelayos	Pelayos	k1gMnSc1	Pelayos
–	–	k?	–
militantní	militantní	k2eAgNnSc1d1	militantní
mládežnické	mládežnický	k2eAgNnSc1d1	mládežnické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
podle	podle	k7c2	podle
Pelaya	Pelay	k1gInSc2	Pelay
z	z	k7c2	z
Asturie	Asturie	k1gFnSc2	Asturie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Margaritas	Margaritas	k1gInSc1	Margaritas
–	–	k?	–
ženské	ženská	k1gFnSc2	ženská
hnutí	hnutí	k1gNnSc2	hnutí
pojmenované	pojmenovaný	k2eAgNnSc4d1	pojmenované
podle	podle	k7c2	podle
Margarity	Margarita	k1gFnSc2	Margarita
de	de	k?	de
Borbón-Parma	Borbón-Parma	k1gFnSc1	Borbón-Parma
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
karlistického	karlistický	k2eAgMnSc2d1	karlistický
uchazeče	uchazeč	k1gMnSc2	uchazeč
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
Charlse	Charls	k1gMnSc2	Charls
VII	VII	kA	VII
<g/>
.	.	kIx.	.
španělského	španělský	k2eAgInSc2d1	španělský
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Falange	Falange	k1gFnSc1	Falange
(	(	kIx(	(
<g/>
Falanga	falanga	k1gFnSc1	falanga
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
FE	FE	kA	FE
(	(	kIx(	(
<g/>
Falange	Falange	k1gFnPc2	Falange
Españ	Españ	k1gMnPc2	Españ
de	de	k?	de
las	laso	k1gNnPc2	laso
JONS	JONS	kA	JONS
<g/>
)	)	kIx)	)
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
sloučením	sloučení	k1gNnSc7	sloučení
dvou	dva	k4xCgFnPc2	dva
fašistických	fašistický	k2eAgFnPc2d1	fašistická
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
falangy	falanga	k1gFnSc2	falanga
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
r.	r.	kA	r.
1933	[number]	k4	1933
José	José	k1gNnPc2	José
Antonio	Antonio	k1gMnSc1	Antonio
Primo	primo	k1gNnSc1	primo
de	de	k?	de
Rivera	River	k1gMnSc2	River
a	a	k8xC	a
Falangou	falanga	k1gFnSc7	falanga
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
r.	r.	kA	r.
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ramiro	Ramiro	k1gNnSc1	Ramiro
Ledesma	Ledesm	k1gMnSc2	Ledesm
–	–	k?	–
Junty	junta	k1gFnSc2	junta
de	de	k?	de
Ofensiva	ofensiva	k1gFnSc1	ofensiva
Nacional-Sindicalista	Nacional-Sindicalista	k1gMnSc1	Nacional-Sindicalista
(	(	kIx(	(
<g/>
Národně-syndikalistická	Národněyndikalistický	k2eAgFnSc1d1	Národně-syndikalistický
junta	junta	k1gFnSc1	junta
útoku	útok	k1gInSc2	útok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Falanga	falanga	k1gFnSc1	falanga
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
masivním	masivní	k2eAgNnSc7d1	masivní
hnutím	hnutí	k1gNnSc7	hnutí
v	v	k7c6	v
r.	r.	kA	r.
1936	[number]	k4	1936
porážkou	porážka	k1gFnSc7	porážka
PRR	PRR	kA	PRR
a	a	k8xC	a
CEDA	CEDA	kA	CEDA
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OJE	oje	k1gFnSc1	oje
(	(	kIx(	(
<g/>
Organización	Organización	k1gMnSc1	Organización
Juvenil	Juvenil	k1gMnSc1	Juvenil
Españ	Españ	k1gMnSc1	Españ
<g/>
)	)	kIx)	)
–	–	k?	–
militantní	militantní	k2eAgInSc4d1	militantní
mládežnického	mládežnický	k2eAgNnSc2d1	mládežnické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sección	Sección	k1gInSc1	Sección
Femenina	Femenina	k1gFnSc1	Femenina
(	(	kIx(	(
<g/>
Ženská	ženský	k2eAgFnSc1d1	ženská
sekce	sekce	k1gFnSc1	sekce
<g/>
)	)	kIx)	)
–	–	k?	–
ženské	ženský	k2eAgNnSc4d1	ženské
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Falange	Falange	k6eAd1	Falange
Españ	Españ	k1gMnSc1	Españ
Tradicionalista	tradicionalista	k1gMnSc1	tradicionalista
y	y	k?	y
de	de	k?	de
las	laso	k1gNnPc2	laso
JONS	JONS	kA	JONS
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
FE	FE	kA	FE
a	a	k8xC	a
karlistické	karlistický	k2eAgFnSc2d1	karlistická
strany	strana	k1gFnSc2	strana
–	–	k?	–
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Francem	Franc	k1gMnSc7	Franc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BEEVOR	BEEVOR	kA	BEEVOR
<g/>
,	,	kIx,	,
Antony	anton	k1gInPc7	anton
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Beta-Dobrovský	Beta-Dobrovský	k2eAgMnSc1d1	Beta-Dobrovský
;	;	kIx,	;
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
319	[number]	k4	319
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7306	[number]	k4	7306
<g/>
-	-	kIx~	-
<g/>
141	[number]	k4	141
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CÍLEK	CÍLEK	kA	CÍLEK
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
předehra	předehra	k1gFnSc1	předehra
:	:	kIx,	:
Španělsko	Španělsko	k1gNnSc1	Španělsko
1936-1939	[number]	k4	1936-1939
:	:	kIx,	:
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
intervence	intervence	k1gFnSc1	intervence
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
431	[number]	k4	431
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86328	[number]	k4	86328
<g/>
-	-	kIx~	-
<g/>
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HEMINGWAY	HEMINGWAY	kA	HEMINGWAY
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
-	-	kIx~	-
Komu	kdo	k3yRnSc3	kdo
zvoní	zvonit	k5eAaImIp3nS	zvonit
hrana	hrana	k1gFnSc1	hrana
<g/>
:	:	kIx,	:
Španělsko	Španělsko	k1gNnSc1	Španělsko
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
osudy	osud	k1gInPc4	osud
partyzánů	partyzán	k1gMnPc2	partyzán
s	s	k7c7	s
úkolem	úkol	k1gInSc7	úkol
vyhodit	vyhodit	k5eAaPmF	vyhodit
most	most	k1gInSc4	most
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
</s>
</p>
<p>
<s>
HRBATA	HRBATA	kA	HRBATA
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
a	a	k8xC	a
Burgos	Burgos	k1gInSc1	Burgos
v	v	k7c6	v
době	doba	k1gFnSc6	doba
španělské	španělský	k2eAgFnSc2d1	španělská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
209	[number]	k4	209
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Zápisky	zápiska	k1gFnPc1	zápiska
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
občanské	občanský	k2eAgNnSc1d1	občanské
:	:	kIx,	:
studie	studie	k1gFnSc1	studie
o	o	k7c6	o
vybraných	vybraný	k2eAgInPc6d1	vybraný
aspektech	aspekt	k1gInPc6	aspekt
Španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
431	[number]	k4	431
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ORWELL	ORWELL	kA	ORWELL
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Hold	hold	k1gInSc4	hold
Katalánsku	Katalánsko	k1gNnSc3	Katalánsko
a	a	k8xC	a
Ohlédnutí	ohlédnutí	k1gNnSc4	ohlédnutí
za	za	k7c7	za
Španělskou	španělský	k2eAgFnSc7d1	španělská
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
219	[number]	k4	219
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
311	[number]	k4	311
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Českoslovenští	československý	k2eAgMnPc1d1	československý
interbrigadisté	interbrigadista	k1gMnPc1	interbrigadista
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Historickém	historický	k2eAgInSc6d1	historický
magazínu	magazín	k1gInSc6	magazín
ČT	ČT	kA	ČT
</s>
</p>
