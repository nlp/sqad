<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
si	se	k3xPyFc3	se
Franco	Franco	k1gMnSc1	Franco
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ani	ani	k8xC	ani
dobýt	dobýt	k5eAaPmF	dobýt
Madrid	Madrid	k1gInSc4	Madrid
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
přerušit	přerušit	k5eAaPmF	přerušit
jeho	jeho	k3xOp3gNnSc4	jeho
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Valencií	Valencie	k1gFnSc7	Valencie
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
