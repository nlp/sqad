<s>
Rákosina	rákosina	k1gFnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
rezervaceRákosinaIUCN	rezervaceRákosinaIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2002	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
224	#num#	k4
-	-	kIx~
225	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
16,39	16,39	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Jistebník	Jistebník	k1gMnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
24,6	24,6	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
29,9	29,9	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Rákosina	rákosina	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
2198	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Rákosina	rákosina	k1gFnSc1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
ev.	ev.	kA
č.	č.	kA
2198	#num#	k4
poblíž	poblíž	k7c2
obce	obec	k1gFnSc2
Jistebník	Jistebník	k1gMnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
spravuje	spravovat	k5eAaImIp3nS
AOPK	AOPK	kA
ČR	ČR	kA
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
tvoří	tvořit	k5eAaImIp3nS
terestrická	terestrický	k2eAgFnSc1d1
rákosina	rákosina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
niž	jenž	k3xRgFnSc4
navazují	navazovat	k5eAaImIp3nP
mokřady	mokřad	k1gInPc1
<g/>
,	,	kIx,
louky	louka	k1gFnPc1
a	a	k8xC
lesní	lesní	k2eAgInSc1d1
porost	porost	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mělké	mělký	k2eAgFnPc4d1
tůně	tůně	k1gFnPc4
zarůstající	zarůstající	k2eAgFnPc4d1
plovoucími	plovoucí	k2eAgFnPc7d1
vodními	vodní	k2eAgFnPc7d1
rostlinami	rostlina	k1gFnPc7
obklopují	obklopovat	k5eAaImIp3nP
společenstva	společenstvo	k1gNnPc1
vysokých	vysoký	k2eAgFnPc2d1
bažinatých	bažinatý	k2eAgFnPc2d1
bylin	bylina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoologicky	zoologicky	k6eAd1
významná	významný	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
bezobratlých	bezobratlí	k1gMnPc2
<g/>
,	,	kIx,
obojživelníků	obojživelník	k1gMnPc2
apod.	apod.	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Beskydy	Beskydy	k1gFnPc1
•	•	k?
Poodří	Poodří	k1gNnPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Radhošť	Radhošť	k1gMnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Šipka	šipka	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bartošovický	Bartošovický	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Bařiny	Bařina	k1gMnSc2
•	•	k?
Bažantula	Bažantul	k1gMnSc2
•	•	k?
Huštýn	Huštýn	k1gInSc1
•	•	k?
Koryta	koryto	k1gNnSc2
•	•	k?
Kotvice	kotvice	k1gFnSc1
•	•	k?
Královec	Královec	k1gInSc1
•	•	k?
Noříčí	Noříčí	k2eAgInSc1d1
•	•	k?
Rákosina	rákosina	k1gFnSc1
•	•	k?
Rybníky	rybník	k1gInPc1
v	v	k7c6
Trnávce	Trnávka	k1gFnSc6
•	•	k?
Suchá	suchý	k2eAgFnSc1d1
Dora	Dora	k1gFnSc1
•	•	k?
Svinec	svinec	k1gInSc1
•	•	k?
Trojačka	Trojačka	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Domorazské	Domorazský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Kamenárka	Kamenárka	k1gFnSc1
•	•	k?
Meandry	meandr	k1gInPc1
Staré	Staré	k2eAgInPc1d1
Odry	odr	k1gInPc1
•	•	k?
Na	na	k7c6
Čermence	Čermenka	k1gFnSc6
•	•	k?
Pikritové	Pikritový	k2eAgFnSc6d1
mandlovce	mandlovka	k1gFnSc6
u	u	k7c2
Kojetína	Kojetín	k1gInSc2
•	•	k?
Polštářové	polštářový	k2eAgFnSc2d1
lávy	láva	k1gFnSc2
ve	v	k7c6
Straníku	straník	k1gMnSc6
•	•	k?
Prameny	pramen	k1gInPc1
Zrzávky	Zrzávka	k1gFnSc2
•	•	k?
Pusté	pustý	k2eAgFnSc2d1
nivy	niva	k1gFnSc2
•	•	k?
Sedlnické	Sedlnický	k2eAgFnSc2d1
sněženky	sněženka	k1gFnSc2
•	•	k?
Stříbrné	stříbrný	k2eAgNnSc4d1
jezírko	jezírko	k1gNnSc4
•	•	k?
Travertinová	travertinový	k2eAgFnSc1d1
kaskáda	kaskáda	k1gFnSc1
•	•	k?
Váňův	Váňův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Vrásový	vrásový	k2eAgInSc1d1
soubor	soubor	k1gInSc1
v	v	k7c4
Klokočůvku	Klokočůvka	k1gFnSc4
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
