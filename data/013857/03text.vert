<s>
Soustava	soustava	k1gFnSc1
SI	se	k3xPyFc3
</s>
<s>
Logo	logo	k1gNnSc1
SI	se	k3xPyFc3
</s>
<s>
Soustava	soustava	k1gFnSc1
SI	SI	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
francouzského	francouzský	k2eAgMnSc2d1
„	„	kIx"
<g/>
Le	Le	k1gMnSc5
Systè	Systè	k1gMnSc5
International	International	k1gFnPc6
d	d	k7
<g/>
'	'	kIx"
<g/>
Unités	Unités	k1gInSc1
<g/>
“	“	kIx"
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	kIx~
česky	česky	k6eAd1
„	„	kIx"
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1
systém	systém	k1gInSc1
jednotek	jednotka	k1gFnPc2
<g/>
“	“	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodně	mezinárodně	k6eAd1
domluvená	domluvený	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
jednotek	jednotka	k1gFnPc2
fyzikálních	fyzikální	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
sedmi	sedm	k4xCc2
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
nich	on	k3xPp3gInPc6
aritmeticky	aritmeticky	k6eAd1
závisejících	závisející	k2eAgFnPc2d1
odvozených	odvozený	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
dekadickými	dekadický	k2eAgFnPc7d1
předponami	předpona	k1gFnPc7
tvořených	tvořený	k2eAgInPc2d1
násobků	násobek	k1gInPc2
a	a	k8xC
dílů	díl	k1gInPc2
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definice	definice	k1gFnSc1
těchto	tento	k3xDgFnPc2
jednotek	jednotka	k1gFnPc2
a	a	k8xC
uchovávání	uchovávání	k1gNnSc4
případných	případný	k2eAgInPc2d1
etalonů	etalon	k1gInPc2
garantuje	garantovat	k5eAaBmIp3nS
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
v	v	k7c4
Sè	Sè	k1gInSc4
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
systém	systém	k1gInSc1
mezinárodních	mezinárodní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
byl	být	k5eAaImAgInS
používaný	používaný	k2eAgInSc1d1
zhruba	zhruba	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1874	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byla	být	k5eAaImAgFnS
Soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
vyhlášena	vyhlásit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
mezinárodně	mezinárodně	k6eAd1
platná	platný	k2eAgFnSc1d1
<g/>
,	,	kIx,
načež	načež	k6eAd1
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
postupně	postupně	k6eAd1
implementována	implementovat	k5eAaImNgFnS
do	do	k7c2
právních	právní	k2eAgInPc2d1
řádů	řád	k1gInPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
probíhala	probíhat	k5eAaImAgFnS
příprava	příprava	k1gFnSc1
nových	nový	k2eAgFnPc2d1
definic	definice	k1gFnPc2
stávajících	stávající	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
vazby	vazba	k1gFnSc2
k	k	k7c3
pevně	pevně	k6eAd1
stanoveným	stanovený	k2eAgFnPc3d1
hodnotám	hodnota	k1gFnPc3
přírodních	přírodní	k2eAgFnPc2d1
konstant	konstanta	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
definitivně	definitivně	k6eAd1
schválena	schválit	k5eAaPmNgFnS
na	na	k7c6
konferenci	konference	k1gFnSc6
konané	konaný	k2eAgFnSc6d1
v	v	k7c6
listopadu	listopad	k1gInSc6
2018	#num#	k4
ve	v	k7c6
Versailles	Versailles	k1gFnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
následně	následně	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
tak	tak	k9
opuštěn	opuštěn	k2eAgInSc4d1
i	i	k8xC
poslední	poslední	k2eAgInSc4d1
fyzický	fyzický	k2eAgInSc4d1
etalon	etalon	k1gInSc4
<g/>
,	,	kIx,
kilogram	kilogram	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
všechny	všechen	k3xTgFnPc1
jednotky	jednotka	k1gFnPc1
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
reprodukovatelné	reprodukovatelný	k2eAgInPc1d1
s	s	k7c7
přesností	přesnost	k1gFnSc7
alespoň	alespoň	k9
na	na	k7c4
šest	šest	k4xCc4
platných	platný	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
díky	díky	k7c3
definovaným	definovaný	k2eAgInPc3d1
fyzikálním	fyzikální	k2eAgInPc3d1
vztahům	vztah	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sedm	sedm	k4xCc1
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
SI	si	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
vzájemná	vzájemný	k2eAgFnSc1d1
závislost	závislost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shora	shora	k6eAd1
proti	proti	k7c3
směru	směr	k1gInSc3
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
<g/>
:	:	kIx,
sekunda	sekunda	k1gFnSc1
(	(	kIx(
<g/>
čas	čas	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
metr	metr	k1gInSc1
(	(	kIx(
<g/>
délka	délka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ampér	ampér	k1gInSc1
(	(	kIx(
<g/>
el.	el.	k?
proud	proud	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kelvin	kelvin	k1gInSc1
(	(	kIx(
<g/>
teplota	teplota	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kandela	kandela	k1gFnSc1
(	(	kIx(
<g/>
svítivost	svítivost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mol	mol	k1gInSc1
(	(	kIx(
<g/>
látkové	látkový	k2eAgNnSc1d1
množství	množství	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
kilogram	kilogram	k1gInSc4
(	(	kIx(
<g/>
hmotnost	hmotnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
stanovuje	stanovovat	k5eAaImIp3nS
povinnost	povinnost	k1gFnSc1
používat	používat	k5eAaImF
soustavu	soustava	k1gFnSc4
jednotek	jednotka	k1gFnPc2
SI	si	k1gNnPc2
zákon	zákon	k1gInSc1
č.	č.	k?
505	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
(	(	kIx(
<g/>
Zákon	zákon	k1gInSc1
o	o	k7c4
metrologii	metrologie	k1gFnSc4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
prováděcí	prováděcí	k2eAgFnSc1d1
vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
264	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Vyhláška	vyhláška	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
o	o	k7c6
základních	základní	k2eAgFnPc6d1
měřicích	měřicí	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
a	a	k8xC
ostatních	ostatní	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
a	a	k8xC
o	o	k7c6
jejich	jejich	k3xOp3gNnSc6
označování	označování	k1gNnSc6
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmito	tento	k3xDgInPc7
předpisy	předpis	k1gInPc7
je	být	k5eAaImIp3nS
také	také	k9
stanoven	stanovit	k5eAaPmNgInS
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
jako	jako	k8xC,k8xS
garant	garant	k1gMnSc1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
etalonů	etalon	k1gInPc2
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnPc1
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
Velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
(	(	kIx(
<g/>
1789	#num#	k4
<g/>
–	–	k?
<g/>
1799	#num#	k4
<g/>
)	)	kIx)
vznikla	vzniknout	k5eAaPmAgFnS
potřeba	potřeba	k1gFnSc1
redefinice	redefinice	k1gFnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
používaných	používaný	k2eAgFnPc2d1
různých	různý	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
proto	proto	k8xC
Ústavodárné	ústavodárný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
pověřilo	pověřit	k5eAaPmAgNnS
vědeckou	vědecký	k2eAgFnSc4d1
komisi	komise	k1gFnSc4
ve	v	k7c6
stanovením	stanovení	k1gNnSc7
soustavy	soustava	k1gFnSc2
jednotek	jednotka	k1gFnPc2
a	a	k8xC
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
germinalu	germinal	k1gInSc2
r.	r.	kA
III	III	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1795	#num#	k4
<g/>
)	)	kIx)
stanovilo	stanovit	k5eAaPmAgNnS
povinnost	povinnost	k1gFnSc4
používání	používání	k1gNnSc1
nového	nový	k2eAgNnSc2d1
„	„	k?
<g/>
absolutního	absolutní	k2eAgInSc2d1
systému	systém	k1gInSc2
metrických	metrický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
“	“	k?
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1801	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	napoleon	k1gInSc1
Bonaparte	bonapart	k1gInSc5
sice	sice	k8xC
svým	svůj	k3xOyFgInSc7
dekretem	dekret	k1gInSc7
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1812	#num#	k4
znovu	znovu	k6eAd1
povolil	povolit	k5eAaPmAgMnS
staré	starý	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
ze	z	k7c2
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1837	#num#	k4
povinnost	povinnost	k1gFnSc4
používat	používat	k5eAaImF
metrickou	metrický	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
ve	v	k7c6
Francii	Francie	k1gFnSc6
opět	opět	k6eAd1
zavedl	zavést	k5eAaPmAgInS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1840	#num#	k4
a	a	k8xC
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
francouzské	francouzský	k2eAgFnPc1d1
vlády	vláda	k1gFnPc1
neustále	neustále	k6eAd1
propagovaly	propagovat	k5eAaImAgFnP
<g/>
,	,	kIx,
nabývala	nabývat	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
soustava	soustava	k1gFnSc1
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
oblibě	obliba	k1gFnSc6
a	a	k8xC
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1875	#num#	k4
podepsali	podepsat	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
18	#num#	k4
zemí	zem	k1gFnPc2
tzv.	tzv.	kA
Metrickou	metrický	k2eAgFnSc4d1
konvenci	konvence	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
mj.	mj.	kA
založila	založit	k5eAaPmAgFnS
Mezinárodní	mezinárodní	k2eAgInSc4d1
úřad	úřad	k1gInSc4
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
<g/>
,	,	kIx,
spravovaný	spravovaný	k2eAgInSc1d1
mezinárodním	mezinárodní	k2eAgInSc7d1
výborem	výbor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6
byla	být	k5eAaImAgFnS
metrická	metrický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
zavedena	zavést	k5eAaPmNgFnS
zákonem	zákon	k1gInSc7
ze	z	k7c2
dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1871	#num#	k4
s	s	k7c7
platností	platnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1876	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
základ	základ	k1gInSc1
jednotek	jednotka	k1gFnPc2
délky	délka	k1gFnSc2
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
navržen	navržen	k2eAgInSc1d1
metr	metr	k1gInSc1
coby	coby	k?
</s>
<s>
1	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
000	#num#	k4
</s>
<s>
000	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
tfrac	tfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
10	#num#	k4
<g/>
\	\	kIx~
<g/>
,000	,000	k4
<g/>
\	\	kIx~
<g/>
,000	,000	k4
<g/>
}}}	}}}	k?
</s>
<s>
zemského	zemský	k2eAgInSc2d1
kvadrantu	kvadrant	k1gInSc2
a	a	k8xC
na	na	k7c6
základě	základ	k1gInSc6
dekretu	dekret	k1gInSc2
ze	z	k7c2
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1799	#num#	k4
byl	být	k5eAaImAgInS
zhotoven	zhotovit	k5eAaPmNgInS
etalon	etalon	k1gInSc1
metru	metr	k1gInSc2
(	(	kIx(
<g/>
nejprve	nejprve	k6eAd1
mosazný	mosazný	k2eAgInSc1d1
<g/>
,	,	kIx,
poté	poté	k6eAd1
platinový	platinový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
nazván	nazvat	k5eAaPmNgInS,k5eAaBmNgInS
archivním	archivní	k2eAgInSc7d1
metrem	metr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gMnSc2
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
mezinárodní	mezinárodní	k2eAgInSc1d1
etalon	etalon	k1gInSc1
metru	metr	k1gInSc2
(	(	kIx(
<g/>
ze	z	k7c2
slitiny	slitina	k1gFnSc2
platiny	platina	k1gFnSc2
a	a	k8xC
iridia	iridium	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
kopie	kopie	k1gFnPc1
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgFnP
členským	členský	k2eAgMnPc3d1
států	stát	k1gInPc2
konvence	konvence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
systém	systém	k1gInSc1
mezinárodní	mezinárodní	k2eAgFnSc7d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
zhruba	zhruba	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1874	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
definován	definovat	k5eAaBmNgInS
jako	jako	k8xC,k8xS
Soustava	soustava	k1gFnSc1
CGS	CGS	kA
(	(	kIx(
<g/>
centimetr-gram-sekunda	centimetr-gram-sekund	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
ale	ale	k9
měla	mít	k5eAaImAgFnS
řadu	řada	k1gFnSc4
odvozenin	odvozenina	k1gFnPc2
a	a	k8xC
modifikací	modifikace	k1gFnPc2
pro	pro	k7c4
některé	některý	k3yIgFnPc4
vědní	vědní	k2eAgFnPc4d1
obory	obora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byla	být	k5eAaImAgFnS
poměrně	poměrně	k6eAd1
brzo	brzo	k6eAd1
redefinována	redefinován	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1889	#num#	k4
tak	tak	k9
vznikla	vzniknout	k5eAaPmAgFnS
Soustava	soustava	k1gFnSc1
MKS	MKS	kA
(	(	kIx(
<g/>
metr-kilogram-sekunda	metr-kilogram-sekund	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
postupujícím	postupující	k2eAgInSc7d1
rozvojem	rozvoj	k1gInSc7
vědy	věda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
však	však	k9
vyvstávala	vyvstávat	k5eAaImAgFnS
potřeba	potřeba	k6eAd1
definovat	definovat	k5eAaBmF
a	a	k8xC
mezinárodně	mezinárodně	k6eAd1
normalizovat	normalizovat	k5eAaBmF
další	další	k2eAgFnPc4d1
fyzikální	fyzikální	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
přípravy	příprava	k1gFnSc2
této	tento	k3xDgFnSc2
nové	nový	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
začal	začít	k5eAaPmAgInS
mezinárodní	mezinárodní	k2eAgInSc1d1
standardizační	standardizační	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
se	se	k3xPyFc4
tak	tak	k9
stalo	stát	k5eAaPmAgNnS
zákonem	zákon	k1gInSc7
č.	č.	k?
35	#num#	k4
<g/>
/	/	kIx~
<g/>
1962	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1962	#num#	k4
(	(	kIx(
<g/>
Zákon	zákon	k1gInSc1
o	o	k7c6
měrové	měrový	k2eAgFnSc6d1
službě	služba	k1gFnSc6
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
veličiny	veličina	k1gFnPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
</s>
<s>
Níže	níže	k1gFnSc1
následuje	následovat	k5eAaImIp3nS
popis	popis	k1gInSc4
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
pro	pro	k7c4
sedm	sedm	k4xCc4
základních	základní	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
stručný	stručný	k2eAgInSc4d1
historický	historický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
</s>
<s>
Značka	značka	k1gFnSc1
veličiny	veličina	k1gFnSc2
(	(	kIx(
<g/>
doporučená	doporučený	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
<g/>
s.	s.	k?
<g/>
18	#num#	k4
<g/>
,	,	kIx,
130	#num#	k4
</s>
<s>
Jednotka	jednotka	k1gFnSc1
</s>
<s>
Značka	značka	k1gFnSc1
(	(	kIx(
<g/>
závazná	závazný	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
t	t	k?
</s>
<s>
sekunda	sekunda	k1gFnSc1
</s>
<s>
s	s	k7c7
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
l	l	kA
(	(	kIx(
<g/>
malé	malý	k2eAgNnSc1d1
L	L	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
x	x	k?
<g/>
,	,	kIx,
r	r	kA
atd.	atd.	kA
</s>
<s>
metr	metr	k1gInSc1
</s>
<s>
m	m	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
m	m	kA
</s>
<s>
kilogram	kilogram	k1gInSc1
</s>
<s>
kg	kg	kA
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1
proud	proud	k1gInSc1
</s>
<s>
I	i	k9
(	(	kIx(
<g/>
velké	velký	k2eAgFnPc4d1
i	i	k8xC
<g/>
)	)	kIx)
<g/>
,	,	kIx,
i	i	k8xC
</s>
<s>
ampér	ampér	k1gInSc1
</s>
<s>
A	a	k9
</s>
<s>
Termodynamická	termodynamický	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
</s>
<s>
T	T	kA
</s>
<s>
kelvin	kelvin	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Látkové	látkový	k2eAgNnSc1d1
množství	množství	k1gNnSc1
</s>
<s>
n	n	k0
</s>
<s>
mol	mol	k1gMnSc1
</s>
<s>
mol	mol	k1gMnSc1
</s>
<s>
Svítivost	svítivost	k1gFnSc1
</s>
<s>
Iv	Iva	k1gFnPc2
(	(	kIx(
<g/>
velké	velký	k2eAgNnSc1d1
i	i	k9
s	s	k7c7
indexem	index	k1gInSc7
malé	malé	k1gNnSc1
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
kandela	kandela	k1gFnSc1
</s>
<s>
cd	cd	kA
</s>
<s>
Sekunda	sekunda	k1gFnSc1
</s>
<s>
Sekunda	sekunda	k1gFnSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
„	„	k?
<g/>
s	s	k7c7
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
času	čas	k1gInSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
definována	definovat	k5eAaBmNgFnS
stanovením	stanovení	k1gNnSc7
pevné	pevný	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
frekvence	frekvence	k1gFnSc2
Δ	Δ	k1gFnPc2
<g/>
,	,	kIx,
přechodu	přechod	k1gInSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
hladinami	hladina	k1gFnPc7
velmi	velmi	k6eAd1
jemné	jemný	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
základního	základní	k2eAgInSc2d1
stavu	stav	k1gInSc2
atomu	atom	k1gInSc2
cesia	cesium	k1gNnSc2
133	#num#	k4
nacházejícího	nacházející	k2eAgInSc2d1
se	se	k3xPyFc4
v	v	k7c6
klidovém	klidový	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
9	#num#	k4
192	#num#	k4
631	#num#	k4
770	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
jednotce	jednotka	k1gFnSc6
Hz	Hz	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3
definice	definice	k1gFnSc1
sekundy	sekund	k1gInPc1
ji	on	k3xPp3gFnSc4
odvozovaly	odvozovat	k5eAaImAgInP
z	z	k7c2
délky	délka	k1gFnSc2
středního	střední	k2eAgInSc2d1
slunečního	sluneční	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gInSc1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
za	za	k7c4
stabilnější	stabilní	k2eAgInSc4d2
základ	základ	k1gInSc4
definice	definice	k1gFnSc2
začal	začít	k5eAaPmAgMnS
považovat	považovat	k5eAaImF
místo	místo	k7c2
dne	den	k1gInSc2
rok	rok	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgInS
koncept	koncept	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
převzatý	převzatý	k2eAgInSc1d1
do	do	k7c2
definice	definice	k1gFnSc2
sekundy	sekunda	k1gFnSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
:	:	kIx,
</s>
<s>
Sekunda	sekunda	k1gFnSc1
je	být	k5eAaImIp3nS
zlomek	zlomek	k1gInSc4
1	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
556	#num#	k4
925,974	925,974	k4
<g/>
7	#num#	k4
tropického	tropický	k2eAgInSc2d1
roku	rok	k1gInSc2
pro	pro	k7c4
0	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1900	#num#	k4
ve	v	k7c4
12	#num#	k4
hodin	hodina	k1gFnPc2
efemeridového	efemeridový	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brzy	brzy	k6eAd1
však	však	k9
technologický	technologický	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
umožnil	umožnit	k5eAaPmAgInS
přesnější	přesný	k2eAgNnSc4d2
měření	měření	k1gNnSc4
a	a	k8xC
udržování	udržování	k1gNnSc4
času	čas	k1gInSc2
pomocí	pomocí	k7c2
atomových	atomový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
tak	tak	k9
byla	být	k5eAaImAgFnS
definice	definice	k1gFnSc1
změněna	změnit	k5eAaPmNgFnS
na	na	k7c6
<g/>
:	:	kIx,
</s>
<s>
Sekunda	sekunda	k1gFnSc1
je	být	k5eAaImIp3nS
doba	doba	k1gFnSc1
trvání	trvání	k1gNnPc2
9	#num#	k4
192	#num#	k4
631	#num#	k4
770	#num#	k4
period	perioda	k1gFnPc2
záření	záření	k1gNnSc2
odpovídajícího	odpovídající	k2eAgInSc2d1
přechodu	přechod	k1gInSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
hladinami	hladina	k1gFnPc7
velmi	velmi	k6eAd1
jemné	jemný	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
základního	základní	k2eAgInSc2d1
stavu	stav	k1gInSc2
atomu	atom	k1gInSc2
cesia	cesium	k1gNnSc2
133	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
upřesněno	upřesnit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
atom	atom	k1gInSc1
cesia	cesium	k1gNnSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
klidu	klid	k1gInSc6
(	(	kIx(
<g/>
časem	časem	k6eAd1
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc1d1
čas	čas	k1gInSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
obecné	obecný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
relativity	relativita	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
teplota	teplota	k1gFnSc1
pozadí	pozadí	k1gNnSc2
blízká	blízký	k2eAgFnSc1d1
0	#num#	k4
K.	K.	kA
Ze	z	k7c2
třetího	třetí	k4xOgInSc2
zákona	zákon	k1gInSc2
termodynamiky	termodynamika	k1gFnSc2
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
teplota	teplota	k1gFnSc1
absolutní	absolutní	k2eAgFnSc2d1
nuly	nula	k1gFnSc2
je	být	k5eAaImIp3nS
nedosažitelná	dosažitelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
však	však	k9
libovolně	libovolně	k6eAd1
přiblížit	přiblížit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínku	podmínka	k1gFnSc4
nulové	nulový	k2eAgFnSc2d1
termodynamické	termodynamický	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
chápat	chápat	k5eAaImF
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
cesiové	cesiový	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
musí	muset	k5eAaImIp3nP
provádět	provádět	k5eAaImF
korekce	korekce	k1gFnPc4
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
teplotu	teplota	k1gFnSc4
pozadí	pozadí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Stávající	stávající	k2eAgNnSc1d1
znění	znění	k1gNnSc1
definice	definice	k1gFnSc2
sekundy	sekunda	k1gFnSc2
získala	získat	k5eAaPmAgFnS
při	při	k7c6
redefinici	redefinice	k1gFnSc6
jednotek	jednotka	k1gFnPc2
SI	si	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
;	;	kIx,
fakticky	fakticky	k6eAd1
zůstala	zůstat	k5eAaPmAgFnS
nezměněna	změnit	k5eNaPmNgFnS
<g/>
,	,	kIx,
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
jen	jen	k9
formálně	formálně	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měla	mít	k5eAaImAgFnS
stejný	stejný	k2eAgInSc4d1
formát	formát	k1gInSc4
jako	jako	k8xS,k8xC
ostatní	ostatní	k2eAgFnPc4d1
nové	nový	k2eAgFnPc4d1
definice	definice	k1gFnPc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vynikne	vyniknout	k5eAaPmIp3nS
idea	idea	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
každá	každý	k3xTgFnSc1
jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
svázána	svázat	k5eAaPmNgFnS
s	s	k7c7
určitou	určitý	k2eAgFnSc7d1
neměnnou	neměnný	k2eAgFnSc7d1,k2eNgFnSc7d1
vlastností	vlastnost	k1gFnSc7
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
pokroku	pokrok	k1gInSc3
v	v	k7c6
metrologii	metrologie	k1gFnSc6
času	čas	k1gInSc2
a	a	k8xC
frekvence	frekvence	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
uvažuje	uvažovat	k5eAaImIp3nS
o	o	k7c6
budoucí	budoucí	k2eAgFnSc6d1
faktické	faktický	k2eAgFnSc6d1
redefinici	redefinice	k1gFnSc6
i	i	k9
u	u	k7c2
sekundy	sekunda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
vstoupit	vstoupit	k5eAaPmF
v	v	k7c4
platnost	platnost	k1gFnSc4
ještě	ještě	k6eAd1
před	před	k7c7
rokem	rok	k1gInSc7
2030	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
předběžných	předběžný	k2eAgInPc2d1
předpokladů	předpoklad	k1gInPc2
nejspíše	nejspíše	k9
v	v	k7c6
roce	rok	k1gInSc6
2026	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Metr	metr	k1gMnSc1
</s>
<s>
Metr	metr	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
„	„	k?
<g/>
m	m	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
délky	délka	k1gFnSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
stanovením	stanovení	k1gNnSc7
pevné	pevný	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
rychlosti	rychlost	k1gFnSc2
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
c	c	k0
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
299	#num#	k4
792	#num#	k4
458	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
jednotce	jednotka	k1gFnSc6
m	m	kA
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
sekunda	sekunda	k1gFnSc1
je	být	k5eAaImIp3nS
definována	definovat	k5eAaBmNgFnS
prostřednictvím	prostřednictvím	k7c2
Δ	Δ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vzhled	vzhled	k1gInSc1
mezinárodního	mezinárodní	k2eAgInSc2d1
prototypu	prototyp	k1gInSc2
metru	metr	k1gInSc2
</s>
<s>
Jako	jako	k8xS,k8xC
standardní	standardní	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
původně	původně	k6eAd1
koncipovala	koncipovat	k5eAaBmAgFnS
délka	délka	k1gFnSc1
kyvadla	kyvadlo	k1gNnPc4
o	o	k7c6
půlperiodě	půlperiod	k1gInSc6
jedné	jeden	k4xCgFnSc2
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
však	však	k9
brzy	brzy	k6eAd1
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
doba	doba	k1gFnSc1
kyvu	kyv	k1gInSc2
poměrně	poměrně	k6eAd1
značně	značně	k6eAd1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
měření	měření	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
jako	jako	k9
definice	definice	k1gFnSc1
metru	metr	k1gInSc2
zvolena	zvolit	k5eAaPmNgFnS
délka	délka	k1gFnSc1
definovaná	definovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
desetimiliontina	desetimiliontina	k1gFnSc1
délky	délka	k1gFnSc2
kvadrantu	kvadrant	k1gInSc2
zemského	zemský	k2eAgInSc2d1
poledníku	poledník	k1gInSc2
procházejícího	procházející	k2eAgInSc2d1
Paříží	Paříž	k1gFnSc7
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
odpovídá	odpovídat	k5eAaImIp3nS
obvodu	obvod	k1gInSc3
Země	zem	k1gFnSc2
přesně	přesně	k6eAd1
40	#num#	k4
000	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
geodetických	geodetický	k2eAgNnPc2d1
měření	měření	k1gNnSc2
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
vyroben	vyrobit	k5eAaPmNgInS
prototyp	prototyp	k1gInSc1
metru	metr	k1gInSc2
<g/>
,	,	kIx,
platino-iridiová	platino-iridiový	k2eAgFnSc1d1
tyč	tyč	k1gFnSc1
o	o	k7c6
průřezu	průřez	k1gInSc6
písmene	písmeno	k1gNnSc2
X	X	kA
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
rysky	ryska	k1gFnPc1
definovaly	definovat	k5eAaBmAgFnP
délku	délka	k1gFnSc4
jednoho	jeden	k4xCgInSc2
metru	metr	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
měřil	měřit	k5eAaImAgInS
za	za	k7c2
teploty	teplota	k1gFnSc2
tání	tání	k1gNnSc1
ledu	led	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Definice	definice	k1gFnSc2
prototypem	prototyp	k1gInSc7
však	však	k9
postupně	postupně	k6eAd1
přestala	přestat	k5eAaPmAgFnS
vyhovovat	vyhovovat	k5eAaImF
potřebám	potřeba	k1gFnPc3
a	a	k8xC
vzrůstající	vzrůstající	k2eAgFnSc2d1
přesnosti	přesnost	k1gFnSc2
metrologie	metrologie	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
definice	definice	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
přírodním	přírodní	k2eAgInSc6d1
jevu	jev	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
Metr	metr	k1gInSc1
je	být	k5eAaImIp3nS
vzdálenost	vzdálenost	k1gFnSc4
rovná	rovnat	k5eAaImIp3nS
1	#num#	k4
650	#num#	k4
736,73	736,73	k4
vlnovým	vlnový	k2eAgFnPc3d1
délkám	délka	k1gFnPc3
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
záření	záření	k1gNnSc4
odpovídajícího	odpovídající	k2eAgInSc2d1
přechodu	přechod	k1gInSc2
mezi	mezi	k7c7
energetickými	energetický	k2eAgFnPc7d1
hladinami	hladina	k1gFnPc7
2	#num#	k4
<g/>
p	p	k?
<g/>
10	#num#	k4
a	a	k8xC
5	#num#	k4
<g/>
d	d	k?
<g/>
5	#num#	k4
atomu	atom	k1gInSc2
kryptonu	krypton	k1gInSc2
86	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
pak	pak	k9
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
definice	definice	k1gFnSc1
opět	opět	k6eAd1
nahrazena	nahradit	k5eAaPmNgFnS
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
za	za	k7c4
definici	definice	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
rychlosti	rychlost	k1gFnSc6
světla	světlo	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Metr	metr	k1gInSc1
je	být	k5eAaImIp3nS
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
urazí	urazit	k5eAaPmIp3nS
světlo	světlo	k1gNnSc1
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
za	za	k7c4
dobu	doba	k1gFnSc4
1	#num#	k4
<g/>
/	/	kIx~
<g/>
299	#num#	k4
792	#num#	k4
458	#num#	k4
sekundy	sekunda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
zde	zde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
k	k	k7c3
reformulaci	reformulace	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
beze	beze	k7c2
změny	změna	k1gFnSc2
definice	definice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definicí	definice	k1gFnPc2
metru	metr	k1gInSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
přesně	přesně	k6eAd1
stanovena	stanoven	k2eAgFnSc1d1
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
mikrovlnného	mikrovlnný	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
v	v	k7c6
definici	definice	k1gFnSc6
sekundy	sekunda	k1gFnSc2
<g/>
,	,	kIx,
vztahem	vztah	k1gInSc7
</s>
<s>
λ	λ	k?
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
f	f	k?
</s>
<s>
=	=	kIx~
</s>
<s>
299792458	#num#	k4
</s>
<s>
9192631770	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
≈	≈	k?
</s>
<s>
3,261	3,261	k4
</s>
<s>
2	#num#	k4
</s>
<s>
…	…	k?
</s>
<s>
c	c	k0
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
lambda	lambda	k1gNnSc1
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
299792458	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9192631770	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
m	m	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
approx	approx	k1gInSc1
3	#num#	k4
<g/>
{	{	kIx(
<g/>
,	,	kIx,
<g/>
}	}	kIx)
<g/>
2612	#num#	k4
<g/>
\	\	kIx~
<g/>
dots	dots	k6eAd1
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
cm	cm	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
</s>
<s>
Porovnání	porovnání	k1gNnSc1
neznámé	známý	k2eNgFnSc2d1
vzdálenosti	vzdálenost	k1gFnSc2
s	s	k7c7
touto	tento	k3xDgFnSc7
vlnovou	vlnový	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
lze	lze	k6eAd1
provádět	provádět	k5eAaImF
interferometricky	interferometricky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Kilogram	kilogram	k1gInSc1
</s>
<s>
Replika	replika	k1gFnSc1
mezinárodního	mezinárodní	k2eAgInSc2d1
prototypu	prototyp	k1gInSc2
kilogramu	kilogram	k1gInSc2
</s>
<s>
Kilogram	kilogram	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
„	„	k?
<g/>
kg	kg	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
hmotnosti	hmotnost	k1gFnSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
stanovením	stanovení	k1gNnSc7
pevné	pevný	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
Planckovy	Planckův	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
h	h	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
6,626	6,626	k4
070	#num#	k4
15	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
34	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
jednotce	jednotka	k1gFnSc6
J	J	kA
s	s	k7c7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
kg	kg	kA
m	m	kA
<g/>
2	#num#	k4
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
metr	metr	k1gInSc1
a	a	k8xC
sekunda	sekunda	k1gFnSc1
jsou	být	k5eAaImIp3nP
definovány	definovat	k5eAaBmNgInP
prostřednictvím	prostřednictvím	k7c2
c	c	k0
a	a	k8xC
Δ	Δ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
hmotnosti	hmotnost	k1gFnSc2
(	(	kIx(
<g/>
původně	původně	k6eAd1
nazývaná	nazývaný	k2eAgNnPc1d1
grave	grave	k1gNnPc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgNnP
koncipována	koncipován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
hmotnost	hmotnost	k1gFnSc1
jednoho	jeden	k4xCgInSc2
litru	litr	k1gInSc2
vody	voda	k1gFnSc2
prosté	prostý	k2eAgFnSc2d1
vzduchu	vzduch	k1gInSc2
za	za	k7c4
teploty	teplota	k1gFnPc4
tuhnutí	tuhnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
definici	definice	k1gFnSc3
této	tento	k3xDgFnSc2
jednotky	jednotka	k1gFnSc2
však	však	k9
první	první	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
stanovila	stanovit	k5eAaPmAgFnS
hmotnost	hmotnost	k1gFnSc1
prototypu	prototyp	k1gInSc2
kilogramu	kilogram	k1gInSc2
<g/>
,	,	kIx,
válečku	válečka	k1gFnSc4
z	z	k7c2
platino-iridiové	platino-iridiový	k2eAgFnSc2d1
slitiny	slitina	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Kilogram	kilogram	k1gInSc1
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
hmotnosti	hmotnost	k1gFnSc2
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
hmotnosti	hmotnost	k1gFnSc3
mezinárodního	mezinárodní	k2eAgInSc2d1
prototypu	prototyp	k1gInSc2
kilogramu	kilogram	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
definice	definice	k1gFnSc1
prototypem	prototyp	k1gInSc7
vydržela	vydržet	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
redefinice	redefinice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
,	,	kIx,
přestože	přestože	k8xS
z	z	k7c2
praktických	praktický	k2eAgInPc2d1
i	i	k8xC
teoretických	teoretický	k2eAgInPc2d1
aspektů	aspekt	k1gInPc2
byla	být	k5eAaImAgFnS
již	již	k6eAd1
dlouho	dlouho	k6eAd1
značně	značně	k6eAd1
problematická	problematický	k2eAgFnSc1d1
<g/>
;	;	kIx,
trvalo	trvat	k5eAaImAgNnS
však	však	k9
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
našla	najít	k5eAaPmAgFnS
dostatečně	dostatečně	k6eAd1
přesná	přesný	k2eAgFnSc1d1
alternativa	alternativa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
definice	definice	k1gFnSc1
tak	tak	k9
je	být	k5eAaImIp3nS
nejpodstatnější	podstatný	k2eAgMnSc1d3
z	z	k7c2
přijatých	přijatý	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
se	s	k7c7
základní	základní	k2eAgFnSc7d1
fyzikální	fyzikální	k2eAgFnSc7d1
konstantou	konstanta	k1gFnSc7
nově	nově	k6eAd1
umožnilo	umožnit	k5eAaPmAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
velikost	velikost	k1gFnSc4
kilogramu	kilogram	k1gInSc2
a	a	k8xC
všech	všecek	k3xTgFnPc2
jednotek	jednotka	k1gFnPc2
od	od	k7c2
něj	on	k3xPp3gInSc2
odvozených	odvozený	k2eAgMnPc2d1
byla	být	k5eAaImAgFnS
spolehlivě	spolehlivě	k6eAd1
časově	časově	k6eAd1
stabilní	stabilní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Planckova	Planckův	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
h	h	k?
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
konstantou	konstanta	k1gFnSc7
kvantové	kvantový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
určuje	určovat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
energií	energie	k1gFnSc7
a	a	k8xC
frekvencí	frekvence	k1gFnSc7
fotonu	foton	k1gInSc2
<g/>
:	:	kIx,
E	E	kA
<g/>
=	=	kIx~
<g/>
hf	hf	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
relativity	relativita	k1gFnSc2
poskytuje	poskytovat	k5eAaImIp3nS
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
energií	energie	k1gFnSc7
a	a	k8xC
hmotností	hmotnost	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
konstantou	konstanta	k1gFnSc7
úměrnosti	úměrnost	k1gFnSc2
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
:	:	kIx,
E	E	kA
<g/>
=	=	kIx~
<g/>
mc²	mc²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
dva	dva	k4xCgInPc1
fyzikální	fyzikální	k2eAgInPc1d1
zákony	zákon	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
odvodit	odvodit	k5eAaPmF
definici	definice	k1gFnSc4
kilogramu	kilogram	k1gInSc2
od	od	k7c2
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
Planckovy	Planckův	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měření	měření	k1gNnPc1
hmotnosti	hmotnost	k1gFnSc2
podle	podle	k7c2
nových	nový	k2eAgFnPc2d1
definic	definice	k1gFnPc2
prakticky	prakticky	k6eAd1
umožňují	umožňovat	k5eAaImIp3nP
wattové	wattový	k2eAgFnPc4d1
váhy	váha	k1gFnPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
nová	nový	k2eAgFnSc1d1
definice	definice	k1gFnSc1
ampéru	ampér	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ampér	ampér	k1gInSc1
</s>
<s>
Ampér	ampér	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
„	„	k?
<g/>
A	a	k8xC
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
stanovením	stanovení	k1gNnSc7
pevné	pevný	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
elementárního	elementární	k2eAgInSc2d1
náboje	náboj	k1gInSc2
e	e	k0
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
1,602	1,602	k4
176	#num#	k4
634	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
19	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
jednotce	jednotka	k1gFnSc6
C	C	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
A	a	k9
s	s	k7c7
<g/>
,	,	kIx,
kde	kde	k6eAd1
sekunda	sekunda	k1gFnSc1
je	být	k5eAaImIp3nS
definována	definovat	k5eAaBmNgFnS
prostřednictvím	prostřednictvím	k7c2
Δ	Δ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ampér	ampér	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
převzat	převzít	k5eAaPmNgInS
z	z	k7c2
jednotky	jednotka	k1gFnSc2
proudu	proud	k1gInSc2
v	v	k7c6
soustavě	soustava	k1gFnSc6
CGS	CGS	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
jednotka	jednotka	k1gFnSc1
definována	definován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
takový	takový	k3xDgInSc1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
tekoucí	tekoucí	k2eAgInPc1d1
vodičem	vodič	k1gInSc7
o	o	k7c6
délce	délka	k1gFnSc6
1	#num#	k4
cm	cm	kA
ve	v	k7c6
tvaru	tvar	k1gInSc6
části	část	k1gFnSc2
kruhového	kruhový	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
o	o	k7c6
poloměru	poloměr	k1gInSc6
1	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ve	v	k7c6
středu	střed	k1gInSc6
kružnice	kružnice	k1gFnSc2
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
o	o	k7c6
intenzitě	intenzita	k1gFnSc6
1	#num#	k4
oersted	oersted	k1gInSc4
<g/>
;	;	kIx,
ampér	ampér	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
jako	jako	k9
desetkrát	desetkrát	k6eAd1
menší	malý	k2eAgFnSc1d2
než	než	k8xS
jednotka	jednotka	k1gFnSc1
v	v	k7c6
systému	systém	k1gInSc6
CGS	CGS	kA
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
nazývaná	nazývaný	k2eAgFnSc1d1
abampér	abampér	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byla	být	k5eAaImAgFnS
definice	definice	k1gFnSc1
nahrazena	nahradit	k5eAaPmNgFnS
novou	nový	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
velikost	velikost	k1gFnSc1
ampéru	ampér	k1gInSc2
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
elektrické	elektrický	k2eAgFnSc2d1
síly	síla	k1gFnSc2
mezi	mezi	k7c7
rovnoběžnými	rovnoběžný	k2eAgInPc7d1
vodiči	vodič	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
Ampér	ampér	k1gInSc1
je	být	k5eAaImIp3nS
stálý	stálý	k2eAgInSc1d1
elektrický	elektrický	k2eAgInSc1d1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
protéká	protékat	k5eAaImIp3nS
dvěma	dva	k4xCgFnPc7
rovnoběžnými	rovnoběžný	k2eAgFnPc7d1
nekonečně	konečně	k6eNd1
dlouhými	dlouhý	k2eAgInPc7d1
vodiči	vodič	k1gInPc7
o	o	k7c6
zanedbatelném	zanedbatelný	k2eAgInSc6d1
průřezu	průřez	k1gInSc6
umístěnými	umístěný	k2eAgInPc7d1
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
1	#num#	k4
m	m	kA
od	od	k7c2
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
mezi	mezi	k7c7
vodiči	vodič	k1gMnPc7
působí	působit	k5eAaImIp3nS
magnetická	magnetický	k2eAgFnSc1d1
síla	síla	k1gFnSc1
o	o	k7c6
velikosti	velikost	k1gFnSc6
2	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
newtonu	newton	k1gInSc2
na	na	k7c4
jeden	jeden	k4xCgInSc4
metr	metr	k1gInSc4
délky	délka	k1gFnSc2
vodiče	vodič	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jinak	jinak	k6eAd1
řečeno	říct	k5eAaPmNgNnS
tato	tento	k3xDgFnSc1
definice	definice	k1gFnPc4
zafixovala	zafixovat	k5eAaPmAgFnS
velikost	velikost	k1gFnSc1
permeability	permeabilita	k1gFnSc2
vakua	vakuum	k1gNnSc2
</s>
<s>
μ	μ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mu	on	k3xPp3gMnSc3
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
na	na	k7c6
hodnotě	hodnota	k1gFnSc6
přesně	přesně	k6eAd1
</s>
<s>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
×	×	k?
</s>
<s>
10	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
7	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
4	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
times	times	k1gInSc1
10	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
H	H	kA
<g/>
/	/	kIx~
<g/>
m.	m.	k?
</s>
<s>
Z	z	k7c2
praktického	praktický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
však	však	k9
definice	definice	k1gFnSc1
byla	být	k5eAaImAgFnS
problematická	problematický	k2eAgFnSc1d1
a	a	k8xC
etalony	etalon	k1gInPc1
proudu	proud	k1gInSc2
se	se	k3xPyFc4
konstruovaly	konstruovat	k5eAaImAgFnP
spíše	spíše	k9
na	na	k7c6
základě	základ	k1gInSc6
Josephsonova	Josephsonův	k2eAgInSc2d1
a	a	k8xC
kvantového	kvantový	k2eAgInSc2d1
Hallova	Hallův	k2eAgInSc2d1
jevu	jev	k1gInSc2
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kterých	který	k3yRgInPc2,k3yQgInPc2,k3yIgInPc2
se	se	k3xPyFc4
vytvářely	vytvářet	k5eAaImAgInP
etalony	etalon	k1gInPc1
elektrického	elektrický	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
</s>
<s>
U	u	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
U	u	k7c2
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
elektrického	elektrický	k2eAgInSc2d1
odporu	odpor	k1gInSc2
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R	R	kA
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
realizace	realizace	k1gFnSc1
ampéru	ampér	k1gInSc2
pak	pak	k6eAd1
spočívala	spočívat	k5eAaImAgFnS
na	na	k7c4
aplikaci	aplikace	k1gFnSc4
Ohmova	Ohmův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
</s>
<s>
I	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
U	u	k7c2
</s>
<s>
/	/	kIx~
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
=	=	kIx~
<g/>
U	u	k7c2
<g/>
/	/	kIx~
<g/>
R	R	kA
<g/>
}	}	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
definice	definice	k1gFnSc1
tak	tak	k6eAd1
namísto	namísto	k7c2
permeability	permeabilita	k1gFnSc2
vakua	vakuum	k1gNnSc2
fixuje	fixovat	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
elementárního	elementární	k2eAgInSc2d1
náboje	náboj	k1gInSc2
(	(	kIx(
<g/>
permeabilita	permeabilita	k1gFnSc1
vakua	vakuum	k1gNnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
s	s	k7c7
novou	nový	k2eAgFnSc7d1
definicí	definice	k1gFnSc7
stala	stát	k5eAaPmAgFnS
z	z	k7c2
fixní	fixní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
empiricky	empiricky	k6eAd1
měřenou	měřený	k2eAgFnSc7d1
veličinou	veličina	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
stanovuje	stanovovat	k5eAaImIp3nS
přesně	přesně	k6eAd1
také	také	k9
hodnota	hodnota	k1gFnSc1
Josephsonovy	Josephsonův	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
</s>
<s>
K	k	k7c3
</s>
<s>
J	J	kA
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
e	e	k0
</s>
<s>
/	/	kIx~
</s>
<s>
h	h	k?
</s>
<s>
≈	≈	k?
</s>
<s>
484	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
H	H	kA
</s>
<s>
z	z	k7c2
</s>
<s>
/	/	kIx~
</s>
<s>
V	v	k7c6
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
K_	K_	k1gMnPc6
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
J	J	kA
<g/>
}	}	kIx)
}	}	kIx)
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
e	e	k0
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
\	\	kIx~
<g/>
approx	approx	k1gInSc1
484	#num#	k4
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
THz	THz	k1gMnSc1
<g/>
/	/	kIx~
<g/>
V	V	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
a	a	k8xC
von	von	k1gInSc1
Klitzingovy	Klitzingův	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
</s>
<s>
R	R	kA
</s>
<s>
K	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
h	h	k?
</s>
<s>
/	/	kIx~
</s>
<s>
e	e	k0
</s>
<s>
2	#num#	k4
</s>
<s>
≈	≈	k?
</s>
<s>
25	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
8	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
Ω	Ω	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R_	R_	k1gMnPc6
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
}	}	kIx)
<g/>
=	=	kIx~
<g/>
h	h	k?
<g/>
/	/	kIx~
<g/>
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
approx	approx	k1gInSc1
25	#num#	k4
<g/>
{	{	kIx(
<g/>
,	,	kIx,
<g/>
}	}	kIx)
<g/>
8	#num#	k4
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}	}	kIx)
\	\	kIx~
<g/>
Omega	omega	k1gFnSc1
}}	}}	k?
</s>
<s>
Druhou	druhý	k4xOgFnSc7
možností	možnost	k1gFnSc7
realizace	realizace	k1gFnSc2
ampéru	ampér	k1gInSc2
je	být	k5eAaImIp3nS
využití	využití	k1gNnSc1
jednoelektronové	jednoelektronový	k2eAgFnSc2d1
pumpy	pumpa	k1gFnSc2
s	s	k7c7
přesným	přesný	k2eAgNnSc7d1
taktováním	taktování	k1gNnSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
by	by	kYmCp3nP
byl	být	k5eAaImAgInS
uzavřen	uzavřen	k2eAgInSc4d1
tzv.	tzv.	kA
metrologický	metrologický	k2eAgInSc4d1
trojúhelník	trojúhelník	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kelvin	kelvin	k1gInSc1
</s>
<s>
Kelvin	kelvin	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
„	„	k?
<g/>
K	k	k7c3
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
termodynamické	termodynamický	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
stanovením	stanovení	k1gNnSc7
pevné	pevný	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
Boltzmannovy	Boltzmannův	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
k	k	k7c3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
1,380	1,380	k4
649	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
23	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
jednotce	jednotka	k1gFnSc6
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
kg	kg	kA
m	m	kA
<g/>
2	#num#	k4
<g/>
s	s	k7c7
<g/>
−	−	k?
<g/>
2	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
kilogram	kilogram	k1gInSc1
<g/>
,	,	kIx,
metr	metr	k1gInSc1
a	a	k8xC
sekunda	sekunda	k1gFnSc1
jsou	být	k5eAaImIp3nP
definovány	definovat	k5eAaBmNgInP
prostřednictvím	prostřednictvím	k7c2
h	h	k?
<g/>
,	,	kIx,
c	c	k0
a	a	k8xC
Δ	Δ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
teploty	teplota	k1gFnSc2
v	v	k7c6
metrickém	metrický	k2eAgInSc6d1
systému	systém	k1gInSc6
byl	být	k5eAaImAgInS
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
<g/>
,	,	kIx,
definovaný	definovaný	k2eAgMnSc1d1
přiřazením	přiřazení	k1gNnSc7
hodnoty	hodnota	k1gFnSc2
0	#num#	k4
°	°	k?
<g/>
C	C	kA
teplotě	teplota	k1gFnSc6
tání	tání	k1gNnPc4
ledu	led	k1gInSc2
a	a	k8xC
100	#num#	k4
°	°	k?
<g/>
C	C	kA
teplotě	teplota	k1gFnSc6
varu	var	k1gInSc2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
moderní	moderní	k2eAgFnSc1d1
definice	definice	k1gFnSc1
kelvinu	kelvin	k1gInSc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
označovaného	označovaný	k2eAgInSc2d1
ještě	ještě	k6eAd1
jako	jako	k9
„	„	k?
<g/>
stupeň	stupeň	k1gInSc1
Kelvina	Kelvina	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
°	°	k?
<g/>
K	k	k7c3
<g/>
)	)	kIx)
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
teplotě	teplota	k1gFnSc6
trojného	trojný	k2eAgInSc2d1
bodu	bod	k1gInSc2
vody	voda	k1gFnSc2
přiřadila	přiřadit	k5eAaPmAgFnS
teplota	teplota	k1gFnSc1
273,16	273,16	k4
°	°	k?
<g/>
K.	K.	kA
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
pak	pak	k6eAd1
jednotka	jednotka	k1gFnSc1
získala	získat	k5eAaPmAgFnS
dnešní	dnešní	k2eAgInSc4d1
název	název	k1gInSc4
kelvin	kelvin	k1gInSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
byla	být	k5eAaImAgFnS
definována	definovat	k5eAaBmNgFnS
jako	jako	k9
<g/>
:	:	kIx,
</s>
<s>
Kelvin	kelvin	k1gInSc1
<g/>
,	,	kIx,
jednotka	jednotka	k1gFnSc1
termodynamické	termodynamický	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
zlomku	zlomek	k1gInSc2
1	#num#	k4
<g/>
/	/	kIx~
<g/>
273,16	273,16	k4
termodynamické	termodynamický	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
trojného	trojný	k2eAgInSc2d1
bodu	bod	k1gInSc2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
praktického	praktický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
však	však	k9
tato	tento	k3xDgFnSc1
definice	definice	k1gFnSc1
přinášela	přinášet	k5eAaImAgFnS
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
zejména	zejména	k9
se	s	k7c7
získáváním	získávání	k1gNnSc7
vzorku	vzorek	k1gInSc2
čisté	čistý	k2eAgFnSc2d1
vody	voda	k1gFnSc2
daného	daný	k2eAgNnSc2d1
izotopového	izotopový	k2eAgNnSc2d1
složení	složení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
definice	definice	k1gFnSc1
kelvinu	kelvin	k1gInSc2
umožňuje	umožňovat	k5eAaImIp3nS
převést	převést	k5eAaPmF
měření	měření	k1gNnSc4
teploty	teplota	k1gFnSc2
na	na	k7c4
měření	měření	k1gNnSc4
energie	energie	k1gFnSc2
částic	částice	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
jednodušší	jednoduchý	k2eAgMnSc1d2
<g/>
,	,	kIx,
zejména	zejména	k9
při	při	k7c6
teplotách	teplota	k1gFnPc6
extrémně	extrémně	k6eAd1
vysokých	vysoký	k2eAgFnPc6d1
nebo	nebo	k8xC
nízkých	nízký	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boltzmannova	Boltzmannův	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
k	k	k7c3
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
konstantou	konstanta	k1gFnSc7
statistické	statistický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
spojuje	spojovat	k5eAaImIp3nS
entropii	entropie	k1gFnSc4
s	s	k7c7
rozdělením	rozdělení	k1gNnSc7
pravděpodobnosti	pravděpodobnost	k1gFnSc2
mikrostavů	mikrostav	k1gInPc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
konstanta	konstanta	k1gFnSc1
úměrnosti	úměrnost	k1gFnSc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
ve	v	k7c6
stavové	stavový	k2eAgFnSc6d1
rovnici	rovnice	k1gFnSc6
ideálního	ideální	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určuje	určovat	k5eAaImIp3nS
také	také	k9
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
teplotou	teplota	k1gFnSc7
plynu	plyn	k1gInSc2
a	a	k8xC
pohybovou	pohybový	k2eAgFnSc7d1
energií	energie	k1gFnSc7
jeho	jeho	k3xOp3gFnPc2
molekul	molekula	k1gFnPc2
(	(	kIx(
<g/>
ekvipartiční	ekvipartiční	k2eAgInSc1d1
teorém	teorém	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mol	mol	k1gMnSc1
</s>
<s>
Mol	mol	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
„	„	k?
<g/>
mol	mol	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
látkového	látkový	k2eAgNnSc2d1
množství	množství	k1gNnSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
mol	mol	k1gMnSc1
obsahuje	obsahovat	k5eAaImIp3nS
přesně	přesně	k6eAd1
6,022	6,022	k4
140	#num#	k4
76	#num#	k4
<g/>
×	×	k?
<g/>
1023	#num#	k4
elementárních	elementární	k2eAgFnPc2d1
entit	entita	k1gFnPc2
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
pevná	pevný	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
Avogadrovy	Avogadrův	k2eAgFnPc1d1
konstanty	konstanta	k1gFnPc1
NA	na	k7c4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
jednotce	jednotka	k1gFnSc6
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
nazývá	nazývat	k5eAaImIp3nS
se	se	k3xPyFc4
Avogadrovo	Avogadrův	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
;	;	kIx,
látkové	látkový	k2eAgNnSc1d1
množství	množství	k1gNnSc1
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
mírou	míra	k1gFnSc7
počtu	počet	k1gInSc2
specifikovaných	specifikovaný	k2eAgFnPc2d1
elementárních	elementární	k2eAgFnPc2d1
entit	entita	k1gFnPc2
<g/>
;	;	kIx,
elementární	elementární	k2eAgFnSc7d1
entitou	entita	k1gFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
atom	atom	k1gInSc4
<g/>
,	,	kIx,
molekula	molekula	k1gFnSc1
<g/>
,	,	kIx,
iont	iont	k1gInSc1
<g/>
,	,	kIx,
elektron	elektron	k1gInSc1
<g/>
,	,	kIx,
jakákoli	jakýkoli	k3yIgFnSc1
jiná	jiný	k2eAgFnSc1d1
částice	částice	k1gFnSc1
nebo	nebo	k8xC
specifikované	specifikovaný	k2eAgNnSc1d1
seskupení	seskupení	k1gNnSc1
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Látková	látkový	k2eAgNnPc1d1
množství	množství	k1gNnPc1
se	se	k3xPyFc4
z	z	k7c2
praktických	praktický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
zjišťovala	zjišťovat	k5eAaImAgFnS
na	na	k7c6
základě	základ	k1gInSc6
hmotnosti	hmotnost	k1gFnSc2
a	a	k8xC
vyjadřovala	vyjadřovat	k5eAaImAgFnS
v	v	k7c6
jednotkách	jednotka	k1gFnPc6
„	„	k?
<g/>
gramatom	gramatom	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
mol	mol	k1gInSc1
atomů	atom	k1gInPc2
<g/>
)	)	kIx)
či	či	k8xC
„	„	k?
<g/>
grammolekula	grammolekula	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
mol	mol	k1gInSc1
molekul	molekula	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
relativní	relativní	k2eAgFnSc2d1
atomové	atomový	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
počítaly	počítat	k5eAaImAgFnP
vůči	vůči	k7c3
kyslíku	kyslík	k1gInSc3
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
byla	být	k5eAaImAgFnS
konvencí	konvence	k1gFnSc7
stanovena	stanoven	k2eAgFnSc1d1
relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
16	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
nebylo	být	k5eNaImAgNnS
jednoznačné	jednoznačný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
čistý	čistý	k2eAgInSc4d1
izotop	izotop	k1gInSc4
kyslík-	kyslík-	k?
<g/>
16	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
směs	směs	k1gFnSc1
izotopů	izotop	k1gInPc2
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
vzdušném	vzdušný	k2eAgInSc6d1
kyslíku	kyslík	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
se	se	k3xPyFc4
proto	proto	k8xC
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
definice	definice	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
uhlíku	uhlík	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
čistému	čistý	k2eAgInSc3d1
izotopu	izotop	k1gInSc3
uhlík-	uhlík-	k?
<g/>
12	#num#	k4
stanovena	stanoven	k2eAgFnSc1d1
relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
přesně	přesně	k6eAd1
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
tak	tak	k9
byla	být	k5eAaImAgFnS
definována	definovat	k5eAaBmNgFnS
nová	nový	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
SI	si	k1gNnSc2
<g/>
,	,	kIx,
mol	mol	k1gInSc1
<g/>
,	,	kIx,
definovaná	definovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
</s>
<s>
Mol	mol	k1gInSc1
je	být	k5eAaImIp3nS
látkové	látkový	k2eAgNnSc4d1
množství	množství	k1gNnSc4
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
obsahuje	obsahovat	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
počet	počet	k1gInSc1
elementárních	elementární	k2eAgFnPc2d1
entit	entita	k1gFnPc2
<g/>
,	,	kIx,
kolik	kolik	k4yRc4,k4yQc4,k4yIc4
je	být	k5eAaImIp3nS
atomů	atom	k1gInPc2
v	v	k7c4
0,012	0,012	k4
kg	kg	kA
uhlíku	uhlík	k1gInSc2
12	#num#	k4
<g/>
C.	C.	kA
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
definice	definice	k1gFnSc1
svazovala	svazovat	k5eAaImAgFnS
látkové	látkový	k2eAgNnSc4d1
množství	množství	k1gNnSc4
s	s	k7c7
hmotností	hmotnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
však	však	k9
technický	technický	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
umožnil	umožnit	k5eAaPmAgInS
jednodušší	jednoduchý	k2eAgFnSc4d2
a	a	k8xC
univerzálnější	univerzální	k2eAgFnSc4d2
definici	definice	k1gFnSc4
molu	mol	k1gInSc2
<g/>
,	,	kIx,
přímým	přímý	k2eAgNnSc7d1
definováním	definování	k1gNnSc7
počtu	počet	k1gInSc2
částic	částice	k1gFnPc2
v	v	k7c6
jednotkovém	jednotkový	k2eAgNnSc6d1
látkovém	látkový	k2eAgNnSc6d1
množství	množství	k1gNnSc6
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
N	N	kA
</s>
<s>
/	/	kIx~
</s>
<s>
N	N	kA
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
=	=	kIx~
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
N_	N_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
}}	}}	k?
</s>
<s>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
zafixováním	zafixování	k1gNnSc7
Avogadrovy	Avogadrův	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
<g/>
:	:	kIx,
1	#num#	k4
mol	molo	k1gNnPc2
je	být	k5eAaImIp3nS
jednoduše	jednoduše	k6eAd1
takové	takový	k3xDgNnSc4
látkové	látkový	k2eAgNnSc4d1
množství	množství	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
právě	právě	k9
přesně	přesně	k6eAd1
6,022	6,022	k4
140	#num#	k4
76	#num#	k4
<g/>
×	×	k?
<g/>
1023	#num#	k4
elementárních	elementární	k2eAgFnPc2d1
entit	entita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
nové	nový	k2eAgFnSc2d1
definice	definice	k1gFnSc2
tak	tak	k6eAd1
není	být	k5eNaImIp3nS
látkové	látkový	k2eAgNnSc1d1
množství	množství	k1gNnSc1
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
hmotnosti	hmotnost	k1gFnSc6
(	(	kIx(
<g/>
ani	ani	k8xC
jiných	jiný	k2eAgFnPc6d1
základních	základní	k2eAgFnPc6d1
veličinách	veličina	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kandela	kandela	k1gFnSc1
</s>
<s>
Kandela	kandela	k1gFnSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
„	„	k?
<g/>
cd	cd	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
svítivosti	svítivost	k1gFnSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
definována	definovat	k5eAaBmNgFnS
stanovením	stanovení	k1gNnSc7
pevné	pevný	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
světelné	světelný	k2eAgFnSc2d1
účinnosti	účinnost	k1gFnSc2
monochromatického	monochromatický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
o	o	k7c6
frekvenci	frekvence	k1gFnSc6
540	#num#	k4
<g/>
×	×	k?
<g/>
1012	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
Kcd	Kcd	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
683	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
jednotce	jednotka	k1gFnSc6
lm	lm	k?
W	W	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
cd	cd	kA
sr	sr	k?
W	W	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
nebo	nebo	k8xC
cd	cd	kA
sr	sr	k?
kg	kg	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
m	m	kA
<g/>
−	−	k?
<g/>
2	#num#	k4
s	s	k7c7
<g/>
3	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
kilogram	kilogram	k1gInSc1
<g/>
,	,	kIx,
metr	metr	k1gInSc1
a	a	k8xC
sekunda	sekunda	k1gFnSc1
jsou	být	k5eAaImIp3nP
definovány	definovat	k5eAaBmNgInP
prostřednictvím	prostřednictvím	k7c2
h	h	k?
<g/>
,	,	kIx,
c	c	k0
a	a	k8xC
Δ	Δ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
standardy	standard	k1gInPc1
svítivosti	svítivost	k1gFnSc2
se	se	k3xPyFc4
používaly	používat	k5eAaImAgInP
různé	různý	k2eAgInPc1d1
„	„	k?
<g/>
standardní	standardní	k2eAgFnPc4d1
svíčky	svíčka	k1gFnPc4
<g/>
“	“	k?
o	o	k7c6
definovaném	definovaný	k2eAgNnSc6d1
složení	složení	k1gNnSc6
a	a	k8xC
parametrech	parametr	k1gInPc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
také	také	k9
definovaná	definovaný	k2eAgFnSc1d1
žárovková	žárovkový	k2eAgFnSc1d1
vlákna	vlákna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
univerzálnější	univerzální	k2eAgFnSc1d2
jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
svítivost	svítivost	k1gFnSc1
1	#num#	k4
cm²	cm²	k?
platiny	platina	k1gFnSc2
ohřáté	ohřátý	k2eAgFnPc1d1
na	na	k7c4
bod	bod	k1gInSc4
tání	tání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
přiblížení	přiblížení	k1gNnSc3
hodnoty	hodnota	k1gFnSc2
nové	nový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
k	k	k7c3
původním	původní	k2eAgFnPc3d1
standardním	standardní	k2eAgFnPc3d1
svíčkám	svíčka	k1gFnPc3
byla	být	k5eAaImAgFnS
definice	definice	k1gFnSc1
„	„	k?
<g/>
nové	nový	k2eAgFnPc4d1
svíčky	svíčka	k1gFnPc4
<g/>
“	“	k?
doplněna	doplnit	k5eAaPmNgFnS
o	o	k7c4
koeficient	koeficient	k1gInSc4
1	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
definována	definovat	k5eAaBmNgFnS
pod	pod	k7c7
novým	nový	k2eAgInSc7d1
mezinárodním	mezinárodní	k2eAgInSc7d1
názvem	název	k1gInSc7
kandela	kandela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
byla	být	k5eAaImAgFnS
zařazena	zařadit	k5eAaPmNgFnS
mezi	mezi	k7c4
základní	základní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
definice	definice	k1gFnSc1
zpřesněna	zpřesněn	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Kandela	kandela	k1gFnSc1
je	být	k5eAaImIp3nS
svítivost	svítivost	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
kolmém	kolmý	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
,	,	kIx,
povrchu	povrch	k1gInSc6
1	#num#	k4
<g/>
/	/	kIx~
<g/>
600	#num#	k4
000	#num#	k4
metru	metr	k1gInSc2
čtverečního	čtvereční	k2eAgNnSc2d1
černého	černý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
o	o	k7c6
teplotě	teplota	k1gFnSc6
tuhnoucí	tuhnoucí	k2eAgFnSc2d1
platiny	platina	k1gFnSc2
za	za	k7c2
tlaku	tlak	k1gInSc2
101	#num#	k4
325	#num#	k4
newtonů	newton	k1gInPc2
na	na	k7c4
metr	metr	k1gInSc4
čtvereční	čtvereční	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Problémy	problém	k1gInPc1
s	s	k7c7
realizací	realizace	k1gFnSc7
přesného	přesný	k2eAgInSc2d1
černého	černý	k2eAgInSc2d1
zářiče	zářič	k1gInSc2
o	o	k7c6
takto	takto	k6eAd1
vysoké	vysoký	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
ale	ale	k8xC
vedly	vést	k5eAaImAgFnP
k	k	k7c3
nové	nový	k2eAgFnSc3d1
radiometrické	radiometrický	k2eAgFnSc3d1
definici	definice	k1gFnSc3
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Kandela	kandela	k1gFnSc1
je	být	k5eAaImIp3nS
svítivost	svítivost	k1gFnSc4
zdroje	zdroj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
monochromatické	monochromatický	k2eAgNnSc4d1
záření	záření	k1gNnSc4
o	o	k7c6
frekvenci	frekvence	k1gFnSc6
540	#num#	k4
<g/>
×	×	k?
<g/>
1012	#num#	k4
Hz	Hz	kA
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
intenzita	intenzita	k1gFnSc1
v	v	k7c6
daném	daný	k2eAgInSc6d1
směru	směr	k1gInSc6
je	být	k5eAaImIp3nS
1	#num#	k4
<g/>
/	/	kIx~
<g/>
683	#num#	k4
wattů	watt	k1gInPc2
na	na	k7c4
steradián	steradián	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Změna	změna	k1gFnSc1
při	při	k7c6
redefinici	redefinice	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
opět	opět	k6eAd1
znamenala	znamenat	k5eAaImAgFnS
pouze	pouze	k6eAd1
úpravu	úprava	k1gFnSc4
formulace	formulace	k1gFnSc2
do	do	k7c2
standardního	standardní	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
beze	beze	k7c2
změny	změna	k1gFnSc2
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odvozené	odvozený	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Odvozená	odvozený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
SI	si	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odvozené	odvozený	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nP
kombinacemi	kombinace	k1gFnPc7
(	(	kIx(
<g/>
povoleny	povolen	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
výhradně	výhradně	k6eAd1
součiny	součin	k1gInPc1
a	a	k8xC
podíly	podíl	k1gInPc1
<g/>
)	)	kIx)
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
kilogram	kilogram	k1gInSc4
na	na	k7c4
metr	metr	k1gInSc4
krychlový	krychlový	k2eAgInSc4d1
pro	pro	k7c4
hustotu	hustota	k1gFnSc4
<g/>
,	,	kIx,
metr	metr	k1gInSc4
čtvereční	čtvereční	k2eAgInSc4d1
pro	pro	k7c4
plochu	plocha	k1gFnSc4
<g/>
,	,	kIx,
metr	metr	k1gInSc4
krychlový	krychlový	k2eAgInSc4d1
pro	pro	k7c4
objem	objem	k1gInSc4
<g/>
,	,	kIx,
metr	metr	k1gInSc4
za	za	k7c4
sekundu	sekunda	k1gFnSc4
pro	pro	k7c4
rychlost	rychlost	k1gFnSc4
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k6eAd1
zajištěno	zajistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
soustava	soustava	k1gFnSc1
SI	se	k3xPyFc3
je	být	k5eAaImIp3nS
koherentní	koherentní	k2eAgInSc4d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
že	že	k8xS
při	při	k7c6
výpočtu	výpočet	k1gInSc6
podle	podle	k7c2
veličinových	veličinův	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
není	být	k5eNaImIp3nS
potřeba	potřeba	k1gFnSc1
dodatečných	dodatečný	k2eAgInPc2d1
číselných	číselný	k2eAgInPc2d1
koeficientů	koeficient	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
odvozené	odvozený	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
v	v	k7c6
průběhu	průběh	k1gInSc6
historie	historie	k1gFnSc2
dostaly	dostat	k5eAaPmAgInP
samostatné	samostatný	k2eAgInPc1d1
názvy	název	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
zjednodušují	zjednodušovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gNnSc4
používání	používání	k1gNnSc4
v	v	k7c6
praktickém	praktický	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmito	tento	k3xDgFnPc7
jednotkami	jednotka	k1gFnPc7
jsou	být	k5eAaImIp3nP
becquerel	becquerel	k1gInSc4
<g/>
,	,	kIx,
coulomb	coulomb	k1gInSc1
<g/>
,	,	kIx,
farad	farad	k1gInSc1
<g/>
,	,	kIx,
gray	gray	k1gInPc1
<g/>
,	,	kIx,
henry	henry	k1gInPc1
<g/>
,	,	kIx,
hertz	hertz	k1gInSc1
<g/>
,	,	kIx,
joule	joule	k1gInSc1
<g/>
,	,	kIx,
katal	katal	k1gInSc1
<g/>
,	,	kIx,
lumen	lumen	k1gInSc1
<g/>
,	,	kIx,
lux	lux	k1gInSc1
<g/>
,	,	kIx,
newton	newton	k1gInSc1
<g/>
,	,	kIx,
ohm	ohm	k1gInSc1
<g/>
,	,	kIx,
pascal	pascal	k1gInSc1
<g/>
,	,	kIx,
radián	radián	k1gInSc1
<g/>
,	,	kIx,
siemens	siemens	k1gInSc1
<g/>
,	,	kIx,
sievert	sievert	k1gInSc1
<g/>
,	,	kIx,
steradián	steradián	k1gInSc1
<g/>
,	,	kIx,
tesla	tesla	k1gFnSc1
<g/>
,	,	kIx,
volt	volt	k1gInSc1
<g/>
,	,	kIx,
watt	watt	k1gInSc1
<g/>
,	,	kIx,
weber	weber	k1gInSc1
a	a	k8xC
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
<g/>
s.	s.	k?
<g/>
26	#num#	k4
<g/>
–	–	k?
<g/>
27	#num#	k4
<g/>
,	,	kIx,
137	#num#	k4
<g/>
–	–	k?
<g/>
138	#num#	k4
</s>
<s>
Definice	definice	k1gFnSc1
<g/>
,	,	kIx,
doporučené	doporučený	k2eAgNnSc1d1
značení	značení	k1gNnSc1
odvozených	odvozený	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
jednotky	jednotka	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
závazné	závazný	k2eAgFnPc1d1
značky	značka	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
upraveny	upravit	k5eAaPmNgFnP
normami	norma	k1gFnPc7
řady	řada	k1gFnSc2
ČSN	ČSN	kA
ISO	ISO	kA
IEC	IEC	kA
80000	#num#	k4
„	„	k?
<g/>
Veličiny	veličina	k1gFnSc2
a	a	k8xC
jednotky	jednotka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
předchozí	předchozí	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
ČSN	ČSN	kA
ISO	ISO	kA
31	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
)	)	kIx)
stejného	stejný	k2eAgInSc2d1
názvu	název	k1gInSc2
<g/>
,	,	kIx,
taktéž	taktéž	k?
založenou	založený	k2eAgFnSc7d1
na	na	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Fyzikálním	fyzikální	k2eAgFnPc3d1
veličinám	veličina	k1gFnPc3
přísluší	příslušet	k5eAaImIp3nS
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	si	k1gNnSc2
jediná	jediný	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
buďto	buďto	k8xC
jednotka	jednotka	k1gFnSc1
základní	základní	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
koherentně	koherentně	k6eAd1
stanovená	stanovený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
odvozená	odvozený	k2eAgFnSc1d1
(	(	kIx(
<g/>
včetně	včetně	k7c2
jednotek	jednotka	k1gFnPc2
bezrozměrných	bezrozměrný	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
praktických	praktický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
se	se	k3xPyFc4
v	v	k7c6
mnoha	mnoho	k4c6
fyzikálních	fyzikální	k2eAgInPc6d1
a	a	k8xC
technických	technický	k2eAgInPc6d1
oborech	obor	k1gInPc6
používají	používat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
nekoherentní	koherentní	k2eNgFnPc1d1
s	s	k7c7
hlavními	hlavní	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
SI	si	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
ně	on	k3xPp3gInPc4
byla	být	k5eAaImAgFnS
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	si	k1gNnSc2
vytvořena	vytvořen	k2eAgFnSc1d1
specifická	specifický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
tzv.	tzv.	kA
vedlejších	vedlejší	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
jsou	být	k5eAaImIp3nP
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
dříve	dříve	k6eAd2
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
všeobecnou	všeobecný	k2eAgFnSc4d1
rozšířenost	rozšířenost	k1gFnSc4
a	a	k8xC
užitečnost	užitečnost	k1gFnSc4
řazeny	řadit	k5eAaImNgFnP
do	do	k7c2
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
nebyly	být	k5eNaImAgFnP
odvozeny	odvodit	k5eAaPmNgFnP
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
předepsaným	předepsaný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
(	(	kIx(
<g/>
buďto	buďto	k8xC
používají	používat	k5eAaImIp3nP
dodatečné	dodatečný	k2eAgInPc4d1
číselné	číselný	k2eAgInPc4d1
koeficienty	koeficient	k1gInPc4
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
odvozené	odvozený	k2eAgInPc1d1
od	od	k7c2
přírodních	přírodní	k2eAgFnPc2d1
konstant	konstanta	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
nejsou	být	k5eNaImIp3nP
definovány	definovat	k5eAaBmNgFnP
pomocí	pomocí	k7c2
součinů	součin	k1gInPc2
a	a	k8xC
podílů	podíl	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
v	v	k7c6
případě	případ	k1gInSc6
logaritmických	logaritmický	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
mimosoustavové	mimosoustavový	k2eAgNnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
u	u	k7c2
některých	některý	k3yIgInPc2
je	být	k5eAaImIp3nS
nadále	nadále	k6eAd1
dovoleno	dovolit	k5eAaPmNgNnS
jejich	jejich	k3xOp3gNnSc1
použití	použití	k1gNnSc1
souběžně	souběžně	k6eAd1
s	s	k7c7
jednotkami	jednotka	k1gFnPc7
SI	si	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Mimosoustavové	Mimosoustavový	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
povolené	povolený	k2eAgFnPc1d1
k	k	k7c3
uživání	uživání	k1gNnSc3
souběžně	souběžně	k6eAd1
s	s	k7c7
jednotkami	jednotka	k1gFnPc7
SI	si	k1gNnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
<g/>
s.	s.	k?
<g/>
33	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
,	,	kIx,
145	#num#	k4
<g/>
–	–	k?
<g/>
146	#num#	k4
</s>
<s>
JednotkaVeličinaZnačka	JednotkaVeličinaZnačka	k1gFnSc1
jednotkyPřevod	jednotkyPřevoda	k1gFnPc2
na	na	k7c4
jednotky	jednotka	k1gFnPc4
SI	se	k3xPyFc3
</s>
<s>
minutačasmin	minutačasmin	k1gInSc1
<g/>
1	#num#	k4
min	mina	k1gFnPc2
=	=	kIx~
60	#num#	k4
s	s	k7c7
</s>
<s>
hodinah	hodinah	k1gInSc1
<g/>
1	#num#	k4
h	h	k?
=	=	kIx~
60	#num#	k4
min	mina	k1gFnPc2
=	=	kIx~
3600	#num#	k4
s	s	k7c7
</s>
<s>
dend	dend	k6eAd1
<g/>
1	#num#	k4
d	d	k?
=	=	kIx~
24	#num#	k4
h	h	k?
=	=	kIx~
86	#num#	k4
400	#num#	k4
s	s	k7c7
</s>
<s>
astronomická	astronomický	k2eAgFnSc1d1
jednotkadélkaau	jednotkadélkaau	k5eAaPmIp1nS
<g/>
1	#num#	k4
au	au	k0
=	=	kIx~
149	#num#	k4
597	#num#	k4
870	#num#	k4
700	#num#	k4
m	m	kA
</s>
<s>
úhlový	úhlový	k2eAgInSc1d1
stupeňúhel	stupeňúhel	k1gInSc1
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
°	°	k?
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
180	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
180	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
rad	rad	k1gInSc1
</s>
<s>
úhlová	úhlový	k2eAgFnSc1d1
minuta	minuta	k1gFnSc1
<g/>
′	′	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
=	=	kIx~
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
°	°	k?
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
10	#num#	k4
</s>
<s>
800	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
10	#num#	k4
<g/>
{	{	kIx(
<g/>
}	}	kIx)
<g/>
800	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
rad	rad	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
úhlová	úhlový	k2eAgFnSc1d1
<g/>
)	)	kIx)
vteřina	vteřina	k1gFnSc1
<g/>
″	″	k?
<g/>
1	#num#	k4
<g/>
″	″	k?
=	=	kIx~
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
′	′	k?
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
648	#num#	k4
</s>
<s>
000	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
648	#num#	k4
<g/>
{	{	kIx(
<g/>
}	}	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
rad	rad	k1gInSc1
</s>
<s>
hektarplochaha	hektarplochaha	k1gFnSc1
<g/>
1	#num#	k4
ha	ha	kA
=	=	kIx~
1	#num#	k4
hm²	hm²	k?
=	=	kIx~
10	#num#	k4
<g/>
⁴	⁴	k?
m²	m²	k?
</s>
<s>
litrobjeml	litrobjeml	k1gMnSc1
<g/>
,	,	kIx,
L1	L1	k1gMnSc1
l	l	kA
=	=	kIx~
1	#num#	k4
L	L	kA
=	=	kIx~
1	#num#	k4
dm	dm	kA
<g/>
3	#num#	k4
=	=	kIx~
103	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
=	=	kIx~
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
m	m	kA
<g/>
3	#num#	k4
</s>
<s>
tunahmotnostt	tunahmotnostt	k1gInSc1
<g/>
1	#num#	k4
t	t	k?
=	=	kIx~
10	#num#	k4
<g/>
³	³	k?
kg	kg	kA
</s>
<s>
daltonPa	daltonPa	k6eAd1
<g/>
1	#num#	k4
Da	Da	k1gFnPc2
=	=	kIx~
1,660	1,660	k4
539	#num#	k4
066	#num#	k4
60	#num#	k4
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
)	)	kIx)
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
27	#num#	k4
kg	kg	kA
</s>
<s>
elektronvoltenergieeV	elektronvoltenergieeV	k?
<g/>
1	#num#	k4
eV	eV	k?
=	=	kIx~
1,602	1,602	k4
176	#num#	k4
634	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
19	#num#	k4
J	J	kA
</s>
<s>
neperlogaritmické	perlogaritmický	k2eNgFnPc1d1
jednotky	jednotka	k1gFnPc1
pro	pro	k7c4
poměrNpbezrozměrné	poměrNpbezrozměrný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
vyjadřující	vyjadřující	k2eAgInSc1d1
logaritmus	logaritmus	k1gInSc1
poměru	poměr	k1gInSc2
dvou	dva	k4xCgFnPc2
hodnot	hodnota	k1gFnPc2
</s>
<s>
belB	belB	k?
</s>
<s>
decibeldB	decibeldB	k?
</s>
<s>
Násobné	násobný	k2eAgFnPc1d1
a	a	k8xC
dílčí	dílčí	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
V	v	k7c6
různých	různý	k2eAgInPc6d1
přírodovědných	přírodovědný	k2eAgInPc6d1
i	i	k8xC
technických	technický	k2eAgInPc6d1
oborech	obor	k1gInPc6
jsou	být	k5eAaImIp3nP
běžné	běžný	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
veličin	veličina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
o	o	k7c4
mnoho	mnoho	k4c4
řádů	řád	k1gInPc2
liší	lišit	k5eAaImIp3nP
od	od	k7c2
velikosti	velikost	k1gFnSc2
hlavní	hlavní	k2eAgFnSc2d1
(	(	kIx(
<g/>
základní	základní	k2eAgFnSc2d1
nebo	nebo	k8xC
odvozené	odvozený	k2eAgFnSc2d1
<g/>
)	)	kIx)
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
používané	používaný	k2eAgFnSc2d1
povolené	povolený	k2eAgFnSc2d1
mimosoustavové	mimosoustavový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zjednodušení	zjednodušení	k1gNnSc4
jejich	jejich	k3xOp3gNnSc2
vyjádření	vyjádření	k1gNnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
jejich	jejich	k3xOp3gInPc1
násobky	násobek	k1gInPc1
a	a	k8xC
díly	díl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
připouští	připouštět	k5eAaImIp3nS
výhradně	výhradně	k6eAd1
dekadické	dekadický	k2eAgInPc4d1
násobky	násobek	k1gInPc4
a	a	k8xC
díly	díl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vyjádření	vyjádření	k1gNnSc3
násobků	násobek	k1gInPc2
nebo	nebo	k8xC
dílů	díl	k1gInPc2
hlavních	hlavní	k2eAgFnPc2d1
i	i	k8xC
povolených	povolený	k2eAgFnPc2d1
mimosoustavových	mimosoustavový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
slouží	sloužit	k5eAaImIp3nP
předpony	předpona	k1gFnPc1
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
předpony	předpona	k1gFnPc1
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
a	a	k8xC
pro	pro	k7c4
ně	on	k3xPp3gFnPc4
vytvořené	vytvořený	k2eAgFnPc4d1
značky	značka	k1gFnPc4
<g/>
,	,	kIx,
závazné	závazný	k2eAgFnPc4d1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
značky	značka	k1gFnSc2
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daná	daný	k2eAgFnSc1d1
násobná	násobný	k2eAgFnSc1d1
či	či	k8xC
dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
pouze	pouze	k6eAd1
jedinou	jediný	k2eAgFnSc4d1
takovou	takový	k3xDgFnSc4
předponu	předpona	k1gFnSc4
<g/>
,	,	kIx,
nelze	lze	k6eNd1
je	být	k5eAaImIp3nS
tedy	tedy	k9
kombinovat	kombinovat	k5eAaImF
(	(	kIx(
<g/>
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
s	s	k7c7
tím	ten	k3xDgNnSc7
lze	lze	k6eAd1
v	v	k7c6
praxi	praxe	k1gFnSc6
občas	občas	k6eAd1
setkat	setkat	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Relativita	relativita	k1gFnSc1
</s>
<s>
Jednotky	jednotka	k1gFnPc1
SI	se	k3xPyFc3
jsou	být	k5eAaImIp3nP
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c6
teorii	teorie	k1gFnSc6
relativity	relativita	k1gFnSc2
realizovány	realizovat	k5eAaBmNgFnP
podle	podle	k7c2
svých	svůj	k3xOyFgFnPc2
definic	definice	k1gFnPc2
lokálně	lokálně	k6eAd1
jako	jako	k9
vlastní	vlastní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
(	(	kIx(
<g/>
veličin	veličina	k1gFnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
,	,	kIx,
vlastní	vlastnit	k5eAaImIp3nS
délka	délka	k1gFnSc1
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
atp.	atp.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
<g/>
s.	s.	k?
<g/>
30	#num#	k4
<g/>
,	,	kIx,
141	#num#	k4
<g/>
–	–	k?
<g/>
142	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Jednoelektronové	jednoelektronový	k2eAgFnSc2d1
pumpy	pumpa	k1gFnSc2
s	s	k7c7
laditelným	laditelný	k2eAgNnSc7d1
taktováním	taktování	k1gNnSc7
se	se	k3xPyFc4
realizují	realizovat	k5eAaBmIp3nP
polovodičovými	polovodičový	k2eAgInPc7d1
přechody	přechod	k1gInPc7
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
tzv.	tzv.	kA
hybridními	hybridní	k2eAgInPc7d1
elektronickými	elektronický	k2eAgInPc7d1
turnikety	turniket	k1gInPc7
využívajícími	využívající	k2eAgInPc7d1
přechodu	přechod	k1gInSc3
mezi	mezi	k7c7
normální	normální	k2eAgFnSc7d1
a	a	k8xC
supravodivou	supravodivý	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpřesnějším	přesný	k2eAgInSc7d3
typem	typ	k1gInSc7
jsou	být	k5eAaImIp3nP
pumpy	pumpa	k1gFnPc1
s	s	k7c7
metalickými	metalický	k2eAgInPc7d1
ostrůvky	ostrůvek	k1gInPc7
<g/>
,	,	kIx,
využívající	využívající	k2eAgInSc1d1
tunelový	tunelový	k2eAgInSc1d1
jev	jev	k1gInSc1
přes	přes	k7c4
přechodovou	přechodový	k2eAgFnSc4d1
bariéru	bariéra	k1gFnSc4
<g/>
;	;	kIx,
jsou	být	k5eAaImIp3nP
však	však	k9
natolik	natolik	k6eAd1
pomalé	pomalý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
proudový	proudový	k2eAgInSc4d1
etalon	etalon	k1gInSc4
jsou	být	k5eAaImIp3nP
nevyhovující	vyhovující	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
se	se	k3xPyFc4
při	při	k7c6
spolupráci	spolupráce	k1gFnSc6
britských	britský	k2eAgInPc2d1
výzkumných	výzkumný	k2eAgInPc2d1
týmů	tým	k1gInPc2
NPL	NPL	kA
a	a	k8xC
Cavendishovy	Cavendishův	k2eAgFnSc2d1
laboratoře	laboratoř	k1gFnSc2
podařilo	podařit	k5eAaPmAgNnS
vytvořit	vytvořit	k5eAaPmF
jednoelektronovou	jednoelektronový	k2eAgFnSc4d1
pumpu	pumpa	k1gFnSc4
na	na	k7c6
bázi	báze	k1gFnSc6
grafenu	grafen	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
chování	chování	k1gNnSc4
podobné	podobný	k2eAgNnSc4d1
kovu	kov	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
přitom	přitom	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
rychlý	rychlý	k2eAgInSc4d1
průchod	průchod	k1gInSc4
elektronů	elektron	k1gInPc2
pumpou	pumpa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dosaženo	dosáhnout	k5eAaPmNgNnS
frekvence	frekvence	k1gFnSc2
řádu	řád	k1gInSc2
gigahertzů	gigahertz	k1gInPc2
<g/>
,	,	kIx,
požadované	požadovaný	k2eAgFnSc2d1
pro	pro	k7c4
proudový	proudový	k2eAgInSc4d1
etalon	etalon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k6eAd1
nejvýznamnějším	významný	k2eAgMnSc7d3
současným	současný	k2eAgMnSc7d1
kandidátem	kandidát	k1gMnSc7
na	na	k7c4
přímou	přímý	k2eAgFnSc4d1
realizaci	realizace	k1gFnSc4
nově	nově	k6eAd1
definovaného	definovaný	k2eAgInSc2d1
ampéru	ampér	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bureau	Burea	k2eAgFnSc4d1
International	International	k1gFnSc4
des	des	k1gNnSc2
Poids	Poidsa	k1gFnPc2
et	et	k?
Mesures	Mesures	k1gMnSc1
<g/>
↑	↑	k?
Fundamental	Fundamental	k1gMnSc1
Physical	Physical	k1gMnSc1
Constants	Constants	k1gInSc1
-	-	kIx~
CODATA	CODATA	kA
<g/>
.	.	kIx.
www.codata.org	www.codata.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BIPM	BIPM	kA
-	-	kIx~
future	futur	k1gMnSc5
revision	revision	k1gInSc1
of	of	k?
the	the	k?
SI	si	k1gNnSc1
<g/>
.	.	kIx.
www.bipm.org	www.bipm.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠÍRA	šíro	k1gNnSc2
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revize	revize	k1gFnSc1
SI	si	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
(	(	kIx(
<g/>
Objective	Objectiv	k1gInSc5
Source	Sourec	k1gInPc1
E-	E-	k1gFnSc1
Learning	Learning	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osel	osel	k1gMnSc1
<g/>
,	,	kIx,
<g/>
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2019-04-25	2019-04-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1214	#num#	k4
<g/>
-	-	kIx~
<g/>
6307	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
505	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
se	s	k7c7
změnami	změna	k1gFnPc7
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
119	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
137	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
13	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
226	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
444	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
481	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
223	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
a	a	k8xC
155	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
<g/>
264	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zákony	zákon	k1gInPc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
<g/>
:	:	kIx,
Zákon	zákon	k1gInSc1
č.	č.	k?
35	#num#	k4
<g/>
/	/	kIx~
<g/>
1962	#num#	k4
Sb	sb	kA
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Bureau	Bureaus	k1gInSc2
international	internationat	k5eAaPmAgInS,k5eAaImAgInS
des	des	k1gNnSc4
poids	poids	k6eAd1
et	et	k?
mesures	mesures	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gFnSc1
Systè	Systè	k1gInSc5
international	internationat	k5eAaPmAgInS,k5eAaImAgInS
d	d	k?
<g/>
’	’	k?
<g/>
unités	unités	k1gInSc1
(	(	kIx(
<g/>
SI	si	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
The	The	k1gMnSc7
International	International	k1gMnSc7
System	Syst	k1gMnSc7
of	of	k?
Units	Units	k1gInSc1
(	(	kIx(
<g/>
SI	si	k1gNnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
92	#num#	k4
<g/>
-	-	kIx~
<g/>
822	#num#	k4
<g/>
-	-	kIx~
<g/>
2272	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
9	#num#	k4
jedenácté	jedenáctý	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
CCTF	CCTF	kA
Strategy	Stratega	k1gFnSc2
Document	Document	k1gInSc4
<g/>
,	,	kIx,
květen	květen	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
RIEHLE	RIEHLE	kA
<g/>
,	,	kIx,
Fritz	Fritz	k1gMnSc1
<g/>
;	;	kIx,
GILL	GILL	kA
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
<g/>
;	;	kIx,
ARIAS	ARIAS	kA
<g/>
,	,	kIx,
Felicitas	Felicitas	k1gFnSc1
<g/>
;	;	kIx,
ROBERTSSON	ROBERTSSON	kA
<g/>
,	,	kIx,
Lennart	Lennart	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
CIPM	CIPM	kA
list	list	k1gInSc1
of	of	k?
recommended	recommended	k1gInSc4
frequency	frequenca	k1gFnSc2
standard	standard	k1gInSc1
values	values	k1gMnSc1
<g/>
:	:	kIx,
guidelines	guidelines	k1gMnSc1
and	and	k?
procedures	procedures	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Towards	Towardsa	k1gFnPc2
a	a	k8xC
new	new	k?
definition	definition	k1gInSc1
of	of	k?
the	the	k?
SI	se	k3xPyFc3
second	second	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
196	#num#	k4
<g/>
-	-	kIx~
<g/>
197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metrologia	Metrologia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IOP	IOP	kA
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc4
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
55	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
196	#num#	k4
<g/>
-	-	kIx~
<g/>
197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1681	#num#	k4
<g/>
-	-	kIx~
<g/>
7575	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1681	#num#	k4
<g/>
-	-	kIx~
<g/>
7575	#num#	k4
<g/>
/	/	kIx~
<g/>
aaa	aaa	k?
<g/>
302	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
6	#num#	k4
jedenácté	jedenáctý	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
GIBLIN	GIBLIN	kA
<g/>
,	,	kIx,
S.	S.	kA
P.	P.	kA
<g/>
;	;	kIx,
BAE	BAE	kA
<g/>
,	,	kIx,
M.	M.	kA
<g/>
-	-	kIx~
<g/>
H.	H.	kA
<g/>
;	;	kIx,
AHN	AHN	kA
<g/>
,	,	kIx,
Ye-Hwan	Ye-Hwan	k1gMnSc1
<g/>
;	;	kIx,
KATAOKA	KATAOKA	kA
<g/>
,	,	kIx,
M.	M.	kA
Robust	Robust	k1gInSc1
operation	operation	k1gInSc1
of	of	k?
a	a	k8xC
GaAs	GaAs	k1gInSc1
tunable	tunable	k6eAd1
barrier	barrier	k1gInSc1
electron	electron	k1gInSc1
pump	pumpa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
299	#num#	k4
<g/>
-	-	kIx~
<g/>
306	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metrologia	Metrologia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BIPM	BIPM	kA
&	&	k?
IOP	IOP	kA
Publishing	Publishing	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
54	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
299	#num#	k4
<g/>
-	-	kIx~
<g/>
306	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1681	#num#	k4
<g/>
-	-	kIx~
<g/>
7575	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1681	#num#	k4
<g/>
-	-	kIx~
<g/>
7575	#num#	k4
<g/>
/	/	kIx~
<g/>
aa	aa	k?
<g/>
634	#num#	k4
<g/>
c.	c.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
CONNOLLY	CONNOLLY	kA
<g/>
,	,	kIx,
M.	M.	kA
R.	R.	kA
<g/>
,	,	kIx,
CHIU	CHIU	kA
<g/>
,	,	kIx,
K.	K.	kA
L.	L.	kA
<g/>
;	;	kIx,
GIBLIN	GIBLIN	kA
<g/>
,	,	kIx,
S.	S.	kA
P.	P.	kA
<g/>
;	;	kIx,
KATAOKA	KATAOKA	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
FLETCHER	FLETCHER	kA
<g/>
,	,	kIx,
J.	J.	kA
D.	D.	kA
<g/>
;	;	kIx,
CHUA	CHUA	kA
<g/>
,	,	kIx,
C.	C.	kA
<g/>
;	;	kIx,
GRIFFITHS	GRIFFITHS	kA
<g/>
,	,	kIx,
J.	J.	kA
P.	P.	kA
<g/>
;	;	kIx,
JONES	JONES	kA
<g/>
,	,	kIx,
G.	G.	kA
A.	A.	kA
C.	C.	kA
<g/>
;	;	kIx,
FAĽKO	FAĽKO	kA
<g/>
,	,	kIx,
V.	V.	kA
I.	I.	kA
<g/>
;	;	kIx,
SMITH	SMITH	kA
<g/>
,	,	kIx,
C.	C.	kA
G.	G.	kA
<g/>
;	;	kIx,
JANSSEN	JANSSEN	kA
T.	T.	kA
J.	J.	kA
B.	B.	kA
M.	M.	kA
Gigahertz	Gigahertz	k1gMnSc1
quantized	quantized	k1gMnSc1
charge	chargat	k5eAaPmIp3nS
pumping	pumping	k1gInSc4
in	in	k?
graphene	graphen	k1gInSc5
quantum	quantum	k1gNnSc4
dots	dots	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
417	#num#	k4
<g/>
–	–	k?
<g/>
420	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Nanotechnology	Nanotechnolog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
417	#num#	k4
<g/>
–	–	k?
<g/>
420	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1748	#num#	k4
<g/>
-	-	kIx~
<g/>
3395	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nnano	nnano	k6eAd1
<g/>
.2013	.2013	k4
<g/>
.73	.73	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
DUMÉ	DUMÉ	kA
<g/>
,	,	kIx,
Belle	bell	k1gInSc5
<g/>
:	:	kIx,
Redefining	Redefining	k1gInSc1
the	the	k?
ampere	amprat	k5eAaPmIp3nS
with	with	k1gInSc4
the	the	k?
help	help	k1gInSc1
of	of	k?
graphene	graphen	k1gInSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
Physics	Physics	k1gInSc1
<g/>
.	.	kIx.
<g/>
World	World	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
3	#num#	k4
třinácté	třináctý	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
4	#num#	k4
třinácté	třináctý	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
3	#num#	k4
čtrnácté	čtrnáctý	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
míry	míra	k1gFnSc2
a	a	k8xC
váhy	váha	k1gFnSc2
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
6	#num#	k4
desáté	desátá	k1gFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
5	#num#	k4
třinácté	třináctý	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc4
č.	č.	k?
3	#num#	k4
šestnácté	šestnáctý	k4xOgFnSc2
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Metrická	metrický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Metrologie	metrologie	k1gFnSc1
</s>
<s>
Fyzika	fyzika	k1gFnSc1
</s>
<s>
Angloamerická	angloamerický	k2eAgFnSc1d1
měrná	měrný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Soustava	soustava	k1gFnSc1
SI	si	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
SI	se	k3xPyFc3
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
</s>
<s>
Zákon	zákon	k1gInSc1
505	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
metrologii	metrologie	k1gFnSc6
(	(	kIx(
<g/>
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgFnPc2d2
změn	změna	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
základní	základní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
ampér	ampér	k1gInSc1
</s>
<s>
kandela	kandela	k1gFnSc1
</s>
<s>
kelvin	kelvin	k1gInSc1
</s>
<s>
kilogram	kilogram	k1gInSc1
</s>
<s>
metr	metr	k1gInSc1
</s>
<s>
mol	mol	k1gMnSc1
</s>
<s>
sekunda	sekunda	k1gFnSc1
odvozené	odvozený	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
becquerel	becquerel	k1gInSc1
</s>
<s>
coulomb	coulomb	k1gInSc1
</s>
<s>
farad	farad	k1gInSc1
</s>
<s>
gray	graa	k1gFnPc1
</s>
<s>
henry	henry	k1gInSc1
</s>
<s>
hertz	hertz	k1gInSc1
</s>
<s>
joule	joule	k1gInSc1
</s>
<s>
katal	katal	k1gMnSc1
</s>
<s>
lumen	lumen	k1gMnSc1
</s>
<s>
lux	lux	k1gInSc1
</s>
<s>
newton	newton	k1gInSc1
</s>
<s>
ohm	ohm	k1gInSc1
</s>
<s>
pascal	pascal	k1gInSc1
</s>
<s>
radián	radián	k1gInSc1
</s>
<s>
siemens	siemens	k1gInSc1
</s>
<s>
sievert	sievert	k1gMnSc1
</s>
<s>
steradián	steradián	k1gInSc1
</s>
<s>
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
</s>
<s>
tesla	tesla	k1gFnSc1
</s>
<s>
volt	volt	k1gInSc1
</s>
<s>
watt	watt	k1gInSc1
</s>
<s>
weber	weber	k1gInSc1
další	další	k2eAgFnSc2d1
</s>
<s>
předpony	předpona	k1gFnPc1
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
</s>
<s>
systémy	systém	k1gInPc1
měření	měření	k1gNnSc2
</s>
<s>
převody	převod	k1gInPc1
jednotek	jednotka	k1gFnPc2
</s>
<s>
nové	nový	k2eAgFnPc1d1
definice	definice	k1gFnPc1
SI	se	k3xPyFc3
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4077436-3	4077436-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
2913	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85084442	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85084442	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
