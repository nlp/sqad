<s>
Soustava	soustava	k1gFnSc1
SI	SI	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
francouzského	francouzský	k2eAgMnSc2d1
„	„	kIx"
<g/>
Le	Le	k1gMnSc5
Systè	Systè	k1gMnSc5
International	International	k1gFnPc6
d	d	k7
<g/>
'	'	kIx"
<g/>
Unités	Unités	k1gInSc1
<g/>
“	“	kIx"
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	kIx~
česky	česky	k6eAd1
„	„	kIx"
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1
systém	systém	k1gInSc1
jednotek	jednotka	k1gFnPc2
<g/>
“	“	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodně	mezinárodně	k6eAd1
domluvená	domluvený	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
jednotek	jednotka	k1gFnPc2
fyzikálních	fyzikální	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
sedmi	sedm	k4xCc2
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
nich	on	k3xPp3gInPc6
aritmeticky	aritmeticky	k6eAd1
závisejících	závisející	k2eAgFnPc2d1
odvozených	odvozený	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
dekadickými	dekadický	k2eAgFnPc7d1
předponami	předpona	k1gFnPc7
tvořených	tvořený	k2eAgInPc2d1
násobků	násobek	k1gInPc2
a	a	k8xC
dílů	díl	k1gInPc2
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>