<s>
Zirkonium	zirkonium	k1gNnSc1
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
90	#num#	k4
</s>
<s>
Zr	Zr	kA
</s>
<s>
40	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
<g/>
,	,	kIx,
Zr	Zr	k1gFnSc1
<g/>
,	,	kIx,
40	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
zirconium	zirconium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
165	#num#	k4
až	až	k6eAd1
220	#num#	k4
ppm	ppm	k?
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
</s>
<s>
0,000022	0,000022	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Šedý	Šedý	k1gMnSc1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-67-7	7440-67-7	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
91,224	91,224	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
160	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
175	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
79	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
IV	IV	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,33	1,33	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
640,1	640,1	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1270	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
2218	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Šesterečná	šesterečný	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
14,02	14,02	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
6,52	6,52	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
5,0	5,0	k4
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
100	#num#	k4
Pa	Pa	kA
při	při	k7c6
3197K	3197K	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
3800	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
22,6	22,6	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1854,85	1854,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
128	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
4408,85	4408,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
4	#num#	k4
682	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
14	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
573	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
25,36	25,36	k4
Jmol	Jmol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
2,36	2,36	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
421	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
−	−	k?
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R	R	kA
<g/>
15	#num#	k4
<g/>
,	,	kIx,
R17	R17	k1gFnSc1
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S	s	k7c7
<g/>
2	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
S43	S43	k1gFnSc1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
88	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
83,4	83,4	k4
hodiny	hodina	k1gFnPc4
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
88Y	88Y	k4
</s>
<s>
γ	γ	k?
</s>
<s>
0,392	0,392	k4
</s>
<s>
88Y	88Y	k4
</s>
<s>
89	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
78,4	78,4	k4
hodiny	hodina	k1gFnPc4
</s>
<s>
ε	ε	k?
</s>
<s>
89Y	89Y	k4
</s>
<s>
β	β	k?
<g/>
+	+	kIx~
</s>
<s>
0,902	0,902	k4
</s>
<s>
89Y	89Y	k4
</s>
<s>
γ	γ	k?
</s>
<s>
0,909	0,909	k4
</s>
<s>
89Y	89Y	k4
</s>
<s>
90	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
51,45	51,45	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
50	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
91	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
11,22	11,22	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
51	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
92	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
17,15	17,15	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
52	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
93	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
stopy	stopa	k1gFnPc1
</s>
<s>
1,53	1,53	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,060	0,060	k4
</s>
<s>
93	#num#	k4
<g/>
Nb	Nb	k1gFnSc1
</s>
<s>
94	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
17,38	17,38	k4
<g/>
%	%	kIx~
</s>
<s>
1,1	1,1	k4
<g/>
×	×	k?
<g/>
1017	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2	#num#	k4
×	×	k?
β	β	k?
<g/>
−	−	k?
</s>
<s>
-	-	kIx~
</s>
<s>
94	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
95	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
2,8	2,8	k4
<g/>
%	%	kIx~
</s>
<s>
2,0	2,0	k4
<g/>
×	×	k?
<g/>
1019	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2	#num#	k4
×	×	k?
β	β	k?
<g/>
−	−	k?
</s>
<s>
3,348	3,348	k4
</s>
<s>
95	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
96	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
2,80	2,80	k4
<g/>
%	%	kIx~
</s>
<s>
2,0	2,0	k4
<g/>
×	×	k?
<g/>
1019	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2	#num#	k4
×	×	k?
β	β	k?
<g/>
−	−	k?
</s>
<s>
-	-	kIx~
</s>
<s>
96	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ti	ten	k3xDgMnPc1
<g/>
⋏	⋏	k?
</s>
<s>
Yttrium	yttrium	k1gNnSc1
≺	≺	k?
<g/>
Zr	Zr	k1gMnSc1
<g/>
≻	≻	k?
Niob	niob	k1gInSc1
</s>
<s>
⋎	⋎	k?
<g/>
Hf	Hf	k1gFnSc1
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Zr	Zr	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
zirconium	zirconium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
mimořádně	mimořádně	k6eAd1
odolný	odolný	k2eAgMnSc1d1
proti	proti	k7c3
korozi	koroze	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
uplatnění	uplatnění	k1gNnSc1
nalézá	nalézat	k5eAaImIp3nS
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
energetice	energetika	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
vykazuje	vykazovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
nízký	nízký	k2eAgInSc1d1
účinný	účinný	k2eAgInSc1d1
průřez	průřez	k1gInSc1
pro	pro	k7c4
záchyt	záchyt	k1gInSc4
neutronů	neutron	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
složkou	složka	k1gFnSc7
různých	různý	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
a	a	k8xC
protikorozních	protikorozní	k2eAgFnPc2d1
ochranných	ochranný	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jako	jako	k8xC,k8xS
objevitel	objevitel	k1gMnSc1
zirkonia	zirkonium	k1gNnSc2
je	být	k5eAaImIp3nS
uváděn	uváděn	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Klaproth	Klaproth	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1789	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
jej	on	k3xPp3gMnSc4
rozkladem	rozklad	k1gInSc7
minerálu	minerál	k1gInSc2
jargonu	jargon	k1gInSc2
ze	z	k7c2
Srí	Srí	k1gFnSc2
Lanky	lanko	k1gNnPc7
<g/>
,	,	kIx,
tehdejšího	tehdejší	k2eAgInSc2d1
Ceylonu	Ceylon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
úspěšný	úspěšný	k2eAgInSc1d1
pokus	pokus	k1gInSc1
o	o	k7c6
izolaci	izolace	k1gFnSc6
elementárního	elementární	k2eAgNnSc2d1
zirkonia	zirkonium	k1gNnSc2
provedl	provést	k5eAaPmAgInS
roku	rok	k1gInSc2
1824	#num#	k4
chemik	chemik	k1gMnSc1
Jöns	Jönsa	k1gFnPc2
Jacob	Jacoba	k1gFnPc2
Berzelius	Berzelius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
produkt	produkt	k1gInSc1
nebyl	být	k5eNaImAgInS
však	však	k9
dokonale	dokonale	k6eAd1
čistý	čistý	k2eAgInSc1d1
a	a	k8xC
skutečně	skutečně	k6eAd1
čisté	čistý	k2eAgNnSc1d1
elementární	elementární	k2eAgNnSc1d1
zirkonium	zirkonium	k1gNnSc1
bylo	být	k5eAaImAgNnS
získáno	získat	k5eAaPmNgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tyčinka	tyčinka	k1gFnSc1
z	z	k7c2
kovového	kovový	k2eAgNnSc2d1
zirkonia	zirkonium	k1gNnSc2
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
středně	středně	k6eAd1
tvrdý	tvrdý	k2eAgMnSc1d1
a	a	k8xC
poměrně	poměrně	k6eAd1
lehký	lehký	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
supravodičem	supravodič	k1gInSc7
prvního	první	k4xOgInSc2
typu	typ	k1gInSc2
za	za	k7c2
teplot	teplota	k1gFnPc2
pod	pod	k7c4
0,70	0,70	k4
K.	K.	kA
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
mimořádnou	mimořádný	k2eAgFnSc7d1
chemickou	chemický	k2eAgFnSc7d1
stálostí	stálost	k1gFnSc7
–	–	k?
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
netečné	netečný	k2eAgNnSc1d1,k2eNgNnSc1d1
k	k	k7c3
působení	působení	k1gNnSc3
vody	voda	k1gFnSc2
a	a	k8xC
odolává	odolávat	k5eAaImIp3nS
působení	působení	k1gNnSc1
většiny	většina	k1gFnSc2
běžných	běžný	k2eAgFnPc2d1
minerálních	minerální	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
i	i	k8xC
roztoků	roztok	k1gInPc2
alkalických	alkalický	k2eAgInPc2d1
hydroxidů	hydroxid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
rozpouštění	rozpouštění	k1gNnSc4
je	být	k5eAaImIp3nS
nejúčinnější	účinný	k2eAgFnSc1d3
kyselina	kyselina	k1gFnSc1
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
HF	HF	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
její	její	k3xOp3gFnPc4
směsi	směs	k1gFnPc4
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
minerálními	minerální	k2eAgFnPc7d1
kyselinami	kyselina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
vykazuje	vykazovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
afinitu	afinita	k1gFnSc4
ke	k	k7c3
kyslíku	kyslík	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jemně	jemně	k6eAd1
rozptýlený	rozptýlený	k2eAgInSc1d1
kov	kov	k1gInSc1
proto	proto	k8xC
může	moct	k5eAaImIp3nS
na	na	k7c6
vzduchu	vzduch	k1gInSc6
samovolně	samovolně	k6eAd1
vzplanout	vzplanout	k5eAaPmF
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
za	za	k7c2
zvýšené	zvýšený	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kusové	kusový	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
(	(	kIx(
<g/>
slitky	slitek	k1gInPc1
<g/>
,	,	kIx,
plechy	plech	k1gInPc1
<g/>
,	,	kIx,
dráty	drát	k1gInPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
však	však	k9
na	na	k7c6
vzduchu	vzduch	k1gInSc6
naprosto	naprosto	k6eAd1
stálé	stálý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Zr	Zr	k1gFnSc2
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
i	i	k9
sloučeniny	sloučenina	k1gFnPc1
Zr	Zr	k1gFnSc7
<g/>
+	+	kIx~
<g/>
3	#num#	k4
a	a	k8xC
Zr	Zr	k1gMnSc1
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Těžba	těžba	k1gFnSc1
zirkonia	zirkonium	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
poměrně	poměrně	k6eAd1
hojně	hojně	k6eAd1
zastoupeno	zastoupit	k5eAaPmNgNnS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
165	#num#	k4
<g/>
–	–	k?
<g/>
220	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
chemické	chemický	k2eAgFnSc3d1
stálosti	stálost	k1gFnSc3
přítomno	přítomno	k1gNnSc4
pouze	pouze	k6eAd1
v	v	k7c6
koncentraci	koncentrace	k1gFnSc6
0,000	0,000	k4
022	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
zirkonia	zirkonium	k1gNnSc2
na	na	k7c4
1	#num#	k4
miliardu	miliarda	k4xCgFnSc4
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalézáme	nalézat	k5eAaImIp1nP
jej	on	k3xPp3gMnSc4
v	v	k7c6
řadě	řada	k1gFnSc6
minerálů	minerál	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
vlastnosti	vlastnost	k1gFnPc4
(	(	kIx(
<g/>
tvrdost	tvrdost	k1gFnSc1
a	a	k8xC
vzhledová	vzhledový	k2eAgFnSc1d1
podobnost	podobnost	k1gFnSc1
s	s	k7c7
diamantem	diamant	k1gInSc7
<g/>
)	)	kIx)
známy	znám	k2eAgFnPc1d1
a	a	k8xC
používány	používat	k5eAaImNgFnP
již	již	k6eAd1
od	od	k7c2
dávnověku	dávnověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgInPc4d3
patří	patřit	k5eAaImIp3nS
křemičitan	křemičitan	k1gInSc4
zirkon	zirkon	k1gInSc1
<g/>
,	,	kIx,
ZrSiO	ZrSiO	k1gFnSc1
<g/>
4	#num#	k4
a	a	k8xC
oxid	oxid	k1gInSc1
zirkonia	zirkonium	k1gNnSc2
baddeleyit	baddeleyit	k1gInSc1
<g/>
,	,	kIx,
ZrO	ZrO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
různé	různý	k2eAgInPc1d1
komplexní	komplexní	k2eAgInPc1d1
zirkonáty	zirkonát	k1gInPc1
jako	jako	k9
zirkelit	zirkelit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInSc1d1
vápník	vápník	k1gInSc1
<g/>
,	,	kIx,
železo	železo	k1gNnSc1
<g/>
,	,	kIx,
titan	titan	k1gInSc1
a	a	k8xC
thorium	thorium	k1gNnSc1
nebo	nebo	k8xC
uhligit	uhligit	k1gInSc1
s	s	k7c7
obsahem	obsah	k1gInSc7
vápníku	vápník	k1gInSc2
<g/>
,	,	kIx,
titanu	titan	k1gInSc2
a	a	k8xC
hliníku	hliník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
těžby	těžba	k1gFnSc2
minerálů	minerál	k1gInPc2
a	a	k8xC
hornin	hornina	k1gFnPc2
s	s	k7c7
výrazným	výrazný	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
zirkonia	zirkonium	k1gNnSc2
patří	patřit	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
jsou	být	k5eAaImIp3nP
k	k	k7c3
získávání	získávání	k1gNnSc3
zirkonia	zirkonium	k1gNnSc2
často	často	k6eAd1
průmyslově	průmyslově	k6eAd1
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
i	i	k8xC
rudy	ruda	k1gFnPc1
titanu	titan	k1gInSc2
jako	jako	k8xC,k8xS
ilmenit	ilmenit	k1gInSc4
a	a	k8xC
rutil	rutil	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
zirkonia	zirkonium	k1gNnSc2
byl	být	k5eAaImAgInS
pomocí	pomocí	k7c2
spektrální	spektrální	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
potvrzen	potvrdit	k5eAaPmNgInS
i	i	k9
ve	v	k7c6
hvězdách	hvězda	k1gFnPc6
podobným	podobný	k2eAgNnSc7d1
našemu	náš	k3xOp1gNnSc3
Slunci	slunce	k1gNnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
složkou	složka	k1gFnSc7
řady	řada	k1gFnSc2
meteoritů	meteorit	k1gInPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
významné	významný	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
v	v	k7c6
měsíčních	měsíční	k2eAgFnPc6d1
horninách	hornina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
čistého	čistý	k2eAgNnSc2d1
zirkonia	zirkonium	k1gNnSc2
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nákladná	nákladný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
případě	případ	k1gInSc6
titanu	titan	k1gInSc2
nelze	lze	k6eNd1
použít	použít	k5eAaPmF
běžné	běžný	k2eAgInPc1d1
metalurgické	metalurgický	k2eAgInPc1d1
postupy	postup	k1gInPc1
jako	jako	k8xS,k8xC
redukce	redukce	k1gFnSc1
uhlím	uhlí	k1gNnSc7
nebo	nebo	k8xC
vodíkem	vodík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
většina	většina	k1gFnSc1
přírodních	přírodní	k2eAgFnPc2d1
surovin	surovina	k1gFnPc2
zirkonia	zirkonium	k1gNnSc2
kontaminována	kontaminovat	k5eAaBmNgNnP
hafniem	hafnium	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vykazuje	vykazovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobné	podobný	k2eAgFnPc4d1
chemické	chemický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
a	a	k8xC
separace	separace	k1gFnPc4
těchto	tento	k3xDgInPc2
příbuzných	příbuzný	k2eAgInPc2d1
prvků	prvek	k1gInPc2
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
obtížná	obtížný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
při	při	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
zirkonia	zirkonium	k1gNnSc2
používá	používat	k5eAaImIp3nS
především	především	k9
tzv.	tzv.	kA
Krollův	Krollův	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
pyrolýzou	pyrolýza	k1gFnSc7
baddeleyitu	baddeleyit	k1gInSc2
s	s	k7c7
uhlíkem	uhlík	k1gInSc7
a	a	k8xC
chlorem	chlor	k1gInSc7
získává	získávat	k5eAaImIp3nS
chlorid	chlorid	k1gInSc1
</s>
<s>
zirkoničitý	zirkoničitý	k2eAgInSc1d1
ZrCl	ZrCl	k1gInSc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZrO	ZrO	k?
<g/>
2	#num#	k4
+	+	kIx~
2	#num#	k4
Cl	Cl	k1gFnSc1
<g/>
2	#num#	k4
+	+	kIx~
2	#num#	k4
C	C	kA
(	(	kIx(
<g/>
900	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
→	→	k?
ZrCl	ZrCl	k1gInSc1
<g/>
4	#num#	k4
+	+	kIx~
2	#num#	k4
CO	co	k8xS
</s>
<s>
Frakční	frakční	k2eAgFnSc7d1
destilací	destilace	k1gFnSc7
se	se	k3xPyFc4
poté	poté	k6eAd1
oddělí	oddělit	k5eAaPmIp3nS
chlorid	chlorid	k1gInSc1
železitý	železitý	k2eAgInSc1d1
FeCl	FeCl	k1gInSc1
<g/>
3	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vzniká	vznikat	k5eAaImIp3nS
z	z	k7c2
příměsí	příměs	k1gFnPc2
železa	železo	k1gNnSc2
<g/>
,	,	kIx,
vyskytujících	vyskytující	k2eAgMnPc2d1
se	se	k3xPyFc4
prakticky	prakticky	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgInPc6
přírodních	přírodní	k2eAgInPc6d1
materiálech	materiál	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
krokem	krok	k1gInSc7
je	být	k5eAaImIp3nS
redukce	redukce	k1gFnSc1
hořčíkem	hořčík	k1gInSc7
v	v	k7c6
inertní	inertní	k2eAgFnSc6d1
argonové	argonový	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
při	při	k7c6
teplotě	teplota	k1gFnSc6
kolem	kolem	k7c2
800	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
ZrCl	ZrCl	k1gInSc1
<g/>
4	#num#	k4
+	+	kIx~
2	#num#	k4
Mg	mg	kA
→	→	k?
Zr	Zr	k1gMnSc1
+	+	kIx~
2	#num#	k4
MgCl	MgCl	k1gInSc1
<g/>
2	#num#	k4
</s>
<s>
Zirkonium	zirkonium	k1gNnSc1
vzniklé	vzniklý	k2eAgFnSc2d1
touto	tento	k3xDgFnSc7
reakcí	reakce	k1gFnSc7
obsahuje	obsahovat	k5eAaImIp3nS
zbytky	zbytek	k1gInPc4
chloridu	chlorid	k1gInSc2
hořečnatého	hořečnatý	k2eAgInSc2d1
a	a	k8xC
kovového	kovový	k2eAgInSc2d1
hořčíku	hořčík	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
odstraňují	odstraňovat	k5eAaImIp3nP
působením	působení	k1gNnSc7
kyseliny	kyselina	k1gFnSc2
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1
HCl	HCl	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
připravené	připravený	k2eAgNnSc1d1
zirkonium	zirkonium	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
stále	stále	k6eAd1
ještě	ještě	k6eAd1
kolem	kolem	k7c2
1	#num#	k4
<g/>
%	%	kIx~
hafnia	hafnium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
není	být	k5eNaImIp3nS
na	na	k7c4
překážku	překážka	k1gFnSc4
běžným	běžný	k2eAgFnPc3d1
aplikacím	aplikace	k1gFnPc3
zirkonia	zirkonium	k1gNnSc2
ve	v	k7c6
slitinách	slitina	k1gFnPc6
a	a	k8xC
při	při	k7c6
povrchové	povrchový	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
využití	využití	k1gNnSc4
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
energetice	energetika	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
třeba	třeba	k6eAd1
toto	tento	k3xDgNnSc4
hafnium	hafnium	k1gNnSc4
oddělit	oddělit	k5eAaPmF
a	a	k8xC
tento	tento	k3xDgInSc1
krok	krok	k1gInSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
10	#num#	k4
<g/>
x	x	k?
cenu	cena	k1gFnSc4
výsledného	výsledný	k2eAgNnSc2d1
<g/>
,	,	kIx,
hafnia	hafnium	k1gNnSc2
prostého	prostý	k2eAgNnSc2d1
<g/>
,	,	kIx,
zirkonia	zirkonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Slitiny	slitina	k1gFnPc1
zirkonia	zirkonium	k1gNnSc2
nacházejí	nacházet	k5eAaImIp3nP
významné	významný	k2eAgNnSc4d1
uplatnění	uplatnění	k1gNnSc4
především	především	k6eAd1
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
energetice	energetika	k1gFnSc6
a	a	k8xC
povrchové	povrchový	k2eAgFnSc3d1
ochraně	ochrana	k1gFnSc3
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
90	#num#	k4
%	%	kIx~
světové	světový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
čistého	čistý	k2eAgNnSc2d1
zirkonia	zirkonium	k1gNnSc2
používáno	používat	k5eAaImNgNnS
při	při	k7c6
výrobě	výroba	k1gFnSc6
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
v	v	k7c6
jaderných	jaderný	k2eAgFnPc6d1
elektrárnách	elektrárna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
zirkonium	zirkonium	k1gNnSc1
velmi	velmi	k6eAd1
málo	málo	k6eAd1
pohlcuje	pohlcovat	k5eAaImIp3nS
neutrony	neutron	k1gInPc1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
slitiny	slitina	k1gFnPc1
jsou	být	k5eAaImIp3nP
současně	současně	k6eAd1
chemicky	chemicky	k6eAd1
i	i	k8xC
mechanicky	mechanicky	k6eAd1
odolné	odolný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitým	důležitý	k2eAgInSc7d1
aspektem	aspekt	k1gInSc7
pro	pro	k7c4
tento	tento	k3xDgInSc4
typ	typ	k1gInSc4
použití	použití	k1gNnSc2
je	být	k5eAaImIp3nS
důkladné	důkladný	k2eAgNnSc4d1
odstranění	odstranění	k1gNnSc4
doprovázejícího	doprovázející	k2eAgNnSc2d1
hafnia	hafnium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vykazuje	vykazovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
600	#num#	k4
<g/>
×	×	k?
větší	veliký	k2eAgInSc1d2
účinný	účinný	k2eAgInSc1d1
průřez	průřez	k1gInSc1
pro	pro	k7c4
tepelné	tepelný	k2eAgInPc4d1
neutrony	neutron	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Komerčně	komerčně	k6eAd1
vyráběné	vyráběný	k2eAgFnPc1d1
slitiny	slitina	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
zařízení	zařízení	k1gNnPc1
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
Zircaloy	Zircalo	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
několik	několik	k4yIc4
typů	typ	k1gInPc2
slitin	slitina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
ve	v	k7c6
všech	všecek	k3xTgInPc6
případech	případ	k1gInPc6
obsahují	obsahovat	k5eAaImIp3nP
přes	přes	k7c4
97	#num#	k4
%	%	kIx~
Zr	Zr	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
do	do	k7c2
nich	on	k3xPp3gFnPc2
legovány	legován	k2eAgInPc4d1
kovy	kov	k1gInPc1
jako	jako	k8xS,k8xC
cín	cín	k1gInSc1
<g/>
,	,	kIx,
nikl	nikl	k1gInSc1
<g/>
,	,	kIx,
chrom	chrom	k1gInSc1
a	a	k8xC
železo	železo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
hafnia	hafnium	k1gNnSc2
by	by	kYmCp3nS
neměl	mít	k5eNaImAgInS
překročit	překročit	k5eAaPmF
0,01	0,01	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
kolem	kolem	k7c2
150	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
trubek	trubka	k1gFnPc2
z	z	k7c2
těchto	tento	k3xDgFnPc2
slitin	slitina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
zirkonium	zirkonium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
živých	živý	k2eAgInPc6d1
organizmech	organizmus	k1gInPc6
chová	chovat	k5eAaImIp3nS
zcela	zcela	k6eAd1
inertně	inertně	k6eAd1
<g/>
,	,	kIx,
slouží	sloužit	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnPc4
slitiny	slitina	k1gFnPc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
implantátů	implantát	k1gInPc2
<g/>
,	,	kIx,
kloubních	kloubní	k2eAgFnPc2d1
náhrad	náhrada	k1gFnPc2
a	a	k8xC
podobných	podobný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgNnSc1d1
uplatnění	uplatnění	k1gNnPc1
nacházejí	nacházet	k5eAaImIp3nP
slitiny	slitina	k1gFnPc1
zirkonia	zirkonium	k1gNnSc2
v	v	k7c6
antikorozní	antikorozní	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
kovů	kov	k1gInPc2
především	především	k9
v	v	k7c6
chemickém	chemický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
navíc	navíc	k6eAd1
slouží	sloužit	k5eAaImIp3nS
i	i	k9
k	k	k7c3
výrobě	výroba	k1gFnSc3
vysoce	vysoce	k6eAd1
tepelně	tepelně	k6eAd1
a	a	k8xC
korozně	korozně	k6eAd1
namáhaných	namáhaný	k2eAgInPc2d1
chemických	chemický	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
<g/>
,	,	kIx,
tepelných	tepelný	k2eAgInPc2d1
výměníků	výměník	k1gInPc2
a	a	k8xC
vakuových	vakuový	k2eAgFnPc2d1
aparatur	aparatura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pyroforických	pyroforický	k2eAgFnPc2d1
(	(	kIx(
<g/>
zápalných	zápalný	k2eAgFnPc2d1
<g/>
)	)	kIx)
vlastností	vlastnost	k1gFnSc7
jemně	jemně	k6eAd1
práškového	práškový	k2eAgNnSc2d1
zirkonia	zirkonium	k1gNnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
při	při	k7c6
výrobě	výroba	k1gFnSc6
zápalných	zápalný	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
pro	pro	k7c4
vojenské	vojenský	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Slitiny	slitina	k1gFnPc1
s	s	k7c7
niobem	niob	k1gInSc7
vykazují	vykazovat	k5eAaImIp3nP
supravodivé	supravodivý	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
při	při	k7c6
relativně	relativně	k6eAd1
vysokých	vysoký	k2eAgFnPc6d1
teplotách	teplota	k1gFnPc6
(	(	kIx(
<g/>
desítky	desítka	k1gFnPc4
kelvinů	kelvin	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
supravodivých	supravodivý	k2eAgInPc2d1
magnetů	magnet	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Vybroušený	vybroušený	k2eAgInSc1d1
syntetický	syntetický	k2eAgInSc1d1
oxid	oxid	k1gInSc1
zirkonia	zirkonium	k1gNnSc2
</s>
<s>
Sloučeninou	sloučenina	k1gFnSc7
zirkonia	zirkonium	k1gNnSc2
s	s	k7c7
největším	veliký	k2eAgInSc7d3
praktickým	praktický	k2eAgInSc7d1
významem	význam	k1gInSc7
je	být	k5eAaImIp3nS
bezesporu	bezesporu	k9
oxid	oxid	k1gInSc4
zirkoničitý	zirkoničitý	k2eAgInSc4d1
ZrO	ZrO	k1gFnSc7
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
zirkoničitý	zirkoničitý	k2eAgInSc1d1
<g/>
,	,	kIx,
ZrO	ZrO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
krystalující	krystalující	k2eAgInSc1d1
v	v	k7c6
krychlové	krychlový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
významným	významný	k2eAgInSc7d1
minerálem	minerál	k1gInSc7
zirkonia	zirkonium	k1gNnSc2
a	a	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
vyrábí	vyrábět	k5eAaImIp3nS
i	i	k9
synteticky	synteticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
velmi	velmi	k6eAd1
významná	významný	k2eAgFnSc1d1
podobnost	podobnost	k1gFnSc1
s	s	k7c7
diamantem	diamant	k1gInSc7
<g/>
,	,	kIx,
především	především	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
jen	jen	k9
o	o	k7c4
málo	málo	k1gNnSc4
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
u	u	k7c2
skutečného	skutečný	k2eAgInSc2d1
diamantu	diamant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
ZrO	ZrO	k1gMnPc1
<g/>
2	#num#	k4
má	mít	k5eAaImIp3nS
tvrdost	tvrdost	k1gFnSc1
8,5	8,5	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgInSc1d3
rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
hustotě	hustota	k1gFnSc6
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
materiálů	materiál	k1gInPc2
–	–	k?
ZrO	ZrO	k1gFnPc4
<g/>
2	#num#	k4
vykazuje	vykazovat	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
5,6	5,6	k4
–	–	k?
6	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
diamant	diamant	k1gInSc1
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
nižší	nízký	k2eAgFnSc4d2
hustotu	hustota	k1gFnSc4
kolem	kolem	k7c2
3,5	3,5	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kubický	kubický	k2eAgInSc1d1
oxid	oxid	k1gInSc1
zirkoničitý	zirkoničitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
proto	proto	k8xC
levnější	levný	k2eAgMnSc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledově	vzhledově	k6eAd1
velmi	velmi	k6eAd1
podobnou	podobný	k2eAgFnSc7d1
náhradou	náhrada	k1gFnSc7
pravého	pravý	k2eAgInSc2d1
diamantu	diamant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
šperkařském	šperkařský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
zpracovávají	zpracovávat	k5eAaImIp3nP
tuny	tuna	k1gFnPc4
tohoto	tento	k3xDgInSc2
materiálu	materiál	k1gInSc2
při	při	k7c6
výrobě	výroba	k1gFnSc6
prstenů	prsten	k1gInPc2
<g/>
,	,	kIx,
náramků	náramek	k1gInPc2
<g/>
,	,	kIx,
náhrdelníků	náhrdelník	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
šperků	šperk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Silná	silný	k2eAgFnSc1d1
podobnost	podobnost	k1gFnSc1
s	s	k7c7
diamantem	diamant	k1gInSc7
pochopitelně	pochopitelně	k6eAd1
svádí	svádět	k5eAaImIp3nS
i	i	k9
jejich	jejich	k3xOp3gFnSc3
úmyslné	úmyslný	k2eAgFnSc3d1
záměně	záměna	k1gFnSc3
<g/>
,	,	kIx,
protože	protože	k8xS
jen	jen	k9
zkušený	zkušený	k2eAgMnSc1d1
klenotník	klenotník	k1gMnSc1
dokáže	dokázat	k5eAaPmIp3nS
rychle	rychle	k6eAd1
a	a	k8xC
spolehlivě	spolehlivě	k6eAd1
rozlišit	rozlišit	k5eAaPmF
tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
materiály	materiál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
však	však	k9
již	již	k6eAd1
na	na	k7c6
trhu	trh	k1gInSc6
spolehlivá	spolehlivý	k2eAgFnSc1d1
a	a	k8xC
cenově	cenově	k6eAd1
dostupná	dostupný	k2eAgNnPc1d1
testovací	testovací	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
spolehlivě	spolehlivě	k6eAd1
odlišit	odlišit	k5eAaPmF
pravý	pravý	k2eAgInSc4d1
diamant	diamant	k1gInSc4
od	od	k7c2
ZrO	ZrO	k1gFnSc2
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Technologicky	technologicky	k6eAd1
velmi	velmi	k6eAd1
významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
keramika	keramika	k1gFnSc1
na	na	k7c6
bázi	báze	k1gFnSc6
ZrO	ZrO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slévárenství	slévárenství	k1gNnSc6
a	a	k8xC
sklářském	sklářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
patří	patřit	k5eAaImIp3nP
tavicí	tavicí	k2eAgFnPc1d1
nádoby	nádoba	k1gFnPc1
a	a	k8xC
vystýlky	vystýlka	k1gFnPc1
tavicích	tavicí	k2eAgFnPc2d1
pecí	pec	k1gFnPc2
vyrobené	vyrobený	k2eAgFnSc2d1
ze	z	k7c2
zirkonoxidové	zirkonoxidový	k2eAgFnSc2d1
keramiky	keramika	k1gFnSc2
k	k	k7c3
nejkvalitnějším	kvalitní	k2eAgMnPc3d3
a	a	k8xC
jejich	jejich	k3xOp3gMnPc3
skutečně	skutečně	k6eAd1
masovému	masový	k2eAgNnSc3d1
nasazení	nasazení	k1gNnSc3
brání	bránit	k5eAaImIp3nS
pouze	pouze	k6eAd1
jejich	jejich	k3xOp3gFnPc7
výrazně	výrazně	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
cena	cena	k1gFnSc1
oproti	oproti	k7c3
materiálům	materiál	k1gInPc3
na	na	k7c6
bázi	báze	k1gFnSc6
grafitu	grafit	k1gInSc2
nebo	nebo	k8xC
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Biologický	biologický	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Díky	díky	k7c3
velmi	velmi	k6eAd1
nízké	nízký	k2eAgFnSc3d1
rozpustnosti	rozpustnost	k1gFnSc3
zirkonia	zirkonium	k1gNnSc2
ve	v	k7c6
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
v	v	k7c6
živých	živý	k2eAgInPc6d1
organizmech	organizmus	k1gInPc6
velmi	velmi	k6eAd1
nízký	nízký	k2eAgMnSc1d1
a	a	k8xC
zirkonium	zirkonium	k1gNnSc1
rozhodně	rozhodně	k6eAd1
nepatří	patřit	k5eNaImIp3nS
mezi	mezi	k7c4
biogenní	biogenní	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc4
nedostatek	nedostatek	k1gInSc1
ve	v	k7c6
stravě	strava	k1gFnSc6
výrazně	výrazně	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
fyziologický	fyziologický	k2eAgInSc4d1
stav	stav	k1gInSc4
organizmu	organizmus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Slitiny	slitina	k1gFnPc1
zirkonia	zirkonium	k1gNnSc2
jsou	být	k5eAaImIp3nP
však	však	k9
ve	v	k7c6
zdravotnictví	zdravotnictví	k1gNnSc6
využívány	využíván	k2eAgInPc1d1
jako	jako	k8xC,k8xS
materiály	materiál	k1gInPc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
různých	různý	k2eAgInPc2d1
tělních	tělní	k2eAgInPc2d1
implantátů	implantát	k1gInPc2
<g/>
,	,	kIx,
kloubních	kloubní	k2eAgFnPc2d1
náhrad	náhrada	k1gFnPc2
a	a	k8xC
podobných	podobný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
zirkonium	zirkonium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
zirkonium	zirkonium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4190943-4	4190943-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5761	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85149899	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85149899	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
