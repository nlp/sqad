<p>
<s>
Amelia	Amelia	k1gFnSc1	Amelia
Mary	Mary	k1gFnSc2	Mary
Earhart	Earharta	k1gFnPc2	Earharta
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1897	[number]	k4	1897
Atchinson	Atchinson	k1gMnSc1	Atchinson
–	–	k?	–
nezvěstná	zvěstný	k2eNgFnSc1d1	nezvěstná
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1937	[number]	k4	1937
<g/>
?	?	kIx.	?
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
letkyně	letkyně	k1gFnSc1	letkyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
přeletěla	přeletět	k5eAaPmAgFnS	přeletět
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letu	let	k1gInSc2	let
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
zmizela	zmizet	k5eAaPmAgFnS	zmizet
nad	nad	k7c7	nad
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Edwina	Edwin	k2eAgFnSc1d1	Edwina
a	a	k8xC	a
Amy	Amy	k1gFnSc1	Amy
Earhartových	Earhartových	k2eAgFnSc1d1	Earhartových
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svých	svůj	k3xOyFgMnPc2	svůj
prarodičů	prarodič	k1gMnPc2	prarodič
v	v	k7c6	v
kansaském	kansaský	k2eAgNnSc6d1	kansaské
městě	město	k1gNnSc6	město
Atchison	Atchisona	k1gFnPc2	Atchisona
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
dědečkem	dědeček	k1gMnSc7	dědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
Alfred	Alfred	k1gMnSc1	Alfred
Otis	Otisa	k1gFnPc2	Otisa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
poprvé	poprvé	k6eAd1	poprvé
seděla	sedět	k5eAaImAgFnS	sedět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
lekce	lekce	k1gFnSc1	lekce
létání	létání	k1gNnSc2	létání
a	a	k8xC	a
koupila	koupit	k5eAaPmAgFnS	koupit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgNnSc4d1	vlastní
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
,	,	kIx,	,
dvouplošník	dvouplošník	k1gInSc1	dvouplošník
Kinner	Kinner	k1gMnSc1	Kinner
Airster	Airster	k1gMnSc1	Airster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
neoficiální	oficiální	k2eNgInSc4d1	neoficiální
rekord	rekord	k1gInSc4	rekord
<g/>
:	:	kIx,	:
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
letěla	letět	k5eAaImAgFnS	letět
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
obdržela	obdržet	k5eAaPmAgFnS	obdržet
jako	jako	k9	jako
šestnáctá	šestnáctý	k4xOgFnSc1	šestnáctý
žena	žena	k1gFnSc1	žena
pilotní	pilotní	k2eAgFnSc4d1	pilotní
licenci	licence	k1gFnSc4	licence
od	od	k7c2	od
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
letecké	letecký	k2eAgFnSc2d1	letecká
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
FAI	FAI	kA	FAI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1928	[number]	k4	1928
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
přeletěla	přeletět	k5eAaPmAgFnS	přeletět
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
Fokker	Fokker	k1gMnSc1	Fokker
F.	F.	kA	F.
<g/>
VIIb	VIIb	k1gMnSc1	VIIb
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
m	m	kA	m
Atlantik	Atlantik	k1gInSc4	Atlantik
(	(	kIx(	(
<g/>
z	z	k7c2	z
Trepassey	Trepassea	k1gFnSc2	Trepassea
Harbor	Harbora	k1gFnPc2	Harbora
v	v	k7c6	v
Newfoundlandu	Newfoundland	k1gInSc6	Newfoundland
do	do	k7c2	do
Burry	Burra	k1gFnSc2	Burra
Port	porta	k1gFnPc2	porta
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
<g/>
)	)	kIx)	)
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
jako	jako	k9	jako
spolucestující	spolucestující	k1gMnSc1	spolucestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
překonala	překonat	k5eAaPmAgFnS	překonat
další	další	k2eAgInSc4d1	další
rekord	rekord	k1gInSc4	rekord
<g/>
:	:	kIx,	:
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
letěla	letět	k5eAaImAgFnS	letět
rychlostí	rychlost	k1gFnSc7	rychlost
asi	asi	k9	asi
290	[number]	k4	290
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1932	[number]	k4	1932
znovu	znovu	k6eAd1	znovu
přeletěla	přeletět	k5eAaPmAgFnS	přeletět
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
letěla	letět	k5eAaImAgFnS	letět
sama	sám	k3xTgMnSc4	sám
v	v	k7c6	v
jednomotorovém	jednomotorový	k2eAgInSc6d1	jednomotorový
letounu	letoun	k1gInSc6	letoun
Lockheed	Lockheed	k1gMnSc1	Lockheed
Vega	Vega	k1gMnSc1	Vega
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
sólový	sólový	k2eAgInSc4d1	sólový
přelet	přelet	k1gInSc4	přelet
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letu	let	k1gInSc6	let
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
56	[number]	k4	56
minut	minuta	k1gFnPc2	minuta
přistála	přistát	k5eAaImAgFnS	přistát
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
a	a	k8xC	a
povětrnostní	povětrnostní	k2eAgInPc4d1	povětrnostní
problémy	problém	k1gInPc4	problém
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
původně	původně	k6eAd1	původně
plánované	plánovaný	k2eAgNnSc4d1	plánované
přistání	přistání	k1gNnSc4	přistání
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
začala	začít	k5eAaPmAgFnS	začít
plánovat	plánovat	k5eAaImF	plánovat
svůj	svůj	k3xOyFgInSc4	svůj
let	let	k1gInSc4	let
okolo	okolo	k7c2	okolo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
podruhé	podruhé	k6eAd1	podruhé
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
s	s	k7c7	s
navigátorem	navigátor	k1gMnSc7	navigátor
Fredem	Fred	k1gMnSc7	Fred
Noonanem	Noonan	k1gMnSc7	Noonan
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
upraveném	upravený	k2eAgInSc6d1	upravený
pro	pro	k7c4	pro
dálkové	dálkový	k2eAgInPc4d1	dálkový
lety	let	k1gInPc4	let
s	s	k7c7	s
doletem	dolet	k1gInSc7	dolet
až	až	k9	až
4000	[number]	k4	4000
km	km	kA	km
Lockheed	Lockheed	k1gInSc4	Lockheed
L-10E	L-10E	k1gFnSc7	L-10E
Electra	Electrum	k1gNnSc2	Electrum
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1937	[number]	k4	1937
z	z	k7c2	z
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zastávkách	zastávka	k1gFnPc6	zastávka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
dorazili	dorazit	k5eAaPmAgMnP	dorazit
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
do	do	k7c2	do
Lae	Lae	k1gFnSc2	Lae
na	na	k7c6	na
Nové	Nové	k2eAgFnSc6d1	Nové
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
35	[number]	k4	35
tisíc	tisíc	k4xCgInSc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
jim	on	k3xPp3gMnPc3	on
ještě	ještě	k9	ještě
posledních	poslední	k2eAgInPc2d1	poslední
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Howlandovu	Howlandův	k2eAgInSc3d1	Howlandův
ostrovu	ostrov	k1gInSc3	ostrov
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
po	po	k7c4	po
1	[number]	k4	1
300	[number]	k4	300
km	km	kA	km
se	se	k3xPyFc4	se
rádiové	rádiový	k2eAgNnSc1d1	rádiové
spojení	spojení	k1gNnSc1	spojení
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
a	a	k8xC	a
letadlo	letadlo	k1gNnSc1	letadlo
"	"	kIx"	"
<g/>
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hledání	hledání	k1gNnSc1	hledání
ostatků	ostatek	k1gInPc2	ostatek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgInP	objevit
fragmenty	fragment	k1gInPc1	fragment
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
artefakty	artefakt	k1gInPc1	artefakt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohly	moct	k5eAaImAgFnP	moct
patřit	patřit	k5eAaImF	patřit
Amelii	Amelie	k1gFnSc4	Amelie
Earhartové	Earhartová	k1gFnSc2	Earhartová
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
navigátorovi	navigátor	k1gMnSc3	navigátor
<g/>
,	,	kIx,	,
na	na	k7c6	na
atolu	atol	k1gInSc6	atol
Nikumaroro	Nikumarora	k1gFnSc5	Nikumarora
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
souostroví	souostroví	k1gNnSc2	souostroví
Kiribati	Kiribati	k1gMnPc2	Kiribati
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
objevů	objev	k1gInPc2	objev
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
trosečníci	trosečník	k1gMnPc1	trosečník
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
až	až	k8xS	až
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c4	na
dehydrataci	dehydratace	k1gFnSc4	dehydratace
(	(	kIx(	(
<g/>
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
není	být	k5eNaImIp3nS	být
zdroj	zdroj	k1gInSc1	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nalezeného	nalezený	k2eAgInSc2d1	nalezený
hliníkového	hliníkový	k2eAgInSc2d1	hliníkový
plátu	plát	k1gInSc2	plát
usuzují	usuzovat	k5eAaImIp3nP	usuzovat
znalci	znalec	k1gMnPc1	znalec
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
letoun	letoun	k1gInSc4	letoun
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
amerických	americký	k2eAgInPc6d1	americký
národních	národní	k2eAgInPc6d1	národní
archivech	archiv	k1gInPc6	archiv
nalezena	nalezen	k2eAgFnSc1d1	nalezena
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Earhartová	Earhartová	k1gFnSc1	Earhartová
zřícení	zřícení	k1gNnSc2	zřícení
i	i	k8xC	i
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
navigátorem	navigátor	k1gInSc7	navigátor
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
na	na	k7c6	na
Marshallových	Marshallův	k2eAgInPc6d1	Marshallův
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
expertů	expert	k1gMnPc2	expert
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
z	z	k7c2	z
FBI	FBI	kA	FBI
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
autentická	autentický	k2eAgFnSc1d1	autentická
<g/>
.	.	kIx.	.
</s>
<s>
Fotka	fotka	k1gFnSc1	fotka
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
nalezena	nalézt	k5eAaBmNgFnS	nalézt
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
časopise	časopis	k1gInSc6	časopis
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
<g/>
Příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
zachycen	zachytit	k5eAaPmNgInS	zachytit
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
Poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
(	(	kIx(	(
<g/>
Amelia	Amelia	k1gFnSc1	Amelia
Earhart	Earhart	k1gInSc1	Earhart
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Flight	Flight	k1gMnSc1	Flight
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Amelia	Amelia	k1gFnSc1	Amelia
Earhartová	Earhartový	k2eAgFnSc1d1	Earhartový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Amelia	Amelia	k1gFnSc1	Amelia
Earhartová	Earhartová	k1gFnSc1	Earhartová
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Amelia	Amelia	k1gFnSc1	Amelia
Earhartová	Earhartová	k1gFnSc1	Earhartová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Amelia	Amelia	k1gFnSc1	Amelia
Earhartová	Earhartový	k2eAgFnSc1d1	Earhartový
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
Amelie	Amelie	k1gFnSc2	Amelie
Earhartové	Earhartový	k2eAgFnSc2d1	Earhartový
v	v	k7c6	v
rodišti	rodiště	k1gNnSc6	rodiště
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Amelia	Amelia	k1gFnSc1	Amelia
Earhartová	Earhartová	k1gFnSc1	Earhartová
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
fotografií	fotografia	k1gFnPc2	fotografia
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Amelia	Amelia	k1gFnSc1	Amelia
Earhart	Earharta	k1gFnPc2	Earharta
Muzeum	muzeum	k1gNnSc1	muzeum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Amelia	Amelia	k1gFnSc1	Amelia
Earhart	Earharta	k1gFnPc2	Earharta
Myths	Mythsa	k1gFnPc2	Mythsa
from	from	k6eAd1	from
the	the	k?	the
Pacific	Pacific	k1gMnSc1	Pacific
War	War	k1gMnSc1	War
</s>
</p>
