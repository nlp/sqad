<s>
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
byl	být	k5eAaImAgInS	být
válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
mezi	mezi	k7c7	mezi
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
podporovanou	podporovaný	k2eAgFnSc7d1	podporovaná
OSN	OSN	kA	OSN
a	a	k8xC	a
Korejskou	korejský	k2eAgFnSc7d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
neformálně	formálně	k6eNd1	formálně
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
