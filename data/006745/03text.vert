<s>
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
byl	být	k5eAaImAgInS	být
válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
mezi	mezi	k7c7	mezi
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
podporovanou	podporovaný	k2eAgFnSc7d1	podporovaná
OSN	OSN	kA	OSN
a	a	k8xC	a
Korejskou	korejský	k2eAgFnSc7d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
neformálně	formálně	k6eNd1	formálně
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
primárně	primárně	k6eAd1	primárně
důsledkem	důsledek	k1gInSc7	důsledek
politického	politický	k2eAgNnSc2d1	politické
rozdělení	rozdělení	k1gNnSc2	rozdělení
Koreje	Korea	k1gFnSc2	Korea
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
vítězných	vítězný	k2eAgMnPc2d1	vítězný
Spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
tichomořské	tichomořský	k2eAgFnSc2d1	tichomořská
části	část	k1gFnSc2	část
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
korejský	korejský	k2eAgInSc1d1	korejský
poloostrov	poloostrov	k1gInSc1	poloostrov
okupován	okupován	k2eAgInSc1d1	okupován
Japonským	japonský	k2eAgNnSc7d1	Japonské
císařstvím	císařství	k1gNnSc7	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Japonska	Japonsko	k1gNnSc2	Japonsko
byl	být	k5eAaImAgInS	být
poloostrov	poloostrov	k1gInSc1	poloostrov
pod	pod	k7c7	pod
sovětsko-americkým	sovětskomerický	k2eAgInSc7d1	sovětsko-americký
dohledem	dohled	k1gInSc7	dohled
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
podle	podle	k7c2	podle
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
okupovanou	okupovaný	k2eAgFnSc4d1	okupovaná
Američany	Američan	k1gMnPc4	Američan
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
okupovanou	okupovaný	k2eAgFnSc4d1	okupovaná
Sověty	Sovět	k1gMnPc7	Sovět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
znemožnění	znemožnění	k1gNnSc2	znemožnění
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
poloostrova	poloostrov	k1gInSc2	poloostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
odcizení	odcizení	k1gNnSc1	odcizení
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
poloostrova	poloostrov	k1gInSc2	poloostrov
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
byl	být	k5eAaImAgInS	být
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
vedený	vedený	k2eAgInSc1d1	vedený
Kimem	Kim	k1gMnSc7	Kim
Ir-senem	Iren	k1gMnSc7	Ir-sen
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
formálně	formálně	k6eAd1	formálně
demokratický	demokratický	k2eAgInSc1d1	demokratický
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc1d1	vedený
Li	li	k8xS	li
Syn-manem	Synan	k1gInSc7	Syn-man
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
vládami	vláda	k1gFnPc7	vláda
se	se	k3xPyFc4	se
stupňovalo	stupňovat	k5eAaImAgNnS	stupňovat
i	i	k9	i
přes	přes	k7c4	přes
pokračující	pokračující	k2eAgInPc4d1	pokračující
rozhovory	rozhovor	k1gInPc4	rozhovor
o	o	k7c4	o
znovusjednocení	znovusjednocení	k1gNnSc4	znovusjednocení
<g/>
,	,	kIx,	,
incidenty	incident	k1gInPc1	incident
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
hranici	hranice	k1gFnSc6	hranice
se	se	k3xPyFc4	se
stupňovaly	stupňovat	k5eAaImAgFnP	stupňovat
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
situace	situace	k1gFnSc1	situace
eskalovala	eskalovat	k5eAaImAgFnS	eskalovat
v	v	k7c4	v
otevřený	otevřený	k2eAgInSc4d1	otevřený
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
když	když	k8xS	když
severokorejské	severokorejský	k2eAgFnPc1d1	severokorejská
jednotky	jednotka	k1gFnPc1	jednotka
vpadly	vpadnout	k5eAaPmAgFnP	vpadnout
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
bojkotoval	bojkotovat	k5eAaImAgInS	bojkotovat
zasedání	zasedání	k1gNnSc4	zasedání
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
zastoupení	zastoupení	k1gNnSc3	zastoupení
Kuomintangu	Kuomintang	k1gInSc2	Kuomintang
-	-	kIx~	-
vlády	vláda	k1gFnSc2	vláda
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
namísto	namísto	k7c2	namísto
Číny	Čína	k1gFnSc2	Čína
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
instituci	instituce	k1gFnSc6	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
prosadit	prosadit	k5eAaPmF	prosadit
rezoluci	rezoluce	k1gFnSc4	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
schvalující	schvalující	k2eAgFnSc4d1	schvalující
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
vyslaly	vyslat	k5eAaPmAgInP	vyslat
na	na	k7c4	na
korejský	korejský	k2eAgInSc4d1	korejský
poloostrov	poloostrov	k1gInSc4	poloostrov
302	[number]	k4	302
tisíce	tisíc	k4xCgInSc2	tisíc
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
jednotkami	jednotka	k1gFnPc7	jednotka
dalších	další	k2eAgInPc2d1	další
dvaceti	dvacet	k4xCc2	dvacet
států	stát	k1gInPc2	stát
pomohli	pomoct	k5eAaPmAgMnP	pomoct
jihokorejským	jihokorejský	k2eAgFnPc3d1	jihokorejská
jednotkám	jednotka	k1gFnPc3	jednotka
invazi	invaze	k1gFnSc4	invaze
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
obránci	obránce	k1gMnPc1	obránce
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
do	do	k7c2	do
Pusanského	Pusanský	k2eAgInSc2d1	Pusanský
perimetru	perimetr	k1gInSc2	perimetr
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Protiofenzíva	protiofenzíva	k1gFnSc1	protiofenzíva
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
zatlačila	zatlačit	k5eAaPmAgFnS	zatlačit
Severokorejce	Severokorejec	k1gMnPc4	Severokorejec
naopak	naopak	k6eAd1	naopak
za	za	k7c4	za
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
až	až	k9	až
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ja-lu	Ja	k1gInSc2	Ja-l
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
reagovala	reagovat	k5eAaBmAgFnS	reagovat
ČLR	ČLR	kA	ČLR
neformálním	formální	k2eNgInSc7d1	neformální
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Severokorejců	Severokorejec	k1gMnPc2	Severokorejec
a	a	k8xC	a
čínské	čínský	k2eAgFnSc2d1	čínská
jednotky	jednotka	k1gFnSc2	jednotka
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
síly	síla	k1gFnPc1	síla
OSN	OSN	kA	OSN
zpět	zpět	k6eAd1	zpět
za	za	k7c4	za
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1951	[number]	k4	1951
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
dynamické	dynamický	k2eAgInPc1d1	dynamický
boje	boj	k1gInPc1	boj
změnily	změnit	k5eAaPmAgInP	změnit
ve	v	k7c4	v
statickou	statický	k2eAgFnSc4d1	statická
zákopovou	zákopový	k2eAgFnSc4d1	zákopová
válku	válka	k1gFnSc4	válka
právě	právě	k6eAd1	právě
kolem	kolem	k7c2	kolem
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
skončily	skončit	k5eAaPmAgInP	skončit
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obnovila	obnovit	k5eAaPmAgFnS	obnovit
hranici	hranice	k1gFnSc4	hranice
kolem	kolem	k7c2	kolem
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnPc4	rovnoběžka
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Korejské	korejský	k2eAgNnSc4d1	korejské
demilitarizované	demilitarizovaný	k2eAgNnSc4d1	demilitarizované
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
před	před	k7c7	před
konfliktem	konflikt	k1gInSc7	konflikt
a	a	k8xC	a
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
války	válka	k1gFnSc2	válka
výrazně	výrazně	k6eAd1	výrazně
materiálně	materiálně	k6eAd1	materiálně
podporoval	podporovat	k5eAaImAgInS	podporovat
armádu	armáda	k1gFnSc4	armáda
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
čínských	čínský	k2eAgMnPc2d1	čínský
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
Pakt	pakt	k1gInSc4	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ujednáním	ujednání	k1gNnSc7	ujednání
uzavřeným	uzavřený	k2eAgMnSc7d1	uzavřený
s	s	k7c7	s
USA	USA	kA	USA
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Japonsku	Japonsko	k1gNnSc3	Japonsko
válku	válek	k1gInSc2	válek
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
kolonií	kolonie	k1gFnPc2	kolonie
japonského	japonský	k2eAgNnSc2d1	Japonské
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
od	od	k7c2	od
severu	sever	k1gInSc2	sever
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
provedla	provést	k5eAaPmAgFnS	provést
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Korejský	korejský	k2eAgInSc4d1	korejský
poloostrov	poloostrov	k1gInSc4	poloostrov
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
japonskou	japonský	k2eAgFnSc7d1	japonská
kapitulací	kapitulace	k1gFnSc7	kapitulace
se	se	k3xPyFc4	se
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c6	na
rozdělení	rozdělení	k1gNnSc6	rozdělení
podle	podle	k7c2	podle
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
s	s	k7c7	s
předpokladem	předpoklad	k1gInSc7	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
japonská	japonský	k2eAgNnPc4d1	Japonské
vojska	vojsko	k1gNnPc4	vojsko
severně	severně	k6eAd1	severně
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
demarkační	demarkační	k2eAgFnSc2d1	demarkační
čáry	čára	k1gFnSc2	čára
se	se	k3xPyFc4	se
vzdají	vzdát	k5eAaPmIp3nP	vzdát
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
kapitulaci	kapitulace	k1gFnSc3	kapitulace
jižních	jižní	k2eAgFnPc2d1	jižní
formací	formace	k1gFnPc2	formace
přijme	přijmout	k5eAaPmIp3nS	přijmout
velení	velení	k1gNnSc1	velení
americké	americký	k2eAgNnSc1d1	americké
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
dočasně	dočasně	k6eAd1	dočasně
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
okupační	okupační	k2eAgFnPc4d1	okupační
zóny	zóna	k1gFnPc4	zóna
-	-	kIx~	-
na	na	k7c4	na
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
s	s	k7c7	s
okupační	okupační	k2eAgFnSc7d1	okupační
správou	správa	k1gFnSc7	správa
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Koreu	Korea	k1gFnSc4	Korea
s	s	k7c7	s
okupační	okupační	k2eAgFnSc7d1	okupační
správou	správa	k1gFnSc7	správa
Armády	armáda	k1gFnSc2	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1945	[number]	k4	1945
okupační	okupační	k2eAgFnPc1d1	okupační
mocnosti	mocnost	k1gFnPc1	mocnost
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
dočasné	dočasný	k2eAgFnSc6d1	dočasná
správě	správa	k1gFnSc6	správa
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
byly	být	k5eAaImAgFnP	být
ustaveny	ustavit	k5eAaPmNgFnP	ustavit
vlády	vláda	k1gFnPc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
poloostrova	poloostrov	k1gInSc2	poloostrov
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
OSN	OSN	kA	OSN
provedly	provést	k5eAaPmAgFnP	provést
volby	volba	k1gFnPc1	volba
ovšem	ovšem	k9	ovšem
bojkotované	bojkotovaný	k2eAgFnPc1d1	bojkotovaná
levicí	levice	k1gFnSc7	levice
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
premiérem	premiér	k1gMnSc7	premiér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Li	li	k9	li
Syn-man	Synan	k1gMnSc1	Syn-man
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
předala	předat	k5eAaPmAgFnS	předat
moc	moc	k6eAd1	moc
přímo	přímo	k6eAd1	přímo
komunistické	komunistický	k2eAgFnSc3d1	komunistická
vládě	vláda	k1gFnSc3	vláda
vedené	vedený	k2eAgFnSc2d1	vedená
Kimem	Kim	k1gMnSc7	Kim
Ir-senem	Iren	k1gMnSc7	Ir-sen
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
antihitlerovské	antihitlerovský	k2eAgFnSc2d1	antihitlerovský
koalice	koalice	k1gFnSc2	koalice
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
Korea	Korea	k1gFnSc1	Korea
sjednotit	sjednotit	k5eAaPmF	sjednotit
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
při	při	k7c6	při
propukávající	propukávající	k2eAgFnSc6d1	propukávající
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
podrobnostech	podrobnost	k1gFnPc6	podrobnost
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Trumana	Truman	k1gMnSc2	Truman
bez	bez	k7c2	bez
podložení	podložení	k1gNnSc2	podložení
jakýmkoliv	jakýkoliv	k3yIgNnSc7	jakýkoliv
referendem	referendum	k1gNnSc7	referendum
nebo	nebo	k8xC	nebo
plebiscitem	plebiscit	k1gInSc7	plebiscit
převzala	převzít	k5eAaPmAgFnS	převzít
zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
za	za	k7c4	za
budoucnost	budoucnost	k1gFnSc4	budoucnost
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
orgánů	orgán	k1gInPc2	orgán
OSN	OSN	kA	OSN
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
základě	základ	k1gInSc6	základ
správy	správa	k1gFnSc2	správa
Armády	armáda	k1gFnSc2	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
s	s	k7c7	s
dohledem	dohled	k1gInSc7	dohled
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
i	i	k9	i
Korejská	korejský	k2eAgFnSc1d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nelegitimním	legitimní	k2eNgInSc7d1	nelegitimní
útvarem	útvar	k1gInSc7	útvar
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Korejcům	Korejec	k1gMnPc3	Korejec
nebylo	být	k5eNaImAgNnS	být
umožněno	umožněn	k2eAgNnSc1d1	umožněno
zvolit	zvolit	k5eAaPmF	zvolit
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Korea	Korea	k1gFnSc1	Korea
stala	stát	k5eAaPmAgFnS	stát
vedle	vedle	k7c2	vedle
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
Berlína	Berlín	k1gInSc2	Berlín
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
soupeření	soupeření	k1gNnSc2	soupeření
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
roli	role	k1gFnSc4	role
sehrály	sehrát	k5eAaPmAgFnP	sehrát
ambice	ambice	k1gFnPc1	ambice
a	a	k8xC	a
iluze	iluze	k1gFnPc4	iluze
jak	jak	k6eAd1	jak
korejských	korejský	k2eAgMnPc2d1	korejský
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
vlády	vláda	k1gFnSc2	vláda
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jihokorejský	jihokorejský	k2eAgMnSc1d1	jihokorejský
prezident	prezident	k1gMnSc1	prezident
Li	li	k8xS	li
Syn-man	Synan	k1gMnSc1	Syn-man
i	i	k8xC	i
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
Strany	strana	k1gFnSc2	strana
práce	práce	k1gFnSc2	práce
Kim	Kim	k1gFnSc2	Kim
Ir-sen	Irna	k1gFnPc2	Ir-sna
neskrývali	skrývat	k5eNaImAgMnP	skrývat
své	svůj	k3xOyFgFnPc4	svůj
záměry	záměra	k1gFnPc4	záměra
sjednocení	sjednocení	k1gNnSc2	sjednocení
poloostrova	poloostrov	k1gInSc2	poloostrov
pod	pod	k7c7	pod
vlastním	vlastní	k2eAgNnSc7d1	vlastní
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Ústavy	ústava	k1gFnPc1	ústava
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
přijaté	přijatý	k2eAgFnSc6d1	přijatá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prohlašovaly	prohlašovat	k5eAaImAgInP	prohlašovat
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
rozšíření	rozšíření	k1gNnSc2	rozšíření
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Příznačné	příznačný	k2eAgNnSc1d1	příznačné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
severokorejská	severokorejský	k2eAgFnSc1d1	severokorejská
ústava	ústava	k1gFnSc1	ústava
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Soul	Soul	k1gInSc1	Soul
a	a	k8xC	a
Pchjong-jang	Pchjongang	k1gInSc1	Pchjong-jang
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
přechodným	přechodný	k2eAgNnSc7d1	přechodné
sídlem	sídlo	k1gNnSc7	sídlo
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
orgánů	orgán	k1gInPc2	orgán
KLDR	KLDR	kA	KLDR
do	do	k7c2	do
"	"	kIx"	"
<g/>
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
"	"	kIx"	"
Soulu	Soul	k1gInSc6	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
jak	jak	k6eAd1	jak
sovětská	sovětský	k2eAgFnSc1d1	sovětská
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
americká	americký	k2eAgNnPc4d1	americké
vojska	vojsko	k1gNnPc4	vojsko
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
korejského	korejský	k2eAgNnSc2d1	korejské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
ČLR	ČLR	kA	ČLR
sledovalo	sledovat	k5eAaImAgNnS	sledovat
s	s	k7c7	s
obavou	obava	k1gFnSc7	obava
zostřující	zostřující	k2eAgFnSc4d1	zostřující
se	s	k7c7	s
vztahy	vztah	k1gInPc7	vztah
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
byl	být	k5eAaImAgInS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
americká	americký	k2eAgFnSc1d1	americká
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
destabilizuje	destabilizovat	k5eAaBmIp3nS	destabilizovat
vztahy	vztah	k1gInPc4	vztah
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
nepříznivě	příznivě	k6eNd1	příznivě
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
jeho	jeho	k3xOp3gInPc4	jeho
plány	plán	k1gInPc4	plán
na	na	k7c4	na
rozbití	rozbití	k1gNnSc4	rozbití
sil	síla	k1gFnPc2	síla
Kuomintangu	Kuomintang	k1gInSc2	Kuomintang
vedeného	vedený	k2eAgInSc2d1	vedený
Čankajškem	Čankajšek	k1gInSc7	Čankajšek
<g/>
,	,	kIx,	,
usazující	usazující	k2eAgFnSc7d1	usazující
se	se	k3xPyFc4	se
na	na	k7c6	na
Tchajvanu	Tchajvan	k1gMnSc6	Tchajvan
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1950	[number]	k4	1950
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
státní	státní	k2eAgMnSc1d1	státní
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Dean	Deana	k1gFnPc2	Deana
G.	G.	kA	G.
Acheson	Acheson	k1gMnSc1	Acheson
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
americký	americký	k2eAgInSc1d1	americký
obranný	obranný	k2eAgInSc1d1	obranný
perimetr	perimetr	k1gInSc1	perimetr
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Aleutské	aleutský	k2eAgInPc4d1	aleutský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
japonské	japonský	k2eAgNnSc4d1	Japonské
souostroví	souostroví	k1gNnSc4	souostroví
Rjúkjú	Rjúkjú	k1gNnPc2	Rjúkjú
a	a	k8xC	a
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Korea	Korea	k1gFnSc1	Korea
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
státních	státní	k2eAgInPc2d1	státní
zájmů	zájem	k1gInPc2	zájem
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
posílila	posílit	k5eAaPmAgFnS	posílit
odhodlání	odhodlání	k1gNnSc4	odhodlání
severokorejského	severokorejský	k2eAgNnSc2d1	severokorejské
vedení	vedení	k1gNnSc2	vedení
rozpoutat	rozpoutat	k5eAaPmF	rozpoutat
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Stalina	Stalin	k1gMnSc4	Stalin
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vojenské	vojenský	k2eAgNnSc4d1	vojenské
zapojení	zapojení	k1gNnSc4	zapojení
USA	USA	kA	USA
do	do	k7c2	do
korejského	korejský	k2eAgInSc2d1	korejský
konfliktu	konflikt	k1gInSc2	konflikt
je	být	k5eAaImIp3nS	být
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnPc2	tvrzení
bývalého	bývalý	k2eAgMnSc2d1	bývalý
náčelníka	náčelník	k1gMnSc2	náčelník
operativy	operativa	k1gFnSc2	operativa
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
Korejské	korejský	k2eAgFnSc2d1	Korejská
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
Ju	ju	k0	ju
Son-čchola	Son-čchola	k1gFnSc1	Son-čchola
příprava	příprava	k1gFnSc1	příprava
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
započala	započnout	k5eAaPmAgFnS	započnout
již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
a	a	k8xC	a
závěrečné	závěrečný	k2eAgNnSc4d1	závěrečné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
padlo	padnout	k5eAaImAgNnS	padnout
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
Kima	Kim	k1gInSc2	Kim
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
a	a	k8xC	a
Stalina	Stalin	k1gMnSc2	Stalin
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Kim	Kim	k1gMnSc1	Kim
Ir-sen	Irna	k1gFnPc2	Ir-sna
začal	začít	k5eAaPmAgMnS	začít
obracet	obracet	k5eAaImF	obracet
na	na	k7c4	na
sovětské	sovětský	k2eAgNnSc4d1	sovětské
vedení	vedení	k1gNnSc4	vedení
s	s	k7c7	s
prosbami	prosba	k1gFnPc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
přepadením	přepadení	k1gNnSc7	přepadení
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
Li	li	k8xS	li
Syn-mana	Synan	k1gMnSc2	Syn-man
nedisponuje	disponovat	k5eNaBmIp3nS	disponovat
podporou	podpora	k1gFnSc7	podpora
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příchod	příchod	k1gInSc1	příchod
severokorejského	severokorejský	k2eAgNnSc2d1	severokorejské
vojska	vojsko	k1gNnSc2	vojsko
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
povstání	povstání	k1gNnSc3	povstání
během	během	k7c2	během
něhož	jenž	k3xRgMnSc2	jenž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
se	s	k7c7	s
severokorejskými	severokorejský	k2eAgFnPc7d1	severokorejská
jednotkami	jednotka	k1gFnPc7	jednotka
sami	sám	k3xTgMnPc1	sám
svrhnou	svrhnout	k5eAaPmIp3nP	svrhnout
tamní	tamní	k2eAgInSc4d1	tamní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
nicméně	nicméně	k8xC	nicméně
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
připravenost	připravenost	k1gFnSc4	připravenost
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
zásahu	zásah	k1gInSc2	zásah
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
rozpoutání	rozpoutání	k1gNnSc2	rozpoutání
totální	totální	k2eAgFnSc2d1	totální
války	válka	k1gFnSc2	válka
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
raději	rád	k6eAd2	rád
těmto	tento	k3xDgFnPc3	tento
žádostem	žádost	k1gFnPc3	žádost
nevyhověl	vyhovět	k5eNaPmAgMnS	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
Stalin	Stalin	k1gMnSc1	Stalin
počítal	počítat	k5eAaImAgMnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
SSSR	SSSR	kA	SSSR
dále	daleko	k6eAd2	daleko
poskytoval	poskytovat	k5eAaImAgMnS	poskytovat
Severní	severní	k2eAgFnSc4d1	severní
Koreji	Korea	k1gFnSc3	Korea
velkou	velký	k2eAgFnSc4d1	velká
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
KLDR	KLDR	kA	KLDR
dále	daleko	k6eAd2	daleko
posilovala	posilovat	k5eAaImAgFnS	posilovat
svou	svůj	k3xOyFgFnSc4	svůj
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
organizovala	organizovat	k5eAaBmAgFnS	organizovat
armádu	armáda	k1gFnSc4	armáda
po	po	k7c6	po
sovětském	sovětský	k2eAgInSc6d1	sovětský
vzoru	vzor	k1gInSc6	vzor
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojenských	vojenský	k2eAgMnPc2d1	vojenský
poradců	poradce	k1gMnPc2	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
hráli	hrát	k5eAaImAgMnP	hrát
také	také	k9	také
etničtí	etnický	k2eAgMnPc1d1	etnický
Korejci	Korejec	k1gMnPc1	Korejec
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
veteráni	veterán	k1gMnPc1	veterán
Lidové	lidový	k2eAgFnSc2d1	lidová
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
armády	armáda	k1gFnSc2	armáda
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Pekingu	Peking	k1gInSc2	Peking
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
severokorejských	severokorejský	k2eAgFnPc6d1	severokorejská
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
převyšovaly	převyšovat	k5eAaImAgFnP	převyšovat
jihokorejské	jihokorejský	k2eAgFnPc1d1	jihokorejská
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
rozhodujících	rozhodující	k2eAgInPc6d1	rozhodující
prvcích	prvek	k1gInPc6	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
nemalém	malý	k2eNgNnSc6d1	nemalé
váhání	váhání	k1gNnSc6	váhání
a	a	k8xC	a
úporných	úporný	k2eAgNnPc6d1	úporné
ujišťováních	ujišťování	k1gNnPc6	ujišťování
od	od	k7c2	od
Kima	Kim	k1gInSc2	Kim
Stalin	Stalin	k1gMnSc1	Stalin
vydal	vydat	k5eAaPmAgMnS	vydat
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
provedením	provedení	k1gNnSc7	provedení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc4	podrobnost
si	se	k3xPyFc3	se
odsouhlasili	odsouhlasit	k5eAaPmAgMnP	odsouhlasit
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
Kima	Kimum	k1gNnSc2	Kimum
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozpracování	rozpracování	k1gNnSc4	rozpracování
plánu	plán	k1gInSc2	plán
vpádu	vpád	k1gInSc2	vpád
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
hlavní	hlavní	k2eAgMnSc1d1	hlavní
vojenský	vojenský	k2eAgMnSc1d1	vojenský
poradce	poradce	k1gMnSc1	poradce
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
generálporučík	generálporučík	k1gMnSc1	generálporučík
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljev	Vasiljev	k1gMnSc1	Vasiljev
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
sovětský	sovětský	k2eAgMnSc1d1	sovětský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
Terentij	Terentij	k1gMnSc4	Terentij
Štykov	Štykov	k1gInSc1	Štykov
oznámil	oznámit	k5eAaPmAgInS	oznámit
Stalinovi	Stalin	k1gMnSc3	Stalin
telegramem	telegram	k1gInSc7	telegram
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
plán	plán	k1gInSc1	plán
útoku	útok	k1gInSc2	útok
dokončen	dokončit	k5eAaPmNgInS	dokončit
a	a	k8xC	a
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
Kimem	Kimem	k1gInSc1	Kimem
Ir-senem	Iren	k1gInSc7	Ir-sen
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozbřeskem	rozbřesk	k1gInSc7	rozbřesk
pod	pod	k7c7	pod
dělostřeleckým	dělostřelecký	k2eAgNnSc7d1	dělostřelecké
krytím	krytí	k1gNnSc7	krytí
překročila	překročit	k5eAaPmAgFnS	překročit
severokorejská	severokorejský	k2eAgFnSc1d1	severokorejská
vojska	vojsko	k1gNnSc2	vojsko
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
jižním	jižní	k2eAgMnSc7d1	jižní
sousedem	soused	k1gMnSc7	soused
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
silou	síla	k1gFnSc7	síla
opětovně	opětovně	k6eAd1	opětovně
sjednotit	sjednotit	k5eAaPmF	sjednotit
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Strany	strana	k1gFnSc2	strana
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgNnSc4d1	pozemní
uskupení	uskupení	k1gNnSc4	uskupení
vojsk	vojsko	k1gNnPc2	vojsko
podle	podle	k7c2	podle
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojenských	vojenský	k2eAgMnPc2d1	vojenský
poradců	poradce	k1gMnPc2	poradce
sestávala	sestávat	k5eAaImAgFnS	sestávat
ze	z	k7c2	z
175	[number]	k4	175
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
se	s	k7c7	s
150	[number]	k4	150
tanky	tank	k1gInPc7	tank
Т	Т	k?	Т
a	a	k8xC	a
172	[number]	k4	172
vojenskými	vojenský	k2eAgNnPc7d1	vojenské
letadly	letadlo	k1gNnPc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
bylo	být	k5eAaImAgNnS	být
pozemní	pozemní	k2eAgNnSc1d1	pozemní
vojsko	vojsko	k1gNnSc1	vojsko
vycvičené	vycvičený	k2eAgFnSc2d1	vycvičená
americkými	americký	k2eAgMnPc7d1	americký
specialisty	specialista	k1gMnPc7	specialista
a	a	k8xC	a
vyzbrojené	vyzbrojený	k2eAgInPc1d1	vyzbrojený
americkými	americký	k2eAgFnPc7d1	americká
zbraněmi	zbraň	k1gFnPc7	zbraň
čítající	čítající	k2eAgFnSc2d1	čítající
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
války	válka	k1gFnSc2	válka
93	[number]	k4	93
tisíc	tisíc	k4xCgInSc4	tisíc
osob	osoba	k1gFnPc2	osoba
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
obrněné	obrněný	k2eAgFnSc2d1	obrněná
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
disponovala	disponovat	k5eAaBmAgFnS	disponovat
sotva	sotva	k9	sotva
tuctem	tucet	k1gInSc7	tucet
lehkých	lehký	k2eAgInPc2d1	lehký
výcvikovo-bojových	výcvikovoojový	k2eAgInPc2d1	výcvikovo-bojový
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
KLDR	KLDR	kA	KLDR
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
zaprodanec	zaprodanec	k1gMnSc1	zaprodanec
<g/>
"	"	kIx"	"
Li	li	k8xS	li
Syn-man	Synan	k1gMnSc1	Syn-man
věrolomně	věrolomně	k6eAd1	věrolomně
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
na	na	k7c6	na
území	území	k1gNnSc6	území
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Tažení	tažení	k1gNnSc1	tažení
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
armády	armáda	k1gFnSc2	armáda
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
vesměs	vesměs	k6eAd1	vesměs
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
po	po	k7c6	po
pouhých	pouhý	k2eAgInPc6d1	pouhý
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
Severokorejci	Severokorejec	k1gMnPc1	Severokorejec
obsadili	obsadit	k5eAaPmAgMnP	obsadit
jihokorejské	jihokorejský	k2eAgNnSc4d1	jihokorejské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Soul	Soul	k1gInSc1	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
směry	směr	k1gInPc1	směr
útoku	útok	k1gInSc2	útok
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
také	také	k9	také
Kesong	Kesonga	k1gFnPc2	Kesonga
<g/>
,	,	kIx,	,
Čchungčchong	Čchungčchonga	k1gFnPc2	Čchungčchonga
<g/>
,	,	kIx,	,
Inčchon	Inčchona	k1gFnPc2	Inčchona
a	a	k8xC	a
Kjonggi	Kjongg	k1gFnSc2	Kjongg
<g/>
.	.	kIx.	.
</s>
<s>
Soulské	soulský	k2eAgNnSc1d1	soulské
letiště	letiště	k1gNnSc1	letiště
Kimpcho	Kimpcha	k1gFnSc5	Kimpcha
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
splněn	splnit	k5eAaPmNgInS	splnit
hlavní	hlavní	k2eAgInSc1d1	hlavní
cíl	cíl	k1gInSc1	cíl
-	-	kIx~	-
blesková	bleskový	k2eAgFnSc1d1	blesková
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
nekonala	konat	k5eNaImAgFnS	konat
a	a	k8xC	a
Li	li	k9	li
Sun-manovi	Sunanův	k2eAgMnPc1d1	Sun-manův
a	a	k8xC	a
významné	významný	k2eAgFnPc1d1	významná
části	část	k1gFnPc1	část
jihokorejské	jihokorejský	k2eAgFnSc2d1	jihokorejská
reprezentace	reprezentace	k1gFnSc2	reprezentace
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
a	a	k8xC	a
opustit	opustit	k5eAaPmF	opustit
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
spoléhalo	spoléhat	k5eAaImAgNnS	spoléhat
severokorejské	severokorejský	k2eAgNnSc1d1	severokorejské
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
nenastalo	nastat	k5eNaPmAgNnS	nastat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
srpna	srpen	k1gInSc2	srpen
bylo	být	k5eAaImAgNnS	být
90	[number]	k4	90
%	%	kIx~	%
území	území	k1gNnSc6	území
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
armádou	armáda	k1gFnSc7	armáda
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
USA	USA	kA	USA
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
země	zem	k1gFnSc2	zem
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
:	:	kIx,	:
doslova	doslova	k6eAd1	doslova
týden	týden	k1gInSc1	týden
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
státní	státní	k2eAgMnSc1d1	státní
tajemník	tajemník	k1gMnSc1	tajemník
Acheson	Acheson	k1gMnSc1	Acheson
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zprávě	zpráva	k1gFnSc6	zpráva
pro	pro	k7c4	pro
Kongres	kongres	k1gInSc4	kongres
USA	USA	kA	USA
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Truman	Truman	k1gMnSc1	Truman
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
začátku	začátek	k1gInSc6	začátek
války	válka	k1gFnSc2	válka
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
domů	dům	k1gInPc2	dům
do	do	k7c2	do
Missouri	Missouri	k1gFnSc2	Missouri
а	а	k?	а
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Acheson	Achesona	k1gFnPc2	Achesona
do	do	k7c2	do
Marylandu	Maryland	k1gInSc2	Maryland
<g/>
.	.	kIx.	.
</s>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
poválečnou	poválečný	k2eAgFnSc4d1	poválečná
demobilizaci	demobilizace	k1gFnSc4	demobilizace
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podstatně	podstatně	k6eAd1	podstatně
oslabila	oslabit	k5eAaPmAgFnS	oslabit
její	její	k3xOp3gFnSc4	její
sílu	síla	k1gFnSc4	síla
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
disponovaly	disponovat	k5eAaBmAgFnP	disponovat
velkým	velký	k2eAgInSc7d1	velký
vojenským	vojenský	k2eAgInSc7d1	vojenský
kontingentem	kontingent	k1gInSc7	kontingent
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Douglase	Douglasa	k1gFnSc6	Douglasa
McArthura	McArthura	k1gFnSc1	McArthura
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
britského	britský	k2eAgNnSc2d1	Britské
Společenství	společenství	k1gNnSc2	společenství
národů	národ	k1gInPc2	národ
neměla	mít	k5eNaImAgFnS	mít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
žádná	žádný	k3yNgFnSc1	žádný
země	země	k1gFnSc1	země
takovou	takový	k3xDgFnSc4	takový
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
Truman	Truman	k1gMnSc1	Truman
přikázal	přikázat	k5eAaPmAgMnS	přikázat
McArthurovi	McArthur	k1gMnSc3	McArthur
zásobit	zásobit	k5eAaPmF	zásobit
výzbrojí	výzbroj	k1gFnSc7	výzbroj
jihokorejskou	jihokorejský	k2eAgFnSc4d1	jihokorejská
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
pod	pod	k7c7	pod
leteckým	letecký	k2eAgNnSc7d1	letecké
krytím	krytí	k1gNnSc7	krytí
evakuovat	evakuovat	k5eAaBmF	evakuovat
občany	občan	k1gMnPc4	občan
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Truman	Truman	k1gMnSc1	Truman
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
rady	rad	k1gInPc4	rad
svých	svůj	k3xOyFgMnPc2	svůj
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
rozpoutat	rozpoutat	k5eAaPmF	rozpoutat
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
KLDR	KLDR	kA	KLDR
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dal	dát	k5eAaPmAgInS	dát
rozkaz	rozkaz	k1gInSc1	rozkaz
7	[number]	k4	7
<g/>
.	.	kIx.	.
flotile	flotila	k1gFnSc6	flotila
zajistit	zajistit	k5eAaPmF	zajistit
obranu	obrana	k1gFnSc4	obrana
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
skončil	skončit	k5eAaPmAgMnS	skončit
s	s	k7c7	s
politikou	politika	k1gFnSc7	politika
nevměšování	nevměšování	k1gNnSc2	nevměšování
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
čínských	čínský	k2eAgMnPc2d1	čínský
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
Čankajškových	Čankajškův	k2eAgFnPc2d1	Čankajškova
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
usazující	usazující	k2eAgFnSc1d1	usazující
se	se	k3xPyFc4	se
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
žádala	žádat	k5eAaImAgFnS	žádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
,	,	kIx,	,
ale	ale	k8xC	ale
vedení	vedení	k1gNnSc1	vedení
USA	USA	kA	USA
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zakročit	zakročit	k5eAaPmF	zakročit
a	a	k8xC	a
komunistického	komunistický	k2eAgMnSc4d1	komunistický
útočníka	útočník	k1gMnSc4	útočník
vypudit	vypudit	k5eAaPmF	vypudit
z	z	k7c2	z
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
díky	díky	k7c3	díky
absenci	absence	k1gFnSc3	absence
zástupce	zástupce	k1gMnSc2	zástupce
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
přitom	přitom	k6eAd1	přitom
jednání	jednání	k1gNnSc4	jednání
rady	rada	k1gFnSc2	rada
bojkotoval	bojkotovat	k5eAaImAgMnS	bojkotovat
kvůli	kvůli	k7c3	kvůli
požadavku	požadavek	k1gInSc3	požadavek
na	na	k7c4	na
nahrazení	nahrazení	k1gNnSc4	nahrazení
reprezentanta	reprezentant	k1gMnSc2	reprezentant
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
zástupcem	zástupce	k1gMnSc7	zástupce
komunistické	komunistický	k2eAgFnSc2d1	komunistická
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1949	[number]	k4	1949
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
apel	apel	k1gInSc4	apel
OSN	OSN	kA	OSN
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koalice	koalice	k1gFnSc1	koalice
států	stát	k1gInPc2	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
proti	proti	k7c3	proti
Severní	severní	k2eAgFnSc3d1	severní
Koreji	Korea	k1gFnSc3	Korea
vést	vést	k5eAaImF	vést
protiútok	protiútok	k1gInSc4	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
velitelem	velitel	k1gMnSc7	velitel
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
generál	generál	k1gMnSc1	generál
MacArthur	MacArthur	k1gMnSc1	MacArthur
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
dorazily	dorazit	k5eAaPmAgFnP	dorazit
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
vojsky	vojsko	k1gNnPc7	vojsko
USA	USA	kA	USA
(	(	kIx(	(
<g/>
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
)	)	kIx)	)
střetla	střetnout	k5eAaPmAgFnS	střetnout
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
u	u	k7c2	u
Osanu	Osan	k1gInSc2	Osan
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
museli	muset	k5eAaImAgMnP	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
vojska	vojsko	k1gNnSc2	vojsko
OSN	OSN	kA	OSN
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
zahájila	zahájit	k5eAaPmAgFnS	zahájit
nekoordinovaný	koordinovaný	k2eNgInSc4d1	nekoordinovaný
ústup	ústup	k1gInSc4	ústup
k	k	k7c3	k
jihokorejskému	jihokorejský	k2eAgInSc3d1	jihokorejský
přístavu	přístav	k1gInSc2	přístav
Pusan	Pusana	k1gFnPc2	Pusana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
znovu	znovu	k6eAd1	znovu
zformovat	zformovat	k5eAaPmF	zformovat
pro	pro	k7c4	pro
následující	následující	k2eAgInSc4d1	následující
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Waltona	Walton	k1gMnSc2	Walton
Walkera	Walker	k1gMnSc2	Walker
<g/>
,	,	kIx,	,
velitele	velitel	k1gMnSc2	velitel
pozemních	pozemní	k2eAgFnPc2d1	pozemní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
obranný	obranný	k2eAgInSc1d1	obranný
perimetr	perimetr	k1gInSc1	perimetr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měřil	měřit	k5eAaImAgInS	měřit
od	od	k7c2	od
Pusanu	Pusan	k1gInSc2	Pusan
na	na	k7c4	na
sever	sever	k1gInSc4	sever
přes	přes	k7c4	přes
150	[number]	k4	150
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejská	severokorejský	k2eAgFnSc1d1	severokorejská
armáda	armáda	k1gFnSc1	armáda
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
tento	tento	k3xDgInSc4	tento
perimetr	perimetr	k1gInSc4	perimetr
dobýt	dobýt	k5eAaPmF	dobýt
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
koaliční	koaliční	k2eAgFnPc1d1	koaliční
jednotky	jednotka	k1gFnPc1	jednotka
nestačily	stačit	k5eNaBmAgFnP	stačit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bránily	bránit	k5eAaImAgFnP	bránit
celou	celý	k2eAgFnSc4d1	celá
linii	linie	k1gFnSc4	linie
perimetru	perimetr	k1gInSc2	perimetr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
zpravodajské	zpravodajský	k2eAgFnSc3d1	zpravodajská
službě	služba	k1gFnSc3	služba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
včas	včas	k6eAd1	včas
upozorňovala	upozorňovat	k5eAaImAgFnS	upozorňovat
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
hlavních	hlavní	k2eAgInPc2d1	hlavní
útoků	útok	k1gInPc2	útok
severokorejských	severokorejský	k2eAgFnPc2d1	severokorejská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
situaci	situace	k1gFnSc4	situace
zvládly	zvládnout	k5eAaPmAgInP	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
perimetru	perimetr	k1gInSc2	perimetr
postupně	postupně	k6eAd1	postupně
přicházely	přicházet	k5eAaImAgFnP	přicházet
nové	nový	k2eAgFnPc1d1	nová
posily	posila	k1gFnPc1	posila
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prvních	první	k4xOgMnPc2	první
britských	britský	k2eAgMnPc2d1	britský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejské	severokorejský	k2eAgFnPc1d1	severokorejská
zásobovací	zásobovací	k2eAgFnPc1d1	zásobovací
linie	linie	k1gFnPc1	linie
byly	být	k5eAaImAgFnP	být
navíc	navíc	k6eAd1	navíc
napadány	napadán	k2eAgFnPc1d1	napadána
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
útok	útok	k1gInSc1	útok
začínal	začínat	k5eAaImAgInS	začínat
hroutit	hroutit	k5eAaImF	hroutit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
již	již	k9	již
mělo	mít	k5eAaImAgNnS	mít
koaliční	koaliční	k2eAgNnSc4d1	koaliční
vedení	vedení	k1gNnSc4	vedení
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
perimetr	perimetr	k1gInSc1	perimetr
bude	být	k5eAaImBp3nS	být
uhájen	uhájit	k5eAaPmNgInS	uhájit
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
tak	tak	k6eAd1	tak
začít	začít	k5eAaPmF	začít
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
na	na	k7c4	na
ofenzivu	ofenziva	k1gFnSc4	ofenziva
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Douglase	Douglas	k1gInSc5	Douglas
MacArthura	MacArthura	k1gFnSc1	MacArthura
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
vylodění	vylodění	k1gNnSc2	vylodění
u	u	k7c2	u
přímořského	přímořský	k2eAgNnSc2d1	přímořské
města	město	k1gNnSc2	město
Inčchon	Inčchona	k1gFnPc2	Inčchona
<g/>
,	,	kIx,	,
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
přes	přes	k7c4	přes
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Pusanského	Pusanský	k2eAgInSc2d1	Pusanský
perimetru	perimetr	k1gInSc2	perimetr
<g/>
.	.	kIx.	.
</s>
<s>
Operaci	operace	k1gFnSc4	operace
provedl	provést	k5eAaPmAgMnS	provést
10	[number]	k4	10
<g/>
.	.	kIx.	.
sbor	sbor	k1gInSc1	sbor
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Edwarda	Edward	k1gMnSc2	Edward
Almonda	Almond	k1gMnSc2	Almond
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
riskantní	riskantní	k2eAgFnSc4d1	riskantní
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
koaliční	koaliční	k2eAgFnPc1d1	koaliční
jednotky	jednotka	k1gFnPc1	jednotka
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
menších	malý	k2eAgFnPc2d2	menší
ztrát	ztráta	k1gFnPc2	ztráta
zvládly	zvládnout	k5eAaPmAgFnP	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
k	k	k7c3	k
Soulu	Soul	k1gInSc3	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postupu	postup	k1gInSc6	postup
přerušily	přerušit	k5eAaPmAgFnP	přerušit
severokorejské	severokorejský	k2eAgFnPc1d1	severokorejská
zásobovací	zásobovací	k2eAgFnPc1d1	zásobovací
a	a	k8xC	a
komunikační	komunikační	k2eAgFnPc1d1	komunikační
trasy	trasa	k1gFnPc1	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
prorazily	prorazit	k5eAaPmAgFnP	prorazit
jednotky	jednotka	k1gFnPc1	jednotka
z	z	k7c2	z
Pusanského	Pusanský	k2eAgInSc2d1	Pusanský
perimetru	perimetr	k1gInSc2	perimetr
nepřátelskými	přátelský	k2eNgFnPc7d1	nepřátelská
liniemi	linie	k1gFnPc7	linie
a	a	k8xC	a
mířily	mířit	k5eAaImAgInP	mířit
taktéž	taktéž	k?	taktéž
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
dobývání	dobývání	k1gNnSc1	dobývání
Soulu	Soul	k1gInSc2	Soul
10	[number]	k4	10
<g/>
.	.	kIx.	.
sborem	sborem	k6eAd1	sborem
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
koaliční	koaliční	k2eAgNnPc4d1	koaliční
vojska	vojsko	k1gNnPc4	vojsko
měla	mít	k5eAaImAgFnS	mít
před	před	k7c7	před
nepřátelskými	přátelský	k2eNgFnPc7d1	nepřátelská
jednotkami	jednotka	k1gFnPc7	jednotka
značnou	značný	k2eAgFnSc4d1	značná
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
Severokorejci	Severokorejec	k1gMnPc1	Severokorejec
fanaticky	fanaticky	k6eAd1	fanaticky
bojovali	bojovat	k5eAaImAgMnP	bojovat
až	až	k9	až
do	do	k7c2	do
totálního	totální	k2eAgNnSc2d1	totální
zničení	zničení	k1gNnSc2	zničení
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
si	se	k3xPyFc3	se
sem	sem	k6eAd1	sem
konečně	konečně	k6eAd1	konečně
probojovala	probojovat	k5eAaPmAgFnS	probojovat
cestu	cesta	k1gFnSc4	cesta
i	i	k9	i
vojska	vojsko	k1gNnPc1	vojsko
z	z	k7c2	z
Pusanského	Pusanský	k2eAgInSc2d1	Pusanský
perimetru	perimetr	k1gInSc2	perimetr
<g/>
.	.	kIx.	.
</s>
<s>
Soul	Soul	k1gInSc1	Soul
padl	padnout	k5eAaImAgMnS	padnout
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
října	říjen	k1gInSc2	říjen
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
zbývající	zbývající	k2eAgFnPc1d1	zbývající
Severokorejské	severokorejský	k2eAgFnPc1d1	severokorejská
jednotky	jednotka	k1gFnPc1	jednotka
vytlačeny	vytlačit	k5eAaPmNgFnP	vytlačit
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
žádala	žádat	k5eAaImAgFnS	žádat
OSN	OSN	kA	OSN
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
o	o	k7c4	o
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
kapitulaci	kapitulace	k1gFnSc4	kapitulace
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnPc1	Kim
Ir-sen	Irna	k1gFnPc2	Ir-sna
ji	on	k3xPp3gFnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
MacArthur	MacArthur	k1gMnSc1	MacArthur
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
dostal	dostat	k5eAaPmAgInS	dostat
(	(	kIx(	(
<g/>
po	po	k7c6	po
výhradách	výhrada	k1gFnPc6	výhrada
socialistických	socialistický	k2eAgFnPc2d1	socialistická
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
od	od	k7c2	od
OSN	OSN	kA	OSN
povolení	povolení	k1gNnPc4	povolení
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnSc2	vojsko
OSN	OSN	kA	OSN
začala	začít	k5eAaPmAgFnS	začít
postupovat	postupovat	k5eAaImF	postupovat
nepřátelským	přátelský	k2eNgNnSc7d1	nepřátelské
územím	území	k1gNnSc7	území
na	na	k7c4	na
sever	sever	k1gInSc4	sever
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
s	s	k7c7	s
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ČLR	ČLR	kA	ČLR
pohrozila	pohrozit	k5eAaPmAgFnS	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
postup	postup	k1gInSc1	postup
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
přiměl	přimět	k5eAaPmAgMnS	přimět
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
obavu	obava	k1gFnSc4	obava
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
nařídil	nařídit	k5eAaPmAgMnS	nařídit
MacArthurovi	MacArthur	k1gMnSc3	MacArthur
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
vojska	vojsko	k1gNnSc2	vojsko
raději	rád	k6eAd2	rád
nepřibližovala	přibližovat	k5eNaImAgFnS	přibližovat
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Amnokkang	Amnokkanga	k1gFnPc2	Amnokkanga
(	(	kIx(	(
<g/>
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
Ja-Lu	Ja-Lus	k1gInSc2	Ja-Lus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
právě	právě	k9	právě
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
KLDR	KLDR	kA	KLDR
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vylodění	vylodění	k1gNnSc3	vylodění
ve	v	k7c6	v
Wonsanu	Wonsan	k1gInSc6	Wonsan
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
jednotky	jednotka	k1gFnSc2	jednotka
OSN	OSN	kA	OSN
obsadily	obsadit	k5eAaPmAgFnP	obsadit
Pchjongjang	Pchjongjang	k1gInSc4	Pchjongjang
(	(	kIx(	(
<g/>
metropoli	metropole	k1gFnSc4	metropole
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Kim	Kim	k1gFnSc6	Kim
Ir-Senovi	Ir-Senův	k2eAgMnPc1d1	Ir-Senův
jen	jen	k6eAd1	jen
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
se	se	k3xPyFc4	se
zmocňoval	zmocňovat	k5eAaImAgInS	zmocňovat
optimismus	optimismus	k1gInSc1	optimismus
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
severokorejských	severokorejský	k2eAgFnPc2d1	severokorejská
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
Čína	Čína	k1gFnSc1	Čína
vnímala	vnímat	k5eAaImAgFnS	vnímat
postup	postup	k1gInSc4	postup
jednotek	jednotka	k1gFnPc2	jednotka
OSN	OSN	kA	OSN
na	na	k7c4	na
sever	sever	k1gInSc4	sever
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
hranicím	hranice	k1gFnPc3	hranice
jako	jako	k8xC	jako
vážné	vážný	k2eAgNnSc4d1	vážné
ohrožení	ohrožení	k1gNnSc4	ohrožení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
sem	sem	k6eAd1	sem
bylo	být	k5eAaImAgNnS	být
dopraveno	dopravit	k5eAaPmNgNnS	dopravit
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
000	[number]	k4	000
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
koaliční	koaliční	k2eAgFnPc1d1	koaliční
jednotky	jednotka	k1gFnPc1	jednotka
dostaly	dostat	k5eAaPmAgFnP	dostat
k	k	k7c3	k
hraniční	hraniční	k2eAgFnSc3d1	hraniční
řece	řeka	k1gFnSc3	řeka
Ja-lu	Ja	k1gInSc2	Ja-l
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
den	den	k1gInSc1	den
začaly	začít	k5eAaPmAgInP	začít
přicházet	přicházet	k5eAaImF	přicházet
první	první	k4xOgFnPc1	první
nejasné	jasný	k2eNgFnPc1d1	nejasná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
potyčkách	potyčka	k1gFnPc6	potyčka
s	s	k7c7	s
čínskými	čínský	k2eAgMnPc7d1	čínský
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
s	s	k7c7	s
čínskými	čínský	k2eAgInPc7d1	čínský
útoky	útok	k1gInPc7	útok
vůbec	vůbec	k9	vůbec
nepočítali	počítat	k5eNaImAgMnP	počítat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
řadách	řada	k1gFnPc6	řada
zavládl	zavládnout	k5eAaPmAgInS	zavládnout
zmatek	zmatek	k1gInSc1	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
OSN	OSN	kA	OSN
podala	podat	k5eAaPmAgFnS	podat
Čína	Čína	k1gFnSc1	Čína
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
čínské	čínský	k2eAgFnSc2d1	čínská
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
prý	prý	k9	prý
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
dobrovolné	dobrovolný	k2eAgMnPc4d1	dobrovolný
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgMnPc4	jenž
Čína	Čína	k1gFnSc1	Čína
nemá	mít	k5eNaImIp3nS	mít
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
osvobozeneckém	osvobozenecký	k2eAgInSc6d1	osvobozenecký
boji	boj	k1gInSc6	boj
jim	on	k3xPp3gMnPc3	on
bránit	bránit	k5eAaImF	bránit
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
klamné	klamný	k2eAgNnSc1d1	klamné
gesto	gesto	k1gNnSc1	gesto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vojáky	voják	k1gMnPc4	voják
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
vojáky	voják	k1gMnPc4	voják
SSSR	SSSR	kA	SSSR
ve	v	k7c6	v
funkcích	funkce	k1gFnPc6	funkce
poradců	poradce	k1gMnPc2	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
MacArthur	MacArthur	k1gMnSc1	MacArthur
nakonec	nakonec	k6eAd1	nakonec
vydal	vydat	k5eAaPmAgMnS	vydat
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
řeku	řeka	k1gFnSc4	řeka
Ja-lu	Ja	k1gInSc2	Ja-l
<g/>
.	.	kIx.	.
</s>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
zasadí	zasadit	k5eAaPmIp3nP	zasadit
ČLA	ČLA	kA	ČLA
konečný	konečný	k2eAgInSc4d1	konečný
úder	úder	k1gInSc4	úder
a	a	k8xC	a
ukončí	ukončit	k5eAaPmIp3nS	ukončit
tím	ten	k3xDgNnSc7	ten
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
na	na	k7c4	na
jednotky	jednotka	k1gFnPc4	jednotka
OSN	OSN	kA	OSN
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
celá	celý	k2eAgFnSc1d1	celá
početná	početný	k2eAgFnSc1d1	početná
čínská	čínský	k2eAgFnSc1d1	čínská
armáda	armáda	k1gFnSc1	armáda
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
je	být	k5eAaImIp3nS	být
zdecimovala	zdecimovat	k5eAaPmAgFnS	zdecimovat
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vydal	vydat	k5eAaPmAgMnS	vydat
MacArthur	MacArthur	k1gMnSc1	MacArthur
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
ale	ale	k9	ale
jednotky	jednotka	k1gFnPc1	jednotka
OSN	OSN	kA	OSN
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
obklíčení	obklíčení	k1gNnSc2	obklíčení
a	a	k8xC	a
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
těžkých	těžký	k2eAgFnPc2d1	těžká
ztrát	ztráta	k1gFnPc2	ztráta
si	se	k3xPyFc3	se
musely	muset	k5eAaImAgFnP	muset
probojovat	probojovat	k5eAaPmF	probojovat
cestu	cesta	k1gFnSc4	cesta
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgFnPc1d1	stejná
ztráty	ztráta	k1gFnPc1	ztráta
jako	jako	k8xS	jako
nepřátelé	nepřítel	k1gMnPc1	nepřítel
jim	on	k3xPp3gInPc3	on
způsobil	způsobit	k5eAaPmAgInS	způsobit
i	i	k8xC	i
mráz	mráz	k1gInSc1	mráz
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Námořní	námořní	k2eAgFnSc1d1	námořní
pěchota	pěchota	k1gFnSc1	pěchota
si	se	k3xPyFc3	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
přístavu	přístav	k1gInSc3	přístav
Chungnam	Chungnam	k1gInSc4	Chungnam
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
evakuována	evakuovat	k5eAaBmNgFnS	evakuovat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
nelítostných	lítostný	k2eNgInPc6d1	nelítostný
bojích	boj	k1gInPc6	boj
uprostřed	uprostřed	k7c2	uprostřed
zimy	zima	k1gFnSc2	zima
jednotky	jednotka	k1gFnSc2	jednotka
OSN	OSN	kA	OSN
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
pod	pod	k7c4	pod
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
MacArthur	MacArthur	k1gMnSc1	MacArthur
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
smířit	smířit	k5eAaPmF	smířit
s	s	k7c7	s
blížící	blížící	k2eAgFnSc7d1	blížící
se	se	k3xPyFc4	se
porážkou	porážka	k1gFnSc7	porážka
a	a	k8xC	a
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
použití	použití	k1gNnSc4	použití
atomových	atomový	k2eAgFnPc2d1	atomová
zbraní	zbraň	k1gFnPc2	zbraň
proti	proti	k7c3	proti
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohutně	mohutně	k6eAd1	mohutně
bombardovat	bombardovat	k5eAaImF	bombardovat
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Všechny	všechen	k3xTgInPc1	všechen
jeho	jeho	k3xOp3gInPc1	jeho
návrhy	návrh	k1gInPc1	návrh
však	však	k9	však
byly	být	k5eAaImAgInP	být
zamítnuty	zamítnut	k2eAgInPc1d1	zamítnut
a	a	k8xC	a
značně	značně	k6eAd1	značně
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
odvolání	odvolání	k1gNnSc3	odvolání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
naplánovala	naplánovat	k5eAaBmAgFnS	naplánovat
ČLR	ČLR	kA	ČLR
obnovit	obnovit	k5eAaPmF	obnovit
ofenzivu	ofenziva	k1gFnSc4	ofenziva
a	a	k8xC	a
dobýt	dobýt	k5eAaPmF	dobýt
celý	celý	k2eAgInSc4d1	celý
Korejský	korejský	k2eAgInSc4d1	korejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
poslány	poslán	k2eAgFnPc4d1	poslána
další	další	k2eAgFnPc4d1	další
početné	početný	k2eAgFnPc4d1	početná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
silvestra	silvestr	k1gInSc2	silvestr
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
podnikly	podniknout	k5eAaPmAgFnP	podniknout
jednotky	jednotka	k1gFnPc1	jednotka
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
ČLR	ČLR	kA	ČLR
–	–	k?	–
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
téměř	téměř	k6eAd1	téměř
miliónu	milión	k4xCgInSc2	milión
mužů	muž	k1gMnPc2	muž
–	–	k?	–
druhý	druhý	k4xOgInSc1	druhý
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Jižní	jižní	k2eAgFnSc4d1	jižní
Koreu	Korea	k1gFnSc4	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
zahynul	zahynout	k5eAaPmAgMnS	zahynout
velitel	velitel	k1gMnSc1	velitel
koaličních	koaliční	k2eAgFnPc2d1	koaliční
pozemních	pozemní	k2eAgFnPc2d1	pozemní
vojsk	vojsko	k1gNnPc2	vojsko
Walton	Walton	k1gInSc4	Walton
Walker	Walker	k1gMnSc1	Walker
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Matthew	Matthew	k1gMnSc1	Matthew
Ridgway	Ridgwaa	k1gFnSc2	Ridgwaa
<g/>
.	.	kIx.	.
</s>
<s>
Čínským	čínský	k2eAgFnPc3d1	čínská
jednotkám	jednotka	k1gFnPc3	jednotka
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
proniknout	proniknout	k5eAaPmF	proniknout
hluboko	hluboko	k6eAd1	hluboko
za	za	k7c4	za
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
dobýt	dobýt	k5eAaPmF	dobýt
Soul	Soul	k1gInSc1	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vojska	vojsko	k1gNnPc4	vojsko
USA	USA	kA	USA
začala	začít	k5eAaPmAgFnS	začít
stávat	stávat	k5eAaImF	stávat
kritickou	kritický	k2eAgFnSc4d1	kritická
<g/>
.	.	kIx.	.
</s>
<s>
Hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
stažení	stažení	k1gNnSc6	stažení
koaličních	koaliční	k2eAgNnPc2d1	koaliční
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Číně	Čína	k1gFnSc3	Čína
a	a	k8xC	a
KLDR	KLDR	kA	KLDR
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
příměří	příměří	k1gNnSc1	příměří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
odmítnuto	odmítnut	k2eAgNnSc1d1	odmítnuto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1951	[number]	k4	1951
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
znovu	znovu	k6eAd1	znovu
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
nyní	nyní	k6eAd1	nyní
významně	významně	k6eAd1	významně
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
generál	generál	k1gMnSc1	generál
Ridgway	Ridgwaa	k1gFnSc2	Ridgwaa
<g/>
.	.	kIx.	.
</s>
<s>
Zřídil	zřídit	k5eAaPmAgInS	zřídit
linii	linie	k1gFnSc4	linie
vojsk	vojsko	k1gNnPc2	vojsko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
a	a	k8xC	a
za	za	k7c2	za
leteckého	letecký	k2eAgNnSc2d1	letecké
a	a	k8xC	a
dělostřeleckého	dělostřelecký	k2eAgNnSc2d1	dělostřelecké
bombardování	bombardování	k1gNnSc2	bombardování
začal	začít	k5eAaPmAgInS	začít
pomalu	pomalu	k6eAd1	pomalu
postupovat	postupovat	k5eAaImF	postupovat
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
taktika	taktika	k1gFnSc1	taktika
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Ridgwayův	Ridgwayův	k2eAgInSc4d1	Ridgwayův
mlýnek	mlýnek	k1gInSc4	mlýnek
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
početným	početný	k2eAgMnPc3d1	početný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slabě	slabě	k6eAd1	slabě
vyzbrojeným	vyzbrojený	k2eAgFnPc3d1	vyzbrojená
čínským	čínský	k2eAgFnPc3d1	čínská
jednotkám	jednotka	k1gFnPc3	jednotka
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
taktika	taktika	k1gFnSc1	taktika
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byl	být	k5eAaImAgMnS	být
Američany	Američan	k1gMnPc4	Američan
znovu	znovu	k6eAd1	znovu
dobyt	dobyt	k2eAgInSc1d1	dobyt
Soul	Soul	k1gInSc1	Soul
a	a	k8xC	a
za	za	k7c4	za
týden	týden	k1gInSc4	týden
byla	být	k5eAaImAgFnS	být
čínská	čínský	k2eAgFnSc1d1	čínská
vojska	vojsko	k1gNnSc2	vojsko
zatlačena	zatlačit	k5eAaPmNgFnS	zatlačit
za	za	k7c2	za
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
MacArthur	MacArthur	k1gMnSc1	MacArthur
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
pokračování	pokračování	k1gNnSc3	pokračování
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
vytlačení	vytlačení	k1gNnSc1	vytlačení
komunistů	komunista	k1gMnPc2	komunista
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
války	válka	k1gFnSc2	válka
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
kritiky	kritika	k1gFnSc2	kritika
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Harryho	Harry	k1gMnSc2	Harry
Trumana	Truman	k1gMnSc2	Truman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
a	a	k8xC	a
nabádal	nabádat	k5eAaBmAgMnS	nabádat
ke	k	k7c3	k
zdrženlivosti	zdrženlivost	k1gFnSc3	zdrženlivost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Vedením	vedení	k1gNnSc7	vedení
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
právě	právě	k9	právě
Ridgway	Ridgwaa	k1gMnSc2	Ridgwaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
Číňané	Číňan	k1gMnPc1	Číňan
pokusili	pokusit	k5eAaPmAgMnP	pokusit
jarní	jarní	k2eAgFnSc7d1	jarní
ofenzivou	ofenziva	k1gFnSc7	ofenziva
prorazit	prorazit	k5eAaPmF	prorazit
americkou	americký	k2eAgFnSc4d1	americká
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
zákopovou	zákopový	k2eAgFnSc4d1	zákopová
a	a	k8xC	a
strnula	strnout	k5eAaPmAgFnS	strnout
v	v	k7c6	v
patovém	patový	k2eAgInSc6d1	patový
stavu	stav	k1gInSc6	stav
na	na	k7c4	na
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
delším	dlouhý	k2eAgNnSc6d2	delší
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
Kesonu	keson	k1gInSc6	keson
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
výměně	výměna	k1gFnSc6	výměna
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
vytvoření	vytvoření	k1gNnSc3	vytvoření
repatriační	repatriační	k2eAgFnSc2d1	repatriační
komise	komise	k1gFnSc2	komise
složené	složený	k2eAgFnSc2d1	složená
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
delegovaných	delegovaný	k2eAgMnPc2d1	delegovaný
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
Československem	Československo	k1gNnSc7	Československo
<g/>
,	,	kIx,	,
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
a	a	k8xC	a
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1951	[number]	k4	1951
byla	být	k5eAaImAgNnP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
jednání	jednání	k1gNnPc1	jednání
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
bojovalo	bojovat	k5eAaImAgNnS	bojovat
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
ztrátami	ztráta	k1gFnPc7	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fronta	fronta	k1gFnSc1	fronta
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
neposunula	posunout	k5eNaPmAgFnS	posunout
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírových	mírový	k2eAgNnPc6d1	Mírové
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
odrážela	odrážet	k5eAaImAgFnS	odrážet
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
jednání	jednání	k1gNnPc1	jednání
ocitala	ocitat	k5eAaImAgNnP	ocitat
na	na	k7c6	na
mrtvých	mrtvý	k2eAgInPc6d1	mrtvý
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
obviňovaly	obviňovat	k5eAaImAgFnP	obviňovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
Indočínská	indočínský	k2eAgFnSc1d1	indočínská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
Harryho	Harry	k1gMnSc2	Harry
Trumana	Truman	k1gMnSc2	Truman
na	na	k7c6	na
postě	post	k1gInSc6	post
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc6d1	spojený
státu	stát	k1gInSc2	stát
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Dwight	Dwight	k2eAgMnSc1d1	Dwight
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
sovětský	sovětský	k2eAgMnSc1d1	sovětský
diktátor	diktátor	k1gMnSc1	diktátor
J.	J.	kA	J.
V.	V.	kA	V.
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byla	být	k5eAaImAgFnS	být
konečně	konečně	k6eAd1	konečně
stanovena	stanovit	k5eAaPmNgFnS	stanovit
demarkační	demarkační	k2eAgFnSc1d1	demarkační
linie	linie	k1gFnSc1	linie
pár	pár	k4xCyI	pár
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
a	a	k8xC	a
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
demilitarizované	demilitarizovaný	k2eAgNnSc1d1	demilitarizované
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
v	v	k7c4	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
dopoledne	dopoledne	k1gNnSc4	dopoledne
podepsala	podepsat	k5eAaPmAgFnS	podepsat
OSN	OSN	kA	OSN
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
"	"	kIx"	"
<g/>
Dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nicméně	nicméně	k8xC	nicméně
nabývala	nabývat	k5eAaImAgFnS	nabývat
platnosti	platnost	k1gFnSc3	platnost
až	až	k9	až
v	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgFnPc2d1	poslední
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
tedy	tedy	k8xC	tedy
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
využily	využít	k5eAaPmAgFnP	využít
k	k	k7c3	k
maximálnímu	maximální	k2eAgInSc3d1	maximální
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nepatrně	nepatrně	k6eAd1	nepatrně
změnilo	změnit	k5eAaPmAgNnS	změnit
zabrané	zabraný	k2eAgNnSc1d1	zabrané
území	území	k1gNnSc1	území
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
bylo	být	k5eAaImAgNnS	být
konečně	konečně	k6eAd1	konečně
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
klidu	klid	k1gInSc3	klid
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
provedla	provést	k5eAaPmAgFnS	provést
jihokorejská	jihokorejský	k2eAgFnSc1d1	jihokorejská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
policie	policie	k1gFnSc1	policie
hromadné	hromadný	k2eAgFnSc2d1	hromadná
popravy	poprava	k1gFnSc2	poprava
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
podezřívala	podezřívat	k5eAaImAgFnS	podezřívat
ze	z	k7c2	z
sympatií	sympatie	k1gFnPc2	sympatie
ke	k	k7c3	k
komunistům	komunista	k1gMnPc3	komunista
<g/>
.	.	kIx.	.
3000	[number]	k4	3000
<g/>
–	–	k?	–
<g/>
7000	[number]	k4	7000
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
u	u	k7c2	u
města	město	k1gNnSc2	město
Tedžon	Tedžona	k1gFnPc2	Tedžona
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
1800	[number]	k4	1800
nedaleko	nedaleko	k7c2	nedaleko
Soulu	Soul	k1gInSc2	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
bylo	být	k5eAaImAgNnS	být
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
popraveno	popraven	k2eAgNnSc1d1	popraveno
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
pravicové	pravicový	k2eAgFnPc1d1	pravicová
paramilitární	paramilitární	k2eAgFnPc1d1	paramilitární
jednotky	jednotka	k1gFnPc1	jednotka
popravily	popravit	k5eAaPmAgFnP	popravit
tisíce	tisíc	k4xCgInPc1	tisíc
levicových	levicový	k2eAgMnPc2d1	levicový
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
rolníků	rolník	k1gMnPc2	rolník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
o	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
zločinech	zločin	k1gInPc6	zločin
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neintervenovali	intervenovat	k5eNaImAgMnP	intervenovat
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
MacArthur	MacArthur	k1gMnSc1	MacArthur
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
omezit	omezit	k5eAaPmF	omezit
masové	masový	k2eAgNnSc4d1	masové
zabíjení	zabíjení	k1gNnSc4	zabíjení
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
informaci	informace	k1gFnSc4	informace
do	do	k7c2	do
Pentagonu	Pentagon	k1gInSc2	Pentagon
a	a	k8xC	a
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
konzulát	konzulát	k1gInSc4	konzulát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Pentagon	Pentagon	k1gInSc1	Pentagon
informace	informace	k1gFnSc2	informace
zatajil	zatajit	k5eAaPmAgInS	zatajit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
John	John	k1gMnSc1	John
J.	J.	kA	J.
Muccio	Muccio	k1gMnSc1	Muccio
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nutil	nutit	k5eAaImAgInS	nutit
jihokorejské	jihokorejský	k2eAgInPc4d1	jihokorejský
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
popravy	poprava	k1gFnPc4	poprava
probíhaly	probíhat	k5eAaImAgFnP	probíhat
humánně	humánně	k6eAd1	humánně
a	a	k8xC	a
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Diplomat	diplomat	k1gInSc1	diplomat
za	za	k7c4	za
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
Henderson	Hendersona	k1gFnPc2	Hendersona
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
k	k	k7c3	k
záznamům	záznam	k1gInPc3	záznam
vypovídajících	vypovídající	k2eAgFnPc2d1	vypovídající
o	o	k7c6	o
hromadných	hromadný	k2eAgInPc6d1	hromadný
masakrech	masakr	k1gInPc6	masakr
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
100	[number]	k4	100
000	[number]	k4	000
proseverních	proseverní	k2eAgMnPc2d1	proseverní
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
vyslané	vyslaný	k2eAgFnPc1d1	vyslaná
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
z	z	k7c2	z
USA	USA	kA	USA
používaly	používat	k5eAaImAgFnP	používat
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
taktiku	taktika	k1gFnSc4	taktika
"	"	kIx"	"
<g/>
Nejdřív	dříve	k6eAd3	dříve
střílej	střílet	k5eAaImRp2nS	střílet
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ptej	ptat	k5eAaImRp2nS	ptat
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
každému	každý	k3xTgMnSc3	každý
civilnímu	civilní	k2eAgMnSc3d1	civilní
uprchlíkovi	uprchlík	k1gMnSc3	uprchlík
blížícímu	blížící	k2eAgMnSc3d1	blížící
se	se	k3xPyFc4	se
k	k	k7c3	k
americkým	americký	k2eAgFnPc3d1	americká
bojovým	bojový	k2eAgFnPc3d1	bojová
pozicím	pozice	k1gFnPc3	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
taktika	taktika	k1gFnSc1	taktika
<g/>
"	"	kIx"	"
vedla	vést	k5eAaImAgFnS	vést
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1950	[number]	k4	1950
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
No	no	k9	no
Gun	Gun	k1gMnSc1	Gun
Ri	Ri	k1gMnSc1	Ri
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
americké	americký	k2eAgFnPc1d1	americká
síly	síla	k1gFnPc1	síla
povraždily	povraždit	k5eAaPmAgFnP	povraždit
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
civilistů	civilista	k1gMnPc2	civilista
v	v	k7c6	v
podezření	podezření	k1gNnSc6	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
nepřátelští	přátelský	k2eNgMnPc1d1	nepřátelský
partyzáni	partyzán	k1gMnPc1	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
,	,	kIx,	,
zraněno	zranit	k5eAaPmNgNnS	zranit
nebo	nebo	k8xC	nebo
pohřešováno	pohřešovat	k5eAaImNgNnS	pohřešovat
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
Korejců	Korejec	k1gMnPc2	Korejec
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
přes	přes	k7c4	přes
33	[number]	k4	33
000	[number]	k4	000
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
3	[number]	k4	3
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
vojsk	vojsko	k1gNnPc2	vojsko
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zabito	zabit	k2eAgNnSc1d1	zabito
nebo	nebo	k8xC	nebo
zraněno	zraněn	k2eAgNnSc1d1	zraněno
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
kolem	kolem	k7c2	kolem
900	[number]	k4	900
000	[number]	k4	000
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
války	válka	k1gFnSc2	válka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
dělící	dělící	k2eAgFnSc1d1	dělící
čára	čára	k1gFnSc1	čára
mezi	mezi	k7c7	mezi
Korejemi	Korea	k1gFnPc7	Korea
skoro	skoro	k6eAd1	skoro
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
–	–	k?	–
trochu	trochu	k6eAd1	trochu
severněji	severně	k6eAd2	severně
od	od	k7c2	od
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
nepopularitu	nepopularita	k1gFnSc4	nepopularita
prezidenta	prezident	k1gMnSc2	prezident
Trumana	Truman	k1gMnSc2	Truman
u	u	k7c2	u
americké	americký	k2eAgFnSc2d1	americká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
již	již	k6eAd1	již
po	po	k7c6	po
prohraných	prohraný	k2eAgFnPc6d1	prohraná
primárkách	primárky	k1gFnPc6	primárky
v	v	k7c6	v
New	New	k1gFnSc6	New
Hampshiru	Hampshir	k1gInSc2	Hampshir
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
na	na	k7c4	na
Korejskou	korejský	k2eAgFnSc4d1	Korejská
válku	válka	k1gFnSc4	válka
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
zapomněla	zapomnět	k5eAaImAgFnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ji	on	k3xPp3gFnSc4	on
připomněl	připomnět	k5eAaPmAgMnS	připomnět
komediální	komediální	k2eAgMnSc1d1	komediální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkem	celkem	k6eAd1	celkem
realistický	realistický	k2eAgInSc1d1	realistický
seriál	seriál	k1gInSc1	seriál
M	M	kA	M
<g/>
*	*	kIx~	*
<g/>
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
S	s	k7c7	s
<g/>
*	*	kIx~	*
<g/>
H.	H.	kA	H.
Vojenské	vojenský	k2eAgFnPc4d1	vojenská
akce	akce	k1gFnPc4	akce
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
jen	jen	k9	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
a	a	k8xC	a
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
nebyla	být	k5eNaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
formálně	formálně	k6eAd1	formálně
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
stále	stále	k6eAd1	stále
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Korejemi	Korea	k1gFnPc7	Korea
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
demilitarizované	demilitarizovaný	k2eAgNnSc1d1	demilitarizované
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
ploty	plot	k1gInPc4	plot
z	z	k7c2	z
ostnatého	ostnatý	k2eAgInSc2d1	ostnatý
drátu	drát	k1gInSc2	drát
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
drží	držet	k5eAaImIp3nP	držet
neustálou	neustálý	k2eAgFnSc4d1	neustálá
hlídku	hlídka	k1gFnSc4	hlídka
vojska	vojsko	k1gNnSc2	vojsko
Severní	severní	k2eAgFnSc2d1	severní
i	i	k8xC	i
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
demilitarizované	demilitarizovaný	k2eAgNnSc1d1	demilitarizované
pásmo	pásmo	k1gNnSc1	pásmo
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
nejhlídanější	hlídaný	k2eAgFnPc1d3	nejhlídanější
hranice	hranice	k1gFnPc1	hranice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
stále	stále	k6eAd1	stále
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
menším	malý	k2eAgInPc3d2	menší
ozbrojeným	ozbrojený	k2eAgInPc3d1	ozbrojený
incidentům	incident	k1gInPc3	incident
-	-	kIx~	-
zejména	zejména	k9	zejména
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
definuje	definovat	k5eAaBmIp3nS	definovat
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Korejemi	Korea	k1gFnPc7	Korea
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
dosti	dosti	k6eAd1	dosti
nejasně	jasně	k6eNd1	jasně
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
příměřím	příměří	k1gNnSc7	příměří
již	již	k6eAd1	již
necítí	cítit	k5eNaImIp3nS	cítit
vázána	vázat	k5eAaImNgFnS	vázat
a	a	k8xC	a
neručí	ručit	k5eNaImIp3nS	ručit
za	za	k7c4	za
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
cizích	cizí	k2eAgFnPc2d1	cizí
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreou	Korea	k1gFnSc7	Korea
nárokované	nárokovaný	k2eAgFnSc2d1	nárokovaná
části	část	k1gFnSc2	část
Žlutého	žlutý	k2eAgNnSc2d1	žluté
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pozornost	pozornost	k1gFnSc4	pozornost
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
potopení	potopení	k1gNnSc4	potopení
jihokorejské	jihokorejský	k2eAgFnSc2d1	jihokorejská
korvety	korveta	k1gFnSc2	korveta
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
závěru	závěr	k1gInSc2	závěr
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
severokorejským	severokorejský	k2eAgNnSc7d1	severokorejské
torpédem	torpédo	k1gNnSc7	torpédo
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
potopení	potopení	k1gNnSc4	potopení
lodi	loď	k1gFnSc2	loď
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
napětí	napětí	k1gNnSc2	napětí
je	být	k5eAaImIp3nS	být
severokorejský	severokorejský	k2eAgInSc4d1	severokorejský
vývoj	vývoj	k1gInSc4	vývoj
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
neútočení	neútočení	k1gNnSc6	neútočení
byly	být	k5eAaImAgFnP	být
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
jednostranně	jednostranně	k6eAd1	jednostranně
vypovězeny	vypovědět	k5eAaPmNgFnP	vypovědět
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
uvalila	uvalit	k5eAaPmAgFnS	uvalit
na	na	k7c4	na
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
další	další	k2eAgFnSc2d1	další
sankce	sankce	k1gFnSc2	sankce
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
třetímu	třetí	k4xOgMnSc3	třetí
jadernému	jaderný	k2eAgInSc3d1	jaderný
testu	test	k1gInSc3	test
<g/>
.	.	kIx.	.
</s>
