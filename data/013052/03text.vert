<p>
<s>
Stegosaurus	Stegosaurus	k1gInSc1	Stegosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zastřešený	zastřešený	k2eAgMnSc1d1	zastřešený
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
ptakopánvého	ptakopánvý	k2eAgMnSc2d1	ptakopánvý
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
období	období	k1gNnSc6	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
jury	jura	k1gFnSc2	jura
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
150	[number]	k4	150
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pojmenování	pojmenování	k1gNnSc1	pojmenování
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
střechovitý	střechovitý	k2eAgMnSc1d1	střechovitý
či	či	k8xC	či
zastřešený	zastřešený	k2eAgMnSc1d1	zastřešený
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
dostal	dostat	k5eAaPmAgInS	dostat
podle	podle	k7c2	podle
plochých	plochý	k2eAgFnPc2d1	plochá
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
menší	malý	k2eAgMnSc1d2	menší
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rody	rod	k1gInPc7	rod
Tyrannosaurus	Tyrannosaurus	k1gInSc1	Tyrannosaurus
<g/>
,	,	kIx,	,
Triceratops	Triceratops	k1gInSc1	Triceratops
a	a	k8xC	a
Apatosaurus	Apatosaurus	k1gInSc1	Apatosaurus
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
vůbec	vůbec	k9	vůbec
nejznámější	známý	k2eAgInPc1d3	nejznámější
rody	rod	k1gInPc1	rod
neptačích	ptačí	k2eNgMnPc2d1	neptačí
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměr	k1gInPc4	rozměr
==	==	k?	==
</s>
</p>
<p>
<s>
Stegosaurus	Stegosaurus	k1gMnSc1	Stegosaurus
byl	být	k5eAaImAgMnS	být
největším	veliký	k2eAgMnSc7d3	veliký
zástupcem	zástupce	k1gMnSc7	zástupce
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
délky	délka	k1gFnSc2	délka
až	až	k9	až
kolem	kolem	k7c2	kolem
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
vážit	vážit	k5eAaImF	vážit
asi	asi	k9	asi
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
velké	velký	k2eAgInPc1d1	velký
exempláře	exemplář	k1gInPc1	exemplář
druhu	druh	k1gInSc2	druh
S.	S.	kA	S.
stenops	stenops	k6eAd1	stenops
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
délky	délka	k1gFnPc4	délka
asi	asi	k9	asi
6,5	[number]	k4	6,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
3500	[number]	k4	3500
kg	kg	kA	kg
<g/>
,	,	kIx,	,
u	u	k7c2	u
druhu	druh	k1gInSc2	druh
S.	S.	kA	S.
ungulatus	ungulatus	k1gInSc1	ungulatus
pak	pak	k6eAd1	pak
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
3800	[number]	k4	3800
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
malou	malý	k2eAgFnSc4d1	malá
lebku	lebka	k1gFnSc4	lebka
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
mozek	mozek	k1gInSc1	mozek
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
velikosti	velikost	k1gFnSc3	velikost
mandarinky	mandarinka	k1gFnSc2	mandarinka
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
vlašského	vlašský	k2eAgInSc2d1	vlašský
ořechu	ořech	k1gInSc2	ořech
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
často	často	k6eAd1	často
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
stopy	stopa	k1gFnPc1	stopa
<g/>
,	,	kIx,	,
objevené	objevený	k2eAgInPc1d1	objevený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Asturii	Asturie	k1gFnSc6	Asturie
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohli	moct	k5eAaImAgMnP	moct
existovat	existovat	k5eAaImF	existovat
ještě	ještě	k9	ještě
větší	veliký	k2eAgMnPc1d2	veliký
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
býložravec	býložravec	k1gMnSc1	býložravec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
zuby	zub	k1gInPc1	zub
byly	být	k5eAaImAgInP	být
malé	malý	k2eAgInPc1d1	malý
<g/>
,	,	kIx,	,
tupé	tupé	k1gNnSc1	tupé
<g/>
,	,	kIx,	,
listovitého	listovitý	k2eAgInSc2d1	listovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ocasu	ocas	k1gInSc2	ocas
měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
trny	trn	k1gInPc4	trn
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnPc1d1	zadní
nohy	noha	k1gFnPc1	noha
byly	být	k5eAaImAgFnP	být
dvakrát	dvakrát	k6eAd1	dvakrát
delší	dlouhý	k2eAgFnPc1d2	delší
než	než	k8xS	než
přední	přední	k2eAgMnSc1d1	přední
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
fosilie	fosilie	k1gFnPc1	fosilie
stegosaurů	stegosaur	k1gMnPc2	stegosaur
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
již	již	k6eAd1	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
rod	rod	k1gInSc1	rod
Stegosaurus	Stegosaurus	k1gInSc1	Stegosaurus
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
popsán	popsán	k2eAgInSc1d1	popsán
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
Othnielem	Othniel	k1gMnSc7	Othniel
Charlesem	Charles	k1gMnSc7	Charles
Marshem	Marsh	k1gInSc7	Marsh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
rozeznávány	rozeznávat	k5eAaImNgInP	rozeznávat
tři	tři	k4xCgInPc1	tři
platné	platný	k2eAgInPc1d1	platný
druhy	druh	k1gInPc1	druh
stegosaura	stegosaur	k1gMnSc2	stegosaur
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
objeveným	objevený	k2eAgInSc7d1	objevený
druhem	druh	k1gInSc7	druh
byl	být	k5eAaImAgMnS	být
Stegosaurus	Stegosaurus	k1gMnSc1	Stegosaurus
armatus	armatus	k1gMnSc1	armatus
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
prozkoumaným	prozkoumaný	k2eAgInSc7d1	prozkoumaný
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
Stegosaurus	Stegosaurus	k1gInSc1	Stegosaurus
stenops	stenopsa	k1gFnPc2	stenopsa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
objevené	objevený	k2eAgFnSc2d1	objevená
kostry	kostra	k1gFnSc2	kostra
známe	znát	k5eAaImIp1nP	znát
druh	druh	k1gInSc4	druh
Stegosaurus	Stegosaurus	k1gInSc4	Stegosaurus
longispinus	longispinus	k1gInSc4	longispinus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
vyčleněn	vyčleněn	k2eAgInSc1d1	vyčleněn
nový	nový	k2eAgInSc1d1	nový
rod	rod	k1gInSc1	rod
Alcovasaurus	Alcovasaurus	k1gInSc1	Alcovasaurus
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
údajném	údajný	k2eAgNnSc6d1	údajné
druhém	druhý	k4xOgNnSc6	druhý
mozkovém	mozkový	k2eAgNnSc6d1	mozkové
centru	centrum	k1gNnSc6	centrum
stegosaurů	stegosaur	k1gInPc2	stegosaur
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
pánevní	pánevní	k2eAgFnSc6d1	pánevní
oblasti	oblast	k1gFnSc6	oblast
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
vyvrácen	vyvrácen	k2eAgInSc1d1	vyvrácen
<g/>
,	,	kIx,	,
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
soustředění	soustředění	k1gNnSc2	soustředění
tukové	tukový	k2eAgFnSc2d1	tuková
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
nervových	nervový	k2eAgInPc2d1	nervový
provazců	provazec	k1gInPc2	provazec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
ovládání	ovládání	k1gNnSc3	ovládání
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Paleobiologie	paleobiologie	k1gFnSc2	paleobiologie
==	==	k?	==
</s>
</p>
<p>
<s>
Stegosauři	Stegosaur	k1gMnPc1	Stegosaur
byli	být	k5eAaImAgMnP	být
poměrně	poměrně	k6eAd1	poměrně
pomalí	pomalý	k2eAgMnPc1d1	pomalý
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
rychlost	rychlost	k1gFnSc1	rychlost
chůze	chůze	k1gFnSc2	chůze
podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
stop	stopa	k1gFnPc2	stopa
nepřesahovala	přesahovat	k5eNaImAgFnS	přesahovat
asi	asi	k9	asi
6-7	[number]	k4	6-7
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Největší	veliký	k2eAgFnSc1d3	veliký
dosud	dosud	k6eAd1	dosud
nalezená	nalezený	k2eAgFnSc1d1	nalezená
stopa	stopa	k1gFnSc1	stopa
stegosaurida	stegosaurida	k1gFnSc1	stegosaurida
<g/>
,	,	kIx,	,
objevená	objevený	k2eAgFnSc1d1	objevená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
měřila	měřit	k5eAaImAgFnS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
55	[number]	k4	55
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
pláty	plát	k1gInPc1	plát
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
sloužily	sloužit	k5eAaImAgFnP	sloužit
buď	buď	k8xC	buď
k	k	k7c3	k
termoregulaci	termoregulace	k1gFnSc3	termoregulace
nebo	nebo	k8xC	nebo
k	k	k7c3	k
vnitrodruhové	vnitrodruhový	k2eAgFnSc3d1	vnitrodruhová
signalizaci	signalizace	k1gFnSc3	signalizace
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
před	před	k7c7	před
teropodními	teropodní	k2eAgMnPc7d1	teropodní
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Obranu	obrana	k1gFnSc4	obrana
před	před	k7c7	před
dravými	dravý	k2eAgInPc7d1	dravý
teropody	teropod	k1gInPc7	teropod
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
až	až	k9	až
90	[number]	k4	90
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
ocasní	ocasní	k2eAgInPc4d1	ocasní
trny	trn	k1gInPc4	trn
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnPc4	sloužící
k	k	k7c3	k
aktivní	aktivní	k2eAgFnSc3d1	aktivní
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgFnSc3d1	efektivní
obraně	obrana	k1gFnSc3	obrana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
účelu	účel	k1gInSc3	účel
měli	mít	k5eAaImAgMnP	mít
stegosauři	stegosaur	k1gMnPc1	stegosaur
také	také	k9	také
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
svaly	sval	k1gInPc1	sval
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgMnSc1d1	umožňující
jim	on	k3xPp3gMnPc3	on
provádět	provádět	k5eAaImF	provádět
rychlé	rychlý	k2eAgInPc4d1	rychlý
obraty	obrat	k1gInPc4	obrat
ocasem	ocas	k1gInSc7	ocas
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
útočníkovi	útočník	k1gMnSc3	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
stegosaurů	stegosaur	k1gInPc2	stegosaur
byla	být	k5eAaImAgNnP	být
nejspíš	nejspíš	k9	nejspíš
podstatně	podstatně	k6eAd1	podstatně
lehčeji	lehko	k6eAd2	lehko
stavěná	stavěný	k2eAgFnSc1d1	stavěná
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
i	i	k8xC	i
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
zachovaným	zachovaný	k2eAgMnSc7d1	zachovaný
jedincem	jedinec	k1gMnSc7	jedinec
stegosaura	stegosaur	k1gMnSc2	stegosaur
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
mladý	mladý	k2eAgMnSc1d1	mladý
dospělec	dospělec	k1gMnSc1	dospělec
z	z	k7c2	z
Wyomingu	Wyoming	k1gInSc2	Wyoming
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
z	z	k7c2	z
85	[number]	k4	85
<g/>
%	%	kIx~	%
kompletní	kompletní	k2eAgFnSc4d1	kompletní
kostru	kostra	k1gFnSc4	kostra
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
Britské	britský	k2eAgNnSc1d1	Britské
přírodovědecké	přírodovědecký	k2eAgNnSc1d1	Přírodovědecké
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
pod	pod	k7c7	pod
populární	populární	k2eAgFnSc7d1	populární
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
Sophie	Sophie	k1gFnSc1	Sophie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Populární	populární	k2eAgFnSc1d1	populární
kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Stegosaurus	Stegosaurus	k1gInSc1	Stegosaurus
patří	patřit	k5eAaImIp3nS	patřit
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
první	první	k4xOgFnSc1	první
věrohodná	věrohodný	k2eAgFnSc1d1	věrohodná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
kostry	kostra	k1gFnSc2	kostra
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
a	a	k8xC	a
nejoblíbenější	oblíbený	k2eAgMnPc4d3	nejoblíbenější
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
ve	v	k7c6	v
slavném	slavný	k2eAgInSc6d1	slavný
románu	román	k1gInSc6	román
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
A.	A.	kA	A.
C.	C.	kA	C.
Doyla	Doyla	k1gMnSc1	Doyla
Ztracený	ztracený	k2eAgInSc1d1	ztracený
svět	svět	k1gInSc1	svět
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
byl	být	k5eAaImAgMnS	být
Stegosaurus	Stegosaurus	k1gMnSc1	Stegosaurus
zvolen	zvolit	k5eAaPmNgMnS	zvolit
státním	státní	k2eAgMnSc7d1	státní
dinosaurem	dinosaurus	k1gMnSc7	dinosaurus
Colorada	Colorado	k1gNnSc2	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
Stegosaurus	Stegosaurus	k1gMnSc1	Stegosaurus
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
filmů	film	k1gInPc2	film
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
Ztracený	ztracený	k2eAgInSc1d1	ztracený
svět	svět	k1gInSc1	svět
<g/>
:	:	kIx,	:
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
a	a	k8xC	a
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
3	[number]	k4	3
nebo	nebo	k8xC	nebo
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Putování	putování	k1gNnSc2	putování
s	s	k7c7	s
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
a	a	k8xC	a
Planeta	planeta	k1gFnSc1	planeta
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dacentrurus	Dacentrurus	k1gMnSc1	Dacentrurus
</s>
</p>
<p>
<s>
Dravidosaurus	Dravidosaurus	k1gMnSc1	Dravidosaurus
</s>
</p>
<p>
<s>
Kentrosaurus	Kentrosaurus	k1gMnSc1	Kentrosaurus
</s>
</p>
<p>
<s>
Lexovisaurus	Lexovisaurus	k1gMnSc1	Lexovisaurus
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stegosaurus	Stegosaurus	k1gInSc1	Stegosaurus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Stegosaurus	Stegosaurus	k1gInSc4	Stegosaurus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
fosilních	fosilní	k2eAgFnPc6d1	fosilní
stopách	stopa	k1gFnPc6	stopa
mladých	mladý	k2eAgMnPc2d1	mladý
stegosaurů	stegosaur	k1gMnPc2	stegosaur
na	na	k7c6	na
webu	web	k1gInSc6	web
Osel	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOCHA	Socha	k1gMnSc1	Socha
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Zastřešený	zastřešený	k2eAgMnSc1d1	zastřešený
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
.	.	kIx.	.
</s>
<s>
OSEL	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
