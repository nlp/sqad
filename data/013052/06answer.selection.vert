<s>
Stegosaurus	Stegosaurus	k1gInSc1	Stegosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zastřešený	zastřešený	k2eAgMnSc1d1	zastřešený
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
ptakopánvého	ptakopánvý	k2eAgMnSc2d1	ptakopánvý
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
období	období	k1gNnSc6	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
jury	jura	k1gFnSc2	jura
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
150	[number]	k4	150
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
