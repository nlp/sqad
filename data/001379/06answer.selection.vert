<s>
Letec	letec	k1gMnSc1	letec
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Aviator	Aviator	k1gMnSc1	Aviator
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
životopisný	životopisný	k2eAgInSc1d1	životopisný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
režiséra	režisér	k1gMnSc2	režisér
Martina	Martin	k1gMnSc2	Martin
Scorsese	Scorsese	k1gFnSc2	Scorsese
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
Howarda	Howard	k1gMnSc2	Howard
Hughese	Hughese	k1gFnSc2	Hughese
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
leteckého	letecký	k2eAgMnSc2d1	letecký
průkopníka	průkopník	k1gMnSc2	průkopník
<g/>
,	,	kIx,	,
režiséra	režisér	k1gMnSc2	režisér
<g/>
,	,	kIx,	,
producenta	producent	k1gMnSc2	producent
a	a	k8xC	a
ropného	ropný	k2eAgMnSc4d1	ropný
magnáta	magnát	k1gMnSc4	magnát
<g/>
.	.	kIx.	.
</s>
