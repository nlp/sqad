<s>
Vápník	vápník	k1gInSc1	vápník
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Calcium	Calcium	k1gNnSc1	Calcium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
prvek	prvek	k1gInSc1	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
jeho	on	k3xPp3gInSc2	on
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
názvu	název	k1gInSc2	název
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc4	Presl
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
více	hodně	k6eAd2	hodně
podobá	podobat	k5eAaImIp3nS	podobat
vlastnostem	vlastnost	k1gFnPc3	vlastnost
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
než	než	k8xS	než
vlastnostem	vlastnost	k1gFnPc3	vlastnost
předcházejícímu	předcházející	k2eAgInSc3d1	předcházející
členu	člen	k1gInSc3	člen
-	-	kIx~	-
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tmavěmodrého	tmavěmodrý	k2eAgInSc2d1	tmavěmodrý
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
lepším	dobrý	k2eAgInPc3d2	lepší
vodičům	vodič	k1gInPc3	vodič
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
jako	jako	k8xS	jako
alkalické	alkalický	k2eAgInPc4d1	alkalický
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
ho	on	k3xPp3gInSc4	on
uchovávat	uchovávat	k5eAaImF	uchovávat
pod	pod	k7c7	pod
petrolejem	petrolej	k1gInSc7	petrolej
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnSc3	sůl
vápníku	vápník	k1gInSc2	vápník
barví	barvit	k5eAaImIp3nS	barvit
plamen	plamen	k1gInSc1	plamen
cihlově	cihlově	k6eAd1	cihlově
červeně	červeně	k6eAd1	červeně
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pouze	pouze	k6eAd1	pouze
vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
i	i	k8xC	i
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
na	na	k7c4	na
nitrid	nitrid	k1gInSc4	nitrid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
a	a	k8xC	a
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
na	na	k7c4	na
hydrid	hydrid	k1gInSc4	hydrid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaH	CaH	k1gFnSc7	CaH
<g/>
2	[number]	k4	2
a	a	k8xC	a
i	i	k9	i
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
prvků	prvek	k1gInPc2	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
za	za	k7c2	za
vyšších	vysoký	k2eAgFnPc2d2	vyšší
teplot	teplota	k1gFnPc2	teplota
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
lidstvu	lidstvo	k1gNnSc3	lidstvo
známy	znám	k2eAgFnPc1d1	známa
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
-	-	kIx~	-
pálením	pálení	k1gNnSc7	pálení
vápence	vápenec	k1gInSc2	vápenec
nebo	nebo	k8xC	nebo
mramoru	mramor	k1gInSc2	mramor
se	se	k3xPyFc4	se
získávalo	získávat	k5eAaImAgNnS	získávat
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
získává	získávat	k5eAaImIp3nS	získávat
pálené	pálený	k2eAgNnSc4d1	pálené
vápno	vápno	k1gNnSc4	vápno
neboli	neboli	k8xC	neboli
oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
CaO	CaO	k1gMnSc1	CaO
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc2	jeho
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
hašené	hašený	k2eAgNnSc4d1	hašené
vápno	vápno	k1gNnSc4	vápno
neboli	neboli	k8xC	neboli
hydroxid	hydroxid	k1gInSc4	hydroxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
malty	malta	k1gFnSc2	malta
<g/>
.	.	kIx.	.
</s>
<s>
Malta	Malta	k1gFnSc1	Malta
se	se	k3xPyFc4	se
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
také	také	k6eAd1	také
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
ze	z	k7c2	z
sádry	sádra	k1gFnSc2	sádra
neboli	neboli	k8xC	neboli
sádrovce	sádrovec	k1gInSc2	sádrovec
či	či	k8xC	či
chemicky	chemicky	k6eAd1	chemicky
dihydrátu	dihydrát	k1gInSc2	dihydrát
síranu	síran	k1gInSc2	síran
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
CaSO	CaSO	k1gMnSc4	CaSO
<g/>
4.2	[number]	k4	4.2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používala	používat	k5eAaImAgFnS	používat
malta	malta	k1gFnSc1	malta
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používala	používat	k5eAaImAgFnS	používat
malta	malta	k1gFnSc1	malta
ze	z	k7c2	z
sádrovce	sádrovec	k1gInSc2	sádrovec
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
staré	starý	k2eAgFnPc1d1	stará
egyptské	egyptský	k2eAgFnPc1d1	egyptská
pyramidy	pyramida	k1gFnPc1	pyramida
a	a	k8xC	a
hrobky	hrobka	k1gFnPc1	hrobka
mají	mít	k5eAaImIp3nP	mít
omítky	omítka	k1gFnPc4	omítka
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
malty	malta	k1gFnSc2	malta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
již	již	k6eAd1	již
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Dioskoridem	Dioskorid	k1gInSc7	Dioskorid
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
pro	pro	k7c4	pro
oxid	oxid	k1gInSc4	oxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vápníku	vápník	k1gInSc2	vápník
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
vápno	vápno	k1gNnSc4	vápno
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
calx	calx	k1gInSc1	calx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
běžné	běžný	k2eAgNnSc1d1	běžné
označovat	označovat	k5eAaImF	označovat
oxid	oxid	k1gInSc4	oxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
jako	jako	k8xS	jako
vápenatou	vápenatý	k2eAgFnSc4d1	vápenatá
zeminu	zemina	k1gFnSc4	zemina
a	a	k8xC	a
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
zemina	zemina	k1gFnSc1	zemina
přeneslo	přenést	k5eAaPmAgNnS	přenést
i	i	k9	i
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
oxidy	oxid	k1gInPc4	oxid
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
(	(	kIx(	(
<g/>
kovy	kov	k1gInPc1	kov
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
jsou	být	k5eAaImIp3nP	být
prvky	prvek	k1gInPc1	prvek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
B	B	kA	B
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc4	vápník
poprvé	poprvé	k6eAd1	poprvé
připravil	připravit	k5eAaPmAgMnS	připravit
sir	sir	k1gMnSc1	sir
Humphry	Humphra	k1gFnSc2	Humphra
Davy	Dav	k1gInPc4	Dav
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
amalgámu	amalgám	k1gInSc2	amalgám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
připravil	připravit	k5eAaPmAgInS	připravit
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
slabě	slabě	k6eAd1	slabě
zvlhčeného	zvlhčený	k2eAgInSc2d1	zvlhčený
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
rtuťové	rtuťový	k2eAgFnSc2d1	rtuťová
katody	katoda	k1gFnSc2	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velké	velký	k2eAgFnSc3d1	velká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
vápník	vápník	k1gInSc1	vápník
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
má	mít	k5eAaImIp3nS	mít
mocenství	mocenství	k1gNnSc3	mocenství
Ca	ca	kA	ca
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
biogenní	biogenní	k2eAgInSc1d1	biogenní
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
stavebních	stavební	k2eAgInPc2d1	stavební
kamenů	kámen	k1gInPc2	kámen
buněk	buňka	k1gFnPc2	buňka
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
tvořena	tvořen	k2eAgFnSc1d1	tvořena
horninami	hornina	k1gFnPc7	hornina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
vápník	vápník	k1gInSc1	vápník
tvoří	tvořit	k5eAaImIp3nS	tvořit
velmi	velmi	k6eAd1	velmi
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
složku	složka	k1gFnSc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
tvoří	tvořit	k5eAaImIp3nS	tvořit
vápník	vápník	k1gInSc1	vápník
3,4	[number]	k4	3,4
-	-	kIx~	-
4,2	[number]	k4	4,2
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pátým	pátý	k4xOgInSc7	pátý
nejzastoupenějším	zastoupený	k2eAgInSc7d3	nejzastoupenější
prvkem	prvek	k1gInSc7	prvek
a	a	k8xC	a
třetím	třetí	k4xOgInSc7	třetí
nejzastoupenějším	zastoupený	k2eAgInSc7d3	nejzastoupenější
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
za	za	k7c4	za
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
před	před	k7c4	před
hořčík	hořčík	k1gInSc4	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pouze	pouze	k6eAd1	pouze
0,4	[number]	k4	0,4
g	g	kA	g
Ca	ca	kA	ca
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
a	a	k8xC	a
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
vápníku	vápník	k1gInSc2	vápník
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
horninou	hornina	k1gFnSc7	hornina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
vápníku	vápník	k1gInSc2	vápník
je	být	k5eAaImIp3nS	být
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
uhličitan	uhličitan	k1gInSc4	uhličitan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaCO	CaCO	k1gFnSc7	CaCO
<g/>
3	[number]	k4	3
tvořený	tvořený	k2eAgInSc1d1	tvořený
minerálem	minerál	k1gInSc7	minerál
kalcitem	kalcit	k1gInSc7	kalcit
nebo	nebo	k8xC	nebo
aragonitem	aragonit	k1gInSc7	aragonit
stejného	stejný	k2eAgNnSc2d1	stejné
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hornina	hornina	k1gFnSc1	hornina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
lokalitách	lokalita	k1gFnPc6	lokalita
biologického	biologický	k2eAgInSc2d1	biologický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
schránek	schránka	k1gFnPc2	schránka
obyvatelů	obyvatel	k1gMnPc2	obyvatel
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
druhohorních	druhohorní	k2eAgInPc2d1	druhohorní
<g/>
)	)	kIx)	)
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
patří	patřit	k5eAaImIp3nS	patřit
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
výskytem	výskyt	k1gInSc7	výskyt
hornin	hornina	k1gFnPc2	hornina
vápencového	vápencový	k2eAgInSc2d1	vápencový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
lokalita	lokalita	k1gFnSc1	lokalita
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Berounem	Beroun	k1gInSc7	Beroun
nebo	nebo	k8xC	nebo
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
typ	typ	k1gInSc1	typ
představuje	představovat	k5eAaImIp3nS	představovat
křída	křída	k1gFnSc1	křída
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
čistý	čistý	k2eAgInSc1d1	čistý
měkký	měkký	k2eAgInSc1d1	měkký
pórovitý	pórovitý	k2eAgInSc1d1	pórovitý
vápenec	vápenec	k1gInSc1	vápenec
s	s	k7c7	s
typicky	typicky	k6eAd1	typicky
zářivě	zářivě	k6eAd1	zářivě
bílou	bílý	k2eAgFnSc7d1	bílá
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
například	například	k6eAd1	například
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
kanálu	kanál	k1gInSc2	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Rujáně	Rujána	k1gFnSc6	Rujána
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnPc1	její
největší	veliký	k2eAgNnPc1d3	veliký
ložiska	ložisko	k1gNnPc1	ložisko
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
geologickém	geologický	k2eAgInSc6d1	geologický
období	období	k1gNnSc6	období
v	v	k7c6	v
pravěkých	pravěký	k2eAgNnPc6d1	pravěké
mořích	moře	k1gNnPc6	moře
vysrážením	vysrážení	k1gNnSc7	vysrážení
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
na	na	k7c6	na
usazených	usazený	k2eAgFnPc6d1	usazená
vápenatých	vápenatý	k2eAgFnPc6d1	vápenatá
skořápkách	skořápka	k1gFnPc6	skořápka
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgNnSc7d3	nejznámější
využitím	využití	k1gNnSc7	využití
je	být	k5eAaImIp3nS	být
plavením	plavení	k1gNnSc7	plavení
přírodní	přírodní	k2eAgFnSc2d1	přírodní
křídy	křída	k1gFnSc2	křída
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
psací	psací	k2eAgFnSc1d1	psací
křída	křída	k1gFnSc1	křída
<g/>
,	,	kIx,	,
důvěrně	důvěrně	k6eAd1	důvěrně
známá	známý	k2eAgFnSc1d1	známá
ze	z	k7c2	z
školního	školní	k2eAgNnSc2d1	školní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
ceněnou	ceněný	k2eAgFnSc7d1	ceněná
odrůdou	odrůda	k1gFnSc7	odrůda
vápence	vápenec	k1gInSc2	vápenec
je	být	k5eAaImIp3nS	být
mramor	mramor	k1gInSc1	mramor
nebo	nebo	k8xC	nebo
travertin	travertin	k1gInSc1	travertin
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
především	především	k9	především
k	k	k7c3	k
dekorativním	dekorativní	k2eAgInPc3d1	dekorativní
účelům	účel	k1gInPc3	účel
-	-	kIx~	-
obklady	obklad	k1gInPc4	obklad
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
naleziště	naleziště	k1gNnPc1	naleziště
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
(	(	kIx(	(
<g/>
carrarský	carrarský	k2eAgInSc4d1	carrarský
mramor	mramor	k1gInSc4	mramor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
slivenecký	slivenecký	k2eAgInSc4d1	slivenecký
mramor	mramor	k1gInSc4	mramor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přeměněnou	přeměněný	k2eAgFnSc4d1	přeměněná
horninu	hornina	k1gFnSc4	hornina
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
rekrystalizovaného	rekrystalizovaný	k2eAgInSc2d1	rekrystalizovaný
vysokým	vysoký	k2eAgInSc7d1	vysoký
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
příměsích	příměs	k1gFnPc6	příměs
a	a	k8xC	a
pigmentu	pigment	k1gInSc6	pigment
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
hornině	hornina	k1gFnSc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Příměsi	příměs	k1gFnPc1	příměs
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
hornině	hornina	k1gFnSc6	hornina
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
nebo	nebo	k8xC	nebo
v	v	k7c6	v
žílách	žíla	k1gFnPc6	žíla
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
metamorfózou	metamorfóza	k1gFnSc7	metamorfóza
přetvářejí	přetvářet	k5eAaImIp3nP	přetvářet
v	v	k7c4	v
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
mramorovou	mramorový	k2eAgFnSc4d1	mramorová
kresbu	kresba	k1gFnSc4	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
méně	málo	k6eAd2	málo
pevným	pevný	k2eAgNnSc7d1	pevné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mramor	mramor	k1gInSc1	mramor
s	s	k7c7	s
kresbou	kresba	k1gFnSc7	kresba
obvykle	obvykle	k6eAd1	obvykle
nepoužívá	používat	k5eNaImIp3nS	používat
pro	pro	k7c4	pro
sochy	socha	k1gFnPc4	socha
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
chemické	chemický	k2eAgInPc1d1	chemický
přechody	přechod	k1gInPc1	přechod
mezi	mezi	k7c7	mezi
uhličitanem	uhličitan	k1gInSc7	uhličitan
a	a	k8xC	a
hydrogenuhličitanem	hydrogenuhličitan	k1gInSc7	hydrogenuhličitan
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
krasových	krasový	k2eAgInPc2d1	krasový
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
vodě	voda	k1gFnSc3	voda
více	hodně	k6eAd2	hodně
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
než	než	k8xS	než
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
roztok	roztok	k1gInSc1	roztok
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
vodě	voda	k1gFnSc6	voda
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
atmosferickým	atmosferický	k2eAgInSc7d1	atmosferický
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc2	vznik
málo	málo	k6eAd1	málo
rozpustného	rozpustný	k2eAgInSc2d1	rozpustný
uhličitanu	uhličitan	k1gInSc2	uhličitan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
usadí	usadit	k5eAaPmIp3nS	usadit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
svého	své	k1gNnSc2	své
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
přírodní	přírodní	k2eAgInPc1d1	přírodní
úkazy	úkaz	k1gInPc1	úkaz
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
jeskynních	jeskynní	k2eAgInPc6d1	jeskynní
systémech	systém	k1gInPc6	systém
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
pomalý	pomalý	k2eAgInSc1d1	pomalý
růst	růst	k1gInSc1	růst
stalaktitů	stalaktit	k1gInPc2	stalaktit
<g/>
,	,	kIx,	,
stalagmitů	stalagmit	k1gInPc2	stalagmit
a	a	k8xC	a
stalagnátů	stalagnát	k1gInPc2	stalagnát
je	být	k5eAaImIp3nS	být
geologickou	geologický	k2eAgFnSc7d1	geologická
obdobou	obdoba	k1gFnSc7	obdoba
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
minerály	minerál	k1gInPc4	minerál
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
vápníku	vápník	k1gInSc2	vápník
je	být	k5eAaImIp3nS	být
dolomit	dolomit	k1gInSc1	dolomit
<g/>
,	,	kIx,	,
směsný	směsný	k2eAgInSc1d1	směsný
uhličitan	uhličitan	k1gInSc1	uhličitan
hořečnato-vápenatý	hořečnatoápenatý	k2eAgInSc4d1	hořečnato-vápenatý
CaMg	CaMg	k1gInSc4	CaMg
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
ložiska	ložisko	k1gNnPc1	ložisko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
i	i	k8xC	i
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Apatit	apatit	k1gInSc1	apatit
3	[number]	k4	3
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
jako	jako	k8xC	jako
poměrně	poměrně	k6eAd1	poměrně
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
fosforečnan	fosforečnan	k1gInSc1	fosforečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
přírodní	přírodní	k2eAgInPc4d1	přírodní
zdroje	zdroj	k1gInPc4	zdroj
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Fluorit	fluorit	k1gInSc1	fluorit
neboli	neboli	k8xC	neboli
kazivec	kazivec	k1gInSc1	kazivec
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc4	minerál
o	o	k7c6	o
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
CaF	CaF	k1gFnSc2	CaF
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
fluorid	fluorid	k1gInSc1	fluorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
ložiska	ložisko	k1gNnPc1	ložisko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
ale	ale	k8xC	ale
i	i	k9	i
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
především	především	k9	především
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
fluoru	fluor	k1gInSc2	fluor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
kámen	kámen	k1gInSc4	kámen
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
ozdobných	ozdobný	k2eAgInPc2d1	ozdobný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Sádrovec	sádrovec	k1gInSc1	sádrovec
neboli	neboli	k8xC	neboli
selenit	selenit	k5eAaImF	selenit
je	být	k5eAaImIp3nS	být
hydratovaný	hydratovaný	k2eAgInSc1d1	hydratovaný
síran	síran	k1gInSc4	síran
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaSO	CaSO	k1gFnSc7	CaSO
<g/>
4	[number]	k4	4
·	·	k?	·
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
SR	SR	kA	SR
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
méně	málo	k6eAd2	málo
zastoupeným	zastoupený	k2eAgMnPc3d1	zastoupený
a	a	k8xC	a
méně	málo	k6eAd2	málo
významným	významný	k2eAgInPc3d1	významný
minerálům	minerál	k1gInPc3	minerál
patří	patřit	k5eAaImIp3nS	patřit
dále	daleko	k6eAd2	daleko
anhydrit	anhydrit	k1gInSc1	anhydrit
CaSO	CaSO	k1gFnSc2	CaSO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
tachhydrid	tachhydrid	k1gInSc4	tachhydrid
CaCl	CaCla	k1gFnPc2	CaCla
<g/>
2.2	[number]	k4	2.2
MgCl	MgCla	k1gFnPc2	MgCla
<g/>
2.12	[number]	k4	2.12
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
polyhalit	polyhalit	k1gInSc1	polyhalit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
MgSO	MgSO	k1gFnSc2	MgSO
<g/>
4.2	[number]	k4	4.2
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4.2	[number]	k4	4.2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
O	O	kA	O
<g/>
,	,	kIx,	,
glauberit	glauberit	k1gInSc1	glauberit
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
CaSO	CaSO	k1gFnPc2	CaSO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
scheelit	scheelit	k1gInSc1	scheelit
CaWO	CaWO	k1gFnSc1	CaWO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
arseniosiderit	arseniosiderit	k1gInSc1	arseniosiderit
6	[number]	k4	6
CaO	CaO	k1gFnPc2	CaO
<g/>
.	.	kIx.	.
3	[number]	k4	3
As	as	k9	as
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3.4	[number]	k4	3.4
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
9	[number]	k4	9
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
wollastonit	wollastonit	k1gInSc1	wollastonit
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
dusičnanů	dusičnan	k1gInPc2	dusičnan
<g/>
,	,	kIx,	,
jodičnanů	jodičnan	k1gInPc2	jodičnan
<g/>
,	,	kIx,	,
uhličitanů	uhličitan	k1gInPc2	uhličitan
<g/>
,	,	kIx,	,
fosforečnanů	fosforečnan	k1gInPc2	fosforečnan
<g/>
,	,	kIx,	,
arseničnanů	arseničnan	k1gInPc2	arseničnan
<g/>
,	,	kIx,	,
boritanů	boritan	k1gInPc2	boritan
a	a	k8xC	a
křemičitanů	křemičitan	k1gInPc2	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
vápník	vápník	k1gInSc1	vápník
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
taveniny	tavenina	k1gFnSc2	tavenina
chloridu	chlorid	k1gInSc2	chlorid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
fluoridem	fluorid	k1gInSc7	fluorid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
nebo	nebo	k8xC	nebo
chloridem	chlorid	k1gInSc7	chlorid
draselným	draselný	k2eAgInSc7d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
produktem	produkt	k1gInSc7	produkt
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
elementární	elementární	k2eAgInSc4d1	elementární
chlor	chlor	k1gInSc4	chlor
nebo	nebo	k8xC	nebo
fluor	fluor	k1gInSc4	fluor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ihned	ihned	k6eAd1	ihned
dále	daleko	k6eAd2	daleko
zpracováván	zpracovávat	k5eAaImNgInS	zpracovávat
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
elektrolýze	elektrolýza	k1gFnSc3	elektrolýza
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
grafitové	grafitový	k2eAgFnSc2d1	grafitová
anody	anoda	k1gFnSc2	anoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
chlor	chlor	k1gInSc1	chlor
nebo	nebo	k8xC	nebo
fluor	fluor	k1gInSc1	fluor
<g/>
,	,	kIx,	,
a	a	k8xC	a
železné	železný	k2eAgFnPc1d1	železná
katody	katoda	k1gFnPc1	katoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
vápník	vápník	k1gInSc1	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc6d1	vysoká
čistotě	čistota	k1gFnSc6	čistota
připravit	připravit	k5eAaPmF	připravit
reakcí	reakce	k1gFnSc7	reakce
chloridu	chlorid	k1gInSc2	chlorid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
s	s	k7c7	s
hliníkem	hliník	k1gInSc7	hliník
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
chlorid	chlorid	k1gInSc1	chlorid
hlinitý	hlinitý	k2eAgInSc1d1	hlinitý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
těká	těkat	k5eAaImIp3nS	těkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
kovový	kovový	k2eAgInSc1d1	kovový
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
přečišťovat	přečišťovat	k5eAaImF	přečišťovat
destilací	destilace	k1gFnSc7	destilace
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
3	[number]	k4	3
CaCl	CaCl	k1gInSc1	CaCl
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
Al	ala	k1gFnPc2	ala
→	→	k?	→
2	[number]	k4	2
AlCl	AlCl	k1gInSc1	AlCl
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
Ca	ca	kA	ca
K	k	k7c3	k
malé	malý	k2eAgFnSc3d1	malá
přípravě	příprava	k1gFnSc3	příprava
vápníku	vápník	k1gInSc2	vápník
lze	lze	k6eAd1	lze
také	také	k9	také
využít	využít	k5eAaPmF	využít
termický	termický	k2eAgInSc4d1	termický
rozklad	rozklad	k1gInSc4	rozklad
azidu	azid	k1gInSc2	azid
vápenatého	vápenatý	k2eAgMnSc2d1	vápenatý
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
a	a	k8xC	a
vápník	vápník	k1gInSc4	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
okolo	okolo	k7c2	okolo
1000	[number]	k4	1000
tun	tuna	k1gFnPc2	tuna
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
vápník	vápník	k1gInSc1	vápník
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc4d1	silná
redukční	redukční	k2eAgFnPc4d1	redukční
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
jemně	jemně	k6eAd1	jemně
rozptýlený	rozptýlený	k2eAgInSc1d1	rozptýlený
kov	kov	k1gInSc1	kov
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
redukcím	redukce	k1gFnPc3	redukce
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
ale	ale	k8xC	ale
i	i	k9	i
redukční	redukční	k2eAgFnSc3d1	redukční
výrobě	výroba	k1gFnSc3	výroba
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
např.	např.	kA	např.
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
zirkonia	zirkonium	k1gNnSc2	zirkonium
<g/>
,	,	kIx,	,
thoria	thorium	k1gNnSc2	thorium
<g/>
,	,	kIx,	,
plutonia	plutonium	k1gNnSc2	plutonium
<g/>
,	,	kIx,	,
hafnia	hafnium	k1gNnSc2	hafnium
<g/>
,	,	kIx,	,
vanadu	vanad	k1gInSc2	vanad
<g/>
,	,	kIx,	,
či	či	k8xC	či
wolframu	wolfram	k1gInSc2	wolfram
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
neodymu	neodym	k1gInSc2	neodym
a	a	k8xC	a
boru	bor	k1gInSc2	bor
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
k	k	k7c3	k
regulaci	regulace	k1gFnSc3	regulace
obsahu	obsah	k1gInSc2	obsah
grafitického	grafitický	k2eAgInSc2d1	grafitický
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
litině	litina	k1gFnSc6	litina
nebo	nebo	k8xC	nebo
také	také	k9	také
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
bismutu	bismut	k1gInSc2	bismut
z	z	k7c2	z
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
reaktivita	reaktivita	k1gFnSc1	reaktivita
kovového	kovový	k2eAgInSc2d1	kovový
vápníku	vápník	k1gInSc2	vápník
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
přísada	přísada	k1gFnSc1	přísada
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vápenatých	vápenatý	k2eAgNnPc2d1	vápenaté
skel	sklo	k1gNnPc2	sklo
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
některých	některý	k3yIgFnPc2	některý
slitin	slitina	k1gFnPc2	slitina
-	-	kIx~	-
například	například	k6eAd1	například
olověný	olověný	k2eAgInSc1d1	olověný
ložiskový	ložiskový	k2eAgInSc1d1	ložiskový
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
olova	olovo	k1gNnSc2	olovo
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
0,04	[number]	k4	0,04
<g/>
%	%	kIx~	%
lithia	lithium	k1gNnSc2	lithium
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
speciální	speciální	k2eAgFnSc7d1	speciální
slitinou	slitina	k1gFnSc7	slitina
je	být	k5eAaImIp3nS	být
Nd-Fe-B	Nd-Fe-B	k1gFnSc1	Nd-Fe-B
slitina	slitina	k1gFnSc1	slitina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
pořizují	pořizovat	k5eAaImIp3nP	pořizovat
trvalé	trvalý	k2eAgInPc1d1	trvalý
magnety	magnet	k1gInPc1	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
do	do	k7c2	do
automobilových	automobilový	k2eAgInPc2d1	automobilový
bezobslužných	bezobslužný	k2eAgInPc2d1	bezobslužný
hermetických	hermetický	k2eAgInPc2d1	hermetický
akumulátorů	akumulátor	k1gInPc2	akumulátor
<g/>
.	.	kIx.	.
</s>
<s>
Vápníku	vápník	k1gInSc2	vápník
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
legovací	legovací	k2eAgFnPc4d1	legovací
látky	látka	k1gFnPc4	látka
pro	pro	k7c4	pro
zesílení	zesílení	k1gNnSc4	zesílení
hliníkových	hliníkový	k2eAgInPc2d1	hliníkový
nosníků	nosník	k1gInPc2	nosník
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vodíku	vodík	k1gInSc2	vodík
do	do	k7c2	do
balónů	balón	k1gInPc2	balón
pro	pro	k7c4	pro
meteorologické	meteorologický	k2eAgInPc4d1	meteorologický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Vápenec	vápenec	k1gInSc1	vápenec
(	(	kIx(	(
<g/>
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
hydroxid	hydroxid	k1gInSc4	hydroxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
dochované	dochovaný	k2eAgInPc1d1	dochovaný
zbytky	zbytek	k1gInPc1	zbytek
staveb	stavba	k1gFnPc2	stavba
postavených	postavený	k2eAgFnPc2d1	postavená
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
vápenné	vápenný	k2eAgFnSc2d1	vápenná
malty	malta	k1gFnSc2	malta
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
neolitu	neolit	k1gInSc2	neolit
(	(	kIx(	(
<g/>
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hašené	hašený	k2eAgNnSc1d1	hašené
vápno	vápno	k1gNnSc1	vápno
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
složkou	složka	k1gFnSc7	složka
mnoha	mnoho	k4c2	mnoho
důležitých	důležitý	k2eAgInPc2d1	důležitý
pojivých	pojivý	k2eAgInPc2d1	pojivý
prvků	prvek	k1gInPc2	prvek
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
malta	malta	k1gFnSc1	malta
<g/>
,	,	kIx,	,
omítkové	omítkový	k2eAgFnPc1d1	omítková
směsi	směs	k1gFnPc1	směs
atd.	atd.	kA	atd.
Při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
aplikaci	aplikace	k1gFnSc6	aplikace
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
reakci	reakce	k1gFnSc3	reakce
bazického	bazický	k2eAgNnSc2d1	bazické
vápna	vápno	k1gNnSc2	vápno
se	se	k3xPyFc4	se
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
původního	původní	k2eAgInSc2d1	původní
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
CaCO	CaCO	k1gMnSc4	CaCO
<g/>
3	[number]	k4	3
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
CO2	CO2	k1gMnPc2	CO2
→	→	k?	→
CaCO	CaCO	k1gFnSc7	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
Hydroxid	hydroxid	k1gInSc4	hydroxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
-	-	kIx~	-
hašené	hašený	k2eAgNnSc1d1	hašené
vápno	vápno	k1gNnSc1	vápno
-	-	kIx~	-
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
v	v	k7c4	v
lékařství	lékařství	k1gNnSc4	lékařství
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
dezinfekční	dezinfekční	k2eAgInPc4d1	dezinfekční
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
k	k	k7c3	k
bělení	bělení	k1gNnSc3	bělení
chlévů	chlév	k1gInPc2	chlév
nebo	nebo	k8xC	nebo
venkovních	venkovní	k2eAgFnPc2d1	venkovní
omítek	omítka	k1gFnPc2	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
na	na	k7c6	na
sklech	sklo	k1gNnPc6	sklo
skleníků	skleník	k1gInPc2	skleník
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
po	po	k7c6	po
zatvrdnutí	zatvrdnutí	k1gNnSc6	zatvrdnutí
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
brání	bránit	k5eAaImIp3nS	bránit
přímému	přímý	k2eAgNnSc3d1	přímé
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
záření	záření	k1gNnSc3	záření
na	na	k7c4	na
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vápenec	vápenec	k1gInSc1	vápenec
i	i	k8xC	i
sádrovec	sádrovec	k1gInSc1	sádrovec
jsou	být	k5eAaImIp3nP	být
složkami	složka	k1gFnPc7	složka
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dnes	dnes	k6eAd1	dnes
patrně	patrně	k6eAd1	patrně
nejběžnějšího	běžný	k2eAgInSc2d3	nejběžnější
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
-	-	kIx~	-
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smíšení	smíšení	k1gNnSc6	smíšení
s	s	k7c7	s
pískem	písek	k1gInSc7	písek
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
a	a	k8xC	a
odolná	odolný	k2eAgFnSc1d1	odolná
hmota	hmota	k1gFnSc1	hmota
-	-	kIx~	-
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
denně	denně	k6eAd1	denně
jako	jako	k9	jako
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
konstrukce	konstrukce	k1gFnPc4	konstrukce
moderních	moderní	k2eAgFnPc2d1	moderní
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
základním	základní	k2eAgInSc7d1	základní
materiálem	materiál	k1gInSc7	materiál
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
leteckých	letecký	k2eAgFnPc2d1	letecká
přistávacích	přistávací	k2eAgFnPc2d1	přistávací
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
železničních	železniční	k2eAgInPc2d1	železniční
pražců	pražec	k1gInPc2	pražec
aj.	aj.	kA	aj.
Uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
křídy	křída	k1gFnSc2	křída
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nátěrová	nátěrový	k2eAgFnSc1d1	nátěrová
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
křídová	křídový	k2eAgFnSc1d1	křídová
běloba	běloba	k1gFnSc1	běloba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
zubních	zubní	k2eAgInPc2d1	zubní
prášků	prášek	k1gInPc2	prášek
<g/>
,	,	kIx,	,
tmelů	tmel	k1gInPc2	tmel
<g/>
,	,	kIx,	,
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
atd.	atd.	kA	atd.
Sádrovec	sádrovec	k1gInSc1	sádrovec
(	(	kIx(	(
<g/>
dihydrát	dihydrát	k1gInSc1	dihydrát
síranu	síran	k1gInSc2	síran
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
štukaterským	štukaterský	k2eAgFnPc3d1	štukaterský
pracím	práce	k1gFnPc3	práce
a	a	k8xC	a
ke	k	k7c3	k
zhotovování	zhotovování	k1gNnSc3	zhotovování
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
sádry	sádra	k1gFnSc2	sádra
neboli	neboli	k8xC	neboli
hemihydrátu	hemihydrát	k1gInSc2	hemihydrát
síranu	síran	k1gInSc2	síran
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
CaSO	CaSO	k1gMnSc4	CaSO
<g/>
4	[number]	k4	4
·	·	k?	·
1⁄	1⁄	k?	1⁄
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
termickým	termický	k2eAgInSc7d1	termický
rozkladem	rozklad	k1gInSc7	rozklad
sádrovce	sádrovec	k1gInSc2	sádrovec
CaSO	CaSO	k1gFnSc7	CaSO
<g/>
4	[number]	k4	4
·	·	k?	·
2	[number]	k4	2
H2O	H2O	k1gFnSc6	H2O
a	a	k8xC	a
po	po	k7c6	po
smíšení	smíšení	k1gNnSc6	smíšení
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opětné	opětný	k2eAgFnSc3d1	opětná
hydrataci	hydratace	k1gFnSc3	hydratace
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
zářivě	zářivě	k6eAd1	zářivě
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
všestranné	všestranný	k2eAgNnSc1d1	všestranné
využití	využití	k1gNnSc1	využití
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kopií	kopie	k1gFnPc2	kopie
různých	různý	k2eAgInPc2d1	různý
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
zubní	zubní	k2eAgNnSc4d1	zubní
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
sádry	sádra	k1gFnSc2	sádra
a	a	k8xC	a
přísad	přísada	k1gFnPc2	přísada
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
tuhnutí	tuhnutí	k1gNnSc6	tuhnutí
lze	lze	k6eAd1	lze
docílit	docílit	k5eAaPmF	docílit
celé	celý	k2eAgFnSc2d1	celá
škály	škála	k1gFnSc2	škála
výsledných	výsledný	k2eAgInPc2d1	výsledný
produktů	produkt	k1gInPc2	produkt
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
tvrdostí	tvrdost	k1gFnSc7	tvrdost
<g/>
,	,	kIx,	,
rychlostí	rychlost	k1gFnSc7	rychlost
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
<g/>
,	,	kIx,	,
barvou	barva	k1gFnSc7	barva
apod.	apod.	kA	apod.
Odrůdy	odrůda	k1gFnSc2	odrůda
sádrovce	sádrovec	k1gInSc2	sádrovec
jsou	být	k5eAaImIp3nP	být
mariánské	mariánský	k2eAgNnSc4d1	Mariánské
sklo	sklo	k1gNnSc4	sklo
a	a	k8xC	a
alabastr	alabastr	k1gInSc4	alabastr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
obkladový	obkladový	k2eAgInSc4d1	obkladový
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Karbid	karbid	k1gInSc1	karbid
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
silné	silný	k2eAgNnSc4d1	silné
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
využívalo	využívat	k5eAaPmAgNnS	využívat
v	v	k7c6	v
hornictví	hornictví	k1gNnSc6	hornictví
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
karbidky	karbidka	k1gFnPc1	karbidka
neboli	neboli	k8xC	neboli
karbidové	karbidový	k2eAgFnPc1d1	karbidová
lampy	lampa	k1gFnPc1	lampa
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
docházelo	docházet	k5eAaImAgNnS	docházet
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
karbidu	karbid	k1gInSc2	karbid
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
ethynu	ethyn	k1gInSc2	ethyn
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
hořel	hořet	k5eAaImAgInS	hořet
a	a	k8xC	a
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
vznikalo	vznikat	k5eAaImAgNnS	vznikat
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lampy	lampa	k1gFnPc1	lampa
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
horníkovy	horníkův	k2eAgFnPc1d1	horníkova
přilbice	přilbice	k1gFnPc1	přilbice
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
regulátoru	regulátor	k1gInSc2	regulátor
přikapávání	přikapávání	k1gNnSc2	přikapávání
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
regulovali	regulovat	k5eAaImAgMnP	regulovat
rychlost	rychlost	k1gFnSc4	rychlost
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
množství	množství	k1gNnSc4	množství
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Fosforečnany	fosforečnan	k1gInPc1	fosforečnan
vápenaté	vápenatý	k2eAgInPc1d1	vápenatý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
CaHPO	CaHPO	k1gFnSc1	CaHPO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
,	,	kIx,	,
dodávající	dodávající	k2eAgInPc4d1	dodávající
rostlinám	rostlina	k1gFnPc3	rostlina
jak	jak	k9	jak
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
vápník	vápník	k1gInSc1	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc4	hydrid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaH	CaH	k1gFnSc7	CaH
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
silné	silný	k2eAgNnSc1d1	silné
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
ceně	cena	k1gFnSc3	cena
k	k	k7c3	k
běžným	běžný	k2eAgFnPc3d1	běžná
redukcím	redukce	k1gFnPc3	redukce
nevyužívá	využívat	k5eNaImIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
hydridu	hydrid	k1gInSc2	hydrid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
hydroxid	hydroxid	k1gInSc1	hydroxid
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nejsnáze	snadno	k6eAd3	snadno
se	se	k3xPyFc4	se
připraví	připravit	k5eAaPmIp3nS	připravit
reakcí	reakce	k1gFnSc7	reakce
zahřátého	zahřátý	k2eAgInSc2d1	zahřátý
vápníku	vápník	k1gInSc2	vápník
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
často	často	k6eAd1	často
vápník	vápník	k1gInSc4	vápník
ve	v	k7c6	v
vodíku	vodík	k1gInSc6	vodík
začne	začít	k5eAaPmIp3nS	začít
hořet	hořet	k5eAaImF	hořet
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
CaO	CaO	k1gMnSc1	CaO
neboli	neboli	k8xC	neboli
žíravé	žíravý	k2eAgNnSc1d1	žíravé
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pálením	pálení	k1gNnSc7	pálení
vápence	vápenec	k1gInSc2	vápenec
neboli	neboli	k8xC	neboli
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgNnSc2d1	vápenaté
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnPc2	reakce
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
vápníkem	vápník	k1gInSc7	vápník
<g/>
.	.	kIx.	.
</s>
<s>
CaCO	CaCO	k?	CaCO
<g/>
3	[number]	k4	3
→	→	k?	→
CaO	CaO	k1gMnSc1	CaO
+	+	kIx~	+
CO2	CO2	k1gMnSc1	CO2
Hydroxid	hydroxid	k1gInSc4	hydroxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
velmi	velmi	k6eAd1	velmi
drobné	drobný	k2eAgFnPc1d1	drobná
destičkovité	destičkovitý	k2eAgFnPc1d1	destičkovitý
krystalky	krystalka	k1gFnPc1	krystalka
portlanditu	portlandita	k1gFnSc4	portlandita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
s	s	k7c7	s
roztoucí	roztoucí	k2eAgFnSc7d1	roztoucí
teplotou	teplota	k1gFnSc7	teplota
rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
dokonce	dokonce	k9	dokonce
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vápenné	vápenný	k2eAgNnSc1d1	vápenné
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
hašením	hašení	k1gNnSc7	hašení
páleného	pálený	k2eAgNnSc2d1	pálené
vápna	vápno	k1gNnSc2	vápno
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnPc2	reakce
vápníku	vápník	k1gInSc2	vápník
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
CaO	CaO	k?	CaO
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
Peroxid	peroxid	k1gInSc1	peroxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
CaO	CaO	k1gFnSc2	CaO
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
peroxidu	peroxid	k1gInSc2	peroxid
sodného	sodný	k2eAgInSc2d1	sodný
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
vápenatých	vápenatý	k2eAgFnPc2d1	vápenatá
solí	sůl	k1gFnPc2	sůl
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpuští	rozpuštit	k5eAaPmIp3nS	rozpuštit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
hůře	zle	k6eAd2	zle
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
soli	sůl	k1gFnPc1	sůl
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
anion	anion	k1gInSc1	anion
soli	sůl	k1gFnSc2	sůl
barevný	barevný	k2eAgMnSc1d1	barevný
(	(	kIx(	(
<g/>
manganistany	manganistan	k1gInPc1	manganistan
<g/>
,	,	kIx,	,
chromany	chroman	k1gInPc1	chroman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
lépe	dobře	k6eAd2	dobře
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
než	než	k8xS	než
soli	sůl	k1gFnPc1	sůl
hořečnaté	hořečnatý	k2eAgFnPc1d1	hořečnatá
<g/>
.	.	kIx.	.
</s>
<s>
Vápenaté	vápenatý	k2eAgFnPc4d1	vápenatá
soli	sůl	k1gFnPc4	sůl
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
snadno	snadno	k6eAd1	snadno
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
i	i	k9	i
komplexy	komplex	k1gInPc1	komplex
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ale	ale	k9	ale
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
vápník	vápník	k1gInSc4	vápník
a	a	k8xC	a
i	i	k9	i
další	další	k2eAgInPc1d1	další
kovy	kov	k1gInPc1	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
typické	typický	k2eAgFnPc1d1	typická
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaF	CaF	k1gFnSc7	CaF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
<g/>
,	,	kIx,	,
gelovitá	gelovitý	k2eAgFnSc1d1	gelovitá
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
vápenatých	vápenatý	k2eAgMnPc2d1	vápenatý
solí	solit	k5eAaImIp3nP	solit
fluoridovými	fluoridový	k2eAgInPc7d1	fluoridový
aniony	anion	k1gInPc7	anion
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
či	či	k8xC	či
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
fluorvodíkovou	fluorvodíková	k1gFnSc7	fluorvodíková
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaCl	CaCl	k1gInSc4	CaCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
normálně	normálně	k6eAd1	normálně
váže	vázat	k5eAaImIp3nS	vázat
2	[number]	k4	2
molekuly	molekula	k1gFnSc2	molekula
vody	voda	k1gFnSc2	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
CaCl	CaCla	k1gFnPc2	CaCla
<g/>
2	[number]	k4	2
·	·	k?	·
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Zahřátím	zahřátí	k1gNnSc7	zahřátí
lze	lze	k6eAd1	lze
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
vodu	voda	k1gFnSc4	voda
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
látku	látka	k1gFnSc4	látka
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
sušení	sušení	k1gNnSc3	sušení
organických	organický	k2eAgFnPc2d1	organická
tekutin	tekutina	k1gFnPc2	tekutina
nebo	nebo	k8xC	nebo
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
sypání	sypání	k1gNnSc3	sypání
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
chodníků	chodník	k1gInPc2	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlororvodíkové	chlororvodíková	k1gFnSc2	chlororvodíková
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaBr	CaBr	k1gInSc4	CaBr
<g/>
2	[number]	k4	2
a	a	k8xC	a
jodid	jodid	k1gInSc4	jodid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaI	CaI	k1gFnSc7	CaI
<g/>
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
krystalické	krystalický	k2eAgFnPc1d1	krystalická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
fotografickém	fotografický	k2eAgInSc6d1	fotografický
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
i	i	k8xC	i
bromid	bromid	k1gInSc4	bromid
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
bromovodíkové	bromovodíkový	k2eAgFnSc6d1	bromovodíkový
popř.	popř.	kA	popř.
kyselině	kyselina	k1gFnSc6	kyselina
jodovodíkové	jodovodíkový	k2eAgFnSc6d1	jodovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc4	dusičnan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
neboli	neboli	k8xC	neboli
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
ledek	ledek	k1gInSc1	ledek
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaCO	CaCO	k1gFnSc7	CaCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
obtížně	obtížně	k6eAd1	obtížně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
stabilních	stabilní	k2eAgFnPc6d1	stabilní
a	a	k8xC	a
jedné	jeden	k4xCgFnSc3	jeden
nestabilní	stabilní	k2eNgFnSc3d1	nestabilní
modifikaci	modifikace	k1gFnSc3	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	s	k7c7	s
srážením	srážení	k1gNnSc7	srážení
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
uhličitanovými	uhličitanový	k2eAgInPc7d1	uhličitanový
aniony	anion	k1gInPc7	anion
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
-	-	kIx~	-
podstata	podstata	k1gFnSc1	podstata
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
vápenné	vápenný	k2eAgFnSc2d1	vápenná
malty	malta	k1gFnSc2	malta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
působením	působení	k1gNnSc7	působení
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kysličníku	kysličník	k1gInSc2	kysličník
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
na	na	k7c4	na
vápenec	vápenec	k1gInSc4	vápenec
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
krasové	krasový	k2eAgFnPc1d1	krasová
oblasti	oblast	k1gFnPc1	oblast
(	(	kIx(	(
<g/>
krápníky	krápník	k1gInPc1	krápník
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
již	již	k9	již
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
vzniku	vznik	k1gInSc2	vznik
krápníků	krápník	k1gInPc2	krápník
také	také	k9	také
schématem	schéma	k1gNnSc7	schéma
koloběhu	koloběh	k1gInSc2	koloběh
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
:	:	kIx,	:
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
↔	↔	k?	↔
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
Hydrogenuhličitan	Hydrogenuhličitan	k1gInSc1	Hydrogenuhličitan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
lehce	lehko	k6eAd1	lehko
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
koloběhu	koloběh	k1gInSc6	koloběh
Ca	ca	kA	ca
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tzv.	tzv.	kA	tzv.
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
tvrdost	tvrdost	k1gFnSc4	tvrdost
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mg	mg	kA	mg
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
jednoduše	jednoduše	k6eAd1	jednoduše
odstranit	odstranit	k5eAaPmF	odstranit
převařením	převaření	k1gNnSc7	převaření
<g/>
.	.	kIx.	.
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
všem	všecek	k3xTgMnPc3	všecek
známý	známý	k2eAgInSc1d1	známý
vodní	vodní	k2eAgInSc4d1	vodní
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
:	:	kIx,	:
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
→	→	k?	→
CaCO	CaCO	k1gMnSc1	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
+	+	kIx~	+
CO2	CO2	k1gMnSc1	CO2
Síran	síran	k1gInSc4	síran
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaSO	CaSO	k1gFnSc7	CaSO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c7	za
nerozpustnou	rozpustný	k2eNgFnSc7d1	nerozpustná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
při	při	k7c6	při
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
v	v	k7c6	v
několika	několik	k4yIc6	několik
krystalických	krystalický	k2eAgFnPc6d1	krystalická
modifikacích	modifikace	k1gFnPc6	modifikace
anebo	anebo	k8xC	anebo
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
nerostů	nerost	k1gInPc2	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
vápenaté	vápenatý	k2eAgFnSc2d1	vápenatá
soli	sůl	k1gFnSc2	sůl
se	s	k7c7	s
síranovými	síranový	k2eAgInPc7d1	síranový
aniony	anion	k1gInPc7	anion
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
vápníku	vápník	k1gInSc2	vápník
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
šťavelan	šťavelan	k1gInSc1	šťavelan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
listech	list	k1gInPc6	list
rebarbory	rebarbora	k1gFnSc2	rebarbora
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
šťavelovou	šťavelový	k2eAgFnSc7d1	šťavelová
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jejich	jejich	k3xOp3gFnSc4	jejich
jedovatost	jedovatost	k1gFnSc4	jedovatost
<g/>
,	,	kIx,	,
a	a	k8xC	a
vápenaté	vápenatý	k2eAgInPc1d1	vápenatý
alkoholáty	alkoholát	k1gInPc1	alkoholát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
vápenatým	vápenatý	k2eAgFnPc3d1	vápenatá
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
organické	organický	k2eAgInPc4d1	organický
komplexy	komplex	k1gInPc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
organických	organický	k2eAgFnPc2d1	organická
vápenatých	vápenatý	k2eAgFnPc2d1	vápenatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
obratlovců	obratlovec	k1gMnPc2	obratlovec
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
<g/>
,	,	kIx,	,
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
tělesných	tělesný	k2eAgFnPc6d1	tělesná
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdé	Tvrdé	k2eAgFnPc1d1	Tvrdé
schránky	schránka	k1gFnPc1	schránka
–	–	k?	–
škeble	škeble	k1gFnSc1	škeble
a	a	k8xC	a
mušle	mušle	k1gFnSc1	mušle
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
chránící	chránící	k2eAgNnPc1d1	chránící
těla	tělo	k1gNnPc1	tělo
různých	různý	k2eAgMnPc2d1	různý
mořských	mořský	k2eAgMnPc2d1	mořský
i	i	k8xC	i
sladkovodních	sladkovodní	k2eAgMnPc2d1	sladkovodní
plžů	plž	k1gMnPc2	plž
a	a	k8xC	a
mlžů	mlž	k1gMnPc2	mlž
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
především	především	k6eAd1	především
sloučeninami	sloučenina	k1gFnPc7	sloučenina
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Mohutné	mohutný	k2eAgInPc4d1	mohutný
korálové	korálový	k2eAgInPc4d1	korálový
útesy	útes	k1gInPc4	útes
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
mořští	mořský	k2eAgMnPc1d1	mořský
polypi	polyp	k1gMnPc1	polyp
z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
korálnatců	korálnatec	k1gMnPc2	korálnatec
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc7	zbytek
vápenitých	vápenitý	k2eAgFnPc2d1	vápenitá
koster	kostra	k1gFnPc2	kostra
těchto	tento	k3xDgMnPc2	tento
uhynulých	uhynulý	k2eAgMnPc2d1	uhynulý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
hlemýždi	hlemýžď	k1gMnPc7	hlemýžď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
jejich	jejich	k3xOp3gFnSc1	jejich
vápenitá	vápenitý	k2eAgFnSc1d1	vápenitá
ulita	ulita	k1gFnSc1	ulita
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c4	před
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
je	být	k5eAaImIp3nS	být
nejhojněji	hojně	k6eAd3	hojně
se	se	k3xPyFc4	se
vyskytujícím	vyskytující	k2eAgInSc7d1	vyskytující
minerálem	minerál	k1gInSc7	minerál
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
zcela	zcela	k6eAd1	zcela
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
zdraví	zdraví	k1gNnSc2	zdraví
našich	náš	k3xOp1gFnPc2	náš
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
potravě	potrava	k1gFnSc6	potrava
představuje	představovat	k5eAaImIp3nS	představovat
vápník	vápník	k1gInSc1	vápník
velmi	velmi	k6eAd1	velmi
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
složku	složka	k1gFnSc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
také	také	k9	také
správné	správný	k2eAgFnSc3d1	správná
funkci	funkce	k1gFnSc3	funkce
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
srážení	srážení	k1gNnSc3	srážení
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
99	[number]	k4	99
<g/>
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
vápníku	vápník	k1gInSc2	vápník
přítomného	přítomný	k2eAgInSc2d1	přítomný
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
a	a	k8xC	a
zubech	zub	k1gInPc6	zub
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgFnSc2d1	zbývající
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
měkkých	měkký	k2eAgFnPc6d1	měkká
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
vápník	vápník	k1gInSc1	vápník
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
pro	pro	k7c4	pro
zdravý	zdravý	k2eAgInSc4d1	zdravý
vývin	vývin	k1gInSc4	vývin
a	a	k8xC	a
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
především	především	k9	především
v	v	k7c6	v
jídelníčku	jídelníček	k1gInSc6	jídelníček
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgMnSc1d1	důležitý
přitom	přitom	k6eAd1	přitom
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
dostatek	dostatek	k1gInSc4	dostatek
samotného	samotný	k2eAgInSc2d1	samotný
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
vitaminu	vitamin	k1gInSc2	vitamin
D	D	kA	D
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
ukládání	ukládání	k1gNnSc6	ukládání
vápníku	vápník	k1gInSc2	vápník
do	do	k7c2	do
kostní	kostní	k2eAgFnSc2d1	kostní
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Distribuci	distribuce	k1gFnSc4	distribuce
a	a	k8xC	a
využití	využití	k1gNnSc4	využití
vápníku	vápník	k1gInSc2	vápník
řídí	řídit	k5eAaImIp3nP	řídit
některé	některý	k3yIgInPc4	některý
hormony	hormon	k1gInPc4	hormon
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
a	a	k8xC	a
příštitných	příštitný	k2eAgNnPc2d1	příštitný
tělísek	tělísko	k1gNnPc2	tělísko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
vápníků	vápník	k1gInPc2	vápník
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
i	i	k8xC	i
prvek	prvek	k1gInSc1	prvek
hořčík	hořčík	k1gInSc1	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
faktorů	faktor	k1gInPc2	faktor
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
onemocnění	onemocnění	k1gNnSc2	onemocnění
křivice	křivice	k1gFnSc2	křivice
neboli	neboli	k8xC	neboli
rachitidy	rachitida	k1gFnSc2	rachitida
<g/>
.	.	kIx.	.
</s>
<s>
U	U	kA	U
Nedostatek	nedostatek	k1gInSc1	nedostatek
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
je	být	k5eAaImIp3nS	být
podezřelý	podezřelý	k2eAgInSc1d1	podezřelý
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
faktorů	faktor	k1gInPc2	faktor
vyvolávající	vyvolávající	k2eAgFnSc4d1	vyvolávající
později	pozdě	k6eAd2	pozdě
roztroušenou	roztroušený	k2eAgFnSc4d1	roztroušená
sklerózu	skleróza	k1gFnSc4	skleróza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
pohybu	pohyb	k1gInSc2	pohyb
či	či	k8xC	či
nedostatečným	dostatečný	k2eNgInSc7d1	nedostatečný
příjmem	příjem	k1gInSc7	příjem
vápníku	vápník	k1gInSc2	vápník
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
vápníku	vápník	k1gInSc2	vápník
z	z	k7c2	z
kostní	kostní	k2eAgFnSc2d1	kostní
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
osteoporóza	osteoporóza	k1gFnSc1	osteoporóza
(	(	kIx(	(
<g/>
řídnutí	řídnutí	k1gNnSc1	řídnutí
kostí	kost	k1gFnPc2	kost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
křehké	křehký	k2eAgFnPc1d1	křehká
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
lámou	lámat	k5eAaImIp3nP	lámat
a	a	k8xC	a
zlomeniny	zlomenina	k1gFnPc4	zlomenina
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
obtížně	obtížně	k6eAd1	obtížně
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
zdlouhavě	zdlouhavě	k6eAd1	zdlouhavě
hojí	hojit	k5eAaImIp3nS	hojit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
výzkumu	výzkum	k1gInSc2	výzkum
prováděného	prováděný	k2eAgInSc2d1	prováděný
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přibližně	přibližně	k6eAd1	přibližně
34	[number]	k4	34
miliónů	milión	k4xCgInPc2	milión
Američanů	Američan	k1gMnPc2	Američan
trpí	trpět	k5eAaImIp3nP	trpět
sníženou	snížený	k2eAgFnSc7d1	snížená
hustotou	hustota	k1gFnSc7	hustota
kostní	kostní	k2eAgFnSc2d1	kostní
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
u	u	k7c2	u
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
již	již	k6eAd1	již
propukla	propuknout	k5eAaPmAgFnS	propuknout
osteoporóza	osteoporóza	k1gFnSc1	osteoporóza
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pacienty	pacient	k1gMnPc7	pacient
trpícími	trpící	k2eAgMnPc7d1	trpící
osteoporózou	osteoporóza	k1gFnSc7	osteoporóza
přitom	přitom	k6eAd1	přitom
najdeme	najít	k5eAaPmIp1nP	najít
z	z	k7c2	z
80	[number]	k4	80
<g/>
%	%	kIx~	%
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
z	z	k7c2	z
20	[number]	k4	20
<g/>
%	%	kIx~	%
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Zlomenina	zlomenina	k1gFnSc1	zlomenina
zaviněná	zaviněný	k2eAgFnSc1d1	zaviněná
řídnutím	řídnutí	k1gNnSc7	řídnutí
kostí	kost	k1gFnPc2	kost
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
životě	život	k1gInSc6	život
objeví	objevit	k5eAaPmIp3nS	objevit
u	u	k7c2	u
poloviny	polovina	k1gFnSc2	polovina
všech	všecek	k3xTgFnPc2	všecek
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
všech	všecek	k3xTgMnPc2	všecek
mužů	muž	k1gMnPc2	muž
starších	starý	k2eAgMnPc2d2	starší
50	[number]	k4	50
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
vápníku	vápník	k1gInSc2	vápník
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
činit	činit	k5eAaImF	činit
800	[number]	k4	800
-	-	kIx~	-
1000	[number]	k4	1000
mg	mg	kA	mg
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
u	u	k7c2	u
kojících	kojící	k2eAgFnPc2d1	kojící
žen	žena	k1gFnPc2	žena
ještě	ještě	k6eAd1	ještě
asi	asi	k9	asi
o	o	k7c4	o
500	[number]	k4	500
mg	mg	kA	mg
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
mají	mít	k5eAaImIp3nP	mít
někteři	někter	k1gMnPc1	někter
lidé	člověk	k1gMnPc1	člověk
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdroj	zdroj	k1gInSc1	zdroj
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
potravě	potrava	k1gFnSc6	potrava
představuje	představovat	k5eAaImIp3nS	představovat
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
novějších	nový	k2eAgFnPc2d2	novější
studií	studie	k1gFnPc2	studie
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
uměle	uměle	k6eAd1	uměle
udržovaný	udržovaný	k2eAgInSc4d1	udržovaný
mýtus	mýtus	k1gInSc4	mýtus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vápník	vápník	k1gInSc1	vápník
v	v	k7c6	v
kravském	kravský	k2eAgNnSc6d1	kravské
mléce	mléko	k1gNnSc6	mléko
obsažený	obsažený	k2eAgInSc1d1	obsažený
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
špatnou	špatný	k2eAgFnSc4d1	špatná
vstřebatelnost	vstřebatelnost	k1gFnSc4	vstřebatelnost
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
studie	studie	k1gFnPc1	studie
navíc	navíc	k6eAd1	navíc
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
trávení	trávení	k1gNnSc6	trávení
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
spotřebováván	spotřebováván	k2eAgInSc4d1	spotřebováván
vápník	vápník	k1gInSc4	vápník
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
tělesných	tělesný	k2eAgFnPc2d1	tělesná
zásob	zásoba	k1gFnPc2	zásoba
-	-	kIx~	-
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
vápníku	vápník	k1gInSc2	vápník
jsou	být	k5eAaImIp3nP	být
nejrozšiřenější	rozšiřený	k2eAgInPc1d3	rozšiřený
právě	právě	k9	právě
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
nejvysší	jvysší	k2eNgFnSc7d1	jvysší
spotřebou	spotřeba	k1gFnSc7	spotřeba
mléka	mléko	k1gNnSc2	mléko
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Vápník	vápník	k1gInSc1	vápník
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
listové	listový	k2eAgFnSc2d1	listová
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
semenech	semeno	k1gNnPc6	semeno
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
máku	mák	k1gInSc2	mák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ořeších	ořech	k1gInPc6	ořech
<g/>
,	,	kIx,	,
ovesných	ovesný	k2eAgFnPc6d1	ovesná
vločkách	vločka	k1gFnPc6	vločka
a	a	k8xC	a
řadě	řada	k1gFnSc6	řada
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
zelenině	zelenina	k1gFnSc6	zelenina
je	být	k5eAaImIp3nS	být
vápník	vápník	k1gInSc1	vápník
(	(	kIx(	(
<g/>
i	i	k9	i
některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
prvky	prvek	k1gInPc1	prvek
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
vázán	vázat	k5eAaImNgInS	vázat
jako	jako	k8xS	jako
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
fytát	fytát	k1gInSc1	fytát
či	či	k8xC	či
šťavelan	šťavelan	k1gInSc1	šťavelan
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vláknina	vláknina	k1gFnSc1	vláknina
omezuje	omezovat	k5eAaImIp3nS	omezovat
jeho	jeho	k3xOp3gNnSc4	jeho
využití	využití	k1gNnSc4	využití
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidská	lidský	k2eAgFnSc1d1	lidská
strava	strava	k1gFnSc1	strava
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
celkově	celkově	k6eAd1	celkově
vyvážená	vyvážený	k2eAgFnSc1d1	vyvážená
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přísunem	přísun	k1gInSc7	přísun
důležitého	důležitý	k2eAgNnSc2d1	důležité
množství	množství	k1gNnSc2	množství
vápníku	vápník	k1gInSc2	vápník
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
dostatek	dostatek	k1gInSc4	dostatek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
minerálních	minerální	k2eAgFnPc2d1	minerální
složek	složka	k1gFnPc2	složka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hořčíku	hořčík	k1gInSc2	hořčík
či	či	k8xC	či
fosforu	fosfor	k1gInSc2	fosfor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Jursík	Jursík	k1gInSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
nekovů	nekov	k1gInPc2	nekov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vápník	vápník	k1gInSc1	vápník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vápník	vápník	k1gInSc1	vápník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
Vápník	vápník	k1gInSc1	vápník
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Vápník	vápník	k1gInSc1	vápník
|	|	kIx~	|
Calcium	Calcium	k1gNnSc1	Calcium
www.calcium.cz	www.calcium.cza	k1gFnPc2	www.calcium.cza
UK	UK	kA	UK
Food	Food	k1gInSc1	Food
Standards	Standards	k1gInSc1	Standards
Agency	Agenca	k1gMnSc2	Agenca
<g/>
:	:	kIx,	:
Calcium	Calcium	k1gNnSc1	Calcium
Nutrition	Nutrition	k1gInSc1	Nutrition
fact	fact	k1gMnSc1	fact
sheet	sheet	k1gMnSc1	sheet
from	from	k1gMnSc1	from
the	the	k?	the
National	National	k1gMnSc1	National
Institutes	Institutes	k1gMnSc1	Institutes
of	of	k?	of
Health	Health	k1gMnSc1	Health
</s>
