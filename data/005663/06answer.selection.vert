<s>
Tudorovci	Tudorovec	k1gMnPc1	Tudorovec
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
waleský	waleský	k2eAgInSc4d1	waleský
rod	rod	k1gInSc4	rod
Tewdur	Tewdur	k1gMnSc1	Tewdur
<g/>
,	,	kIx,	,
připomínaný	připomínaný	k2eAgMnSc1d1	připomínaný
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
anglickou	anglický	k2eAgFnSc7d1	anglická
královskou	královský	k2eAgFnSc7d1	královská
dynastií	dynastie	k1gFnSc7	dynastie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
<g/>
.	.	kIx.	.
</s>
