<s>
Tudorovci	Tudorovec	k1gMnPc1	Tudorovec
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
waleský	waleský	k2eAgInSc4d1	waleský
rod	rod	k1gInSc4	rod
Tewdur	Tewdur	k1gMnSc1	Tewdur
<g/>
,	,	kIx,	,
připomínaný	připomínaný	k2eAgMnSc1d1	připomínaný
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
anglickou	anglický	k2eAgFnSc7d1	anglická
královskou	královský	k2eAgFnSc7d1	královská
dynastií	dynastie	k1gFnSc7	dynastie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
hlavní	hlavní	k2eAgMnPc1d1	hlavní
panovníci	panovník	k1gMnPc1	panovník
dynastie	dynastie	k1gFnSc2	dynastie
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
–	–	k?	–
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
boji	boj	k1gInSc3	boj
rozvrácené	rozvrácený	k2eAgFnSc2d1	rozvrácená
Anglie	Anglie	k1gFnSc2	Anglie
stala	stát	k5eAaPmAgFnS	stát
renesanční	renesanční	k2eAgFnSc1d1	renesanční
mocnost	mocnost	k1gFnSc1	mocnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
staletích	staletí	k1gNnPc6	staletí
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
náboženské	náboženský	k2eAgFnSc3d1	náboženská
reformaci	reformace	k1gFnSc3	reformace
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
the	the	k?	the
Church	Church	k1gInSc1	Church
of	of	k?	of
England	England	k1gInSc1	England
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
odvíjel	odvíjet	k5eAaImAgInS	odvíjet
od	od	k7c2	od
Owena	Owen	k1gMnSc2	Owen
Tudora	tudor	k1gMnSc2	tudor
<g/>
,	,	kIx,	,
velšského	velšský	k2eAgMnSc2d1	velšský
hraběte	hrabě	k1gMnSc2	hrabě
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
</s>
<s>
Owen	Owen	k1gInSc1	Owen
Tudor	tudor	k1gInSc1	tudor
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
králi	král	k1gMnSc6	král
Jindřichu	Jindřich	k1gMnSc6	Jindřich
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Kateřinou	Kateřina	k1gFnSc7	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Tudor	tudor	k1gInSc1	tudor
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
Margaretu	Margareta	k1gFnSc4	Margareta
Beaufort	Beaufort	k1gInSc1	Beaufort
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Beaufortů	Beaufort	k1gInPc2	Beaufort
<g/>
.	.	kIx.	.
</s>
<s>
Margareta	Margareta	k1gFnSc1	Margareta
(	(	kIx(	(
<g/>
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
potomkem	potomek	k1gMnSc7	potomek
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
of	of	k?	of
Gaunt	Gaunt	k1gMnSc1	Gaunt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
rodu	rod	k1gInSc2	rod
Lancasterů	Lancaster	k1gInPc2	Lancaster
<g/>
.	.	kIx.	.
</s>
<s>
Margareta	Margareta	k1gFnSc1	Margareta
porodila	porodit	k5eAaPmAgFnS	porodit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
13	[number]	k4	13
let	let	k1gInSc1	let
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
manželově	manželův	k2eAgFnSc6d1	manželova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
dvou	dva	k4xCgFnPc2	dva
růží	růž	k1gFnPc2	růž
stalo	stát	k5eAaPmAgNnS	stát
anglickým	anglický	k2eAgMnSc7d1	anglický
panovníkem	panovník	k1gMnSc7	panovník
jako	jako	k8xC	jako
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
tudorovské	tudorovský	k2eAgNnSc1d1	tudorovské
období	období	k1gNnSc1	období
v	v	k7c6	v
anglických	anglický	k2eAgFnPc6d1	anglická
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
doba	doba	k1gFnSc1	doba
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
po	po	k7c4	po
konec	konec	k1gInSc4	konec
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1558	[number]	k4	1558
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
1603	[number]	k4	1603
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
od	od	k7c2	od
tudorovského	tudorovský	k2eAgNnSc2d1	tudorovské
období	období	k1gNnSc2	období
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
panovnice	panovnice	k1gFnSc2	panovnice
jako	jako	k8xS	jako
alžbětinská	alžbětinský	k2eAgFnSc1d1	Alžbětinská
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tudorovci	Tudorovec	k1gMnPc1	Tudorovec
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
anglickým	anglický	k2eAgFnPc3d1	anglická
královským	královský	k2eAgFnPc3d1	královská
dynastiím	dynastie	k1gFnPc3	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
přišel	přijít	k5eAaPmAgInS	přijít
Owen	Owen	k1gInSc1	Owen
Tudor	tudor	k1gInSc4	tudor
–	–	k?	–
asi	asi	k9	asi
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1425	[number]	k4	1425
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
sahá	sahat	k5eAaImIp3nS	sahat
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Erbovním	erbovní	k2eAgNnSc7d1	erbovní
znamením	znamení	k1gNnSc7	znamení
Owena	Owen	k1gMnSc2	Owen
Tudora	tudor	k1gMnSc2	tudor
–	–	k?	–
v	v	k7c6	v
kymerštině	kymerština	k1gFnSc6	kymerština
Owain	Owain	k1gMnSc1	Owain
ap	ap	kA	ap
Maredud	Maredud	k1gMnSc1	Maredud
ap	ap	kA	ap
Tudor	tudor	k1gInSc1	tudor
jarl	jarl	k1gMnSc1	jarl
Henevettet	Henevettet	k1gInSc1	Henevettet
–	–	k?	–
byl	být	k5eAaImAgInS	být
červený	červený	k2eAgInSc1d1	červený
štít	štít	k1gInSc1	štít
se	s	k7c7	s
stříbrnou	stříbrný	k2eAgFnSc7d1	stříbrná
krokví	krokev	k1gFnSc7	krokev
posetou	posetý	k2eAgFnSc4d1	posetá
hermelínem	hermelín	k1gInSc7	hermelín
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
stříbrnými	stříbrný	k2eAgFnPc7d1	stříbrná
přilbami	přilba	k1gFnPc7	přilba
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
varianty	variant	k1gInPc1	variant
znaku	znak	k1gInSc2	znak
byly	být	k5eAaImAgInP	být
poměrně	poměrně	k6eAd1	poměrně
rozmanité	rozmanitý	k2eAgInPc1d1	rozmanitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
dostaneme	dostat	k5eAaPmIp1nP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Owen	Owen	k1gInSc1	Owen
roku	rok	k1gInSc2	rok
1423	[number]	k4	1423
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
manželství	manželství	k1gNnSc4	manželství
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
anglickém	anglický	k2eAgInSc6d1	anglický
králi	král	k1gMnSc6	král
Jindřichovi	Jindřichův	k2eAgMnPc1d1	Jindřichův
V.	V.	kA	V.
(	(	kIx(	(
<g/>
†	†	k?	†
1422	[number]	k4	1422
<g/>
)	)	kIx)	)
Kateřinou	Kateřina	k1gFnSc7	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
skutkem	skutek	k1gInSc7	skutek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
příbuzným	příbuzný	k1gMnSc7	příbuzný
Lancasterů	Lancaster	k1gMnPc2	Lancaster
a	a	k8xC	a
zařadil	zařadit	k5eAaPmAgMnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnSc4d1	přední
anglickou	anglický	k2eAgFnSc4d1	anglická
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Anglii	Anglie	k1gFnSc4	Anglie
bohaté	bohatý	k2eAgFnSc2d1	bohatá
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
významných	významný	k2eAgFnPc2d1	významná
událostí	událost	k1gFnPc2	událost
–	–	k?	–
předně	předně	k6eAd1	předně
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
porážka	porážka	k1gFnSc1	porážka
ve	v	k7c6	v
stoleté	stoletý	k2eAgFnSc6d1	stoletá
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
vyhrotily	vyhrotit	k5eAaPmAgInP	vyhrotit
se	se	k3xPyFc4	se
mocenské	mocenský	k2eAgInPc1d1	mocenský
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgFnPc7d1	poslední
větvemi	větev	k1gFnPc7	větev
Plantagenetů	Plantagenet	k1gInPc2	Plantagenet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
starší	starý	k2eAgNnSc4d2	starší
odnoží	odnoží	k1gNnSc4	odnoží
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Lancasteru	Lancaster	k1gInSc2	Lancaster
(	(	kIx(	(
<g/>
odznak	odznak	k1gInSc1	odznak
červená	červený	k2eAgFnSc1d1	červená
růže	růže	k1gFnSc1	růže
<g/>
)	)	kIx)	)
a	a	k8xC	a
mladší	mladý	k2eAgMnPc1d2	mladší
větví	větvit	k5eAaImIp3nP	větvit
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
odznak	odznak	k1gInSc1	odznak
bílá	bílý	k2eAgFnSc1d1	bílá
růže	růže	k1gFnSc1	růže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
uvedených	uvedený	k2eAgFnPc2d1	uvedená
okolností	okolnost	k1gFnPc2	okolnost
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
války	válka	k1gFnPc1	válka
růží	růž	k1gFnPc2	růž
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1455	[number]	k4	1455
<g/>
–	–	k?	–
<g/>
1485	[number]	k4	1485
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
potulovaly	potulovat	k5eAaImAgInP	potulovat
zbytky	zbytek	k1gInPc1	zbytek
zubožené	zubožený	k2eAgFnSc2d1	zubožená
anglické	anglický	k2eAgFnSc2d1	anglická
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
vázl	váznout	k5eAaImAgMnS	váznout
obchod	obchod	k1gInSc4	obchod
apod.	apod.	kA	apod.
K	k	k7c3	k
nejednotě	nejednota	k1gFnSc3	nejednota
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
také	také	k9	také
přispěly	přispět	k5eAaPmAgInP	přispět
rozbroje	rozbroj	k1gInPc1	rozbroj
mezi	mezi	k7c7	mezi
nejmocnějšími	mocný	k2eAgInPc7d3	nejmocnější
aristokratickými	aristokratický	k2eAgInPc7d1	aristokratický
rody	rod	k1gInPc7	rod
–	–	k?	–
de	de	k?	de
la	la	k1gNnSc1	la
Pole	pole	k1gFnSc1	pole
vévody	vévoda	k1gMnSc2	vévoda
ze	z	k7c2	z
Suffolku	Suffolek	k1gInSc2	Suffolek
<g/>
,	,	kIx,	,
Mortimery	Mortimera	k1gFnSc2	Mortimera
hrabaty	hrabě	k1gNnPc7	hrabě
z	z	k7c2	z
Marchu	March	k1gInSc2	March
<g/>
,	,	kIx,	,
Beaufoty	Beaufota	k1gFnPc1	Beaufota
vévody	vévoda	k1gMnSc2	vévoda
ze	z	k7c2	z
Somersetu	Somerset	k1gInSc2	Somerset
<g/>
,	,	kIx,	,
rodem	rod	k1gInSc7	rod
de	de	k?	de
Neville	Neville	k1gFnSc2	Neville
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zdědili	zdědit	k5eAaPmAgMnP	zdědit
titul	titul	k1gInSc4	titul
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Warwicku	Warwick	k1gInSc2	Warwick
<g/>
,	,	kIx,	,
Courtenayů	Courtenay	k1gInPc2	Courtenay
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Devonu	devon	k1gInSc2	devon
nebo	nebo	k8xC	nebo
Percyů	Percy	k1gInPc2	Percy
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Northhumberlandu	Northhumberland	k1gInSc2	Northhumberland
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
bohatství	bohatství	k1gNnSc4	bohatství
původně	původně	k6eAd1	původně
drobné	drobný	k2eAgFnSc2d1	drobná
venkovské	venkovský	k2eAgFnSc2d1	venkovská
šlechty	šlechta	k1gFnSc2	šlechta
Woodvillů	Woodvill	k1gMnPc2	Woodvill
–	–	k?	–
Alžběta	Alžběta	k1gFnSc1	Alžběta
Woodvillová	Woodvillová	k1gFnSc1	Woodvillová
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
manželkou	manželka	k1gFnSc7	manželka
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Richard	Richard	k1gMnSc1	Richard
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1450	[number]	k4	1450
dekorován	dekorován	k2eAgInSc1d1	dekorován
Podvazkovým	podvazkový	k2eAgInSc7d1	podvazkový
řádem	řád	k1gInSc7	řád
a	a	k8xC	a
jmenován	jmenován	k2eAgInSc1d1	jmenován
lordem	lord	k1gMnSc7	lord
Riversem	Rivers	k1gMnSc7	Rivers
<g/>
.	.	kIx.	.
</s>
<s>
Owen	Owen	k1gInSc1	Owen
Tudor	tudor	k1gInSc1	tudor
se	se	k3xPyFc4	se
logicky	logicky	k6eAd1	logicky
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
růží	růž	k1gFnPc2	růž
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Lancasterů	Lancaster	k1gInPc2	Lancaster
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1461	[number]	k4	1461
však	však	k9	však
padl	padnout	k5eAaPmAgInS	padnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
Yorků	York	k1gInPc2	York
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
sehráli	sehrát	k5eAaPmAgMnP	sehrát
až	až	k9	až
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Richmondu	Richmond	k1gInSc2	Richmond
(	(	kIx(	(
<g/>
jemu	on	k3xPp3gInSc3	on
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
připisován	připisovat	k5eAaImNgInS	připisovat
anglický	anglický	k2eAgInSc1d1	anglický
štít	štít	k1gInSc1	štít
–	–	k?	–
pole	pole	k1gNnSc1	pole
1	[number]	k4	1
a	a	k8xC	a
4	[number]	k4	4
modré	modrý	k2eAgFnSc2d1	modrá
poseté	posetý	k2eAgInPc1d1	posetý
zlatými	zlatý	k2eAgFnPc7d1	zlatá
liliemi	lilie	k1gFnPc7	lilie
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
červené	červená	k1gFnPc4	červená
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
zlatými	zlatý	k2eAgFnPc7d1	zlatá
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
kráčejícími	kráčející	k2eAgInPc7d1	kráčející
lvy	lev	k1gInPc7	lev
a	a	k8xC	a
modrým	modrý	k2eAgInSc7d1	modrý
lemem	lem	k1gInSc7	lem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
lilie	lilie	k1gFnPc4	lilie
a	a	k8xC	a
merlety	merlet	k1gInPc4	merlet
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
Beaufortovou	Beaufortová	k1gFnSc7	Beaufortová
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
otcem	otec	k1gMnSc7	otec
prvního	první	k4xOgMnSc2	první
Tudora	tudor	k1gMnSc2	tudor
na	na	k7c6	na
anglickém	anglický	k2eAgInSc6d1	anglický
trůně	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Jindřicha	Jindřich	k1gMnSc4	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1485	[number]	k4	1485
<g/>
–	–	k?	–
<g/>
1509	[number]	k4	1509
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jasper	Jasper	k1gMnSc1	Jasper
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroku	Pembrok	k1gInSc2	Pembrok
(	(	kIx(	(
<g/>
erb	erb	k1gInSc1	erb
–	–	k?	–
štít	štít	k1gInSc1	štít
modro-červeně	modro-červeně	k6eAd1	modro-červeně
polcený	polcený	k2eAgInSc1d1	polcený
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
stříbrnými	stříbrný	k2eAgInPc7d1	stříbrný
lvy	lev	k1gInPc7	lev
–	–	k?	–
2,1	[number]	k4	2,1
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
velkým	velký	k2eAgMnSc7d1	velký
ochráncem	ochránce	k1gMnSc7	ochránce
a	a	k8xC	a
podporovatelem	podporovatel	k1gMnSc7	podporovatel
synovce	synovec	k1gMnSc2	synovec
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zmítala	zmítat	k5eAaImAgFnS	zmítat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Mezníkem	mezník	k1gInSc7	mezník
byla	být	k5eAaImAgFnS	být
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bosworthském	Bosworthský	k2eAgNnSc6d1	Bosworthský
poli	pole	k1gNnSc6	pole
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Tudorovci	Tudorovec	k1gMnSc3	Tudorovec
poražen	poražen	k2eAgMnSc1d1	poražen
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Yorků	York	k1gInPc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
situaci	situace	k1gFnSc3	situace
pojistil	pojistit	k5eAaPmAgInS	pojistit
ženitbou	ženitba	k1gFnSc7	ženitba
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
z	z	k7c2	z
Yorku	York	k1gInSc2	York
a	a	k8xC	a
jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
tudorovským	tudorovský	k2eAgMnSc7d1	tudorovský
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
mocenských	mocenský	k2eAgFnPc2d1	mocenská
bitev	bitva	k1gFnPc2	bitva
a	a	k8xC	a
sporů	spor	k1gInPc2	spor
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
doslovnému	doslovný	k2eAgNnSc3d1	doslovné
vyvraždění	vyvraždění	k1gNnSc3	vyvraždění
staré	starý	k2eAgFnSc2d1	stará
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
panování	panování	k1gNnSc4	panování
Plantagenetů	Plantagenet	k1gInPc2	Plantagenet
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
pozici	pozice	k1gFnSc4	pozice
získaly	získat	k5eAaPmAgInP	získat
rody	rod	k1gInPc1	rod
Howardů	Howard	k1gMnPc2	Howard
<g/>
,	,	kIx,	,
Stanleyů	Stanley	k1gMnPc2	Stanley
<g/>
,	,	kIx,	,
Arundelů	Arundel	k1gMnPc2	Arundel
a	a	k8xC	a
nižší	nízký	k2eAgFnSc1d2	nižší
venkovská	venkovský	k2eAgFnSc1d1	venkovská
šlechta	šlechta	k1gFnSc1	šlechta
–	–	k?	–
gentry	gentr	k1gInPc1	gentr
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
Tudorovcem	Tudorovec	k1gMnSc7	Tudorovec
na	na	k7c6	na
anglickém	anglický	k2eAgInSc6d1	anglický
trůně	trůn	k1gInSc6	trůn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známý	známý	k2eAgMnSc1d1	známý
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1509	[number]	k4	1509
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
šestkrát	šestkrát	k6eAd1	šestkrát
ženat	ženat	k2eAgInSc1d1	ženat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Španělka	Španělka	k1gFnSc1	Španělka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Aragonská	aragonský	k2eAgFnSc1d1	Aragonská
<g/>
;	;	kIx,	;
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
rozvést	rozvést	k5eAaPmF	rozvést
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
anglikánskou	anglikánský	k2eAgFnSc4d1	anglikánská
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
papež	papež	k1gMnSc1	papež
uvalil	uvalit	k5eAaPmAgMnS	uvalit
i	i	k9	i
klatbu	klatba	k1gFnSc4	klatba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
měl	mít	k5eAaImAgInS	mít
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
anglickou	anglický	k2eAgFnSc7d1	anglická
šlechtičnou	šlechtična	k1gFnSc7	šlechtična
Annou	Anna	k1gFnSc7	Anna
Boleynovou	Boleynová	k1gFnSc7	Boleynová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
dceru	dcera	k1gFnSc4	dcera
Alžbětu	Alžběta	k1gFnSc4	Alžběta
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
narodil	narodit	k5eAaPmAgMnS	narodit
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Annu	Anna	k1gFnSc4	Anna
za	za	k7c4	za
intrikánku	intrikánka	k1gFnSc4	intrikánka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1536	[number]	k4	1536
ji	on	k3xPp3gFnSc4	on
nechal	nechat	k5eAaPmAgInS	nechat
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Jana	Jana	k1gFnSc1	Jana
Seymourová	Seymourový	k2eAgFnSc1d1	Seymourová
–	–	k?	–
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
zanechala	zanechat	k5eAaPmAgFnS	zanechat
následníka	následník	k1gMnSc4	následník
Eduarda	Eduard	k1gMnSc4	Eduard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sama	sám	k3xTgFnSc1	sám
těžký	těžký	k2eAgInSc4d1	těžký
porod	porod	k1gInSc4	porod
nepřežila	přežít	k5eNaPmAgFnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
ženou	žena	k1gFnSc7	žena
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
protestantská	protestantský	k2eAgFnSc1d1	protestantská
šlechtična	šlechtična	k1gFnSc1	šlechtična
Anna	Anna	k1gFnSc1	Anna
Klevská	Klevský	k2eAgFnSc1d1	Klevská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
toto	tento	k3xDgNnSc1	tento
manželství	manželství	k1gNnSc1	manželství
skončilo	skončit	k5eAaPmAgNnS	skončit
rozvodem	rozvod	k1gInSc7	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Pátou	pátý	k4xOgFnSc7	pátý
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Kateřina	Kateřina	k1gFnSc1	Kateřina
Howardová	Howardová	k1gFnSc1	Howardová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
skončila	skončit	k5eAaPmAgFnS	skončit
pro	pro	k7c4	pro
údajné	údajný	k2eAgNnSc4d1	údajné
cizoložství	cizoložství	k1gNnSc4	cizoložství
na	na	k7c6	na
popravišti	popraviště	k1gNnSc6	popraviště
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
manželkou	manželka	k1gFnSc7	manželka
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
Kateřina	Kateřina	k1gFnSc1	Kateřina
Parrová	Parrová	k1gFnSc1	Parrová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svého	svůj	k3xOyFgMnSc4	svůj
hrubého	hrubý	k2eAgMnSc4d1	hrubý
chotě	choť	k1gMnSc4	choť
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
projevil	projevit	k5eAaPmAgInS	projevit
svoji	svůj	k3xOyFgFnSc4	svůj
anglosaskou	anglosaský	k2eAgFnSc4d1	anglosaská
krutost	krutost	k1gFnSc4	krutost
při	při	k7c6	při
popravách	poprava	k1gFnPc6	poprava
odpůrců	odpůrce	k1gMnPc2	odpůrce
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
obětí	oběť	k1gFnPc2	oběť
těchto	tento	k3xDgFnPc2	tento
poprav	poprava	k1gFnPc2	poprava
byl	být	k5eAaImAgMnS	být
sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
More	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
lord	lord	k1gMnSc1	lord
kancléř	kancléř	k1gMnSc1	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Následujícím	následující	k2eAgMnSc7d1	následující
Tudorovcem	Tudorovec	k1gMnSc7	Tudorovec
byl	být	k5eAaImAgMnS	být
mladičký	mladičký	k2eAgMnSc1d1	mladičký
a	a	k8xC	a
churavý	churavý	k2eAgMnSc1d1	churavý
Eduard	Eduard	k1gMnSc1	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1537	[number]	k4	1537
<g/>
–	–	k?	–
<g/>
1553	[number]	k4	1553
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
svojí	svůj	k3xOyFgFnSc7	svůj
nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
praneteř	praneteř	k1gFnSc4	praneteř
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
sedmnáctiletou	sedmnáctiletý	k2eAgFnSc4d1	sedmnáctiletá
Janu	Jana	k1gFnSc4	Jana
Greyovou	Greyová	k1gFnSc4	Greyová
–	–	k?	–
devítidenní	devítidenní	k2eAgFnSc1d1	devítidenní
královna	královna	k1gFnSc1	královna
–	–	k?	–
pozdější	pozdní	k2eAgFnSc7d2	pozdější
královnou	královna	k1gFnSc7	královna
Marií	Maria	k1gFnPc2	Maria
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
zrádkyni	zrádkyně	k1gFnSc4	zrádkyně
a	a	k8xC	a
bez	bez	k7c2	bez
milosti	milost	k1gFnSc2	milost
popravena	popravit	k5eAaPmNgFnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
panovaly	panovat	k5eAaImAgFnP	panovat
nevlastní	vlastní	k2eNgFnPc1d1	nevlastní
sestry	sestra	k1gFnPc1	sestra
Eduarda	Eduard	k1gMnSc2	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
–	–	k?	–
citovaná	citovaný	k2eAgFnSc1d1	citovaná
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1553	[number]	k4	1553
<g/>
–	–	k?	–
<g/>
1558	[number]	k4	1558
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Aragonské	aragonský	k2eAgFnSc2d1	Aragonská
<g/>
,	,	kIx,	,
vnutila	vnutit	k5eAaPmAgFnS	vnutit
zemi	zem	k1gFnSc3	zem
opět	opět	k5eAaPmF	opět
katolictví	katolictví	k1gNnSc4	katolictví
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
např.	např.	kA	např.
během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
dne	den	k1gInSc2	den
upálit	upálit	k5eAaPmF	upálit
300	[number]	k4	300
stoupenců	stoupenec	k1gMnPc2	stoupenec
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
ten	ten	k3xDgInSc4	ten
přídomek	přídomek	k1gInSc4	přídomek
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
Tudorovcem	Tudorovec	k1gMnSc7	Tudorovec
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uklidnění	uklidnění	k1gNnSc3	uklidnění
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
renesanční	renesanční	k2eAgFnSc2d1	renesanční
společnosti	společnost	k1gFnSc2	společnost
–	–	k?	–
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
nastolení	nastolení	k1gNnSc3	nastolení
anglikánského	anglikánský	k2eAgNnSc2d1	anglikánské
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
rozvoji	rozvoj	k1gInSc6	rozvoj
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
divadla	divadlo	k1gNnSc2	divadlo
–	–	k?	–
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozvoji	rozvoj	k1gInSc3	rozvoj
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
a	a	k8xC	a
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Ameriky	Amerika	k1gFnSc2	Amerika
–	–	k?	–
díky	díky	k7c3	díky
admirálovi	admirál	k1gMnSc3	admirál
siru	sir	k1gMnSc3	sir
Francisi	Francis	k1gMnSc3	Francis
Drakeovi	Drakeus	k1gMnSc3	Drakeus
oslabila	oslabit	k5eAaPmAgFnS	oslabit
vliv	vliv	k1gInSc4	vliv
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
vůči	vůči	k7c3	vůči
svým	svůj	k3xOyFgMnPc3	svůj
rivalům	rival	k1gMnPc3	rival
dovedla	dovést	k5eAaPmAgFnS	dovést
být	být	k5eAaImF	být
nemilosrdná	milosrdný	k2eNgFnSc1d1	nemilosrdná
jako	jako	k8xC	jako
jiní	jiný	k2eAgMnPc1d1	jiný
Tudorovci	Tudorovec	k1gMnPc1	Tudorovec
<g/>
.	.	kIx.	.
</s>
<s>
Nesmlouvavá	smlouvavý	k2eNgFnSc1d1	nesmlouvavá
byla	být	k5eAaImAgFnS	být
vůči	vůči	k7c3	vůči
své	svůj	k3xOyFgFnSc3	svůj
skotské	skotský	k2eAgFnSc3d1	skotská
sokyni	sokyně	k1gFnSc3	sokyně
Marii	Maria	k1gFnSc3	Maria
Stuartovně	Stuartovna	k1gFnSc3	Stuartovna
–	–	k?	–
předně	předně	k6eAd1	předně
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
katolička	katolička	k1gFnSc1	katolička
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
vévodů	vévoda	k1gMnPc2	vévoda
de	de	k?	de
Guise	Guise	k1gFnSc2	Guise
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
skotskou	skotský	k2eAgFnSc7d1	skotská
královnou	královna	k1gFnSc7	královna
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
dědice	dědic	k1gMnSc4	dědic
a	a	k8xC	a
s	s	k7c7	s
Tudorovci	Tudorovec	k1gMnPc7	Tudorovec
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
příbuzenském	příbuzenský	k2eAgInSc6d1	příbuzenský
poměru	poměr	k1gInSc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neprovdala	provdat	k5eNaPmAgFnS	provdat
–	–	k?	–
není	být	k5eNaImIp3nS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
snažila	snažit	k5eAaImAgFnS	snažit
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
anglické	anglický	k2eAgFnSc2d1	anglická
tradice	tradice	k1gFnSc2	tradice
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
obviněna	obvinit	k5eAaPmNgFnS	obvinit
ze	z	k7c2	z
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
cizoložství	cizoložství	k1gNnSc2	cizoložství
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
poprava	poprava	k1gFnSc1	poprava
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Alžběty	Alžběta	k1gFnSc2	Alžběta
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
nakonec	nakonec	k6eAd1	nakonec
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
syn	syn	k1gMnSc1	syn
Marie	Maria	k1gFnSc2	Maria
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
Stuart	Stuart	k1gInSc1	Stuart
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
krátká	krátký	k2eAgFnSc1d1	krátká
a	a	k8xC	a
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
i	i	k9	i
drsná	drsný	k2eAgFnSc1d1	drsná
<g/>
.	.	kIx.	.
</s>
<s>
Ukončili	ukončit	k5eAaPmAgMnP	ukončit
války	válka	k1gFnSc2	válka
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
potlačili	potlačit	k5eAaPmAgMnP	potlačit
povstání	povstání	k1gNnSc3	povstání
posledních	poslední	k2eAgMnPc2d1	poslední
stoupenců	stoupenec	k1gMnPc2	stoupenec
Plantagenetů	Plantagenet	k1gInPc2	Plantagenet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vedli	vést	k5eAaImAgMnP	vést
lordi	lord	k1gMnPc1	lord
Lovell	Lovell	k1gMnSc1	Lovell
a	a	k8xC	a
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dění	dění	k1gNnSc2	dění
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
a	a	k8xC	a
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
<g/>
.	.	kIx.	.
</s>
<s>
Určitým	určitý	k2eAgInSc7d1	určitý
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
prvkem	prvek	k1gInSc7	prvek
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gNnSc4	jejich
anglosaský	anglosaský	k2eAgInSc1d1	anglosaský
původ	původ	k1gInSc1	původ
<g/>
;	;	kIx,	;
Plantageneti	Plantagenet	k1gMnPc1	Plantagenet
totiž	totiž	k9	totiž
byli	být	k5eAaImAgMnP	být
cizinci	cizinec	k1gMnPc1	cizinec
–	–	k?	–
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
velmi	velmi	k6eAd1	velmi
starý	starý	k2eAgInSc1d1	starý
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
je	být	k5eAaImIp3nS	být
připomínán	připomínán	k2eAgMnSc1d1	připomínán
waleský	waleský	k2eAgMnSc1d1	waleský
kníže	kníže	k1gMnSc1	kníže
Owain	Owain	k1gMnSc1	Owain
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
pohledu	pohled	k1gInSc6	pohled
lze	lze	k6eAd1	lze
panování	panování	k1gNnSc1	panování
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
porovnat	porovnat	k5eAaPmF	porovnat
s	s	k7c7	s
absolutismem	absolutismus	k1gInSc7	absolutismus
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Možná	možná	k9	možná
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
škodu	škoda	k1gFnSc4	škoda
ještě	ještě	k6eAd1	ještě
uvést	uvést	k5eAaPmF	uvést
jméno	jméno	k1gNnSc4	jméno
Wallis	Wallis	k1gFnSc2	Wallis
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Wallis-Cromwellové	Wallis-Cromwellové	k2eAgFnSc1d1	Wallis-Cromwellové
<g/>
:	:	kIx,	:
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
původ	původ	k1gMnSc1	původ
lord	lord	k1gMnSc1	lord
protektor	protektor	k1gMnSc1	protektor
Oliver	Oliver	k1gMnSc1	Oliver
Cromwell	Cromwell	k1gMnSc1	Cromwell
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
rovněž	rovněž	k9	rovněž
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
připomínali	připomínat	k5eAaImAgMnP	připomínat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudorem	tudor	k1gInSc7	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
pocházelo	pocházet	k5eAaImAgNnS	pocházet
šest	šest	k4xCc1	šest
anglických	anglický	k2eAgMnPc2d1	anglický
králů	král	k1gMnPc2	král
a	a	k8xC	a
královen	královna	k1gFnPc2	královna
<g/>
:	:	kIx,	:
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
(	(	kIx(	(
<g/>
1485	[number]	k4	1485
<g/>
–	–	k?	–
<g/>
1509	[number]	k4	1509
<g/>
)	)	kIx)	)
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
(	(	kIx(	(
<g/>
1509	[number]	k4	1509
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1547	[number]	k4	1547
<g/>
–	–	k?	–
<g/>
1553	[number]	k4	1553
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
Greyová	Greyová	k1gFnSc1	Greyová
(	(	kIx(	(
<g/>
1553	[number]	k4	1553
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravnučka	pravnučka	k1gFnSc1	pravnučka
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vládla	vládnout	k5eAaImAgFnS	vládnout
9	[number]	k4	9
dnů	den	k1gInPc2	den
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
Tudorovna	Tudorovna	k1gFnSc1	Tudorovna
řečená	řečený	k2eAgFnSc1d1	řečená
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
(	(	kIx(	(
<g/>
1553	[number]	k4	1553
<g/>
-	-	kIx~	-
<g/>
1558	[number]	k4	1558
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
Tudorovna	Tudorovna	k1gFnSc1	Tudorovna
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tudorovci	Tudorovec	k1gMnPc1	Tudorovec
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
Anglie	Anglie	k1gFnSc2	Anglie
Seznam	seznam	k1gInSc1	seznam
panovníků	panovník	k1gMnPc2	panovník
Irska	Irsko	k1gNnSc2	Irsko
Dějiny	dějiny	k1gFnPc1	dějiny
Irska	Irsko	k1gNnSc2	Irsko
NEILLANDS	NEILLANDS	kA	NEILLANDS
<g/>
,	,	kIx,	,
Robin	robin	k2eAgInSc1d1	robin
<g/>
.	.	kIx.	.
</s>
<s>
Války	válek	k1gInPc1	válek
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
620	[number]	k4	620
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
MAUROIS	MAUROIS	kA	MAUROIS
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
BUBEN	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
heraldiky	heraldika	k1gFnSc2	heraldika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
135	[number]	k4	135
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
HONZÁK	HONZÁK	kA	HONZÁK
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
