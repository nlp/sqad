<s>
Diaspora	diaspora	k1gFnSc1
(	(	kIx(
<g/>
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
DiasporaAutor	DiasporaAutor	k1gMnSc1
</s>
<s>
Greg	Greg	k1gMnSc1
Egan	Egan	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Diaspora	diaspora	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
sci-fi	sci-fi	k1gFnSc1
román	román	k1gInSc4
Datum	datum	k1gInSc1
vydání	vydání	k1gNnSc1
</s>
<s>
září	zářit	k5eAaImIp3nS
1997	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Diaspora	diaspora	k1gFnSc1
je	být	k5eAaImIp3nS
sci-fi	sci-fi	k1gFnSc1
román	román	k1gInSc1
Grega	Grega	k1gFnSc1
Egana	Egan	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
do	do	k7c2
češtiny	čeština	k1gFnSc2
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
originálu	originál	k1gInSc2
Diaspora	diaspora	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
přeložen	přeložen	k2eAgInSc1d1
roku	rok	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
nakladatelství	nakladatelství	k1gNnSc6
Návrat	návrat	k1gInSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kotrle	Kotrle	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7174	#num#	k4
<g/>
-	-	kIx~
<g/>
607	#num#	k4
<g/>
-X	-X	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
Diaspory	diaspora	k1gFnSc2
je	být	k5eAaImIp3nS
situován	situovat	k5eAaBmNgInS
do	do	k7c2
budoucnosti	budoucnost	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
lidstvo	lidstvo	k1gNnSc1
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
konzervativní	konzervativní	k2eAgMnPc4d1
tělaře	tělař	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
i	i	k9
přes	přes	k7c4
nevýhody	nevýhoda	k1gFnPc4
z	z	k7c2
toho	ten	k3xDgNnSc2
plynoucí	plynoucí	k2eAgMnSc1d1
zůstávají	zůstávat	k5eAaImIp3nP
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
organických	organický	k2eAgNnPc6d1
tělech	tělo	k1gNnPc6
(	(	kIx(
<g/>
byť	byť	k8xS
mnohdy	mnohdy	k6eAd1
značně	značně	k6eAd1
geneticky	geneticky	k6eAd1
modifikovaných	modifikovaný	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
Gleisnerovy	Gleisnerův	k2eAgFnPc4d1
roboty	robota	k1gFnPc4
–	–	k?
umělá	umělý	k2eAgNnPc4d1
fyzická	fyzický	k2eAgNnPc4d1
těla	tělo	k1gNnPc4
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
a	a	k8xC
obyvatele	obyvatel	k1gMnSc4
polis	polis	k1gFnSc2
–	–	k?
superpočítačů	superpočítač	k1gInPc2
obývaných	obývaný	k2eAgInPc2d1
inteligentním	inteligentní	k2eAgInSc7d1
software	software	k1gInSc1
–	–	k?
bytostmi	bytost	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
žijí	žít	k5eAaImIp3nP
již	již	k6eAd1
plně	plně	k6eAd1
ve	v	k7c6
virtuální	virtuální	k2eAgFnSc6d1
realitě	realita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Román	román	k1gInSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
právě	právě	k9
na	na	k7c4
obyvatele	obyvatel	k1gMnPc4
polis	polis	k1gFnSc4
a	a	k8xC
na	na	k7c4
nesčetné	sčetný	k2eNgFnPc4d1
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
jim	on	k3xPp3gFnPc3
otevřeny	otevřen	k2eAgInPc1d1
bez	bez	k7c2
omezení	omezení	k1gNnSc2
fyzického	fyzický	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Postavy	postava	k1gFnPc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Játima	Játima	k1gFnSc1
je	být	k5eAaImIp3nS
Sirotče	Sirotec	k1gMnSc5
vytvořené	vytvořený	k2eAgFnSc3d1
v	v	k7c6
polis	polis	k1gFnSc6
Koniši	Koniše	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgMnPc4
rodiče	rodič	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ústřední	ústřední	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
románu	román	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
se	se	k3xPyFc4
zajímá	zajímat	k5eAaImIp3nS
o	o	k7c4
matematiku	matematika	k1gFnSc4
a	a	k8xC
objevování	objevování	k1gNnSc4
neznáma	neznámo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Blanche	Blanche	k1gFnSc1
je	být	k5eAaImIp3nS
dalším	další	k2eAgMnSc7d1
obyvatelem	obyvatel	k1gMnSc7
polis	polis	k1gFnSc2
Koniši	Koniše	k1gFnSc3
–	–	k?
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
prvních	první	k4xOgInPc2
tří	tři	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
Játima	Játima	k1gNnSc1
potká	potkat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blanche	Blanche	k1gNnSc1
později	pozdě	k6eAd2
nalézá	nalézat	k5eAaImIp3nS
zálibu	záliba	k1gFnSc4
v	v	k7c6
teorii	teorie	k1gFnSc6
Kozuchové	Kozuchový	k2eAgFnSc6d1
(	(	kIx(
<g/>
fiktivní	fiktivní	k2eAgFnSc6d1
jednotné	jednotný	k2eAgFnSc6d1
polní	polní	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
sestavené	sestavený	k2eAgFnPc1d1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
románu	román	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
učiní	učinit	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
podstatné	podstatný	k2eAgInPc1d1
objevy	objev	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Orlando	Orlando	k6eAd1
je	být	k5eAaImIp3nS
tělař	tělař	k1gMnSc1
(	(	kIx(
<g/>
člověk	člověk	k1gMnSc1
v	v	k7c6
téměř	téměř	k6eAd1
nezměněné	změněný	k2eNgFnSc6d1
podobě	podoba	k1gFnSc6
v	v	k7c6
organickém	organický	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
<g/>
)	)	kIx)
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
troskách	troska	k1gFnPc6
Atlanty	Atlanta	k1gFnSc2
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orlando	Orlanda	k1gFnSc5
posléze	posléze	k6eAd1
„	„	k?
<g/>
migruje	migrovat	k5eAaImIp3nS
<g/>
“	“	k?
do	do	k7c2
polis	polis	k1gFnSc2
pomocí	pomocí	k7c2
sofistikované	sofistikovaný	k2eAgFnSc2d1
nanotechnologie	nanotechnologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
Diaspora	diaspora	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
popisem	popis	k1gInSc7
„	„	k?
<g/>
orphanogeneze	orphanogeneze	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
následným	následný	k2eAgInSc7d1
vznikem	vznik	k1gInSc7
Játimy	Játima	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
moderních	moderní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
je	být	k5eAaImIp3nS
Játima	Játima	k1gNnSc4
již	již	k6eAd1
„	„	k?
<g/>
dospělý	dospělý	k1gMnSc1
<g/>
“	“	k?
během	během	k7c2
několika	několik	k4yIc2
dní	den	k1gInPc2
–	–	k?
polis	polis	k1gInSc1
běží	běžet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
osmsetkrát	osmsetkrát	k6eAd1
rychleji	rychle	k6eAd2
(	(	kIx(
<g/>
měřeno	měřit	k5eAaImNgNnS
subjektivním	subjektivní	k2eAgInSc7d1
časem	čas	k1gInSc7
jeho	jeho	k3xOp3gMnPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
než	než	k8xS
fyzický	fyzický	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
Játima	Játima	k1gNnSc4
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
přítelem	přítel	k1gMnSc7
Inoširem	Inošir	k1gMnSc7
navštěvují	navštěvovat	k5eAaImIp3nP
kolonii	kolonie	k1gFnSc4
tělařů	tělař	k1gMnPc2
v	v	k7c6
Atlantě	Atlanta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
Gleisnerův	Gleisnerův	k2eAgMnSc1d1
robot	robot	k1gMnSc1
Karpal	Karpal	k1gMnSc1
pomocí	pomocí	k7c2
detektoru	detektor	k1gInSc2
gravitačních	gravitační	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
zjišťuje	zjišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
neutronová	neutronový	k2eAgFnSc1d1
dvojhvězda	dvojhvězda	k1gFnSc1
(	(	kIx(
<g/>
Lacerta	Lacerta	k1gFnSc1
G-	G-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
ztrácí	ztrácet	k5eAaImIp3nS
energii	energie	k1gFnSc4
mnohem	mnohem	k6eAd1
rychleji	rychle	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
očekávalo	očekávat	k5eAaImAgNnS
a	a	k8xC
namísto	namísto	k7c2
předpokládaných	předpokládaný	k2eAgInPc2d1
osmi	osm	k4xCc2
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
zbývají	zbývat	k5eAaImIp3nP
do	do	k7c2
srážky	srážka	k1gFnSc2
neutronových	neutronový	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
jen	jen	k9
4	#num#	k4
dny	den	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Játima	Játima	k1gNnSc1
a	a	k8xC
Inoširo	Inoširo	k1gNnSc1
se	se	k3xPyFc4
vracejí	vracet	k5eAaImIp3nP
do	do	k7c2
Atlanty	Atlanta	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
varovali	varovat	k5eAaImAgMnP
tělaře	tělař	k1gMnSc4
aby	aby	kYmCp3nP
buďto	buďto	k8xC
migrovali	migrovat	k5eAaImAgMnP
do	do	k7c2
polis	polis	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
alespoň	alespoň	k9
připravili	připravit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiace	radiace	k1gFnSc1
z	z	k7c2
výsledného	výsledný	k2eAgNnSc2d1
gama	gama	k1gNnSc2
záblesku	záblesk	k1gInSc2
výrazně	výrazně	k6eAd1
poškodí	poškodit	k5eAaPmIp3nS
atmosféru	atmosféra	k1gFnSc4
a	a	k8xC
druh	druh	k1gInSc4
homo	homo	k6eAd1
sapiens	sapiens	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
organické	organický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
zaniká	zanikat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Játima	Játima	k1gNnSc1
a	a	k8xC
Inoširo	Inoširo	k1gNnSc1
stíhají	stíhat	k5eAaImIp3nP
několik	několik	k4yIc4
náhodných	náhodný	k2eAgMnPc2d1
tělařů	tělař	k1gMnPc2
zachránit	zachránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Román	Román	k1gMnSc1
je	být	k5eAaImIp3nS
pojmenován	pojmenovat	k5eAaPmNgMnS
podle	podle	k7c2
mise	mise	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
podstupuje	podstupovat	k5eAaImIp3nS
většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
polis	polis	k1gFnPc2
Carter-Zimmermanu	Carter-Zimmerman	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diaspora	diaspora	k1gFnSc1
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc4
stovek	stovka	k1gFnPc2
klonů	klon	k1gInPc2
polis	polis	k1gFnPc2
C-Z	C-Z	k1gFnSc7
vypuštěných	vypuštěný	k2eAgInPc2d1
k	k	k7c3
rozličným	rozličný	k2eAgInPc3d1
hvězdným	hvězdný	k2eAgInPc3d1
systémům	systém	k1gInPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
hledaly	hledat	k5eAaImAgFnP
inteligentní	inteligentní	k2eAgFnPc1d1
civilizace	civilizace	k1gFnPc1
převážně	převážně	k6eAd1
ve	v	k7c6
snaze	snaha	k1gFnSc6
vysvětlit	vysvětlit	k5eAaPmF
příčinu	příčina	k1gFnSc4
rychlého	rychlý	k2eAgInSc2d1
kolapsu	kolaps	k1gInSc2
Lacerty	Lacert	k1gInPc4
a	a	k8xC
předejít	předejít	k5eAaPmF
tak	tak	k8xS,k8xC
možným	možný	k2eAgNnSc7d1
ohrožením	ohrožení	k1gNnSc7
jejich	jejich	k3xOp3gFnSc2
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
C-Z	C-Z	k1gFnPc2
zahájí	zahájit	k5eAaPmIp3nP
Diasporu	diaspora	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
selže	selhat	k5eAaPmIp3nS
jejich	jejich	k3xOp3gInSc4
pokus	pokus	k1gInSc4
propojit	propojit	k5eAaPmF
Galaxii	galaxie	k1gFnSc4
sítí	síť	k1gFnPc2
červích	červí	k2eAgFnPc2d1
děr	děra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
cesty	cesta	k1gFnSc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
klonu	klon	k1gInSc6
C-Z	C-Z	k1gFnSc3
mířícím	mířící	k2eAgInSc6d1
k	k	k7c3
Formalhautu	Formalhaut	k1gInSc3
objeví	objevit	k5eAaPmIp3nS
Blanche	Blanche	k1gNnSc1
rozšíření	rozšíření	k1gNnSc2
stávající	stávající	k2eAgFnSc2d1
jednotné	jednotný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vysvětlí	vysvětlit	k5eAaPmIp3nP
předešlé	předešlý	k2eAgInPc4d1
špatně	špatně	k6eAd1
vyšlé	vyšlý	k2eAgInPc4d1
experimenty	experiment	k1gInPc4
a	a	k8xC
špatnou	špatný	k2eAgFnSc4d1
průchodnost	průchodnost	k1gFnSc4
červích	červí	k2eAgFnPc2d1
děr	děra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
Blanche	Blanche	k1gInSc1
odesílá	odesílat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
teorii	teorie	k1gFnSc4
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
formalhautské	formalhautský	k2eAgFnPc4d1
C-Z	C-Z	k1gFnPc4
zničeno	zničen	k2eAgNnSc1d1
kosmickým	kosmický	k2eAgNnSc7d1
smetím	smetí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Posléze	posléze	k6eAd1
účastníci	účastník	k1gMnPc1
Diaspory	diaspora	k1gFnSc2
objeví	objevit	k5eAaPmIp3nP
civilizaci	civilizace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zakódovala	zakódovat	k5eAaPmAgFnS
do	do	k7c2
neutronů	neutron	k1gInPc2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
planet	planeta	k1gFnPc2
(	(	kIx(
<g/>
neutrony	neutron	k1gInPc7
podle	podle	k7c2
teorie	teorie	k1gFnSc2
Kozuchové	Kozuch	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
i	i	k9
velmi	velmi	k6eAd1
složitou	složitý	k2eAgFnSc4d1
vnitřní	vnitřní	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
unikla	uniknout	k5eAaPmAgFnS
masivnímu	masivní	k2eAgInSc3d1
výbuchu	výbuch	k1gInSc3
jádra	jádro	k1gNnSc2
Galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
zničit	zničit	k5eAaPmF
veškerý	veškerý	k3xTgInSc4
život	život	k1gInSc4
v	v	k7c6
Mléčné	mléčný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
Diaspory	diaspora	k1gFnSc2
následují	následovat	k5eAaImIp3nP
tuto	tento	k3xDgFnSc4
civilizaci	civilizace	k1gFnSc4
do	do	k7c2
duálního	duální	k2eAgInSc2d1
vesmíru	vesmír	k1gInSc2
ukrytého	ukrytý	k2eAgInSc2d1
ve	v	k7c6
složité	složitý	k2eAgFnSc6d1
struktuře	struktura	k1gFnSc6
v	v	k7c6
neutronech	neutron	k1gInPc6
a	a	k8xC
opouštějí	opouštět	k5eAaImIp3nP
známý	známý	k2eAgInSc4d1
vesmír	vesmír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
románu	román	k1gInSc6
Egan	Egana	k1gFnPc2
představuje	představovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
elegantní	elegantní	k2eAgFnPc4d1
fyzikální	fyzikální	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
podivuhodnější	podivuhodný	k2eAgMnPc1d2
–	–	k?
až	až	k6eAd1
dospěje	dochvít	k5eAaPmIp3nS
k	k	k7c3
nečekanému	čekaný	k2eNgInSc3d1
závěru	závěr	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celém	celý	k2eAgInSc6d1
románu	román	k1gInSc6
představí	představit	k5eAaPmIp3nS
několik	několik	k4yIc1
možných	možný	k2eAgFnPc2d1
forem	forma	k1gFnPc2
mimozemské	mimozemský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
–	–	k?
např.	např.	kA
šestnáctirozměrné	šestnáctirozměrný	k2eAgFnPc4d1
olihně	oliheň	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
software	software	k1gInSc4
běžící	běžící	k2eAgInSc4d1
na	na	k7c6
počítači	počítač	k1gInSc6
tvořeném	tvořený	k2eAgInSc6d1
Wangovými	Wangův	k2eAgInPc7d1
koberci	koberec	k1gInPc7
–	–	k?
organickými	organický	k2eAgFnPc7d1
molekulami	molekula	k1gFnPc7
v	v	k7c6
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
do	do	k7c2
sebe	se	k3xPyFc2
zapadají	zapadat	k5eAaImIp3nP,k5eAaPmIp3nP
podle	podle	k7c2
přesných	přesný	k2eAgFnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
Turingův	Turingův	k2eAgInSc1d1
stroj	stroj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Fyzika	fyzika	k1gFnSc1
v	v	k7c6
Diaspoře	diaspora	k1gFnSc6
</s>
<s>
Teorie	teorie	k1gFnSc1
Kozuchové	Kozuchový	k2eAgFnSc2d1
je	být	k5eAaImIp3nS
fiktivní	fiktivní	k2eAgFnSc1d1
polní	polní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
jsou	být	k5eAaImIp3nP
veškeré	veškerý	k3xTgFnPc1
elementární	elementární	k2eAgFnPc1d1
částice	částice	k1gFnPc1
tvořeny	tvořen	k2eAgFnPc1d1
ústími	ústí	k1gNnPc7
mikroskopických	mikroskopický	k2eAgFnPc2d1
kvantovaných	kvantovaný	k2eAgFnPc2d1
červích	červí	k2eAgFnPc2d1
děr	děra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
je	být	k5eAaImIp3nS
čtyřrozměrna	čtyřrozměrna	k1gFnSc1
a	a	k8xC
používá	používat	k5eAaImIp3nS
navíc	navíc	k6eAd1
šestirozměrné	šestirozměrný	k2eAgNnSc1d1
standardní	standardní	k2eAgNnSc1d1
vlákno	vlákno	k1gNnSc1
tvaru	tvar	k1gInSc2
hypersféry	hypersféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorii	teorie	k1gFnSc4
později	pozdě	k6eAd2
rozšíří	rozšířit	k5eAaPmIp3nP
Blanche	Blanche	k1gFnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
standardní	standardní	k2eAgNnSc4d1
vlákno	vlákno	k1gNnSc4
zvolí	zvolit	k5eAaPmIp3nS
dvanáctirozměrný	dvanáctirozměrný	k2eAgInSc1d1
torus	torus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
románu	román	k1gInSc2
vyspělá	vyspělý	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
lidem	člověk	k1gMnPc3
sděluje	sdělovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
skrytých	skrytý	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
nekonečně	konečně	k6eNd1
mnoho	mnoho	k4c1
<g/>
.	.	kIx.
</s>
<s>
Introdus	Introdus	k1gInSc1
je	být	k5eAaImIp3nS
mechanismus	mechanismus	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pomocí	pomocí	k7c2
nanotechnologie	nanotechnologie	k1gFnSc2
je	být	k5eAaImIp3nS
organické	organický	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
člověka	člověk	k1gMnSc2
přečteno	přečten	k2eAgNnSc1d1
<g/>
,	,	kIx,
molekulárně	molekulárně	k6eAd1
přeměněno	přeměněn	k2eAgNnSc1d1
a	a	k8xC
posléze	posléze	k6eAd1
překopírováno	překopírován	k2eAgNnSc1d1
do	do	k7c2
podoby	podoba	k1gFnSc2
software	software	k1gInSc1
v	v	k7c6
polis	polis	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Femtomechanismy	Femtomechanismus	k1gInPc1
jsou	být	k5eAaImIp3nP
mechanismy	mechanismus	k1gInPc1
fungující	fungující	k2eAgInPc1d1
přibližně	přibližně	k6eAd1
v	v	k7c6
rozměrech	rozměr	k1gInPc6
atomových	atomový	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
–	–	k?
složitým	složitý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
udržují	udržovat	k5eAaImIp3nP
shluky	shluk	k1gInPc4
jader	jádro	k1gNnPc2
v	v	k7c6
excitovaném	excitovaný	k2eAgInSc6d1
stavu	stav	k1gInSc6
vytvářejíce	vytvářet	k5eAaImSgFnP
tak	tak	k9
de-facto	de-facto	k1gNnSc1
„	„	k?
<g/>
jadernou	jaderný	k2eAgFnSc4d1
molekulu	molekula	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Duální	duální	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
rozšířené	rozšířený	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
Kozuchové	Kozuch	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
neutrony	neutron	k1gInPc4
(	(	kIx(
<g/>
a	a	k8xC
jiné	jiný	k2eAgFnSc2d1
částice	částice	k1gFnSc2
<g/>
)	)	kIx)
složitou	složitý	k2eAgFnSc4d1
vnitřní	vnitřní	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
závisející	závisející	k2eAgFnSc4d1
na	na	k7c6
přesném	přesný	k2eAgNnSc6d1
napojení	napojení	k1gNnSc6
ústí	ústit	k5eAaImIp3nS
červích	červí	k2eAgFnPc2d1
děr	děra	k1gFnPc2
uvnitř	uvnitř	k7c2
částice	částice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
lze	lze	k6eAd1
docílit	docílit	k5eAaPmF
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
vytvořená	vytvořený	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
přesně	přesně	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
emuluje	emulovat	k5eAaImIp3nS
výpočet	výpočet	k1gInSc4
duálního	duální	k2eAgInSc2d1
vesmíru	vesmír	k1gInSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
pět	pět	k4xCc1
rozměrů	rozměr	k1gInPc2
prostorových	prostorový	k2eAgInPc2d1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
časový	časový	k2eAgInSc4d1
a	a	k8xC
čtyři	čtyři	k4xCgInPc4
rozměry	rozměr	k1gInPc4
jako	jako	k8xC,k8xS
standardní	standardní	k2eAgNnSc4d1
vlákno	vlákno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
takového	takový	k3xDgInSc2
vesmíru	vesmír	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
románu	román	k1gInSc2
lidé	člověk	k1gMnPc1
překopírují	překopírovat	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
zachránili	zachránit	k5eAaPmAgMnP
před	před	k7c7
výbuchem	výbuch	k1gInSc7
jádra	jádro	k1gNnSc2
galaxie	galaxie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodné	pozoruhodný	k2eAgFnSc2d1
na	na	k7c6
pětirozměrném	pětirozměrný	k2eAgInSc6d1
vesmíru	vesmír	k1gInSc6
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
elektrony	elektron	k1gInPc1
netvoří	tvořit	k5eNaImIp3nP
stabilní	stabilní	k2eAgInPc1d1
orbitaly	orbital	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
spadnou	spadnout	k5eAaPmIp3nP
do	do	k7c2
jader	jádro	k1gNnPc2
atomů	atom	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
že	že	k8xS
planety	planeta	k1gFnPc1
nemají	mít	k5eNaImIp3nP
stabilní	stabilní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
tyto	tento	k3xDgFnPc1
předpovědi	předpověď	k1gFnPc1
odpovídají	odpovídat	k5eAaImIp3nP
současné	současný	k2eAgFnSc3d1
fyzice	fyzika	k1gFnSc3
aplikované	aplikovaný	k2eAgFnSc2d1
na	na	k7c4
pětirozměrný	pětirozměrný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Formy	forma	k1gFnPc1
života	život	k1gInSc2
v	v	k7c6
Diaspoře	diaspora	k1gFnSc6
</s>
<s>
Wangovy	Wangův	k2eAgInPc1d1
koberce	koberec	k1gInPc1
jsou	být	k5eAaImIp3nP
gigantické	gigantický	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
tvořené	tvořený	k2eAgFnPc1d1
z	z	k7c2
organických	organický	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
jako	jako	k9
Wangovy	Wangův	k2eAgFnPc1d1
destičky	destička	k1gFnPc1
–	–	k?
podle	podle	k7c2
přesných	přesný	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
jediným	jediný	k2eAgMnSc7d1
možným	možný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
vyskládají	vyskládat	k5eAaPmIp3nP
rovinu	rovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molekuly	molekula	k1gFnSc2
volně	volně	k6eAd1
plovoucí	plovoucí	k2eAgFnSc1d1
v	v	k7c6
moři	moře	k1gNnSc6
se	se	k3xPyFc4
postupně	postupně	k6eAd1
na	na	k7c4
již	již	k6eAd1
existující	existující	k2eAgInSc4d1
Wangův	Wangův	k2eAgInSc4d1
koberec	koberec	k1gInSc4
nabalují	nabalovat	k5eAaImIp3nP
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
probíhá	probíhat	k5eAaImIp3nS
výpočet	výpočet	k1gInSc1
obdobně	obdobně	k6eAd1
jako	jako	k8xC,k8xS
např.	např.	kA
na	na	k7c6
Turingově	Turingově	k1gFnSc6
stroji	stroj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
knize	kniha	k1gFnSc6
tento	tento	k3xDgInSc1
obrovský	obrovský	k2eAgInSc1d1
„	„	k?
<g/>
počítač	počítač	k1gInSc1
<g/>
“	“	k?
mimoděk	mimoděk	k6eAd1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
virtuální	virtuální	k2eAgFnSc4d1
realitu	realita	k1gFnSc4
pro	pro	k7c4
život	život	k1gInSc4
šestnáctirozměrných	šestnáctirozměrný	k2eAgFnPc2d1
olihní	oliheň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozkročenci	Rozkročenec	k1gMnPc1
mezi	mezi	k7c7
hvězdami	hvězda	k1gFnPc7
jsou	být	k5eAaImIp3nP
členové	člen	k1gMnPc1
prastaré	prastarý	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
své	svůj	k3xOyFgInPc4
myšlenkové	myšlenkový	k2eAgInPc4d1
pochody	pochod	k1gInPc4
zpomalit	zpomalit	k5eAaPmF
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednotlivci	jednotlivec	k1gMnPc1
rozprostírají	rozprostírat	k5eAaImIp3nP
mezi	mezi	k7c7
několika	několik	k4yIc7
hvězdnými	hvězdný	k2eAgInPc7d1
systémy	systém	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
myšlení	myšlení	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
pomalu	pomalu	k6eAd1
–	–	k?
pomocí	pomocí	k7c2
radiových	radiový	k2eAgInPc2d1
signálů	signál	k1gInPc2
šířících	šířící	k2eAgInPc2d1
se	se	k3xPyFc4
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
rychlostí	rychlost	k1gFnSc7
světla	světlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Transmutéři	Transmutér	k1gMnPc1
jsou	být	k5eAaImIp3nP
civilizace	civilizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
obdobně	obdobně	k6eAd1
jako	jako	k8xS,k8xC
lidé	člověk	k1gMnPc1
žije	žít	k5eAaImIp3nS
v	v	k7c6
polis	polis	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
nejde	jít	k5eNaImIp3nS
o	o	k7c4
počítače	počítač	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
měnily	měnit	k5eAaImAgFnP
v	v	k7c6
čase	čas	k1gInSc6
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
o	o	k7c4
dlouhý	dlouhý	k2eAgInSc4d1
řetězec	řetězec	k1gInSc4
po	po	k7c6
sobě	sebe	k3xPyFc6
jdoucích	jdoucí	k2eAgInPc2d1
stavů	stav	k1gInPc2
téhož	týž	k3xTgInSc2
polis	polis	k1gInSc4
„	„	k?
<g/>
vytesaných	vytesaný	k2eAgInPc2d1
<g/>
“	“	k?
do	do	k7c2
odolného	odolný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
z	z	k7c2
křemíku	křemík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
měnící	měnící	k2eAgFnSc1d1
řada	řada	k1gFnSc1
polis	polis	k1gFnSc2
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
napříč	napříč	k7c7
dlouhým	dlouhý	k2eAgInSc7d1
řetězcem	řetězec	k1gInSc7
navzájem	navzájem	k6eAd1
duálních	duální	k2eAgInPc2d1
vesmírů	vesmír	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
tvoří	tvořit	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
„	„	k?
<g/>
tiky	tik	k1gInPc1
<g/>
“	“	k?
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
příklad	příklad	k1gInSc4
civilizace	civilizace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
již	již	k6eAd1
za	za	k7c4
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
vyčerpala	vyčerpat	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
možnosti	možnost	k1gFnPc4
o	o	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
stála	stát	k5eAaImAgFnS
a	a	k8xC
dále	daleko	k6eAd2
se	se	k3xPyFc4
nemá	mít	k5eNaImIp3nS
kam	kam	k6eAd1
vyvíjet	vyvíjet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Poustevníci	poustevník	k1gMnPc1
jsou	být	k5eAaImIp3nP
civilizace	civilizace	k1gFnPc4
obývající	obývající	k2eAgInSc4d1
duální	duální	k2eAgInSc4d1
pětirozměrný	pětirozměrný	k2eAgInSc4d1
vesmír	vesmír	k1gInSc4
–	–	k?
tato	tento	k3xDgFnSc1
civilizace	civilizace	k1gFnSc1
uzpůsobila	uzpůsobit	k5eAaPmAgFnS
ekosystém	ekosystém	k1gInSc4
své	svůj	k3xOyFgFnSc2
planety	planeta	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jednotliví	jednotlivý	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
bez	bez	k7c2
jakékoliv	jakýkoliv	k3yIgFnSc2
námahy	námaha	k1gFnSc2
získali	získat	k5eAaPmAgMnP
veškerý	veškerý	k3xTgInSc4
komfort	komfort	k1gInSc4
a	a	k8xC
potravu	potrava	k1gFnSc4
–	–	k?
o	o	k7c4
průzkum	průzkum	k1gInSc4
vesmíru	vesmír	k1gInSc2
či	či	k8xC
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
civilizacemi	civilizace	k1gFnPc7
nemají	mít	k5eNaImIp3nP
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Diaspora	diaspora	k1gFnSc1
na	na	k7c4
LEGII	legie	k1gFnSc4
<g/>
.	.	kIx.
</s>
