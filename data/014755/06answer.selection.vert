<s>
Diaspora	diaspora	k1gFnSc1
je	být	k5eAaImIp3nS
sci-fi	sci-fi	k1gFnSc1
román	román	k1gInSc1
Grega	Grega	k1gFnSc1
Egana	Egan	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
do	do	k7c2
češtiny	čeština	k1gFnSc2
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
originálu	originál	k1gInSc2
Diaspora	diaspora	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
přeložen	přeložen	k2eAgInSc1d1
roku	rok	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
nakladatelství	nakladatelství	k1gNnSc6
Návrat	návrat	k1gInSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kotrle	Kotrle	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7174	#num#	k4
<g/>
-	-	kIx~
<g/>
607	#num#	k4
<g/>
-X	-X	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>