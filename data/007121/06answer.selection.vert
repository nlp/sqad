<s>
Těstoviny	těstovina	k1gFnPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
nich	on	k3xPp3gMnPc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
1	[number]	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
ve	v	k7c6
známé	známý	k2eAgFnSc6d1
Apiciově	Apiciův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
(	(	kIx(
<g/>
který	který	k3yIgInSc1
žil	žít	k5eAaImAgMnS
v	v	k7c6
době	doba	k1gFnSc6
císaře	císař	k1gMnSc2
Tiberia	Tiberium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
popisována	popisován	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
sekaného	sekaný	k2eAgNnSc2d1
masa	maso	k1gNnSc2
nebo	nebo	k8xC
ryb	ryba	k1gFnPc2
obložených	obložený	k2eAgFnPc2d1
těstovinami	těstovina	k1gFnPc7
"	"	kIx"
<g/>
lasagne	lasagnout	k5eAaPmIp3nS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>