<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1954	[number]	k4	1954
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
slovenského	slovenský	k2eAgInSc2d1	slovenský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
poslancem	poslanec	k1gMnSc7	poslanec
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
v	v	k7c6	v
kabinetu	kabinet	k1gInSc6	kabinet
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
velkého	velký	k2eAgInSc2d1	velký
koncernu	koncern	k1gInSc2	koncern
Agrofert	Agrofert	k1gMnSc1	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
využívání	využívání	k1gNnSc3	využívání
své	svůj	k3xOyFgFnSc2	svůj
značné	značný	k2eAgFnSc2d1	značná
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
mediální	mediální	k2eAgFnSc2d1	mediální
moci	moc	k1gFnSc2	moc
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgMnS	označovat
za	za	k7c4	za
oligarchu	oligarcha	k1gMnSc4	oligarcha
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
čerpání	čerpání	k1gNnSc3	čerpání
dotací	dotace	k1gFnPc2	dotace
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
státním	státní	k2eAgFnPc3d1	státní
zákázkám	zákázka	k1gFnPc3	zákázka
obviňován	obviňován	k2eAgInSc4d1	obviňován
ze	z	k7c2	z
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
strávil	strávit	k5eAaPmAgMnS	strávit
Babiš	Babiš	k1gMnSc1	Babiš
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc2	mládí
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
Chemapol	chemapol	k1gInSc1	chemapol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
jako	jako	k8xS	jako
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
delegát	delegát	k1gMnSc1	delegát
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
jej	on	k3xPp3gInSc4	on
začala	začít	k5eAaPmAgFnS	začít
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
(	(	kIx(	(
<g/>
StB	StB	k1gFnSc1	StB
<g/>
)	)	kIx)	)
evidovat	evidovat	k5eAaImF	evidovat
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
důvěrníka	důvěrník	k1gMnSc2	důvěrník
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
jako	jako	k8xC	jako
agenta	agent	k1gMnSc4	agent
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Bureš	Bureš	k1gMnSc1	Bureš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
vědomou	vědomý	k2eAgFnSc4d1	vědomá
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
StB	StB	k1gFnPc7	StB
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
ČSFR	ČSFR	kA	ČSFR
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založil	založit	k5eAaPmAgMnS	založit
Agrofert	Agrofert	k1gInSc4	Agrofert
jako	jako	k8xC	jako
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
firmu	firma	k1gFnSc4	firma
slovenského	slovenský	k2eAgInSc2d1	slovenský
Petrimexu	Petrimex	k1gInSc2	Petrimex
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
předtím	předtím	k6eAd1	předtím
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1995	[number]	k4	1995
Agrofert	Agrofert	k1gInSc4	Agrofert
za	za	k7c2	za
nejasných	jasný	k2eNgFnPc2d1	nejasná
okolností	okolnost	k1gFnPc2	okolnost
převzal	převzít	k5eAaPmAgInS	převzít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
založené	založený	k2eAgFnSc2d1	založená
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
společnosti	společnost	k1gFnSc2	společnost
O.	O.	kA	O.
<g/>
F.	F.	kA	F.
<g/>
I.	I.	kA	I.
Z	z	k7c2	z
Agrofertu	Agrofert	k1gInSc2	Agrofert
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgInS	stát
významný	významný	k2eAgInSc1d1	významný
agrochemický	agrochemický	k2eAgInSc1d1	agrochemický
holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
kolem	kolem	k7c2	kolem
250	[number]	k4	250
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
holdingu	holding	k1gInSc2	holding
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
mediální	mediální	k2eAgInSc1d1	mediální
dům	dům	k1gInSc1	dům
MAFRA	MAFRA	kA	MAFRA
<g/>
,	,	kIx,	,
vydávající	vydávající	k2eAgFnSc2d1	vydávající
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
celostátní	celostátní	k2eAgInPc4d1	celostátní
deníky	deník	k1gInPc4	deník
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
a	a	k8xC	a
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
Agrofert	Agrofert	k1gMnSc1	Agrofert
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
až	až	k6eAd1	až
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
své	svůj	k3xOyFgFnPc4	svůj
firmy	firma	k1gFnPc4	firma
převedl	převést	k5eAaPmAgMnS	převést
do	do	k7c2	do
svěřenského	svěřenský	k2eAgInSc2d1	svěřenský
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jeho	jeho	k3xOp3gNnSc1	jeho
jmění	jmění	k1gNnSc1	jmění
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
82	[number]	k4	82
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
činilo	činit	k5eAaImAgNnS	činit
456	[number]	k4	456
<g/>
.	.	kIx.	.
nejbohatšího	bohatý	k2eAgMnSc2d3	nejbohatší
člověka	člověk	k1gMnSc2	člověk
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgMnSc4	druhý
nejbohatšího	bohatý	k2eAgMnSc4d3	nejbohatší
Čecha	Čech	k1gMnSc4	Čech
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vůbec	vůbec	k9	vůbec
nejzámožnějšího	zámožný	k2eAgMnSc2d3	nejzámožnější
Slováka	Slovák	k1gMnSc2	Slovák
<g/>
.	.	kIx.	.
<g/>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
založil	založit	k5eAaPmAgMnS	založit
občanskou	občanský	k2eAgFnSc4d1	občanská
iniciativu	iniciativa	k1gFnSc4	iniciativa
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Akce	akce	k1gFnSc1	akce
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
později	pozdě	k6eAd2	pozdě
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
hnutí	hnutí	k1gNnSc6	hnutí
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
za	za	k7c7	za
Českou	český	k2eAgFnSc7d1	Česká
stranou	strana	k1gFnSc7	strana
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
18,65	[number]	k4	18,65
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Babiš	Babiš	k1gInSc1	Babiš
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
vicepremiérem	vicepremiér	k1gMnSc7	vicepremiér
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vládních	vládní	k2eAgInPc2d1	vládní
postů	post	k1gInPc2	post
však	však	k9	však
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
premiér	premiér	k1gMnSc1	premiér
Sobotka	Sobotka	k1gMnSc1	Sobotka
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jeho	jeho	k3xOp3gNnSc4	jeho
odvolání	odvolání	k1gNnSc4	odvolání
pro	pro	k7c4	pro
údajné	údajný	k2eAgInPc4d1	údajný
skandály	skandál	k1gInPc4	skandál
<g/>
,	,	kIx,	,
neodvádění	neodvádění	k1gNnSc1	neodvádění
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
úkolování	úkolování	k1gNnSc1	úkolování
novinářů	novinář	k1gMnPc2	novinář
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
dalších	další	k2eAgFnPc6d1	další
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
hnutí	hnutí	k1gNnPc2	hnutí
ANO	ano	k9	ano
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
78	[number]	k4	78
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
stranický	stranický	k2eAgMnSc1d1	stranický
lídr	lídr	k1gMnSc1	lídr
středočeské	středočeský	k2eAgFnSc2d1	Středočeská
kandidátky	kandidátka	k1gFnSc2	kandidátka
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
jej	on	k3xPp3gNnSc2	on
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
pověřil	pověřit	k5eAaPmAgMnS	pověřit
jednáním	jednání	k1gNnPc3	jednání
o	o	k7c6	o
sestavení	sestavení	k1gNnSc6	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
jej	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
nezískala	získat	k5eNaPmAgFnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
podala	podat	k5eAaPmAgFnS	podat
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vládla	vládnout	k5eAaImAgFnS	vládnout
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
jej	on	k3xPp3gInSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
podruhé	podruhé	k6eAd1	podruhé
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc4	jeho
druhou	druhý	k4xOgFnSc4	druhý
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Menšinový	menšinový	k2eAgInSc1d1	menšinový
kabinet	kabinet	k1gInSc1	kabinet
byl	být	k5eAaImAgInS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
ANO	ano	k9	ano
a	a	k9	a
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
a	a	k8xC	a
tolerancí	tolerance	k1gFnSc7	tolerance
od	od	k7c2	od
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
získala	získat	k5eAaPmAgFnS	získat
vláda	vláda	k1gFnSc1	vláda
důvěru	důvěra	k1gFnSc4	důvěra
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
2017	[number]	k4	2017
a	a	k8xC	a
lednu	leden	k1gInSc3	leden
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
již	již	k9	již
jako	jako	k9	jako
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
vydán	vydat	k5eAaPmNgInS	vydat
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
ČR	ČR	kA	ČR
k	k	k7c3	k
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
kvůli	kvůli	k7c3	kvůli
kauze	kauza	k1gFnSc3	kauza
Čapí	čapí	k2eAgNnSc1d1	čapí
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinný	rodinný	k2eAgInSc1d1	rodinný
původ	původ	k1gInSc1	původ
===	===	k?	===
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
;	;	kIx,	;
rodina	rodina	k1gFnSc1	rodina
matky	matka	k1gFnSc2	matka
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
<g/>
Otec	otec	k1gMnSc1	otec
Ing.	ing.	kA	ing.
Štefan	Štefan	k1gMnSc1	Štefan
Babiš	Babiš	k1gMnSc1	Babiš
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
(	(	kIx(	(
<g/>
VŠE	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
pracoval	pracovat	k5eAaImAgInS	pracovat
jako	jako	k9	jako
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
pracovník	pracovník	k1gMnSc1	pracovník
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
KSS	KSS	kA	KSS
<g/>
)	)	kIx)	)
a	a	k8xC	a
vůči	vůči	k7c3	vůči
režimu	režim	k1gInSc3	režim
zaujímal	zaujímat	k5eAaImAgMnS	zaujímat
konformní	konformní	k2eAgInSc4d1	konformní
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
jako	jako	k8xC	jako
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
delegát	delegát	k1gMnSc1	delegát
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
,	,	kIx,	,
po	po	k7c6	po
necelém	celý	k2eNgInSc6d1	necelý
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
z	z	k7c2	z
KSS	KSS	kA	KSS
a	a	k8xC	a
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
musel	muset	k5eAaImAgMnS	muset
pracovat	pracovat	k5eAaImF	pracovat
mimo	mimo	k7c4	mimo
svůj	svůj	k3xOyFgInSc4	svůj
obor	obor	k1gInSc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
k	k	k7c3	k
oboru	obor	k1gInSc3	obor
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
odborný	odborný	k2eAgMnSc1d1	odborný
asistent	asistent	k1gMnSc1	asistent
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
VŠE	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařila	podařit	k5eAaPmAgFnS	podařit
politická	politický	k2eAgFnSc1d1	politická
rehabilitace	rehabilitace	k1gFnSc1	rehabilitace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
navráceno	navrácen	k2eAgNnSc1d1	navráceno
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
KSS	KSS	kA	KSS
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
držel	držet	k5eAaImAgInS	držet
režimem	režim	k1gInSc7	režim
vytyčené	vytyčený	k2eAgFnPc1d1	vytyčená
linie	linie	k1gFnPc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
obchodní	obchodní	k2eAgMnSc1d1	obchodní
přidělenec	přidělenec	k1gMnSc1	přidělenec
ve	v	k7c6	v
Stálé	stálý	k2eAgFnSc6d1	stálá
misi	mise	k1gFnSc6	mise
ČSSR	ČSSR	kA	ČSSR
u	u	k7c2	u
OSN	OSN	kA	OSN
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
odštěpného	odštěpný	k2eAgInSc2d1	odštěpný
závodu	závod	k1gInSc2	závod
PZO	PZO	kA	PZO
Polytechna	polytechna	k1gFnSc1	polytechna
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
;	;	kIx,	;
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jej	on	k3xPp3gInSc4	on
několikrát	několikrát	k6eAd1	několikrát
prověřovala	prověřovat	k5eAaImAgFnS	prověřovat
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
rozvědka	rozvědka	k1gFnSc1	rozvědka
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
před	před	k7c7	před
trestním	trestní	k2eAgNnSc7d1	trestní
stíháním	stíhání	k1gNnSc7	stíhání
jej	on	k3xPp3gNnSc2	on
uchránila	uchránit	k5eAaPmAgFnS	uchránit
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
amnestie	amnestie	k1gFnSc1	amnestie
z	z	k7c2	z
května	květen	k1gInSc2	květen
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
<g/>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgMnSc1	žádný
komunistický	komunistický	k2eAgMnSc1d1	komunistický
prominent	prominent	k1gMnSc1	prominent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nezastával	zastávat	k5eNaImAgMnS	zastávat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
funkce	funkce	k1gFnPc4	funkce
v	v	k7c6	v
komunistické	komunistický	k2eAgFnSc6d1	komunistická
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
funkcích	funkce	k1gFnPc6	funkce
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
schválení	schválení	k1gNnPc4	schválení
orgány	orgán	k1gInPc7	orgán
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
jej	on	k3xPp3gMnSc4	on
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
otcovo	otcův	k2eAgNnSc4d1	otcovo
postavení	postavení	k1gNnSc4	postavení
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
protekční	protekční	k2eAgNnSc1d1	protekční
dítko	dítko	k1gNnSc1	dítko
režimu	režim	k1gInSc2	režim
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nomenklaturní	nomenklaturní	k2eAgNnSc4d1	nomenklaturní
dítě	dítě	k1gNnSc4	dítě
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
příslušníka	příslušník	k1gMnSc4	příslušník
privilegované	privilegovaný	k2eAgFnSc2d1	privilegovaná
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
Matka	matka	k1gFnSc1	matka
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
<g/>
,	,	kIx,	,
Adriana	Adriana	k1gFnSc1	Adriana
Babišová	Babišový	k2eAgFnSc1d1	Babišový
rozená	rozený	k2eAgFnSc1d1	rozená
Scheibnerová	Scheibnerová	k1gFnSc1	Scheibnerová
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
bratislavskou	bratislavský	k2eAgFnSc4d1	Bratislavská
VŠE	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
manželova	manželův	k2eAgFnSc1d1	manželova
vytíženost	vytíženost	k1gFnSc1	vytíženost
a	a	k8xC	a
pracovní	pracovní	k2eAgInPc1d1	pracovní
pobyty	pobyt	k1gInPc1	pobyt
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
jí	jíst	k5eAaImIp3nS	jíst
však	však	k9	však
přisoudily	přisoudit	k5eAaPmAgFnP	přisoudit
skromnější	skromný	k2eAgFnSc4d2	skromnější
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
tajemnice	tajemnice	k1gFnSc1	tajemnice
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
institucích	instituce	k1gFnPc6	instituce
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
byla	být	k5eAaImAgFnS	být
tajemnicí	tajemnice	k1gFnSc7	tajemnice
Ústavu	ústav	k1gInSc2	ústav
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
v	v	k7c6	v
KSS	KSS	kA	KSS
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
také	také	k9	také
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
komisích	komise	k1gFnPc6	komise
ustavených	ustavený	k2eAgInPc2d1	ustavený
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
v	v	k7c6	v
lidových	lidový	k2eAgInPc6d1	lidový
soudech	soud	k1gInPc6	soud
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
podnikatele	podnikatel	k1gMnSc2	podnikatel
Alexandra	Alexandr	k1gMnSc2	Alexandr
Babiše	Babiš	k1gMnSc2	Babiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zahraničnímu	zahraniční	k2eAgInSc3d1	zahraniční
působení	působení	k1gNnSc4	působení
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
žil	žít	k5eAaImAgMnS	žít
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
také	také	k9	také
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
při	při	k7c6	při
československém	československý	k2eAgNnSc6d1	Československé
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Nové	Nové	k2eAgNnSc1d1	Nové
Mesto	Mesto	k1gNnSc1	Mesto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	let	k1gInPc6	let
nechala	nechat	k5eAaPmAgFnS	nechat
zapsat	zapsat	k5eAaPmF	zapsat
do	do	k7c2	do
tenisové	tenisový	k2eAgFnSc2d1	tenisová
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
kurtech	kurt	k1gInPc6	kurt
bratislavského	bratislavský	k2eAgInSc2d1	bratislavský
Slovanu	Slovan	k1gInSc2	Slovan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
při	při	k7c6	při
turnajích	turnaj	k1gInPc6	turnaj
sbíral	sbírat	k5eAaImAgMnS	sbírat
míčky	míček	k1gInPc7	míček
a	a	k8xC	a
získával	získávat	k5eAaImAgMnS	získávat
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
výdělky	výdělek	k1gInPc4	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přidal	přidat	k5eAaPmAgMnS	přidat
volejbal	volejbal	k1gInSc4	volejbal
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
slavil	slavit	k5eAaImAgMnS	slavit
úspěchy	úspěch	k1gInPc7	úspěch
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
přesunu	přesun	k1gInSc6	přesun
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
září	září	k1gNnSc6	září
1969	[number]	k4	1969
chodit	chodit	k5eAaImF	chodit
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
Collè	Collè	k1gMnSc1	Collè
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
;	;	kIx,	;
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
hrál	hrát	k5eAaImAgMnS	hrát
volejbal	volejbal	k1gInSc4	volejbal
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Servette	Servett	k1gInSc5	Servett
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
juniorské	juniorský	k2eAgFnSc2d1	juniorská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Babiš	Babiš	k1gInSc1	Babiš
nakonec	nakonec	k6eAd1	nakonec
vychodil	vychodit	k5eAaImAgInS	vychodit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
ročník	ročník	k1gInSc1	ročník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
posléze	posléze	k6eAd1	posléze
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
trombocytopenií	trombocytopenie	k1gFnSc7	trombocytopenie
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
podání	podání	k1gNnSc2	podání
poležel	poležet	k5eAaPmAgInS	poležet
zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc4	rok
a	a	k8xC	a
během	během	k7c2	během
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
bydlel	bydlet	k5eAaImAgMnS	bydlet
Babiš	Babiš	k1gInSc4	Babiš
u	u	k7c2	u
babičky	babička	k1gFnSc2	babička
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Ladislava	Ladislav	k1gMnSc2	Ladislav
Novomeského	Novomeský	k2eAgMnSc2d1	Novomeský
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
poměrně	poměrně	k6eAd1	poměrně
elitní	elitní	k2eAgFnSc1d1	elitní
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
vyučováním	vyučování	k1gNnSc7	vyučování
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bodoval	bodovat	k5eAaImAgMnS	bodovat
prý	prý	k9	prý
hlavně	hlavně	k9	hlavně
dobrými	dobrý	k2eAgInPc7d1	dobrý
výkony	výkon	k1gInPc7	výkon
v	v	k7c6	v
meziškolních	meziškolní	k2eAgNnPc6d1	meziškolní
utkáních	utkání	k1gNnPc6	utkání
ve	v	k7c6	v
volejbalu	volejbal	k1gInSc6	volejbal
a	a	k8xC	a
basketbalu	basketbal	k1gInSc6	basketbal
a	a	k8xC	a
pomohly	pomoct	k5eAaPmAgInP	pomoct
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
občasné	občasný	k2eAgInPc4d1	občasný
dovozy	dovoz	k1gInPc4	dovoz
gramofonových	gramofonový	k2eAgFnPc2d1	gramofonová
desek	deska	k1gFnPc2	deska
rockových	rockový	k2eAgFnPc2d1	rocková
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
moderního	moderní	k2eAgNnSc2d1	moderní
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
dostával	dostávat	k5eAaImAgInS	dostávat
přes	přes	k7c4	přes
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
"	"	kIx"	"
<g/>
lehce	lehko	k6eAd1	lehko
přidrzlý	přidrzlý	k2eAgMnSc1d1	přidrzlý
čahoun	čahoun	k1gMnSc1	čahoun
s	s	k7c7	s
polodlouhými	polodlouhý	k2eAgInPc7d1	polodlouhý
vlasy	vlas	k1gInPc7	vlas
<g/>
"	"	kIx"	"
též	též	k9	též
chodil	chodit	k5eAaImAgMnS	chodit
za	za	k7c4	za
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
si	se	k3xPyFc3	se
omluvenky	omluvenka	k1gFnSc2	omluvenka
s	s	k7c7	s
napodobeninou	napodobenina	k1gFnSc7	napodobenina
babiččina	babiččin	k2eAgInSc2d1	babiččin
podpisu	podpis	k1gInSc2	podpis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
ředitel	ředitel	k1gMnSc1	ředitel
školy	škola	k1gFnSc2	škola
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
podpisy	podpis	k1gInPc1	podpis
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
a	a	k8xC	a
Babišovi	Babišův	k2eAgMnPc1d1	Babišův
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vyloučení	vyloučení	k1gNnSc1	vyloučení
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
tehdy	tehdy	k6eAd1	tehdy
hodně	hodně	k6eAd1	hodně
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
jsem	být	k5eAaImIp1nS	být
snad	snad	k9	snad
trojku	trojka	k1gFnSc4	trojka
z	z	k7c2	z
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
po	po	k7c6	po
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
se	se	k3xPyFc4	se
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
manželkou	manželka	k1gFnSc7	manželka
Beatou	Beatý	k2eAgFnSc7d1	Beatý
Adamovičovou	Adamovičův	k2eAgFnSc7d1	Adamovičova
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
prý	prý	k9	prý
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k9	už
každý	každý	k3xTgMnSc1	každý
studoval	studovat	k5eAaImAgMnS	studovat
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
<g/>
Maturoval	maturovat	k5eAaBmAgMnS	maturovat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1974	[number]	k4	1974
ze	z	k7c2	z
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
ruštiny	ruština	k1gFnSc2	ruština
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
studium	studium	k1gNnSc1	studium
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
byl	být	k5eAaImAgInS	být
Babiš	Babiš	k1gInSc1	Babiš
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Obchodní	obchodní	k2eAgFnSc4d1	obchodní
fakultu	fakulta	k1gFnSc4	fakulta
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
(	(	kIx(	(
<g/>
VŠE	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
spolupodílel	spolupodílet	k5eAaImAgMnS	spolupodílet
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Babiš	Babiš	k1gMnSc1	Babiš
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
školu	škola	k1gFnSc4	škola
velké	velký	k2eAgNnSc1d1	velké
uznání	uznání	k1gNnSc4	uznání
neměl	mít	k5eNaImAgInS	mít
<g/>
,	,	kIx,	,
připadala	připadat	k5eAaImAgFnS	připadat
mu	on	k3xPp3gMnSc3	on
natolik	natolik	k6eAd1	natolik
nenáročná	náročný	k2eNgNnPc1d1	nenáročné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prý	prý	k9	prý
nudil	nudit	k5eAaImAgMnS	nudit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
v	v	k7c6	v
předvolebním	předvolební	k2eAgInSc6d1	předvolební
videorozhovoru	videorozhovor	k1gInSc6	videorozhovor
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
lehká	lehký	k2eAgFnSc1d1	lehká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Učili	učit	k5eAaImAgMnP	učit
nás	my	k3xPp1nPc4	my
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
starší	starý	k2eAgMnSc1d2	starší
kolegové	kolega	k1gMnPc1	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
koho	kdo	k3yQnSc4	kdo
nesrazila	srazit	k5eNaPmAgFnS	srazit
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
,	,	kIx,	,
udělal	udělat	k5eAaPmAgMnS	udělat
ekonomku	ekonomka	k1gFnSc4	ekonomka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nicméně	nicméně	k8xC	nicméně
některé	některý	k3yIgInPc4	některý
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
statistiku	statistika	k1gFnSc4	statistika
nebo	nebo	k8xC	nebo
účetnictví	účetnictví	k1gNnSc4	účetnictví
<g/>
,	,	kIx,	,
udělal	udělat	k5eAaPmAgInS	udělat
až	až	k9	až
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
práci	práce	k1gFnSc3	práce
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
organizaci	organizace	k1gFnSc6	organizace
studentů	student	k1gMnPc2	student
AIESEC	AIESEC	kA	AIESEC
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
pobočku	pobočka	k1gFnSc4	pobočka
na	na	k7c4	na
VŠE	všechen	k3xTgNnSc4	všechen
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
vedl	vést	k5eAaImAgInS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ekonoma	ekonom	k1gMnSc2	ekonom
Karla	Karel	k1gMnSc2	Karel
Kříže	Kříž	k1gMnSc2	Kříž
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
spolurozhodoval	spolurozhodovat	k5eAaImAgMnS	spolurozhodovat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ze	z	k7c2	z
studentů	student	k1gMnPc2	student
vycestuje	vycestovat	k5eAaPmIp3nS	vycestovat
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
vycestoval	vycestovat	k5eAaPmAgInS	vycestovat
na	na	k7c4	na
brigády	brigáda	k1gFnPc4	brigáda
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Lille	Lill	k1gInSc6	Lill
a	a	k8xC	a
Dijonu	Dijon	k1gInSc6	Dijon
a	a	k8xC	a
v	v	k7c6	v
belgické	belgický	k2eAgFnSc6d1	belgická
bance	banka	k1gFnSc6	banka
Kredietbank	Kredietbanka	k1gFnPc2	Kredietbanka
<g/>
.	.	kIx.	.
</s>
<s>
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
clech	clo	k1gNnPc6	clo
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
liberalizaci	liberalizace	k1gFnSc6	liberalizace
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
ukončil	ukončit	k5eAaPmAgInS	ukončit
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
diplomem	diplom	k1gInSc7	diplom
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Perglera	Pergler	k1gMnSc2	Pergler
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Babiše	Babiš	k1gMnPc4	Babiš
období	období	k1gNnSc2	období
studia	studio	k1gNnSc2	studio
na	na	k7c4	na
VŠE	všechen	k3xTgNnSc4	všechen
zlomové	zlomový	k2eAgFnPc1d1	zlomová
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
kariérní	kariérní	k2eAgInSc4d1	kariérní
start	start	k1gInSc4	start
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
to	ten	k3xDgNnSc4	ten
nebyl	být	k5eNaImAgMnS	být
ten	ten	k3xDgMnSc1	ten
lehkovážný	lehkovážný	k2eAgMnSc1d1	lehkovážný
floutek	floutek	k1gMnSc1	floutek
jako	jako	k8xS	jako
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ambiciózní	ambiciózní	k2eAgMnSc1d1	ambiciózní
svazák	svazák	k1gMnSc1	svazák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
cílem	cíl	k1gInSc7	cíl
–	–	k?	–
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
otcova	otcův	k2eAgInSc2d1	otcův
vzoru	vzor	k1gInSc2	vzor
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
delegátem	delegát	k1gMnSc7	delegát
<g/>
,	,	kIx,	,
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
vydělávat	vydělávat	k5eAaImF	vydělávat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Začátek	začátek	k1gInSc1	začátek
pracovní	pracovní	k2eAgFnSc2d1	pracovní
kariéry	kariéra	k1gFnSc2	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
mužských	mužský	k2eAgMnPc2d1	mužský
absolventů	absolvent	k1gMnPc2	absolvent
po	po	k7c6	po
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
nestrávil	strávit	k5eNaPmAgInS	strávit
rok	rok	k1gInSc1	rok
na	na	k7c6	na
vojně	vojna	k1gFnSc6	vojna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
promoci	promoce	k1gFnSc6	promoce
tak	tak	k9	tak
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
podniku	podnik	k1gInSc2	podnik
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
PZO	PZO	kA	PZO
<g/>
)	)	kIx)	)
Chemapol	chemapol	k1gInSc1	chemapol
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1980	[number]	k4	1980
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Petrimex	Petrimex	k1gInSc4	Petrimex
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInPc4	první
týdny	týden	k1gInPc4	týden
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
Petrimexu	Petrimex	k1gInSc6	Petrimex
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Babiše	Babiš	k1gMnSc4	Babiš
údajně	údajně	k6eAd1	údajně
rozčarováním	rozčarování	k1gNnSc7	rozčarování
<g/>
,	,	kIx,	,
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
prý	prý	k9	prý
dal	dát	k5eAaPmAgMnS	dát
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vedení	vedení	k1gNnSc1	vedení
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
slitovalo	slitovat	k5eAaPmAgNnS	slitovat
a	a	k8xC	a
přeřadilo	přeřadit	k5eAaPmAgNnS	přeřadit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
kontrolu	kontrola	k1gFnSc4	kontrola
dováženého	dovážený	k2eAgInSc2d1	dovážený
čpavku	čpavek	k1gInSc2	čpavek
<g/>
,	,	kIx,	,
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
obchodní	obchodní	k2eAgFnSc2d1	obchodní
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
plasty	plast	k1gInPc4	plast
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
také	také	k9	také
pomocné	pomocný	k2eAgInPc4d1	pomocný
gumárenské	gumárenský	k2eAgInPc4d1	gumárenský
přípravky	přípravek	k1gInPc4	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
začal	začít	k5eAaPmAgMnS	začít
jeho	jeho	k3xOp3gInSc4	jeho
rychlý	rychlý	k2eAgInSc4d1	rychlý
kariérní	kariérní	k2eAgInSc4d1	kariérní
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Prošel	projít	k5eAaPmAgMnS	projít
funkcemi	funkce	k1gFnPc7	funkce
samostatného	samostatný	k2eAgMnSc2d1	samostatný
referenta	referent	k1gMnSc2	referent
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k1gMnSc2	vedoucí
referátu	referát	k1gInSc2	referát
a	a	k8xC	a
už	už	k6eAd1	už
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
zástupce	zástupce	k1gMnSc2	zástupce
ředitele	ředitel	k1gMnSc2	ředitel
této	tento	k3xDgFnSc2	tento
obchodní	obchodní	k2eAgFnSc2d1	obchodní
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kariérním	kariérní	k2eAgInSc6d1	kariérní
růstu	růst	k1gInSc6	růst
Babišovi	Babišův	k2eAgMnPc1d1	Babišův
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
jeho	jeho	k3xOp3gFnSc1	jeho
píle	píle	k1gFnSc1	píle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
politická	politický	k2eAgFnSc1d1	politická
angažovanost	angažovanost	k1gFnSc1	angažovanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
vedl	vést	k5eAaImAgMnS	vést
základní	základní	k2eAgFnSc4d1	základní
organizaci	organizace	k1gFnSc4	organizace
SSM	SSM	kA	SSM
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Babiš	Babiš	k1gMnSc1	Babiš
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
Antonem	Anton	k1gMnSc7	Anton
Rakickým	Rakický	k2eAgMnSc7d1	Rakický
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gMnSc7	jeho
ochráncem	ochránce	k1gMnSc7	ochránce
(	(	kIx(	(
<g/>
Rakický	Rakický	k2eAgInSc1d1	Rakický
byl	být	k5eAaImAgInS	být
šéfem	šéf	k1gMnSc7	šéf
32	[number]	k4	32
<g/>
.	.	kIx.	.
obchodní	obchodní	k2eAgFnSc2d1	obchodní
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
celého	celý	k2eAgInSc2d1	celý
Petrimexu	Petrimex	k1gInSc2	Petrimex
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
vzniku	vznik	k1gInSc2	vznik
Agrofertu	Agrofert	k1gInSc2	Agrofert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kádrovou	kádrový	k2eAgFnSc7d1	kádrová
rezervou	rezerva	k1gFnSc7	rezerva
na	na	k7c4	na
vycestování	vycestování	k1gNnSc4	vycestování
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
znalosti	znalost	k1gFnSc3	znalost
francouzštiny	francouzština	k1gFnSc2	francouzština
připadaly	připadat	k5eAaPmAgInP	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
severoafrické	severoafrický	k2eAgFnSc2d1	severoafrická
země	zem	k1gFnSc2	zem
obchodující	obchodující	k2eAgInPc4d1	obchodující
s	s	k7c7	s
fosfáty	fosfát	k1gInPc7	fosfát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
delegátem	delegát	k1gMnSc7	delegát
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
===	===	k?	===
</s>
</p>
<p>
<s>
Vycestovat	vycestovat	k5eAaPmF	vycestovat
pracovně	pracovně	k6eAd1	pracovně
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
Babišova	Babišův	k2eAgNnSc2d1	Babišovo
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
podání	podání	k1gNnSc2	podání
jeho	jeho	k3xOp3gInSc1	jeho
dávný	dávný	k2eAgInSc1d1	dávný
sen	sen	k1gInSc1	sen
už	už	k6eAd1	už
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1985	[number]	k4	1985
byl	být	k5eAaImAgInS	být
Babiš	Babiš	k1gInSc1	Babiš
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
marockého	marocký	k2eAgNnSc2d1	marocké
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Rabatu	rabat	k1gInSc2	rabat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
československým	československý	k2eAgMnSc7d1	československý
obchodním	obchodní	k2eAgMnSc7d1	obchodní
delegátem	delegát	k1gMnSc7	delegát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
desítku	desítka	k1gFnSc4	desítka
podniků	podnik	k1gInPc2	podnik
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
nakupoval	nakupovat	k5eAaBmAgMnS	nakupovat
marocké	marocký	k2eAgInPc4d1	marocký
fosfáty	fosfát	k1gInPc4	fosfát
a	a	k8xC	a
sjednával	sjednávat	k5eAaImAgInS	sjednávat
kontrakty	kontrakt	k1gInPc4	kontrakt
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
československého	československý	k2eAgNnSc2d1	Československé
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
popisu	popis	k1gInSc2	popis
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
kolegů	kolega	k1gMnPc2	kolega
se	se	k3xPyFc4	se
Babiš	Babiš	k1gInSc1	Babiš
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
"	"	kIx"	"
<g/>
choval	chovat	k5eAaImAgMnS	chovat
mimořádně	mimořádně	k6eAd1	mimořádně
profesionálně	profesionálně	k6eAd1	profesionálně
a	a	k8xC	a
nekomunisticky	komunisticky	k6eNd1	komunisticky
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
západní	západní	k2eAgMnSc1d1	západní
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
perfektně	perfektně	k6eAd1	perfektně
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
oblékal	oblékat	k5eAaImAgMnS	oblékat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgFnPc4d1	získaná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
obchodních	obchodní	k2eAgNnPc2d1	obchodní
jednání	jednání	k1gNnPc2	jednání
západního	západní	k2eAgInSc2d1	západní
stylu	styl	k1gInSc2	styl
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
budoucnost	budoucnost	k1gFnSc4	budoucnost
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
devizou	deviza	k1gFnSc7	deviza
<g/>
.	.	kIx.	.
<g/>
Babiš	Babiš	k1gMnSc1	Babiš
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgInSc4d2	vyšší
příjem	příjem	k1gInSc4	příjem
než	než	k8xS	než
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
cizí	cizí	k2eAgFnSc6d1	cizí
měně	měna	k1gFnSc6	měna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
delegát	delegát	k1gMnSc1	delegát
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
s	s	k7c7	s
ostrahou	ostraha	k1gFnSc7	ostraha
a	a	k8xC	a
život	život	k1gInSc4	život
v	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
(	(	kIx(	(
<g/>
slunné	slunný	k2eAgNnSc1d1	slunné
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
blízkost	blízkost	k1gFnSc1	blízkost
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
velmi	velmi	k6eAd1	velmi
líbil	líbit	k5eAaImAgInS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
si	se	k3xPyFc3	se
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
síť	síť	k1gFnSc4	síť
obchodních	obchodní	k2eAgFnPc2d1	obchodní
známostí	známost	k1gFnPc2	známost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
díky	díky	k7c3	díky
sportovním	sportovní	k2eAgInPc3d1	sportovní
úspěchům	úspěch	k1gInPc3	úspěch
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
několikrát	několikrát	k6eAd1	několikrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
země	zem	k1gFnSc2	zem
<g/>
[	[	kIx(	[
<g/>
jakou	jaký	k3yRgFnSc7	jaký
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
tenisu	tenis	k1gInSc6	tenis
do	do	k7c2	do
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
prý	prý	k9	prý
Babiš	Babiš	k1gMnSc1	Babiš
nechtěl	chtít	k5eNaImAgMnS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
ani	ani	k8xC	ani
rok	rok	k1gInSc4	rok
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
dostal	dostat	k5eAaPmAgMnS	dostat
atraktivní	atraktivní	k2eAgFnSc4d1	atraktivní
nabídku	nabídka	k1gFnSc4	nabídka
vést	vést	k5eAaImF	vést
obchody	obchod	k1gInPc4	obchod
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
doly	dol	k1gInPc4	dol
rasolu	rasol	k1gInSc2	rasol
(	(	kIx(	(
<g/>
severoafrického	severoafrický	k2eAgInSc2d1	severoafrický
léčebného	léčebný	k2eAgInSc2d1	léčebný
jílu	jíl	k1gInSc2	jíl
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
nepropustné	propustný	k2eNgInPc4d1	nepropustný
obaly	obal	k1gInPc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gMnSc1	jeho
ochránce	ochránce	k1gMnSc1	ochránce
Anton	Anton	k1gMnSc1	Anton
Rakický	Rakický	k2eAgMnSc1d1	Rakický
se	se	k3xPyFc4	se
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
Petrimexu	Petrimex	k1gInSc2	Petrimex
<g/>
.	.	kIx.	.
</s>
<s>
Obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
na	na	k7c4	na
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
funkci	funkce	k1gFnSc6	funkce
ředitele	ředitel	k1gMnSc2	ředitel
32	[number]	k4	32
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sám	sám	k3xTgMnSc1	sám
kdysi	kdysi	k6eAd1	kdysi
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
nakonec	nakonec	k6eAd1	nakonec
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
on	on	k3xPp3gInSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
Babišovou	Babišův	k2eAgFnSc7d1	Babišova
manželkou	manželka	k1gFnSc7	manželka
spolužačka	spolužačka	k1gFnSc1	spolužačka
z	z	k7c2	z
gymnázia	gymnázium	k1gNnSc2	gymnázium
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
lékařka	lékařka	k1gFnSc1	lékařka
Beata	Beata	k1gFnSc1	Beata
(	(	kIx(	(
<g/>
Beatrice	Beatrice	k1gFnSc1	Beatrice
<g/>
)	)	kIx)	)
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Adamovičová	Adamovičová	k1gFnSc1	Adamovičová
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dceru	dcera	k1gFnSc4	dcera
Adrianu	Adriana	k1gFnSc4	Adriana
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Andreje	Andrej	k1gMnSc4	Andrej
<g/>
.	.	kIx.	.
</s>
<s>
Manžel	manžel	k1gMnSc1	manžel
dcery	dcera	k1gFnSc2	dcera
Adriany	Adriana	k1gFnSc2	Adriana
Martin	Martin	k1gMnSc1	Martin
Bobek	Bobek	k1gMnSc1	Bobek
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
Agrofertu	Agrofert	k1gInSc2	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Andrej	Andrej	k1gMnSc1	Andrej
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dopravní	dopravní	k2eAgInSc4d1	dopravní
pilot	pilot	k1gInSc4	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Adriana	Adriana	k1gFnSc1	Adriana
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
statutárních	statutární	k2eAgInPc6d1	statutární
orgánech	orgán	k1gInPc6	orgán
Babišovy	Babišův	k2eAgFnSc2d1	Babišova
Imoby	Imoba	k1gFnSc2	Imoba
i	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
Farma	farma	k1gFnSc1	farma
Čapí	čapět	k5eAaImIp3nS	čapět
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
trestně	trestně	k6eAd1	trestně
stíhány	stíhat	k5eAaImNgInP	stíhat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
kauzou	kauza	k1gFnSc7	kauza
Čapí	čapí	k2eAgNnSc4d1	čapí
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Babiš	Babiš	k1gMnSc1	Babiš
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
o	o	k7c4	o
20	[number]	k4	20
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgMnSc1d2	mladší
družkou	družka	k1gFnSc7	družka
Monikou	Monika	k1gFnSc7	Monika
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Herodesovou	Herodesový	k2eAgFnSc4d1	Herodesová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Vivien	Vivien	k1gInSc4	Vivien
a	a	k8xC	a
Frederika	Frederik	k1gMnSc2	Frederik
<g/>
.	.	kIx.	.
</s>
<s>
Monika	Monika	k1gFnSc1	Monika
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
změnila	změnit	k5eAaPmAgFnS	změnit
příjmení	příjmení	k1gNnSc4	příjmení
na	na	k7c4	na
Babišová	Babišový	k2eAgNnPc4d1	Babišový
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
Čapím	čapí	k2eAgNnSc6d1	čapí
hnízdě	hnízdo	k1gNnSc6	hnízdo
velká	velký	k2eAgFnSc1d1	velká
svatba	svatba	k1gFnSc1	svatba
Andreje	Andrej	k1gMnSc2	Andrej
s	s	k7c7	s
Monikou	Monika	k1gFnSc7	Monika
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
rozhovorech	rozhovor	k1gInPc6	rozhovor
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Evidence	evidence	k1gFnSc2	evidence
Státní	státní	k2eAgFnSc2d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
dokumentů	dokument	k1gInPc2	dokument
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
archívu	archív	k1gInSc2	archív
slovenského	slovenský	k2eAgInSc2d1	slovenský
Ústavu	ústav	k1gInSc2	ústav
paměti	paměť	k1gFnSc2	paměť
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
ÚPN	ÚPN	kA	ÚPN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1980	[number]	k4	1980
evidován	evidován	k2eAgMnSc1d1	evidován
jako	jako	k8xS	jako
důvěrník	důvěrník	k1gMnSc1	důvěrník
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
StB	StB	k1gMnSc1	StB
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
25085	[number]	k4	25085
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1982	[number]	k4	1982
v	v	k7c6	v
bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
vinárně	vinárna	k1gFnSc6	vinárna
U	u	k7c2	u
obuvníka	obuvník	k1gMnSc2	obuvník
měl	mít	k5eAaImAgMnS	mít
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kpt.	kpt.	k?	kpt.
Rastislava	Rastislav	k1gMnSc2	Rastislav
Mátraye	Mátray	k1gMnSc2	Mátray
a	a	k8xC	a
por.	por.	k?	por.
Júliuse	Júliuse	k1gFnSc1	Júliuse
Šumana	Šumana	k1gFnSc1	Šumana
podepsat	podepsat	k5eAaPmF	podepsat
vázací	vázací	k2eAgInSc4d1	vázací
akt	akt	k1gInSc4	akt
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
agentem	agent	k1gMnSc7	agent
StB	StB	k1gFnSc2	StB
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Bureš	Bureš	k1gMnSc1	Bureš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Registrován	registrován	k2eAgInSc1d1	registrován
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
Oddělení	oddělení	k1gNnSc2	oddělení
kontrarozvědné	kontrarozvědný	k2eAgFnSc2d1	kontrarozvědná
ochrany	ochrana	k1gFnSc2	ochrana
československého	československý	k2eAgInSc2d1	československý
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
Odboru	odbor	k1gInSc2	odbor
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Správy	správa	k1gFnSc2	správa
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
III	III	kA	III
<g/>
.	.	kIx.	.
odboru	odbor	k1gInSc2	odbor
XII	XII	kA	XII
<g/>
.	.	kIx.	.
správy	správa	k1gFnSc2	správa
ZNB	ZNB	kA	ZNB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
oddělení	oddělení	k1gNnSc1	oddělení
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
sledování	sledování	k1gNnSc2	sledování
zastupitelských	zastupitelský	k2eAgFnPc2d1	zastupitelská
organizací	organizace	k1gFnPc2	organizace
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
výstavy	výstava	k1gFnPc1	výstava
a	a	k8xC	a
veletrhy	veletrh	k1gInPc1	veletrh
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
monitorování	monitorování	k1gNnSc4	monitorování
činnosti	činnost	k1gFnSc2	činnost
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
rozvědek	rozvědka	k1gFnPc2	rozvědka
v	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
obchodě	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Dochované	dochovaný	k2eAgFnPc1d1	dochovaná
listiny	listina	k1gFnPc1	listina
přitom	přitom	k6eAd1	přitom
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babiš	Babiš	k1gInSc1	Babiš
se	s	k7c7	s
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
vědomě	vědomě	k6eAd1	vědomě
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
již	již	k6eAd1	již
jako	jako	k8xS	jako
důvěrník	důvěrník	k1gMnSc1	důvěrník
<g/>
.	.	kIx.	.
<g/>
Agentem	agent	k1gMnSc7	agent
StB	StB	k1gMnSc7	StB
byl	být	k5eAaImAgMnS	být
Babiš	Babiš	k1gMnSc1	Babiš
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gInSc4	on
PZO	PZO	kA	PZO
Petrimex	Petrimex	k1gInSc4	Petrimex
vyslal	vyslat	k5eAaPmAgMnS	vyslat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
;	;	kIx,	;
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
StB	StB	k1gFnSc2	StB
se	se	k3xPyFc4	se
za	za	k7c4	za
uvedené	uvedený	k2eAgNnSc4d1	uvedené
období	období	k1gNnSc4	období
setkal	setkat	k5eAaPmAgInS	setkat
celkem	celkem	k6eAd1	celkem
sedmnáctkrát	sedmnáctkrát	k6eAd1	sedmnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
Krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Bureš	Bureš	k1gMnSc1	Bureš
<g/>
"	"	kIx"	"
figuruje	figurovat	k5eAaImIp3nS	figurovat
nejméně	málo	k6eAd3	málo
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
dalších	další	k2eAgInPc6d1	další
svazcích	svazek	k1gInPc6	svazek
StB	StB	k1gFnSc2	StB
–	–	k?	–
jednak	jednak	k8xC	jednak
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
"	"	kIx"	"
<g/>
Oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Bureš	Bureš	k1gMnSc1	Bureš
<g/>
"	"	kIx"	"
nejméně	málo	k6eAd3	málo
dvakrát	dvakrát	k6eAd1	dvakrát
zmíněn	zmíněn	k2eAgMnSc1d1	zmíněn
jako	jako	k8xS	jako
autor	autor	k1gMnSc1	autor
hlášení	hlášení	k1gNnSc2	hlášení
určených	určený	k2eAgMnPc2d1	určený
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
"	"	kIx"	"
<g/>
Voják	voják	k1gMnSc1	voják
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
figuruje	figurovat	k5eAaImIp3nS	figurovat
jako	jako	k9	jako
opakovaný	opakovaný	k2eAgMnSc1d1	opakovaný
návštěvník	návštěvník	k1gMnSc1	návštěvník
konspiračního	konspirační	k2eAgInSc2d1	konspirační
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
mluvčího	mluvčí	k1gMnSc2	mluvčí
Agrofertu	Agrofert	k1gInSc2	Agrofert
Karla	Karel	k1gMnSc2	Karel
Hanzelky	Hanzelka	k1gMnSc2	Hanzelka
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
schůzek	schůzka	k1gFnPc2	schůzka
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babiš	Babiš	k1gMnSc1	Babiš
odmítal	odmítat	k5eAaImAgMnS	odmítat
dovážet	dovážet	k5eAaImF	dovážet
nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
fosfáty	fosfát	k1gInPc4	fosfát
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Babiš	Babiš	k1gMnSc1	Babiš
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
StB	StB	k1gFnSc7	StB
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
.	.	kIx.	.
</s>
<s>
Obvinění	obviněný	k1gMnPc1	obviněný
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
StB	StB	k1gFnSc2	StB
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
předvolební	předvolební	k2eAgInSc4d1	předvolební
boj	boj	k1gInSc4	boj
a	a	k8xC	a
Ústav	ústav	k1gInSc1	ústav
paměti	paměť	k1gFnSc2	paměť
národa	národ	k1gInSc2	národ
zažaloval	zažalovat	k5eAaPmAgInS	zažalovat
<g/>
.	.	kIx.	.
<g/>
Ústav	ústav	k1gInSc1	ústav
paměti	paměť	k1gFnSc2	paměť
národa	národ	k1gInSc2	národ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravdivost	pravdivost	k1gFnSc4	pravdivost
archivovaných	archivovaný	k2eAgInPc2d1	archivovaný
dokumentů	dokument	k1gInPc2	dokument
nijak	nijak	k6eAd1	nijak
neověřuje	ověřovat	k5eNaImIp3nS	ověřovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ale	ale	k8xC	ale
ředitel	ředitel	k1gMnSc1	ředitel
ústavu	ústav	k1gInSc2	ústav
Ondrej	Ondrej	k1gMnSc1	Ondrej
Krajňák	Krajňák	k1gMnSc1	Krajňák
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
obecně	obecně	k6eAd1	obecně
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
evidence	evidence	k1gFnPc1	evidence
byly	být	k5eAaImAgFnP	být
vykonstruované	vykonstruovaný	k2eAgFnPc1d1	vykonstruovaná
<g/>
,	,	kIx,	,
když	když	k8xS	když
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
existovaly	existovat	k5eAaImAgFnP	existovat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Také	také	k9	také
podle	podle	k7c2	podle
Radka	Radek	k1gMnSc2	Radek
Schovánka	Schovánek	k1gMnSc2	Schovánek
z	z	k7c2	z
analogického	analogický	k2eAgInSc2d1	analogický
českého	český	k2eAgInSc2d1	český
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
zachované	zachovaný	k2eAgInPc1d1	zachovaný
dokumenty	dokument	k1gInPc1	dokument
Ústavu	ústav	k1gInSc2	ústav
paměti	paměť	k1gFnSc2	paměť
národa	národ	k1gInSc2	národ
padělkem	padělek	k1gInSc7	padělek
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
Bratislava	Bratislava	k1gFnSc1	Bratislava
I	i	k9	i
nepravomocně	pravomocně	k6eNd1	pravomocně
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
ve	v	k7c6	v
svazcích	svazek	k1gInPc6	svazek
StB	StB	k1gFnSc2	StB
evidován	evidovat	k5eAaImNgInS	evidovat
neoprávněně	oprávněně	k6eNd1	oprávněně
<g/>
.	.	kIx.	.
</s>
<s>
Soudkyně	soudkyně	k1gFnSc1	soudkyně
v	v	k7c6	v
odůvodnění	odůvodnění	k1gNnSc6	odůvodnění
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Soud	soud	k1gInSc1	soud
neměl	mít	k5eNaImAgInS	mít
za	za	k7c4	za
prokázané	prokázaný	k2eAgNnSc4d1	prokázané
<g/>
,	,	kIx,	,
že	že	k8xS	že
navrhovatel	navrhovatel	k1gMnSc1	navrhovatel
vědomě	vědomě	k6eAd1	vědomě
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
agentem	agent	k1gMnSc7	agent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
paměti	paměť	k1gFnSc2	paměť
národa	národ	k1gInSc2	národ
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
odvolal	odvolat	k5eAaPmAgInS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
původní	původní	k2eAgInSc1d1	původní
rozsudek	rozsudek	k1gInSc4	rozsudek
koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
ÚPN	ÚPN	kA	ÚPN
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
podal	podat	k5eAaPmAgMnS	podat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
dovolání	dovolání	k1gNnPc2	dovolání
k	k	k7c3	k
slovenskému	slovenský	k2eAgInSc3d1	slovenský
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
nižších	nízký	k2eAgFnPc2d2	nižší
instancí	instance	k1gFnPc2	instance
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
nezákonných	zákonný	k2eNgInPc6d1	nezákonný
důkazech	důkaz	k1gInPc6	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
ÚPN	ÚPN	kA	ÚPN
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
rozsudku	rozsudek	k1gInSc2	rozsudek
byl	být	k5eAaImAgInS	být
Babiš	Babiš	k1gInSc1	Babiš
ve	v	k7c6	v
svazcích	svazek	k1gInPc6	svazek
StB	StB	k1gFnSc2	StB
evidován	evidovat	k5eAaImNgInS	evidovat
neoprávněně	oprávněně	k6eNd1	oprávněně
<g/>
.	.	kIx.	.
<g/>
ÚPN	ÚPN	kA	ÚPN
následně	následně	k6eAd1	následně
podal	podat	k5eAaPmAgMnS	podat
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
stížnost	stížnost	k1gFnSc1	stížnost
k	k	k7c3	k
slovenskému	slovenský	k2eAgInSc3d1	slovenský
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
předchozí	předchozí	k2eAgFnSc2d1	předchozí
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
soudů	soud	k1gInPc2	soud
zrušil	zrušit	k5eAaPmAgInS	zrušit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
procesním	procesní	k2eAgFnPc3d1	procesní
vadám	vada	k1gFnPc3	vada
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
případ	případ	k1gInSc1	případ
krajskému	krajský	k2eAgInSc3d1	krajský
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Napadeno	napaden	k2eAgNnSc1d1	napadeno
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žalovanou	žalovaný	k2eAgFnSc7d1	žalovaná
stranou	strana	k1gFnSc7	strana
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
o	o	k7c6	o
evidenci	evidence	k1gFnSc6	evidence
osob	osoba	k1gFnPc2	osoba
StB	StB	k1gFnPc2	StB
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
Ústav	ústav	k1gInSc1	ústav
paměti	paměť	k1gFnSc2	paměť
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dovozovaly	dovozovat	k5eAaImAgInP	dovozovat
slovenské	slovenský	k2eAgInPc1d1	slovenský
obecné	obecný	k2eAgInPc1d1	obecný
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
i	i	k9	i
nezbavení	nezbavení	k1gNnSc1	nezbavení
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
tří	tři	k4xCgMnPc2	tři
bývalých	bývalý	k2eAgMnPc2d1	bývalý
členů	člen	k1gMnPc2	člen
StB	StB	k1gFnSc2	StB
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
plynula	plynout	k5eAaImAgFnS	plynout
nezákonnost	nezákonnost	k1gFnSc1	nezákonnost
výpovědí	výpověď	k1gFnPc2	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ústním	ústní	k2eAgInSc6d1	ústní
komentáři	komentář	k1gInSc6	komentář
nálezu	nález	k1gInSc2	nález
také	také	k9	také
zaznělo	zaznít	k5eAaPmAgNnS	zaznít
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušníci	příslušník	k1gMnPc1	příslušník
StB	StB	k1gFnSc2	StB
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
nedůvěryhodní	důvěryhodný	k2eNgMnPc1d1	nedůvěryhodný
<g/>
,	,	kIx,	,
když	když	k8xS	když
svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
spoluprosazovali	spoluprosazovat	k5eAaImAgMnP	spoluprosazovat
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
důvěryhodnost	důvěryhodnost	k1gFnSc4	důvěryhodnost
obecné	obecný	k2eAgInPc1d1	obecný
soudy	soud	k1gInPc1	soud
dostatečně	dostatečně	k6eAd1	dostatečně
nezkoumaly	zkoumat	k5eNaImAgInP	zkoumat
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
dostatečně	dostatečně	k6eAd1	dostatečně
nezdůvodnily	zdůvodnit	k5eNaPmAgFnP	zdůvodnit
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
čeho	co	k3yRnSc2	co
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gFnPc1	jejich
výpovědi	výpověď	k1gFnPc1	výpověď
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
důvěryhodné	důvěryhodný	k2eAgInPc4d1	důvěryhodný
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rozsudkem	rozsudek	k1gInSc7	rozsudek
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
žalobu	žaloba	k1gFnSc4	žaloba
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
na	na	k7c4	na
Ústav	ústav	k1gInSc4	ústav
paměti	paměť	k1gFnSc2	paměť
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
počátkem	počátkem	k7c2	počátkem
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
podal	podat	k5eAaPmAgMnS	podat
Babiš	Babiš	k1gInSc4	Babiš
proti	proti	k7c3	proti
pravomocnému	pravomocný	k2eAgInSc3d1	pravomocný
rozsudku	rozsudek	k1gInSc3	rozsudek
dovolání	dovolání	k1gNnSc2	dovolání
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zamítnuto	zamítnout	k5eAaPmNgNnS	zamítnout
a	a	k8xC	a
Babiš	Babiš	k1gMnSc1	Babiš
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodlá	hodlat	k5eAaImIp3nS	hodlat
Slovensko	Slovensko	k1gNnSc4	Slovensko
žalovat	žalovat	k5eAaImF	žalovat
u	u	k7c2	u
Evropského	evropský	k2eAgInSc2d1	evropský
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
úmysl	úmysl	k1gInSc4	úmysl
Babiš	Babiš	k1gMnSc1	Babiš
dle	dle	k7c2	dle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
už	už	k6eAd1	už
oznámil	oznámit	k5eAaPmAgInS	oznámit
slovenskému	slovenský	k2eAgMnSc3d1	slovenský
premiérovi	premiér	k1gMnSc3	premiér
Peterovi	Peter	k1gMnSc3	Peter
Pellegrinimu	Pellegrinim	k1gMnSc3	Pellegrinim
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgNnPc1d1	slovenské
ministerstva	ministerstvo	k1gNnPc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
avizovala	avizovat	k5eAaBmAgFnS	avizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
žaloby	žaloba	k1gFnSc2	žaloba
budou	být	k5eAaImBp3nP	být
Slovensko	Slovensko	k1gNnSc4	Slovensko
před	před	k7c7	před
evropským	evropský	k2eAgInSc7d1	evropský
soudem	soud	k1gInSc7	soud
hájit	hájit	k5eAaImF	hájit
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
skutečně	skutečně	k6eAd1	skutečně
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
podal	podat	k5eAaPmAgMnS	podat
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
stížnost	stížnost	k1gFnSc1	stížnost
na	na	k7c4	na
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
soud	soud	k1gInSc4	soud
však	však	k9	však
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
nepřijatelnosti	nepřijatelnost	k1gFnSc6	nepřijatelnost
stížnosti	stížnost	k1gFnSc2	stížnost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
zažaloval	zažalovat	k5eAaPmAgMnS	zažalovat
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
o	o	k7c4	o
milión	milión	k4xCgInSc4	milión
eur	euro	k1gNnPc2	euro
slovenský	slovenský	k2eAgInSc1d1	slovenský
deník	deník	k1gInSc1	deník
Nový	nový	k2eAgInSc1d1	nový
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc4d1	bývalý
příslušníka	příslušník	k1gMnSc4	příslušník
československé	československý	k2eAgFnSc2d1	Československá
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
StB	StB	k1gMnSc2	StB
<g/>
)	)	kIx)	)
Jána	Ján	k1gMnSc2	Ján
Sarkocyho	Sarkocy	k1gMnSc2	Sarkocy
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
rozhovor	rozhovor	k1gInSc4	rozhovor
deník	deník	k1gInSc1	deník
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
Babiše	Babiš	k1gMnSc4	Babiš
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
vědomého	vědomý	k2eAgMnSc4d1	vědomý
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
StB	StB	k1gFnSc2	StB
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
tak	tak	k6eAd1	tak
podle	podle	k7c2	podle
Babiše	Babiše	k1gFnSc2	Babiše
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
vytvářet	vytvářet	k5eAaImF	vytvářet
úmyslně	úmyslně	k6eAd1	úmyslně
"	"	kIx"	"
<g/>
nepravdivý	pravdivý	k2eNgInSc1d1	nepravdivý
obraz	obraz	k1gInSc1	obraz
vypočítavého	vypočítavý	k2eAgMnSc2d1	vypočítavý
<g/>
,	,	kIx,	,
nečestného	čestný	k2eNgMnSc2d1	nečestný
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1	vydavatel
se	se	k3xPyFc4	se
omluvit	omluvit	k5eAaPmF	omluvit
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babiš	Babiš	k1gMnSc1	Babiš
neprokázal	prokázat	k5eNaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
evidován	evidovat	k5eAaImNgInS	evidovat
u	u	k7c2	u
StB	StB	k1gFnPc2	StB
neoprávněně	oprávněně	k6eNd1	oprávněně
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
žalobu	žaloba	k1gFnSc4	žaloba
proti	proti	k7c3	proti
deníku	deník	k1gInSc3	deník
i	i	k9	i
proti	proti	k7c3	proti
Sarkocymu	Sarkocym	k1gInSc2	Sarkocym
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Sarkocy	Sarkocy	k1gInPc1	Sarkocy
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babiš	Babiš	k1gMnSc1	Babiš
neunesl	unést	k5eNaPmAgMnS	unést
důkazní	důkazní	k2eAgNnSc4d1	důkazní
břemeno	břemeno	k1gNnSc4	břemeno
a	a	k8xC	a
žalobu	žaloba	k1gFnSc4	žaloba
raději	rád	k6eAd2	rád
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
tak	tak	k6eAd1	tak
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vědomým	vědomý	k2eAgMnSc7d1	vědomý
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
StB	StB	k1gMnSc7	StB
a	a	k8xC	a
dostával	dostávat	k5eAaImAgMnS	dostávat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
zaplaceno	zaplatit	k5eAaPmNgNnS	zaplatit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnikání	podnikání	k1gNnSc1	podnikání
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Agrofert	Agrofert	k1gInSc4	Agrofert
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
domluvili	domluvit	k5eAaPmAgMnP	domluvit
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Babiš	Babiš	k1gMnSc1	Babiš
zřízení	zřízení	k1gNnSc2	zřízení
kanceláře	kancelář	k1gFnSc2	kancelář
Petrimexu	Petrimex	k1gInSc2	Petrimex
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
kanceláře	kancelář	k1gFnSc2	kancelář
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
samostatný	samostatný	k2eAgInSc1d1	samostatný
podnik	podnik	k1gInSc1	podnik
AGROFERT	AGROFERT	kA	AGROFERT
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
základního	základní	k2eAgInSc2d1	základní
kapitálu	kapitál	k1gInSc2	kapitál
Agrofertu	Agrofert	k1gInSc2	Agrofert
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
majoritním	majoritní	k2eAgMnSc7d1	majoritní
vlastníkem	vlastník	k1gMnSc7	vlastník
stala	stát	k5eAaPmAgFnS	stát
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
společnost	společnost	k1gFnSc1	společnost
O.	O.	kA	O.
<g/>
F.	F.	kA	F.
<g/>
I.V	I.V	k1gFnSc1	I.V
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
Agrofert	Agrofert	k1gInSc4	Agrofert
akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
největším	veliký	k2eAgInSc7d3	veliký
českým	český	k2eAgInSc7d1	český
zemědělským	zemědělský	k2eAgInSc7d1	zemědělský
<g/>
,	,	kIx,	,
potravinářským	potravinářský	k2eAgInSc7d1	potravinářský
a	a	k8xC	a
chemickým	chemický	k2eAgInSc7d1	chemický
holdingem	holding	k1gInSc7	holding
<g/>
.	.	kIx.	.
</s>
<s>
Holdingová	holdingový	k2eAgFnSc1d1	holdingová
společnost	společnost	k1gFnSc1	společnost
ovládá	ovládat	k5eAaImIp3nS	ovládat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
230	[number]	k4	230
právně	právně	k6eAd1	právně
samostatných	samostatný	k2eAgFnPc2d1	samostatná
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
konsolidované	konsolidovaný	k2eAgFnPc1d1	konsolidovaná
tržby	tržba	k1gFnPc1	tržba
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
téměř	téměř	k6eAd1	téměř
132,5	[number]	k4	132,5
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
firmy	firma	k1gFnPc4	firma
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
nájmu	nájem	k1gInSc6	nájem
57	[number]	k4	57
tisíc	tisíc	k4xCgInPc2	tisíc
hektarů	hektar	k1gInPc2	hektar
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
(	(	kIx(	(
<g/>
0,7	[number]	k4	0,7
procenta	procento	k1gNnSc2	procento
území	území	k1gNnSc2	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nebo	nebo	k8xC	nebo
1,6	[number]	k4	1,6
procenta	procento	k1gNnSc2	procento
veškeré	veškerý	k3xTgFnSc2	veškerý
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
portfolia	portfolio	k1gNnSc2	portfolio
Agrofertu	Agrofert	k1gInSc2	Agrofert
patří	patřit	k5eAaImIp3nS	patřit
kromě	kromě	k7c2	kromě
chemické	chemický	k2eAgFnSc2d1	chemická
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
potravinářských	potravinářský	k2eAgInPc2d1	potravinářský
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
také	také	k9	také
vydavatelská	vydavatelský	k2eAgFnSc1d1	vydavatelská
firma	firma	k1gFnSc1	firma
AGF	AGF	kA	AGF
Media	medium	k1gNnSc2	medium
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
od	od	k7c2	od
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
také	také	k9	také
Agrofert	Agrofert	k1gInSc1	Agrofert
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
velký	velký	k2eAgInSc1d1	velký
mediální	mediální	k2eAgInSc1d1	mediální
koncern	koncern	k1gInSc1	koncern
MAFRA	MAFRA	kA	MAFRA
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vydával	vydávat	k5eAaPmAgInS	vydávat
dva	dva	k4xCgInPc1	dva
celostátní	celostátní	k2eAgInPc1d1	celostátní
tištěné	tištěný	k2eAgInPc1d1	tištěný
deníky	deník	k1gInPc1	deník
(	(	kIx(	(
<g/>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Mladou	mladý	k2eAgFnSc4d1	mladá
frontu	fronta	k1gFnSc4	fronta
DNES	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInSc4d1	regionální
deník	deník	k1gInSc4	deník
Metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
provozoval	provozovat	k5eAaImAgMnS	provozovat
tři	tři	k4xCgFnPc4	tři
televizní	televizní	k2eAgFnPc4d1	televizní
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
zpravodajské	zpravodajský	k2eAgInPc4d1	zpravodajský
servery	server	k1gInPc4	server
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
pozice	pozice	k1gFnSc1	pozice
a	a	k8xC	a
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
také	také	k6eAd1	také
virtuálního	virtuální	k2eAgMnSc4d1	virtuální
mobilního	mobilní	k2eAgMnSc4d1	mobilní
operátora	operátor	k1gMnSc4	operátor
Mobil	mobil	k1gInSc4	mobil
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
<g/>
Agrofert	Agrofert	k1gMnSc1	Agrofert
má	mít	k5eAaImIp3nS	mít
pobočku	pobočka	k1gFnSc4	pobočka
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
společnosti	společnost	k1gFnPc4	společnost
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledně	posledně	k6eAd1	posledně
jmenované	jmenovaný	k2eAgFnSc6d1	jmenovaná
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
velký	velký	k2eAgInSc1d1	velký
chemický	chemický	k2eAgInSc1d1	chemický
závod	závod	k1gInSc1	závod
SKW	SKW	kA	SKW
Piesteritz	Piesteritz	k1gInSc1	Piesteritz
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Lutherstadt	Lutherstadt	k2eAgInSc1d1	Lutherstadt
Wittenberg	Wittenberg	k1gInSc1	Wittenberg
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Sasko-Anhaltsko	Sasko-Anhaltsko	k1gNnSc1	Sasko-Anhaltsko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
umělých	umělý	k2eAgNnPc2d1	umělé
hnojiv	hnojivo	k1gNnPc2	hnojivo
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
koncernu	koncern	k1gInSc2	koncern
také	také	k9	také
velkopekárna	velkopekárna	k1gFnSc1	velkopekárna
Lieken	Liekna	k1gFnPc2	Liekna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
výroben	výrobna	k1gFnPc2	výrobna
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
dvanáct	dvanáct	k4xCc1	dvanáct
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
sídlo	sídlo	k1gNnSc1	sídlo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
přeneseno	přenést	k5eAaPmNgNnS	přenést
z	z	k7c2	z
Weissenfelsu	Weissenfels	k1gInSc2	Weissenfels
též	též	k9	též
do	do	k7c2	do
Wittenbergu	Wittenberg	k1gInSc2	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
učinil	učinit	k5eAaPmAgMnS	učinit
závazek	závazek	k1gInSc4	závazek
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
proinvestuje	proinvestovat	k5eAaPmIp3nS	proinvestovat
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
celkově	celkově	k6eAd1	celkově
900	[number]	k4	900
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
kritizován	kritizován	k2eAgMnSc1d1	kritizován
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
záběry	záběr	k1gInPc4	záběr
tajnou	tajný	k2eAgFnSc7d1	tajná
kamerou	kamera	k1gFnSc7	kamera
zachycující	zachycující	k2eAgFnSc4d1	zachycující
jeho	jeho	k3xOp3gFnSc4	jeho
schůzku	schůzka	k1gFnSc4	schůzka
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
šéfem	šéf	k1gMnSc7	šéf
antimonopolního	antimonopolní	k2eAgInSc2d1	antimonopolní
úřadu	úřad	k1gInSc2	úřad
Martinem	Martin	k1gMnSc7	Martin
Pecinou	Pecina	k1gMnSc7	Pecina
<g/>
.	.	kIx.	.
</s>
<s>
Schůzka	schůzka	k1gFnSc1	schůzka
v	v	k7c6	v
autosalonu	autosalon	k1gInSc6	autosalon
Mercedes	mercedes	k1gInSc4	mercedes
Forum	forum	k1gNnSc1	forum
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Chodově	Chodov	k1gInSc6	Chodov
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
antimonopolní	antimonopolní	k2eAgInSc1d1	antimonopolní
úřad	úřad	k1gInSc1	úřad
posuzoval	posuzovat	k5eAaImAgInS	posuzovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
Babišova	Babišův	k2eAgInSc2d1	Babišův
koncernu	koncern	k1gInSc2	koncern
Agrofert	Agrofert	k1gInSc1	Agrofert
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Martina	Martin	k1gMnSc2	Martin
Peciny	pecina	k1gFnSc2	pecina
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
podnikatelem	podnikatel	k1gMnSc7	podnikatel
setkával	setkávat	k5eAaImAgMnS	setkávat
opakovaně	opakovaně	k6eAd1	opakovaně
a	a	k8xC	a
schůzka	schůzka	k1gFnSc1	schůzka
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
autosalonu	autosalon	k1gInSc6	autosalon
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
neměl	mít	k5eNaImAgMnS	mít
kancelář	kancelář	k1gFnSc4	kancelář
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pobíraly	pobírat	k5eAaImAgInP	pobírat
Babišem	Babiš	k1gMnSc7	Babiš
vlastněné	vlastněný	k2eAgFnSc2d1	vlastněná
firmy	firma	k1gFnSc2	firma
státní	státní	k2eAgFnSc2d1	státní
dotace	dotace	k1gFnSc2	dotace
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
3	[number]	k4	3
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Babiš	Babiš	k1gInSc1	Babiš
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
odpustilo	odpustit	k5eAaPmAgNnS	odpustit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
Agrofertu	Agrofert	k1gInSc2	Agrofert
daně	daň	k1gFnSc2	daň
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1,476	[number]	k4	1,476
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
Polský	polský	k2eAgMnSc1d1	polský
lobbista	lobbista	k1gMnSc1	lobbista
Jacek	Jacek	k1gMnSc1	Jacek
Spyra	Spyra	k1gMnSc1	Spyra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
přesvědčoval	přesvědčovat	k5eAaImAgInS	přesvědčovat
Babiše	Babiš	k1gInSc2	Babiš
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
chystá	chystat	k5eAaImIp3nS	chystat
jeho	jeho	k3xOp3gNnSc1	jeho
stíhání	stíhání	k1gNnSc1	stíhání
za	za	k7c4	za
údajnou	údajný	k2eAgFnSc4d1	údajná
korupci	korupce	k1gFnSc4	korupce
při	při	k7c6	při
privatizaci	privatizace	k1gFnSc6	privatizace
Unipetrolu	Unipetrol	k1gInSc2	Unipetrol
<g/>
,	,	kIx,	,
a	a	k8xC	a
nabízel	nabízet	k5eAaImAgMnS	nabízet
Babišovi	Babiš	k1gMnSc3	Babiš
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
úplatek	úplatek	k1gInSc4	úplatek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
dovede	dovést	k5eAaPmIp3nS	dovést
toto	tento	k3xDgNnSc1	tento
stíhání	stíhání	k1gNnSc1	stíhání
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
obviněn	obvinit	k5eAaPmNgMnS	obvinit
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
byl	být	k5eAaImAgMnS	být
Spyra	Spyra	k1gMnSc1	Spyra
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadoval	požadovat	k5eAaImAgMnS	požadovat
po	po	k7c6	po
Babišovi	Babiš	k1gMnSc3	Babiš
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
odsouzen	odsouzen	k2eAgInSc4d1	odsouzen
za	za	k7c4	za
podvod	podvod	k1gInSc4	podvod
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
Babiš	Babiš	k1gMnSc1	Babiš
z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
Agrofertu	Agrofert	k1gInSc2	Agrofert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Biopaliva	Biopalivo	k1gNnSc2	Biopalivo
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
schválila	schválit	k5eAaPmAgFnS	schválit
poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
novelu	novela	k1gFnSc4	novela
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tendencemi	tendence	k1gFnPc7	tendence
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
EU	EU	kA	EU
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
povinný	povinný	k2eAgInSc4d1	povinný
podíl	podíl	k1gInSc4	podíl
biopaliv	biopalit	k5eAaPmDgInS	biopalit
v	v	k7c6	v
benzínu	benzín	k1gInSc2	benzín
a	a	k8xC	a
naftě	nafta	k1gFnSc6	nafta
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákon	zákon	k1gInSc1	zákon
pomohl	pomoct	k5eAaPmAgInS	pomoct
prolobbovat	prolobbovat	k5eAaImF	prolobbovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ekonoma	ekonom	k1gMnSc2	ekonom
a	a	k8xC	a
politika	politik	k1gMnSc2	politik
Petra	Petr	k1gMnSc2	Petr
Macha	Mach	k1gMnSc2	Mach
díky	díky	k7c3	díky
přimíchávání	přimíchávání	k1gNnSc3	přimíchávání
biopaliv	biopalit	k5eAaPmDgInS	biopalit
spotřebitelé	spotřebitel	k1gMnPc1	spotřebitel
doplácejí	doplácet	k5eAaImIp3nP	doplácet
asi	asi	k9	asi
dvě	dva	k4xCgFnPc4	dva
koruny	koruna	k1gFnPc4	koruna
na	na	k7c4	na
litr	litr	k1gInSc4	litr
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
údajně	údajně	k6eAd1	údajně
připadne	připadnout	k5eAaPmIp3nS	připadnout
firmám	firma	k1gFnPc3	firma
patřícím	patřící	k2eAgMnSc6d1	patřící
Babišovi	Babiš	k1gMnSc6	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Babiše	Babiš	k1gInSc2	Babiš
biopaliva	biopalivo	k1gNnSc2	biopalivo
zdražují	zdražovat	k5eAaImIp3nP	zdražovat
cenu	cena	k1gFnSc4	cena
nafty	nafta	k1gFnSc2	nafta
maximálně	maximálně	k6eAd1	maximálně
o	o	k7c4	o
46	[number]	k4	46
haléřů	haléř	k1gInPc2	haléř
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
,	,	kIx,	,
benzín	benzín	k1gInSc4	benzín
prý	prý	k9	prý
díky	díky	k7c3	díky
biopalivům	biopaliv	k1gInPc3	biopaliv
dokonce	dokonce	k9	dokonce
zlevnil	zlevnit	k5eAaPmAgMnS	zlevnit
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
biopalivům	biopaliv	k1gInPc3	biopaliv
se	se	k3xPyFc4	se
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Kalouskem	Kalousek	k1gMnSc7	Kalousek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
ze	z	k7c2	z
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČR	ČR	kA	ČR
vydá	vydat	k5eAaPmIp3nS	vydat
o	o	k7c4	o
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
navíc	navíc	k6eAd1	navíc
na	na	k7c4	na
biopaliva	biopaliv	k1gMnSc4	biopaliv
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
příjemců	příjemce	k1gMnPc2	příjemce
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
Babišovy	Babišův	k2eAgFnPc1d1	Babišova
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
podporu	podpora	k1gFnSc4	podpora
schválila	schválit	k5eAaPmAgFnS	schválit
(	(	kIx(	(
<g/>
zamítnutím	zamítnutí	k1gNnSc7	zamítnutí
Kalouskova	Kalouskův	k2eAgInSc2d1	Kalouskův
pozměňovacího	pozměňovací	k2eAgInSc2d1	pozměňovací
návrhu	návrh	k1gInSc2	návrh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
SynBiol	SynBiol	k1gInSc4	SynBiol
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
akcionářem	akcionář	k1gMnSc7	akcionář
společnosti	společnost	k1gFnSc2	společnost
SynBiol	SynBiola	k1gFnPc2	SynBiola
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
s	s	k7c7	s
aktivy	aktiv	k1gInPc7	aktiv
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1,9	[number]	k4	1,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
tvořenými	tvořený	k2eAgFnPc7d1	tvořená
hotovostí	hotovost	k1gFnPc2	hotovost
na	na	k7c6	na
účtech	účet	k1gInPc6	účet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
již	již	k9	již
aktiva	aktivum	k1gNnSc2	aktivum
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
24	[number]	k4	24
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
společníkem	společník	k1gMnSc7	společník
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
87,75	[number]	k4	87,75
<g/>
%	%	kIx~	%
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Hartenberg	Hartenberg	k1gMnSc1	Hartenberg
Holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
vlastníkem	vlastník	k1gMnSc7	vlastník
půjčky	půjčka	k1gFnSc2	půjčka
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
22	[number]	k4	22
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
společnosti	společnost	k1gFnPc1	společnost
Hartenberg	Hartenberg	k1gMnSc1	Hartenberg
Holding	holding	k1gInSc1	holding
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
společníky	společník	k1gMnPc7	společník
v	v	k7c4	v
Hartenberg	Hartenberg	k1gInSc4	Hartenberg
Holding	holding	k1gInSc4	holding
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
<g/>
)	)	kIx)	)
Jozef	Jozef	k1gInSc1	Jozef
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
partner	partner	k1gMnSc1	partner
Penta	Pent	k1gInSc2	Pent
Investments	Investmentsa	k1gFnPc2	Investmentsa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Libor	Libor	k1gMnSc1	Libor
Němeček	Němeček	k1gMnSc1	Němeček
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
investic	investice	k1gFnPc2	investice
Agrofertu	Agrofert	k1gInSc2	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Hartenberg	Hartenberg	k1gInSc1	Hartenberg
Holding	holding	k1gInSc1	holding
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
investiční	investiční	k2eAgInSc1d1	investiční
fond	fond	k1gInSc1	fond
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
však	však	k9	však
investičním	investiční	k2eAgInSc7d1	investiční
fondem	fond	k1gInSc7	fond
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
investičních	investiční	k2eAgFnPc6d1	investiční
společnostech	společnost	k1gFnPc6	společnost
a	a	k8xC	a
fondech	fond	k1gInPc6	fond
<g/>
)	)	kIx)	)
a	a	k8xC	a
investuje	investovat	k5eAaBmIp3nS	investovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2014	[number]	k4	2014
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
společnosti	společnost	k1gFnSc2	společnost
Agrofert	Agrofert	k1gInSc1	Agrofert
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
odštěpením	odštěpení	k1gNnPc3	odštěpení
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
SynBiol	SynBiola	k1gFnPc2	SynBiola
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nástupnickou	nástupnický	k2eAgFnSc7d1	nástupnická
společností	společnost	k1gFnSc7	společnost
odštěpené	odštěpený	k2eAgFnSc2d1	odštěpená
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Odštěpeným	odštěpený	k2eAgInSc7d1	odštěpený
majetkem	majetek	k1gInSc7	majetek
jsou	být	k5eAaImIp3nP	být
převážné	převážný	k2eAgInPc1d1	převážný
podíly	podíl	k1gInPc1	podíl
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vlastní	vlastní	k2eAgFnPc4d1	vlastní
nemovitosti	nemovitost	k1gFnPc4	nemovitost
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Farma	farma	k1gFnSc1	farma
Čapí	čapět	k5eAaImIp3nS	čapět
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
či	či	k8xC	či
Anděl	Anděl	k1gMnSc1	Anděl
Media	medium	k1gNnSc2	medium
Centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
vlastník	vlastník	k1gMnSc1	vlastník
sídla	sídlo	k1gNnSc2	sídlo
skupiny	skupina	k1gFnSc2	skupina
MAFRA	MAFRA	kA	MAFRA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
SynBiol	SynBiol	k1gInSc1	SynBiol
měl	mít	k5eAaImAgInS	mít
konsolidovaná	konsolidovaný	k2eAgNnPc4d1	konsolidované
aktiva	aktivum	k1gNnPc4	aktivum
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
11,7	[number]	k4	11,7
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
a	a	k8xC	a
konsolidované	konsolidovaný	k2eAgFnSc2d1	konsolidovaná
tržby	tržba	k1gFnSc2	tržba
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
888	[number]	k4	888
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
Hlídací	hlídací	k2eAgMnSc1d1	hlídací
pes	pes	k1gMnSc1	pes
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
součástí	součást	k1gFnSc7	součást
koncernu	koncern	k1gInSc2	koncern
SynBiol	SynBiol	k1gInSc1	SynBiol
společnost	společnost	k1gFnSc1	společnost
IMOBA	IMOBA	kA	IMOBA
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dvanácti	dvanáct	k4xCc7	dvanáct
dceřinými	dceřin	k2eAgInPc7d1	dceřin
či	či	k8xC	či
vnukovskými	vnukovský	k2eAgFnPc7d1	vnukovská
společnostmi	společnost	k1gFnPc7	společnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
francouzská	francouzský	k2eAgFnSc1d1	francouzská
společnost	společnost	k1gFnSc1	společnost
Exorep	Exorep	k1gInSc1	Exorep
byla	být	k5eAaImAgFnS	být
provozovatelem	provozovatel	k1gMnSc7	provozovatel
luxusní	luxusní	k2eAgFnSc2d1	luxusní
restaurace	restaurace	k1gFnSc2	restaurace
Paloma	Palomum	k1gNnSc2	Palomum
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Mougins	Mouginsa	k1gFnPc2	Mouginsa
na	na	k7c6	na
Azurovém	azurový	k2eAgNnSc6d1	azurové
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Restaurace	restaurace	k1gFnSc1	restaurace
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
počáteční	počáteční	k2eAgFnSc1d1	počáteční
investice	investice	k1gFnSc1	investice
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
65	[number]	k4	65
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
interiér	interiér	k1gInSc4	interiér
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
Babišova	Babišův	k2eAgFnSc1d1	Babišova
žena	žena	k1gFnSc1	žena
Monika	Monika	k1gFnSc1	Monika
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
osobně	osobně	k6eAd1	osobně
vybral	vybrat	k5eAaPmAgInS	vybrat
šéfkuchaře	šéfkuchař	k1gMnSc4	šéfkuchař
Nicholase	Nicholasa	k1gFnSc6	Nicholasa
Decherchiho	Decherchi	k1gMnSc2	Decherchi
<g/>
.	.	kIx.	.
</s>
<s>
Restaurace	restaurace	k1gFnSc1	restaurace
získala	získat	k5eAaPmAgFnS	získat
půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
jednu	jeden	k4xCgFnSc4	jeden
michelinskou	michelinský	k2eAgFnSc4d1	michelinská
hvězdu	hvězda	k1gFnSc4	hvězda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Exorep	Exorep	k1gInSc1	Exorep
však	však	k9	však
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
vykázala	vykázat	k5eAaPmAgFnS	vykázat
ztrátu	ztráta	k1gFnSc4	ztráta
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Průhonicích	Průhonice	k1gFnPc6	Průhonice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
otevřena	otevřít	k5eAaPmNgFnS	otevřít
druhá	druhý	k4xOgFnSc1	druhý
luxusní	luxusní	k2eAgFnSc1d1	luxusní
restaurace	restaurace	k1gFnSc1	restaurace
toho	ten	k3xDgNnSc2	ten
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgMnSc1d1	spadající
rovněž	rovněž	k9	rovněž
vzdáleně	vzdáleně	k6eAd1	vzdáleně
pod	pod	k7c4	pod
Decherchiho	Decherchi	k1gMnSc4	Decherchi
vedení	vedení	k1gNnSc2	vedení
a	a	k8xC	a
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
Andrejem	Andrej	k1gMnSc7	Andrej
Babišem	Babiš	k1gMnSc7	Babiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střet	střet	k1gInSc4	střet
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
svěřenské	svěřenský	k2eAgInPc1d1	svěřenský
fondy	fond	k1gInPc1	fond
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
i	i	k9	i
přes	přes	k7c4	přes
prezidentské	prezidentský	k2eAgNnSc4d1	prezidentské
veto	veto	k1gNnSc4	veto
schválena	schválen	k2eAgFnSc1d1	schválena
změna	změna	k1gFnSc1	změna
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
zákonu	zákon	k1gInSc6	zákon
o	o	k7c6	o
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
(	(	kIx(	(
<g/>
159	[number]	k4	159
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
též	též	k9	též
Lex	Lex	k1gFnSc1	Lex
Babiš	Babiš	k1gInSc1	Babiš
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vládním	vládní	k2eAgMnPc3d1	vládní
činitelům	činitel	k1gMnPc3	činitel
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
přijímání	přijímání	k1gNnSc2	přijímání
státních	státní	k2eAgFnPc2d1	státní
dotací	dotace	k1gFnPc2	dotace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jejich	jejich	k3xOp3gFnPc2	jejich
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
Babiš	Babiš	k1gInSc1	Babiš
převedl	převést	k5eAaPmAgInS	převést
akcie	akcie	k1gFnSc2	akcie
společností	společnost	k1gFnPc2	společnost
Agrofert	Agrofert	k1gInSc1	Agrofert
a	a	k8xC	a
SynBiol	SynBiol	k1gInSc1	SynBiol
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
svěřenských	svěřenský	k2eAgInPc2d1	svěřenský
fondů	fond	k1gInPc2	fond
s	s	k7c7	s
názvy	název	k1gInPc7	název
AB	AB	kA	AB
private	privat	k1gInSc5	privat
trust	trust	k1gInSc4	trust
I	I	kA	I
a	a	k8xC	a
AB	AB	kA	AB
private	privat	k1gInSc5	privat
trust	trust	k1gInSc4	trust
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
svěřenské	svěřenský	k2eAgFnPc1d1	svěřenský
správce	správce	k1gMnSc2	správce
určil	určit	k5eAaPmAgMnS	určit
předsedu	předseda	k1gMnSc4	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Agrofertu	Agrofert	k1gInSc2	Agrofert
Zbyňka	Zbyněk	k1gMnSc4	Zbyněk
Průšu	Průša	k1gMnSc4	Průša
a	a	k8xC	a
právníka	právník	k1gMnSc4	právník
Alexeje	Alexej	k1gMnSc4	Alexej
Bílka	Bílek	k1gMnSc4	Bílek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
přinesly	přinést	k5eAaPmAgFnP	přinést
organizace	organizace	k1gFnPc1	organizace
Transparency	Transparenc	k2eAgFnPc1d1	Transparenc
International	International	k1gFnSc3	International
(	(	kIx(	(
<g/>
pobočka	pobočka	k1gFnSc1	pobočka
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
Neovlivní	ovlivnit	k5eNaPmIp3nS	ovlivnit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
verifikačního	verifikační	k2eAgInSc2d1	verifikační
dokumentu	dokument	k1gInSc2	dokument
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
RPVS	RPVS	kA	RPVS
(	(	kIx(	(
<g/>
315	[number]	k4	315
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
Z.	Z.	kA	Z.
z.	z.	k?	z.
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
konečným	konečný	k2eAgNnSc7d1	konečné
užívaťelom	užívaťelom	k1gInSc1	užívaťelom
výhod	výhoda	k1gFnPc2	výhoda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
evropské	evropský	k2eAgFnSc2d1	Evropská
směrnice	směrnice	k1gFnSc2	směrnice
proti	proti	k7c3	proti
praní	praní	k1gNnSc3	praní
špinavých	špinavý	k2eAgInPc2d1	špinavý
peněz	peníze	k1gInPc2	peníze
(	(	kIx(	(
<g/>
§	§	k?	§
13	[number]	k4	13
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
skutečným	skutečný	k2eAgMnSc7d1	skutečný
majitelem	majitel	k1gMnSc7	majitel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Agrofertu	Agrofert	k1gInSc6	Agrofert
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
dle	dle	k7c2	dle
českého	český	k2eAgInSc2d1	český
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obchodních	obchodní	k2eAgFnPc6d1	obchodní
společnostech	společnost	k1gFnPc6	společnost
a	a	k8xC	a
družstvech	družstvo	k1gNnPc6	družstvo
(	(	kIx(	(
<g/>
§	§	k?	§
74	[number]	k4	74
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ovládající	ovládající	k2eAgFnSc7d1	ovládající
osobou	osoba	k1gFnSc7	osoba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Agrofert	Agrofert	k1gInSc4	Agrofert
to	ten	k3xDgNnSc1	ten
popřela	popřít	k5eAaPmAgFnS	popřít
a	a	k8xC	a
reagovala	reagovat	k5eAaBmAgFnS	reagovat
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
konečný	konečný	k2eAgInSc1d1	konečný
užívaťeľ	užívaťeľ	k?	užívaťeľ
výhod	výhoda	k1gFnPc2	výhoda
<g/>
"	"	kIx"	"
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
ovládající	ovládající	k2eAgFnSc7d1	ovládající
osobou	osoba	k1gFnSc7	osoba
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
schválil	schválit	k5eAaPmAgInS	schválit
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
nová	nový	k2eAgNnPc4d1	nové
pravidla	pravidlo	k1gNnPc4	pravidlo
proti	proti	k7c3	proti
střetu	střet	k1gInSc3	střet
zájmů	zájem	k1gInPc2	zájem
při	při	k7c6	při
čerpání	čerpání	k1gNnSc6	čerpání
dotací	dotace	k1gFnPc2	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svěřenské	svěřenský	k2eAgInPc1d1	svěřenský
fondy	fond	k1gInPc1	fond
s	s	k7c7	s
odloženými	odložený	k2eAgFnPc7d1	odložená
firmami	firma	k1gFnPc7	firma
již	již	k9	již
neovládá	ovládat	k5eNaImIp3nS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
o	o	k7c4	o
nedovolený	dovolený	k2eNgInSc4d1	nedovolený
střet	střet	k1gInSc4	střet
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
firmy	firma	k1gFnSc2	firma
tímto	tento	k3xDgInSc7	tento
fondem	fond	k1gInSc7	fond
spravované	spravovaný	k2eAgFnSc2d1	spravovaná
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
přijít	přijít	k5eAaPmF	přijít
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
čerpat	čerpat	k5eAaImF	čerpat
evropské	evropský	k2eAgFnPc4d1	Evropská
dotace	dotace	k1gFnPc4	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
osoba	osoba	k1gFnSc1	osoba
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
orgánu	orgán	k1gInSc6	orgán
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
o	o	k7c6	o
dotacích	dotace	k1gFnPc6	dotace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
včetně	včetně	k7c2	včetně
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
takovémto	takovýto	k3xDgInSc6	takovýto
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
buď	buď	k8xC	buď
odstoupit	odstoupit	k5eAaPmF	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
prodat	prodat	k5eAaPmF	prodat
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
čerpající	čerpající	k2eAgFnSc2d1	čerpající
dotace	dotace	k1gFnSc2	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mluvčího	mluvčí	k1gMnSc2	mluvčí
Agrofertu	Agrofert	k1gInSc2	Agrofert
Karla	Karel	k1gMnSc2	Karel
Hanzelky	Hanzelka	k1gMnSc2	Hanzelka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
opatření	opatření	k1gNnSc1	opatření
koncernu	koncern	k1gInSc2	koncern
dotknout	dotknout	k5eAaPmF	dotknout
nemělo	mít	k5eNaImAgNnS	mít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Babiš	Babiš	k1gInSc1	Babiš
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
není	být	k5eNaImIp3nS	být
ovládající	ovládající	k2eAgFnSc1d1	ovládající
osobou	osoba	k1gFnSc7	osoba
ani	ani	k8xC	ani
skutečným	skutečný	k2eAgMnSc7d1	skutečný
majitelem	majitel	k1gMnSc7	majitel
svěřenských	svěřenský	k2eAgInPc2d1	svěřenský
fondů	fond	k1gInPc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analytika	analytik	k1gMnSc2	analytik
Pirátů	pirát	k1gMnPc2	pirát
Janusze	Janusze	k1gFnSc2	Janusze
Koniecznyho	Konieczny	k1gMnSc2	Konieczny
by	by	kYmCp3nP	by
legislativa	legislativa	k1gFnSc1	legislativa
mohla	moct	k5eAaImAgFnS	moct
Babiše	Babiše	k1gFnSc2	Babiše
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
coby	coby	k?	coby
osobu	osoba	k1gFnSc4	osoba
obmyšlenou	obmyšlený	k2eAgFnSc4d1	obmyšlená
<g/>
.	.	kIx.	.
<g/>
Transparency	Transparenc	k2eAgMnPc4d1	Transparenc
International	International	k1gMnPc4	International
podala	podat	k5eAaPmAgFnS	podat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
oznámení	oznámení	k1gNnPc2	oznámení
o	o	k7c4	o
podezření	podezření	k1gNnSc4	podezření
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
přestupku	přestupek	k1gInSc2	přestupek
k	k	k7c3	k
Městskému	městský	k2eAgInSc3d1	městský
úřadu	úřad	k1gInSc3	úřad
v	v	k7c6	v
Černošicích	Černošik	k1gInPc6	Černošik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
porušuje	porušovat	k5eAaImIp3nS	porušovat
zákon	zákon	k1gInSc4	zákon
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadále	nadále	k6eAd1	nadále
ovládá	ovládat	k5eAaImIp3nS	ovládat
Agrofert	Agrofert	k1gInSc1	Agrofert
a	a	k8xC	a
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
zákon	zákon	k1gInSc1	zákon
jako	jako	k8xS	jako
členovi	člen	k1gMnSc6	člen
vlády	vláda	k1gFnSc2	vláda
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
zahájil	zahájit	k5eAaPmAgInS	zahájit
správní	správní	k2eAgNnSc4d1	správní
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
měl	mít	k5eAaImAgMnS	mít
vydat	vydat	k5eAaPmF	vydat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dosud	dosud	k6eAd1	dosud
nepravomocně	pravomocně	k6eNd1	pravomocně
došlo	dojít	k5eAaPmAgNnS	dojít
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
Právo	právo	k1gNnSc1	právo
úřad	úřad	k1gInSc1	úřad
za	za	k7c4	za
přestupek	přestupek	k1gInSc4	přestupek
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
udělil	udělit	k5eAaPmAgInS	udělit
pokutu	pokuta	k1gFnSc4	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
200	[number]	k4	200
000	[number]	k4	000
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zpolitizované	zpolitizovaný	k2eAgNnSc1d1	zpolitizované
<g/>
,	,	kIx,	,
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
určitě	určitě	k6eAd1	určitě
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
odvolá	odvolat	k5eAaPmIp3nS	odvolat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
ke	k	k7c3	k
správnímu	správní	k2eAgInSc3d1	správní
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
skutečně	skutečně	k6eAd1	skutečně
jeho	jeho	k3xOp3gInPc4	jeho
právníci	právník	k1gMnPc1	právník
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
podali	podat	k5eAaPmAgMnP	podat
odvolání	odvolání	k1gNnSc3	odvolání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Krajském	krajský	k2eAgInSc6d1	krajský
úřadu	úřad	k1gInSc6	úřad
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hejtmankou	hejtmanka	k1gFnSc7	hejtmanka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Pokorná	Pokorná	k1gFnSc1	Pokorná
Jermanová	Jermanová	k1gFnSc1	Jermanová
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
důsledku	důsledek	k1gInSc6	důsledek
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
odbor	odbor	k1gInSc1	odbor
správních	správní	k2eAgFnPc2d1	správní
agend	agenda	k1gFnPc2	agenda
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
odvoláním	odvolání	k1gNnSc7	odvolání
měl	mít	k5eAaImAgMnS	mít
zabývat	zabývat	k5eAaImF	zabývat
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
byl	být	k5eAaImAgInS	být
sloučen	sloučen	k2eAgInSc1d1	sloučen
s	s	k7c7	s
odborem	odbor	k1gInSc7	odbor
legislativně	legislativně	k6eAd1	legislativně
právním	právní	k2eAgInSc7d1	právní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
o	o	k7c6	o
reorganizaci	reorganizace	k1gFnSc6	reorganizace
informoval	informovat	k5eAaBmAgInS	informovat
týdeník	týdeník	k1gInSc1	týdeník
Respekt	respekt	k1gInSc1	respekt
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
pirátská	pirátský	k2eAgFnSc1d1	pirátská
strana	strana	k1gFnSc1	strana
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
riziko	riziko	k1gNnSc4	riziko
ohrožení	ohrožení	k1gNnSc4	ohrožení
důvěryhodnosti	důvěryhodnost	k1gFnSc2	důvěryhodnost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
Babišova	Babišův	k2eAgInSc2d1	Babišův
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Hejtmanka	hejtmanka	k1gFnSc1	hejtmanka
Jermanová	Jermanová	k1gFnSc1	Jermanová
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozhodování	rozhodování	k1gNnSc4	rozhodování
úředníků	úředník	k1gMnPc2	úředník
v	v	k7c6	v
Babišově	Babišův	k2eAgFnSc6d1	Babišova
kauze	kauza	k1gFnSc6	kauza
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
<g/>
Transparency	Transparency	k1gInPc1	Transparency
International	International	k1gFnSc1	International
rovněž	rovněž	k9	rovněž
připravila	připravit	k5eAaPmAgFnS	připravit
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
komisi	komise	k1gFnSc3	komise
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
čerpání	čerpání	k1gNnSc2	čerpání
evropských	evropský	k2eAgFnPc2d1	Evropská
dotací	dotace	k1gFnPc2	dotace
Agrofertem	Agrofert	k1gInSc7	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
Transparency	Transparenca	k1gFnSc2	Transparenca
International	International	k1gFnSc2	International
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
se	s	k7c7	s
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2018	[number]	k4	2018
zabýval	zabývat	k5eAaImAgInS	zabývat
Evropský	evropský	k2eAgInSc1d1	evropský
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
rozpočtovou	rozpočtový	k2eAgFnSc4d1	rozpočtová
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
doporučení	doporučení	k1gNnSc4	doporučení
začala	začít	k5eAaPmAgFnS	začít
podezření	podezření	k1gNnSc4	podezření
prošetřovat	prošetřovat	k5eAaImF	prošetřovat
i	i	k9	i
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
EK	EK	kA	EK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
po	po	k7c6	po
českých	český	k2eAgInPc6d1	český
úřadech	úřad	k1gInPc6	úřad
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přehled	přehled	k1gInSc4	přehled
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
součástí	součást	k1gFnSc7	součást
skupiny	skupina	k1gFnSc2	skupina
Agrofert	Agrofert	k1gInSc1	Agrofert
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přitom	přitom	k6eAd1	přitom
financovány	financován	k2eAgMnPc4d1	financován
z	z	k7c2	z
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
fondů	fond	k1gInPc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
Forum	forum	k1gNnSc1	forum
24	[number]	k4	24
mělo	mít	k5eAaImAgNnS	mít
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
průtahům	průtah	k1gInPc3	průtah
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
nespolupracovala	spolupracovat	k5eNaImAgFnS	spolupracovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
změnil	změnit	k5eAaPmAgInS	změnit
Podpůrný	podpůrný	k2eAgInSc1d1	podpůrný
a	a	k8xC	a
garanční	garanční	k2eAgInSc1d1	garanční
rolnický	rolnický	k2eAgInSc1d1	rolnický
a	a	k8xC	a
lesnický	lesnický	k2eAgInSc1d1	lesnický
fond	fond	k1gInSc1	fond
(	(	kIx(	(
<g/>
PGRLF	PGRLF	kA	PGRLF
<g/>
)	)	kIx)	)
podmínky	podmínka	k1gFnPc1	podmínka
udělování	udělování	k1gNnSc2	udělování
dotací	dotace	k1gFnPc2	dotace
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgInS	být
možný	možný	k2eAgInSc4d1	možný
dvojí	dvojí	k4xRgInSc4	dvojí
právní	právní	k2eAgInSc4d1	právní
výklad	výklad	k1gInSc4	výklad
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
do	do	k7c2	do
souladu	soulad	k1gInSc2	soulad
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
legislativou	legislativa	k1gFnSc7	legislativa
platnou	platný	k2eAgFnSc7d1	platná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Úprava	úprava	k1gFnSc1	úprava
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
malých	malý	k2eAgFnPc2d1	malá
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
součástí	součást	k1gFnSc7	součást
koncernu	koncern	k1gInSc2	koncern
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
by	by	kYmCp3nP	by
pak	pak	k6eAd1	pak
měly	mít	k5eAaImAgFnP	mít
přijít	přijít	k5eAaPmF	přijít
o	o	k7c4	o
dotaci	dotace	k1gFnSc4	dotace
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
až	až	k9	až
po	po	k7c6	po
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
přechodném	přechodný	k2eAgNnSc6d1	přechodné
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Úprava	úprava	k1gFnSc1	úprava
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
i	i	k9	i
firem	firma	k1gFnPc2	firma
patřících	patřící	k2eAgFnPc2d1	patřící
do	do	k7c2	do
holdingu	holding	k1gInSc2	holding
Agrofert	Agrofert	k1gInSc1	Agrofert
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgInSc1d1	další
podnět	podnět	k1gInSc1	podnět
Pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
komisi	komise	k1gFnSc3	komise
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
neoprávněně	oprávněně	k6eNd1	oprávněně
čerpanou	čerpaný	k2eAgFnSc4d1	čerpaná
dotaci	dotace	k1gFnSc4	dotace
společnosti	společnost	k1gFnSc2	společnost
SPV	SPV	kA	SPV
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
za	za	k7c4	za
17,5	[number]	k4	17,5
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Přílohou	příloha	k1gFnSc7	příloha
podnětu	podnět	k1gInSc2	podnět
byla	být	k5eAaImAgFnS	být
databáze	databáze	k1gFnSc1	databáze
státních	státní	k2eAgFnPc2d1	státní
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
získal	získat	k5eAaPmAgInS	získat
holding	holding	k1gInSc1	holding
Agrofert	Agrofert	k1gInSc1	Agrofert
po	po	k7c6	po
platnosti	platnost	k1gFnSc6	platnost
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
800	[number]	k4	800
zakázek	zakázka	k1gFnPc2	zakázka
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
přes	přes	k7c4	přes
8	[number]	k4	8
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
120	[number]	k4	120
zakázek	zakázka	k1gFnPc2	zakázka
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dohledat	dohledat	k5eAaPmF	dohledat
částku	částka	k1gFnSc4	částka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celková	celkový	k2eAgFnSc1d1	celková
částka	částka	k1gFnSc1	částka
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgInSc4d2	vyšší
<g/>
.	.	kIx.	.
<g/>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
projednal	projednat	k5eAaPmAgInS	projednat
případ	případ	k1gInSc4	případ
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Střet	střet	k1gInSc1	střet
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
evropského	evropský	k2eAgInSc2d1	evropský
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
:	:	kIx,	:
případ	případ	k1gInSc1	případ
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
přijal	přijmout	k5eAaPmAgInS	přijmout
i	i	k9	i
usnesení	usnesení	k1gNnSc4	usnesení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgNnSc4	který
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
434	[number]	k4	434
europoslanců	europoslanec	k1gMnPc2	europoslanec
z	z	k7c2	z
545	[number]	k4	545
přítomných	přítomný	k1gMnPc2	přítomný
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
EP	EP	kA	EP
Bar	bar	k1gInSc1	bar
Staes	Staes	k1gInSc1	Staes
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
prokázal	prokázat	k5eAaPmAgInS	prokázat
konflikt	konflikt	k1gInSc1	konflikt
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
po	po	k7c6	po
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zpětně	zpětně	k6eAd1	zpětně
vymáhat	vymáhat	k5eAaImF	vymáhat
dotace	dotace	k1gFnPc4	dotace
vyplacené	vyplacený	k2eAgFnPc4d1	vyplacená
Agrofertu	Agrofert	k1gInSc2	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
se	se	k3xPyFc4	se
netýkal	týkat	k5eNaImAgInS	týkat
jen	jen	k9	jen
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
dotací	dotace	k1gFnPc2	dotace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
státních	státní	k2eAgFnPc2d1	státní
zakázek	zakázka	k1gFnPc2	zakázka
Lesů	les	k1gInPc2	les
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
firmy	firma	k1gFnSc2	firma
Čepro	Čepro	k1gNnSc4	Čepro
nebo	nebo	k8xC	nebo
Správy	správa	k1gFnSc2	správa
státních	státní	k2eAgFnPc2d1	státní
hmotných	hmotný	k2eAgFnPc2d1	hmotná
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
možné	možný	k2eAgNnSc4d1	možné
porušení	porušení	k1gNnSc4	porušení
směrnice	směrnice	k1gFnSc2	směrnice
24	[number]	k4	24
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
o	o	k7c6	o
zadávání	zadávání	k1gNnSc6	zadávání
veřejných	veřejný	k2eAgFnPc2d1	veřejná
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
konflikt	konflikt	k1gInSc4	konflikt
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
by	by	kYmCp3nP	by
proti	proti	k7c3	proti
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
řízení	řízení	k1gNnSc2	řízení
u	u	k7c2	u
Evropského	evropský	k2eAgInSc2d1	evropský
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
infringement	infringement	k1gInSc1	infringement
<g/>
.	.	kIx.	.
</s>
<s>
EP	EP	kA	EP
krom	krom	k7c2	krom
jiného	jiný	k2eAgNnSc2d1	jiné
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
znepokojení	znepokojení	k1gNnSc4	znepokojení
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nedodržovala	dodržovat	k5eNaImAgFnS	dodržovat
čl	čl	kA	čl
<g/>
.	.	kIx.	.
61	[number]	k4	61
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
finančního	finanční	k2eAgNnSc2d1	finanční
nařízení	nařízení	k1gNnSc2	nařízení
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Evropskou	evropský	k2eAgFnSc4d1	Evropská
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
Radu	rada	k1gFnSc4	rada
i	i	k8xC	i
vnitrostátní	vnitrostátní	k2eAgMnPc4d1	vnitrostátní
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
napravili	napravit	k5eAaPmAgMnP	napravit
a	a	k8xC	a
zajistili	zajistit	k5eAaPmAgMnP	zajistit
vrácení	vrácení	k1gNnSc4	vrácení
nesprávně	správně	k6eNd1	správně
nebo	nebo	k8xC	nebo
protiprávně	protiprávně	k6eAd1	protiprávně
vyplacených	vyplacený	k2eAgInPc2d1	vyplacený
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
<g/>
Kvůli	kvůli	k7c3	kvůli
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
byly	být	k5eAaImAgFnP	být
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
pozastaveny	pozastaven	k2eAgFnPc4d1	pozastavena
platby	platba	k1gFnPc4	platba
veškerých	veškerý	k3xTgFnPc2	veškerý
dotací	dotace	k1gFnPc2	dotace
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
Agrofertem	Agrofert	k1gInSc7	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
následně	následně	k6eAd1	následně
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádosti	žádost	k1gFnPc1	žádost
o	o	k7c4	o
příslušné	příslušný	k2eAgFnPc4d1	příslušná
platby	platba	k1gFnPc4	platba
ani	ani	k8xC	ani
nebude	být	k5eNaImBp3nS	být
Evropské	evropský	k2eAgNnSc1d1	Evropské
komisi	komise	k1gFnSc4	komise
k	k	k7c3	k
proplacení	proplacení	k1gNnSc3	proplacení
posílat	posílat	k5eAaImF	posílat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
koncem	konec	k1gInSc7	konec
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
nepožadované	požadovaný	k2eNgFnPc1d1	požadovaný
a	a	k8xC	a
neproplacené	proplacený	k2eNgFnPc1d1	neproplacená
platby	platba	k1gFnPc1	platba
přesáhly	přesáhnout	k5eAaPmAgFnP	přesáhnout
částku	částka	k1gFnSc4	částka
161,2	[number]	k4	161,2
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
o	o	k7c4	o
dotaci	dotace	k1gFnSc4	dotace
na	na	k7c4	na
výrobní	výrobní	k2eAgFnSc4d1	výrobní
linku	linka	k1gFnSc4	linka
na	na	k7c4	na
toastový	toastový	k2eAgInSc4d1	toastový
chleba	chléb	k1gInSc2	chléb
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Penam	Penam	k1gInSc4	Penam
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
kolem	kolem	k7c2	kolem
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
dotace	dotace	k1gFnPc4	dotace
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
11	[number]	k4	11
projektů	projekt	k1gInPc2	projekt
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
výši	výše	k1gFnSc6	výše
asi	asi	k9	asi
61	[number]	k4	61
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
organických	organický	k2eAgFnPc2d1	organická
syntéz	syntéza	k1gFnPc2	syntéza
<g/>
,	,	kIx,	,
Cerea	Cerea	k1gFnSc1	Cerea
<g/>
,	,	kIx,	,
Kostelecké	Kostelecké	k2eAgFnPc1d1	Kostelecké
uzeniny	uzenina	k1gFnPc1	uzenina
<g/>
,	,	kIx,	,
Navos	Navos	k1gInSc1	Navos
a	a	k8xC	a
Farmtec	Farmtec	k1gInSc1	Farmtec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odložení	odložení	k1gNnSc3	odložení
plateb	platba	k1gFnPc2	platba
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
u	u	k7c2	u
několika	několik	k4yIc2	několik
dalších	další	k2eAgInPc2d1	další
projektů	projekt	k1gInPc2	projekt
Agrofertu	Agrofert	k1gInSc2	Agrofert
spravovaných	spravovaný	k2eAgInPc2d1	spravovaný
Státním	státní	k2eAgInSc7d1	státní
zemědělským	zemědělský	k2eAgInSc7d1	zemědělský
intervenčním	intervenční	k2eAgInSc7d1	intervenční
fondem	fond	k1gInSc7	fond
<g/>
.	.	kIx.	.
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
zahájila	zahájit	k5eAaPmAgFnS	zahájit
na	na	k7c6	na
českých	český	k2eAgNnPc6d1	české
ministerstvech	ministerstvo	k1gNnPc6	ministerstvo
audit	audit	k1gInSc4	audit
evropských	evropský	k2eAgFnPc2d1	Evropská
dotací	dotace	k1gFnPc2	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
českým	český	k2eAgMnPc3d1	český
úřadům	úřada	k1gMnPc3	úřada
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
verzi	verze	k1gFnSc4	verze
auditní	auditní	k2eAgFnSc2d1	auditní
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgMnS	být
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
ve	v	k7c6	v
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
kvůli	kvůli	k7c3	kvůli
přetrvávajícím	přetrvávající	k2eAgFnPc3d1	přetrvávající
vazbám	vazba	k1gFnPc3	vazba
na	na	k7c4	na
svoje	své	k1gNnSc4	své
bývalé	bývalý	k2eAgFnSc2d1	bývalá
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgMnSc4	ten
požadovala	požadovat	k5eAaImAgFnS	požadovat
vrácení	vrácení	k1gNnSc3	vrácení
všech	všecek	k3xTgFnPc2	všecek
prověřovaných	prověřovaný	k2eAgFnPc2d1	prověřovaná
evropských	evropský	k2eAgFnPc2d1	Evropská
dotací	dotace	k1gFnPc2	dotace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Agrofert	Agrofert	k1gInSc1	Agrofert
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
od	od	k7c2	od
platnosti	platnost	k1gFnSc2	platnost
české	český	k2eAgFnSc2d1	Česká
legislativní	legislativní	k2eAgFnSc2d1	legislativní
úpravy	úprava	k1gFnSc2	úprava
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
na	na	k7c4	na
zprávu	zpráva	k1gFnSc4	zpráva
reagoval	reagovat	k5eAaBmAgMnS	reagovat
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
projevem	projev	k1gInSc7	projev
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
porušil	porušit	k5eAaPmAgMnS	porušit
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
žádné	žádný	k3yNgFnPc4	žádný
dotace	dotace	k1gFnPc4	dotace
vracet	vracet	k5eAaImF	vracet
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
auditní	auditní	k2eAgFnSc2d1	auditní
zprávy	zpráva	k1gFnSc2	zpráva
byl	být	k5eAaImAgInS	být
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zemědělství	zemědělství	k1gNnSc6	zemědělství
doručen	doručit	k5eAaPmNgInS	doručit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstva	ministerstvo	k1gNnPc1	ministerstvo
mají	mít	k5eAaImIp3nP	mít
2	[number]	k4	2
měsíce	měsíc	k1gInSc2	měsíc
na	na	k7c4	na
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
založil	založit	k5eAaPmAgMnS	založit
občanskou	občanský	k2eAgFnSc4d1	občanská
iniciativu	iniciativa	k1gFnSc4	iniciativa
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Akce	akce	k1gFnSc1	akce
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
nevyloučil	vyloučit	k5eNaPmAgMnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
bylo	být	k5eAaImAgNnS	být
ANO	ano	k9	ano
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
též	též	k9	též
jen	jen	k9	jen
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
registrováno	registrovat	k5eAaBmNgNnS	registrovat
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
volbě	volba	k1gFnSc6	volba
získal	získat	k5eAaPmAgInS	získat
73	[number]	k4	73
ze	z	k7c2	z
76	[number]	k4	76
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
hnutím	hnutí	k1gNnSc7	hnutí
chce	chtít	k5eAaImIp3nS	chtít
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
a	a	k8xC	a
za	za	k7c2	za
nižší	nízký	k2eAgFnSc2d2	nižší
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
státu	stát	k1gInSc2	stát
na	na	k7c6	na
daních	daň	k1gFnPc6	daň
odvádí	odvádět	k5eAaImIp3nS	odvádět
23	[number]	k4	23
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
holding	holding	k1gInSc1	holding
zhruba	zhruba	k6eAd1	zhruba
800	[number]	k4	800
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
hnutí	hnutí	k1gNnSc1	hnutí
ANO	ano	k9	ano
od	od	k7c2	od
Babiše	Babiš	k1gInSc2	Babiš
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
vlastních	vlastní	k2eAgInPc2d1	vlastní
prostředků	prostředek	k1gInPc2	prostředek
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
okolo	okolo	k7c2	okolo
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
konaných	konaný	k2eAgInPc2d1	konaný
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
finančně	finančně	k6eAd1	finančně
podpořil	podpořit	k5eAaPmAgInS	podpořit
uskupení	uskupení	k1gNnSc4	uskupení
Východočeši	Východočech	k1gMnPc1	Východočech
<g/>
,	,	kIx,	,
Mimo	mimo	k7c4	mimo
Jiné	jiný	k2eAgInPc4d1	jiný
a	a	k8xC	a
zlínské	zlínský	k2eAgInPc4d1	zlínský
M.	M.	kA	M.
O.	O.	kA	O.
R.	R.	kA	R.
(	(	kIx(	(
<g/>
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
Morální	morální	k2eAgFnSc4d1	morální
Očistu	očista	k1gFnSc4	očista
Radnice	radnice	k1gFnSc1	radnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
jejich	jejich	k3xOp3gFnPc4	jejich
kandidátky	kandidátka	k1gFnPc4	kandidátka
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
sedmi	sedm	k4xCc2	sedm
členů	člen	k1gMnPc2	člen
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
Hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xC	jako
lídr	lídr	k1gMnSc1	lídr
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
značný	značný	k2eAgInSc1d1	značný
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
18,65	[number]	k4	18,65
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Babiš	Babiš	k1gInSc1	Babiš
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
byl	být	k5eAaImAgMnS	být
Babiš	Babiš	k1gMnSc1	Babiš
funkcionáři	funkcionář	k1gMnSc3	funkcionář
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Kalouskem	Kalousek	k1gMnSc7	Kalousek
a	a	k8xC	a
Markem	Marek	k1gMnSc7	Marek
Ženíškem	Ženíšek	k1gMnSc7	Ženíšek
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
komunistického	komunistický	k2eAgMnSc4d1	komunistický
udavače	udavač	k1gMnSc4	udavač
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
reagoval	reagovat	k5eAaBmAgInS	reagovat
podáním	podání	k1gNnSc7	podání
trestního	trestní	k2eAgNnSc2d1	trestní
oznámení	oznámení	k1gNnSc2	oznámení
pro	pro	k7c4	pro
pomluvu	pomluva	k1gFnSc4	pomluva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kandidátem	kandidát	k1gMnSc7	kandidát
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
na	na	k7c4	na
post	post	k1gInSc4	post
vicepremiéra	vicepremiér	k1gMnSc2	vicepremiér
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
funkcí	funkce	k1gFnPc2	funkce
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
doporučil	doporučit	k5eAaPmAgMnS	doporučit
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
podnikat	podnikat	k5eAaImF	podnikat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
zkusili	zkusit	k5eAaPmAgMnP	zkusit
zaměstnanecký	zaměstnanecký	k2eAgInSc4d1	zaměstnanecký
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
popřel	popřít	k5eAaPmAgMnS	popřít
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaImF	stát
předsedou	předseda	k1gMnSc7	předseda
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
III	III	kA	III
<g/>
.	.	kIx.	.
sněmu	sněm	k1gInSc2	sněm
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
obhájil	obhájit	k5eAaPmAgMnS	obhájit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
post	posta	k1gFnPc2	posta
předsedy	předseda	k1gMnSc2	předseda
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
delegátů	delegát	k1gMnPc2	delegát
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
volbě	volba	k1gFnSc6	volba
všech	všecek	k3xTgInPc2	všecek
186	[number]	k4	186
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2015	[number]	k4	2015
označil	označit	k5eAaPmAgMnS	označit
propojení	propojení	k1gNnSc4	propojení
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
byznysu	byznys	k1gInSc2	byznys
Andrejem	Andrej	k1gMnSc7	Andrej
Babišem	Babiš	k1gMnSc7	Babiš
za	za	k7c4	za
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Babišovo	Babišův	k2eAgNnSc1d1	Babišovo
ANO	ano	k9	ano
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
za	za	k7c4	za
"	"	kIx"	"
<g/>
hrůzostrašný	hrůzostrašný	k2eAgInSc4d1	hrůzostrašný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zavedení	zavedení	k1gNnSc4	zavedení
elektronické	elektronický	k2eAgFnSc2d1	elektronická
evidence	evidence	k1gFnSc2	evidence
tržeb	tržba	k1gFnPc2	tržba
(	(	kIx(	(
<g/>
EET	EET	kA	EET
<g/>
)	)	kIx)	)
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
<g/>
-li	i	k?	-li
zavedena	zaveden	k2eAgFnSc1d1	zavedena
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zvýšit	zvýšit	k5eAaPmF	zvýšit
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
na	na	k7c6	na
zavedení	zavedení	k1gNnSc6	zavedení
EET	EET	kA	EET
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
kritiky	kritik	k1gMnPc4	kritik
od	od	k7c2	od
opozičních	opoziční	k2eAgMnPc2d1	opoziční
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odejde	odejít	k5eAaPmIp3nS	odejít
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
hnutí	hnutí	k1gNnSc1	hnutí
ANO	ano	k9	ano
po	po	k7c6	po
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2016	[number]	k4	2016
získalo	získat	k5eAaPmAgNnS	získat
ANO	ano	k9	ano
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgNnPc4	tři
senátorská	senátorský	k2eAgNnPc4d1	senátorské
křesla	křeslo	k1gNnPc4	křeslo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
poté	poté	k6eAd1	poté
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Senát	senát	k1gInSc1	senát
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	fact	k2eAgNnSc4d1	facto
zbytečný	zbytečný	k2eAgInSc4d1	zbytečný
a	a	k8xC	a
nebránil	bránit	k5eNaImAgMnS	bránit
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gNnSc4	jeho
zrušení	zrušení	k1gNnSc4	zrušení
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
agentury	agentura	k1gFnSc2	agentura
STEM	sto	k4xCgNnSc7	sto
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
lednu	leden	k1gInSc3	leden
2017	[number]	k4	2017
nejpopulárnějším	populární	k2eAgNnSc7d3	nejpopulárnější
politikem	politikum	k1gNnSc7	politikum
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
počtvrté	počtvrté	k4xO	počtvrté
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
IV	IV	kA	IV
<g/>
.	.	kIx.	.
celostátním	celostátní	k2eAgInSc6d1	celostátní
sněmu	sněm	k1gInSc6	sněm
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
získal	získat	k5eAaPmAgInS	získat
195	[number]	k4	195
hlasů	hlas	k1gInPc2	hlas
od	od	k7c2	od
206	[number]	k4	206
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odevzdali	odevzdat	k5eAaPmAgMnP	odevzdat
volební	volební	k2eAgInSc4d1	volební
lístek	lístek	k1gInSc4	lístek
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
však	však	k9	však
premiér	premiér	k1gMnSc1	premiér
Sobotka	Sobotka	k1gMnSc1	Sobotka
podal	podat	k5eAaPmAgMnS	podat
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
Babišovo	Babišův	k2eAgNnSc4d1	Babišovo
odvolání	odvolání	k1gNnSc4	odvolání
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důvod	důvod	k1gInSc1	důvod
udal	udat	k5eAaPmAgInS	udat
skandály	skandál	k1gInPc4	skandál
neodvádění	neodvádění	k1gNnPc1	neodvádění
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
úkolování	úkolování	k1gNnSc1	úkolování
novinářů	novinář	k1gMnPc2	novinář
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgInS	učinit
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
místopředsedy	místopředseda	k1gMnSc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
Babiše	Babiš	k1gMnSc2	Babiš
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Richard	Richard	k1gMnSc1	Richard
Brabec	Brabec	k1gMnSc1	Brabec
a	a	k8xC	a
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnSc7	finance
pak	pak	k6eAd1	pak
nahradil	nahradit	k5eAaPmAgMnS	nahradit
další	další	k2eAgMnSc1d1	další
spolustraník	spolustraník	k1gMnSc1	spolustraník
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
sněmovního	sněmovní	k2eAgInSc2d1	sněmovní
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
výboru	výbor	k1gInSc2	výbor
Ivan	Ivan	k1gMnSc1	Ivan
Pilný	pilný	k2eAgInSc1d1	pilný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgInPc1d1	politický
postoje	postoj	k1gInPc1	postoj
===	===	k?	===
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
odmítá	odmítat	k5eAaImIp3nS	odmítat
progresivní	progresivní	k2eAgNnSc4d1	progresivní
zdanění	zdanění	k1gNnSc4	zdanění
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
by	by	kYmCp3nS	by
snížit	snížit	k5eAaPmF	snížit
daně	daň	k1gFnPc4	daň
lidem	lid	k1gInSc7	lid
s	s	k7c7	s
příjmy	příjem	k1gInPc7	příjem
do	do	k7c2	do
113	[number]	k4	113
tisíc	tisíc	k4xCgInPc2	tisíc
a	a	k8xC	a
snížit	snížit	k5eAaPmF	snížit
sociální	sociální	k2eAgNnSc4d1	sociální
pojištění	pojištění	k1gNnSc4	pojištění
pro	pro	k7c4	pro
živnostníky	živnostník	k1gMnPc4	živnostník
a	a	k8xC	a
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
také	také	k9	také
odmítá	odmítat	k5eAaImIp3nS	odmítat
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
z	z	k7c2	z
čeho	co	k3yInSc2	co
vyplácet	vyplácet	k5eAaImF	vyplácet
důchody	důchod	k1gInPc4	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
jsou	být	k5eAaImIp3nP	být
penze	penze	k1gFnPc4	penze
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ČR	ČR	kA	ČR
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
si	se	k3xPyFc3	se
proto	proto	k6eAd1	proto
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
navýšit	navýšit	k5eAaPmF	navýšit
důchody	důchod	k1gInPc4	důchod
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
průměrné	průměrný	k2eAgFnSc2d1	průměrná
mzdy	mzda	k1gFnSc2	mzda
<g/>
.	.	kIx.	.
<g/>
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
podle	podle	k7c2	podle
Babiše	Babiš	k1gInSc2	Babiš
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
proti	proti	k7c3	proti
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
její	její	k3xOp3gFnSc3	její
další	další	k2eAgFnSc3d1	další
integraci	integrace	k1gFnSc3	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
se	se	k3xPyFc4	se
také	také	k9	také
staví	stavit	k5eAaImIp3nS	stavit
proti	proti	k7c3	proti
zavedení	zavedení	k1gNnSc3	zavedení
eura	euro	k1gNnSc2	euro
jako	jako	k8xC	jako
zákonné	zákonný	k2eAgFnSc2d1	zákonná
měny	měna	k1gFnSc2	měna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
<g/>
Babiš	Babiš	k1gMnSc1	Babiš
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
uprchlické	uprchlický	k2eAgFnPc4d1	uprchlická
kvóty	kvóta	k1gFnPc4	kvóta
na	na	k7c6	na
rozmístění	rozmístění	k1gNnSc6	rozmístění
žadatelů	žadatel	k1gMnPc2	žadatel
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
evropské	evropský	k2eAgFnSc2d1	Evropská
migrační	migrační	k2eAgFnSc2d1	migrační
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
migrační	migrační	k2eAgFnSc4d1	migrační
vlnu	vlna	k1gFnSc4	vlna
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
největší	veliký	k2eAgInSc4d3	veliký
<g />
.	.	kIx.	.
</s>
<s>
ohrožení	ohrožení	k1gNnSc1	ohrožení
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
Babiše	Babiše	k1gFnSc2	Babiše
by	by	kYmCp3nP	by
tábory	tábor	k1gInPc1	tábor
pro	pro	k7c4	pro
migranty	migrant	k1gMnPc4	migrant
"	"	kIx"	"
<g/>
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
založeny	založit	k5eAaPmNgInP	založit
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
přijít	přijít	k5eAaPmF	přijít
žádný	žádný	k3yNgMnSc1	žádný
uprchlík	uprchlík	k1gMnSc1	uprchlík
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prošel	projít	k5eAaPmAgMnS	projít
táborem	tábor	k1gInSc7	tábor
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Babiš	Babiš	k1gMnSc1	Babiš
přičetl	přičíst	k5eAaPmAgMnS	přičíst
vinu	vina	k1gFnSc4	vina
za	za	k7c7	za
útok	útok	k1gInSc1	útok
kamionem	kamion	k1gInSc7	kamion
na	na	k7c6	na
vánočních	vánoční	k2eAgInPc6d1	vánoční
trzích	trh	k1gInPc6	trh
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
německé	německý	k2eAgFnSc6d1	německá
kancléřce	kancléřka	k1gFnSc6	kancléřka
Angele	Angela	k1gFnSc3	Angela
Merkelové	Merkelová	k1gFnSc3	Merkelová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
Babiše	Babiše	k1gFnSc2	Babiše
svou	svůj	k3xOyFgFnSc7	svůj
politikou	politika	k1gFnSc7	politika
umožnila	umožnit	k5eAaPmAgFnS	umožnit
"	"	kIx"	"
<g/>
nekontrolovanou	kontrolovaný	k2eNgFnSc4d1	nekontrolovaná
migraci	migrace	k1gFnSc4	migrace
<g/>
"	"	kIx"	"
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
tento	tento	k3xDgInSc4	tento
Babišův	Babišův	k2eAgInSc4d1	Babišův
výrok	výrok	k1gInSc4	výrok
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
NATO	NATO	kA	NATO
Jensem	Jens	k1gMnSc7	Jens
Stoltenbergem	Stoltenberg	k1gMnSc7	Stoltenberg
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Babiš	Babiš	k1gMnSc1	Babiš
NATO	NATO	kA	NATO
za	za	k7c4	za
nezájem	nezájem	k1gInSc4	nezájem
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
evropských	evropský	k2eAgFnPc2d1	Evropská
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
NATO	NATO	kA	NATO
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
účelově	účelově	k6eAd1	účelově
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
somálským	somálský	k2eAgMnPc3d1	somálský
pirátům	pirát	k1gMnPc3	pirát
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
zájem	zájem	k1gInSc1	zájem
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
NATO	NATO	kA	NATO
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
rady	rada	k1gFnSc2	rada
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
...	...	k?	...
NATO	NATO	kA	NATO
uprchlíci	uprchlík	k1gMnPc1	uprchlík
nezajímají	zajímat	k5eNaImIp3nP	zajímat
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
jejich	jejich	k3xOp3gFnSc7	jejich
vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
člen	člen	k1gInSc1	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
a	a	k8xC	a
pašeráci	pašerák	k1gMnPc1	pašerák
na	na	k7c6	na
území	území	k1gNnSc6	území
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc4	člen
<g />
.	.	kIx.	.
</s>
<s>
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
operují	operovat	k5eAaImIp3nP	operovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
Babiš	Babiš	k1gMnSc1	Babiš
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
anexi	anexe	k1gFnSc4	anexe
Krymu	Krym	k1gInSc2	Krym
Ruskem	Rusko	k1gNnSc7	Rusko
k	k	k7c3	k
zabrání	zabrání	k1gNnSc3	zabrání
Sudet	Sudety	k1gFnPc2	Sudety
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
"	"	kIx"	"
<g/>
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
sankce	sankce	k1gFnPc1	sankce
vůči	vůči	k7c3	vůči
Rusku	Rusko	k1gNnSc3	Rusko
však	však	k9	však
už	už	k6eAd1	už
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nevhodné	vhodný	k2eNgNnSc4d1	nevhodné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
české	český	k2eAgMnPc4d1	český
podnikatele	podnikatel	k1gMnPc4	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Nevěří	věřit	k5eNaImIp3nP	věřit
také	také	k9	také
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Putin	putin	k2eAgMnSc1d1	putin
plánoval	plánovat	k5eAaImAgMnS	plánovat
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
<g/>
Babiš	Babiš	k1gMnSc1	Babiš
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
označil	označit	k5eAaPmAgMnS	označit
čínskou	čínský	k2eAgFnSc4d1	čínská
společnost	společnost	k1gFnSc4	společnost
CEFC	CEFC	kA	CEFC
China	China	k1gFnSc1	China
Energy	Energ	k1gInPc4	Energ
Company	Compana	k1gFnSc2	Compana
Limited	limited	k2eAgInPc4d1	limited
za	za	k7c2	za
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
a	a	k8xC	a
netransparentního	transparentní	k2eNgMnSc2d1	netransparentní
investora	investor	k1gMnSc2	investor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
Babiše	Babiš	k1gInSc2	Babiš
vazby	vazba	k1gFnSc2	vazba
na	na	k7c4	na
premiéra	premiér	k1gMnSc4	premiér
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Sobotku	Sobotka	k1gMnSc4	Sobotka
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
CEFC	CEFC	kA	CEFC
kupují	kupovat	k5eAaImIp3nP	kupovat
sportovní	sportovní	k2eAgInPc4d1	sportovní
kluby	klub	k1gInPc4	klub
<g/>
,	,	kIx,	,
nemovitosti	nemovitost	k1gFnPc4	nemovitost
nebo	nebo	k8xC	nebo
média	médium	k1gNnPc4	médium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
nám	my	k3xPp1nPc3	my
chtějí	chtít	k5eAaImIp3nP	chtít
Číňané	Číňan	k1gMnPc1	Číňan
opravdu	opravdu	k6eAd1	opravdu
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
by	by	kYmCp3nP	by
koupit	koupit	k5eAaPmF	koupit
OKD	OKD	kA	OKD
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Babiš	Babiš	k1gMnSc1	Babiš
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
čínské	čínský	k2eAgFnPc4d1	čínská
investice	investice	k1gFnPc4	investice
jiný	jiný	k2eAgInSc1d1	jiný
názor	názor	k1gInSc1	názor
než	než	k8xS	než
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
druhé	druhý	k4xOgFnSc2	druhý
Babišovy	Babišův	k2eAgFnSc2d1	Babišova
vlády	vláda	k1gFnSc2	vláda
označil	označit	k5eAaPmAgMnS	označit
KSČM	KSČM	kA	KSČM
za	za	k7c4	za
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
státotvorně	státotvorně	k6eAd1	státotvorně
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vznik	vznik	k1gInSc4	vznik
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
ostatně	ostatně	k6eAd1	ostatně
hnutí	hnutí	k1gNnPc2	hnutí
ANO	ano	k9	ano
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
ustavení	ustavení	k1gNnSc2	ustavení
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
v	v	k7c6	v
77	[number]	k4	77
%	%	kIx~	%
hlasování	hlasování	k1gNnSc6	hlasování
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgFnSc4d3	veliký
shodu	shoda	k1gFnSc4	shoda
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sněmovních	sněmovní	k2eAgFnPc6d1	sněmovní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přitom	přitom	k6eAd1	přitom
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
můžeme	moct	k5eAaImIp1nP	moct
zabránit	zabránit	k5eAaPmF	zabránit
nástupu	nástup	k1gInSc2	nástup
levice	levice	k1gFnSc2	levice
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
já	já	k3xPp1nSc1	já
jako	jako	k9	jako
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
nejsem	být	k5eNaImIp1nS	být
pyšný	pyšný	k2eAgMnSc1d1	pyšný
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomu	ten	k3xDgNnSc3	ten
můžu	můžu	k?	můžu
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
izraelským	izraelský	k2eAgMnSc7d1	izraelský
premiérem	premiér	k1gMnSc7	premiér
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Netanjahuem	Netanjahu	k1gMnSc7	Netanjahu
označil	označit	k5eAaPmAgMnS	označit
Izrael	Izrael	k1gMnSc1	Izrael
za	za	k7c4	za
strategického	strategický	k2eAgMnSc4d1	strategický
partnera	partner	k1gMnSc4	partner
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Babiše	Babiš	k1gInSc2	Babiš
je	být	k5eAaImIp3nS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
uznány	uznán	k2eAgFnPc4d1	uznána
hranice	hranice	k1gFnPc4	hranice
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
po	po	k7c6	po
šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
ale	ale	k9	ale
s	s	k7c7	s
přesunem	přesun	k1gInSc7	přesun
české	český	k2eAgFnSc2d1	Česká
ambasády	ambasáda	k1gFnSc2	ambasáda
z	z	k7c2	z
Tel	tel	kA	tel
Avivu	Aviva	k1gFnSc4	Aviva
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
eskalaci	eskalace	k1gFnSc3	eskalace
izraelsko-palestinského	izraelskoalestinský	k2eAgInSc2d1	izraelsko-palestinský
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
<g/>
Babiš	Babiš	k1gInSc1	Babiš
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
vystoupení	vystoupení	k1gNnSc3	vystoupení
Česka	Česko	k1gNnSc2	Česko
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
členy	člen	k1gMnPc4	člen
v	v	k7c6	v
EU	EU	kA	EU
a	a	k8xC	a
jsme	být	k5eAaImIp1nP	být
spojencem	spojenec	k1gMnSc7	spojenec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
czexitu	czexit	k1gInSc6	czexit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
naši	náš	k3xOp1gFnSc4	náš
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
Babiše	Babiše	k1gFnSc2	Babiše
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
"	"	kIx"	"
<g/>
vrátit	vrátit	k5eAaPmF	vrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
podstatě	podstata	k1gFnSc3	podstata
<g/>
,	,	kIx,	,
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
poslání	poslání	k1gNnSc3	poslání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
a	a	k8xC	a
prosperující	prosperující	k2eAgInSc4d1	prosperující
kontinent	kontinent	k1gInSc4	kontinent
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Kritizoval	kritizovat	k5eAaImAgInS	kritizovat
politiku	politika	k1gFnSc4	politika
tureckého	turecký	k2eAgMnSc2d1	turecký
prezidenta	prezident	k1gMnSc2	prezident
Erdoğ	Erdoğ	k1gMnSc2	Erdoğ
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgNnSc6	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
dnes	dnes	k6eAd1	dnes
Evropa	Evropa	k1gFnSc1	Evropa
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
Polsko	Polsko	k1gNnSc1	Polsko
či	či	k8xC	či
Orbána	Orbána	k1gFnSc1	Orbána
<g/>
,	,	kIx,	,
že	že	k8xS	že
dělají	dělat	k5eAaImIp3nP	dělat
něco	něco	k3yInSc4	něco
nedemokratického	demokratický	k2eNgNnSc2d1	nedemokratické
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nekritizují	kritizovat	k5eNaImIp3nP	kritizovat
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
?	?	kIx.	?
</s>
<s>
Vždyť	vždyť	k9	vždyť
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Při	při	k7c6	při
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
na	na	k7c4	na
vysídlence	vysídlenec	k1gMnPc4	vysídlenec
a	a	k8xC	a
běžence	běženec	k1gMnPc4	běženec
německá	německý	k2eAgFnSc1d1	německá
kancléřka	kancléřka	k1gFnSc1	kancléřka
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
vysídlení	vysídlení	k1gNnSc4	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
takový	takový	k3xDgInSc1	takový
výrok	výrok	k1gInSc1	výrok
je	být	k5eAaImIp3nS	být
absolutně	absolutně	k6eAd1	absolutně
nepřijatelný	přijatelný	k2eNgInSc1d1	nepřijatelný
a	a	k8xC	a
odmítám	odmítat	k5eAaImIp1nS	odmítat
ho	on	k3xPp3gNnSc4	on
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
připomínáme	připomínat	k5eAaImIp1nP	připomínat
hrůzy	hrůza	k1gFnPc4	hrůza
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
<g/>
,	,	kIx,	,
Lidice	Lidice	k1gInPc1	Lidice
<g/>
,	,	kIx,	,
Ležáky	Ležáky	k1gInPc1	Ležáky
a	a	k8xC	a
zabití	zabití	k1gNnSc1	zabití
našich	náš	k3xOp1gMnPc2	náš
parašutistů	parašutista	k1gMnPc2	parašutista
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
2017	[number]	k4	2017
===	===	k?	===
</s>
</p>
<p>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
rozdávali	rozdávat	k5eAaImAgMnP	rozdávat
stoupenci	stoupenec	k1gMnPc1	stoupenec
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
brožuru	brožura	k1gFnSc4	brožura
<g/>
,	,	kIx,	,
sepsanou	sepsaný	k2eAgFnSc4d1	sepsaná
Andrejem	Andrej	k1gMnSc7	Andrej
Babišem	Babiš	k1gInSc7	Babiš
(	(	kIx(	(
<g/>
či	či	k8xC	či
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
jménu	jméno	k1gNnSc6	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
O	o	k7c6	o
čem	co	k3yInSc6	co
sním	snít	k5eAaImIp1nS	snít
<g/>
,	,	kIx,	,
když	když	k8xS	když
náhodou	náhodou	k6eAd1	náhodou
spím	spát	k5eAaImIp1nS	spát
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
byla	být	k5eAaImAgFnS	být
popsána	popsán	k2eAgFnSc1d1	popsána
"	"	kIx"	"
<g/>
Vize	vize	k1gFnSc1	vize
2035	[number]	k4	2035
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
naše	náš	k3xOp1gFnPc4	náš
děti	dítě	k1gFnPc4	dítě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ještě	ještě	k9	ještě
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
Babiš	Babiš	k1gMnSc1	Babiš
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
redaktorem	redaktor	k1gMnSc7	redaktor
deníku	deník	k1gInSc2	deník
Právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
volebního	volební	k2eAgNnSc2d1	volební
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
svého	svůj	k3xOyFgNnSc2	svůj
premiérského	premiérský	k2eAgNnSc2d1	premiérské
angažmá	angažmá	k1gNnSc2	angažmá
"	"	kIx"	"
<g/>
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
již	již	k6eAd1	již
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
to	ten	k3xDgNnSc4	ten
svým	svůj	k3xOyFgInSc7	svůj
věkem	věk	k1gInSc7	věk
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
chvíli	chvíle	k1gFnSc4	chvíle
normální	normální	k2eAgInSc4d1	normální
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
věnovat	věnovat	k5eAaImF	věnovat
se	se	k3xPyFc4	se
rodině	rodina	k1gFnSc3	rodina
i	i	k8xC	i
firmě	firma	k1gFnSc3	firma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
ve	v	k7c6	v
dnech	den	k1gInPc6	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
hnutí	hnutí	k1gNnSc1	hnutí
ANO	ano	k9	ano
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
78	[number]	k4	78
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
jako	jako	k8xC	jako
lídr	lídr	k1gMnSc1	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dostal	dostat	k5eAaPmAgInS	dostat
48	[number]	k4	48
645	[number]	k4	645
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
zvolených	zvolený	k2eAgMnPc2d1	zvolený
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Premiér	premiér	k1gMnSc1	premiér
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
pověřil	pověřit	k5eAaPmAgMnS	pověřit
lídra	lídr	k1gMnSc4	lídr
ANO	ano	k9	ano
Andreje	Andrej	k1gMnPc4	Andrej
Babiše	Babiše	k1gFnSc2	Babiše
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
jednáním	jednání	k1gNnSc7	jednání
o	o	k7c6	o
sestavení	sestavení	k1gNnSc6	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
jej	on	k3xPp3gInSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
obou	dva	k4xCgMnPc2	dva
aktérů	aktér	k1gMnPc2	aktér
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
vláda	vláda	k1gFnSc1	vláda
jmenována	jmenován	k2eAgFnSc1d1	jmenována
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
po	po	k7c6	po
seznámení	seznámení	k1gNnSc6	seznámení
prezidenta	prezident	k1gMnSc2	prezident
s	s	k7c7	s
ministerskými	ministerský	k2eAgMnPc7d1	ministerský
adepty	adept	k1gMnPc7	adept
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Babiše	Babiš	k1gInSc2	Babiš
prezident	prezident	k1gMnSc1	prezident
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
posunem	posun	k1gInSc7	posun
termínu	termín	k1gInSc2	termín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
už	už	k9	už
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
jmenované	jmenovaný	k2eAgFnSc2d1	jmenovaná
vlády	vláda	k1gFnSc2	vláda
mohl	moct	k5eAaImAgInS	moct
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
summitu	summit	k1gInSc3	summit
prezidentů	prezident	k1gMnPc2	prezident
a	a	k8xC	a
premiérů	premiér	k1gMnPc2	premiér
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
chtěl	chtít	k5eAaImAgMnS	chtít
rovněž	rovněž	k9	rovněž
osobně	osobně	k6eAd1	osobně
podpořit	podpořit	k5eAaPmF	podpořit
získání	získání	k1gNnSc4	získání
sněmovní	sněmovní	k2eAgFnSc2d1	sněmovní
důvěry	důvěra	k1gFnSc2	důvěra
pro	pro	k7c4	pro
Babišovu	Babišův	k2eAgFnSc4d1	Babišova
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
požádala	požádat	k5eAaPmAgFnS	požádat
vláda	vláda	k1gFnSc1	vláda
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
ANO	ano	k9	ano
a	a	k8xC	a
nestraníků	nestraník	k1gMnPc2	nestraník
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
o	o	k7c4	o
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
,	,	kIx,	,
sněmovní	sněmovní	k2eAgFnSc1d1	sněmovní
schůze	schůze	k1gFnSc1	schůze
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
večer	večer	k6eAd1	večer
přerušena	přerušit	k5eAaPmNgFnS	přerušit
a	a	k8xC	a
hlasování	hlasování	k1gNnSc1	hlasování
odloženo	odložit	k5eAaPmNgNnS	odložit
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
vláda	vláda	k1gFnSc1	vláda
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gInSc2	Babiš
důvěru	důvěra	k1gFnSc4	důvěra
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Podpořilo	podpořit	k5eAaPmAgNnS	podpořit
ji	on	k3xPp3gFnSc4	on
všech	všecek	k3xTgMnPc2	všecek
78	[number]	k4	78
poslanců	poslanec	k1gMnPc2	poslanec
hnutí	hnutí	k1gNnPc1	hnutí
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgFnPc4d1	zbývající
strany	strana	k1gFnPc4	strana
ale	ale	k8xC	ale
hlasovaly	hlasovat	k5eAaImAgFnP	hlasovat
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
odhlasovala	odhlasovat	k5eAaPmAgFnS	odhlasovat
vláda	vláda	k1gFnSc1	vláda
svoji	svůj	k3xOyFgFnSc4	svůj
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
přijal	přijmout	k5eAaPmAgMnS	přijmout
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Babiše	Babiš	k1gInSc2	Babiš
jednáním	jednání	k1gNnSc7	jednání
o	o	k7c4	o
sestavení	sestavení	k1gNnSc4	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
sjezdu	sjezd	k1gInSc6	sjezd
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
svého	svůj	k3xOyFgMnSc2	svůj
nového	nový	k2eAgMnSc2d1	nový
předsedy	předseda	k1gMnSc2	předseda
Jana	Jan	k1gMnSc2	Jan
Hamáčka	Hamáček	k1gMnSc2	Hamáček
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
ANO	ano	k9	ano
o	o	k7c6	o
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ČSSD	ČSSD	kA	ČSSD
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
požadovala	požadovat	k5eAaImAgNnP	požadovat
pět	pět	k4xCc4	pět
ministerských	ministerský	k2eAgNnPc2d1	ministerské
křesel	křeslo	k1gNnPc2	křeslo
včetně	včetně	k7c2	včetně
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
(	(	kIx(	(
<g/>
resorty	resort	k1gInPc1	resort
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
váhu	váha	k1gFnSc4	váha
ve	v	k7c6	v
spojitosti	spojitost	k1gFnSc6	spojitost
s	s	k7c7	s
kauzou	kauza	k1gFnSc7	kauza
Čapí	čapí	k2eAgNnSc4d1	čapí
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ANO	ano	k9	ano
tehdy	tehdy	k6eAd1	tehdy
ochotno	ochoten	k2eAgNnSc1d1	ochotno
přiznat	přiznat	k5eAaPmF	přiznat
ČSSD	ČSSD	kA	ČSSD
jen	jen	k6eAd1	jen
čtyři	čtyři	k4xCgNnPc1	čtyři
ministerstva	ministerstvo	k1gNnPc1	ministerstvo
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
demisi	demise	k1gFnSc6	demise
pod	pod	k7c7	pod
Babišovým	Babišův	k2eAgNnSc7d1	Babišovo
vedením	vedení	k1gNnSc7	vedení
o	o	k7c4	o
vyhoštění	vyhoštění	k1gNnSc4	vyhoštění
tří	tři	k4xCgMnPc2	tři
ruských	ruský	k2eAgMnPc2d1	ruský
agentů	agent	k1gMnPc2	agent
s	s	k7c7	s
diplomatickým	diplomatický	k2eAgInSc7d1	diplomatický
statusem	status	k1gInSc7	status
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
Sergeji	Sergej	k1gMnSc3	Sergej
Skripalovi	Skripal	k1gMnSc3	Skripal
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
dceři	dcera	k1gFnSc3	dcera
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgMnS	odehrát
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Martina	Martin	k1gMnSc2	Martin
Fendrycha	Fendrych	k1gMnSc2	Fendrych
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
jednoznačně	jednoznačně	k6eAd1	jednoznačně
pro	pro	k7c4	pro
prozápadní	prozápadní	k2eAgInSc4d1	prozápadní
a	a	k8xC	a
protiruský	protiruský	k2eAgInSc4d1	protiruský
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
<g/>
Začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
skončilo	skončit	k5eAaPmAgNnS	skončit
první	první	k4xOgNnSc1	první
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
mezi	mezi	k7c7	mezi
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
ANO	ano	k9	ano
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
vládě	vláda	k1gFnSc6	vláda
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
neochota	neochota	k1gFnSc1	neochota
ANO	ano	k9	ano
přenechat	přenechat	k5eAaPmF	přenechat
sociálním	sociální	k2eAgMnPc3d1	sociální
demokratům	demokrat	k1gMnPc3	demokrat
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
ČSSD	ČSSD	kA	ČSSD
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Zimola	Zimola	k1gFnSc1	Zimola
navštívili	navštívit	k5eAaPmAgMnP	navštívit
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
prezidenta	prezident	k1gMnSc4	prezident
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
jim	on	k3xPp3gMnPc3	on
prezident	prezident	k1gMnSc1	prezident
doporučil	doporučit	k5eAaPmAgMnS	doporučit
trvat	trvat	k5eAaImF	trvat
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
požadavcích	požadavek	k1gInPc6	požadavek
a	a	k8xC	a
neustupovat	ustupovat	k5eNaImF	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
v	v	k7c6	v
demisi	demise	k1gFnSc6	demise
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
připraven	připravit	k5eAaPmNgMnS	připravit
podřídit	podřídit	k5eAaPmF	podřídit
se	se	k3xPyFc4	se
Zemanově	Zemanův	k2eAgFnSc3d1	Zemanova
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Když	když	k8xS	když
prezident	prezident	k1gMnSc1	prezident
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pověří	pověřit	k5eAaPmIp3nS	pověřit
vyjednáváním	vyjednávání	k1gNnSc7	vyjednávání
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgNnSc2d1	jiné
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
to	ten	k3xDgNnSc1	ten
budu	být	k5eAaImBp1nS	být
akceptovat	akceptovat	k5eAaBmF	akceptovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
podruhé	podruhé	k6eAd1	podruhé
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
předložil	předložit	k5eAaPmAgInS	předložit
seznam	seznam	k1gInSc1	seznam
příštích	příští	k2eAgInPc2d1	příští
členů	člen	k1gInPc2	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
seznámit	seznámit	k5eAaPmF	seznámit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
Babiš	Babiš	k1gInSc4	Babiš
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jej	on	k3xPp3gMnSc4	on
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
podruhé	podruhé	k6eAd1	podruhé
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
menšinovou	menšinový	k2eAgFnSc4d1	menšinová
vládu	vláda	k1gFnSc4	vláda
sestavil	sestavit	k5eAaPmAgInS	sestavit
jak	jak	k6eAd1	jak
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
složili	složit	k5eAaPmAgMnP	složit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
předepsaný	předepsaný	k2eAgInSc4d1	předepsaný
slib	slib	k1gInSc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
nejmenoval	jmenovat	k5eNaImAgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
zástupce	zástupce	k1gMnSc1	zástupce
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
poslance	poslanec	k1gMnSc2	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
Miroslava	Miroslav	k1gMnSc2	Miroslav
Pocheho	Poche	k1gMnSc2	Poche
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vedení	vedení	k1gNnSc1	vedení
tohoto	tento	k3xDgNnSc2	tento
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
ujal	ujmout	k5eAaPmAgMnS	ujmout
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
obdržela	obdržet	k5eAaPmAgFnS	obdržet
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
celkově	celkově	k6eAd1	celkově
pět	pět	k4xCc1	pět
křesel	křeslo	k1gNnPc2	křeslo
včetně	včetně	k7c2	včetně
jí	on	k3xPp3gFnSc2	on
zprvu	zprvu	k6eAd1	zprvu
odepíraného	odepíraný	k2eAgNnSc2d1	odepíraný
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
ANO	ano	k9	ano
dostalo	dostat	k5eAaPmAgNnS	dostat
deset	deset	k4xCc4	deset
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
podepsaly	podepsat	k5eAaPmAgInP	podepsat
oba	dva	k4xCgInPc1	dva
subjekty	subjekt	k1gInPc1	subjekt
koaliční	koaliční	k2eAgFnSc4d1	koaliční
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
jmenovanou	jmenovaná	k1gFnSc7	jmenovaná
ministryní	ministryně	k1gFnPc2	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
za	za	k7c4	za
ANO	ano	k9	ano
byla	být	k5eAaImAgFnS	být
Taťána	Taťána	k1gFnSc1	Taťána
Malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
po	po	k7c6	po
veřejné	veřejný	k2eAgFnSc6d1	veřejná
kritice	kritika	k1gFnSc6	kritika
kvůli	kvůli	k7c3	kvůli
plagiátorství	plagiátorství	k1gNnSc3	plagiátorství
diplomových	diplomový	k2eAgFnPc2d1	Diplomová
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
však	však	k9	však
13	[number]	k4	13
dní	den	k1gInPc2	den
nato	nato	k6eAd1	nato
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
krátce	krátce	k6eAd1	krátce
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
prezidentem	prezident	k1gMnSc7	prezident
pověřit	pověřit	k5eAaPmF	pověřit
k	k	k7c3	k
vedení	vedení	k1gNnSc3	vedení
tohoto	tento	k3xDgInSc2	tento
resortu	resort	k1gInSc2	resort
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
našel	najít	k5eAaPmAgMnS	najít
nový	nový	k2eAgMnSc1d1	nový
ministr	ministr	k1gMnSc1	ministr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
opozičních	opoziční	k2eAgFnPc2d1	opoziční
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
premiér	premiér	k1gMnSc1	premiér
narychlo	narychlo	k6eAd1	narychlo
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
nového	nový	k2eAgMnSc4d1	nový
adepta	adept	k1gMnSc4	adept
na	na	k7c4	na
ministra	ministr	k1gMnSc4	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Kněžínka	Kněžínek	k1gMnSc2	Kněžínek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
prezident	prezident	k1gMnSc1	prezident
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
získala	získat	k5eAaPmAgFnS	získat
Babišova	Babišův	k2eAgFnSc1d1	Babišova
vláda	vláda	k1gFnSc1	vláda
důvěru	důvěra	k1gFnSc4	důvěra
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
105	[number]	k4	105
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
<g/>
)	)	kIx)	)
ku	k	k7c3	k
91	[number]	k4	91
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
již	již	k6eAd1	již
před	před	k7c7	před
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
ji	on	k3xPp3gFnSc4	on
podpořit	podpořit	k5eAaPmF	podpořit
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ji	on	k3xPp3gFnSc4	on
tolerovat	tolerovat	k5eAaImF	tolerovat
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
titulu	titul	k1gInSc2	titul
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
zasedl	zasednout	k5eAaPmAgMnS	zasednout
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Rady	rada	k1gFnSc2	rada
vlády	vláda	k1gFnSc2	vláda
pro	pro	k7c4	pro
koordinaci	koordinace	k1gFnSc4	koordinace
boje	boj	k1gInSc2	boj
s	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
s	s	k7c7	s
následným	následný	k2eAgNnSc7d1	následné
schválením	schválení	k1gNnSc7	schválení
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zastával	zastávat	k5eAaImAgMnS	zastávat
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
vždy	vždy	k6eAd1	vždy
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
předtím	předtím	k6eAd1	předtím
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Robert	Robert	k1gMnSc1	Robert
Pelikán	Pelikán	k1gMnSc1	Pelikán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
však	však	k9	však
nový	nový	k2eAgMnSc1d1	nový
koordinátor	koordinátor	k1gMnSc1	koordinátor
boje	boj	k1gInSc2	boj
s	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
nebyl	být	k5eNaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
a	a	k8xC	a
automaticky	automaticky	k6eAd1	automaticky
řízení	řízení	k1gNnSc1	řízení
Rady	rada	k1gFnSc2	rada
připadlo	připadnout	k5eAaPmAgNnS	připadnout
premiérovi	premiér	k1gMnSc3	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
konfliktu	konflikt	k1gInSc3	konflikt
zájmů	zájem	k1gInPc2	zájem
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
opozicí	opozice	k1gFnSc7	opozice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
stranou	stranou	k6eAd1	stranou
Pirátů	pirát	k1gMnPc2	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
premiérovo	premiérův	k2eAgNnSc4d1	premiérovo
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
nebude	být	k5eNaImBp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
nic	nic	k3yNnSc1	nic
podnikat	podnikat	k5eAaImF	podnikat
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
se	se	k3xPyFc4	se
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
jako	jako	k8xC	jako
zástupce	zástupce	k1gMnSc1	zástupce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
oslav	oslava	k1gFnPc2	oslava
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínkové	vzpomínkový	k2eAgFnSc3d1	vzpomínková
ceremonii	ceremonie	k1gFnSc3	ceremonie
u	u	k7c2	u
Vítězného	vítězný	k2eAgInSc2d1	vítězný
oblouku	oblouk	k1gInSc2	oblouk
přihlížely	přihlížet	k5eAaImAgFnP	přihlížet
desítky	desítka	k1gFnPc1	desítka
státníků	státník	k1gMnPc2	státník
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prezidentů	prezident	k1gMnPc2	prezident
USA	USA	kA	USA
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
a	a	k8xC	a
Ruska	Ruska	k1gFnSc1	Ruska
Vladimira	Vladimir	k1gMnSc2	Vladimir
Putina	putin	k2eAgMnSc2d1	putin
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
investigativní	investigativní	k2eAgMnPc1d1	investigativní
novináři	novinář	k1gMnPc1	novinář
Sabina	Sabina	k1gFnSc1	Sabina
Slonková	Slonková	k1gFnSc1	Slonková
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Kubík	Kubík	k1gMnSc1	Kubík
uveřejnili	uveřejnit	k5eAaPmAgMnP	uveřejnit
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
Babišovým	Babišův	k2eAgMnSc7d1	Babišův
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
vypátrali	vypátrat	k5eAaPmAgMnP	vypátrat
a	a	k8xC	a
kontaktovali	kontaktovat	k5eAaImAgMnP	kontaktovat
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
mladší	mladý	k2eAgFnSc2d2	mladší
obvinil	obvinit	k5eAaPmAgMnS	obvinit
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
po	po	k7c4	po
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
aféry	aféra	k1gFnSc2	aféra
Čapí	čapí	k2eAgNnSc4d1	čapí
hnízdo	hnízdo	k1gNnSc4	hnízdo
"	"	kIx"	"
<g/>
uklidil	uklidit	k5eAaPmAgInS	uklidit
<g/>
"	"	kIx"	"
na	na	k7c4	na
Krym	Krym	k1gInSc4	Krym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
následně	následně	k6eAd1	následně
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
vůli	vůle	k1gFnSc3	vůle
zadržoval	zadržovat	k5eAaImAgMnS	zadržovat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
dokumentů	dokument	k1gInPc2	dokument
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
Čapího	čapí	k2eAgInSc2d1	čapí
hnízda	hnízdo	k1gNnPc4	hnízdo
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
reagoval	reagovat	k5eAaBmAgMnS	reagovat
sdělením	sdělení	k1gNnSc7	sdělení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
psychicky	psychicky	k6eAd1	psychicky
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
léky	lék	k1gInPc4	lék
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
odjel	odjet	k5eAaPmAgMnS	odjet
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
zveřejněné	zveřejněný	k2eAgFnPc4d1	zveřejněná
informace	informace	k1gFnPc4	informace
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
opozice	opozice	k1gFnSc1	opozice
premiéra	premiéra	k1gFnSc1	premiéra
Babiše	Babiše	k1gFnSc1	Babiše
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
k	k	k7c3	k
demisi	demise	k1gFnSc3	demise
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
přijal	přijmout	k5eAaPmAgInS	přijmout
usnesení	usnesení	k1gNnSc4	usnesení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nepřijatelnou	přijatelný	k2eNgFnSc4d1	nepřijatelná
účast	účast	k1gFnSc4	účast
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
kauzy	kauza	k1gFnSc2	kauza
Čapí	čapí	k2eAgInSc4d1	čapí
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nP	kdyby
poslanci	poslanec	k1gMnPc1	poslanec
vládě	vláda	k1gFnSc3	vláda
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
opět	opět	k6eAd1	opět
pověří	pověřit	k5eAaPmIp3nS	pověřit
sestavením	sestavení	k1gNnSc7	sestavení
nového	nový	k2eAgInSc2d1	nový
kabinetu	kabinet	k1gInSc2	kabinet
Babiše	Babiše	k1gFnSc2	Babiše
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
pak	pak	k6eAd1	pak
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
navzdory	navzdory	k7c3	navzdory
výzvám	výzva	k1gFnPc3	výzva
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
etickým	etický	k2eAgInSc7d1	etický
kodexem	kodex	k1gInSc7	kodex
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
neodstoupí	odstoupit	k5eNaPmIp3nS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
nedůvěře	nedůvěra	k1gFnSc6	nedůvěra
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
Babiš	Babiš	k1gInSc1	Babiš
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vládou	vláda	k1gFnSc7	vláda
je	být	k5eAaImIp3nS	být
však	však	k9	však
ustál	ustát	k5eAaPmAgMnS	ustát
<g/>
,	,	kIx,	,
poslanci	poslanec	k1gMnPc1	poslanec
koaliční	koaliční	k2eAgFnSc2d1	koaliční
ČSSD	ČSSD	kA	ČSSD
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
opustili	opustit	k5eAaPmAgMnP	opustit
sál	sál	k1gInSc4	sál
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
ANO	ano	k9	ano
pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
<g/>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
na	na	k7c4	na
kauzu	kauza	k1gFnSc4	kauza
reagovala	reagovat	k5eAaBmAgFnS	reagovat
uspořádáním	uspořádání	k1gNnSc7	uspořádání
několika	několik	k4yIc2	několik
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
s	s	k7c7	s
největších	veliký	k2eAgFnPc2d3	veliký
demonstrací	demonstrace	k1gFnPc2	demonstrace
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
při	při	k7c6	při
výročí	výročí	k1gNnSc6	výročí
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
spolek	spolek	k1gInSc4	spolek
Milion	milion	k4xCgInSc4	milion
chvilek	chvilka	k1gFnPc2	chvilka
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
akci	akce	k1gFnSc4	akce
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Demisi	demise	k1gFnSc4	demise
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
na	na	k7c6	na
Hradčanském	hradčanský	k2eAgNnSc6d1	Hradčanské
náměstí	náměstí	k1gNnSc6	náměstí
u	u	k7c2	u
sochy	socha	k1gFnSc2	socha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
několikatisícihlavý	několikatisícihlavý	k2eAgInSc1d1	několikatisícihlavý
dav	dav	k1gInSc1	dav
na	na	k7c4	na
pochod	pochod	k1gInSc4	pochod
přes	přes	k7c4	přes
Národní	národní	k2eAgFnSc4d1	národní
třídu	třída	k1gFnSc4	třída
na	na	k7c4	na
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
večer	večer	k1gInSc1	večer
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
konal	konat	k5eAaImAgInS	konat
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
spojený	spojený	k2eAgMnSc1d1	spojený
s	s	k7c7	s
protestem	protest	k1gInSc7	protest
proti	proti	k7c3	proti
Babišovi	Babiš	k1gMnSc3	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Řečníci	řečník	k1gMnPc1	řečník
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c6	o
svobodě	svoboda	k1gFnSc6	svoboda
a	a	k8xC	a
také	také	k9	také
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
současnou	současný	k2eAgFnSc4d1	současná
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
podporu	podpora	k1gFnSc4	podpora
Babišovi	Babiš	k1gMnSc3	Babiš
vyjádřily	vyjádřit	k5eAaPmAgFnP	vyjádřit
asi	asi	k9	asi
čtyři	čtyři	k4xCgFnPc1	čtyři
desítky	desítka	k1gFnPc1	desítka
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Klárově	Klárov	k1gInSc6	Klárov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
"	"	kIx"	"
<g/>
kauza	kauza	k1gFnSc1	kauza
Huawei	Huawe	k1gFnSc2	Huawe
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Národní	národní	k2eAgInSc1d1	národní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
kybernetickou	kybernetický	k2eAgFnSc4d1	kybernetická
a	a	k8xC	a
informační	informační	k2eAgFnSc4d1	informační
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
NÚKIB	NÚKIB	kA	NÚKIB
<g/>
)	)	kIx)	)
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
5G	[number]	k4	5G
mobilními	mobilní	k2eAgFnPc7d1	mobilní
technologiemi	technologie	k1gFnPc7	technologie
a	a	k8xC	a
telefony	telefon	k1gInPc7	telefon
čínské	čínský	k2eAgFnSc2d1	čínská
firmy	firma	k1gFnSc2	firma
Huawei	Huawe	k1gFnSc2	Huawe
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
napojení	napojení	k1gNnSc2	napojení
firmy	firma	k1gFnSc2	firma
na	na	k7c4	na
čínské	čínský	k2eAgFnPc4d1	čínská
tajné	tajný	k2eAgFnPc4d1	tajná
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Varování	varování	k1gNnSc1	varování
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
také	také	k9	také
čínské	čínský	k2eAgFnSc2d1	čínská
společnosti	společnost	k1gFnSc2	společnost
ZTE	ZTE	kA	ZTE
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
přikázal	přikázat	k5eAaPmAgMnS	přikázat
stažení	stažení	k1gNnSc4	stažení
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
Huawei	Huawe	k1gFnSc2	Huawe
z	z	k7c2	z
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
sešel	sejít	k5eAaPmAgMnS	sejít
se	se	k3xPyFc4	se
s	s	k7c7	s
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
s	s	k7c7	s
Babišem	Babiš	k1gMnSc7	Babiš
čínská	čínský	k2eAgFnSc1d1	čínská
ambasáda	ambasáda	k1gFnSc1	ambasáda
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
čínská	čínský	k2eAgFnSc1d1	čínská
strana	strana	k1gFnSc1	strana
bere	brát	k5eAaImIp3nS	brát
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
úsilí	úsilí	k1gNnSc2	úsilí
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
nápravu	náprava	k1gFnSc4	náprava
příslušných	příslušný	k2eAgFnPc2d1	příslušná
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
přijme	přijmout	k5eAaPmIp3nS	přijmout
účinná	účinný	k2eAgNnPc4d1	účinné
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
opakování	opakování	k1gNnSc4	opakování
podobných	podobný	k2eAgFnPc2d1	podobná
událostí	událost	k1gFnPc2	událost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Babiše	Babiš	k1gMnSc2	Babiš
česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
žádnou	žádný	k3yNgFnSc4	žádný
chybu	chyba	k1gFnSc4	chyba
neudělala	udělat	k5eNaPmAgFnS	udělat
a	a	k8xC	a
čínský	čínský	k2eAgMnSc1d1	čínský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
o	o	k7c4	o
setkání	setkání	k1gNnSc4	setkání
lhal	lhát	k5eAaImAgMnS	lhát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Zemanem	Zeman	k1gMnSc7	Zeman
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
Babiš	Babiš	k1gMnSc1	Babiš
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
všichni	všechen	k3xTgMnPc1	všechen
operátoři	operátor	k1gMnPc1	operátor
v	v	k7c6	v
ČR	ČR	kA	ČR
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
technologiích	technologie	k1gFnPc6	technologie
Huawei	Huawe	k1gFnSc2	Huawe
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nám	my	k3xPp1nPc3	my
to	ten	k3xDgNnSc1	ten
NÚKIB	NÚKIB	kA	NÚKIB
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
a	a	k8xC	a
předložil	předložit	k5eAaPmAgMnS	předložit
nějaké	nějaký	k3yIgFnSc2	nějaký
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Babiš	Babiš	k1gMnSc1	Babiš
i	i	k8xC	i
Zeman	Zeman	k1gMnSc1	Zeman
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
postup	postup	k1gInSc4	postup
NÚKIB	NÚKIB	kA	NÚKIB
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
Huawei	Huawe	k1gFnSc2	Huawe
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2019	[number]	k4	2019
na	na	k7c6	na
V.	V.	kA	V.
volebním	volební	k2eAgInSc6d1	volební
sjezdu	sjezd	k1gInSc6	sjezd
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
<g/>
,	,	kIx,	,
když	když	k8xS	když
neměl	mít	k5eNaImAgMnS	mít
žádného	žádný	k3yNgMnSc4	žádný
protikandidáta	protikandidát	k1gMnSc4	protikandidát
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
206	[number]	k4	206
hlasů	hlas	k1gInPc2	hlas
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
238	[number]	k4	238
přítomných	přítomný	k2eAgMnPc2d1	přítomný
delegátů	delegát	k1gMnPc2	delegát
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
oficiální	oficiální	k2eAgFnSc6d1	oficiální
návštěvě	návštěva	k1gFnSc6	návštěva
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
byl	být	k5eAaImAgInS	být
Babiš	Babiš	k1gInSc1	Babiš
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
přijat	přijat	k2eAgInSc1d1	přijat
prezidentem	prezident	k1gMnSc7	prezident
Donaldem	Donald	k1gMnSc7	Donald
Trumpem	Trump	k1gMnSc7	Trump
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
dvoustranných	dvoustranný	k2eAgInPc2d1	dvoustranný
rozhovorů	rozhovor	k1gInPc2	rozhovor
Babiš	Babiš	k1gMnSc1	Babiš
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
spojenectví	spojenectví	k1gNnSc4	spojenectví
USA	USA	kA	USA
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
trvá	trvat	k5eAaImIp3nS	trvat
již	již	k6eAd1	již
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Zmínil	zmínit	k5eAaPmAgInS	zmínit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželkou	manželka	k1gFnSc7	manželka
prezidenta	prezident	k1gMnSc2	prezident
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
byla	být	k5eAaImAgFnS	být
Američanka	Američanka	k1gFnSc1	Američanka
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
jednání	jednání	k1gNnSc2	jednání
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
kybernetická	kybernetický	k2eAgFnSc1d1	kybernetická
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
nákup	nákup	k1gInSc1	nákup
vrtulníků	vrtulník	k1gInPc2	vrtulník
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
možná	možná	k9	možná
dostavba	dostavba	k1gFnSc1	dostavba
jaderných	jaderný	k2eAgInPc2d1	jaderný
bloků	blok	k1gInPc2	blok
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
či	či	k8xC	či
dovoz	dovoz	k1gInSc1	dovoz
amerického	americký	k2eAgInSc2d1	americký
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
plynu	plyn	k1gInSc2	plyn
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
rovněž	rovněž	k9	rovněž
apeloval	apelovat	k5eAaImAgMnS	apelovat
na	na	k7c4	na
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezaváděl	zavádět	k5eNaImAgInS	zavádět
dovozní	dovozní	k2eAgNnPc4d1	dovozní
cla	clo	k1gNnPc4	clo
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průběh	průběh	k1gInSc1	průběh
jednání	jednání	k1gNnSc2	jednání
kladně	kladně	k6eAd1	kladně
hodnotila	hodnotit	k5eAaImAgFnS	hodnotit
řada	řada	k1gFnSc1	řada
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
ODS	ODS	kA	ODS
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
ocenil	ocenit	k5eAaPmAgMnS	ocenit
<g/>
,	,	kIx,	,
že	že	k8xS	že
schůzka	schůzka	k1gFnSc1	schůzka
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
"	"	kIx"	"
<g/>
tradici	tradice	k1gFnSc4	tradice
návštěv	návštěva	k1gFnPc2	návštěva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zná	znát	k5eAaImIp3nS	znát
Česko	Česko	k1gNnSc1	Česko
již	již	k6eAd1	již
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
uvědomují	uvědomovat	k5eAaImIp3nP	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
velkým	velký	k2eAgMnPc3d1	velký
podporovatelům	podporovatel	k1gMnPc3	podporovatel
euroatlantické	euroatlantický	k2eAgFnSc2d1	euroatlantická
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Místopředseda	místopředseda	k1gMnSc1	místopředseda
Pirátů	pirát	k1gMnPc2	pirát
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Peksa	Peks	k1gMnSc2	Peks
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
schůzka	schůzka	k1gFnSc1	schůzka
může	moct	k5eAaImIp3nS	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
předcházení	předcházení	k1gNnSc3	předcházení
obchodních	obchodní	k2eAgFnPc2d1	obchodní
válek	válka	k1gFnPc2	válka
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
naopak	naopak	k6eAd1	naopak
přišli	přijít	k5eAaPmAgMnP	přijít
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
podporu	podpora	k1gFnSc4	podpora
sankcí	sankce	k1gFnPc2	sankce
vůči	vůči	k7c3	vůči
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
<g/>
Americká	americký	k2eAgNnPc1d1	americké
média	médium	k1gNnPc1	médium
byla	být	k5eAaImAgNnP	být
k	k	k7c3	k
Babišovi	Babišův	k2eAgMnPc1d1	Babišův
kritická	kritický	k2eAgFnSc1d1	kritická
<g/>
,	,	kIx,	,
porovnávala	porovnávat	k5eAaImAgFnS	porovnávat
ho	on	k3xPp3gMnSc4	on
s	s	k7c7	s
Trumpem	Trump	k1gInSc7	Trump
a	a	k8xC	a
uváděla	uvádět	k5eAaImAgFnS	uvádět
poměrně	poměrně	k6eAd1	poměrně
podrobně	podrobně	k6eAd1	podrobně
jeho	jeho	k3xOp3gFnPc4	jeho
domácí	domácí	k2eAgFnPc4d1	domácí
kauzy	kauza	k1gFnPc4	kauza
<g/>
.	.	kIx.	.
</s>
<s>
Deb	Deb	k?	Deb
Riechmannová	Riechmannová	k1gFnSc1	Riechmannová
si	se	k3xPyFc3	se
všimla	všimnout	k5eAaPmAgFnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babiš	Babiš	k1gInSc1	Babiš
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Trump	Trump	k1gMnSc1	Trump
bohatý	bohatý	k2eAgMnSc1d1	bohatý
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
také	také	k6eAd1	také
on	on	k3xPp3gMnSc1	on
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
nacionalistické	nacionalistický	k2eAgFnSc6d1	nacionalistická
vlně	vlna	k1gFnSc6	vlna
a	a	k8xC	a
odmítání	odmítání	k1gNnSc1	odmítání
migrantů	migrant	k1gMnPc2	migrant
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
chválil	chválit	k5eAaImAgMnS	chválit
Trumpovu	Trumpův	k2eAgFnSc4d1	Trumpova
Zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
parafrázoval	parafrázovat	k5eAaBmAgMnS	parafrázovat
jeho	jeho	k3xOp3gFnSc4	jeho
rétoriku	rétorika	k1gFnSc4	rétorika
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Make	Make	k1gInSc1	Make
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
great	great	k2eAgMnSc1d1	great
again	again	k1gMnSc1	again
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
si	se	k3xPyFc3	se
vede	vést	k5eAaImIp3nS	vést
velmi	velmi	k6eAd1	velmi
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
silné	silný	k2eAgMnPc4d1	silný
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgInPc4d1	dobrý
obchodní	obchodní	k2eAgInPc4d1	obchodní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
Koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
podal	podat	k5eAaPmAgInS	podat
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
demisi	demise	k1gFnSc4	demise
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
za	za	k7c4	za
ANO	ano	k9	ano
Jan	Jan	k1gMnSc1	Jan
Kněžínek	Kněžínek	k1gMnSc1	Kněžínek
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nominoval	nominovat	k5eAaBmAgMnS	nominovat
Marii	Maria	k1gFnSc3	Maria
Benešovou	Benešová	k1gFnSc4	Benešová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
post	post	k1gInSc4	post
ministryně	ministryně	k1gFnSc2	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
zastávala	zastávat	k5eAaImAgFnS	zastávat
i	i	k9	i
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
.	.	kIx.	.
</s>
<s>
Obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
dopadů	dopad	k1gInPc2	dopad
výměny	výměna	k1gFnSc2	výměna
ministrů	ministr	k1gMnPc2	ministr
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
kauzy	kauza	k1gFnSc2	kauza
Čapí	čapí	k2eAgNnSc4d1	čapí
hnízdo	hnízdo	k1gNnSc4	hnízdo
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
další	další	k2eAgInPc1d1	další
protesty	protest	k1gInPc1	protest
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
organizované	organizovaný	k2eAgFnSc6d1	organizovaná
opět	opět	k6eAd1	opět
především	především	k9	především
spolkem	spolek	k1gInSc7	spolek
Milion	milion	k4xCgInSc1	milion
chvilek	chvilka	k1gFnPc2	chvilka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
ještě	ještě	k6eAd1	ještě
zesílily	zesílit	k5eAaPmAgInP	zesílit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejněn	k2eAgInPc4d1	zveřejněn
předběžné	předběžný	k2eAgInPc4d1	předběžný
výsledky	výsledek	k1gInPc4	výsledek
dvou	dva	k4xCgInPc2	dva
auditů	audit	k1gInPc2	audit
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgFnPc2	který
Babiš	Babiš	k1gInSc1	Babiš
i	i	k9	i
po	po	k7c6	po
převodu	převod	k1gInSc6	převod
akcií	akcie	k1gFnPc2	akcie
Agrofertu	Agrofert	k1gInSc2	Agrofert
do	do	k7c2	do
svěřeneckých	svěřenecký	k2eAgInPc2d1	svěřenecký
fondů	fond	k1gInPc2	fond
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
Agrofert	Agrofert	k1gInSc4	Agrofert
proto	proto	k8xC	proto
nebyl	být	k5eNaImAgMnS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
čerpat	čerpat	k5eAaImF	čerpat
evropské	evropský	k2eAgFnPc4d1	Evropská
dotace	dotace	k1gFnPc4	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
organizátorů	organizátor	k1gMnPc2	organizátor
účastnilo	účastnit	k5eAaImAgNnS	účastnit
120	[number]	k4	120
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
se	se	k3xPyFc4	se
Babiš	Babiš	k1gMnSc1	Babiš
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
barmskou	barmský	k2eAgFnSc7d1	barmská
vůdkyní	vůdkyně	k1gFnSc7	vůdkyně
<g/>
,	,	kIx,	,
nositelkou	nositelka	k1gFnSc7	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
a	a	k8xC	a
někdejší	někdejší	k2eAgInSc1d1	někdejší
disidentkou	disidentka	k1gFnSc7	disidentka
Aun	Aun	k1gFnSc1	Aun
Schan	Schan	k1gMnSc1	Schan
Su	Su	k?	Su
Ťij	Ťij	k1gMnSc1	Ťij
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yQgFnSc4	který
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c4	o
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
i	i	k8xC	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozvoje	rozvoj	k1gInSc2	rozvoj
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
či	či	k8xC	či
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
ocenil	ocenit	k5eAaPmAgMnS	ocenit
její	její	k3xOp3gNnSc4	její
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
demokratizaci	demokratizace	k1gFnSc4	demokratizace
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kauzy	kauza	k1gFnPc1	kauza
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Čapí	čapí	k2eAgNnSc1d1	čapí
hnízdo	hnízdo	k1gNnSc1	hnízdo
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejkontroverznějších	kontroverzní	k2eAgInPc2d3	nejkontroverznější
počinů	počin	k1gInPc2	počin
kolem	kolem	k7c2	kolem
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
je	být	k5eAaImIp3nS	být
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
"	"	kIx"	"
<g/>
kauza	kauza	k1gFnSc1	kauza
Čapí	čapí	k2eAgNnSc1d1	čapí
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
kauzou	kauza	k1gFnSc7	kauza
podezřelý	podezřelý	k2eAgMnSc1d1	podezřelý
z	z	k7c2	z
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
dotačního	dotační	k2eAgInSc2d1	dotační
podvodu	podvod	k1gInSc2	podvod
(	(	kIx(	(
<g/>
§	§	k?	§
212	[number]	k4	212
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
)	)	kIx)	)
a	a	k8xC	a
poškození	poškození	k1gNnSc3	poškození
finančních	finanční	k2eAgInPc2d1	finanční
zájmů	zájem	k1gInPc2	zájem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
§	§	k?	§
260	[number]	k4	260
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
)	)	kIx)	)
ve	v	k7c6	v
"	"	kIx"	"
<g/>
zločinném	zločinný	k2eAgNnSc6d1	zločinné
spolčení	spolčení	k1gNnSc6	spolčení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
§	§	k?	§
89	[number]	k4	89
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
17	[number]	k4	17
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podstatou	podstata	k1gFnSc7	podstata
kauzy	kauza	k1gFnSc2	kauza
je	být	k5eAaImIp3nS	být
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
účelové	účelový	k2eAgNnSc4d1	účelové
čerpání	čerpání	k1gNnSc4	čerpání
dotace	dotace	k1gFnSc2	dotace
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
určené	určený	k2eAgFnPc1d1	určená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
nebo	nebo	k8xC	nebo
středně	středně	k6eAd1	středně
velké	velký	k2eAgFnPc4d1	velká
firmy	firma	k1gFnPc4	firma
<g/>
,	,	kIx,	,
když	když	k8xS	když
podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
koncern	koncern	k1gInSc4	koncern
Agrofert	Agrofert	k1gMnSc1	Agrofert
formálně	formálně	k6eAd1	formálně
převedl	převést	k5eAaPmAgMnS	převést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
společnost	společnost	k1gFnSc1	společnost
Čapí	čapět	k5eAaImIp3nS	čapět
hnízdo	hnízdo	k1gNnSc4	hnízdo
na	na	k7c4	na
jiného	jiný	k2eAgMnSc4d1	jiný
majitele	majitel	k1gMnSc4	majitel
krytého	krytý	k2eAgInSc2d1	krytý
listinnými	listinný	k2eAgFnPc7d1	listinná
akciemi	akcie	k1gFnPc7	akcie
na	na	k7c4	na
majitele	majitel	k1gMnPc4	majitel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Agrofert	Agrofert	k1gInSc1	Agrofert
by	by	kYmCp3nS	by
dotaci	dotace	k1gFnSc4	dotace
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
majitelem	majitel	k1gMnSc7	majitel
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
lihovaru	lihovar	k1gInSc2	lihovar
stala	stát	k5eAaPmAgFnS	stát
společnost	společnost	k1gFnSc1	společnost
IMOBA	IMOBA	kA	IMOBA
z	z	k7c2	z
holdingu	holding	k1gInSc2	holding
Agrofert	Agrofert	k1gInSc1	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
ručením	ručení	k1gNnSc7	ručení
omezeným	omezený	k2eAgNnSc7d1	omezené
ZZN	ZZN	kA	ZZN
AGRO	AGRO	kA	AGRO
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
z	z	k7c2	z
holdingu	holding	k1gInSc2	holding
Agrofert	Agrofert	k1gMnSc1	Agrofert
transformovala	transformovat	k5eAaBmAgFnS	transformovat
na	na	k7c4	na
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
anonymními	anonymní	k2eAgFnPc7d1	anonymní
akciemi	akcie	k1gFnPc7	akcie
na	na	k7c4	na
majitele	majitel	k1gMnPc4	majitel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Farmu	farma	k1gFnSc4	farma
Čapí	čapí	k2eAgNnSc1d1	čapí
hnízdo	hnízdo	k1gNnSc1	hnízdo
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
dotaci	dotace	k1gFnSc4	dotace
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
korun	koruna	k1gFnPc2	koruna
od	od	k7c2	od
ROP	ropa	k1gFnPc2	ropa
Střední	střední	k2eAgFnSc2d1	střední
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byly	být	k5eAaImAgFnP	být
zákonem	zákon	k1gInSc7	zákon
zakázány	zakázat	k5eAaPmNgFnP	zakázat
anonymní	anonymní	k2eAgFnPc1d1	anonymní
akcie	akcie	k1gFnPc1	akcie
a	a	k8xC	a
Čapí	čapí	k2eAgNnSc1d1	čapí
hnízdo	hnízdo	k1gNnSc1	hnízdo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
společnosti	společnost	k1gFnSc2	společnost
IMOBA	IMOBA	kA	IMOBA
<g/>
.	.	kIx.	.
<g/>
Policie	policie	k1gFnSc1	policie
obdržela	obdržet	k5eAaPmAgFnS	obdržet
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
širší	široký	k2eAgFnSc2d2	širší
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
kauza	kauza	k1gFnSc1	kauza
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
Čapímu	čapí	k2eAgNnSc3d1	čapí
hnízdu	hnízdo	k1gNnSc3	hnízdo
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
svolána	svolat	k5eAaPmNgFnS	svolat
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
schůze	schůze	k1gFnSc1	schůze
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zde	zde	k6eAd1	zde
Babiš	Babiš	k1gMnSc1	Babiš
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
majiteli	majitel	k1gMnSc3	majitel
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
inkriminované	inkriminovaný	k2eAgFnSc6d1	inkriminovaná
době	doba	k1gFnSc6	doba
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gFnPc4	jeho
dospělé	dospělý	k2eAgFnPc4d1	dospělá
děti	dítě	k1gFnPc4	dítě
Adriana	Adriana	k1gFnSc1	Adriana
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
partnerky	partnerka	k1gFnSc2	partnerka
Moniky	Monika	k1gFnSc2	Monika
Martin	Martin	k1gMnSc1	Martin
Herodes	Herodes	k1gMnSc1	Herodes
<g/>
.	.	kIx.	.
<g/>
Policie	policie	k1gFnSc1	policie
ukončila	ukončit	k5eAaPmAgFnS	ukončit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2017	[number]	k4	2017
a	a	k8xC	a
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
zahájení	zahájení	k1gNnSc4	zahájení
trestního	trestní	k2eAgInSc2d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
vůči	vůči	k7c3	vůči
Andreji	Andrej	k1gMnSc3	Andrej
Babišovi	Babiš	k1gMnSc3	Babiš
<g/>
,	,	kIx,	,
Jaroslavu	Jaroslav	k1gMnSc3	Jaroslav
Faltýnkovi	Faltýnek	k1gMnSc3	Faltýnek
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
osobám	osoba	k1gFnPc3	osoba
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
základě	základ	k1gInSc6	základ
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
trestního	trestní	k2eAgNnSc2d1	trestní
oznámení	oznámení	k1gNnSc2	oznámení
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
podezření	podezření	k1gNnSc4	podezření
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
dotačního	dotační	k2eAgInSc2d1	dotační
podvodu	podvod	k1gInSc2	podvod
(	(	kIx(	(
<g/>
§	§	k?	§
212	[number]	k4	212
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
)	)	kIx)	)
a	a	k8xC	a
poškození	poškození	k1gNnSc3	poškození
finančních	finanční	k2eAgInPc2d1	finanční
zájmů	zájem	k1gInPc2	zájem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
§	§	k?	§
260	[number]	k4	260
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
)	)	kIx)	)
ve	v	k7c6	v
"	"	kIx"	"
<g/>
zločinném	zločinný	k2eAgNnSc6d1	zločinné
spolčení	spolčení	k1gNnSc6	spolčení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
§	§	k?	§
89	[number]	k4	89
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
17	[number]	k4	17
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podle	podle	k7c2	podle
obvinění	obvinění	k1gNnSc2	obvinění
dotaci	dotace	k1gFnSc4	dotace
určenou	určený	k2eAgFnSc4d1	určená
malým	malý	k2eAgNnSc7d1	malé
a	a	k8xC	a
středně	středně	k6eAd1	středně
velkým	velký	k2eAgFnPc3d1	velká
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
čerpal	čerpat	k5eAaImAgInS	čerpat
koncern	koncern	k1gInSc1	koncern
Agrofert	Agrofert	k1gInSc1	Agrofert
skrze	skrze	k?	skrze
nastrčenou	nastrčený	k2eAgFnSc4d1	nastrčená
malou	malý	k2eAgFnSc4d1	malá
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
měla	mít	k5eAaImAgFnS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
škoda	škoda	k1gFnSc1	škoda
49	[number]	k4	49
997	[number]	k4	997
443,36	[number]	k4	443,36
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2017	[number]	k4	2017
pro	pro	k7c4	pro
vydání	vydání	k1gNnSc4	vydání
obou	dva	k4xCgMnPc2	dva
poslanců	poslanec	k1gMnPc2	poslanec
k	k	k7c3	k
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
znovu	znovu	k6eAd1	znovu
nabyli	nabýt	k5eAaPmAgMnP	nabýt
poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nechají	nechat	k5eAaPmIp3nP	nechat
vydat	vydat	k5eAaPmF	vydat
k	k	k7c3	k
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
k	k	k7c3	k
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
skutečně	skutečně	k6eAd1	skutečně
znovu	znovu	k6eAd1	znovu
vydala	vydat	k5eAaPmAgFnS	vydat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
111	[number]	k4	111
ku	k	k7c3	k
69	[number]	k4	69
hlasům	hlas	k1gInPc3	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
žalobce	žalobce	k1gMnSc1	žalobce
stíhání	stíhání	k1gNnSc2	stíhání
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Faltýnka	Faltýnka	k1gFnSc1	Faltýnka
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
osob	osoba	k1gFnPc2	osoba
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Babiše	Babiš	k1gMnSc2	Babiš
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
příbuzných	příbuzný	k1gMnPc2	příbuzný
však	však	k9	však
nadále	nadále	k6eAd1	nadále
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
<g/>
Udělení	udělení	k1gNnSc1	udělení
dotace	dotace	k1gFnSc2	dotace
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
také	také	k6eAd1	také
vyšetřováno	vyšetřovat	k5eAaImNgNnS	vyšetřovat
Evropským	evropský	k2eAgInSc7d1	evropský
úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
podvodům	podvod	k1gInPc3	podvod
(	(	kIx(	(
<g/>
OLAF	OLAF	kA	OLAF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
OLAF	OLAF	kA	OLAF
své	svůj	k3xOyFgNnSc4	svůj
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
a	a	k8xC	a
nálezy	nález	k1gInPc4	nález
zaslal	zaslat	k5eAaPmAgMnS	zaslat
českým	český	k2eAgInPc3d1	český
úřadům	úřad	k1gInPc3	úřad
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc3d1	Evropská
komisi	komise	k1gFnSc3	komise
<g/>
.	.	kIx.	.
</s>
<s>
Žalobci	žalobce	k1gMnPc1	žalobce
doporučili	doporučit	k5eAaPmAgMnP	doporučit
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
financí	finance	k1gFnPc2	finance
zprávu	zpráva	k1gFnSc4	zpráva
OLAFu	OLAFa	k1gFnSc4	OLAFa
nezveřejňovat	zveřejňovat	k5eNaImF	zveřejňovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jejího	její	k3xOp3gNnSc2	její
zařazení	zařazení	k1gNnSc2	zařazení
do	do	k7c2	do
vyšetřovacího	vyšetřovací	k2eAgInSc2d1	vyšetřovací
spisu	spis	k1gInSc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
však	však	k9	však
přesto	přesto	k8xC	přesto
brzy	brzy	k6eAd1	brzy
pronikla	proniknout	k5eAaPmAgFnS	proniknout
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc4d1	celé
znění	znění	k1gNnSc4	znění
poprvé	poprvé	k6eAd1	poprvé
uveřejnily	uveřejnit	k5eAaPmAgInP	uveřejnit
servery	server	k1gInPc1	server
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
iHned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Economia	Economia	k1gFnSc1	Economia
11	[number]	k4	11
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novinářů	novinář	k1gMnPc2	novinář
zpráva	zpráva	k1gFnSc1	zpráva
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
dotační	dotační	k2eAgInSc4d1	dotační
podvod	podvod	k1gInSc4	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
§	§	k?	§
216	[number]	k4	216
a	a	k8xC	a
§	§	k?	§
260	[number]	k4	260
českého	český	k2eAgInSc2d1	český
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
závěry	závěr	k1gInPc4	závěr
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
figuruje	figurovat	k5eAaImIp3nS	figurovat
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hybatel	hybatel	k1gMnSc1	hybatel
kauzy	kauza	k1gFnSc2	kauza
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
může	moct	k5eAaImIp3nS	moct
padnout	padnout	k5eAaPmF	padnout
i	i	k9	i
na	na	k7c4	na
Babišovy	Babišův	k2eAgMnPc4d1	Babišův
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
<g/>
,	,	kIx,	,
Janu	Jana	k1gFnSc4	Jana
Mayerovou	Mayerová	k1gFnSc4	Mayerová
a	a	k8xC	a
Josefa	Josefa	k1gFnSc1	Josefa
Nenadála	nadát	k5eNaBmAgFnS	nadát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dotaci	dotace	k1gFnSc4	dotace
vyřizovali	vyřizovat	k5eAaImAgMnP	vyřizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
Babiš	Babiš	k1gInSc1	Babiš
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
OLAFu	OLAFus	k1gInSc2	OLAFus
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vedl	vést	k5eAaImAgMnS	vést
"	"	kIx"	"
<g/>
zkorumpovaný	zkorumpovaný	k2eAgMnSc1d1	zkorumpovaný
<g/>
"	"	kIx"	"
OLAF	OLAF	kA	OLAF
bylo	být	k5eAaImAgNnS	být
zpolitizované	zpolitizovaný	k2eAgNnSc1d1	zpolitizované
jak	jak	k8xC	jak
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Šéf	šéf	k1gMnSc1	šéf
úřadu	úřad	k1gInSc2	úřad
OLAF	OLAF	kA	OLAF
Nicholas	Nicholas	k1gMnSc1	Nicholas
Ilett	Ilett	k1gMnSc1	Ilett
se	se	k3xPyFc4	se
důrazně	důrazně	k6eAd1	důrazně
ohradil	ohradit	k5eAaPmAgMnS	ohradit
proti	proti	k7c3	proti
Babišovým	Babišův	k2eAgFnPc3d1	Babišova
výhradám	výhrada	k1gFnPc3	výhrada
k	k	k7c3	k
nezávislosti	nezávislost	k1gFnSc3	nezávislost
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
společnost	společnost	k1gFnSc1	společnost
IMOBA	IMOBA	kA	IMOBA
částku	částka	k1gFnSc4	částka
50	[number]	k4	50
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
vrátila	vrátit	k5eAaPmAgFnS	vrátit
Regionálnímu	regionální	k2eAgInSc3d1	regionální
operačnímu	operační	k2eAgInSc3d1	operační
programu	program	k1gInSc3	program
(	(	kIx(	(
<g/>
ROP	ropa	k1gFnPc2	ropa
<g/>
)	)	kIx)	)
Střední	střední	k2eAgFnPc1d1	střední
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dotaci	dotace	k1gFnSc4	dotace
původně	původně	k6eAd1	původně
přidělil	přidělit	k5eAaPmAgInS	přidělit
<g/>
.	.	kIx.	.
</s>
<s>
Trvala	trvat	k5eAaImAgFnS	trvat
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
na	na	k7c6	na
tvrzení	tvrzení	k1gNnSc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
neznamená	znamenat	k5eNaImIp3nS	znamenat
přiznání	přiznání	k1gNnSc4	přiznání
porušení	porušení	k1gNnSc2	porušení
dotačních	dotační	k2eAgFnPc2d1	dotační
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Generální	generální	k2eAgNnSc1d1	generální
finanční	finanční	k2eAgNnSc1d1	finanční
ředitelství	ředitelství	k1gNnSc1	ředitelství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
nereagovalo	reagovat	k5eNaBmAgNnS	reagovat
na	na	k7c4	na
oznámení	oznámení	k1gNnSc4	oznámení
německé	německý	k2eAgFnSc2d1	německá
protistrany	protistrana	k1gFnSc2	protistrana
o	o	k7c4	o
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
daňových	daňový	k2eAgInPc2d1	daňový
úniků	únik	k1gInPc2	únik
pomocí	pomocí	k7c2	pomocí
objednávek	objednávka	k1gFnPc2	objednávka
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
od	od	k7c2	od
německého	německý	k2eAgInSc2d1	německý
Agrofertu	Agrofert	k1gInSc2	Agrofert
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
Čapí	čapí	k2eAgNnSc4d1	čapí
hnízdo	hnízdo	k1gNnSc4	hnízdo
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
280	[number]	k4	280
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
období	období	k1gNnSc4	období
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
finanční	finanční	k2eAgFnSc1d1	finanční
správa	správa	k1gFnSc1	správa
zahájila	zahájit	k5eAaPmAgFnS	zahájit
řízení	řízení	k1gNnSc4	řízení
proti	proti	k7c3	proti
jednomu	jeden	k4xCgInSc3	jeden
subjektu	subjekt	k1gInSc3	subjekt
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zadavatelů	zadavatel	k1gMnPc2	zadavatel
reklamy	reklama	k1gFnSc2	reklama
z	z	k7c2	z
holdingu	holding	k1gInSc2	holding
Agrofert	Agrofert	k1gInSc4	Agrofert
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
bez	bez	k7c2	bez
doměření	doměření	k1gNnSc2	doměření
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
bez	bez	k7c2	bez
dalšího	další	k2eAgInSc2d1	další
podnětu	podnět	k1gInSc2	podnět
orgánům	orgán	k1gInPc3	orgán
činným	činný	k2eAgInPc3d1	činný
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
se	se	k3xPyFc4	se
věcí	věc	k1gFnSc7	věc
zabývají	zabývat	k5eAaImIp3nP	zabývat
kriminalisté	kriminalista	k1gMnPc1	kriminalista
7	[number]	k4	7
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
Odboru	odbor	k1gInSc2	odbor
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
kriminality	kriminalita	k1gFnSc2	kriminalita
krajského	krajský	k2eAgNnSc2d1	krajské
ředitelství	ředitelství	k1gNnSc2	ředitelství
policie	policie	k1gFnSc2	policie
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
na	na	k7c6	na
krácení	krácení	k1gNnSc6	krácení
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jinou	jiný	k2eAgFnSc4d1	jiná
kauzu	kauza	k1gFnSc4	kauza
a	a	k8xC	a
řeší	řešit	k5eAaImIp3nP	řešit
ji	on	k3xPp3gFnSc4	on
tak	tak	k9	tak
jiní	jiný	k2eAgMnPc1d1	jiný
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
<g/>
.	.	kIx.	.
</s>
<s>
Farma	farma	k1gFnSc1	farma
podle	podle	k7c2	podle
odborného	odborný	k2eAgInSc2d1	odborný
posudku	posudek	k1gInSc2	posudek
inkasovala	inkasovat	k5eAaBmAgFnS	inkasovat
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgFnPc4d2	vyšší
částky	částka	k1gFnPc4	částka
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
reálné	reálný	k2eAgNnSc1d1	reálné
<g/>
,	,	kIx,	,
a	a	k8xC	a
reklama	reklama	k1gFnSc1	reklama
postrádala	postrádat	k5eAaImAgFnS	postrádat
elementární	elementární	k2eAgInSc4d1	elementární
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
Čapího	čapí	k2eAgNnSc2d1	čapí
hnízda	hnízdo	k1gNnSc2	hnízdo
využívaly	využívat	k5eAaImAgFnP	využívat
především	především	k9	především
firmy	firma	k1gFnPc1	firma
z	z	k7c2	z
holdingu	holding	k1gInSc2	holding
Agrofert	Agrofert	k1gInSc1	Agrofert
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
server	server	k1gInSc1	server
iRozhlas	iRozhlas	k1gInSc4	iRozhlas
seznam	seznam	k1gInSc4	seznam
10	[number]	k4	10
nejzávažnějších	závažný	k2eAgInPc2d3	nejzávažnější
důkazů	důkaz	k1gInPc2	důkaz
trestního	trestní	k2eAgNnSc2d1	trestní
stíhání	stíhání	k1gNnSc2	stíhání
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
případ	případ	k1gInSc1	případ
uzavřít	uzavřít	k5eAaPmF	uzavřít
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
měsíce	měsíc	k1gInSc2	měsíc
pak	pak	k6eAd1	pak
web	web	k1gInSc1	web
Česká	český	k2eAgFnSc1d1	Česká
justice	justice	k1gFnSc1	justice
informoval	informovat	k5eAaBmAgInS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyšetřovatele	vyšetřovatel	k1gMnPc4	vyšetřovatel
kauzy	kauza	k1gFnSc2	kauza
Čapí	čapět	k5eAaImIp3nS	čapět
hnízdo	hnízdo	k1gNnSc1	hnízdo
Pavla	Pavel	k1gMnSc2	Pavel
Nevtípila	Nevtípila	k1gFnSc2	Nevtípila
prošetřovala	prošetřovat	k5eAaImAgFnS	prošetřovat
Generální	generální	k2eAgFnSc1d1	generální
inspekce	inspekce	k1gFnSc1	inspekce
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
kvůli	kvůli	k7c3	kvůli
úniku	únik	k1gInSc3	únik
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
stížnosti	stížnost	k1gFnSc2	stížnost
samotného	samotný	k2eAgMnSc4d1	samotný
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
probíhaly	probíhat	k5eAaImAgFnP	probíhat
personální	personální	k2eAgFnPc1d1	personální
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
pražské	pražský	k2eAgFnSc2d1	Pražská
policejní	policejní	k2eAgFnSc2d1	policejní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Čapí	čapět	k5eAaImIp3nS	čapět
hnízdo	hnízdo	k1gNnSc4	hnízdo
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
po	po	k7c6	po
nátlaku	nátlak	k1gInSc6	nátlak
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
odešel	odejít	k5eAaPmAgMnS	odejít
ředitel	ředitel	k1gMnSc1	ředitel
GIBS	GIBS	kA	GIBS
Michal	Michal	k1gMnSc1	Michal
Murín	Murín	k1gMnSc1	Murín
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
odešel	odejít	k5eAaPmAgMnS	odejít
šéf	šéf	k1gMnSc1	šéf
pražské	pražský	k2eAgFnSc2d1	Pražská
policie	policie	k1gFnSc2	policie
Miloš	Miloš	k1gMnSc1	Miloš
Trojánek	Trojánek	k1gMnSc1	Trojánek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
pak	pak	k9	pak
náměstek	náměstek	k1gMnSc1	náměstek
pro	pro	k7c4	pro
kriminální	kriminální	k2eAgFnSc4d1	kriminální
policii	policie	k1gFnSc4	policie
a	a	k8xC	a
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
Ivan	Ivan	k1gMnSc1	Ivan
Smékal	Smékal	k1gMnSc1	Smékal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
byla	být	k5eAaImAgFnS	být
lhůta	lhůta	k1gFnSc1	lhůta
pro	pro	k7c4	pro
uzavření	uzavření	k1gNnSc4	uzavření
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
kauzy	kauza	k1gFnSc2	kauza
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
a	a	k8xC	a
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
Nevtípil	Nevtípil	k1gMnSc1	Nevtípil
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svým	svůj	k3xOyFgMnPc3	svůj
nadřízeným	nadřízený	k1gMnPc3	nadřízený
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
kauzu	kauza	k1gFnSc4	kauza
dostat	dostat	k5eAaPmF	dostat
před	před	k7c4	před
soud	soud	k1gInSc4	soud
bez	bez	k7c2	bez
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
mladšího	mladý	k2eAgMnSc2d2	mladší
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
nedostupnost	nedostupnost	k1gFnSc4	nedostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
Nevtípil	Nevtípil	k1gMnSc1	Nevtípil
opět	opět	k6eAd1	opět
prověřován	prověřovat	k5eAaImNgMnS	prověřovat
z	z	k7c2	z
GIBS	GIBS	kA	GIBS
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
obrátili	obrátit	k5eAaPmAgMnP	obrátit
přímo	přímo	k6eAd1	přímo
Nevtípilovi	Nevtípilův	k2eAgMnPc1d1	Nevtípilův
nadřízení	nadřízený	k1gMnPc1	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
ukončila	ukončit	k5eAaPmAgFnS	ukončit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
až	až	k6eAd1	až
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
požádala	požádat	k5eAaPmAgFnS	požádat
žalobce	žalobce	k1gMnSc4	žalobce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
postavil	postavit	k5eAaPmAgMnS	postavit
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
před	před	k7c4	před
soud	soud	k1gInSc4	soud
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
podvodu	podvod	k1gInSc2	podvod
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
podal	podat	k5eAaPmAgMnS	podat
advokát	advokát	k1gMnSc1	advokát
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiše	k1gFnSc2	Babiše
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Bartončík	Bartončík	k1gMnSc1	Bartončík
<g/>
,	,	kIx,	,
v	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
Městského	městský	k2eAgNnSc2d1	Městské
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zastavení	zastavení	k1gNnSc4	zastavení
trestního	trestní	k2eAgNnSc2d1	trestní
stíhání	stíhání	k1gNnSc2	stíhání
svého	svůj	k3xOyFgMnSc4	svůj
klienta	klient	k1gMnSc4	klient
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutek	skutek	k1gInSc1	skutek
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgInSc2	který
je	být	k5eAaImIp3nS	být
obviněn	obviněn	k2eAgMnSc1d1	obviněn
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
údajně	údajně	k6eAd1	údajně
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kauza	kauza	k1gFnSc1	kauza
údajného	údajný	k2eAgInSc2d1	údajný
únosu	únos	k1gInSc2	únos
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
mladšího	mladý	k2eAgMnSc2d2	mladší
====	====	k?	====
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
vyšetřovaných	vyšetřovaný	k2eAgFnPc2d1	vyšetřovaná
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
Čapího	čapí	k2eAgNnSc2d1	čapí
hnízda	hnízdo	k1gNnSc2	hnízdo
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
letech	let	k1gInPc6	let
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
držet	držet	k5eAaImF	držet
vlastnický	vlastnický	k2eAgInSc4d1	vlastnický
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
farmě	farma	k1gFnSc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
hospitalizován	hospitalizovat	k5eAaBmNgMnS	hospitalizovat
na	na	k7c6	na
psychiatrii	psychiatrie	k1gFnSc6	psychiatrie
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
policie	policie	k1gFnSc1	policie
vyšetřovala	vyšetřovat	k5eAaImAgFnS	vyšetřovat
<g/>
,	,	kIx,	,
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
psychiatrička	psychiatrička	k1gFnSc1	psychiatrička
Dita	Dita	k1gFnSc1	Dita
Protopopová	Protopopový	k2eAgFnSc1d1	Protopopový
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
pracující	pracující	k2eAgMnSc1d1	pracující
pro	pro	k7c4	pro
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gInSc2	Babiš
<g/>
,	,	kIx,	,
lékařský	lékařský	k2eAgInSc1d1	lékařský
posudek	posudek	k1gInSc1	posudek
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
nezpůsobilosti	nezpůsobilost	k1gFnSc6	nezpůsobilost
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
trestního	trestní	k2eAgNnSc2d1	trestní
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Policisté	policista	k1gMnPc1	policista
však	však	k9	však
omluvě	omluva	k1gFnSc6	omluva
neuvěřili	uvěřit	k5eNaPmAgMnP	uvěřit
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
se	se	k3xPyFc4	se
pečovatelem	pečovatel	k1gMnSc7	pečovatel
Babiše	Babiše	k1gFnSc2	Babiše
mladšího	mladý	k2eAgMnSc2d2	mladší
stal	stát	k5eAaPmAgMnS	stát
Petr	Petr	k1gMnSc1	Petr
Protopopov	Protopopov	k1gInSc4	Protopopov
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Dity	Dita	k1gFnSc2	Dita
Protopopové	Protopopový	k2eAgFnSc2d1	Protopopový
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2016	[number]	k4	2016
a	a	k8xC	a
2017	[number]	k4	2017
cestoval	cestovat	k5eAaImAgInS	cestovat
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
a	a	k8xC	a
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
na	na	k7c4	na
anektovaný	anektovaný	k2eAgInSc4d1	anektovaný
Krym	Krym	k1gInSc4	Krym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
i	i	k9	i
dočasně	dočasně	k6eAd1	dočasně
zanechal	zanechat	k5eAaPmAgMnS	zanechat
s	s	k7c7	s
ženou	žena	k1gFnSc7	žena
jménem	jméno	k1gNnSc7	jméno
Jelizaveta	Jelizaveta	k1gFnSc1	Jelizaveta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Krymu	Krym	k1gInSc2	Krym
odeslal	odeslat	k5eAaPmAgMnS	odeslat
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
mladší	mladý	k2eAgMnSc1d2	mladší
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
e-mail	eail	k1gInSc1	e-mail
české	český	k2eAgFnSc3d1	Česká
policii	policie	k1gFnSc3	policie
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
unesen	unést	k5eAaPmNgMnS	unést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
dostat	dostat	k5eAaPmF	dostat
pryč	pryč	k6eAd1	pryč
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
probíhající	probíhající	k2eAgFnSc2d1	probíhající
kauzy	kauza	k1gFnSc2	kauza
a	a	k8xC	a
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
čeká	čekat	k5eAaImIp3nS	čekat
"	"	kIx"	"
<g/>
nedobrovolný	dobrovolný	k2eNgInSc1d1	nedobrovolný
psychiatrický	psychiatrický	k2eAgInSc1d1	psychiatrický
zákrok	zákrok	k1gInSc1	zákrok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
podnět	podnět	k1gInSc4	podnět
začala	začít	k5eAaPmAgFnS	začít
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
a	a	k8xC	a
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Babiš	Babiš	k1gInSc4	Babiš
mladší	mladý	k2eAgFnSc2d2	mladší
navrátil	navrátit	k5eAaPmAgMnS	navrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
případ	případ	k1gInSc4	případ
prošetřovala	prošetřovat	k5eAaImAgFnS	prošetřovat
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kauze	kauza	k1gFnSc3	kauza
Čapí	čapí	k2eAgNnSc1d1	čapí
hnízdo	hnízdo	k1gNnSc1	hnízdo
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
nevyslechla	vyslechnout	k5eNaPmAgFnS	vyslechnout
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2018	[number]	k4	2018
jej	on	k3xPp3gNnSc2	on
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
Ženevě	Ženeva	k1gFnSc6	Ženeva
vyhledali	vyhledat	k5eAaPmAgMnP	vyhledat
reportéři	reportér	k1gMnPc1	reportér
Slonková	Slonková	k1gFnSc1	Slonková
a	a	k8xC	a
Kubík	kubík	k1gInSc1	kubík
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
na	na	k7c4	na
skrytou	skrytý	k2eAgFnSc4d1	skrytá
kameru	kamera	k1gFnSc4	kamera
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
svou	svůj	k3xOyFgFnSc4	svůj
verzi	verze	k1gFnSc4	verze
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Uveřejnění	uveřejnění	k1gNnSc1	uveřejnění
reportáže	reportáž	k1gFnSc2	reportáž
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
bouřlivé	bouřlivý	k2eAgFnPc4d1	bouřlivá
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
i	i	k8xC	i
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
masových	masový	k2eAgInPc2d1	masový
demonstrací	demonstrace	k1gFnSc7	demonstrace
proti	proti	k7c3	proti
premiéru	premiér	k1gMnSc3	premiér
Babišovi	Babiš	k1gMnSc3	Babiš
a	a	k8xC	a
opoziční	opoziční	k2eAgFnPc1d1	opoziční
strany	strana	k1gFnPc1	strana
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
hlasování	hlasování	k1gNnSc4	hlasování
o	o	k7c4	o
vyslovení	vyslovení	k1gNnSc1	vyslovení
nedůvěře	nedůvěra	k1gFnSc3	nedůvěra
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
vláda	vláda	k1gFnSc1	vláda
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
komunistů	komunista	k1gMnPc2	komunista
ustála	ustát	k5eAaPmAgFnS	ustát
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
starší	starý	k2eAgMnSc1d2	starší
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
exmanželkou	exmanželka	k1gFnSc7	exmanželka
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
následně	následně	k6eAd1	následně
přestali	přestat	k5eAaPmAgMnP	přestat
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
českými	český	k2eAgNnPc7d1	české
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc1	jejich
právní	právní	k2eAgMnSc1d1	právní
zástupce	zástupce	k1gMnSc1	zástupce
podal	podat	k5eAaPmAgMnS	podat
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
na	na	k7c4	na
reportéry	reportér	k1gMnPc4	reportér
pro	pro	k7c4	pro
porušení	porušení	k1gNnSc4	porušení
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Korunové	korunový	k2eAgInPc1d1	korunový
dluhopisy	dluhopis	k1gInPc1	dluhopis
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babišovy	Babišův	k2eAgInPc1d1	Babišův
příjmy	příjem	k1gInPc1	příjem
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaké	jaký	k3yQgNnSc4	jaký
přiznal	přiznat	k5eAaPmAgMnS	přiznat
v	v	k7c6	v
majetkovém	majetkový	k2eAgNnSc6d1	majetkové
přiznání	přiznání	k1gNnSc6	přiznání
<g/>
,	,	kIx,	,
a	a	k8xC	a
možná	možná	k9	možná
se	se	k3xPyFc4	se
též	též	k9	též
dopustil	dopustit	k5eAaPmAgMnS	dopustit
krácení	krácení	k1gNnSc4	krácení
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
totiž	totiž	k9	totiž
jako	jako	k9	jako
soukromá	soukromý	k2eAgFnSc1d1	soukromá
osoba	osoba	k1gFnSc1	osoba
koupil	koupit	k5eAaPmAgMnS	koupit
od	od	k7c2	od
Agrofertu	Agrofert	k1gInSc2	Agrofert
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gFnSc4	jehož
byl	být	k5eAaImAgMnS	být
majoritním	majoritní	k2eAgMnSc7d1	majoritní
vlastníkem	vlastník	k1gMnSc7	vlastník
<g/>
)	)	kIx)	)
korunové	korunový	k2eAgInPc4d1	korunový
dluhopisy	dluhopis	k1gInPc4	dluhopis
celkem	celkem	k6eAd1	celkem
za	za	k7c4	za
1,5	[number]	k4	1,5
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
se	s	k7c7	s
zúročením	zúročení	k1gNnSc7	zúročení
6	[number]	k4	6
%	%	kIx~	%
p.	p.	k?	p.
a.	a.	k?	a.
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gInPc2	jejichž
úroků	úrok	k1gInPc2	úrok
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
nemusela	muset	k5eNaImAgFnS	muset
platit	platit	k5eAaImF	platit
daň	daň	k1gFnSc4	daň
<g/>
,	,	kIx,	,
a	a	k8xC	a
Babiš	Babiš	k1gMnSc1	Babiš
tedy	tedy	k9	tedy
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
nezdanitelných	zdanitelný	k2eNgFnPc2d1	nezdanitelná
90	[number]	k4	90
<g />
.	.	kIx.	.
</s>
<s>
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
<g/>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
(	(	kIx(	(
<g/>
poslanci	poslanec	k1gMnPc1	poslanec
za	za	k7c7	za
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
novináři	novinář	k1gMnSc3	novinář
z	z	k7c2	z
Deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
E15	E15	k1gFnSc1	E15
a	a	k8xC	a
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
však	však	k9	však
podle	podle	k7c2	podle
Babišových	Babišová	k1gFnPc2	Babišová
daňových	daňový	k2eAgFnPc2d1	daňová
přiznání	přiznání	k1gNnSc4	přiznání
usuzovali	usuzovat	k5eAaImAgMnP	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměl	mít	k5eNaImAgMnS	mít
na	na	k7c4	na
koupi	koupě	k1gFnSc4	koupě
oněch	onen	k3xDgInPc2	onen
dluhopisů	dluhopis	k1gInPc2	dluhopis
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
vlastní	vlastní	k2eAgInSc4d1	vlastní
kapitál	kapitál	k1gInSc4	kapitál
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podle	podle	k7c2	podle
Babišova	Babišův	k2eAgNnSc2d1	Babišovo
předchozího	předchozí	k2eAgNnSc2d1	předchozí
tvrzení	tvrzení	k1gNnSc2	tvrzení
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
jeho	jeho	k3xOp3gInPc1	jeho
čisté	čistý	k2eAgInPc1d1	čistý
příjmy	příjem	k1gInPc1	příjem
po	po	k7c6	po
zdanění	zdanění	k1gNnSc6	zdanění
v	v	k7c6	v
období	období	k1gNnSc6	období
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
pouze	pouze	k6eAd1	pouze
1,11	[number]	k4	1,11
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
zohlednění	zohlednění	k1gNnSc2	zohlednění
jeho	jeho	k3xOp3gMnPc2	jeho
soukromých	soukromý	k2eAgMnPc2d1	soukromý
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
přitom	přitom	k6eAd1	přitom
koupil	koupit	k5eAaPmAgMnS	koupit
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
dluhopisů	dluhopis	k1gInPc2	dluhopis
za	za	k7c4	za
1,252	[number]	k4	1,252
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
veškerých	veškerý	k3xTgInPc2	veškerý
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgFnPc4	který
si	se	k3xPyFc3	se
dluhopisy	dluhopis	k1gInPc4	dluhopis
koupil	koupit	k5eAaPmAgInS	koupit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nebyl	být	k5eNaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
takto	takto	k6eAd1	takto
Babišem	Babiš	k1gMnSc7	Babiš
investované	investovaný	k2eAgFnSc2d1	investovaná
do	do	k7c2	do
Agrofertu	Agrofert	k1gInSc2	Agrofert
byly	být	k5eAaImAgInP	být
navíc	navíc	k6eAd1	navíc
za	za	k7c4	za
netržní	tržní	k2eNgInSc4d1	netržní
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc4d1	vysoký
úrok	úrok	k1gInSc4	úrok
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
jak	jak	k6eAd1	jak
k	k	k7c3	k
poškozování	poškozování	k1gNnSc3	poškozování
Agrofertu	Agrofert	k1gInSc2	Agrofert
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ke	k	k7c3	k
krácení	krácení	k1gNnSc3	krácení
daní	daň	k1gFnPc2	daň
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
daních	daň	k1gFnPc6	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Agrofert	Agrofert	k1gMnSc1	Agrofert
totiž	totiž	k9	totiž
započítával	započítávat	k5eAaImAgMnS	započítávat
úroky	úrok	k1gInPc4	úrok
za	za	k7c7	za
dluhopisy	dluhopis	k1gInPc7	dluhopis
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
provozních	provozní	k2eAgInPc2d1	provozní
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
by	by	kYmCp3nS	by
tak	tak	k9	tak
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c6	o
krácení	krácení	k1gNnSc6	krácení
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
cca	cca	kA	cca
22,8	[number]	k4	22,8
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiše	k1gFnSc2	Babiše
podáno	podat	k5eAaPmNgNnS	podat
trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
pro	pro	k7c4	pro
porušení	porušení	k1gNnSc4	porušení
povinnosti	povinnost	k1gFnSc2	povinnost
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
cizího	cizí	k2eAgInSc2d1	cizí
majetku	majetek	k1gInSc2	majetek
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
§	§	k?	§
220	[number]	k4	220
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
a	a	k8xC	a
pro	pro	k7c4	pro
zkrácení	zkrácení	k1gNnSc4	zkrácení
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
poplatků	poplatek	k1gInPc2	poplatek
a	a	k8xC	a
podobných	podobný	k2eAgFnPc2d1	podobná
povinných	povinný	k2eAgFnPc2d1	povinná
plateb	platba	k1gFnPc2	platba
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
§	§	k?	§
240	[number]	k4	240
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
podezření	podezření	k1gNnSc1	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
transakce	transakce	k1gFnSc1	transakce
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
krácením	krácení	k1gNnSc7	krácení
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
Policií	policie	k1gFnSc7	policie
ČR	ČR	kA	ČR
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
vážné	vážný	k2eAgFnPc1d1	vážná
<g/>
.	.	kIx.	.
</s>
<s>
Trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
(	(	kIx(	(
<g/>
bod	bod	k1gInSc1	bod
26	[number]	k4	26
<g/>
)	)	kIx)	)
taktéž	taktéž	k?	taktéž
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
podezření	podezření	k1gNnSc1	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
dluhopisů	dluhopis	k1gInPc2	dluhopis
půjčil	půjčit	k5eAaPmAgMnS	půjčit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
Agrofertu	Agrofert	k1gInSc2	Agrofert
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
dceřiné	dceřiný	k2eAgFnSc2d1	dceřiná
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
za	za	k7c4	za
netržní	tržní	k2eNgInSc4d1	netržní
úrok	úrok	k1gInSc4	úrok
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
také	také	k9	také
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
poškozování	poškozování	k1gNnSc4	poškozování
společnosti	společnost	k1gFnSc2	společnost
Agrofert	Agrofert	k1gMnSc1	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
právním	právní	k2eAgInSc7d1	právní
předpokladem	předpoklad	k1gInSc7	předpoklad
trestního	trestní	k2eAgNnSc2d1	trestní
oznámení	oznámení	k1gNnSc2	oznámení
proti	proti	k7c3	proti
Babišovi	Babiš	k1gMnSc3	Babiš
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
usnesení	usnesení	k1gNnSc6	usnesení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
precedentním	precedentní	k2eAgInSc6d1	precedentní
případu	případ	k1gInSc6	případ
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
majetek	majetek	k1gInSc1	majetek
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
jako	jako	k8xS	jako
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jejího	její	k3xOp3gMnSc4	její
akcionáře	akcionář	k1gMnSc4	akcionář
majetkem	majetek	k1gInSc7	majetek
cizím	cizí	k2eAgMnPc3d1	cizí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Babišovými	Babišův	k2eAgInPc7d1	Babišův
příjmy	příjem	k1gInPc7	příjem
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
pořad	pořad	k1gInSc1	pořad
Reportéři	reportér	k1gMnPc1	reportér
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
díl	díl	k1gInSc4	díl
"	"	kIx"	"
<g/>
Ministerské	ministerský	k2eAgInPc4d1	ministerský
počty	počet	k1gInPc4	počet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
příjmům	příjem	k1gInPc3	příjem
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
nesrovnalosti	nesrovnalost	k1gFnSc2	nesrovnalost
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
a	a	k8xC	a
odpovědět	odpovědět	k5eAaPmF	odpovědět
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
nejasností	nejasnost	k1gFnPc2	nejasnost
ohledně	ohledně	k7c2	ohledně
korunových	korunový	k2eAgInPc2d1	korunový
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Babiš	Babiš	k1gMnSc1	Babiš
poslal	poslat	k5eAaPmAgMnS	poslat
jako	jako	k8xC	jako
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
dopis	dopis	k1gInSc1	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
vládní	vládní	k2eAgFnSc7d1	vládní
krizí	krize	k1gFnSc7	krize
a	a	k8xC	a
demonstracemi	demonstrace	k1gFnPc7	demonstrace
proti	proti	k7c3	proti
Babišovi	Babiš	k1gMnSc3	Babiš
a	a	k8xC	a
Zemanovi	Zeman	k1gMnSc3	Zeman
<g/>
,	,	kIx,	,
usnesením	usnesení	k1gNnSc7	usnesení
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
veřejných	veřejný	k2eAgNnPc6d1	veřejné
vystoupeních	vystoupení	k1gNnPc6	vystoupení
opakovaně	opakovaně	k6eAd1	opakovaně
lhal	lhát	k5eAaImAgMnS	lhát
a	a	k8xC	a
zneužíval	zneužívat	k5eAaImAgMnS	zneužívat
svá	svůj	k3xOyFgNnPc4	svůj
média	médium	k1gNnPc4	médium
ke	k	k7c3	k
kompromitaci	kompromitace	k1gFnSc3	kompromitace
politických	politický	k2eAgMnPc2d1	politický
soupeřů	soupeř	k1gMnPc2	soupeř
<g/>
"	"	kIx"	"
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
odvoláním	odvolání	k1gNnSc7	odvolání
Babiše	Babiš	k1gInSc2	Babiš
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
ministra	ministr	k1gMnSc2	ministr
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2018	[number]	k4	2018
zveřejnilo	zveřejnit	k5eAaPmAgNnS	zveřejnit
Forum	forum	k1gNnSc1	forum
<g/>
24	[number]	k4	24
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
prověřuje	prověřovat	k5eAaImIp3nS	prověřovat
Národní	národní	k2eAgFnSc1d1	národní
centrála	centrála	k1gFnSc1	centrála
proti	proti	k7c3	proti
organizovanému	organizovaný	k2eAgInSc3d1	organizovaný
zločinu	zločin	k1gInSc3	zločin
kvůli	kvůli	k7c3	kvůli
prodeji	prodej	k1gInSc3	prodej
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
Profrost	Profrost	k1gFnSc1	Profrost
z	z	k7c2	z
holdingu	holding	k1gInSc2	holding
Agrofert	Agrofert	k1gInSc4	Agrofert
právě	právě	k6eAd1	právě
Agrofertu	Agrofert	k1gInSc2	Agrofert
a	a	k8xC	a
manažerce	manažerka	k1gFnSc3	manažerka
Profrostu	Profrost	k1gInSc2	Profrost
Simoně	Simona	k1gFnSc6	Simona
Sokolové	Sokolové	k2eAgNnSc7d1	Sokolové
<g/>
.	.	kIx.	.
</s>
<s>
Prodejem	prodej	k1gInSc7	prodej
akcií	akcie	k1gFnPc2	akcie
měl	mít	k5eAaImAgMnS	mít
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c4	na
korunové	korunový	k2eAgInPc4d1	korunový
dluhopisy	dluhopis	k1gInPc4	dluhopis
získat	získat	k5eAaPmF	získat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
serveru	server	k1gInSc2	server
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
prodej	prodej	k1gInSc4	prodej
podílu	podíl	k1gInSc2	podíl
firmy	firma	k1gFnSc2	firma
uměle	uměle	k6eAd1	uměle
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šestkrát	šestkrát	k6eAd1	šestkrát
předražený	předražený	k2eAgMnSc1d1	předražený
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
tak	tak	k6eAd1	tak
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
daňovému	daňový	k2eAgInSc3d1	daňový
úniku	únik	k1gInSc3	únik
přes	přes	k7c4	přes
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
obvodní	obvodní	k2eAgFnSc1d1	obvodní
státní	státní	k2eAgFnSc1d1	státní
zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
4	[number]	k4	4
Blanka	Blanka	k1gFnSc1	Blanka
Valsamisová	Valsamisový	k2eAgFnSc1d1	Valsamisová
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
případem	případ	k1gInSc7	případ
korunových	korunový	k2eAgInPc2d1	korunový
dluhopisů	dluhopis	k1gInPc2	dluhopis
zabývá	zabývat	k5eAaImIp3nS	zabývat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podezření	podezření	k1gNnSc2	podezření
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
porušení	porušení	k1gNnSc2	porušení
povinnosti	povinnost	k1gFnSc2	povinnost
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
cizího	cizí	k2eAgInSc2d1	cizí
majetku	majetek	k1gInSc2	majetek
dle	dle	k7c2	dle
§	§	k?	§
220	[number]	k4	220
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
a	a	k8xC	a
3	[number]	k4	3
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
však	však	k9	však
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
z	z	k7c2	z
městského	městský	k2eAgNnSc2d1	Městské
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
sloučili	sloučit	k5eAaPmAgMnP	sloučit
několik	několik	k4yIc4	několik
trestních	trestní	k2eAgNnPc2d1	trestní
oznámení	oznámení	k1gNnPc2	oznámení
do	do	k7c2	do
jednoho	jeden	k4xCgMnSc2	jeden
a	a	k8xC	a
případ	případ	k1gInSc4	případ
odložili	odložit	k5eAaPmAgMnP	odložit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Únik	únik	k1gInSc4	únik
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
ČEZ	ČEZ	kA	ČEZ
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
se	se	k3xPyFc4	se
Policie	policie	k1gFnSc1	policie
začala	začít	k5eAaPmAgFnS	začít
zabývat	zabývat	k5eAaImF	zabývat
trestním	trestní	k2eAgNnSc7d1	trestní
oznámením	oznámení	k1gNnSc7	oznámení
na	na	k7c4	na
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gInSc2	Babiš
ohledně	ohledně	k7c2	ohledně
nelegálního	legální	k2eNgInSc2d1	nelegální
úniku	únik	k1gInSc2	únik
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
ČEZ	ČEZ	kA	ČEZ
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
zneužití	zneužití	k1gNnSc2	zneužití
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
s	s	k7c7	s
trestní	trestní	k2eAgFnSc7d1	trestní
sazbou	sazba	k1gFnSc7	sazba
pět	pět	k4xCc4	pět
až	až	k9	až
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
bulharské	bulharský	k2eAgFnSc2d1	bulharská
části	část	k1gFnSc2	část
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
bulharské	bulharský	k2eAgFnSc2d1	bulharská
společnosti	společnost	k1gFnSc2	společnost
Inercom	Inercom	k1gInSc1	Inercom
a	a	k8xC	a
arbitrážním	arbitrážní	k2eAgNnSc6d1	arbitrážní
řízení	řízení	k1gNnSc6	řízení
ČEZu	ČEZa	k1gFnSc4	ČEZa
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
důvěrných	důvěrný	k2eAgFnPc2d1	důvěrná
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Bulharský	bulharský	k2eAgMnSc1d1	bulharský
premiér	premiér	k1gMnSc1	premiér
Bojko	Bojko	k1gNnSc1	Bojko
Borisov	Borisovo	k1gNnPc2	Borisovo
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvěrné	důvěrný	k2eAgInPc4d1	důvěrný
dokumenty	dokument	k1gInPc4	dokument
dostal	dostat	k5eAaPmAgMnS	dostat
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
toto	tento	k3xDgNnSc4	tento
obvinění	obvinění	k1gNnSc4	obvinění
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
Finanční	finanční	k2eAgFnSc2d1	finanční
správy	správa	k1gFnSc2	správa
===	===	k?	===
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
sestříhaného	sestříhaný	k2eAgInSc2d1	sestříhaný
záznamu	záznam	k1gInSc2	záznam
odposlechu	odposlech	k1gInSc2	odposlech
Babiše	Babiše	k1gFnSc2	Babiše
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
publikovala	publikovat	k5eAaBmAgFnS	publikovat
na	na	k7c6	na
webu	web	k1gInSc6	web
Skupina	skupina	k1gFnSc1	skupina
Julius	Julius	k1gMnSc1	Julius
Šuman	Šuman	k1gMnSc1	Šuman
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
novináři	novinář	k1gMnPc1	novinář
několika	několik	k4yIc2	několik
deníků	deník	k1gInPc2	deník
dovozují	dovozovat	k5eAaImIp3nP	dovozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babišovy	Babišův	k2eAgInPc1d1	Babišův
výrazy	výraz	k1gInPc1	výraz
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
jedeme	jet	k5eAaImIp1nP	jet
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
ty	ten	k3xDgInPc4	ten
naši	náš	k3xOp1gMnPc1	náš
klekli	kleknout	k5eAaPmAgMnP	kleknout
na	na	k7c4	na
tu	tu	k6eAd1	tu
FAU	FAU	kA	FAU
<g />
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
insolvenci	insolvence	k1gFnSc6	insolvence
<g/>
,	,	kIx,	,
obstavený	obstavený	k2eAgInSc4d1	obstavený
účty	účet	k1gInPc4	účet
<g/>
,	,	kIx,	,
vagóny	vagón	k1gInPc4	vagón
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
mohou	moct	k5eAaImIp3nP	moct
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
jako	jako	k8xS	jako
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
cíleně	cíleně	k6eAd1	cíleně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Finanční	finanční	k2eAgFnSc4d1	finanční
správu	správa	k1gFnSc4	správa
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomocí	pomocí	k7c2	pomocí
zajišťovacích	zajišťovací	k2eAgInPc2d1	zajišťovací
příkazů	příkaz	k1gInPc2	příkaz
DPH	DPH	kA	DPH
de	de	k?	de
facto	facto	k1gNnSc1	facto
zlikvidovala	zlikvidovat	k5eAaPmAgFnS	zlikvidovat
firmu	firma	k1gFnSc4	firma
FAU	FAU	kA	FAU
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
označil	označit	k5eAaPmAgInS	označit
právní	právní	k2eAgInSc1d1	právní
výklad	výklad	k1gInSc1	výklad
finanční	finanční	k2eAgFnSc2d1	finanční
správy	správa	k1gFnSc2	správa
za	za	k7c4	za
absurdní	absurdní	k2eAgFnPc4d1	absurdní
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Finanční	finanční	k2eAgFnSc1d1	finanční
správa	správa	k1gFnSc1	správa
postupovala	postupovat	k5eAaImAgFnS	postupovat
nezákonným	zákonný	k2eNgInSc7d1	nezákonný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
majitelé	majitel	k1gMnPc1	majitel
firmy	firma	k1gFnSc2	firma
FAU	FAU	kA	FAU
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
po	po	k7c6	po
státu	stát	k1gInSc6	stát
žádat	žádat	k5eAaImF	žádat
náhradu	náhrada	k1gFnSc4	náhrada
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
Finanční	finanční	k2eAgFnSc2d1	finanční
správy	správa	k1gFnSc2	správa
Gabriela	Gabriela	k1gFnSc1	Gabriela
Štěpanyová	Štěpanyová	k1gFnSc1	Štěpanyová
naznačené	naznačený	k2eAgFnSc2d1	naznačená
souvislosti	souvislost	k1gFnSc2	souvislost
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
mluvčí	mluvčí	k1gMnSc1	mluvčí
Agrofertu	Agrofert	k1gInSc2	Agrofert
Karel	Karel	k1gMnSc1	Karel
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
spojitost	spojitost	k1gFnSc4	spojitost
holdingu	holding	k1gInSc2	holding
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
FAU	FAU	kA	FAU
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
však	však	k9	však
uvedly	uvést	k5eAaPmAgFnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Agrofert	Agrofert	k1gMnSc1	Agrofert
měl	mít	k5eAaImAgMnS	mít
o	o	k7c4	o
sklad	sklad	k1gInSc4	sklad
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
také	také	k9	také
přimíchávala	přimíchávat	k5eAaImAgFnS	přimíchávat
biopaliva	biopaliva	k1gFnSc1	biopaliva
do	do	k7c2	do
nafty	nafta	k1gFnSc2	nafta
a	a	k8xC	a
benzinu	benzin	k1gInSc2	benzin
<g/>
,	,	kIx,	,
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
firma	firma	k1gFnSc1	firma
FAU	FAU	kA	FAU
ho	on	k3xPp3gNnSc4	on
nechtěla	chtít	k5eNaImAgFnS	chtít
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
premiéra	premiér	k1gMnSc2	premiér
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
prošetření	prošetření	k1gNnSc6	prošetření
případu	případ	k1gInSc2	případ
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obratem	obratem	k6eAd1	obratem
poukázalo	poukázat	k5eAaPmAgNnS	poukázat
na	na	k7c4	na
již	již	k6eAd1	již
probíhající	probíhající	k2eAgNnSc4d1	probíhající
řešení	řešení	k1gNnSc4	řešení
dvou	dva	k4xCgFnPc2	dva
kasačních	kasační	k2eAgFnPc2d1	kasační
stížností	stížnost	k1gFnPc2	stížnost
spjatých	spjatý	k2eAgFnPc2d1	spjatá
s	s	k7c7	s
případem	případ	k1gInSc7	případ
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Sabiny	Sabina	k1gFnSc2	Sabina
Slonkové	Slonkové	k2eAgInSc1d1	Slonkové
odposlech	odposlech	k1gInSc1	odposlech
také	také	k9	také
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
měl	mít	k5eAaImAgMnS	mít
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
daňového	daňový	k2eAgNnSc2d1	daňové
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
neveřejné	veřejný	k2eNgNnSc4d1	neveřejné
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
výhradně	výhradně	k6eAd1	výhradně
mezi	mezi	k7c7	mezi
daňovým	daňový	k2eAgInSc7d1	daňový
subjektem	subjekt	k1gInSc7	subjekt
a	a	k8xC	a
finančním	finanční	k2eAgInSc7d1	finanční
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popud	popud	k1gInSc4	popud
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Milana	Milan	k1gMnSc2	Milan
Chovance	Chovanec	k1gMnSc2	Chovanec
se	s	k7c7	s
případem	případ	k1gInSc7	případ
zabývá	zabývat	k5eAaImIp3nS	zabývat
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ministra	ministr	k1gMnSc2	ministr
Jurečky	Jurečka	k1gMnPc4	Jurečka
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
policie	policie	k1gFnSc1	policie
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
nahrávky	nahrávka	k1gFnPc4	nahrávka
pravé	pravý	k2eAgFnPc4d1	pravá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sestříhané	sestříhaný	k2eAgInPc4d1	sestříhaný
kousky	kousek	k1gInPc4	kousek
zvukového	zvukový	k2eAgInSc2d1	zvukový
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
FAU	FAU	kA	FAU
následně	následně	k6eAd1	následně
spor	spor	k1gInSc4	spor
s	s	k7c7	s
Finanční	finanční	k2eAgFnSc7d1	finanční
správou	správa	k1gFnSc7	správa
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
kasační	kasační	k2eAgFnSc1d1	kasační
stížnost	stížnost	k1gFnSc1	stížnost
této	tento	k3xDgFnSc2	tento
státní	státní	k2eAgFnSc2d1	státní
instituce	instituce	k1gFnSc2	instituce
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
právního	právní	k2eAgMnSc2d1	právní
zástupce	zástupce	k1gMnSc2	zástupce
FAU	FAU	kA	FAU
Alfréda	Alfréd	k1gMnSc2	Alfréd
Šrámka	Šrámek	k1gMnSc2	Šrámek
to	ten	k3xDgNnSc1	ten
otevírá	otevírat	k5eAaImIp3nS	otevírat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
náhradě	náhrada	k1gFnSc3	náhrada
škody	škoda	k1gFnSc2	škoda
od	od	k7c2	od
státu	stát	k1gInSc2	stát
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
přes	přes	k7c4	přes
jednu	jeden	k4xCgFnSc4	jeden
miliardu	miliarda	k4xCgFnSc4	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
Babiš	Babiš	k1gMnSc1	Babiš
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
finanční	finanční	k2eAgFnSc4d1	finanční
a	a	k8xC	a
celní	celní	k2eAgFnSc4d1	celní
správu	správa	k1gFnSc4	správa
zneužíval	zneužívat	k5eAaImAgMnS	zneužívat
a	a	k8xC	a
úkoloval	úkolovat	k5eAaImAgMnS	úkolovat
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc1d1	finanční
správa	správa	k1gFnSc1	správa
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
samostatně	samostatně	k6eAd1	samostatně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastní	vlastní	k2eAgFnSc2d1	vlastní
analytické	analytický	k2eAgFnSc2d1	analytická
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zadání	zadání	k1gNnSc2	zadání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
a	a	k8xC	a
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
médií	médium	k1gNnPc2	médium
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Agrofert	Agrofert	k1gInSc1	Agrofert
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
akvizici	akvizice	k1gFnSc4	akvizice
vydavatelského	vydavatelský	k2eAgInSc2d1	vydavatelský
domu	dům	k1gInSc2	dům
Mafra	Mafra	k1gMnSc1	Mafra
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
můžu	můžu	k?	můžu
tady	tady	k6eAd1	tady
veřejně	veřejně	k6eAd1	veřejně
prohlásit	prohlásit	k5eAaPmF	prohlásit
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
mojich	mojicha	k1gFnPc2	mojicha
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
vnuček	vnučka	k1gFnPc2	vnučka
<g/>
,	,	kIx,	,
že	že	k8xS	že
já	já	k3xPp1nSc1	já
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nebudu	být	k5eNaImBp1nS	být
nikdy	nikdy	k6eAd1	nikdy
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
redaktorů	redaktor	k1gMnPc2	redaktor
v	v	k7c6	v
Mafře	Mafra	k1gFnSc6	Mafra
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
anonymním	anonymní	k2eAgInSc6d1	anonymní
twitterovém	twitterový	k2eAgInSc6d1	twitterový
účtu	účet	k1gInSc6	účet
objevily	objevit	k5eAaPmAgFnP	objevit
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgNnPc6	který
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gInSc4	Babiš
hovořil	hovořit	k5eAaImAgMnS	hovořit
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
redaktorem	redaktor	k1gMnSc7	redaktor
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
Dnes	dnes	k6eAd1	dnes
Markem	Marek	k1gMnSc7	Marek
Přibilem	Přibil	k1gMnSc7	Přibil
o	o	k7c6	o
připravovaných	připravovaný	k2eAgInPc6d1	připravovaný
článcích	článek	k1gInPc6	článek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nahrávce	nahrávka	k1gFnSc6	nahrávka
majitel	majitel	k1gMnSc1	majitel
deníku	deník	k1gInSc2	deník
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
měly	mít	k5eAaImAgFnP	mít
publikovat	publikovat	k5eAaBmF	publikovat
materiály	materiál	k1gInPc7	materiál
proti	proti	k7c3	proti
ministru	ministr	k1gMnSc3	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
a	a	k8xC	a
ministru	ministr	k1gMnSc3	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Chovancovi	Chovanec	k1gMnSc3	Chovanec
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
následně	následně	k6eAd1	následně
autenticitu	autenticita	k1gFnSc4	autenticita
nahrávek	nahrávka	k1gFnPc2	nahrávka
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
zmanipulované	zmanipulovaný	k2eAgInPc4d1	zmanipulovaný
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
ho	on	k3xPp3gInSc4	on
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Přibil	přibít	k5eAaPmAgMnS	přibít
<g/>
)	)	kIx)	)
mi	já	k3xPp1nSc3	já
tam	tam	k6eAd1	tam
něco	něco	k3yInSc1	něco
podsouval	podsouvat	k5eAaImAgMnS	podsouvat
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
kopal	kopat	k5eAaImAgMnS	kopat
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
rozhovor	rozhovor	k1gInSc1	rozhovor
byl	být	k5eAaImAgInS	být
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
provokace	provokace	k1gFnSc1	provokace
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
vím	vědět	k5eAaImIp1nS	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zároveň	zároveň	k6eAd1	zároveň
podal	podat	k5eAaPmAgMnS	podat
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
právníka	právník	k1gMnSc2	právník
Aleše	Aleš	k1gMnSc2	Aleš
Rozehnala	Rozehnal	k1gMnSc2	Rozehnal
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Babiš	Babiš	k1gInSc1	Babiš
ovlivňováním	ovlivňování	k1gNnSc7	ovlivňování
obsahu	obsah	k1gInSc2	obsah
dopustil	dopustit	k5eAaPmAgMnS	dopustit
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
podplacení	podplacení	k1gNnSc2	podplacení
dle	dle	k7c2	dle
§	§	k?	§
332	[number]	k4	332
tr	tr	k?	tr
<g/>
.	.	kIx.	.
zák	zák	k?	zák
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
uniklé	uniklý	k2eAgFnPc4d1	uniklá
nahrávky	nahrávka	k1gFnPc4	nahrávka
vydali	vydat	k5eAaPmAgMnP	vydat
redaktoři	redaktor	k1gMnPc1	redaktor
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
Dnes	dnes	k6eAd1	dnes
a	a	k8xC	a
iDnes	iDnes	k1gInSc1	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
společné	společný	k2eAgNnSc4d1	společné
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
označují	označovat	k5eAaImIp3nP	označovat
chování	chování	k1gNnSc4	chování
svého	svůj	k3xOyFgMnSc2	svůj
kolegy	kolega	k1gMnSc2	kolega
za	za	k7c4	za
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
exces	exces	k1gInSc4	exces
a	a	k8xC	a
požadují	požadovat	k5eAaImIp3nP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vedení	vedení	k1gNnSc1	vedení
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
podobným	podobný	k2eAgFnPc3d1	podobná
situacím	situace	k1gFnPc3	situace
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Redaktoři	redaktor	k1gMnPc1	redaktor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
podepsali	podepsat	k5eAaPmAgMnP	podepsat
pod	pod	k7c4	pod
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnutí	hnutí	k1gNnSc4	hnutí
ANO	ano	k9	ano
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
nemá	mít	k5eNaImIp3nS	mít
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
možnost	možnost	k1gFnSc4	možnost
skrytě	skrytě	k6eAd1	skrytě
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
nebo	nebo	k8xC	nebo
manipulovat	manipulovat	k5eAaImF	manipulovat
obsah	obsah	k1gInSc4	obsah
článků	článek	k1gInPc2	článek
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
frontě	fronta	k1gFnSc6	fronta
Dnes	dnes	k6eAd1	dnes
nebo	nebo	k8xC	nebo
na	na	k7c6	na
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
přes	přes	k7c4	přes
150	[number]	k4	150
redaktorů	redaktor	k1gMnPc2	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
novináře	novinář	k1gMnSc2	novinář
Jana	Jan	k1gMnSc2	Jan
Novotného	Novotný	k1gMnSc2	Novotný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
opustil	opustit	k5eAaPmAgMnS	opustit
redakci	redakce	k1gFnSc4	redakce
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
DNES	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Babiš	Babiš	k1gMnSc1	Babiš
volá	volat	k5eAaImIp3nS	volat
samotným	samotný	k2eAgMnPc3d1	samotný
novinářům	novinář	k1gMnPc3	novinář
<g/>
,	,	kIx,	,
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
jim	on	k3xPp3gMnPc3	on
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mohou	moct	k5eAaImIp3nP	moct
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Plesl	Plesl	k1gInSc4	Plesl
prý	prý	k9	prý
upravuje	upravovat	k5eAaImIp3nS	upravovat
texty	text	k1gInPc4	text
podle	podle	k7c2	podle
přání	přání	k1gNnSc2	přání
ministra	ministr	k1gMnSc4	ministr
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
bývalých	bývalý	k2eAgMnPc2d1	bývalý
novinářů	novinář	k1gMnPc2	novinář
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
DNES	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Biben	Bibna	k1gFnPc2	Bibna
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
výpovědi	výpověď	k1gFnSc6	výpověď
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
chybu	chyba	k1gFnSc4	chyba
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
Forum	forum	k1gNnSc1	forum
24	[number]	k4	24
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
analyzoval	analyzovat	k5eAaImAgInS	analyzovat
zapojení	zapojení	k1gNnSc4	zapojení
českých	český	k2eAgNnPc2d1	české
médií	médium	k1gNnPc2	médium
do	do	k7c2	do
medializace	medializace	k1gFnSc2	medializace
některých	některý	k3yIgFnPc2	některý
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
kauz	kauza	k1gFnPc2	kauza
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
vrcholných	vrcholný	k2eAgMnPc2d1	vrcholný
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
deník	deník	k1gInSc1	deník
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
kauzách	kauza	k1gFnPc6	kauza
"	"	kIx"	"
<g/>
dominantním	dominantní	k2eAgMnSc7d1	dominantní
aktérem	aktér	k1gMnSc7	aktér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
kauzami	kauza	k1gFnPc7	kauza
týkajícímí	týkajícímit	k5eAaPmIp3nP	týkajícímit
se	se	k3xPyFc4	se
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
tento	tento	k3xDgInSc1	tento
deník	deník	k1gInSc1	deník
"	"	kIx"	"
<g/>
spíše	spíše	k9	spíše
jen	jen	k6eAd1	jen
pasivním	pasivní	k2eAgMnSc7d1	pasivní
komentátorem	komentátor	k1gMnSc7	komentátor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
[	[	kIx(	[
<g/>
..	..	k?	..
<g/>
]	]	kIx)	]
více	hodně	k6eAd2	hodně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
[	[	kIx(	[
<g/>
jejich	jejich	k3xOp3gInSc1	jejich
<g/>
]	]	kIx)	]
bagatelizování	bagatelizování	k1gNnSc1	bagatelizování
a	a	k8xC	a
relativizování	relativizování	k1gNnSc1	relativizování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
středu	střed	k1gInSc6	střed
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
kauze	kauza	k1gFnSc3	kauza
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
opoziční	opoziční	k2eAgFnSc2d1	opoziční
ODS	ODS	kA	ODS
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
svolána	svolán	k2eAgFnSc1d1	svolána
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
schůze	schůze	k1gFnSc1	schůze
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
usnesla	usnést	k5eAaPmAgFnS	usnést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
"	"	kIx"	"
<g/>
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
veřejných	veřejný	k2eAgNnPc6d1	veřejné
vystoupeních	vystoupení	k1gNnPc6	vystoupení
opakovaně	opakovaně	k6eAd1	opakovaně
lhal	lhát	k5eAaImAgMnS	lhát
a	a	k8xC	a
zneužíval	zneužívat	k5eAaImAgMnS	zneužívat
svá	svůj	k3xOyFgNnPc4	svůj
média	médium	k1gNnPc4	médium
ke	k	k7c3	k
kompromitaci	kompromitace	k1gFnSc3	kompromitace
politických	politický	k2eAgMnPc2d1	politický
soupeřů	soupeř	k1gMnPc2	soupeř
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
2018	[number]	k4	2018
redakci	redakce	k1gFnSc3	redakce
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
již	již	k6eAd1	již
patřily	patřit	k5eAaImAgFnP	patřit
do	do	k7c2	do
svěřeneckého	svěřenecký	k2eAgInSc2d1	svěřenecký
fondu	fond	k1gInSc2	fond
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
<g/>
,	,	kIx,	,
opustila	opustit	k5eAaPmAgFnS	opustit
<g />
.	.	kIx.	.
</s>
<s>
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
redaktorka	redaktorka	k1gFnSc1	redaktorka
Petra	Petra	k1gFnSc1	Petra
Procházková	Procházková	k1gFnSc1	Procházková
kvůli	kvůli	k7c3	kvůli
textu	text	k1gInSc3	text
článku	článek	k1gInSc2	článek
o	o	k7c4	o
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
organizaci	organizace	k1gFnSc4	organizace
dětského	dětský	k2eAgInSc2d1	dětský
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podporoval	podporovat	k5eAaImAgInS	podporovat
stanovisko	stanovisko	k1gNnSc4	stanovisko
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
ohledně	ohledně	k7c2	ohledně
přijetí	přijetí	k1gNnSc2	přijetí
syrských	syrský	k2eAgMnPc2d1	syrský
válečných	válečný	k2eAgMnPc2d1	válečný
sirotků	sirotek	k1gMnPc2	sirotek
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc4	který
redakce	redakce	k1gFnSc1	redakce
obdržela	obdržet	k5eAaPmAgFnS	obdržet
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
asistentky	asistentka	k1gFnSc2	asistentka
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgInSc4	který
poslala	poslat	k5eAaPmAgFnS	poslat
údajná	údajný	k2eAgFnSc1d1	údajná
prezidentka	prezidentka	k1gFnSc1	prezidentka
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
organizace	organizace	k1gFnSc2	organizace
přímo	přímo	k6eAd1	přímo
Andreji	Andrej	k1gMnSc3	Andrej
Babišovi	Babiš	k1gMnSc3	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
redakci	redakce	k1gFnSc6	redakce
opustili	opustit	k5eAaPmAgMnP	opustit
i	i	k9	i
další	další	k2eAgMnPc1d1	další
redaktoři	redaktor	k1gMnPc1	redaktor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Martin	Martin	k1gMnSc1	Martin
C.	C.	kA	C.
Putna	putna	k1gFnSc1	putna
a	a	k8xC	a
Lukáš	Lukáš	k1gMnSc1	Lukáš
Rous	Rous	k1gMnSc1	Rous
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ovlivňování	ovlivňování	k1gNnPc1	ovlivňování
GIBS	GIBS	kA	GIBS
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
provedlo	provést	k5eAaPmAgNnS	provést
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
kontrolu	kontrola	k1gFnSc4	kontrola
GIBS	GIBS	kA	GIBS
(	(	kIx(	(
<g/>
Generální	generální	k2eAgFnSc2d1	generální
inspekce	inspekce	k1gFnSc2	inspekce
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
<g/>
)	)	kIx)	)
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
její	její	k3xOp3gMnSc1	její
šéf	šéf	k1gMnSc1	šéf
Michal	Michal	k1gMnSc1	Michal
Murín	Murín	k1gMnSc1	Murín
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
údajnou	údajný	k2eAgFnSc4d1	údajná
výzvu	výzva	k1gFnSc4	výzva
premiéra	premiér	k1gMnSc2	premiér
v	v	k7c6	v
demisi	demise	k1gFnSc6	demise
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
přitom	přitom	k6eAd1	přitom
obvykle	obvykle	k6eAd1	obvykle
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
NKÚ	NKÚ	kA	NKÚ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
iniciaci	iniciace	k1gFnSc3	iniciace
této	tento	k3xDgFnSc2	tento
kontroly	kontrola	k1gFnSc2	kontrola
se	se	k3xPyFc4	se
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
sněmovny	sněmovna	k1gFnSc2	sněmovna
přiznal	přiznat	k5eAaPmAgMnS	přiznat
sám	sám	k3xTgMnSc1	sám
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
magazínu	magazín	k1gInSc2	magazín
Neovlivní	ovlivnit	k5eNaPmIp3nS	ovlivnit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
obřího	obří	k2eAgInSc2d1	obří
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
ho	on	k3xPp3gNnSc2	on
vede	vést	k5eAaImIp3nS	vést
jako	jako	k9	jako
trestně	trestně	k6eAd1	trestně
stíhanou	stíhaný	k2eAgFnSc4d1	stíhaná
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
personálních	personální	k2eAgFnPc2d1	personální
změn	změna	k1gFnPc2	změna
by	by	kYmCp3nS	by
tak	tak	k9	tak
mohl	moct	k5eAaImAgInS	moct
oslabit	oslabit	k5eAaPmF	oslabit
vyšetřovatele	vyšetřovatel	k1gMnPc4	vyšetřovatel
své	svůj	k3xOyFgFnSc2	svůj
trestní	trestní	k2eAgFnSc2d1	trestní
kauzy	kauza	k1gFnSc2	kauza
Pavla	Pavel	k1gMnSc2	Pavel
Nevtípila	Nevtípila	k1gMnSc2	Nevtípila
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
magazínu	magazín	k1gInSc2	magazín
Neovlivní	ovlivnit	k5eNaPmIp3nS	ovlivnit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
realizuje	realizovat	k5eAaBmIp3nS	realizovat
na	na	k7c6	na
GIBS	GIBS	kA	GIBS
"	"	kIx"	"
<g/>
totalitní	totalitní	k2eAgFnSc4d1	totalitní
inspekční	inspekční	k2eAgFnSc4d1	inspekční
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
nepřípustná	přípustný	k2eNgFnSc1d1	nepřípustná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
v	v	k7c6	v
Podmínkách	podmínka	k1gFnPc6	podmínka
přistoupení	přistoupení	k1gNnSc2	přistoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
EU	EU	kA	EU
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
28	[number]	k4	28
–	–	k?	–
finanční	finanční	k2eAgFnSc1d1	finanční
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
takovou	takový	k3xDgFnSc4	takový
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
cílená	cílený	k2eAgFnSc1d1	cílená
<g/>
,	,	kIx,	,
neplánovaná	plánovaný	k2eNgFnSc1d1	neplánovaná
a	a	k8xC	a
provedená	provedený	k2eAgFnSc1d1	provedená
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
politický	politický	k2eAgInSc4d1	politický
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc1	znak
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
zákonná	zákonný	k2eAgFnSc1d1	zákonná
možnost	možnost	k1gFnSc1	možnost
odvolat	odvolat	k5eAaPmF	odvolat
Michala	Michal	k1gMnSc4	Michal
Murína	Murín	k1gMnSc4	Murín
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
trestně	trestně	k6eAd1	trestně
stíhán	stíhat	k5eAaImNgMnS	stíhat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
není	být	k5eNaImIp3nS	být
vedena	veden	k2eAgFnSc1d1	vedena
kárná	kárný	k2eAgFnSc1d1	kárná
žaloba	žaloba	k1gFnSc1	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
rozhodnut	rozhodnut	k2eAgMnSc1d1	rozhodnut
Murína	Murín	k1gMnSc2	Murín
odvolat	odvolat	k5eAaPmF	odvolat
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gMnSc4	on
postavit	postavit	k5eAaPmF	postavit
mimo	mimo	k7c4	mimo
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Zlámalová	Zlámalová	k1gFnSc1	Zlámalová
z	z	k7c2	z
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
policejní	policejní	k2eAgInSc4d1	policejní
puč	puč	k1gInSc4	puč
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Murín	Murín	k1gMnSc1	Murín
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
dopise	dopis	k1gInSc6	dopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevidí	vidět	k5eNaImIp3nS	vidět
pro	pro	k7c4	pro
odvolání	odvolání	k1gNnSc4	odvolání
jediný	jediný	k2eAgInSc1d1	jediný
závažný	závažný	k2eAgInSc1d1	závažný
důvod	důvod	k1gInSc1	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Stanovisko	stanovisko	k1gNnSc1	stanovisko
shodné	shodný	k2eAgNnSc1d1	shodné
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
zastává	zastávat	k5eAaImIp3nS	zastávat
naopak	naopak	k6eAd1	naopak
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
Pavel	Pavel	k1gMnSc1	Pavel
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
nepodal	podat	k5eNaPmAgMnS	podat
podnět	podnět	k1gInSc4	podnět
ke	k	k7c3	k
kárnému	kárný	k2eAgNnSc3d1	kárné
řízení	řízení	k1gNnSc3	řízení
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
cituje	citovat	k5eAaBmIp3nS	citovat
opatření	opatření	k1gNnSc1	opatření
Olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
předané	předaný	k2eAgFnSc2d1	předaná
premiérovi	premiér	k1gMnSc3	premiér
"	"	kIx"	"
<g/>
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
případného	případný	k2eAgInSc2d1	případný
kázeňského	kázeňský	k2eAgInSc2d1	kázeňský
přestupku	přestupek	k1gInSc2	přestupek
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
<g/>
)	)	kIx)	)
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
"	"	kIx"	"
<g/>
chování	chování	k1gNnSc6	chování
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
prověřováním	prověřování	k1gNnSc7	prověřování
úniků	únik	k1gInPc2	únik
informací	informace	k1gFnPc2	informace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
právní	právní	k2eAgFnSc2d1	právní
analýzy	analýza	k1gFnSc2	analýza
dopisu	dopis	k1gInSc2	dopis
<g/>
,	,	kIx,	,
ze	z	k7c2	z
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
Pavel	Pavel	k1gMnSc1	Pavel
Zeman	Zeman	k1gMnSc1	Zeman
Michalovi	Michal	k1gMnSc3	Michal
Murínovi	Murín	k1gMnSc3	Murín
vytýká	vytýkat	k5eAaImIp3nS	vytýkat
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc4	žádný
pochybení	pochybení	k1gNnSc4	pochybení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
platných	platný	k2eAgInPc2d1	platný
zákonů	zákon	k1gInPc2	zákon
nevyplývá	vyplývat	k5eNaImIp3nS	vyplývat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
však	však	k9	však
Michal	Michal	k1gMnSc1	Michal
Murín	Murín	k1gMnSc1	Murín
avizoval	avizovat	k5eAaBmAgMnS	avizovat
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
GIBS	GIBS	kA	GIBS
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
podle	podle	k7c2	podle
zjištění	zjištění	k1gNnSc2	zjištění
Neovlivní	ovlivnit	k5eNaPmIp3nS	ovlivnit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
oznámili	oznámit	k5eAaPmAgMnP	oznámit
záměr	záměr	k1gInSc4	záměr
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
také	také	k9	také
policejní	policejní	k2eAgMnSc1d1	policejní
prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Tuhý	tuhý	k2eAgMnSc1d1	tuhý
<g/>
,	,	kIx,	,
komisař	komisař	k1gMnSc1	komisař
Pavel	Pavel	k1gMnSc1	Pavel
Nevtípil	Nevtípil	k1gMnSc1	Nevtípil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
Babišovu	Babišův	k2eAgFnSc4d1	Babišova
kauzu	kauza	k1gFnSc4	kauza
Čapí	čapět	k5eAaImIp3nS	čapět
hnízdo	hnízdo	k1gNnSc1	hnízdo
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
šéf	šéf	k1gMnSc1	šéf
Miloš	Miloš	k1gMnSc1	Miloš
Trojánek	Trojánek	k1gMnSc1	Trojánek
<g/>
.	.	kIx.	.
<g/>
Výběr	výběr	k1gInSc1	výběr
Murínova	Murínův	k2eAgMnSc2d1	Murínův
nástupce	nástupce	k1gMnSc2	nástupce
Radima	Radim	k1gMnSc2	Radim
Dragouna	dragoun	k1gMnSc2	dragoun
považují	považovat	k5eAaImIp3nP	považovat
opoziční	opoziční	k2eAgFnPc1d1	opoziční
strany	strana	k1gFnPc1	strana
za	za	k7c4	za
netransparentní	transparentní	k2eNgNnSc4d1	netransparentní
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
Pirátů	pirát	k1gMnPc2	pirát
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gMnPc2	KDU-ČSL
projevili	projevit	k5eAaPmAgMnP	projevit
výhrady	výhrada	k1gFnPc4	výhrada
k	k	k7c3	k
výběru	výběr	k1gInSc3	výběr
členů	člen	k1gMnPc2	člen
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Dragouna	dragoun	k1gMnSc2	dragoun
zvolila	zvolit	k5eAaPmAgFnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
komisi	komise	k1gFnSc4	komise
jako	jako	k8xS	jako
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
<g/>
,	,	kIx,	,
Forum	forum	k1gNnSc1	forum
<g/>
24	[number]	k4	24
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gFnSc1	jeho
(	(	kIx(	(
<g/>
údajné	údajný	k2eAgFnPc1d1	údajná
<g/>
)	)	kIx)	)
vazby	vazba	k1gFnPc1	vazba
ke	k	k7c3	k
členům	člen	k1gMnPc3	člen
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Společnost	společnost	k1gFnSc1	společnost
IMOBA	IMOBA	kA	IMOBA
===	===	k?	===
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
Julius	Julius	k1gMnSc1	Julius
Šuman	Šuman	k1gMnSc1	Šuman
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
analýzu	analýza	k1gFnSc4	analýza
podnikatelských	podnikatelský	k2eAgInPc2d1	podnikatelský
vztahů	vztah	k1gInPc2	vztah
okolo	okolo	k7c2	okolo
společnosti	společnost	k1gFnSc2	společnost
IMOBA	IMOBA	kA	IMOBA
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
transakcemi	transakce	k1gFnPc7	transakce
kolem	kolem	k7c2	kolem
společnosti	společnost	k1gFnSc2	společnost
si	se	k3xPyFc3	se
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
přes	přes	k7c4	přes
daňové	daňový	k2eAgInPc4d1	daňový
ráje	ráj	k1gInPc4	ráj
a	a	k8xC	a
firmy	firma	k1gFnPc4	firma
PLASTAGRA	PLASTAGRA	kA	PLASTAGRA
<g/>
,	,	kIx,	,
FERTAGRA	FERTAGRA	kA	FERTAGRA
a	a	k8xC	a
Synbiol	Synbiol	k1gInSc1	Synbiol
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
financí	finance	k1gFnPc2	finance
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yRnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jako	jako	k9	jako
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
dani	daň	k1gFnSc3	daň
z	z	k7c2	z
dividend	dividenda	k1gFnPc2	dividenda
a	a	k8xC	a
stát	stát	k1gInSc1	stát
tak	tak	k6eAd1	tak
připravil	připravit	k5eAaPmAgInS	připravit
na	na	k7c6	na
daních	daň	k1gFnPc6	daň
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
s	s	k7c7	s
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
koupila	koupit	k5eAaPmAgFnS	koupit
Babišova	Babišův	k2eAgFnSc1d1	Babišova
skupina	skupina	k1gFnSc1	skupina
Agrofert	Agrofert	k1gInSc1	Agrofert
mediální	mediální	k2eAgFnSc4d1	mediální
skupinu	skupina	k1gFnSc4	skupina
MAFRA	MAFRA	kA	MAFRA
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
kterou	který	k3yIgFnSc4	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
celostátní	celostátní	k2eAgInSc1d1	celostátní
deník	deník	k1gInSc1	deník
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
poté	poté	k6eAd1	poté
odešlo	odejít	k5eAaPmAgNnS	odejít
několik	několik	k4yIc1	několik
redaktorů	redaktor	k1gMnPc2	redaktor
včetně	včetně	k7c2	včetně
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
Dalibora	Dalibor	k1gMnSc2	Dalibor
Balšínka	Balšínek	k1gMnSc2	Balšínek
<g/>
.	.	kIx.	.
</s>
<s>
Balšínek	Balšínek	k1gMnSc1	Balšínek
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
bývalými	bývalý	k2eAgMnPc7d1	bývalý
redaktory	redaktor	k1gMnPc7	redaktor
pak	pak	k6eAd1	pak
založili	založit	k5eAaPmAgMnP	založit
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
server	server	k1gInSc4	server
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
a	a	k8xC	a
vytkli	vytknout	k5eAaPmAgMnP	vytknout
si	se	k3xPyFc3	se
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
protiváhou	protiváha	k1gFnSc7	protiváha
oligarchizovaným	oligarchizovaný	k2eAgNnPc3d1	oligarchizovaný
českým	český	k2eAgNnPc3d1	české
médiím	médium	k1gNnPc3	médium
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
server	server	k1gInSc1	server
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
rozhovor	rozhovor	k1gInSc4	rozhovor
redaktora	redaktor	k1gMnSc2	redaktor
Daniela	Daniel	k1gMnSc2	Daniel
Kaisera	Kaiser	k1gMnSc2	Kaiser
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Válkovou	Válková	k1gFnSc7	Válková
<g/>
,	,	kIx,	,
ministryní	ministryně	k1gFnSc7	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
za	za	k7c4	za
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Babišova	Babišův	k2eAgNnSc2d1	Babišovo
zbohatnutí	zbohatnutí	k1gNnSc2	zbohatnutí
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
údajné	údajný	k2eAgFnSc2d1	údajná
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
a	a	k8xC	a
možného	možný	k2eAgNnSc2d1	možné
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
obsahu	obsah	k1gInSc2	obsah
novin	novina	k1gFnPc2	novina
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
tématem	téma	k1gNnSc7	téma
rozhovoru	rozhovor	k1gInSc2	rozhovor
bylo	být	k5eAaImAgNnS	být
postavení	postavení	k1gNnSc1	postavení
Čechů	Čech	k1gMnPc2	Čech
za	za	k7c2	za
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
označil	označit	k5eAaPmAgInS	označit
Echo	echo	k1gNnSc4	echo
<g/>
24	[number]	k4	24
za	za	k7c7	za
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
napadat	napadat	k5eAaImF	napadat
jeho	jeho	k3xOp3gNnSc2	jeho
samotného	samotný	k2eAgNnSc2d1	samotné
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
redaktory	redaktor	k1gMnPc4	redaktor
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
za	za	k7c4	za
"	"	kIx"	"
<g/>
tuneláře	tunelář	k1gMnPc4	tunelář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vytunelovali	vytunelovat	k5eAaPmAgMnP	vytunelovat
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
udělali	udělat	k5eAaPmAgMnP	udělat
tam	tam	k6eAd1	tam
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
ztrátu	ztráta	k1gFnSc4	ztráta
a	a	k8xC	a
fandili	fandit	k5eAaImAgMnP	fandit
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Za	za	k7c7	za
celou	celý	k2eAgFnSc7d1	celá
věcí	věc	k1gFnSc7	věc
měli	mít	k5eAaImAgMnP	mít
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
stát	stát	k5eAaImF	stát
místopředseda	místopředseda	k1gMnSc1	místopředseda
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
ČEZ	ČEZ	kA	ČEZ
Martin	Martin	k1gMnSc1	Martin
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
dále	daleko	k6eAd2	daleko
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
váš	váš	k3xOp2gMnSc1	váš
bílý	bílý	k1gMnSc1	bílý
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
Klenor	Klenor	k1gMnSc1	Klenor
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgNnSc4d1	velké
majetkové	majetkový	k2eAgNnSc4d1	majetkové
přiznání	přiznání	k1gNnSc4	přiznání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prokázal	prokázat	k5eAaPmAgInS	prokázat
potom	potom	k6eAd1	potom
ty	ten	k3xDgInPc4	ten
vaše	váš	k3xOp2gInPc4	váš
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Adama	Adam	k1gMnSc2	Adam
Drdy	Drda	k1gMnSc2	Drda
z	z	k7c2	z
Revolver	revolver	k1gInSc4	revolver
Revue	revue	k1gFnSc4	revue
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc4	Jiří
X.	X.	kA	X.
Doležala	Doležal	k1gMnSc4	Doležal
z	z	k7c2	z
Reflexu	reflex	k1gInSc2	reflex
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
politika	politik	k1gMnSc2	politik
Miroslava	Miroslav	k1gMnSc2	Miroslav
Macka	Macek	k1gMnSc2	Macek
nebo	nebo	k8xC	nebo
serveru	server	k1gInSc2	server
Mediaguru	Mediagur	k1gInSc2	Mediagur
tím	ten	k3xDgNnSc7	ten
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
investorovi	investor	k1gMnSc3	investor
deníku	deník	k1gInSc2	deník
Janu	Jan	k1gMnSc3	Jan
Klenorovi	Klenor	k1gMnSc3	Klenor
finanční	finanční	k2eAgFnSc7d1	finanční
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Drdy	Drda	k1gMnSc2	Drda
tím	ten	k3xDgNnSc7	ten
navíc	navíc	k6eAd1	navíc
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
potenciálním	potenciální	k2eAgMnSc7d1	potenciální
konkurentům	konkurent	k1gMnPc3	konkurent
svých	svůj	k3xOyFgNnPc2	svůj
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Balšínek	Balšínek	k1gMnSc1	Balšínek
všechna	všechen	k3xTgNnPc4	všechen
Babišova	Babišův	k2eAgNnPc4d1	Babišovo
nařčení	nařčení	k1gNnPc4	nařčení
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k5eAaPmIp2nS	Babiš
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
slova	slovo	k1gNnPc4	slovo
omluvil	omluvit	k5eAaPmAgMnS	omluvit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
prověřovat	prověřovat	k5eAaImF	prověřovat
hospodaření	hospodaření	k1gNnSc4	hospodaření
zpravodajského	zpravodajský	k2eAgInSc2d1	zpravodajský
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
problém	problém	k1gInSc4	problém
si	se	k3xPyFc3	se
zvyknout	zvyknout	k5eAaPmF	zvyknout
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
politik	politik	k1gMnSc1	politik
nemůže	moct	k5eNaImIp3nS	moct
říkat	říkat	k5eAaImF	říkat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
doopravdy	doopravdy	k6eAd1	doopravdy
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
skutečně	skutečně	k6eAd1	skutečně
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
kontrola	kontrola	k1gFnSc1	kontrola
z	z	k7c2	z
finančního	finanční	k2eAgInSc2d1	finanční
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
redakce	redakce	k1gFnSc2	redakce
nebyly	být	k5eNaImAgInP	být
při	při	k7c6	při
kontrole	kontrola	k1gFnSc6	kontrola
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
žádné	žádný	k3yNgFnPc1	žádný
nesrovnalosti	nesrovnalost	k1gFnPc1	nesrovnalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
ÚOOZ	ÚOOZ	kA	ÚOOZ
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
investigativního	investigativní	k2eAgMnSc2d1	investigativní
novináře	novinář	k1gMnSc2	novinář
Janka	Janek	k1gMnSc2	Janek
Kroupy	Kroupa	k1gMnSc2	Kroupa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Babiš	Babiš	k1gMnSc1	Babiš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
informátorem	informátor	k1gMnSc7	informátor
Útvaru	útvar	k1gInSc2	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
(	(	kIx(	(
<g/>
ÚOOZ	ÚOOZ	kA	ÚOOZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
Babiše	Babiš	k1gMnSc4	Babiš
útvar	útvar	k1gInSc1	útvar
sledoval	sledovat	k5eAaImAgInS	sledovat
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zapleten	zaplést	k5eAaPmNgInS	zaplést
do	do	k7c2	do
vraždy	vražda	k1gFnSc2	vražda
Františka	František	k1gMnSc2	František
Mrázka	Mrázek	k1gMnSc2	Mrázek
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgInPc1d1	důležitý
postavy	postav	k1gInPc1	postav
českého	český	k2eAgNnSc2d1	české
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
podezření	podezření	k1gNnSc4	podezření
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepotvrdilo	potvrdit	k5eNaPmAgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
poté	poté	k6eAd1	poté
nabízel	nabízet	k5eAaImAgMnS	nabízet
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
detektivů	detektiv	k1gMnPc2	detektiv
buď	buď	k8xC	buď
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kariérní	kariérní	k2eAgInSc1d1	kariérní
růst	růst	k1gInSc1	růst
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
informátorem	informátor	k1gMnSc7	informátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
reorganizaci	reorganizace	k1gFnSc3	reorganizace
policejních	policejní	k2eAgInPc2d1	policejní
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgInS	být
ÚOOZ	ÚOOZ	kA	ÚOOZ
sloučen	sloučit	k5eAaPmNgInS	sloučit
s	s	k7c7	s
Útvarem	útvar	k1gInSc7	útvar
odhalování	odhalování	k1gNnSc1	odhalování
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
finanční	finanční	k2eAgFnSc2d1	finanční
kriminality	kriminalita	k1gFnSc2	kriminalita
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
centrály	centrála	k1gFnSc2	centrála
proti	proti	k7c3	proti
organizovanému	organizovaný	k2eAgInSc3d1	organizovaný
zločinu	zločin	k1gInSc3	zločin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výroky	výrok	k1gInPc1	výrok
o	o	k7c6	o
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Letech	let	k1gInPc6	let
===	===	k?	===
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
2016	[number]	k4	2016
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
Babišovy	Babišův	k2eAgInPc1d1	Babišův
výroky	výrok	k1gInPc1	výrok
o	o	k7c6	o
romském	romský	k2eAgInSc6d1	romský
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnSc2	jeho
vyjádření	vyjádření	k1gNnSc2	vyjádření
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Varnsdorfu	Varnsdorf	k1gInSc2	Varnsdorf
novináři	novinář	k1gMnPc1	novinář
o	o	k7c6	o
táboru	tábor	k1gInSc6	tábor
lžou	lhát	k5eAaImIp3nP	lhát
<g/>
,	,	kIx,	,
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
koncentrační	koncentrační	k2eAgNnSc4d1	koncentrační
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pracovní	pracovní	k2eAgInSc1d1	pracovní
tábor	tábor	k1gInSc1	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
pracujících	pracující	k2eAgMnPc2d1	pracující
Romů	Rom	k1gMnPc2	Rom
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byly	být	k5eAaImAgFnP	být
doby	doba	k1gFnPc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všichni	všechen	k3xTgMnPc1	všechen
Romové	Rom	k1gMnPc1	Rom
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
píší	psát	k5eAaImIp3nP	psát
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
blbečci	blbeček	k1gMnPc1	blbeček
<g/>
,	,	kIx,	,
že	že	k8xS	že
tábor	tábor	k1gInSc1	tábor
v	v	k7c6	v
Letech	let	k1gInPc6	let
byl	být	k5eAaImAgInS	být
koncentrák	koncentrák	k1gInSc1	koncentrák
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
lež	lež	k1gFnSc4	lež
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
pracovní	pracovní	k2eAgInSc1d1	pracovní
tábor	tábor	k1gInSc1	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
nepracoval	pracovat	k5eNaImAgMnS	pracovat
<g/>
,	,	kIx,	,
šup	šup	k1gInSc1	šup
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
ostré	ostrý	k2eAgFnSc6d1	ostrá
kritice	kritika	k1gFnSc6	kritika
jiných	jiný	k2eAgMnPc2d1	jiný
vrcholných	vrcholný	k2eAgMnPc2d1	vrcholný
politiků	politik	k1gMnPc2	politik
zprvu	zprvu	k6eAd1	zprvu
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
citoval	citovat	k5eAaBmAgMnS	citovat
slova	slovo	k1gNnSc2	slovo
svého	svůj	k3xOyFgMnSc2	svůj
známého	známý	k1gMnSc2	známý
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
výrok	výrok	k1gInSc1	výrok
vytržený	vytržený	k2eAgInSc1d1	vytržený
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
za	za	k7c4	za
výrok	výrok	k1gInSc4	výrok
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Dodatečně	dodatečně	k6eAd1	dodatečně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
pietní	pietní	k2eAgNnSc4d1	pietní
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Letech	let	k1gInPc6	let
osobně	osobně	k6eAd1	osobně
zavítá	zavítat	k5eAaPmIp3nS	zavítat
a	a	k8xC	a
sežene	sehnat	k5eAaPmIp3nS	sehnat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
výkup	výkup	k1gInSc4	výkup
vepřína	vepřín	k1gInSc2	vepřín
a	a	k8xC	a
výstavbu	výstavba	k1gFnSc4	výstavba
památníku	památník	k1gInSc2	památník
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvu	návštěva	k1gFnSc4	návštěva
podnikl	podniknout	k5eAaPmAgMnS	podniknout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
kultury	kultura	k1gFnSc2	kultura
Danielem	Daniel	k1gMnSc7	Daniel
Hermanem	Herman	k1gMnSc7	Herman
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Robertem	Robert	k1gMnSc7	Robert
Pelikánem	Pelikán	k1gMnSc7	Pelikán
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Demonstrace	demonstrace	k1gFnSc2	demonstrace
a	a	k8xC	a
občanské	občanský	k2eAgFnSc2d1	občanská
výzvy	výzva	k1gFnSc2	výzva
proti	proti	k7c3	proti
Andreji	Andrej	k1gMnSc3	Andrej
Babišovi	Babiš	k1gMnSc3	Babiš
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
sedmi	sedm	k4xCc2	sedm
krajských	krajský	k2eAgNnPc2d1	krajské
měst	město	k1gNnPc2	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
konaly	konat	k5eAaImAgFnP	konat
demonstrace	demonstrace	k1gFnPc1	demonstrace
za	za	k7c4	za
Babišovo	Babišův	k2eAgNnSc4d1	Babišovo
odstoupení	odstoupení	k1gNnSc4	odstoupení
z	z	k7c2	z
vládních	vládní	k2eAgFnPc2d1	vládní
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
Zemanovo	Zemanův	k2eAgNnSc4d1	Zemanovo
odstoupení	odstoupení	k1gNnSc4	odstoupení
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
Babišovy	Babišův	k2eAgInPc1d1	Babišův
"	"	kIx"	"
<g/>
oligarchické	oligarchický	k2eAgInPc1d1	oligarchický
manýry	manýry	k1gInPc1	manýry
<g/>
"	"	kIx"	"
a	a	k8xC	a
Zemanovo	Zemanův	k2eAgNnSc4d1	Zemanovo
"	"	kIx"	"
<g/>
nepojmenovatelné	pojmenovatelný	k2eNgNnSc4d1	nepojmenovatelné
chování	chování	k1gNnSc4	chování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Organizátorkou	organizátorka	k1gFnSc7	organizátorka
pražské	pražský	k2eAgFnSc2d1	Pražská
demonstrace	demonstrace	k1gFnSc2	demonstrace
byla	být	k5eAaImAgFnS	být
občanka	občanka	k1gFnSc1	občanka
Šárka	Šárka	k1gFnSc1	Šárka
Fialová	Fialová	k1gFnSc1	Fialová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
demonstranti	demonstrant	k1gMnPc1	demonstrant
zaplnili	zaplnit	k5eAaPmAgMnP	zaplnit
horní	horní	k2eAgFnSc4d1	horní
polovinu	polovina	k1gFnSc4	polovina
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
organizátorů	organizátor	k1gMnPc2	organizátor
přišlo	přijít	k5eAaPmAgNnS	přijít
20	[number]	k4	20
až	až	k8xS	až
30	[number]	k4	30
tisíc	tisíc	k4xCgInSc4	tisíc
protestujících	protestující	k2eAgInPc2d1	protestující
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
odhadem	odhad	k1gInSc7	odhad
8	[number]	k4	8
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Operátor	operátor	k1gInSc1	operátor
T-Mobile	T-Mobila	k1gFnSc3	T-Mobila
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
počet	počet	k1gInSc1	počet
demonstrujících	demonstrující	k2eAgMnPc2d1	demonstrující
na	na	k7c4	na
19	[number]	k4	19
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Zlíně	Zlín	k1gInSc6	Zlín
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
stovky	stovka	k1gFnPc1	stovka
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Babiš	Babiš	k1gInSc1	Babiš
počet	počet	k1gInSc4	počet
demonstrujících	demonstrující	k2eAgMnPc2d1	demonstrující
bagatelizoval	bagatelizovat	k5eAaImAgInS	bagatelizovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
pohledu	pohled	k1gInSc2	pohled
to	ten	k3xDgNnSc1	ten
nebyla	být	k5eNaImAgFnS	být
až	až	k9	až
taková	takový	k3xDgFnSc1	takový
velká	velká	k1gFnSc1	velká
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Václaváku	Václavák	k1gInSc6	Václavák
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc1	ten
mám	mít	k5eAaImIp1nS	mít
tolik	tolik	k4xDc1	tolik
lajků	lajk	k1gInPc2	lajk
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
výroku	výrok	k1gInSc3	výrok
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
předala	předat	k5eAaPmAgFnS	předat
skupina	skupina	k1gFnSc1	skupina
studentů	student	k1gMnPc2	student
okolo	okolo	k7c2	okolo
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Mináře	Minář	k1gMnSc2	Minář
osobně	osobně	k6eAd1	osobně
Andreji	Andrej	k1gMnSc3	Andrej
Babišovi	Babiš	k1gMnSc3	Babiš
výzvu	výzva	k1gFnSc4	výzva
"	"	kIx"	"
<g/>
Chvilka	chvilka	k1gFnSc1	chvilka
pro	pro	k7c4	pro
Andreje	Andrej	k1gMnSc4	Andrej
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jej	on	k3xPp3gMnSc4	on
žádali	žádat	k5eAaImAgMnP	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
ANO	ano	k9	ano
2011	[number]	k4	2011
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vedení	vedení	k1gNnSc2	vedení
vlády	vláda	k1gFnSc2	vláda
dodržoval	dodržovat	k5eAaImAgMnS	dodržovat
své	svůj	k3xOyFgInPc4	svůj
sliby	slib	k1gInPc4	slib
o	o	k7c4	o
zachovávání	zachovávání	k1gNnSc4	zachovávání
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Výzva	výzva	k1gFnSc1	výzva
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
deset	deset	k4xCc4	deset
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
odkazovaly	odkazovat	k5eAaImAgInP	odkazovat
na	na	k7c4	na
Babišovu	Babišův	k2eAgFnSc4d1	Babišova
předvolební	předvolební	k2eAgFnSc4d1	předvolební
"	"	kIx"	"
<g/>
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
občany	občan	k1gMnPc7	občan
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
žádali	žádat	k5eAaImAgMnP	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Babiš	Babiš	k1gMnSc1	Babiš
nechal	nechat	k5eAaPmAgMnS	nechat
vydat	vydat	k5eAaPmF	vydat
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prodal	prodat	k5eAaPmAgMnS	prodat
svá	svůj	k3xOyFgNnPc4	svůj
média	médium	k1gNnPc4	médium
a	a	k8xC	a
stát	stát	k1gInSc4	stát
neřídil	řídit	k5eNaImAgMnS	řídit
jako	jako	k8xC	jako
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
získala	získat	k5eAaPmAgFnS	získat
výzva	výzva	k1gFnSc1	výzva
přes	přes	k7c4	přes
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
signatářů	signatář	k1gMnPc2	signatář
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
bývalých	bývalý	k2eAgMnPc2d1	bývalý
chartistů	chartista	k1gMnPc2	chartista
i	i	k8xC	i
běžných	běžný	k2eAgMnPc2d1	běžný
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
projevil	projevit	k5eAaPmAgMnS	projevit
zájem	zájem	k1gInSc4	zájem
se	se	k3xPyFc4	se
s	s	k7c7	s
iniciátory	iniciátor	k1gInPc7	iniciátor
výzvy	výzva	k1gFnSc2	výzva
setkat	setkat	k5eAaPmF	setkat
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
však	však	k9	však
následně	následně	k6eAd1	následně
vyjednávali	vyjednávat	k5eAaImAgMnP	vyjednávat
podmínky	podmínka	k1gFnPc4	podmínka
takového	takový	k3xDgNnSc2	takový
setkání	setkání	k1gNnSc6	setkání
–	–	k?	–
nechtěli	chtít	k5eNaImAgMnP	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
totiž	totiž	k9	totiž
setkat	setkat	k5eAaPmF	setkat
za	za	k7c7	za
zavřenými	zavřený	k2eAgFnPc7d1	zavřená
dveřmi	dveře	k1gFnPc7	dveře
v	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
veřejně	veřejně	k6eAd1	veřejně
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
komunistického	komunistický	k2eAgInSc2d1	komunistický
puče	puč	k1gInSc2	puč
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
spustila	spustit	k5eAaPmAgFnS	spustit
tatáž	týž	k3xTgFnSc1	týž
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
spolku	spolek	k1gInSc2	spolek
Milion	milion	k4xCgInSc1	milion
Chvilek	chvilka	k1gFnPc2	chvilka
<g/>
,	,	kIx,	,
z.	z.	k?	z.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
kampaň	kampaň	k1gFnSc1	kampaň
Milion	milion	k4xCgInSc4	milion
chvilek	chvilka	k1gFnPc2	chvilka
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
projektem	projekt	k1gInSc7	projekt
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
výzva	výzva	k1gFnSc1	výzva
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Chvilka	chvilka	k1gFnSc1	chvilka
pro	pro	k7c4	pro
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiše	Babiš	k1gInSc2	Babiš
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
trestně	trestně	k6eAd1	trestně
stíhaného	stíhaný	k2eAgMnSc4d1	stíhaný
člověka	člověk	k1gMnSc4	člověk
vedeného	vedený	k2eAgMnSc4d1	vedený
jako	jako	k8xC	jako
agent	agent	k1gMnSc1	agent
StB	StB	k1gFnSc2	StB
a	a	k8xC	a
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
ko	ko	k?	ko
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
politických	politický	k2eAgFnPc2d1	politická
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgInSc7d1	původní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
sto	sto	k4xCgNnSc4	sto
dní	den	k1gInPc2	den
nasbírat	nasbírat	k5eAaPmF	nasbírat
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
podpisů	podpis	k1gInPc2	podpis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
výzvu	výzev	k1gInSc2	výzev
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
254	[number]	k4	254
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
společností	společnost	k1gFnSc7	společnost
včetně	včetně	k7c2	včetně
řady	řada	k1gFnSc2	řada
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
sběr	sběr	k1gInSc1	sběr
podpisů	podpis	k1gInPc2	podpis
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
signatářů	signatář	k1gMnPc2	signatář
přes	přes	k7c4	přes
420	[number]	k4	420
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnPc1d1	další
tři	tři	k4xCgFnPc4	tři
demonstrace	demonstrace	k1gFnPc4	demonstrace
svolala	svolat	k5eAaPmAgFnS	svolat
iniciativa	iniciativa	k1gFnSc1	iniciativa
Milion	milion	k4xCgInSc4	milion
chvilek	chvilka	k1gFnPc2	chvilka
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
pietní	pietní	k2eAgFnSc6d1	pietní
připomínce	připomínka	k1gFnSc6	připomínka
událostí	událost	k1gFnPc2	událost
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
před	před	k7c7	před
českým	český	k2eAgInSc7d1	český
rozhlasem	rozhlas	k1gInSc7	rozhlas
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
vypískán	vypískán	k2eAgMnSc1d1	vypískán
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pětisetčlenným	pětisetčlenný	k2eAgInSc7d1	pětisetčlenný
skandujícím	skandující	k2eAgInSc7d1	skandující
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
i	i	k8xC	i
projev	projev	k1gInSc4	projev
šéfa	šéf	k1gMnSc2	šéf
sněmovny	sněmovna	k1gFnSc2	sněmovna
Radka	Radek	k1gMnSc2	Radek
Vondráčka	Vondráček	k1gMnSc2	Vondráček
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přehlušen	přehlušit	k5eAaPmNgInS	přehlušit
pískotem	pískot	k1gInSc7	pískot
<g/>
.15	.15	k4	.15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
</s>
<s>
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
i	i	k8xC	i
nejasností	nejasnost	k1gFnPc2	nejasnost
okolo	okolo	k7c2	okolo
únosu	únos	k1gInSc2	únos
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
konala	konat	k5eAaImAgFnS	konat
demonstrace	demonstrace	k1gFnSc1	demonstrace
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
aktivistickou	aktivistický	k2eAgFnSc7d1	aktivistická
skupinou	skupina	k1gFnSc7	skupina
AUVA	AUVA	kA	AUVA
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
až	až	k6eAd1	až
5000	[number]	k4	5000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
volali	volat	k5eAaImAgMnP	volat
po	po	k7c6	po
demisi	demise	k1gFnSc6	demise
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiše	k1gFnSc2	Babiše
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgNnSc4d1	další
protestní	protestní	k2eAgNnSc4d1	protestní
shromáždění	shromáždění	k1gNnSc4	shromáždění
požadující	požadující	k2eAgFnSc4d1	požadující
demisi	demise	k1gFnSc4	demise
premiéra	premiér	k1gMnSc2	premiér
Babiše	Babiš	k1gMnSc2	Babiš
již	již	k6eAd1	již
probíhala	probíhat	k5eAaImAgFnS	probíhat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
spolku	spolek	k1gInSc2	spolek
Milion	milion	k4xCgInSc1	milion
chvilek	chvilka	k1gFnPc2	chvilka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2018	[number]	k4	2018
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
demonstracích	demonstrace	k1gFnPc6	demonstrace
mj.	mj.	kA	mj.
i	i	k9	i
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
čerstvě	čerstvě	k6eAd1	čerstvě
prezentovanými	prezentovaný	k2eAgFnPc7d1	prezentovaná
zprávami	zpráva	k1gFnPc7	zpráva
o	o	k7c6	o
údajném	údajný	k2eAgInSc6d1	údajný
únosu	únos	k1gInSc6	únos
premiérova	premiérův	k2eAgMnSc2d1	premiérův
syna	syn	k1gMnSc2	syn
na	na	k7c6	na
Ruskem	Rusko	k1gNnSc7	Rusko
okupovaný	okupovaný	k2eAgInSc1d1	okupovaný
poloostrov	poloostrov	k1gInSc1	poloostrov
Krym	Krym	k1gInSc1	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
na	na	k7c6	na
postu	post	k1gInSc6	post
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kontroverzní	kontroverzní	k2eAgFnSc6d1	kontroverzní
situaci	situace	k1gFnSc6	situace
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
Jana	Jan	k1gMnSc2	Jan
Kněžínka	Kněžínek	k1gMnSc2	Kněžínek
dosazena	dosazen	k2eAgFnSc1d1	dosazena
Marie	Marie	k1gFnSc1	Marie
Benešová	Benešová	k1gFnSc1	Benešová
<g/>
,	,	kIx,	,
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
sérii	série	k1gFnSc4	série
demonstrací	demonstrace	k1gFnPc2	demonstrace
konajících	konající	k2eAgFnPc2d1	konající
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
protestů	protest	k1gInPc2	protest
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
před	před	k7c7	před
letními	letní	k2eAgFnPc7d1	letní
prázdninami	prázdniny	k1gFnPc7	prázdniny
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
demonstrací	demonstrace	k1gFnPc2	demonstrace
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
nás	my	k3xPp1nPc4	my
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
konanou	konaný	k2eAgFnSc4d1	konaná
na	na	k7c6	na
Letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
největší	veliký	k2eAgFnSc7d3	veliký
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
historii	historie	k1gFnSc6	historie
od	od	k7c2	od
revolučních	revoluční	k2eAgFnPc2d1	revoluční
shromáždění	shromáždění	k1gNnSc4	shromáždění
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
velkou	velká	k1gFnSc7	velká
demonstrace	demonstrace	k1gFnPc4	demonstrace
proti	proti	k7c3	proti
Andreji	Andrej	k1gMnSc3	Andrej
Babišovi	Babiš	k1gMnSc3	Babiš
svolal	svolat	k5eAaPmAgMnS	svolat
spolek	spolek	k1gInSc4	spolek
Milion	milion	k4xCgInSc4	milion
Chvilek	chvilka	k1gFnPc2	chvilka
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2019	[number]	k4	2019
opět	opět	k6eAd1	opět
na	na	k7c4	na
Letnou	Letná	k1gFnSc4	Letná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zelená	zelený	k2eAgFnSc1d1	zelená
perla	perla	k1gFnSc1	perla
2013	[number]	k4	2013
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
obdržel	obdržet	k5eAaPmAgMnS	obdržet
anticenu	anticen	k2eAgFnSc4d1	anticena
Zelená	zelený	k2eAgFnSc1d1	zelená
perla	perla	k1gFnSc1	perla
2013	[number]	k4	2013
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výrok	výrok	k1gInSc4	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
asi	asi	k9	asi
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
demokracie	demokracie	k1gFnSc2	demokracie
poděláme	podělat	k5eAaPmIp1nP	podělat
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
jinak	jinak	k6eAd1	jinak
totiž	totiž	k9	totiž
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
k	k	k7c3	k
dálnici	dálnice	k1gFnSc3	dálnice
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
i	i	k9	i
jezevec	jezevec	k1gMnSc1	jezevec
<g/>
,	,	kIx,	,
ekologický	ekologický	k2eAgMnSc1d1	ekologický
terorista	terorista	k1gMnSc1	terorista
a	a	k8xC	a
taky	taky	k6eAd1	taky
každý	každý	k3xTgMnSc1	každý
starosta	starosta	k1gMnSc1	starosta
chce	chtít	k5eAaImIp3nS	chtít
mít	mít	k5eAaImF	mít
výjezd	výjezd	k1gInSc4	výjezd
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
2018	[number]	k4	2018
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
obdržel	obdržet	k5eAaPmAgInS	obdržet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tomio	Tomio	k6eAd1	Tomio
Okamurou	Okamura	k1gFnSc7	Okamura
<g/>
,	,	kIx,	,
Věrou	Věra	k1gFnSc7	Věra
Adámkovou	Adámková	k1gFnSc7	Adámková
a	a	k8xC	a
"	"	kIx"	"
<g/>
dalšími	další	k2eAgFnPc7d1	další
hvězdami	hvězda	k1gFnPc7	hvězda
politického	politický	k2eAgNnSc2d1	politické
nebe	nebe	k1gNnSc2	nebe
<g/>
"	"	kIx"	"
zlatý	zlatý	k2eAgInSc1d1	zlatý
bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
družstev	družstvo	k1gNnPc2	družstvo
za	za	k7c4	za
"	"	kIx"	"
<g/>
blábolení	blábolení	k1gNnSc4	blábolení
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
pitomostí	pitomost	k1gFnPc2	pitomost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
laudatia	laudatium	k1gNnSc2	laudatium
byly	být	k5eAaImAgInP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
jeho	jeho	k3xOp3gInPc1	jeho
výroky	výrok	k1gInPc1	výrok
o	o	k7c6	o
homeopaticích	homeopatice	k1gFnPc6	homeopatice
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Otázky	otázka	k1gFnSc2	otázka
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
roku	rok	k1gInSc2	rok
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
rozvíjejících	rozvíjející	k2eAgFnPc2d1	rozvíjející
se	se	k3xPyFc4	se
evropských	evropský	k2eAgFnPc2d1	Evropská
ekonomik	ekonomika	k1gFnPc2	ekonomika
dle	dle	k7c2	dle
časopisu	časopis	k1gInSc2	časopis
Emerging	Emerging	k1gInSc1	Emerging
Markets	Markets	k1gInSc1	Markets
</s>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
vydal	vydat	k5eAaPmAgInS	vydat
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
tyto	tento	k3xDgFnPc4	tento
publikace	publikace	k1gFnPc4	publikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
O	o	k7c6	o
čem	co	k3yQnSc6	co
sním	snít	k5eAaImIp1nS	snít
<g/>
,	,	kIx,	,
když	když	k8xS	když
náhodou	náhodou	k6eAd1	náhodou
spím	spát	k5eAaImIp1nS	spát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
282	[number]	k4	282
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
v	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
s	s	k7c7	s
18	[number]	k4	18
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
128	[number]	k4	128
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HANZELKA	HANZELKA	kA	HANZELKA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Babišovi	Babiš	k1gMnSc6	Babiš
bez	bez	k7c2	bez
Babiše	Babiše	k1gFnSc2	Babiše
:	:	kIx,	:
14	[number]	k4	14
rozhovorů	rozhovor	k1gInPc2	rozhovor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agrofert	Agrofert	k1gMnSc1	Agrofert
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
95	[number]	k4	95
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788026066392	[number]	k4	9788026066392
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOLÁČEK	Koláček	k1gMnSc1	Koláček
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Y.	Y.	kA	Y.
13	[number]	k4	13
<g/>
.	.	kIx.	.
komnaty	komnata	k1gFnSc2	komnata
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Petrklíč	petrklíč	k1gInSc1	petrklíč
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
209	[number]	k4	209
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788072295357	[number]	k4	9788072295357
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERGLER	PERGLER	kA	PERGLER
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
:	:	kIx,	:
příběh	příběh	k1gInSc1	příběh
oligarchy	oligarcha	k1gMnSc2	oligarcha
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
162	[number]	k4	162
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788020434456	[number]	k4	9788020434456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KMENTA	Kment	k1gMnSc4	Kment
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boss	boss	k1gMnSc1	boss
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kmenta	Kment	k1gMnSc2	Kment
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
346	[number]	k4	346
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788087569320	[number]	k4	9788087569320
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LEMEŠANI	LEMEŠANI	kA	LEMEŠANI
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Bureše	Bureš	k1gMnSc2	Bureš
Babišem	Babiš	k1gInSc7	Babiš
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Independent	independent	k1gMnSc1	independent
Media	medium	k1gNnSc2	medium
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
972789	[number]	k4	972789
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PATOČKA	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
;	;	kIx,	;
VLASATÁ	vlasatý	k2eAgFnSc1d1	vlasatá
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
.	.	kIx.	.
</s>
<s>
Žlutý	žlutý	k2eAgMnSc1d1	žlutý
baron	baron	k1gMnSc1	baron
:	:	kIx,	:
skutečný	skutečný	k2eAgInSc1d1	skutečný
plán	plán	k1gInSc1	plán
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiše	k1gFnSc2	Babiše
<g/>
:	:	kIx,	:
zřídit	zřídit	k5eAaPmF	zřídit
stát	stát	k1gInSc4	stát
jako	jako	k8xS	jako
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Referendum	referendum	k1gNnSc1	referendum
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
166	[number]	k4	166
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788027016747	[number]	k4	9788027016747
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VÁLKO	válka	k1gFnSc5	válka
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Počestný	počestný	k2eAgMnSc1d1	počestný
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Zvolen	Zvolen	k1gInSc1	Zvolen
<g/>
:	:	kIx,	:
FOTO	foto	k1gNnSc1	foto
Badinka	Badinka	k1gFnSc1	Badinka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
125	[number]	k4	125
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
968830	[number]	k4	968830
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOVORKOVÁ	Hovorková	k1gFnSc1	Hovorková
<g/>
,	,	kIx,	,
Johana	Johana	k1gFnSc1	Johana
<g/>
.	.	kIx.	.
</s>
<s>
Obrana	obrana	k1gFnSc1	obrana
před	před	k7c7	před
Babišem	Babiš	k1gInSc7	Babiš
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Trnová	trnový	k2eAgFnSc1d1	Trnová
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Trnová	trnový	k2eAgFnSc1d1	Trnová
<g/>
:	:	kIx,	:
Bourdon	bourdon	k1gInSc1	bourdon
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
212	[number]	k4	212
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788076110045	[number]	k4	9788076110045
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
</s>
</p>
<p>
<s>
Agrofert	Agrofert	k1gMnSc1	Agrofert
</s>
</p>
<p>
<s>
ANO	ano	k9	ano
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
</s>
</p>
<p>
<s>
Kauza	kauza	k1gFnSc1	kauza
Čapí	čapět	k5eAaImIp3nS	čapět
hnízdo	hnízdo	k1gNnSc4	hnízdo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gInSc4	Babiš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Spis	spis	k1gInSc1	spis
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc1d1	vedený
na	na	k7c4	na
agenta	agent	k1gMnSc4	agent
StB	StB	k1gMnSc4	StB
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Bureš	Bureš	k1gMnSc1	Bureš
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	s	k7c7	s
MFD	MFD	kA	MFD
a	a	k8xC	a
LN	LN	kA	LN
změnily	změnit	k5eAaPmAgFnP	změnit
po	po	k7c6	po
Babišově	Babišův	k2eAgInSc6d1	Babišův
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Mafry	Mafra	k1gFnSc2	Mafra
-	-	kIx~	-
článek	článek	k1gInSc1	článek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Přítomnost	přítomnost	k1gFnSc1	přítomnost
</s>
</p>
