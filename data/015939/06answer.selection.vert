<s>
Velkou	velký	k2eAgFnSc4d1
pečeť	pečeť	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
světle	světle	k6eAd1
modré	modrý	k2eAgNnSc1d1
kruhové	kruhový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
středu	střed	k1gInSc6
je	být	k5eAaImIp3nS
modrý	modrý	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
nápisem	nápis	k1gInSc7
TUEBOR	TUEBOR	kA
(	(	kIx(
<g/>
„	„	k?
<g/>
Budu	být	k5eAaImBp1nS
obhajovat	obhajovat	k5eAaImF
<g/>
“	“	k?
<g/>
)	)	kIx)
v	v	k7c6
hlavě	hlava	k1gFnSc6
štítu	štít	k1gInSc2
a	a	k8xC
zobrazující	zobrazující	k2eAgMnPc4d1
muže	muž	k1gMnPc4
se	s	k7c7
zbraní	zbraň	k1gFnSc7
v	v	k7c6
ruce	ruka	k1gFnSc6
stojícího	stojící	k2eAgInSc2d1
na	na	k7c6
zeleném	zelený	k2eAgInSc6d1
břehu	břeh	k1gInSc6
jezera	jezero	k1gNnSc2
při	při	k7c6
východu	východ	k1gInSc6
slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>