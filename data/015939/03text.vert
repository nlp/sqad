<s>
Pečeť	pečeť	k1gFnSc1
Michiganu	Michigan	k1gInSc2
</s>
<s>
Pečeť	pečeť	k1gFnSc1
státu	stát	k1gInSc2
Michigan	Michigan	k1gInSc4
</s>
<s>
Pečeť	pečeť	k1gFnSc1
Michiganu	Michigan	k1gInSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
federálních	federální	k2eAgMnPc2d1
států	stát	k1gInPc2
USA	USA	kA
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1835	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Velkou	velký	k2eAgFnSc4d1
pečeť	pečeť	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
světle	světle	k6eAd1
modré	modrý	k2eAgNnSc1d1
kruhové	kruhový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
středu	střed	k1gInSc6
je	být	k5eAaImIp3nS
modrý	modrý	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
nápisem	nápis	k1gInSc7
TUEBOR	TUEBOR	kA
(	(	kIx(
<g/>
„	„	k?
<g/>
Budu	být	k5eAaImBp1nS
obhajovat	obhajovat	k5eAaImF
<g/>
“	“	k?
<g/>
)	)	kIx)
v	v	k7c6
hlavě	hlava	k1gFnSc6
štítu	štít	k1gInSc2
a	a	k8xC
zobrazující	zobrazující	k2eAgMnPc4d1
muže	muž	k1gMnPc4
se	s	k7c7
zbraní	zbraň	k1gFnSc7
v	v	k7c6
ruce	ruka	k1gFnSc6
stojícího	stojící	k2eAgInSc2d1
na	na	k7c6
zeleném	zelený	k2eAgInSc6d1
břehu	břeh	k1gInSc6
jezera	jezero	k1gNnSc2
při	při	k7c6
východu	východ	k1gInSc6
slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muž	muž	k1gMnSc1
symbolizuje	symbolizovat	k5eAaImIp3nS
stranu	strana	k1gFnSc4
a	a	k8xC
míru	míra	k1gFnSc4
a	a	k8xC
schopnost	schopnost	k1gFnSc4
hájit	hájit	k5eAaImF
svá	svůj	k3xOyFgNnPc4
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štítonošem	štítonoš	k1gMnSc7
tohoto	tento	k3xDgInSc2
znaku	znak	k1gInSc2
je	být	k5eAaImIp3nS
heraldicky	heraldicky	k6eAd1
vpravo	vpravo	k6eAd1
jelen	jelena	k1gFnPc2
Wapiti	Wapit	k2eAgMnPc1d1
a	a	k8xC
vlevo	vlevo	k6eAd1
los	los	k1gInSc1
evropský	evropský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
jsou	být	k5eAaImIp3nP
typickým	typický	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
Michiganu	Michigan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nad	nad	k7c7
štítem	štít	k1gInSc7
je	být	k5eAaImIp3nS
heraldicky	heraldicky	k6eAd1
vpravo	vpravo	k6eAd1
hledící	hledící	k2eAgMnSc1d1
orel	orel	k1gMnSc1
bělohlavý	bělohlavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
pařátech	pařát	k1gInPc6
drží	držet	k5eAaImIp3nS
svazek	svazek	k1gInSc1
šípů	šíp	k1gInPc2
a	a	k8xC
olivovou	olivový	k2eAgFnSc4d1
ratolest	ratolest	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
ním	on	k3xPp3gMnSc7
je	být	k5eAaImIp3nS
červená	červený	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
s	s	k7c7
bíle	bíle	k6eAd1
napsaným	napsaný	k2eAgNnSc7d1
heslem	heslo	k1gNnSc7
v	v	k7c6
latině	latina	k1gFnSc6
<g/>
:	:	kIx,
E	E	kA
PLURIBUS	PLURIBUS	kA
UNUM	UNUM	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
Z	z	k7c2
mnohých	mnohý	k2eAgFnPc2d1
jeden	jeden	k4xCgMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
tyto	tento	k3xDgInPc1
prvky	prvek	k1gInPc1
symbolizují	symbolizovat	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
převzaty	převzít	k5eAaPmNgFnP
z	z	k7c2
velké	velký	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
pečeti	pečeť	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
štítem	štít	k1gInSc7
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
opět	opět	k6eAd1
s	s	k7c7
latinským	latinský	k2eAgNnSc7d1
heslem	heslo	k1gNnSc7
<g/>
:	:	kIx,
SI	si	k1gNnPc2
QUAERIS	QUAERIS	kA
PENINSULAM	PENINSULAM	kA
AMOENAM	AMOENAM	kA
CIRCUMSPICE	CIRCUMSPICE	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
Pokud	pokud	k8xS
hledáte	hledat	k5eAaImIp2nP
příjemný	příjemný	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
,	,	kIx,
dívejte	dívat	k5eAaImRp2nP
se	se	k3xPyFc4
kolem	kolem	k7c2
sebe	se	k3xPyFc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modrý	modrý	k2eAgInSc1d1
kruh	kruh	k1gInSc1
přechází	přecházet	k5eAaImIp3nS
<g/>
,	,	kIx,
ve	v	k7c6
směru	směr	k1gInSc6
od	od	k7c2
středu	střed	k1gInSc2
ven	ven	k6eAd1
<g/>
,	,	kIx,
do	do	k7c2
červeného	červený	k2eAgNnSc2d1
mezikruží	mezikruží	k1gNnSc2
s	s	k7c7
černým	černý	k2eAgInSc7d1
nápisem	nápis	k1gInSc7
<g/>
:	:	kIx,
THE	THE	kA
GREAT	GREAT	kA
SEAL	SEAL	kA
OF	OF	kA
THE	THE	kA
STATE	status	k1gInSc5
OF	OF	kA
MICHIGAN	Michigan	k1gInSc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
Velká	velký	k2eAgFnSc1d1
pečeť	pečeť	k1gFnSc1
státu	stát	k1gInSc2
Michigan	Michigan	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
horní	horní	k2eAgFnSc6d1
a	a	k8xC
A.D.	A.D.	k1gFnSc1
MDCCCXXXV	MDCCCXXXV	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
1835	#num#	k4
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
mezikruží	mezikruží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pečeť	pečeť	k1gFnSc1
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1835	#num#	k4
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
sám	sám	k3xTgMnSc1
druhý	druhý	k4xOgMnSc1
guvernér	guvernér	k1gMnSc1
tohoto	tento	k3xDgInSc2
státu	stát	k1gInSc2
–	–	k?
Lewis	Lewis	k1gFnSc2
Cass	Cassa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečeť	pečeť	k1gFnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
součástí	součást	k1gFnSc7
Michiganské	michiganský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
(	(	kIx(
<g/>
modrý	modrý	k2eAgInSc1d1
štít	štít	k1gInSc1
uprostřed	uprostřed	k6eAd1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pečeti	pečeť	k1gFnSc2
<g/>
,	,	kIx,
vytištěn	vytištěn	k2eAgMnSc1d1
na	na	k7c4
dokumenty	dokument	k1gInPc4
bez	bez	k7c2
designu	design	k1gInSc2
nebo	nebo	k8xC
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pečeť	pečeť	k1gFnSc1
guvernéra	guvernér	k1gMnSc2
</s>
<s>
Pečeť	pečeť	k1gFnSc1
státního	státní	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
</s>
<s>
Pečeť	pečeť	k1gFnSc1
generálního	generální	k2eAgMnSc2d1
prokurátora	prokurátor	k1gMnSc2
</s>
<s>
Pečeť	pečeť	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
financí	finance	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Seal	Seal	k1gInSc1
of	of	k?
Michigan	Michigan	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
E-Reference	E-Reference	k1gFnSc1
desk	deska	k1gFnPc2
–	–	k?
Great	Great	k1gInSc1
Seal	Seal	k1gInSc1
of	of	k?
the	the	k?
State	status	k1gInSc5
of	of	k?
Michigan	Michigan	k1gInSc1
<g/>
.	.	kIx.
www.e-referencedesk.com	www.e-referencedesk.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Michiganu	Michigan	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
pečetí	pečeť	k1gFnSc7
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pečeť	pečeť	k1gFnSc1
Michiganu	Michigan	k1gInSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
CIVIC	CIVIC	kA
HERALDRY	HERALDRY	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
Michigan	Michigan	k1gInSc1
<g/>
,	,	kIx,
state	status	k1gInSc5
seal	seal	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NETSTATE	NETSTATE	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
The	The	k1gFnSc3
Great	Great	k2eAgInSc1d1
Seal	Seal	k1gInSc1
of	of	k?
Michigan	Michigan	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
E-Reference	E-Reference	k1gFnSc1
desk	deska	k1gFnPc2
–	–	k?
Great	Great	k1gInSc1
Seal	Seal	k1gInSc1
of	of	k?
the	the	k?
State	status	k1gInSc5
of	of	k?
Michigan	Michigan	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
State	status	k1gInSc5
symbols	symbolsit	k5eAaPmRp2nS
USA	USA	kA
–	–	k?
Michigan	Michigan	k1gInSc1
State	status	k1gInSc5
Seal	Seal	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Seznam	seznam	k1gInSc1
pečetí	pečeť	k1gFnSc7
států	stát	k1gInPc2
USA	USA	kA
Federální	federální	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Alabama	Alabama	k1gFnSc1
•	•	k?
Aljaška	Aljaška	k1gFnSc1
•	•	k?
Arizona	Arizona	k1gFnSc1
•	•	k?
Arkansas	Arkansas	k1gInSc1
•	•	k?
Colorado	Colorado	k1gNnSc1
•	•	k?
Connecticut	Connecticut	k1gMnSc1
•	•	k?
Delaware	Delawar	k1gMnSc5
•	•	k?
Florida	Florida	k1gFnSc1
•	•	k?
Georgie	Georgie	k1gFnSc1
•	•	k?
Havaj	Havaj	k1gFnSc1
•	•	k?
Idaho	Ida	k1gMnSc2
•	•	k?
Illinois	Illinois	k1gFnSc2
•	•	k?
Indiana	Indiana	k1gFnSc1
•	•	k?
Iowa	Iow	k1gInSc2
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Kalifornie	Kalifornie	k1gFnSc2
•	•	k?
Kansas	Kansas	k1gInSc1
•	•	k?
Kentucky	Kentucka	k1gFnSc2
•	•	k?
Louisiana	Louisiana	k1gFnSc1
•	•	k?
Maine	Main	k1gInSc5
•	•	k?
Maryland	Marylanda	k1gFnPc2
•	•	k?
Massachusetts	Massachusetts	k1gNnSc4
•	•	k?
Michigan	Michigan	k1gInSc1
•	•	k?
Minnesota	Minnesota	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mississippi	Mississippi	k1gFnSc2
•	•	k?
Missouri	Missouri	k1gFnSc2
•	•	k?
Montana	Montana	k1gFnSc1
•	•	k?
Nebraska	Nebraska	k1gFnSc1
•	•	k?
Nevada	Nevada	k1gFnSc1
•	•	k?
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
•	•	k?
New	New	k1gMnSc4
Jersey	Jersea	k1gFnSc2
•	•	k?
New	New	k1gFnSc1
York	York	k1gInSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Ohio	Ohio	k1gNnSc1
•	•	k?
Oklahoma	Oklahom	k1gMnSc2
•	•	k?
Oregon	Oregon	k1gMnSc1
•	•	k?
Pensylvánie	Pensylvánie	k1gFnSc1
•	•	k?
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Tennessee	Tennessee	k1gInSc1
•	•	k?
Texas	Texas	k1gInSc1
•	•	k?
Utah	Utah	k1gInSc1
•	•	k?
Vermont	Vermont	k1gInSc1
•	•	k?
Virginie	Virginie	k1gFnSc1
•	•	k?
Washington	Washington	k1gInSc1
•	•	k?
Wisconsin	Wisconsin	k1gInSc1
•	•	k?
Wyoming	Wyoming	k1gInSc4
•	•	k?
Západní	západní	k2eAgFnSc2d1
Virginie	Virginie	k1gFnSc2
Federální	federální	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
District	District	k1gInSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
Autonomní	autonomní	k2eAgFnSc1d1
ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
Americké	americký	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Guam	Guam	k1gInSc1
(	(	kIx(
<g/>
znak	znak	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Portoriko	Portoriko	k1gNnSc1
(	(	kIx(
<g/>
znak	znak	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
