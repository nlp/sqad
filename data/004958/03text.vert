<s>
Guambiano	Guambiana	k1gFnSc5	Guambiana
je	on	k3xPp3gMnPc4	on
jihoamerický	jihoamerický	k2eAgInSc1d1	jihoamerický
domorodý	domorodý	k2eAgInSc1d1	domorodý
jazyk	jazyk	k1gInSc1	jazyk
barbakoské	barbakoský	k2eAgFnSc2d1	barbakoský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
jím	on	k3xPp3gNnSc7	on
asi	asi	k9	asi
23	[number]	k4	23
500	[number]	k4	500
domorodců	domorodec	k1gMnPc2	domorodec
v	v	k7c6	v
andské	andský	k2eAgFnSc6d1	andská
části	část	k1gFnSc6	část
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
písmem	písmo	k1gNnSc7	písmo
je	být	k5eAaImIp3nS	být
latinka	latinka	k1gFnSc1	latinka
<g/>
.	.	kIx.	.
</s>
