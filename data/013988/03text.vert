<s>
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
Bojový	bojový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
ČSLA	ČSLA	kA
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
-	-	kIx~
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Bojový	bojový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
ČSLA	ČSLA	kA
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
-	-	kIx~
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Země	zem	k1gFnSc2
</s>
<s>
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1954	#num#	k4
(	(	kIx(
<g/>
Přejmenování	přejmenování	k1gNnSc1
z	z	k7c2
Československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
)	)	kIx)
Zánik	zánik	k1gInSc1
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
transformace	transformace	k1gFnSc1
zpět	zpět	k6eAd1
na	na	k7c4
Československou	československý	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
armáda	armáda	k1gFnSc1
Velikost	velikost	k1gFnSc1
</s>
<s>
200	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
Motto	motto	k1gNnSc1
</s>
<s>
ZA	za	k7c4
VLAST	vlast	k1gFnSc4
–	–	k?
ZA	za	k7c4
SOCIALISMUS	socialismus	k1gInSc4
Podřízené	podřízený	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
Československé	československý	k2eAgNnSc1d1
letectvoČeskoslovenské	letectvoČeskoslovenský	k2eAgNnSc1d1
válečné	válečný	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
</s>
<s>
Prezident	prezident	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
Martin	Martin	k1gMnSc1
Dzúr	Dzúr	k1gMnSc1
během	během	k7c2
inspekce	inspekce	k1gFnSc2
Bratislavské	bratislavský	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
ČSLA	ČSLA	kA
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
přehlídka	přehlídka	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Letenské	letenský	k2eAgFnSc6d1
pláni	pláň	k1gFnSc6
na	na	k7c4
výročí	výročí	k1gNnSc4
konce	konec	k1gInSc2
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1985	#num#	k4
</s>
<s>
Brigadýrka	brigadýrka	k1gFnSc1
k	k	k7c3
uniformě	uniforma	k1gFnSc3
vzor	vzor	k1gInSc1
62	#num#	k4
pro	pro	k7c4
důstojníky	důstojník	k1gMnPc4
</s>
<s>
Muž	muž	k1gMnSc1
v	v	k7c6
polním	polní	k2eAgInSc6d1
stejnokroji	stejnokroj	k1gInSc6
ČSLA	ČSLA	kA
na	na	k7c6
Tankovém	tankový	k2eAgInSc6d1
dni	den	k1gInSc6
ve	v	k7c6
VTM	VTM	kA
Lešany	Lešana	k1gFnSc2
</s>
<s>
Československý	československý	k2eAgInSc1d1
polní	polní	k2eAgInSc1d1
telefon	telefon	k1gInSc1
TP	TP	kA
25	#num#	k4
</s>
<s>
Skupina	skupina	k1gFnSc1
odvedených	odvedený	k2eAgMnPc2d1
branců	branec	k1gMnPc2
v	v	k7c6
lesích	les	k1gInPc6
okolo	okolo	k7c2
Bzence	Bzenec	k1gInSc2
(	(	kIx(
<g/>
cca	cca	kA
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ČSLA	ČSLA	kA
<g/>
;	;	kIx,
též	též	k9
slovensky	slovensky	k6eAd1
Československá	československý	k2eAgFnSc1d1
ľudová	ľudový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
ČSĽA	ČSĽA	kA
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
armádní	armádní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
KSČ	KSČ	kA
<g/>
)	)	kIx)
a	a	k8xC
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1954	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
členská	členský	k2eAgFnSc1d1
síla	síla	k1gFnSc1
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
bylo	být	k5eAaImAgNnS
jméno	jméno	k1gNnSc1
armády	armáda	k1gFnSc2
změněno	změnit	k5eAaPmNgNnS
na	na	k7c6
"	"	kIx"
<g/>
Československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
z	z	k7c2
názvu	název	k1gInSc2
bylo	být	k5eAaImAgNnS
odstraněno	odstranit	k5eAaPmNgNnS
adjektivum	adjektivum	k1gNnSc1
„	„	k?
<g/>
Lidové	lidový	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
Armádu	armáda	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Ozbrojené	ozbrojený	k2eAgFnSc2d1
síly	síla	k1gFnSc2
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Československa	Československo	k1gNnSc2
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
ČSLA	ČSLA	kA
</s>
<s>
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
byla	být	k5eAaImAgFnS
schválena	schválit	k5eAaPmNgFnS
Prozatímní	prozatímní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
československých	československý	k2eAgFnPc2d1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
reorganizaci	reorganizace	k1gFnSc3
československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
bojovali	bojovat	k5eAaImAgMnP
proti	proti	k7c3
nacismu	nacismus	k1gInSc3
na	na	k7c6
všech	všecek	k3xTgFnPc6
frontách	fronta	k1gFnPc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
postupně	postupně	k6eAd1
vraceli	vracet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
Československa	Československo	k1gNnSc2
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
vojenské	vojenský	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
postupně	postupně	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
přes	přes	k7c4
16	#num#	k4
pěších	pěší	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
doplňovaly	doplňovat	k5eAaImAgFnP
tankový	tankový	k2eAgInSc4d1
sbor	sbor	k1gInSc4
a	a	k8xC
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1
divizi	divize	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československý	československý	k2eAgInSc1d1
I.	I.	kA
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
sloužil	sloužit	k5eAaImAgInS
pod	pod	k7c7
sovětskou	sovětský	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
československou	československý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
vojenským	vojenský	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počáteční	počáteční	k2eAgInSc1d1
optimismus	optimismus	k1gInSc1
ohledně	ohledně	k7c2
plánů	plán	k1gInPc2
na	na	k7c4
obnovu	obnova	k1gFnSc4
armády	armáda	k1gFnSc2
vystřídala	vystřídat	k5eAaPmAgFnS
deziluze	deziluze	k1gFnSc1
<g/>
,	,	kIx,
pramenící	pramenící	k2eAgFnSc1d1
z	z	k7c2
rozbité	rozbitý	k2eAgFnSc2d1
poválečné	poválečný	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
nedostatku	nedostatek	k1gInSc2
lidských	lidský	k2eAgInPc2d1
a	a	k8xC
materiálních	materiální	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
válce	válka	k1gFnSc6
pověřena	pověřit	k5eAaPmNgFnS
vyhnáním	vyhnání	k1gNnSc7
Němců	Němec	k1gMnPc2
a	a	k8xC
Maďarů	Maďar	k1gMnPc2
a	a	k8xC
podílela	podílet	k5eAaImAgFnS
se	se	k3xPyFc4
také	také	k9
na	na	k7c4
pomoci	pomoct	k5eAaPmF
národnímu	národní	k2eAgNnSc3d1
hospodářství	hospodářství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotky	jednotka	k1gFnPc4
Sboru	sbor	k1gInSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
se	se	k3xPyFc4
navíc	navíc	k6eAd1
účastnily	účastnit	k5eAaImAgFnP
bojů	boj	k1gInPc2
proti	proti	k7c3
Organizaci	organizace	k1gFnSc3
ukrajinských	ukrajinský	k2eAgMnPc2d1
nacionalistů	nacionalista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
ujala	ujmout	k5eAaPmAgFnS
moci	moct	k5eAaImF
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
významným	významný	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
důstojníků	důstojník	k1gMnPc2
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
pronásledována	pronásledovat	k5eAaImNgFnS
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
vojáci	voják	k1gMnPc1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
odejít	odejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politické	politický	k2eAgInPc1d1
procesy	proces	k1gInPc1
se	se	k3xPyFc4
zaměřovaly	zaměřovat	k5eAaImAgInP
hlavně	hlavně	k9
na	na	k7c4
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
bojovali	bojovat	k5eAaImAgMnP
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
paradoxně	paradoxně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
také	také	k9
k	k	k7c3
pronásledování	pronásledování	k1gNnSc3
vojáků	voják	k1gMnPc2
bojujících	bojující	k2eAgMnPc2d1
ve	v	k7c6
válce	válka	k1gFnSc6
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
plně	plně	k6eAd1
dostala	dostat	k5eAaPmAgFnS
pod	pod	k7c4
moc	moc	k1gFnSc4
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zásadní	zásadní	k2eAgFnSc3d1
reorganizaci	reorganizace	k1gFnSc3
sovětského	sovětský	k2eAgInSc2d1
modelu	model	k1gInSc2
a	a	k8xC
vojenské	vojenský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
byly	být	k5eAaImAgFnP
rozpuštěny	rozpuštěn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
mezi	mezi	k7c7
Československem	Československo	k1gNnSc7
a	a	k8xC
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
Dohoda	dohoda	k1gFnSc1
o	o	k7c6
způsobu	způsob	k1gInSc6
a	a	k8xC
podmínkách	podmínka	k1gFnPc6
vypořádání	vypořádání	k1gNnPc2
dodávaného	dodávaný	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
a	a	k8xC
materiálu	materiál	k1gInSc2
poskytovaná	poskytovaný	k2eAgFnSc1d1
půjčkou	půjčka	k1gFnSc7
SSSR	SSSR	kA
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
téměř	téměř	k6eAd1
44	#num#	k4
milionů	milion	k4xCgInPc2
rublů	rubl	k1gInPc2
na	na	k7c4
nákup	nákup	k1gInSc4
vojenské	vojenský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k6eAd1
letadel	letadlo	k1gNnPc2
a	a	k8xC
radarů	radar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nárůstu	nárůst	k1gInSc3
šíření	šíření	k1gNnSc2
a	a	k8xC
zvýšení	zvýšení	k1gNnSc2
počtu	počet	k1gInSc2
vojáků	voják	k1gMnPc2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1953	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
pořádala	pořádat	k5eAaImAgFnS
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
na	na	k7c6
Letné	Letná	k1gFnSc6
pravidelné	pravidelný	k2eAgFnSc2d1
přehlídky	přehlídka	k1gFnSc2
Dne	den	k1gInSc2
vítězství	vítězství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
přehlídka	přehlídka	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
každých	každý	k3xTgNnPc2
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehlídka	přehlídka	k1gFnSc1
rovněž	rovněž	k9
připomínala	připomínat	k5eAaImAgFnS
pražské	pražský	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
z	z	k7c2
těchto	tento	k3xDgFnPc2
přehlídek	přehlídka	k1gFnPc2
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Kde	kde	k6eAd1
domov	domov	k1gInSc4
můj	můj	k3xOp1gInSc4
a	a	k8xC
Nad	nad	k7c7
Tatrou	Tatra	k1gFnSc7
sa	sa	k?
blýska	blýska	k1gFnSc1
(	(	kIx(
<g/>
československá	československý	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
<g/>
)	)	kIx)
hráli	hrát	k5eAaImAgMnP
shromážděné	shromážděný	k2eAgFnPc4d1
kapely	kapela	k1gFnPc4
na	na	k7c6
přehlídce	přehlídka	k1gFnSc6
a	a	k8xC
poté	poté	k6eAd1
následovala	následovat	k5eAaImAgFnS
státní	státní	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehlídky	přehlídka	k1gFnPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
také	také	k9
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Složky	složka	k1gFnPc1
armády	armáda	k1gFnSc2
</s>
<s>
Součástí	součást	k1gFnSc7
ČSLA	ČSLA	kA
bylo	být	k5eAaImAgNnS
pozemní	pozemní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
a	a	k8xC
vojska	vojsko	k1gNnSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pozemní	pozemní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
</s>
<s>
Pozemní	pozemní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
nejpočetnější	početní	k2eAgFnSc7d3
složkou	složka	k1gFnSc7
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
jeho	jeho	k3xOp3gFnSc2
bojové	bojový	k2eAgFnSc2d1
síly	síla	k1gFnSc2
byly	být	k5eAaImAgFnP
operační	operační	k2eAgFnPc1d1
a	a	k8xC
taktické	taktický	k2eAgFnPc1d1
rakety	raketa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
údernou	úderný	k2eAgFnSc7d1
silou	síla	k1gFnSc7
byly	být	k5eAaImAgInP
tanky	tank	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělostřelectvo	dělostřelectvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vybaveno	vybavit	k5eAaPmNgNnS
různými	různý	k2eAgInPc7d1
typy	typ	k1gInPc7
děl	dělo	k1gNnPc2
a	a	k8xC
minometů	minomet	k1gInPc2
<g/>
,	,	kIx,
protitankovými	protitankový	k2eAgInPc7d1
kanóny	kanón	k1gInPc7
<g/>
,	,	kIx,
řízenými	řízený	k2eAgFnPc7d1
protitankovými	protitankový	k2eAgFnPc7d1
střelami	střela	k1gFnPc7
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
pozemnímu	pozemní	k2eAgNnSc3d1
vojsku	vojsko	k1gNnSc3
patřily	patřit	k5eAaImAgInP
motostřelecké	motostřelecký	k2eAgInPc1d1
a	a	k8xC
tankové	tankový	k2eAgInPc1d1
svazky	svazek	k1gInPc1
<g/>
,	,	kIx,
útvary	útvar	k1gInPc1
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
dělostřelectvo	dělostřelectvo	k1gNnSc1
a	a	k8xC
speciální	speciální	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	on	k3xPp3gFnPc4
ženijní	ženijní	k2eAgFnPc4d1
<g/>
,	,	kIx,
spojovací	spojovací	k2eAgFnPc4d1
<g/>
,	,	kIx,
chemické	chemický	k2eAgFnPc4d1
<g/>
,	,	kIx,
železniční	železniční	k2eAgFnPc4d1
a	a	k8xC
výsadkové	výsadkový	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
útvary	útvar	k1gInPc1
a	a	k8xC
zařízení	zařízení	k1gNnPc1
poskytovaly	poskytovat	k5eAaImAgFnP
bojovým	bojový	k2eAgInPc3d1
útvarům	útvar	k1gInPc3
nezbytné	nezbytný	k2eAgFnSc2d1,k2eNgFnSc2d1
technické	technický	k2eAgFnSc2d1
<g/>
,	,	kIx,
materiální	materiální	k2eAgFnSc2d1
a	a	k8xC
další	další	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ženijní	ženijní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
vybaveno	vybavit	k5eAaPmNgNnS
silničními	silniční	k2eAgInPc7d1
a	a	k8xC
zemními	zemní	k2eAgInPc7d1
stroji	stroj	k1gInPc7
<g/>
,	,	kIx,
odminovacími	odminovací	k2eAgInPc7d1
tanky	tank	k1gInPc7
a	a	k8xC
technikou	technika	k1gFnSc7
k	k	k7c3
zajištění	zajištění	k1gNnSc3
přepravy	přeprava	k1gFnSc2
vojsk	vojsko	k1gNnPc2
přes	přes	k7c4
vodní	vodní	k2eAgInPc4d1
toky	tok	k1gInPc4
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
terénní	terénní	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železniční	železniční	k2eAgInSc4d1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
schopné	schopný	k2eAgNnSc1d1
zajišťovat	zajišťovat	k5eAaImF
výstavbu	výstavba	k1gFnSc4
i	i	k8xC
obnovu	obnova	k1gFnSc4
železničních	železniční	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
včetně	včetně	k7c2
mostů	most	k1gInPc2
a	a	k8xC
sdělovacích	sdělovací	k2eAgNnPc2d1
a	a	k8xC
zabezpečovacích	zabezpečovací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
a	a	k8xC
údržbu	údržba	k1gFnSc4
železničních	železniční	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železniční	železniční	k2eAgFnSc1d1
i	i	k8xC
ženijní	ženijní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgFnP
na	na	k7c4
likvidaci	likvidace	k1gFnSc4
následků	následek	k1gInPc2
živelních	živelní	k2eAgFnPc2d1
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
pohrom	pohroma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
přibližně	přibližně	k6eAd1
201	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
v	v	k7c6
aktivní	aktivní	k2eAgFnSc6d1
službě	služba	k1gFnSc6
v	v	k7c6
ČSLA	ČSLA	kA
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
asi	asi	k9
145	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
72	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
sloužilo	sloužit	k5eAaImAgNnS
v	v	k7c6
pozemních	pozemní	k2eAgFnPc6d1
silách	síla	k1gFnPc6
(	(	kIx(
<g/>
běžně	běžně	k6eAd1
označovaných	označovaný	k2eAgNnPc2d1
jako	jako	k8xC,k8xS
armáda	armáda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
100	#num#	k4
000	#num#	k4
z	z	k7c2
nich	on	k3xPp3gInPc2
byli	být	k5eAaImAgMnP
branci	branec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
mírovém	mírový	k2eAgInSc6d1
stavu	stav	k1gInSc6
se	se	k3xPyFc4
armáda	armáda	k1gFnSc1
dělila	dělit	k5eAaImAgFnS
na	na	k7c4
dva	dva	k4xCgInPc4
vojenské	vojenský	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
<g/>
,	,	kIx,
Západní	západní	k2eAgMnPc1d1
a	a	k8xC
Východní	východní	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
Západní	západní	k2eAgInSc4d1
okruh	okruh	k1gInSc4
spadala	spadat	k5eAaImAgFnS,k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
s	s	k7c7
velitelstvím	velitelství	k1gNnSc7
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
s	s	k7c7
jednou	jeden	k4xCgFnSc7
tankovou	tankový	k2eAgFnSc7d1
divizí	divize	k1gFnSc7
a	a	k8xC
třemi	tři	k4xCgFnPc7
motostřeleckými	motostřelecký	k2eAgFnPc7d1
divizemi	divize	k1gFnPc7
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
v	v	k7c6
Písku	Písek	k1gInSc6
se	s	k7c7
dvěma	dva	k4xCgFnPc7
tankovými	tankový	k2eAgFnPc7d1
divizemi	divize	k1gFnPc7
a	a	k8xC
dvěma	dva	k4xCgFnPc7
motostřeleckými	motostřelecký	k2eAgFnPc7d1
divizemi	divize	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
tvořily	tvořit	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
tankové	tankový	k2eAgFnPc4d1
divize	divize	k1gFnPc4
s	s	k7c7
velitelstvím	velitelství	k1gNnSc7
v	v	k7c6
Trenčíně	Trenčín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgFnPc2d1
pět	pět	k4xCc1
divizí	divize	k1gFnPc2
s	s	k7c7
tzv.	tzv.	kA
uloženou	uložený	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
bylo	být	k5eAaImAgNnS
připraveno	připravit	k5eAaPmNgNnS
k	k	k7c3
personálnímu	personální	k2eAgNnSc3d1
naplnění	naplnění	k1gNnSc1
mobilizovanými	mobilizovaný	k2eAgFnPc7d1
zálohami	záloha	k1gFnPc7
do	do	k7c2
48	#num#	k4
hodin	hodina	k1gFnPc2
od	od	k7c2
vyhlášení	vyhlášení	k1gNnSc2
mobilizace	mobilizace	k1gFnSc1
<g/>
,	,	kIx,
celková	celkový	k2eAgFnSc1d1
válečná	válečný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
ČSLA	ČSLA	kA
tedy	tedy	k9
v	v	k7c6
případě	případ	k1gInSc6
vypuknutí	vypuknutí	k1gNnSc2
války	válka	k1gFnSc2
činila	činit	k5eAaImAgFnS
15	#num#	k4
vševojskových	vševojskový	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Západní	západní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
provozní	provozní	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
311	#num#	k4
<g/>
.	.	kIx.
těžká	těžký	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Jince	Jince	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
základna	základna	k1gFnSc1
(	(	kIx(
<g/>
Jince	Jince	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
41	#num#	k4
<g/>
.	.	kIx.
samostatný	samostatný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
dělostřelecký	dělostřelecký	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
(	(	kIx(
<g/>
Dašice	Dašice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
71	#num#	k4
<g/>
.	.	kIx.
kanónová	kanónový	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Žamberk	Žamberk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
75	#num#	k4
<g/>
.	.	kIx.
těžká	těžký	k2eAgFnSc1d1
houfnicová	houfnicový	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
82	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
raketová	raketový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Jihlava	Jihlava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
opravárenská	opravárenský	k2eAgFnSc1d1
technická	technický	k2eAgFnSc1d1
základna	základna	k1gFnSc1
(	(	kIx(
<g/>
Jaroměř	Jaroměř	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
radiotechnická	radiotechnický	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
71	#num#	k4
<g/>
.	.	kIx.
výsadkový	výsadkový	k2eAgInSc1d1
úderný	úderný	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Chrudim	Chrudim	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
výsadková	výsadkový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
speciálního	speciální	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
Prostějov	Prostějov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
radiotechnická	radiotechnický	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
zvláštního	zvláštní	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
Zbiroh	Zbiroh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
smíšený	smíšený	k2eAgInSc1d1
pluk	pluk	k1gInSc1
radioelektronického	radioelektronický	k2eAgInSc2d1
boje	boj	k1gInSc2
(	(	kIx(
<g/>
Kolín	Kolín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
pontonová	pontonový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Strašice	Strašice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
52	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
dálkových	dálkový	k2eAgInPc2d1
spojů	spoj	k1gInPc2
(	(	kIx(
<g/>
Lipník	Lipník	k1gInSc1
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
59	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
dálkových	dálkový	k2eAgInPc2d1
spojů	spoj	k1gInPc2
(	(	kIx(
<g/>
Beroun	Beroun	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
60	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
prapor	prapor	k1gInSc1
zvláštního	zvláštní	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
Unhošť	Unhošť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
102	#num#	k4
<g/>
.	.	kIx.
brigáda	brigáda	k1gFnSc1
chemické	chemický	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
(	(	kIx(
<g/>
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
smíšená	smíšený	k2eAgFnSc1d1
letka	letka	k1gFnSc1
velení	velení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
(	(	kIx(
<g/>
Bechyně	Bechyně	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
rota	rota	k1gFnSc1
letištního	letištní	k2eAgNnSc2d1
a	a	k8xC
radiotechnického	radiotechnický	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Bechyně	Bechyně	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
311	#num#	k4
<g/>
.	.	kIx.
samostatný	samostatný	k2eAgInSc1d1
výcvikový	výcvikový	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Hradiště	Hradiště	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
313	#num#	k4
<g/>
.	.	kIx.
samostatný	samostatný	k2eAgInSc1d1
výcvikový	výcvikový	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Boletice	Boletika	k1gFnSc3
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
automobilní	automobilní	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
automobilní	automobilní	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Hlučín	Hlučín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
brigáda	brigáda	k1gFnSc1
materiálního	materiální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Bílina	Bílina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
brigáda	brigáda	k1gFnSc1
materiálního	materiální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
brigáda	brigáda	k1gFnSc1
potrubní	potrubní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
pohonných	pohonný	k2eAgFnPc2d1
hmot	hmota	k1gFnPc2
(	(	kIx(
<g/>
Roudnice	Roudnice	k1gFnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
ženijní	ženijní	k2eAgFnSc1d1
silniční	silniční	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Hodonín	Hodonín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
silniční	silniční	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Horní	horní	k2eAgFnSc1d1
Počáply	Počáply	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
Varnsdorf	Varnsdorf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
Bučovice	Bučovice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
strážní	strážní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Příbram	Příbram	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
sklady	sklad	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
okruhový	okruhový	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
sklad	sklad	k1gInSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnSc2
(	(	kIx(
<g/>
Zdice	Zdice	k1gFnPc1
<g/>
,	,	kIx,
Vamberk	Vamberk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc4d1
sklad	sklad	k1gInSc4
proviantního	proviantní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
Rychnovek	Rychnovka	k1gFnPc2
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnSc2
(	(	kIx(
<g/>
Terezín	Terezín	k1gInSc1
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc4d1
ženijní	ženijní	k2eAgInSc4d1
sklad	sklad	k1gInSc4
(	(	kIx(
<g/>
Dolní	dolní	k2eAgInSc4d1
Bousov	Bousov	k1gInSc4
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Jánská	Jánská	k1gFnSc1
<g/>
,	,	kIx,
Jaroměř	Jaroměř	k1gFnSc1
<g/>
,	,	kIx,
Dobříš	Dobříš	k1gFnSc1
<g/>
,	,	kIx,
Hněvkovice	Hněvkovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc1d1
spojovací	spojovací	k2eAgInSc1d1
sklad	sklad	k1gInSc1
a	a	k8xC
opravna	opravna	k1gFnSc1
(	(	kIx(
<g/>
Červené	Červené	k2eAgFnPc1d1
Pečky	Pečky	k1gFnPc1
<g/>
)	)	kIx)
pobočka	pobočka	k1gFnSc1
(	(	kIx(
<g/>
Červený	červený	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc4d1
chemický	chemický	k2eAgInSc4d1
sklad	sklad	k1gInSc4
(	(	kIx(
<g/>
Račice	račice	k1gFnSc1
nad	nad	k7c7
Trotinou	Trotina	k1gFnSc7
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnSc2
(	(	kIx(
<g/>
Rumburk	Rumburk	k1gInSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Zásmuky	Zásmuky	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc1d1
automobilní	automobilní	k2eAgInSc1d1
sklad	sklad	k1gInSc1
(	(	kIx(
<g/>
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnSc2
(	(	kIx(
<g/>
Poprad	Poprad	k1gInSc1
<g/>
,	,	kIx,
Nemecká	Nemecká	k1gFnSc1
<g/>
,	,	kIx,
Vysoké	vysoký	k2eAgNnSc1d1
Mýto	mýto	k1gNnSc1
<g/>
,	,	kIx,
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc4d1
sklad	sklad	k1gInSc4
politicko-osvětových	politicko-osvětový	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnPc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Čáslav	Čáslav	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc4d1
sklad	sklad	k1gInSc4
výstrojního	výstrojní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
Jaroměř	Jaroměř	k1gFnSc1
pobočky	pobočka	k1gFnSc2
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhová	okruhový	k2eAgFnSc1d1
základna	základna	k1gFnSc1
pohonný	pohonný	k2eAgInSc1d1
hmot	hmota	k1gFnPc2
(	(	kIx(
<g/>
Chlumec	Chlumec	k1gInSc1
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnSc2
(	(	kIx(
<g/>
Halenkov	Halenkov	k1gInSc1
<g/>
,	,	kIx,
Mošnov	Mošnov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc4d1
zdravotnický	zdravotnický	k2eAgInSc4d1
sklad	sklad	k1gInSc4
(	(	kIx(
<g/>
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
pobočky	pobočka	k1gFnSc2
(	(	kIx(
<g/>
Dobrá	dobrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Golčův	Golčův	k2eAgInSc1d1
Jeníkov	Jeníkov	k1gInSc1
<g/>
,	,	kIx,
Vlčí	vlčí	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
okruhový	okruhový	k2eAgInSc4d1
sklad	sklad	k1gInSc4
ubytovacího	ubytovací	k2eAgInSc2d1
a	a	k8xC
stavebního	stavební	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
výzbrojní	výzbrojní	k2eAgFnSc1d1
základna	základna	k1gFnSc1
(	(	kIx(
<g/>
Jaroměř	Jaroměř	k1gFnSc1
<g/>
)	)	kIx)
pobočka	pobočka	k1gFnSc1
(	(	kIx(
<g/>
Terezín	Terezín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
silniční	silniční	k2eAgFnSc1d1
mostní	mostní	k2eAgFnSc1d1
technická	technický	k2eAgFnSc1d1
základna	základna	k1gFnSc1
Chrudim	Chrudim	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
Příbram	Příbram	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
provozní	provozní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Příbram	Příbram	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
321	#num#	k4
<g/>
.	.	kIx.
těžká	těžký	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Rokycany	Rokycany	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
základna	základna	k1gFnSc1
(	(	kIx(
<g/>
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
322	#num#	k4
<g/>
.	.	kIx.
kanónová	kanónový	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Dobřany	Dobřany	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
216	#num#	k4
<g/>
.	.	kIx.
protitankový	protitankový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Most	most	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
průzkumný	průzkumný	k2eAgInSc1d1
dělostřelecký	dělostřelecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Holýšov	Holýšov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
171	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Rožmitál	Rožmitál	k1gInSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
opravárenská	opravárenský	k2eAgFnSc1d1
technická	technický	k2eAgFnSc1d1
základna	základna	k1gFnSc1
(	(	kIx(
<g/>
Rožmitál	Rožmitál	k1gInSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
radiotechnický	radiotechnický	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Holýšov	Holýšov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
51	#num#	k4
<g/>
.	.	kIx.
ženijní	ženijní	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Litoměřice	Litoměřice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
91	#num#	k4
<g/>
.	.	kIx.
pontonový	pontonový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Litoměřice	Litoměřice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
pluk	pluk	k1gInSc1
dálkových	dálkový	k2eAgInPc2d1
spojů	spoj	k1gInPc2
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
71	#num#	k4
<g/>
.	.	kIx.
radiotechnický	radiotechnický	k2eAgInSc1d1
pluk	pluk	k1gInSc1
zvláštního	zvláštní	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
Kladno	Kladno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
smíšený	smíšený	k2eAgInSc1d1
pluk	pluk	k1gInSc1
radioelektronického	radioelektronický	k2eAgInSc2d1
boje	boj	k1gInSc2
(	(	kIx(
<g/>
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
103	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
chemické	chemický	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
(	(	kIx(
<g/>
Lešany	Lešana	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
vrtulníkový	vrtulníkový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Plzeň-Bory	Plzeň-Bora	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
111	#num#	k4
<g/>
.	.	kIx.
letištní	letištní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Plzeň-Bory	Plzeň-Bora	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
rota	rota	k1gFnSc1
spojovacího	spojovací	k2eAgNnSc2d1
a	a	k8xC
radiotechnického	radiotechnický	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Plzeň-Bory	Plzeň-Bora	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
letka	letka	k1gFnSc1
velení	velení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
(	(	kIx(
<g/>
Plzeň-Bory	Plzeň-Bora	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
rota	rota	k1gFnSc1
letištního	letištní	k2eAgNnSc2d1
a	a	k8xC
radiotechnického	radiotechnický	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Plzeň-Bory	Plzeň-Bora	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
101	#num#	k4
<g/>
.	.	kIx.
letka	letka	k1gFnSc1
bezpilotních	bezpilotní	k2eAgInPc2d1
průzkumných	průzkumný	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
(	(	kIx(
<g/>
Stříbro	stříbro	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
brigáda	brigáda	k1gFnSc1
materiálního	materiální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Terezín	Terezín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
silniční	silniční	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Jince-Velcí	Jince-Velcí	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Slaný	Slaný	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
motostřelecká	motostřelecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Sušice	Sušice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
motostřelecká	motostřelecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
motostřelecká	motostřelecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
Písek	Písek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
provozní	provozní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Písek	Písek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
331	#num#	k4
<g/>
.	.	kIx.
těžká	těžký	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Hranice	hranice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
základna	základna	k1gFnSc1
(	(	kIx(
<g/>
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
332	#num#	k4
<g/>
.	.	kIx.
kanónová	kanónový	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Jičín	Jičín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
217	#num#	k4
<g/>
.	.	kIx.
protitankový	protitankový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Lešany	Lešana	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
průzkumný	průzkumný	k2eAgInSc1d1
dělostřelecký	dělostřelecký	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
(	(	kIx(
<g/>
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
251	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Kroměříž	Kroměříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
radiotechnický	radiotechnický	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Vimperk	Vimperk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ženijní	ženijní	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
72	#num#	k4
<g/>
.	.	kIx.
pontonový	pontonový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Kamýk	Kamýk	k1gInSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Písek	Písek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
pluk	pluk	k1gInSc1
dálkových	dálkový	k2eAgInPc2d1
spojů	spoj	k1gInPc2
(	(	kIx(
<g/>
Písek	Písek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
74	#num#	k4
<g/>
.	.	kIx.
radiotechnický	radiotechnický	k2eAgInSc1d1
pluk	pluk	k1gInSc1
zvláštního	zvláštní	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
Horažďovice	Horažďovice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
smíšený	smíšený	k2eAgInSc1d1
pluk	pluk	k1gInSc1
radioelektronického	radioelektronický	k2eAgInSc2d1
boje	boj	k1gInSc2
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
105	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
chemické	chemický	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
(	(	kIx(
<g/>
Jaroměř	Jaroměř	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
51	#num#	k4
<g/>
.	.	kIx.
vrtulníkový	vrtulníkový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Prostějov	Prostějov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
51	#num#	k4
<g/>
.	.	kIx.
letištní	letištní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Prostějov	Prostějov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
61	#num#	k4
<g/>
.	.	kIx.
rota	rota	k1gFnSc1
spojovacího	spojovací	k2eAgNnSc2d1
a	a	k8xC
radiotechnického	radiotechnický	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Prostějov	Prostějov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
52	#num#	k4
<g/>
.	.	kIx.
letka	letka	k1gFnSc1
velení	velení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
(	(	kIx(
<g/>
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
52	#num#	k4
<g/>
.	.	kIx.
letka	letka	k1gFnSc1
letištního	letištní	k2eAgNnSc2d1
a	a	k8xC
radiotechnického	radiotechnický	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
104	#num#	k4
<g/>
.	.	kIx.
letka	letka	k1gFnSc1
bezpilotních	bezpilotní	k2eAgFnPc2d1
průzkumný	průzkumný	k2eAgInSc4d1
prostředků	prostředek	k1gInPc2
(	(	kIx(
<g/>
Krašovice	Krašovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
brigáda	brigáda	k1gFnSc1
materiálního	materiální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
Pacov	Pacov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
silniční	silniční	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Vimperk	Vimperk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
motostřelecká	motostřelecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Kroměříž	Kroměříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
motostřelecká	motostřelecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Východní	východní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
(	(	kIx(
<g/>
Trenčín	Trenčín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
provozní	provozní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Trenčín	Trenčín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ženijní	ženijní	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Sereď	Sereď	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
automobilní	automobilní	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Hlohovec	Hlohovec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
42	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Trenčín	Trenčín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
Žilina	Žilina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
Malacky	malacky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Topoľčany	Topoľčan	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
Prešov	Prešov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
</s>
<s>
Síly	síla	k1gFnPc1
vzdušné	vzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
a	a	k8xC
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
CPA	cpát	k5eAaImSgInS
slavily	slavit	k5eAaImAgInP
jako	jako	k8xC,k8xS
den	den	k1gInSc1
vzniku	vznik	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgNnSc3
datu	datum	k1gNnSc3
odletěl	odletět	k5eAaPmAgInS
na	na	k7c4
slovenskou	slovenský	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Slovenskému	slovenský	k2eAgInSc3d1
národnímu	národní	k2eAgInSc3d1
povstání	povstání	k1gNnSc2
ze	z	k7c2
SSSR	SSSR	kA
1	#num#	k4
<g/>
.	.	kIx.
československý	československý	k2eAgInSc1d1
samostatný	samostatný	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
něj	on	k3xPp3gInSc2
později	pozdě	k6eAd2
vyrostla	vyrůst	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
smíšená	smíšený	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
operačně	operačně	k6eAd1
působila	působit	k5eAaImAgFnS
v	v	k7c6
závěru	závěr	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
sovětském	sovětský	k2eAgNnSc6d1
letectvu	letectvo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Československé	československý	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
plně	plně	k6eAd1
vybaveno	vybavit	k5eAaPmNgNnS
nadzvukovými	nadzvukový	k2eAgFnPc7d1
stíhačkami	stíhačka	k1gFnPc7
<g/>
,	,	kIx,
útočnými	útočný	k2eAgInPc7d1
vrtulníky	vrtulník	k1gInPc7
<g/>
,	,	kIx,
systémy	systém	k1gInPc7
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
a	a	k8xC
elektronickým	elektronický	k2eAgNnSc7d1
sledovacím	sledovací	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Vojska	vojsko	k1gNnPc1
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
státu	stát	k1gInSc2
</s>
<s>
Vojska	vojsko	k1gNnPc1
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
PVOS	PVOS	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
tvořena	tvořit	k5eAaImNgFnS
protiletadlovými	protiletadlový	k2eAgInPc7d1
raketovými	raketový	k2eAgInPc7d1
útvary	útvar	k1gInPc7
<g/>
,	,	kIx,
útvary	útvar	k1gInPc1
stíhacího	stíhací	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
radiotechnickými	radiotechnický	k2eAgInPc7d1
a	a	k8xC
speciálními	speciální	k2eAgInPc7d1
útvary	útvar	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
stíhací	stíhací	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
76	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
raketová	raketový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
77	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
raketová	raketový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
186	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
raketová	raketový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Pezinok	Pezinok	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
radiotechnická	radiotechnický	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
Žatec	Žatec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
stíhací	stíhací	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
letištní	letištní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
stíhací	stíhací	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Žatec	Žatec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
letištní	letištní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Žatec	Žatec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
opravny	opravna	k1gFnSc2
(	(	kIx(
<g/>
Žatec	Žatec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
185	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
Kralovice	Kralovice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
71	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
raketová	raketový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Slaný	Slaný	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Žatec	Žatec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
provozní	provozní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
Žatec	Žatec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
radiotechnická	radiotechnický	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
(	(	kIx(
<g/>
Chomutov	Chomutov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hodnosti	hodnost	k1gFnPc1
</s>
<s>
1954	#num#	k4
-	-	kIx~
1959	#num#	k4
</s>
<s>
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
měla	mít	k5eAaImAgFnS
soustavu	soustava	k1gFnSc4
hodností	hodnost	k1gFnPc2
převzatu	převzat	k2eAgFnSc4d1
ze	z	k7c2
Sovětské	sovětský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
r.	r.	kA
1958	#num#	k4
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
dílčím	dílčí	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
byly	být	k5eAaImAgInP
završeny	završit	k5eAaPmNgInP
zákonem	zákon	k1gInSc7
76	#num#	k4
<g/>
/	/	kIx~
<g/>
1959	#num#	k4
o	o	k7c6
některých	některý	k3yIgInPc6
služebních	služební	k2eAgInPc6d1
poměrech	poměr	k1gInPc6
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1960	#num#	k4
-	-	kIx~
1990	#num#	k4
</s>
<s>
Od	od	k7c2
r.	r.	kA
1960	#num#	k4
bylo	být	k5eAaImAgNnS
upraveno	upravit	k5eAaPmNgNnS
seskupení	seskupení	k1gNnSc1
hodnostních	hodnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
s	s	k7c7
malými	malý	k2eAgFnPc7d1
odchylkami	odchylka	k1gFnPc7
platilo	platit	k5eAaImAgNnS
i	i	k8xC
v	v	k7c6
obnovené	obnovený	k2eAgFnSc6d1
Československé	československý	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
do	do	k7c2
r.	r.	kA
1992	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
mužstvo	mužstvo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
vojín	vojín	k1gMnSc1
</s>
<s>
poddůstojníci	poddůstojník	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
svobodník	svobodník	k1gMnSc1
<g/>
,	,	kIx,
desátník	desátník	k1gMnSc1
<g/>
,	,	kIx,
četař	četař	k1gMnSc1
</s>
<s>
praporčíci	praporčík	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
rotný	rotný	k1gMnSc1
<g/>
,	,	kIx,
rotmistr	rotmistr	k1gMnSc1
<g/>
,	,	kIx,
nadrotmistr	nadrotmistr	k1gMnSc1
<g/>
,	,	kIx,
podpraporčík	podpraporčík	k1gMnSc1
<g/>
,	,	kIx,
praporčík	praporčík	k1gMnSc1
<g/>
,	,	kIx,
nadpraporčík	nadpraporčík	k1gMnSc1
</s>
<s>
důstojníci	důstojník	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
podporučík	podporučík	k1gMnSc1
<g/>
,	,	kIx,
poručík	poručík	k1gMnSc1
<g/>
,	,	kIx,
nadporučík	nadporučík	k1gMnSc1
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
<g/>
,	,	kIx,
major	major	k1gMnSc1
<g/>
,	,	kIx,
podplukovník	podplukovník	k1gMnSc1
<g/>
,	,	kIx,
plukovník	plukovník	k1gMnSc1
</s>
<s>
generálové	generál	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
generálmajor	generálmajor	k1gMnSc1
<g/>
,	,	kIx,
generálporučík	generálporučík	k1gMnSc1
<g/>
,	,	kIx,
generálplukovník	generálplukovník	k1gMnSc1
<g/>
,	,	kIx,
armádní	armádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
</s>
<s>
Rovněž	rovněž	k9
bylo	být	k5eAaImAgNnS
upraveno	upravit	k5eAaPmNgNnS
hodnostní	hodnostní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
více	hodně	k6eAd2
přiblížilo	přiblížit	k5eAaPmAgNnS
tradičnímu	tradiční	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
na	na	k7c6
výložkách	výložka	k1gFnPc6
udává	udávat	k5eAaImIp3nS
následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1
</s>
<s>
Poddůstojníci	poddůstojník	k1gMnPc1
</s>
<s>
Praporčíci	praporčík	k1gMnPc1
</s>
<s>
Nižší	nízký	k2eAgMnPc1d2
důstojníci	důstojník	k1gMnPc1
</s>
<s>
Vyšší	vysoký	k2eAgMnPc1d2
důstojníci	důstojník	k1gMnPc1
</s>
<s>
Generálové	generál	k1gMnPc1
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
pro	pro	k7c4
objasnění	objasnění	k1gNnSc4
událostí	událost	k1gFnPc2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
charakterizovala	charakterizovat	k5eAaBmAgFnS
Československou	československý	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
takto	takto	k6eAd1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
vedle	vedle	k7c2
SNB	SNB	kA
a	a	k8xC
LM	LM	kA
chápána	chápat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgInSc4
z	z	k7c2
přímých	přímý	k2eAgInPc2d1
mocenských	mocenský	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
ovládání	ovládání	k1gNnSc2
společnosti	společnost	k1gFnSc2
a	a	k8xC
bezprostředního	bezprostřední	k2eAgNnSc2d1
zvládání	zvládání	k1gNnSc2
vnitropolitických	vnitropolitický	k2eAgFnPc2d1
potíží	potíž	k1gFnPc2
<g/>
,	,	kIx,
KSČ	KSČ	kA
prostřednictvím	prostřednictvím	k7c2
obrovského	obrovský	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
Hlavní	hlavní	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
správy	správa	k1gFnSc2
ČSLA	ČSLA	kA
prolínajícího	prolínající	k2eAgNnSc2d1
se	se	k3xPyFc4
do	do	k7c2
nejnižších	nízký	k2eAgFnPc2d3
jednotek	jednotka	k1gFnPc2
prakticky	prakticky	k6eAd1
kontrolovala	kontrolovat	k5eAaImAgFnS
svůj	svůj	k3xOyFgInSc4
absolutní	absolutní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
Sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
navrhoval	navrhovat	k5eAaImAgMnS
komunistický	komunistický	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
národní	národní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
Václavík	Václavík	k1gMnSc1
nasazení	nasazení	k1gNnSc2
armády	armáda	k1gFnSc2
proti	proti	k7c3
demonstrantům	demonstrant	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc1
návrh	návrh	k1gInSc1
nebyl	být	k5eNaImAgInS
vyslyšen	vyslyšet	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Vojenská	vojenský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
1	#num#	k4
[	[	kIx(
<g/>
1945	#num#	k4
<g/>
-	-	kIx~
<g/>
1950	#num#	k4
<g/>
]	]	kIx)
:	:	kIx,
Velitelství	velitelství	k1gNnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Praha	Praha	k1gFnSc1
zažila	zažít	k5eAaPmAgFnS
vojenskou	vojenský	k2eAgFnSc4d1
přehlídku	přehlídka	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
23	#num#	k4
letech	léto	k1gNnPc6
|	|	kIx~
Domov	domov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-10-28	2008-10-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Prahou	Praha	k1gFnSc7
má	mít	k5eAaImIp3nS
burácet	burácet	k5eAaImF
velkolepá	velkolepý	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
přehlídka	přehlídka	k1gFnSc1
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HAVELKA	Havelka	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CZK	CZK	kA
-	-	kIx~
Hodnosti	hodnost	k1gFnPc1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
[	[	kIx(
<g/>
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1992	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
4306	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
76	#num#	k4
<g/>
/	/	kIx~
<g/>
1959	#num#	k4
o	o	k7c6
některých	některý	k3yIgInPc6
služebních	služební	k2eAgInPc6d1
poměrech	poměr	k1gInPc6
vojáků	voják	k1gMnPc2
<g/>
↑	↑	k?
HODNOSTNÍ	hodnostní	k2eAgNnSc4d1
OZNAČENÍ	označení	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
vojenské	vojenský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
ČESKOSLOVENSKÉ	československý	k2eAgFnSc2d1
LIDOVÉ	lidový	k2eAgFnSc2d1
ARMÁDY	armáda	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Závěrečná	závěrečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
pro	pro	k7c4
objasnění	objasnění	k1gNnSc4
událostí	událost	k1gFnPc2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
-	-	kIx~
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
název	název	k1gInSc1
předcházející	předcházející	k2eAgInSc1d1
a	a	k8xC
následující	následující	k2eAgInSc1d1
po	po	k7c6
názvu	název	k1gInSc6
ČSLA	ČSLA	kA
<g/>
)	)	kIx)
</s>
<s>
Armáda	armáda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Armáda	armáda	k1gFnSc1
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Dům	dům	k1gInSc1
armády	armáda	k1gFnSc2
</s>
<s>
Označení	označení	k1gNnSc1
letadel	letadlo	k1gNnPc2
používaných	používaný	k2eAgMnPc2d1
v	v	k7c6
Československu	Československo	k1gNnSc6
po	po	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
</s>
<s>
Pomocné	pomocný	k2eAgInPc1d1
technické	technický	k2eAgInPc1d1
prapory	prapor	k1gInPc1
</s>
<s>
Zpravodajská	zpravodajský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
ČSLA	ČSLA	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
</s>
<s>
Technika	technika	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Czechoslovak	Czechoslovak	k1gMnSc1
People	People	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Army	Army	k1gInPc7
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
revize	revize	k1gFnSc2
nebylo	být	k5eNaImAgNnS
určeno	určen	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
Šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Překlad	překlad	k1gInSc1
<g/>
}}	}}	k?
požaduje	požadovat	k5eAaImIp3nS
zadat	zadat	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
do	do	k7c2
parametru	parametr	k1gInSc2
„	„	k?
<g/>
revize	revize	k1gFnSc1
<g/>
“	“	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
