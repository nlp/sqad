<s>
Piazza	Piazza	k1gFnSc1	Piazza
di	di	k?	di
Spagna	Spagna	k1gFnSc1	Spagna
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
vrchem	vrch	k1gInSc7	vrch
Monte	Mont	k1gInSc5	Mont
Pincio	Pincio	k1gNnSc4	Pincio
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Říma	Řím	k1gInSc2	Řím
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
vedou	vést	k5eAaImIp3nP	vést
známé	známý	k2eAgInPc4d1	známý
Španělské	španělský	k2eAgInPc4d1	španělský
schody	schod	k1gInPc4	schod
vzhůru	vzhůru	k6eAd1	vzhůru
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
Trinita	Trinita	k1gFnSc1	Trinita
dei	dei	k?	dei
Monti	Monť	k1gFnSc2	Monť
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1723	[number]	k4	1723
<g/>
–	–	k?	–
<g/>
1726	[number]	k4	1726
</s>
