<s>
Přívalový	přívalový	k2eAgInSc1d1	přívalový
déšť	déšť	k1gInSc1	déšť
(	(	kIx(	(
<g/>
liják	liják	k1gInSc1	liják
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
deště	dešť	k1gInSc2	dešť
s	s	k7c7	s
obrovským	obrovský	k2eAgNnSc7d1	obrovské
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spadnou	spadnout	k5eAaPmIp3nP	spadnout
za	za	k7c4	za
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
přívalové	přívalový	k2eAgFnPc1d1	přívalová
neboli	neboli	k8xC	neboli
bleskové	bleskový	k2eAgFnPc1d1	blesková
povodně	povodeň	k1gFnPc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Dešťové	dešťový	k2eAgFnPc1d1	dešťová
kapky	kapka	k1gFnPc1	kapka
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
bývají	bývat	k5eAaImIp3nP	bývat
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
běžných	běžný	k2eAgFnPc2d1	běžná
kapek	kapka	k1gFnPc2	kapka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
ke	k	k7c3	k
krupobití	krupobití	k1gNnSc3	krupobití
<g/>
.	.	kIx.	.
</s>
<s>
Přívalové	přívalový	k2eAgInPc1d1	přívalový
deště	dešť	k1gInPc1	dešť
v	v	k7c6	v
jarním	jarní	k2eAgNnSc6d1	jarní
a	a	k8xC	a
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
poklesem	pokles	k1gInSc7	pokles
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
studená	studený	k2eAgFnSc1d1	studená
fronta	fronta	k1gFnSc1	fronta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
meteorologické	meteorologický	k2eAgInPc1d1	meteorologický
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obvykle	obvykle	k6eAd1	obvykle
bývají	bývat	k5eAaImIp3nP	bývat
doprovázeny	doprovázet	k5eAaImNgFnP	doprovázet
i	i	k8xC	i
silným	silný	k2eAgInSc7d1	silný
nárazovým	nárazový	k2eAgInSc7d1	nárazový
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
vítr	vítr	k1gInSc1	vítr
pak	pak	k6eAd1	pak
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
vody	voda	k1gFnSc2	voda
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
komplikací	komplikace	k1gFnPc2	komplikace
například	například	k6eAd1	například
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Blesky	blesk	k1gInPc1	blesk
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
i	i	k9	i
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
požárů	požár	k1gInPc2	požár
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
lokálním	lokální	k2eAgFnPc3d1	lokální
záplavám	záplava	k1gFnPc3	záplava
v	v	k7c6	v
níže	nízce	k6eAd2	nízce
položených	položený	k2eAgNnPc6d1	položené
místech	místo	k1gNnPc6	místo
nebo	nebo	k8xC	nebo
i	i	k9	i
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
intravilánu	intravilán	k1gInSc6	intravilán
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kanalizace	kanalizace	k1gFnSc1	kanalizace
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
najednou	najednou	k6eAd1	najednou
odvést	odvést	k5eAaPmF	odvést
pryč	pryč	k6eAd1	pryč
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
masa	masa	k1gFnSc1	masa
vody	voda	k1gFnSc2	voda
pak	pak	k6eAd1	pak
odtéká	odtékat	k5eAaImIp3nS	odtékat
i	i	k9	i
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
nemůže	moct	k5eNaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průvodním	průvodní	k2eAgInSc7d1	průvodní
jevem	jev	k1gInSc7	jev
přívalových	přívalový	k2eAgInPc2d1	přívalový
dešťů	dešť	k1gInPc2	dešť
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
zatopené	zatopený	k2eAgInPc4d1	zatopený
sklepy	sklep	k1gInPc4	sklep
<g/>
,	,	kIx,	,
podchody	podchod	k1gInPc4	podchod
<g/>
,	,	kIx,	,
podjezdy	podjezd	k1gInPc4	podjezd
<g/>
,	,	kIx,	,
jámy	jáma	k1gFnPc4	jáma
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
popadané	popadaný	k2eAgInPc1d1	popadaný
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
utržené	utržený	k2eAgFnPc1d1	utržená
větve	větev	k1gFnPc1	větev
<g/>
,	,	kIx,	,
stržené	stržený	k2eAgFnPc1d1	stržená
střechy	střecha	k1gFnPc1	střecha
<g/>
,	,	kIx,	,
ucpaná	ucpaný	k2eAgFnSc1d1	ucpaná
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
,	,	kIx,	,
poškozené	poškozený	k2eAgMnPc4d1	poškozený
části	část	k1gFnSc2	část
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
poškozené	poškozený	k2eAgInPc4d1	poškozený
venkovní	venkovní	k2eAgInPc4d1	venkovní
rozvody	rozvod	k1gInPc4	rozvod
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
atd.	atd.	kA	atd.
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
odpoledne	odpoledne	k1gNnSc4	odpoledne
přívalový	přívalový	k2eAgInSc1d1	přívalový
déšť	déšť	k1gInSc1	déšť
spláchl	spláchnout	k5eAaPmAgInS	spláchnout
ornici	ornice	k1gFnSc4	ornice
a	a	k8xC	a
bahno	bahno	k1gNnSc4	bahno
z	z	k7c2	z
polí	pole	k1gFnPc2	pole
nad	nad	k7c7	nad
vsí	ves	k1gFnSc7	ves
Olešná	Olešný	k2eAgNnPc4d1	Olešné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
středy	středa	k1gFnSc2	středa
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
přehnala	přehnat	k5eAaPmAgFnS	přehnat
bouřka	bouřka	k1gFnSc1	bouřka
s	s	k7c7	s
přívalovým	přívalový	k2eAgInSc7d1	přívalový
deštěm	dešť	k1gInSc7	dešť
nad	nad	k7c7	nad
Rokycanskem	Rokycansko	k1gNnSc7	Rokycansko
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
vsí	ves	k1gFnSc7	ves
Němčovice	Němčovice	k1gFnSc2	Němčovice
protekla	protéct	k5eAaPmAgFnS	protéct
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
ornice	ornice	k1gFnSc1	ornice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nevsákla	vsáknout	k5eNaPmAgFnS	vsáknout
na	na	k7c4	na
kukuřičného	kukuřičný	k2eAgMnSc4d1	kukuřičný
pole	pole	k1gNnPc1	pole
nad	nad	k7c7	nad
vsí	ves	k1gFnSc7	ves
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
zaplavil	zaplavit	k5eAaPmAgInS	zaplavit
přívalový	přívalový	k2eAgInSc1d1	přívalový
déšť	déšť	k1gInSc1	déšť
část	část	k1gFnSc1	část
pražské	pražský	k2eAgFnSc2d1	Pražská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Karlín	Karlín	k1gInSc1	Karlín
<g/>
.	.	kIx.	.
</s>
<s>
Přívalová	přívalový	k2eAgFnSc1d1	přívalová
povodeň	povodeň	k1gFnSc1	povodeň
</s>
