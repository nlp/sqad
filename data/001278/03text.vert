<s>
Fluor	fluor	k1gInSc1	fluor
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Fluorum	Fluorum	k1gNnSc1	Fluorum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekovový	kovový	k2eNgInSc4d1	nekovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
,	,	kIx,	,
zelenožlutý	zelenožlutý	k2eAgInSc1d1	zelenožlutý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
mimořádně	mimořádně	k6eAd1	mimořádně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
elektronegativitou	elektronegativita	k1gFnSc7	elektronegativita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejlehčím	lehký	k2eAgInSc7d3	nejlehčí
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
halogenů	halogen	k1gInPc2	halogen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
lidé	člověk	k1gMnPc1	člověk
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
vysoké	vysoký	k2eAgFnSc3d1	vysoká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
Henrimu	Henrim	k1gInSc2	Henrim
Moissanovi	Moissan	k1gMnSc6	Moissan
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
chlazené	chlazený	k2eAgFnSc2d1	chlazená
směsi	směs	k1gFnSc2	směs
KHF2	KHF2	k1gFnSc2	KHF2
v	v	k7c6	v
HF	HF	kA	HF
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výrobu	výroba	k1gFnSc4	výroba
fluoru	fluor	k1gInSc2	fluor
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
nevelkém	velký	k2eNgNnSc6d1	nevelké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
minerály	minerál	k1gInPc4	minerál
fluoru	fluor	k1gInSc2	fluor
jsou	být	k5eAaImIp3nP	být
kazivec	kazivec	k1gInSc4	kazivec
CaF	CaF	k1gFnSc2	CaF
<g/>
2	[number]	k4	2
a	a	k8xC	a
fluorapatit	fluorapatit	k5eAaImF	fluorapatit
Ca	ca	kA	ca
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
výrobě	výroba	k1gFnSc3	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoku	roztok	k1gInSc2	roztok
KHF2	KHF2	k1gFnPc2	KHF2
v	v	k7c6	v
HF	HF	kA	HF
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
extrémní	extrémní	k2eAgFnSc3d1	extrémní
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
fluor	fluor	k1gInSc1	fluor
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
ihned	ihned	k6eAd1	ihned
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
skladováním	skladování	k1gNnSc7	skladování
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
využívá	využívat	k5eAaPmIp3nS	využívat
kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc1d1	základní
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
chemikálie	chemikálie	k1gFnSc1	chemikálie
<g/>
,	,	kIx,	,
kryolit	kryolit	k1gInSc1	kryolit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
teploty	teplota	k1gFnSc2	teplota
tání	tání	k1gNnSc1	tání
bauxitu	bauxit	k1gInSc2	bauxit
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
fluorid	fluorid	k1gInSc1	fluorid
uranový	uranový	k2eAgInSc1d1	uranový
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
izotopů	izotop	k1gInPc2	izotop
uranu	uran	k1gInSc2	uran
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
využívá	využívat	k5eAaImIp3nS	využívat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
teflonu	teflon	k1gInSc2	teflon
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
syntetických	syntetický	k2eAgInPc2d1	syntetický
organických	organický	k2eAgInPc2d1	organický
polymerů	polymer	k1gInPc2	polymer
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
k	k	k7c3	k
biogenním	biogenní	k2eAgInPc3d1	biogenní
prvkům	prvek	k1gInPc3	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
a	a	k8xC	a
zubech	zub	k1gInPc6	zub
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc4	plyn
v	v	k7c6	v
silné	silný	k2eAgFnSc6d1	silná
vrstvě	vrstva	k1gFnSc6	vrstva
zelenožlutý	zelenožlutý	k2eAgInSc1d1	zelenožlutý
s	s	k7c7	s
dráždivým	dráždivý	k2eAgInSc7d1	dráždivý
zápachem	zápach	k1gInSc7	zápach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
a	a	k8xC	a
toxický	toxický	k2eAgInSc1d1	toxický
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leptá	leptat	k5eAaImIp3nS	leptat
dokonce	dokonce	k9	dokonce
i	i	k9	i
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Kapalný	kapalný	k2eAgInSc1d1	kapalný
fluor	fluor	k1gInSc1	fluor
má	mít	k5eAaImIp3nS	mít
banánově	banánově	k6eAd1	banánově
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Disociační	disociační	k2eAgFnSc1d1	disociační
enthalpie	enthalpie	k1gFnSc1	enthalpie
molekuly	molekula	k1gFnSc2	molekula
fluoru	fluor	k1gInSc2	fluor
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
disociační	disociační	k2eAgFnSc3d1	disociační
enthalpii	enthalpie	k1gFnSc3	enthalpie
molekuly	molekula	k1gFnSc2	molekula
jodu	jod	k1gInSc2	jod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
malou	malý	k2eAgFnSc7d1	malá
pevností	pevnost	k1gFnSc7	pevnost
vazby	vazba	k1gFnSc2	vazba
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
fluoru	fluor	k1gInSc2	fluor
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnSc1d2	menší
překryv	překrýt	k5eAaPmDgInS	překrýt
vazebných	vazebný	k2eAgInPc2d1	vazebný
orbitalů	orbital	k1gInPc2	orbital
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
větším	veliký	k2eAgNnSc7d2	veliký
odpuzováním	odpuzování	k1gNnSc7	odpuzování
atomů	atom	k1gInPc2	atom
fluoru	fluor	k1gInSc2	fluor
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
odpuzování	odpuzování	k1gNnSc3	odpuzování
volných	volný	k2eAgInPc2d1	volný
elektronových	elektronový	k2eAgInPc2d1	elektronový
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
sloučeninách	sloučenina	k1gFnPc6	sloučenina
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
oxidační	oxidační	k2eAgNnSc1d1	oxidační
číslo	číslo	k1gNnSc1	číslo
F	F	kA	F
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
fluor	fluor	k1gInSc4	fluor
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
komplexech	komplex	k1gInPc6	komplex
oxidační	oxidační	k2eAgFnPc1d1	oxidační
číslo	číslo	k1gNnSc4	číslo
F0	F0	k1gFnSc2	F0
a	a	k8xC	a
opravdu	opravdu	k6eAd1	opravdu
pouze	pouze	k6eAd1	pouze
formální	formální	k2eAgNnSc4d1	formální
oxidační	oxidační	k2eAgNnSc4d1	oxidační
číslo	číslo	k1gNnSc4	číslo
F	F	kA	F
<g/>
+	+	kIx~	+
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorné	fluorný	k2eAgFnSc6d1	fluorný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
oxidační	oxidační	k2eAgNnSc4d1	oxidační
číslo	číslo	k1gNnSc4	číslo
F	F	kA	F
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
elektronegativitu	elektronegativita	k1gFnSc4	elektronegativita
než	než	k8xS	než
vodík	vodík	k1gInSc4	vodík
i	i	k8xC	i
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
halogeny	halogen	k1gInPc4	halogen
obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nP	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
halogen	halogen	k1gInSc1	halogen
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
(	(	kIx(	(
<g/>
lehčí	lehký	k2eAgMnSc1d2	lehčí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vytěsnit	vytěsnit	k5eAaPmF	vytěsnit
halogen	halogen	k1gInSc4	halogen
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
(	(	kIx(	(
<g/>
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
)	)	kIx)	)
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
halogenidu	halogenid	k1gInSc2	halogenid
<g/>
.	.	kIx.	.
</s>
<s>
Lehčí	lehký	k2eAgInSc1d2	lehčí
halogen	halogen	k1gInSc1	halogen
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
halogenid	halogenid	k1gInSc4	halogenid
a	a	k8xC	a
těžší	těžký	k2eAgInSc4d2	těžší
halogen	halogen	k1gInSc4	halogen
z	z	k7c2	z
halogenidu	halogenid	k1gInSc2	halogenid
v	v	k7c4	v
halogen	halogen	k1gInSc4	halogen
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
převážně	převážně	k6eAd1	převážně
iontové	iontový	k2eAgFnPc1d1	iontová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
iontovou	iontový	k2eAgFnSc7d1	iontová
vazbou	vazba	k1gFnSc7	vazba
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
sloučeninami	sloučenina	k1gFnPc7	sloučenina
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kovalentní	kovalentní	k2eAgFnSc4d1	kovalentní
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ochotně	ochotně	k6eAd1	ochotně
až	až	k9	až
explozivně	explozivně	k6eAd1	explozivně
slučuje	slučovat	k5eAaImIp3nS	slučovat
již	již	k9	již
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
bromem	brom	k1gInSc7	brom
<g/>
,	,	kIx,	,
jodem	jod	k1gInSc7	jod
<g/>
,	,	kIx,	,
sírou	síra	k1gFnSc7	síra
<g/>
,	,	kIx,	,
fosforem	fosfor	k1gInSc7	fosfor
<g/>
,	,	kIx,	,
arsenem	arsen	k1gInSc7	arsen
<g/>
,	,	kIx,	,
antimonem	antimon	k1gInSc7	antimon
<g/>
,	,	kIx,	,
borem	bor	k1gInSc7	bor
<g/>
,	,	kIx,	,
křemíkem	křemík	k1gInSc7	křemík
a	a	k8xC	a
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kovy	kov	k1gInPc1	kov
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
teplot	teplota	k1gFnPc2	teplota
nebo	nebo	k8xC	nebo
při	při	k7c6	při
mírném	mírný	k2eAgNnSc6d1	mírné
zahřátí	zahřátí	k1gNnSc6	zahřátí
jen	jen	k9	jen
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
povlak	povlak	k1gInSc1	povlak
brání	bránit	k5eAaImIp3nS	bránit
další	další	k2eAgFnSc4d1	další
reakci	reakce	k1gFnSc4	reakce
-	-	kIx~	-
pasivace	pasivace	k1gFnSc2	pasivace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
silnějším	silný	k2eAgNnSc6d2	silnější
zahřívání	zahřívání	k1gNnSc6	zahřívání
reakce	reakce	k1gFnSc2	reakce
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
nebo	nebo	k8xC	nebo
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
vzplanou	vzplanout	k5eAaPmIp3nP	vzplanout
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
červeného	červený	k2eAgInSc2d1	červený
žáru	žár	k1gInSc2	žár
působí	působit	k5eAaImIp3nS	působit
fluor	fluor	k1gInSc1	fluor
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c4	na
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
platinu	platina	k1gFnSc4	platina
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
fluoru	fluor	k1gInSc2	fluor
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
fluorovodík	fluorovodík	k1gInSc1	fluorovodík
a	a	k8xC	a
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
ozonu	ozon	k1gInSc2	ozon
<g/>
,	,	kIx,	,
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
však	však	k8xC	však
působením	působení	k1gNnSc7	působení
fluoru	fluor	k1gInSc2	fluor
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
fluorovodík	fluorovodík	k1gInSc1	fluorovodík
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
fluorná	fluorný	k2eAgFnSc1d1	fluorný
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
silné	silný	k2eAgFnSc3d1	silná
reaktivitě	reaktivita	k1gFnSc3	reaktivita
vytěsňuje	vytěsňovat	k5eAaImIp3nS	vytěsňovat
fluor	fluor	k1gInSc1	fluor
většinu	většina	k1gFnSc4	většina
anionů	anion	k1gMnPc2	anion
ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
anion	anion	k1gInSc4	anion
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
-	-	kIx~	-
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
chemickou	chemický	k2eAgFnSc7d1	chemická
podstatou	podstata	k1gFnSc7	podstata
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
fluoridu	fluorid	k1gInSc2	fluorid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
fluor	fluor	k1gInSc1	fluor
extrémně	extrémně	k6eAd1	extrémně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
jej	on	k3xPp3gMnSc4	on
připravit	připravit	k5eAaPmF	připravit
ani	ani	k8xC	ani
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
skladovat	skladovat	k5eAaImF	skladovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známý	k2eAgFnSc1d1	známá
doložená	doložený	k2eAgFnSc1d1	doložená
sloučenina	sloučenina	k1gFnSc1	sloučenina
fluoru	fluor	k1gInSc2	fluor
je	být	k5eAaImIp3nS	být
kazivec	kazivec	k1gInSc1	kazivec
(	(	kIx(	(
<g/>
fluorit	fluorit	k1gInSc1	fluorit
<g/>
)	)	kIx)	)
CaF	CaF	k1gFnSc1	CaF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
Georgius	Georgius	k1gInSc4	Georgius
Agricola	Agricola	k1gFnSc1	Agricola
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
jako	jako	k8xC	jako
tavidlo	tavidlo	k1gNnSc4	tavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schwanhard	Schwanhard	k1gMnSc1	Schwanhard
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
působením	působení	k1gNnSc7	působení
silné	silný	k2eAgFnSc2d1	silná
kyseliny	kyselina	k1gFnSc2	kyselina
na	na	k7c4	na
kazivec	kazivec	k1gInSc4	kazivec
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
kyselé	kyselý	k2eAgInPc1d1	kyselý
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
leptají	leptat	k5eAaImIp3nP	leptat
sklo	sklo	k1gNnSc4	sklo
-	-	kIx~	-
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
fluorovodík	fluorovodík	k1gInSc1	fluorovodík
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
uměleckému	umělecký	k2eAgNnSc3d1	umělecké
leptání	leptání	k1gNnSc3	leptání
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1678	[number]	k4	1678
popsal	popsat	k5eAaPmAgMnS	popsat
J.	J.	kA	J.
S.	S.	kA	S.
Elsholtz	Elsholtz	k1gMnSc1	Elsholtz
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
kazivce	kazivec	k1gInSc2	kazivec
modrobílé	modrobílý	k2eAgFnSc2d1	modrobílá
světélkování	světélkování	k1gNnSc3	světélkování
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1750	[number]	k4	1750
popisuje	popisovat	k5eAaImIp3nS	popisovat
Johan	Johan	k1gMnSc1	Johan
Gottschalk	Gottschalk	k1gMnSc1	Gottschalk
Wallerius	Wallerius	k1gMnSc1	Wallerius
stejný	stejný	k2eAgInSc4d1	stejný
jev	jev	k1gInSc4	jev
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
George	Georg	k1gInSc2	Georg
Gabriel	Gabriel	k1gMnSc1	Gabriel
Stokes	Stokes	k1gMnSc1	Stokes
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
název	název	k1gInSc1	název
fluorescence	fluorescence	k1gFnSc1	fluorescence
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
provedl	provést	k5eAaPmAgInS	provést
Andreas	Andreas	k1gInSc1	Andreas
Sigismund	Sigismund	k1gInSc1	Sigismund
Marggraf	Marggraf	k1gInSc1	Marggraf
první	první	k4xOgInPc4	první
chemické	chemický	k2eAgInPc4d1	chemický
pokusy	pokus	k1gInPc4	pokus
s	s	k7c7	s
kazivcem	kazivec	k1gInSc7	kazivec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
našel	najít	k5eAaPmAgMnS	najít
D.	D.	kA	D.
P.	P.	kA	P.
Morichini	Morichin	k2eAgMnPc1d1	Morichin
fluoridy	fluorid	k1gInPc4	fluorid
ve	v	k7c6	v
vápenatých	vápenatý	k2eAgFnPc6d1	vápenatá
fosíliích	fosílie	k1gFnPc6	fosílie
a	a	k8xC	a
v	v	k7c6	v
zubech	zub	k1gInPc6	zub
a	a	k8xC	a
Jöns	Jöns	k1gInSc4	Jöns
Jacob	Jacoba	k1gFnPc2	Jacoba
Berzelius	Berzelius	k1gMnSc1	Berzelius
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
<g/>
.	.	kIx.	.
</s>
<s>
André-Marie	André-Marie	k1gFnSc1	André-Marie
Ampè	Ampè	k1gFnSc2	Ampè
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1812	[number]	k4	1812
název	název	k1gInSc1	název
nového	nový	k2eAgInSc2d1	nový
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
součástí	součást	k1gFnSc7	součást
kazivce	kazivec	k1gInSc2	kazivec
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
le	le	k?	le
fluore	fluor	k1gInSc5	fluor
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
fleue	fleu	k1gInSc2	fleu
-	-	kIx~	-
téci	téct	k5eAaImF	téct
<g/>
)	)	kIx)	)
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
jej	on	k3xPp3gMnSc4	on
Humphry	Humphra	k1gFnPc4	Humphra
Davymu	Davym	k1gInSc2	Davym
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1813	[number]	k4	1813
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nedařila	dařit	k5eNaImAgFnS	dařit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
přípravě	příprava	k1gFnSc6	příprava
plyn	plyn	k1gInSc1	plyn
zreagoval	zreagovat	k5eAaPmAgInS	zreagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
se	s	k7c7	s
stěnami	stěna	k1gFnPc7	stěna
nádob	nádoba	k1gFnPc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
fluor	fluor	k1gInSc4	fluor
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
až	až	k9	až
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1886	[number]	k4	1886
Henrimu	Henrim	k1gInSc2	Henrim
Moissanovi	Moissan	k1gMnSc3	Moissan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
připravil	připravit	k5eAaPmAgMnS	připravit
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
chlazeného	chlazený	k2eAgInSc2d1	chlazený
roztoku	roztok	k1gInSc2	roztok
kyselého	kyselý	k2eAgInSc2d1	kyselý
fluoridu	fluorid	k1gInSc2	fluorid
draselného	draselný	k2eAgInSc2d1	draselný
KHF	KHF	kA	KHF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
rozpuštěného	rozpuštěný	k2eAgNnSc2d1	rozpuštěné
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
kapalném	kapalný	k2eAgInSc6d1	kapalný
fluorovodíku	fluorovodík	k1gInSc6	fluorovodík
v	v	k7c6	v
přístroji	přístroj	k1gInSc6	přístroj
s	s	k7c7	s
platinovými	platinový	k2eAgFnPc7d1	platinová
a	a	k8xC	a
iridiovými	iridiový	k2eAgFnPc7d1	iridiová
elektrodami	elektroda	k1gFnPc7	elektroda
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
U	U	kA	U
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
těsně	těsně	k6eAd1	těsně
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
zátkou	zátka	k1gFnSc7	zátka
z	z	k7c2	z
fluoridu	fluorid	k1gInSc2	fluorid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
CaF	CaF	k1gFnPc7	CaF
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
plyn	plyn	k1gInSc1	plyn
reagoval	reagovat	k5eAaBmAgInS	reagovat
s	s	k7c7	s
křemíkem	křemík	k1gInSc7	křemík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plamene	plamen	k1gInSc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
se	se	k3xPyFc4	se
podařila	podařit	k5eAaPmAgFnS	podařit
po	po	k7c6	po
74	[number]	k4	74
neúspěšných	úspěšný	k2eNgNnPc6d1	neúspěšné
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Henri	Henri	k6eAd1	Henri
Moissan	Moissan	k1gMnSc1	Moissan
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
objev	objev	k1gInSc4	objev
fluoru	fluor	k1gInSc2	fluor
a	a	k8xC	a
vynález	vynález	k1gInSc1	vynález
elektrické	elektrický	k2eAgFnSc2d1	elektrická
pece	pec	k1gFnSc2	pec
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
)	)	kIx)	)
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
chemické	chemický	k2eAgFnPc1d1	chemická
manufaktury	manufaktura	k1gFnPc1	manufaktura
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
kryolitu	kryolit	k1gInSc2	kryolit
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
AlF	alfa	k1gFnPc2	alfa
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
tavidlo	tavidlo	k1gNnSc1	tavidlo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
Thomas	Thomas	k1gMnSc1	Thomas
Midgley	Midglea	k1gFnPc4	Midglea
<g/>
,	,	kIx,	,
A.	A.	kA	A.
L.	L.	kA	L.
Henne	Henn	k1gInSc5	Henn
a	a	k8xC	a
R.	R.	kA	R.
R.	R.	kA	R.
McNary	McNara	k1gFnSc2	McNara
připravili	připravit	k5eAaPmAgMnP	připravit
freon	freon	k1gInSc4	freon
CCl	CCl	k1gFnSc2	CCl
<g/>
2	[number]	k4	2
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nehořlavý	hořlavý	k2eNgInSc1d1	nehořlavý
a	a	k8xC	a
netoxický	toxický	k2eNgInSc1d1	netoxický
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
chladírenském	chladírenský	k2eAgInSc6d1	chladírenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
připravili	připravit	k5eAaPmAgMnP	připravit
Otto	Otto	k1gMnSc1	Otto
Ruff	Ruff	k1gMnSc1	Ruff
a	a	k8xC	a
R.	R.	kA	R.
Keim	Keim	k1gInSc4	Keim
interhalogenidy	interhalogenida	k1gFnSc2	interhalogenida
fluoru	fluor	k1gInSc2	fluor
(	(	kIx(	(
<g/>
sloučeniny	sloučenina	k1gFnSc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
halogenidy	halogenid	k1gInPc7	halogenid
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
poslední	poslední	k2eAgFnSc1d1	poslední
interhalogenidní	interhalogenidní	k2eAgFnSc1d1	interhalogenidní
sloučenina	sloučenina	k1gFnSc1	sloučenina
W.	W.	kA	W.
Mayaou	Mayaa	k1gFnSc7	Mayaa
-	-	kIx~	-
ClF	ClF	k1gFnSc1	ClF
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
připravil	připravit	k5eAaPmAgMnS	připravit
Roy	Roy	k1gMnSc1	Roy
J.	J.	kA	J.
Plunkett	Plunkett	k1gInSc1	Plunkett
teflon	teflon	k1gInSc1	teflon
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
připravena	připravit	k5eAaPmNgFnS	připravit
kyselina	kyselina	k1gFnSc1	kyselina
fluorná	fluorný	k2eAgFnSc1d1	fluorný
HOF	HOF	kA	HOF
ve	v	k7c6	v
vážitelném	vážitelný	k2eAgNnSc6d1	vážitelné
množství	množství	k1gNnSc6	množství
S.	S.	kA	S.
Rozenem	Rozeno	k1gNnSc7	Rozeno
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
se	se	k3xPyFc4	se
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
13	[number]	k4	13
<g/>
.	.	kIx.	.
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
prvek	prvek	k1gInSc1	prvek
(	(	kIx(	(
<g/>
na	na	k7c6	na
dvanáctém	dvanáctý	k4xOgInSc6	dvanáctý
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
mangan	mangan	k1gInSc1	mangan
1060	[number]	k4	1060
ppm	ppm	k?	ppm
a	a	k8xC	a
na	na	k7c6	na
čtrnáctém	čtrnáctý	k4xOgInSc6	čtrnáctý
místě	místo	k1gNnSc6	místo
baryum	baryum	k1gNnSc1	baryum
390	[number]	k4	390
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
fluor	fluor	k1gInSc1	fluor
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
544	[number]	k4	544
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
oceánů	oceán	k1gInPc2	oceán
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
mg	mg	kA	mg
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
fluoridů	fluorid	k1gInPc2	fluorid
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
není	být	k5eNaImIp3nS	být
fluor	fluor	k1gInSc1	fluor
příliš	příliš	k6eAd1	příliš
bohatě	bohatě	k6eAd1	bohatě
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
fluoru	fluor	k1gInSc2	fluor
připadá	připadat	k5eAaPmIp3nS	připadat
přes	přes	k7c4	přes
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
fluor	fluor	k1gInSc1	fluor
přítomen	přítomen	k2eAgInSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
nejvýznamnějším	významný	k2eAgInPc3d3	nejvýznamnější
minerálům	minerál	k1gInPc3	minerál
patří	patřit	k5eAaImIp3nS	patřit
fluorit	fluorit	k1gInSc1	fluorit
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
kazivec	kazivec	k1gInSc1	kazivec
<g/>
)	)	kIx)	)
CaF	CaF	k1gFnSc1	CaF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
kryolit	kryolit	k1gInSc1	kryolit
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
AlF	alfa	k1gFnPc2	alfa
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
a	a	k8xC	a
fluoroapatit	fluoroapatit	k5eAaImF	fluoroapatit
Ca	ca	kA	ca
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kryolit	kryolit	k1gInSc1	kryolit
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vzácným	vzácný	k2eAgInPc3d1	vzácný
minerálům	minerál	k1gInPc3	minerál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
množství	množství	k1gNnSc4	množství
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
topazu	topaz	k1gInSc6	topaz
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnPc2	SiO
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
sellaitu	sellait	k2eAgFnSc4d1	sellait
MgF	MgF	k1gFnSc4	MgF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
villiaumitu	villiaumita	k1gFnSc4	villiaumita
NaF	NaF	k1gFnSc2	NaF
<g/>
,	,	kIx,	,
bastnezitu	bastnezit	k1gInSc2	bastnezit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
carobbiit	carobbiit	k2eAgMnSc1d1	carobbiit
KF	KF	kA	KF
<g/>
,	,	kIx,	,
frankdicksonit	frankdicksonit	k1gInSc1	frankdicksonit
BaF	baf	k0	baf
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
griceit	griceit	k1gMnSc1	griceit
LiF	LiF	k1gMnSc1	LiF
<g/>
,	,	kIx,	,
tveitit-	tveitit-	k?	tveitit-
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
Ca	ca	kA	ca
<g/>
14	[number]	k4	14
<g/>
Y	Y	kA	Y
<g/>
5	[number]	k4	5
<g/>
F	F	kA	F
<g/>
43	[number]	k4	43
a	a	k8xC	a
zavarickit	zavarickit	k1gMnSc1	zavarickit
BiOF	BiOF	k1gMnSc1	BiOF
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
plynného	plynný	k2eAgInSc2d1	plynný
fluoru	fluor	k1gInSc2	fluor
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
značně	značně	k6eAd1	značně
obtížná	obtížný	k2eAgFnSc1d1	obtížná
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
reaktivitě	reaktivita	k1gFnSc3	reaktivita
volného	volný	k2eAgInSc2d1	volný
fluoru	fluor	k1gInSc2	fluor
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
riziková	rizikový	k2eAgFnSc1d1	riziková
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
elektronové	elektronový	k2eAgFnSc3d1	elektronová
afinitě	afinita	k1gFnSc3	afinita
fluoru	fluor	k1gInSc2	fluor
lze	lze	k6eAd1	lze
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
pouze	pouze	k6eAd1	pouze
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
výroba	výroba	k1gFnSc1	výroba
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
zdařila	zdařit	k5eAaPmAgFnS	zdařit
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
Henrimu	Henrim	k1gInSc2	Henrim
Moissanovi	Moissan	k1gMnSc3	Moissan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
připravil	připravit	k5eAaPmAgMnS	připravit
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
chlazeného	chlazený	k2eAgInSc2d1	chlazený
roztoku	roztok	k1gInSc2	roztok
hydrofluoridu	hydrofluorid	k1gInSc2	hydrofluorid
draselného	draselný	k2eAgInSc2d1	draselný
KHF	KHF	kA	KHF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
rozpuštěného	rozpuštěný	k2eAgNnSc2d1	rozpuštěné
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
kapalném	kapalný	k2eAgInSc6d1	kapalný
fluorovodíku	fluorovodík	k1gInSc6	fluorovodík
v	v	k7c6	v
přístroji	přístroj	k1gInSc6	přístroj
s	s	k7c7	s
platinovými	platinový	k2eAgFnPc7d1	platinová
a	a	k8xC	a
iridiovými	iridiový	k2eAgFnPc7d1	iridiová
elektrodami	elektroda	k1gFnPc7	elektroda
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
U	U	kA	U
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
těsně	těsně	k6eAd1	těsně
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
zátkou	zátka	k1gFnSc7	zátka
z	z	k7c2	z
fluoridu	fluorid	k1gInSc2	fluorid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
CaF	CaF	k1gFnPc7	CaF
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
Argo	Argo	k6eAd1	Argo
nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
fluoru	fluor	k1gInSc2	fluor
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
taveniny	tavenina	k1gFnSc2	tavenina
hydrofluoridu	hydrofluorid	k1gInSc2	hydrofluorid
draselného	draselný	k2eAgMnSc4d1	draselný
KHF2	KHF2	k1gMnSc4	KHF2
při	při	k7c6	při
240	[number]	k4	240
°	°	k?	°
<g/>
C	C	kA	C
-	-	kIx~	-
250	[number]	k4	250
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
elektricky	elektricky	k6eAd1	elektricky
vyhřívané	vyhřívaný	k2eAgFnSc6d1	vyhřívaná
měděné	měděný	k2eAgFnSc6d1	měděná
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
katodou	katoda	k1gFnSc7	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Anoda	anoda	k1gFnSc1	anoda
je	být	k5eAaImIp3nS	být
grafitová	grafitový	k2eAgFnSc1d1	grafitová
tyč	tyč	k1gFnSc1	tyč
obklopená	obklopený	k2eAgFnSc1d1	obklopená
diafragmou	diafragma	k1gFnSc7	diafragma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
přístupu	přístup	k1gInSc3	přístup
vodíku	vodík	k1gInSc2	vodík
k	k	k7c3	k
anodě	anoda	k1gFnSc3	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
dihydrofluorid	dihydrofluorid	k1gInSc1	dihydrofluorid
draselný	draselný	k2eAgInSc1d1	draselný
KH2F3	KH2F3	k1gFnPc7	KH2F3
namísto	namísto	k7c2	namísto
hydrofluoridu	hydrofluorid	k1gInSc2	hydrofluorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
teplota	teplota	k1gFnSc1	teplota
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
,	,	kIx,	,
a	a	k8xC	a
měděná	měděný	k2eAgFnSc1d1	měděná
nádoba	nádoba	k1gFnSc1	nádoba
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
hořčíkovou	hořčíkový	k2eAgFnSc7d1	hořčíková
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Monelova	Monelův	k2eAgInSc2d1	Monelův
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
zavedl	zavést	k5eAaPmAgMnS	zavést
L.	L.	kA	L.
A.	A.	kA	A.
Bigelow	Bigelow	k1gFnSc1	Bigelow
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
velmi	velmi	k6eAd1	velmi
čistého	čistý	k2eAgInSc2d1	čistý
fluoru	fluor	k1gInSc2	fluor
výrobu	výroba	k1gFnSc4	výroba
z	z	k7c2	z
hydrofluoridu	hydrofluorid	k1gInSc2	hydrofluorid
draselného	draselný	k2eAgMnSc4d1	draselný
KHF2	KHF2	k1gMnSc4	KHF2
při	při	k7c6	při
250	[number]	k4	250
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
utěsněné	utěsněný	k2eAgFnSc6d1	utěsněná
niklové	niklový	k2eAgFnSc6d1	niklová
nádobě	nádoba	k1gFnSc6	nádoba
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
grafitových	grafitový	k2eAgFnPc2d1	grafitová
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
výroba	výroba	k1gFnSc1	výroba
fluoru	fluor	k1gInSc2	fluor
provádí	provádět	k5eAaImIp3nS	provádět
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
typech	typ	k1gInPc6	typ
elektrolyzérů	elektrolyzér	k1gInPc2	elektrolyzér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
se	se	k3xPyFc4	se
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
provádí	provádět	k5eAaImIp3nS	provádět
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
72	[number]	k4	72
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
elektrolyzuje	elektrolyzovat	k5eAaImIp3nS	elektrolyzovat
se	se	k3xPyFc4	se
dihydrofluorid	dihydrofluorid	k1gInSc1	dihydrofluorid
draselný	draselný	k2eAgInSc1d1	draselný
KH2F3	KH2F3	k1gMnSc7	KH2F3
a	a	k8xC	a
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
se	se	k3xPyFc4	se
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
provádí	provádět	k5eAaImIp3nS	provádět
při	při	k7c6	při
240	[number]	k4	240
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
elektrolyzuje	elektrolyzovat	k5eAaBmIp3nS	elektrolyzovat
se	se	k3xPyFc4	se
hydrofluorid	hydrofluorid	k1gInSc1	hydrofluorid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
typech	typ	k1gInPc6	typ
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
negrafitizovatelné	grafitizovatelný	k2eNgFnPc1d1	grafitizovatelný
kompaktní	kompaktní	k2eAgFnPc1d1	kompaktní
tyče	tyč	k1gFnPc1	tyč
z	z	k7c2	z
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
anody	anoda	k1gFnPc4	anoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
mají	mít	k5eAaImIp3nP	mít
plášť	plášť	k1gInSc4	plášť
z	z	k7c2	z
měkké	měkký	k2eAgFnSc2d1	měkká
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
katodou	katoda	k1gFnSc7	katoda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
typ	typ	k1gInSc1	typ
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
výhod	výhoda	k1gFnPc2	výhoda
-	-	kIx~	-
uvnitř	uvnitř	k7c2	uvnitř
komor	komora	k1gFnPc2	komora
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgInSc1d2	nižší
tlak	tlak	k1gInSc1	tlak
plynného	plynný	k2eAgInSc2d1	plynný
fluorovodíku	fluorovodík	k1gInSc2	fluorovodík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
teplotě	teplota	k1gFnSc6	teplota
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
<g/>
,	,	kIx,	,
elektrolyzér	elektrolyzér	k1gInSc1	elektrolyzér
má	mít	k5eAaImIp3nS	mít
sníženou	snížený	k2eAgFnSc4d1	snížená
možnost	možnost	k1gFnSc4	možnost
koroze	koroze	k1gFnSc2	koroze
a	a	k8xC	a
anody	anoda	k1gFnSc2	anoda
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgFnSc4d2	delší
životnost	životnost	k1gFnSc4	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Plyny	plyn	k1gInPc4	plyn
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
elektrolyzéru	elektrolyzér	k1gInSc2	elektrolyzér
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
krátkou	krátký	k2eAgFnSc7d1	krátká
přepážkou	přepážka	k1gFnSc7	přepážka
nebo	nebo	k8xC	nebo
diafragmou	diafragma	k1gFnSc7	diafragma
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorní	laboratorní	k2eAgInPc1d1	laboratorní
elektrolyzéry	elektrolyzér	k1gInPc1	elektrolyzér
pracují	pracovat	k5eAaImIp3nP	pracovat
proudu	proud	k1gInSc2	proud
při	při	k7c6	při
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
A	A	kA	A
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
elektrolyzéry	elektrolyzér	k1gInPc4	elektrolyzér
při	při	k7c6	při
proudu	proud	k1gInSc6	proud
4	[number]	k4	4
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
000	[number]	k4	000
A	A	kA	A
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
V.	V.	kA	V.
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
elektrolyzéry	elektrolyzér	k1gInPc1	elektrolyzér
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
3	[number]	k4	3
<g/>
*	*	kIx~	*
<g/>
0,8	[number]	k4	0,8
<g/>
*	*	kIx~	*
<g/>
0,6	[number]	k4	0,6
m	m	kA	m
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
až	až	k9	až
1	[number]	k4	1
t	t	k?	t
elektrolytu	elektrolyt	k1gInSc2	elektrolyt
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
1	[number]	k4	1
h	h	k?	h
provozu	provoz	k1gInSc2	provoz
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
kg	kg	kA	kg
fluoru	fluor	k1gInSc2	fluor
a	a	k8xC	a
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
podniku	podnik	k1gInSc6	podnik
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
až	až	k9	až
9	[number]	k4	9
tun	tuna	k1gFnPc2	tuna
kapalného	kapalný	k2eAgInSc2d1	kapalný
fluoru	fluor	k1gInSc2	fluor
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
fluoru	fluor	k1gInSc2	fluor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
ročně	ročně	k6eAd1	ročně
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
fluoru	fluor	k1gInSc2	fluor
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
fluor	fluor	k1gInSc1	fluor
průmyslově	průmyslově	k6eAd1	průmyslově
nevyrábí	vyrábět	k5eNaImIp3nS	vyrábět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrobny	výrobna	k1gFnPc4	výrobna
na	na	k7c4	na
menší	malý	k2eAgNnSc4d2	menší
množství	množství	k1gNnSc4	množství
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Obtížná	obtížný	k2eAgFnSc1d1	obtížná
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
výroba	výroba	k1gFnSc1	výroba
fluoru	fluor	k1gInSc2	fluor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
následné	následný	k2eAgNnSc4d1	následné
uchování	uchování	k1gNnSc4	uchování
vyrobeného	vyrobený	k2eAgInSc2d1	vyrobený
fluoru	fluor	k1gInSc2	fluor
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
kovové	kovový	k2eAgFnPc4d1	kovová
tlakové	tlakový	k2eAgFnPc4d1	tlaková
nádoby	nádoba	k1gFnPc4	nádoba
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
vrstvou	vrstva	k1gFnSc7	vrstva
teflonu	teflon	k1gInSc2	teflon
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
objem	objem	k1gInSc1	objem
2,27	[number]	k4	2,27
tun	tuna	k1gFnPc2	tuna
kapalného	kapalný	k2eAgInSc2d1	kapalný
fluoru	fluor	k1gInSc2	fluor
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
však	však	k9	však
výroba	výroba	k1gFnSc1	výroba
koncipuje	koncipovat	k5eAaBmIp3nS	koncipovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
fluor	fluor	k1gInSc1	fluor
co	co	k3yRnSc4	co
nejrychleji	rychle	k6eAd3	rychle
spotřebován	spotřebovat	k5eAaPmNgInS	spotřebovat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
fluor	fluor	k1gInSc1	fluor
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
kapalného	kapalný	k2eAgInSc2d1	kapalný
fluoru	fluor	k1gInSc2	fluor
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
směsi	směs	k1gFnSc2	směs
s	s	k7c7	s
kapalným	kapalný	k2eAgInSc7d1	kapalný
kyslíkem	kyslík	k1gInSc7	kyslík
jako	jako	k8xS	jako
okysličovadla	okysličovadlo	k1gNnPc4	okysličovadlo
v	v	k7c6	v
kapalinových	kapalinový	k2eAgInPc6d1	kapalinový
raketových	raketový	k2eAgInPc6d1	raketový
motorech	motor	k1gInPc6	motor
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vyššímu	vysoký	k2eAgInSc3d2	vyšší
dosahovanému	dosahovaný	k2eAgInSc3d1	dosahovaný
specifickému	specifický	k2eAgInSc3d1	specifický
impulsu	impuls	k1gInSc3	impuls
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
skladováním	skladování	k1gNnSc7	skladování
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
pro	pro	k7c4	pro
obsluhu	obsluha	k1gFnSc4	obsluha
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
byla	být	k5eAaImAgFnS	být
brzy	brzy	k6eAd1	brzy
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
SSSR	SSSR	kA	SSSR
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
a	a	k8xC	a
vyzkoušeny	vyzkoušen	k2eAgInPc1d1	vyzkoušen
prototypy	prototyp	k1gInPc1	prototyp
motorů	motor	k1gInPc2	motor
pracujících	pracující	k2eAgInPc2d1	pracující
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
pohonnou	pohonný	k2eAgFnSc7d1	pohonná
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
raketové	raketový	k2eAgInPc4d1	raketový
motory	motor	k1gInPc4	motor
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
studují	studovat	k5eAaImIp3nP	studovat
sloučeniny	sloučenina	k1gFnPc1	sloučenina
fluoru	fluor	k1gInSc2	fluor
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
využíval	využívat	k5eAaPmAgInS	využívat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
těžko	těžko	k6eAd1	těžko
dostupných	dostupný	k2eAgInPc2d1	dostupný
fluoridů	fluorid	k1gInPc2	fluorid
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
účinné	účinný	k2eAgNnSc1d1	účinné
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Většího	veliký	k2eAgNnSc2d2	veliký
praktického	praktický	k2eAgNnSc2d1	praktické
uplatnění	uplatnění	k1gNnSc2	uplatnění
než	než	k8xS	než
čistý	čistý	k2eAgInSc4d1	čistý
fluor	fluor	k1gInSc4	fluor
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
:	:	kIx,	:
Kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgNnPc4d1	fluorovodíkové
HF	HF	kA	HF
<g/>
,	,	kIx,	,
dodávána	dodávat	k5eAaImNgFnS	dodávat
na	na	k7c4	na
trh	trh	k1gInSc4	trh
často	často	k6eAd1	často
jako	jako	k8xC	jako
40	[number]	k4	40
<g/>
%	%	kIx~	%
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
leptá	leptat	k5eAaImIp3nS	leptat
a	a	k8xC	a
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
sklo	sklo	k1gNnSc4	sklo
a	a	k8xC	a
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
leptání	leptání	k1gNnSc1	leptání
a	a	k8xC	a
matování	matování	k1gNnSc1	matování
skla	sklo	k1gNnSc2	sklo
<g/>
)	)	kIx)	)
i	i	k9	i
při	při	k7c6	při
chemických	chemický	k2eAgInPc6d1	chemický
rozkladech	rozklad	k1gInPc6	rozklad
odolných	odolný	k2eAgFnPc2d1	odolná
silikátových	silikátový	k2eAgFnPc2d1	silikátová
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
s	s	k7c7	s
fluorovodíkem	fluorovodík	k1gInSc7	fluorovodík
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
polovodičovém	polovodičový	k2eAgInSc6d1	polovodičový
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
řízenému	řízený	k2eAgNnSc3d1	řízené
odleptávání	odleptávání	k1gNnSc3	odleptávání
určených	určený	k2eAgFnPc2d1	určená
vrstev	vrstva	k1gFnPc2	vrstva
křemíkové	křemíkový	k2eAgFnSc2d1	křemíková
matrice	matrice	k1gFnSc2	matrice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
snad	snad	k9	snad
nejpoužívanější	používaný	k2eAgNnSc1d3	nejpoužívanější
polární	polární	k2eAgNnSc1d1	polární
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
též	též	k9	též
k	k	k7c3	k
fluoraci	fluorace	k1gFnSc3	fluorace
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
freonů	freon	k1gInPc2	freon
<g/>
,	,	kIx,	,
fluoroplastů	fluoroplast	k1gInPc2	fluoroplast
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Kryolit	kryolit	k1gInSc1	kryolit
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
AlF	alfa	k1gFnPc2	alfa
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
podstatně	podstatně	k6eAd1	podstatně
snižuje	snižovat	k5eAaImIp3nS	snižovat
bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
(	(	kIx(	(
<g/>
bauxitu	bauxit	k1gInSc2	bauxit
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
cennou	cenný	k2eAgFnSc7d1	cenná
přísadou	přísada	k1gFnSc7	přísada
při	při	k7c6	při
elektrolytické	elektrolytický	k2eAgFnSc6d1	elektrolytická
výrobě	výroba	k1gFnSc6	výroba
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Hexafluorid	Hexafluorid	k1gInSc1	Hexafluorid
uranu	uran	k1gInSc2	uran
UF6	UF6	k1gFnSc2	UF6
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
těkavá	těkavý	k2eAgFnSc1d1	těkavá
sloučenina	sloučenina	k1gFnSc1	sloučenina
a	a	k8xC	a
již	již	k9	již
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
při	při	k7c6	při
metodě	metoda	k1gFnSc6	metoda
difusní	difusní	k2eAgFnSc2d1	difusní
separace	separace	k1gFnSc2	separace
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
postupů	postup	k1gInPc2	postup
pro	pro	k7c4	pro
oddělování	oddělování	k1gNnSc4	oddělování
izotopů	izotop	k1gInPc2	izotop
uranu	uran	k1gInSc2	uran
235U	[number]	k4	235U
a	a	k8xC	a
238	[number]	k4	238
<g/>
U.	U.	kA	U.
Na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
výrobu	výroba	k1gFnSc4	výroba
hexafluoridu	hexafluorid	k1gInSc2	hexafluorid
uranu	uran	k1gInSc2	uran
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
%	%	kIx~	%
vyrobeného	vyrobený	k2eAgInSc2d1	vyrobený
fluoru	fluor	k1gInSc2	fluor
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
fluoridu	fluorid	k1gInSc2	fluorid
sírového	sírový	k2eAgInSc2d1	sírový
SF	SF	kA	SF
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
dielektrikum	dielektrikum	k1gNnSc1	dielektrikum
a	a	k8xC	a
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
fluoračních	fluorační	k2eAgNnPc2d1	fluorační
činidel	činidlo	k1gNnPc2	činidlo
(	(	kIx(	(
<g/>
ClF	ClF	k1gFnSc1	ClF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
BrF	BrF	k1gFnSc1	BrF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
IF	IF	kA	IF
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
fluoraci	fluorace	k1gFnSc3	fluorace
při	při	k7c6	při
organických	organický	k2eAgFnPc6d1	organická
syntézách	syntéza	k1gFnPc6	syntéza
(	(	kIx(	(
<g/>
BF	BF	kA	BF
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
ve	v	k7c6	v
Friedel-Craftsově	Friedel-Craftsův	k2eAgFnSc6d1	Friedel-Craftsův
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
,	,	kIx,	,
<g/>
-polymerace	olymerace	k1gFnSc1	-polymerace
alkenů	alken	k1gInPc2	alken
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
wolframový	wolframový	k2eAgInSc1d1	wolframový
WF6	WF6	k1gMnSc7	WF6
a	a	k8xC	a
fluorid	fluorid	k1gInSc4	fluorid
rhenový	rhenový	k2eAgInSc4d1	rhenový
ReF	ReF	k1gFnSc7	ReF
<g/>
6	[number]	k4	6
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
nanášení	nanášení	k1gNnSc3	nanášení
tenké	tenký	k2eAgFnSc2d1	tenká
kovové	kovový	k2eAgFnSc2d1	kovová
vrstvy	vrstva	k1gFnSc2	vrstva
(	(	kIx(	(
<g/>
metodou	metoda	k1gFnSc7	metoda
CVD	CVD	kA	CVD
<g/>
)	)	kIx)	)
wolframu	wolfram	k1gInSc2	wolfram
a	a	k8xC	a
rhenia	rhenium	k1gNnSc2	rhenium
na	na	k7c4	na
složitě	složitě	k6eAd1	složitě
vytvarované	vytvarovaný	k2eAgFnPc4d1	vytvarovaná
součástky	součástka	k1gFnPc4	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
kompletně	kompletně	k6eAd1	kompletně
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
atomy	atom	k1gInPc1	atom
halogenů	halogen	k1gInPc2	halogen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
chemickou	chemický	k2eAgFnSc7d1	chemická
stabilitou	stabilita	k1gFnSc7	stabilita
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
toxické	toxický	k2eAgNnSc1d1	toxické
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
započala	započnout	k5eAaPmAgFnS	započnout
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
významný	významný	k2eAgInSc1d1	významný
úspěch	úspěch	k1gInSc1	úspěch
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
sloučenin	sloučenina	k1gFnPc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
byl	být	k5eAaImAgInS	být
objev	objev	k1gInSc1	objev
inertních	inertní	k2eAgInPc2d1	inertní
fluorovaných	fluorovaný	k2eAgInPc2d1	fluorovaný
olejů	olej	k1gInPc2	olej
<g/>
,	,	kIx,	,
mazacích	mazací	k2eAgInPc2d1	mazací
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
polymerů	polymer	k1gInPc2	polymer
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
,	,	kIx,	,
nehořlavé	hořlavý	k2eNgFnPc1d1	nehořlavá
<g/>
,	,	kIx,	,
netoxické	toxický	k2eNgFnPc1d1	netoxická
-	-	kIx~	-
tedy	tedy	k8xC	tedy
zcela	zcela	k6eAd1	zcela
inertní	inertní	k2eAgNnPc1d1	inertní
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
všestranné	všestranný	k2eAgNnSc1d1	všestranné
<g/>
.	.	kIx.	.
</s>
<s>
Oleje	olej	k1gInPc1	olej
a	a	k8xC	a
tuky	tuk	k1gInPc1	tuk
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
mazání	mazání	k1gNnSc3	mazání
namáhaných	namáhaný	k2eAgFnPc2d1	namáhaná
součástek	součástka	k1gFnPc2	součástka
a	a	k8xC	a
ložisek	ložisko	k1gNnPc2	ložisko
a	a	k8xC	a
oleje	olej	k1gInPc1	olej
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
měrnými	měrný	k2eAgFnPc7d1	měrná
tepelnými	tepelný	k2eAgFnPc7d1	tepelná
kapacitami	kapacita	k1gFnPc7	kapacita
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
jako	jako	k9	jako
chladicí	chladicí	k2eAgFnPc4d1	chladicí
kapaliny	kapalina	k1gFnPc4	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Polymer	polymer	k1gInSc1	polymer
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
teflon	teflon	k1gInSc1	teflon
(	(	kIx(	(
<g/>
chemicky	chemicky	k6eAd1	chemicky
polytetrafluorethylen	polytetrafluorethylen	k1gInSc1	polytetrafluorethylen
[	[	kIx(	[
<g/>
CF	CF	kA	CF
<g/>
2	[number]	k4	2
<g/>
-CF	-CF	k?	-CF
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
chemicky	chemicky	k6eAd1	chemicky
a	a	k8xC	a
tepelně	tepelně	k6eAd1	tepelně
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
mnohostranné	mnohostranný	k2eAgNnSc1d1	mnohostranné
využití	využití	k1gNnSc1	využití
od	od	k7c2	od
pokrývání	pokrývání	k1gNnSc2	pokrývání
povrchu	povrch	k1gInSc2	povrch
kuchyňského	kuchyňský	k2eAgNnSc2d1	kuchyňské
nádobí	nádobí	k1gNnSc2	nádobí
po	po	k7c4	po
výrobu	výroba	k1gFnSc4	výroba
odolných	odolný	k2eAgFnPc2d1	odolná
chemických	chemický	k2eAgFnPc2d1	chemická
aparatur	aparatura	k1gFnPc2	aparatura
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
agresivními	agresivní	k2eAgNnPc7d1	agresivní
médii	médium	k1gNnPc7	médium
<g/>
,	,	kIx,	,
nádob	nádoba	k1gFnPc2	nádoba
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
silných	silný	k2eAgFnPc2d1	silná
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
reaktivních	reaktivní	k2eAgFnPc2d1	reaktivní
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Dobrých	dobrý	k2eAgFnPc2d1	dobrá
elektrických	elektrický	k2eAgFnPc2d1	elektrická
izolačních	izolační	k2eAgFnPc2d1	izolační
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
tepelné	tepelný	k2eAgFnSc2d1	tepelná
odolnosti	odolnost	k1gFnSc2	odolnost
teflonu	teflon	k1gInSc2	teflon
využívá	využívat	k5eAaImIp3nS	využívat
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
teflonu	teflon	k1gInSc2	teflon
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
velmi	velmi	k6eAd1	velmi
odolná	odolný	k2eAgNnPc1d1	odolné
těsnění	těsnění	k1gNnPc1	těsnění
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
účely	účel	k1gInPc4	účel
i	i	k8xC	i
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Freony	freon	k1gInPc1	freon
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vždy	vždy	k6eAd1	vždy
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nejméně	málo	k6eAd3	málo
2	[number]	k4	2
atomy	atom	k1gInPc4	atom
fluoru	fluor	k1gInSc2	fluor
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
atomy	atom	k1gInPc1	atom
jiného	jiný	k2eAgInSc2d1	jiný
halogenu	halogen	k1gInSc2	halogen
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
chloru	chlor	k1gInSc3	chlor
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejznámější	známý	k2eAgInSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
dichlordifluormethan	dichlordifluormethan	k1gInSc1	dichlordifluormethan
CCl	CCl	k1gFnSc2	CCl
<g/>
2	[number]	k4	2
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
halogenouhlovodíky	halogenouhlovodík	k1gInPc1	halogenouhlovodík
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
desetiletích	desetiletí	k1gNnPc6	desetiletí
masového	masový	k2eAgNnSc2d1	masové
uplatnění	uplatnění	k1gNnSc2	uplatnění
především	především	k9	především
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
oborech	obor	k1gInPc6	obor
<g/>
:	:	kIx,	:
Chladírenská	chladírenský	k2eAgFnSc1d1	chladírenská
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradily	nahradit	k5eAaPmAgFnP	nahradit
dříve	dříve	k6eAd2	dříve
používaný	používaný	k2eAgInSc1d1	používaný
toxický	toxický	k2eAgInSc1d1	toxický
amoniak	amoniak	k1gInSc1	amoniak
nebo	nebo	k8xC	nebo
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
jsou	být	k5eAaImIp3nP	být
freony	freon	k1gInPc1	freon
chladicím	chladicí	k2eAgNnSc7d1	chladicí
médiem	médium	k1gNnSc7	médium
desítek	desítka	k1gFnPc2	desítka
milionů	milion	k4xCgInPc2	milion
chladniček	chladnička	k1gFnPc2	chladnička
i	i	k8xC	i
automobilových	automobilový	k2eAgFnPc2d1	automobilová
a	a	k8xC	a
bytových	bytový	k2eAgFnPc2d1	bytová
klimatizačních	klimatizační	k2eAgFnPc2d1	klimatizační
jednotek	jednotka	k1gFnPc2	jednotka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
pěnových	pěnový	k2eAgFnPc2d1	pěnová
hmot	hmota	k1gFnPc2	hmota
(	(	kIx(	(
<g/>
pěnový	pěnový	k2eAgInSc1d1	pěnový
polystyren	polystyren	k1gInSc1	polystyren
<g/>
,	,	kIx,	,
pěnový	pěnový	k2eAgInSc1d1	pěnový
polyurethan	polyurethan	k1gInSc1	polyurethan
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
využívaly	využívat	k5eAaImAgFnP	využívat
jako	jako	k9	jako
pěnicí	pěnice	k1gFnSc7	pěnice
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnSc1d1	hnací
náplň	náplň	k1gFnSc1	náplň
do	do	k7c2	do
sprejů	sprej	k1gInPc2	sprej
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc4	nezávadnost
a	a	k8xC	a
nehořlavost	nehořlavost	k1gFnSc4	nehořlavost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
ekologických	ekologický	k2eAgInPc2d1	ekologický
důvodů	důvod	k1gInPc2	důvod
produkce	produkce	k1gFnSc2	produkce
freonů	freon	k1gInPc2	freon
celosvětově	celosvětově	k6eAd1	celosvětově
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
ozonové	ozonový	k2eAgFnSc2d1	ozonová
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
fluor	fluor	k1gInSc1	fluor
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
své	svůj	k3xOyFgFnPc4	svůj
sloučeniny	sloučenina	k1gFnPc4	sloučenina
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jediném	jediný	k2eAgNnSc6d1	jediné
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
F	F	kA	F
<g/>
-	-	kIx~	-
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gFnPc2	jeho
anorganických	anorganický	k2eAgFnPc2d1	anorganická
sloučenin	sloučenina	k1gFnPc2	sloučenina
velmi	velmi	k6eAd1	velmi
omezen	omezen	k2eAgInSc1d1	omezen
(	(	kIx(	(
<g/>
jediné	jediný	k2eAgNnSc1d1	jediné
kladné	kladný	k2eAgNnSc1d1	kladné
oxidační	oxidační	k2eAgNnSc1d1	oxidační
číslo	číslo	k1gNnSc1	číslo
má	mít	k5eAaImIp3nS	mít
fluor	fluor	k1gInSc4	fluor
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorné	fluorný	k2eAgFnSc6d1	fluorný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zde	zde	k6eAd1	zde
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
formální	formální	k2eAgInSc4d1	formální
význam	význam	k1gInSc4	význam
tohoto	tento	k3xDgNnSc2	tento
oxidačního	oxidační	k2eAgNnSc2d1	oxidační
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komplexních	komplexní	k2eAgFnPc6d1	komplexní
sloučeninách	sloučenina	k1gFnPc6	sloučenina
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
fluor	fluor	k1gInSc1	fluor
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
jako	jako	k8xC	jako
ligand	ligand	k1gInSc4	ligand
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gInPc1	jeho
komplexy	komplex	k1gInPc1	komplex
nejsou	být	k5eNaImIp3nP	být
většinou	většinou	k6eAd1	většinou
nijak	nijak	k6eAd1	nijak
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
fluor	fluor	k1gInSc1	fluor
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c4	mnoho
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
mnoho	mnoho	k4c4	mnoho
anorganických	anorganický	k2eAgFnPc2d1	anorganická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gFnPc2	jeho
sloučenin	sloučenina	k1gFnPc2	sloučenina
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
fluoridy	fluorid	k1gInPc1	fluorid
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
atomy	atom	k1gInPc1	atom
kovů	kov	k1gInPc2	kov
svých	svůj	k3xOyFgNnPc2	svůj
maximálních	maximální	k2eAgNnPc2d1	maximální
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
fluoridy	fluorid	k1gInPc1	fluorid
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
fluor	fluor	k1gInSc1	fluor
tvoří	tvořit	k5eAaImIp3nS	tvořit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
skupinou	skupina	k1gFnSc7	skupina
fluoridů	fluorid	k1gInPc2	fluorid
jsou	být	k5eAaImIp3nP	být
interhalogenidové	interhalogenidový	k2eAgFnPc1d1	interhalogenidový
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc4	sloučenina
halogenu	halogen	k1gInSc2	halogen
s	s	k7c7	s
halogeny	halogen	k1gInPc7	halogen
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
fluor	fluor	k1gInSc1	fluor
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
hodnotu	hodnota	k1gFnSc4	hodnota
elektronegativity	elektronegativita	k1gFnSc2	elektronegativita
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
také	také	k9	také
několik	několik	k4yIc4	několik
sloučenin	sloučenina	k1gFnPc2	sloučenina
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
průmyslově	průmyslově	k6eAd1	průmyslově
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
dvě	dva	k4xCgFnPc1	dva
kyseliny	kyselina	k1gFnPc1	kyselina
-	-	kIx~	-
kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
HF	HF	kA	HF
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
fluorná	fluorný	k2eAgFnSc1d1	fluorný
HOF	HOF	kA	HOF
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
HF	HF	kA	HF
byla	být	k5eAaImAgFnS	být
donedávna	donedávna	k6eAd1	donedávna
jedinou	jediný	k2eAgFnSc7d1	jediná
známou	známý	k2eAgFnSc7d1	známá
kyselinou	kyselina	k1gFnSc7	kyselina
fluoru	fluor	k1gInSc2	fluor
<g/>
.	.	kIx.	.
</s>
<s>
Fluorovodík	fluorovodík	k1gInSc1	fluorovodík
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
<g/>
,	,	kIx,	,
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leptá	leptat	k5eAaImIp3nS	leptat
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zahříváním	zahřívání	k1gNnSc7	zahřívání
fluoridu	fluorid	k1gInSc2	fluorid
vápenatého	vápenatý	k2eAgNnSc2d1	vápenaté
s	s	k7c7	s
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zředěných	zředěný	k2eAgInPc6d1	zředěný
vodných	vodný	k2eAgInPc6d1	vodný
roztocích	roztok	k1gInPc6	roztok
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
středně	středně	k6eAd1	středně
silná	silný	k2eAgFnSc1d1	silná
až	až	k8xS	až
slabá	slabý	k2eAgFnSc1d1	slabá
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xS	jako
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
HF	HF	kA	HF
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
neopatrném	opatrný	k2eNgNnSc6d1	neopatrné
potřísnění	potřísnění	k1gNnSc6	potřísnění
kůže	kůže	k1gFnSc2	kůže
difunduje	difundovat	k5eAaImIp3nS	difundovat
kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
tkáněmi	tkáň	k1gFnPc7	tkáň
až	až	k9	až
ke	k	k7c3	k
kostem	kost	k1gFnPc3	kost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
chemicky	chemicky	k6eAd1	chemicky
napadá	napadat	k5eAaPmIp3nS	napadat
a	a	k8xC	a
neopatrný	opatrný	k2eNgMnSc1d1	neopatrný
chemik	chemik	k1gMnSc1	chemik
pocítí	pocítit	k5eAaPmIp3nS	pocítit
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
velmi	velmi	k6eAd1	velmi
nepříjemnou	příjemný	k2eNgFnSc4d1	nepříjemná
a	a	k8xC	a
úpornou	úporný	k2eAgFnSc4d1	úporná
bolest	bolest	k1gFnSc4	bolest
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
kovy	kov	k1gInPc7	kov
tvoří	tvořit	k5eAaImIp3nS	tvořit
kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
soli	sůl	k1gFnSc2	sůl
fluoridy	fluorid	k1gInPc4	fluorid
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc4	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
jsou	být	k5eAaImIp3nP	být
nejhůře	zle	k6eAd3	zle
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
halogenidů	halogenid	k1gInPc2	halogenid
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
i	i	k8xC	i
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
měřítku	měřítko	k1gNnSc6	měřítko
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
fluorná	fluorný	k2eAgFnSc1d1	fluorný
HOF	HOF	kA	HOF
neboli	neboli	k8xC	neboli
monofluorovaná	monofluorovaný	k2eAgFnSc1d1	monofluorovaný
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
nejzajímavější	zajímavý	k2eAgNnSc1d3	nejzajímavější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
málo	málo	k6eAd1	málo
známá	známý	k2eAgFnSc1d1	známá
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pevná	pevný	k2eAgFnSc1d1	pevná
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
samovolně	samovolně	k6eAd1	samovolně
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
fluorovodíku	fluorovodík	k1gInSc2	fluorovodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ji	on	k3xPp3gFnSc4	on
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
výrobě	výroba	k1gFnSc6	výroba
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
destruktivní	destruktivní	k2eAgNnSc1d1	destruktivní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oxidacích	oxidace	k1gFnPc6	oxidace
se	se	k3xPyFc4	se
z	z	k7c2	z
kyseliny	kyselina	k1gFnSc2	kyselina
fluorné	fluorný	k2eAgNnSc1d1	fluorný
odštěpuje	odštěpovat	k5eAaImIp3nS	odštěpovat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
fluorovodík	fluorovodík	k1gInSc4	fluorovodík
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
fluorná	fluorný	k2eAgFnSc1d1	fluorný
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
zaváděním	zavádění	k1gNnSc7	zavádění
plynné	plynný	k2eAgFnSc2d1	plynná
směsi	směs	k1gFnSc2	směs
z	z	k7c2	z
10	[number]	k4	10
<g/>
%	%	kIx~	%
fluoru	fluor	k1gInSc2	fluor
a	a	k8xC	a
90	[number]	k4	90
<g/>
%	%	kIx~	%
dusíku	dusík	k1gInSc2	dusík
do	do	k7c2	do
acetonitrilu	acetonitril	k1gInSc2	acetonitril
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Acetonitril	Acetonitril	k1gMnSc1	Acetonitril
CH3CN	CH3CN	k1gMnSc1	CH3CN
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
netečný	tečný	k2eNgInSc1d1	netečný
a	a	k8xC	a
fluor	fluor	k1gInSc1	fluor
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
fluorné	fluorný	k2eAgFnSc2d1	fluorný
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
fluor	fluor	k1gInSc1	fluor
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
prvek	prvek	k1gInSc1	prvek
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
větší	veliký	k2eAgFnSc4d2	veliký
elektronegativitu	elektronegativita	k1gFnSc4	elektronegativita
než	než	k8xS	než
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
několik	několik	k4yIc1	několik
fluoridů	fluorid	k1gInPc2	fluorid
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
+	+	kIx~	+
nebo	nebo	k8xC	nebo
zlomkových	zlomkový	k2eAgNnPc6d1	zlomkové
oxidačních	oxidační	k2eAgNnPc6d1	oxidační
číslech	číslo	k1gNnPc6	číslo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
OF	OF	kA	OF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
O	o	k7c6	o
<g/>
2	[number]	k4	2
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
O	o	k7c6	o
<g/>
4	[number]	k4	4
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
O	o	k7c6	o
<g/>
5	[number]	k4	5
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
O	o	k7c6	o
<g/>
6	[number]	k4	6
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Difluorid	Difluorid	k1gInSc1	Difluorid
kyslíku	kyslík	k1gInSc2	kyslík
OF2	OF2	k1gFnSc2	OF2
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
zaváděním	zavádění	k1gNnSc7	zavádění
fluoru	fluor	k1gInSc2	fluor
do	do	k7c2	do
vodného	vodné	k1gNnSc2	vodné
roztoku	roztok	k1gInSc3	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Difluorid	Difluorid	k1gInSc1	Difluorid
dikyslíku	dikyslík	k1gInSc2	dikyslík
O2F2	O2F2	k1gFnSc2	O2F2
je	být	k5eAaImIp3nS	být
slabě	slabě	k6eAd1	slabě
hnědá	hnědý	k2eAgFnSc1d1	hnědá
plynná	plynný	k2eAgFnSc1d1	plynná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
fluor	fluor	k1gInSc4	fluor
<g/>
,	,	kIx,	,
připravuje	připravovat	k5eAaImIp3nS	připravovat
se	s	k7c7	s
přímým	přímý	k2eAgNnSc7d1	přímé
slučováním	slučování	k1gNnSc7	slučování
fluoru	fluor	k1gInSc2	fluor
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
doutnavého	doutnavý	k2eAgInSc2d1	doutnavý
výboje	výboj	k1gInSc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
dokáže	dokázat	k5eAaPmIp3nS	dokázat
fluor	fluor	k1gInSc1	fluor
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
vytvářet	vytvářet	k5eAaImF	vytvářet
nestálé	stálý	k2eNgFnPc4d1	nestálá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
se	s	k7c7	s
vzácnými	vzácný	k2eAgInPc7d1	vzácný
plyny	plyn	k1gInPc7	plyn
argonem	argon	k1gInSc7	argon
<g/>
,	,	kIx,	,
kryptonem	krypton	k1gInSc7	krypton
<g/>
,	,	kIx,	,
xenonem	xenon	k1gInSc7	xenon
a	a	k8xC	a
radonem	radon	k1gInSc7	radon
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
interhalogenních	interhalogenní	k2eAgFnPc2d1	interhalogenní
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Interhalogeny	Interhalogen	k1gInPc1	Interhalogen
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc4	sloučenina
halogenů	halogen	k1gInPc2	halogen
s	s	k7c7	s
halogeny	halogen	k1gInPc7	halogen
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
fluoru	fluor	k1gInSc2	fluor
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
halogeny	halogen	k1gInPc7	halogen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
fluorid	fluorid	k1gInSc1	fluorid
chlorný	chlorný	k2eAgInSc1d1	chlorný
ClF	ClF	k1gFnSc3	ClF
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc4	fluorid
chloritý	chloritý	k2eAgInSc4d1	chloritý
ClF	ClF	k1gFnSc7	ClF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
fluorid	fluorid	k1gInSc4	fluorid
chlorečný	chlorečný	k2eAgInSc4d1	chlorečný
ClF	ClF	k1gFnSc7	ClF
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
kationt	kationt	k1gInSc1	kationt
fluorido	fluorida	k1gFnSc5	fluorida
dichlorný	dichlorný	k2eAgInSc4d1	dichlorný
Cl	Cl	k1gFnSc7	Cl
<g/>
2	[number]	k4	2
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
kationt	kationt	k1gInSc1	kationt
tetrafluorido	tetrafluorida	k1gFnSc5	tetrafluorida
chlorečný	chlorečný	k2eAgInSc4d1	chlorečný
ClF	ClF	k1gFnSc7	ClF
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
a	a	k8xC	a
kationt	kationt	k1gInSc1	kationt
hexafluorido	hexafluorida	k1gFnSc5	hexafluorida
chloristý	chloristý	k2eAgInSc4d1	chloristý
ClF	ClF	k1gFnSc7	ClF
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
s	s	k7c7	s
bromem	brom	k1gInSc7	brom
fluorid	fluorid	k1gInSc1	fluorid
bromný	bromný	k2eAgMnSc1d1	bromný
BrF	BrF	k1gMnSc1	BrF
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc4	fluorid
bromitý	bromitý	k2eAgInSc4d1	bromitý
BrF	BrF	k1gFnSc7	BrF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc4	fluorid
bromičný	bromičný	k2eAgInSc4d1	bromičný
BrF	BrF	k1gFnSc7	BrF
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
kationt	kationt	k1gInSc1	kationt
tetrafluorido	tetrafluorida	k1gFnSc5	tetrafluorida
bromičný	bromičný	k2eAgInSc4d1	bromičný
BrF	BrF	k1gFnSc7	BrF
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
a	a	k8xC	a
kationt	kationt	k1gInSc1	kationt
hexafluorido	hexafluorida	k1gFnSc5	hexafluorida
bromistý	bromistý	k2eAgInSc4d1	bromistý
BrF	BrF	k1gFnSc7	BrF
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
a	a	k8xC	a
s	s	k7c7	s
jódem	jód	k1gInSc7	jód
fluorid	fluorid	k1gInSc1	fluorid
jodný	jodný	k1gMnSc1	jodný
IF	IF	kA	IF
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc4	fluorid
joditý	joditý	k2eAgInSc4d1	joditý
IF	IF	kA	IF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc4	fluorid
jodičný	jodičný	k2eAgInSc4d1	jodičný
IF	IF	kA	IF
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc4	fluorid
jodistý	jodistý	k2eAgInSc4d1	jodistý
IF	IF	kA	IF
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
kationt	kationt	k1gInSc1	kationt
tetrafluorido	tetrafluorida	k1gFnSc5	tetrafluorida
jodičný	jodičný	k2eAgInSc4d1	jodičný
IF	IF	kA	IF
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
a	a	k8xC	a
kationt	kationt	k1gInSc1	kationt
hexafluorido	hexafluorida	k1gFnSc5	hexafluorida
jodistý	jodistý	k2eAgInSc4d1	jodistý
IF	IF	kA	IF
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Nejstálejší	stálý	k2eAgFnPc1d3	nejstálejší
interhalogenidy	interhalogenida	k1gFnPc1	interhalogenida
tvoří	tvořit	k5eAaImIp3nP	tvořit
fluor	fluor	k1gInSc4	fluor
s	s	k7c7	s
jodem	jod	k1gInSc7	jod
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
stabilní	stabilní	k2eAgFnSc1d1	stabilní
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
<g/>
,	,	kIx,	,
stabilita	stabilita	k1gFnSc1	stabilita
těchto	tento	k3xDgFnPc2	tento
sloučenin	sloučenina	k1gFnPc2	sloučenina
také	také	k9	také
klesá	klesat	k5eAaImIp3nS	klesat
se	s	k7c7	s
zvětšujícím	zvětšující	k2eAgMnSc7d1	zvětšující
se	se	k3xPyFc4	se
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
kationtu	kation	k1gInSc2	kation
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
molekul	molekula	k1gFnPc2	molekula
interhalogenidů	interhalogenid	k1gMnPc2	interhalogenid
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
pravidly	pravidlo	k1gNnPc7	pravidlo
VSEPR	VSEPR	kA	VSEPR
a	a	k8xC	a
elektronegativitou	elektronegativita	k1gFnSc7	elektronegativita
substituentů	substituent	k1gInPc2	substituent
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tvar	tvar	k1gInSc4	tvar
molekuly	molekula	k1gFnSc2	molekula
ClF	ClF	k1gFnSc2	ClF
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
tvaru	tvar	k1gInSc2	tvar
T	T	kA	T
<g/>
,	,	kIx,	,
BrF	BrF	k1gFnSc1	BrF
<g/>
5	[number]	k4	5
tvaru	tvar	k1gInSc2	tvar
tetragonální	tetragonální	k2eAgFnSc2d1	tetragonální
pyramidy	pyramida	k1gFnSc2	pyramida
a	a	k8xC	a
IF7	IF7	k1gFnSc7	IF7
tvaru	tvar	k1gInSc2	tvar
pentagonální	pentagonální	k2eAgFnSc2d1	pentagonální
bipyramidy	bipyramida	k1gFnSc2	bipyramida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Interhalogenidy	Interhalogenida	k1gFnPc1	Interhalogenida
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
volný	volný	k2eAgInSc4d1	volný
fluor	fluor	k1gInSc4	fluor
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
silné	silný	k2eAgInPc4d1	silný
oxidační	oxidační	k2eAgInPc4d1	oxidační
a	a	k8xC	a
fluorační	fluorační	k2eAgInPc4d1	fluorační
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
jako	jako	k9	jako
Lewisovy	Lewisův	k2eAgFnPc1d1	Lewisova
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
interhalogenidních	interhalogenidní	k2eAgFnPc2d1	interhalogenidní
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
fluor	fluor	k1gInSc1	fluor
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
také	také	k9	také
oxidy	oxid	k1gInPc1	oxid
interhalogenidních	interhalogenidní	k2eAgFnPc2d1	interhalogenidní
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
nazývat	nazývat	k5eAaImF	nazývat
fluoridy	fluorid	k1gInPc4	fluorid
oxokyselin	oxokyselina	k1gFnPc2	oxokyselina
<g/>
)	)	kIx)	)
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
halogeny	halogen	k1gInPc7	halogen
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
fluor	fluor	k1gInSc1	fluor
fluorid-oxid	fluoridxida	k1gFnPc2	fluorid-oxida
chloritý	chloritý	k2eAgMnSc1d1	chloritý
ClOF	ClOF	k1gMnSc1	ClOF
<g/>
,	,	kIx,	,
fluorid-dioxid	fluoridioxid	k1gInSc1	fluorid-dioxid
chlorečný	chlorečný	k2eAgInSc1d1	chlorečný
ClO	clo	k1gNnSc4	clo
<g/>
2	[number]	k4	2
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
fluorid-trioxid	fluoridrioxid	k1gInSc4	fluorid-trioxid
chloristý	chloristý	k2eAgInSc4d1	chloristý
ClO	clo	k1gNnSc4	clo
<g/>
3	[number]	k4	3
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
trifluorid-oxid	trifluoridxid	k1gInSc4	trifluorid-oxid
chlorečný	chlorečný	k2eAgInSc4d1	chlorečný
ClOF	ClOF	k1gFnSc7	ClOF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
trifluorid-dioxid	trifluoridioxid	k1gInSc4	trifluorid-dioxid
chloristý	chloristý	k2eAgInSc4d1	chloristý
ClO	clo	k1gNnSc4	clo
<g/>
2	[number]	k4	2
<g/>
F	F	kA	F
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
molekul	molekula	k1gFnPc2	molekula
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
teorií	teorie	k1gFnSc7	teorie
VSEPR	VSEPR	kA	VSEPR
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
téměř	téměř	k6eAd1	téměř
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
chemické	chemický	k2eAgFnSc2d1	chemická
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Polymerní	polymerní	k2eAgInSc1d1	polymerní
polytetrafluorethylen	polytetrafluorethylen	k1gInSc1	polytetrafluorethylen
(	(	kIx(	(
<g/>
teflon	teflon	k1gInSc1	teflon
<g/>
)	)	kIx)	)
o	o	k7c6	o
vzorci	vzorec	k1gInSc6	vzorec
F-	F-	k1gFnSc2	F-
<g/>
(	(	kIx(	(
<g/>
-CF	-CF	k?	-CF
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
n-F	n-F	k?	n-F
<g/>
,	,	kIx,	,
polymerní	polymerní	k2eAgInSc1d1	polymerní
uhlovodík	uhlovodík	k1gInSc1	uhlovodík
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
fluorem	fluor	k1gInSc7	fluor
<g/>
.	.	kIx.	.
</s>
<s>
Freony	freon	k1gInPc1	freon
-	-	kIx~	-
řada	řada	k1gFnSc1	řada
plynných	plynný	k2eAgFnPc2d1	plynná
nebo	nebo	k8xC	nebo
kapalných	kapalný	k2eAgFnPc2d1	kapalná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jeden	jeden	k4xCgMnSc1	jeden
až	až	k8xS	až
pět	pět	k4xCc1	pět
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
substituovány	substituovat	k5eAaBmNgInP	substituovat
fluorem	fluor	k1gInSc7	fluor
<g/>
,	,	kIx,	,
chlorem	chlor	k1gInSc7	chlor
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
bromem	brom	k1gInSc7	brom
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
též	též	k9	též
označovány	označovat	k5eAaImNgInP	označovat
zkratkou	zkratka	k1gFnSc7	zkratka
CFC	CFC	kA	CFC
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
chlorofluorocarbon	chlorofluorocarbona	k1gFnPc2	chlorofluorocarbona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
schopnosti	schopnost	k1gFnSc3	schopnost
narušovat	narušovat	k5eAaImF	narušovat
ozonovou	ozonový	k2eAgFnSc4d1	ozonová
vrstvu	vrstva	k1gFnSc4	vrstva
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
regulováno	regulovat	k5eAaImNgNnS	regulovat
Montrealským	montrealský	k2eAgInSc7d1	montrealský
protokolem	protokol	k1gInSc7	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Perfluoroktansulfonan	Perfluoroktansulfonan	k1gMnSc1	Perfluoroktansulfonan
(	(	kIx(	(
<g/>
PFOS	PFOS	kA	PFOS
<g/>
)	)	kIx)	)
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
teflonu	teflon	k1gInSc2	teflon
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
látka	látka	k1gFnSc1	látka
ovlivňující	ovlivňující	k2eAgFnSc1d1	ovlivňující
povrchové	povrchový	k2eAgNnSc4d1	povrchové
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
listinu	listina	k1gFnSc4	listina
Stockholmské	stockholmský	k2eAgFnSc2d1	Stockholmská
úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
perzistentních	perzistentní	k2eAgFnPc6d1	perzistentní
organických	organický	k2eAgFnPc6d1	organická
látkách	látka	k1gFnPc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
fluor	fluor	k1gInSc1	fluor
není	být	k5eNaImIp3nS	být
klasickým	klasický	k2eAgInSc7d1	klasický
biogenním	biogenní	k2eAgInSc7d1	biogenní
prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
příjem	příjem	k1gInSc1	příjem
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
především	především	k6eAd1	především
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
zdravých	zdravý	k2eAgInPc2d1	zdravý
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
zubním	zubní	k2eAgInSc7d1	zubní
kazem	kaz	k1gInSc7	kaz
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
fluorhydroxyapatitu	fluorhydroxyapatit	k1gInSc2	fluorhydroxyapatit
(	(	kIx(	(
<g/>
pentahydrát	pentahydrát	k1gInSc1	pentahydrát
<g/>
)	)	kIx)	)
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pro	pro	k7c4	pro
agresivní	agresivní	k2eAgNnSc4d1	agresivní
prostředí	prostředí	k1gNnSc4	prostředí
dutiny	dutina	k1gFnSc2	dutina
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zabránit	zabránit	k5eAaPmF	zabránit
útoku	útok	k1gInSc3	útok
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
na	na	k7c4	na
sklovinu	sklovina	k1gFnSc4	sklovina
(	(	kIx(	(
<g/>
hydroxyapatit	hydroxyapatit	k5eAaImF	hydroxyapatit
je	on	k3xPp3gNnSc4	on
po	po	k7c6	po
dekarboxylaci	dekarboxylace	k1gFnSc6	dekarboxylace
disociován	disociovat	k5eAaBmNgMnS	disociovat
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
této	tento	k3xDgFnSc2	tento
sloučeniny	sloučenina	k1gFnSc2	sloučenina
je	být	k5eAaImIp3nS	být
1000	[number]	k4	1000
<g/>
krát	krát	k6eAd1	krát
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
fyziologické	fyziologický	k2eAgInPc4d1	fyziologický
pochody	pochod	k1gInPc4	pochod
(	(	kIx(	(
<g/>
tvorba	tvorba	k1gFnSc1	tvorba
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
,	,	kIx,	,
zprostředkuje	zprostředkovat	k5eAaPmIp3nS	zprostředkovat
vazbu	vazba	k1gFnSc4	vazba
fosforečnanu	fosforečnan	k1gInSc2	fosforečnan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
ve	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
zubní	zubní	k2eAgFnPc1d1	zubní
pasty	pasta	k1gFnPc1	pasta
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
záměrně	záměrně	k6eAd1	záměrně
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
obsah	obsah	k1gInSc4	obsah
sloučenin	sloučenina	k1gFnPc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
(	(	kIx(	(
<g/>
fluorid	fluorid	k1gInSc1	fluorid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc1	fluorid
cínatý	cínatý	k2eAgInSc1d1	cínatý
<g/>
,	,	kIx,	,
fluorofosforečnan	fluorofosforečnan	k1gInSc1	fluorofosforečnan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
umělé	umělý	k2eAgNnSc1d1	umělé
zvyšováni	zvyšovat	k5eAaImNgMnP	zvyšovat
obsahu	obsah	k1gInSc2	obsah
fluoru	fluor	k1gInSc2	fluor
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fluorování	fluorování	k1gNnSc4	fluorování
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přípustné	přípustný	k2eAgNnSc1d1	přípustné
množství	množství	k1gNnSc1	množství
fluoru	fluor	k1gInSc2	fluor
v	v	k7c6	v
zubních	zubní	k2eAgFnPc6d1	zubní
pastách	pasta	k1gFnPc6	pasta
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
EU	EU	kA	EU
1500	[number]	k4	1500
ppm	ppm	k?	ppm
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
roste	růst	k5eAaImIp3nS	růst
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
dentální	dentální	k2eAgFnSc2d1	dentální
fluorózy	fluoróza	k1gFnSc2	fluoróza
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
hnědými	hnědý	k2eAgFnPc7d1	hnědá
shvrnami	shvrna	k1gFnPc7	shvrna
na	na	k7c6	na
zubech	zub	k1gInPc6	zub
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednorázovém	jednorázový	k2eAgNnSc6d1	jednorázové
požití	požití	k1gNnSc6	požití
pouhých	pouhý	k2eAgInPc2d1	pouhý
150	[number]	k4	150
mg	mg	kA	mg
fluoridu	fluorid	k1gInSc2	fluorid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
NaF	NaF	k1gFnSc1	NaF
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nevolnosti	nevolnost	k1gFnSc3	nevolnost
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc6	zvracení
<g/>
,	,	kIx,	,
průjmu	průjem	k1gInSc6	průjem
a	a	k8xC	a
akutní	akutní	k2eAgFnPc4d1	akutní
bolesti	bolest	k1gFnPc4	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgFnSc1d1	akutní
otrava	otrava	k1gFnSc1	otrava
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nS	léčit
infuzí	infuze	k1gFnSc7	infuze
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Fluoridový	Fluoridový	k2eAgInSc1d1	Fluoridový
anion	anion	k1gInSc1	anion
je	být	k5eAaImIp3nS	být
protoplasmatický	protoplasmatický	k2eAgInSc1d1	protoplasmatický
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
zasahující	zasahující	k2eAgNnSc1d1	zasahující
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
neurotoxický	urotoxický	k2eNgInSc1d1	neurotoxický
a	a	k8xC	a
váže	vázat	k5eAaImIp3nS	vázat
ionty	ion	k1gInPc4	ion
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
