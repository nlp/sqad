<s>
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
socha	socha	k1gFnSc1
<g/>
,	,	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
1739	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Socha	Socha	k1gMnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Socha	socha	k1gFnSc1
na	na	k7c4
průčelí	průčelí	k1gNnSc4
Městské	městský	k2eAgFnSc2d1
knihovnyv	knihovnyv	k1gInSc1
únoru	únor	k1gInSc6
2020	#num#	k4
<g/>
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Autor	autor	k1gMnSc1
</s>
<s>
snad	snad	k9
O.J.	O.J.	k1gMnSc1
<g/>
Wenda	Wenda	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
neznámý	známý	k2eNgMnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Rok	rok	k1gInSc1
vzniku	vznik	k1gInSc2
</s>
<s>
1739	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Umělecký	umělecký	k2eAgInSc1d1
směr	směr	k1gInSc1
</s>
<s>
baroko	baroko	k1gNnSc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
19080	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
888	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
Popis	popis	k1gInSc1
Výška	výška	k1gFnSc1
</s>
<s>
2	#num#	k4
m	m	kA
Materiál	materiál	k1gInSc1
</s>
<s>
pískovec	pískovec	k1gInSc1
Umístění	umístění	k1gNnSc2
Umístění	umístění	k1gNnSc2
</s>
<s>
Karlovy	Karlův	k2eAgFnSc2d1
Varyulice	Varyulice	k1gFnSc2
I.	I.	kA
P.	P.	kA
Pavlovaprůčelí	Pavlovaprůčelý	k2eAgMnPc1d1
budovy	budova	k1gFnPc4
Městské	městský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
41,2	41,2	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
47,64	47,64	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Socha	Socha	k1gMnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c4
průčelí	průčelí	k1gNnSc4
budovy	budova	k1gFnSc2
Městské	městský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
v	v	k7c6
ulici	ulice	k1gFnSc6
I.	I.	kA
P.	P.	kA
Pavlova	Pavlův	k2eAgFnSc1d1
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
je	být	k5eAaImIp3nS
datován	datovat	k5eAaImNgInS
k	k	k7c3
roku	rok	k1gInSc3
1739	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
pravděpodobný	pravděpodobný	k2eAgMnSc1d1
autor	autor	k1gMnSc1
díla	dílo	k1gNnSc2
byl	být	k5eAaImAgMnS
O.	O.	kA
J.	J.	kA
Wenda	Wenda	k1gFnSc1
ze	z	k7c2
Žlutic	Žlutice	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
event.	event.	k?
je	být	k5eAaImIp3nS
autor	autor	k1gMnSc1
neznámý	známý	k2eNgMnSc1d1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Socha	socha	k1gFnSc1
byla	být	k5eAaImAgFnS
prohlášena	prohlásit	k5eAaPmNgFnS
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
,	,	kIx,
památkově	památkově	k6eAd1
chráněna	chráněn	k2eAgMnSc4d1
je	být	k5eAaImIp3nS
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1958	#num#	k4
<g/>
,	,	kIx,
event.	event.	k?
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1964	#num#	k4
<g/>
,	,	kIx,
rejstř	rejstř	k1gFnSc1
<g/>
.	.	kIx.
č.	č.	k?
ÚSKP	ÚSKP	kA
19080	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
888	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Socha	Socha	k1gMnSc1
zakladatele	zakladatel	k1gMnSc2
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
<g/>
,	,	kIx,
císaře	císař	k1gMnSc2
a	a	k8xC
krále	král	k1gMnSc2
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
pochází	pocházet	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
1739	#num#	k4
a	a	k8xC
autorem	autor	k1gMnSc7
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
O.	O.	kA
J.	J.	kA
Wenda	Wenda	k1gFnSc1
ze	z	k7c2
Žlutic	Žlutice	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
je	být	k5eAaImIp3nS
připisování	připisování	k1gNnSc4
tohoto	tento	k3xDgNnSc2
autorství	autorství	k1gNnSc2
mylné	mylný	k2eAgNnSc1d1
a	a	k8xC
autor	autor	k1gMnSc1
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgMnS
jako	jako	k9
neznámý	známý	k2eNgMnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sochu	socha	k1gFnSc4
městu	město	k1gNnSc3
daroval	darovat	k5eAaPmAgInS
apelační	apelační	k2eAgMnSc1d1
rada	rada	k1gMnSc1
von	von	k1gInSc4
Schuppig	Schuppiga	k1gFnPc2
jako	jako	k8xS,k8xC
poděkování	poděkování	k1gNnSc2
za	za	k7c4
lázeňskou	lázeňský	k2eAgFnSc4d1
léčbu	léčba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c6
vysokém	vysoký	k2eAgInSc6d1
podstavci	podstavec	k1gInSc6
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
nároží	nároží	k1gNnSc6
renesanční	renesanční	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
původní	původní	k2eAgFnSc2d1
karlovarské	karlovarský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
na	na	k7c6
Tržišti	tržiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1875	#num#	k4
však	však	k9
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
radnice	radnice	k1gFnSc1
pro	pro	k7c4
celkový	celkový	k2eAgInSc4d1
špatný	špatný	k2eAgInSc4d1
stavební	stavební	k2eAgInSc4d1
stav	stav	k1gInSc4
zbořena	zbořit	k5eAaPmNgNnP
<g/>
,	,	kIx,
socha	socha	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
ještě	ještě	k6eAd1
před	před	k7c7
demolicí	demolice	k1gFnSc7
budovy	budova	k1gFnSc2
snesena	snesen	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
průčelí	průčelí	k1gNnSc4
budovy	budova	k1gFnSc2
bývalé	bývalý	k2eAgFnSc2d1
okresní	okresní	k2eAgFnSc2d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
krajské	krajský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
I.	I.	kA
P.	P.	kA
Pavlova	Pavlův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1975	#num#	k4
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
kvůli	kvůli	k7c3
špatnému	špatný	k2eAgInSc3d1
stavu	stav	k1gInSc3
restaurována	restaurován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byla	být	k5eAaImAgFnS
krajská	krajský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
přemístěna	přemístit	k5eAaPmNgFnS
do	do	k7c2
Dvorů	Dvůr	k1gInPc2
a	a	k8xC
budova	budova	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
sídlem	sídlo	k1gNnSc7
Městské	městský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Socha	socha	k1gFnSc1
na	na	k7c4
průčelí	průčelí	k1gNnSc4
budovy	budova	k1gFnSc2
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
majetku	majetek	k1gInSc6
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
2011	#num#	k4
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
na	na	k7c4
žádost	žádost	k1gFnSc4
městských	městský	k2eAgMnPc2d1
zastupitelů	zastupitel	k1gMnPc2
sochu	socha	k1gFnSc4
bezúplatně	bezúplatně	k6eAd1
převedl	převést	k5eAaPmAgMnS
do	do	k7c2
majetku	majetek	k1gInSc2
města	město	k1gNnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Socha	socha	k1gFnSc1
barokního	barokní	k2eAgInSc2d1
stylu	styl	k1gInSc2
o	o	k7c6
výšce	výška	k1gFnSc6
dva	dva	k4xCgInPc4
metry	metr	k1gInPc4
zhotovena	zhotoven	k2eAgNnPc4d1
z	z	k7c2
pískovce	pískovec	k1gInSc2
stojí	stát	k5eAaImIp3nS
na	na	k7c6
nízkém	nízký	k2eAgInSc6d1
nepravidelném	pravidelný	k2eNgInSc6d1
hranolovém	hranolový	k2eAgInSc6d1
soklu	sokl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachycuje	zachycovat	k5eAaImIp3nS
císaře	císař	k1gMnPc4
s	s	k7c7
typickými	typický	k2eAgInPc7d1
atributy	atribut	k1gInPc7
důstojnosti	důstojnost	k1gFnSc2
<g/>
,	,	kIx,
korunou	koruna	k1gFnSc7
na	na	k7c6
hlavě	hlava	k1gFnSc6
a	a	k8xC
říšským	říšský	k2eAgNnSc7d1
jablkem	jablko	k1gNnSc7
v	v	k7c6
pravé	pravý	k2eAgFnSc6d1
ruce	ruka	k1gFnSc6
<g/>
;	;	kIx,
pod	pod	k7c7
levou	levý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
přidržuje	přidržovat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc1
knihy	kniha	k1gFnPc1
symbolizující	symbolizující	k2eAgInPc4d1
právní	právní	k2eAgInPc4d1
a	a	k8xC
statutární	statutární	k2eAgNnSc4d1
založení	založení	k1gNnSc4
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postava	postava	k1gFnSc1
je	být	k5eAaImIp3nS
oděna	odět	k5eAaPmNgFnS
do	do	k7c2
zvlněného	zvlněný	k2eAgInSc2d1
hermelínového	hermelínový	k2eAgInSc2d1
pláště	plášť	k1gInSc2
splývajícího	splývající	k2eAgInSc2d1
z	z	k7c2
ramen	rameno	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dodává	dodávat	k5eAaImIp3nS
celé	celá	k1gFnPc4
kompozici	kompozice	k1gFnSc4
dynamičnost	dynamičnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
POCHE	POCHE	kA
<g/>
,	,	kIx,
Emanuel	Emanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Čech	Čechy	k1gFnPc2
2	#num#	k4
/	/	kIx~
<g/>
K	k	k7c3
<g/>
–	–	k?
<g/>
O	O	kA
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
580	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
s.	s.	k?
36	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
VYČICHLO	vyčichnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
–	–	k?
pomník	pomník	k1gInSc4
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památky	památka	k1gFnPc1
a	a	k8xC
příroda	příroda	k1gFnSc1
Karlovarska	Karlovarsko	k1gNnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
BURACHOVIČ	BURACHOVIČ	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
;	;	kIx,
JÁCHYMOVSKÝ	jáchymovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Vladislav	Vladislav	k1gMnSc1
<g/>
;	;	kIx,
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
MIESSLER	MIESSLER	kA
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
VYLITA	vylit	k2eAgMnSc4d1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
WIESER	WIESER	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
ZAHRADNÍČEK	Zahradníček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
104	#num#	k4
s.	s.	k?
S.	S.	kA
71	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Socha	Socha	k1gMnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
–	–	k?
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Socha	Socha	k1gMnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infocentrum	infocentrum	k1gNnSc1
města	město	k1gNnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
POCHE	POCHE	kA
<g/>
,	,	kIx,
Emanuel	Emanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Čech	Čechy	k1gFnPc2
2	#num#	k4
/	/	kIx~
<g/>
K	k	k7c3
<g/>
–	–	k?
<g/>
O	o	k7c4
<g/>
/	/	kIx~
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
580	#num#	k4
s.	s.	k?
S.	S.	kA
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BURACHOVIČ	BURACHOVIČ	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
;	;	kIx,
JÁCHYMOVSKÝ	jáchymovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Vladislav	Vladislav	k1gMnSc1
<g/>
;	;	kIx,
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
MIESSLER	MIESSLER	kA
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
VYLITA	vylit	k2eAgMnSc4d1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
WIESER	WIESER	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
ZAHRADNÍČEK	Zahradníček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
104	#num#	k4
s.	s.	k?
S.	S.	kA
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Socha	Socha	k1gMnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
–	–	k?
socha	socha	k1gFnSc1
Karla	Karla	k1gFnSc1
IV	IV	kA
<g/>
.	.	kIx.
–	–	k?
článek	článek	k1gInSc1
na	na	k7c6
stránce	stránka	k1gFnSc6
Památky	památka	k1gFnSc2
a	a	k8xC
příroda	příroda	k1gFnSc1
Karlovarska	Karlovarsko	k1gNnSc2
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Vyčichlo	vyčichnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Socha	Socha	k1gMnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
–	–	k?
článek	článek	k1gInSc1
na	na	k7c6
stránce	stránka	k1gFnSc6
Infocentrum	infocentrum	k1gNnSc1
města	město	k1gNnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Socha	Socha	k1gMnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
–	–	k?
na	na	k7c6
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Památky	památka	k1gFnPc1
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
Seznam	seznam	k1gInSc1
objektů	objekt	k1gInPc2
karlovarských	karlovarský	k2eAgInPc2d1
lázeňských	lázeňský	k2eAgInPc2d1
lesů	les	k1gInPc2
a	a	k8xC
parků	park	k1gInPc2
Rozhledny	rozhledna	k1gFnPc1
</s>
<s>
Diana	Diana	k1gFnSc1
</s>
<s>
Doubská	Doubský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Aberg	Aberg	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Goethova	Goethův	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
</s>
<s>
vyhlídka	vyhlídka	k1gFnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlídky	vyhlídka	k1gFnPc1
</s>
<s>
altán	altán	k1gInSc1
Camera	Camer	k1gMnSc2
obscura	obscur	k1gMnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
sedátko	sedátko	k1gNnSc1
/	/	kIx~
Paraplíčko	Paraplíčko	k?
</s>
<s>
Dorotin	Dorotin	k2eAgInSc1d1
altán	altán	k1gInSc1
/	/	kIx~
chrámek	chrámek	k1gInSc1
kněžny	kněžna	k1gFnSc2
Kuronské	Kuronský	k2eAgFnSc2d1
</s>
<s>
Jungmannův	Jungmannův	k2eAgInSc1d1
altán	altán	k1gInSc1
</s>
<s>
Keglevičův	Keglevičův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
</s>
<s>
altán	altán	k1gInSc1
Kristýna	Kristýna	k1gFnSc1
</s>
<s>
Mayerův	Mayerův	k2eAgInSc1d1
gloriet	gloriet	k1gInSc1
/	/	kIx~
vyhlídka	vyhlídka	k1gFnSc1
Jelení	jelení	k2eAgFnSc1d1
skok	skok	k1gInSc4
</s>
<s>
altán	altán	k1gInSc1
Panorama	panorama	k1gNnSc1
</s>
<s>
Petrova	Petrův	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
</s>
<s>
Rohanův	Rohanův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
</s>
<s>
chata	chata	k1gFnSc1
Rusalka	rusalka	k1gFnSc1
</s>
<s>
Tři	tři	k4xCgInPc1
kříže	kříž	k1gInPc1
</s>
<s>
Vídeňské	vídeňský	k2eAgNnSc4d1
sedátko	sedátko	k1gNnSc4
Památníky	památník	k1gInPc4
<g/>
,	,	kIx,
pomníky	pomník	k1gInPc4
</s>
<s>
pomník	pomník	k1gInSc4
Ludwiga	Ludwig	k1gMnSc2
van	vana	k1gFnPc2
Beethovena	Beethoven	k1gMnSc2
</s>
<s>
pomník	pomník	k1gInSc1
Antonína	Antonín	k1gMnSc2
Dvořáka	Dvořák	k1gMnSc2
</s>
<s>
pomník	pomník	k1gInSc1
Jurije	Jurije	k1gMnSc2
Gagarina	Gagarin	k1gMnSc2
</s>
<s>
busta	busta	k1gFnSc1
Johanna	Johann	k1gMnSc2
Wolfganga	Wolfgang	k1gMnSc2
von	von	k1gInSc1
Goetha	Goetha	k1gFnSc1
</s>
<s>
busta	busta	k1gFnSc1
Galluse	Galluse	k1gFnSc1
von	von	k1gInSc1
Hochbergera	Hochbergera	k1gFnSc1
</s>
<s>
památník	památník	k1gInSc1
Fryderyka	Fryderyek	k1gMnSc2
Chopina	Chopin	k1gMnSc2
</s>
<s>
socha	socha	k1gFnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
sady	sada	k1gFnSc2
Jeana	Jean	k1gMnSc2
de	de	k?
Carro	Carro	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
socha	socha	k1gFnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
sady	sada	k1gFnSc2
Karla	Karel	k1gMnSc2
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Kniha	kniha	k1gFnSc1
</s>
<s>
busta	busta	k1gFnSc1
Theodora	Theodor	k1gMnSc2
Körnera	Körner	k1gMnSc2
</s>
<s>
pomník	pomník	k1gInSc1
Tomáše	Tomáš	k1gMnSc2
Garrigua	Garriguus	k1gMnSc2
Masaryka	Masaryk	k1gMnSc2
</s>
<s>
pomník	pomník	k1gInSc1
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
</s>
<s>
busta	busta	k1gFnSc1
Adama	Adam	k1gMnSc2
Mickiewicze	Mickiewicze	k1gFnSc2
</s>
<s>
pomník	pomník	k1gInSc4
obětem	oběť	k1gFnPc3
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
busta	busta	k1gFnSc1
Petra	Petr	k1gMnSc2
Velikého	veliký	k2eAgInSc2d1
</s>
<s>
pomník	pomník	k1gInSc1
Friedricha	Friedrich	k1gMnSc2
Schillera	Schiller	k1gMnSc2
</s>
<s>
pomník	pomník	k1gInSc1
Karla	Karel	k1gMnSc2
Filipa	Filip	k1gMnSc2
Schwarzenberga	Schwarzenberg	k1gMnSc2
</s>
<s>
pomník	pomník	k1gInSc4
Bedřicha	Bedřich	k1gMnSc2
Smetany	smetana	k1gFnSc2
</s>
<s>
pomník	pomník	k1gInSc4
vojákům	voják	k1gMnPc3
Americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
Pamětní	pamětní	k2eAgFnSc2d1
desky	deska	k1gFnSc2
</s>
<s>
památník	památník	k1gInSc1
Fryderyka	Fryderyek	k1gMnSc2
Chopina	Chopin	k1gMnSc2
</s>
<s>
Óda	óda	k1gFnSc1
na	na	k7c4
Vřídlo	vřídlo	k1gNnSc4
</s>
<s>
pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
na	na	k7c6
Skále	skála	k1gFnSc6
kněžen	kněžna	k1gFnPc2
Sochy	socha	k1gFnSc2
<g/>
,	,	kIx,
plastiky	plastika	k1gFnPc4
a	a	k8xC
reliéfy	reliéf	k1gInPc4
</s>
<s>
Duch	duch	k1gMnSc1
pramenů	pramen	k1gInPc2
</s>
<s>
Hygie	Hygie	k1gFnSc1
(	(	kIx(
<g/>
Sadová	sadový	k2eAgFnSc1d1
kolonáda	kolonáda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hygie	Hygie	k1gFnSc1
(	(	kIx(
<g/>
Vřídelní	vřídelní	k2eAgFnSc1d1
kolonáda	kolonáda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
Fryderyka	Fryderyek	k1gMnSc2
Chopina	Chopin	k1gMnSc2
</s>
<s>
Jelen	Jelen	k1gMnSc1
</s>
<s>
Kamzík	kamzík	k1gMnSc1
/	/	kIx~
Jelení	jelení	k2eAgInSc1d1
skok	skok	k1gInSc1
</s>
<s>
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
průčelí	průčelí	k1gNnSc1
Městské	městský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Lví	lví	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
</s>
<s>
Panna	Panna	k1gFnSc1
Marie	Marie	k1gFnSc1
neposkvrněného	poskvrněný	k2eNgNnSc2d1
početí	početí	k1gNnSc2
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Nepomucký	Nepomucký	k1gMnSc1
(	(	kIx(
<g/>
u	u	k7c2
kostela	kostel	k1gInSc2
Sv.	sv.	kA
Máří	Máří	k?
Magdaleny	Magdalena	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Nepomucký	Nepomucký	k1gMnSc1
(	(	kIx(
<g/>
u	u	k7c2
Školní	školní	k2eAgFnSc2d1
lávky	lávka	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
socha	socha	k1gFnSc1
Bedřicha	Bedřich	k1gMnSc2
Smetany	smetana	k1gFnSc2
Sloupy	sloup	k1gInPc4
a	a	k8xC
obelisky	obelisk	k1gInPc4
</s>
<s>
Findlaterův	Findlaterův	k2eAgInSc1d1
obelisk	obelisk	k1gInSc1
</s>
<s>
sloup	sloup	k1gInSc1
se	s	k7c7
sochou	socha	k1gFnSc7
Karla	Karel	k1gMnSc2
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
sady	sada	k1gFnSc2
Jeana	Jean	k1gMnSc2
de	de	k?
Carro	Carro	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
sloup	sloup	k1gInSc1
se	s	k7c7
sochou	socha	k1gFnSc7
kočky	kočka	k1gFnSc2
</s>
<s>
sloup	sloup	k1gInSc1
se	s	k7c7
sousoším	sousoší	k1gNnSc7
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
sloup	sloup	k1gInSc1
</s>
<s>
pomník	pomník	k1gInSc1
Karla	Karel	k1gMnSc2
Filipa	Filip	k1gMnSc2
Schwarzenberga	Schwarzenberg	k1gMnSc2
</s>
<s>
Tereziin	Tereziin	k2eAgInSc1d1
obelisk	obelisk	k1gInSc1
</s>
<s>
sloup	sloup	k1gInSc1
vděčných	vděčný	k2eAgMnPc2d1
Maďarů	Maďar	k1gMnPc2
</s>
<s>
sloup	sloup	k1gInSc1
vévodů	vévoda	k1gMnPc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
Kaple	kaple	k1gFnSc2
a	a	k8xC
kapličky	kaplička	k1gFnSc2
</s>
<s>
Ave	ave	k1gNnSc1
Maria	Mario	k1gMnSc2
</s>
<s>
Ecce	Ecce	k5eAaImRp2nP
homo	homo	k1gNnSc4
(	(	kIx(
<g/>
ulice	ulice	k1gFnSc2
I.	I.	kA
P.	P.	kA
Pavlova	Pavlův	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Ecce	Ecce	k6eAd1
homo	homo	k6eAd1
(	(	kIx(
<g/>
lázeňské	lázeňský	k2eAgInPc4d1
lesy	les	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Panny	Panna	k1gFnPc1
Marie	Maria	k1gFnSc2
Bolestné	bolestné	k1gNnSc4
/	/	kIx~
Mariánská	mariánský	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1
pobožnost	pobožnost	k1gFnSc1
</s>
<s>
Svaté	svatý	k2eAgFnPc1d1
Anny	Anna	k1gFnPc1
/	/	kIx~
Schwarzova	Schwarzův	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
</s>
<s>
Svatého	svatý	k2eAgMnSc4d1
Linharta	Linhart	k1gMnSc4
</s>
<s>
Svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
</s>
<s>
U	u	k7c2
Obrazu	obraz	k1gInSc2
Kříže	kříž	k1gInSc2
a	a	k8xC
křížky	křížek	k1gInPc4
</s>
<s>
Keglevičův	Keglevičův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1
pobožnost	pobožnost	k1gFnSc1
</s>
<s>
Petrova	Petrův	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
</s>
<s>
Rohanův	Rohanův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
</s>
<s>
smírčí	smírčí	k2eAgInSc1d1
kříž	kříž	k1gInSc1
č.	č.	k?
0004	#num#	k4
</s>
<s>
smírčí	smírčí	k2eAgInSc1d1
kříž	kříž	k1gInSc1
č.	č.	k?
0005	#num#	k4
</s>
<s>
smírčí	smírčí	k2eAgInSc1d1
kříž	kříž	k1gInSc1
č.	č.	k?
1037	#num#	k4
/	/	kIx~
pamětní	pamětní	k2eAgInSc1d1
kříž	kříž	k1gInSc1
</s>
<s>
Tři	tři	k4xCgMnPc1
kříže	kříž	k1gInSc2
Pavilóny	pavilón	k1gInPc4
<g/>
,	,	kIx,
gloriety	gloriet	k1gInPc4
<g/>
,	,	kIx,
altány	altán	k1gInPc4
a	a	k8xC
větší	veliký	k2eAgFnPc4d2
chaty	chata	k1gFnPc4
</s>
<s>
altán	altán	k1gInSc1
Básníků	básník	k1gMnPc2
</s>
<s>
Beethovenův	Beethovenův	k2eAgInSc1d1
altán	altán	k1gInSc1
</s>
<s>
altán	altán	k1gInSc1
Bellevue	bellevue	k1gFnSc2
</s>
<s>
altán	altán	k1gInSc1
Camera	Camer	k1gMnSc2
obscura	obscur	k1gMnSc2
</s>
<s>
Dorotin	Dorotin	k2eAgInSc1d1
altán	altán	k1gInSc1
/	/	kIx~
chrámek	chrámek	k1gInSc1
kněžny	kněžna	k1gFnSc2
Kuronské	Kuronský	k2eAgFnSc2d1
</s>
<s>
pavilon	pavilon	k1gInSc1
Dorotka	Dorotka	k1gFnSc1
</s>
<s>
Fibichův	Fibichův	k2eAgInSc1d1
altán	altán	k1gInSc1
</s>
<s>
Findlaterův	Findlaterův	k2eAgInSc1d1
altán	altán	k1gInSc1
</s>
<s>
Chopinův	Chopinův	k2eAgInSc1d1
altán	altán	k1gInSc1
</s>
<s>
Jungmannův	Jungmannův	k2eAgInSc1d1
altán	altán	k1gInSc1
</s>
<s>
altán	altán	k1gInSc1
Aloise	Alois	k1gMnSc2
Kleina	Klein	k1gMnSc2
</s>
<s>
altán	altán	k1gInSc1
Kristýna	Kristýna	k1gFnSc1
</s>
<s>
Libušina	Libušin	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
Mayerův	Mayerův	k2eAgInSc1d1
gloriet	gloriet	k1gInSc1
/	/	kIx~
vyhlídla	vyhlídnout	k5eAaPmAgFnS
Jelení	jelení	k2eAgInSc4d1
skok	skok	k1gInSc4
</s>
<s>
altán	altán	k1gInSc1
Na	na	k7c6
Výšině	výšina	k1gFnSc6
</s>
<s>
altán	altán	k1gInSc1
Panorama	panorama	k1gNnSc1
</s>
<s>
altán	altán	k1gInSc4
pramenu	pramen	k1gInSc2
Svoboda	Svoboda	k1gMnSc1
</s>
<s>
chata	chata	k1gFnSc1
Přátelství	přátelství	k1gNnSc2
</s>
<s>
chata	chata	k1gFnSc1
Rusalka	rusalka	k1gFnSc1
</s>
<s>
altán	altán	k1gInSc1
Rybářská	rybářský	k2eAgFnSc1d1
bašta	bašta	k1gFnSc1
</s>
<s>
altán	altán	k1gInSc1
U	u	k7c2
Obrazu	obraz	k1gInSc2
</s>
<s>
altán	altán	k1gInSc1
U	u	k7c2
Tří	tři	k4xCgInPc2
křížů	kříž	k1gInPc2
Typizované	typizovaný	k2eAgFnSc2d1
či	či	k8xC
jednodušší	jednoduchý	k2eAgFnSc2d2
chaty	chata	k1gFnSc2
<g/>
,	,	kIx,
sruby	srub	k1gInPc4
a	a	k8xC
přístřeškyv	přístřeškyv	k1gInSc4
lázeňských	lázeňský	k2eAgFnPc2d1
lesích	les	k1gInPc6
</s>
<s>
Bezejmenná	bezejmenný	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
Čapkova	Čapkův	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
Doubská	Doubský	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
chata	chata	k1gFnSc1
Drahovice	Drahovice	k1gFnSc2
</s>
<s>
altán	altán	k1gInSc1
Ferdy	Ferda	k1gMnSc2
Mravence	mravenec	k1gMnSc2
</s>
<s>
Gogolova	Gogolův	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
chata	chata	k1gFnSc1
Hubertus	hubertus	k1gInSc1
</s>
<s>
Jezdecká	jezdecký	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
Kazatelna	kazatelna	k1gFnSc1
</s>
<s>
Křižíkova	Křižíkův	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
chata	chata	k1gFnSc1
Lesní	lesní	k2eAgFnSc1d1
pobožnost	pobožnost	k1gFnSc1
</s>
<s>
Luční	luční	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
</s>
<s>
chata	chata	k1gFnSc1
Myslivců	Myslivec	k1gMnPc2
</s>
<s>
chata	chata	k1gFnSc1
Na	na	k7c6
Hřišti	hřiště	k1gNnSc6
</s>
<s>
chata	chata	k1gFnSc1
Na	na	k7c6
Rozcestí	rozcestí	k1gNnSc6
</s>
<s>
chata	chata	k1gFnSc1
Na	na	k7c6
Skalce	skalka	k1gFnSc6
</s>
<s>
chata	chata	k1gFnSc1
Na	na	k7c6
Výsluní	výsluní	k1gNnSc6
</s>
<s>
chata	chata	k1gFnSc1
Na	na	k7c6
Zátiší	zátiší	k1gNnSc6
</s>
<s>
přístřešek	přístřešek	k1gInSc1
Nad	nad	k7c7
Lomem	lom	k1gInSc7
</s>
<s>
chata	chata	k1gFnSc1
Nad	nad	k7c7
Roklí	rokle	k1gFnSc7
</s>
<s>
přístřešek	přístřešek	k1gInSc1
Pod	pod	k7c7
Ottovou	Ottův	k2eAgFnSc7d1
výšinou	výšina	k1gFnSc7
</s>
<s>
chata	chata	k1gFnSc1
Pod	pod	k7c7
Střelnicí	střelnice	k1gFnSc7
</s>
<s>
Russelův	Russelův	k2eAgInSc1d1
srub	srub	k1gInSc1
</s>
<s>
chata	chata	k1gFnSc1
Skautů	skaut	k1gMnPc2
</s>
<s>
Smrkový	smrkový	k2eAgInSc1d1
srub	srub	k1gInSc1
</s>
<s>
chata	chata	k1gFnSc1
U	u	k7c2
Ave	ave	k1gNnSc2
Maria	Mario	k1gMnSc2
</s>
<s>
chata	chata	k1gFnSc1
U	u	k7c2
Černé	Černé	k2eAgFnSc2d1
Marie	Maria	k1gFnSc2
</s>
<s>
chata	chata	k1gFnSc1
U	u	k7c2
Křížku	křížek	k1gInSc2
</s>
<s>
chata	chata	k1gFnSc1
U	u	k7c2
Letiště	letiště	k1gNnSc2
</s>
<s>
chata	chata	k1gFnSc1
U	u	k7c2
Myslivny	myslivna	k1gFnSc2
</s>
<s>
altán	altán	k1gInSc1
U	u	k7c2
Vojty	Vojta	k1gMnSc2
</s>
<s>
chata	chata	k1gFnSc1
V	v	k7c6
Sedle	sedlo	k1gNnSc6
Lavičky	lavička	k1gFnSc2
a	a	k8xC
sedátka	sedátko	k1gNnSc2
</s>
<s>
lavička	lavička	k1gFnSc1
Allschlaraffia	Allschlaraffia	k1gFnSc1
</s>
<s>
Aranyho	Aranyze	k6eAd1
lavička	lavička	k1gFnSc1
</s>
<s>
lavička	lavička	k1gFnSc1
Básníků	básník	k1gMnPc2
</s>
<s>
České	český	k2eAgNnSc1d1
sedátko	sedátko	k1gNnSc1
/	/	kIx~
Paraplíčko	Paraplíčko	k?
</s>
<s>
lavička	lavička	k1gFnSc1
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
</s>
<s>
Páralovo	Páralův	k2eAgNnSc1d1
sedátko	sedátko	k1gNnSc1
</s>
<s>
Schwabeho	Schwabeze	k6eAd1
lavička	lavička	k1gFnSc1
</s>
<s>
lavička	lavička	k1gFnSc1
U	u	k7c2
Obrazu	obraz	k1gInSc2
</s>
<s>
Vídeňské	vídeňský	k2eAgNnSc1d1
sedátko	sedátko	k1gNnSc1
</s>
<s>
Wolfova	Wolfův	k2eAgFnSc1d1
lavička	lavička	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
