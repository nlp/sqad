<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
jamajský	jamajský	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
a	a	k8xC
kytarista	kytarista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mezinárodní	mezinárodní	k2eAgFnSc7d1
hudební	hudební	k2eAgFnSc7d1
a	a	k8xC
kulturní	kulturní	k2eAgFnSc7d1
ikonou	ikona	k1gFnSc7
<g/>
?	?	kIx.
</s>