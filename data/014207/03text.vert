<s>
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
</s>
<s>
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
Bob	Bob	k1gMnSc1
na	na	k7c6
vystoupení	vystoupení	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Robert	Robert	k1gMnSc1
Nesta	Nest	k1gMnSc2
Marley	Marlea	k1gMnSc2
Jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc1d1
</s>
<s>
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
Přezdívky	přezdívka	k1gFnSc2
</s>
<s>
Král	Král	k1gMnSc1
Reggae	Regga	k1gInSc2
<g/>
,	,	kIx,
Tuff	Tuff	k1gMnSc1
Gong	gong	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1945	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Nine	Nin	k1gInSc2
Mile	mile	k6eAd1
<g/>
,	,	kIx,
Saint	Saint	k1gMnSc1
Ann	Ann	k1gMnSc1
Parish	Parish	k1gMnSc1
<g/>
,	,	kIx,
Jamajka	Jamajka	k1gFnSc1
Původ	původ	k1gInSc1
</s>
<s>
Trenchtown	Trenchtown	k1gInSc1
<g/>
,	,	kIx,
Kingston	Kingston	k1gInSc1
<g/>
,	,	kIx,
Jamajka	Jamajka	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
36	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Miami	Miami	k1gNnPc2
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
akrální	akrální	k2eAgInSc1d1
lentiginózní	lentiginózní	k2eAgInSc1d1
melanom	melanom	k1gInSc1
Žánry	žánr	k1gInPc1
</s>
<s>
Reggae	Reggae	k1gFnSc1
<g/>
,	,	kIx,
ska	ska	k?
<g/>
,	,	kIx,
rocksteady	rocksteada	k1gFnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
zpěvák-skladatel	zpěvák-skladatel	k1gMnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
Zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnPc1
Aktivní	aktivní	k2eAgFnPc1d1
roky	rok	k1gInPc4
</s>
<s>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Studio	studio	k1gNnSc1
One	One	k1gFnSc2
<g/>
,	,	kIx,
Beverley	Beverlea	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
Upsetter	Upsetter	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Trojan	Trojan	k1gMnSc1
Records	Recordsa	k1gFnPc2
<g/>
,	,	kIx,
Island	Island	k1gInSc1
<g/>
/	/	kIx~
<g/>
Tuff	Tuff	k1gMnSc1
Gong	gong	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
The	The	k?
Wailers	Wailers	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
Rock	rock	k1gInSc1
and	and	k?
Roll	Roll	k1gInSc1
Hall	Hall	k1gInSc1
of	of	k?
Fame	Fame	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
Cena	cena	k1gFnSc1
Grammy	Gramma	k1gFnSc2
za	za	k7c4
celoživotní	celoživotní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Rita	Rita	k1gFnSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
Cindy	Cinda	k1gFnSc2
Breakspeare	Breakspear	k1gMnSc5
Děti	dítě	k1gFnPc1
</s>
<s>
Ziggy	Zigg	k1gInPc1
MarleyCedella	MarleyCedella	k1gFnSc1
MarleyStephen	MarleyStephen	k1gInSc1
MarleyRohan	MarleyRohan	k1gMnSc1
MarleyJulian	MarleyJulian	k1gMnSc1
MarleyKy-Mani	MarleyKy-Man	k1gMnPc1
MarleyDamian	MarleyDamian	k1gMnSc1
MarleySharon	MarleySharon	k1gMnSc1
Marley	Marlea	k1gMnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Norval	Norvat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
Marley	Marlea	k1gFnPc4
a	a	k8xC
Cedella	Cedella	k1gMnSc1
Booker	Booker	k1gMnSc1
Web	web	k1gInSc4
</s>
<s>
www.bobmarley.com	www.bobmarley.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bob	Bob	k1gMnSc1
Marley	Marley	k1gMnSc1
<g/>
,	,	kIx,
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Robert	Roberta	k1gFnPc2
Nesta	Nest	k1gMnSc2
Marley	Marlea	k1gMnSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
Saint	Saint	k1gMnSc1
Ann	Ann	k1gMnSc1
Parish	Parish	k1gMnSc1
<g/>
,	,	kIx,
Jamajka	Jamajka	k1gFnSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
<g/>
,	,	kIx,
Miami	Miami	k1gNnSc1
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
jamajský	jamajský	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
a	a	k8xC
kytarista	kytarista	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
mezinárodní	mezinárodní	k2eAgFnSc7d1
hudební	hudební	k2eAgFnSc7d1
a	a	k8xC
kulturní	kulturní	k2eAgFnSc7d1
ikonou	ikona	k1gFnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
skladbách	skladba	k1gFnPc6
mísil	mísit	k5eAaImAgInS
převážně	převážně	k6eAd1
reggae	reggae	k1gInSc1
<g/>
,	,	kIx,
ska	ska	k?
a	a	k8xC
rocksteady	rocksteada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
se	s	k7c7
skupinou	skupina	k1gFnSc7
The	The	k1gFnSc2
Wailers	Wailersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
vytvořil	vytvořit	k5eAaPmAgInS
výrazný	výrazný	k2eAgInSc1d1
autorský	autorský	k2eAgInSc1d1
a	a	k8xC
vokální	vokální	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
později	pozdě	k6eAd2
rezonoval	rezonovat	k5eAaImAgInS
s	s	k7c7
diváky	divák	k1gMnPc7
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wailers	Wailersa	k1gFnPc2
vydali	vydat	k5eAaPmAgMnP
některé	některý	k3yIgFnPc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
prvních	první	k4xOgFnPc2
reggae	reggae	k1gFnPc2
nahrávek	nahrávka	k1gFnPc2
s	s	k7c7
producentem	producent	k1gMnSc7
Lee	Lea	k1gFnSc3
"	"	kIx"
<g/>
Scratch	Scratch	k1gInSc1
<g/>
"	"	kIx"
Perrym	Perrym	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
se	se	k3xPyFc4
Wailers	Wailers	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
rozpadli	rozpadnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
Marley	Marlea	k1gFnSc2
vydal	vydat	k5eAaPmAgInS
na	na	k7c4
sólovou	sólový	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
,	,	kIx,
přestěhoval	přestěhovat	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
Anglie	Anglie	k1gFnSc2
a	a	k8xC
zažil	zažít	k5eAaPmAgMnS
vrchol	vrchol	k1gInSc4
vydáním	vydání	k1gNnSc7
alba	album	k1gNnSc2
Exodus	Exodus	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
získal	získat	k5eAaPmAgMnS
celosvětovou	celosvětový	k2eAgFnSc4d1
reputaci	reputace	k1gFnSc4
a	a	k8xC
zvýšil	zvýšit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
status	status	k1gInSc4
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejprodávanějších	prodávaný	k2eAgMnPc2d3
umělců	umělec	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
prodejem	prodej	k1gInSc7
více	hodně	k6eAd2
než	než	k8xS
75	#num#	k4
milionů	milion	k4xCgInPc2
nahrávek	nahrávka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Album	album	k1gNnSc1
Exodus	Exodus	k1gInSc4
zůstalo	zůstat	k5eAaPmAgNnS
v	v	k7c6
britských	britský	k2eAgInPc6d1
žebříčcích	žebříček	k1gInPc6
po	po	k7c4
dobu	doba	k1gFnSc4
56	#num#	k4
po	po	k7c6
sobě	se	k3xPyFc3
jdoucích	jdoucí	k2eAgMnPc2d1
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahovalo	obsahovat	k5eAaImAgNnS
čtyři	čtyři	k4xCgInPc1
britské	britský	k2eAgInPc1d1
hitové	hitový	k2eAgInPc1d1
singly	singl	k1gInPc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Exodus	Exodus	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Waiting	Waiting	k1gInSc1
in	in	k?
Vain	Vain	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Jamming	Jamming	k1gInSc1
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
One	One	k1gFnSc1
Love	lov	k1gInSc5
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
vydal	vydat	k5eAaPmAgInS
album	album	k1gNnSc4
Kaya	Kayum	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
obsahovalo	obsahovat	k5eAaImAgNnS
singly	singl	k1gInPc7
"	"	kIx"
<g/>
Is	Is	k1gFnSc1
This	Thisa	k1gFnPc2
Love	lov	k1gInSc5
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
Satisfy	Satisf	k1gInPc1
My	my	k3xPp1nPc1
Soul	Soul	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
největších	veliký	k2eAgInPc2d3
hitů	hit	k1gInPc2
<g/>
,	,	kIx,
Legend	legenda	k1gFnPc2
<g/>
,	,	kIx,
vyšla	vyjít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nejlépe	dobře	k6eAd3
prodávaným	prodávaný	k2eAgMnSc7d1
reggae	reggaat	k5eAaPmIp3nS
albem	album	k1gNnSc7
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
byla	být	k5eAaImAgFnS
umělci	umělec	k1gMnPc7
na	na	k7c6
noze	noha	k1gFnSc6
diagnostikována	diagnostikovat	k5eAaBmNgFnS
rakovina	rakovina	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
podlehl	podlehnout	k5eAaPmAgMnS
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
v	v	k7c6
Miami	Miami	k1gNnSc6
ve	v	k7c6
věku	věk	k1gInSc6
36	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
oddaným	oddaný	k2eAgInSc7d1
rastafariánem	rastafarián	k1gInSc7
se	s	k7c7
smyslem	smysl	k1gInSc7
pro	pro	k7c4
duchovnost	duchovnost	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
inspirovalo	inspirovat	k5eAaBmAgNnS
jeho	jeho	k3xOp3gFnSc4
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
připisována	připisován	k2eAgFnSc1d1
popularizace	popularizace	k1gFnSc1
reggae	regga	k1gFnSc2
hudby	hudba	k1gFnSc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
také	také	k9
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
symbol	symbol	k1gInSc1
jamajské	jamajský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Rolling	Rolling	k1gInSc4
Stone	ston	k1gInSc5
ho	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
zařadil	zařadit	k5eAaPmAgInS
na	na	k7c4
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
seznamu	seznam	k1gInSc6
nejlepších	dobrý	k2eAgMnPc2d3
hudebních	hudební	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1945	#num#	k4
na	na	k7c6
farmě	farma	k1gFnSc6
dědečka	dědeček	k1gMnSc2
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
v	v	k7c6
Nine	Nine	k1gFnSc6
Mile	mile	k6eAd1
v	v	k7c4
Saint	Saint	k1gInSc4
Ann	Ann	k1gMnSc1
Parish	Parish	k1gMnSc1
na	na	k7c6
Jamajce	Jamajka	k1gFnSc6
Norvalu	Norvala	k1gFnSc4
Sinclairu	Sinclair	k1gMnSc3
Marleyemu	Marleyem	k1gMnSc3
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Cedelle	Cedelle	k1gFnSc1
Booker	Bookra	k1gFnPc2
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Norval	Norval	k1gMnSc1
Marley	Marlea	k1gFnSc2
byl	být	k5eAaImAgMnS
bílý	bílý	k2eAgMnSc1d1
Jamajčan	Jamajčan	k1gMnSc1
původně	původně	k6eAd1
ze	z	k7c2
Sussexu	Sussex	k1gInSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
rodina	rodina	k1gFnSc1
prohlašovala	prohlašovat	k5eAaImAgFnS
syrský	syrský	k2eAgInSc4d1
židovský	židovský	k2eAgInSc4d1
původ	původ	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Norval	Norval	k1gFnPc1
měl	mít	k5eAaImAgInS
sloužit	sloužit	k5eAaImF
jako	jako	k9
kapitán	kapitán	k1gMnSc1
námořní	námořní	k2eAgFnSc2d1
složky	složka	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Royal	Royal	k1gMnSc1
Marines	Marines	k1gMnSc1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
manželství	manželství	k1gNnSc2
s	s	k7c7
Cedellou	Cedella	k1gFnSc7
Booker	Bookra	k1gFnPc2
-	-	kIx~
tehdy	tehdy	k6eAd1
18	#num#	k4
<g/>
letou	letý	k2eAgFnSc7d1
Afrojamajčankou	Afrojamajčanka	k1gFnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zaměstnán	zaměstnat	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
dozorce	dozorce	k1gMnSc1
na	na	k7c6
plantáži	plantáž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Plné	plný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
Boba	Bob	k1gMnSc2
Marleyho	Marley	k1gMnSc2
je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
Robert	Robert	k1gMnSc1
Nesta	Nest	k1gInSc2
Marley	Marlea	k1gFnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
některé	některý	k3yIgInPc1
zdroje	zdroj	k1gInPc1
dokládají	dokládat	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnSc4
rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
jako	jako	k9
Nesta	Nest	k1gMnSc4
Robert	Robert	k1gMnSc1
Marley	Marlea	k1gFnSc2
s	s	k7c7
vysvětlením	vysvětlení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
byl	být	k5eAaImAgMnS
Marley	Marlea	k1gFnPc4
ještě	ještě	k9
chlapec	chlapec	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
jamajský	jamajský	k2eAgMnSc1d1
pasový	pasový	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
prohodil	prohodit	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
první	první	k4xOgInSc4
a	a	k8xC
střední	střední	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
Nesta	Nest	k1gMnSc4
znělo	znět	k5eAaImAgNnS
jako	jako	k9
dívčí	dívčí	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Norval	Norval	k1gMnSc1
poskytoval	poskytovat	k5eAaImAgMnS
manželce	manželka	k1gFnSc3
a	a	k8xC
dítěti	dítě	k1gNnSc6
finanční	finanční	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zřídkakdy	zřídkakdy	k6eAd1
je	být	k5eAaImIp3nS
viděl	vidět	k5eAaImAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgInS
často	často	k6eAd1
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
navštěvoval	navštěvovat	k5eAaImAgInS
Stepney	Stepney	k1gInPc4
Primary	Primara	k1gFnSc2
a	a	k8xC
Junior	junior	k1gMnSc1
High	Higha	k1gFnPc2
School	Schoola	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
zátoku	zátoka	k1gFnSc4
Saint	Saint	k1gMnSc1
Ann	Ann	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
Bobu	bob	k1gInSc3
Marleymu	Marleym	k1gInSc2
10	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
infarkt	infarkt	k1gInSc4
ve	v	k7c6
věku	věk	k1gInSc6
70	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
let	let	k1gInSc4
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Marleyho	Marley	k1gMnSc2
matka	matka	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vdala	vdát	k5eAaPmAgFnS
za	za	k7c2
Edwarda	Edward	k1gMnSc2
Bookera	Booker	k1gMnSc2
<g/>
,	,	kIx,
amerického	americký	k2eAgMnSc2d1
státního	státní	k2eAgMnSc2d1
úředníka	úředník	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
měl	mít	k5eAaImAgMnS
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gMnSc2
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
dva	dva	k4xCgInPc4
americké	americký	k2eAgMnPc4d1
bratry	bratr	k1gMnPc4
-	-	kIx~
Richarda	Richard	k1gMnSc4
a	a	k8xC
Anthonyho	Anthony	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marley	Marlea	k1gFnPc1
a	a	k8xC
Neville	Neville	k1gFnPc1
Livingston	Livingston	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
známý	známý	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Bunny	Bunna	k1gFnPc1
Wailer	Wailer	k1gInSc1
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
v	v	k7c6
Nine	Nine	k1gNnSc6
Mile	mile	k6eAd1
kamarádi	kamarád	k1gMnPc1
už	už	k6eAd1
od	od	k7c2
dětství	dětství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začali	začít	k5eAaPmAgMnP
společně	společně	k6eAd1
vystupovat	vystupovat	k5eAaImF
na	na	k7c4
Stepney	Stepne	k2eAgFnPc4d1
Primary	Primara	k1gFnPc4
a	a	k8xC
Junior	junior	k1gMnSc1
High	Higha	k1gFnPc2
School	Schoola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Marley	Marlea	k1gFnSc2
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
matkou	matka	k1gFnSc7
domov	domov	k1gInSc4
v	v	k7c4
Nine	Nine	k1gInSc4
Mile	mile	k6eAd1
opustil	opustit	k5eAaPmAgMnS
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
12	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
přestěhoval	přestěhovat	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
Trenchtown	Trenchtowna	k1gFnPc2
v	v	k7c6
Kingstonu	Kingston	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cedella	Cedella	k1gMnSc1
Booker	Booker	k1gMnSc1
a	a	k8xC
Thadeus	Thadeus	k1gMnSc1
Livingston	Livingston	k1gInSc1
(	(	kIx(
<g/>
otec	otec	k1gMnSc1
Bunny	Bunna	k1gMnSc2
Wailera	Wailer	k1gMnSc2
<g/>
)	)	kIx)
měli	mít	k5eAaImAgMnP
společně	společně	k6eAd1
dceru	dcera	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
Claudette	Claudett	k1gInSc5
Pearlová	Pearlová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
byla	být	k5eAaImAgFnS
mladší	mladý	k2eAgFnSc7d2
sestrou	sestra	k1gFnSc7
jak	jak	k8xS,k8xC
Boba	Bob	k1gMnSc4
<g/>
,	,	kIx,
tak	tak	k9
Bunnyho	Bunny	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
Marleyovi	Marleyův	k2eAgMnPc1d1
a	a	k8xC
Livingstonovi	Livingstonův	k2eAgMnPc1d1
žili	žít	k5eAaImAgMnP
společně	společně	k6eAd1
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
domě	dům	k1gInSc6
v	v	k7c6
Trenchtownu	Trenchtown	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc1
hudební	hudební	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
prohlubovali	prohlubovat	k5eAaImAgMnP
a	a	k8xC
poslouchali	poslouchat	k5eAaImAgMnP
nejnovější	nový	k2eAgInSc4d3
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
z	z	k7c2
amerických	americký	k2eAgFnPc2d1
rozhlasových	rozhlasový	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc4
vysílání	vysílání	k1gNnSc1
na	na	k7c4
Jamajku	Jamajka	k1gFnSc4
dosáhlo	dosáhnout	k5eAaPmAgNnS
a	a	k8xC
nové	nový	k2eAgFnPc1d1
hudby	hudba	k1gFnPc1
-	-	kIx~
ska	ska	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Přesun	přesun	k1gInSc1
do	do	k7c2
Trenchtownu	Trenchtown	k1gInSc2
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgMnS
jako	jako	k9
osudový	osudový	k2eAgMnSc1d1
a	a	k8xC
Marley	Marlea	k1gFnPc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
ocitl	ocitnout	k5eAaPmAgInS
ve	v	k7c6
vokální	vokální	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
s	s	k7c7
Bunny	Bunn	k1gMnPc7
Wailerem	Wailer	k1gMnSc7
<g/>
,	,	kIx,
Peterem	Peter	k1gMnSc7
Toshem	Tosh	k1gInSc7
<g/>
,	,	kIx,
Beverley	Beverleum	k1gNnPc7
Kelsoem	Kelsoem	k1gInSc1
a	a	k8xC
Junior	junior	k1gMnSc1
Braithwaitem	Braithwait	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnSc1
Higgs	Higgsa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
úspěšného	úspěšný	k2eAgInSc2d1
vokálního	vokální	k2eAgInSc2d1
aktu	akt	k1gInSc2
Higgs	Higgsa	k1gFnPc2
a	a	k8xC
Wilson	Wilsona	k1gFnPc2
<g/>
,	,	kIx,
sídlil	sídlit	k5eAaImAgInS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
St.	st.	kA
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
zpěvák	zpěvák	k1gMnSc1
Roy	Roy	k1gMnSc1
Wilson	Wilson	k1gMnSc1
byl	být	k5eAaImAgMnS
vychován	vychovat	k5eAaPmNgMnS
babičkou	babička	k1gFnSc7
Juniora	junior	k1gMnSc2
Braithwaita	Braithwait	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Higgs	Higgsa	k1gFnPc2
a	a	k8xC
Wilson	Wilsona	k1gFnPc2
zkoušeli	zkoušet	k5eAaImAgMnP
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
domu	dům	k1gInSc2
mezi	mezi	k7c7
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
ulicí	ulice	k1gFnSc7
a	a	k8xC
nedlouho	dlouho	k6eNd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
Marley	Marlea	k1gMnSc2
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
bydlící	bydlící	k2eAgMnPc1d1
ve	v	k7c4
2	#num#	k4
<g/>
.	.	kIx.
ulici	ulice	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Junior	junior	k1gMnSc1
Braithwaite	Braithwait	k1gInSc5
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
shromáždili	shromáždit	k5eAaPmAgMnP
kolem	kolem	k7c2
tohoto	tento	k3xDgNnSc2
úspěšného	úspěšný	k2eAgNnSc2d1
dua	duo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Marley	Marleum	k1gNnPc7
ani	ani	k9
ostatní	ostatní	k2eAgMnSc1d1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
nehráli	hrát	k5eNaImAgMnP
na	na	k7c4
žádné	žádný	k3yNgInPc4
nástroje	nástroj	k1gInPc4
a	a	k8xC
měli	mít	k5eAaImAgMnP
větší	veliký	k2eAgInSc4d2
zájem	zájem	k1gInSc4
být	být	k5eAaImF
vokální	vokální	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
souboru	soubor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Higgs	Higgs	k1gInSc1
byl	být	k5eAaImAgInS
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
pomohl	pomoct	k5eAaPmAgMnS
rozvíjet	rozvíjet	k5eAaImF
jejich	jejich	k3xOp3gInPc4
vokály	vokál	k1gInPc4
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
důležitější	důležitý	k2eAgMnSc1d2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
začal	začít	k5eAaPmAgInS
Marleyho	Marley	k1gMnSc4
učit	učit	k5eAaImF
hrát	hrát	k5eAaImF
na	na	k7c4
kytaru	kytara	k1gFnSc4
-	-	kIx~
a	a	k8xC
tak	tak	k6eAd1
vytvořil	vytvořit	k5eAaPmAgInS
základ	základ	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
později	pozdě	k6eAd2
Marleymu	Marleym	k1gInSc3
umožnil	umožnit	k5eAaPmAgInS
složit	složit	k5eAaPmF
některé	některý	k3yIgFnPc4
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
reggae	reggae	k1gFnPc2
hitů	hit	k1gInPc2
v	v	k7c6
historii	historie	k1gFnSc6
žánru	žánr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1962	#num#	k4
<g/>
-	-	kIx~
<g/>
72	#num#	k4
<g/>
:	:	kIx,
Rané	raný	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
1962	#num#	k4
nahrál	nahrát	k5eAaBmAgMnS,k5eAaPmAgMnS
Marley	Marle	k2eAgFnPc4d1
čtyři	čtyři	k4xCgFnPc4
skladby	skladba	k1gFnPc4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Judge	Judge	k1gFnSc1
Not	nota	k1gFnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
One	One	k1gFnSc1
Cup	cup	k1gInSc1
of	of	k?
Coffee	Coffee	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Do	do	k7c2
You	You	k1gFnSc2
Still	Stilla	k1gFnPc2
Love	lov	k1gInSc5
Me	Me	k1gMnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
Terror	Terror	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ve	v	k7c6
Federal	Federal	k1gFnSc6
Studios	Studios	k?
pro	pro	k7c4
místního	místní	k2eAgMnSc4d1
producenta	producent	k1gMnSc4
Leslie	Leslie	k1gFnSc2
Konga	Kongo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Tři	tři	k4xCgMnPc1
ze	z	k7c2
skladeb	skladba	k1gFnPc2
byly	být	k5eAaImAgInP
vydány	vydat	k5eAaPmNgInP
na	na	k7c4
Beverley	Beverlea	k1gFnPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
s	s	k7c7
"	"	kIx"
<g/>
One	One	k1gMnSc2
Cup	cup	k1gInSc1
of	of	k?
Coffee	Coffee	k1gInSc1
<g/>
"	"	kIx"
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Bobby	Bobba	k1gFnSc2
Martell	Martell	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1963	#num#	k4
se	se	k3xPyFc4
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
<g/>
,	,	kIx,
Bunny	Bunna	k1gFnPc1
Wailer	Wailer	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Tosh	Tosh	k1gMnSc1
<g/>
,	,	kIx,
Junior	junior	k1gMnSc1
Braithwaite	Braithwait	k1gInSc5
<g/>
,	,	kIx,
Beverley	Beverley	k1gInPc1
Kelso	Kelsa	k1gFnSc5
<g/>
,	,	kIx,
a	a	k8xC
Cherry	Cherra	k1gFnSc2
Smith	Smitha	k1gFnPc2
nazývali	nazývat	k5eAaImAgMnP
The	The	k1gMnSc1
Teenagers	Teenagers	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
název	název	k1gInSc4
změnili	změnit	k5eAaPmAgMnP
na	na	k7c4
Wailing	Wailing	k1gInSc4
Rudeboys	Rudeboysa	k1gFnPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
na	na	k7c4
Wailing	Wailing	k1gInSc4
Wailers	Wailers	k1gInSc4
-	-	kIx~
v	v	k7c4
ten	ten	k3xDgInSc4
moment	moment	k1gInSc4
je	být	k5eAaImIp3nS
objevil	objevit	k5eAaPmAgMnS
producent	producent	k1gMnSc1
Coxsone	Coxson	k1gInSc5
Dodd	Doddo	k1gNnPc2
a	a	k8xC
konečně	konečně	k6eAd1
pak	pak	k6eAd1
na	na	k7c4
Wailers	Wailers	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnPc4
singl	singl	k1gInSc1
"	"	kIx"
<g/>
Simmer	Simmer	k1gInSc1
Down	Down	k1gInSc1
<g/>
"	"	kIx"
pro	pro	k7c4
Coxsonovův	Coxsonovův	k2eAgInSc4d1
label	label	k1gInSc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
únoru	únor	k1gInSc6
1964	#num#	k4
jamajským	jamajský	k2eAgNnSc7d1
#	#	kIx~
<g/>
1	#num#	k4
nejprodávanějším	prodávaný	k2eAgInSc7d3
singlem	singl	k1gInSc7
s	s	k7c7
odhadem	odhad	k1gInSc7
70	#num#	k4
000	#num#	k4
prodaných	prodaný	k2eAgFnPc2d1
kopií	kopie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
The	The	k1gFnSc1
Wailers	Wailersa	k1gFnPc2
nyní	nyní	k6eAd1
začali	začít	k5eAaPmAgMnP
pravidelně	pravidelně	k6eAd1
nahrávat	nahrávat	k5eAaImF
pro	pro	k7c4
Studio	studio	k1gNnSc4
One	One	k1gFnSc2
a	a	k8xC
pracovali	pracovat	k5eAaImAgMnP
se	s	k7c7
zajetými	zajetý	k2eAgMnPc7d1
jamajskými	jamajský	k2eAgMnPc7d1
hudebníky	hudebník	k1gMnPc7
jako	jako	k8xS,k8xC
Ernest	Ernest	k1gMnSc1
Ranglin	Ranglin	k2eAgMnSc1d1
(	(	kIx(
<g/>
aranžér	aranžér	k1gMnSc1
"	"	kIx"
<g/>
It	It	k1gFnSc1
Hurts	Hurtsa	k1gFnPc2
To	to	k9
Be	Be	k1gMnSc5
Alone	Alon	k1gMnSc5
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
klávesista	klávesista	k1gMnSc1
Jackie	Jackie	k1gFnSc2
Mittoo	Mittoo	k1gMnSc1
a	a	k8xC
saxofonista	saxofonista	k1gMnSc1
Roland	Rolanda	k1gFnPc2
Alphonso	Alphonsa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
the	the	k?
Wailers	Wailersa	k1gFnPc2
opustili	opustit	k5eAaPmAgMnP
Braithwaite	Braithwait	k1gInSc5
<g/>
,	,	kIx,
Kelso	Kelsa	k1gFnSc5
a	a	k8xC
Smith	Smith	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
zůstalo	zůstat	k5eAaPmAgNnS
jádro	jádro	k1gNnSc1
-	-	kIx~
trio	trio	k1gNnSc1
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gMnSc2
<g/>
,	,	kIx,
Bunny	Bunna	k1gMnSc2
Wailer	Wailer	k1gMnSc1
a	a	k8xC
Peter	Peter	k1gMnSc1
Tosh	Tosh	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
se	se	k3xPyFc4
Marley	Marle	k1gMnPc4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Ritou	Rita	k1gFnSc7
Andersonovou	Andersonův	k2eAgFnSc7d1
a	a	k8xC
společně	společně	k6eAd1
nakrátko	nakrátko	k6eAd1
odjeli	odjet	k5eAaPmAgMnP
do	do	k7c2
USA	USA	kA
poblíž	poblíž	k7c2
bydliště	bydliště	k1gNnSc2
jeho	jeho	k3xOp3gFnSc2
matky	matka	k1gFnSc2
do	do	k7c2
Wilmingtonu	Wilmington	k1gInSc2
v	v	k7c6
Delaware	Delawar	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
asistent	asistent	k1gMnSc1
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
pro	pro	k7c4
DuPont	DuPont	k1gInSc4
a	a	k8xC
na	na	k7c6
montážní	montážní	k2eAgFnSc6d1
lince	linka	k1gFnSc6
pro	pro	k7c4
Chrysler	Chrysler	k1gInSc4
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Donald	Donald	k1gMnSc1
Marley	Marlea	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ačkoli	ačkoli	k8xS
Marley	Marle	k1gMnPc4
vyrůstal	vyrůstat	k5eAaImAgMnS
jako	jako	k8xC,k8xS
katolík	katolík	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
mimo	mimo	k7c4
vliv	vliv	k1gInSc4
své	svůj	k3xOyFgFnSc2
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgMnS
zajímat	zajímat	k5eAaImF
rastafariánství	rastafariánství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
návratu	návrat	k1gInSc6
na	na	k7c4
Jamajku	Jamajka	k1gFnSc4
formálně	formálně	k6eAd1
na	na	k7c6
rastafariánství	rastafariánství	k1gNnSc6
konvertoval	konvertovat	k5eAaBmAgMnS
nechal	nechat	k5eAaPmAgMnS
si	se	k3xPyFc3
narůst	narůst	k5eAaPmF
dready	dread	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
finančním	finanční	k2eAgInSc6d1
nesouladu	nesoulad	k1gInSc6
s	s	k7c7
Doddem	Dodd	k1gInSc7
se	se	k3xPyFc4
Marley	Marlea	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
skupina	skupina	k1gFnSc1
spojila	spojit	k5eAaPmAgFnS
s	s	k7c7
Lee	Lea	k1gFnSc3
"	"	kIx"
<g/>
Scratch	Scratch	k1gInSc1
<g/>
"	"	kIx"
Perrym	Perrym	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
studiovou	studiový	k2eAgFnSc7d1
kapelou	kapela	k1gFnSc7
The	The	k1gFnSc2
Upsetters	Upsettersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
spolupráce	spolupráce	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
méně	málo	k6eAd2
než	než	k8xS
rok	rok	k1gInSc4
<g/>
,	,	kIx,
nahráli	nahrát	k5eAaBmAgMnP,k5eAaPmAgMnP
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
mnozí	mnohý	k2eAgMnPc1d1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
práci	práce	k1gFnSc4
Wailers	Wailersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marley	Marlea	k1gFnSc2
a	a	k8xC
Perry	Perra	k1gFnSc2
se	se	k3xPyFc4
rozešli	rozejít	k5eAaPmAgMnP
po	po	k7c6
sporu	spor	k1gInSc6
ohledně	ohledně	k7c2
přiřazení	přiřazení	k1gNnSc2
nahrávacích	nahrávací	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
zůstali	zůstat	k5eAaPmAgMnP
přáteli	přítel	k1gMnPc7
a	a	k8xC
za	za	k7c4
čas	čas	k1gInSc4
opět	opět	k6eAd1
spolupracovali	spolupracovat	k5eAaImAgMnP
</s>
<s>
Rok	rok	k1gInSc1
1969	#num#	k4
přinesl	přinést	k5eAaPmAgInS
další	další	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
jamajské	jamajský	k2eAgFnSc6d1
populární	populární	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
byl	být	k5eAaImAgInS
rytmus	rytmus	k1gInSc1
ještě	ještě	k6eAd1
více	hodně	k6eAd2
zpomalen	zpomalen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
beat	beat	k1gInSc1
měl	mít	k5eAaImAgInS
pomalý	pomalý	k2eAgInSc1d1
<g/>
,	,	kIx,
ustálený	ustálený	k2eAgInSc1d1
a	a	k8xC
tikající	tikající	k2eAgInSc1d1
rytmus	rytmus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
poprvé	poprvé	k6eAd1
slyšet	slyšet	k5eAaImF
v	v	k7c6
písni	píseň	k1gFnSc6
"	"	kIx"
<g/>
Do	do	k7c2
The	The	k1gFnSc2
Reggay	Reggaa	k1gFnSc2
<g/>
"	"	kIx"
od	od	k7c2
The	The	k1gFnSc2
Maytals	Maytalsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marley	Marleum	k1gNnPc7
se	se	k3xPyFc4
ještě	ještě	k9
více	hodně	k6eAd2
přiblížil	přiblížit	k5eAaPmAgMnS
k	k	k7c3
producentovi	producent	k1gMnSc3
Leslie	Leslie	k1gFnSc2
Kongovi	Kong	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
tvůrců	tvůrce	k1gMnPc2
reggae	regga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nahrávkám	nahrávka	k1gFnPc3
Kong	Kongo	k1gNnPc2
spojil	spojit	k5eAaPmAgMnS
Wailers	Wailers	k1gInSc4
s	s	k7c7
jeho	jeho	k3xOp3gMnPc7
studiovými	studiový	k2eAgMnPc7d1
hudebníky	hudebník	k1gMnPc7
nazvanými	nazvaný	k2eAgInPc7d1
Beverley	Beverley	k1gInPc7
<g/>
'	'	kIx"
<g/>
s	s	k7c7
All-Stars	All-Stars	k1gInSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
patřili	patřit	k5eAaImAgMnP
basisté	basista	k1gMnPc1
Lloyd	Lloydo	k1gNnPc2
Parks	Parksa	k1gFnPc2
a	a	k8xC
Jackie	Jackie	k1gFnSc2
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
bubeník	bubeník	k1gMnSc1
Paul	Paul	k1gMnSc1
Douglas	Douglas	k1gMnSc1
(	(	kIx(
<g/>
Paul	Paul	k1gMnSc1
Douglas	Douglas	k1gMnSc1
<g/>
)	)	kIx)
hráči	hráč	k1gMnPc1
na	na	k7c6
klávesy	klávesa	k1gFnSc2
Gladstone	Gladston	k1gInSc5
Anderson	Anderson	k1gMnSc1
a	a	k8xC
Winston	Winston	k1gInSc1
Wright	Wrighta	k1gFnPc2
a	a	k8xC
kytaristé	kytarista	k1gMnPc1
Rad	rada	k1gFnPc2
Bryan	Bryan	k1gMnSc1
<g/>
,	,	kIx,
Lynn	Lynn	k1gMnSc1
Taitt	Taitt	k1gMnSc1
a	a	k8xC
Hux	Hux	k1gMnSc1
Brown	Brown	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Jak	jak	k6eAd1
píše	psát	k5eAaImIp3nS
David	David	k1gMnSc1
Moskowitz	Moskowitz	k1gMnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Skladby	skladba	k1gFnPc1
nahrané	nahraný	k2eAgFnPc1d1
v	v	k7c6
tomto	tento	k3xDgNnSc6
složení	složení	k1gNnSc6
ilustrovaly	ilustrovat	k5eAaBmAgFnP
nejranější	raný	k2eAgNnSc4d3
úsilí	úsilí	k1gNnSc4
Wailers	Wailersa	k1gFnPc2
v	v	k7c6
novém	nový	k2eAgInSc6d1
stylu	styl	k1gInSc6
reggae	regga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmizely	zmizet	k5eAaPmAgInP
trubky	trubka	k1gFnPc4
a	a	k8xC
saxofony	saxofon	k1gInPc4
dřívějších	dřívější	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
ve	v	k7c6
stylu	styl	k1gInSc6
ska	ska	k?
a	a	k8xC
nyní	nyní	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
hrály	hrát	k5eAaImAgInP
instrumentální	instrumentální	k2eAgInPc1d1
breaky	break	k1gInPc1
na	na	k7c4
elektrickou	elektrický	k2eAgFnSc4d1
kytaru	kytara	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahrané	nahraný	k2eAgFnPc4d1
skladby	skladba	k1gFnPc4
byly	být	k5eAaImAgFnP
později	pozdě	k6eAd2
vydány	vydat	k5eAaPmNgFnP
na	na	k7c6
desce	deska	k1gFnSc6
The	The	k1gMnSc1
Best	Best	k1gMnSc1
of	of	k?
The	The	k1gFnSc2
Wailers	Wailersa	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
písní	píseň	k1gFnPc2
"	"	kIx"
<g/>
Soul	Soul	k1gInSc1
Shakedown	Shakedown	k1gNnSc1
Party	party	k1gFnPc2
<g/>
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Stop	stop	k2eAgInSc1d1
That	That	k2eAgInSc1d1
Train	Train	k1gInSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Caution	Caution	k1gInSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Go	Go	k1gMnSc1
Tell	Tell	k1gMnSc1
It	It	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Mountain	Mountain	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Soon	Soon	k1gInSc1
Come	Com	k1gFnSc2
<g/>
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Can	Can	k1gFnSc1
<g/>
’	’	k?
<g/>
t	t	k?
You	You	k1gMnSc1
See	See	k1gMnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Soul	Soul	k1gInSc1
Captives	Captivesa	k1gFnPc2
<g/>
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Cheer	Cheer	k1gMnSc1
Up	Up	k1gMnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Back	Back	k1gMnSc1
Out	Out	k1gMnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
Do	do	k7c2
It	It	k1gFnSc2
Twice	Twice	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1968	#num#	k4
a	a	k8xC
1972	#num#	k4
Bob	Bob	k1gMnSc1
a	a	k8xC
Rita	Rita	k1gFnSc1
Marley	Marlea	k1gFnSc2
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Tosh	Tosha	k1gFnPc2
a	a	k8xC
Bunny	Bunna	k1gFnSc2
Wailer	Wailer	k1gInSc1
přestříhali	přestříhat	k5eAaPmAgMnP
některé	některý	k3yIgFnPc4
starší	starý	k2eAgFnPc4d2
skladby	skladba	k1gFnPc4
pod	pod	k7c7
JAD	JAD	kA
Records	Records	k1gInSc1
v	v	k7c6
Kingstonu	Kingston	k1gInSc6
a	a	k8xC
Londýně	Londýn	k1gInSc6
ve	v	k7c6
snaze	snaha	k1gFnSc6
zkomerčnit	zkomerčnit	k5eAaPmF
zvuk	zvuk	k1gInSc4
Wailers	Wailersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bunny	Bunno	k1gNnPc7
později	pozdě	k6eAd2
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc4
skladby	skladba	k1gFnPc4
"	"	kIx"
<g/>
nikdy	nikdy	k6eAd1
neměly	mít	k5eNaImAgFnP
být	být	k5eAaImF
vydány	vydat	k5eAaPmNgFnP
na	na	k7c4
album	album	k1gNnSc4
<g/>
...	...	k?
<g/>
byly	být	k5eAaImAgFnP
to	ten	k3xDgNnSc1
jen	jen	k9
ukázky	ukázka	k1gFnPc1
nahrávek	nahrávka	k1gFnPc2
pro	pro	k7c4
nahrávací	nahrávací	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
Bob	bob	k1gInSc4
a	a	k8xC
Rita	rito	k1gNnPc4
navštívili	navštívit	k5eAaPmAgMnP
skladatele	skladatel	k1gMnSc4
Jimmyho	Jimmy	k1gMnSc2
Normana	Norman	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
bytě	byt	k1gInSc6
v	v	k7c6
Bronxu	Bronx	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norman	Norman	k1gMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
nějaké	nějaký	k3yIgInPc4
texty	text	k1gInPc4
pro	pro	k7c4
"	"	kIx"
<g/>
Time	Tim	k1gFnSc2
Is	Is	k1gFnSc2
on	on	k3xPp3gMnSc1
My	my	k3xPp1nPc1
Side	Side	k1gNnPc6
<g/>
"	"	kIx"
od	od	k7c2
Kai	Kai	k1gMnSc2
Windinga	Winding	k1gMnSc2
(	(	kIx(
<g/>
kryté	krytý	k2eAgFnPc1d1
Rolling	Rolling	k1gInSc4
Stones	Stonesa	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
psal	psát	k5eAaImAgMnS
pro	pro	k7c4
Johnnyho	Johnny	k1gMnSc4
Nashe	Nash	k1gInSc2
a	a	k8xC
Jimi	on	k3xPp3gInPc7
Hendrixe	Hendrixe	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Třídenní	třídenní	k2eAgInSc1d1
jam	jam	k1gInSc1
session	session	k1gInSc1
s	s	k7c7
Normanem	Norman	k1gMnSc7
a	a	k8xC
dalšími	další	k1gNnPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
Normanova	Normanův	k2eAgMnSc2d1
spoluautora	spoluautor	k1gMnSc2
Ala	ala	k1gFnSc1
Pyfroma	Pyfroma	k1gFnSc1
<g/>
,	,	kIx,
přineslo	přinést	k5eAaPmAgNnS
24	#num#	k4
<g/>
-minutou	-minutý	k2eAgFnSc4d1
stopu	stopa	k1gFnSc4
s	s	k7c7
Marleyho	Marley	k1gMnSc2
a	a	k8xC
Norman-Pyfromovými	Norman-Pyfromový	k2eAgFnPc7d1
kompozicemi	kompozice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
stopa	stopa	k1gFnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
Reggae	Regga	k1gMnSc2
archiváře	archivář	k1gMnSc2
Rogera	Rogera	k1gFnSc1
Steffense	Steffense	k1gFnSc1
vzácná	vzácný	k2eAgFnSc1d1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
ovlivněná	ovlivněný	k2eAgFnSc1d1
spíše	spíše	k9
popem	pop	k1gMnSc7
než	než	k8xS
reggae	reggae	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
snahy	snaha	k1gFnSc2
dostat	dostat	k5eAaPmF
Marleyho	Marley	k1gMnSc4
do	do	k7c2
amerických	americký	k2eAgInPc2d1
žebříčků	žebříček	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
článku	článek	k1gInSc2
v	v	k7c4
"	"	kIx"
<g/>
The	The	k1gFnSc4
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
"	"	kIx"
Marley	Marlea	k1gFnPc1
na	na	k7c6
stopě	stopa	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
experimentoval	experimentovat	k5eAaImAgMnS
s	s	k7c7
různými	různý	k2eAgInPc7d1
zvuky	zvuk	k1gInPc7
<g/>
,	,	kIx,
přijal	přijmout	k5eAaPmAgMnS
styl	styl	k1gInSc4
"	"	kIx"
<g/>
doo-wop	doo-wop	k1gInSc4
<g/>
"	"	kIx"
v	v	k7c6
písni	píseň	k1gFnSc6
"	"	kIx"
<g/>
Stay	Staa	k1gFnSc2
With	With	k1gMnSc1
Me	Me	k1gMnSc1
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
pomalý	pomalý	k2eAgInSc1d1
love	lov	k1gInSc5
song	song	k1gInSc1
ve	v	k7c6
stylu	styl	k1gInSc6
umělců	umělec	k1gMnPc2
let	léto	k1gNnPc2
šedesátých	šedesátý	k4xOgNnPc2
<g/>
"	"	kIx"
ve	v	k7c6
skladbě	skladba	k1gFnSc6
"	"	kIx"
<g/>
Splish	Splish	k1gInSc1
for	forum	k1gNnPc2
My	my	k3xPp1nPc1
Splash	Splash	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Umělec	umělec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
rodil	rodit	k5eAaImAgMnS
mimo	mimo	k7c4
svou	svůj	k3xOyFgFnSc4
rodnou	rodný	k2eAgFnSc4d1
Jamajku	Jamajka	k1gFnSc4
-	-	kIx~
Marley	Marley	k1gInPc4
-	-	kIx~
žil	žít	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
v	v	k7c4
Ridgmount	Ridgmount	k1gInSc4
Gardens	Gardensa	k1gFnPc2
v	v	k7c4
Bloomsbury	Bloomsbur	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1972	#num#	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
<g/>
:	:	kIx,
Přechod	přechod	k1gInSc1
k	k	k7c3
Island	Island	k1gInSc1
Records	Records	k1gInSc4
</s>
<s>
Rok	rok	k1gInSc1
1974	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
rokem	rok	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
z	z	k7c2
kapely	kapela	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
Peter	Peter	k1gMnSc1
Tosh	Tosh	k1gMnSc1
i	i	k9
Bunny	Bunen	k2eAgFnPc4d1
Livingstone	Livingston	k1gInSc5
a	a	k8xC
on	on	k3xPp3gMnSc1
byl	být	k5eAaImAgMnS
donucen	donucen	k2eAgMnSc1d1
postavit	postavit	k5eAaPmF
nové	nový	k2eAgInPc4d1
Wailers	Wailers	k1gInSc4
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
přejmenované	přejmenovaný	k2eAgInPc1d1
na	na	k7c4
Bob	bob	k1gInSc4
Marley	Marlea	k1gFnSc2
&	&	k?
Wailers	Wailers	k1gInSc1
<g/>
)	)	kIx)
včetně	včetně	k7c2
ženského	ženský	k2eAgNnSc2d1
pěveckého	pěvecký	k2eAgNnSc2d1
tria	trio	k1gNnSc2
s	s	k7c7
Ritou	Rita	k1gFnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
úspěchu	úspěch	k1gInSc3
s	s	k7c7
albem	album	k1gNnSc7
Natty	Natta	k1gFnSc2
dread	dread	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
stala	stát	k5eAaPmAgFnS
hvězda	hvězda	k1gFnSc1
celosvětového	celosvětový	k2eAgInSc2d1
formátu	formát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
zranil	zranit	k5eAaPmAgMnS
při	při	k7c6
fotbalu	fotbal	k1gInSc6
<g/>
,	,	kIx,
lékaři	lékař	k1gMnSc3
u	u	k7c2
něj	on	k3xPp3gNnSc2
zjistili	zjistit	k5eAaPmAgMnP
zhoubné	zhoubný	k2eAgNnSc4d1
rakovinné	rakovinný	k2eAgNnSc4d1
bujení	bujení	k1gNnSc4
(	(	kIx(
<g/>
patrně	patrně	k6eAd1
po	po	k7c6
otci	otec	k1gMnSc6
zdědil	zdědit	k5eAaPmAgMnS
predispozice	predispozice	k1gFnSc2
k	k	k7c3
rakovině	rakovina	k1gFnSc3
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
jinak	jinak	k6eAd1
u	u	k7c2
lidí	člověk	k1gMnPc2
černé	černý	k2eAgFnSc2d1
pleti	pleť	k1gFnSc2
velice	velice	k6eAd1
výjimečná	výjimečný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
mu	on	k3xPp3gMnSc3
nabídnuta	nabídnut	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
rastafariánská	rastafariánský	k2eAgFnSc1d1
víra	víra	k1gFnSc1
neuznávala	uznávat	k5eNaImAgFnS
„	„	k?
<g/>
zásahy	zásah	k1gInPc4
do	do	k7c2
lidského	lidský	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
horšící	horšící	k2eAgInSc4d1
se	se	k3xPyFc4
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
stále	stále	k6eAd1
koncertoval	koncertovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
vyprodal	vyprodat	k5eAaPmAgMnS
obrovskou	obrovský	k2eAgFnSc4d1
Madison	Madison	k1gInSc4
Square	square	k1gInSc4
Garden	Gardna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
oslavu	oslava	k1gFnSc4
koncertního	koncertní	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
vydal	vydat	k5eAaPmAgMnS
dvojalbum	dvojalbum	k1gNnSc4
Babylon	Babylon	k1gInSc1
by	by	k9
bus	bus	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
některými	některý	k3yIgFnPc7
recenzemi	recenze	k1gFnPc7
označováno	označovat	k5eAaImNgNnS
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgFnPc2d3
živých	živý	k2eAgFnPc2d1
reggae	reggae	k1gFnPc2
nahrávek	nahrávka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
vystoupení	vystoupení	k1gNnSc1
se	se	k3xPyFc4
odehrálo	odehrát	k5eAaPmAgNnS
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1980	#num#	k4
v	v	k7c6
Pittsburghu	Pittsburgh	k1gInSc6
v	v	k7c6
rámci	rámec	k1gInSc6
Uprising	Uprising	k1gInSc1
tour	tour	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc4d1
píseň	píseň	k1gFnSc4
dozpíval	dozpívat	k5eAaPmAgInS
s	s	k7c7
vypětím	vypětí	k1gNnSc7
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
konečně	konečně	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgMnS
s	s	k7c7
nemocí	nemoc	k1gFnSc7
bojovat	bojovat	k5eAaImF
a	a	k8xC
podstoupil	podstoupit	k5eAaPmAgMnS
radikální	radikální	k2eAgFnSc4d1
chemoterapii	chemoterapie	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
přišel	přijít	k5eAaPmAgMnS
o	o	k7c4
osm	osm	k4xCc4
let	léto	k1gNnPc2
nestříhané	stříhaný	k2eNgFnSc2d1
dredy	dreda	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
už	už	k6eAd1
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
kondičním	kondiční	k2eAgInSc6d1
běhu	běh	k1gInSc6
v	v	k7c6
newyorském	newyorský	k2eAgNnSc6d1
Central	Central	k1gFnPc2
Parku	park	k1gInSc3
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
zkolaboval	zkolabovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakovina	rakovina	k1gFnSc1
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
jater	játra	k1gNnPc2
i	i	k8xC
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
roku	rok	k1gInSc2
1981	#num#	k4
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
v	v	k7c6
Miami	Miami	k1gNnSc6
na	na	k7c6
Floridě	Florida	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnPc1
poslední	poslední	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
patřila	patřit	k5eAaImAgNnP
jeho	jeho	k3xOp3gNnSc1
synovi	syn	k1gMnSc3
Ziggymu	Ziggym	k1gInSc2
"	"	kIx"
<g/>
Za	za	k7c4
peníze	peníz	k1gInPc4
život	život	k1gInSc1
nekoupíš	koupit	k5eNaPmIp2nS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
po	po	k7c6
slavnostních	slavnostní	k2eAgInPc6d1
obřadech	obřad	k1gInPc6
v	v	k7c6
Kingstonu	Kingston	k1gInSc6
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gInPc4
ostatky	ostatek	k1gInPc4
převezeny	převezen	k2eAgInPc4d1
do	do	k7c2
rodného	rodný	k2eAgNnSc2d1
městečka	městečko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
uloženy	uložit	k5eAaPmNgFnP
v	v	k7c6
kopci	kopec	k1gInSc6
Nine	Nin	k1gFnSc2
Miles	Milesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pochován	pochován	k2eAgMnSc1d1
spolu	spolu	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
kytarou	kytara	k1gFnSc7
Gibson	Gibson	k1gInSc4
Les	les	k1gInSc4
Paul	Paula	k1gFnPc2
<g/>
,	,	kIx,
fotbalovým	fotbalový	k2eAgInSc7d1
míčem	míč	k1gInSc7
<g/>
,	,	kIx,
marihuanovým	marihuanový	k2eAgInSc7d1
pukem	puk	k1gInSc7
<g/>
,	,	kIx,
prstenem	prsten	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
dal	dát	k5eAaPmAgMnS
etiopský	etiopský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Asfa	Asf	k1gInSc2
Wossen	Wossna	k1gFnPc2
a	a	k8xC
biblí	bible	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Děti	dítě	k1gFnPc1
</s>
<s>
Se	s	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
Ritou	Rita	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
3	#num#	k4
společné	společný	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
a	a	k8xC
2	#num#	k4
adoptované	adoptovaný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Sharon	Sharon	k1gInSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dcera	dcera	k1gFnSc1
Rity	Rita	k1gFnSc2
z	z	k7c2
předchozího	předchozí	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
ji	on	k3xPp3gFnSc4
adoptoval	adoptovat	k5eAaPmAgMnS
a	a	k8xC
přijal	přijmout	k5eAaPmAgMnS
za	za	k7c4
vlastní	vlastní	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Cedella	Cedella	k1gFnSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1967	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
Bobova	Bobův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
a	a	k8xC
nejstarší	starý	k2eAgFnSc1d3
biologické	biologický	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
David	David	k1gMnSc1
Nesta	Nesta	k1gMnSc1
"	"	kIx"
<g/>
Ziggy	Zigga	k1gFnPc1
<g/>
"	"	kIx"
Marley	Marle	k2eAgFnPc1d1
(	(	kIx(
<g/>
*	*	kIx~
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1968	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Bobův	Bobův	k2eAgMnSc1d1
první	první	k4xOgMnSc1
a	a	k8xC
nejstarší	starý	k2eAgMnSc1d3
biologický	biologický	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Stephen	Stephen	k1gInSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stephanie	Stephanie	k1gFnSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
1974	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dcera	dcera	k1gFnSc1
Rity	Rita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
mimomanželský	mimomanželský	k2eAgInSc4d1
poměr	poměr	k1gInSc4
s	s	k7c7
mužem	muž	k1gMnSc7
jménem	jméno	k1gNnSc7
Ital	Ital	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Stephanie	Stephanie	k1gFnSc2
adoptoval	adoptovat	k5eAaPmAgInS
a	a	k8xC
přijal	přijmout	k5eAaPmAgInS
za	za	k7c4
vlastní	vlastní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Bob	Bob	k1gMnSc1
měl	mít	k5eAaImAgMnS
dalších	další	k2eAgFnPc2d1
6	#num#	k4
dětí	dítě	k1gFnPc2
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Rohan	Rohan	k1gMnSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matkou	matka	k1gFnSc7
je	být	k5eAaImIp3nS
Janet	Janet	k1gInSc1
Hunt	hunt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Karen	Karen	k1gInSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matkou	matka	k1gFnSc7
je	být	k5eAaImIp3nS
Janet	Janet	k1gInSc1
Bowe	Bow	k1gFnSc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
zvaná	zvaný	k2eAgFnSc1d1
Janet	Janet	k1gMnSc1
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Julian	Julian	k1gMnSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matkou	matka	k1gFnSc7
je	on	k3xPp3gInPc4
Lucy	Lucy	k1gInPc4
Pounder	Poundra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ky-Mani	Ky-Man	k1gMnPc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matkou	matka	k1gFnSc7
je	být	k5eAaImIp3nS
Anita	Anita	k1gFnSc1
Belnavis	Belnavis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Damian	Damian	k1gInSc1
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matkou	matka	k1gFnSc7
je	být	k5eAaImIp3nS
Miss	miss	k1gFnSc1
World	Worlda	k1gFnPc2
roku	rok	k1gInSc2
1976	#num#	k4
Cindy	Cinda	k1gFnSc2
Breakspeare	Breakspear	k1gMnSc5
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
i	i	k9
nejznámější	známý	k2eAgFnSc7d3
milenkou	milenka	k1gFnSc7
Boba	Bob	k1gMnSc2
Marleyho	Marley	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
i	i	k9
poslední	poslední	k2eAgMnSc1d1
a	a	k8xC
nejmladší	mladý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Marleyho	Marley	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Makeda	Makeda	k1gMnSc1
Jahnesta	Jahnest	k1gMnSc2
Marley	Marlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matkou	matka	k1gFnSc7
je	být	k5eAaImIp3nS
Yvette	Yvett	k1gInSc5
Crichton	Crichton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodila	narodit	k5eAaPmAgFnS
se	s	k7c7
19	#num#	k4
dní	den	k1gInPc2
po	po	k7c6
smrti	smrt	k1gFnSc6
Boba	Bob	k1gMnSc2
Marleyho	Marley	k1gMnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gFnSc1
nejmladší	mladý	k2eAgFnSc1d3
dcera	dcera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
–	–	k?
Jamajka	Jamajka	k1gFnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
na	na	k7c6
Hollywoodském	hollywoodský	k2eAgInSc6d1
Chodníku	chodník	k1gInSc6
slávy	sláva	k1gFnSc2
</s>
<s>
Jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
The	The	k1gFnSc1
Wailers	Wailersa	k1gFnPc2
<g/>
"	"	kIx"
</s>
<s>
The	The	k?
Wailing	Wailing	k1gInSc1
Wailers	Wailers	k1gInSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soul	Soul	k1gInSc1
Rebels	Rebels	k1gInSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soul	Soul	k1gInSc1
Revolution	Revolution	k1gInSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Best	Best	k1gMnSc1
of	of	k?
The	The	k1gFnSc2
Wailers	Wailersa	k1gFnPc2
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
v	v	k7c6
hitparádě	hitparáda	k1gFnSc6
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
US	US	kA
200	#num#	k4
</s>
<s>
US	US	kA
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
</s>
<s>
1973	#num#	k4
</s>
<s>
Catch	Catch	k1gInSc1
a	a	k8xC
Fire	Fire	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1973	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
171	#num#	k4
</s>
<s>
51	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
/	/	kIx~
</s>
<s>
1973	#num#	k4
</s>
<s>
Burnin	Burnin	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1973	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
151	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
zlaté	zlatý	k2eAgFnSc2d1
</s>
<s>
Jako	jako	k9
"	"	kIx"
<g/>
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
&	&	k?
The	The	k1gFnSc2
Wailers	Wailersa	k1gFnPc2
<g/>
"	"	kIx"
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
v	v	k7c6
hitparádě	hitparáda	k1gFnSc6
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
US	US	kA
200	#num#	k4
</s>
<s>
US	US	kA
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
</s>
<s>
1974	#num#	k4
</s>
<s>
Natty	Natta	k1gFnPc1
Dread	Dread	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1974	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
92	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
/	/	kIx~
</s>
<s>
1976	#num#	k4
</s>
<s>
Rastaman	Rastaman	k1gMnSc1
Vibration	Vibration	k1gInSc4
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1976	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
zlaté	zlatý	k2eAgFnSc2d1
</s>
<s>
1977	#num#	k4
</s>
<s>
Exodus	Exodus	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1977	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
20	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
zlaté	zlatý	k2eAgFnSc2d1
</s>
<s>
CAN	CAN	kA
<g/>
:	:	kIx,
8	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgInPc1d1
</s>
<s>
1978	#num#	k4
</s>
<s>
Kaya	Kaya	k6eAd1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1978	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
50	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
/	/	kIx~
</s>
<s>
1979	#num#	k4
</s>
<s>
Survival	Survivat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1979	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
70	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
/	/	kIx~
</s>
<s>
CAN	CAN	kA
<g/>
:	:	kIx,
2	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgInPc1d1
</s>
<s>
1980	#num#	k4
</s>
<s>
Uprising	Uprising	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1980	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
45	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
/	/	kIx~
</s>
<s>
1983	#num#	k4
</s>
<s>
Confrontation	Confrontation	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1983	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
55	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
2	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgInPc1d1
</s>
<s>
Živá	živý	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
1975	#num#	k4
–	–	k?
Live	Live	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s>
1978	#num#	k4
–	–	k?
Babylon	Babylon	k1gInSc1
by	by	kYmCp3nS
Bus	bus	k1gInSc4
</s>
<s>
1991	#num#	k4
–	–	k?
Talkin	Talkin	k1gInSc1
<g/>
'	'	kIx"
Blues	blues	k1gNnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
–	–	k?
Live	Live	k1gInSc1
at	at	k?
the	the	k?
Roxy	Roxa	k1gFnSc2
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
–	–	k?
Live	Live	k1gFnPc2
<g/>
:	:	kIx,
1973	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
</s>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1
kompilace	kompilace	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
v	v	k7c6
hitparádě	hitparáda	k1gFnSc6
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
US	US	kA
200	#num#	k4
</s>
<s>
US	US	kA
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
</s>
<s>
US	US	kA
Reggae	Reggae	k1gFnSc1
</s>
<s>
1984	#num#	k4
</s>
<s>
Legend	legenda	k1gFnPc2
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1984	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
54	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
diamantové	diamantový	k2eAgFnSc2d1
</s>
<s>
UK	UK	kA
<g/>
:	:	kIx,
6	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgFnSc2d1
</s>
<s>
CAN	CAN	kA
<g/>
:	:	kIx,
2	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgInPc1d1
</s>
<s>
1992	#num#	k4
</s>
<s>
Songs	Songs	k1gInSc1
of	of	k?
Freedom	Freedom	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1992	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
86	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
2	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgInPc1d1
</s>
<s>
UK	UK	kA
<g/>
:	:	kIx,
stříbrné	stříbrný	k2eAgFnSc2d1
</s>
<s>
CAN	CAN	kA
<g/>
:	:	kIx,
platinové	platinový	k2eAgFnSc2d1
</s>
<s>
1995	#num#	k4
</s>
<s>
Natural	Natural	k?
Mystic	Mystice	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Legend	legenda	k1gFnPc2
Lives	Lives	k1gMnSc1
On	on	k3xPp3gMnSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1995	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
67	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
zlaté	zlatý	k2eAgFnSc2d1
</s>
<s>
2001	#num#	k4
</s>
<s>
One	One	k?
Love	lov	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gMnSc1
Very	Vera	k1gFnSc2
Best	Best	k1gMnSc1
of	of	k?
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2001	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
-	-	kIx~
</s>
<s>
45	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
/	/	kIx~
</s>
<s>
CAN	CAN	kA
<g/>
:	:	kIx,
platinové	platinový	k2eAgFnSc2d1
</s>
<s>
2005	#num#	k4
</s>
<s>
Africa	Africa	k6eAd1
Unite	Unit	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gFnSc1
Singles	Singles	k1gMnSc1
Collection	Collection	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2005	#num#	k4
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1
<g/>
:	:	kIx,
Tuff	Tuff	k1gInSc1
Gong	gong	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
101	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
/	/	kIx~
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1
remixová	remixový	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
1981	#num#	k4
–	–	k?
Chances	Chances	k1gMnSc1
Are	ar	k1gInSc5
</s>
<s>
1999	#num#	k4
–	–	k?
Chant	Chant	k1gInSc1
Down	Down	k1gMnSc1
Babylon	Babylon	k1gInSc1
</s>
<s>
2007	#num#	k4
–	–	k?
Roots	Roots	k1gInSc1
<g/>
,	,	kIx,
Rock	rock	k1gInSc1
<g/>
,	,	kIx,
Remixed	Remixed	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.bobmarley.com	www.bobmarley.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
http://www.discogs.com/artist/Bob+Marley	http://www.discogs.com/artist/Bob+Marley	k1gInPc7
<g/>
↑	↑	k?
7	#num#	k4
Fascinating	Fascinating	k1gInSc1
Facts	Facts	k1gInSc4
About	About	k2eAgMnSc1d1
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SAMUELS	SAMUELS	kA
<g/>
,	,	kIx,
A.	A.	kA
J.	J.	kA
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gMnSc2
<g/>
:	:	kIx,
Anatomy	anatom	k1gMnPc4
of	of	k?
an	an	k?
Icon	Icon	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
'	'	kIx"
<g/>
Marley	Marlea	k1gMnSc2
<g/>
'	'	kIx"
-	-	kIx~
a	a	k8xC
new	new	k?
view	view	k?
of	of	k?
a	a	k8xC
cultural	culturat	k5eAaImAgMnS,k5eAaPmAgMnS
icon	icon	k1gInSc4
-	-	kIx~
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TOYNBEE	TOYNBEE	kA
<g/>
,	,	kIx,
Jason	Jason	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
Herald	Herald	k1gInSc1
of	of	k?
a	a	k8xC
Postcolonial	Postcolonial	k1gMnSc1
World	World	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
<g/>
,	,	kIx,
8	#num#	k4
May	May	k1gMnSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7456	#num#	k4
<g/>
-	-	kIx~
<g/>
5737	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1969	#num#	k4
<g/>
–	–	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GOODEN	GOODEN	kA
<g/>
,	,	kIx,
Lou	Lou	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reggae	Reggae	k1gFnSc1
Heritage	Heritage	k1gFnSc1
<g/>
:	:	kIx,
Jamaica	Jamaica	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Music	Music	k1gMnSc1
History	Histor	k1gInPc4
<g/>
,	,	kIx,
Culture	Cultur	k1gMnSc5
&	&	k?
Politic	Politice	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
AuthorHouse	AuthorHouse	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4107	#num#	k4
<g/>
-	-	kIx~
<g/>
8062	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
293	#num#	k4
<g/>
–	–	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
interview	interview	k1gNnSc6
<g/>
↑	↑	k?
BARRETT	BARRETT	kA
<g/>
,	,	kIx,
Aston	Aston	k1gNnSc1
"	"	kIx"
<g/>
Family	Famil	k1gInPc1
Man	mana	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interview	interview	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
19	#num#	k4
February	Februara	k1gFnSc2
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
6	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MCATEER	MCATEER	kA
<g/>
,	,	kIx,
Amberly	Amberl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deadly	Deadly	k1gFnPc2
profitable	profitable	k6eAd1
<g/>
:	:	kIx,
The	The	k1gFnSc1
13	#num#	k4
highest-earning	highest-earning	k1gInSc1
dead	dead	k6eAd1
celebrities	celebrities	k1gInSc1
<g/>
.	.	kIx.
www.theglobeandmail.com	www.theglobeandmail.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Globe	globus	k1gInSc5
and	and	k?
Mail	mail	k1gInSc1
<g/>
,	,	kIx,
15	#num#	k4
October	October	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
October	October	k1gInSc1
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MESCHINO	MESCHINO	kA
<g/>
,	,	kIx,
Patricia	Patricius	k1gMnSc2
<g/>
.	.	kIx.
'	'	kIx"
<g/>
Exodus	Exodus	k1gInSc1
<g/>
'	'	kIx"
Returns	Returns	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nielsen	Nielsen	k2eAgInSc1d1
Business	business	k1gInSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
6	#num#	k4
October	October	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
23	#num#	k4
August	August	k1gMnSc1
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2510	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MASOURI	MASOURI	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wailing	Wailing	k1gInSc1
Blues	blues	k1gFnSc2
–	–	k?
The	The	k1gMnSc1
Story	story	k1gFnSc2
of	of	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Wailers	Wailersa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Music	Music	k1gMnSc1
Sales	Sales	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
11	#num#	k4
November	November	k1gInSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
85712	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
35	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SONI	Soňa	k1gFnPc1
<g/>
,	,	kIx,
Varun	Varun	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Spiritual	Spiritual	k1gMnSc1
Legacy	Legaca	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JEAN	Jean	k1gMnSc1
<g/>
,	,	kIx,
Wyclef	Wyclef	k1gMnSc1
<g/>
.	.	kIx.
100	#num#	k4
Greatest	Greatest	k1gInSc1
Artists	Artists	k1gInSc4
<g/>
:	:	kIx,
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MOSKOWITZ	MOSKOWITZ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
33879	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Observer	Observer	k1gMnSc1
Reporter	Reporter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ziggy	Zigg	k1gInPc1
Marley	Marle	k2eAgInPc1d1
to	ten	k3xDgNnSc4
adopt	adopt	k2eAgInSc4d1
Judaism	Judaism	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Jamaica	Jamaica	k1gFnSc2
Observer	Observra	k1gFnPc2
<g/>
.	.	kIx.
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOMBACH	HOMBACH	kA
<g/>
,	,	kIx,
Jean-Pierre	Jean-Pierr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Father	Fathra	k1gFnPc2
of	of	k?
Music	Music	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Lulu	lula	k1gFnSc4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781471620454	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
52	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KENNER	KENNER	kA
<g/>
,	,	kIx,
Rob	roba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Real	Real	k1gInSc4
Revolutionary	Revolutionara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vibe	Vib	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vibe	Vibe	k1gNnSc7
Media	medium	k1gNnSc2
Group	Group	k1gInSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1070	#num#	k4
<g/>
-	-	kIx~
<g/>
4701	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
ADAMS	ADAMS	kA
<g/>
,	,	kIx,
Tim	Tim	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
the	the	k?
regret	regret	k5eAaImF,k5eAaPmF,k5eAaBmF
that	that	k2eAgInSc4d1
haunted	haunted	k1gInSc4
his	his	k1gNnSc2
life	lif	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Observer	Observer	k1gMnSc1
<g/>
.	.	kIx.
8	#num#	k4
April	April	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MOSKOWITZ	MOSKOWITZ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
33879	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MOSKOWITZ	MOSKOWITZ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
33879	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DAVIS	DAVIS	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
the	the	k?
biography	biographa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Littlehampton	Littlehampton	k1gInSc1
Book	Book	k1gInSc1
Services	Services	k1gInSc4
Ltd	ltd	kA
<g/>
,	,	kIx,
28	#num#	k4
July	Jula	k1gFnSc2
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
213168599	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stepney	Stepne	k2eAgFnPc4d1
Primary	Primara	k1gFnPc4
and	and	k?
Junior	junior	k1gMnSc1
High	High	k1gMnSc1
School	School	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc4
Marley	Marlea	k1gFnSc2
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
16	#num#	k4
September	September	k1gInSc1
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARLEY	MARLEY	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listen	listen	k1gInSc1
to	ten	k3xDgNnSc4
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gMnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Music	Music	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Revolution	Revolution	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Open	Open	k1gNnSc1
Road	Roada	k1gFnPc2
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
31	#num#	k4
January	Januara	k1gFnSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4532	#num#	k4
<g/>
-	-	kIx~
<g/>
2494	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
65	#num#	k4
<g/>
–	–	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MOSKOWITZ	MOSKOWITZ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
33879	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Family	Famil	k1gMnPc7
Settles	Settles	k1gMnSc1
Lawsuit	Lawsuit	k1gMnSc1
With	With	k1gMnSc1
Singer	Singer	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Half-Brother	Half-Brothra	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cedella	Cedella	k1gMnSc1
Marley	Marlea	k1gFnSc2
Booker	Booker	k1gMnSc1
<g/>
:	:	kIx,
Keeper	Keeper	k1gMnSc1
of	of	k?
the	the	k?
Marley	Marlea	k1gFnSc2
flame	flamat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
April	April	k1gInSc1
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
interview	interview	k1gNnSc6
<g/>
↑	↑	k?
CUNNINGHAM	CUNNINGHAM	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Memorial	Memorial	k1gInSc1
Services	Servicesa	k1gFnPc2
for	forum	k1gNnPc2
Cedella	Cedella	k1gMnSc1
Marley	Marlea	k1gFnSc2
Booker	Booker	k1gMnSc1
Tonight	Tonight	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
15	#num#	k4
April	April	k1gInSc1
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OBRECHT	OBRECHT	kA
<g/>
,	,	kIx,
Jas	jas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Early	earl	k1gMnPc7
Years	Yearsa	k1gFnPc2
<g/>
:	:	kIx,
From	From	k1gInSc1
Nine	Nin	k1gFnSc2
Miles	Milesa	k1gFnPc2
To	to	k9
London	London	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
interview	interview	k1gNnSc6
<g/>
↑	↑	k?
FOSTER	FOSTER	kA
<g/>
,	,	kIx,
Chuck	Chuck	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnSc1
Higgs	Higgsa	k1gFnPc2
–	–	k?
No	no	k9
Man	Man	k1gMnSc1
Could	Coulda	k1gFnPc2
Stop	stop	k1gInSc1
The	The	k1gMnSc1
Source	Source	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
November	November	k1gInSc1
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PARELES	PARELES	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnSc1
Higgs	Higgsa	k1gFnPc2
<g/>
,	,	kIx,
59	#num#	k4
<g/>
,	,	kIx,
Reggae	Reggae	k1gFnSc1
Performer	Performer	k1gMnSc1
<g/>
;	;	kIx,
Taught	Taught	k1gInSc1
a	a	k8xC
Generation	Generation	k1gInSc1
of	of	k?
Singers	Singers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
22	#num#	k4
December	December	k1gInSc1
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
November	November	k1gInSc1
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
Solo	Solo	k1gMnSc1
<g/>
,	,	kIx,
1962	#num#	k4
Wailer	Wailer	k1gInSc1
–	–	k?
The	The	k1gFnSc2
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gMnSc2
Compendium	Compendium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
8	#num#	k4
November	Novembero	k1gNnPc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gMnSc1
Beverley	Beverlea	k1gFnSc2
Label	Label	k1gMnSc1
and	and	k?
Leslie	Leslie	k1gFnSc1
Kong	Kongo	k1gNnPc2
<g/>
:	:	kIx,
Music	Musice	k1gFnPc2
Business	business	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bobmarley	bobmarley	k1gInPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
21	#num#	k4
June	jun	k1gMnSc5
2006	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JEFFREY	JEFFREY	kA
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disputes	Disputes	k1gMnSc1
Over	Over	k1gMnSc1
Copyrights	Copyrights	k1gInSc1
'	'	kIx"
<g/>
Scorch	Scorch	k1gInSc1
<g/>
'	'	kIx"
Jamaican	Jamaican	k1gInSc1
Reggae	Regga	k1gFnSc2
Artists	Artistsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nielsen	Nielsen	k2eAgInSc1d1
Business	business	k1gInSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
16	#num#	k4
July	Jula	k1gFnSc2
1994	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
92	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2510	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ranglin	Ranglin	k1gInSc1
Interview	interview	k1gNnSc1
with	with	k1gInSc1
Angus	Angus	k1gMnSc1
Taylor	Taylor	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
February	Februara	k1gFnSc2
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
6	#num#	k4
November	Novembero	k1gNnPc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnSc1
Wailers	Wailers	k1gInSc1
<g/>
'	'	kIx"
Biography	Biographa	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
10	#num#	k4
September	September	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WHITE	WHITE	kA
<g/>
,	,	kIx,
Timothy	Timoth	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
<g/>
:	:	kIx,
1945	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jann	Jann	k1gMnSc1
Wenner	Wenner	k1gMnSc1
<g/>
,	,	kIx,
25	#num#	k4
June	jun	k1gMnSc5
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
21	#num#	k4
April	April	k1gInSc1
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MOSKOWITZ	MOSKOWITZ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Words	Wordsa	k1gFnPc2
and	and	k?
Music	Music	k1gMnSc1
of	of	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
MOSKOWITZ	MOSKOWITZ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Words	Wordsa	k1gFnPc2
and	and	k?
Music	Music	k1gMnSc1
of	of	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
MCKINLEY	MCKINLEY	kA
<g/>
,	,	kIx,
Jesse	Jesse	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pre-reggae	Pre-reggae	k1gNnSc1
tape	tap	k1gInSc2
of	of	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
is	is	k?
found	found	k1gInSc1
and	and	k?
put	puta	k1gFnPc2
on	on	k3xPp3gMnSc1
auction	auction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
19	#num#	k4
December	December	k1gInSc1
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
4	#num#	k4
January	Januara	k1gFnSc2
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MUIR	MUIR	kA
<g/>
,	,	kIx,
Hugh	Hugh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blue	Blu	k1gInSc2
plaque	plaquat	k5eAaPmIp3nS
marks	marks	k6eAd1
flats	flats	k6eAd1
that	that	k5eAaPmF
put	puta	k1gFnPc2
Marley	Marlea	k1gFnSc2
on	on	k3xPp3gMnSc1
road	road	k1gMnSc1
to	ten	k3xDgNnSc4
fame	fame	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UK	UK	kA
<g/>
:	:	kIx,
27	#num#	k4
October	October	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
7	#num#	k4
September	September	k1gInSc1
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Recenze	recenze	k1gFnSc1
alba	alba	k1gFnSc1
Babylon	Babylon	k1gInSc1
By	by	kYmCp3nS
Bus	bus	k1gInSc4
na	na	k7c4
www.rollingstone.com	www.rollingstone.com	k1gInSc4
<g/>
↑	↑	k?
Recenze	recenze	k1gFnSc2
alba	album	k1gNnSc2
Babylon	Babylon	k1gInSc4
By	by	k9
Bus	bus	k1gInSc4
na	na	k7c4
www.allmusic.com	www.allmusic.com	k1gInSc4
<g/>
↑	↑	k?
MOSKOWITZ	MOSKOWITZ	kA
<g/>
,	,	kIx,
DAVID	David	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
DAVID	David	k1gMnSc1
VLADO	VLADO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
-	-	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
words	wordsa	k1gFnPc2
and	and	k?
music	music	k1gMnSc1
of	of	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Westport	Westport	k1gInSc1
<g/>
,	,	kIx,
Conn	Conn	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Praeger	Praeger	k1gInSc1
1	#num#	k4
online	onlinout	k5eAaPmIp3nS
resource	resourka	k1gFnSc3
(	(	kIx(
<g/>
xiii	xiii	k1gNnSc1
<g/>
,	,	kIx,
176	#num#	k4
pages	pagesa	k1gFnPc2
<g/>
)	)	kIx)
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6522	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6522	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
231693753	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rude	Rude	k1gFnSc1
boys	boy	k1gMnPc2
</s>
<s>
Damian	Damian	k1gInSc1
Marley	Marlea	k1gFnSc2
</s>
<s>
Reggae	Reggae	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Bob	bob	k1gInSc1
Marley	Marlea	k1gFnSc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Discografie	Discografie	k1gFnSc1
na	na	k7c4
Discogs	Discogs	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Robertnestamarley	Robertnestamarle	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
wbs	wbs	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Nejobsáhlejší	obsáhlý	k2eAgFnSc2d3
stránky	stránka	k1gFnSc2
o	o	k7c6
Bobovi	Bob	k1gMnSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
OneLove	OneLov	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Stránky	stránka	k1gFnSc2
o	o	k7c6
Bobovi	Bob	k1gMnSc6
<g/>
,	,	kIx,
roots	roots	k6eAd1
reggae	reggae	k1gNnSc1
a	a	k8xC
rastafariánství	rastafariánství	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Bobmarley	Bobmarle	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
kvalitne	kvalitnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Bohužel	bohužel	k9
již	již	k6eAd1
dlouho	dlouho	k6eAd1
needitované	editovaný	k2eNgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
and	and	k?
the	the	k?
Wailers	Wailersa	k1gFnPc2
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
•	•	k?
Junior	junior	k1gMnSc1
Braithwaite	Braithwait	k1gInSc5
•	•	k?
Beverley	Beverley	k1gInPc1
Kelso	Kelsa	k1gFnSc5
•	•	k?
Bunny	Bunna	k1gFnSc2
Wailer	Wailer	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Tosh	Tosh	k1gMnSc1
•	•	k?
Cherry	Cherra	k1gFnSc2
Smith	Smith	k1gMnSc1
•	•	k?
Constantine	Constantin	k1gInSc5
„	„	k?
<g/>
Vision	vision	k1gInSc1
<g/>
“	“	k?
Walker	Walker	k1gMnSc1
•	•	k?
Earl	earl	k1gMnSc1
„	„	k?
<g/>
Chinna	Chinno	k1gNnSc2
<g/>
“	“	k?
Smith	Smith	k1gMnSc1
•	•	k?
Aston	Aston	k1gMnSc1
Barrett	Barrett	k1gMnSc1
•	•	k?
Joe	Joe	k1gFnSc1
Higgs	Higgsa	k1gFnPc2
•	•	k?
Earl	earl	k1gMnSc1
Lindo	Linda	k1gFnSc5
•	•	k?
Carlton	Carlton	k1gInSc4
Barrett	Barrett	k2eAgInSc4d1
•	•	k?
Al	ala	k1gFnPc2
Anderson	Anderson	k1gMnSc1
•	•	k?
Alvin	Alvin	k1gMnSc1
Patterson	Patterson	k1gMnSc1
•	•	k?
Junior	junior	k1gMnSc1
Marvin	Marvina	k1gFnPc2
•	•	k?
Donald	Donald	k1gMnSc1
Kinsey	Kinsea	k1gFnSc2
•	•	k?
Tyrone	Tyron	k1gInSc5
Downie	Downie	k1gFnPc4
•	•	k?
I	i	k9
Threes	Threes	k1gInSc4
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
The	The	k?
Wailing	Wailing	k1gInSc1
Wailers	Wailers	k1gInSc1
•	•	k?
Soul	Soul	k1gInSc1
Rebels	Rebels	k1gInSc1
•	•	k?
Soul	Soul	k1gInSc1
Revolution	Revolution	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Best	Best	k1gMnSc1
of	of	k?
The	The	k1gFnSc1
Wailers	Wailers	k1gInSc1
•	•	k?
Catch	Catch	k1gInSc1
a	a	k8xC
Fire	Fire	k1gInSc1
•	•	k?
Burnin	Burnin	k1gInSc1
<g/>
'	'	kIx"
•	•	k?
Natty	Natt	k1gInPc1
Dread	Dread	k1gInSc1
•	•	k?
Rastaman	Rastaman	k1gMnSc1
Vibration	Vibration	k1gInSc1
•	•	k?
Exodus	Exodus	k1gInSc1
•	•	k?
Kaya	Kay	k1gInSc2
•	•	k?
Survival	Survival	k1gMnSc1
•	•	k?
Uprising	Uprising	k1gInSc1
•	•	k?
Confrontation	Confrontation	k1gInSc4
Koncertní	koncertní	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Live	Live	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Babylon	Babylon	k1gInSc1
by	by	kYmCp3nS
Bus	bus	k1gInSc4
•	•	k?
Talkin	Talkin	k1gInSc1
<g/>
'	'	kIx"
Blues	blues	k1gNnSc1
•	•	k?
Live	Liv	k1gInSc2
at	at	k?
the	the	k?
Roxy	Roxa	k1gFnSc2
Kompilační	kompilační	k2eAgFnSc2d1
alba	album	k1gNnPc4
</s>
<s>
Rasta	Rasta	k1gFnSc1
Revolution	Revolution	k1gInSc1
•	•	k?
African	African	k1gInSc1
Herbsman	Herbsman	k1gMnSc1
•	•	k?
Chances	Chances	k1gMnSc1
Are	ar	k1gInSc5
•	•	k?
Interviews	Interviewsa	k1gFnPc2
•	•	k?
Legend	legenda	k1gFnPc2
•	•	k?
Songs	Songs	k1gInSc1
of	of	k?
Freedom	Freedom	k1gInSc1
•	•	k?
Natural	Natural	k?
Mystic	Mystice	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Legend	legenda	k1gFnPc2
Lives	Lives	k1gMnSc1
On	on	k3xPp3gInSc1
•	•	k?
Rock	rock	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Rock	rock	k1gInSc1
•	•	k?
Chant	Chant	k1gInSc1
Down	Down	k1gMnSc1
Babylon	Babylon	k1gInSc1
•	•	k?
One	One	k1gFnSc2
Love	lov	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gMnSc1
Very	Vera	k1gFnSc2
Best	Best	k1gMnSc1
of	of	k?
Bob	Bob	k1gMnSc1
Marley	Marlea	k1gFnSc2
&	&	k?
The	The	k1gFnSc2
Wailers	Wailersa	k1gFnPc2
•	•	k?
Gold	Gold	k1gMnSc1
•	•	k?
Keep	Keep	k1gInSc1
On	on	k3xPp3gInSc1
Skanking	Skanking	k1gInSc1
•	•	k?
Africa	Africa	k1gFnSc1
Unite	Unit	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gFnSc1
Singles	Singles	k1gMnSc1
Collection	Collection	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20011212024	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118578057	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2276	#num#	k4
8131	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81020153	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
14778710	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81020153	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
