<s>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
My	my	k3xPp1nPc1	my
Nightmare	Nightmar	k1gMnSc5	Nightmar
je	být	k5eAaImIp3nS	být
první	první	k4xOgInPc4	první
sólové	sólový	k2eAgInPc4d1	sólový
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
alba	album	k1gNnPc1	album
vycházela	vycházet	k5eAaImAgNnP	vycházet
jako	jako	k8xC	jako
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
Alice	Alice	k1gFnSc2	Alice
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
)	)	kIx)	)
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
amerického	americký	k2eAgMnSc2d1	americký
zpěváka	zpěvák	k1gMnSc2	zpěvák
Alice	Alice	k1gFnSc2	Alice
Coopera	Cooper	k1gMnSc2	Cooper
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
My	my	k3xPp1nPc1	my
Nightmare	Nightmar	k1gMnSc5	Nightmar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Dick	Dick	k1gMnSc1	Dick
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
"	"	kIx"	"
<g/>
Devil	Devil	k1gInSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Food	Fooda	k1gFnPc2	Fooda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
,	,	kIx,	,
Kelley	Kelle	k2eAgFnPc1d1	Kelle
Jay	Jay	k1gFnPc1	Jay
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
38	[number]	k4	38
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Widow	Widow	k1gMnSc1	Widow
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
"	"	kIx"	"
<g/>
Some	Som	k1gInSc2	Som
Folks	Folks	k1gInSc1	Folks
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
)	)	kIx)	)
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
"	"	kIx"	"
<g/>
Only	Onla	k1gFnSc2	Onla
Women	Women	k2eAgMnSc1d1	Women
Bleed	Bleed	k1gMnSc1	Bleed
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
"	"	kIx"	"
<g/>
Department	department	k1gInSc1	department
of	of	k?	of
Youth	Youth	k1gInSc1	Youth
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
"	"	kIx"	"
<g/>
Cold	Cold	k1gInSc1	Cold
Ethyl	ethyl	k1gInSc1	ethyl
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
)	)	kIx)	)
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Years	Years	k1gInSc1	Years
Ago	aga	k1gMnSc5	aga
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
)	)	kIx)	)
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
"	"	kIx"	"
<g/>
Steven	Stevna	k1gFnPc2	Stevna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Awakening	Awakening	k1gInSc1	Awakening
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Wagner	Wagner	k1gMnSc1	Wagner
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
)	)	kIx)	)
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
"	"	kIx"	"
<g/>
Escape	Escap	k1gInSc5	Escap
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Anthony	Anthona	k1gFnSc2	Anthona
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnSc2	Kim
Fowley	Fowlea	k1gMnSc2	Fowlea
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
"	"	kIx"	"
<g/>
Devils	Devils	k1gInSc1	Devils
<g/>
'	'	kIx"	'
Food	Food	k1gInSc1	Food
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Alternate	Alternat	k1gMnSc5	Alternat
Version	Version	k1gInSc4	Version
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
"	"	kIx"	"
<g/>
Cold	Cold	k1gInSc1	Cold
Ethyl	ethyl	k1gInSc1	ethyl
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Alternate	Alternat	k1gMnSc5	Alternat
Version	Version	k1gInSc4	Version
<g/>
)	)	kIx)	)
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Awakening	Awakening	k1gInSc1	Awakening
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Alternate	Alternat	k1gMnSc5	Alternat
Version	Version	k1gInSc4	Version
<g/>
)	)	kIx)	)
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
Alice	Alice	k1gFnSc2	Alice
Cooper	Coopra	k1gFnPc2	Coopra
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Bob	Bob	k1gMnSc1	Bob
Ezrin	Ezrin	k1gMnSc1	Ezrin
-	-	kIx~	-
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Vincent	Vincent	k1gMnSc1	Vincent
Price	Price	k1gMnSc1	Price
-	-	kIx~	-
efekty	efekt	k1gInPc1	efekt
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Dick	Dick	k1gMnSc1	Dick
Wagner	Wagner	k1gMnSc1	Wagner
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Steve	Steve	k1gMnSc1	Steve
Hunter	Hunter	k1gMnSc1	Hunter
-	-	kIx~	-
Electric	Electrice	k1gFnPc2	Electrice
and	and	k?	and
Acoustic	Acoustice	k1gFnPc2	Acoustice
Guitar	Guitar	k1gMnSc1	Guitar
Josef	Josef	k1gMnSc1	Josef
Chirowski	Chirowsk	k1gFnSc2	Chirowsk
-	-	kIx~	-
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
clavinet	clavinet	k1gMnSc1	clavinet
Prakash	Prakash	k1gMnSc1	Prakash
John	John	k1gMnSc1	John
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Tony	Tony	k1gMnSc1	Tony
Levin	Levin	k1gMnSc1	Levin
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Pentti	Pentť	k1gFnSc2	Pentť
"	"	kIx"	"
<g/>
Whitey	Whitea	k1gFnSc2	Whitea
<g/>
"	"	kIx"	"
Glan	Glan	k1gInSc1	Glan
-	-	kIx~	-
bicí	bicí	k2eAgFnSc2d1	bicí
Johnny	Johnna	k1gFnSc2	Johnna
"	"	kIx"	"
<g/>
Bee	Bee	k1gMnSc1	Bee
<g/>
"	"	kIx"	"
Badanjek	Badanjka	k1gFnPc2	Badanjka
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
</s>
